//当前视窗
var curDiv = 1;
// 配置内容请求URL
var c_url = "";
// 配置组别请求URL
var g_url = "";
// 配置内容表格列集合
var c_columns = [];
// 配置组别表格列集合
var g_columns = [];

// 初始化全局参数
function initParam() {
	c_url = base + '/admin/agentconfig/list.do';
	g_url = base + "/admin/agentconfig/gplst.do";
	c_columns = [ {
		field : 'groupName',
		title : '组别名称',
		align : 'center',
		valign : 'middle',
	}, {
		field : 'name',
		title : '配置名称',
		align : 'center',
		valign : 'middle',
	}, {
		field : 'title',
		title : '配置标题',
		align : 'center',

		valign : 'middle'
	}, {
		field : 'type',
		title : '配置类型',
		align : 'center',
		valign : 'bottom',
		formatter : typeFormatter
	}, {
		field : 'initValue',
		title : '预设值',
		align : 'center',
		valign : 'bottom'
	}, {
		field : 'orderNo',
		title : '排序编码',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		events : operateEventsConf,
		formatter : statusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		events : operateEventsConf,
		formatter : operateFormatterConf
	} ];

	g_columns = [ {
		field : 'name',
		title : '组别名称',
		align : 'center',
		valign : 'middle',
	}, {
		field : 'orderNo',
		title : '排序编码',
		align : 'center',
		valign : 'middle',
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		events : operateEventsGroup,
		formatter : operateFormatterGroup
	} ];
}
// 初始化表格内容
function initTab() {
	window.table = new Game.Table({
		id : 'datagrid_tb',
		url : c_url,
		queryParams : queryParams,// 参数
		toolbar : $('#toolbar'),
		columns : c_columns
	});
}

// 切变表格内容
function getTab(cols, url) {

	$("#datagrid_tb").bootstrapTable('refreshOptions', {
		columns : cols,
		url : url,
		pageNumber : 1
	});
}

// 类型格式化方法
function typeFormatter(value, row, index) {

	return GlobalTypeUtil.getTypeName(8, 1, value);
}

function statusFormatter(value, row, index) {
	if (value === 2) {
		return '<span class="text-success">可见</span><a href="#"><span class="text-danger opStop">[禁用]</span></a><a href="#"><span class="text-danger opHidden">[隐藏]</span></a>';
	} else if (value === 3) {
		return '<span class="text-success">隐藏</span><a href="#"><span class="text-danger opStart">[可见]</span></a><a href="#"><span class="text-danger opStop">[禁用]</span></a>';
	}
	return '<span class="text-success">禁用</span><a href="#"><span class="text-danger opStart">[可见]</span></a><a href="#"><span class="text-danger opHidden">[隐藏]</span></a>';
}

function operateFormatterGroup(value, row, index) {
	return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
			'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
			'<a class="remove" href="javascript:void(0)" title="删除">',
			'<i class="glyphicon glyphicon-remove"></i>', '</a>' ].join('');
}

function operateFormatterConf(value, row, index) {
	return [ '<a class="detail" href="javascript:void(0)" title="预览">',
			'<i class="glyphicon glyphicon-eye-open"></i>', '</a>',
			'<a class="eidt" href="javascript:void(0)" title="修改">',
			'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
			'<a class="remove" href="javascript:void(0)" title="删除">',
			'<i class="glyphicon glyphicon-remove"></i>', '</a>' ].join('');
}
// 组别操作事件
window.operateEventsGroup = {
	'click .eidt' : function(e, value, row, index) {
		$("#editmodel").modal('toggle');
		$("#gphdId").val(row.id);
		$("#groupName").val(row.name);
		$("#orderNo").val(row.orderNo);
	},
	'click .remove' : function(e, value, row, index) {
		remove(row);
	}
};
// 内容操作事件
window.operateEventsConf = {
	'click .detail' : function(e, value, row, index) {

		detail(row);
		$("#detailmodel").modal('toggle');
	},
	'click .eidt' : function(e, value, row, index) {

		$("#editmodel").modal('toggle');
		$("#confId").val(row.id);
		$("#name").val(row.name);
		$("#title").val(row.title);
		$("#type").val(row.type);
		$("#key").val(row.key);
		$("#source").val(row.source);
		$("#initValue").val(row.initValue);
		$("#expand").val(row.expand);
		$("#remark").val(row.remark);
		$("#status").val(row.status);
		$("#groupId").val(row.groupId);
		$("#platform").val(row.platform);
		$("#bOrderNo").val(row.orderNo);
	},
	'click .opStart' : function(e, value, row, index) {
		row.status = 2;
		updStatus(row);
	},
	'click .opStop' : function(e, value, row, index) {
		row.status = 1;
		updStatus(row);
	},
	'click .opHidden' : function(e, value, row, index) {
		row.status = 3;
		updStatus(row);
	},
	'click .remove' : function(e, value, row, index) {
		remove(row);
	}
};

// 设置传入参数
function queryParams(params) {
	params["name"] = $("#sname").val();
	params["groupName"] = $("#sgroupName").val();
	params["groupId"] = $("#sgroupId").val();
	params["platform"] = $("#splatform").val();
	return params
}
$(function() {
	initParam();
	initTab();
	initCombo();
	initStationLst();
	// 绑定下拉事件
	bindNavTab();
	bindStationLst();
	bindPlatfromCombo();
	initGroupCombo(1);
});

function bindNavTab() {
	$("a[data-toggle='tab']").click(function() {
		var id = this.id;
		if (id === "confnav") {
			$("#confnav_div").removeClass("hidden");
			$("#setnav_div").addClass("hidden");
			$("#confEditDiv").removeClass("hidden");
			$("#groupEditDiv").addClass("hidden");
			$("#gnSearch").removeClass("hidden");
			$("#saveCheck").addClass("hidden");
			getTab(c_columns, c_url);
			curDiv = 1;
		} else if (id === "setnav") {
			$("#setnav_div").removeClass("hidden");
			$("#confnav_div").addClass("hidden");
			$("#gnSearch").addClass("hidden");
			$("#stationId").val(0);
			$("#stationId").change();
			$("#saveConf").removeClass("hidden");
			$("#saveCheck").removeClass("hidden");
			curDiv = 2;
		} else if (id === "groupnav") {
			$("#confnav_div").removeClass("hidden");
			$("#setnav_div").addClass("hidden");
			$("#groupEditDiv").removeClass("hidden");
			$("#confEditDiv").addClass("hidden");
			$("#gnSearch").addClass("hidden");
			$("#saveCheck").addClass("hidden");
			getTab(g_columns, g_url);
			curDiv = 3;
		} else if (id === "stationnav") {
			$("#setnav_div").removeClass("hidden");
			$("#confnav_div").addClass("hidden");
			$("#stationId").val(0);
			$("#stationId").change();
			$("#saveConf").addClass("hidden");
			$("#gnSearch").addClass("hidden");
			$("#saveCheck").addClass("hidden");
			curDiv = 4;
		}
	});
}

function initStationLst() {
	$.ajax({
		url : base + "/admin/station/combo.do",
		success : function(result) {
			var eachdata = {
				"data" : result
			};
			var html = template('combolst_tpl', eachdata);
			$("#stationId").html(html);
		}
	});
}

function bindStationLst() {
	$("#stationId").change(function() {
		$("#settings").html("");
		$("#kj_gps").html("");
		$("#saveConf").attr("disabled", "disabled")
		var selval = $(this).children('option:selected').val();
		if (selval > 0) {
			if (curDiv == 2) {
				loadMenuByStationId(selval);
			} else {
				loadConfByStationId(selval);
			}
		}
	});
}
/**
 * 加载租户权限菜单
 */
function loadMenuByStationId(staId) {
	$.ajax({
		url : base + "/admin/agentconfig/stationset.do",
		data : {stationId : staId},
		success : function(data) {
			var checklst = {};
			var groupMap = {
				0 : {
					id : 0,
					name : "其他",
					sons : []
				}
			};
			var gp;
			for (var i = 0; i < data.gps.length; i++) {
				gp = data.gps[i];
				gp.sons = [];
				groupMap[gp.id] = gp;
			}
			data.gps.push(groupMap[0]);
			var groupchecklst = {},pms=data.pm;
			for (var i = 0; i < pms.length; i++) {
				checklst[pms[i].configId] = 'checked="checked"';
			}
			for (var i = 0; i < data.confs.length; i++) {
				var c = data.confs[i];
				if (c.groupId) {
					if (checklst[c.id]) {
						groupchecklst[c.groupId] = 'checked="checked"';
					}
					groupMap[c.groupId].sons.push(c);
				} else {
					if (checklst[c.id]) {
						groupchecklst[0] = 'checked="checked"';
					}
					groupMap[0].sons.push(c);
				}
			}

			data.checkLst = checklst;
			data.groupchecklst = groupchecklst;

			var html = template('settings_tpl', data);
			$("#settings").html(html);
			$("#saveConf").removeAttr("disabled");

			// 绑定向上和向下的级联操作
			$(":checkbox").change(function() {
				checkMenu(this);
			});
		}
	});
}

/**
 * 加载租户权限菜单
 */
function loadConfByStationId(staId) {
	$.ajax({
		url : base + "/admin/agentconfig/stationvals.do",
		data : {
			stationId : staId
		},
		success : function(data) {
			handlerData(data);
			initXeditable(data);
			initGroupCombo();
		}
	});
}

function checkMenu(chk) {
	sonMenu(chk);
	parentMenu(chk);
}

function sonMenu(chk) {
	var val = $(chk).val();
	var curChecked = chk.checked;
	// 点击当前checkbox下级的处理
	var others = $(":checkbox[parent='" + val + "']");
	if (!others) {
		return;
	}
	var len = others.length;
	others.each(function() {
		this.checked = curChecked;
		// 递归调用
		sonMenu(this);
	});
}

function parentMenu(chk) {
	var curChecked = chk.checked;
	var par = $(chk).attr("parent")
	var others = $(":checkbox[parent='" + par + "']:checked");// 同级别其他元素
	if (!others) {
		return;

	}
	var len = others.length;
	// 点击当前checkbox上级的处理
	if ((len == 0 && !curChecked) || (len == 1 && curChecked)) {
		var chkson = document.getElementById(par);
		if(chkson){
			chkson.checked = curChecked;
			// 递归调用
			parentMenu(chkson);
		}
	}
}

function initCombo() {
	var typedata = {
		"data" : GlobalTypeUtil.getCombo(8, 1)
	};
	var typehtml = template('combolst_tpl', typedata);
	$("#type").html(typehtml);
}

function initGroupCombo(platform) {
	$.ajax({
		url : base + "/admin/agentconfig/gpcombo.do",
		data : {
			platform : platform
		},
		success : function(result) {
			var data = {
				"data" : result
			};
			if(curDiv == 4){
				var gpsHtml = template('gps_tpl',data);
				$("#kj_gps").html(gpsHtml);
			}else{
				var html = template('combolst_tpl', data);
				$("#groupId").html(html);
				$("#sgroupId").html(html);
			}
		}
	});
}

function updStatus(row) {
	$.ajax({
		url : base + "/admin/agentconfig/updStatus.do",
		data : row,
		success : function(result) {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	});
}

function remove(row) {
	var d_url = base + "/admin/agentconfig/delete.do";
	if (curDiv == 3) {
		d_url = base + "/admin/agentconfig/delgp.do";
	}
	$.ajax({
		url : d_url,
		data : row,
		success : function(result) {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	});
}

function search() {
	$("#datagrid_tb").bootstrapTable('refresh');
}

function add() {
	$("#editmodel").modal('toggle');
	$("#confId").val("");
	$("#groupId").val("");
	$("#gphdId").val("");
	$("#name").val("");
	$("#groupName").val("");
	$("#title").val("");
	$("#type").val("");
	$("#key").val("");
	$("#source").val("");
	$("#initValue").val("");
	$("#expand").val("");
	$("#remark").val("");
	$("#status").val("");
	$("#platform").val("");
	$("#orderNo").val("");
	$("#bOrderNo").val("");
}

function bindPlatfromCombo() {
	$("#platform").change(function() {
		var selval = $(this).children('option:selected').val();
		if (selval > 0) {
			initGroupCombo(selval);
		}
	});
	$("#splatform").change(function() {
		var selval = $(this).children('option:selected').val();
		if (selval > 0) {
			initGroupCombo(selval);
		}
	});
}

function save() {
	var sid = $("#confId").val();
	var sname = $("#name").val();
	var orderNo = $("#bOrderNo").val();
	var platform = $("#platform").val();

	var surl = base + "/admin/agentconfig/save.do";
	if (curDiv == 3) {
		sid = $("#gphdId").val();
		surl = base + "/admin/agentconfig/savegp.do";
		sname = $("#groupName").val();
		orderNo = $("#orderNo").val();
		platform = $("#splatform").val();
	}

	var smitdata = {
		id : sid,
		name : sname,
		title : $("#title").val(),
		type : $("#type").val(),
		key : $("#key").val(),
		source : $("#source").val(),
		initValue : $("#initValue").val(),
		expand : $("#expand").val(),
		status : $("#status").val(),
		remark : $("#remark").val(),
		groupId : $("#groupId").val(),
		platform : platform,
		orderNo : orderNo
	};
	$.ajax({
		url : surl,
		data : smitdata,
		success : function(result) {
			$("#datagrid_tb").bootstrapTable('refresh');
			if (curDiv == 3) {
				initGroupCombo(selval);
			}
		}
	});
}

function saveSet() {
	var ids = [];
	$("#settings").find("input[name='conf_chk']:checked").each(function() {
		ids.push($(this).val());
	});
	$.ajax({
		url : base + "/admin/agentconfig/saveset.do",
		data : {
			stationId : $('#stationId').val(),
			ids : ids.join(",")
		},
		success : function(result) {
			Msg.info("保存成功");
		}
	});
}

var isCheckAll = false;
function saveCheck(){
	if (isCheckAll) {
        $("input[type='checkbox']").each(function(){
            this.checked = false;
        });
        isCheckAll = false;
        $("#saveCheck").text("全选");
    } else {
        $("input[type='checkbox']").each(function(){
            this.checked = true;
        });
        isCheckAll = true;
        $("#saveCheck").text("取消全选");
    }  
}

// 预览配置内容前台显示效果
function detail(conf) {
	var confs = [];
	confs.push(conf);
	var data = {
		"confs" : confs
	};

	handlerData(data);
	initXeditable(data);
}

function initXeditable(data) {
	var where = null;
	var options = {
		emptytext : '无配置',
		params : setParams,
		placement : 'right'
	}
	if (curDiv == 1) {
		where = $("#detail_table");
	} else if (curDiv == 4) {
		options.url = base + "/admin/agentconfig/savestval.do";
		where = $("#settings")
	}
	var html = template('config_tpl', data);
	where.html(html);

	where.find("a").each(function() {

		var type = $(this).attr("data-type");
		var source = $(this).attr("data-source");
		var val = $(this).attr("data-value");

		if (val !== undefined && val !== "") {
			$(this).html(val.replace(/</g,"&lt;"));
		}

		if (type === 'select') {
			options.source = source;
			var arr = evil(source);
			for (var i = 0; i < arr.length; i++) {
				var obj = arr[i];
				if (obj[val] !== undefined) {
					$(this).html(obj[val]);
					break;
				}
			}
		} else if (type === 'date') {
			options.template = 'yyyy-mm-dd';
			options.format = 'yyyy-mm-dd';
			options.viewformat = 'yyyy-mm-dd';
			options.language = 'zh-CN';
			options.datepicker = {
				todayBtn : 'linked',
				weekStart : 1,
				startView : 0,
				language : 'zh-CN',
				minViewMode : 0,
				autoclose : false
			}
		} else if (type === 'datetime') {
			var fmt = 'yyyy-mm-dd hh:ii';
			var timeOpts = {
				language : 'zh-CN',
				todayBtn : 'linked',
				weekStart : 1,
				minViewMode : 0,
				autoclose : false
			};
			if (source) {
				var src = evil("(" + source + ")");
				for ( var key in src) {
					if (key == "format") {
						fmt = src[key];
					} else {
						timeOpts[key] = src[key];
					}
				}
			}
			options.template = fmt;
			options.format = fmt;
			options.viewformat = fmt;
			options.datetimepicker = timeOpts;

		} else if (type === 'combodate') {
			options.template = 'YYYY-MM-DD HH:mm:ss';
			options.format = 'YYYY-MM-DD HH:mm:ss';
			options.viewformat = 'YYYY-MM-DD HH:mm:ss';
			options.datetimepicker = {
				language : 'zh-CN',
				todayBtn : 'linked',
				weekStart : 1
			}
		} else if (type === 'textarea') {
			options.placement = 'bottom';
		} else if (type === 'checklist') {
			$(this).html("");
			options.source = source;
			var arr = evil(source);
			var checks = val.split(",");
			for (var i = 0; i < checks.length; i++) {
				var ck = checks[i];
				for (var j = 0; j < arr.length; j++) {
					var obj = arr[j];
					if (obj[ck] !== undefined) {
						$(this).append(obj[ck]);
						$(this).append("<br>");
						break;
					}
				}
			}
		}
		$(this).editable(options);
	});
}

function handlerData(data) {

	var gps = [];
	var groupMap = {
		0 : {
			id : 0,
			name : "其他",
			sons : []
		}
	};
	for (var i = 0; i < data.confs.length; i++) {
		var c = data.confs[i];
		if (curDiv == 1) {
			c.value = c.initValue;
		}
		if (c.groupId) {
			if (groupMap[c.groupId]) {
				groupMap[c.groupId].sons.push(c);
			} else {
				var gp = {
					id : c.groupId,
					name : c.groupName,
					sons : []
				};
				groupMap[c.groupId] = gp;
				gp.sons.push(c);
				gps.push(gp);
			}

		} else {
			groupMap[0].sons.push(c);
		}
	}
	if (data.confs.length > 0 && (gps.length == 0)) {
		gps.push(groupMap[0]);
	}
	data.gps = gps;
	return data;
}

function setParams(params) {
	if ($(this).attr("data-type") === 'checklist') {
		params.value = params.value.join(",");
	}
	params.configId = $(this).attr("id");
	return params;
}
// 重写 eval
function evil(fn) {
	try {
		var Fn = Function;  //一个变量指向Function，防止有些前端编译工具报错
		return new Fn('return ' + fn)();
	} catch (e) {
		console.log(e)
	}
}