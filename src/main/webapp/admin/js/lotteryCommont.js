$.fn.editable.defaults.mode = 'inline';
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function(fmt) { // author: meizz
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

//格式化成两位小数
function toDecimal(x) {    
    var val = Number(x)   
   if(!isNaN(parseFloat(val))) {    
      val = val.toFixed(2);    
   }    
   return  val;     
} 


//格式化公告类型
function articleType(value){
	switch(value){
	case 1:
		return '关于我们';
		break;
	case 2:
		return '取款帮助';
		break;
	case 3:
		return '存款帮助';
		break;
	case 4:
		return '联盟方案';
		break;
	case 5:
		return '联盟协议';
		break;
	case 6:
		return '联系我们';
		break;
	case 7:
		return '常见问题';
		break;
	case 8:
		return '玩法介绍';
		break;
	case 9:
		return '彩票公告';
		break;
	case 10:
		return '视讯公告';
		break;
	case 11:
		return '体育公告';
		break;
	case 12:
		return '电子公告';
		break;
	case 13:
		return '最新公告';
		break;
	}
}
/**
 * 
 * @param attrName
 *            select的ID属性
 * @param value
 *            option ID值
 */
function beSelect(attrName, value) {
	var obj = document.getElementById(attrName);
	for (var i = 0; i < obj.length; i++) {
		if (obj[i].value == value) {
			obj[i].selected = true;
		}
	}
}

/**
 * 判断是否是打开状态 attrName select ID值 value 参数值 status 状态
 */
function isOpen(attrName, value, status) {
	var obj = $('.' + attrName);
	for (var i = 0; i < obj.length; i++) {
		if (obj[i].id == value) {
			// if(status==2){
			obj[i].checked = true;
			// }
		}
	}
}

function groupViewCZ(value){
	switch(value){
		case 1:
			return "时时彩";
		case 2:
			return "低频彩";
		case 3:
			return "PK10";
		case 5:
			return "11选5";
		case 6:
			return "香港彩";
		case 7 :
			return "快三";
		case 8:
			return "快乐十分";
		case 9:
			return "PC蛋蛋";
		default:
			break;
	}
}


// 格式化彩种类型
function showCZ(value) {
	switch (value) {
	case 2:
	case 9:
	case 52:
		return '时时彩';
	case 3:
	case 8:
	case 53:
	case 158:
		return '北京赛车(快开)';
	case 4:
	case 54:
	case 15:
		return '排列3/福彩3D';
	case 5:
	case 55:
	case 14:
		return '11选5';
	case 6:
		return '香港彩';
	case 66:
		return '系统香港彩';
	case 7:
	case 11:
	case 57:
	case 161:
		return 'PC蛋蛋';
	case 10:
	case 100:
	case 58:
		return '快三';
	case 12:
		return '快乐十分';
	case 1:
	case 51:
		return '系统彩';
	case 929:
		return '(系统)时时彩';
	case 828:
		return '(系统)赛车';
	case 102:
		return '(系统)快三';
	}
}

//开奖状态
function openState(value){
	switch (value) {
	case 1:
		return '<span class="label label-danger">未开奖</span>';
		break;
	case 2:
		return '<span class="label label-primary">未派奖</span>';
		break;
	case 3:
		return '<span class="label label-success">已开奖</span>';
		break;
	case 4:
		return '<span class="label label-warning">未开奖完</span>';
		break;
	case 5:
		return '<span class="label label-info">已经取消</span>';
		break;
	case 6:
		return '<span class="label label-default">已经回滚</span>';
		break;
	}
}
function touZhuState(value){
	switch (value) {
	case 1:
		return '<span class="label label-success">已结束</span>';
		break;
	case 2:
		return '<span class="label label-danger">未开始</span>';
		break;
	case 3:
		return '<span class="label label-primary">投注中</span>';
		break;
	}
}

// 状态
function statusFormatter(value, row, index) {
	if (value === 2) {
		return '<span class="text-success">启用</span><a href="#"><span class="text-danger stateClose">(禁用)</span></a>';
	}
	return '<span class="text-danger">禁用</span><a href="#"><span class="text-success stateOpen">(启用)</span></a>';
}

// 格式化时间
function dateFormatter(value, row, index) {
	return DateUtil.formatDatetime(value);
}

//格式化时间
function dateFormatterDate(value, row, index) {
	return DateUtil.formatDate(value);
}

// 刷新
function refresh() {
	$("#datagrid_tb").bootstrapTable('refresh');
}

//表格刷新并返回第一页
function initAndRefresh(){
	$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
}


/**
 * 提示框
 * param 文本消息
 * index 索引
 */
function tips(param,index){
	switch (index) {
	case 1://普通提示框
		layer.alert(param, {
			  icon: 0,
			  skin: 'layer-ext-moon'
			});
		break;
	case 2://状态开启提示框
		layer.alert(param+' 已被启用!', {
			  icon: 1,
			  skin: 'layer-ext-moon'
			})
		break;
	case 3://状态关闭提示框
		layer.alert(param+' 已被禁用!', {
			icon: 1,
			skin: 'layer-ext-moon'
		})
		break;
	case 4://状态关闭提示框
		layer.alert(param, {
			icon: 2,
			skin: 'layer-ext-moon'
		})
		break;
	default:
		layer.alert('【参数不正确!】', {
			icon: 1,
			skin: 'layer-ext-moon'
		})
		break;
	}
	
}

function icon(param){
	switch (param) {
	case 1://修改
		return '<a class="eidt" href="javascript:void(0)" title="修改"><i class="glyphicon glyphicon-pencil"></i>修改</a>  ';
		break;
	case 2://删除
		return '<a class="del" href="javascript:void(0)" title="删除"><i class="glyphicon glyphicon-remove"></i>删除</a>  ';
		break;
	case 3://保存
		return '<a class="save" href="javascript:void(0)" title="保存"><i class="glyphicon glyphicon-ok"></i>保存</a>  ';
		break;
	default:
		break;
	}
}

//格式化时间
function formatDate(row) {
	var date = new Date(row);
	return date.Format("yyyy-MM-dd hh:mm:ss");
}

/* 
说明：下拉列表选中函数 
参数obj：要选中的下拉列表 
参数selectvalue：标识选中的参数 
*/  
function selectitem(obj , selectvalue){  
    var options = obj.options;  
    for(var i = 0; i < options.length; i++) {  
        if(selectvalue == options[i].value) {  
            options[i].selected = true;  
        }  
    }     
}  