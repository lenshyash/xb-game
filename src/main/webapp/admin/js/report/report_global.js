define(['jquery','bootstrap','Fui','fui_table'],function(){
	return {
		render:function(sport,real,dianZi,lottery,markSix,thirdSports,thirdLottery,chess){
			var $form=$("#report_global_form_id");
			var totalColumns = [],bunkoColumns = [];
			if (sport === 'on') {
				totalColumns.push({
					field : 'sportTotal',
					title : '体育下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sportAward',
					title : '体育派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sportBunko',
					title : '体育输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (real === 'on') {
				totalColumns.push({
					field : 'realTotal',
					title : '真人下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'realAward',
					title : '真人派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'realBunko',
					title : '真人输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (dianZi === 'on') {
				totalColumns.push({
					field : 'dianZiTotal',
					title : '电子下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'dianZiAward',
					title : '电子派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'dianZiBunko',
					title : '电子输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (lottery === 'on') {
				totalColumns.push({
					field : 'lotteryTotal',
					title : '彩票下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'lotteryAward',
					title : '彩票派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'lotteryBunko',
					title : '彩票输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sysLotteryTotal',
					title : '系统彩下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sysLotteryAward',
					title : '系统彩派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sysLotteryBunko',
					title : '系统彩输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sfMarkSixTotal',
					title : '系统六合彩下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sfMarkSixAward',
					title : '系统六合彩派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sfMarkSixBunko',
					title : '系统六合彩输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (markSix === 'on') {
				totalColumns.push({
					field : 'markSixTotal',
					title : '六合下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'markSixAward',
					title : '六合派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'markSixBunko',
					title : '六合输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (thirdSports === 'on') {
				totalColumns.push({
					field : 'thirdSportsTotal',
					title : '三方体育下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'thirdSportsAward',
					title : '三方体育派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'thirdSportsBunko',
					title : '三方体育输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (thirdLottery === 'on') {
				totalColumns.push({
					field : 'thirdLotteryTotal',
					title : '三方彩票下注总记',
					align : 'center',
					valign : 'middle',
					formatter : slotFormatter
				});
				totalColumns.push({
					field : 'thirdLotteryAward',
					title : '三方彩票派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'thirdLotteryBunko',
					title : '三方彩票输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (chess === 'on') {
				totalColumns.push({
					field : 'chessTotal',
					title : '棋牌下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'chessAward',
					title : '棋牌派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'chessBunko',
					title : '棋牌输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			bunkoColumns.push({
				field : 'allBunko',
				title : '全部输赢总计',
				align : 'center',
				valign : 'middle',
				formatter : moneyFormatter
			});
			var td1=td2=td3=td4=null;
		function addTables(data){
			if(td1){
				td1.bootstrapTable('load', data);
				td2.bootstrapTable('load', data);
				td3.bootstrapTable('load', data);
				td4.bootstrapTable('load', data);
				return;
			}
			td1=$("#report_global_datagrid_tb_base").bootstrapTable({
				data:data,
				striped : true,
				columns : [{
					field : 'depositTotal',
					title : '存款总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'withdrawTotal',
					title : '提款总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'rebateAgentTotal',
					title : '返点总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'rebateTotal',
					title : '反水总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'manualDepositTotal',
					title : '手动加款',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'manualWithdrawTotal',
					title : '手动扣款',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'betCountTotal',
					title : '投注总人数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'firstDepositTotal',
					title : '首充总人数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'depositHandlerArtificial',
					title : '手动确认充值金额',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}]
			});
			td2=$("#report_global_datagrid_tb_self").bootstrapTable({data:data,striped : true,columns :totalColumns});
			td3=$("#report_global_datagrid_tb_other").bootstrapTable({data:data,striped : true,columns :bunkoColumns});
			td4=$("#report_global_datagrid_tb_base_ex").bootstrapTable({
				data:data,
				striped : true,
				columns : [{
					field : 'giveTotal',
					title : '存款赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'giveRegisterTotal',
					title : '注册赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'giveLevelUpTotal',
					title : '升级赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				},{
					field : 'activeAwardTotal',
					title : '活动中奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}]
			});
		}
			$form.find(".search-btn").click(function(){
				$form.find(".search-btn").prop("disabled",true).html('<img src="'+baseInfo.baseUrl+'/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
				$.ajax({
					url : baseInfo.baseUrl+"/admin/stationGlobalReport/list.do",
					data : $form.serialize(),
					success : function(result) {
						$form.find(".search-btn").prop("disabled",false).html('查询');
						if(result.msg){
							layer.msg(result.msg);
							return;
						}
						addTables([result]);
					}
				});
				return false;
			}).click();
			$form.find(".refresh-btn").click(function(){
				$form.find(".refresh-btn").prop("disabled",true).html('<img src="'+baseInfo.baseUrl+'/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
				var params = $form.serialize();
				params += "&refresh=1";
				$.ajax({
					url : baseInfo.baseUrl+"/admin/stationGlobalReport/list.do",
					data : params,
					success : function(result) {
						$form.find(".refresh-btn").prop("disabled",false).html('刷新');
						if(result.msg){
							layer.msg(result.msg);
							return;
						}
						addTables([result]);
					}
				});
				return false;
			});
			if(!$form.parents(".fui-box").data("open-tabl1")){
				$form.parents(".fui-box").data("open-tabl1",1);
				$form.parents(".fui-box").on("click",".open-tab",function(){
					var it = $(this)
						,url=it.attr("href")||it.attr("url")
						,option
						,item=null;
					url = unescape(url).replaceTmBySelector($form);
					item=Fui.navbar.getMenuItem(url)
					if(item){
						option = {
							id:it.attr("id")||item.id
							,title:item.title
							,url:url
							,icon:item.icon
							,refresh:true
						};
					}else{
						option = {
							id:it.attr("id")
							,title:it.attr("title")||it.text()
							,url:url
							,icon:it.data("icon")
							,refresh:true
						};
					}
					Fui.tab.tabAdd(option);
					event.preventDefault();
				});
			}
			function moneyFormatter(value, row, index) {
				if (value === undefined) {
					return "<span class='text-primary'>0.00</span>";
				}
				if (value > 0) {
					return [ '<span class="text-danger">', '</span>' ]
							.join(Fui.toDecimal(value,2));
				}
				return [ '<span class="text-primary">', '</span>' ]
						.join(Fui.toDecimal(value,2));
			}
			//存款关联
			function cunkuanFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/payonlinerd/index.do?account={[name=account]:}&begin={[name=begin]:} 00:00:00&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
		
			//取款关联
			function qukuanFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memdrawrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//手动加款
			function sdjkFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&param=sdjk&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
			//手动扣款
			function sdkkFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&param=sdkk&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
			//官方彩票下注
			function cpxzFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=common'>"+moneyFormatter(value)+"</a>";
			}
			//系统彩票下注
			function syscpxzFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=sysNoSF'>"+moneyFormatter(value)+"</a>";
			}
			//十分六合彩下注
			function sflhcpxzFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=SFLHC'>"+moneyFormatter(value)+"</a>";
			}
			//官方彩票下注
			function cpzjFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=common&type=2'>"+moneyFormatter(value)+"</a>";
			}
			//系统彩票下注
			function syscpzjFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=sysNoSF&type=2'>"+moneyFormatter(value)+"</a>";
			}
			//十分六合彩下注
			function sflhcpzjFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&code=SFLHC&type=2'>"+moneyFormatter(value)+"</a>";
			}
			//体育下注关联
			function tyFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/sports/record/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//三方体育下注关联
			function sspFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/rspbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//三方彩票下注关联
			function slotFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/rlotbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//棋牌下注关联
			function chessFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/rchessbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//lhc下注关联
			function lhcFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/gameRecord/lhcIndex.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//真人游戏计录 
			function realFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/rbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//电子
			function electronFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/rebet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59'>"+moneyFormatter(value)+"</a>";
			}
			//手动确认关联
			function handlerDepositFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/payonlinerd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&handlerType=1'>"+moneyFormatter(value)+"</a>";
			}
			
			//充值赠送关联
			function depositGiftFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&moneyType=80&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
			
			//注册赠送关联
			function registerGiftFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&moneyType=79&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
			
			//注册赠送关联
			function levelUpGiftFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&moneyType=78&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
			
			//活动中奖关联
			function activeAwardFormatter(value, row, index){
				return "<a class='open-tab' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:} 23:59:59&moneyType=18&agentName={[name=agent]:}'>"+moneyFormatter(value)+"</a>";
			}
		}
	}
});