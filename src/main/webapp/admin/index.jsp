<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>总控后台</title>
</head>
<body>
	<jsp:include page="/admin/include/bootstrap.jsp"></jsp:include>
	
	<div class="modal show" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="top:80px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title text-center text-primary" id="gridSystemModalLabel" >管理员登录</h1>
      </div>
      <div class="modal-body">
	          <div class="form-group">
	            <input type="text" id="username" class="form-control input-lg" placeholder="登陆账号" required autofocus>
	          </div>
	          <div class="form-group">
	            <input type="password" id="password" class="form-control input-lg" placeholder="密码" required>
	          </div>
	          <div class="form-inline">
	            <input type="text" id="verifyCode" class="form-control input-lg" placeholder="验证码" required>
	            <img id="verifycodeImg" style="cursor: pointer;width: 21%;margin-top: -3px;height: 30%;" alt="验证码" src="${base}/admin/verifycode.do" onclick="refreshVerifyCode(this)">
	          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-lg btn-block" onclick='doLogin();'>登录</button>
      </div>
    </div>
  </div>
</div>
	
	<script language="javascript">
		$(function() {
			$("#verifyCode").keyup(function(event){
			  if(event.keyCode ==13){
				  doLogin();
			  }
			});
			$("#password").keyup(function(event){
			  if(event.keyCode ==13){
				  if(!$("#verifyCode").val()){
					  $("#verifyCode").focus();
					  return;
				  }
				  doLogin();
			  }
			});
		});
		function refreshVerifyCode() {
			var $img = $("#verifycodeImg");
			var time = (new Date()).getTime();
			$img.attr("src","${base}/admin/verifycode.do?time=" + time);
		}
		function doLogin() {
			var account = $("#username").val().trim();
			var pwd = $("#password").val();
			if (account == '') {
				Msg.info("请输入账号");
				$("#username").focus();
				return;
			}
			if (pwd == '') {
				Msg.info("请输入密码");
				$("#password").focus();
				return;
			}
			$ajax({
				url : "${base}/admin/login.do",
				data : {
					account : account,
					password : pwd,
					verifyCode : $("#verifyCode").val()
				},
				success : function(data, textStatus, xhr) {
					var ceipstate = xhr.getResponseHeader("ceipstate")
					if (!ceipstate || ceipstate == 1) {// 正常响应
						window.location.href = "${base}" || "/";
					} else if (ceipstate == 2) {// 后台异常
						Msg.error("后台异常，请联系管理员!");
						refreshVerifyCode();
					} else if (ceipstate == 3) { // 业务异常
						Msg.info(data.msg);
						refreshVerifyCode();
					}
				}
			});
		}
	</script>
</body>
</html>