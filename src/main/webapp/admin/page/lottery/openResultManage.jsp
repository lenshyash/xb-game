<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
		<div class="container">
		<!-- 彩种 -->
		<input id="smallTypeId" type="hidden"/> 
		<div id="toolbar">
		<div id="search" class="form-inline">
				<div class="input-group">
						<input type="text" class="form-control" id="begin" placeholder="开奖时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control" id="keyQiHao" placeholder="期号">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="methodType">
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
			</div>
			<div class="form-inline" style="margin-top: 5px;">
				<div class="input-group">
					<input type="text" id="end" class="form-control" placeholder="开奖时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">上周</button>
				<button class="btn btn-default">本月</button>
				<button class="btn btn-default">上月</button>
				<button class="btn btn-danger" onclick="addData(0);" type="button">添加今天期号</button>
				<button class="btn btn-success" onclick="addData(1);" type="button">添加明天期号</button>
				<button class="btn btn-warning" onclick="addData(2);" type="button">添加后天期号</button>
			</div>
		</div>
		<ul class="nav nav-pills smallTab"></ul>
		<table id="datagrid_tb"></table>
	</div>
	
	<!-- 	修改 -->
	<div class="modal fade" id="editmodel"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">开奖结果修改</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<input id="bcId" class="form-control" type="hidden"/> 
						
							<tr>
								<td width="20%" class="text-right">彩种名称：</td>
								<td width="35%" class="text-left">
								<input id="czName" class="form-control" disabled="disabled" type="text"/> 
								</td>
							</tr>
							
							<tr>
								<td width="20%" class="text-right">期数：</td>
								<td width="35%" class="text-left">
								<input id="qishu" class="form-control" disabled="disabled" type="text"/> 
								</td>
							</tr>
							
							<tr>
								<td width="20%" class="text-right">开奖号码：</td>
								<td width="35%" class="text-left">
								<input id="haoMa" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
							<td width="20%" class="text-center info" colspan="4">
							<span class="glyphicon glyphicon-info-sign"></span>
							温馨提示:开奖号码格式为数字+半角逗号组成(示例:1,2,3,4,5)
							</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>
		<script type="text/javascript">
		$(function() {
			czGroup();
			bindbtn();
		});
		
		//设置传入参数
		function queryParams(params) {
			var stationId = $("#methodType").val();
			if(!stationId){
				stationId = 0;
			}
			params['stationId'] = stationId;
			params['code'] = $("#smallTypeId").val();
			params['qihao'] = $("#keyQiHao").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			return params
		}
		
		function todayData(){
			var begin = DateUtil.getCurrentDate();
			var end = begin;
			$('#begin').val(begin);
			$('#end').val(end);
			refresh();
		}
		
		function getTab() {
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/lotteryData/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ 
				{
					field : 'qiHao',
					title : '期号',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'haoMa',
					title : '开奖结果',
					align : 'center',
					width : '500',
					valign : 'middle'
				}, 
				{
					field : 'openStatus',
					title : '开盘状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : openState
				}, {
					field : 'endTime',
					title : '开奖时间',
					align : 'center',
					width : '400',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'openTime',
					title : '派奖时间',
					align : 'center',
					width : '400',
					valign : 'middle',
					formatter : dateFormatter
				}
				, 
				{
					field : 'tzState',
					title : '投注状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : touZhuState
				}
				,{
					title : '操作',
					align : 'center',
					width : '300',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} 
				]
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function changeHM(value, row, index) {
			if(row.lotCode == "EFC"||row.lotCode == "WFC"||row.lotCode == "HKWFC"||row.lotCode == "AMWFC"||row.lotCode == "FFC"||row.lotCode == "SFLHC"||row.lotCode == "TMLHC"||row.lotCode == "SFSC"|| row.lotCode == "WFK3"|| row.lotCode == "JPK3"|| row.lotCode == "KRK3"|| row.lotCode == "HKK3"|| row.lotCode == "AMK3"||row.lotCode == "FFK3"||row.lotCode == "WFSC"||row.lotCode == "WFFT"||row.lotCode == "LBJSC"||row.lotCode == "SFFT"||row.lotCode == "SF28"
				||row.lotCode == "SFC"||row.lotCode == "ESFC"||row.lotCode == "FF3D"||row.lotCode == "FF11X5"||row.lotCode == "SF11X5"||row.lotCode == "WF11X5"||row.lotCode == "WF3D"||row.lotCode == "SFKLSF"||row.lotCode == "WFKLSF"||row.lotCode == "XWFSC"||row.lotCode == "XSFSC"){
				return '<input class="form-control" type="text" style="width:200px;" value="'+row.haoMa+'" id="'+row.id+'"/>';
			}else{
				return row.haoMa;
			}
		}

		function operateFormatter(value, row, index) {
			var col= '<span class="label label-info lottery_add" id="'+row.id+'" onclick="lottery_delete(this);">删除期号</span>&nbsp;';
			if(row.lotCode != "EFC"&&row.lotCode != "WFC"&&row.lotCode != "HKWFC"&&row.lotCode != "AMWFC"&&row.lotCode != "FFC"&&row.lotCode != "SFLHC"&&row.lotCode != "TMLHC"&&row.lotCode != "SFSC"&&row.lotCode != "FFK3"&&row.lotCode != "WFK3"&&row.lotCode != "JPK3"&&row.lotCode != "KRK3"&&row.lotCode != "HKK3"&&row.lotCode != "AMK3"&&row.lotCode != "WFSC"&&row.lotCode != "WFFT"&&row.lotCode != "LBJSC"&&row.lotCode != "SFFT"&&row.lotCode != "SF28"
				&&row.lotCode != "SFC"&&row.lotCode != "ESFC"&&row.lotCode != "FF3D"&&row.lotCode != "FF11X5"&&row.lotCode != "SF11X5"&&row.lotCode != "WF11X5"&&row.lotCode != "WF3D"&&row.lotCode != "SFKLSF"&&row.lotCode != "WFKLSF"&&row.lotCode != "XSFSC"&&row.lotCode != "XWFSC"){
			switch (row.openStatus) {
				case 1://未开奖
					if(row.tzState==1){
						col+='<span class="label label-success lottery_add addResult" id="'+row.id+'">添加结果</span>&nbsp;';
			 			col+='<span class="label label-info lottery_add" id="'+row.id+'" onclick="lottery_cancel(this);">单期取消</span>&nbsp;';
					}else if(row.tzState == 3 && row.lotCode=="LHC"){
						col+='<span class="label label-success lottery_add addResult" id="'+row.id+'">添加结果</span>&nbsp;';
					}
					return col;
					break;
				case 2://未派奖
					return col;
					break;
				case 6://已经回滚
					if(row.lotCode == "LHC" && row.tzState == 3){
						col+='<span class="label label-info lottery_add" id="'+row.id+'" onclick="lottery_openDisc(this);">开盘</span>&nbsp;';
					}else{
						col+='<span class="label label-success lottery_add addResult" id="'+row.id+'" onclick="lottery_add(this);">添加结果</span>&nbsp;';
			 			col+='<span class="label label-info lottery_add" id="'+row.id+'" onclick="lottery_redo(this);">重新结算</span>&nbsp;';
					}
		 			return col;
		 			break;
				case 3://已开奖
					col+='<span class="label label-warning lottery_add" id="'+row.id+'" onclick="lottery_rollback(this);">派奖回滚</span>&nbsp;';
					col+='<span class="label label-warning lottery_add" id="'+row.id+'" onclick="lottery_repair(this,'+row.id+');">补派奖</span>&nbsp;';
					return col;
					break;
				case 4://未开奖完
					col+='<span class="label label-info lottery_add" id="'+row.id+'" onclick="lottery_redo(this);">重新结算</span>&nbsp;';
					return col;
					break;
				case 5://已经取消
		 			return col;
					break;
				}
			}
		}

		window.operateEvents = {
			'click .addResult' : function(e, value, row, index) {
					$("#editmodel").modal('toggle');
					$("#haoMa").val(row.haoMa);
					$("#bcId").val(row.id);
					$("#qishu").val(row.qiHao);
					$("#czName").val($('#smallTypeId').attr('czName'));
				}

			};
		
		
			//开盘
			function lottery_openDisc(obj){
				Msg.confirm('确定要开盘？', function() {
					$.ajax({
						url : "${base}/admin/lotteryData/reopenLHC.do",
						data :{"id" : obj.id},
						success : function(result) {
							layer.msg('开盘成功!', {
								icon : 1
							});
							refresh();
						}
					});
				});
			}

			//单期取消
			function lottery_cancel(obj) {
				dealData(obj.id, 1);

			}

			//重新结算
			function lottery_redo(obj) {
				dealData(obj.id, 3);
			}

			//派奖回滚
			function lottery_rollback(obj) {
				dealData(obj.id, 4);
			}
			function lottery_repair(obj,id) {
				dealData(obj.id, 10);
			}
			//删除期号
			function lottery_delete(obj) {
				dealData(obj.id, 11);

			}
			function search(){
				refresh();
			}

			//锁定
			function lottery_lock(obj) {
				dealData(obj.id, 5);
			}

			function dealData(bid, type) {
				var doMsg = '';
				switch (type) {
				case 1:
					doMsg = '单期取消';
					break;
				case 3:
					doMsg = '重新结算';
					break;
				case 4:
					doMsg = '派奖回滚';
					break;
				case 5:
					doMsg = '锁定';
					break;
				case 10:
					doMsg='补派奖';
					break;
				case 11:
					doMsg='删除期号';
					break;
				}
				
				var bc = {
					"id" : bid,
					"type" : type,
					"lotCode":$("#smallTypeId").val()
				};

				Msg.confirm('确定要' + doMsg + '？', function() {
					$.ajax({
						url : "${base}/admin/lotteryData/czOperator.do",
						data : bc,
						success : function(result) {
							layer.msg(doMsg + '成功!', {
								icon : 1
							});
							refresh();
						}
					});
				});
			}

			//添加/预设开奖号码
			function editor() {
				var haoMa = $("#haoMa").val();
				var bcId = $("#bcId").val();
				var qishu = $("#qishu").val();
				var czName = $("#czName").val();
				var stationId = $("#methodType").val();
				if (haoMa == null || haoMa == "") {
					layer.alert('开奖号码不能为空!', 2);
					return false;
				}

				var bc = {
					"haoMa" : haoMa,
					"bcId" : bcId,
					"lotCode":$("#smallTypeId").val()
				};
				Msg.confirm('确认操作正确无误？<br/><br/>彩种名称:&nbsp;' + czName
						+ '<br/>期&nbsp;&nbsp;&nbsp;&nbsp;数:&nbsp;' + qishu
						+ '<br/>开奖号码:&nbsp;' + haoMa, function() {

					$.ajax({
						url : "${base}/admin/lotteryData/addResult.do",
						data : bc,
						success : function(result) {
							if(result.success){
								layer.alert('更新成功!', 1);
								refresh();
							}else{
								layer.alert(result.msg, 1);
							}
						}
					});
				});
			}

			//站点列表
			function stationList() {
				var url = '${base}/admin/station/combo.do';
				$.ajax({
					url : url,
					success : function(j) {
						var col = '';
						for ( var i in j) {
							col += '<option value="'+j[i].id+'">'
									+ j[i].name + '</option>';
						}
						$('#methodType').html(col).val(j[0].id);
					}
				});
			}

			//彩种列表
			var czArray = new Array();
			function czGroup() {
				$.ajax({
					url : "${base}/admin/lottery/czGroup.do",
					success : function(i) {
						var col = "",code,name;
						for ( var j in i) {
							code=i[j].code;
							name=i[j].name;
							czArray[j] = name;
							var czIndex = $.inArray(name, czArray);
							if(j == czIndex){
								if (j == 0) {
									col += '<li class="isActive active" id="activeId_'+code+'"><a href="#" class="lotGroupId" id="'+code+'">'
										+ name + '</a></li>';
								
										$('#smallTypeId').val(code).attr("czname",name);
									if(code!='FFC'||code!="EFC"||code!="WFC"||code!="HKWFC"||code!="AMWFC"||code!="SFLHC"||code!="TMLHC"||code!="FFK3"||code!="WFK3"||code!="JPK3"||code!="KRK3"||code!="HKK3"||code!="AMK3"||code!="SFSC"||code!="FFSC"||code!="SFK3"||code!="ESK3" ||code!="WFSC"||code!="WFFT"||code!="LBJSC"||code!="SFFT"||code!="SF28"||code!="TMK3"||code!="WFLHC"||code!="HKMHLHC"
										||code!='SFC'||code!='ESFC'||code!="FF3D"||code!="FF11X5"||code!="SF11X5"||code!="WF11X5"||code!="WF3D"||code!="SFKLSF"||code!="WFKLSF"||code!="AMLHC"||code!="XSFSC"||code!="XWFSC"){
										$("#methodType").hide();
									}
								} else {
									col += '<li class="isActive" id="activeId_'+code+'"><a href="#"  class="lotGroupId" id="'+code+'">'
										+ name + '</a></li>';
								}
						 	}
						}
						$(".smallTab").html(col);
						todayData();
						stationList();
						getTab();
						$('.lotGroupId').click(
								function() {
									debugger;
									var id=this.id;
									$('.isActive')
											.removeClass("active");
									$('#activeId_' + id).addClass(
											"active");
									if(id=='FFC'||id=="EFC"||id=="WFC"||id=="HKWFC"||id=="AMWFC"||id=="SFLHC"||id=="TMLHC"||id=="FFK3"||id=="WFK3"||id=="JPK3"||id=="KRK3"||id=="HKK3"||id=="AMK3"||id=="SFSC" ||id=="FFSC" ||id=="SFK3" ||id=="ESK3" ||id=="WFSC" ||id=="WFFT" ||id=="LBJSC" ||id=="SFFT"||id=="SF28"||id=="TMK3"||id=="WFLHC"||id=="HKMHLHC"
										||id=='SFC'||id=='ESFC'||id=="FF3D"||id=="FF11X5"||id=="SF11X5"||id=="WF11X5"||id=="WF3D"||id=="SFKLSF"||id=="WFKLSF"||id=="XSFSC"||id=="XWFSC"){
										$("#methodType").show();
									}else{
										$("#methodType").hide();
									}
									$('#smallTypeId').val(id);
									$('#smallTypeId').attr("czName",
											this.innerText);
									$("#datagrid_tb").bootstrapTable(
											'refreshOptions', {
												pageNumber : 1
											});
								});
					}
				});
			}
			
			function setDate(begin, end) {
				$('#begin').val(begin);
				$('#end').val(end);
			}
			
			function bindbtn() {
				$(".form-inline .btn-default").click(function() {
					var type = $(this).html();
					var begin = "";
					var end = "";
					if ('今日' === type) {
						begin = DateUtil.getCurrentDate();
						end = begin;
					} else if ('昨日' === type) {
						begin = DateUtil.getLastDate();
						end = begin;
					} else if ('本周' === type) {
						begin = DateUtil.getWeekStartDate();
						end = DateUtil.getCurrentDate();
					} else if ('上周' === type) {
						begin = DateUtil.getLastWeekStartDate();
						end = DateUtil.getLastWeekEndDate();
					} else if ('本月' === type) {
						begin = DateUtil.getMonthDate();
						end = DateUtil.getCurrentDate();
					} else if ('上月' === type) {
						begin = DateUtil.getLastMonthStartDate();
						end = DateUtil.getLastMonthEndDate();
					}
					setDate(begin, end);
					search();
				});
			}
			
			function addData(type){
				var msg="确定要添加";
				switch(type){
				case 0:msg=msg+"今天";break;
				case 1:msg=msg+"明天";break;
				case 2:msg=msg+"后天";break;
				}
				Msg.confirm(msg+"期号吗？", function() {
					$.ajax({
						url : "${base}/admin/lotteryData/generateTodayData.do",
						data :{"type" :type},
						success : function(json) {
							layer.msg(json.msg||'期号添加成功!', {icon : 1});
						}
					});
				});
			}
		</script>
</body>
</html>
<style>
.lottery_cancel,.lottery_add{
	cursor:pointer;
}
</style>