<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
.panel-default{position: relative;}
.panel-heading{position: fixed;left:22px;top:0;width:95.5%;z-index:10}
.panel-body{margin-top:54px;}
</style>
</head>
<body>
	<jsp:include page="/admin/include/bootstrap.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div id="toolbar">
					<div id="search" class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<select class="form-control" id="sbclm_stationId"></select>
							</div>
							<div class="input-group">
								<select class="form-control" id="play_identify" disabled="disabled">
									<option value="1" selected="selected">第一版玩法</option>
									<option value="2">第二版玩法</option>
									<option value="3">第三版玩法(d站奖金模式)</option>
									<option value="4">第四版玩法</option>
									<option value="5">第五版玩法(d站赔率模式)</option>
								</select>
							</div>
						</div>
						<div class="input-group" id="checkNotOrAll">
								<label class="text-info"><input type="checkbox" style="vertical-align: top;">全选</label>
						</div>
						<button class="btn btn-primary" id="toSave" onclick="saveNewLot();">保存</button>
					</div>
				</div>
			</div>
			
			<!-- 彩种列表 -->
			<div class="panel-body" id="menus"></div>
		</div>
	</div>
	<script id="station_tpl" type="text/html">
	{{each data as at}}
			<option value="{{at.id}}" {{if ${stationId} == at.id}}selected="selected"{{/if}}>{{at.name}}</option>
	{{/each}}
	</script>
	<script id="menus_tpl" type="text/html">
	{{each typeList as type}}
		<div class="progress progress-danger progress-striped"><div class="bar" style="width: 100%"></div></div>
		<div class="checkbox firCheckBox">
			<label><strong class="text-info">{{$formatType type}}</strong></label>
			【{{each lotList as lottery}}
				{{if lottery != null && lottery.type == type}}
					<label class="text-info lotChecked"><input type="checkbox" data_type="1" id="{{lottery.id}}_{{lottery.stationId}}_{{lottery.modelStatus}}" name="{{lottery.type}}" {{if lottery.status == 2}}checked="checked"{{/if}}>{{lottery.name}}</label>
				{{/if}}	
			 {{/each}}】
			 {{each lotList as lottery index}}
				{{if lottery != null && lottery.type == type}}
					<div class="checkbox secCheckBox">
						{{each lottery.groupList as group}}
							<label class="groupChecked"><input type="checkbox" data_type="2" id="{{group.id}}_{{group.stationId}}_{{group.modelStatus}}" name="{{lottery.type}}__{{group.code}}" {{if group.status == 2}}checked="checked"{{/if}}>{{group.name}}</label>
							<div class="checkbox trdCheckBox" name="{{lottery.type}}__{{group.code}}">
								{{each group.smallList as play}}
									<label class="playChecked"><input type="checkbox" data_type="3" id="{{play.id}}_{{play.stationId}}_{{play.modelStatus}}_{{group.id}}" name="{{lottery.type}}__{{group.code}}__{{play.code}}" {{if play.status == 2}}checked="checked"{{/if}}>{{play.name}}</label>
								{{/each}}
							</div>
						{{/each}}
					</div>
				{{/if}}
			 {{/each}}
		</div>
	{{/each}}
	</script>
	<script type="text/javascript">
	var parent_index = parent.layer.getFrameIndex(window.name);
	$(function(){
		stationList();
		$("#sbclm_stationId").change(function() {
			 layer.load(3,{shade: [0.3,'#000'],skin:''});
			 getStationVersion();
		});
		
		$("#play_identify").change(function() {
			layer.load(3,{shade: [0.3,'#000'],skin:''});
			initData();
		});
		
		$("#checkNotOrAll").on("click","input[type=checkbox]",function(){
			var t = $(this),resFlag = t.is(":checked"),_checkMenus = $("#menus");
			_checkMenus.find("input[type=checkbox]").prop("checked",resFlag);
		})
	});	
	
		//站点列表
		function stationList() {
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(result) {
					var data = {
							"data" : result
						};
					var html = template('station_tpl', data);
					$('#sbclm_stationId').html(html);
					getStationVersion();
				}
			});
		}
		
		function getStationVersion(){
			var stationId = $("#sbclm_stationId").val();
			$.ajax({
				url:"${base}/admin/lotStationManage/stationVersion.do",
				data:{stationId:stationId},
				success:function(res){
					$("#play_identify").val(res.version);
					initData();
				}
			})
		}
		
		//站点玩法列表
		function initData(){
			layer.load(3,{shade: [0.3,'#000'],skin:''});
			var stationId = $('#sbclm_stationId').val();
			var identify = $('#play_identify').val();
			var url = '${base}/admin/lottery/newBcList.do?stationId='+stationId+'&identify='+identify;
			$.ajax({
				url : url,
				success : function(data) {
					layer.closeAll('loading');
					var html = template('menus_tpl', data);
					$("#menus").html(html);
					
					var lotCheck = $(".firCheckBox");	//彩种
					var groupCheck = $(".secCheckBox")	//大类
					var playCheck = $(".trdCheckBox");		//小类
					lotCheck.on("click",".lotChecked input[type=checkbox]",function(){
						//需要全部选中
						var it = $(this),name = it.attr("name"),is_check;
						var len = it.parent().parent().find(".lotChecked input:checked").length;
						is_check=len>0?true:false;
						it.parent().parent().find('.secCheckBox input').each(function(index,item){
							item = $(item);
							item.prop("checked",is_check);
						})
					});
					groupCheck.on("click",".groupChecked input[type=checkbox]",function(){
						//需要选中相应的小类
						var it=$(this), name = it.attr("name");
						var is_check = it.is(":checked");
						it.parent().parent().find('div[name='+name+'] input').each(function(index,item){
							item = $(item);
							item.prop("checked",is_check);
						})
					});
					playCheck.on("click",".playChecked input[type=checkbox]",function(){
						var it = $(this),is_check,val=it.parent().parent().attr("name");
						var len = it.parent().parent().find("input:checked").length;
						is_check=len>0?true:false;
						groupCheck.find('.groupChecked input[name='+val+']').prop("checked",is_check);
					});
				}
			});
		}
		
		function saveNewLot(){
			//保存彩种
			layer.load(3,{shade: [0.3,'#000'],skin:''});
			var menus = $("#menus"),lotIds='',groupIds='',playIds='',dataType,ids=0,is_check;
			menus.find("input").each(function(index,item){
				item = $(item);
				dataType = parseInt(item.attr("data_type"));
				ids = item.attr("id");
				is_check = item.is(":checked");
				var ids_split = ids.split("_");	//该字段所属内容为id_stationId_modelStatus
				//1.如果stationId = 0 的话属于模版内容，如果未选中则直接过滤。如果选中则后台进行新增
				//2.如果stationId > 0 则属于相应的站点内容，必会选中，假设该当前取消选中且stationId>0.则后台进行修改状态/或者删除
				//3.如果还是选中且stationId>0，则属于未变动，无需任何操作
				if(ids_split[1]==0 && !is_check){	
					return true;	//未选中不处理
				}
				//如果stationId>0则就是属于站点数据，如果modelStatus == 1 则说明未显示
				if(ids_split[1]>0){
					//情况说明--------------------
					//1。modelStatus == 2,is_check未选中，则说明该数据被取消,反之is_check选中则属于不变动
					//2。modelStatus == 1,is_check选中，则说明该数据被选中，反之is_check未选中则属于不变动
					if(ids_split[2] == 2 && is_check){return true;}
					if(ids_split[2] == 1 && !is_check){return true;}
				}
				//is_check = !is_check?'_1,':'_2,';
				switch(dataType){
					case 1:
						lotIds += ids + ",";
						break;
					case 2:
						groupIds += ids + ",";
						break;
					case 3:
						playIds += ids + ",";
						break;
				}
			})
			lotIds = lotIds.substring(0,lotIds.length-1);
			groupIds = groupIds.substring(0,groupIds.length-1);
			playIds = playIds.substring(0,playIds.length-1);
			var stationId = $('#sbclm_stationId').val(),identify=$('#play_identify').val(),data={
				"lotIds" :lotIds,
				"groupIds" :groupIds,
				"playIds" :playIds,
				"identify" :identify,
				"stationId":stationId
			};
			$.ajax({
				url:"${base}/admin/lottery/saveNewStationLottery.do",
				data:data,
				success:function(result){
					layer.closeAll('loading');
					if(result.success){
						layer.alert("保存成功",{icon:1},function(index){
							//initData();	关闭ifram层，无需在刷新数据
							layer.close(index);
							parent.layer.close(parent_index);
						});
					}
				}
			})
		}
		
		
		template.helper("$formatType",function(type){
			return showCZ(type);
		})
	</script>
	<style>
	.secCheckBox {
	margin-left: 50px;
	}
	.trdCheckBox {
	margin-left: 80px;
	}
	.checkbox, .radio{
	margin-bottom:15px;
	}
	label{
		margin-top: 3px;
		margin-left: 5px;
		margin-right: 5px;
	}
	.progress{
		background-color : gray;
	}
	.noShow{
		display: none;
	}
	</style>
</body>
</html>