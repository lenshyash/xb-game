<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
	<input id="smallTypeId" type="hidden"/>
	<input id="bigTypeId" type="hidden"/>
	<input id="lotType" type="hidden"/>
		<div class="form-group">
			<div id="search" class="form-inline">
				<div class="input-group">
					<button class="btn btn-primary" onclick="add();">重新生成赔率</button>
				</div>
				<div class="input-group">
					<select class="form-control" id="lotteryVersion" disabled="disabled">
						<option value="1">第一版玩法</option>
						<option value="2">第二版玩法</option>
						<option value="3">第三版玩法(d站奖金模式)</option>
						<option value="4">第四版玩法</option>
						<option value="5">第五版玩法(d站赔率模式)</option>
					</select>
				</div>
				<div class="input-group">
					<select class="form-control" id="methodType"></select>
				</div>
				<span class="label label-success">温馨提示：彩票第二版赔率须在此配置，彩票第一版和第三版请移至 [ 站点赔率设置管理 ] 菜单</span>
			</div>
		</div>
		<ul class="nav nav-tabs" id="lotteryType">
			
		</ul>
		<div id="toolbar" style="max-width:1000px;width:1000px;">
			<div id="search" class="form-inline">
				<label class="sr-only" for="methodType">查询类型</label> <input
					type="text" class="hide" id="lottype">
				<div class="form-group hide">
					<button class="btn btn-danger">大类</button>
					<div class="input-group">
						<ul class="nav nav-pills smallTab" style="width:900px;"></ul>
					</div>
				</div>
				<div class="form-group hide">
					<button class="btn btn-danger">小类</button>
					<div class="input-group">
						<ul class="nav nav-pills playTab" style="width:900px;"></ul>
					</div>
				</div>
				<div class="form-group hide">
					<button class="btn btn-danger">房间号</button>
					<div class="input-group">
						<ul class="nav nav-pills lotRoom" style="width:900px;"></ul>
					</div>
				</div>
				<button class="btn btn-primary hide" onclick="search();">查询</button>
			</div>
		</div>
		<div class="modal fade" id="newModel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">赔率生成选择</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="30%" class="text-right">彩种玩法：</td>
								<td width="70%" class="text-left"><select id="lotWF" class="form-control"></select></td>
							</tr>
							<tr>
								<td width="30%" class="text-right">彩种大类：</td>
								<td width="70%" class="text-left"><select id="lotBigWF" class="form-control"></select></td>
							</tr>
							<tr>
								<td width="30%" class="text-right">彩种小类：</td>
								<td width="70%" class="text-left"><select id="lotSmallWF" class="form-control"></select></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="save();">保存</button>

				</div>
			</div>
		</div>
	</div>
		<table id="datagrid_tb"></table>
	</div>
	<script id="station_tpl" type="text/html">
	{{each data as at}}
			<option value="{{at.id}}">{{at.name}}</option>
	{{/each}}
	</script>
	<script type="text/html" id="view_group_tpl">
	{{each data as at index}}
		{{if at != null}}
			<li {{if index == 0}}class="active"{{/if}}><a href="#" data-toggle="tab" class="lotName" id="{{at}}">{{$formatTypeName at}}</a></li>
		{{/if}}
	{{/each}}
	</script>
	<script type="text/html" id="group_list_tpl">
	{{if data.total > 0}}
		{{each data.rows as res index}}
				<li class="isActive {{if index == 0}}active{{/if}}"><a href="#" class="{{if data.type == 1}}lotGroupId{{else if data.type == 2}}lotPlayCode{{else}}lotRoomClick{{/if}}" lottype="{{res.lotType}}" id="{{if data.type ==1 || data.type == 3}}{{res.id}}{{else}}{{res.code}}{{/if}}">{{res.name}}</a></li>
		{{/each}}
	{{/if}}
	</script>
	<script type="text/javascript">
		$(function() {
			stationList();
			$("#methodType").change(function() {
				getStationVersion();
			});
		})
		
		//设置传入参数
		function queryParams(params) {
			params['playCode'] = $(".playTab li.active a").attr("id");
			params['stationId'] = $('#methodType').val();
			params['lotType'] = $("#lotteryType li.active a").attr("id");
			if($("#lotteryVersion").val() == 4){
				params['roomId'] = $(".lotRoom li.active a").attr("id");
			}
			return params
		}
		
		
		function add(){
			var data = {
					"stationId" :$("#methodType").val(),
					"lotType" : $("#lotteryType li.active>a").attr("id"),
					"playCode" : $(".playTab li.active>a").attr("id")
			}
			if($("#lotteryVersion").val() == 4){
				data['roomId'] = $(".lotRoom li.active a").attr("id");
			}
			var staText = $('#methodType').find("option:selected").text();
			Msg.confirm('确定重新生成站点【'+staText+'】的赔率?',function(){
				var url = '${base}/admin/lotteryPeiLv/savePeiLv.do';
				$.ajax({
					url : url,
					data:data,
					success : function(result) {
						Msg.info('赔率遗漏新增成功');
						refresh();
					}
				});
			});
			
		}
		
		function getLotterGroup(version){
			$.ajax({
				url : "${base}/admin/lottery/list.do",
				data : {
					"identify":version,
					"stationId":$("#methodType").val(),
					"rows":100,
					"page":1
					},
				success : function(result) {
					var lType = new Array();
					for(var i=0;i<result.rows.length;i++){
						if(lType.indexOf(result.rows[i].type) == -1){
							lType[i]=result.rows[i].type;
						}
					}
					var data = {
							"data":lType
					}
					var html = template('view_group_tpl',data);
					$("#lotteryType").html(html);
					groupTypeList();
					getRoom();
					$(".lotName").click(function(){
						$(this).parent().addClass("active").siblings().removeClass("active");
						groupTypeList();
						getRoom();
					})
				}
			});
		}
		
		function groupTypeList(){
			var typeId = $("#lotteryType li.active a").attr("id");
			var stationId = $("#methodType").val();
			var data = {
					"lotType":typeId,
					"stationId":stationId
			}
			$.ajax({
				url:"${base}/admin/lotTypeGroup/list.do",
				data:data,
				success:function(result){
					if(result.total>0){
						result.type = 1;
						var data = {
								"data":result
						}
						var html = template('group_list_tpl',data);
						$(".smallTab").html(html).parent().parent().removeClass("hide");
						$(".lotGroupId").click(function(){
							$(this).parent().addClass("active").siblings().removeClass("active");
							playTypeList();
							refresh();
						})
					}else{
						$(".smallTab").parent().parent().addClass("hide");
					}
					playTypeList();
				}
			})
		}
		
		function playTypeList(){
			var groupId = $(".smallTab li.active a").attr("id");
			var stationId = $("#methodType").val();
			var data = {
					"groupId":groupId,
					"stationId":stationId
			}
			$.ajax({
				url:'${base}/admin/lotType/list.do',
				data:data,
				success:function(result){
					if(result.total>0){
						result.type = 2;
						var data = {
								"data":result
						}
						var html = template('group_list_tpl',data);
						$(".playTab").html(html).parent().parent().removeClass("hide");
						$(".lotPlayCode").click(function(){
							$(this).parent().addClass("active").siblings().removeClass("active");
							refresh();
						})
					}else{
						$(".playTab").parent().parent().addClass("hide");
					}
					getTab();
					refresh();
				}
			})
		}
		
		//站点列表
		function stationList() {
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(result) {
					var data = {
							"data" : result
						};
					var html = template('station_tpl', data);
					$('#methodType').html(html);
					getStationVersion();
				}
			});
		}
		
		function getStationVersion(){
			var stationId = $("#methodType").val();
			$.ajax({
				url:"${base}/admin/lotStationManage/stationVersion.do",
				data:{stationId:stationId},
				success:function(res){
					$("#lotteryVersion").val(res.version);
					getLotterGroup(res.version);
				}
			})
		}
		
		function getRoom(){
			//第四版加载房间号
			var version = $("#lotteryVersion").val();
			if(version == 4){
				$(".lotRoom").parent().parent().removeClass("hide");
				getLotteryRoom();
			}else{
				$(".lotRoom").parent().parent().addClass("hide");
			}
		}
		
		
		function getLotteryRoom(){
			var typeId = $("#lotteryType li.active a").attr("id");
			var stationId = $("#methodType").val();
			var data = {
					lotType:typeId,
					stationId:stationId,
			}
			$.ajax({
				url:'${base}/admin/lotteryPeiLv/get_room.do',
				data:data,
				success:function(result){
					if(result.length>0){
						result.type = 3;
						result.total = result.length;
						result.rows = result;
						var data = {
								"data":result
						}
						var html = template('group_list_tpl',data);
						$(".lotRoom").html(html).parent().parent().removeClass("hide");
						$(".lotRoomClick").click(function(){
							$(this).parent().addClass("active").siblings().removeClass("active");
							refresh();
						})
					}else{
						$(".lotRoom").parent().parent().addClass("hide");
					}
				}
			})
		}
		
		template.helper("$formatTypeName",function(type){
			return showCZ(type);
		})

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/markSix/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '号码',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, 
				{
					field : 'odds',
					title : '赔率',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : oddsInputFormatter
				}, 
				{
					field : 'minBetAmmount',
					title : '最小下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : minBetInputFormatter
				}, {
					field : 'maxBetAmmount',
					title : '最大下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : maxBetInputFormatter
				}, {
					field : 'sortNo',
					title : '序号',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : sortNoInputFormatter
				},{
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				},{
					title : '操作',
					align : 'center',
					width : '150',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function oddsInputFormatter(value, row, index) {
			return [ '<input id="odds_'+row.id+'" name="odds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function minBetInputFormatter(value, row, index) {
			return [ '<input id="minBet_'+row.id+'" name="minBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}
		
		function maxBetInputFormatter(value,row,index){
			return [ '<input id="maxBet_'+row.id+'" name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
			.join('');
		}

		function sortNoInputFormatter(value, row, index) {
			return [ '<input id="sortNo_'+row.id+'" name="sortNo" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function operateFormatter(value, row, index) {
			return [icon(3)].join('');
		}

		window.operateEvents = {
			'click .save' : function(e, value, row, index) {
				save(row);
			},'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			}
		};
		
		function open(row){
			var bc = {
	 				"status" : 2,
					"id" :row.id,
					"stationId":$("#methodType").val(),
	        	};
			$.ajax({
				url : "${base}/admin/markSix/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function close(row) {
			var bc = {
	 				"status" : 1,
					"id" :row.id,
					"stationId":$("#methodType").val(),
	        	};
			$.ajax({
				url : "${base}/admin/markSix/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function save(row) {
			var bc = {
				'id' : row.id,
				'odds' : $("#odds_"+row.id).val(),
				'minBetAmmount' : $("#minBet_"+row.id).val(),
				'maxBetAmmount' : $("#maxBet_"+row.id).val(),
				'sortNo' : $("#sortNo_"+row.id).val()
			};
			$.ajax({
				url : "${base}/admin/markSix/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}
	</script>
	<style>
	.fixed-table-toolbar 
	.bars, 
	.fixed-table-toolbar 
	.columns,
	.fixed-table-toolbar 
	.search {
		position:relative;
		margin-top:10px;
		margin-bottom:10px;
	}
</style>
</body>
</html>
