<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
		<div class="container">
		<ul class="nav nav-tabs" id="czGroup"></ul>
			<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="minCount">
							<option value="1">1分钟</option>
							<option value="2">2分钟</option>
							<option value="3">3分钟</option>
							<option value="4">4分钟</option>
							<option value="5">5分钟</option>
							<option value="6">6分钟</option>
							<option value="7">7分钟</option>
							<option value="8">8分钟</option>
							<option value="9">9分钟</option>
							<option value="10">10分钟</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onClick="deal(1);">批量加</button>
				<button class="btn btn-primary" onClick="deal(2);">批量减</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<input type="hidden" id="lotType">
	<script type="text/javascript">
		$(function() {
			czGroup();
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/lotteryTime/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'lotCode',
					title : '彩票编码',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, {
					field : 'actionNo',
					title : '期号',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'actionTime',
					title : '开奖时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}
				, {
					title : '操作',
					align : 'center',
					width : '50',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} 
				]
			});
		}
		
		function operateFormatter(value, row, index) {
			return [ '<a class="edit" href="javascript:void(0)" id="'+row.id+'" title="修改">',
						'<i class="glyphicon glyphicon-pencil"></i>修改','</a>  '].join('');
		}
		
		window.operateEvents = {
				'click .edit' : function(e, value, row, index) {
					var timeVal = $('#date_'+this.id).val();
					var openId =this.id;
					openTime(timeVal,openId);
				}
			};
		
		
		function deal(param){
			var code = 1;
			var msg = '加上';
			if(param == 1){
			}else if(param == 2){
				code = -1;
				msg = '减去';
			}
			excutTime(code,msg);
		}
		
		function excutTime(time,msg){
			var num = $('#minCount').val();
			var code = $('#lotType').val();
			var bc = {
					"time" :time*num,
					"code" :code 
	        	};
			layer.confirm('确定要把全部开奖时间'+msg+'【'+num+'分钟】？', {
				  btn: ['确定','取消']
				}, function(){
	 			
					$.ajax({
						url:"${base}/admin/lotteryTime/batchTime.do",
						data : bc,
						DataType:"json",
						success:function(j){
							layer.msg('设置成功!', {icon: 1});
		 					refresh();
						}
							
					});
					
				}, function(){});
		}
		
		var czArray = new Array();
		function czGroup(){
			$.ajax({
				url:"${base}/admin/lottery/czGroup.do",
				success:function(j){
					var col = '';
					for(var i in j){
						czArray[i] = j[i].name;
						var czIndex = $.inArray(j[i].name, czArray);
						if(i == czIndex){
							if(i*1 == 0){
			 					col+='<li class="active"><a href="#" data-toggle="tab" class="lotName" id="'+j[i].code+'">'+j[i].name+'</a></li>';
								$('#lotType').val(j[i].code);
							}else{
								col+='<li><a href="#" data-toggle="tab" class="lotName" id="'+j[i].code+'">'+j[i].name+'</a></li>';
							}
						}
					}
					$('#czGroup').html(col);
					$('.lotName').click(function(){
						$('#lotType').val(this.id);
		 				initAndRefresh();
					});
					getTab();
				}
			});
		}
		
		function openTime(timeVal,openId){
			var bc = {
					"timeVal" :timeVal,
					"openId" :openId 
	        	};
			if(timeVal!=""&&openId!=""){
			layer.confirm('确定要把开奖时间设置成【'+timeVal+'】？', {
				  btn: ['确定','取消']
				}, function(){
	 				$.ajax({
					url : "${base}/admin/lotteryTime/updateTime.do",
					data : bc,
					success : function(result) {
						layer.msg('更新成功!', {icon: 1});
	 					refresh();
					}
				});
				}, function(){});
			
			}
		}
		
		function statusFormatter(value, row, index) {

			var sn = GlobalTypeUtil.getTypeName(2, 1, value);
			if (value === 2) {
				return [ '<span class="text-success">', '</span>' ].join(sn);
			}
			return [ '<span class="text-danger">', '</span>' ].join(sn);
		}
		
		function dateFormatter(value, row, index) {
			var date = new Date(value);
// 			var col = '<a href="#" class="openTime showTime" data-type="combodate" data-template="HH:mm:ss" data-format="HH:mm:ss" data-viewformat="HH:mm:ss" data-pk="1" data-title="开奖时间设置" class="editable editable-click editable-empty editable-open" data-original-title="开奖时间设置" title="开奖时间设置" id="'+row.id+'">'+date.Format("hh:mm:ss")+'</a>';
			var col = '<input type="text" class="form-control" id="date_'+row.id+'" value="'+date.Format("hh:mm:ss")+'">';
			return col;
		}
		
		//设置传入参数
		function queryParams(params) {
			params['lotType'] = $('#lotType').val();
			return params;
		}
		
		function search() {
			refresh();
		}
	</script>
</body>
</html>