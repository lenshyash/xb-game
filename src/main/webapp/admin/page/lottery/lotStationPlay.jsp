<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
		<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="station_id">查询类型</label>
			</div>
		</div>
		<div class="form-group"> 
			<div id="search" class="form-inline">
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="lotteryVersion" disabled="disabled">
							<option value="1">第一版玩法</option>
							<option value="2">第二版玩法</option>
							<option value="3">第三版玩法(d站奖金模式)</option>
							<option value="4">第四版玩法</option>
							<option value="5">第五版玩法(d站赔率模式)</option>
						</select>
					</div>
					<div class="input-group"><select class="form-control" id="station_id">
					</select></div>
				</div>
			</div>
		</div>
		<ul class="nav nav-tabs czGroup" id="lotteryType">
    	</ul>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">玩法大类修改</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">玩法分组名称：</td>
								<td width="25%" class="text-left">
								<input id="fzName" class="form-control" type="text"/> 
								</td>
								<td width="25%" class="text-right">序号：</td>
								<td width="25%" class="text-left">
								<input id="fzSortNo" class="form-control" type="text"/> 
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" 
						onclick="editor();">保存</button>
						
				</div>
			</div>
		</div>
	</div>
	<script id="station_tpl" type="text/html">
	{{each data as at}}
			<option value="{{at.id}}">{{at.name}}</option>
	{{/each}}
	</script>
	<script type="text/html" id="view_group_tpl">
	{{each data as at index}}
		{{if at != null}}
			<li {{if index == 0}}class="active"{{/if}}><a href="#" data-toggle="tab" class="lotName" id="{{at}}">{{$formatTypeName at}}</a></li>
		{{/if}}
	{{/each}}
	</script>
	<script type="text/javascript">
	function getTab() {
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/lotTypeGroup/listByStationIdAndType.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			columns : [ {
				field : 'lotType',
				title : '彩票类型',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : showCZ
			}, {
				field : 'name',
				title : '玩法分组名称',
				align : 'center',
				width : '180',
				valign : 'bottom'
			}, {
				field : 'code',
				title : '彩票编码',
				align : 'center',
				width : '180',
				valign : 'bottom'
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle'
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '50',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ]
		});
	}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
					'<a class="del" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {

				$("#editmodel").modal('toggle');
				$("#fzName").val(row.name);
				$("#fzSortNo").val(row.sortNo);
				$("#playId").val(row.id);
			},
			'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			},
			'click .del' : function(e, value, row, index) {
				del(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#station_id").val();
			params['type'] = $("#lotteryType li.active a").attr("id");
			return params
		}
		
		$(function() {
			stationList();
			$("#station_id").change(function() {
				var curVal=$(this).find("option:checked").attr("value");
				getStationVersion();
			});
			
		})
		
		function getLotterGroup(){
			var identify = $('#lotteryVersion').val();
			$.ajax({
				url : "${base}/admin/lottery/list.do",
				data : {
					"identify":identify,
					"stationId":$("#station_id").val(),
					"rows":100,
					"page":1
					},
				success : function(result) {
					var lType = new Array();
					for(var i=0;i<result.rows.length;i++){
						if(lType.indexOf(result.rows[i].type) == -1){
							lType[i]=result.rows[i].type;
						}
					}
					var data = {
							"data":lType
					}
					var html = template('view_group_tpl',data);
					$("#lotteryType").html(html);
					getTab();
					refresh();
					$('.lotName').click(function() {
						$(this).parent().addClass("active").siblings().removeClass("active");
						refresh();
					});
				}
			});
		}
		
		template.helper("$formatTypeName",function(type){
			return showCZ(type);
		})
		
		//点击新增站点彩种
		function addMode(){
			var stationId = $('#station_id').val();//站点ID
			czStationList(stationId);//初始化
			$("#newModel").modal('toggle');
			
		}
		
		//站点列表
		function stationList() {
			var stationId = $('#station_id').val();
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(result) {
					var data = {
							"data" : result
						};
					var html = template('station_tpl', data);
					$('#station_id').html(html);
					//查询版本号
					getStationVersion();
				}
			});
		}
		
		function getStationVersion(){
			var stationId = $("#station_id").val();
			$.ajax({
				url:"${base}/admin/lotStationManage/stationVersion.do",
				data:{stationId:stationId},
				success:function(res){
					$("#lotteryVersion").val(res.version);
					getLotterGroup();
				}
			})
		}
		
		function search() {
			refresh();
		}
		
		function open(row) {
			var bc = {
	 				"status" : 2,
	 				"stationId":$('#station_id').val(),
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotTypeGroup/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert(showCZ(row.lotType)+'【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		function close(row) {
			var bc = {
	 				"status" : 1,
	 				"stationId":$('#station_id').val(),
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotTypeGroup/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert(showCZ(row.lotType)+'【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		function del(row) {
			layer.confirm('确定要把【'+row.name+'】删除掉？', {
				  btn: ['确定','取消']
				}, function(){
		 			$.ajax({
	 				url : "${base}/admin/lotTypeGroup/del.do",
	 				data : row,
	 				success : function(result) {
	 					layer.msg('删除成功!', {icon: 1});
	 					refresh();
	 				}
	 			});
				 
				}, function(){});
				
		}
		
		function editor(){
			var bc = {
	 				"name" : $('#fzName').val(),
					"id" :$('#playId').val(),
					"sortNo" :$('#fzSortNo').val() 
	        	};
				$.ajax({
					url : "${base}/admin/lotTypeGroup/update.do",
					data : bc,
					success : function(result) {
						layer.alert('修改成功!', {
							  icon: 1,
							  skin: 'layer-ext-moon'
							})
						refresh();
					}
				});
		}
	</script>
</body>
</html>
