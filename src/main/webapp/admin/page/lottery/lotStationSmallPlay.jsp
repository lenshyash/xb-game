<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
	<input id="smallTypeId" type="hidden"/>
		<div class="form-group"> 
			<div id="search" class="form-inline">
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="lotteryVersion" disabled="disabled">
							<option value="1">第一版玩法</option>
							<option value="2">第二版玩法</option>
							<option value="3">第三版玩法(d站奖金模式)</option>
							<option value="4">第四版玩法</option>
							<option value="5">第五版玩法(d站赔率模式)</option>
						</select>
					</div>
					<div class="input-group"><select class="form-control" id="methodType"></select></div>
					<span class="label label-success">温馨提示：彩票第一版和第三版奖金需在此设置，第二版请移至 [ 站点赔率V2设置管理 ] 菜单</span>
				</div>
			</div>
		</div>
		<ul class="nav nav-tabs czGroup" id="lotteryType">

		</ul>
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="methodType">查询类型</label>
				<div class="form-group">
					<div class="input-group hide">
						<select class="form-control" id="methodType2">
							<option value="0">状态</option>
							<option value="2">启用</option>
							<option value="1">停用</option>
						</select>
					</div>
					<div class="input-group">
						<ul class="nav nav-pills smallTab" style="width:1000px;"></ul>
					</div>
					<div class="input-group hide">
						<input type="text" class="form-control" id="keyword"
							placeholder="关键字">
					</div>
				</div>
				<button class="btn btn-primary hide" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<!-- 修改 -->
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">玩法小类修改</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">玩法介绍：</td>
								<td width="75%" class="text-left"><input id="playMethod"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">详细介绍：</td>
								<td width="75%" class="text-left"><input id="detailDesc"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">中奖范例：</td>
								<td width="75%" class="text-left"><input id="winExample"
									class="form-control" type="text" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script id="station_tpl" type="text/html">
	{{each data as at}}
			<option value="{{at.id}}">{{at.name}}</option>
	{{/each}}
	</script>
	<script type="text/html" id="view_group_tpl">
	{{each data as at index}}
		{{if at != null}}
			<li {{if index == 0}}class="active"{{/if}}><a href="#" data-toggle="tab" class="lotName" id="{{at}}">{{$formatTypeName at}}</a></li>
		{{/if}}
	{{/each}}
	</script>
	<script type="text/html" id="group_list_tpl">
	{{if data.total > 0}}
		{{each data.rows as res index}}
				<li class="isActive {{if index == 0}}active{{/if}}"><a href="#" class="lotGroupId" id="{{res.id}}">{{res.name}}</a></li>
		{{/each}}
	{{/if}}
	</script>
	<script type="text/javascript">
		$(function() {
			version();
			stationList();
			$("#methodType").change(function() {
				var curVal=$(this).find("option:checked").attr("value");
				getStationVersion();
				});
		})
		
		//设置传入参数
		function queryParams(params) {
			params['groupId'] = $(".smallTab li.active a").attr("id");
			params['stationId'] = $("#methodType").val();
			return params
		}
		
		function getLotterGroup(){
			var identify = $('#lotteryVersion').val();
			$.ajax({
				url : "${base}/admin/lottery/list.do",
				data : {
					"identify":identify,
					"stationId":$("#methodType").val(),
					"rows":100,
					"page":1
					},
				success : function(result) {
					var lType = new Array();
					for(var i=0;i<result.rows.length;i++){
						if(lType.indexOf(result.rows[i].type) == -1){
							lType[i]=result.rows[i].type;
						}
					}
					var data = {
							"data":lType
					}
					var html = template('view_group_tpl',data);
					$("#lotteryType").html(html);
					getLotTypeGroup();
					$('.lotName').click(function() {
						$(this).parent().addClass("active").siblings().removeClass("active");
						getLotTypeGroup();
					});
				}
			});
		}
		
		template.helper("$formatTypeName",function(type){
			return showCZ(type);
		})

		function getLotTypeGroup() {
			var stationId = $("#methodType").val();
			var type = $("#lotteryType li.active a").attr("id");
			$.ajax({
				url : "${base}/admin/lotTypeGroup/list.do",
				data : {
					lotType : type,
					stationId : stationId
				},
				type : "get",
				dataType : 'json',
				success : function(result) {
					var data = {
							"data":result
					}
					var html = template('group_list_tpl',data);
					$(".smallTab").html(html);
					getTab();
					$(".lotGroupId").click(function(){
						$(this).parent().addClass("active").siblings().removeClass("active");
						refresh();
					})
				}
			});
		}
		
		//站点列表
		function stationList() {
			var stationId = $('#methodType').val();
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(result) {
					var data = {
							"data" : result
						};
					var html = template('station_tpl', data);
					$('#methodType').html(html);
					getStationVersion();
				}
			});
		}
		
		function getStationVersion(){
			var stationId = $("#methodType").val();
			$.ajax({
				url:"${base}/admin/lotStationManage/stationVersion.do",
				data:{stationId:stationId,type:1},
				success:function(res){
					$("#lotteryVersion").val(res.version);
					getLotterGroup();
				}
			})
		}
		
		var version1= [];
		var version3= [];
		function version(){
			version1 = [ {
				field : 'name',
				title : '玩法小类名称',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : playNameFormatter
			}, 
// 				{
// 					field : 'maxBonusOdds',
// 					title : '最大中奖金额(元)',
// 					align : 'center',
// 					width : '200',
// 					valign : 'bottom',
// 					formatter : maxMInputFormatter
// 				}, 
			{
				field : 'minBonusOdds',
				title : '最小中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : minMInputFormatter
			}, 
//				{
//					field : 'maxRakeback',
//					title : '最高返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : maxFInputFormatter
//				}, {
//					field : 'minRakeback',
//					title : '最低返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : minFInputFormatter
//				}, 
			{
				field : 'maxNumber',
				title : '最高注数',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : maxNInputFormatter
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : sequenceInputFormatter
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '150',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ];
			
			version3 = [ {
				field : 'name',
				title : '玩法小类名称',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : playNameFormatter
			}, {
				field : 'maxBonusOdds',
				title : '最大中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'bottom',
				formatter : maxMInputFormatter
			}, {
				field : 'minBonusOdds',
				title : '最小中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : minMInputFormatter
			}, 
//				{
//					field : 'maxRakeback',
//					title : '最高返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : maxFInputFormatter
//				}, {
//					field : 'minRakeback',
//					title : '最低返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : minFInputFormatter
//				}, 
			{
				field : 'maxNumber',
				title : '最高注数',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : maxNInputFormatter
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : sequenceInputFormatter
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '150',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ];
		}

		function getTab() {
			if($("#lotteryVersion").val()==1||$("#lotteryVersion").val()==2||$("#lotteryVersion").val()==4){
				table.bootstrapTable('refreshOptions', {
					columns : version1,
					url : '${base}/admin/lotType/list.do',
					pageNumber : 1
				});
			}else{
				table.bootstrapTable('refreshOptions', {
					columns : version3,
					url : '${base}/admin/lotType/list.do',
					pageNumber : 1
				});
			}
			
		}

		function maxMInputFormatter(value, row, index) {
			return [ '<input id="maxBonusOdds_'+row.id+'" name="maxBonusOdds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function minMInputFormatter(value, row, index) {
			if(!value){
				value = "0"
			}
			return [ '<input id="minBonusOdds_'+row.id+'" name="minBonusOdds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

// 		function maxFInputFormatter(value, row, index) {
// 			return [ '<input id="maxRakeback" name="maxRakeback" class="form-control" type="text" value="'+value+'"/> ' ]
// 					.join('');
// 		}

// 		function minFInputFormatter(value, row, index) {
// 			return [ '<input id="minRakeback" name="minRakeback" class="form-control" type="text" value="'+value+'"/> ' ]
// 					.join('');
// 		}

		function maxNInputFormatter(value, row, index) {
			return [ '<input id="maxNumber_'+row.id+'" name="maxNumber" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}
		
		function playNameFormatter(value, row, index) {
			return [ '<input id="playName_'+row.id+'" name="name" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function sequenceInputFormatter(value, row, index) {
			return [ '<input id="sortNo_'+row.id+'" name="sortNo" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function operateFormatter(value, row, index) {
			return [icon(3),icon(1)].join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				$("#editmodel").modal('toggle');
				$("#playId").val(row.id);
				$("#playMethod").val(row.playMethod);
				$("#detailDesc").val(row.detailDesc);
				$("#winExample").val(row.winExample);
			},
			'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			},
			'click .save' : function(e, value, row, index) {
				save($(e.currentTarget).parents("tr"),row);
			}
		};

		function open(row) {
			var bc = {
	 				"status" : 2,
	 				"stationId" : $("#methodType").val(),
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotType/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		function close(row) {
			var bc = {
	 				"status" : 1,
	 				"stationId" : $("#methodType").val(),
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotType/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}

		function save(tr,row) {
			var maxBonusOdds=null;
			if($("#lotteryVersion").val()==3){
				maxBonusOdds = $("#maxBonusOdds_"+row.id).val();
			}
			var bc = {
				'id' : row.id,
				'maxBonusOdds' : maxBonusOdds,
				'minBonusOdds' : tr.find("#minBonusOdds_"+row.id).val(),
// 				'maxRakeback' : $("#maxRakeback").val(),
// 				'minRakeback' : $("#minRakeback").val(),
				'maxNumber' : tr.find("#maxNumber_"+row.id).val(),
				'name' : tr.find("#playName_"+row.id).val(),
				'sortNo' : tr.find("#sortNo_"+row.id).val(),
				"stationId" : $("#methodType").val()
			};
			$.ajax({
				url : "${base}/admin/lotType/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}

		function editor() {
			var bc = {
				'id' : $("#playId").val(),
				'playMethod' : $("#playMethod").val(),
				'detailDesc' : $("#detailDesc").val(),
				'winExample' : $("#winExample").val(),
				"stationId" : $("#methodType").val()
			};
			$.ajax({
				url : "${base}/admin/lotType/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}
		
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/lotType/list.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			columns : version1
		});
	</script>
	<style>
	.fixed-table-toolbar 
	.bars, 
	.fixed-table-toolbar 
	.columns,
	.fixed-table-toolbar 
	.search {
		position:relative;
		margin-top:10px;
		margin-bottom:10px;
	}
</style>
</body>
</html>
