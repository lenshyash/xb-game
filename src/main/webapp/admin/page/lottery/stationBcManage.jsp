<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/bootstrap.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div id="toolbar">
					<div id="search" class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<select class="form-control" id="sbclm_stationId"></select>
							</div>
							<div class="input-group">
								<select class="form-control" id="play_identify">
									<option value="1" selected="selected">第一版玩法</option>
									<option value="2">第二版玩法</option>
									<option value="3">第三版玩法(d站奖金模式)</option>
									<option value="4">第四版玩法</option>
									<option value="5">第五版玩法(d站赔率模式)</option>
								</select>
							</div>
						</div>
						<button class="btn btn-primary" id="toSave" onclick="editor();">保存</button>
					</div>
				</div>
			</div>
			
			<!-- 彩种列表 -->
			<div class="panel-body" id="menus"></div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){
		stationList();
		$("#sbclm_stationId").change(function() {
			layer.msg('初始化彩种中,请稍等。。。');
			$('#toSave').attr("disabled",true);
			 setTimeout(function (){
				 $('#toSave').attr("disabled",false)
			   }, 1500);
			initData();
			refresh();
		});
		
		$("#play_identify").change(function() {
			layer.msg('初始化彩种中,请稍等。。。');
			$('#toSave').attr("disabled",true);
			 setTimeout(function (){
				 $('#toSave').attr("disabled",false)
			   }, 1500);
			initData();
			refresh();
		});
	});	
	
		//站点列表
		function stationList() {
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j){
						if(${stationId}==j[i].id){
							col+='<option selected="selected" value="'+j[i].id+'">'+j[i].name+'</option>';
						}else{
							col+='<option value="'+j[i].id+'">'+j[i].name+'</option>';
						}
							
					}
					$('#sbclm_stationId').html(col);
					initData();
				}
			});
		}
		
		//站点玩法列表
		function initData(){
			var stationId = $('#sbclm_stationId').val();
			var identify = $('#play_identify').val();
			var url = '${base}/admin/lottery/bcList.do?stationId='+stationId+'&identify='+identify;
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j){
							col+='<div class="progress progress-danger progress-striped"><div class="bar" style="width: 100%"></div></div>';
							col+='<div class="checkbox firCheckBox"><label>';
							col+='<strong class="text-info">'+j[i].name+'</strong></label>【';
							var isShow = -1;
							for(var cz in j[i].cz){
// 								if(j[i].cz[cz].modelStatus==2){
// 									isShow = 1;
									if(j[i].cz[cz].modelStatus==2){
										col+='<label class="text-info"><input type="checkbox" class="firCz firCz_'+j[i].type+'" id="'+j[i].cz[cz].id+'" checked="checked" value="'+j[i].type+'">';
									}
									else{
										col+='<label class="text-info"><input type="checkbox" class="firCz firCz_'+j[i].type+'" id="'+j[i].cz[cz].id+'" value="'+j[i].type+'">';
									}
									col+=j[i].cz[cz].name+'</label>';
// 								}
							}
// 							if(isShow>0){
								col+='】';
// 							}else{
// 								col+='<span class="text-info">暂无可用的彩种</span>】';
// 							}
							
							for(var erji in j[i].rows){//二级
// 							  if(j[i].rows[erji].modelStatus==2&&isShow>0){
									col+='<div class="checkbox secCheckBox"><label>';
								if(j[i].rows[erji].modelStatus==2){
									col+='<input type="checkbox" checked="checked" id="'+j[i].rows[erji].id+'" class="secNav secNav_'+j[i].type+'" value="'+j[i].rows[erji].lotType+"_"+j[i].rows[erji].code+'">';//二级
								}
								else{
									col+='<input type="checkbox" id="'+j[i].rows[erji].id+'" class="secNav secNav_'+j[i].type+'" value="'+j[i].rows[erji].lotType+"_"+j[i].rows[erji].code+'">';//二级
								}
								col+=j[i].rows[erji].name+'</label>';//二级
								col+='<div class="checkbox trdCheckBox">';//三级
							for(var sanji in j[i].rows[erji].smallList){//三级
// 								if(j[i].rows[erji].smallList[sanji].modelStatus==2&&isShow>0){
									if(j[i].rows[erji].smallList[sanji].modelStatus==2){
										col+='<label><input type="checkbox" id="'+j[i].rows[erji].smallList[sanji].id+'" class="thiNav thiNav_'+j[i].type+' secThiNav_'+j[i].rows[erji].lotType+"_"+j[i].rows[erji].code+'" checked="checked">';//三级
									}else{
										col+='<label><input type="checkbox" id="'+j[i].rows[erji].smallList[sanji].id+'" class="thiNav thiNav_'+j[i].type+' secThiNav_'+j[i].rows[erji].lotType+"_"+j[i].rows[erji].code+'">';
									}
									col+=j[i].rows[erji].smallList[sanji].name+'</label>';//三级
// 								}
							}
							col+='</div>';
							col+='</div>';
// 								}
							}
							col+='</div>';//一级
					$('#menus').html(col);
					$('.firNav').click(function(){
						var czs = $('.firCz_'+this.value);
						var secs = $('.secNav_'+this.value);
						var thirs = $('.thiNav_'+this.value);
						selectOrInvert(this.checked,czs);
						selectOrInvert(this.checked,secs);
						selectOrInvert(this.checked,thirs);
					});
					$('.firCz').click(function(){
						var obj = $('.firCz_'+this.value);
						var isCheck = 0;
						for (var i = 0; i < obj.length; i++) {
							if(obj[i].checked){
								isCheck ++;
							}
						}
						var secs = $('.secNav_'+this.value);
 						var thirs = $('.thiNav_'+this.value);
						if(isCheck==0){//没选中彩种,就全选
	 						selectOrInvert(false,secs);
	 						selectOrInvert(false,thirs);
						}else if(isCheck==1&&this.checked){//全部反选状态下,任选一个彩种则全选
							selectOrInvert(true,secs);
		 					selectOrInvert(true,thirs);
						}
						
					});
					$('.secNav').click(function(){
						var thirs = $('.secThiNav_'+this.value);
						selectOrInvert(this.checked,thirs);
					});
				  }
				}
			});
		}
		
		//保存复选框
		function editor(){
			var czList = $('.firCz');//彩种
			var groupList = $('.secNav');//大类
			var play = $('.thiNav');//小类
			var czLottery = '';
			var groupLottery = '';
			var playLottery = '';
			for(var i in czList){
				if(czList[i].id>0){
					if(czList[i].checked){
						czLottery+=czList[i].id+'_2'+',';
					}else{
						czLottery+=czList[i].id+'_1'+',';
					}
				}
				
			}
			for(var j in groupList){
				if(groupList[j].id>0){
					if(groupList[j].checked){
						groupLottery+=groupList[j].id+'_2'+',';
					}else{
						groupLottery+=groupList[j].id+'_1'+',';
					}
				}
					
			}
			for(var k in play){
				if(play[k].id>0){
					if(play[k].checked){
						playLottery+=play[k].id+'_2'+',';
					}else{
						playLottery+=play[k].id+'_1'+',';
					}
				}
				
			}
				var stationId = $('#sbclm_stationId').val();
				var bc = {
						"czLottery" :czLottery,
						"groupLottery" :groupLottery,
						"playLottery" :playLottery,
						"identify" :$('#play_identify').val(),
						"stationId":stationId
		        	};
				
				//避免点太快两次请求!
				$('#toSave').attr("disabled",true);
				 setTimeout(function (){
					 $('#toSave').attr("disabled",false)
				   }, 1500);
				layer.msg('正在保存中,请稍等。。。');
				
// 				layer.confirm('确定要保存所有修改？', {
// 					  btn: ['确定','取消']
// 					}, function(){
			 			$.ajax({
		 				url : "${base}/admin/lottery/saveStationCheckBox.do",
		 				data : bc,
		 				success : function(result) {
		 					layer.msg('修改成功!', {icon: 1});
		 				}
		 			});
// 					}, function(){});
		}
		
		//全选反选
		function selectOrInvert(isChecked, obj) {
			if (isChecked) {// 全选
				for (var i = 0; i < obj.length; i++) {
					obj[i].checked = true;
				}
			} else {// 反选
				for (var i = 0; i < obj.length; i++) {
					obj[i].checked = false;
				}
			}
		}
	</script>
	<style>
	.secCheckBox {
	margin-left: 50px;
	}
	.trdCheckBox {
	margin-left: 80px;
	}
	.checkbox, .radio{
	margin-bottom:15px;
	}
	label{
		margin-top: 3px;
		margin-left: 5px;
		margin-right: 5px;
	}
	.progress{
		background-color : gray;
	}
	.noShow{
		display: none;
	}
	</style>
</body>
</html>