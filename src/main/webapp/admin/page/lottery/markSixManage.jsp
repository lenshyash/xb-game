<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
	<input id="smallTypeId" type="hidden"/>
	<input id="lotType" type="hidden"/>
		<ul class="nav nav-tabs czGroup">
			<li class="active"><a href="#" data-toggle="tab" class="lotName" id="6">香港彩</a>
			</li>
		</ul>
			<ul class="nav nav-pills smallTab"></ul>
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="methodType">查询类型</label> <input
					type="text" class="hide" id="lottype">
				<div class="form-group">
					<!-- <button class="btn btn-primary" onclick="add()">新增</button> -->
					<div class="input-group">
						<ul class="nav nav-pills playSmallTab"></ul>
					</div>
					<div class="input-group hide">
						<input type="text" class="form-control" id="keyword"
							placeholder="关键字">
					</div>
				</div>
				<button class="btn btn-primary hide" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<script type="text/javascript">
		$(function() {
			getLotTypeGroup(6);
			getTab();
		})
		
		//设置传入参数
		function queryParams(params) {
			params['playCode'] = $('#smallTypeId').val();
			params['stationId'] = 0;
			params['lotType'] = $('#lotType').val();
			return params
		}
		
		function getLotPlayId(groupId){
			var data = {
					"groupId":groupId,
					"stationId":0
			};
			$.ajax({
				url:'${base}/admin/lotType/list.do',
				data:data,
				dataType:'json',
				type:'get',
				success:function(i){
					var cal = "";
					for(var x in i.rows){
						if(x==0){
							cal+='<li class="isActives active" id="activeId_'+i.rows[x].code+'"><a href="#" class="lotPlayCode" lotType="'+i.rows[x].lotType+'" id="'+i.rows[x].code+'">'+i.rows[x].name+'</a></li>';
							$('#smallTypeId').val(i.rows[x].code);
							$('#lotType').val(i.rows[x].lotType);
						}else{
							cal+='<li class="isActives" id="activeId_'+i.rows[x].code+'"><a href="#"  class="lotPlayCode" lotType="'+i.rows[x].lotType+'" id="'+i.rows[x].code+'">'+i.rows[x].name+'</a></li>';
						}
					}
					$(".playSmallTab").html(cal);
					$(".lotPlayCode").click(function(){
						$(".isActives").removeClass("active");
						$("#activeId_"+this.id).addClass("active");
						$("#smallTypeId").val(this.id);
						$('#lotType').val($(this).attr('lotType'));
						refresh();
					});
					refresh();
				}
			})
		}

		function getLotTypeGroup(id) {
			var bc = {
	 				"lotType" : id,
					"stationId" :0
	        	};
					$.ajax({
						url : "${base}/admin/lotTypeGroup/list.do",
						data : bc,
						type : "get",
						dataType : 'json',
						success : function(i) {
							var col = "";
							for(var j in i.rows){
								if(j==0){
									col+='<li class="isActive active" id="activeId_'+i.rows[j].id+'"><a href="#" class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
									$('#smallTypeId').val(i.rows[j].id);
									getLotPlayId(i.rows[j].id);
								}else{
									col+='<li class="isActive" id="activeId_'+i.rows[j].id+'"><a href="#"  class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
								}
							}
							$(".smallTab").html(col);
							$('.lotGroupId').click(function(){
								$('.isActive').removeClass("active");
								$('#activeId_'+this.id).addClass("active");
								getLotPlayId(this.id);
							});
						}
					});
		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/markSix/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '号码',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, 
				{
					field : 'odds',
					title : '赔率',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : oddsInputFormatter
				}, 
				{
					field : 'minBetAmmount',
					title : '最小下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : minBetInputFormatter
				}, {
					field : 'maxBetAmmount',
					title : '最大下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : maxBetInputFormatter
				}, {
					field : 'sortNo',
					title : '序号',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : sortNoInputFormatter
				},{
					field : 'moduleStatus',
					title : '模版状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				},{
					title : '操作',
					align : 'center',
					width : '150',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function oddsInputFormatter(value, row, index) {
			return [ '<input id="odds_'+row.id+'" name="odds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function minBetInputFormatter(value, row, index) {
			return [ '<input id="minBet_'+row.id+'" name="minBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}
		
		function maxBetInputFormatter(value,row,index){
			return [ '<input id="maxBet_'+row.id+'" name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
			.join('');
		}

		function sortNoInputFormatter(value, row, index) {
			return [ '<input id="sortNo_'+row.id+'" name="sortNo" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function operateFormatter(value, row, index) {
			return [icon(3)].join('');
		}

		window.operateEvents = {
			'click .save' : function(e, value, row, index) {
				save(row);
			},'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			}
		};
		
		function open(row){
			var bc = {
	 				"status" : 2,
					"id" :row.id,
	        	};
			$.ajax({
				url : "${base}/admin/markSix/moduleCloseOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function close(row) {
			var bc = {
	 				"status" : 1,
					"id" :row.id,
	        	};
			$.ajax({
				url : "${base}/admin/markSix/moduleCloseOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function save(row) {
			var bc = {
				'id' : row.id,
				'odds' : $("#odds_"+row.id).val(),
				'minBetAmmount' : $("#minBet_"+row.id).val(),
				'maxBetAmmount' : $("#maxBet_"+row.id).val(),
				'sortNo' : $("#sortNo_"+row.id).val()
			};
			$.ajax({
				url : "${base}/admin/markSix/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}
	</script>
	<style>
	.fixed-table-toolbar 
	.bars, 
	.fixed-table-toolbar 
	.columns,
	.fixed-table-toolbar 
	.search {
		position:relative;
		margin-top:10px;
		margin-bottom:10px;
	}
</style>
</body>
</html>
