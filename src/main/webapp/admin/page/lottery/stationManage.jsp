<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
		<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="keyword">关键字</label> <label class="sr-only" for="slm_stationId">查询类型</label>
					<div class="input-group">
						<span class="btn btn-primary" onclick="newAddModel();">新版站点玩法管理</span>
					</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="lotteryVersion" disabled="disabled">
							<option value="1">第一版玩法</option>
							<option value="2">第二版玩法</option>
							<option value="3">第三版玩法(d站奖金模式)</option>
							<option value="4">第四版玩法</option>
							<option value="5">第五版玩法(d站赔率模式)</option>
						</select>
					</div>
					<div class="input-group">
						<select class="form-control" id="slm_stationId">
						</select>
					</div>
					<div class="input-group">
						<span class="btn btn-warning" onclick="closeOtherVersion();">关闭其他版本</span>&nbsp;&nbsp;
						<!-- <span class="btn btn-success" onclick="reMode();">重新生成玩法</span> -->
						<!-- <span class="btn btn-primary" onclick="addMode();">站点玩法管理</span> -->
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">彩种编辑</h4>
				</div>
				<div class="modal-body">
					<input id="bcId" class="form-control" type="hidden"/> 
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">彩种名称：</td>
								<td width="35%" class="text-left">
								<input type="text" class="form-control" id="name" />
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">开封盘时间差(秒)：</td>
								<td width="35%" class="text-left">
								<input type="text" class="form-control" id="ago" />
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">显示时分组：</td>
								<td width="35%" class="text-left">
								<select id="viewGroup" class="form-control">
								<option value="1">时时彩</option>
								<option value="2">低频彩</option>
								<option value="3">PK10</option>
								<option value="5">11选5</option>
								<option value="6">香港彩</option>
								<option value="7">快三</option>
								<option value="8">快乐十分</option>
								<option value="9">PC蛋蛋</option>
								</select>
								</td>
							</tr>
							<tr style="display:none" id="typeTr">
								<td width="15%" class="text-right">彩种类型：</td>
								<td width="35%" class="text-left">
								<select id="lotType" class="form-control">
								<option value="8">北京赛车</option>
								<option value="9">时时彩</option>
								<option value="10">快3</option>
								<option value="11">PC蛋蛋</option>
								<option value="12">快乐十分</option>
								<option value="14">11选5</option>
								<option value="15">排列3</option>
								<option value="6">香港彩</option>
								<option value="66">十分香港彩</option>
								<option value="828">系统赛车</option>
								<option value="929">系统时时彩</option>
								<option value="102">系统快3</option>
								</select>
								</td>
							</tr>
							
							<tr>
								<td width="15%" class="text-right">序号：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="sortNo" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">状态：</td>
								<td width="35%" class="text-left">
								<select id="status" class="form-control">
										<option value="1">禁用</option>
										<option value="2">启用</option>
								</select>
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">图标链接：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="imgUrl" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	var version;
	function getTab() {
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/lottery/list.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			columns : [ {
				field : 'name',
				title : '彩种名称',
				align : 'center',
				width : '200',
				valign : 'middle',
			}, {
				field : 'identify',
				title : '彩票版本',
				align : 'center',
				width : '180',
				valign : 'bottom'
			}, {
				field : 'ago',
				title : '开封盘时间差(秒)',
				align : 'center',
				width : '180',
				valign : 'bottom'
			}, {
				field : 'code',
				title : '彩种编码',
				align : 'center',
				width : '200',
				valign : 'middle'
			}, {
				field : 'viewGroup',
				title : '显示时分组',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : groupViewCZ
			}, {
				field : 'balls',
				title : '球数',
				align : 'center',
				width : '200',
				valign : 'middle'
			}, {
				field : 'type',
				title : '彩种类型',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : showCZ
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle'
			}, {
				field : 'status',
				title : '彩种状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ]
		});
	}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '修改</a>  '
// 					'<a class="del" href="javascript:void(0)" title="删除">',
// 					,'<i class="glyphicon glyphicon-remove"></i>', '</a>' 
					]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				version = $("#lotteryVersion").val();
				if(version==2){
					$("#typeTr").show();
					$("#lotType").val(row.type);
				}else{
					$("#typeTr").hide();
				}
				$("#editmodel").modal('toggle');
				$("#name").val(row.name);
				$("#sortNo").val(row.sortNo);
				$("#viewGroup").val(row.viewGroup);
				
				$("#ago").val(row.ago);
				$("#status").val(row.status);
				$('#bcId').val(row.id);
				$('#imgUrl').val(row.imgUrl);
			},
			'click .stateOpen' : function(e, value, row, index) {
				row.status = 2;
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				row.status = 1;
				close(row);
			}
// 			,
// 			'click .del' : function(e, value, row, index) {
// 				del(row);
// 			}
		};
		
		function reMode(){
			var sid = $('#slm_stationId').val();
			var version = $("#lotteryVersion").val();
			var staText = $('#slm_stationId').find("option:selected").text();
			var data = {
					stationId:sid,
					version:version
			}
			Msg.confirm('确定重新生成站点【'+staText+'】的玩法?',function(){
				var url = '${base}/admin/lottery/reloadPlay.do';
				$.ajax({
					url : url,
					data:data,
					success : function(result) {
						if(result.success){
							Msg.info('玩法重新生成成功!');
						}else{
							Msg.info(result.msg);
						}
					}
				});
			});
			
		}

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#slm_stationId").val();
			params['modelStatus'] = 2;
			return params
		}
		
		$(function() {
			stationList();
			$("#slm_stationId").change(function() {
				getStationVersion();
				refresh();
			});
		})
		
		//新版新增站点彩种
		function newAddModel(){
			layer.open({
				type:2,
				title:'新增玩法',
				shadeClose:true,
				shade:0.8,
				area:['1000px','100%'],
				content:'${base}/admin/lotStationManage/newLottery.do?stationId='+$("#slm_stationId").val(),
				end:function(){
					search();
				}
			});
		}
		
		function closeOtherVersion(){
			var text = $("#lotteryVersion option:selected").text();
			layer.confirm('当前版本为'+text+',确定关闭其他版本的数据？',{
				btn:['确定','取消']
			},function(index){
				var stId = $("#slm_stationId").val();
				var version = $("#lotteryVersion").val();
				$.ajax({
					url:'${base}/admin/lottery/closeOtherVersion.do',
					data:{stationId:stId,version:version},
					success:function(data){
						refresh();
						layer.msg('关闭彩种成功!',{icon:1});
					}
				})
				layer.close(index);
			})
		}
		
		//点击新增站点彩种
		function addMode(){
			layer.confirm('确定还使用旧版本管理站点彩种？', {
				  btn: ['确定','取消'] //按钮
				}, function(index){
					layer.open({
						  type: 2,
						  title: '新增玩法',
						  shadeClose: true,
						  shade: 0.8,
						  area: ['1000px', '100%'],
						  content: '${base}/admin/lotStationManage/newPlay.do?stationId='+$("#slm_stationId").val()
					}); 
					layer.close(index);
				});
		}
		
		//站点列表
		function stationList() {
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j){
						col+='<option value="'+j[i].id+'">'+j[i].name+'</option>';
					}
					$('#slm_stationId').html(col);
					getStationVersion();
				}
			});
		}
		
		function getStationVersion(){
			var stationId = $("#slm_stationId").val();
			$.ajax({
				url:"${base}/admin/lotStationManage/stationVersion.do",
				data:{stationId:stationId},
				success:function(res){
					$("#lotteryVersion").val(res.version);
					getTab();
				}
			})
		}
		
		//站点彩种列表
		function czStationList() {
			var url =  "${base}/admin/lottery/list.do";
			$.ajax({
				url : url,
				success : function(j) {
					var col='';
					for(var i in j.rows){
							if(i%5==0){
								col+='<div class="checkbox">';
								col+='<label class="checkbox-inline">';
								col+='<input type="checkbox" id="'+j.rows[i].code+'" class="czFxk" value="'+j.rows[i].id+'">'+j.rows[i].name+'</label>';
							}else{
								col+='<label class="checkbox-inline">';
								col+='<input type="checkbox" id="'+j.rows[i].code+'" class="czFxk" value="'+j.rows[i].id+'">'+j.rows[i].name+'</label>';
							}
					}
					$('.czNameList').html(col);
				}
			});
		}
		
		function search() {
			refresh();
		}
		
		function open(row) {
			$.ajax({
				url : "${base}/admin/lottery/closeOrOpen.do",
				data : row,
				success : function(result) {
					tips(row.name,2);
					refresh();
				}
			});
		}
		function close(row) {
			$.ajax({
				url : "${base}/admin/lottery/closeOrOpen.do",
				data : row,
				success : function(result) {
					tips(row.name,3);
					refresh();
				}
			});
		}
		function del(row) {
			var bc = {
					"id" :row.id,
					"modelStatus":1
			};
			layer.confirm('确定要把【'+row.name+'】删除掉？', {
				  btn: ['确定','取消']
				}, function(){
					$.ajax({
						url : "${base}/admin/lottery/update.do",
						data : bc,
						success : function(result) {
							layer.msg('删除成功!', {icon: 1});
							refresh();
						}
					});
				 
				}, function(){});
		}
		function editor(){
			var lottype =0;
			if(version==2){
				lottype = $('#lotType').val();
			}
			var bc = {
	 				"name" : $('#name').val(),
					"ago" :$('#ago').val(),
					"balls" :$('#balls').val(),
					"sortNo" :$('#sortNo').val(),
					"viewGroup" :$('#viewGroup').val(),
					"status" :$('#status').val(),
					"id" :$('#bcId').val(),
					"type" :lottype,
					"imgUrl" :$('#imgUrl').val(),
	        	};
			$.ajax({
				url : "${base}/admin/lottery/update.do",
				data : bc,
				success : function(result) {
					tips('更新成功!',1);
					refresh();
				}
			});
		}
		
	</script>
</body>
</html>
