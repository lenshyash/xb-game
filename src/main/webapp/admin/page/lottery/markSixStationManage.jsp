<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
	<input id="smallTypeId" type="hidden"/>
	<input id="keyVal" type="hidden" value=""/> 
	<input id="keyValName" type="hidden" value=""/> 
	<input id="lotType" type="hidden"/>
		<ul class="nav nav-tabs czGroup">
			<li class="active"><a href="#" data-toggle="tab" class="lotName" id="6">香港彩</a></li>
		</ul>
		<ul class="nav nav-pills smallTab"></ul>
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="methodType">查询类型</label> <input
					type="text" class="hide" id="lottype">
				<div class="form-group">
					<!-- <button class="btn btn-primary" onclick="add()">新增</button> -->
					<a class='btn btn-danger' href='javascript:generatePage()'>生成文件</a>
					<div class="input-group"><select class="form-control" id="methodType"></select></div>
					<div class="input-group">
						<ul class="nav nav-pills playSmallTab"></ul>
					</div>
					<div class="input-group hide">
						<input type="text" class="form-control" id="keyword"
							placeholder="关键字">
					</div>
				</div>
				<button class="btn btn-primary hide" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<script type="text/javascript">
		$(function() {
			stationList(null);
			$("#methodType").change(function() {
				var curVal=$(this).find("option:checked").attr("value");
				var curValName=$(this).find("option:checked").html();
				$("#keyVal").val(curVal);
				$("#keyValName").val(curValName);
				getLotPlayId(null);
				getLotTypeGroup(6);
				getTab();
				refresh();
			});
		})
		
		//设置传入参数
		function queryParams(params) {
			params['playCode'] = $('#smallTypeId').val();
			params['stationId'] = $("#keyVal").val();
			params['lotType'] = $('#lotType').val();
			return params
		}
		
		
		//站点列表
		function stationList(attrId) {
			var stationId = $('#methodType').val();
			var url = '${base}/admin/station/combo.do';
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j){
						if(i==0){
							//直接填充当前第一个站点Id=#keyVal，不需要设置为1，以防站点更换无法找到相应的站点Id
							$("#keyVal").val(j[i].id);
							$("#keyValName").val(j[i].name);
						}
						if(stationId==j[i].id){
							col+='<option selected="selected" value="'+j[i].id+'">'+j[i].name+'</option>';
						}else{
							col+='<option value="'+j[i].id+'">'+j[i].name+'</option>';
						}
					}
					if(attrId==null){
						$('#methodType').html(col);
					}else{
						$('#'+attrId).html(col);
					}
					getLotTypeGroup(6);
					getTab();
				}
			});
		}
		
		function getLotPlayId(groupId){
			var data = {
					"groupId":groupId,
					"stationId":$("#keyVal").val()
			};
			$.ajax({
				url:'${base}/admin/lotType/list.do',
				data:data,
				dataType:'json',
				type:'get',
				success:function(i){
					var cal = "";
					for(var x in i.rows){
						if(x==0){
							cal+='<li class="isActives active" id="activeId_'+i.rows[x].code+'"><a href="#" class="lotPlayCode" lotType="'+i.rows[x].lotType+'" id="'+i.rows[x].code+'">'+i.rows[x].name+'</a></li>';
							$('#smallTypeId').val(i.rows[x].code);
							$('#lotType').val(i.rows[x].lotType);
						}else{
							cal+='<li class="isActives" id="activeId_'+i.rows[x].code+'"><a href="#"  class="lotPlayCode" lotType="'+i.rows[x].lotType+'" id="'+i.rows[x].code+'">'+i.rows[x].name+'</a></li>';
						}
					}
					$(".playSmallTab").html(cal);
					$(".lotPlayCode").click(function(){
						$(".isActives").removeClass("active");
						$("#activeId_"+this.id).addClass("active");
						$("#smallTypeId").val(this.id);
						$('#lotType').val($(this).attr('lotType'));
						refresh();
					});
					refresh();
				}
			})
		}

		function getLotTypeGroup(id) {
			var bc = {
	 				"lotType" : id,
					"stationId" :$("#keyVal").val()
	        	};
					$.ajax({
						url : "${base}/admin/lotTypeGroup/list.do",
						data : bc,
						type : "get",
						dataType : 'json',
						success : function(i) {
							var col = "";
							for(var j in i.rows){
								if(j==0){
									col+='<li class="isActive active" id="activeId_'+i.rows[j].id+'"><a href="#" class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
									$('#smallTypeId').val(i.rows[j].id);
									getLotPlayId(i.rows[j].id);
								}else{
									col+='<li class="isActive" id="activeId_'+i.rows[j].id+'"><a href="#"  class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
								}
							}
							$(".smallTab").html(col);
							$('.lotGroupId').click(function(){
								$('.isActive').removeClass("active");
								$('#activeId_'+this.id).addClass("active");
								getLotPlayId(this.id);
							});
						}
					});
		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/markSix/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '号码',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, 
				{
					field : 'odds',
					title : '赔率',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : oddsInputFormatter
				}, 
				{
					field : 'minBetAmmount',
					title : '最小下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : minBetInputFormatter
				}, {
					field : 'maxBetAmmount',
					title : '最大下注金额',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : maxBetInputFormatter
				}, {
					field : 'sortNo',
					title : '序号',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : sortNoInputFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				},{
					title : '操作',
					align : 'center',
					width : '150',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function oddsInputFormatter(value, row, index) {
			return [ '<input id="odds_'+row.id+'" name="odds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function minBetInputFormatter(value, row, index) {
			return [ '<input id="minBet_'+row.id+'" name="minBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}
		
		function maxBetInputFormatter(value,row,index){
			return [ '<input id="maxBet_'+row.id+'" name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/> ' ]
			.join('');
		}

		function sortNoInputFormatter(value, row, index) {
			return [ '<input id="sortNo_'+row.id+'" name="sortNo" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function operateFormatter(value, row, index) {
			return [icon(3)].join('');
		}

		window.operateEvents = {
			'click .save' : function(e, value, row, index) {
				save(row);
			},'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			}
		};
		
		
		function open(row){
			var bc = {
	 				"status" : 2,
					"id" :row.id,
					"stationId":$("#keyVal").val()
	        	};
			$.ajax({
				url : "${base}/admin/markSix/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function close(row) {
			var bc = {
	 				"status" : 1,
					"id" :row.id,
					"stationId":$("#keyVal").val()
	        	};
			$.ajax({
				url : "${base}/admin/markSix/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function save(row) {
			var bc = {
				'id' : row.id,
				'odds' : $("#odds_"+row.id).val(),
				'minBetAmmount' : $("#minBet_"+row.id).val(),
				'maxBetAmmount' : $("#maxBet_"+row.id).val(),
				'sortNo' : $("#sortNo_"+row.id).val(),
				'stationId':$("#keyVal").val()
			};
			$.ajax({
				url : "${base}/admin/markSix/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}
		
		//生成静态页面
		function generatePage(){
			var bc = {
					'stationId':$("#keyVal").val()
			};
			$.ajax({
					url:'${base}/admin/markSix/generatePage.do',
					data:bc,
					success:function(result){
						tips('生成('+$("#keyValName").val()+')站点页面成功',1);
						refresh();
					}
			})
		}
	</script>
	<style>
	.fixed-table-toolbar 
	.bars, 
	.fixed-table-toolbar 
	.columns,
	.fixed-table-toolbar 
	.search {
		position:relative;
		margin-top:10px;
		margin-bottom:10px;
	}
</style>
</body>
</html>
