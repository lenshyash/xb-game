<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
	<input id="smallTypeId" type="hidden"/>
		<div class="form-group">
			<div class="input-group">
				<select class="form-control" id="lotteryVersion" onchange="getLotterGroup()">
					<option value="1">第一版玩法</option>
					<option value="2">第二版玩法</option>
					<option value="3">第三版玩法(d站赔率模式)</option>
					<option value="4">第四版玩法</option>
					<option value="5">第五版玩法(d站赔率模式)</option>
				</select>
			</div>
		</div>
		<ul class="nav nav-tabs czGroup" id="lotteryType">
<!-- 			<li class="active"> -->
<!--      	 		<a href="#" data-toggle="tab" class="lotName" id="1">系统彩</a> -->
<!--      	 	</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="2">时时彩</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="3">pk10</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="4">排列三</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="5">11选5</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="7">PC蛋蛋</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="8">pk10(第二版)</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="9">时时彩(第二版)</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="10">快三(第二版)</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="11">PC蛋蛋(第二版)</a> -->
<!--       		</li> -->
<!--       		<li> -->
<!--       			<a href="#" data-toggle="tab" class="lotName" id="12">快乐十分</a> -->
<!--       		</li> -->
		</ul>
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="methodType">查询类型</label> <input
					type="text" class="hide" id="lottype">
				<div class="form-group">
					<button class="btn btn-primary" onclick="add()">新增</button>
					<div class="input-group">
						<ul class="nav nav-pills smallTab" style="width:900px;"></ul>
					</div>
					<div class="input-group hide">
						<input type="text" class="form-control" id="keyword"
							placeholder="关键字">
					</div>
				</div>
				<button class="btn btn-primary hide" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	
	<!-- 新增 -->
	<div class="modal fade" id="addmodel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">新增玩法小类</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="newId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">玩法名称：</td>
								<td width="25%" class="text-left"><input id="newName"
									class="form-control" type="text" /></td>
								<td width="25%" class="text-right">玩法编码：</td>
								<td width="25%" class="text-left"><input id="newCode"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">最大中奖金额：</td>
								<td width="25%" class="text-left"><input id="newMaxBonusOdds"
									class="form-control" type="text" /></td>
								<td width="25%" class="text-right">最小中奖金额：</td>
								<td width="25%" class="text-left"><input id="newMinBonusOdds"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">玩法种类：</td>
								<td width="25%" class="text-left">
								<select id="playType" class="form-control">
<!-- 										<option value="1" selected="selected">系统彩</option> -->
<!-- 										<option value="2">时时彩</option> -->
<!-- 										<option value="3">pk10</option> -->
<!-- 										<option value="4">排列三</option> -->
<!-- 										<option value="5">11选5</option> -->
<!-- 										<option value="7">PC蛋蛋</option> -->
<!-- 										<option value="8">pk10(第二版)</option> -->
<!-- 										<option value="9">时时彩(第二版)</option> -->
<!-- 										<option value="10">快三(第二版)</option> -->
<!-- 										<option value="11">PC蛋蛋(第二版)</option> -->
<!-- 										<option value="12">快乐十分</option> -->
									</select>
								</td>
								<td width="25%" class="text-right">最高注数：</td>
								<td width="25%" class="text-left"><input id="newMaxNumber"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">玩法介绍：</td>
								<td width="75%" class="text-left" colspan="3"><input id="newPlayMethod"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">详细介绍：</td>
								<td width="75%" class="text-left" colspan="3"><input id="newDetailDesc"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">中奖范例：</td>
								<td width="75%" class="text-left" colspan="3"><input id="newWinExample"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">序号：</td>
								<td width="25%" class="text-left"><input id="newSortNo"
									class="form-control" type="text" /></td>
								<td width="25%" class="text-right">状态：</td>
								<td width="25%" class="text-left">
									<select id="newStatus" class="form-control">
										<option value="2">启用</option>
										<option value="1">停用</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="save2();">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 修改 -->
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">玩法小类修改</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">玩法介绍：</td>
								<td width="75%" class="text-left"><input id="playMethod"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">详细介绍：</td>
								<td width="75%" class="text-left"><input id="detailDesc"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">中奖范例：</td>
								<td width="75%" class="text-left"><input id="winExample"
									class="form-control" type="text" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			version();
			getLotterGroup();
// 			getLotTypeGroup(1);
// 			getTab();
// 			$('.lotName').click(function() {
// 				$('#smallTypeId').val("-1");
// 				getLotTypeGroup(this.id);
// 			});
		})
		
		//设置传入参数
		function queryParams(params) {
			params['groupId'] = $('#smallTypeId').val();
			params['stationId'] = 0;
			return params
		}
		
		function getLotterGroup(){
			var identify = $('#lotteryVersion').val();
			$.ajax({
				url : "${base}/admin/lottery/list.do",
				data : {
					"identify":identify,
					"stationId":0,
					"rows":100,
					"page":1
					},
				success : function(result) {
					var lType = new Array();
					var d = '';
					var c = '';
					var type='';
					for(var i=0;i<result.rows.length;i++){
						if(lType.indexOf(result.rows[i].type) == -1){
							lType[i]=result.rows[i].type;
							type = showCZ(result.rows[i].type);
							if(i==0){
								d+='<li class="active"><a href="#" data-toggle="tab" class="lotName" id="'+result.rows[i].type+'">'+type+'</a></li>';
								c+='<option value="'+result.rows[i].type+'" selected="selected">'+type+'</option>';
								$("#lottype").val(result.rows[i].type);
								continue;
							}
							d+='<li><a href="#" data-toggle="tab" class="lotName" id="'+result.rows[i].type+'">'+type+'</a></li>';
							c+='<option value="'+result.rows[i].type+'">'+type+'</option>';
						}
					}
					$("#lotteryType").html(d);
					getLotTypeGroup($("#lottype").val());
					$('.lotName').click(function() {
						$("#lottype").val(this.id);
						getLotTypeGroup($("#lottype").val());
// 						refresh();
					});
					$("#playType").html(c);
				}
			});
		}
		
		function getLotTypeGroup(id) {
			var bc = {
	 				"lotType" : id,
					"stationId" :0
	        	};
					$.ajax({
						url : "${base}/admin/lotTypeGroup/list.do",
						data : bc,
						type : "get",
						dataType : 'json',
						success : function(i) {
							var col = "";
							for(var j in i.rows){
								if(j==0){
									col+='<li class="isActive active" id="activeId_'+i.rows[j].id+'"><a href="#" class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
									$('#smallTypeId').val(i.rows[j].id);
								}else{
									col+='<li class="isActive" id="activeId_'+i.rows[j].id+'"><a href="#"  class="lotGroupId" id="'+i.rows[j].id+'">'+i.rows[j].name+'</a></li>';
								}
							}
							$(".smallTab").html(col);
							getTab();
							$('.lotGroupId').click(function(){
								$('.isActive').removeClass("active");
								$('#activeId_'+this.id).addClass("active");
								$('#smallTypeId').val(this.id);
								refresh();
							});
							refresh();
						}
					});
		}
		
		var version1= [];
		var version3= [];
		function version(){
			version1 = [ {
				field : 'name',
				title : '玩法小类名称',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : playNameFormatter
			}, 
// 				{
// 					field : 'maxBonusOdds',
// 					title : '最大中奖金额(元)',
// 					align : 'center',
// 					width : '200',
// 					valign : 'bottom',
// 					formatter : maxMInputFormatter
// 				}, 
			{
				field : 'minBonusOdds',
				title : '最小中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : minMInputFormatter
			}, 
//				{
//					field : 'maxRakeback',
//					title : '最高返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : maxFInputFormatter
//				}, {
//					field : 'minRakeback',
//					title : '最低返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : minFInputFormatter
//				}, 
			{
				field : 'maxNumber',
				title : '最高注数',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : maxNInputFormatter
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : sequenceInputFormatter
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '150',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ];
			
			version3 = [ {
				field : 'name',
				title : '玩法小类名称',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : playNameFormatter
			}, {
				field : 'maxBonusOdds',
				title : '最大中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'bottom',
				formatter : maxMInputFormatter
			}, {
				field : 'minBonusOdds',
				title : '最小中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : minMInputFormatter
			}, 
//				{
//					field : 'maxRakeback',
//					title : '最高返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : maxFInputFormatter
//				}, {
//					field : 'minRakeback',
//					title : '最低返水',
//					align : 'center',
//					width : '200',
//					valign : 'middle',
//					formatter : minFInputFormatter
//				}, 
			{
				field : 'maxNumber',
				title : '最高注数',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : maxNInputFormatter
			}, {
				field : 'sortNo',
				title : '序号',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : sequenceInputFormatter
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				events : operateEvents,
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '150',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ];
		}

		function getTab() {
			if($("#lotteryVersion").val()==1||$("#lotteryVersion").val()==2||$("#lotteryVersion").val()==4){
				table.bootstrapTable('refreshOptions', {
					columns : version1,
					url : '${base}/admin/lotType/list.do',
					pageNumber : 1
				});
			}else{
				table.bootstrapTable('refreshOptions', {
					columns : version3,
					url : '${base}/admin/lotType/list.do',
					pageNumber : 1
				});
			}
			
		}
		
		function maxMInputFormatter(value, row, index) {
			return [ '<input id="maxBonusOdds_'+row.id+'" name="maxBonusOdds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function minMInputFormatter(value, row, index) {
			if(!value){
				return [ '<input id="minBonusOdds_'+row.id+'" name="minBonusOdds" class="form-control" type="text" value="0"/> ' ]
				.join('');
			}
			return [ '<input id="minBonusOdds_'+row.id+'" name="minBonusOdds" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

// 		function maxFInputFormatter(value, row, index) {
// 			return [ '<input id="maxRakeback_'+row.id+'" name="maxRakeback" class="form-control" type="text" value="'+value+'"/> ' ]
// 					.join('');
// 		}
		function playNameFormatter(value, row, index) {
			return [ '<input id="playName'+row.id+'" name="name" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

// 		function minFInputFormatter(value, row, index) {
// 			return [ '<input id="minRakeback_'+row.id+'" name="minRakeback" class="form-control" type="text" value="'+value+'"/> ' ]
// 					.join('');
// 		}

		function maxNInputFormatter(value, row, index) {
			return [ '<input id="maxNumber_'+row.id+'" name="maxNumber" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function sequenceInputFormatter(value, row, index) {
			return [ '<input id="sortNo_'+row.id+'" name="sortNo" class="form-control" type="text" value="'+value+'"/> ' ]
					.join('');
		}

		function operateFormatter(value, row, index) {
			return [icon(3),icon(1)].join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				$("#editmodel").modal('toggle');
				$("#playId").val(row.id);
				$("#playMethod").val(row.playMethod);
				$("#detailDesc").val(row.detailDesc);
				$("#winExample").val(row.winExample);
			},
			'click .stateOpen' : function(e, value, row, index) {
				open(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				close(row);
			},
			'click .save' : function(e, value, row, index) {
				save(row);
			}
		};
		
		function add(){
			$("#addmodel").modal('toggle');
			$("#newStatus").val("2");
			$("#newName").val("");
			$("#newCode").val("");
			$("#newMaxNumber").val("");
			$("#newMaxBonusOdds").val("");
			$("#newMinBonusOdds").val("");
			$("#newPlayMethod").val("");
			$("#newDetailDesc").val("");
			$("#newWinExample").val("");
			$("#newSortNo").val("");
		}
		
		function open(row) {
			var bc = {
	 				"status" : 2,
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotType/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已启用!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		function close(row) {
			var bc = {
	 				"status" : 1,
					"id" :row.id
	        	};
			$.ajax({
				url : "${base}/admin/lotType/closeOrOpen.do",
				data : bc,
				success : function(result) {
					layer.alert('【'+row.name+'】已关闭!', {
						  icon: 1,
						  skin: 'layer-ext-moon'
						})
					refresh();
				}
			});
		}
		
		function save(row) {
			var maxBonusOdds=null;
			if($("#lotteryVersion").val()==3){
				maxBonusOdds = $("#maxBonusOdds_"+row.id).val();
			}
			var bc = {
				'id' : row.id,
				'maxBonusOdds' : maxBonusOdds,
				'minBonusOdds' : $("#minBonusOdds_"+row.id).val(),
// 				'maxRakeback' : $("#maxRakeback").val(),
// 				'minRakeback' : $("#minRakeback").val(),
				'maxNumber' : $("#maxNumber_"+row.id).val(),
				'name' : $("#playName"+row.id).val(),
				'sortNo' : $("#sortNo_"+row.id).val()
			};
			$.ajax({
				url : "${base}/admin/lotType/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}

		function save2(){
			var name = $("#newName").val();
			var code = $("#newCode").val();
			var maxNumber = $("#newMaxNumber").val();
			var maxBonusOdds = $("#newMaxBonusOdds").val();
			var minBonusOdds = $("#newMinBonusOdds").val();
			var lotType = $("#playType").val();
			var playMethod = $("#newPlayMethod").val();
			var detailDesc = $("#newDetailDesc").val();
			var winExample = $("#newWinExample").val();
			var sortNo = $("#newSortNo").val();
			var status = $("#newStatus").val();
			if(name==null||name==""){
				tips('玩法名称不能为空！',4);
				return false;
			}else if(code==null||code==""){
				tips('玩法编码不能为空！', 4);
				return false;
			}else if(maxNumber==null||maxNumber==""){
				tips('最高注数不能为空！', 4);
				return false;
			}else if(maxBonusOdds==null||maxBonusOdds==""){
				tips('最大中奖金额不能为空！', 4);
				return false;
			}else if(minBonusOdds==null||minBonusOdds==""){
				tips('最小中奖金额不能为空！', 4);
				return false;
			}else if(sortNo==null||sortNo==""){
				tips('序号不能为空！', 4);
				return false;
			}
			var bc = {
					'groupId' : $('#smallTypeId').val(),
	 				'name' : name,
					'code' : code,
					'lotType' : lotType,
					'maxNumber' : maxNumber,
	 				'maxBonusOdds' : maxBonusOdds,
					'minBonusOdds' : minBonusOdds,
					'playMethod' : playMethod,
					'detailDesc' : detailDesc,
					'winExample' : winExample,
					'sortNo' : sortNo,
					'status' : status
				};
				$.ajax({
					url : "${base}/admin/lotType/save.do",
					data : bc,
					success : function(result) {
						tips('新增成功', 1);
						$("#addmodel").modal('toggle');
						refresh();
					},
	                error: function () {
	                	$("#addmodel").modal('toggle');
	                }
				})
		}

		function editor() {
			var bc = {
				'id' : $("#playId").val(),
				'playMethod' : $("#playMethod").val(),
				'detailDesc' : $("#detailDesc").val(),
				'winExample' : $("#winExample").val()
			};
			$.ajax({
				url : "${base}/admin/lotType/update.do",
				data : bc,
				success : function(result) {
					tips('修改成功', 1);
					refresh();
				}
			})
		}
		
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/lotType/list.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			columns : version1
		});
	</script>
	<style>
	.fixed-table-toolbar 
	.bars, 
	.fixed-table-toolbar 
	.columns,
	.fixed-table-toolbar 
	.search {
		position:relative;
		margin-top:10px;
		margin-bottom:10px;
	}
</style>
</body>
</html>
