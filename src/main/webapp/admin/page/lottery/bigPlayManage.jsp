<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
					<input type="hidden" id="lottype" value="1">
					<div class="input-group">
						<select class="form-control" id="lotteryVersion" onchange="getLotterGroup()">
							<option value="1">第一版玩法</option>
							<option value="2">第二版玩法</option>
							<option value="3">第三版玩法(d站奖金模式)</option>
							<option value="4">第四版玩法</option>
							<option value="5">第五版玩法(d站赔率模式)</option>
						</select>
					</div>
					<button class="btn btn-primary" onclick="newCz();">新增</button>
				</div>
			</div>
		</div>
		<ul class="nav nav-tabs czGroup" id="lotteryType">
<!-- 			<li class="active"><a href="#" data-toggle="tab" class="lotName" -->
<!-- 				id="1">系统彩</a></li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="2">时时彩</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="3">pk10</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="4">排列三</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="5">11选5</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="6">香港彩</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="7">PC蛋蛋</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="8">pk10(第二版)</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="9">时时彩(第二版)</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="10">快三(第二版)</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="11">PC蛋蛋(第二版)</a> -->
<!-- 			</li> -->
<!-- 			<li><a href="#" data-toggle="tab" class="lotName" id="12">快乐十分</a> -->
<!-- 			</li> -->
		</ul>
		<table id="datagrid_tb"></table>
	</div>


	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">玩法大类修改</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">玩法分组名称：</td>
								<td width="25%" class="text-left"><input id="fzName"
									class="form-control" type="text" /></td>
								<td width="25%" class="text-right">序号：</td>
								<td width="25%" class="text-left"><input id="fzSortNo"
									class="form-control" type="text" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="editor();">保存</button>

				</div>
			</div>
		</div>
	</div>

	<!-- 新增 -->
	<div class="modal fade" id="newModel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">玩法大类新增</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">彩票类型：</td>
								<td width="25%" class="text-left"><select id="lotTypeNew"
									class="form-control">
<!-- 										<option value="1" selected="selected">系统彩</option> -->
<!-- 										<option value="2">时时彩</option> -->
<!-- 										<option value="3">pk10</option> -->
<!-- 										<option value="4">排列三</option> -->
<!-- 										<option value="5">11选5</option> -->
<!-- 										<option value="7">PC蛋蛋</option> -->
<!-- 										<option value="8">pk10(第二版)</option> -->
<!-- 										<option value="9">时时彩(第二版)</option> -->
<!-- 										<option value="10">快三(第二版)</option> -->
<!-- 										<option value="11">PC蛋蛋(第二版)</option> -->
<!-- 										<option value="12">快乐十分</option> -->
								</select></td>
								<td width="25%" class="text-right">玩法分组名称：</td>
								<td width="25%" class="text-left"><input id="nameNew"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">彩票编码：</td>
								<td width="25%" class="text-left"><input id="codeNew"
									class="form-control" type="text" /></td>
								<td width="25%" class="text-right">序号：</td>
								<td width="25%" class="text-left"><input id="sortNoNew"
									class="form-control" type="text" /></td>
							</tr>
							<tr>
								<td width="25%" class="text-right">状态：</td>
								<td width="25%" class="text-left">
									<!-- 								<input id="statusNew" class="form-control" type="text"/>  -->
									<select id="statusNew" class="form-control">
										<option value="2">启用</option>
										<option value="1">停用</option>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="save();">保存</button>

				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$(function() {
			getLotterGroup();
		})
		
		function getLotterGroup(){
			var identify = $('#lotteryVersion').val();
			$.ajax({
				url : "${base}/admin/lottery/list.do",
				data : {
					"identify":identify,
					"stationId":0,
					"rows":100,
					"page":1
					},
				success : function(result) {
					var lType = new Array();
					var d = '';
					var c = '';
					var type='';
					for(var i=0;i<result.rows.length;i++){
						if(lType.indexOf(result.rows[i].type) == -1){
							lType[i]=result.rows[i].type;
							type = showCZ(result.rows[i].type);
							if(i==0){
								d+='<li class="active"><a href="#" data-toggle="tab" class="lotName" id="'+result.rows[i].type+'">'+type+'</a></li>';
								c+='<option value="'+result.rows[i].type+'" selected="selected">'+type+'</option>';
								$("#lottype").val(result.rows[i].type);
								continue;
							}
							d+='<li><a href="#" data-toggle="tab" class="lotName" id="'+result.rows[i].type+'">'+type+'</a></li>';
							c+='<option value="'+result.rows[i].type+'">'+type+'</option>';
						}
					}
					$("#lotteryType").html(d);
					getTab();
					refresh();
					$('.lotName').click(function() {
						$("#lottype").val(this.id);
						refresh();
					});
					$("#lotTypeNew").html(c);
				}
			});
		}
		
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/lotTypeGroup/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'lotType',
					title : '彩票类型',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : showCZ
				}, {
					field : 'name',
					title : '玩法分组名称',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'code',
					title : '彩票编码',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'sortNo',
					title : '序号',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '50',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {

				$("#editmodel").modal('toggle');
				$("#fzName").val(row.name);
				$("#fzSortNo").val(row.sortNo);
				$("#playId").val(row.id);
			},
			'click .stateOpen' : function(e, value, row, index) {
				closeOrOpen(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				closeOrOpen(row);
			}
		};

		function newCz() {
			$("#newModel").modal('toggle');
		}

		function save() {
			var lotType = $('#lotTypeNew').val();
			var name = $('#nameNew').val();
			var code = $('#codeNew').val();
			var sortNo = $('#sortNoNew').val();
			var status = $('#statusNew').val();
			if (lotType == "") {
				tips('彩票类型不能为空!', 1);
				return false;
			}
			if (name == "") {
				tips('玩法名称不能为空!', 1);
				return false;
			}
			if (code == "") {
				tips('彩票编码不能为空!', 1);
				return false;
			}
			if (sortNo == "") {
				tips('序号不能为空!', 1);
				return false;
			}
			var bc = {
				"lotType" : lotType,
				"name" : name,
				"code" : code,
				"sortNo" : sortNo,
				"status" : status
			};
			$.ajax({
				url : "${base}/admin/lotTypeGroup/save.do",
				data : bc,
				success : function(result) {
					tips('保存成功!', 1);
					refresh();
				}
			});
		}

		function editor() {
			var bc = {
				"name" : $('#fzName').val(),
				"id" : $('#playId').val(),
				"sortNo" : $('#fzSortNo').val()
			};
			$.ajax({
				url : "${base}/admin/lotTypeGroup/update.do",
				data : bc,
				success : function(result) {
					tips('更新成功!', 1);
					refresh();
				}
			});
		}

		function closeOrOpen(row) {
			var state = 1;
			if (row.status == 1) {
				state = 2;
			}
			var bc = {
				"status" : state,
				"id" : row.id,
				"stationId" : row.stationId
			};
			$.ajax({
				url : "${base}/admin/lotTypeGroup/closeOrOpen.do",
				data : bc,
				success : function(result) {
					if (row.status == 1) {
						tips(row.name, 2);
					} else {
						tips(row.name, 3);
					}
					refresh();
				}
			});
		}

		//设置传入参数
		function queryParams(params) {
			params['lotType'] = $("#lottype").val();
			params['stationId'] = 0;
			return params
		}
		
		function search() {
			getLotterGroup();
			refresh();
		}
	</script>
</body>
</html>
