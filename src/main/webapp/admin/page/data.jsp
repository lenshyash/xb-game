<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
		<div class="form-group">
			<div class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" id="startDate" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="input-group">
					<input type="text" class="form-control" id="endDate" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="input-group">
					<select class="form-control" id="dstationId">
						<option value="">所有站点</option>
						<c:forEach items="${stationList1 }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
					</select>
				</div>
				<button id="start_btn" class="btn btn-primary" onclick="start();">开始执行</button>
			</div>
		</div>
	</div></div>
	<script language="javascript">
		$(function() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#startDate').datetimepicker(options);
			$('#endDate').datetimepicker(options);
		});
		function start() {
			$("#start_btn").attr("disabled", "disabled");
			$("#start_btn").html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="56" height="20">');
			var startDate= $('#startDate').val(),
				endDate=$('#endDate').val();
			var index=layer.confirm("确定要执行"+startDate+"至"+endDate+"数据合并？",function(){
				layer.close(index);
				$.ajax({
					url : "${base}/admin/station/accountDailyMoneyMerge.do",
					data : {
						startDate : startDate,
						endDate:endDate,
						stationId:$("#dstationId").val()
					},
					success : function(result) {
						layer.msg(result.msg,{icon:6});
					},
					complete : function() {
						$("#start_btn").removeAttr("disabled");
						$("#start_btn").html("开始执行");
					}
				});
			});
		}
	</script>
</body>
</html>