<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<table id="datagrid_tb"></table>
	</div>
	<!-- 新增 -->
	<div class="modal fade bs-example-modal-lg" id="updateModel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog"><form action="${base}/admin/cert/upload.do" class="form-submit" method="post" enctype="multipart/form-data" id="updateCertFormId">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">编辑证书</h4>
				</div> <input type="hidden" id="certId" name="certId">
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="margin-bottom:0px">
						<tbody>
							<tr>
								<td width="30%" class="text-right media-middle">证书1：</td>
								<td><input name="crt1" class="form-control required" type="file"/></td>
							</tr>
							<tr>
								<td class="text-right media-middle">证书2：</td>
								<td><input name="crt2" class="form-control required" type="file"/></td>
							</tr>
							<tr>
								<td class="text-right media-middle">key文件：</td>
								<td><input name="keyFile" class="form-control required" type="file"/></td>
							</tr>
							<tr>
								<td class="text-right media-middle">csr文件：</td>
								<td><input name="csrFile" class="form-control required" type="file"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveFn();">保存</button>
				</div>
			</form></div>
		</div>
	</div>
	<!-- 查看 -->
	<div class="modal fade bs-example-modal-lg" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">查看证书csr内容</h4>
				</div>
				<div class="modal-body"><textarea style="height:400px;width:867px;" id="viewModelContent" readonly></textarea></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	$(function() {
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/cert/list.do',
			toolbar : $('#toolbar'),
			sidePagination: 'client',
			pageSize:50,
			columns : [{
				field : 'id',
				title : '编号',
				align : 'center'
			}, {
				field : 'name',
				title : '名称',
				align : 'center'
			}, {
				field : 'fileName',
				title : '证书文件名',
				align : 'center'
			}, {
				field : 'keyFile',
				title : 'key文件',
				align : 'center',
				formatter:keyFileFormatter
			}, {
				field : 'csrFile',
				title : 'csr文件',
				align : 'center',
				events : operateEvents,
				formatter:csrFileFormatter
			}, {
				title : '操作',
				align : 'center',
				events : operateEvents,
				formatter: operatorFormatter
			}]
		});
	});
	function keyFileFormatter(value, row, index) {
		if(value){
			return '<span class="label label-primary">存在</span>';
		}
		return '<span class="label label-default">不存在</span>';
	}
	function csrFileFormatter(value,row,index){
		if(value){
			return '<a class="viewCsr" href="javascript:void(0)" title="查看">'+value+'</a>';
		}
		return '<span class="label label-default">不存在</span>';
	}
	function operatorFormatter(value, row, index) {
		return '<a class="edit" href="javascript:void(0)" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	
	window.operateEvents = {
		'click .edit' : function(e, value, row, index) {
			$("#updateModel").modal('toggle');
			$("#certId").val(row.id);
		},
		'click .viewCsr' : function(e, value, row, index) {
			$.get("${base}/admin/cert/viewCsr.do?id="+row.id,function(text){
				$("#viewModel").modal('toggle');
				$("#viewModelContent").html(text);
			},"html");
		}
	};
	function saveFn(){
		var $form=$("#updateCertFormId");
		$.ajax({
			type:'POST',
			url:"${base}/admin/cert/upload.do",
			data:new FormData($form[0]),
			dataType:"json",
			cache: false,
			contentType : false,
			processData : false,
			success: function(json){
				$form[0].reset();
				layer.msg(json.msg||"上传成功");
			},
			error: function(){
				layer.msg('网络异常');
			}
		});
	}
</script>
</body>
</html>
