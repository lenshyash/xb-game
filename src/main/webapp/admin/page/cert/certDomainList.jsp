<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="stationFolder">
							<option value="">选择站点</option>
							<c:forEach items="${stations}" var="s"><option value="${s.floder}">${s.floder}</option></c:forEach>
						</select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="domainName" placeholder="域名">
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="certId" placeholder="证书编号">
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" type="button" onclick="setOwnCert();">一键配置证书</button>
				<button class="btn btn-primary" type="button" onclick="setHttps();">一键配置跳转</button>
				<button class="btn btn-primary" type="button" onclick="save();">保存</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
<script type="text/javascript">
var certDomains=null;
$(function() {
	$.ajax({
		url:'${base}/admin/certDomain/list.do',
		dataType:"json",
		type:"POST",
		success: function(data){
			certDomains=data;
			data = [];
			var item=null;
			for(var i=0,len=certDomains.length;i<len;i++){
				item = certDomains[i];
				if(item.domainName.indexOf("yb876.com")>-1){
					continue;
				}
				data.push(item);
			}
			new Game.Table({
				id : 'datagrid_tb',
				data : data,
				toolbar : $('#toolbar'),
				sidePagination: 'client',
				showRefresh:false,
				showToggle:false,
				pageSize:data.length,
				columns : [{
					field : 'domainName',
					title : '域名',
					align : 'center'
				}, {
					field : 'folder',
					title : '编号',
					align : 'center'
				}, {
					field : 'certId',
					title : '所属证书',
					align : 'center',
					formatter: certIdFormatter
				}, {
					field : 'ownCertId',
					title : '配置证书',
					align : 'center',
					formatter: ownCertIdFormatter
				}, {
					field : 'https',
					title : 'HTTPS跳转',
					align : 'center',
					formatter: httpsFormatter
				}, {
					title : '操作',
					align : 'center',
					events : operateEvents,
					formatter: operatorFormatter
				}]
			});
		}
	});
});
function httpsFormatter(value, row, index) {
	if(value==true){
		return '<span class="label label-primary">是</span>';
	}
	return '<span class="label label-default">否</span>';
}
function certIdFormatter(value,row,index){
	if(!value){
		return "-";
	}
	if(row.ownCertId && row.ownCertId!=value){
		return '<span class="label label-warning">'+value+'</span>';
	}
	return '<span class="label label-primary">'+value+'</span>';
}
function ownCertIdFormatter(value,row,index){
	if(!value){
		return "-";
	}
	if(row.certId && row.certId!=value){
		return '<span class="label label-success">'+value+'</span>';
	}
	return '<span class="label label-primary">'+value+'</span>';
}
function operatorFormatter(value, row, index) {
	var s="";
	if(row.certId && row.ownCertId==row.certId){
		s= s+'<a class="confUnCert" href="javascript:void(0)" title="取消配置证书">取消配置证书</a>    ';
	}else{
		s= s+'<a class="confCert" href="javascript:void(0)" title="配置证书">配置证书</a>    ';
	}
	if(row.https==true){
		s=s+'<a class="confUnHttps" href="javascript:void(0)" title="配置不跳转">配置不跳转</a>    ';
	}else{
		s=s+'<a class="confHttps" href="javascript:void(0)" title="配置跳转">配置跳转</a>    ';
	}
	s=s+'<a class="del" href="javascript:void(0)" title="删除">删除</a>';
	return s;
}

window.operateEvents = {
	'click .confHttps' : function(e, value, row, index) {
		var item=null;
		for(var i=0,len=certDomains.length;i<len;i++){
			item = certDomains[i];
			if(item.domainName.indexOf("yb876.com")>-1){
				continue;
			}
			if(item.domainName==row.domainName){
				item.https=true;
				search();
				return;
			}
		}
	},
	'click .confUnHttps' : function(e, value, row, index) {
		var item=null;
		for(var i=0,len=certDomains.length;i<len;i++){
			item = certDomains[i];
			if(item.domainName.indexOf("yb876.com")>-1){
				continue;
			}
			if(item.domainName==row.domainName){
				item.https=false;
				search();
				return;
			}
		}
	},
	'click .confUnCert' : function(e, value, row, index) {
		var item=null;
		for(var i=0,len=certDomains.length;i<len;i++){
			item = certDomains[i];
			if(item.domainName.indexOf("yb876.com")>-1){
				continue;
			}
			if(item.domainName==row.domainName){
				item.ownCertId="";
				search();
				return;
			}
		}
	},
	'click .confCert' : function(e, value, row, index) {
		var item=null;
		for(var i=0,len=certDomains.length;i<len;i++){
			item = certDomains[i];
			if(item.domainName.indexOf("yb876.com")>-1){
				continue;
			}
			if(item.domainName==row.domainName){
				item.ownCertId=item.certId;
				search();
				return;
			}
		}
	},
	'click .del' : function(e, value, row, index) {
		var item=null;
		for(var i=0,len=certDomains.length;i<len;i++){
			item = certDomains[i];
			if(item.domainName.indexOf("yb876.com")>-1){
				continue;
			}
			if(item.domainName==row.domainName){
				certDomains.splice(i,1);
				search();
				return;
			}
		}
	}
};
function setOwnCert(){
	var item=null;
	for(var i=0,len=certDomains.length;i<len;i++){
		item = certDomains[i];
		if(item.domainName.indexOf("yb876.com")>-1){
			continue;
		}
		item.ownCertId=item.certId;
	}
	search();
}
function setHttps(){
	var item=null;
	for(var i=0,len=certDomains.length;i<len;i++){
		item = certDomains[i];
		if(item.domainName.indexOf("yb876.com")>-1){
			continue;
		}
		item.https=true;
	}
	search();
}
function search() {
	var data=[],folder=$("#stationFolder").val(),
		domainName=$("#domainName").val().replace(/(^\s*)|(\s*$)/g, ''),
		certId=$("#certId").val().replace(/(^\s*)|(\s*$)/g, '');
	for(var i=0,len=certDomains.length;i<len;i++){
		item = certDomains[i];
		if(item.domainName.indexOf("yb876.com")>-1){
			continue;
		}
		if(folder && folder!=''){
			if(!item.folder || item.folder.indexOf(folder)== -1){
				continue;
			}
		}
		if(domainName && domainName!=''){
			if(!item.domainName || item.domainName.indexOf(domainName)== -1){
				continue;
			}
		}
		if(certId && certId!=''){
			if((!item.certId || item.certId!=certId) && (!item.ownCertId || item.ownCertId!=certId)){
				continue;
			}
		}
		data.push(item);
	}
	$("#datagrid_tb").bootstrapTable('refreshOptions',{data:data});
}

function save(){
	var item=null,msg="";
	for(var i=0,len=certDomains.length;i<len;i++){
		item = certDomains[i];
		if(item.domainName.indexOf("yb876.com")>-1){
			continue;
		}
		if(!item.folder){
			msg=msg+"域名："+item.domainName+" 没有对应的站点<br>";
		}
		if(item.certId!=item.ownCertId){
			msg=msg+"域名："+item.domainName+" 证书不一致<br>";
		}
	}
	if(msg!=""){
		layer.confirm(msg, {icon: 3, title:'提示'}, function(index){
			$.ajax({
				url:'${base}/admin/certDomain/save.do',
				dataType:"json",
				type:"POST",
				data:{data:JSON.stringify(certDomains)},
				success: function(data){
					layer.msg(data.msg||'保存成功');
				}
			});
		});
	}else{
		$.ajax({
			url:'${base}/admin/certDomain/save.do',
			dataType:"json",
			type:"POST",
			data:{data:JSON.stringify(certDomains)},
			success: function(data){
				layer.msg(data.msg||'保存成功');
			}
		});
	}
}
</script>
</body>
</html>
