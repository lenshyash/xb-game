<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<button class="btn btn-primary" type="button" onclick="viewDefault(0);">查看默认库</button>
			<button class="btn btn-primary" type="button" onclick="showDeleteCache();">删除缓存</button>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="cachemodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">清理缓存</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">库：</td>
								<td width="80%"><input type="text" class="form-control" name="db" id="cache_db"/></td>
							</tr>
							<tr>
								<td class="text-right">key：</td>
								<td><input type="text" class="form-control" name="key" id="cache_key" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteCacheSubmit();">删除</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="viewemodel" tabindex="-1" role="dialog" aria-labelledby="viewemodelLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="viewemodelLabel"></h4>
				</div>
				<div class="modal-body"><div  style="height:500px;overflow:auto;">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="50%">key</td>
								<td width="50%">操作</td>
							</tr>
						</tbody><tbody id="viewemodeltr">
						</tbody>
					</table>
				</div></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteCacheSubmit();">删除</button>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/cache/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '名称',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'key',
					title : '键值',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'db',
					title : '数据库',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'timeout',
					title : '过期时间(秒)',
					align : 'center',
					valign : 'middle'
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="detail" href="javascript:void(0)" title="查看"><i class="glyphicon glyphicon-eye-open"></i></a>  ',
					'<a class="flushdb" href="javascript:void(0)" title="清空"><i class="glyphicon glyphicon-refresh"></i></a>' ]
					.join('');
		}
		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				viewDefault(row.db);
			},
			'click .flushdb' : function(e, value, row, index) {
				$.ajax({
					url : "${base}/admin/cache/flushCache.do",
					data : {db:row.db},
					success : function(result) {
						layer.msg(result.msg||"缓存已清理");
					}
				}); 
			}
		};
		//设置传入参数
		function queryParams(params) {
			params[$("#methodType").val()] = $("#keyword").val();
			return params
		}
		$(function() {
			getTab();
		})
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
		function viewDefault(db){
			$("#viewemodelLabel").html("查看缓存：库"+db);
			$.ajax({
				url : "${base}/admin/cache/getCacheList.do",
				data : {db:db},
				success : function(json) {
					var h='';
					if(json && json.length){
						for(var i=0;i<json.length;i++){
							h+='<tr><td>'+json[i]+'</td><td><a href="javascript:void(0)" onclick="viewDetail('+db+',\''
								+json[i]+'\')">查看</a>&nbsp; &nbsp;<a href="javascript:void(0)" onclick="deleteCache('+db
								+',\''+json[i]+'\',this)">删除</a></td></tr>';
						}
					}else{
						h='<tr><td colspan="2">该库没有缓存数据</td></tr>';
					}
					$("#viewemodeltr").html(h);
					$("#viewemodel").modal('toggle');
				}
			});
		}
		function showDeleteCache(){
			$("#cachemodel").modal('toggle');
		}
		function deleteCacheSubmit(){
			var db=$("#cache_db").val(),key=$("#cache_key").val();
			deleteCache(db,key);
		}
		function deleteCache(db,key,obj){
			if(db==undefined || db-0<0||db-0>16){
				layer.msg("请输入缓存库，库只能是0～16的数字");
				return false;
			}
			if(!key){
				layer.msg("请输入缓存key");
				return false;
			}
			$.ajax({
				url : "${base}/admin/cache/delCache.do",
				data : {db:db,key:key},
				success : function(result) {
					layer.msg(result.msg||"缓存已清理");
					if(result.success && obj){
						$(obj).parent().parent().remove();
					}
				}
			}); 
		}
		function viewDetail(db,key){
			$.ajax({
				url : "${base}/admin/cache/getCacheContext.do",
				data : {db:db,key:key},
				success : function(result) {
					var c=result.content;
					if(c && c.indexOf("<")>-1){
						c=c.replace(/</g,"&lt;");
					}
					layer.alert(c);
				}
			}); 
		}
	</script>
</body>
</html>