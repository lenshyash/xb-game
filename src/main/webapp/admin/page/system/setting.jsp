<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}

.secCheckBox {
	margin-left: 80px;;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="settings"></div>
	</div>

	<script id="config_tpl" type="text/html">
		{{each gps as gp}}
		 	<div class="panel panel-default">
				<div class="panel-heading">{{gp.name}}</div>
				<div class="panel-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							{{each gp.sons as conf}}
								<tr>
									<td width="30%" class="text-center">{{conf.name}}</td>
									<td width="70%"><a href="#" id="{{conf.id}}" data-type="{{conf.type}}" data-value="{{conf.initValue}}" data-pk="{{conf.id}}" data-title="{{conf.title}}" data-source="{{conf.source}}"></a></td>
								</tr>
							{{/each}}
						</tbody>
					</table>
				</div>
			</div>
		{{/each}}
	</script>
	<script>
	$(function() {
		loadConf();
	});

	/**
	 * 加载设置内容
	 */
	function loadConf() {
		$.ajax({
			url : "${base}/admin/setting/settings.do",
			success : function(data) {

				handlerData(data);
				initXeditable(data);
			}
		});
	}

	function initXeditable(data) {
		var where = $("#settings");
		var options = {
			emptytext : '无配置',
			params : setParams,
			placement : 'right',
			url : "${base}/admin/setting/saveval.do"
		}
		var html = template('config_tpl', data);
		where.html(html);

		where.find("a").each(function() {

			var type = $(this).attr("data-type");
			var source = $(this).attr("data-source");
			var val = $(this).attr("data-value");

			if (val !== undefined && val !== "") {
				$(this).html(val.replace(/</g,"&lt;"));
			}

			if (type === 'select') {
				options.source = source;
				var arr = evil(source);
				for (var i = 0; i < arr.length; i++) {
					var obj = arr[i];
					if (obj[val] !== undefined) {
						$(this).html(obj[val]);
						break;
					}
				}
			} else if (type === 'date') {
				options.template = 'yyyy-mm-dd';
				options.format = 'yyyy-mm-dd';
				options.viewformat = 'yyyy-mm-dd';
				options.language = 'zh-CN';
				options.datepicker = {
					todayBtn : 'linked',
					weekStart : 1,
					startView : 0,
					language : 'zh-CN',
					minViewMode : 0,
					autoclose : false
				}
			} else if (type === 'datetime') {
				var fmt = 'yyyy-mm-dd hh:ii';
				var timeOpts = {
					language : 'zh-CN',
					todayBtn : 'linked',
					weekStart : 1,
					minViewMode : 0,
					autoclose : false
				};
				if (source) {
					var src = evil("(" + source + ")");
					for ( var key in src) {
						if (key == "format") {
							fmt = src[key];
						} else {
							timeOpts[key] = src[key];
						}
					}
				}
				options.template = fmt;
				options.format = fmt;
				options.viewformat = fmt;
				options.datetimepicker = timeOpts;

			} else if (type === 'combodate') {
				options.template = 'YYYY-MM-DD HH:mm:ss';
				options.format = 'YYYY-MM-DD HH:mm:ss';
				options.viewformat = 'YYYY-MM-DD HH:mm:ss';
				options.datetimepicker = {
					language : 'zh-CN',
					todayBtn : 'linked',
					weekStart : 1
				}
			} else if (type === 'textarea') {
				options.placement = 'bottom';
			} else if (type === 'checklist') {
				$(this).html("");
				options.source = source;
				var arr = evil(source);
				var checks = val.split(",");
				for (var i = 0; i < checks.length; i++) {
					var ck = checks[i];
					for (var j = 0; j < arr.length; j++) {
						var obj = arr[j];
						if (obj[ck] !== undefined) {
							$(this).append(obj[ck]);
							$(this).append("<br>");
							break;
						}
					}
				}
			}
			$(this).editable(options);
		});
	}

	function handlerData(data) {

		var gps = [];
		var groupMap = {
			0 : {
				id : 0,
				name : "其他",
				sons : []
			}
		};
		for (var i = 0; i < data.confs.length; i++) {
			var c = data.confs[i];
			if (c.groupId) {
				if (groupMap[c.groupId]) {
					groupMap[c.groupId].sons.push(c);
				} else {
					var gp = {
						id : c.groupId,
						name : c.groupName,
						sons : []
					};
					groupMap[c.groupId] = gp;
					gp.sons.push(c);
					gps.push(gp);
				}

			} else {
				groupMap[0].sons.push(c);
			}
		}
		if (data.confs.length > 0 && (gps.length == 0)) {
			gps.push(groupMap[0]);
		}
		data.gps = gps;
		return data;
	}

	function setParams(params) {
		if ($(this).attr("data-type") === 'checklist') {
			params.value = params.value.join(",");
		}
		params.id = $(this).attr("id");
		return params;
	}
	// 重写 eval
	function evil(fn) {
		try {
			var Fn = Function;  //一个变量指向Function，防止有些前端编译工具报错
			return new Fn('return ' + fn)();
		} catch (e) {
			console.log(e)
		}
	}
	</script>
</body>
</html>