<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="spayName">支付平台名称</label> <label class="sr-only" for="stype">支付类型</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="stype">
							<option value="0">全部类型</option>
							<option value="5">在线支付</option>
							<option value="6">快速入款</option>
							<option value="7">一般入款</option>
							<option value="801">虚拟币支付</option>
						</select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="spayName" placeholder="支付平台名称">
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" onclick="add();">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑域名</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="ppId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">支付平台类型：</td>
								<td width="35%" class="text-left"><select id="type" class="form-control">
										<option value="5">在线支付</option>
										<option value="6">快速入款</option>
										<option value="7">一般入款</option>
										<option value="801">虚拟币支付</option>
								</select></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">支付平台名称：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="name" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">支付平台公司：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="company" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">显示图标样式：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="iconCss" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">状态：</td>
								<td width="35%" class="text-left"><select id="status" class="form-control">
										<option value="1">禁用</option>
										<option value="2">启用</option>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script id="stationlst_tpl" type="text/html">
	{{each data as st}}
			<option value="{{st.id}}">{{st.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/payplatform/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '支付平台名称',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'company',
					title : '支付平台公司',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'type',
					title : '支付类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'iconCss',
					title : '图标样式',
					align : 'center',
					valign : 'bottom'
				}, {
					field : 'status',
					title : '站点状态',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function typeFormatter(value, row, index) {

			return GlobalTypeUtil.getTypeName(1, 1, value);
		}

		function statusFormatter(value, row, index) {
			if (value === 2) {
				return '<span class="text-success">启用</span><a href="#"><span class="text-danger stateClose">(禁用)</span></a>';
			}
			return '<span class="text-danger">禁用</span><a href="#"><span class="text-success stateOpen">(启用)</span></a>';
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
					'<a class="remove" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {

				$("#editmodel").modal('toggle');
				$("#ppId").val(row.id);
				$("#name").val(row.name);
				$("#company").val(row.company);
				$("#iconCss").val(row.iconCss);
				$("#status").val(row.status);
				$("#type").val(row.type);
			},
			'click .stateOpen' : function(e, value, row, index) {
				updStatus(row.id, 2);
			},
			'click .stateClose' : function(e, value, row, index) {
				updStatus(row.id, 1);
			},
			'click .remove' : function(e, value, row, index) {
				remove(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params["type"] = $("#stype").val();
			params["payName"] = $("#spayName").val();
			return params
		}
		$(function() {
			getTab();
		})

		function updStatus(id, status) {
			$.ajax({
				url : "${base}/admin/payplatform/updstatus.do",
				data : {
					id : id,
					status : status
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function remove(row) {
			$.ajax({
				url : "${base}/admin/payplatform/delete.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function add() {
			$("#editmodel").modal('toggle');
			$("#ppId").val("");
			$("#name").val("");
			$("#iconCss").val("");
			$("#status").val("");
			$("#type").val("");
			$("#company").val("");
		}

		function save() {
			$.ajax({
				url : "${base}/admin/payplatform/save.do",
				data : {
					id : $("#ppId").val(),
					name : $("#name").val(),
					company : $("#company").val(),
					iconCss : $("#iconCss").val(),
					status : $("#status").val(),
					type : $("#type").val()
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
	</script>
</body>
</html>