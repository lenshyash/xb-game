<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
					<button class="btn btn-primary" onclick="newUser();">新增</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<!-- 新增用户 -->
	<div class="modal fade bs-example-modal-lg" id="addmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<input type="hidden" id="accountId" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">新增用户</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">用户账号：</td>
								<td width="35%" class="text-left"><input name="account" id="account" class="form-control" type="text"></span></td>
								<td width="15%" class="text-right">用户分组：</td>
								<td width="35%" class="text-left"><select name="groupName" id="groupId" class="form-control"></select></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">密码：</td>
								<td width="35%" class="text-left"><input name="pwd" id="pwd" class="form-control" type="password" /></td>
								<td width="15%" class="text-right">确认密码：</td>
								<td width="35%" class="text-left"><input name="rpwd" id="rpwd" class="form-control" type="password" /></td>
							</tr>

							<tr>
								<td width="15%" class="text-right">状态：</td>
								<td width="35%" class="text-left"><select id="status" class="form-control" name="status">
										<option value="1">禁用</option>
										<option value="2">启用</option>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script id="grouplst_tpl" type="text/html">
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>

	<script type="text/javascript">
		$(function() {
			getTab();
			initGroup();
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/user/list.do',
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '用户账号',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, {
					field : 'creator',
					title : '创建者',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'modifyUser',
					title : '最后修改者',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'groupName',
					title : '组别',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '80',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		//组别
		function initGroup() {
			$.ajax({
				url : "${base}/admin/dictionary/user/group.do",
				success : function(result) {
					var eachdata = {
						"data" : result
					};
					var html = template('grouplst_tpl', eachdata);
					$("#groupId").html(html);
				}
			});
		}

		function newUser() {
			$("#addmodel").modal('toggle');
			$("#account").val("");
			$("#groupId").val("");
			$("#status").val("");
			$("#accountId").val("");
			$("#pwd").val("");
			$("#rpwd").val("");
		}

		function statusFormatter(value, row, index) {
			if (value === 2) {
				return '<span class="text-success">启用</span><a href="#"><span class="text-danger stateClose">(禁用)</span></a>';
			}
			return '<span class="text-danger">禁用</span><a href="#"><span class="text-success stateOpen">(启用)</span></a>';
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a> ',
					'<a class="del" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a> ' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				$("#addmodel").modal('toggle');
				$("#account").val(row.account);
				$("#groupId").val(row.groupId);
				$("#status").val(row.status);
				$("#accountId").val(row.id);
				$("#pwd").val("");
				$("#rpwd").val("");

			},
			'click .del' : function(e, value, row, index) {
				del(row);
			},
			'click .stateOpen' : function(e, value, row, index) {
				closeOrOpen(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				closeOrOpen(row);
			}
		};

		function closeOrOpen(row) {
			$.ajax({
				url : "${base}/admin/user/closeOrOpen.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function del(value) {
			Msg.confirm("是否删除用户" + value.account + "?", function() {
				$.ajax({
					url : "${base}/admin/user/del.do",
					data : value,
					success : function(result) {
						$("#datagrid_tb").bootstrapTable('refresh');
					}
				});
			});
		}

		//保存
		function save() {
			var password = $("#pwd").val();
			var rpassword = $("#rpwd").val();
			var accountId = $("#accountId").val();
			if (!accountId) {
				if (!password) {
					Msg.info("新密码不能为空！");
					return;
				}
				if (!rpassword) {
					Msg.info("确认密码不能为空！");
					return;
				}
			}

			if (password !== rpassword) {
				Msg.info("两次密码不一致！");
				return;
			}
			$.ajax({
				url : "${base}/admin/user/save.do",
				data : {
					id : accountId,
					account : $("#account").val(),
					groupId : $("#groupId").val(),
					status : $("#status").val(),
					pwd : password,
					rpwd : rpassword
				},
				success : function(result) {
					Msg.info("操作成功！");
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
