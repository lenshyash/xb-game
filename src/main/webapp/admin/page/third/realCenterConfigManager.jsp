<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="keyword">租户前缀</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text"  name="tenantName" class="form-control" id="keyword" placeholder="真人中心租户前缀">
					</div>
				</div>
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				<button class="btn btn-primary" id="newAddButton" onclick="javascript:newRcc();">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<!-- 新增 -->
	<div class="modal fade bs-example-modal-lg" id="newModel"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">新增真人中心配置</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="accountId">
					<table class="table table-bordered table-striped" id="AddTable" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">配置站点：</td>
								<td width="35%" class="text-left">
									<select name="stationId" id="stationIdNew"></select>
								</td>
								<td width="15%" class="text-right">真人中心apiUrl：</td>
								<td width="35%" class="text-left">
									<input name="realCenterApiUrl" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">真人中心租户ID：</td>
								<td width="35%" class="text-left">
									<input name="tenantId" class="form-control" type="text"/> 
								</td>
								<td width="15%" class="text-right">租户名称(前缀)：</td>
								<td width="35%" class="text-left">
									<input name="tenantName" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">租户秘钥：</td>
								<td width="35%" class="text-left">
									<input name="tenantSecret" class="form-control" type="text"/> 
								</td>
								<td width="15%" class="text-right">配置状态：</td>
								<td width="35%" class="text-left">
									<select name="status"
										class="form-control">
											<option value="2" selected="selected">启用</option>
											<option value="1">禁用</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
<!-- 	修改 -->
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">真人中心租户修改</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="accountId">
					<table class="table table-bordered table-striped" style="clear: both" id="UpdateTable">
						<tbody>
							<tr>
								<td width="15%" class="text-right">真人中心apiUrl：</td>
								<td width="35%" class="text-left" colspan="3">
									<input name="realCenterApiUrl" id="realCenterApiUrlUpdate" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">真人中心租户ID：</td>
								<td width="35%" class="text-left">
									<input name="tenantId" class="form-control" id="tenantIdUpdate" type="text"/> 
								</td>
								<td width="15%" class="text-right">租户名称(前缀)：</td>
								<td width="35%" class="text-left">
									<input name="tenantName" class="form-control" id="tenantNameUpdate" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">租户秘钥：</td>
								<td width="35%" class="text-left" colspan="3">
									<input name="tenantSecret" class="form-control" id="tenantSecretUpdate" type="text"/> 
									<input name="id"  id="idUpdate" class="form-control" type="hidden"/> 
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			getTab();
		})
		//新增
		function newRcc(){
			$("#newModel").modal('toggle');
			stationList('stationIdNew');
		}
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/rc/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'floder',
					title : '所属网站目录',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, {
					field : 'realCenterApiUrl',
					title : '真人中心apiurl',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'tenantId',
					title : '真人中心租户id',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'tenantName',
					title : '真人中心租户名称',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'tenantSecret',
					title : '租户秘钥',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'modifyDatetime',
					title : '修改时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				},  {
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}
		//保存
		function save(){
			var param = {};
			$("#AddTable").find("input").each(function(i){
				var name = $(this).attr("name");
				var val = $(this).val();
				param[name]=val;
			});
			$("#AddTable").find("select").each(function(i){
				var name = $(this).attr("name");
				var val = $(this).val();
				param[name]=val;
			});
			$.ajax({
				url : "${base}/admin/rc/save.do",
				data : param,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
			
		}
		
		function editor(){
			var param = {};
			$("#UpdateTable").find("input").each(function(i){
				var name = $(this).attr("name");
				var val = $(this).val();
				param[name]=val;
			});
			$("#UpdateTable").find("select").each(function(i){
				var name = $(this).attr("name");
				var val = $(this).val();
				param[name]=val;
			});
			
			$.ajax({
				url : "${base}/admin/rc/update.do",
				data : param,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="edit" href="javascript:void(0)" title="修改">',
						'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
						'<a class="del" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>  ']
					.join('');
		}
		
		//状态
		function statusFormatter(value, row, index) {
			if (value == 2) {
				return '<span class="text-success">启用</span><a href="#"><span class="text-success disable" title="点击禁用">禁用</span></a>';
			}
			return '<span class="text-danger">禁用</span><a href="#"><span class="text-danger enable" title="点击启用">启用</span></a>';
		}
		window.operateEvents = {
			'click .edit' : function(e, value, row, index) {
								$("#editmodel").modal('toggle');
								$("#realCenterApiUrlUpdate").val(row.realCenterApiUrl);
								$("#tenantIdUpdate").val(row.tenantId);
								$("#tenantNameUpdate").val(row.tenantName);
								$("#tenantSecretUpdate").val(row.tenantSecret);
								$('#idUpdate').val(row.id);
								stationList('stationIdUpdate');
			},
			'click .enable' : function(e, value, row, index) {
				enable(row);
			},
			'click .disable' : function(e, value, row, index) {
				disable(row);
			},
			'click .del' : function(e, value, row, index) {
				del(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params["tenantName"] = $("#keyword").val();
			return params
		}
		
		//站点列表
		function stationList(id) {
			var col='';
			$.ajax({
				url : "${base}/admin/station/list.do",
				success : function(result) {
					for(var j in result.rows){
						console.log(result.rows[j].id+"||"+result.rows[j].name);
						col+='<option value="'+result.rows[j].id+'">'+result.rows[j].name+'('+result.rows[j].floder+')</option>';
					}
					$('#'+id).html(col);
				}
			});
		}
		
		function enable(row) {
			$.ajax({
				url : "${base}/admin/rc/enable.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function disable(row) {
			$.ajax({
				url : "${base}/admin/rc/disable.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function del(row) {
			$.ajax({
				url : "${base}/admin/rc/del.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
