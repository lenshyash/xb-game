<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="keyword">三方账户名称</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="username"  class="form-control" id="username" placeholder="三方账户名称">
					</div>
				</div>
				<label class="sr-only" for="keyword">本地账户名称</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="account"  class="form-control" id="account" placeholder="本地账户名称">
					</div>
				</div>
				<label class="sr-only" for="keyword">三方游戏类型</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="gameType" id="gameType">
							<option value="">选择游戏类型</option>
							<option value="1">AG</option>
							<option value="2">BBIN</option>
							<option value="3">MG</option>
						</select>
					</div>
				</div>
				
				<label class="sr-only" for="keyword">选择租户</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="stationId" id="stationId">
							<option value="">选择租户</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			getTab();
			stationList('stationId');
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/ruser/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'floder',
					title : '所属网站目录',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, 
				 {
					field : 'gameType',
					title : '三方游戏类型',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:gameTypeFormatter
				},{
					field : 'username',
					title : '三方账户名称',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'account',
					title : '本地系统账户',
					align : 'center',
					width : '200',
					valign : 'middle'
				},{
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				},{
					field : 'modifyDatetime',
					title : '更新时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}]
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function gameTypeFormatter(value, row, index) {
			if(value==1){
				return "AG";
			}else if(value==2){
				return "BBIN";
			}else if(value==3){
				return "MG";
			}else{
				return "";
			}
		}
		//设置传入参数search
		function queryParams(params) {
			params["username"] = $("#username").val();
			params["account"] = $("#account").val();
			params["gameType"] = $("#gameType").val();
			params["stationId"] = $("#stationId").val();
			return params
		}
		//站点列表
		function stationList(id) {
			$.ajax({
				url : "${base}/admin/station/list.do",
				success : function(result) {
					var col = "<option value=\"\">选择租户</option>";
					for(var j in result.rows){
						console.log(result.rows[j].id+"||"+result.rows[j].name);
						col+='<option value="'+result.rows[j].id+'">'+result.rows[j].name+'('+result.rows[j].floder+')</option>';
					}
					$('#'+id).html(col);
				}
			});
		}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
