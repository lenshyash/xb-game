<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>
				<label class="sr-only" for="keyword">账户名称</label> 
				<label class="sr-only" for="keyword">解析数据的状态</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="parseStatus" id="parseStatus">
							<option value="0">选择状态</option>
							<option value="1">未解析</option>
							<option value="2">解析完成</option>
							<option value="3">解析失败</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
				</div>
				
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				endDate : curDate,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);
			var t = DateUtil.getCurrentDate();
			setDate(t+" 00:00:00", t+" 23:59:59");
			bindbtn();
			getTab();
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/thirdDataReceive/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'createTime',
					title : '创建时间',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : dateFormatter
				}, {
					field : 'uniqueKey',
					title : '唯一标示',
					align : 'center',
					width : '200',
					valign : 'middle',
				},{
					title : '状态',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:parseStatusFormatter
				 }, {
					title : '操作',
					align : 'center',
					width : '100',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				}]
			});
		}
		function operateFormatter(value, row, index) {
			if(row.status==1){
				return '<a class="edit" href="javascript:void(0)">重新解析</a>';
			}
			return "";
		}
		window.operateEvents = {
			'click .edit' : function(e, value, row, index) {
				Msg.confirm('确定要重新解析【'+row.uniqueKey+'】？', function() {
					$.ajax({
		 				url : "${base}/admin/thirdDataReceive/reparse.do?id="+row.id,
		 				success : function(result) {
		 					layer.msg(result.msg||"解析成功");
		 					search();
		 				}
		 			});
				});
			}
		};
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function parseStatusFormatter(value, row, index) {
			var col = '';
			switch(row.status){
			case 1:
				col = '<span class="label label-primary">未解析</span>';
				break;
			case 2:
				col = '<span class="label label-success">解析完成</span>';
				break;
			case 3:
				col = '<span class="label label-danger">解析中</span>';
				break;
			}
			return col;
		}
		//设置传入参数search
		function queryParams(params) {
			params["status"] = $("#parseStatus").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			return params
		}
		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
				});
			}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
