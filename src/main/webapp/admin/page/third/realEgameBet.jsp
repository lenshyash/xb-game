<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>
				
				<label class="sr-only" for="keyword">账户名称</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="account"  class="form-control" id="account" placeholder="本地账户名称">
					</div>
				</div>
				<label class="sr-only" for="keyword">选择租户</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="stationId" id="stationId">
							<option value="">选择租户</option>
						</select>
					</div>
				</div>
				<label class="sr-only" for="keyword">电子游戏类型</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="gameType" id="gameType">
							<option value="">选择电子游戏类型</option>
							<option value="4">MG电子游戏类型</option>
							<option value="5">AG电子游戏类型</option>
							<option value="6">BBIN电子游戏类型</option>
							<option value="7">AG捕鱼王</option>
							<option value="98">BG电子</option>
						</select>
					</div>
				</div>
				
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
				</div>
				
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			getTab();
			stationList("stationId");
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
		})
		
		
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/rebet/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'floder',
					title : '所属网站目录',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, 
				 {
					field : 'gameType',
					title : '电子游艺游戏类型',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : gameTypeFormat
				},{
					field : 'account',
					title : '注单账户',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'thirdGameType',
					title : '三方游戏类型',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'gameCode',
					title : '三方游戏code',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'betId',
					title : '投注id',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'betMoney',
					title : '投注金额',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'payMoney',
					title : '输赢金额',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:payMoneyFormat
				}, 
				{
					field : 'betTime',
					title : '投注时间(游戏GMT-0)',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : dateFormatter
				}, 
				{
					field : 'bjBetTime',
					title : '投注时间(北京)',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : dateFormatter
				}, 
				{
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}]
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function payMoneyFormat(value, row, index) {
			var f=0;
			if(row["gameType"]!=4){
				f= (value||0)+row['betMoney'];
			}else{
				f= value||0;
			}
			return f;
		}
		
		function gameTypeFormat(value, row, index) {
			if(value==4){
				return "mg电子";
			}else if(value==5){
				return "ag电子";
			}else if(value==6){
				return "bbin电子";
			}else if(value==7){
				return "AG捕鱼王";
			}else if(value==98){
				return "BG电子";
			}else{
				return "";
			}
		}
		//设置传入参数search
		function queryParams(params) {
			params["account"] = $("#account").val();
			params["gameType"] = $("#gameType").val();
			params["stationId"] = $("#stationId").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			return params
		}
		//站点列表
		function stationList(id) {
			$.ajax({
				url : "${base}/admin/station/list.do",
				success : function(result) {
					var col = "<option value=\"\">选择租户</option>";
					for(var j in result.rows){
						console.log(result.rows[j].id+"||"+result.rows[j].name);
						col+='<option value="'+result.rows[j].id+'">'+result.rows[j].name+'('+result.rows[j].floder+')</option>';
					}
					$('#'+id).html(col);
				}
			});
		}
		
		
		$(function(){
			var begin = "";
			var end = "";
			begin = DateUtil.getCurrentDate();
			end = begin;
			setDate(begin, end);
			bindbtn();
		});
		
		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin, end);
				search();
				});
			}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
