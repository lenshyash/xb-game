<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="keyword">关键字</label> <label
					class="sr-only" for="methodType">查询类型</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="methodType">
							<option value="floder">模版名称</option>
							<option value="name">站点名称</option>
							<option value="account">租户账号</option>
						</select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="keyword"
							placeholder="关键字">
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog"
		aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">一键转出余额</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="stationId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">站点名称：</td>
								<td width="35%" class="text-left"><span id="name"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">模板名称：</td>
								<td width="35%" class="text-left"><span id="floder"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">游戏平台：</td>
								<td width="35%" class="text-left"><select
									class="form-control" id="platform">
										<c:forEach items="${platforms }" var="p">
											<option value="${p.value }">${p }</option>
										</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onclick="transOut();">确认</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/station/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '站点名称',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, {
					field : 'account',
					title : '租户超级管理员',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'agentName',
					title : '默认代理',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'floder',
					title : '模板名称',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [
					'<a class="transout" href="javascript:void(0)" title="修改">',
					'一键转出会员余额', '</a>  ' ].join('');
		}

		window.operateEvents = {
			'click .transout' : function(e, value, row, index) {
				$("#editmodel").modal('toggle');
				$("#name").html(row.name);
				$("#stationId").val(row.id);
				$("#floder").html(row.floder);
			}
		};

		function transOut() {
			Msg.confirm('确定要转出该站点所有会员的余额吗', {
				btn : [ '确定' ]
			}, function() {
				$.ajax({
					url : "${base}/admin/realtrans/transout.do",
					data : {
						platform : $("#platform").val(),
						stationId : $("#stationId").val()
					},
					success : function(result) {
						Msg.info("操作成功");
					}
				});
			});
		}

		//设置传入参数
		function queryParams(params) {
			params[$("#methodType").val()] = $("#keyword").val();
			return params
		}
		$(function() {
			getTab();
		})

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>