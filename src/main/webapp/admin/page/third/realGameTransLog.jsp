<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" value="${begin }" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<label class="sr-only" for="keyword">账户名称</label> 
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="account"  class="form-control"
							 id="account" placeholder="本地账户名称">
						</div>
					</div>
					<label class="sr-only" for="keyword">三方游戏类型</label> 
					<div class="form-group">
						<div class="input-group">
							<select class="form-control" name="gameType" id="gameType">
								<option value="" selected>全部类型</option>
								<option value="1">AG</option>
								<option value="2">BBIN</option>
								<option value="3">MG</option>
								<option value="4">QT</option>
								<option value="5">ALLBET</option>
								<option value="6">PT</option>
								<option value="7">OG</option>
								<option value="8">DS</option>
								<option value="9">CQ9</option>
								<option value="11">JDB</option>
								<option value="12">KY</option>
								<option value="10">IBC</option>
								<option value="92">TTG</option>
								<option value="95">MW</option>
								<option value="96">ISB</option>
			                    <option value="991">M8H</option>
			                    <option value="99">M8</option>
			                    <option value="98">BG</option>
			                    <option value="97">VR</option>
			                     <option value="13">EBET</option>
								</select>
						</div>
					</div>
					<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				</div>
				
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" value="${end }" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<label class="sr-only" for="keyword">选择转账状态</label> 
					<div class="form-group">
						<div class="input-group">
							<select class="form-control" name="transStatus" id="transStatus">
								<option value="">选择转账状态</option>
								<option value="1">转账成功</option>
								<option value="2">转账失败</option>
								<option value="3">处理中</option>
							</select>
						</div>
					</div>
					
					<label class="sr-only" for="keyword">选择转账方向</label> 
					<div class="form-group">
						<div class="input-group">
							<select class="form-control" name="transType" id="transType">
								<option value="">选择转账状态</option>
								<option value="1">从系统转入三方</option>
								<option value="2">从三方转入系统</option>
							</select>
						</div>
					</div>
					<label class="sr-only" for="keyword">选择租户</label> 
					<div class="form-group">
						<div class="input-group">
							<select class="form-control" name="stationId" id="stationId">
								<option value="">选择租户</option>
							</select>
						</div>
					</div>
				</div>
				
				
				
			<!-- 	<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				&nbsp;&nbsp;
				<button class="btn btn-primary" onclick="javascript:refrashTrasnLimit();">更新真人额度</button> -->
				查看额度请选择相应站点和游戏类型：&nbsp;&nbsp;
				总额度:<span id="maxquto" style="color: blue">0</span>&nbsp;&nbsp;
				已使用额度:<span id="useQuota" style="color: green" >0</span> 
				&nbsp;&nbsp;
				剩余额度:<span  id="remainQuota" style="color: red">0</span> 
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			bindbtn();
			var s = getQueryString("begin");
			var e = getQueryString("end");
			if(!s&&!e){
				var begin = "";
				var end = "";
				begin = DateUtil.getCurrentDate();
				end = begin;
				setDate(begin, end);
			}else{
				setDate(s, e);
			}
			getTab();
			stationList("stationId");
			$("#stationId").on("change",function(){
				getCache();
			});
			$("#gameType").on("change",function(){
				getCache();
			});
		})
		
		
		
		function getCache(){
			var stationId =  $("#stationId").val();
			if(stationId==null || stationId==''){
				return;
			}
			var gameType= $("#gameType").val();
			if(gameType==null || gameType==''){
				return;
			}
			var param={};
			param["gameType"]=gameType;
			param["stationId"]=stationId;
			$.ajax({
				url : "${base}/admin/realGameTrans/getTrasnLimitCache.do",
				data : param,
				success : function(result) {
					try{
						var maxquto = result.maxQuota;
						var useQuota = result.useQuota;
						var remainQuota = maxquto-useQuota;
						$("#useQuota").text(useQuota);
						if(maxquto==-1||maxquto=='-1'){
							$("#maxquto").text('无限');
							$("#remainQuota").text('无限')
						}else{
							$("#maxquto").text(maxquto);
							$("#remainQuota").text(remainQuota);
						}
					}catch(e){
						console.log(e);
					}
				}
			});
		}
		
		function refrashTrasnLimit(){
			var stationId =  $("#stationId").val();
			if(stationId==null || stationId==''){
				layer.msg("请先选择站点");
				return;
			}
			var gameType= $("#gameType").val();
			if(gameType==null || gameType==''){
				layer.msg("请先选择游戏类型");
				return;
			}
			var param={};
			param["gameType"]=gameType;
			param["stationId"]=stationId;
			$.ajax({
				url : "${base}/admin/realGameTrans/refrashTrasnLimit.do",
				data : param,
				success : function(result) {
					try{
						
						var maxquto = result.maxQuota;
						var useQuota = result.useQuota;
						$("#useQuota").text(useQuota);
						if(maxquto==-1||maxquto=='-1'){
							$("#maxquto").text('无限');
							$("#remainQuota").text('无限')
						}else{
							$("#maxquto").text(maxquto);
							$("#remainQuota").text(result.remainingQuota);
						}
					}catch(e){
						console.log(e);
					}
				}
			});
		}
		
		function getQueryString(key) {
			var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
			var result = window.location.search.substr(1).match(reg);
			return result ? decodeURI(result[2]) : null;
		};
		
		
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/realGameTrans/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'floder',
					title : '所属网站目录',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, 
				 {
					field : 'gameType',
					title : '类型',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:gameTypeFormatter
				},
				 {
					field : 'transType',
					title : '转账类型',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:transTypeFormatter
				},
				{
					field : 'account',
					title : '本地账户',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'transId',
					title : '本地转账单号',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'thirdTransId',
					title : '三方转账单号',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'transMoney',
					title : '转账金额',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'transStatus',
					title : '转账状态',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : transStatusFormatter,
				}, 
				{
					field : 'createDatetime',
					title : '转账时间',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : dateFormatter
				}, 
				/* {
					field : 'resultTime',
					title : '转账成功时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, */ 
				{
					title : '操作',
					align : 'center',
					valign : 'middle',
					width : '150',
					formatter : operateFormatter
				}]
			});
			
			
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function transTypeFormatter(value, row, index) {
			var mesg = "",gameType = row.gameType;
			switch(row.gameType-0){
			case 1:mesg="AG";break;
			case 2:mesg="BBIN";break;
			case 3:mesg="MG";break;
			case 4:mesg="QT";break;
			case 5:mesg="ALLBET";break;
			case 6:mesg="PT";break;
			case 7:mesg="OG";break;
			case 8:mesg="DS";break;
			case 9:mesg="CQ9";break;
			case 10:mesg="IBC";break;
			case 11:mesg="JDB";break;
			case 92:mesg="TTG";break;
			case 95:mesg="MW";break;
			case 96:mesg="ISB";break;
	        case 99:mesg="M8";break;
	        case 991:mesg="M8H";break;
	        case 98:mesg="BG";break;
	        case 97:mesg="VR";break;
	        case 12:mesg="KY";break;
	        case 13:mesg="EBET";break;
			}
			if(value==1){
				return "从"+mesg+'转账到系统账户';
			}else{
				return "从系统账户转账到"+mesg;
			}
		}
		
		function transStatusFormatter(value, row, index){
			if(value==1){
				return "转账成功";
			}else if(value == 2){
				return "转账失败";
			}else{
				return "处理中";
			}
		}
		
		function gameTypeFormatter(value, row, index) {
			if(value==1){
				return "AG";
			}else if(value==2){
				return "BBIN";
			}else if(value==3){
				return "MG";
			}else{
				return "";
			}
		}
		//设置传入参数search
		function queryParams(params) {
			params["transType"] = $("#transType").val();
			params["account"] = $("#account").val();
			params["gameType"] = $("#gameType").val();
			params["transStatus"] = $("#transStatus").val();
			params["stationId"] = $("#stationId").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			params['stationId'] = $("#stationId").val();
			return params
		}
		
		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				$('.zhtj').val('0');
				setDate(begin, end);
				search();
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		//站点列表
		function stationList(id) {
			$.ajax({
				url : "${base}/admin/station/list.do",
				success : function(result) {
					var col = "<option value=\"\">选择租户</option>";
					for(var j in result.rows){
						console.log(result.rows[j].id+"||"+result.rows[j].name);
						col+='<option value="'+result.rows[j].id+'">'+result.rows[j].name+'('+result.rows[j].floder+')</option>';
					}
					$('#'+id).html(col);
				}
			});
		}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
		function operateFormatter(value, row, index) {
			if(row.transStatus == 3){
				return "";
			}
			return '<a class="roll" onclick ="rollback('+row.id+')" >回滚</a>';
		}
		function rollback(id){
			if(confirm("是否确定回滚该订单?")){
				$.ajax({
	 				url : "${base}/admin/realGameTrans/rollback.do?id="+id,
	 				success : function(result) {
	 					layer.msg(result.msg||"回滚成功");
	 					search();
	 				}
	 			});
			}
			/* Msg.confirm('是否确定回滚该订单？', function() {
				$.ajax({
	 				url : "${base}/admin/realGameTrans/rollback.do?id="+id,
	 				success : function(result) {
	 					layer.msg(result.msg||"回滚成功");
	 					search();
	 				}
	 			});
			}); */
		};
	</script>
</body>
</html>
