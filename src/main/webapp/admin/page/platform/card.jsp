<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab" >密保卡</a></li>
			<li><a href="#tab2" data-toggle="tab">用户绑定</a></li>
		</ul>
		<div class="tab-content">
		    <div class="tab-pane active" id="tab1">
    			<div id="toolbar">
					<button class="btn btn-primary" onclick="add();">生成密保卡</button>
				</div>
				<table id="datagrid_tb"></table>
			</div>
		    <div class="tab-pane" id="tab2">
		    	<br/>
			<div id="search" class="form-inline">
	
				<div class="panel panel-default">
					<div class="panel-heading">
						<div id="toolbar">
							<div id="search" class="form-inline">
								<select id="stationId" onchange="selectStation(this.value)" class="form-control"></select>
							</div>
						</div>
					</div>
					<div class="panel-body" id="menus">
					
					</div>
				</div>
			</div>
		    </div>
		</div>
	</div>
	
	<!--  绑定密保卡窗口 -->
	<div class="modal fade" id="bindCardWin" tabindex="-1" role="dialog" aria-labelledby="logoutLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="cardTitle">密保卡</h4>
				</div>
				<div class="modal-body">
					数据加载中....
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>

	<script type="text/javascript">
		$(function(){
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result
					};
					var html = template('agentlst_tpl', eachdata);
					$("#stationId").append(html);
				}
			});
		});
	
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/security/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'code',
					title : '卡号',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					valign : 'middle',
					formatter:function(value){
						return DateUtil.formatDatetime(value);
					}
				}, {
					field : 'stationId',
					title : '绑定站点',
					align : 'center',
					valign : 'bottom',
					formatter:function(value,row){
						if(value){
							return row.name+"("+row.floder+")";
						}
						return "<font color='red'>未使用</font>";
					}
				}, {
					field : 'useCount',
					title : '绑定账号个数',
					align : 'center',
					valign : 'middle'
				},{
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="detail" href="javascript:void(0)" title="查看">',
					'<i class="glyphicon glyphicon-eye-open"></i>', '</a>  '
				/* 	,
					'<a class="remove" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>'  */
					
					]
					.join('');
		}

		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				window.open("${base}/admin/security/getCard.do?id="+row.id);
			},
			'click .remove' : function(e, value, row, index) {
				remove(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params[$("#methodType").val()] = $("#keyword").val();
			return params
		}
		$(function() {
			getTab();
		})

	/* 	function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		} */

		function add() {
			$.ajax({
				url : "${base}/admin/security/createCard.do",
				success : function(data, textStatus, xhr) {
					Msg.info("成功生成一张密保卡");
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		
		function selectStation(stationId){
			if(stationId == 0){
				$("#menus").html("");	
				return;
			}
			
			$.ajax({
				url : "${base}/admin/security/getAgentUsers.do",
				data:{
					stationId : stationId
				},
				success : function(result) {
					var html = template('account_list', result);
					$("#menus").html(html);
				}
			});
			
		}
			
		function showBindWin(userId,account){
			$("#bindCardWin").modal("show");
			$("#cardTitle").html("当前账号:"+ account);
			loadCardInfo(userId);
		}
		
		function loadCardInfo(userId){
			var $body = $("#bindCardWin").find(".modal-body");
			$body.html("数据加载中....");
			$.ajax({
				url : "${base}/admin/security/getBindCards.do",
				data:{
					userId : userId
				},
				success : function(result) {
					result["userId"] = userId;
					var html = template('card_list', result);
					$body.html(html);
				}
			});
		}
		
		function bind(){
			var cardNo = $.trim($("#cardForm").find("input[name='cardNo']").val());
			if(!cardNo){
				Msg.info("请输入卡号");
				return;
			}
			$.ajax({
				url : "${base}/admin/security/bind.do",
				data : $("#cardForm").serialize(),
				success : function(data, textStatus, xhr) {
					loadCardInfo($("#cardForm").find("input[name='userId']").val());
				}
			});
		}
		
		function unbind(userId,cardId){
			Msg.confirm("是否确认要解除绑定?",function(){
				$.ajax({
					url : "${base}/admin/security/unbind.do",
					data : {
						userId:userId,
						cardId:cardId
					},
					success : function(data, textStatus, xhr) {
						loadCardInfo(userId);
					}
				});
			});
		}
		
	</script>
<script id="account_list" type="text/html">
{{if data.length == 0}}
	<h1> 该站点下没有账号 </h1>
{{/if}}
{{each data as account}}

	{{if account.accountStatus == 1 }}
		<div style="min-width:100px;margin-left:10px;float:left;background-color:#AAAAAA;" class="alert" role="alert">
	{{/if}}

	{{if account.accountStatus != 1 && account.cs }}
		<div style="min-width:100px;margin-left:10px;float:left" class="alert alert-danger" role="alert">
	{{/if}}

	{{if account.accountStatus != 1 && !account.cs}}
		<div style="min-width:100px;margin-left:10px;float:left" class="alert alert-success" role="alert">
	{{/if}}
		<a href="javascript:showBindWin({{account.id}},'{{account.account}}')" class="alert-link">{{account.account}}
			{{if account.cs}}({{account.cs}}){{/if}}
		</a>
	</div>
{{/each}}
</script>

<script id="card_list" type="text/html">
{{if data.length == 0}}
	&nbsp;&nbsp;<font color="red" style="font-size:16px;"> 账号未绑定密保卡</font>
{{/if}}
{{each data as card}}
	<div style="margin-top:5px;font-size:18px;color:blue;">
		{{card.code}} &nbsp;<button type="button" class="btn btn-danger" onclick="unbind({{userId}},{{card.id}})">删除</button>
	</div>
{{/each}}
<div style="margin-top:20px;"> 
	 <form class="form-inline" role="form" id="cardForm">  
		<input name="userId" value="{{userId}}" type="hidden"/>
		<input type="text" class="form-control" style="width:80%" placeholder="密保卡序列号" name="cardNo" />	
		<button type="button" class="btn btn-primary" onclick="bind()">新增</button>
	</form>
</div>
</script>
	
<script id="agentlst_tpl" type="text/html">
	<option value="0">请选择站点</option>
	{{each data as st}}
			<option value="{{st.id}}">{{st.name}}</option>
	{{/each}}
</script>