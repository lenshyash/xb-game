<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="saccount">用户名</label>
				<div class="form-group">
					<div class="input-group">
						<select id="groupId" class="form-control"></select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="saccount" placeholder="用户名">
					</div>
					<div class="input-group">
						<select class="form-control" id="saccountType">
							<option value="0">全部类型</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" onclick="add();">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade bs-example-modal-lg" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑账号</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="accountId"> <input type="hidden" id="agentId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">所属站点：</td>
								<td width="35%" class="text-left"><select id="stationId" class="form-control">
								</select></td>
								<td width="15%" class="text-right">用户类型：</td>
								<td width="35%" class="text-left"><select id="accountType" class="form-control"></select></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">用户账号：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="account" /></td>
								<td width="15%" class="text-right">账号状态：</td>
								<td width="35%" class="text-left"><select id="accountStatus" class="form-control">
										<option value="1">禁用</option>
										<option value="2">启用</option>
								</select></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">密码：</td>
								<td width="35%" class="text-left"><input type="password" class="form-control" id="newpwd" /></td>
								<td width="15%" class="text-right">确认密码：</td>
								<td width="35%" class="text-left"><input type="password" class="form-control" id="newrpwd" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editpwd" tabindex="-1" role="dialog" aria-labelledby="editpwdLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editpwdLabel">修改密码</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="pwdatId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">用户名：</td>
								<td width="80%" class="text-left"><input type="text" class="form-control" id="pwdat" disabled="disabled" /></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">密码类型：</td>
								<td width="80%" class="text-left"><select id="pwdType" class="form-control"><option value="1">登录密码</option>
										<option value="2">提款密码</option></select></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">新密码：</td>
								<td width="80%" class="text-left"><input type="password" class="form-control" id="pwd" /></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">确认密码：</td>
								<td width="80%" class="text-left"><input type="password" class="form-control" id="rpwd" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updpwd();">保存</button>
				</div>
			</div>
		</div>
	</div>

	<script id="accountType_tpl" type="text/html">
	{{each data as tl i}}
			{{if i > 0}}
			<option value="{{tl.id}}">{{tl.name}}</option>
			{{/if}}
	{{/each}}
			{{if data}}
				<option value="{{data[0].id}}">{{data[0].name}}</option>
			{{/if}}
	</script>
	<script id="grouplst_tpl" type="text/html">
	<option value="0">请选择站点</option>
	{{each data as at}}
			<option value="{{at.id}}">{{at.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var curStation = 0;
		function initAgentLst() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result
					};
					var html = template('grouplst_tpl', eachdata);
					$("#groupId").append(html);
					$("#stationId").append(html);
				}
			});
		}

		var agentMulti = '';
		function bindAgentLst() {
			$("#groupId").change(function() {
				var selval = $(this).children('option:selected').val();
				if (selval > 0) {
					curStation = selval;
					$("#datagrid_tb").bootstrapTable('refresh');
					setAgentMulti(selval);
				}
			});
		}

		function setAgentMulti(stationId) {
			$.ajax({
				url : "${base}/admin/account/stagentmulti.do",
				data : {
					stationId : stationId
				},
				success : function(result) {
					agentMulti = result.agentMulti;
				}
			});

		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/account/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '用户名',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'userName',
					title : '用户姓名',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'agentName',
					title : '所属上级',
					align : 'center',
					valign : 'bottom'
				}, {
					field : 'money',
					title : '账号余额',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'accountType',
					title : '用户类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter

				}, {
					field : 'accountStatus',
					title : '用户状态',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'lastLoginIp',
					title : '最后登录ip',
					align : 'center',
					valign : 'middle'
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function statusFormatter(value, row, index) {

			if (value === 2) {
				return '<span class="text-success">启用</span><a href="#"><span class="text-danger stateClose">(禁用)</span></a>';
			}
			return '<span class="text-danger">禁用</span><a href="#"><span class="text-success stateOpen">(启用)</span></a>';
		}

		function typeFormatter(value, row, index) {

			return GlobalTypeUtil.getTypeName(2, 2, value);
		}

		function moneyFormatter(value, row, index) {

			if (value === undefined) {
				return value;
			}
			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ].join(value);
			}
			return [ '<span class="text-primary">', '</span>' ].join(value);
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [
					'<a class="password" href="javascript:void(0)" title="修改密码">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a> ' ]
					.join('');
		}

		window.operateEvents = {
			'click .password' : function(e, value, row, index) {
				$("#editpwd").modal('toggle');
				$("#pwdatId").val(row.id);
				$("#pwdat").val(row.account);
				$("#pwdType").val("1");
				if (row.accountType == 1 || row.accountType == 4) {
					$("#pwdType").removeAttr("disabled");
				} else {
					$("#pwdType").attr("disabled", "disabled");
				}
			},
			'click .stateOpen' : function(e, value, row, index) {
				updStatus(row, 2);
			},
			'click .stateClose' : function(e, value, row, index) {
				updStatus(row, 1);
			}
		};

		function updStatus(row, status) {
			$.ajax({
				url : "${base}/admin/account/updStatus.do",
				data : {
					id : row.id,
					accountStatus : status
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		//设置传入参数
		function queryParams(params) {
			params["stationId"] = curStation;
			params["accountType"] = $("#saccountType").val();
			params["account"] = $("#saccount").val();
			return params
		}
		$(function() {
			initAgentLst();
			bindAgentLst();
			initCombo();
			getTab();

		})

		function initCombo() {
			var eachdata = {
				"data" : GlobalTypeUtil.getCombo(2, 2)
			};
			var html = template('accountType_tpl', eachdata);
			$("#accountType").append(html);
			$("#saccountType").append(html);
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function updpwd() {
			var upd_url = "${base}/admin/account/updpwd.do";
			var password = $("#pwd").val();
			var rpassword = $("#rpwd").val();
			if ($("#pwdType").val() == 2) {
				upd_url = "${base}/admin/account/updreppwd.do";
			}
			if (!password) {
				Msg.info("新密码不能为空！");
				return;
			}
			if (!rpassword) {
				Msg.info("确认密码不能为空！");
				return;
			}

			if (password !== rpassword) {
				Msg.info("两次密码不一致！");
				return;
			}
			$.ajax({
				url : upd_url,
				data : {
					id : $("#pwdatId").val(),
					pwd : password,
					rpwd : rpassword
				},
				success : function(result) {
					Msg.info("修改成功！");
				}
			});

		}

		function save() {

			var password = $("#newpwd").val();
			var rpassword = $("#newrpwd").val();
			var accountId = $("#accountId").val();
			if (!accountId) {
				if (!password) {
					Msg.info("新密码不能为空！");
					return;
				}
				if (!rpassword) {
					Msg.info("确认密码不能为空！");
					return;
				}
			}

			if (password !== rpassword) {
				Msg.info("两次密码不一致！");
				return;
			}

			$.ajax({
				url : "${base}/admin/account/save.do",
				data : {
					id : accountId,
					account : $("#account").val(),
					accountType : $("#accountType").val(),
					accountStatus : $("#accountStatus").val(),
					stationId : $("#stationId").val(),
					pwd : password,
					rpwd : rpassword
				},
				success : function(result) {
					Msg.info("操作成功！");
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function add() {
			$("#editmodel").modal('toggle');
			$("#accountId").val("");
			$("#account").val("");
			$("#account").removeAttr("disabled");
			$("#accountType").val("");
			$("#accountType").removeAttr("disabled");
			$("#agentName").removeAttr("disabled");
			$("#accountStatus").val("");
			$("#agentName").val("");
			$("#agentId").val("");
			$("#accountType option").addClass("hidden");
			$("#accountType option").eq(1).removeClass("hidden");
			if (agentMulti != 'on') {
				$("#accountType option").eq(3).removeClass("hidden");
			}
			$("#stationId").val("");
			$("#stationId").removeAttr("disabled");
		}
	</script>
</body>
</html>