<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<input type="hidden" id="stationId" value="1">
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="methodType" onchange="search()"></select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="add()">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
		<span style="color: red;">备注：彩票佣金是根据流水计算。电子、真人、体育佣金是根据盈利计算</span>
	</div>
	
	<div class="modal fade" id="model"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog" style="width: 900px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">代理返点</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">会员数量：</td>
								<td width="30%" class="text-left">
								<input id="effectMemberI" class="form-control" type="text"/> 
								</td>
								<td width="20%" class="text-right">盈利金额：</td>
								<td width="30%" class="text-left">
								<input id="profitMinI" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="20%" class="text-right">体育佣金比例：</td>
								<td width="30%" class="text-left">
								<input id="sportsGameI" class="form-control" type="text"/> 
								</td>
								<td width=20%" class="text-right">彩票佣金比例：</td>
								<td width="30%" class="text-left">
								<input id="lotteryGameI" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="20%" class="text-right">真人佣金比例：</td>
								<td width="30%" class="text-left">
								<input id="realGameI" class="form-control" type="text"/> 
								</td>
								<td width="20%" class="text-right">电子游戏佣金比例：</td>
								<td width="30%" class="text-left">
								<input id="videoGameI" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="15%" class="text-right">状态：</td>
								<td width="35%" class="text-left">
								<select id="statusI" class="form-control">
								<option value="2">启用</option>
								<option value="1">停用</option>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="saveModel" onclick="save();">保存</button>

				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$(function() {
			stationList();
		})
		
		//站点列表
		function stationList() {
			var stationId = $('#stationId').val();
			var url = '${base}/admin/station/list.do';
			$.ajax({
				url : url,
				success : function(j) {
					var col = '';
					for ( var i in j.rows) {
						if (stationId == j.rows[i].id) {
							col += '<option selected="selected" value="'+j.rows[i].id+'">'
									+ j.rows[i].name
									+ '('
									+ j.rows[i].floder + ')</option>';
						} else {
							col += '<option value="'+j.rows[i].id+'">'
									+ j.rows[i].name + '('
									+ j.rows[i].floder + ')</option>';
						}
					}
					$('#methodType').html(col);
					getTab();
				}
			});
		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/proxyRebateTactics/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'effectMember',
					title : '会员数量',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : hyslFormatter
				}, {
					field : 'profitMin',
					title : '盈利金额',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : yljeFormatter
				}, {
					field : 'sportsGame',
					title : '体育佣金比例',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter : bfbFormatter
				}, {
					field : 'realGame',
					title : '真人佣金比例',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : bfbFormatter
				}, {
					field : 'videoGame',
					title : '电子游戏佣金比例',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : bfbFormatter
				}, {
					field : 'lotteryGame',
					title : '彩票佣金比例',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : bfbFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					field : 'memo',
					title : '描述',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : memoFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '50',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}
		
		function memoFormatter(value, row, index){
			if(value==null || value==""){
				return [value ].join('');
			}
			if(value.length>8){
				return [value.substr(0, 8)+'。。' ].join('');
			}else{
				return [value ].join('');
			} 
		}
		
		function hyslFormatter(value, row, index){
			return ['最低有效会员      '+value+ ' 人' ].join('');	
		}
		
		function yljeFormatter(value, row, index){
			return [value+ '  元以上' ].join('');	
		}
		
		function bfbFormatter(value, row, index){
			if(value==undefined){
				value=0;
			}
			return [value+ '%' ].join('');
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ' ]
					.join('');
			
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				$("#model").modal('toggle');
				$("#editLabel").text("修改代理返点策略");
				$("#playId").val(row.id)
				$("#effectMemberI").val(row.effectMember);
				$("#profitMinI").val(row.profitMin);
				$("#sportsGameI").val(row.sportsGame);
				$("#videoGameI").val(row.videoGame);
				$("#lotteryGameI").val(row.lotteryGame);
				$("#realGameI").val(row.realGame);
				$("#statusI").val(row.status);
				$("#saveModel").attr("onclick","updateSave()");
			},
			'click .stateOpen' : function(e, value, row, index) {
				closeOrOpen(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				closeOrOpen(row);
			}
		};

		function add(){
			$("#model").modal('toggle');
			$("#editLabel").text("新增代理返点策略");
			$("#effectMemberI").val("");
			$("#profitMinI").val("");
			$("#sportsGameI").val("");
			$("#videoGameI").val("");
			$("#lotteryGameI").val("");
			$("#realGameI").val("");
			$("#statusI").val("2");
			$("#saveModel").attr("onclick","addSave()");
		}

		function addSave() {
			var curDate = new Date();
			var effectMember = $("#effectMemberI").val();
			var profitMin = $("#profitMinI").val();
			var sportsGame = $("#sportsGameI").val();
			var videoGame = $("#videoGameI").val();
			var lotteryGame = $("#lotteryGameI").val();
			var realGame = $("#realGameI").val();
			var status = $("#statusI").val();
			if(effectMember==""){
				tips('会员数量不能为空!',1);
				return false;
			}
			if(profitMin==""){
				tips('盈利金额不能为空!',1);
				return false;
			}
			if(sportsGame==""){
				tips('体育佣金不能为空!',1);
				return false;
			}
			if(lotteryGame==""){
				tips('彩票佣金不能为空!',1);
				return false;
			}
			if(realGame==""){
				tips('真人佣金不能为空!',1);
				return false;
			}
			if(realGame==""){
				tips('电子游戏佣金不能为空!',1);
				return false;
			}
			var bc = {
	 				"effectMember" : effectMember,
					"profitMin" :profitMin,
					"sportsGame" :sportsGame, 
					"videoGame" :videoGame, 
					"lotteryGame" :lotteryGame, 
					"realGame" :realGame, 
					"status" :status,
					"createTime" : curDate,
					"stationId" : $("#methodType").val()
	        	};
			$.ajax({
				url : "${base}/admin/proxyRebateTactics/addSave.do",
				data : bc,
				success : function(result) {
					tips('保存成功!',1);
					refresh();
					$("#model").modal('toggle');
				},
                error: function () {
                    alert('输入格式有误');
                }
			});
		}

		function updateSave() {
			var curDate = new Date();
			var effectMember = $("#effectMemberI").val();
			var profitMin = $("#profitMinI").val();
			var sportsGame = $("#sportsGameI").val();
			var videoGame = $("#videoGameI").val();
			var lotteryGame = $("#lotteryGameI").val();
			var realGame = $("#realGameI").val();
			var status = $("#statusI").val();
			if(effectMember==""){
				tips('会员数量不能为空!',1);
				return false;
			}
			if(profitMin==""){
				tips('盈利金额不能为空!',1);
				return false;
			}
			if(sportsGame==""){
				tips('体育佣金不能为空!',1);
				return false;
			}
			if(lotteryGame==""){
				tips('彩票佣金不能为空!',1);
				return false;
			}
			if(realGame==""){
				tips('真人佣金不能为空!',1);
				return false;
			}
			if(videoGame==""){
				tips('电子彩票佣金不能为空!',1);
				return false;
			}
			var bc = {
	 				"effectMember" : effectMember,
					"profitMin" :profitMin,
					"sportsGame" :sportsGame, 
					"videoGame" :videoGame, 
					"lotteryGame" :lotteryGame, 
					"realGame" :realGame, 
					"status" :status,
					"modifyDatetime" : curDate,
					"stationId" : $("#methodType").val(),
					"id" : $("#playId").val()
	        	};
				$.ajax({
					url : "${base}/admin/proxyRebateTactics/updateSave.do",
					data : bc,
					success : function(result) {
						tips('更新成功!',1);
						refresh();
						$("#model").modal('toggle');
					},
	                error: function () {
	                    alert('输入格式有误');
	                }
				});
		}

		function closeOrOpen(row) {
			var state = 1;
			if(row.status==1){
				state = 2;
			}
			var bc = {
	 				"status" : state,
					"id" :row.id,
					"stationId" :row.stationId 
	        	};
			$.ajax({
				url : "${base}/admin/proxyRebateTactics/closeOrOpen.do",
				data : bc,
				success : function(result) {
					if(row.status==1){
						tips("状态",2);
					}else{
						tips("状态",3);
					}
					refresh();
				}
			});
		}

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#methodType").val();
			return params
		}

		function search() {
			refresh();
		}
	</script>
</body>
</html>
