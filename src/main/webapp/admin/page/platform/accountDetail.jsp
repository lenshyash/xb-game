<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-bordered table-striped">
	<tbody>
		<tr>
			<td width="20%" class="text-right">投注账号：</td>
			<td width="30%">${map.account}</td>
			<td width="20%" class="text-right">用户类型：</td>
			<td width="30%"><c:if test="${map.accountType==1 }">会员</c:if><c:if test="${map.accountType==4 }">代理</c:if></td>
		</tr>
		<tr>
			<td class="text-right">账号状态：</td>
			<td><c:if test="${map.accountStatus==2 }">启用</c:if><c:if test="${map.accountStatus!=2 }">禁用</c:if></td>
			<td class="text-right">用户姓名：</td>
			<td>${map.userName }</td>
		</tr>
		<tr>
			<td class="text-right">银行账号：</td>
			<td>${map.cardNo }</td>
			<td class="text-right">取现银行：</td>
			<td>${map.bankName }</td>
		</tr>
		<tr>
			<td class="text-right">电话：</td>
			<td>${map.phone }</td>
			<td class="text-right">邮箱：</td>
			<td>${map.email }</td>
		</tr>
		<tr>
			<td class="text-right">银行地址：</td>
			<td>${map.bankAddress }</td>
			<td class="text-right">QQ：</td>
			<td>${map.qq }</td>
		</tr>
		<tr>
			<td class="text-right">所属上级：</td>
			<td width="85%" colspan="3" class="text-left">${parentNames }</td>
		</tr>
	</tbody>
</table>