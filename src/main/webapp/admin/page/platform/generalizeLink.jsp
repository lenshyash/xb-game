<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
	<input type="hidden" id="stationId" value="1">
	<input value="" id="yhType"  type="hidden"/>
		<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
			<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="methodType"></select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="getByType">
						<option selected="selected" value="0">全部</option>
						<option value="2">会员</option>
						<option value="1">代理</option>
						</select>
					</div>
				</div>
<!-- 				<button class="btn btn-primary" onclick="add();">新增</button> -->
<!-- 				<button class="btn btn-primary" onclick="search();">搜索</button> -->
			</div>
		</div>
		<table id="datagrid_tb"></table>
		
	</div>
	
	<script type="text/javascript">
		$(function() {
			stationList();
			$("#methodType").change(function() {
				var curVal=$(this).find("option:checked").attr("value");
				$("#stationId").val(curVal);
				refresh();
				});
			$("#getByType").change(function() {
				var curVal = $(this).find("option:checked").attr("value");
				$("#yhType").val(curVal);
				$("#datagrid_tb").bootstrapTable('refresh');
			});
		})
		
		//站点列表
		function stationList() {
			var stationId = $('#stationId').val();
			var url = '${base}/admin/station/list.do';
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j.rows){
						if(stationId==j.rows[i].id){
							col+='<option selected="selected" value="'+j.rows[i].id+'">'+j.rows[i].name+'('+j.rows[i].floder+')</option>';
						}else{
							col+='<option value="'+j.rows[i].id+'">'+j.rows[i].name+'('+j.rows[i].floder+')</option>';
						}
					}
					$('#methodType').html(col);
					getTab();
				}
			});
		}
		
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/generalizeLink/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [
				{
					field : 'userAccount',
					title : '账号',
					align : 'center',
					width : '100',
					valign : 'middle'
				},
				{
					field : 'type',
					title : '注册类型',
					align : 'center',
					width : '100',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'cpRolling',
					title : '彩票返点数',
					align : 'center',
					width : '100',
					valign : 'middle'
				}, {
					field : 'adminUrl',
					title : '推广链接',
					align : 'center',
					width : '100',
					valign : 'middle',
					formatter : pasteFormatter,
					class:'copyBtns'
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					width : '100',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}
// 				, {
// 					title : '操作',
// 					align : 'center',
// 					width : '50',
// 					valign : 'middle',
// 					events : operateEvents,
// 					formatter : operateFormatter
// 				} 
				]
				,onLoadSuccess:function(){
				$('.handIco').zclip({
	 					path:"${base}/common/js/pasteUtil/ZeroClipboard.swf",
	 					copy:function(){
	 						return this.id;
	 					},
	 			    	afterCopy:function(){
		 			    	layer.alert('复制成功,请按ctrl+v粘贴:<br/>'+this.id);
		 			    }
	 				});
		}
			});
		}
		
		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ' ]
					.join('');
		}
		
		function typeFormatter(value, row, index) {
			var type = row.type;
			if(type ==1){
				return '<span class="label label-info">代理</span>';
			}else{
				return '<span class="label label-success">会员</span>';
			}
		}
		
		function pasteFormatter(value, row, index) {
			return row.adminUrl
					+ '&nbsp;<span class="label label-primary handIco" id="'+ row.adminUrl+'" linkUrl="'+row.adminUrl+'">复制</span>';
		}
		
		window.operateEvents = {
				'click .eidt' : function(e, value, row, index) {
					$("#model").modal('toggle');
					$("#playId").val(row.id);
				},
				'click .stateOpen' : function(e, value, row, index) {
					closeOrOpen(row);
				},
				'click .stateClose' : function(e, value, row, index) {
					closeOrOpen(row);
				}
			};
		
		function closeOrOpen(row){
			var state = 1;
			if(row.status==1){
				state = 2;
			}
			var bc = {
	 				"status" : state,
					"id" :row.id,
					"stationId" :row.stationId
	        	};
			$.ajax({
				url : "${base}/admin/generalizeLink/updateStatusById.do",
				data : bc,
				success : function(result) {
					if(row.status==1){
						tips(row.linkUrl,2);
					}else{
						tips(row.linkUrl,3);
					}
					refresh();
				}
			});
		}
		
		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#stationId").val();
			params['type'] = $("#yhType").val();
			return params
		}

		function search() {
			refresh();
		}
		
	</script>
</body>
</html>
<style>
.handIco{
	cursor:pointer;
}
.copyBtns{position:relative;}
</style>