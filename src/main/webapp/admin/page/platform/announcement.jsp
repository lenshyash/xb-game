<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<link rel="stylesheet" href="${base}/common/bootstrap/select/css/bootstrap-select.min.css">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<script src="${base}/common/bootstrap/select/js/bootstrap-select.js"></script>
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group">
							<label class="sr-only" for="stype">类型</label> <select class="form-control" id="stype">
								<option value="0">全部类型</option>
								<option value="1">群发</option>
								<option value="2">指定站点</option>
							</select>
						</div>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" onclick="add();">新增</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑公告</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="ancId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="25%" class="text-right">结束时间：</td>
								<td><input type="text" class="form-control" id="endDatetime" /></td>
							</tr>
							<tr>
								<td class="text-right">类型：</td>
								<td><select id="type" class="form-control">
									<option value="1" selected>群发</option>
									<option value="2">指定站点</option>
								</select></td>
							</tr>
							<tr class="hidden station_wrap">
								<td class="text-right">选中站点：</td>
								<td><select id="stationIds"title="请选中站点" class="form-control selectpicker"data-live-search="true" multiple data-max-options="100">
									<c:forEach items="${stations}" var="s"><option value="${s.id}" title="${s.floder}">(${s.floder})${s.name }</option></c:forEach>
								</select></td>
							</tr>
							<tr>
								<td class="text-right">内容：</td>
								<td><textarea id="content" class="form-control">
								</textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script id="type_tpl" type="text/html">
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
			var options_datetime = {
				language : 'zh-CN',
				autoclose : true,
				minView : 0,
				format : 'yyyy-mm-dd hh:mm:ss'
			};
			$('#endDatetime').datetimepicker(options_datetime);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/announcement/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'type',
					title : '公告类别',
					align : 'center',
					width : '100',
					formatter : typeFormatter
				}, {
					field : 'endDatetime',
					title : '结束时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}
		function typeFormatter(value, row, index) {
			switch(value){
			case 1:return "群发";
			case 2:return "指定站点";
			}
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
					'<a class="remove" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				if(row.type==1){
					$("#stationIds").selectpicker('deselectAll')
					$("#editmodel").modal('toggle');
					$("#ancId").val(row.id);
					$("#endDatetime").val(DateUtil.formatDatetime(row.endDatetime));
					$("#type").val(row.type).change();
					$("#content").val(row.content);
				}else{
					$.ajax({
						url : '${base}/admin/announcement/getStationIds.do?id='+row.id,
						success : function(sIds) {
							$("#stationIds").selectpicker('val', sIds);
							$("#editmodel").modal('toggle');
							$("#ancId").val(row.id);
							$("#endDatetime").val(DateUtil.formatDatetime(row.endDatetime));
							$("#type").val(row.type).change();
							$("#content").val(row.content);
						}
					});
				}
			},
			'click .remove' : function(e, value, row, index) {
				remove(row.id);
			}
		};
		$("#type").change(function(){
			var v=$(this).val();
			if(v==1){
				$(".station_wrap").addClass("hidden");
			}else{
				$(".station_wrap").removeClass("hidden");
			}
		});
		//设置传入参数
		function queryParams(params) {
			params['type'] = $("#stype").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		$(function() {
			getTab();
			bindbtn();
		})

		function remove(id) {
			$.ajax({
				url : "${base}/admin/announcement/delete.do?id="+id,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function add() {
			$("#editmodel").modal('toggle');
			$("#ancId").val("");
			$("#endDatetime").val("");
			$("#type").val("1").change();
			$("#stationIds").selectpicker('deselectAll')
			$("#content").val("");
		}

		function save() {
			var ele = {
				id : $("#ancId").val(),
				type : $("#type").val(),
				endDatetime : $("#endDatetime").val(),
				content : $("#content").val(),
				stationIds:$("#stationIds").val()
			};
			$.ajax({
				url : '${base}/admin/announcement/save.do',
				data : ele,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin, end);
				search();
			});
		}
	</script>
</body>
</html>