<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}

.secCheckBox {
	margin-left: 80px;;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#" data-toggle="tab" id="confnav">配置内容</a></li>
			<li><a href="#" data-toggle="tab" id="setnav">绑定站点</a></li>
			<li><a href="#" data-toggle="tab" id="groupnav">配置组别</a></li>
			<li><a href="#" data-toggle="tab" id="stationnav">站点配置</a></li>
		</ul>
		<div id="confnav_div">
			<div id="toolbar">
				<div id="search" class="form-inline">
					<label class="sr-only" for="splatform">配置归属</label>
					<div class="form-group">
						<div class="input-group" id="gnSearch">
							<select id="sgroupId" class="form-control">
							</select>
						</div>
						<div class="input-group">
							<input type="text" class="form-control" id="sname" placeholder="配置名称">
						</div>
						<select class="form-control" id="splatform">
							<option value="1">租户</option>
							<option value="2">系统</option>
						</select>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" onclick="add();">新增</button>
				</div>
			</div>
			<table id="datagrid_tb"></table>
		</div>
		<style type="text/css">
			#settings{height:500px;overflow: hidden;overflow-y:scroll;position: relative;}
			#kj_gps button{
			outline:none;padding:0;
			}
			#kj_gps button a{color:#333;text-decoration:none;display: inline-block;padding:5px 10px;}
		</style>
		<div id="setnav_div" class="hidden">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div id="toolbar">
						<div id="search" class="form-inline">
							<button id="saveConf" class="btn btn-primary" onclick="saveSet();" disabled="disabled">保存</button>
							<button id="saveCheck" class="btn btn-primary hidden" onclick="saveCheck();">全选</button>
							<select id="stationId" class="form-control"></select>
							<div class="btn-group btn-group-sm" id="kj_gps">
							   
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body" id="settings"></div>
			</div>
		</div>

	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑配置</h4>
				</div>
				<div class="modal-body">
					<div id="confEditDiv">
						<input type="hidden" id="confId">
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody>
								<tr>
									<td width="20%" class="text-right">配置归属：</td>
									<td width="80%" class="text-left"><select id="platform" class="form-control">
											<option value="1">租户</option>
											<option value="2">系统</option>
									</select></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">配置组别：</td>
									<td width="80%" class="text-left"><select id="groupId" class="form-control">
									</select></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">配置名称：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="name" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">配置标题：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="title" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">键：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="key" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">配置类型：</td>
									<td width="80%" class="text-left"><select id="type" class="form-control">
									</select></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">状态：</td>
									<td width="80%" class="text-left"><select id="status" class="form-control">
											<option value="1">禁用</option>
											<option value="2">启用</option>
											<option value="3">隐藏</option>
									</select></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">排序编码：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="bOrderNo" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">预设值：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="initValue" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">拓展属性：</td>
									<td width="80%" class="text-left"><textarea id="expand" class="form-control"></textarea></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">绑定数据源：</td>
									<td width="80%" class="text-left"><textarea id=source class="form-control"></textarea></td>
								</tr>
								<tr class="agentpro hidden">
									<td width="20%" class="text-right">备注：</td>
									<td width="80%" class="text-left"><textarea id="remark" class="form-control"></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="groupEditDiv" class="hidden">
						<input type="hidden" id="gphdId">
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody>
								<tr>
									<td width="20%" class="text-right">组别名称：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="groupName" /></td>
								</tr>
								<tr>
									<td width="20%" class="text-right">排序编码：</td>
									<td width="80%" class="text-left"><input type="text" class="form-control" id="orderNo" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="detailmodel" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="detailLabel">预览配置</h4>
				</div>
				<div class="modal-body">
					<div id="detail_table"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script id="combolst_tpl" type="text/html">
	<option value="0">请选择</option>
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>

	<script type="text/text/html" id="gps_tpl">
		{{each data as d index}}
			{{if index == 0}}
				<button type="button" class="btn btn-default active"><a href="#gps_{{d.id}}">{{d.name}}</a></button>
			{{else}}
				<button type="button" class="btn btn-default"><a href="#gps_{{d.id}}">{{d.name}}</a></button>
			{{/if}}
		{{/each}}
	</script>
	<script id="config_tpl" type="text/html">
		{{each gps as gp}}
		 	<div class="panel panel-default" id="gps_{{gp.id}}">
				<div class="panel-heading">{{gp.name}}</div>
				<div class="panel-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							{{each gp.sons as conf}}
								<tr>
									<td width="30%" class="text-center">{{conf.name}}</td>
									<td width="70%"><a style="word-break: break-all;max-width:50%;display:inline-block;" href="#" id="{{conf.configId}}" data-type="{{conf.type}}" data-value="{{conf.value}}" data-pk="{{conf.id}}" data-title="{{conf.title}}" data-source="{{conf.source}}"{{conf.expand}}></a></td>
								</tr>
							{{/each}}
						</tbody>
					</table>
				</div>
			</div>
		{{/each}}
	</script>

	<script id="settings_tpl" type="text/html">
	{{each gps as gp}}
	<div class="checkbox">
		<label>
  			<input type="checkbox" id="{{gp.id}}" value="{{gp.id}}" {{groupchecklst[gp.id]}}> {{gp.name}}
		</label>
			<div class="secCheckBox">
				{{each gp.sons as conf}}
					<label checkbox-inline >
  						<input type="checkbox" name="conf_chk" id="{{conf.id}}" parent="{{gp.id}}" value="{{conf.id}}" {{checkLst[conf.id]}}> {{conf.name}}
					</label>
				{{/each}}
			</div>
	</div>
	{{/each}}
	</script>
	<script>
		var base = "${base}";
		$("#kj_gps").on("click","button a",function(){
			$(this).parent().addClass("active").siblings().removeClass("active");
		})
	</script>
	<script src="${base}/admin/js/agentconfig.js?v=1.24"></script>
</body>
</html>