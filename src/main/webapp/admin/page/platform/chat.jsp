<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="saccount">聊天室编号</label>
				<div class="form-group">
					<div class="input-group">
						<select id="groupId" class="form-control"></select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="platform" placeholder="聊天室编号">
					</div>
					<!-- <!-- <div class="input-group">
						<select class="form-control" id="saccountType">
							<option value="0">全部类型</option>
						</select>
					</div> -->
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" onclick="add();">一键生成</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade bs-example-modal-lg" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">生成聊天室</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationId" class="form-control">
								<option value="">请选择站点</option>
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td class="text-right">聊天室编号：</td>
								<td class="text-left">
									<input type="text" class="form-control" id="platformNo" />
								</td>
							</tr> 
							<!-- <tr style="display:none" id="folderTr">
								<td class="text-right">模板：</td>
								<td class="text-left">
								<select id="folderList" class="form-control">
									<option value="">请选择模板</option>
								</select>
								</td>
							</tr> -->
							<tr style="display:none" id="domainTr">
								<td class="text-right">聊天室域名：</td>
								<td class="text-left">
								<select id="domainList" class="form-control">
								<option value="">请选择域名</option>
								</select>
								</td>
							</tr>
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editpwd" tabindex="-1" role="dialog" aria-labelledby="editpwdLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editpwdLabel">修改密码</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="pwdatId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">用户名：</td>
								<td width="80%" class="text-left"><input type="text" class="form-control" id="pwdat" disabled="disabled" /></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">密码类型：</td>
								<td width="80%" class="text-left"><select id="pwdType" class="form-control"><option value="1">登录密码</option>
										<option value="2">提款密码</option></select></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">新密码：</td>
								<td width="80%" class="text-left"><input type="password" class="form-control" id="pwd" /></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">确认密码：</td>
								<td width="80%" class="text-left"><input type="password" class="form-control" id="rpwd" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updpwd();">保存</button>
				</div>
			</div>
		</div>
	</div>

	<script id="accountType_tpl" type="text/html">
	{{each data as tl i}}
			{{if i > 0}}
			<option value="{{tl.id}}">{{tl.name}}</option>
			{{/if}}
	{{/each}}
			{{if data}}
				<option value="{{data[0].id}}">{{data[0].name}}</option>
			{{/if}}
	</script>
	<script id="grouplst_tpl" type="text/html">
	<option value="">请选择站点</option>
	{{each data as at}}
			<option value="{{at.id}}">{{at.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var curStation = 0;
		function initAgentLst() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result
					};
					var html = template('grouplst_tpl', eachdata);
					$("#groupId").append(html);
					$("#stationId").append(html);
				}
			});
		}

		var agentMulti = '';
		function bindAgentLst() {
			$("#groupId").change(function() {
				var selval = $(this).children('option:selected').val();
				if (selval > 0) {
					curStation = selval;
					$("#datagrid_tb").bootstrapTable('refresh');
					setAgentMulti(selval);
				}
			});
		}

		function setAgentMulti(stationId) {
			$.ajax({
				url : "${base}/admin/account/stagentmulti.do",
				data : {
					stationId : stationId
				},
				success : function(result) {
					agentMulti = result.agentMulti;
				}
			});

		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/chat/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '名称',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'platformId',
					title : '聊天室编号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'token',
					title : '聊天室token',
					align : 'center',
					valign : 'bottom'
				}, {
					field : 'domain',
					title : '聊天室域名',
					align : 'center',
					valign : 'bottom'
				},{
					field : 'platformId',
					title : '超管账号',
					align : 'center',
					valign : 'middle',
					formatter : superformatter
				},{
					field : 'platformPass',
					title : '密码',
					align : 'center',
					valign : 'middle'
				}]
			});
		}

		function superformatter(value, row, index) {
			return value+'admin';
		}
		window.operateEvents = {
			'click .password' : function(e, value, row, index) {
				$("#editpwd").modal('toggle');
				$("#pwdatId").val(row.id);
				$("#pwdat").val(row.account);
				$("#pwdType").val("1");
				if (row.accountType == 1 || row.accountType == 4) {
					$("#pwdType").removeAttr("disabled");
				} else {
					$("#pwdType").attr("disabled", "disabled");
				}
			},
			'click .stateOpen' : function(e, value, row, index) {
				updStatus(row, 2);
			},
			'click .stateClose' : function(e, value, row, index) {
				updStatus(row, 1);
			}
		};

		function updStatus(row, status) {
			$.ajax({
				url : "${base}/admin/account/updStatus.do",
				data : {
					id : row.id,
					accountStatus : status
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		//设置传入参数
		function queryParams(params) {
			params["stationId"] = curStation;
			params["platformId"] = $("#platform").val();
			return params
		}
		$(function() {
			initAgentLst();
			bindAgentLst();
			initCombo();
			getTab();

		})

		function initCombo() {
			var eachdata = {
				"data" : GlobalTypeUtil.getCombo(2, 2)
			};
			var html = template('accountType_tpl', eachdata);
			$("#accountType").append(html);
			$("#saccountType").append(html);
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function updpwd() {
			var upd_url = "${base}/admin/account/updpwd.do";
			var password = $("#pwd").val();
			var rpassword = $("#rpwd").val();
			if ($("#pwdType").val() == 2) {
				upd_url = "${base}/admin/account/updreppwd.do";
			}
			if (!password) {
				Msg.info("新密码不能为空！");
				return;
			}
			if (!rpassword) {
				Msg.info("确认密码不能为空！");
				return;
			}

			if (password !== rpassword) {
				Msg.info("两次密码不一致！");
				return;
			}
			$.ajax({
				url : upd_url,
				data : {
					id : $("#pwdatId").val(),
					pwd : password,
					rpwd : rpassword
				},
				success : function(result) {
					Msg.info("修改成功！");
				}
			});

		}

		function save() {

			var stationId = $("#drdStationId").val();
			//var folder = $("#folderList").val();
			var platformNo = $("#platformNo").val();
			var domain = $("#domainList").val();
			$.ajax({
				url : "${base}/admin/chat/save.do",
				data : {
					stationId : stationId,
					platformNo :platformNo,
					domain :domain
				},
				success : function(result) {
					var html = "";
					html += '<div style="margin-left:50px;margin-top: 30px">';
					html+='<span style="font-size:16px;color:#333">  聊天室ID：'+result.platformId+'</span></br>';
					html+='<span  style="font-size:16px;color:#333"> 超级管理员：'+result.superAdmin+'</span></br>';
					html+='<span  style="font-size:16px;color:#333"> 登录密码：'+result.platPass+'</span></br>';
					html+='<span  style="font-size:16px;color:#333"> 聊天室后台链接：'+result.domain+'</span></br>';
					html+='<span style="color:red;font-size:14px" >提示信息：请记录以上信息登录聊天室租户后台</span>';
					html+='</div>';
					layer.open({
						type : 1, //page层
						area : [ '420px', '380px' ],
						skin:'layui-layer-rim',
						title : '聊天室新建信息',
						shade : 0.6, //遮罩透明度
						scrollbar : false,
						offset : '300px',//距离顶部
						moveType : 0, //拖拽风格，0是默认，1是传统拖动
						shift : 0, //0-6的动画形式，-1不开启
						content:html,
				        btn:['确定'],
				        yes: function (index) {
				        	window.location.reload();
				        }
				    });
					//Msg.info("操作成功!聊天室消息");
					//$("#datagrid_tb").bootstrapTable('refresh');
					//window.location.reload();
				}
			});
		}
		function add() {
			$("#editmodel").modal('toggle');
			 $("#drdStationId").bind('change',function(){
				 $.ajax({
						url: '${base}/admin/chat/folderlist.do',
						data: {
							stationId:$(this).val()
						},
						success: function (data) {
							
							if(data.success){
								/* if(data.folderList){
									var d = data.folderList;
									var html="";
									for(var i = 0; i< d.length; i++){
										html+='<option value="'+d[i].folder+'">'+d[i].folder+'</option>'
									}
									$("#folderList").html(html);
									$("#folderTr").show();
								}else{
									$("#folderTr").hide();
								} */
								if(data.domainList){
									var d = data.domainList;
									var html="";
									for(var i = 0; i< d.length; i++){
										html+='<option value="'+d[i].domain+'">'+d[i].domain+'</option>'
									}
									$("#domainList").html(html);
									$("#domainTr").show();
								}else{
									$("#domainTr").hide();
								}
							}
						}
					});
			 })
		}
	</script>
</body>
</html>