<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<jsp:include page="/admin/include/lotteryComment.jsp"></jsp:include>
		<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="methodType" onchange="search()"></select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="add();">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
		<span style="color: red;">备注：会员返水是根据当天的有效投注计算</span>
	</div>
	
	<div class="modal fade" id="model"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog" style="width: 900px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">会员反水</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">彩票返水比例(%)：</td>
								<td width="30%" class="text-left">
								<input id="lotteryGameE" class="form-control" type="text"/> 
								</td>
								<td width="20%" class="text-right">体育返水比例(%)：</td>
								<td width="30%" class="text-left">
								<input id="sportsGameE" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="20%" class="text-right">有效投注：</td>
								<td width="30%" class="text-left">
								<input id="profitBetE" class="form-control" type="text"/> 
								</td>
								<td width="20%" class="text-right">打码量倍数：</td>
								<td width="30%" class="text-left">
								<input id="multipleE" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td width="20%" class="text-right">六合彩特码B返水比例(%)：</td>
								<td width="30%" class="text-left"><input id="markSixTmbE" class="form-control" type="text"/></td>
								<td width="20%" class="text-right">六合彩正码B返水比例(%)：</td>
								<td width="30%" class="text-left"><input id="markSixZmbE" class="form-control" type="text"/></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">最高返水上限：</td>
								<td width="30%" class="text-left">
								<input id="upperLimitE" class="form-control" type="text"/> 
								</td>
							</tr>
							<tr>
								<td class="text-right">描述：</td>
								<td class="text-left">
								<textarea id="memoE" class="form-control"></textarea>
								</td>
								<td class="text-right">状态：</td>
								<td class="text-left">
								<select id="statusNew" class="form-control">
								<option value="2">启用</option>
								<option value="1">停用</option>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="modalSave"
						onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
		$(function() {
			stationList();
		})
		
		//站点列表
		function stationList() {
			var url = '${base}/admin/station/list.do';
			$.ajax({
				url : url,
				success : function(j) {
					var	col='';
					for(var i in j.rows){
						col+='<option value="'+j.rows[i].id+'">'+j.rows[i].name+'('+j.rows[i].floder+')</option>';
					}
					$('#methodType').html(col);
					getTab();
				}
			});
		}
		
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/memberRollBack/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [
					{
						field : 'lotteryGame',
						title : '彩票返水比例',
						align : 'center',
						width : '100',
						valign : 'middle',
						formatter : bfbFormatter
					}, {
						field : 'sportsGame',
						title : '体育返水比例',
						align : 'center',
						width : '100',
						valign : 'middle',
						formatter : bfbFormatter
					}, {
						field : 'markSixTmb',
						title : '六合彩特码B比例',
						align : 'center',
						width : '100',
						valign : 'middle',
						formatter : bfbFormatter
					}, {
						field : 'markSixZmb',
						title : '六合彩正码B比例',
						align : 'center',
						width : '100',
						valign : 'middle',
						formatter : bfbFormatter
					}, {
						field : 'profitBet',
						title : '有效投注',
						align : 'center',
						width : '100',
						valign : 'middle'
					}, {
							field : 'multiple',
							title : '打码量倍数',
							align : 'center',
							width : '100',
							valign : 'middle'
						}, {
						field : 'upperLimit',
						title : '最高返水上限',
						align : 'center',
						width : '100',
						valign : 'middle'
					}, {
						field : 'status',
						title : '状态',
						align : 'center',
						width : '100',
						valign : 'middle',
						events : operateEvents,
						formatter : statusFormatter
					}, {
						field : 'memo',
						title : '描述',
						align : 'center',
						width : '200',
						valign : 'middle',
						formatter : memoFormatter
					}, {
						title : '操作',
						align : 'center',
						width : '50',
						valign : 'middle',
						events : operateEvents,
						formatter : operateFormatter
					} ]
			});
		}
		
		function memoFormatter(value, row, index){
			if(value==null || value==""){
				return [value ].join('');
			}
			if(value.length>8){
				return [value.substr(0, 8)+'。。' ].join('');
			}else{
				return [value ].join('');
			}
		}
		
		function bfbFormatter(value, row, index){
			if(value==undefined){
				value=0;
			}
			return [value+ '%' ].join('');
		}
		
		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ' ]
					.join('');
		}
		
		window.operateEvents = {
				'click .eidt' : function(e, value, row, index) {
					$("#model").modal('toggle');
					$("#editLabel").text("会员返水修改");
					$("#playId").val(row.id);
					$("#lotteryGameE").val(row.lotteryGame);
					$("#sportsGameE").val(row.sportsGame);
					$("#markSixTmbE").val(row.markSixTmb);
					$("#markSixZmbE").val(row.markSixZmb);
					$("#profitBetE").val(row.profitBet);
 					$("#multipleE").val(row.multiple);
					$("#upperLimitE").val(row.upperLimit);
					$("#statusNew").val(row.status);
					$("#memoE").val(row.memo);
					$("#modalSave").attr("onclick","updateSave()");
				},
				'click .stateOpen' : function(e, value, row, index) {
					closeOrOpen(row);
				},
				'click .stateClose' : function(e, value, row, index) {
					closeOrOpen(row);
				}
			};
		
		function add(){
			$("#model").modal('toggle');
			$("#editLabel").text("会员返水新增");
			$("#lotteryGameE").val("");
			$("#sportsGameE").val("");
			$("#markSixTmbE").val("");
			$("#markSixZmbE").val("");
			$("#profitBetE").val("");
 			$("#multipleE").val("");
			$("#upperLimitE").val("");
			$("#statusNew").val("2");
			$("#memoE").val("");
			$("#modalSave").attr("onclick","addSave()");
		}
		
		function addSave(){
			var curDate = new Date();
			var lotteryGame = $("#lotteryGameE").val();
			var sportsGame = $("#sportsGameE").val();
			var markSixTmb = $("#markSixTmbE").val();
			var markSixZmb = $("#markSixZmbE").val();
			var profitBet = $("#profitBetE").val();
 			var multiple = $("#multipleE").val();
			var upperLimit = $("#upperLimitE").val();
			var statusNew = $("#statusNew").val();
			var memo = $("#memoE").val();
			if(lotteryGame==""){
				aler('彩票游戏返水比例不能为空!',1);
				return false;
			}
			if(sportsGame==""){
				aler('体育游戏返水比例不能为空!',1);
				return false;
			}
			if(profitBet==""){
				aler('有效投注不能为空!',1);
				return false;
			}
			if(upperLimit==""){
				aler('返水上限不能为空!',1);
				return false;
			}
			var bc = {
				 "lotteryGame" : lotteryGame,
				 "sportsGame" : sportsGame,
				 "markSixTmb" : markSixTmb,
				 "markSixZmb" : markSixZmb,
				 "profitBet" : profitBet,
				 "multiple" : multiple,
				 "upperLimit" : upperLimit,
				 "status" : statusNew,
				 "memo" : memo,
				 "createDatetime" : curDate,
				 "stationId" : $("#methodType").val()
	        	};
			$.ajax({
				url : "${base}/admin/memberRollBack/addSave.do",
				data : bc,
				success : function(result) {
					tips('保存成功!',1);
					$("#model").modal('toggle');
					refresh();
				},
                error: function () {
                    alert('输入格式有误');
                }
			});
		}
		
		function updateSave(){
			var curDate = new Date();
			var lotteryGame = $("#lotteryGameE").val();
			var sportsGame = $("#sportsGameE").val();
			var markSixTmb = $("#markSixTmbE").val();
			var markSixZmb = $("#markSixZmbE").val();
			var profitBet = $("#profitBetE").val();
 			var multiple = $("#multipleE").val();
			var upperLimit = $("#upperLimitE").val();
			var statusNew = $("#statusNew").val();
			var memo = $("#memoE").val();
			if(lotteryGame==""){
				aler('彩票游戏返水比例不能为空!',1);
				return false;
			}
			if(sportsGame==""){
				aler('体育游戏返水比例不能为空!',1);
				return false;
			}
			if(profitBet==""){
				aler('有效投注不能为空!',1);
				return false;
			}
			if(upperLimit==""){
				aler('返水上限不能为空!',1);
				return false;
			}
			var bc = {
					"id" : $('#playId').val(),
					 "lotteryGame" : lotteryGame,
					 "sportsGame" : sportsGame,
					 "markSixTmb" : markSixTmb,
					 "markSixZmb" : markSixZmb,
					 "profitBet" : profitBet,
 					 "multiple" : multiple,
					 "upperLimit" : upperLimit,
					 "status" : statusNew,
					 "memo" : memo,
					 "modifyDatetime" : curDate,
					 "stationId" : $("#methodType").val()
	        	};
				$.ajax({
					url : "${base}/admin/memberRollBack/updateSave.do",
					data : bc,
					success : function(result) {
						tips('更新成功!',1);
						$("#model").modal('toggle');
						refresh();
					},
	                error: function () {
	                    alert('输入格式有误');
	                }
				});
		}
		
		function closeOrOpen(row){
			var state = 1;
			if(row.status==1){
				state = 2;
			}
			var bc = {
	 				"status" : state,
					"id" :row.id,
					"stationId" :row.stationId 
	        	};
			$.ajax({
				url : "${base}/admin/memberRollBack/closeOrOpen.do",
				data : bc,
				success : function(result) {
					if(row.status==1){
						tips("状态",2);
					}else{
						tips("状态",3);
					}
					refresh();
				}
			});
		}
		
		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#methodType").val();
			return params
		}

		function search() {
			refresh();
		}
	</script>
</body>
</html>