<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="keyword">账户名称</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="account"  class="form-control" id="account" placeholder="本地账户名称">
					</div>
				</div>
				
				<label class="sr-only" for="keyword">选择租户</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="stationId" id="stationId">
							<option value="">选择租户</option>
						</select>
					</div>
				</div>
				
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			getTab();
			stationList("stationId");
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/prr/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'domain',
					title : '所属网站',
					align : 'center',
					width : '200',
					valign : 'middle',
				}, 
				 {
					field : 'account',
					title : '代理名称',
					align : 'center',
					width : '180',
					valign : 'bottom'
				},
				 {
					field : 'totalMember',
					title : '总投注会员',
					align : 'center',
					width : '180',
					valign : 'bottom'
				},
				{
					field : 'totalProfitMember',
					title : '总有效投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'totalRollMoney',
					title : '总反水金额',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'realTotalBet',
					title : '真人总投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'realProfitBet',
					title : '真人总有效投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'realProfitMoney',
					title : '真人总盈利',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'realRebateMoney',
					title : '真人总返点',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'lotteryTotalBet',
					title : '彩票总投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'lotteryProfitBet',
					title : '彩票总有效投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'lotteryProfitMoney',
					title : '彩票总盈利',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'lotteryRebateMoney',
					title : '彩票总返点',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'egameTotalBet',
					title : '电子游艺总投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'egameProfitBet',
					title : '电子游艺总有效投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'egameProfitMoney',
					title : '电子游艺总盈利',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'egameRebateMoney',
					title : '电子游艺总返点',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'sportsTotalBet',
					title : '体育总投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'sportsProfitBet',
					title : '体育总有效投注',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'sportsProfitMoney',
					title : '体育总盈利',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'sportsRebateMoney',
					title : '体育总返点金额',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'rebateMonth',
					title : '返点月份',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, 
				{
					field : 'status',
					title : '返点状态',
					align : 'center',
					width : '180',
					valign : 'bottom',
					formatter:statusFormatter
				},{
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : optFormatter
				}]
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		function statusFormatter(value, row, index) {
			if(value==1){
				return "返点已计算";
			}else if(value==2){
				return "返点已经回滚";
			}else if(value==3){
				return "返点到账";
			}else{
				return "状态未知";
			}
		}
		
		function optFormatter(value, row, index) {
			var value = row.status;
			if(value==1){
				return '<span class="text-success"></span><a href="#"><span class="text-success rebate" title="">手动返点</span></a>';
			}else if(value==2){
				return '<span class="text-success"></span><a href="#"><span class="text-success rerebate" title="">重新返点</span></a>';
			}else if(value==3){
				return '<span class="text-success"></span><a href="#"><span class="text-success cancelRebate" title="">回滚</span></a>';
			}else{
				return '<span class="text-success">状态未知</span>';
			}
		}
		
		window.operateEvents = {
				'click .rebate' : function(e, value, row, index) {
					var url = '${base}/admin/prr/rebate.do';
					var stationId = row.stationId;
					var rebateMonth = row.rebateMonth;
					var account = row.account;
					var param = {};
					param["stationId"]=stationId;
					param["rebateMonth"]=rebateMonth;
					param["account"]=account;
					$.post(url,param,function(json){
						if(json.code==0||json.code=='0'){
							alert("手动返点结算成功");
							$("#datagrid_tb").bootstrapTable('refresh');
						}else{
							alert(json.msg);
						}
					});
				},
				'click .rerebate' : function(e, value, row, index) {
					var url = '${base}/admin/prr/rerebate.do';
					var stationId = row.stationId;
					var rebateMonth = row.rebateMonth;
					var account = row.account;
					var param = {};
					param["stationId"]=stationId;
					param["rebateMonth"]=rebateMonth;
					param["account"]=account;
					$.post(url,param,function(json){
						if(json.code==0||json.code=='0'){
							alert("手动重新结算成功");
							$("#datagrid_tb").bootstrapTable('refresh');
						}else{
							alert(json.msg);
						}
					});
				},
				'click .cancel' : function(e, value, row, index) {
					var url = '${base}/admin/prr/cancel.do';
					var stationId = row.stationId;
					var rebateMonth = row.rebateMonth;
					var account = row.account;
					var param = {};
					param["stationId"]=stationId;
					param["rebateMonth"]=rebateMonth;
					param["account"]=account;
					$.post(url,param,function(json){
						if(json.code==0||json.code=='0'){
							alert("回滚返点成功");
							$("#datagrid_tb").bootstrapTable('refresh');
						}else{
							alert(json.msg);
						}
					});
				}
		};
		//设置传入参数search
		function queryParams(params) {
			params["account"] = $("#account").val();
			params["stationId"] = $("#stationId").val();
			return params
		}
		//站点列表
		function stationList(id) {
			$.ajax({
				url : "${base}/admin/station/list.do",
				success : function(result) {
					var col = "<option value=\"\">选择租户</option>";
					for(var j in result.rows){
						console.log(result.rows[j].id+"||"+result.rows[j].name);
						col+='<option value="'+result.rows[j].id+'">'+result.rows[j].name+'('+result.rows[j].floder+')</option>';
					}
					$('#'+id).html(col);
				}
			});
		}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
