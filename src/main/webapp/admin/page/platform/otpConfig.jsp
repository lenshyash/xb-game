<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
/* .icoFontlist:hover
{
    white-space:normal; 
    overflow:auto;

} */
.icoFontlist{
    display: inline-block;
	  max-width:365px; 
	  word-wrap:break-word; 
	  text-overflow:ellipsis; 
	  white-space:nowrap; 
	  overflow:hidden; 
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab" >绑定动态口令</a></li>
			<li><a href="#tab2" data-toggle="tab">过滤管理员</a></li>
		</ul>
		<div class="tab-content">
		    <div class="tab-pane active" id="tab1">
				<div id="toolbar">
					<div id="search" class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<select class="form-control" id="sstationId">
									<option value="0">全部站点</option>
									<c:forEach items="${stations}" var="s"><option value="${s.id}">${s.name}</option></c:forEach>
								</select>
							</div>
							<div class="input-group">
								<input type="text" class="form-control" id="siccid" placeholder="手机卡唯一标识">
							</div>
						</div>
						<button class="btn btn-primary" onclick="search();">查询</button>
						<button class="btn btn-primary" onclick="add();">新增</button>
					</div>
				</div>
				<table id="datagrid_tb"></table>
				<div style="font-size:16px;color:red;">
				<p>1.站点绑定后，所有管理员都将需要动态密码(谷歌验证码)，除非特别指定某个管理员不需要动态密码(谷歌验证码)</p>
				<p>2.谷歌验证码单个站点只能绑定一个</p>
				</div>
			</div>
			<div class="tab-pane" id="tab2">
				<br/>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div id="toolbar">
							<div class="form-inline">
								<select onchange="selectStation(this.value)" class="form-control">
									<option value="0">全部站点</option>
									<c:forEach items="${stations}" var="s"><option value="${s.id}">${s.name}</option></c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="panel-body" id="escapeDivId">
					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">绑定动态密码</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="otpId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="30%" class="text-right">站点：</td>
								<td><select class="form-control" id="stationId">
									<option value="0">全部站点</option>
									<c:forEach items="${stations}" var="s"><option value="${s.id}">${s.name}</option></c:forEach>
								</select></td>
							</tr>
							<tr>
								<td width="30%" class="text-right">验证类型：</td>
								<td><select class="form-control" id="checkType">
									<option value="1">动态密码</option>
									<option value="2">谷歌验证码</option>
								</select></td>
							</tr>
							<tr id = "icTr">
								<td class="text-right">手机卡唯一标识：</td>
								<td><input type="text" class="form-control" id="iccid" /></td>
							</tr>
							<tr id="emailTr" class="hidden">
								<td class="text-right">邮箱：</td>
								<td><input type="text" class="form-control" id="email" /></td>
							</tr >
							<tr id="pwdTr" >
								<td class="text-right">APP密码：</td>
								<td><input type="text" class="form-control" id="password" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
	$(function() {
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/admin/otpConfig/list.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			columns : [ {
				field : 'stationId',
				title : '站点',
				align : 'center',
				formatter : function(value, row, index) {
					if(!value){
						return "-";
					}
					switch(value){
					<c:forEach items="${stations}" var="s">case ${s.id}:return "${s.name}";</c:forEach>
					}
				}
			}, {
				field : 'type',
				title : '验证类型',
				align : 'center',
				formatter : typeFormatter
			}, {
				field : 'iccid',
				title : '手机卡唯一标识',
				align : 'center'
			}, {
				field : 'password',
				title : '秘钥',
				align : 'center'
			}, {
				field : 'url',
				title : '二维码地址',
				align : 'center',
				formatter : urlFormatter
			}, {
				field : 'createDatetime',
				title : '创建时间',
				align : 'center',
				formatter : DateUtil.formatDatetime
			}, {
				title : '操作',
				align : 'center',
				valign : 'middle',
				events : operateEvents,
				formatter : operateFormatter
			} ]
		});
	});
	function operateFormatter(value, row, index) {
		if(row.type==2){
			return '<a class="remove" href="javascript:void(0)" title="删除"><i class="glyphicon glyphicon-remove"></i></a>';
		}else{
			return '<a class="eidt" href="javascript:void(0)" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>   '+
			'<a class="remove" href="javascript:void(0)" title="删除"><i class="glyphicon glyphicon-remove"></i></a>';
		}
	}
	function urlFormatter(value, row, index) {
		var key = 'url'+index;
		return '<span class ="icoFontlist">'+value+'</span><span class="label label-primary copy-btn" onclick="copy(\''+key+'\')">复制</span><input type="text" style="position:absolute;margin-left: 1000px;" id="'+key+'" value="'+value+'">';
		
		
// 		if (!value) {
// 			return "";
// 		}
// 		if (value.length < 20) {
// 			return value;
// 		}
// 		return [ '<a class="open-text" dialog-text="'+value+'" dialog-title="链接详情" title="'+value+'">', '</span>' ].join(value.substr(0,20)+"...");
	}
	function typeFormatter(value, row, index) {
		if(value==1){
			return "动态密码";
		}else if(value ==2){
			return "谷歌验证码";
		}
		return "-";
	}
	window.operateEvents = {
		'click .eidt' : function(e, value, row, index) {
			$("#editmodel").modal('toggle');
			$("#otpId").val(row.id);
			$("#stationId").val(row.stationId).prop("disabled",true);
			$("#iccid").val(row.iccid).prop("readonly",true);
			$("#password").val(row.password);
		},
		'click .remove' : function(e, value, row, index) {
			$.ajax({
				url : "${base}/admin/otpConfig/delete.do?id="+row.id,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
	};
	$('#checkType').change(function(){
		var type = $(this).val();
		switch(type){
			case "1":
				$("#icTr").removeClass("hidden");
				$("#emailTr").addClass("hidden");
				$("#pwdTr").removeClass("hidden");
				break;
			case "2":
				$("#icTr").addClass("hidden");
				$("#pwdTr").addClass("hidden");
				$("#emailTr").removeClass("hidden");
				break;
		}
	});
	function copy(id){
	    var Url2=document.getElementById(id);
	    Url2.select(); // 选择对象
	    document.execCommand("Copy"); // 执行浏览器复制命令
	    layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
	}
	
	//设置传入参数
	function queryParams(params) {
		params["stationId"] = $("#sstationId").val();
		params["iccid"] = $("#siccid").val();
		return params
	}
	
	function search() {
		$("#datagrid_tb").bootstrapTable('refresh');
	}

	function add() {
		$("#editmodel").modal('toggle');
		$("#otpId").val("");
		$("#stationId").val($("#sstationId").val()).prop("disabled",false);
		$("#iccid").val("").prop("readonly",false);
		$("#password").val("");
	}

	function save() {
		$.ajax({
			url : "${base}/admin/otpConfig/save.do",
			data : {
				id : $("#otpId").val(),
				stationId : $("#stationId").val(),
				iccid : $("#iccid").val(),
				type : $("#checkType").val(),
				password : $("#password").val(),
				email : $("#email").val()
			},
			success : function(result) {
				$("#datagrid_tb").bootstrapTable('refresh');
			}
		});
	}
	
	function selectStation(stationId){
		if(stationId>0){
			$.ajax({
				url : "${base}/admin/otpConfig/getAgentUsers.do",
				data:{
					stationId : stationId
				},
				success : function(result) {
					if(result.msg){
						layer.msg(result.msg);
						$("#escapeDivId").html("");
						return;
					}
					var html = template('account_list', result);
					$("#escapeDivId").html(html);
				}
			});
		}else{
			$("#escapeDivId").html("");
		}
	}
	function changeEscapeStatus(stationId,account,escId){
		var msg,icon=3,esc=false;
		if(escId>0){
			msg="确定要为管理员“"+account+"”启用动态密码？";
		}else{
			msg="确定要取消管理员“"+account+"”的动态密码功能？";
			icon=5;
			esc=true;
		}
		layer.confirm(msg,{icon:icon, title:'确认'},function(){
			$.ajax({
				url : "${base}/admin/otpConfig/changeEscapeStatus.do",
				data:{
					stationId : stationId,
					account:account,
					esc:esc
				},
				success : function(result) {
					if(result.msg){
						layer.msg(result.msg);
						return;
					}
					layer.msg("操作成功");
					selectStation(stationId);
				}
			});
		});
	}
</script>
<script id="account_list" type="text/html">
{{if data.length == 0}}
	<h1> 该站点下没有账号 </h1>
{{/if}}
{{each data as a}}
	{{if !a.escId }}
		<div style="min-width:100px;margin-left:10px;float:left" class="alert alert-danger" role="alert">
	{{/if}}
	{{if a.escId}}
		<div style="min-width:100px;margin-left:10px;float:left" class="alert alert-success" role="alert">
	{{/if}}
		<a onclick="changeEscapeStatus({{a.stationId}},'{{a.account}}',{{a.escId||0}})" href="javascript:void(0)" class="alert-link">{{a.account}}
			{{if a.escId}}(未启用动态密码){{/if}}
		</a>
	</div>
{{/each}}
</script>
</body>
</html>