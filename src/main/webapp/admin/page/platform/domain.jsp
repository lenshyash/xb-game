<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="domain">域名</label> <label class="sr-only" for="groupId">站点</label>
				<div class="form-group">
					<div class="input-group">
						<select id="groupId" class="form-control"></select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="sdomain" placeholder="域名">
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="sagentName" placeholder="代理">
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" onclick="add();">新增</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑域名</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="domainId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">站点名称：</td>
								<td width="35%"><select id="stationId" class="form-control"></select></td>
								<td width="15%" class="text-right">状态：</td>
								<td width="35%"><select id="status" class="form-control">
										<option value="1">禁用</option>
										<option value="2" selected>启用</option>
								</select></td>
							</tr>
							<tr>
								<td class="text-right">域名：</td>
								<td colspan="2"><textarea id="domain" class="form-control" rows="3"></textarea></td>
								<td>数据格式：域名,代理账号<br/>域名       代理账号<br/>域名（可以没有代理账号）</td>
							</tr>
							<tr>
								<td class="text-right">默认代理：</td>
								<td><input type="text" class="form-control" id="agentName" /></td>
								<td class="text-right">注册开关：</td>
								<td><select id="isRegSwitch" class="form-control">
										<option value="1">禁用</option>
										<option value="2"selected>启用</option>
								</select></td>
							</tr>
							<tr>
								<td class="text-right">线路检测开关：</td>
								<td><select id="isXlSwitch" class="form-control">
										<option value="1" selected>禁用</option>
										<option value="2">启用</option>
								</select></td>
								<td class="text-right">域名类型：</td>
								<td><select id="type" class="form-control">
										<option value=""selected>通用域名</option>
										<option value="1">后台域名</option>
										<option value="2">前台域名</option>
										<option value="3">支付域名</option>
										<option value="4">聊天室域名</option>
										<option value="5">app域名</option>
								</select></td>
							</tr>
							<tr>
								<td class="text-right">前台默认主页：</td>
								<td colspan=3><input type="text" class="form-control" id="defaultHome" /></td>
								
							</tr>
							<tr>
								<td class="text-right">模板代码：</td>
								<td><input type="text" class="form-control" id="folder" /></td>
								<td class="text-right">绑定站点名称：</td>
								<td><input type="text" class="form-control" id="domainStationName" /></td>
							</tr>
							<tr>
								<td class="text-right">是否启用聊天室：</td>
								<td><select id="isChatSwitch" class="form-control">
									<option value="1" selected>禁用</option>
									<option value="2">启用</option>
								</select>
								</td>
								<td class="text-right">域名指定聊天室：</td>
								<td><input type="text" class="form-control" id="domainPlatToken" /></td>
							</tr>
							<tr>
								<td class="text-right">是否启用接口限制：</td>
								<td><select id="interfaceLimit" class="form-control">
									<option value="1" selected>禁用</option>
									<option value="2">启用</option>
								</select>
								</td>
								<td class="text-right">接口单秒限制访问频率：</td>
								<td><input type="number" class="form-control" id="limitNum" step="0.01"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script id="stationlst_tpl" type="text/html">
	<option value="0">请选择站点</option>
	{{each data as st}}
			<option value="{{st.id}}">{{st.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var curStation = 0;
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/domain/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'name',
					title : '站点名称',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'floder',
					title : '域名目录',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'domain',
					title : '域名',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'agentName',
					title : '默认代理',
					align : 'center',
					width : '180',
					valign : 'bottom'
				}, {
					field : 'status',
					title : '站点状态',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : statusFormatter
				}
				,{
					field : 'isRegSwitch',
					title : '注册开关',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : isRegSwitchFormatter
				}
				,{
					field : 'isXlSwitch',
					title : '线路检测开关',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : isXlSwitchFormatter
				},{
					field : 'isChatSwitch',
					title : '域名聊天室开关',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : isChatSwitchFormatter
				},{
						field : 'interfaceLimit',
						title : '域名访问限制开关',
						align : 'center',
						width : '200',
						valign : 'middle',
						events : operateEvents,
						formatter : interfaceLimitFormatter
					}
				,{
					field : 'type',
					title : '域名类型',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : typeFormatter
				},{
					field : 'defaultHome',
					title : '前台默认主页',
					align : 'center',
					width : '200',
					valign : 'middle'
				},{
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function statusFormatter(value, row, index) {

			var sn = GlobalTypeUtil.getTypeName(2, 1, value);
			if (value === 2) {
				return [ '<span class="text-success">', '</span>' ].join(sn);
			}
			return [ '<span class="text-danger">', '</span>' ].join(sn);
		}

		function isRegSwitchFormatter(value, row, index){
			if(value == 2){
				return ['<span class="text-success">启用</span><a href="#"><span class="isRegSwitch text-danger">(禁用)</span></a>' ].join('');
			}else{
				return ['<span class="text-danger">禁用</span><a href="#"><span class="isRegSwitch text-success">(启用)</span></a>' ].join('');
			}
		}
		
		
		function isXlSwitchFormatter(value, row, index){
			if(value == 2){
				return ['<span class="text-success">启用</span><a href="#"><span class="isXlSwitch text-danger">(禁用)</span></a>' ].join('');
			}else{
				return ['<span class="text-danger">禁用</span><a href="#"><span class="isXlSwitch text-success">(启用)</span></a>' ].join('');
			}
		}
		function isChatSwitchFormatter(value, row, index){
			if(value == 2){
				return ['<span class="text-success">启用</span><a href="#"><span class="isChatSwitch text-danger">(禁用)</span></a>' ].join('');
			}else{
				return ['<span class="text-danger">禁用</span><a href="#"><span class="isChatSwitch text-success">(启用)</span></a>' ].join('');
			}
		}
		function interfaceLimitFormatter(value, row, index){
			if(value == 2){
				return ['<span class="text-success">启用</span><a href="#"><span class="interfaceLimit text-danger">(禁用)</span></a>' ].join('');
			}else{
				return ['<span class="text-danger">禁用</span><a href="#"><span class="interfaceLimit text-success">(启用)</span></a>' ].join('');
			}
		}

		
		function typeFormatter(value, row, index){
			if(value == 2){
				return ['前台域名' ].join('');
			} else if(value == 1){
				return ['后台域名' ].join('');
			}else if (value == 3){
				return ['支付域名' ].join('');
			}else if (value == 4){
				return ['聊天室域名' ].join('');
			}else if (value == 5){
				return ['app域名' ].join('');
			}else {
				return ['通用域名' ].join('');
			}
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
					'<a class="start" href="javascript:void(0)" title="启用">',
					'<i class="glyphicon glyphicon-ok-circle"></i>', '</a>  ',
					'<a class="stop" href="javascript:void(0)" title="停用">',
					'<i class="glyphicon glyphicon-ban-circle"></i>', '</a>',
					'<a class="remove" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
					.join('');
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {

				$("#editmodel").modal('toggle');
				$("#domainId").val(row.id);
				$("#domain").val(row.domain);
				$("#stationId").val(row.stationId);
				$("#status").val(row.status||2);
				$("#agentName").val(row.agentName);
				$("#isRegSwitch").val(row.isRegSwitch);
				$("#isXlSwitch").val(row.isXlSwitch);
				$("#type").val(row.type);
				$("#defaultHome").val(row.defaultHome);
				$("#folder").val(row.folder);
				$("#domainStationName").val(row.domainStationName);
				$("#domainPlatToken").val(row.domainPlatToken);
				$("#isChatSwitch").val(row.isChatSwitch);
				$("#interfaceLimit").val(row.interfaceLimit);
				$("#limitNum").val(row.limitNum);
			},
			'click .start' : function(e, value, row, index) {
				row.domainId = row.id;
				open(row);
			},
			'click .stop' : function(e, value, row, index) {
				row.domainId = row.id;
				close(row);
			},
			'click .remove' : function(e, value, row, index) {
				row.domainId = row.id;
				remove(row);
			},
			'click .isXlSwitch' : function(e, value, row, index) {
				row.domainId = row.id;
				if(row.isXlSwitch==null){
					row.isXlSwitch=1;
				}
				isXlSwitch(row);
			},
			'click .isRegSwitch' : function(e, value, row, index) {
				row.domainId = row.id;
				if(row.isRegSwitch==null){
					row.isRegSwitch=1;
				}
				isRegSwitch(row);
			},'click .isChatSwitch' : function(e, value, row, index) {
				row.domainId = row.id;
				if(row.isChatSwitch==null){
					row.isChatSwitch=1;
				}
				isChatSwitch(row);
			},'click .interfaceLimit': function(e, value, row, index) {
				row.domainId = row.id;
				if(row.interfaceLimit==null){
					row.interfaceLimit=1;
				}
				interfaceLimit(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params["stationId"] = curStation;
			params["domain"] = $("#sdomain").val();
			params["agentName"]=$("#sagentName").val();
			return params
		}
		$(function() {
			initCombo();
			bindstationLst();
			getTab();
		})

		function initCombo() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(data) {
					var eachdata = {
						"data" : data
					};
					var html = template('stationlst_tpl', eachdata);
					$("#stationId").append(html);
					$("#groupId").append(html);
				}
			});
		}

		function bindstationLst() {
			$("#groupId").change(function() {
				var selval = $(this).children('option:selected').val();
				curStation = selval;
				$("#datagrid_tb").bootstrapTable('refresh');
			});
		}

		function open(row) {
			$.ajax({
				url : "${base}/admin/domain/open.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function close(row) {
			$.ajax({
				url : "${base}/admin/domain/close.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function remove(row) {
			$.ajax({
				url : "${base}/admin/domain/delete.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function add() {
			$("#editmodel").modal('toggle');
			$("#domainId").val("");
			$("#domain").val("");
			$("#stationId").val($("#groupId").val());
			$("#status").val("2");
			$("#agentName").val("");
			$("#isRegSwitch").val("2");
			$("#isXlSwitch").val("1");
			$("#type").val("2");
			$("#defaultHome").val("");
			$("#folder").val("");
			$("#domainStationName").val("");
			$("#isChatSwitch").val("2");
			$("#interfaceLimit").val("2");
			$("#limitNum").val("0");
		}

		function save() {
			$.ajax({
				url : "${base}/admin/domain/save.do",
				data : {
					id : $("#domainId").val(),
					stationId : $("#stationId").val(),
					domain : $("#domain").val(),
					status : $("#status").val(),
					agentName : $("#agentName").val(),
					isRegSwitch : $("#isRegSwitch").val(),
					isXlSwitch : $("#isXlSwitch").val(),
					type : $("#type").val(),
					defaultHome : $("#defaultHome").val(),
					folder:$("#folder").val(),
					domainStationName:$("#domainStationName").val(),
					domainPlatToken:$("#domainPlatToken").val(),
					isChatSwitch:$("#isChatSwitch").val(),
					interfaceLimit:$("#interfaceLimit").val(),
					limitNum:$("#limitNum").val(),
				},
				success : function(result) {
					if(!result.success){
						layer.msg(result.msg);
						return;
					}
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		
		function isRegSwitch(row){
			$.ajax({
				url : "${base}/admin/domain/updateIsRegSwitch.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		
		function isXlSwitch(row){
			$.ajax({
				url : "${base}/admin/domain/updateIsXlSwitch.do",
				data : row,
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		
		function isChatSwitch(row){
			$.ajax({
				url : "${base}/admin/domain/updateIsChatSwitch.do",
				data : {
					isChatSwitch:row.isChatSwitch,
					domainId:row.domainId
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
		function interfaceLimit(row){
			$.ajax({
				url : "${base}/admin/domain/updateInterfaceLimit.do",
				data : {
					interfaceLimit:row.interfaceLimit,
					domainId:row.domainId
				},
				success : function(result) {
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
	</script>
</body>
</html>