<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" id="begin" value="${curDate }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${curDate }" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
				</div>
				
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
			bindbtn();
			getTab();
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/global/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				sidePagination: 'client',
				pageSize:50,
				columns : [{
					title : '站点',
					align : 'center',
					valign : 'middle',
					formatter:function(value,row,index){
						return row.stationName+"("+row.folder+")";
					}
				},{
                    title: "彩票",
                    valign:"center",
                    align:"center",
                    formatter:function(value,row,index){
                    	var r= row.lotBetAmount||0;
						r =r-(row.lotWinAmount||0);
						r = parseFloat(r);
						r=Math.round(r*100) / 100;
						var s=(row.lotBetAmount||0)+"<br>";
						s=s+'<span class="text-danger">' + (row.lotWinAmount||0)+"</span><br>";
						s=s+'<span class="text-success">'+r+'</span>';
                    	return s;
					}
                },{
                    title: "体育",
                    valign:"sport",
                    align:"center",
                    formatter:function(value,row,index){
                    	var r= row.sportsBetAmount||0;
						r =r-(row.sportsWinAmount||0);
						r = parseFloat(r);
						r=Math.round(r*100) / 100;
						var s=(row.sportsBetAmount||0)+"<br>";
						s=s+'<span class="text-danger">' + (row.sportsWinAmount||0)+"</span><br>";
						s=s+'<span class="text-success">'+ r +'</span>';
                    	return s;
					}
                },{
                    title: "六合彩",
                    valign:"center",
                    align:"center",
                    formatter:function(value,row,index){
                    	var r= row.sixBetAmount||0;
						r =r-(row.sixWinAmount||0);
						r = parseFloat(r);
						r=Math.round(r*100) / 100;
						var s=(row.sixBetAmount||0)+"<br>";
						s=s+'<span class="text-danger">' + (row.sixWinAmount||0)+"</span><br>";
						s=s+'<span class="text-success">'+r+'</span>';
                    	return s;
					}
                },{
					field : 'sysLotName',
					title : '系统彩',
					align : 'center',
					width:'150px'
				}, {
					field : 'depositTotal',
					title : '存款总计',
					align : 'center',
					formatter : moneyFormatter
				}, {
					field : 'depositHandlerArtificial',
					title : '手动确认充值金额',
					align : 'center',
					formatter : moneyFormatter
				}, {
					field : 'manualDepositTotal',
					title : '手动加款',
					align : 'center',
					formatter : moneyFormatter
				}, {
					field : 'withdrawTotal',
					title : '提款总计',
					align : 'center',
					formatter : moneyFormatter
				}, {
					field : 'manualWithdrawTotal',
					title : '手动扣款',
					align : 'center',
					formatter : moneyFormatter
				}]
			});
		}
		function moneyFormatter(value, row, index) {
			if (value === undefined) {
				return value;
			}
			var f = parseFloat(value);
			f=Math.round(f*100) / 100;
			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ].join(f);
			}
			return [ '<span class="text-primary">', '</span>' ].join(f);
		}
		//设置传入参数search
		function queryParams(params) {
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin, end);
				search();
				});
			}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
