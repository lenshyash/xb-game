<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	vertical-align : middle;
}
.table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th{
	padding:13px 15px!important;
}
</style>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div class="row list-group">
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataSynchronization/synDailiData.do" id="btn1">报表数据同步</button></div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataSynchronization/fixUpLowRelationship.do" id="btn2">修复上下级关系</button></div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataSynchronization/delSysaccountCache.do" id="btn3">删除代理及所有下级缓存</button></div>
		</div>
	</div>
	<!-- 新增 -->
	<div class="modal fade" id="newModel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width:600px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">数据同步</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationId" class="form-control">
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td class="text-right">开始时间：</td>
								<td class="text-left">
								<input type="text" class="form-control" id="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
								</td>
							</tr>
							<tr>
								<td class="text-right">结束时间：</td>
								<td class="text-left">
									<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteDataRecordBtn">确定</button>
				</div>
			</div>
		</div>
	</div>
	
		<!-- 新增 2-->
	<div class="modal fade" id="newModeltwo" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width:600px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">数据同步</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationIdTwo" class="form-control">
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteDataRecordBtnTwo">确定</button>
				</div>
			</div>
		</div>
	</div>
	
			<!-- 新增 3-->
	<div class="modal fade" id="newModelThree" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width:600px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">数据同步</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationIdThree" class="form-control">
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteDataRecordBtnThree">确定</button>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		var startTime = '${startTime }';
		var endTime = '${endTime }';
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
		}
		$(function() {
			getTab();
		})

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

	</script>
	
	<script type="text/javascript">
		$(function() {
			var delUrl="";
			$("#btn1[url]").click(function(){
				delUrl=$(this).attr("url");
				$("#editLabel").html($(this).text());
				$("#newModel").modal('toggle');
			});
			$("#deleteDataRecordBtn").click(function(){
				if(delUrl){
					$.post(delUrl,{stationId:$("#drdStationId").val(),end:$("#end").val(),begin:$("#begin").val()},function(d){
						if(d.success){
							layer.alert('报表数据同步成功！');
						}else{
							layer.alert('报表数据同步失败！');
							}
						
					},"json");
					delUrl="";
				}
			});
		})
		
		$(function() {
			var delUrl="";
			$("#btn2[url]").click(function(){
				delUrl=$(this).attr("url");
				$("#editLabel").html($(this).text());
				$("#newModeltwo").modal('toggle');
			});
			$("#deleteDataRecordBtnTwo").click(function(){
				if(delUrl){
					$.post(delUrl,{stationId:$("#drdStationIdTwo").val()},function(d){
						if(d.success){
							layer.alert('修复上下级关系成功！');
						}else{
							layer.alert('修复上下级关系失败！');
							}
						
					},"json");
					delUrl="";
				}
			});
		})
		
		$(function() {
			var delUrl="";
			$("#btn3[url]").click(function(){
				delUrl=$(this).attr("url");
				$("#editLabel").html($(this).text());
				$("#newModelThree").modal('toggle');
			});
			$("#deleteDataRecordBtnThree").click(function(){
				if(delUrl){
					$.post(delUrl,{stationId:$("#drdStationIdThree").val()},function(d){
						if(d.success){
							layer.alert('删除缓存成功！');
						}else{
							layer.alert('删除缓存成功！');
							}
						
					},"json");
					delUrl="";
				}
			});
		})
	</script>
</body>
</html>
