<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" id="ip" placeholder="请输入IP查询">
						</div>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" type="button" onclick="showAddBlackIp();">添加IP</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	
	<div class="modal fade" id="balckipmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">添加IP</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">IP：</td>
								<td><input type="text" class="form-control" name="ip" id="ipabc" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addBlackIpSubmit();">删除</button>
				</div>
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		var types = {};
		var curDiv = 1;
		var startTime = '${startTime }';
		var endTime = '${endTime }';
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/blackIp/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'ip',
					title : 'ip',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					width : '80',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				}]
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="detail" href="javascript:void(0)" title="详情">',
					'详情', '</a>  ' ].join('');
		}
		
		function deleteCache(ip,obj){
			$.ajax({
				url : "${base}/admin/blackIp/add.do",
				data : {ip:ip},
				success : function(result) {
					layer.msg(result.msg||"ip已添加");
					if(result.success && obj){
						$(obj).parent().parent().remove();
					}
				}
			}); 
		}
		
		function addBlackIpSubmit(){
			var ip=$("#ipabc").val();
			console.log("111"+ip);
			deleteCache(ip);
		}
		
		function showAddBlackIp(){
			$("#balckipmodel").modal('toggle');
		}
			
		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$("#detailLog").modal('toggle');
				$("#account_span").html(row.account);
				$("#type_span").html(types[row.payType]);
				$("#createTime_span").html(DateUtil.formatDatetime(row.createDatetime));
				$("#crateIp_span").html(row.accountIp);
				$("#content").val(row.logContent);
				$("#remark").val(row.remark);
				$("#params").val(row.params);
			}
		};
	
		function del(value) {
			Msg.confirm("是否删除用户" + value.ip + "?", function() {
				$.ajax({
					url : "${base}/admin/blackIp/delete.do",
					data : value,
					success : function(result) {
						$("#datagrid_tb").bootstrapTable('refresh');
					}
				});
			});
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
	
		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '</a> ',
					'<a class="del" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a> ' ]
					.join('');
		}
		
		window.operateEvents = {
				'click .eidt' : function(e, value, row, index) {
					$("#addmodel").modal('toggle');
					$("#account").val(row.account);
					$("#groupId").val(row.groupId);
					$("#status").val(row.status);
					$("#accountId").val(row.id);
					$("#pwd").val("");
					$("#rpwd").val("");

				},
				'click .del' : function(e, value, row, index) {
					del(row);
				},
				'click .stateOpen' : function(e, value, row, index) {
					closeOrOpen(row);
				},
				'click .stateClose' : function(e, value, row, index) {
					closeOrOpen(row);
				}
			};
		
		function typeFormatter(value, row, index) {
			return types[value];
		}

		//设置传入参数
		function queryParams(params) {
			params['ip'] = $("#ip").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		$(function() {
			getTab();
			
		})

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
	</script>
</body>
</html>