<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#" data-toggle="tab" id="controlnav">总控管理日志</a></li>
			<li><a href="#" data-toggle="tab" id="platformnav">平台管理日志</a></li>
		</ul>
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group hidden" id="station_combo_div">
							<label class="sr-only" for="type">选择站点</label> <select class="form-control" id="stationId">
							</select>
						</div>
						<div class="input-group">
							<label class="sr-only" for="stype">搜索类型</label> <select class="form-control" id="stype">
							</select>
						</div>
						<div class="input-group">
							<input type="text" class="form-control" id="saccount" placeholder="输入用户名查询">
						</div>
						<div class="input-group">
							<input type="text" class="form-control" id="scontent" placeholder="输入内容查询">
						</div>
						<div class="input-group">
							<input type="text" class="form-control" id="ip" placeholder="输入操作ip查询">
						</div>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="detailLog" tabindex="-1" role="dialog" aria-labelledby="detailLogLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="detailLogLabel">日志详情</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="pwdatId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">用户名：</td>
								<td width="80%" class="text-left"><span id="account_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">日志类型：</td>
								<td width="80%" class="text-left"><span id="type_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作时间：</td>
								<td width="80%" class="text-left"><span id="createTime_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作IP：</td>
								<td width="80%" class="text-left"><span id="crateIp_span"></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作记录：</td>
								<td width="80%" class="text-left"><textarea id="content" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">参数：</td>
								<td width="80%" class="text-left"><textarea id="params" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作备注：</td>
								<td width="80%" class="text-left"><textarea id="remark" class="form-control"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script id="combolst_tpl" type="text/html">
	<option value="0">请选择{{opName}}</option>
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var logTypes = {
			"1" : [],
			"2" : []
		};
		var types = {};
		var curDiv = 1;
		var startTime = '${startTime }';
		var endTime = '${endTime }';
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/operationLog/control.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '账号',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'logType',
					title : '日志类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'logContent',
					title : '操作记录',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'accountIp',
					title : 'IP',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'address',
					title : 'IP地址',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'createDatetime',
					title : '操作时间',
					align : 'center',
					width : '200px',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="detail" href="javascript:void(0)" title="详情">',
					'详情', '</a>  ' ].join('');
		}

		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$("#detailLog").modal('toggle');
				$("#account_span").html(row.account);
				$("#type_span").html(types[row.logType]);
				$("#createTime_span").html(DateUtil.formatDatetime(row.createDatetime));
				$("#crateIp_span").html(row.accountIp);
				$("#content").val(row.logContent);
				$("#remark").val(row.remark);
				$("#params").val(row.params);
			}
		};

		// 切变表格内容
		function chgTab(url) {

			$("#datagrid_tb").bootstrapTable('refreshOptions', {
				url : url,
				pageNumber : 1
			});
		}

		function bindNavTab() {
			$("a[data-toggle='tab']").click(function() {
				var id = this.id;
				if (id === "controlnav") {
					$("#station_combo_div").addClass("hidden");
					$("#stationId").val(0);
					$("#begin").val(startTime);
					$("#end").val(endTime);
					chgTab("${base}/admin/operationLog/control.do");
					curDiv = 1;
					chgLogTypes(curDiv);
					$("#saccount").val("");
				} else if (id === "platformnav") {
					$("#station_combo_div").removeClass("hidden");
					$("#stationId").val(0);
					$("#begin").val(startTime);
					$("#end").val(endTime);
					chgTab("${base}/admin/operationLog/platform.do");
					curDiv = 2;
					chgLogTypes(curDiv);
					$("#saccount").val("");
				}
			});
		}

		function initStationLst() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result,
						"opName" : "站点"
					};
					var html = template('combolst_tpl', eachdata);
					$("#stationId").html(html);
				}
			});
		}

		function initLogTypes() {
			$.ajax({
				url : "${base}/admin/operationLog/typecombo.do",
				success : function(result) {
					toLogTypeMap(result);
					chgLogTypes(curDiv);
				}
			});
		}

		function toLogTypeMap(datas) {
			for (var i = 0; i < datas.length; i++) {
				logTypes[datas[i].platform].push(datas[i]);
				types[datas[i].id] = datas[i].name;
			}
		}

		function chgLogTypes(flag) {
			var eachdata = {
				"data" : logTypes[flag],
				"opName" : "类型"
			};
			var html = template('combolst_tpl', eachdata);
			$("#stype").html(html);
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function typeFormatter(value, row, index) {
			return types[value];
		}

		function remarkFormatter(value, row, index) {
			return [ "<p class='text-danger'>", "</p>" ].join(value);
		}

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#stationId").val();
			params['account'] = $("#saccount").val();
			params['content'] = $("#scontent").val();
			params['type'] = $("#stype").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			params['ip'] = $("#ip").val();
			return params
		}
		$(function() {
			initLogTypes();
			getTab();
			initStationLst();
			bindNavTab();
		})

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
	</script>
</body>
</html>