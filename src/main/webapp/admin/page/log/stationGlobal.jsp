<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>

<body>

	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<script type="text/javascript" src="../js/getDateUtils.js"></script>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#" data-toggle="tab">站点全局报表</a></li>
		</ul>
<form class="table-tool" method="post" id="report_global_form_id">
	<div class="form-group fui-data-wrap"   id="toolbar"><br>
							<div class="input-group">
							<select class="form-control" id="stationId" name="stationId">
							</select>
							</div>
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" id="begin" placeholder="开始日期" id="begin"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today' onclick="getTodayDate2()" >今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday' onclick="getLastdayDate2()">昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek' onclick="getThisWeekDate2()">本周</button>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agent" placeholder="代理及下线查询">
			</div>
			<button class="btn btn-primary search-btn fui-date-search" type="button">查询</button>
			<button class="btn btn-primary refresh-btn" type="button">刷新</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" id="end" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek' onclick="getLastWeekDate2()">上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth' onclick="getThisMonthDate2()">本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth' onclick="getLastMonthDate2()">上月</button>
				<input type="text" class="form-control" name="account" value="${saccount }" placeholder="会员帐号">
			</div>
			<div class="input-group" style="width: 266px;">
			<input type="text" class="form-control" name="filterAccount"  placeholder="查询过滤账号（必须用英文逗号隔开）">

			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" selected="selected">普通</option>
					<option value="2">引导</option>
				</select>
			</div>
		</div>
	</div>
</form>



<div class="container">
<table id="report_global_datagrid_tb_base" class="center"></table>
<br>

<table id="report_global_datagrid_tb_base_ex"></table>
<br>
<table id="report_global_datagrid_tb_self"></table>
<br>
<table id="report_global_datagrid_tb_other"></table>
<br>
</div>
<div class="container">
<div id="remark_div" style="padding: 10px;">
	<span>输赢公式：</span><span class="text-danger">投注-派奖=输赢</span><br> <span>全部输赢公式：</span><span class="text-danger">各类游戏输赢总和-代理返点-会员反水=全部输赢</span><br> <span>派奖总计：</span> <span
		class="text-danger">因为派奖是定时执行任务，会出现时间临界点情况，当存在24点前的投注，会造成些许误差</span>
	<div style="color:blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
</div>
</div>


<script id="combolst_tpl" type="text/html">
<option value="0">请选择</option>
{{each data as tl}}
		<option value="{{tl.id}}">{{tl.name}}</option>
{{/each}}
</script>

<script>var baseInfo={account:"${USER_SESSION_AGENT.account}",baseUrl:"${base}"};</script>
<script src="${base}/common/js/require.js"></script>

<script type="text/javascript">
	window.UEDITOR_HOME_URL="${base}/common/js/UEditor/";
	require.config({
		baseUrl: '${base}/common',
		paths: {
			jquery: 'jquery/jquery-1.12.4.min'
			,jquery_validation:'js/jquery-validate/jquery.validate.min'
			,bootstrap:'bootstrap/3.3.7/js/bootstrap.min'
			,bootstrap_editable:'bootstrap/editable/js/bootstrap-editable.min'
			,bootstrapTable:'bootstrap-table/bootstrap-table.min'
			,moment:'js/moment/moment.min'
			,moment_zh:'js/moment/locale/zh-cn'
			,datetimepicker: 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min'
			,layer:'js/layer3/layer.min'
			,Fui:'js/fui/fui.js?v=1.1.4'
			,fui_table:'js/fui/fui-table.js?v=1.0.5'
			,fui_form:'js/fui/fui-form.js?v=1.0.2'
			,fui_datetime:'js/fui/fui-datetime.js?v=1.0.1'
			,fui_switch:'js/fui/fui-switch.js?v=1.0.1'
			,template:'js/artTemplate/template'
			,bootstrapSwitch:'bootstrap/switch/js/bootstrap-switch.min'
			,ueditor: 'js/UEditor/ueditor.all.min'
			,ueditor_zh:'js/UEditor/zh-cn'
			,zeroclipboard:'js/UEditor/third-party/zeroclipboard/ZeroClipboard.min'
			,Chart:'bootstrap/chart/2.4.0/Chart.min'
			,bootstrapSelect:"bootstrap/select/js/bootstrap-select.min"
		},
		map: {
	      '*': {
	        'css': 'js/require-lib/css.min'
	        ,'text': 'js/require-lib/text'
	      } 
	    },
		shim: {
			'bootstrap':['jquery']
			,'bootstrap_editable':['jquery','bootstrap','datetimepicker','css!${base}/common/bootstrap/editable/css/bootstrap-editable.css']
			,'bootstrapTable':['jquery','bootstrap','css!${base}/common/bootstrap/table/css/bootstrap-table.min.css']
	    	,'datetimepicker':['jquery','css!${base}/common/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css']
	    	,'bootstrapSwitch':['jquery','css!${base}/common/bootstrap/switch/css/bootstrap-switch.min.css']
	    	,'bootstrapSelect':['css!${base}/common/bootstrap/select/css/bootstrap-select.min.css']
	    	,'ueditor': {
	            deps: ['zeroclipboard', '${base}/common/js/UEditor/ueditor.config.js'],
	            exports: 'UE',
	            init:function(ZeroClipboard){
	                //导出到全局变量，供ueditor使用
	                window.ZeroClipboard = ZeroClipboard;
	            }
	        }
	    	,'ueditor_zh':['ueditor']
		}
	});
	requirejs(['${base}/admin/js/report/report_global.js?v=3.7'],function(globalReport){
		globalReport.render('${sport}','${real}','${dianZi}','${lottery}','${markSix}','${thirdSports}','${thirdLottery}','${chess}');
	});
	</script>
<script type="text/javascript">
	//js请求查找站点ID
	function initStationLst() {
		$.ajax({
			url : "${base}/admin/station/combo.do?status=2",
			success : function(result) {
				var eachdata = {
					"data" : result
				};
				var html = template('combolst_tpl', eachdata);
				$("#stationId").html(html);
			}
		});
	}
		//这里调用getTab只是为了点击日期时能使用bootstrap的日历控件
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);
		}
		$(function() {
			getTab();
			initStationLst();
		})
	</script>
</body>
</html>