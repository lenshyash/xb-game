﻿<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>

	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs">
			
			<li class="active"><a href="#" data-toggle="tab" id="platformnav">站点手动加款数据</a></li>
		</ul>
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group" id="station_combo_div">
							<label class="sr-only" for="type">选择站点</label> <select class="form-control" id="stationId">
							</select>
						</div>
						
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<script id="combolst_tpl" type="text/html">
	<option value="0">请选择</option>
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var startTime = '${startTime }';
		var endTime = '${endTime }';
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/stationHandMoney/control.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '账号',
					align : 'center',
					valign : 'middle',
				},  {
					sortable:true,
					field : 'money',
					title : '加款金额',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'name',
					title : '站点名称',
					align : 'center',
					valign : 'middle',
				} ]
			});
		}

		// 切变表格内容
		function chgTab(url) {

			$("#datagrid_tb").bootstrapTable('refreshOptions', {
				url : url,
				pageNumber : 1
			});
		}

		function bindNavTab() {
			$("a[data-toggle='tab']").click(function() {
				var id = this.id;
				if (id === "controlnav") {
					$("#station_combo_div").addClass("hidden");
					$("#stationId").val(0);
					$("#begin").val(startTime);
					$("#end").val(endTime);
					chgTab("${base}/admin/stationHandMoney/control.do");
					curDiv = 1;
				} else if (id === "platformnav") {
					$("#station_combo_div").removeClass("hidden");
					$("#stationId").val(0);
					$("#begin").val(startTime);
					$("#end").val(endTime);
					
					curDiv = 2;
				}
			});
		}

		function initStationLst() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result
					};
					var html = template('combolst_tpl', eachdata);
					$("#stationId").html(html);
				}
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		function remarkFormatter(value, row, index) {
			return [ "<p class='text-danger'>", "</p>" ].join(value);
		}

		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#saccount").val();
			params['stationId'] = $("#stationId").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		$(function() {
			getTab();
			initStationLst();
			bindNavTab();
		})

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
	</script>
	<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
</body>
</html>