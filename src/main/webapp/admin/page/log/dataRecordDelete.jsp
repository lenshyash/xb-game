<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	vertical-align : middle;
}
</style>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div class="row list-group">
		  <div class="col-md-2">投注记录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/bettingLog.do?type=lottery">删除彩票记录</button></div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/bettingLog.do?type=sport">删除体育记录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">帐变记录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/financeLog.do">删除帐变记录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">充值记录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/payonlineLog.do">删除充值记录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">提款纪录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/withdrawLog.do">删除提款纪录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">真人转账纪录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/realchangerdLog.do">删除真人转账纪录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">管理日志纪录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/operationLog.do">删除管理日志纪录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">登录日志纪录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/loginLog.do">删除登录日志纪录</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">报表纪录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/reportLog.do">删除报表纪录</button></div>
		</div>		
		<div class="row list-group">
		  <div class="col-md-2">站内信记录:</div>
		  <div class="col-md-2"><button class="btn btn-primary" url="${base}/admin/dataRecordDelete/userMessage.do">删除站内信纪录(无需指定站点)</button></div>
		</div>
		<div class="row list-group">
		  <div class="col-md-2">站点数据:</div>
		  <div class="col-md-2"><button class="btn-primary" url="${base}/admin/dataRecordDelete/getbackupStationData.do" id="saveStation">保存站点信息</button></div>
			<c:if test="${not empty isZhanShi}">
		  	<div class="col-md-2">
		  		<button class="btn btn-primary" url="${base}/admin/dataRecordDelete/deleteStationData.do" >删除站点信息</button>
		  	</div>
		  </c:if>
		</div>
	</div>
	<!-- 新增 -->
	<div class="modal fade" id="newModel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">删除数据</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationId" class="form-control">
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td class="text-right">时间：</td>
								<td class="text-left">
								
									<select id="drdTimeLen" class="form-control">
										<option value="1">一个月之前数据</option>
										<option value="2">兩个月之前数据</option>
										<option value="3">三个月之前数据</option>
										<option value="6">半年之前数据</option>
										<option value="0">全部数据</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteDataRecordBtn">确定</button>
				</div>
			</div>
		</div>
	</div>
	
		<!-- 保存站点信息 -->
	<div class="modal fade" id="newModel2" tabindex="-1" role="dialog" aria-labelledby="editLabel2" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel2">保存站点数据</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="playId">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td class="text-right">站点：</td>
								<td class="text-left">
								<select id="drdStationId2" class="form-control">
								<c:forEach items="${stationList }" var="s"><option value="${s.id }">${s.name }(${s.floder })</option></c:forEach>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteDataRecordBtn2">确定</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
		$(function() {
			var delUrl="";
			$(".btn[url]").click(function(){
				delUrl=$(this).attr("url");
				$("#editLabel").html($(this).text());
				$("#newModel").modal('toggle');
			});
			$("#deleteDataRecordBtn").click(function(){
				if(delUrl){
					$.post(delUrl,{stationId:$("#drdStationId").val(),timeLen:$("#drdTimeLen").val()},function(d){
						layer.alert(d.msg);
					},"json");
					delUrl="";
				}
			});
			
			$("#saveStation").click(function(){
				delUrl=$(this).attr("url");
				$("#editLabel2").html($(this).text());
				$("#newModel2").modal('toggle');
			});

			$("#deleteDataRecordBtn2").click(function(){
				if(delUrl){
					var options=$("#drdStationId2 option:selected");
					var stationId = options.val();
					var tempwindow=window.open('_blank');
					tempwindow.location='getbackupStationData.do?stationId='+stationId;
					delUrl="";
				}
			});
			
		})
	</script>
</body>
</html>
