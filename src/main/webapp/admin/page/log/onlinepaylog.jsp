<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group">
							<label class="sr-only" for="type">选择站点</label> <select class="form-control" id="stationId">
							</select>
						</div>
						<!-- 
						<div class="input-group">
							<label class="sr-only" for="payType">支付类型</label> <select class="form-control" id="payType">
							</select>
						</div>
						 -->
						<div class="input-group">
							<input type="text" class="form-control" id="content" placeholder="输入订单号（支持本地订单号和第三方订单号）查询">
						</div>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="detailLog" tabindex="-1" role="dialog" aria-labelledby="detailLogLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="detailLogLabel">日志详情</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="pwdatId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-right">用户名：</td>
								<td width="80%" class="text-left"><span id="account_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">日志类型：</td>
								<td width="80%" class="text-left"><span id="type_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作时间：</td>
								<td width="80%" class="text-left"><span id="createTime_span"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作IP：</td>
								<td width="80%" class="text-left"><span id="crateIp_span"></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作记录：</td>
								<td width="80%" class="text-left"><textarea id="content" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">参数：</td>
								<td width="80%" class="text-left"><textarea id="params" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">操作备注：</td>
								<td width="80%" class="text-left"><textarea id="remark" class="form-control"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script id="combolst_tpl" type="text/html">
	<option value="0">请选择{{opName}}</option>
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		var types = {};
		var curDiv = 1;
		var startTime = '${startTime }';
		var endTime = '${endTime }';
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/admin/onlinepaylog/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'stationId',
					title : '站点',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'payType',
					title : '日志类型',
					align : 'center',
					valign : 'middle'/*,
					formatter : typeFormatter*/
				}, {
					field : 'notifyUrl',
					title : '通知url',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'params',
					title : '通知参数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'content',
					title : '流数据',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'headers',
					title : '请求头',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'notifyIp',
					title : '通知服务器IP',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'createDatetime',
					title : '操作时间',
					align : 'center',
					width : '200px',
					valign : 'middle',
					formatter : dateFormatter
				}/*, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				}*/ ]
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="detail" href="javascript:void(0)" title="详情">',
					'详情', '</a>  ' ].join('');
		}

		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$("#detailLog").modal('toggle');
				$("#account_span").html(row.account);
				$("#type_span").html(types[row.payType]);
				$("#createTime_span").html(DateUtil.formatDatetime(row.createDatetime));
				$("#crateIp_span").html(row.accountIp);
				$("#content").val(row.logContent);
				$("#remark").val(row.remark);
				$("#params").val(row.params);
			}
		};

		function initStationLst() {
			$.ajax({
				url : "${base}/admin/station/combo.do",
				success : function(result) {
					var eachdata = {
						"data" : result,
						"opName" : "站点"
					};
					var html = template('combolst_tpl', eachdata);
					$("#stationId").html(html);
				}
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function typeFormatter(value, row, index) {
			return types[value];
		}

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#stationId").val();
			params['payType'] = $("#payType").val();
			params['content'] = $("#content").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		$(function() {
			getTab();
			initStationLst();
		})

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
	</script>
</body>
</html>