<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/admin/include/adminmenu.jsp"></jsp:include>
	<div class="container">
		<table id="datagrid_tb"></table>
	</div>
	<script type="text/javascript">
	$("#datagrid_tb").bootstrapTable({
		url : '${base}/admin/onlineStat/list.do',
		showPageSummary:true,
		showFooter : true,
		columns : [ {
			field : 'folder',
			title : '站点编号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'name',
			title : '站点名称',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			field : 'count',
			title : '在线人数',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				var r=0;
				for(var i=rows.length-1;i>=0;i--){
					r = r+rows[i].count;
				}
				return r;
			}
		} ]
	});
	</script>
</body>
</html>