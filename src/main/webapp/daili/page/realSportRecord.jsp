<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" id="begin"
						placeholder="开始日期"> <span
						class="glyphicon glyphicon-th form-control-feedback"
						aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>

				<label class="sr-only" for="keyword">账户名称</label>
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="account" class="form-control"
							id="account" placeholder="本地账户名称">
					</div>
				</div>

				<label class="sr-only" for="keyword">电子游戏类型</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="gameType" id="gameType">
							<option value="">选择游戏类型</option>
							<option value="10">IBC</option>
							<option value="99">M8</option>
							<option value="991">M8H</option>
						</select>
					</div>
				</div>

				<button class="btn btn-primary" onclick="javascript:search();">查询</button>

				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control"
							placeholder="线束日期"> <span
							class="glyphicon glyphicon-th form-control-feedback"
							aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb" data-show-footer="true"></table>
	</div>
</body>
</html>
<script type="text/javascript">
	$(function() {
		var curDate = new Date();
		var options = {
			language : 'zh-CN',
			autoclose : true,
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			forceParse : 0,
			showSecond : 1,
			minuteStep : 1,
			format : 'yyyy-mm-dd hh:ii:00'
		};
		$('#begin').datetimepicker(options);
		options.format = "yyyy-mm-dd hh:ii:59";
		$('#end').datetimepicker(options);
		var t = DateUtil.formatDate(new Date()), begin = "", end = "";
		<c:if test="${empty param.begin}">begin = t + " 00:00:00";
		</c:if>
		<c:if test="${empty param.end}">end = t + " 23:59:59";
		</c:if>
		<c:if test="${not empty param.begin}">begin = "${param.begin}";
		</c:if>
		<c:if test="${not empty param.end}">end = "${param.end}";
		</c:if>
		<c:if test="${not empty param.account}">$('#account').val(
				"${param.account}");
		</c:if>
		setDate(begin, end);
		bindbtn();
		getTab();
	});
	function getTab() {
		window.table = new Game.Table({
			id : 'datagrid_tb',
			url : '${base}/daili/rsbet/list.do',
			queryParams : queryParams,//参数
			toolbar : $('#toolbar'),
			showPageSummary:true,
			showAllSummary:true,
			columns : [
					{
						field : 'platformType',
						title : '类型',
						align : 'center',
						valign : 'middle',
					},
					{
						field : 'account',
						title : '注单账户',
						align : 'center',
						valign : 'middle'
					},
					{
						field : 'bettingCode',
						title : '注单号',
						align : 'center',
						valign : 'middle'
					},
					{
						field : 'gameName',
						title : '体育/投注类型',
						align : 'center',
						valign : 'middle',
						formatter : SportType
					},
					{
						field : 'league',
						title : '联赛名称 / 队伍 / 赔率',
						align : 'center',
						valign : 'middle',
						formatter : contentFormatter
					},
					{
						title : '注单结果',
						align : 'center',
						valign : 'middle',
						formatter : scoreFormatter,
						pageSummaryFormat : function(rows, aggsData) {
							return "小计:";
						},
						allSummaryFormat : function(rows, aggsData) {
							return "总计:";
						}
					},
					{
						field : 'bettingMoney',
						title : '投注金额',
						align : 'center',
						valign : 'middle',
						pageSummaryFormat : function(rows, aggsData) {
							return getTotal(rows, "bettingMoney", 2);
						},
						allSummaryFormat : function(rows, aggsData) {
							if (!aggsData) {
								return "0.00"
							}
							return aggsData.bettingMoneyCount ? aggsData.bettingMoneyCount
									.toFixed(2)
									: "0.00";
						}
					},
					{
						field : 'realBettingMoney',
						title : '有效投注金额',
						align : 'center',
						valign : 'middle',
						pageSummaryFormat : function(rows, aggsData) {
							return getTotal(rows, "realBettingMoney", 4);
						},
						allSummaryFormat : function(rows, aggsData) {
							if (!aggsData) {
								return "0.0000"
							}
							return aggsData.realBettingMoneyCount ? aggsData.realBettingMoneyCount
									.toFixed(4)
									: "0.0000";
						}
					},
					{
						sortable : true,
						field : 'winMoney',
						title : '中奖金额',
						align : 'center',
						valign : 'middle',
						pageSummaryFormat : function(rows, aggsData) {
							return getTotal(rows, "winMoney", 4);
						},
						allSummaryFormat : function(rows, aggsData) {
							if (!aggsData) {
								return "0.0000"
							}
							return aggsData.winMoneyCount ? aggsData.winMoneyCount
									.toFixed(4)
									: "0.0000";
						}
					},{
						field : 'league',
						title : '盈亏金额',
						align : 'center',
						valign : 'middle',
						formatter : lossMoney,
						pageSummaryFormat : function(rows, aggsData) {
							return getLoss(rows, "winMoney","realBettingMoney", 4);
						},
						allSummaryFormat : function(rows, aggsData) {
							if (!aggsData) {
								return "0.0000"
							}
							return aggsData.winMoneyCount ? (aggsData.realBettingMoneyCount - aggsData.winMoneyCount )
									.toFixed(4)
									: "0.0000";
						}
					}, {
						field : 'bettingTime',
						title : '投注时间',
						align : 'center',
						valign : 'middle',
						formatter : dateFormatter
					}, {
						sortable : true,
						field : 'createDatetime',
						title : '创建时间',
						align : 'center',
						valign : 'middle',
						formatter : dateFormatter
					} ]
		});
	}
	function SportType(value, row, index){
		var type = row.gameName + '<br />' +row.playName
		return type
	}
	function lossMoney(value, row, index){
		var money;
		if(row.realBettingMoney == 0){
			money = 0
		}else{
			money = (row.bettingMoney - row.winMoney)
		}
		return money.toFixed(4) + ''
	}
	function getTotal(rows,itemKey,scale){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			total +=r[itemKey]||0;
		}
		return total.toFixed(scale)+"";
	}
	function getLoss(rows,win,bet,scale){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			total +=(r[bet] - r[win])||0;
		}
		return total.toFixed(scale)+"";
	}
	function contentFormatter(value, row, index) {
		var html = "";
		html += "联赛  " + row.league;
		if (row.homeTeam == row.awayTeam && row.homeTeam != "") {
			html += row.homeTeam;
		} else if (row.homeTeam != "") {
			if(row.score == undefined){
				row.score = ''
			}else{
				row.score = "("+row.score+")"
			}
			html += "<br>主队 "+row.homeTeam+" -VS- "+"客队 "+row.awayTeam + ' <b style="color:red;">'+row.score+'</b>'
		}
		if(row.resScore == undefined){
			row.resScore = ''
		}else{
			row.resScore = "("+row.resScore+")"
		}
		html +="<br><b style='color:red;'>"+ row.info +"</b> @<font color =red > "+row.odds+"<font> <span style='color:blue;'>"+row.resScore+"</span>" ;
		return html;
	}

	function scoreFormatter(value, row, index) {
		var html = "注单状态:";
		if (row.resStatus == 1) {
			html += "未结算";
		} else {
			if (row.resStatus == 2) {
				html += "全赢";
			} else if (row.resStatus == 3) {
				html += "全输";
			} else if (row.resStatus == 4) {
				html += "赢一半";
			} else if (row.resStatus == 5) {
				html += "输一半";
			} else if (row.resStatus == 6) {
				html += "和局";
			}
			if(row.score == undefined){
				row.score = '-'
			}
		}

		return html;
	}

	function dateFormatter(value, row, index) {
		return DateUtil.formatDatetime(value);
	}

	function setDate(begin, end) {
		$('#begin').val(begin);
		$('#end').val(end);
	}

	function bindbtn() {
		$(".form-inline .btn-default").click(function() {
			var type = $(this).html();
			var begin = "";
			var end = "";
			if ('今日' === type) {
				begin = DateUtil.getCurrentDate();
				end = begin;
			} else if ('昨日' === type) {
				begin = DateUtil.getLastDate();
				end = begin;
			} else if ('本周' === type) {
				begin = DateUtil.getWeekStartDate();
				end = DateUtil.getCurrentDate();
			} else if ('上周' === type) {
				begin = DateUtil.getLastWeekStartDate();
				end = DateUtil.getLastWeekEndDate();
			} else if ('本月' === type) {
				begin = DateUtil.getMonthDate();
				end = DateUtil.getCurrentDate();
			} else if ('上月' === type) {
				begin = DateUtil.getLastMonthStartDate();
				end = DateUtil.getLastMonthEndDate();
			}
			setDate(begin + " 00:00:00", end + " 23:59:59");
			search();
		});
	}

	function search() {
		$("#datagrid_tb").bootstrapTable('refresh');
	}
	
	//设置传入参数search
	function queryParams(params) {
		params["account"] = $("#account").val();
		params["gameType"] = $("#gameType").val();
		params['startTime'] = $("#begin").val();
		params['endTime'] = $("#end").val();
		return params
	}
</script>
