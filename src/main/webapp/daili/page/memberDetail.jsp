<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<table class="table table-bordered table-striped">
	<tbody>
		<tr>
			<th width="18%" class="text-right">登录账号：</th>
			<td width="20%">${map.account }</td>
			<th width="18%" class="text-right">所属上级：</th>
			<td>${parentNames }</td>
		</tr>
		<tr>
			<th class="text-right">会员姓名：</th>
			<td>${map.userName }</td>
			<th class="text-right">类别：</th>
			<td><c:if test="${map.accountType==1 }">会员</c:if><c:if test="${map.accountType==4 }">代理</c:if></td>
		</tr>
		<tr>
			<th class="text-right">会员余额：</th>
			<td><fmt:formatNumber pattern="#,##0.00" value="${map.money }"/></td>
		</tr>
		<tr>
			<th class="text-right">状态：</th>
			<td><c:if test="${map.accountStatus==2 }">启用</c:if><c:if test="${map.accountStatus!=2 }">禁用</c:if></td>
			<th class="text-right">取现银行：</th>
			<td>${map.bankName }</td>
		</tr>
		<tr>
			<th class="text-right">银行账号：</th>
			<td>${map.cardNo }</td>
			<th class="text-right">银行地址：</th>
			<td>${map.bankAddress }</td>
		</tr>
	</tbody>
</table>
