<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">站内信</li>
		</ol>
	</div>
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<label class="sr-only" for="stitle">标题名称</label>
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control" id="stitle" placeholder="标题名称">
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑站内信</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="messageId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">站内信标题：</td>
								<td width="35%" class="text-left"><div id="title"></div></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">站内信内容：</td>
								<td width="35%" class="text-left"><div id="message"></div></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();">确认</button>
				</div>
			</div>
		</div>
	</div>
	<script id="stationlst_tpl" type="text/html">
	{{each data as st}}
			<option value="{{st.id}}">{{st.name}}</option>
	{{/each}}
	</script>
	<script type="text/javascript">
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/dlmessage/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'title',
					title : '站内信标题',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : titleFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					valign : 'middle',
					formatter : statusFormatter
				}, {
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function titleFormatter(value, row, index) {
			return [ '<a class="read" href="javascript:void(0)" title="阅读">',
					'</a>  ' ].join(value);
		}

		function statusFormatter(value, row, index) {
			if (value === 2) {
				return '<span class="text-danger label label-success">已读</span>';
			}
			return '<span class="text-muted label label-default">未读</span>';
		}
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="read" href="javascript:void(0)" title="阅读">',
					'阅读', '</a>  ',
					'<a class="remove" href="javascript:void(0)" title="删除">',
					'删除', '</a>' ].join('');
		}

		window.operateEvents = {
			'click .read' : function(e, value, row, index) {
				read(row);
			},
			'click .stateOpen' : function(e, value, row, index) {
				updStatus(row.id, 2);
			},
			'click .stateClose' : function(e, value, row, index) {
				updStatus(row.id, 1);
			},
			'click .remove' : function(e, value, row, index) {
				remove(row);
			}
		};

		//设置传入参数
		function queryParams(params) {
			params["title"] = $("#stitle").val();
			return params
		}
		$(function() {
			getTab();
		})

		function remove(row) {
			Msg.confirm('确定要删除该站内信吗?', {
				btn : [ '确定', '取消' ]
			}, function() {
				$.ajax({
					url : "${base}/daili/dlmessage/delete.do",
					data : {
						id : row.id
					},
					success : function(result) {
						$("#datagrid_tb").bootstrapTable('refresh');
					}
				});
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function add() {
			$("#editmodel").modal('toggle');
			$("#messageId").val("");
			$("#title").val("");
			$("#message").val("");
		}

		function read(row) {
			$.ajax({
				url : "${base}/daili/dlmessage/read.do",
				data : {
					id : row.id
				},
				success : function(result) {
					$("#editmodel").modal('toggle');
					$("#title").html(row.title);
					$("#message").html(row.message);
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
	</script>
</body>
</html>