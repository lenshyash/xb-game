<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">重新设置登录密码</li>
		</ol>
	</div>

	<div class="container">
		<table class="table table-bordered table-striped" style="clear: both">
			<tbody>
				<tr>
					<td width="15%" class="text-left">＊密码：</td>
					<td width="85%"><input type="password" class="form-control" id="password" /></td>
					</td>
				</tr>
				<tr>
					<td width="15%" class="text-left">＊确认密码：</td>
					<td width="85%"><input type="password" class="form-control" id="rpassword" /></td>
				</tr>
				<tr>
					<td width="100%" class="text-center" colspan="3"><button class="btn btn-success" onclick="submit();">
							<i class="glyphicon glyphicon-ok"></i> 提交
						</button></td>
				</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		function submit() {
			var password = $("#password").val();
			var rpassword = $("#rpassword").val();
			if (!password) {
				alert("新密码不能为空！");
				return;
			}
			if (!rpassword) {
				alert("确认不能为空！");
				return;
			}

			if (password !== rpassword) {
				alert("两次密码不一致！");
				return;
			}
			$.ajax({
				url : "${base}/daili/updloginpwd.do",
				data : {
					pwd : password,
					rpwd : rpassword
				},
				success : function(result) {
					Msg.confirm('修改成功', {
						btn : [ '确认' ]
					}, function() {
						window.location.href = "${base}/daili";
					});
				}
			});
		}
	</script>
</body>
</html>