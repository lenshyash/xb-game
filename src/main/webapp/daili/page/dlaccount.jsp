<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">下级列表</li>
		</ol>
	</div>
	<input type="hidden" value="0" id="agentId">
	<div class="container-fluid">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<button class="btn btn-primary" onclick="add();">新增</button>
				<label class="sr-only" for="saccount">输入会员名查询</label>
				<div class="form-group">
					<div class="input-group">
						<select id="searchType" class="form-control">
							<option value="1">直属下级</option>
							<option value="2">所有下级</option>
						</select>
					</div>
					<div class="input-group">
						<select id="onlineFlag" class="form-control">
							<option value="">全部</option>
							<option value="1">离线</option>
							<option value="2">在线</option>
						</select>
					</div>
					<div class="input-group">
						<select id="stype" class="form-control">
							<option value="">全部</option>
							<option value="1">会员</option>
							<option value="4">代理</option>
						</select>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="saccount" placeholder="输入会员账号查询" style="width:140px";>
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="sUserName" placeholder="输入会员姓名查询" style="width:140px";>
					</div>
					<div class="input-group">
						<select name="levelGroup" class="form-control" id="levelGroup"><option value="">所有等级</option><c:forEach items="${levels}" var="l"><option value="${l.id }"<c:if test="${(not empty level && level==l.id)}">selected</c:if>>${l.levelName }</option></c:forEach></select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="search();">查询</button>
				<button class="btn btn-primary" onclick="back();">返回</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade bs-example-modal-lg" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">编辑用户</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="accountId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">登录账号：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="account" disabled="disabled" /></td>
								<td width="15%" class="text-right">用户类型：</td>
								<td width="35%" class="text-left"><select id="accountType" class="form-control" disabled="disabled">
										<option value="1" id="member_opt">会员</option>
										<option value="4" id="agent_opt">代理</option>
										<option value="5" id="general_opt">总代理</option>
								</select></td>
							</tr>
							<tr class="newpwd_tr">
								<td width="15%" class="text-right">密码：</td>
								<td width="35%" class="text-left"><input type="password" class="form-control" id="newpwd" /></td>
								<td width="15%" class="text-right">确认密码：</td>
								<td width="35%" class="text-left"><input type="password" class="form-control" id="newrpwd" /></td>
							</tr>
							<tr class="userinfo_tr">
								<td width="15%" class="text-right">账号状态：</td>
								<td width="35%" class="text-left"><select id="accountStatus" class="form-control" disabled="disabled">
										<option value="1">禁用</option>
										<option value="2">启用</option>
								</select></td>
								<td width="15%" class="text-right">用户姓名：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="userName" disabled="disabled" /></td>
							</tr>
							<tr class="userinfo_tr">
								<td width="15%" class="text-right">银行账号：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="cardNo" disabled="disabled" />
								<td width="15%" class="text-right">取现银行：</td>
								<td width="35%" class="text-left">
									<input id="bankName" class="form-control" disabled="disabled"/>
								</td>
								</td>
							</tr>

							<tr class="rebateNum_tr">
								<td width="15%" class="text-right multiData hidden">返点数：</td>
								<td width="35%" class="text-left multiData hidden"><input type="text" class="form-control" id="rebateNum" /></td>
								<td width="50%" class="text-left multiData hidden" colspan="2"><span id="rebate_desc"></span></td>
							</tr>
							<tr class="profitShare_tr">
								<td width="15%" class="text-right profitShareData hidden">占成数：</td>
								<td width="35%" class="text-left profitShareData hidden"><input type="text" class="form-control" id="profitShare" /></td>
								<td width="50%" class="text-left profitShareData hidden" colspan="2"><span id="profitShare_desc"></span></td>
							</tr>
							<tr class="dynamicRate_tr">
								<td width="15%" class="text-right dynamicRateData hidden">赔率点：</td>
								<td width="35%" class="text-left dynamicRateData hidden"><input type="text" class="form-control" id="dynamicRate" /></td>
								<td width="50%" class="text-left dynamicRateData hidden" colspan="2"><span id="dynamicRate_desc"></span></td>
							</tr>
							
							<tr class="userinfo_tr">
								<td width="15%" class="text-right showTel">电话：</td>
								<td width="35%" class="text-left showTel"><input type="text" class="form-control" id="phone" disabled="disabled" /></td>
								<td width="15%" class="text-right showEmail">邮箱：</td>
								<td width="35%" class="text-left showEmail"><input type="text" class="form-control" id="email" disabled="disabled" /></td>
							</tr>
							<tr class="parentNames_tr">
								<td width="15%" class="text-right showBankAdd">银行地址：</td>
								<td width="35%" class="text-left showBankAdd"><input type="text" class="form-control" id="bankAddress" disabled="disabled" /></td>
								<td width="15%" class="text-right showQQ">QQ：</td>
								<td width="35%" class="text-left showQQ"><input type="text" class="form-control" id="qq" disabled="disabled" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right showWechat">微信：</td>
								<td width="35%" colspan="3" class="text-left showWechat"><input type="text" class="form-control" id="wechat" disabled="disabled" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">打码量：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="betNum" disabled="disabled" /></td>
								<td width="15%" class="text-right">提款所需：</td>
								<td width="35%" class="text-left"><input type="text" class="form-control" id="drawNeed" disabled="disabled" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">所属上级：</td>
								<td width="85%" colspan="3" class="text-left"><textarea class="form-control" id="parentNames" disabled="disabled"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save();" id="save_btn">保存</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bs-example-modal-lg" id="editmodel2" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">加款</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered" style="margin-top:10px;clear: both">
		                <tbody>
							<tr>
								<td width="30%" class="text-right active">会员账号：</td>
		                        <td><span id="accountspan" class="text-primary"></span></td>
							</tr>
							<tr>
								<td class="text-right active">账号余额：</td>
		                        <td><span id="moneyspan" class="text-danger"></span></td>
							</tr>  
							<tr>
								<td class="text-right active">操作金额：</td>
		                        <td><input type="text" class="form-control" id="money" /></td>
							</tr>
							<tr>
								<td colspan="2" class="text-center"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submit1();">确认</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<!-- 修改返点数 -->
	<input type="hidden" id="curFands">
	<div class="modal fade bs-example-modal-lg" id="editmodel3" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">下级高级修改</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered" style="margin-top: 10px; clear: both">
						<tbody>
							<tr>
								<input type="hidden" id="fdsAcc" />
								<td width="35%" class="text-right active">会员账号：</td>
								<td width="65%" colspan="2"><span id="fsAccount" class="text-primary"></span></td>
							</tr>
							<tr class="fs_tr hidden">
								<td width="35%" class="text-right active">当前点数：</td>
								<td width="65%" colspan="2"><span id="fsCur" class="text-primary"></span></td>
							</tr>
							<tr class="ps_tr hidden">
								<td width="35%" class="text-right active">当前占成：</td>
								<td width="65%" colspan="2"><span id="curProfitShare" class="text-primary"></span></td>
							</tr>
							<tr class="dr_tr hidden">
								<td width="35%" class="text-right active">当前赔率点：</td>
								<td width="65%" colspan="2"><span id="curDynamicRate" class="text-primary"></span></td>
							</tr>
							
							<tr class="fs_tr hidden">
								<td width="35%" class="text-right active">点数修改为：</td>
								<td width="65%" colspan="2"><input type="text" class="form-control" style="width: 25%; float: left;" id="fsAdd" /> <span style="line-height: 32px;">(可设置返点区间: <span
										class="lable lable-success"><span id="fsCur2"></span>-<span class="cfands"></span></span>)
								</span></td>
							</tr>
							<tr class="ps_tr hidden">
								<td width="35%" class="text-right active">占成修改为：</td>
								<td width="65%" colspan="2"><input type="text" class="form-control" style="width: 25%; float: left;" id="profitShare_upd" /> <span style="line-height: 32px;">(可设置占成区间: <span
										class="lable lable-success"><span id="minPS"></span>-<span id="maxPS"></span></span>)
								</span></td>
							</tr>
							<tr class="dr_tr hidden">
								<td width="35%" class="text-right active">赔率点修改为：</td>
								<td width="65%" colspan="2"><input type="text" class="form-control" style="width: 25%; float: left;" id="dynamicRate_upd" /> <span style="line-height: 32px;">(可设置赔率区间: <span
										class="lable lable-success"><span id="minDR"></span>-<span id="maxDR"></span></span>)
								</span></td>
							</tr>
							
							<tr>
								<td width="35%" class="text-right">所属上级：</td>
								<td width="65%" colspan="3" class="text-left"><textarea class="form-control" id="parentNames2" disabled="disabled"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addFs();">确认修改</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function()  {
			fdsDate();
		});
		
		function submit1() {
			$.ajax({
				url : "${base}/daili/dlaccount/saveCash.do",
				data : {
					id : $("#accountId").val(),
					money : $("#money").val()
				},
				success : function(data) {
					Msg.info("操作成功！");
					search();
				}
			});
		}
		function search1(account) {
			$.ajax({
				url : "${base}/daili/dlaccount/memmny.do",
				data : {
					account : account
				},
				success : function(data) {
					$("#accountId").val(data.accountId);
					$("#accountspan").html(data.account);
					$("#moneyspan").html(data.money);
				}
			});
		}
	</script>

	<script type="text/javascript">
		console.log("${memberRateFlag}")
		var ps = [ {
			"id" : 0,
			"flag" : 0
		} ]; //轨迹
		var parentId = 0; //当前父节点ID
		var opFlag = 0;
		var ops = [ 'agentId', 'agentParentId' ];
		var agentMulti = "";
		var profitShareFlag = "";
		var dynamicRateFlag = "";
		var mergeRebateFlag = "";
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/dlaccount/list.do',
				sortType:'local',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '用户账号',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : accountFormatter
				},
				{
					field : 'agentName',
					title : '所属上级',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'money',
					title : '账号余额',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : moneyFormatter
				},
				{
					field : 'balanceGemMoney',
					title : '余额宝余额',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : moneyFormatter
				},
				<c:if test="${scoreShowFlag}">
				{
					field : 'score',
					title : '积分余额',
					align : 'center',
					valign : 'middle'
				}, 
				</c:if>
				{
					field : 'gameShare',
					title : '返点数',
					align : 'center',
					valign : 'middle',
					sortable: true
				},{
					field : 'profitShare',
					title : '占成数',
					align : 'center',
					valign : 'middle',
					sortable: true
				}, <c:if test="${memberRateFlag}"> {
					field : 'rate',
					title : '自身赔率点',
					align : 'center',
					valign : 'middle'
				}, </c:if>{
					field : 'userName',
					title : '用户姓名',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'accountType',
					title : '用户类型',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : typeFormatter
				}, {
					field : 'levelGroup',
					title : '会员等级',
					align : 'center',
					valign : 'middle',
					sortable:true,
					formatter : levelFormatter
				}, {
					field : 'createDatetime',
					title : '注册时间',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : dateFormatter
				}, {
					field : 'lastLoginDatetime',
					title : '最后登录时间',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : dateFormatter
				}, {
					field : 'lastLoginIp',
					title : '最后登录ip',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'lastLoginIpAddress',
					title : '最后登录ip地址',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'online',
					title : '在线情况',
					align : 'center',
					valign : 'middle',
					sortable: true,
					formatter : onlineFormatter,
				}, {
					field : 'accountStatus',
					title : '状态',
					align : 'center',
					valign : 'middle',
					formatter : statusFormatter,
				}, {
					title : '操作',
					align : 'center',
					width : '200',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
		}

		function userFormatter(value, row, index){
			if(value){
				return '**'+value.Right(1);
			}
		}
		
		// 从右边截取i位有效字符
		String.prototype.Right = function(i) { // 为String对象增加一个Right方法
			return this.slice(this.length - i, this.length); // 返回值为 以“该字符串长度减i”为起始 到
																// 该字符串末尾 的截取字符串
		};
		
		function addFs(){
			var fs = $('#curFands').val();//可分配的返点数
			var inputFs = $('#fsAdd').val();//输入的返点数
			var	curFds = $('#fsCur2').text();//当前返点数
			var accId= $('#fdsAcc').val();
			if(!inputFs){
				//Msg.info('请输入返点数!');
				//return false;
			}
			if(inputFs*1 > fs*1){
				Msg.info('输入的返点数不能大于可分配返点数!');
				return false;
			}
			if( inputFs!=0 && inputFs*1 < curFds*1){
				Msg.info('输入的返点数不能小于下级最大返点!');
				return false;
			}
			var inputPS;
			if("on" == "${profitShareFlag}"){
				inputPS = $('#profitShare_upd').val();//输入的占成数
				var	minPS = $('#minPS').text();
				var maxPS= $('#maxPS').text();
				if(!inputPS){
					Msg.info('请输入占成数!');
					return false;
				}
				if(inputPS*1 > maxPS*1){
					Msg.info('输入的占成数不能大于可分配占成数!');
					return false;
				}
				if( inputPS!=0 && inputPS*1 < minPS*1){
					Msg.info('输入的占成数不能小于下级最大占成!');
					return false;
				}
			}
			var inputDR;
			if('${memberRateFlag}'=='true'){
				inputDR = $('#dynamicRate_upd').val();//输入的占成数
				var	minDR = $('#minDR').text();
				var maxDR= $('#maxDR').text();
				if(!inputDR){
					Msg.info('请输入赔率点!');
					return false;
				}
				if(inputDR*1 > maxDR*1){
					Msg.info('输入的赔率点不能大于可分配赔率点!');
					return false;
				}
				if( inputDR!=0 && inputDR*1 < minDR*1){
					Msg.info('输入的赔率点不能小于下级最大赔率点!');
					return false;
				}
			}
			//校验成功,修改返点数
 			$.ajax({
 				url : "${base}/daili/dlaccount/saveFds.do",
 				data:{
 					"id":accId,
 					"rebateNum":inputFs,
 					"profitShare":inputPS,
 					"dynamicRate":inputDR
 				},
 				success : function(result) {
 					Msg.info("修改成功！");
 					$("#datagrid_tb").bootstrapTable('refresh');
 				}
 			});
		}
		
		function onlineFormatter(value, row, index) {
			if (value === 2) {
				return '<span class="label label-success">在线</span>';
			}else if(value==1){
				return '<span class="label label-default">离线</span>';
			}
		}

		function accountFormatter(value, row, index) {

			return [
					'<a class="detail" href="javascript:void(0)" title="详情"><span class="text-danger">',
					'</span></a>' ].join(value);
		}

		function typeFormatter(value, row, index) {
			return [ '<span class="text-primary">', '</span>' ]
					.join(GlobalTypeUtil.getTypeName(2, 2, value));
		}

		function statusFormatter(value, row, index) {

			if (value === 2) {
				return '<span class="text-success">启用</span>';
			}
			return '<span class="text-danger">禁用</span>';
		}
		function moneyFormatter(value, row, index) {
			if (value === undefined) {
				return value;
			}
			var f = parseFloat(value);
			f=Math.round(f*100) / 100;
			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ].join(f);
			}
			return [ '<span class="text-primary">', '</span>' ].join(f);
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		function operateFormatter(value, row, index) {
			var agentid = $("#agentId").val();
			var btns = [];
			var up = '<a class="doup" href="javascript:void(0)" title="上一级">上一级</a>  ';
			var down = '<a class="dodown" href="javascript:void(0)" title="下一级">下一级</a>  ';
			var updateFanDian = '<a class="updateFanDian" href="javascript:void(0)" title="高级返点">高级修改</a>   ';
			var chongqian = '<a class="chongqian" href="javascript:void(0)" title="加款">加款</a>';
			var shenhe = '<a class="shenhe"  title="审核">审核</a>   ';
			if(row.accountStatus == 3 && agentid == 1 && !${reviewClose}){
				btns.push(shenhe);
			}
			if (row.accountType == 4) {
				if (row.agentId == 0) {
					return btns.join('');
				}
				if (row.agentId && row.level > 2) {
					btns.push(up);
				}
				btns.push(down);
				if(row.agentName == $("#head_pwdat").val()){
 					btns.push(updateFanDian);
				}
			}
			if(${isCzkg}){
				if($('#agentId').val() == 0 || $('#agentId').val() == "1"){
					btns.push(chongqian);
				}
			}
			return btns.join('');
		}

		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$("#save_btn").addClass("hidden");
				if (row.accountType == 4 || row.accountType==1) {
					$.ajax({
						url : "${base}/daili/dlaccount/multidata.do",
						data:{id:row.id},
						success : function(result) {
							setParam(row);
							$("#agent_opt").removeClass("hidden");
							$("#general_opt").removeClass("hidden");
							$("#member_opt").removeClass("hidden");
							$("#parentNames_tr").removeClass("hidden");
							if ("off" == result.multiAgent || row.accountType!=4) {
								$(".multiData").addClass("hidden");
							} else if ("on" == result.multiAgent && "on" != result.mergeRebate && row.accountType==4) {
								$("#rebateNum").val();
								$("#rebateNum").attr("disabled", "disabled");
								$("#rebateNum").val(row.gameShare);
								$(".multiData").removeClass("hidden");
								$(".rebateNum_tr").removeClass("hidden");
							}
							if ("off" == result.profitShare  || row.accountType!=4) {
								$(".profitShareData").addClass("hidden");
							} else if ("on" == result.profitShare  && row.accountType==4) {
								$("#profitShare").val();
								$("#profitShare").attr("disabled", "disabled");
								$("#profitShare").val(row.profitShare);
								$(".profitShareData").removeClass("hidden");
								$(".profitShare_tr").removeClass("hidden");
							}
							if ("off" == result.dynamicRate) {
								$(".dynamicRateData").addClass("hidden");
							} else if ("on" == result.dynamicRate) {
								$("#dynamicRate").val();
								$("#dynamicRate").attr("disabled", "disabled");
								$("#dynamicRate").val(row.rate);
								$(".dynamicRateData").removeClass("hidden");
								$(".dynamicRate_tr").removeClass("hidden");
							}
							
							$('#rebate_desc').html("(可设置返点区间: "+result.rebateMinNum+" - " + result.rebateMaxNum +" )");
							$('#profitShare_desc').html("(可设置占成区间: "+result.profitShareMin+" - " + result.profitShareMax +" )");
							$('#dynamicRate_desc').html("(可设置赔率区间: "+result.dynamicRateMin+" - " + result.dynamicRateMax +" )");
							agentMulti = result.multiAgent;
							profitShareFlag = result.profitShare;
							dynamicRateFlag = result.dynamicRate;
							mergeRebateFlag = result.mergeRebate;
						}
					});
				} else {
					setParam(row);
					$(".rebateNum_tr").addClass("hidden");
					$(".profitShare_tr").addClass("hidden");
					$(".dynamicRate_tr").addClass("hidden");
				}

			},
			'click .doup' : function(e, value, row, index) {
				$("#saccount").val("");
				doDown(row.agentId, 0);
			},
			'click .dodown' : function(e, value, row, index) {
				$("#saccount").val("");
				doDown(row.id, 1);
			},
			'click .updateFanDian' : function(e, value, row, index) {
				fds(row);
			},
			'click .chongqian' : function(e, value, row, index) {
				var cur = $('#agentId').val();
				if(cur=="1"){
					//给会员或代理加款
					$("#editmodel2").modal('toggle');
					search1(row.account);
				}else{
					Msg.info("只能给下级会员或代理加款");
				}
				
			},
			'click .shenhe' : function(e, value, row, index) {
				sh(row);
			}
		};
		
		function sh(row){
			$.ajax({
				url : "${base}/daili/dlaccount/updStatus.do",
				data:{
					id:row.id,
					accountStatus:2,
					},
				success : function(result) {
					Msg.info("审核成功！");
					$("#datagrid_tb").bootstrapTable('refresh');
				},
			});
		}
		
		function fds(row){
			$.ajax({
				url : "${base}/daili/dlaccount/multidata.do",
				data:{id:row.id},
				success : function(result) {
					$("#editmodel3").modal('toggle');
					$('#fsAdd').val('');
					$('#fsAccount').html(row.account);
					$('#fsCur').html(row.gameShare);
					$('#fdsAcc').val(row.id);
					$("#parentNames2").val(row.parentNames);
					$('#curFands').val(result.rebateMaxNum);
					$('.cfands').html(result.rebateMaxNum);
					$('#fsCur2').html(result.rebateMinNum);
					if("on"==result.multiAgent && "on" != result.mergeRebate){
						$(".fs_tr").removeClass("hidden");
					}else{
						$(".fs_tr").addClass("hidden");
					}
					
					if("on" == result.profitShare){
						$(".ps_tr").removeClass("hidden");
						$('#curProfitShare').val(result.curProfitShare);
						$('#minPS').html(result.profitShareMin);
						$('#maxPS').html(result.profitShareMax);
					}else{
						$(".ps_tr").addClass("hidden");
					}
					if("on" == result.dynamicRate){
						$(".dr_tr").removeClass("hidden");
						$('#curDynamicRate').html(row.rate);
						$('#minDR').html(result.dynamicRateMin);
						$('#maxDR').html(result.dynamicRateMax);
					}else{
						$(".dr_tr").addClass("hidden");
					}
					
				}
			});
			
		}
		
		function fdsDate(){
			$.ajax({
				url : "${base}/daili/dlaccount/multidata.do",
				data:{limit:1},
				success : function(result) {
					$('#curFands').val(result.rebateMaxNum);
					$('.cfands').html(result.rebateMaxNum);
				}
			});
		}

		function setParam(row) {
			$("#editmodel").modal('toggle');
			$("#account").attr("disabled", "disabled");
			$("#account").val(row.account);
			/* if(row.cardNo && row.cardNo.length>4){
				$("#cardNo").val('**'+row.cardNo.Right(4));
			}else{ */
				$("#cardNo").val(row.cardNo);
			/* } */
			$("#accountId").val(row.id);
			$("#bankName").val(row.bankName);
			$("#userName").val(row.userName);
			$("#betNum").val(row.betNum);
			$("#drawNeed").val(row.drawNeed);
			if(row.phone){
				$('.showTel').show();
			}else{
				$('.showTel').hide();
			}
			if(row.email){
				$('.showEmail').show();
			}else{
				$('.showEmail').hide();
			}
			if(row.bankAddress){
				$('.showBankAdd').show();
			}else{
				$('.showBankAdd').hide();
			}
			if(row.qq){
				$('.showQQ').show();
			}else{
				$('.showQQ').hide();
			}
			if(row.wechat){
				$('.showWechat').show();
			}else{
				$('.showWechat').hide();
			}
			$("#phone").val(row.phone);
			$("#wechat").val(row.wechat);
			$("#qq").val(row.qq);
			$("#email").val(row.email);
			$("#bankName").val(row.bankName);
			$("#bankAddress").val(row.bankAddress);
			$("#accountStatus").val(row.accountStatus);
			$("#accountType").val(row.accountType);
			if(row.parentNames){
				$("#parentNames").val(row.parentNames+'>'+row.account);
			}
			
			$("#newpwd").val("");
			$("#newrpwd").val("");
			$(".newpwd_tr").addClass("hidden");
			$(".userinfo_tr").removeClass("hidden");

			$("#cardNo").attr("disabled", "disabled");
			$("#bankName").attr("disabled", "disabled");
			$("#userName").attr("disabled", "disabled");
			$("#betNum").attr("disabled", "disabled");
			$("#drawNeed").attr("disabled", "disabled");
			$("#phone").attr("disabled", "disabled");
			$("#qq").attr("disabled", "disabled");
			$("#email").attr("disabled", "disabled");
			$("#bankAddress").attr("disabled", "disabled");
			$("#accountStatus").attr("disabled", "disabled");
			$("#accountType").attr("disabled", "disabled");
			$("#rebateNum").attr("disabled", "disabled");
			$("#parentNames_tr").removeClass("hidden");
		}

		function doDown(id, flag) {
			if (ps && ps[ps.length - 1].id != id) {
				var data = {};
				data.id = id
				data.flag = flag;
				ps.push(data);
			}
			opFlag = flag;
			parentId = id;
			$("#searchType").val(2);
			$("#datagrid_tb").bootstrapTable('refreshOptions', {
				pageNumber : 1
			});
		}
		//设置传入参数
		function queryParams(params) {
			params["accountType"] = $("#stype").val();
			params["online"] = $("#onlineFlag").val();
			params["account"] = $("#saccount").val();
			params["userName"] = $("#sUserName").val();
			var curId = "${agentId}";
			if(parentId==0 || parentId==curId){
				$('#agentId').val('1');
			}else{
				$('#agentId').val('-1');
			}
			if(!params["account"]){
				params[ops[opFlag]] = parentId;
			}else{
				ps = [ {
					"id" : 0,
					"flag" : 0
				} ]; //轨迹
				parentId = 0; //当前父节点ID
				opFlag = 0;
			}
			params["searchType"] = $("#searchType").val();
	        params["levelGroup"] = $("#levelGroup option:selected").val();
			
			return params
		}
		$(function() {
			$("#onlineFlag").val('${online}');
			var searchType = '${searchType}';
			if(searchType){
				$("#searchType").val(searchType);
			}
			<c:if test="${not empty accountType}">$("#stype").val("${accountType}");</c:if>
			getTab();
			bindType();
			checkMutli();
		})

		function checkMutli() {
			$.ajax({
				url : "${base}/daili/dlaccount/multidata.do",
				data:{limit:1},
				success : function(result) {
					if ("off" == result.multiAgent) {
						$("#datagrid_tb").bootstrapTable("hideColumn",
								"gameShare");
					} else if ("on" == result.multiAgent && "on" != result.mergeRebate) {
						$("#datagrid_tb").bootstrapTable("showColumn",
								"gameShare");
					}
					if ("off" == result.profitShare) {
						$("#datagrid_tb").bootstrapTable("hideColumn",
								"profitShare");
					} else if ("on" == result.multiAgent && "on" != result.mergeRebate) {
						$("#datagrid_tb").bootstrapTable("showColumn",
								"profitShare");
					}
					if ("off" == result.dynamicRate) {
						$("#datagrid_tb").bootstrapTable("hideColumn",
								"dynamicRate");
					} else if ("on" == result.multiAgent && "on" != result.mergeRebate) {
						$("#datagrid_tb").bootstrapTable("showColumn",
								"dynamicRate");
					}
					
				}
			});
		}

		function bindType() {
			$("#accountType").change(function() {
				var selval = $(this).children('option:selected').val();
				if (selval == 4 && agentMulti == "on") {
					$(".multiData").removeClass("hidden");
				} else {
					$(".multiData").addClass("hidden");
				}
			});
		}
		function back() {
			var index = ps.length - 2;

			if (parentId == 0 || index < 0) {
				Msg.info("当前已经是最顶级了");
				return;
			}
			data = ps[index];
			parentId = data.id;
			opFlag = data.flag;
			var nps = [];
			for (var i = 0; i < ps.length; i++) {
				if (i == ps.length - 1) {
					break;
				}
				nps.push(ps[i]);
			}
			ps = nps;
			search();
		}
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
		
		function add() {
			$.ajax({
				url : "${base}/daili/dlaccount/multidata.do",
				data:{limit:1},
				success : function(result) {
					$("#editmodel").modal('toggle');
					$("#account").val("");
					$("#account").removeAttr("disabled");
					$("#accountId").val("");
					$("#accountStatus").val(2);
					$("#accountStatus").attr("disabled", "disabled");
					$("#accountType").val("");
					$("#newpwd").val("");
					$("#newrpwd").val("");
					$("#rebateNum").val("");
					$(".newpwd_tr").removeClass("hidden");
					$(".userinfo_tr").addClass("hidden");
					$("#save_btn").removeClass("hidden");
					$("#rebateNum").removeAttr("disabled");
					$("#accountType").removeAttr("disabled");
					$("#parentNames").val("");
					$(".parentNames_tr").addClass("hidden");
					$("#general_opt").addClass("hidden");
					if ("off" == result.multiAgent ) {
						if("off"==result.mergeRebate){
							if('${loginUser.accountType}' == 4){
								$("#agent_opt").addClass("hidden");
							}else{
								$("#member_opt").addClass("hidden");
							}
							$(".multiData").addClass("hidden");
						}
						
					} else if ("on" == result.multiAgent && "on" != result.mergeRebate) {
						$(".multiData").removeClass("hidden");
						$("#rebateNum").val("");
						$(".rebateNum_tr").removeClass("hidden");
						$("#agent_opt").removeClass("hidden");
						$("#rebate_desc").html(
								"(可设置返点区间: "+result.rebateMinNum+" - " + result.rebateMaxNum +" )");
					}
					if ("off" == result.dynamicRate) {
						/* if('${loginUser.accountType}' == 4){
							$("#agent_opt").addClass("hidden");
						}else{
							$("#member_opt").addClass("hidden");
						}
						$(".multiData").addClass("hidden"); */
					} else if ("on" == result.dynamicRate) {
						$("#dynamicRate").removeAttr("disabled");
						$(".dynamicRateData").removeClass("hidden");
						$("#dynamicRate").val("");
						$(".dynamicRate_tr").removeClass("hidden");
						//$("#agent_opt").removeClass("hidden");
						$("#dynamicRate_desc").html(
								"(可设置赔率区间: "+result.dynamicRateMin+" - " + result.dynamicRateMax +" )");
					}
					
					agentMulti = result.multiAgent;
				}
			});
		}
	
		var levels ={};
		<c:forEach items="${levels}" var="l">levels['${l.id}']='${l.levelName}';</c:forEach>
		function levelFormatter(value, row, index) {
			return [ '<span class="text-primary">', '</span>' ].join(levels[value]);
		}
		
		function save() {
			var password = $("#newpwd").val();
			var rpassword = $("#newrpwd").val();
			var accountId = $("#accountId").val();
			if (!accountId) {
				if (!password) {
					Msg.info("新密码不能为空！");
					return;
				}
				if (!rpassword) {
					Msg.info("确认密码不能为空！");
					return;
				}
			}

			if (password !== rpassword) {
				Msg.info("两次密码不一致！");
				return;
			}
			$.ajax({
				url : "${base}/daili/dlaccount/save.do",
				data : {
					id : accountId,
					account : $("#account").val(),
					accountStatus : $("#accountStatus").val(),
					pwd : password,
					rpwd : rpassword,
					type : $("#accountType").val(),
					rebateNum : $("#rebateNum").val(),
					dynamicRate : $("#dynamicRate").val()
				},
				success : function(result) {
					Msg.info("保存成功！");
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}
	</script>
</body>
</html>