<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">取款日志</li>
		</ol>
	</div>

	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="input-group">
						<select id="ssearchType" class="form-control">
							<option value="1">直属下级</option>
							<option value="2">所有下级</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sagent">代理及下级查询</label> <input type="text" class="form-control" id="sagent" value="${sagent }" placeholder="代理及下级查询">
					</div>
					<div class="input-group">
						<label class="sr-only" for="userAccount">单用户查询</label> <input type="text" class="form-control" id="userAccount" value="${userAccount }" placeholder="单用户查询">
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" onclick="reset();">重置</button>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<label class="sr-only" for="saccountType"></label> <select class="form-control" id="saccountType">
							<option value="0" class="text-warning">全部类型</option>
							<option value="1" class="text-warning">会员</option>
							<option value="4" class="text-success">代理</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sstatus">状态</label> <select class="form-control" id="sstatus">
							<option value="0" class="text-warning">全部记录</option>
							<option value="1" class="text-warning">处理中</option>
							<option value="2" class="text-success">处理成功</option>
							<option value="3" class="text-danger">处理失败</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sstatus">状态</label> <select class="form-control" id="sstatus">
							<option value="0" class="text-warning">全部记录</option>
							<option value="1" class="text-warning">处理中</option>
							<option value="2" class="text-success">处理成功</option>
							<option value="3" class="text-danger">处理失败</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sdrawTimes">状态</label> <select class="form-control" id="sdrawTimes">
							<option value="">提款类型</option>
							<option value="1" class="text-warning">首次提款</option>
							<option value="2" class="text-danger">二次提款</option>
							<option value="3" class="text-success">三次提款</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<script type="text/javascript">
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				endDate : curDate,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/dldrawrd/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '提款会员',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'orderNo',
					title : '订单号',
					align : 'center',
					valign : 'middle',
					formatter : orderFormatter
				}
				// 				, {
				// 					field : 'cardNo',
				// 					title : '收款账号',
				// 					align : 'center',
				// 					valign : 'middle'
				// 				}
				// 				, {
				// 					field : 'userName',
				// 					title : '收款户名',
				// 					align : 'center',
				// 					valign : 'middle'
				// 				}
				, {
					field : 'bankName',
					title : '开户行',
					align : 'center',
					valign : 'middle',
					formatter : addressFormatter
				}, {
					field : 'drawMoney',
					title : '提款金额',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'createDatetime',
					title : '交易时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'type',
					title : '存取类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					valign : 'middle',
					formatter : statusFormatter
				}, {
					field : 'remark',
					title : '备注',
					align : 'center',
					valign : 'middle'
				} ]
			});
		}

		function typeFormatter(value, row, index) {
			return GlobalTypeUtil.getTypeName(1, 1, value);
		}

		function addressFormatter(value, row, index) {
			if (value) {
				return value;
			}
		}

		function statusFormatter(value, row, index) {

			var sn = GlobalTypeUtil.getTypeName(1, 3, value);

			if (value === 2) {
				return [ '<span class="text-success">', '</span>' ].join(sn);
			} else if (value === 3) {
				return [ '<span class="text-danger">', '</span>' ].join(sn);
			} else if (value === 4) {
				return [ '<span class="text-default">', '</span>' ].join(sn);
			}
			return [ '<span class="text-primary">', '</span>' ].join(sn);
		}

		function moneyFormatter(value, row, index) {

			if (value === undefined) {
				return "0.00";
			}

			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ]
						.join(toDecimal2(value));
			}
			return [ '<span class="text-primary">', '</span>' ].join("0.00");

		}
		
		function orderFormatter(value, row, index) {
			var html="";
			var text=''
			if(row.firstDraw==2){
				html=" <button class='btn btn-default' id='prompt'>首提</button>"
			}
			if (row.drawTimes == 1){
				text ="<span class='label label-warning'>首提</span>"
			}
			if (row.drawTimes == 2){
				text ="<span class='label label-danger'>二提</span>"
			}
			if (row.drawTimes == 3){
				text ="<span class='label label-success'>三提</span>"
			}
			return value+[text].join('')+html;
		}
		
		//制保留2位小数，如：2，会在2后面补上00.即2.00 
		function toDecimal2(x) {
			var f = parseFloat(x);
			if (isNaN(f)) {
				return false;
			}
			var f = Math.round(x * 100) / 100;
			var s = f.toString();
			var rs = s.indexOf('.');
			if (rs < 0) {
				rs = s.length;
				s += '.';
			}
			while (s.length <= rs + 2) {
				s += '0';
			}
			return s;
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		function remarkFormatter(value, row, index) {
			return [ "<p class='text-danger'>", "</p>" ].join(value);
		}

		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#userAccount").val();
			params['agent'] = $("#sagent").val();
			params['status'] = $("#sstatus").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			params['accountType'] = $("#saccountType").val();
			params['searchType'] = $("#ssearchType").val();
			params['drawTimes'] = $("#sdrawTimes").val();
			return params
		}
		$(function() {
// 			var begin = DateUtil.formatDatetime(DateUtil.getCurrentDate());
// 			setDate(begin, begin);
			bindbtn();
			today();
			getTab();
		})
		
		function today(){
			var cur = DateUtil.getCurrentDate();
			setDate(cur+" 00:00:00", cur+" 23:59:59");
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
		
		function reset() {
			var cur = DateUtil.getCurrentDate();
			$("#userAccount").val("");
			$("#sagent").val("");
			$("#saccountType").val("0");
			$("#sstatus").val("0");
			setDate(cur, cur);
		}
		
		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
			});
		}
	</script>
	<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
</body>
</html>