<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">取款日志</li>
		</ol>
	</div>

	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="input-group">
						<select id="ssearchType" class="form-control">
							<option value="1">直属下级</option>
							<option value="2">所有下级</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sagent">代理及下级查询</label> <input type="text" class="form-control" id="sagent" value="${sagent }" placeholder="代理及下级查询">
					</div>
					<div class="input-group">
						<label class="sr-only" for="userAccount">单用户查询</label> <input type="text" class="form-control" id="userAccount" value="${userAccount }" placeholder="单用户查询">
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" onclick="reset();">重置</button>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<label class="sr-only" for="saccountType"></label> <select class="form-control" id="saccountType">
							<option value="0" class="text-warning">全部类型</option>
							<option value="1" class="text-warning">会员</option>
							<option value="4" class="text-success">代理</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="loginOs">操作系统</label> <input type="text" class="form-control" id="loginOs" value="${loginOs }" placeholder="输入操作系统">
					</div>
					<div class="input-group">
						<label class="sr-only" for="ip">ip查询</label> <input type="text" class="form-control" id="ip" value="${ip }" placeholder="输入操作ip查询">
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<script type="text/javascript">
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				endDate : curDate,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/dlloginlog/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'id',
					title : '编号',
					align : 'center',
					valign : 'bottom'
				}, {
					field : 'agentName',
					title : '上级代理',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'account',
					title : '账号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'accountType',
					title : '用户类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'content',
					title : '操作记录',
					align : 'center',
					valign : 'middle',
					formatter : remarkFormatter
				}, {
					field : 'accountIp',
					title : 'IP',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'address',
					title : 'IP地址',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'loginOs',
					title : '操作系统',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'createDatetime',
					title : '操作时间',
					align : 'center',
					width : '200px',
					valign : 'middle',
					formatter : dateFormatter
				} ]
			});
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		function remarkFormatter(value, row, index) {
			return [ "<p class='text-danger'>", "</p>" ].join(value);
		}
		var typeNames={
			1 : "会员",
			2 : "公司超级管理员",
			3 : "公司管理员",
			4 : "代理",
			5 : "总代理",
			6 : "试玩账号"
		};
		function typeFormatter(value, row, index) {
			return [ '<span class="text-primary">', '</span>' ]
					.join(typeNames[value]);
		}

		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#userAccount").val();
			params['agentName'] = $("#sagent").val();
			params['status'] = $("#sstatus").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			params['accountType'] = $("#saccountType").val();
			params['searchType'] = $("#ssearchType").val();
			params['ip'] = $("#ip").val();
			params['loginOs'] = $("#loginOs").val();
			return params
		}
		$(function() {
			bindbtn();
			today();
			getTab();
		})
		
		function today(){
			var cur = DateUtil.getCurrentDate();
			setDate(cur+" 00:00:00", cur+" 23:59:59");
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
		
		function reset() {
			var cur = DateUtil.getCurrentDate();
			$("#userAccount").val("");
			$("#sagent").val("");
			$("#saccountType").val("0");
			$("#sstatus").val("0");
			$("#ip").val("");
			$("#loginOs").val("");
			setDate(cur+" 00:00:00", cur+" 23:59:59");
		}
		
		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
			});
		}
	</script>
</body>
</html>