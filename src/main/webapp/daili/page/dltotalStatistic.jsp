<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

<input type="hidden" id="lottype" value="-1">
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" value="${begin }" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="form-group">
						<div class="input-group">
							<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" id="agentName" value="${agentName }" placeholder="代理及下线查询">
						</div>
						<div class="input-group">
							<select id="searchType" class="form-control">
								<option value="1" <c:if test="${searchType == 1}">selected ="selected" </c:if> >直属下级</option>
								<option value="2" <c:if test="${searchType == 2}">selected ="selected" </c:if> >所有下级</option>
							</select>
						</div>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" value="${begin }" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<input type="text" class="form-control" id="account" value="${account }" placeholder="会员名称">
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"  data-show-footer="true"></table>
	</div>
	<script type="text/javascript">
		function getTab() {
			var options = {
				language : 'zh-CN',
				autoclose : 1,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/dlstatistic/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				showPageSummary:true,
				showAllSummary:true,
				columns : [ {
					field : 'account',
					title : '用户账号',
					align : 'center',
					valign : 'middle',
					pageSummaryFormat : function(rows, aggsData) {
						return "小计:";
					},
					allSummaryFormat : function(rows, aggsData) {
						return "总计:";
					}
				}, {
					field : 'accountType',
					title : '类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'bettingAmount',
					title : '投注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'bettingAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'bettingAmountTotal');
					}
				}, {
					field : 'awardAmount',
					title : '中奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'awardAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'awardAmountTotal');
					}
				}, {
					field : 'depositAmount',
					title : '存款总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'depositAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'depositAmountTotal');
					}
				}, {
					field : 'depositGiftAmount',
					title : '存款赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'depositGiftAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'depositGiftAmountTotal');
					}
				},{
					field : 'depositTimes',
					title : '存款次数',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'depositTimes');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'depositTimesTotal');
					}
				}, {
					field : 'registerGiftAmount',
					title : '注册赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'registerGiftAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'registerGiftAmountTotal');
					}
				}, {
					field : 'withdrawAmount',
					title : '提款总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'withdrawAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'withdrawAmountTotal');
					}
				}, {
					field : 'rebateAmount',
					title : '反水总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'rebateAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'rebateAmountTotal');
					}
				}, {
					field : 'rebateAgentAmount',
					title : '返点总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'rebateAgentAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'rebateAgentAmountTotal');
					}
				}, {
					field : 'manualDepositAmount',
					title : '手动加款',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'manualDepositAmount');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'manualDepositAmountTotal');
					}
				}
// 				, {
// 					field : 'manualWithdrawAmount',
// 					title : '手动扣款',
// 					align : 'center',
// 					valign : 'middle',
// 					formatter : moneyFormatter,
// 					pageSummaryFormat:function(rows,aggsData){
// 						return getTotal(rows,'manualWithdrawAmount');;
// 					},
// 					allSummaryFormat:function(rows,aggsData){
// 						return totalFormatter(aggsData,'manualWithdrawAmountTotal');
// 					}
// 				}
				, {
					field : 'profit',
					title : '盈亏',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'profit');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'profitTotal');
					}
				}, {
					field : 'teamProfit',
					title : '团队盈亏',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter,
					formatter : moneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return getTotal(rows,'teamProfit');;
					},
					allSummaryFormat:function(rows,aggsData){
						return totalFormatter(aggsData,'teamProfitTotal');
					}
				}, {
					title : '上下级',
					align : 'center',
					valign : 'middle',
					formatter : optFormatter
				} ],
				onLoadSuccess : function(d) {
					$('.upLevel').click(function(){
						if('${loginUser.account}' == this.id){
							$("#account").val("");
						}else{
							$("#account").val(this.id);
						}
						
						$("#agentName").val("");
						$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
					});
					$('.nextLevel').click(function(){
						$("#account").val("");
						$("#agentName").val(this.id);
						$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
					});
					return false;
				}
			});
		}
		function getTotal(rows,itemKey){
			var total = 0;
			for(var i=0;i<rows.length;i++){
				var r = rows[i];
				if(!r[itemKey]){
					continue;
				}
				total += r[itemKey];
			}
			return aggsMoneyFormatter(total.toFixed(2));
		}

		function optFormatter(value, row, index) {
			value = row.agentId;
			var col = "";
			if (value && value != 0) {
				col += '<a href="javascript:void(0)" class="uplevel"><span class="text-success upLevel" id="'+row.agentName+'">上级</span></a>';
			}
			value = row.accountType;
			if (value == 4) {
				col += '<a href="javascript:void(0)" class="downlevel"><span class="text-success nextLevel" id="'+row.account+'">下级</span></a>';
			}
			return col;
		}

		var memType = {
				1 : "会员",
				2 : "租户超级管理员",
				3 : "租户管理员",
				4 : "代理",
				5 : "总代理"
			};

		function typeFormatter(value, row, index) {
			return memType[value];
		}
		function moneyFormatter(value, row, index) {
			if (value === undefined || value == 0) {
				return "<span class='text-primary'>-</span>";
			}
			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ].join(value
						.toFixed(2));
			}
			return [ '<span class="text-primary">', '</span>' ].join(value
					.toFixed(2));
		}

			function totalFormatter(aggsData,key){
				var money = "0.00";
				if(aggsData && aggsData[key]){
					money = aggsData[key].toFixed(2);
				}
				return aggsMoneyFormatter(money);
			}

			function aggsMoneyFormatter(value){
				if (value && value > 0) {
					return [ '<span class="text-danger">', '</span>' ].join(value);
				}
				return [ '<span class="text-primary">', '</span>' ].join(value);
			}
		
		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#account").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			params['searchType'] = $("#searchType").val();
			params['agentName'] = $("#agentName").val();
			return params
		}
		$(function() {
			bindbtn();
			getTab();
		})
		
		function search() {
			$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();;
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin, end);
				search();
			});
		}
	</script>
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	vertical-align : middle;
}
</style>
</body>
</html>