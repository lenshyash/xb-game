<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
	<div class="container">
		<div id="toolbar">
			<form id="searchForm">
				<div class="form-group">
					<div class="form-inline">
						<div class="form-group">
						
							<div class="input-group">
								<input type="text" name="bettingCode" class="form-control" placeholder="注单号"></span>
							</div>
							<div class="input-group">
								<input type="text" class="form-control" name="account" placeholder="会员账号">
							</div>
							<div class="input-group">
								<select name="sportType" class="form-control" >
									<option value="0">--所有球类--</option>
									<option value="1">足球</option>
									<option value="2">篮球</option>
								</select>
							</div>
							
							<div class="input-group">
								<input type="text" class="form-control" name="home" placeholder="主队名称">
							</div>
						</div>
						
						<button class="btn btn-primary" type="button" onclick="search();">查询</button>
					</div>
					<div class="form-inline" style="margin-top: 5px;">
						<div class="input-group">
							<input type="text" class="form-control" name="beginDate" placeholder="开始日期" value="${date } 00:00:00"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="input-group">
							<input type="text" name="endDate" class="form-control" placeholder="线束日期" value="${date } 23:59:59"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="input-group">
							<select name="balanceStatus" class="form-control" >
								<option value="0">--结算状态--</option>
								<option value="1">未结算</option>
								<option value="2">已结算</option>
								<option value="3">赛事腰斩</option>
							</select>
						</div>
						
						<div class="input-group">
							<input type="text" class="form-control" name="guest" placeholder="客队名称">
						</div>
						<button class="btn btn-success" type="button" onclick="resetform();">重置</button>
					</div>
				</div>
			</form>
		</div>
		<table id="datagrid_tb"></table>
	</div>
</body>
</html>
<script type="text/javascript">
var FormUtil = {
	getValues : function(formId) {
		var form = $("#" + formId);
		var o = {};
		var a = form.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	}
}
	var $form = $("#searchForm");
	var parameter = {}; 
	function search(){
		var values = FormUtil.getValues("searchForm");
		var json = JSON.encode(values);
		parameter["search"] = json;
		table.bootstrapTable("refresh");
		//console.info(values);
	}
	
	function resetform(){
		if(parameter.search){
			delete parameter.search;
		}
		$("#searchForm")[0].reset();
		//parameter.pageNumber = 1;
		table.bootstrapTable("selectPage",1);
	}
	
	var curDate = new Date();
	var options = {
		language : 'zh-CN',
		autoclose : true,
	    weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showSecond:1,
        minuteStep:1,
		endDate : curDate,
		format : 'yyyy-mm-dd hh:ii:00'
	};
	$form.find('input[name=beginDate]').datetimepicker(options);
	options.format="yyyy-mm-dd hh:ii:59";
	$form.find('input[name=endDate]').datetimepicker(options);
	
	
	window.table = new Game.Table({
		id : 'datagrid_tb',
		url : '${base}/daili/sportRecord/list.do',
		showToggle : false,
		queryParams : function(param){//参数
			if(!parameter){
				return param;
			}
			for(var key in parameter){
				param[key] = parameter[key];
			}
			return param;
		},
		toolbar : $('#toolbar'),
		columns : [ {
			title : '单号/投注日期',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter:function(value,row, index){
				var bd = new Date(row.bettingDate);
				return row.bettingCode + "<br/>" + bd.format("MM月dd日,hh:mm:ss");
			}
		}, {	
			title : '会员',
			align : 'center',
			width : '100',
			valign : 'bottom',
			formatter:function(value,row, index){
				return row.memberName;
			}
		},{
			title : '投注类型',
			align : 'center',
			width : '150',
			//filed : "sportType",
			valign : 'middle',
			formatter:function(value,row, index){
				var st = row.sportType
				var html = "";
				if(st == 1){
					html += "足球";
				}else if(st == 2){
					html += "篮球";
				}
			
				if(row.gameTimeType == 1){
					html += " - 滚球";
				}else if(row.gameTimeType == 2){
					html += " - 今日";
				}else if(row.gameTimeType == 3){
					html += " - 早盘";
				}
				
				html += "<br/>";
				if(row.mix == 2){
					return html + "混合过关";
				}
				var ts = row.typeNames;
				if(!ts){
					return html;
				}
				return html + ts.replace("-","<br>");
			}
		}, {
			title : '投注项',
			align : 'center',
			width : '300',
			valign : 'middle',
			field : 'remark',
			formatter:function(value,row, index){
				if(row.mix != 2){
					return toBetHtml(JSON.decode(value),row);
				}
				var html = "";
				var arr = JSON.decode(value)
				for(var i=0;i<arr.length;i++){
					if(i != 0){
						html += "<div style='border-bottom:1px #303030 dotted;'></div>";
					}
					html += toBetHtml(arr[i],row);
				}
				return html;
			}
		}, {
			field : 'bettingMoney',
			title : '投注额',
			align : 'center',
			width : '80',
			valign : 'middle',
			formatter:function(value,row, index){
				return "<span class='text-danger'>"+value+"</span>";
			}
		}, {
			title : '提交状态',
			align : 'center',
			field : 'bettingStatus',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				
				if(value == 1){
					return "<font color='blue'>待确认</font>";
				}
				if(value == 2){
					return "已确认";
				}
				
				if(value == 3){
					return "<font color='red'>取消</font>";
				}
				
				if(value == 4){
					return "<font color='red'>手动取消</font>";
				}
			}
		},{
			title : '结算状态',
			align : 'center',
			field : 'balance',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				if(value == 1){
					return "<font color='blue'>未结算</font>";
				}
				if(value == 2){
					return "<font color='green'>已结算</font>";
				}
				
				if(value == 3){
					return "<font color='red'>结算失败</font>";
				}
				
				if(value == 4){
					return "<font color='red'>比赛腰斩</font>";
				}
				
				if(value == 5 || value == 6){
					return "<font color='green'>手动结算</font>";
				}
			}
		},{
			title : '派彩金额',
			align : 'center',
			field : 'bettingResult',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				if(!value && value != 0){
					return value;
				}
				
				if(value > 0){
					return "<span class='text-danger'>"+value+"</span>";
				}
				return "<span class='text-primary'>"+value+"</span>";
			}
		}]
	});
	
	function toBetHtml(item,row){
		var con = item.con;
		if(con.indexOf("vs") == -1){
			con = '<span class="text-danger">'+ con +'</span>';
		}
		var homeFirst = row.homeTeam  == item.firstTeam;//主队是否在前
		var scoreStr = "";
		
		
		if(row.gameTimeType == 1){
			if(homeFirst){
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH +":" + row.scoreC + ")</b></font>";
			}else{
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC +":" + row.scoreH + ")</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if(item.half === true && row.mix == 2){
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}
		
		var html  = item.league +"<br/>" + 
					home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
					"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
		var balance = row.mix != 2 ? row.balance : item.balance;		
		if(balance == 4){
			html = "<s style='color:red;'>" + html +"</s>"
		}else if(balance == 2 || balance == 5 || balance == 6){
			var mr = row.mix != 2 ? row.result:item.matchResult;
			if(homeFirst){
				html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
			}else{
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>("+ss[1]+":"+ss[0]+")</font>";
			}
		}			
		return html;
	}
	
</script>
