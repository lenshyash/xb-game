<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

<input type="hidden" id="lottype" value="-1">
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" id="account" placeholder="会员账号">
						</div>
						<div class="input-group">
							<input type="text" class="form-control" id="yxQiHao" placeholder="投注期号">
						</div>
						<div class="input-group">
							<label class="sr-only" for="type">类型</label> 
							<select class="form-control" id="type">
								<option value=" ">全部类型</option>
								<option value="1">等待开奖</option>
								<option value="2">已中奖</option>
								<option value="3">未中奖</option>
								<option value="4">撤单</option>
								<option value="5">派奖回滚成功</option>
								<option value="6">回滚异常的</option>
								<option value="7">开奖异常</option>
							</select>
						</div>
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<input type="text" class="form-control" id="yxOrder" placeholder="订单号">
					</div>
					<div class="input-group">
						<select class="form-control" id="groupCodeById">
							<option value="" selected>--全部大类--</option>
							<c:forEach items="${groupList}" var="group">
								<option value="${group.id}">${group.name}</option>
							</c:forEach>
						</select>
					</div>
					<div class="input-group" style="display:none;">
						<select class="form-control" id="playCodeById">
						</select>
					</div>
					<div class="input-group" style="display: none;text-align: center;font-size: 15px;font-weight: bold;" id="buyAccountCountSumMoneny">
						<strong class="input-group" >总计:<span >投注人数:【<span style="color: #46986c" id="buyAccountCount"> 0</span>】</span> <span style="margin-right: 15px">投注金额:【<span style="color: #46986c" id="buySum"> 0</span>】</span></strong>
					</div>
					</div>
			</div>
		</div>
		<table id="datagrid_tb"  data-show-footer="true"></table>

	</div>
	
	<div class="modal fade" id="editmodel"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">订单详情</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped"
						style="clear: both">
							<tbody>
							<tr>
								<td width="20%" class="text-center" colspan="4">订单号:<span id="dingdh"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">账号:</td>
								<td width="35%" class="text-left"><span id="zhangh"></span></td>
								<td width="20%" class="text-right">中奖注数:</td>
								<td width="35%" class="text-left"><span id="zhongjzs"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">下注时间:</td>
								<td width="35%" class="text-left"><span id="xiazsj"></span></td>
								<td width="20%" class="text-right">投注注数:</td>
								<td width="35%" class="text-left"><span id="touzzs"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">彩种:</td>
								<td width="35%" class="text-left"><span id="caiz"></span></td>
								<td width="20%" class="text-right">状态:</td>
								<td width="35%" class="text-left"><span id="zhuangt"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">期号:</td>
								<td width="35%" class="text-left"><span id="qih"></span></td>
								<td width="20%" class="text-right">投注总额:</td>
								<td width="35%" class="text-left"><span id="touzze"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">玩法:</td>
								<td width="35%" class="text-left"><span id="wanf"></span></td>
								<td width="20%" class="text-right">赔率:</td>
								<td width="35%" class="text-left"><span id="danzjj"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">开奖号码:</td>
								<td width="35%" class="text-left"><span id="kaijhm"></span></td>
								<td width="20%" class="text-right">中奖金额:</td>
								<td width="35%" class="text-left"><span id="zhongjje"></span></td>
							</tr>
							<c:if test="${isFanShui }">
							<tr>
								<td width="20%" class="text-right" id="mutil">返水状态:</td>
								<td width="35%" class="text-left"><span id="fanszt"></span></td>
								<td width="20%" class="text-right">返水金额:</td>
								<td width="35%" class="text-left"><span id="jine"></span></td>
								
							</tr>
							</c:if>
							<tr>
								<td width="20%" class="text-right">盈亏:</td>
								<td width="35%" class="text-left"><span id="yingkui"></span></td>
							</tr>
							<tr>
							<td width="20%" class="text-right" colspan="4">
							<textarea class="form-control" rows="3" id="touzhm"></textarea>
							</td>
								 
							</tr>
							<td width="20%" class="text-center info" colspan="4">
							<span class="glyphicon glyphicon-info-sign"></span>
							温馨提示:(中奖号码,既有本命年(0尾),又有非本命年(非0尾) 可能会出现2种赔率的情况)
							</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function getTab() {
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/gameRecord/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				showPageSummary:true,
				showAllSummary:true,
				columns : [ 
				   {
					field : 'orderId',
					title : '订单号',
					align : 'center',
					valign : 'bottom',
					formatter : orderFormatter
				}, {
					field : 'account',
					title : '投注账号',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : accountFormatter
				}, {
					field : 'qiHao',
					title : '期号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'playName',
					title : '玩法名称',
					align : 'center',
					width : '120',
					valign : 'middle'
				}, {
					field : 'haoMa',
					title : '投注号码',
					align : 'center',
					formatter : wsFormatter
				}, {
					field : 'createTime',
					title : '投注时间',
					width : '200',
					align : 'center',
					formatter : dateFormatter
				} , {
					field : 'buyZhuShu',
					title : '注数',
					align : 'center',
					pageSummaryFormat:function(rows,aggsData){
						return "小计:";
					},
					allSummaryFormat:function(rows,aggsData){
						return "总计:";
					}
				} ,{
					field : 'buyMoney',
					title : '投注金额(元)',
					align : 'center',
					pageSummaryFormat:function(rows,aggsData){
						buyCountSUm(aggsData)
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.buyMoney != null && row.status < 4){
								r = r+row.buyMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00"
						}
						return aggsData.buySum ? aggsData.buySum.toFixed(2) : "0.00";
					}
				},
				 {
					field : 'winMoney',
					title : '中奖金额(元)',
					align : 'center',
					pageSummaryFormat:function(rows,aggsData){
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.winMoney != null){
								r = r+row.winMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00"
						}
						return aggsData.winSum ? aggsData.winSum.toFixed(2) : "0.00";
					}
				},
				 {
					field : 'status',
					title : '状态',
					align : 'center',
					formatter : statusFormatter
				}
			],
				onLoadSuccess:function (data){
					if ($('.no-records-found').length){
						$('#buyAccountCountSumMoneny').css('display','none')
					}
				}
			});
		}
		
		function accountFormatter(value, row, index) {
			return '<a class="detail" href="javascript:void(0)" title="用户详情"><span class="text-danger">'+value+'</span></a>';
		}
		
		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$.get("${base}/daili/dlaccount/view.do?id="+row.accountId,function(html){
					Msg.info(html,"查看账户详细");
				},"html");
			}
		};
		function buyCountSUm(aggsData){
			$(function (){
				if (aggsData){
					$('#buyAccountCountSumMoneny').css('display','inline-block')
					if('buyAccountCount' in aggsData && aggsData.buyAccountCount != 0){
						$('#buyAccountCount').parents('span').css('display','inline-block')
						$('#buyAccountCount').text(aggsData.buyAccountCount)
					}else{
						$('#buyAccountCount').parents('span').css('display','none')
					}
					if('buySum' in aggsData && aggsData.buySum != 0){
						$('#buySum').parents('span').css('display','inline-block')
						$('#buySum').text(aggsData.buySum)
					}else{
						$('#buySum').parents('span').css('display','none')
					}
					if(!(('buyAccountCount' in aggsData) && ('buySum' in aggsData))){
						$('#buyAccountCountSumMoneny').css('display','none')
					}
				}else {
					$('#buyAccountCountSumMoneny').css('display','none')
				}
			})
		}
		
		//位数格式化
		function wsFormatter(value, row, index) {
			if(value!=null && value.length>10){
				return  value.substring(0, 10)+"...";
			}else{
				return value;
			}
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		//格式化订单详情		
		function orderFormatter(value, row, index){
			return '<a href="#" onClick="orderDesc(\''+row.orderId+'\',\''+row.account+'\',\''+row.lotCode+'\');">' + row.orderId+ '</a>';
		}
		
		//查看化订单详情		
		function orderDesc(orderId,account,lotCode) {
			$.ajax({
				url : "${base}/daili/gameRecord/getOrderDetailForList.do",
				data : {
					"orderId" : orderId,
					"account" : account,
					"lotCode" : lotCode
				},
				success:function(j){
					$("#editmodel").modal('toggle');
		 			$('#dingdh').html(j.orderId);
		 			$('#zhangh').html(j.account);
		 			$('#danzje').html(fmoney(2/j.model,2));
		 			$('#xiazsj').html(DateUtil.formatDatetime(j.createTime));
		 			$('#touzzs').html(j.buyZhuShu);
		 			$('#caiz').html('六合彩');
		 			$('#beis').html(j.multiple);
		 			$('#qih').html(j.qiHao);
		 			if(j.buyMoney != null){
		 				$('#touzze').html(fmoney(j.buyMoney,2));
		 			}
		 			
		 			$('#wanf').html(j.playName);
		 			if(j.peilv != null){
		 				$('#danzjj').html(j.peilv);
		 			}
		 			
		 			$('#kaijhm').html(j.lotteryHaoMa);
		 			if(j.rollBackMoney != null){
		 				$('#jine').html(fmoney(j.rollBackMoney,2));
		 			}
		 			$('#zhuangt').html(kjzt(j.status));
		 			if(j.winMoney){
		 				$('#zhongjje').html(fmoney(j.winMoney,2));
		 			}else{
		 				$('#zhongjje').html('0.00');
		 			}
		 			if(j.winZhuShu){
		 				$('#zhongjzs').html(j.winZhuShu);
		 			}else{
		 				$('#zhongjzs').html("0.00");
		 			}
		 			
		 			$('#touzhm').html(j.haoMa);
		 			var yk = '0.00';
		 			var wm = j.winMoney;
		 			var rb = j.rollBackMoney;
		 			var bm = j.buyMoney;
		 			if(wm && wm*1 >=0){
		 				yk = yk*1 + wm*1;
		 			}
		 			if(rb && rb*1 >=0){
		 				yk = yk*1 + rb*1;
		 			}
		 			if(bm && bm*1 >=0){
		 				yk = yk*1 - bm*1;
		 			}
		 			$('#yingkui').html(fmoney(yk*1,2));
		 			if("${isMulti}"){
		 				$('#mutil').html("返点状态:");
		 				$('#fanszt').html(mutilFormatter(j.rollBackStatus,1));
		 			}else{
		 				$('#mutil').html("返水状态:");
		 				$('#fanszt').html(mutilFormatter(j.rollBackStatus,2));
		 			}
				}
			});
		}
		
		function mutilFormatter(value,type){
			var col = '';
			if(type == 1){
				col = "返点";
			}else{
				col = "返水";
			}
			switch(value){
			case 1:
			case 2:
				col = "还未"+col;
				break;
			case 3:
				col = col + "已经回滚"
				break;
			case 4:
				col = "已经"+col;
				break;
			}
			return col;
		}
		
		//格式化成两位小数
		function fmoney(s, n) { 
			n = n > 0 && n <= 20 ? n : 2; 
			s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
			var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
			t = ""; 
			for (i = 0; i < l.length; i++) { 
			t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "" : ""); 
			} 
			return t.split("").reverse().join("") + "." + r; 
			} 
		
		function statusFormatter(value, row, index) {
			return kjzt(row.status);
		}
		
		function kjzt(obj){
			var col = '';
			switch(obj){
			case 1:
				col = '<span class="label label-primary" >等待开奖</span>';
				break;
			case 2:
				col = '<span class="label label-success" >已中奖</span>';
				break;
			case 3:
				col = '<span class="label label-danger" >未中奖</span>';
				break;
			case 4:
				col = '<span class="label label-info" >撤单</span>';
				break;
			case 5:
				col = '<span class="label label-success">派奖回滚成功</span>';
				break;
			case 6:
				col = '<span class="label label-warning">回滚异常的</span>';
				break;
			case 7:
				col = '<span class="label label-warning">开奖异常</span>';
				break;
			}
			return col;
		}
		
		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#account").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			params['qihao'] = $("#yxQiHao").val();
			params['orderCode'] = $("#yxOrder").val();
			params['status'] = $("#type").val();
			params['account'] = $("#account").val();
			params['code'] = 'LHC';
			params['playCode'] = $("#playCodeById").val();
			return params
		}
		$(function() {
			czGroup();
			bindbtn();
			var t = DateUtil.formatDate(new Date()),
			begin = t +" 00:00:00",
			end = t+" 23:59:59";
			setDate(begin, end);
			getTab();
			changeGroup();
		})
		
			function czGroup(){
			$.ajax({
				url:"${base}/daili/gameRecord/czGroup.do",
				data:"GET",
				DataType:"json",
				success:function(j){
					var col = '<option value="-1" selected="selected">全部彩种</option>';
					for(var i in j){
						if(j[i].code!='LHC'){
							col+='<option value="'+j[i].code+'" id="'+j[i].code+'">'+j[i].name+'</option>';
						}
					}
					$('#czType').html(col);
				}
					
			});
		}
		
		function changeGroup(){
			var $con = $("#toolbar");
			$con.on("change","#groupCodeById",function(){
				var $this = $(this),html='',pcode='';
				if(!$this.val()){
					$con.find("#playCodeById").html('').parent().hide();
					return;
				}
				$.ajax({
					url:"${base}/daili/gameRecord/getPlayByList.do",
					data:{groupId:$this.val()},
					success:function(j){
						if(j.success){
							$.each(j.playList,function(i,d){
								pcode += d.code + "|";
								html += '<option value="'+d.code+'">'+d.name+'</option>'
							})
							html = '<option value="'+pcode.substring(0,pcode.length-1)+'">--全部小类--</option>' + html;
							$con.find("#playCodeById").html(html).parent().show();
						}
					}
				})
			})
		}
		
		function search() {
			$('.zhtj').val('0');
			$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				$('.zhtj').val('0');
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
			});
		}
	</script>
</body>
</html>