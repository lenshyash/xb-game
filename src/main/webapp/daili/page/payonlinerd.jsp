<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">存款日志</li>
		</ol>
	</div>
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" id="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="input-group">
						<select id="ssearchType" class="form-control">
							<option value="1">直属下级</option>
							<option value="2">所有下级</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sagent">代理及下级查询</label> <input type="text" class="form-control" id="sagent" value="${sagent }" placeholder="代理及下级查询">
					</div>
					<div class="input-group">
						<label class="sr-only" for="saccount">单用户查询</label> <input type="text" class="form-control" id="saccount" value="${saccount }" placeholder="单用户查询">
					</div>
					<button class="btn btn-primary" onclick="search();">查询</button>
					<button class="btn btn-primary" onclick="reset();">重置</button>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<label class="sr-only" for="stype">类型</label> <select class="form-control" id="stype">
							<option value="0">全部类型</option>
							<option value="5">在线支付</option>
							<option value="6">快速入款</option>
							<option value="7">一般入款</option>
							<option value="1">人工加款</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="sstatus">状态</label> <select class="form-control" id="sstatus">
							<option value="0">全部状态</option>
							<option value="1" class="text-success">处理中</option>
							<option value="2" class="text-primary">充值成功</option>
							<option value="3" class="text-danger">充值失败</option>
						</select>
					</div>
					<div class="input-group">
						<label class="sr-only" for="scomTimes">状态</label> <select class="form-control" id="scomTimes">
							<option value="">存款类型</option>
							<option value="1" class="text-warning">首次存款</option>
							<option value="2" class="text-danger">二次存款</option>
							<option value="3" class="text-success">三次存款</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	<div class="modal fade" id="confirmModel" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="confirmLabel">处理申请</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="comId">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td width="15%" class="text-right">会员账号：</td>
								<td width="35%" class="text-left"><span id="account_span"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">充值人：</td>
								<td width="35%" class="text-left"><span id="depositor_span"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">转账类型：</td>
								<td width="35%" class="text-left"><span id="payName_span"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">转账金额：</td>
								<td width="35%" class="text-left"><span id="money_span"></span></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">手续费：</td>
								<td width="35%" class="text-left"><input id="fee" class="form-control" value="0" /></td>
							</tr>
							<tr>
								<td width="15%" class="text-right">处理结果：</td>
								<td width="35%" class="text-left"><select id="status" class="form-control">
										<option value="1">批准</option>
										<option value="2">未通过</option>
								</select></td>
							</tr>
							<tr class="opDesc_tr hidden">
								<td width="15%" class="text-right">处理说明：</td>
								<td width="35%" class="text-left"><textarea id="opDesc" class="form-control"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="handler();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				endDate : curDate,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : "${base}/daili/deposit/list.do",
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'account',
					title : '会员账号',
					align : 'center',
					valign : 'middle',
				}, {
					field : 'orderNo',
					title : '订单号',
					align : 'center',
					valign : 'middle',
					formatter : orderFormatter
				}, {
					field : 'payName',
					title : '支付平台',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'money',
					title : '付款金额(/元)',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}
// 				, {
// 					field : 'remark',
// 					title : '支付账号后十位',
// 					align : 'center',
// 					valign : 'middle'
// 				}
				, {
					field : 'createDatetime',
					title : '申请时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}, {
					field : 'type',
					title : '类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, {
					field : 'status',
					title : '状态',
					align : 'center',
					valign : 'middle',
					formatter : statusFormatter
				}]
			});
		}
//operateFormatter  descFormatter


		function typeFormatter(value, row, index) {

			return GlobalTypeUtil.getTypeName(1, 1, value);
		}

		function statusFormatter(value, row, index) {

			var sn = GlobalTypeUtil.getTypeName(1, 2, value);

			if (value === 2) {
				return [ '<span class="text-primary">', '</span>' ].join(sn);
			} else if (value === 3) {
				return [ '<span class="text-danger">', '</span>' ].join(sn);
			} else if (value === 4) {
				return [ '<span class="text-default">', '</span>' ].join(sn);
			}
			return [ '<span class="text-success">', '</span>' ].join(sn);
		}
	
		function moneyFormatter(value, row, index) {

			if (value === undefined) {
				return "0.00";
			}

			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ]
						.join(toDecimal2(value));
			}
			return [ '<span class="text-primary">', '</span>' ].join("0.00");

		}
		
		function orderFormatter(value, row, index) {
			var html="";
			var text = '';
			if (row.comTimes == 1){
				text ="<span class='label label-warning'>首充</span>"
			}
			if (row.comTimes == 2){
				text ="<span class='label label-danger'>二充</span>"
			}
			if (row.comTimes == 3){
				text ="<span class='label label-success'>三充</span>"
			}
			return value+[text].join('')+html;
		}
		
		//制保留2位小数，如：2，会在2后面补上00.即2.00 
		function toDecimal2(x) {
			var f = parseFloat(x);
			if (isNaN(f)) {
				return false;
			}
			var f = Math.round(x * 100) / 100;
			var s = f.toString();
			var rs = s.indexOf('.');
			if (rs < 0) {
				rs = s.length;
				s += '.';
			}
			while (s.length <= rs + 2) {
				s += '0';
			}
			return s;
		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		function remarkFormatter(value, row, index) {
			return [ "<p class='text-danger'>", "</p>" ].join(value);
		}

		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#saccount").val();
			params['agent'] = $("#sagent").val();
			params['searchType'] = $("#ssearchType").val();
			params['type'] = $("#stype").val();
			params['status'] = $("#sstatus").val();
			params['comTimes'] = $("#scomTimes").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			return params
		}
		$(function() {
			bindbtn();
			today();
			getTab();
			bindStatusChg();
		});

		function bindStatusChg() {
			$("#status").change(function() {
				var selval = $(this).children('option:selected').val();
				if (selval > 0) {
					if (selval == 1) {
						$(".opDesc_tr").addClass("hidden");
					} else {
						$(".opDesc_tr").removeClass("hidden");
					}
				}
			});
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refreshOptions', {
				pageNumber : 1
			});
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}
		
		function reset() {
			var cur = DateUtil.getCurrentDate();
			$("#saccount").val("");
			$("#sagent").val("");
			$("#stype").val("0");
			$("#sstatus").val("0");
			setDate(cur, cur);
		}
		
		function today(){
			var cur = DateUtil.getCurrentDate();
			setDate(cur+" 00:00:00", cur+" 23:59:59");
		}
		
		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
			});
		}
	</script>
	<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
</body>
</html>