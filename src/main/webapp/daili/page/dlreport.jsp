<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">团队统计</li>
		</ol>
	</div>

	<div class="container-fluid">
		<div id="toolbar">
			<div id="search">
				<div class="form-group">
					<div class="form-inline">
						<div class="input-group">
							<input type="text" class="form-control" id="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<button class="btn btn-default">今日</button>
						<button class="btn btn-default">昨日</button>
						<button class="btn btn-default">本周</button>
						<div class="input-group">
							<select id="searchType" class="form-control">
								<option value="1">直属下级</option>
								<option value="2">所有下级</option>
							</select>
						</div>
						<div class="input-group">
							<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" id="agent" placeholder="代理及下级查询">
						</div>
						<div class="input-group">
							<label class="sr-only" for="account">单用户查询</label> <input type="text" class="form-control" id="account" placeholder="单用户查询">
						</div>
						<button class="btn btn-primary" onclick="search();">查询</button>
						<button class="btn btn-primary" onclick="reset();">重置</button>
					</div>
					<div class="form-inline" style="margin-top: 5px;">
						<div class="input-group">
							<input type="text" id="end" class="form-control" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<button class="btn btn-default">上周</button>
						<button class="btn btn-default">本月</button>
						<button class="btn btn-default">上月</button>
					</div>
				</div>
			</div>
		</div>
		<br>
		<table id="datagrid_tb_base"></table>
		<br>
		<table id="datagrid_tb_self"></table>
		<br>
		<table id="datagrid_tb_other"></table>
		<br>
		<table id="datagrid_tb_cunqu"></table>
		<br>
		<div id="remark_div" style="padding: 10px;">
			<span>输赢公式：</span><span class="text-danger">投注-派奖=输赢</span><br> <span>全部输赢公式：</span><span class="text-danger">各类游戏输赢总和-代理返点-会员反水=全部输赢</span><br> <span>派奖总计：</span> <span
				class="text-danger">因为派奖是定时执行任务，会出现时间临界点情况，当存在24点前的投注，会造成些许误差</span>
				<div style="color:blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
		</div>
	</div>
	<script type="text/javascript">
		function getTab() {
			var curDate = new Date();
			var options = {
				language : 'zh-CN',
				autoclose : true,
				minView : 2,
				endDate : curDate,
				format : 'yyyy-mm-dd'
			};
			$('#begin').datetimepicker(options);
			$('#end').datetimepicker(options);

			$("#datagrid_tb_base").bootstrapTable({
				striped : true,
				columns : [ {
					field : 'depositTotal',
					title : '存款总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'withdrawTotal',
					title : '提款总计',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'rebateAgentTotal',
					title : '返点总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'rebateTotal',
					title : '反水总计',
					align : 'center',
					width : '200',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'giveTotal',
					title : '充值赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'giveRegisterTotal',
					title : '注册赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				},{
					field : 'activeAwardTotal',
					title : '活动中奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				},{
					field : 'manualDepositTotal',
					title : '手动加款',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'manualWithdrawTotal',
					title : '手动扣款',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'sumMoney',
					title : '团队余额',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				}, {
					field : 'firstDepositTotal',
					title : '首充人数',
					align : 'center',
					valign : 'middle'
				},  {
					field : 'registerAccountYes',
					title : '昨日注册人数',
					align : 'center',
					valign : 'middle'
				},  {
					field : 'registerAccount',
					title : '今日注册人数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'betCountTotal',
					title : '总投注人数',
					align : 'center',
					valign : 'middle'
				}  ]
			});
			
			$("#datagrid_tb_cunqu").bootstrapTable({
				striped : true,
				columns : [ {
					field : 'betCountTotal',
					title : '投注总人数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'depositTimes',
					title : '存款总次数',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'withdrawTimes',
					title : '提款总次数',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'depositMemberCount',
					title : '充值总人数',
					align : 'center',
					width : '200',
					valign : 'middle'
				}, {
					field : 'withdrawMemberCount',
					title : '提款总人数',
					align : 'center',
					valign : 'middle'
				} ]
			});
			
			var totalColumns = [];
			var bunkoColumns = [];
			var lottery = '${lottery}';
			var real = '${real}';
			var sport = '${sport}';
			var markSix = '${markSix}';
			if (sport === 'on') {
				totalColumns.push({
					field : 'sportTotal',
					title : '体育下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sportAward',
					title : '体育派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sportBunko',
					title : '体育输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (real === 'on') {
				totalColumns.push({
					field : 'realTotal',
					title : '真人下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'realAward',
					title : '真人派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'realBunko',
					title : '真人输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if ("${dianZi}" === 'on') {
				totalColumns.push({
					field : 'dianZiTotal',
					title : '电子下注总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'dianZiAward',
					title : '电子派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'dianZiBunko',
					title : '电子输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			
			if (lottery === 'on') {
				totalColumns.push({
					field : 'lotteryTotal',
					title : '彩票下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'lotteryAward',
					title : '彩票派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'lotteryBunko',
					title : '彩票输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sysLotteryTotal',
					title : '其他彩下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sysLotteryAward',
					title : '其他彩派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sysLotteryBunko',
					title : '其他彩输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sfMarkSixTotal',
					title : '系统六合彩下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'sfMarkSixAward',
					title : '系统六合彩派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'sfMarkSixBunko',
					title : '系统六合彩输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if (markSix === 'on') {
				totalColumns.push({
					field : 'markSixTotal',
					title : '六合下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'markSixAward',
					title : '六合派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'markSixBunko',
					title : '六合彩输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			if ('${thirdSports}' === 'on') {
				totalColumns.push({
					field : 'thirdSportsTotal',
					title : '三方体育下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'thirdSportsAward',
					title : '三方体育派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'thirdSportsBunko',
					title : '三方体育输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			
			if ('${thirdLottery}' === 'on') {
				totalColumns.push({
					field : 'thirdLotteryTotal',
					title : '三方彩票下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'thirdLotteryAward',
					title : '三方彩票派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'thirdLotteryBunko',
					title : '三方彩票输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			
			if ('${chess}' === 'on') {
				totalColumns.push({
					field : 'chessTotal',
					title : '棋牌下注总记',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				totalColumns.push({
					field : 'chessAward',
					title : '棋牌派奖总计',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
				bunkoColumns.push({
					field : 'chessBunko',
					title : '棋牌输赢',
					align : 'center',
					valign : 'middle',
					formatter : moneyFormatter
				});
			}
			bunkoColumns.push({
				field : 'allBunko',
				title : '全部输赢总计',
				align : 'center',
				valign : 'middle',
				formatter : moneyFormatter
			});

			$("#datagrid_tb_self").bootstrapTable({
				striped : true,
				columns : totalColumns
			});

			$("#datagrid_tb_other").bootstrapTable({
				striped : true,
				columns : bunkoColumns
			});
		}

		function typeFormatter(value, row, index) {

			return GlobalTypeUtil.getTypeName(1, 1, value);
		}
		function moneyFormatter(value, row, index) {

			if (value === undefined) {
				return "<span class='text-primary'>0.00</span>";
			}

			if (value > 0) {
				return [ '<span class="text-danger">', '</span>' ]
						.join(toDecimal2(value));
			}
			return [ '<span class="text-primary">', '</span>' ]
					.join(toDecimal2(value));

		}

		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}

		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#account").val();
			params['agent'] = $("#agent").val();
			params['begin'] = $("#begin").val();
			params['end'] = $("#end").val();
			params["searchType"] = $("#searchType").val();
			return params
		}

		$(function() {
			getTab();
			bindbtn();
			initData();
		})

		function initData() {
			$.ajax({
				url : "${base}/daili/dlreport/list.do",
				data : queryParams({}),
				success : function(result) {
					data = [];
					data.push(result);
					$("#datagrid_tb_base").bootstrapTable('load', data);
					$("#datagrid_tb_self").bootstrapTable('load', data);
					$("#datagrid_tb_other").bootstrapTable('load', data);
					$("#datagrid_tb_cunqu").bootstrapTable('load', data);
				}
			});
		}

		function search() {
			initData();
		}

		//制保留2位小数，如：2，会在2后面补上00.即2.00 
		function toDecimal2(x) {
			var f = parseFloat(x);
			if (isNaN(f)) {
				return false;
			}
			var f = Math.round(x * 100) / 100;
			var s = f.toString();
			var rs = s.indexOf('.');
			if (rs < 0) {
				rs = s.length;
				s += '.';
			}
			while (s.length <= rs + 2) {
				s += '0';
			}
			return s;
		}

		function reset() {
			var cur = DateUtil.getCurrentDate();
			$("#account").val("");
			$("#agent").val("");
			setDate(cur, cur);
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin, end);
				search();
			});
		}
	</script>
</body>
</html>