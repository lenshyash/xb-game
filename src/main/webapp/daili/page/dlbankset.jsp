<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
<style type="text/css">
td {
	vertical-align: middle !important;
}
</style>
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

	<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li><a href="#">在线取款</a></li>
			<li class="active">银行设置</li>
		</ol>
	</div>

	<div class="container">
		<table class="table table-bordered table-striped" style="clear: both">
			<tbody>
				<tr>
					<td width="15%" class="text-left">＊出款银行名称：</td>
					<td width="35%"><select id="bankName" class="form-control">
							<option value="建设银行">建设银行</option>
							<option value="工商银行" selected="selected">工商银行</option>
							<option value="农业银行">农业银行</option>
							<option value="中国邮政银行">中国邮政银行</option>
							<option value="中国银行">中国银行</option>
							<option value="中国招商银行">中国招商银行</option>
							<option value="中国交通银行">中国交通银行</option>
							<option value="中国民生银行">中国民生银行</option>
							<option value="中信银行">中信银行</option>
							<option value="中国兴业银行">中国兴业银行</option>
							<option value="浦发银行">浦发银行</option>
							<option value="平安银行">平安银行</option>
							<option value="华夏银行">华夏银行</option>
							<option value="广州银行">广州银行</option>
							<option value="BEA东亚银行">BEA东亚银行</option>
							<option value="广州农商银行">广州农商银行</option>
							<option value="顺德农商银行">顺德农商银行</option>
							<option value="北京银行">北京银行</option>
							<option value="杭州银行">杭州银行</option>
							<option value="温州银行">温州银行</option>
							<option value="上海农商银行">上海农商银行</option>
							<option value="中国光大银行">中国光大银行</option>
							<option value="渤海银行">渤海银行</option>
							<option value="浙商银行">浙商银行</option>
							<option value="晋商银行">晋商银行</option>
							<option value="汉口银行">汉口银行</option>
							<option value="上海银行">上海银行</option>
							<option value="广发银行">广发银行</option>
							<option value="深圳发展银行">深圳发展银行</option>
							<option value="东莞银行">东莞银行</option>
							<option value="宁波银行">宁波银行</option>
							<option value="南京银行">南京银行</option>
							<option value="北京农商银行">北京农商银行</option>
							<option value="重庆银行">重庆银行</option>
							<option value="广西农村信用社">广西农村信用社</option>
							<option value="吉林银行">吉林银行</option>
							<option value="江苏银行">江苏银行</option>
							<option value="成都银行">成都银行</option>
							<option value="尧都区农村信用联社">尧都区农村信用联社</option>
							<option value="浙江稠州商业银行">浙江稠州商业银行</option>
							<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
					</select></td>
					<td width="50%" class="text-left" rowspan="6">备注： <br> <br>1.标记有＊者为必填项目。 <br> <br>2.真实姓名须与出款帐户户名一致，否则不予出款。 <br> <br>3.绑定的资料为忘记密码时的认证，请会员务必填写正确资料。
					</td>
				</tr>
				<tr>
					<td width="15%" class="text-left">＊持卡人姓名：</td>
					<td width="35%"><input type="text" class="form-control" id="userName" /></td>
				</tr>
				<tr>
					<td width="15%" class="text-left">＊出款银行帐号：</td>
					<td width="35%"><input type="text" class="form-control" id="cardNo" /></td>
				</tr>
				<tr>
					<td width="15%" class="text-left">＊取款密码：</td>
					<td width="35%"><input type="password" class="form-control" id="repPwd" /></td>
				</tr>
				<tr>
					<td width="50%" class="text-center" colspan="3"><button class="btn btn-success" onclick="submit();">
							<i class="glyphicon glyphicon-ok"></i> 提交
						</button></td>
				</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		function submit() {
			var bankName = $("#bankName").val();
			var province = $("#province").val();
			var city = $("#city").val();
			var bankAddress = $("#bankAddress").val();
			var cardNo = $("#cardNo").val();
			var repPwd = $("#repPwd").val();
			var userName = $("#userName").val();
			if (!bankName) {
				Msg.info("请选择出款银行名称！");
				return;
			}
			if (!userName) {
				Msg.info("请输入持卡人姓名！");
				return;
			}
			if (!cardNo) {
				Msg.info("请输入出款银行帐号！");
				return;
			}
			if (!repPwd) {
				Msg.info("请输入取款密码！");
				return;
			}
			var param = {};
			param["bankName"] = bankName;
			param["userName"] = userName;
			param["province"] = province;
			param["city"] = city;
			param["bankAddress"] = bankAddress;
			param["cardNo"] = cardNo;
			param["repPwd"] = repPwd;
			$.ajax({
				url : "${base}/daili/dldraw/savebank.do",
				data : param,
				success : function(result) {
					checkflag = false;
					window.location.href = "${base}/daili/dldraw/index.do";
				}
			});
		}
	</script>
</body>
</html>