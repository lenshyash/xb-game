<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
		<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">推广链接</li>
		</ol>
	</div>
	
		<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
			<input value="" id="yhType"  type="hidden"/>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="getByType">
						<option selected="selected" value="0">全部</option>
						<option value="2">会员</option>
						<option value="1">代理</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="addLink();">添加推广链接</button>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>
	
	<!-- 修改 -->
	<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">推广链接生成</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
						<input value="" id="zjId"  type="hidden"/>
						<input value="" id="linkType"  type="hidden"/>
							<tr>
								<td width="30%" class="text-right">推广用户类型：</td>
								<td width="55%" class="text-left">
								<select class="form-control" id="tgType">
								<option value="2">会员</option>
								<option value="1" selected="selected">代理</option>
								</select>
								</td>
							</tr>
							<tr>
								<td width="30%" class="text-right">代理机制：</td>
								<td width="55%" class="text-left">
								<input type="text" class="form-control" disabled="disabled" id="dljz" />
								</td>
							</tr>
<!-- 							<tr> -->
<!-- 								<td width="30%" class="text-right">生成链接：</td> -->
<!-- 								<td width="55%" class="text-left"> -->
<!-- 								<input type="text" class="form-control" disabled="disabled" id="urlLink" /> -->
<!-- 								</td> -->
<!-- 							</tr> -->
							<tr id="cpfds">
								<td width="30%" class="text-right">彩票返点数<span class="">(<span id="minRoll">0</span>-<span id="getCpFds"></span>)</span>：</td>
								<td width="55%" class="text-left">
								<input type="text" class="form-control" id="rollPoint" placeholder="请输入返点数!"/>
								<input id="minFd" type="hidden" value="0">
								<input id="maxFd" type="hidden" value="">
								</td>
							</tr>
							<tr id="profitShare_tr">
								<td width="30%" class="text-right">占成数<span class="">(<span id="minPS">0</span>-<span id="maxPS"></span>)</span>：</td>
								<td width="55%" class="text-left">
								<input type="text" class="form-control" id="profitShare" placeholder="请输入占成数!"/>
								</td>
							</tr>
							<tr id="dynamicRate_tr">
								<td width="30%" class="text-right">动态赔率点<span class="">(<span id="minDR">0</span>-<span id="maxDR"></span>)</span>：</td>
								<td width="55%" class="text-left">
								<input type="text" class="form-control" id="dynamicRate" placeholder="请输入赔率点数!"/>
								</td>
							</tr>
							<tr>
								<td width="30%" class="text-right">造成策略：</td>
								<td width="55%" class="text-left">
									<input type="radio" name="generalizeType" checked="checked" value="1"/>随机
									<input type="radio" name="generalizeType" value="2"/>自选
								</td>
							</tr>
							<tr id="code_tr" class="hidden">
								<td width="30%" class="text-right">推广码：</td>
								<td width="55%" class="text-left">
								<input type="text" class="form-control" id="linkCode" placeholder="请输入推广码!"/>
								</td>
							</tr>
							<tr>
								<td width="30%" class="text-right">状态：</td>
								<td width="55%" class="text-left">
								<select id="status" class="form-control">
										<option value="1">禁用</option>
										<option value="2" selected="selected">启用</option>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editor();">保存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			getTab();
			
			getFds();
			
			$("#tgType").change(function() {
				console.log("${profitShare}");
				var curVal = $(this).find("option:checked").attr("value");
				$("#linkType").val(curVal);
				if (curVal == 1 && "${isMulti}" == "true") {
					$('#cpfds').show();
				} else {
					$('#cpfds').hide();
				}
				if(curVal == 1 && '${profitShare}' == 'on'){
					$("#profitShare_tr").show();
				}else{
					$("#profitShare_tr").hide();
				}
				if('${memberRate}' == 'on'){
					$("#dynamicRate_tr").show();
				}else{
					$("#dynamicRate_tr").hide();
				}
			});
			$("input[name='generalizeType']").change(function() {
				if($(this).val() == 2){
					$("#code_tr").removeClass("hidden");
				}else{
					$("#code_tr").addClass("hidden");
				}
				$("#linkCode").val("");
			});
			$("#getByType").change(function() {
				var curVal = $(this).find("option:checked").attr("value");
				$("#yhType").val(curVal);
				$("#datagrid_tb").bootstrapTable('refresh');
			});
		})

		//得到返点数
		function getFds() {
			$.ajax({
				url : "${base}/daili/generalizeLink/multidata.do",
				success : function(j) {
					var rebateMaxNum = j.rebateMaxNum;
					$('#maxFd').val(rebateMaxNum);
					$('#minRoll').html(j.rebateMinNum);
					$('#getCpFds').html(rebateMaxNum);
					$('#minPS').html(j.profitShareMin);
					$('#maxPS').html(j.profitShareMax);
					$('#minDR').html(j.dynamicRateMin);
					$('#maxDR').html(j.dynamicRateMax);
				}
			});
		}

		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/generalizeLink/list.do',
				queryParams : queryParams,
				toolbar : $('#toolbar'),
				columns : [ {
					field : 'userAccount',
					title : '账号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'type',
					title : '类型',
					align : 'center',
					valign : 'middle',
					formatter : typeFormatter
				}, 
				<c:if test="${isMulti}">
				{
					field : 'cpRolling',
					title : '返点',
					align : 'center',
					valign : 'middle'
				}, 
				</c:if>
				<c:if test="${'on' eq profitShare}">
				{
					field : 'profitShare',
					title : '占成',
					align : 'center',
					valign : 'middle'
				}, 
				</c:if>
				<c:if test="${'on' eq memberRate}">
				{
					field : 'dynamicRate',
					title : '赔率点',
					align : 'center',
					valign : 'middle'
				}, 
				</c:if>
				
				{
					field : 'linkKey',
					title : '推广码',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'linkUrl',
					title : '推广链接',
					align : 'center',
					valign : 'middle',
					formatter : pasteFormatter,
					class:'copyBtns'
				}, {
					title : '短链接',
					align : 'center',
					valign : 'middle',
					formatter : shortUrlFormatter,
					class:'copyBtns'
				},{
					title : '加密链接',
					align : 'center',
					valign : 'middle',
					formatter : encodeUrlFormatter,
					class:'copyBtns'
				},
				{
					field : 'status',
					title : '状态',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : statusFormatter
				}, {
					title : '操作',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : operateFormatter
				} ]
				,onLoadSuccess:function(){
						$('.handIco').zclip({
	 	 					path:"${base}/common/js/pasteUtil/ZeroClipboard.swf",
	 	 					copy:function(){
	 	 						return this.id;
	 	 					},
	 	 			    	afterCopy:function(){
	 		 			    	layer.alert('复制成功,请按ctrl+v粘贴:<br/>'+this.id);
	 		 			    }
	 	 				});
				}
			});
		}

		function operateFormatter(value, row, index) {
			return [ '<a class="eidt" href="javascript:void(0)" title="修改">',
					'<i class="glyphicon glyphicon-pencil"></i>', '修改</a>  ',
					'<a class="del" href="javascript:void(0)" title="删除">',
					'<i class="glyphicon glyphicon-remove"></i>', '删除</a>  ' ]
					.join('');
		}

		function typeFormatter(value, row, index) {
			var type = row.type;
			if (type == 1) {
				return '<span class="label label-info">代理</span>';
			} else {
				return '<span class="label label-success">会员</span>';
			}
		}

		function pasteFormatter(value, row, index) {
			return row.linkUrl + '&nbsp;<span class="label label-primary handIco" id="'+ row.linkUrl+'" linkUrl="'+row.linkUrl+'">复制</span>';
		}
		function shortUrlFormatter(value, row, index) {
			var r ="";
			if(row.shortUrl1){
				r+=row.shortUrl1 + '&nbsp;<span class="label label-primary handIco" id="'+ row.shortUrl1+'" linkUrl="'+row.shortUrl1+'">复制</span>';
			}
			if(row.shortUrl2){
				r+="<div style='padding-top:4px'>"+row.shortUrl2 + '&nbsp;<span class="label label-primary handIco" id="'+ row.shortUrl2+'" linkUrl="'+row.shortUrl2+'">复制</span></div>';
			}
			return r;
		}
		function encodeUrlFormatter(value, row, index) {
			var url="${domain}/vote_topic_"+row.linkKey + '.do';
			return url+ '&nbsp;<span class="label label-primary handIco" id="'+ url+'" linkUrl="'+url+'">复制</span>';
		}

		window.operateEvents = {
			'click .eidt' : function(e, value, row, index) {
				$("#editmodel").modal('toggle');
				if(!${isMulti}){
					if(!${mergeRebate}){
						$('#dljz').val('一级代理');
						$("#profitShare_tr").hide();
					}else{
						$('#dljz').val('多级代理');
					}
					$('#cpfds').hide();
				}else{
					$('#dljz').val('多级代理');
					if (row.type == 1 ) {
						$('#cpfds').show();
					} else {
						$('#cpfds').hide();
					}
				}
				
				if(row.type == 1 && "${profitShare}"=="on"){
					$("#profitShare_tr").show();
				}else{
					$("#profitShare_tr").hide();
				}
				if('${memberRate}' == 'on'){
					$("#dynamicRate_tr").show();
				}else{
					$("#dynamicRate_tr").hide();
				}
				$("#zjId").val(row.id);
				$('#urlLink').val(row.linkUrl);
				$('#rollPoint').val(row.cpRolling);
				$('#profitShare').val(row.profitShare);
				$('#status').val(row.status);
				$('#linkCode').val(row.linkKey);
				$('#tgType').val(row.type);
				$('#tgType').attr('onmousedown', 'return false;');

				$("#linkType").val(row.type);
				$('#dynamicRate').val(row.dynamicRate);
			},
			'click .stateOpen' : function(e, value, row, index) {
				closeOrOpen(row);
			},
			'click .stateClose' : function(e, value, row, index) {
				closeOrOpen(row);
			},
			'click .del' : function(e, value, row, index) {
				del(row);
			}
		};

		//添加推广链接地址
		function addLink() {
			$("#editmodel").modal('toggle');
			if(!${isMulti}){
				if(!${mergeRebate}){
					$('#tgType').attr('onmousedown', 'return false;');
					$('#dljz').val('一级代理');
					$('#tgType').val('2');
				}else{
					$('#dljz').val('多级代理');
				}
				$('#cpfds').hide();
				
			}else{
				$('#tgType').attr('onmousedown', 'return true;');
				$('#dljz').val('多级代理');
				$("#tgType").val('1');
				$('#cpfds').show();
			}
// 			$('#urlLink').val(url);
			$('#rollPoint').val('');
			$("input[name=generalizeType][value=1]").prop("checked",true).change();
			$('#profitShare').val('');
			/* if ('${isMulti}' == 'on') {
				$("#tgType").val('1');
			}else{
				$('#tgType').val('2');
			} */
			if('${memberRate}' == 'on'){
				$('#dynamicRate_tr').show();
			}else{
				$('#dynamicRate_tr').hide();
			}
			if ("${profitShare}"=="on" && $("#tgType").val() == 1){
				$("#profitShare_tr").show();
			}else{
				$("#profitShare_tr").hide();
			}
		}

		//保存编辑的推广链接地址
		function editor() {
			var zjId = $('#zjId').val();
			var type = $("#tgType").val();
			var linkCode = $('#linkCode').val();
			var rollPoint = $('#rollPoint').val();
			var profitShare = $('#profitShare').val();
			var dynamicRate = $('#dynamicRate').val();
			var status = $('#status').val();
			// 			console.log(zjId+"zjId  "+"type:"+type+"   linkCode:"+linkCode+"   rollPoint:"+rollPoint+"   status:"+status);
			if ("${isMulti}" == "true") {
				if (type == 1) {
					if (rollPoint == null || rollPoint == "") {
						layer.alert("彩票返点数为必填项!");
						return false;
					}
					var minFd = $('#minFd').val();
					var maxFd = $('#maxFd').val();
					if (rollPoint * 1 < minFd * 1) {
						layer.alert("彩票返点数不能小于" + minFd + "!");
						return false;
					}
					if (rollPoint * 1 > maxFd * 1) {
						layer.alert("彩票返点数不能大于" + maxFd + "!");
						return false;
					}
				}
			}
			
			if($("input[name='generalizeType']:checked").val() == 2){
				if(!$("#linkCode").val() || isNaN($("#linkCode").val()) || $("#linkCode").val().length != 5 ){
					layer.alert("推广码只能填写5位数字!");
					return ;
				}
			}
			
			if ('${profitShare}'=='on' && type == 1) {
				if (!profitShare) {
					layer.alert("占成数为必填项!");
					return false;
				}
				var minPS = $('#minPS').html();
				var maxPS = $('#maxPS').html();
				if (profitShare * 1 < minFd * 1) {
					layer.alert("占成数不能小于" + minPS + "!");
					return false;
				}
				if (profitShare * 1 > maxPS * 1) {
					layer.alert("占成数不能大于" + maxPS + "!");
					return false;
				}
			}
			if ('${memberRate}'=='on') {
				if (!dynamicRate) {
					layer.alert("赔率点为必填项!");
					return false;
				}
				var minDR = $('#minDR').html();
				var maxDR = $('#maxDR').html();
				if (dynamicRate * 1 < minDR * 1) {
					layer.alert("赔率点不能小于" + minDR + "!");
					return false;
				}
				if (dynamicRate * 1 > maxDR * 1) {
					layer.alert("赔率点不能大于" + maxDR + "!");
					return false;
				}
			}
			
			var param = {};
			param["id"] = zjId;
			param["type"] = type;
			param["linkKey"] = linkCode;
			param["cpRolling"] = rollPoint;
			param["profitShare"] = profitShare;
			param["status"] = status;
			param["dynamicRate"] = dynamicRate;
			param["generalizeType"] = $("input[name='generalizeType']:checked").val();
			param["linkCode"] = $("#linkCode").val();
			$.ajax({
				url : "${base}/daili/generalizeLink/updateOrSave.do",
				data : param,
				success : function() {
					layer.alert("保存成功！");
					$("#zjId").val("");
					
// 					var url = "${url}";
// 					url = url.replace(/link998/, randomCode);
// 					$('#linkCode').val(randomCode);
					$("#datagrid_tb").bootstrapTable('refresh');
				}
			});
		}

		//删除
		function del(row) {
			Msg.confirm('确定要把【' + row.linkUrl + '】删除掉？', function() {
				$.ajax({
					url : "${base}/daili/generalizeLink/del.do",
					data : {
						id : row.id
					},
					success : function() {
						layer.alert("删除成功！");
						$("#datagrid_tb").bootstrapTable('refresh');
					}
				});
			});
		}

		function closeOrOpen(row) {
			var state = 1;
			if (row.status == 1) {
				state = 2;
			}
			var bc = {
				"status" : state,
				"id" : row.id
			};
			$.ajax({
				url : "${base}/daili/generalizeLink/updateStatusById.do",
				data : bc,
				success : function(result) {
					if (row.status == 1) {
						tips(row.linkUrl, 2);
					} else {
						tips(row.linkUrl, 3);
					}
					search();
				}
			});
		}

		//设置传入参数
		function queryParams(params) {
			params['stationId'] = $("#stationId").val();
			params['type'] = $("#yhType").val();
			return params
		}

		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}

		function tips(param, index) {
			switch (index) {
			case 1://普通提示框
				layer.alert(param, {
					icon : 0,
					skin : 'layer-ext-moon'
				});
				break;
			case 2://状态开启提示框
				layer.alert('【' + param + '】已被启用!', {
					icon : 1,
					skin : 'layer-ext-moon'
				})
				break;
			case 3://状态关闭提示框
				layer.alert('【' + param + '】已被禁用!', {
					icon : 1,
					skin : 'layer-ext-moon'
				})
				break;
			case 4://状态关闭提示框
				layer.alert(param, {
					icon : 2,
					skin : 'layer-ext-moon'
				})
				break;
			default:
				layer.alert('【参数不正确!】', {
					icon : 1,
					skin : 'layer-ext-moon'
				})
				break;
			}

		}
		
	</script>
</body>
</html>
<style>
.handIco{
	cursor:pointer;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	vertical-align : middle;
}
.form-control1{
	width:60%
}
.copyBtns{position:relative;}
</style>