<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>
		<div class="container" style="margin-top: 20px;">
		<ol class="breadcrumb">
			<li><a href="#">首页</a></li>
			<li class="active">棋牌游戏记录</li>
		</ol>
	</div>
	
	<div class="container">
		<div id="toolbar">
			<div id="search" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" value="${startTime }" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button class="btn btn-default">今日</button>
				<button class="btn btn-default">昨日</button>
				<button class="btn btn-default">本周</button>
				
				<label class="sr-only" for="keyword">账户名称</label> 
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="account"  class="form-control" id="account" value="${account }" placeholder="本地账户名称">
					</div>
				</div>
				<label class="sr-only" for="keyword">三方游戏类型</label> 
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="gameType" id="gameType">
							<option value="">全部类型</option>
							<option value="1">AG</option>
							<option value="2">BBIN</option>
							<option value="3">MG</option>
							<option value="5">ALLBET</option>
							<option value="7">OG</option>
							<option value="8">DS</option>
							<option value="12">KY</option>
							<option value="97">VR</option>
							<option value="98">BG</option>
						</select>
					</div>
				</div>
				<button class="btn btn-primary" onclick="javascript:search();">查询</button>
				
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" class="form-control" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"></table>
	</div>

	<script type="text/javascript">
		$(function() {
			var options = {
				language : 'zh-CN',
				autoclose : true,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);
			var t = DateUtil.formatDate(new Date()),begin = "",end = "";
			<c:if test="${empty param.begin}">begin = t +" 00:00:00";</c:if>
			<c:if test="${empty param.end}">end = t +" 23:59:59";</c:if>
			<c:if test="${not empty param.begin}">begin="${param.begin}";</c:if>
			<c:if test="${not empty param.end}">end="${param.end}";</c:if>
			<c:if test="${not empty param.account}">$('#account').val("${param.account}");</c:if>
			setDate(begin, end);
			bindbtn();
			getTab();
		})
		function getTab() {
			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/rchessbet/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				columns : [ 
				 {
					field : 'platfromType',
					title : '三方游戏类型',
					align : 'center',
					valign : 'bottom'
				},{
					field : 'account',
					title : '注单账户',
					align : 'center',
					valign : 'bottom'
				}, 
				{
					field : 'bettingCode',
					title : '注单号',
					align : 'center',
					valign : 'bottom'
				}, 
				{
					field : 'gameCode',
					title : '游戏局号',
					align : 'center',
					valign : 'bottom'
				}, 
				 {
					field : 'bettingContent',
					title : '投注内容',
					align : 'center',
					valign : 'middle',
					width:100,
					formatter : contentFormatter
				},
				{
					field : 'bettingMoney',
					title : '投注金额',
					align : 'center',
					valign : 'bottom'
				}, 
				{
					field : 'winMoney',
					title : '中奖金额',
					align : 'center',
					valign : 'bottom'
				}, 
				{
					field : 'bettingTime',
					title : '投注时间',
					align : 'center',
					valign : 'bottom',
					formatter : dateFormatter
				}, 
				{
					field : 'createDatetime',
					title : '创建时间',
					align : 'center',
					valign : 'middle',
					formatter : dateFormatter
				}]
			});
		}
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		function contentFormatter(value, row, index) {
			if (value && value.length > 10) {
				return "<div title='"+value+"'>"+value.substring(0, 10) + "...<div>";
			}
			return value;
		}
		//设置传入参数search
		function queryParams(params) {
			params["account"] = $("#account").val();
			params["gameType"] = $("#gameType").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			return params
		}
		
		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
				});
			}
		
		function search() {
			$("#datagrid_tb").bootstrapTable('refresh');
		}
	</script>
</body>
</html>
