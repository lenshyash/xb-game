<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta charset="utf-8">
</head>
<body>
	<jsp:include page="/daili/include/dailimenu.jsp"></jsp:include>

<input type="hidden" id="lottype" value="-1">
	<div class="container">
		<div id="toolbar">
			<div class="form-group">
				<div class="form-inline">
					<div class="input-group">
						<input type="text" class="form-control" value="${begin } 00:00:00" id="begin" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">今日</button>
					<button class="btn btn-default">昨日</button>
					<button class="btn btn-default">本周</button>
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" id="account" placeholder="会员账号">
						</div>
						<div class="input-group">
							<label class="sr-only" for="type">类型</label> 
							<select class="form-control" id="type">
								<option value=" ">全部类型</option>
								<option value="1">等待开奖</option>
								<option value="2">已中奖</option>
								<option value="3">未中奖</option>
								<option value="4">撤单</option>
								<option value="5">派奖回滚成功</option>
								<option value="6">回滚异常的</option>
								<option value="7">开奖异常</option>
								<option value="8">和局</option>
							</select>
						</div>
						<div class="input-group">
							<label class="sr-only" for="czType">彩种类型</label> 
							<select class="form-control" id="czType">
								<option value="-1">全部彩种</option>
							</select>
						</div>
					</div>
					<button class="btn btn-primary CastAutoRefresh" onclick="search();">查询</button>
					<div class="form-group CastAutoRefresh">
						<label>
							<input  type="checkbox">
						</label>
						<label for="name">自动刷新</label>
						<select class="form-control">
							<option>5</option>
							<option>10</option>
							<option>20</option>
							<option>30</option>
							<option>60</option>
							<option>120</option>
						</select>
					</div>
				</div>
				<div class="form-inline" style="margin-top: 5px;">
					<div class="input-group">
						<input type="text" id="end" value="${begin } 23:59:59" class="form-control" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
					</div>
					<button class="btn btn-default">上周</button>
					<button class="btn btn-default">本月</button>
					<button class="btn btn-default">上月</button>
					<div class="input-group">
						<input type="text" class="form-control" id="yxQiHao" placeholder="投注期号">
					</div>
					<div class="input-group">
						<input type="text" class="form-control" id="yxOrder" placeholder="订单号">
					</div>
					<c:if test="${version eq 4}">
						<div class="input-group">
							<label class="sr-only" for="roomId">投注房间</label> 
							<select class="form-control" id="roomId">
								<option value="" selected>全部房间</option>
								<c:forEach items="${roomList}" var="room">
									<option value="${room.id}">${room.name}</option>
								</c:forEach>
							</select>
						</div>
					</c:if>
					<div class="input-group" style="display: none;text-align: center;font-size: 15px;font-weight: bold;" id="buyAccountCountSumMoneny">
						<strong class="input-group" >总计:<span >投注人数:【<span style="color: #46986c" id="buyAccountCount"> 0</span>】</span> <span style="margin-right: 15px">投注金额:【<span style="color: #46986c" id="buySum"> 0</span>】</span></strong>
					</div>
				</div>
			</div>
		</div>
		<table id="datagrid_tb"  data-show-footer="true"></table>

	</div>
	
	<div class="modal fade" id="editmodel"
		tabindex="-1" role="dialog" aria-labelledby="editLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="editLabel">订单详情</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped"
						style="clear: both">
						<tbody>
							<tr>
								<td width="20%" class="text-center" colspan="4">订单号:<span id="dingdh"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">账号:</td>
								<td width="35%" class="text-left"><span id="zhangh"></span></td>
								<td width="20%" class="text-right">单注金额:</td>
								<td width="35%" class="text-left"><span id="danzje"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">下注时间:</td>
								<td width="35%" class="text-left"><span id="xiazsj"></span></td>
								<td width="20%" class="text-right">投注注数:</td>
								<td width="35%" class="text-left"><span id="touzzs"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">彩种:</td>
								<td width="35%" class="text-left"><span id="caiz"></span></td>
								<td width="20%" class="text-right">倍数:</td>
								<td width="35%" class="text-left"><span id="beis"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">期号:</td>
								<td width="35%" class="text-left"><span id="qih"></span></td>
								<td width="20%" class="text-right">投注总额:</td>
								<td width="35%" class="text-left"><span id="touzze"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">玩法:</td>
								<td width="35%" class="text-left"><span id="wanf"></span></td>
								<!-- 如果是第二版则显示赔率 -->
								<td width="20%" class="text-right version1">单注奖金:</td>
								<td width="35%" class="text-left version1"><span id="danzjj"></span></td>
								
								<td width="20%" class="text-right version2 hide">赔率:</td>
								<td width="35%" class="text-left version2 hide"><span id="peilv"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">开奖号码:</td>
								<td width="35%" class="text-left"><span id="kaijhm"></span></td>
								<td width="20%" class="text-right">中奖注数:</td>
								<td width="35%" class="text-left"><span id="zhongjzs"></span></td>
							</tr>
							<tr>
								<td width="20%" class="text-right">状态:</td>
								<td width="35%" class="text-left"><span id="zhuangt"></span></td>
								<td width="20%" class="text-right ">中奖金额:</td>
								<td width="35%" class="text-left"><span id="zhongjje"></span></td>
							</tr>
							
								<c:if test="${isFanShui }">
								<tr>
								<td width="20%" class="text-right">返水金额:</td>
								<td width="35%" class="text-left"><span id="jine"></span></td>
								<td width="20%" class="text-right" id="mutil">返水状态:</td>
								<td width="35%" class="text-left"><span id="fanszt"></span></td>
								</tr>
								</c:if>
								
							
							<tr>
								<td width="20%" class="text-right">盈亏:</td>
								<td width="35%" class="text-left"><span id="yingkui"></span></td>
							</tr>
							<tr>
							<td width="20%" class="text-right" colspan="4">
							<textarea class="form-control" rows="3" id="touzhm"></textarea>
							</td>
								 
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function getTab() {
			var options = {
				language : 'zh-CN',
				autoclose : 1,
			    weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        forceParse: 0,
		        showSecond:1,
		        minuteStep:1,
				format : 'yyyy-mm-dd hh:ii:00'
			};
			$('#begin').datetimepicker(options);
			options.format="yyyy-mm-dd hh:ii:59";
			$('#end').datetimepicker(options);

			window.table = new Game.Table({
				id : 'datagrid_tb',
				url : '${base}/daili/gameRecord/list.do',
				queryParams : queryParams,//参数
				toolbar : $('#toolbar'),
				showPageSummary:true,
				showAllSummary:true,
				columns : [ 
				   {
					field : 'orderId',
					title : '订单号',
					align : 'center',
					valign : 'bottom',
					formatter : orderFormatter
				}, {
					field : 'account',
					title : '投注账号',
					align : 'center',
					valign : 'middle',
					events : operateEvents,
					formatter : accountFormatter
				}, {
					title : '彩种名称',
					align : 'center',
					valign : 'middle',
					formatter : czFormatter
				}, {
					field : 'qiHao',
					title : '期号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'playName',
					title : '玩法名称',
					align : 'center',
					width : '100',
					valign : 'middle'
				}, {
					field : 'haoMa',
					title : '投注号码',
					align : 'center',
					width : '140',
					formatter : wsFormatter
				}, {
					field : 'createTime',
					title : '投注时间',
					width : '150',
					align : 'center',
					formatter : dateFormatter
				}, {
					field : 'buyZhuShu',
					title : '注数',
					align : 'center'
				},{
					field : 'multiple',
					title : '倍数',
					align : 'center'
				},{
					field : 'model',//1元  10角 100分';
					title : '模式',
					width : '50',
					align : 'center',
					formatter : modeFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return "小计:";
					},
					allSummaryFormat:function(rows,aggsData){
						return "总计:";
					}
				},{
					field : 'buyMoney',
					title : '投注金额',
					width : '50',
					align : 'center',
					pageSummaryFormat:function(rows,aggsData){
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.buyMoney != null && row.status < 4){
								r = r+row.buyMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						buyCountSUm(aggsData)
						if(!aggsData){
							return "0.00"
						}
						return aggsData.buySum ? aggsData.buySum.toFixed(2) : "0.00";
					}
				},{
					field : 'betIp',
					title : '投注IP',
					width : '100',
					align : 'center',
					valign : 'middle',
				} ,
				 {
					field : 'winMoney',
					title : '中奖金额',
					width : '50',
					align : 'center',
					pageSummaryFormat:function(rows,aggsData){
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.winMoney != null){
								r = r+row.winMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00"
						}
						return aggsData.winSum ? aggsData.winSum.toFixed(2) : "0.00";
					}
				}
				<c:if test="${version eq 4}">,{
					field : 'roomName',
					title : '房间名称',
					width : '120',
					align : 'center',
					formatter : roomIdFormatter
				}</c:if>
				,{
					field : 'status',
					title : '状态',
					width : '80',
					align : 'center',
					formatter : statusFormatter
				}
			],
				onLoadSuccess:function (data){
					if ($('.no-records-found').length){
						$('#buyAccountCountSumMoneny').css('display','none')
					}
				}
			});
		}
		function buyCountSUm(aggsData){
			$(function (){
				if (aggsData){
					$('#buyAccountCountSumMoneny').css('display','inline-block')
					if('buyAccountCount' in aggsData && aggsData.buyAccountCount != 0){
						$('#buyAccountCount').parents('span').css('display','inline-block')
						$('#buyAccountCount').text(aggsData.buyAccountCount)
					}else{
						$('#buyAccountCount').parents('span').css('display','none')
					}
					if('buySum' in aggsData && aggsData.buySum != 0){
						$('#buySum').parents('span').css('display','inline-block')
						$('#buySum').text(aggsData.buySum)
					}else{
						$('#buySum').parents('span').css('display','none')
					}
					if(!(('buyAccountCount' in aggsData) && ('buySum' in aggsData))){
						$('#buyAccountCountSumMoneny').css('display','none')
					}
				}else {
					$('#buyAccountCountSumMoneny').css('display','none')
				}
			})
		}
		function accountFormatter(value, row, index) {
			return '<a class="detail" href="javascript:void(0)" title="用户详情"><span class="text-danger">'+value+'</span></a>';
		}
		
		window.operateEvents = {
			'click .detail' : function(e, value, row, index) {
				$.get("${base}/daili/dlaccount/view.do?id="+row.accountId,function(html){
					Msg.info(html,"查看账户详细");
				},"html");
			}
		};
		//位数格式化
		function wsFormatter(value, row, index) {
			if(value!=null && value.length>10){
				return  value.substring(0, 10)+"...";
			}else{
				return value;
			}
		}
		
		function roomIdFormatter(value,row,index){
			if(!value){
				return "-";
			}else{
				return value;
			}
		}
		
		function dateFormatter(value, row, index) {
			return DateUtil.formatDatetime(value);
		}
		
		//格式化订单详情		
		function orderFormatter(value, row, index){
			return '<a href="#" onClick="orderDesc(\''+row.orderId+'\',\''+row.account+'\',\''+row.lotCode+'\');">' + row.orderId+ '</a>';
		}
		
		//查看化订单详情		
		function orderDesc(orderId,account,lotCode) {
			$.ajax({
				url : "${base}/daili/gameRecord/getOrderDetailForList.do",
				data : {
					"orderId" : orderId,
					"account" : account,
					"lotCode" : lotCode
				},
				success:function(j){
					$("#editmodel").modal('toggle');
		 			$('#dingdh').html(j.orderId);
		 			$('#zhangh').html(j.account);
		 			$('#sellingTime').html(DateUtil.formatDatetime(j.sellingTime));
		 			$('#sealTime').html(DateUtil.formatDatetime(j.sealTime));
		 			$('#xiazsj').html(DateUtil.formatDatetime(j.createTime));
		 			$('#touzzs').html(j.buyZhuShu);
		 			$('#caiz').html(cz(j.lotCode));
		 			$('#beis').html(j.multiple);
		 			$('#qih').html(j.qiHao);
		 			if(j.buyMoney != null){
		 				$('#touzze').html(fmoney(j.buyMoney,2));
		 			}
		 			if(j.peilv){
		 				$('#peilv').html(j.peilv);
		 				$('.version1').addClass("hide");
		 				$('.version2').removeClass("hide");
		 				if(j.playCode.indexOf("zuxuansan")>=0 || j.playCode.indexOf("zuxuanliu") >=0){
		 					$("#danzje").html("- -");
		 				}else{
		 					$('#danzje').html(fmoney(j.buyMoney,2)/j.buyZhuShu);
		 				}
		 			}else{
		 				$('.version2').addClass("hide");
		 				$('.version1').removeClass("hide");
		 				if(j.playCode.indexOf("zuxuansan")>=0 || j.playCode.indexOf("zuxuanliu") >=0){
		 					$("#danzje").html("- -");
		 				}else{
		 					$('#danzje').html(fmoney(2/j.model,2));
		 				}
		 			}
		 			
		 			if(j.groupName){
		 				$('#wanf').html(j.groupName+'--'+j.playName);
		 			}else{
		 				$('#wanf').html(j.playName);
		 			}
		 			if(j.minBonusOdds != null){
		 				$('#danzjj').html(fmoney(j.minBonusOdds,2));
		 			}
		 			
	 				if(j.winZhuShu){
		 				$('#zhongjzs').html(j.winZhuShu);
		 			}else{
		 				$('#zhongjzs').html("0");
		 			}
		 			
		 			$('#kaijhm').html(j.lotteryHaoMa||"");
		 			if(j.rollBackMoney != null){
		 				$('#jine').html(fmoney(j.rollBackMoney,2));
		 			}
		 			$('#zhuangt').html(kjzt(j.status));
		 			
		 			$('#touzhm').html(j.haoMa);
		 			
		 			var yk = '0.00';
		 			var wm = j.winMoney;
		 			var rb = j.rollBackMoney;
		 			var bm = j.buyMoney;
		 			if(wm && wm*1 >=0){
		 				yk = yk*1 + wm*1;
		 			}
		 			if(rb && rb*1 >=0){
		 				yk = yk*1 + rb*1;
		 			}
		 			if(bm && bm*1 >=0){
		 				yk = yk*1 - bm*1;
		 			}
		 			if(j.status == 8){
		 				$('#yingkui').html('0.00');
		 				$('#zhongjje').html(fmoney(j.buyMoney*1,2));
		 			}else{
		 				$('#yingkui').html(fmoney(yk*1,2));
			 			if(j.winMoney){
			 				$('#zhongjje').html(fmoney(j.winMoney,2));
			 			}else{
			 				$('#zhongjje').html('0.00');
			 			}
		 			}
		 			
		 			
		 			if("${isMulti}"){
		 				$('#mutil').html("返点状态:");
		 				$('#fanszt').html(mutilFormatter(j.rollBackStatus,1));
		 			}else{
		 				$('#mutil').html("返水状态:");
		 				$('#fanszt').html(mutilFormatter(j.rollBackStatus,2));
		 			}

				}
			});
		}
		
		function mutilFormatter(value,type){
			var col = '';
			if(type == 1){
				col = "返点";
			}else{
				col = "返水";
			}
			switch(value){
			case 1:
			case 2:
				col = "还未"+col;
				break;
			case 3:
				col = col + "已经回滚"
				break;
			case 4:
				col = "已经"+col;
				break;
			}
			return col;
		}
		
		//格式化彩种
		function czFormatter(value, row, index) {
			return cz(row.lotCode);
		}
		
		function cz(obj){
			var lotName = "";
		    $("#czType option").each(function(){ 
		        var txt = $(this).val(); 
		        if(txt==obj){
		        	lotName = $("#czType").find('option[value='+obj+']').text();
		        }
		    });
			return lotName;
		    
			//下面用不到了
			var lotName = '';
			switch(obj){
			case 'CQSSC':
				lotName = "重庆时时彩";
				break;
			case 'PL3':
				lotName = "排列三";
				break;
			case 'SH11X5':
				lotName = "上海11选5";
				break;
			case 'GX11X5':
				lotName = "广西11选5";
				break;
			case 'FC3D':
				lotName = "福彩3D";
				break;
			case 'BJSC':
				lotName = "北京赛车";
				break;
			case 'EFC':
				lotName = "二分彩";
				break;
			case 'WFC':
				lotName = "五分彩";
				break;
			case 'HKWFC':
				lotName = "香港五分彩";
				break;
			case 'AMWFC':
				lotName = "澳门五分彩";
				break;
			case 'SFC':
				lotName = "十分彩";
				break;
			case 'ESFC':
				lotName = "二十分彩";
				break;
			case 'XJSSC':
				lotName = "新疆时时彩";
				break;
			case 'PCEGG':
				lotName = "PC蛋蛋";
				break;
			case 'JX11X5':
				lotName = "江西11选5";
				break;
			case 'GD11X5':
				lotName = "广东11选5";
				break;
			case 'SD11X5':
				lotName = "山东11选5";
				break;
			case 'TJSSC':
				lotName = "天津时时彩";
				break;
			case 'FFC':
				lotName = "分分彩";
				break;
			case 'HNKLSF':
				lotName = "湖南快乐十分";
				break;
			case 'GDKLSF':
				lotName = "广东快乐十分";
				break;
			case 'JSSB3':
				lotName = "江苏骰宝(快3)";
				break;
			case 'XYFT':
				lotName = "幸运飞艇";
				break;
			case 'LXYFT':
				lotName = "老幸运飞艇";
				break;
			case 'AZXY10':
				lotName = "澳洲幸运10";
				break;
			case 'CQXYNC':
				lotName = "重庆幸运农场";
				break;
			case 'AHK3':
				lotName = "安徽快三";
				break;
			case 'HBK3':
				lotName = "湖北快三";
				break;
			case 'SHHK3':
				lotName = "上海快三";
				break;
			case 'HEBK3':
				lotName = "河北快三";
				break;
			case 'GXK3':
				lotName = "广西快三";
				break;
			case 'GZK3':
				lotName = "贵州快三";
				break;
			case 'SFSC':
				lotName = "极速赛车";
				break;
			case 'XSFSC':
				lotName = "新极速赛车";
				break;
			case 'XWFSC':
				lotName = "幸运赛车";
				break;
			case 'SFFT':
				lotName = "极速飞艇";
				break;
			case 'WFSC':
				lotName = "五分赛车";
				break;
			case 'WFFT':
				lotName = "五分飞艇";
				break;
			case 'LBJSC':
				lotName = "老北京赛车";
				break;
			case 'JND28':
				lotName = "加拿大28";
				break;
			case 'FFK3':
				lotName = "极速快三";
				break;
			case 'WFK3':
				lotName = "幸运快三";
				break;
			case 'JPK3':
				lotName = "日本快三";
				break;
			case 'KRK3':
				lotName = "韩国快三";
				break;
			case 'HKK3':
				lotName = "香港快三";
				break;
			case 'AMK3':
				lotName = "澳门快三";
				break;
			case 'SFK3':
				lotName = "十分快三";
				break;
			case 'ESK3':
				lotName = "二十分快三";
				break;
			case 'BJK3':
				lotName = "北京快三";
				break;
			case 'GSK3':
				lotName = "甘肃快三";
				break;
			case 'JXK3':
				lotName = "江西快三";
				break;
			case 'FFSC':
				lotName = "疯狂赛车";
				break;
			case 'SF28':
				lotName = "极速28";
				break;
			case 'TXFFC':
				lotName = "腾讯分分彩";
				break;
			case 'HNFFC':
				lotName = "河内分分彩";
				break;
			case 'HNWFC':
				lotName = "河内五分彩";
				break;
			case 'AZXY5':
				lotName = "澳洲幸运5";
				break;
			case 'TMK3':
				lotName = "三分快三";
				break;
			case 'WFLHC':
				lotName = "五分六合彩";
				break;
			case 'HKMHLHC':
				lotName = "香港马会六合彩";
				break;
			case 'SFLHC':
				lotName = "十分六合彩";
				break;
			case 'TMLHC':
				lotName = "三分六合彩";
				break;
			case 'FF11X5':
				lotName = "极速11选5";
				break;
			case 'FF3D':
				lotName = "极速3D";
				break;
			case 'SF11X5':
				lotName = "三分11选5";
				break;
			case 'WF11X5':
				lotName = "五分11选5";
				break;
			case 'WF3D':
				lotName = "五分3D";
				break;
			case 'SFKLSF':
				lotName = "三分快乐十分";
				break;
			case 'WFKLSF':
				lotName = "五分快乐十分";
				break;
			}
			return lotName;
		}
		
		//格式化成两位小数
		function fmoney(s, n) { 
			n = n > 0 && n <= 20 ? n : 2; 
			s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
			var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
			t = ""; 
			for (i = 0; i < l.length; i++) { 
			t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "" : ""); 
			} 
			return t.split("").reverse().join("") + "." + r; 
			} 
		
		function modeFormatter(value, row, index) {
			var col = '';
			switch(value){
			case 1:
				col = '元';
				break;
			case 10:
				col = '角';
				break;
			case 100:
				col = '分';
				break;
			}
			return col;
		}
		
		function statusFormatter(value, row, index) {
			return kjzt(row.status);
		}
		
		function kjzt(obj){
			var col = '';
			switch(obj){
			case 1:
				col = '<span class="label label-primary" >等待开奖</span>';
				break;
			case 2:
				col = '<span class="label label-success" >已中奖</span>';
				break;
			case 3:
				col = '<span class="label label-danger" >未中奖</span>';
				break;
			case 4:
				col = '<span class="label label-info" >撤单</span>';
				break;
			case 5:
				col = '<span class="label label-success">派奖回滚成功</span>';
				break;
			case 6:
				col = '<span class="label label-warning">回滚异常的</span>';
				break;
			case 7:
				col = '<span class="label label-warning">开奖异常</span>';
				break;
			case 8:
				col = '<span class="label label-success">和局</span>';
				break;
			case 10:
				col = '<span class="label label-success">和局中奖</span>';
				break;
			}
			
			return col;
		}
		
		//设置传入参数
		function queryParams(params) {
			params['account'] = $("#account").val();
			params['startTime'] = $("#begin").val();
			params['endTime'] = $("#end").val();
			params['status'] = $("#type").val();
			params['qihao'] = $("#yxQiHao").val();
			params['orderCode'] = $("#yxOrder").val();
			params['account'] = $("#account").val();
			params['roomId'] = $("#roomId").val();
			var code = $("#czType").val();
			if(code.indexOf('-1')==-1){
				params['code'] = code;
			}
			return params
		}
		$(function() {
			czGroup();
			bindbtn();
			getTab();
		})
		
			function czGroup(){
			$.ajax({
				url:"${base}/daili/gameRecord/czGroup.do",
				data:"GET",
				DataType:"json",
				success:function(j){
					var col = '<option value="-1" selected="selected">全部彩种</option>';
					for(var i in j){
						if(j[i].code!='LHC'){
							col+='<option value="'+j[i].code+'" id="'+j[i].code+'">'+j[i].name+'</option>';
						}
					}
					$('#czType').html(col);
				}
					
			});
		}
		
		function search() {
			$('.zhtj').val('0');
			$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
		}

		function setDate(begin, end) {
			$('#begin').val(begin);
			$('#end').val(end);
		}

		function bindbtn() {
			$(".form-inline .btn-default").click(function() {
				var type = $(this).html();
				var begin = "";
				var end = "";
				if ('今日' === type) {
					begin = DateUtil.getCurrentDate();;
					end = begin;
				} else if ('昨日' === type) {
					begin = DateUtil.getLastDate();
					end = begin;
				} else if ('本周' === type) {
					begin = DateUtil.getWeekStartDate();
					end = DateUtil.getCurrentDate();
				} else if ('上周' === type) {
					begin = DateUtil.getLastWeekStartDate();
					end = DateUtil.getLastWeekEndDate();
				} else if ('本月' === type) {
					begin = DateUtil.getMonthDate();
					end = DateUtil.getCurrentDate();
				} else if ('上月' === type) {
					begin = DateUtil.getLastMonthStartDate();
					end = DateUtil.getLastMonthEndDate();
				}
				$('.zhtj').val('0');
				setDate(begin+" 00:00:00", end+" 23:59:59");
				search();
			});
		}

		$(function(){
			var  times=''
			var time =''
			$('.CastAutoRefresh input').change(function(){
				if ($('.CastAutoRefresh input').is(":checked")){
					// debugger
					autoRefreshs(time,false)
					time =$('.CastAutoRefresh option:selected').text()
					autoRefreshs(time,true)
				}else{
					autoRefreshs(time,false)
				}
			})

			$('.CastAutoRefresh select').change(function(){
				if ($('.CastAutoRefresh input').is(":checked")){
					// debugger
					autoRefreshs(time,false)
					time =$('.CastAutoRefresh option:selected').text()
					autoRefreshs(time,true)
				}else {
					autoRefreshs(time,false)
				}
			})

			function autoRefreshs(time,statu){
				if (statu == true){
					times = setInterval(function(){
						search();
					},time *1000 )
				}else {
					clearInterval(times)
				}

			}
		})
	</script>
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	vertical-align : middle;
}
</style>
</body>
</html>