<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/include/base.jsp"></jsp:include>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title}</title>
</head>
<body>
	<jsp:include page="/daili/include/bootstrap.jsp"></jsp:include>
	
	
<div class="modal show" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="top:80px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title text-center text-primary" id="gridSystemModalLabel" >代理登录</h1>
      </div>
      <div class="modal-body">
	          <div class="form-group">
	            <input type="text" id="username" class="form-control input-lg" placeholder="登陆账号" required autofocus>
	          </div>
	          <div class="form-group">
	            <input type="password" id="password" class="form-control input-lg" placeholder="登录密码" required>
	          </div>
	          <div class="form-inline">
	            <input type="text" id="verifyCode" class="form-control input-lg" placeholder="验证码" required>
	            <img id="verifycodeImg" style="cursor: pointer;width: 21%;margin-top: -3px;height: 30%;" alt="验证码" src="${base }/daili/verifycode.do" onclick="refreshVerifyCode(this)">
	          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-lg btn-block" onclick='doLogin();'>立刻登录</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
	
	<script language="javascript">
		$(function() {
			$("#verifyCode").keyup(function(event){
			  if(event.keyCode ==13){
				  doLogin();
			  }
			});
			$("#password").keyup(function(event){
			  if(event.keyCode ==13){
				  if(!$("#verifyCode").val()){
					  $("#verifyCode").focus();
					  return;
				  }
				  doLogin();
			  }
			});
		});
		function refreshVerifyCode() {
			var $img = $("#verifycodeImg");
			var time = (new Date()).getTime();
			$img.attr("src","${base}/daili/verifycode.do?time=" + time);
		}
		function doLogin() {

			var account = $("#username").val().trim();
			var pwd = $("#password").val();
			if (account == '') {
				Msg.info("请输入账号");
				$("#username").focus();
				return;
			}
			if (pwd == '') {
				Msg.info("请输入密码");
				$("#password").focus();
				return;
			}
			$ajax({
				url : "${base}/daili/login.do",
				data : {
					account : account,
					password : pwd,
					verifyCode : $("#verifyCode").val()
				},
				success : function(data, textStatus, xhr) {
					var ceipstate = xhr.getResponseHeader("ceipstate")
					if (!ceipstate || ceipstate == 1) {// 正常响应
						window.location.href = "${base}/daili";
					} else if (ceipstate == 2) {// 后台异常
						Msg.error("后台异常，请联系管理员!");
						refreshVerifyCode();
					} else if (ceipstate == 3) { // 业务异常
						refreshVerifyCode();
						Msg.info(data.msg);
					}
				}
			});
		}
		
		function showCardWin(points){
			$("#cardModel").modal('show');
			$form = $("#cardForm");
			$form[0].reset();
			$form.find(".ccode").each(function(index,element){
				this.innerHTML = points[index];
			});
		}
		
		
		function login4Card(){
			
			$form = $("#cardForm");
			var code = "";
			$form.find("input[name=p]").each(function(index,element){
				var v = $.trim(this.value);
				code += v;
			});
			
			if(code.length != 3){
				Msg.info("请输入密保卡");
				return;
			}
			
			$ajax({
				url : "${base}/daili/login4Card.do",
				data : {
					code : code
				},
				success : function(data, textStatus, xhr) {
					var ceipstate = xhr.getResponseHeader("ceipstate")
					if (!ceipstate || ceipstate == 1) {// 正常响应
						window.location.href = "${base}/daili";
					} else if (ceipstate == 2) {// 后台异常
						Msg.error("后台异常，请联系管理员!");
					} else if (ceipstate == 3) { // 业务异常
						Msg.info(data.msg);
					}
				}
			});
		}
		
	</script>
</body>
</html>