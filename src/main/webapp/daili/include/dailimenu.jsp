<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="dailiheadmenu"></div>
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="logoutLabel">提示</h4>
			</div>
			<div class="modal-body">确定退出登录吗？</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="doLogout();">确定</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="head_editpwd" tabindex="-1" role="dialog" aria-labelledby="head_editpwdLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="head_editpwdLabel">修改密码</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
						<tr>
							<td width="20%" class="text-right">用户名：</td>
							<td width="80%" class="text-left"><input type="text" class="form-control" value="${loginUser.account}" id="head_pwdat" disabled="disabled" /></td>
						</tr>
						<tr>
							<td width="20%" class="text-right">密码类型：</td>
							<td width="80%" class="text-left"><select class="form-control" id="ohead_pwd_type"><option value="1">登录密码</option>
									<option value="2">提款密码</option></select></td>
						</tr>
						<tr>
							<td width="20%" class="text-right">旧密码：</td>
							<td width="80%" class="text-left"><input type="password" class="form-control" id="ohead_pwd" /></td>
						</tr>
						<tr>
							<td width="20%" class="text-right">新密码：</td>
							<td width="80%" class="text-left"><input type="password" class="form-control" id="head_pwd" /></td>
						</tr>
						<tr>
							<td width="20%" class="text-right">确认密码：</td>
							<td width="80%" class="text-left"><input type="password" class="form-control" id="rhead_pwd" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doUpdpwd();">保存</button>
			</div>
		</div>
	</div>
</div>
<jsp:include page="bootstrap.jsp"></jsp:include>
<jsp:include page="dailistyle.jsp"></jsp:include>

<nav class="navbar navbar-default navbar-fixed-top" style="position: relative;">
	<div class="container">
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="${base }/daili/dlaccount/index.do">下级列表</a></li>
				<%-- 				<li><a href="${base }/daili/gameRecord/index.do">游戏记录</a></li> --%>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-fire"
						aria-hidden="true"></span> 游戏记录 <span class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<c:if test="${ isLotteryRecordOpened}">
							<li class=""><a href="${base }/daili/gameRecord/index.do">彩票投注记录</a></li>
						</c:if>
						<c:if test="${ isSixRecordOpened}">
							<li><a href="${base}/daili/gameRecord/lhcIndex.do">六合投注记录</a></li>
						</c:if>
						<c:if test="${isRealRecordOpened }">
							<li><a href="${base}/daili/rbet/index.do">真人投注记录</a></li>
						</c:if>
						<c:if test="${ isElectronicRecordOpened}">
							<li><a href="${base}/daili/rebet/index.do">电子投注记录</a></li>
						</c:if>
						<c:if test="${ isSportRecordOpened}">
							<li><a href="${base}/daili/sportRecord/index.do">体育投注记录</a></li>
						</c:if>
						<c:if test="${ isRealSportOpened}">
							<li><a href="${base}/daili/rsbet/index.do">三方体育记录</a></li>
						</c:if>
						<c:if test="${ isThirdLotteryOpened}">
							<li><a href="${base}/daili/rlotbet/index.do">三方彩票记录</a></li>
						</c:if>
						<c:if test="${ isChessOpened}">
							<li><a href="${base}/daili/rchessbet/index.do">棋牌记录</a></li>
						</c:if>
						<c:if test="${ isDjOpened}">
							<li><a href="${base}/daili/esportsbet/index.do">电竞记录</a></li>
						</c:if>
					</ul></li>
				<li><a href="${base}/daili/dlmnyrd/index.do">团队账变</a></li>
				<li><a href="${base}/daili/dlreport/index.do">团队统计</a></li>
				<li><a href="${base}/daili/dlstatistic/index.do">团队报表</a></li>
				<c:if test="${loginUser.accountType eq 4}">
					<li><a href="${base}/daili/generalizeLink/index.do">推广链接</a></li>
				</c:if>
				<li><a href="${base}/daili/dlfh/index.do">业绩提成</a></li>
				
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					</span> 日志记录 <span class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<c:if test="${isMemberDepositOpened}">
							<li><a href="${base}/daili/deposit/index.do">存款日志</a></li>
						</c:if>
						<li><a href="${base}/daili/dldrawrd/index.do">取款日志</a></li>
						<li><a href="${base}/daili/dlloginlog/index.do">登录日志</a></li>
				</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li id="message_li"><a href="${base}/daili/dlmessage/index.do">站内信 <span class="badge" id="message_span">${messageCountNav}</span></a></li>
				<li id="online_li"><a href="${base}/daili/dlaccount/index.do?online=2&searchType=2&accountType=1">在线 <span class="badge" id="online_span">${onlineCountNav}</span></a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">${loginUser.account}<span class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<li><a href="${base }/daili/perinfopg.do">账号信息</a></li>
						<li><a href="${base }/daili/dldraw/index.do">在线取款</a></li>
						<li><a href="#" data-toggle="modal" data-target="#head_editpwd">修改密码</a></li>
						<li><a href="#" data-toggle="modal" data-target="#logout">退出</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>
<div style="height: 52px;">&nbsp;</div>
<script type="text/javascript">
	$(function() {
		$("#navbar > ul li").hover(function() {
			$(this).addClass('open');
		}, function() {
			$(this).removeClass('open');
		});
	});
	function doLogout() {
		window.location.href = "${base}/daili/logout.do";
	}

	function doUpdpwd() {
		var opassword = $("#ohead_pwd").val();
		var password = $("#head_pwd").val();
		var rpassword = $("#rhead_pwd").val();

		if (!opassword) {
			Msg.info("旧密码不能为空！");
			return;
		}

		if (!password) {
			Msg.info("新密码不能为空！");
			return;
		}
		if (!rpassword) {
			Msg.info("确认密码不能为空！");
			return;
		}

		if (password !== rpassword) {
			Msg.info("两次密码不一致！");
			return;
		}
		$.ajax({
			url : "${base}/daili/updloginpwd.do",
			data : {
				pwd : password,
				rpwd : rpassword,
				opwd : opassword,
				pwdType : $("#ohead_pwd_type").val()
			},
			success : function(result) {
				if ($("#ohead_pwd_type").val() == 1) {
					Msg.confirm('修改成功,请重新登录', {
						btn : [ '确定' ]
					}, function() {
						window.location.href = "${base}/daili/logout.do";
					});
				} else {
					Msg.info("修改成功！");
				}
			}
		});
	}
</script>