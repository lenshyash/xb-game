<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${_title }</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
    <!--<script src="./js/zepto.js"></script>-->
</head>
<body class="flexWrap" style="background: rgb(248, 249, 251)">
<div id="login" class="box p-r">
    <div class="flexWrap">
        <div class="top flex">
            <div class="d-i flex1">
                <a href="${m }/index.do" class="left-white"></a>
            </div>
            <c:if test="${testAccount eq 'of' }">
	            <div class="loginBefore">
	                <a class="p-l-r-15 p-b-t-5 c-w b-1 b-r-35 f-s-14" href="javascript:void(0)" onclick="location.href='${m}/swregpage.do'">试玩</a>
	            </div>
            </c:if>
        </div>
        <div style="height: 1rem"></div>
        <div class="m-b-15 m-10 b-r-6 c-6 p-b-t-5 ">
            <div class="weui-tab">
                <div class="weui-navbar">
                    <a class="weui-navbar__item weui-bar__item--on f-s-20 f-w-900" href="#tab1">
                        用户登录
                    </a>
                    <a class="weui-navbar__item f-s-20 f-w-900" href="${m }/regpage.do">
                        免费注册
                    </a>
                </div>
                <div class="weui-tab__bd">
                    <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active p-l-r-30 p-b-t-15">
                        <div class="weui-cells" >
                            <div class="weui-cell m-b-15">
                                <div class="weui-cell__bd">
                                    <p>欢迎来到迪士尼彩乐园，我们更专注彩票，博彩乐趣都在这里。</p>
                                </div>
                            </div>
                            <div class="weui-cell">
                                <div class="weui-cell__bd">
                                    <label class="username"></label>
                                    <input type="text" name="username" placeholder="用户昵称" class="c-6">
                                </div>
                            </div>
                            <div class="weui-cell p-r">
                                <div class="weui-cell__bd">
                                    <label class="password"></label>
                                    <input type="text" name="password" placeholder="您的密码" class="c-6">
                                    <a href="##" class="forget-password">忘记密码</a>
                                </div>
                            </div>
                            <div class="weui-cell">
                                <div class="weui-cell__bd p-r">
                                    <label class="verify"></label>
                                    <input type="text" name="verify" placeholder="验证码" class="c-6">
                                    <img src="image/yzm.jpg" class="verify-code">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn">登录</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-c b-t p-b-t-10 p-f w-b100" style="bottom: 0;left: 0">
            <span class="c-w p-l-r-20 p-b-t-5 b-r-6 f-s-14 " style="background: rgb(136, 136, 136)">桌面版</span>
        </div>
        <div class="h-70"></div>
    </div>
</div>

<!-- body 最后 -->

<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
</html>
<style>
    .btn::before {
        content: "";
        width: 100%;
        height: 100%;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        position: absolute;
        z-index: -1;
        background: linear-gradient(to right, rgb(125, 202, 255), rgb(26, 81, 148));
    }
    .btn::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 150%;
        top: 20%;
        left: 0px;
        z-index: -1;
        opacity: 0.5;
        border-radius: 50%;
        background: linear-gradient(91deg, rgb(154, 232, 255), rgb(0, 114, 211));
    }
    .verify-code{
        position: absolute;
        top: 8px;
        right: 0px;
        width: 70px;
    }
    .forget-password{
        position: absolute;
        top: 11px;
        right: 0px;
        color: rgb(4, 186, 238);
    }
    #tab1 ::-webkit-input-placeholder{
        color: #c5c5c5;
        font-weight: 700;
    }
    #tab1 :-moz-placeholder{
        color: #c5c5c5;
        font-weight: 700;
    }
    #tab1 ::-moz-placeholder{
        color: #c5c5c5;
        font-weight: 700;
    }
    #tab1 :-ms-input-placeholder{
        color: #c5c5c5;
        font-weight: 700;
    }
    .weui-cell__bd{
        height: 100%;
    }
    .username{
        background: url("./image/user.svg");
    }
    .password{
        background: url("./image/eye.png") center center / contain no-repeat;;
    }
    .verify{
        background: url("./image/save.svg");
    }
    .weui-tab__bd label {
        position: absolute;
        font-size: 0px;
        width: 20px;
        height: 20px;
        left: 0px;
        top: 50%;
        transform: translateY(-50%);
    }
    .weui-tab__bd label {
        position: absolute;
        font-size: 0px;
        width: 20px;
        height: 20px;
        left: 0px;
        top: 50%;
        transform: translateY(-50%);
    }
    .weui-tab__bd input {
        width: 100%;
        height: 100%;
        text-align: left;
        box-sizing: border-box;
        padding-left: 45px;
        background-color: transparent;
        font-size: 0.75rem;
        outline: none;
        border-width: 0px;
        border-style: initial;
        border-color: initial;
        border-image: initial;
    }
    .weui-cells:before{
        border: none;
    }
    .weui-cells{
        margin-top: 0;
        font-size: 14px;
    }
    .weui-cell{
        padding: 0;
        height: 40px;
        width: 100%;
    }
    .weui-tab__bd .weui-tab__bd-item {
        background: #fff;
        border-radius: 8px;
    }

    .weui-navbar__item.weui-bar__item--on {
        background: none;
        border-bottom: 3px solid #fff;
    }

    .top {
        padding: 1em 1em 0;
        color: #fff;
    }

    .game-list {
        box-shadow: rgba(0, 0, 0, 0.08) 0px 3px 6px 0px;
    }

    .game-box {
        box-shadow: rgba(0, 0, 0, 0.08) 0px 3px 6px 1px;
    }

    .box {
        height: 100%;
    }

    .box::before {
        content: "";
        position: absolute;
        top: 0px;
        right: 0px;
        left: 0px;
        width: 100%;
        height: 30%;
        background-image: url('./image/Banner.jpg'), linear-gradient(to right, rgb(125, 202, 255) 0%, rgb(26, 81, 148) 100%);
        background-size: cover;
        z-index: -1;
    }

    .weui-navbar + .weui-tab__bd {
        padding-top: 49px;
        background: #fff;
    }

    .dDuZFA .game-icon-played {
        position: absolute;
        top: -2px;
        right: 4px;
        width: 25px;
        height: 25px;
        background: url('./image/hot.svg') no-repeat;
    }

    .dDuZFA {
        position: relative;
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 6px 0px;
        padding-bottom: 5px;
        border-radius: 8px;
    }
</style>