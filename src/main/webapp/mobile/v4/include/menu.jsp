 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <div id="menu" class="w-b100 h-b100 p-f" style="background-color: rgba(0,0,0,.7);display: none">
        <div class="w-b70 bg-w h-b100 flexWrap">
            <div class="account">
                <div class="profile"></div>
                <div class="username">${loginMember.account }</div>
                <div class="balance">${loginMember.money }</div>
            </div>
            <div class="weui-cells flexContent" style="margin: 0">
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item home"></div><a href="${m }/bet_lotterys.do?lotCode=BJSC"  class="f-s-16 c-black">游戏大厅</a>
                    </div>
                </div>
              <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item report"></div><a href="${m }/report.do"  class="f-s-16 c-black">未结明细</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item todayreport"></div><a href="${m }/report.do"  class="f-s-16 c-black">今天已结</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item history"></div><a href="${m }/betting_record_lottery.do"  class="f-s-16 c-black">报表查询</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item openlottery"></div><a href="${m}/draw_notice.do"  class="f-s-16 c-black">开奖结果</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item dresult"></div><a href="http://1681391.com/" target="_blank"  class="f-s-16 c-black">全国开奖网</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item rule"></div><a href="${m }/lottery_rules.do?lotCode=BJSC"  class="f-s-16 c-black">游戏规则</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item chat"></div><span class="f-s-16">聊天室</span>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item join"></div><a href="${m }/rookieHelp.do"  class="f-s-16 c-black">加盟合作</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item promo"></div><a href="${m }/active.do"  class="f-s-16 c-black">优惠活动</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item share"></div><a href="${m }/appDownload.do"  class="f-s-16 c-black">下载APP</a>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item server"></div><a href="${kfUrl }"  class="f-s-16 c-black">在线客服</a>
                    </div>
                </div>
              <!--   <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item phone"></div><span class="f-s-16">手机设置</span>
                    </div>
                </div> 
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item download"></div><span class="f-s-16">下载APP</span>
                    </div>
                </div>-->
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="icon-item quit"></div><span class="f-s-16">退出</span>
                    </div>
                </div>
            </div>
        </div>
    </div>