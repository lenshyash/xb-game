<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="lhcTpl">
            <div class="">
			{{each data.list as value i}}
                <div class="row-60 flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[0]}}">{{value.haoMaList[0]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[0]}}</p>
                                </div>
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[1]}}">{{value.haoMaList[1]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[1]}}</p>
                                </div>
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[2]}}">{{value.haoMaList[2]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[2]}}</p>
                                </div>
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[3]}}">{{value.haoMaList[3]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[3]}}</p>
                                </div>
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[4]}}">{{value.haoMaList[4]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[4]}}</p>
                                </div>
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[5]}}">{{value.haoMaList[5]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[5]}}</p>
                                </div>
                            </div>
                            <div class="d-i v-t l-h-20">
                                <div class="nomalBall">+</div>
                                <!--<p class="" style="height: 24px"></p>-->
                            </div>
                            <div class="flex1">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[6]}}">{{value.haoMaList[6]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[6]}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="f-s-18 f-w-900 c-r">{{$gyhSum value.haoMaList 3}}</span></div>
                            <div class="{{$gyhDs value.haoMaList 3 4}}"><span class="">{{$gyhDs value.haoMaList 3 1}}</span></div>
                            <div class="{{$gyhDx value.haoMaList 175 4 3}}"><span class="">{{$gyhDx value.haoMaList 175 1 3}}</span></div>
                            
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
{{each data.list as value i}}
                <div class="row-60 flex f-s-18">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c f-s-16">
                            <div class="flex1 v-m">
                                <div class="d-i ">
                                    <div class="{{$yanSe value.haoMaList[6]}}">{{value.haoMaList[6]}}</div>
                                    <p class="shengxiao">{{$shengXiao value.haoMaList[6]}}</p>
                                </div>
                            </div>
                            <div class="flex1 flex-m"><span class="{{$lhcTeMaDs value.haoMaList[6] 2}}">{{$lhcTeMaDs value.haoMaList[6] 1}}</span></div>
                            <div class="flex1 flex-m"><span class="{{$lhcTeMaDx value.haoMaList[6] 2}}">{{$lhcTeMaDx value.haoMaList[6] 1}}</span></div>
                            <div class="flex-m"><span class=" {{$lhcTeMaHeDs value.haoMaList[6] 2}} " style="min-width: 1.9rem">{{$lhcTeMaHeDs value.haoMaList[6] 1}}</span></div>
                            <div class="flex-m"><span class=" {{$lhcTeMaHeDx value.haoMaList[6] 2}}" style="min-width: 1.9rem">{{$lhcTeMaHeDx value.haoMaList[6] 1}}</span></div>
                            <div class="flex-m"><span class=" {{$lhcTeMaWeiDx value.haoMaList[6] 2}}" style="min-width: 1.9rem">{{$lhcTeMaWeiDx value.haoMaList[6] 1}}</span></div>
                        </div>
                    </div>
                </div>
{{/each}}
            </div>

            <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=25?'blueBall':'originBall'}}">{{value.haoMaList[0]>=25?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=25?'blueBall':'originBall'}}">{{value.haoMaList[1]>=25?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=25?'blueBall':'originBall'}}">{{value.haoMaList[2]>=25?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]>=25?'blueBall':'originBall'}}">{{value.haoMaList[3]>=25?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]>=25?'blueBall':'originBall'}}">{{value.haoMaList[4]>=25?'大':'小'}}</span></div>
			                <div class="flex1"><span class="{{value.haoMaList[5]>=25?'blueBall':'originBall'}}">{{value.haoMaList[5]>=25?'大':'小'}}</span></div>

                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[3]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[4]%2!=0?'单':'双'}}</span></div>
							<div class="flex1"><span class="{{value.haoMaList[5]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[5]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>

        <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[0] 7 2}}">{{$gyhDx2 value.haoMaList[0] 7 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[1] 7 2}}">{{$gyhDx2 value.haoMaList[1] 7 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[2] 7 2}}">{{$gyhDx2 value.haoMaList[2] 7 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[3] 7 2}}">{{$gyhDx2 value.haoMaList[3] 7 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[4] 7 2}}">{{$gyhDx2 value.haoMaList[4] 7 1}}</span></div>
			                <div class="flex1"><span class="{{$gyhDx2 value.haoMaList[5] 7 2}}">{{$gyhDx2 value.haoMaList[5] 7 1}}</span></div>

                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

        <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[0] 3}}">{{$gyhDs2 value.haoMaList[0] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[1] 3}}">{{$gyhDs2 value.haoMaList[1] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[2] 3}}">{{$gyhDs2 value.haoMaList[2] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[3] 3}}">{{$gyhDs2 value.haoMaList[3] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[4] 3}}">{{$gyhDs2 value.haoMaList[4] 1}}</span></div>
			                <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[5] 3}}">{{$gyhDs2 value.haoMaList[5] 1}}</span></div>

                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

 
</script>
