<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="syxwTpl">

            <div class="">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[0]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[1]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[2]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[3]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[4]}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=6?'blueBall':'originBall'}}">{{value.haoMaList[0]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=6?'blueBall':'originBall'}}">{{value.haoMaList[1]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=6?'blueBall':'originBall'}}">{{value.haoMaList[2]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]>=6?'blueBall':'originBall'}}">{{value.haoMaList[3]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]>=6?'blueBall':'originBall'}}">{{value.haoMaList[4]>=6?'大':'小'}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[3]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[4]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 2}}</div></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="flex1 p-l-5"><span class="{{$gyhDxYouHe value.haoMaList 30 3 2}}">{{$gyhDxYouHe value.haoMaList 30 1 2}}</span></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="flex1 p-l-5 v-m"><span class="{{$gyhDsYouHe value.haoMaList 2 29 3}}">{{$gyhDsYouHe value.haoMaList 2 29 1}}</span></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="{{$gyhWeiDx value.haoMaList 1 2}}">{{$gyhWeiDx value.haoMaList 1 1}}</div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="flex1 p-l-5 v-m"><span class="{{$cpLh value.haoMaList 10 3}}">{{$cpLh value.haoMaList 10 1}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

</script>
