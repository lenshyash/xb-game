<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="klsfTpl">
            <div class="">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[0]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[1]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[2]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[3]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{value.haoMaList[4]}}</span></div>
							<div class="flex1"><span class="blueBall">{{value.haoMaList[5]}}</span></div>
							<div class="flex1"><span class="blueBall">{{value.haoMaList[6]}}</span></div>
							<div class="flex1"><span class="blueBall">{{value.haoMaList[7]}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=11?'blueBall':'originBall'}}">{{value.haoMaList[0]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=11?'blueBall':'originBall'}}">{{value.haoMaList[1]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=11?'blueBall':'originBall'}}">{{value.haoMaList[2]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]>=11?'blueBall':'originBall'}}">{{value.haoMaList[3]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]>=11?'blueBall':'originBall'}}">{{value.haoMaList[4]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[5]>=11?'blueBall':'originBall'}}">{{value.haoMaList[5]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]>=11?'blueBall':'originBall'}}">{{value.haoMaList[6]>=11?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[7]>=11?'blueBall':'originBall'}}">{{value.haoMaList[7]>=11?'大':'小'}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[3]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[4]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[5]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[5]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[6]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[7]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[7]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

                

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 5}}</div></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="flex1 p-l-5"><span class="{{$gyhDs value.haoMaList 3 3}}">{{$gyhDs value.haoMaList 3 1}}</span></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="flex1 p-l-5 v-m"><span class="{{$gyhDx value.haoMaList 85 3 4}}">{{$gyhDx value.haoMaList 85 1 4}}</span></div>
                            <div class="flex1 p-l-5"><span class="p-l-10"></span></div>
                            <div class="{{$gyhWeiDx value.haoMaList 2 3}}">{{$gyhWeiDx value.haoMaList 2 1}}</div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[0] 4}}">{{$gyhWeiDx2 value.haoMaList[0] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[1] 4}}">{{$gyhWeiDx2 value.haoMaList[1] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[2] 4}}">{{$gyhWeiDx2 value.haoMaList[2] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[3] 4}}">{{$gyhWeiDx2 value.haoMaList[3] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[4] 4}}">{{$gyhWeiDx2 value.haoMaList[4] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[5] 4}}">{{$gyhWeiDx2 value.haoMaList[5] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[6] 4}}">{{$gyhWeiDx2 value.haoMaList[6] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhWeiDx2 value.haoMaList[7] 4}}">{{$gyhWeiDx2 value.haoMaList[7] 1}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[0] 3}}">{{$gyhDs2 value.haoMaList[0] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[1] 3}}">{{$gyhDs2 value.haoMaList[1] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[2] 3}}">{{$gyhDs2 value.haoMaList[2] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[3] 3}}">{{$gyhDs2 value.haoMaList[3] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[4] 3}}">{{$gyhDs2 value.haoMaList[4] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[5] 3}}">{{$gyhDs2 value.haoMaList[5] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[6] 3}}">{{$gyhDs2 value.haoMaList[6] 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhDs2 value.haoMaList[7] 3}}">{{$gyhDs2 value.haoMaList[7] 1}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[0] 3}}">{{$dnxb123 value.haoMaList[0] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[1] 3}}">{{$dnxb123 value.haoMaList[1] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[2] 3}}">{{$dnxb123 value.haoMaList[2] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[3] 3}}">{{$dnxb123 value.haoMaList[3] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[4] 3}}">{{$dnxb123 value.haoMaList[4] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[5] 3}}">{{$dnxb123 value.haoMaList[5] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[6] 3}}">{{$dnxb123 value.haoMaList[6] 1}}</span></div>
                            <div class="flex1"><span class="{{$dnxb123 value.haoMaList[7] 3}}">{{$dnxb123 value.haoMaList[7] 1}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$zfb123 value.haoMaList[0] 3}}">{{$zfb123 value.haoMaList[0] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[1] 3}}">{{$zfb123 value.haoMaList[1] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[2] 3}}">{{$zfb123 value.haoMaList[2] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[3] 3}}">{{$zfb123 value.haoMaList[3] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[4] 3}}">{{$zfb123 value.haoMaList[4] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[5] 3}}">{{$zfb123 value.haoMaList[5] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[6] 3}}">{{$zfb123 value.haoMaList[6] 1}}</span></div>
<div class="flex1"><span class="{{$zfb123 value.haoMaList[7] 3}}">{{$zfb123 value.haoMaList[7] 1}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>

</script>
