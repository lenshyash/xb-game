<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="pcddTpl">
                 <div class="text-c">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100">
                            <div class="flex1"><span class="{{$yanSe2 value.haoMaList[0]}}">{{value.haoMaList[0]}}</span></div>
							<div class="flex1">{{value.haoMaList[1]}}</div>
                            <div class="flex1"><span class="{{$yanSe2 value.haoMaList[2]}}">{{value.haoMaList[2]}}</span></div>
							<div class="flex1">{{value.haoMaList[3]}}</div>
                            <div class="flex1"><span class="{{$yanSe2 value.haoMaList[4]}}">{{value.haoMaList[4]}}</span></div>
							<div class="flex1">{{value.haoMaList[5]}}</div>
                            <div class="flex1"><span class="{{$yanSe2 value.haoMaList[6]}}">{{value.haoMaList[6]}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]>=14?'blueBall':'originBall'}}">{{value.haoMaList[6]>=14?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[6]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>

           <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
							<div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$spliceNum value.haoMaList[6]}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]>=14?'blueBall':'originBall'}}">{{value.haoMaList[6]>=14?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[6]%2!=0?'单':'双'}}</span></div>

				
                            <div class="flex1"><span class="{{$gyhSz value.haoMaList[6] 1 2}}">{{$gyhSz value.haoMaList[6] 1 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhSz value.haoMaList[6] 1 2}}">{{$gyhSz value.haoMaList[6] 1 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhSz value.haoMaList[6] 1 2}}">{{$gyhSz value.haoMaList[6] 1 1}}</span></div>
                            <div class="flex1"><span class="{{$gyhSz value.haoMaList[6] 1 2}}">{{$gyhSz value.haoMaList[6] 1 1}}</span></div>
							
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
</script>
