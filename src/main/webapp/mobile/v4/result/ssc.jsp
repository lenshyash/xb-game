<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="sscTpl">
             <div class="">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[0]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[1]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[2]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[3]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[4]}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=5?'blueBall':'originBall'}}">{{value.haoMaList[0]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=5?'blueBall':'originBall'}}">{{value.haoMaList[1]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=5?'blueBall':'originBall'}}">{{value.haoMaList[2]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]>=5?'blueBall':'originBall'}}">{{value.haoMaList[3]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]>=5?'blueBall':'originBall'}}">{{value.haoMaList[4]>=5?'大':'小'}}</span></div>
                        </div>
                    </div>
                </div>
{{/each}}
            </div>
            <div class="hide">
{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[3]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[4]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 2}}</div></div>
                            <div class="flex1 p-l-5"><span class="{{$gyhDx value.haoMaList 23 3 2}}">{{$gyhDx value.haoMaList 23 1 2}}</span></div>
                            <div class="flex1 p-l-5"><span class="{{$gyhDs value.haoMaList 2 3}}">{{$gyhDs value.haoMaList 2 1}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 2 1}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 3 1}}</span></div>
                        </div>
                    </div>
                </div>
              {{/each}}  
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{$gyhZh value.haoMaList[0] 1}}">{{$gyhZh value.haoMaList[0] 0}}</span></div>
                            <div class="flex1"><span class="{{$gyhZh value.haoMaList[1] 1}}">{{$gyhZh value.haoMaList[1] 0}}</span></div>
                            <div class="flex1"><span class="{{$gyhZh value.haoMaList[2] 1}}">{{$gyhZh value.haoMaList[2] 0}}</span></div>
							<div class="flex1"><span class="{{$gyhZh value.haoMaList[3] 1}}">{{$gyhZh value.haoMaList[3] 0}}</span></div>
							<div class="flex1"><span class="{{$gyhZh value.haoMaList[4] 1}}">{{$gyhZh value.haoMaList[4] 0}}</span></div>
                        </div>
                    </div>
                </div>
              {{/each}}  
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1 p-l-5"><span class="flex1 lhc-b b-r-6 c-w">万千龙</span></div>
                            <div class="flex1 p-l-5"><span class="flex1 lhc-b b-r-6 c-w">万千龙</span></div>
                            <div class="flex1 p-l-5"><span class="flex1 lhc-b b-r-6 c-w">万千龙</span></div>
<div class="flex1 p-l-5"><span class="flex1 lhc-b b-r-6 c-w">万千龙</span></div>
<div class="flex1 p-l-5"><span class="flex1 lhc-b b-r-6 c-w">万千龙</span></div>
                        </div>
                    </div>
                </div>
              {{/each}}  
            </div>

            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 2 1}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 3 1}}</span></div>
                        </div>
                    </div>
                </div>
              {{/each}}  
            </div>


   
</script>
