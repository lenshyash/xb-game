<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="dpcTpl">
        
            <div class="">
		{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[0]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[1]}}</span></div>
                            <div class="flex1"><span class="blueBall">{{$spliceNum value.haoMaList[2]}}</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhSum value.haoMaList 4}}</span></div>
                            <div class="flex1"><span class="f-s-20">跨度:</span></div>
                            <div class="flex1"><span class="f-w-900 f-s-20 c-r">{{$gyhKd value.haoMaList 1 1}}</span></div>

                        </div>
                    </div>
                </div>
		{{/each}}
            </div>
            <div class="hide">
{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=5?'blueBall':'originBall'}}">{{value.haoMaList[0]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=5?'blueBall':'originBall'}}">{{value.haoMaList[1]>=5?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=5?'blueBall':'originBall'}}">{{value.haoMaList[2]>=5?'大':'小'}}</span></div>
                        </div>
                    </div>
                </div>
	{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBall':'originBall'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
							<div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 4}}</div></div>
							<div class="flex1 p-l-5"><span class="{{$gyhDx value.haoMaList 14 3 4}}">{{$gyhDx value.haoMaList 14 1 4}}</span></div>
                            <div class="flex1 p-l-5"><span class="{{$gyhDs value.haoMaList 4 3}}">{{$gyhDs value.haoMaList 4 1}}</span></div>
                            <div class="flex1 p-l-5"><span class="{{$cpLh value.haoMaList 16 3}}">{{$cpLh value.haoMaList 16 1}}</span></div>
							<div class="{{$teShuWanFa value.haoMaList 1 3}}">{{$teShuWanFa value.haoMaList 1 1}}</div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
        
</script>
