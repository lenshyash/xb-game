<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="pk10Tpl">
      <div class="">
				{{each data.list as value i}}


                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[0]}}">{{$spliceNum value.haoMaList[0]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[1]}}">{{$spliceNum value.haoMaList[1]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[2]}}">{{$spliceNum value.haoMaList[2]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[3]}}">{{$spliceNum value.haoMaList[3]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[4]}}">{{$spliceNum value.haoMaList[4]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[5]}}">{{$spliceNum value.haoMaList[5]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[6]}}">{{$spliceNum value.haoMaList[6]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[7]}}">{{$spliceNum value.haoMaList[7]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[8]}}">{{$spliceNum value.haoMaList[8]}}</span></div>
                            <div class="flex1"><span class="bj-item bj-{{$spliceNum value.haoMaList[9]}}">{{$spliceNum value.haoMaList[9]}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            <div class="hide">
				{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><span class="{{value.haoMaList[0]>=6?'blueBg':'originBg'}}">{{value.haoMaList[0]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]>=6?'blueBg':'originBg'}}">{{value.haoMaList[1]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]>=6?'blueBg':'originBg'}}">{{value.haoMaList[2]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]>=6?'blueBg':'originBg'}}">{{value.haoMaList[3]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]>=6?'blueBg':'originBg'}}">{{value.haoMaList[4]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[5]>=6?'blueBg':'originBg'}}">{{value.haoMaList[5]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]>=6?'blueBg':'originBg'}}">{{value.haoMaList[6]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[7]>=6?'blueBg':'originBg'}}">{{value.haoMaList[7]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[8]>=6?'blueBg':'originBg'}}">{{value.haoMaList[8]>=6?'大':'小'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[9]>=6?'blueBg':'originBg'}}">{{value.haoMaList[9]>=6?'大':'小'}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            <div class="hide">
                {{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><span class="{{value.haoMaList[0]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[0]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[1]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[1]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[2]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[2]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[3]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[3]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[4]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[4]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[5]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[5]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[6]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[6]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[7]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[7]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[8]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[8]%2!=0?'单':'双'}}</span></div>
                            <div class="flex1"><span class="{{value.haoMaList[9]%2!=0?'blueBg':'originBg'}}">{{value.haoMaList[9]%2!=0?'单':'双'}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            <div class="hide">
                 {{each data.list as value i}}

                 <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 1}}</div></div>
                            <div class="flex1 p-l-10"><span class="{{$gyhDx value.haoMaList 11 2 1}}">{{$gyhDx value.haoMaList 11 1 1}}</span></div>
                            <div class="flex1 p-r-10"><span class="{{$gyhDs value.haoMaList 1 2}}">{{$gyhDs value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 1 2}}">{{$cpLh value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 2 2}}">{{$cpLh value.haoMaList 2 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 3 2}}">{{$cpLh value.haoMaList 3 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 4 2}}">{{$cpLh value.haoMaList 4 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 5 2}}">{{$cpLh value.haoMaList 5 1}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            </div>

            <div class="hide">
                 {{each data.list as value i}}

                 <div class="row flex">
                    <div class="left">
                        <span class="title">测试</span>
                        <span class="title">123</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">{{$gyhSum value.haoMaList 1}}</div></div>
                            <div class="flex1 p-l-10"><span class="{{$gyhDx value.haoMaList 11 2 1}}">{{$gyhDx value.haoMaList 11 1 1}}</span></div>
                            <div class="flex1 p-r-10"><span class="{{$gyhDs value.haoMaList 1 2}}">{{$gyhDs value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 1 2}}">{{$cpLh value.haoMaList 1 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 2 2}}">{{$cpLh value.haoMaList 2 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 3 2}}">{{$cpLh value.haoMaList 3 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 4 2}}">{{$cpLh value.haoMaList 4 1}}</span></div>
                            <div class="flex1"><span class="{{$cpLh value.haoMaList 5 2}}">{{$cpLh value.haoMaList 5 1}}</span></div>
                        </div>
                    </div>
                </div>
				{{/each}}
            </div>
            </div>
</script>
