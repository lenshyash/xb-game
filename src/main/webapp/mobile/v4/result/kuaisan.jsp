<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/html" id="kuaisanTpl">
            <div class="">
				{{each data.list as value i}}
                <div class="row-60 flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="touzi {{$kuaiSanNum value.haoMaList[0] 1}}"></span></div>
                            <div class="flex1"><span class="touzi {{$kuaiSanNum value.haoMaList[1] 1}}"></span></div>
                            <div class="flex1"><span class="touzi {{$kuaiSanNum value.haoMaList[2] 1}}"></span></div>
                            <div class="flex1"><span class="f-w-900 c-r f-s-20">{{$gyhSum value.haoMaList 4}}</span></div>
                            
                        </div>
                    </div>
                </div>
    			{{/each}}
            </div>
            <div class="hide">
			{{each data.list as value i}}
                <div class="row flex">
                    <div class="left">
                        <span class="title">{{value.qiHao}}</span>
                        <span class="title">{{value.time}}</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="c-g f-w-900 f-s-20">{{$kuaiSanNum value.haoMaList[0] 2}}</span></div>
                            <div class="flex1"><span class="c-b f-w-900 f-s-20">{{$kuaiSanNum value.haoMaList[1] 2}}</span></div>
                            <div class="flex1"><span class="c-r f-w-900 f-s-20">{{$kuaiSanNum value.haoMaList[2] 2}}</span></div>
                        </div>
                    </div>
                </div>
			{{/each}}
            </div>
</script>
