<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${_title }</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="${res }/css/weui.min.css">
    <link rel="stylesheet" href="${res }/css/jquery-weui.css">
    <link rel="stylesheet" href="${res }/css/common.css">
</head>
<body class="bg-238 flexWrap">
<div class="box p-r">
    <div class="flexWrap">
        <div class="top flex">
            <div class="d-i flex1 logo"></div>
            <c:if test="${isLogin }">
	            <div class="d-i text-r f-s-18">
	                <p class="f-s-21">(试玩)游客</p>
	                <div class="p-r">
	                    ￥2000.00
	                    <span class="icon arrow-white"></span>
	                    <div class="modal hide">
	                        <div class="p-20 text-l f-s-14">
	                            <p class="c-9">未结算金额:<span class="pull-r c-3 f-w-900"><span class="icon money">￥</span>0.0</span></p>
	                            <p class="c-9 p-t-10">今日输赢:<span class="pull-r c-3 f-w-900"><span class="icon money">￥</span>0.0</span></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </c:if>
            <c:if test="${!isLogin }">
	            <div class="loginBefore">
	                <a class="p-l-r-15 p-b-t-5 c-w b-1 b-r-35 f-s-14" href="${m }/login.do">登陆</a>
	                <c:if test="${isReg ne 'off' }">
	                		<a class="p-l-r-15 p-b-t-5 c-w b-1 b-r-35 f-s-14" href="${m }/regpage.do">注册</a>
	                </c:if>
	                <c:if test="${testAccount eq 'of' }">
	                		<a class="p-l-r-15 p-b-t-5 c-w b-1 b-r-35 f-s-14" href="javascript:void(0)" onclick="location.href='${m}/swregpage.do'">试玩</a>
	                </c:if>
	            </div>
            </c:if>
        </div>
        <p class="title c-w text-c p-t-15">欢迎光临 Welcome!</p>
        <div style="height: 1rem"></div>
        <div class="m-b-15 game-list m-10 bg-w b-r-6 c-6 p-b-t-5 p-l-r-20 ">
            <div class="f-s-14 f-w-900 checkGame p-r"><div class="select d-i">最近游戏</div> <span class="arrow-black"></span>
            <div class="modal hide"><ul><li>最近游戏</li><li>热门游戏</li></ul></div>
            </div>
            <div class="flex p-b-5">
            		<c:set var="hotCount" value="0" scope="request"></c:set>
            		<c:forEach items="${bcLotterys }" var="lot" >
            			<c:if test="${hotCount lt 4 }">
	            			<c:if test="${lot.hotGame eq 1 }">
	            				<div class="flex1">
			                    <div style="background: url(./image/SGFT.svg) center center no-repeat;height: 4rem"></div>
			                    <p class="f-s-14 text-c">${lot.name }</p>
			                </div>
			                <c:set var="hotCount" value="${hotCount+1 }"></c:set>
            				</c:if>
            			</c:if>
            		</c:forEach>
            </div>
        </div>
        <!-- 容器 -->
        <div class="m-l-r-10 game-box b-r-6 flexContent bg-w">
            <div class="weui-tab">
                <div class="weui-navbar bg-238 c-3 f-w-900">
                		<c:forEach items="${lotGroupMap }" var="group" varStatus="i">
                 		<a class="weui-navbar__item f-s-14 l-h-20 <c:if test="${i.index eq 0 }">weui-bar__item--on</c:if> " href="#group_${group.key }">${group.value }</a>
                 	</c:forEach>
                </div>
                <div class="weui-tab__bd gameList flex flexContent">
                <c:forEach items="${groupLotteryMap }" var ="group" varStatus="i">
                 <div id="group_${group.key}" class="weui-tab__bd-item fadeInRight <c:if test="${i.index eq 0 }">weui-tab__bd-item--active</c:if>" style="width:100%;">
                 	<ul class="p-10">
                 		<c:forEach items="${group.value }" var="lot">
                 			<li class="d-i p-10">
                 				<a href="${m }/bet_lotterys.do?lotCode=${lot.code}">
		                             <div class="sc-5u18nc-0 dDuZFA">
		                             	<c:choose>
		                             		<c:when test="${lot.hotGame eq 1 }"><span class="game-icon-played"></span></c:when>
		                             		<c:when test="${lot.newGame eq 1 }"><span class="game-icon-recent"></span></c:when>
		                             	</c:choose>
		                                 
		                                 <div class="game-icon"
		                                      style="background-image: url(./image/SGFT.svg);"></div>
		                                 <div class="game-name f-s-14 text-c c-6">${lot.name }</div>
		                             </div>
	                             </a>
	                         </li>
                 		</c:forEach>
                     </ul>
                 </div>
                </c:forEach>
                </div>
            </div>
        </div>
        <div class="h-70"></div>
        <jsp:include page="./include/footer.jsp"/>
   		<jsp:include page="./include/menu.jsp"/>
</div>

<!-- body 最后 -->
<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
</html>
<style>
    .loginBefore a{
        background-color: rgba(255, 255, 255, 0.2);
    }


    .top {
        padding: 1em 1em 0;
        color: #fff;
    }
    .title {
        font-family: Tahoma;
        font-size: 20px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
    }
    .game-list {
        box-shadow: rgba(0, 0, 0, 0.08) 0px 3px 6px 0px;
    }
    .game-box {
        box-shadow: rgba(0, 0, 0, 0.08) 0px 3px 6px 1px;
    }
    .box{
        height: 100%;
    }

    .box::before {
        content: "";
        position: absolute;
        top: 0px;
        right: 0px;
        left: 0px;
        width: 100%;
        height: 30%;
        background-image: url('./image/Banner.jpg'), linear-gradient(to right, rgb(125, 202, 255) 0%, rgb(26, 81, 148) 100%);
        background-size: cover;
        z-index: -1;
    }

    .weui-navbar + .weui-tab__bd {
        padding-top: 35px;
        background: #fff;
    }

    .dDuZFA .game-icon {
/*         width: 64px; */
        height: 64px;
        background-size: auto 100%;
        /*margin: 0px auto;*/
        background-position: center center;
        background-repeat: no-repeat;
        margin: 0 5px;
    }
    .gameList{
        height: 20rem;
        overflow: scroll;
    }
    .gameList ul {
        width: 100%;
        box-sizing: border-box;
        display: flex;
        flex-wrap: wrap;
        align-items: flex-start;
        list-style: none;
        margin: 0px;
        padding: 8px;
    }

    .gameList ul li {
        display: block;
        box-sizing: border-box;
        width: 100%;
        list-style: none;
        margin: 0px;
        padding: 5px;
        flex: 0 0 25%;
        overflow: hidden;
    }

    .gameList ul li {
        display: block;
        box-sizing: border-box;
        width: 100%;
        list-style: none;
        margin: 0px;
        padding: 5px;
        flex: 0 0 25%;
        overflow: hidden;
    }
    .dDuZFA .game-icon-played {
        position: absolute;
        top: -2px;
        right: 4px;
        width: 25px;
        height: 25px;
        background: url('./image/hot.svg') no-repeat;
    }
    
    .dDuZFA .game-icon-recent {
        position: absolute;
	    top: -2px;
	    right: 4px;
	    width: 25px;
	    height: 25px;
        background: url('./image/new.svg') no-repeat;
    }

    .dDuZFA {
        position: relative;
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 6px 0px;
        padding-bottom: 5px;
        border-radius: 8px;
    }
</style>