<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
</head>
<body>
<div>
    <div class="top bg text-c h-50 l-h-50">
        <a class="back-to-game c-w f-s-20 f-w-900" target="_blank" href="http://1681391.com">推荐APP</a>
    </div>
    <div class=" kLDWnr">
        <div class=" fvatTI">
            <div class=" dGMEQb"></div>
            <div class=" iTzDjM" size="16">扫码下载APP</div>
            <div class=" kxluZd" style="text-align: center; padding: 10px 0px;"><img
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAKoklEQVR4Xu2d4XblNggGk/d/6PS0yblNfLw2jADb2elvIdDHCCH5bvr+8fHx8eZ/KvBLFHgX6F+SSZfxnwIvoN/f3x8vydFhM72+jlg6DtNpXTog+66LQHco/Pb2JtBNwu5MK9ADWgv0gMhfLgR6QGuBHhBZoK8ReeuV9q320Pv5s0IPcG2FHhDZCn2NyFboXt3TFbrjqKNLPDquaVWk67tTLNN6Un/ULqp16NmOJpwGf2QXXVimKtL13SkWqjVdA/VH7aJxCvTJm/FTNlc3KHT+KjuB3igZFSSTADontcvEFh17p1gqiocV2gr9R45oKxbdTJlx0Y0n0AIt0JmdVT02ulO9FMaUp3rGZq8bFY1zuULTr15HS33K8xtNVzQ5dP6MHY3lrnkX6H9/Q3vw09mOPnLaX8VlK3PiZTbU97EVhUygBRr10FboxLat2KkJd1boP4hF85DR3grd0B5MtwDT/mw5Ngo8cadmqsQ0YNP+BFqgXwp4KdzfDvbQiZJJT4QO+Gh1Syy3ZChdOz0tBDqRNoFOiPU1VKA/hfDZLs/Oy6KjStFwBFqgKTsCffJ2T4WtOJmt0FT9pqTScKzQVmjKjhW6aTNboRPPi8v07kxgD12rqkAngKbPUx3PdhWJ28ZF19dhRzGv0OWv6aFp4gTaDyt0g77sKnZqVQUTaIEW6BMF7rRh6cnVcX+o0MWWo+nfFB4xXZG4qhNIoDdK3nWnViXclsOWw5bDlgMxUHFyLbccKPIFo99wRNLlVyR84uSi66OnoX+ssUhxChh1T/112NE1ULtoIbNCU4Wb/j8qT7lMLsiGTAX6xpdXlNGFDWSF3ihOf8lFE1fRS2V6RRonBWXaH43ziXm35aB0LVRM6rIDzOhRTmOusovGKdALilPAqEvqr8OOroHalQJNg5i260hcVMhMi/OUOKfzR/2ln+2oo2m7p4DylDin80f9CXTi0muFppjN2Qm0QL8U6PgtzhzKn54EWqAFenrXEX9P6U2fEifJwRU2Vmgr9O+v0FfsrEmftFec/lpGL6GTWt7Z1+vDyp2DrIhNoCtUvP8cAn2SIyv0/SH+HqFAC/SziD2JVqAFWqCfqIA99BOzlo/ZCm2FzlNzY4vQz0dp/PRC1VFNnzLnkdZ0DTR/1O7KvAv0QoWmgE0nnIJJ7abXN/al8MqFbZPRAV/HnFbo/DYS6IRm9HcXRy6mN3piuSVDp9cn0Im0CXRCrK+hAr3RrOMof8qcthxrG8hLoZfCPEELmnVs2N2Wg66K/jqM2k3H2eGP9tf0lKFr6Iizu4Vb/rBCwaR2NDl38tcBCtWF2lEwqV1UM4Fe+IPn3cdn1dMjhTYKUSZOgS7KhhW6SMjASwbVmrZU9tCb3NJnJiv0vgICfTFgAp2v3rR1oHbR9sce2h46T/PCH6ocA5r2L0iNBUHu5K87OVdftqjW1I7qOfbpO3pMZBI3Ldb0GiqSOqEnzUOHngK9UZWeThS+7qQK9NvbG00q3akdMHSA8pQ5p/Wkee/Q0wpthe7gcXlOuikFWqCX4euYQKATqlKxOo7Ijjk71peQt2QoXUNphaZBUAXoR5Dpr1fT0B7567gf0bzT/EV5Wf49NF1YNMDtOCqIQFPF9+1o3mn+otEL9IM+8lAYrNDR7XAzGOixS6tNQqYfQ6k/gT5X3Ap9s01JWyO6mc8RseV4KTB91NGk0or5FBim89CxKaNaW6Gt0FFWLm2bokG2/ny04+kqurDM6witYE/paenJRfNHc0Tj3H2HpkF0wEDnpAmg/gSaUpO3i7YxVuiFH2YJdB5MaiHQG+U6Ln4CTfHM2wm0QIeomW63QkHtDBJogQ6xI9AbmToEoXN6KQwx/GMQ1Zq2W/kIPy1KK3R0Mhrs1m7aX8VzUWbtdH3Ujq6PFoiMFt/HVmyu0CtHh5BU5LtWhkwSqZ7Ujmot0JmsHoztSBwNrSMWOie1E+hEn9xRMTsSJ9D7ClQc81TbTKsZPS1sOU6y0bG56JzUzgpthX4pMA0R/QBET0or9AZ2KiStGh3+pmOhm4Ta0fVFj/lbthx0p9JFd/ijsdCEdABG4Zve6DROmqOo1su/h+4IkAJGY6H+oiLT+TOXJoH+VEugF2gT6LmXk6jWAi3QCwoIdOj1oFzhC/6oeUcLEK1SHfpl5qT3o4oXHit0JlObsdOATfuj0gg0VS5h9xsqpkDvJ/z2/6YwwemPoTThHRWFPmvRjTe9dpojur6ov9Cnb5qcpzyjCXQUl/VxAp3QcLpK0eTQOGlhoZethPThoVSzqAMr9M3+1TdNON0k9HSKArYdR9cX9SfQAh1lpWScQCdknK5SNDk0TluOcxis0Fboc0oKR9AiEA3hV31Y6RDrTj3mdCxRiKr65IqTS6BPsjYN0Z1eJAR6o8B0cqzQFMFaO5oHK3RiA9GUWaHzygl0XrNdCyokfT0oCvvHNNOn2vQaqNbR3NpD20OXMx2Fb+vYlsOWoxzGiglvAXTFQu48B+2Fr0xOpoIdaU/X0N0eZHiJVu/lDyuZoK4cK9C16kcBq/Ia9SfQJ4rT6hZNQCbh05vSCp3JzvDYaRgEujbBUT2t0FZoRF4UMDT5jlHUn0ALNGIuChiaXKDPZbPlONcoM+L2QNOEZ0ToHksvcB2Xnw496VfEO9lRBqIbqPVLIQ2e2gn0vnICTYm62E6gBdoKfbIJo0fddhpbjtrqFs2DQAv0HxXoaFUo5gJNldvYRYW0Qv+vwJWtnxXaCv33VeiOHUcLaEfFpOujfXLHUU51mc4D9Re1C1VomvBoEJlxNHHUjr5RH9kJdCbjubECvfDH0K3Q+WfCHJ750QIt0HlqTv44z5UnukALtEAjBYqMaC9M7eyh9xXo0LMCESu0FRpx9GuBphej6VeA6Urb4Q+Rd4FRRw8d3UDLFVqg88R0aJaPos9CoDfadrzTdlRMmjiBzm8mK3RCMwqYQM+9Qwu0QCcUqB1KNzo9Rb/7s4e+2V/wr0XrmtkE2h76GvKavAr0jYGeTg5lrCPOihZgO0f3fcWW44LfJdCk0rd7ukkEuki5Oz3bdVQ+gc6DEs2DFdoKnadrQTO6mQU6kaboG2diysOhNKm2HOcZsEIvVJtzefdHCHReOSt0QjMrdEKsr6FUM7qZBXqTIypkPtWfFtEEZOana6CxUH+0NaL+/sovhVSsDHDfx1KI6FMZhajDH42F5kigKaUJO4HeF4s+yUY3yV9zKaS7P8Hwj6ECLdAvBegupnYU2mjVqJqfbkq6uag/qgv1Z8tRRdjBPBSijp6WxkIBE+iNArTSUrsOvilEAp3PRmmFzrtfs7jr+2dmVU9ZA63QHZs5qu/ypTDqqGrcU2CglbbjlKGACXQVtQfzCHReZIFO9LR5edcsBDqvn0ALdOgJMY/Wp8VTNqUtB81wwu4pMNhDJ5JaONRL4YmY9LgW6EJKE1OFgE7Md+nQjheC6QV1rIHOSTczPUUrioBATxO7cCJ09LRXwpeRPhqnQGdUHRhLq+lRaHROK/RAwjsSd3HYP9xT+Dp0EeiLyeiAYXpJHWugcwr0dPY3/mjiLg7bCh1IgD30RiR6oQpoXTqkY1PSOR9doUuz4mQqcJECr1eOi/zrVgVKFRDoUjmd7GoF/gGhbe7GMgZG+wAAAABJRU5ErkJggg==">
            </div>
            <div class=" hSadsX">
                <div>https://dsn1151a.com</div>
                <button>复制下载链接</button>
            </div>
            <div class=" jxCsF">
                <div>推荐APP和好友一起玩翻天～</div>
                <ol>
                    <li>使用二维码工具扫描上方二维码</li>
                    <li>开启下载页面后点击安装</li>
                </ol>
            </div>
            <div class=" dfXXZw">
                <button>推荐好友</button>
                <button>保存二维码</button>
            </div>
        </div>
        <div class="-2 fvatTI">
            <div class=" kXAQlX"><span>网页版网址</span><span>dsn02.co</span>
                <button class=" cIjVfp">复制</button>
            </div>
        </div>
    </div>
    <jsp:include page="./include/footer.jsp"/>
   	<jsp:include page="./include/menu.jsp"/>
</div>


<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
</html>
<style>
    .top {
        position: relative;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 52px;
        line-height: 52px;
        background-image: linear-gradient(to right, rgb(124, 201, 254), rgb(27, 82, 149));
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px 0px;
        z-index: 10;
    }
    .kLDWnr {
        height: 100%;
        display: flex;
        flex-direction: column;
        background-color: rgb(234, 234, 234);
        overflow: auto;
        flex: 1 1 100%;
    }
    .fvatTI {
        display: flex;
        flex-direction: column;
        background-color: rgb(255, 255, 255);
        flex: 0 0 auto;
        border-bottom: 4px solid rgb(234, 234, 234);
    }
    .dGMEQb {
        width: 100%;
        min-height: 1px;
        margin-top: 0px;
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 8px;
        flex: 0 0 auto;
    }
    .iTzDjM {
        color: rgb(138, 138, 138);
        font-size: 16px;
        font-weight: bold;
        margin-bottom: 8px;
        text-align: center;
        overflow-x: visible;
        white-space: normal;
        display: block;
        text-overflow: ellipsis;
        text-indent: 0px;
        flex: 0 0 auto;
    }
    .hSadsX {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        border-radius: 6px;
        margin: 0px 24px 12px;
        overflow: hidden;
    }
    .hSadsX > div {
        background-color: rgb(234, 234, 234);
        color: rgb(24, 134, 211);
        white-space: nowrap;
        text-overflow: ellipsis;
        text-align: center;
        box-sizing: border-box;
        font-size: 14px;
        flex: 1 1 100%;
        overflow: hidden;
        padding: 8px;
    }
    .hSadsX > button {
        display: block;
        background-color: rgb(24, 134, 211);
        color: rgb(255, 255, 255);
        white-space: nowrap;
        text-align: center;
        box-sizing: border-box;
        font-size: 12px;
        margin: 0px;
        flex: 0 0 auto;
        outline: 0px;
        border-width: 0px;
        border-style: initial;
        border-color: initial;
        border-image: initial;
        padding: 8px;
    }
    .jxCsF {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        box-sizing: border-box;
        background-color: rgb(234, 234, 234);
        border-radius: 6px;
        margin: 0px 24px 12px;
        padding: 12px 8px;
        overflow: hidden;
    }
    .jxCsF > div {
        font-size: 14px;
        color: rgb(19, 106, 167);
        text-align: center;
        margin-left: -36px;
    }
    ol {
        display: block;
        list-style-type: decimal;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        padding-inline-start: 40px;
    }
    ul, li {
         list-style: decimal;
    }
    .jxCsF > ol {
        font-size: 14px;
        color: rgb(165, 165, 165);
        padding-left: 16px;
        margin: 6px auto;
    }
    .dfXXZw {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        border-radius: 6px;
        margin: 0px 24px 12px;
        overflow: hidden;
    }
    .dfXXZw > button {
        color: rgb(255, 255, 255);
        white-space: nowrap;
        text-align: center;
        font-size: 14px;
        flex: 1 1 100%;
        outline: 0px;
        border-width: 0px;
        border-style: initial;
        border-color: initial;
        border-image: initial;
        margin: 0px;
        padding: 12px 8px;
    }
    .dfXXZw > button:first-child {
        background-color: rgb(1, 200, 187);
    }
    .dfXXZw > button:last-child {
        background-color: rgb(254, 201, 38);
    }
    .dfXXZw > button:first-child::before {
        content: "";
        width: 14px;
        height: 14px;
        margin-right: 12px;
        display: inline-block;
        vertical-align: middle;
        background-size: contain;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zd…MtNDUuOTk2LTEwMi43MzEtMTAyLjczMy0xMDIuNzMxeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==);
    }
    .fvatTI {
        display: flex;
        flex-direction: column;
        background-color: rgb(255, 255, 255);
        flex: 0 0 auto;
        border-bottom: 4px solid rgb(234, 234, 234);
    }
    .kXAQlX {
        display: flex;
        flex-wrap: nowrap;
        margin: 12px 24px;
    }
    .kXAQlX > span {
        font-size: 12px;
        display: inline-block;
        color: rgb(165, 165, 165);
        white-space: nowrap;
        vertical-align: middle;
        flex: 0 0 auto;
        margin: 0px 6px;
    }
    .kXAQlX > span {
        font-size: 12px;
        display: inline-block;
        color: rgb(165, 165, 165);
        white-space: nowrap;
        vertical-align: middle;
        flex: 0 0 auto;
        margin: 0px 6px;
    }
    .kXAQlX > span:nth-child(2) {
        color: rgb(24, 134, 211);
        white-space: normal;
        flex: 1 1 100%;
    }
    .cIjVfp {
        color: rgb(77, 177, 231);
        text-align: center;
        white-space: nowrap;
        font-size: 12px;
        border-width: 0px;
        border-style: initial;
        border-color: initial;
        border-image: initial;
        outline: none;
        background: transparent;
    }
</style>
