<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
    <link rel="stylesheet" href="./css/lottery.css">
</head>
<body class="flexWrap">
<div class="flexWrap">
    <div class="top bg text-c h-50 l-h-50">
        <a class="back-to-game c-w f-s-20 f-w-900" target="_blank" href="http://1681391.com">未结明细</a>
    </div>
    <div>
        <div class="table-header">
            <div class="col col2">注单号</div>
            <div class="col col3">类型</div>
            <div class="col col1-5">玩法</div>
            <div class="col col1-5">下注</div>
            <div class="col col1-5"> 可赢</div>
        </div>
    </div>
    <div class="flexContent">
        <div class="table-content text-c">
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649578</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 双</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div>10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649577</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 大</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div class="d-b">10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649578</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 双</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div>10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649577</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 大</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div class="d-b">10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649578</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 双</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div>10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649577</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 大</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div class="d-b">10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649578</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 双</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div>10.0</div>
                </div>
            </div>
            <div class="table-row">
                <div class="col col2">
                    <div class="green_color">190513494649577</div>
                    <div>2019-05-14</div>
                    <div>12:52:13</div>
                </div>
                <div class="col col3">
                    <div>广东快乐十分</div>
                    <div class="green_color">第 2019051415 期</div>
                    <div class="blue_color">盘口（A）</div>
                </div>
                <div class="col col1-5">
                    <div class="blue_color">第一球 大</div>
                    <div class="red_color"><span class="blue_color">@</span> 1.9999</div>
                </div>
                <div class="col col1-5">
                    <div class="text-c">10</div>
                </div>
                <div class="col col1-5 blue_color">
                    <div class="d-b">10.0</div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-footer pagination p-r">
        <div class="pagination">
            <div class="flex w-b100">
                <div class="flex1 p-r">
                    <span class="prev disabled"><a href=""></a></span>
                </div>
                <div class="flex1">
                    <span class="current">
                        <form class="page-jump">
                            <input type="text" placeholder="1" value="">
                        </form><span>/&nbsp;1&nbsp;页</span></span>
                </div>
                <div class="flex1 p-r">
                    <span class="next disabled"><a href=""></a></span>
                </div>
            </div>
        </div>
        <div class="m-t-5">
            <div class="footer-inline">
                <div class="title">注数</div>
                <span class="content">14</span></div>
            <div class="footer-inline">
                <div class="title">下注金额(计)</div>
                <span class="content">140</span></div>
            <div class="footer-inline">
                <div class="title">结果(总计)</div>
                <span class="content blue_color">139.8</span></div>
        </div>
    </div>
    <div style="height: 50px"></div>
     <jsp:include page="./include/footer.jsp"/>
   	 <jsp:include page="./include/menu.jsp"/>
</div>

<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="js/common.js"></script>
</body>
<style>
    .page-jump input {
        width: 25px;
        height: 23px;
        font-size: 0.875rem;
        font-weight: bold;
        text-align: center;
        box-sizing: border-box;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(204, 204, 204);
        border-image: initial;
        border-radius: 4px;
        margin: 0px;
        background: transparent;
    }
    .green_color {
        color: rgb(121, 199, 38) !important;
    }
    .blue_color {
        color: rgb(46, 105, 169) !important;
    }
    .red_color {
        color: rgb(255, 31, 31) !important;
    }
    .col {
        font-size: 0.6rem;
    }
    .table-header .col2 {
        flex: 2 1 0%;
    }
    .table-header .col3 {
        flex: 3 1 0%;
    }
    .col1-5 {
        flex: 1.5 1 0%;
    }
    .table-header {
        height: 30px;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        background-color: rgb(255, 255, 255);
        border-bottom: 1px solid rgb(234, 234, 234);
    }
    .table-header > div {
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        box-sizing: border-box;
        border-right: 1px solid rgb(234, 234, 234);
    }
    .table-content {
        overflow: auto;
        flex: 1 1 0%;
        height: 100%;
        background-color: rgb(235, 235, 235);
    }
    .table-content > .table-row {
        display: flex;
        width: 100%;
        height: auto;
        min-height: 50px;
        -webkit-box-align: center;
        align-items: center;
        box-sizing: border-box;
        background-color: rgb(255, 255, 255);
        border-bottom: 1px solid rgb(234, 234, 234);
        padding: 4px 0px;
    }
    .table-footer {
        height: 70px;
        display: block;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
        background-color: rgb(255, 255, 255);
        position: relative;
        border-top: 1px solid rgb(234, 234, 234);
    }
    .table-footer .footer-inline {
        height: 20px;
        line-height: 20px;
        box-sizing: border-box;
        display: flex;
        width: 100%;
        font-size: 0.75rem;
        padding: 0px 20px;
    }
    span.prev a, span.next a {
        width: 30px;
        height: 30px;
        color: rgb(255, 255, 255);
        font-size: 0px;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        background-image: url('./image/arrow-left.png');
        background-color: rgb(32, 97, 179);
        background-size: 70% 70%;
        border-radius: 4px;
        margin: auto;
        background-position: center center;
        background-repeat: no-repeat;
    }
    .dWynsh .page-navi span.disabled a {
        pointer-events: none;
        background-color: rgb(204, 204, 204);
        color: rgb(66, 66, 66);
    }

    .dWynsh .page-navi span {
        -webkit-box-pack: center;
        justify-content: center;
        position: relative;
        flex: 1 1 33%;
    }

    .table-footer .pagination {
        position: absolute;
        right: 0px;
        bottom: 0px;
        height: 40px;
        width: 200px;
    }

    .table-footer .footer-inline .title {
        width: 100px;
        color: rgb(170, 170, 170);
        display: block;
        flex: 1 1 30%;
        line-height: inherit;
        text-align: left;
        font-size: 14px;
        position: inherit;
        height: inherit;
        border: none;
        background: none;
    }

    .table-footer .footer-inline .content {
        width: calc(100% - 100px);
        display: block;
        color: rgb(95, 95, 95);
        z-index: 1;
        flex: 1 1 70%;
    }

    .table-footer .pagination {
        position: absolute;
        right: 0px;
        bottom: 0px;
        height: 40px;
        width: 200px;
    }
    span.current {
        display: flex;
        line-height: 40px;
    }
    span.disabled a {
        pointer-events: none;
        background-color: rgb(204, 204, 204);
        color: rgb(66, 66, 66);
    }
    span.next a {
        transform: scaleX(-1);
    }
</style>
</html>
