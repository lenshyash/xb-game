<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
</head>
<jsp:include page="include/head.jsp"></jsp:include>
<body class="flexWrap">
<div id="lottery_div_type_${bl.type }" class="flexWrap">
    <div class="top">
        <div class="showMenu c-w pull-l p-l-10"> <span class="arrow-white"></span></div>
        <div class="p-a p-l-r-10 b-1 h-20 center l-h-20 f-s-16" style="border-radius: 15px"><a href="${res}/tongji.html" class="c-w">统计</a></div>
        <div class="p-r pull-r c-w p-r-10">
            ￥2000.00
            <span class="icon arrow-white"></span>
            <div class="modal hide" style="right: 10px;top: 43px;">
                <div class="p-b-t-15 p-l-r-10 text-l f-s-14">
                    <p class="c-9 h-20 l-h-20">未结算金额:<span class="pull-r c-3 f-w-900"><span class="icon money">￥</span>0.0</span>
                    </p>
                    <p class="c-9 h-20 l-h-20">今日输赢:<span class="pull-r c-3 f-w-900"><span
                            class="icon money">￥</span>0.0</span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="yingkui flex text-c f-s-14 c-6 b-b hide">
        <div class="flex1 b-r p-b-t-5">
            <p>额度</p>
            <p class="blue_color f-s-16">2000.0</p>
        </div>
        <div class="flex1 b-r p-b-t-5">
            <p>未结算金额</p>
            <p class="blue_color f-s-16">2000.0</p>
        </div>
        <div class="flex1 p-b-t-5">
            <p>今日输赢</p>
            <p class="blue_color f-s-16">2000.0</p>
        </div>
    </div>
    <div class="showYingkui" style="background-color: rgb(235, 235, 235);">
        <div class="uq_icon">
            <div class="uq_icon2"></div>
        </div>
    </div>
    <div class="bDntQY">
    		<div class="p-l-10 csoaEf" id="lastkjdiv">
		</div>
        <div class="p-l-10 csoaEf" id="currentkjdiv">
            <div class="draw"><span>31059903</span>期</div>
            <div class="timer-wrapper">
                <div class="close">封盘: <span>00:18</span></div>
                <div class="open">开奖: <span>00:33</span></div>
                <div class="song"></div>
            </div>
            <audio id="song">
                <source src="" type="audio/mpeg">
            </audio>
        </div>
        <div class="rough_lines"></div>
    </div>
    <div class="rough_lines"></div>
    <div class="weui-tab flexContent bet" style="overflow: hidden">
    </div>
    <jsp:include page="../include/footer.jsp"/>
   	<jsp:include page="../include/menu.jsp"/>
   	<jsp:include page="./include/lotteryMenu.jsp"/>
   	
    <div id="cart" class="p-f p-l-r-10" style="display: none">
        <p class="selectCount"><span>已选1注</span></p>
        <div class="flex p-t-20 m-t-10 text-c b-b p-b-10">
            <div class="flex1 money"><span class="">5</span></div>
            <div class="flex1 money"><span>10</span></div>
            <div class="flex1 money"><span>20</span></div>
            <div class="flex1 money"><span>50</span></div>
            <div class="flex1 money"><span>100</span></div>
            <div class="flex1 f-s-16 c-6">编辑</div>
        </div>
        <div class="bets p-t-10 p-b-20">
            <input class="bets-input-value" placeholder="输入金额" min="0" type="number" pattern="\d*" step="1" value="">
            <input class="styled-checkbox" type="checkbox" id="enablePresetAmount" style="opacity: 0;width: 0">
            <label for="enablePresetAmount" class="p-r"></label>
            <p class="d-i f-s-12 p-l-r-5 v-m" style="width: 25px;height: 25px;line-height: 12px">预设金额</p>
            <a href="##" class="f-s-18 c-h">取消</a>
            <button class="result-btn">确认 0</button>
        </div>
    </div>
    <div id="cartConfirm" class="w-b100 h-b100 p-f" style="background-color: rgba(0,0,0,.7);display: none">
        <div class="w-b80 bg-w confirm">
            <div class="header text-c flex">
                <div class="flex1 f-s-16">极速时时彩</div>
                <div class="flex1 f-s-21">投注成功</div>
                <div class="flex1"></div>
            </div>
            <div class="table-wrapper">
                <div class="table ">
                    <div class="head f-s-16 c-6"><span>玩法</span><span>金额</span><span>可赢奖金</span></div>
                    <div class="body long ">
                        <!--<div class="flex">-->
                        <!--<span class="w-b50 c-b">-->
                        <!--<p class="">第一球 单</p>-->
                        <!--<p><span class="c-h">@ </span>1.995</p>-->
                        <!--</span>-->
                        <!--<span class="flex">10</span>-->
                        <!--<span class="w-b20">10.0</span>-->
                        <!--</div>-->
                    </div>
                    <div class="footer"><span class="count">总计注</span><span class="buyMoney"></span><span></span><span class="winMoney">180.5</span></div>
                </div>
            </div>
            <button class="confirm-btn">确认</button>
        </div>
    </div>
</div>
</body>
</html>
<script>
$(function () {
	initData();
	initLottery();
	$('.bet').on('click','.arrow-black,.upper-black',function(){ 
		$(this).toggleClass('arrow-black').toggleClass('upper-black').parents('.bet-panel-header').siblings().find('.b_btn').toggleClass('closeBox');
	}).on('click','.b_btn',function(){ 
		let money = $('.bets-input-value').val();
        $(this).toggleClass('active');
        let check = $('.flexContent .b_btn.active').length;
        $('.selectCount span').html('已选'+check+'注');
        check? $('#cart').show(): $('#cart').hide();
        $('.result-btn').html('确认 '+ (money * check))
	});
});

function initData(){
	$.ajax({
		url : "${m}/lotteryDown.do",
		data:{
			version:"${bl.identify}",
			lotCode:"${bl.code}"
		},
		success : function(result) {
			if(result && result.last && result.last.haoMa){
// 				result.last.haoMa = "01,02,03,04,05,06,07,08,09,10";
// 				result.last.haoMa = "01,05,06";
				var hmLst = result.last.haoMa.split(",");
				var hmFmt =[];
				var prefix = "kj_ks_";
				if('${bl.type}' == '8'){
					prefix = "div_color_";
				}
				for (var i = 0; i < hmLst.length; i++) {
					if(!isNaN(hmLst[i])){
						hmFmt.push(prefix+~~hmLst[i]);
					}
				}
				result.last['haoMaLst'] = hmLst;
				result.last['haoMaLstCss'] = hmFmt;
			}
			var html = template('tmp_kaijiang_top', result);
			$("#lastkjdiv").html(html);
			$("#currentkjdiv .draw span").html(result.current.qiHao);
		}
	});
}

function initLottery(){
	$.ajax({
		url : "${m}/getLotGamePlays.do",
		data:{
			lotCode:"${bl.code}"
		},
		success : function(result) {
			if(result){
				$(".showMenu").prepend(result.name);
				console.info(result);
				var data = initBodyData(result);
				console.info(data);
				var html = template('tmp_touzhu', data);
	 			$(".bet").append(html);
			}
		}
	});
}
</script>
<jsp:include page="include/touzhu/${bl.type}.jsp"></jsp:include>
<jsp:include page="include/kaijiang/${bl.type}.jsp"></jsp:include>