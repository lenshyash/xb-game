<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="lotteryMenu" class="flexContent w-b100 h-b100 p-f" style="background-color: rgba(0,0,0,.7);display: none">
    <div class="w-b80 bg-w pull-r p-l-r-10 p-b-t-20" style="min-height: 100%">
        <div class="game-div">
            <h3 class="game-title">
                <span>极速彩系列</span></h3>
            <div class="game-item-wrapper">
                <span class="game-item hot "><a href="./pk10.html" class="c-black d-b">北京赛车</a></span>
                <span class="game-item hot "><a href="./cqhlsx.html" class="c-black d-b">重庆欢乐生肖</a></span>
                <span class="game-item new "><a href="./dpc.html" class="c-black d-b">排列三</a></span>
                <span class="game-item "><a href="./lhc.html" class="c-black d-b">六合彩</a></span>
                <span class="game-item "><a href="syxw.html" class="c-black d-b">广东十一选五</a></span>
                <span class="game-item "><a href="./pcdd.html" class="c-black d-b">PC蛋蛋</a></span>
                <span class="game-item "><a href="./ks.html" class="c-black d-b">广西快三</a></span>
                <span class="game-item "><a href="./ssc.html" class="c-black d-b">新疆时时彩</a></span>
                <span class="game-item "><a href="klsf.html" class="c-black d-b   ">湖北快乐十分</a></span>
                <span class="game-item ">极速快乐十分</span>
                <span class="game-item new ">极速牛牛(娱乐)</span>
            </div>
        </div>
        <div class="game-div">
            <h3 class="game-title"><span>极速彩系列</span></h3>
            <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                    class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                    class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                    class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                    class="game-item new ">极速牛牛(娱乐)</span></div>
        </div>
        <div class="game-div">
            <h3 class="game-title"><span>极速彩系列</span></h3>
            <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                    class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                    class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                    class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                    class="game-item new ">极速牛牛(娱乐)</span></div>
        </div>
        <div class="game-div">
            <h3 class="game-title"><span>极速彩系列</span></h3>
            <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                    class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                    class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                    class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                    class="game-item new ">极速牛牛(娱乐)</span></div>
        </div>
    </div>
</div>