<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.num{
    height: 1.4rem;
    width: 1.5rem;
    line-height: 26px;
    text-indent: -3px;
}
.div_color_chat {
	text-shadow: rgb(0, 0, 0) 0px 0px 0px;
    color: rgb(8, 8, 8);
    font-style: normal;
}
</style>
<script>
	function initBodyData(result){
		var multiCode = ["heshu","yizi","erzi","erzidingwei","sanzidingwei","zuxuansan","zuxuanliu","kuadu"];
		var options = [ "总和", "亚军", "季军", "第四名", "第五名", "第六名", "第七名", "第八名", "第九名", "第十名" ];
		var zhenghe = ["万位","千位","百位","十位","个位"];
		var digit = ["万千","万百","万十","万个","千百","千十","千个","百十","百个","十个"];
		var thrSpec = ["前三","中三","后三"];
		var rules = [];
		var rules2 = [];
		var rules3 = [];
		var group;
		var ru;
		var ru2;
		var op;
		var r;
		if(result && result.rules){
			for (var y = 0; y < result.rules.length; y++) {
				rules2 = [];
				group = result.rules[y];
				rules2.code = group.code;
				rules2.name = group.name;
				var a = multiCode.indexOf(group.code);
				console.log(group.code);
				if(a==-1){
					for (var i = 0; i < result.rules[y].rules.length; i++) {
						ru = result.rules[y].rules[i];
						switch (ru.code){
							case "zhenghe":
								r={};
								r.name=ru.name;
								rules3 = [];
								var numOdd;
								for (var k = 0; k < ru.oddPlayItems.length; k++) {
									ru2 = ru.oddPlayItems[k];
									if(ru2.isNowYear == 1){
										rules3.push(ru2);
									}else if(ru2.markType == "shuzi"){
										numOdd = ru2;
									}
								}
								r.rules = rules3;
								rules2.rules = [];
								rules2.rules.push(r);
								for (var j = 0; j < zhenghe.length; j++) {
									op = zhenghe[j];
									r = {};
									r.name = op;
									r.isNum=true;
									rules3 = [];
									//添加1-9数字
									addNumOdds(9,numOdd,rules3);
									for (var k = 0; k < ru.oddPlayItems.length; k++) {
										ru2 = ru.oddPlayItems[k];
										if(ru2.isNowYear != 1 && ru2.markType == "qita"){
											rules3.push(ru2);
										}
									}
									r.rules = rules3;
									rules2.rules.push(r);
								}
								break;
							case "wanwei":
							case "qianwei":
							case "baiwei":
							case "shiwei":
							case "gewei":
								r={};
								r.name=ru.name;
								r.isNum=true;
								rules3 = [];
								for (var k = 0; k < ru.oddPlayItems.length; k++) {
									ru2 = ru.oddPlayItems[k];
									if(ru2.markType == "shuzi"){
										addNumOdds(9,ru2,rules3);
									}
								}
								r.rules = rules3;
								rules2.rules = [];
								rules2.rules.push(r);
								r = {};
								r.name = ru.name+" 双面盘";
								rules3 = [];
								for (var k = 0; k < ru.oddPlayItems.length; k++) {
									ru2 = ru.oddPlayItems[k];
									if(ru2.isNowYear != 1 && ru2.markType == "qita"){
										rules3.push(ru2);
									}
								}
								r.rules = rules3;
								rules2.rules.push(r);
								break;
							case "heweishu":
								rules2.rules = [];
								ru2 = ru.oddPlayItems[0];
								for (var j = 0; j < digit.length; j++) {
									op = digit[j];
									r = {};
									r.name = op;
									rules3 = [];
									playCompose1(ru2,rules3);
									r.rules = rules3;
									rules2.rules.push(r);
								}
								for (var j = 0; j < thrSpec.length; j++) {
									op = thrSpec[j];
									r = {};
									r.name = op;
									rules3 = [];
									playCompose1(ru2,rules3);
									r.rules = rules3;
									rules2.rules.push(r);
								}
								break;
							case "longhudou":
								rules2.rules = [];
								for (var j = 0; j < digit.length; j++) {
									op = digit[j];
									r = {};
									r.name = op;
									rules3 = [];
									for (var k = 0; k < ru.oddPlayItems.length; k++) {
										ru2 = ru.oddPlayItems[k];
										rules3.push(ru2);
									}
									r.rules = rules3;
									rules2.rules.push(r);
								}
								break;
						}
					}
					rules.push(rules2);
				}else{
					rules2.rules = [];
					var numOdds;
					for (var j = 0; j < result.rules[y].rules.length; j++) {
						r = {};
						r.name = result.rules[y].rules[j].name;
						r.isNum=true;
						rules3 = [];
						numOdds = result.rules[y].rules[j].oddPlayItems[0];
						switch(group.code){
							case "heshu":
							case "erzi":
							case "kuadu":
								for (var k = 0; k < result.rules[y].rules[j].oddPlayItems.length; k++) {
									ru2 = result.rules[y].rules[j].oddPlayItems[k];
									rules3.push(ru2);
								}
								break;
							case "yizi":
							case "zuxuansan":
							case "zuxuanliu":
								addNumOdds(9,numOdds,rules3);
								break;
							case "erzidingwei":
								addNumOdds(99,numOdds,rules3);
								break;
							case "sanzidingwei":
								addNumOdds(999,numOdds,rules3);
								break;
						}
						r.rules = rules3;
						rules2.rules.push(r);
					}
					rules.push(rules2);
				}
			}
		}
		var data ={"rules":rules};
		console.log(data);
		return data;
	}
	
	function addNumOdds(num,odd,rules3){
		var r;
		for (var  i = 0; i <= num; i++) {
			r = new Object;
			r.id = odd.id;
			r.name =i;
			r.odds = odd.odds;
			rules3.push(r);
			
		}
	}
	function playCompose1(odd,rules3){
		var nams = ["大","小","单","双","质","合"];
		var r;
		for (var  i = 0; i <= nams.length; i++) {
			r = new Object;
			r.id = odd.id;
			r.name =nams[i];
			r.odds = odd.odds;
			rules3.push(r);
		}
	}
	template.helper('$spliceNumCss', function (num) {
		debugger;
		if(isIntNum(num)){
			num = num.toString()
			if(num.length>1){
				return num.charAt(num.length - 1);
			}else{
				return num;
			}
			
		}else{
			return "chat";
		}
	});
	function isIntNum(val){
	    var regNeg = /^[0-9]+.?[0-9]*$/; // 负整数
	    if(regNeg.test(val)){
	        return true;
	    }else{
	        return false;
	    }
	}
</script>
<script type="text/html" id="tmp_touzhu">
<div class="weui-tab flexContent bet" style="overflow: hidden">
   <div class="weui-navbar">
	{{each rules as ru i }}
       <a class="weui-navbar__item {{if i==0}}weui-bar__item--on{{/if}}" href="#_{{ru.code}}">{{ru.name}}</a>
	{{/each}}
   </div>
   <div class="weui-tab__bd flexContent" style="max-height: 31rem  ">
	{{each rules as ru i }}
        <div id="_{{ru.code}}" class="weui-tab__bd-item {{if i==0}}weui-tab__bd-item--active{{/if}}">
			{{each ru.rules as r2 i2 }}
			<div class="bet-panel-wrapper">
                <div class="bet-panel-header">
                    <div class="title">{{r2.name}}<span class="arrow"></span> <span class="icon arrow-black"></span></div>
                </div>
                <div class="ReactCollapse--collapse">
                    <div class="ReactCollapse--content">
                        <div class="bet-panel-content">
                        		{{each r2.rules as odds i3 }}
							{{if !r2.isNum}}
                        			<div class="b_btn kEXwXI" data-name="{{odds.name }}" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
	                                <div class="gbajzm flex">
	                                    <span class="flex1 text-r p-r-10">{{odds.name }}</span><span class="b_odds flex1 text-l">{{odds.odds }}</span>
	                                </div>
	                            </div>
							{{/if}}
							{{if r2.isNum}}
								<div class="b_btn kEXwXI" data-name="{{odds.name }}" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
									<div class="gbajzm" >
                                     	<span class="num div_color_{{$spliceNumCss odds.name}}">{{odds.name }}</span><span class="b_odds p-l-5">{{odds.odds }}</span>
                                 	</div>
								</div>
							{{/if}}
                        		{{/each}}
                        </div>
                    </div>
                </div>
            </div>
		{{/each}}
        </div>
        {{/each}}
    </div>
</div>
</script>