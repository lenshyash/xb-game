<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script>
	function initBodyData(result){
		var options = [ "冠军", "亚军", "季军", "第四名", "第五名", "第六名", "第七名", "第八名", "第九名", "第十名" ];
		var rules = [];
		var rules2 = [];
		var rules3 = [];
		var ru;
		var ru2;
		var op;
		var r;
		if(result && result.rules){
			for (var y = 0; y < result.rules.length; y++) {
				for (var i = 0; i < result.rules[y].rules.length; i++) {
					ru = result.rules[y].rules[i];
					rules2 = [];
					if(ru.code == 'shuangmianpan'){
						r={};
						r.name=ru.name;
						rules3 = [];
						for (var k = 0; k < ru.oddPlayItems.length; k++) {
							ru2 = ru.oddPlayItems[k];
							if(ru2.isNowYear == 1){
								rules3.push(ru2);
							}
						}
						r.rules = rules3;
						rules2.rules = [];
						rules2.rules.push(r);
						
						for (var j = 0; j < options.length; j++) {
							op = options[j];
							r = {};
							r.name = op;
							rules3 = [];
							for (var k = 0; k < ru.oddPlayItems.length; k++) {
								ru2 = ru.oddPlayItems[k];
								if(j > 4 && (ru2.name == '龙' || ru2.name == '虎')){
									continue;
								}
								if(ru2.isNowYear != 1){
									rules3.push(ru2);
								}
							}
							r.rules = rules3;
							rules2.rules.push(r);
						}
					}else if(ru.code == 'danhao1-10'){
						rules2.rules = [];
						for (var j = 0; j < options.length; j++) {
							op = options[j];
							r = {};
							r.name = op;
							rules3 = [];
							for (var kk = 1; kk <= options.length; kk++) {
								for (var k = 0; k < ru.oddPlayItems.length; k++) {
									ru2 = ru.oddPlayItems[k];
									var a ={};
									a.name = kk;
									a.odds = ru2.odds;
									a.code = kk;
									a.id = ru2.id;
									rules3.push(a);
								}
							}
							r.rules = rules3;
							rules2.rules.push(r);
						}
					}else if(ru.code == 'guan-yajun'){
						r={};
						r.name=ru.name;
						rules3 = [];
						for (var k = 0; k < ru.oddPlayItems.length; k++) {
							ru2 = ru.oddPlayItems[k];
							rules3.push(ru2);
						}
						r.rules = rules3;
						rules2.rules = [];
						rules2.rules.push(r);
					}
					rules2.code = ru.code;
					rules2.name = ru.name;
					rules.push(rules2);
				}
			}
		}
		var data ={"rules":rules};
		return data;
	}
</script>
<script type="text/html" id="tmp_touzhu">
<div class="weui-tab flexContent bet" style="overflow: hidden">
   <div class="weui-navbar">
	{{each rules as ru i }}
       <a class="weui-navbar__item {{if i==0}}weui-bar__item--on{{/if}}" href="#_{{ru.code}}">{{ru.name}}</a>
	{{/each}}
   </div>
   <div class="weui-tab__bd flexContent" style="max-height: 31rem  ">
	{{each rules as ru i }}
        <div id="_{{ru.code}}" class="weui-tab__bd-item {{if i==0}}weui-tab__bd-item--active{{/if}}">
		{{each ru.rules as r2 i2 }}
			<div class="bet-panel-wrapper">
                <div class="bet-panel-header">
                    <div class="title">{{r2.name}}<span class="arrow"></span> <span class="icon arrow-black"></span></div>
                </div>
                <div class="ReactCollapse--collapse">
                    <div class="ReactCollapse--content">
                        <div class="bet-panel-content">
                        		{{each r2.rules as odds i3 }}
							{{if ru.code != 'danhao1-10'}}
                        			<div class="b_btn kEXwXI" data-name="{{odds.name }}" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
	                                <div class="gbajzm flex">
	                                    <span class="flex1 text-r p-r-10">{{odds.name }}</span><span class="b_odds flex1 text-l">{{odds.odds }}</span>
	                                </div>
	                            </div>
							{{/if}}
							{{if ru.code == 'danhao1-10'}}
								<div class="b_btn kEXwXI" data-name="{{odds.name }}" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
									<div class="gbajzm" >
                                     	<span class="num div_color_{{odds.name }}">{{odds.name }}</span><span class="b_odds p-l-5">{{odds.odds }}</span>
                                 	</div>
								</div>
							{{/if}}
                        		{{/each}}
                        </div>
                    </div>
                </div>
            </div>
		{{/each}}
        </div>
        {{/each}}
    </div>
</div>
</script>