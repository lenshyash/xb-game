<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script>
	function initBodyData(result){
		var rules = [];
		var rules2 = [];
		var rules3 = [];
		var ru;
		var ru2;
		var op;
		var r;
		if(result && result.rules){
			for (var y = 0; y < result.rules.length; y++) {
				for (var i = 0; i < result.rules[y].rules.length; i++) {
					ru = result.rules[y].rules[i];
					rules2 = [];
					rules2.rules = [];
					for (var k = 0; k < ru.oddPlayItems.length; k++) {
						ru2 = ru.oddPlayItems[k];
						rules3 = getRule(rules2.rules,ru2);
						if(!rules3){
							r={};
							r.name=ru2.name;
							if(ru2.isNowYear == 4){
								r.name = '点数';
							}
							if(ru2.isNowYear == 2 || ru2.isNowYear ==3 ){
								r.name = '围骰、全骰';
							}
							r.isNowYear=ru2.isNowYear;
							rules3 = [];
							r.rules = rules3;
							rules2.rules.push(r);
						}
						rules3.push(ru2);
					}
					rules2.code = ru.code;
					rules2.name = ru.name;
					rules.push(rules2);
				}
			}
		}
		var data ={"rules":rules};
		return data;
	}
	
	function getRule(rules,ru){
		for (var i = 0; i < rules.length; i++) {
			if(rules[i].isNowYear == ru.isNowYear){
				return rules[i].rules;
			}else if(rules[i].isNowYear == 2 && ru.isNowYear==3){
				return rules[i].rules;
			}
		}
		return null;
	}
	
	
</script>
<script type="text/html" id="tmp_touzhu">
<div class="weui-tab flexContent bet" style="overflow: hidden">
   <div class="weui-navbar">
	{{each rules as ru i }}
       <a class="weui-navbar__item {{if i==0}}weui-bar__item--on{{/if}}" href="#_{{ru.code}}">{{ru.name}}</a>
	{{/each}}
   </div>
   <div class="weui-tab__bd flexContent" style="max-height: 31rem  ">
	{{each rules as ru i }}
        <div id="_{{ru.code}}" class="weui-tab__bd-item {{if i==0}}weui-tab__bd-item--active{{/if}}">
		{{each ru.rules as r2 i2 }}
			<div class="bet-panel-wrapper">
                <div class="bet-panel-header">
                    <div class="title">{{r2.name}}<span class="arrow"></span> <span class="icon arrow-black"></span></div>
                </div>
                <div class="ReactCollapse--collapse">
                    <div class="ReactCollapse--content">
                        <div class="bet-panel-content">
                        		{{each r2.rules as odds i3 }}
								{{if odds.isNowYear == 1}}
								<div class="b_btn kEXwXI" data-name="1" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="2" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="3" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="4" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="5" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="6" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="大" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <span class="flex1 text-r p-r-5">大 </span><span class="b_odds flex1 text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								<div class="b_btn kEXwXI" data-name="小" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <span class="flex1 text-r p-r-5">小 </span><span class="b_odds flex1 text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								{{/if}}
								{{if odds.isNowYear == 6}}
								<div class="b_btn kEXwXI" data-name="1-1" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi one"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="2-2" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi two"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="3-3" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><div class="touzi three"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="4-4" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi four"> &nbsp;</div><div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="5-5" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi five"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="6-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi six"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								{{/if}}
								{{if odds.isNowYear == 5}}
								<div class="b_btn kEXwXI" data-name="1-2" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi two"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="1-3" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi three"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="1-4" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="1-5" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="1-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="2-3" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi three"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="2-4" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="2-5" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="2-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="3-4" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="3-5" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="3-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="4-5" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi four"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="4-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi four"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI"data-name="5-6" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <div class="touzi five"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								{{/if}}
								{{if odds.isNowYear == 2 || odds.isNowYear == 3}}
								{{if odds.isNowYear == 2}}
								<div class="b_btn kEXwXI" data-name="111" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi one"> &nbsp;</div><div class="touzi one"> &nbsp;</div><div class="touzi one"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="222" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi two"> &nbsp;</div><div class="touzi two"> &nbsp;</div><div class="touzi two"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="333" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi three"> &nbsp;</div><div class="touzi three"> &nbsp;</div><div class="touzi three"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="444" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi four"> &nbsp;</div><div class="touzi four"> &nbsp;</div><div class="touzi four"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="555" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi five"> &nbsp;</div><div class="touzi five"> &nbsp;</div><div class="touzi five"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
                                <div class="b_btn kEXwXI" data-name="666" data-odds="{{odds.odds}}">
                                    <div class="gbajzm">
                                        <div class="touzi six"> &nbsp;</div><div class="touzi six"> &nbsp;</div><div class="touzi six"> &nbsp;</div><span class="b_odds text-l">{{odds.odds}}</span>
                                    </div>
                                </div>
								{{/if}}
									{{if odds.isNowYear == 3}}
									<div class="b_btn kEXwXI" data-name="全骰" data-odds="{{odds.odds}}">
                                    		<div class="gbajzm">
                                        		<span class="flex1 text-r p-r-5">全骰</span><span class="b_odds flex1 text-l">{{odds.odds}}</span>
                                    		</div>
                                		</div>
									{{/if}}
								{{/if}}
								{{if odds.isNowYear == 4}}
								<div class="b_btn kEXwXI" data-name="{{odds.name}}" data-code="{{odds.code}}" data-id="{{odds.id}}" data-odds="{{odds.odds }}">
                                    <div class="gbajzm">
                                        <span class="flex1 text-r p-r-5">{{odds.name}} </span><span class="b_odds flex1 text-l">{{odds.odds }}</span>
                                    </div>
                                </div>
								{{/if}}
								
                        		{{/each}}
                        </div>
                    </div>
                </div>
            </div>
		{{/each}}
        </div>
        {{/each}}
    </div>
</div>
</script>