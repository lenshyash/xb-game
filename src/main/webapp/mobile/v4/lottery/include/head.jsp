<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="${res }/css/weui.min.css">
<link rel="stylesheet" href="${res }/css/jquery-weui.css">
<link rel="stylesheet" href="${res }/css/common.css">
<link rel="stylesheet" href="${res }/css/lottery.css?v=5">
<script src="${res }/js/jquery.min.js"></script>
<script src="${res }/js/jquery-weui.min.js"></script>
<script src="${res }/js/common.js"></script>
<script src="${base}/common/js/artTemplate/template.js"></script>
<script src="${res }/js/core.js""></script>
<script>
    $(function () {
        $('.arrow-black').click(function () {
            $(this).toggleClass('arrow-black').toggleClass('upper-black').parents('.bet-panel-header').siblings().find('.b_btn').toggleClass('closeBox');
        });
        // 显示隐藏侧边栏
        $('.showMenu').click(function () {
            $('#lotteryMenu').show().removeClass('fadeInRightOut').addClass('fadeInRight');
        });
        $('#lotteryMenu').click(function () {
            $(this).removeClass('fadeInRight').addClass('fadeInRightOut');
            setTimeout(function () {
                $('#lotteryMenu').hide()
            }, 200)
        });

        $('.showYingkui').click(function () {
            $('.yingkui').toggleClass('hide');
            $('.uq_icon div').toggleClass('uq_icon2')
        })
    })
</script>