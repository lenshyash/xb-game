<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
</head>
<body class="flexWrap">
<div id="history" class="">
    <div class="top bg text-c h-50 l-h-50">
        <a class="back-to-game c-w f-s-20 f-w-900" target="_blank" href="http://1681391.com">报表查询</a>
    </div>
    <div class="flex choiceKj b-b">
        <div class="flex1 p-l-r-10 jnQSGx p-r">
            <input tabindex="1" type="date">
        </div>
        <div class="flex1 p-l-r-10 jnQSGx p-r">
            <input tabindex="1" type="date">
        </div>
    </div>
    <div class="fieldset button small p-l-r-10">
        <button class="field-button">查询</button>
    </div>
    <div class="content flexWrap">
        <div class="rough_lines"></div>
        <div class="table history">
            <div class="table-header">
                <div class="col col3">日期</div>
                <div class="col col3">注数</div>
                <div class="col col2">下注金额</div>
                <div class="col col2">盈亏</div>
            </div>
            <div class="table-content flexContent">
                <div class="no-data">☹&nbsp;暂无数据</div>
            </div>
            <div class="rough_lines"></div>
            <div class="table-footer">
                <div class="col col3">总计</div>
                <div class="col col1">0</div>
                <div class="col col2">0.0</div>
                <div class="col col2"><span class="blue_color">0.0</span></div>
            </div>
        </div>
    </div>
    <div style="height: 72px;"></div>
    <jsp:include page="./include/footer.jsp"/>
   	<jsp:include page="./include/menu.jsp"/>
</div>
<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
<style>
    #history{
        height: 100%;
        display: flex;
        flex-direction: column;
    }
    .top {
        position: relative;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 52px;
        line-height: 52px;
        background-image: linear-gradient(to right, rgb(124, 201, 254), rgb(27, 82, 149));
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px 0px;
        z-index: 10;
    }
    .choiceKj > div {
        box-sizing: border-box;
        height: 100%;
        text-align: center;
        flex: 1 1 50%;
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: justify;
        justify-content: space-between;
        padding: 0px 10px;
        border-right: 1px solid rgb(234, 234, 234);
    }
    .jnQSGx input {
        color: rgb(102, 102, 102);
        font-weight: 400;
        font-size: 0.8125rem;
        background-color: rgba(255, 255, 255, 0);
        -webkit-appearance: none;
        height: 100%;
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
    }
    .jnQSGx::after {
        content: "";
        position: absolute;
        height: 20px;
        width: 20px;
        top: 10px;
        right: 10px;
        background: url(./image/date.png) center center / contain no-repeat;
    }
    .choiceKj {
        height: 45px;
        background-color: rgb(255, 255, 255);
        display: flex;
    }
    .field-button {
        display: block;
        height: 40px;
        line-height: 40px;
        text-align: center;
        width: 100%;
        color: rgb(255, 255, 255);
        font-size: 0.875rem;
        border-width: 0px;
        border-style: initial;
        border-color: initial;
        border-image: initial;
        border-radius: 40px;
        background: linear-gradient(to right, rgb(125, 202, 255) 0%, rgb(26, 81, 148) 100%);
        display: table-cell;
        vertical-align: middle;
    }
    .fieldset.button.small {
        height: 4rem;
        line-height: 4rem;
    }
    .table .col3 {
        flex: 3 1 0%;
    }
    .table .col2 {
        flex: 2 1 0%;
    }
    .table .col {
        font-size: 0.625rem;
    }
    .content {
        position: relative;
        flex: 1 1 100%;
        overflow: hidden;
    }
    .table .table-header {
        height: 1.8rem;
        line-height: 1.8rem;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        background-color: rgb(255, 255, 255);
        border-bottom: 1px solid rgb(234, 234, 234);
        text-align: center;
    }
    .no-data {
        height: 50px;
        line-height: 50px;
        font-size: 0.875rem;
        text-align: center;
        background-color: rgb(255, 255, 255);
        color: rgb(170, 170, 170);
    }
    .table {
        -webkit-box-flex: 1;
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        height: 100%;
    }
    .table .table-footer {
        display: flex;
        height: 2rem;
        line-height: 2rem;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
        background-color: rgb(255, 255, 255);
        position: relative;
        border-top: 1px solid rgb(234, 234, 234);
        text-align: center;
        font-weight: bold;
    }
    .table .table-content {
        height: 100%;
        background-color: rgb(235, 235, 235);
        overflow: auto;
        flex: 1 1 0%;
    }
</style>
</html>
<script>
    // 设置日期为今天
    let d = new Date();
    let month = ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1));
    let day = (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
    let today = d.getFullYear() + '-' + month + '-' + day;
    $('input[type=date]').val(today);
</script>
