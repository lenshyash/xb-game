<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
</head>
<body class="flexWrap">
<div class="flexWrap">
    <div class="top bg text-c h-50 l-h-50">
        <div onclick="goBack()" class="showMenu c-w p-a p-l-10 p-t-15"><span class="left-white"></span></div>
        <a class="back-to-game c-w f-s-20 f-w-900" target="_blank" href="http://1681391.com">优惠活动</a>
    </div>
    <div class="m-b-15 b-r-6 c-6 flexContent">
        <div class="weui-tab flexWrap">
            <div class="weui-navbar">
                <a class="weui-navbar__item weui-bar__item--on c-w" href="#tab1">
                    全部
                </a>
                <a class="weui-navbar__item c-w" href="#tab2">
                    存提送
                </a>
                <a class="weui-navbar__item c-w" href="#tab3">
                    彩票券
                </a>
                <a class="weui-navbar__item c-w" href="#tab4">
                    彩票券
                </a>
            </div>
            <div class="weui-tab__bd ">
                <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active p-l-r-30 p-b-t-15">
                    <p>111111</p>
                    <p style="height: 1000px"></p>
                    <p>22222</p>
                </div>
                <div id="tab2" class="weui-tab__bd-item p-l-r-30 p-b-t-15">
                    页面2
                </div>
                <div id="tab3" class="weui-tab__bd-item">
                    页面2
                </div>
                <div id="tab4   " class="weui-tab__bd-item">
                    页面4
                </div>
            </div>
        </div>
    </div>
    <div style="height: 50px"></div>
    <jsp:include page="./include/footer.jsp"/>
   	<jsp:include page="./include/menu.jsp"/>
</div>


<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
</html>
<style>
    .top {
        position: relative;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 52px;
        line-height: 52px;
        background-image: linear-gradient(to right, rgb(124, 201, 254), rgb(27, 82, 149));
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px 0px;
        z-index: 10;
    }
    .weui-navbar {
        background-image: linear-gradient(to right, rgb(124, 201, 254), rgb(27, 82, 149));
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px 0px;
    }
    .weui-navbar__item.weui-bar__item--on {
        background: none !important;
        color: #fff;
        border-bottom: 3px solid #fff;
    }
    .affiliate-title {
        height: 48px;
        line-height: 48px;
        background-color: rgb(255, 255, 255);
        text-align: left;
        color: rgb(95, 95, 95);
        font-family: Tahoma;
        font-size: 13px;
        font-weight: bold;
        text-indent: 16px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(242, 243, 245);
        border-image: initial;
    }
    .weui-navbar+.weui-tab__bd {
        padding-top: 35px;
    }
    .feedback-contact-info {
        margin-top: 8px;
        background-color: rgb(255, 255, 255);
    }
    .feedback-contact-info .feedback-group {
        height: 40px;
        line-height: 40px;
        font-size: 0.75rem;
        padding: 0px 20px;
    }
    .feedback-contact-info .feedback-group:not(:last-child) {
        border-bottom: 1px solid rgb(234, 234, 234);
    }
    .feedback-contact-info .feedback-group .feedback-label {
        float: left;
        color: rgb(178, 178, 178);
    }
    .feedback-contact-info .feedback-group .feedback-content {
          float: right;
          color: rgb(46, 105, 169);
    }
    .switch {
        display: flex;
        background-color: rgb(255, 255, 255);
        list-style: none;
        margin: 0px;
        padding: 0px;
    }
    .switch li {
        display: block;
        height: 75px;
        position: relative;
        flex: 1 1 33%;
    }
    .switch li:not(:last-child) {
        border-right: 1px solid rgb(234, 234, 234);
    }
    .switch li a {
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
    }
    .switch li a div {
        width: 35px;
        height: 35px;
        margin: 15px auto;
    }
    .switch li a div.wechat {
        background: url(./image/ic_wechat.png) center center / contain no-repeat;
    }
    .switch li a div.qq {
        background: url(./image/ic_qq.png) center center / contain no-repeat;
    }
    .switch li a span {
        display: block;
        position: absolute;
        bottom: 6px;
        left: 0px;
        font-size: 0.75rem;
        width: 100%;
        text-align: center;
    }
    .support-time {
        margin-top: 8px;
        background-color: rgb(255, 255, 255);
        box-sizing: border-box;
        width: 100%;
        padding: 13px 24px;
        overflow: hidden;
    }
    .support-time p {
        text-align: center;
        font-size: 0.75rem;
        font-weight: normal;
        color: rgb(95, 95, 95);
        margin: 0px;
    }
    .weui-navbar {
        display: -webkit-box;
        display: -webkit-flex;
        display: flex;
        position: fixed;
        z-index: 500;
        top: 52px;
        width: 100%;
        background-color: #fafafa;
    }
</style>
