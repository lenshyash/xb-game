var base;
$(function() {// 初始化内容
	show('pk10','SFSC');
});  
let show = function(lot,code){
	var loading = false,itemsPerLoad = 20,page=1,totalCount = 0;
	base = $("#base").val();
	$.ajax({
		url:base+ "/mobile/v4/draw_notice_details_data.do",
		data:{lotCode:code,page:page,rows:itemsPerLoad},
		dataType:'json',
		type:'get',
		success:function(data){
			if(data.success){
				if(lot=='pk10' || lot=='ssc' || lot=='lhc' || lot=='kuaisan' || lot=='dpc' || lot=='syxw' || lot=='klsf' || lot=='pcdd'){
					var html = template(lot+'Tpl',data);
					$('.flexContent .'+lot+'').empty();
					$('.flexContent .'+lot+'').append(html);
				}
			}
		}
	})
	template.helper('$gyhSum', function (haoma,type) {
		if(type==1){
			return parseInt(haoma[0])+parseInt(haoma[1]);
		}else if(type==2){
			return parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);
		}else if(type==3){
			return parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6]);
		}else if(type==4){
			return parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2]);
		}else if(type==5){
			return parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7]);
		}
		
		});
	
	template.helper('$spliceNum', function (num) {
		return parseInt(num);
	});
		
	template.helper('$teShuWanFa', function (haoma,num,type) {
		if(num==1){
	        var n1=parseInt(haoma[0]);
	        var n2=parseInt(haoma[1]);
	        var n3=parseInt(haoma[2]);
		}else if(num==2){
	        var n1=parseInt(haoma[1]);
	        var n2=parseInt(haoma[2]);
	        var n3=parseInt(haoma[3]);
		}else if(num==3){
	        var n1=parseInt(haoma[2]);
	        var n2=parseInt(haoma[3]);
	        var n3=parseInt(haoma[4]);
		}
		
        if((n1==0 || n2==0 || n3==0) && (n1==9 || n2==9 || n3==9)){
            if(n1==0){
                n1=10;
            }
            if(n2==0){
                n2=10;
            }
            if(n3==0){
                n3=10;
            }
        }

        if(n1==n2 && n2==n3){
            return type==1?"豹子":"c-r f-w-900 f-s-20";
        }else if((n1==n2) || (n1==n3) || (n2==n3)){
            return type==1?"对子":"f-w-900 f-s-20 c-g";
        }else if((n1==10 || n2==10 || n3==10) && (n1==9 || n2==9 || n3==9) && (n1==1 || n2==1 || n3==1)){
            return type==1?"顺子":"f-w-900 f-s-20 c-b";
        }else if( ( (Math.abs(n1-n2)==1) && (Math.abs(n2-n3)==1) ) || ((Math.abs(n1-n2)==2) && (Math.abs(n1-n3)==1) && (Math.abs(n2-n3)==1)) ||((Math.abs(n1-n2)==1) && (Math.abs(n1-n3)==1)) ){
            return type==1?"顺子":"f-w-900 f-s-20 c-b";
        }else if((Math.abs(n1-n2)==1) || (Math.abs(n1-n3)==1) || (Math.abs(n2-n3)==1)){
            return type==1?"半顺":"f-w-900 f-s-20 p-l-r-5 c-o";
        }else{
            return type==1?"杂六":"f-w-900 f-s-20 c-red";
        }
		});
	
	template.helper('$gyhKd', function (haoma,num,type) {
		var ary;
		
		switch (num) {
		case 1:	
			ary = [parseInt(haoma[0]),parseInt(haoma[1]),parseInt(haoma[2])];
	        var maxN = Math.max.apply(null,ary);
	        var minN = Math.min.apply(null,ary);
			break;
		case 2:
			ary = [parseInt(haoma[1]),parseInt(haoma[2]),parseInt(haoma[3])];
	        var maxN = Math.max.apply(null,ary);
	        var minN = Math.min.apply(null,ary);
			break;
		case 3:
			ary = [parseInt(haoma[2]),parseInt(haoma[3]),parseInt(haoma[4])];
	        var maxN = Math.max.apply(null,ary);
	        var minN = Math.min.apply(null,ary);
			break;

		default:
			break;
		}
		
		if(type==1){
			return maxN-minN;
		}
		
		var sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2]);
		if(type==0){
			return sum;
		}else if(type==1){
			var ary = [parseInt(haoma[0]),parseInt(haoma[1]),parseInt(haoma[2])];
	        var maxN = Math.max.apply(null,ary);
	        var minN = Math.min.apply(null,ary);
	        return maxN-minN;
		}else if(type==2){
			return sum>=10?'大':'小';
		}else if(type==3){
			return sum>=10?'blueBall':'originBall';
		}
		});
	
	template.helper('$lhcTeMaDs', function (num,type) {	
		
			var num = parseInt(num);
			if(num==49){
				//return "和"
				if(type==1){
					return "和"
				}else {
					return "lhc-g o-r-6 c-w p-l-r-5"
				}
			}else if(num%2!=0){
				if(type==1){
					return "单"
				}else {
					return "lhc-b o-r-6 c-w p-l-r-5"
				}
			}else{
				if(type==1){
					return "双"
				}else {
					return "lhc-y o-r-6 c-w p-l-r-5"
				}
			}
		});
	
	template.helper('$lhcTeMaDx', function (num,type) {	
		
		var num = parseInt(num);
		if(num==49){
			//return "和"
			if(type==1){
				return "和"
			}else {
				return "lhc-g g-r-6 c-w  m-l-r-10 p-l-r-5"
			}
		}else if(num>=1 && num<=24){
			if(type==1){
				return "小"
			}else {
				return "lhc-y g-r-6 c-w  m-l-r-10 p-l-r-5"
			}
		}else{
			if(type==1){
				return "大"
			}else {
				return "lhc-b g-r-6 c-w  m-l-r-10 p-l-r-5"
			}
		}
	});
	
	template.helper('$lhcTeMaHeDs', function (num,type) {	
		var he = parseInt(num.charAt(0))+parseInt(num.charAt(1));
		
		if(num==49){
			if(type==1){
				return "和"
			}else {
				return "lhc-g b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else if(he%2!=0){
			if(type==1){
				return "和单"
			}else {
				return "lhc-b b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else{
			if(type==1){
				return "和双"
			}else {
				return "lhc-y b-r-6 c-w m-r-5 p-l-r-5"
			}
		}
	});
	
	template.helper('$lhcTeMaHeDx', function (num,type) {	
		var he = parseInt(num.charAt(0))+parseInt(num.charAt(1));
		
		if(num==49){
			//return "和"
			if(type==1){
				return "和"
			}else {
				return "lhc-g b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else if(he>=7){
			if(type==1){
				return "和大"
			}else {
				return "lhc-b b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else{
			if(type==1){
				return "和小"
			}else {
				return "lhc-y b-r-6 c-w m-r-5 p-l-r-5"
			}
		}
	});
	
	template.helper('$lhcTeMaWeiDx', function (num,type) {	
		
		var wei= num.charAt(num.length-1);
		if(num==49){
		
			if(type==1){
				return "和"
			}else {
				return "lhc-g b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else if(wei>=5 && wei<=9){
			if(type==1){
				return "尾大"
			}else {
				return "lhc-b b-r-6 c-w m-r-5 p-l-r-5"
			}
		}else{
			if(type==1){
				return "尾小"
			}else {
				return "lhc-y b-r-6 c-w m-r-5 p-l-r-5"
			}
		}	
	});
	
	template.helper('$gyhWeiDx', function (haoma,num,type) {
		var sum;
		switch (num) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);		
			break;
		case 2:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7]);
			break;
		}
		var sum = sum.toString();
		var wei= sum.charAt(sum.length-1);
		
		if(wei>=5 && wei<=9){
				
			if(type==1){
				return "尾大"
			}else if(type==2){
				return "f-w-900 f-s-20 c-b"
			}else if(type==3){
				return "c-b f-w-900 f-s-20"
			}else if(type==4){
				return "blueBall"
			}
		}else{
			if(type==1){
				return "尾小"
			}else if(type==2){
				return "f-w-900 f-s-20 c-orange"
			}else if(type==3){
				return "c-o f-w-900 f-s-20"
			}else if(type==4){
				return "originBall"
			}
		}
	});
	
	template.helper('$gyhWeiDx2', function (num,type) {
		var sum;
		sum=num;
		var sum = sum.toString();
		var wei= sum.charAt(sum.length-1);
		
		if(wei>=5 && wei<=9){
				
			if(type==1){
				return "尾大"
			}else if(type==2){
				return "f-w-900 f-s-20 c-b"
			}else if(type==3){
				return "c-b f-w-900 f-s-20"
			}else if(type==4){
				return "blueBall"
			}
		}else{
			if(type==1){
				return "尾小"
			}else if(type==2){
				return "f-w-900 f-s-20 c-orange"
			}else if(type==3){
				return "c-o f-w-900 f-s-20"
			}else if(type==4){
				return "originBall"
			}
		}
	});
	
	
	
	template.helper('$kuaiSanNum', function (num,type) {
		if(num==1){
			return type==1?'one':'魚'
		}
		if(num==2){
			return type==1?'two':'虾'
		}
		if(num==3){
			return type==1?'three':'葫芦'
		}
		if(num==4){
			return type==1?'four':'铜钱'
		}
		if(num==5){
			return type==1?'five':'蟹'
		}
		if(num==6){
			return type==1?'six':'鸡'
		}
	});
	
	template.helper('$gyhDs', function (haoma,num,type) {
		var sum;
		switch (num) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1]);
			break;
		case 2:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7]);
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2]);
			break;
		case 5:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7]);
			sum = sum.toString();
			
		    var wei= sum.charAt(sum.length-1);
		    var tou= sum.charAt(0);
		    
		    sum = wei+tou;
			break;
		case 6:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);
			sum = sum.toString();
			
		    var wei= sum.charAt(sum.length-1);
		    var tou= sum.charAt(0);
		    
		    sum = wei+tou;
			break;
		}
		
		if(sum%2!=0){
			if(type==1){
				return "单"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "blueBall"
			}else if(type==4){
				return "flex1 lhc-b b-r-6 c-w"
			}else if(type==5){
				return "和单"
			}
		}else {
			if(type==1){
				return "双"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "originBall"
			}else if(type==4){
				return "flex1 lhc-y b-r-6 c-w"
			}else if(type==5){
				return "和双"
			}
		}
	});
	
	template.helper('$gyhDs2', function (num,type) {
		var sum;
		num=num.toString();
		var wei= num.charAt(num.length-1);
		var tou = num.charAt(0);
		sum= parseInt(wei)+parseInt(tou);
		if(sum%2!=0){
			if(type==1){
				return "和单"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "blueBall"
			}
		}else {
			if(type==1){
				return "和双"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "originBall"
			}
		}
	});
	
	template.helper('$gyhDsYouHe', function (haoma,num,he,type) {
		var sum;
		switch (num) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1]);
			break;
		case 2:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7]);
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2]);
			break;
		}
		if(sum==he){
			if(type==1){
				return "和"
			}else if(type==2){
				return "greenBg"
			}else if(type==3){
				return "greenBall"
			}
		}else if(sum%2!=0){
			if(type==1){
				return "单"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "blueBall"
			}
		}else {
			if(type==1){
				return "双"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "originBall"
			}
		}
	});
	
	template.helper('$gyhDx', function (haoma,max,type,type2) {
		var sum;
		
		switch (type2) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1]);
			break;
		case 2:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7])
			break;
		case 5:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);
			
			sum = sum.toString();
			
		    var wei= sum.charAt(sum.length-1);
		    var tou= sum.charAt(0);
		    
		    sum = wei+tou;
			break;
		}
		
		if(sum>=max){
			if(type==1){
				return "大"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "blueBall"
			}else if(type==4){
				return "flex1 lhc-b b-r-6 c-w m-l-r-10"
			}
		}else{
			if(type==1){
				return "小"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "originBall"
			}else if(type==4){
				return "flex1 lhc-y b-r-6 c-w  m-l-r-10"
			}
		}
	});
	
	template.helper('$gyhDx2', function (num,max,type) {
		
		
	
		
		var wei= num.charAt(num.length-1);
		var tou = num.charAt(0);
		
		var sum = parseInt(wei)+parseInt(tou);
		
		if(sum>=max){
			if(type==1){
				return "和大"
			}else if(type==2){
				return "blueBall"
			}
		}else {
			if(type==1){
				return "和小"
			}else if(type==2){
				return "originBall"
			}
		}

		
	});
	
	template.helper('$kuaiSanDx', function (haoma,max,type,type2) {
		var sum;
		
		switch (type2) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])
			break;
		}
		var num1 = parseInt(haoma[0]);
		var num2 = parseInt(haoma[1]);
		var num3 = parseInt(haoma[2]);
		
		//通吃情况 进入这个if直接return了 
		if(num1==num2 && num2==num3){
			if(type==1){
				return "通吃"
			}else if(type==2){
				return "greenBall"
			}
		}
		
		if(sum>=max){
			if(type==1){
				return "大"
			}else if(type==2){
				return "blueBall"
			}
		}else{
			if(type==1){
				return "小"
			}else if(type==2){
				return "originBall"
			}
		}
	});
	
	template.helper('$gyhDxYouHe', function (haoma,he,type,type2) {
		var sum;
		
		switch (type2) {
		case 1:
			sum = parseInt(haoma[0])+parseInt(haoma[1]);
			break;
		case 2:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4]);
			break;
		case 3:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])
			break;
		case 4:
			sum = parseInt(haoma[0])+parseInt(haoma[1])+parseInt(haoma[2])+parseInt(haoma[3])+parseInt(haoma[4])+parseInt(haoma[5])+parseInt(haoma[6])+parseInt(haoma[7])
			break;
		}
		
		if(sum==he){
			if(type==1){
				return "和"
			}else if(type==2){
				return "greenBg"
			}else if(type==3){
				return "greenBall"
			}
		}	
		else if(sum>he){
			if(type==1){
				return "大"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "blueBall"
			}
		}else{
			if(type==1){
				return "小"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "originBall"
			}
		}
	});
	
	template.helper('$gyhZh', function (num,type) {
		if(type==0){
			if(num==1 || num==2 || num==3 || num==5 || num==7){
				return "质";
			}else{
				return "和";
			}
		}else if(type==1){
			if(num==1 || num==2 || num==3 || num==5 || num==7){
				return "blueBall";
			}else{
				return "originBall";
			}
		}
	});
	

	
    function subtractYear(year) {
        var jiaziYear = 1804;
        if (year < jiaziYear) {// 如果年份小于起始的甲子年(startYear = 1804),则起始甲子年往前偏移
            jiaziYear = jiaziYear - (60 + 60 * ((jiaziYear - year) / 60));// 60年一个周期
        }
        return year - jiaziYear;
    }
	
    function getHaoMaStrByAge(age) {
        if (age == 12)
            return "02,14,26,38";
        if (age == 11)
            return "03,15,27,39";
        if (age == 10)
            return "04,16,28,40";
        if (age == 9)
            return "05,17,29,41";
        if (age == 8)
            return "06,18,30,42";
        if (age == 7)
            return "07,19,31,43";
        if (age == 6)
            return "08,20,32,44";
        if (age == 5)
            return "09,21,33,45";
        if (age == 4)
            return "10,22,34,46";
        if (age == 3)
            return "11,23,35,47";
        if (age == 2)
            return "12,24,36,48";
        if (age == 1)
            return "01,13,25,37,49";
        return null;
    }
    
	template.helper('$shengXiao', function (haoma) {
        var myDate = new Date();
        var year = myDate.getFullYear();
        
        var ZODIACS = ["鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪"];
        
        var ary = [];
        for (var i = 0; i < 12; i++) {
            var w = subtractYear(year + i);
            var aa = ZODIACS[(w % 12)];
            var bb = getHaoMaStrByAge(i + 1);
            ary.push(bb,aa);
        }
        
        var num =  haoma;
        if (ary[0].indexOf(num)!=-1) {
            return ary[1]
        }else if(ary[2].indexOf(num)!=-1){
            return ary[3]
        }else if(ary[4].indexOf(num)!=-1){
            return ary[5]
        }else if(ary[6].indexOf(num)!=-1){
            return ary[7]
        }else if(ary[8].indexOf(num)!=-1){
            return ary[9]
        }else if(ary[10].indexOf(num)!=-1){
            return ary[11]
        }else if(ary[12].indexOf(num)!=-1){
            return ary[13]
        }else if(ary[14].indexOf(num)!=-1){
            return ary[15]
        }else if(ary[16].indexOf(num)!=-1){
            return ary[17]
        }else if(ary[18].indexOf(num)!=-1){
            return ary[19]
        }else if(ary[20].indexOf(num)!=-1){
            return ary[21]
        }else if(ary[22].indexOf(num)!=-1){
            return ary[23]
        }
        
	});
	
	template.helper('$yanSe', function (haoma) {
        // 红波
        var COLOR_RED =[ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ];
        // 蓝波
        var COLOR_BLUE = [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ];
        // 绿波
        var COLOR_GREEN = [5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ];

        var num = parseInt(haoma);
         if ( COLOR_RED.indexOf(num)!=-1){
            return "redBall"
         }else if(COLOR_BLUE.indexOf(num)!=-1){
        	 return "blueBall"
         }else {
        	 return "greenBall"
         }
	});
	
	template.helper('$yanSe2', function (haoma) {
        // 红波
        var COLOR_RED =[ 3 , 6 , 9 , 12 , 15 , 18 , 21 , 24 ];
        // 绿波
        var COLOR_GREEN = [ 1 , 4 , 7 , 10 , 16 , 19 , 22 , 25 ];
        // 蓝波
        var COLOR_BLUE = [2 , 5 , 8 , 11 , 17 , 20 , 23 , 26 ];
        //灰波
        var COLOR_GRAY = [ 0,13,14,27 ];
        
        var num = parseInt(haoma);
         if ( COLOR_RED.indexOf(num)!=-1){
            return "redBall"
         }else if(COLOR_GREEN.indexOf(num)!=-1){
        	 return "blueBall"
         }else if(COLOR_BLUE.indexOf(num)!=-1){
        	 return "greenBall"
         }else if(COLOR_GRAY.indexOf(num)!=-1){
        	 return "grayBall"
         }
	});
	
	template.helper('$gyhSz', function (haoma,num,type) {
        var ary1;
        var ary2;
        var ary3;
        var ary4;
        
        haoma = parseInt(haoma);
        
		switch (num) {
		case 1:
			ary1 =[ 15,17,19,21,23,25,27 ];
			ary2 = [1,3,5,7,9,11,13];
			ary3 =[ 14,16,18,20,22,24,26 ];
			ary4 =[ 0,2,4,6,8,10,12 ];
			break;
		case 2:
			ary1 =[22,23,24,25,26,27];
			ary2 = [0,1,2,3,4,5];
			break;
		}
		
		if(ary1.indexOf(haoma)!=-1){
			if(type==1){
				return "大单"
			}else if(type==2){
				return "blueBall"
			}else if(type==3){
				return "极大"
			}
		}else if(ary2.indexOf(haoma)!=-1){
			if(type==1){
				return "小单"
			}else if(type==2){
				return "originBall"
			}else if(type==3){
				return "极小"
			}
		}else if(ary3.indexOf(haoma)!=-1){
			if(type==1){
				return "大双"
			}else if(type==2){
				return "blueBall"
			}
		}else if(ary4.indexOf(haoma)!=-1){
			if(type==1){
				return "小双"
			}else if(type==2){
				return "originBall"
			}
		}
        
		
	});
	

	
//	template.helper('zfb321', function (haoma,type) {
//        // 中
//        var zhong =[ 1,2,3,4,5,6,7 ];
//        // 发
//        var fa = [ 8,9,10,11,12,13,14  ];
//        // 白
//        var bai = [15,16,17,18,19,20 ];
//
//        var num = parseInt(haoma);
//        
//         if ( zhong.indexOf(num)!=-1){
//            return type==1?"中":'redBall'
//         }else if(fa.indexOf(num)!=-1){
//        	 return type==1?"发":'blueBall'
//         }else if(bai.indexOf(num)!=-1){
//        	 return type==1?"白":'greenBall'
//         }
//	});
	template.helper('$zfb123', function (haoma,type) {
        // 中
        var zhong =[ 1,2,3,4,5,6,7 ];
        // 发
        var fa = [ 8,9,10,11,12,13,14  ];
        // 白
        var bai = [15,16,17,18,19,20 ];

        var num = parseInt(haoma);
        
         if ( zhong.indexOf(num)!=-1){
            return type==1?"中":'redBall'
         }else if(fa.indexOf(num)!=-1){
        	 return type==1?"发":'blueBall'
         }else if(bai.indexOf(num)!=-1){
        	 return type==1?"白":'greenBall'
         }
	});

	
	template.helper('$dnxb123', function (haoma,type) {
        // 东
        var dong =[1,5,9,13,17];
        // 西
        var nan = [ 2,6,10,14,18 ];
        // 南
        var xi = [3,7,11,15,19  ];
        //北
        var bei = [4,8,12,16,20 ];
        
        var num = parseInt(haoma);
        
         if ( dong.indexOf(num)!=-1){
            return type==1?"东":'redBall'
         }else if(nan.indexOf(num)!=-1){
        	 return type==1?"南":'blueBall'
         }else if(xi.indexOf(num)!=-1){
        	 return type==1?"西":'originBall'
         }else if(bei.indexOf(num)!=-1){
        	 return type==1?"北":'greenBall'
         }
	});
	
	template.helper('$cpLh', function (haoma,num,type) {
		var num1,num2;
		switch(num){
			case 1:
				num1 = haoma[0];
				num2 = haoma[9];
				break;
			case 2:
				num1 = haoma[1];
				num2 = haoma[8];
				break;
			case 3:
				num1 = haoma[2];
				num2 = haoma[7];
				break;
			case 4:
				num1 = haoma[3];
				num2 = haoma[6];
				break;
			case 5:
				num1 = haoma[4];
				num2 = haoma[5];
				break;
			case 6:
				num1 = haoma[0];
				num2 = haoma[7];
				break;
			case 7:
				num1 = haoma[1];
				num2 = haoma[6];
				break;
			case 8:
				num1 = haoma[2];
				num2 = haoma[5];
				break;
			case 9:
				num1 = haoma[3];
				num2 = haoma[4];
				break;
			case 10:
				num1 = haoma[0];
				num2 = haoma[4];
				break;
			case 11:
				num1 = haoma[0];
				num2 = haoma[1];
				break;
			case 12:
				num1 = haoma[1];
				num2 = haoma[2];
				break;
			case 13:
				num1 = haoma[2];
				num2 = haoma[3];
				break;
			case 14:
				num1 = haoma[3];
				num2 = haoma[4];
				break;
			case 15:
				num1 = haoma[4];
				num2 = haoma[0];
				break;
			case 16:
				num1 = haoma[0];
				num2 = haoma[2];
				break;
			case 17:
				num1 = haoma[0];
				num2 = haoma[3];
				break;
			case 18:
				num1 = haoma[1];
				num2 = haoma[3];
				break;
			case 19:
				num1 = haoma[1];
				num2 = haoma[4];
				break;
			case 20:
				num1 = haoma[2];
				num2 = haoma[4];
				break;

		}
		if(num1>num2){
			if(type==1){
				return "龙"
			}else if(type==2){
				return "blueBg"
			}else if(type==3){
				return "blueBall"
			}
		}else if(num1<num2){
			if(type==1){
				return "虎"
			}else if(type==2){
				return "originBg"
			}else if(type==3){
				return "originBall"
			}

		}else {
			if(type==1){
				return "和"
			}else if(type==2){
				return "greenBg"
			}else if(type==3){
				return "greenBall"
			}
		}
	});
    $('.flexContent .'+lot+'').removeClass('hide').siblings().addClass('hide');
    $('.tableHead .'+lot+'').removeClass('hide').siblings('.right').addClass('hide');
    $('.closeLotteryMenu').click();
};
$('.tableHead .right div').click(function () {
    let index = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    $('.content:not(".hide")').children('div').eq(index).removeClass('hide').siblings().addClass('hide');
});
$('.game-item').click(function () {
    $('.showMenu div').text($(this).text());
});

// 显示隐藏侧边栏
$('.showMenu').click(function () {
    $('#lotteryMenu').show().removeClass('fadeInRightOut').addClass('fadeInRight');
});
$('.closeLotteryMenu').click(function () {
    $('#lotteryMenu').removeClass('fadeInRight').addClass('fadeInRightOut');
    setTimeout(function () {
        $('#lotteryMenu').hide()
    }, 200)
});
// 设置日期为今天
let d = new Date();
let month = ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1));
let day = (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
let today = d.getFullYear() + '-' + month + '-' + day;
$('input[type=date]').val(today);