var goBack = function () {
    window.history.go(-1);
};
$(function () {
    // 显示下注信息
    $('.result-btn').click(function () {
        let betMoney = $('.bets-input-value').val();
        if(betMoney === ''){
            $.alert("请输入投注金额");
            return;
        }
        // 注单展示
        let allBet = $('.b_btn.active');
        let winMoney = 0;
        $('#cartConfirm .body').html('');
        for(let i=0;i<allBet.length;i++){
            let name = allBet.eq(i).attr('data-name');
            let odds = allBet.eq(i).attr('data-odds');
            let html = $('<div class="flex">\n' +
                '                            <span class="w-b50 c-b">\n' +
                '                                <p class="">'+name+'</p>\n' +
                '                                <p><span class="c-h">@ </span>'+odds+'</p>\n' +
                '                            </span>\n' +
                '                            <span class="flex">'+betMoney+'</span>\n' +
                '                            <span class="w-b20">'+((betMoney* odds)-betMoney).toFixed(2)+'</span>\n' +
                '                        </div>');
            $('#cartConfirm .body').append(html);
            winMoney += ((betMoney* odds)-betMoney);
            console.log(winMoney)
        }
        $('#cartConfirm .count').html('总计'+allBet.length+'注');
        $('#cartConfirm .buyMoney').html(allBet.length * betMoney);
        $('#cartConfirm .winMoney').html(winMoney.toFixed(2));
        // 清空选中号码
        $('.b_btn.active').removeClass('active');
        let check = $('.styled-checkbox')[0].checked;
        if(!check){
            $('.bets-input-value').val('');
            $('.result-btn').html('确认 0');
        }
        $('#cart').hide();
        $('#cartConfirm').show();
    });
    $('.confirm-btn').click(function () {
        $('#cartConfirm').hide()
    });
    // 购物车
    // $('.b_btn').click(function () {
    //     $(this).toggleClass('active');
    //     let check = $('.flexContent .active').length;
    //     $('.selectCount span').html('已选'+check+'注');
    //     check? $('#cart').show(): $('#cart').hide();
    // });
    $('.b_btn').on('click',function () {
        let money = $('.bets-input-value').val();
        $(this).toggleClass('active');
        let check = $('.flexContent .b_btn.active').length;
        $('.selectCount span').html('已选'+check+'注');
        check? $('#cart').show(): $('#cart').hide();
        $('.result-btn').html('确认 '+ (money * check))
    });
    $('#cart .money').click(function () {
        let check = $('.flexContent .b_btn.active').length;
        let money = $(this).text();
        $('.bets-input-value').val(money);
        $('.result-btn').html('确认 '+ (money * check))
    });
    $(".bets-input-value").bind('input propertychange',function(){
        let check = $('.flexContent .b_btn.active').length;
        let money = $(this).val();
        money === ''? money = 0: '';
        $('.result-btn').html('确认 '+ (money * check))
     });

    // 显示隐藏 彩票侧边栏
    $('.showMenu').click(function () {
        console.log(1);
        $('#lotteryMenu').show().removeClass('fadeInRightOut').addClass('fadeInRight');
    });
    $('#lotteryMenu').click(function () {
        $(this).removeClass('fadeInRight').addClass('fadeInRightOut');
        setTimeout(function () {
            $('#lotteryMenu').hide()
        }, 200)
    });

    // 显示隐藏侧边栏
    $('.menu').click(function () {
        console.log(1);
        $('#menu').show().removeClass('fadeInLeftOut').addClass('fadeInLeft');
    });
    $('#menu').click(function () {
        $(this).removeClass('fadeInLeft').addClass('fadeInLeftOut');
        setTimeout(function () {
            $('#menu').hide()
        }, 300)
    });
    //显示今日输赢
    $('.top .icon').click(function () {
       $(this).toggleClass('arrow-white').toggleClass('upper-white').siblings().toggleClass('hide')
    });    
    // 最近游戏
    $('.game-list').click(function () {
        $(this).find('.checkGame span').toggleClass('arrow-black').toggleClass('upper-black');
        $(this).find('.modal').toggleClass('hide')
    });
    $('.checkGame .modal li').click(function () {
        $('.checkGame .select').text($(this).text())
    });
    // 底部导航切换
    $('.weui-tabbar a').click(function () {
        $(this).addClass('weui-bar__item--on').siblings().removeClass('weui-bar__item--on')
    });

});
