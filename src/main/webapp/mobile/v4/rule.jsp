<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
    <link rel="stylesheet" href="./css/lottery.css">
</head>
<body class="flexWrap">
<div>
    <div class="top bg text-c h-50 l-h-50">
        <div class="showMenu c-w p-a p-l-10">PC蛋蛋 <span class="arrow-white"></span></div>
        <a class="back-to-game c-w f-s-20 f-w-900" target="_blank" href="http://1681391.com">规则</a>
    </div>
    <div id="lotteryMenu" class="flexContent w-b100 h-b100 p-f" style="background-color: rgba(0,0,0,.7);display: none">
        <div class="w-b80 bg-w pull-r p-l-r-10 p-b-t-20" style="min-height: 100%">
            <div class="game-div">
                <h3 class="game-title"><span>极速彩系列</span></h3>
                <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                        class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                        class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                        class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                        class="game-item new ">极速牛牛(娱乐)</span></div>
            </div>
            <div class="game-div">
                <h3 class="game-title"><span>极速彩系列</span></h3>
                <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                        class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                        class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                        class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                        class="game-item new ">极速牛牛(娱乐)</span></div>
            </div>
            <div class="game-div">
                <h3 class="game-title"><span>极速彩系列</span></h3>
                <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                        class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                        class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                        class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                        class="game-item new ">极速牛牛(娱乐)</span></div>
            </div>
            <div class="game-div">
                <h3 class="game-title"><span>极速彩系列</span></h3>
                <div class="game-item-wrapper"><span class="game-item hot ">极速赛车</span><span class="game-item hot ">极速时时彩</span><span
                        class="game-item new ">极速牛牛(简约)</span><span class="game-item ">极速百家乐</span><span
                        class="game-item ">极速六合彩</span><span class="game-item ">极速飞艇</span><span class="game-item ">极速11选5</span><span
                        class="game-item ">极速快乐8</span><span class="game-item ">极速快3</span><span class="game-item ">极速快乐十分</span><span
                        class="game-item new ">极速牛牛(娱乐)</span></div>
            </div>
        </div>
    </div>
    <jsp:include page="./include/footer.jsp"/>
   	<jsp:include page="./include/menu.jsp"/>
</div>


<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
</body>
</html>
<style>
    .top {
        position: relative;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 52px;
        line-height: 52px;
        background-image: linear-gradient(to right, rgb(124, 201, 254), rgb(27, 82, 149));
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px 0px;
        z-index: 10;
    }
</style>
