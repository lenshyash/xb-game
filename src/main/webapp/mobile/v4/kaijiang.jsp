<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="./css/weui.min.css">
    <link rel="stylesheet" href="./css/jquery-weui.css">
    <link rel="stylesheet" href="./css/common.css">
    <link rel="stylesheet" href="./css/lottery.css">
</head>
<body class="flexWrap">
<input id="base" type="hidden" var="${base }"/>

<div class="flexWrap">
    <div class="top bg text-c h-50 l-h-50">
        <a class="back-to-game" target="_blank" href="http://1681391.com">全国开奖网</a>
        <p class="f-s-20 c-w f-w-900">开奖结果</p>
    </div>
    <div class="flex choiceKj">
        <div class="flex1 p-l-r-10 cmZbu b-r showMenu">
            <div class="text-l">极速赛车</div>
            <span class="arrow-black"></span>
        </div>
        <div class="flex1 p-l-r-10 jnQSGx p-r">
            <input tabindex="1" type="date">
        </div>
    </div>

    <div id="lotteryMenu" class="flexContent w-b100 h-b100 p-f flex" style="background-color: rgba(0,0,0,.7);display: none">
        <div class="w-b20 h-b100 closeLotteryMenu"></div>
        <div class="w-b80 bg-w pull-r p-l-r-10 p-b-t-20" style="min-height: 136%">
           	<c:forEach var="map" items="${bcmap}">
           	   <div class="game-div">
	                <h3 class="game-title" ><span>${map.key }</span></h3>
	                <div class="game-item-wrapper">
	                	<c:forEach var="bc" items="${map.value }">
	                    	<span onclick="show('${bc.key}','${bc.code }')" class="game-item hot">${bc.name }</span>
	                    </c:forEach>
	                </div>
	           </div>
           	</c:forEach>
        </div>
    </div>
    <div>
        <div class="tableHead">
            <div class="left">
                <div class="eoLxBD">期数</div>
                <div class="eoLxBD">时间</div>
            </div>
            <div class="right pk10">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">大小</div>
                <div class="cjbmrj">单双</div>
                <div class="cjbmrj">总和/形态</div>

            </div>

            <div class="right syxw cqhlsx">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">大小</div>
                <div class="cjbmrj">单双</div>
                <div class="cjbmrj">总和/形态</div>
            </div>

            <div class="right klsf">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">大小</div>
                <div class="cjbmrj">单双</div>
                <div class="cjbmrj">总和/形态</div>
                <!--  <div class="cjbmrj">尾大/小</div>
                <div class="cjbmrj">和单/双</div>
                <div class="cjbmrj">东南西北</div>
                <div class="cjbmrj">中发白</div> -->
            </div>

             <div class="right ssc">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">大小</div>
                <div class="cjbmrj">单双</div>
                <div class="cjbmrj">总和/跨度</div>
                <!-- <div class="cjbmrj">质和</div> -->

            </div>

            <div class="right lhc hide">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">总和</div>
                <div class="cjbmrj">特码</div>
                <!-- <div class="cjbmrj">正码大小</div>
                <div class="cjbmrj">正码单双</div>
                 <div class="cjbmrj">正码和大/小</div>
                <div class="cjbmrj">正码和单/双</div> -->
            </div>
            <div class="right kuaisan hide">
                <div class="cjbmrj active">号码</div>
            </div>
            <div class="right pcdd hide">
            <div class="cjbmrj active">号码</div>
            <!-- <div class="cjbmrj">总和/形态</div> -->
            </div>
            <div class="right dpc hide">
                <div class="cjbmrj active">号码</div>
                <div class="cjbmrj">大小</div>
                <div class="cjbmrj">单双</div>
				<div class="cjbmrj">总和/形态</div>
            </div>
        </div>
    </div>
    <div class="flexContent">
        <div class="content  pk10">

        </div>
        <div class="content hide ssc">

        </div>
        <div class="content hide lhc">

        </div>
        <div class="content hide syxw">

        </div>
        <div class="content hide kuaisan">

        </div>
        <div class="content hide cqhlsx">
            <div class="">
                <div class="row flex">
                    <div class="left">
                        <span class="title">31065646</span>
                        <span class="title">14:16</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="hlsx sx0"> </span></div>
                            <div class="flex1"><span class="hlsx sx1"> </span></div>
                            <div class="flex1"><span class="hlsx sx2"> </span></div>
                            <div class="flex1"><span class="hlsx sx3"> </span></div>
                            <div class="flex1"><span class="hlsx sx4"> </span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide">
                <div class="row flex">
                    <div class="left">
                        <span class="title">31065646</span>
                        <span class="title">14:16</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">大</span></div>
                            <div class="flex1"><span class="originBall">小</span></div>
                            <div class="flex1"><span class="blueBall">大</span></div>
                            <div class="flex1"><span class="originBall">小</span></div>
                            <div class="flex1"><span class="blueBall">大</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide">
                <div class="row flex">
                    <div class="left">
                        <span class="title">31065646</span>
                        <span class="title">14:16</span>
                    </div>
                    <div class="right">
                        <div class="flex w-b100 text-c">
                            <div class="flex1"><span class="blueBall">单</span></div>
                            <div class="flex1"><span class="originBall">双</span></div>
                            <div class="flex1"><span class="blueBall">单</span></div>
                            <div class="flex1"><span class="originBall">双</span></div>
                            <div class="flex1"><span class="blueBall">单</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide">
                <div class="row flex">
                    <div class="left">
                        <span class="title">31065646</span>
                        <span class="title">14:16</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">16</div></div>
                            <div class="flex1 p-l-5"><span class="blueBall">大</span></div>
                            <div class="flex1 p-l-5"><span class="originBall">双</span></div>
                            <div class="flex1 p-l-5"><span class="originBall">虎</span></div>
                            <div class="f-w-900 f-s-20 c-g">对子</div>
                            <div class="f-w-900 f-s-20 p-l-r-5 c-o">半顺</div>
                            <div class="f-w-900 f-s-20 c-b" >顺子</div>
                        </div>
                    </div>
                </div>
                <div class="row flex">
                    <div class="left">
                        <span class="title">31065646</span>
                        <span class="title">14:16</span>
                    </div>
                    <div class="right">
                        <div class="flex">
                            <div class="flex1"><div class="f-w-900 c-r p-l-r-5">16</div></div>
                            <div class="flex1 p-l-5"><span class="blueBall">大</span></div>
                            <div class="flex1 p-l-5"><span class="originBall">双</span></div>
                            <div class="flex1 p-l-5"><span class="originBall">虎</span></div>
                            <div class="f-w-900 f-s-20 c-g">对子</div>
                            <div class="f-w-900 f-s-20 p-l-r-5 c-gray">杂六</div>
                            <div class="f-w-900 f-s-20 c-b" >顺子</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content hide klsf">

        </div>
        <div class="content hide pcdd">

        </div>
        <div class="content hide dpc">

        </div>
    </div>
    <div class="h-60"></div>
  	<jsp:include page="./include/footer.jsp"/>
   	<jsp:include page="./include/menu.jsp"/>
</div>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<jsp:include page="./result/pk10.jsp"></jsp:include>
<jsp:include page="./result/ssc.jsp"></jsp:include>
<jsp:include page="./result/lhc.jsp"></jsp:include>
<jsp:include page="./result/kuaisan.jsp"></jsp:include>
<jsp:include page="./result/dpc.jsp"></jsp:include>
<jsp:include page="./result/syxw.jsp"></jsp:include>
<jsp:include page="./result/klsf.jsp"></jsp:include>
<jsp:include page="./result/pcdd.jsp"></jsp:include>
<script src="./js/jquery.min.js"></script>
<script src="./js/jquery-weui.min.js"></script>
<script src="./js/common.js"></script>
<script src="./js/mobile-result.js"></script>
</body>
</html>

<style>
    .back-to-game {
        position: absolute;
       /*  left: 15px; */
       	right:20px;
        top: 13px;
        height: 24px;
        line-height: 24px;
        color: rgb(255, 255, 255);
        font-size: 0.8125rem;
        padding: 2px 12px;
        border-radius: 20px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(255, 255, 255);
        border-image: initial;
    }
    .choiceKj {
        height: 45px;
        background-color: rgb(255, 255, 255);
        display: flex;
    }
    .choiceKj > div {
        box-sizing: border-box;
        height: 100%;
        text-align: center;
        flex: 1 1 50%;

        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: justify;
        justify-content: space-between;
        padding: 0px 10px;
        border-right: 1px solid rgb(234, 234, 234);
    }
    .cmZbu div {
        color: rgb(102, 102, 102);
        font-weight: 400;
        font-size: 0.8125rem;
        background-color: rgba(255, 255, 255, 0);
        line-height: 2.5rem;
        border-width: 1px;
        border-style: solid;
        border-color: rgba(255, 0, 0, 0);
        border-image: initial;
    }
    .jnQSGx input {
        color: rgb(102, 102, 102);
        font-weight: 400;
        font-size: 0.8125rem;
        background-color: rgba(255, 255, 255, 0);
        -webkit-appearance: none;
        height: 100%;
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
    }
    .jnQSGx::after {
        content: "";
        position: absolute;
        height: 20px;
        width: 20px;
        top: 10px;
        right: 10px;
        background: url(./image/date.png) center center / contain no-repeat;
    }
    .tableHead {
        display: flex;
        justify-content: space-around;
        -webkit-box-align: center;
        align-items: center;
        height: 40px;
        /*box-shadow: rgb(204, 204, 204) 0px 0px 5px;*/
        background-color: rgb(255, 255, 255);
        border-bottom: 1px solid rgb(204, 204, 204);
    }
    .left {
        display: flex;
        justify-content: space-around;
        -webkit-box-align: center;
        align-items: center;
        height: 100%;
        flex: 3 1 0%;
        flex-flow: row wrap;
    }
    .right {
        display: flex;
        justify-content: space-around;
        -webkit-box-align: center;
        align-items: center;
        height: 100%;
        flex: 5 1 0%;
        flex-flow: row wrap;
    }
    .eoLxBD {
        font-size: 0.875rem;
        color: rgb(102, 102, 102);
    }
    .cjbmrj {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        font-size: 0.8125rem;
        height: 25px;
        user-select: none;
        color: rgb(255, 255, 255) !important;
        background-color: rgb(95, 95, 95) !important;
        border-radius: 7px;
        padding: 0px 5px;
        margin: 0px 2px;
        border-width: 1px !important;
        border-style: solid !important;
        border-color: rgb(95, 95, 95) !important;
        border-image: initial !important;
    }
    .active {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        font-size: 0.8125rem;
        height: 25px;
        user-select: none;
        color: rgb(255, 255, 255) !important;
        background-color: rgb(0, 102, 204) !important;
        border-radius: 7px;
        padding: 0px 5px;
        margin: 0px 2px;
        border-width: 1px !important;
        border-style: solid !important;
        border-color: rgb(0, 102, 204) !important;
        border-image: initial !important;
    }
    .left span.title {
        font-size: -0.25rem;
        color: rgb(51, 51, 51);
        border: none;
        line-height: 21px;
    }
    .row{
        height: 2.5rem;
        width: 100%;
        border-bottom: 1px solid rgb(204, 204, 204);
    }
    .row-60{
        height: 3.5rem;
        width: 100%;
        border-bottom: 1px solid rgb(204, 204, 204);
    }
    .bj-item{
        height: 1.3rem;
        width: 1.3rem;
        display: inline-block;
        line-height: 1.3rem;
        color: rgb(255, 255, 255);
        text-align: center;
        text-indent: 0px;
        font-size: 0.875rem;
        font-weight: bold;
        font-style: italic;
        text-shadow: rgb(0, 0, 0) 0px 0px 2px;
        margin-right: 1px;
        border-radius: 4px;
    }
    .bj-1{
        background-color: rgb(255, 253, 60);
    }
    .bj-2{
        background-color: rgb(0, 140, 250);
    }
    .bj-3{
        background-color: rgb(77, 77, 77);
    }
    .bj-4{
        background-color: rgb(255, 112, 34);
    }
    .bj-5 {
        background-color: rgb(119, 255, 253);
    }
    .bj-6{
        background-color: rgb(66, 36, 248);
    }
    .bj-7{
        background-color: rgb(227, 227, 227);
    }
    .bj-8 {
        background-color: rgb(255, 0, 26);
    }
    .bj-9{
        background-color: rgb(121, 0, 7);
    }
    .bj-10{
        background-color: rgb(50, 197, 51);
    }
    .blueBg{
        height: 1.3rem;
        width: 1.3rem;
        line-height: 1.3rem;
        color: rgb(255, 255, 255);
        text-align: center;
        font-size: 0.875rem;
        font-weight: bold;
        margin-right: 2px;
        background-color: rgb(0, 108, 218);
        border-radius: 4px;
        display: inline-block;
    }
    .originBg {
        height: 1.3rem;
        width: 1.3rem;
        line-height: 1.3rem;
        color: rgb(255, 255, 255);
        text-align: center;
        font-size: 0.875rem;
        font-weight: bold;
        margin-right: 2px;
        background-color: rgb(255, 167, 53);
        border-radius: 4px;
        display: inline-block;
    }

    .flex-m{
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        display: inline-flex;
    }

    .lhc-y{
        background: linear-gradient(rgb(255, 154, 0) 0px, rgb(255, 102, 0));
    }
    .lhc-g{
        background: linear-gradient(rgb(89, 225, 75) 1%, rgb(58, 193, 44));
    }
    .lhc-r{
        background: linear-gradient(rgb(250, 116, 118) 0px, rgb(238, 9, 9));
    }
    .lhc-b{
        background: linear-gradient(rgb(97, 156, 255) 0px, rgb(10, 94, 255));
    }

    .c-g{
        color: rgb(0, 221, 52);
    }
    .c-gray{
        color: rgb(102, 102, 102);
    }
    .c-o{
        color: rgb(255, 155, 0);
    }
    .c-b{
        color: rgb(0, 102, 204);
    }
</style>
