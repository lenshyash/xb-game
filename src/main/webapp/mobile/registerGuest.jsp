﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<%
Long stationId = StationUtil.getStation().getId();
pageContext.setAttribute("onoffRegister", StationConfigUtil.get(stationId, StationConfig.onoff_register));
%>
<!DOCTYPE html>
<html>
<head>
<title>用户试玩注册</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="${base }/mobile/style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base }/mobile/style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base }/mobile/style/global.css?v=2" type="text/css" />
<c:if test="${onoffRegister!='on'}">
<script>
	// location.href = "${station}/index.do";
</script>
</c:if>
</head>
<body style="background: url(${base }/mobile/images/jiazaibox2.jpg) no-repeat center top; background-size: 100% 100%;">

<!-- id=innerbox 标签外面 不放任何标签  为自适应标签 -->
<div class="" id="innerbox"> 
	<div class="jiazaibox">
		<div class="cl h40"></div>
		<div class="cl h40"></div>
		<div class="dabt" style="font-size: 48px;">用户试玩注册</div>
		<!-- <div class="xiaowz">www.12345.com</div> -->
		<div class="cl h40"></div>
		<div class="zhucebox">
			<div class="zhucetl">账户信息</div>
			<ul>
				<li>
					<div class="zhucemz">
						用户账号：
					</div>
					<div class="zhuceinptbox">
						<input id="account" type="text" class="zcinpt" value="${userName }" disabled="disabled" />
						<div class="f18 cw l32"> *请输入5-11个字元, 仅可输入英文字母以及数字的组合!!</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						登入密码：
					</div>
					<div class="zhuceinptbox">
						<input id="password" type="password" class="zcinpt" value="" />
						<div class="f18 cw l32"> *请输入6-16个英文或数字，且符合0~9或a~z字元!</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						确认密码：
					</div>
					<div class="zhuceinptbox">
						<input id="rpassword" type="password" class="zcinpt" value="" />
						<div class="f18 cw l32">*请再次输入您的登录密码！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						验证码：
					</div>
					<div class="zhuceinptbox">
						<input id="verifyCode" type="text" class="zcinpt" style="width: 160px;" value="" />
						<img src="${base }/verifycode.do" alt="点击刷新" name="verifyCodeImg" id="verifyCodeImg" style="margin-bottom: -12px; cursor: pointer; width: 100px;" />
						<div class="f18 cw l32"> </div>
					</div>
					<div class="cl"></div>
				</li>
			</ul>
		</div>
		<div class="cl h40"></div>
		<div class="tac">
			<input id="doregisterbtn" type="submit" class="lijizhucebtn" value="立即注册" />
		</div>
		<div class="cl"></div>
		<div class="tac wjmm" style="margin-top: 20px;">
			<a href="${base }/mobile/index.do" class="cw" >返回首页</a>
			<a href="${base }/mobile/login.do" class="cw" >返回登录</a>
		</div>
	</div>
</div>
</body>

<script type="text/javascript" src="${base }/mobile/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${base }/mobile/script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="${base }/mobile/script/global.js"></script>
<script type="text/javascript" src="${base }/mobile/script/common.js"></script>
<script type="text/javascript">
	$(function(){
		$("#verifyCodeImg").on("click", function(){
			$(this).attr("src", "${base }/verifycode.do?" + new Date());
		});

		$("#doregisterbtn").on("click", function(){
			var params = {};
			params.account = $("#account").val();
			params.password = $("#password").val();
			params.rpassword = $("#rpassword").val();
			params.verifyCode = $("#verifyCode").val();
			$.ajax({
				type : "POST",
				url : "${base}/registerTestGuest.do",
				data : {
					data: JSON.stringify(params)
				},
				success : function(result) {
					if(result.success == false){
						$.alertB(result.msg);
						$("#verifyCodeImg").attr("src", "${base }/verifycode.do?" + new Date());
					} else if(result.success == true){
						$.alert("注册成功");
						location.href = "${base }/mobile/index.do";
					}
				}
			});
		});
	});
</script>
</html>