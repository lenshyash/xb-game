﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 10px 16px;
    margin-left: -1px;
    line-height: 1.3333333;
    color: #cd0005;
    text-decoration: none;
    background-color: #ddd;
    border: 1px solid #fff;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #cd0005;
    border-color: #cd0005;
}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 3;
    color: #cd0005;
    background-color: #eee;
    border-color: #ddd;
}
</style>
</head>
<body>
	<div class="top"	>
		<div class="inner">
			<div class="back">
				<a href="javascript: history.go(-1);" style="color: #fff;"><em class="iconfont icon-left"></em></a> <span class="vbktl">站内信</span>
			</div>
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<!-- id=innerbox 标签外面 不放任何标签 为自适应标签 -->
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div class="caizhongtable" id="recordContent">
			</div>
			<nav id="pageview">
			</nav>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
	
	<div class="modal fade" id="quotaTransModal" tabindex="-1" role="dialog" aria-labelledby="quotaTransModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title f22" id="quotaTransModalLabel">额度转换</h4>
				</div>
				<div class="modal-body">
					<div class="f18" id="uMessage"></div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
</html>
<script type="text/javascript">
	var reloadRecordView = function (currentPageNo, pageSize) {
		currentPageNo = currentPageNo || 1;
		pageSize = pageSize || 20;

		$._ajax({url: baseUrl + "/center/news/message/list.do", data:{pageNumber: currentPageNo, pageSize: pageSize}}, function (status, message, data, result) {
			data = JSON.parse(result.responseText);
			var tpl = _.template($("#recoredNewTemplate").html());
			var version = result.responseJSON.version;
			$("#recordContent").html(tpl({data : data.list, version: version}));

			$("a[name=showmessage]").click(function(){
				var id = $(this).parents("tr").data("id");
				var title = $(this).parents("tr").data("title");
				var message = $(this).parents("tr").data("message");
				$("#quotaTransModalLabel").html(title);
				$("#uMessage").html(message);

				$.ajax({
					url: baseUrl + '/center/news/message/read.do',
					data: {
						id : id
					},
					success: function(r){
						$("#quotaTransModal").modal('show');
					}
				});
			});

			var pageviewTpl = _.template($("#pageviewTemplate").html());
			$("#pageview").html(pageviewTpl({data: data}));

			$("#pageview .pagination li a").on("click", function(){
				var pageNo = $(this).attr("pageno");
				reloadRecordView(pageNo);
			});
		});
	}

	$("#quotaTransModal").on('hide.bs.modal', function(){
		reloadRecordView();
	});
	reloadRecordView();
	//站内信全选
	function checkAll(){
	     $("#messageBody input:checkbox").prop("checked", $("#theadInp").prop("checked"));
    } 
    function check(){
         //当选中的长度等于checkbox的长度的时候,就让控制全选反选的checkbox设置为选中,否则就为未选中
         if($("#messageBody input:checkbox").length === $("#messageBody input:checked").length) {
             $("#theadInp").prop("checked", true);
         } else {
             $("#theadInp").prop("checked", false);
         }
    }
    function mdel(id){
    	        if (confirm("确认删除此信吗？")) {
    				$.ajax({
    					url : "${base}/center/news/message/batchNewDelete.do",
    					data : {
    						id : id
    					},
    					success : function(result) {
    						reloadRecordView();
    					}
    				});
    			}
    }
   	function batchRead(){
     	 var check=$("input[name='mesic']:checked");//选中的复选框  
     	 var rids = "";
     	 check.each(function(){  
 	        var row=$(this).parent("td").parent("tr");  
 	        rids += row.attr("data-id") +",";
 	        
     	 });
        	rids = rids.substring(0,rids.length-1);
     	if (confirm("确认标记为已读吗？")) {
			$.ajax({
					url:'${base}/center/news/message/batchRead.do',
					data:{id:rids},
					success:function(res){
						if(res.success){
							alert('标记成功');
							reloadRecordView();
						}else{
							alert(res.msg);
						}
					}
				})
		}
   	}
   	function batchDel(){
     	 var check=$("input[name='mesic']:checked");//选中的复选框 
     	 var dids = "";
     	 check.each(function(){  
	  	      var row=$(this).parent("td").parent("tr");  
	  	    	  dids += row.attr("data-id") +",";
     	 });
     	 dids = dids.substring(0,dids.length-1);
     	 if (confirm("确认标记为已读吗？")) {
	  	      $.ajax({
					url:'${base}/center/news/message/batchNewDelete.do',
					data:{id:dids},
					success:function(res){
						if(res.success){
							alert('删除成功');
							reloadRecordView();
						}else{
							alert(res.msg);
						}
					}
				})
 	      	}
   	}
</script>
<script type="text/html" id="recoredNewTemplate" style="display: none;">
				<table>
					<tr class="caizhongtabletr">
					    <td>	<input type="checkbox" id= "theadInp" onclick = "checkAll()"></td> 	
						<td>状态</td>
						<td>标题</td>
						<td>发布时间</td>
					</tr>
					<tbody id = "messageBody">
					{# 
						_.map(data, function(item){
						var statusTitle = "<span style='color: #ff0000;'>未读</span>"; // 状态（1、未读，2、已读）
						switch(item.status){
						case 1:
							statusTitle = "<span style='color: #ff0000;'>未读</span>";
							break;
						case 2:
							statusTitle = "<span style='color: #00ff00;'>已读</span>";
							break;
						}
						var showMessage = $("<div>" + item.message + "</div>").text();
					 #}
					<tr class="" data-id="{{item.id}}" data-title="{{item.title}}" data-message='{{item.message}}'>
						<td>	<input type="checkbox" name = "mesic" onclick = "check()"></td>
						<td>{{statusTitle}}</td>
						<td><a name="showmessage" >{{item.title}}</a></td>
						<td>{{new Date(item.createDatetime).format()}}</td>
						<td>	<input class="siteButton" style="color:#900;" type="button" onclick="mdel({{item.id}})" value= "删除"/><td/>
					</tr>
					{#
						});
					#}
					<i
					</tbody>
				</table>
				<input type="button" class="siteButton" onclick ="batchRead();" style="margin-top: 20px;margin-left: 70%;" value="标记已读"/>
				<input type="button" class="siteButton" id ="pldel" onclick="batchDel()" style="" value="批量删除"/>
</script>
<script type="text/html" id="pageviewTemplate" style="display: none;">
				{#
				if(data.totalPageCount > 1){
				#}
				<ul class="pagination pagination-lg pull-right">
					<li>
						<a href="javascript: void(0);" aria-label="Previous" pageno="{{data.prePage}}">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<!-- 
					<li class="active"><a href="javascript: void(0);">1</a></li>
					<li><a href="javascript: void(0);">2</a></li>
					<li><a href="javascript: void(0);">3</a></li>
					<li><a href="javascript: void(0);">4</a></li>
					<li><a href="javascript: void(0);">5</a></li>
					 -->
					<li>
						<a href="javascript: void(0);" aria-label="Next" pageno="{{data.nextPage}}">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
				{#
				}
				#}
</script>
<style>
	.siteButton{
	position: relative;
    overflow: visible;
    display: inline-block;
    padding: 0.5em 1em;
    border: 1px solid #d4d4d4;
    margin: 0;
    text-decoration: none;
    text-align: center;
    text-shadow: 1px 1px 0 #fff;
    font:11px/normal sans-serif;
    color: #333;
    white-space: nowrap;
    cursor: pointer;
    outline: none;
    background-color: #ececec;
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f4f4f4), to(#ececec));
    background-image: -moz-linear-gradient(#f4f4f4, #ececec);
    background-image: -ms-linear-gradient(#f4f4f4, #ececec);
    background-image: -o-linear-gradient(#f4f4f4, #ececec);
    background-image: linear-gradient(#f4f4f4, #ececec);
    -moz-background-clip: padding; /* for Firefox 3.6 */
    background-clip: padding-box;
    border-radius: 0.2em;
    /* IE hacks */
    zoom: 1;
    *display: inline;
	}
</style>
<%@include file="include/check_login.jsp" %>