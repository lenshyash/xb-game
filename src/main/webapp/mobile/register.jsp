﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<%
Long stationId = StationUtil.getStation().getId();
pageContext.setAttribute("onoffRegister", StationConfigUtil.get(stationId, StationConfig.onoff_register));
%>
<!DOCTYPE html>
<html>
<head>
<title>${_title }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="${base }/mobile/script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base }/mobile/style/fonts/iconfont.css" type="text/css" />

<style type="text/css">
.jiazaibox{
	text-align:center;
}
.dabt{
	color:#fff;
	font-size: 32px;
	line-height: 40px;
	font-weight: bold;
	margin-top: 36px;
}
.zhucebox{
	width: 90%;
	margin:0 auto;
	border:1px solid #fff;
	position:relative;
	padding: 11.7px 0px;
	margin-top: 26px;
}
.zhucetl{
	position:absolute;
	left: 18px;
	top: -11px;
	line-height:23px;
	font-size:16px;
	color:#fff;
	padding:0px 10px;
	background:#d50405;
}
.zhucemz{
	float:left;
	width: 83px;
	color:#fff;
	text-align:right;
	font-size:14px;
	line-height:26px;
	
}
.zhucebox li{
	padding:7px 0px;
	padding-bottom:0px;
}
.zhuceinptbox{
	float: left;
	width: 70%;
	text-align: left;
}
.zcinpt{
	height: 26px;
	border: none;
	background:#fff;
	border-radius: 5px;
	width: 90%;
	padding-left: 6px;
	font-size: 13px;
}
.tips {
	font-size: 13px;
	color: #fff;
}
.tac {
	text-align: center
}
.lijizhucebtn{
	width: 90%;
	text-align:center;
	line-height:30px;
	color:#000;
	border:none;
	background:#ff8a00;
	font-size: 15px;
	border-radius: 5px;
}
.wjmm{
	font-size: 14px;
	
}
.wjmm a{
	margin: 0px 13px;
	line-height: 32px;
	color: #fff;
}
ul {
	padding: 0px;
	list-style: none;
}
.cl {
	clear: both;
}

.linecentermodel {
	margin: auto;
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
}
</style>

<c:if test="${onoffRegister!='on'}">
<script>
	// location.href = "${station}/index.do";
</script>
</c:if>
</head>
<body style="background: url(${base }/mobile/images/jiazaibox2.jpg) no-repeat center top; background-size: 100% 100%;">
	<div class="jiazaibox">
		<div class="dabt">${_title}</div>
		<div class="zhucebox">
			<div class="zhucetl">账户信息</div>
			<ul>
				<li>
					<div class="zhucemz">
						用户账号：
					</div>
					<div class="zhuceinptbox">
						<input id="username" type="text" class="zcinpt" value="" />
						<div class="tips"> *请输入5-11个字元, 仅可输入英文字母以及数字的组合!!</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						登入密码：
					</div>
					<div class="zhuceinptbox">
						<input id="password" type="password" class="zcinpt" value="" />
						<div class="tips"> *请输入6-16个英文或数字，且符合0~9或a~z字元 ！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						确认密码：
					</div>
					<div class="zhuceinptbox">
						<input id="confirmPass" type="password" class="zcinpt" value="" />
						<div class="tips">*请再次输入您的登录密码！</div>
					</div>
					<div class="cl"></div>
				</li>
				
			</ul>
		</div>
		<div class="cl h40"></div>
		<div class="zhucebox">
			<div class="zhucetl" style="background:#ba0305;">个人资料</div>
			
			<ul>
				<li class="userName" style="display: none;">
					<div class="zhucemz" >
						真实姓名：
					</div>
					<div class="zhuceinptbox">
						<input id="realname" type="text" class="zcinpt" value="" />
						<div class="tips"> *真实姓名必须与您银行账户相同！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="email" style="display: none;">
					<div class="zhucemz">
						电子邮箱：
					</div>
					<div class="zhuceinptbox">
						<input id="email" type="text" class="zcinpt" value="" />
						<div class="tips">  *请认真填写，以便有优惠活动可以即时的通知您！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="phone" style="display: none;">
					<div class="zhucemz">
						手机号码：
					</div>
					<div class="zhuceinptbox">
						<input id="phone" type="text" class="zcinpt" value="" />
						<div class="tips">  *请认真填写，以便有优惠活动可以即时的通知您！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="qq" style="display: none;">
					<div class="zhucemz">
						QQ号码：
					</div>
					<div class="zhuceinptbox">
						<input id="qqnum" type="text" class="zcinpt" value="" />
						<div class="tips">  *请认真填写，以便有优惠活动可以即时的通知您！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="wechat" style="display: none;">
					<div class="zhucemz">
						微信号：
					</div>
					<div class="zhuceinptbox">
						<input id="wechat" type="text" class="zcinpt" value="" />
						<div class="tips">  *请认真填写！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="cardNo" style="display: none;">
					<div class="zhucemz">
						银行卡号：
					</div>
					<div class="zhuceinptbox">
						<input id="cardNoNum" type="text" class="zcinpt" value="" />
						<div class="tips">  *银行卡号非常重要，请认真填写！</div>
					</div>
					<div class="cl"></div>
				</li>
				<li class="receiptPwd" style="display: none;">
					<div class="zhucemz">
						提款密码：
					</div>
					<div class="zhuceinptbox">
						<input id="cashpass" type="password" class="zcinpt" value="" />
						<div class="tips"> *提款认证必须，请务必记住!</div>
					</div>
					<div class="cl"></div>
				</li>
				<li>
					<div class="zhucemz">
						验证码：
					</div>
					<div class="zhuceinptbox">
						<input id="verifyCode" type="text" class="zcinpt" style="width: 94px;" value="" />
						<img src="${base }/mobile/regVerifycode.do" alt="点击刷新" name="verifyCodeImg" id="verifyCodeImg" style="cursor: pointer; width: 58px;" />
						<div class="tips"> </div>
					</div>
					<div class="cl"></div>
				</li>
			</ul>
		</div>
		<div class="tac" style="margin-top: 30px;">
			<input id="doregisterbtn" type="submit" class="lijizhucebtn" value="立即注册" />
		</div>
		<div class="tac wjmm" style="margin-top: 20px; margin-bottom: 20px;">
			<a href="${base }/mobile/index.do" >返回首页</a>
			<a href="${base }/mobile/login.do" >返回登录</a>
		</div>
	</div>
	<div id="ycf-alert" class="modal">
		<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
			<div class="modal-content">
				<div class="modal-body bg-danger" style="border-radius: 5px; padding: 10px;">
					<p style="font-size: 15px; font-weight: bold;">[Message]</p>
				</div>
				<div class="modal-footer" style="padding: 5px; text-align: center;">
					<button type="button" class="btn btn-warning ok" data-dismiss="modal">[BtnOk]</button>
					<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="${base }/mobile/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${base }/mobile/script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${base }/mobile/script/bootstrap/js/bootstrap_alert.js"></script>
<script type="text/javascript" src="${base }/mobile/script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="${base }/mobile/script/common.js"></script>
<c:if test="${isWechatPop eq 'on'}"><jsp:include page="${base }/mobile/wechat/wechat.jsp"></jsp:include></c:if>
<script type="text/javascript">
	$(function(){
		var $username = $("#username"), $password = $("#password"), $confirmPass = $("#confirmPass"), $realname = $("#realname"), $phone = $("#phone"), $qqnum = $("#qqnum"), $wechat = $("#wechat"), $cardNoNum = $("#cardNoNum"), $email = $("#email"), 
			$cashpass = $("#cashpass"), $doregisterbtn = $("#doregisterbtn"), $verifyCode = $("#verifyCode"), $verifyCodeImg = $("#verifyCodeImg");

		$verifyCodeImg.on("click", function(){
			$(this).attr("src", "${base }/mobile/regVerifycode.do?" + new Date());
		});

		$.ajax({
			type : "POST",
			url : stationUrl + "/registerConfig.do",
			data : {},
			success : function(result) {
				if(result.success == false){
					// $.alertB(result.msg);
					Modal.alert({
						msg : result.msg
					}).on(function(e) {
						return;
					});
				} else if(result.success == true){
					var config = {
							userName: !1,
							sex: !1,
							qq: !1,
							phone: !1,
							email: !1,
							receiptPwd: !1,
							cardNo: !1,
							wechat: !1
					};
					_.map(result.data, function(item){
						config[item.key] = !0;
						$("li." + item.key).show();
					});

					$doregisterbtn.on("click", function(){
						var params = {
								username: $username.val(),
								password: $password.val(),
								rpassword: $confirmPass.val(),
								verifyCode: $verifyCode.val()
						};

						config.userName == !0 ? params.realname = $realname.val() : false;
						config.phone == !0 ? params.phone = $phone.val() : false;
						config.qq == !0 ? params.qqnum = $qqnum.val() : false;
						config.email == !0 ? params.email = $email.val() : false;
						config.receiptPwd == !0 ? params.cashpass = $cashpass.val() : false;
						config.cardNo == !0 ? params.cardNoNum = $cardNoNum.val() : false;
						config.wechat == !0 ? params.wechat = $wechat.val() : false;

						$.ajax({
							type : "POST",
							url : stationUrl + "/toregister.do",
							data : params,
							success : function(result) {
								if(result.success == false){
									// $.alertB(result.msg);
									Modal.alert({
										msg : result.msg
									}).on(function(e) {
										return;
									});
									$("#verifyCodeImg").attr("src", "${base }/mobile/regVerifycode.do?" + new Date());
								} else if(result.success == true){
									// $.alert("注册成功");
									Modal.alert({
										msg : "注册成功"
									}).on(function(e) {
										location.href = "${base }/mobile/index.do";
										return;
									});
								}
							}
						});
					});
				}
			}
		});
	});
</script>
</html>