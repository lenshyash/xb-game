
$(function(){
	$("#selectQueryType span").on("click", function(){
		var querytype = $(this).data("querytype");
		$("#selectQueryType").data("current", querytype).trigger("change");
	});

	$("#selectQueryType").on("change", function(){
		reloadRecordView();
	});

	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});

	$("#queryBallcode").on("change", function(){
		reloadRecordView();
	});

	var reloadRecordView = function () {
		$._ajax({url: stationUrl + "/betRecord/getSportsBetRecord.do", data:{querytype: $("#selectQueryType").data("current"), datetime: $("#queryDatetime").data("value"), ballcode: $("#queryBallcode").data("value")}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredTemplate").html());
			$("#recordContent").html(tpl({data : data}));
		});
	
	}

	reloadRecordView();
});