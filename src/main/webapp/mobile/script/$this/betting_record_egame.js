
$(function(){
	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});
	$("#queryEgameType").on("change", function(){
		reloadRecordView();
	});
	var reloadRecordView = function () {
		$._ajax({url: stationUrl + "/betRecord/getEgameBetRecord.do", data:{dateType: $("#queryDatetime").data("value"),egameType:$("#queryEgameType").data("value")}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredTemplate").html());
			$("#recordContent").html(tpl({data : data}));
			var bs = result.responseJSON.sumBet;
			var ws = result.responseJSON.sumWin;
			if(!bs){
				bs = '0';
			}
			if(!ws){
				ws = '0';
			}
			$('#sumBet').html(bs+'元');
			$('#sumWin').html(ws+'元');
		});
	
	}

	reloadRecordView();
});