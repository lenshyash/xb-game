
$(function(){
	var reloadRecordView = function () {
		$._ajax({url: stationUrl + "/lotteryBet/getBcLotteryOrderDetail.do", data:{lotCode: lotCode, orderId: orderId}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredDetailsTemplate").html());
			$("#innerbox.recordDetailsContent").html(tpl({data : $.extend(data.bcLotteryOrder, {version: data.version})}));
		});
	}

	reloadRecordView();
});