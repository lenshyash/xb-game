
$(function(){

	$(function(){
		$._ajax(stationUrl + "/member/meminfo.do", function (status, message, data, result) {
			console.log(data);
			$(".meminfoMoney").html(data.money);
		});
	});

	$("#selectQueryType span").on("click", function(){
		var querytype = $(this).data("querytype");
		$("#selectQueryType").data("current", querytype).trigger("change");
	});

	$("#selectQueryType").on("change", function(){
		reloadRecordView();
	});

	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});

	$("#queryCondition").on("change", function(){
		reloadRecordView();
	});

	var reloadRecordView = function () {
		$._ajax({url: stationUrl + "/betRecord/getMoneyRecord.do", data:{querytype: $("#selectQueryType").data("current"), datetime: $("#queryDatetime").data("value"), condition: $("#queryCondition").data("value")}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredTemplate").html());
			$("#recordContent").html(tpl({data : data}));
		});
	
	}

	reloadRecordView();
});