$(function(){
	$._ajax({url: stationUrl + "/lottery/getAllActiveLot.do", async: !0}, function (status, message, data, result) {
		var $lotterySelUl = $("#lotterySelUl").empty();
		for(var key in data){
			var value = data[key];
			value.version = result.responseJSON.version;
			if(value.code != "LHC"){
				$lotterySelUl.append('<li data-value="' + value.code + '">' + value.name + '</li>');
			}
		}
	});
	$("#selectQueryType span").on("click", function(){
		var querytype = $(this).data("querytype");
		$("#selectQueryType").data("current", querytype).trigger("change");
	});

	$("#selectQueryType").on("change", function(){
		reloadRecordView();
	});

	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});

	$("#queryLotCode").on("change", function(){
		reloadRecordView();
	});

	var reloadRecordView = function (currentPageNo, pageSize) {
		currentPageNo = currentPageNo || 1;
		pageSize = pageSize || 20;
		$._ajax({url: stationUrl + "/lotteryBet/getBcLotteryOrder.do", data:{querytype: $("#selectQueryType").data("current"), datetime: $("#queryDatetime").data("value"), lotcode: $("#queryLotCode").data("value"), page: currentPageNo, rows: pageSize}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredNewTemplate").html());
			var version = result.responseJSON.version;
			$("#recordContent").html(tpl({data : data.list, version: version}));
			var bs = data.aggsData.buySum;
			var ws = data.aggsData.winSum;
			if(!bs){
				bs = '0';
			}
			if(!ws){
				ws = '0';
			}
			$('#sumBet').html(bs+'元');
			$('#sumWin').html(ws+'元');
			var xs = data.list.length;
			if(xs > 0){
				var xb = data.list[xs-1].xBet;
				var xw = data.list[xs-1].xWin;
				if(!xb){
					xb = '0';
				}
				if(!xw){
					xw = '0';
				}
				$('#xBet').html(xb+'元');
				$('#xWin').html(xw+'元');
			}
			$("#recordContent").find("a[name='chedan']").on("click", function(){
				var $tr=$(this).parents("tr");
				Modal.confirm({msg:"确定撤销订单？"}).on(function(f){
					if(f){
						$.ajax({
							url: baseUrl + '/lotteryBet/cancelOrder.do',
							data: {
								orderId : $tr.data("orderid"),
								lotCode : $tr.data("lotcode"),
							},
							success: function(r){
								if(r.success == false){
									Modal.alert({msg :r.msg});
								} else {
									Modal.alert({msg :"撤单成功"});
									reloadRecordView();
								}
							}
						});
					}
				});
			});

			var pageviewTpl = _.template($("#pageviewTemplate").html());
			$("#pageview").html(pageviewTpl({data: data}));

			$("#pageview .pagination li a").on("click", function(){
				var pageNo = $(this).attr("pageno");
				reloadRecordView(pageNo);
			});
		});
	}

	reloadRecordView();
});