
$(function(){
	$("#selectQueryType span").on("click", function(){
		var querytype = $(this).data("querytype");
		$("#selectQueryType").data("current", querytype).trigger("change");
	});

	$("#selectQueryType").on("change", function(){
		reloadRecordView();
	});

	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});

	var reloadRecordView = function (currentPageNo, pageSize) {
		currentPageNo = currentPageNo || 1;
		pageSize = pageSize || 20;

		$._ajax({url: stationUrl + "/lotteryBet/getBcLotteryOrder.do", data:{querytype: $("#selectQueryType").data("current"), datetime: $("#queryDatetime").data("value"), lotcode: "LHC", page: currentPageNo, rows: pageSize}}, function (status, message, data, result) {
			var tpl = _.template($("#recoredNewTemplate").html());
			$("#recordContent").html(tpl({data : data.list}));
			var bs = data.aggsData.buySum;
			var ws = data.aggsData.winSum;
			if(!bs){
				bs = '0';
			}
			if(!ws){
				ws = '0';
			}
			$('#sumBet').html(bs+'元');
			$('#sumWin').html(ws+'元');
			
			var pageviewTpl = _.template($("#pageviewTemplate").html());
			$("#pageview").html(pageviewTpl({data: data}));

			$("#pageview .pagination li a").on("click", function(){
				var pageNo = $(this).attr("pageno");
				reloadRecordView(pageNo);
			});

			$("a[name=chedan]").on("click", function(){
				var orderId = $(this).parents("tr").data("orderid");
				var lotCode = $(this).parents("tr").data("lotcode");
				$.dialog({
					title: "系统讯息",
					content: "确定撤销订单？",
					canel: function () {
						return;
					}, canelValue: "取消撤单",
					ok: function () {
						$.ajax({
							url: baseUrl + "/lotteryV2Bet/cancelOrder.do",
							data: {
								orderId : orderId,
								lotCode : lotCode,
							},
							success: function(r){
								if(r.success == false){
									$.alert(r.msg);
								} else {
									$.alert("撤单成功");
									reloadRecordView();
								}
							}
						});
						
					}, okValue: "确认撤单"
				});
				
//                var confirm = window.confirm("确定撤销订单？");
//                if(confirm){
//						var orderId = $(this).parents("tr").data("orderid");
//						var lotCode = $(this).parents("tr").data("lotcode");
//						$.ajax({
//							url: baseUrl + "/lotteryV2Bet/cancelOrder.do",
//							data: {
//								orderId : orderId,
//								lotCode : lotCode,
//							},
//							success: function(r){
//								if(r.success == false){
//									alert(r.msg);
//								} else {
//									alert("撤单成功");
//									reloadRecordView();
//								}
//							}
//						});
//					}
                
                
                
				})
				
			});
	}

	reloadRecordView();
});