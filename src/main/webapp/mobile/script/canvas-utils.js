/**
 * 获取mimeType
 * 
 * @param {String}
 *            type the old mime-type
 * @return the new mime-type
 */
function _fixType(type) {
	type = type.toLowerCase().replace(/jpg/i, 'jpeg');
	var r = type.match(/png|jpeg|bmp|gif/)[0];
	return 'image/' + r;
};

/**
 * 在本地进行文件保存
 * 
 * @param {String}
 *            data 要保存到本地的图片数据
 * @param {String}
 *            filename 文件名
 */
function saveFile(data, filename) {
	var save_link = document.createElementNS('http://www.w3.org/1999/xhtml',
			'a');
	save_link.href = data;
	save_link.download = filename;

	var event = document.createEvent('MouseEvents');
	event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false,
			false, false, false, 0, null);
	save_link.dispatchEvent(event);
};

function getBase64Image(image) {
	var canvas = document.createElement("canvas");
	canvas.width = image.width;
	canvas.height = image.height;

	var ctx = canvas.getContext("2d");
	ctx.drawImage(image, 0, 0, image.width, image.height);
	var ext = image.src.substring(image.src.lastIndexOf(".") + 1).toLowerCase();
	var dataURL = canvas.toDataURL("image/" + ext);
	return dataURL;
}

function srcToBase64Image(imageSrc, callback){
	var image = new Image();
	image.crossOrigin = '';
	// image.origin = '';
	image.src = imageSrc;
	image.onload = function() {
		var base64 = getBase64Image(image);
		callback.apply(callback, [base64]);
	}
}