﻿var onClickEventString;
if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
	// onClickEventString = "touchend click";
	onClickEventString = "touchend";
}else{
	onClickEventString = "click";
}

function suofangFn(id, num) {
	var oWidth = document.documentElement.clientWidth
			|| document.body.clientWidth;
	var oHeight = document.documentElement.clientHeight
			|| document.body.clientHeight;
	var objx = document.getElementById(id);
	if (objx) {
		suofang = oWidth / num;
		oHeight = oHeight / suofang;
		if (oWidth < 640) {
			objx.style.position = "relative";
			objx.style.left = "0px";
			objx.style.top = "0px";

			objx.style.width = num + "px";
			/* objx.style.height = oHeight + "px"; */
			/* objx.style.webkitOverflowScrolling = "auto"; */
			objx.style.transformOrigin = "left top 0px";
			objx.style.webkitTransformOrigin = "left top 0px";
			objx.style.transform = "scale(" + suofang + ")";
			objx.style.webkitTransform = "scale(" + suofang + ")"
		} else {
			objx.style.width = num + "px";
		}
		return true;
	}
	return false;
}

function zishiying() {
	var hashka = suofangFn("innerbox", 640);
	if (hashka) {
		var oHeight = document.documentElement.clientHeight || document.body.clientHeight;
		oHeight = oHeight < 600 ? 600 : oHeight;
		// $("body").height(oHeight);
		$("body").height(800);
	}
}

function stopBuFn(e) {
	var e = e || event;
	if (e && e.stopPropagation) {
		e.stopPropagation();

	} else {
		e.cancelBubble = true;
	}
}

$(document).ready(
		function() {

			zishiying();

			function carouselTopFn(obj, stopTime, slipTime) {
				var stopTime = stopTime ? stopTime : 2000;
				var slipTime = slipTime ? slipTime : 600;
				var obj = $(obj);
				var tutu = obj.find('.tutu')
				var leftBtn = obj.find('.leftBtn')
				var rightBtn = obj.find('.rightBtn')
				var yuandian = obj.find('.yuandian')

				tutu.append(tutu.children('li').eq(0).clone(true));
				var len = tutu.children('li').height();
				var cLength = tutu.children('li').length;
				var iNow = 0;
				for (var i = 0; i < cLength - 1; i++) {
					yuandian.append('<span></span>')
				}
				yuandian.children().eq(0).addClass('cur')
				var leftBtn = function() {
					if (iNow > 0) {
						iNow--;
						tutu.stop().animate({
							top : -len * iNow
						}, slipTime);
						yuandian.children('span').eq(iNow).addClass('cur')
								.siblings().removeClass('cur');
					} else {
						tutu.css({
							top : -len * (cLength - 1)
						})
						iNow = cLength - 2;
						tutu.stop().animate({
							top : -len * iNow
						}, slipTime);
						yuandian.children('span').eq(iNow).addClass('cur')
								.siblings().removeClass('cur');
					}
				}
				var rightBtn = function() {
					if (iNow < cLength - 2) {
						iNow++;
						tutu.stop().animate({
							top : -len * iNow
						}, slipTime);
						yuandian.children('span').eq(iNow).addClass('cur')
								.siblings().removeClass('cur');
					} else {
						tutu.stop().animate({
							top : -len * (cLength - 1)
						}, slipTime, function() {
							tutu.css({
								top : 0
							});
							iNow = 0;
						});
						yuandian.children('span').eq(0).addClass('cur')
								.siblings().removeClass('cur');
					}
				}

				var times = window.setInterval(function() {
					rightBtn();
				}, stopTime)
				obj.hover(function() {
					window.clearInterval(times);
				}, function() {
					times = window.setInterval(function() {
						rightBtn();
					}, stopTime)
				})
				obj.find('.leftBtn').click(function() {
					leftBtn();
				})
				obj.find('.rightBtn').click(function() {
					rightBtn();
				})

				yuandian.children('span').click(
						function() {
							iNow = $(this).index();
							$(this).addClass('cur').siblings().removeClass(
									'cur');
							tutu.stop().animate({
								top : -len * iNow
							}, slipTime);
							yuandian.children('span').eq(iNow).addClass('cur')
									.siblings().removeClass('cur');
						})
			}

			// carouselTopFn('.lunleft', 12000)

			$(".wangqilu li:odd,.zhuihaotable tr:even").addClass("cur")

			$(".jinexuanzeul li").click(function() {
				$(this).addClass("cur").siblings().removeClass("cur")
			})

			$(".close-tan").click(function() {
				$(this).parents(".tanchuceng").fadeOut(200)
			})

			$(".touzhuqie span").click(
					function() {
						$(this).addClass("cur").siblings().removeClass("cur")
						var x = $(this).index()
						$(".tzhuanqie-huan").eq(x).addClass("cur").siblings()
								.removeClass("cur")
					})

			$(".shanchubtn-tan").click(function() {
				$(".querensc-tan").fadeIn(200)
			})

			$(".kjgongxtable tr:odd").addClass("odd")

			$(".kaijinghaomaul li").click(function() {
				$(this).find(".kjgonggaoxla").toggle()
				$(this).find(".xiaicon-m").toggle()
			});

			// 下拉框组件
			(function(){
				$(document).click(function() {
					$(".sjxialabox").hide();
				});
				$(document).delegate(".selectComponent", onClickEventString, function(e) {
					stopBuFn(e);
					$(this).find(".sjxialabox").toggle();
				});
				$(document).delegate(".selectComponent ul li", onClickEventString,
						function() {
							var htmlx = $(this).html();
							$(this).parents(".sjxialabox").siblings(".fangsjbox")
									.html(htmlx);

							var $selectComponent = $(this).parents(
									".selectComponent:first");
							if ($selectComponent.data("value") != $(this).data(
									"value")) {
								$selectComponent.data("value",
										$(this).data("value")).trigger("change");
							}
							$selectComponent.trigger("select");
						});

			})();

			_.templateSettings = {
				evaluate : /\{#([\s\S]+?)#\}/g,
				interpolate : /\{\{(.+?)\}\}/g
			};
		});
