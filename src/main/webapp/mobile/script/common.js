
$.extend({
	alert: function(message, callback){
		var $body = $("body"), $iAlertKkkDiv = $("#iAlertKkkDiv");
		if(!$iAlertKkkDiv.length){
			$iAlertKkkDiv = $('<div id="iAlertKkkDiv" class="tanchuceng" style="display: none;"><div class="tanchuin"><div class="tangbiaoti">系统讯息</div><div class="tannr"><div class="l20 tac" id="alertContent">这里是提示内容</div><div class="tannrbtn"><a id="okBtn" href="javascript: void(0);" class="">确定</a></div></div></div></div>');
			$okBtn = $iAlertKkkDiv.find("#okBtn");
			$okBtn.on("click", function(){
				$iAlertKkkDiv.hide();
				if(callback){
					callback.apply(callback, []);
				}
			});
		}
		var $alertContent = $iAlertKkkDiv.find("#alertContent");
		$alertContent.html(message);
		$body.append($iAlertKkkDiv);
		$iAlertKkkDiv.show();
	},
	alertB: function(message){
		var $body = $("body"), $iAlertBKkkDiv = $("#iAlertBKkkDiv");
		if(!$iAlertBKkkDiv.length){
			$iAlertBKkkDiv = $('<div id="iAlertBKkkDiv" class="tanchuceng" style="display: none;"><div class="tanchuin" style="background:#666;"><div class="tangbiaoti">系统讯息</div><div class="tannr"><div class="l20 tac" id="alertContent">这里是提示内容</div><div class="tannrbtn"><a id="okBtn" href="javascript: void(0);" class="">确定</a></div></div></div></div>');
			$okBtn = $iAlertBKkkDiv.find("#okBtn");
			$okBtn.on("click", function(){
				$iAlertBKkkDiv.hide();
			});
		}
		var $alertContent = $iAlertBKkkDiv.find("#alertContent");
		$alertContent.html(message);
		$body.append($iAlertBKkkDiv);
		$iAlertBKkkDiv.show();
	},
	dialog: function(options){
		options = options || {};
		options.title = options.title || "温馨提示";
		options.content = options.content || "";
		options.ok = options.ok || function(){};
		options.okValue = options.okValue || "确定";
		options.canel = options.canel || function(){};
		options.canelValue = options.canelValue || "取消";
		options.width = options.width || 240;

		var $body = $("body"), $iDialogKkkDiv = $("#iDialogKkkDiv");
		if(!$iDialogKkkDiv.length){
			$iDialogKkkDiv = $('<div id="iDialogKkkDiv" class="tanchuceng querensc-tan" ><div class="tanchuin"><div id="title" class="tangbiaoti">温馨提示</div><div class="tannr"><div id="content" class="l30 tac">确定删除此投注号码</div><div class="tannrbtn"><a id="canelBtn" href="javascript: void(0);" class="close-tan" >取消</a><a id="okBtn" href="javascript: void(0);" class="close-tan" >确定</a></div></div></div></div>');
		}
		var $content = $iDialogKkkDiv.find("#content"), $title = $iDialogKkkDiv.find("#title"), $okBtn = $iDialogKkkDiv.find("#okBtn"), $canelBtn = $iDialogKkkDiv.find("#canelBtn"),
			$tanchuin = $iDialogKkkDiv.find("div.tanchuin"), $tannr = $iDialogKkkDiv.find("div.tannr");
		var close = function(){$iDialogKkkDiv.hide();};
		$okBtn.off("click").on("click", options.ok).on("click", close).text(options.okValue);
		$canelBtn.off("click").on("click", options.canel).on("click", close).text(options.canelValue);
		$title.text(options.title);
		$content.html(options.content);
		$tanchuin.css("width", options.width + "px");
		$tannr.css("width", (options.width - 10) + "px");
		$body.append($iDialogKkkDiv);
		$iDialogKkkDiv.show();
		return $iDialogKkkDiv;
	},
	getStrResouce: function (url){
		var resourceContent = "";
		$.ajax({
			type : "GET",
			url : url,
			dataType : "text",
			data : {},
			async : false, // 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。注意，同步请求将锁住浏览器，用户其它操作必须等待请求完成才可以执行。
			success : function(result) {
				resourceContent = result;
			}
		});
		return resourceContent;
	},
	_ajax: function (arg, FN) {
		if (_.isString(arg)){
			arg = {
				url: arg
			};
		} else if (!_.isObject(arg)) throw "第一个参数必须是物件(ajax 的设定档)或字串";
		$.isFunction(FN) || (FN = function () {});
		return $.ajax($.extend({}, {
			async: !0,
			cache: !1,
			dataType: "json"
		
		}, arg, {
			beforeSend: function (h) {
				
			}, success: function (h, m, r) {
				if($.http.isLogout(h.code)){
					// G = !1;
					// k.user.personInfoIsSet(null);
					// k.ext.isSecretBrowse() || $.map(A, function (h) {localStorage.removeItem(h)});
					// k.event.trigger("ajaxLogout", !1, h.message, h.data, r);
				} else {
					// k.ext.isSecretBrowse() || $.map(A, function (h) {var k = $.cookie(h); k && localStorage.setItem(h,k)});
					FN($.http.isSucc(h.code), h.message, h.data, r);
				}
			}, error: function (h, m, r) {
				m = {
					code: h.status,
					message: r
				};
				try {
					m = JSON.parse(h.responseText)
				} catch (v) {}

				h.status = m.code;

				if($.http.isLogout(m.code)) {
					// G = !1, k.ext.isSecretBrowse() || $.map(A, function (h) { localStorage.removeItem(h) }), k.event.trigger("ajaxLogout", !1, m.message, m.data, h)
				} else {
					FN($.http.isSucc(m.code), m.message, m.data, h);
				}
			}
		})).done(function(h, k, u){console.debug("ajax Done! %s, %s, %o", h, k, u)}).fail(function(h, k, u){console.debug("ajax error! %s, %s, %o", h, k, u)});
	},
	http: {
		isLogout: function (h) {
			return -1 !== $.inArray(1 * h, [401, 403, 503]) ? !0 : !1;
		},
		isSucc: function (h) {
			return -1 !== $.inArray(1 * h / 100, [1, 2, 3]) ? !0 : !1;
		}
	}
});

//对Date的扩展，将 Date 转化为指定格式的String
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
//例子：
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
//(new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
Date.prototype.format = function(fmt) { // author: meizz
	fmt = fmt || "yyyy-MM-dd HH:mm:ss";
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"HH+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

//复制到剪切板js代码 只支持IE及以IE为内核的浏览器。
function copyToClipBoard(s) {
	if (window.clipboardData) {
		window.clipboardData.setData("Text", s);
		alert("已经复制到剪切板！" + "\n" + s);
	} else if (navigator.userAgent.indexOf("Opera") != -1) {
		window.location = s;
	} else if (window.netscape) {
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
		} catch (e) {
			alert("被浏览器拒绝！\n请在浏览器地址栏输入'about:config'并回车\n然后将'signed.applets.codebase_principal_support'设置为'true'");
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
			return;
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
		if (!trans)
			return;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
		var copytext = s;
		str.data = copytext;
		trans.setTransferData("text/unicode", str, copytext.length * 2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
			return false;
		clip.setData(trans, null, clipid.kGlobalClipboard);
		alert("已经复制到剪切板！" + "\n" + s)
	} else {
		alert("此功能不支持该浏览器,请手动复制内容");
	}
}

// /////////////////////////////////////////////////
function setCookie(c_name, value, expiredays) {
	var exdate = new Date()
	exdate.setDate(exdate.getDate() + expiredays)
	document.cookie = c_name + "=" + escape(value)
			+ ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
}
function getCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=")
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1
			c_end = document.cookie.indexOf(";", c_start)
			if (c_end == -1)
				c_end = document.cookie.length
			return unescape(document.cookie.substring(c_start, c_end))
		}
	}
	return ""
}

var plus_os_name = getCookie("plus.os.name");

var identification = "WebBrowser";

switch (plus_os_name) {
case "Android":
	// Android平台: plus.android.*
	// alert("Android");
	identification = "Android";
	break;
case "iOS":
	// iOS平台: plus.ios.*
	// alert("iOS");
	identification = "IOS";
	break;
default:
	identification = "WebBrowser";
	break;
};
