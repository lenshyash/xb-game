﻿<%@page import="com.game.constant.BusinessConstant"%>
<%@page import="com.game.util.StationConfigUtil"%>
<%@ page import="com.game.model.lottery.LotteryEnum"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../include/common.jsp"%>
<%
String lotCode = request.getParameter("lotCode");
if (StringUtils.isEmpty(lotCode)) {
	lotCode = "LHC";
}

LotteryEnum lotteryEnum = LotteryEnum.getEnum(lotCode);
pageContext.setAttribute("lotCode", lotCode);
pageContext.setAttribute("lotName", lotteryEnum.getLotName());

Long stationId = StationUtil.getStation().getId();
String template = StationConfigUtil.get(stationId, StationConfig.lottery_template_name);
Integer version = 1;
if(StringUtils.isNotEmpty(template) && template.startsWith("v2")){
	version = 2;
}

pageContext.setAttribute("version", version);
%>
<!DOCTYPE html>
<html class="ui-mobile">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<title>${website_name }</title>
<link href="${station }/lhc/content/bootstrap.min.css" rel="stylesheet" />
<link href="${station }/lhc/content/layout.css?v=2" rel="stylesheet" />
<script type="text/javascript" src="${station }/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${station }/script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="${station }/script/global.js"></script>
<script type="text/javascript" src="${station }/script/common.js"></script>
<style type="text/css">
.modal-body{
    height: 50%;
    overflow: scroll;
}

.footercontent {
	padding: 3px 0px;
}
</style>
</head>
<body>
	<div class="bg-info header-logo">
		<div class="pull-left register">
			<a href="${station }/index.do"> <span
				class="glyphicon glyphicon-home fontsize"></span>
			</a>
		</div>
		<div class="pull-right register">
			<c:if test="${version=='2'}">
				<a class="btn" style="background: #DBE6E9; font-weight: bold; border-radius: 10px;" onclick="caizhongxz()"> 彩种选择 <span class="caret"></span></a>
			</c:if>
		</div>
		<div style="clear: both"></div>
	</div>
	<!-- <div class="bg-info header-logo text-center fontwidth">彩票游戏</div> -->
	<div class="body-banner" id="caizhong" style="display: none;"></div>
	<script>
		$(function(){
			$._ajax(stationUrl + "/lottery/getAllActiveLot.do", function (status, message, data, result) {
				var tplHtml = $("#lotteryItemTemplate").html();
				var $caizhong = $("#caizhong").empty();
				$caizhong.html(_.template(tplHtml)({list: data, version: "${version}"}));
			});
		});
	</script>
	<script type="text/html" style="display: none;" id="lotteryItemTemplate">
		<table>
			{#
				_.each(list, function(item, index, list){
			#}
			{# print((index == 0) ? '<tr>' : ''); #}
			{# print((index != 0 && index % 3 == 0 && index != list.length - 1) ? '</tr><tr>' : ''); #}
				<td><a data-gname="{{item.code}}" class="{{item.code == '${lotCode }' ? 'gameActive' : ''}}" href="{# if( item.code != 'LHC' && item.code != 'SFLHC') { print(version == '2' ? '${station}/v2/lottery.do?lotCode=' + item.code : '${station}/do_pick_number.do?lotCode=' + item.code ); } else { print('${station}/lhc/index.do?lotCode=' + item.code); } #}"><b>{{item.name}}</b><br /><!-- <span class="reciprocal_lhc">00:00:00</span> --></a></td>
			{# print((index == list.length - 1) ? '</tr>' : ''); #}
			{#
				});
			#}
		</table>
	</script>
	<link href="${station }/lhc/content/LHC.css" rel="stylesheet" />
	<style>
	span[class^=span_ball_] {
		height: 26px;
		width: 26px;
		display: inline-block;
		color: #fff;
		padding-top: 2px;
	}
	</style>
	<div class="lhcGame postionre">
		<div class="text-center paddingtopbotton5 result-time-bg">
			第 <span class="text-danger" id="lastResultTerm"></span> 期开奖结果&nbsp;&nbsp;&nbsp;&nbsp; <a href="${station }/draw_notice_details.do?lotCode=${lotCode }" class="btn btn-info btn-sm">历史开奖</a>
		</div>
		<div class="text-center result-con">
			<span id="d_ball1"></span> 
			<span id="d_ball2"></span> 
			<span id="d_ball3"></span> 
			<span id="d_ball4"></span> 
			<span id="d_ball5"></span>
			<span id="d_ball6"></span> 
			<span>+</span> 
			<span id="d_ball7"></span>
		</div>
		<div class="text-center paddingtopbotton5 result-time-bg">
			<span class="fontwidth text-red">${lotName }</span>&nbsp;&nbsp;&nbsp;余额:&nbsp;<span class="text-red" id="createshow"></span>&nbsp; <a href="<c:if test="${lotCode eq 'LHC'}">${station }/betting_record_sixmark.do</c:if><c:if test="${lotCode eq 'SFLHC'}">${station }/betting_record_lottery.do</c:if>" class="btn btn-info btn-sm">下注记录</a>
		</div>
		<div class="text-center paddingtopbotton5 result-time-bg">
			<span class="text-danger memRound_lhc"></span> 期封盘时间：<span class="text-right text-red fengpantime"></span> <span class="reciprocal_lhc">00:00:00</span>
		</div>
		<div id="markSixPlayGroupsDiv" class="gamename">
			<!-- 
			<label class="selected">特码</label>
			<label>正码</label>
			<label>特码生肖</label>
			<label>一肖</label>
			<label>正肖</label>
			<label>连肖</label>
			<label>连尾</label>
			<label>合肖</label>
			<label>连码</label>
			<label>自选不中</label>
			 -->
		</div>
		<script type="text/javascript">
			$(function(){
				$._ajax({url: stationUrl + "/marksix/getMarkSixPlayGroups.do", async: !0, data: {lotCode: "${lotCode}"}}, function (status, message, data, result) {
					var tpl = $("#markSixPlayGroupsTemplate").html();
					$("#markSixPlayGroupsDiv").html(_.template(tpl)({list: data}));

					//设置LHC主选单的显示隐藏事件
					$("#markSixPlayGroupsDiv label").each(function() {
						$(this).click(function() {
							$(this).addClass('selected').siblings('label').removeClass('selected');
							
							var groupid = $(this).data("groupid");
							var groupcode = $(this).data("groupcode");
							$._ajax({url: stationUrl + "/lottery/getPlays.do", async: !0, data: {groupId: groupid}}, function (status, message, data, result) {
								var tpl = $("#markSixPlayTemplate").html();
								$("#playButtonDiv").html(_.template(tpl)({list: data}));

								$("#playButtonDiv button").unbind();
								$("#playButtonDiv button").each(function(){
									var $this = $(this);
									$this.click(function(){
										var playcode = $this.data("playcode");
										var detaildesc = $this.data("detaildesc");
										var winexample = $this.data("winexample");
										var lottype = $this.data("lottype");

										$("#playDetailDesc").html(detaildesc);
										$("#playWinExample").html(winexample);

										var resurce = $.getStrResouce(stationUrl + "/lhc/template/" + groupcode + "/" + playcode + ".html?t=6");
										$(".ball-content").html(resurce);

										shengXiaoNumsMethed(playcode);
										getGamesOdds(playcode, lottype);
										KuaiJieShuRu_lhc();

										$this.addClass('active').siblings('button').removeClass('active');
										$(".Content_TeMa input[type=text]").val('');//清空文本框数据
										$(".Content_TeMa .betmoney").val('');//清空金额
										$(".qingkong").click();
									});
								});
								$("#playButtonDiv button:eq(0)").click();
							});
							
							// Setgamename_tab($(this).text());
							Setgamename_tab(groupcode);
						});
					});
					$("#markSixPlayGroupsDiv label:eq(0)").click();//第一个点击
				});
			});
		</script>
		<script id="markSixPlayGroupsTemplate" type="text/html" style="display: none;">
				{#
				_.map(list, function(item, index){
				if(item.code != 'zm16'){
				#}
				<label class="{{index == 0 ? 'selected' : ''}}" data-groupid="{{item.id }}" data-groupcode="{{item.code}}">{{item.name}}</label>
				{#
				}
				});
				#}
		</script>
		<script id="markSixPlayTemplate" type="text/html" style="display: none;">
				{#
				_.map(list, function(item, index){
				#}
				<button type="button" class="btn btn-default {{index == 0 ? 'active' : ''}}" data-playcode="{{item.code}}" data-detaildesc="{{item.detailDesc}}" data-winexample="{{item.winExample}}" data-lottype="{{item.lotType}}">{{item.name}}</button>
				{#
				});
				#}
		</script>

		<div class="fengpan hide">封盘中</div>
		<div class="gamename kuaijie">
			<div class="form-group" style="margin-bottom: 5px;">
				<div class="input-group">
					<span class="input-group-addon form_span">快捷金额</span> <input type="text" class="form-control" id="kjmoney" placeholder="输入后点击赔率"> <span class="input-group-addon form_span qingkong">清空</span> <span class="input-group-addon form_span" id="kjbet">下注</span>
				</div>
			</div>
		</div>

		<!-- 页面内容 -->
		<!--特码生肖-->
		<div class="Content_TeMa all_Content">
			<div id="playButtonDiv" class="btn-group result-time-bg btn-group-sm">
				<!-- 
				<button type="button" class="btn btn-default active">特码</button>
				<button type="button" class="btn btn-default">特码B</button>
				<button type="button" class="btn btn-default">半波</button>
				 -->
			</div>
			<div style="background-color: #F7FBFD;">
				<button type="button" class="btn btn-default btn-sm pull-right"  data-toggle="modal" onclick="showPlayDesc();">玩法说明</button>
				<div style="clear: both"></div>
			</div>
			<div class="ball-content">
			</div>
		</div>
		<script type="text/javascript">
			function showPlayDesc(){
				$('#playDescModal').modal({
	//						keyboard: true
				});
			}
		</script>
		<div class="modal fade" id="playDescModal" tabindex="-1" role="dialog" aria-labelledby="playDescModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="playDescModalLabel">玩法说明</h4>
					</div>
					<div class="modal-body">
						<!-- <div><span><b>玩法介绍</b></span></div> -->
						<div id="playDetailDesc"></div>
						<!-- <div style="margin: 20px 0px 0px 0px;"><span><b>投注说明</b></span></div> -->
						<div id="playWinExample"></div>
						<div style="margin: 20px 0px"></div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" value="${lotCode }" id="GameType" /> 
		<input type="hidden" value="" id="rateCha" /> 
		<input type="hidden" value="" id="Credit" />
	</div>
	<script>
		var game = $("#GameType").val();
		$(function() {
			//初始化：取开奖结果
			execGetLastResult();

			getXiaoNums();//取生肖号码

			$("#kjmoney").attr("oninput", "kjmoneyChange_lhc()");
		});
		function kjmoneyChange_lhc() {
			$("td.inputed").next("td").children("input[type=text]").val($("#kjmoney").val());
			$("td.inputed").next("td").next("td").children("input[type=text]").val($("#kjmoney").val());
		}

		//快捷输入
		function KuaiJieShuRu_lhc() {
			$("span.text-red").parent("td").each(function() {
				$(this).click(function() {
					$(this).prev("td").toggleClass("inputed");
					$(this).toggleClass("inputed");
					$(this).next("td").toggleClass("inputed");
					var kj = $("#kjmoney").val();
					if ($(this).attr("class").indexOf("inputed") > -1) {
						$(this).next("td").children("input[type=text]").val(kj);
					} else {
						$(this).next("td").children("input[type=text]").val('');
					}
				});
			});
			$("span.text-red").parent("td").next("td").each(function() {
				$(this).click(function() {
					$(this).prev("td").click();
				});
			});
			$("span.text-red").parent("td").prev("td").each(function() {
				$(this).click(function() {
					$(this).next("td").click();
				});
			});

			$("b.text-red").parent("td").each(function() {
				$(this).click(function() {
					$(this).toggleClass("inputed");
					var kj = $("#kjmoney").val();
					if ($(this).attr("class").indexOf("inputed") > -1) {
						$(this).next("td").next("td").children("input[type=text]").val(kj);
					} else {
						$(this).next("td").next("td").children("input[type=text]").val('');
					}

					var groupcode = $(".gamename label.selected").data("groupcode");
					if (groupcode == "wsl" || groupcode == "lx") {
						if ($(this).attr("class").indexOf("inputed") > -1) {
							$(this).next("td").next("td").children("input[type=checkbox]").prop("checked", true);
						} else {
							$(this).next("td").next("td").children("input[type=checkbox]").prop("checked", false);
						}
					}
				});
			});
		}

		//设置LHC主选单的显示隐藏事件
		function Setgamename_tab(text) {
			$(".qingkong").click();
			switch (text) {
			case "tm": { // 特码
				$(".kuaijie").removeClass("hide");
				// GetTMRate_forB();//特码B赔率差
				break;
			}
			case "zm": { // 正码
				$(".kuaijie").removeClass("hide");
				break;
			}
			case "ztm": { // 正特码
				$(".kuaijie").removeClass("hide");
				break;
			}
			case "lm": { // 连码
				$(".kuaijie").addClass("hide");
				break;
			}
			case "tmbb": { // 特码半波
				$(".kuaijie").removeClass("hide");
				break;
			}
			case "yxws": { // 一肖
				$(".kuaijie").removeClass("hide");
				break;
			}
			case "tmsx": { // 特码生肖
				$(".kuaijie").removeClass("hide");
				break;
			}
			case "hx": { // 合肖
				$(".kuaijie").addClass("hide");
				hxCheckboxClick();
				break;
			}
			case "qbz": { // 全不中
				$(".kuaijie").addClass("hide");
				hxCheckboxClick();
				break;
			}
			case "wsl": { // 连尾
				$(".kuaijie").addClass("hide");
				break;
			}
			case "lx": { // 连肖
				$(".kuaijie").addClass("hide");
				break;
			}
			default:
				break;
			}
		}
		//合肖多选框点击事件
		function hxCheckboxClick() {
			var rateCount = 0;
			var allrate = 0;
			$(".Content_HeXiao input[type=checkbox]").unbind();
			$(".Content_HeXiao input[type=checkbox]").each(function() {
				$(this).click(function() {
					var hxGame = $(".hxButton button.active").html();
					var bzrate = parseFloat($("#rateCount").val()) * 1000;
					var zrate = 0;
					if ($(this).is(":checked")) {
						rateCount = $(".Content_HeXiao input[type=checkbox]:checked").length;
						if (rateCount > 11) {
							Modal.alert({msg : '选择不能超过11位！'}).on(function(e) {
								$(".Content_HeXiao input[type=checkbox]").removeAttr("checked");//清空文本框数据
								$("#hxrate").html('0.00');
								return;
							});
						}
						for (var i = 0; i < rateCount; i++) {
							if (hxGame == "中") {
								zrate += parseInt(parseFloat($(this).attr("rate")) * 1000);
								allrate = zrate / rateCount / rateCount / 1000;
							} else {
								bzrate -= parseInt(parseFloat($(this).attr("rate")) * 1000);
								allrate = bzrate / (12 - rateCount) / (12 - rateCount) / 1000;
							}
						}
						$("#hxrate").html(newFixed(allrate, 2));
					}
				});
			});
		}

		function newFixed(num, len) {
			var add = 0;
			var s, temp;
			var s1 = num + "";
			var start = s1.indexOf(".");
			if (s1.substr(start + len + 1, 1) >= 5)
				add = 1;
			var temp = Math.pow(10, len);
			s = Math.floor(num * temp) + add;
			return s / temp;
		}

		//上一期开奖结果
		function GetLastResult() {
			$.ajax({
				url : stationUrl + '/marksix/getLastResult.do',
				data: {lotCode: "${lotCode}"},
				success : function(data) {
					if (!data.success) {
						return;
					}
					var result = data.data;
					$("#lastResultTerm").html(result.dropDate);
					$("#d_ball1").attr("class", "span_ball_" + (result.num1 == "?" ? "04" : result.num1)).html(result.num1);
					$("#d_ball2").attr("class", "span_ball_" + (result.num2 == "?" ? "04" : result.num2)).html(result.num2);
					$("#d_ball3").attr("class", "span_ball_" + (result.num3 == "?" ? "04" : result.num3)).html(result.num3);
					$("#d_ball4").attr("class", "span_ball_" + (result.num4 == "?" ? "04" : result.num4)).html(result.num4);
					$("#d_ball5").attr("class", "span_ball_" + (result.num5 == "?" ? "04" : result.num5)).html(result.num5);
					$("#d_ball6").attr("class", "span_ball_" + (result.num6 == "?" ? "04" : result.num6)).html(result.num6);
					$("#d_ball7").attr("class", "span_ball_" + (result.num7 == "?" ? "01" : result.num7)).html(result.num7);
				}
			});
		}

		function execGetLastResult() {
			GetLastResult();
			GetGameOpenInfo();
			setInterval("GetLastResult()", 1000 * 20);
			//开盘信息_LHC
			setInterval("GetGameOpenInfo()", 1000 * 20);
		}
		//取开盘信息_LHC
		function GetGameOpenInfo() {
			var url = '${station }/marksix/getGameOpenInfo.do';
			var code = "${lotCode}";
			if(code && code == 'SFLHC'){
				url = '${station }/v2/getGameOpenInfo.do';
			}
			$.ajax({
				url : url,
				data: {lotCode: "${lotCode}"},
				async : true,
				error : function(result) {
				},
				success : function(result) {
					if (!result.success) {
						Modal.alert({
							msg : result.msg
						}).on(function() {
							// location.href = location.href;
							if(result.msg != "目前尚未开盘"){
								// location.href = stationUrl + "/index.do";
							}
						});
						return;
					} else {
						var Current_Term = result.data.dropDate;
						var CloseTime = result.data.closeTime;
						var IsOpen = result.data.isOpen;
						if (IsOpen) {
							$(".fengpan").addClass("hide");
							CloseTime = CloseTime.substring(0, CloseTime.length - 3);
							$(".fengpantime").html(CloseTime);//封盘时间
							$(".memRound_lhc").html(Current_Term);//新的开奖期数
						} else {
							$(".fengpan").removeClass("hide");
							$("input[type=text]").val('');
							$("input[type=checkbox]").removeAttr("checked");
							$(".qingkong").click();
						}
					}
				}
			});
		}

		//取赔率的方法
		function getGamesOdds(playCode, lottype) {
			var data = {
				"playCode" : playCode,
				"lotType" : lottype
			};
			$('#myModal').modal('show');
			$.ajax({
				type : "post",
				url : stationUrl + "/marksix/getOdds.do",
				data : data,
				async : false,
				error : function(dt) {
				},
				success : function(res) {
					if(res.success){
						switch(playCode){
						case "tm_a":
						case "tm_b":
						case "zm_a":
						case "zm_b":
						case "z1t":
						case "z2t":
						case "z3t":
						case "z4t":
						case "z5t":
						case "z6t":
							var qita = [];
							_.each(res.data, function(item, index, list){
								if(item.markType == "shuzi"){
									$(".marksix_content [data-val='"+item.name+"']").data("marksixid", item.id).html(item.odds); 
								} else {
									qita.push(item);
								}
							});
							var tpl = $("#marksixFooterTemplate").html();
							$(".marksix_footer").html(_.template(tpl)({list: qita}));
							break;
						case "bb":
							var qita = [];
							_.each(res.data, function(item, index, list){
								if(item.markType == "shuzi"){
									// 忽略
								} else {
									qita.push(item);
								}
							});
							var tpl = $("#marksixFooterTemplate").html();
							$(".marksix_footer tr:not(.bg-warning)").remove();
							$(".marksix_footer").append(_.template(tpl)({list: qita}));
							break;
						case "ztxztw":
							_.each(res.data, function(item, index, list){
								if(item.markType == "shengxiao"){
									if(item.isNowYear == 1){
										$(".marksix_content [data-val=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									} else {
										$(".marksix_content [data-val!=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									}
								} else if(item.markType == "weishu") {
									if(item.isNowYear == 1){
										$(".marksix_footer span.text-red[data-val=0尾]").data("marksixid", item.id).html(item.odds);
									} else {
										$(".marksix_footer span.text-red[data-val!=0尾]").data("marksixid", item.id).html(item.odds);
									}
								}
							});
							break;
						case "szy":
						case "tc":
						case "eztzzt":
						case "ezt":
						case "eqz":
						case "szezzs":
						case "szes":
						case "sqz":
						case "siqz":
						case "eztzze":
						case "szezze":
							_.each(res.data, function(item, index, list){
								if(item.markType == "shuzi"){
									$(".marksixRate span.text-red").data("marksixid", item.id).html(item.odds);
								} else {
									// 忽略
								}
							});
							break;
						case "ba_bz":
						case "jiu_bz":
						case "liu_bz":
						case "qi_bz":
						case "shi_bz":
						case "shie_bz":
						case "shiy_bz":
						case "w_bz":
							_.each(res.data, function(item, index, list){
								if(item.markType == "shuzi"){
									$("#iiiiiirate").data("marksixid", item.id).html(item.odds);
								} else {
									// 忽略
								}
							});
							break;
						case "erx_lbz":
						case "erx_lz":
						case "sanx_lbz":
						case "sanx_lz":
						case "six_lbz":
						case "six_lz":
						case "wux_lz":
						case "txsm":
							_.each(res.data, function(item, index, list){
								if(item.markType == "shengxiao"){
									if(item.isNowYear == 1){
										$(".marksix_content tr[data-val=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									} else {
										$(".marksix_content tr[data-val!=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									}
								} else {
									// 忽略
								}
							});
							break;
						case "hx_bax":
						case "hx_ex":
						case "hx_jiux":
						case "hx_liux":
						case "hx_qix":
						case "hx_sanx":
						case "hx_shix":
						case "hx_six":
						case "hx_syx":
						case "hx_wux":
							_.each(res.data, function(item, index, list){
								if(item.markType == "shengxiao"){
									if(item.isNowYear == 1){
										$(".marksix_content [data-val=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									} else {
										$(".marksix_content [data-val!=" + window.nowYear + "] td b.text-red").data("marksixid", item.id).html(item.odds);
									}
									/*
									if(item.isNowYear == 1){
										// 忽略
									} else {
										$("#iiiiiirate").html(item.odds);
									}
									*/
								} else {
									// 忽略
								}
							});
							break;
						case "erw_lbz":
						case "erw_lz":
						case "sanw_lbz":
						case "sanw_lz":
						case "siw_lbz":
						case "siw_lz":
							_.each(res.data, function(item, index, list){
								if(item.markType == "weishu") {
									if(item.isNowYear == 1){
										$(".marksix_content tr[data-val=0尾] td b").data("marksixid", item.id).html(item.odds);
									} else {
										$(".marksix_content tr[data-val!=0尾] td b").data("marksixid", item.id).html(item.odds);
									}
								}
							});
							break;
						}
					} else {
						Modal.alert({
							msg : res.msg
						});
					}
				},
				complete : function(data) {
					$('#myModal').modal('hide');
				}
			});
		}
		//取特码赔率差
		function GetTMRate_forB() {
			$.ajax({
				url : '/SSC/Ajax/GetTMReateB',
				type : "post",
				async : false,
				success : function(data) {
					$("#rateCha").val(data);
				}
			});
		}
		//取生肖号码
		function getXiaoNums() {
			$.ajax({
				url : stationUrl + '/marksix/getXiaoNums.do',
				type : "post",
				success : function(data) {
					if(data.success){
						window.shenxiao = data.data;
						window.nowYear = data.nowYear;
					} else {
						Modal.alert({
							msg : data.msg
						});
					}
				}
			});
		}
		//生肖号码处理方法
		function shengXiaoNumsMethed(playcode) {
			var objs = window.shenxiao;
			switch(playcode){
			case "txsm":
			case "ztxztw":
			case "erx_lbz":
			case "erx_lz":
			case "sanx_lbz":
			case "sanx_lz":
			case "six_lbz":
			case "six_lz":
			case "wux_lz":
				for (var i = 0; i < objs.length; i++) {
					var spans = "";
					$(".marksix_content tr[data-val=" + objs[i].xiao + "] td:eq(0)").html(objs[i].xiao);
					var arr = objs[i].nums.split(',');
					for (var k = 0; k < arr.length; k++) {
						var num = arr[k];
						/*
						if(0 < num && num < 10){
							num = "0" + num;
						}
						*/
						spans += "<span class='span_ball_" + num + "'>" + num + "</span>";
					}
					$(".marksix_content tr[data-val=" + objs[i].xiao + "] td:eq(2)").html(spans);
				}
				break;
			case "hx_bax":
			case "hx_ex":
			case "hx_jiux":
			case "hx_liux":
			case "hx_qix":
			case "hx_sanx":
			case "hx_shix":
			case "hx_six":
			case "hx_syx":
			case "hx_wux":
				for (var i = 0; i < objs.length; i++) {
					var spans = "";
					$(".marksix_content tr[data-val=" + objs[i].xiao + "] td:eq(0)").html(objs[i].xiao);
					var arr = objs[i].nums.split(',');
					for (var k = 0; k < arr.length; k++) {
						var num = arr[k];
						/*
						if(0 < num && num < 10){
							num = "0" + num;
						}
						*/
						spans += "<span class='span_ball_" + num + "'>" + num + "</span>";
					}
					$(".marksix_content tr[data-val=" + objs[i].xiao + "] td:eq(2)").html(spans);
				}
				break;
			}
		}

		//提交按钮点击事件(多选框-自选不中)
		function addToBetList_ZXBZ(playCode, playName) {
			switch(playCode){
			case "w_bz":
				min = 5;
				break;
			case "liu_bz":
				min = 6;
				break;
			case "qi_bz":
				min = 7;
				break;
			case "ba_bz":
				min = 8;
				break;
			case "jiu_bz":
				min = 9;
				break;
			case "shi_bz":
				min = 10;
				break;
			case "shiy_bz":
				min = 11;
				break;
			case "shie_bz":
				min = 12;
				break;
			}
			var sx_a = $(".marksix_content input:checked");
			var len = sx_a.length;
			if (len < min) {
				Modal.alert({
					msg : "选球数不能小于" + min + "位!"
				}).on(function() {
					return;
				});
				return;
			}
			switch (min) {
			case 3:
			case 4:
				if (len > 8) {
					Modal.alert({
						msg : '选球数请勿大于 8 位!'
					}).on(function() {
						return;
					});
					return;
				}
				break;
			case 6:
			case 5:
				if (len > (min + 3)) {
					Modal.alert({
						msg : "选球数请勿大于 " + (min + 3) + "  位!"
					}).on(function() {
						return;
					});
					return;
				}
				break;
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
				if (len > (min + 2)) {
					Modal.alert({
						msg : "选球数请勿大于 " + (min + 2) + "  位!"
					}).on(function() {
						return;
					});
					return;
				}
				break;
			}
			var je = $("input.betmoney").val();
			if (je == "") {
				Modal.alert({
					msg : '请输入金额!'
				}).on(function() {
					return;
				});
				return;
			}
			if (je != "") {
				var ex = /^[1-9]\d*$/
				if (!ex.test(je)) {
					Modal.alert({
						msg : '输入金额无效!'
					}).on(function() {
						return;
					});
					return;
				}
			}
			var sx_list = new Array();
			sx_a.each(function() {
				var val = $(this).parent().prev("td").data("val");
				sx_list.push(val);
			});
			var sxzhArr = getZH(sx_list, min);
			var pl = $("#iiiiiirate").html();
			var qishu = $(".memRound_lhc").html();

			var markSixId = $("#iiiiiirate").data("marksixid");
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "playCode": playCode, "state": 1};
			var pour = [{"haoma": sx_list.join(","), "money": je, "markSixId": markSixId}];

			var allje = 0;
			var htmls = "";
			for (var i = 0; i < sxzhArr.length; i++) {
				allje += je * 1;
				htmls += "<tr><td>" + playName + "</td><td>" + sxzhArr[i] + "</td><td>" + je + "</td></tr>";
			}
			var creat = $("#Credit").val() * 1;
			//if (allje > creat) {
			//    Modal.alert({ msg: '金额不足!' }).on(function () {
			//        return;
			//    });
			//    return;
			//}
			if (pour.length > 0) {
				betData.pour = pour;
				$("#myModal1 .counts").html(sxzhArr.length);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$(".marksix_content input[type=checkbox]").removeAttr("checked");
				$("input.betmoney").val('');
				$("#myModal1").modal("show");
			}
		}
		//得到排列组合
		function getZH(arr, num) {
			var result = new Array();

			for (var i = 0; i < Math.pow(2, arr.length); i++) {
				var a = 0;
				var zh = "";
				for (var j = 0; j < arr.length; j++) {
					if (i >> j & 1) {
						a++;
						if (zh === "")
							zh += arr[j];
						else
							zh += "," + arr[j];
					}
				}
				if (a == num) {
					result.push(zh);
				}
			}
			return result;
		}
		//提交按钮点击事件(多选框-连码)
		function addToBetList_LianM(playCode, playName) {
			var je = $("input.betmoney").val();
			if (je == "") {
				Modal.alert({
					msg : '请输入金额!'
				}).on(function() {
					return;
				});
				return;
			}
			if (je != "") {
				var ex = /^[1-9]\d*$/
				if (!ex.test(je)) {
					Modal.alert({
						msg : '输入金额无效!'
					}).on(function() {
						return;
					});
					return;
				}
			}
			var sx_list = new Array();
			var sx_a = $(".marksix_content input[name=siquan]:checked");
			var len = sx_a.length;
			if (len > 10) {
				Modal.alert({
					msg : '选球数不能超过10位!'
				}).on(function() {
					return;
				});
				return;
			}
			var sxzhArr = new Array();
			sx_a.each(function() {
				sx_list.push($(this).parent().prev("td").data("val"));
			});
			switch (playCode) {
			case "tc":
			case "eqz":
			case "eztzze":
			case "eztzzt":
			case "ezt":
				if (len < 2) {
					Modal.alert({
						msg : '选球数不足!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var z = 0; z < len - 1; z++) {
					for (var y = z + 1; y < len; y++) {
						if (z != y) {
							sxzhArr.push(sx_list[z] + "," + sx_list[y]);
						}
					}
				}
				break;
			case "sqz":
			case "szezze":
			case "szezzs":
			case "szes":
				if (len < 3) {
					Modal.alert({
						msg : '选球数不足!'
					}).on(function() {
						return;
					});
					return;
				}

				for (var k = 0; k < len - 2; k++) {
					for (var z = k + 1; z < len - 1; z++) {
						for (var y = z + 1; y < len; y++) {
							if (k != z && k != y && z != y) {
								sxzhArr.push(sx_list[k] + "," + sx_list[z]
										+ "," + sx_list[y]);
							}
						}
					}
				}
				break;
			case "szy":
			case "siqz":
				if (len < 4) {
					Modal.alert({
						msg : '选球数不足!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var j = 0; j < len - 3; j++) {
					for (var k = j + 1; k < len - 2; k++) {
						for (var z = k + 1; z < len - 1; z++) {
							for (var y = z + 1; y < len; y++) {
								if (j != k && j != z & j != y && k != z
										&& k != y && z != y) {
									sxzhArr.push(sx_list[j] + "," + sx_list[k]
											+ "," + sx_list[z] + ","
											+ sx_list[y]);
								}
							}
						}
					}

				}
				break;
			}
			var pl = $("#lmrate").html();
			var markSixId = $("#lmrate").data("marksixid");
			var qishu = $(".memRound_lhc").html();
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "playCode": playCode, "state": 1};
			var pour = [{"haoma": sx_list.join(","), "money": je, "markSixId": markSixId}];
			var allje = 0;
			var htmls = "";
			for (var i = 0; i < sxzhArr.length; i++) {
				allje += je * 1;
				htmls += "<tr><td>" + playName + "</td><td>" + sxzhArr[i]
						+ "</td><td>" + je + "</td></tr>";
			}
			var creat = $("#Credit").val() * 1;
			//if (allje > creat) {
			//    Modal.alert({ msg: '金额不足!' }).on(function () {
			//        return;
			//    });
			//    return;
			//}
			if (pour.length > 0) {
				betData.pour = pour;
				$("#myModal1 .counts").html(sxzhArr.length);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$(".marksix_content input[type=checkbox]").prop("checked", false);
				$("input.betmoney").val('');
				$("#myModal1").modal("show");
			}

		}
		//提交按钮点击事件(多选框-合肖)
		function addToBetList_HeXiao(playCode, playName, minSelCount) {
			var sx_list = new Array();

			var sx_a = $(".marksix_content input:checked");
			var len = sx_a.length;
			if (len < minSelCount) {
				Modal.alert({
					msg : '所选生肖不足!'
				}).on(function() {
					return;
				});
				return;
			}
			if (len > 12) {
				Modal.alert({
					msg : '所选生肖不能超过12位!'
				}).on(function() {
					return;
				});
				return;
			}

			var je = $("input.betmoney").val();
			if (je == "") {
				Modal.alert({
					msg : '请输入金额!'
				}).on(function() {
					return;
				});
				return;
			}
			if (je != "") {
				var ex = /^[1-9]\d*$/
				if (!ex.test(je)) {
					Modal.alert({
						msg : '输入金额无效!'
					}).on(function() {
						return;
					});
					return;
				}
			}

			sx_a.each(function() {
				var val = $(this).parents("tr[data-val]").data("val");
				sx_list.push(val);
			});

			var a = 1, b = 1;
			var selectedNum = sx_list.length;
			//计算最大注数
			for(var i=0;i<minSelCount;i++){
				a = a * (minSelCount-i);
			}
			for(var i=selectedNum;i>selectedNum-minSelCount;i--){
				b = b * i;
			}

			var zhushu = b / a;
			var qishu = $(".memRound_lhc").html();
			var markSixId = $(".marksix_content tr[data-val] b.text-red:eq(0)").data("marksixid");
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "playCode": playCode, "state": 1};
			var pour = [{"haoma": sx_list.join(","), "money": je, "markSixId": markSixId}];
			var allje = zhushu * je;
			var htmls = "<tr><td>" + playName + "</td><td>" + sx_list.join(",") + "</td><td>" + allje + "</td></tr>";

			if (pour.length > 0) {
				betData.pour = pour;
				$("#myModal1 .counts").html(zhushu);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$(".marksix_content input[type=checkbox]").removeAttr("checked");
				$("input.betmoney").val('');
				$("#myModal1").modal("show");
			}
		}

		//提交按钮点击事件(多选框)
		function addToBetList_Checkbox(playCode, playName) {
			var sx_a = $(".marksix_content input:checked");
			var len = sx_a.length;
			var je = $("input.betmoney").val();
			if (je == "") {
				Modal.alert({
					msg : '请输入金额!'
				}).on(function() {
					return;
				});
				return;
			}
			if (je != "") {
				var ex = /^[1-9]\d*$/
				if (!ex.test(je)) {
					Modal.alert({
						msg : '输入金额无效!'
					}).on(function() {
						return;
					});
					return;
				}
			}
			var shenxiaoArr = new Array();
			var sx_list = new Array();
			var rate_list = new Array();
			var sxzhArr = new Array();
			var ratezhArr = new Array();
			sx_a.each(function() {
				var val = $(this).parents("tr[data-val]").data("val");
				var rate = $(this).parent().prev("td").prev("td").find("b.text-red").html;
				sx_list.push(val);
				rate_list.push(rate);
			});
			if (playCode == "erx_lz" || playCode == "erx_lbz") {
				if (len < 2) {
					Modal.alert({
						msg : '所选生肖不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '生肖最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var i = 0; i < len - 1; i++) {
					for (var k = i + 1; k < len; k++) {
						if (i != k) {
							sxzhArr.push(sx_list[i] + "," + sx_list[k]);
							var minrate = [ rate_list[i], rate_list[k] ].sort(function(a, b) {return a - b})[0];
							ratezhArr.push(minrate);
						}
					}
				}
			}
			if (playCode == "sanx_lz" || playCode == "sanx_lbz") {
				if (len < 3) {
					Modal.alert({
						msg : '所选生肖不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '生肖最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var k = 0; k < len - 2; k++) {
					for (var z = k + 1; z < len - 1; z++) {
						for (var y = z + 1; y < len; y++) {
							if (k != z && k != y && z != y) {
								sxzhArr.push(sx_list[k] + "," + sx_list[z] + "," + sx_list[y]);
								var minrate = [ rate_list[k], rate_list[z], rate_list[y] ].sort(function(a, b) { return a - b })[0];
								ratezhArr.push(minrate);
							}
						}
					}
				}
			}
			if (playCode == "six_lz" || playCode == "six_lbz") {
				if (len < 4) {
					Modal.alert({
						msg : '所选生肖不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '生肖最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var j = 0; j < len - 3; j++) {
					for (var k = j + 1; k < len - 2; k++) {
						for (var z = k + 1; z < len - 1; z++) {
							for (var y = z + 1; y < len; y++) {
								if (j != k && j != z & j != y && k != z && k != y && z != y) {
									sxzhArr.push(sx_list[j] + "," + sx_list[k] + "," + sx_list[z] + "," + sx_list[y]);
									var minrate = [ rate_list[j], rate_list[k], rate_list[z], rate_list[y] ].sort(function(a, b) { return a - b })[0];
									ratezhArr.push(minrate);
								}
							}
						}
					}
				}
			}
			if (playCode == "wux_lz") {
				if (len < 5) {
					Modal.alert({
						msg : '所选生肖不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '生肖最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var i = 0; i < len - 4; i++) {
					for (var j = i + 1; j < len - 3; j++) {
						for (var k = j + 1; k < len - 2; k++) {
							for (var z = k + 1; z < len - 1; z++) {
								for (var y = z + 1; y < len; y++) {
									if (i != j && i != k && i != z && i != y && j != k && j != z & j != y && k != z && k != y && z != y) {
										sxzhArr.push(sx_list[i] + "," + sx_list[j] + "," + sx_list[k] + "," + sx_list[z] + "," + sx_list[y]);
										var minrate = [ rate_list[i], rate_list[j], rate_list[k], rate_list[z], rate_list[y] ].sort(function(a, b) { return a - b })[0];
										ratezhArr.push(minrate);
									}
								}
							}
						}
					}
				}
			}
			if (playCode == "erw_lz" || playCode == "erw_lbz") {
				if (len < 2) {
					Modal.alert({
						msg : '所选尾数不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '尾数最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var z = 0; z < len - 1; z++) {
					for (var y = z + 1; y < len; y++) {
						if (z != y) {
							sxzhArr.push(sx_list[z] + "," + sx_list[y]);
							var minrate = [ rate_list[z], rate_list[y] ].sort(function(a, b) { return a * 1 < b * 1 })[0];
							ratezhArr.push(minrate);
						}
					}
				}
			}
			if (playCode == "sanw_lz" || playCode == "sanw_lbz") {
				if (len < 3) {
					Modal.alert({
						msg : '所选尾数不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '尾数最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var k = 0; k < len - 2; k++) {
					for (var z = k + 1; z < len - 1; z++) {
						for (var y = z + 1; y < len; y++) {
							if (k != z && k != y && z != y) {
								sxzhArr.push(sx_list[k] + "," + sx_list[z] + "," + sx_list[y]);
								var minrate = [ rate_list[k], rate_list[z], rate_list[y] ].sort(function(a, b) { return a * 1 < b * 1 })[0];
								ratezhArr.push(minrate);
							}
						}
					}
				}
			}
			if (playCode == "siw_lz" || playCode == "siw_lbz") {
				if (len < 4) {
					Modal.alert({
						msg : '所选尾数不足!'
					}).on(function() {
						return;
					});
					return;
				}
				if (len > 6) {
					Modal.alert({
						msg : '尾数最多能选6个!'
					}).on(function() {
						return;
					});
					return;
				}
				for (var j = 0; j < len - 3; j++) {
					for (var k = j + 1; k < len - 2; k++) {
						for (var z = k + 1; z < len - 1; z++) {
							for (var y = z + 1; y < len; y++) {
								if (j != k && j != z & j != y && k != z && k != y && z != y) {
									sxzhArr.push(sx_list[j] + "," + sx_list[k] + "," + sx_list[z] + "," + sx_list[y]);
									var minrate = [ rate_list[j], rate_list[k], rate_list[z], rate_list[y] ].sort(function(a, b) { return a * 1 < b * 1 })[0];
									ratezhArr.push(minrate);
								}
							}
						}
					}
				}
			}
			var qishu = $(".memRound_lhc").html();
			var markSixId = $(".marksix_content tr[data-val] b.text-red:eq(0)").data("marksixid");
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "playCode": playCode, "state": 1};
			var pour = [{"haoma": sx_list.join(","), "money": je, "markSixId": markSixId}];
			var allje = 0;
			var htmls = "";
			for (var i = 0; i < sxzhArr.length; i++) {
				allje += je * 1;
				htmls += "<tr><td>" + playName + "</td><td>" + sxzhArr[i] + "</td><td>" + je + "</td></tr>";
			}
			//var creat = $("#Credit").val() * 1;
			//if (allje > creat) {
			//    Modal.alert({ msg: '金额不足!' }).on(function () {
			//        return;
			//    });
			//    return;
			//}
			if (pour.length > 0) {
				betData.pour = pour;
				$("#myModal1 .counts").html(sxzhArr.length);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$(".marksix_content input[type=checkbox]").removeAttr("checked");
				$("input.betmoney").val('');
				$("#myModal1").modal("show");
			}
		}

		//提交按钮点击事件(文本框)
		function addToBetList(playCode, playName) {
			var inputVal = $(".marksix_content input[type=text], .marksix_footer input[type=text]");
			var validate = "";
			var allje = 0;
			var con;
			inputVal.each(function() {
				var je = $(this).val();
				if (je != "") {
					var ex = /^[1-9]\d*$/
					if (!ex.test(je)) {
						validate = "金额无效！";
						con = $(this);
						return false;
					} else {
						allje += je * 1;
					}
				}
			});
			var Credit = $("#Credit").val() * 1;
			if (validate != "") {
				Modal.alert({
					msg : validate
				}).on(function() {
					$(con).focus();
					return;
				});
				return;
			} else if (allje == 0) {
				Modal.alert({
					msg : '请先下注!'
				}).on(function() {
					return;
				});
				return;
			} else if (allje > Credit) {
				Modal.alert({ msg: '金额不足,请重新输入!' }).on(function () {
					return;
				});
				return;
			}

			var qishu = $(".memRound_lhc").html();
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "playCode": playCode, "state": 2};
			var pour = [];
			var zhucount = 0;
			var htmls = "";
			inputVal.each(function() {
				var je = $(this).val();
				var space = $(this).data("space");
				if (je != "") {
					zhucount++;
					if(space == 2){
						$dataObj = $(this).parent().prev("td").prev("td").find(".text-red");
						val = $dataObj.parents("tr[data-val]").data("val");
					} else {
						$dataObj = $(this).parent().prev("td").find(".text-red");
						val = $dataObj.data("val");
					}
					var markSixId = $dataObj.data("marksixid");
					var rate = $dataObj.html();
					if (!rate || rate == 0) {
						Modal.alert({
							msg : '赔率0 ,无法下注!'
						}).on(function() {
							return false;
						});
						return;
					}
					pour.push({"haoma": val, "money": je, "markSixId": markSixId});
					htmls += "<tr><td>" + playName + "</td><td>" + val + "</td><td>" + je + "</td></tr>";
				}
			});
			if (pour.length > 0) {
				betData.pour = pour;
				$("#myModal1 .counts").html(zhucount);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$("#myModal1").modal("show");
				$(".qingkong").click();
			}
		}
	</script>

	<!-- <div class="text-center footercontent">Copyright &copy; ${website_name } Reserved</div> -->
	<script src="${station }/lhc/scripts/bootstrap.min.js"></script>

	<script src="${station }/lhc/scripts/bootstrap_alert.js"></script>
	<!-- system modal start弹窗html -->
	<div id="ycf-alert" class="modal">
		<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
			<div class="modal-content">
				<div class="modal-body bg-info" style="border-radius: 5px; padding: 10px;">
					<p style="font-size: 15px; font-weight: bold;">[Message]</p>
				</div>
				<div class="modal-footer" style="padding: 5px; text-align: center;">
					<button type="button" class="btn btn-primary ok" data-dismiss="modal">[BtnOk]</button>
					<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<img class="center-block linecentermodel" width="40" height="40" src="${station }/lhc/imgs/load.gif" />
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="margin: 35% auto 0; width: 90%">
			<div class="modal-content">
				<div class="modal-header bg-info" style="padding: 10px; border-radius: 5px;">
					<h5 class="modal-title">
						下注明细 <span class="badge pull-right text-white counts">0</span>
					</h5>
				</div>
				<div class="modal-body" style="padding: 5px;">
					<table class="table table-striped">
						<thead>
							<tr class="bg-warning">
								<td>类型</td>
								<td>号码</td>
								<td>金额</td>
							</tr>
						</thead>
						<tbody id="betDetail"></tbody>
					</table>
				</div>
				<div class="modal-footer" style="padding: 6px;">
					<span class="text-primary">金额:<span class="jinge ">0.0</span>&nbsp;&nbsp;&nbsp;
					</span>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary tijiao">确认</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<!--system modal END弹窗html-->
	<script>
		//此方法解决JS的sort()排序方法在IE9/safari兼容性问题
		!function(window) {
			var ua = window.navigator.userAgent.toLowerCase(), reg = /msie|applewebkit.+safari/;
			if (reg.test(ua)) {
				var _sort = Array.prototype.sort;
				Array.prototype.sort = function(fn) {
					if (!!fn && typeof fn === 'function') {
						if (this.length < 2)
							return this;
						var i = 0, j = i + 1, l = this.length, tmp, r = false, t = 0;
						for (; i < l; i++) {
							for (j = i + 1; j < l; j++) {
								t = fn.call(this, this[i], this[j]);
								r = (typeof t === 'number' ? t : !!t ? 1 : 0) > 0 ? true
										: false;
								if (r) {
									tmp = this[i];
									this[i] = this[j];
									this[j] = tmp;
								}
							}
						}
						return this;
					} else {
						return _sort.call(this);
					}
				};
			}
		}(window);
		var tt;
		function caizhongxz() {
			$("#caizhong").slideToggle(500);
			clearTimeout(tt);
			if ($("#caizhong").is(":visible")) {
				tt = setTimeout(function() {
					$("#caizhong").slideUp(500);
				}, 1000 * 25);
			}
		}
		var SysSecond_lhc = 0;
		$(function() {
			$("input[type=text]").onlyNum();//所有文本框只能输入数字

			GetCredit();
			QingKong();

			setInterval("SetRemainTime()", 1000);
			if ($("#caizhong").is(":visible")) {
				tt = setTimeout(function() {
					$("#caizhong").slideUp(500);
				}, 1000 * 25);
			}
		});

		//将时间减去1秒，计算天、时、分、秒
		function SetRemainTime() {
			if (SysSecond_lhc > 0) {
				var second = Math.floor(SysSecond_lhc % 60); // 计算秒
				var minite = Math.floor((SysSecond_lhc / 60) % 60); //计算分
				var hours = Math.floor(SysSecond_lhc / 60 / 60);
				if (second < 10) {
					second = "0" + second
				}
				if (minite < 10) {
					minite = "0" + minite
				}
				SysSecond_lhc = SysSecond_lhc - 1;
				$(".reciprocal_lhc").html(hours + ":" + minite + ":" + second);
			} else {
				$(".reciprocal_lhc").html("暂停下注");
				GetSingleGameOpen();
			}
		}

		function GetSingleGameOpen() {
			var url = '${station }/marksix/getGameOpenInfo.do';
			var code = "${lotCode}";
			if(code && code == 'SFLHC'){
				url = '${station }/v2/getGameOpenInfo.do';
			}
			$.ajax({
				type : "post",
				url : url,
				data: {lotCode: "${lotCode}"},
				async : true,
				error : function(result) {
				},
				success : function(result) {
					if (!result.success) {
						return;
					}
					SysSecond_lhc = result.data.closeSecond;
				}
			});
		}

		//清空文本框
		function QingKong() {
			$(".qingkong").click(function() {
				$("input[type=text]").val('');
				$("td.inputed").removeClass("inputed");
				$("td input[type=checkbox]").removeAttr("checked");
			});
		}
		$("#kjbet").click(function(){ $("input[type=button][value=确认下注]").click(); });

		//取账户额度
		function GetCredit() {
			if ($("#Credit").length > 0) {
				$._ajax({url: stationUrl + "/member/meminfo.do", async: !1}, function (status, message, data, result) {
					userMoney = data.money;
					$("#Credit").val(userMoney);
					$("#createshow").html(userMoney);
				});
			}
		}

		//提交下注
		function submitBetGame(params) {
			$("#myModal1").modal("hide");
			var data = {
				"data" : params
			};
			$('#myModal').modal('show');
			$.ajax({
				type : "post",
				url : baseUrl + "/marksixbet/doMarkBets.do",
				data : data,
				async : true,
				contentType : "application/x-www-form-urlencoded",
				error : function() {
					Modal.alert({
						msg : '网络连接超时,请重试!'
					}).on(function(e) {
						return;
					});
				},
				success : function(data) {
					if (!data.success) {
						Modal.alert({
							msg : data.msg
						}).on(function(e) {
							return;
						});
					} else {
						Modal.alert({
							msg : "您的注单已成功提交下注，请至下注记录查看!"
						}).on(function(e) {
							GetCredit();//取账户额度
							return;
						});
					}
				},
				complete : function(data) {
					$('#myModal').modal('hide');
				}
			});
		}
	</script>
	<style>
		#myModal1 .modal-footer .btn{
			width: 20%;
		}
		.modal{
			position: absolute;
		}
		#ycf-alert{
			position : fixed;
		}
	</style>
</body>
</html>
<%@include file="../include/check_login.jsp"%>