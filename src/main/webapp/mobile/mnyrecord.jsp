﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2"
	type="text/css" />
	<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 10px 16px;
    margin-left: -1px;
    line-height: 1.3333333;
    color: #cd0005;
    text-decoration: none;
    background-color: #ddd;
    border: 1px solid #fff;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #cd0005;
    border-color: #cd0005;
}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 3;
    color: #cd0005;
    background-color: #eee;
    border-color: #ddd;
}
</style>
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<a href="personal_center.do" style="color: #fff;"><em
					class="iconfont icon-left"></em></a> <span class="vbktl">帐变记录</span>
			</div>
			<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
	<div class="" id="innerbox">
		<div class="touzhubox">
			<!-- 
			<div id="selectQueryType" class="touzhuqie" data-current="all">
				<span data-querytype="all" class="cur" style="width: 100%;">全部记录 </span> 
				<span data-querytype="win" class="" style="width: 25%;">中奖记录 </span> 
				<span data-querytype="init" class="" style="width: 25%;">未开奖 </span> 
				<span data-querytype="canel" class="" style="width: 25%;">未成功订单 </span> 
			</div>
				 -->
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent"
						data-value="">
						<div class="fangsjbox">时间</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="today">今天</li>
								<li data-value="yesterday">昨天</li>
								<li data-value="week">近一周</li>
								<li data-value="month">近30天</li>
								<li data-value="monthbefore">30天以前</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryDateType" class="touzhusj selectComponent" data-value="" style="width: 300px;">
						<div class="fangsjbox">选择类型</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul id="moneyType">
								<li data-value="" selected="selected">选择类型</li>
								<c:forEach items="${typelist }"  var ="type"> 
									<li data-value="${type.type }" >${type.name }</li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div class="caizhongtable">
				<table>
					<tr class="caizhongtabletr">
						<td>日期</td>
						<td>类别</td>
						<td>变前金额</td>
						<td>变动金额</td>
						<td>变后余额</td>
						<td>订单号</td>
						<td>查看</td>
					</tr>
					<tbody id="record_tb">
					
					</tbody>
				</table>
			</div>
			
			<nav id="pageview"></nav>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script id="moneytype_tpl" type="text/html">
	{{each data as v}}
        <li data-value="{{v.id}}">{{v.name}}</option>
	{{/each}}
</script>
<script type="text/javascript">
	$(function() {
		getData();
	})
	
	$("#queryDatetime").on("change", function(){
		getData();
	});
	
	$("#queryDateType").on("change", function(){
		getData();
	});
	
	function getData(currentPageNo, pageSize){
		currentPageNo = currentPageNo || 1;
		pageSize = pageSize || 20;
		
		$.ajax({
			"url":"${base}/mobile/mnyRecordData.do",
			"type":"POST",
			"data":{
				pageNumber:currentPageNo,
				pageSize:pageSize,
				qtime : $("#queryDatetime").data("value"),
				qtype : $("#queryDateType").data("value")
			},
			"success":function(data){
				var j = data.list;
				var col = "";
				if(j){
					for(var i in j){
						col+='<tr>';
						col+='<td>'+getLocalTime(j[i].createDatetime)+'</td>';
						col+='<td>'+mnType(j[i].type)+'</td>';
						col+='<td>'+j[i].beforeMoney+'</td>';
						col+='<td style="color:red;">'+j[i].money+'</td>';
						col+='<td>'+j[i].afterMoney+'</td>';
						if(!j[i].orderId){
							col+='<td>-</td>';
							col+='<td>-</td>';
						}else{
							col+='<td>'+j[i].orderId+'</td>';
							col+='<td onclick="location.href=\'mnyrecord_details.do?id='+j[i].id+'\'">更多</td>';
						}
						col+='</tr>';
					}
				}
				$("#record_tb").html(col);
				var pageviewTpl = _.template($("#pageviewTemplate").html());
				$("#pageview").html(pageviewTpl({data: data}));

				$("#pageview .pagination li a").on("click", function(){
					var pageNo = $(this).attr("pageno");
					getData(pageNo);
				});
			}
		});
	}
	
	function getLocalTime(nS) {  
		var date = new Date(nS);
		var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '+ date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
	   return dateFormat;     
	}	
	
	function mnType(p){
		var moneyType = ${moneyType};
		for(var key in moneyType)
		{
			if(p == key){
				return moneyType[key];
			}
		}
	}
</script>
<script type="text/html" id="pageviewTemplate" style="display: none;">
{#
if(data.totalPageCount > 1){
#}
<ul class="pagination pagination-lg pull-right">
	<li>
		<a href="javascript: void(0);" aria-label="Previous" pageno="{{data.prePage}}">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<!-- 
	<li class="active"><a href="javascript: void(0);">1</a></li>
	<li><a href="javascript: void(0);">2</a></li>
	<li><a href="javascript: void(0);">3</a></li>
	<li><a href="javascript: void(0);">4</a></li>
	<li><a href="javascript: void(0);">5</a></li>
	 -->
	<li>
		<a href="javascript: void(0);" aria-label="Next" pageno="{{data.nextPage}}">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
{#
}
#}
</script>
<style>
.caizhongtable table td{
    text-align: center;
    font-size: 16px;
    line-height: 30px;
    color: #a1a19f;
    width: 15%;
}
</style>
</html>
<%@include file="include/check_login.jsp"%>