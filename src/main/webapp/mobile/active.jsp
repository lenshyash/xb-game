<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp" %>
<%
Long stationId = StationUtil.getStation().getId();
%>
<!DOCTYPE html>
<html>
<head>
<title>优惠活动</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />

<style type="text/css">
blockquote, body, code, dd, div, dl, dt, fieldset, form, h1, h2, h3, h4, h5, h6, input, legend, li, ol, p, pre, td, textarea, th, ul{
	font-size: 20px;
}

.activeDetailsDiv img {
	width: 100%;
}
</style>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
</head>
<body>
<div class="top">
	<div class="inner">
		<div class="back">
			<a href="index.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">优惠活动</span>
		</div>
		<div class="cl"></div>
	</div>
</div>

<div class="h44"></div>

<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox1"> 
	<div class="dexcontin">
	</div>
	<div class="cl h36"></div>
</div>

</body>
<script type="text/javascript">
	$(function(){
		$.ajax({
			type : "POST",
			url : baseUrl + "/getConfig/getPreferential.do",
			data : {
			},
			success : function(result) {
				var tplHtml = $("#noticeTemplate").html();
				var $noticeContent = $(".dexcontin");
				$noticeContent.html(_.template(tplHtml)({list: result}));
				$('.activeDetailsDiv img').each(function(){
					$(this).css('width','100%');
				})
				if(!result||result.length==0){
					$noticeContent.html("暂无优惠活动");
				}
				$(".activeDetails").click(function(){
					// $(".activeDetailsDiv").hide();
					var $this = $(this);
					$this.parents(".dexitem").next("div").toggle();
				});
			}
		});
	});
</script>
<script type="text/html" id="noticeTemplate" style="display: none;">
	{#
		_.map(list, function(item, index){
	#}
		<div class="dexitembox">
			<!--<div class="dexbt">
				<em class="iconfont icon-yuanquan2"></em>{{item.title}}
			</div>-->
			<div class="dexitem">
				<div class="">
					<a href="javascript: void(0);" class="activeDetails" index="{{index}}">
						<img src="{{item.titleImgUrl}}" alt="" width="100%" />
						<!-- 
						<div class="youzhe">
							 {{item.title}}
						</div>
						 -->
					</a>
				</div>
			</div>
			<div class="activeDetailsDiv" style="display: none;background: ${activeBackColor};">
				{{item.content}}
			</div>
			<div class="h6"></div>
		</div>
	{#
		});
	#}
</script>
</html>