﻿<%@page import="com.game.common.onlinepay.OnlinepayUtils.Base64"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.game.util.StationConfigUtil"%>
<%@page import="com.game.util.StationUtil" %>
<%@page import="com.game.constant.StationConfig" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="include/common.jsp" %>
<%
String[] baseParamKey = new String[]{"title", "toUrl", "backUrl", "actionMethod"};

String title = request.getParameter(baseParamKey[0]);
title = new String(title.getBytes("ISO-8859-1"));
String toUrl = request.getParameter(baseParamKey[1]);
String backUrl = request.getParameter(baseParamKey[2]);
String actionMethod = request.getParameter(baseParamKey[3]);
if(StringUtils.isEmpty(actionMethod)){
	actionMethod = "post";
}

if(!StringUtils.isEmpty(toUrl)){
	title = Base64.getDecode(title);
	toUrl = Base64.getDecode(toUrl);
	backUrl = Base64.getDecode(backUrl);

	pageContext.setAttribute(baseParamKey[0], title);
	pageContext.setAttribute(baseParamKey[1], toUrl);
	pageContext.setAttribute(baseParamKey[2], backUrl);
	pageContext.setAttribute(baseParamKey[3], actionMethod);
}

Long stationId = StationUtil.getStation().getId();
pageContext.setAttribute("mobileOpenOutlinkType", StationConfigUtil.get(stationId, StationConfig.mobile_open_outlink_type));
%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/base.css" type="text/css" />
</head>
<body>
	<div class="navbar fn-clear">
		<div class="left">
			<c:if test="${empty backUrl}">
				<a class="back" href="javascript: top.history.go(-1);"><icon class="iconfont icon-left"></icon></a>
			</c:if>
			<c:if test="${not empty backUrl}">
				<a class="back" href="${backUrl }"><icon class="iconfont icon-left"></icon></a>
			</c:if>
			<span class="title">${title }</span>
		</div>
	</div>
	<c:choose>
		<c:when test="${mobileOpenOutlinkType eq 'blank' }">
			<div class="browser-warpper">
				跳转到${title }，如果没有自动打开请点击“<a href="${toUrl }" target="_blank">打开</a>”
			</div>
			<script type="text/javascript">
				window.open("${toUrl }", "_blank");
				// location.href = "${backUrl }";
			</script>
		</c:when>
		<c:when test="${mobileOpenOutlinkType eq 'href' }">
			<script type="text/javascript">
				location.href = "${toUrl }";
			</script>
		</c:when>
		<c:otherwise>
			<div class="browser-warpper">
				<iframe name="browserFrame" src="${toUrl }"></iframe>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>