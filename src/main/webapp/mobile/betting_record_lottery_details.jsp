﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<%
String lotCode = request.getParameter("lotCode");
String orderId = request.getParameter("orderId");
pageContext.setAttribute("lotCode", lotCode);
pageContext.setAttribute("orderId", orderId);
%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
</head>
<body >
<div class="top">
	<div class="inner">
		
		<div class="back">
			<!-- <a href="betting_record_lottery.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> -->
			<a href="javascript: history.go(-1);" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">注单详情</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="recordDetailsContent" id="innerbox">
</div>
</body>

<script type="text/javascript">
	var lotCode = "${lotCode}";
	var orderId = "${orderId}";
</script>
<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/betting_record_lottery_details.js"></script>
</html>
<script type="text/html" id="recoredDetailsTemplate" style="display: none;">
	{#
						var statusTitle = "其他";
						var isHJ = '0.00';
						if(data.winMoney){
							isHJ = data.winMoney.toFixed(2);//中奖金额
						}
						var zhushu = data.buyZhuShu;
						var dzje = '0.00';
						dzje = data.buyMoney/(zhushu*data.multiple)

						/*
						switch(data.status){
						case 1:
							statusTitle = "未开奖";
							break;
						case 2:
							statusTitle = "已派奖";
							break;
						case 3:
							statusTitle = "未中奖";
							break;
						case 4:
							statusTitle = "撤单";
							break;
						}
						*/
						//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
						var yingkui = 0;
						if(data.status == 1 ){
							statusTitle='未开奖'
						}else if(data.status == 2 ){
							yingkui = data.winMoney - data.buyMoney;
							if(data.rollBackMoney){
								yingkui = yingkui + data.rollBackMoney;
							}
							statusTitle='已中奖'
						}else if(data.status == 3 ){
							yingkui = data.winMoney - data.buyMoney;
							if(data.rollBackMoney){
								yingkui = yingkui + data.rollBackMoney;
							}
							statusTitle='未中奖'
						}else if(data.status == 4 ){
							statusTitle='撤单'
						}else if(data.status == 5 ){
							statusTitle='回滚成功'
						}else if(data.status == 6 ){
							statusTitle='回滚异常'
						}else if(data.status == 7 ){
							statusTitle='开奖异常'
						}else if(data.status == 8 ){
							isHJ = data.buyMoney.toFixed(2);//如果是和局返还金额(即中奖金额 = 投注金额)
							statusTitle='和局'
						}
	#}
	<div class="shangqkaij tac">
		<span class="tac co000">订单号： <em class="hong">{{data.orderId}}</em> </span>
		<div class="cl"></div>
	</div>
	<div class="bgf6 ov">
		<table class="tymingxtab tymingxtabcur">
			<tr>
				<td class="">
					<div class="tymingxtabdiv">账号</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.account}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">单注金额</div>
				</td>
				<td class="bttd">
<!--
					<div class="tymingxtabdiv">{{data.version == 2 || data.lotCode == "LHC" ? data.buyMoney : 2.00 / data.model}}</div>
		-->	
				<div class="tymingxtabdiv">{{dzje.toFixed(2)}}</div>	
</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">下注时间</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{new Date(data.createTime).format()}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">投注注数</div>
				</td>
				<td class="bttd">
<!--
					<div class="tymingxtabdiv">{{data.version == 2 || data.lotCode == "LHC" ? "1" : data.buyZhuShu}}</div>
	-->		
<div class="tymingxtabdiv">{{ data.buyZhuShu}}</div>	
</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">彩种</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.lotName}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">倍数</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.multiple}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">期号</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.qiHao}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">投注总额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.buyMoney}}</div>
				</td>
				
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">玩法</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.playName}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">{{data.version == 2 || data.lotCode == "LHC" ? "赔率" : "奖金"}}</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.peilv}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">开奖号码</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.lotteryHaoMa ? data.lotteryHaoMa : "尚未颁奖"}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">中奖注数</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.winZhuShu ? data.winZhuShu : 0}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">状态</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{statusTitle}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">中奖金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{isHJ}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">盈亏</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{yingkui.toFixed(2)}}</div>
				</td>
				<td class="">
					<!-- <div class="tymingxtabdiv">投注号码</div> -->
				</td>
				<td class="bttd">
					<!-- <div class="tymingxtabdiv">{{data.haoMa}}</div> -->
				</td>
			</tr>
			<tr>
				<td class="" colspan="4">
					<div style="width: 160%; min-height: 200px; font-size: 22px; padding: 20px;">{{data.haoMa}}</div>
				</td>
			</tr>
		</table>
		<div class="cl h80"></div>
	</div>
</script>
<%@include file="include/check_login.jsp" %>