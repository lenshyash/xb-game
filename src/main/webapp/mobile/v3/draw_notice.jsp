<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/head.jsp" />
</head>
<body>
<input type="hidden" value="${dtSum}" id="dtSumValue">
	<div class="page" id="page_draw_notice_jsp">
		<jsp:include page="include/barTab.jsp" />
		<header class="bar bar-nav">
			<a class="title">开奖结果</a>
		</header>
		<c:if test="${domainFolder eq 'd00519'}">
		<style>
			.bar-nav{
				background-color:#171719!important;
			}
		</style>
		</c:if>
		<div class="content pull-to-refresh-content" data-ptr-distance="55">
			<!-- 默认的下拉刷新层 -->
			<div class="pull-to-refresh-layer">
				<div class="preloader"></div>
				<div class="pull-to-refresh-arrow"></div>
			</div>
			<div class="list-block media-list">
				<ul></ul>
			</div>
		</div>
	</div>
</body>
</html>
<jsp:include page="include/need_js.jsp" />