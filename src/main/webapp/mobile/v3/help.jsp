<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/head.jsp"></jsp:include>
	<link rel="stylesheet" href="${res}/css/weui.min.css?v=1.0">
  </head>
  <body>
	<div class="page" id="page_active_desc_jsp">
	  <jsp:include page="include/barTab.jsp"/>
  	  <header class="bar bar-nav">
			<a class="title">帮助中心</a> 
			<span class="pull-left">
				 <button class="button button-link button-nav pull-left" style="color: white;" onclick="history.go(-1)">
				    <span class="icon icon-left"></span>
				    返回
				  </button>
			</span>
	  </header>
	  <div class="content">
	    <!-- 这里是页面内容区 -->
		<div class="weui-grids">
		 <c:forEach var="j" items="${hplist}">
			  <a external href="${m }/rookieHelpDesc.do?code=${j.code}" class="weui-grid js_grid">
			    <div class="weui-grid__icon">
			      <i class="iconfont icon-iconfonticonfontinfo"></i>
			    </div>
			    <p class="weui-grid__label">
			      ${j.title }
			    </p>
			  </a>
          </c:forEach>
		</div>
	  </div>
	</div>
  </body>
</html>
<jsp:include page="include/need_js.jsp"/>