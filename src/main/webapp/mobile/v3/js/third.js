	/**
	 * 体育投注记录
	 */
	$(document).on("pageInit","#sport_bet_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		// 保存沙巴体育记录
		var lbcData;
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , code = null, status = 0;
		
		changeData();
		
//		$page.on("click","a.button-danger",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#bettingRecordSportDetail");
//		});
		
		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();
			
			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			code = $("#sportType").val();
			status = $("#recordType").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			var url = M.res+"/sport/hg/getOrderData.do";
			var sbty = 0; 
			//  判断是不是沙巴体育
			if($("#classType").val() == 0){
				
			}else if($("#classType").val() == 1 ||  $("#classType").val() == 2){
				url = document.location.protocol + "//" + window.location.host +"/center/record/betrecord/sportsrecord.do";
				sbty = 1;
				code = $("#classType").val();
			}
				$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time + " 00:00:00",
					endTime : end_time + " 23:59:59",
					sportType : code,
					recordType : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					lbcData = data;
					var aggsData = data.aggsData;
					data = data.rows;
					var dataLen = data.length;
					if(dataLen == 0 && page == 1){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							var html = '';
							var xBet = unExist(aggsData.totalBetMoney);
							var xWin = unExist(aggsData.totalBetResult); 
							var dWin = parseFloat(xWin) - parseFloat(xBet);
							if(sbty == 1){
								var winMoney = aggsData.winMoneyCount - aggsData.bettingMoneyCount + ''
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+aggsData.bettingMoneyCount+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+aggsData.winMoneyCount+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+winMoney.substring(0,4)+'</p></div>';
							}else{
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
							}
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						
						totalPageCount = dataLen;
						var result = "";
						$.each(data,function(i,j){
							/**
							 * 记录列表
							 * */
							result += '<li ';
							if(i%2==0){
								result += 'style="background:#f9f9f9"';
							}
							if(sbty == 1){
								result += '><a href="javascript:void(0);" id="'+j.id+'" class="item-link item-content lbcOrderDetail">';
							}else{
								result += '><a href="javascript:void(0);" id="'+j.id+'" class="item-link item-content betOrderDetail">';
							}
							result += '<div class="item-inner"><div class="item-title-row">';
							if(j.league){
								if(j.bettingStatus === 4){
									result += '<div class="item-title hengxian"><em>';
								}else{
									result += '<div class="item-title"><em>';
								}
								if(j.sportType == 1){
									result += '<font style="margin-right:.3rem;">⚽</font>️';
								}else if(j.sportType == 2){
									result += '<font style="margin-right:.3rem;">🏀️</font>';
								}
								result += j.league+'</em></div>';
							}
			                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
			                if(j.homeTeam){
			                	if(sbty == 1){
			                		result += '<div class="item-subtitle" style="text-align:center;margin:.3rem;">'+j.homeTeam+' <font color=firebrick>VS</font> '+j.guestTeam+'</div>';
			                	}else{
			                		result += '<div class="item-subtitle" style="text-align:center;margin:.3rem;">'+j.homeTeam+' <font color=firebrick>VS</font> '+j.guestTeam+'</div>';
			                	}
			                }
			                result += '<div class="item-subtitle" style="color:lightcoral;">'+gameTimeTypeHelp(j.gameTimeType)+' -- '+plateHelp(j.plate);
			                if(j.odds){
			                	result += ' -- 赔率('+j.odds+')';
			                }
			                result += ' <em><font color="red">'+getStatusRemark(j)+ '</font></em>'
			                result += '</div>';
			                if(sbty == 1){
			                	result += '<div class="item-subtitle" style="color:cornflowerblue;">'+unExistLine(j.gameName)+'</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">'+unExistLine(getlbcStatus(j))+'</em></div>';
			                }else{
			                	result += '<div class="item-subtitle" style="color:cornflowerblue;">'+getSportName(j.sportType)+'('+j.typeNames+')</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingDate)+'<em style="display:inline-block;float:right;">'+getBetStatus(j)+'</em></div>';

			                }
							result += '</div></a></li>';
							
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
						function getStatusRemark(j){
							if(j.bettingStatus === 4){
								return '('+j.statusRemark+ ')'
							}
							return  '';
						}
						function toBetHtml(item,order){
							var row = order;
							var con = item.con;
							if(con.indexOf("vs") == -1){
								con = '<span class="text-danger">'+ con +'</span>';
							}
							var homeFirst = row.homeTeam  == item.firstTeam;//主队是否在前
							var scoreStr = "";
							if(row.gameTimeType == 1){
								if(homeFirst){
									scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH +":" + row.scoreC + ")</b></font>";
								}else{
									scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC +":" + row.scoreH + ")</b></font>";
								}
							}
							var home = item.firstTeam;
							var guest = item.lastTeam;
							if(item.half === true && row.mix == 2){
								home = home + "<font color='gray'>[上半]</font>";
								guest = guest + "<font color='gray'>[上半]</font>";
							}
							
							var html  = item.league +"<br/>" + 
										home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
										"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
							var balance = row.mix != 2 ? row.balance : item.balance;
							var bt = row.bettingStatus;
							if(balance == 4){
								html = "<s style='color:red;'>" + html+"(赛事腰斩)</s>"
							}else if(bt == 3 || bt == 4){
								html = "<s style='color:red;'>" + html+"("+row.statusRemark+")</s>"
							}else if(balance == 2 || balance == 5 || balance == 6){
								var mr = row.mix != 2 ? row.result:item.matchResult;
								if(homeFirst){
									html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
								}else{
									var ss = mr.split(":");
									html = html + "&nbsp;<font color='blue'>("+ss[0]+":"+ss[1]+")</font>";
								}
							}			
							return html;
						}
						function toLbcHtml(row){
							if(typeof(row.parlayData) != 'undefined'){
								try{
									row.parlayData = JSON.parse(row.parlayData)
								}catch(err){
								}
								var html = '';
								for(var i = 0; i < row.parlayData.length; i++){
									html  += unExistLine(row.parlayData[i].leagueName) +"<br/>" + 
									unExistLine(row.parlayData[i].homeTeam) + "&nbsp;vs&nbsp;" + unExistLine(row.parlayData[i].awayTeam) + "<br/>" +
									"<font color='red'></font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ unExistLine(row.parlayData[i].odds) +"</font><br><hr>";	
								}
							}else{
								if(row.score == undefined){
									row.score = '-:-'
								}
								var html  = unExistLine(row.league) +"<br/>" + 
								unExistLine(row.homeTeam) + "&nbsp;vs&nbsp;" + unExistLine(row.awayTeam) + "<br/>" + '<p>' + unExistLine(row.info) + '</p>' +
								"<font color='red'></font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ unExistLine(row.odds) +"</font><font color='blue'>("+ unExistLine(row.score) +")</font>";			
							}
							return html;
						}
						function getMatchInfo(order){
							if(order.mix != 2 && sbty != 1){
								return toBetHtml(JSON.decode(order.remark),order);
							}else if(order.mix != 2 && sbty == 1){
								return toLbcHtml(order);
							}
							var html = "";
							var arr = JSON.decode(order.remark)
							for(var i=0;i<arr.length;i++){
								if(i != 0){
									html += "<div style='border-bottom:1px #303030 dotted;'></div>";
								}
								if(sbty == 1){
									html += toLbcHtml(order);
								}else{
									html += toBetHtml(arr[i],order);
								}
							}
							return html;
						}
						
						function getBalanceStatus(order){
							var value = order.balance;
							if(value == 1){
								return "<font color='blue'>未结算</font>";
							}
							
							if(value == 2 || value == 5 || value == 6){
								return "<font color='green'>已结算</font>";
							}
							
							if(value == 3){
								return "<font color='red'>结算失败</font>";
							}
							
							if(value == 4){
								return "<font color='red'>比赛腰斩</font>";
							}
						}
						function getBettingStatus(order){
							var value = order.bettingStatus;
							if(value == 1){
								return "<font color='blue'>待确认</font>";
							}
							if(value == 2){
								return "已确认";
							}
							
							if(value == 3){
								var content = "取消";
								if(order.statusRemark){
									content += "("+order.statusRemark+")";
								}
								return "<font color='red'>" + content + "</font>";
							}
							
							if(value == 4){
								return "<font color='red'>取消</font>";
							}
						}
						
						function getSportName(type){
							if(type == 1){
								return "足球";
							}
							if(type == 2){
								return "篮球";
							}
							return "其他";
						}
						$('.betOrderDetail').click(function(){
							var id= $(this).attr('id');
							if(!id){
								return;
							}
							$.showIndicator();
							$.ajax({
								url:M.base+"/mobile/sports/hg/goOrderDetailById.do",
								data:{id:id},
								dataType:'json',
								type:'post',
								success:function(order){
									$.hideIndicator();
									$(".lottery_detail #plate").html(plateHelp(order.plate));
									$(".lottery_detail #odds").html(order.odds);
									$(".lottery_detail #bettingMoney").html(order.bettingMoney);
									$("#bettingOrderCode").html(order.bettingCode);
									$(".lottery_detail #bettingDate").html(order.bettingDate);
									$(".lottery_detail #sportType").html(getSportName(order.sportType));
									$(".lottery_detail #bettingStatus").html(getBettingStatus(order));
									$(".lottery_detail #balance").html(getBalanceStatus(order));
									$(".lottery_detail #matchInfo").html(getMatchInfo(order));
									$(".lottery_detail #typeNames").html(order.typeNames);
									if(order.bettingResult == undefined){
										$(".lottery_detail #bettingResult").html('');
									}else if(order.bettingResult > 0){
										$(".lottery_detail #bettingResult").html("<font color='green'>"+order.bettingResult+"</font>");
									}else{
										$(".lottery_detail #bettingResult").html(order.bettingResult);
									}
									$.popup("#betRecordLotteryPopup");
								}
							})
							
						});
						$('.lbcOrderDetail').click(function(){
							var id= $(this).attr('id');
							if(!id){
								return;
							}
							for(var i = 0; i < lbcData.rows.length; i++){
								if(lbcData.rows[i].id == id){
									if(lbcData.rows[i].playName == undefined){
										lbcData.rows[i].playName = '-'
									}
									$(".lottery_detail #plate").html('亚洲盘');
									$(".lottery_detail #bettingMoney").html(lbcData.rows[i].bettingMoney);
									$("#bettingOrderCode").html(lbcData.rows[i].bettingCode);
									$(".lottery_detail #bettingDate").html(lbcData.rows[i].bettingTime);
									$(".lottery_detail #sportType").html(lbcData.rows[i].gameName);
									$(".lottery_detail #bettingStatus").html('已确认');
									$(".lottery_detail #balance").html(getlbcStatus(lbcData.rows[i]));
									$(".lottery_detail #matchInfo").html(getMatchInfo(lbcData.rows[i]));
									$(".lottery_detail #typeNames").html(lbcData.rows[i].playName);
									$(".lottery_detail #bettingResult").html("<font color='green'>"+lbcData.rows[i].winMoney+"</font>");
								}
							}
							$.popup("#betRecordLotteryPopup");
						});
						

					}
				}
			})
		}
		
		function getBetStatus(row){
			if(row.bettingStatus == 3 || row.bettingStatus == 4){
				return "<font color='red'>系统取消</font>";
			}
			
			if(row.balance == 1){
				return "等待开奖";
			}
			
			if(row.balance == 4){
				return "<font color='blue'>赛事腰斩</font>";
			}
			
			if(row.balance == 2 || row.balance == 5 || row.balance == 6){
				if(row.bettingResult > 0){
					return "<font color='green'>派彩:"+row.bettingResult+"</font>";
				}
				return "<font color='red'>输</font>";
			}
		}
		function getlbcStatus(row){
			if(row.resStatus == 3){
				return "未中奖";
			}
			if(row.resStatus == 1){
				return "等待开奖";
			}
			if(row.resStatus == 4){
				return "<font color='blue'>撤单</font>";
			}
			if(row.resStatus == 5){
				return "<font color='blue'>派奖回滚成功</font>";
			}
			if(row.resStatus == 2 ){
				return "<font color='red'>已中奖</font>";
			}
			if(row.resStatus == 6 ){
				return "<font color='red'>回滚异常</font>";
			}
			if(row.resStatus == 7 ){
				return "<font color='red'>开奖异常</font>";
			}
			if(row.resStatus == 10){
				return "<font color='red'>取消注单</font>";
			}
		}
		
		//H:亚洲盘  I:印尼盘 E:欧洲盘 M:马来西亚盘
		function plateHelp(p){
			if(!p)return '';
			var pname = '';
			switch (p) {
			case 'H':
				pname = '亚洲盘';
				break;
			case 'I':
				pname = '印尼盘';
				break;
			case 'E':
				pname = '欧洲盘';
				break;
			case 'M':
				pname = '马来西亚盘';
				break;
			}
			return pname;
		}
		
		//1:滚球   2:今日  3:早盘
		function gameTimeTypeHelp(p){
			if(!p)return '';
			var pname = '';
			switch (p) {
			case 1:
				pname = '滚球';
				break;
			case 2:
				pname = '今日';
				break;
			case 3:
				pname = '早盘';
				break;
			}
			return pname;
		}
		
		//1 待确认  2：已确认  3：已取消
		function bettingStatusHelp(p){
			if(!p)return '';
			var pname = '';
			switch (p) {
			case 1:
				pname = '<font color="#337ab7">待确认</font>';
				break;
			case 2:
				pname = '<font color="#5cb85c">已确认</font>';
				break;
			case 3:
				pname = '<font color="red">已取消</font>';
				break;
			}
			return pname;
		}
		
		function getSportName(type){
			if(type == 1){
				return "足球";
			}
			if(type == 2){
				return "篮球";
			}
			return "其他";
		}
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller(); light7不需要此参数
			},1000)
		})
	})

	/**
	 * 真人投注记录
	 */
	$(document).on("pageInit","#live_game_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , type = null;
		
//		$page.on("click","#live_game_btn",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#liveGameHistoryDetail");
//		});
		
		changeData();
		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();
			
			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			type = $("#realType").val();
			ajaxData();
		}

		function ajaxData(){
			console.info('start_time:'+start_time);
			console.info('end_time:'+end_time);
			console.info('type:'+type);
			console.info('page:'+page);
			console.info('itemsPerLoad:'+itemsPerLoad);
			
			$.ajax({
				url:M.res+"/third/liveBetRecord.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time + " 00:00:00",
					endTime : end_time + " 23:59:59",
					type : type,
					page : page,
					rows : itemsPerLoad
				},
				success:function(res){
					if(res.msg && !res.success){
						$.toast(res.msg);
						//$.router.loadPage("#live_game_history");
						return;
					}else{
						if(res.total == 0){
							//无数据
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
							},2000)
							return;
						}else{
							totalPageCount = Math.ceil(res.total / itemsPerLoad);
							if(page<2){
								var html = '';
								var xBet = res.aggsData.bettingMoneyCount||0;
								var xWin = res.aggsData.winMoneyCount||0; 
								var xrWin = res.aggsData.realBettingMoneyCount||0;
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 有效投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xrWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}
							
							var result = "", temp ="", bd=null;;
							$.each(res.rows,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9">';
								}else{
									result += '<li>';
								}
								result += '<a href="javascript:void(0);" order_id="'+j.bettingCode+'" class="item-link item-content betOrderDetail">';
								result += '<div class="item-inner"><div class="item-title-row">';
//				                result += '<div class="item-title"><em>彩种：'+egameType(j.type)+'</em></div>';
								result += '<div class="item-title"><em>游戏名称：'+j.gameType+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
				                result += '<div class="item-subtitle">';
				        		/*if(j.type==3){
				        			result += item.tableCode;
				        		}else if(j.type==2){
				        			result+='';
				        		}else{
				        			if(j.gameTypeCode=="HUNTER"){
				        				result += item.playType;
				        			}
				        			result += item.tableCode;
				        		}*/
				        		 result += '</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">盈亏：'+j.winMoney+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
						}
					}
				}
			})
		}
	})
	/**
	 * 三方彩票记录
	 */
	$(document).on("pageInit","#lottery_third_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , type = null;
		
//		$page.on("click","#live_game_btn",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#liveGameHistoryDetail");
//		});
		
		changeData();
		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();
			
			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			type = $("#realType").val();
			ajaxData();
		}

		function ajaxData(){
			console.info('start_time:'+start_time);
			console.info('end_time:'+end_time);
			console.info('type:'+type);
			console.info('page:'+page);
			console.info('itemsPerLoad:'+itemsPerLoad);
			$.ajax({
				url:M.base+"/center/record/betrecord/thirdlotteryrecord.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time + " 00:00:00",
					endTime : end_time + " 23:59:59",
					type : type,
					page : page,
					rows : itemsPerLoad
				},
				success:function(res){
					if(res.msg && !res.success){
						$.toast(res.msg);
						//$.router.loadPage("#live_game_history");
						return;
					}else{
						if(res.total == 0){
							//无数据
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
							},2000)
							return;
						}else{
							totalPageCount = Math.ceil(res.total / itemsPerLoad);
							if(page<2){
								var html = '';
								var xBet = res.aggsData.bettingMoneyCount||0;
								var xWin = res.aggsData.winMoneyCount||0;
                                var xrWin = res.aggsData.realBettingMoneyCount||0;
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 有效投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xrWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}
							
							var result = "", temp ="", bd=null;;
							$.each(res.rows,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9">';
								}else{
									result += '<li>';
								}
								result += '<a href="javascript:void(0);" order_id="'+j.bettingCode+'" class="item-link item-content betOrderDetail">';
								result += '<div class="item-inner"><div class="item-title-row">';
				                result += '<div class="item-title"><em>彩种：'+egameType(j.type)+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
				                result += '<div class="item-subtitle">';
				        		if(j.type==3){
				        			result += j.tableCode;
				        		}else if(j.type==2){
				        			result+='';
				        		}else{
				        			if(j.gameTypeCode=="HUNTER"){
				        				result += j.playType;
				        			}
				        			result += j.tableCode;
				        		}
				        		 result += '</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">盈亏：'+j.winMoney+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
						}
					}
				}
			})
		}
	})
	
	/**
	 * 棋牌游戏记录
	 */
	$(document).on("pageInit","#chess_game_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , type = null;
		
//		$page.on("click","#live_game_btn",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#liveGameHistoryDetail");
//		});
		
		changeData();
		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();
			
			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			type = $("#realType").val();
			ajaxData();
		}
		
		function ajaxData(){
			console.info('start_time:'+start_time);
			console.info('end_time:'+end_time);
			console.info('type:'+type);
			console.info('page:'+page);
			console.info('itemsPerLoad:'+itemsPerLoad);
			
			$.ajax({
				url:M.base+"/center/record/betrecord/chessrecord.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time + " 00:00:00",
					endTime : end_time + " 23:59:59",
					type : type,
					page : page,
					rows : 99999
				},
				success:function(res){
					if(res.msg && !res.success){
						$.toast(res.msg);
						//$.router.loadPage("#live_game_history");
						return;
					}else{
						if(res.total == 0){
							//无数据
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
							},2000)
							return;
						}else{
							totalPageCount = Math.ceil(res.total / itemsPerLoad);
							if(page<2){
								var html = '';
								var xBet = res.aggsData.bettingMoneyCount||0;
								var xWin = res.aggsData.winMoneyCount||0;
                                var xrWin = res.aggsData.realBettingMoneyCount||0;
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 有效投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xrWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}
							
							var result = "", temp ="", bd=null;;
							$.each(res.rows,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9">';
								}else{
									result += '<li>';
								}
								result += '<a href="javascript:void(0);" order_id="'+j.bettingCode+'" class="item-link item-content betOrderDetail">';
								result += '<div class="item-inner"><div class="item-title-row">';
				                result += '<div class="item-title"><em>彩种：'+j.gameType+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
//				                result += '<div class="item-subtitle">';
//				        		if(j.type==3){
//				        			result += item.tableCode;
//				        		}else if(j.type==2){
//				        			result+='';
//				        		}else{
//				        			if(j.gameTypeCode=="HUNTER"){
//				        				result += j.playType;
//				        			}
//				        			result += j.tableCode;
//				        		}
//				        		 result += '</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">盈亏：'+j.winMoney+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
							$('.betOrderDetail').click(function(){
								var codeId = $(this).attr('order_id');
								var codehtml = '';
								for(var n=0;n<res.rows.length;n++){
									if(res.rows[n].bettingCode == codeId){
										var date = new Date(res.rows[n].bettingTime);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
								        var Y = date.getFullYear() + '-';
								        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
								        var D = date.getDate() + ' ';
								        var h = date.getHours() + ':';
								        var m = (date.getMinutes() < 10 ? '0'+(date.getMinutes()) : date.getMinutes()) + ':';
								        var s = (date.getSeconds() < 10 ? '0'+(date.getSeconds()) : date.getSeconds());

										$('#kyxiangqing').find('.1').text(res.rows[n].bettingCode);
										$('#kyxiangqing').find('.2').text(res.rows[n].account);
										$('#kyxiangqing').find('.3').text(Y+M+D+h+m+s);
										$('#kyxiangqing').find('.4').text(res.rows[n].gameType);
										$('#kyxiangqing').find('.5').text(res.rows[n].gameCode);
										$('#kyxiangqing').find('.6').text(res.rows[n].bettingMoney);
										$('#kyxiangqing').find('.7').text(res.rows[n].realBettingMoney);
										$('#kyxiangqing').find('.8').text(res.rows[n].bettingContent);
										$('#kyxiangqing').find('.9').text(res.rows[n].winMoney);
										$('#kyxiangqing').show(200);
									}
								}
							});
							$('.closeThis').click(function(){
								$('#kyxiangqing').hide(200);
							});
						}
					}
				}
			})
		}
	})
	/**
	 * 电子投注记录
	 */
	$(document).on("pageInit","#e_game_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , type = null;
		
//		$page.on("click","#e_game_btn",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#egameHistoryDetail");
//		});
		
		changeData();
		
		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();
			
			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			type = $("#egameType").val();
			ajaxData();
		}
		function ajaxData(){
			$.ajax({
				url:M.base+"/center/record/betrecord/egamerecord.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time,
					endTime : end_time,
					type : type,
					pageNumber : page,
					pageSize : itemsPerLoad,
				},
				success:function(res){
					if(res.msg && !res.success){
						$.toast(res.msg);
						//$.router.loadPage("#e_game_history");
						return;
					}else{
						if(res.total == 0){
							//无数据
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
							},2000)
							return;
						}else{
							totalPageCount = Math.ceil(res.total / itemsPerLoad);
							if(page<2){
								var html = '';
								var xBet = res.aggsData.bettingMoneyCount||0;
								var xWin = res.aggsData.winMoneyCount||0;
                                var xrWin = res.aggsData.realBettingMoneyCount||0;
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 有效投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xrWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}
							
							var result = "", temp ="", bd=null;;
							$.each(res.rows,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9">';
								}else{
									result += '<li>';
								}
								result += '<a href="javascript:void(0);" order_id="'+j.bettingCode+'" class="item-link item-content betOrderDetail">';
								result += '<div class="item-inner"><div class="item-title-row">';
				                result += '<div class="item-title"><em>彩种：'+egameType(j.type)+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
				                result += '<div class="item-subtitle">';
				        		if(j.type==3){
				        			result += j.tableCode;
				        		}else if(j.type==2){
				        			result+='';
				        		}else{
				        			if(j.gameTypeCode=="HUNTER"){
				        				result += item.playType;
				        			}
				        			result += j.tableCode;
				        		}
				        		 result += '</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">盈亏：'+j.winMoney+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
							
							
						}
						
					
					
					}
				}
			});
			
		}
		
		//电子游戏类型
		function egameType(item){
			var temp = '其他';
			switch(item.type-0){
				case 1: temp = "AG";break;
				case 2: temp = "BBIN";break;
				case 3: temp = "MG";break;
				case 4:temp = "QT";break;
				case 5:temp = "ALLBET";break;
				case 6:temp = "PT";break;
				case 7:temp = "OG";break;
				case 8:temp = "DS";break;
				case 9:temp = "CQ9";break;
				case 11:temp = "JDB";break;
				case 12:temp = "KY";break;
				case 92:temp = "ttg";break;
				case 95:temp = "mw";break;
				case 96:temp = "ISB";break;
				case 98:temp = "BG";break;
			}
			return temp;
		}
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller(); light7不需要此参数
			},1000)
		})
		
	})
	/**
	 * 电竞投注记录
	 */
	$(document).on("pageInit","#esport_history",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , type = null;

//		$page.on("click","#e_game_btn",function(){
//			initFlag = true;
//			changeData();
//			$.router.loadPage("#egameHistoryDetail");
//		});

		changeData();

		var $androidActionSheet = $page.find('#androidActionsheet');
		$page.on("click","#showAndroidActionSheet",function(){
			if($androidActionSheet.is(":hidden")){
				$androidActionSheet.fadeIn(200);
			}else{
				$androidActionSheet.fadeOut(200);
			}
		}).on("click","#androidActionsheet .weui-mask",function(){
			$androidActionSheet.fadeOut(200);
		}).on("click","a.button-danger",function(){
			changeData();

			$androidActionSheet.fadeOut(200);
		})

		function changeData(){
			page = 1;//重新初始化页数
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			type = $("#egameType").val();
			ajaxData();
		}
		function ajaxData(){
			$.ajax({
				url:M.base+"/native/esportsRecord.do ",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time,
					endTime : end_time,
					type : type,
					pageNumber : page,
					pageSize : itemsPerLoad,
				},
				success:function(res){
					if(res.msg && !res.success){
						$.toast(res.msg);
						//$.router.loadPage("#e_game_history");
						return;
					}else{
						if(res.total == 0){
							//无数据
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								$(".infinite-scroll .list-container").html('<li style="text-align: center;background: #efeff4;line-height: 4rem;">暂没有投注记录</li>')
							},2000)
							return;
						}else{
							totalPageCount = Math.ceil(res.total / itemsPerLoad);
							if(page<2){
								var html = '';
								var xBet = res.aggsData.bettingMoneyCount||0;
								var xWin = res.aggsData.winMoneyCount||0;
                                var xrWin = res.aggsData.realBettingMoneyCount||0;
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 有效投注</em><p style="color: red">'+MobileV3.record.toDecimal2(xrWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖</em><p style="color: #00aa00">'+MobileV3.record.toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利</em><p style="color: red">'+MobileV3.record.toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}

							var result = "", temp ="", bd=null;;
							$.each(res.rows,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9" data-index="'+i+'">';
								}else{
									result += '<li data-index="'+i+'">';
								}
								result += '<a href="javascript:void(0);" order_id="'+j.bettingCode+'" class="item-link item-content betOrderDetail">';
								result += '<div class="item-inner">';
				                result += '<div class="item-title-row"><div class="item-title"><em>游戏名称：'+j.gameName+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.bettingMoney+'</em>元</div></div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+getMyDate(j.bettingTime)+'<em style="display:inline-block;float:right;">盈亏：'+j.winMoney+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
							$('.list-container li').click(function () {
								let index = $(this).attr('data-index') , data = res.rows[index]
								$(".ebettingCode").html(data.bettingCode)
								$(".eaccount").html(data.account)
								$(".ebettingTime").html(getMyDate(data.bettingTime))
								$(".egameName").html(data.gameName)
								$(".einfo").html(data.league)
								$(".ebettingMoney").html(data.bettingMoney)
								$(".ewinMoney").html(data.winMoney)
								$("#kyxiangqing").css('margin-top','0')
							})
						}
					}
				}
			});

		}
		//电竞游戏类型
		function esportType(item){
			var temp = '其他';
			switch(item.type-0){
				case 15:return "LH";
				case 16:return "KX";
			}
			return temp;
		}

		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller(); light7不需要此参数
			},1000)
		})

	})
	function getMyDate(str){
		                    var oDate = new Date(str),
		                        oYear = oDate.getFullYear(),
		                        oMonth = oDate.getMonth()+1,
		                        oDay = oDate.getDate(),
		                        oHour = oDate.getHours(),
		                        oMin = oDate.getMinutes(),
		                        oSen = oDate.getSeconds(),
		                        oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay) +' '+ getzf(oHour) +':'+ getzf(oMin) +':'+getzf(oSen);//最后拼接时间
		                    return oTime ;
		                };
		                function getzf(num){
		                    if(parseInt(num) < 10){
		                        num = '0'+num;
		                    }
		                    return num;
		                }
		                function unExistLine(un){
		                	if(typeof(un)=='undefined'){
		                		un = '-'
		                	}
		                	return un;
		                }