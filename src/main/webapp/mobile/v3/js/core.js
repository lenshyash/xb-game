(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;
	window.$ajax = _ajax;
	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					alert("[" + opt.url + "] 404 not found");
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					if(window.reloadImg){
						reloadImg();
					}
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					if(window.reloadImg){
						reloadImg();
					}
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					if(window.reloadImg){
						reloadImg();
					}
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					if(window.reloadImg){
						reloadImg();
					}
					alert("没有权限");
				} else if (ceipstate == 6) {// 登录异常
					alert(data.msg);
					top.location.href = M.base + "/loginError.do";
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
})(Zepto);