var processBetTraceCore = function(){
	return{
	    generatedTraceData: false,// 是否已经生成并显示追号期号
	    thisOrderMaybeLose: false,// 可能出现亏损
	    qiHaos:[],// 可追号得期号
	    startQiShu:10,// 追号期数
	    startTimes:1,// 起始倍数
	    totalCost:0,// 总下注金额
	    kuajieTimers:1,//两期之间的倍数差
	    profitArr:false,// 盈利情况
	    initView:function(d){// 保存当天可追号得期号，并显示追号期号
	    	this.qiHaos = d;
	    	this.generateBetView();
	    },
	    generateBetView: function() {// 生成并显示追号期号
	    	if(!MobileV3.curQiHao || !this.qiHaos||this.qiHaos.length==0)return;
	        this.generatedTraceData = true;
	        var html = "",len = this.qiHaos.length,qh,cur=false,bb=this.startTimes;
            for (var a = 0,b=0; a < len && b<this.startQiShu;a++) {
            	qh = this.qiHaos[a];
            	cur = a == 0?false:true;
            	var data = {
    	        		bs:bb,
    	        		qh:this.qiHaos[a],
    	        		index:a+1,
    	        		curs:cur,
    	        }
            	if(qh<MobileV3.curQiHao){
            		continue;
            	}
            	if(this.kuajieTimers == 1){
            		bb = bb + this.kuajieTimers;
            	}else{
            		bb = bb*this.kuajieTimers;
            	}
            	b++;
            	html += template('bettraceTpl',data);
            }
            $("#fbOrderList").html(html);
	        this.updateBetData();
	    },
	    updateBetData: function() {// 更新追号期号信息
	        if (!this.generatedTraceData) {
	            return false;
	        }
	        var trs=$("#orderList").find("li[buyinfo]"),tongJi=true,zhuiHao=$("#doIntelligentSelectionPage").hasClass("page-current");
	    	if(trs.length>0){
	    		var playCode="",buyInfos=[],cost=0,c,e;
	    		trs.each(function(){
		    		var it = $(this);
		    		if(playCode==""){
		    			playCode=it.attr("c");
		    		}else if(playCode!=it.attr("c") && zhuiHao){
		    			tongJi=false;
		    		}
		    		cost +=(parseFloat(it.attr("m"),10)||0);
		    		c = it.attr("buyinfo").split("\|");
	    			e={};
	    			e.cash=parseInt(c[3])/parseInt(c[2]);
		    		e.totalCost=cost;
	    			e.rate=(parseFloat(it.attr("r"),10)||0);
	    			e.rateCash=e.cash*e.rate;
	    			e.balls=c[4];
	    			e.ballArgs=[];
	    			e.balls.match("@")&&(c=e.balls.split("@"),e.balls=c[1],e.ballArgs=c[0].split(","));
	    			e.balls=e.balls.split(",");
	    			c=(BetLotterys.lotType == 3 || BetLotterys.lotType == 5 || BetLotterys.lotType == 53 || BetLotterys.lotType == 55)?/\d\d|龙|虎/g:/\d|大|小|单|双/g;
	    			for(var d in e.balls)
	    				e.balls[d]=e.balls[d].match(c);
		    		buyInfos.push(e);
		    	});
	    		if(!tongJi){
	    			$.toast('您选择的多种玩法无法计算盈利');
		    	}
	    		this.totalCost=cost;
	    		processBetTraceCore.calcView();
	        } else {// 还没有下注情况，不可以追号
	            this.totalCost = 0;
	            this.profitArr = false;
	            this.generatedTraceData = false;
	        }
	    },
	    calcView: function() {// 计算投注金额及其盈利情况，并显示
	        var traceCount = 0,amount=0;
	        this.thisOrderMaybeLose = false;
	        var _this = this,totalCost=_this.totalCost,beforeCount=0;
	        $("#fbOrderList").find('tr').each(function(i, j) {
	            j = $(this).children();
	            var betRate = parseInt(j.eq(1).find('.jiajianinpt').val())||0;// 倍数
	            if (betRate > 0) {
	                traceCount++;
	                i=betRate * totalCost;
                	beforeCount+=i;
                	j.eq(2).find("span.betCost").text(i.toFixed(2));
                    j.eq(2).find("span.betTotalCost").text(beforeCount.toFixed(2));
	            } else {
	            	j.eq(2).find("span.betCost").text('--');
	            	j.eq(2).find("span.betTotalCost").text('--');
	            }
	        });
	        $("#fbCount").text(traceCount);// 追号期数
	        $("#fbCash").text(beforeCount.toFixed(2));// 追号总金额
	    },
	    calcProfit:function(buyInfos,playCode){// 计算下注盈利
	    	var a,r={};
	    	this.temp=[];
	    	for(var i = 0,len=buyInfos.length;i<len;i++){
	    		a=buyInfos[i];
	    		switch(playCode){
		    	case "dwd":// 定位胆
		    		r=this.multipleWinning(a.balls,a.rateCash);
	    			break;
		    	case "q2zx_fs":
		    	case "h2zx_hz":
		    	case "q2zx_hz":
		    	case "h2zx_fs":
		    	case "q3zx_fs":
		    	case "z3zx_fs":
		    	case "h3zx_fs":
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    		break;
		    	case "bdw_q31m":
		    	case "bdw_h31m":
		    	case "bdw_z31m":
		    		this.combineProfitArray(r,a.balls,a.rateCash);break;
		    	case "rxwf_r2zx_fs":
		    		r=this.multipleWinning(this.anyDirect(2,a.balls),a.rateCash);break;
		    	case "rxwf_r3zx_fs":
		    		r=this.multipleWinning(this.anyDirect(3,a.balls),a.rateCash);break;
		    	case "rxwf_r3zux_zu3":
		    		r=this.multipleWinning(this.anyGroup(3,2,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r3zux_zu6":
		    		r=this.multipleWinning(this.anyGroup(3,3,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r4zx_fs":
		    		r=this.multipleWinning(this.anyDirect(4,a.balls),a.rateCash);break;
		    	case "q3zux_zu3":
		    	case "h3zux_zu3":
		    	case "z3zux_zu3":
		    		this.combineProfitArray(r,this.generatorGroup(2,a.balls),a.rateCash);break;
		    	case "q3zux_zu6":
		    	case "h3zux_zu6":
		    	case "z3zux_zu6":
		    		this.combineProfitArray(r,this.generatorGroup(3,a.balls),a.rateCash);break;
		    	default:
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    	}
	    	}
    		var m=Number.MIN_VALUE,x=Number.MAX_VALUE,b;
			for(var d in r){
				b=r[d];
				m=Math.max(m,b);
				x=Math.min(x,b);
			}
			if(m!=Number.MIN_VALUE){
				return {maxProfit:parseFloat(m.toFixed(2)),minProfit:parseFloat(x.toFixed(2))};
	    	}
	    	return false;
	    },
	    generatorBetTrace:function(betRate, beforeCount){// 计算每一条追号得盈利
	    	var min = this.profitArr.minProfit,max = this.profitArr.maxProfit,minEarn=0,maxEarn=0,earnStr="",percentStr="";
	    	minEarn = min*betRate -beforeCount;
	    	maxEarn=max*betRate - beforeCount;
	    	earnStr = (minEarn==maxEarn)?minEarn.toFixed(2):minEarn.toFixed(2)+"至"+maxEarn.toFixed(2);
	    	percentStr = (minEarn==maxEarn)?((minEarn/beforeCount*100).toFixed(2)+"%"):((minEarn/beforeCount*100).toFixed(2)+"%")+"至"+((maxEarn/beforeCount*100).toFixed(2)+"%");
	    	return {earnStr:earnStr,percentStr:percentStr,minEarn:minEarn,maxEarn:maxEarn};
	    },
	    multipleWinning:function(b,d){
	    	var c,a,e,f,g,h;
	    	for(e in b){
	    		this.temp[e]instanceof Array||(this.temp[e]=[]),this.combineProfitArray(this.temp[e],b[e],d);
	    	}
    		h=g=0;
    		for(e in this.temp){
    			a=[];
    			for(f in this.temp[e])
    				a.push(this.temp[e][f]);
    			c=Math.max.apply(null,a);
    			a=Math.min.apply(null,a);
    			-Infinity===c&&(c=0);
    			Infinity===a&&(a=0);
    			g+=c;
    			h+=a
    		}
    		return{maxProfit:g,minProfit:h}
	    },
	    combineProfitArray:function(b,d,c){
	    	if(!(d instanceof Array))return!1;
	    	var a;
	    	for(var e= 0,f=d.length;e<f;e++){
	    		a=d[e],null!==a&&void 0!==a&&(b.hasOwnProperty(a)?b[a]+=c:b[a]=c);
	    	}
	    	return b
    	},
    	expandArray:function(b){
    		var d,c,a,e,f,g,h,k,l,n,m;
    		if(0===b.length)return!1;
    		b=b.slice();
    		m=b.splice(0,1)[0];
    		m instanceof Array||(m=m.toString().split(","));
    		f=0;
    		for(k=b.length;f<k;f++)
    			for(a=b[f],c=m.slice(),m=[],g=0,l=a.length;g<l;g++)
    				for(e=a[g],h=0,n=c.length;h<n;h++)
    					d=c[h],m.push(""+d+e);
    		return m
    	},
    	anyDirect:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G,J,L,K,M;
    		D=[];
    		if(2===b){
    			for(r=e=0,m=d.length-b;0<=m?e<=m:e>=m;r=0<=m?++e:--e){
    				if(null!==d[r]){
    					for(q=d[r],g=0,l=q.length;g<l;g++){
    						for(c=q[g],u=f=p=r+1,t=d.length;p<=t?f<t:f>t;u=p<=t?++f:--f){
    							if(null!==d[u]){
    								for(v=d[u],k=0,n=v.length;k<n;k++){
    									a=v[k];
    									h=""+r+u;
    									D[h]instanceof Array||(D[h]=[]);
    									D[h].push(""+c+a);
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(3===b){
    			for(r=n=0,z=d.length-b;0<=z?n<=z:n>=z;r=0<=z?++n:--n){
    				if(null!==d[r]){
    					for(B=d[r],m=0,f=B.length;m<f;m++){
    						for(c=B[m],u=q=C=r+1,A=d.length-1;C<=A?q<A:q>A;u=C<=A?++q:--q){
    							if(null!==d[u]){
    								for(E=d[u],p=0,k=E.length;p<k;p++){
    									for(a=E[p],g=t=x=u+1,w=d.length;x<=w?t<w:t>w;g=x<=w?++t:--t){
    										if(null!==d[g]){
    											for(y=d[g],v=0,l=y.length;v<l;v++){
    												e=y[v];
    												h=""+r+u+g;
    												D[h]instanceof Array||(D[h]=[]);
    												D[h].push(""+c+a+e);
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(4===b){
    			for(r=F=0,p=d.length-b;0<=p?F<=p:F>=p;r=0<=p?++F:--F){
    				if(null!==d[r]){
    					for(t=d[r],I=0,k=t.length;I<k;I++){
    						for(c=t[I],u=H=v=r+1,x=d.length-2;v<=x?H<x:H>x;u=v<=x?++H:--H){
    							if(null!==d[u]){
    								for(w=d[u],G=0,l=w.length;G<l;G++){
    									for(a=w[G],g=J=y=u+1,z=d.length-1;y<=z?J<z:J>z;g=y<=z?++J:--J){
    										if(null!==d[g]){
    											for(B=d[g],L=0,n=B.length;L<n;L++){
    												for(e=B[L],q=K=C=g+1,A=d.length;C<=A?K<A:K>A;q=C<=A?++K:--K){
    													if(null!==d[q]){
    														for(E=d[q],M=0,m=E.length;M<m;M++){
    															f=E[M];
    															h=""+r+u+g+q;
    															D[h]instanceof Array||(D[h]=[]);
    															D[h].push(""+c+a+e+f);
    														}
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else {
    			layer.msg("任"+b+"直选错误",{icon:5});
    		}
    		return D
    	},
    	anyGroup:function(b,d,c,a){
    		var e,f;
    		null==b&&(b=2);
    		null==d&&(d=2);
    		null==c&&(c=[]);
    		null==a&&(a=['万','千','百','十','个']);
    		f=[];
    		b=parseInt(b);
    		d=parseInt(d);
    		c instanceof Array||(c=c.split(","));
    		if(a.length<b){
    			layer.msg("组选复式参数错误"),{icon:5};
    			return false;
    		}
    		c=this.generatorGroup(d,c);
    		e=this.generatorGroup(b,a);
    		b=0;
    		for(a=e.length;b<a;b++){
    			d=e[b];
    			f[d]=c;
    		}
    		return f
    	},
    	generatorGroup:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G;
    		null==b&&(b=2);
    		null==d&&(d=[]);
    		G=[];
    		b=parseInt(b);
    		d=d instanceof Array?d.slice():d.splt(",");
    		if(1===b)return d;
    		if(2===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=0,p=d.length;l<p;l++)
    					u=d[l],G.push(""+r+u);
    		else if(3===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=n=0,q=d.length;0<=q?n<q:n>q;l=0<=q?++n:--n)
    					for(u=d[l],p=m=k=l+1,f=d.length;k<=f?m<f:m>f;p=k<=f?++m:--m)l=d[p],G.push(""+r+u+l);
    		else if(4===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=q=0,m=d.length-2;0<=m?q<m:q>m;l=0<=m?++q:--q)
    					for(u=d[l],p=k=f=l+1,w=d.length-1;f<=w?k<w:k>w;p=f<=w?++k:--k)
    						for(l=d[p],n=g=v=p+1,y=d.length;v<=y?g<y:g>y;n=v<=y?++g:--g)p=d[n],G.push(""+r+u+l+p);
    		else if(5===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=m=0,k=d.length-3;0<=k?m<k:m>k;l=0<=k?++m:--m)
    					for(u=d[l],p=f=g=l+1,w=d.length-2;g<=w?f<w:f>w;p=g<=w?++f:--f)
    						for(l=d[p],n=v=y=p+1,h=d.length-1;y<=h?v<h:v>h;n=y<=h?++v:--v)
    							for(p=d[n],q=x=z=n+1,c=d.length;z<=c?x<c:x>c;q=z<=c?++x:--x)
    								n=d[q],G.push(""+r+u+l+p+n);
    		else if(6===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,k=d.length-4;0<=k?f<k:f>k;l=0<=k?++f:--f)
    					for(u=d[l],p=v=w=l+1,g=d.length-3;w<=g?v<g:v>g;p=w<=g?++v:--v)
    						for(l=d[p],n=x=y=p+1,h=d.length-2;y<=h?x<h:x>h;n=y<=h?++x:--x)
    							for(p=d[n],q=C=z=n+1,c=d.length-1;z<=c?C<c:C>c;q=z<=c?++C:--C)
    								for(n=d[q],m=A=B=q+1,a=d.length;B<=a?A<a:A>a;m=B<=a?++A:--A)q=d[m],G.push(""+r+u+l+p+n+q);
    		else if(7===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=w=0,f=d.length-5;0<=f?w<f:w>f;l=0<=f?++w:--w)
    					for(u=d[l],p=y=g=l+1,v=d.length-4;g<=v?y<v:y>v;p=g<=v?++y:--y)
    						for(l=d[p],n=z=h=p+1,x=d.length-3;h<=x?z<x:z>x;n=h<=x?++z:--z)
    							for(p=d[n],q=c=C=n+1,B=d.length-2;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-1;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										m=d[k],G.push(""+r+u+l+p+n+q+m);
    		else if(8===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,w=d.length-6;0<=w?f<w:f>w;l=0<=w?++f:--f)
    					for(u=d[l],p=g=v=l+1,y=d.length-5;v<=y?g<y:g>y;p=v<=y?++g:--g)
    						for(l=d[p],n=h=x=p+1,z=d.length-4;x<=z?h<z:h>z;n=x<=z?++h:--h)
    							for(p=d[n],q=c=C=n+1,B=d.length-3;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-2;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length-1;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										for(m=d[k],t=k=I=k+1,H=d.length;I<=H?k<H:k>H;t=I<=H?++k:--k)
    											t=d[t],G.push(""+r+u+l+p+n+q+m+t);
    		else layer.msg("长度组选错误",{icon:5});
    		return G
    	}
	}
}();