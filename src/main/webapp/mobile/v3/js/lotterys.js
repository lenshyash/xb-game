MobileV3.betting = function(){
	return {
		resetSumbetCount:function(){
			bet_result = {};
			$(".betZhushu,.orderTotalCost").text(0);
//			if(_Core.code == 'LHC' || _Core.code == 'SFLHC'){
//				Mark.resetTableDataInfo();
//			}
		},
			sumbetCount: function(eventEle) {// 计算投注注数和投注金额入口
				var c2 =$("#select_area_content_standard"),
				_type=c2.attr("_type"),
				_row = c2.attr('_row'),
		        _column = c2.attr('_column'),
		        _val = c2.attr('_val');
				var _vs;
				if(_type==22){
					var it=c2.find('#lt_write_box'),val=it.val(),pattern=new RegExp(it.attr("_pattern")),type=it.attr("_type"),desc=it.attr("_desc")||"";
				 	_vs=val.split(/，|;|\r|\n| /);
		        	for(var i =0;i<_vs.length;i++){
		        		if(_vs[i] && !pattern.test(_vs[i])){
		        			if(evt.type=="blue"||evt.type=="focusout")layer.msg(_vs[i]+"格式不对。"+desc,{icon:5});
		        			MobileV3.betting.sumTextareaBetCount(type,null);
		        			return;
		        		}
		        	}
				}
		        bet_result = {};
		        bet_result.types = _type;
		        switch (_type) {
	            case "1":
	            	MobileV3.betting.__Duplex(_row, _column,_val);
	                break;
	            case "3":
	            	MobileV3.betting.__Positioninggall(_row, _column);
	                break;
	            case "4":
	            	MobileV3.betting.__ElectionsAndValues(_row, _column);break;
	            case "5":
	            	MobileV3.betting.__Constitute(_row, _column, _val, c2.attr('_arbitrary'));
	                break;
	            case "8":
	            	MobileV3.betting.__Arbitrary_Duplex(_row, _column);break;
	            case "11":
	            	MobileV3.betting.__Nrepeat(_row, _column);break;
	            case "12":
	            	MobileV3.betting.__UseC(_row, _column);break;
	            case "13":
	            	MobileV3.betting._HuYi2Row(c2,eventEle);break;
	            case "22":
	            	MobileV3.betting.sumTextareaBetCount(_type,_vs);break;
	        }
		},
		sumTextareaBetCount: function(_type,_vals) {// 计算单式投注注数和投注金额入口
	    	bet_result = {};
	    	bet_result.types = _type;
	    	var zhuShu = 0, ma="";
	    	if(_vals){
	    		for(var i=0,len=_vals.length;i<len;i++){
		    		if(_vals[i]){
		        		ma=ma+";"+_vals[i];
		        		zhuShu++;
		    		}
		    	}
	    	}
	        bet_result.betCount = zhuShu;
	        bet_result.myarray =ma?ma.substring(1):"";
	        bet_result.errorTagStr = "";
	        bet_result.optional = "";
	        $(".betZhushu").text(zhuShu);
	        /*$("#beiShu").val()$(".choose-money li.on").attr("value");*/
	        var t=$("#lotteryBetModel a.active").data("unit");
	        if(t==""||t==undefined){
	        	t = 1;
	        }
	        var s = $("#beishuInput").val();
	        if(s==""||s==undefined){
	        	s = 1;
	        }
	        var l = MobileV3.price;
	        l *=MobileV3.utils.__FloatMul(zhuShu,s);
	        l = MobileV3.utils.__FloatDiv(l, t);
	        $(".orderTotalCost").text(l);
	    },
	    _HuYi2Row:function(con,eventEle){
	    	var arr = ["",""],n=eventEle.data("num");
	    	if(eventEle.hasClass("cur")){
	    		if(n.length==2){
		    		con.find("span[data-num='"+n.substring(1)+"']").removeClass("cur");
		    	}else{
		    		con.find("span[data-num='"+n+n+"']").removeClass("cur");
		    	}
	    	}
	    	con.find(".xuanmabox").each(function(a, e) {
	            $(e).find("span.cur").each(function(i,j) {
	            	arr[a] +=$(j).data("num");
	            });
	        });
	    	var zhuShu=arr[0].length/2*arr[1].length;
	    	MobileV3.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Duplex: function(_row, _column,_val) {
	    	_row = _row || 1;
	    	_column = _column || 1;
	        var arr = [],row = 0,column = 0,zhuShu = 1;
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		column++;
	            		if("undefined" != typeof arr[a]){
	            			arr[a] += i;
	            		}else{
	            			arr[a] =i;
	            			row++;
	            		}
	            	}
	            })
	        });
	        if (row != _row || column < _column) {
	        	MobileV3.betting.resetSumbetCount();
	        	return false;
	        }
	        for (var t in arr) zhuShu *= arr[t].length;
	        if(_val){
	        	zhuShu=zhuShu/_val;
	        }
	        MobileV3.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Positioninggall: function(a, e) {
	        e = e || 1;
	        var g = 0,
	            c = 0,
	            h = 0,
	            l = [],q=BetLotterys.lotCode,
	            m = Array(parseInt(a || 5));
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            		}else{
	            			m[a] =i;
	            		}
	            	}
	            });
	            h++
	        });
	        if (0 > h || c < e) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        for (var t in m) g += m[t].length;
	        if ("SFSC" == q || "BJSC"  == q || "WFSC"  == q || "WFFT"  == q || "LBJSC"  == q || "FFSC"  == q ||"XYFT"  == q  ||"LXYFT"  == q || "SD11X5" == q || "SH11X5" == q || "GX11X5" == q || "JX11X5" == q || "GD11X5" == q) g /= 2;
	        MobileV3.betting.__showAndSaveBet(g, m)
	    },
	    __ElectionsAndValues: function(a, e) {
	        var c = 0,
	            h = 0,
	            l = 0,
	            m = [];
	        switch (e) {
	            case "J_3":
	                var q = [10, 54, 96, 126, 144, 150, 144, 126, 96, 54];
	                break;
	            case "S_3":
	                q = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1];
	                break;
	            case "B_3":
	            	q = [0, 0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 8, 8, 6, 6, 4, 4, 2, 2];
	        		break;
	            case "G_3":
	                q = [-1, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1];
	                break;
	            case "J_2":
	                q = [10, 18, 16, 14, 12, 10, 8, 6, 4, 2];
	                break;
	            case "S_2":
	                q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
	                break;
	            case "G_2":
	                q = [-1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1];
	                break;
	            case "S_1":
	                q = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	                break;
	            case "K_3":
	                q = [0,0,0,1,3, 6, 10, 15, 21, 25, 27, 27, 25, 21, 15, 10, 6, 3, 1];
	                break;
	        }
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		h++;
	            		m.push(i);
	            	}
	            });
	            l++
	        });
	        if (l < a) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        for (var t in m) c += parseInt(q[m[t]]);
	        MobileV3.betting.__showAndSaveBet(c, m)
	    },
	    __Constitute: function(_row, _column, _val, _arbitrary) {
	        var h = 1,l = 1,column = 0,row = 0,arr = [];
	        _arbitrary = _arbitrary || 1;
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		column++;
	        			arr.push(i);
	            	}
	            });
	            row++;
	        });
	        if (row != _row || column < _column) {
	        	MobileV3.betting.resetSumbetCount();
	        	return false;
	        }
	        var a=0;
	        switch (_val) {
	            case "G_2":
	                for (a = arr.length; a > arr.length - _column; a--) h *= a;
	                break;
	            case "P_1":
	            case "P_2":
	            case "P_3":
	            case "G_3":
	            case "R_2":
	                for (a = 0; a < _column; a++) h *= arr.length - a;
	                for (a = _column; 0 < a; a--) l *= a;
	                h /= l;
	                break;
	        }
	        var r;
	        switch (_arbitrary) {
	            case 1:
	                break;
	            default:{
	            	h *= $(".radio_select span#positioninfo").text();
	                r = MobileV3.betting.position_OptionalArr().join(",");
	            }
	        }
	        MobileV3.betting.__showAndSaveBet(h, arr,false, r)
	    },
	    position_OptionalArr: function() {
	        var e = [];
	        $(".radio_select input[name^='position_']").each(function() {
	            $(this).prop("checked") && e.push($(this).val())
	        });
	        return e
	    },
	    __Arbitrary_Duplex: function(a, e) {
	        var c = 0,
	            h = 0,
	            m = [],
	            q = [];
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            			q[a] += 1
	            		}else{
	            			m[a] =i;
	            			q[a] = 1;
	            			h++;
	            		}
	            	}
	            });
	        });
	        if (h < a || c < e) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        var g = MobileV3.betting.__Elections(q, e);
	        while(m.length<5){
	        	m[m.length]="";
	        }
	        MobileV3.betting.__showAndSaveBet(g, m)
	    },
	    __Elections: function(a, e) {
	        var g = 0,
	            c = $.map(a, function(a) {
	                if ("" !== a) return a
	            }),
	            c = MobileV3.betting.__C_combine(c, e);
	        switch (e) {
	            case "2":
	                for (var h in c) g += c[h][0] * c[h][1];
	                break;
	            case "3":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2];
	                break;
	            case "4":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2] * c[h][3]
	        }
	        return g
	    },
	    __C_combine: function(a, e) {
	        for (var g = [[]], c = [], h = 0, l = a.length; h < l; ++h)
	            for (var m = 0,q = g.length; m < q; ++m)(g[m].length < e - 1 ? g : c).push(g[m].concat([a[h]]));
	        return c
	    },
	    __Nrepeat: function(a, e) {
	        var g = 1,
	            c = 0,
	            h = 0,
	            l = [],
	            m = [];
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a].push(i);
	            			l[a] += i
	            		}else{
	            			m[a]=[i];
	            			l[a] =i;
	            			h++;
	            		}
	            	}
	            })
	        });
	        if (h < a || c < e) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        switch (e) {
	            case "2":
	                for (var t in m) g *= MobileV3.betting.___P(m[t].length, 1);
	                g -= MobileV3.betting.arrayIntersection(m[0],m[1]).length;
	                break;
	            case "3":
	                g = m[0].length * m[1].length * m[2].length;
	                var q = MobileV3.betting.arrayIntersection(m[0], m[1]).length * m[2].length;
	                var t = MobileV3.betting.arrayIntersection(m[1], m[2]).length * m[0].length;
	                var r = MobileV3.betting.arrayIntersection(m[2], m[0]).length * m[1].length,
	                    y = 2 * MobileV3.betting.count(MobileV3.betting.array_intersect(m[0], m[1], m[2])),
	                    g = g - q - t - r + y
	        }
	        MobileV3.betting.__showAndSaveBet(g, l)
	    },
	    __UseC: function(a, e) {
	        var g = [],h = 0,l = 0,m = $("#select_area_content_standard").attr("_g11") || false;
	        $("#select_area_content_standard li.numRow").each(function(a, e) {
	            $(e).find(".xuanmabox span.cur").each(function(i,j) {
	            	j=$(j);
	            	i=j.data("num");
	            	if(i){
	            		l++;
	            		g.push(i);
	        			h++;
	            	}
	            })
	        });
	        if (m) {
	            if (h < a || l < e) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        } else if (h != a || l < e) return $(".betZhushu,.orderTotalCost").text(0), !1;
	        m = MobileV3.betting.__C_combine(g, e).length;
	        MobileV3.betting.__showAndSaveBet(m, g)
	    },
	    arrayIntersection: function(a, e) {
	        for (var g = 0, c = 0, h = []; g < a.length && c < e.length;) a[g] < e[c] ? g++ : (a[g] > e[c] || (h.push(a[g]), g++), c++);
	        return h
	    },
	    ___P: function(a, e) {
	        return MobileV3.betting.___factorial(a) / MobileV3.betting.___factorial(a - e)
	    },
	    ___C: function(a, e) {
	        return e > a ? 0 : MobileV3.betting.___P(a, e) / MobileV3.betting.___factorial(e)
	    },
	    ___H: function(a, e) {
	        return MobileV3.betting.___C(e + a - 1, a - 1)
	    },
	    ___factorial: function(a) {
	        return 1 == a || 0 == a ? 1 : a * MobileV3.betting.___factorial(a -1)
	    },
	    array_intersect: function(a) {
	        var e = {},
	            g = arguments.length,
	            c = g - 1,
	            h = "",
	            l = {},
	            m = 0,
	            q = "";
	        a: for (h in a) b: for (m = 1; m < g; m++) {
	            l = arguments[m];
	            for (q in l)
	                if (l[q] === a[h]) {
	                    m === c && (e[h] = a[h]);
	                    continue b;
	                }
	            continue a;
	        }
	        return e
	    },
	    count: function(a, e) {
	        var g, c = 0;
	        if (null === a || "undefined" ===
	            typeof a) return 0;
	        if (a.constructor !== Array && a.constructor !== Object) return 1;
	        "COUNT_RECURSIVE" === e && (e = 1);
	        1 != e && (e = 0);
	        for (g in a) a.hasOwnProperty(g) && (c++, 1 != e || !a[g] || a[g].constructor !== Array && a[g].constructor !== Object || (c += this.count(a[g], 1)));
	        return c
	    },
	    __showAndSaveBet: function(zhuShu, arr,c, h) {// 显示并记录投注信息
	    	zhuShu = zhuShu || 0;
	    	arr = arr || [];
	        c = c || [];
	        h = h || "";
	        bet_result.betCount = zhuShu;
	        var ma="";
	        for(var i=0,len=arr.length;i<len;i++){
	        	if(arr[i]){
	        		ma=ma+","+arr[i];
	        	}else{
	        		ma=ma+",-";
	        	}
	        }
	        bet_result.myarray =ma.substring(1);
	        bet_result.errorTagStr = c.toString();
	        bet_result.optional = h
	        $(".betZhushu").text(zhuShu);
	        var l = MobileV3.price , s = $("#beishuInput").val(),t = $("#lotteryBetModel a.active").data("unit");
	        if(!s){s=1;}if(!t){t = 1;}
	        l *= MobileV3.utils.__FloatMul(zhuShu,s);
	        l = MobileV3.utils.__FloatDiv(l, t);
	        $(".orderTotalCost").text(l);
	    },
	    resultBetData:function(){
	    	var l = MobileV3.price,s = $("#beishuInput").val(),t=$("#lotteryBetModel a.active").data("unit");
	    	l *= MobileV3.utils.__FloatMul(zhuShu,s);
	    	l = MObileV3.utils.__FloatDiv(l,t);
	    	
	    	return l;
	    }
	}
}();


MobileV3.utils = function(){
	return {
		rand:function(a,e){
			var g = arguments.length;
	        if (0 === g) a = 0, e = 2147483647;
	        else if (1 === g) throw Error("Warning: rand() expects exactly 2 parameters, 1 given");
	        return Math.floor(Math.random() * (e - a + 1)) + a;
		},
		__FloatMul: function(a, e) {
	    	var g = 0,
	            c = a.toString(),
	            h = e.toString();
	        try {
	            g += c.split(".")[1].length;
	        } catch (l) {}
	        try {
	            g += h.split(".")[1].length;
	        } catch (m) {}
	        return Number(c.replace(".", "")) * Number(h.replace(".", "")) / Math.pow(10, g)
	    },
	    __FloatDiv: function(a, e) {
	        var g = 0,
	            c = 0,
	            h=a.toString(), l=e.toString();
	        try {
	            g = h.split(".")[1].length;
	        } catch (m) {}
	        try {
	            c = l.split(".")[1].length;
	        } catch (q) {}
	        h = Number(h.replace(".", ""));
	        l = Number(l.replace(".",""));
	        return h / l * Math.pow(10, c - g)
	    }
	}
}();