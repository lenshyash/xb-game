var PayChange = {};
var frontLabel;
PayChange.online = function(){
	return{
		onlineSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),
				_it = paySetting.attr("pay_type"), payId = paySetting.val(),$input=$("#"+_it).find("input[type=number]"),
				amount1 =$input.val(),
				min = paySetting.attr("min"),
				max = paySetting.attr("max");
			$it.attr("disabled","disabled");
			amount1 = parseFloat(amount1) + parseFloat(rechargeNumber)
			if(!payId){
				$it.removeAttr("disabled");
				$.toast("请选择支付方式");return false;
			}
			try{
				min = parseFloat(min,10);
			}catch(e){min = 0;}
			try{
				max = parseFloat(max,10);
			}catch(e){max = 0;}
			if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
				$it.removeAttr("disabled");
				$.toast("请输入正确金额");
				return false;
			}
			amount1 = parseFloat(amount1,10);
			if(amount1<min){
				$it.removeAttr("disabled");
				$.toast("充值金额必须不小于"+min);
				return false;
			}
			if(max>0 && amount1 > max){
				$it.removeAttr("disabled");
				$.toast("充值金额必须不大于"+max);
				return false;
			}
			var iconCss = paySetting.attr("iconcss");
			var payType = paySetting.attr("pays_type");
			PayChange.online.dptcommit(amount1,payId,iconCss,payType);
			return false;
		},
		dptcommit:function(m,payId,iconCss,payType){
			topay(m, payId, iconCss, payType, function(result){
				if(result.success == false){
					$.toast(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toOnlinePayTemplate', result.data);
						$("#templateHtml").html(html);
						$.popup("#czPopup");
						$('.popup-overlay').remove();
					}else{
						$.toast("系统发生错误");
					}
				}
			});
		}
	}

}();

PayChange.fast = function(){
	return {
		fastSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),_it = paySetting.attr("pay_type"),
	        payId = paySetting.val(),$input=$("#"+_it).find("input[type=number]"),
	        amount1 =$input.val(),
	        depositor = $("#"+_it).find("#payAccountName").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max"),
			virtual=paySetting.attr("title")
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	 $it.removeAttr("disabled");
	        	$.toast("请选择支付方式");return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	if (virtual == 'virtual'){
					$.toast("请输入个数");
				}else {
					$.toast("请输入正确金额");
				}
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	if (virtual == 'virtual' ){
					$.toast("充值个数必须不小于" + min);
				}else {
					$.toast("充值金额必须不小于" + min);
				}

	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	if (virtual == 'virtual'){
					$.toast("充值个数必须不大于"+max);
				}else{
					$.toast("充值金额必须不大于"+max);
				}

	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(frontLabel && !depositor && !virtual){
	        	var name = $("#flabel").html();
	        	$.toast("请输入"+name);
	        	$it.removeAttr("disabled");
	        	return false;
	        }
	        PayChange.fast.dptcommit(amount1,depositor,payId);
	        return false;
		},
		dptcommit:function(m,ur,payId){
			$.ajax({
				url : M.base+"/m/ajax/dptcommitb.do",
				type:'post',
				dataType:'json',
				data : {
					money : m,
					depositor : ur,
					payId : payId
				},
				success : function(result) {
					if(result.fast){
						var html = template('fast_bank_desc_tmp', result);
						$("#fast_bank_desc #fast_bank_desc_html").html(html);
						$('#fast_pay_money').html(m);
						$.modal({
		               		 text:'充值申请已提交',
		               		 buttons:[{
		               			 	text: '我知道了',
		               			 	onClick:function(){
		               					$.popup("#fast_bank_desc")
		               					$('.popup-overlay').css('z-index',1);
		               			 	}
		           			 }]
		               	 })
					}
				}
			});
		}
	}
}();

PayChange.virtual = function(){
	return{
		virtualSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),
				_it = paySetting.attr("pay_type"), payId = paySetting.val(),$input=$("#"+_it).find("input[type=number]"),
				amount1 =$input.val(),
				min = paySetting.attr("min"),
				max = paySetting.attr("max");
			$it.attr("disabled","disabled");
			amount1 = parseFloat(amount1) + parseFloat(rechargeNumber)
			if(!payId){
				$it.removeAttr("disabled");
				$.toast("请选择支付方式");return false;
			}
			try{
				min = parseFloat(min,10);
			}catch(e){min = 0;}
			try{
				max = parseFloat(max,10);
			}catch(e){max = 0;}
			if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
				$it.removeAttr("disabled");
				$.toast("请输入正确金额");
				return false;
			}
			amount1 = parseFloat(amount1,10);
			if(amount1<min){
				$it.removeAttr("disabled");
				$.toast("充值金额必须不小于"+min);
				return false;
			}
			if(max>0 && amount1 > max){
				$it.removeAttr("disabled");
				$.toast("充值金额必须不大于"+max);
				return false;
			}
			var iconCss = paySetting.attr("iconcss");
			var payType = paySetting.attr("pays_type");
			PayChange.virtual.dptcommitVirtual(amount1,payId,iconCss,payType);
			return false;
		},
		dptcommitVirtual:function(m,payId,iconCss,payType){
			topayVirtual(m, payId, iconCss, payType, function(result){
				if(result.success == false){
					$.toast(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toOnlinePayTemplate', result.data);
						$("#templateHtml").html(html);
						$.popup("#czPopup");
						$('.popup-overlay').remove();
					}else{
						$.toast("系统发生错误");
					}
				}
			});
		}
	}
}();


PayChange.bank = function(){
	return {
		bankSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),bank = $from.find("input[name=radio]:checked"),_it = bank.attr("pay_type")
	         bankId =bank.val(),
	         $input=$("#"+_it).find("input[type=number]"),
	         amount1 =$input.val(),
	         depositor = $("#"+_it).find("#bankAccountName").val(),
	         min = bank.attr("min");
			 max = bank.attr("max");
	    	 $it.attr("disabled","disabled");
	    	 if(!bankId){
	    		 $it.removeAttr("disabled");
	    		 $.toast("请选择支付银行");return false;
	         }
	         try{
	             min = parseFloat(min,10);
	         }catch(e){min = 0;}
	         try{
	             max = parseFloat(max,10);
	         }catch(e){max = 0;}
	         if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	 $.toast("请输入正确金额");
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         amount1 = parseFloat(amount1,10);
	         if(min>0 && amount1<min){
	        	 $.toast("充值金额必须不小于" + min);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(max>0 && amount1>max){
	        	 $.toast("充值金额必须不大于"+max);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(!depositor) {
	        	 var name = $("#flabel").html();
		        	$.toast("请输入"+name);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        PayChange.bank.dptcommit(amount1,depositor,bankId);
	        return false;
		},
		dptcommit:function(m,ur,bankId){
			$.ajax({
				url : M.base+"/m/ajax/dptcommitc.do",
				type:'post',
				dataType:'json',
				data : {
					money : m,
					depositor : ur,
					bankId : bankId
				},
				success : function(result) {
					if(result.bank){
						var html = template('bank_desc_tmp', result);
						$("#fast_bank_desc #fast_bank_desc_html").html(html);
						$('#bank_pay_money').html(m);
						$.modal({
	               		 text:'充值申请已提交，线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！',
	               		 buttons:[{
	               			 	text: '我知道了',
	               			 	onClick:function(){
		               			 	$.popup("#fast_bank_desc")
		           					$('.popup-overlay').css('z-index',1);
	               			 	}
	           			 }]
	               	 })
					}
				}
			});
		}
	}
}();


/**
 * 重构线下充值页面
 * 公司入款onoff_show_pay_normal,一般入款onoff_show_pay_quick
 */ 
PayChange.fastAndBank = function(){
	return {
		generatePayOrder:function(quick,id,payType,_title){
			//先生成订单号
			$.ajax({
				url:M.base+'/m/ajax/generatePayOrder.do',
				data:{payQuick:quick,payId:id},
				dataType:'json',
				type:'post',
				success:function(res){
					if(res.success){
						var result = res.payList;
						result.title = _title;
						var html = template(quick,result);
						$("#"+payType).html(html);
						frontLabel = res.payList.frontLabel;
						$('#bank li').eq(1).find('.item-inner').append($('<span class="copy1" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						$('#bank li').eq(1).find('.item-inner .copy1').attr('data-clipboard-text', $('#bank li').eq(1).find('.item-inner .item-input').text())
						$('#bank li').eq(2).find('.item-inner').append($('<span class="copy" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						$('#bank li').eq(2).find('.item-inner .copy').attr('data-clipboard-text', $('#bank li').eq(2).find('.item-inner .item-input').text())
						$('#fast li').eq(0).find('.item-inner').append($('<span class="copy2" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						$('#fast li').eq(0).find('.item-inner .copy2').attr('data-clipboard-text', $('#fast li').eq(0).find('.item-inner .item-input').text())
						$('#fast li').eq(1).find('.item-inner').append($('<span class="copyid" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						$('#fast li').eq(1).find('.item-inner .copyid').attr('data-clipboard-text', $('#fast li').eq(1).find('.item-inner .item-input').text())
						// $('#virtual li').eq(0).find('.item-inner').append($('<span class="copy3" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						// $('#virtual li').eq(0).find('.item-inner .copy3').attr('data-clipboard-text', $('#virtual li').eq(0).find('.item-inner .item-input').text())
						// $('#virtual li').eq(1).find('.item-inner').append($('<span class="copy3id" style="width: 2rem;display: block;position: absolute;margin-left: -44px;text-align: center;border: 1px solid red;border-radius: 4px;">复制</span>'))
						// $('#virtual li').eq(1).find('.item-inner .copy3id').attr('data-clipboard-text', $('#virtual li').eq(1).find('.item-inner .item-input').text())
						
						var clipboard1 = new ClipboardJS('.copy1');
					    var clipboard = new ClipboardJS('.copy');
					    var clipboard2 = new ClipboardJS('.copy2');
						var clipboards = new ClipboardJS('.copyid');
						// var clipboard3 = new ClipboardJS('.copy3');
						// var clipboard3s = new ClipboardJS('.copy3id');

						//虚拟货币输入金额动态显示
						$('.virtualMoney').bind('input propertychange ',function(event){
							$('#allVirtual').text(Number($(this).val()) * Number($('#allVirtual').attr('exchangeRate')))
						})

						clipboard1.on('success', function(e) {
                            $.toast('复制成功')
					    	return false;
					    });
						clipboard1.on('error', function(e) {
					    	return false;
					    });
					    clipboard.on('success', function(e) {
                            $.toast('复制成功')
					    	return false;
					    });
					    
					    clipboard.on('error', function(e) {
					    	return false;
					    });
					    
					    clipboard2.on('success', function(e) {
                            $.toast('复制成功')
					    	return false;
					    });
					    clipboard2.on('error', function(e) {
					    	return false;
					    });
					    clipboards.on('success', function(e) {
//					    	clipboard1.destroy();
//					    	clipboard2.destroy();
//					    	clipboards.destroy();
//					    	clipboard.destroy();
                            $.toast('复制成功')
					    	return false;
					    });
					    clipboards.on('error', function(e) {
					    	return false;
					    });
						// clipboard3.on('error',function (e) {
						// 	return false;
						// })
						// clipboard3.on('success',function (e) {
						// 	$.toast('复制成功')
						// 	return false;
						// })
						//兑换跳转

						$('.buttonChangeVirtual').click(function(){
							window.open(	$('.buttonChangeVirtual').attr('data_href'))
						})
					}else{
						$.toast(res.msg);
					}
				}
			})
		},
	}
}();
