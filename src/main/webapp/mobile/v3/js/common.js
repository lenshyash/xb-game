$(function(){
	/**
	 * 首页
	 */
	$(document).on("pageInit","#page_index_jsp",function(e,pageId,$page){
		

		
	})
	
	
	
	/**
	 * 个人中心
	 */
	$(document).on("pageInit","#page_personal_center_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		if(!M.isLogin){
			$.showPreloader('请先登录');
			location.href = M.res+'/login.do';
		}
		$page.on('click',"#logoutBtn",function(){
			$.modal({
       		 text:'确定要退出吗？',
       		 buttons:[{
   			 	text: '确定',
   			 	onClick:function(){
   			 		$.ajax({
   			 			url:M.res + '/logout.do',
   			 			dataType:'json',
   			 			type:'get',
   			 			success:function(res){
   			 				window.location.href = M.res + '/login.do';
   			 			}
   			 		})
   			 	}
   			 },{text: '取消'}]
       	 })
		})
	})
	
	/**
	 * 优惠活动
	 */
	$(document).on("pageInit","#page_active_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		
		
	})
	
});
//自动转换是否开启
// function autoTrans(){
// 	$.ajax({
// 		url:'/native/getStationSwitch.do',
// 		type:'post',
// 		success:function (res) {
// 			if (res.success){
// 				for (let [index,val] of res.content.entries()){
// 					if (val.switchKey == 'autoTrans'){
// 						if (res.content[index].onFlag =='on'){
//
// 						}
// 					}
// 				}
// 			}
// 		}
// 	})
// }

//首页
var MobileIndexUtil = {
	a:1,
	b:100,
	c:null,
	d:false,
	go:function(url,type){
		$("#loading-page").css('display','flex')
		$.ajax({
			url : url,
			async : false,
			success : function(result) {
				transMoney(type,result)
				$("#loading-page").css('display','none')
				// if(!result.success){
				// 	$.toast(result.msg);
				// }
			}
		});
	},	
	toLogin:function(e){
		location.href = M.res + '/login.do';
	}
}
var autoTranIs = false
autoTrans()
function autoTrans(){
	$.ajax({
		url:'/native/getStationSwitch.do',
		type:'post',
		success:function (res) {
			if (res.success){
				for (let [index,val] of res.content.entries()){
					if (val.switchKey == 'autoTrans'){
						if (res.content[index].onFlag =='on'){
							autoTranIs = true
						}
					}
				}
			}
		}
	})
}
function transMoney(changeTo,result){
	let params = {
		changeTo:changeTo,
		changeFrom:'sys'
	}
	if(autoTranIs){
		$.ajax({
			url:'/native/fastThirdRealTransMoney.do',
			data:params,
			async:false,
			type:'post',
			success:function (res) {
				sessionStorage.setItem("autoTrans", "true");
			}
		})
	}
	if(result.url){
		location.href = result.url;
	}else{//bbin 返回的是 html
		var windowOpen = window.open(result.html);
		var new_doc = windowOpen.document.open("text/html","replace");
		new_doc.write(result.html);
		new_doc.close();
	}
}