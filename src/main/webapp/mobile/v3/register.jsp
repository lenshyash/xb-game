﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width">
    <link rel="stylesheet" href="${res }/css/login/global.css?ver=4.5" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        body.login-bg{padding-top:0px!important;}
    </style>
        <title>${_title }--注册</title>
    </head>
<body class="login-bg">
<div class="header">
    <div class="headerTop" style="text-align: center;">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="window.history.go(-1);">reveal</button>
        </div>
        <div class="center regheader">
<!--           <h2 class="ui-toolbar-title active" id="isHuiyuan">会员注册</h2> -->
<!--           <h2 class="ui-toolbar-title" id="isDaili">代理注册</h2> -->
        </div>
    </div>
</div>
<c:if test="${domainFolder eq 'd00519'}">
		<style>
			.header{
				background: rgb(23, 23, 25)!important;
			}
			.header #reveal-left, .header .reveal-left{
				background: url(../../images/blank_02.png) no-repeat!important;
			}
		</style>
		</c:if>
    <div class="login">
        <form id="register_form">
        <ul>
            <li>
                <span class="logi">用户账号</span><input type="text" id="accountno" name="uid" placeholder="用户名为5-11个英文字母或数字">
            </li>
            <li>
                <span class="logi">登录密码</span><input type="password" id="password1" name="pwd" placeholder="密码为6-16个英文字母或数字">
            </li>
            <li id="reg_tb_after">
                <span class="logi">重复密码</span><input type="password" id="password2" placeholder="请再次输入密码">
            </li>
            <li>
                <span class="logi">验&nbsp;&nbsp;证&nbsp;&nbsp;码</span>
                <input type="text" id="code" name="captcha" placeholder="请输入验证码" style="width:50%;position:position;">
<%--                 <img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" src="${base}/regVerifycode.do" onclick="reloadImg();"> --%>
				<img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" onclick="reloadImg();">
            </li>
        </ul>
        </form>
        <form id="register_daili_form" style="display: none;">
               <ul>
            <li>
                <span class="logi">用户账号</span><input type="text" class="accountno" name="uid" placeholder="用户名为5-11个英文字母或数字">
            </li>
            <li>
                <span class="logi">登录密码</span><input type="password" class="password1" name="pwd" placeholder="密码为6-16个英文字母或数字">
            </li>
            <li id="reg_tb_after">
                <span class="logi">重复密码</span><input type="password" class="password2" placeholder="请再次输入密码">
            </li>
            <li class="yzmBox">
                <span class="logi">验&nbsp;&nbsp;证&nbsp;&nbsp;码</span>
                <input type="text" class="code" name="captcha" placeholder="请输入验证码" style="width:50%;position:position;">
<%--                 <img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" src="${base}/regVerifycode.do" onclick="reloadImg();"> --%>
				<img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" onclick="dailireloadImg();">
            </li>
        </ul>
        </form>
    	         <button class="login-btn login-btn-no" onclick="regDl()" id="register_btn_dl" style="display:none;">立即注册</button>
    	         <button class="login-btn login-btn-no" id="register_btn">立即注册</button>
        <c:if test="${not empty testAccount && testAccount eq 'on'}">
        	<a href="${base }/mobile/swregpage.do" class="reg-btn" id="login_demo" style="margin-top: 12px; font-size:16px;">免费试玩</a>
        </c:if>
        <button class="reg-btn" onclick="location.href='${base}/mobile/login.do'">返回登录</button>
        <button class="reg-btn" onclick="location.href='${base}/mobile/index.do'">返回首页</button>
        <br>
        <div class="login-p">
        <c:if test="${kfSwitch eq 'on'}">
           		<a class="fl" <c:if test="${mobileOpenOutlinkType eq 'blank' }">href="javascript:void(0);" onclick="window.open('${kfUrl }');"</c:if><c:if test="${mobileOpenOutlinkType eq 'inner' }">href="${m }/customerService.do"</c:if><c:if test="${mobileOpenOutlinkType eq 'href' }">href="${kfUrl }"</c:if>>在线客服</a>
        </c:if>
        <a class="fr" href="${base }/mobile/login.do">已有帐号，直接登录</a></div>
        <div class="login-p" style="color: #fe0103;">1.用户帐号请输入5-11个英文字母或数字，不能用中文。</div>
        <div class="login-p" style="color: #fe0103;">2.登录密码请输入6-16个英文字母或数字。</div>
    </div>

    
<style>
    .center {text-align: center}
</style>

<div class="beet-odds-tips round" id="tip_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="tip_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que" style="width: 100%;" onclick="tipOk()"><span>确定</span></button>
    </div>
</div>
<style>
    .login ul li{
        display: flex;
        align-items:center;
    }
</style>
<div id="tip_bg" class="tips-bg" style="display: none;"></div>
<script src="${base }/mobile/anew/resource/new/js/jquery-2.1.4.js?ver=1.0"></script>
<script type="text/javascript" src="${base}/regconf.do"></script>
<script type="text/javascript" src="${base }/mobile/anew/resource/new/js/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<c:if test="${domainFolder eq 't00606'}">
    <script>
        $(".accountno").attr('placeholder','用户名为5-10个英文字母或数字')
        $("#accountno").attr('placeholder','用户名为5-10个英文字母或数字')
    </script>
</c:if>
<script>
    var nums = 1;
    var func;
    function tipOk() {
        $('#tip_pop').hide();
        $('#tip_bg').hide();
    }
    function msgAlert (msg,funcParm) {
        $('div#tip_pop_content').html(msg);
        $('div#tip_pop').show();
        $('div#tip_bg').show();
        func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
    function reloadImg(){
    	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
    	$("#regCode").attr("src", url);
    }
    function dailireloadImg(){
        nums += 1;
        let src = location.origin+'/native/regVerifycode.do?v=' + nums;
        $('.yzmBox #regCode').attr('src', src)
    }
    // 切换会员/代理注册
    var daili = []
    $(function(){
    	reloadImg();
    	dailireloadImg();
    	 //注册开关
   	 $.ajax({
           url:"/native/getGuestSwitch.do",
           type:"GET",
           contentType : "application/x-www-form-urlencoded",
           success : function(data) {
             // 添加代理注册配置选项
             if(data.success){
                 isOnDai = data.content.dailiRegisterOnFlag;//代理	
                 isOnReg = data.content.memberRegisterOnFlag;//注册
                 if(isOnDai == 'off'){
               	  $('.regheader').html('<h2 class="ui-toolbar-title active" id="isHuiyuan">会员注册</h2>')	
                 }else if(isOnReg == 'off'){
               	  $('.regheader').html('<h2 class="ui-toolbar-title active" id="isDaili">代理注册</h2>')
             	   	   dailireloadImg();
                     $('#register_daili_form').show().siblings('form').hide();
                     $('#register_btn_dl').show().siblings('button').hide();
                 }
                 if(isOnDai == 'on' && isOnReg == 'on'){
               	  $('.regheader').html('<h2 class="ui-toolbar-title active" id="isHuiyuan">会员注册</h2><h2 class="ui-toolbar-title" id="isDaili">代理注册</h2>')
                 }
                 $('.center .ui-toolbar-title').click(function(){
                     $(this).addClass('active').siblings().removeClass('active');
                     if($(this).text() === '会员注册'){
                     	reloadImg();
                       $('#register_form').show().siblings('form').hide();
                       $('#register_btn').show().siblings('button').hide();
                     }else{
                     	dailireloadImg();
                        $('#register_daili_form').show().siblings('form').hide();
                        $('#register_btn_dl').show().siblings('button').hide();
                     }
                   })
             }
           }
        });
     
      
     
      
     $.ajax({
        url:"/native/regconfig.do?platForm=2",
        data : "",
        type:"GET",
        xhrFields: {
          withCredentials: true
        },
        contentType : "application/x-www-form-urlencoded",
        success : function(result, textStatus, xhr) {
          // 添加代理注册配置选项
          if(result.success){
              daili = result.content;
              result.content.forEach(function(item){
              !item.remindText? item.remindText='请输入'+item.name: '';
              let html = '<li id="reg_tb_after">'+
                '<span class="logi" style="width:4rem">'+item.name+'</span><input type="text"  pattern="'+item.regex+'" class="set '+item.key+'" placeholder="'+item.remindText+'"></li>';
              $('.yzmBox').before($(html))
            })
          }
        }
     });
     
     $('.yzmBox #regCode').attr('src', '/native/regVerifycode.do?v=1');
    })
</script>
<script id="regconflst_tpl" type="text/html">
<%--{{each data as tl}}<li><span class="logi">{{tl.name}}</span>--%>
<%--{{if tl.type==1}}{{if tl.name == '推广码'}}<input type="text" placeholder="请输入{{tl.name}},没有请联系客服人员" name="{{tl.key}}" id="{{tl.key}}">--%>
<%--{{else}}<input type="text" placeholder="请输入{{tl.name}}" name="{{tl.key}}" id="{{tl.key}}">{{/if}}--%>
<%--{{/if}}{{if tl.type==6}}<input type="password" name="{{tl.key}}" id="{{tl.key}}" placeholder="请输入{{tl.name}}">{{/if}}{{$addValidateFiled tl}}</li>--%>
<%--{{/each}}--%>

{{each data as tl}}
<li>
    <span class="logi">{{tl.name}}</span>
    {{if tl.type==1}}
    <input type="text" placeholder="{{tl.remindText}}" name="{{tl.key}}" id="{{tl.key}}">
    {{else}}
    <input type="text" placeholder="请输入{{tl.name}},没有请联系客服人员" name="{{tl.key}}" id="{{tl.key}}">
    {{/if }}
</li>
{{/each}}
</script>

<script type="text/javascript">
$(function() {
	initRegConf();
})
function getLinkCode(){
	var linkCode;
	arrcookie = document.cookie.split("; ");
	$.each(arrcookie,function(index,item){
		if(item.indexOf('linkCode') >= 0){
			$("#register_btn_dl").show()
			$("#register_btn").hide()
			linkCode = item.substring(9)
			/* var tug = '<li><span class="logi">推广码</span><input type="text" placeholder="请输入推广码,没有可不填" name="promoCode" id="promoCode"></li>'
			$("#reg_tb_after").after(tug); */
			if(Request['linkCode'].length > 3){
				$("#promoCode").attr("disabled","disabled");
				$("#promoCode").val(linkCode)
			}

		}
	})
}
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
var Request = new Object();
Request = GetRequest();
var fileds = [];
var validateFiled = [];

function initRegConf() {
	for (var i = 0; i < $_regconf.length; i++) {
		var conf = $_regconf[i];
		if (conf.source && (conf.type == 2 || conf.type == 3 || conf.type == 4)) {
			conf.sourceLst = conf.source.split(",");
		}
	}
	var eachdata = {
		"data" : $_regconf
	};
	var html = template('regconflst_tpl', eachdata);
	$("#verifycode_div").before(html);
	$("#reg_tb_after").after(html);
	getLinkCode()
}

template.helper('$addValidateFiled', function(conf) {
	if (conf.requiredVal == 2 || conf.validateVal == 2) {
		validateFiled.push(conf);
	}
});

function reset() {
	$('form')[0].reset();
}

function validate() {
	for (var i = 0; i < validateFiled.length; i++) {
		var filed = validateFiled[i];
		var val = getVal(filed);
		if(filed.regex){
			filed.regex = filed.regex.replace(/\\+/g,'\\');
			var regex = new RegExp(filed.regex);
		}

		if (filed.requiredVal == 2) {
			if (!val) {
				RC.alert(filed.name + "必须输入！",filed.key);
				return false;
			}
		}

		if (filed.validateVal == 2) {
			if (!regex.test(val)) {
				RC.alert(filed.name + "格式错误！",filed.key);
				return false;
			}
		}
	}
	return true;
}

function getVal(filed) {
	var val = "";
	if (filed.type == 1 || filed.type == 2 || filed.type == 5
			|| filed.type == 6) {
		val = $("#" + filed.key).val();
	} else if (filed.type == 3) {
		val = $("input[name='" + filed.key + "']:checked").val();
	} else if (filed.type == 4) {
		var vals = [];
		$("input[name='" + filed.key + "']:checked").each(function() {
			vals.push(this.value);
		})
		val = vals.join(",");
	}
	return val;
}

function getCommitData() {
	var data = {};
	for (var i = 0; i < $_regconf.length; i++) {
		var filed = $_regconf[i];
		var val = getVal(filed);
		data[filed.key] = val;
	}
	return data;
}
</script>
<script>
    $('#register_btn').click(function(event) {
    	var account = $('form#register_form input#accountno').val().trim();
        var password1 = $('form#register_form input#password1').val().trim();
        var password2 = $('form#register_form input#password2').val().trim();
        var code = $('form#register_form input#code').val().trim();//验证码
        if(!account){
        	msgAlert("用户名不能为空!");
       	 return false;
        }
        if(!password1){
        	msgAlert("密码不能为空!");
       	 return false;
        }
        if(password1.length<6){
        	msgAlert("密码不能小于6位!");
       	 return false;
        }
        if(!password2){
        	msgAlert("确认密码不能为空!");
       	 return false;
        }
        if(password1 != password2){
         	msgAlert("两次密码不一致!");
       	 return false;
        }
        if(!code){
        	msgAlert("验证码不能为空!");
       	 return false;
        }
        
        var data = getCommitData();
        
        //可能网络原因或者手机型号原因,刷新一下
        if(!account || !password1 || !password2 || !code){
        	location.href = location.href;
        }
        
        data["account"] = account;
        data["password"] = password1;
    	data["rpassword"] = password2;
        data["verifyCode"] = code;
        
        var targetUrl = '${base}/mobile/index.do';//跳转的页面
    	var isGuest = $('#isGuest').val();//是否是试玩注册
    	var stationB = $('#stationB').val();
    	var url = '${base}/register.do';
    	if(isGuest){
    		url = '${base}/registerTestGuest.do';
    	}
      	$.ajax({
     		url:url,
     		data : "data="+JSON.encode(data),
     		type:"POST",
     		contentType : "application/x-www-form-urlencoded",
    		success : function(result, textStatus, xhr) {
    			var ceipstate = xhr.getResponseHeader("ceipstate")
    			if (!ceipstate || ceipstate == 1) {// 正常响应
    				if(!result.success){
    					msgAlert(result.msg);
    				}else{
    					msgAlert("注册成功!");
    					location.href = targetUrl;
    				}
    			} else if (ceipstate == 2) {// 后台异常
    				$('form#register_form input#code').val('');
    				msgAlert("后台异常，请联系管理员!");
    				reloadImg();
    			} else if (ceipstate == 3) { // 业务异常
    				$('form#register_form input#code').val('');
    				msgAlert(result.msg);
    				reloadImg();
    			}
    		}
     	});
    });
    function regDl(){
   	 var account = $('.accountno').val();
        var password1 = $('.password1').val();
        var password2 = $('.password2').val();
        var code = $('form#register_daili_form input.code').val().trim();

        let flag = true;

        if(!account){
       	 alert("用户名不能为空!");
       	 return false;
        }
        if(!password1){
       	 alert("密码不能为空!");
       	 return false;
        }
        if(password1.length<6){
       	 alert("密码不能小于6位!");
       	 return false;
        }
        if(!password2){
       	 alert("确认密码不能为空!");
       	 return false;
        }
        if(password1 != password2){
       	 alert("两次密码不一致!");
       	 return false;
        }
        if(!code){
       	 alert("验证码不能为空!");
       	 return false;
        }

        var isParam = $('#isParam').val();
        var data = {};
        data["account"] = account;
        data["password"] = password1;
    	  data["rpassword"] = password2;
        data["verifyCode"] = code;
        data['platForm'] = 2;

        daili.forEach(function(item){
            if (item.requiredVal == 2){
                data[item.key] = $('.'+item.key+'').val();
                let val = $('.'+item.key+'').val();
                if(!val){
                    alert(item.name+"不能为空!");
                    flag = false;
                }

            }else {
                data[item.key] = $('.'+item.key+'').val();
            }
            if (item.validateVal == 2){
                let reg = eval('/'+ $('.'+item.key+'').attr('pattern') + '/');
                let txt = $('.'+item.key+'').val().match(reg);
                item.remindText !== '' && !txt? item.remindText = item.name+ '输入不合法，请重新输入':'';
                if(txt === null){
                    alert(item.remindText);
                    flag = false;
                }
            }

        })
        if(!flag){
          return false;
        }


      	$.ajax({
        		url:"${base}/native/register.do",
        		data : data,
            xhrFields: {
              withCredentials: true
            },
        		type:"POST",
   			success : function(data, textStatus, xhr) {
          console.log(data)
   				var ceipstate = xhr.getResponseHeader("ceipstate")
   				if (ceipstate == 1) {// 正常响应
   					if(!data.success){
   						alert(data.msg);
   					}else{
   						if(data.content.reviewFlag){
   							alert("等待审核!");
   						}else{
   							location.href = '${base}/mobile/index.do';
   						}	
   					}
   					fn.success(data, textStatus, xhr);
   				} else if (ceipstate == 2) {// 后台异常
   					validCodes();
   					alert("后台异常，请联系管理员!");
   				} else if (ceipstate == 3) { // 业务异常
   					validCodes();
   					alert(data.msg);
   				} else if (ceipstate == 4) {// 未登陆异常
   					validCodes();
   					alert("登陆超时，请重新登陆");
   				} else if (ceipstate == 5) {// 没有权限
   					validCodes();
   					alert("没有权限");
   				} else if (ceipstate == 6) {// 登录异常
   					alert(data.msg);
   					top.location.href = base + "/loginError.do";
   				} else {
   					if(!data.success){
   						alert(data.msg);
   					}else{
   						alert("注册成功!");
   						parent.location.href = '${base}/daili';
   					}
   					fn.success(data, textStatus, xhr);
   				}
   			}
        	});
   }
</script>
</body>
</html>