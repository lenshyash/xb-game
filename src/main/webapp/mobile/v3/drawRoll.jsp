<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
    .drawRollBox{
        width: 100%;
        height: 300px;
        background: #ffffff;
    }
    .drawRollBox .boxTitle{
        float: left;
        width: 100%;
        height: 35px;
        text-align: center;
        line-height: 35px;
        font-size: 18px;
    }
    .drawRollBox .boxContent{
        width: 100%;
        height: 260px;
        float: left;
        overflow: hidden;
    }
    .drawRollBox .boxContent table{
        width: 100%;
        height: 30px;
    }
    .drawRollBox .boxContent table tr{
        border-bottom: 1px solid #f8f8f8;
        height: 30px;
        line-height: 30px;
    }
    .drawRollBox .boxContent table td{
        width: 33%;
        font-size: 12px;
    }
    .drawRollBox .boxContent table td:nth-child(1){
        padding-left: 20px;
    }
    .drawRollBox .boxContent table td:nth-child(2){
        text-align: center;
    }
    .drawRollBox .boxContent table td:nth-child(3){
        padding-right: 20px;
        text-align: right;
        color: red;
    }
</style>
<div class="drawRollBox">
    <div class="boxTitle">
        最新中奖
    </div>
    <div class="boxContent">
        <div id="boxboxTableTop">
            <table id="boxTableTop1"></table>
            <table id="boxTableTop2"></table>
        </div>
    </div>
</div>
<script>
    var heightRoll = 0,boxheight=0;
    $(function () {
        getDrawData()
    })
    function getDrawData(){
        $.ajax({
            async : false,
            type : "post",
            url : '${base}/native/getWinData.do' ,
            dataType : 'json',
            data: {},
            xhrFields : {
                withCredentials : true
            },
            success : function(data) {
                if(data.content.length == 0){
                    $(".drawRollBox").hide()
                    return false
                }
                var html = ''
                // data = JSON.parse('{"success":true,"content":[{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"225568","id":303,"stationId":23,"type":"幸运快三","winDate":1558368000000,"winMoney":100000.00},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99},{"account":"jisoo","id":380,"stationId":23,"type":"啊啊","winDate":1563552000000,"winMoney":111.00},{"account":"lex","id":391,"stationId":23,"type":"北京赛车","winDate":1563552000000,"winMoney":999.99}]}')
                $.each(data.content,function(index,list){
                    html += '<tr><td>'+list.account+'</td><td>'+list.type+'</td><td>'+list.winMoney+'元</td></tr>'
                })
                $("#boxTableTop1").html(html)
                $("#boxTableTop2").html(html)
                boxheight = $("#boxTableTop1").height()
                setInterval(function(){
                    heightRoll --
                    if(heightRoll < -boxheight){
                        heightRoll = 0
                    }
                    $("#boxTableTop1").css('margin-top', heightRoll+'px')
                },50);
            },
            error : function(data) {
                console.log(data)
            }
        })
    }
</script>