<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns="http:/www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="max-age=0">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=1.0">
    <meta name="language" content="zh-CN">
    <meta name="MobileOptimized" content="240">
    <title>棋牌游戏</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <meta name="screen-orientation" content="portrait">
    <meta name="x5-fullscreen" content="true">
    <meta name="x5-orientation" content="portrait">
    <link href="${base}/mobile/third/mg/css/tt.css" rel="stylesheet">
    <link href="${base}/mobile/third/mg/css/index.css" rel="stylesheet">
    <style>
        .game_logo {
            position: relative;
            display: inline-block;
            width: 120px;
            height: 140px;
            overflow: hidden;
            cursor: pointer;
        }

        .game_logo img {
            position: absolute;
            left: 0;
            width: 240px;
            height: 116px;
        }

        .game_logo img:hover {
            left: -120px;
        }

        .game_logo1 {
            position: relative;
            display: inline-block;
            width: 120px;
            height: 140px;
            overflow: hidden;
            cursor: pointer;
        }

        .game_logo1 img {
            position: absolute;
            left: 0;
            height: 116px;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="app">
    <div class="head">
        <a href="${base }/mobile/index.do" class="ico ico_home"></a>
        <h1>棋牌游戏</h1>
    </div>
    <div class="app-body">
        <div class="app-content">
            <div class="scrollable-index">
                <div class="scrollable-con">
                    <div class="game-content" id="gamelist_div"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base }/common/template/third/egame/js/h5common.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script id="gamelist_tpl" type="text/html">
    <c:forEach items="${qpGameItems }" var="data">
    <div class="game-item" style="margin-bottom: 30px">
        <c:if test="${data.playCode == 'ky'}">
            <div class="game_logo${data.playCode}" onclick="toKY_motile(${data.gameId});">
                <img src="${data.imgUrl }" style="height: 100px;width: 100px">
            </div>
        </c:if>
        <c:if test="${data.playCode == 'th'}">
            <div class="game_logo${data.playCode}" onclick="toTH_motile(${data.gameId});">
                <img src="${data.imgUrl }" style="height: 100px;width: 100px">
            </div>
        </c:if>
        <p style="bottom: -20%">${data.title}</p>
    </div>
    </c:forEach>
</script>
<script>
    $(function() {
        var eachdata = {
            "data" : hgegamedata
        };
        var html = template('gamelist_tpl', eachdata);
        $("#gamelist_div").html(html);
    });

    function toKY_motile(gameid) {
        var sw = '';
        csw = '';
        sh = '';
        csh = '';
        ctop = '';
        cleft = '';

        sw = $(window.parent).width();
        sh = $(window.parent).height();
        csw = $(window.parent).width();
        csh = $(window.parent).height();
        ctop = 0;
        cleft = 0;

        // var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
        //     + csh + ',left=' + cleft + ',top=' + ctop
        //     + ',scrollbars=no,location=1,resizable=yes');
        $.ajax({
            url : "${base}/forwardKy.do",
            sync : false,
            data : {
                gameId : gameid
            },
            success : function(json) {
                transMoney('ky',json)
                // if (json.success) {
                //     windowOpen.location.href = json.url;
                // } else {
                //     alert(json.msg);
                //     return;
                // }
            }
        });
    }
    function toTH_motile(gameid) {
        var sw = '';
        csw = '';
        sh = '';
        csh = '';
        ctop = '';
        cleft = '';

        sw = $(window.parent).width();
        sh = $(window.parent).height();
        csw = $(window.parent).width();
        csh = $(window.parent).height();
        ctop = 0;
        cleft = 0;

        // var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
        //     + csh + ',left=' + cleft + ',top=' + ctop
        //     + ',scrollbars=no,location=1,resizable=yes');
        $.ajax({
            url : "${base}/forwardTh.do",
            sync : false,
            data : {
                gameId : gameid
            },
            success : function(json) {
                transMoney('th',json)
                // if (json.success) {
                //     windowOpen.location.href = json.url;
                // } else {
                //     alert(json.msg);
                //     return;
                // }
            }
        });
    }
    // 额度自动转换
    var autoTranIs = false
    autoTrans()
    function autoTrans(){
        $.ajax({
            url:'/native/getStationSwitch.do',
            type:'post',
            success:function (res) {
                if (res.success){
                    for (let [index,val] of res.content.entries()){
                        if (val.switchKey == 'autoTrans'){
                            if (res.content[index].onFlag =='on'){
                                autoTranIs = true
                            }
                        }
                    }
                }
            }
        })
    }
    function transMoney(changeTo,json){
        let params = {
            changeTo:changeTo,
            changeFrom:'sys'
        }
        if(autoTranIs){
            $.ajax({
                url:'/native/fastThirdRealTransMoney.do',
                type:'post',
                data:params,
                async:false,
                success:function (res) {
                    sessionStorage.setItem("autoTrans", "true");
                }
            })
        }
        if (json.success) {
            location.href=json.url;
        } else {
            alert(json.msg);
            return;
        }
    }
</script>

