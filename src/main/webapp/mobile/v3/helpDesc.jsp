<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/head.jsp"></jsp:include>
  </head>
  <body>
	<div class="page" id="page_active_desc_jsp">
	  <jsp:include page="include/barTab.jsp"/>
  	  <header class="bar bar-nav">
			<a class="title">帮助中心详情</a> 
			<span class="pull-left">
				 <button class="button button-link button-nav pull-left" style="color: white;" onclick="history.go(-1)">
				    <span class="icon icon-left"></span>
				    返回
				  </button>
			</span>
	  </header>
	  <div class="content">
	    <!-- 这里是页面内容区 -->
		<c:forEach var="j" items="${hplist}">
			 ${j.content }<br/>
		</c:forEach>
	  </div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.content img').each(function(){
				$(this).css('width','100%');
			})
			$('.content table').each(function(){
				$(this).attr('width','100%');
			})
		});
	</script>
  </body>
</html>
<jsp:include page="include/need_js.jsp"/>