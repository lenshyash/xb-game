<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row no-gutter" style="margin-top: 2rem;margin-bottom: 2rem;"></div>

<nav class="bar bar-tab higher">
  <a class="tab-item <c:if test="${navMenu eq 'index'}">active</c:if>" external href="${m }">
    <span class="icon icon-home"></span>
    <span class="tab-label">投注大厅</span>
  </a>
  <c:if test="${isCpOnOff=='on'}">
	  <a external class="tab-item  <c:if test="${navMenu eq 'game_center'}">active</c:if>" href="${m }/game_center.do">
	    <span class="icon icon-app"></span>
	    <span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
	  </a>
      <c:choose>
          <c:when test="${domainFolder != 'x00279'}">
              <a external class="tab-item <c:if test="${navMenu eq 'draw_notice'}">active</c:if>" href="${m }/draw_notice.do">
                  <span class="icon icon-gift"></span>
                  <span class="tab-label">开奖公告</span>
              </a>
          </c:when>
          <c:otherwise>
          </c:otherwise>
      </c:choose>
  </c:if>
    <c:choose>
        <c:when test="${domainFolder != 'x00257'}">
            <a class="tab-item <c:if test="${navMenu eq 'interact'}">active</c:if>" external href="${m }/interact.do">
                <span class="icon icon-home"></span>
                <span class="tab-label">互动中心</span>
            </a>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
  <c:if test="${isLogin }">
  	<a external class="tab-item <c:if test="${navMenu eq 'personal_center'}">active</c:if>" href="${m }/personal_center.do">
  </c:if>
  <c:if test="${!isLogin }">
  	<a external class="tab-item <c:if test="${navMenu eq 'personal_center'}">active</c:if>" href="${m }/login.do">
  </c:if>
    <span class="icon icon-me">
    	<c:choose>
    		<c:when test="${isLogin && indexMsgCount > 0 }">
    			<span id="websiteSmsP_bm" class="badge">${indexMsgCount}</span>
    		</c:when>
    		<c:otherwise>
    			<span id="websiteSmsP_bm" class="badge" style="display:none;"></span>
    		</c:otherwise>
    	</c:choose>
    </span>
    <span class="tab-label">个人中心</span>
  </a>
</nav>

