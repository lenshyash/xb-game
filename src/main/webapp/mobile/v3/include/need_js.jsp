<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--引入js --%>
<jsp:include page="templateTpl.jsp"></jsp:include>
<script type="text/javascript" src="${res}/js/layer_mobile/layer.js" charset="utf-8"></script>
<script type="text/javascript" src="${res}/js/fastclick.js" charset="utf-8"></script>
<script type='text/javascript' src='${res}/js/touchSlide.js' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/common.js?v=6.2' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/third.js?v=1.12' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/mobile.js?v=${caipiao_version}6' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/lotterys.js?v=1' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/lottery_zhuihao.js?v=1' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/jquery.ajax.js?v=1' charset='utf-8'></script>
<script type="text/javascript" src="${res}/js/swiper-3.4.2.jquery.min.js"></script>