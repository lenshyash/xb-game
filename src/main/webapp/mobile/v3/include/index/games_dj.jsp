<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 电竞 -->
<script>
    var MobileIndexUtils = {
        a:1,
        b:100,
        c:null,
        d:false,
        go:function(url,type){
            $("#loading-page").css('display','flex')
            $.ajax({
                url : url,
                async : false,
                success : function(result) {
                    // if(!result.success){
                    //     $.toast(result.msg);
                    //     return;
                    // }
                    transMoneyDj(type,result)

                }
            });
        },
        toLogin:function(e){
            location.href = M.res + '/login.do';
        },
    }
    autoTrans()
    function autoTrans(){
        $.ajax({
            url:'/native/getStationSwitch.do',
            type:'post',
            success:function (res) {
                if (res.success){
                    for (let [index,val] of res.content.entries()){
                        if (val.switchKey == 'autoTrans'){
                            if (res.content[index].onFlag =='on'){
                                autoTranIs = true
                            }
                        }
                    }
                }
            }
        })
    }
    function transMoneyDj(changeTo,result){
        let params = {
            changeTo:changeTo,
            changeFrom:'sys'
        }
        if(autoTranIs){
            $.ajax({
                url:'/native/fastThirdRealTransMoney.do',
                type:'post',
                data:params,
                async:false,
                success:function (res) {
                    sessionStorage.setItem("autoTrans", "true");
                }
            })
        }
        if(changeTo == 'kx'){
            let url = result.content.url.substring(0,result.content.url.length-2)
            result.content.url = url + '&host=' + location.host +'#/'
        }
        if(result.content.url){
            location.href = result.content.url;
        }else{//bbin 返回的是 html
            $("#loading-page").css('display','none')
            var windowOpen = window.open(result.html);
            var new_doc = windowOpen.document.open("text/html","replace");
            new_doc.write(result.html);
            new_doc.close();
        }
    }
</script>
<c:if test="${isDjOnOff == 'on'}">
    <c:if test="${isIbcDjOnOff eq 'on'}">
        <div class="index_lottery_item">
            <a
                    <c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtils.go('${base}/native/forwardReal.do?playCode=ibc&eSport=1','ibc');"</c:if>
                    <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
                <div class="lottery_img">
                    <img src="${base}/native/resources/images/Saba_logo.png" />
                </div>
                <div class="lottery_info">
                    <div class="lottery_name">沙巴电竞</div>
                </div>
            </a>
            <div class="cl"></div>
        </div>

    </c:if>
    <c:if test="${isKxOnOff eq 'on'}">
        <div class="index_lottery_item">
            <a
                    <c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtils.go('${base}/native/forwardReal.do?playCode=kx','kx');"</c:if>
                    <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
                <div class="lottery_img">
                    <img src="${base}/native/resources/images/KXDJ.png" />
                </div>
                <div class="lottery_info">
                    <div class="lottery_name">凯旋电竞</div>
                </div>
            </a>
            <div class="cl"></div>
        </div>

    </c:if>
</c:if>
<!-- <div class="index_lottery_item" id="kxdj" style="display: none"> -->
<!--     <a -->
<%--             <c:if test="${isLogin}">href="javascript:void(0);" onclick="testLogin();"</c:if> --%>
<%--             <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external"> --%>
<!--         <div class="lottery_img"> -->
<%--             <img src="${base}/native/resources/images/Saba_logo.png" /> --%>
<!--         </div> -->
<!--         <div class="lottery_info"> -->
<!--             <div class="lottery_name">开心电竞</div> -->
<!--         </div> -->
<!--     </a> -->
<!--     <div class="cl"></div> -->
<!-- </div> -->
<script>
    $(function () {
        if(location.host == 'www.xb336.com' ||location.host == 'xb336.com'){
            $("#kxdj").show()
        }
    })
    function testLogin(){
        var data = {
            userID: "${loginMember.account}",
            secretkey: "0f34ec7808d54ccfbc006548a55b3881"
        }
        $.ajax({
            url:"https://webapi.xinbocloud.com/api/Merchant/Login",
            data:JSON.stringify(data),
            headers:{'Content-Type':'application/json;charset=utf8'},
            xhrFields : {
                withCredentials : true
            },
            dataType:"json",
            type:"post",
            success:function(j){
                console.log(j)
                if(j.resultCode == 6){
                    djRegister()
                } else if(j.resultCode == 1){
                    location.href = 'https://api.xinbocloud.com?userName=${loginMember.account}&token=' + j.resultData.accessToken + '#/'
                }
            }
        });
    }
    function djRegister(){
        var data = {
            userID: "${loginMember.account}",
            secretkey: "0f34ec7808d54ccfbc006548a55b3881"
        }
        $.ajax({
            url:"https://webapi.xinbocloud.com/api/Merchant/Register",
            data:JSON.stringify(data),
            headers:{'Content-Type':'application/json;charset=utf8'},
            xhrFields : {
                withCredentials : true
            },
            dataType:"json",
            type:"post",
            success:function(j){
                if(j.resultData){
                    testLogin()
                }
            }
        });
    }
</script>