<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="chessLists"></div>
<c:if test="${isKyOnOff eq 'on' && isGuest != true}">
		<div class="index_lottery_item">
			<a href="${base}/mobile/v3/getQpGameItems.do?playCode=ky" class="external">
				<div class="lottery_img">
					<img src="${base }/common/template/third/chess/images/game0.png" />
				</div>
				<div class="lottery_info">
					<div class="lottery_name">开元棋牌</div>
				</div>
			</a>
		<div class="cl"></div>
		</div>
</c:if>
<c:if test="${isThOnOff  eq 'on'}">
	<div class="index_lottery_item">
		<a href="${base}/mobile/v3/getQpGameItems.do?playCode=th" class="external">
			<div class="lottery_img">
				<img src="${base }/native/resources/images/chess/thlogo.png" />
			</div>
			<c:if test="${domainFolder eq 'b01202'}">
				<div class="lottery_info">
					<div class="lottery_name">天豪棋牌与捕鱼</div>
				</div>
				<script>
					$(function(){
						$('.lottery_info').last().remove()
					})
				</script>
			</c:if>
			<div class="lottery_info">
				<div class="lottery_name">天豪棋牌</div>
			</div>
		</a>
		<div class="cl"></div>
	</div>
</c:if>
	