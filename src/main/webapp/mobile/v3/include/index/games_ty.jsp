<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 体育 -->
<input type="hidden" id="mUrl" value="${m }">
<c:if test="${isTyOnOff=='on' && isGuest != true}">
	<c:if test="${isHgSysOnOff ne 'off'}">
		<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${m }/sport/hg/index.do"</c:if>
			<c:if test="${!isLogin}">href="${m }/login.do"</c:if> class="external" title="皇冠体育">
			<div class="lottery_img">
				<img src="${res }/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<c:choose>
					<c:when test="${domainFolder == 'b10801'}">
						<div class="lottery_name">体育竞技</div>
					</c:when>
					<c:otherwise>
						<div class="lottery_name">皇冠体育</div>
					</c:otherwise>
				</c:choose>

			</div>
			</a>
			<div class="cl"></div>
		</div>
	</c:if>
</c:if>
<c:if test="${isM8OnOff eq 'on' && isGuest != true}">
		<div class="index_lottery_item">
		 <a 
		 <c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardM8.do?h5=1', 'M8');"</c:if>
		 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external" title="M8体育">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">M8体育</div>
			</div>
		</a>
		<div class="cl"></div>
		</div>
	</c:if>
	<c:if test="${isM8HOnOff eq 'on' && isGuest != true}">
		<div class="index_lottery_item">
		 <a 
		 <c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardM8H.do?h5=1', 'M8H');"</c:if>
		 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external" title="M8体育(皇冠版)">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">M8体育(皇冠版)</div>
			</div>
		</a>
		<div class="cl"></div>
		</div>
	</c:if>
	<c:if test="${isIbcOnOff eq 'on' && isGuest != true}">
		<div class="index_lottery_item">
		 <a 
		 <c:if test="${isLogin}">href="javascript:void(0);" onclick="shbbedzh();"</c:if> 
		 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external" title="沙巴体育">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/Saba_logo.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">沙巴体育</div>
			</div>
		</a>
		<script>
			function shbbedzh(){
				<%--if (confirm("沙巴体育需要转入额度后才能下注，点击【确定】进行额度转换，点击【取消】则直接进入沙巴体育!")) {--%>
				<%--	location.href = $("#mUrl").val() + '/live_game.do'--%>
			    <%--} else { --%>
			    <%--	MobileIndexUtil.go('${base}/forwardIbc.do?h5=1', '10', this);--%>
			    <%--}--%>
				MobileIndexUtil.go('${base}/forwardIbc.do?h5=1', 'ibc');
			}
		</script>
		<div class="cl"></div>
		</div>
		<style>
			#sb_message{
				display:block!important;
			}
		</style>
		</c:if>
		<c:if test="${isBBTYOnOff == 'on'}">
		<div class="index_lottery_item">
		 <a href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardBbin.do?type=ball','bbin')"  class="external" title="BB体育">
			<div class="lottery_img">
				<img src="${base }/common/template/sports/hg/images/sports/bb_sports.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">BB体育</div>
			</div>
		</a>
		<div class="cl"></div>
		<style>
			#sb_message{
				display:block!important;
			}
		</style>
		</div>
	</c:if>   
	<div id="loading-page">
		<img src="${base }/mobile/v3/images/loading-1.gif" style="height:37px;" />
	</div>
	<style>
		#loading-page{
			position: fixed;left:0;right:0;top:0;bottom:0;background:#000;z-index:999;opacity: 0.4;display:none;justify-content: center;align-items: center;
		}
	</style>
	<jsp:include page="/common/template/third/page/live_demo.jsp" />