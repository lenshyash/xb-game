<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabBox1" class="tabBox">
	<div class="hd">
		<h3><a href="#">新闻</a><span>News</span></h3>
		<ul>
			<li class="on"><a href="javascript:void(0)">国内</a></li>
			<li class=""><a href="javascript:void(0)">国际</a></li>
			<li class=""><a href="javascript:void(0)">时事</a></li>
		</ul>
	</div>
	<div class="tempWrap" style="overflow: hidden; position: relative; height: 284px; transition: 200ms;"><div class="bd" id="tabBox1-bd" style="width: 1125px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 200ms; transform: translate(0px, 0px) translateZ(0px);"><!-- 添加id，js用到 -->
		<div class="con" style="display: table-cell; vertical-align: top; width: 375px;"><!-- 高度自适应需添加外层 -->
			<ul>
				<li><a href="#">官方明确感染H7N9高危人群</a></li>
				<li><a href="#">台官员:将驱离进入钓岛大陆船</a></li>
				<li><a href="#">陈云长子吁学江泽民思想方法</a></li>
				<li><a href="#">央视批抗日剧目:比着俗争着二</a></li>
				<li><a href="#">网友:离3次结3次可避税国五条</a></li>
				<li><a href="#">足疗女子自称东北人引发血案</a></li>
				<li><a href="#">男性神秘“根浴”会所被调查</a></li>
				<li><a href="#">女孩一年吃掉48公里长方便面</a></li>
			</ul>
		</div>
		<div class="con" style="display: table-cell; vertical-align: top; width: 375px;"><!-- 高度自适应需添加外层 -->
			<ul>
				<li><a href="#">日:沈阳军区部队开赴中朝边境</a></li>
				<li><a href="#">印度核潜艇经我周边未被发现</a></li>
				<li><a href="#">中国航母战斗群重防空轻反潜</a></li>
				<li><a href="#">日本军车爆车胎造成一死一伤</a></li>
			</ul>
		</div>
		<div class="con" style="display: table-cell; vertical-align: top; width: 375px;"><!-- 高度自适应需添加外层 -->
			<ul>
				<li><a href="#">农业占GDP低政府支持力度大</a></li>
				<li><a href="#">林志玲:至今珍藏本山送的手绢</a></li>
				<li><a href="#">五粮液高管年薪超茅台近一倍</a></li>
				<li><a href="#">视频:朝鲜导弹反复出舱</a></li>
				<li><a href="#">《我是歌手》明晚决赛开战</a></li>
				<li><a href="#">皖新传媒一年花13亿委托理财</a></li>
			</ul>
		</div>

	</div></div>
</div>
<script>

TouchSlide( { slideCell:"#tabBox1",
	endFun:function(i){ //高度自适应
		var bd = document.getElementById("tabBox1-bd");
		bd.parentNode.style.height = bd.children[i].children[0].offsetHeight+"px";
		if(i>0)bd.parentNode.style.transition="200ms";//添加动画效果
	}
} );
</script>