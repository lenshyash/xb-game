<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 真人 -->
<c:if test="${isZrOnOff=='on' && isGuest != true}">
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardAg.do?h5=1&amp;gameType=11');"</c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/agreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardMg.do?gameType=1&amp;gameid=66936&amp;h5=1');"</c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/mgreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardBbin.do?type=live');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/bbinreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BBIN真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<%-- <c:if test="${isVrOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardVr.do');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/vr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">VR游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if> --%>
	<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardBg.do?type=2&h5=1');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/bg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isAbOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardAb.do');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/ab.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AB真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isOgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardOg.do?gametype=mobile');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ogzr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">OG真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	
	<c:if test="${isDsOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardDs.do?h5=1');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/dszr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">DS真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	</c:if>
	
	<c:if test="${isDzOnOff eq 'on' }">
	<c:if test="${(isAgDzOnOff eq 'on' && isGuest != true) ||(empty isAgDzOnOff && isAgOnOff eq 'on' && isGuest != true)}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardAg.do?h5=0&amp;gameType=6');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/agreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG电子</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${(isMgDzOnOff eq 'on' && isGuest != true) ||(empty isMgDzOnOff && isMgOnOff eq 'on' && isGuest != true)}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${m }/third/index.do?code=mg"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/mg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG电子</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
		<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=pt"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/pt.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">PT电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isQtOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=qt"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/qt.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">QT电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isIsbOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=isb"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/isb.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">ISB电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isCq9OnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=cq9"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/cq9.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">CQ9电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isJdbOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=jdb"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/jdb.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">JDB电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isTtgOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=ttg"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/ttg.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">TTG电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isMwOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${m }/third/index.do?code=mw"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${res}/images/real/mw.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">MW电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isByOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=by" </c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();" </c:if> class="external">
				<div class="lottery_img">
					<img src="${res}/images/real/by.png" />
				</div>
				<div class="lottery_info">
					<div class="lottery_name">捕鱼专题</div>
				</div>
			</a>
			<div class="cl"></div>
			</div>

		</c:if>
		<%-- <c:if test="${isKyOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardKy.do?h5=1');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${res}/images/real/ky.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">KY棋牌</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if> --%>
	</c:if>
