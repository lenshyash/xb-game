<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<c:if test="${isActive }">
		<div class="row no-gutter contact">
			<dl style="width: 33.33%">
				<a class="external share" href="javascript:void(0)" target="_blank">
					<dt>
						<i class="icon icon-share"></i>
					</dt>
					<dd>分享</dd>
				</a>
			</dl>
			<dl style="width: 33.33%">
				<a class="external" href="${base}/?toPC=1">
					<dt>
						<i class="icon icon-computer"></i>
					</dt>
					<dd>电脑版</dd>
				</a>
			</dl>
			<dl style="width: 33.33%">
				<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
					<dt>
						<i class="icon icon-refresh"></i>
					</dt>
					<dd>刷新</dd>
				</a>
			</dl>
		</div>
	</c:if>
	<c:if test="${!isActive }">
		<div class="row no-gutter contact">
			<dl style="width: 25%">
				<a class="external share" href="javascript:void(0)" target="_blank">
					<dt>
						<i class="icon icon-share"></i>
					</dt>
					<dd>分享</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="${base}/?toPC=1">
					<dt>
						<i class="icon icon-computer"></i>
					</dt>
					<dd>电脑版</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="${m}/active.do">
					<dt>
						<i class="icon icon-gift"></i>
					</dt>
					<dd>优惠活动</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
					<dt>
						<i class="icon icon-refresh"></i>
					</dt>
					<dd>刷新</dd>
				</a>
			</dl>
		</div>
	</c:if>
	<style>
		@charset "utf-8";
/* CSS Document */
/*
弹出插件 AND 分享插件
autho：smohan
http://www.smohan.net
*/

div.demo{ width:600px; height:320px; line-height:20px; padding-top:20px; border-top:2px dashed #cecece; margin:30px auto; }



/*Layer*/
.Smohan_Layer_Shade{position:fixed; _position:absolute; zoom:1; top:0; left:0; width:100%; height:100%; overflow:hidden; background:url(images/hei.png) repeat; z-index:99900;}
.Smohan_Layer_box{ min-width:300px; height:auto; padding:20px; background:#ffffff; position:fixed; _position:absolute; top:40%; left:50%; overflow:hidden; z-index:99999;-moz-border-radius:20px;-webkit-border-radius:20px;
border-radius:20px;}
.Smohan_Layer_box h3{ display:block; width:100%; height:30px; line-height:30px; padding:0px 0px 10px 0px; margin:0; border-bottom:1px solid #cccccc; overflow:hidden;}
.Smohan_Layer_box h3 .text{ float:left;font-size:16px; font-family:'Microsoft JhengHei','Microsoft YaHei', Arial, sans-serif; font-weight:600; color:red; text-indent:0.5em; display:block;}
.Smohan_Layer_box h3 .close{ display:block; width:30px; height:30px; background:url(images/Smohan.layer.close.png) 0px 0px no-repeat; float:right;}
.Smohan_Layer_box h3 .close:hover{background-position:0px -31px;}
.Smohan_Layer_box .layer_content{ display:block; width:100%; height:100%; margin-top:10px; margin-bottom:10px; padding:0; overflow:hidden; position:relative;}
.Smohan_Layer_box .layer_content .loading{ display:block; width:36px; height:36px; background:url(../images/Load.gif) center center no-repeat; position:absolute; top:0; left:0;}
/*Share*/
#Share{ display:none; width:360px; height:120px; padding:15px 10px;}
#Share ul{ margin:0; padding:0; list-style-type:none;}
#Share ul li{ float:left; display:block; width:50px; height:50px;margin-left:5px; margin-right:5px; cursor:pointer; position:relative;}
#Share ul li a{ display:block; width:28px; height:28px; margin-left:11px; margin-top:12px;}
#Share ul li span{ display:block;width:40px;height:10px;background:url(images/share_shade.png) 0px 0px no-repeat; position:absolute; left:5px; bottom:0px;}
#Share ul li a.share1{ background:url(images/share_icon2.png) 0px 0px;}
#Share ul li a.share2{ background:url(images/share_icon2.png) -28px 0px;}
#Share ul li a.share3{ background:url(images/share_icon2.png) -56px 0px;}
#Share ul li a.share4{ background:url(images/share_icon2.png) -84px 0px;}
#Share ul li a.share5{ background:url(images/share_icon2.png) -112px 0px;}
#Share ul li a.share6{ background:url(images/share_icon2.png) -140px 0px;}
/*微信二维码*/
.none{
	display: none;
}
.qrcode{
	width:180px;
	height:240px;
	padding:20px;
	position: fixed;
	left:50%;
	top:50%;
	margin-left:-120px;
	margin-top:-140px;
	background: #fff;
	z-index: 100000;
}
.qrcode img{
	max-width: 100%;
	max-height:100%;
}
.weixin_close{
	background: #7CBDD0;
	display: inline-block;
	width:100%;
	height:30px;
	line-height: 30px;
	text-align: center;
    color: #fff;
    text-decoration: none
}
	</style>
	<script>
	/*
	弹出插件 AND 分享插件
	autho：smohan
	http://www.smohan.net
	*/

	//这是弹出层，IE9以下无法圆角
	;(function($){$.fn.SmohanPopLayer=function(options){var Config={Shade:true,Event:"click",Content:"Content",Title:"Smohan.net"};var options=$.extend(Config,options);var layer_width=$('#'+options.Content).outerWidth(true);var layer_height=$('#'+options.Content).outerHeight(true)
	var layer_top=(layer_height+40)/2;var layer_left=(layer_width+40)/2;var load_left=(layer_width-36)/2;var load_top=(layer_height-100)/2;var layerhtml="";if(options.Shade==true){layerhtml+='<div class="Smohan_Layer_Shade" style="display:none;"></div>';}
	layerhtml+='<div class="Smohan_Layer_box" style="width:100%;height:140px; margin-top:-'+layer_top+'px;margin-left:-'+layer_left+'px;display:none;" id="layer_'+options.Content+'">';layerhtml+='<h3><b class="text">'+options.Title+'</b><a href="javascript:void(0)" class=" external close"></a></h3>';layerhtml+='<div class="layer_content">';layerhtml+='<div class="loading" style="left:'+load_left+'px;top:'+load_top+'px;"></div>';layerhtml+='<div id="'+options.Content+'" style="display:block;">'+$("#"+options.Content).html()+'</div>';layerhtml+='</div>';layerhtml+='</div>';$('body').prepend(layerhtml);if(options.Event=="unload"){$('#layer_'+options.Content).animate({opacity:'show',marginTop:'-'+layer_top+'px'},"slow",function(){$('.Smohan_Layer_Shade').show();$('.Smohan_Layer_box .loading').hide();});}else{$(this).on(options.Event,function(e){$('#layer_'+options.Content).animate({opacity:'show',marginTop:'-'+layer_top+'px'},"slow",function(){$('.Smohan_Layer_Shade').show();$('.Smohan_Layer_box .loading').hide();});});}
	$('.Smohan_Layer_box .close').click(function(e){$('.Smohan_Layer_box').animate({opacity:'hide',marginTop:'-300px'},"slow",function(){$('.Smohan_Layer_Shade').hide();$('.Smohan_Layer_box .loading').show();});});};})(jQuery);

	//分享	
	$(document).ready(function(e) {
		var share_html = "";
		//share_html += '<a href="javascript:void(0)" id="smohan_share" title="分享"></a>';
		share_html += '<div id="Share"><ul>';
		share_html += '<li title="分享到QQ空间"><a href="javascript:void(0)" class="share1 external"></a><span></span></li>';
		/*share_html += '<li title="分享到微信"><a href="javascript:void(0)" class="share6 external"></a><span></span></li>';*/
	  share_html += '<li title="分享到新浪微博"><a href="javascript:void(0)" class="share2 external"></a><span></span></li>';
		share_html += '<li title="分享到人人网"><a href="javascript:void(0)" class="share3 external" ></a><span></span></li>';
		share_html += '<li title="分享到朋友网"><a href="javascript:void(0)" class="share4 external"></a><span></span></li>';
 		share_html += '<li title="分享到腾讯微博"><a href="javascript:void(0)" class="share5"></a><span></span></li>';
 		share_html += '</ul></div>';
		$('body').prepend(share_html);
	  (!!$(".qrcode").length || $('body').append($('<div id="qrcode" class="qrcode none"></div>')))
	    


	    /*调用方法 start*/

	    $('.share').SmohanPopLayer({Shade : true,Event:'click',Content : 'Share', Title : '分享${_title}到各大社区'});
	    
	    /*调用方法 end*/


	    $('#Share li').each(function() {
	    $(this).hover(function(e) {
		  $(this).find('a').animate({ marginTop: 2}, 'easeInOutExpo');
		  $(this).find('span').animate({opacity:0.2},'easeInOutExpo');
		 },function(){
		  $(this).find('a').animate({ marginTop: 12}, 'easeInOutExpo');
		  $(this).find('span').animate({opacity:1},'easeInOutExpo');
	   });
	});
	var share_url = encodeURIComponent(location.href);
	var share_title = encodeURIComponent(document.title);
	var share_pic = "http://www.jq22.com/img/cs/500x300b.png";  //默认的分享图片
	var share_from = encodeURIComponent("jQuery插件库"); //分享自（仅用于QQ空间和朋友网，新浪的只需更改appkey 和 ralateUid就行）
	//Qzone
	$('#Share li a.share1').click(function(e) {
	    window.open("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url="+share_url+"&title="+share_title+"&pics="+share_pic+"&site="+share_from+"","newwindow");
	});	  
	//Sina Weibo
	$('#Share li a.share2').click(function(e) {
	var param = {
	    url:share_url ,
	    appkey:'678438995',
	    title:share_title,
	    pic:share_pic,
	    ralateUid:'3061825921',
	    rnd:new Date().valueOf()
	  }
	  var temp = [];
	  for( var p in param ){
	    temp.push(p + '=' + encodeURIComponent( param[p] || '' ) )
	  }
	window.open('http://v.t.sina.com.cn/share/share.php?' + temp.join('&'));	
	});
	//renren
	$('#Share li a.share3').click(function(e) {
	window.open('http://widget.renren.com/dialog/share?resourceUrl='+share_url+'&title='+share_title+'&images='+share_pic+'','newwindow');
	});
	//pengyou
	$('#Share li a.share4').click(function(e) {
	window.open('http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?to=pengyou&url='+share_url+'&pics='+share_pic+'&title='+share_title+'&site='+share_from+'','newwindow');
	});
	//tq
	$('#Share li a.share5').click(function(e) {
	window.open('http://share.v.t.qq.com/index.php?c=share&a=index&title='+share_title+'&site='+share_from+'&pic='+share_pic+'&url='+share_url+'','newwindow');
	});
	//kaixin
	$('#Share li a.share6').click(function(e) {
	    if(!$('#qrcode img').length){
	      var qrcode = new QRCode(document.getElementById("qrcode"), {
	        text: location.href,
	        width: 180,
	        height: 180,
	        colorDark : "#7CBDD0",
	        colorLight : "#ffffff",
	        correctLevel : QRCode.CorrectLevel.H
	      });
	    }
	    
	    !!$('.qrcode_msg').length || $("#qrcode").append('<p class="t_c qrcode_msg">点击右上角【...】开始分享</p><a class="weixin_close" href="javascript:;">暂不分享</a>')
	    $('.Smohan_Layer_box').animate({
	      'margin-top' : '-200px',
	    }).fadeOut(300)
	    $("#qrcode").fadeIn(0);
	}); 
	});
	$(document).delegate(".weixin_close",'click',function(){
	    $('.qrcode,.Smohan_Layer_Shade').fadeOut(300);

	})

	</script>
	