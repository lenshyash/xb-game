<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 侧边栏菜单 --%>
<div class="panel panel-left panel-reveal theme-dark" id='panel-left-demo'>

<header class="bar bar-nav" style="background-color: #ec2829;">
  <h1 class="title">个人中心</h1>
</header>
<div class="content leftNavHelp">
  <c:if test="${isLogin }">
  <div class="list-block media-list" style="margin: 0 0 1.75rem 0;">
    <ul>
      <li>
        <div class="item-content">
          <div class="item-media">
              <c:choose>
                  <c:when test="${domainFolder == 'x00246'}">
                      <img src="${res }/images/image_2020_10_09T06_50_49_618Z.png" style="width: 2.2rem;">
                  </c:when>
                  <c:otherwise>
                      <img src="${base }/mobile/v3/images/touxiang.png" style="width: 2.2rem;">
                  </c:otherwise>
              </c:choose>
          </div>
          <div class="item-inner">
            <div class="item-title-row">
              <div class="item-title">${loginMember.account}</div>
            </div>
            <div class="item-subtitle">
            	${loginMember.money}
            	<c:if test="${not empty cashName}">
               		${cashName}
	            </c:if>
				<c:if test="${empty cashName}">
                  	元 
                </c:if>
	        </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  </c:if>
  <div class="content-block-title" style="font-size:1rem;"><span class="icon icon-app"></span> 常见</div>
  <div class="list-block">
    <ul>
      <li class="item-content item-link" onclick="location.href = '${m }/active.do'">
        <div class="item-media"><span class="icon icon-gift"></span></div>
        <div class="item-inner">
          <div class="item-title">最新优惠</div>
          <div class="item-after">GO</div>
        </div>
      </li>
       <!--
      <li class="item-content item-link" onclick="location.href = '${m }/active.do'">
        <div class="item-media"><i class="iconfont">&#xe600;</i></div>
        <div class="item-inner">
          <div class="item-title">最新资讯</div>
          <div class="item-after">GO</div>
        </div>
      </li>
     -->
      <li class="item-content item-link" onclick="location.href = '${m }/rookieHelp.do'">
        <div class="item-media"><i class="iconfont">&#xe62e;</i></div>
        <div class="item-inner">
          <div class="item-title">帮助中心</div>
          <div class="item-after">GO</div>
        </div>
      </li>
      
    </ul>
  </div>
  
  <div class="content-block-title" style="font-size:1rem;"><span class="icon icon-settings"></span> 设置</div>
  <div class="list-block">
    <ul>
      <li class="item-content">
        <div class="item-media"><i class="iconfont icon-gliconledou"></i></div>
        <div class="item-inner">
          <div class="item-title">红包</div>
          <div class="item-after">
				<div class="item-input">
	              <label class="label-switch">
	                <input type="checkbox" id="setRedBag" checked="checked">
	                <div class="checkbox"></div>
	              </label>
	            </div>
		  </div>
        </div>
      </li>
      <li class="item-content">
        <div class="item-media"><span class="icon icon-computer"></span></div>
        <div class="item-inner">
          <div class="item-title">首页弹窗</div>
          <div class="item-after">
				<div class="item-input">
	              <label class="label-switch">
	                <input type="checkbox" id="setWindowFrame" checked="checked">
	                <div class="checkbox"></div>
	              </label>
	            </div>
		  </div>
        </div>
      </li>
      <!--  
      <li class="item-content">
        <div class="item-media"><i class="iconfont icon-laba"></i></div>
        <div class="item-inner">
          <div class="item-title">点击音效</div>
          <div class="item-after">
				<div class="item-input">
	              <label class="label-switch">
	                <input type="checkbox" id="setVoice" checked="checked">
	                <div class="checkbox"></div>
	              </label>
	            </div>
		  </div>
        </div>
      </li>
      -->
      <li class="item-content">
        <div class="item-media"><i class="iconfont icon-xuanzhong"></i></div>
        <div class="item-inner">
          <div class="item-title">动画特效</div>
          <div class="item-after">
				<div class="item-input">
	              <label class="label-switch">
	                <input type="checkbox" id="setTeXiao" checked="checked">
	                <div class="checkbox"></div>
	              </label>
	            </div>
		  </div>
        </div>
      </li>
    </ul>
  </div>
  
  <div class="list-block close-panel">
    <ul>
      <li class="item-content">
        <div class="item-media"><i class="icon icon-emoji"></i></div>
        <div class="item-inner">
          <div class="item-title">关闭</div>
        </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
$('.leftNavHelp input[type="checkbox"]').each(function(){
	initAllSetting(this.id);
	$(this).change(function(){
		changeCheck(this.id,$(this).is(':checked'));
	});
});

//初始化
function initAllSetting(id){
	if(!localStorage.getItem(id)){
		$('#'+id).attr('checked',true);
	}else{
		$('#'+id).attr('checked',false);
	}
}
 
function changeCheck(id,chcek){
	if(chcek){
		localStorage.removeItem(id);
		$('#'+id).attr('checked',true);
	}else{
		localStorage.setItem(id,true);
		$('#'+id).attr('checked',false);
	}
	initSwitch(id);
}

var cacheItem = new Array('setRedBag','setWindowFrame','setTeXiao');//'setVoice'
$(function(){
	for(var i in cacheItem){
		initSwitch(cacheItem[i]);
	}
});
	
function initSwitch(p){
	switch(p){
		case 'setRedBag': 
			//红包
			if(localStorage.getItem(p)){
				$('.red-bag-float').hide();
			}else{
				$('.red-bag-float').show();
			}
			break;
		case 'setWindowFrame': 
			//弹窗
			/* noticeAlert(); */
			break;
		case 'setVoice': 
			if(!localStorage.getItem(p)){
				$('body').click(function(){
					var beepOne = $("#clickVoice")[0];
					beepOne.play();
				});
			}else{
				$('body').unbind();
			}
			break;
		case 'setTeXiao': 
			if(!localStorage.getItem(p)){
				$('head').append('<link rel="stylesheet" href="${res}/css/animate.min.css?v=1.0">');
			}
			break;
	}
}
</script>
</div>
<audio id="clickVoice" controls="controls" preload="auto" style="display: none;">
	<source src="${res }/js/map/beep.mp3" controls></source>
	<source src="${res }/js/map/beep.ogg" controls></source>
	Your browser isn't invited for super fun audio time.
</audio>