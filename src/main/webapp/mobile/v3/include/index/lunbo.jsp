<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type='text/javascript' src='${res }/js/touchSlide.js' charset='utf-8'></script>
<div id="focus" class="row no-gutter focus animated flipInX">
	<div class="hd">
		<ul></ul>
	</div>
	<div class="bd">
		<ul>
			<c:forEach items="${agentLunBos }" var="item" varStatus="status">
				<c:if test="${item.titleUrl != ''}">
					<li onclick="window.open('${ item.titleUrl}');"><a href="javascript:void(0);"></a><img src="${item.titleImg}"/></a></li>
				</c:if>
				<c:if test="${item.titleUrl == ''}">
					<li><a href="javascript:void(0);"></a><img src="${item.titleImg}"/></a></li>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div>
<script type="text/javascript">
$(function(){
	TouchSlide({ 
		slideCell:"#focus",
		titCell:".hd ul",
		mainCell:".bd ul", 
		interTime: 5000,
		effect:"leftLoop", 
		autoPlay:true,//自动播放
		autoPage:true //自动分页
	});
});
</script>