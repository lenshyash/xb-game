<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<input type="hidden" value="${base }" id="baseText">
	<input type="hidden" value="${onOffLotterySort }" id="onOffLotterySort">
	<input type="hidden" value="${isVrOnOff }" id="isVrOnOff">
	<input type="hidden" value="${isGuest }" id="isGuest">
	
<c:if test="${onOffLotterySort == 'off' || empty onOffLotterySort}">
	<c:if test="${isCpOnOff=='on'}">
			<c:forEach items="${bcLotterys }" var="item" varStatus="status">
				<div class="index_lottery_item"  style="position: relative;">
				<c:choose>
				   <c:when test="${isLogin}">  
				        <a href="${station }/v3/bet_lotterys.do?lotCode=${item.code }" class="external">
				   </c:when>

				   <c:otherwise> 
				      	<a href="${base }/mobile/v3/login.do" class="external">
				   </c:otherwise>
				</c:choose>
					   <c:if test="${not empty item.gifUrl }">
						   <img  src="${item.gifUrl}" style="position: absolute;z-index:20;width: 4.8rem;right: 0;top: 0;"/>
					   </c:if>
					   <div class="lottery_img">
							<c:if test="${empty  item.imgUrl}">
							<img src="${res }/images/lottery/${item.code}.png" />
							</c:if>
							<c:if test="${not empty item.imgUrl}">
							<img src="${item.imgUrl }" />
							</c:if>
						</div>
						<div class="lottery_info">
							<div class="lottery_name">${item.name}</div>
						</div>
					</a>
					<div class="cl"></div>
				</div>
			</c:forEach>
			<!--  -->
	</c:if>
	<c:if test="${isVrOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardVr.do');" href="javascript:void(0);"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
					<div class="lottery_img">
						<img src="${base }/mobile/anew/resource/new/images/vr.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">VR娱乐游戏</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
	</c:if>
</c:if>
<c:if test="${onOffLotterySort=='on'}">
<c:if test="${isCpOnOff=='on'}">
<div id='fenlei'></div>
	<script>
 	$(function(){
 		var base = $('#baseText').val();
 		var $base = $('#baseText').val() + '/mobile/v3';
 		var lotteryLists;
 		$.ajax({
 			url:"${base}/native/getLoctterys.do",
 			dataType:"JSON",
 			type:"POST",
 			success:function(res){
 				if(res){
 					lotteryLists = res.content;
 					caizhongFl();
 				}
 			}
 		});
 		function caizhongFl(){
 			var html = ''; let judegImgUrl =false
 			var lst = {
 					"1":{"lotGroup":"1","name":"时时彩","imgCode":"FFC"},
 					"2":{"lotGroup":"2","name":"低频彩","imgCode":"PL3"},
 					"3":{"lotGroup":"3","name":"PK10","imgCode":"BJSC"},
 					"4":{"lotGroup":"5","name":"11选5","imgCode":"11X5"},
 					"5":{"lotGroup":"6","name":"六合彩","imgCode":"LHC"},
 					"6":{"lotGroup":"7","name":"快三","imgCode":"K3"},
 					"7":{"lotGroup":"8","name":"快乐十分","imgCode":"KLSF"},
 					"8":{"lotGroup":"9","name":"PC蛋蛋","imgCode":"28"}
 			}
 			for(let i =1;i<9;i++){
 			    for (let index=0;index<lotteryLists.length;index++){
 			        if (lotteryLists[index].code.indexOf(lst[i].imgCode) > -1){
                        lst[i].imgUrl = lotteryLists[index].imgUrl
                    }
                }
 			    if (lst[i].imgUrl == '' || lst[i].imgUrl == undefined || 'imgUrl' in lst[i] == false){
 			    	let imgUrlNum = lotteryLists[0].imgUrl.lastIndexOf('/')
					let imgUrl = lotteryLists[0].imgUrl.slice(0,imgUrlNum)
					switch (lst[i].lotGroup) {
						case "1":
							lst[i].imgUrl = imgUrl +'/FFC.png'
							break;
						case "2":
							lst[i].imgUrl = imgUrl +'/PL3.png'
							break;
						case "3":
							lst[i].imgUrl = imgUrl +'/LBJSC.png'
							break;
						case "5":
							lst[i].imgUrl = imgUrl +'/GX11X5.png'
							break;
						case "6":
							lst[i].imgUrl = imgUrl +'/HKMHLHC.png'
							break;
						case "7":
							lst[i].imgUrl = imgUrl +'/TMK3.png'
							break;
						case "8":
							lst[i].imgUrl = imgUrl +'/HNKLSF.png'
							break;
						case "9":
							lst[i].imgUrl = imgUrl +'/JND28.png'
							break;
					}
				}
            }
 			for(var i=1;i<9;i++){
 				if(i == '3' || i == '6' || i == '8'){
 					html += '<div class="index_lottery_item lotteryClick" data="'+lst[i].lotGroup+'">';
 					html += '	<a href="#" class="external">';
 					html += '		<div class="lottery_img">';
					html += '			<img src="'+lst[i].imgUrl+'"/>';

					// html += '			<img src="'+$base+'/images/lottery/'+lst[i].imgCode+'.png"/>';
 					html += '		</div>';
 					html += '		<div class="lottery_info">';
 					html += '			<div class="lottery_name">'+lst[i].name+'</div>';
 					html += '		</div>';
 					html += '	</a>';
 					html += '	<div class="cl"></div>';
 					html += '</div>';
 					if(i == '3'){
 						html += '<div class="caizhongZs'+i+'" style="display:none;width:100%;clear: left;">';
 						html += '</div><br/>';
 					}else if(i == '6'){
 						html += '<div class="caizhongZs'+i+'" style="display:none;width:100%;clear: left;">';
 						html += '</div><br/>';
 					}else if(i == '8'){
 						/* vr 娱乐城 */
 						var vrUrl  = "MobileIndexUtil.go('"+base+"/forwardVr.do')";
 						html += '<div class="index_lottery_item" id="VRyulecheng">'
 						html += ' <a '
 						html += ' <c:if test="${isLogin}">onclick="'+vrUrl+'" href="javascript:void(0);"</c:if>'
						html += ' <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">'
 						html += '						<div class="lottery_img">'
 						html += '							<img src="'+base+'/mobile/anew/resource/new/images/vr.png" />'
 						html += '						</div>'
 						html += '						<div class="lottery_info">'
 						html += '							<div class="lottery_name">VR娱乐游戏</div>'
 						html += '						</div>'
 						html += '</a>'
 						html += '<div class="cl"></div>'
 						html += '</div>'
 						/*  */
 						html += '<div class="caizhongZs'+i+'" style="display:none;width:100%;clear: left;">';
 						html += '</div>';
 					}
 				}else{
 					html += '<div class="index_lottery_item lotteryClick" data="'+lst[i].lotGroup+'">';
 					html += '	<a href="#" class="external">';
 					html += '		<div class="lottery_img">';
					// html += '			<img src="'+$base+'/images/lottery/'+lst[i].imgCode+'.png"/>';
						html += '			<img src="'+lst[i].imgUrl+'"/>';
 					html += '		</div>';
 					html += '		<div class="lottery_info">';
 					html += '			<div class="lottery_name">'+lst[i].name+'</div>';
 					html += '		</div>';
 					html += '	</a>';
 					html += '	<div class="cl"></div>';
 					html += '</div>';
 				}
 			}
 			$('#fenlei').html(html);
 			if($('#isVrOnOff').val() == "on" && $('#').val() != true){
 				$('#VRyulecheng').css('display','block');
 			}else{
 				$('#VRyulecheng').css('display','none');
 			}
 	 		$('.lotteryClick').click(function(){
 	 			$(this).siblings('.lotteryClick').removeClass('borderStyle');
 				var lotteryHtml = '';
 				var thisData = $(this).attr('data');
 				var lotteryGroups ;
				$.ajax({
					url:"${base}/native/getLoctterys.do",
					data:{lotGroup:thisData},
					type:"post",
					success:function (res) {
						lotteryGroups = res.content
						for(var n=0;n<lotteryGroups.length;n++){
							if(lotteryGroups[n].hotGame == 2){
								lotteryHtml += '<div class="index_lottery_style" style="background: url('+lotteryGroups[n].gifUrl+');background-size:100% 100%;">';
							} else {
								lotteryHtml += '<div class="index_lottery_style">';
							}
							lotteryHtml += '	<a href="'+$base+'/bet_lotterys.do?lotCode='+lotteryGroups[n].code+'" class="external">';
							lotteryHtml += ' <a '
							lotteryHtml += ' <c:if test="${isLogin}"> href="'+$base+'/bet_lotterys.do?lotCode='+lotteryGroups[n].code+'"</c:if>'
							lotteryHtml += ' <c:if test="${!isLogin}"> href="${base }/mobile/v3/login.do" </c:if> class="external">'
							lotteryHtml += '		<div class="lottery_img">';
							lotteryHtml += '			<img src="'+lotteryGroups[n].imgUrl+'" style="width: 60px;">';

							// lotteryHtml += '			<img src="'+$base+'/images/lottery/'+lotteryLists[n].code+'.png"/ style="width: 50px;">';
							lotteryHtml += '		</div>';
							lotteryHtml += '		<div class="lottery_info">';
							lotteryHtml += '			<div class="lottery_name" style="font-size: 0.7rem;">'+lotteryGroups[n].name+'</div>';
							lotteryHtml += '		</div>';
							lotteryHtml += '	</a>';
							lotteryHtml += '</div>';
						}
						if(thisData == '1' || thisData == '2' || thisData == '3'){
							$('.caizhongZs3').html(lotteryHtml);
							if($(this).hasClass('borderStyle')){
								$(this).removeClass('borderStyle');
								$('.caizhongZs3').hide(150);
							}else{
								$(this).addClass('borderStyle');
								$('.caizhongZs3').show(200);
							}
							$('.caizhongZs6').hide(150);
							$('.caizhongZs8').hide(150);
						}else if(thisData == '5' || thisData == '6' || thisData == '7' ){
							if($(this).hasClass('borderStyle')){
								$(this).removeClass('borderStyle');
								$('.caizhongZs6').hide(150);
							}else{
								$(this).addClass('borderStyle');
								$('.caizhongZs6').show(200);
							}
							$('.caizhongZs3').hide(150);
							$('.caizhongZs6').html(lotteryHtml);
							$('.caizhongZs8').hide(150);
						}else if(thisData == '8' || thisData == '9'){
							if($(this).hasClass('borderStyle')){
								$(this).removeClass('borderStyle');
								$('.caizhongZs8').hide(150);
							}else{
								$(this).addClass('borderStyle');
								$('.caizhongZs8').show(200);
							}
							$('.caizhongZs3').hide(150);
							$('.caizhongZs6').hide(150);
							$('.caizhongZs8').html(lotteryHtml)
						}
					}
				})
 				<%--for(var n=0;n<lotteryGroups.length;n++){--%>
 				<%--	if(lotteryLists[n].lotGroup == thisData){--%>
 				<%--		lotteryHtml += '<div class="index_lottery_style">';--%>
					<%--	lotteryHtml += '	<a href="'+$base+'/bet_lotterys.do?lotCode='+lotteryLists[n].code+'" class="external">';--%>
					<%--	lotteryHtml += ' <a '--%>
					<%--	lotteryHtml += ' <c:if test="${isLogin}"> href="'+$base+'/bet_lotterys.do?lotCode='+lotteryLists[n].code+'"</c:if>'--%>
					<%--	lotteryHtml += ' <c:if test="${!isLogin}"> href="${base }/mobile/v3/login.do" </c:if> class="external">'--%>
 				<%--		lotteryHtml += '		<div class="lottery_img">';--%>
					<%--	lotteryHtml += '			<img src="'+lotteryLists[n].imgUrl+'" style="width: 50px;">';--%>

					<%--	// lotteryHtml += '			<img src="'+$base+'/images/lottery/'+lotteryLists[n].code+'.png"/ style="width: 50px;">';--%>
 				<%--		lotteryHtml += '		</div>';--%>
 				<%--		lotteryHtml += '		<div class="lottery_info">';--%>
 				<%--		lotteryHtml += '			<div class="lottery_name" style="font-size: 0.5rem;">'+lotteryLists[n].name+'</div>';--%>
 				<%--		lotteryHtml += '		</div>';--%>
 				<%--		lotteryHtml += '	</a>';--%>
 				<%--		lotteryHtml += '</div>';--%>
 				<%--	}--%>
 				<%--}--%>
 			});
 		}
 	})
 	</script>
</c:if>
</c:if>
	<style>
 	.borderStyle{ 
 		border-bottom:2px solid orange; 
 	} 
 	.index_lottery_style{ 
 		padding: 0.2rem 0.2rem; 
     	height: 5rem;
     	position:relative; 
    	width:33%;
     	display:inline-block; 
     	text-align: center; 
 	} 
	</style>

