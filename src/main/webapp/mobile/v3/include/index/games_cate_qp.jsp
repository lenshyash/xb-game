<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	var MobileIndexUtil = {
			a:1,
			b:100,
			c:null,
			d:false,
			go:function(url){
				$("#loading-page").css('display','flex')
				$.ajax({
					url : url,
					async : false,
					success : function(result) {
						if(!result.success){
							$.toast(result.msg);
							return;
						}
						if(result.url){
							location.href = result.url;
						}else{//bbin 返回的是 html
							$("#loading-page").css('display','none')
							var windowOpen = window.open(result.html);
							var new_doc = windowOpen.document.open("text/html","replace");
							new_doc.write(result.html);
							new_doc.close();
						}
					}
				});
			},	
			toLogin:function(e){
				location.href = M.res + '/login.do';
			}
		}
	</script>
	<c:if test="${isKyOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardKy.do?h5=1');" href="javascript:void(0);"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ky.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">KY棋牌</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	