<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
$(function(){
	noticeAlert();
    $(".close123").click(function () {
        $(".newNoticeFrame").hide()
    })
});

function noticeAlert(){
	if(!localStorage.getItem('setWindowFrame')){
		noticeData();
	}
}

function showNoticeAlert(){
	noticeData();
}
function noticeData(){
	$.ajax({
		url : "${base}/getConfig/getArticle.do",
		data : {
			code : 19
		},
		type : "post",
		dataType : 'json',
		success : function(j) {
            var mobileLength = j.length
		    if(j.length > 0){
                $(".newNoticeFrame").show()
                newNoticeFrame(j)
            }
            if (mobileLength == 1) {
                // $(".active-content .active-main").find(".activeTitles:eq(0)").hide()
                $(".active-content .active-main").find(".contentStyle:eq(0)").show()
            }else{
                // $(".active-content .active-main").find(".contentStyle:eq(0)").show()
            }
			// if(localStorage.getItem("popModule") == 'hide'){
			// 	if((new Date().getTime() - localStorage.getItem("popDate")) > 86400000){
			// 		if (j.length > 0) {
            //             newNoticeFrame(j)
			// 		}
			// 	}
			// }else{
			// 	if (j.length > 0) {
            //         newNoticeFrame(j)
			// 	}
			// }
			
		}
	});
}

function noticeFrame(t,c){
	layer.open({
		  shade: 0.9, //遮罩透明度
		  skin:'layui-layer-rim',
		  content: c 
		  ,title: t
		  ,anim: 'up',
		  btn: ['知道了'],
		  yes: function(index){
			    $('#show_notice_content').show();
			    layer.close(index);
			  }
		});
	$('.layui-m-layersection .layui-m-layerchild h3').css({
			'font-size':'.8rem',
			'height':'1.5rem',
			'line-height':'1.5rem',
			'background-color': '#e5d7ba',
		    'color': '#4c2515',
		    'font-weight':' bold',
	});
	$('.layui-m-layerbtn').css({
		'width':'28%',
	    'height': '35px',
	    'margin': '0 35% 2%',
	    'line-height': '35px',
	    'background': '#f4eace',
	    'border-radius': '5px',
	});
	$('.layui-m-layerbtn span').css('background','#ee8e4c');
	/* $('.layui-m-layerchild').css('background-color','#e5d7ba'); */
	$("#noPop").click(function(){
		 $(this).hide()
		 $("#yesPop").show()
		 localStorage.setItem("popModule",'hide');
        localStorage.setItem("popDate",new Date().getTime())
	})
	$("#yesPop").click(function(){
		$(this).hide()
		$("#noPop").show()
		 localStorage.setItem("popModule",'show');
	})
}
function newNoticeFrame(res) {
    var html = ''
    $.each(res,function (index,item) {
        html +='<div class="active-item"><a href="#" class="activeTitles"><div class="red-title">' +
            '</div><span class="textStyle">'+item.title+'</span></a> <div class="contentStyle">'+item.content+'</div></div>'
    })
    $(".active-main").html(html)
    $(".active-item").click(function () {
        $(this).find('.contentStyle').toggle()
    })
}
</script>
<%--new弹窗样式--%>
<div class="newNoticeFrame">
    <div class="active-inner">
        <div class="active-top">
            <div class="top-active">平台公告</div>
            <a href="#" class="close123">×</a>
        </div>
        <div class="active-content">
            <div class="active-main">
                <%--<div class="active-item">--%>
                    <%--<a href="#" class="activeTitles">--%>
                        <%--<div class="red-title"></div>--%>
                        <%--<span class="textStyle">asdad</span>--%>
                    <%--</a>--%>
                    <%--<div class="contentStyle">--%>

                    <%--</div>--%>
                <%--</div>--%>
            </div>
        </div>
    </div>
</div>
<script>

</script>
<style>
    .newNoticeFrame{
        background: rgba(0,0,0,.5);
        position: fixed;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        z-index: 99999;
        display: none;
    }
    .active-inner{
        font-family: 'Microsoft YaHei',Arial,Verdana;
        width: 90%;
        height: 80%;
        left: 5%;
        top: 10%;
        position: absolute;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding: .5rem;
        background: rgba(255,255,255,0.3);
        z-index: 99;
    }
    .active-top{
        width: 100%;
        position: relative;
        text-align: center;
        line-height: 4rem;
        border-radius: .4rem .4rem 0 0;
        background: #b62929;
    }
    .top-active{
        font-size: 1.2rem;
        letter-spacing: .2rem;
        text-shadow: none;
        color: #fff;
    }
    .close123{
        position: absolute;
        right: .5rem;
        top: 0;
        display: block;
        width: 3rem;
        line-height: 4rem;
        text-align: center;
        font-size: 2.2rem;
        color: #fff;
    }
    .active-content{
        position: absolute;
        top: 4.5rem;
        bottom: .5rem;
        left: .5rem;
        right: .5rem;
        max-height: 100%;
        overflow: auto;
        border-radius: 0 0 .4rem .4rem;
        background: #fff;
    }
    .active-main{
        margin: 0 4% 1rem 4%;
    }
    .active-item{
        padding: 1rem 0;
        border-bottom: 1px dashed #dbdbdb;
        float: left;
        width: 100%;
    }
    .activeTitles{
        clear: both; color: #000;
        text-decoration: none;
        -webkit-tap-highlight-color: transparent;
        background:#fff;
        display:block;
    }
    .red-title{
        background: #b62929;
        width: .6rem;
        height: .6rem;
        margin: 1.2rem 0;
        border-radius: 50%;
        float: left;
    }
    .textStyle{
        max-width: 90%;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 0.9rem;
        padding-left: 0.6rem;
        color: rgb(76, 76, 76);
        float: left;
        line-height:3rem
    }
    .contentStyle{
        float: left;
        width: 100%;
        white-space: normal;
        word-break: break-all;
        display: none;
    }
    .contentStyle img{
        width: 100%!important;
    }
</style>