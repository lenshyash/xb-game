<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${isZrOnOff=='on' && isGuest != true}">
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardAg.do?h5=1&amp;gameType=11','ag');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/agreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardMg.do?gameType=1&amp;gameid=66936&amp;h5=1','mg');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/mgreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardBbin.do?type=live','bbin');" href="javascript:void(0);"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/bbinreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BBIN真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>

	<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardBg.do?type=2&amp;h5=1','bg');" href="javascript:void(0);"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/bg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BG真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isAbOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardAb.do','ab');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ab.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AB真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	
	<c:if test="${isOgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardOg.do?gametype=mobile','og');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ogzr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">OG真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	
	<c:if test="${isDsOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardDs.do?h5=1','ds');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/dszr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">DS真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${m }/third/index.do?code=ptzr"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/pt.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">PT真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isEbetOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="MobileIndexUtil.go('${base}/forwardEbet.do?','ebet');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ebet.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">EBET真人娱乐</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	
	</c:if>