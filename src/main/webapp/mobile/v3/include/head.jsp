<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta http-equiv="Access-Control-Allow-Origin" content="*">
<link rel="shortcut icon" href="${base }/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="${base }/images/favicon.ico"> 
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="${desktopIconLink }">
<link rel="stylesheet" href="${res}/js/light7/css/light7.min.css">
<link rel="stylesheet" href="${res}/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${res}/css/common.css?v=1.0">
<link rel="stylesheet" href="${res}/css/swiper-3.4.2.min.css">
<%--彩票css先独立，防止冲突，开发完成后合并到common.css  --%>
<link rel="stylesheet" href="${res}/css/lottery.css">
<script type="text/javascript">
M = {
	base : '${base}',
	folder : "${stationFolder}",
	caipiao_version : '${caipiao_version}',
	version : '${version}',
	isLogin : '${isLogin}',
	res : '${m}',
	k3baoziDrawShow:'${k3baoziDrawShow}'
}
</script>
<script type='text/javascript' src='${base}/mobile/v3/js/jquery-2.1.4.js' charset='utf-8'></script>
<script type='text/javascript' src='${res}/js/light7/js/light7.min.js' charset='utf-8'></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<!-- 修改头部颜色 -->
<script>
window.onload=function(){ 
	var jsp = "<span id='mobileheadcolor' style='display:none;'>${mobileHeadColor }</span>"
	$("body").append(jsp)	
	mobileheadcolor();
} 
function mobileheadcolor(){
	var color = $("#mobileheadcolor").text()
	var of = $("#colorOn").text()
	if(of == 1){
		localStorage.setItem("colorB",color);	
	}else{
		color = (localStorage.getItem("colorB"))
	}
	var jdb = '<style>.bar-nav{background:'+color+'}</style>'
	$(".bar-nav").css("background",color)
	$("body").after(jdb)
}
var balanceIs = sessionStorage.getItem("autoTrans");

<c:if test="${isLogin}">
if(balanceIs == 'true' || balanceIs == null){
	thirdBalance()
}

	function thirdBalance(){
		$.ajax({
			url:'/native/checkLastThirdBalance.do',
			type:'post',
			success:function (res) {
				if(res.success){
					sessionStorage.setItem("autoTrans", "false");
				}
			}
		})
	}
</c:if>
</script>
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/mobile/chatMobileModule.jsp" ></jsp:include>
</c:if>
