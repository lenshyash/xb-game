<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 投注记录注单详情tpl --%>
<input id="domainFolder" value="${domainFolder }" type="hidden">
<script type="text/html" id="lotteryOrderDetail">
		<div class="shangqkaij tac">
			<span class="tac co000">订单号： <em class="hong">{{orderId}}</em></span>
			<div class="cl"></div>
		</div>
		<table class="lottery_detail" style="width:100%;">
			<tbody>
				<tr>
					{{if domainFolder != 'd00530'}}
						<td class="bttd">帐号</td><td>{{account}}</td>
					{{/if}}
					{{if version == 2 || version == 5 || lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC' || lotCode == 'WFLHC'|| lotCode == 'HKMHLHC'}}
						<td class="bttd">单注金额</td><td class="last4">{{(buyMoney/buyZhuShu).toFixed(2)}}</td>
					{{else}}
						<td class="bttd">单注金额</td><td class="last4">{{(2/model).toFixed(2)}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">下注时间</td><td>{{createTime}}</td>
					<td class="bttd">投注注数</td><td>{{buyZhuShu}}</td>
				</tr>
				<tr>
					<td class="bttd">彩种</td><td>{{lotName}}</td>
					<td class="bttd">倍数</td><td>{{multiple}}</td>
				</tr>
				<tr>
					<td class="bttd">期号</td><td>{{qiHao}}</td>
					<td class="bttd">投注总额</td><td>{{buyMoney.toFixed(2)}}</td>
				</tr>
				<tr>
					<td class="bttd">玩法</td><td>{{playName}}</td>
					{{if version == 2 || version == 5 || lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC' || lotCode == 'WFLHC'|| lotCode == 'HKMHLHC'}}
						<td class="bttd">赔率</td><td>{{peilv?peilv:odds}}</td>
					{{else}}
						<td class="bttd">奖金</td><td>{{odds}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">开奖号码</td><td>{{lotteryHaoMa}}</td>
					<td class="bttd">中奖注数</td><td>{{winZhuShu?winZhuShu:0}}</td>
				</tr>
				<tr>
					<td class="bttd">状态</td><td>{{$showStatus status}}</td>
					<td class="bttd">中奖金额</td><td>{{winMoney?winMoney.toFixed(2):0.00}}</td>
				</tr>
				<tr>
					{{if winMoney > 0 && winZhuShu > 0}}
						<td class="bttd">盈亏</td><td>{{(winMoney - buyMoney).toFixed(2)}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">投注号码</td>
					<td class="" colspan="3">
						<div style="width: 100%;font-size: .8rem; padding: .5rem;">{{haoMa}}</div>
					</td>
				</tr>
			</tbody>
		</table>
		{{if lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC'|| lotCode == 'WFLHC'|| lotCode == 'HKMHLHC'}}
		<div style="width: 100%;background: #D9EDF7;border-radius: 3px;padding: 5px;text-align: center;font-size: 12px;">
			温馨提示:(中奖号码,既有本命年(0尾),又有非本命年(非0尾) 可能会出现2种赔率的情况)
		</div>
		{{/if}}
	</script>
	
<%--开奖结果tpl --%>
<script type="text/html" id="pageDrawNoticeTpl">
			{{each data as value i}}
			<li style="{{i%2!=0?'background:#f4f4f4':''}}"><a href="${base}/mobile/v3/draw_notice_details.do?lotCode={{value.lotCode}}" class="item-link item-content">
				<div class="item-media">
					{{if value.imgUrl == null || value.imgUrl== ""}}
						<img src="${base}/mobile/anew/resource/images/{{value.lotCode}}.png">
					{{else}}
						<img src="{{value.imgUrl}}">
					{{/if}}
				</div>
				<div class="item-inner">
					<div class="item-title-row">
						<div class="item-title" style="font-size: .7rem;">{{value.lotName}}</div>
						<div class="item-after">第{{value.qihao}}期</div>
					</div>
					<div class="item-text">{{$tplHaoMa value.haoma value.lotCode}}</div>
					</div>
				</a>
			</li>
			{{/each}}
		</script>
<%-- 开奖号码详情 --%>	
<script type="text/html" id="drawNoticeDetailsDataTpl">
		{{each data.list as value i}}
		<li style="{{i%2!=0?'background:#f4f4f4':''}}">
			<div class="item-content">
				<div class="item-inner">
				<div class="item-title-row">
					<div class="item-title" style="font-size: .7rem;">第{{value.qiHao}}期</div>
					<div class="item-after">{{value.date}}&nbsp;{{value.time}}</div>
				</div>
				<div class="item-subtitle">{{$tplHaoMas value.haoMaList}}</div>
				<div class="item-subtitle item-mark">{{$tplHaoMasLHC value.haoMaList,value.date}}</div>
				</div>
			</div>
		</li>
		{{/each}}
	</script>
	
<%-- 站内信 --%>
<script type="text/html" id="sysAgentMessageTpl">
	<li class="{{types}}" data-id="{{id}}"><label class="label-checkbox item-content" style="padding-left:1.5rem;"> 
		<input type="checkbox" name="checkbox" value="{{userMessageId}}">
		<div class="item-media" style="display:none;">
			<i class="icon icon-form-checkbox"></i>
		</div>
		<div class="item-inner" style="font-size:.7rem;">
			<div class="item-title-row">
				<div class="item-title">{{titles}}</div>
				<div class="item-after">{{createTime}}</div>
			</div>
		</div>
	</label></li>
</script>