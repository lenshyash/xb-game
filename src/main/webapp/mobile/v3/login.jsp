<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="${res}/css/login/global.css?ver=4.4" type="text/css">
    <script src="${base}/mobile/anew/resource/new/js/jquery.min.js?ver=4.4"></script>
    <script>
    	$(function(){
    		var _padding = function()
    		{
    			try{
	    			var l = $("body>.header").height();
		    		if($("body>.lott-menu").length>0)
		    		{
		    			l += $("body>.lott-menu").height();
		    		}
		    		$("#wrapper_1").css("paddingTop",l+"px");
	    		}catch(e){}
	    		try{
		    		if($("body>.menu").length>0)
		    		{
		    			var l = $("body>.menu").height();
		    		}
		    		$("#wrapper_1").css("paddingBottom",l+"px");
	    		}catch(e){}
    		};
    		(function(){
    			_padding();
    		})();
    		$(window).bind("load",_padding);
    	});
    </script>
    <!-- hide address bar -->
        <style>
            body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
            #wrapper_1{overflow-y:visible!important;}
            body.login-bg{padding-top:0px!important;}
        </style>
        <script>
            function loaded(){}//空实现
        </script>
        <title>${_title }--登录</title>
    </head>
    <body class="login-bg" data-ext-version="1.4.2">
        <div class="header">
            <div class="headerTop">
                <div class="ui-toolbar-left">
                    <button id="reveal-left" onclick="window.history.go(-1);">reveal</button>
                </div>
                <h1 class="ui-toolbar-title">登录</h1>
            </div>
        </div>
		<c:if test="${domainFolder eq 'd00519'}">
		<style>
			.header{
				background: rgb(23, 23, 25)!important;
			}
			.header #reveal-left, .header .reveal-left{
				background: url(../../images/blank_02.png) no-repeat!important;
			}
		</style>
		</c:if>
		<input type="hidden" value="${onOffVerifyCode}" id="loginCode">
        <div class="login">
            <ul>
                <li>
                   	<span class="login-peo"></span>
                   	<input type="text" id="log_account" placeholder="请输入用户账号">
                   	<span class="login-gli"></span>
                </li>
                <li>
                    <span class="login-pass"></span>
                    <input type="password" id="log_password" placeholder="请输入密码">
                </li>
                <c:if test="${onOffVerifyCode  eq 'true'}">
	                <li id="showCode">
	                    <span class="login-pass"></span>
	                    <input type="text" id="code" placeholder="请输入验证码" style="position:relative;top:0px;" onfocus="reloadImg();">
	                    <img src="${base}/mobile/authnum.do" id="regCode" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" onclick="reloadImg();">
	                </li>
                </c:if>
            </ul>
            <div class="row" style="margin-top: 8px;text-align: right;margin-bottom: 8px;">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="checkbox">
						<label class="white" style="color: #666;">
							<input id="remberLogin" type="checkbox" checked="checked"> 记住密码
						</label>
					</div>
				</div>
			</div>
            <div class="login-p">
            	<c:if test="${kfSwitch eq 'on'}">
	            	<a class="fl" <c:if test="${mobileOpenOutlinkType eq 'blank' }">href="javascript:void(0);" onclick="window.open('${kfUrl }');"</c:if><c:if test="${mobileOpenOutlinkType eq 'inner' }">href="${m }/customerService.do"</c:if><c:if test="${mobileOpenOutlinkType eq 'href' }">href="${kfUrl }"</c:if>>在线客服</a>
            	</c:if>
            </div>

	            <button class="login-btn" id="login_btn">登录</button>
	            
	            <c:if test="${isReg eq 'on'}">
	            	<button class="reg-btn" onclick="location.href='${m}/regpage.do'">立即注册</button>
	            </c:if>
	            
	            <c:if test="${not empty testAccount && testAccount eq 'on'}">
	            	<button class="reg-btn" id="login_demo" onclick="location.href='${m}/swregpage.do'">免费试玩</button>
	            </c:if>
	            <button class="reg-btn" onclick="location.href='${m}'">返回首页</button>
        </div>

<!-- 加载中 -->
<div class="loading" style="display: none"></div>
<div class="loading-bg" style="display: none"></div>
<div class="beet-odds-tips round" id="tip_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="tip_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            号码选择有误
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que" style="width: 100%;" onclick="tipOk()"><span>确定</span></button>
    </div>
</div>
<div id="tip_bg" class="tips-bg" style="display: none;"></div>
<script>
    function loadingShow(tips,bg) {
        if(tips == ""||typeof(tips) == "undefined"){
            $(".loading").css("left","50%");
            $(".loading").css("margin-left","-2em");
            $(".loading").html("加载中...");
        }else{
            $(".loading").html(tips);
            $(".loading").css("left",Math.ceil(($(document).width() - $(".loading").width())/2));
            $(".loading").css("margin-left",0);
        }

        bg   = (bg == ""||typeof(bg) == "undefined")?1:0;
        if (bg == 1){
            $(".loading-bg").show();
        }else{
            $(".loading-bg").hide();
        }
        $(".loading").show();
    }
    function loadingHide() {
        $(".loading").hide();
        $(".loading-bg").hide();
    }
</script>
        
<style>
    .center {text-align: center}
</style>
<script type="text/javascript" src="${m}/js/jquery.cookie.js"></script>    
<script>

	$(function(){
		getMarkLoginInfo();
	});

	function tipOk() {
	    $('#tip_pop').hide();
	    $('#tip_bg').hide();
	}
	
	function msgAlert (msg,funcParm) {
	    $('div#tip_pop_content').html(msg);
	    $('div#tip_pop').show();
	    $('div#tip_bg').show();
	    func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
	}

	function reloadImg(){
		var url = "${base}/mobile/authnum.do?timestamp=" + (new Date().getTime());
		$("#regCode").attr("src", url);
	}
	
	function msgAlert (msg,funcParm) {
	    $('div#tip_pop_content').html(msg);
	    $('div#tip_pop').show();
	    $('div#tip_bg').show();
	    func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
	}
	
	//登陆验证
    $('#login_btn').click(function (event) {
        var userName = $('#log_account').val();
        var password = $('#log_password').val();
        var code = $('#code').val();
        if (!userName) {
            msgAlert('请输入用户名！');
            return;
        }
        if (!password) {
            msgAlert('请输入密码！');
            return;
        }
        loadingShow();
        
        //可能网络原因或者手机型号原因,多判断一次非空情况
        if(!userName || !password){
        	userName = $('#log_account').val();
        	password = $('#log_password').val();
        }
        
		var data = {
			'account' : userName,
			'password' : password,
			'verifyCode' : code
		};
		
		$.ajax({
			url : "${m}/dologin.do",
			type : "post",
			data : data,
			success : function(result, textStatus, xhr) {
				loadingHide();
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					if(!result.success){
						$('#code').val('');
						reloadImg();
						msgAlert(result.msg);
					}else{
						var remberLoginChk = $('#remberLogin').prop("checked");
						if(remberLoginChk){
							setMarkLoginInfo(userName,password);//存cookie
						}else{
							setMarkLoginInfo('','');
						}
						location.href = '${m}/index.do';
					}
				} else if (ceipstate == 2) {// 后台异常
					$('#code').val('');
					reloadImg();
					msgAlert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					$('#code').val('');
					$('#showCode').show();
					if(result.msg == 'needCode'){
						msgAlert('验证码错误!');
					}else{
						msgAlert(result.msg);
					}
					reloadImg();
				}
			}
		});
        
    });
	
	function setMarkLoginInfo(u,p){
		$.cookie('userName', u, { expires: 7, path: '/' });
		$.cookie('password', p, { expires: 7, path: '/' });
	}
	
	function getMarkLoginInfo(){
		var u = $.cookie('userName');
		var p = $.cookie('password');
		$('#log_account').val(u);
        $('#log_password').val(p);
	}
</script>
</body>
</html>