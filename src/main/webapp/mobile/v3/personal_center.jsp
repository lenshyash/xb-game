<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_personal_center_jsp">
		<jsp:include page="include/barTab.jsp" />
		<div class="content">
<style>
	#openChat{
		width:40px!important;
		top:60px!important;
	}
</style>
<script>
	$("#openChat img").attr('src','${base}/common/template/member/center/img/1516676959037063.png')
</script>

			<!-- 头部 -->
			<div class="content personal_center_content">
				<div class="usertou">
					<div class="userminc">
						<a href="${m }/personal_info.do">    
							<div class="user_logined_img">
								<c:choose>
									<c:when test="${domainFolder == 'x00246'}">
										<img src="${res }/images/image_2020_10_09T06_50_49_618Z.png">
									</c:when>
									<c:otherwise>
										<img src="${res }/images/touxiang.png">
									</c:otherwise>
								</c:choose>
							</div> <span class="account">${loginMember.account}</span>
						</a>			
						<span class="account vip_level" style="color:yellow;margin-left:10px;font-weight: 600;"></span>
						<a id="logoutBtn" class="pull-right"> 退出 <em
							class="iconfont icon_loginout"></em>
						</a>
						<img class="vipImg" src="${loginMember.levelIcon}" style="position: absolute;width: 47px;">	
					</div>
					<script>
						var mob = "${domainFolder}";
						var vip = '${loginMember.levelIcon}';
						if(mob == 'b203'){
							$('#vip_message').css('display','none');
						};
						console.log(mob);
						if(mob =='d00519'){
							$('.usertou').css('background','#171719');
						}
						if(vip){
							$('.vipImg').show();
							$('.vipImg').attr('id','vip_message')
							$('.vip_level').hide();
							$('.vip_level').attr('id','')
						}else{
							$('.vipImg').hide();
							$('.vipImg').attr('id','')
							$('.vip_level').show();
							$('.vip_level').attr('id','vip_message')
						}
					</script>
					<div style="width:100%;height:2.6rem;margin-top:5px;display:none;border-bottom:1px solid #b50003;" id="p_level">
						<div style="padding-left:5%">VIP等级:<span class="vip_level"></span></div>
						<div style="padding-left:5%">升级充值所需:<span id="vip_need"></span></div>
						<div style="padding-left:5%">下一级:<span id="vip_next"></span></div>
						<div style="padding-left:5%">晋级赠送彩金:<span id="vip_gif"></span></div>
				</div>
				<script>
$.ajax({
	url : "${base}/center/member/meminfo/mobieLevel.do",
	data : {},
	type : "post",
	dataType : 'json',
	success : function(j) {
		if(j != null) {
			$(".vip_level").html(j.current.toUpperCase());
			$("#vip_next").html(j.next.toUpperCase());
			$("#vip_need").html(j.need);
			$("#vip_gif").html(j.gifMoney);
		};
	}
});
$("#vip_message").click(function(){
	$("#p_level").toggle(200)
})
$('#p_level').find('div').css('font-size','16px')
</script>
				<style>
					#p_level div{
						width:40%;
						float: left;
						color:#fff;
						white-space: nowrap;
					}
					#p_level div span{
						color:yellow;
						padding-left:5px;
					}
				</style>
					<div class="doudoubox">
						<em class="iconfont icon-shujuku jindouicon"></em> 可用余额： <span
							id="meminfoMoney">${loginMember.money}</span>
						<c:if test="${not empty cashName}">
                		${cashName}
	                </c:if>
						<c:if test="${empty cashName}">
	                  	元 
	                </c:if>
					<c:if test="${isBgemOnOff=='on'}">
						<img src="${m}/personal/images/yuebao/yuebaoLink.png" style="width: 70px;right: 15px;position: absolute" alt="" onclick="location.href='${base}/mobile/v3/balanceGemInfo.do'">
					</c:if>
					</div>
				</div>

				<c:if test="${isGuest != true && iosExamine ne 'off' }">
					<div class="qmenu" style="display: flex;">
						<a external href="${m }/pay_select_way.do" class="chongzhitkuan"
							style="flex-grow: 1;"> <em class="iconfont icon-qia"></em>充值
						</a> <a external href="${m }/withdraw_money.do" class="chongzhitkuan"
							style="flex-grow: 1;"> <em class="iconfont icon-qukuanji1"></em>提款
						</a>
						<c:if test="${isZrOnOff=='on' || isDzOnOff eq 'on' || isChessOnOff eq 'on' || isThdLotOnOff eq 'on' || isTsOnOff eq 'on'}">
							<a external href="${m}/live_game.do" class="chongzhitkuan"
								style="flex-grow: 1;"> <em class="iconfont icon-qukuanji1"></em>额度转换
							</a>
						</c:if>
					</div>
				</c:if>

				<!-- 游戏记录 -->
				<ul class="userlb" style="margin: 0rem 0rem 0.4rem;">
					<c:if test="${isCpOnOff=='on'}">
						<li><a href="#" onclick="location.href = '${m}/betting_record_lottery.do'"><em
								class="iconfont icon-lianxijilu icontublb"></em> 彩票投注记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isThdLotOnOff=='on'}">
						<li><a href="${m}/betting_record_lottery_third.do"><em
								class="iconfont icon-sfcp icontublb"></em> 三方彩票记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isLhcOnOff=='on'}">
						<li><a href="${m }/betting_record_sixmark.do"><em class="iconfont icon-61 icontublb"></em>
								六合投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isTyOnOff=='on' && isGuest != true}">
						<li><a href="${m}/sport/hg/betting_record_sport.do"><em
								class="iconfont icon-zuqiu1 icontublb"></em> 体育投注记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isZrOnOff=='on' && isGuest != true}">
						<li><a href="${m }/third/betting_record_live.do"><em
								class="iconfont icon-jilu1 icontublb"></em> 真人投注记录<em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isChessOnOff=='on'}">
						<li><a href="${m}/betting_record_chess.do"><em
								class="iconfont icon-qipai icontublb"></em> 棋牌游戏记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isKxOnOff=='on'}">
					<li><a href="#" onclick="location.href='${m}/third/betting_record_esport.do'"><em
							class="iconfont icon-taiqiu icontublb"></em> 电竞投注记录 <em
							class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isDzOnOff=='on' && isGuest != true}">
						<li><a href="${m }/third/betting_record_egame.do"><em
								class="iconfont icon-iconwtbappwenjian icontublb"></em> 电子游戏记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${iosExamine ne 'off' && isChangeMoney=='on' && isGuest != true}">
						<li>
							<a href="${m }/mnyrecord.do">
								<em class="iconfont icon-record icontublb"></em> 用户账变记录
								<em class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
					</c:if>
					<c:if test="${switchKey ne 'off'}">
						<li >
							<a href="#" onclick="location.href='${m}/awardRecord.do'" >
								<img src="images/win.png" alt="" style="height: 27px;width: 27px;margin-right: 5px;">
								<span style="padding-top: 2px">活动中奖记录</span>
								<em class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
					</c:if>
					<c:if test="${iosExamine ne 'off'}">
						<li><a href="${m }/account_details.do"><em
								class="iconfont icon-jilu icontublb"></em> 账户明细记录 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<li><a href="${m}/upd_password.do?type=1" class="denglumima-btn"><em
							class="iconfont icon-21 icontublb"></em> 登陆密码修改 <em
							class="iconfont icon-right iconjtlb"></em></a></li>
					<c:if test="${iosExamine ne 'off' && isGuest != true }">
						<li><a href="${m}/upd_password.do?type=2" class="qukanmima-btn"><em
								class="iconfont icon-jilu01 icontublb"></em> 取款密码修改 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${isBackWater ne 'off'}">
						<li>
							<a href="#" onclick="location.href='${m}/backwater.do'" class="qukanmima-btn"><em
								class="iconfont icon-wenjianjia icontublb"></em> 反水记录<em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
					</c:if>
					<c:if test="${isExChgOnOff ne 'off' && isGuest != true }">
						<li><a href="${m}/scoreChange.do" class="qukanmima-btn"><em
								class="iconfont icon-bean-fill icontublb"></em> 积分兑换 <em
								class="iconfont icon-right iconjtlb"></em></a></li>
					</c:if>
					<c:if test="${loginMember.accountType eq 4}">
<!-- 					<hr style="height:3px;border:none;background:#4aa9db;">
 -->						<li style="border-top:5px solid #e4393c; ">
							<a href="#" onclick="location.href='${m}/info.do'" class="qukanmima-btn"><em
								class="iconfont icon-kefu5 icontublb"></em> 代理说明 <em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
						<li>
							<a href="#" onclick="location.href='${m}/teamStatic.do'" class="qukanmima-btn"><em
								class="iconfont icon-wenjianjia icontublb"></em> 团队统计 <em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
						<li>
							<a href="#" onclick="location.href='${m}/subMember.do'" class="qukanmima-btn"><em
								class="iconfont icon-user3 icontublb"></em> 会员管理 <em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
						<li>
							<a href="#" onclick="location.href='${m}/betting_record_lottery.do?come=daili'" class="qukanmima-btn"><em
								class="iconfont icon-caipiao icontublb"></em> 投注明细<em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
						<li>
							<a href="#" onclick="location.href='${m}/sponsoredLinks.do'"  class="qukanmima-btn"><em
								class="iconfont icon-iconwtbappwenjian icontublb"></em> 下级开户 <em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
						<li>
							<a href="#" onclick="location.href='${m}/commission.do'" class="qukanmima-btn"><em
								class="iconfont icon-shujuku icontublb"></em> 业绩提成 <em
								class="iconfont icon-right iconjtlb"></em>
							</a>
						</li>
					</c:if>
					
				</ul>

				<!-- 站内信 -->
				<ul class="userlb">
					<li><a href="${m }/website_message.do"> <em
							class="iconfont icon-email icontublb"></em> <span>我的站内信 <icon
									class="iconfont icon-yuanquan1"
									style="color: #cd0005; font-size: 0.95rem;position:relative;top:-0.1rem;"></icon>
								<span
								style="font-size: 0.65rem; position: relative; color: #ffffff; left: -0.95rem; top: -0.1rem;"
								id="zhanneixin">${indexMsgCount }</span></span> <em
							class="iconfont icon-right iconjtlb"></em></a></li>
				</ul>

			</div>

		</div>
	</div>
</body>

</html>
<jsp:include page="include/need_js.jsp"></jsp:include>
