<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="live_game_history">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a>
			<button class="button button-link button-nav pull-right"
				style="color: #fff;" id="showAndroidActionSheet">筛选</button>
			 <a class="title">真人投注记录</a>
		</header>
		<div class="content">
			<!-- 这里是页面内容区 -->

			<div class="content infinite-scroll infinite-scroll-bottom"
				data-distance="50">
				<div class="card hide" id="countData"
					style="margin: .5rem; background: #ececec;">
					<div class="card-content">
						<div class="card-content-inner"
							style="display: flex; text-align: center;"></div>
					</div>
				</div>
				<div class="list-block media-list inset"
					style="margin: 0 .5rem .5rem">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
			<div class="weui-skin_android" id="androidActionsheet"
				style="display: none">
				<div class="weui-mask"></div>
				<div class="weui-actionsheet">
					<div class="weui-actionsheet__menu">
						<div class="list-block" style="margin: 0;">
							<ul>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">开始</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${startTime}" max="${endTime}"
													class="changeData" style="font-size: .7rem; color: red;"
													name="start_time">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">截止</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${endTime}" max="${endTime}"
													style="font-size: .7rem; color: red;" name="end_time"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">平台类型</div>
											<div class="item-input" style="background: #FFF;">
												<select name="code" id="realType" class="changeData">
													<option value="0">所有记录</option>
													<option value="1">AG</option>
													<option value="2">BBIN</option>
													<option value="3">MG</option>
													<option value="5">ALLBET</option>
													<option value="7">OG</option>
													<option value="8">DS</option>
													<option value="12">KY</option>
													<option value="98">BG</option>
													<option value="97">VR</option>
												</select>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="content-block" style="padding: .5rem .75rem; margin: 0;">
							<div class="row">
								<div class="col-100">
									<a href="javascript:void(0);" id="live_game_btn"
										class="button button-big button-fill button-danger">确定</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<%-- <div class="page" id='liveGameHistoryDetail'>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${res}/sport/betting_record_sport.do">
				<span class="icon icon-left"></span> 返回
			</a>
			<a class='title'>真人投注记录</a>
		</header>
		<div class="content infinite-scroll infinite-scroll-bottom"
			data-distance="50">
			<div class="card hide" id="countData"
				style="margin: .5rem; background: #ececec;">
				<div class="card-content">
					<div class="card-content-inner"
						style="display: flex; text-align: center;"></div>
				</div>
			</div>
			<div class="list-block media-list inset" style="margin:0 .5rem .5rem">
				<ul class="list-container">
				</ul>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
	</div> --%>
	<script type="text/html" id="bettingSportDetail">
		<div class="shangqkaij tac">
			<span class="tac co000">订单号： <em class="hong">{{bettingCode}}</em></span>
			<div class="cl"></div>
		</div>
		<table class="lottery_detail" style="width:100%;">
			<tbody>
				<tr>
					<td class="bttd">会员</td><td>${loginMember.account}</td>
						<td class="bttd">单注金额</td><td class="last4">{{bettingMoney}}</td>
				</tr>
				<tr>
					<td class="bttd">下注时间</td><td>{{bettingDate}}</td>
					<td class="bttd">球类</td>
					<td>
						{{if gameTimeType == 1}}足球{{/if}}
						{{if gameTimeType == 2}}篮球{{/if}}
						{{if gameTimeType == 3}}其他{{/if}}
					</td>
				</tr>
				<tr>
					<td class="bttd">联赛</td><td>{{league}}</td>
					<td class="bttd">赔率</td><td>{{odds}}</td>
				</tr>
				<tr>
					<td class="bttd">比分</td><td>{{result}}</td>
					<td class="bttd">盘</td><td>{{plate}}</td>
				</tr>
				<tr>
					<td class="bttd">盈亏</td><td>dfs34</td>
				</tr>
				<tr>
					<td class="bttd">赛事</td>
					<td class="" colspan="3">
						<div style="width: 100%;font-size: .8rem; padding: .5rem;">{{remark}}</div>
					</td>
				</tr>
				
			</tbody>
		</table>
	</script>
	<script>
		function getMatchInfo() {
			if (order.mix != 2) {
				return toBetHtml(JSON.decode(order.remark));
			}
			var html = "";
			var arr = JSON.decode(order.remark)
			for (var i = 0; i < arr.length; i++) {
				if (i != 0) {
					html += "<div style='border-bottom:1px #303030 dotted;'></div>";
				}
				html += toBetHtml(arr[i]);
			}
			return html;
		}
	</script>
	<div class="popup popup-about" id="betRecordLotteryPopup">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-right close-popup">&nbsp;关闭</a>
			<a class="title">订单详情</a>
		</header>
		<div class="content" style="padding: 0; margin: 0;">
			<div class="content-inner">
				<div class="content-block betOrderDetail"
					style="margin: 0; padding: 0;">

					<div class="shangqkaij tac">
						<span class="tac co000">订单号： <em class="hong"
							id="bettingCode"></em></span>
						<div class="cl"></div>
					</div>

					<table class="lottery_detail" style="width: 100%;">
						<tbody>
							<tr>
								<td class="bttd">会员</td>
								<td>${loginMember.account}</td>
								<td class="bttd">下注金额</td>
								<td class="last4" id="bettingMoney"></td>
							</tr>
							<tr>
								<td class="bttd">下注时间</td>
								<td id="bettingDate"></td>
								<td class="bttd">球类</td>
								<td id="sportType"></td>
							</tr>
							<tr>
								<td class="bttd">类型</td>
								<td id="typeNames"></td>
								<td class="bttd">盘</td>
								<td id="plate"></td>
							</tr>
							<tr>
								<td class="bttd">提交状态</td>
								<td id="bettingStatus"></td>
								<td class="bttd">结算状态</td>
								<td id="balance"></td>
							</tr>
							<tr>
								<td class="bttd">派彩金额</td>
								<td id="bettingResult"></td>
							</tr>
							<tr>
								<td class="bttd">赛事</td>
								<td class="" colspan="3">
									<div id="matchInfo"></div>
								</td>
							</tr>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />