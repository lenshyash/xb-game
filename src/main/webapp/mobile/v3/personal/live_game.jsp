<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="liveGameZH">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a> <a class="title">真人额度转换</a>
		</header>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="card" id="countData"
				style="margin: .5rem; background: #ececec;">
				<div class="card-content">
					<div class="card-content-inner"
						style="display: flex; text-align: center;">
						<div style="flex-grow: 1;">
							<em>账户余额</em>
							<p style="color: red" id="account_money" money="${not empty loginMember.money?loginMember.money:0.00}">${not empty loginMember.money?loginMember.money:0.00}${cashName}</p>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${(isZrOnOff=='on' || isDzOnOff eq 'on' || isChessOnOff eq 'on' || isThdLotOnOff eq 'on' || isTsOnOff eq 'on') && isGuest != true}">
				<div class="list-block media-list" style="margin: 0;">
					<ul>
						<c:if test="${isKxOnOff eq 'on' && isGuest != true}">
							<li datatype="kx"><a href="#" class="item-link item-content">
								<div class="item-media">
									<img src="${base}/native/resources/images/KXDJ.png" style='width: 3rem;'>
								</div>
								<div class="item-inner">
									<div class="item-subtitle">凯旋电竞</div>
									<div class="item-title-row">
										<div class="item-title">
											<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
										</div>
										<div class="item-after">
											<font color="red" onclick="MobileIndexUtil.go('${base }/forwardKx.do?h5=1', 'kx','16',this,'hong');">进入游戏</font>
										</div>
									</div>
									<div class="item-text" data-type="kx" style="height: initial;">
										<em class="btn-kx">点击刷新&nbsp;&nbsp;</em>
										<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="kx">刷新</button>
									</div>
								</div>
							</a></li>
						</c:if>
						<c:if test="${isIbcOnOff eq 'on' && isGuest != true}">
							<li datatype="ibc"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${base }/mobile/anew/resource/new/images/Saba_logo.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">沙巴体育</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="javascript: MobileIndexUtil.go('${base}/forwardIbc.do?h5=1', 'ibc', '10', this);">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ibc" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ibc">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
					
						<c:if test="${(isAgOnOff eq 'on' || isAgDzOnOff eq 'on') && isGuest != true}">
							<li datatype="ag"><a href="javascript:void(0);" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/agreal.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">AG真人娱乐</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardAg.do?h5=1&gameType=11', 'ag','1',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ag" style="height: initial;">
											<em class="btn-ag">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ag">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						
						
						<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
							<li datatype="bbin"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/bbinreal.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">BBIN真人娱乐</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardBbin.do?type=live', 'bbin', '2',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="bbin" style="height: initial;">
											<em class="btn-bbin">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="bbin">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isVrOnOff eq 'on' && isGuest != true}">
							<li datatype="vr"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/vr.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">VR娱乐游戏</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardVr.do', 'vr', '97',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="vr" style="height: initial;">
											<em class="btn-vr">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="vr">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isKyOnOff eq 'on' && isGuest != true}">
							<li datatype="ky"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/ky.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">KY棋牌</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardKy.do', 'ky', '12',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ky" style="height: initial;">
											<em class="btn-ky">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ky">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isThOnOff eq 'on'}">
							<li datatype="th"><a href="#" class="item-link item-content">
								<div class="item-media">
									<img src="${res}/images/real/th.png" style='width: 3rem;'>
								</div>
								<div class="item-inner">
									<div class="item-subtitle">天豪棋牌</div>
									<div class="item-title-row">
										<div class="item-title">
											<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
										</div>
										<div class="item-after">
											<font color="red" onclick="MobileIndexUtil.go('${base }/forwardTh.do', 'th', '12',this,'hong');">进入游戏</font>
										</div>
									</div>
									<div class="item-text" data-type="th" style="height: initial;">
										<em class="btn-th">点击刷新&nbsp;&nbsp;</em>
										<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="th">刷新</button>
									</div>
								</div>
							</a></li>
						</c:if>
						<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
							<li datatype="bg"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/bg.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">BG真人娱乐</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardBg.do?type=2&h5=1', 'bg', '98',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="bg" style="height: initial;">
											<em class="btn-bbin">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="bg">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${(isMgOnOff eq 'on' || isMgDzOnOff eq 'on') && isGuest != true}">
							<li datatype="mg"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/mgreal.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">MG真人娱乐</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base}/forwardMg.do?gameType=1&gameid=59627&h5=1', 'mg', '3', this, 'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="mg" style="height: initial;">
											<em class="btn-mg">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="mg">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isAbOnOff eq 'on' && isGuest != true}">
							<li datatype="ab"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/ab.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">AB</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base}/forwardAb.do', 'ab', '5', this, 'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ab" style="height: initial;">
											<em class="btn-ab">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ab">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
							<li datatype="pt"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/pt.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">PT</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=pt'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="pt" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="pt">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isQtOnOff eq 'on' && isGuest != true}">
							<li datatype="qt"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/qt.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">QT</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=qt'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="qt" style="height: initial;">
											<em class="btn-qt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="qt">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isOgOnOff eq 'on' && isGuest != true}">
							<li datatype="og"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/og.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">OG</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;
												<font color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base}/forwardOg.do?gametype=mobile', 'og', '5', this, 'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="og" style="height: initial;">
											<em class="btn-og">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="og">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isDsOnOff eq 'on' && isGuest != true}">	
							<li datatype="ds"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/dszr.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">DS</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base}/forwardDs.do?h5=1', 'ds', '5', this, 'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ds" style="height: initial;">
											<em class="btn-ds">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ds">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isCq9OnOff eq 'on' && isGuest != true}">
							<li datatype="cq9"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/cq9.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">CQ9</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=cq9'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="cq9" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="cq9">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isJdbOnOff eq 'on' && isGuest != true}">
							<li datatype="jdb"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/jdb.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">JDB</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=jdb'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="jdb" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="jdb">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isMwOnOff eq 'on' && isGuest != true}">
							<li datatype="mw"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/mw.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">MW</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=mw'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="mw" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="mw">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isTtgOnOff eq 'on' && isGuest != true}">
							<li datatype="ttg"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/ttg.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">TTG</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=ttg'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ttg" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ttg">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isIsbOnOff eq 'on' && isGuest != true}">
							<li datatype="isb"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/isb.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">ISB</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="location.href='${m }/third/index.do?code=isb'">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="isb" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="isb">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isM8OnOff eq 'on' && isGuest != true}">
							<li datatype="m8"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${base }/mobile/anew/resource/new/images/hgsport.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">M8</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="javascript: go('${base}/forwardM8.do?h5=1', 'm8','99', this);">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="m8" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="m8">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isM8HOnOff eq 'on' && isGuest != true}">
							<li datatype="m8h"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${base }/mobile/anew/resource/new/images/hgsport.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">M8H</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="javascript: go('${base}/forwardM8H.do?h5=1', 'm8h','991', this);">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="m8h" style="height: initial;">
											<em class="btn-pt">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="m8h">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						<c:if test="${isEbetOnOff eq 'on' && isGuest != true}">
							<li datatype="ebet"><a href="#" class="item-link item-content">
									<div class="item-media">
										<img src="${res}/images/real/ebet.png" style='width: 3rem;'>
									</div>
									<div class="item-inner">
										<div class="item-subtitle">EBET真人娱乐</div>
										<div class="item-title-row">
											<div class="item-title">
												<font color="#057edb" id="zhuanchuBtn">转出</font>&nbsp;&nbsp;&nbsp;<font
												color="#057edb" id="zhuanruBtn">转入</font>
											</div>
											<div class="item-after">
												<font color="red" onclick="MobileIndexUtil.go('${base }/forwardEbet.do', 'ebet','13',this,'hong');">进入游戏</font>
											</div>
										</div>
										<div class="item-text" data-type="ebet" style="height: initial;">
											<em class="btn-ebet">点击刷新&nbsp;&nbsp;</em>
											<button class="button button-fill button-danger" style="height:1.2rem;line-height: 1.2rem;display: initial;" data-type="ebet">刷新</button>
										</div>
									</div>
							</a></li>
						</c:if>
						
					</ul>
				</div>
			</c:if>
		</div>
		<script type="text/html" id="resultLayerTpl">
		<div class="list-block" style="margin:1rem 0;">
			<ul><li class="item-content"><div class="item-inner"><div class="item-title">{{names}}金额：</div>
				<div class="item-input" style="width:50%;">
				<input style="height:1.5rem;" type="text" id="zhuanhuanMoney" onkeyup="this.value=this.value.replace(/\D/g,'')" /></div>
				</div></li>
			</ul>
		</div>
		</script>
		<script type="text/javascript">

		</script>
	</div>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />