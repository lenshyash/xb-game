<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="pagePersonalInfo">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${res}/personal_center.do"> <span class="icon icon-left"></span>
				返回
			</a>
			<a class="title">个人中心</a>
		</header>
		<style type="text/css">
			.list-block .item-title{width:30%;text-align: right;}
			.list-block .item-after{width:63%;}
		</style>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="list-block" style="margin: 0;">
				<ul>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">帐号:</div>
							<div class="item-after">${userInfo1['account']}</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">真实姓名:</div>
							<div class="item-after" id="userName" reg="/^[\u4E00-\u9FA5]+(·[\u4E00-\u9FA5]+)*$/">
								<c:if test="${empty userInfo1['userName']}">
									<p><a href="${res}/withdraw_money.do" class="button button-fill button-success">立即绑定</a></p>
								</c:if>
								<c:if test="${not empty userInfo1['userName']}">
									${userInfo1['userName'] }
								</c:if>
							</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">手机号码:</div>
							<div class="item-after" id="phone" reg="/^1\d{10}$/">${empty userInfo1['phone']?'<p><a href="#" class="button button-fill button-danger">立即绑定</a></p>':userInfo1['phone']}</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">电子邮箱:</div>
							<div class="item-after" id="email" reg="/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/">${empty userInfo1['email']?'<p><a href="#" class="button button-fill button-danger">立即绑定</a></p>':userInfo1['email']}</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">QQ:</div>
							<div class="item-after" id="qq" reg="/^[0-9]*$/">${empty userInfo1['qq']?'<p><a href="#" class="button button-fill button-danger">立即绑定</a></p>':userInfo1['qq']}</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">银行卡号:</div>
							<div class="item-after" id="cardNo" reg="/^\d{15,25}$/">
								<c:if test="${empty userInfo1['cardNo']}">
									<p><a href="${res}/withdraw_money.do" class="button button-fill button-success">立即绑定</a></p>
								</c:if>
								<c:if test="${not empty userInfo1['cardNo']}">
									${userInfo1['cardNo'] }
								</c:if>
							</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">开户行:</div>
							<div class="item-after" id="bankName">
								<c:if test="${empty userInfo1['bankName']}">
									<p><a href="${res}/withdraw_money.do" class="button button-fill button-success">立即绑定</a></p>
								</c:if>
								<c:if test="${not empty userInfo1['bankName']}">
									${userInfo1['bankName'] }
								</c:if>
							</div>
						</div>
					</li>
					<li class="item-content">
						<div class="item-inner">
							<div class="item-title">开户地址:</div>
							<div class="item-after" id="bankAddress">${empty userInfo1['bankAddress']?'<p><a href="#" class="button button-fill button-danger">立即绑定</a></p>':userInfo1['bankAddress']}</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<script type="text/html" id="bangdingTpl">
		<div class="list-block" style="margin:1rem 0;">
			<ul><li class="item-content"><div class="item-inner"><div class="item-title" style="width:initial;">{{names}}：</div>
				<div class="item-input" style="width:65%;">
				<input style="height:1.5rem;" type="text" id="bangdingValue" /></div>
				</div></li>
			</ul>
		</div>
	</script>
	</div>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />