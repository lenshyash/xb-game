<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <title>${_title }余额宝</title>
</head>
<body>
<style>
    *{
        overflow-x: hidden;
        margin: 0;
        padding: 0;
    }
    .main-body{
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        background: #f4f4f4;
        z-index: -2;
    }
    .main-body .go-out{
        width: 100%;
        height: 50px;
        z-index: 10;
        float: left;
        display: flex;
        align-items: center;
        padding-left: 10px;
        cursor: pointer;
        border-bottom: 1px solid #f3f3e9;
        background: #ffffff;
    }
    .main-body .go-out img{
        width: 15px;
    }
    .main-body .go-out span{
        font-size: 14px;
        color: #418ce2;
    }
    .main-body .operate-info {
        color: #9b9b9b;
        padding-left: 5%;
        height: 50px;
        line-height: 50px;
        background: #ffffff;
        width: 100%;
        float: left;
    }
    .main-body .operate-input {
        padding-left: 5%;
        height: 80px;
        line-height: 80px;
        background: #ffffff;
        width: 100%;
        float: left;
        border-bottom: 1px solid #f3f3e9;
        font-size: 24px;
        box-sizing: border-box;
    }
    .main-body .operate-input input[type=number]{
        height: 40px;
        width: 70%;
        font-size: 18px;
        border: none;
        -webkit-appearance : none ;
    }
    .main-body .operate-btn {
        padding-left: 5%;
        height: 80px;
        line-height: 80px;
        width: 100%;
        float: left;
        box-sizing: border-box;
    }
    .main-body .operate-btn input[type=button]{
        height: 47px;
        width: 90%;
        border: none;
        -webkit-appearance : none ;
        border-radius: 3px;
        background: darkgray;
        color: #ffffff;
    }
</style>
<div class="main-body">
    <div class="go-out">
        <img src="${m}/personal/images/yuebao/icon1.png" onclick="outLotteryVersion()">&nbsp;<span onclick="outLotteryVersion()">返回</span>
        <span style="color: #000000;font-weight: 600;display: block;margin: 0 auto;width: 140px;" id="yueTitle">转入余额宝</span>
    </div>
    <div class="operate-info">
        转入金额（元）
    </div>
    <div class="operate-input">
        <span style="font-size: 30px">￥</span>&nbsp;<input type="number" id="money">
        <span id="allMoney"  style="font-size:16px; color: #418ce2;" onclick="replaceMoney()">全部</span>
    </div>
    <div class="operate-btn">
        <input class="operate-info-button" type="button" value="确认转入" id='operate-btn-into' disabled="disabled">
        <input class="operate-info-button" type="button" value="确认转出" id='operate-btn-out' style="display: none" disabled="disabled">
    </div>
</div>
<div id="lotteryLoadImg" style="position: absolute;left:0;right:0;bottom:0;top:0;background: rgba(0,0,0,0.6);z-index:99999999;display: none;align-items: center;justify-content: center;">
    <img src="${res}/images/loading-1.gif"/>
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script>
    $("#money").bind("input propertychange",function(){
        if(!$(this).val()){
            $(".operate-info-button").css('background','darkgray').attr('disabled','disabled')
        } else {
            $(".operate-info-button").css('background','#418ce2').attr('disabled',false)
        }
    })
    var type,outMoney, webVersion = 0;
    $(function () {
        webVersion = Request['webVersion']
        if(Request['operate'] == 'out'){
            type = 2
            outMoney = Request['outMoney']
            $("#money").attr('placeholder','本次最多可转出' + outMoney + '元')
            $(".operate-info").html('转出金额（元）')
            $("#yueTitle").html('转出余额宝')
            $("#operate-btn-into").hide()
            $("#operate-btn-out").show()
            $("#operate-btn-out").click(function () {
                yueOperate()
            })
        } else {
            getMeminfo()
            type = 1
            $("#operate-btn-into").show()
            $("#operate-btn-out").hide()
            $("#operate-btn-into").click(function () {
                yueOperate()
            })
        }
    })
    function replaceMoney() {
        $("#money").val(outMoney)
        $(".operate-info-button").css('background','#418ce2').attr('disabled',false)
    }
    function yueOperate() {
        $("#lotteryLoadImg").css('display','flex')
        if(!$("#money").val()){
            alert('请输入金额')
            return false
        }
        $.ajax({
            url : '${m}/bgTransfer.do',
            data:{
                money: $("#money").val(),
                type: type
            },
            dataType:'json',
            type:'get',
            success:function(data){
                if(data.success){
                    alert('操作成功')
                    location.href='${m}/balanceGemInfo.do?webVersion=' + webVersion
                } else {
                    alert(data.msg)
                }
                $("#lotteryLoadImg").css('display','none')
            }
        })
    }
    function getMeminfo() {
        $.ajax({
            url : '${base}/meminfo.do',
            data:{},
            dataType:'json',
            type:'get',
            success:function(data){
                outMoney = data.money
                var index
                try {
                    index = outMoney.indexOf('.')
                    outMoney = outMoney.substring(0,index+3)
                } catch (e) {

                }
                $("#money").attr('placeholder','本次最多可转入' + outMoney + '元')
            }
        })
    }
    function outLotteryVersion() {
        location.href='${m}/balanceGemInfo.do?webVersion=' + webVersion
    }
    function GetRequest() {
        var url = location.search; //获取url中"?"符后的字串
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for(var i = 0; i < strs.length; i ++) {
                theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    }
    var Request = new Object();
    Request = GetRequest();

</script>
</body>
</html>