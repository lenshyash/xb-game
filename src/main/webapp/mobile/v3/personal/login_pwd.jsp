<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="content">
	<div class="list-block ico_margin" style="margin:0;">
		<ul>
			<li>
				<div class="item-content">
					<div class="item-media">
						<em class="icon1 icoModifyPas"></em>
					</div>
					<div class="item-inner">
						<div class="item-title label">输入旧密码</div>
						<div class="item-input">
							<input type="password" name="old_psd" placeholder="请输入旧密码"
								minlength="6" maxlength="20">
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="list-block ico_margin" style="margin:0;">
		<ul>
			<li>
				<div class="item-content">
					<div class="item-media">
						<em class="icon1 icoModifyPas"></em>
					</div>
					<div class="item-inner">
						<div class="item-title label">输入新密码</div>
						<div class="item-input">
							<input type="password" name="new_psd" placeholder="请输入新密码"
								minlength="6" maxlength="20">
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="item-content">
					<div class="item-media">
						<em class="icon1 icoModifyPas"></em>
					</div>
					<div class="item-inner">
						<div class="item-title label">重复新密码</div>
						<div class="item-input">
							<input type="password" name="new_psd2" placeholder="请再次输入新密码"
								minlength="6" maxlength="20">
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
			<div class="col-100">
				<a href="javascript:void(0);" id="btn"
					class="button button-big button-fill button-danger">提交</a>
			</div>
		</div>
	</div>
</div>