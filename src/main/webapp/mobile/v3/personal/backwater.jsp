<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="daili/head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<div id="app">
    <header class="top">
        <span>反水记录</span>
        <!---->
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back"></a>
    </header>
    <div class="user-main">
    <div class="textMore dataType dataTypea">
                <em>今天</em>
                <i class="iconfont"></i>
            </div>
    <div class="textMore dataType dataTypes" style="right:3rem;">
         <em>状态</em>
         <i class="iconfont"></i>
    </div>
        <table class="col3Table col3TableTitle">
            <tbody>
            <tr>
                <th>游戏类型</th>
                <th>时间</th>
                <th>金额</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </tbody>
        </table>
        <table class="ajaxContent col3Table col3TableCon">
            <tbody>
            	
            </tbody>
        </table>
        <div id="PageLoading" class="fullLoading" style="display: none;">
            <div class="loadingCell">
                <div class="iconLoadingCon">
                    <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                </div>
            </div>
        </div>
        <div  class="_problemBox _problemBoxa" style="display: none;">
            <div class="blackBg"></div>
                <div class="moreLayer moreLayera">
                    <ul>
                        <li><a value="0">今天</a></li>
                        <li><a value="1">昨天</a></li>
                        <li><a value="7">七天</a></li>
                    </ul>
                    <ul>
                        <li><a value="hide">取消</a></li>
                    </ul>
                </div>
        </div>
        <div  class="_problemBox _problemBoxs" style="display: none;">
            <div class="blackBg"></div>
                <div class="moreLayer moreLayers">
                    <ul>
                        <li><a value="">全部</a></li>
                        <li><a value="9">未领取</a></li>
                        <li><a value="4">已领取</a></li>
                    </ul>
                    <ul>
                        <li><a value="hide">取消</a></li>
                    </ul>
                </div>
        </div>
        <!---->
    </div>
    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
	var status = '';
	$('html').css('font-size',document.body.clientWidth/16)
	$(function(){
		$(".dataTypea").click(function(){
			$("._problemBoxa").show()
		})
		$(".dataTypes").click(function(){
			$("._problemBoxs").show()
		})
		$(".moreLayera li").click(function(){
			if($(this).find('a').attr('value') == 'hide'){
				$('._problemBoxa').hide();
				return false;
			}
            //更改选项
            let date = $(this).find('a').text()
            $(".dataTypea").find("em").text(date)

			$(".ajaxContent tbody").html('');
			today($(this).find('a').attr('value')) //日期
			initRdsData(1)
			$("._problemBoxa").hide();
		})
		$(".moreLayers li").click(function(){
			if($(this).find('a').attr('value') == 'hide'){
				$('._problemBoxs').hide();
				return false;
			}
            //更改选项
			let date = $(this).find('a').text();
			$(".dataTypes").find("em").text(date);

			$(".ajaxContent tbody").html('');
			status = $(this).find('a').attr('value')
			initRdsData(1)
			$("._problemBoxs").hide();
		})
		initRdsData(1)
		
	})
	function initRdsData(p){
			$("#PageLoading").show()
			$("#loadData").remove()
			if(!endTime){
				endTime = startTime
			}
			var data = {
				status:status,
				startTime:startTime+ " 00:00:00",
				endTime:endTime+ " 23:59:59",
				rows:10,
				page:p
			}
			$.ajax({
				url:"${base}/center/record/backwater/list.do",
				data:data,
				success:function(res){
					page = p;
					$("#PageLoading").hide()
					if(res.total == 0){
						//无数据
						var html = '<tr><td colspan="6">暂无数据</td></tr>';
						$(".ajaxContent tbody").html(html)
					}else{
						addTemplate(res);
					}
				}
			})
		}
	function addTemplate(res){
			var temp ="";
			$.each(res.rows,function(index, item){
				temp+='<tr><td>'+getFormat(item.betType,1)+'</td><td style="white-space:nowrap;">'+item.betDate+'</td><td>'+item.backwaterMoney+'</td><td>'+getFormat(item.backwaterStatus,2)+'</td><td style="color:red;">'+getFormat(item.backwaterStatus,3,item.id) +'</td></tr>'
			})
			if(res.rows.length <= 10){
				temp+='<tr><td colspan="4" class="msg loadingMsg">已显示全部数据</td></td>'
			}else{
				temp+='<tr id="loadData"><td colspan="4" class="msg loadingMsg"><a style="color:#4aa9db;" onclick="initRdsData('+(page+1)+')">点击加载更多数据</a></td>'
			}
			$(".ajaxContent tbody").append(temp);/* 
			$("#orderBetAmount").html(res.aggsData.totalBetMoney||0);
			$("#orderWinAmount").html(res.aggsData.totalBetResult||0); */
			//$("#orderBetOrderTblFoot").show();
		}
	function getFormat(value,t,rowId){
		if(t == 1){
			if(value==1){
				return "彩票";
			}else if(value==2){
				return "真人";
			}else if(value==3){
				return "电子";
			}else if(value==4){
				return "体育";
			}else if(value==5){
				return "六合特码B";
			}else if(value==6){
				return "六合正码B";
			}else if(value==11){
				return "彩票";
			}else if(value==15){
				return "十分六合彩特码B";
			}else if(value==16){
				return "十分六合彩正码B";
			}else if(value==99){
				return "三方体育";
			}else if(value==90){
			    return "棋牌"
            }else if (value == 91){
			    return "三方彩票"
            }else {
			    return "反水"
            }
		}else if(t == 2){
			if(value==4){
				return '已反水';
			}else if(value==9){
				return '待领取';
			}else if(value==8){
				return '已过期';
			}
		}else if(t == 3){
			if(value==9){
				return '<a href="#" onclick="cashing(\''+rowId+'\')">领取</a>';
			}else{
				return '';
			}
		}
	}
	function cashing(rowId){
		$.ajax({
			url:"${base}/center/record/backwater/cashing.do",
			data:{
				id:rowId
			},
			success:function(res){
				if(res.success){
					$(".ajaxContent tbody").html('');
					initRdsData(1);
				}else{
					alert(res.msg)
				}
			}
		})
	}
</script>
</body>
</html>