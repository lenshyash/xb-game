<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_billWeb_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a>
			<button class="button button-link button-nav pull-right"
				style="color: #fff;" id="showAndroidActionSheet">筛选</button>
			<a class="title">账户明细</a>
		</header>
		<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
		<div class="content">

			<div class="content infinite-scroll infinite-scroll-bottom"
				data-distance="50">
				<div class="card hide" id="countData"
					style="margin: .5rem; background: #ececec;">
					<div class="card-content">
						<div class="card-content-inner"
							style="display: flex; text-align: center;"></div>
					</div>
				</div>
				<div class="list-block media-list inset" style="margin-top: .5rem;">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>

			<div class="weui-skin_android" id="androidActionsheet"
				style="display: none">
				<div class="weui-mask"></div>
				<div class="weui-actionsheet">
					<div class="weui-actionsheet__menu">
						<div class="list-block" style="margin: 0;">
							<ul>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">开始时间</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${startTime }" max="${endTime }"
													name="start_time" style="font-size: .7rem; color: red;"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">截止时间</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${endTime }" max="${endTime }"
													name="end_time" style="font-size: .7rem; color: red;"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">账户类型</div>
											<div class="item-input" style="background: #FFF;">
												<select name="type" id="searchType" class="changeData">
													<option value="recharge">充值记录</option>
													<option value="withdraw">取款记录</option>
													<c:if test="${isZrOnOff == 'on'}">
														<%--<option value="thirdTransfer">真人转换记录</option> --%>
													</c:if>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li id="cz_mush">
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">交易类型</div>
											<div class="item-input" style="background: #FFF;">
												<select name="type" id="type" class="changeData">
													<option value="">全部</option>
													<option value="5">在线存款</option>
													<option value="6">快速入款</option>
													<option value="7">一般存款</option>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li id="cz_mushs">
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">处理状态</div>
											<div class="item-input" style="background: #FFF;">
												<select id="status" class="changeData">
													<option value="">全部</option>
													<option value="1">处理中</option>
													<option value="2">处理成功</option>
													<option value="3">处理失败</option>
												</select>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="content-block" style="padding: .5rem .75rem; margin: 0;">
							<div class="row">
								<div class="col-100">
									<a href="javascript:void(0);"
										class="button button-big button-fill button-danger">确定</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<%-- <div class="page" id='billsWeb'>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${res}/account_details.do"> <span class="icon icon-left"></span>
				返回
			</a> <a class='title'>充值记录</a>
		</header>
		<div class="content infinite-scroll infinite-scroll-bottom"
			data-distance="50">
			<div class="card hide" id="countData"
				style="margin: .5rem; background: #ececec;">
				<div class="card-content">
					<div class="card-content-inner"
						style="display: flex; text-align: center;"></div>
				</div>
			</div>
			<div class="list-block media-list inset" style="margin-top: .5rem;">
				<ul class="list-container">
				</ul>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
	</div> --%>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>