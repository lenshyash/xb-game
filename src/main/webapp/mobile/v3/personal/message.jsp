<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_gonggao_jsp">
		<!-- 这里是页面内容区 -->
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a> 
			<button class="button button-link button-nav pull-right" style="color:#fff;">编辑</button>
			<a class="title">站内信</a>
		</header>
		<nav class="bar bett-foot">
			<button id="batch_delete" class="btn-add">删除</button>
			<button id="all_checked" class="btn-none">全选</button>
			<button id="change_ok_reader" class="btn-none">已读</button>
		</nav>
		<div class="content" style="background: #FFF;">
			<div class="newsContent list-block media-list" style="margin: 0;">
				<ul class="record_list">
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			var pageDatas = [];
		</script>
	</div>
	<jsp:include page="../personal/popup/gonggao_popup.jsp"></jsp:include>
</body>
<jsp:include page="../include/need_js.jsp" />
</html>