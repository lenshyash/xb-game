<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_score_change_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>返回
<%--				href="#" onclick="goOut()"> <span class="icon icon-left"></span>返回--%>
			</a> <a class="title">积分兑换</a>
		</header>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="card" style="margin: 0; background: #f3f3f3;">
				<div class="card-header">
					<span
						style="width: 100%; text-align: center; display: inline-block;">当前账户：<font
						color="red">${loginMember.account}</font></span>
				</div>
			</div>
			<div class="list-block" style="margin: 0;">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-media"></div>
							<div class="item-inner">
								<div class="item-title label">账户余额：</div>
								<div class="item-input" id="nowMoney">${loginMember.money}</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media"></div>
							<div class="item-inner">
								<div class="item-title label">账户积分：</div>
								<div class="item-input" id="nowSocre">${change.score}</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media"></div>
							<div class="item-inner">
								<div class="item-title label">兑换类型：</div>
								<div class="item-input">
									<select style="border:1px solid #ddd;height:1.8rem;" id="exchangeType">
										<option selected="selected" value="">----请选择类型----</option>
										<option value="1">现金兑换积分</option>
										<option value="2">积分兑换现金</option>
									</select>
								</div>
							</div>
						</div>
					</li>
					<li id="duihuanSM" style="display: none;">
						<div class="item-content">
							<div class="item-media"></div>
							<div class="item-inner">
								<div class="item-input" style="font-size:.7rem;color:red;"></div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media"></div>
							<div class="item-inner">
								<div class="item-title label">兑换额度：</div>
								<div class="item-input"><input type="text" id="amount" name="amount" style="border: 1px solid #ddd;height:1.8rem;"></div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block">
				<div class="row">
					<div class="col-100">
						<a href="#" class="button button-big button-fill button-danger" id="scoreExchangeBtn">立即兑换</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var configs = ${configs};
		</script>
	</div>
	<script>
        function GetRequest() {
            var url = location.search; //获取url中"?"符后的字串
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for(var i = 0; i < strs.length; i ++) {
                    theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
        var Request = new Object();
        Request = GetRequest();
        function goOut(){
            if(Request['goOut'] == 'aicai'){
                location.href = '/mobile/index.do#/index3'
			} else {
				window.history.back()
			}
		}
	</script>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />