<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
	<div id="app">
    <div id="shadow" style="display: none;"></div>
    <header class="top">
        <span>代理报表</span>
        <!---->
        <a href="#" onclick="window.history.back()" class="iconfont back"></a>
    </header>
    <div class="user-main" style="padding-bottom: 0px;" s="[object Object]">
        <div>
            <div class="textMore dataType">
                <em id="inquiryDate">今天</em>
                <i class="iconfont"></i>
            </div>
            <div class="_problemBox" style="display: none;">
                <div class="blackBg"></div>
                <div class="moreLayer">
                    <ul>
                        <li><a value="0">今天</a></li>
                        <li><a value="1">昨天</a></li>
                        <li><a value="this">本月</a></li>
                        <li><a value="last">上月</a></li>
                    </ul>
                    <ul>
                        <li onclick="$('._problemBox').hide()"><a>取消</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="searchBtn">
            <div class="searchCon">
                <input type="text" placeholder="下级报表查询" id="account" class="user-input proxySearch" />
            </div>
            <a id="submitBtn">
                <div class="submitTouch" onclick="initRdsData(1)">
                    <i class="iconfont"></i>
                </div> </a>
        </div>
        <div id="PageLoading" class="fullLoadintextMore dataTypeg" style="display: none;">
            <div class="loadingCell">
                <div class="iconLoadingCon">
                    <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                </div>
            </div>
        </div>
        <div id="proxy3colCon" class="proxy3colCon" style="overflow: auto;">
            <ul class="PLSdetail fix" style="display: flex;flex-wrap: wrap;justify-content: center;align-items: center;">

            </ul>
        </div>
        <style>
            .proxy3colCon{
                height: auto;
            }
            .PLSdetail{
                width: 100%;
                transform: none;
                padding: 0;
            }
            .PLSdetail li{
                font-size: 0.7rem;
                line-height: 0.9rem;
                float: left;
                width: 33%;
                height: 5rem;
                padding: 1rem 0;
                text-align: center;

            }
            .PLSdetail li span{
                font-size: 0.7rem;
            }
        </style>
    </div>
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
	$('html').css('font-size',document.body.clientWidth/16)
	$(function(){
		$(".dataType").click(function(){
			$("._problemBox").show()
		})
		$(".moreLayer li").click(function(){
			today($(this).find('a').attr('value'))
            $("#inquiryDate").html($(this).find('a').html())
			initRdsData(1)
			$("._problemBox").hide();
		})
		initRdsData(1);
	})
			function initRdsData(p){
		    	$("#PageLoading").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
					account: $("#account").val(),
					orderId :'',
					type: 2,
					begin:startTime+'  00:00:00',
					end:endTime+'  23:59:59',
					pageSize:10,
					pageNumber:p
				}
				$.ajax({
					<%--url:"${base}/daili/dlreport/list.do",--%>
					url:"${base}/native/teamStaticNew.do",
					data:data,
					success:function(res){
				    	$("#PageLoading").hide()
						if(res.content == 0){
							console.log("没有数据")
						}else{
							addTemplate(res.content)
                            console.log()
						} 
					}
				})
			}
	function addTemplate(res){
        var html = ''
		for ( var  i in  res) {
		    html += '<li><span>¥'+unExistzero(res[i].switchDesc)+'</span>'+res[i].switchKey +'</li>'
        }
		$(".PLSdetail").html(html)
	}
	function unExistzero(un){
		if(typeof(un)=='undefined'){
			un = 0
		}
		return un;
	}
</script>
</body>
</html>