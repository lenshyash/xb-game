<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui" />
<meta name="apple-touch-fullscreen" content="yes" />
<meta name="mobile-web-app-capable" content="yes" />
<link href="${m }/personal/daili/css/app.62fd3aca29a64cfaa2d4b4baa8640c59.css" rel="stylesheet" />
<title>${_title }</title>
