<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<div id="app">
    <header class="top">
        <span>业绩提成</span>
        <!---->
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back"></a>
    </header>
    <div class="user-main">
    <div class="textMore dataType">
                <em>今天</em>
                <i class="iconfont"></i>
            </div>
        <table class="col3Table col3TableTitle">
            <tbody>
            <tr>
                <th>会员账号</th>
                <th>变动金额</th>
                <th>变动后余额</th>
                <th>变动时间</th>
            </tr>
            </tbody>
        </table>
        <table class="ajaxContent col3Table col3TableCon">
            <tbody>
            	
            </tbody>
        </table>
        <div id="PageLoading" class="fullLoading" style="display: none;">
            <div class="loadingCell">
                <div class="iconLoadingCon">
                    <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                </div>
            </div>
        </div>
        <div  class="_problemBox" style="display: none;">
            <div class="blackBg"></div>
                <div class="moreLayer">
                    <ul>
                        <li><a value="0">今天</a></li>
                        <li><a value="1">昨天</a></li>
                        <li><a value="this">本月</a></li>
                        <li><a value="last">上月</a></li>
                    </ul>
                    <ul>
                        <li onclick="$('._problemBox').hide()"><a>取消</a></li>
                    </ul>
                </div>
        </div>
        <!---->
    </div>
    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
	$('html').css('font-size',document.body.clientWidth/16)
	$(function(){
		$(".dataType").click(function(){
			$("._problemBox").show()
		})
		$(".moreLayer li").click(function(){
			$(".ajaxContent tbody").html('');
			today($(this).find('a').attr('value'))
			initRdsData(1)
			$("._problemBox").hide();
		})
		initRdsData(1)
		
	})
	function initRdsData(p){
			$("#PageLoading").show()
			$("#loadData").remove()
			if(!endTime){
				endTime = startTime
			}
			var data = {
				startTime:startTime+ " 00:00:00",
				endTime:endTime+ " 23:59:59",
				pageSize:10,
				pageNumber:p
			}
			$.ajax({
				url:"${base}/daili/dlfh/list.do",
				data:data,
				success:function(res){
					page = p;
					$("#PageLoading").hide()
					if(res.total == 0){
						//无数据
						var html = '<tr><td colspan="6">暂无数据</td></tr>';
						$(".ajaxContent tbody").html(html)
					}else{
						addTemplate(res);
					}
				}
			})
		}
	function addTemplate(res){
			var temp ="", bd=null,str='',name='';
			$.each(res.rows,function(index,item){
				str = item.remark.split(" ");
				$.each(str,function(index,item){
			        if(item.indexOf('投注人') >= 0){
			        	name = item.substring(4,(item.length-1))
			        }
			    })
				bd = new Date(item.bettingDate);
				temp += '<tr><td>'+item.account+'</td>';
				temp += '<td style="color:#4aa9db;">'+item.money+'</td>';
				temp += '<td style="color:#e4393c;">'+item.afterMoney+'</td>';
				temp += '<td>'+getMyDate(item.createDatetime)+'</td>';
			})
			if(res.rows.length < 10){
				temp+='<tr><td colspan="4" class="msg loadingMsg">已显示全部数据</td></td>'
			}else{
				temp+='<tr id="loadData"><td colspan="4" class="msg loadingMsg"><a style="color:#4aa9db;" onclick="initRdsData('+(page+1)+')">点击加载更多数据</a></td>'
			}
			$(".ajaxContent tbody").append(temp);/* 
			$("#orderBetAmount").html(res.aggsData.totalBetMoney||0);
			$("#orderWinAmount").html(res.aggsData.totalBetResult||0); */
			//$("#orderBetOrderTblFoot").show();
		}
</script>
</body>
</html>