<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<style>
    .creatAccountTitle input{
        margin-left: 20px;
        padding: 5px 12px;
        border: 0px;
        border-radius: 3px;
        background: #fff;
    }
    .back{
        background: rgb(220, 59, 64)!important;
        color:#fff;
    }
</style>
<div id="app">
<input id="zjId"  type="hidden" value="${link.id}"/>
    <input type="hidden" value="${domainFolder}" id="domainFolder"/>
		<input id="linkCode"  type="hidden" value="${link.linkKey}"/>
		<input id="linkType"  type="hidden" value="${link.type}"/>
    <header class="top">
        <span></span>
        <ul>
            <li class="on"><a >下级开户</a></li>
            <li class=""><a href="${m }/dailiLink.do">邀请码</a></li>
            <li class=""><a href="${m}/oddsTable.do">赔率表</a></li>
        </ul>
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back" style=""></a>
    </header>
    <div class="user-main creatAccount" style="padding-bottom: 0px;">
        <div class="creatAccountTitle radio">
            开户类型
            <c:choose>
                <c:when test="${domainFolder == 'd00557'}">
                    <c:if test="${trunOnoff ne 'on' }">
                        <input type="button" value="会员类型"  dvalue="2">
                    </c:if>
                    <input type="button" id="dailineix" value="代理类型" class="back" dvalue="1">
                </c:when>
                <c:otherwise>
                    <input type="button" id="dailineix" value="代理类型" class="back" dvalue="1">
                    <c:if test="${trunOnoff ne 'on' }">
                        <input type="button" value="会员类型"  dvalue="2">
                    </c:if>
                </c:otherwise>
            </c:choose>

            <c:if test="${linkDesc != ''}">
                <p >链接说明：${linkDesc }</p>
            </c:if>
            <p id="fandian">彩票返点数：(<span id="minRoll" style="color:#4aa9db">0</span>-<span id="getCpFds"style="color:#4aa9db">0</span>)</p>
            <p id="zhancheng">占成数：(<span id="minPS" style="color:#4aa9db">0</span>-<span id="maxPS"style="color:#4aa9db">0</span>)</p>
            <p id="dongtai">动态赔率点：(<span  id="minDR" style="color:#4aa9db">0</span>-<span id="maxDR" style="color:#4aa9db">0</span>)</p>
        </div>
		<div class="tabLI">
            <ul class="creatAccountDetail Backli">
                 <li><span>代理机制：</span><input type="texminRollt" class="form-control" disabled="disabled" id="dljz" value="${dljz}"/></li>
                 <li><span>生成链接：</span><input type="text" class="form-control" disabled="disabled" id="urlLink" value="${link.linkUrl}"/></li>
                 <li id="szfd">
                 	<span>设置返点：</span><input type="number" class="form-control" id="rollPoint" placeholder="请输入返点数!" value=""/>
                 	<input id="minFd" type="hidden" value="0">
					<input id="maxFd" type="hidden" value="">
                 </li>
                <li id="szzc" style="display: block">
                 	<span>设置占成：</span><input type="number" class="form-control" id="profitShare" placeholder="请输入占成数!" value=""/>
                 </li>
                <li id="dtplfd">
                    <span>动态赔率：</span><input type="number" class="form-control" id="dynamicRate" placeholder="请输入赔率点数!" value=""/>
                </li>
                 <li><span>状态：</span>
                 	<select id="status" class="form-control" value="${link.status}"  style="width: 60%;height:30px;padding-left:5px;">
						<option value="1">禁用</option>
						<option value="2" selected="selected">启用</option>
				</select>
                 </li>
            </ul>
            <a class="BTN submitBtn" onclick="creatLink()">生成邀请码</a>
        </div>
    </div>
    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
    var domainFolder = $("#domainFolder").val();
	var zjId = '';  //id
	var type = 1;   //代理类型
	var linkCode = '';  //推广码
	var rollPoint = '';  //返点
	var takePoint = '';  //占成
	var dynamicRate = ''; //赔率点
	var status = '';
	var maxFd = 0;
    $('html').css('font-size',document.body.clientWidth/16)
    $(function(){
        loading();
        changeType($(".creatAccountTitle input:eq(0)"));
        $(".creatAccountTitle input").click(function () {   changeType($(this))  })//开户类型选择，style
    })
    function changeType(that){
        console.log(that)
        that.siblings('input').removeClass('back');
        that.addClass('back');
        $("#rollPoint").val();
        $('#profitShare').val();
        $('#dynamicRate').val();
        type = that.attr('dvalue'); //开户类型的值 1为代理 2为会员
        if (type == 1  && "${isMulti}" == "true"){ //返点数
            $('#szfd,#fandian').show();
        }else {
            $('#szfd,#fandian').hide();
        }
        if ( agentRebateDuoji == 'off'){
            $('#szfd,#fandian').hide();
        }
        if (type == 1 && profitShareSwitch == 'on'){ //占成开关开启  返点数，占成数都显示
            $('#szzc,#zhancheng').show();
        }else{
            $('#szzc,#zhancheng').hide();
        }

        if ( type == 1 && '${memberRate}' == 'on'){//动态赔率开关
            $('#dtplfd,#dongtai').show();
        }else {
            $('#dtplfd,#dongtai').hide();
        }
        if (type == 2 && '${memberRate}' == 'on'){
            $('#dtplfd,#dongtai').show();
        }
        if (mergeRebateHebing == 'on'){   //多级代理返点赔率合并开关
            $('#dtplfd,#dongtai').show();
        }else {
            if (agentRebateDuoji == 'on' ){
                $('#szfd,#fandian').show();
            }
            if (dynamicRateDontai == 'on'){
                $('#dtplfd,#dongtai').show();
            }
        }
    }
    // 动态赔率 dynamicRate if on  dynamicRateMax dynamicRateMin
    // 多级代理 multiAgent  if on rebateMaxNum
    // 占成  profitShare if on profitShareMax
    // 合并开关 mergeRebate  if on  dynamicRateMax dynamicRateMin
    // 动态赔率 dynamicRate
    var profitShareSwitch = '';
    var multiAgentDuoji ='';
    var mergeRebateHebing='';
    var dynamicRateDontai ='';
    var agentRebateDuoji ='';

    function loading(){
			$.ajax({
				url : "${base}/daili/generalizeLink/multidata.do",
                async:false,
				success : function(j) {
				    profitShareSwitch = j.profitShare; //代理占成开关0
                    multiAgentDuoji = j.multiAgent;    //多级代理开关 多级代理或者一级代理
                    mergeRebateHebing = j.mergeRebate; //多级代理返点赔率合并开关 1 2
                    dynamicRateDontai = j.dynamicRate; //动态赔率开关 2
                    agentRebateDuoji = j.agentRebate;   //多级代理返点开关 2
                    var rebateMaxNum = j.rebateMaxNum;
                    $('#maxFd').val(rebateMaxNum); // 请输入返点数0-
                    $('#minRoll').html(j.rebateMinNum); //彩票返点数 -0
                    $('#getCpFds').html(rebateMaxNum); //彩票返点数 0- `
                    $('#minPS').html(j.profitShareMin); //占成数 最小
                    $('#maxPS').html(j.profitShareMax);	//占成数 最大
                    $('#minDR').html(j.dynamicRateMin);	//动态赔率返点 最小 1
                    $('#maxDR').html(j.dynamicRateMax); //动态赔率返点 最大
                }
			});
			$('#cpfds').show();
			if('${flag}'=='1'){
				$('#cpfds').show();
			}
		}

    function creatLink(){ //点击生成 传参数给后台
    	 zjId = $('#zjId').val();
		 linkCode = $('#linkCode').val();
		 rollPoint = ($('#rollPoint').val() == "" || $('#rollPoint').val() == 0 ) ?  0 : $('#rollPoint').val(); //返点
         takePoint =  ($('#profitShare').val() == "" || $('#profitShare').val() == 0 ) ?  0 : $('#profitShare').val();  //占成
         dynamicRate =  ($('#dynamicRate').val() == "" || $('#dynamicRate').val() == 0 ) ?  0 : $('#dynamicRate').val(); //动态赔率
		 status = $('#status').val();
            var minFd = $('#minRoll').html();
            var maxFd = $('#getCpFds').html();
            var maxZc = $('#minPS').html();
            var minZc = $('#maxPS').html();
            var minDR = $('#minDR').html();
            var maxDR = $('#maxDR').html();

            if (type == 1  && "${isMulti}" == "true" && (rollPoint == null )){ //返点数
                alert("彩票返点为必填项");
                return false;
                if (rollPoint *1  < minFd *1 ) {
                    alert("彩票返点数不能小于" + minFd + "!");
                    return false;
                }
                if (rollPoint*1  > maxFd*1 ) {
                    alert("彩票返点数不能大于" + maxFd + "!");
                    return false;
                }
            }
            if (type == 1 && profitShareSwitch == 'on' && (takePoint == null )){ //占成开关开启  返点数，占成数都显示
                alert("占成数为必填项");
                return false;
                if (takePoint*1  < minZc *1 ) {
                    alert("占成数不能小于" + minZc + "!");
                    return false;
                }
                if (takePoint*1  > maxZc *1 ) {
                    alert("占成数不能大于" + maxZc + "!");
                    return false;

                }
            }
            if ('${memberRate}' == 'on' && (dynamicRate == null )){  //动态赔率开关
                alert("动态赔率为必填项");
                return false;
                if (dynamicRate*1 < minDR *1){
                    alert("动态赔率不能小于"+ minDR + "!");
                    return false;
                }
                if (dynamicRate*1 > maxDR *1){
                    alert("动态赔率不能大于"+ maxDR + "!");
                    return false;
                }

            }




		var data = {
				id:zjId,
				type:type,
				linkKey:linkCode,
				cpRolling:rollPoint,
                profitShare:takePoint,
                status:status,
                dynamicRate:dynamicRate,
                generalizeType:type,
                linkCode:linkCode,
		};
		$.ajax({
			url : "${base}/daili/generalizeLink/updateOrSave.do",
			data : data,
			success : function(data) {
				if(data.success){
					location.href="${m }/dailiLink.do"
				}else{
					alert(data.msg);
				}
			}
		});
	}

</script>
</body>
</html>