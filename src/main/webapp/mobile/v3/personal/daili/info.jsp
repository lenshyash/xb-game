<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<div id="app">
    <header class="top">
        <span>代理说明</span>
        <!---->
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back"></a>
    </header>
    <div class="info-content">
       
    </div>
</div>
<style>
	.info-content{
		position:absolute;
		top:50px;left:0px;right:0px;bottom:0px;
		padding:10px;
	}
</style>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
	$('html').css('font-size',document.body.clientWidth/16)
	getCode24()
	function getCode24(){
		$.ajax({
			url : "${base}/getConfig/getArticle.do?code=24",
			data : {},
			success : function(data) {
				$(".info-content").html(data[0].content)
			}
		});
	}
</script>
</body>
</html>