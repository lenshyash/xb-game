<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<style>
    .creatAccountTitle input{
        margin-left: 20px;
        padding: 5px 12px;
        border: 0px;
        border-radius: 3px;
        background: #fff;
    }
    .back{
        background: rgb(220, 59, 64)!important;
        color:#fff;
    }
</style>
<div id="app">
    <input id="zjId"  type="hidden" value="${link.id}"/>
    <input id="linkCode"  type="hidden" value="${link.linkKey}"/>
    <input id="linkType"  type="hidden" value="${link.type}"/>
    <header class="top">
        <span></span>
        <ul>
            <li class=""><a href="${m }/sponsoredLinks.do">下级开户</a></li>
            <li class=""><a href="${m }/dailiLink.do">邀请码</a></li>
            <li class="on"><a >赔率表</a></li>
        </ul>
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back" style=""></a>
    </header>
    <div class="user-main creatAccount" style="padding-bottom: 0px;">
        <div class="GroupList">
            <span>彩种：</span>
            <select name="group" id="GroupList">

            </select>
        </div>
        <div class="oddsTable">
            <table border="1" id="oddsTable" style="border-color: #c3bdbd;">
                <thead>
                <tr>
                </tr>
                </thead>
                <tbody>
                <tr>
                </tr>
                </tbody>
            </table>
            <div id="PageLoading" class="fullLoading" style="display: none;">
                <div class="loadingCell">
                    <div class="iconLoadingCon">
                        <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
    $('html').css('font-size',document.body.clientWidth/16)
    $(function () {
        $.ajax({
            url:"${base}/native/getGroupList.do",
            data:'',
            type:"post",
            success:function (res) {
                if (res.success){
                    $.each(res.content,function (index,item) {
                        // console.log(index);
                        $("#GroupList").append("<option value='"+item.type+"'>"+ item.name +"</option>")
                    })
                }
            }
        })
        $("#GroupList").change(function () {
            let checkVal = $("#GroupList").val()
            reBateOddsByLot(checkVal)
        })
        reBateOddsByLot()

    })
    function reBateOddsByLot(lotType=8){
        $("#PageLoading").show()
        $.ajax({
            url:"${base}/native/getRebateOddsByLot.do.do",
            data:{lotType:lotType},
            type:"post",
            success:function (res) {
                // console.log(res)
                if (res.success){
                    if (res.content[0].items){
                        let arr = new Array();arr.length = 0

                        arr = res.content[0].items
                        let html = new Array();html.length = 0
                        html[0] += "<tr><td style='width: 3rem;display: block;border: none'>玩法/返点</td>"
                        $.each(arr,function (index,item) {
                            html[0]+= "<td>"+ item.num+"</td>"

                        })
                        html[0] +="</tr>"
                        $.each(res.content,function (index,item) {
                            html[1] += "<tr><td>"+ item.name+"</td>"
                            $.each(item.items,function (index,items) {
                                html[1] += "<td>"+ items.odds+"</td>"
                            })
                            html[1] +="</tr>"

                        })
                        $("#oddsTable thead tr").remove();
                        $("#oddsTable tbody tr").remove();
                        $("#oddsTable thead ").append(html[0])
                        $("#oddsTable tbody ").append(html[1])
                        $("#PageLoading").hide()
                    }else{
                        console.log("没有数据！")
                    }
                }else {
                    alert(res.msg)
                }
            }
        })
    }
</script>
</body>
</html>