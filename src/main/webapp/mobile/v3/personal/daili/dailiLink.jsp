<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp" />
</head>
<body style="font-size: 21.7589px;">
<style>
    .creatAccountTitle input{
        margin-left: 20px;
        padding: 5px 12px;
        border: 0px;
        border-radius: 3px;
        background: #fff;
    }
    .back{
        background: rgb(220, 59, 64)!important;
        color:#fff;
    }
</style>
<div id="app">
    <header class="top">
        <span></span>
        <ul>
            <li class=""><a href="${m }/sponsoredLinks.do">下级开户</a></li>
            <li class="on"><a>邀请码</a></li>
            <li class=""><a href="${m}/oddsTable.do">赔率表</a></li>
        </ul>
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back" style=""></a>
    </header>
    <div class="user-main">
        <div class="creatAccountTitle radio">
            开户类型
            <input type="button" value="代理类型" class="back" dvalue="1">
            <c:if test="${trunOnoff ne 'on' }">
                <input type="button" value="会员类型" dvalue="2">
            </c:if>
        </div>
        <table class="col3Table col3TableTitle"  style="width:100%;margin-top:70px;position: absolute;">
            <tbody>
            <tr>
                <th>邀请码</th>
                <th>返点</th>
                <th>推广链接</th>
                <th>操作</th>
            </tr>
            </tbody>
        </table>
        <table class="ajaxContent col3Table col3TableCon" style="margin-top:57px;">
            <tbody>
            <!--  <tr data-id="5516475" class="active">
                <td>
                    <ins style="color: rgb(51, 136, 255);">
                        ccc44400
                    </ins> </td>
                <td>1级玩家</td>
                <td>2018-10-26</td>
                <td style="color: red;">0</td>
            </tr>
           <tr>
                <td colspan="4" class="msg loadingMsg">已显示全部数据</td>
            </tr> -->
            </tbody>
        </table>
        <div id="PageLoading" class="fullLoading" style="display: none;">
            <div class="loadingCell">
                <div class="iconLoadingCon">
                    <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                </div>
            </div>
        </div>
        <!---->
    </div>

    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script src="${m }/personal/daili/js/clipboard.min.js"></script>
<script>
    $('html').css('font-size',document.body.clientWidth/16)
    var type = 1;
    $(function(){
        $(".creatAccountTitle input").click(function () {
            $(".ajaxContent tbody").html('');
            $(this).siblings('input').removeClass('back')
            $(this).addClass('back')
            type = $(this).attr('dvalue')
            initRdsData(1)
        })
        initRdsData(1)
    })
    function initRdsData(p){
        $("#PageLoading").show()
        $("#loadData").remove()
        var data = {
            type: type,
            pageSize:10,
            pageNumber:p,
            stationId: ''
        }
        $.ajax({
            url:"${base}/daili/generalizeLink/list.do",
            data:data,
            success:function(res){
                $("#PageLoading").hide()
                if(res.total == 0){
                    //无数据
                    var html = '';
                    html = '<tr><td colspan="6">暂无数据</td></tr>'
                    $(".ajaxContent tbody").html(html)
                }else{
                    addTemplate(res)
                }
            }
        })
    }
    function addTemplate(res){
        var temp ="", bd=null;
        $.each(res.rows,function(index,item){
            var linU = item.linkUrl;
            if(item.shortUrl1){
                linU = item.shortUrl1;
            }
            bd = new Date(item.bettingDate);
            temp += '<tr>';
            temp += '<td>'+item.linkKey+'</td>';
            temp += '<td>'+unExist(item.cpRolling)+'</td>';
            temp += '<td style="white-space:unset;"><input type="text" style="position:absolute;z-index:-2;" id="copy'+index+'" value="'+linU+'"/><a id="oncopy'+index+'" style=";color:#4aa9db;" onclick="copy(&quot;copy'+index+'&quot;)">复制</a></td>';
            temp += '<td>'+statusFormatter(item)+'&nbsp;&nbsp;<a onclick="del('+ item.id+')" href="javascript:void(0)"><span  style="color:red;">删除</span></a></td>';
            temp += '<tr><td colspan="4" style="background: #f3f3f3;text-align: left;">链接：' +linU + '</td></tr>'

        })
        if(res.rows.length < 10){
            temp+='<tr><td colspan="4" class="msg loadingMsg">已显示全部数据</td></td>'
        }else{
            temp+='<tr id="loadData"><td colspan="4" class="msg loadingMsg"><a style="color:#4aa9db;" onclick="initRdsData('+(page+1)+')">点击加载更多数据</a></td>'
        }
        $(".ajaxContent tbody").append(temp);
    }

    function statusFormatter(row){
        if(row.status==1){
            return '<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')" style="color:#4aa9db;" ><span id="status'+row.id+'" data-status="2"  style="color:#4aa9db;">启用</span></a>';
        }else{
            return '<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')"  style="color:red;"><span  id="status'+row.id+'" data-status="1"  style="color:red;">禁用</span></a>';
        }
    }
    function changeStatus(id){
        var status = $("#status"+id+"").attr("data-status");
        var txt="禁用成功";
        if(status==2){
            txt="启用成功";
        }
        $.ajax({
            url : "${base}/daili/generalizeLink/updateStatusById.do",
            data : {
                id : id,
                status:status
            },
            success : function() {
                alert(txt);
                window.location.reload();
            }
        });
    }
    function copy(id){
        //var Url2=document.getElementById(id);
        //Url2.select(); // 选择对象
        //document.execCommand("Copy"); // 执行浏览器复制命令
        //alert("已复制好，可贴粘。"); */
        $('#on'+id).attr('data-clipboard-target', '#'+id);
        var clipboard = new ClipboardJS ("#on"+id);
        clipboard.on('success', function (e) {
            if(e.text){
                alert('复制成功');
            }else{
                alert('复制失败');
            }
        });
    }
    function del(id) {
        var ok = confirm("是否确定删除？")
        if (ok==true) {
            $.ajax({
                url : "${base}/daili/generalizeLink/del.do",
                data : {
                    id : id
                },
                success : function() {
                    window.location.reload();
                }
            });
        }
    }
</script>
</body>
</html>