<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp" />
	<script type='text/javascript' src='${base}/mobile/v3/js/jquery-2.1.4.js' charset='utf-8'></script>
</head>
<body style="font-size: 21.7589px;">
<input type="hidden" value="${loginMember.account}" id="agentName">
<div id="app">
    <header class="top">
        <span>会员管理</span>
        <!---->
        <div class="iconfont back" style="display: none;"></div>
        <a href="#" onclick="window.history.back()" class="iconfont back"></a>
        
        <select name="code" id="code" class="changeData">
			<option value="1">直属下级</option>
			<option value="2">所有下级</option>
		</select>
		<script>
			function getCookie(name) { 
			    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
			 
			    if(arr=document.cookie.match(reg))
			 
			        return unescape(arr[2]); 
			    else 
			        return null; 
			} 

			
			$('#code').change(function(){
				var ss = $(this).children('option:selected').val();  
				if(ss == '1'){
					initRdsData(1, 1)
				}else if(ss == '2'){
					initRdsData(1, 2)
				}
			});
			var userName = getCookie('userName');
			
		</script>
		<style>
			select{
			    position: absolute;
			    right: 10px;
			    color: #fff;
			    height: 2.15rem;
			    -moz-box-sizing: border-box;
			    box-sizing: border-box;
			    -webkit-appearance: none;
			    padding: 0 0 0 .25rem;
			    margin: 0;
			    font-family: inherit;
			    font-size: .85rem;
			    background: 0 0;
			    border: none;
			    border-radius: 0;
			    box-shadow: none;
			    -moz-appearance: none;
			    -ms-appearance: none;
			    appearance: none;
			}
			
			select option{
				color:red;
			}
		</style>
    </header>
    <div class="user-main">
        <table class="col3Table col3TableTitle">
            <tbody>
            <tr>
                <th>账号</th>
                <th>用户类型</th>
                <th>登录时间</th>
                <th>操作</th>
            </tr>
            </tbody>
        </table>
        <table class="ajaxContent col3Table col3TableCon">
            <tbody>
            <!--  <tr data-id="5516475" class="active">
                <td>
                    <ins style="color: rgb(51, 136, 255);">
                        ccc44400
                    </ins> </td>
                <td>1级玩家</td>
                <td>2018-10-26</td>
                <td style="color: red;">0</td>
            </tr>
           <tr>
                <td colspan="4" class="msg loadingMsg">已显示全部数据</td>
            </tr> -->
            </tbody>
        </table>
<div id="PageLoading" class="fullLoading" style="display: none;">
            <div class="loadingCell">
                <div class="iconLoadingCon">
                    <img src="${m }/personal/daili/images/loadingMobile.gif" alt="" class="loadingImg" />
                </div>
            </div>
        </div>
        <!---->
    </div>
    <!---->
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script src="${m }/personal/daili/js/index.js"></script>
<script>
	$('html').css('font-size',document.body.clientWidth/16)
	$(function(){
		initRdsData(1,1)
	})
	function initRdsData(p,searchType,id,is){
		    	$("#PageLoading").show()
				var data = {
		    		searchType:searchType,
					account: '',
					accountType :0,
					rows : 100000,
					page : p
				}
		    	if(is == 0){
					data.agentParentId = id
				}else if(is == 1){
					data.agentId = id
				}
				$.ajax({
					url:"${base}/center/member/meminfo/accountList.do",
					data:data,
					success:function(res){
				    	$("#PageLoading").hide()
						if(res.totalCount == 0){
							//无数据
							var html = '';
							html = '<tr><td colspan="6">暂无下级,<a style="color:#4aa9db;" onclick="initRdsData(1)">查看所有会员</a></td></tr>'
							$(".ajaxContent tbody").html(html)
						}else{
							addTemplate(res)
						} 
					}
				})
			}
	function addTemplate(res){		
		var html='',agent='',name=$("#agentName").val();
		$.each(res.list,function(index, item){
			if(item.agentId != 0 && item.agentName !=name && item.accountType != 1){
				agent = '<a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.agentId+'&quot;,1)">上级</a>&nbsp;&nbsp;<a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.accountId+'&quot;,0)">下级</a>'
			}else if(item.agentId != 0 && item.agentName ==name && item.accountType != 1){
				agent = '<a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.accountId+'&quot;,0)">下级</a>'
			}else{
				agent = ''
			}
			if(item.account != userName){
				html+='<tr class="active"> <td><ins style="color: rgb(51, 136, 255);">'+item.account+'</ins> </td><td>'+userType[item.accountType]+'</td><td>'+getMyDate(item.createDatetime)+'</td><td style="color: red;">'+agent+'</td></tr>'
			}
			
		})
		html+='<tr><td colspan="4" class="msg loadingMsg">已显示全部数据,<a style="color:#4aa9db;" onclick="initRdsData(1)">查看所有会员</a></td></td>'
		$(".ajaxContent tbody").html(html)
	}
</script>
</body>
</html>