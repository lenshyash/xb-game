<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="lottery_bet_history">
		<input type="hidden" value="${isUnCancel}" id="isCancel">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a> 
			<button class="button button-link button-nav pull-right"
				style="color: #fff;" id="showAndroidActionSheet">筛选</button>
			<a class="title">六合彩投注记录</a>
		</header>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="content infinite-scroll infinite-scroll-bottom"
				data-distance="50">
				<div class="card hide" id="countData"
					style="margin: .5rem; background: #ececec;">
					<div class="card-content">
						<div class="card-content-inner"
							style="display: flex; text-align: center;"></div>
					</div>
				</div>
				<div class="list-block media-list inset"
					style="margin: 0 .5rem .5rem">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
			<div class="weui-skin_android" id="androidActionsheet"
				style="display: none">
				<div class="weui-mask"></div>
				<div class="weui-actionsheet">
					<div class="weui-actionsheet__menu">
						<div class="list-block" style="margin: 0;">
							<ul>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">开始</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${startTime}" max="${endTime}"
													class="changeData" style="font-size: .7rem; color: red;"
													name="start_time">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">截止</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${endTime}" max="${endTime}"
													style="font-size: .7rem; color: red;" name="end_time"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">彩种</div>
											<div class="item-input" style="background: #FFF;">
												<select name="code" id="code" class="changeData">
													<option value="LHC">六合彩</option>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">状态</div>
											<div class="item-input" style="background: #FFF;">
												<select name="status" id="status" class="changeData">
													<option value="">全部</option>
													<option value="1">未开奖</option>
													<option value="2">已中奖</option>
													<option value="3">未中奖</option>
													<option value="4">撤单</option>
												</select>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="content-block" style="padding: .5rem .75rem; margin: 0;">
							<div class="row">
								<div class="col-100">
									<a href="javascript:void(0);"
										class="button button-big button-fill button-danger">确定</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="lotteryOrderDetail">
		<div class="shangqkaij tac">
			<span class="tac co000">订单号： <em class="hong">{{orderId}}</em></span>
			<div class="cl"></div>
		</div>
		<table class="lottery_detail" style="width:100%;">
			<tbody>
				<tr>
					<td class="bttd">帐号</td><td>{{account}}</td>
					{{if version == 2 || version == 5 || lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC' || lotCode == 'WFLHC'|| lotCode == 'HKMHLHC'}}
						<td class="bttd">单注金额</td><td class="last4">{{(buyMoney/buyZhuShu).toFixed(2)}}</td>
					{{else}}
						<td class="bttd">单注金额</td><td class="last4">{{(2/model).toFixed(2)}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">下注时间</td><td>{{createTime}}</td>
					<td class="bttd">投注注数</td><td>{{buyZhuShu}}</td>
				</tr>
				<tr>
					<td class="bttd">彩种</td><td>{{lotName}}</td>
					<td class="bttd">倍数</td><td>{{multiple}}</td>
				</tr>
				<tr>
					<td class="bttd">期号</td><td>{{qiHao}}</td>
					<td class="bttd">投注总额</td><td>{{buyMoney.toFixed(2)}}</td>
				</tr>
				<tr>
					<td class="bttd">玩法</td><td>{{playName}}</td>
					{{if version == 2 || version == 5 || lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC' || lotCode == 'WFLHC'|| lotCode == 'HKMHLHC'}}
						<td class="bttd">赔率</td><td>{{odds}}</td>
					{{else}}
						<td class="bttd">奖金</td><td>{{odds}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">开奖号码</td><td>{{lotteryHaoMa}}</td>
					<td class="bttd">中奖注数</td><td>{{winZhuShu?winZhuShu:0}}</td>
				</tr>
				<tr>
					<td class="bttd">状态</td><td>{{$showStatus status}}</td>
					<td class="bttd">中奖金额</td><td>{{winMoney?winMoney.toFixed(2):0.00}}</td>
				</tr>
				<tr>
					{{if winMoney > 0 && winZhuShu > 0}}
						<td class="bttd">盈亏</td><td>{{(winMoney - buyMoney).toFixed(2)}}</td>
					{{/if}}
				</tr>
				<tr>
					<td class="bttd">投注号码</td>
					<td class="" colspan="3">
						<div style="width: 100%;font-size: .8rem; padding: .5rem;">{{haoMa}}</div>
					</td>
				</tr>
			</tbody>
		</table>
	</script>
	<jsp:include page="../personal/popup/betting_record_lottery_popup.jsp"></jsp:include>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />