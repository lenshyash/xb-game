<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_upd_pwd_jsp">
		<input type="hidden"  id="types" value="${type}"/>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${res}/personal_center.do"> <span class="icon icon-left"></span> 返回
			</a>
			<a class="title">${type == 1?'修改登录密码':'修改取款密码'}</a>
		</header>
		<%-- 这里是页面内容区 --%>
		<c:if test="${type == 1}">
			<jsp:include page="../personal/login_pwd.jsp"></jsp:include>
		</c:if>
		<c:if test="${type == 2}">
			<jsp:include page="../personal/draw_pwd.jsp"></jsp:include>
		</c:if>
	</div>
	<jsp:include page="../personal/popup/login_pwd_popup.jsp"></jsp:include>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>