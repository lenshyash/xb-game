<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <title>${_title }余额宝</title>
</head>
<body>
<style>
    *{
        overflow-x: hidden;
        margin: 0;
        padding: 0;
    }
    .main-body{
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        background: #f4f4f4;
        z-index: -2;
    }
    .main-body .head-background{
        width: 100%;
        height: 196px;
        background: #ff6633;
        position: absolute;
        top: 0;
        z-index: -1;
    }
    .main-body .go-out{
        width: 100%;
        height: 50px;
        z-index: 10;
        float: left;
        display: flex;
        align-items: center;
        padding-left: 10px;
        cursor: pointer;
    }
    .main-body .go-out img{
        width: 15px;
    }
    .main-body .go-out span{
        font-size: 14px;
        color: #fff;
    }
    .main-body .content{
        float: left;
        width: 100%;
        height: 340px;
        padding-left: 5%;
    }
    .main-body .content .content-radius{
        width: 90%;
        height: 100%;
        border-radius: 8px;
        background: #ffffff;
    }
    .main-body .content .content-radius p{
        text-align: center;
    }
    .main-body .content .content-radius .row{
        width: 100%;
        height: 30px;
        text-align: center;
        line-height: 30px;
    }
    .main-body .content .content-radius .row .lucre{
        height: 100%;background: #f1f1f1;border-radius: 20px;width: 140px;font-size: 13px;margin: 0 auto;
    }
    .main-body .content .content-radius .row span{
        color: orangered;
    }
    .main-body .content .content-radius .row .part{
        width: 50%;float: left;text-align: center;
    }
    .main-body .content .content-radius .row input[type=button]{
        width: 151px;height: 47px;border-radius: 3px;border: none;-webkit-appearance : none ;
    }
    .main-body .part-list{
        width: 100%;
        float: left;
        position: relative;
    }
    .main-body .part-list .classifyButotn{
        width: 60px;height: 26px;border: 1px solid #9c9c9c; border-radius: 20px;font-size: 13px;display: flex;justify-content: center;align-items: center;background: #fefefe;
        margin-top: 16px;float: left;
    }
    .main-body .part-list ul li{
        height: 60px;
        width: 100%;
        background: #ffffff;
        border-bottom: 1px solid #dddddd;
    }
    .part-list-left{
        float: left;margin-left: 5%;text-align: left;height: 40px;margin-top: 10px;
    }
    .part-list-right{
        float: right;margin-right: 5%;text-align: right;height: 40px;margin-top: 10px;
    }
    .moneyClassify{
        width: 100%;
        height: 120px;
        position: absolute;
        background: #ffffff;
        padding-left: 5%;
        padding-right: 5%;
        box-sizing: border-box;
        display: none;
        z-index: 9999;
    }
    .moneyClassify input[type=button]{
        width: 48%;height: 45px;margin-top: 10px;border: none;background: #f5f5f5;border-radius: 3px;-webkit-appearance : none ;
    }
    .moneyClassify input[type=button]:hover{
        background: #e4f1fb;
        color: #4691e2;
    }
</style>
<div class="main-body">
    <div class="head-background"></div>
    <div class="go-out"><img src="${m}/personal/images/yuebao/out.png" onclick="outLotteryVersion()">&nbsp;<span onclick="outLotteryVersion()">返回</span></div>
    <div class="content">
        <div class="content-radius">
            <div class="row"></div>
            <div class="row"  style="color: #333333;font-size: 13px;">总金额（元）</div>
            <div class="row" style="font-size: 20px;font-weight: 600" id="totalMoney"></div>
            <div style="height: 10px"></div>
            <div class="row"  style="color: #333333"><div class="lucre">昨日收益 <span id="zuorishouyi">0.00</span>元</div></div>
            <div class="row"></div>
            <div class="row"><div class="part">累计收益（元）</div> <div class="part">七日年化（%）</div></div>
            <div class="row"><div class="part" id="leijishouyi">0.00</div> <div class="part" id="jizhunnianhua">0.00</div></div>
            <div class="row"></div>
            <div class="row" style="height: 49px;">
                <div class="part"><input type="button" style="background: #fcf6ee;color: #ff6633" value="转出" id="outMoneyLink"></div>
                <div class="part"><input type="button" style="background: #ff6633;color: #fcf6ee" value="转入" id="infoMobeyLink"></div>
            </div>
        </div>
    </div>
    <div class="part-list">
        <div style="height: 60px;padding-left: 5%;">
            <div class="classifyButotn" id="lucre-switch"><span id="classifyTitle">全部</span> <img  style="width: 11px;margin-left: 5px" src="${m}/personal/images/yuebao/xiala.png"></div>
            <div style="float: right;margin-right: 5%;text-align: right;margin-top: 20px;">
                <span style="font-size: 12px;color: #888888;">显示最近30天记录</span>
                <%--<br>--%>
                <%--<span style="color: #ff6633;font-weight: 600" id="zongji">0.00</span>--%>
            </div>
        </div>
        <div class="moneyClassify">
            <input type="button" style="float: left;" value="全部" id="inquiry0" onclick="getYueTake('all')">
            <input type="button" style="float: right;" value="转入" id="inquiry2" onclick="getYueTake(2)">
            <input type="button" style="float: left;" value="收益" id="inquiry1" onclick="getYueTake(1)">
            <input type="button" style="float: right;" value="转出" id="inquiry3" onclick="getYueTake(3)">
        </div>
        <div style="width: 100%;min-height: 300px">
            <div id="cover" style="background: rgba(0,0,0,.5);height: 100%;width:100%;position: absolute;z-index: 10;display: none"></div>
            <ul id="yuebList">
            </ul>
            <div style="width: 100%;height: 80px;color: #888888;line-height: 40px;text-align: center;float: left;background: #f4f4f4" id="xialajiazai">下拉加载更多</div>
        </div>
    </div>
</div>
<div id="lotteryLoadImg" style="position: absolute;left:0;right:0;bottom:0;top:0;background: rgba(0,0,0,0.6);z-index:99999999;display: none;align-items: center;justify-content: center;">
    <img src="${res}/images/loading-1.gif"/>
</div>
<script src="${m }/js/jquery-2.1.4.js"></script>
<script>
$(function () {
    today(30)
    getYueTake()
    getTotalMoney()
    $("#lucre-switch").click(function () {
        $(".moneyClassify").toggle()
        $("#cover").toggle()
    })
    webVersion = Request['webVersion']
    $("#outMoneyLink").click(function () {
        location.href = '${m}/balanceGemRecord.do?operate=out&outMoney=' + outMoney + '&webVersion=' + webVersion
    })
    $("#infoMobeyLink").click(function () {
        location.href='${m}/balanceGemRecord.do?operate=into' + '&webVersion=' + webVersion
    })
})
var pageNum  = 1 , totalPageCount = 1, outMoney = 0 ,webVersion= 0 , startTime , endTime;
function getYueTake(type) {
    $("#lotteryLoadImg").css('display','flex')
    $(".moneyClassify").hide()
    $("#cover").hide()
    var html = '';
    if(type != 'append'){
        $("#yuebList").html('')
    }
    if(type == 'all' || type == 1 || type == 2 || type == 3 ){
        pageNum = 1
    }
    if(!type){
        type = ''
    }
    $("#classifyTitle").html(gettype(type))
    $.ajax({
        url : '${m}/bgMemberRecord.do',
        data:{
            page: pageNum,
            rows: 8,
            type: type,
            startTime: startTime,
            endTime: endTime
        },
        dataType:'json',
        type:'get',
        success:function(data){
            if(data.totalPageCount <= pageNum){
                $("#xialajiazai").html('暂无更多记录')
            }
            if(data.list.length > 0){
                $("#zongji").html('￥' + data.aggsData.totalMoney)
                totalPageCount = data.totalPageCount
                for(var i = 0; i < data.list.length; i++){
                    html += '<li> <div class="part-list-left"> ' +
                        '<span style="font-size: 12px">'+gettype(data.list[i].type)+'</span> <br>' +
                        ' <span style="color: #888888;">'+getMyDate(data.list[i].createDatetime)+'</span></div><div class="part-list-right">' +
                        '<span style="font-size: 12px">'+data.list[i].money+'</span><br> <span style="color: #888888;">'+data.list[i].backMoney+'</span>' +
                        ' </div></li>'
                }
                $("#yuebList").append(html)
            }
            $(window).scroll(function(){
                //获取滚动条滚动的位置
                if ($(document).scrollTop() >= $(document).height() - $(window).height()) {
                    pageNum ++
                    if(totalPageCount >= pageNum){
                        getYueTake('append')
                    }
                }
            })
            $("#lotteryLoadImg").css('display','none')
        }
    })
}
function getTotalMoney(){
    $("#lotteryLoadImg").css('display','flex')
    $.ajax({
        url : '${base}/center/bgem/balancegem/bgMemberInfo.do',
        data:{ },
        dataType:'json',
        type:'get',
        success:function(data){
            if(data.success){
                outMoney = data.content.bgMoney
                $("#totalMoney").html(data.content.bgMoney)
                $("#jizhunnianhua").html(data.content.strategy.benchmarkAnnualYield)
                $("#leijishouyi").html(data.content.bgIncome)
                $("#zuorishouyi").html(data.content.yesterdayIncome)
            }
        }
    })
    $("#lotteryLoadImg").css('display','none')
};
function getMyDate(str){
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth()+1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay);//最后拼接时间
    return oTime ;
};
function getzf(num){
    if(parseInt(num) < 10){
        num = '0'+num;
    }
    return num;
}
function gettype(num) {
    switch (num) {
        case 1:
            return '收益'
        case 2:
            return '转入'
        case 3:
            return '转出'
        case 'all':
            return '全部'
    }
}

function outLotteryVersion() {
    if(webVersion == 2){
        location.href = '${base}/mobile/index.do#/index3'
    } else{
        location.href='${m}'
    }
}
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function today(day){
    var year = new Date().getYear()+1900, month = new Date().getMonth()+1;
    var today = year + '-' +month +'-'+((new Date().getDate())-day);
    if(day == 7){
        startTime = year + '-' +month+'-'+((new Date().getDate())-day)
        endTime = year + '-' +month +'-'+((new Date().getDate()))
    }else if(day == 30){
        if((month - 1) == 0){
            month = 12
        } else {
            month --
        }
        startTime = year + '-' + month + '-' + (new Date().getDate())
        endTime = year + '-' +(month + 1) +'-'+((new Date().getDate()))
    }else if(day == 'this'){
        startTime = year + '-' +month +'-1'
        endTime = year + '-' +month +'-'+((new Date().getDate()))
    }else if(day == 'last'){
        startTime = year + '-' +(month-1) +'-1'
        endTime = year + '-' +(month-1) +'-'+(new Date(year,month-1,0).getDate())
    }else{
        startTime = today
        endTime = today
    }
    console.log(startTime + '...' + endTime)
}
var Request = new Object();
Request = GetRequest();

</script>
</body>
</html>