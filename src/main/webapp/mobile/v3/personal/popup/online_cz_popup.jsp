<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="czPopup">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-right close-popup"> 关闭 </a>
		<h1 class="title" style="color: white;">充值</h1>
	</header>
	<div class="content">
		<div class="content-inner">
			<div class="content-block" id="templateHtml" style="margin: 1rem 0 0;">
				
			</div>
		</div>
	</div>
</div>