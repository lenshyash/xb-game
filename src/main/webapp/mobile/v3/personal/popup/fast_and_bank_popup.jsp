<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="fast_bank_desc">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-right close-popup"> 返回充值页面 </a>
		<h1 class="title" style="color: white;">充值</h1>
	</header>
	<div class="content" id="fast_bank_desc_html">
		
	</div>
</div>
<script type="text/html" style="display: none;" id="fast_bank_desc_tmp">
	<div class="list-block fast-info">
		<ul>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值方式</div><div class="item-input">{{payName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">订单号</div><div class="item-input">{{orderNo}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{fast.payUserName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{fast.payAccount}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input" id="fast_pay_money"></div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">二&nbsp;&nbsp;维&nbsp;&nbsp;码</div><div class="item-input"><img src="{{fast.qrCodeImg}}" title="{{fast.qrCodeImg}}" style="margin: .5rem 0;width: 8rem; height: 8rem;display:inline-block;"></div></div></div>
			</li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
		<div class="col-50"><div class="button button-big button-fill button-danger" onclick="iosAnroidBrowser('index')">返回首页</div></div>
		<div class="col-50"><div class="button button-big button-fill button-success" onclick="iosAnroidBrowser('source')">查看充值记录</div></div>
		</div>
	</div>
</script>
<script type="text/html" style="display: none;" id="virtual_desc_tmp">
	<div class="list-block fast-info">
		<ul>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值方式</div><div class="item-input">{{payName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">订单号</div><div class="item-input">{{orderNo}}</div></div></div>
			</li>
<%--			<li><div class="item-content"><div class="item-media"></div>--%>
<%--				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{virtual.payUserName}}</div></div></div>--%>
<%--			</li>--%>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{virtual.payAccount}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input" id="virtual_pay_money"></div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">二&nbsp;&nbsp;维&nbsp;&nbsp;码</div><div class="item-input"><img src="{{virtual.qrCodeImg}}" title="{{virtual.qrCodeImg}}" style="margin: .5rem 0;width: 8rem; height: 8rem;display:inline-block;"></div></div></div>
			</li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
			<div class="col-50"><div class="button button-big button-fill button-danger" onclick="iosAnroidBrowser('index')">返回首页</div></div>
			<div class="col-50"><div class="button button-big button-fill button-success" onclick="iosAnroidBrowser('source')">查看充值记录</div></div>
		</div>
	</div>
</script>
<script type="text/html" style="display: none;" id="bank_desc_tmp">
	<div class="list-block fast-info">
		<ul>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值银行</div><div class="item-input">{{payName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">订单号</div><div class="item-input">{{orderNo}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{bank.creatorName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{bank.bankCard}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">开户网点</div><div class="item-input">{{bank.bankAddress}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input" id="bank_pay_money"></div></div></div>
			</li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
		<div class="col-50"><div class="button button-big button-fill button-danger" onclick="iosAnroidBrowser('index')">返回首页</div></div>
		<div class="col-50"><div class="button button-big button-fill button-success" onclick="iosAnroidBrowser('source')">查看充值记录</div></div>
		</div>
	</div>
</script>