<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="popup popup-about" id="betRecordLotteryPopup">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-right close-popup">&nbsp;关闭</a>
		<a class="title">订单详情</a>
	</header>
	<div class="content" style="padding: 0; margin: 0;">
		<div class="content-inner">
			<div class="content-block betOrderDetail"
				style="margin: 0; padding: 0;"></div>
		</div>
	</div>
</div>

