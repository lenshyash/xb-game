<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="loginPopup">
	<header class="bar bar-nav">
		<a class="title">${type == 1?'修改登录密码':'修改取款密码'}</a>
	</header>
	<div class="content">
		<div class="content-inner">
			<div class="content-block">
				<div class="openComplete">
					<img
						src="${base}/mobile/v3/images/ico-complete.png"
						style="display: inline-block;">
					<p>${type == 1?'修改登录密码':'修改取款密码'}成功</p>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="${res}" class="button button-big button-fill button-danger close-popup">返回首页</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>