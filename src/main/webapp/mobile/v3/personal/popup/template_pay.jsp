<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/html" style="display: none;" id="toOnlinePayTemplate">
<form action="{{formAction}}" method="post" id="onlinePayForm">
	{{each formHiddens as item}}
		<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}
	<div class="list-block">
		<ul>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">会员帐号</div><div class="item-input">{{account}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">订&nbsp;&nbsp;单&nbsp;&nbsp;号</div><div class="item-input">{{orderId}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">{{amount}}</div></div></div></li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
			<div class="col-100">
				<a href="javascript:void(0);" class="button button-big button-fill button-danger" onclick="onlineSubmit();">提&nbsp;&nbsp;交</a>
			</div>
		</div>
	</div>
</form>
</script>

<script type="text/html" style="display: none;" id="onoff_show_pay_quick">
	<div class="list-block czList">
		<ul>

			{{if iconCss == 'virtual'}}
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">钱包类型</div><div class="item-input">{{payUserName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款地址</div><div class="item-input">{{payAccount}}</div></div></div>
			</li>
			{{/if}}

			{{if iconCss != 'virtual'}}
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{payUserName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{payAccount}}</div></div></div>
			</li>
			{{/if}}

			{{if qrCodeImg}}
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">二&nbsp;&nbsp;维&nbsp;&nbsp;码</div><div class="item-input"><img onclick="mobileGetPayImgUrl('{{qrCodeImg}}')" src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="margin: .5rem 0;width: 8rem; height: 8rem;display:inline-block;"></div></div></div>
			</li>
			{{/if}}
			{{if iconCss == 'virtual'}}
			<li><div class="item-content"><div class="item-media"><em class="icon icoMoney"></em></div>
				<div class="item-inner"><div class="item-title label">{{walletType}}数量</div><div class="item-input">
				<input type="number" id="money" class="virtualMoney" placeholder="请输入金额" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
				</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">当日汇率</div><div class="item-input">
					1{{walletType}}≈{{rate}}RMB
				</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner">
					<div class="item-title label">约等于</div>
					<div class="item-input">
						<div id="allVirtual" exchangeRate="{{rate}}" style="display: inline-block;">0</div>
							<span>元</span>
						</div>
						<div class="buttonChangeVirtual" data_href="{{platformAddress }}" target="_blank">
							<span>兑换</span>
						</div>
					</div>
			</div>
			</li>
			{{/if}}
			{{if iconCss != 'virtual'}}
			<li><div class="item-content"><div class="item-media"><em class="icon icoMoney"></em></div>
				<div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">
					<input type="number" id="money" placeholder="请输入金额" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
				</div></div></div>
			</li>
			{{/if}}

			{{if frontLabel && iconCss != 'virtual'}}
			<li><div class="item-content"><div class="item-media"><em class="icon icoNickname"></em></div>
				<div class="item-inner"><div class="item-title label" id="flabel">{{frontLabel}}</div><div class="item-input">
				<input type="text" id="payAccountName" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;" placeholder="{{frontLabel}}">
				</div></div></div>
			</li>
			{{/if}}
		</ul>
	</div>
    {{if iconCss != 'virtual'}}
    <div class="card" style="background: #fbfbfb;">
        <div class="card-content">
            <div class="card-content-inner" style="color:red;">贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span style="color:blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</div>
        </div>
    </div>
    {{/if}}
    {{if iconCss == 'virtual'}}
        <div class="card" style="background: #fbfbfb;">
            <div class="card-content">
                <div class=""></div>
                <div class="card-content-inner" >
                    <dl>
                        <dt style="color:red;">贴心提醒：</dt>
						{{ if  qrcodeDesc == ' '}}
                        <dd style="color: #000;">1. 钱包链接类型请选择{{walletType}}否则将不可找回</dd>
                        <dd style="color: #000;">2. 确保转入金额不含转账手续费,否则无法到账</dd>
						{{/if}}
						{{ if  qrcodeDesc != ' '}}
						<dd style="color: #000;white-space: pre-wrap;" id="virtualQrcodeDesc" >{{qrcodeDesc}}</dd>
						{{/if}}
                    </dl>
                </div>
            </div>
        </div>
    {{/if}}
</script>


<script type="text/html" style="display: none;" id="onoff_show_pay_virtual">
	<div class="list-block czList" id="onoff_show_pay_virtuals">
		<ul>
<%--			<li><div class="item-content"><div class="item-media"></div>--%>
<%--				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{payUserName}}</div></div></div>--%>
<%--			</li>--%>
<%--			<li><div class="item-content"><div class="item-media"></div>--%>
<%--				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{payAccount}}</div></div></div>--%>
<%--			</li>--%>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">二&nbsp;&nbsp;维&nbsp;&nbsp;码</div><div class="item-input"><img onclick="mobileGetPayImgUrl('{{qrCodeImg}}')" src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="margin: .5rem 0;width: 8rem; height: 8rem;display:inline-block;"></div></div></div>
			</li>
			<li><div class="item-content" style="min-height: 1.2rem;"><div class="item-media"></div>
				<div class="item-inner" style="min-height: 1.2rem;padding:0;"><div class="item-title label">链类型</div><div class="item-input">
					<div>{{blockChainType}}</div>
			</li>
			<li><div class="item-content" style="min-height: 1.2rem;"><div class="item-media"></div>
				<div class="item-inner" style="min-height: 1.2rem;padding:0;"><div class="item-title label">参考汇率</div><div class="item-input">
				<div id="referenceRateVritual" data-values="{{referenceRate}}">1{{payName}}≈{{referenceRate}}RMB</div>
			</li>
			<li><div class="item-content" style="min-height: 1.2rem;"><div class="item-media"></div>
				<div class="item-inner" style="min-height: 1.2rem;padding:0;"><div class="item-title label">约等于</div><div class="item-input">
					<div id="allVirtual" style="display: inline-block;">0</div><span>元</span>
			</li>
			<li><div class="item-content" ><div class="item-media"><em class="icon icoMoney"></em></div>
				<div class="item-inner" ><div class="item-title label">充值数量</div><div class="item-input">
					<input type="number" id="money" onclick="virtualsNum()"  placeholder="请输入数量" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
				</div></div></div>
			</li>
<%--			{{if frontLabel}}--%>
<%--			<li><div class="item-content"><div class="item-media"><em class="icon icoNickname"></em></div>--%>
<%--				<div class="item-inner"><div class="item-title label" id="flabel">{{frontLabel}}</div><div class="item-input">--%>
<%--					<input type="text" id="payAccountName" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;" placeholder="{{frontLabel}}">--%>
<%--				</div></div></div>--%>
<%--			</li>--%>
<%--			{{/if}}--%>
		</ul>
	</div>
	<div class="card" style="background: #fbfbfb;">
		<div class="card-content">
			<div class="card-content-inner" style="color:red;padding: 0.25rem">
				<img src="{{tipsShowUrl}}" alt=""  onclick="window.open('{{tipsRedirectUrl}}')" style="height: 2.8rem;width: 2.8rem;border-radius: 50%;position: absolute;right: 10px;z-index: 2;">
			</div>
		</div>
	</div>
</script>
<script type="text/html" style="display: none;" id="onoff_show_pay_normal">
	<div class="list-block">
		<ul>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">充值银行</div><div class="item-input">{{title}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{creatorName}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{bankCard}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"></div>
				<div class="item-inner"><div class="item-title label">开户网点</div><div class="item-input">{{bankAddress}}</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"><em class="icon icoMoney"></em></div>
				<div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">
				<input type="number" id="money" placeholder="请输入金额" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
				</div></div></div>
			</li>
			<li><div class="item-content"><div class="item-media"><em class="icon icoNickname"></em></div>
				<div class="item-inner"><div class="item-title label">存款人姓名</div><div class="item-input">
				<input type="text" id="bankAccountName" placeholder="请填写真实姓名" style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
				</div></div></div>
			</li>
		</ul>
	</div>
	<div class="card" style="background: #fbfbfb;">
        <div class="card-content">
			<c:choose>
				<c:when test="${domainFolder == 'b167'}">
					<div  class="card-content-inner" style="color:red;">请使用微信、支付宝、网银转账至公司入款银行卡号，转账成功后联系客服提供存款人姓名、游戏账号上分。</div>
				</c:when>
				<c:otherwise>
					<div class="card-content-inner" style="color:red;">贴心提示：线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！</div>
				</c:otherwise>
			</c:choose>
        </div>
    </div>
</script>
<script>

	function mobileGetPayImgUrl(url){
		var browser = {
	            versions: function() {
	                var a = navigator.userAgent,
	                    b = navigator.appVersion;
	                return {
	                    trident: a.indexOf("Trident") > -1,
	                    presto: a.indexOf("Presto") > -1,
	                    webKit: a.indexOf("AppleWebKit") > -1,
	                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
	                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
	                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
	                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
	                    iPhone: a.indexOf("iPhone") > -1,
	                    iPad: a.indexOf("iPad") > -1,
	                    webApp: a.indexOf("Safari") == -1
	                }
	            }(),
	            language: (navigator.browserLanguage || navigator.language).toLowerCase()
	        };
		if (browser.versions.android) {
	         //Android
	         if(isLocaApp){
	        	 android.payImgUrl(url+'');
	         }
	     } else if (browser.versions.ios) {
	         //ios
	    	 if(isLocaApp){
	    		 WTK.share(JSON.stringify({'method':'payImgUrl','data':url}));	 
	         }
	     }
	}


</script>