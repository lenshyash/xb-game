<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../include/head.jsp" />
</head>
<body>
<div style="width:100%;height:100%;z-index:9999999;position: absolute;background: #fff;transition: .5s;margin-top: 1200px;" id="kyxiangqing">
    <div style="width:100%;height:40px;background:#ec2829;">
			<span style="display:block;margin:0 auto;width: 100px;text-align: center;height: 100%;line-height: 40px;color: #fff;">
				注单详情
			</span>
        <sapn style="position: absolute;top: 0px;color: #fff;display: block;width: 65px;height: 40px;text-align: center;line-height: 40px;" class="closeThis">关闭</sapn>
    </div>
    <table border="1" style="width: 100%;line-height: 44px;">
        <tr style="border-bottom: 2px solid #ec2829;">
            <th colspan="2">注单号：<span class="ebettingCode" style="color:#ec2829;">3daf79f7-a138-42f8-bcdd-7a237e6f16ac</span></th>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">注单账户</td>
            <td class="eaccount">gosick</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">投注时间</td>
            <td class="ebettingTime">1590744492000</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">游戏名称</td>
            <td class="egameName">英雄联盟</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">投注内容</td>
            <td class="einfo">2020 季中邀请赛-null</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">投注金额</td>
            <td class="ebettingMoney">10</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">有效投注</td>
            <td class="ebettingMoney">10</td>
        </tr>
        <tr style="border-bottom: 1px solid #c7c7c7;">
            <td style="background: rgba(226, 226, 226, 0.55);width:100px;">中奖金额</td>
            <td class="ewinMoney">28</td>
        </tr>
    </table>
    <script>
        $('.closeThis').click(function(){
            $('#kyxiangqing').css('margin-top',document.body.clientHeight+'px');
        });
    </script>
    <style>
        #kyxiangqing td{
            text-align:center;
            padding:  0 10px;
        }
    </style>
</div>
<div class="page" id="esport_history">
    <header class="bar bar-nav">
        <a class="button button-link button-nav pull-left back"
           href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
            返回
        </a>
        <button class="button button-link button-nav pull-right"
                style="color: #fff;" id="showAndroidActionSheet">筛选</button>
        <a class="title">电竞投注记录</a>
    </header>
    <div class="content">
        <!-- 这里是页面内容区 -->
        <div class="content infinite-scroll infinite-scroll-bottom"
             data-distance="50">
            <div class="card hide" id="countData"
                 style="margin: .5rem; background: #ececec;">
                <div class="card-content">
                    <div class="card-content-inner"
                         style="display: flex; text-align: center;"></div>
                </div>
            </div>
            <div class="list-block media-list inset"
                 style="margin: 0 .5rem .5rem">
                <ul class="list-container">
                </ul>
            </div>
            <div class="infinite-scroll-preloader">
                <div class="preloader"></div>
            </div>
        </div>

        <div class="weui-skin_android" id="androidActionsheet"
             style="display: none">
            <div class="weui-mask"></div>
            <div class="weui-actionsheet">
                <div class="weui-actionsheet__menu">
                    <div class="list-block" style="margin: 0;">
                        <ul>
                            <li>
                                <div class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title label">开始</div>
                                        <div class="item-input" style="background: #FFF;">
                                            <input type="date" value="${startTime}" max="${endTime}"
                                                   class="changeData" style="font-size: .7rem; color: red;"
                                                   name="start_time">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title label">截止</div>
                                        <div class="item-input" style="background: #FFF;">
                                            <input type="date" value="${endTime}" max="${endTime}"
                                                   style="font-size: .7rem; color: red;" name="end_time"
                                                   class="changeData">
                                        </div>
                                    </div>
                                </div>
                            </li>
<%--                            <li>--%>
<%--                                <div class="item-content">--%>
<%--                                    <div class="item-inner">--%>
<%--                                        <div class="item-title label">平台</div>--%>
<%--                                        <div class="item-input" style="background: #FFF;">--%>
<%--                                            <select name="code" id="classType" class="changeData">--%>
<%--                                                <option value="0">皇冠体育</option>--%>
<%--                                                <option value="1">沙巴体育</option>--%>
<%--                                                <option value="2">BB体育</option>--%>
<%--                                            </select>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </li>--%>
<%--                            <li class="hgSwitch">--%>
<%--                                <div class="item-content">--%>
<%--                                    <div class="item-inner">--%>
<%--                                        <div class="item-title label">球种</div>--%>
<%--                                        <div class="item-input" style="background: #FFF;">--%>
<%--                                            <select name="code" id="sportType" class="changeData">--%>
<%--                                                <option value="0">所有球种</option>--%>
<%--                                                <option value="1">足球</option>--%>
<%--                                                <option value="2">篮球</option>--%>
<%--                                            </select>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </li>--%>
<%--                            <li class="hgSwitch">--%>
<%--                                <div class="item-content">--%>
<%--                                    <div class="item-inner">--%>
<%--                                        <div class="item-title label">状态</div>--%>
<%--                                        <div class="item-input" style="background: #FFF;">--%>
<%--                                            <select name="status" id="recordType" class="changeData">--%>
<%--                                                <option value="1">全部</option>--%>
<%--                                                <option value="2">已中奖</option>--%>
<%--                                                <option value="3">未开奖</option>--%>
<%--                                                <option value="4">未成功</option>--%>
<%--                                            </select>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </li>--%>
                        </ul>
                        <script>
                            $("#classType").click(function(){
                                if($('#classType').val() == 0){
                                    $(".hgSwitch").show()
                                }else{
                                    $(".hgSwitch").hide()
                                }
                            })
                        </script>
                    </div>
                    <div class="content-block" style="padding: .5rem .75rem; margin: 0;">
                        <div class="row">
                            <div class="col-100">
                                <a href="javascript:void(0);" class="button button-big button-fill button-danger">确定</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getMatchInfo() {
        if (order.mix != 2) {
            return toBetHtml(JSON.decode(order.remark));
        }
        var html = "";
        var arr = JSON.decode(order.remark)
        for (var i = 0; i < arr.length; i++) {
            if (i != 0) {
                html += "<div style='border-bottom:1px #303030 dotted;'></div>";
            }
            html += toBetHtml(arr[i]);
        }
        return html;
    }
</script>
<div class="popup popup-about" id="betRecordLotteryPopup">
    <header class="bar bar-nav">
        <a class="button button-link button-nav pull-right close-popup">&nbsp;关闭</a>
        <a class="title">订单详情</a>
    </header>
    <div class="content" style="padding: 0; margin: 0;">
        <div class="content-inner">
            <div class="content-block betOrderDetail"
                 style="margin: 0; padding: 0;">

                <div class="shangqkaij tac">
						<span class="tac co000">订单号： <em class="hong"
                                                         id="bettingOrderCode"></em></span>
                    <div class="cl"></div>
                </div>

                <table class="lottery_detail" style="width: 100%;">
                    <tbody>
                    <tr>
                        <td class="bttd">会员</td>
                        <td>${loginMember.account}</td>
                        <td class="bttd">下注金额</td>
                        <td class="last4" id="bettingMoney"></td>
                    </tr>
                    <tr>
                        <td class="bttd">下注时间</td>
                        <td id="bettingDate"></td>
                        <td class="bttd">球类</td>
                        <td id="sportType"></td>
                    </tr>
                    <tr>
                        <td class="bttd">类型</td>
                        <td id="typeNames"></td>
                        <td class="bttd">盘</td>
                        <td id="plate"></td>
                    </tr>
                    <tr>
                        <td class="bttd">提交状态</td>
                        <td id="bettingStatus"></td>
                        <td class="bttd">结算状态</td>
                        <td id="balance"></td>
                    </tr>
                    <tr>
                        <td class="bttd">派彩金额</td>
                        <td id="bettingResult"></td>
                    </tr>
                    <tr>
                        <td class="bttd">赛事</td>
                        <td class="" colspan="3">
                            <div id="matchInfo"></div>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
2.脚本的class需要定义为：page_script；
3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />