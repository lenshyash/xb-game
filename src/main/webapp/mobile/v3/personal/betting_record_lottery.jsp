<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="lottery_bet_history">
		<input type="hidden" value="${isUnCancel}" id="isCancel">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
				返回
			</a>
			<button class="button button-link button-nav pull-right"
				style="color: #fff;" id="showAndroidActionSheet">筛选</button>
			<c:choose>
				<c:when test="${domainFolder== 'x002105'|| domainFolder == 'x002111'}">
					<a class="title">竞猜投注记录</a>
				</c:when>
				<c:when test="${domainFolder== 'x002128'}">
					<a class="title">竞猜记录</a>
				</c:when>
				<c:when test="${domainFolder== 'x00290'}">
					<a class="title">公益任务记录</a>
				</c:when>
				<c:when test="${domainFolder== 'x002117'}">
					<a class="title">助力投注记录</a>
				</c:when>
				<c:otherwise>
					<a class="title">彩票投注记录</a>
				</c:otherwise>
			</c:choose>
		</header>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="content infinite-scroll infinite-scroll-bottom"
				data-distance="50">
				<div class="card hide" id="countData"
					style="margin: .5rem; background: #ececec;">
					<div class="card-content">
						<div class="card-content-inner"
							style="display: flex; text-align: center;"></div>
					</div>
				</div>
				<div class="list-block media-list inset"
					style="margin: 0 .5rem .5rem">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>

			<div class="weui-skin_android" id="androidActionsheet"
				style="display: none">
				<div class="weui-mask"></div>
				<div class="weui-actionsheet">
					<div class="weui-actionsheet__menu">
						<div class="list-block" style="margin: 0;">
							<ul>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">开始</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${startTime}" max="${endTime}"
													class="changeData" style="font-size: .7rem; color: red;"
													name="start_time">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">截止</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${endTime}" max="${endTime}"
													style="font-size: .7rem; color: red;" name="end_time"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">彩种</div>
											<div class="item-input" style="background: #FFF;">
												<select name="code" id="code" class="changeData">
													<option value="">全部</option>
													<c:forEach items="${bl}" var="b" varStatus="bIndex">
														<c:if test="${b.code != 'LHC'}">
															<option value="${b.code}">${b.name}</option>
														</c:if>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">状态</div>
											<div class="item-input" style="background: #FFF;">
												<select name="status" id="status" class="changeData">
													<option value="">全部</option>
													<option value="1">未开奖</option>
													<option value="2">已中奖</option>
													<option value="3">未中奖</option>
													<option value="4">撤单</option>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li id="accountValue" style="display:none;">
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">账号</div>
											<div class="item-input" style="background: #FFF;">
												<input type="text"  value="" placeholder="请输入会员账号"  id="accountDaili" />
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="content-block"
							style="padding: .5rem .75rem; margin: 0;">
							<div class="row">
								<div class="col-100">
									<a href="javascript:void(0);"
										class="button button-big button-fill button-danger">查询</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../personal/popup/betting_record_lottery_popup.jsp"></jsp:include>
	<script>
		$(function(){
			if(Request['come'] == 'daili'){
				$("#accountValue").show()
				$(".title").text('投注明细')
			}
		})
	</script>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />