<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_bank_card_detail">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${res}/personal_center.do"> <span class="icon icon-left"></span>
				返回
			</a> <a class="title">绑定新的银行卡</a>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="card" style="margin:0 0 .05rem 0;">
				<div class="card-header card_bot" style="background: rgb(245, 245, 245);">
						<span style="width:100%;display:inline-block;text-align: center;">带<font color="red">*</font>号为必填项</span>
					</div>
			</div>
			<div class="list-block" style="margin: 0;">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>持&nbsp;&nbsp;卡&nbsp;&nbsp;人</div>
								<div class="item-input">
									<input type="text" name="cardName" id="cardName" placeholder="请输入持卡人姓名" value="${member.userName}">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>提款银行</div>
								<div class="item-input">
									<select name="bankName" id="bankName">
										<option value="建设银行">建设银行</option>
										<option value="工商银行" selected="selected">工商银行</option>
										<option value="农业银行">农业银行</option>
										<option value="中国邮政银行">中国邮政银行</option>
										<option value="中国银行">中国银行</option>
										<option value="中国招商银行">中国招商银行</option>
										<option value="中国交通银行">中国交通银行</option>
										<option value="中国民生银行">中国民生银行</option>
										<option value="中信银行">中信银行</option>
										<option value="中国兴业银行">中国兴业银行</option>
										<option value="浦发银行">浦发银行</option>
										<option value="平安银行">平安银行</option>
										<option value="华夏银行">华夏银行</option>
										<option value="广州银行">广州银行</option>
										<option value="BEA东亚银行">BEA东亚银行</option>
										<option value="广州农商银行">广州农商银行</option>
										<option value="顺德农商银行">顺德农商银行</option>
										<option value="北京银行">北京银行</option>
										<option value="杭州银行">杭州银行</option>
										<option value="温州银行">温州银行</option>
										<option value="上海农商银行">上海农商银行</option>
										<option value="中国光大银行">中国光大银行</option>
										<option value="渤海银行">渤海银行</option>
										<option value="浙商银行">浙商银行</option>
										<option value="晋商银行">晋商银行</option>
										<option value="汉口银行">汉口银行</option>
										<option value="上海银行">上海银行</option>
										<option value="广发银行">广发银行</option>
										<option value="深圳发展银行">深圳发展银行</option>
										<option value="东莞银行">东莞银行</option>
										<option value="宁波银行">宁波银行</option>
										<option value="南京银行">南京银行</option>
										<option value="北京农商银行">北京农商银行</option>
										<option value="重庆银行">重庆银行</option>
										<option value="广西农村信用社">广西农村信用社</option>
										<option value="吉林银行">吉林银行</option>
										<option value="江苏银行">江苏银行</option>
										<option value="成都银行">成都银行</option>
										<option value="尧都区农村信用联社">尧都区农村信用联社</option>
										<option value="浙江稠州商业银行">浙江稠州商业银行</option>
										<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
										<option value="other">其他</option>
									</select>
								</div>
							</div>
						</div>
					</li>
					<li style="display:none;" id="otherBank">
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>其他银行</div>
								<div class="item-input">
									<input type="text" name="otherBankName" id="otherBankName" placeholder="请输入银行名称">								
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>开户省份</div>
								<div class="item-input">
<!-- 									<input type="text" name="bankProvince" id="bankProvince" placeholder="请输入开户省份"> -->
									<select name='bankProvince' id="bankProvince">
								        <option value=''>请选择省份</option>
								    </select>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>开户城市</div>
								<div class="item-input">
<!-- 									<input type="text" name="bankCity" id="bankCity" placeholder="请输入开户城市"> -->
									<select name='bankCity' id="bankCity">
								        <option value=''>请选择城市</option>
								    </select>
								</div>
							</div>
						</div>
					</li>
					<script type='text/javascript' src='${base}/mobile/v3/js/area.js' charset='utf-8'></script>
					<script>
                        var province = '';
                        $(function() {
                            //城市联动
                            $.each(city, function(index, value) {
                                province += '<option value="' + value.name + '" index="' + index + '"> ' + value.name + '</option>'
                            });
                            $('select[name=bankProvince]').append(province).change(function() {
                                //$('select[name=city]').html('');
                                var option = '';
                                if ($(this).val() == '') {
                                    option += '<option value="">请选择</option>';
                                } else {
                                    var index = $(':selected', this).attr('index');
                                    var data = city[index].child;
                                    for (var i = 0; i < data.length; i++) {
                                        option += '<option value="' + data[i] + '">' + data[i] + '</option>';
                                    }
                                }
                                $('select[name=bankCity]').html(option);
                            })
                        })
					</script>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">&nbsp;开户网点</div>
								<div class="item-input">
									<input type="text" name="bankcardaddress" id="bankcardaddress" placeholder="请输入开户网点">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>银行卡号</div>
								<div class="item-input">
									<input type="number" name="cardNo" id="cardNo" placeholder="请输入卡号" value="${member.cardNo}">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label"><font color="red">*</font>提款密码</div>
								<div class="item-input">
									<input type="password" name="cashPassword" id="cashPassword" placeholder="请输入提款密码">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block">
				<div class="row">
					<div class="col-100">
						<a href="javascript:void(0);" id="upData"
							class="button button-big button-fill button-danger">下一步</a>
					</div>
				</div>
			</div>
		</div>
	</div
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>