<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_billsWebZB_jsp">
		<header class="bar bar-nav"> <a
			class="button button-link button-nav pull-left back"
			href="#" onclick="window.history.back()"> <span class="icon icon-left"></span>
			返回
		</a> 
		<button class="button button-link button-nav pull-right"
				style="color: #fff;" id="showAndroidActionSheet">筛选</button>
		<a class="title">账变记录</a> </header>
		<input id="domainFolder" value="${domainFolder }" type="hidden">
		<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
		<div class="content">
			<div class="content infinite-scroll infinite-scroll-bottom"
				data-distance="50">
				<div class="card hide" id="countData"
					style="margin: .5rem; background: #ececec;">
					<div class="card-content">
						<div class="card-content-inner"
							style="display: flex; text-align: center;"></div>
					</div>
				</div>
				<div class="list-block media-list inset" style="margin-top: .5rem;">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>

			<div class="weui-skin_android" id="androidActionsheet"
				style="display: none">
				<div class="weui-mask"></div>
				<div class="weui-actionsheet">
					<div class="weui-actionsheet__menu">
						<div class="list-block" style="margin: 0;">
							<ul>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">开始</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${startTime}" max="${endTime }"
													name="start_time" style="font-size: .7rem; color: red;"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">截止</div>
											<div class="item-input" style="background: #FFF;">
												<input type="date" value="${endTime}" max="${endTime}"
													name="end_time" style="font-size: .7rem; color: red;"
													class="changeData">
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="item-content">
										<div class="item-inner">
											<div class="item-title label">账变类型</div>
											<div class="item-input" style="background: #FFF;">
												<select id="moneyType" class="changeData">
													<option value="">全部</option>
													<c:forEach items="${typelist }"  var ="type"> 
														<option value="${type.type }">${type.name }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="content-block" style="padding: .5rem .75rem; margin: 0;">
							<div class="row">
								<div class="col-100">
									<a href="javascript:void(0);"
										class="button button-big button-fill button-danger">确定</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				var moneyType = ${moneyType};
			</script>
		</div>
	</div>
	<%-- <div class="page" id='billsWebZB'>
		<header class="bar bar-nav"> <a
			class="button button-link button-nav pull-left back"
			href="${res}/mnyrecord.do"> <span class="icon icon-left"></span>
			返回
		</a> <a class='title'>帐变记录</a> </header>
		<div class="content infinite-scroll infinite-scroll-bottom"
			data-distance="50">
			<div class="card hide" id="countData"
				style="margin: .5rem; background: #ececec;">
				<div class="card-content">
					<div class="card-content-inner"
						style="display: flex; text-align: center;"></div>
				</div>
			</div>
			<div class="list-block media-list inset" style="margin-top: .5rem;">
				<ul class="list-container">
				</ul>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
	</div> --%>
</body>
</html>
<jsp:include page="../include/need_js.jsp" />