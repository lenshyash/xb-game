<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_setUp_pwd_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
			   href="#" onclick="window.history.back()"> <span class="icon icon-left"></span> 返回
			</a>
			<a class="title">首次设置取款密码</a>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="firstSetPas">
				<dl>
					<dt>
						<em class="icon1 icoModifyPas"></em><span>请设置取款密码</span>
					</dt>
					<dd>
						<div class="pasFrame">
							<%-- <div class="dang"></div> --%>
							<input name="password" type="number" maxlength="6"
								class="inputPas" autocomplete="off">
							<ul>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>
						<em class="icon1 icoModifyPas"></em><span>再次输入取款密码</span>
					</dt>
					<dd>
						<div class="pasFrame">
							<%-- <div class="dang"></div> --%>
							<input name="password2" type="number" maxlength="6"
								class="inputPas" autocomplete="off">
							<ul>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
					</dd>
				</dl>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);" id="setBtn"
								class="button button-big button-fill button-danger">下一步</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>