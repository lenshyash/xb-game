<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../include/head.jsp" />
	<script type="text/javascript">
		var baseUrl = '${base}';
	</script>
</head>
<body>
<%--<input type="hidden" value="3,1,2" id="isPaymentSort">--%>
<input type="hidden" value="${isPaymentSort }" id="isPaymentSort">
<script>
	var isLocaApp = false;
	try{isLocaApp = android.isAndroidApp()}catch(err){}
	function iosAnroidBrowser(is) {
		var browser = {
			versions: function() {
				var a = navigator.userAgent,
						b = navigator.appVersion;
				return {
					trident: a.indexOf("Trident") > -1,
					presto: a.indexOf("Presto") > -1,
					webKit: a.indexOf("AppleWebKit") > -1,
					gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
					mobile: !!a.match(/AppleWebKit.*Mobile.*/),
					ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
					android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
					iPhone: a.indexOf("iPhone") > -1,
					iPad: a.indexOf("iPad") > -1,
					webApp: a.indexOf("Safari") == -1
				}
			}(),
			language: (navigator.browserLanguage || navigator.language).toLowerCase()
		};
		if (browser.versions.android) {
			//Android
			if(!isLocaApp){
				locaUrlIosAndroid(is)
			}else{
				if(is == 'center'){
					android.confirm('back');
				}else if(is == 'index'){
					android.confirm('homePage');
				}else if(is == 'source'){
					android.confirm('source');
				}
			}
		} else if (browser.versions.ios) {
			//ios
			if(!isLocaApp){
				locaUrlIosAndroid(is)
			}else{
				if(is == 'center'){
					WTK.share(JSON.stringify({"method": "back"}));
				}else if(is == 'index'){
					WTK.share(JSON.stringify({"method": "homePage"}));
				}else if(is == 'source'){
					WTK.share(JSON.stringify({"method": "source"}));
				}
			}
		} else {
			locaUrlIosAndroid(is)
		}
	}
	var picCallback = function(photos) {
		try{
			isLocaApp = photos
		}catch(err){}
	}
	function locaUrlIosAndroid(is){
		if (Request['goOut'] == 'haoYou'){
			location.href = "http://localhost:8089/#/recharge"
			// location.href = window.location.protocol+ '//'+window.location.host + '/#/recharge'
		}else if(is == 'center' || Request['goOut'] == 'aicai'){
			location.href = window.location.protocol+ '//'+window.location.host + '/mobile/index.do#/index3'
		}else if(is == 'index'){
			<%--location.href='${m}/personal_center.do'--%>
			window.history.back()
		}else if(is == 'source'){
			if(window.location.search){
				location.href = location.origin+'/mobile/#/jiaoyi'
			}else{
				location.href='${m}/account_details.do'
			}
		}
	}
	function GetRequest() {
		var url = location.search; //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
			strs = str.split("&");
			for(var i = 0; i < strs.length; i ++) {
				theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
			}
		}
		return theRequest;
	}
	var Request = new Object();
	Request = GetRequest();
	console.log(Request['goOut'])
</script>
<script src="${m }/personal/daili/js/clipboard.min.js"></script>
<div class="page" id="page_toRegChange_jsp">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-left back"
		   href="#" onclick="iosAnroidBrowser('index')"> <span class="icon icon-left"></span> 返回
		</a>
		<a class="title">充值</a>
	</header>
	<%-- 这里是页面内容区 --%>
	<link rel="stylesheet"
		  href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
	<div class="content">
		<div class="card">
			<div class="card-content">
				<div class="card-content-inner chongzhi_top">
					帐号：<em>${loginMember.account}</em>&nbsp;&nbsp;&nbsp;&nbsp; 余额：<em>${loginMember.money}元</em>
				</div>
			</div>
		</div>
		<%--		线上充值--%>
		<c:if test="${dptMap.get('onlineFlag') eq 'on'}">
			<div class="card" id="onlineFlagModule1">
				<div class="card-header card_bot"
					 style="background: rgb(245, 245, 245);">
					<em class="icon icoTop"></em><span>一般充值</span>
				</div>
				<div class="card-content">
					<div class="card-content-inner">
						<div class="list-block media-list">
							<ul class="radioClass">
								<c:choose>
									<c:when test="${onlineMap.size() == 0}">
										<li
												style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
									</c:when>
									<c:otherwise>
										<c:forEach items="${onlineMap}" var="line" varStatus="lines">
											<li><span  data-fixedFlag="${line.fixedFlag}" data-randomFlag="${line.randomFlag}" data-fixedAmount="${line.fixedAmount}" data-randomAmount="${line.randomAmount}"
													<c:choose>
														<c:when test="${not empty line.icon}">style="background: url('${line.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '1'}">style="background: url('${base}/common/template/lottery/jiebao/images/banks.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '5'}">style="background: url('${base}/common/template/lottery/jiebao/images/qqpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '6'}">style="background: url('${base}/common/template/lottery/jiebao/images/jdpay.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '7'}">style="background: url('${base}/common/template/lottery/jiebao/images/baidu.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '8'}">style="background: url('${base}/common/template/lottery/jiebao/images/union.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '9'}">style="background: url('${base}/common/template/lottery/jiebao/images/unionpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '11'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '12'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:otherwise>
															class="icons ${line.iconCss}"
														</c:otherwise>
													</c:choose>>${line.payName}</span>
												<input id="radio${line.id}" name="radio" type="radio"
													   value="${line.id}" min="${line.min}" max="${line.max}"
													   iconcss="${line.iconCss}" pays_type="${line.payType}"
													   pay_type="online"> <label for="radio${line.id}"><font
														style="font-size: 14px;">充值金额范围<strong
														style="color: red;">${line.min}</strong>-<strong
														style="color: red;">${line.max}</strong>元
												</font></label>
												<c:if test="${fn:length(line.payDesc) > 0}">
													<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${line.payDesc}">${line.payDesc}</marquee>
												</c:if>
											</li>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<%--		线下--%>
		<c:if test="${dptMap.get('fastFlag') eq 'on' || dptMap.get('bankFlag') eq 'on'}">
			<div class="card">
				<div class="card-header card_bot"
					 style="background: rgb(245, 245, 245);">
					<em class="icon icoBot"></em><span>快速充值</span>
				</div>
				<div class="card-content">
					<div class="card-content-inner">
						<div class="list-block media-list">
							<ul class="radioClass xxClass">
								<c:choose>
									<c:when test="${fastMap.size() == 0 && bankMap.size() == 0}">
										<li
												style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
									</c:when>
									<c:otherwise>
										<c:if test="${fastMap.size() > 0}">
											<c:forEach items="${fastMap}" var="fast" varStatus="fasts">
												<li>
													<div style="width: 100%;display: flex;height: 55px;">
													<span
															<c:choose>
																<c:when test="${not empty fast.icon}">style="background: url('${fast.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${fast.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${fast.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
																<c:otherwise>
																	class="icons ${fast.iconCss}"
																</c:otherwise>
															</c:choose>>${fast.payName}</span>
														<input id="radio${fast.id}" name="radio" type="radio"
															   value="${fast.id}" title="${fast.iconCss}" min="${fast.min}" max="${fast.max}"
															   flabel="${fast.frontLabel}" pay_type="fast"> <label
															for="radio${fast.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
															style="color: red;">${fast.min}</strong>元
													</font></label>
													</div>
													<c:if test="${fn:length(fast.qrcodeDesc) > 0}">
														<div style="padding-left: 8px;float: left;color: red">${fast.qrcodeDesc}</div>
													</c:if>
												</li>
											</c:forEach>
										</c:if>
										<c:if test="${bankMap.size() > 0}">
											<div class="card-header card_bot" style="background: rgb(245, 245, 245);">
												<em class="icon icon-bank"></em><span style="width: unset;height: unset;line-height: unset;">银行充值</span>
											</div>
											<c:forEach items="${bankMap}" var="bank" varStatus="banks">
												<li>
													<div style="width: 100%;display: flex;height: 55px;">
													<span title="${bank.payName}"
															<c:choose>
																<c:when test="${not empty bank.icon}">style="background: url('${bank.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${bank.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${bank.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
																<c:otherwise>
																	class="icons ${bank.iconCss}"
																</c:otherwise>
															</c:choose>>${bank.payName}</span>
														<input id="radio${bank.id}" name="radio" type="radio"
															   value="${bank.id}" min="${bank.min}" max="${bank.max}"
															   pay_type="bank"> <label for="radio${bank.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
															style="color: red;">${bank.min}</strong>元
													</font></label>
													</div>
													<c:if test="${fn:length(bank.bankDesc) > 0}">
														<div style="padding-left: 8px;float: left;width: 80%;color: red">${bank.bankDesc}</div>
													</c:if>
												</li>
											</c:forEach>
										</c:if>
									</c:otherwise>
								</c:choose>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<%--		虚拟货币--%>
		<c:if test="${dptMap.get('virtualFlag') eq 'on'}">
			<div class="card">
				<div class="card-header card_bot"
					 style="background: rgb(245, 245, 245);">
					<em class="icon icoBot"></em><span>虚拟货币充值</span>
				</div>
				<div class="card-content">
					<div class="card-content-inner">
						<div class="list-block media-list">
							<ul class="radioClass xxClass">
								<c:choose>
									<c:when test="${virtualMap.size() == 0 }">
										<li style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
									</c:when>
									<c:otherwise>
										<c:if test="${virtualMap.size() > 0}">
											<c:forEach items="${virtualMap}" var="virtual" varStatus="virtuals">
												<li>
													<div style="width: 100%;display: flex;height: 55px;">
													<span
															<c:choose>
																<c:when test="${not empty virtual.icon}">style="background: url('${virtual.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${virtual.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${virtual.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
																<c:otherwise>
																	class="icons ${virtual.iconCss}"
																</c:otherwise>
															</c:choose>>${virtual.payName}</span>
														<input id="radio${virtual.id}" name="radio" type="radio"
															   value="${virtual.id}" min="${virtual.min}" max="${virtual.max}"
															   flabel="${virtual.frontLabel}" iconCss="${virtual.iconCss}" pay_type="virtual" pays_type="${virtual.payType}"> <label
															for="radio${virtual.id}">
														<font
																style="font-size: 14px;">充值金额范围<strong
																style="color: red;">${virtual.min}</strong>-<strong
																style="color: red;">${virtual.max}</strong>元
														</font>
													</label>
													</div>
													<c:if test="${fn:length(virtual.qrcodeDesc) > 0}">
														<div style="padding-left: 8px;float: left;width: 100%;color: red">${virtual.qrcodeDesc}</div>
													</c:if>
												</li>
											</c:forEach>
										</c:if>
									</c:otherwise>
								</c:choose>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<%--		线上充值--%>
		<c:if test="${dptMap.get('onlineFlag') eq 'on'}">
			<div class="card" id="onlineFlagModule2">
				<div class="card-header card_bot"
					 style="background: rgb(245, 245, 245);">
					<em class="icon icoTop"></em><span>一般充值</span>
<%--					<em class="icon icoTop"></em><span>在线支付</span>--%>
				</div>
				<div class="card-content">
					<div class="card-content-inner">
						<div class="list-block media-list">
							<ul class="radioClass">
								<c:choose>
									<c:when test="${onlineMap.size() == 0}">
										<li
												style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
									</c:when>
									<c:otherwise>
										<c:forEach items="${onlineMap}" var="line" varStatus="lines">
											<li>
												<div style="width: 100%;display: flex;height: 55px;">
													<span  data-fixedFlag="${line.fixedFlag}" data-randomFlag="${line.randomFlag}" data-fixedAmount="${line.fixedAmount}" data-randomAmount="${line.randomAmount}"
															<c:choose>
																<c:when test="${not empty line.icon}">style="background: url('${line.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
																<c:when test="${line.payType == '1'}">style="background: url('${base}/common/template/lottery/jiebao/images/banks.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
																<c:when test="${line.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
																<c:when test="${line.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '5'}">style="background: url('${base}/common/template/lottery/jiebao/images/qqpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '6'}">style="background: url('${base}/common/template/lottery/jiebao/images/jdpay.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '7'}">style="background: url('${base}/common/template/lottery/jiebao/images/baidu.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '8'}">style="background: url('${base}/common/template/lottery/jiebao/images/union.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '9'}">style="background: url('${base}/common/template/lottery/jiebao/images/unionpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '11'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:when test="${line.payType == '12'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
																<c:otherwise>
																	class="icons ${line.iconCss}"
																</c:otherwise>
															</c:choose>>${line.payName}</span>
													<input id="radio${line.id}" name="radio" type="radio"
														   value="${line.id}" min="${line.min}" max="${line.max}"
														   iconcss="${line.iconCss}" pays_type="${line.payType}"
														   pay_type="online"> <label for="radio${line.id}"><font
														style="font-size: 14px;">充值金额范围<strong
														style="color: red;">${line.min}</strong>-<strong
														style="color: red;">${line.max}</strong>元
												</font></label>
												</div>
												<c:if test="${fn:length(line.payDesc) > 0}">
													<div style="padding-left: 8px;float: left;width: 80%;color: red">${fast.qrcodeDesc}</div>
												</c:if>
											</li>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<div class="list-block czList hide" id="online">
			<ul>
				<li>
					<div class="item-content">
						<div class="item-media">
							<em class="icon icoMoney"></em>
						</div>
						<div class="item-inner">
							<div class="item-title label">充值金额</div>
							<div class="item-input">
								<input type="number" id="money" placeholder="请输入金额"
									   style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
							</div>
						</div>
					</div>
					<div class="inputMoneyModule">

					</div>
				</li>
			</ul>
		</div>
		<style>
			.inputMoneyModule{
				min-height: 48px;
				width: 100%;
				text-align: center;
				display: none;
			}
			.inputMoneyModule button{
				width: 22%;
				margin: 3px 3px;
				border-radius: 5px;
				background: #ffffff;
				border: 1px solid #eeeeee;
				color: #000000;
			}
			.inputActive{
				background: #f6383a!important;
				color: #ffffff!important;
				border: 1px solid #f6383a!important;
			}
		</style>
		<div class="list-block czList hide" id="bank">
		</div>
		<div class="list-block czList hide" id="fast">
		</div>
		<div class="list-block czList hide" id="virtual">
			<ul>
				<li>
					<div class="item-content">
						<div class="item-media">
							<em class="icon icoMoney"></em>
						</div>
						<div class="item-inner">
							<div class="item-title label">充值金额</div>
							<div class="item-input">
								<input type="number" id="money" placeholder="请输入金额"
									   style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
							</div>
						</div>
					</div>
					<div class="inputMoneyModule">

					</div>
				</li>
			</ul>
		</div>
		<div class="card card-desc hide">
			<div class="card-content">
				<div class="card-content-inner"></div>
			</div>
		</div>
		<div class="content-block">
			<div class="row zf-btn">
				<div class="col-100">
					<a href="javascript:void(0);" id="btn"
					   class="button button-big button-fill button-danger">下一步</a>
				</div>
			</div>
		</div>
	</div>
	<script>
		var onlineHtml = $("#onlineFlagModule").html()
		var isPaymentSort = $("#isPaymentSort").val()
		try {
			if(isPaymentSort[0] == 2 || isPaymentSort[0] == 3){
				$("#onlineFlagModule1").remove()
			} else {
				$("#onlineFlagModule2").remove()
			}
		} catch (e) {

		}
	</script>
	<div style="display:none;" id="onlineDesc">${empty dptMap.get('onlineDesc')?'及时自动到账，推荐':dptMap.get('onlineDesc')}</div>
	<div style="display:none;" id="fastDesc">${empty dptMap.get('fastDesc')?'支持微信/支付宝二维码扫描':dptMap.get('fastDesc')}</div>
	<div style="display:none;" id="bankDesc">${empty dptMap.get('bankDesc')?'支持网银转账，ATM转账，银行柜台汇款':dptMap.get('bankDesc')}</div>
	<div style="display:none;" id="virtualDesc">${empty dptMap.get('virtualDesc ')?'支持虚拟货币转账':dptMap.get('virtualDesc ')}</div>
	<jsp:include page="../personal/popup/online_cz_popup.jsp"></jsp:include>
	<jsp:include page="../personal/popup/fast_and_bank_popup.jsp"></jsp:include>
	<jsp:include page="../personal/popup/template_pay.jsp"></jsp:include>
	<script type="text/javascript" src="${base}/common/js/onlinepay/pay.js?v=6.2673"></script>
	<script type="text/javascript" src="${res}/js/pay_change.js?v=1.213"></script>
	<script type="text/javascript">
		var hostUrl1="${hostUrl1}";
		var baseUrl = '${base}';

		function onlineSubmit(){
			$("#onlinePayForm").submit();
		}
	</script>
</div>
<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>