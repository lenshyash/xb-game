﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">    
<title>${_title }_签到</title>
<link rel="stylesheet" href="${base }/mobile/anew/signIn/css/bootstrap3/css/bootstrap.min.css">
<link rel="stylesheet" href="${base }/mobile/anew/signIn/css/style.css">
<link rel="stylesheet" href="${base }/mobile/anew/signIn/css/qiandao_style.css">
</head>
<body>
<div class="container">
<input type="button" value="签到规则" style="position:absolute;z-index:99999;top:20px;right:8px;border-radius: 8px;border:0;padding:5px 8px;display:none;" id="qdbutton">
	<div id="qdmodule" style="display:flex;position: fixed;top:0;left:0;bottom:0;right:0;background:rgba(0,0,0,0.3);z-index:100000;display:none;overflow: auto;">
		<img id="closeqd" src="${base}/common/qd/images/close_white.png" style="width:40px;position:absolute;right:5px;top:5px;z-index:9999999;" />
		<div id="qdneirong" style="width:100%;height:100%;display: flex;align-items: center;justify-content: center;">
			
		</div>
	</div>
	<style>
		#qdneirong img{
			width:100%;
		}
	</style>
	<div class="row">
		<div class="col-xs-12 clearPadding">
			<div class=""><img src="${base }/mobile/anew/signIn/images/qdBanner.jpg" class="img-responsive"></div>
			
			<div class="calendar">
				<div class="calenbox">
					<div id="calendar"></div>
				</div>
				<div class="text-center">
					<c:if test="${signed }"><button class="btn btn-qiandao-sign">已签到</button></c:if>
					<c:if test="${!signed }"><button class="btn btn-qiandao" onClick="signIn()">马上签到</button></c:if>
					
				</div>
			</div>
			<div class="padding10">
				<div class="font16 pb20 borderb">
					<strong>已连续签到${signCount }天</strong>
					<strong class="pull-right"><a href="#" class="btn btn-lingqu text-white" onclick="iosAndroidQd()" style="background-color: red;">返回首页</a></strong>
				</div>
				<script>
				var isLocaApp = false;
				try{isLocaApp = android.isAndroidApp()}catch(err){}
				function iosAndroidQd() {
				    var browser = {
				            versions: function() {
				                var a = navigator.userAgent,
				                    b = navigator.appVersion;
				                return {
				                    trident: a.indexOf("Trident") > -1,
				                    presto: a.indexOf("Presto") > -1,
				                    webKit: a.indexOf("AppleWebKit") > -1,
				                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
				                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
				                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
				                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
				                    iPhone: a.indexOf("iPhone") > -1,
				                    iPad: a.indexOf("iPad") > -1,
				                    webApp: a.indexOf("Safari") == -1
				                }
				            }(),
				            language: (navigator.browserLanguage || navigator.language).toLowerCase()
				        };
				    
					 if (browser.versions.android) {
				         //Android
				         if(!isLocaApp){
				        	 location.href= '${base}/index.do'
				         }else{
				        	 android.webHome('homePage');
				         }
				     } else if (browser.versions.ios) {
				         //ios
				    	 if(!isLocaApp){
				    		 location.href= '${base}/index.do'
				         }else{
				        	 WTK.share(JSON.stringify({'method':'homePage'}));	 
				         }
				     }else{
				    	 
// 				    	 location.href= '${m }/index.do'
				    	 window.history.back();
				     }
				}
				var picCallback = function(photos) {
					try{
						isLocaApp = photos
					}catch(err){}
				}
				</script>
				<!-- 
				<div class="libaolist">
				 
					<div class="clearfix borderb ptb10">
						<div class="col-md-12 clearPadding">
							<div class="media">
								<a class="media-left pt3" href="javascript:void(0);">
									<img src="${base }/mobile/anew/signIn/images/dou.png" style="width:30px;height:30px;">
								</a>
								<div class="media-body">
									<div>签到规则:</div>
									<div class="text-muted font12">这是签到规则!</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</div>
<div class="maskbox"></div>

<div class="qdbox">
	<div class="text-center text-green font18 sign-status"><strong>签到成功！</strong></div>
	<div class="text-center pt10">您已经连续签到&nbsp;<span class="text-green">${signCount + 1}</span>&nbsp;天</div>
	<div class="text-center ptb15"><img src="${base }/mobile/anew/signIn/images/gift.png" style="width:125px;margin-left:20px;"></div>
	<div class="text-center"><button class="btn btn-lottery"><a href="turnlate.html" target="_blank">去抽奖</a></button></div>
</div>

<!-- 未登录弹窗 -->
<div class="unlogin">
	<div class="text-center text-green font18 sign-status"><strong id="fade-sign-info"></strong></div>
	<div class="text-center ptb15"></div>
	<div class="text-center"><button class="btn btn-login" id="fade-sign-body"></button></div>
</div>

<!-- 弹窗 -->
<div class="sign-info">
	<div class="text-center text-green font18 sign-status"><strong id="fade-sign-info"></strong></div>
	<div class="text-center ptb15" id="fade-sign-body"></div>
	<div class="text-center"><button class="btn btn-login fade-sign-know">知道了</button></div>
</div>

<script src="${base }/mobile/anew/signIn/js/jquery-1.10.2.min.js"></script>
<script src="${base }/mobile/v3/page/index/signIn/js/calendar.js"></script>
<script>
var isLogin = '${isLogin}';
var base = '${base}';

function  signIn(){
	if(!isLogin || isLogin == 'false'){
		fadeUnloginAlert('阿哦~您还没有登录哦!','<a href="${m }/login.do">去登录</a></button>');
	}else{
		//签到动作
		calUtil.signToday();
	}
	//显示窗口
	$(".maskbox").fadeIn();
	$(".maskbox").height($(document).height());
}

$(".maskbox").click(function(){
	$(".maskbox").fadeOut();
	$(".qdbox").fadeOut();
	$(".unlogin").fadeOut();
	$(".sign-info").fadeOut();
});

$("div.unlogin").click(function(){
	$(".unlogin").fadeOut();
});

$(".fade-sign-know").click(function(){
	$(".sign-info").fadeOut();
	location.href = location.href;
});

function fadeUnloginAlert(t,b){
	$('div.unlogin #fade-sign-info').html(t);
	$('div.unlogin #fade-sign-body').html(b);
	$(".unlogin").fadeIn();
}

function fadeAlert(t,b){
	$('div.sign-info #fade-sign-info').html(t);
	$('div.sign-info #fade-sign-body').html(b);
	$(".sign-info").fadeIn();
}

//初始化日历
$(function(){
    var signList = [];
    calUtil.draw(signList);
    calUtil.initSign();
    $("#closeqd").click(function(){
    	$("#qdmodule").hide()
    })
    $("#qdbutton").click(function(){
    	$("#qdmodule").show()
    })
});
qdgz();
function qdgz(){
	$.ajax({
		url : "${base}/getConfig/getArticle.do",
		data:{code:17},
		type : "post",
		dataType : 'json',
		success : function(r) {
			if(r.length > 0 ){
				$("#qdbutton").show()
			}
			try{
				$("#qdneirong").html(r[0].content)
			}catch(err){
				
			}
		}
	});
}
</script>
</body>
</html>