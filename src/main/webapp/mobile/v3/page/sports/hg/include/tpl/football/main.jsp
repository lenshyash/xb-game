<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">

	{{each games as val i}}
		<!-- 输出联赛 -->
		{{if i== 0 || games[i-1].league != val.league }}
			<div class="bg-titleliansai">
				<span class="Legimg {{$displayLegClass val.league}}"></span>{{val.league}}
			</div>
		{{/if}}	
		
			<div class="panel panel-success" league="{{val.league}}" style="{{$displayLeg val.league}}">
				<div class="panel-heading">
					{{$formatDate gameType val}}
				</div>
				<div class="panel-body">
					<table>
						<thead>
							<tr class="bg-title">
								<td class="width15">球队</td>
								<td>场次</td>
								<td>独赢</td>
								<td class="minwidth40">让球</td>
								<td class="minwidth40">大小</td>
								<td>单双</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td rowspan="2">
									<div class="teamContent">{{$teamName val.home}}</div>
									<div class="teamContent">{{$teamName val.guest}}</div>
									<div>和局</div>
								</td>
								<td>全场</td>
								<td>
									<span title="独赢" class="text-red font-bold spanborder">{{$showOdds val.ior_MH 'ior_MH' val.gid '全胜'}}</span> 
									<span title="独赢" class="text-red font-bold spanborder">{{$showOdds val.ior_MC "ior_MC" val.gid '全负'}}</span> 
									<span title="独赢" class="text-red font-bold">{{$showOdds val.ior_MN 'ior_MN' val.gid '全平'}}</span>
								</td>
								<td>
									<div title="让球数" class="pull-left">
										<span class="spanborder">{{$letBall val.CON_RH true}}</span> 
										<span class="spanborder">{{$letBall val.CON_RC false}}</span> 
										<span></span>
									</div>
									<div title="让球赔率" class="pull-right">
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_RH 'ior_RH' val.gid '全让胜'}}</span> 
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_RC 'ior_RC' val.gid '全让负'}}</span> 
										<span class="text-red font-bold"></span>	
									</div>
									<div class="clearfix"></div>
								</td>
								<td>
									<div title="大小球" class="pull-left">
										<span class="spanborder">{{$appendBefore val.CON_OUH '大'}}</span> 
										<span class="spanborder">{{$appendBefore val.CON_OUC '小'}}</span> 
										<span></span>
									</div>
									<div title="大小球赔率" class="pull-right">
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_OUH 'ior_OUH' val.gid '全大球'}}</span> 
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_OUC 'ior_OUC' val.gid '全小球'}}</span> 
										<span class="text-red font-bold"></span>
									</div>
									<div class="clearfix"></div>
								</td>
								<td>
									<span title="单" class="text-red font-bold spanborder">{{$showOdds val.ior_EOO 'ior_EOO' val.gid '单'}}</span> 
									<span title="双" class="text-red font-bold spanborder">{{$showOdds val.ior_EOE 'ior_EOE' val.gid '双'}}</span> 
									<span class="text-red font-bold"></span>
								</td>
							</tr>
							<tr class="banchangBG">
								<td>半场</td>
								<td>
									<span title="独赢" class="text-red font-bold spanborder">{{$showOdds val.ior_HMH 'ior_HMH' val.gid '半胜'}}</span>
									<span title="独赢" class="text-red font-bold spanborder">{{$showOdds val.ior_HMC 'ior_HMC' val.gid '半负'}}</span> 
									<span title="独赢" class="text-red font-bold">{{$showOdds val.ior_HMN 'ior_HMN' val.gid '半平'}}</span>
								</td>
								<td>
									<div title="让球" class="pull-left">
										<span class="spanborder">{{$letBall val.CON_HRH true}}</span> 
										<span class="spanborder">{{$letBall val.CON_HRC false}}</span> 
										<span></span>
									</div>
									<div class="pull-right">
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_HRH 'ior_HRH' val.gid '半让胜'}}</span>
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_HRC 'ior_HRC' val.gid '半让负'}}</span>
										<span class="text-red font-bold"></span>
									</div>
									<div class="clearfix"></div>
								</td>
								<td>
									<div title="大球" class="pull-left">
										<span class="spanborder">{{$appendBefore val.CON_HOUH '大'}}</span> 
										<span class="spanborder">{{$appendBefore val.CON_HOUC '小'}}</span>
										<span></span>
									</div>
									<div class="pull-right">
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_HOUH 'ior_HOUH' val.gid '半大球'}}</span> 
										<span class="text-red font-bold spanborder">{{$showOdds val.ior_HOUC 'ior_HOUC' val.gid '半小球'}}</span> 
										<span class="text-red font-bold"></span>
									</div>
									<div class="clearfix"></div>
								</td>
								<td>
									<span class="text-red font-bold spanborder">
									</span><span class="text-red font-bold spanborder">
									</span><span class="text-red font-bold"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
	{{/each}}
</script>