MobileSportResultView = function(cfg) {
	for ( var key in cfg) {
		this[key] = cfg[key];
	}
	this.init();
}


MobileSportResultView.prototype = {
	id : null,//dom的id 渲染到对应的dom
	dataURL : base + "/mobile/sports/hg/getResult.do",//请求数据的链接
	title : '',
	now:null,//当前时间，服务端传过来
	sportType:null,//赛果类型
	tableTpl : null,//容器模板
	gameTpl : null,//模板ID
	dataRenderDomId : "dataContent",
	init : function() {
		this.initBaseView();
		this.bindEvent();
		this.refresh();
	},
	initBaseView : function() {
		var html = template(this.tableTpl, this);
		$("#" + this.id).html(html);
	},
	bindEvent : function(){
		var me = this;
		var date = new Date(this.now);
		$('#SearchDate').val(date.format("yyyy-MM-dd"));
		$('#SearchDate').datetimepicker({
            format: 'yyyy-mm-dd',
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            endDate:date
        });
	
		$("#searchResultBtn").bind("click",function(){
			me.refresh();
		});
	},
	bindLegEvent:function(){
		$(".bg-titleliansai").bind("click",function(){
			var $ele = $(this);
			var $title = $ele.find(".Legimg");
			var open = $title.hasClass("LegClose");
			var status = "";
			if(open){
				$title.removeClass("LegClose");
				status = "none";
			}else{
				$title.addClass("LegClose");
				status = "";
			}
			var legName = $.trim($(this).text());
			$(".panel-success[league='"+legName+"']").css("display",status);
		})
	},
	refresh : function(){
		var me = this;
		var date = $('#SearchDate').val();
		
		$.ajax({
			data : {
				sportType : this.sportType,
				date:date
			},
			url : this.dataURL,
			success : function(data) {
				me.initView(data);
			},
			error : function() {
				Msg.info("网络错误,数据加载失败!");
			}
		});
	},
	initView : function(data) {
		var content = $("#" + this.dataRenderDomId);
		if(data.data.length == 0){
			content.html('<div class="text-center text-red paddingtopbotton5">暂无赛果</div>');
			return;
		}
		var html = template(this.gameTpl, data);
		content.html(html);
		this.bindLegEvent();
	}
}


//是一个整数数字
function isNumber(score){
	if(score == undefined || score == null){
		return false;
	}
	var s = ~~score;
	if(s+"" == score){ 
		return true;
	}
	return false;
}

template.helper('$formatTime', function (time) {
	var date = new Date(time);
	return date.format("MM-dd <br> hh:mm");;
});

template.helper('$printClass', function (score,number,clazz) {
	if(isNumber(score) == number){
		return " "+clazz;
	}
	return "";
});

template.helper('$showAddTimeScore', function (score) {
	if(!isNumber(score)){
		return "-";
	}
	return score;
});

var refreshGameCount = function (gameCount){
	if(!gameCount){
		return;
	}
	for(var key in gameCount){
		var gc = gameCount[key] ? gameCount[key] : 0;
		$("#COUNT_"+key).html(gc);
	}
}

