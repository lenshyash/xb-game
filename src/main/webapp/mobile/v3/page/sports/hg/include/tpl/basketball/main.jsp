<%@ page language="java" pageEncoding="UTF-8" %>

<script type="text/html" id="DATA_TR">
    {{each games as val i}} {{if i== 0 || games[i-1].league != val.league }}
    <div class="bg-titleliansai">
        <span class="Legimg {{$displayLegClass val.league}}"></span>{{val.league}}
    </div>
    {{/if}}
    <div class="panel panel-success" league="{{val.league}}" style="{{$displayLeg val.league}}">
        <div class="panel-heading">{{$formatDate gameType val}}</div>
        <div class="panel-body">
            <table>
                <thead>
                <tr class="bg-title">
                    <td class="width15">球队</td>
                    <td class="minwidth40">独赢</td>
                    <td class="minwidth40">让球</td>
                    <td class="minwidth40">大小球</td>
                    <td class="minwidth40">单节让球</td>
                    <td class="width15">单节总入球</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="teamContent">{{$teamName val.home}}</div>
                        <div class="teamContent">{{$teamName val.guest}}</div>
                    </td>
                    <td>
                        <span class="text-red font-bold spanborder">{{$showOdds val.ior_MH 'ior_MH' val.gid '全胜'}}</span>
                        <span class="text-red font-bold ">{{$showOdds val.ior_MC "ior_MC" val.gid '全负'}}</span>
                    </td>
                    <td>
                        <div title="让球数" class="pull-left">
                            <span class="spanborder">{{$letBall val.CON_RH true}}</span>
                            <span class="">{{$letBall val.CON_RC false}}</span>
                        </div>
                        <div title="让球赔率" class="pull-right">
                            <span class="text-red font-bold spanborder">{{$showOdds val.ior_RH 'ior_RH' val.gid '全让胜'}}</span>
                            <span class="text-red font-bold ">{{$showOdds val.ior_RC 'ior_RC' val.gid '全让负'}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td>
                        <div title="大小球" class="pull-left">
                            <span class="spanborder">{{$appendBefore val.CON_OUH '大'}}</span>
                            <span class="">{{$appendBefore val.CON_OUC '小'}}</span>
                        </div>
                        <div title="大小球赔率" class="pull-right">
                            <span class="text-red font-bold spanborder">{{$showOdds val.ior_OUH 'ior_OUH' val.gid '全大球'}}</span>
                            <span class="text-red font-bold ">{{$showOdds val.ior_OUC 'ior_OUC' val.gid '全小球'}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td>
                        <div title="让球数" class="pull-left">
                            <span class="spanborder">{{$letBall val.MS_RATIO_REH true}}</span>
                            <span class="">{{$letBall val.MS_RATIO_REC false}}</span>
                        </div>
                        <div title="让球数赔率">
                            <span class="text-red font-bold spanborder">{{$showOdds val.MS_IOR_ROUH 'MS_IOR_ROUH' val.gid '全让胜'}}</span>
                            <span class="text-red font-bold ">{{$showOdds val.MS_IOR_ROUC 'MS_IOR_ROUC' val.gid '全让负'}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td>
                        <div title="单节让球" class="pull-left">
                            <span class="spanborder">{{$appendBefore val.MS_RATIO_ROUO '大'}}</span>
                            <span class="">{{$appendBefore val.MS_RATIO_ROUU '小'}}</span>
                        </div>
                        <div title="单节让球赔率" class="pull-right">
                            <span class="text-red font-bold spanborder">{{$showOdds val.MS_IOR_ROUH 'MS_IOR_ROUH' val.gid '全大球'}}</span>
                            <span class="text-red font-bold ">{{$showOdds val.MS_IOR_ROUC 'MS_IOR_ROUC' val.gid '全小球'}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                </tr>
                </tbody>
            </table>


        </div>
    </div>
    {{/each}}
</script>
