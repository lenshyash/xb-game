<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/mobile/v3/page/sports/hg/include/base.jsp"></jsp:include>
<html class="ui-mobile">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<title>${_title}</title>
	<jsp:include page="/mobile/v3/page/sports/hg/include/source.jsp"></jsp:include>
	<script src="${base}/common/template/sports/hg/js/factory/football.js"></script>
</head>

<body>
	<jsp:include page="/mobile/v3/page/sports/hg/include/layout/head.jsp"></jsp:include>
	<div id="sport_view"></div>
	<div class="text-center footercontent">${copyright}</div>
	<jsp:include page="/mobile/v3/page/sports/hg/include/betwin.jsp"></jsp:include>
</body>
</html>
<jsp:include page="/mobile/v3/page/sports/hg/include/tpl/content.jsp"></jsp:include>
<jsp:include page="/mobile/v3/page/sports/hg/include/tpl/football/half_full.jsp"></jsp:include>

<script language="javascript">

var sportView = new MobileSportView({
	id : 'sport_view',
	title:'今日足球半场全场:',
	tableTpl:'BASE_TABLE',
	gameTpl:'DATA_TR',
	gameType:"FT_TD_HF",
	getParams:function(params){
		params.gameType = this.gameType;
		params.sortType = 2;
	}
});	
</script>