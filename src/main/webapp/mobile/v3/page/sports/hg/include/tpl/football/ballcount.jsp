<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">
{{each games as val i}}
	{{if i== 0 || games[i-1].league != val.league }}
		<div class="bg-titleliansai">
			<span class="Legimg {{$displayLegClass val.league}}"></span>{{val.league}}
		</div>
	{{/if}}	
	       
      <div class="panel panel-success" league="{{val.league}}" style="{{$displayLeg val.league}}">
          <div class="panel-body">
              <table>
                  <thead>
                      <tr class="bg-title">
                          <td>时间</td>
                          <td class="width30">球队</td>
                          <td>0-1</td>
                          <td>2-3</td>
                          <td>4-6</td>
                          <td>7或以上</td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>{{$formatDate gameType val}}</td>
                          <td>{{$teamName val.home}}<br />{{$teamName val.guest}}</td>
                          <td><span class="text-red font-bold">{{$showOdds val.ior_T01 'ior_T01' val.gid '0-1'}}</span></td>
                          <td><span class="text-red font-bold">{{$showOdds val.ior_T23 'ior_T23' val.gid '2-3'}}</span></td>
                          <td><span class="text-red font-bold">{{$showOdds val.ior_T46 'ior_T46' val.gid '4-6'}}</span></td>
                          <td><span class="text-red font-bold">{{$showOdds val.ior_OVER 'ior_OVER' val.gid '7-'}}</span></td>
                      </tr>
                  </tbody>
              </table>
          </div>
      </div>
{{/each}}

</script>




