﻿function suofangFn(id, num) {
	var oWidth = document.documentElement.clientWidth
			|| document.body.clientWidth;
	var oHeight = document.documentElement.clientHeight
			|| document.body.clientHeight;
	var objx = document.getElementById(id);
	if (objx) {
		suofang = oWidth / num;
		oHeight = oHeight / suofang;
		if (oWidth < 640) {
			objx.style.position = "relative";
			objx.style.left = "0px";
			objx.style.top = "0px";
			objx.style.width = num + "px";
			objx.style.transformOrigin = "left top 0px";
			objx.style.webkitTransformOrigin = "left top 0px";
			objx.style.transform = "scale(" + suofang + ")";
			objx.style.webkitTransform = "scale(" + suofang + ")"
		} else {
			objx.style.width = num + "px";
		}
		return true;
	}
	return false;
}

function zishiying() {
	var hashka = suofangFn("innerbox", 640);
	if (hashka) {
		var oHeight = document.documentElement.clientHeight
				|| document.body.clientHeight;
		$("body").height(oHeight);
	}
	$("#innerbox .touzhubox").css("minHeight",400);
}

zishiying();


function stopBuFn(e) {
	var e = e || event;
	if (e && e.stopPropagation) {
		e.stopPropagation();

	} else {
		e.cancelBubble = true;
	}
}

$(function() {
	$(document).click(function() {
		$(".sjxialabox").hide();
		$(".shangqixiala").hide();
	});

	$(".touzhuqie span").click(
			function() {
				$(this).addClass("cur").siblings().removeClass("cur");
				var x = $(this).index()
				$(".tzhuanqie-huan").eq(x).addClass("cur").siblings().removeClass("cur")
			}
	);

	$(".selectComponent").on("click", function(e) {
		stopBuFn(e)
		$(this).find(".sjxialabox").toggle()
	});
	$(".selectComponent ul li").on("click",
		function() {
			var htmlx = $(this).html();
			$(this).parents(".sjxialabox").siblings(".fangsjbox").html(htmlx);
			var $selectComponent = $(this).parents(".selectComponent:first");
			if ($selectComponent.data("value") != $(this).data("value")) {
				$selectComponent.data("value",$(this).data("value")).trigger("change");
			}
			$selectComponent.trigger("select");
		});



	$("#selectQueryType span").on("click", function(){
		var querytype = $(this).data("querytype");
		$("#selectQueryType").data("current", querytype).trigger("change");
	});

	$("#selectQueryType").on("change", function(){
		reloadRecordView();
	});

	$("#queryDatetime").on("change", function(){
		reloadRecordView();
	});

	$("#queryBallcode").on("change", function(){
		reloadRecordView();
	});

	var reloadRecordView = function () {
		$.ajax({
			url: base + "/mobile/sports/hg/getOrderData.do", 
			data:{
				recordType: $("#selectQueryType").data("current"), 
				date: $("#queryDatetime").data("value"), 
				sportType: $("#queryBallcode").data("value")
			},
			success:function(data){
				if(data.rows.length == 0){
					$("#recordContent").html("<div style='width:100%;font-size:28px;line-height:300px;' align='center' >没有注单数据</div>");
					return;
				}
				var result = {
					rows:[]
				}
				for(var i=0;i<data.rows.length;i++){
					var row = data.rows[i];
					var obj = {
						id:row.id,
						betMoney : row.bettingMoney,
						sportName : getSportName(row.sportType),
						betDate : new Date(row.bettingDate).format("MM月dd日,hh:mm:ss"),
						betStatus: getBetStatus(row)
					};
					result.rows.push(obj);
				}
				template.config("escape", false);
				var html = template("DATA_TR",result);
				$("#recordContent").html(html);
			}
		});
	};
	reloadRecordView();
});

function getBetStatus(row){
	if(row.bettingStatus == 3 || row.bettingStatus == 4){
		return "<font color='red'>注单取消</font>";
	}
	
//	if(row.balance == 2){
//		return "等待开奖";
//	}
	
	if(row.balance == 1){
		return "等待开奖";
	}
	
	if(row.balance == 4){
		return "<font color='blue'>赛事腰斩</font>";
	}
	
	if(row.balance == 2 || row.balance == 5 || row.balance == 6){
		if(row.bettingResult > 0){
			return "<font color='green'>派彩:"+row.bettingResult+"</font>";
		}
		return "<font color='red'>输</font>";
	}
}

function getSportName(type){
	if(type == 1){
		return "足球";
	}
	if(type == 2){
		return "篮球";
	}
	return "其他";
}

function goDeatilPage(id){
	window.location.href = base + "/mobile/sports/hg/goOrderDetailPage.do?id="+id;
}