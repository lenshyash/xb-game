template.helper('$showOdds', function (odds,oddsType,gid,project) {
	if(!odds){
		return "";
	}
	var content = "";
	var title = "";
	var keepData = global.gameMap[gid];
	switch(oddsType){
		case "ior_EOO":
			content = "<font color='black' style='font-weight:normal;'>单</font>&nbsp;";
			title = "单";
			break;
		case "ior_EOE":
			content = "<font color='black' style='font-weight:normal;'>双</font>&nbsp;";
			title = "双";
			break;
		case "ior_MH":
		case "ior_HMH":
		case "ior_RH":
		case "ior_HRH":
			title = toText(keepData["home"]);
			break;
		case "ior_MC":
		case "ior_HMC":
		case "ior_RC":
		case "ior_HRC":
			title = toText(keepData["guest"]);
			break;
		case "ior_HMN": 			
		case "ior_MN":
			title = "和局";
			break;	
		case "ior_HOUH":
		case "ior_OUH":
		case "ior_OUHO":
		case "ior_OUCO":
			title = "大";
			break;	
		case "ior_HOUC":
		case "ior_OUC":
		case "ior_OUHU":
		case "ior_OUCU":
			title = "小";
			break;	
	}
	var css = chekOddsChange(odds,oddsType,gid); 
	var oddsStr = odds;
	if(sportView.gameType.indexOf("_MN") > -1 || sportView.gameType.indexOf("_MX") > -1){
		oddsStr = HG.printf(odds,global.iorpoints);//保留两位小数
	}
	return content + '<a href="javascript:void(0);" title="'+title+'" onclick=bet('+gid+',"'+oddsType+'","'+project+'",'+odds+');><font color="red" '+css+'>' +oddsStr+ '</font></a>';
});


function replaceAll(str, sptr, sptr1){
    while (str.indexOf(sptr) >= 0){
       str = str.replace(sptr, sptr1);
    }
    return str;
}

template.helper('$letBall', function (project,isHome) {
	var isHomeStrong = project.indexOf("-") == -1;
	if(isHome){
		if(isHomeStrong){
			return project;
		}
		return "";
	}else{
		if(isHomeStrong){
			return "";
		}else{
			return project.substring(1);
		}
	}
	return str;
});

template.helper('$appendBefore', function (content,appendStr) {
	content = replaceAll(content, ' ', '');
	if(content != ""){
		return appendStr + content;
	}
	return content;
});

template.helper('$teamName', function (teamName) {
	return teamName.replace("[中]","<font color=\"#005aff\">[中]</font>");
});

function toText(html){
	if(html.indexOf("<") == -1){
		return html;
	}
	var div = document.createElement("div");
	div.innerHTML = html;
	return $(div).text();
}

function chekOddsChange (odds,oddsType,gid) {
	if(!odds || ! global.preGameMap){
		return "";
	}
	var keepData = global.preGameMap[gid];
	if(!keepData){
		return "";
	}
	var oldOdds = keepData[oddsType];
	if(oldOdds && oldOdds != odds){
		return '  style="background-color : yellow" ';
	}
	return "";
};

template.helper('$formatDate', function (gameType,rowData) {
	var sportFac = getSportFactory(gameType);//体育工厂	
	var str = sportFac.showMobileGameTime(rowData);
	return str;
});

template.helper('$displayLeg', function (legName) {
	if(global.openLegs[legName] === true){
		return "";
	}
	return "display:none;";
});

template.helper('$displayLegClass', function (legName) {
	if(global.openLegs[legName] === true){
		return "LegClose";
	}
	return "";
});

