/**
 * 系统消息框
 */
(function(){
	var _tags = document.getElementsByTagName("script");
	window.base = _tags[_tags.length - 1].getAttribute("path");
	
	var _parent = window;
	while(_parent != window.parent){
		_parent = window.parent;
	}
	
	function unbind(){
		$("#alert_ok_btn").unbind();
		$("#alert_cancel_btn").unbind();
	}
	
	function closeWin(){
		$("#ycf-alert").modal('hide');
	}
	
	var baseConfig  = {
		okText : "确认",
		cancelText : "关闭",
		okFn:closeWin ,
		showOk:false,
		showCancel:true,
		cancelFn:closeWin,
		content: '',
	};
	function showWin(cfg){
		for(var key in baseConfig){
			if(cfg[key] === undefined){
				cfg[key] = baseConfig[key];
			}
		}
		unbind();
		$("#alert_msg_content").html(cfg.content);
		var $okBtn = $("#alert_ok_btn");
		var $cancelBtn = $("#alert_cancel_btn");
		if(cfg.showOk){
			$okBtn.css("display","");
			$okBtn.html(cfg.okText);
			$okBtn.bind("click",cfg.okFn);
		}else{
			$okBtn.css("display","none");
		}
		
		if(cfg.showCancel){
			$cancelBtn.css("display","");
			$cancelBtn.html(cfg.cancelText);
			$cancelBtn.bind("click",cfg.cancelFn);
		}else{
			$cancelBtn.css("display","none");
		}
		$("#ycf-alert").modal('show');
	}
	
	window.Msg =  {
		info:function(msg,title){
			var t = title ? title : "提示";
			var content = t + ":" + msg;
			showWin({content:content});
		},
		warn:function(msg,title){
			var t = title ? title : "警告";
			var content = t + ":" + msg;
			showWin({content:content});
		},
		error:function(msg,title){
			var t = title ? title : "异常";
			var content = t + ":" + msg;
			showWin({content:content});
		},
		confirm:function(msg, fn,title){
			showWin({
				cancelText:'取消',
				content:msg,
				showOk:true,
				okFn:fn
			});
		}
	}
})();


(function($){  
    //备份jquery的ajax方法  
    var _ajax=$.ajax;  

    // 重写jquery的ajax方法
    window.$post =function(opt){  
    	if(!opt.dataType){
    		opt.dataType = "json";
    	}
    	if(!opt.type){
    		opt.type = "post";
    	}
        _ajax(opt);  
    };  
    
    //重写jquery的ajax方法  
    $.ajax=function(opt){  
    	if(!opt.dataType){
    		opt.dataType = "json";
    	}
    	if(!opt.type){
    		opt.type = "post";
    	}
        //备份opt中error和success方法  
        var fn = {  
            error:function(XMLHttpRequest, textStatus, errorThrown){},  
            success:function(data, textStatus,xhr){}  
        }  
        if(opt.error){  
            fn.error=opt.error;  
        }  
        if(opt.success){  
            fn.success=opt.success;  
        }  
          
        //扩展增强处理  
        var _opt = $.extend(opt,{  
            error:function(XMLHttpRequest, textStatus, errorThrown){  
            	var statusCode = XMLHttpRequest.status;
                //错误方法增强处理  
                if(statusCode == 404){
                	Msg.error("["+opt.url+"] 404 not found");
                }else{
                	fn.error(XMLHttpRequest, textStatus, errorThrown);  
                }
            },  
            success:function(data, textStatus,xhr){
            	var ceipstate = xhr.getResponseHeader("ceipstate")
            	if(ceipstate == 1){//正常响应
            		fn.success(data, textStatus,xhr);  
            	}else if(ceipstate == 2){//后台异常
            		Msg.error("后台异常，请联系管理员!");
            	}else if(ceipstate == 3){ //业务异常
            		Msg.info(data.msg);
            	}else if(ceipstate == 4){//未登陆异常
            		window.top.location.href = base || "/";
            	}else if(ceipstate == 5){//没有权限
            		Msg.info(data.msg,"权限不足");
            	}else{
            		fn.success(data, textStatus,xhr);  
            	}
            }  
        });  
        _ajax(_opt);  
    };  
})(jQuery);  

function html_encode(str){   
  var s = "";   
  if (str.length == 0) return "";   
  s = str.replace(/&/g, "&gt;");   
  s = s.replace(/</g, "&lt;");   
  s = s.replace(/>/g, "&gt;");   
  s = s.replace(/ /g, "&nbsp;");   
  s = s.replace(/\'/g, "&#39;");   
  s = s.replace(/\"/g, "&quot;");   
  s = s.replace(/\n/g, "<br>");   
  return s;   
}   
 
function html_decode(str){   
  var s = "";   
  if (str.length == 0) return "";   
  s = str.replace(/&gt;/g, "&");   
  s = s.replace(/&lt;/g, "<");   
  s = s.replace(/&gt;/g, ">");   
  s = s.replace(/&nbsp;/g, " ");   
  s = s.replace(/&#39;/g, "\'");   
  s = s.replace(/&quot;/g, "\"");   
  s = s.replace(/<br>/g, "\n");   
  return s;   
}

/**
 * JSON 
 */
var JSON = JSON || {};
(function () {

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                   this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;
    function quote(string) {
        escapable.lastIndex = 0;
        return escapable.test(string) ?
            '"' + string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' :
            '"' + string + '"';
    }
    function str(key, holder) {
        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];
        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }
        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }
        switch (typeof value) {
        case 'string':
            return quote(value);
        case 'number':
            return isFinite(value) ? String(value) : 'null';
        case 'boolean':
        case 'null':
            return String(value);
        case 'object':
            if (!value) {
                return 'null';
            }
            gap += indent;
            partial = [];
            if (Object.prototype.toString.apply(value) === '[object Array]') {

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }
                v = partial.length === 0 ? '[]' :
                    gap ? '[\n' + gap +
                            partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                          '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }
            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {
                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }
            v = partial.length === 0 ? '{}' :
                gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                        mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }
    if (typeof JSON.encode !== 'function') {
        JSON.encode = function (value, replacer, space) {
            var i;
            gap = '';
            indent = '';
            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }
            } else if (typeof space === 'string') {
                indent = space;
            }
            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                     typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }
            return str('', {'': value});
        };
    }
    if (typeof JSON.decode !== 'function') {
        JSON.decode = function (text) {
        	return eval("("+text+")");	
        };
    }
}());

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
Date.prototype.format = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

DateUtil = {
	formatDate : function(time) {
		if (!time) {
			return "";
		}
		var date = new Date(time);
		return date.format("yyyy-MM-dd");
	},
	formatDatetime : function(time) {
		if (!time) {
			return "";
		}
		var date = new Date(time);
		return date.format("yyyy-MM-dd hh:mm:ss");
	}
}

function setValues(formId,data){
	var form = $("#"+formId);
	if(form.length == 0){
		return;
	}
	var f = form[0];
	for(var i=0;i<f.length;i++){  
	     var element = f[i];  
	     var type = element.type;
	     var name = element.name;
	     var v = data[name];
	     if(v == undefined){
	    	 continue;
	     }
	     if(type == "checkbox"){  
	    	 continue;
	     }else if(type == "text" || type == "hidden" || type == "select-one"){
	    	 $(f[name]).val(v);
	     }else if(type == "radio"){
	    	 if(element.value == v){
	    		 element.checked = true;
	    	 }
	     }  
	}  
	for(var key in data){
		form.find("input")
	}
}

//对Date的扩展，将 Date 转化为指定格式的String
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
//例子：
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
//(new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
Date.prototype.format = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

CookieUtil = {
	delCookie : function(name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = this.getCookie(name);
		if (cval != null) {
			document.cookie = name + "=" + cval + ";expires="
					+ exp.toGMTString() + ";path=/";
		}
	},
	addCookie : function(sName, sValue, day) {
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() + day);
		document.cookie = sName
				+ "="
				+ escape(sValue)
				+ ((expireDate == null) ? "" : ";expires="
						+ expireDate.toGMTString()) + ";path=/";
	},
	getCookie : function(name) {
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg)) {
			return unescape(arr[2]);
		}
		return null;
	}
}

