var global = {
	sport_factory_map : {},
	show_ior:100,
	iorpoints:2,
	openLegs:{}
};

MobileSportView = function(cfg) {
	for ( var key in cfg) {
		this[key] = cfg[key];
	}
	this.init();
}

emptyFn = function(){};

MobileSportView.prototype = {
	id : null,//dom的id 渲染到对应的dom
	dataURL : base + "/mobile/sports/hg/getData.do",//请求数据的链接
	title : '',
	getParams : emptyFn,//ajax参数回调
	tableTpl : null,//容器模板
	gameTpl : null,//模板ID
	gameType : null,//游戏类型

	autoRefreshTime : 180,//自动刷新时间 单位秒
	showRefresh : true,//显示刷新按钮
	showTimer : true,//显示倒计时自动刷新

	pageNo : 1,//当前页码
	pageSize : 1,//总页数
	oddsType : 'H', // 默认香港盘口
	refreshBtnId : "refresh_btn",
	timerContextId : 'refresh_btn',
	pageTextId : 'pg_txt', //分页状态栏
	pageSelectorId : 'view_page_selector',

	timer : null,//private 定时器
	dataRenderDomId : "dataContent", //private string  赛事渲染的div
	init : function() {
		this.initBaseView();
		this.bindEvent();
		this.refresh();
	},
	toMapArr : function(headers,games){
		var arr = [];
		if(global.gameMap){//保存上一次数据
			global.preGameMap = global.gameMap;
		}
		
		global.gameMap = {};
		//global.myLeg = {};
	
		var sportFac = getSportFactory(this.gameType);//体育工厂			
		for (var i = 0; i < games.length; i++) {
			var map = {
				gameType:this.gameType
			};
			for (var j = 0; j < headers.length; j++) {
				var key = headers[j];
				map[key] = games[i][j];
			}
			var leg = map["league"];
			var gid = map["gid"];
//			if(leg !== undefined){
//				var legArr = global.myLeg[leg];
//				if(!legArr){
//					legArr = [];
//					global.myLeg[leg] = legArr;
//				}
//				legArr.push(gid);
//			}
			
			sportFac.formatOdds(map);//转换赔率
			global.gameMap[gid] = map;
			arr.push(map);
		}
		return arr;
	},
	initBaseView : function() {
		if (window.global) {
			window.global.gameType = this.gameType;
		}
		var data = {
			gameType : this.gameType,
			title : this.title,
			pageTextId:this.pageTextId,
			pageSelectorId:this.pageSelectorId,
			showOddsTypeList : this.showOddsTypeList,//显示盘口
			showRefresh : this.showRefresh,//显示刷新按钮
			oddsType : this.oddsType
		};
		
		//var now = new Date();	
		//data.dataRenderDomId = this.dataRenderDomId = this.id + "_" + now.getTime();

		if (this.showTimer) {
			data.autoRefreshTime = this.autoRefreshTime;
			data.refreshBtnId = this.refreshBtnId;
		}
		var html = template(this.tableTpl, data);
		$("#" + this.id).html(html);
	},
	formatViewData:function(data){
		var dataArr = this.toMapArr(data.headers,data.games);
		return {games:dataArr,gameType:this.gameType};
	},
	initView : function(data) {
		var tplData = data;
		if (this.formatViewData != emptyFn) {
			tplData = this.formatViewData(data);
		}
		var content = $("#" + this.dataRenderDomId);
		if(tplData.games.length == 0){
			content.html('<div class="text-center text-red paddingtopbotton5">暂无赛事</div>');
			return;
		}
		var html = template(this.gameTpl, tplData);
		
		content.html(html);
		$(".bg-titleliansai").bind("click",function(){
			var $ele = $(this);
			var $title = $ele.find(".Legimg");
			var open = $title.hasClass("LegClose");
			var status = "";
			var legName = $.trim($(this).text());
			if(open){
				$title.removeClass("LegClose");
				status = "none";
				global.openLegs[legName] = false;
			}else{
				$title.addClass("LegClose");
				status = "";
				global.openLegs[legName] = true;
			}
			content.find(".panel-success[league='"+legName+"']").css("display",status);
		})
	},
	stopTimer : function() {
		if (!this.timer || this.showTimer !== true) {
			return;
		}
		clearInterval(this.timer);
		this.timer = null;
	},
	openTimer : function() {
		if (this.showTimer !== true) {
			return;
		}
		var me = this;
		me["_autoRefreshTime"] = this.autoRefreshTime;
		this.timer = setInterval(function() {
			if (me["_autoRefreshTime"] == 0) {
				me.refresh.call(me);
			}
			var sec = --me["_autoRefreshTime"];
			$("#" + me.timerContextId).html(sec < 0 ? "0" : sec + "");
		}, 1000);
	},
	bindEvent : function() {
		var me = this;
		//绑定刷新按钮事件
		if (this.refreshBtnId) {
			$("#" + this.refreshBtnId).bind("click", function() {
				me.refresh.call(me);
			});
		}

		//绑定选中页码事件
		if (this.pageSelectorId) {
			var pageSelector = $("#" + this.pageSelectorId);
			pageSelector.bind("change", function() {
				me.goPage.call(me, pageSelector.val());
			});
		}
	},
	refresh : function(params) {
		var pageSelector = $("#" + this.pageSelectorId);
		var pageNo = ~~pageSelector.val();
		if (pageNo <= 0) {
			pageNo = 1;
		}
		this.goPage(pageNo, params);
		this.pageNo = pageNo;
	},
	goPage : function(_pageNo, paramsObject) {
		this.stopTimer();
		$("#" + this.refreshBtnId).removeClass("refresh_btn");
		$("#" + this.refreshBtnId).addClass("refresh_on");
		var params = null;
		if (paramsObject) {
			params = paramsObject;
		} else {
			params = {};
		}
		params.pageNo = _pageNo;
		this.getParams(params);
		var me = this;
		$.ajax({
			data : params,
			url : this.dataURL,
			success : function(data) {
				me.pageSize = data.pageCount;
				me.pageNo = _pageNo;
				me.initView(data);
				me._refreshPagingStatus(data);
				refreshGameCount(data.gameCount);
			},
			complete : function(data) {
				me.openTimer();
			},
			error : function() {
				Msg.info("网络错误,数据加载失败!");
			}
		});
	},
	/**
	 * 刷新分页状态
	 * @param data
	 */
	_refreshPagingStatus : function(data) {
		this._refreshPageSelector();
		//this.openTimer();
		$("#" + this.pageTextId).html(this.pageNo + " / " + this.pageSize + " 页&nbsp;&nbsp;");
		$("#" + this.refreshBtnId).removeClass("refresh_on");
		$("#" + this.refreshBtnId).addClass("refresh_btn");
	},
	/**
	 * 刷新页码选择
	 */
	_refreshPageSelector : function() {
		var selector = $("#" + this.pageSelectorId);
		selector.html("");
		for (var i = 0; i < this.pageSize; i++) {
			var selectTag = "";
			if (i + 1 == this.pageNo) {
				selectTag = " selected ";
			}
			selector.append("<option value='" + (i + 1) + "' " + selectTag
					+ ">" + (i + 1) + "</option>");
		}
	}
}


function bet(gid,oddsType,project,odds){
	var gameData = global.gameMap[gid];
	//console.info(gameData);
	var fac = getSportFactory(gameData.gameType);
	var data = fac.getBetInfoData(gameData,oddsType);
	if(!data){
		Msg.info("数据异常");
		return;
	}
	if(typeof data == "string"){
		Msg.info(data);
		return;
	}
	var isMix = fac.isMix();
	var viewData = {
		sport:fac.getName(),	
		isMix:isMix
	};
	$("#betMoney").val('')
	$("#winMoney").text(0)
	addBetItem(data,gameData,viewData);//调用父页面addBetItem
}

//注单操作
$(function(){
	
	var gameType = null;
	var betItems = [];//array
	var betMap = {}; //object
	var gidKey = "gid";
	
	var needKeys = ["gid","mid","odds","type","project","scoreH","scoreC"];

	function getNeedData(){
		var data = {
			plate:"H",
			gameType:gameType
		};
		var bis = [];
		for(var i=0;i<betItems.length;i++){
			var item = betItems[i];
			var bi = {};
			for(var j=0;j<needKeys.length;j++){
				if(item[needKeys[j]] !== undefined){
					bi[needKeys[j]] = item[needKeys[j]];
				}
			}
			bis.push(bi);
		}
		data["items"] = bis;
		return data;
	}
	
	window.addBetItem = function(betInfo,gameData,viewData){
		var isMix = viewData.isMix;
		var gt = gameData.gameType;
		var gid = null;
		if(gt.indexOf("BK") == 0){//篮球
			gid = betInfo.mid;
			gidKey = "mid"
		}else{
			gid = betInfo.gid;
		}
		betInfo.deleteId = gid; //用于界面删除注单使用
		if(gt != gameType || isMix !== true){//非混合过关或者投注大类发生变化  重置数据  
			betItems = [];
			betMap = {};
		}
		if(isMix && betMap[gid]){//已经存在这场赛事
			if(betInfo.type == betMap[gid].type){ //点两次 就触发取消
				delBetItem(gid);
				return;
			}
			
			for(var i = 0;i <betItems.length;i++ ){
				if(betItems[i][gidKey] == gid){//replace
					betInfo.replace = true;
					betItems[i] = betInfo;
					showBetWin(viewData);
					betMap[gid] = betInfo;
					return;
				}
			}
		}
		betItems.push(betInfo);
		betMap[gid] = betInfo;
		gameType = gt;
		//展示投注界面
		showBetWin();
	}
	
	window.showBetWin = function(showWin){
		var fac = getSportFactory(gameType);
		var viewData = {
			isMix:fac.isMix()
		};
		viewData.items = betItems;
		var height = 115;
		if(betItems.length > 1){
			var height = 100 * betItems.length
		}
		var maxHeight = window.screen.height - 170 - 50;
		if(height > maxHeight){
			height = maxHeight;
		}
		$("#orderList").css("height",height);
		
		$(".firameContent").css("height",170 + height);
		var html = template("order_item", viewData);
		$("#orderList").html(html);
		if(showWin !== false){
			showBW();
		}
	}
	
	
	window.delBetItem = function(gid){
		var delItem = betMap[gid];
		$("#del" + gid).remove();
		if(!delItem){
			return;
		}
		delete betMap[gid];
		if(!betItems || betItems.length == 0){
			return;
		}
		var arr = [];
		for(var i = 0;i <betItems.length;i++ ){
			if(betItems[i][gidKey] != gid){
				arr.push(betItems[i]);
			}
		}
		betItems = arr;
		showBetWin();
	}
	
	window.refreshOdds = function(){
		var data = getNeedData();
		if(data.items.length == 0){
			return;
		}
		var json = JSON.encode(data);
		$post({
			url : base + "/sports/hg/bet/getOdds.do",
			data: {
				data : json
			},
			success:function(data, textStatus,xhr){
		    	var ceipstate = xhr.getResponseHeader("ceipstate")
		    	if(ceipstate == 4){//未登陆异常
		    		showSportMsg("用户未登陆，请先登陆");
		    		return;
		    	}
		    	if(ceipstate == 1){
		    		//更新赔率
		    		var arr = data.odds;
		    		for(var i= 0;i < arr.length; i++){
		    			var bi = arr[i];
						var gid = bi.gid;
						var cacheData = getMatchCache(gid);
						if(!cacheData){
							continue;
						}
						var oddsDiv = $("#"+gid+"betodds");
						if(cacheData["odds"] == bi.odds){
							oddsDiv.css("background-color","transparent");
						}else{
							oddsDiv.css("background-color","yellow");
							oddsDiv.html(bi.odds);
							cacheData["odds"] = bi.odds;
						}
		    		}
		    	}else{
		    		showSportMsg(data.msg);
		    		return;
		    	}
			}
		});
	}
	
	function showSportMsg(msg){
		Msg.info(msg);
		clearData();
		showBetWin();
		hideBW();
	}
	
	function getMatchCache(gid){
		for(var i=0;i<betItems.length;i++){
			var bi = betItems[i];
			if(bi.gid == gid){
				return bi;
			}
		}
		return null;
	}
	
	window.calWinMoney = function(inputEl){
		var val = $.trim(inputEl.value);
		if(val.length == 0){
			$("#winMoney").html("0");
			return ;
		}
		
		if(betItems.length == 0){
			$("#winMoney").html("0");
			return ;
		}
		var gameKey = new GameKey(gameType);
		var total = parseInt(val);
		var betMoney = total;
		for(var i = 0;i < betItems.length;i++){
			var bi = betItems[i];
			var odds = bi.odds * 1;
			var itemKey = new BetItemKey(bi.type); 
			if(gameKey.isMain() &&  (itemKey.isLetBall() || itemKey.isBigSmall())){
				odds += 1;
			}
			total = accMul(total,odds);
		}
		
		var result = HG.printf(total - betMoney,2);
		$("#winMoney").html(result);
		//return result;
	}
	
	
	
	window.submitOrder = function(){
		if(betItems.length == 0){
			return;
		}
		var data = getNeedData();
		var betMoney = $("#betMoney").val();
		if(!betMoney){
			Msg.info("请输入有效投注金额");
			$("#betMoney").focus();
			return;
		}
		
		if(!checkBetMoney(betMoney)){
			return;
		}
		
		Msg.confirm("是否确认要下注?",function(){
			betMoney = ~~betMoney;
			var money = betMoney;
			var minMoeny = parseInt($("#minBettingMoney").text());
			if(money < minMoeny){
				Msg.info("单注最低金额不能少于"+minMoeny);
				return;
			}
			data.money = money;
			$accept = $("#autoAccept");
			if($accept.length > 0 && $accept[0].checked ){
				data.acceptBestOdds = true;
			}
			var json = JSON.encode(data)
			$.ajax({
				url : base + "/mobile/sports/hg/bet.do",
				data : {
					data : json
				},
				success:function(data){
					if(data.success){
						clearData();
						showBetWin();
						hideBW();
						$("#winMoney").html("0");
						$("#betMoney").val("");;
						Msg.info("下注成功<br/>单号:"+data.code);
						//updateMemberInfo();//更新余额
					}else{//赔率变动
						var gid = data.gid;
						var cacheData = getMatchCache(gid);
						cacheData["odds"] = data.newOdds;
						var oddsDiv = $("#"+gid+"betodds");
						oddsDiv.html(data.newOdds);
						oddsDiv.css("background-color","yellow");
						Msg.info("赔率变动");
					}
				}
			});
		});
	};
	
	
	function checkBetMoney(money) { 
	    var re = /^\d+(?=\.{0,1}\d+$|$)/ 
        if (!re.test(money)) { 
            Msg.info("请输入有效投注");
            return false;
        }
	    return true;
	} 
	
	var clearData = function(){
		betItems = [];
		betMap = {};
	}
});


function accMul(arg1, arg2) {
	var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
	try {
		m += s1.split(".")[1].length
	} catch (e) {
		
	}
	try {
		m += s2.split(".")[1].length
	} catch (e) {
		
	}
	return Number(s1.replace(".", "")) * Number(s2.replace(".", ""))/ Math.pow(10, m)
}

function RepNumber(obj) {
	var reg = /^[\d]+$/g;
	if (!reg.test(obj.value)) {
		var txt = obj.value;
		txt.replace(/[^0-9]+/, function(char, index, val) {// 匹配第一次非数字字符
			obj.value = val.replace(/\D/g, "");// 将非数字字符替换成""
			var rtextRange = null;
			if (obj.setSelectionRange) {
				obj.setSelectionRange(index, index);
			} else {// 支持ie
				rtextRange = obj.createTextRange();
				rtextRange.moveStart('character', index);
				rtextRange.collapse(true);
				rtextRange.select();
			}
		})
	}
	calWinMoney(obj);
}

function showBW(){
	
	var $btn = $(".arrowSlide");
	var kfcontent = parseInt($btn.css("right").replace("px", ""));
	if (kfcontent != 0 ) {
		return;
	}
	var height = 170 + $("#orderList").height();
	$(".firameContent").animate({
		width : '235px'
	}, "slow");
	$btn.animate({
		right : '235px'
	}, "slow");
	$btn.removeClass("arrowbg").addClass("noarrowbg");
}
function hideBW(){
	var $btn = $(".arrowSlide");
	var kfcontent = parseInt($btn.css("right").replace("px", ""));
	if (kfcontent == 0 ) {
		return;
	}
	$(".firameContent").animate({
		width : 0
	}, "slow");
	
	$btn.animate({
		right : 0
	}, "slow");
	$btn.removeClass("noarrowbg").addClass("arrowbg");
}



$(function(){
	$(".arrowSlide").click(function() {
		var $btn = $(".arrowSlide");
		var kfcontent = parseInt($btn.css("right").replace("px", ""));
		if (kfcontent > 0) {
			hideBW();
		} else {
			showBW();
		}
	});
});


var refreshGameCount = function (gameCount){
	if(!gameCount){
		gameCount = window._gameCount;
	}else{
		window._gameCount = gameCount;
	}
	
	if(!gameCount){
		return;
	}
	
	for(var key in gameCount){
		var gc = gameCount[key] ? gameCount[key] : 0;
		$("#COUNT_"+key).html(gc);
	}
}

