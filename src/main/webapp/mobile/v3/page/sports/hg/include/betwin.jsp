<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- 弹窗消息 -->    
<div id="ycf-alert" class="modal">
	<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
		<div class="modal-content">
			<div class="modal-body bg-info" style="border-radius: 5px; padding: 10px;">
				<p style="font-size: 15px; font-weight: bold;" id="alert_msg_content">[Message]</p>
			</div>
			<div class="modal-footer" style="padding: 5px; text-align: center;">
				<button type="button" id="alert_ok_btn" class="btn btn-primary ok" >[BtnOk]</button>
				<button type="button" id="alert_cancel_btn" class="btn btn-default cancel" >[BtnCancel]</button>
			</div>
		</div>
	</div>
</div>

<!-- 遮挡层 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<img class="center-block linecentermodel" width="40" height="40" src="${base}/mobile/sports/hg/img/load.gif" />
</div>
<!-- 下注框 -->

<div class="arrowSlide arrowbg inputAlone" style="position:fixed;bottom:0px;right:0px;"></div>

<div class="betPop inputAlone">
		<div class="firameContent" style="overflow:hidden;height:265px;">
			<div style="height:50px;background-color:#330000;color:#CCCC99;">
				<div style="width:100%;font-size:15px;" align="center">交易单</div>
				<div style="font-size:12px;">
					<span class="pull-left">&nbsp;<input type="checkbox" id="autoAccept"/><label for="autoAccept">接受最佳赔率</label></span>
					<span class="pull-right" onclick="refreshOdds()">刷新赔率&nbsp;</span>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="orderList" style="width:235px;" >
				<div style="width: 216px; height: 63px; text-align: center; padding-top: 26px;">
					<font style="font: 12px Arial, Helvetica, sans-serif; font-weight: bold;">点击赔率便可将<br>选项加到交易单里。
					</font>
				</div>
			</div>
			<div style="background-color:#330000;color:#CCCC99;">
				<div style="padding-left:5px;padding-top:10px;padding-bottom:10px;">
					<c:choose>
						<c:when test="${domainFolder == 'b219'}">
							<div>交易金额: <input style="color:black;width:100px;" type="text" id="betMoney" onkeyup="RepNumber(this)"/></div>
							<div >可赢金额:&nbsp;<span id="winMoney">0</span></div>
						</c:when>
						<c:otherwise>
							<div>交易金额: <input class="float" style="color:black;width:100px;" type="text" id="betMoney"/></div>
						</c:otherwise>
					</c:choose>
					<div>单注最低: <font id="minBettingMoney">${minBettingMoney}</font></div>
					<div></div>
				</div>
			</div>
			<div>
				<button style="width:100%;" type="button" onclick="submitOrder()" class="btn btn-large btn-success">确认交易</button>
			</div>
		</div>
</div>
<script>
	/**
	 * @desc 移动端模拟键盘
	 * @author: Demigodliu
	 * @date: 2020/06/13 19:36
	 **/
	;(function ($) {
		$.fn.extend({
			mobileKeyboard: function (option) {
				let $this = this;
				option = $.extend($.fn.mobileKeyboard.defaults, option);
				let _obj = {
					pointBtn: $("[data-code='.']"),     // 点按钮
					xBtn: $("[data-code='x']"),         // x按钮
					emptyBtn: $("[data-code='']"),      // 空按钮
					focusTimer: null,                   // 模拟焦点的定时器
					/**
					 * @desc 初始化键盘
					 * @author: Demigodliu
					 * @date: 2020/06/13 21:13
					 **/
					init() {
						let that = this,
								{emptyBtn, pointBtn, xBtn} = that;

						// 根据用户传的键盘类型来初始化键盘
						switch (+option.type) {
							case 0:
								emptyBtn.removeClass("hide");
								pointBtn.addClass("hide");
								xBtn.addClass("hide");
								break;
							case 1:
								emptyBtn.addClass("hide");
								pointBtn.removeClass("hide");
								xBtn.addClass("hide");
								break;
							case 2:
								emptyBtn.addClass("hide");
								pointBtn.addClass("hide");
								xBtn.removeClass("hide");
								break;
						}

						// 键盘子项绑定点击事件
						that.handleItemClick();

						// 完成按钮绑定点击事件
						that.complated();

						// 屏蔽原生键盘,拉起自定义键盘
						$this.blur();
						$(".mobile-keyboard").slideDown();

						// 点击半透明蒙版，等同于完成按钮的点击事件
						$('.mobile-keyboard-shadow').unbind("click")
								.bind("click", () => { $(".mobile-keyboard_finish > span").click() });

						// 开启模拟焦点
						that.simulationFocus();
					},
					/**
					 * @desc 键盘子项绑定点击事件
					 * @author: Demigodliu
					 * @date: 2020/06/13 21:14
					 **/
					handleItemClick() {
						let that = this,
								timerStatus = false;

						$(".mobile-keyboard_main-item").unbind("click").bind("click", function () {
							// 如果正在执行中，直接退出
							if(timerStatus){ return false; }
							timerStatus = true;

							// 关闭模拟焦点
							that.stopSimulationFocus();

							// 获取当前按钮值
							let code = $(this).data("code");
							let val = $this.val();
							// 判断是否有特殊键位
							switch(code){
								case ".":
									if(val.indexOf(".") === -1){
										$this.val(val + code);
									}
									break;
								case "delete":
									$this.val(val.substr(0, val.length - 1));
									break;
								default:
									$this.val(val + code);
							}
							// 输入完成，重新开启模拟焦点
							that.simulationFocus();
							timerStatus = false;

							return false;
						});
					},
					/**
					 * @desc 模拟焦点
					 * @author: Demigodliu
					 * @date: 2020/06/13 22:51
					 **/
					simulationFocus(){
						let that = this;

						that.focusTimer = setInterval(() => {
							if($this.val().indexOf("|") !== -1){
								$this.val($this.val().substr(0, $this.val().length - 1));
							}else{
								$this.val($this.val() + '|');
							}
						},500);
					},
					/**
					 * @desc 暂停模拟光标
					 * @author: Demigodliu
					 * @date: 2020/06/13 22:58
					 **/
					stopSimulationFocus(){
						let that = this;
						clearInterval(that.focusTimer);
						$this.val($this.val().replace(/\|/, ''));
					},
					/**
					 * @desc 完成按钮点击事件
					 * @author: Demigodliu
					 * @date: 2020/06/13 21:34
					 **/
					complated(){
						let that = this;

						$(".mobile-keyboard_finish > span").unbind("click").bind("click", () => {
							// 关闭模拟光标
							that.stopSimulationFocus();

							// 如果是小数键盘，去掉小数点前多余的零
							if(+option.type === 1 && !!$this.val()){ $this.val(+$this.val()); }

							// 收起键盘后销毁键盘
							$(".mobile-keyboard").slideUp();
							$(".inputAlone").css('bottom','0')
							return false;
						})
					}
				};
				_obj.init();
			}
		});

		// 向页面填充键盘
		$("body").append(`
        <div class="mobile-keyboard">
            <div class="mobile-keyboard_finish"><span>完成</span></div>
            <div class="mobile-keyboard_main">
                <div class="mobile-keyboard_main-item" data-code="1">1</div>
                <div class="mobile-keyboard_main-item" data-code="2">2</div>
                <div class="mobile-keyboard_main-item" data-code="3">3</div>
                <div class="mobile-keyboard_main-item" data-code="4">4</div>
                <div class="mobile-keyboard_main-item" data-code="5">5</div>
                <div class="mobile-keyboard_main-item" data-code="6">6</div>
                <div class="mobile-keyboard_main-item" data-code="7">7</div>
                <div class="mobile-keyboard_main-item" data-code="8">8</div>
                <div class="mobile-keyboard_main-item" data-code="9">9</div>
                <div class="mobile-keyboard_main-item" data-code=""></div>
                <div class="mobile-keyboard_main-item hide" data-code=".">.</div>
                <div class="mobile-keyboard_main-item hide" data-code="x">x</div>
                <div class="mobile-keyboard_main-item" data-code="0">0</div>
                <div class="mobile-keyboard_main-item" data-code="delete"></div>
            </div>
            <div class="mobile-keyboard-shadow"></div>
        </div>
    `);

		$.fn.mobileKeyboard.defaults = {
			type: 0 // 类型 {纯数字： 0， 小数： 1， 身份证 2}
		};
	})(jQuery);
</script>
<script>
	$('.float').bind("click", function(){
		$(this).mobileKeyboard({type: 1});
		$(".inputAlone").css('bottom','300px')
	});
</script>
<style>

	.mobile-keyboard {
		position: fixed;
		left: 0;
		bottom: 0;
		display: none;
		width: 100%;
		border-top: 1px solid #e0dfdf;
		background-color: #d6d8dd;
		z-index: 19940405;
		font-family: -apple-system-font, Helvetica Neue, sans-serif;
	}

	.mobile-keyboard_finish {
		display: flex;
		justify-content: flex-end;
		color: #00a1ff;
		font-weight: bold;
		font-size: 16px;
		line-height: 16px;
		padding: 16px 20px;
		letter-spacing: 1px;
		background-color: #f9f9f9;
	}

	.mobile-keyboard_main {
		display: flex;
		flex-flow: row wrap;
		width: 100%;
		height: calc(100% - 8px);
		padding: 8px 8px 0;
	}

	.mobile-keyboard_main-item {
		width: calc((100% - 32px) / 3);
		height: 46px;
		line-height: 46px;
		margin-bottom: 8px;
		text-align: center;
		color: #333;
		border-radius: 6px;
		font-size: 26px;
		background-color: #fff;
	}

	.mobile-keyboard_main-item:active {
		background-color: #bec9d5;
	}

	.mobile-keyboard_main-item:not(:nth-child(3n)) {
		margin-right: 8px;
	}

	[data-code=''] {
		background-color: transparent;
	}

	[data-code='x'] {
		margin-right: 8px;
	}

	.mobile-keyboard_main-item:last-child {
		background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkEAYAAAAgckkXAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAASAAAAEgARslrPgAAEdFJREFUeNrt3XtcVFUeAPDfmRloJVjRymyLavnQQ1wfzB0IUVE0xBCCNcZXKJK64n4SbAMfpO5qPiDWRMIPwtZHMDUQwQoQDEWzdeIxc0f9wz5mgW70WLUVy4CamfvbPw6XPlIT94Izl8f5/uNn7sw5c8+R35x773kBMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAwz8BGlT0BpAZcCLgVc+v3v22a3zW6brVYrfT7Mr7NtsG2wbbBYLsy5MOfCnFu3nPW9gy5AuBquhqtZtAi+hC/hy4wMyIRMyBwxQunzYuRobcUtuAW3NDWRV8gr5JWqKtJEmkjTkSPGG8Ybxhtnztypbxo0AcJpOS2nXboUdKADXV4emMAEJjJoyj+4nDwp3CXcJdy1erXZYDaYDUZjT3NSKV0UR/Nr9Gv0a4yMRCMa0ZiTwwJjMAgJIZkkk2TW1nIFXAFXsGZNT3MasH8oumG6YbphEycK14XrwvXqahJAAkjAkCFKnxejkEWwCBb985+mJFOSKSklRWqyARcg/tn+2f7Zo0fbxtvG28afPk2SSBJJGj5c6fNi+gYiEIEICQlGs9FsNOfmdvt5pU/4TtG16lp1rV5eQp1QJ9QZDCSZJJPkhx6SnAECAl6/DiEQAiFXrihdHqaLYAiGYI0GWqAFWh5+GLIgC7KGDZOdzwpYASt+/FH9nvo99XujRtWX1ZfVlzU12ft4vw+QwC8Cvwj8YvhwS4OlwdLw0UewFbbCVl9fyRmsglWw6upVW4OtwdYwadLZrLNZZ7MuXVK6XMyv0+v1er1erW4KbQptCp0xA+/H+/H+7dthM2yGzePGSc4oCqIg6sAB00bTRtPG2Fh7H+u3AcJxHMdxbm6wBbbAluPHYT2sh/UTJkhNj2/gG/jG999jMiZj8rRpvX3awSiD/kAOGWLJteRacg8dgkqohMqIiG4TLoflsFwQVNWqalX1gw82FDcUNxR/803Xj/W7p1hTcApOQY0GnoFn4JmiIrmBQR/z/vQT8SW+xPe551hg9G+1XrVetV5tbW1j28a2jZ0/H4IgCIIkXCLnQi7kqlS2WFusLTYy0t7H+lmAEHLr4q2Lty7m5Un+pRB1/GJAHMRB3OLFJk+Tp8mzulrpEjF3RmcPuwEMYEhPl5yQAw64kBB7b/ebAOGyuWwue+tWeB6eh+fj4+Wmx1IsxdKXXzYFmYJMQe+8o3R5GMcQ5gvzhflHj0r9PMkiWSTL/sOcPh8gtAc8MRH2wl7Yu26d3PQ4Fafi1Nde46v4Kr4qM1Pp8jCO5VPnU+dT19zcecXQHX/wB/+RI+293WcDROuiddG6zJ0LCZAACTt3ys6g4ykFv4Pfwe9Yu1bp8jDOUVxcXFxcbLPhUlyKSyUEiA1sYHNxsfd2nwsQ+nQqJITkkBySU1Ag3kzJy6Wiwn2D+wb3DYsX09eISpeL6Z/6TIBoR2hHaEeMHQsBEAABpaWQAzmQc9ddUtNjDdZgTX29ZZ5lnmXe3Lkfkg/Jh8RqVbpcTP+meIDoynXlunJvb2iGZmg+dgzqoR7qPT0lZ7AW1sLaS5esedY8a15k5PmU8ynnU374QelyMQODYgEy/vT40+NP33ef4Cq4Cq6VlWQCmUAm2L9Z+oUJMAEmfPWVcEW4IlwJDaWBcfWqUuVhBiaNs79w4j0T75l4j4dH+0vtL7W/VFVFjz7+uLxcbt4UcoVcITc83GwxW8wWNnaKcQyntSD0ca2LS/uj7Y+2P3r4MD2q1UpNj7txN+5ub8dROApHRUbSwDh3zvlVxgwmTgoQQjAO4zDurbfo6xkzpKbEHMzBHJtNdVB1UHUwNpbfz+/n93/0kRKVxQw+Dr/Eoo9tMzNhH+yDfQsXys4gARIg4a9/NfJG3siXlChSSw6vHzc34ZRwSjg1cyY5Qo6QI0OGuGhcNC6a48frFtQtqFvw3/8652xUKno+CxfiQTyIBzkOlsEyWHbx4ncx38V8F/Pmm58lfpb4WeKPPypdb87isBaEVrTYQZeYKDc9bTn+/nee53mez8tTroocw1/vr/fXjxxJn8KdPauaqpqqmlpSQnaRXWTX/v1Wi9VitXzyiZ/Bz+BnmDnTUechBqj2uva69rp4T5ifTxaQBWTBypXkB/ID+SE7e2j+0Pyh+R9+KF4qK11/znLHA0Tro/XR+sTH00Fg27b1LJecHD6AD+ADNm9WuoIcRUgX0oX0TZsgDdIg7bHHfvGBjglBqguqC6oL777LNXPNXHN4+J36/s7pAgAAUF5OwkgYCQsNtZuAAAHy1FP0X71e6fpzljsWINpt2m3abRER8Cl8Cp/KXzUEl+NyXF5S4u3t7e3tvXKl0hXjcBfhIlwcM6bbz4kdplVQBVWlpb0NlK6BQf+1P5r1FxIhERK9vJStPOfpdYD4Lfdb7rd8wgRSQkpISVER8Sf+xF8j+d4Gq7Eaq0+d8ljmscxjWWysOJZG6YpxuPWwHtafPSv5810DheM4jps1S2ryXgcGBxxwiIJBMAiG06eVrj5n6XGA+H3v973f96NGkWySTbLLyuhR8T9Ago2wETaeO0eeJk+Tp6Oj6dCQ9nalK8RZ6JCYf/wDDWhAwxdfSE4oBsoKWAErSkq6C5SxGWMzxmbcfTf9IaqooEdlBIZoNIyG0Tt2mHPNuebcjz9Wuv6cRXaAjEsYlzAu4cEHVamqVFVqZSUJJIEk8J57pKbHdEzH9KYmOof4mWdMvIk38TdvKl0Rzib2/NOpv5MnQwqkQMrly5IzEANFBzrQlZbqXtW9qnv12WfFt8UWw+Vbl29dvn3/fRJKQkno1KlyzxOfw+fwudxculzO6tVK15uzSQ6QMWPGjBkzZtgwdZQ6Sh117BidufXII5K/KRACIfDaNfWT6ifVT86caTKZTCbT118rXQFKMyebk83JV65ABmRAxtNPy25RjGAEo6srPoAP4AOHDmnztHnavDlzOluMaqiG6mnTenZ2WVl8Kp/Kp65YQV8PvlHR3QaIOCne1dXV1dX1/ffJerKerB89Wt7XtLYKVsEqWKOiGh5qeKjhoU8/VbrgfQ39wfj88962KCSX5JLcoqKethh0KnNmJj2fVavowcEXGCK7ASIur/JT9U/VP1UXFtKjkybJy95iwTAMw7DZswfbtWtP9bpF6SkxMEaYRphG/O1v9ODgDQyR3QBpOtN0pulMYiLZTXaT3T9f23ar42kHJmESJsXH89v4bfy2Y8eULmh/I7YoJIgEkaCQEIcFCguM32Q3QHAkjsSRc+fKztEEJjClpPCL+EX8ogMHlC5gf9frSy97bguMl16iB1lgdGX/HoQAASJjSMFtk+Q//1zpgg00NrShDa9fBx544HvRknT8P9F7lZMnlS5XX2e/BdmLe3GvjHWjOuaOYz3WY/3Bg9pYbaw2dvJkpQvY34n9GJolmiWaJeXlcAJOwIle1Kv4/9SMzdhcXCxuD6F0OfsquwFC4kk8iRfHQvG81AzFbQboU5T33tPF6mJ1sX/6k9IF7W86A+MRzSOaRyoqevxUyp6Ox8OqdFW6Kv3w4a79KAxlN0DotW9rq22nbadt58yZuB2343YZj2c7BtuhJ3qi57Fj43AcjsNHH1W6wH1dZwdfoUuhS2FZGUkn6SR9yhSHfaHYj9LRotDvj45Wuh76im77Qc4Gnw0+G3ztGllH1pF14eG4E3fiThnzEz6Gj+HjP/xBw2k4DVdZ6d/s3+zfLL3nfbAQWwzYDtthe2UlPSp/SAgWYzEWixPT/vMfyQk7AoX2zBcVsRaFktyTLj5NgRiIgZgZM+jyPC0tkr+JAAHy5JM2D5uHzePoUd9Dvod8D7m7K10BSuvaYsA6WAfrgoNlZ9TxVIr35r1576VL6RKcwcGdQ3ukYi3KbWSPxeKv8lf5q+fPC9OEacK0mBhxtXSp6ck0Mo1MCwgYkj4kfUh6YWHnau2DjDhCodctRhiGYdjOnbc/rv25w1GzS7NLs2v6dMmrnou6tChyRw8PFD0ezWvWm/Vm/YkTdMZZXJzktVBvM2vWLd0t3S3dm2/S14Nnc01LhaXCUrF2bU9bDDEwaEes2MH3S+IOSurL6svqyyEhPQ4UBATMz6czCocOVbr+nKXX80GM+437jfsLC2EP7IE9P/+CyRMXpz2vPa89v3270hXiLDgf5+N8+VNp8W18G99+/fXuAqOrXgcKAQLk3nuhBmqgJiBA6fpzljs2o5AOW8/KgniIh3j5f+j0sfKaNdrj2uPa40lJSleMo5EMkkEypC901xkYvrwv7/vyyz39XjFQrFnWLGvW1KlyAwUfx8fxcWctIqG8Oz4n3fSi6UXTi6+8AgfgABzYu1duetJIGknj669zBs7AGebPV7qCHIWOcUtLo68sFrsfREDAHTt6GxhdnSPnyDly+bIYKNJu5t99V7wHVbr+nMVBq5oguj/h/oT7E3/5C90qTZziKYG4mnsBFEBBfj7XwrVwLb+xmEA/ZbxhvGG8ceYMfRUYCHrQg/5f/xK3bSDRJJpER0XRljk52VHnIQYKWU1Wk9V+fvToxo2QD/mQv38/3o13490vvuhudDe6GwfPYg0ih98Ud26yGG2JtkR/8AE9KnfYfGsr3of34X3Tp9ONcGprlagspv/QClpBK1gs3a6R0DH40zTPNM80749/7Pq2w1dWFDdZpJcKERF0v2u5TbSbG7lKrpKrZWW0A+uJJxx93gwD4MS1ecW559Y2a5u1LTy8p09RhGQhWUg+erRz4TWGcSCnb39wbs+5Pef2fPmlTW/T2/Th4bgLd+Gu//1PanoyiUwik7y9hUahUWj84AM6xkvGfiIMI4Ni+4PQMV4XLpBEkkgSw8PpZopyN74ZM0Z9Q31DfePIEZ8snyyfLOk7UjGMFIrvMEUvverq8GF8GB+eNw8bsAEbpG+dJg4DH1owtGBoQWGhOJde6XIxA4PiASKiy8uUl6ueVT2revaFF8S57fJyiY5ubGxsbGx84w2ly8MMDH0mQETGcmO5sfztt8k+so/s27ChZ7msWEFnNKamKl0epn/rcwEiMrYb243tW7diJEZiZHa23PTEjbgRty1bdMG6YF3wCy8oXR7GmQihgyy7H/yKURiFUfYH2faT0bMqFR1FWlgod/l98Z4GL+NlvDx7ttnb7G32FtcSZgYa+ndy77307+TatW4TICBgXR29Fw4M7Pp2n21BbicINxffXHxz8cKFEAqhEFpTIzWl2JOq0qv0Kn1hoa5V16prDQpSukSMY9BuABlTlCMgAiK++sre2/0kQADErb/US9RL1Ev+/Gc8hafwlNksLxc3N4EXeIEvK6PbUPv6Kl0u5k4iRJgiTBGmSJ8GABVQARX19XZzVLpIPSX2pNv22fbZ9p05I3YgSk0vrlSosqlsKtvEiUY3o5vRzQlLfDIOQWc8ig91pO9MRveG9PU1e5g9zB6ffNL1/X7TgnTVUNxQ3FD8zTdoRjOaw8JgFayCVdLnV9AlPb28sAqrsKqqig6qHD5c6XIx0oj3Glwal8aliXtYSg8MTMVUTDUY7AWGqN+2IF1ptVqtVstxkA3ZkH3yJFlJVpKVHh6SM9gEm2CT0Ug7LE+cULo8TBca0IDGxYW0kBbS8thjsBJWwsrp0+mbMjZuEiEg4OTJ9Ob83/+297EBEyCizvkja2ANrCkv75xTzTAAgEVYhEV79vA+vA/vI+57Yl+/vcSyx+Rp8jR5VldDHMRB3OLFPVtMghloxL0wyRwyh8yRvi35gAsQkSnIFGQKeucd9EIv9BoEu+YyvwrLsAzLDh+2vmV9y/pWRAS9pPqNKc5dDLhLLHvoqilpaeLiEEqfD+MYWIu1WPvtt+Qp8hR5avNmGhDi2Dz52zsMmgDpKC4RlxcitaSW1KakdM6BZ/oXcaOmWTgLZxmNZDPZTDaXllqNVqPVuGcPnWsvY+VPOwZZgPyMPjd/4AGhWCgWinU6VYwqRhXzu98pfV7MrxMOC4eFw+3tEAuxEPv115iGaZh25Yq4drTS58cwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMP0Df8H3yRrHwxOvsMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMDktMDRUMTU6NDg6NDArMDg6MDC8sItTAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTA5LTA0VDE1OjQ4OjQwKzA4OjAwze0z7wAAAEh0RVh0c3ZnOmJhc2UtdXJpAGZpbGU6Ly8vaG9tZS9hZG1pbi9pY29uLWZvbnQvdG1wL2ljb25fcGpnenFmaGgwMy9kZWxldGUuc3ZnjkC/KgAAAABJRU5ErkJggg==") no-repeat center center / 26px;
	}

	.mobile-keyboard-shadow{
		position: absolute;
		left: 0;
		top: -100vh;
		width: 100%;
		height: 100vh;
		background-color: rgba(0,0,0,.5);
		z-index: -1;
	}
</style>