<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/html" id="DATA_TR">
{{each games as val i}}
	<!-- 输出联赛 -->
	{{if i== 0 || games[i-1].league != val.league }}
		<div class="bg-titleliansai">
			<span class="Legimg {{$displayLegClass val.league}}"></span>{{val.league}}
		</div>
	{{/if}}	
<div class="panel panel-success" league="{{val.league}}" style="{{$displayLeg val.league}}">
   <div class="panel-heading">
       {{$formatDate gameType val}}
    </div>
    <div class="panel-body">
        <table>
            <thead>
                <tr class="bg-title">
                    
                    <td class="width30">球队</td>
                    <td>主/主</td>
                    <td>主/和</td>
                    <td>主/客</td>
                    <td>和/主</td>
                    <td>和/和</td>
                    <td>和/客</td>
                    <td>客/主</td>
                    <td>客/和</td>
                    <td>客/客</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$teamName val.home}}<br />{{$teamName val.guest}}</td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FHH 'ior_FHH' val.gid '主主'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FHN 'ior_FHN' val.gid '主和'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FHC 'ior_FHC' val.gid '主客'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FNH 'ior_FNH' val.gid '和主'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FNN 'ior_FNN' val.gid '和和'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FNC 'ior_FNC' val.gid '和客'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FCH 'ior_FCH' val.gid '客主'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FCN 'ior_FCN' val.gid '客和'}}</span></td>
                    <td><span class="text-red font-bold">{{$showOdds val.ior_FCC 'ior_FCC' val.gid '客客'}}</span></td>
                 </tr>
            </tbody>
        </table>
    </div>
</div>
{{/each}}
</script>