<%@ page language="java" pageEncoding="UTF-8"%>

<script  type="text/html" id="DATA_TR">
{{each data as val i}}
<!-- 输出联赛 -->
	{{if i== 0 || data[i-1].league != val.league }}
		<div class="bg-titleliansai">
			<span class="Legimg"></span>{{val.league}}
		</div>
	{{/if}}
	
    <div class="panel panel-success" league='{{val.league}}' style="display:none;">
        <div class="panel-body">
            <table class="game result">
                <thead>
                    <tr class="bg-title">
                        <td class="rdt" style="width:50px">时间</td>
                        <td colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;赛果</td>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TrBgOdd ">
                        <td rowspan="9" style="width:50px">{{$formatTime val.startTime}}</td>
                        <td class="rname" style="width:50px;">比赛队伍</td>
                        <td colspan="2"  style="width:100px;text-align:center"><b style="color:#c00">{{val.homeTeam}}</b></td>
                        <td colspan="2" style="width:100px;text-align:center">{{val.guestTeam}}</td>
                    </tr>
                    <tr class="TrBgOdd lclass">
                         <td class="{{$printClass val.scoreTime1H false 'bad'}}">第1节</td>
                         <td colspan="2">{{val.scoreTime1H}}</td>
                         <td colspan="2">{{val.scoreTime1G}}</td>
                    </tr>
                    <tr class="TrBgOdd lclass">
                         <td class="{{$printClass val.scoreTime2H false 'bad'}}">第2节</td>
                         <td colspan="2">{{val.scoreTime2H}}</td>
                         <td colspan="2">{{val.scoreTime2G}}</td>
                    </tr>
                    <tr class="TrBgOdd lclass">
                         <td class="{{$printClass val.scoreTime3H false 'bad'}}">第3节</td>
                         <td colspan="2">{{val.scoreTime3H}}</td>
                         <td colspan="2">{{val.scoreTime3G}}</td>
                    </tr>
                    <tr class="TrBgOdd lclass">
                         <td class="{{$printClass val.scoreTime4H false 'bad'}}">第4节</td>
                         <td colspan="2">{{val.scoreTime4H}}</td>
                         <td colspan="2">{{val.scoreTime4G}}</td>
                    </tr>
                    <tr class="TrBgOdd odd lclass" style="background-color: #eaeaea;">
                        <td class="{{$printClass val.scoreH1H false 'bad'}}">上半</td>
                        <td colspan="2">{{val.scoreH1H}}</td>
                        <td colspan="2">{{val.scoreH1G}}</td>
                    </tr>
                    <tr class="TrBgOdd odd lclass" style="background-color: #eaeaea;">
                        <td class="{{$printClass val.scoreH2H false 'bad'}}">下半</td>
                        <td colspan="2">{{val.scoreH2H}}</td>
                        <td colspan="2">{{val.scoreH2G}}</td>
                    </tr>
                    <tr class="TrBgOdd lclass">
                        <td>加时</td>
                        <td colspan="2" >{{$showAddTimeScore scoreAddH}}</td>
                        <td colspan="2" >{{$showAddTimeScore scoreAddG}}</td>
                    </tr>
                    <tr class="TrBgOdd even lclass" style="background-color: #d4e8ff; color: #c00;">
                        <td class="{{$printClass val.scoreFullH false 'bad'}}{{$printClass val.scoreFullH true 'red'}}">全场</td>
                        <td colspan="2" style="color:#c00">{{val.scoreFullH}}</td>
                        <td colspan="2" style="color:#c00">{{val.scoreFullG}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
{{/each}}
</script>