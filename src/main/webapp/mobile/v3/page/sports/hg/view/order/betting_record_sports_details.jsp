﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/global.css?v=2" type="text/css" />
</head>
<body >
<div class="top">
	<div class="inner">
		
		<div class="back">
			<a href="${base}/mobile/sports/hg/goOrderPage.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">体育投注记录</span>
		</div>
		<div class="cl"></div>
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="shangqkaij tac" style="background:#dfdfdf;">
		<span class="tac co000">投注记录明细</span>
		<div class="cl"></div>
	</div>
	<div class="bgf6 ov">
		<table class="tymingxtab">
			<tr>
				<td class="bttd" width="100px">
					<div class="tymingxtabdiv">注单编号</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="bettingCode"></div>
				</td>
			</tr>
			<tr>
				<td class="bttd" width="100px">
					<div class="tymingxtabdiv">投注时间</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="bettingDate"></div>
				</td>
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赛事</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="matchInfo">
		
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">球类</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="sportType"></div>
				</td>
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">类型</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="typeNames">
						
					</div>
				</td>
			</tr>
			<!-- 
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赔率</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="odds">
						
					</div>
				</td>
			</tr> -->
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">下注金额</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="bettingMoney"></div>
				</td>
				
			</tr>

			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">提交状态</div>
				</td>
				<td>
					<div class="tymingxtabdiv"  id="bettingStatus"></div>
				</td>
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">结算状态</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="balance"></div>
				</td>
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv" >派彩金额</div>
				</td>
				<td>
					<div class="tymingxtabdiv" id="bettingResult">-</div>
				</td>
				
			</tr>
		</table>
		<div class="cl h80"></div>
	</div>
</div>
</body>

<script type="text/javascript" src="${base}/mobile/script/jquery-1.11.1.min.js"></script>
<script src="${base}/common/js/artTemplate/template.js"></script>
<script src="${base}/mobile/sports/hg/js/core.js" path="${base}"></script>

</html>
<script language="javascript">
	var order = ${orderJson};
	(function(){
		$("#odds").html(order.odds);
		$("#bettingMoney").html(order.bettingMoney);
		$("#bettingCode").html(order.bettingCode);
		$("#bettingDate").html(getBettingDate());
		$("#sportType").html(getSportName(order.sportType));
		$("#bettingStatus").html(getBettingStatus());
		$("#balance").html(getBalanceStatus());
		$("#matchInfo").html(getMatchInfo());
		$("#typeNames").html(order.typeNames);
		if(order.bettingResult == undefined){
			
		}else if(order.bettingResult > 0){
			$("#bettingResult").html("<font color='green'>"+order.bettingResult+"</font>");
		}else{
			$("#bettingResult").html(order.bettingResult);
		}
	})();
	
	function getBalanceStatus(){
		var value = order.balance;
		if(value == 1){
			return "<font color='blue'>未结算</font>";
		}
		
		if(value == 2 || value == 5 || value == 6){
			return "<font color='green'>已结算</font>";
		}
		
		if(value == 3){
			return "<font color='red'>结算失败</font>";
		}
		
		if(value == 4){
			return "<font color='red'>比赛腰斩</font>";
		}
	}
	
	function getBettingDate(){
		var betDate = new Date(order.bettingDate);
		return betDate.format("MM月dd日,hh:mm:ss");
	}
	
	function getBettingStatus(){
		var value = order.bettingStatus;
		if(value == 1){
			return "<font color='blue'>待确认</font>";
		}
		if(value == 2){
			return "已确认";
		}
		
		if(value == 3){
			var content = "取消";
			if(order.statusRemark){
				content += "("+order.statusRemark+")";
			}
			return "<font color='red'>" + content + "</font>";
		}
		
		if(value == 4){
			return "<font color='red'>取消</font>";
		}
	}
	
	function getSportName(type){
		if(type == 1){
			return "足球";
		}
		if(type == 2){
			return "篮球";
		}
		return "其他";
	}
	

	function toBetHtml(item){
		var row = order;
		var con = item.con;
		if(con.indexOf("vs") == -1){
			con = '<span class="text-danger">'+ con +'</span>';
		}
		var homeFirst = row.homeTeam  == item.firstTeam;//主队是否在前
		var scoreStr = "";
		
		
		if(row.gameTimeType == 1){
			if(homeFirst){
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH +":" + row.scoreC + ")</b></font>";
			}else{
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC +":" + row.scoreH + ")</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if(item.half === true && row.mix == 2){
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}
		
		var html  = item.league +"<br/>" + 
					home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
					"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
		var balance = row.mix != 2 ? row.balance : item.balance;
		var bt = row.bettingStatus;
		if(balance == 4){
			html = "<s style='color:red;'>" + html+"(赛事腰斩)</s>"
		}else if(bt == 3 || bt == 4){
			html = "<s style='color:red;'>" + html+"("+row.statusRemark+")</s>"
		}else if(balance == 2 || balance == 5 || balance == 6){
			var mr = row.mix != 2 ? row.result:item.matchResult;
			if(homeFirst){
				html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
			}else{
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>("+ss[0]+":"+ss[1]+")</font>";
			}
		}			
		return html;
	}


	function getMatchInfo(){
		if(order.mix != 2){
			return toBetHtml(JSON.decode(order.remark));
		}
		var html = "";
		var arr = JSON.decode(order.remark)
		for(var i=0;i<arr.length;i++){
			if(i != 0){
				html += "<div style='border-bottom:1px #303030 dotted;'></div>";
			}
			html += toBetHtml(arr[i]);
		}
		return html;
	}

</script>