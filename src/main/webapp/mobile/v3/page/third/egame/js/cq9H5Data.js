var cq9data = [ {
	"ButtonImagePath" : "65_cn.png",
	"DisplayName" : "黄金渔场",
	"typeid" : 1,
	"LapisId" : "AB1"
}, {
	"ButtonImagePath" : "52_cn.png",
	"DisplayName" : "跳高高",
	"typeid" : 1,
	"LapisId" : "52"
}, {
	"ButtonImagePath" : "7_cn.png",
	"DisplayName" : "跳起来",
	"typeid" : 1,
	"LapisId" : "7"
}, {
	"ButtonImagePath" : "24_cn.png",
	"DisplayName" : "跳起来2",
	"typeid" : 1,
	"LapisId" : "24"
},{
	"ButtonImagePath" : "10_cn.png",
	"DisplayName" : "五福临门",
	"typeid" : 1,
	"LapisId" : "10"
},{
	"ButtonImagePath" : "31_cn.png",
	"DisplayName" : "武圣",
	"typeid" : 1,
	"LapisId" : "31"
}, {
	"ButtonImagePath" : "33_cn.png",
	"DisplayName" : "火烧连环船",
	"typeid" : 1,
	"LapisId" : "33"
}, {
	"ButtonImagePath" : "16_cn.png",
	"DisplayName" : "五行",
	"typeid" : 1,
	"LapisId" : "16"
}, {
	"ButtonImagePath" : "15_cn.png",
	"DisplayName" : "金鸡报喜",
	"typeid" : 1,
	"LapisId" : "15"
},{
	"ButtonImagePath" : "64_cn.png",
	"DisplayName" : "宙斯",
	"typeid" : 1,
	"LapisId" : "64"
},{
	"ButtonImagePath" : "58_cn.png",
	"DisplayName" : "金鸡报喜2",
	"typeid" : 1,
	"LapisId" : "58"
},{
	"ButtonImagePath" : "55_cn.png",
	"DisplayName" : "魔龙传奇",
	"typeid" : 1,
	"LapisId" : "55"
}, {
	"ButtonImagePath" : "50_cn.png",
	"DisplayName" : "洪福齐天",
	"typeid" : 1,
	"LapisId" : "50"
}, {
	"ButtonImagePath" : "1_cn.png",
	"DisplayName" : "钻石水果王",
	"typeid" : 1,
	"LapisId" : "1"
}, {
	"ButtonImagePath" : "9_cn.png",
	"DisplayName" : "钟馗运财",
	"typeid" : 1,
	"LapisId" : "9"
}, {
	"ButtonImagePath" : "29_cn.png",
	"DisplayName" : "水世界",
	"typeid" : 1,
	"LapisId" : "29"
},   {
	"ButtonImagePath" : "56_cn.png",
	"DisplayName" : "暗夜公爵",
	"typeid" : 1,
	"LapisId" : "56"
}, {
	"ButtonImagePath" : "23_cn.png",
	"DisplayName" : "金元宝",
	"typeid" : 1,
	"LapisId" : "23"
},{
	"ButtonImagePath" : "70_cn.png",
	"DisplayName" : "万宝龙",
	"typeid" : 1,
	"LapisId" : "70"
}, {
	"ButtonImagePath" : "46_cn.png",
	"DisplayName" : "狼月",
	"typeid" : 1,
	"LapisId" : "46"
}, {
	"ButtonImagePath" : "44_cn.png",
	"DisplayName" : "豪华水果王",
	"typeid" : 1,
	"LapisId" : "44"
}, {
	"ButtonImagePath" : "39_cn.png",
	"DisplayName" : "飞天",
	"typeid" : 1,
	"LapisId" : "39"
}, {
	"ButtonImagePath" : "63_cn.png",
	"DisplayName" : "寻龙诀",
	"typeid" : 1,
	"LapisId" : "63"
} , {
	"ButtonImagePath" : "19_cn.png",
	"DisplayName" : "风火轮",
	"typeid" : 1,
	"LapisId" : "19"
}, {
	"ButtonImagePath" : "57_cn.png",
	"DisplayName" : "神兽争霸",
	"typeid" : 1,
	"LapisId" : "57"
},  {
	"ButtonImagePath" : "54_cn.png",
	"DisplayName" : "火草泥马",
	"typeid" : 1,
	"LapisId" : "54"
},{
	"ButtonImagePath" : "38_cn.png",
	"DisplayName" : "舞力全开",
	"typeid" : 1,
	"LapisId" : "38"
}, {
	"ButtonImagePath" : "8_cn.png",
	"DisplayName" : "甜蜜蜜",
	"typeid" : 1,
	"LapisId" : "8"
},{
	"ButtonImagePath" : "43_cn.png",
	"DisplayName" : "恭贺新喜",
	"typeid" : 1,
	"LapisId" : "43"
}, {
	"ButtonImagePath" : "49_cn.png",
	"DisplayName" : "寂寞星球",
	"typeid" : 1,
	"LapisId" : "49"
}, {
	"ButtonImagePath" : "48_cn.png",
	"DisplayName" : "莲",
	"typeid" : 1,
	"LapisId" : "48"
}, {
	"ButtonImagePath" : "26_cn.png",
	"DisplayName" : "777",
	"typeid" : 1,
	"LapisId" : "26"
}, {
	"ButtonImagePath" : "53_cn.png",
	"DisplayName" : "来电99",
	"typeid" : 1,
	"LapisId" : "53"
}, {
	"ButtonImagePath" : "12_cn.png",
	"DisplayName" : "金玉满堂",
	"typeid" : 1,
	"LapisId" : "12"
}, {
	"ButtonImagePath" : "62_cn.png",
	"DisplayName" : "非常钻",
	"typeid" : 1,
	"LapisId" : "62"
},{
	"ButtonImagePath" : "6_cn.png",
	"DisplayName" : "1945",
	"typeid" : 1,
	"LapisId" : "6"
},  {
	"ButtonImagePath" : "59_cn.png",
	"DisplayName" : "夏日猩情",
	"typeid" : 1,
	"LapisId" : "59"
}, {
	"ButtonImagePath" : "5_cn.png",
	"DisplayName" : "金大款",
	"typeid" : 1,
	"LapisId" : "5"
},{
	"ButtonImagePath" : "66_cn.png",
	"DisplayName" : "火爆777",
	"typeid" : 1,
	"LapisId" : "66"
}, {
	"ButtonImagePath" : "2_cn.png",
	"DisplayName" : "棋圣",
	"typeid" : 1,
	"LapisId" : "2"
}, {
	"ButtonImagePath" : "32_cn.png",
	"DisplayName" : "通天神探狄仁杰",
	"typeid" : 1,
	"LapisId" : "32"
},  {
	"ButtonImagePath" : "17_cn.png",
	"DisplayName" : "祥狮献瑞",
	"typeid" : 1,
	"LapisId" : "17"
},{
	"ButtonImagePath" : "30_cn.png",
	"DisplayName" : "三国序",
	"typeid" : 1,
	"LapisId" : "30"
}, {
	"ButtonImagePath" : "4_cn.png",
	"DisplayName" : "森林泰后",
	"typeid" : 1,
	"LapisId" : "4"
}, {
	"ButtonImagePath" : "40_cn.png",
	"DisplayName" : "镖王争霸",
	"typeid" : 1,
	"LapisId" : "40"
}, {
	"ButtonImagePath" : "51_cn.png",
	"DisplayName" : "嗨爆大马戏",
	"typeid" : 1,
	"LapisId" : "51"
}, {
	"ButtonImagePath" : "3_cn.png",
	"DisplayName" : "血之吻",
	"typeid" : 1,
	"LapisId" : "3"
}, {
	"ButtonImagePath" : "36_cn.png",
	"DisplayName" : "夜店大亨",
	"typeid" : 1,
	"LapisId" : "36"
}, {
	"ButtonImagePath" : "28_cn.png",
	"DisplayName" : "食神",
	"typeid" : 1,
	"LapisId" : "28"
}, {
	"ButtonImagePath" : "25_cn.png",
	"DisplayName" : "扑克拉霸",
	"typeid" : 1,
	"LapisId" : "25"
}, {
	"ButtonImagePath" : "45_cn.png",
	"DisplayName" : "超级发",
	"typeid" : 1,
	"LapisId" : "45"
}, {
	"ButtonImagePath" : "18_cn.png",
	"DisplayName" : "雀王",
	"typeid" : 1,
	"LapisId" : "18"
}, {
	"ButtonImagePath" : "27_cn.png",
	"DisplayName" : "魔法世界",
	"typeid" : 1,
	"LapisId" : "27"
},{
	"ButtonImagePath" : "41_cn.png",
	"DisplayName" : "水球大战",
	"typeid" : 1,
	"LapisId" : "41"
}, {
	"ButtonImagePath" : "20_cn.png",
	"DisplayName" : "发发发",
	"typeid" : 1,
	"LapisId" : "20"
}, {
	"ButtonImagePath" : "11_cn.png",
	"DisplayName" : "梦游仙境",
	"typeid" : 1,
	"LapisId" : "11"
}, {
	"ButtonImagePath" : "42_cn.png",
	"DisplayName" : "福尔摩斯",
	"typeid" : 1,
	"LapisId" : "42"
},{
	"ButtonImagePath" : "34_cn.png",
	"DisplayName" : "地鼠战役",
	"typeid" : 1,
	"LapisId" : "34"
}, {
	"ButtonImagePath" : "13_cn.png",
	"DisplayName" : "樱花妹子",
	"typeid" : 1,
	"LapisId" : "13"
}, {
	"ButtonImagePath" : "14_cn.png",
	"DisplayName" : "绝赢巫师",
	"typeid" : 1,
	"LapisId" : "14"
},{
	"ButtonImagePath" : "22_cn.png",
	"DisplayName" : "庶务西游二课",
	"typeid" : 1,
	"LapisId" : "22"
},{
	"ButtonImagePath" : "47_cn.png",
	"DisplayName" : "法老宝藏",
	"typeid" : 1,
	"LapisId" : "47"
}, {
	"ButtonImagePath" : "21_cn.png",
	"DisplayName" : "野狼传说",
	"typeid" : 1,
	"LapisId" : "21"
}];