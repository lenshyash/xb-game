$(function() {
	var data = agdata;
	var ismg = false;
	var type = "ag";
	var path = "common/template/third/egame/images";
	switch (code) {
	case 'mg':
		data = mgdata;
		type = "mg";
		ismg = true;
		path = "common/template/third/egame/images/v2";
		break;
	case 'pt':
		type = "pt";
		data = ptdata;
		path = "common/template/third/newEgame/images";
		break;
	case 'ptzr':
		type = "pt";
		data = ptzrdata;
		path = "common/template/third/newEgame/images";
		$(".head").find('h1').html('PT真人娱乐')
		break;
	case 'qt':
		type = "qt";
		data = qtdata;
		path = "common/template/third/newEgame/images";
		break;
	case 'isb':
		type = "isb";
		data = isbdata;
		path = "common/template/third/newEgame/images/isb/";
		break;
	case 'cq9':
		type = "cq9";
		data = cq9data;
		path = "common/template/third/newEgame/images/cq9/";
		break;
	case 'jdb':
		type = "jdb";
		data = jdbdata;
		path = "common/template/third/newEgame/images/jdb/";
		break;
	case 'ttg':
		type = "ttg";
		data = ttgdata;
		path = "common/template/third/newEgame/images/ttg/";
		break;
	case 'mw':
		type = "mw";
		data = mwdata;
		path = "common/template/third/newEgame/images/mw/";
		break;
	default:
		break;
	}
	var eachdata = {
		"data" : data,
		"ismg" : ismg,
		"type" : type,
		"path" : path
	};
	var html = template('gamelist_tpl', eachdata);
	$("#gamelist_div").html(html);
});

(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					layer.msg("[" + opt.url + "] 404 not found", {
						skin : ' '
					});
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					layer.msg("后台异常，请联系管理员!", {
						skin : ' '
					});
				} else if (ceipstate == 3) { // 业务异常
					layer.msg(data.msg, {
						skin : ' '
					});
				} else if (ceipstate == 4) {// 未登陆异常
					layer.msg("登陆超时，请重新登陆", {
						skin : ' '
					});
				} else if (ceipstate == 5) {// 没有权限
					layer.msg("没有权限", {
						skin : ' '
					});
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
})(jQuery);
var lodingHtmlStatus = true;
function iosEgameLink(gameId) {
    var browser = {
            versions: function() {
                var a = navigator.userAgent,
                    b = navigator.appVersion;
                return {
                    trident: a.indexOf("Trident") > -1,
                    presto: a.indexOf("Presto") > -1,
                    webKit: a.indexOf("AppleWebKit") > -1,
                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                    iPhone: a.indexOf("iPhone") > -1,
                    iPad: a.indexOf("iPad") > -1,
                    webApp: a.indexOf("Safari") == -1
                }
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
        };
	 if (browser.versions.ios) {
         //Android
         if(NewiOS){
        	 WTK.share(JSON.stringify({'method':'locaEgameLink',"gameId": gameId}));
             lodingHtmlStatus =  false;
         }
     }
}
function toMG(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';

	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
			+ csh + ',left=' + cleft + ',top=' + ctop
			+ ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardMg.do",
		sync : false,
		data : {
			gameType : 3,
			h5 : 1,
			gameid : gameid
		},
		success : function(json) {
			transMoney('mg',json)
			// if (json.success) {
			// 	windowOpen.location.href = json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}

function toAG(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';

	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
			+ csh + ',left=' + cleft + ',top=' + ctop
			+ ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardAg.do",
		sync : false,
		data : {
			h5 : 1,
			gameType : gameid
		},
		success : function(json) {
			transMoney('ag',json)
			// if (json.success) {
			// 	windowOpen.location.href = json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}

function toPT(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var csw = $(window.parent).width(), csh = $(window.parent).height(), windowOpen = window
			.open("", '_blank', 'width=' + csw + ',height=' + csh
					+ ',left=0,top=0,scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardPt.do",
		sync : false,
		data : {
			h5 : 1,
			gameCode : gameid
		},
		success : function(json) {
			transMoney('pt',json)
			// if (json.success) {
			// 	windowOpen.location.href = json.url;
			// } else {
			// 	layer.msg(json.msg, {
			// 		skin : ' '
			// 	});
			// 	return;
			// }
		}
	});
}
var htmlLoding = "<html><head><title>加载中...</title></head><body><center>加载中...</center></body></html>";
function lodingHtml(windowOpen){
	var new_doc = windowOpen.document.open("text/html","replace");
    new_doc.write(htmlLoding);
    new_doc.close();
}

function toQT(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';

	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
			+ csh + ',left=' + cleft + ',top=' + ctop
			+ ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardQt.do",
		sync : false,
		data : {
			gameId : gameid
		},
		success : function(json) {
			transMoney('qt',json)
			// if (json.success) {
			// 	windowOpen.location.href = json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}
function toTTG(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';
	
	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardTtg.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			transMoney('ttg',json)
			// if (json.success) {
			// 	windowOpen.location.href=json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}
function toMW(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';
	
	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardMw.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			transMoney('mw',json)
			// if (json.success) {
			// 	windowOpen.location.href=json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}
function toISB(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardIsb.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			transMoney('isb',json)
			// if (json.success) {
			// 	windowOpen.location.href=json.url;
			// } else {
			// 	layer.msg(json.msg);
			// 	return;
			// }
		}
	});
}

function toCQ9(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardCq9.do",
		sync : false,
		data:{
			h5 : 1,
			gameId : gameid
		},
		success : function(json) {
			transMoney('cq9',json)
			// if (json.success) {
			// 	windowOpen.location.href=json.url;
			// } else {
			// 	alert(json.msg);
			// 	return;
			// }
		}
	});
}
function toJDB(gameid) {
	iosEgameLink(gameid)
	if(!lodingHtmlStatus){
		return false;
	}
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';
	
	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	lodingHtml(windowOpen);
	$.ajax({
		url : base + "/forwardJdb.do",
		sync : false,
		data:{
			h5 : 1,
			gameId : gameid
		},
		success : function(json) {
			alert(1)
			transMoney('jdb',json)
			// if (json.success) {
			// 	windowOpen.location.href=json.url;
			// } else {
			// 	alert(json.msg);
			// 	return;
			// }
		}
	});
}

// 额度自动转换
var autoTranIs = false
autoTrans()
function autoTrans(){
	$.ajax({
		url:'/native/getStationSwitch.do',
		type:'post',
		success:function (res) {
			if (res.success){
				for (let [index,val] of res.content.entries()){
					if (val.switchKey == 'autoTrans'){
						if (res.content[index].onFlag =='on'){
							autoTranIs = true
						}
					}
				}
			}
		}
	})
}
function transMoney(changeTo,json){
	let params = {
		changeTo:changeTo,
		changeFrom:'sys'
	}
	if(autoTranIs){
		$.ajax({
			url:'/native/fastThirdRealTransMoney.do',
			type:'post',
			data:params,
			async:false,
			success:function (res) {
				sessionStorage.setItem("autoTrans", "true");
			}
		})
	}
	if (json.success) {
		location.href=json.url;
	} else {
		alert(json.msg);
		return;
	}
}