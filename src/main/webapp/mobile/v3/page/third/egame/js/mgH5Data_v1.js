var mgdata = [
	{
		"DisplayEnName": "Cricket Star Scratch",
		"DisplayName": "板球明星",
		"ButtonImagePath": "BTN_CricketStarScratch_ZH",
		"LapisId": "7076",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CricketStarScratch"
	},
	{
		"DisplayEnName": "Elven Gold",
		"DisplayName": "精灵黄金",
		"ButtonImagePath": "BTN_ElvenGold_ZH",
		"LapisId": "7074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ElvenGold"
	},
	{
		"DisplayEnName": "Hyper Gold",
		"DisplayName": "超猎黄金",
		"ButtonImagePath": "BTN_HyperGold_ZH",
		"LapisId": "7073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HyperGold"
	},
	{
		"DisplayEnName": "Legacy of Oz",
		"DisplayName": "奥兹Oz传奇",
		"ButtonImagePath": "BTN_LegacyofOz_ZH",
		"LapisId": "7072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LegacyofOz"
	},
	{
		"DisplayEnName": "Soccer Striker",
		"DisplayName": "足球前锋",
		"ButtonImagePath": "BTN_SoccerStriker_ZH",
		"LapisId": "7075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SoccerStriker"
	},
	{
		"DisplayEnName": "10000 Wishes",
		"DisplayName": "10000个愿望",
		"ButtonImagePath": "BTN_10000Wishes_ZH",
		"LapisId": "6742",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_10000Wishes"
	},
	{
		"DisplayEnName": "108 Heroes",
		"DisplayName": "108好汉",
		"ButtonImagePath": "BTN_108Heroes_ZH",
		"LapisId": "1302",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_108Heroes"
	},
	{
		"DisplayEnName": "108 Heroes Multiplier Fortunes",
		"DisplayName": "108好汉乘数财富",
		"ButtonImagePath": "BTN_108HeroesMF_ZH",
		"LapisId": "1897",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_108HeroesMF"
	},
	{
		"DisplayEnName": "5 Reel Drive",
		"DisplayName": "5卷的驱动器",
		"ButtonImagePath": "BTN_5ReelDrive1",
		"LapisId": "1035",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_5ReelDrive1"
	},
	{
		"DisplayEnName": "777 Mega Deluxe",
		"DisplayName": "777绝对豪华",
		"ButtonImagePath": "BTN_777MegaDeluxe_ZH",
		"LapisId": "6300",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_777MegaDeluxe"
	},
	{
		"DisplayEnName": "777 Royal Wheel",
		"DisplayName": "777皇家幸运轮",
		"ButtonImagePath": "BTN_777RoyalWheel_ZH",
		"LapisId": "6221",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_777RoyalWheel"
	},
	{
		"DisplayEnName": "8 Golden Skulls of Holly Roger Megaways ™",
		"DisplayName": "8个Holly Roger金骷髅Megaways™",
		"ButtonImagePath": "BTN_8GoldenSkullsofHollyRogerMegaways™_ZH",
		"LapisId": "6334",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_8GoldenSkullsofHollyRogerMegaways™"
	},
	{
		"DisplayEnName": "9 Blazing Diamonds",
		"DisplayName": "9颗炽热钻石",
		"ButtonImagePath": "BTN_9BlazingDiamonds_ZH",
		"LapisId": "6399",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9BlazingDiamonds"
	},
	{
		"DisplayEnName": "9 Masks of Fire",
		"DisplayName": "9个烈焰面具",
		"ButtonImagePath": "BTN_9MasksOfFire_ZH",
		"LapisId": "5363",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9MasksOfFire"
	},
	{
		"DisplayEnName": "9 Pots of Gold",
		"DisplayName": "9罐黄金",
		"ButtonImagePath": "BTN_9PotsofGold_ZH",
		"LapisId": "5619",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9PotsofGold"
	},
	{
		"DisplayEnName": "A Dark Matter",
		"DisplayName": "黑暗阴影",
		"ButtonImagePath": "BTN_ADarkMatter_ZH",
		"LapisId": "5382",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ADarkMatter"
	},
	{
		"DisplayEnName": "A Tale of Elves",
		"DisplayName": "精灵传说",
		"ButtonImagePath": "BTN_ATaleofElves_ZH",
		"LapisId": "6218",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ATaleofElves"
	},
	{
		"DisplayEnName": "Action Ops Snow & Sable",
		"DisplayName": "秘密行动雪诺和塞布尔",
		"ButtonImagePath": "BTN_ActionOpsSnow&Sable_ZH",
		"LapisId": "2076",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ActionOpsSnow&Sable"
	},
	{
		"DisplayEnName": "Adventure Palace HD",
		"DisplayName": "冒险宫殿",
		"ButtonImagePath": "BTN_AdventurePalaceHD",
		"LapisId": "1010",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AdventurePalaceHD"
	},
	{
		"DisplayEnName": "Adventures Of Doubloon Island™",
		"DisplayName": "金币岛大冒险",
		"ButtonImagePath": "BTN_AdventuresOfDoubloonIsland_ZH",
		"LapisId": "6304",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AdventuresOfDoubloonIsland"
	},
	{
		"DisplayEnName": "Age of Conquest",
		"DisplayName": "征服时代",
		"ButtonImagePath": "BTN_AgeofConquest_ZH",
		"LapisId": "5618",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgeofConquest"
	},
	{
		"DisplayEnName": "Age of Discovery",
		"DisplayName": "史地大发现",
		"ButtonImagePath": "BTN_AgeofDiscovery3",
		"LapisId": "1246",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgeofDiscovery3"
	},
	{
		"DisplayEnName": "Agent Jane Blonde",
		"DisplayName": "特务珍金",
		"ButtonImagePath": "BTN_AgentJaneBlonde7",
		"LapisId": "1155",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgentJaneBlonde7"
	},
	{
		"DisplayEnName": "Agent Jane Blonde Returns",
		"DisplayName": "特工简布隆德归来",
		"ButtonImagePath": "BTN_AgentJaneBlondeReturns_ZH",
		"LapisId": "2088",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgentJaneBlondeReturns"
	},
	{
		"DisplayEnName": "Alaskan Fishing",
		"DisplayName": "阿拉斯加捕捞",
		"ButtonImagePath": "BTN_AlaskanFishing1",
		"LapisId": "1004",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlaskanFishing1"
	},
	{
		"DisplayEnName": "Alchemist Stone",
		"DisplayName": "贤者之石",
		"ButtonImagePath": "BTN_AlchemistStone_ZH",
		"LapisId": "5926",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemistStone"
	},
	{
		"DisplayEnName": "Alchemy Blast",
		"DisplayName": "炼金爆炸",
		"ButtonImagePath": "BTN_AlchemyBlast",
		"LapisId": "5490",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemyBlast"
	},
	{
		"DisplayEnName": "Alchemy Fortunes",
		"DisplayName": "炼金财宝",
		"ButtonImagePath": "BTN_AlchemyFortunes_ZH",
		"LapisId": "6219",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemyFortunes"
	},
	{
		"DisplayEnName": "Amazing Link Zeus",
		"DisplayName": "神奇连环宙斯",
		"ButtonImagePath": "BTN_AmazingLinkZeus_ZH",
		"LapisId": "6332",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AmazingLinkZeus"
	},
	{
		"DisplayEnName": "American Roulette",
		"DisplayName": "美国轮盘",
		"ButtonImagePath": "BTN_USRoulette1",
		"LapisId": "1248",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_USRoulette1"
	},
	{
		"DisplayEnName": "Ancient Fortunes : Poseidon Megaways™",
		"DisplayName": "古代财富Poseidon Megaways",
		"ButtonImagePath": "BTN_AncientFortunesPoseidonMegaways_ZH",
		"LapisId": "6747",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AncientFortunesPoseidonMegaways"
	},
	{
		"DisplayEnName": "Ancient Fortunes: Zeus",
		"DisplayName": "宙斯古代财富",
		"ButtonImagePath": "BTN_AncientFortunesZeus_ZH",
		"LapisId": "4111",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AncientFortunesZeus"
	},
	{
		"DisplayEnName": "Andar Bahar Royale",
		"DisplayName": "安达巴哈",
		"ButtonImagePath": "BTN_AndarBaharRoyale",
		"LapisId": "6372",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AndarBaharRoyale"
	},
	{
		"DisplayEnName": "Ariana",
		"DisplayName": "爱丽娜",
		"ButtonImagePath": "BTN_Ariana_ZH",
		"LapisId": "1021",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Ariana"
	},
	{
		"DisplayEnName": "Asian Beauty",
		"DisplayName": "亚洲美人",
		"ButtonImagePath": "BTN_AsianBeauty1",
		"LapisId": "1384",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AsianBeauty1"
	},
	{
		"DisplayEnName": "Assassin Moon",
		"DisplayName": "刺客之月",
		"ButtonImagePath": "BTN_AssassinMoon_ZH",
		"LapisId": "6254",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AssassinMoon"
	},
	{
		"DisplayEnName": "Astro Legends: Lyra and Erion",
		"DisplayName": "太空传奇莱拉和叶莉昂",
		"ButtonImagePath": "BTN_AstroLegendsLyraandErion",
		"LapisId": "5651",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AstroLegendsLyraandErion"
	},
	{
		"DisplayEnName": "Atlantis Rising",
		"DisplayName": "亚特兰蒂斯崛起",
		"ButtonImagePath": "BTN_AtlantisRising_ZH",
		"LapisId": "6952",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AtlantisRising"
	},
	{
		"DisplayEnName": "Augustus",
		"DisplayName": "奥古斯都",
		"ButtonImagePath": "BTN_Augustus_ZH",
		"LapisId": "6257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Augustus"
	},
	{
		"DisplayEnName": "Aurora Wilds",
		"DisplayName": "极光百搭",
		"ButtonImagePath": "BTN_AuroraWilds_ZH",
		"LapisId": "4328",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AuroraWilds"
	},
	{
		"DisplayEnName": "Avalon HD",
		"DisplayName": "阿瓦隆",
		"ButtonImagePath": "BTN_AvalonHD_ZH",
		"LapisId": "1013",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AvalonHD"
	},
	// {
	// 	"DisplayEnName": "Badminton Hero",
	// 	"DisplayName": "热血羽球",
	// 	"ButtonImagePath": "BTN_BadmintonHero_ZH",
	// 	"LapisId": "2061",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_BadmintonHero"
	// },
	{
		"DisplayEnName": "Banana Odyssey",
		"DisplayName": "香蕉奥德赛",
		"ButtonImagePath": "BTN_BananaOdyssey",
		"LapisId": "4296",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BananaOdyssey"
	},
	{
		"DisplayEnName": "Bar Bar Black Sheep 5 Reel",
		"DisplayName": "黑绵羊咩咩叫5轴",
		"ButtonImagePath": "BTN_BarBarBlackSheep5Reel_ZH",
		"LapisId": "1788",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BarBarBlackSheep5Reel"
	},
	{
		"DisplayEnName": "Bars and Stripes",
		"DisplayName": "美国酒吧",
		"ButtonImagePath": "BTN_barsandstripes",
		"LapisId": "1257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_barsandstripes"
	},
	{
		"DisplayEnName": "Basketball Star",
		"DisplayName": "篮球巨星",
		"ButtonImagePath": "BTN_BasketballStar_ZH",
		"LapisId": "1159",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStar"
	},
	{
		"DisplayEnName": "Basketball Star Deluxe",
		"DisplayName": "篮球明星豪华版",
		"ButtonImagePath": "BTN_BasketballStarDeluxe_ZH",
		"LapisId": "4256",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStarDeluxe"
	},
	{
		"DisplayEnName": "Basketball Star on Fire",
		"DisplayName": "篮球巨星火力全开",
		"ButtonImagePath": "BTN_BasketballStarOnFire_ZH",
		"LapisId": "6184",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStarOnFire"
	},
	{
		"DisplayEnName": "Battle Royale",
		"DisplayName": "大逃杀",
		"ButtonImagePath": "BTN_BattleRoyale_ZH",
		"LapisId": "1995",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BattleRoyale"
	},
	{
		"DisplayEnName": "Beautiful Bones",
		"DisplayName": "美丽骷髅",
		"ButtonImagePath": "BTN_BeautifulBones_ZH",
		"LapisId": "1890",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BeautifulBones"
	},
	{
		"DisplayEnName": "Big Kahuna",
		"DisplayName": "征服钱海",
		"ButtonImagePath": "BTN_BigKahuna1",
		"LapisId": "1399",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BigKahuna1"
	},
	{
		"DisplayEnName": "Big Top",
		"DisplayName": "马戏篷",
		"ButtonImagePath": "BTN_BigTop1",
		"LapisId": "1133",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BigTop1"
	},
	{
		"DisplayEnName": "Bikini Party",
		"DisplayName": "比基尼派对",
		"ButtonImagePath": "BTN_BikiniParty_ZH",
		"LapisId": "1290",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BikiniParty"
	},
	{
		"DisplayEnName": "Blazing Mammoth",
		"DisplayName": "炽热猛犸象",
		"ButtonImagePath": "BTN_BlazingMammoth_ZH",
		"LapisId": "6957",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BlazingMammoth"
	},
	{
		"DisplayEnName": "Boat of Fortune",
		"DisplayName": "金龙财宝",
		"ButtonImagePath": "BTN_BoatofFortune_ZH",
		"LapisId": "5491",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BoatofFortune"
	},
	{
		"DisplayEnName": "Book of King Arthur",
		"DisplayName": "亚瑟王之书",
		"ButtonImagePath": "BTN_BookOFKingArthur_ZH",
		"LapisId": "6626",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookOFKingArthur"
	},
	{
		"DisplayEnName": "Book of Oz",
		"DisplayName": "Oz之书",
		"ButtonImagePath": "BTN_BookOfOz_ZH",
		"LapisId": "2075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookOfOz"
	},
	// {
	// 	"DisplayEnName": "Book of Oz Lock 'N Spin",
	// 	"DisplayName": "Oz之书锁定并旋转",
	// 	"ButtonImagePath": "BTN_BookOfOzLockNSpin_ZH",
	// 	"LapisId": "4329",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_BookOfOzLockNSpin"
	// },
	{
		"DisplayEnName": "Bookie of Odds",
		"DisplayName": "好运经纪人",
		"ButtonImagePath": "BTN_BookieOfOdds_ZH",
		"LapisId": "2089",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookieOfOdds"
	},
	{
		"DisplayEnName": "Boom Pirates",
		"DisplayName": "炸弹海盗",
		"ButtonImagePath": "BTN_BoomPirates_ZH",
		"LapisId": "5695",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BoomPirates"
	},
	{
		"DisplayEnName": "Break Away",
		"DisplayName": "冰球突破",
		"ButtonImagePath": "BTN_BreakAway1",
		"LapisId": "1229",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAway1"
	},
	{
		"DisplayEnName": "Break Away Deluxe",
		"DisplayName": "冰球突破豪华版",
		"ButtonImagePath": "BTN_BreakAwayDeluxe_ZH",
		"LapisId": "2074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayDeluxe"
	},
	{
		"DisplayEnName": "Break Away Lucky Wilds",
		"DisplayName": "冰球突破幸运百搭",
		"ButtonImagePath": "BTN_BreakAwayLuckyWilds_ZH",
		"LapisId": "5325",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayLuckyWilds"
	},
	{
		"DisplayEnName": "Break Away Ultra",
		"DisplayName": "冰球突破终极全胜",
		"ButtonImagePath": "BTN_BreakAwayUltra_ZH",
		"LapisId": "6251",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayUltra"
	},
	{
		"DisplayEnName": "Break Da Bank",
		"DisplayName": "抢银行",
		"ButtonImagePath": "BTN_BreakDaBank1",
		"LapisId": "1023",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBank1"
	},
	{
		"DisplayEnName": "Break Da Bank Again",
		"DisplayName": "银行爆破",
		"ButtonImagePath": "BTN_BreakDaBankAgain1",
		"LapisId": "1097",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBankAgain1"
	},
	{
		"DisplayEnName": "Break Da Bank Again Respin",
		"DisplayName": "银行爆破再旋转",
		"ButtonImagePath": "BTN_BreakDaBankAgainRespin_ZH",
		"LapisId": "4297",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBankAgainRespin"
	},
	{
		"DisplayEnName": "Bridesmaids",
		"DisplayName": "伴娘",
		"ButtonImagePath": "BTN_bridesmaids",
		"LapisId": "1360",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_bridesmaids"
	},
	{
		"DisplayEnName": "Burning Desire",
		"DisplayName": "燃烧的欲望",
		"ButtonImagePath": "BTN_BurningDesire1",
		"LapisId": "1318",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BurningDesire1"
	},
	{
		"DisplayEnName": "Bush Telegraph",
		"DisplayName": "丛林摇摆",
		"ButtonImagePath": "BTN_BushTelegraph1",
		"LapisId": "1173",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BushTelegraph1"
	},
	{
		"DisplayEnName": "Bust the Bank",
		"DisplayName": "抢劫银行",
		"ButtonImagePath": "BTN_BustTheBank1",
		"LapisId": "1204",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BustTheBank1"
	},
	{
		"DisplayEnName": "Candy Dreams",
		"DisplayName": "梦果子乐园",
		"ButtonImagePath": "BTN_CandyDreams_ZH",
		"LapisId": "1886",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CandyDreams"
	},
	{
		"DisplayEnName": "Carnaval",
		"DisplayName": "狂欢节",
		"ButtonImagePath": "BTN_Carnaval2",
		"LapisId": "1117",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Carnaval2"
	},
	{
		"DisplayEnName": "Carnaval Jackpot",
		"DisplayName": "嘉年华大奖",
		"ButtonImagePath": "BTN_CarnavalJackpot_ZH",
		"LapisId": "6303",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CarnavalJackpot"
	},
	{
		"DisplayEnName": "Cash Crazy",
		"DisplayName": "财运疯狂",
		"ButtonImagePath": "BTN_CashCrazy9",
		"LapisId": "1393",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashCrazy9"
	},
	{
		"DisplayEnName": "Cash of Kingdoms",
		"DisplayName": "富贵王国",
		"ButtonImagePath": "BTN_CashofKingdoms_ZH",
		"LapisId": "2067",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashofKingdoms"
	},
	{
		"DisplayEnName": "Cashapillar",
		"DisplayName": "昆虫派对",
		"ButtonImagePath": "BTN_Cashapillar1",
		"LapisId": "1366",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Cashapillar1"
	},
	{
		"DisplayEnName": "CashOccino",
		"DisplayName": "现金咖啡",
		"ButtonImagePath": "BTN_CashOccino_ZH",
		"LapisId": "1996",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashOccino"
	},
	{
		"DisplayEnName": "Cashville",
		"DisplayName": "挥金如土",
		"ButtonImagePath": "BTN_Cashville1",
		"LapisId": "1197",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Cashville1"
	},
	{
		"DisplayEnName": "Centre Court",
		"DisplayName": "中心球场",
		"ButtonImagePath": "BTN_CentreCourt1",
		"LapisId": "1291",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CentreCourt1"
	},
	{
		"DisplayEnName": "Chicago Gold",
		"DisplayName": "芝加哥黄金",
		"ButtonImagePath": "BTN_ChicagoGold_ZH",
		"LapisId": "6302",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ChicagoGold"
	},
	{
		"DisplayEnName": "Classic 243",
		"DisplayName": "经典243",
		"ButtonImagePath": "BTN_Classic243",
		"LapisId": "1879",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Classic243"
	},
	{
		"DisplayEnName": "Cool Buck - 5 Reel",
		"DisplayName": "运财酷儿-5卷轴",
		"ButtonImagePath": "BTN_CoolBuck_5Reel_ZH",
		"LapisId": "1884",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CoolBuck_5Reel"
	},
	{
		"DisplayEnName": "Cool Wolf",
		"DisplayName": "酷狼",
		"ButtonImagePath": "BTN_CoolWolf3",
		"LapisId": "1084",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CoolWolf3"
	},
	{
		"DisplayEnName": "Couch Potato",
		"DisplayName": "沙发土豆",
		"ButtonImagePath": "BTN_CouchPotato2",
		"LapisId": "1327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CouchPotato2"
	},
	{
		"DisplayEnName": "Crazy Chameleons",
		"DisplayName": "疯狂变色龙",
		"ButtonImagePath": "BTN_CrazyChameleons1",
		"LapisId": "1202",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CrazyChameleons1"
	},
	{
		"DisplayEnName": "Cricket Star",
		"DisplayName": "板球明星",
		"ButtonImagePath": "BTN_CricketStar",
		"LapisId": "1075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CricketStar"
	},
	{
		"DisplayEnName": "Crystal Rift™",
		"DisplayName": "水晶裂谷",
		"ButtonImagePath": "BTN_CrystalRift",
		"LapisId": "2069",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CrystalRift"
	},
	{
		"DisplayEnName": "Cuoi and the Banyan Tree",
		"DisplayName": "阿贵与树",
		"ButtonImagePath": "BTN_CuoiAndTheBanyanTree_ZH",
		"LapisId": "5532",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CuoiAndTheBanyanTree"
	},
	{
		"DisplayEnName": "Deadmau5",
		"DisplayName": "鼠来宝",
		"ButtonImagePath": "BTN_Deadmau5",
		"LapisId": "6186",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Deadmau5"
	},
	{
		"DisplayEnName": "Deck The Halls",
		"DisplayName": "闪亮的圣诞节",
		"ButtonImagePath": "BTN_DeckTheHalls1",
		"LapisId": "1234",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DeckTheHalls1"
	},
	{
		"DisplayEnName": "Deco Diamonds",
		"DisplayName": "德科钻石",
		"ButtonImagePath": "BTN_DecoDiamonds_ZH",
		"LapisId": "2047",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DecoDiamonds"
	},
	{
		"DisplayEnName": "Deuces Wild",
		"DisplayName": "万能两点",
		"ButtonImagePath": "BTN_DeucesWildPowerPoker1",
		"LapisId": "1415",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DeucesWildPowerPoker1"
	},
	{
		"DisplayEnName": "Diamond Empire",
		"DisplayName": "钻石帝国",
		"ButtonImagePath": "BTN_DiamondEmpire_ZH",
		"LapisId": "1949",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DiamondEmpire"
	},
	{
		"DisplayEnName": "Diamond King Jackpots",
		"DisplayName": "钻石之王",
		"ButtonImagePath": "BTN_DiamondKingJackpots_ZH",
		"LapisId": "6220",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DiamondKingJackpots"
	},
	{
		"DisplayEnName": "Divine Diamonds",
		"DisplayName": "神圣钻石",
		"ButtonImagePath": "BTN_DivineDiamonds_ZH",
		"LapisId": "6953",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DivineDiamonds"
	},
	{
		"DisplayEnName": "Dolphin Coast",
		"DisplayName": "海豚海岸",
		"ButtonImagePath": "BTN_DolphinCoast_ZH",
		"LapisId": "2065",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DolphinCoast"
	},
	{
		"DisplayEnName": "Dolphin Quest",
		"DisplayName": "寻访海豚",
		"ButtonImagePath": "BTN_DolphinQuest",
		"LapisId": "1309",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DolphinQuest"
	},
	{
		"DisplayEnName": "Double Wammy",
		"DisplayName": "双重韦密",
		"ButtonImagePath": "BTN_DoubleWammy1",
		"LapisId": "1102",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DoubleWammy1"
	},
	{
		"DisplayEnName": "Dragon Dance",
		"DisplayName": "舞龙",
		"ButtonImagePath": "BTN_DragonDance_ZH",
		"LapisId": "1037",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DragonDance"
	},
	{
		"DisplayEnName": "Dragon Shard",
		"DisplayName": "神龙碎片",
		"ButtonImagePath": "BTN_DragonShard_ZH",
		"LapisId": "4257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DragonShard"
	},
	{
		"DisplayEnName": "Dragonz",
		"DisplayName": "幸运龙宝贝",
		"ButtonImagePath": "BTN_Dragonz_sc",
		"LapisId": "1424",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Dragonz_en"
	},
	{
		"DisplayEnName": "Dream Date",
		"DisplayName": "梦幻邂逅",
		"ButtonImagePath": "BTN_DreamDate_ZH",
		"LapisId": "1948",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DreamDate"
	},
	{
		"DisplayEnName": "Eagles Wings",
		"DisplayName": "雄鹰之翼",
		"ButtonImagePath": "BTN_EaglesWings1",
		"LapisId": "1236",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EaglesWings1"
	},
	{
		"DisplayEnName": "Egyptian Tombs",
		"DisplayName": "埃及古墓",
		"ButtonImagePath": "BTN_EgyptianTombs_ZH",
		"LapisId": "6417",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EgyptianTombs"
	},
	{
		"DisplayEnName": "Emerald Gold",
		"DisplayName": "金翠绿",
		"ButtonImagePath": "BTN_EmeraldGold_ZH",
		"LapisId": "6627",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmeraldGold"
	},
	{
		"DisplayEnName": "EmotiCoins",
		"DisplayName": "表情金币",
		"ButtonImagePath": "BTN_EmotiCoins_ZH",
		"LapisId": "1895",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Emoticoins"
	},
	{
		"DisplayEnName": "Emperor of The Sea",
		"DisplayName": "青龙出海",
		"ButtonImagePath": "BTN_EmperorOfTheSea_ZH",
		"LapisId": "1882",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmperorOfTheSea"
	},
	{
		"DisplayEnName": "Emperor of the Sea Deluxe",
		"DisplayName": "青龙出海豪华版",
		"ButtonImagePath": "BTN_EmperoroftheSeaDeluxe_ZH",
		"LapisId": "6253",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmperoroftheSeaDeluxe"
	},
	{
		"DisplayEnName": "Exotic Cats",
		"DisplayName": "异域狂兽",
		"ButtonImagePath": "BTN_ExoticCats_ZH",
		"LapisId": "2060",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ExoticCats "
	},
	{
		"DisplayEnName": "Fire Forge",
		"DisplayName": "烈火锻金",
		"ButtonImagePath": "BTN_FireForge_ZH",
		"LapisId": "6330",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FireForge"
	},
	{
		"DisplayEnName": "Fish Party",
		"DisplayName": "派对鱼",
		"ButtonImagePath": "BTN_FishParty",
		"LapisId": "1113",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FishParty"
	},
	{
		"DisplayEnName": "Flower Fortunes Megaways™",
		"DisplayName": "花的幸运Megaways",
		"ButtonImagePath": "BTN_FlowerFortunesMegaways_ZH",
		"LapisId": "6305",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FlowerFortunesMegaways"
	},
	{
		"DisplayEnName": "Football Star",
		"DisplayName": "足球明星",
		"ButtonImagePath": "BTN_footballstar1",
		"LapisId": "1186",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_footballstar1"
	},
	{
		"DisplayEnName": "Football Star Deluxe",
		"DisplayName": "足球明星豪华版",
		"ButtonImagePath": "BTN_FootballStarDeluxe_ZH",
		"LapisId": "5488",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FootballStarDeluxe"
	},
	{
		"DisplayEnName": "Forbidden Throne",
		"DisplayName": "禁忌王座",
		"ButtonImagePath": "BTN_ForbiddenThrone",
		"LapisId": "1887",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ForbiddenThrone"
	},
	{
		"DisplayEnName": "Forgotten Island Megaways™",
		"DisplayName": "遗忘之岛Megaways",
		"ButtonImagePath": "BTN_ForgottenIslandMegaways_ZH",
		"LapisId": "6298",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ForgottenIslandMegaways"
	},
	{
		"DisplayEnName": "Fortune Girl",
		"DisplayName": "金库甜心",
		"ButtonImagePath": "BTN_FortuneGirl_ZH",
		"LapisId": "1888",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FortuneGirl"
	},
	{
		"DisplayEnName": "Fortunium",
		"DisplayName": "财富之都",
		"ButtonImagePath": "BTN_Fortunium_ZH",
		"LapisId": "1993",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Fortunium"
	},
	{
		"DisplayEnName": "Fruit Blast",
		"DisplayName": "水果大爆发",
		"ButtonImagePath": "BTN_FruitBlast_ZH",
		"LapisId": "1943",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FruitBlast"
	},
	{
		"DisplayEnName": "Fruit Vs Candy",
		"DisplayName": "水果VS糖果",
		"ButtonImagePath": "BTN_FruitVsCandy_ZH",
		"LapisId": "1878",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FruitVsCandy"
	},
	{
		"DisplayEnName": "Galaxy Glider",
		"DisplayName": "星际骑手",
		"ButtonImagePath": "BTN_GalaxyGlider_ZH",
		"LapisId": "5696",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GalaxyGlider"
	},
	{
		"DisplayEnName": "Gems & Dragons",
		"DisplayName": "魔龙宝珠",
		"ButtonImagePath": "BTN_Gems&Dragons_ZH",
		"LapisId": "6256",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Gems&Dragons"
	},
	{
		"DisplayEnName": "Gems Odyssey",
		"DisplayName": "宝石奥德赛",
		"ButtonImagePath": "BTN_GemsOdyssey_ZH",
		"LapisId": "1945",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GemsOdyssey"
	},
	{
		"DisplayEnName": "Giant Riches",
		"DisplayName": "巨人财富",
		"ButtonImagePath": "BTN_GiantRiches",
		"LapisId": "1955",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GiantRiches"
	},
	{
		"DisplayEnName": "Girls With Guns-L-Jungle Heat",
		"DisplayName": "美女枪手丛林激战",
		"ButtonImagePath": "BTN_GirlswithGuns1",
		"LapisId": "1313",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GirlswithGuns1"
	},
	{
		"DisplayEnName": "Gnome Wood",
		"DisplayName": "矮木头",
		"ButtonImagePath": "BTN_GnomeWood_ZH",
		"LapisId": "1900",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GnomeWood"
	},
	{
		"DisplayEnName": "Gold Collector",
		"DisplayName": "淘金者",
		"ButtonImagePath": "BTN_GoldCollector_ZH",
		"LapisId": "6333",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldCollector"
	},
	{
		"DisplayEnName": "Gold Factory",
		"DisplayName": "黄金工厂",
		"ButtonImagePath": "BTN_GoldFactory_ZH",
		"LapisId": "1267",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldFactory"
	},
	{
		"DisplayEnName": "Goldaur Guardians",
		"DisplayName": "黄金守卫",
		"ButtonImagePath": "BTN_GoldaurGuardians_ZH",
		"LapisId": "6214",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldaurGuardians"
	},
	{
		"DisplayEnName": "Golden Era",
		"DisplayName": "黄金时代",
		"ButtonImagePath": "BTN_GoldenEra",
		"LapisId": "1041",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenEra"
	},
	{
		"DisplayEnName": "Golden Princess",
		"DisplayName": "黄金公主",
		"ButtonImagePath": "BTN_GoldenPrincess",
		"LapisId": "1190",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenPrincess"
	},
	{
		"DisplayEnName": "Golden Stallion",
		"DisplayName": "金色骏马",
		"ButtonImagePath": "BTN_GoldenStallion_ZH",
		"LapisId": "6301",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenStallion"
	},
	{
		"DisplayEnName": "Gopher Gold",
		"DisplayName": "黄金囊地鼠",
		"ButtonImagePath": "BTN_GopherGold2",
		"LapisId": "1216",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GopherGold2"
	},
	{
		"DisplayEnName": "Halloween",
		"DisplayName": "万圣劫",
		"ButtonImagePath": "BTN_Halloween",
		"LapisId": "1904",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Halloween"
	},
	{
		"DisplayEnName": "Halloweenies",
		"DisplayName": "万圣节",
		"ButtonImagePath": "BTN_Halloweenies1",
		"LapisId": "1047",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Halloweenies1"
	},
	{
		"DisplayEnName": "Happy Holidays",
		"DisplayName": "快乐假日",
		"ButtonImagePath": "BTN_HappyHolidays_ZH",
		"LapisId": "1072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HappyHolidays"
	},
	{
		"DisplayEnName": "Happy Monster Claw",
		"DisplayName": "开心娃娃机",
		"ButtonImagePath": "BTN_HappyMonsterClaw_ZH",
		"LapisId": "2101",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HappyMonsterClaw"
	},
	{
		"DisplayEnName": "Harveys",
		"DisplayName": "哈维斯的晚餐",
		"ButtonImagePath": "BTN_Harveys1",
		"LapisId": "1139",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Harveys1"
	},
	{
		"DisplayEnName": "Hidden Palace Treasures",
		"DisplayName": "大藏金",
		"ButtonImagePath": "BTN_HiddenPalaceTreasures_ZH",
		"LapisId": "5479",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HiddenPalaceTreasures"
	},
	{
		"DisplayEnName": "Hidden Treasures of River Kwai",
		"DisplayName": "桂河藏宝",
		"ButtonImagePath": "BTN_HiddenTreasuresOfRiverKwai_ZH",
		"LapisId": "5531",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HiddenTreasuresOfRiverKwai"
	},
	{
		"DisplayEnName": "High Society",
		"DisplayName": "上流社会",
		"ButtonImagePath": "BTN_HighSociety",
		"LapisId": "1163",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HighSociety"
	},
	{
		"DisplayEnName": "Highlander",
		"DisplayName": "时空英豪",
		"ButtonImagePath": "BTN_Highlander_ZH",
		"LapisId": "1909",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Highlander"
	},
	{
		"DisplayEnName": "Hip Hop Thai",
		"DisplayName": "泰嘻哈",
		"ButtonImagePath": "BTN_HipHopThai_ZH",
		"LapisId": "6074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HipHopThai"
	},
	{
		"DisplayEnName": "Hippie Days",
		"DisplayName": "嬉皮时光",
		"ButtonImagePath": "BTN_HippieDays_ZH",
		"LapisId": "6071",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HippieDays"
	},
	{
		"DisplayEnName": "HitMan",
		"DisplayName": "终极杀手",
		"ButtonImagePath": "BTN_Hitman1",
		"LapisId": "1321",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Hitman1"
	},
	{
		"DisplayEnName": "Holly Jolly Penguins",
		"DisplayName": "圣诞企鹅",
		"ButtonImagePath": "BTN_HollyJollyPenguins_ZH",
		"LapisId": "1910",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HollyJollyPenguins"
	},
	{
		"DisplayEnName": "Hound Hotel",
		"DisplayName": "酷犬酒店",
		"ButtonImagePath": "BTN_HoundHotel_ZH",
		"LapisId": "1063",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HoundHotel"
	},
	{
		"DisplayEnName": "Huangdi - The Yellow Emperor",
		"DisplayName": "轩辕帝传",
		"ButtonImagePath": "BTN_Huangdi_TheYellowEmperor_zh",
		"LapisId": "1849",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Huangdi_TheYellowEmperor_en"
	},
	{
		"DisplayEnName": "Hyper Strike™",
		"DisplayName": "究极闪电",
		"ButtonImagePath": "BTN_HyperStrike™_ZH",
		"LapisId": "6327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HyperStrike™"
	},
	{
		"DisplayEnName": "Immortal Romance",
		"DisplayName": "不朽情缘",
		"ButtonImagePath": "BTN_ImmortalRomance1",
		"LapisId": "1103",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ImmortalRomance1"
	},
	{
		"DisplayEnName": "Incan Adventure",
		"DisplayName": "挖到宝",
		"ButtonImagePath": "BTN_IncanAdventure_ZH",
		"LapisId": "5489",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_IncanAdventure"
	},
	{
		"DisplayEnName": "Ingots of Cai Shen",
		"DisplayName": "财神元宝",
		"ButtonImagePath": "BTN_Ingotsof CaiShen_ZH",
		"LapisId": "6258",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Ingotsof CaiShen"
	},
	{
		"DisplayEnName": "Isis",
		"DisplayName": "埃及女神伊西絲",
		"ButtonImagePath": "BTN_isis",
		"LapisId": "1250",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_isis"
	},
	{
		"DisplayEnName": "Joker Poker",
		"DisplayName": "小丑扑克",
		"ButtonImagePath": "BTN_JokerPoker1",
		"LapisId": "1418",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JokerPoker1"
	},
	{
		"DisplayEnName": "Joyful Joker Megaways",
		"DisplayName": "欢乐小丑 Megaways",
		"ButtonImagePath": "BTN_JoyfulJokerMegaways_ZH",
		"LapisId": "6955",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JoyfulJokerMegaways"
	},
	{
		"DisplayEnName": "Jungle Jim and the Lost Sphinx",
		"DisplayName": "丛林吉姆与失落的狮身人面像",
		"ButtonImagePath": "BTN_JungleJimandtheLostSphinx_ZH",
		"LapisId": "4340",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JungleJimandtheLostSphinx"
	},
	{
		"DisplayEnName": "Jungle Jim EL Dorado",
		"DisplayName": "丛林吉姆-黄金国",
		"ButtonImagePath": "BTN_Junglejim_zh",
		"LapisId": "1244",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JungleJim"
	},
	{
		"DisplayEnName": "Jurassic World",
		"DisplayName": "侏罗纪世界",
		"ButtonImagePath": "BTN_JurassicWorld",
		"LapisId": "1891",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JurassicWorld"
	},
	{
		"DisplayEnName": "Karaoke Party",
		"DisplayName": "K歌乐韵",
		"ButtonImagePath": "BTN_KaraokeParty_ZH",
		"LapisId": "1053",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KaraokeParty"
	},
	{
		"DisplayEnName": "Kathmandu",
		"DisplayName": "卡萨缦都",
		"ButtonImagePath": "BTN_Kathmandu1",
		"LapisId": "1151",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Kathmandu1"
	},
	{
		"DisplayEnName": "King of the Ring",
		"DisplayName": "擂台王者",
		"ButtonImagePath": "BTN_KingoftheRing_ZH",
		"LapisId": "5477",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingoftheRing"
	},
	{
		"DisplayEnName": "King Tusk",
		"DisplayName": "大象之王",
		"ButtonImagePath": "BTN_KingTusk_ZH",
		"LapisId": "1908",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingTusk"
	},
	{
		"DisplayEnName": "Kings of Cash",
		"DisplayName": "现金之王",
		"ButtonImagePath": "BTN_KingsofCash1",
		"LapisId": "1400",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingsofCash1"
	},
	{
		"DisplayEnName": "Kitty Cabana",
		"DisplayName": "凯蒂卡巴拉",
		"ButtonImagePath": "BTN_KittyCabana_ZH",
		"LapisId": "1286",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KittyCabana"
	},
	{
		"DisplayEnName": "Ladies Nite",
		"DisplayName": "女仕之夜",
		"ButtonImagePath": "BTN_LadiesNite5",
		"LapisId": "1389",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadiesNite5"
	},
	{
		"DisplayEnName": "Ladies Nite 2 Turn Wild",
		"DisplayName": "淑女之夜2狂野变身",
		"ButtonImagePath": "BTN_LadiesNite2TurnWild_ZH",
		"LapisId": "5487",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadiesNite2TurnWild"
	},
	{
		"DisplayEnName": "Lady in Red",
		"DisplayName": "红衣女郎",
		"ButtonImagePath": "BTN_LadyInRed2",
		"LapisId": "1124",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadyInRed2"
	},
	{
		"DisplayEnName": "Lara Croft® Temples and Tombs™",
		"DisplayName": "萝拉神庙古墓",
		"ButtonImagePath": "BTN_LaraCroftTemplesandTombs_ZH",
		"LapisId": "4110",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LaraCroftTemplesandTombs"
	},
	{
		"DisplayEnName": "Legend of the Moon Lovers",
		"DisplayName": "奔月传说",
		"ButtonImagePath": "BTN_LegendoftheMoonLovers_ZH",
		"LapisId": "4295",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LegendoftheMoonLovers"
	},
	{
		"DisplayEnName": "Life of Riches",
		"DisplayName": "富裕人生",
		"ButtonImagePath": "BTN_LifeOfRiches_zh",
		"LapisId": "1851",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LifeOfRiches_en"
	},
	{
		"DisplayEnName": "Lion's Pride",
		"DisplayName": "狮子的骄傲",
		"ButtonImagePath": "BTN_lionsPride",
		"LapisId": "1049",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_lionsPride"
	},
	{
		"DisplayEnName": "Liquid Gold",
		"DisplayName": "液体黄金",
		"ButtonImagePath": "BTN_LiquidGold",
		"LapisId": "1756",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LiquidGold"
	},
	{
		"DisplayEnName": "Loaded",
		"DisplayName": "炫富一族",
		"ButtonImagePath": "BTN_Loaded1",
		"LapisId": "1245",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Loaded1"
	},
	{
		"DisplayEnName": "Long Mu Fortunes",
		"DisplayName": "龙母财富",
		"ButtonImagePath": "BTN_LongMuFortunes_ZH",
		"LapisId": "5365",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LongMuFortunes"
	},
	{
		"DisplayEnName": "Lost Vegas",
		"DisplayName": "迷失拉斯维加斯",
		"ButtonImagePath": "BTN_LostVegas_zh",
		"LapisId": "1420",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LostVegas"
	},
	{
		"DisplayEnName": "Lucha Legends",
		"DisplayName": "摔角传奇",
		"ButtonImagePath": "BTN_LuchaLegends_ZH",
		"LapisId": "2066",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuchaLegends"
	},
	{
		"DisplayEnName": "Lucky Bachelors",
		"DisplayName": "幸运单身族",
		"ButtonImagePath": "BTN_LuckyBachelors_ZH",
		"LapisId": "4339",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyBachelors"
	},
	{
		"DisplayEnName": "Lucky Firecracker",
		"DisplayName": "招财鞭炮",
		"ButtonImagePath": "BTN_LuckyFirecracker_ZH",
		"LapisId": "1126",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyFirecracker"
	},
	{
		"DisplayEnName": "Lucky Koi",
		"DisplayName": "幸运锦鲤",
		"ButtonImagePath": "BTN_LuckyKoi",
		"LapisId": "1060",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyKoi"
	},
	{
		"DisplayEnName": "Lucky Leprechaun",
		"DisplayName": "幸运的小妖精",
		"ButtonImagePath": "BTN_LuckyLeprechaun",
		"LapisId": "1212",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyLeprechaun"
	},
	{
		"DisplayEnName": "Lucky Little Gods",
		"DisplayName": "宝贝财神",
		"ButtonImagePath": "BTN_LuckyLittleGods_ZH",
		"LapisId": "1944",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyLittleGods"
	},
	{
		"DisplayEnName": "Lucky Riches Hyperspins",
		"DisplayName": "幸运富豪",
		"ButtonImagePath": "BTN_LuckyRichesHyperspins_ZH",
		"LapisId": "5620",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyRichesHyperspins"
	},
	{
		"DisplayEnName": "Lucky Silat",
		"DisplayName": "黄金对决",
		"ButtonImagePath": "BTN_LuckySilat_ZH",
		"LapisId": "5483",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckySilat"
	},
	{
		"DisplayEnName": "Lucky Thai Lanterns",
		"DisplayName": "泰幸运水灯",
		"ButtonImagePath": "BTN_LuckyThaiLanterns_ZH",
		"LapisId": "5698",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyThaiLanterns"
	},
	{
		"DisplayEnName": "Lucky Twins",
		"DisplayName": "幸运双星",
		"ButtonImagePath": "BTN_luckyTwins_ZH",
		"LapisId": "1283",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwins"
	},
	{
		"DisplayEnName": "Lucky Twins Catcher",
		"DisplayName": "幸运双星接财神",
		"ButtonImagePath": "BTN_LuckyTwinsCatcher_ZH",
		"LapisId": "6070",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwinsCatcher"
	},
	{
		"DisplayEnName": "Lucky Twins Jackpot",
		"DisplayName": "幸运双星大奖",
		"ButtonImagePath": "BTN_LuckyTwinsJackpot_ZH",
		"LapisId": "5142",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwinsJackpot"
	},
	{
		"DisplayEnName": "Lucky Zodiac",
		"DisplayName": "幸运生肖",
		"ButtonImagePath": "BTN_LuckyZodiac_ZH",
		"LapisId": "1273",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyZodiac"
	},
	{
		"DisplayEnName": "Mad Hatters",
		"DisplayName": "疯狂的帽子",
		"ButtonImagePath": "BTN_MadHatters1",
		"LapisId": "1314",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MadHatters1"
	},
	{
		"DisplayEnName": "Magic of Sahara",
		"DisplayName": "魔力撒哈拉",
		"ButtonImagePath": "BTN_MagicOfSahara_ZH",
		"LapisId": "4288",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MagicOfSahara"
	},
	{
		"DisplayEnName": "Makan Makan",
		"DisplayName": "食在印尼",
		"ButtonImagePath": "BTN_MakanMakan_ZH",
		"LapisId": "6188",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MakanMakan"
	},
	{
		"DisplayEnName": "Maui Mischief™",
		"DisplayName": "酷太酒店",
		"ButtonImagePath": "BTN_MauiMischief™_ZH",
		"LapisId": "6329",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MauiMischief™"
	},
	{
		"DisplayEnName": "Mayan Princess",
		"DisplayName": "玛雅公主",
		"ButtonImagePath": "BTN_MayanPrincess1",
		"LapisId": "1343",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MayanPrincess1"
	},
	{
		"DisplayEnName": "Mega Money Multiplier",
		"DisplayName": "巨额现金乘数",
		"ButtonImagePath": "BTN_MegaMoneyMultiplier",
		"LapisId": "1885",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MegaMoneyMultiplier"
	},
	{
		"DisplayEnName": "Mega Money Rush",
		"DisplayName": "巨款大冲击",
		"ButtonImagePath": "BTN_MegaMoneyRush_ZH",
		"LapisId": "1942",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MegaMoneyRush"
	},
	{
		"DisplayEnName": "Mermaids Millions",
		"DisplayName": "海底世界",
		"ButtonImagePath": "BTN_MermaidsMillions1",
		"LapisId": "1308",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MermaidsMillions1"
	},
	{
		"DisplayEnName": "Moby Dick",
		"DisplayName": "白鲸记",
		"ButtonImagePath": "BTN_MobyDick",
		"LapisId": "1905",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MobyDick"
	},
	{
		"DisplayEnName": "Monster Blast",
		"DisplayName": "怪物爆炸",
		"ButtonImagePath": "BTN_MonsterBlast_ZH",
		"LapisId": "5697",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MonsterBlast"
	},
	// {
	// 	"DisplayEnName": "Monster Wheels",
	// 	"DisplayName": "怪物赛车",
	// 	"ButtonImagePath": "BTN_MonsterWheels_ZH",
	// 	"LapisId": "1903",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_MonsterWheels"
	// },
	{
		"DisplayEnName": "Munchkins",
		"DisplayName": "怪兽曼琪肯",
		"ButtonImagePath": "BTN_Munchkins2",
		"LapisId": "1008",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Munchkins2"
	},
	{
		"DisplayEnName": "Mystic Dreams",
		"DisplayName": "神秘梦境",
		"ButtonImagePath": "BTN_MysticDreams1",
		"LapisId": "1254",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MysticDreams1"
	},
	{
		"DisplayEnName": "Neptune's Riches: Ocean of Wilds",
		"DisplayName": "海王星宝藏狂野之海",
		"ButtonImagePath": "BTN_NeptunesRiches_ZH",
		"LapisId": "6217",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_NeptunesRiches"
	},
	{
		"DisplayEnName": "Noble Sky",
		"DisplayName": "宏伟苍穹",
		"ButtonImagePath": "BTN_NobleSky_ZH",
		"LapisId": "6073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_NobleSky"
	},
	{
		"DisplayEnName": "Odin's Riches",
		"DisplayName": "奥丁的财宝",
		"ButtonImagePath": "BTN_OdinsRiches_ZH",
		"LapisId": "6956",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OdinsRiches"
	},
	{
		"DisplayEnName": "Oink Country Love",
		"DisplayName": "呼撸撸爱上乡下",
		"ButtonImagePath": "BTN_OinkCountryLove_ZH",
		"LapisId": "1896",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OinkCountryLove"
	},
	{
		"DisplayEnName": "Oni Hunter",
		"DisplayName": "鬼狩",
		"ButtonImagePath": "BTN_OniHunter_ZH",
		"LapisId": "6328",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OniHunter"
	},
	{
		"DisplayEnName": "Oni Hunter Plus",
		"DisplayName": "鬼狩加強版",
		"ButtonImagePath": "BTN_OniHunterPlus_ZH",
		"LapisId": "6959",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OniHunterPlus"
	},
	{
		"DisplayEnName": "Our Days",
		"DisplayName": "有你的校园",
		"ButtonImagePath": "BTN_OurDays_ZH",
		"LapisId": "2073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OurDays"
	},
	{
		"DisplayEnName": "Party Island",
		"DisplayName": "派对岛",
		"ButtonImagePath": "BTN_PartyIsland.png",
		"LapisId": "1737",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PartyIsland.png"
	},
	{
		"DisplayEnName": "Pets Go Wild",
		"DisplayName": "狂野宠物",
		"ButtonImagePath": "BTN_PetsGoWild_ZH",
		"LapisId": "4298",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PetsGoWild"
	},
	{
		"DisplayEnName": "Pho Win",
		"DisplayName": "越南必赢",
		"ButtonImagePath": "BTN_PhoWin_ZH",
		"LapisId": "5478",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PhoWin"
	},
	{
		"DisplayEnName": "Ping Pong Star",
		"DisplayName": "乒乓巨星",
		"ButtonImagePath": "BTN_PingPongStar_ZH",
		"LapisId": "5144",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PingPongStar"
	},
	{
		"DisplayEnName": "Pistoleras",
		"DisplayName": "持枪王者",
		"ButtonImagePath": "BTN_Pistoleras_ZH",
		"LapisId": "1160",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Pistoleras"
	},
	{
		"DisplayEnName": "Playboy",
		"DisplayName": "花花公子",
		"ButtonImagePath": "BTN_Playboy",
		"LapisId": "1188",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Playboy"
	},
	{
		"DisplayEnName": "Playboy Fortunes",
		"DisplayName": "花花公子财富",
		"ButtonImagePath": "BTN_PlayboyFortunes_ZH",
		"LapisId": "5528",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyFortunes"
	},
	{
		"DisplayEnName": "Playboy Gold",
		"DisplayName": "黄金花花公子",
		"ButtonImagePath": "BTN_PlayboyGold_ZH",
		"LapisId": "1946",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyGold"
	},
	{
		"DisplayEnName": "Playboy™ Gold Jackpots",
		"DisplayName": "花花公子黄金大奖",
		"ButtonImagePath": "BTN_PlayboyGoldJackpots_ZH",
		"LapisId": "4327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyGoldJackpots"
	},
	{
		"DisplayEnName": "Poke The Guy",
		"DisplayName": "进击的猿人",
		"ButtonImagePath": "BTN_PokeTheGuy_ZH",
		"LapisId": "1954",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PokeTheGuy"
	},
	{
		"DisplayEnName": "Pollen Party",
		"DisplayName": "花粉之国",
		"ButtonImagePath": "BTN_PollenParty_ZH",
		"LapisId": "1881",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PollenParty"
	},
	{
		"DisplayEnName": "Pretty Kitty",
		"DisplayName": "漂亮猫咪",
		"ButtonImagePath": "BTN_prettykitty_zh",
		"LapisId": "1045",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_prettykitty_en"
	},
	{
		"DisplayEnName": "Pure Platinum",
		"DisplayName": "纯铂",
		"ButtonImagePath": "BTN_PurePlatinum1",
		"LapisId": "1312",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PurePlatinum1"
	},
	{
		"DisplayEnName": "Queen of Alexandria™",
		"DisplayName": "亚历山大女王",
		"ButtonImagePath": "BTN_QueenofAlexandria_ZH",
		"LapisId": "6748",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_QueenofAlexandria"
	},
	{
		"DisplayEnName": "Queen of the Crystal Rays",
		"DisplayName": "水晶女皇",
		"ButtonImagePath": "BTN_QueenOfTheCrystalRays",
		"LapisId": "4289",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_QueenOfTheCrystalRays"
	},
	{
		"DisplayEnName": "Rabbit in the Hat",
		"DisplayName": "帽子里的兔子",
		"ButtonImagePath": "BTN_RabbitintheHat",
		"LapisId": "1275",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RabbitintheHat"
	},
	{
		"DisplayEnName": "Reel Gems",
		"DisplayName": "宝石转轴",
		"ButtonImagePath": "BTN_ReelGems1",
		"LapisId": "1207",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelGems1"
	},
	{
		"DisplayEnName": "Reel Gems Deluxe",
		"DisplayName": "宝石转轴豪华版",
		"ButtonImagePath": "BTN_ReelGemsDeluxe_ZH",
		"LapisId": "6069",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelGemsDeluxe"
	},
	{
		"DisplayEnName": "Reel Spinner",
		"DisplayName": "旋转大战",
		"ButtonImagePath": "BTN_ReelSpinner_zh",
		"LapisId": "1294",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelSpinner_en"
	},
	{
		"DisplayEnName": "Reel Strike",
		"DisplayName": "卷行使价",
		"ButtonImagePath": "BTN_ReelStrike1",
		"LapisId": "1157",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelStrike1"
	},
	// {
	// 	"DisplayEnName": "Reel Talent",
	// 	"DisplayName": "真正高手",
	// 	"ButtonImagePath": "BTN_ReelTalent_ZH",
	// 	"LapisId": "2070",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_ReelTalent"
	// },
	{
		"DisplayEnName": "Reel Thunder",
		"DisplayName": "雷电击",
		"ButtonImagePath": "BTN_ReelThunder2",
		"LapisId": "1293",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelThunder2"
	},
	{
		"DisplayEnName": "Relic Seekers",
		"DisplayName": "探陵人",
		"ButtonImagePath": "BTN_RelicSeekers_ZH",
		"LapisId": "4109",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RelicSeekers"
	},
	{
		"DisplayEnName": "Retro Reels",
		"DisplayName": "复古旋转",
		"ButtonImagePath": "BTN_RetroReels1",
		"LapisId": "1110",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RetroReels1"
	},
	{
		"DisplayEnName": "Retro Reels - Extreme Heat",
		"DisplayName": "复古卷轴-极热",
		"ButtonImagePath": "BTN_RRExtreme1",
		"LapisId": "1189",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRExtreme1"
	},
	{
		"DisplayEnName": "Retro Reels Diamond Glitz",
		"DisplayName": "复古卷轴钻石耀眼",
		"ButtonImagePath": "BTN_RRDiamondGlitz1",
		"LapisId": "1100",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRDiamondGlitz1"
	},
	{
		"DisplayEnName": "Rhyming Reels - Georgie Porgie",
		"DisplayName": "押韵的卷轴-乔治波尔吉",
		"ButtonImagePath": "BTN_RhymingReelsGeorgiePorgie",
		"LapisId": "1782",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RhymingReelsGeorgiePorgie"
	},
	{
		"DisplayEnName": "Rhyming Reels - Hearts & Tarts",
		"DisplayName": "童话转轴红心皇后",
		"ButtonImagePath": "BTN_RRHearts&Tarts1",
		"LapisId": "1009",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRHearts&Tarts1"
	},
	{
		"DisplayEnName": "Riviera Riches",
		"DisplayName": "海滨财富",
		"ButtonImagePath": "BTN_RivieraRiches1",
		"LapisId": "1359",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RivieraRiches1"
	},
	// {
	// 	"DisplayEnName": "Robin of Sherwood",
	// 	"DisplayName": "雪伍德的罗宾汉",
	// 	"ButtonImagePath": "BTN_RobinofSherwood",
	// 	"LapisId": "1994",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_RobinofSherwood"
	// },
	{
		"DisplayEnName": "Romanov Riches",
		"DisplayName": "罗曼诺夫财富",
		"ButtonImagePath": "BTN_RomanovRiches_ZH",
		"LapisId": "2068",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RomanovRiches"
	},
	{
		"DisplayEnName": "Rugby Star",
		"DisplayName": "橄榄球明星",
		"ButtonImagePath": "BTN_RugbyStar_ZH",
		"LapisId": "1287",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RugbyStar"
	},
	{
		"DisplayEnName": "Rugby Star Deluxe",
		"DisplayName": "橄榄球明星豪华版",
		"ButtonImagePath": "BTN_RugbyStarDeluxe_ZH",
		"LapisId": "5447",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RugbyStarDeluxe"
	},
	{
		"DisplayEnName": "Santa Paws",
		"DisplayName": "圣诞之掌",
		"ButtonImagePath": "BTN_SantaPaws",
		"LapisId": "1785",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SantaPaws"
	},
	{
		"DisplayEnName": "Santas Wild Ride",
		"DisplayName": "圣诞老人的百搭摩拖车",
		"ButtonImagePath": "BTN_SantasWildRide1_ZH",
		"LapisId": "1095",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SantasWildRide1"
	},
	{
		"DisplayEnName": "Scrooge",
		"DisplayName": "小气财神",
		"ButtonImagePath": "BTN_Scrooge1_ZH",
		"LapisId": "1030",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Scrooge1"
	},
	{
		"DisplayEnName": "Secret Admirer",
		"DisplayName": "秘密崇拜者",
		"ButtonImagePath": "BTN_SecretAdmirer1",
		"LapisId": "1325",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SecretAdmirer1"
	},
	{
		"DisplayEnName": "Secret Romance",
		"DisplayName": "秘密爱慕者",
		"ButtonImagePath": "BTN_SecretRomance_1_ZH",
		"LapisId": "1877",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SecretRomance_en"
	},
	// {
	// 	"DisplayEnName": "Secrets of Sengoku",
	// 	"DisplayName": "战国奥义",
	// 	"ButtonImagePath": "BTN_SecretsofSengoku_ZH",
	// 	"LapisId": "5486",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_SecretsofSengoku"
	// },
	{
		"DisplayEnName": "Serengeti Gold",
		"DisplayName": "塞伦盖蒂黄金",
		"ButtonImagePath": "BTN_SerengetiGold_ZH",
		"LapisId": "6628",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SerengetiGold"
	},
	{
		"DisplayEnName": "Shamrock Holmes",
		"DisplayName": "三叶草福尔摩斯",
		"ButtonImagePath": "BTN_ShamrockHolmes_ZH",
		"LapisId": "6252",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShamrockHolmes"
	},
	{
		"DisplayEnName": "Shanghai Beauty",
		"DisplayName": "上海美人",
		"ButtonImagePath": "BTN_ShanghaiBeauty",
		"LapisId": "1421",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShanghaiBeauty"
	},
	{
		"DisplayEnName": "Sherlock of London",
		"DisplayName": "伦敦夏洛克",
		"ButtonImagePath": "BTN_SherlockofLondon",
		"LapisId": "4287",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SherlockofLondon"
	},
	// {
	// 	"DisplayEnName": "Shogun Of Time",
	// 	"DisplayName": "时界门之将军",
	// 	"ButtonImagePath": "BTN_ShogunOfTime_ZH",
	// 	"LapisId": "2086",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_ShogunOfTime"
	// },
	{
		"DisplayEnName": "Showdown Saloon",
		"DisplayName": "对决沙龙",
		"ButtonImagePath": "BTN_ShowdownSaloon_ZH",
		"LapisId": "2077",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShowdownSaloon"
	},
	{
		"DisplayEnName": "Silver Fang",
		"DisplayName": "银芳",
		"ButtonImagePath": "BTN_SilverFang1",
		"LapisId": "1062",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverFang1"
	},
	{
		"DisplayEnName": "Silver Lioness 4x",
		"DisplayName": "纯银母狮4x",
		"ButtonImagePath": "BTN_SilverLioness4x",
		"LapisId": "4112",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverLioness4x"
	},
	{
		"DisplayEnName": "Silver Seas",
		"DisplayName": "银色之海",
		"ButtonImagePath": "BTN_SilverSeas_ZH",
		"LapisId": "6954",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverSeas"
	},
	{
		"DisplayEnName": "Silverback: Multiplier Mountain",
		"DisplayName": "银背乘数山",
		"ButtonImagePath": "BTN_SilverbackMultiplierMountain_ZH",
		"LapisId": "6255",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverbackMultiplierMountain"
	},
	{
		"DisplayEnName": "Sisters of Oz Jackpots",
		"DisplayName": "Oz姊妹累积奖金",
		"ButtonImagePath": "BTN_SistersofOzJackpots_ZH",
		"LapisId": "5925",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SistersofOzJackpots"
	},
	{
		"DisplayEnName": "Six Acrobats",
		"DisplayName": "杂技群英会",
		"ButtonImagePath": "BTN_SixAcrobats_ZH",
		"LapisId": "1892",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SixAcrobats"
	},
	{
		"DisplayEnName": "So Many Monsters",
		"DisplayName": "怪兽多多",
		"ButtonImagePath": "BTN_somanymonsters",
		"LapisId": "1001",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somanymonsters"
	},
	{
		"DisplayEnName": "So Much Candy",
		"DisplayName": "糖果多多",
		"ButtonImagePath": "BTN_somuchcandy",
		"LapisId": "1374",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somuchcandy"
	},
	{
		"DisplayEnName": "So Much Sushi",
		"DisplayName": "寿司多多",
		"ButtonImagePath": "BTN_somuchsushi",
		"LapisId": "1032",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somuchsushi"
	},
	{
		"DisplayEnName": "Solar Wilds",
		"DisplayName": "太阳系百搭符号",
		"ButtonImagePath": "BTN_SolarWilds_ZH",
		"LapisId": "6331",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SolarWilds"
	},
	{
		"DisplayEnName": "Songkran Party",
		"DisplayName": "狂欢泼水节",
		"ButtonImagePath": "BTN_SongkranParty_ZH",
		"LapisId": "5480",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SongkranParty"
	},
	{
		"DisplayEnName": "Spring Break",
		"DisplayName": "春假",
		"ButtonImagePath": "BTN_SpringBreak2",
		"LapisId": "1002",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SpringBreak2"
	},
	{
		"DisplayEnName": "Stardust",
		"DisplayName": "星尘",
		"ButtonImagePath": "BTN_StarDust_ZH",
		"LapisId": "1404",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StarDust"
	},
	{
		"DisplayEnName": "Starlight Kiss",
		"DisplayName": "星梦之吻",
		"ButtonImagePath": "BTN_StarlightKiss",
		"LapisId": "1167",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StarlightKiss"
	},
	{
		"DisplayEnName": "Stash of the Titans",
		"DisplayName": "泰坦之藏匿",
		"ButtonImagePath": "BTN_StashoftheTitans1",
		"LapisId": "1320",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StashoftheTitans1"
	},
	{
		"DisplayEnName": "Sterling Silver",
		"DisplayName": "纯银",
		"ButtonImagePath": "BTN_SterlingSilver3D1",
		"LapisId": "1170",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SterlingSilver3D1"
	},
	{
		"DisplayEnName": "Sugar Parade",
		"DisplayName": "糖果巡游",
		"ButtonImagePath": "BTN_SugarParade_ZH",
		"LapisId": "1893",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SugarParade"
	},
	{
		"DisplayEnName": "Summer Holiday",
		"DisplayName": "暑假",
		"ButtonImagePath": "BTN_SummerHoliday",
		"LapisId": "1784",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SummerHoliday"
	},
	{
		"DisplayEnName": "Summertime",
		"DisplayName": "夏天",
		"ButtonImagePath": "BTN_summertime",
		"LapisId": "1130",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_summertime"
	},
	{
		"DisplayEnName": "SunQuest",
		"DisplayName": "探索太阳",
		"ButtonImagePath": "BTN_SunQuest3",
		"LapisId": "1127",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SunQuest3"
	},
	{
		"DisplayEnName": "SunTide",
		"DisplayName": "太阳征程",
		"ButtonImagePath": "BTN_SunTide_Button_ZH",
		"LapisId": "1150",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SunTide_Button"
	},
	{
		"DisplayEnName": "Supe it Up",
		"DisplayName": "跑起来",
		"ButtonImagePath": "BTN_SupeItUp2",
		"LapisId": "1289",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SupeItUp2"
	},
	{
		"DisplayEnName": "Sure Win",
		"DisplayName": "必胜",
		"ButtonImagePath": "BTN_SureWin",
		"LapisId": "1711",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SureWin"
	},
	{
		"DisplayEnName": "Tally Ho",
		"DisplayName": "泰利嗬",
		"ButtonImagePath": "BTN_TallyHo1",
		"LapisId": "1395",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TallyHo1"
	},
	// {
	// 	"DisplayEnName": "Tarzan",
	// 	"DisplayName": "泰山",
	// 	"ButtonImagePath": "BTN_Tarzan_Button_en",
	// 	"LapisId": "1847",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_Tarzan_Button_en"
	// },
	{
		"DisplayEnName": "Tarzan® and the Jewels of Opar ™",
		"DisplayName": "Tarzan® 和欧巴珠宝",
		"ButtonImagePath": "BTN_TarzanandtheJewelsofOpar_ZH",
		"LapisId": "6185",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TarzanandtheJewelsofOpar"
	},
	{
		"DisplayEnName": "Tasty Street",
		"DisplayName": "妹妹很饿",
		"ButtonImagePath": "BTN_TastyStreet_ZH",
		"LapisId": "1901",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TastyStreet"
	},
	{
		"DisplayEnName": "The Finer Reels of Life",
		"DisplayName": "至尊人生",
		"ButtonImagePath": "BTN_TheFinerReelsOfLife_ZH",
		"LapisId": "1200",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheFinerReelsOfLife"
	},
	{
		"DisplayEnName": "The Gold Swallow",
		"DisplayName": "金燕报恩",
		"ButtonImagePath": "BTN_TheGoldSwallow_ZH",
		"LapisId": "6075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGoldSwallow"
	},
	{
		"DisplayEnName": "The Golden Mask Dance",
		"DisplayName": "黄金假面舞",
		"ButtonImagePath": "BTN_TheGoldenMaskDance_ZH",
		"LapisId": "5699",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGoldenMaskDance"
	},
	{
		"DisplayEnName": "The Great Albini",
		"DisplayName": "伟大魔术师",
		"ButtonImagePath": "BTN_TheGreatAlbini",
		"LapisId": "2087",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGreatAlbini"
	},
	{
		"DisplayEnName": "The Heat Is On",
		"DisplayName": "热力四射",
		"ButtonImagePath": "BTN_TheHeatIsOn",
		"LapisId": "1883",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheHeatIsOn"
	},
	{
		"DisplayEnName": "The Immortal Sumo",
		"DisplayName": "永远的横纲",
		"ButtonImagePath": "BTN_TheImmortalSumo_ZH",
		"LapisId": "5530",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheImmortalSumo"
	},
	{
		"DisplayEnName": "The Incredible Balloon Machine",
		"DisplayName": "不可思议的气球机",
		"ButtonImagePath": "BTN_TheIncredibleBalloonMachine_ZH",
		"LapisId": "5448",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheIncredibleBalloonMachine"
	},
	{
		"DisplayEnName": "The Phantom Of The Opera",
		"DisplayName": "歌剧魅影",
		"ButtonImagePath": "BTN_ThePhantomOfTheOpera_ZH",
		"LapisId": "1906",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThePhantomOfTheOpera"
	},
	{
		"DisplayEnName": "The Rat Pack",
		"DisplayName": "鼠帮",
		"ButtonImagePath": "BTN_RatPack1",
		"LapisId": "1398",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RatPack1"
	},
	{
		"DisplayEnName": "The Twisted Circus",
		"DisplayName": "奇妙马戏团",
		"ButtonImagePath": "BTN_TwistedCircus",
		"LapisId": "1386",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TwistedCircus"
	},
	{
		"DisplayEnName": "Thunderstruck",
		"DisplayName": "雷神",
		"ButtonImagePath": "BTN_Thunderstruck1",
		"LapisId": "1028",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Thunderstruck1"
	},
	{
		"DisplayEnName": "Thunderstruck II",
		"DisplayName": "雷神2",
		"ButtonImagePath": "BTN_ThunderstruckTwo1",
		"LapisId": "1330",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThunderstruckTwo1"
	},
	{
		"DisplayEnName": "Thunderstruck Wild Lightning",
		"DisplayName": "雷霆万钧之百搭闪电",
		"ButtonImagePath": "BTN_ThunderstruckWildLightning_ZH",
		"LapisId": "6988",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThunderstruckWildLightning"
	},
	{
		"DisplayEnName": "Tigers Eye",
		"DisplayName": "虎眼",
		"ButtonImagePath": "BTN_tigersEye",
		"LapisId": "1232",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_tigersEye"
	},
	{
		"DisplayEnName": "Tiki Reward",
		"DisplayName": "提基奖励",
		"ButtonImagePath": "BTN_TikiReward_ZH",
		"LapisId": "6216",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TikiReward"
	},
	{
		"DisplayEnName": "Tiki Vikings™",
		"DisplayName": "蒂基维京",
		"ButtonImagePath": "BTN_TikiVikings™_ZH",
		"LapisId": "4290",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TikiVikings™"
	},
	{
		"DisplayEnName": "Titans of the Sun Hyperion",
		"DisplayName": "太阳神之许珀里翁",
		"ButtonImagePath": "BTN_TitansOfTheSunHyperion_ZH",
		"LapisId": "1208",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TitansOfTheSunHyperion"
	},
	{
		"DisplayEnName": "Titans of the Sun Theia",
		"DisplayName": "太阳神之忒伊亚",
		"ButtonImagePath": "BTN_TitansOfTheSunTheia_ZH",
		"LapisId": "1385",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TitansOfTheSunTheia"
	},
	{
		"DisplayEnName": "Tomb Raider",
		"DisplayName": "古墓丽影",
		"ButtonImagePath": "BTN_TombRaider2",
		"LapisId": "1122",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TombRaider2"
	},
	{
		"DisplayEnName": "Tomb Raider Secret of the Sword",
		"DisplayName": "古墓丽影秘密之剑",
		"ButtonImagePath": "BTN_TombRaiderSotS1",
		"LapisId": "1383",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TombRaiderSotS1"
	},
	{
		"DisplayEnName": "Treasure Dash",
		"DisplayName": "逃宝",
		"ButtonImagePath": "BTN_TreasureDash_ZH",
		"LapisId": "5143",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasureDash"
	},
	{
		"DisplayEnName": "Treasure Palace",
		"DisplayName": "财宝宫殿",
		"ButtonImagePath": "BTN_TreasurePalace",
		"LapisId": "1020",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasurePalace"
	},
	{
		"DisplayEnName": "Treasures of Lion City",
		"DisplayName": "海底宝城",
		"ButtonImagePath": "BTN_TreasuresOfLionCity_ZH",
		"LapisId": "4286",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasuresOfLionCity"
	},
	{
		"DisplayEnName": "Untamed Giant Panda",
		"DisplayName": "大熊猫",
		"ButtonImagePath": "BTN_UntamedGiantPanda",
		"LapisId": "1222",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_UntamedGiantPanda"
	},
	// {
	// 	"DisplayEnName": "Village People® Macho Moves",
	// 	"DisplayName": "型男舞步",
	// 	"ButtonImagePath": "BTN_VillagePeople®MachoMoves_ZH",
	// 	"LapisId": "4271",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_VillagePeople®MachoMoves"
	// },
	{
		"DisplayEnName": "Vinyl Countdown",
		"DisplayName": "黑胶热舞",
		"ButtonImagePath": "BTN_VinylCountdown4_ZH",
		"LapisId": "1306",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_VinylCountdown4"
	},
	{
		"DisplayEnName": "Voila!",
		"DisplayName": "瞧！",
		"ButtonImagePath": "BTN_voila_2",
		"LapisId": "1241",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_voila_2"
	},
	{
		"DisplayEnName": "Wacky Panda",
		"DisplayName": "囧囧熊猫",
		"ButtonImagePath": "BTN_WackyPanda_ZH",
		"LapisId": "1911",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WackyPanda"
	},
	{
		"DisplayEnName": "Wanted Outlaws",
		"DisplayName": "歹徒通缉令",
		"ButtonImagePath": "BTN_WantedOutlaws_ZH",
		"LapisId": "6187",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WantedOutlaws"
	},
	{
		"DisplayEnName": "Western Gold",
		"DisplayName": "西域黄金",
		"ButtonImagePath": "BTN_WesternGold_ZH",
		"LapisId": "6072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WesternGold"
	},
	{
		"DisplayEnName": "What A Hoot",
		"DisplayName": "猫头鹰乐园",
		"ButtonImagePath": "BTN_WhataHoot3",
		"LapisId": "1171",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WhataHoot3"
	},
	// {
	// 	"DisplayEnName": "Wicked Tales Dark Red",
	// 	"DisplayName": "黑暗故事神秘深红",
	// 	"ButtonImagePath": "BTN_WickedTalesDarkRed",
	// 	"LapisId": "2064",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WickedTalesDarkRed"
	// },
	{
		"DisplayEnName": "Wild Catch",
		"DisplayName": "荒野垂钓",
		"ButtonImagePath": "BTN_WildCatch_ZH",
		"LapisId": "1152",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WildCatch"
	},
	{
		"DisplayEnName": "Wild Orient",
		"DisplayName": "东方珍兽",
		"ButtonImagePath": "BTN_WildOrient_Button_ZH",
		"LapisId": "1164",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WildOrient_Button"
	},
	// {
	// 	"DisplayEnName": "Wild Scarabs",
	// 	"DisplayName": "百搭圣甲虫",
	// 	"ButtonImagePath": "BTN_WildScarabs_ZH",
	// 	"LapisId": "1992",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WildScarabs"
	// },
	// {
	// 	"DisplayEnName": "Win Sum Dim Sum",
	// 	"DisplayName": "开心点心",
	// 	"ButtonImagePath": "BTN_WinSumDimSum_zh",
	// 	"LapisId": "1345",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WinSumDimSum_en"
	// },
	{
		"DisplayEnName": "WP Forest Party JP",
		"DisplayName": "王牌森林舞会JP版",
		"ButtonImagePath": "BTN_WPForest Party_ZH",
		"LapisId": "5705",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WPForest Party"
	},
	{
		"DisplayEnName": "Zombie Hoard",
		"DisplayName": "丧尸来袭",
		"ButtonImagePath": "BTN_ZombieHoard",
		"LapisId": "2085",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ZombieHoard"
	}
]