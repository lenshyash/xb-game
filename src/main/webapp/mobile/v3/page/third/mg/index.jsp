<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns="http:/www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="Cache-Control" content="max-age=0">
<meta name="viewport" content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=1.0">
<meta name="language" content="zh-CN">
<meta name="MobileOptimized" content="240">
<title>手机网投</title>
<title>MG电子游艺</title>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<meta name="screen-orientation" content="portrait">
<meta name="x5-fullscreen" content="true">
<meta name="x5-orientation" content="portrait">
<link href="${base}/mobile/third/mg/css/tt.css" rel="stylesheet">
<link href="${base}/mobile/third/mg/css/index.css" rel="stylesheet">
<style>
.game_logo {
	position: relative;
	display: inline-block;
	width: 120px;
	height: 140px;
	overflow: hidden;
	cursor: pointer;
}

.game_logo img {
	position: absolute;
	left: 0;
	width: 240px;
	height: 116px;
}

.game_logo img:hover {
	left: -120px;
}

.game_logo1 {
	position: relative;
	display: inline-block;
	width: 120px;
	height: 140px;
	overflow: hidden;
	cursor: pointer;
}

.game_logo1 img {
	position: absolute;
	left: 0;
	height: 116px;
	width: 100%;
}
</style>
</head>
<body>
	<div class="app">
		<div class="head">
			<a href="${m }/index.do" class="ico ico_home"></a>
			<h1>MG电子</h1>
		</div>
		<div class="app-body">
			<div class="app-content">
				<div class="scrollable-index">
					<div class="scrollable-con">
						<div class="game-content" id="gamelist_div"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base }/common/template/third/egame/js/h5common.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script id="gamelist_tpl" type="text/html">
	{{each data as game}}
		<div class="game-item">
			<div class="game_logo{{game.single }}" onclick="toMG_motile({{game.LapisId}});">
				<img src="${base}/common/template/third/egame/images/v2/{{game.ButtonImagePath}}.png">
			</div>
			<p>{{game.DisplayName}}</p>
		</div>
	{{/each}}
</script>
<script>
	$(function() {
		var eachdata = {
			"data" : hgegamedata
		};
		var html = template('gamelist_tpl', eachdata);
		$("#gamelist_div").html(html);
	});

	function toMG_motile(gameid) {
		var sw = '';
		csw = '';
		sh = '';
		csh = '';
		ctop = '';
		cleft = '';

		sw = $(window.parent).width();
		sh = $(window.parent).height();
		csw = $(window.parent).width();
		csh = $(window.parent).height();
		ctop = 0;
		cleft = 0;

		var windowOpen = window.open("", '_blank', 'width=' + csw + ',height='
				+ csh + ',left=' + cleft + ',top=' + ctop
				+ ',scrollbars=no,location=1,resizable=yes');
		$.ajax({
			url : "${base}/forwardMg.do",
			sync : false,
			data : {
				gameType : 3,
				h5 : 1,
				gameid : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href = json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
	}
</script>

