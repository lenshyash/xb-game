<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<style>
		#openChat{
			width:50px!important;
			top:50px!important;
		}
	</style>
<%--	 长龙助手--%>
	<div class="box-warp">
		<div class="longLoading" style="position: absolute;width: 100%;height: 100%;display: none;justify-content: center;align-items: center;z-index:9999;background: rgba(0,0,0,.5)">
			<img src="data:image/gif;base64,R0lGODlhJQAlAJECAL3L2AYrTv///wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgACACwAAAAAJQAlAAACi5SPqcvtDyGYIFpF690i8xUw3qJBwUlSadmcLqYmGQu6KDIeM13beGzYWWy3DlB4IYaMk+Dso2RWkFCfLPcRvFbZxFLUDTt21BW56TyjRep1e20+i+eYMR145W2eefj+6VFmgTQi+ECVY8iGxcg35phGo/iDFwlTyXWphwlm1imGRdcnuqhHeop6UAAAIfkEBQoAAgAsEAACAAQACwAAAgWMj6nLXAAh+QQFCgACACwVAAUACgALAAACFZQvgRi92dyJcVJlLobUdi8x4bIhBQAh+QQFCgACACwXABEADAADAAACBYyPqcsFACH5BAUKAAIALBUAFQAKAAsAAAITlGKZwWoMHYxqtmplxlNT7ixGAQAh+QQFCgACACwQABgABAALAAACBYyPqctcACH5BAUKAAIALAUAFQAKAAsAAAIVlC+BGL3Z3IlxUmUuhtR2LzHhsiEFACH5BAUKAAIALAEAEQAMAAMAAAIFjI+pywUAIfkEBQoAAgAsBQAFAAoACwAAAhOUYJnAagwdjGq2amXGU1PuLEYBACH5BAUKAAIALBAAAgAEAAsAAAIFhI+py1wAIfkEBQoAAgAsFQAFAAoACwAAAhWUL4AIvdnciXFSZS6G1HYvMeGyIQUAIfkEBQoAAgAsFwARAAwAAwAAAgWEj6nLBQAh+QQFCgACACwVABUACgALAAACE5RgmcBqDB2MarZqZcZTU+4sRgEAIfkEBQoAAgAsEAAYAAQACwAAAgWEj6nLXAAh+QQFCgACACwFABUACgALAAACFZQvgAi92dyJcVJlLobUdi8x4bIhBQAh+QQFCgACACwBABEADAADAAACBYSPqcsFADs=" alt="">
		</div>
		<div class="longDragonMenu">
			<h3  class="dragon-tit pr">长龙助手<img class="dragonX" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAtCAYAAADoSujCAAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMzggNzkuMTU5ODI0LCAyMDE2LzA5LzE0LTAxOjA5OjAxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+IEmuOgAAA9ZJREFUaIHN2cuSFEUUxvEf08PA9Na30JcQRC7OzgYREUciBBHBOyIijhdURIJAAxERUUTEhffLGsOXYeEbeBlcZFbQU2ZmVXfXDH6brqqTec7/q+qszKpadf36dQmtwVWcxW+pBiuou3EAD+HPenAq0WENvscAv2LdMsI1aV1kGOAHgW2J6gbWxoZb4n4fv2D98jFmtT7W7sf9LRImhg1UZ35zLVEfP1tZE/fEmv3a8c1qJoYN/I0bmYSViQ3dMWa1AT/5L3ylG/ir2hk28A8ew6VMxz5+tLwm7o01cvCXBMbF6kB9DCxij2YTGyfCTGuj8PdoDU/6LrQYG36aSdQXxkqXJprgL0amm/VAyoDYcE/smFI/Ftw0EmZam2Ku2Uz8IvZKwJM3IHbYi08y8VnhSkxiogn+ggI8ZQNix8djopRmI0D91ttG1S1xbSZ+AfsU4Gk2ICbYh48z8bXS80dJTfDntYCnnQEx0RMxcUr1Gbyk5Iw6pPPYrwU87Q2ICffjo0y8mslLJprgzxkBntEMiImfjIVSWiMA3peIzcXYTKbvOWHV2Rqe0Q2IBQ7gw0x8RrgSc0PH5vCdPPxZY8DD9Kgdom7ioFtm6poRgAdYhW+U4Z8yBjzjGxALVoUPJuIz+HZoO6UP8Iwx4ZnMgFj4abfM1JUDpwN4xhsDdd2MIO+P0OeMDuDpxgAB5FkBrEln8JwO4OnOAAHod7Xlbk2LsU0n8HRrYICvG3JOxTb3d1W0KwMDfKU8aCvN4FrsM7G6MDAQgNrAV+rMxKQGtkaQ1Zn4eziVia2OfbdOAjDJPFDB53KcxOG4vYgXE20qE4TZemSNewW2KcO/6xa8uH0y03Y65to2Dsg4BprgT+ClxPHDMZbStHATGNnEqAa2C/C9TPwdHCn0P6Js4hoeGAVoFAPbhTfWOfi38XKLPEdi25R6wpXY3haqrYEdyvBv4WjborFtycRVPNgmURsDO3BFHv44XmlTrKajsW9KPXwZaxfVZGCnMvybONZUpKBjyiauCB82sioZ2InL8vBv4NUGwDY6FnOl1MMXkSWpnIFdyvCvY6ElYBstKJu4jIdTwZSBXfhMHn4Br43G10qlvD18LrAtUd3AvGb43JnqQqUr2xPYHhk+OGxgXnilnoMv/Ve7VGls9YTvBPPVgcrAbs3wubvFcqh0d+sJrI8Spu/dwiv0HHxp0llOHRcePVMnrid8N1g1hbv8/+ArlWb4Hu6cwgs4nWhQWrOspHJrrNM4VI2B5y19ciqtGm+H6qvcUwLzkjX9ofj7h/zDx+3UCWFM3GHo6e5fGGTNxIG6lqMAAAAASUVORK5CYII="></h3>
			<div class="mint-navbar">

			</div>
			<div class="mint-list">

			</div>
			<div class="mint-qihao" v-if="changlongLottery">
				<div>期号：<span id="longQiHao"></span></div>
				<div style="color: red">倒计时：<span id="longDaoJiShi"></span></div>
			</div>
			<div class="mint-lottery-list">
				<table id="longContent">

				</table>
			</div>
			<div class="mint-touzhu">
				<span>每注<input id="longMoney" type="number" min="0">元</span>
				<span><input onclick="longTouzhu()" type="button" value="投注"></span>
			</div>
		</div>
	</div>
	<script>
		var longData = ''
		// 长龙助手
		function showLong(){
			$(".box-warp").css('display','flex')
			getLongMenu()
			$(".dragonX").click(function () {
				$('.box-warp').css('display','none')
			})
		}
		// 获取一级菜单
		function getLongMenu(){
			var html = ''
			$(".longLoading").css('display','flex')
			$.ajax({
				url : "${base}/native/longDragonMenu.do",
				type:'get',
				data : {},
				success : function(result) {
					longData = result.content
					$(".longLoading").css('display','none')
					for(var i = 0; i <result.content.length; i++){
						html +='<div data-index='+i+'>'+result.content[i].name+'</div>'
					}
					$(".mint-navbar").html(html)
					getLoingMenuTwo(0)
					$(".mint-navbar div").click(function () {
						$(this).addClass('is-selected')
						$(this).siblings().removeClass('is-selected')
						getLoingMenuTwo($(this).attr('data-index'))
					})
				}
			});
		}
		function getLoingMenuTwo(index){
			var html = ''
			for(var i = 0; i <longData[index].childen.length; i++){
				html +='<div data-code='+longData[index].childen[i].code+'><img src="'+longData[index].childen[i].imgUrl+'"><br><span>'+longData[index].childen[i].name+'</span></div>'
			}
			$(".mint-list").html(html)
			$(".mint-list div").click(function () {
				longDragonLottery($(this).attr('data-code'))
			})
			longDragonLottery(longData[index].childen[0].code)
		}
		var countdownMs = '' ,countdownEnd = '' ,changlongCode  ='' ,changlongLottery ={} , touzhuClick = 999 ,touzhuItem = 0;
		function longDragonLottery(code){
			$(".longLoading").css('display','flex')
			var html = '<tbody>'
			countdownMs = ''
			if(code){
				changlongCode = code
			}
			var params = {
				lotCode:changlongCode
			}
			$.ajax({
				url : "${base}/native/lotLdList.do",
				type:'post',
				data : params,
				success : function(result) {
					$(".longLoading").css('display','none')
					changlongLottery = result
					window.clearInterval(countdownEnd)
					countdownEnd = setInterval(() => {
						Countdown();
					}, 1000);
					$("#longQiHao").html(changlongLottery.current.qiHao)
					for(var i = 0 ; i < changlongLottery.ommitList.length; i++){
						html+='<tr><td>'+changlongLottery.ommitList[i].groupName+'<span style="color: blue;margin-left: 5px">'+changlongLottery.ommitList[i].playName+'</span><span style="color: red;margin-left: 5px">'+changlongLottery.ommitList[i].omissionCount+'</span></td><td>'
						for(var j = 0; j < changlongLottery.ommitList[i].playNumbers.split(',').length; j++){
							html +='<span class="codeOption" data-touzhu='+j+' data-item='+i+'>'+changlongLottery.ommitList[i].playNumbers.split(',')[j]+'</span>'
						}
						html+='</td></tr>'
					}
					html +='</tbody>'
					$("#longContent").html(html)
					$("#longContent tr td span").click(function () {
						$('#longContent tr td').children().removeClass('codeOptionOn')
						$(this).addClass('codeOptionOn')
						touzhuClick = $(this).attr('data-touzhu')
						touzhuItem = $(this).attr('data-item')
					})
				}
			});
		}
		function Countdown(){
			var time = 0;
			if(countdownMs){
				time = countdownMs - 1000
			} else {
				time = (new Date(changlongLottery.current.activeTime.replace(/-/g,"/"))).getTime() - (new Date(changlongLottery.current.serverTime.replace(/-/g,"/"))).getTime()
			}
			if(time <= 0){
				$("#longDaoJiShi").html('正在开奖')
				longDragonLottery()
			} else {
				var minutes = parseInt((parseInt(time) % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = parseInt((parseInt(time) % (1000 * 60)) / 1000);
				if (isNaN(minutes) || isNaN(seconds)){
					minutes =  (((time | 0) % (1000 * 60 * 60)) / (1000 * 60) | 0);
					seconds = (((time | 0) % (1000 * 60)) / 1000 | 0);
				}
				$("#longDaoJiShi").html(minutes + " 分 " + seconds + " 秒 ")
				countdownMs = time
			}
		}
		function longTouzhu(){
			var item = changlongLottery.ommitList[touzhuItem] , money = $('#longMoney').val()
			if(touzhuClick == 999){
				alert('请选择下注内容');
				return false
			}
			if(money <= 0){
				alert('请正确填写投注金额');
				return false
			}
			var params = {
				lotCode:item.lotCode,
				playName:item.playNumbers.split(',')[touzhuClick],
				groupName:item.groupName,
				groupCode:item.groupCode,
				playCode:item.playCode,
				qiHao:changlongLottery.current.qiHao,
				money:money,
			}
			$(".longLoading").css('display','flex')
			$.ajax({
				url : "${base}/native/doLdBet.do",
				type:'post',
				data : params,
				success : function(result) {
					$(".longLoading").css('display','none')
					if(result.success){
						alert('投注成功');
					} else {
						alert(result.msg);
					}
				}
			});
		}
	</script>
	<style>
		.box-warp{
			position: absolute;
			z-index: 99999;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			background:rgba(0,0,0,.6);
			display: none;
			align-items: center;
			justify-content: center;
			font-size: 14px;
		}
		.longDragonMenu{
			width: 93%;
			background: #ffffff;
			border-radius: 3px;
			display: inline-block;
			vertical-align: middle;
			float: left;
		}
		.dragon-tit {
			height:45px;
			line-height:45px;
			background:#f4f4f4;
			font-weight:400;
			position: relative;
			text-align: center;
			font-size: 16px;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		}
		.dragonX{position:absolute;
			top:0;
			bottom:0;
			margin:auto 0;
			right:10px;
			width:15px;
		}
		.mint-navbar{
			height: 40px;
			width: 100%;
			background: #ffffff;
			float: left;
			display: flex
		}
		.mint-navbar div{
			float: left;height: 100%;font-size: 12px;width: 100%;text-align: center;line-height: 40px;border-bottom: 2px solid #ffffff;color: #000000;
		}
		.is-selected{
			border-bottom: 2px solid #dc3b3b!important;
			color: #dc3b3b!important;
		}
		.mint-list{
			height: 80px;
			width: 100%;
			background: #ffffff;
			float: left;
			display: flex;
			overflow-x:auto;
		}
		.mint-list div{
			margin-left: 10px;
			min-width: 80px;
			text-align: center;
		}
		.mint-list img{
			width: 50px;
		}
		.mint-list span{
			font-size: 12px;
			white-space:nowrap;
		}
		.mint-qihao{
			height: 40px;
			width: 100%;
			background: #ffffff;
			float: left;
			display: flex;
		}
		.mint-qihao div{
			width: 50%;
			text-align: center;
			line-height: 40px;
		}
		.mint-lottery-list{
			float: left;width: 100%;height: 200px;overflow-y: auto
		}
		.mint-lottery-list table{
			width: 100%;
			text-align: center;
		}
		.mint-lottery-list table td{
			width: 35%;
			height: 30px;
			line-height: 30px;
		}
		.mint-lottery-list table td:nth-child(2){
			width: 65%;
			height: 30px;
			line-height: 30px;
		}
		.mint-lottery-list table td:nth-child(2){
			width: 100%;
			display: flex;
			justify-content: center;
			align-items: center;
			margin: 8px 0;
		}
		.mint-lottery-list table td:nth-child(2){

		}
		.mint-lottery-list table td .codeOption{
			display: inline-block;
			padding: 3px 15px;
			border:1px solid #7d7e80;
			float: left;
			margin-left: 10px;
			border-radius: 5px;
		}
		.codeOptionOn{
			background: red!important;
			color: #ffffff!important;
			border: 1px solid red!important;
		}
		.mint-touzhu{
			margin-top: 10px;
			height: 40px;
			width: 100%;
			background: #ffffff;
			float: left;
		}
		.mint-touzhu span{
			display: inline-block;
			height: 100%;
			width: 50%;
			float: left;
			line-height: 40px;
			padding-left: 20px;
			box-sizing: border-box;
		}
		.mint-touzhu span:nth-child(2){
			padding-left: 30px!important;
			text-align: center;
		}
		.mint-touzhu input[type='number']{
			width: 50px;
			height: 30px;
			border: 1px solid #7d7e80;
			border-radius: 5px;
			margin: 0 4px;
			padding-left: 5px;
		}
		.mint-touzhu input[type='button']{
			color: #ffffff;
			background: orange;
			border-radius: 5px;
			padding: 0 15px;
			height: 35px;
			line-height: 35px;
			margin-top: 2.5px;
		}
	</style>
	<div class="page" id="page_index_jsp">
	<span id="colorOn" style="display:none;">1</span>
		<!-- 头部 -->
		<header class="bar bar-nav">
			<div class="content">
				<h1 class="title title-1" style="width:12rem;">
					<span class="icon icon-menu open-panel" data-panel='#panel-left-demo'></span>
					<c:choose>
						<c:when test="${not empty mobileLogoUrl && domainFolder !='x00286' && domainFolder !='x002100'}">
							<img style="position:absolute;height:2.2rem;margin-left:5px;" src="${mobileLogoUrl }" /></h1>
						</c:when>
						<c:otherwise>
							<span class="bar-title">${_title }</span></h1>
						</c:otherwise>
					</c:choose>
<%--					<c:if test="${not empty mobileLogoUrl}">--%>
<%--							<img style="position:absolute;height:2.2rem;margin-left:5px;" src="${mobileLogoUrl }" /></h1>--%>
<%--					</c:if>--%>
<%--					<c:if test="${empty mobileLogoUrl}">--%>
<%--							<span class="bar-title">${_title }</span></h1>--%>
<%--					</c:if>--%>
				<h1 class="title title-2" style="padding-right:0;margin:0;">
					<div class="login_status_bar">
						<c:if test="${!isLogin }">
							<em class="iconfont icon-user8"></em>
							<a href="${m }/login.do" class="external">登录</a>
							<c:if test="${isReg eq 'on'}">
								<a href="${m }/regpage.do" class="external"> | 注册</a>
							</c:if>
						</c:if>
						<c:if test="${isLogin }">
							<a external href="${m }/personal_center.do">
								<div class="user_logined_img">
									<c:choose>
										<c:when test="${domainFolder == 'x00246'}">
											<img src="${res }/images/image_2020_10_09T06_50_49_618Z.png">
										</c:when>
										<c:otherwise>
											<img src="${res }/images/touxiang.png">
										</c:otherwise>
									</c:choose>
								</div> <span> <em class="iconfont icon-jindous2x"></em><span>${loginMember.money }</span>
							</span>
							</a>
						</c:if>
					</div>
				</h1>
				<!-- 判断如果域名是 bet365lk那么久显示副标题 不是就不显示 -->
				<div id="reserveTitle" style="display:none;position:absolute;top:1.7rem;color:#fff;font-size:14px;">
					------2018世界杯官方指定网投
				</div>
			</div>
		</header>

		<!-- 底部工具栏-->
		<jsp:include page="include/barTab.jsp" />
		<div class="content reserveTitle">
						<script>
						if(window.location.host.indexOf("bet365lk.com") != -1 ){
							$("#reserveTitle").show();
							$(".bar-nav").height('3rem');
							$(".reserveTitle").css('top','3rem');
						}
				</script>
			<!-- 这里是页面内容区 -->

			<!-- 轮播图 -->
			<c:if test="${!empty agentLunBos }">
				<jsp:include page="include/index/lunbo.jsp" />
			</c:if>

			<!-- 系统公告 -->
			<c:if test="${not empty publicInfo }">
				<div class="row no-gutter notice">
					<span class="tips_title">公告</span>
					<div id="noticeContext" style="margin-left: 47px; overflow: hidden; height:25px; line-height: 1; padding: 3px 0px; position: relative;">
					     <div id="noticeText" style="white-space: nowrap; position: absolute;">
<%--								${publicInfo }--%>
					     </div>
					     <script>
					     	var text = $('#noticeText').text();
					     	$('#noticeText').text(text);
					     </script>
				     </div>
				</div>
			</c:if>
			<style>
				.notice .tips_title{
					border:none!important;
				}
			</style>
			<script type="text/javascript">
			$(function(){
				var str = $("#noticeText").html()
				if(str != undefined){
					str=str.replace(/<p>/g,"");
					str=str.replace(/<\/p>/g,"");
					str=str.replace(/<br>/g,"");
					$("#noticeText").html(str)
				}
			})
			function newsNotice(){
				var ntWidth = 0;
			    try { /// 消息滚动
			        setTimeout(function () {
			            var noticeText    = document.getElementById('noticeText');
			            if (!noticeText) return false;
			            noticeText.style.left = noticeContext.offsetWidth + 'px';
			            ntWidth = noticeContext.offsetWidth + 'px';
			            //当字数太少时赋予默认初始宽度
			            if (noticeText.scrollWidth < noticeContext.offsetWidth) {
			                noticeText.style.width = noticeContext.offsetWidth + 'px';
			            }
			            setInterval(function () {
			                var cw = noticeContext.offsetWidth,
			                    tw = noticeText.scrollWidth,
			                    tx = noticeText.offsetLeft;
			                if((tw + tx) < 0){
			                	//重新开始滚动
			                	noticeText.style.left = ntWidth;
			                }else{
			                	//持续滚动
			                	noticeText.style.left = (tx - 6) + 'px';
			                }
			            }, 200);
			        }, 500);
			    } catch (e) { }
			}

			newsNotice();
			</script>

			<!-- no-gutter: 无间距 -->
			<c:if test="${iosExamine ne 'off' }">
				<div class="row no-gutter quickMenu animated bounceInLeft" style="display: flex;justify-content: space-between">
					<c:if test="${onoffLongDragon  eq 'on'}">
					<div class="col-20 " style="margin: auto;">

							<a external class="qMenuItem color_huang" onclick="showLong()" style="margin-top: 0.4rem">
								<div class="b-r-b50" style="background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABgCAMAAAA+cTwJAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAABpUExURUxpcfddO/hiN/deOPttMvtpNf1zL/tsM/1vN/1xMv////lmNfxvMP97KvppNP12Lf55LPZbOvprM/10MP77+P7p3/7Zyf20lf3CqfyJW/7z7vtxP/2ngf1/QPycdfyFSvyVZf3Mt/l4TkbjOGUAAAAKdFJOUwDq//+mfOfNHE2nZ7wUAAAJJUlEQVRo3q1aiWKqOhBt3VBqlUpWEtb//8h3JgmYIFjtfWOrEJI5sycBPj5+o+PxdNrvdll2OBTFN6goDocs2+32p9Px+PGvdATzw/dTOgDq+Hf+v7GPgf6Ac9xn329Stn8L5rT7/hPtTi8jZN9/puwllOM/IDiUXy123BXf/0jF7jnK8fD9P9DhGcipQJLNFEHD1LZ2PO9brHtlXwQM6vsOxWP88f4ZxP9F+1VD/U51W78GcvorRN1Z20jZPe1Tr4EcD68giIrlrBK9UHZdCu2PHqNrdy2u+CsK99tC3hYH16jtWnSK5Z5YVXHZ6La9M0e3orW9ELIt/Jjd3FLXkVxnq4RoJ97u93q1fR4RY6xSFrLomryku8ZKJXjf1dO4mbWya0yW57lorykV0mvBBa9yf8jyivcQXSgF9rAj7219H5CtqOEu9mAh5xit9HxVo7UUFeiuEbzEuIBK6ZDTuhqtInFlkQ6oHQbj4FPXte4guRCcIDgOeiXJaumQbFWNq7aMmM1AisYZqOqutb5eS5Kl67SGGyTcD/ZF2870iBXZzc3CncgylSu0cs7VyKu8FuTisqg1wqR/sG8UWjMVr6WtSGYmbHyllCKYvyOhG6SjhtHo18VUb+d8rkVqqvJals4CPoaEA+F9XZTTtbYVFLEcvOoG0Y1IquAKF1HMhVR5XTPWZCriRQTl2555y6gGHoaA7oLuIK9sy3A1hBWAlK3LEllSl7Gs5d1YhzKiq5cbYcQZcxyEpPySTadreBWKAYsH/siMvldKdnVZwGrIFaXbYpS1LA9jqSoXqW47VbnQ947GMVeAoksy6MDBkUDh8p6zscrYiEsoWieS/jx9YhyKFp8EIa/zCrYr67FwVYqqSGdlL/hkPVbJYuIQHLLDIVhf/f8cpkYtty6vAxOkc9nwkZ+QsudVxJ/Sn6FLoOCQzDnhjK9zeXZUdrAwjtFAbTBbXbeW6pQzkCxrO4HkCUlNBkZN83zOZUj1w3lOpeTOqnXXlPfG0gc0+La4IticP1EPd3UIFt6Fgd7pR4cXfUAcNu20EpXQZQRdBxCrz2XbSCg2lqwRgwt5Pneo0L0Og5zTT49qWF6JuobSQjTt+YyQbSlsIaK3lmrQC2mDqQzOaqTibIRBmdMGMVHJAHIKGKkWxiJGWGckYzU5w1XYHvlRn1sR3D6aj/qTCOgz1npk7bkWHF67Y+znWmhIBTNIJIG1CCnKiw5pCJVq5dl4N5VUeI0BBtCMViG+mOggGM+Rl+i09xipGjW5rGmQ3RXNeYTQtMaLPM5T3qO6QdzCI5hySckzKeMu9+gPSWU9YuxmahBrea7B19IIGNZMFwNGDsFpLpxmQRQsJeGvVvn4lh1klYpMukswQgQhv5izZasondQ9ssDXG0NCTMVnoQufdYh6h8xFSUHRjhjZzB+NIIO3ppXkejkqYepJcqH1lIPxSgXrIXP28e2tSf/ZEoaWrga65Rq3ZFKDstX3U0liUlu+kH9TTNE1MebHIobRLBQJxHrtCosQcQJYhFC+Qkjwc0eTSxVCdxnjbGTlZg7h0ojOYnMoRGuTrxIMaTTVmVGRLJSr2/RxVFukeEXzHZ34tKs4x1yksAKBNVX+BETaUsNvlY0K1mNJPBtjMG16b2s3aXRUODTC3kWweIKBtaIxik/RcnjQY9JlIqQLjGziprE0rmnSIhtRgttFPYCQEAFCaKaMv0bn+DLPMXLVksdE4/o7jFvK+2zu/KndYEegZm2qeorB5a11xcLxdRgp6VykDQbrnW5ImqyINgj8IVeYMDeBnYPvvIDR5L1n3bcTbDuYpM9g/QLPak0rLjXXq+puKBJB+wUMmzt81MOZPrFqVP657AI0TYmJLswazMXyjnFIx8tcQlAYoze3dRBV9To+b2RSwOQwqNxjHBYwVN7cUF55d3tCFyxc0oY0L1VnFGseMC63C/5utz7vXALR8dhOROdjHwg+NDNUHbteWGB4KTzGJSGRD1LowHe4rNNtdh6Xey4vigvX7DCytCvPB7c5ok1kXl1eJyPvilT2hj28a85SDNWLaNlKRVa8gfGl78EFXzSqW8KgqZXTeh9rJj2Yy5t0s/c06YyWZgmD+A45ZH+fvx8/eYQNt6G53DF2d22pXw4zCr7u7K/L1xf1HH8jGiaP9MNlCCz8uiQZ0+Tyy6DUdl9f9/Y1irHoq6umHHTDqH3EiKjJLb4hkPx6n4aAwdQwtTmMfdLN5g39wLRieBfiMrAxPe6N+0cM6fRAf5VXzbsgOmD0eoZxmtlKUGc6Yrma8RBON7OK0XhbsT7q4tbtx6SbwQSGGZ9ABqFn5s6Zw17VL2xFq9iXx0cM8kTis8RXTkdWrXkqJDo3c4yPT3e8+do4wkFiDGrHF1HvfSUdVNR/7Ofvp2CxHA3/DHvnpP/ScaAqH6ht4NisTY33foMIWxwTtYe9837zK2nsp6TC8HGBIBY6NWFFKePGcC/59DuGHFfMgOldyVyQow+3HpLWcJ/h+IIexJYxMywq4LsESymTNI83ebPNK9TkatPlauWqseEunU2apzuK+5cwMEFubO5YGPMAoXxdn0Fsplv7x1cgNJlJ5uQIW6llQ1WimV243w9/xViC2PfQhfakM4xBcn9rUA2bZVO9ZKwh5wTENC3jE05b0wQt1FyLTfQU5Pj5K0afEwMSt09lxeYw3ERthvmgz/jRwS6ItNk6woE7pt8xqMRmSxkg9OZ+HYS1gfc2do8P45MnB8dtQiPvcczWcDZsKcf4vc0BKGcm2oluFsanT0D226eEBTK+ONytA3PjdfAbfqHc+Zzmz6SyZxAa5Rost1AF/Nwd0EbLnrunBcouAmy32cPjqKd6aB2Mo+jWG93QlXQrVAjaTA/LENvHB1JPrfWTumvQ9LBA2lX2S5ZysbV9lX7M8Iy5p93iA8LsdZDfu2Rrz5y3f6cU9mf92fPuYaD/+L+fpNWdju3J76qhguN/tvFn+3gas/y5n/9M1wh5/8tbAD8/Y18/ztM25hFhxRjb0P/3NwH2n9uI9/v0uX/ptY/PeIy3yOsIx1ffXsneld6Lkb31JsspVeY1FU5/eVXmdYA3X5KZv1P0i0Kf//JO0eO7UZ+fAQ8Hb70b9R+8urcRCxg9mwAAAABJRU5ErkJggg==);background-size:100% 100%;width: 36px;height: 29px;margin: 0 auto;" title="${onoffLongDragon}">
								</div>
								长龙助手</a>
					</div>
					</c:if>
					<div class="col-20" style="margin: auto">
						<a external class="qMenuItem color_huang" href="${m }/personal_center.do"><icon
								class="iconfont ft34">&#xe675;</icon>存／取款</a>
					</div>
					<div class="col-20" style="margin: auto">
					<c:choose>
						   <c:when test="${fn:contains(domainFolder,'b202')}">
						        <a external class="qMenuItem color_lv" href="${m}/active.do"><icon
								class="iconfont pb2">&#xe64c;</icon>优惠活动</a>
						   </c:when>
						   <c:otherwise>
						   <a external class="qMenuItem color_lv" href="${m }/personal_center.do"><icon
								class="iconfont pb2">&#xe64c;</icon>投注记录</a>
						   </c:otherwise>
						</c:choose>
					</div>
					<%-- <div class="col-25">
						<a external class="qMenuItem color_hong" href="${m }/live_game.do" style="padding-top: 33px;">
							<img src="${base }/mobile/anew/resource/images/livedemo.png" style="position: absolute;margin-left: 17px;margin-top: -28px;"/>
						额度转换</a>
					</div>--%>
					<div class="col-20" style="margin: auto">
						<c:choose>
						   <c:when test="${fn:contains(domainFolder,'b202')}">
						         <a external class="qMenuItem color_hong" style="white-space:nowrap;font-size:0.65rem;"
								href="${m }/appDownload.do"><icon class="iconfont pb2">&#xe630;</icon><span id="amylMoney">APP下载</span></a>
						  	<script>
								 if(window.location.host == 'am1512.com' || window.location.host == 'www.am1512.com' || window.location.host == 'jindingplay.com' || window.location.host == 'jinding990.com'){
									$("#amylMoney").html('下APP送18元')
								}
							 </script>
						   </c:when>
							<c:when test="${fn:contains(domainFolder,'t006') || fn:contains(domainFolder,'t00606')}">
								<c:if test="${isActive }">
									<a class="qMenuItem color_hong external" href="${m}/active.do"><icon
											class="iconfont pb2">&#xe630;</icon>优惠活动</a>
								</c:if>
								<c:if test="${!isActive }">
									<a external class="qMenuItem color_hong"
									   href="${appQRCodeLinkIos }"><icon class="iconfont pb2">&#xe630;</icon>APP下载</a>
								</c:if>
							</c:when>
						   <c:otherwise>
						   	<c:if test="${isActive }">
							<a class="qMenuItem color_hong external" href="${m}/active.do"><icon
											class="iconfont pb2">&#xe630;</icon>优惠活动</a>
								</c:if>
								<c:if test="${!isActive }">
							<a external class="qMenuItem color_hong"
								href="${m }/appDownload.do"><icon class="iconfont pb2">&#xe630;</icon>A P P下载</a>
						</c:if>
						   </c:otherwise>
						</c:choose>

					</div>
					<div class="col-20" style="margin: auto">
						<a class="qMenuItem color_lan external"
						   <c:if test="${mobileOpenOutlinkType eq 'blank' }">href="javascript:void(0);" onclick="window.open('${kfUrl }');"</c:if>
						   <c:if test="${mobileOpenOutlinkType eq 'inner' }">href="${m }/customerService.do"</c:if>
						   <c:if test="${mobileOpenOutlinkType eq 'href' }">href="${kfUrl }"</c:if>>
							<icon class="iconfont pb2">&#xe654;</icon>在线客服
						</a>
					</div>
				</div>
			</c:if>
			<div class="tabBox" style="margin: auto;">
				<div class="hd">
					<h3>
						<a href="javascript:void(0);" style="color: #967f7f;">游戏大厅</a><span>GAMES</span>
					</h3>
				</div>
			</div>
			<div class="row no-gutter index_margin animated bounceInRight">
				<c:if test="${v eq 'v1'}">
					<jsp:include page="include/index/games_cp.jsp"></jsp:include>
					<jsp:include page="include/index/games_ty.jsp"></jsp:include>
					<jsp:include page="include/index/games_zr.jsp"></jsp:include>
					<jsp:include page="include/index/games_qpify.jsp"></jsp:include>
				</c:if>
				<c:if test="${v eq 'v2'}">
					<jsp:include page="include/index/games_zr.jsp"></jsp:include>
					<jsp:include page="include/index/games_ty.jsp"></jsp:include>
					<jsp:include page="include/index/games_cp.jsp"></jsp:include>
					<jsp:include page="include/index/games_qpify.jsp"></jsp:include>
				</c:if>
				<c:if test="${v eq 'v3'}">
					<jsp:include page="include/index/games_ty.jsp"></jsp:include>
					<jsp:include page="include/index/games_cp.jsp"></jsp:include>
					<jsp:include page="include/index/games_zr.jsp"></jsp:include>
					<jsp:include page="include/index/games_qpify.jsp"></jsp:include>
				</c:if>
				<c:if test="${v eq 'v5'}">
					<jsp:include page="include/index/games_qpify.jsp"></jsp:include>
					<jsp:include page="include/index/games_ty.jsp"></jsp:include>
					<jsp:include page="include/index/games_cp.jsp"></jsp:include>
					<jsp:include page="include/index/games_zr.jsp"></jsp:include>
				</c:if>
				<c:if test="${v eq 'v4'}">
					<jsp:include page="include/index/games_tab_category.jsp"></jsp:include>
				</c:if>

			</div>
			<jsp:include page="drawRoll.jsp"></jsp:include>
			<c:if test="${not empty onOffSignIn && 'on'  eq onOffSignIn}">
				<div class="m_hot sign" style="margin-top:10px;">
					<c:if test="${!isLogin }"><a href="javascript:MobileIndexUtil.toLogin();" external></c:if>
					<c:if test="${isLogin }"><a href="${m }/signIn.do" external></c:if>
						<img src="" width="100%">
					</a>
				</div>
			</c:if>
			<c:if test="${not empty onOffTurnlate && 'on'  eq onOffTurnlate}">
				<div class="m_hot rotary" style="margin-top:10px;">
					<c:if test="${!isLogin }"><a href="javascript:MobileIndexUtil.toLogin();" external></c:if><c:if test="${isLogin }"><a href="${m }/turnlate.do" external></c:if>
						<img src="" width="100%">
					</a>
				</div>
			</c:if>
			<c:if test="${domainFolder == 'd00308'}">
				<div class="m_hot " style="margin-top:10px;">
					<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADFApgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwBuk6XYarc20NqkNy+7AS2jQqmMkZcuCcd8A8V0Nv8ADHUDFO089jHKQAiwlgsmDkb8r8pHtn0qn4Akt9T1WRLGL7Lu02YLItwHkjJkXlRgcfMTn6etakL3NzpuqXMWu6rHNYAl4Z2GSRn0J4yDUYiu6TtY2w+H9sm72LFj4D1BddbUdSmsrnfJukdi7vIOTxlQEJPpnjjtTP8AhXcrrJKTbW7s3yW8c7tGp4BYOVDH12kcnvUKTXMOhWmp3GvakGuWKLBC+WYgnpk+361u6q15p+j6bbHWvsjs+ZprpgJWXqQOoLDPT9ayjjLpuxtPBcrUeb8+hhzfDq+jldrZ7J1kRFZXdkPHGMhScAdDioo/APiG0leW0vLDzX3EybmQsQxKEgKRxnkdzTkks9ctGW/8UXEAWTiK4VAxIHUYPQ5p02rTRzy2lr4pmZlj3LNKEWDtwWGTn8KlY7rb8UX/AGc9lLX0Yaf8O9SsBeALpcq3UAhkBLxk5JLEkKc57DjBpH+GEr2+I7lI5nBEga4eRBkckKVG4k+uMCt7xDqOpL4ft1tmgxcRL5l6LhY1VsD7ueSGGeRWVa3Fzpmsw/2Vqi6ussJR4JL0ZL+2T/LJ61c8Zyys0ZwwLlHmv/S/L5mWfhrrxnMn2uwP7ojeZpCxJyQDlcbSeT3pj/DXxE0h3XunzRMu1kkeQFgOhDBOCOvFaWta9qF7o2sq7+V9kvVijMO5Dt3EckHnpVzxXfKl5aW8moXkUc0IE6Q5AUc/N90lieQcGk8cknYccuk2k3v/AMB/qUbX4dXkOnG1kSyZWufMIFw+VUIEwrBM7iBkkiqUvwkuVjtWtNQWGeEkBHdiiI7EuB8o3EA8Ejk9cVPaao1rbahDZatetHbwn7OJOjZ5Zh8uVx2yas2culXMNjNqPii7eRCsjwPIdocYOCducZ4qFjeZ2t+JcstcdW/wZUh+GmtLbpHJqFqowBiOV9q5bL4BUZyDnBxz7U+T4YXREojntowfMBZ5HkLBs/MQVADHPTpWv4wmujrWm2tvdzW63GEJikI6sBnqOmank8KX8Eby/wDCQXuVG4DLc4Ge7U5YqTk4xjexCwcOSMpTtcwZfhbcyaSLQXVuG87OGLMFjByE3FcnHrjnvRbfDC8tbmCVLyz+Qq5yGJVgc5HHzAdgatP4gv73wTcM9w63EM6x+bGxUkcemPpS69d366D4eNvdTrNNHyVkILkhcZOeevepePio3S6XLWWTclFvrb8LlZfhbLHdborqzWFVUIGQsdwYHccjB4496sD4bSxSb4by1UhCAFhKjeQRv4zjg9Pwz3rofDPiD+042s7xfL1GHh0YY3DpuFULG/uI/GGtb5pJIYYWdYmYlRjHQHpWrxcXGMl1Mlg53lF/ZMuT4YtNeNcm7tkd4jHIRAGWbJyC6EAYH90H8avW/gW4t7dIUuoSqKFJYklsDAJO3iq2k6dqPii3mv5tZngYSFBHHnaPphhjrViTR9a0OW3uLDUZ7/LYkiY/LgeuW79KzWNnbnUNP66GrwUE+R1FzfP8yZvB162QJ7Ug5+8zdT1421E3gzUmz+/sjnnln/kFxXbDJAOOvavO/EGqalqWr3X9lzypb6emXMbEbiDyTjr/APWrevivYxT3OfD4R15OO1i5/wAILfMDvlsyc5wGbDH1OV4/Cj/hBb7ON1mVyR99u/fG3rXU6Hqi6tpMN2CN7DEij+Fh1rgrXxFqema5e3LPNcWKXDRyoz7goLEDGTx0+naoq41QUX0ZdLASqOcesTU/4QnVEXPm2p+U8LI47Y4woFc9dsdMvns7l0iuF52M+Ce/HOSD6nrXfa9qYm8IzXtlOwDqhSSNsEAsM8jmp9Os7PVPDFm2owR3O6EFmlXcT9Sea1jik6nIu1zCWFcaftH3seaJP5pIhbeyjcQr5Cg8DPIAGTgZ6ninedDCOWedyAdquVQ+4bliR3GMHpmukvfBunzwTNokslg7AkRXEbeSrAEj5SAVA65GeK5u40HUrR3S93FwfneOE7ORwwONpBHbOfUCuqM0zmcOoafrU1jeRNGFW38zLpCu1GBOCxGSCQCTnOc9q63RfEttrjvHHbXEBXOPN2fNj+EEE8gclTyB2rzjWLXy7BpkeFl8yMOfmByDgdBjJzz2Irqvh3un1PVfIZHngQKWx8q8ghPxAIJ96py5dSXC+x2/pjlSMcUoUVM0bbBIEZVc5IYfcPce4PrTMANzx/KtYTTRhKLi9RNtLjilwc9PwpdtO4muqDHFHfk8dSSeOOaeBnr3NRzTw20LTTypHEgyzyOEVcepJAH4mpbHFN7DJ5oraF5p5VjijXe7sRhR7k9M9AOpNVrHWdP1K4eGzuRLKqeZswc7SQMkYztzwOP5Vx/jXWtJ1KO2sk1GOWEs0lxFG5yRtIGCOhyR17c46VzNnfaegkOoamlyxG0MnmINhIIU5UE4IBwOBjPesJVLM7qWClPVntGxum0jvkA/yPP6VmSa7p8Fw8MspDIwUkIxXcOSAVB6DH1zXmyaroKKqiecgnICTuR+oqNtcsizsblNzL8pXdle3OR1FT7W5q8umj0T/hJbLe6hZiqnO9FUgqemckFST6jinf8ACTWXBMNyD0I2r36AYJ5Neaz6tpU67XncHYy4UleuCc8dTj8qa15orvuae4+bAyspHIBGTx1wT+lPnQvqNQ9LPia0CuPJuiQM4VFyAOeecZ7HNWI/EOmTOirc8uQB+7cBSexIXA5wOTXl5vdGllWZppd6tuxvO0sBjkd8jrVldU05/la5+U9iuAAc5/PpS9ohrA1Nj1nzUMvlF080HBTIJzjPGOcYp/lvtJCnd6hSR/LmvKHvdCub5b6SScXSxqjOsjAMQCMjGeoIH4e1WmutJubd0Mr4YEDdK/UjAPAo9qhvLqm53Z17Rg8iHVbIeWcN/pCDB6YJJ4Iwcg8ir8E0N1AlxbzRywucrLEwdWHQYI4x+NeSaB4dsy6JfXiXEKzea8aFjFOjAqFYFRggjJIJ+7zXofg23Np4YsrRgfMiV1wMcAO5UDHUEEcjNCncwqYadNXZuKOOOfen4o6nOc+9KAa0OawgHNAU4Pr2p+33pvGAx4Uck88flRexSWonI749c/8A16UdyDgY6g5PHoB1yMVgeJtSeDTpre1d47k/fMbANGOpwScAkcjPbrjIrgtF8UeINPjmWbUXvJZH3q067/LUZIUDjHykZPr7UnrsUonr2Dk8H1Axx6cmjHOOD6mvIr7xjq008Ooxv5aRx4+z7DtbJYlmA4HQAEnpzXQ+F/iBYa/sg8/7PfNx9neQEMePuN0b2BwT6Vm52djohhrq7O8bpx6dugI9zwKOQS2CR7LyPTg9utcZN4u+13d9pWnrczahbrINrYUKAB85JI+6TgD72ecYrmPBcuqaPrPlao08K3QZ33KXaaQhRuJBJG3G7JAGXNJzYlQd9D1pcHjv2IP3uOo9v65p5B7nB7Cs1r+3U4N7HkdRvX354Pemf2laAZF0mRzncDj647VPtS1hZPU1cD73UdMUcjj9TWFZ64sgkN0FgAdgjeYGDqCQDkE44wefWrLaxZmMqLiMnBxlgPpmj2gfVZGvtPXB/AA1FuCMNzKMnC7jwT6D39hWRYatCbVTcy26TfxCN8j9akuNWhktZPsl1a/aNvyGR12gjpuwc4o5w+rSRrBTtHOBjgnuO3WoLy7trGAz3dzFbwjhpJXCKCemSSAM1mR6te4CkadJhAPlnOS4POARwoGO/Fcl4v0XWfFcNqh1bTIPKZz9mEhKcgAHcQCfy4p85m6Er6nodtcR3cYlhdZI2+60ZBBHqD3qdc55/wA55rjvCOnal4chWyubqK6hlwWkWUExMFChRuwSoVQemck8V2AYMSARnHBzwc9KfMKVNofgNxXIfEVQfDCnH/LzGT+ddNe39rp1sbm7uI7eEEKZJDgZPA/OuL8ca5Y6hoX2WxL3jefGQ0C/uz8xHEhwh5GMA5qoP3kZyjppuea3EotrSaYo8iou4JH94+gBPr1qDTZJ9W06HUXuVs7eQtxGFyMEg5diNpPXAyMd88U+/junsLguqRosbPjaWZgBnBzgA9hgmqllp1q/hqBpVEN3NEwihYnajA5ICHlT36c5z0Nds6qi9yI0pS6F2GSyhV1tfMbnD43SM7YBH3QSTg49yKjuo4rpiAQu0B1eRWAQgkemTkDHSpJbqFL+5lihcRyygIVAVlQIpAGSBnngZpjXEc19K0CyHEICB1IcneTkBckkg9FyfauerWTjobUqLvZkcNlApRnMjZ427thB6HGCSRn1AqWRoYIiF8mHIZMptTdgkDJJHPvUcZMMihWEm4gb22qBk91J38jnIFPEr7UyoGTgKVyrZJG0EgHceowDXD7Rs7/ZJIfHIJfM2AbVJ3oOSoPIyTzyDnPbvUVrKNrsuJIwZFLDJAUMQCcZAOKkwrLyxwOFdm3FSORzwMj34xUMd15zBtyszgFSzAlTjkYXIHrzQJxsQajI0YiR0Id2zGxZSrYGDwTnJ9hj1xVmONhY7ma4GYtxLTIWQEZyFJzz2Jqvq+5RZsFOPNAOeBnPvyD7Vctt0mkpH5T8xCMFkJLcfxHH5VnUl7oRXvkE7ETyIY8NIgG3cuVwc8AE4I6c9KSKZXlRwfMHzAeWSWUAZIZgDwfQim3Uy/aYmJwpXO4qSHz05xyc8ZFRLGSvAZ35A86XcCByMGPJGffFOMnYT3NCPd5ahlIbuFA4J6gA8/niiolLsAJDnJwxVthXHAGecgHucfnRVAdn4MutV/tK4u0htrm6SzcFEhWMSuZEyQwJAwMk5IyTxnBxo6nBqYj1C/n0D7M00LLNKt2CoXGCdo4zxVP4c3THxFJA8FwksViwkknnSR5GDJyVByhxjgjHvXod5cafPp3+kzQm0uFIyzfK6kEnBzyMZNLE0ufqLCV/ZfZv95wmkW+oz2VhcpoK3a224wSNchV++c/L0yD6+ldXLPb3GlRz+JbW1tG3sipMyOFJzghvUgZxV2wuNNs7eGzs5YkjLGONAercsQM9T1NN1GfR51EN+1tKFc4WVQwDDIPXv1FYwoqELX/yNquIdSp8Nvvuee2N1p+jXE1rJaWmsxs2+KWJdzAeh4PT/GrGhnRbvVptR1F9OtbcptSywuAfUg/0613lqukWtu8tmlrBFuCs0ahRuOBg8dSSKqyW/hy+mffFYzSjO4si5O3huvXHeslhXG2qN3jIyv7rv3/rQyPGq26WWkDbGLVbpBgAbAmP5bf0rNkgsrrxPaGzn0aC0iuFZPJcLJJ04IxgnPpXaxjTtWt/JCwXEC4+RlBUDnGARj8qqS2vh2wuo0ktrKGYEMuYkBU9jnHHPrWlSg5S5rq2hjTxPLDks7q/4nHeJLGDStN1eA38M1zc3Sz+SuAyAknkZ54NaPiyeVWhto7q6VnsnkeKMjaAqk5JIzz04NdRNZ6NPqLtNb2j3iqsjFkUuB2Y5HT5cfhUUt5oN0JJZJbOQmPy3c4JMZzgE/3Tz7UnQ0lra5UcVflbi3b/AIH+RwkVxKvh+5gnvLhv9BWVI5Cvl4Zh04zkHjk81pJBGdT8Hny48PbsWG0cnYMV1sNpolzCkUUNrIqptUbAfkB9+2f1qVF026ayu0W1lxlbaVQpIyCcIfoD09KUcM117fmVLGb2j3/FWOR8bRvN4h0iKOTy3baqyAfdJYAHrWg/hrXGQq/iOUr0I8s/T+9XSz2FrdTR3E9ukksRBjdlztPUYyKklIztHTr61osJFzlKXUxeOlGnGEFt5I4nX9Hg0PwVLbQtuJlVnduNxzjt0+lU9XYf2R4V5/hX/wBkrubi2gu4TFcxJLGcHa6gg45HBpj6bZSJCj2sLJB/qlZRhMY6ccdB0qJ4O7fLorL8y6eP5UufV3b+9WMXxNoUk0iatpZ8vUIcEhf+WgH9cfmOKwPDd3LquuarN5YWWa0cFB6/KO/qa9CqvDY2lvcPcQ20SSyZ3uqgM2eTnA55qqmDvUU4vTqTTx1qThJXfQ4zwrrWl6fo1zY6jceRI8rZQo2QMAHoODWRq9roubaPQ7ma5nkfaUbPfp1Ar0O40bTLuUzXFjbySnqzRjJ+pxzTrXSNOsZfMtrKCKTpuVADg+nesng6koqDasvvN44+lGbqRTu+mlit4l1b+x9Cd1bFxIBHHjqGxyfwHNcS/iaHwFa21lc6Y1zcX8P2iVvNCYBJAXlT2H516Jc2NrdlDc20UxjJKGRA20nGcZqG90XTNTlSW+062uZEG1WliDFR1wMg8Zrq9jerzy26Hn1K8vYezp6Nu7ZzGhynw/4nn0p2ItLvDwFs8EjI/TI+oFSeE4IrnVPEMM8YkieUqysOGyz11M1haXEkUs1tFI8X+rZkGVxyMEjjmlt7O2tZJHggjieU5kZFALHk5OOvU1lHCuMlror/AInVLGqUHp7zS+9dTz/XLW88NQ3VgpaTS7vmMnPykEHH14/rXXaHqtjFothZS3KpcPaiRVYY+XnJzjoMH8q1ri3hu4jFcRJLGTkrIoIOOnWuD8W3uk6RrHlPqC2U7adsSH7KXDRhnJCsM7c8g8dOnWqoYT2dVyT0M8RjPa0VBrW+/c7RJkuFDxuGRgH3bgwOe4YcHnnIyKWQLMhicFgectjII5yPp296w/CNzHN4djKkBfNkClSPmAIxjPHNbPAB4wowDkdM8d+4PUV2LR2RxtJo5DXEsL+2ubCa1AuguyKZCAEcEFQckALuAJFM+F3lvears2txGCVPUnk5zg5yKj8QxAatLgDLfPgY53dD17gVa+FLbxq+SThouR7hjySAc1pJ+6Zx3PRmiDKcjr1B74rGvmt9PiaW5njgtdwBkmdUVcnuWIrd7VUvdPt9QtZLe7hSeCRdrxyDKsvoazjLlKnBSRz39vaMGx/a2ng5xhrpP/iqUeINFOCNX0/B7/ao/c+tcfq/gXSdLuCh0m0eJgfJkaM+uSCc8EY49az28N6IWbGk2Y4LL8nUdOOa7IrmV0zka5dGeg/8JBowx/xNtPA9ftUfXk+tcR8U9ctZvCi2tle21ws9wElSKVX3KAWAbByBlQecZxUP/CN6Jn/kE2XUkgx+oxzz74rk/G9tY6Zb2cFnBbQGRmkkEZALbcAbuc8BiATilVi4o3wnK6qvsclvla9SZ2Z3MqOWY8sR3Prxxj0pnzszPuPLMeT6nnHpzxUlur3MyRW4DyM4VAzBTkkAD5iOpwM9KtXNjNbXLwvBKrKSoUoSQQeQMDB5zyMivPal1PpIzpLRFLMhGCxIA459aPnxhmbkdM+vWrQtZcgeVICexUg8fUU9tPnEe/yJiucbhGxXjr82McUlFlurT7lPMmSA7AdOKXdID98+vP4j+tXf7PuSc/Zp+e4QkfypP7NuScfZpvxQ/wCFVZk+2p9yn5jjHzEcYHNSefLtwXJBIyPpzVv+zLrbn7NMPbYf8KP7Mu8qv2ebcxwB5THn6AE0uUFWp9ysLyUEEMc9MdvXipo75wepAx07GpU0e4ZsOGjb+61vIf1CkZ9uvtStpEo6MxI6kW0uAffK0KIPEQ7mho+rzpdO6u21MAEdTt5/A8/lXrHhnWikU6TSIIxelQZDhVRo1ckk8YBB5968d0vSr6SJ3t7cSJ5jIW81FwQBkFWYH07d69A0mw1FUvN9mzWrqhB3Aq2EYEEAkjnbmqirM5a84Tjoejf8JFomf+Qzp/rg3cef0al/4STQsD/idad9Ptcf+NecDw7pOxWOlW2/aCSYzxnBI5weuaX/AIR7R+2mWuD0Hl8nke9d8aLaufPuok2j0X/hJNC761puMc/6Wn+NMuNctXtGfT7qC5fJQSQyLIkZABOSCQCAe/8AKvPY/DujyThBpVqCAX3eWMLhsbeT3B/StmOOCxtkitLZVhhAMcCKcckjGACcEnPPpUShY0hrqMvUURXMrvzIMhnboAuQoP1yeeufauL+WWe1BUOEcHHO1iAcdPqa6/VLlNM0meW4YvtRiVQAliOBjsAC3U8D8a8vj1uG9ltYESYyLLGVDAYJLAHOD9ayc4rQ7IYeclzdC7qF1f28TeTdSqpm6qcBeSMjjgjpzV6300NFHNfM13dckNOcqmcHjAGGHr2ra8RwwnVdNgdR5RnkYDbwMKMEAck5J45qSKxuplGyFsY5aRgv1OCQenbGfaiDi9WbVFyxsjndZiuprKaYu7FVHzsSDzwvPGeAeO/4VzC6hdyQRxefJtR2dWLHd8wUHnOccZH/ANevQ9fsLu28M3LGaHyUQOwjhLgqSAMk4IPIxgV588Ijd0KkYYAg9Rj/AB61FSWuh1YelFxvIf8A2jetEi/aZfkygw5+bPIJ9SSSM+gFO/tK9wP38hxzyeQR3HvRDA0jbERnPUbVJ5GSM4HpSiHgYUnjOR06+v1zWTR0LkQo1K9JGZ3JJz1OPqR60HUbw/8ALw/5mosIWGHQe24ZGPbNPVEKgB0ywz98ZwfbOaLDbh3HHUbs5/fyDIAI3HBxT/7UveR9obBGMHmmfZ2ZRjHucil+xynkRMc+lUkQ5QJ11y/ByZmPfJyT+f8AnNTL4gvYwBuYr/dycYJyRj3NVPsFxjBhf6AZx+VC6bds2PssxwOyEnnpgAZI96LdCXKFjbtfFl3E6kyuPmyTnjjufU4xXSeH/F13PazWz3DIVmcq5PCliZMn14bGK4U6RqCLl7J1GOSXX8jzwfrVrR7e6gvXiSCR5SQ4iQbyxAGQNuQenOM470pLsZSlB7noXjzXn1Twc8Us2n27P5Uot2uAZmGeRtOMAHvXCz6lDcr5EGl6cluSGAFoEZ+w4VyAT1wOO2a0PFVjdSacZ2tZ1KhleSWBgDuxJuOVGAGdlB6fLXPx8yKWckb8jccbu464P44pSnyrQwpYeMpXLNuIy8ixRRLltpSPG0Y4bOCeR1x1HcVLIwV9yyJucbztYl328A4UElQBjkDnrSWsim1DuNz+Y64IAcKpIAGOenBIB5qK6Vv7ZtkKlpfKbeqrkjnjOOnrXM6rb1O6NKKV0iO4bF7IqqdrcjaAQMKMZBIGafAFkvBnlQXznkrxkAkcAjPFR3EijVSHYluhYRkkkgEHgHr1qOMqZ7fD+YolAIZGztbgjkAkEdQMmtFI45R94155l3RKECMIwSgUK7f7QBPJ9+vtVQxszSFGYMWwP3fltGeoIZyASB0A70u5I9sSMjwglF+YbVbOFAUE4LEd+nfFOuAY4zgsr4GxmxkEdxkgcUos0khwk85QIipiYHBXJwuMjKkDIHTjmjEm4tkFt2W2/KGyOcqAR+ZqBGy4eTaVfB3KxcKRwANozggAk+tPmkfe2d6gtgDep5A5HXP6UbmMitqEKJBCwVAxnXJQdcnjOCc1csW3Wcasku3AACoVAwcEknkj6DFV7qKVrX0AYMH3MSSORkMoI49Kl0u0juYGULLu3FCEVc7CcHBLAjHTpTqfCSvjGX8ginjYcb1CANySC2B06EdOcUzzgq4KYIGQrEAkjngg8mnXkhnu9rruRW4DKCMjlScHqB2NJEwi4UtGh+RjHGVRTnDMGPHPtUxfuje+pJIolZY3TH7zJDxEhiDkdcD2Bz1opxz5S4Qr+9UBVzgZbHBOBk0VSYtDoPhW0Ft4sd3a5DT2jxI0iARklwwAOcnIXuBj8RXoNzpV29rf24hZo4IpFsypGW8w7iMHgbcBRntXkElsskAeW6jmuYpBKh+0N5chHGWCjAIAGBnB609NR163O8+ItRdVRgI1vvLVgxPQhyAR2OOPauipSUzmpVnDY9imsbnUxbJMl6qxzFjJOIw0Z2MFZdnHDYxkdfaltbG/MEX2iACYah5rhD8pXpuHt3x1ryObWNZXTJlfXdajfzQIo2vdxZQBkM6sSuDyCDk96jOsa7NfNjVNRLMwKBdUkUFRwQYwcAgjBbPWpWHW9yniXa1j2Ga1vCLqyW1crNdpMs+5diruRjnJzu+UjGOeO3SBdKvltYwRLJue5Hkts2xhvMIYYGcnIXknh68ki13xLbQTwzatdxsY5CDPqD7gTwAMNkEe4HPcUyHXvEkFq8Y168EkqCMo16ZSSGIYqxYFWK4Ix06ZoeGvqCxLS2PcdAN0tqIrlbtSioALhYwBjj5dnJ/Go5Rd2moXrR6e92t1s2lWQKMAKQ245wME8Z6nvXlWj6lrclrcXDa7fNbx3iYeS7yyxHkbhuIzzyoJJPAHaixuNZulMzeINRltY4ZULRXUgILMcEtuwSOgbP4ZqvY+7uT7fVu256P/AGdqn25tS2IWlkIaEL84jcBAM7sYACMR7HvVmz0iW30gGSWaW6Nn5KRtswmVGQNoA6gcknp1ryOK88UywCNtfvC0jqS0dxLjAYlwDk9OmSRxxVj7R4i8hEl1rURKtwshRZ5slSoBBOcnByAOmepqVh0hvEylpY9T1GyvpmVLdP8AW2iwGRuiZYbicEdBnp3qCC2v9NeJXtzPDFdGZRbKAAHSQFQGbsxyef4689a516Tw9ZQxa7qP2sQsrFLvBMgJBLs7A4J4B4x0OKxZZPFcMqB9W1s5V2by7x3VMEhF3IzAsx4ye/Sl7FXvcpV3y2aPf4pjLa+aYniyOFcDcPrgkfkaFyTkjIxz9K+Ydd8U+ILO6lRNf1ZY43aNCL2QjAYgDIYEkfTJ71r+F/E3iKKxupZ7zW7vescaH7QzAAgEMCzABj6dcVrGDbsYSklqfRHlk8hgQeho8s+or57j8TeIrKWR7rUr9Irlm2I165MbgZKklgAD1ABIHQZrRtPFGsXMCytqGooSCAPPfayg4yhz82OmTzntWvsfMz9oe6eU3qP1pPLPqK8TXxBrHA/tS9PT/l4f157+lN/t/WDgnVr7pk/6Q/8AjT+rvuT7dHt3lt7UmxvQV4ifEOsZ/wCQrfc9D9ofH86X/hINYOVOrXwOOv2h+PfrT+rvuNV0e27G9P1pNjf3f1rxE+ItYJJ/tW/Hsbh+3B70n/CQ6wQP+JtffQXD/wCNVHCyfUl4hI9w2t/d/Wk2t/dNeIf8JFq4OW1a++huX/oTWhNreoLo9rcLq16ZnnYNm7cggKSOhyMYqJ4dxdrjjiEz1/DD+E/lXjXxRUTeL1uSp8q00pnc79u8rIQFDAHB3MoJ+veo49d1eWLJ1S9C52k/aHYevUnHTtRqkkcuoQW8yKUW1hEiOAQ4a7iJ3DPIIHIodJwV2ONRTdinpd34n0XSLTUZLryLBtkZiS3fNqhOAxXG0rz8ylgwHOM8V6EdY1PTIt+p2sLxooczWrFlRMZDFWAJAHJwDj6c15b4svb6HSfLeaFkacYIjKn5fmG4A4IGBgd+9J4N1rUr2OfS5bkvaSwugg6rhvlJLEkgkE8/jWFupte2h6H4icSXqSjIDRplnPy/KOeRx0PTNXfhYRu13DAgXCAHIPG0+lZGpSrJp1jMXA8yEsWOcnBAyRjHH61o/DbUrYXGvRySbJBfY2tgA4XGAehx7U5PQFFyeiPTCfypfSoxIsigofpT8jI5qN0PXZlW7s4b6CSCdN0bgAg+v9CK4HVNNm026MUnKMSYpMcMCDkH0bHbv1r0jOO9Y2u3dpHbLa3NvJcyXGVit4xl5GHP/ARx1JAFaU5uBnOmpanDED5+AO+QTkY4yeORzivKPHlws/i25MgIECpAxGCTtHHHTHzV6Tq95daOG83Rb7PSNWaIlmPAUhHZufYHjmvK9bI17Vpbm3iaBmQsyTyAOzLwSckAbh0UE429c8VvWqpoypU2mZYFgSGeW6VupCxIxGeByzDB9hXQQ+KdSkRQdShd14L3MJaQkDHJUEk9OMZxT7bwZYNLGk2vqEaMyO6+WduCoA5YZ4J64PHTvWK2nxHyz9rdy6yfKFOV2gkliB3AA4556VgnzdDp13ua0virWrYRNHdoYpQfLeNQFfHDAZGQQSMggEZHHNNTxnq6ylxeMHwqb1wDhc4BGMfLuPbnJzVFdPLo6Ga4KrKsZVkXaxKswIwTyQuO3vVRbeKSWJALs7ghyyDkM4XtngZyPXvikn5Byt9ToF8a6ueTqEq9eMjt1xxzUq+MNWYYGqygnIwGGeOcdOuO1cw1uix+aYJ8HzE2/wC4wA57AlufTHGanh01JzIBFMuHVeU5wVZiTzxgr16Ed+1NvyHyvudFH4s1ZlB/tmfrjO9eM/hSv4p1KSIpLqs8kbfeQuPmA5AOBnrg/hWDJosqwLcPazeSbdLjdjJUO+wLzjJzgg9DVRbVGiaU21wdnmE8YDBCMEkc5OefTtnJpp32QrPudg3ijU0TMmpzCPeOSwAJHAxkdh2GanHim+YZlvppUYcNGuSc+zBea5STTbSE4Es7S9HTYSyEruAbOAOpxgnjk4OAWR2CusBX7SnnpGV9FDvt5JOcZ6YGfpSu+w+XTc7GHVIzLuLsWPO6RVQnPGDgn3/zivSvC2tpd6ZPablVzCxCq+TgA428cDivA3tLi3jDrPexZaVVCjujBT2PXcMVfsbjxDYTQvZ67dxb5BHjfsb5mVMYwehZc+3vxTs5dCWmlueuttLAgDbkDGeDwDx+OaRVZuDwAMknjbg+/fFYekahdXSyW8WnukVrmMzsQFlYMwcqpOdoZfvHBJY8DFaq6gkDslyY4lJVciWM+XnAG8Bi4JY8Hb3AOOCepVeWNjl9m+YvKflREX5eoHfPPJPrjPFRXtwLVCqk+eRlQOoXsWPYEZAPfJxnBwXc6WrSQqh87OMNghCcD5iD1JPT0POKpxp5brM7F2yWOAAOOCPmIBK4PGcYI5yOeOpUb2PXoYZWvIyNWkkuLFnl+aORGQFSSNoUnaDxwOB9a8y0cA6zp4I/5bLjHcnHH8q9T1jJkuSXDKPNbKnAIaNSOO3AHsSeOhryvSAV1azYrkBgQp7gAEfjXE207s9my9nZHqupzH/hLtEAw582fIJHDKF4z0HJxmtvy9QlXc08dunUsiGRlyOMlgAAMHn1rgdV1q6vdf0+e0QJLGW8kE/MpYoDk4AwOcdePeu1v47SxlM2taoE8kAiSWcRswJJJIIyckYXAOQvY5raEm9jzMRS5LcxxvjbcyRraXzXC7WecG5EoyMegAA5zjtxXn7s5lJaV2J5YsSCD0ySM8V3/ibxJ4TvNPktbNZPNkl8w3UERxuGeX3YJzk5AH8q4S4s7kSELD90csuWVtwyuMDOSvJ9ARmt4eZyVZNx9xk0MUUbo/8AaVsGznCiTtgjGF6g4NdMviGC5kWa50XStQuictKztAXY5BcqQAWOOv6Vx8sV1bIkkyNArLmNpkYFxxjkA9iPwxUauSpBubfA4OQxHHqcZqrp7GHNPueg/wDCaTW0QZNCsY0XgmORZgpPQEqDjPvTV+IlxG5A02yVccxtENi45JAIyCehPcAVwMd01vLk3UAyCCXjOGB7HGfyqD7Um3abqMADBym4cdeQckGhegNz7npY+JN2MgWmnLk8A2q8/TmpP+FmXw5Ftpo+toM/lmvMRdZbb9oAUj72wYP0Gcipilw0QZGlYZIOIj2AIOc9Dniq07Epy7npkfxQ1MDO3TgPRbReP1q0vxJ1GWVJGns1K5G0QKFYHGd3POMce9eSSmaCRVkeRGIzgRY9Rzk5PPFN8yQDlpo1bJ3mMBTwPU0l6CvLue8W3xEupPmdrPaeSWgVVOeQSxOASOOa0IfEtnd3dvdTLaJcQZET+QVK54OGXPpivnuOSQMAl823cCAoXggdeTxj15q6ur6lboXXU35GSWjHzAcDnOABU2sCu+p9IXOsWur2L20+50lUqVKrzjgggkA+uK8cvdLk0rVJbRndjgEyjG2TcAQWB4DYwSAcA57Vi2Pi7W4IXnNzFIke3cjRj94oOGIYNwwGCfWtuTULjWZPtNxDL5ihrdEREUkb2x1bDk9SeMdsjmuesjvwUnctW4ihDoqAqGZmEmWGSSWPIGBuyRjPFQxyMFAAYou3DMGIUkAg5Ax39aLfeSGeJ43bewUrhlVWK5+UkP0wQDx3wOaYoRJQWwpReXA+ZQOg5OM+1ccnyvU9iFpLQpllOtTEFSpUEHOACVBzuBI46Y60vW5tt5RcSqezEZ6cA80y93jUYHCAt5G8bsgEgkZOB3OeRmpIVbz4T5YTbKjKzZIIJIIJxzzn8K1vroedUXvF10k2nG+NypDbW4IPBypAwPfOarSPBC4QMsIfkAOAGx6lj37gZzVlrXIChjKduG8vJGckHIbBJOM8VFIoyWLeYuzdjaFAOTjPJB4x1pxHII5BJhyihujBcHa2eQMcHB4pjB0bbhoU5CyqiBVJGCwYtnI6jNNV0BOWDqFDBQcjaeASTgAn0GfWnlVhlIDeVvIbKttJCqAeuc8c9KpGT7BIQ1jK5T5miJwoLbSRnqKbp9xcwtJ5Ktxu3SLGTtBJJGQKF8toJMcyFJFzyTkgk5yBj2rPa6SMMhBwHEu0byQQcgAAbTkc8mqlrEybtK5YDSLOkhhYICAGZcKGC4+8xAJ9up7VItunJRIwzZVvncOo6cgrjms+xuXlvgoAKqsgCISRnGCvOeM9D1xWt9o6j5Q3Vdzdcc4AOM496lLSw276iqrSI/mOCqyb8lANpU5BJBI596KVmDK6neCxwQI2IbIz1AIH50UBYiMsUl2bnbDJdK42M8aQgHGNxYMQCBjgjBPHualpJbSxiWWxhaLLBHimILEAYLDB2qPXoae2kXemztdlrZjvLJC26RzjJDYwAcA8gZ547Vm2tldzW1xFGmZZBg7WAwWONpJIBP8As5zXbZnBc228uPTLSU20mzztrJ9pCq2AACXUMDk/l3xVWfUmW+uLZ5tm2bBLKGYlSQCSMEt/u5z6VNdWrQeFtJtG2oz3kmS0gUNsOSdgzxk53A8HjBrPaMvd3cjT4L3DsRI8Y2gEkEKGLkjPUDjpzQF0SwySrbLNFBatFI2+NZZMlyAB8wUMQQcjJwD61WNxFGu97UbN+DIqKwbJIAClhnBGd34Y709Gt10bKb0uEhdJHXKpMpckAAAnk5JJAz0HIojjtRo0z3N2Y2hhjxFhVEm5yCDk7l29xjp1INFx9DU0vz57C1nSW5iH2vyVWIqI9xUFi2TkvtxjAIHTIrLt7wXcklxLFZSSyZMjGJ2LEYJYhWAJLE8Yz3xVzSZImTT0judkb3srpbrGY4x8uC464IHAHJxj61lWdrHMNkM8jJsfzCyKrDCgkMA2QAeh4z+lPyEjSm1G5Ejfb7WBpvP2TQPEVDYAByVYgMMYB9O1M/tKKKJXXShEJH2blkkJ4GSgABJJ6g4AzxmqjeVLam4WJSXmEjuGPzEoCVbtwc4Hbpk1WvZo4rWLa29X3EqJM7gCcZzjgH3HNO2gK1zrbuV20nR0mXmRZpEt5UDtGQ5UKrEggEDHOcHk1mpdWsqNLDYzRouCkiyu5QZ2g7cBQc8gluD71Yv2K6Z4ZuWkCt5dw5EijczeYzcckck5xmsi4doY7NfPlEvKSEumWzMTtO0kEk9+gPTjmpQ2jF1tgSMEfMwHUYYDpyT0x37ivQdDheLQ4Y3ADhmjYoOPkYqpABIPHfNebaypeeKEBcu2V/l19c16po9uE0CwflQ8CvkqQGyASf61tRtzamVW9rIytQ0VL68SeR5HjRQFgXGzAOSSWIIBPJ4zn2q1FD5NrHHuXbHkYUEYzycZ9evueuK0nj+bOAR2AyQfrxTBbSPuZYiwB6gE/wAs1u4q9zJN2sUWU7iB6fr/APqoVRuXjoQW9MZq5NayJHuZWVdvLMNoyTnocH8qiWLLFiCMfN8vUg8Dj0zzTbXQmzKxA9sdh7dqMFc52kcbtx7d8fhUsvlQIrzOqAuEywPJJwOnqamUxxq2VyxHykgZwefXOPqKbQJMo7SBjGPWkCnmrJjJPIO7nvn60giO7HStIGc9yvtrOgjR7id0glLOQzHC/KSwPHzY/MDitpoyEJC5AUnHckdKi/s6WzkMV4z+adpCQsqlkAwMYJHUjHP+NYV5LmRpCOg6xkd2a3LfKWEhLg70YApjOAGUgnPcHj3pdaO7WfvBRHPYo27OFDDdzgHgEA4/Grtpb+ULeUTGVZFLKJHyAAQpzkAdTgYJ4rLvGUy30kqs6f23BAEbqyRqQVPfPasasnaxvQilMyPFtxO1pbxTgY3+YrkgscKQNxB5xnJBAI9Kn8BgnU42GGVOGO04YEgDGAeQTiqnjKUTfZgJdzKrMzKNpJbnHPJ+pAq54JdY512qUQITgHLFgCVznHGcHpWEXpqdVVJM7q/OfD+lA9Ta4OM4+Yk9wPSs3w1bySS6oFKh59QlAZjgYXHTHc9PxrU1ZDDpenxkY2Wyp7hsEmqHh8mOK7IjV2+2zghsgHJHHAJ/KuevL3dDtwMXKpodJZ61f6Y7IrlkBwYZSCqkcgAjOM+/PtXW6d4ptLvakpEMnQBjgEjrg/pziuBmRfLaZFEfO7AJIJbqOBkY6Z6DOe1WpEhGkQylCZZGWMBkwzEMCduSMEjOM9a5YVGd+Iw1OSvazPU1ZWUEH0NfP3xdvbqTx75C3skaRWyhfmO2MEEsRgggEcnuccV21jrF7YSOlrOZ4oslkbtg8gcnHXqM15T8RNQa+8Z3VwyqheNAR9AQR78f/qrrpz5zyq2FlS1exteCdH1jWb+5ezhmMK2ssSSTu2I2ZSNzE53MegA5GQTjFYy61NZX2q3ywm4JiFsGmUFiFBG7aAQWJzwcZAz2r1v4KfJ4CbfwDfTEHPbIx1rxy4jD6Tq92sjRpFfsiqpyCGLAEn1zx9Ca3ik3Y5dUjK3Sx2qpldoJJyPvAlWJPcjcqkAdMY701bqdXyD8yhsYHJDDDY9AQcEdfahZDNZ28hQoxyCCOCVIAb1y2evQ4pNrRsVCMCvGNuS2ea6oJWMmmWkvp8/6+VPnEm4kYLhSA5AycgEgHHQnjNNFw6uHJ/hCAq5ONrBh1A6Hke/Wq4Uhs7SG6kgEfzq9pc5ttQt7k9EkAIAHQ8NweM7Sce9U7WEk72IvtLkBTGSqszqrHozYL/8AfWFyDxkCnw3TwsuzfD82SykqVYAgE5HHBYc+temTeWJyyrGAFDZZBgAggbuMDIxz6ioNXYLY3Rtot7JC+0+WGZ2KHkAZ3EkgZ+lc6rLm5bGjpytc88e/aQ4wGIUIMN8uMggDnkA8jjrnGajaQbXVocjnlgQOeScA9OM/Xmu3h2MkbuFz5YZnVOVIUEnBXgDofQ1pJ9mdgGlUqyghQARxkkkAZAPtnNbKSj0MmpHmrXUzEs00pfAJZiCxOCATxy2OM+lPjvLqPymWaRBEVEW1uU2ksAuRglWJI7Ak9av+IrtbrUWRHV4rf7sgAUuTjJABwP4QMnsaygV243kAc8Af49PX3rVJPUWuxb+2XEwRXlJHzZySD8xBYEgdCQCcDrWgszHS74YOBEZQABuyGQkkgkkgKMDsMnrWVD/rwAxwCCCSOQeo/WtWyiWSO7QqCDFICAAMrgg5JIPUgcCiUVbQi7vY9K0m/aS0s7iQoqtskzsCjJCnHBIGQevcn1rxGSRJdeuWuHVY5L1i0r7sIokJJIUFs4GAAD+lek+D5JLzRNKFw7TRoPKZWHUowwfqAwH4CvK9SO3ULtQBuM8oyvQZcgEd685yu7HpToqEVI9y0dLQwwvppZ7VnJgaTJkZCSSWLDsS34YzjimtdKBCqkyskhUw7chiGY8EZJyCuNoIAzzWX4bWW90C1cOEi2ANIDnLDKkgHBPHGOB7mu2s7G2sk3wxKr7BmQ8sw6AZxjg54HTPWsbNs74VVGNjAl0u5dWnvXIdoXZlXBZtwYAFs4GAuBjJxwcV5qmn20ctu6QRCRWySAQHwMkHkngdvavX79lMWxfuiPZnjnO8noT39a8rYYliyOGlwcdSxBA/Wm4K2oo1JOaXQsQrA+q2mJpJXjkAcDamBIRjcAThgozwTx75rB8ReI7ybVpYIJY47W0u5WtxGgwpB2hwSSSSF469TxWzpMbW3iK63jDxzQh0b+IgMMfiSfyri9QxHqd6hwrCZwAqkgZY4FRQDHxukXzd3M2mTudMR0LZd4wCGYjJ3JnI5bO4Hg9q2vDuo2M1vc2mpXSxLDHuhlKknJTDKQB0OFBPqOOvEXhkEWO4QpMysWjiZSQSUA4Ucnv6jisPyRHb3SjBZUUdCCdzMTnvjFbQld2OWvQUKSaOv8QxQXGjaRLHfXMy7CrCfd5cZKqAi5UDJII4J4AHaudS3idsgRE8cBgDxwQcjqK63Q9UgvIoLQq4uI4t22RQQ2DjggnBAI6gZ5raEK9diZ6g4Hcn25x611RVjz2meeLbxbW2pH9Nwz+opWUFfkRB/dVcZP6V6M8jqx3MoXjgIuP5VNHjyuY03ZyoAX/Cr50LlbPM4/L3Y2oFbgkAEgnr2/D2rp7ayvm0b7fDewRRxowwGIcbGwEOVKgEEYycmszxQuzXLpxx5jrlcgDhFyePfn8azo7hniMTMcvgMQTg8Yzjp2q1ZkSTRNNctcsxnd5SPk3OwJCgkjoPekGzZ8ir8nPAwMfiMdKYEbaxKnletOXj5WGBjkevFapLsZtssMqqpwF+Rhkqg3HOCBz7nnGaydUsjcytE0qYhySVXO5yoJI6cD7o46DPetG7uGtrF58ElApIA4ZiuF5HIOcnOMetZcLM9ip3fMRnd1UkcbiBk5wOa5MTK2iOnDRvuRaXarZX+yZzsnQxyggdGHI4PpjBHWtYmaLwbNdyMvmrELZ1AIZcHZg9s4UZIOfxrJ05g1uJMLIYyu0SOCo3MR0B56Z5IxXSajb7/DuuWts22JY471TIWBxtG8DjBJfd1x+dcMlc76T5W7HKaHMW1yxZ53P74BlJY7geuPfsR/OvQWuoROcBmOAAcc88nBIHBBHAB9M15roeJdd06HON0y/MegznpXfXcUz6sLK3eNU8tZd+wbywyAQSeAvB/oa58RFaM7cBN2dzMkkz4hnJJPJZS3BOVAGeSAD1wO9OKRpdWrJncJMuxz8ygAgFev5A0XtqbS+gmErSs43O8mXPHAwAAeg6DNNeVeH3gmMllYqcrkkZBIyWI4+nNLaxnP4mWppwXyLjcWbPlyIwIySQQSAeOwp0s4KhSXV9mfnYZAJ4IIz1PHTjvUJu4jKUG5NoxhyCRjpnJ69qlaTPltDtI6kGQrvz6gKSB39qtaCYgXyyXBUshJV1LAsSACGLKMgdsDFLJhmBIICkqdyLtVc5LElsj0yATjsabEoSPawk3AbMlCSwBwCTggkgZI//AF1FIqySks0bq68o0fLr0ALEgDPSmyHuTyxoLG5ZCY2SNjmOTgEjkHIz/ntWfPJJ50gV5NuAdqsOTtz6YwelXlLPavCImM0iMnmJsIZsYOMsCAfcVl3S5csXWMBEZgwJwCAcgjg/nV290xkR2LY1iIuxKck4YkA4wCQBnPuP1rcExV/KWU/KSCvmIDx0JGTkjt2HeudsuNWgZZd/3iAFI3ZXA5IxXQ7GK7dpBPGzyFOAeuSWGanoPcn3EIcMBnGNxJHT2HH4ZoqMl1jY7vmVsDYwjVQD1IOR096KOYstX1xZ30bo017GPK8tmjsWb5QScg5AGSc8nOKg0u10zTGW5jub2dEO4lrZY1znnJLcDnrg/WjTrO/g8M38N4VjkKgq0knzM45AGDgDHcnj0rlTPqVxGxtWYTSSkIqMMbAcbQD/AAgd+c11xq3djzuVbneXE0EVpbR7UYzykq04CscMSQFI4HPBB59DWUzJBbnTXnS3RmZ7hZo41MxbnYwLFxuGMdOOeKqagupWo0gtDKFlQRzO0ZY7wTlQTwSOny5ANZP9oak8crjyPLBYIGVTIccAHcQcAcZPXtVjSReVdNjtlgNxcHeSXfbGXcA5C8PgAH06nnjNT79Kh8gO77SQYjKihJFTj5WJyRgbWPIJHODWV/al+Y7hw8AaAgHaikDkg+meew/WnX99fwQRTtemI7fnRtpOTyQoIwQOvGB680Cex0tpJaw2tgyXLb/OMqRKoVWVeMkHlcnoRnI6ZFVre5gt2leSK8mbYyYkhVmQlAhJwQGwBkkkfTvWbFu/4lFw8sTNKkhyWIdlUkDKkABRjAIJ+grNsb6fHmORIEUFozuDEEkHHbGOoPbriquI2fI08zbRa6gquzMsexR944CjazH8SOKfdWVi8TxXK3CYZ4iyvHtDsoJ+6WBx7cdsg1hW2p3ItUaOfMiysBIDhiAMgYGcEnk4yfwqKa+uxbREO5kZsKNoBXPRQDyQPTrSchpaneaounHSdHlvPtKN5c6P9mRJAA0jELgHCtg5OSMjpXMs2kC7D22pvsEKRn7XGsZJAAOQhbAPqOc9hVvV7mSPTNF+zs4i/s65mUyAqQfNba5464x1qmt9qXnxo7g2t0FMatsBZXUBssAcBjwc8ehxzUXGI1jbza1DfQ2m+3uZjFGW+ZYZiMhQRwxA+YAHIHB5q3Z6vr1jAbC1it5BbnyUVoQ7kg4IC7snA6kDA+lTrcXV/qdublopJbecXJZBsKYUfuQOEyOuc57VrXWjF7uSW2u57eQy72MZHzOBjcARxnvg0czWqKUbnIz6nd3MrH+ztNkb5iWSJzuA6nIYggdyOlURqUsbFTplgc91icf+zg/pXbx6HeSq23WY5SyujJJbrt2uSXAKknDEkkAdaz5fAGogFkurPYTlS7Nkj/vkmj2z6gqJy8eqXcEqvbK1vJkNmPdtIHXIYnIFa3/CY6hsAa3iz13LHwwPfBIIB9O1WJPB+qRtsBtnxz8jnGfxAqFvC2sDBFrvPqJEz9etCrD9iKfGl7IyhoI1XeDhVAwV5HXOSCM5qC58XXlwQY2uYT0ykxAOPohp3/CMayPlFiSRzgyIOP8AvqlHhXWyuRYPtP8A00T+jUOt5i9gVf8AhKtTCri4uD2wzZ6f8BBpy+LdV3KE8pm9JIv6kjFWh4Q108jTX5/20x/6FT18G68D/wAeDKfUyJ/8VVe3fQTw5YsfFVyG3X0Vqygg7kLIFAGecAggnAyDn2qXUPFn9othbRFO0EKJwflPBIBAwT1BJxiorfwhrkc8cyxQRvG6yKWkBAZSCDgZzz2xVy+8N67qESJdT6UqRyyS4iVlbc3LZIXkEducHpWbqczuylS90ms9duTAmNODoj5LJIrIuSMEiPcygDpxgnvUtzpF1Y+FNNur4s08mqm8uAmCqrtZiTg5AIHftxVrwzpDaFH9mZlklkuomkeNCFXBCgAnBOc5IIFcst4b2/12BHEl3czSIkauc5YFeBnA5OPT8KcpXFGPLqjU13QrbU7nSrTTdRhuLq8k+YQYdYIx952IJI2nHUfStHSfBw0nU0hXUZrl9hVkWNY0QkEE5DHcQpJUkqM471c8MWMmi6PHpFnBA+pSuxmmiBDMmQQA2CQqjOWIwRkckiuohsILGOK3839/O5BYrzIwUsQoySVGM4rOT6GqXM7sy/E10IbRpGwRHGXGMYbap4znI6dcc9qseB9Iu9Y8MS38Msccr3sxMRzsOSP4sZ6c8Csrxl5kVjKrnbIv8RwN2CAVIPTr+NX/AIb+LI9D8NxWNzZu8XnSOHjI3cnjIbHfvmhx5o2NI1ZUpXgbEmmXdv8AJc20q5O0tsLq3025JH1AqpP5yWK20kAWBVEauVKtwwJ5bGOAfxrurLxbo15hftAidv4JARj6kjH61rBLe5iDARyow4YAEMP5Vj7BLU6v7Rk/jieYQ3C7rmU8HyNuFUbmxyTwSOcDA715f48uLT/hKXItgWigEblJjgscYIwpxwcEDOeuRivpC58P6bdNua2QHoCpwf0ry7xh8HbrU9YfU9K1BSJHDNBImCuB/CejHIGM4AGadODjqRiMTGqrJC+Db+6svCETWBEMISR3jYlwSpG85wPmOc8cDFeZtMf7FeHc5S4l8x1VDggliSSAcEHA+hr0GS3vPBmhJp9xY3yLcRSQRTSLHskk2MduFdiucZGByQM15pbfaCUt4ZY/mIji8xxHtzwQSTjCk55xWtKVnqROjzwXKJ5+WRjsG1AoUcKFwQMA9hn86n0/T21BiltLE0gAOHkXAyQORuzgZzkcVNNqT2gaE28OyL5MLOpRSOTg7cYPbk9aqPrQlyPskJjPOGYH3xkAE/StnWtsZ/Vopa7jGVI2cFwMfd3OARhgOQcZyfTt9algaHDBmXDHZ8vUE5I6Z6Yqst1MkrRWMQiL5JjBDg8Z5B5AAzz+FWF1KaNlS5td7ucgIAC2eBgAdvr/AFxftlYzWG1O80zxFaRadBLqMiNLsAZfLZ+mc5ABOCB1IxUF74ugikMttpt1n5TmaPy1dmztZeSSuFPbHHOK4/zm+05NhKE24IVwWGMk8ZHX9KSXc8YENtOgKbXcybiTkEEAn5RgEFR3wRUWhzXFKlUWiLcGq3TXyXLv5xR96oxCq2CSOhIAyT3Oe9X5PELQiTOmRcq2SbospDZOcKpOeuOgBx9K5gRanG2ViZwRn50Ge/cHA/GnrPK8gtninEzdESZcnAz0x2xnmuj2sNjB4arc0byGSS9nlYFWkYbkJGVBVSMkcEkClFu6KCw2oDndgdBjv7VRurlowjzQzLvyFDSgBsjk/Xgdf/1tkuktVUoArOM5EhyAfqME1arRRnKjUW6NSKFkZQ4DhR8wB555DLkfKeOvORnjuL1ioiuQpcES/I+B8pyQRj8cDHbrXMQ3lxM7JFeJvXBCSLu6EDkg569hWxYvqF+xFpbkyiUIBGjSLu65XA9jlWIPTrSeIjYpYao7aHR+CYpZ9Mmi8zylW8lX5Rls5APUgYGcAj3rjNZuLV9cvUeC1t4FmwJFiJK4YAkkkHnBOVByeMV2mgw3/hxpLO/sLlLi7m8yHbsHmNyWYbmA4A6An6dM0dR8FRapq0lz5rW0TvvZC26Rdw6BcBTk8/e4/GuGybujuqOXKos67R5LePTBDaeW1vC/lROilRIu0MHAOSuS54PpnvXRNcuY8spGIkGBnnGPb6Vy2k6fHo+mCyhlkkWNyzM+N7HABbGcAdBjPGM966FI2MG5yu3HI3EtztPpgfSk79DSFrajbxhHlVJ44wBjkhiT+ZNeaFjDOjEjiYnLdOASP1r0G/ZUBAZvvqDuGMk7gMYJrzjUv9VOMfLvIx2GGP6570pbGvNeSsX/AA3K2p6/qUl0VFxcxqY1UYDTDgAZ7Y3fka4rWo44tWuY4nMipPjfIhUMwyCOMnAORk9a3dNvjY6hFLbBo5oikolDFgzKWAIBA2jBII5z14rK1PTbq4vbu6BRvOcufL4BBJLHaTzgtjHX0zUU2kPFRlK1jpfCfiNdD0K5gFp59w7sS8hCqgYAAKRktwO+M1gQ4e4mUgbX2HBBwPnkBzn044qe0s7cWqxJdp9qfJCYdSxAUYw6qO2cgk5OMd6zWna3kCt1cFJAWKuGDtgkEEAjsK2XLzaHLKMpRszSjlfT57W5hYB4Jj8zKGDSDIOQDnbtP65rpNJ8T29zKYb8xwgt+7mQnZtPAUqASCMdT1LYOMc8kupW9wjfaXvDlwylZAoQgBW/h5J2jHtUUk+nu2GmugT94M4GPToOTiulSicsaDkeoxS2s8qxRSrJI/REJJO0ZboOgHJPb3qRLu1MCskrFD84co2GBAIIyAduD0OM15lFqaQ5NveP5oUBZHVtw4xgYGOnFVTrTbUjmjYoiYG2ViUBOOhAOCMVDkrlOnbQ6bxI0F7qrG1mjZQpDHePvjnAB5YkY6d8+lZotDCu92wCRxg8g56cf574rKjvbXaHS85/utbY28kdQSDgY6GkjvxHtZbpDtwd7WzYH0weTmuinONjnqQlc2vtC4B2MegBwe31xSFTJIzBSMcgn39jgdPesl9YCrlV85gSCSrIuSAeDg8+xpV8Rx7guGT1PlhsgcZwWHHv+lae1itDP2UmbMtuJbGdJG2I6bWcHAUkYBweCeMAA1zsQlSB7SViAVKgDBKMDk45BwTnPcHpmnzaxLNKrGdCNwCqsRHXqcE5/nRA00yuUiR+gLyQlChHIwSc8fQ1yVmpM6KScFZlu3mEs8ZIRY5GwyrzgjAA5A5HXnrmt2+dNPlhLbri1n0uaJ0YBd21mZQcEkABhzgn2rm2lhtIBNLCxH8JS5TGSMgEAkgnt6d6t3mvWuoT6eGgmiZCyTxfLuVSMEAkjcx9wBXO0rGsXqZXh82o12wd0f8A1gyyudygdMcfeFegPZxSXCXck7eb5QQxghVTHJBbOT6DArhdLj0211mG7e6uJIIcyYkREDMOAMhzxxyRnHYHrWtceLLaNY1jnWQAD5Cx25AAJOFP4c/WuatBy2PQwlSMNy/qcgfV4YSoEcKBlznJDcHJ4yCec9ulVp90ZYFYwFXPyp2IGRuzzkYGcVnnVYb6efUSyqIgAwRixAHJbBAyecduak07VLS/1OG0i3O0jFcYJ4PJwAPSkqTInWje6NBpmSUqXLEMRnemOeSSM54PHSrUeJgWZQA2TlSQApJ55AP5flXPTa9a2sjQTwOJUO1wZjjI4PITnAGMjihvFVrtbyolLNzj5mAzz6DpWvsjN1jaimIZWCgA4DHa5LZGExkAjnrx0qeZijIX5+YgkADOBnPOP/1Vy0niolspDb7/AFeNhjjHGTTB4uvdvyfYwoGP9USfTufTimqZLqnWR3CbtuFDAnlcBvTmqN7BPD5bMGG9MkqqkYAAGMkY9awV8T6vOwhjniVn+ULGgyxJxznt75rovEFxfpplncWEyoyDbOI5Udvm5HAJJHbOOO9aKCsZOpcyrFJDrEQfflcuQeigckDGQSOgroY5shhvUyqOQmGJz14BJHrXM+H77ULzxFbG8mGyJg8hkdY1IHH8WMjPYZpfEE+sW+pultPNJa8NE0BWRcEeqg8fXBqeTQOex1CxvJGV2Tbs8kQOQc9M5UCiuAZtclxmW5/7+EA/XBFFL2Y/aM9aRpwAluy+QBmQON67SOVIAzkj/DIrnLPQ5bWRLsPMwjuMrG6IqrHn5SPm3dOACBzmtrRrkmB4ZlZDEOC4MbqMZGFPqCDk5GOmaluLoMhhtLW3kkU75Zdg+UjjGGYZ5zxnPriuVVGpEON0QaxI0kEMPmyOEeQiMhh5ZJJwARncc5/HvXFQztHazbAM3EckYLKd6gKDtBIyCR14P4V0GqXKXEsjpctIwRSykoQuFGCGLBjz0Cq3HGT1qPSNGvPEFwyi1t/L3MxeWXILNydgTLEknJJGAeOa9JSVtTOEHJ6GXoyl9RsjJKJP9M5CjGVGQuBjBBABA/E1r2s32S1hj8mMrcadPLK7JvZmjyFAyPlAAHA5zVrV/B+peG7eKZ0ElqJcySQ4DRM3AYbiNxboQOBWWzSyyCGN7YxR2pRHYqow6jdEAG+4Tn5yRxyeuKfMmEouOjLenoX062VEjw90wCqVbbgAlQCSyqT0H48jmqVxHJK8koUfPcyOXdRhAJCCTjG1QBjJq1bX8NsdLtoGxew+Y5ZAGVXI4IJAywXAIJK4Gc063hnW6YyR6UEIJaSPYxIDEBdpkGcnkjGM8Z707olMxtSD/br5Yh8pvpAqBRuIDEDAHQAcZ6DvSXzC1tdQEOFUkxgqp+4ZSMcjOCOOMEjvW0sMzXkz6hZ2EHzu+6aVDIXyQAdr4LE8kA8ehqneiae92z6VY3CjCKZLhAy7eAoAcAHA5BOaTaEk7l3WBFPYaWuFfNjdlXWTqFlYgDAIOemB9DjrVLT5Zv7aFz9m3yoLR1SNP9kcFScHI6n9BXQ6jpGsJo+ly2ejRXVvBFMrytEQ6qzllBUkErgj5QMg9zWBHM1zEs2zSIjtUMIoHO3C4QDDcggAg5GT1xUJl2dtQ8Oh4tRhmHmSAzNCCyA5Zo8DcQSBz1HPHIOK6mWQLASrhcjHLjJPQ9T2rmo/tMGjizMlqdjmUG2UgbgMMSxPzsBxleB0q6miard2sdxHbAxS4eMCVBuVxkYBboc8A4J7CsZ1dbI9TD4SMqak2Zd5fXUV/OsSWuxJCkYeEkKo4A4YZOO9Oj1S8BVjBExx1gDx4+mc1YtNE1PUIlmgtd8bKXUl0QNjgdSCSTwKLbSb+5DrDE6sswgKl0Pzk4Cj5ueeMjj3qfad0a/U4p/EN/ty5hBBtZyOuTdnOfwQ08eJ7rAU2023IB23SkgHuQVBoGl6jJdSWiZkkCK5SN0OQwypGGIbOeMEmq1zYXOny7LqJo5CvyCRMDn68HHfHSolUj2Ljgk3ZSLn/CT3MaBtk6o5KqzKNpxzwSACfYHNOXxYwCqQ/rnavfp0brWam+SQwwh5HBCqDlt2Tg/KBjB6DAyaU2cnlI6oPKlLGIADLBDg4A5GD1J6VCqx7GssA1vI0h4ulVhhZMDrnAP6EiopfFlyQW2TsvUjztmPphTSW+kajLAj24QxSDcp8xAGyccbiDweo7Hg02PSb+5uHtQm+eN9jx+ZGpUnsMsMntxnmrU49jF4RfzDG8STynaYbjOcAfbiPbtHW/ot1LeRNsWYRiJHIkfeQW5U5ABIYAnGK5ebT7uG6S3MGZZiTGEdJCQD/skgZ98U+4s9RsYPNmguIUVsZbIVWHAzjjA7dvQmj2i7C+qL+Y6rUrq6jg8q0ijaaTj96G2oo5LMBzgnge5q1Z+G9burSC/vptPvLZpxIMKfPtpSMIxIVckHAKn15rG8GSQ3t1cWNyWLtHvQ7jncCPkJycg9QBznkelekWsNpZaTfokUsd1KgE0nzEMCduVJGAMn3PfNbRd1c4K8FTlYr2trFYwPFHkEnDOxBZiO5YdgegHTuKxNS1Nz4z062EpjW2blhg7HkUgtjPVc96kvPEdhpkrQzzxKqNgIrBy3IONo6E+pOBXCaBfXV94wle5B2mcvgIQFYsFJJOckg49KlLqxXXQ634isbaCZPPWdptn79cYYk7jjBIH3eaxNCLf2RBnvnOegIII/EYqb4kzbIrCLgo++bggj5flIAAHBz9aqafe21np1obmUCIplpFySuSABgAjkHuQa1iyLam7uGM4Ge4xgHKnGc1Bd+Jrnw4UntZrgzuMJFCeZCCASc8BB0z64FVF1SFVUu58zHzr87Bc5BbOwE4HYgGpdOsh4g1GMW7xidLdgCzsApZ1IwSpyCB6YIzznFJ+RfKmdZ4d+KmozwxnWdHaEyEiJgxBkA5YrlQpwOT82fbvXcWPizRb5VVbzypMZ2TqUOD6kjH5GuG1XSW0vwaslzeI6RXCNBEqFvLLZUgg8jAJPHUCuPS9gbaAz44BLLzg8dyc8egpLVakOFz1H4lJbXHgW5usrI0LpJFIGB2MWCkg/QkcV4EkK3E6xMg3SMBgoW+8QM4AJIHUY5rotWv2l05EjmI3OCE8zIyOmRnGAcHHTPesGGSKOIEowZXJypIHcfgefXH86xm7PQ9PB0n7O5bm0eIQXbh3H2Y+UkTqoYhfmJYHAA2hiOvTk5wDBHo+6WzBaAC6UESyISkeTxvIAw2OqjJA64qIRrNOiQvJuLYzySFJwWwT275JrpLLwTfXeiT3kUkU09sd72is27acjIJ4JI59+lTzXLnGMfiMG1s4rTWUR7SJvKlYMskZXkAjlSc4OQQPxp8drbXOiTSGGMzG5Yb0BBVeMAEnjHp71UhuIo7hXYu7jAJkbJBBwSSTk9h7d6kGohLNrYRHaZWmDZOd5wCOBjAHvW2HqRUZcwsRQ53CUNkVvsSAqNh2qWJCk/KOOvPQ9fwq02mwLb78v5xG51LnGPbA+n51EuoCLkqWC8kjgtgg7Twc56DHTryKs2c73k7215Z39rtcCE2ts05xySGYH5icjOB26VhaTd0b1a0aVlYptYRc7l9Bkk9evrx1pfsSKc456BhnJ9Rn2q5ffZ7K+mgVpXZOF3R7AxxklgT8p9QapC7XbuXBXrkZI6jOe9X1NIuLjzCi1TDLsjIzjOScemOvByfx700245VURDjIGMkAHA9eOtbeiWpu1e43WiRLuUedGXcqCCzDLAADIwOean8U2Gn6RqMKWzySRyorqwU7XXHDg/wC0cjAJwU96v1OdVabny2MJFMaKiqo+fIY4zgA8ZA6Z5pYNa1G2EcMN7LFCj52RsFGDknOOSe+aRpYTlkZiM5ywGOhGAc9eKrNtUPco6+bkFXAGAACT1Oc5x2xWbfQ6ZQjbmSO60q7N9BBcOfOljEg8xmLbc7SByepwTjitIyfNgAHnjd2z1xj1ri/Cevatf3Etjd3MtzwpggUBgpLffwiliQOOnG7nFdzeaJqFnam5aLKJkurAqVyecZ4bHPQ9/rWsNFqeTWlzyuiJ5i6yKxJBO0hjyVIBxx64FW31KdonR3yuNm0YAGOn6Y/KsAX8BJIuYDvU4zKuTyAD16YpV1CJ2ADqSVzhTnIBGSMdsY61dzJampJNlCSRjcpween1/GuL1rEdjO5JXMuQR6Fz0/Kt9rglzFyNxBGI3c4yccKpOD0BxgkH0rndajlNzaW0yunms0rK2MbBuwQQcEZ/UEHkYoktCubWxjWd8q6pi4XblGQuTxkkYIxyDjAPBrWcmGV4SyeYoBIUj5sgEEDPOAeg5rIttGu7kieK289FClmRhkbhnOCc8npgdc1PFc3NmXhuDcJIqkguu05HAYqwz0OM5H41zHdzS0uT6jsvVAcklMlSGyOcD8CCMgijTxFdnybgeYuACG5PAzkHHUk9TTLhXSAZBClRySPm4GOMDr/nFMguBZJM7R5aGWNQsbbQd4Oc4yf4aiTaeh0Q5Oprw6Dp1wkgSFwREHJkcE5JIPQ47DHvms688JmXZLADGjEqi9CxGM7QfmYc/eAxnPpWvBqltGzsEXDMSTuPGOAAMZOOT+NNk1qIRRhSfNTOxyTuVTwduRweP/rU4zknqKph6c1ojnf+ETmjJWSY8HBKuTtIwevAzz9KsReGIAmZpXlQvgMGKhiAMjJHOKtrrb4kJY5mUAlSAWAOOcg5wRxwKkGuLKyCYHYgMaKpOFBJOAAMgkkljyTn0rVVGYfVYLWxU/4R6whIBNxjOVIkGeRnPSiTR7BEL4uDjLEebjAAzxkY/H9KjmvWaVm8yQLycBD8vtz2q3HqULMAqSGTJChVyWIyRgEYJH1x60KpLYqdClykuieHoLi5ju7pFk02TaTbyNlwjDPDAjDEYOQDjpzVfVNF0i2vG8u2uJIJSWhaS5AJAYqSQFOMFTgHBIwSATgeiafpejX2kRXKtPukhX7QQ5VtxALAqVBA3Z7AenFcD4suoTrDQ20waC3QRxhmBbgAnOAMncWGfbrTm5WujipRhz2aM2W100wMq2bI7KQCZydpz1+72qlJpVqiKyXVyJQM58sde+Du5P4VIZjk7W+bnBxnI68jHGPeoWYkZLpknAbnB+n+ee1Z88zqnh6diTTnsdHgmmEDT3hz5LO48tQQCNy4OTnJxx6VveH73xKpm3KqWciOJBPbrtDtySQRkHJPOePTFc7Zl/t8LW8RnlQhhEqEswBOPlAJ4+levabBo1zp0cws7SSQxplyzDcyjGSpbBYHOQcZ69MVvTUpbnBWUIO0TznUILQajaLqFpZPcSxEyJCPKiBDFQx5PzMqg7hxzVBLW2t7u6EWkwMybQondyrZ5Gz5Rgkcknj0rtrzVrKbWDaWjRm1hxbu8YUqWCggLwRgE7Op+7nvXNXlw63ro7MSzkMGBUNzkHkY46daiU+V8o4UOdcyZnW2pXYlltYLWO3O3d5WAQ7HIAJx0wPWkVr+33GG1mhQBQSs6MdmecADPSm3kxGoAk/MVLZI6YwRjGKJLpt6YlfAySTjgEdOMcVakYShrYh1C5eOQsYYZN8zKjAMdqg4wQAfpgVRkv5kY4iCEDOFXA/Edce/6V0mh2o1LWoLFJVWWacbFdSVYkkgnHPvwCfY16RJ4PnWwCW0tq74LJlAoAPH3gDlvXOPoKbb3EoaHjSXGryqXgSR0P3WVPlYdcgso4phk1s7gsNz6ALCOT+C1swukSiFDlUwq/OSBgcgY6ADmr8lwVTId8K2AFYgfmATS52UqZyudf3YEV4uQCSIRx9MDpSGa+uI0EaPhD5StHGcbjyVOBySecV08NwWUYLBeigu3zAcZ55xXT+F7Gx1mW8jmCIdiqVQjEjA4BxkfNgZyOR3xTUm9A5EjzdINRSW1nuo5PJaZSnmYypJ4zkZA9c8fjTtZjvTrcyWfm+WcNiIcAEdgMcH3r0Xxzp1lpWmpKbj/SZZhsEh2sy55YD0Udzgdsk1w8swbX7l5GJieFCDk/MOCMY5565q3dK5m0m9DHXTtblcL5U+89iwHT6miupt5ovtSlQFYHIIyceuOTnNFZObLUEdLfaTNc61lJVFrM4wkTM0gAAymT1JPOeg9ulTGZtN8T2dp9mZRHG5VINoc8AEkscNgD0ya2bW2WCKyZ0nJXBeRyN7SEYOCMbQAAOck1leKre7uoI2F0sEAwSZFBZQDwowQVA9MkmuONRc9wlF20M3UpIpbGytjaXUlz5nmuVUGONHO7qq8MVIGM8HOAOlanhzUoLd7keH9DeXyJWjd5JBGVUkkAE4yQMA9CfSi0t44FcQ+ckTAsQ27LjHJdRnCgdhyfY1TXXHsrr7TaRRTeYCHABQsFJ5dT0I6jvj16V0xr87sjShFxV0dDfazrGp2v8AZs2kxzpcSCMR21yySRk8hiShC9epyK4LxF4c1PQJnS8s2jD2zwxFcFWXHZhkYHvg+2K9Bn8TS2dhZXFxNHbQENcSWyx7HZVGUywyXDZ4GF4x0pdL8UJd6lcQ3xM85IJG0N5YKghNp4B7tjJB45xmr9pY2lQdSPNI88t5ooY9Lt95k8qzmkmTAG0kZ2jpnjDZz1ODjpVCzitpoIGWJ5JS3l7l25VTIWOMkAEg4BzgnjjrW/4k0xLXxrctFuWN7Uyjy13MpYZKjkBjnPoKy5tPl01ryND+8is1hKRqWbMpyVGQBuyeCMgVpCXMrnDOCjKxDqU9v9tW+MREEuoXMx+RVcYJAUfMwBxzk9+metV9Hkkt3tZbWzOoXsikxwKpZQw53sACWx+HqTUcKPNp0cRN0WE0jttCsVBUYPIHzZ6+3QZrY8O6sugXonjSWdooc7FQZZ85K8EEKV4z1P8Ad7VM5W0OihRlP3raHpOm+N9UhCJqGmOU6GS3QgKTx91uoPqSMCvPtYhgi8U6q9klubOSA30IVTgqEEgYDggj0OATwMCu1n8YR60kEOj2kkt3dq/liZVRVC5JYkt8yr0HQn0rA17wvqWpItyipGptDEEt5PMfJOTnIUEEcE56cgdqmNSzsa1qTnG8UcbZ3LnU3twZBCvmeWjMGKgsSM4AHI9K76LWNKj0qKBNTt4ruGHCLHG4UOFwOCpypbqM8djXIiN9rsXIV5zIIpFUSqSMFmCkgZPRc+9VZTiQufukHlTk8elYTqcsmz0cJh1UpWZ2Oj6zptrpdtbtc+W6bhiXO4dyvAI2g9+pPGB1qz4Y1PTdNuLpdQvVGJWdYLeB5ZCSdwdmCkqMdAQD64PFcKCA5Zh0Ykg4IJHJPXpmuh8GeJdC0qCaw1a48tncyLcuh289QSCWJB6Z49cVVKbkLEUVTV0zr9WvPDl073enukF6lvvm8y3YExJzkKdpLKB1HQcVxHiXVbXUJbaS2cf6sK8ZLM0eOB8xGCuOc9c9eOa35/ElrqPiu0n8P6LNeW62z27XBwfMVjlgNxAKjoQdpqa40rTomgY6UqxSsHUTS4yhOCAAAGJHXDcdaqavoYUJOL5mcVpN3FHrdjI0pjVblWZ2X7oBydwB4GPTNbW2G20+HzLy1zHZ3yMiSEtmXcVxxkk0nhixjmtVM9pBKPOwTI6HzEXhkALBlIPcggjpVbULe2t9Z09bi2s1j2qZijqIpGJAOdrMVUdASckc4FZKCR2TqucjV0nVrKKCOyluQk7RlmCQqI2LRFQjuzZBUkMSo5Iqjp+qWdtqN9fym3dWmzGTGzTMx6FScBVU/Mc/exgc1eu9P0+1tS01rpTQWylJGWKRAzs3CxsSQwBx8wznuBXGeXjgkkLgHHGDnnPfjse9Em4rQijSVS6Z1DXNpbeIILhLqzgjCTEfYkZAgwQgZiCSWHXgBR0zW79nk8VM2nC/aPy4lldhIJCAhAJIAwSTjGSOOcDpXnBA27MDGc8dD9a9E+HISy0fUb94+DJtXIGGVFJIOOvzYFaYeTnKzMsZR9nDmT2NC18L2WmQNDaw7HdQDIznzAAQcggfeBHAx9Tiuq8KxLA17DPFHKs4AdCQGbHAARm4JXOcHg1BMhRXZSNxG4DG4txgEHIC+p659q1NPkto5/KGNpwqNIoKqQQQAeoBPqc16lWEVDQ8FzlKV5HEeN/AOj22jX2saLFc2Vzbp9oktGAMcijluvI4yTgk+1cD4SiGp6+FMkkccPKm3l2MzFSQwYg54yOn4Zr3bUP9PkvtIODHPbmIlDgAyKckZz1OB1rwKw1W00PXbWKB/Nilby5UtgWmYk4COGCj72DtU8Yxk1xTjZGsJEnjpiviIwmV5I4YIyvmNuKsQS2QAOT1OPTtXLjXr0LaRSLG0MJyYI1wHycktgnJI6H9K3/Fiy65rc89i5lgkQIkiBsNtyCUG3JBzgZOazIvCV1Hhbm4gth1/fOU3AkAY2hmyeuCBRFdypeQq67q91O7fbWtYvvfNlVX0yQpLE/Stnwh4pvbPxCktzfTyoylFDoNrEkYBUAZB7DI5x0rAu40ifyROs2R5gfkZY8EjqcADgkAn0FVbeaW0u4biFyk0UglRscAqQQSDnPIzitVBMjma3PWtb1dtd3Wg1GSDy3Lie3VSIyAVAIJw+c9cjGMc9Rxk1l4ggmVJNTlkt5Tsiu45V8tH6/MNoPQHrgZ7mq9jqjgbkKpsBcKc7mJIBC4BG7BLckDAPJOAbWn3gl1aa1t90iTpuMSgkkgjYAD1IbnI7ZPoDbpR5boSm2ytvnSe4tbi8luPKwxaVFGCOM8Z+UZ5OcV0Pg+Gzke4a6gtpJd8ZRZMsyjDAlRkcMcDJzjrg4weZQM9zdzghwWEZz/AHueQOMqCP8A61Caxd6PBNJbXDxByC8a7SJCOBnIyB7A9K8+/vWZ7cF/s90dk9naS+LbKzjtFtyYpHJiQ5JIO2QjccjAZQSRkkcdBXW6bbrpd9FcG6cxRQPvDH+EcliwH3s4HOcdhXhtz4l1rUpxJJeFH4YGMBeMgjH4gHk9q3rPxZq19qEcM7xyRNExUsCWVlUsXGCBkgFTkdGPGcV1RpK3NY8qrWbdrndPp+hm6WVrCyffeCA7kADK5BCjLjcevzDkjPy8VheH9L0yaA/abKGZvt0iJuD4CqTwxHBAUE445AGaxrXXZ7508m7linjkcWroqkIrbflIIPfoeo9aWztNTttXh04aiIlk/wBJDoSyMoJYNtIBb5lHXHPqOKnlu7I3pTcobjtUsVsvETpYW0kkUTAshZpQpPByQoxkEewHQmvRLTQYIzbXEOouLXzI5Nip+7yCMkMrckHv3xjHNcxHdzmxa0junNu4COowNxVs5yBknc2TzxkYzirdhFcW7Qu0nlRxt5irtdtpUgghVUjOeQCcHnOK6FhHa5zTxTk+VmXrVqup60s95BJFLObl2LI0TybFLISCDkEgE4HT68S6FpOmX9jBNNZCVyAHkjgZxvKOx3nKhMbAMjcDuXpzXbeJfEsZ0+G222d/LcJnLxdA2RyA2QSTgKGwOSTxg+cTQzWt18t5mNI/LgjgLKscQ6AgkAnLOAec4PPSsXQa1ZrHFcy5TrPDt5poa600QpAYJ/kDI0ineoIBwpKkYOQRjpzniqfjCOXVLvSHeCQ6ed8W8ZhZgvJyxXYgyflJPIzgZ6Zvh2Z4b8OJG5RkyuD5gJUnbyMkYBIbsDzXf2IaSTWEhk3rthQFQDllJO8BsgE7iv8AIHFFlaxjGTjLmucJq+gaXaaZPcw2qpMFEiFb5TsI4KqMfMpDAtjOTjkZri5G3Rvlv4D6ZORx2/Su98awzWljcXSw20Qe5MFyiRnDqQpjbnBBJDZwACVBIFcBHG87GMMQz5BO0EZwcnPXgZNck1Z6Ht4SbnTabNLwzo+oT21gsE8ttDdPIWMDhHIAABLAZVScjJOD05PTr2t/EVlbSpb3c/lSDEkNxOZY3UjBHKgjPPI6H16Vz2g3dy99DZ2rLbrt8uJVJy20btu4j+Lk4I69xXXzXGtRqty0DIsaZKtsVCT0DYYkjv270cz3M3SUXys4HWNUn0O8a2uLe2kUFTEqwBd0eMKwbOSM5HTqpzjPFWPxhaOuZtNZG5yyOW9fQD/P0rovEmnr4j0B9QidJb+yYmaOGEqrRnAZlbJ3KrMMYx/Fx3rk7O2smtmha0gLqAA/zZ6YJJzz0yOOK1jNWuzjlQl7XkR3XhOxXUreXVbC6mtYy5iRMgsAFB3EhjyCx2jPHPHNVfF12dLeE3sivcJbkRlEwzhiR8zDIEhxkgcYwcZJrlrHVL7wtJ5mmyukU3Dxsf8AWbe5yO+ccc11ceiW3iTQmu9aW6S9Z2aKS2xI0YG0liucOoDAYB3dT7CnONiHhqqnqctoWuyWCBY76eDflXKKNrAEYySM8c9Pet6416HUE8nVbCC9t3+SKeHKOpBJ3DOSWIwMMQMAEd65PUtI/sK6eM3cdxblQ8VxHnEqsThsEZXkFT3yp4xyYbW8KrIqS5R8d8DcAcHocHGQM9s1DStoa06zT5ZG1qdrbW5D6fe3AGMiK4UA4IGQHBKt0GeR9KzVmMlvdeYWONsp7HIzjg9ueM9B9atzSX+oqtl5Ks5xKMgLnAHAyQMgEZ6Z44qC1t5IL+5huYmyqbJQw3YLEYBIIHpjBP8ASsGrO52p3VkdBodpY3WnW15qqtbRvv8AMkVyFkY5KkEKdrcEYOAQByDxWnr2j6ZBpyzwwyRyO8BKtKdhVwNxIKjGcA5JGSelZ8OuX3mpZpAnlAiL7OqAltuFIJPBOOM4HTPHenqmqXqeZYSwCJUkDqrKd6hcBCCTgkAAE4weMVcZx6iVCqup1DeE9Ea2lBivi8qtJHIzAHA3bdq4IVTjByTkKDkZqno/hzTbzw9Z3s1uzyyQiQkSMBIWkdRnK424TsxOc54xnnl8Wamkgy0Eg2sGDIxLs3BdgSMPgAZHAx070/Rk1m605raySJrWSTZI00oAHQtHywO3kE7V9Oa15oN2RjONSK1Zo3Ok6HZX+rC8juvs9mI3QRzkOA6BiNpXDbSepK4B57ZwrnWdMt4heaUHSf5SYZ/nPAwSGBG31PXPt0p3iKfW7IXBuvLjW/Iidbd0KyLGAoQYJIAUL1wfXngciJCHIBwMY6A9eTVKKOOrWkna51dr441G3gdLqCK5DAnzC5V2BznJAwc9egxWrJ4nspNOtLi+062maQfZ50OAG2gbHHAKnBweuQAfWuM0t4hfxxTRLIkpMeJJCmM4wwIU49OlXvEEQgW3jRbaJU3BUgkZs7jnDblByAQM9wB0qlE5lOXQ9Dm8I+HBLAiC9xPC0wDXnzIqrk5ITDEAjqR1x2yeZ07R49Q0iS5h0m5uJ0XdiC8C9TgKUK5wMc4JJ7AU+w8Qa7LZM9vbq/2GFUlR4gVZAm0OSW5bbgEDjbt681p+Gb99E0iJPIikV2LsFkKyscAHO8Kp+UKMBs8cA5zRBRvqdPPJx3OevPEV/f6dHpFtp9pbiOTzMWVqyy5XPcMWHUgg/pTodW12KIQS2MsoAxuktH3AdgSMZAHtnHcV2dj410p5JppZbeGWVhu2xt5jlQFBIVSQcLjryec81eTxlaTvtjiu3GcE/Z3PP0IB/Sun2kDm5Km55ktxNbMzzWoRnbLCSMjkgDhcjnHT2p1226UtExRSgcAE/Nkkk4PU/SvT28Q6XKn2S7tmSOc4AurOTYxPO0kKTyeRg/jXneupENWfyYZhE5DKNrAKxAL5UqCSGLDjj0rnxHJJ3idOH50rMyJUEDoxfzOShJIPYYyAf601Ihc5CSqmPusi89eQMEgnHriluo5TtWG3l2AE4KseemAQvt/9ekZZ1QBILjlcg+UeD1HQDOPcfnSoxi/iIrwqX91Eq3Ri1WF23C2jmV3OSCyjocjkHvx9a7vxB4qu28NTKupwv5iEeZGy723dCcHIBHXAyT6VwDwzyOpSCT5mPy7SpIPbJ4GD6/Wqkmh3sRTzIOGyp2uuWwfrVVFG+jJpRq8uqNyC581I2BfheRvC8kYJ3BeQexqdXDLuSWU5AwS4YEEcH7o7VkRwXNvnCKNnGWdevbGCccVbilCLtZ1BBAIJJxnkdAfWlKMEr3Lp060naxWl1SYIuJfn8tctn7oPXHGKk0fxFdaUjrGqyRyHed7Hdu6ZVgPlJ78HNUjpj7iRcR+vyk5x1x0py6aHx++k69lB/TIFZ88FsaPC1rianqU2qXz3UzEnG1FLE7QDkA5zkevqajsbq2hnb7R5mSuA6vgKAOFPBOPf9Kn/ALJRv+WshOeFKKv8mNPGkWrMCS7seytz+HFDqoccDVe5q2rQzR+bGZGA5/dyHgfTbn+VFWNLuDplsYUiUxL8xkc5O70wBkjNFZcyZbwso6HZS6td74oLe6UNMDltmAuAdoByeuOpFZtxrUN3b2zahfxyTSBZDHHZFiuQD8oLKDjOCxPJBwKy9LmWXXba3S2GxpfLllDs3lnBIC5AIB9SMHt60k8VxrtrZ3DSpbQRz+WZU2lWUepyAD/skgY71nTpJfEjiu7XR0sdxa3srnTbnUoryKJ3kiaAQMyrnJbLOPcDgnqcA1Fb/wBlaPYRBLa3vNTkUTPctuYsW5HfAAHBXnNV7GKw0y2uVS4n/tFo2HmK4JYHoCdxAVRjAySOnNZcilAof6YAO5SAPbGfek6ns3oj1sBh416bT3Ohk1zRb/U97+HbNbZyC3mB5JIyOGAIbnB7AVe0aHSTqkk1lE0c8LkhJVZUkjBIUx5H3gAAc8n9a5iwgkM5VoRcqvLMuA6/Q5AB/E1sRqUVhbXbZ6+XK2GX8TnNEsWrWsdlLJ7yu56F3XdH1bVdUF9bbm06EHzFj4IbJB2gAsxK474PtXH3F3qNzrE87TvE0kxlW1mREVUY5VSGYEAZxgkHtkGuj/tnW7aVY7GeeNi4TEGBuk7A7gcg+oqDVfE2o3MFzp2pwW85eMiUTW6+YAeeowQQPr61VGvpsc2Ly2XtdGjEgF9cRGJJZb4yuVjVY1BwCVLDYSSM5HIHHIyOauW/hHW4BJeXWnTRW8EbM7NsZeOcYVs8jjjJAqja6xPpsm6zLRSqP9ZGu11I4GeACAOMDtW1H4x15kgijuoIVXOGZT5bZ4wRgkjtk4/rVVKqerNKOGq048kdjobZbBvLmtLFLUzou8QtlY0zkcH7uTzwTzWj4VuLn+0vs99skaLcBtHDqG+Uk88BcEcDnjrxXL26zhcN5SrjYAhL/KBgDPGR/nJ61JDPNasVDGNvu/uyBuOcjGTnbnnk1yqveeh6csB+55UZOvRxQ31xb2cA8uC7mjKxnOSrkKWwMliByc49K5+aOZQ0pglC4AZypIXAycsOOnp+Nb1xOk7OzbV3OXKRtgKx5Jz1Jz659eKa07C2ktUlBjZfmRiCpPUEAHqB36VrKd9WjlpYeVOPKmc8sqGKYl13AbApx689/StPw99giMslwpMiHcojKhjuOCBuBGAOcHj0OanjmihXPlW8hb59zWwypA7EMQR7nFXrfVLd3CNYp90ElFAGD0GD6exrJ1WvhO1YWNRWkb0l0k/lbAnkSsBuUqiKM4LZBHIPpn8a0Vv/APhI/DD2FpdGLUWiMtqi7N2F6Bc5wGXn1HpnissW6TREJESFG/AXhcc8YGM57Dn2rKguJLfVwln5ifMIyEdVYgHJ5K5A7EYHHHWilVle7MsVhIytGLsciwvtJvfniktbgZT95EVdh043DBGfSobe9a2mE1vLGjg5UrtI6d85GfwNelXW67IlleRiAfmwDjJ7bgcc1QubdIbd3DN8mAcKm3J4ycLng0SqxuEcJOxyMniHVJhmXUXfcpXl0wFYYI+7gHHoM1REo2qQ6E54y4yMcge/pzXQNawMc+VGTnk7QAzDnIyBUsGkvcIDBZiQE43qhxnoM4U01JyF7D2erZzDTICF8xMg7Qc568dq9A0hzN4FFtZsoYiQOpGXaQ5Jxg5GTyBjp69ahtLHVLe322sF1HDneFjt4nIzwfvYNSXd9rWmRpK/2iPfkhpYIxuI6cqTzjnkCumhWjSfM0cOIw7r+4pHe/bIZ4iQyn5SjDBUHIxgZ5Ge/HFdDaRwKv21Y0cn5jJxjnk8EgYGOpxivINHuJv7NwrRhQzDDKXcljk5O4EluwwPY1LPfeZ/o6FWbeCRCWQOV5wCWIODjt0reeYKWiRgsjla9z1FrKNfEeo6rPqCwWQiiZdoGRsBywbJ4HpivnPxDBbXMtxr+kI0a/2hIhjXJWME7oyvQgEAknnn0rv47O6jRkS7umi52LsLADGQM55HpwRUuoeHZrS2Te7mNwdyxx+ZnA4GAo/CsPrLeyB5VFOzkef3Gr3kywtHNLHLNDF57IOJCFbOQBgHOM4I9cVFaxyzSxloGKbgGLBuh6jJOcdvX0rpWt3jbLRNGGyVEkZXPY4yBkemKTDKwdWQN0xt6/TnioeJa6Gscpi1dSOW1jQ9Vigj1qdECzzfNFEfmjbBIBABwuFOOT74rL8u4Ztwtpj77D/hXdx311aRMEljlUEsQ8eceuMkZB688nHHoXL4gvU/gtuRyRFgn6Zbj8M1X1ppbBHKI9WclpdnO0rtLFPHGBghYzkqSDwDwSDg1v8A/CI6hp2sR6lNqViiS/vCYnZgrHBCOmFYAgZyuQMVqarqjTwJCIGj3gNJtP3gQcKSQOvXoDxVAPO4DOzl+hLMOPbpkH0GaccY2hvKI9yjdWsn9tX4jEf2csZVYoVVmIAwqgk7eeMn15zxVJtFkvLuGG6d7ay4zL8rEnoMKSMjJxk9Bz2wdss6kZUYPGMHH44J/HpUlpZi8uk869hgRm2oZc4c+gBHCr3JIHuaxlVbd0dH1OMaXI3oc9e+Ffs1266fO11CMESGB1PI5BAyMAgDIPeprPw3qgeV/L+bbiMgsME4GDle4zXpVt4fedliXxIrsVwghZASBk5Chjn8Kmk8L28OTc60Pl+8zzKCO+DyMHvWrxNXlsjnWAwfV6nlNtoV1Y3EO9gvlOSQjHOTjGMgEHr14/prM91/abXTlSqxiFAzAMqgEjIHpknjv61NeW0MF9JFHOZwjbPNA/1h5IIwxyOetIEUfwjI4wRzUfWJLU2jgKVtNh9nb3mnxb76SRPOOFVVDEAAnhcjOc5JyPzq1PfTLGgiml2MoJjkXABzkA4YkgYyfTjrVMuSw35xwMNk4HOADz1P8hXTJ4V06RUP/CRQFjwTsU4JAz0cHPIHAPOOK3hi6z6nPVwOEg/eOVvJL+6XIu388OHDsq5LAcYIIGPwqKy0ye6Wfc/lKWViyKzHg5OACec5z0GOneuzufB+n2cojl1sIz4KR/Z8sc5AIAbJ789q5zUY4bK8aGzv/tMW0N50eVVjyCOpztx/49SqYirbUmnhMLJ2iSbBbypLFfRvKvzAqrjb1BXJQDBGQR19/Xb0bVpY9TeVrqOMSSgzR4YgIAAWQgHJ6DBAx1z1rlWmZvlMp9wrEgn3/OiG/uLXzhG+BINrHAyAOTg9Aeep61isRPY6VltHdmjqF3ezz61DezrcpekshRuEYMNrEkDBCjBxnqawrGzNnFcgoSzxeXuVsELnLFhz7Yxzgfn1cXhHWZ4I5gIyrpuyCSVzgjgA4J/xrXXwSBYW0qxM14GBuI3clD1JAAA9FI+mKb55aigqFL3UcjpOj3sUtvfKbhZUbchWNCrLjGfmYEbhx0rpb2bUbyz+zCKGOIMWAZznJGBkhedvP/fWMd6uahbz6ZAbm42lFGTsJLEk4GAQMAVjza5CsO6FGLn5djcBc+vXPPYe/IrnnVqLQ9KFKhUtLsZ0djNo8btJOjrKjReUCVDAjnOPugcHp1x61k6T4ZaWVlgumKIMtuXjk8A9fcmrk11LdSs80gZgCpPQL7gdT34/M8Cum0PQ3GnrqMlzBGs6gokkpRduSBnI4JK+pxVQlOSsjOrGhCpzyMiDwukbEyvFPF0dWUkOPbnIx6ittbfyrSK0iCxxQKEhKuSy8kkg4AySQSPb3rVuLWAWcaLfWUF2Dl5BKpBAyCBlhjIwM/Wq91braxxy70ZHIKhMEEdDggkEE9KmcaiQU6lCo7M5bW/CsOqrDJJdLbpbRFCwTAblmJOTwSzMeOOccdTk6bo2lXFsdPh0x5brkrcLIYn25By2QwHcY6jjnrW5rWoBVe1VhtxmQ9z6L+HGT6kDtU2nyLp2k+Y0YEjqZHXByxHTJ9ACM+9TCtJKxU8vpSfMkY174dvYnDPqlvbLHuMbzMA6qeAG25yMADO0dKzV0uV7pYlntVd22lw74JAGGb5SenIOB1q3czNdSNNL88j9SR29PoOlVmyrqwzng8Hnj155Fae0l1M44SEdjqfD2lTWM5+3RWd7bkbkkgu0EisM9mxnOemR2zWtrvh/UtY8Gwq1j5mo24Lx+WEBK5ICjDHnbtyMnkcVxP2uXeHDncB8rKqg/hkHmums7nzLNJoXi3kAkOz/ADMOpJ3DB9Rjr7EU/bpaMUsDOWsWcLc6Bq9uSJdKvA3YGB29M5wuMD69Ku2o1HRVggvtI324mNwGaLzGUsACylWxzgY6iuybxNq4vo7OO4m8tHzKLRt0rIOcDcMDqOTn0pzSCS3ki/srWZA67sSFNvI56Nxk844xW1OV9UcNem4S5Znluq28+o37Sw2Xlo5bCoCFJJJJ5PDHjPatvT/DekvpkgnSRrp027kbLRucEKoA55zk9AOhPQdzHoMG6zL6Xv8ANXy5/nx5eEUb3JODkg8jp6HNcjqUFzoerSwqZ4tuTGZNu9oySM8Eg5ORnI6Z46Vc5SRhDDUpsg03wVrml3Lypc2CLyrmQMVwDxwy9+2Oa0JfDdxqdkLSfU9P+/nMUamTJ5xkNk5+lYs1xMxG+aUkdQ0hIHr3xWr4Thu5Nb87T4rYzQxs5E+QMHjqASDxwccc5rOM5s3nhaFOOxQ1LSbXTIECau8zvhGSJtowgAG7qDgYH4UXF5Dcac1rJA0hcfdOD5TDgMpBIJAHU4znGOK7q2v4P+ErNi2kpaS3MAExkCFtykkYIyGBBwCMdOecgWdL1F7vXNRsDZW8cdr8wKxjJUk7MggcEcnGf61tGMmtzlcqXNblPMY5HgiWJIvL2E4AYEr1OMgZJ75zUq317GRtlMbAbtwcjaT785qXUJFk1O8eIfI07FOMHbuJGAO2Kq7SegAP0rN3va53xjT5dEPk1C/K7DeOzYAI81sccDggjp6GruqabqGnQW7XM8cqXCeYmJGYIoGQCSAckemRVGxuLCDU4f7RV5LZTh0jwWIHPAyB39a9Hm/snUI7K1ksWnjSDzI0mQnyUCgjIBBJI+XAJwQfrVxhzIwq1lSnZLQ818yXA3ThTjoMjH6iky7c+exHqCcfzrvrUxWkelwxWduq3126xsyg+WhJcZBBJAVguM9utcp4pUnxBOpwqKihVRQoxtBPAJ6nnmlKm4mtKvGcrWMmTavLzHnn7xH9TUmm2MWqajDZo6KZDjzJGwqgdSSSB+Heq3loATsPPGT2zWv4fuNNgvnXVbYTQyLsXIJVHI5JAwWGMcg5B5wazhdvUqrLlV0Ran4eu9O1F7VIGlWPA3rGzbcjPzAA4YjqCc56ZHNa2r+G44bPTZNNS4l82LMqt8zBmGehC7QQe/Q8Vp6C7/2ZrqaM115/2om3EzkyHCAKGyME5GBnn6dK11/tfOlOJmRV2i/XcBvGBkEYO7LZHBBrsVJNHmyxMlI83nje1laF4GjlQgFWABHH1NVmkc5w2B+tbHijjxJdY5JIOT7gGsUqR8xrjmrOx6UZcyTAZY9cgnBJ4x26jJxW3qWjz6E1ukjxSNcxCSMxsTt7dwOc1kRjnOQMc59PrWn4g1w6ilkVynk2qQhmHKkY3t9C2QPanZWFdqRnSyAlUQ/KowPViDyevc0VXt0y5wNpB6ld23v6jrRSWhy16j52a0GrS215PCYhMrgpG4cK0YPIIIJJHPQjI7VYiglhurWwkura7jvIxGYVwgiU8gkMVXJJPJOf5U1bNLadFuLmyjZXUxwZbzHYgEYAUrgA4zu49O1GuWaprmoJDp73DszbZJEMaqdxJbAJDAHoCV459q7JWueIr2uTQaHf6FqFva3wSGRvNYLtPzBTkEMMqVIOQQcj0qWTMrcHOOMD269zmr0WpasulvaXrxJarGCuX372PUoxGBnqQDkjtVBm2ybjwSTzgAZHpiuTE2ex7mTpq7ZtaOhjtDhQMt8pPf14q5eKhgbKjJPB9PWqVjcoLdRnIRiuSOh6nBqPUb1gyRD5SuCT6k+grzkvePqXKMYXNvRTqflS/YpLEKr5Y3K8qTg4HY/mK25Pth1HcdKjlhwRI0aK8xJ9MkAgj35HPtWB4YsXaa31LzbVlwwEEswViOQCeozkZzitZtHu5PD8lissW9rrzVZJ2KqhctjeAGJUcdAOPSvSopctmfN4ufNUujmvEmmQpGJRoosGd/kuTMSXHXAXkbiPeubW1kDbVdZAWxhjgkEcYwK7XxXpN08j3i27QWcS5O5m69CxBzg9uO3auYhSBWYyJnCnBB+6RyMVhW0dkejg3eF2LFLPaplNyRhcEbt3I4AHAxzXTaJ5troxvbxbbzrmT92LqQRIFA24yQeCBnGK5mISy3UUOUxI+zzGbAGeCT7f54rv9OtplsI7GK6sLuKID5XhSQ89zljgjp0p4aCvdkZjiHy2iRyTGO1hzFpKXEjgKs8gSNu/ysUy/HoozWF4vmhQWVskUSSsrSYQAcdDk4HHccc1215HKUR/KtD5a5LzwgqpHXGT8oHXINcf4jtEnaTUJb21kJ2KIomXPBwcYJz68cV1VbW0PMwkpOpds4ssNvv7ce1S2e9p18vGUYNkAOc9gQSCQe+OlOkghLYBKDPXioo7eNZ1IJdRhQN4UsCeRnqM+tefo2e/z8q5kd1obDTtKu9VlLOmw7AznDhfTgYy3APfrmr0Damnh/TprS1ilnnVXuJJYgCAwyW5xklu2en51PM9pe+HlY2c8FsZUjWIbQ6hXAAAyQAe3OAKn0bSrW3/ANLtYrzDoIws8oYY3Z3KN2Mn8OK7IxilY8avUnJtsztdnuYfDsLX0cUF5LIFZEQqM5yAM5GceprkpdTaNHAYF88KRzg9SexNa/iKw0vS4JYTa6gJyhEMkkpkiyeQVJYkn8M/SuQaFw24sOxzyR0+meevNc9eCvod+DqPksTmYFtx5YY69B3Arp/Cb3V2t5Z2upTWcix70VNu0t0yWIJwD1GOnOa5ARsV2h1BzxnOT7dDzXX2eo+GbCRZobPUBcIpwyopbDAgkAsAcgkcj9aKKsyMZJuNka/9oaxceE4nWedNUe78oZQbwS4UK2F4wDzx+NYXjafyryK0F7cXJRA0nmMu1XIwAMAYJ54PTviuhlvNIj8NJdrazSWJlyEJIZSWGSSHyTnnqa5nV9W0C6t7lk0sfa5dxMrbgyu3VsEnrXTUSsebQU+e5z0N80McqxkDzl8vdn7pHJ+pwCBWnpiiOLznQAlSQC4JVegyCByevY1l2UcBZpZUDhMBVYgDOM7uTyAP1rQ8xDkKyqCMgAdc8EnPXHTH5VwSfvaHv0k+X3juY9Qvnt7aGxspkdRtZ5IRtY4wCMN0/wD10/Xr7XLe7SHTUM6bcuxjBCsemDuBwO/pVDQPEQiutmo6lIyt8sUb8q5PXlVz+n1xW5bQ6pateXFxfTagpU+RD5Sxgnk7T0weMcscgknFdtOPu3PDxFRxnZHK+KLi5uls5rjTrm3CIYy0+35nOD8u1jxgE5Nc+COm0+ueKl1DU9SuWaC/uJWYSbzHJgFTyOwGQAcZ6VArDdj2rGpud+H+EjmwUwRz1FVcEA/Lnt2Oc1Yum8uMuW6DOPpV7XtAk0L7KJZhIZ0L42kbSMZ56Hr1pKLaKnNKVjOMbR26zMxfzcgDdkjHBzkcE549s1ZjIKDgZ+n+c/WqzXUr20duAEiiHQYyxAIyT34NTQttjUFgCOMHt7VFraGqY5mI5A7YPbrUbRzTtst4mnYAHCoX4BBwQOq+o70sjA9Cv5n/AAqax1i70id5rKQKzJtOQTxkHjA9acN9SKqbi7HXww2d3rHh/Vba1+zXDKVnhELRhAI3AJ+XCqDwMnJ3d8VJM0FhqWv6g8JnmLEwwNbMy7GAGR8uGy20nuFU+tZOg+LNY1DxBZ2tzKGik3FlKkZIRmHHOcEd6TxB4m1fT/EN1aWtxtiicBV5I6AnGcDHNdqlDlPC9lV9pY5WWCe2YedbyRM5wFaMoMA5JAIHyjPGKGJAwD+mf8KuahqV7qssUt9c72iBCHuu7GfzwKolAG5LexYEVySs3oe3Ri1GzFVXlkWNVZ3bO1VBJyAScADriuw+yrfeGLRZbLyNRs54xHiFv3g3Ak8L3HJJ7p1Ga5C2u5rG7W4tJzDMhJWRSPlzkHGcjkZH0NbEPjPxAZkH9ozuu8bgGUcZGf4eQR2rWk0jkxcJzWh2N3aQ/wDCUQXtyHP2e03R7UZtzAnJGAQGwSME4yRzxXn2sfbLm9ub64sZbZJnJVTFsVQeAABwCQMkc5Ndn4u8QatpdxaCxupIkkj3OEAAZuCcgg9iPauTvvEOp6pD5F9dySxg7grYHOCM8DPerqzVrHPhKU1K5lbTIMe30oZAchieeDjjNKHYk7VLdsqv+JFRtKwbnYMdRhic/gCK51a56sk2rHoEmj6pqlpZ2k2raeiRJhRaSlHIO3IOCdxwOuOtaOtaFDf/AGa1s7yK2mi4YNMwlICgAABs+pIIycjkAc8r4Uj1WzuUvLPRxeLIhCOXVCoJGSrEnjjnjPGBjv17WujabqY1K5W2t72fCkmQnzCSCcZ+YHAxkgADr2z207NHh1YyjU0RjX2mX1toFxp0mq2RgUMWWQ5lOMEAMW4zgdjXDLuPJ7gY9Rn/AD7V0fiTw1fi5udXkMV3C7eaZVIUqoP93oQPlAwTn2rnFC92YMOoPAHP0rmrp3PWwPwjX3hh8vTkDOCcdgT/ACrsbXSV1nSbHTpfEKmI8xWy24JBGSQWLZOOecfhxmuP2EnIZiffGOOcj1wPatvwxZ6wb432mwQSkMY2e4GFBODk4JYE8AYHr70UnrYMdC8bnU6xpWmaw1vpy30EElsCGiS2DMxCjAY5HQc477qxtYjh0uw+yLrryy2RzFaNEBhsA449myOe9dHPd6XpV1Fe6g1tDqDgxuwJZs5GcgDIGQBkjPtgVyGvaC0cU+rwXqXFrIxMhLDcSzZ2gAkMf++SB2roqpOOh5+Di+f3jP0+2LN9pmDmJTlRjJYjqT7DP4n6U/VLxZFSGNuDy4ycH0/TNXZ5YhZwvFENuweWUY5GOCDngEfjWFIzOxJ7kHPc46fhzXmxV3qfTTlyU9Bu7Oe6jGSBgc+5PpTHKsoIH4/Q4rpNJt9OvPDOpJJb/wClW6mRbhl4QNgLjB6/K3GK5wqX6k5PrgHjjtXTKCSOGFVyYg27R6+vWta2uI4tBZWJ37iiYx8zYBP0wCMnvx6Vjco2AfmNBJzjHcEjnqOM/lWUoHRGbRZtr66tb4XFrdPFLkIXjxuUEjI5BBHevRLdLlLm6tJdUuZ1kto5Y5pAuU3M/IwB1Cg8152gQvHv3bcgMVwGC9yM4HAruJPEuhWzI8bTXDeSIFaNQdyYwFbcRgnk5HcmurD2W55WYQnN6FbUrm6PhIy2OpNdy2UuJblUCiRW+Y5U5wVBUVwtxdTXUu+4dnbO3LEnaQBhScD64/xrp4fEdlZXarZ6YsWnPGY7mBSSJACQCATgMOQRnn14o8Qx6E2jWz6b5byh9qsN28ICSQ/TBGcDj8T1q5WkjChGVN2aOOZwAepbvtBIJ9RkdKdZzXJvIBbGdJS4VCnBJJxxg8468/lSlScE8sRyeeT3NWNOuRp+p2928QlWFw5jLlQ2MjqASOvYE1nHc6aqbVzW8Rre6X4mgmF09xdqEYu2Mqw+XCkDDAgbgAAfmxjPNdpfNcWdrdavY2pa8niCFNhwo5IJXOSVBwQDnpgcVzq+JtK/t251E6dNK7Rp5DsoDoQoBTGSACQfnGSPSqK+L9VTVWvVlXDgA27AmMLkkAHGd3XLdc9gOK6E1Y872dSWpy+xyQPOYkDGCQG44OR1B9u3elSBeCS5PuTW9r+sJrdxBLHapb7YyCq4yXLEkkhQScYHI7VmbQEwfvDoawktbo7oKXLZla1sTc30EEKDzZHCIOc5JBPavS5pCfGAiXIWKxKl8HbgsWHTHr065rg9J1N9H1FL5IopXHygTAkc8EgjOD74rSt/F1/BLeSIsbfaJBIokG5UbpkZwSAoCj/dzitqc1Hc5a9Kc3odOLGa5j0doEA+wXRZg2c7E+Q9AeRjp271yXjBAviKdwB86IxK9D8oBqD+3NR+wi080+Ws5m3gEOXLFjkg5ILE5AHNV7++u9UuhcXs/my4xuwAcDpwAOn0pVakZbDoUZQd2Z7BivWtXwxp4vvEFsmBsiYykM2S23naABnJ9Rms4q2MY+nPP8qFeaGVJoiYnVsq6sQQR6Ec1hBtM6akXJWud1DI2p3viS2RgruPLR1OASqhAcjkE7c5xn15qb+zbs2+iRG5hW4splkkDTECQKAMAnlicdwBXBW97qNk0z211JG0w2ytHjLDJPOR6nqOahjZ1bIlkB6gliSMdMEkniulVraHnvDtu9zb8ZRtH4mnyh+aKIgMCCMoD0IHT61h7cqe+D0Ip1zcNNKXuJ3lck5Z3BOB05J9KrteJGw5GMY5dQAffk1zzTk7o7IyjCNmy3btAl1At07RwFwZGjGWCA84z37DPWqE8wuJ3mJIVm3gMQTjqOnGAKgvLphKwSVGG3JK5wuOQCSAc/Snwov2e2cr8pjwV/H1zWig7HJWxivaJctApyxdd3OF7e2cmimpFHuQgKcc4+nNFS4HM67lqest4dMOnyxRRWVpuIIkhcbGxkkkOCVIPAAPXtWnbWrzQBRdSRhQocFElCttAw4BJHuNx57nrWeuqobfabsCEkb5WhF0WUdFZCEAx2YHPrmmS3Ph6TBurnW7tScmPzEijOe2I1zj6kn3qlTm9WYOcTO1TT7WwYzFGEmGUGSNY4mY8/u4y7Esc8k4x29K5oxyMqIytuGM7hyTjJxj375rsmvvDqIUh8K+Zno8165xznjIJGeh6VHJfaY5APh6xjGcbd7sRnpyGGAPTFN4eTR1YfMIUlymJY28cVqgeYhmGSpU8knr09Ko3UaG6cM+VBznPXP09DWdq3jXVbHVLmDybPYr4VWiIG0cjHzZ4rNTxVLczoZbaCMMfnaNSOTz0JOetcv1aSkex/alKUUdNGgHXaM4Py4BOfU49K3G1OE+FEsTLKLtZN6kZGctnJIIycfh7Vh2JW6tBNxgswIUEg4JA5x6VO0ahSAQOOhH8uc1uqNRbHNLF4ectSszTSMFaaVk/utK7D9Tg/iab8iKcckdxznHrjNJticYLquecMGI/mBTHEIuUt3uoUkZflViAD6DggA455PTvWTpzvqjphiqNrJkgmi8+My5EW47gMZweSACeuOOa6Vtf03SrI22gQMryAGS7kAL5xxjHVh0HGAex61zYsQAW8+Dj5sq6445yOTnHfFW4IoI9JvLt7iBIoXSNjwzsWxgqFJJA7/ngUoqa2Qqk6E9ZSOn1DxJFbWelyWs8cpVAtzC4JDJtwQwIOPm/wDr5rM1e90C7tfMttNjh1GQ8sVxtHcjHHI4H8q53+0raP5tkxTB5WLOcnjgnJz1xim2+qQXdwLWFZBKyk7Wj2hcDJJyc5PY05Ko+hFN4aLupDXKuxA8v6HNNywPCoSTjDA4b1B46Vd+zvHkyNCozgFpFUHAz1PHNNCGQDZNBweB5oIyevQnj3FY+ymdn1mls2dZpWoLDoX2U7JnZl/d7yAyDGckg4yOeufx4rVh1mQX3nSrstlh2xIg+8QepBHBx27e9ee2eoRQyyW0dxFvHzeWHGG29cNyuB165rc1y6bSdA+2tqNlK6uFZYZ1d2zwCBnp39qpKp0M5Twz1bJJfEM8mlPYSwpINuyKVsExg8HGc844Bz+dYLI5OW5YnPBH4fl0rEfxXCSxMUpY5JwoGSTzkZOPpUR8URdopfzAP8qcqU5asmGJw9PZnQbCWB+bcPQj/Go/LUEA9uMMT26cisI+KU727kem4D+lMbxOmCPsT88ZEv8A9jQqUkwljqLWrO3uNUtJvDFtpkSS+bEcuzKAp7nGCSeawXjR/lI6kAqDxg9jnPSsRvFAbObNvb9993vnhaF8RlmOy12nGc+bn3H8NW4Se5lDF0IO51CxQxIqExHbnPyjPPJ5JNP+QAb36kEZHBI5yOOK5I+Jpyf9RvIwMMwIwenQA03/AISG4KgNEoB6BmIGfUDNZ/VpXudDzOjy2PRbPxSllYRxQ6bC1ygx58h4Ug/eOFBOe+CM+1Um8R6pJffazdyBguBGPuqvcEHjBPvnHeuUW81GWQBLZdgwS0qMApxznJySPpWnEJAo3sPYgdPpkmulUqiRwPE4dvmZsavrE2tzpLKkcYRNoWMAn3ySM49BniqKgAcfjnIp0Uavzul9iSDn8hT1iUMN27r1zU/V5NlxzClHYrXH+q4UHOQDjI/UjtzUuqao2prZ24hMUNrD5KJ5hc4OMksepOPw6VFqkawaTNOhIdFBDMf4i4B4+hrK8Lm41PUJhOy+TChwrHA3EgYyBycZI+lUqEjOeOpSdyyuD1wVzjI4wR9cVeVCEAHLdDj1PAHPNZN0ZoPFL2Ks32aSUMkeVBMfUkbiAPlDHkiurWytDOACnks2AxJxg9CcE8+/SpeHe41mcDM8slufxJ4xjJPaozCG4yxyM4z2PrwMGt660+1jYbLZAo6HzclsDAzhicE1z3iO3uFs0nsFkVkJDqAVLZwQ3PHAz+FT9XZf9pwCzkl069jvLcgSwnKhskdCDkA5IIz3p95Nc6ldvqE6r5kpzujUhGyOgBJI4FcNNqV+krI1zLuQ/dxyGGDzgVqLNc2WqwwXV+z2rFX8xWG1kbo4A6YPUH0q/YysYf2jT5uax0ggPOB83TIyRnqR0o+yvngNn2z/AFzXN+JJbuw1P7JHcOqRIvMbEB85IYZ6g1jfbb9j81zP9Q55/Ko+rM0ebR7Hbvay5wCx7kFRnnoRxzTfs84ZSBnacgsuMcexrFkkuDoVsRLI9xKcptZtxJyAvX1xWW80keoGJbyVoegZnYAkdc/Q5FUsPIh5pFrY7q+vdS1Vo2u3EpiBVcAjg4HOAfQVXFvKADswAOTk4/EkVg+IhJbpaFGkTMeW2vzzjrgkViQ399byb47mUMCDyxbr068fnRLDyYQzOMdkdxyUGDHt65yMD8cjNL1HDfKeoJA4rm4fEuty3EcRvmG5gjHy0O0HrjC+lNbxTrSPKoulY+YVLtEhLYyOOMYwOoo+rDearqjs4L/ULa3+zwXUsULktsQYGehIJHBwO3HtVGSNZW3TZZvu72YseSMgsSTzXESXeoXEpklmmckf3un5EVLbfaJpURjNtYgYLEAA8E9ewpqjIhZlD+U7Rrh/sv2Tz5DbbwwiLtjIyAQCeg544Ht0qIhFXJnZfqw6ck4yprl9ddo9UcB2UEKMhjzgYHfjIFX9ahm/sDT5CHTYCjsehDDIz3P3f1qnh29xRzRR2ibCxiQHZKP94YOQe5xjmp4ri6tVkFtdSw+YoRwnG4DPU9c89RisXwxuNtMSM5cAkDpwOOTW6Yyep9uKccL5jea828SpIrFy8kpZ8AGRmyxB45JPOcfXipIpHgwFkYrvBKH7p6gEjABIyRnrzWfrUGxIpSQMAqTyOhwOc9eTUU37+PTlXzkZrIL1IL4kcEjJAJ7cnHHWm8K+41m0F9k6B9QaSIIEWGPblljGFLA4OM5PTriqTT2wbd50S5PA3D+WeBU2o2oTUJkLiRRsG5WGARGuM4yAfXGec1jahorXhV4SiT9CpA+Yeh9Kn6rYuecN6JaGn9rto1bF0iqeDtb72M4yAcEdTgiiGSG63i2dZWXGQvuSByfcGuRuNMvoGw1jIAMgbF3Zx34yfzpdNvL7Rr5LlY2RgChSVPlbPVSCBwfbn3pqgnuYTzWS1SOmupPsrlZl8s7tgLK2CeDgEDBIB5weKrDVLVW2GWPfkjAyeRzzgEfrWHqWq3eqSq1zM2FXESISEXHZRnoT3zmorOOOaU75RCiqXdh0yACe2MnIHFU6C6GSzWozov7Z04jm5TGeOG5788etDavp5yTdJ6dD/h+VcgYzjkYPB9uR0pRG2MgZ+lSqKRf9q1OqOrOraf189Sc5yobHHsQB7/WmHVrH7wbjoRgkY9+cn29K5Ug4yf6n+op0Sk7jzxzjP4VXskhPM5vodOuq2UrEJ5rsAWA2jnH4jtULa1Zf8s1n45Hyjgnv161z8jshLJkMGznvxx+tC4VyMdyQPrz/AFpqloZyzGozb/tyyztCS9cAbRn6AhuBT11iGaWOFYpGZm24OMkk+x5444x0rnI0IyQvJ5xkdD2PFbeim1t3Eryv9oGdhKgCPIPQjOSaFAj6/UaLUmqpHdyW6W4by3ZNyvw3J5GVzj8ahuNc2MV+zfdOCTIc/lisdZGD+YSVzwA3YHqOK1rizj/4Rzzin77du3tndjcRzz6AVapoh42oyJNZkkcgRAZUjljx7jA7Uo1SZkzsjbkgjHPX61lQ7t/yqSxHAXnOewq7Lo1/GwZFQkrkAZzzz34z+NHKhLF1C22o3BTcfLz04U5/Hn+VVP7Uu2fZvXPUhR/jVdUu45dk0TIh6nb+HWot4e5EIiXJfAIfqenX0ocEJ4qo+pufaJmXPmkY4zgZ9KQyS/ITK3Q8nGDzjpTGIC7CuMdRnJ6+p61NIFKKw/hyeSOMEnJxScUHtpvqJIrNECCcg9TxzQykxMQWOOpFL5oIVhvKsM8jjI7invGgPzKuCcHdkjn6Gk4oFOT6mS8Ik1F1GCOMj+6AMnPXP4ZqtcRBLoIG68AgD+oq4sqJfSzHCtGMAKPQYwM9AfxqpNIWu1dwWJVTtzjcDyB04NaRSsYSlLuWZ4RHBGMfNknkdMD9avWG1rOEsoZhxu5yfb0qC8cTQRhAQiqcZ5we+T3x0qSxbNoFLFeSd2OBzgVMn2HHuzQWEHkK6t/d3AkEegGOo560UkIQuBuQA87l46cDAJ7DiisXc3TR2fkIGLFQWPBY55A6DGcfnSkBV+QhB9OT+RFOyc4wBSNg9Tn3r00kec2xFYAbQ+M8ZGBn8zTwpJ2BiM8YXH55NMGOMLkeuBj9akVsDAKj6fNj8T0ppIRwPjKBo9WVyp2yRK2WxnjjsO+Kw4uJI8LznOPocV2HjiESRWk4zu3lOuAB2z2rnDbKLm3UbQPLUsR/ePHOfzrlmlzG8Iux3Ohqw0lMFuc4GRg4JH64q0wO4ZbBx0AyfzNJZQ+RYxxhc4TPHuMfzqTcRyf5V0waUbMya1IvLBUg5Psxyf5Vxviy28rUFl28SRBjkdSPl5x24rt1ySWP1/Cue8XW6taW9wMhkcoR2ORx+tRUs0VHmMC8jX+xtMnTgnzIiRwOGOcgdc+1dPptqlvpNsiHIwSWKjcSScsCMceg6+9c9NtPhnTl3KXS5kLL9ckcdTXXwAQ2MYZ9+2Pl1B4B5OAOelYqyL1ZVkARsFhuZtp2g4HrjnPPrmqe2TcWjUq+PvAHd0wQScn8Ks3GpBIneGC4kUIXyq7VZR6Egkn2IFVdLvLzUkkZ7Rco+0ImUXBHBBLZ3evUfQ05VI9CoxszntWZ/tZUy7hlcjeCBkYAwc5Iqe1sJPsaMqKiupIbaSWBHPQjbgU7XlnbUd08XluUUFRGI9oXjIBHfrnBzXoF14L1HR/AbalfvaLALMbIzKQysy4VBgDJJODk1GhXM7nnOlq7+cqxDa8bDPcAKRtxjGCeScZo/s19wcRAEgfMFA98gD1984rb8M2ZL3DtnJGwZGOvJOD04rdFsjEnHbPbFNRHr1OI/s5+TtOTzx0560wWLdNp/Ku7+yoTjaDn2zR9jj6hRn6U3ESOGOnvj7pH4Up02Qjhf0ruvsqE42qPwpVt0zjCnHYClyIbRw8ekyk/cP5Ul5pr29sZHHHTI/L+Vd+tvEFyAPyrB8YOlvpcCLtEsk+duP4dp4/PAocULQ53RLMXt7IhUELED69GH9Kk1y1Gn3UUe0BvLD9AQMYIyD3PpkVp+BoyLu9mIO3yQg4JBJYHj34/KjxZFKNft7j7M0kbICqD/lpt4IB5we3INCRN+ho6KrXOkw3Lu0srsSzliWJGO54NX/s2MZHtj1FJo+xLEBUmQ7ySkqBWDHluBwRnoRjPoKu4BOSeTWisIriHavXjsB2qVEBABwfzp4wOPWpEXJAx9B0/wodikY3iSPGgTsB025Cn/bAP6c1Q8Eg+TesAmC0YJKcnGTkHIGMegzWr4ohd/DV06IX2KpcoCAoLqMkY6enWovCXkLYRNGwVTFll8wFi7HccDbgKdvAJJHrQTLc57xE0ieJ1wD5oEZQAbj6jggjP1/KuyjhkaCJwYwWTqo4bAzg4BGOvAFc5q0NvL47hileSON4Y2LRgqVYZIY/e5BGOhH16V2s8kceQhV+gJACLzxxxnH4A0nsEdzL+xPG2UIUjqyAZBPX6UxkkViDM56g854Ix3yKtmRn6ykL/AHVA/UnmpIEidtrMQgBLBcAcZPIJzk1DNLo868UqY9aXKx5+zxklVKhic9cHk9898dqz3ug06Tm3hRcZ8sKQjY46EknPfkD0rpvHdvD9psHjYIZVZWUAHaoKgEY78nFYjW41PWjb20EvknCrEgBdYgAWI3EAcAnBqbEPc2NTtkaW3ItIAwt4wUjDsqkZJALEnbhuAffk1lvYqcAwgD1XP+Nd9c2sN3PJKhUbm4USAnAUAKcd+Og4qL+xZZWKx20knOMhWYfhhQP1qrAcCist9bxfPsWVdu1jxggnGO4+tLPYNHLI4UBd5JVVJHJJJJJOOK6GLTi3i+e0wFEDuWV127QVx0PIOWGK1Z9LKWlw5Rf3cEhwDhiAhzkHv0oEji4reW+gZSSywru+bPPIAGc46Nn8Kg/s9yWAXnGBwcNjOB+FdPoNutxo7sADvkQBW42lFIwcdchv0q8bBN2cCTqMtnI49BwBx+lNIq5zEMFvZXtnMEVWLRl3YFgp3c45AyMDqCOe9EmnWk1wzruDu8gUNIAmAxPde+D0I5xWjr1gkEEDoCsRPbLc5ycg46gZ6/w1HpVuZroylfkiAI4BUls4wpB5xmsWnc3VuUprZHdjbjPY1etbMjnafQcevB/KttbVd3bPU/8A189PwqQRBSMYxXQo6GN+xw3iRTDfuD0aMEFvQggY/EV02uIJfDDuFJHlxnBPAIUDP4Gsvxlbj/RrkL8zhoyO3GCuPzNbd/5R8JSbm/dG2UEkEbfu56+4xRYlszfCELSadcEnOHHoOnUj/Pat1ojGMhWK5yeDxnofzrJ8HxGPSXeQ/MZjxg9ABj863mZ0G4MQUGflbnBz0BGKasVEx/EEAOkOVAykiEkkHjDZ/A5rAcyNplruB2x+YqHcT8oAIXp0Byf+BV2F0FuNOnt2OEkhYZOPlH3iAcdCVFcbauZRaQuNpaTfjoSrbQc5z1we3amrXM5p30O0uoVM7GP5YikWwADCjykJ6AZyc81W+z7iThdxxyRyPyNaMzRE7SrAjKK4fcNowAOg7YqthQxHzDj6ChJGqbsRKkyAqpBHf58fljmsTxLbSDTkcruzIACx578d66NV3N8gVvoAMfnVPXrf7Tok6HzPlw44GMqeCSOQoBPTnpUySC5yWmWccui3kzRlnRCquCcqAQc8jp82Ki0+0NwZh5Ak2KBsyccnJIBxnPFdD4dhEegX/wA4Zy7YVeQRtGCc9zyPoKb4UsVW3uZSXeT5QBs+VQCcAMTknnHQYx3zxCsKzMtrGIE74XX28wKOfQFT06darNp/XbEwH+0c/wAgK9Ajh8t9zBfmH3WQEHtznJPI7YpZbO0uFyqrbyDqrA7D+PUfTB+tCSHr1PPP7Mf+4f6/l1qCe1NsqZGN7EDtwOa72TTJUwRAzL0DRoWDewIBP51heLbGa2tLSV7eVA0jjMkZQjKrjqBTdiWc9b2L3MRlRGOCRkDr3NVcFHwB84OOfTvXd+F9Lnu9EikhtZmXcwJVCVYBsnOOmOmf0rlbqFrfxRLCYzxcsAj4QZLc5znGM+9ISEbS3DYK9e5B/TFOGmzAPnIXHUjniu2k0uW3LEqrIr4LHgtjjOCc9s9BxVcWrswREJckgAADrx3OO9OytcNeh56oLsqhOXI4HqTmumvEuDpU8RUECMnjuFwBWZaadNba/Hb7Gwk55YZO1CQSefXtXXSQkbwy8YKE+oAwacYi1OF0tguqWhYcLMOfqSa775SuCBnrg8j6kcda4nT7OSDVIXljfylm8vzGXAJHHBJwcGuuOVJwp+vrRFDRMyrxkA9wSo78HrUNxbrPAybIx8pAby1yMdDkDPWnFhjk8jigN8rAc8Yx9R9RzVNIFa5zIXzGAKHJUkjPI55xn06U+5t2hLD7PMinjcSMfjgcD61CjSBkQjJLKNo5bg4PzAkdRXVTr5kTJyBIp+6OTkkc57ioSTHc5lWaOBsEnPOM5HHYVUXVInUK+5GYZ3EYH5jP8qtWmbuZFQAlzkqSABwM55xj6Vpz+HrSZiY1KdsKc4B47+lJxDmZyzMHlmZTkHOD69/5VEVy7NnAD4J6ngdsVcvbM2F89sSXC7TuHXkA/pUCxMrtASTIWKfVgcHFO2hHUtLG6wICjDI689+tWdNb/RFDqSPccZzms6S21FRy0megxnt+FWVM9tp8bKhLAkHOST79Rj8jWbiylI1tsbJuKrkA84yOemKKy01Q4KvFIPcYb+WB+lFTys0Uz6NHw0UKc3MvrlX5/VaqSfD2NGYy6rDF6bipI+uSAR9K5tNb0xx/pEmtTL3DaoMn2wEBxTn1Xw2SP+KYa4Yd7i/d8Z/CuhNmFjbbwroFqn+l+IoFbuUlQJ+AwT+H61i6vB4ds7ctYay2oXAP+qgUHaB3LEqAB7An2NEfiawtFP2PwtpcSjlS8Zk/PJ5pH8c3soxBpGiKg6N9iGR9MtgfkaG5DSs7mVoemyancTX2o21t9liIjSC4uVRWYcknJySM8kYyeg7V0Fj4e0STWpjaJYjyoUUG3zIgY8kPuB3HuNoGB37VmT+L9UnXEkWnR5xwtnGd3pwwOaqRaxe2kpuLdo7Zw2N8KLHjPJB/hH4Vk4u9zqjXSVrHqtl4D0JIw0ts9w2MjzWJC/Tbjj2NXW8GeHQoB0iLHXln/wAa8fk8ba9dbgurTyHo+0IAuemcAY+ozWe2oandsDcalc7epWO5fdj2bGP0FO0jnlZu57S/hrwraqHksrVAD/HIVHPuW9e3rVLVfBukapbLE9nBBpYUSTuQVaQdgGJ+Uf3jgGvNo9Vto7Brb+zLeVWGTJdSySu+DkliWAB/3QCO1a6ePNWgtfsyQWYhIKkyRO5IPIzucg57560teoGp8N9D0m5+1rbW0E9lZ3bvA7RKxy4zwTkkAHHIxj3r0SLTbCABYbK3QIOAsS8Y+oz+teKQ+Ir+1vXvLcRW8zdWgUoCTz0B2j8qsz+NvE0iMn9pTI55wFTOPb5cUrDTPZmsLUAg2sOB0PlJk9jjIrL1K60PSrZ5ngsWdeEjjjRnYn+FQB39egrxx7rUb0LLNrWc/wALXLqc98gDH5VLZW9pBcpPc3lmxXjc8Ekr4PUDbIhwfUk0ctgNG80q08YeKJDrcVtEQgZ2ScRNbxqMIqMcg843Eg5HQCu51fRtG8UxWmifuLmGJMtickxoBhSApIY7vXp1rnrbxH4QsoQg0Z5nxh3aAZJPHAZmwB0xk49+tV7XX/C2mXr3OlaPcWryHcwiYRq5xjJGTjj0NDi+gW1Ogb4W6TApFlPPAhO4q5DjIGAQSAc+xJHtVST4XlR8mq9Bg74MnJ57NzTpPilHsymks/YZmA/Taf51m3HxN1UtutrW1iB5O9Wk/QFTmhJjOZvbGW3vpbRDJLIh2MQmAc8AhVyf5Vqx+DdekgSZdPk2uNwX5dxzzyCev5U4+Ota80yJFYI7nO+O22sT6klifzNNfxl4juW41UQZ4K/KAT7fKcfnVq4XGf8ACH6+vP8AZU6g/wB7bn9CagPhrVY5Nr2/l45ZnYBB+JAI/I/Spl1C4usfb/EjRN3KlnI+m0KKJG8PxgNe67qt5ntBEVJ+hZzTuxXZS/syaLl5baLnrJMuW+gAOfxxXLeKbOW/hjtraOGZoFaWSeOQt5eFOI9xAG5uuACSeOK7mBvBJlYXFrqUqFQVEioSxx6gj8Oas6Zq3hHRyDZ6XeXDBi6G5C7Vc8nAzgknocZHak2xHn/hSOKy0hhLO0UkkhkZHilyoUELtCowwQc5Jz7VW8VOZHtrqJ3Z7fnKpIEBJBydyqR0wMZz7V67N8T2jBWx0mOIH7pMm4fioUY/OsS88eavfMfOSx2dwtsGwD672P6UK4GRpOntqtq0ugxS3FkrMQxY5U8ZBJySQe/APYVqJ4S19/8AmFTnHQ/KBz+IqGPxfrNvEVtZ0t0yDi2hjTJzjkbT/KqNzrmq3jH7RqNy7EdS5Bwf93A/SnqGxcm0e6tWZZ7G5j2/fDGMD8MuDVRvKjOJvtMK9QZIEYYHX7shJrPkJkYGTLsP4n+Y/mcmmMqgjCrx2A4+mARQgMDxXrKzMtjDK7FHxIzZTcQcBQoJ+UZBJJOcdq6Lw7Y2mmaPGhvrWKeX97ctIWzkAhV4Vh8oJHHXPSo1gWNiUjRdxztKqw55PUEjp2NJkgnYMHOcoNq59h0quVit1Mrxc0Ft4gsL6wv455goLuiMEVlYFVLEguD1OAtenaHbeD9U8N2Wp390bW4nUNKpmGUbuOAf1FefzRpK+141IzjD44z6HBzU8EFvHjLPEOmFtwwOPcMMfiKTiw6ndTyfD6DAS7u7pgfmEUbsePUlQDWZfax4chgzo/hy61CfkD7XIsKx56E5bLEenHHesHdahhgXnTJMdyUQ49VIJx64NL9ptVYb9I06ZiMK0qSMcdePnxj61PKMw7rRZ9Z1P7ReLZ6ejAKkf2hRGigHA4Z2UAnOeSfbpW34e8EwtrEUN1qNt5ktxGyCOcFpVGSVJxngLnGB2znNTyao01tJbLaWECONreRaIpIPHBbJz75rFSxtobn7TFFsnA3iVc7lzx1B4PvRyiPonyNFtIgrxadCB0DBBwO/IHNYvibxro/hnR2uElinlc7IYIGVt74JAO3JUHB5x0rxf7RNCMo2HYks5y5bHTIcsOc9gKiaQSymV1xKOSyKqsPToAB+FPkGbfhPRDretza7rtxHaNdEtdLdupaZSQRgAqQAQuGOCNvQ856TxJb+DILG6htdZZL0qUQFjIm4jkkhSCuOpBOPfpXBMVLF3ZdxOS23IJ9TkEn8aPkGApKgdFzhT3OAPbsMU+TzEReB7F7TU59L1iGe3tJOUuUwVQ4JBAJyyMOdygnoOO3dy6P4WOWXxLjByzPaSEA8egAI6Vw7qZJI5gSZIlKxybyWjBzkKAQAOe1CzzDg3E+303tjHrgsR+FLlEVvFt7pS3wtLK5lu40JHmNCUUseC23dkhR0zjOe2Oe28O+B49X0wzaNdW32dcBnbdudiASXyTtYccAYxjk1x01vFcpsmiRlJGV2j6gk9Tn0J4qa132NuYbSSWCIvvZY5XG5yMEkhsnjA9se9Lk1uXz6WO0k8CXNvkPqemL67rkKR+YNVpfDsVuD5ms6UB6/aW/kEOa5F+WJdVPqzcn8zzQI8fcG0eoIq0mS2as1rby3KW0OoWNxvkUeZGxZVJOFB3KMHOemfekvYYLS1e3uXzKGMX2fyHbDEkYYsFUDPOQWA49azFjLEMOXHIzjjHerVxdXd8qrcXM020YAeRiFHGQATiqEdFHp+m6p9ltdOmgs3hjLv55UBlAUEqQ3JDZODnIbIx0qs+l2cTMjeILBmXjEaNIB3GMcnrWLY6fZ3N5HHdT2lmGPNzOjZXHTAXILDsOB1yeMHeltvD0e1JtYv78YwrWVskAPsGOCBx3BrNlGLqYtYbVki1NI3mQlZZLeVETBwTkq3zHOFXGDzkjAzX8K+EW1OWe8sllniiJETtgP5gC8lTnCtuYAHJytdCt94ZtGTbo2pahjndd37kKR04GAT9QRUWn+ILHSr+S90/REhkL70Vrp5FVsYOAMDueobr7VNncuMl1LaeDNadf+Qdeg8DJjjOMded4z+Q6VK/gnV1AP2K+OeyWyE/iTIMU6T4n62rMiQWUYJOCsLZXPcEt19zx7Vk3vjDXNQj2XGpz+Uf4FcKPxKqCPwOKpXJuXm8HatEMy2I68Ca5SA/zb9ao6loqrpl1Dd3dgiOhBU3DFlA5AyqkHkAdKynaSbPmSySfLnEjFup68nnHoetdHYQeDtP8AKmu/t18+MmJYhHFnryA2R+eKbFc4fQba5bwvq167lA8aJCEAeSSZiRsAAJHAPJAH8z0Pwyk8Oi4udM8QQxrLJKptzOXjLZAUgYOMAgdcHnvmugj8WaNo93Jc6J4ahtp3GwztIcsh6koBjtjrmsTVvEV7rE/mT21lHtwcQWygDB4+ZiSTx2qVFj5j19vCvhi2DtJp9tHuA3F5GAwMjgk8Y9qoO/gPS3VmaxVxwAC0nPtya8duLy4uc+fLJKpOcSOzZ98EkVErdQq49geP5VSpiuetz+O/DVixNpYSlyP+WaKisPqDwPwzXk/xO8RXfiy80xLa18u1TcYkW4DFnJAJYsBg4C4BBGMHOSQGBc8gnmnGFGyWSMsf4mQE4HTqKfswuaWleKtV0LQrbSLW6e28hSCFVGLHJJB+XGckg5J6Zz2rldX0G31O8a8tb947i4kLzR3vQOx3cOoPG4kDjj1rZEK7TgZyc5qYWykEAAA9V9aagCMq2TWdPRLfWbZwpwkdyXjdsHoDhiTyD83UelWLu5eK0lkEXmEKT5YwSTgEge3PXr6Yq21tH8o2xjZyMRjK5+o4pNgUjbjOTg9P5U+XoDdtDjtE+1R6tuk8zy9jc7TghjuGc8kZJOTzXUeYwHBO/HG4ce1TlGBJKkE/rRhiMFapRshXOKvNG1Ns5l88FtwQOSCSSTgHA659K1LDULu3jS21CylxGMCZBk46jIGc4z1H5GugKjGAnakjPl7QAUx2/wAKmMBEJVASCOnH5VQ1GzubmLFpKiKOXy2C2eABwc/55rVZFbIKA89BwaaYTxtP/AT1/OqcRJnPWfh26tbyKcyJ+6YOEUkqwHJBxjnPtW2plCjHlluo5OCcZGOD1qUrKD06c9envSndgcZx6cYpKAXOLuIZ7TVcyWziNH3iJSANmc4DYwAB+NdZBKl3AsyH743YwQRn2/r+FWSFkADKCM5wwB5/GhQUOR9McD+QpqIXOY1yzne+V/KkClQgYJkHHXpyOfXtWaui35YzQgMWY5UHL8HORnAH513ySMqkAEbuoz1z60m5CNpXAPbAA/Sk4Cuc5Z3sgVLe+tXEqkBW2HtwMkcH3OfwNaElhbyrgpn355B+hFaZXcMHGPTJxTPJHTqp6kUco7mHNoUDcBR9RkH9TRW1s28A/nRRyDuj39NLsIW8tLG1A9fJXP6inRW9lLGxWxtl5HBiU/0oorlbNEQ31vYxQv8A6DASSF4jQfe7/d7ViQ/DzRiPOmkup9x+ZZHUA/gqj9KKKaEx1tpmmWl9JZWGmWkJRdwkmQzHH/Aj1+h/CtW30jTLmQST2UUzqcKZUUgA9toAXA9xn3oooYIvjR9MdFJ060wvQeQn+FI+haVznT7c84+4BRRSuyiJvC+hg/8AIMgOBxxTf+EZ0MHA0uAAkA9f8aKKYFHV/B2m3NoFt0W0YsMyRLlsdxycViQ/DbTpR895c4H93HPPvmiimiWWofhro6Nue4vZOehkUDH/AHzWrD4Q8OxQrGNKjbJ5Z2Yk/rRRQwJD4L8OMedJh5GfvP8A41F/wg/hkkj+yIR/wN//AIqiikhoU+BvDm440yMYHZ2/xpi+CPDign+zEOD3kb/GiimwOc8a6JpWh6PHc2WnQ+bLMIz5jOwA+m7rXAxbWIIBBPA56fpRRTQjb0Tw5/bZfbd+Rt9Y9+f1Fdda/DbSZI/3l1emUKGZlZQp9gNpx+dFFNgWR8NNE8k/vr3GAceYvp/u1IPhpogEn728+Xrh15/8d4oopAMf4caKACJr0Z6/vF/+Jpv/AArfRCAvm3uSevmL/wDE0UUxkn/CutFt8O73U3AOHdf/AImnp8PtDCF9twVDEbDIMfyoopCZIfAOgkMwgkGztvODXn3jSxtPD+spaWsI8polkJJ+bJ7ZOePbFFFNbiMixQX94LYfu8/xdf04ru9M+HNrckNeX8zh4ty+SgQqffO7P5UUU2wIvEngXTtI0Z7y1uLkyCVI8SMpXnv0B/WuUtdHFzJJGJyu0kZZd2f1ooqW2I6uz+HCSQxzT6mWR+NqQbSPxLEfpV5vhlpDJ8l5frn1dD/7KKKKYEX/AArKxYALfz5Hd0VqgPw0t92P7UnGG/hjA6/jRRQB5osocEgEbpmjPPORnB/+tVnTbVNQ1FbTiNgceZjP6UUVQHosXwy09CqyX1wzsOSoCj8uatH4W6OjAG6uyR3BUY+nFFFIB6/DLRh0ub0D2ZM/+g1Sb4Yaal1sN7c7PRQo/oaKKaAtp8LtFVci6v8Akngun/xNJ/wrLRdm77Rfc9vMX/4miikA4fC/Rlb/AI+r4/8AA0/+JqT/AIVpo6jH2m9P/Ak/+JoopgVm+GulG8Vftl7guBgsv9Fq7F8NNI8tAbq9Oe+5P/iaKKTASb4XaOI2K3d+PpIvHPb5aZF8MtGKZa5vjn1kX2/2aKKkaBvhrou7b599xwD5q/8AxNRyfC/Rh925vFwcfeX/AOJoopgyu/w003zTH9suSPUhf8K8/wDEGn2uj+ILrToRLIbeSNGkkcEOG9ABkEfUj2ooprcCvHGC7RjgDn8auRWXmxJJvx5nG0rkD9aKKYjX/wCEQJhBF8BkZx5P/wBlSHwgDIEN6pHUfueO3bdRRSW4EF74YFnpD35u96gjEYjx198np9K551CeWMk+Z0yen+NFFWgJTb7bT7RuyP7uP61HakXBIACjy/M55/CiimwJLZftDqq/JnPJGacRgdeaKKoYjMVGCd38qCNuWPNFFIlj/LLLbtkDzunX5f15pmQU3YGO4oorSID8YAPXNI+AOmSOmaKKpbksaY1Jyc5PpUeNrMOuKKKAFHIzSEAxlsYIoopMCMMPK8zaOTjFOAGM4H40UUgFIwwpMgZwB+NFFMBN2O1CtuPTFFFIB45bkUUUUCP/2Q==" alt=""  style="max-width:100%;height: auto ">
				</div>
			</c:if>
			<!-- 底部新闻 -->
			<%-- 	    <jsp:include page="include/index/news.jsp"/> --%>
			<!-- 底部工具图标 -->
						<!-- 底部工具栏 -->
			<c:if test="${not empty onOffToolUtil && onOffToolUtil eq 'on'}">
				<jsp:include page="include/index/bottom_tool.jsp" />
			</c:if>
		</div>

		<!-- 统计代码 -->
		<div style="display: none;">
			${accessStatisticsCode }
			<!-- APP独立统计代码判断 -->
			<c:if test="${isAppASCOnoff eq 'on' and isAppTerminal }">
				${appAccessStatisticsCode }
			</c:if>
		</div>

		<!-- 红包开始 -->
		<c:if test="${onOffRedBag eq 'on'}">
		<style type="text/css">
			.red-bag-float{position:absolute;height:110px;width:90px;z-index: 999999;right: 0px;top: 50%;}
		</style>
		<div class="red-bag-float">
<%--			<c:if test="${domainFolder == 'b22102'}">--%>
<%--				<a href="http://www.917550.cc/" external>--%>
<%--					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABuCAYAAACnQwS5AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAABBSSURBVHja7Jx7lBXVlcZ/3TSNICCPSERRwDEVo7a2clBQkUY0SDBRjFEiYjAVRzKaB2SZOGIEBNeoIzE6GuOjFGPE6BDAKDCKDg2OshwLacSolERgeATCI81DIPKaP+oruizuvX3ffRtrr3VX36pbj3O+2ufb397nVJcdOHCA2Apv5TEEMdAx0LHFQMdAx0DHFgMdAx1bDHSpWkWuF/Ack+rnk4Bb8tTWGxPsOw64I0/XnwrMz/Qky3ZzB9pzzNXAQst2/y+HDlwowAsBdPAwL8zhuiuALcDcHJytHBgBvGDZ7q6MqMNzzDDgD8BXc+jEcuArwInAY1kAMBgo0yeRrQUGAp2BhzK8/kLgdLXNANNy6Gdr4EHg8Yw42nPMMcB/aLNzHrxxhTzyijSP3wHUAP+V5vFbgB8B16d5/AfA14GleaKddsJyuOeYoZl49LeBL+l7WR5jwgyB2JhNA7KhqynAojSOm59mOzKxoAw6KhOgTwt9X53nBu1L45hdOVx/EcW3emCjvp/oOaZlUqA9x/T2HNM2NBQBPgHebWZKal+xb2jZ7m7gFW1uC9rgOaav55jKqEePBJ7S91eB3cAvLNvd6Tmm3HPMEbEaPsRaeI5pre+TgPXAG5bt7vcc8z0Fx71RoFcDV3qOuQxYAJxr2e40zzFDgLeBM/LUuOuy5N+SBBqY7TlmrGW764A+wN2eY7pIhey0bHd/FOggAt+m4bDYc8wtwMtAd0XqfNifgFOBf2+KYZ5n+0wOOslzzMvAWst21wP/DLQHFicKhm+K0HsD3TzHnAvcq99+atnu9jw2cAfwc+BM6dnmbHcAm4AhwL9p32UhlfV5oC3brQfuk5w7HRijnyZatjs1jw1rHRlF50pjb22OKFu2uxIYBnwK3OQ55hTh9xbw2ueADiIjMBmYA5wFdAPGWLZ7R57bVqsaRdgeUwb5TDMF+3XgYmAVcBHwEfB9y3b3eo5pEfbo4Z5jxlm2uw8YCjwPXGnZ7v2eYyo8x1zkOSZficvZ4q6rI/s3KlAOBJY1AV6ts6xz9PIcc6JluwuBvpJ5gyzbXeY5ZjDwQLiotBp40nPMfst2JwKeLlIB/BGotGz3tTx26mjVUUYA/xJRIf+tTzHtbOUOy7M41wC3e44ZYNnuciUveI45D3gReCLs0f8L/A24U0EwsHuBbwFvFKiDQ4A/AzcXGdhAQVXil3GPyhJkJH27AdM9x7QXyO1UDmgJzDwItGW724DndOLtwZAARkuCvVjATrdVAes90VYhM8ZpUjoPaZi/Kiebm8N13xcVVqmwFeQKJ4kZFkTl3WS5/cWeYzoBV2n/E5bt/rkIXlYFTBfg12TLmRFgFynQfgfoqr9HKyA/Ktqan8tNLNvdC4wPAJaw+I62Jyg9pyy89k4lvukKVD9W5nOhvK6VZbtrEgSDbKtcjdlWBeUpGWrtExJknpUaLbcAvfBnU67LQ8LUBuhm2a7nOWai2OB8xZinLNsdFZV3PT3HdLRsd4aeRoVIfIh09axIRa8YdpQyrLeANcDvVI85pZHzApB7AbeKHrYr+PYKHZOPrLRc3Hy+Zbu/1MNrD4wNQJauPqg6TgN+7Tnmh5btTot47MPKFtc1oVQ9TgplRGjfMmWYuxTcjgOOkfdWFaldO4AjgGc8x5xm2W6QB8zxHNNVRboVwA8Djl4AdFKB5AchkE8CbgD+Tv7r0qnsfzQMt6Q45qvy0PPl+UO0XVVkJ1gM9Ag7geeYKim1QYGQCFTHVgnrFsDjnmO+oXMukURZaNnu34vY+K3AXQpgXwfuVt1gUUiabdT2Ivwpr8dUP9lSZKBf198rBPLRknT/pIfwepg6AH4lpfE14EHPMXN1MIHo9hzTOtksbwGrY3MzlF83a3QW2irkqNNUI/qK9o/Dn/Ddq2LcnmhRaRv+XOFyATxIgbDOst0ZnmMeaYLEopStSun2To24cs8x3UVje4EbLNtdcEj1TmB/CJyn5KUGmA0M02TAKHl7bL51E0YPi+ZuVzr+LjDQst0pUfcPCPw0nfikZbvXeI7pbNnuZv32dKIH8wW3YAL2WmCSZbvPeI7pYNnuH4XZZUDLQMWFgVurp/KB55gbQyB3CnH1xhjfg7Yl5KxVYoR6zzH9PMfMU0CsP4Q6pCpG4U9b/dZzTKAFt+NP1ALUCfzuX2CAOwMd8WvOn4ZB9xxzl9L7GpUuXkvG0TNDefslyqraAPMCKeM55lIJ8lZZNLLFYQD0JPyJ1/XAh/Lat5XY3SZM56kgB8k417LdCSq2bFbGOBX4LTBTE483KyhmExj7pnlczywfSicOnb1JZNVZghzUfq5Wqv0H/HnC64RZIIW/ZdnujpRAC+xH8JcXjFO1az9wreeYjoqskPkK0bY0TF42ZqfIczK1yWk+oEvIbrlvZ+HREuhPw8LGK5QwnWPZ7g1RkKMJS7i+0RHYbNnuncCdof1dRCXohkHxpi5FkaZan1vJbGXqrfgzH7NClbwdCR7eNarYXazj07UJ+JMasyQEppJ8PV61OLkdDeXbrsAexbbBIYyOBjZZtvu5KmVZoleUPcfU4E+UrlDd4SXLdheq1roCOBb4CX4BfR1wD3B/Es+Zkydu7J6g/HkC/oRooa4fjK43gUv1QJZr1AxQ4Oup3wbiTyo8CdyZFtAC+2dKLQObDdwkD7pL3+fgr8/7m7y1vsiBKZ+VuqVK+aP2e2A4/vKLYJWrK+qYgL9Uop2OnQVcZdnuzrSoQzw92XPMFtVAOgDfwC/ADwvVHr6sv10UJKbrptuLBPRnFGb16BHAnlC/A6ooB/4i0KerTBHYo8CPLdtN9LBSZ3qW7T4l3nscfzXOMYq0N6r8Fy4wXaC/j6lm0lztWPxlcEdJdXXU/nqN3AuAHwjkf6g6N8iy3VHJQE5JHQmopL2CQg/gFct2N2iFqYu/lu5V3fwtbZ8pWmlO1kL9uFBADwd+gz/91js0eobhL899P933exoFWgtnThLNbAK2aKFN8PtgcdMKperPisdfBr6pBp9NbjPNhbQzJF+XqvL2KLBBnn0v8DMF+jGR847CfyuiDfBxMAmbzNItEn0Tf/3FBmCt55i5nmN+6jnmeAXE7ylTQpEYReL+opfngLElCHI/Df3jxcuBtl4s8CuVgIyRrLtK1PkX0chyyd9WOXt0yHO/K7XRM7T7Uw2tB/GL3Z/gT3stU4YWePX7opN/xa/dBoF4bxOAWyYq6Csl1UF9ugAIqpQD8RfGnKO+XAz8Un0MbLf6Pa4xb84IaIHdTlnQEMDS0NknsT9BjW+hAPK4htdxqp/coMsM1/E3yRN+VSSAz9e9b5L+flP0sAZ/duQNZb33KTk6XjWLGmCissLtodzieVEGeeHoNMAPS6Jyee7X5CVDgUfwV+wE6+m2ASerA1OVAd5TYJD7KkbUitJep+El0IfFwa/omHeUhL0tr93ViDJLqwEVOQDcSRx8rjzhBIG9XsHxI+AllRDn6TNAxZhJoqH9opK/4q/bQHWEPXlSEPsUoGcAR6o9g0Mg71A7OgH/CXwsLh6tjPCA+rNEamqB+DlzvsrWozWl/kSK+sI7ArS3OvyaPMdo+2RlXeeolnGmhuU4VQ4fyhJgo+A1kobJ3YsUD05XTKmRghotZztbfD0U+H6S625U2eG5onq0ZbtLPcf0VRAZJA84UiJ+mTxgNv47K/0UsefL03uL4ycDL4jLx0u9tMJf9FiuYJOJnSUK2CCQrxDIKJPboAzvT/LMPvhzfEGJc7tS7CoFyX9oe570ddYlhnxydKIMq0ZBpV5DsKcCT1AtW4m/JmOQ6KKnPHJmSCLOSrMpXcSrPTRSRmhUGSmhKlGULc/eLEpoo4C9TSWGJRk6XGE9OoVdpCh9Doe+3rxGgWg38AsBM1IVr8H4y17vUQBqjT/h0EuatTH7tUBGHnuWQF6t0dQDfwZkna4/VDQVtRUKkA/kU34WAuid4r9NkkTlGsYblMLW4q8QrRAfjhXQ70gd1KtKNgJ/Sv92/JWtqWwQ8F19/6uC3m3y7BmSoyjTWyW6a6+29RTlBYpos/7mV7wXkDo6akhWCrzVqvleIs7uoCi/XsftkYc/Kx79UNz9qSRjqrpzoHfRA/p9KHZ0kRe3kuysEC/P1oPvKJo7oJGzrhDUUUigo0X1+4Ark/y+XFzdXhLxHpVbZwiI8UqIElkvFbZQ9vYo/jpoS0H5PH0S2WIalvZmKwqajDoSWVdx7hrRSaX4b5s8+gNlZi9Ia0+Ut49S8LpGwz5R8nC9HtT9ooLnpDB+LiXUVvtP0EOrlH7fqlFUlKUTxfLowI7EnyxoJaDXitM7qop2qgLlOgXTPqKT9xTgEiUy/aUiBmroz9H1vyy6eVcSrUxgt5GO36xPTlZqHh0uQn0SCWI3i1/bRo5dqiF9vOoULVU5i+rmH4lulkhlPE/Dci3Eve/iLwp/Qtq46NaUa+kqBcypNMysh60Kf13bG+LRG6WpT9ZDuVfVtpfEzZeqytYyQbWupx5W56bqbEUTAv2Z6gx3q/zYVQFwT0hmbRAv7xF3j5UM3EbDe+Tb5TB98JdAfEmKpo3O3SRtvK0J+9qkQAe2T4Hv40aOO1ZcfEDndFd9ZIGC2y5xcUn+L5Dmsgz3UhomD/qJVhapdjKxOXSgOQB9m3j4WRWjlkiaXasi1BgpjXYx0NnbgwL6aqmLqI7+nRKWY0UhnWOgM7e7FACNEplk9pE093uq+rWLgU7f+qlG0UdANma7RSPTJQPLSq1DFSUK9Hql35m+Rnw//hrAFjTNDHuzA/rjHM5dFlPHF9hioGOgDy9L9mpFNYlX8Af2dPTNUNlIRf+Dx+FPW2Ua0KpD2wMS7K8j8taTLNruZPefly8AI2Xi0Zbt1mUSDDvQMDWUyGqT7O8eOa82i7ZXJ7l3ov3hYvp4peTp3L+mQI7boZRVR4/IKIjaU/jLEnocdtSBP5mayhtX5Rno8Sl+H1lgDFZmQW+prpU+0OKZASXsICvzfK0J2Z5cqlNZ6YyeDpFguDIC7MokgTBXbq3OQzCs0z/TLUmgo6OnJqIKpuTicRkE4HwokQHJKDeZvBuZJEAdlHX6VwmJ+DbKrzVJGpSpXU7Df2VflUdebdJg2D0JQLUZSqQeWaiF+iT7fxK658zDBeimppLosI4+2Lo837OW7EqrYR1fa9lubaZAr9LNUwWJ8UkSgWjCUJsjCInaMJ9mZsnk3RRgil6+n5fkmAkJoi4JgM4mkNVHsqzqNDPTkrVSLSrVRTz6jALSxheWoxNJr/rmDnSpenR9CjpaEnt09laTpsRrlvxcSkBnkpXF1FEEa5beXEoeXduIdq4TnYyOgc7NBiTg7DCdPI3/elvUyhrh+mYL9OVKUFYlmTMsVBreP7KvPrQd9v7+aWaamQbq/sUGulqfWgpb1Imqjsv1CVPNgFCbMgmmMzJsS39SzwDlJRiubMLRNjMP13gxi9FTkHul9GjLdld6junJoaXOZDr3aT5f8MnlQV2fAoRVkXuMT9LxuiRtDAffBxppx/w0PLpRKs359bfYDk8dHQMdWwx0DHQMdGwx0DHQscVAl4L9/wCxJPjlyr1JbwAAAABJRU5ErkJggg==" alt="logo">--%>
<%--				</a>--%>
<%--			</c:if>--%>
			 <a href="${base }/mobile/redPackage.do" external>
			 	<img src="${base }/common/images/redpacket/rt-ad-m.png" width="100%" height="100%"/>
			 </a>
			 <div style="width: 1.5rem;height: 1.5rem;position: absolute;bottom: 0px;margin-left: 39%;z-index: 9999999;" onclick="hideRedBag();"></div>
		</div>
		<script type="text/javascript">
			function hideRedBag(){
				$('.red-bag-float').hide();
			}
		</script>
		</c:if>
		<!-- 红包结束 -->
		<!-- 添加到主屏幕快捷方式 -->
		<c:if test="${not empty addToHomeScreen &&  addToHomeScreen ne 'off'}">
		<script type='text/javascript' src='${station }/anew/resource/addToHome/js/addtohomescreen.js'></script>
		<link rel="stylesheet" href="${station }/anew/resource/addToHome/css/addtohomescreen.css">
		<script>
			addToHomescreen({
				displayPace: ${addToHomeScreen} //显示时长
			});
		</script>
		</c:if>
	</div>
	<script type="text/javascript">
		// console.log('模板','${domainFolder}')
		function lotteryV3Activer(){
			$.ajax({
				url:"${base}/native/preferentialActive.do",
				dataType:"JSON",
				type:"POST",
				success:function(res){
					for (let i in res.content) {
						switch (res.content[i].readFlag) {
							// 红包
// 							case 3:
// 								$('.red-bag-float img').attr('src',res.content[i].titleImgLink);
// 								break;
							// 大转盘
							case 4:
								$('.rotary img').attr('src',res.content[i].titleImgLink);
								break;
							// 签到
							case 5:
								$('.sign img').attr('src',res.content[i].titleImgLink);
								break;
							default:

						}
					}

				}
			})
		}
		lotteryV3Activer();
		function getPopNotices(){
			$.ajax({
				url:"${base}/native/getPopNotices.do?code=13",
				dataType:"JSON",
				type:"get",
				success:function(res){
				    console.log(res)
                    var html = '';
                    if(res.success){
                        for(var i = 0; i < res.content.length; i++){
                            html += res.content[i].content + '<span style="margin-left: 20px;"></span>'
                        }
                        $("#noticeText").html(html)
                    }
				}
			})
		}
        getPopNotices();
	</script>
	<c:if test="${isWechatPop eq 'on'}">
	<jsp:include page="include/index/wechat.jsp" />
	</c:if>
	<c:if test="${domainFolder == 'a010201'}"><jsp:include page="${base }/member/a010201/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder == 'a101'}"><jsp:include page="${base }/member/a101/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder == 'a09502'}"><jsp:include page="${base }/member/a09502/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder == 'x00204'}"><jsp:include page="${base }/member/x00204/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder == 'a010101'}"><jsp:include page="${base }/member/a010101/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder != 'a010201' && domainFolder != 'a101' && domainFolder != 'a09502' && domainFolder != 'x00204' && domainFolder != 'a010101'}">
	<jsp:include page="include/index/noticeAlert.jsp" /></c:if>
	<!-- 侧边栏 -->
	<jsp:include page="include/index/common.jsp" />
	<jsp:include page="include/need_js.jsp"></jsp:include>
	<div id="weOther" style="position: absolute;top:0;left: 0;right: 0;bottom: 0;background: rgba(0,0,0,0.8);z-index: 99999;display: none">
		<img src="${m }/images/wxOther1.png" alt="" style="width: 100%;height: 100%;">
	</div>
	<script>
        var browser = {
            versions: function() {
                var a = navigator.userAgent,
                    b = navigator.appVersion;
                return {
                    trident: a.indexOf("Trident") > -1,
                    presto: a.indexOf("Presto") > -1,
                    webKit: a.indexOf("AppleWebKit") > -1,
                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                    iPhone: a.indexOf("iPhone") > -1,
                    iPad: a.indexOf("iPad") > -1,
                    webApp: a.indexOf("Safari") == -1
                }
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
        };
		$(function () {
            if (browser.versions.android) {
                //Android
                $("#weOther").find('img').attr('src','${m }/images/wxOther2.png')
            } else if (browser.versions.ios) {
                //ios
                $("#weOther").find('img').attr('src','${m }/images/wxOther1.png')
            }
        })
        var wx= (function(){
                return navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1
            }
        )();
        if(wx){
            // alert("是微信");
			$("#weOther").show()
        }else {
            // alert("不是微信");
        }
	</script>
</body>
</html>
