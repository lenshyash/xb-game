<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/head.jsp"></jsp:include>
  </head>
  <body>
	<div class="page" id="page_customer_service_jsp">
	  <%-- <jsp:include page="include/barTab.jsp"/> --%>
	  <header class="bar bar-nav">
			<a class="title">在线客服</a> 
			<span class="pull-left">
				 <button class="button button-link button-nav pull-left" style="color: white;" onclick="iosAnroidbarTab()">
				    <span class="icon icon-left"></span>
				    返回
				  </button>
			</span>
	  </header>
	  <div class="content">
	    <!-- 这里是页面内容区 -->
		  <c:choose>
			  <c:when test="${domainFolder == 'd00535'}">
				  <iframe src="${kfUrl }&userName=${loginMember.account}" style="width: 100%;height: 100%;"></iframe>
			  </c:when>
			  <c:otherwise>
				  <iframe src="${kfUrl }" style="width: 100%;height: 100%;"></iframe>
			  </c:otherwise>
		  </c:choose>
	  </div>
	</div>
		<script>
	var isLocaApp = false;
	try{isLocaApp = android.isAndroidApp();}catch(err){}
	function iosAnroidbarTab(is) {
	    var browser = {
	            versions: function() {
	                var a = navigator.userAgent,
	                    b = navigator.appVersion;
	                return {
	                    trident: a.indexOf("Trident") > -1,
	                    presto: a.indexOf("Presto") > -1,
	                    webKit: a.indexOf("AppleWebKit") > -1,
	                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
	                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
	                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
	                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
	                    iPhone: a.indexOf("iPhone") > -1,
	                    iPad: a.indexOf("iPad") > -1,
	                    webApp: a.indexOf("Safari") == -1
	                }
	            }(),
	            language: (navigator.browserLanguage || navigator.language).toLowerCase()
	        };
		 if (browser.versions.android) {
	         //Android
	         if(!isLocaApp){
	        	 if(is == 'content'){
	        		 history.go(-1)
	        	 }else{
		        	 location.href='${m }'
	        	 }
	         }else{
	        	 android.webBack('back');
	         }
	     } else if (browser.versions.ios) {
	         //ios
	    	 if(!isLocaApp){
				 if(is == 'content'){
					 history.go(-1)
	        	 }else{
		        	 location.href='${m }'
	        	 }
	         }else{
	        	 WTK.share(JSON.stringify({'method':'back'}));
	         }
	     }else{
	    	 if(is == 'content'){
	    		 history.go(-1)
        	 }else{
	        	 location.href='${m }'
        	 }
	     }
	}
	var picCallback = function(photos) {
		try{
			isLocaApp = photos
		}catch(err){}
	}
	</script>
  </body>
</html>
<jsp:include page="include/need_js.jsp"/>