﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/head.jsp" />
	<script src="${base}/common/js/qrcode.min.js"></script>
</head>
<body>
<script type="text/javascript">
    $(function(){
        var iosImgCode = '${appQRCodeLinkIos }'
        var androidImgCode = '${appQRCodeLinkAndroid }'
        try{
			if((iosImgCode.indexOf('jpg') == -1) && (iosImgCode.indexOf('jpeg') == -1) && (iosImgCode.indexOf('png') == -1)){
				new QRCode(document.getElementById("iosQrcodeCreat"), "${ appQRCodeLinkIos}");
			}
			if((androidImgCode.indexOf('jpg') == -1) && (androidImgCode.indexOf('jpeg') == -1) && (androidImgCode.indexOf('png') == -1)){
				new QRCode(document.getElementById("androidQrcodeCreat"), "${ appQRCodeLinkAndroid}");
			}
		} catch (e) {
			console.log(e)
		}
    })
</script>
	<div class="page" id="appDownload">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"  onclick="iosAnroidHome()">
				<span class="icon icon-left"></span>返回
			</a> <a class="title">APP下载</a>
		</header>
		<script>
		var isLocaApp = false;
		function iosAnroidHome() {
		    var browser = {
		            versions: function() {
		                var a = navigator.userAgent,
		                    b = navigator.appVersion;
		                return {
		                    trident: a.indexOf("Trident") > -1,
		                    presto: a.indexOf("Presto") > -1,
		                    webKit: a.indexOf("AppleWebKit") > -1,
		                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
		                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
		                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
		                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
		                    iPhone: a.indexOf("iPhone") > -1,
		                    iPad: a.indexOf("iPad") > -1,
		                    webApp: a.indexOf("Safari") == -1
		                }
		            }(),
		            language: (navigator.browserLanguage || navigator.language).toLowerCase()
		        };
			 if (browser.versions.android) {
		         //Android
		         if(!isLocaApp){
		        	 location.href= '${res}'
		         }else{
		        	 android.webHome('homePage');
		         }
		     } else if (browser.versions.ios) {
		         //ios
		    	 if(!isLocaApp){
		    		 location.href= '${res}'
		         }else{
		        	 WTK.share(JSON.stringify({'method':'homePage'}));	  
		         }
		     }else{
		    	 location.href= '${res}'
		     }
		}
		try{isLocaApp = android.isAndroidApp()}catch(err){}
		var picCallback = function(photos) {
			try{
				isLocaApp = photos
			}catch(err){}
		}
		</script>
		<nav class="bar bar-tab">
			<span
				style="display: inline-block; width: 100%; color: #FFF; text-align: center; line-height: 2.5rem; font-size: .7rem;">版权所有：2009-2020©${website_name }</span>
		</nav>
		<div class="content amyl3">
			<!-- 这里是页面内容区 -->
			<div class="card facebook-card" style="margin: 0;min-height: 100%;">
				<div class="card-header no-border" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<c:choose>
							<c:when test="${domainFolder == 'b01701' }">
								<a href="${appDownloadLinkIos}" class="external" target="_blank" >
									<img style="max-width: 60%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAd4AAABaCAYAAAAWyDe5AAAACXBIWXMAAAsTAAALEwEAmpwYAAAGkmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDggNzkuMTY0MDM2LCAyMDE5LzA4LzEzLTAxOjA2OjU3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0YWQ0MDEwNi00ODEzLTcyNGYtOTVkNi1jZmZlODM3NTE4YjgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUYyOEVDNzY2MTIxMTFFNjhDNDY5MzMzRTRCNDc1RkEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N2U4OTY0MjktYjBmMC1kNzQ5LTg1ZmYtZWYwNWFkNWM2Yzg4IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIwLTA2LTMwVDIwOjE1OjA4KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMC0wNi0zMFQyMDozMTowMSswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMC0wNi0zMFQyMDozMTowMSswODowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmVhYjU1MTgxLWE4NmQtMTE0Yy04NWYzLTY1YjUwM2UzMjY0ZCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0YWQ0MDEwNi00ODEzLTcyNGYtOTVkNi1jZmZlODM3NTE4YjgiLz4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6N2U4OTY0MjktYjBmMC1kNzQ5LTg1ZmYtZWYwNWFkNWM2Yzg4IiBzdEV2dDp3aGVuPSIyMDIwLTA2LTMwVDIwOjMxOjAxKzA4OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDxwaG90b3Nob3A6VGV4dExheWVycz4gPHJkZjpCYWc+IDxyZGY6bGkgcGhvdG9zaG9wOkxheWVyTmFtZT0i6Iu55p6c54K55Ye75LiL6L29IiBwaG90b3Nob3A6TGF5ZXJUZXh0PSLoi7nmnpzngrnlh7vkuIvovb0iLz4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpUZXh0TGF5ZXJzPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlS1PYgAACRrSURBVHic7Z15mFTlne8/Z6m1aWlEobsFhYQlNmtDFFQg4NIhZhQMEYNEA6I8ZBITzUQTnasTzcTcUZ/IjXNvHJSYxLgPI0wSxUYFBQ1ommZtBJkALt0tbo291HaW+8ehilq7az9Vzft5nn6oc+qc9/1R2/ddfot0bMm3KSFOAyYDE4EvAiOAWmAwMOD4n8Mm2wQCgUBQmoSAruN/nwCtwGHgf4BdwA7gY5tsS0C1uf9TgUuArwIXAGPsNUcgEAgEZYgDGHT8bzjWBC6eA8DrwIvABuDTYhkXjx3COxBYCFwFzAaU8BPSgAEoZ56FPGw48pChyKedhjRoEFLFACS3C8nlBtXusYJAIBAISgpNwwz4Mf0BzO4uzM8+w/j4Y4yjH2K8/x76u0cwu7rGYE3ulgI6sAl4GngGOFZMc6UiLjVPBW7EEl1P+KRaPwV13HjUceORq2uKZYtAIBAITiKM9ja0vXusv+bt0U/5sMT3QaCpGLYUQ3i/AvwLMCd8Qp00Gcc503BMmy5msAKBQCAoLppGaNtWQm9tQ9u5I/qZjcBdwKuF7L6QwlsP3AtcDCDX1KKOn4Dz4gbkIUMK1adAIBAIBGljHD1K8KVGtD27Mdpaw6dfAm4FmgvRZyGEtwpLcJcBsjy0Gse503DNuwIUpfc7BQKBQCCwA10nsO45Qm9uw/iwHcAAHgZ+CnTks6t8C+884P9hhQDhnH0h7qu/DQ4RASQQCASCMiAUwv/EHwlueiV8phX4R2BdvrrIl/B6gZXADQDqpHrcC7+FXFubj7YFAoFAICgqxgcf4H/2abSdkdXmh4GbgJ5c286H8I4C1gATkRXcVy7EOffSXNssOczubrQ9u9Fb9qDt3488ZAjeH91it1kCgUAgKCDB9c/jf/YZMHSwknEsAA7m0mauLsUXAv8JDFJGj8GzZFm/m+Ua7e0EG18gtHUrpq8HUwLJBNOX86BHIBAIBCWOc+6lqBMn4/vdavR3DkwE3gSuBF7Ots1chPfbwGrA6Zg2Hc+y5f1rL9fnI/DcswQ2bIg5LZnH//VW2GCUQCAQCIqNXFtLxS0/xbd6FaFtWwcBz2M5EP8xm/ayFd7vAv8XkByzvoJn6fVZNlOaaLt24v/db9E//QxJSn6NNLCqqDYJBAKBwEYcDjwrvgcuF6HXXnUCfwAqgd9k2lQ2wnsDx0XXOedC3NcuzaKJ0iX42ib8j662lpRTiC6YKP1sSV0gEAgEfeNZej2SohDc+IqEpYU6sCqTNuQM+7wcS937peiGtryG/9HVwIkl5eRIKOPGF8UmgUAgEJQW7muX4pxzIYCEFUJ7eSb3ZyK8XwGeBBTn7Dn9TnSNI0fwrX44rWsllxt1wsQCWyQQCASCUsV97VKcs+eAVejnSayiP2mRrvCOwgoe9joumIn7O9dlamNpEwrRvfK+NC82cZx/AZLTWVCTBAKBQFDauL9zHY4LZoKVy2Itllb2STrC68WK0x3omH4+nuuXZ2tjydKzehVmR7pVoSRcl88vpDkCgUAgKBM81y/HMf18sErersHSzF5JR3gfACYqo8fgua5/eS8D6O+9i75tW1rXSoDr6muQqqoKapNAIBAIygfPddejjB4DMBH4P31d35dX8zxgObKMZ8l1/StO9zi+DX9BMiXM3r2pAJC+OArXJQ1FsEoQRj90CO3gO5FjxznnImcx8Altb8L45JPIsXgfc8O/bi3GkcORY9fCq1DKpJ52/GdKHjwYx5SpNlpU3ph+P0ZbW+RYGjQoo+9oYENj5HHZvhcOB56ly+i643bQ9euBv2AtPSelN+GtwvLWwn3lVci1Z+TTzNIgGIRt29Flsw8vZkBR8P7jjUUxS3AC7eA7BJ54LHKsjhoN2Qjvls1ozSdqXAvhzQ3jyOHY1/OyeTZakxkJn6n6qeX5Y18iGG1tdN99Z+TYdfU1GX2/+novTL8f0+/PasBdTOSaWtzfXIj/6SfBCjPaRIqqRr0J7y+BWnVSfb/MvQygvb0PI+gnZbhuGFmm4pafIp96ajHMEpQR+qFDdpsQgzJypN0mCAR5w/T76Xngfoy2Vjwrvo9aV2e3Sb3inHsp2ttvo+1srgX+N7Ai2XWphHcyxysNua9cWBADSwE9aqksFfLgwXhv/jHyGcMKb5Cg7Ige6ZcCpzz6WN8XCQRlQFh09QP7Aei575cZz6btwH3lVXRZFY2uBx4CdsRfk0p47+V4vG5/Fhzj44+TnjexHKkc58/A/e1rkTyeotrVn9Hb28DnT/t64+OPYu//4P2s+jW7u2LbyXCmKtfUILndWfUtODkJbW8itGWz3WZkTKns15t+P2ZXZ8y5wBOPoe9rwbN8Rcl+H+UzzsA5ew7BTRsV4H7g4vhrkgnvV4BL5CFDcS/KS63e0iVFhSFH3Xic/3AZ6tmlvaxRjgSeeTpmbzBT/KszysyWkkxnqhV33i2WcQUZYXzySU6fdbuI3q+PdnxKRcLgeF8LgT7uSceJSq6qouKOu2JmvQBacxM9D9yPe+mykhggJMO9+FprK7O9/SKsxBqbop9PJrz/AuCYNh3KIEmE2dmJ/v57mB0doKrWBrzLhXL6EOhjpipX14CqIjmdyCNGoo79Eo7xE5C/8MXUNxkhzM8/x/joU7SOj1ACISRPBQw+zcrfXAavmaBwVNx5d0bXxw8Acr3fLgphhxjs2E+041O6aM1NfQ44op2o5NpajNbWpNdJbjcVt/0vfE8+TqhxfeS8fmA/Pff8HO/td5Sm+KoqjnOmEfjTOrA0dVPM03GX1wNz5JpaXPOuKI6BWaAffAdtRzPant0Y7e2YAb9V0SAUwtR1JI8HyeNFGngK6pixKGePQxk9GnlQrHOUa/4VOC9usMTXmyLmORRCO/R39Ja96PvfxvjoPYzP/RDSkEwJ47g7tARIAypRRoxA+dLZOM6ZhjxkSIFfCUGpkatQCKERnGzIQ2siwptKsD2LFgPEiK/Z2Un3bbfiufGmkvRKd82/glDTWxitrbOxtLU5/Fy88P4AQB03HhSleBamib5jB74X/owRtewQwTQtAVVVMAzM7i7M7i6Cra2waSOoKuroMaiT6lEnTUaurgZZQTrllMS2QiG0XbsINb+FtmcP5rETWa3MkIKkGiCBKZkxHtFmVyfant1oe3YTWPMsjvPOx/X1y/pnKFaWOGbMRMlgCV/f1xLzZXQ0zEU+7fSM+w1tejlmVO26+pqM7pcGDcq4T0H+yYcXefzSqNndlZd24+NX1VGjIcPPWSriZ55q/dSMvkeZEP1ZV+v7FjSzuytmKViurUUe2vssVD5rRMZ2eRYtRvJWEFy7Jua8tv/tkhReZAW1brylQfBDYEn4qWjhHQgsBHCWmNeY/sF7BJ56Am337t5q9fWOpqHta0Hb1wJPPY5y1gik6mqU6hrkoUMhFEJva8N4/z3099/H7PgsaTOSQ0+vP9Mk9MbrhN54HeclDbgXLgI12/LH/YdMvyABYkfBzunnZzUr1Pe1xApviX3Gy51sB0S9kWywU4glbf3A/ry0G+9xq4wcmbcVjHjhVc6uK8pn2PuDm/q8Rj90KOb1c8y+qGC2uefNRxk+HN+DK62+GuZGZsOliPOSrxJ8qREsbb2J43G90UqwEPCqkyYjDxlabPtSEmx8Ef+Tf7QOshXdJOhHDsORw2h5azE1wQ2NaHt24Vl6QzitmEDQr8h2QCQQZIpjylS0hrkAJS26APKQIaiT6tF2NnuwNHYVxArvVQCOc6bZYF5y/E89QfDFF+w2Iy8Ybe103/NzPMtX4DjvArvNERSIz5fmtrSY6/0CwclAqQtuNI5zp6FZcb1XESe8p3K8lqBj2nQ7bEvA//ST/UZ0o/GteggzFMI5a7bdppQF8ftk2e61Zrq3LChN0tlz7Avjw7aYbQepshJlVO4rUfLgwTm3cTKinF1XlmFX6eI4dxq+hx8CK1T3VODTsPDOBRS1fkpJ7EMG1v4XwReft9uMguF/dDXSgMrSdAgoEkZHB+ZnyffR41FHjY48Nj/7DD3N+6KRB50a49WerjONSJxRWqSz59gXgQ2NMXumyqgxeWm3P5HLykvgiceyCkPKR99q/dTSey9VFbV+ClrzdgVLa58Iq+zFAOqEibbZFia09Q38657rO39yGWKaEqgqnssvRznzLLvNsZXQW2/m9OUsFpnGkp6scbwCgSA16oSJaM3bAS4hSnhnAqh14+yyCwCzvR3ff/ymf4ouEo7hNbi/d5OVuEPQLymnON74mV8u5GMAIPJMZ05gQ2PWpTIFxUMdNz78cAZYe7ynA6OkAZXIQ6vtsguArof+3db+C4l89ji8//TjkoyPFggE5Udoe1NkWdc5fwGur87N27ZIuisvgVdeQvJ6cUyqzzqnfXyZRvey5SjZ1gjwlOa2kDxkKFJlJWZn5yhgiIpViQjlzDNtNSy0YzvmkSO22lAITECpPYMBt/7EblNKCsc558bs3eaC/6nHYwL4c/rixiHXiNUJQemhv3uE4J/WRo6Da9cQerkR95JlefEdSWflxejoQNvyGmBllFLGjMV784+zEv/o3M6mz9cvQ9OU4WeitewFmKQCEwDkYcNtNSrw1FO29l8oJKeDilt/arcZJYdcVZW0oH1wy2YCzzyZUQ5WqWJAzLFyxrC0v7jBLZsJvvBnPDfeVJo5X4tIIbMhxWN8/FFM+j9BZoQFLxqzsxPfgysJ1U/Ffe2Sgi8/B174S8yxNGSocETsBXnYcLCEd4IKjAKsFIo2oR85hPFhm239FwrTBO/S65EGVtltSslj+v34Hn8s8oPSc8/PGXDvrwr6Rdbb2yLVjrpvuzUvtT57fr3S1vtzoVjZkMDyKhfCmz3qjFnQ3Z00DEdrbqKruQnX1dfgnDmrIN8ho6Mj4f1zL7gyu8YyXB7W29uQ3J6y29eO0thRKjACQB58ml32oO3YaVvfhUT9whdwTD/fbjPKBn1nJIe4NXpf9VDBQgNMvz+Sdi5M4InHUEeNzmmZK9d4xP4cz5gNmYSdpd1mgXI1x+BxF3QFRTnzLFyXNERWiMzOzoRrAk88RmjTy3iuX5FdmtVeamcfr7oTQZ0xK/NQv+OvUfzrpO9rgV4GgL4HV2K0tuJomIvra18vGwGO0tgRKlANIA0caJtBoV07OVF+vv/gmv8Nu00oGyS3G+/Nt8R4x2rNTQQ2NBZkFuZb9VBCKTLX1df0y72lcqYYYWf5ytUcTbHiSZ0zZuL48jkxq0XRGK2tdN99J+qMWXgWX5PR7DeT2tnalteS9t8b2bxGoe1Nke9tqHE9+p5dDPjFv2XUhl1EaWx12KsZqbLSFmPMjg6MQ/+DidSvZFeqqkKdOMluM8oKZeRInPMXxFQfyccsNJ7AhsaEHxR1xqy8CHymmZUS7MjxfsHJh+R24112A9p5F+B76N+Tzn61La/RtbM5b85X+UYZMzbiIKkfPJDyusCaZ2KOHbMvKqhd+SRKY09XgUoAyZ2dK3iuGG2tYJr9SnTB/pjocsU9bz56y54YL+WeB+7L235vOAQjGmXMWDyL85MjOdMRfHyWnlzvF5y8qHV1DLj3VwReXJ9QOg+K73yVCdEOkskGDhA72wVLyJwzZxXctnwRpbGVKuABkJxOW4wxPvnYln4LikneQmVORtxLl9F9262R43zt9+rtbfh/tzrmnFRZiee7389a1DOt65tv7O6/0MiDB+clP3M0hcrVHE029WbzgeR24543H8fESfgeSdxOAWuVpPvgAVwLF+GcMTNlW/HfN9Pvp+vWH8UIY8Uv783LXrZ81giIWr3R29sS2o2f7Tovm19WXtRRGutVAevIphzN2rFjyJgY/WnOK1mu9YLsUKprcC9bHvE4BuvHIrhlc68/FL1h+v303PPzmB8NqbIS7+135DTyt7uur939FxrHlKl5Xxo9GXI1KyNHUnHHXfifW5PUe9zs7MS/ehXa9qa0Z7++xx+L+f64rr4mRhyjV18yjRCQvN5Y+z79DKLajp/tyrW1ZTXbBaI11iHbaQcAnZ/T/xaas6+iI7BwzpiZMNPxr15leVpmiOn30/PA/QlLWO4ly0762F1B/0Vyu/EsWoz3lttS+vBozU1033k7oe29+wpoLS0xzlP5Fr74FUL9g/cjj02/P2GlyrVgYVnNduNRgSDgRNNsmfVKwSAG/c2fGSTVYbcJZY/72iV0HzwQI5jxI+Fs8dx4U8YzKf3QIbSD7+Tcd28ENjTmtb1MHNP0fS0xGYQKSXxIj6BwhPd+faseSuqMZ3Z29urjY/r9+OLS+XquX5FX4YvPEBcdUhR4cX3Mb4AyZmxJOoj1iaaFH4VUwAc4zWAQyQbh7X9BRMcxTbstKHvkqipcCxfhX70KZcxY3Euzm6FaoUo/joRcOBrmZvXFjc8pWwjy3n4GIVJac1NZeEnHD07sKBIQb0O+Pe/zjeR24/3BTQS3bI7ZwgFwzl+AWpc6Y5lv1UMxwuecvyDv/1fJ7Q7nMgZOeDbr7W0JjmLuby3Oa9/FwgwGww97VKATGGj6fQnr7MXADrEvBsYnHyMPFfu8ueKcMRPJ60WtG5fTCDscchGqn1Keo2VBhPjBiTpqdNL0o8W0IZMBjp04Z8xEGTUqkoRCGTMW11fnprxeP3QoYTCmt+yh58jhXvsJbXrZmrX2gmPGzJjvojKpPrKcbXZ2Ws6Qj8YuMaszZpXF65wM0+8LP+xUgY+AYWZnJ5w6uOjGqJWnEDANJMn+7eZ8YnzwPoiQogT869Zi9PGlTUZoy+aUz8XH/fmfejwhf3O6baXCtfCqjO8RCEoRpbom4njlnHNhxgPa6FC/VBitrUk9qmPsiMsLrpx5FlrUcXhwEEaqrMxb2J8dRK0afKQC7QDmsWO2GGN4vf1OdAG0vXtxXvJVu80oOYwjhwu+nJnOD0OmuC6bl3NFpdCunUnjK1OhjBmb87JaJk5+joa5OIuU4lT/4P2EJU9B8Qg7XpUS6oQJMT4GCZnlFi4qa4eqKI1tV4HDYF88bX8tIKDt2YXp82Vdo1JQeqSqqJQOoe1NCaLraJibEOqhzpgVWW7TD+wnuPWNov1AyqedXrbLeILCIA0alHasePTyezqVruIHsUp1Tcw+b8y19VOzDiUsFaI09rAKHAQw2tttMUYedKot/RYaSdcIbnoF19e+brcpJYV81gjyvauvx3k+K2PG9rrUnBU5FNj2r1ubVHQ9ixYnCK9n8TX0HP0wMmsPNa7HOHwo6zqnvRE/g89XCJzptxLr92avXFOTdrF1gX3IVVVpx+PGxEZnWekqep83jFRZiWf5ipT3JEu2UYpEaexBFdgNYLz/ni3GyNVDwemEEx5f/QIJ8D//vBDeONzz5ue9zZ5fr4xZvnZ/a3FJzNxMvz9pCEdYdJMR9sDueeD+E7lrD+yn69Yf4b35lrz+v7Q9u5G83l6dzUy/n9Df3iK0+dU+vcr1Q4cIbn2DUON6nPMX9PpeS243ck0N/ufWoP31dQbc+6tc/iuCfkIyB1/Pit4zy3Xfditq/VQcM2bm7IRZSKI0drcM7ATQ33vXFmOkigEoZ46wpe9CYiAhdX1O4PHChp8IShOtpYWuW3+UILrO+Qv6XDoOi68yZmzknNnZSffdd+JftzYyo8wF0+8n8MyT+B5cSc+vVyYtixfa3kTnd2+wEpcc2E9w4ysp2wttb6L77jsjM/jQy4292hnY0EjXrT8i1GjFaAZeFLV5Tzbik+GEtjclzbLV23ZduA2tuQnfgyvxlervrWmitewFK4J2pwocBQ6anZ2jjA/bkYdW93p/IVDGjO61IkU5E3ipEWVyPeq48XabIigCpt+fskRbJkk7IrHHcTPm4No1hF5uzLnKjP+5NZHl+XD8buVvHo6ZLcQnVQg1rsd9xYKkMwrHlKn4o/bnzM5Ogptf63W5MXp7ILh2DY6Jk0pipUKQOUZHR8xxdDKW6LCi6M9y9MpPaHtTQn3sMIFXXsK77Iakz5mfxtb/Vc48KzPDi0TUxLYFOBrebtsMjNJa9uK0QXjVujqCzz+PNRjof/Tcdz8Vd/+sX87sBSdIVZQ8nBM6032ocNID35OPx8wEwlVmArW1uBYszCoDV/zMwjk/UVDVujrk2toY79LQ395K6eTivGx+zD5faNPLKYXXOXMWwT+tjXmtfI88VDa1Vfs7RkcHobfejDkXLaBmd1ev0QPpJGORTzvdajdJ8ZKYtra8hn7hxUkHZdGpJQGUM4b12qddRL12GwHCcTwvAWi7d9lgEqijxoKq2NJ3UZB0uv/tF7Yt5wsKS2h7E13//BP8q1cliK5aP5UB9/4qJ+cPz6LFeG68KSHfrtHaiu/BlXT9808IbtmcMOtIhf+px2OOpcrKlEkU1HPPizkOvvDnlO06zjk3wb5UOYAltxv3kmUJ1+c7ZaYgO0y/j8ATj8X8hcVUa27KW8ie1tKStHhJ9DYLJH5mw+jvHok5lk4tzRz52tv7wg9fASIOpusBXWvertiSs9nlwlE3jtCuncXtt5j0BOi+62d4f/BD1ImT7LZGkAdC25sIrHkmaaIAqbKyz7JrmeCYMhXlC1/E/4ffJcwkjNbWSEysOmMWjvopKZ1MAhsaE340e4uPdEybFuORbbS2pvQilauqYsKhwEpWkmpG7pgylWBUAXSA4J/W2pICUhBLrl7Ccm0t8lCrjeiwIuWMYZE9W+3gO/Tc98uY+8KrQ/j8dN99Z+S8fmA/gQ2NCSsoxt8P5tXugqBpaDuaAXRgE5wQ3k+BV4ELQ9u24rhgRtFtU6ef17+FFxP0EL6V96PMnIPnm1emrBgiKF1Mvx+tZW9KwQVrlluIQuNyVVUk326yJW2wluXCwqeMGYtSNz6yd6q3tyWkOlTGjO11cKBU1yQsNwc3vpLSQcx53gUxwqs1N2F0dKR8LdzfWhzzA2t2dhJ44S8ZxS7HLzf2RXyBBrO7K6lz2cmOWj81ZpAXCdOrqIjspcqDB0dCQqPfR8fsi3rd3w9u2ZzwWYzfkokfxAWeeAx58ODIQM7o6Ij5XOa7bnO+CL25LfzwVeAzICak8ingwtBbb9ojvPVT+2VYUTymCdprG+l+ayuOiy7BMf185DPOsNssQR8YHR0EX91keesmETywRvnuxd/pNeF8PnDOmInjy+cQeHF9r5mw9AP7MdpacUybBpCQ9xbSSzivnnteTD/aX1+HFMKo1tUlJEEIvropZWiRMnJkwg98qHE9zunnp+1olWsGLP3A/hjREFi4r12CufCqvM4iTb8/aY3gZH4QnsXX0LWzOdYP4MGVhGbMwr3gSgIv/CWmDfmsEXmzM59ECe/T4QfRuRqfBXq0nc0YRz8spl2AtefjmH5B/6xUlATD5yPw5/8m+Jf/ttsUQRqYfl9K0ZUqK3FdfQ0Vd9xVcNGN9Ol24543nwEPPIijYW7K1RPPiu+jVNckXWJ2NMxNS9zCwh3G7OxEa0mdAN9xUexMJ/Ry7/u2yfJgB/60rk+7BIVFrqrKq+jq7W30PHB/WqILx9Narvh+QjvaltfouvnGxKxvo8fkzdZ8YXx0FG1nM1hVAJ8Jn48W3o7wE0GbHByyyXRS7jgvn2+3CYI0UKpr8N5+R4LAORrmUnH3PbguabAlcF+uqsKzaDED7v0V7mXLY5xS3MuWo9bVoR86lHRZz33FgrT6CKfyi0Z7J3X4XzKhjo/ZjG/f0RDr3KU1N4nl336E0dFBzz0/Txj8ybW1vXr8q3V1OOf3/TmVKiuLNujNhGDji+GHz2JpLEBC9r5fA0u0vXtA10EprqexPGwY0tlnw759/TSwKBbHV+Ygl6IzgCApSnUN3ptvofvuO3E0zMX1ta+XjBOQ5HbjnDET54yZ6O1t6AcPnti79bgTln/7ygYUj3reBZEZhjJmLMrw4SmvDe8LAzi/9g84vnxOn325vvb1mBmMVFmJ6fMlvbYU9vLKoW5xKSFXVeG9+RZ6Hrgv8jlU66fiWb6iz8+Ge958zJ7upMk1wjgvm59Pc/ODoaO17AkfrYx+Kl54m4GNRlvrnMC653B945tFsC4W94Kr8P3rz4rerx2kO+MQlA7KyJGc8miJZsc5jlJdEzODUKprGHDvryKJPRwNczOeHTinn48y/EzU8RPSGmx4b7kto0GJXFUVKRrhaJibMlEHgPcHN6XdbqH4fGn5lqezC2XkSLy330HPPT/HcVFDRuljPYsW45hUj//x3yc4NToa5pbkamlg7XNhWzdhaWuEZHFDdwFzQm9tw3X5/KKHFqlf/CLyqFHoBw/2fXEZ47ykAWngQLvNEJwkSG433mU3EDoeapQpysiRGWWVymYlwH3FgpJaReiN+Fm3PLiwtczjKwTlUp6yUES/Jqlej/AgMJttGbWujgG/+De0lpaIJ7s6anRpZjvTNEJvRZyq7op/Wjq25NvJbnsJuMg5ew7u71xXQOuSox85TPfP7ih6v8VCGlBJ5QO/Ln68dD9Fb28D34m8wHJNTckmShcIBP0f/+9/S3DTRoCXgYvjn09Vgf7HgB7ctBHjgw8KaF5ylLNG4Jg0uej9FhxJAl3DvXyFEN08olTXRGZkysiRQnQFAoFtGK0fhEVXx9LSBFIJ7w7gEQD/fz6d4pLC4rruBpBTmVeemJqGc+YcHBMm2m2KQCAQCAqA/9mIZq7G0tIEelO224BWbUczwfXP59eyNJBPOQXvkmX9xrvZNMFUHLiv/Y7dpggEAoGgAARffCGcHrIN+Gmq63oT3s+A74Gl4EZr8Zec1ZmzUMZNKHq/haLyx/8ELpfdZggEAoEgzxgftuNf82z48HscTw+ZjL7WctcCqzAMfL/7LYRC+bEwAwbc+EPkigrMMp77mia4vnYpyjixxCwQCAT9Dk3D99tHwhr5KPBcb5ens4l6M7BLf+eA1XCxcblwf/8mJKl8k0mqI0fivmqR3WYIBAKBoAD4Vj8czsq1B0jMcxlHOsLbAywAjoW2voHvkdwSkmeD+qUv4Zr3DcoykbOq4r05qWObQCAQCMoc3yOrCG19A+AY8A0szeyVdN2GDwJXAD2h1zfj//1vszYyW1zzrsBxwVfKTnwrfnI70imn2G2GQCAQCPKM//e/JfT6ZrDE9hvAO+ncp9w2Oe19x8NY0+iF+uHDsvn5MdRJ9VmYmj2OKVMwPvwQ4/33itpvtnh/dAvq2aWXuFsgEAgEueH/w6PR8boLgdTJpOPIRHgB9gNHga/rhw9Jtojv1HMwPzqK9u67edn3lVwuJLcHyelEkrCKQ+TaZsUAKm75iRBdgUAg6If4//AowY2vAJhYe7qPZ3J/NumTHsJaov734MZXJFM38CxdlkUz2eO+YQXKiC/geyLzZPVyTS3qpMmoY8Yi19QieT2gOqwntRBmdzdGayva3w+iv70P/e+HkCBtr2pl+HA8P/ynguduFQgEAkHx8T26mtBrm+CE6P4m0zZS5WpOh2uwMnM4HNOm41m2HByObNvKCuO9d/Gvew6t6W8pr5EAKitRJ0/BMf28jBPEa3t3E9j0CnrzDtBCVtrHaEzr1Zc9XlyXXorzsnmZ/jcEAoFAUOpoGr5H/oPQtq0AIeB64A/ZNJWL8AJchFXgd5AyegyeJcsidTiLiX7kMKG//hXj3cMYHR2ga0jeCpRhw5DGT8Q5aXLO+XvNzz5D27WT0K6dmB8dxfT7kLwVSEOH4KybgDJlakKxcIFAIBCUP0ZrK77frUZ/5wBYiTGuxCqAkBW5Ci/AKOC/gAnICu4rF+Kce2mubQoEAoFAYDvB9c/jf/YZMHSA3VjhtWl5L6ciU+eqZHwK/B4YgmlO1fbuQT98GOXMs8QMUCAQCARlidHaim/1wwRf3mClH4SHgW8CH+badj5mvNHMw3K+qgZwzr4Q9+JrRAk8gUAgEJQHmob/8ccIbnolfKYN+C6wLl9d5Ft4AaqAe4FlgCxXV+M4ZxqueVeAouS7L4FAIBAIckfXCax7jtBb2zDa2wEMLAfiW4GOfHZVCOENUw/ch+WAZYXxjJ+A8+IG5CFDCtWnQCAQCARpYxw9SvClRrQ9uzHaWsOnXwZuAZoL0WchhTfMbOBOYE74hDppMo5p03GcO13MggUCgUBQXHSd0JtbCW3birZzR/QzG4G7gU2F7L4YwhtmKnAjVmotT/ikOvXLqOMmoI4bhzxkaLFsEQgEAsFJhHH0Q7S9e9H27o7P/eADngEeBJqKYUsxhTfMQOBbwFXALCAy5ZUqK1GGn4k8bDhydTXyaacjDRyINGCAldrR4wU53boOAoFAIDgpMAxMXw9mIIDZ1YV57BjGxx9htLdjvP8e+nvvYnZ2Rt+hA68BTwNPYVUWKhp2CG80pwMNwCXABVgxwQKBQCAQ5JuDwOvABqAR+MguQ+wW3niGAJOACVgiPBKoAQYDFVizZTHlFQgEAkE0BtastRv4BCsE6BCW2O4GdmIV+CkJ/j+i2Vucr/wPMAAAAABJRU5ErkJggg==" alt="">
								</a>
							</c:when>
							<c:otherwise>
								<a href="${appDownloadLinkIos}" class="external" target="_blank">
									<img src="${base}/mobile/v3/images/500vip/store_btn.png"
										 style="max-width: 60%;">
								</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<c:if test="${not empty appQRCodeLinkIos && appQRCodeLinkIos!=''}">
				<div class="card-content" style="display: flex;justify-content: center;">
					<%--<img src="${appQRCodeLinkIos }" style="display: inline;width: 13rem;">--%>
					<div id="iosQrcodeCreat" style="display: flex;justify-content: center;margin: 10px 0;width:256px;height: 256px;background: url('${appQRCodeLinkIos }');background-size:100%"></div>
				</div>
				</c:if>
				<div class="card-header no-border" name="${domainFolder}" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<c:choose>
							<c:when test="${domainFolder == 'b01701' }">
								<a href="${appDownloadLinkAndroid}" class="external"
								   target="_blank" >
									<img style="max-width: 60%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAd4AAABaCAYAAAAWyDe5AAAACXBIWXMAAAsTAAALEwEAmpwYAAAGkmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDggNzkuMTY0MDM2LCAyMDE5LzA4LzEzLTAxOjA2OjU3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0YWQ0MDEwNi00ODEzLTcyNGYtOTVkNi1jZmZlODM3NTE4YjgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUYyOEVDN0E2MTIxMTFFNjhDNDY5MzMzRTRCNDc1RkEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6ZTM1NjVhN2YtYjc1Ni0xZjQ3LThjOGEtNDIxYmJkNjVlODQyIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIwLTA2LTMwVDIwOjE1OjA0KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMC0wNi0zMFQyMDozMToxMyswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMC0wNi0zMFQyMDozMToxMyswODowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmVhYjU1MTgxLWE4NmQtMTE0Yy04NWYzLTY1YjUwM2UzMjY0ZCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0YWQ0MDEwNi00ODEzLTcyNGYtOTVkNi1jZmZlODM3NTE4YjgiLz4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZTM1NjVhN2YtYjc1Ni0xZjQ3LThjOGEtNDIxYmJkNjVlODQyIiBzdEV2dDp3aGVuPSIyMDIwLTA2LTMwVDIwOjMxOjEzKzA4OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDxwaG90b3Nob3A6VGV4dExheWVycz4gPHJkZjpCYWc+IDxyZGY6bGkgcGhvdG9zaG9wOkxheWVyTmFtZT0i5a6J5Y2T54K55Ye75LiL6L29IiBwaG90b3Nob3A6TGF5ZXJUZXh0PSLlronljZPngrnlh7vkuIvovb0iLz4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpUZXh0TGF5ZXJzPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlEnPW8AACUwSURBVHic7Z17XJTV2ve/HAYZkIMgBwFpCMQDipKH1MQ2HiuzjHZar+7S0sptB3fPUw/trXu/pZVbe92WlmWa5WO7sh3ZNneZCSoeEjNExAOJIAIqiHIQRhgO7x/j4JyZGeaI6/v5+PnMve77XusanJnfva51rety80h/ACeiJzAESARiARkQAQQD3W/8kzjINoFAIBA4Jwrg2o1/VUA5UAwUAseAo8BlB9mmg6eDxw8CJgKTgbuAeMeaIxAIBAIXRAL0uPGvN8oJnDYFwH5gB7ATuGIv47Rxc8CMNwCYDswAfgd4qE4Ee/kxOOB2EgNu43bfXsh8QomUBtND0p3unlK6e3rj5e7oZwWBQCAQOBNNrc1ca77OtWY5VxXXKJNXUdxQwdn6CxyrOUduzVmqmurUb2kBdgNfAluAGnvaa0/hHQo8j1J0parGqb1GMCF0CBNCh9C3e6S9bBEIBALBLcTpa2X8VHGUnyqOsu1CtvopOUrxXQ0csYct9hDeu4G/ASmqhvvCh/FI5BimR42hm7tYshUIBAKB/WhsVbCldB9fle3jPxd/UT+VCbwG7LHl+LYU3iRgOTABoJ9fFJNCk1gQez+xvuG2GlMgEAgEApMprL/Ie4Xf8WNFDqfqSlXNPwGvADm2GNMWwhuIUnCfAtz7dI/gkci7WNzvMSTuHsbvFAgEAoHAAShaW1hy6nO+KtvPb9fKAVqBj4A0oNqaY1lbeB8E3ke5BYinYyazctBcvD28rDmGQCAQCAQ24XpLEy/lrWdd0Q5VUznwR+Bba41hLeH1AVYB8wCmhA9n2cDZ9PeLskbfAoFAIBDYlRO1Jbyav4ntFw+rmj4CFgINne3bw31G3872EQf8CNzn6ebBWwOf4N3BzxDSzb+z/QoEAoFA4BBCugXwWO+x+Hp6s7syj1bahgJTUa7/dmoPsHsnbRsHZAOJo4P7kzP+Xf6rz0Od7NI8Ju79C08c+YfNx3niyD+YuPcvNh9HIBAIBM7Df/V5iJzx7zI6uD8osypmA+M702dnhHcW8D3QY0ZUMj/e9bpDXMsTZCP5rDiD/JpzNhsjv+YcnxVnMEE20mZjCAQCgcA56e8XxY93vc6MqGRQZsf6D0oNtAhL13jnA+8Bbk/eNpF1dzxn6fhWIf77p2lya6b4no9t0r/shyfxavOk4N51NulfIBAIBK7B07+u4eNzOwHagAXAWnP7sGTGO48bovtMzD0OF12AbWMXU9p8kQ3FO63e94binZQ2X2Tb2MVW71sgEAgErsW6O57jmZh7ANxQauHT5vZhbuLjB1Cqu9szMffw3pD55o5nE/r69mZmwDSeOb6Kp2QT29vrmuXkVhdxrLaYk3Xn+e1aOddbmpC3NgEgdffC28OLPt0j6O/Xm0R/GYMDY/DzbM9oyTPHVzEzYBp9fXvb/X0JBAKBwPlQad+HRT+4odxCexH4t6n3m+NqvhulX9tnnmwya5P+aJ6ldkCWMZMJgaN4NGoMHxR9T9blfO3E2B0S7OVHcs8Eno25ly9K9/FT9UGKx31mI4sFAoFA4KrMz3mfj4p3gHKL0RSUhRc6xFThjQN+AQIejx7Hx0NftNBM2/KHX1byeckepQPAGrTBY9F387/DXrJShwKBQCDoSjx55B02lWSAssLRMOBMR/eYso/XB+U+3dse6z2WT4f9qbN2Wp2vy/bzf355m4yKXOuJLoAbHK89R3r5QUK9A+jvJ9zNAoFAILjJgxEj+a2+nOO157xRlrrdBCiM3WOK8K4B7hsd3J+vRqTh6UT5lhtbFTx15F3+evIzKhptV06xorGGr8r2U9xQyT3hd+Dp5jx/A4GS4tpyqhvrCOzm52hTBALBLcZ9YcPYffk45+WXw4AwYJux6ztyNT8IbPVwcydn/LsMcKIZX6m8inv2/1W9moRd6Ns9kvSRf6Gvn6gd7CxsLtjO7Px1xEoCyJn8IT4Sacc3OQHFteUsy9vUfjw48HbmJ0x3oEWmk19VyOC9mkswzQ9ZLZWtzfH85kGN49yxK0kIjnWQNa5PRukhyhsutx/Pip9i1r1bzmW2H6cNehyZf4RV7bMHp+pKScp4AUVrC8BDwFZD1xqLag5EGa3FmwmPO5XoljRUMmrPy1y6ftXuY5++VsaIzP9iz9g3GRJ4u93HF9ykUn6VBT+/TXr1cQAKFTXM3reUT8YscgnxrVfIWV9xsP14rgNtEQg6w5ZzmRqfZXOEt7zhssa9zyse0blmc8F2xoQnObUg9/OL4o0Bj/PK8Y2g3Ga0GwNVjYzt430LiJgSPtzuaSCN0dDSSMreV/WKbpBXd0YF9bPaWKOC+hHk1V2nvb75OuP3/YWa5nqrjSUwnxBpD4K8NF3L6dXHWZKzwUEWCQQCa7MsZyOz89cxeW8aGaWHHG2OUV7qM40p4cNBWaFvmaHrDM14h3Cj0tCbCX+wtm2d4r79/5dz1yt12gf49ean5DcI7RZAZuUx7j/wOo2tRte3DdLNXcJ3o/9KSkgiFY01TMj6Cyfqzt+8wK2NGkUD9+17nf2/+7ulb0VgBVaOeJHTmWVk1Ze0t60o20lCQYxZT93OwJWmOvKrCq3Wn3CdClydtflbWFS8FVB6tCYdfpPVNTOdeknmrYTHVRWN5gIfAEe1rzEkvMsBj3myyST432YzA81l2emv2X8lH9o0Q5fd3GBy2FBCuwUAkBKSSKQ0mLP1Fy0aJ1IaTEpIIgCh3QKYHDaUk9fO09ameV129UmWnf6atL4PWzSOoPP4SKRsHPkqk/emUai4GWA3O38dScH9XEp80quPk77XelvXXGnNtauivZbsCswNHcUHd6XZbbwIn54axzlVp9q/t/f2HkNqRW77chLA8wWfkVt9lpUjXnTKJaUB/tHMk03mo+IdHsDbwATta/QJ793AxDjfXqxMdJ5Vp6L6Syw6sQl9+4Xa2iC9/ABzZZOI8Q3jm/KDlMov63ZiIqXyy3xZmsVDEaMoqr9EevkBHdFVjuvGohObmBE1hhjfMIvHE3QOmX8Ea4csYNLhNzXaXzv2MVtS3nCQVbcemwu2W7W/CJ+ejIu606p9CsxDO/DJEJlXT2gcP7vfoJe1HVUQVZi0p8FrZP4RfDJmEexbqiG+6ysOcmVfHcuHLnDKdd9Vg+ex53IeBdfKx6PcYrRb/bw+4f0bwPSoMUg9vGxuoKmk5W9CmZNa/0bdcw0VDNq1gChpT0oadF3R5tDU2szMw28T7RNCqfwyrfpUt5020vI38eWIlzs1pqBzjIu6k5fLJ7KiTJmve27oKJbc8YyDrbq1mJ1v3SIic0NHCeF1MNqBT6Ziyj36gqj04SORsiXlDZblbGx3O4PSQ5S7N40dY5c5nfh2c5fwSOQY3ji9BZSaulv9vLbwJgEp/fyiWNzvMftYaAKXGqv5pnw/HWXHaG1r67ToqmNaX258U76fS43zCOsWaLWxuzr6tqNYk/UVB1n/g/k/GPqwl8vW3i4+gcCVSEuaA6AhvoWKGuJ2zefA6KWMCBvkIMv0s7j/o6SXH+BkXenvUGprjuqctvC+ADAhdAgSJ0qU8c+SPR3MOh1La1sb/yzZw5/6uN56jkDgSuRXFZJTdcrq/W4r2WuVfrUD+qz10Kbaq26Lvo2RFNyPTxI6Lr6zofgHjQBHU+4J9Qky2x594psaOJCBQXFm92VrPN08GB86hJPKXBMvArPbz6ldFwBMB3gu9n572tchuy/nOdqEDtl9OU8Ir+CWJnfsSqv256sncCan6pTVXdqg+UPeGVwtkr4jEoJjTQpQ3FeZpyG8tvw7qItvauBAp963/3zsVNYUfgdKbV3IjX296sI7HfC5L3wYcb697G2fQZrbWsitOetoMzokt+YszW0tIp2khcRKAkjpMcDRZgCmrU8JdHGlCHKB8xBjwfpsWtIconxDSY0Z57SiCxDrG86U8OFsv3hYilJj14Gm8M4AeCRyjAPMM8zF69WUyqscbUaHlMqruHi9mihpsKNNcUlSegwwe30zo/QQYdKeVv/BX2/BFpBn9y/rtGCvrzho0dgqxPYhgSuiLZz51UUm3ecq3oVHIu9S7eudgZbwBqEMeWZ6lHMJ74XrVxxtgslcuH5FCK+dqJRfbd8+lOwbzeIBsxgZlujUT7+CzjO592hygzufnU47sO/rIQuJ84/udL9dBXPX0k/Xl2kcd2Zr2c/Vpzt1/+TeowmR9rD4fmszPSqZ2UdWgXKrbhBwRSW89wAeU3uNoJu7xCHG1TY34O/po9Me4xvG4ZSVeDvR1iZ9XG9pordPiN5zht6bwHI2nNra/jqrvoRJh98Uie5vAUKkPWzyoxrnHy0+O2p0di29M/dm1ZeQ1Yn7c4P7OZXwerl7MrXXCLZdyPZAqbX/VAnvBIDJYXfY1aAL16/y99P/YvulX6hRXCO0WyBTw0fwcnxqew7enl7+SNw8aWpV4KxxzW6Al7uEAMlNcb3SVMeKgnS2XcymorGaAEl3poQN43/6/p5e3s7zoXBFKuVXdYJhXo6c6FQ/nPaYQVljO5alM4vS+gqr9aXC1TKNCQSmMjnsDrZdyAaYiJrwJgOMDxlsN0NKGioZnvknqprq2tuuNF3jVF0pX5TtIWfcagIlvlxpqiN0+yy72dUZKqZsJsjLj2pFPUMzX+R8w8216StN11hz9js+L93D4ZR/EG1gdizomMW/fqjT9tKgmQ6wxDCuMoOyZoRwZ/v6JOFpl/ibORMZpYdYcmIza4YuFH87J2ZC6BDVyzGgXOMNAeJ6evnTp7v9sn/MPrJKQ3TVOd9QxczDK9k+ejFSj274e0qpbZbbzTZL8PeUIvXoBsDMwys1RFedqqY6Zh9ZRUbyrZ3KMCE41qJgoPyqQp0gpk8Snraqa0kEKQlcgUr5VeYffY9CRQ2D977Ey5ETeWnQTKt8F2bFTzEpeCm/qpDUg68xJzKFcRHDGBgUZ1GchXZwYlf7Dsb59iKkWwCVjTVxQKg7ykpEDA6Msashey8fN3p+x6XDXGmqQ+rhhcTNWNlg50Di5onUw4srTXXsuHTY6LUdvXeBYVaf+krjOFYSQGrMOAdZIxA4jgU/v61RGGRF2U7G7HrR6jmzjbH61FcUKmpYVLyV0QcW8WnBNruN7WokBshULwd7AoMABjlRFSIlblxqrNGpt+rsXGqsoaPUlgLLyCg9pDPbXTtkgVNGMlsrE5K9sXYSDGNsK9lrtcQVtyK59ed12goVNczOX8eG4h9s7n7W9j7FSgJ4In6qRX31kOjWPe9qDPK/jV0VuQCDPIE4gDg7uplNxd3N9QTMFW12BRoUcpac2KzRlho40GmT6LuqoNhzndAVH0yciX3j32Fl3mfthUHUyaovYfDel1gqm8YLAx+1ycOptvdpcfwMi8dJCIwBtR1J+VWFBj+LDQo5S3I2MCv2Xpda11bT2DhPQAZwmzTUUfYYxM0FZ46uaLM90Zdz1lLSq4/btd6p2K7kGOzhOrWFh8LW+0lDpD14a8RzPHTpbv4nd51GykYVi4q3srEsk+UJc3gwJsWs/otry1mWt0nvuStNdRpl+kCZNnJfpXnpfafflmL2w3N6UQYrynayomwnqYED+Vviky7xvVTTWJknEA4Q7h3oKHsEgi5HauBAmy+T3CqpLW2Rm1kbW3go7LWfdETYILanLOfTgm08X/CZzvlCRQ0PH13F3PJDLLnjGZNtqlfIzfqMWfJ5HBOirCgUH6C59e6S/DIJ6Ippg0LOkoIv24/Tq4/z380NZo/rCNQ0NlwV1UzPbgGOskcg6HLY4ym8M+klBV0LH4mU+QnTGRs+lNeOfawzG4WbpTI/SXja6dIt+molGCpvuKz3uvSiDI2AsmTfaKcrB2gINY0N8QT8APw8nS9ARdD1iPDpydzQUSZff7q+TMeF1pnZpD4Xman26KuWIxA4EwnBsWxJecPoks7s/HX8u+wAy4cuMFpAPtQnSKe8X2l9hY53wJQSgIZIupH+U7tQgr58zdqzXYDFA1wjxwNoaKyfJyAFkDp5SkZB12Bc1J1mremk/Pi8xnGybzRbUizfA70sZ6OG8Cb7Rovi806OLSKt7ZGr2ZKqO9ZiVvwUxoQn8cqR9/TOftOrj5O+a77R2W+ItIfOuWf3L9M4Vr9fO7+zOWvc2kFZVxXXdG3Wmu06c3ClPnxu5HkAfDwBL8BhOZoFAkNsLtiuM9tdM3Shxf01KORsLMvUaHtKdo/F/Qnsgz0CZ1wl05g5yPwj2JLyBmvzt+hd+wXTZ78A2ZfyNNZxUwMHagizdn5nc9e454aOau9/fcVBPlA7p2+2+7fEJ03u2xnwcm/PRyFx/swUgluSSvlVnS9aZ/Mxaz8xAzZLvmGNPMoCgTVQrf0+d2SV3shn1ez3wj2fGBTKBoWcPxxeodFmbeGT+YRpHBfXlrc/DLx7/AuN7+7c0FEu/aDkCTQBXo2tCjHrFTgNi3/9UOOLFisJ6FQ+5kr5VZ01r6WyaU6ZfMORaLsSbYl2KTmB7UgIjmV7ynKW5GzQu+93dbzxVJMvZb+j8X1cHT/T6sIX5au5pfVs7Xlk/hHkVxXqrCunDXrcqmPbg6bWZtVLhScgB7zkLU1CeAVOgb4MVYvjZ3Rqa4Z2YYVYSQAvDHzU4v66Kq6wRUk7cGhu6Ci7r9PnVxXqeDWcPb+wj0TKWyOeY2Lp8PYcz6B0GRvLOLW5YLuOi9nSDFXGSNKqs5xdeYJxUXfy2rGPNdpXx8/s0C3ujDS0NLa/9ATqgIC6ZjmBEl/HWSUQoHRpzT/6nk77vso84gMs2zqg/cMBncuyYwpLZdN0nuD1sa9Sc93M1PvAPvtbBV2PcVF3ssO/N68ceY/c+vMsH2o87ap2Uoz06uOkf9fxQ6spyy3qDyvawWg7Kn8lqiBUIzisM2kpHU3dzUI/dZ5AJRB1ubGG3tKejrNKIED/Oizc2INYcZBk32gW9kllYtRIk4Qzo/SQjkBpB4XYgqnRY012xakLrzn32UJ4zdnq1Vn0bRUT2AeZfwSfjFlEkdo6qqPxkUhJDRzYLrRZ9SVkaX3G/3f4yy67PHS5sf13rdITuAhw8Xq1o+wRCNqZFT+FpOB+BhPoZ9WXkHV0FbH5AcyJTOGpftMMuqAzSg8x6fCbGm2xkgCWD11gdbs/uCtNIwrTHtjCtWlPl+3mgu06P6wC++EjkTpdgFJK6GC9259AGVzpKsky9KGmsRc9gWKAc/IKB5kjEGiSEBxLQnAsT/Wbxr/O7mRV0Xc6s2BVKbJFxVt5OXKiTsJ0faILympGzvKE7wzYc4arjXYylQgf4XFzRkx9qNRee7ckt/nQnv2hQLc9VhLA4qSnzOrL2VDT2GJP4AzAmWvlDjNIINBHiLQH8xOm80T8VNKLMthQ/INe16R6wvRn+zxAecNlvW7Yr4csdKkN9/bAnBlusYluyeLacvZdzOnQna9KppJfVUioT5Bd8hoLnJsRYYOIlQToPGgbczFnX8rjmqKBkWGJTu2GVtPYM55AHkBe7TmHGSQQGMNHImVW/BRmxU8ho/QQS05sNrwf8bB+N9VS2TSzq7Nooy+S1drYon9D2Ykq5VcZs+tFFsfPIDVmnN4frfyqQraV7GVjWSYRXgFkTlptcJxvizL5rPindldhR8FwlfKrbDil9Fo4IjJZ4HxkX8rTEd2OXMwFNSXMzl9HrKTj5SdHoqaxee5ALsCxmmJH2WOQNtocbYLZuKLNrsS4qDvJnLSaA6OXmuwmXR0/k7SkOTa2zPVYmfdZe+H0KZmvkH1Jt6Rb6sHXWFS8lUJFDVn1JWSUHjLY38nqYo31uY/PGC7nVym/Sq8fZrev46+vOMi3RZkGrxe4PvlVhe3/NhdsZ3PBdirlV9vPF9eW6yTpAEivyKZBIddpb+/3Rl5n1fJTvZFrHUVrWxu7KnIB2oBcT6ACOFPZWBP327Vy+nR3nvWv1jbXEzFXtNkVGRE2iBFhgxhjQn3f5ws+o6S+wuUKZ9uS/KpCjUQKWfUlFNSU6MwsFsbcr5FucMu5TIPu+qnRYzUC4tZXHOR5AwXNQ6Q9NFIEArySv9HkaHWB4yiuLdcQN1V+Zu1tR4aqJKmjSitZXFvO5L1penc0FCpqSC/KMLh0UVh/QePYGWM48mqLVS9PABWqlJFZQNyuylznEV43CHPBUoVh3QLADcTE17bkVxWy+tRXJid8UK0Dzw0dZVHxbdBfraWzdGYfr6loJyZoUMh57sgqjbZk32i96TPv7T0G1IR3fcVB0gys9SYEx2psBwFlgXlDDztpgx5n/a6b771QUcOSnA28NeI5k96XwLaszd9CbvVZMq+e0CuIHdGR6IJy764x0VWxpOBLg8sh6uOkBg402057kFF5TPUyE5QpIwF+AubsuPQrz8bcaxdDhvXowy9Xf9N/0g3uDk4kyMuP6y1NSiFzdtzgeksTQV5+3B2cyJ6qYwbFd1iPPva1rYvQoJDz86VjfPDbv41+qbV//NVp3w98IprFA2aZFZChr1qLNbB0H6+lpBdl6KyRLx4wS+/fQeYfQbJvtMb135/fx/yE6Xr7frbPAxrr7IuKtxpcc5P5R7BUNk1jlryibCezqoRnwlmwVSazZN9o+vpGUiG/qld0vx6ykFfyN7a3G3ooK67VDAqO9e1lE3s7y56b3oAMuCm8PwAt2y5kezS1NqtXUbAZ65IWMHZPGtdaruuc6ynxZcMdynJw8pYmm9tiLeQtTXh7eLHhjucZuXshl5vqda7p7uHNuiTr7yPtymRfyiOj/Bc2lmUafSpO9o1mzdCFJATHUim/yo7zB1hS8KXee7LqS5h0+E3lNgUjwUVdjeLach3X/NzQUUY9ANMjk8lSm/WuKvrOoPCODEvUiUrdcf6AwQeWFwY+qvP/+tyRVUaDuAT2wdDWHn2kBg6kUlGr8YC2VDaNqdFj24+1H6YMzXR/HP5nxkXdSZ2iQeOzuqJsJw9dultjOeRs7XmNexMCY0wz2I40tTbz3cXDAC3AbrgpvFeAPcC4L0uz+EN056I/TSExIIZjE9awrOArtl3IpqKxhkhpMFN73Ula/MP08g4CwN3NFaa7SlS2ynxDyRm/hmUFX7PtwiHK5FWEdgtgaq8RpMU/QrRPiIMtdW4q5VfJqypgZ/lh0iuyO3RzqcRT/cddNTtNjRlHelGGQQFWBRctKfjSqSMircUrR3TTcXaUcF7b3VyoqCH7Up7eSFMfiZQ5kSkas9gNxT8YFF4fiZTF8TM0fmCz6kv4tijT5Cj00/VlbC4wHMhlCttK9mrUku2I0vqun/cgxj+qPYBR5hPWvgSiWrrQ3gKmnRDFmPfG0D57leiCMpmO9hbCPxxewfKEOe2fjezKExr3ay+rOANbSrNUL/cAV+Gm8AJ8AYz7V9l+uwgvQLRPCO8P+SNvD3qKakU9Pb387TLbtge9vIN4J3EeKwbO4XJTLYESX/VCyAI18qsKOVNbws+V+fxcfdrkNILJvtE8JbvH6GxVtRXJFAFWJeRYKpvGo7GTnTJIozNsLtiu44I3JeG8PndzRvkvBrd4PBo7WUN4s+pLyDcQZAXK0oza/y/mBFrpSy1oLvqypN3qhEh72GSL17KcjXr/3uqiq2LjyFeJ2zW//bhQUcPDR1cxt/wQgwNv1+nHGZcovirbr3rZXufUXf080LD94mHOaEWJ2Rofj25EeAfpFd3WtjaXiBQ2ZKeXuycR3kFCdLV4NXsNKT8+j+c3DzJ470s8fHQVK8p2miS6c0NH8ePwP5M5aTWz4qeY9OOsEuCcyR/yScLTxEoMB+4tKt5K3K75rM3fYtZ7cmbyqwp1XMzJvtEmJ5yfHpmscbyxzPDWH5VQq7OtZK/B630kUtYO0Vx+UUWyCroOxbXlTM/8i45YxkoC9IouKD9LXw9ZqNO+vuKgRrQ9KPf7Ohtn6y+yXelmlgPtPyjqwlutOrGm8Dt72mYUbw8vWml1tBkd0kor3h5ejjbDZUgIjDErQX6ybzSr42dy4Z5P+OCuNIszUJkjwGPDh1o0hjOiHcUMsGboQpPXtbX/FoWKGvKrCg1ery7Uyb7R9A+UGe1/XNSdOmK9pOBLo/s3Ba5FbtVpHY9LrCSAHWOXGf0+PxiTwlLZtA77nxgxvLMmWp13C7epXn6FUmMBTVczwLvA7J8qjqJobUHi7mEf64wg9fBC5hNGbs1ZnDe8uQ2ZTxhSIbwmo2/9RpvUwIE8EDmaMeFJVnf7duSCXiqb5pRuK0vZOPJV5vz8Vvvf29xC5gnBse0PKXMiUzqMvr639xiWNtUzLmKYyYntFw+Y1b7uFysJYO0Q/eXqJvceTa6D1/LO1Jbw8NFVDrXB1XgwJoVc/2hSD75GoaKG1MCBvDfyv02KqUhLmkOAl6/OLFdFRwGCjqC5rYVdFUdVh6vUz2kLbw6QeaquNGXJqc95fcAs21tnAml9f89j2boZTZwHN9L6/t7RRrgc6j+0oBTalNDBDO3Zn4FBcXaJMtYnwKCMtu1KyPwj2J6ynE8LtrGlLMuimqY7xi4z+QFI5h9hdrawcVF3kvqb8mHL2Lp9iLRHlw6A68okBMeyY+wyo1vSDDE/YTpjw4eyufB7jeQvS2XTnPL7uuTkF5ysKwVlJHOO+jk3j/QHtK+/G9gd3z2CnPHv0s1dYhcjO+KlYxt4t/DfuifsmazCwFgvxD7AykTXrpzhKL4tyiTOP9ppZpcNCjlFteV2sye/qlAjmnZy79FCVFwA1edEHVt+ZirlV6louGK38SxB28YY/4hbYouePhpbFSTteoECZWGEFG5sI1KhT3hBmVBj/DzZZNYm/dHmRprKTxVH+fjcTk7VlSFvuY4bbni7e1F6/TJXmq4ZvdfbwwuZNARwo7VNuWbs7uYOtFEsr1Qm6jBCkFd3orx7cr21iTbakHp4088vkidvm8iE0CHWeYMCgUAgcHnm57zPR8U7AHYBE7TPGxLeIcAvgMex8asZ4B+t7xqn4c/HP2X5mXSjM9+B/tEcHa9/U/6QXc9zvNZIoI8bvBKXypsDn+ikpQKBQCDoypyoO0/iT8+BMmHGMOCo9jXu2g03OAqsB/hz/ibbWGdFJO6eJribjQVmdRC01XZjDIFAIBAIjPDn45+qXm5Aj+iCYeEFeBUo/+7iYVb+ttWqhlmbptbmDq9pNaLMxs6ZM4ZAIBAIbl3+ceZbVXrIC4DB7CPGhPcqsADg1fxPOVF33silAoFAIBDcuvx2rZzFJzarDhdwIz2kPowJL8BWYF1LWyvP5rzXYQCSQCAQCAS3Go2tCub9ulqlkRuBb4xd35HwAvwJOHag6iTzckTFEIFAIBAI1Jn767vsqzoBcBzosKC0KcLbADwM1Hx+fi9PHnmncxYKBAKBQNBFePLIO3x+fi9ADZCKUjONYorwApwBHgIaNpVkMD/nfYuNFAgEAoGgKzA/5302lWSAUmxTgd9Muc9U4QXIBB4DWj4q3sGCo2vNNtJWtJmSuspYhSMTqh+ZNIZAIBAIbgkWHF2rSpLRglIbTS6nZY7wAvwbpf+67cOiH5xGfBVtLR1e02zkGmPnzBlDIBAIBF2fBUfX8mHRD6DMIPE8Sm00GXOFF+AD1MT3mZw1FnRhXSK8gzq8JlDia9E5c8YQCAQCQdfmmZw16qL7HGD2DNQS4QV4H3gCUGwo3snMw287dKvRjKhko+fdcGPB7fcbPL/g9vtx6yB7VUdjCAQCgaDr0tiqYObht9lQvBNAAcxGqYVm4+E+o6+ldhwDDgBT82tLpHsu5zMyqB8h3fwt7c9i/CU+hHkH8Z+L2ehL/zhPNplX+z1i8P7EABkXrl/h12p9hb3beG/IAsaFJFrPYIFAIBC4DCfrSpmRvZzvLx0BZWKMB1HmubCIzggvQBHwNZByXl4Z9lHRDqQeXowO7t+ZPi1iWI84RgTFU9cs50pTHRJ3D+4M6suifjNY1G9Gh/ff32s4vX1CqFE0UK24hp+nlAmhSXyQtIDUyFF2eAcCgUAgcDb+32/fMP3Q3znXUAGQB0xEWUTIYgxVJzIXH2AVMA9gSvhwlg2cTX+/KGv0LRAIBAKBXTlZV0ra8U/Yrsy9DPARsBAT9ul2hLWEV8WDKIOvwgGejpnMPxLn0c1dYs0xBAKBQCCwCY2tCv507CPWFe1QNV0A5gPfWmsMawsvQCCwHHgKcI/vHsEjkWNY1O9RJO4e1h5LIBAIBIJOo2htYempL/iqbB8F18oBWlGW9nsFqLbmWLYQXhVJwApgPEA/vygmhSaxIPZ+Yn3DbTWmQCAQCAQmU1h/kfcKv+PHihxO1ZWqmncBLwM5thjTlsKr4nfAX4EUVcN94cOYEZXM9MhkMQsWCAQCgV1RtLawpSyLL0uz+M9FjTipTOB1YLctx7eH8KoYijLDx3RAqmqcFjGSiaFJjA8dTJxvL3vZIhAIBIJbiDP1F9hVkcvOihy2lv+sfkoObAFWA0fsYYs9hVdFAPAoMAMYC7RPeUO6BZAYIGOQ/23EdY8gxieMcO8eBHn54evhTYDEBw83S3N+CAQCgaAr0tLWSo2igfqW61xpquPi9asUNVzizLVy8mrPcaymmMrGGo1bgL3Al8AXKCsL2Q1HCK86IcAklPui7gLiHGmMQCAQCLosZ4D9wE7gR6DSUYY4Wni1CQUGA4NQinAM0AsIBnxRzpbFlFcgEAgE6rSinLXWA1UotwAVoRTbPCAXqHCYdVr8f6I7vb10oBHYAAAAAElFTkSuQmCC" alt="">
								</a>
							</c:when>
							<c:otherwise>
								<a href="${appDownloadLinkAndroid}" class="external" target="_blank">
									<img src="${base}/mobile/v3/images/500vip/android_btn.png"
										 style="max-width: 60%;">
								</a>
							</c:otherwise>
						</c:choose>

					</div>
				</div>
				<c:if test="${not empty appQRCodeLinkAndroid && appQRCodeLinkAndroid!=''}">
				<div class="card-content" style="display: flex;justify-content: center;">
					<%--<img src="${appQRCodeLinkAndroid }" style="display: inline;width: 13rem;">--%>
					<div id="androidQrcodeCreat" style="display: flex;justify-content: center;margin: 10px 0;width:256px;height: 256px;background: url('${appQRCodeLinkAndroid }');background-size:100%"></div>
				</div>
				</c:if>
				<div class="card-footer no-border">
					<span style="display: inline-block; width: 100%;text-align: center; font-size: .6rem;" id="wenxintishi">温馨提示：推荐使用支付宝或者浏览器扫码二维码下载</span>
				</div>
			</div>
		</div>
    <div id="common" class="AppDownload amyl4"  style="display:none;">
    	        	<div class="amylImg">
	        		<img src="${res }/images/amapp18.jpg" style="width:100%;padding-top:2.2rem;">
	        	</div>
	    <div  class="main child-view" style="padding-top:0;margin-top:-8px;padding-bottom:100px;">
	        <div  class="appcommon">
	            <div  class="intro">
	            	<img  src="https://tpxb.me/Krxinbo002/amylc/logo.png" class="amyl" style="display:none;">
	            	<img  src="https://tpxb.me/Krxinbo002/amylc/logo.png"  class="amyl2-logo">
	                <p >下载APP 投注更便捷</p>
	                <p class="amyl1ss"  style="display: none;">下载后请确认APP名称为<span style="color:red;font-weight: 600;" >“澳门娱乐”</span></p>
					<p class="amyl2ss"  >下载后请确认APP名称为<span style="color:red;font-weight: 600;" >“澳门娱乐城”</span></p>
	                </div>
	            <div  class="downLink">
	            <%--<a style="color:red;background: none;width:18em;padding:0;margin-bottom:25px;line-height:2em;font-weight: 600;">--%>
	            		<%--下载APP注册并完善会员信息后，即可向在线客服申请<span id="amylOne">1</span>8元彩金！--%>
	            <%--</a>--%>
	            <a href="#" onclick="window.location.href='https://xsign.xin/app/detail/qjKtquJTy'" class="amyl"style="display:none;">
		            <img src="${res }/images/iphone_small.png">
		            iOS版下载
	            </a>
	            <a  target="_blink" onclick="window.location.href='https://m.xb783.com/download/amyl/amyl.apk'" href="#" class="amyl" style="display:none;">
		            <img src="${res }/images/Android_small.png">
		                                    安卓版下载
	            </a>
	            <a href="#"  class="amyl2" onclick="window.location.href='https://xsign.xin/app/detail/GpovHJ2Vv'">
		            <img src="${res }/images/iphone_small.png">
		            iOS版下载
	            </a>
	            <a  target="_blink" href="#"  class="amyl2" onclick="window.location.href='https://m.xb783.com/download/amylc/amylc.apk'">
		            <img src="${res }/images/Android_small.png">
		                                    安卓版下载
	            </a>
	            <a href="#"  class="jsdaili" onclick="window.location.href='https://xsign.xin/app/detail/Ngc9qJ2L7'" style="display:none;">
		            <img src="${res }/images/iphone_small.png">
		            iOS版下载
	            </a>
	            <a  target="_blink" href="#"  class="jsdaili" onclick="window.location.href='https://m.xb783.com/download/amylc18/amylc18.apk'" style="display:none;">
		            <img src="${res }/images/Android_small.png">
		                                    安卓版下载
	            </a>
	            </div>
	        </div>
	    </div>
	</div>
	<style>
        #common{
            -webkit-tap-highlight-color:transparent;height:100%;overflow: auto;
        }
        .child-view{
            /*position:absolute;*/
            width:100%;
            transition:all .2s cubic-bezier(.55,0,.1,1);
        }
        .main{
            font-size:inherit;
            padding-top:2.3em;
            height:100%;
        }
        .appcommon{
            height:100%;overflow:auto;background:url(${res}/images/appBg_11062.png);background-size:cover
        }
        .downLink{
            margin-top:3em;
            width:100%;
            min-height:310px;
        }
        .intro img{
            display:block;width:4.5em;height:4.5em;margin:3em auto 1em;border-radius:.85em .85em;border:2px solid #fff;padding:1px;box-sizing:border-box
        }
        .intro p{
            text-align:center;font-size:.7em;line-height:2em;color:#666
        }
        .downLink a{
            display:block;margin:0 auto;width:14em;text-align:center;color:#fff;border-radius:1.5em;font-size:.8em;height:2.8em;line-height:2.8em;margin-bottom:1em;padding-left:2em;position:relative;background:#e53333
        }
        .downLink a img{
            position:absolute;left:1.8em;width:30px;top:0.25em;
        }
    </style>
    <c:choose>
		 <c:when test="${fn:contains(domainFolder,'b202_') or domainFolder eq 'b202' }">
		 <script>
			 $(".amyl4").css('display','block');
			 $(".amyl3").hide()
			 $(".amylImg").hide()
			 $("#amylOne").hide()
			 $(".amyl2-logo").css('display','block')
			 if(window.location.host == 'am1512.com' || window.location.host == 'www.am1512.com' || window.location.host.indexOf('jz032.com') != -1|| window.location.host.indexOf('jz8383.com') != -1){
				$(".amyl").css('display','block');
				 $(".amyl2-logo").css('display','none')
				$(".appcommon .amylss").css('display','block');
				$(".appcommon .amyl2ss").hide();
				$(".amyl2").hide()
				$(".amylImg").show()
				$("#amylOne").show()
			} else if (window.location.host == 'amylc18.com' || window.location.host == 'www.amylc18.com'){
			// } else if (window.location.host == 'xb336.com' || window.location.host == 'www.xb336.com'){
				 $(".appcommon .amyl2ss").hide();
				 $(".amyl2").hide()
				 $(".jsdaili").css('display','block')
			 }else{
				$(".child-view").css('padding-top','2.3rem').css('padding-bottom','0')
			}
			 if(window.location.host.indexOf('jz8383.com') != -1){
			     $(".downLink").find('a').hide()
			 }
		 </script>
		 </c:when>
	</c:choose>
	</div>
<script>
	$(function () {
		if( '${domainFolder}' == 'b01202'){
			$("#wenxintishi").html('温馨提示：若是在下载APP过程中遇到任何问题，请联系24小时在线客服QQ：910188222')
		}
	})
</script>
</body>
</html>
