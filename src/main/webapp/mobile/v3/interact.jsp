<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/head.jsp" />
</head>
<body>
<input id="mobanId" value="${domainFolder }" type="hidden">
	<div class="page" id="page_draw_notice_jsp">
		<jsp:include page="include/barTab.jsp" />
		<header class="bar bar-nav">
			<a class="title">互动大厅</a>
		</header>
		<div class="contents pull-to-refresh-content" data-ptr-distance="55">
			<!-- 默认的下拉刷新层 -->
			<div class="pull-to-refresh-layer">
				<div class="preloader"></div>
				<div class="pull-to-refresh-arrow"></div>
			</div>
			<div class="list-block media-list">
				<ul>
					<li style=""><a href="${res }/active.do" class="item-link item-content">
					<div class="item-media">
							<img src="${res }/images/youhuihuodong.png">
					</div>
					<div class="item-inner">
						<div class="item-title-row">
							<div class="item-title" style="font-size: .7rem;line-height:30px;">优惠活动</div>
							<div class="item-after"></div>
						</div>
						<div class="item-text" style="line-height:30px;">优惠活动，点击查看</div>
						</div>
					</a>
				</li>
				<li id="clickChatLink" style="display:none;"><a href="#" onclick="openChatIframe()" class="item-link item-content">
					<div class="item-media">
							<img src="${res }/images/liaotianshi.png">
					</div>
					<div class="item-inner">
						<div class="item-title-row">
							<div class="item-title" style="font-size: .7rem;line-height:30px;">交流大厅</div>
							<div class="item-after"></div>
						</div>
						<div class="item-text" style="line-height:30px;">多人在线聊天</div>
						</div>
					</a>
				</li>
				<c:if test="${kfshow == 'on'}">
					<li id="serviceCode" style="display:block;"><a href="${res}/serviceCode.do"  class="item-link item-content">
						<div class="item-media" >
							<img src="${res }/images/serviceCode.png">
						</div>
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title" style="font-size: .7rem;line-height:30px;">客服二维码</div>
								<div class="item-after"></div>
							</div>
							<div class="item-text" style="line-height:30px;">微信和QQ二维码</div>
						</div>
					</a>
					</li>
				</c:if>
				<script>
					var mbid = $('#mobanId').val();
					if(mbid == 'b01202'){
						$('#clickChatLink').hide();
					}
				</script>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
<jsp:include page="include/need_js.jsp" />