<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when
		test="${version == 2 || version == 5 || bl.code == 'LHC' || bl.code == 'SFLHC' || bl.code == 'TMLHC' || bl.code == 'WFLHC'|| bl.code == 'HKMHLHC'|| bl.code == 'AMLHC'}">
		<nav class="bar bar-tab bett-foot">
			<div class="beet-foot-txt">
				<p>
					<span class="betDataAll"> 共<span class="betZhushu">0</span>注&nbsp;&nbsp;
						共<span class="orderTotalCost">0</span>元
					</span>
				</p>
			</div>
			<button id="PeiLvbetReadyBtn" class="btn-add">投注</button>
			<button id="PeiLvresetSelectBtn" class="btn-none">清空</button>
		</nav>
	</c:when>
	<c:otherwise>
		<nav class="bar bar-tab bett-foot">
			<div class="beet-foot-txt">
				<p>
					<span class="betDataAll"> 共<span class="betZhushu">0</span>注&nbsp;&nbsp;
						共<span class="orderTotalCost">0</span>元
					</span>
				</p>
			</div>
			<button id="betReadyBtn" class="btn-add">投注</button>
			<button id="resetSelectBtn" class="btn-none">清空</button>
		</nav>
	</c:otherwise>
</c:choose>