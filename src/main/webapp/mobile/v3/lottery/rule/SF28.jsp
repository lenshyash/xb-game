<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="gz_item">
	<jsp:include page="rule_base.jsp"></jsp:include>
	<div class="b_tit">游戏规则说明</div>
	<c:choose>
		<c:when test="${bl.identify == 1 || bl.identify == 3}">
			<%-- 奖金版本说明 --%>
			<strong id="dwd"> <span>定位胆</span>
			</strong>
			<p>
				<span>从第一、第二、第三位号选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第一位、第二位、第三位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：第一位1开奖号码：第一位1，即中定位胆第一位。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="bdw"> <span>不定位胆</span>
			</strong>
			<p>
				<span>选一个号码组成一注。</span>
			</p>
			<p>
				<span>从0-9中选择1个号码，每注由1号码组成，只要开奖号码的第一位、第二位、第三位中包含所选号码，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：12开奖号码：至少出现1和2各一个，即中不定位胆。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="sxzx"> <span>三星组选</span>
			</strong>
			<p>
				<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。注：开重复号码不算中奖。例：开奖号码161，投注号码126</span>
			</p>
			<p>
				<span>投注方案：2 6 9 开奖号码:2 6 9（顺序不限），即中三星组选。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong> <span id="sxfs">三星复式</span>
			</strong>
			<p>
				<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</span>
			</p>
			<p>
				<span>投注方案： 345 &nbsp;开奖号码: 345，即中三星直选。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="q2zx"> <span>前二组选</span>
			</strong>
			<p>
				<span>从第一位、第二位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的第一位、第二位相同，顺序不限，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：4 7 开奖号码:前二位4 7 （顺序不限），即中前二组选。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="q2zx_fs"> <span>前二复式</span>
			</strong>
			<p>
				<span>从第一位、第二位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：58开奖号码：前二位58，即中前二复式</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="h2zx"> <span>后二组选</span>
			</strong>
			<p>
				<span>从第二位、第三位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的第二位、第三位相同，顺序不限，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：4 7 开奖号码:后二位4 7 （顺序不限），即中后二组选。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="h2zx_fs"> <span>后二复式</span>
			</strong>
			<p>
				<span>从第二位、第三位各选一个号码组成一注。</span>
			</p>
			<p>
				<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：58开奖号码：后二位58，即中后二复式。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="hz"> <span>和值</span>
			</strong>
			<p>
				<span>从0-27中任意选择1个或1个以上号码。</span>
			</p>
			<p>
				<span>所选数值等于开奖号码相加之和，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：和值1开奖号码：001或010或100，即中和值。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="dxds"> <span>大小单双</span>
			</strong>
			<p>
				<span>和值的数字为大小单双</span>
			</p>
			<p>
				<span>和值数字0-12为小，15-27为大，奇数为单，偶数为双，13，14为合局</span>
			</p>
			<p>
				<span>所选大小单双与开奖和值相符，即为中奖</span>
			</p>

		</c:when>
		<c:otherwise>
			<%-- 赔率版本说明 --%>
			<h2>投注规则:</h2>
			<c:if test="${!pceggDrawShow  }">
				<style>
					.hidePcegg{
						display:none;
					}
				</style>
			</c:if>
			<dd>
				<ul>
					<li>大：三个位置的数值相加和大于等于14，15,16,17,18,19,20,21,22,23,24,25,26，27为大。
						<span class="hidePcegg">举例出14押大回本本金。</span></li>
					<li>小：三个位置的数值相加和小于等于0，01,02,03,04,05,06,07,08,09,10,11,12，13为小。
						<span class="hidePcegg">举例出13押小回本本金。</span></li>
					<li>单：数字1,3,5,7,9,11,13,15,17,19,21,23,25,27为单。<span class="hidePcegg">举例出13押单回本本金。</span></li>
					<li>双：数字0,2,4,6,8,10,12,14,16,18,20,22,24,26为双。<span class="hidePcegg">举例出14押双回本本金。</span></li>
					<li>大单（三个数值和）：15,17,19,21,23,25,27为大单。</li>
					<li>小单（三个数值和）：01,03,05,07,09,11,13为小单。<span class="hidePcegg">举例买100元小单开13就回本金。</span></li>
					<li>大双（三个数值和）：14,16,18,20,22,24,26为大双。<span class="hidePcegg">举例买100元大双开14就回本金。</span></li>
					<li>小双（三个数值和）：00,02,04,06,08,10,12为小双。</li>
					<li>极大（三个数值和）：22,23,24,25,26,27为极大。</li>
					<li>极小（三个数值和）：00,01,02,03,04,05为极小。</li>
					<li>红波（三个数值和）：03,06，09,12,15,18,21,24为红波。</li>
					<li>绿波（三个数值和）：01,04,07,10,16,19,22，25为绿波。</li>
					<li>蓝波（三个数值和）：02,05,08,11,17,20,23,26为蓝波。</li>
					<li>购买波色如果三个数值和为0,13,14,27视为不中奖</li>
					<li>特码包三（三个数值和）：可選取三個數字投注，選取三個數字當中任何一個與開獎結果相同即中獎，（比如:开奖结果 15
						选择 11 15 20,那就视为中奖）。</li>
					<li>特码（三个数值和）：單選取一個數字投注。</li>
					<li>豹子：當期開出三個數字相同即為豹子（ 例
						0+0+0=0,1+1+1=3,2+2+2=6,3+3+3=9,4+4+4=12,5+5+5=15,6+6+6=18,7+7+7=21,8+8+8=24，9+9+9=27）。
					</li>
				</ul>
			</dd>
		</c:otherwise>
	</c:choose>
</div>