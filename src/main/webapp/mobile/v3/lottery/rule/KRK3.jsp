<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="gz_item">
	<jsp:include page="rule_base.jsp"></jsp:include>
	<div class="b_tit">游戏规则说明</div>
	<c:choose>
		<c:when test="${bl.identify == 1 || bl.identify == 3}">
			<%-- 奖金版本说明 --%>
			<strong class="p1" id="hz">和值投注</strong>
			<p class="p1">是指对三个号码的和值进行投注，包括“和值3”至“和值18”投注。&nbsp;</p>
			<p class="p1">投注方案：15</p>
			<p class="p1">开奖号码:555</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="dxds">大小单双</strong>
			<p class="p1">是指对三个号码的总和进行投注，总和大于10为大，小于等于10为小&nbsp;</p>
			<p class="p1">投注方案：单</p>
			<p class="p1">开奖号码:111</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="sthtx">三同号通选（即全包）</strong>
			<p class="p1">是指对所有相同的三个号码（111、222、…、666）进行投注；&nbsp;</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="sthdx">三同号单选</strong>
			<p class="p1">是指从所有相同的三个号码（111、222、…、666）中任意选择一组号码进行投注。</p>
			<p>
				<br />
			</p>
			<strong class="p1" id="ethfx">二同号复选</strong>
			<p class="p1">是指对三个号码中两个指定的相同号码和一个任意号码进行投注；</p>
			<p>
				<br />
			</p>
			<strong class="p1" id="ethdx">二同号单选</strong>
			<p class="p1">是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注</p>
			<p>
				<br />
			</p>
			<strong class="p1" id="sbth">三不同号</strong>
			<p class="p1">是指对三个各不相同的号码进行投注。</p>
			<p>
				<br />
			</p>
			<strong class="p1" id="ebth">二不同号</strong>
			<p class="p1">是指对三个号码中两个指定的不同号码和一个任意号码进行投注</p>
			<p>
				<br />
			</p>
			<strong class="p1" id="slhtx">三连号通选（即全包）</strong>
			<p class="p1">是指对所有三个相连的号码（仅限：123、234、345、456）进行投注。</p>
			<p>
				<br />
			</p>
		</c:when>
		<c:otherwise>
			<%-- 赔率版本说明 --%>
			<h2>${bl.name}</h2>
			<c:if test="${!k3baoziDrawShow  }">
				<style>
					.hideKsan{
						display:none;
					}
				</style>
			</c:if>
			<dl>
				<dt>◎双面</dt>
				<dd>
					<ul>
						<li>以全部开出的三个号码、加起来的总和来判定。</li>
						<li>大小：三个开奖号码总和值11~17 为大；总和值4~10 为小；<span class="hideKsan">若三个号码相同、则不算中奖。</span></li>
						<li>单双：三个开奖号码总和值5,7,9,11,13,15,17为单；总和值4,6,8,10,12,14,16为双；<span class="hideKsan">若三个号码相同、则不算中奖。</span>
						</li>
					</ul>
				</dd>
				<dt>◎三军</dt>
				<dd>
					<ul>
						<li>三个开奖号码其中一个与所选号码相同时、即为中奖。举例：如开奖号码为1、1、3，则投注三军1或三军3皆视为中奖。 <font
							color="red">备注：不论当局指定点数出现几次，仅派彩一次(不翻倍)。 </font>
						</li>
					</ul>
				</dd>
				<dt>◎围骰/全骰</dt>
				<dd>
					<ul>
						<li>围骰：开奖号码三字同号、且与所选择的围骰组合相符时，即为中奖。</li>
						<li>全骰：全选围骰组合、开奖号码三字同号，即为中奖。</li>
					</ul>
				</dd>
				<dt>◎点数</dt>
				<dd>
					<ul>
						<li>开奖号码总和值为4、5、6、7、8、9、10、11、12、13、14、15、16、17
							时，即为中奖；若开出3、18，则不算中奖。举例：如开奖号码为1、2、3、总和值为6、则投注「6」即为中奖。</li>
					</ul>
				</dd>
				<dt>◎长牌</dt>
				<dd>
					<ul>
						<li>
							任选一长牌组合、当开奖结果任2码与所选组合相同时，即为中奖。举例：如开奖号码为1、2、3、则投注长牌12、长牌23、长牌13皆视为中奖。
						</li>
					</ul>
				</dd>
				<dt>◎短牌</dt>
				<dd>
					<ul>
						<li>
							开奖号码任两字同号、且与所选择的短牌组合相符时，即为中奖。举例：如开奖号码为1、1、3、则投注短牌1、1，即为中奖。</li>
					</ul>
				</dd>
				<dt>◎豹子</dt>
				<dd>
					<ul>
						<li>豹子：开奖的三个骰子点数相同为豹子，选择指定点数的豹子与开奖结果一致则中奖<span class="hideKsan">，其它不中奖</span>。</li>
						<li>如：投注豹子：666，开奖：666 则中奖<span class="hideKsan">，其它不中奖</span>。</li>

					</ul>

				</dd>
			</dl>
		</c:otherwise>
	</c:choose>
</div>