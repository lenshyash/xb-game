<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="gz_item">
	<jsp:include page="rule_base.jsp"></jsp:include>
	<div class="b_tit">游戏规则说明</div>
	<c:choose>
		<c:when test="${bl.identify == 1 || bl.identify == 3}">
			<%-- 奖金版本说明 --%>
			<strong class="p1" id="dwd">定位胆</strong>
			<p class="p1">从第一名到第十名任意位置上选择1个或1个以上号码。&nbsp;</p>
			<p class="p1">从第一名到第十名任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
			<p class="p1">投注方案： 第一名01</p>
			<p class="p1">开奖号码:第一名01，即中定位胆第一名。</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="q3zx_fs">前三复式</strong>
			<p class="p1">从第一名、第二名、第三名中各选择1个号码组成一注。&nbsp;</p>
			<p class="p1">选号与开奖号码按位猜对3位即中奖。</p>
			<p class="p1">&nbsp;投注方案： 01 02 03</p>
			<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
			<p class="p1">即中前三复式</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="q2zx_fs">前二复式</strong>
			<p class="p1">从第一名、第二名中至少选择1个号码组成一注。&nbsp;</p>
			<p class="p1">选号与开奖号码按位猜对2位即中奖。</p>
			<p class="p1">&nbsp;投注方案：01 02</p>
			<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
			<p class="p1">即中前二复式。</p>
			<p class="p2">
				<br />
			</p>
			<strong class="p1" id="q1zx_fs">前一复式</strong>
			<p class="p1">从第一名中至少选择1个号码组成一注。</p>
			<p class="p1">选号与开奖号码按位猜对1位即中奖。</p>
			<p class="p1">投注方案：01</p>
			<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
			<p class="p1">即中前一直选。</p>
			<p>
				<br />
			</p>
			<strong id="dxds"> <span>冠亚和值--大小单双</span>
			</strong>
			<p>
				<span>和值的数字为大小单双</span>
			</p>
			<p>
				<span>和值数字3-10为小，12-19为大，奇数为单，偶数为双</span>
			</p>
			<p>
				<span>所选大小单双与开奖和值相符，即为中奖</span>
			</p>
			<p>
				<br />
			</p>
			<strong id="gyhz"> <span>冠亚和值--和值</span>
			</strong>
			<p>
				<span>从3-19中任意选择1个或1个以上号码。</span>
			</p>
			<p>
				<span>所选数值等于开奖号码前两位相加之和，即为中奖。</span>
			</p>
			<p>
				<span>投注方案：和值15开奖号码前两位：0807或0906或1005，即中冠亚和值。</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="longhu_gunjun"> <span>冠军龙虎</span>
			</strong>
			<p>
				<span>选择龙虎作为投注号码</span>
			</p>
			<p>
				<span>“第一名”号码大于“第十名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
			</p>
			<p>
				<span>投注方案：龙，开奖号码：10********01</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="longhu_yajun"> <span>亚军龙虎</span>
			</strong>
			<p>
				<span>选择龙虎作为投注号码</span>
			</p>
			<p>
				<span>“第二名”号码大于“第九名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
			</p>
			<p>
				<span>投注方案：龙，开奖号码：*10******01*</span>
			</p>
			<p>
				<span><br /></span>
			</p>
			<strong id="longhu_yajun"> <span>季军龙虎</span>
			</strong>
			<p>
				<span>选择龙虎作为投注号码</span>
			</p>
			<p>
				<span>“第三名”号码大于“第八名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
			</p>
			<p>
				<span>投注方案：龙，开奖号码：**10****01**</span>
			</p>
			<p>
		</c:when>
		<c:otherwise>
			<%-- 赔率版本说明 --%>
			<h2>两面盘（冠军~第十名）</h2>
			<ul>
				<li>大小∶开出之号码大於或等於6为大，小於或等於5为小。</li>
				<li>单双∶开出之号码为双数叫双，如4、8；号码为单数叫单，如5、9。</li>
				<li>冠军 龙/虎∶"第一名"之车号大於"第十名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
				<li>亚军 龙/虎∶"第二名"之车号大於"第九名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
				<li>季军 龙/虎∶"第三名"之车号大於"第八名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
				<li>第四名 龙/虎∶"第四名"之车号大於"第七名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
				<li>第五名 龙/虎∶"第五名"之车号大於"第六名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
			</ul>
			<h2>两面盘（冠亚和）: "冠军车号 + 亚军车号 = 冠亚和"</h2>
			<ul>
				<li>冠亚和大小∶以冠军车号和亚军车号之和大小来判断胜负。"冠亚和 "大於11为大，小於11为小${dx11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
					<li>冠亚和单双∶以冠军车号和亚军车号之和单双来判断胜负，"冠亚和"为双数叫双，为单数叫单${ds11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
					<li>冠亚和指定:以指定冠军车号和亚军车号之和来判断胜负；冠亚车号总和可能出现结果为3~19，投中对应"冠亚和指定"数字视为中奖，其馀情形视为不中奖</li>
			</ul>
			<h2>冠军~第十名车号指定:</h2>
			<ul>
				<li>冠军~第十名车号指定: 每一个车号为一投注组合，开奖结果"投注车号"对应所投名次视为中奖，其馀情形视为不中奖。</li>
			</ul>
		</c:otherwise>
	</c:choose>
</div>