<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="page" id='doIntelligentSelectionPage'
	style="background: #f6f6f6;">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-left back"
			href="${res}/bet_lotterys.do?lotCode=${bl.code}"> <span
			class="icon icon-left"></span>返回
		</a> <a class="title">智能追号</a>
	</header>

	<div class="bar zhuhaobox">
		<input id="fbDlgStopBet" type="checkbox" class="vm" value="1"
			checked="checked"> 中奖后停止追号 &nbsp;&nbsp;&nbsp; 追
		<div class="btngroup">
			<em class="jiabtn btnjiaj">-</em> <input id="qihaoCount" type="number"
				class="jiajianinpt" value="10"> <em class="jianbtn btnjiaj">+</em>
		</div>
		期
	</div>
	<nav class="bar bar-tab bett-foot">
		<div class="beet-foot-txt">
			<p>
				<em id="fbCount">0</em>期 <em id="fbCash" class="hong">0</em>元
			</p>
		</div>
		<button id="fbBet" class="btn-add">投注</button>
	</nav>
	<div class="content doIntelligentSelectionContent">
		<table class="zhuihaotable">
			<thead>
				<tr class="thtrbox">
					<td width="15%">序号</td>
					<td width="35%">倍数</td>
					<td width="60%">累计投入</td>
				</tr>
			</thead>
			<tbody id="fbOrderList">
			</tbody>
		</table>
	</div>
</div>
<script type="text/html" id="bettraceTpl">
	<tr class="tbtrbox">
		<td>{{index}}</td>
		<td>
			<div class="btngroup">
				<em class="jiabtn btnjiaj">-</em>
				<input type="number" class="jiajianinpt" value="{{bs}}">
				<em class="jianbtn btnjiaj">+</em>
			</div>
		</td>
		<td class="betInfo">
			<div>
				<span>期数</span>
				<span class="round">{{qh}}</span>
				<span style="color:red">{{if !curs}}[当前期]{{/if}}</span>
			</div>
			<div>
				<span>投入金额</span>
				<span class="betCost hong">--</span>元
			</div>
			<div>
				<span>累计投入</span>
				<span class="betTotalCost hong">--</span>元
			</div>
		</td>
	</tr>
</script>