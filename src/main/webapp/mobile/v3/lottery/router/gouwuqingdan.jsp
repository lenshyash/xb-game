<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="page" id='buyCardPage' style="background: #f6f6f6;">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-left back"
			href="${res}/bet_lotterys.do?lotCode=${bl.code}"> <span
			class="icon icon-left"></span>返回
		</a> <a class="title">${bl.name} 购彩列表</a>
	</header>
	<nav class="bar bar-tab bett-foot">
		<div class="beet-foot-txt">
			<p>
				共<span id="bcTotalCount">0</span>注&nbsp;&nbsp;共<span id="bcTotalMoney">0</span>元
			</p>
		</div>
		<button id="bcBet" class="btn-add">投注</button>
		<c:if test="${bl.code != 'FC3D' && bl.code != 'PL3'}">
			<button id="toznzhuihbtn" class="btn-none zhinegzhuihaobtn">智能追号</button>
		</c:if>
	</nav>
	<div class="content">
		<div class="row no-gutter tzdansanbtn">
			<div class="tzdansanbtnitem">
				<a href="${res}/bet_lotterys.do?lotCode=${bl.code}" class="back"><em
					class="iconfont icon-plus"></em>再选一注</a>
			</div>
			<div class="tzdansanbtnitem">
				<a id="bcSelectOne" href="javascript: void(0);" class=""><em
					class="iconfont icon-plus"></em>机选一注</a>
			</div>
			<div class="tzdansanbtnitem">
				<a id="bcClear" href="javascript: void(0);" class="">清空列表</a>
			</div>
		</div>
		<div class="tiaobox">
			<div class="tiaoneibox">
				<ul class="tiaoneiul" id="orderList">
					<%--<li style="display: none;"><a href="javascript: void(0);"
						class="">投注号码为空</a></li> --%>
				</ul>
				<div class="xieyi-a">&nbsp;</div>
			</div>
		</div>
	</div>
</div>
<script type="text/html" id="gouwucheTpl">
	<li buyinfo="{{buyInfo}}" c="{{playCode}}" m="{{money_count}}" r="{{jiangjin}}" z="{{zhushu_count}}">
		<div class="haoma">{{if list.optional}}{{list.optional}}@{{list.myarray}}{{else}}{{list.myarray}}{{/if}}</div>
		<div>{{bs}}倍 {{yjf_model}}</div>
		<div>{{lottery_wanfa}} {{zhushu_count}}注 {{money_count}}元</div> <em class="icons icons-remove"></em>
	</li>
</script>