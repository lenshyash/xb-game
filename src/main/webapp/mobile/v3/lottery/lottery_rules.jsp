<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
<input type="hidden" value="${res}/bet_lotterys.do?lotCode=${bl.code}" id="resLocaUrl"/>
	<div class="page" id="page_lottery_rules_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="#" onclick="iosAnroidHome()"> <span
				class="icon icon-left"></span>返回
			</a> <a class="title">${bl.name}-玩法规则</a>
		</header>
		<div class="content content_rule" style="background: #FFF;">
			<div class="list-block" style="margin:0;position: fixed;width:100%;">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">全部彩种</div>
								<div class="item-input">
									<select>
										<c:forEach items="${blList}" var="bls">
											<option value="${bls.code}" <c:if test="${bls.code == bl.code }">selected</c:if>>${bls.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<!-- 这里是页面内容区 -->
			<jsp:include page="rule/${bl.code}.jsp" />
		</div>
	</div>
</body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="../include/need_js.jsp" />