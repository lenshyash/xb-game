<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
<style type="text/css">
	.roomNameAnimate{
   		line-height: 50px;
   		-webkit-transform: translate3d(0,0,0);
   		transform: translate3d(0,0,0);
   		-webkit-transition: all .6s ease;
   		transition: all .6s ease;
  	}
</style>
</head>
<body>
<input type="hidden" value="${bl.code}" id="lotteryCodeIos"/>
<input type="hidden" value="${bl.name}" id="lotteryNameIos"/>
<input type="hidden" value="${bl.type}" id="lotteryTypeIos"/>
<input type="hidden" value="${domainFolder}" id="domainFolder"/>
<input type="hidden" value="${res}" id="resLocaUrl"/>
<input type="hidden" value="${dtSum}" id="dtSumValue">
<input type="hidden" value="${bl.showFlag}" id="showFlag">
<div id="lotteryLoadImg" style="position: absolute;left:0;right:0;bottom:0;top:0;background: rgba(0,0,0,0.6);z-index:99999999;display: none;align-items: center;justify-content: center;">
	<img src="${res}/images/loading-1.gif"/>
</div>
<style>
		#openChat{
			width:40px!important;
			top:52px!important;
		}
	</style>
		<c:choose>
			<c:when test="${version == 2 || version == 5 || bl.code == 'LHC' || bl.code == 'SFLHC' || bl.code == 'TMLHC' || bl.code == 'WFLHC'|| bl.code == 'HKMHLHC'|| bl.code == 'AMLHC'}">
				<div class="page" id="page_bet_lotterys_version2_jsp">
			</c:when>
			<c:otherwise>
				<div class="page" id="page_bet_lotterys_version1_jsp">
			</c:otherwise>
		</c:choose>
		<header class="bar bar-nav bar-nav-height">
			<a class="button button-link button-nav pull-left back"
				href="${base}/mobile"> <span
				class="icon icon-left"></span>返回
			</a>
			<div class="roomNameAnimate">
			 	<a class="title">${bl.name}<span class="icon icon-downs"></span></a>
			 	<a class="title" id="groupAndPlayName" style="top: 2.2rem;"></a>
			</div>
			<div class="ui-bett-right">
				<a class="bett-head pull-right open-panel" data-panel=".panel-right"></a>
			</div>
		</header>
		<jsp:include page="bet_lottery_nav.jsp"></jsp:include>
		<div id="showHideNav" style="position: absolute;top:2.2rem;width:100%;z-index: 100;background: #FFFFFF;opacity: 0;">
			<div class="card list-block-navTab swiper-container">
				<div class="card-content-inner swiper-wrapper">
					<div class="swiper-slide active">
						<span>加载中...</span>
					</div>
				</div>
			</div>
			<div class="card list-block-palyNavTab swiper-container1">
				<div class="card-content-inner swiper-wrapper">
					<div class="swiper-slide active">
						<span>加载中...</span>
					</div>
				</div>
			</div>
		</div>
		<div class="kuaijie" style="position: absolute;bottom: 2.5rem;background: #FFF;display: none;z-index:9999;">
			<div class="form-group" style="padding: .1rem .5rem;">
				<div class="input-group">
					<span class="input-group-addon form_span">快捷金额</span> 
					<input type="number" class="form-control" id="kjmoney" placeholder="请输入投注金额" >
				</div>
			</div>
		</div>
		<script>
			$(function(){
				$(".kuaijie").click(function(){
					$("#kjmoney").focus();
				})
				$(document).on('focusin', function () {
					$(".kjybtz").css('position','absolute')
				});
					  
				$(document).on('focusout', function () {
					$(".kjybtz").css('position','fixed')
				});
			})
			/* function beishuText(){
				$("#beishuInput").focus();    
				$("#beishuInput").focus(function(){
					 $("#phoneBsText").hide();
				});
				$("#beishuInput").blur(function(){
					 $("#phoneBsText").show();
				});
			} */
		</script>
		<div class="card card-top">
			<div class="card-content">
				<div class="card-content-inner">
					<ul class="bet-room-down">
						<li class="roomLi">
							<p>第<font id="issue">加载中...</font>期<font id="kp_jz">截止</font>
							</p>
							<div class="time-late" id="bpCountDown">
								<span>0</span><span>0</span><span class="time-kong"></span><span>0</span><span>0</span><span
									class="time-kong"></span><span>0</span><span>0</span>
							</div>
						</li>
						<li><c:choose>
								<c:when
									test="${version == 2 || version == 5 || bl.code == 'LHC' || bl.code == 'SFLHC' || bl.code == 'TMLHC' || bl.code == 'WFLHC' || bl.code == 'HKMHLHC'}">
									<p>余额</p>
									<p>
										<span class="icoAcer" id="nowMoney">0</span>
									</p>
								</c:when>
								<c:otherwise>
									<p>
										<span id="versionJJName">奖金：</span><span id="versionJJPeiLv">加载中...</span>
									</p>
									<p style="margin-top: .2rem;">
										<span style="color: #000;">余额：</span><span id="nowMoney">0</span>元
									</p>
								</c:otherwise>
							</c:choose></li>
					</ul>
				</div>
			</div>
			<script>
			function lotteryReplaceMoney(string){
				$("#nowMoney").html(string)
			}
			</script>
			
			<div class="card-footer">
				<span style="width: 100%;">第<font id="versionQiHao"
					color="red">加载中...</font>期 <span class="openHaoMa"></span>
				</span> <%--<em></em> --%>
			</div>
		</div>
		<c:choose>
			<c:when test="${bl.code == 'BJSC' || bl.code =='SFSC' || bl.code =='SFFT' || bl.code =='FFSC' || bl.code =='WFSC' || bl.code =='WFFT' || bl.code =='LBJSC' || bl.code == 'XYFT'  || bl.code == 'LXYFT'  || bl.code == 'AZXY10' || bl.code == 'LHC' || bl.code == 'SFLHC' || bl.code == 'TMLHC' || bl.code == 'HNKLSF' || bl.code == 'GDKLSF' || bl.code == 'HKMHLHC'|| bl.code == 'AMLHC'}">
				<div class="content" id="content" style="top: 7.5rem;">
			</c:when>
			<c:when test="${bl.code == 'CQXYNC'}">
				<div class="content" id="content" style="top: 7.9rem;">
			</c:when>
			<c:otherwise>
				<div class="content" id="content" style="top: 6.9rem;">
			</c:otherwise>
		</c:choose>
			<c:choose>
				<c:when
					test="${version == 2 || version == 5 || bl.code == 'LHC' || bl.code == 'SFLHC' || bl.code == 'TMLHC' || bl.code == 'WFLHC'|| bl.code == 'HKMHLHC'|| bl.code == 'AMLHC'}">
					<div class="card card-navTab" style="margin-bottom: .2rem;">
						<div class="card-content">
							<div class="card-content-inner"></div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="card card-navTab">
						<div class="card-header">
							<a href="javascript: void(0);"
								class="button button-dark pull-left open-popup open-playdesc"
								data-popup=".popup-playdesc">玩法说明</a>
							<a href="javascript: void(0);"
								class="button button-dark pull-left" id="nowWinLost">今日输赢</a>
							<a id="goBuyCartBtn"
								href="javascript: void(0);"
								class="button button-dark pull-right buyCartCount"
								style="display: none;">返回购彩清单(<span id="orderCountShow">0</span>)
							</a> <a id="bpRandom" href="javascript: void(0);"
								class="button button-dark pull-right" style="display: inline;"><em
								class="iconfont icon-ttpodicon"></em>机选</a>
						</div>
						<div class="card-content">
							<div class="card-content-inner"></div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="zlBack" id="shareSelect" >
		    <div class="ziliaoN">
		        <div class="zlhead">温馨提示</div>
		        <div class="zlindex">
					<img src="${res}/images/green.png">
					<span>下注成功!</span>
		        </div>
		        <div class="zlfooter">
		            <span type="0" class="zlfooterBut zlfooterBut1">分享注单</span>
		            <span type="1" class="zlfooterBut zlfooterBut1">继续投注</span>
		        </div>
		    </div>
		</div>
		<div class="zlBack" id="sheetSuccess" >
		    <div class="ziliaoN">
		        <div class="zlhead">温馨提示</div>
		        <div class="zlindex">
					<img src="${res}/images/green.png">
					<span>分享成功!</span>
		        </div>
		        <div class="zlfooter">
		            <span type="0" class="zlfooterBut zlfooterBut2">互动大厅</span>
		            <span type="1" class="zlfooterBut zlfooterBut2">继续投注</span>
		        </div>
		    </div>
		</div>
		<input value="${yjf }" style="display:none;" class='danweiIs'>
    <style>
    	.zlBack{
    	display:none;
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
    background: #00000036;
    /*text-align: center;*/
}
.ziliaoN{
    background: #FFF;
    width: 240px;
    margin: 255px auto;
    font-size: 14px;
    border-radius: 5px;
}
.zlindex>img{
	height:20px;
}
.zlhead{
    height: 38px;
    line-height: 50px;
    text-align: center;
}
.zlindex{
	display: flex;
    padding: 20px 30px;
    max-height: 50vh;
    font-size: 22px;
    word-wrap: break-word;
    color: #666;
    line-height: 22px;
    border-radius: 5px;
    text-align: center;
    justify-content:center;
}
.zlfooter span:nth-last-of-type(2)
{
    width: 48%;
    height: 39px;
    border-radius: 0 0 0 3px;
    border-right: 1px solid #f1f1f1;
    color:#4a95e4 ;
}
.zlfooter span:nth-of-type(2){
    color: #999;
    width: 48%;
    height: 39px;
    border-radius: 0 3px 0 0;
}
.zlfooter span{
    position: relative;
    display: inline-block;
}
.zlfooter{
    position: relative;
    height: 40px;
    line-height: 40px;
    text-align: center;
    border-top: 1px solid #EBEBEB;
}
    </style>
		<script type="text/html" id="gameGroupTpl">
		{{each list as value i}}
			<div class="swiper-slide {{if i == 0}}active{{/if}}" data-code="{{value.code}}" data-type="{{value.lotType}}" data-id="{{value.id}}">
				<span>{{value.name}}</span>
			</div>
		{{/each}}
		</script>
		<script type="text/html" id="gamePlayTpl">
		{{each list as value i}}
			<div class="swiper-slide {{if i == 0}}active{{/if}}" data-code="{{value.code}}" data-type="{{value.lotType}}" data-groupid="{{value.groupId}}" data-id="{{value.id}}">
				<span>{{value.name}}</span>
			</div>
		{{/each}}
		</script>
		<script type="text/html" id="drawNoticeDetailsDataTpl">
		{{each list as value i}}
		<li style="{{i%2!=0?'background:#f4f4f4':''}}">
			<div class="item-content">
				<div class="item-inner">
				<div class="item-title-row" style="font-size: .7rem;">
					<div class="item-title">第{{value.qiHao}}期</div>
					<div class="item-after">{{value.date}}</div>
				</div>
				<div class="item-subtitle" style="line-height: 1.8rem;">{{$tplHistoryHaoMas value.haoMa}}</div>
				<div class="item-subtitle item-mark">{{$tplHaoMasLHC value.haoMa,value.date.substring(0,10)}}</div>
				</div>
			</div>
		</li>
		{{/each}}
		</script>
		<script type="text/html" id="lotteryV2DataTpl" >
			<table style="width:100%;">
				<thead>	
					<tr style="background-color: #fcf8e3;">
						<th style="line-height:1.7rem;">类型</th>
						<th style="line-height:1.7rem;">号码</th>
						<th style="line-height:1.7rem;">金额</th>
					</tr>
				</thead>
				<tbody>
					{{each list as value i}}
						<tr {{if i%2!=0}}style="background-color:#f6f6f6;"{{/if}}>
							<td style="line-height:1.7rem;">{{leixing}}</td>
							<td style="line-height:1.7rem;">{{value.name}}</td>
							<td style="line-height:1.7rem;" class="totalNum1">{{value.money}}</td>
						</tr>
					{{/each}}
						<tr class="typeConversion">
							<td style="line-height:1.7rem;">单位： </td>
							<td style="line-height:1.7rem;">
								<span class="danwei danweiC" data="1">元</span>
								<span class="danwei" data="10">角</span>
								<span class="danwei waneModel" data="100">分</span>
							</td>
						</tr>
						<tr style="background-color: #fcf8e3;">
							<td colspan="3" style="line-height:1.7rem;">共<span class="red">{{total}}</span>注&nbsp;共<span class="red totalNum">{{totalM}}</span>元</td>
						</tr>
				</tbody>
			</table>
		</script>
		<script type="text/html" id="ZDXXtpl">
			<div class="list-block" style="margin:0;" id="lotteryBetInfo">
				<ul>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">玩法类型：{{lotteryWf}}</div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">购买号码：{{buyHm}}</div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">{{_jj}}{{_fd}}</div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">倍数：</div>
							<div class="item-input">
                				<input class="beet-money-int" id="beishuInput" style="text-align:left;" type="text" value="1" maxlength="7"">
								<div id="phoneBsText" style="width:100%;height:43px;position: absolute;bottom:0;z-index:9999;display:none;" onclick="beishuText()"></div>
              				</div>
							<div class="item-title" style="margin-left:.5rem;padding-right:.5rem;">倍</div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner" id="lotteryBetModel">
          					单位:
								{{if bet_model == '111'}}
									<a class="money-unit active" data-unit="1" href="javascript:;">元</a>
									<a class="money-unit" data-unit="10" href="javascript:;">角</a>
									<a class="money-unit" data-unit="100" href="javascript:;">分</a>
								{{else if bet_model == '110'}}
									<a class="money-unit active" data-unit="1" href="javascript:;">元</a>
									<a class="money-unit" data-unit="10" href="javascript:;">角</a>
								{{else}}
									<a class="money-unit active" data-unit="1" href="javascript:;">元</a>
								{{/if}}
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">下注注数：<span class="betZhushu">{{betCount}}</span>注</div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          					<div class="item-title">下注总额：<span class="orderTotalCost">{{betMoney}}</span>元</div>
        				</div>
      				</li>
				</ul>
			</div>
		</script>
		<script type="text/html" id="nowWinLostTpl">
			<div class="list-block" style="margin:0">
				<ul>	
					<li class="item-content">
        				<div class="item-inner">
          				<div class="item-title">今日消费:</div>
          				<div class="item-after" style="margin-left:1rem;"><font color="red">{{allBetAmount}}元</font></div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          				<div class="item-title">今日中奖:</div>
          				<div class="item-after" style="margin-left:1rem;"><font color="red">{{allWinAmount}}元</font></div>
        				</div>
      				</li>
					<li class="item-content">
        				<div class="item-inner">
          				<div class="item-title">今日盈亏:</div>
          				<div class="item-after" style="margin-left:1rem;"><font color="red">{{yingkuiAmount}}元</font></div>
        				</div>
      				</li>
				</ul>
			</div>
		</script>
		<jsp:include page="../include/need_js.jsp" />
		<script type="text/javascript">
			var animateName = true;
			var BetLotterys = {
					lotCode:'${bl.code}',
					lotType:${bl.type},
					dxdsSetting:'${dxdsSetting}',
					bet_model:'${yjf}',
			}
			
			//header 滚动
			window.setInterval(function() {
				if (animateName) {
					//加载大类小类到头部进行滚动
					var name = MobileV3.resultGroupAndPlayName();
					$('.roomNameAnimate').css('transform',
							'translate3d(0,-2.2rem,0)');
					$("#groupAndPlayName").text(name);
					animateName = false;
				} else {
					$('.roomNameAnimate')
							.css('transform', 'translate3d(0,0,0)');
					animateName = true;
				}
			}, 2000);
			$(function(){
				var val = $('#beishuInput').val()
				$('#beishuInput').focus();
				$('#beishuInput').val('');
				$('#beishuInput').val(val);
			})
		</script>
		<style>
			.danwei{
				width: 1.6rem;
			    text-align: center;
			    height: 1.2rem;
			    line-height: 1.2rem;
				color: #666;
			    background: #f3f3f3;
			    border: 1px solid #ddd;
			    display: inline-block;
			    margin-right: -0.3rem;
				
			}
			.danweiC{
			    background: #dc5d55;
			    color: #fff;
			    border: 1px solid #dc5d55;
			}
		</style>
	</div>
	<!-- 选中 -->
	<audio id="sound_bet_select"
		src="${res}/js/map/bet_select.mp3" controls="controls"
		hidden="true"></audio>
	<jsp:include page="../lottery/popup/lotterys_wfsm_popup.jsp"></jsp:include>
	<jsp:include page="../lottery/popup/draw_notice_popup.jsp"></jsp:include>
	<jsp:include page="../lottery/popup/open_right_lottery_all_popup.jsp"></jsp:include>
	<c:if test="${version != 2 && bl.code != 'LHC' && bl.code != 'SFLHC' && bl.code != 'TMLHC' && bl.code != 'WFLHC'&& bl.code != 'HKMHLHC' && bl.code != 'AMLHC'&& version != 5}">
	<jsp:include page="../lottery/router/gouwuqingdan.jsp"></jsp:include>
	<jsp:include page="../lottery/router/zhuihao.jsp"></jsp:include>
	</c:if>
</body>
</html>
