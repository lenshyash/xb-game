<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>${lotName}结果走势图</title>
    <link href="${base }/common/template/lottery/trendchartnew/css/z_style.css?v=1.2" rel="stylesheet" type="text/css">
    <link href="${base }/common/template/lottery/trendchartnew/css/datepicker.css" rel="stylesheet">
    <link href="${base }/common/template/lottery/trendchartnew/css/common.css?v=1.0.1" rel="stylesheet">
    <script src="${base }/common/template/lottery/trendchartnew/js/jquery.min.js"></script>
<body>
<style>
	.cl-40 var i{
		position: inherit;
	}
	.noFixedQh{
		position: absolute;
		background:#fff;
		margin-left:-1px;
		z-index:1;
	}
	.noFixedJh{
		margin-left:100px;
	}
</style>
<div id="box" class="qc">
    <input type="hidden" value="${res}/bet_lotterys.do?lotCode=${lotCode}" id="resLocaUrl"/>
    <script>
        var isLocaApp = false;
        try{isLocaApp = android.isAndroidApp()}catch(err){}
        var picCallback = function(photos) {
            try{
                isLocaApp = photos
            }catch(err){}
        }
        var resLocaUrl = $("#resLocaUrl").val()
        function iosAnroidHome() {
            var browser = {
                versions: function() {
                    var a = navigator.userAgent,
                        b = navigator.appVersion;
                    return {
                        trident: a.indexOf("Trident") > -1,
                        presto: a.indexOf("Presto") > -1,
                        webKit: a.indexOf("AppleWebKit") > -1,
                        gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                        mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                        ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                        android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                        iPhone: a.indexOf("iPhone") > -1,
                        iPad: a.indexOf("iPad") > -1,
                        webApp: a.indexOf("Safari") == -1
                    }
                }(),
                language: (navigator.browserLanguage || navigator.language).toLowerCase()
            };
            if (browser.versions.android) {
                //Android
                if(!isLocaApp){
                    window.history.back()
                }else{
                    android.webBack('back');
                }
            } else if (browser.versions.ios) {
                //ios
                if(!isLocaApp){
                    window.history.back()
                }else{
                    WTK.share(JSON.stringify({'method':'back'}));
                }
            }else{
                window.history.back()
            }
        }
    </script>
    <div class="qc" id="Right">
        <div class="main" style="width: 100% ; overflow: auto">
            <div class="main-box no_${lotBallNum}-box">
                <div class="main-nav" style="position:fixed;width:100%;z-index:20;">
                    <div class="cplogo" style="width:75px;" onclick="iosAnroidHome()">
	                    <img src="${base }/common/template/lottery/trendchartnew/images/goOut.png" alt="" style="margin-left:5px;margin-right:0px;">
	                    <span>返回</span>
                    </div>
                    <div class="cplogo"  style="width:150px;">
                    <c:if test="${empty lotImgUrl }">
                    <img src="${base }/common/template/lottery/trendchartnew/images/${lotCode}.png" alt="">
                    </c:if>
                    <c:if test="${not empty lotImgUrl }">
                    	<img src="${lotImgUrl }" alt="">
                    </c:if>
                    <span>${lotName}</span></div>
                    <div class="czqh">
                        <span class="curretSsc">切换彩种</span><span class="xiajian"></span>
                        <div class="min" style="display: none;">
                            <dl>
                                <dt>
                                    <span class="iconOne"></span>
                                    高频彩：
                                </dt>
                                <dd>
                                    <ul>
                                        <c:forEach items="${highLotList}" var="lot">
                                            <li>
                                                <a href="${base}/mobile/v3/bet_lotterys_trends.do?lotCode=${lot.code}">${lot.name}</a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </dd>
                            </dl>
                            <dl>
                                <dt>
                                    <span class="iconTwo"></span>
                                    低频彩：
                                </dt>
                                <dd>
                                    <ul>
                                        <c:forEach items="${lowLotList}" var="lot">
                                            <li>
                                                <a href="${base}/mobile/v3/bet_lotterys_trends.do?lotCode=${lot.code}">${lot.name}</a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <%--<ul class="btn-navs">
                        <li class="fff" data-contentid="zhexianData" data-id="shuju1">基本走势</li>
                        <li data-contentid="shuju2" data-id="shuju2">K线图</li>
                        <li data-id="shuju5">历史开奖数据</li>
                    </ul>
                    <div class="rule"><a href="#" target="_blank">玩法规则</a></div>--%>

                </div>


                <div class="qhbox" style="margin-top:38px;">
                    <div class="search">
                        <!-- <ul class="search-left">
                            <li><label>遗漏数据<input id="checkboxYlsj" checked="checked" type="checkbox"></label></li>
                            <li><label>遗漏分层<input id="checkboxYlfc" type="checkbox"></label></li>
                            <li class="zhe"><label>折线<input id="checkboxZhexian" checked="checked"
                                                            type="checkbox"></label></li>
                        </ul> -->
                        <c:if test="${lotCode != 'LHC' and lotCode != 'FC3D' and lotCode != 'PL3'}">
                            <ul class="search-right" style="margin-left:auto;position: fixed;z-index:10;">
                                <!-- <li>
                                    <div><b>查询：</b></div>
                                </li> -->
                                <li>
                                    <div class="qi xuan">
                                        <a href="javascript:void(0)" class="on30qi">近30期</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="qi">
                                        <a href="javascript:void(0)" class="on50qi">近50期</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="qi">
                                        <a href="javascript:void(0)" class="on100qi">近100期</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="qi2">
                                        <a href="javascript:void(0)" class="on200qi">前天</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="qi2">
                                        <a href="javascript:void(0)" class="on200qi">昨天</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="qi2 xuan2">
                                        <a href="javascript:void(0)" class="on200qi">今天</a>
                                    </div>
                                </li>
                            </ul>
                        </c:if>
                    </div>
                    <div id="htmlContent">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="xiajiao">
        快速购买 安徽快3
    </div>
    <div class="mb">
    </div>
</div>

<%--<canvas class="no_${lotBallNum}-canvas-nomal" id="canvas"></canvas>
&lt;%&ndash;北京赛车&ndash;%&gt;
<canvas class="no_${lotBallNum}-canvas-nomal2" id="canvas2"></canvas>--%>

<input type="hidden" value="${lotCode}" id="lotCode">
<input type="hidden" value="${lotBallNum}" id="lotBallNum">
<script src="${base }/common/template/lottery/trendchartnew/js/dateFormat.js"></script>
<script src="${base }/common/template/lottery/trendchartnew/js/global.js?v=1.0.1"></script>
<script src="${base }/common/template/lottery/trendchartnew/js/layer.js"></script>
<input type="hidden" value="${dx11Setting }" id="dx11Setting" />
<input type="hidden" value="${ds11Setting }" id="ds11Setting" />
<script>
    // 闪烁
    var blinkColorArr = "#fa6200|#0f3f94".split("|");
    $(".blink").each(function () {
        var obj = $(this);
        setInterval(function () {
            var tmpColor = blinkColorArr[parseInt(Math.random() * blinkColorArr.length)];
            $(obj).css("color", tmpColor);
        }, 200);
    });

</script>
<script src="${base }/common/template/lottery/trendchartnew/js/echarts.min.js"></script>
<script src="${base }/common/template/lottery/trendchartnew/js/htmlHandel/no_${lotBallNum}_baseJs.js?v=1.3"></script>
<script>
    $.extend({
        getStrResouce: function (url) {
            var resourceContent = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "text",
                data: {},
                async: false, // 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。注意，同步请求将锁住浏览器，用户其它操作必须等待请求完成才可以执行。
                success: function (result) {
                    resourceContent = result;
                }
            });
            return resourceContent;
        }
    });
    var lotCode = $("#lotCode").val();
    var pageSize = 30;
    var recentDay = 1;
    $(function () {
        var startT = 0;
        $(".search-right .qi a").click(function () {
            $(".search-right .qi.xuan").removeClass("xuan");    //方法从被选元素移除一个或多个类.
            $(this).parent().addClass("xuan");   //addClass向被选元素添加一个或多个类 //parent)_返回被选元素的直接父元素。
            var flag = $(this).html();   //html()返回或设置被选元素的内容
            if (flag == "近30期") {
                pageSize = 30;
            } else if (flag == "近50期") {
                pageSize = 50;
            } else if (flag == "近100期") {
                pageSize = 100;
            } else if (flag == "近150期") {
                pageSize = 150;
            } else if (flag == "近200期") {
                pageSize = 200;
            }
            ajaxGetData(lotCode, pageSize, recentDay);
        });

        $(".search-right .qi2 a").click(function () {
            $(".search-right .qi2.xuan2").removeClass("xuan2");    //方法从被选元素移除一个或多个类.
            $(this).parent().addClass("xuan2");   //addClass向被选元素添加一个或多个类 //parent)_返回被选元素的直接父元素。
            var flag = $(this).html();   //html()返回或设置被选元素的内容
            if (flag == "今天") {
                recentDay = 1;
            }
            else if (flag == "昨天") {
                recentDay = 2;
            } else if (flag = "前天") {
                recentDay = 3;
            }
            ajaxGetData(lotCode, pageSize, recentDay);
        });
    });

    function showLoading() {
        layer.load(2, {
            shade: [0.1, '#000'] //0.1透明度的白色背景
        })
    }

    function hideLoading() {
        layer.closeAll();
    }

    $(function () {
        hideLoading();
    });

    $(function () {
        //悬停
        $(".czqh").hover(function () {
            $(".min").show()
        }, function () {
            $(".min").hide()
        });
    });

    $(function () {
        var right_w = $(".shuoming .shuomingright").outerWidth();
        var right_h = $(".shuoming .shuomingright").outerHeight();
        var left_h = $(".shuoming .shuomingleft").outerHeight();
        if (right_h > left_h) {
            $(".shuoming .shuomingleft").css("height", right_h);
        }
        var zong_w = $(".shuoming").outerWidth();
        var left_w = zong_w - right_w - 5;
        $(".shuoming .shuomingleft").css("width", left_w);
    });

    //加载模板
    function getTemContext(lotBallNum , lotCode) {
        var resurce = $.getStrResouce("${base }/common/template/lottery/trendchartnew/template/no_" + lotBallNum + "_baseTrend.html?v=1.0.2");
        $("#htmlContent").html(resurce);
    }

    $(function () {

        getTemContext($("#lotBallNum").val(),$("#lotCode").val());
        $(".btn-navs li").click(function (e) {
            $(".shuju").hide();
            $("#" + $(this).data("id")).show();
            $(this).attr("class", "fff").siblings().removeClass();
            if ($(this).data("id") == 'shuju1' || $(this).data("id") == 'shuju2' || $(this).data("id") == 'shuju3') {

            } else {

            }
        });

        var type = '1';
        if (typeof type == 'undefined' || type == '' || isNaN(type)) {
            type = 1;
        } else {
            type = Tools.parseInt(type);
        }
        $("[data-id='shuju" + type + "']").trigger("click");
        /*$(".search-right .qi a").eq(0).trigger("click");*/
        ajaxGetData(lotCode, pageSize, recentDay);
    });
    var xyk3OnOff = ''
    function ajaxGetData(lotCode, pageSize, recentDay) {
        ajaxRequest({
            url: "${base}/lottery/trendChart/lotteryOpenNum.do?lotCode="+lotCode+"&recentDay="+recentDay+"&rows="+pageSize+"&order=desc",
            data: {},
            beforeSend: function() {
                showLoading();
            },
            success: function(result) {
           	 	if (result.success == false) {
                    hideLoading();
                    alert("当前无开奖数据！");
                    return;
                }
                var data = result.list
                xyk3OnOff = result.bzSelect
                // 折线
                zhexian(data);
                // k线
                kxian(data);
                // 直方图
                zhifang(data);
                // 历史数据
                lssj(data);
                hideLoading();

                $(".btn-navs li").each(function() {
                    if($(this).hasClass("fff")) {
                        return;
                    }

                });
                if($("#checkboxYlfc").is(":checked")) {
                    $(".ylfc").addClass("ylfc-bg");
                } else {
                    $(".ylfc").removeClass("ylfc-bg");
                }


                function GetQueryString(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
                    var context = "";
                    if (r != null)
                        context = r[2];
                    reg = null;
                    r = null;
                    return context == null || context == "" || context == "undefined" ? "" : context;
                }
                GetQueryString("l")==1&& $(".btn-navs li:last").trigger("click");
            }
        });
    }

    $(function () {
        $("#checkboxZhexian").change(function () {
            if ($(this).is(":checked")) {
                $(".zhexian").show();
            } else {
                $(".zhexian").hide();
            }
        });

        $("#checkboxYlsj").change(function () {
            if ($(this).is(":checked")) {
                $(".transparent").addClass("not-transparent");
            } else {
                $(".transparent").removeClass("not-transparent");
            }
        });

        $("#checkboxYlfc").change(function () {
            if ($(this).is(":checked")) {
                $(".ylfc").addClass("ylfc-bg");
            } else {
                $(".ylfc").removeClass("ylfc-bg");
            }
        });
    });
</script>
<div class="layui-layer-move"></div>
</body>
</html>

