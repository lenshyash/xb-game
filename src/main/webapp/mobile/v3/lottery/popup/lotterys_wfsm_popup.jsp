<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="popup popup-playdesc">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-left close-popup"
			href="javascript: void(0);"> <span class="icon icon-left"></span>关闭
		</a> <a class="title">玩法说明</a>
	</header>
	<div class="content">
		<div class="card">
			<div class="card-header">玩法介绍</div>
			<div class="card-content">
				<div class="card-content-inner" id="playDetailDesc"></div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">投注说明</div>
			<div class="card-content">
				<div class="card-content-inner" id="playWinExample"></div>
			</div>
		</div>
	</div>
</div>