<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-right panel-cover" style="background: #efeff4;">
	<div class="content-block" style="margin: 0; padding: 0;">
		<div class="card">
			<div class="card-content">
				<div class="list-block">
					<ul>
						<li><a href="#" class="item-link item-content close-panel">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoSignOut"></em>
								</div>
								<div class="item-inner" style="margin-left: 2rem;">
									<div class="item-title">关闭</div>
								</div>
						</a></li>
						<c:choose>
							<c:when test="${domainFolder == 'd00551'}">

							</c:when>
							<c:otherwise>
								<li>
									<a
											href="#" onclick="iosAnroidChat()"
											class="item-link item-content external">
										<div class="item-media" style="padding: 0;">
											<em class="iconfont icon-user3 icontublb"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">互动大厅</div>
										</div>
									</a></li>
							</c:otherwise>
						</c:choose>

						<li>
							<div class="item-content">
								<div class="item-media">
									<i class="icon icon-form-toggle" style="width:1.2rem;"></i>
								</div>
								<div class="item-inner">
									<div class="item-title label">声音</div>
									<div class="item-input">
										<label class="label-switch" style="float:right;"> <input type="checkbox" id="audioChecked">
											<div class="checkbox"></div>
										</label>
									</div>
								</div>
								<script>
									$(function(){
										
										 if(localStorage.getItem("audioBtn") == 'true'){
											 $("#audioChecked").prop('checked','true')
										}else{
											$("#audioChecked").prop('checked')
										} 
										function changes(){
											if($("#audioChecked").prop('checked')){
												localStorage.setItem("audioBtn",'true');
												var sound = document.getElementById("sound_bet_select");
												try{
													sound.pause();
													sound.currentTime=0;
													sound.play();
												}catch(a){}
											}else{
												localStorage.setItem("audioBtn",'false');
											}
										}
										$("#audioChecked").bind("change", changes);
									})
								</script>
							</div>
						</li>
						<li>
						<c:if test="${bl.code == 'LHC'}">
							<a href="#" onclick="iosAnroidLottery('notesLhc')" class="item-link item-content close-panel">
						</c:if>
						<c:if test="${bl.code != 'LHC'}">
							<a href="#" onclick="iosAnroidLottery('notes')" class="item-link item-content close-panel">
						</c:if>
								<div class="item-media" style="padding: 0;">
									<em class="iconfont icon-lianxijilu icontublb"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">投注记录</div>
								</div>
						</a></li>
						<li><a
							href="#" onclick="iosAnroidLottery('history')"
							class="item-link item-content external">
								<div class="item-media" style="padding: 0;">
									<em class="iconfont icon-jilu1 icontublb"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">历史开奖</div>
								</div>
						</a></li>
						<li><a
							href="#" onclick="location.href='${res}/lottery_rules.do?lotCode=${bl.code}'"
							class="item-link item-content close-panel">
								<div class="item-media" style="padding: 0;">
									<em class="iconfont icon-jilu icontublb"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">玩法说明</div>
								</div>
						</a></li>
						<li><a
							href="#" onclick="location.href='${res}/bet_lotterys_trends.do?lotCode=${bl.code}'"
							class="item-link item-content close-panel">
								<div class="item-media" style="padding: 0;">
									<em class="iconfont icon-shujudata icontublb"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">走势图</div>
								</div>
						</a></li>
						<%-- <li><a
							href="#" onclick="location.href='${res}/interact.do?lotCode=${bl.code}'"
							class="item-link item-content close-panel">
								<div class="item-media" style="padding: 0;">
									<em class="iconfont icon-user icontublb"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">交流大厅</div>
								</div>
						</a></li> --%>
					</ul>
				</div>
			</div>
		</div>
		<c:forEach items="${lotterySort}" var="sort">
			<div class="content-block-title" style="margin: .75rem .75rem .5rem;">
				${sort==1?'时时彩':sort==2?'低频彩':sort==3?'PK10':sort==5?'11选5':sort==6?'香港彩':sort==7?'快三':sort==8?'快乐十分':'PC蛋蛋'}
			</div>
			<div class="card">
				<div class="card-content">
					<div class="list-block">
						<ul>
							<c:forEach items="${blList}" var="bl">
								<c:if test="${bl.viewGroup == sort}">
									<li><a
										href="#" onclick="iosAnroidLotteryType('${bl.code }')"
										class="item-link item-content external">
											<div class="item-media">
<%-- 												<img alt="${bl.name}" --%>
<%-- 													src="${base}/mobile/v3/images/lottery/${bl.code}.png" --%>
<!-- 													width="30"> -->
												<c:if test="${empty bl.imgUrl}">
												<img alt="${bl.name}" src="${base}/mobile/v3/images/lottery/${bl.code}.png" style="width:30px;"/>
												</c:if>
												<c:if test="${not empty bl.imgUrl}">
												<img alt="${bl.name}" src="${bl.imgUrl }" style="width:30px;"/>
												</c:if>
											</div>
											<div class="item-inner">
												<div class="item-title">${bl.name}</div>
											</div>
									</a></li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>
<input type="hidden" value="${onoffMobileChat}" id="mobileChatOnOff" />
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="../../../floatChat.jsp" />
</c:if>
<script>
var isLocaApp = false;
try{isLocaApp = android.isAndroidApp()}catch(err){}
var lotteryCodeIos = $("#lotteryCodeIos").val()
var lotteryNameIos = $("#lotteryNameIos").val()
var lotteryTypeIos = $("#lotteryTypeIos").val()
var resLocaUrl = $("#resLocaUrl").val()
function iosAnroidLotteryType(url) {
    var browser = {
            versions: function() {
                var a = navigator.userAgent,
                    b = navigator.appVersion;
                return {
                    trident: a.indexOf("Trident") > -1,
                    presto: a.indexOf("Presto") > -1,
                    webKit: a.indexOf("AppleWebKit") > -1,
                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                    iPhone: a.indexOf("iPhone") > -1,
                    iPad: a.indexOf("iPad") > -1,
                    webApp: a.indexOf("Safari") == -1
                }
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
        };
	 if (browser.versions.android) {
         //Android
         if(!isLocaApp){
        	 location.href='${base}/mobile/v3/bet_lotterys.do?lotCode='+url+''
         }else{
        	 android.locaLottery(url);
         }
     } else if (browser.versions.ios) {
         //ios
    	  if(!isLocaApp){
    		 location.href='${base}/mobile/v3/bet_lotterys.do?lotCode='+url+'' 	
         }else{
        	 WTK.share(JSON.stringify({'method':'locaLotteryUrl',"lotteryCode": url,"lotteryNameIos": $("#lotteryNameIos").val(),"lotteryTypeIos":$("#lotteryTypeIos").val()}));
         } 
/*     	 location.href='${base}/mobile/v3/bet_lotterys.do?lotCode='+url+'' 	
 */     }else{
    	 location.href='${base}/mobile/v3/bet_lotterys.do?lotCode='+url+''
     }
}
var picCallback = function(photos) {
	try{
		isLocaApp = photos
	}catch(err){}
}
</script>