<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="popup" id="popup-draw-notice">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-left close-popup"
			href="javascript: void(0);"> <span class="icon icon-left"></span>关闭
		</a> <a class="title"></a>
	</header>
	<div class="content">
		<div class="list-block media-list" style="margin: 0;">
			<ul>
			</ul>
		</div>
		<div class="content-block">
			<div class="row">
				<div class="col-100">
					<a href="#" onclick="iosAnroidLottery('lsModule')" class="button button-big button-fill button-danger external">历史开奖结果</a>
				</div>
			</div>
		</div>
	</div>
</div>