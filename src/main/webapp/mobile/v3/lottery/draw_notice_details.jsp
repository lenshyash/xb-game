<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/head.jsp" />
</head>
<body>
	<div class="page" id="page_draw_notice_details_jsp">
		<input type="hidden" value="${lotCode}" id="drawNoticeCode"/>
		<input type="hidden" value="${dxdsSetting}" id="dxdsSetting"/>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="#" onclick="window.history.go(-1)"> <span
				class="icon icon-left"></span> 返回
			</a> <a class="title">${lotName}</a>
		</header>
		<div class="content infinite-scroll">
			<div class="list-block media-list">
				<%-- 直接投注 --%>
				<div class="card" style="margin:0;">
					<div class="card-header"><a class="external" style="display:inline-block;width:100%;text-align: center;color:red;" href="${res}/bet_lotterys.do?lotCode=${lotCode}">直接投注</a></div>
				</div>
				<ul>
				</ul>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
	</div>
	<style>
		.list-block .item-inner {
		    padding-top: 0rem;
		    padding-right: 0.75rem;
		    padding-bottom: 0rem;
		}
		
		.list-block li.media-item .item-inner, .list-block.media-list .item-inner {
		    padding-top: 0rem;
		    padding-bottom: 0rem;
		}
	</style>
</body>
</html>
<jsp:include page="../include/need_js.jsp" />