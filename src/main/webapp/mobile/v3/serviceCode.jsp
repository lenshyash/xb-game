<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="include/head.jsp"></jsp:include>
    <script src="${base}/common/js/qrcode.min.js"></script>
</head>
<body>
<input type="hidden" value="${base}" id="base">
<div class="page" id="page_active_jsp" style="background: ${activeBackColor};">
    <jsp:include page="include/barTab.jsp"/>
    <header class="bar bar-nav">
        <a class="title">客服二维码</a>
        <span class="pull-left">
				 <button class="button button-link button-nav pull-left" style="color: white;" onclick="history.go(-1)">
				    <span class="icon icon-left"></span>
				    返回${base}
				  </button>
			</span>
    </header>
    <div class="content">
<%--        <div class="weChatCode">--%>
<%--            <div class="weChatCode-content">--%>
<%--                <span type="text" >QQ二维码</span>--%>
<%--                <div class=" weChatCode-content-img">--%>
<%--                    <img src="http://a0.att.hudong.com/17/12/01300000433093126093125172516.jpg" alt="qq" width="256" height="256" >--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="qqChatCode-hedaer">--%>
<%--            <div class="qqChatCode-content">--%>

<%--            </div>--%>
<%--        </div>--%>
    </div>
    <script>
        $(function () {
            let urlStr = window.location.href.indexOf("mobile")
            let url = window.location.href.slice(0,urlStr)
            // let urlEnd = urlStr + 10
            // let urlStr = winow.location.href.replace("mobile/v3/","")

            $.ajax({
                url: url+ "native/getStationConfig.do",
                type:"post",
                success:function (res) {
                    if (res.success){
                        let html=""
                        $.each(res.content,function (index,item) {
                            $.each(res.content[index].qrcodelink,function (indexs,items) {
                                html +='<div class="ChatCode">'
                                html +=        '<div class="ChatCode-content">'
                                html +=            '<span type="text" class="'+items.type+'" >'+items.name+'</span>'
                                html +=          ' <div class=" ChatCode-content-img">'
                                html +=               '<img src="'+items.qrcodelink+'" alt="qq" width="256" height="256" >'
                                html +=            '</div>'
                                html +=        '</div>'
                                html +=    '</div>'
                            })
                        })
                        $('.content').html(html)
                    }
                }
            })
        })

    </script>
</div>
</body>
</html>
<jsp:include page="include/need_js.jsp"/>