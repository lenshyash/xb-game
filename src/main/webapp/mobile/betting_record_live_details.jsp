﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<%
String lotCode = request.getParameter("lotCode");
String orderId = request.getParameter("orderId");
pageContext.setAttribute("lotCode", lotCode);
pageContext.setAttribute("orderId", orderId);
%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
</head>
<body >
<div class="top">
	<div class="inner">
		
		<div class="back">
			<a href="betting_record_lottery.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">注单详情</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="recordDetailsContent" id="innerbox">
</div>
</body>

<script type="text/javascript">
	var lotCode = "${lotCode}";
	var orderId = "${orderId}";
</script>
<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/betting_record_lottery_details.js"></script>
</html>
<script type="text/html" id="recoredDetailsTemplate" style="display: none;">
	{#
						var statusTitle = "其他";
						switch(data.status){
						case 1:
							statusTitle = "未开奖";
							break;
						case 2:
							statusTitle = "已派奖";
							break;
						case 3:
							statusTitle = "未中奖";
							break;
						case 4:
							statusTitle = "撤单";
							break;
						}
	#}
	<div class="shangqkaij tac">
		<span class="tac co000">订单号： <em class="hong">{{data.orderId}}</em> </span>
		<div class="cl"></div>
	</div>
	<div class="bgf6 ov">
		<table class="tymingxtab tymingxtabcur">
			<tr>
				<td class="">
					<div class="tymingxtabdiv">账号</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.account}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">单注金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">2.00</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">下注时间</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{new Date(data.createTime).format()}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">投注注数</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.buyZhuShu}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">彩种</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.lotName}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">倍数</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.multiple}}</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">期号</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.qiHao}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">投注总额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.buyMoney}}</div>
				</td>
				
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">玩法</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.playName}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">奖金</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">17.00</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">开奖号码</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.lotteryHaoMa ? data.lotteryHaoMa : "尚未颁奖"}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">中奖注数</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">17.00</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">状态</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{statusTitle}}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">中奖金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">17.00</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">盈亏</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">0</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">投注号码</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">{{data.haoMa}}</div>
				</td>
			</tr>
		</table>
		<div class="cl h80"></div>
	</div>
</script>
<%@include file="include/check_login.jsp" %>