﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<script type='text/javascript' src='${base}/mobile/v3/js/area.js' charset='utf-8'></script>
<style type="text/css">
td.txtright {
	text-align: right;
}
td {
	font-size: 24px;
	color: #777;
	padding: 5px;
	font-weight: bold;
}
th {
	font-size: 10px;
	text-align: right;
	width: 100px;
}
.red {
	color: #ff5400;
}
.modal td.txtright {
	text-align: right;
}
.modal td {
	font-size: 14px;
	color: #777;
	padding: 5px;
}
.modal .red {
	color: #ff5400;
}
.modal table input{
	width:51%;
	padding-left:6px;
	height: 30px;
	background:#fff;
	border: none;
	font-size: 12px;
	color:#cd0005;
	border:1px solid #ddd;
}
.modal table select{
	width:51%;
	padding-left:6px;
	height: 30px;
	background:#fff;
	border: none;
	font-size: 12px;
	color:#cd0005;
	border:1px solid #ddd;
}

</style>
</head>
<body >


<div class="top">
	<div class="inner">
		
		<div class="back">
			<a href="javascript: history.go(-1); //personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">提款</span>
		</div>
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="shangqkaij">
		<span class="fl co777">
		当前账户：${account }
		</span>
		<div class="cl"></div>
	</div>
	<div class="chongzhibox">
		<div class="cl h12"></div>
		<div id="qukuanContent">
		</div>
		<div class="cl h33"></div>
		<div class="tac">
			<a id="next" href="javascript: void(0);" class="xiayibubtns">下一步</a>
		</div>
		<div class="cl h80"></div><div class="cl h80"></div>
	</div>
	<div class="cl h80"></div>
</div>

<div class="chongzhitishi">
	<div class="inner f12 l18">
		<em class="iconfont icon-iconfonticonfontinfo"></em>
		充值无手续费<!-- ，为防止套现充值后需消费满30%以上方可提款，否则提款申请将延后60天以上处理。 -->
	</div>
</div>

<div class="modal fade" id="setReceiptPwdModal" tabindex="-1" role="dialog" aria-labelledby="setReceiptPwdModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title f14" id="setReceiptPwdModalLabel">未设置取款密码，请设置</h4>
			</div>
			<div class="modal-body">
				<ul class="xiugaimimaul">
					<li>
						<div class="fl xiugaimmbt">输入取款密码：</div>
						<div class="fl">
							<input id="receiptPwd" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="">
						</div>
						<div class="cl h10"></div>
					</li>
					<li>
						<div class="fl xiugaimmbt">重复取款密码：</div>
						<div class="fl">
							<input id="confirmReceiptPwd" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="">
						</div>
						<div class="cl h10"></div>
					</li>
					<li style="margin-top: 10px;">
						<div class="fl xiugaimmbt" style="width: 94px;">&nbsp;</div>
						<div class="fl" style="width: 200px;"><a id="setReceiptPwdSubmitBtn" href="javascript: void(0);" class="btn btn-default btn-sm pull-right" style="margin-left: 10px;">设置</a></div>
						<div class="cl h10"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="setBankInfoModal" tabindex="-1" role="dialog" aria-labelledby="setBankInfoModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title f14" id="setBankInfoModalLabel">未设置银行卡信息，请设置</h4>
			</div>
			<div class="modal-body">
			<table width="400" border="0" cellspacing="0" cellpadding="0" class="tables">
                            <tbody>
                                <tr style="height: 50px;">
                                    <th><span class="hong">＊</span>持卡人姓名：</th>
                                    <td class="positionre">
                                        <input class="w200 required userInfo" name="truename" type="text">
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <th><span class="hong">＊</span>出款银行名称：</th>
                                    <td class="positionre">
                                        <select name="bankName" id="bankName">
                                         <option value="建设银行">建设银行</option>
                                         <option value="工商银行" selected="selected">工商银行</option>
                                         <option value="农业银行">农业银行</option>
                                         <option value="中国邮政银行">中国邮政银行</option>
                                         <option value="中国银行">中国银行</option>
                                         <option value="中国招商银行">中国招商银行</option>
                                         <option value="中国交通银行">中国交通银行</option>
                                         <option value="中国民生银行">中国民生银行</option>
                                         <option value="中信银行">中信银行</option>
                                         <option value="中国兴业银行">中国兴业银行</option>
                                         <option value="浦发银行">浦发银行</option>
                                         <option value="平安银行">平安银行</option>
                                         <option value="华夏银行">华夏银行</option>
                                         <option value="广州银行">广州银行</option>
                                         <option value="BEA东亚银行">BEA东亚银行</option>
                                         <option value="广州农商银行">广州农商银行</option>
                                         <option value="顺德农商银行">顺德农商银行</option>
                                         <option value="北京银行">北京银行</option>
                                         <option value="杭州银行">杭州银行</option>
                                         <option value="温州银行">温州银行</option>
                                         <option value="上海农商银行">上海农商银行</option>
                                         <option value="中国光大银行">中国光大银行</option>
                                         <option value="渤海银行">渤海银行</option>
                                         <option value="浙商银行">浙商银行</option>
                                         <option value="晋商银行">晋商银行</option>
                                         <option value="汉口银行">汉口银行</option>
                                         <option value="上海银行">上海银行</option>
                                         <option value="广发银行">广发银行</option>
                                         <option value="深圳发展银行">深圳发展银行</option>
                                         <option value="东莞银行">东莞银行</option>
                                         <option value="宁波银行">宁波银行</option>
                                         <option value="南京银行">南京银行</option>
                                         <option value="北京农商银行">北京农商银行</option>
                                         <option value="重庆银行">重庆银行</option>
                                         <option value="广西农村信用社">广西农村信用社</option>
                                         <option value="吉林银行">吉林银行</option>
                                         <option value="江苏银行">江苏银行</option>
                                         <option value="成都银行">成都银行</option>
                                         <option value="尧都区农村信用联社">尧都区农村信用联社</option>
                                         <option value="浙江稠州商业银行">浙江稠州商业银行</option>
                                         <option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
                                         <option value="qita">其他</option>
                                        </select>
                                        <input class="input-200" name="bankName" placeholder="请输入银行名称" id="bankName2" style="display:none;"/>
                                    </td>
                                </tr>
                                <!-- 
                                <tr style="height: 50px;">
                                    <th>区域：</th>
                                    <td class="positionre">
                                        <input name="country" type="text" class="w60 userInfo" size="10" maxlength="10">&nbsp;省
                                        <input name="countrysecond" type="text" class="w80 userInfo" size="10" maxlength="10">&nbsp;县市</td>
                                </tr>
                                -->
                                <tr>
                                    <th><span class="hong">＊</span>开户省份：</th>
                                    <td class="positionre">
<!--                                         <input type="text" class="w200 userInfo" id="bankProvince" name="bankProvince"> -->
									<select name='bankProvince' id="bankProvince">
								        <option value=''>请选择省份</option>
								    </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th><span class="hong">＊</span>开户城市：</th>
                                    <td class="positionre">
<!--                                         <input type="text" class="w200 userInfo" id="bankCity" name="bankCity"> -->
									<select name='bankCity' id="bankCity">
								        <option value=''>请选择城市</option>
								    </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>开户行网点：</th>
                                    <td class="positionre">
                                        <input type="text" class="w200 userInfo" id="bankcardaddress" name="bankcardaddress">
                                    </td>
                                </tr>
                                
                                <tr id="this_way" style="height: 50px;">
                                    <th><span class="hong">＊</span>出款银行帐号：</th>
                                    <td class="positionre">
                                        <input class="w200 required number userInfo" minlength="10" name="bankno" type="text" xx_xx="checkme">
                                        <div class="c_red" xx_xx="checkmsg_bankno"></div>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <th><span class="hong">＊</span>取款密码：</th>
                                    <td class="positionre">
                                        <input class="w200 required userInfo" name="wcode" type="password" autocomplete="false">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
				<ul class="xiugaimimaul">
					<li style="margin-top: 10px;">
						<div class="fl xiugaimmbt" style="width: 94px;">&nbsp;</div>
						<div class="fl" style="width: 200px;"><a id="setBankInfoSubmitBtn" href="javascript: void(0);" class="btn btn-default btn-sm pull-right" style="margin-left: 10px;">设置</a></div>
						<div class="cl h10"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript">
	$(function(){
		
		var province = '';
        $.each(city, function(index, value) {
            province += '<option value="' + value.name + '" index="' + index + '"> ' + value.name + '</option>'

        });
        $('select[name=bankProvince]').append(province).change(function() {
            //$('select[name=city]').html('');
            var option = '';
            if ($(this).val() == '') {
                option += '<option value="">请选择</option>';
            } else {
                var index = $(':selected', this).attr('index');
                var data = city[index].child;
                for (var i = 0; i < data.length; i++) {
                    option += '<option value="' + data[i] + '">' + data[i] + '</option>';
                }
            }
            $('select[name=bankCity]').html(option);
        })
		
		window.load = function(){   
			document.getElementById('money').value='';   
			};  
		
		$("#bankName").change(function() {
			var isQita = this.value;
			if(isQita == 'qita'){
				$("#bankName2").show();
			}else{
				$("#bankName2").val('');
				$("#bankName2").hide();
			}
		});

		$._ajax(stationUrl + "/member/checkAccountSecurityInfo.do", function (status, message, data, result) {
			console.log(data);

			if(data.notReceiptPwd){
				$("#setReceiptPwdModal").modal("show").on("hide.bs.modal", function(){
					location.href = "personal_center.do";
				});
				$("#setReceiptPwdSubmitBtn").on("click", function(){
					var receiptPwd = $("#receiptPwd").val(), confirmReceiptPwd = $("#confirmReceiptPwd").val();
					if(!receiptPwd){
						$.alert("取款密码不能为空");
					} else if(receiptPwd != confirmReceiptPwd){
						$.alert("两次取款密码不一致");
					} else {
						$.ajax({
							type : "POST",
							url : stationUrl + "/member/initcashpass.do",
							data : {
								newpassword: $("#receiptPwd").val(),
								confirmpassword: $("#confirmReceiptPwd").val()
							},
							success : function(result) {
								if(result.success == false){
									$.alert(result.msg);
								} else if(result.success == true){
									$.alert("修改成功");
									location.href = location.href;
								}
							}
						});
					}
				});
				/*
				$("div.top, #innerbox").hide();
				var $dialog = $.dialog({
					width: 280,
					title: "未设置取款密码，请设置",
					content: $("#setReceiptPwdTemplate").html(),
					okValue: "设置",
					ok: function(e){
						console.log(e);
						console.log(this);
						console.log($(this));
						var receiptPwd = $("#receiptPwd").val(), confirmReceiptPwd = $("#confirmReceiptPwd").val();
						if(!receiptPwd){
							$.alert("取款密码不能为空");
							$dialog.show();
						} else if(receiptPwd != confirmReceiptPwd){
							$.alert("两次取款密码不一致");
							return false;
						} else {
							
						}
					},
					canelValue: "返回"
				});
				*/
			} else if(data.notBankInfo){
				$("#setBankInfoModal").modal("show").on("hide.bs.modal", function(){
					location.href = "personal_center.do";
				});
				
				var username = '${userInfo1.userName}';
				if(username){
					$("input[name='truename']").val(username);
					$("input[name='truename']").attr("disabled","disabled");
				}
				var cardNo = '${userInfo1.cardNo}';
				if(cardNo){
					$("input[name='bankno']").val(cardNo);
					$("input[name='bankno']").attr("disabled","disabled");
				}
				$("#setBankInfoSubmitBtn").on("click", function(){
					var param = {};
					if(!username){
						username = $("input[name='truename']").val();
					}
                    param["userName"] = username;
                    
                    var cashBankname = $("#bankName").val();
                    var bankProvince = $("#bankProvince").val();
                    var bankCity = $("#bankCity").val();
            		var qita = $('#bankName2').val();
            		if(qita){
            			cashBankname = qita;
            		}else{
            			if (!cashBankname) {
            				alert("开户银行不能为空");
            				return false;
            			}else if (!bankProvince || bankProvince == '请选择省份') {
               				alert("开户省份不能为空");
               				return false;
            			}else if (!bankCity || bankCity == '请选择城市') {
               				alert("开户城市不能为空");
               				return false;
            			}else if(cashBankname == 'qita'){
            				if(!qita){
            					alert("开户银行不能为空");
            					return false;
            				}
            			}
            		}
                    
                    param["bankName"] = cashBankname;
                    param["province"] = $("input[name='country']").val();
                    param["city"] = $("input[name='countrysecond']").val();
                    param["bankAddress"] = $("input[name='bankcardaddress']").val();
                    param["cardNo"] = $("input[name='bankno']").val();
                    param["repPwd"] = $("input[name='wcode']").val();

					$.ajax({
						type : "POST",
						url : stationUrl + "/member/setBankInfo.do",
						data : param,
						success : function(result) {
							if(result.success == false){
								$.alert(result.msg);
								$("#iAlertKkkDiv").css("z-index", 9999);
							} else if(result.success == true){
								$.alert("修改成功");
								location.href = location.href;
							}
						}
					});
				});
			} else {
				$._ajax(stationUrl + "/withdraw/qukuanData.do", function (status, message, data, result) {
					console.log(data);
					var tplHtml = $("#qukuanTemplate").html();
					var $qukuanContent = $("#qukuanContent");
					$qukuanContent.html(_.template(tplHtml)(data));
				});
				var canNext = true;
				$("#next").on("click", function(){
					if(canNext){
						canNext = false;
						$.ajax({
							type : "POST",
							url : stationUrl + "/withdraw/withdrawcommit.do",
							data : {
								money: $("#money").val(),
								repPwd: $("#userQxpassword").val()
							},
							success : function(result) {
								if(result.success == false){
									$.alert(result.msg ? result.msg : "后台异常，请联系管理员！");
								} else if(result.success == true){
									$.dialog({
										title: "系统讯息",
										content: "取款申请已经提交",
										canel: function () {
											location.href="account_details.do";
										}, canelValue: "查看记录",
										ok: function () {
											location.href="personal_center.do";
										}, okValue: "返回个人中心"
									});
								}
							},
							complete : function(XMLHttpRequest, code){
								canNext = true;
							}
						});
					}
				});
			}
		});

	});
</script>
<script type="text/html" id="qukuanTemplate" style="display: none;">
			<table border="0" cellpadding="3" cellspacing="3" width="100%">
				<colgroup>
					<col width="25%">
					<col width="75%">
				</colgroup>
				<tbody>
					<tr>
						<td class="txtright">提示信息：</td>
					</tr>
					<tr>
						<td colspan="2" class="txtleft cbcolor" style="padding-left:3rem;">
							每天的取款处理时间为：<span style="font-size: 22px; color: #ffb400;"> {{star}} 至 {{end}}</span>;<br>取款1-3分钟内到账。(如遇高峰期，可能需要延迟到5-10分钟内到帐)<br>用户每日最小提现&nbsp;<span style="color: #ffb400; font-size: 22px;">{{min}}</span>&nbsp;元，最大提现&nbsp;<span style="color: #ffb400; font-size: 22px;">{{max}}</span>&nbsp;元<br>今日可取款<span style="font-size: 22px; color: #ffb400;"> {{wnum}}</span> 次，已取款<span style="font-size: 22px; color: #ffb400;"> {{curWnum}}</span> 次
						</td>
					</tr>
					<tr>
						<td class="txtright">消费比例：</td>
					</tr>
					<tr>
						<td colspan="2" class="txtleft cbcolor" style="padding-left:3rem;">
							出款需达投注量：<span style="font-size: 22px; color: #ffb400; margin-right: 10px;">{{checkBetNum}}</span>有效投注金额：<span style="font-size: 22px; color: #ffb400; margin-right: 10px;">{{member.betNum}}</span><br>是否能取款：<span style="font-size: 22px; color: #ffb400; margin-right: 10px;">{{drawFlag}}</span>
						</td>
					</tr>
					<tr>
						<td class="txtright">真实姓名：</td>
						<td>{{member.userName}}</td>
					</tr>
					<tr>
						<td class="txtright">账户余额：</td>
						<td><span class="red">{{member.money}}</span>RMB</td>
					</tr>
					<tr>
						<td class="txtright">银行：</td>
						<td>{{member.bankName}}</td>
					</tr>
					<!-- 
					<tr>
						<td class="txtright">开户行：</td>
						<td>{{member.province}}{{member.city}}{{member.bankAddress}}</td>
					</tr>
					 -->
					<tr>
						<td class="txtright">银行账号：</td>
						<td>{{member.cardNo}}</td>
					</tr>
	
					<tr>
						<td class="txtright"><span class="red">*</span>取款金额：</td>
						<td class="chongzhiinptu"><input class="inp3 cbinput" style="height: 5rem;" id="money" autocomplete="off" name="money" type="text"></td>
					</tr>
					<tr>
						<td class="txtright"><span class="red">*</span>取款密码：</td>
						<td class="chongzhiinptu"><input class="inp3 cbinput" style="height: 5rem;" id="userQxpassword" autocomplete="off" name="userQxpassword" type="password"></td>
					</tr>
				</tbody>
			</table>
</script>
<script type="text/html" id="setReceiptPwdTemplate" style="display: none;">
			<ul class="xiugaimimaul">
				<li>
					<div class="fl xiugaimmbt">输入取款密码：</div>
					<div class="fl">
						<input id="receiptPwd" type="text" class="xiugaimminpt" placeholder="密码需包英文数字" value="">
					</div>
					<div class="cl h10"></div>
				</li>
				<li>
					<div class="fl xiugaimmbt">重复取款密码：</div>
					<div class="fl">
						<input id="confirmReceiptPwd" type="text" class="xiugaimminpt" placeholder="密码需包英文数字" value="">
					</div>
					<div class="cl h10"></div>
				</li>
				
			</ul>
</script>
<style>
.cbcolor{
/* 	background-color:white; */
	border-radius:1rem;
}
.cbinput{
    border-radius: 1rem;
}
</style>
</html>
<%@include file="include/check_login.jsp" %>