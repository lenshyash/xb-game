$(function(){
	//此方法解决移动端浏览器里面的iframe不能上下滑动问题
//	function scrollIframeForIOS(iframe, iframeWrapper) {
//		if (!navigator.userAgent.match(/iPad|iPhone/i))
//			return false;
//		var touchY = 0, touchX = 0;
//		iframe = typeof (iframe) == "string" ? document.getElementById(iframe)
//				: iframe;
//		iframe.onload = function() {
//			var ifrWin = iframe.contentWindow, ifrDoc = ifrWin.document;
//			// iframe的上一级节点
//			iframeWrapper = iframeWrapper || ifrWin.frameElement.parentNode;
//			// 记录手指按下的位置
//			ifrDoc.body.addEventListener("touchstart", function(event) {
//				touchX = event.targetTouches[0].pageX;
//				touchY = event.targetTouches[0].pageY;
//			});
//			ifrDoc.body
//					.addEventListener(
//							"touchmove",
//							function(event) {
//								e.preventDefault(); // 阻止整个页面拖动
//								iframeWrapper.scrollLeft += (touchX - event.targetTouches[0].pageX);
//								iframeWrapper.scrollTop += (touchY - event.targetTouches[0].pageY);
//							});
//		}
//		return true;
//	};
//	//此方法解决JS的sort()排序方法在IE9/safari兼容性问题
//	!function(window) {
//		var ua = window.navigator.userAgent.toLowerCase(), reg = /msie|applewebkit.+safari/;
//		if (reg.phone(ua)) {
//			var _sort = Array.prototype.sort;
//			Array.prototype.sort = function(fn) {
//				if (!!fn && typeof fn === 'function') {
//					if (this.length < 2)
//						return this;
//					var i = 0, j = i + 1, l = this.length, tmp, r = false, t = 0;
//					for (; i < l; i++) {
//						for (j = i + 1; j < l; j++) {
//							t = fn.call(this, this[i], this[j]);
//							r = (typeof t === 'number' ? t : !!t ? 1 : 0) > 0 ? true
//									: false;
//							if (r) {
//								tmp = this[i];
//								this[i] = this[j];
//								this[j] = tmp;
//							}
//						}
//					}
//					return this;
//				} else {
//					return _sort.call(this);
//				}
//			};
//		}
//	}(window);
	var tt;
	$(function() {
		$(".sport_nav").click(function() {
			$(".nav_content").toggle(500);
			clearTimeout(tt);
			if ($(".nav_content").is(":visible")) {
				tt = setTimeout(function() {
					$(".nav_content,.gameTypeUL").slideUp(500);
				}, 1000 * 40);
			}
		});

		$(".nav_content_item1").each(function() {
			$(this).click(function() {
				$(this).next(".gameTypeUL").toggle(500);
			});
		});

		scrollIframeForIOS('iframeBox');
	});

	$(".arrowSlide").click(function() {
		var kfcontent = parseInt($(".kf-content").css("width").replace("px", ""));
		if (kfcontent > 45) {
			$(".firameContent").animate({
				width : 0
			}, "slow");
			$(".kf-content").animate({
				width : 45
			}, "slow");
			$(".firameContent iframe").animate({
				height : 0
			}, "slow");
			$(".arrowSlide").removeClass("noarrowbg").addClass("arrowbg");
		} else {
			$(".kf-content").animate({
				width : '280px'
			}, "slow");
			$(".firameContent").animate({
				width : '235px',
				height : '365px'
			}, "slow");
			$(".firameContent iframe").animate({
				height : '365px'
			}, "slow");
			$(".arrowSlide").removeClass("arrowbg").addClass("noarrowbg");

			setTimeout(function() {
				$(".firameContent").animate({
					width : 0
				}, "slow");
				$(".kf-content").animate({
					width : 45
				}, "slow");
				$(".firameContent iframe").animate({
					height : 0
				}, "slow");
				$(".arrowSlide").removeClass("noarrowbg").addClass("arrowbg");
			}, 1000 * 50);
		}
	});
	
});
