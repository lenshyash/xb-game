<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/mobile/chatMobileModule.jsp" ></jsp:include>
</c:if>
<style>
	#openChat{
		width:40px!important;
		top:5px!important;
		right:95px!important;
	}
</style>
<div class="bg-info header-logo">
	<div class="pull-left register">
		<a href="${base}/mobile"> <span class="glyphicon glyphicon-home fontsize"></span>
		</a>
	</div>
	<div class="pull-right register">
		<div class="dropdown_nav">
			<a class="btn sport_nav"> 投注选择 <span class="caret"></span>
			</a>
			<div class="nav_content" style="display: none;">
				<div class="nav_content_item">滚 球</div>
				<div class="gunqiu">
					<a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=RB_FT_MN"> 
					<img src="${base}/mobile/sports/hg/img/head_ball_ft.gif">&nbsp;足球 <span>(<b id="COUNT_RB_FT" class="text-red">0</b>)
					</span>
					</a>
				</div>
				<script>
					console.log('篮球开关：','${onoff_sports_basketBall_game}');
				</script>
				<c:if test="${onoff_sports_basketBall_game  eq 'on' }">
				<div class="gunqiu">
					<a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=RB_BK_MN"> 
					<img src="${base}/mobile/sports/hg/img/head_ball_bk.gif">&nbsp;篮球 <span>(<b id="COUNT_RB_BK" class="text-red">0</b>)
					</span>
					</a>
				</div>
				</c:if>
				<div class="nav_content_item">今日赛事</div>
				<div class="nav_content_item1">
					<img src="${base}/mobile/sports/hg/img/head_ball_ft.gif">&nbsp;足球 <span>(<b id="COUNT_TD_FT" class="text-red">0</b>)
					</span>
				</div>
				<ul class="gameTypeUL" style="display: none;">
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_FT_MN">独赢/让球/大小/单双</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_FT_TI">波胆</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_FT_BC">总入球</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_FT_HF">半场/全场</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_FT_MX">综合过关</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/result.do?sportType=FT">足球赛果</a></li>
				</ul>
				<c:if test="${onoff_sports_basketBall_game  eq 'on' }">
				<div class="nav_content_item1">
					<img src="${base}/mobile/sports/hg/img/head_ball_bk.gif">&nbsp;篮球 <span>(<b id="COUNT_TD_BK" class="text-red">0</b>)
					</span>
				</div>
				</c:if>
				<ul class="gameTypeUL" style="display: none;">
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_BK_MN">独赢/让球/大小/单双</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=TD_BK_MX">综合过关</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/result.do?sportType=BK">蓝球赛果</a></li>
				</ul>
				<div class="nav_content_item">早 盘</div>
				<div class="nav_content_item1">
					<img src="${base}/mobile/sports/hg/img/head_ball_ft.gif">&nbsp;足球 <span>(<b id="COUNT_FT_FT" class="text-red">0</b>)
					</span>
				</div>
				<ul class="gameTypeUL" style="display: none;">
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_FT_MN">独赢/让球/大小/单双</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_FT_TI">波胆</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_FT_BC">总入球</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_FT_HF">半场/全场</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_FT_MX">综合过关</a></li>
				</ul>
				<c:if test="${onoff_sports_basketBall_game  eq 'on' }">
				<div class="nav_content_item1">
					<img src="${base}/mobile/sports/hg/img/head_ball_bk.gif">&nbsp;篮球 <span>(<b id="COUNT_FT_BK" class="text-red">0</b>)
					</span>
				</div>
				</c:if>
				<ul class="gameTypeUL" style="display: none;">
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_BK_MN">独赢/让球/大小/单双</a></li>
					<li><a target="_self" href="${base}/mobile/sports/hg/goPage.do?dataType=FT_BK_MX">综合过关</a></li>
				</ul>
				<div class="nav_content_item" style="margin-bottom: 1px;">
					<a href="${base}/mobile/sports/hg/goOrderPage.do">交易状况</a>
				</div>
				<!-- 
				<div class="nav_content_item">
					<a target="_self" href="javascript:void(0);">账户历史</a>
				</div> -->
			</div>
		</div>
	</div>
	<div style="clear: both"></div>
</div>

<script language="javascript">
var tt;
$(function() {
	$(".sport_nav").click(function() {
		$(".nav_content").toggle(500);
		clearTimeout(tt);
		if ($(".nav_content").is(":visible")) {
			tt = setTimeout(function() {
				$(".nav_content,.gameTypeUL").slideUp(500);
			}, 1000 * 40);
		}
	});

	$(".nav_content_item1").each(function() {
		$(this).click(function() {
			$(this).next(".gameTypeUL").toggle(500);
		});
	});
	//scrollIframeForIOS('iframeBox');
});
</script>