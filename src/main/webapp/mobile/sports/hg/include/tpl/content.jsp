<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="BASE_TABLE">
<div class="bg-info header-logo text-center fontwidth">体育赛事</div>
	<link href="${base}/mobile/sports/hg/css/sportGame.css" rel="stylesheet" />
	<div class="text-left title-background">
		<span id="titles" class="pull-left">{{title}}</span>&nbsp;&nbsp;
		<span id="{{pageTextId}}" class="yemashu">总共1/1页</span> 
		<select id="{{pageSelectorId}}" class="color000 yemashu">
			<option value='1' selected='selected'>1</option>
		</select> 
		{{if showRefresh}}
		<span  id="{{refreshBtnId}}" class="pull-right refresh_btn miaoshu">
			{{autoRefreshTime}}
		</span>
		{{/if}}
		<div class="clearfix"></div>
	</div>
	<div id="dataContent">
	
	
	</div>
</div>		
</script>

<script type="text/html" id="order_item">
{{if !items || items.length == 0}}
	<div style="width: 216; height: 63px; text-align: center; padding-top: 26px;">
		<font style="font: 12px Arial, Helvetica, sans-serif; font-weight: bold;">
			点击赔率便可将<br>选项加到交易单里。
		</font>
	</div>
{{/if}}
{{each items as val}}
	<div id="del{{val.deleteId}}">
		<div class="leag" style="padding-right: 3px;">
		<div class="leag_txt">
			<div style="{{if isMix}}float:left;{{/if}}width:210px;overflow:hidden;">{{val.league}}</div>
			</div>
			{{if isMix}}
			<span class="deletebtn">
				<a href="javascript:void(0)" onclick="delBetItem({{val.deleteId}});" title="取消">X</a>
			</span>
			<div class="clearfix"></div>
			{{/if}}
		</div>
		<div class="gametype">{{val.title}}</div>
		<div class="teamName">
			<span class="tName">{{#val.team}}</span>
		</div>
		<p class="team"  {{if val.replace}} style="background-color:rgb(255, 255, 0);" {{/if}}>
			{{#val.oddsDesc}}
		</p>
	</div>
{{/each}}
</script>
