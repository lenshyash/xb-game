<%@ page language="java" pageEncoding="UTF-8"%>
<script  type="text/html" id="DATA_TR">
{{each data as val i}}
<!-- 输出联赛 -->
	{{if i== 0 || data[i-1].league != val.league }}
		<div class="bg-titleliansai">
			<span class="Legimg"></span>{{val.league}}
		</div>
	{{/if}}
	
    <div class="panel panel-success" league='{{val.league}}' style="display:none;">
        <div class="panel-body">
            <table class="game result">
                <thead>
                    <tr class="bg-title">
                        <td class="rdt" style="width:50px">时间</td>
                        <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;赛果</td>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TrBgOdd ">
                        <td rowspan="3" style="width:50px">{{$formatTime val.startTime}}</td>
                        <td class="rname" style="width:50px;">比赛队伍</td>
                        <td colspan="2" style="width:100px;text-align:center"><b style="color:#c00">{{val.homeTeam}}</b></td>
                        <td colspan="2" style="width:100px;text-align:center">{{val.guestTeam}}</td>
                    </tr>
                    <tr class="TrBgOdd odd" style="background-color: #eaeaea;">
                        <td >半场</td>
                        <td colspan="2" >{{val.scoreH1H}}</td>
                        <td colspan="2" >{{val.scoreH1G}}</td>
                    </tr>
                    <tr class="TrBgOdd even " style="background-color: #d4e8ff; color: #c00;">
                        <td class="{{$printClass val.scoreFullH true 'red'}}">全场</td>
                        <td colspan="2" class="{{$printClass val.scoreFullH true 'bold red'}}">{{val.scoreFullH}}</td>
                        <td colspan="2" class="{{$printClass val.scoreFullH true 'bold red'}}">{{val.scoreFullG}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
{{/each}}
</script>