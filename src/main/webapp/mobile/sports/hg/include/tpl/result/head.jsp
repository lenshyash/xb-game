<%@ page language="java" pageEncoding="UTF-8"%>

<script  type="text/html" id="BASE_TABLE">
<div class="text-left title-background">
    <span id="titles" class="pull-left">{{title}}</span>&nbsp;&nbsp;&nbsp;&nbsp;
    <div class="clearfix"></div>
</div>

<div class="input-group">
    <span class="input-group-addon">日期</span>
    <input type="text" class="form-control form_date" id="SearchDate" >
    <span id="searchResultBtn" class="input-group-addon seachDate">
    <span class="glyphicon glyphicon-search">
    </span></span>
</div>
<div class="clearfix"></div>
<div id="dataContent"></div>
</script>

