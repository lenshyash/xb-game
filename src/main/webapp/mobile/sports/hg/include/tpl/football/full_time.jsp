<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">
{{each games as val i}}
	{{if i== 0 || games[i-1].league != val.league }}
		<div class="bg-titleliansai">
			<span class="Legimg {{$displayLegClass val.league}}"></span>{{val.league}}
		</div>
	{{/if}}	
	<div class="panel panel-success" league="{{val.league}}" style="{{$displayLeg val.league}}">
	<div class="panel-heading">{{$formatDate gameType val}}</div>
     <div class="panel-body">
         <table>
             <thead>
                 <tr class="bg-title">
                     <td class="width15">球队</td>
                     <td>1:0</td>
                     <td>2:0</td>
                     <td>2:1</td>
                     <td>3:0</td>
                     <td>3:1</td>
                     <td>3:2</td>
                     <td>4:0</td>
                     <td>4:1</td>
                     <td>4:2</td>
                     <td>4:3</td>
                 </tr>
             </thead>
             <tbody>
                 <tr>
                     <td>{{$teamName val.home}}</td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H1C0 'ior_H1C0' val.gid '1:0'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H2C0 'ior_H2C0' val.gid '2:0'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H2C1 'ior_H2C1' val.gid '2:1'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H3C0 'ior_H3C0' val.gid '3:0'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H3C1 'ior_H3C1' val.gid '3:1'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H3C2 'ior_H3C2' val.gid '3:2'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H4C0 'ior_H4C0' val.gid '4:0'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H4C1 'ior_H4C1' val.gid '4:1'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H4C2 'ior_H4C2' val.gid '4:2'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H4C3 'ior_H4C3' val.gid '4:3'}}</span></td>
               </tr>
               <tr>
                   <td>{{$teamName val.guest}}</td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H0C1 'ior_H0C1' val.gid '0:1'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H0C2 'ior_H0C2' val.gid '0:2'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H1C2 'ior_H1C2' val.gid '1:2'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H0C3 'ior_H0C3' val.gid '0:3'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H1C3 'ior_H1C3' val.gid '1:3'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H2C3 'ior_H2C3' val.gid '2:3'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H0C4 'ior_H0C4' val.gid '0:4'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H1C4 'ior_H1C4' val.gid '1:4'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H2C4 'ior_H2C4' val.gid '2:4'}}</span></td>
                   <td><span class="text-red font-bold">{{$showOdds val.ior_H3C4 'ior_H3C4' val.gid '3:4'}}</span></td>
               </tr>
           </tbody>
       </table>
       <table>
           <thead>
               <tr class="bg-title">
                   <td class="width15"></td>
                   <td>0:0</td>
                   <td>1:1</td>
                   <td>2:2</td>
                   <td>3:3</td>
                   <td>4:4</td>
                   <td>其它</td>
               </tr>
           </thead>
           <tbody>
               <tr>
                   <td></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_H0C0 'ior_H0C0' val.gid '0:0'}}</span></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_H1C1 'ior_H1C1' val.gid '1:1'}}</span></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_H2C2 'ior_H2C2' val.gid '2:2'}}</span></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_H3C3 'ior_H3C3' val.gid '3:3'}}</span></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_H4C4 'ior_H4C4' val.gid '4:4'}}</span></td>
                   <td><span  class="text-red font-bold">{{$showOdds val.ior_OVH 'ior_OVH' val.gid '+4'}}</span></td>
               </tr>
           </tbody>
       </table>
     </div>
   </div>
</div>
{{/each}}
</script>