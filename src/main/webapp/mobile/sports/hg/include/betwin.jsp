<%@ page language="java" pageEncoding="UTF-8"%>

<!-- 弹窗消息 -->    
<div id="ycf-alert" class="modal">
	<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
		<div class="modal-content">
			<div class="modal-body bg-info" style="border-radius: 5px; padding: 10px;">
				<p style="font-size: 15px; font-weight: bold;" id="alert_msg_content">[Message]</p>
			</div>
			<div class="modal-footer" style="padding: 5px; text-align: center;">
				<button type="button" id="alert_ok_btn" class="btn btn-primary ok" >[BtnOk]</button>
				<button type="button" id="alert_cancel_btn" class="btn btn-default cancel" >[BtnCancel]</button>
			</div>
		</div>
	</div>
</div>

<!-- 遮挡层 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<img class="center-block linecentermodel" width="40" height="40" src="${base}/mobile/sports/hg/img/load.gif" />
</div>
<!-- 下注框 -->

<div class="arrowSlide arrowbg" style="position:fixed;bottom:0px;right:0px;"></div>

<div class="betPop">
	
		<div class="firameContent" style="overflow:hidden;height:265px;">
			<div style="height:50px;background-color:#330000;color:#CCCC99;">
				<div style="width:100%;font-size:15px;" align="center">交易单</div>
				<div style="font-size:12px;">
					<span class="pull-left">&nbsp;<input type="checkbox" id="autoAccept"/><label for="autoAccept">接受最佳赔率</label></span>
					<span class="pull-right" onclick="refreshOdds()">刷新赔率&nbsp;</span>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="orderList" >
				<div style="width: 216px; height: 63px; text-align: center; padding-top: 26px;">
					<font style="font: 12px Arial, Helvetica, sans-serif; font-weight: bold;">点击赔率便可将<br>选项加到交易单里。
					</font>
				</div>
			</div>
			<div style="background-color:#330000;color:#CCCC99;">
				<div style="padding-left:5px;padding-top:10px;padding-bottom:10px;">
					<div>交易金额: <input style="color:black;width:100px;" type="number" onkeyup="RepNumber(this)" id="betMoney"/></div>
					<div >可赢金额:&nbsp;<span id="winMoney">0</span></div>
					<div>单注最低: <font id="minBettingMoney">${minBettingMoney}</font></div>
					<div></div>
				</div>
			</div>
			<div>
				<button style="width:100%;" type="button" onclick="submitOrder()" class="btn btn-large btn-success">确认交易</button>
			</div>
		</div>
</div>