﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
	<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/fonts/iconfont.css" type="text/css" />
	<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/reset.css" type="text/css" />
	<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/style/global.css?v=2" type="text/css" />
	<link href="${base}/mobile/sports/hg/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<a href="${base}/mobile/personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> <span class="vbktl">体育投注记录</span>
			</div>
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div id="selectQueryType" class="touzhuqie" data-current="1">
				<span data-querytype="1" class="cur" style="width: 25%;">全部记录 </span> 
				<span data-querytype="2" class="" style="width: 25%;">中奖记录 </span> 
				<span data-querytype="3" class="" style="width: 25%;">未开奖 </span> 
				<span data-querytype="4" class="" style="width: 25%;">未成功订单 </span> 
			</div>
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent" data-value="1">
						<div class="fangsjbox">今天</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="1">今天</li>
								<li data-value="2">昨天</li>
								<li data-value="3">近一周</li>
								<li data-value="4">近30天</li>
							<!--<li data-value="5">30天以前</li>  -->	
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryClasscode" class="touzhusj selectComponent" data-value="0">
						<div class="fangsjbox">平台</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="0">皇冠体育</li>
								<li data-value="1">沙巴体育</li>
								<li data-value="2">BB体育</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryBallcode" class="touzhusj selectComponent" data-value="0">
						<div class="fangsjbox">所有球种</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="0">所有球种</li>
								<li data-value="1">足球</li>
								<li data-value="2">篮球</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div id="recordContent" class="caizhongtable">
				<!--  -->
			</div>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
</body>

<script type="text/javascript" src="${base}/mobile/script/jquery-1.11.1.min.js"></script>
<script src="${base}/mobile/sports/hg/js/bootstrap.min.js"></script>
<script src="${base}/mobile/sports/hg/js/bootstrap_alert.js"></script>
<script src="${base}/common/js/artTemplate/template.js"></script>
<script src="${base}/mobile/sports/hg/js/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/mobile/sports/hg/js/order.js"></script>
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/mobile/chatMobileModule.jsp" ></jsp:include>
</c:if>
<style>
	#openChat{
		width:40px!important;
		top:5px!important;
	}
</style>
<script>
	$("#openChat img").attr('src','${base}/common/template/member/center/img/1516676959037063.png')
</script>
<!-- 修改头部颜色 -->
<script>
var color = localStorage.getItem("colorB")
$(".top").css("background",color)
</script>

</html>

<script type="text/html" id="DATA_TR">
<table>
	<tr class="caizhongtabletr">
		<td>球类</td>
		<td>投注时间</td>
		<td>投注金额</td>
		<td>当前状态</td>
		<td>操作</td>
	</tr>
	<tbody>
		{{each rows as item}}
			<tr>
				<td>{{item.sportName}}</td>
				<td>{{item.betDate}}</td>
				<td>{{item.betMoney}}元</td>
				<td>{{item.betStatus}}</td>
				<td><a href="javascript:goDeatilPage({{item.id}})" class="lanx">查看详情</a></td>
			</tr>
		{{/each}}
	</tbody>
</table>
</script>
<script type="text/html" id="DATA_BET">
<table>
	<tr class="caizhongtabletr">
		<td>球类</td>
		<td>投注时间</td>
		<td>投注金额</td>
		<td>当前状态</td>
	</tr>
	<tbody>
		{{each rows as item}}
			<tr>
				<td>{{item.sportName}}</td>
				<td>{{item.betDate}}</td>
				<td>{{item.betMoney}}元</td>
				<td>{{item.betStatus}}</td>
			</tr>
		{{/each}}
	</tbody>
</table>
</script>

