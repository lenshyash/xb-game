<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/mobile/sports/hg/include/base.jsp"></jsp:include>
<html class="ui-mobile">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<title>${_title}</title>
	<jsp:include page="/mobile/sports/hg/include/tpl/result/source.jsp"></jsp:include>
</head>

<body>
	<jsp:include page="/mobile/sports/hg/include/layout/head.jsp"></jsp:include>
	<div id="result_view"></div>
	<div class="text-center footercontent">${copyright}</div>
</body>
</html>
<jsp:include page="/mobile/sports/hg/include/tpl/result/head.jsp"></jsp:include>
<jsp:include page="/mobile/sports/hg/include/tpl/result/football.jsp"></jsp:include>

<script language="javascript">

var gameCountMap = ${gameCount};
refreshGameCount(gameCountMap);
var view = new MobileSportResultView({
	id : 'result_view',
	title:'足球赛果:',
	tableTpl:'BASE_TABLE',
	gameTpl:'DATA_TR',
	sportType:"FT",
	now:<%=System.currentTimeMillis()%>
});	
</script>