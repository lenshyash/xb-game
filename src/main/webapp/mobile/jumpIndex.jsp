<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv=Content-Type content="text/html;charset=utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta name=format-detection content="telphone=no"/>
<meta name=apple-mobile-web-app-capable content=yes />
<link rel="stylesheet" rev="stylesheet" href="${base }/common/css/font-awesome/css/font-awesome.min.css" type="text/css" />
<title>首页</title>
<style>
/*-- 清除原属性 --*/
html, body, div, p { box-sizing: border-box; }
html, body { height: 100%; }

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video, input { margin: 0; padding: 0; border: none; outline: 0; font-size: 100%; font: inherit; vertical-align: baseline; }
html, body, form, fieldset, p, div, h1, h2, h3, h4, h5, h6 { -webkit-text-size-adjust: none; }
article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
body { font-family: 'Helvetica Neue', Arial, "Hiragino Sans GB", 'Microsoft YaHei', sans-serif; -webkit-touch-callout: none;-webkit-text-size-adjust: none; -moz-text-size-adjust: none; -ms-text-size-adjust: none; -o-text-size-adjust: none; text-size-adjust: none }
a { color: #666; text-decoration: none; }
a:hover { color: #666; text-decoration: none;}
img { max-width: 100%; }
.wrap {height: 100%; overflow: auto; color: #999;padding-bottom:4em;}
.logo {margin:28px auto 18px; text-align: center; }
.btn { width:239px; margin:0 auto;   }
.btn a { display: block; }
.go-btn{background: #e25e4e;text-align: center;border-radius: 27px;color: #fff;padding: 10px;}
.go-btn1{fong-size:15%;padding:15px;}
.tit { font-size: 80%; text-align: center; margin:20px 0 40px;}
.wap-text { text-align: center; font-size: 80%; padding-bottom: 10px; }
.min-btn { margin-bottom:2em;}
.min-btn .fa{font-size:2em;}
.min-btn span{font-size:1.4em;margin-left: 0.5em;}
.android{border:2px solid #e25e4e;text-align: center;border-radius: 27px;color:#e25e4e;padding: 10px;}
.ios{border:2px solid #4da54e;text-align: center;border-radius: 27px;color:#4da54e;padding: 10px;}
.wap-foot { position: fixed; bottom:0px; width: 100%; text-align: center;background:#fff;line-height: 3.5em;}
</style>
</head>
<body>
	<div class="wrap">
		<div class="logo"><c:if test="${ not empty indexLogo1 && indexLogo1!=''}"><img src="${indexLogo1}" style="width:200px;"></c:if><c:if test="${empty indexLogo1 || indexLogo1==''}">${_title }</c:if></div>
		<div class="btn"><a href="${base }/mobile/index.do" class="go-btn go-btn1">继续访问WAP手机端</a></div>
		<p class="tit">温馨提示：WAP手机端直接点击链接按钮访问</p>
		<c:if test="${(not empty Androidapp && Androidapp!='') || (not empty iosapp && iosapp!='') }"><div class="wap-text">推荐您使用手机APP购彩,请选择下载  <i class="fa fa-download"></i></div></c:if>
		<c:if test="${not empty Androidapp && Androidapp!='' }"><div class="btn min-btn"><a href="${Androidapp }" class="android"><i class="fa fa-android"></i><span>Android下载</span></a></div></c:if>
		<c:if test="${not empty iosapp && iosapp!=''}"><div class="btn min-btn"><a href="${iosapp }" class="ios"><i class="fa fa-apple"></i><span>App Store下载</span></a></div></c:if>
		<div class="btn"><a href="${base }/index.do?mobileTo=pc" class="go-btn">返回PC端</a></div>
	</div>
</body>
</html>
