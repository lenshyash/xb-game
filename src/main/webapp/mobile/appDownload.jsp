﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp"%>
<%
	Long stationId = StationUtil.getStation().getId();
	pageContext.setAttribute("appDownloadLinkIos", StationConfigUtil.get(stationId, StationConfig.app_download_link_ios));
	pageContext.setAttribute("appDownloadLinkAndroid", StationConfigUtil.get(stationId, StationConfig.app_download_link_android));
	pageContext.setAttribute("appQRCodeLinkIos", StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_ios));
	pageContext.setAttribute("appQRCodeLinkAndroid", StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_android));
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv=Content-Type content="text/html;charset=utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta name=format-detection content="telphone=no" />
<meta name=apple-mobile-web-app-capable content=yes />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<title>APP下载</title>
<style>

img {
	max-width: 100%;
}
.btn {
	width: 239px;
	height: 45px;
	margin: 20px auto 20px;
}
.wap-img{
text-align: center;
}
.wap-text {
	text-align: center;
	margin-top: 50px;
	font-size: 80%;
	padding-bottom: 10px;
}
.wap-text i {
	width: 10px;
	height: 10px;
	display: inline-block;
	margin-left: 5px
}
.wap-foot {
	font-size: 80%;
	position: fixed;
	bottom: 11px;
	width: 100%;
	text-align: center;
}
</style>
</head>
<body>
<div class="top">
	<div class="inner">
		<div class="back">
			<a href="index.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">APP下载</span>
		</div>
		<div class="cl"></div>
		
	</div>
</div><div class="cl h44"></div>
	<div id="innerbox"><div class="kaijinghaomaul">
		<div class="btn">
			<a href="${appDownloadLinkIos }" target="_blank"><img src="images/500vip/store_btn.png"></a>
		</div>
		<c:if test="${not empty appQRCodeLinkIos && appQRCodeLinkIos!=''}">
		<div class="wap-img">
			<img src="${appQRCodeLinkIos }" style="width: 13rem">
		</div>
		</c:if>
		<div class="btn">
			<a href="${appDownloadLinkAndroid }" target="_blank"><img src="images/500vip/android_btn.png"></a>
		</div>
		<c:if test="${not empty appQRCodeLinkAndroid && appQRCodeLinkAndroid!=''}">
		<div class="wap-img">
			<img src="${appQRCodeLinkAndroid }" style="width: 13rem">
		</div>
		</c:if>
		<div class="wap-text">
			温馨提示：推荐使用支付宝或者浏览器扫码二维码下载
		</div>	
		<div class="cl h80"></div>	
	</div></div>
	<div class="bomnav"><p class="wap-foot">版权所有：2009-2020©${website_name }</p></div>
</body>
</html>
