﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
</head>
<body >
<div class="top">
	<div class="inner">
		<div class="back">
			<a href="personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">个人中心</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="cl h22"></div>
	<ul class="gerenzxul">
		<li>
			<span class="fl tar w180">账号：</span>
			<span class="fl hong pl12">${account }</span>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">真实姓名：</span>
				<span class="fl  pl12 cobbb" id="userName">您的真实姓名</span>
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">手机号码：</span>
				<span class="fl  pl12 cobbb" id="phone">您的手机号码</span>
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">电子邮箱：</span>
				<span class="fl  pl12 cobbb" id="email">您的电子邮箱</span>
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">QQ：</span>
				<span class="fl  pl12 cobbb" id="qq">您的QQ</span>
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">银行卡账号：</span>
				<span class="fl  pl12 cobbb" id="cardNo">您的银行卡账号</span>
				<!-- <em class="iconfont icon-right"></em> -->
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">开户行：</span>
				<span class="fl  pl12 cobbb" id="bankName">您的开户行</span>
			</a>
			<div class="cl"></div>
		</li>
		<li>
			<a href="javascript: void(0);" class="" >
				<span class="fl tar w180">开户行地址：</span>
				<span class="fl  pl12 cobbb" id="bankAddress">您的开户行地址</span>
			</a>
			<div class="cl"></div>
		</li>
	</ul>
	<div class="cl h80"></div>
</div>
</body>

<script type="text/javascript">
	$(function(){
		$._ajax(stationUrl + "/member/accountInfo.do", function (status, message, data, result) {
			console.log(data);
			$("#userName").html(data.userName);
			$("#phone").html(data.phone);
			$("#email").html(data.email);
			$("#qq").html(data.qq);
			$("#cardNo").html(data.cardNo);
			$("#bankName").html(data.bankName);
			$("#bankAddress").html(data.bankAddress);
		});

		zishiying();
	});
</script>
</html>
<%@include file="include/check_login.jsp" %>