﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/base.css?v=1" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
<link rel="stylesheet" rev="stylesheet" href="style/font-awesome/css/font-awesome.min.css" type="text/css" />
<style type="text/css">
.bankicon{
	margin: 0px;
	padding: 0px;
}
</style>
<style type="text/css">
td.txtright {
	text-align: right;
}
td {
	color: #777;
	line-height: 20px;
}
</style>
</head>
<body>
	<div class="navbar fn-clear">
		<div class="left">
			<a class="back" href="javascript: history.go(-1);"><icon class="iconfont icon-left"></icon></a>
			<span class="title">充值</span>
		</div>
	</div>
	
	<div class="simple-row mt42">
		<span style="width: 42%; display: inline-block;">当前账户：<span class="hong">${account }</span></span><span style="display: inline-block; text-align: right; width: 58%; padding-right: 10px;">余额：<span style="min-width: 50px; display: inline-block;" class="hong" id="meminfoMoney">0</span>元<i class="icon-spinner refresh-money ml10"></i></span>
	</div>
	
	<div class="simple-row lh48 bg-color-white">
		<span>请输入充值金额</span>
		<input id="money" type="text" value="100">元
	</div>
	
	<div class="simple-row">
		请选择充值方式
	</div>
	
	<div class="warpper">
		<div class="pay-menu fn-clear">
			<a class="item display-none" payaction="wechatpay" href="javascript: void(0);" >微信</a>
			<a class="item display-none" payaction="qqpay" href="javascript: void(0);">QQ</a>
			<a class="item display-none" payaction="alipay" href="javascript: void(0);">支付宝</a>
			<a class="item display-none" payaction="onlinepay" href="javascript: void(0);">在线支付</a>
			<a class="item display-none" payaction="bankpay" href="javascript: void(0);">银行转账</a>
		</div>
		<div class="pay-content"></div>
		<div class="pay-input-content"></div>
		<div class="text-center">
			<button class="lh22 width-percent-90 mb20 mt20" id="next">下一步</button>
		</div>
		<div class="pay-tips">
			<span>温馨提示:</span>
			<div class="tips">
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/jquery.qrcode.js"></script>
<script type="text/javascript" src="script/base64.min.js"></script>
<script type="text/javascript" src="script/html2canvas/html2canvas.js"></script>
<script type="text/javascript" src="script/clipboard/clipboard.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/canvas-utils.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js?v=1"></script>
<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>
<script type="text/javascript">
var hostUrl1="${hostUrl1}";
	var openWechat = function(payaction){
		// "weixin://" : 打开微信scheme
		// "weixin://dl/scan" : 打开微信扫一扫的scheme
		// "alipayqr://platformapi/startapp?saId=10000007" : 支付宝打开扫码
		// "alipayqr://platformapi/startapp?saId=20000056" : 支付宝打开付款码
		var appLink = {wechatpay: "weixin://", alipay: "alipayqr://platformapi/startapp?saId=10000007"};

		var open_app_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
		open_app_link.href = appLink[payaction];
		open_app_link.target = "hiddenIFrame";
		var event = document.createEvent('MouseEvents');
		event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		open_app_link.dispatchEvent(event);
	}

	var toApppayOnlinepay = function (filename, payaction) {}
	var copyToClip = function (content) {}


	switch (identification) {
	case "Android":
		toApppayOnlinepay = function(filename, payaction){
			var payTitleArr = {wechatpay: "微信", alipay: "支付宝"};
			var payTitle = payTitleArr[payaction];

			var ws = plus.webview.currentWebview();
			// 截屏绘制
			var bitmap = new plus.nativeObj.Bitmap("test");
			// 将webview内容绘制到Bitmap对象中
			ws.draw(bitmap, function(){
				// console.log('截屏绘制图片成功');

				var andoridSavePath = "_doc/ifa66_screenshot.png"; // "/storage/sdcard0/Download/ifa66_screenshot.png"
				bitmap.save(andoridSavePath, { // "_downloads/", "/storage/sdcard0/Download/" + filename
					overwrite: true // 仅在保存的图片路径文件存在时有效： true表示覆盖存在的文件； false表示不覆盖，如果文件存在，则返回失败。 默认值为false。
				}, function(event){
					// var target = event.target; // 保存后的图片url路径，以"file://"开头
					// var size = event.size; // 保存后图片的大小，单位为字节（Byte）
					// var width = event.width; // 保存后图片的实际宽度，单位为px
					// var height = event.height; // 保存后图片的实际高度，单位为px
					// alert(target);
					// alert(size);
					// alert(width);
					// alert(height);
					bitmap.clear();
					// console.log('保存图片成功：'+JSON.stringify(event));

					/*
					var content = plus.android.runtimeMainActivity();
					var cr = content.getContentResolver();
					try{
						// 将截屏扫描到系统相册
						// android.provider.MediaStore.Images.Media.insertImage
						var imagePath = plus.android.invoke("android.provider.MediaStore$Images$Media", "insertImage", cr, andoridSavePath, "", "");
						// alert(imagePath);

						if(imagePath == null || imagePath == "null"){
							$.alert("截屏失败，请手动完成截屏并打开微信");
						} else {
							$.alert("截屏完成，点击确定打开微信。", function(){
								openWechat(payaction);
							});
						}
					} catch (e) {
						// alert(JSON.stringify(e));
						// console.log(e);
						$.alert("截屏失败，请手动完成截屏并打开微信");
					}
					*/

					try {
						plus.gallery.save(andoridSavePath, function (){
							$.alert("截屏完成，点击确定打开" + payTitle + "。", function(){
								openWechat(payaction);
							});
						}, function (){
							$.alert("截屏失败，请手动完成截屏并打开" + payTitle);
						});
					} catch (e){
						// alert(JSON.stringify(e));
						$.alert("截屏失败，请手动完成截屏并打开" + payTitle);
					}
				}, function(e){
					bitmap.clear();
					// console.log('保存图片失败：'+JSON.stringify(e));
				});
			}, function(e){
				bitmap.clear();
				// console.log('截屏绘制图片失败：'+JSON.stringify(e));
			});
		}

		copyToClip = function (content) {
			try {
				var Context = plus.android.importClass("android.content.Context");
				var main = plus.android.runtimeMainActivity();
				var clip = main.getSystemService(Context.CLIPBOARD_SERVICE);
				plus.android.invoke(clip, "setText", content);
				return true;
			} catch (e) {
				return false;
			}
		}
		break;
	case "IOS":
		toApppayOnlinepay = function(filename, payaction){
			var payTitleArr = {wechatpay: "微信", alipay: "支付宝"};
			var payTitle = payTitleArr[payaction];

			var ws = plus.webview.currentWebview();
			// 截屏绘制
			var bitmap = new plus.nativeObj.Bitmap("test");
			// 将webview内容绘制到Bitmap对象中
			ws.draw(bitmap, function(){
				// console.log('截屏绘制图片成功');

				var iosSavePath = "_doc/ifa66_screenshot.png";
				bitmap.save(iosSavePath, { // "_downloads/", "/var/mobile/Media/DCIM/100APPLE/" + filename, 
					overwrite: true // 仅在保存的图片路径文件存在时有效： true表示覆盖存在的文件； false表示不覆盖，如果文件存在，则返回失败。 默认值为false。
				}, function(event){
					// var target = event.target; // 保存后的图片url路径，以"file://"开头
					// var size = event.size; // 保存后图片的大小，单位为字节（Byte）
					// var width = event.width; // 保存后图片的实际宽度，单位为px
					// var height = event.height; // 保存后图片的实际高度，单位为px
					// alert(target);
					// alert(size);
					// alert(width);
					// alert(height);
					bitmap.clear();
					// console.log('保存图片成功：'+JSON.stringify(event));

					try {
						plus.gallery.save(iosSavePath, function (){
							$.alert("截屏完成，点击确定打开" + payTitle + "。", function(){
								openWechat(payaction);
							});
						}, function (){
							$.alert("截屏失败，请手动完成截屏并打开" + payTitle);
						});
					} catch (e){
						// alert(JSON.stringify(e));
						$.alert("截屏失败，请手动完成截屏并打开" + payTitle);
					}
				}, function(e){
					bitmap.clear();
					// console.log('保存图片失败：'+JSON.stringify(e));
				});
			}, function(e){
				bitmap.clear();
				// console.log('截屏绘制图片失败：'+JSON.stringify(e));
			});
		}

		copyToClip = function (content) {
			try {
				var UIPasteboard  = plus.ios.importClass("UIPasteboard");
				var generalPasteboard = UIPasteboard.generalPasteboard();
				// 设置文本内容:
				generalPasteboard.setValueforPasteboardType(content, "public.utf8-plain-text");
				return true;
			} catch (e) {
				return false;
			}
		}
		break;
	default:
		break;
	};

	function xinghao(inn){
		var out = "";
		for(var i = 0; i < inn.length - 1; i++){
			out += "*";
		}
		return out + inn.substr(-1, 1);
	}

	$(function(){
		var $refreshMoney = $(".refresh-money");
		$(".refresh-money").click(function(){
			var $this = $(this);
			$this.addClass("icon-spin");
			$._ajax(stationUrl + "/member/meminfo.do", function (status, message, data, result) {
				$("#meminfoMoney").html(data.money);
				$this.removeClass("icon-spin");
			});
		});
		$refreshMoney.click();
	});

	$(function(){
		$._ajax(stationUrl + "/deposit/config.do", function (status, message, data, result) {
			var paydata = {wechatpay: [], qqpay: [], alipay: [], bankpay: [], onlinepay: []};
			var paytips = {wechatpay: data.fastDesc, qqpay: data.fastDesc, alipay: data.fastDesc, bankpay: data.bankDesc, onlinepay: data.onlineDesc};
			var payInputTemplateId = {wechatpay: "apppayInputTemplate", qqpay: "apppayInputTemplate", alipay: "apppayInputTemplate", bankpay: "bankpayInputTemplate", onlinepay: "onlinepayInputTemplate"};
			var payContentTemplateId = {wechatpay: "apppayContentTemplate", qqpay: "apppayContentTemplate", alipay: "apppayContentTemplate", bankpay: "bankpayContentTemplate", onlinepay: "onlinepayContentTemplate"};

			if(data.onlineFlag == 'on'){
				$._ajax({url: stationUrl + "/deposit/onlinepay.do", async: !1}, function (status, message, data, result) {
					// paydata.onlinepay = data.onlines;
					for(var key in data.onlines){
						var item = data.onlines[key];
						if(item.payType + "" == "3" && item.iconCss != "yinbao" && item.iconCss != "dinpay30"){
							paydata.wechatpay.push(item);
						}
						if(item.payType + "" == "4" /*&& (item.iconCss == "danbaopay" || item.iconCss == "xunfutong")*/){
							paydata.alipay.push(item);
						}
					}
				});
			}
			if(data.fastFlag == 'on'){
				$._ajax({url: stationUrl + "/deposit/fastpay.do", async: !1}, function (status, message, data, result) {
					for(var key in data.fasts){
						var item = data.fasts[key];
						if(item.iconCss == "alipay"){
							paydata.alipay.push(item);
						}
						if(item.iconCss == "wechat"){
							paydata.wechatpay.push(item);
						}
						if(item.iconCss == "qqpay"){
							paydata.qqpay.push(item);
						}
					}
				});
			}
			if(data.bankFlag == 'on'){
				$._ajax({url: stationUrl + "/deposit/bankpay.do", async: !1}, function (status, message, data, result) {
					paydata.bankpay = data.banks;
				});
			}

			var $paymenu_items = $(".pay-menu .item");
			var showPaymenuItemCount = 0;
			if(paydata.wechatpay.length > 0){
				showPaymenuItemCount++;
				$paymenu_items.siblings("[payaction=wechatpay]").show();
			}
			if(paydata.qqpay.length > 0){
				showPaymenuItemCount++;
				$paymenu_items.siblings("[payaction=qqpay]").show();
			}
			if(paydata.alipay.length > 0){
				showPaymenuItemCount++;
				$paymenu_items.siblings("[payaction=alipay]").show();
			}
			if(paydata.bankpay.length > 0){
				showPaymenuItemCount++;
				$paymenu_items.siblings("[payaction=bankpay]").show();
			}
			if(paydata.onlinepay.length > 0){
				showPaymenuItemCount++;
				$paymenu_items.siblings("[payaction=onlinepay]").show();
			}
			$paymenu_items.css("width", (100 / showPaymenuItemCount) + "%");

			var payaction = !1;
			var dataIndex = !1;
			$(".pay-menu .item").click(function(){
				var $this = $(this);
				$paymenu_items.removeClass("selected");
				$this.addClass("selected");

				payaction = $this.attr("payaction");
				var list = paydata[payaction];

				var $payContent = $(".pay-content");
				var tplHtml = $("#" + payContentTemplateId[payaction]).html();
				$payContent.html(_.template(tplHtml)({list: list, payaction: payaction}));

				var $pay_tips = $(".pay-tips .tips");
				$pay_tips.html(paytips[payaction]);

				var $payInputContent = $(".pay-input-content");
				$payInputContent.empty();
				$(".payidRadio").on("change", function(){
					var $this = $(this);
					dataIndex = $this.attr("index");
					var payType = $this.attr("payType");

					var payaaction = payaction;
					if(payType == "3"){
						payaaction = "onlinepay";
					}
					var payInputTplHtml = $("#" + payInputTemplateId[payaaction]).html();
					$payInputContent.html(_.template(payInputTplHtml)(paydata[payaaction][dataIndex]));
				});
				$(".payidRadio:first").click();
			});
			if($paymenu_items.length){
				$paymenu_items.siblings(":visible").first().click();
			}

			$("#next").on("click", function(){
				var money = $("#money").val();
				var payId = $("input.payidRadio:checked").val();
				var iconCss = $("input.payidRadio:checked").attr("iconCss");
				var min = parseFloat($("input.payidRadio:checked").attr("min")||-1,10);
				var max = parseFloat($("input.payidRadio:checked").attr("max")||-1,10);
				var payType = $("input.payidRadio:checked").attr("payType");
				if(!payId){
					$.alert("请选择充值方式");
					return;
				}
				if(!money || isNaN(money)){
					$.alert("充值金额格式错误");
					return;
				}
				money=parseFloat(money,10);
				if(min!=-1 && money < min){
					$.alert("充值金额不能小于" + min + "元");
					return;
				}
				/*
				if((payaction == "onlinepay" || (payaction == "wechatpay" && payType == "3")) && iconCss == "mobao" && money > 1000){
					$.alert("魔宝支付手机支付金额不能大于1000元");
					return;
				}
				*/
				if(max!=-1 && money > max){
					$.alert("充值金额不能大于" + max + "元");
					return;
				}

				if(iconCss == "alipay" || iconCss == "wechat" || iconCss == "qqpay"){
					$.ajax({
						type : "POST",
						url : stationUrl + "/deposit/fastpaySubmit.do",
						data : {
							money: money,
							bankCards: $("#bankCards").val(),
							payId: payId
						},
						success : function(result) {
							if(result.success == false){
								$.alert(result.msg);
							} else if(result.success == true){
								$.alert("充值申请已提交，请尽快完成充值！");
								var tplHtml = $("#apppaySuccessTemplate").html();
								var $body = $("body");
								var orderNo = result.data;

								$body.html(_.template(tplHtml)($.extend(paydata[payaction][dataIndex], {orderNo: orderNo, money: money, payaction: payaction})));
								$('body,html').animate({scrollTop: 0},0);

								if(identification == "WebBrowser"){
									var clipboard = new Clipboard('#copyAccount');
									clipboard.on('success', function(e) {
										// console.info('Action:', e.action);
										// console.info('Text:', e.text);
										// console.info('Trigger:', e.trigger);
										$.alert("已复制收款账号");
										e.clearSelection();
									});
									clipboard.on('error', function(e) {
										// console.error('Action:', e.action);
										// console.error('Trigger:', e.trigger);
										$.alert("不支持，请手动复制");
									});
								} else {
									var $copyAccount = $("#copyAccount");
									$copyAccount.click(function(){
										var content = $copyAccount.attr("clipboard-value");
										var result = copyToClip(content);
										if(result){
											$.alert("已复制收款账号");
										} else {
											$.alert("复制失败");
										}
									});
								}
							}
						}
					});
				} else if(payaction == "bankpay"){
					$.ajax({
						type : "POST",
						url : stationUrl + "/deposit/bankpaySubmit.do",
						data : {
							money: money,
							depositor: $("#depositor").val(),
							bankId: payId
						},
						success : function(result) {
							if(result.success == false){
								$.alert(result.msg);
							} else if(result.success == true){
								$.alert("充值申请已提交，请尽快完成充值！");
								var tplHtml = $("#bankpaySuccessTemplate").html();
								var $body = $("body");

								$body.html(_.template(tplHtml)($.extend(paydata.bankpay[dataIndex], {orderNo: result.data, money: money})));
								$('body,html').animate({scrollTop: 0},0);
							}
						}
					});
				} else if(payaction == "onlinepay" || (payaction == "wechatpay" && payType == "3") || (payaction == "alipay" && payType == "4")){
					topay(money, payId, iconCss, payType, function(result){
						if(result.success == false){
							$.alert(result.msg);
						} else if(result.success == true){
							if(result && result.data && result.data.formParams){
								var orderId = result.data.orderId;
								var amount = result.data.amount;
								var payReferer = result.data.payReferer;
								if((payaction == "wechatpay" && payType == "3") || (payaction == "alipay" && payType == "4")){
									var tplHtml = $("#apppayOnlineSuccessTemplate").html();
									var $body = $("body");
									$body.html(_.template(tplHtml)({orderNo: orderId, money: amount, payaction: payaction, identification: identification}));
									$('body,html').animate({scrollTop: 0},0);

									var redirectUrl = "";
									var redirectParams = {};
									_.map(result.data.formParams, function(value, key){
										if(key == "redirectUrl"){
											redirectUrl = value;
										} else {
											redirectParams[key] = value;
										}
									});
									$.ajax({
										type : "POST",
										dataType: "json",
										url : baseUrl + "/onlinepay/utils/getWecahtQrcode.do",
										data : {
											iconCss: iconCss,
											redirectUrl: redirectUrl,
											payId:payId,
											redirectParams: JSON.stringify(redirectParams),
											payReferer: payReferer
										},
										success : function(result) {
											if(result.success == false){
												$.alert(result.msg);
											} else if(result.success == true){
												switch (iconCss) {
												case "mobao":
													var options = {
														render: 'image',
														ecLevel: 'H',
														minVersion: parseInt(1, 10),
														color: '#333333',
														bgColor: '#ffffff',
														text: Base64.decode(result.qrcodeUrl),
														size: parseInt(165, 10),
														radius: parseInt(40, 10) * 0.01,
														quiet: parseInt(2, 10),
														mode: parseInt(0, 10),

														//label: '',
														//labelsize: parseInt(11, 10) * 0.01,
														//fontname: '',
														//fontcolor: '',

														//image:$("#img-buffer")[0],
														//imagesize: parseInt(26, 10) * 0.01
													};
													//clean content element
													$("#wechat_qrcode").empty().qrcode(options);

													break;
												case "danbaopay":
												case "xunfutong":
												case "qifupay":
												case "easypay":
												case "kcpay":
												case "shangyinxin":
												case "ludepay":
													var options = {
														render: 'image',
														ecLevel: 'H',
														minVersion: parseInt(1, 10),
														color: '#333333',
														bgColor: '#ffffff',
														text: result.qrcodeUrl,
														size: parseInt(165, 10),
														radius: parseInt(40, 10) * 0.01,
														quiet: parseInt(2, 10),
														mode: parseInt(0, 10),
													};
													$("#wechat_qrcode").empty().qrcode(options);

													break;
												default:
													var qrcodeUrl = result.qrcodeUrl;
													if(qrcodeUrl){
														$("#qrCodeImg").attr("src", qrcodeUrl);
													} else {
														$("#qrCodeImg").attr("alt", "加载失败");
													}
												}
											}
										}
									});

									$("#toApppayOnlinepay").click(function(){
										if(identification == "WebBrowser"){
											openWechat(payaction);
										} else {
											toApppayOnlinepay(orderId + ".jpg", payaction);
										}
									});
								} else {
									var tplHtml = $("#toOnlinepayTemplate").html();
									var $body = $("body");
									$body.html(_.template(tplHtml)(result.data));
									$('body,html').animate({scrollTop: 0},0);
								}
							}else{
								$.alert("系统发生错误");
							}
						}
					}, false, true);
				}
			});
		});
	});
</script>
<script type="text/html" id="apppayContentTemplate" style="display: none;">
		<div>
			{#
			_.map(list, function(item, index){
			#}
				<label>
					<div class="ml18 apppay-item">
						{#
							if(payaction == "wechatpay"){
								if(item.payType == "3"){
									// print('<div class="ft13 lh20">微信支付（扫一扫直接到账）</div><div class="ft12 lh20 color_gray_2">金额范围：1-5000，推荐使用，支付后立即到账</div>');
									print('<div class="ft13 lh20">微信支付1</div><div class="ft12 lh20 color_gray_2">金额范围：1-5000，推荐使用，支付后立即到账</div>');
								} else {
									// print('<div class="ft13 lh20">微信支付（添加好友进行支付）</div><div class="ft12 lh20 color_gray_2">微信支付成功->提供游戏账号->3分钟内审核到账</div>');
									print('<div class="ft13 lh20">微信支付2</div><div class="ft12 lh20 color_gray_2">添加微信好友->微信转账成功->提供购彩账号</div>');
								}
							} else if(payaction == "qqpay"){
								print('<div class="ft13 lh20">QQ转账</div><div class="ft12 lh20 color_gray_2">添加QQ好友->QQ转账成功->提供购彩账号</div>');
							} else {
								if(item.payType == "4"){
									print('<div class="ft13 lh20">支付宝支付（实时到账）</div><div class="ft12 lh20 color_gray_2">金额范围：1-3000，支付后请稍后查看余额</div>');
									// print('<div class="ft13 lh20">支付1</div><div class="ft12 lh20 color_gray_2">金额范围：1-3000，支付后请稍后查看余额</div>');
								} else {
									print('<div class="ft13 lh20">支付宝支付（添加好友进行支付）</div><div class="ft12 lh20 color_gray_2">' + item.payAccount + '</div>');
									// print('<div class="ft13 lh20">支付2</div><div class="ft12 lh20 color_gray_2">请添加好友后转账，支付宝账号：' + item.payAccount + '</div>');
								}
							}
						#}
						<input type="radio" class="payidRadio" name="payidRadio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payType="{{item.payType}}" index="{{index}}" /><label class="radio"></label>
					</div>
				</label>
			{#
			});
			#}
		</div>
</script>
<script type="text/html" id="onlinepayContentTemplate" style="display: none;">
		<div class="ft10">
			{#
			_.map(list, function(item, index){
			#}
			<label>
				<div class="onlinepay-item">
					<div class="" style="transform: scale(0.8); display: inline-block;">
						{#
							if(!item.icon){
								print('<span title="' + item.payName + '" class="icon ' + item.iconCss + '">' + item.payName + '</span>');
							} else {
								print('<span title="' + item.payName + '" class="icon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(\'' + item.icon + '\');">' + item.payName + '</span>');
							}
						#}
						<span class="ml10 ft18">最小充值金额<font color="#ff0000">{{item.min}}</font>元</span>
					</div>
					<input type="radio" class="payidRadio" name="payidRadio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payType="{{item.payType}}" index="{{index}}" />
					<label class="radio"></label>
				</div>
			</label>
			{#
			});
			#}
		</div>
</script>
<script type="text/html" id="bankpayContentTemplate" style="display: none;">
		<div class="simple-row" style="padding-top: 4px; padding-bottom: 4px;">
			<div class="lh16">请选择您欲转入的银行卡号</div>
			<div class="ft12 lh12" style="color: #999999;">提醒您，同行转账才能立即到账哦！</div>
		</div>
		<div class="ml10">
			{#
			_.map(list, function(item, index){
			#}
			<div class="bankpay-item">
				<label>
					<div><span class="title">银行</span><span class="content">{{item.payName}}</span><input type="radio" class="payidRadio" name="payidRadio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payType="{{item.payType}}" index="{{index}}" /><label class="radio"></label></div>
					<div>
						<span class="title">收款人</span>
						<span class="content">{{item.creatorName}}</span>
						{# if(identification == "WebBrowser"){} else { #}
								<a class="button float-right ft12 iclipboard" style="padding: 0px 2px; margin-right: 50px; margin-top: 4px;" href="javascript: copyToClip('{{item.creatorName}}') ? $.alert('已复制收款人') : $.alert('复制失败');">复制</a>
						{# } #}
					</div>
					<div>
						<span class="title">银行卡号</span>
						<span class="content">{{item.bankCard}}</span>
						{# if(identification == "WebBrowser"){} else { #}
								<a class="button float-right ft12 iclipboard" style="padding: 0px 2px; margin-right: 50px; margin-top: 4px;" href="javascript: copyToClip('{{item.bankCard}}') ? $.alert('已复制银行卡号') : $.alert('复制失败');">复制</a>
						{# } #}
					</div>
					<div>
						<span class="title">开户地址</span>
						<span class="content">{{item.bankAddress}}</span>
						{# if(identification == "WebBrowser"){} else { #}
								<a class="button float-right ft12 iclipboard" style="padding: 0px 2px; margin-right: 50px; margin-top: 4px;" href="javascript: copyToClip('{{item.bankAddress}}') ? $.alert('已复制开户地址') : $.alert('复制失败');">复制</a>
						{# } #}
					</div>
				</label>
			</div>
			{#
			});
			#}
		</div>
</script>
<script type="text/html" id="apppayInputTemplate" style="display: none;">
		<!-- 
		<div class="ml10">
			<em class="iconfont icon-yuanquan1 hong"></em> <span class="hong">{{payName}}账号</span> <span class="ft9">请填写正确的<span>{{payName}}</span>账号，否则无法到帐</span>
		</div>
		<input id="bankCards" type="text" class="row" value="">
		 -->
</script>

<script type="text/html" id="bankpayInputTemplate" style="display: none;">
		<div class="ml10">
			<em class="iconfont icon-yuanquan1 hong"></em><span class="hong">存款人姓名</span>
		</div>
		<input id="depositor" type="text" class="row" value="">
</script>

<script type="text/html" id="onlinepayInputTemplate" style="display: none;">
</script>

<script type="text/html" id="bankpaySuccessTemplate" style="display: none;">
	<div class="body-background-warpper bg-color-gray">
		<div class="navbar fn-clear">
			<div class="left">
				<a class="back" href="javascript: history.go(-1);"><icon class="iconfont icon-left"></icon></a>
				<span class="title">充值</span>
			</div>
		</div>
	
		<div class="simple-row mt42 border-bottom-red">
			<span>当前账户：${account }</span>
		</div>
	
		<div class="simple-row lh50">
			<em class="iconfont icon-yuanquan1 hong"></em> 请尽快完成充值</a>
		</div>

		<div>
			<table border="0" cellpadding="3" cellspacing="3">
				<colgroup>
					<col width="30%">
					<col width="70%">
				</colgroup>
				<tbody>
					<tr>
						<td class="txtright">充值银行：</td>
						<td>{{payCom}}</td>
					</tr>
					<tr>
						<td class="txtright">收款姓名：</td>
						<td>{{creatorName}}</td>
					</tr>
					<tr>
						<td class="txtright">收款账号：</td>
						<td>{{bankCard}}</td>
					</tr>
					<tr>
						<td class="txtright">开户网点：</td>
						<td>{{bankAddress}}</td>
					</tr>
					<tr>
						<td class="txtright">订单号：</td>
						<td>{{orderNo}}</td>
					</tr>
					<tr>
						<td class="txtright">充值金额：</td>
						<td>{{money}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</script>
<script type="text/html" id="toOnlinepayTemplate" style="display: none;">
	<div class="body-background-warpper bg-color-gray">
		<div class="navbar fn-clear">
			<div class="left">
				<a class="back" href="javascript: history.go(-1);"><icon class="iconfont icon-left"></icon></a>
				<span class="title">充值</span>
			</div>
		</div>
	
		<div class="simple-row mt42 border-bottom-red">
			<span>当前账户：${account }</span>
		</div>

		<div class="pt20" >
		<form action="iBrowser.do" method="get" target='_self'>
			<input type="hidden" name="title" value="在线支付"/>
			<input type="hidden" name="toUrl" value="{{formAction}}"/>

			{#
			_.map(formParams, function(value, key){
			#}
				<input type="hidden" name="{{key}}" value="{{value}}"/>
			{#
			});
			#}
			<div class="lh40 ml10">
				订单号： <span class="ml18">{{orderId}}</span>
			</div>
			<div class="lh40 ml10">
				会员账号： <span class="ml18">{{account}}</span>
			</div>
			<div class="lh40 ml10">
				充值金额： <span class="ml18">{{amount}}</span>
			</div>
			<div class="text-center">
				<button class="lh22 width-percent-90 mb20 mt20" type='submit'>确认送出</button>
			</div>
		</form>
		</div>
	</div>
</script>
<script type="text/html" id="apppaySuccessTemplate" style="display: none;">
	<div class="body-background-warpper bg-color-gray">
		<div class="navbar fn-clear">
			<div class="left">
				<a class="back" href="javascript: history.go(-1);"><icon class="iconfont icon-left"></icon></a>
				<span class="title">充值</span>
			</div>
		</div>
	
		<div class="simple-row mt42 border-bottom-red">
			<span>当前账户：${account }</span>
		</div>
	
		<div class="simple-row lh22 mt4 pl20">
			<!-- <em class="iconfont icon-yuanquan1 hong"></em> --> {{payName}}支付信息：<!-- 请尽快完成充值 --></a>
		</div>

		<div class="pl20 pr20">
			<table class="screenshot width-percent-100" border="0" cellpadding="3" cellspacing="3">
				<colgroup>
					<col width="80px">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<td style="letter-spacing: 4px;">订单号：</td>
						<td>{{orderNo}} <!-- <a class="button float-right" style="padding: 0px 2px;" href="javascript: copyToClipBoard({{orderNo}});">复制订单号</a> --><div class="clear"></div></td>
					</tr>
					<tr>
						<td>充值金额：</td>
						<td>{{money}}</td>
					</tr>
					<tr>
						<td>收款账号：</td>
						<td>{{payAccount}} <a id="copyAccount" clipboard-value="{{payAccount}}" class="button float-right" style="padding: 0px 2px;" href="javascript: void(0);">复制账号</a></td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="qrcode">
								{# if(qrCodeImg.indexOf(".jpg") < 0 && qrCodeImg.indexOf(".jpeg") < 0 && qrCodeImg.indexOf(".png") < 0 && qrCodeImg.indexOf(".gif") < 0 && qrCodeImg.indexOf(".bmp") < 0 && qrCodeImg.indexOf("http://") < 0 && qrCodeImg.indexOf("https://") < 0 && qrCodeImg.substr(0, 2) != "//"){print(qrCodeImg);}else{print('<img id="qrCodeImg" src="' + qrCodeImg + '" style="width: 200px; height: 200px;" alt="' + qrCodeImg + '">');} #}
							</div>
						</td>
					</tr>
					<!-- 
					<tr>
						<td colspan="2"><span class="ft9" >务必将此订单编号填写到附言里，否则无法入款成功。</span></td>
					</tr>
					 -->
				</tbody>
			</table>
		</div>
		<div class="text-center">
			<a class="button lh22 mb20 mt15" style="letter-spacing: 5px;" href="javascript: location.reload();">上一步</a>
			<a class="button lh22 mb20 mt15 ml38" href="javascript: location.href='index.do';">我已支付</a>
		</div>
		<div class="ft9 pl10 pr10 pb20 bg-color-gray">
			<span>扫码步骤:</span>
			{#
			if(payaction == "wechatpay"){
			#}
			<div class="tips">
				<p class="ft13 lh20">1，请扫描二维码添加微信好友（或复制收款账号添加微信好友）。</p>
				<p class="ft13 lh20">2，添加微信好友之后，转账相应金额。</p>
				<p class="ft13 lh20">3，向转账的微信提供游戏账号。</p>
				<p class="ft13 lh20">4，等待客服人员为您充值到账。</p>
			</div>
			{#
			} else if(payaction == "qqpay"){
			#}
			<div class="tips">
				<p class="ft13 lh20">1，请扫描二维码添加QQ好友（或复制收款账号添加QQ好友）。</p>
				<p class="ft13 lh20">2，添加QQ好友之后，转账相应金额。</p>
				<p class="ft13 lh20">3，向转账的QQ提供游戏账号。</p>
				<p class="ft13 lh20">4，等待客服人员为您充值到账。</p>
			</div>
			{#
			} else {
			#}
			<div class="tips">
				<p class="ft13 lh20">1，请扫描二维码添加支付宝好友（或复制收款账号添加支付宝好友）。</p>
				<p class="ft13 lh20">2，添加支付宝好友之后，转账相应金额。</p>
				<p class="ft13 lh20">3，向转账的支付宝提供游戏账号。</p>
				<p class="ft13 lh20">4，等待客服人员为您充值到账。</p>
			</div>
			{#
			}
			#}
		</div>
		<iframe id="hiddenIFrame" name="hiddenIFrame" style="display: none;"></iframe>
	</div>
</script>
<script type="text/html" id="apppayOnlineSuccessTemplate" style="display: none;">
	<div class="body-background-warpper bg-color-gray">
		<div class="navbar fn-clear">
			<div class="left">
				<a class="back" href="javascript: history.go(-1);"><icon class="iconfont icon-left"></icon></a>
				{#
				if(payaction == "wechatpay"){
				#}
					<span class="title">微信充值</span>
				{#
				} else {
				#}
					<span class="title">支付宝充值</span>
				{#
				}
				#}
			</div>
		</div>
	
		<div class="simple-row mt42 border-bottom-red">
			<span>当前账户：${account }</span>
		</div>
	
		<div class="simple-row lh22 mt4 pl20">
			<!-- <em class="iconfont icon-yuanquan1 hong"></em> --> 
			{#
			if(payaction == "wechatpay"){
			#}
				微信支付扫码信息：
			{#
			} else {
			#}
				支付宝扫码信息：
			{#
			}
			#}
		</div>

		<div class="pl20 pr20">
			<table class="screenshot width-percent-100" border="0" cellpadding="3" cellspacing="3">
				<colgroup>
					<col width="80px">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<td style="letter-spacing: 4px;">订单号：</td>
						<td>{{orderNo}}</td>
					</tr>
					<tr>
						<td>充值金额：</td>
						<td>{{money}}</td>
					</tr>
					<tr>
						<td colspan="2"><div id="wechat_qrcode" class="qrcode"><img id="qrCodeImg" src="" alt="二维码加载中" /></div></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="text-center">
			<a class="button lh22 mb20 mt15" style="letter-spacing: 5px;" href="javascript: location.reload();">上一步</a>
			<button class="lh22 mb20 mt15 ml18" id='toApppayOnlinepay'>立即充值</button>
			<a class="button lh22 mb20 mt15 ml18" href="javascript: location.href='index.do';">我已支付</a>
		</div>
		<div class="ft9 pl10 pr10 pb20 bg-color-gray">
			<span>扫码步骤:</span>
			{#
			if(payaction == "wechatpay"){
				if(identification == "WebBrowser"){
			#}
			<div class="tips">
				<p class="ft9">1，请先手动截屏，保存图片。</p>
				<p class="ft9">2，点击“立即充值”按钮，打开微信。</p>
				<p class="ft9">3，进入扫一扫界面，点击右上角，选择“从相册选取二维码”，从相册中选取保存的二维码图片进行扫码，进入支付页面。</p>
				<p class="ft9">4，输入您预充值的金额并进行支付。</p>
				<p class="ft9">5，在支付完成后，请点击“我已支付”提交审核。</p>
				<p class="ft9">6，如果充值未及时到账，请联系在线客服。</p>
			</div>
			{#
				} else {
			#}
			<div class="tips">
				<p class="ft9">1，点击“立即充值”按钮 ，自动截屏，并打开微信。</p>
				<p class="ft9">2，进入扫一扫界面，点击右上角，选择“从相册选取二维码”，从相册中选取自动截屏的二维码图片进行扫码，进入支付页面。</p>
				<p class="ft9">3，输入您预充值的金额并进行支付。</p>
				<p class="ft9">4，在支付完成后，请点击“我已支付”提交审核。</p>
				<p class="ft9">5，如果充值未及时到账，请联系在线客服。</p>
			</div>
			{#
				}
			} else {
				if(identification == "WebBrowser"){
			#}
			<div class="tips">
				<p class="ft9">1，请先手动截屏，保存图片。</p>
				<p class="ft9">2，点击“立即充值”按钮，打开支付宝。</p>
				<p class="ft9">3，进入扫一扫界面，点击右上角第一个图标，从相册中选取保存的二维码图片进行扫码，进入支付页面。</p>
				<p class="ft9">4，输入您预充值的金额并进行支付。</p>
				<p class="ft9">5，在支付完成后，请点击“我已支付”提交审核。</p>
				<p class="ft9">6，如果充值未及时到账，请联系在线客服。</p>
			</div>
			{#
				} else {
			#}
			<div class="tips">
				<p class="ft9">1，点击“立即充值”按钮 ，自动截屏，并打开支付宝。</p>
				<p class="ft9">2，进入扫一扫界面，点击右上角第一个图标，从相册中选取保存的二维码图片进行扫码，进入支付页面。</p>
				<p class="ft9">3，输入您预充值的金额并进行支付。</p>
				<p class="ft9">4，在支付完成后，请点击“我已支付”提交审核。</p>
				<p class="ft9">5，如果充值未及时到账，请联系在线客服。</p>
			</div>
			{#
				}
			}
			#}
		</div>
		<iframe id="hiddenIFrame" name="hiddenIFrame" style="display: none;"></iframe>
	</div>
</script>
</html>
<%@include file="include/check_login.jsp" %>