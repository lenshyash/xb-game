﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<style type="text/css">

.kakakakaka input{
	width: 120px;
	padding: 0px 8px;
	height: 30px;
	background:#fff;
	border: none;
	/* font-size: 26px; */
	color:#cd0005;
	border:1px solid #ddd;
}

</style>
</head>
<body >


<div class="top">
	<div class="inner">
		
		<div class="back">
			<!-- <a href="index.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> -->
			<a href="javascript: history.go(-1);" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">真人娱乐</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="touzhubox">
	<div class="cl "></div>
	<div class="keyongzhye">
		<div class="keyongzhyein">
			<div class="f22 l30">可用余额</div>
			<div class="f22 hong l30"><span class="meminfoMoney">0.0</span>元</div>
		</div>
	</div>
	<div class="keyongzhye">
		<div class="keyongzhyein" style="border-right:none;">
			<div class="f22 l30">账户余额</div>
			<div class="f22 hong l30"><span class="meminfoMoney">0.0</span>元</div>
		</div>
	</div>
	<div class="cl "></div>

	<div class="cl"></div>
	
	<ul class="gerenzxul">
	<c:if test="${(isZrOnOff=='on' || isDzOnOff eq 'on' || isChessOnOff eq 'on' || isThdLotOnOff eq 'on' || isTsOnOff eq 'on')&& isGuest != true}">
	<c:if test="${isIbcOnOff eq 'on' && isGuest != true}">
		<li data-livetype="ibc">
			<span class="fl tar w80 f22">沙巴体育：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ibc" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ibc">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardIbc.do?h5=1', '10', this);" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
		<li data-livetype="ag">
			<span class="fl tar w80 f22">AG：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ag" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ag">刷新</a>
			<span class="fr pr24 f22"><a href="javascript:void(0)"onclick="go('${base }/forwardAg.do?h5=1&gameType=11', '1',this,'hong');" target="_blank" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
		<li data-livetype="bbin">
			<span class="fl tar w80 f22">BBIN：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit bbin" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="bbin">刷新</a>
			<span class="fr pr24 f22"><a href="javascript:void(0)"onclick="go('${base }/forwardBbin.do?type=live', '2',this,'hong');" target="_blank" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isKyOnOff eq 'on' && isGuest != true}">
		<li data-livetype="ky">
			<span class="fl tar w80 f22">KY：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ky" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ky">刷新</a>
			<span class="fr pr24 f22"><a href="javascript:void(0)"onclick="go('${base }/forwardky.do?h5=1', '12',this,'hong');" target="_blank" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
		<li data-livetype="bg">
			<span class="fl tar w80 f22">BG：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit bg" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="bg">刷新</a>
			<span class="fr pr24 f22"><a href="javascript:void(0)"onclick="go('${base }/forwardBg.do?type=2&h5=1', '98',this,'hong');" target="_blank" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isVrOnOff eq 'on' && isGuest != true}">
		<li data-livetype="vr">
			<span class="fl tar w80 f22">VR：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit vr" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="vr">刷新</a>
			<span class="fr pr24 f22"><a href="javascript:void(0)"onclick="go('${base }/forwardVr.do', '97',this,'hong');" target="_blank" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
		<li data-livetype="mg">
			<span class="fl tar w80 f22">MG：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit mg" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="mg">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardMg.do?gameType=1&gameid=59627&h5=1', '3', this, 'hong');" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isAbOnOff eq 'on' && isGuest != true}">
		<li data-livetype="ab">
			<span class="fl tar w80 f22">AB：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ab" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ab">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardAb.do', '5', this, 'hong');" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>	
	<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
		<li data-livetype="pt">
			<span class="fl tar w80 f22">PT：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit pt" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="pt">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=pt" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>	
	<c:if test="${isQtOnOff eq 'on' && isGuest != true}">
		<li data-livetype="qt">
			<span class="fl tar w80 f22">QT：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit qt" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="qt">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=qt" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>	
	<c:if test="${isOgOnOff eq 'on' && isGuest != true}">
		<li data-livetype="og">
			<span class="fl tar w80 f22">OG：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit og" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="og">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardOg.do?gametype=mobile', '5', this, 'hong');" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isDsOnOff eq 'on' && isGuest != true}">	
		<li data-livetype="ds">
			<span class="fl tar w80 f22">DS：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ds" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ds">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardDs.do?h5=1', '3', this, 'hong');" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isCq9OnOff eq 'on' && isGuest != true}">
		<li data-livetype="cq9">
			<span class="fl tar w80 f22">CQ9：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit cq9" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="cq9">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=cq9" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isJdbOnOff eq 'on' && isGuest != true}">
		<li data-livetype="jdb">
			<span class="fl tar w80 f22">JDB：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit jdb" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="jdb">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=jdb" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isTtgOnOff eq 'on' && isGuest != true}">
		<li data-livetype="ttg">
			<span class="fl tar w80 f22">TTG：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit ttg" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="ttg">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=ttg" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isMwOnOff eq 'on' && isGuest != true}">
		<li data-livetype="mw">
			<span class="fl tar w80 f22">MW：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit mw" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="mw">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=mw" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isIsbOnOff eq 'on' && isGuest != true}">
		<li data-livetype="isb">
			<span class="fl tar w80 f22">ISB：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit isb" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="isb">刷新</a>
			<span class="fr pr24 f22"><a href="${base }/mobile/egame/index.do?code=isb" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isM8OnOff eq 'on' && isGuest != true}">
		<li data-livetype="m8">
			<span class="fl tar w80 f22">M8：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit m8" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="m8">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardM8.do?h5=1', '99', this);" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
	<c:if test="${isM8HOnOff eq 'on' && isGuest != true}">
		<li data-livetype="m8h">
			<span class="fl tar w80 f22">M8H：</span>
			<span class="fl pl12 f22"><span class="hong credit">0</span> <span class="cobbb">RMB</span></span>
			<a href="javascript: void(0);" class="btn btn-default f18 refreshcredit m8h" style="margin: 22px 0px 0px 20px;padding: 1px 6px;" data-type="m8h">刷新</a>
			<span class="fr pr24 f22"><a href="javascript: go('${base}/forwardM8H.do?h5=1', '991', this);" class="hong">进入游戏</a></span>
			<span class="fr pr24 f22"><a class="lanx trans" data-transtype="in" data-toggle="modal" data-target="#quotaTransModal">转入</a> <a class="lanx trans" data-transtype="out" data-toggle="modal" data-target="#quotaTransModal">转出</a></span>
			<div class="cl"></div>
		</li>
	</c:if>
		</c:if>
	</ul>
	<div class="cl"></div>
	<div id="recordContent" class="tzhuanqie-huan cur">
	</div>
	<div class="cl"></div>
	<div class="cl h80"></div>
	</div>
</div>

<div class="modal fade" id="quotaTransModal" tabindex="-1" role="dialog" aria-labelledby="quotaTransModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title f22" id="quotaTransModalLabel">额度转换</h4>
			</div>
			<div class="modal-body">
				<div class="f18"><span class="hong" id="currLiveType">AG</span>额度转换</div>
				<div class="f16" style="margin-top: 20px;">从<span class="hong" id="transFrom">系统</span>转账到<span class="hong" id="transTo">AG</span></div>
				<div class="kakakakaka" style="margin-top: 20px;">
					<span class="f16">金额：</span> <input id="transMoney" type="text" value=""> <a id="transSubmitBtn" href="javascript: void(0);" class="btn btn-default btn-sm" style="margin-left: 10px;">转换额度</a>
				</div>
				<div style="margin-top: 20px; " class="hong" id="errorMessageDiv"></div>
				<div style="margin-top: 20px;">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js?v=1"></script>
<script type="text/javascript" src="script/$this/jump_live.js?v=87"></script>
<script type="text/javascript">
	$(function(){
		var loadBalance = function(){
			$._ajax(stationUrl + "/member/meminfo.do", function (status, message, data, result) {
				console.log(data);
				$(".meminfoMoney").html(data.money);
			});
		}

		loadBalance();

		$(".refreshcredit").on("click", function(){
			var type = $(this).data("type");

			var $creditSpan = $(this).parent().find("span.credit");

			$creditSpan.html("<img src='${station}/images/loading.gif' />");

			$.ajax({
		        url:'${base}/rc4m/getBalance.do',
		        type:'POST',
		        data:{type: type},
		        success:function(json,status,xhr){
		         	var ceipstate = xhr.getResponseHeader("ceipstate")
					if (!ceipstate || ceipstate == 1) {// 正常响应
						if(json.balance==-1 || json.balance=='-1'){
							$creditSpan.text("第三方账户还未开通");
						}else{
							$creditSpan.text(json.balance);
						}
					} else {// 后台异常
						$creditSpan.text("获取失败")
					}
		        	
		        }
		    });
		});

		$(".refreshcredit").trigger("click");

		$(".trans").on("click", function(){
			var transtype = $(this).data("transtype");
			var livetype = $(this).parents("li").data("livetype");

			var transFrom,transTo;
			if(transtype == "in"){
				transFrom = "sys";
				transTo = livetype;
			} else { // "out"
				transFrom = livetype;
				transTo = "sys";
			}

			$("#currLiveType").html(livetype);
			$("#transFrom").html(transFrom == "sys" ? "系统" : transFrom).data("value", transFrom);
			$("#transTo").html(transTo == "sys" ? "系统" : transTo).data("value", transTo);
			$("#errorMessageDiv").empty();
		});

		$("#transSubmitBtn").on("click", function(){
			$("#transSubmitBtn").addClass("disabled");
			$("#errorMessageDiv").empty();
			var transFrom = $("#transFrom").data("value");
			var transTo = $("#transTo").data("value");
			var transMoney = $("#transMoney").val();

			if(transMoney){
				$.ajax({
			        url:'${base}/rc4m/thirdRealTransMoney.do',
			        type:'POST',
			        data:{changeFrom: transFrom, changeTo: transTo, quota: transMoney},
			        success:function(json,status,xhr){
			        	var ceipstate = xhr.getResponseHeader("ceipstate")
						if (!ceipstate || ceipstate == 1) {// 正常响应
				        	if(json.success){
				        		$("#quotaTransModal").modal('hide');
								var qqqqqqqqqqqq = function(){
									$(".refreshcredit." + $("#currLiveType").text()).trigger("click");
									loadBalance();
								}
								$.dialog({content: "转账成功", okValue: "刷新余额", ok: function(){
									qqqqqqqqqqqq();
								}, canelValue: "刷新余额", canel: function(){
									qqqqqqqqqqqq();
								}});
				        	}else{
				        		$("#errorMessageDiv").html("错误信息：" + json.msg);
				        	}
						}else {// 登录异常
							$("#errorMessageDiv").html("错误信息：" + json.msg);
						}
			        	$("#transSubmitBtn").removeClass("disabled");
			        }
			    });
			} else {
				$("#errorMessageDiv").html("错误信息：请输入转换额度");
				$("#transSubmitBtn").removeClass("disabled");
			}
		});
	});
</script>
</html>
<%@include file="include/check_login.jsp" %>