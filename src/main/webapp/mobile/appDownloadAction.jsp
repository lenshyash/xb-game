﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp"%>
<%
	Long stationId = StationUtil.getStation().getId();
	pageContext.setAttribute("appDownloadLinkIos", StationConfigUtil.get(stationId, StationConfig.app_download_link_ios));
	pageContext.setAttribute("appDownloadLinkAndroid", StationConfigUtil.get(stationId, StationConfig.app_download_link_android));
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>APP下载</title>
<script type="text/javascript">
	var Terminal = {
		// 辨别移动终端类型
		platform : function() {
			var u = navigator.userAgent, app = navigator.appVersion;
			return {
				// android终端或者uc浏览器
				android : u.indexOf('Android') > -1 || u.indexOf('Linux') > -1,
				// 是否为iPhone或者QQHD浏览器
				iPhone : u.indexOf('iPhone') > -1,
				// 是否iPad
				iPad : u.indexOf('iPad') > -1
			};
		}(),
		// 辨别移动终端的语言：zh-cn、en-us、ko-kr、ja-jp
		language : (navigator.browserLanguage || navigator.language).toLowerCase()
	}
	// 根据不同的终端，跳转到不同的地址
	var theUrl = '${appDownloadLinkAndroid}'; // 默认为Android地址
	if (Terminal.platform.android) {
		// theUrl = '你的Android APP对应下载地址：apk文件地址';
	} else if (Terminal.platform.iPhone) {
		// theUrl = '你的iPhone APP对应下载地址：APP Store地址';
		theUrl = '${appDownloadLinkIos}';
	} else if (Terminal.platform.iPad) {
		// 还可以通过language，区分开多国语言版
		/*
		switch (Terminal.language) {
		case 'en-us':
			theUrl = '你的iPad APP（英文版）对应下载地址：APP Store地址';
			break;
		case 'ko-kr':
			theUrl = '你的iPad APP（韩语版）对应下载地址：APP Store地址';
			break;
		case 'ja-jp':
			theUrl = '你的iPad APP（日文版）对应下载地址：APP Store地址';
			break;
		default:
			theUrl = '你的iPad APP（中文版-默认）对应下载地址：APP Store地址';
		}
		*/
		theUrl = '${appDownloadLinkIos}';
	}
	location.href = theUrl;
</script>
</head>
<body>
</body>
</html>
