﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2"
	type="text/css" />
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<a href="personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> <span class="vbktl">真人投注记录</span>
			</div>
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div id="selectQueryType" class="touzhuqie" data-current="all">
				<span data-querytype="all" class="cur" style="width: 100%;">全部记录 </span> 
			</div>
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent" data-value="">
						<div class="fangsjbox">时间</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="today">今天</li>
								<li data-value="yesterday">昨天</li>
								<li data-value="week">近一周</li>
								<li data-value="month">近30天</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryLiveType" class="touzhusj selectComponent" data-value="">
						<div class="fangsjbox">平台类型</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="1">AG</li>
								<li data-value="3">MG</li>
								<li data-value="2">BBIN</li>
								<li data-value="5">ALLBET</li>
								<li data-value="7">OG</li>
								<li data-value="8">DS</li>
								<li data-value="12">KY</li>
								<li data-value="98">BG</li>
								<li data-value="97">BG</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div class="caizhongtable">
				<table>
					<tr class="caizhongtabletr">
						<td>游戏类型</td>
						<td>投注时间</td>
						<td>投注金额</td>
						<td>输赢金额</td>
					</tr>
					<tbody id="recordContent">
					</tbody>
					<tfoot>
						<td colspan="2" style="text-align:right">总计：</td>
						<td id="sumBet"></td>
						<td id="sumWin"></td>
					</tfoot>
				</table>
			</div>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/betting_record_live.js?v=11"></script>
<script type="text/html" id="recoredTemplate" style="display: none;">
{# 
	_.map(data, function(item){
 #}
<tr>
	<td>{{item.gameType}}</td>
	<td>{{item.bettime}}</td>
	<td>{{item.betMoney}}元</td>
	<td><em class="hong">{{item.payMoney}}</em></td>
</tr>
{#
	});
#}
</script>
</html>
<%@include file="include/check_login.jsp"%>