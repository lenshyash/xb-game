﻿<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="include/common.jsp" %>
<%
String[] baseParamKey = new String[]{"title", "toUrl", "backUrl", "actionMethod"};

String title = request.getParameter(baseParamKey[0]);
title = new String(title.getBytes("ISO-8859-1"));
String toUrl = request.getParameter(baseParamKey[1]);
String backUrl = request.getParameter(baseParamKey[2]);
String actionMethod = request.getParameter(baseParamKey[3]);
if(StringUtils.isEmpty(actionMethod)){
	actionMethod = "post";
}

if(!StringUtils.isEmpty(toUrl)){
	toUrl = toUrl.replace("{@}", "&");

	Map<String, String> params = new HashMap<String, String>();

	if(!StringUtils.isEmpty(toUrl) && "get".equals(actionMethod)){
		String paramsStr = toUrl.substring(toUrl.indexOf("?") + 1);
		String[] paramStrArr = paramsStr.split("&");
		for(String paramStr : paramStrArr){
			if(!StringUtils.isEmpty(paramStr)){
				String[] ppp = paramStr.split("=");
				if(ppp != null && ppp.length >= 2){
					params.put(ppp[0], ppp[1]);
				}
			}
		}
	}

	pageContext.setAttribute(baseParamKey[0], title);
	pageContext.setAttribute(baseParamKey[1], toUrl);
	pageContext.setAttribute(baseParamKey[2], backUrl);
	pageContext.setAttribute(baseParamKey[3], actionMethod);

	List<String> filter = Arrays.asList(baseParamKey);

	Enumeration<String> pNames = request.getParameterNames();
	while(pNames.hasMoreElements()){
		String pName = pNames.nextElement();
		if(filter.contains(pName)){
			continue;
		}
		String pValue = request.getParameter(pName);
		params.put(pName, pValue);
	}

	pageContext.setAttribute("params", params);
}

%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/base.css" type="text/css" />
</head>
<body>
	<div class="navbar fn-clear">
		<div class="left">
			<c:if test="${empty backUrl}">
				<a class="back" href="javascript: top.history.go(-1);"><icon class="iconfont icon-left"></icon></a>
			</c:if>
			<c:if test="${not empty backUrl}">
				<a class="back" href="${backUrl }"><icon class="iconfont icon-left"></icon></a>
			</c:if>
			<span class="title">${title }</span>
		</div>
	</div>

	<div class="browser-warpper">
		<form id="browserParamsForm" action="${toUrl }" target="browserFrame" method="${actionMethod }">
			<c:forEach items="${params }" var="p">
				<input name="${p.key }" value="${p.value }" type="hidden" />
			</c:forEach>
		</form>
		<iframe name="browserFrame"></iframe>
	</div>
</body>
<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		/*
		var toUrl = "${toUrl }";
		toUrl = toUrl.replace(/{@}/g, "&");
		var $tempForm = $("<form>");
		$tempForm.attr({
			action: toUrl,
			target: "browserFrame",
			method: "post"
		});
		<c:forEach items="${params }" var="p">
		$tempForm.append('<input name="${p.key }" value="${p.value }" type="hidden" />');
		</c:forEach>
		$tempForm.submit();
		*/
		$("#browserParamsForm").submit();
	});
</script>
</html>