﻿<%@ page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<% 
Long stationId = StationUtil.getStation().getId();

pageContext.setAttribute("lottery_order_cancle_switch", StationConfigUtil.get(stationId, StationConfig.lottery_order_cancle_switch));
 %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 10px 16px;
    margin-left: -1px;
    line-height: 1.3333333;
    color: #cd0005;
    text-decoration: none;
    background-color: #ddd;
    border: 1px solid #fff;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #cd0005;
    border-color: #cd0005;
}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 3;
    color: #cd0005;
    background-color: #eee;
    border-color: #ddd;
}
</style>
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<a href="personal_center.do" style="color: #fff;"><em
					class="iconfont icon-left"></em></a> <span class="vbktl">六合彩投注记录</span>
			</div>
			<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div id="selectQueryType" class="touzhuqie" data-current="all">
				<span data-querytype="all" class="cur" style="width: 25%">全部记录 </span> 
				<span data-querytype="init" class="" style="width: 25%">未开奖 </span> 
				<span data-querytype="win" class="" style="width: 25%">中奖纪录 </span> 
				<span data-querytype="lost" class="" style="width: 25%">未中奖 </span> 
				<!-- <span data-querytype="canel" class="">撤单 </span> --> 
			</div>
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent" data-value="today">
						<div class="fangsjbox">今天</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="today">今天</li>
								<li data-value="yesterday">昨天</li>
								<li data-value="week">近一周</li>
								<li data-value="month">近30天</li>
								<li data-value="monthbefore">30天以前</li>
								<!-- 
								<li data-value="60before">60天以前</li>
								<li data-value="90before">90天以前</li>
								 -->
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div class="caizhongtable" id="recordContent">
			</div>
			<nav id="pageview">
			</nav>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/betting_record_sixmark.js?v=1.11"></script>
</html>
<script type="text/html" id="recoredNewTemplate" style="display: none;">
				<table>
					<tr class="caizhongtabletr">
						<td>彩种</td>
						<td>投注时间</td>
						<td>投注金额</td>
						<td>当前状态</td>
						<td>操作</td>
					</tr>
					<tbody>
					{# 
						_.map(data, function(item){
						var statusTitle = "其他";
					switch(item.status){
						case 1:
							statusTitle = "未开奖";
							break;
						case 2:
							statusTitle = "已派奖";
							break;
						case 3:
							statusTitle = "未中奖";
							break;
						case 4:
							statusTitle = "撤单";
							break;
						case 5:
							statusTitle = "派奖回滚成功";
							break;
						case 6:
							statusTitle = "回滚异常的";
							break;
						case 7:
							statusTitle = "开奖异常";
							break;
						case 8:
							statusTitle = "和局";
							break;
						}
					 #}
					<tr data-orderid="{{item.orderId}}" data-lotcode="{{item.lotCode}}">
						<td>{{item.lotName}}</td>
						<td>{{new Date(item.createTime).format()}}</td>
						<td>{{item.buyMoney}}元</td>
						<td>{{statusTitle}}</td>
						<td><a href="betting_record_lottery_details.do?lotCode={{item.lotCode}}&orderId={{item.orderId}}" class="lanx">查看</a>
							<!-- <a href="{# if( item.lotCode != 'LHC') { print('do_pick_number.do?lotCode=' + item.lotCode ); } else { print('lhc/index.do'); } #}" class="lanx">继续投注</a> -->
							<c:if test="${lottery_order_cancle_switch ne 'off' }">
							{# if(item.status == 1 && item.lotCode == 'LHC') { print('<a href="javascript: void(0);" class="lanx" name="chedan">撤单</a>'); } #}
							</c:if>
						</td>
					</tr>
					{#
						});
					#}
					<tr>
						<td colspan="2" style="text-align:right;">总计:</td>
						<td id="sumBet">0元</td>
						<td id="sumWin">0元</td>
						<td></td>
					</tr>
					</tbody>
				</table>
</script>
<script type="text/html" id="pageviewTemplate" style="display: none;">
				{#
				if(data.totalPageCount > 1){
				#}
				<ul class="pagination pagination-lg pull-right">
					<li>
						<a href="javascript: void(0);" aria-label="Previous" pageno="{{data.prePage}}">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<!-- 
					<li class="active"><a href="javascript: void(0);">1</a></li>
					<li><a href="javascript: void(0);">2</a></li>
					<li><a href="javascript: void(0);">3</a></li>
					<li><a href="javascript: void(0);">4</a></li>
					<li><a href="javascript: void(0);">5</a></li>
					 -->
					<li>
						<a href="javascript: void(0);" aria-label="Next" pageno="{{data.nextPage}}">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
				{#
				}
				#}
</script>
<%@include file="include/check_login.jsp" %>