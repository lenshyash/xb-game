var agdata = [
            { 
            	"DisplayName": "捕鱼王者",
            	"ButtonImagePath": "SB36.gif",
            	"LapisId": "6" ,
            	"typeid":"4"
            },{
            	"DisplayName" : "赌城之夜",
            	"ButtonImagePath" : "SB56_ZH.png",
            	"LapisId" : "SB56",
            	"typeid" : "4"
            },{
            	"DisplayName" : "极品飙车",
            	"ButtonImagePath" : "WH54_ZH.png",
            	"LapisId" : "WHGWH54",
            	"typeid" : "4"
            },{
            	"DisplayName" : "寿司大食客",
            	"ButtonImagePath" : "WH26_ZH.png",
            	"LapisId" : "WHGWH26",
            	"typeid" : "4"
            },{
            	"DisplayName" : "皇家戏台",
            	"ButtonImagePath" : "WH40_ZH.png",
            	"LapisId" : "WHGWH40",
            	"typeid" : "4"
            },{
            	"DisplayName" : "蒸汽战争",
            	"ButtonImagePath" : "WH44_ZH.png",
            	"LapisId" : "WHGWH44",
            	"typeid" : "4"
            },{
            	"DisplayName" : "魅惑魔女",
            	"ButtonImagePath" : "SB58_ZH.png",
            	"LapisId" : "169",
            	"typeid" : "4"
            },{
            	"DisplayName" : "橫行霸道",
            	"ButtonImagePath" : "WH36_ZH.png",
            	"LapisId" : "WHGWH36",
            	"typeid" : "4"
            },{
            	"DisplayName" : "古惑仔",
            	"ButtonImagePath" : "WH42_ZH.png",
            	"LapisId" : "WHGWH42",
            	"typeid" : "4"
            },{
            	"DisplayName" : "神奇宠物",
            	"ButtonImagePath" : "WH55_ZH.png",
            	"LapisId" : "WHGWH55",
            	"typeid" : "4"
            },{
            	"DisplayName" : "魔龙",
            	"ButtonImagePath" : "SB57_ZH.png",
            	"LapisId" : "168",
            	"typeid" : "4"
            },{
            	"DisplayName" : "财宝塔罗",
            	"ButtonImagePath" : "WH19_ZH.png",
            	"LapisId" : "WHGWH19",
            	"typeid" : "4"
            },{
            	"DisplayName" : "埃及宝藏",
            	"ButtonImagePath" : "WH28_ZH.png",
            	"LapisId" : "WHGWH28",
            	"typeid" : "4"
            },{
            	"DisplayName" : "封神演义",
            	"ButtonImagePath" : "WH23_ZH.png",
            	"LapisId" : "WHGWH23",
            	"typeid" : "4"
            },{
            	"DisplayName" : "和风剧院",
            	"ButtonImagePath" : "WH27_ZH.png",
            	"LapisId" : "WHGWH27",
            	"typeid" : "4"
            },{
            	"DisplayName" : "点石成金",
            	"ButtonImagePath" : "WH30_ZH.png",
            	"LapisId" : "WHGWH30",
            	"typeid" : "4"
            },{
            	"DisplayName" : "圣女贞德",
            	"ButtonImagePath" : "WH02_ZH.png",
            	"LapisId" : "WHGWH02",
            	"typeid" : "4"
            },{
            	"DisplayName" : "五狮进宝",
            	"ButtonImagePath" : "WH07_ZH.png",
            	"LapisId" : "WHGWH07",
            	"typeid" : "4"
            },{
            	"DisplayName" : "发财熊猫",
            	"ButtonImagePath" : "WH12_ZH.png",
            	"LapisId" : "WHGWH12",
            	"typeid" : "4"
            },{
            	"DisplayName" : "十二生肖",
            	"ButtonImagePath" : "WH38_ZH.png",
            	"LapisId" : "WHGWH38",
            	"typeid" : "4"
            },{
            	"DisplayName" : "招财锦鲤",
            	"ButtonImagePath" : "WH35_ZH.png",
            	"LapisId" : "WHGWH35",
            	"typeid" : "4"
            },{
            	"DisplayName" : "白雪公主",
            	"ButtonImagePath" : "WH18_ZH.png",
            	"LapisId" : "WHGWH18",
            	"typeid" : "4"
            },{
            	"DisplayName" : "葫芦兄弟",
            	"ButtonImagePath" : "WH20_ZH.png",
            	"LapisId" : "WHGWH20",
            	"typeid" : "4"
            },{
            	"DisplayName" : "内衣橄榄球",
            	"ButtonImagePath" : "WH34_ZH.png",
            	"LapisId" : "WHGWH34",
            	"typeid" : "4"
            },{
            	"DisplayName" : "贪玩蓝月",
            	"ButtonImagePath" : "WH32_ZH.png",
            	"LapisId" : "WHGWH32",
            	"typeid" : "4"
            },{
            	"DisplayName" : "多宝鱼虾蟹",
            	"ButtonImagePath" : "SB55_ZH.png",
            	"LapisId" : "166",
            	"typeid" : "4"
            },{
            	"DisplayName" : "跳跳乐",
            	"ButtonImagePath" : "WC01_ZH.png",
            	"LapisId" : "WHGWC01",
            	"typeid" : "4"
            },{
            	"DisplayName" : "永恒之吻",
            	"ButtonImagePath" : "WH21_ZH.png",
            	"LapisId" : "WHGWH21",
            	"typeid" : "4"
            },{
            	"DisplayName" : "恐怖嘉年华",
            	"ButtonImagePath" : "WH22_ZH.png",
            	"LapisId" : "WHGWH22",
            	"typeid" : "4"
            },{
            	"DisplayName" : "僵尸来袭",
            	"ButtonImagePath" : "WH24_ZH.png",
            	"LapisId" : "WHGWH24",
            	"typeid" : "4"
            },{
            	"DisplayName" : "狂野女巫",
            	"ButtonImagePath" : "WH29_ZH.png",
            	"LapisId" : "WHGWH29",
            	"typeid" : "4"
           },
            { "DisplayName" : "嫦娥奔月","ButtonImagePath" : "WH17_ZH.png","LapisId" : "WHGWH17","typeid" : "4"},
            { "DisplayName" : "钻石女王","ButtonImagePath" : "WA01_ZH.png","LapisId" : "WHGWA01","typeid" : "4"},
            { "DisplayName" : "亚瑟王","ButtonImagePath" : "WH06_ZH.png","LapisId" : "WHGWH06","typeid" : "4"},
            { "DisplayName" : "爱丽丝大冒险","ButtonImagePath" : "WH10_ZH.png","LapisId" : "WHGWH10","typeid" : "4"},
            { "DisplayName": "战火风云", "ButtonImagePath": "WH11_ZH.png", "LapisId": "WHGWH11" ,"typeid":"4"},
            { "DisplayName": "水果拉霸", "ButtonImagePath": "FRU.jpg", "LapisId": "501" ,"typeid":"3"},
            { "DisplayName": "杰克高手", "ButtonImagePath": "PKBJ.jpg", "LapisId": "502" ,"typeid":"3"},
            { "DisplayName": "极速幸运轮", "ButtonImagePath": "TGLW.jpg", "LapisId": "507","typeid":"2"},
            { "DisplayName": "太空漫游", "ButtonImagePath": "SB01.jpg", "LapisId": "508" ,"typeid":"3"},
            { "DisplayName": "复古花园", "ButtonImagePath": "SB02.jpg", "LapisId": "509" ,"typeid":"3"},
           // { "DisplayName": "关东煮", "ButtonImagePath": "SB03.jpg", "LapisId": "510" ,"typeid":"4"},
            { "DisplayName": "牧场咖啡", "ButtonImagePath": "SB04.jpg", "LapisId": "511" ,"typeid":"4"},
            { "DisplayName": "甜一甜屋", "ButtonImagePath": "SB05.jpg", "LapisId": "512" ,"typeid":"4"},
            { "DisplayName": "日本武士", "ButtonImagePath": "SB06.jpg", "LapisId": "513" ,"typeid":"4"},
            { "DisplayName": "象棋老虎机", "ButtonImagePath": "SB07.jpg", "LapisId": "514" ,"typeid":"4"},
            { "DisplayName": "麻将老虎机", "ButtonImagePath": "SB08.jpg", "LapisId": "515" ,"typeid":"4"},
            { "DisplayName": "西洋棋老虎机", "ButtonImagePath": "SB09.jpg", "LapisId": "516" ,"typeid":"4"},
            { "DisplayName": "开心农场", "ButtonImagePath": "SB10.jpg", "LapisId": "517" ,"typeid":"4"},
            { "DisplayName": "夏日营地", "ButtonImagePath": "SB11.jpg", "LapisId": "518" ,"typeid":"4"},
            { "DisplayName": "海底漫游", "ButtonImagePath": "SB12.jpg", "LapisId": "519" ,"typeid":"4"},
            { "DisplayName": "鬼马小丑", "ButtonImagePath": "SB13.jpg", "LapisId": "520" ,"typeid":"4"},
            { "DisplayName": "机动乐园", "ButtonImagePath": "SB14.jpg", "LapisId": "521" ,"typeid":"4"},
            { "DisplayName": "惊吓鬼屋", "ButtonImagePath": "SB15.jpg", "LapisId": "522" ,"typeid":"4"},
            { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" ,"typeid":"4"},
            { "DisplayName": "海洋剧场", "ButtonImagePath": "SB17.jpg", "LapisId": "524" ,"typeid":"4"},
            { "DisplayName": "水上乐园", "ButtonImagePath": "SB18.jpg", "LapisId": "525" ,"typeid":"4"},
            { "DisplayName": "空中战争", "ButtonImagePath": "SB19.jpg", "LapisId": "526" ,"typeid":"4"},
            { "DisplayName": "摇滚狂迷", "ButtonImagePath": "SB20.jpg", "LapisId": "527" ,"typeid":"4"},
            { "DisplayName": "越野机车", "ButtonImagePath": "SB21.jpg", "LapisId": "528" ,"typeid":"4"},
            { "DisplayName": "埃及奥秘", "ButtonImagePath": "SB22.jpg", "LapisId": "529" ,"typeid":"4"},
            { "DisplayName": "欢乐时光", "ButtonImagePath": "SB23.png", "LapisId": "530" ,"typeid":"4"},
            { "DisplayName": "侏罗纪", "ButtonImagePath": "SB24.jpg", "LapisId": "531" ,"typeid":"4"},
            { "DisplayName": "土地神", "ButtonImagePath": "SB25.jpg", "LapisId": "532" ,"typeid":"4"},
            { "DisplayName": "布袋和尚", "ButtonImagePath": "SB26.jpg", "LapisId": "533" ,"typeid":"4"},
            { "DisplayName": "正财神", "ButtonImagePath": "SB27.jpg", "LapisId": "534" ,"typeid":"4"},
            { "DisplayName": "武财神", "ButtonImagePath": "SB28.jpg", "LapisId": "535" ,"typeid":"4"},
            { "DisplayName": "偏财神", "ButtonImagePath": "SB29.jpg", "LapisId": "536" ,"typeid":"4"},
            { "DisplayName": "天空守护者", "ButtonImagePath": "SB31.jpg", "LapisId": "542" ,"typeid":"3"},
            { "DisplayName": "齐天大圣", "ButtonImagePath": "SB32.gif", "LapisId": "543" ,"typeid":"2"},
            { "DisplayName": "糖果碰碰乐", "ButtonImagePath": "SB33.gif", "LapisId": "544" ,"typeid":"2"},
            { "DisplayName": "冰河世界", "ButtonImagePath": "SB34.jpg", "LapisId": "545" ,"typeid":"4"},
            { "DisplayName": "欧洲列强争霸", "ButtonImagePath": "SB35.gif", "LapisId": "547" ,"typeid":"4"},
            { "DisplayName": "上海百乐门", "ButtonImagePath": "SB37.jpg", "LapisId": "549" ,"typeid":"2"},
            { "DisplayName": "性感女僕", "ButtonImagePath": "AV01.jpg", "LapisId": "537" ,"typeid":"3"},
            { "DisplayName": "百搭二王", "ButtonImagePath": "PKBD.gif", "LapisId": "540" ,"typeid":"2"},
            { "DisplayName": "红利百搭", "ButtonImagePath": "PKBB.gif", "LapisId": "541" ,"typeid":"2"},
            { "DisplayName": "水果拉霸2", "ButtonImagePath": "FRU2.gif", "LapisId": "546" ,"typeid":"2"},
            { "DisplayName": "龙珠", "ButtonImagePath": "XG01.jpg", "LapisId": "200" ,"typeid":"4"},
            { "DisplayName": "幸运8", "ButtonImagePath": "XG02.jpg", "LapisId": "201" ,"typeid":"4"},
            { "DisplayName": "闪亮女郎", "ButtonImagePath": "XG03.jpg", "LapisId": "202" ,"typeid":"2"},
            { "DisplayName": "金鱼", "ButtonImagePath": "XG04.jpg", "LapisId": "203" ,"typeid":"2"},
            { "DisplayName": "中国新年", "ButtonImagePath": "XG05.jpg", "LapisId": "204" ,"typeid":"2"},
            { "DisplayName": "海盗王", "ButtonImagePath": "XG06.jpg", "LapisId": "205" ,"typeid":"4"},
            { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" ,"typeid":"4"},
            { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" ,"typeid":"4"}
        ];