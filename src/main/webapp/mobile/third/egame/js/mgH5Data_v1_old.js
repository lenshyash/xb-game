var mgdata = [{
	"DisplayName": "香蕉奥德赛",
	"DisplayEnName": "Banana Odyssey",
	"ButtonImagePath": "Banana_Odyssey_ZH",
	"LapisId": "SMG_bananaOdyssey",
	"typeid":"8"
},{
	"DisplayName": "再抢银行再旋转",
	"DisplayEnName": "Break da Bank Again Respin",
	"ButtonImagePath": "BreakDaBankAgainRespin",
	"LapisId": "SMG_breakDaBankAgainRespin",
	"typeid":"8"
},{
	"DisplayName": "奔月传说",
	"DisplayEnName": "Legend of the Moon Lovers",
	"ButtonImagePath": "LegendOfTheMoonLovers_Button_ZH",
	"LapisId": "SMG_LegendOftheMoonLovers",
	"typeid":"8"
},{
	"DisplayName": "帽中的兔子",
	"DisplayEnName": "Rabbit In The Hat",
	"ButtonImagePath": "RabbitInTheHat_GameButton",
	"LapisId": "SMG_rabbitinthehat",
	"typeid":"8"
},{
	"DisplayName": "魔力撒哈拉",
	"DisplayEnName": "Magic of Sahara",
	"ButtonImagePath": "MagicOfSahara_Button_Rollover_zh",
	"LapisId": "SMG_magicOfSahara",
	"typeid":"8"
},{
	"DisplayName": "伦敦的夏洛克",
	"DisplayEnName": "Sherlock Of London Online Slot",
	"ButtonImagePath": "Sherlock_of_London_ZH",
	"LapisId": "SMG_sherlockOfLondonOnlineSlot",
	"typeid":"8"
},{
	"DisplayName": "蒂基维京",
	"DisplayEnName": "Tiki Vikings",
	"ButtonImagePath": "TikiVikings_BTN_Rollover_ZH",
	"LapisId": "SMG_tikiVikings",
	"typeid":"8"
},{
	"DisplayName": "海底宝城",
	"DisplayEnName": "Treasures of Lion City",
	"ButtonImagePath": "TreasuresOfLionCity_BTN_Rollover_zh",
	"LapisId": "SMG_treasuresOfLionCity",
	"typeid":"8"
},{
	"DisplayName": "水晶射线女王",
	"DisplayEnName": "Queen of the Crystal Rays",
	"ButtonImagePath": "QueenOfTheCrystalRays_BTN_Rollover",
	"LapisId": "SMG_queenOfTheCrystalRays",
	"typeid":"8"
},{
	"DisplayName": "狂野宠物",
	"DisplayEnName": "Pets Go Wild",
	"ButtonImagePath": "PetsGoWild_BTN_Rollover",
	"LapisId": "SMG_petsGoWild",
	"typeid":"8"
},{
	"DisplayName": "型男舞步",
	"DisplayEnName": "Village People Macho Moves",
	"ButtonImagePath": "VillagePeople_BTN_Rollover_ZH",
	"LapisId": "SMG_villagePeople",
	"typeid":"8"
},{
		"DisplayName": "篮球巨星豪华版",
		"DisplayEnName": "Basketball Star Deluxe",
		"ButtonImagePath": "BasketballStar-Deluxe_BTN_Rollover_zh",
		"LapisId": "SMG_basketballStarDeluxe",
		"typeid":"8"
	},{
		"DisplayName": "神龙碎片",
		"DisplayEnName": "Dragon Shard",
		"ButtonImagePath": "DragonShard_BTN_Rollover_ZH",
		"LapisId": "SMG_dragonShard",
		"typeid":"8"
	},{
		"DisplayName": "Zeus古代财富",
		"DisplayEnName": "Ancient Fortunes: Zeus",
		"ButtonImagePath": "AncientFortunesZeus_BTN_Rollover_ZH",
		"LapisId": "SMG_ancientFortunesZeus",
		"typeid":"8"
	},{
		"DisplayName": "罗拉卡芙特之神庙古墓",
		"DisplayEnName": "Lara Croft: Temples and Tombs",
		"ButtonImagePath": "LaraCroftTemplesAndTombs_BTN_Rollover_ZH",
		"LapisId": "SMG_laraCroftTemplesAndTombs",
		"typeid":"8"
	},{
		"DisplayName": "探陵人",
		"DisplayEnName": "Relic Seekers",
		"ButtonImagePath": "RelicSeekers_BTN_Rollover_zh",
		"LapisId": "SMG_relicSeekers",
		"typeid":"8"
	},{
		"DisplayName": "银色雌狮4x",
		"DisplayEnName": "Silver Lioness 4x",
		"ButtonImagePath": "SilverLioness4x_Button",
		"LapisId": "SMG_silverLioness4x",
		"typeid":"8"
	},{
		"DisplayName": "特工简.布隆德归来",
		"DisplayEnName": "Agent Jane Blonde Returns",
		"ButtonImagePath": "AJBR_BTN_Rollover_ZH",
		"LapisId": "SMG_agentjaneblondereturns",
		"typeid":"8"
	},{
		"DisplayName": "好运经纪人",
		"DisplayEnName": "Bookie of Odds",
		"ButtonImagePath": "BookieOfOdds_BTN_Rollover_ZH",
		"LapisId": "SMG_bookieOfOdds",
		"typeid":"8"
	},{
		"DisplayName": "时界门之将军",
		"DisplayEnName": "Shogun of Time",
		"ButtonImagePath": "ShogunOfTime_BTN_Rollover_ZH",
		"LapisId": "SMG_shogunofTime",
		"typeid":"8"
	},{
		"DisplayName": "伟大魔术师",
		"DisplayEnName": "The Great Albini",
		"ButtonImagePath": "TheGreatAlbini_BTN_Rollover_ZH",
		"LapisId": "SMG_theGreatAlbini",
		"typeid":"8"
	},{
		"DisplayName": "丧尸来袭",
		"DisplayEnName": "Zombie Hoard",
		"ButtonImagePath": "ZombieHoard_Button_Rollover_ZH",
		"LapisId": "SMG_zombieHoard",
		"typeid":"8"
	},{
		"DisplayName": "开心娃娃机",
		"DisplayEnName": "Happy Monster Claw",
		"ButtonImagePath": "HappyMonsterClaw_BTN_Rollover_ZH",
		"LapisId": "SMG_happyMonsterClaw",
		"typeid":"8"
	},{
	    "DisplayName": "囧囧熊猫",
	    "DisplayEnName": "Wacky Panda",
	    "ButtonImagePath": "BTN_WackyPanda_ZH",
	    "LapisId": "SMG_wackyPanda",
	    "typeid":"8"
	},{
	    "DisplayName": "篮球巨星",
	    "DisplayEnName": "Basketball Star",
	    "ButtonImagePath": "BTN_BasketballStar_ZH",
	    "LapisId": "SMG_basketballStar",
	    "typeid":"8"
	}, {
	    "DisplayName": "猫头鹰乐园",
	    "DisplayEnName": "What A Hoot",
	    "ButtonImagePath": "BTN_WhataHoot3",
	    "LapisId": "SMG_whatAHoot",
	    "typeid":"8"
	},{
	    "DisplayName": "东方珍兽",
	    "DisplayEnName": "Wild Orient",
	    "ButtonImagePath": "BTN_WildOrient_Button_ZH",
	    "LapisId": "SMG_wildOrient",
	    "typeid":"8"
	},{
	    "DisplayName": "歌剧魅影",
	    "DisplayEnName": "The Phantom Of The Opera",
	    "ButtonImagePath": "BTN_ThePhantomOfTheOpera_ZH",
	    "LapisId": "SMG_thePhantomOfTheOpera",
	    "typeid":"8"
	}, {
	    "DisplayName": "热力四射",
	    "DisplayEnName": "The Heat Is On",
	    "ButtonImagePath": "BTN_TheHeatIsOn",
	    "LapisId": "SMG_theHeatIsOn",
	    "typeid":"8"
	}, {
	    "DisplayName": "妹妹很饿",
	    "DisplayEnName": "Tasty Street",
	    "ButtonImagePath": "BTN_TastyStreet_ZH",
	    "LapisId": "SMG_tastyStreet",
	    "typeid":"8"
	},  {
	    "DisplayName": "泰山",
	    "DisplayEnName": "Tarzan",
	    "ButtonImagePath": "Tarzan_Button_en",
	    "LapisId": "SMG_tarzan",
	    "typeid":"8"
	},{
	    "DisplayName": "糖果巡游",
	    "DisplayEnName": "Sugar Parade",
	    "ButtonImagePath": "BTN_SugarParade_ZH",
	    "LapisId": "SMG_sugarParade",
	    "typeid":"8"
	}, {
	    "DisplayName": "星尘",
	    "DisplayEnName": "Stardust",
	    "ButtonImagePath": "BTN_StarDust_ZH",
	    "LapisId": "SMG_stardust",
	    "typeid":"8"
	}, {
	    "DisplayName": "杂技群英会",
	    "DisplayEnName": "Six Acrobats",
	    "ButtonImagePath": "BTN_SixAcrobats_ZH",
	    "LapisId": "SMG_sixAcrobats",
	    "typeid":"8"
	}, {
	    "DisplayName": "上海美人",
	    "DisplayEnName": "Shanghai Beauty",
	    "ButtonImagePath": "BTN_ShanghaiBeauty",
	    "LapisId": "SMG_shanghaiBeauty",
	    "typeid":"8"
	}, {
	    "DisplayName": "秘密爱慕者",
	    "DisplayEnName": "Secret Romance",
	    "ButtonImagePath": "BTN_SecretRomance_1_ZH",
	    "LapisId": "SMG_secretRomance",
	    "typeid":"8"
	}, {
	    "DisplayName": "押韵的卷轴-心挞",
	    "DisplayEnName": "Rhyming Reels - Hearts & Tarts",
	    "ButtonImagePath": "BTN_RRHearts_Tarts1",
	    "LapisId": "SMG_retroReelsExtremeHeat",
	    "typeid":"8"
	}, {
	    "DisplayName": "进击的猿人",
	    "DisplayEnName": "Poke The Guy",
	    "ButtonImagePath": "BTN_PokeTheGuy",
	    "LapisId": "SMG_pokeTheGuy",
	    "typeid":"8"
	}, {
	    "DisplayName": "钻石帝国",
	    "DisplayEnName": "Diamond Empire",
	    "ButtonImagePath": "BTN_DiamondEmpire_ZH",
	    "LapisId": "SMG_diamondEmpire",
	    "typeid":"8"
	}, {
	    "DisplayName": "梦幻邂逅",
	    "DisplayEnName": "Dream Date",
	    "ButtonImagePath": "BTN_DreamDate_ZH",
	    "LapisId": "SMG_dreamDate",
	    "typeid":"8"
	}, {
	    "DisplayName": "108好汉乘数财富",
	    "DisplayEnName": "108 Heroes Multiplier Fortunes",
	    "ButtonImagePath": "BTN_108HeroesMF_ZH",
	    "LapisId": "SMG_108heroesMultiplierFortunes",
	    "typeid":"8"
	}, {
	    "DisplayName": "三國",
	    "DisplayEnName": "3 Empires",
	    "ButtonImagePath": "BTN_3Empire_ZH",
	    "LapisId": "SMG_3empires",
	    "typeid":"8"
	}, {
	    "DisplayName": "漂亮猫咪",
	    "DisplayEnName": "Pretty Kitty",
	    "ButtonImagePath": "BTN_prettykitty_zh",
	    "LapisId": "SMG_prettyKitty",
	    "typeid":"8"
	}, {
	    "DisplayName": "猴子基诺",
	    "DisplayEnName": "Monkey Keno",
	    "ButtonImagePath": "BTN_MonkeyKeno_ZH",
	    "LapisId": "SMG_MonkeyKeno",
	    "typeid":"8"
	},  {
	    "DisplayName": "侏罗纪世界",
	    "DisplayEnName": "Jurassic World",
	    "ButtonImagePath": "BTN_JurassicWorld",
	    "LapisId": "SMG_jurassicWorld",
	    "typeid":"8"
	},{
	    "DisplayName": "纯铂",
	    "DisplayEnName": "Pure Platinum",
	    "ButtonImagePath": "BTN_PurePlatinum1",
	    "LapisId": "SMG_purePlatinum",
	    "typeid":"8"
	}, {
	    "DisplayName": "拉美西斯的财富",
	    "DisplayEnName": "Ramesses Riches",
	    "ButtonImagePath": "BTN_RamessesRiches",
	    "LapisId": "SMG_RamessesRiches",
	    "typeid":"8"
	}, {
	    "DisplayName": "花粉之国",
	    "DisplayEnName": "Pollen Party",
	    "ButtonImagePath": "BTN_PollenParty_ZH",
	    "LapisId": "SMG_pollenParty",
	    "typeid":"8"
	},{
	    "DisplayName": "黄金花花公子",
	    "DisplayEnName": "Playboy Gold",
	    "ButtonImagePath": "BTN_PlayboyGold_ZH",
	    "LapisId": "SMG_playboyGold",
	    "typeid":"8"
	},{
	    "DisplayName": "躲猫猫",
	    "DisplayEnName": "Peek-a-boo",
	    "ButtonImagePath": "BTN_PeekABoo_ZH",
	    "LapisId": "SMG_peekABoo5Reel",
	    "typeid":"8"
	},{
	    "DisplayName": "呼噜噜爱上乡下",
	    "DisplayEnName": "Oink Country Love",
	    "ButtonImagePath": "BTN_OinkCountryLove_ZH",
	    "LapisId": "SMG_oinkCountryLove",
	    "typeid":"8"
	},{
	    "DisplayName": "怪物赛车",
	    "DisplayEnName": "Monster Wheels",
	    "ButtonImagePath": 	"MonsterWheels_BTN_Rollover_ZH",
	    "LapisId": "SMG_monsterWheels",
	    "typeid":"8"
	},{
	    "DisplayName": "巨款大冲击",
	    "DisplayEnName": "Mega Money Rush",
	    "ButtonImagePath": "BTN_MegaMoneyRush_ZH",
	    "LapisId": "SMG_megaMoneyRush",
	    "typeid":"8"
	},{
	    "DisplayName": "幸运生肖",
	    "DisplayEnName": "Lucky Zodiac",
	    "ButtonImagePath": "BTN_LuckyZodiac_ZH",
	    "LapisId": "SMG_luckyZodiac",
	    "typeid":"8"
	},{
	    "DisplayName": "幸运双星",
	    "DisplayEnName": "Lucky Twins",
	    "ButtonImagePath": "BTN_luckyTwins_ZH",
	    "LapisId": "SMG_luckyTwins",
	    "typeid":"8"
	},{
	    "DisplayName": "宝贝财神",
	    "DisplayEnName": "Lucky Little Gods",
	    "ButtonImagePath": "BTN_LuckyLittleGods_ZH",
	    "LapisId": "SMG_luckyLittleGods",
	    "typeid":"8"
	}, {
	    "DisplayName": "迷失拉斯维加斯",
	    "DisplayEnName": "Lost Vegas",
	    "ButtonImagePath": "BTN_LostVegas_zh",
	    "LapisId": "SMG_lostVegas",
	    "typeid":"8"
	},{
	    "DisplayName": "富裕人生",
	    "DisplayEnName": "Life of Riches",
	    "ButtonImagePath": "BTN_LifeOfRiches_zh",
	    "LapisId": "SMG_lifeOfRiches",
	    "typeid":"8"
	},{
	    "DisplayName": "忍者法宝",
	    "DisplayEnName": "Ninja Magic",
	    "ButtonImagePath": "BTN_NinjaMagic_zh",
	    "LapisId": "SMG_ninjamagic",
	    "typeid":"8"
	}, 
	{"DisplayName":"摆脱","ButtonImagePath":"BTN_BreakAway1","LapisId":"SMG_breakAway","typeid":"2"},
    { "DisplayName": "冒险宫", "ButtonImagePath": "BTN_AdventurePalaceHD", "LapisId": "SMG_adventurePalace" ,"typeid":"2"},
    { "DisplayName": "厨神", "ButtonImagePath": "BTN_BigChef_ZH", "LapisId": "SMG_BigChef" ,"typeid":"2"},
    { "DisplayName": "爱丽娜", "ButtonImagePath": "BTN_Ariana_ZH", "LapisId": "SMG_ariana","typeid":"2"},
    { "DisplayName": "幸运小妖精", "ButtonImagePath": "BTN_LuckyLeprechaun", "LapisId": "SMG_luckyLeprechaun" ,"typeid":"2"},
    { "DisplayName": "阿瓦隆", "ButtonImagePath": "BTN_AvalonHD_ZH", "LapisId": "SMG_avalon" ,"typeid":"2"},
    { "DisplayName": "金公主", "ButtonImagePath": "BTN_GoldenPrincess", "LapisId": "SMG_goldenPrincess","typeid":"2"},
    { "DisplayName": "兔子帽子", "ButtonImagePath": "BTN_RabbitintheHat", "LapisId": "SMG_rabbitinthehat" ,"typeid":"2"},
    { "DisplayName": "持枪王者", "ButtonImagePath": "BTN_Pistoleras_ZH", "LapisId": "SMG_pistoleras" ,"typeid":"2"},
    { "DisplayName": "凯蒂卡巴拉", "ButtonImagePath": "BTN_KittyCabana_ZH", "LapisId": "SMG_KittyCabana" ,"typeid":"2"},
    { "DisplayName": "酷犬酒店", "ButtonImagePath": "BTN_HoundHotel_ZH", "LapisId": "SMG_HoundHotel" ,"typeid":"2"},
    { "DisplayName": "招财鞭炮", "ButtonImagePath": "BTN_LuckyFirecracker", "LapisId": "SMG_luckyfirecracker" ,"typeid":"2"},
    { "DisplayName": "黄金时代", "ButtonImagePath": "BTN_GoldenEra", "LapisId": "SMG_goldenEra" ,"typeid":"2"},
    { "DisplayName": "怪兽总动员", "ButtonImagePath": "BTN_somanymonsters", "LapisId": "SMG_soManyMonsters" ,"typeid":"1"},
    { "DisplayName": "糖果屋", "ButtonImagePath": "BTN_somuchcandy", "LapisId": "SMG_soMuchCandy" ,"typeid":"5"},
    { "DisplayName": "寿司大餐", "ButtonImagePath": "BTN_somuchsushi", "LapisId": "SMG_soMuchSushi" ,"typeid":"5"},
    { "DisplayName": "海底狂欢", "ButtonImagePath": "BTN_FishParty1", "LapisId": "SMG_fishParty" ,"typeid":"2"},
    { "DisplayName": "蛮荒野狼", "ButtonImagePath": "BTN_CoolWolf1", "LapisId": "SMG_coolWolf","typeid":"2" },
    { "DisplayName": "足球明星", "ButtonImagePath": "BTN_footballstar1", "LapisId": "SMG_footballStar" ,"typeid":"2" },
    { "DisplayName": "上流社会", "ButtonImagePath": "BTN_HighSociety", "LapisId": "SMG_highSociety" ,"typeid":"2"},
    { "DisplayName": "星光之吻", "ButtonImagePath": "BTN_StarlightKiss", "LapisId": "SMG_starlightKiss","typeid":"2"},
    { "DisplayName": "神秘格罗夫", "ButtonImagePath": "BTN_MysticDreams1", "LapisId": "SMG_mysticDreams","typeid":"3" },
    { "DisplayName": "海豚探秘", "ButtonImagePath": "BTN_DolphinQuest", "LapisId": "SMG_dolphinQuest" ,"typeid":"2"},
    { "DisplayName": "胸围银行", "ButtonImagePath": "BTN_BustTheBank1", "LapisId": "SMG_bustTheBank" ,"typeid":"2"},
    {"DisplayName":"幸运锦锂","ButtonImagePath":"BTN_LuckyKoi","LapisId":"SMG_luckyKoi","typeid":"2"},
    {"DisplayName":"花花公子","ButtonImagePath":"BTN_Playboy1","LapisId":"SMG_playboy","typeid":"2"},
    {"DisplayName":"人生经典","ButtonImagePath":"BTN_FinerReelsofLife","LapisId":"SMG_theFinerReelsOfLife","typeid":"1"},
    {"DisplayName":"猫头鹰","ButtonImagePath":"BTN_WhataHoot3","LapisId":"SMG_whatAHoot","typeid":"2"},
    {"DisplayName":"心跳时刻","ButtonImagePath":"BTN_VinylCountdown4","LapisId":"SMG_vinylCountdown","typeid":"2"},
    {"DisplayName":"野性大熊猫","ButtonImagePath":"BTN_UntamedGiantPanda","LapisId":"SMG_untamedGiantPanda","typeid":"5"},
    {"DisplayName":"古墓丽影2","ButtonImagePath":"BTN_TombRaiderSotS1","LapisId":"SMG_RubyTombRaiderII","typeid":"2"},
    {"DisplayName":"古墓丽影","ButtonImagePath":"BTN_TombRaider2","LapisId":"SMG_tombRaider","typeid":"2"},
    {"DisplayName":"雷神索尔II","ButtonImagePath":"BTN_ThunderstruckTwo1","LapisId":"SMG_thunderstruck2","typeid":"2"},
    {"DisplayName":"英伦时光","ButtonImagePath":"BTN_TallyHo1","LapisId":"SMG_tallyHo","typeid":"2"},
    { "DisplayName": "太阳探密", "ButtonImagePath": "BTN_SunQuest3", "LapisId": "SMG_sunQuest","typeid":"2"  },
    { "DisplayName": "泰坦神庙", "ButtonImagePath": "BTN_StashoftheTitans1", "LapisId": "SMG_stashOfTheTitans" ,"typeid":"2"},
    { "DisplayName": "海滨嘉年华", "ButtonImagePath": "BTN_SpringBreak2", "LapisId": "SMG_springBreak" ,"typeid":"2"},
    { "DisplayName": "银坊", "ButtonImagePath": "BTN_SilverFang1", "LapisId": "SMG_silverFang" ,"typeid":"2"},
    { "DisplayName": "发射！", "ButtonImagePath": "BTN_Shoot", "LapisId": "SMG_shoot" ,"typeid":"2"},
    { "DisplayName": "暗恋", "ButtonImagePath": "BTN_SecretAdmirer1", "LapisId": "SMG_secretAdmirer","typeid":"2" },
    { "DisplayName": "暴走圣诞", "ButtonImagePath": "BTN_SantasWildRide1", "LapisId": "SMG_santasWildRide" ,"typeid":"2"},
    { "DisplayName": "复古钻石老虎机", "ButtonImagePath": "BTN_RRDiamondGlitz1", "LapisId": "SMG_retroReelsDiamondGlitz" ,"typeid":"2"},
    { "DisplayName": "火热老虎机", "ButtonImagePath": "BTN_RRExtreme1", "LapisId": "SMG_retroReelsExtremeHeat" ,"typeid":"2"},
    { "DisplayName": "复古老虎机", "ButtonImagePath": "BTN_RetroReels1", "LapisId": "SMG_retroReels" ,"typeid":"2"},
    { "DisplayName": "速食轮动", "ButtonImagePath": "BTN_ReelThunder2", "LapisId": "SMG_reelThunder" ,"typeid":"2"},
    { "DisplayName": "海上驱动", "ButtonImagePath": "BTN_ReelStrike1", "LapisId": "SMG_reelStrike" ,"typeid":"2"},
    { "DisplayName": "宝石连线", "ButtonImagePath": "BTN_ReelGems1", "LapisId": "SMG_reelGems" ,"typeid":"2"},
    { "DisplayName": "百万美人鱼", "ButtonImagePath": "BTN_MermaidsMillions1", "LapisId": "SMG_mermaidsMillions" ,"typeid":"2"},
    { "DisplayName": "马雅公主", "ButtonImagePath": "BTN_MayanPrincess1", "LapisId": "SMG_mayanPrincess","typeid":"2"},
    { "DisplayName": "疯狂帽商", "ButtonImagePath": "BTN_MadHatters1", "LapisId": "SMG_madHatters" ,"typeid":"2"},
    { "DisplayName": "嘻哈摇滚", "ButtonImagePath": "BTN_Loaded1", "LapisId": "SMG_loaded" ,"typeid":"2"},
    { "DisplayName": "红衣女郎", "ButtonImagePath": "BTN_LadyInRed2", "LapisId": "SMG_ladyInRed" ,"typeid":"2"},
    { "DisplayName": "淑女之夜", "ButtonImagePath": "BTN_LadiesNite5", "LapisId": "SMG_ladiesNite" ,"typeid":"2"},
    { "DisplayName": "国王现金", "ButtonImagePath": "BTN_KingsofCash1", "LapisId": "SMG_kingsOfCash","typeid":"2" },
    { "DisplayName": "浪漫传奇", "ButtonImagePath": "BTN_ImmortalRomance1", "LapisId": "SMG_immortalRomance" ,"typeid":"2"},
    { "DisplayName": "黄金鼠", "ButtonImagePath": "BTN_GopherGold2", "LapisId": "SMG_gopherGold" ,"typeid":"2"},
    { "DisplayName": "双钻宝", "ButtonImagePath": "BTN_DoubleWammy1", "LapisId": "SMG_doubleWammy" ,"typeid":"1"},
    { "DisplayName": "双威王牌", "ButtonImagePath": "BTN_DoubleDoubleBonus1", "LapisId": "SMG_doubleDoubleBonus" ,"typeid":"6"},
    { "DisplayName": "疯狂变色龙", "ButtonImagePath": "BTN_CrazyChameleons1", "LapisId": "SMG_crazyChameleons","typeid":"2" },
    { "DisplayName": "慵懒土豆", "ButtonImagePath": "BTN_CouchPotato2", "LapisId": "SMG_couchPotato" ,"typeid":"1"},
    { "DisplayName": "土豪金", "ButtonImagePath": "BTN_Cashville1", "LapisId": "SMG_cashville" ,"typeid":"2"},
    { "DisplayName": "疯狂现金", "ButtonImagePath": "BTN_CashCrazy9", "LapisId": "SMG_cashCrazy" ,"typeid":"1"},
    { "DisplayName": "狂欢节", "ButtonImagePath": "BTN_Carnaval2", "LapisId": "SMG_carnaval" ,"typeid":"2"},
    { "DisplayName": "布希电报", "ButtonImagePath": "BTN_BushTelegraph1", "LapisId": "SMG_bushTelegraph","typeid":"2" },
    { "DisplayName": "银行破坏家", "ButtonImagePath": "BTN_BreakDaBank1", "LapisId": "SMG_breakDaBank" ,"typeid":"1"},
    { "DisplayName": "马戏团", "ButtonImagePath": "BTN_BigTop1", "LapisId": "SMG_bigTop"  ,"typeid":"3"},
    { "DisplayName": "水果丛林", "ButtonImagePath": "BTN_BigKahuna1", "LapisId": "SMG_bigKahuna","typeid":"2" },
    { "DisplayName": "东方美人", "ButtonImagePath": "BTN_AsianBeauty1", "LapisId": "SMG_asianBeauty" ,"typeid":"2"},
    { "DisplayName": "美式轮盘", "ButtonImagePath": "BTN_USRoulette1", "LapisId": "SMG_americanRouletteGold" ,"typeid":"5"},
    { "DisplayName": "阿拉斯加冰钓", "ButtonImagePath": "BTN_AlaskanFishing1", "LapisId": "SMG_alaskanFishing" ,"typeid":"2"},
    { "DisplayName": "狂野马戏团", "ButtonImagePath": "BTN_TwistedCircus", "LapisId": "SMG_theTwistedCircus" ,"typeid":"1"}
 ];