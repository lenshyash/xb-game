<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http:/www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="Cache-Control" content="max-age=0">
<meta name="language" content="zh-CN">
<meta name="MobileOptimized" content="240">
<title>电子游艺</title>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width,maximum-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="screen-orientation" content="portrait">
<meta name="x5-fullscreen" content="true">
<meta name="x5-orientation" content="portrait">
<link href="${base}/mobile/third/mg/css/tt.css" rel="stylesheet">
<link href="${base}/mobile/third/mg/css/index.css" rel="stylesheet">
<style>
.game_logo {
	position: relative;
	display: inline-block;
	width: 120px;
	height: 140px;
	overflow: hidden;
	cursor: pointer;
}

.game_logo img {
	position: absolute;
	left: 10px;
	width: 100px;
	height: 100px;
}

/* .game_logo img:hover {
	left: -120px;
} */

.game_logo1 {
	position: relative;
	display: inline-block;
	width: 120px;
	height: 140px;
	overflow: hidden;
	cursor: pointer;
}

.game_logo1 img {
	position: absolute;
	left: 0;
	height: 116px;
	width: 100%;
}
</style>
</head>
<body>
<script>
var isLocaApp = false,NewiOS = false;
try{isLocaApp = android.isAndroidApp()}catch(err){}
var picCallback = function(photos) {
	try{
		isLocaApp = photos
		NewiOS = photos
	}catch(err){}
}
function iosAndroidDz(is) {
    var browser = {
            versions: function() {
                var a = navigator.userAgent,
                    b = navigator.appVersion;
                return {
                    trident: a.indexOf("Trident") > -1,
                    presto: a.indexOf("Presto") > -1,
                    webKit: a.indexOf("AppleWebKit") > -1,
                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                    iPhone: a.indexOf("iPhone") > -1,
                    iPad: a.indexOf("iPad") > -1,
                    webApp: a.indexOf("Safari") == -1
                }
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
        };
	 if (browser.versions.android) {
         //Android
         if(!isLocaApp){
        	 location.href="${base }/mobile/index.do"
         }else{     
           	 	android.backPage('back');
         }
     } else if (browser.versions.ios) {
         //ios
    	 if(!isLocaApp){
    		 location.href="${base }/mobile/index.do"
         }else{
        	 WTK.share(JSON.stringify({'method':'back'}));	 
         }
     }else{
    	 location.href="${base }/mobile/index.do"
     }
}
</script>
		<div class="head">
			<a href="javascript:(0)" onclick="iosAndroidDz()" class="ico ico_home"></a>
			<h1>电子游戏</h1>
		</div>
	<div class="app">

		<div class="app-body">
			<div class="app-content">
				<div class="scrollable-index">
					<div class="scrollable-con">
						<div class="game-content" id="gamelist_div"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/mobile/chatMobileModule.jsp" ></jsp:include>
</c:if>
<style>
	#openChat{
		width:40px!important;
		top:2px!important;
	}
</style>
<script>
	$("#openChat img").attr('src','${base}/common/template/member/center/img/1516676959037063.png')
</script>
<!--  
<script src="${base }/common/template/third/newEgame/js/egameData_v1.js"></script>
-->
<script src="${base}/common/template/third/newEgame/js/egameData_${egameVersion}.js?v=13"></script>
<script src="${base }/mobile/third/egame/js/mgH5Data_${egameVersion}.js?v=5"></script>
<script src="${base }/mobile/third/egame/js/ptH5Data.js?v=5"></script>
<script src="${base }/mobile/third/egame/js/cq9H5Data.js"></script>
<script src="${base }/mobile/third/egame/js/ttgH5Data.js"></script>
<script src="${base }/mobile/third/egame/js/mwH5Data.js"></script>
<script src="${base }/mobile/third/egame/js/agH5Data.js?v=2"></script>
<script src="${base }/mobile/third/egame/js/isbH5Data.js?v=3"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script>
	var code = '${code}';
	var base = '${base}';
</script>
<script src="${base}/common/js/layer3/layer.min.js"></script>
<script src="${base }/mobile/v3/page/third/egame/js/egame.js?v=14"></script>
<script id="gamelist_tpl" type="text/html">
	{{each data as game}}
		<div class="game-item">
			<div class="game_logo{{game.single }}"
				{{if type == 'by'}}
					onclick="toBY('{{game.LapisId}}')"
				{{/if}} 
				{{if type == 'ag'}}
					onclick="toAG('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'mg'}}
					onclick="toMG('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'pt'}}
					onclick="toPT('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'qt'}}
					onclick="toQT('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'cq9'}}
					onclick="toCQ9('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'jdb'}}
					onclick="toJDB('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'ttg'}}
					onclick="toTTG('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'mw'}}
					onclick="toMW('{{game.LapisId}}')"
				{{/if}}
				{{if type == 'isb'}}
					onclick="toISB('{{game.LapisId}}')"
				{{/if}}
				>
				{{if ismg}}
					{{if game.ButtonImagePath == 'BTN_3Empire_ZH.jpg' | game.ButtonImagePath == 'BTN_108Empire_ZH.jpg' | game.ButtonImagePath == 'BTN_CricketStar.jpg'}}
						<img src="${base}/{{path}}/{{game.ButtonImagePath}}" style="width:220px;">
					{{else if game.LapisId == '28682' | game.LapisId == '28688' | game.LapisId == '28704' | game.LapisId == '28804' | game.LapisId == '28936' | game.LapisId == '28946' | game.LapisId == '29136' | game.LapisId == '29050' | game.LapisId == '29453' | game.LapisId == '29118'}}
						<img src="${base}/{{path}}/{{game.ButtonImagePath}}.png" >
					{{else}}
						<img src="${base}/{{path}}/{{game.ButtonImagePath}}.png" style="width:220px;">
					{{/if}}
					
				{{else}}
					<img src="${base}/{{path}}/{{game.ButtonImagePath}}" >
				{{/if}}
			</div>
			<p>{{game.DisplayName}}</p>
		</div>
	{{/each}}
</script>