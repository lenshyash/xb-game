﻿<%@ page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<% 
String backUrl = request.getParameter("backUrl");

pageContext.setAttribute("backUrl", backUrl);

Long stationId = StationUtil.getStation().getId();

pageContext.setAttribute("lottery_order_cancle_switch", StationConfigUtil.get(stationId, StationConfig.lottery_order_cancle_switch));
 %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="script/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 10px 16px;
    margin-left: -1px;
    line-height: 1.3333333;
    color: #cd0005;
    text-decoration: none;
    background-color: #ddd;
    border: 1px solid #fff;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #cd0005;
    border-color: #cd0005;
}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 3;
    color: #cd0005;
    background-color: #eee;
    border-color: #ddd;
}
</style>
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<!-- personal_center.do -->
				<c:if test="backUrl"></c:if>
				<c:choose>
					<c:when test="${empty backUrl }">
						<a href="javascript: history.go(-1);" style="color: #fff;"><em class="iconfont icon-left"></em></a>
					</c:when>
					<c:otherwise>
						<a href="${backUrl }" style="color: #fff;"><em class="iconfont icon-left"></em></a>
					</c:otherwise>
				</c:choose>
				<span class="vbktl">彩票投注记录</span>
			</div>
			<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<!-- id=innerbox 标签外面 不放任何标签 为自适应标签 -->
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div id="selectQueryType" class="touzhuqie" data-current="all">
				<span data-querytype="all" class="cur">全部记录 </span> 
				<span data-querytype="init" class="">未开奖 </span> 
				<span data-querytype="win" class="">中奖纪录 </span> 
				<span data-querytype="lost" class="">未中奖 </span> 
				<!-- <span data-querytype="canel" class="">撤单 </span> --> 
				<c:if test="${version == 1 || version == 3}">
					<span data-querytype="follow" class="">追号管理</span>
				</c:if>
			</div>
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent" data-value="today">
						<div class="fangsjbox">今天</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="today">今天</li>
								<li data-value="yesterday">昨天</li>
								<li data-value="week">近一周</li>
								<li data-value="month">近30天</li>
								<li data-value="monthbefore">30天以前</li>
								<!-- 
								<li data-value="60before">60天以前</li>
								<li data-value="90before">90天以前</li>
								 -->
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryLotCode" class="touzhusj selectComponent" data-value="">
						<div class="fangsjbox">彩种筛选</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul id="lotterySelUl">
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div class="caizhongtable" id="recordContent">
			</div>
			<nav id="pageview">
			</nav>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
	<div id="ycf-alert" class="modal">
		<div class="modal-dialog modal-sm" style="width: 250px; height: 108px;margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0;">
			<div class="modal-content">
				<div class="modal-body bg-info" style="border-radius: 5px; padding: 10px;">
					<p style="font-size: 15px; font-weight: bold;">[Message]</p>
				</div>
				<div class="modal-footer" style="padding: 5px; text-align: center;">
					<button type="button" class="btn btn-primary ok" data-dismiss="modal">[BtnOk]</button>
					<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/bootstrap/js/bootstrap_alert.js"></script>
<script type="text/javascript" src="script/$this/betting_record_lottery.js?v=1.1.2"></script>
</html>
<script type="text/html" id="recoredNewTemplate" style="display: none;">
				<table>
					<tr class="caizhongtabletr">
						<td>彩种</td>
						<td>投注时间</td>
						<td>投注金额</td>
						<td>派奖金额</td>
						<td>操作</td>
						<td>当前状态</td>
					</tr>
					<tbody>
					{# 
						_.map(data, function(item){
						if(!item.winMoney){
							item.winMoney = '-';
						}else{
							item.winMoney = item.winMoney +'元';
						}
						var statusTitle = "其他";
						switch(item.status){
						case 1:
							statusTitle = "未开奖";
							break;
						case 2:
							statusTitle = "已派奖";
							break;
						case 3:
							statusTitle = "未中奖";
							break;
						case 4:
							statusTitle = "撤单";
							break;
						case 5:
							statusTitle = "派奖回滚成功";
							break;
						case 6:
							statusTitle = "回滚异常的";
							break;
						case 7:
							statusTitle = "开奖异常";
							break;
						case 8:
							statusTitle = "和局";
							break;
						}
					 #}
					<tr class="" data-orderid="{{item.orderId}}" data-lotcode="{{item.lotCode}}">
						<td>{{item.lotName}}</td>
						<td>{{new Date(item.createTime).format()}}</td>
						<td>{{item.buyMoney}}元</td>
						<td>{{item.winMoney}}</td>
						<td><a href="betting_record_lottery_details.do?lotCode={{item.lotCode}}&orderId={{item.orderId}}" class="lanx">查看</a>
							<c:if test="${lottery_order_cancle_switch ne 'off' }">
							{# if(item.status == 1 && item.lotCode != 'LHC') {#} <a href="javascript: void(0);" class="lanx" name="chedan">撤单</a>{#  } #}
							</c:if>
						</td>
						<td>{{statusTitle}}</td>
					</tr>
					{#
						});
					#}
					<tr class="">
						<td colspan="2" style="text-align:right;">小计:</td>
						<td id="xBet">0元</td>
						<td id="xWin">0元</td>
						<td colspan="2"></td>
					</tr>
					<tr class="">
						<td colspan="2" style="text-align:right;">总计:</td>
						<td id="sumBet">0元</td>
						<td id="sumWin">0元</td>
						<td colspan="2"></td>
					</tr>
					<!-- 
					<tr class="">
						<td>重庆时时彩</td>
						<td>16-05-09 12:12</td>
						<td>10.00元</td>
						<td>中奖<em class="hong">26.00</em>元
						</td>
						<td><a href="betting_record_lottery_details.do" class="lanx">查看详情</a>
							<a href="javascript: void(0);" class="lanx">继续投注</a></td>
					</tr>
					 -->
					</tbody>
				</table>
</script>
<script type="text/html" id="pageviewTemplate" style="display: none;">
				{#
				if(data.totalPageCount > 1){
				#}
				<ul class="pagination pagination-lg pull-left">
					<li>
						<a href="javascript: void(0);" style="height:7rem;width:7rem;" aria-label="Previous" pageno="{{data.prePage}}">
							<span aria-hidden="true" style="line-height:5rem;margin-left:1rem;font-size:5rem;">&laquo;</span>
						</a>
					</li>
				</ul>
					<!-- 
					<li class="active"><a href="javascript: void(0);">1</a></li>
					<li><a href="javascript: void(0);">2</a></li>
					<li><a href="javascript: void(0);">3</a></li>
					<li><a href="javascript: void(0);">4</a></li>
					<li><a href="javascript: void(0);">5</a></li>
					 -->
			<ul class="pagination pagination-lg pull-right">
					<li>
						<a href="javascript: void(0);" style="height:7rem;width:7rem;" aria-label="Next" pageno="{{data.nextPage}}">
							<span aria-hidden="true" style="line-height:5rem;margin-left:1rem;font-size:5rem;">&raquo;</span>
						</a>
					</li>
				</ul>
				{#
				}
				#}
</script>
<%@include file="include/check_login.jsp" %>