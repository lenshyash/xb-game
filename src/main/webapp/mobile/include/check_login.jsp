﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="p" uri="/permission"%>
<%
%>
		<p:login login="false">
				<link rel="stylesheet" rev="stylesheet" href="${station }/script/bootstrap/css/bootstrap.min.css" type="text/css" />
				<style type="text/css">
					.linecentermodel {
						margin: auto;
						position: absolute;
						top: 0;
						left: 0;
						bottom: 0;
						right: 0;
					}
				</style>
				<script type="text/javascript" src="${station }/script/jquery.cookie.js"></script>
				<script type="text/javascript" src="${station }/script/bootstrap/js/bootstrap.min.js"></script>
				<script type="text/javascript" src="${station }/script/bootstrap/js/bootstrap_alert.js"></script>
				<script>
					// alert("请先登录！");
					// location.href = "${station}/login.html";
					/*
					$.dialog({
						title: "系统讯息",
						content: "请先登录！",
						ok: function () {
							location.href = "login.html";
						},
						canle: function () {
							location.href = "login.html";
						}
					});
					*/
					$(function(){
						var mobile_alk_chk = $.cookie('mobile_alk_chk');
						if(mobile_alk_chk != null && mobile_alk_chk){
							$.ajax({
								type : "POST",
								url : "${station}/alklogin.do",
								data : {
									_alk_chk: mobile_alk_chk
								},
								success : function(result) {
									if(result.code == 400){
										Modal.alert({
											msg : result.message
										}).on(function(e) {
											location.href = "${station}/login.do";
											return;
										});
									} else if(result.code == 200){
										location.reload();
									}
								}
							});
						} else {
							/**
							Modal.alert({
								msg : "请先登录！"
							}).on(function(e) {
								location.href = "${station}/login.do";
								return;
							});
							**/
						}
					});
				</script>
				<div id="ycf-alert" class="modal">
					<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
						<div class="modal-content">
							<div class="modal-body bg-danger" style="border-radius: 5px; padding: 10px;">
								<p style="font-size: 15px; font-weight: bold;">[Message]</p>
							</div>
							<div class="modal-footer" style="padding: 5px; text-align: center;">
								<button type="button" class="btn btn-warning ok" data-dismiss="modal">[BtnOk]</button>
								<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>
							</div>
						</div>
					</div>
				</div>
		</p:login>