﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
</head>
<body >
<div class="top">
	<div class="inner">
		
		<div class="back">
			<!-- <a href="betting_record_lottery.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> -->
			<a href="javascript: history.go(-1);" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">帐变详情</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="recordDetailsContent" id="innerbox">
<div class="shangqkaij tac">
		<span class="tac co000">订单号：<em class="hong">${m.orderId }</em> </span>
		<div class="cl"></div>
	</div>
	<div class="bgf6 ov">
		<table class="tymingxtab tymingxtabcur">
			<tr>
				<td class="">
					<div class="tymingxtabdiv">日期</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv" id="createDatetime"><fmt:formatDate value="${m.bizDatetime}" var="bizDateTime" pattern="yyyy-MM-dd hh:mm:ss" />${bizDateTime}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">会员名</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv">${m.account }</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">变动前金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv" id="beforeMoney"></div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">变动金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv" id="money" style="color:red;"></div>
				</td>
			</tr>
			<tr>
				<td class="">
					<div class="tymingxtabdiv">变动后金额</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv" id="afterMoney">${m.afterMoney}</div>
				</td>
				<td class="">
					<div class="tymingxtabdiv">交易类型</div>
				</td>
				<td class="bttd">
					<div class="tymingxtabdiv" id="mtype"></div>
				</td>
			</tr>
			<tr>
				<td class="" colspan="4">
					<div style="min-height: 200px; font-size: 22px; padding: 20px;">${m.remark }</div>
				</td>
			</tr>
		</table>
		<div class="cl h80"></div>
	</div>
</div>
</body>
<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script>
//格式化成两位小数
function fmoney(s, n) { 
	n = n > 0 && n <= 20 ? n : 2; 
	s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
	var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
	t = ""; 
	for (i = 0; i < l.length; i++) { 
	t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : ""); 
	} 
	return t.split("").reverse().join("") + "." + r; 
};

function getLocalTime(nS) {  
	var date = new Date(nS);
	var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '+ date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
   return dateFormat;     
}

$(function() {
	var bf = '${m.beforeMoney}';
	if(bf){
		$('#beforeMoney').html(fmoney(bf,2));
	}
	var m = '${m.money }';
	if(m){
		$('#money').html(fmoney(m,2));
	}
	var am = '${m.afterMoney }';
	if(am){
		$('#afterMoney').html(fmoney(am,2));
	}
	//var ct = '${m.bizDatetime }';
	//if(ct){
	//	$('#createDatetime').html(getLocalTime(ct));
	//}
	$('#mtype').html(mnType(${m.type}));
});

function mnType(p){
	var moneyType = ${moneyType};
	for(var key in moneyType)
	{
		if(p == key){
			return moneyType[key];
		}
	}
}
</script>
</html>
<%@include file="include/check_login.jsp" %>