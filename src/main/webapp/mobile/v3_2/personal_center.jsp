<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/res.jsp"/>
  </head>
  <body>
	<div class="page">
	  <jsp:include page="include/barTab.jsp"/>
	  <div class="content">
	  
	    <!-- 头部 -->
	    <div class="content personal_center_content">
			<div class="usertou">
				<div class="userminc">
					<a href="${m }/personal_info.do" class="external">
						<div class="user_logined_img"><img src="${res }/images/touxiang.png"></div>
						<span class="account">${loginMember.account}</span>
					</a>
					<a href="${m }/logout.do" class="pull-right">
						退出
						<em class="iconfont icon_loginout"></em>
					</a>
				</div>
				<div class="doudoubox">
					<em class="iconfont icon-shujuku jindouicon"></em>
					可用余额： 
					<span id="meminfoMoney">${loginMember.money}</span>
	                <c:if test="${not empty cashName}">
                		${cashName}
	                </c:if>
	                <c:if test="${empty cashName}">
	                  	元 
	                </c:if>
				</div>
			</div>
			
			<c:if test="${isGuest != true && iosExamine ne 'off' }">
				<div class="qmenu" style="display: flex;">
					<a href="${m }/pay_select_way.do" class="chongzhitkuan external" style="flex-grow:1;">
						<em class="iconfont icon-qia"></em>充值
					</a>
					<a href="${m }/withdraw_money.do" class="chongzhitkuan external" style="flex-grow:1;">
						<em class="iconfont icon-qukuanji1"></em>提款
					</a>
					<c:if test="${isZrOnOff=='on' || isDzOnOff eq 'on' || isChessOnOff eq 'on' || isThdLotOnOff eq 'on' || isTsOnOff eq 'on'}">
						<a href="${m }/live_game.do" class="chongzhitkuan external" style="flex-grow:1;">
							<em class="iconfont icon-qukuanji1"></em>额度转换
						</a>
					</c:if>
				</div>
			</c:if>
			
		    <!-- 游戏记录 -->
	    	<ul class="userlb">
	    		<c:if test="${isCpOnOff=='on'}">
					<li><a href="${m }/betting_record_lottery.do" class="external"><em class="iconfont icon-lianxijilu icontublb"></em> 彩票投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isLhcOnOff=='on'}">
					<li><a href="${m }/betting_record_sixmark.do" class="external"><em class="iconfont icon-61 icontublb"></em> 六合投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isTyOnOff=='on' && isGuest != true}">
					<li><a href="${m }/sports/hg/goOrderPage.do" class="external"><em class="iconfont icon-zuqiu1 icontublb"></em> 体育投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isZrOnOff=='on' && isGuest != true}">
					<li><a href="${m }/betting_record_live.do" class="external"><em class="iconfont icon-jilu1 icontublb"></em> 真人投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isDzOnOff=='on' && isGuest != true}">	
					<li><a href="${m }/betting_record_egame.do" class="external"><em class="iconfont icon-iconwtbappwenjian icontublb"></em> 电子游戏记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${iosExamine ne 'off' && onoffChangeMoney=='on' && isGuest != true}">	
					<li><a href="${m }/mnyrecord.do" class="external"><em class="iconfont icon-record icontublb"></em> 用户账变记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${iosExamine ne 'off'}">	
					<li><a href="${m }/account_details.do" class="external"><em class="iconfont icon-jilu icontublb"></em> 账户明细纪录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
					<li><a href="javascript:;" class="denglumima-btn"><em class="iconfont icon-21 icontublb"></em> 登陆密码修改 <em class="iconfont icon-right iconjtlb"></em></a></li>
				<c:if test="${iosExamine ne 'off' && isGuest != true }">	
					<li><a href="javascript:;" class="qukanmima-btn"><em class="iconfont icon-jilu01 icontublb"></em> 取款密码修改 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
			</ul>
	    
		    <!-- 站内信 -->
		    <ul class="userlb">
				<li><a href="${m }/website_message.do" class="external">
				<em class="iconfont icon-email icontublb"></em> 
				<span>我的站内信 
					<icon class="iconfont icon-yuanquan1" style="color: #cd0005; font-size: 0.95rem;position:relative;top:-0.1rem;"></icon>
					<span style="font-size: 0.65rem; position: relative; color: #ffffff;left:-0.95rem;top:-0.1rem;" id="zhanneixin">${indexMsgCount }</span></span>
				<em class="iconfont icon-right iconjtlb"></em></a></li>
			</ul>
			
	    </div>
	    
	  </div>
	</div>
  </body>
</html>
<c:if test="${!isLogin }">
	<script>
		$.showPreloader('请先登录');
		location.href = '${m}/login.do';
	</script>
</c:if>
<jsp:include page="include/common/need.jsp"/>