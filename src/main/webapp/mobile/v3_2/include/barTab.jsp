<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row no-gutter" style="margin-top: 2rem;margin-bottom: 2rem;"></div>

<nav class="bar bar-tab">
  <a class="tab-item external <c:if test="${navMenu eq 'index'}">active</c:if>" href="${m }">
    <span class="icon icon-home"></span>
    <span class="tab-label">投注大厅</span>
  </a>
  <c:if test="${isCpOnOff=='on'}">
	  <a class="tab-item external <c:if test="${navMenu eq 'game_center'}">active</c:if>" href="${m }/game_center.do">
	    <span class="icon icon-app"></span>
	    <span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
	  </a>
	  <a class="tab-item external <c:if test="${navMenu eq 'draw_notice'}">active</c:if>" href="${m }/draw_notice.do">
	    <span class="icon icon-gift"></span>
	    <span class="tab-label">开奖公告</span>
	  </a>
  </c:if>
  <a class="tab-item external <c:if test="${navMenu eq 'personal_center'}">active</c:if>" href="${m }/personal_center.do">
    <span class="icon icon-me"></span>
    <span class="tab-label">个人中心</span>
  </a>
</nav>

