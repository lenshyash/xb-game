<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 注意: 1.每个页面必须有这一段，class为page_style； -->
<style type="text/css" class="page_style">
  .my-btn{background: red}
</style>
<script type="text/javascript" class="page_script">
  //页面私有作用域
  var pageJS = function(){
    //相对的全局变量
    this.par1 = 0;
    this.par2 = 0;

    this.fun1 = function(){
    	$.toast("操作失败");
    };
    
    this.fun2 = function(){
    	$.toast('操作成功，正在跳转...', 2345, 'success top');
    };
  }

  //页面初始化函数，用于sui初始化后回调
  function pageJS_init(){
     window.pageJS = new pageJS();
     //---do something------
//      window.pageJS.fun1();
//      window.pageJS.fun2();
     //---------------------
  }
</script>

<script type="text/javascript">
  //兼容刷新url调用的脚本
  $(function(){
    pageJS_init();
  });
</script>