<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="${base }/images/favicon.ico">
<link rel="stylesheet" href="${res }/css/sm.css">
<link rel="stylesheet" href="${res }/css/sm-extend.css">
<link rel="stylesheet" href="${base}/mobile/v3/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${res }/css/common.css">
<%--彩票css先独立，防止冲突，开发完成后合并到common.css  --%>
<link rel="stylesheet" href="${base}/mobile/v3/css/lottery.css">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<script type="text/javascript">
M = {
	base : '${base}',
	folder : "${stationFolder}",
	version : '${version}',
}
</script>
<script type='text/javascript' src='${res }/js/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${res }/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${res }/js/sm-extend.js' charset='utf-8'></script>
<%-- 彩票js  --%>
<script type='text/javascript' src="${res}/js/lottery.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>