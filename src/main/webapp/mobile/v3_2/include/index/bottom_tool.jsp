<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row no-gutter contact mobile-device" style="display: none;">
	<dl class="ico-row-botton">
		<a class="external" href="http://www.jiathis.com/share" target="_blank">
			<dt>
				<i class="icon icon-share"></i>
			</dt>
			<dd>分享</dd>
		</a>
	</dl>
	<dl class="ico-row-botton">
		<a class="external" href="${base}/?toPC=1">
			<dt>
				<i class="icon icon-computer"></i>
			</dt>
			<dd>电脑版</dd>
		</a>
	</dl>
	<dl class="ico-row-botton index_bt_active" style="display: none;">
		<a class="external" href="${m }/active.do">
			<dt class="active">
				<i class="iconfont pb2">&#xe654;</i>
			</dt>
			<dd>优惠活动</dd>
		</a>
	</dl>
	<dl class="ico-row-botton">
		<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
			<dt>
				<i class="icon icon-refresh"></i>
			</dt>
			<dd>刷新</dd>
		</a>
	</dl>
</div>
<script>
//判断是否是苹果/安卓设备
var device = $.device;
console.log('android:'+device.android);
console.log('ios:'+device.ios);
console.log('iphone:'+device.iphone);
console.log('ipad:'+device.ipad);
var iosExamine = '${iosExamine}';
if(!device.android && !device.iphone && iosExamine != 'off'){
	$('div.mobile-device').show();
	var isActive = '${isActive }';
	if(isActive == 'true'){
		$('dl.ico-row-botton').width('33.3%');
	}else{
		$('dl.index_bt_active').show();
		$('dl.ico-row-botton').width('25%');
	}
}
</script>