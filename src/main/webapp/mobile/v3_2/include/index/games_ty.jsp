<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 体育 -->
<c:if test="${isTyOnOff=='on' && isGuest != true}">
	<c:if test="${isHgSysOnOff ne 'off'}">
	<div class="index_lottery_item">
		<a
			<c:if test="${isLogin}">href="${base }/mobile/sports/hg/index.do"</c:if>
			<c:if test="${!isLogin}">href="javascript:void(0);" onclick="location.href='${m }/login.do'"</c:if>
			class="external" title="皇冠体育">
			<div class="lottery_img">
				<img src="${res }/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">皇冠体育</div>
			</div>
		</a>
		<div class="cl"></div>
	</div>
	</c:if>
</c:if>
<c:if test="${isM8OnOff eq 'on' && isGuest != true}">
	<div class="index_lottery_item">
		<a
			<c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardM8.do?h5=1', '99', this);"</c:if>
			<c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();" </c:if>
			class="external" title="M8体育">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">M8体育</div>
			</div>
		</a>
		<div class="cl"></div>
	</div>
</c:if>
<c:if test="${isM8HOnOff eq 'on' && isGuest != true}">
	<div class="index_lottery_item">
		<a
			<c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardM8H.do?h5=1', '991', this);"</c:if>
			<c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();" </c:if>
			class="external" title="M8体育(皇冠版)">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">M8体育(皇冠版)</div>
			</div>
		</a>
		<div class="cl"></div>
	</div>
</c:if>
<c:if test="${isIbcOnOff eq 'on' && isGuest != true}">
	<div class="index_lottery_item">
		<a
			<c:if test="${isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.go('${base}/forwardIbc.do?h5=1', '10', this);"</c:if>
			<c:if test="${!isLogin}">href="javascript:void(0);" onclick="MobileIndexUtil.toLogin();" </c:if>
			class="external" title="沙巴体育">
			<div class="lottery_img">
				<img src="${base }/mobile/anew/resource/new/images/hgsport.png" />
			</div>
			<div class="lottery_info">
				<div class="lottery_name">沙巴体育</div>
			</div>
		</a>
		<div class="cl"></div>
	</div>
</c:if>
