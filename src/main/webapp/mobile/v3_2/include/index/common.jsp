<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
function toLogin(){
	location.href = '${m}/login.do'
}

// $(function(){
//     $.showPreloader('加载中');
//     setTimeout(function () {
//         $.hidePreloader();
//     }, 2000);
// });
</script>
<!-- 侧边栏菜单 -->
<div class="panel panel-left panel-reveal theme-dark" id='panel-left-demo'>
  <div class="content-block">
    <p>这是一个侧栏</p>
    <p>你可以在这里放用户设置页面</p>
    <p><a href="javascript:void(0);" class="close-panel">关闭</a></p>
  </div>
  <div class="list-block">
    <!-- .... -->
  </div>
</div>
<div class="panel panel-right panel-reveal">
  <div class="content-block">
    <p>这是一个侧栏</p>
    <!-- Click on link with "close-panel" class will close panel -->
    <p><a href="#" class="close-panel">关闭</a></p>
  </div>
</div>

<script type="text/javascript">
   $.init();

   function sui_ajax_callBack(html){
       window.sui_ajax_data = html;
   }

   $(document).on("pageInit", function(e, pageId, $page) {
	   console.log(pageId);
      //销毁之前的私有style
      $(".page_style").remove();
      //销毁之前的私有js对象
      if(typeof(pageJS) == "undefined"){
        pageJS = null;
      }

      var ajData = null;

      //设置html页面数据缓存
      if(!window.htmlCache){
          window.htmlCache = {};  
      }

      //将页面数据放到页面缓存
      if(!window.htmlCache[pageId]){
        var ajData = window.sui_ajax_data;
        window.htmlCache[pageId] = ajData;
        //销毁sui_ajax_data
        window.sui_ajax_data = null;
      }else{
        ajData = window.htmlCache[pageId];
      }

      if(ajData){
        var $doc = $('<html></html>');
        $doc.append(ajData);

        //添加私有syle,这样的页面渲染是否有问题，待考察
        var $style = $doc.find(".page_style");  //私有style对象
        $("html").append($style);

        //执行私有js
        var script = $doc.find(".page_script").html();
        // eval(script);
        evil(script);
        pageJS_init(); //调用页面私有js
      }

  });
   // 重写 eval
   function evil(fn) {
     try {
       var Fn = Function;  //一个变量指向Function，防止有些前端编译工具报错
       return new Fn('return ' + fn)();
     } catch (e) {
       console.log(e)
     }
   }
</script>