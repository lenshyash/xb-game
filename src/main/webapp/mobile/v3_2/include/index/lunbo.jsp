<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type='text/javascript' src='${res }/js/touchSlide.js' charset='utf-8'></script>
<div id="focus" class="row no-gutter focus">
	<div class="hd">
		<ul></ul>
	</div>
	<div class="bd">
		<ul>
			<c:forEach items="${agentLunBos }" var="item" varStatus="status">
				<li class="disable-router"><a href="${ item.titleUrl}"></a><img src="${item.titleImg}"/></a></li>
			</c:forEach>
		</ul>
	</div>
</div>
<script type="text/javascript">
$(function(){
	TouchSlide({ 
		slideCell:"#focus",
		titCell:".hd ul",
		mainCell:".bd ul", 
		interTime: 5000,
		effect:"leftLoop", 
		autoPlay:true,//自动播放
		autoPage:true //自动分页
	});
});
</script>