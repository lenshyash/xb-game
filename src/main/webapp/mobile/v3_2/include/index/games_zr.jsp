<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${isZrOnOff=='on' && isGuest != true}">
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="go('${base}/forwardAg.do?h5=1&amp;gameType=11', '1',this);" class="external"</c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/agreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="go('${base}/forwardMg.do?gameType=1&amp;gameid=66936&amp;h5=1', '3', this);" class="external"</c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/mgreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardBbin.do?type=live', '2');" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/bbinreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BBIN真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isVrOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardVr.do', '97');" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/vr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">VR游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isKyOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardKy.do', '12');" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/ky.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">KY棋牌</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardBg.do?type=2&h5=1', '98');" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/bg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BG真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isAbOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">onclick="go('${base}/forwardAb.do', '5',this);" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/ab.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AB真人</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	</c:if>
	
	<c:if test="${isDzOnOff eq 'on' }">
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:void(0);" onclick="go('${base}/forwardAg.do?h5=0&amp;gameType=6', '1',this);" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/agreal.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG电子</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=mg" class="external"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
									<div class="lottery_img">
										<img src="${res}/images/real/mg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG电子</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
		<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=pt" class="external"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
					<div class="lottery_img">
						<img src="${res}/images/real/pt.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">PT电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isQtOnOff eq 'on' && isGuest != true}">
				<div class="index_lottery_item">
				 <a 
				 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=qt" class="external"</c:if>
				 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
					<div class="lottery_img">
						<img src="${res}/images/real/qt.png" />
					</div>
					<div class="lottery_info">
						<div class="lottery_name">QT电子</div>
					</div>
				</a>
				<div class="cl"></div>
				</div>
		</c:if>
		<c:if test="${isByOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=by" class="external"</c:if> 
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" class="external"</c:if> >
				<div class="lottery_img">
					<img src="${res}/images/real/by.png" />
				</div>
				<div class="lottery_info">
					<div class="lottery_name">捕鱼专题</div>
				</div>
			</a>
			<div class="cl"></div>
			</div>

		</c:if>
	</c:if>
