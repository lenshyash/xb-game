<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/res.jsp" />
</head>
<body>
	<div class="page-group">
	<div class="page" id="page_game_center_jsp">
		<jsp:include page="include/barTab.jsp" />
		<header class="bar bar-nav">
			<a class="title">购彩大厅</a> <span class="pull-right">
				<div class="ui-toolbar-right ui-head">
					<a class="head-list" val="1" href="javascript:;"></a> <a
						class="head-icon" val="2" href="javascript:;"></a>
				</div>
			</span>
		</header>
		<div class="row no-gutter lott-menu">
			<ul>
				<li class="all cur" data-cat="0"></li>
				<li class="gaopin" data-cat="1"></li>
				<li class="dipin" data-cat="2"></li>
			</ul>
		</div>
		<div class="content">
			<%-- 这里是页面内容区 --%>
			<div class="list-block media-list list-liebiao">
				<ul>
					<c:forEach items="${bcLotterys}" var="item">
						<li lot_code="${item.code}"
							class="game_category_${(item.type == '4' || item.type == '54' || item.type == '6' || item.type == '15')?2:1}">
							<a href="#" class="item-link item-content">
								<div class="item-media">
									<img src="${base}/mobile/anew/resource/images/${item.code}.png">
								</div>
								<div class="item-inner">
									<div class="item-title-row">
										<div class="item-title">${item.name}</div>
										<div class="item-after">
											第<span style="color: red;" id="next_qihao_${item.code}">加载中...</span>期
										</div>
									</div>
									<div class="item-subtitle" id="next_open_haoma_${item.code}">2
										3 5</div>
									<div class="item-title-row">
										<div class="item-title">
											距第<span style="color: red;" id="cur_qihao_${item.code}">加载中...</span>期<span
												id="open_close_${item.code}">截止</span>
										</div>
										<div class="item-after" id="is_times_${item.code}">00:00:00</div>
									</div>
								</div>
						</a>
						</li>
					</c:forEach>
				</ul>
			</div>

			<div class="list-block media-list lottery-list list-jiugg hide">
				<ul>
				<c:forEach items="${bcLotterys}" var="item">
					<li class="game_category_${(item.type == '4' || item.type == '54' || item.type == '6' || item.type == '15')?2:1}"><a
						href="#">
							<div class="hot-icon">
								<img src="${base}/mobile/anew/resource/images/${item.code}.png">
							</div>
							<p class="hot-text">${item.name}</p>
							<p class="last-time">
								<span class="is_times_${item.code}">00:00:00</span>
							</p>
					</a></li>
				</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</div>
<jsp:include page="include/common/need.jsp" />
</body>
</html>
