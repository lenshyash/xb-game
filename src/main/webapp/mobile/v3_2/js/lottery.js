$(function(){
	'use strict';
	/**
	 * 够彩大厅
	 */
	$(document).on("pageInit", "#page_game_center_jsp", function(e, pageId, $page) {
		console.info("当前加载id=" + pageId);
		var timers,downTimers;
		initCurData(null);	//首次加载无需携带code
		function initCurData(lotCode){
			$.ajax({
				url : M.base + '/mobile/v3/lotterys.do',
				data:{lotCode:lotCode,version:M.version,needLast:true},
				dataType:'json',
				type:'get',
				success:function(data){
					$.each(data.lot,function(i,j){
						dataCur(data[j.code]);
						dataTimer(data.serverTime,data[j.code],j);
					})
				}
			})
		}
		function dataCur(cur){
			if(!cur){return;}
			$("#next_qihao_"+cur.lotCode).text(cur.lastQiHao);
			$("#cur_qihao_"+cur.lotCode).text(cur.qiHao);
			if(cur.lastHaoMa.indexOf("?")>=0){
				$("#next_open_haoma_"+cur.lotCode).html('<span style="color:red">开奖中...</span>');
				clearInterval(timers);
				timers = window.setInterval(function(){
					//5秒中获取一次开奖结果,正式开启
				},5000);
			}else{
				$("#next_open_haoma_"+cur.lotCode).html(lotteryHaoMa(cur.lastHaoMa,cur.lotCode,1));
			}
		}
		
		function dataTimer(serverTime,cur,lot){
			if(!cur){return;}
			var state = 1;
			if(cur.id && cur.id == 1){
				state = 2;
			}
			var server_time = Date.parse(new Date(serverTime));
			var action_time = cur.actionTime;
			var diff_time = ( action_time - server_time ) / 1E3;
			var ddTime = diff_time -(action_time - new Date().getTime()) / 1E3;	//和访问机器相差的秒数
			MobileV3.diffTimer(diff_time);
			clearInterval(downTimers);
			downTimers = window.setInterval(function(){
				//MobileV3.tickV2(lot.ago,action_time,diff_time,ddTime,state,lot.code);
				tick(lot.ago,action_time,diff_time,ddTime,state,lot.code);
				diff_time--;
			},1000);
		}
		
		function lastData(){
			
		}
		
		function tick(ago,action_time,diff_time,ddTime,state,code){
			var vg = 0,dTime = 0;
			dTime = diff_time - (action_time - new Date().getTime()) / 1E3;//	计算倒计时和js时间的差值
			if(dTime > ddTime && dTime - ddTime > 1){
				diff_time = diff_time - parseInt(dTime-ddTime);
			}
			vg = diff_time - ago;
			if(diff_time > 0 && diff_time > ago && state == 1){
				var html = MobileV3.diffTimer(vg);
				if(code == 'FFC'){
					console.info('code='+code + "--" +html);
				}
			}else{
				var html = MobileV3.diffTimer(diff_time);
				if(code == 'FFC'){
					console.info('code=' + code + "==" + html);
				}
			}
			if(diff_time <= 0){
				clearInterval(downTimers);
				initCurData(code);
			}
		}
		/**
		 *排版切换 
		 */
		var uiVal = 2,_$page = $page.find(".list-liebiao");	//默认排版val
		$page.find(".ui-toolbar-right a").on("click",function(){
			var t = $(this),isUi = t.parent().hasClass("ui-head");
			uiVal = parseInt(t.attr("val"));
			if(uiVal == 1 && isUi){	//九宫格
				t.parent().removeClass("ui-head");
				$page.find(".list-jiugg").removeClass("hide").siblings(".list-liebiao").addClass("hide");
			}else if(uiVal == 2 && !isUi){	//竖列表
				t.parent().addClass("ui-head");
				$page.find(".list-liebiao").removeClass("hide").siblings(".list-jiugg").addClass("hide");
			}
			//切换排版时初始化选中的频率彩
			var cat = $page.find(".lott-menu li.cur").data("cat");
			_$page = uiVal == 1?$page.find(".list-jiugg"):$page.find(".list-liebiao");
			if(cat == 0){
				_$page.find("ul li").removeClass("hide");
			}else{
				_$page.find("ul li").addClass("hide").siblings(".game_category_"+cat).removeClass("hide");
			}
		})
		
		/**
		 * 彩种切换
		 */
		$page.find(".lott-menu li").on("click",function(){
			var t = $(this),cat = t.data("cat");
			t.addClass("cur").siblings().removeClass("cur");
			if(cat == 0){
				_$page.find("ul li").removeClass("hide");
			}else{
				_$page.find("ul li").addClass("hide").siblings(".game_category_"+cat).removeClass("hide");
			}
		})
	});
	
	
	/**
	 * 开奖结果
	 */
	$(document).on("pageInit", "#page_draw_notice_jsp", function(e, pageId, $page) {
		console.info("当前加载id=" + pageId);
		function initPageDraw(){
			$.ajax({
				url:M.base + "/mobile/v3/draw_notice_data.do",
				dataType:'json',
				type:'get',
				success:function(data){
					if(data.success){
						var html = template('pageDrawNoticeTpl',data);
						$page.find('.content .list-block ul').html(html);
					}
				}
			})
		}
		
		initPageDraw();
		var $content = $page.find(".content").on('refresh', function(e){
		      // 模拟2s的加载过程
		      setTimeout(function() {
		    	 initPageDraw();
		    	  // 加载完毕需要重置
		    	  $.pullToRefreshDone($content);
		      }, 2000);
		 });
		
		
		template.helper('$tplHaoMa', function (haoma,code) {
			if(haoma.indexOf('?') >= 0){
				return '<span style="color:red">开奖中...</span>';
			}
		    return lotteryHaoMa(haoma,code,1);
		});
	})
	
	/**
	 * 开奖结果详情
	 */
	$(document).on("pageInit", "#page_draw_notice_details_jsp", function(e, pageId, $page) {
		console.info("当前加载id=" + pageId);
		//初始化内容
		$page.find('.content .list-block ul li').remove();
		var loading = false,itemsPerLoad = 10,page=1,totalCount = 0;
		function initPageDrawData(){
			$.ajax({
				url:M.base + "/mobile/v3/draw_notice_details_data.do",
				data:{lotCode:lotCode,page:page,rows:itemsPerLoad},
				dataType:'json',
				type:'get',
				success:function(data){
					if(data.success){
						if(page<2){
							totalCount = data.data.totalPageCount;
						}
						var html = template('drawNoticeDetailsDataTpl',data);
						$page.find('.content .list-block ul').append(html);
					}
				}
			})
		}
		//初始化先执行一次
		if(!($(".infinite-scroll-preloader").text() == "加载完毕")){
			setTimeout(function(){
				initPageDrawData();
			},300)
		}
		$page.on('infinite', function() {
		      // 如果正在加载，则退出
		      if (loading) return;
		      // 设置flag
		      loading = true;
		      // 模拟1s的加载过程
		      setTimeout(function() {
		        loading = false;
		        page++;
		        if (page>totalCount) {
		        	$(".infinite-scroll-preloader").text("加载完毕");
		        	return;
		        }
		        //加载数据
		        initPageDrawData();
		        $.refreshScroller();
		      }, 1000);
		});
		template.helper('$tplHaoMas', function (haoma) {
			return lotteryHaoMa(haoma,lotCode,2);
		});
	});
	
	
	/**
	 * 初始化
	 */
	$.init();
})

/**
 * 获取url内的参数值
 * @param name
 * @returns
 */
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

function lotteryHaoMa(haoma,code,type){
	var html = '',hmArr,sum=0;
	if((typeof haoma=='string')&&haoma.constructor==String){
		//字符串类型
		hmArr = haoma.split(",");
	}else if((typeof haoma=='object')&&haoma.constructor==Array){
		//数组类型
		hmArr = haoma;
	}
	switch(code){
		case "FFC":
		case "WFC":
		case "HKWFC":
		case "AMWFC":
		case "SFC":
		case "ESFC":
		case "EFC":
		case "CQSSC":
		case "XJSSC":
		case "TJSSC":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					sum = '?';
					html += '<span class="ssc_img">-</span>';
				}else{
					sum += parseInt(hmArr[i]);
					html += '<span class="ssc_img">'+firstAddZero(hmArr[i])+'</span>';
				}
			}
			if(type == 2){
				html += '=<span class="ssc_img">'+firstAddZero(sum)+'</span>'
				if(sum != '?'){
					html += '<span class="ssc_img">'+(sum>=23?'大':'小')+'</span>';
					html += '<span class="ssc_img">'+(sum%2==0?'双':'单')+'</span>';
				}else{
					html += '<span class="ssc_img">-</span>';
					html += '<span class="ssc_img">-</span>';
				}
			}
			return html;
		case "BJSC":
		case "XYFT":
		case "LXYFT":
		case "SFSC":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					if(type == 1){
						html += '<span class="pk10_kjjg">-</span>';
					}else{
						html += '<span class="pk10_span">-</span>';
					}
				}else{
					if(type == 1){
						html += '<span class="pk10 b'+firstAddZero(hmArr[i])+'"></span>';
					}else{
						html += '<span class="pk10_img pk10_bj_'+firstAddZero(hmArr[i])+'"></span>';
					}
				}
			}
			return html;
		case "HNKLSF":
		case "GDKLSF":
		case "SFKLSF":
		case "WFKLSF":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					if(type == 1){
						html += '<span class="sfc_img_litter">-</span>';
					}else{
						html += '<span class="sfc_img">-</span>';
					}
				}else{
					if(type == 1){
						html += '<span class="sfc_img_litter">'+firstAddZero(hmArr[i])+'</span>';
					}else{
						html += '<span class="sfc_img">'+firstAddZero(hmArr[i])+'</span>';
					}
				}
			}
			return html;
		case "CQXYNC":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					html += '<span class="xync_span">-</span>';
				}else{
					html += '<span class="xync_img xync_bj_'+firstAddZero(hmArr[i])+'"></span>';
				}
			}
			return html;
		case "JSSB3":
		case "HBK3":
		case "AHK3":
		case "SHHK3":
		case "HEBK3":
		case "GXK3":
		case "GZK3":
		case "BJK3":
		case "JXK3":
		case "GSK3":
		case "FFK3":
		case "WFK3":
		case "JPK3":
		case "KRK3":
		case "HKK3":
		case "AMK3":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					sum = '?';
					html += '<span class="k3_span">-</span>';
				}else{
					sum += parseInt(hmArr[i]);
					html += '<span class="k3_img k3_bj_'+firstAddZero(hmArr[i])+'">-</span>'
				}
			}
			if(type == 2){
				if(sum != '?'){
					html += '<span class="k3_img sum_k3_img">'+firstAddZero(sum)+'</span>';
					html += '<span class="k3_img sum_k3_img">'+(sum>=11?'大':'小')+'</span>';
					html += '<span class="k3_img sum_k3_img">'+(sum%2!=0?'单':'双')+'</span>';
				}else{
					html += '<span class="k3_img sum_k3_img">-</span>';
					html += '<span class="k3_img sum_k3_img">-</span>';
					html += '<span class="k3_img sum_k3_img">-</span>';
				}
			}
			return html;
		case "GD11X5":
		case "SD11X5":
		case "JX11X5":
		case "SH11X5":
		case "GX11X5":
		case "FF11X5":
		case "SF11X5":
		case "WF11X5":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					sum = '?';
					html += '<span class="syx5_img">-</span>';
				}else{
					sum += parseInt(hmArr[i]);
					html += '<span class="syx5_img">'+firstAddZero(hmArr[i])+'</span>';
				}
			}
			if(type == 2){
				if(sum != '?'){
					html += '=<span class="syx5_img">'+firstAddZero(sum)+'</span>'
					html += '<span class="syx5_img">'+(sum>=30?'大':'小')+'</span>';
					html += '<span class="syx5_img">'+(sum%2==0?'双':'单')+'</span>';
				}else{
					html += '=<span class="syx5_img">-</span>'
					html += '<span class="syx5_img">-</span>';
					html += '<span class="syx5_img">-</span>';
				}
			}
			return html;
		case "FC3D":
		case "PL3":
		case "FF3D":
		case "WF3D":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					html += '<span class="pl3_img">-</span>';
				}else{
					html += '<span class="pl3_img">'+firstAddZero(hmArr[i])+'</span>'
				}
			}
			return html;
		case "LHC":
		case "SFLHC":
		case "TMLHC":
		case "TMLHC":
		case "WFLHC":
		case "HKMHLHC":
			for(var i=0;i<hmArr.length;i++){
				if(hmArr[i] == '?'){
					if(i == 6){
						html += '=<span class="lhc_img">-</span>';
					}else{
						html += '<span class="lhc_img">-</span>';
					}
				}else{
					if(i == 6){
						html += '=<span class="lhc_img lhc_bj_'+firstAddZero(hmArr[i])+'">'+firstAddZero(hmArr[i])+'</span>';
					}else{
						html += '<span class="lhc_img lhc_bj_'+firstAddZero(hmArr[i])+'">'+firstAddZero(hmArr[i])+'</span>';
					}
				}
			}
			return html;
		case "PCEGG":
		case "JND28":
		case "SF28":
			if(type == 1){
				for(var i=0;i<hmArr.length;i++){
					if(hmArr[i] == '?'){
						sum = '?';
						if(i == 2){
							html += '<span class="pcegg_img pcegg_bj_wh">-</span>';
						}else{
							html += '<span class="pcegg_img pcegg_bj_wh">-</span>+';
						}
					}else{
						sum += parseInt(hmArr[i]);
						if(i == 2){
							html += '<span class="pcegg_img pcegg_bj_'+firstAddZero(hmArr[i])+'">-</span>';
						}else{
							html += '<span class="pcegg_img pcegg_bj_'+firstAddZero(hmArr[i])+'">-</span>+';
						}
					}
				}
				if(sum != '?'){
					html += '=<span class="pcegg_img pcegg_bj_'+firstAddZero(sum)+'">-</span>'
				}else{
					html += '=<span class="pcegg_img pcegg_bj_wh">-</span>'
				}
			}else{
				for(var i=0;i<hmArr.length;i++){
					if(i%2==0){
						if(hmArr[i] == '?'){
							html += '<span class="pcegg_img pcegg_bj_wh">-</span>';
						}else{
							html += '<span class="pcegg_img pcegg_bj_'+firstAddZero(hmArr[i])+'">-</span>';
						}
					}else{
						html += hmArr[i];
					}
				}
			}
			return html;
	}
}

function firstAddZero(value){
	value = parseInt(value);
	if(value < 10){
		return "0"+value;
	}
	return value;
}



var MobileV3 = {
	timeFormat:function(e){
		return new Date(e.replace(/-/g,"/"));
	},
	diffTimer:function(intDiff){
		var day=0,hour=0,hours=0,minute=0,second=0,time_html='';//时间默认值		
		if(intDiff > 0){
			day = Math.floor(intDiff / (60 * 60 * 24));
			hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
			hours = Math.floor(intDiff / (60*60));
			minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
			second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
		}
		if (hours <= 9 ) hours = '0' + hours;
		if (minute <= 9) minute = '0' + minute;
		if (second <= 9) second = '0' + second;
		time_html = hours + ':' + minute + ':' + second;
		return time_html;
	},
	tickV2:function(ago,action_time,diff_time,ddTime,state,code){
		var vg = 0,dTime = 0;
		dTime = diff_time - (action_time - new Date().getTime()) / 1E3;//	计算倒计时和js时间的差值
		if(dTime > ddTime && dTime - ddTime > 1){
			diff_time = diff_time - parseInt(dTime-ddTime);
		}
		vg = diff_time - ago;
		if(diff_time > 0 && diff_time > ago && state == 1){
			var html = MobileV3.diffTimer(vg);
			if(code=='WFC'){
				console.info('code='+code + "--" +html);
			}
		}else{
			var html = MobileV3.diffTimer(diff_time);
		}
		if(diff_time <= 0){
			
		}
	}
}