<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/res.jsp"/>
  </head>
  <body>
	<div class="page">
	
	<!-- 头部 -->
	<header class="bar bar-nav">
	  <div class="content">
		   <h1 class="title title-1"><span class="icon icon-menu open-panel" data-panel='#panel-left-demo'></span>${_title }</h1>
		   <h1 class="title title-2">
		   	<div class="login_status_bar">
		   		<c:if test="${!isLogin }">
					<em class="iconfont icon-user8"></em><a href="${m }/login.do" class="external">登录</a>
				 	<a href="${m }/regpage.do" class="external"> | 注册</a>
			 	</c:if>
			 	<c:if test="${isLogin }">
					<a href="${station }/personal_center.do">
						<div class="user_logined_img">
							<img src="${res }/images/touxiang.png">
						</div> <span> <em class="iconfont icon-jindous2x"></em><span>${loginMember.money }</span>
					</span>
					</a>
			 	</c:if>
			</div>
		   </h1>
	   </div>
	 </header>
	
	  <!-- 底部工具栏-->
	  <jsp:include page="include/barTab.jsp"/>
	  <div class="content">
	    <!-- 这里是页面内容区 -->
	    
	    <!-- 轮播图 -->
	    <c:if test="${!empty agentLunBos }">
			<jsp:include page="include/index/lunbo.jsp"/>
	    </c:if>
	    
	    <!-- 系统公告 -->
	    <c:if test="${not empty publicInfo }">
	 		<div class="row no-gutter notice">
				<span class="tips_title">公告</span>
				<marquee scrollamount="3" class="tips_content font_color_gray" style="width: 85%;height: 1rem;margin-left:13%;margin-top:0.2rem;">${publicInfo }</marquee>
			</div>
	    </c:if>
	    
    	<!-- no-gutter: 无间距 -->
		<c:if test="${iosExamine ne 'off' }">
			<div class="row no-gutter quickMenu">
				<div class="col-25">
					<a class="qMenuItem color_huang"
						href="${m }/personal_center.do"><icon
							class="iconfont ft34">&#xe675;</icon>存／取款</a>
				</div>
				<div class="col-25">
					<a class="qMenuItem color_hong"
						href="${m }/personal_center.do"><icon
							class="iconfont pb2">&#xe64c;</icon>投注纪录</a>
				</div>
				<div class="col-25">
					<c:if test="${isActive }">
						<a class="qMenuItem color_lv external" href="${m }/active.do"><icon
								class="iconfont pb2">&#xe630;</icon>优惠活动</a>
					</c:if>
					<c:if test="${!isActive }">
						<a class="qMenuItem color_lv external" href="${m }/appDownload.do"><icon
								class="iconfont pb2">&#xe630;</icon>A P P下载</a>
					</c:if>
						
				</div>
				<div class="col-25">
					<a class="qMenuItem color_lan external" href="${kfUrl}"><icon class="iconfont pb2">&#xe654;</icon>在线客服</a>
				</div>
			</div>
		</c:if>
	    
		<div class="tabBox" style="margin:auto;">
			<div class="hd">
				<h3><a href="javascript:void(0);" style="color: #967f7f;">彩种大厅</a><span>LOTTERY</span></h3>
			</div>
		</div>
		<div class="row no-gutter index_margin">	
			<c:if test="${v eq 'v1'}">
				<jsp:include page="include/index/games_cp.jsp"></jsp:include>
				<jsp:include page="include/index/games_ty.jsp"></jsp:include>
				<jsp:include page="include/index/games_zr.jsp"></jsp:include>
			</c:if>
			<c:if test="${v eq 'v2'}">
				<jsp:include page="include/index/games_zr.jsp"></jsp:include>
				<jsp:include page="include/index/games_ty.jsp"></jsp:include>
				<jsp:include page="include/index/games_cp.jsp"></jsp:include>
			</c:if>
			<c:if test="${v eq 'v3'}">
				<jsp:include page="include/index/games_ty.jsp"></jsp:include>
				<jsp:include page="include/index/games_cp.jsp"></jsp:include>
				<jsp:include page="include/index/games_zr.jsp"></jsp:include>
			</c:if>
		</div>
	    
	    <!-- 底部新闻 -->
<%-- 	    <jsp:include page="include/index/news.jsp"/> --%>
	    <!-- 底部工具图标 -->
	    <jsp:include page="include/index/bottom_tool.jsp"/>
	  </div>
	</div>
	<jsp:include page="include/common/need.jsp"/>
	<jsp:include page="include/index/common.jsp"/>
  </body>
</html>