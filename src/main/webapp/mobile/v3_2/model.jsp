<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
	<jsp:include page="include/res.jsp"/>
  </head>
  <body>
	<div class="page">
	  <jsp:include page="include/barTab.jsp"/>
	  <div class="content">
	    <!-- 这里是页面内容区 -->
	    
	    
	  </div>
	</div>
  </body>
</html>
<!-- 注意: 1.每个页面必须有pageJS作用域,pageJS_init函数，以及兼容刷新url调用的脚本；
      2.脚本的class需要定义为：page_script；
      3.必须添加"兼容刷新url调用的脚本",这个可在blade模板中统一添加 -->
<jsp:include page="include/common/need.jsp"/>