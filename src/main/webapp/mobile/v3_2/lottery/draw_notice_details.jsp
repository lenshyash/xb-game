<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/res.jsp" />
</head>
<body>
	<div class="page-group">
		<div class="page" id="page_draw_notice_details_jsp">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/mobile/v3/draw_notice.do"> <span
					class="icon icon-left"></span> 返回
				</a> <a class="title">${lotName}</a>
			</header>
			<div class="content infinite-scroll">
				<div class="list-block media-list">
					<ul>
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var lotCode = '${lotCode}';
		</script>
		<script type="text/html" id="drawNoticeDetailsDataTpl">
		{{each data.list as value i}}
		<li style="{{i%2!=0?'background:#f4f4f4':''}}">
			<div class="item-content">
				<div class="item-inner">
				<div class="item-title-row">
					<div class="item-title">第{{value.qiHao}}期</div>
					<div class="item-after">{{value.date}}&nbsp;{{value.time}}</div>
				</div>
				<div class="item-subtitle">{{$tplHaoMas value.haoMaList}}</div>
				</div>
			</div>
		</li>
		{{/each}}
	</script>
	</div>
</body>
</html>
<jsp:include page="../include/common/need.jsp" />