<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/res.jsp" />
</head>
<body>
	<div class="page-group">
		<div class="page" id="page_draw_notice_jsp">
			<jsp:include page="include/barTab.jsp" />
			<header class="bar bar-nav">
				<a class="title">开奖结果</a>
			</header>
			<div class="content pull-to-refresh-content" data-ptr-distance="55">
				<!-- 默认的下拉刷新层 -->
				<div class="pull-to-refresh-layer">
					<div class="preloader"></div>
					<div class="pull-to-refresh-arrow"></div>
				</div>
				<div class="list-block media-list">
					<ul></ul>
				</div>
			</div>
			<script type="text/html" id="pageDrawNoticeTpl">
			{{each data as value i}}
			<li style="{{i%2!=0?'background:#f4f4f4':''}}"><a href="${base}/mobile/v3/draw_notice_details.do?lotCode={{value.lotCode}}" class="item-link item-content">
				<div class="item-media">
					<img src="${base}/mobile/anew/resource/images/{{value.lotCode}}.png">
				</div>
				<div class="item-inner">
					<div class="item-title-row">
						<div class="item-title">{{value.lotName}}</div>
						<div class="item-after">第{{value.qihao}}期</div>
					</div>
					<div class="item-text">{{$tplHaoMa value.haoma value.lotCode}}</div>
					</div>
				</a>
			</li>
			{{/each}}
		</script>
		</div>
	</div>
</body>
</html>
<jsp:include page="include/common/need.jsp" />