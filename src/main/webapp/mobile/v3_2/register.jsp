﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="${base }/mobile/anew/resource/new/css/global.css?ver=4.4" type="text/css">
    <script src="${base }/mobile/anew/resource/new/js/jquery.min.js?ver=4.4"></script>
    <script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base }/mobile/anew/resource/new/js/core.js" path="${base}"></script>
	<script type="text/javascript" src="${base}/regconf.do"></script>
	<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>
    <script>
    	$(function(){
    		var _padding = function()
    		{
    			try{
	    			var l = $("body>.header").height();
		    		if($("body>.lott-menu").length>0)
		    		{
		    			l += $("body>.lott-menu").height();
		    		}
		    		$("#wrapper_1").css("paddingTop",l+"px");
	    		}catch(e){}
	    		try{
		    		if($("body>.menu").length>0)
		    		{
		    			var l = $("body>.menu").height();
		    		}
		    		$("#wrapper_1").css("paddingBottom",l+"px");
	    		}catch(e){}
    		};
    		(function(){
    			_padding();
    		})();
    		$(window).bind("load",_padding);
    	});
    </script>
    <!-- hide address bar -->
        <style>
            body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
            #wrapper_1{overflow-y:visible!important;}
            body.login-bg{padding-top:0px!important;}
        </style>
        <title>${_title }--注册</title>
    </head>
<body class="login-bg" data-ext-version="1.4.2">
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="window.history.go(-1);">reveal</button>
        </div>
        <h1 class="ui-toolbar-title">快速注册</h1>
    </div>
</div>
    <div class="login">
        <form id="register_form">
        <ul>
            <li>
                <span class="logi">用户账号</span><input type="text" id="accountno" name="uid" placeholder="请输入用户名">
            </li>
            <li>
                <span class="logi">登录密码</span><input type="password" id="password1" name="pwd" placeholder="请输入密码">
            </li>
            <li id="reg_tb_after">
                <span class="logi">重复密码</span><input type="password" id="password2" placeholder="请再次输入密码">
            </li>
            <li>
                <span class="logi">验&nbsp;&nbsp;证&nbsp;&nbsp;码</span>
                <input type="text" id="code" name="captcha" placeholder="请输入验证码" style="width:50%;position:position;">
                <img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" src="${base}/regVerifycode.do" onclick="reloadImg();">
            </li>
        </ul>
        </form>
        <button class="login-btn login-btn-no" id="register_btn">立即注册</button>
        <c:if test="${not empty testAccount && testAccount eq 'on'}">
        	<a href="${base }/mobile/swregpage.do" class="reg-btn" id="login_demo" style="margin-top: 12px; font-size:16px;">免费试玩</a>
        </c:if>
        <button class="reg-btn" onclick="location.href='${base}/mobile/login.do'">返回登录</button>
        <button class="reg-btn" onclick="location.href='${base}/mobile/index.do'">返回首页</button>
        <br>
        <div class="login-p">
        <c:if test="${kfSwitch eq 'on'}">
        	<a class="fl" href="${kfUrl }" target="_blank">在线客服</a>
        </c:if>
        <a class="fr" href="${base }/mobile/login.do">已有帐号，直接登录</a></div>
        <div class="login-p" style="color: #fe0103;">1.用户帐号请输入5-11个英文字母或数字，不能用中文。</div>
        <div class="login-p" style="color: #fe0103;">2.登录密码请输入6-16个英文字母或数字。</div>
    </div>

    
<style>
    .center {text-align: center}
</style>

<div class="beet-odds-tips round" id="tip_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="tip_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            号码选择有误
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que" style="width: 100%;" onclick="tipOk()"><span>确定</span></button>
    </div>
</div>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<script>
    var func;
    function tipOk() {
        $('#tip_pop').hide();
        $('#tip_bg').hide();
    }
    function msgAlert (msg,funcParm) {
        $('div#tip_pop_content').html(msg);
        $('div#tip_pop').show();
        $('div#tip_bg').show();
        func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
    function reloadImg(){
    	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
    	$("#regCode").attr("src", url);
    }
</script>

    <script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
		<li><span class="logi">{{tl.name}}</span>
				{{if tl.type==1}}
					 <input type="text" class="joinUsInput" placeholder="请输入{{tl.name}}" name="{{tl.key}}" id="{{tl.key}}">
				{{/if}}
				{{if tl.type==2}}
						<select id="{{tl.key}}" class="joinUsInput">
						{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
						{{/if}}
					</select>
				{{/if}}
				{{if tl.type==3}}
					{{if tl.sourceLst}}
					<p>
					{{each tl.sourceLst as rad i}}
						<input type="radio" class="joinUsInput" name="{{tl.key}}" value="{{i+1}}">{{rad}}</input>
					{{/each}}
					</p>
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<input type="checkbox" class="joinUsInput" name="{{tl.key}}" placeholder="请输入{{tl.name}}" value="{{i+1}}">{{chk}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<textarea id="{{tl.key}}"></textarea>
				{{/if}}
				{{if tl.type==6}}
					<input type="password" class="joinUsInput" name="{{tl.key}}" id="{{tl.key}}" placeholder="请输入{{tl.name}}">
				{{/if}}
				{{$addValidateFiled tl}}
			</li>
		{{/each}}
	</script> 


<script>
    $('#register_btn').click(function(event) {
    	var account = $('#accountno').val();
        var password1 = $('#password1').val();
        var password2 = $('#password2').val();
        var code = $('#code').val();//验证码
        if(!account){
        	msgAlert("用户名不能为空!");
       	 return false;
        }
        if(!password1){
        	msgAlert("密码不能为空!");
       	 return false;
        }
        if(password1.length<6){
        	msgAlert("密码不能小于6位!");
       	 return false;
        }
        if(!password2){
        	msgAlert("确认密码不能为空!");
       	 return false;
        }
        if(password1 != password2){
         	msgAlert("两次密码不一致!");
       	 return false;
        }
        if(!code){
        	msgAlert("验证码不能为空!");
       	 return false;
        }
        
        var data = getCommitData();
        
        //可能网络原因或者手机型号原因,多判断一次非空情况
        if(!account || !password1 || !password2 || !code){
        	account = $('#accountno').val();
            password1 = $('#password1').val();
            password2 = $('#password2').val();
            code = $('#code').val();
        }
        
        data["account"] = account;
        data["password"] = password1;
    	data["rpassword"] = password2;
        data["verifyCode"] = code;
        
        var targetUrl = '${base}/mobile/index.do';//跳转的页面
    	var isGuest = $('#isGuest').val();//是否是试玩注册
    	var stationB = $('#stationB').val();
    	var url = '${base}/register.do';
    	if(isGuest){
    		url = '${base}/registerTestGuest.do';
    	}
      	$.ajax({
     		url:url,
     		data : {
    			data : JSON.encode(data)
    		},
     		type:"POST",
    		success : function(result, textStatus, xhr) {
    			var ceipstate = xhr.getResponseHeader("ceipstate")
    			if (!ceipstate || ceipstate == 1) {// 正常响应
    				if(!result.success){
    					msgAlert(result.msg);
    				}else{
    					msgAlert("注册成功!");
    					parent.location.href = targetUrl;
    				}
    			} else if (ceipstate == 2) {// 后台异常
    				msgAlert("后台异常，请联系管理员!");
    				reloadImg();
    			} else if (ceipstate == 3) { // 业务异常
    				msgAlert(result.msg);
    				reloadImg();
    			}
    		}
     	});
    });
    
</script>
</body>
</html>