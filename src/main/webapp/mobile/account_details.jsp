﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<%
Long stationId = StationUtil.getStation().getId();
String onoffLiveGame = StationConfigUtil.get(stationId, StationConfig.onoff_zhen_ren_yu_le);
String onoffOnlinepayRecoed = StationConfigUtil.get(stationId, StationConfig.onoff_online_pay_class);
pageContext.setAttribute("isZrOnOff", onoffLiveGame);
pageContext.setAttribute("isOprOnOff", onoffOnlinepayRecoed);

if("on".equals(onoffOnlinepayRecoed) && "on".equals(onoffLiveGame)){
	pageContext.setAttribute("menuItemWidth", "33");
} else if("on".equals(onoffOnlinepayRecoed) || "on".equals(onoffLiveGame)){
	pageContext.setAttribute("menuItemWidth", "50");
} else {
	pageContext.setAttribute("menuItemWidth", "100");
}
%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=3" type="text/css" />
</head>
<body >


<div class="top">
	<div class="inner">
		
		<div class="back">
			<a href="personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">账户明细</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="touzhubox">
	<div class="cl "></div>
	<div class="keyongzhye">
		<div class="keyongzhyein">
			<div class="f22 l30">可用余额</div>
			<div class="f22 hong l30"><span class="meminfoMoney">0.00</span>元</div>
		</div>
	</div>
	<div class="keyongzhye">
		<div class="keyongzhyein" style="border-right:none;">
			<div class="f22 l30">账户余额</div>
			<div class="f22 hong l30"><span class="meminfoMoney">0.00</span>元</div>
		</div>
	</div>
	<div class="cl "></div>

	<div id="selectQueryType" class="touzhuqie" data-current="${isOprOnOff!='on' ? 'withdraw' : 'recharge'}">
		<!-- <span data-querytype="all" class="cur" style="width: 25%;">明细</span>  -->
		<!-- <span data-querytype="bonus" class="" style="width: 25%;">奖金派发</span> --> 
		<c:if test="${isOprOnOff=='on'}">
		<span data-querytype="recharge" class="cur" style="width: ${menuItemWidth}%;">充值记录</span> 
		</c:if>
		<span data-querytype="withdraw" class="${isOprOnOff!='on' ? 'cur' : ''}" style="width: ${menuItemWidth}%;">取款记录</span> 
		<c:if test="${isZrOnOff=='on'}">
		<span data-querytype="thirdTransfer" class="" style="width: ${menuItemWidth}%;">真人转款记录</span> 
		</c:if>
	</div>
	<div class="cl"></div>
	<div class="touzhusjbox" style="height: 6px;">
		<!-- 
		<div class="fl pl22 pt12">
			<div id="queryDatetime" class="touzhusj selectComponent" data-value="">
				<div class="fangsjbox">时间</div>
				<em class="iconfont icon-down downem"></em>
				<div class="sjxialabox">
					<ul>
						<li data-value="today">今天</li>
						<li data-value="yesterday">昨天</li>
						<li data-value="week">近一周</li>
						<li data-value="month">近30天</li>
						<li data-value="monthbefore">30天以前</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="fl pl22 pt12">
			<div id="queryCondition" class="touzhusj selectComponent" data-value="">
				<div class="fangsjbox">明细筛选</div>
				<em class="iconfont icon-down downem"></em>
				<div class="sjxialabox">
					<ul>
						<li data-value="init">处理中</li>
						<li data-value="suc">处理成功</li>
						<li data-value="err">处理失败</li>
					</ul>
				</div>
			</div>
		</div>
		 -->
	</div>
	<div class="cl"></div>
	<div id="recordContent" class="tzhuanqie-huan cur">
	</div>
	<div class="cl"></div>
	<div class="cl h80"></div>
	</div>
</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/account_details.js"></script>
</html>
<script type="text/html" id="recoredTemplate" style="display: none;">
		<div class="touzjilulbbox">
			<ul class="touzjilulbboxul">
				{#
					_.map(data, function(item){

						var statusTitle = "其他";
						switch(item.status){
						case 1:
							statusTitle = "处理中";
							break;
						case 2:
							statusTitle = "成功";
							break;
						case 3:
							statusTitle = "失败";
							break;
						case 4:
							statusTitle = "取消";
							break;
						case 5:
							statusTitle = "已过期";
							break;
						}

				#}
				<li>
					<a href="javascript: void(0);" class="" >
					<div class="touzjilulbboxul-sj">{{item.betdate}} <em class="shijianyuand"></em></div>
					
					<div class="fl">
						<div class="f22 l46">{{item.title}}</div>
						<div class="f18 ">{{item.bettime}}</div>
					</div>
					<div class="fr">
						<div class="fl" style="margin-right: 0px; padding-top: 12px; width: 160px;">
							<div class="ft18" style="text-align: right; font-size: 20px;">{{statusTitle}}</div>
							<div class="ft18" style="margin-top: 6px; text-align: right; font-size: 17px; color: #ff4444; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;" title="{{item.opDesc}}">{{item.opDesc}}</div>
						</div>
						<div class="fr l78 f18 {# if(item.money > 0){ print('hong');}#}" style="min-width: 100px; text-align: right;">{{item.money}}元</div>
					</div>
					
					<em class="rightmsj iconfont icon-right"></em>
					</a>
				</li>
				{#
					});
				#}
				<!-- 
				<li>
					<a href="javascript: void(0);" class="" >
					<div class="touzjilulbboxul-sj">06-09  <em class="shijianyuand"></em></div>
					
					<div class="fl">
						<div class="f22 l46">奖金发放</div>
						<div class="f18 ">03:12</div>
					</div>
					<div class="fr">
						<div class="l78 f22 hong">100元</div>
					</div>
					<em class="rightmsj iconfont icon-right"></em>
					</a>
				</li>
				 -->
			</ul>
		</div>
</script>
<%@include file="include/check_login.jsp" %>