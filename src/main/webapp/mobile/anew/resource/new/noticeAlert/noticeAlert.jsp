<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="${base }/mobile/anew/resource/new/noticeAlert/js/layer.js"></script>
<script>
$(function(){
	noticeAlert();
});

function noticeAlert(){
	var isShow = $.cookie('no_show_notice');
	if(!isShow){//不再显示
		noticeData();
	}
}

function showNoticeAlert(){
	var isShow = $.cookie('no_show_notice',false);
	noticeData();
}

function noticeData(){
	$.ajax({
		url : "${base}/getConfig/getArticle.do",
		data : {
			code : 19
		},
		type : "post",
		dataType : 'json',
		success : function(j) {
			if(localStorage.getItem("popModule") == 'hide'){
				if((new Date().getTime() - localStorage.getItem("popDate")) > 86400000){
					if (j.length > 0) {
						noticeFrame(j[0].title,j[0].content);
					}
				}
			}else{
				if (j.length > 0) {
					noticeFrame(j[0].title,j[0].content);
				}
			}
			
		}
	});
}

function noticeFrame(t,c){
	layer.open({
		  content: '<div style="overflow-x: auto;overflow-y: auto;width:280px;height:200px;">'+ c + '</div>' +'<input type ="button" value="今日不再显示" id="noPop" style="position:absolute;top:10px;right:7px;padding:3px 5px; border:none;"/>'
			+'<input type ="button" value="今日显示" id="yesPop" style="position:absolute;top:10px;right:7px;padding:3px 5px; border:none;display:none;background:red;color:#fff;boder-radius:3px;"/>'
		  ,title: [t, 'background-color: #cecece;text-align:left;']
		  ,anim: 'up'
		  ,style:'width: 300px; height: 305px;'
		  ,btn: ['我知道了'],
		  yes: function(index){
// 			    $.cookie('no_show_notice',true);
			    $('#show_notice_content').show();
			    layer.close(index);
			  }
		});
	$("#noPop").click(function(){
		 $(this).hide()
		 $("#yesPop").show()
		 localStorage.setItem("popModule",'hide');
       localStorage.setItem("popDate",new Date().getTime())
	})
	$("#yesPop").click(function(){
		$(this).hide()
		$("#noPop").show()
		 localStorage.setItem("popModule",'show');
	})
}
</script>