<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>${_title }</title>
<link href="${base }/mobile/anew/resource/new/css/ui-dialog.css" rel="stylesheet">
<link href="${base }/mobile/anew/resource/new/css/m.css" rel="stylesheet" type="text/css">
<link id="invite_style" type="text/css" rel="stylesheet" href="${base }/mobile/anew/resource/new/css/invite.css">
<script src="${base }/mobile/anew/resource/new/js/jquery-1.11.0.min.js"></script>
<script src="${base }/mobile/anew/resource/new/js/dialog-min.js"></script>
<script src="${base }/mobile/anew/resource/new/js/swipe.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base }/mobile/anew/resource/new/js/core.js" path="${base}"></script>
<script language="javascript">
//<img src="${base }/mobile/anew/resource/new/images/loading.gif" />
</script>
<style>
.addWrap{width:100%;background:#fff;margin:auto;position:relative;}
.addWrap .swipe{/*height:350px;*/overflow: hidden;visibility: hidden;position:relative;}
.addWrap .swipe-wrap{overflow:hidden;position:relative;}
.addWrap .swipe-wrap > div {float: left;width: 100%;position:relative;}

#position{padding:0;text-align:center;}
#position li{width:10px;height:10px;margin:0 3px;display:inline-block;-webkit-border-radius:5px;border-radius:5px;background-color:#AFAFAF;}
#position li.cur{background-color:#FF0000;}
</style>