<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<style>
		html,
		body {
			background-color: #efeff4;
		}
		p {
			text-indent: 22px;
		}
		span.mui-icon {
			font-size: 14px;
			color: #007aff;
			margin-left: -15px;
			padding-right: 10px;
		}
		.mui-off-canvas-left {
			color: #fff;
		}
		.title {
			margin: 35px 15px 10px;
		}
		.title+.content {
			margin: 10px 15px 35px;
			color: #bbb;
			text-indent: 1em;
			font-size: 14px;
			line-height: 24px;
		}
		input {
			color: #000;
		}
	</style>
<div id="offCanvasWrapper" class="mui-off-canvas-wrap mui-draggable">
	<!--侧滑菜单部分-->
	<aside id="offCanvasSide" class="mui-off-canvas-left">
		<div id="offCanvasSideScroll" class="mui-scroll-wrapper">
			<div class="mui-scroll">
				<div class="title">侧滑导航</div>
				<div class="content">
					这是可拖动式侧滑菜单示例，你可以在这里放置任何内容；关闭侧滑菜单有多种方式： 1.在手机屏幕任意位置向左拖动(drag)；2.点击本侧滑菜单页之外的任意位置; 3.点击如下红色按钮
					<span class="android-only">；4.Android手机按back键；5.Android手机按menu键
					</span>。
					<p style="margin: 10px 15px;">
						<button id="offCanvasHide" type="button" class="mui-btn mui-btn-danger mui-btn-block" style="padding: 5px 20px;">关闭侧滑菜单</button>
					</p>

				</div>
				<div class="title" style="margin-bottom: 25px;">侧滑列表示例</div>
				<ul class="mui-table-view mui-table-view-chevron mui-table-view-inverted">
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 1
						</a>
					</li>
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 2
						</a>
					</li>
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 3
						</a>
					</li>
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 4
						</a>
					</li>
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 5
						</a>
					</li>
					<li class="mui-table-view-cell">
						<a class="mui-navigate-right">
							Item 6
						</a>
					</li>
				</ul>
			</div>
		</div>
	</aside>
	<!--主界面部分-->
	<div class="mui-inner-wrap">
		<header class="mui-bar mui-bar-nav">
			<a href="#offCanvasSide" class="mui-icon mui-action-menu mui-icon-bars mui-pull-left"></a>
			<a class="mui-action-back mui-btn mui-btn-link mui-pull-right">关闭</a>
			<h1 class="mui-title">${_title }</h1>
		</header>
		<div id="offCanvasContentScroll" class="mui-content mui-scroll-wrapper">
			<div class="mui-scroll">
				<jsp:include page="lunbo.jsp"/>
				<!-- 
				<form class="mui-input-group" style="margin-bottom: 15px;">
					<div class="mui-input-row mui-radio">
						<label>主界面移动、菜单不动</label>
						<input name="style" type="radio" checked="" value="main-move">
					</div>
					<div class="mui-input-row mui-radio">
						<label>缩放式侧滑（类手机QQ）</label>
						<input name="style" type="radio" value="main-move-scalable">
					</div>
				</form>
 				-->
			</div>
		</div>
		<!-- off-canvas backdrop -->
		<div class="mui-off-canvas-backdrop"></div>
	</div>
</div>
<script>
	mui.init();
	 //侧滑容器父节点
	var offCanvasWrapper = mui('#offCanvasWrapper');
	 //主界面容器
	var offCanvasInner = offCanvasWrapper[0].querySelector('.mui-inner-wrap');
	 //菜单容器
	var offCanvasSide = document.getElementById("offCanvasSide");
	 //移动效果是否为整体移动
	var moveTogether = false;
	 //侧滑容器的class列表，增加.mui-slide-in即可实现菜单移动、主界面不动的效果；
	var classList = offCanvasWrapper[0].classList;
	 //变换侧滑动画移动效果；
	mui('.mui-input-group').on('change', 'input', function() {
		if (this.checked) {
			offCanvasSide.classList.remove('mui-transitioning');
			offCanvasSide.setAttribute('style', '');
			classList.remove('mui-slide-in');
			classList.remove('mui-scalable');
			switch (this.value) {
				case 'main-move':
					if (moveTogether) {
						//仅主内容滑动时，侧滑菜单在off-canvas-wrap内，和主界面并列
						offCanvasWrapper[0].insertBefore(offCanvasSide, offCanvasWrapper[0].firstElementChild);
					}
					break;
				case 'all-move':
					moveTogether = true;
					//整体滑动时，侧滑菜单在inner-wrap内
					offCanvasInner.insertBefore(offCanvasSide, offCanvasInner.firstElementChild);
					break;
			}
			offCanvasWrapper.offCanvas().refresh();
		}
	});
	 //菜单界面，‘关闭侧滑菜单’按钮的点击事件
	document.getElementById('offCanvasHide').addEventListener('tap', function() {
		offCanvasWrapper.offCanvas('close');
	});
	 //主界面和侧滑菜单界面均支持区域滚动；
	mui('#offCanvasSideScroll').scroll();
	mui('#offCanvasContentScroll').scroll();
	 //实现ios平台原生侧滑关闭页面；
	if (mui.os.plus && mui.os.ios) {
		mui.plusReady(function() { //5+ iOS暂时无法屏蔽popGesture时传递touch事件，故该demo直接屏蔽popGesture功能
			plus.webview.currentWebview().setStyle({
				'popGesture': 'none'
			});
		});
	}
</script>