<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="foot">
  <ul>
    <li><a href="${base }/mobile/personal_center.do" class="ico_u">会员中心</a></li>
    <c:if test="${isCpOnOff=='on'}">
    <li><a href="${base }/mobile/game_center.do" class="ico_money"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></a></li>
    <li><a href="${base }/mobile/draw_notice.do" class="ico_kj">开奖公告</a></li>
    </c:if>
    <li style="border-right:none;"><a href="${kfUrl }" target="_blank" class="ico_kf">在线客服</a></li>
  </ul>
  <div class="fx"></div>
</div>