
$.dialog = function(options) {
	options = options || {};
	var title = options.title || "温馨提示";
	var text = options.content || "";
	var callbackOk = options.ok || function() {};
	var okValue = options.okValue || "确定";
	var callbackCancel = options.canel || function() {};
	var canelValue = options.canelValue || "取消";

	/*
	if (typeof title === 'function') {
		callbackCancel = arguments[2];
		callbackOk = arguments[1];
		title = undefined;
	}
	*/

	return $.modal({
		text : text || '',
		title : typeof title === 'undefined' ? "" : title,
		buttons : [ {
			text : canelValue,
			onClick : callbackCancel
		}, {
			text : okValue,
			// bold : true,
			onClick : callbackOk
		} ]
	});
};