
"use strict";
FastClick.attach(document.body);
var onClickEventString;
//if (/(iPhone|iPad|iPod|iOS)/i.phone(navigator.userAgent)) {
//	// onClickEventString = "touchend click";
//	onClickEventString = "touchend";
//} else if(/(UC)/i.phone(navigator.userAgent)){
//	onClickEventString = "touchend";
//} else {
//	onClickEventString = "click";
//}
onClickEventString = "click";
function ViewModel (){
	return {
		Info : {}, Function : {}, Component : {}, Action : {}
	};
}

function ActionModel (){
	var model = new ViewModel();
	return $.extend({
		
	}, model);
}

window.$$ = window.Application = $.extend({ActionInstance: {}}, new ViewModel());

$$.Action.Index = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
				switch (identification) {
				case "Android":
				case "IOS":
					$(".contact").hide();
					break;
				default:
					break;
				};

				/* 最新公告 */
				$(function(){
					$._ajax(stationUrl + "/system/notice.do", function (status, message, data, result) {
						var $noticeContent = $("#noticeContent");
						if(data.length){
							var $div = $("<div>");
							$div.append(data);
							$noticeContent.html($div.text());
						} else {
							$noticeContent.html("");
						}
					});
				});

				/* 自动登录 */
				$(function(){
					var logined = !1;
					if($("#logined").length > 0){
						logined = !0;
					}

					var loadSame = function(logined, money){
						var tplHtml = $("#loginStatusBarTemplate").html();
						var $loginStatusBar = $("#loginStatusBar").empty();
						$loginStatusBar.html(_.template(tplHtml)({logined: logined, money: money}));
					}

					if(logined == !1){
						var mobile_alk_chk = $.cookie('mobile_alk_chk');
						if(mobile_alk_chk != null && mobile_alk_chk){
							$.ajax({
								type : "POST",
								url : stationUrl + "/alklogin.do",
								data : {
									_alk_chk: mobile_alk_chk
								},
								success : function(result) {
									if(result.code == 200){
										loadSame(!0, result.data);
									}
								}
							});
						}
					}
				});

				/* 在线客服 */
				$(function(){
					var iBrowserOpen = function (customerServiceUrl){
						var title = "客服";
						var toUrl = customerServiceUrl;
						var backUrl = location.href;
						location.href = stationUrl + "/iCrowser.do?title=" + Base64.encode(title) + "&toUrl=" + Base64.encode(toUrl) + "&backUrl=" + Base64.encode(backUrl) + "&actionMethod=get";
					}
					var toCustomerServiceFunc = function (customerServiceUrl){
						iBrowserOpen(customerServiceUrl);
					}

					switch (identification) {
					case "Android":
					case "IOS":
						toCustomerServiceFunc = function (customerServiceUrl){
							plus.runtime.openURL(customerServiceUrl, function (){
								iBrowserOpen(customerServiceUrl);
							});
						}
						break;
					default:
						break;
					};

					$._ajax(stationUrl + "/system/customerServiceUrlLink.do", function (status, message, data, result) {
						if (data) {
							if (data.indexOf("http://") < 0 && data.indexOf("https://") < 0) {
								data = "http://" + data;
							}
						}
						var $customerServiceUrlLink = $("#customerServiceUrlLink");
						$customerServiceUrlLink.click(function(){
							toCustomerServiceFunc(data);
						});
					});
				});
			},
			destroy: function(){
				
			}
		};
	}
};

$$.Action.GameCenter = {
	name: "",
	LotteryCountDown: function(gameType){
		var g = {};
		g.qihaoInfo = {};
		g.stop = function () {
			clearTimeout(this.timeout);
			_.isObject(this.xhr) && this.xhr.abort && this.xhr.abort();
			return this
		};
		g.update = g.start = function () {
			var tthis = this;
			this.$round = $("#bpRound_" + gameType);
			this.$countDownMsg = $(".bpCountDown_" + gameType);
			var c = this;
			this.xhr = $._ajax({url: stationUrl + "/lottery/getCountDown.do", data: {lotCode: gameType}}, function (b, e, f, g) {
				if(b) {
					if(_.isEmpty(f)){
					} else {
						(c.time.serverTime = f.serverTime, c.$round.html(f.qiHao ? f.qiHao : "无期数"), tthis.qihaoInfo.now = {
							startBet: f.startbet || 0,
							stopBet: f.activeTime || 0,
							round: f.qiHao || 0,
							lottyID: f.lottyid || 0
						}, c.tick())
					}
				} else {
				}
			})
		};
		g.tick = function () {
			clearTimeout(this.timeout);
			var tthis = this,
				c = this.time.getCountDown(this.qihaoInfo.now.startBet),
				e = this.time.getCountDown(this.qihaoInfo.now.stopBet);
			if(0 <= c.seconds) {
				this.$round.html(this.qihaoInfo.now.round);
				var h1 = c.h.length > 1 ? c.h.substr(0,1) : 0;
				var h2 = c.h.substr(c.h.length - 1,1);
				var m1 = c.m.length > 1 ? c.m.substr(0,1) : 0;
				var m2 = c.m.substr(c.m.length - 1,1);
				var s1 = c.s.length > 1 ? c.s.substr(0,1) : 0;
				var s2 = c.s.substr(c.s.length - 1,1);
				this.$countDownMsg.html(h1 + "" + h2 + ":" + m1 + "" + m2 + ":" + s1 + "" + s2);
				this.timeout = setTimeout(function () {
					tthis.tick.apply(tthis)
				}, c.ms)
			} else {
				if(0 <= e.seconds){
					this.$round.html(this.qihaoInfo.now.round);
					var h1 = e.h.length > 1 ? e.h.substr(0,1) : 0;
					var h2 = e.h.substr(e.h.length - 1,1);
					var m1 = e.m.length > 1 ? e.m.substr(0,1) : 0;
					var m2 = e.m.substr(e.m.length - 1,1);
					var s1 = e.s.length > 1 ? e.s.substr(0,1) : 0;
					var s2 = e.s.substr(e.s.length - 1,1);
					this.$countDownMsg.html(h1 + "" + h2 + ":" + m1 + "" + m2 + ":" + s1 + "" + s2);
					this.timeout = setTimeout(function () {
						tthis.tick.apply(tthis)
					}, e.ms);
				} else {
					g.stop();
					g.start();
					$$.Action.GameCenter.getLastResult(gameType);
				}
			}
		}
		g.time = {
			getOffset : function () {
				var h = eval(sessionStorage.getItem("timeOffset"));
				return function () {
					(null === h || isNaN(h)) && 
					$._ajax({url: stationUrl + "/system/getServerTime.do", async: !1}, function (k, m, r) {
						k && (h = new Date - new Date(r.serverTime.replace(/-/g, "/")), sessionStorage.setItem("timeOffset", h))
					});
					return h
				}
			}(),
			getServerTime : function () {
				return new Date(new Date - this.getOffset())
			},
			getCountDown : function (h) {
				"string" == typeof h && (h = new Date(h.replace(/-/g, "/")));
				if (!(h instanceof Date) || isNaN(h.getTime())) return !1;
				h = (new Date(h - this.getServerTime())).getTime() / 1E3;
				return {
					h: ("0" + Math.floor(Math.abs(h) / 3600)).slice(-2),
					m: ("0" + Math.floor(Math.abs(h) % 3600 / 60)).slice(-2),
					s: ("0" + Math.floor(Math.abs(h) % 60)).slice(-2),
					ms: Math.floor(h % 1 * 1E3),
					seconds: h
				}
			}
		}

		return g;
	},
	getLastResult: function(lotCode){
		var thisAction = this;
		$._ajax({url: stationUrl + "/lottery/lastResult.do", data: {code: lotCode}}, function (status, message, rdata, result) {
			if(rdata){
				$("#last_period_" + lotCode).html(rdata.qiHao);
				var haoma = rdata.haoMa;
				if(haoma != "" && haoma.substr(0, 1) != "?"){
					var haoMaList = haoma.split(",");
					$("#last_haoma_" + lotCode).html(haoMaList.join(" "));
				} else {
					$("#last_haoma_" + lotCode).html('等待开奖');
					setTimeout(function(){thisAction.getLastResult(lotCode);}, 1000 * 36);
				}
			}
		});
	},
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
				var $lotter_lis = $(".lottery-list-erect li[lotcode]");
				$.each($lotter_lis, function(index, item){
					var $lotter_li = $(item);
					var lotCode = $lotter_li.attr("lotcode");
					thisAction.getLastResult(lotCode);
					var lcd = new thisAction.LotteryCountDown(lotCode);
					lcd.start();
				});

				$(".lott-menu ul li").on("click", function(){
					$(".lott-menu ul li").removeClass("cur");
					$(this).addClass("cur");
					$("#lottypetype").val($(this).data("cat")).trigger("change");
				});
				$("#lottypetype").on("change", function(){
					// console.log($(this).val());
					// reloadView("" + $(this).val());
					var lottypetype = $(this).val();
					switch(lottypetype + ""){
					case "0":
						$(".lotteryContent li").show();
						break;
					case "1":
						$(".lotteryContent li").hide();
						$(".lotteryContent li.game_category_1").show();
						break;
					case "2":
						$(".lotteryContent li").hide();
						$(".lotteryContent li.game_category_2").show();
						break;
					}
				});

				$(".ui-toolbar-right a").on("click", function(){
					var val = $(this).attr("val");
					if(val == "1"){
						$(".ui-toolbar-right").removeClass("ui-head");
						$(".lottery-list").show();
						$(".lottery-list-erect").hide();
					} else {
						$(".ui-toolbar-right").addClass("ui-head");
						$(".lottery-list").hide();
						$(".lottery-list-erect").show();
					}
				});
			},
			destroy: function(){
				
			}
		};
	}
};

$$.Action.DrawNotice = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
			},
			destroy: function(){
				
			}
		};
	}
};

$$.Action.PersonalCenter = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
				//实时获取站内信
				var message = function(){
					var t1;
					function message() {
						$.ajax({
							url : baseUrl + "/lottery/message.do",
							success : function(j) {
								$("#zhanneixin").html(j);
							}
						});
					}
					return {
						start : function() {
							var a = this;
							clearTimeout(t1);
							message();
							t1 = setTimeout(function(){a.start()},Math.floor(Math.random() * 1000 + 1) + 5000);
						},
						stop  : function() {
							clearTimeout(t1);
						}
					};
				}();

				message.start();

				$(".close-tan").click(function() {
					$(this).parents(".tanchuceng").fadeOut(200)
				})
				$(".denglumima-btn").click(function(){
					$(".denglumima-tan").fadeIn(200)
				});
				$(".qukanmima-btn").click(function(){
					$(".qukuanmian-tan").fadeIn(200)
				});
				$("#passwordDialogOkBtn").on("click", function(){
					$.ajax({
						type : "POST",
						url : stationUrl + "/member/uppass.do",
						data : {
							oldpassword: $("#oldpassword").val(),
							newpassword: $("#newpassword").val(),
							confirmpassword: $("#confirmpass").val()
						},
						success : function(result) {
							if(result.success == false){
								$.alert(result.msg);
							} else if(result.success == true){
								$(".denglumima-tan").hide();
								$.alert("修改成功");
							}
						}
					});
				});
				$("#cashpassDialogOkBtn").on("click", function(){
					$.ajax({
						type : "POST",
						url : stationUrl + "/member/upcashpass.do",
						data : {
							oldpassword: $("#oldcashpasswd").val(),
							newpassword: $("#newcashpasswd").val(),
							confirmpassword: $("#confirmcashpw").val()
						},
						success : function(result) {
							if(result.success == false){
								$.alert(result.msg);
							} else if(result.success == true){
								$(".qukuanmian-tan").hide();
								$.alert("修改成功");
							}
						}
					});
				});

				$("#logoutBtn").on("click", function(){
					$.cookie('mobile_alk_chk', '', {expires: -1, path: '/'});
					location.href = stationUrl + "/logout.do";
				});

//				$._ajax(stationUrl + "/member/meminfo.do", function (status, message, data, result) {
//					$("#meminfoMoney").html(data.money);
//				});
			
			},
			destroy: function(){
				
			}
		};
	}
};

$$.Action.DoPickNumber = {
	name: "",
	firstLoad: true,
	lotCode: "",
	getInstance: function(){
		// {Info : {}, Function : {}, Component : {}, Action : {}}
		var baseAction = new ActionModel();
		var thisAction = this;

		baseAction.Info = {
			playType : 2, // 0
			betType : 1, // 0
			subid : 12345, // 0
			round : {
				now : {
					startBet : 0,
					stopBet : 0,
					round : 0,
					lottyID : 0
				},
				next : {
					startBet : 0,
					stopBet : 0,
					round : 0,
					lottyID : 0
				}
			},
			betAmount : 0,
			ballNum : null,
			ballWrongNum : null,
			prevPlayInfo : {
				gameType : 0,
				betType : 0,
				subid : 0
			}
		}

		return $.extend(baseAction, {
			init: function (){
				thisAction.lotCode = $("#lotCode").val();

				this.ballNumSelectInit();
				if(thisAction.firstLoad){
					this.anyBtnInit();
					this.playTypeSelectInit();
					this.betBtnInit();
					baseAction.Function.countDown = new this.LotteryCountDown(thisAction.lotCode);
					baseAction.Function.countDown.start();
				}
				baseAction.Component.footer = new this.FooterOperation();
				baseAction.Component.ball = new this.BallOperation();
				baseAction.Function.betClassParser = new this.BetClassParser();

				this.gameMenuInit();
				this.haomaHistoryInit();
				this.orderCountShowInit();

				thisAction.firstLoad = false;

				$(document).off("click.shangqixiala").on("click.shangqixiala", function() {
					$(".shangqixiala").hide();
				})
				$(".bett-head").off("click.toggle").on("click.toggle", function(e) {
					$$.Function.stopPropagation(e);
					$(".beet-rig").toggle();
				});
				$(document).off("click.beet-rig.toggle").on("click.beet-rig.toggle", function(e) {
					$(".beet-rig").hide();
				});
			},
			reinit: function (){
				var buyCartCount = _.keys(thisAction.buyCart.get(thisAction.lotCode)).length;
				if(buyCartCount){
					$("#orderCountShow").text(buyCartCount);
					$("#goBuyCartBtn").show();
				} else {
					$("#orderCountShow").text(0);
					$("#goBuyCartBtn").hide();
				}
			},
			destroy: function(){
				
			},
			BallOperation: function() {
				var ball = {};
				ball.get = function() {
					var ballSelectArr = [];
					$("#gameContent .numRow").each(function(c, d) {
						var ballSelectItem = [];
						$.map($(this).find(".ballNum.cur"), function(b) {
							(b = $(b).attr("data-num")) && ballSelectItem.push(b)
						});
						ballSelectArr.push(ballSelectItem)
					});
					return ballSelectArr;
				};
				ball.calc = function() { // 计算注数
					var b = this.get(), c = thisAction.ballCalc.calc(baseAction.Info.playType, baseAction.Info.betType, baseAction.Info.subid, b);
					baseAction.Info.betAmount = c;
					baseAction.Info.ballNum = b;
					return c
				};
				ball.reset = function() {
					$("#gameContent input.ballNum").prop("checked", false);
					$("#gameContent .ballNum.cur").removeClass("cur");
					$("#gameContent textarea").val("");
					$("#beishuInput").val(1).trigger("change");
					baseAction.Info.betAmount = 0;
					baseAction.Info.ballNum = null;
					baseAction.Info.ballWrongNum = null;
					baseAction.Component.footer.betCount(0);
				};
				return ball;
			},
			FooterOperation: function () {
				var footer = {};
				footer.showRandomBtn = function (c) {
					c ? $("#bpRandom").show() : $("#bpRandom").hide();
					return this
				};
				footer.betCount = function (b) {
					var beishu = $("#beishuInput").val();
					var cashUnit = $("#cashUnit").val();
					if(!cashUnit){
						cashUnit = 1;
					}
					$(".bpBetCount").text(b), $(".orderTotalCost").text(2 * b * beishu / cashUnit);
				};
				return footer;
			},
			ballNumSelectInit: function(){
				/*
				var d = baseAction.Info,
					b = baseAction.Function,
					f = baseAction.Component;
					*/
				var ballNumClickFunc = function (c) {
					var $this = $(this);
					try{
						if (2 == baseAction.Info.playType && 6 == baseAction.Info.betType) {
							$this.hasClass("cur") ? $this.removeClass("cur") : (baseAction.Function.ball.reset(), $this.addClass("cur"));
						} else if (5 == baseAction.Info.playType && 71 == baseAction.Info.betType || 4 == baseAction.Info.playType && -1 != [53, 54].indexOf(baseAction.Info.betType)) {
							if (c = 0, 4 == baseAction.Info.playType ? c = 53 == baseAction.Info.betType ? 2 : 1 : 5 == baseAction.Info.playType && (c = -1 != [812, 845].indexOf(baseAction.Info.subid) ? 1 : -1 != [913, 924, 935].indexOf(baseAction.Info.subid) ? 2 : baseAction.Info.subid),
									$this.hasClass("cur")) {
								$this.removeClass("cur").removeAttr("data-selectTime");
							} else {
								var e = $this.attr("data-num");
								$this.hasClass("cur") ? $this.removeClass("cur").removeAttr("data-selectTime") : false;
								$this.attr("data-selectTime", (new Date).getTime());
								$this.addClass("cur");
								var k = {};
								$("#gameContent .numRow").eq(0).find(".cur[data-selectTime]").each(function (b, c) {
									k[$this.attr("data-selectTime")] = $this
								});
								e = _.keys(k).sort();
								$.map(_.first(e, e.length - c), function (b) {
									k[b].removeClass("cur").removeAttr("data-selectTime")
								})
							}
						} else if(2 == baseAction.Info.playType && -1 != [7, 8].indexOf(baseAction.Info.betType) && -1 != [30, 40, 50].indexOf(baseAction.Info.subid)){
							$this.toggleClass("cur");
						} else {
							$this.toggleClass("cur");
						};
						2 == baseAction.Info.playType && -1 != [14, 15, 17, 18].indexOf(baseAction.Info.betType) && -1 != [20, 30, 40].indexOf(baseAction.Info.subid) ? $("#gameContent textarea").trigger("keyup") : baseAction.Component.footer.betCount(baseAction.Component.ball.calc());
					} catch (e){
						$this.toggleClass("cur");
						console.log(e.name + ": " + e.message);
					}
				}
				if(onClickEventString == "touchend"){
					// $("#gameContent").on("click", "input.ballNum", ballNumClickFunc);
					// $("#gameContent").on(onClickEventString, "span.ballNum", ballNumClickFunc);

					// $("#gameContent").delegate("input.ballNum", "click", ballNumClickFunc);
					// $("#gameContent").delegate("span.ballNum", onClickEventString, ballNumClickFunc);

					$("#gameContent").off("click.ballNumSelectInit", "input.ballNum", ballNumClickFunc).on("click.ballNumSelectInit", "input.ballNum", ballNumClickFunc);
					$("#gameContent").off(onClickEventString + ".ballNumSelectInit", "span.ballNum", ballNumClickFunc).on(onClickEventString + ".ballNumSelectInit", "span.ballNum", ballNumClickFunc);
				} else {
					// $("#gameContent").on(onClickEventString, ".ballNum", ballNumClickFunc);

					// $("#gameContent").delegate(".ballNum", onClickEventString, ballNumClickFunc);

					$("#gameContent").off(onClickEventString + ".ballNumSelectInit", ".ballNum", ballNumClickFunc).on(onClickEventString + ".ballNumSelectInit", ".ballNum", ballNumClickFunc);
				}

				$("#gameContent").off(onClickEventString, ".numRow [data-fastselect]").on(onClickEventString, ".numRow [data-fastselect]", function(){
					var $this = $(this);
					var actionType = $this.data("fastselect");

					var $numRow = $this.parents(".numRow");
					var $ballNum = $numRow.find(".ballNum");
					var length = $ballNum.length;

					$ballNum.removeClass("cur");
					console.info(actionType);
					switch(actionType){
					case "all":
						$ballNum.trigger(onClickEventString);
						break;
					case "large":
						$numRow.find(".ballNum:gt(" + parseInt((length / 2) - 1) + ")").trigger(onClickEventString);
						break;
					case "less":
						$numRow.find(".ballNum:lt(" + parseInt(length / 2) + ")").trigger(onClickEventString);
						break;
					case "odd":
						if(thisAction.lotCode == "BJSC" || thisAction.lotCode == "XYFT"  || thisAction.lotCode == "LXYFT" || thisAction.lotCode == "GD11X5" || thisAction.lotCode == "SD11X5" || thisAction.lotCode == "JX11X5" || thisAction.lotCode == "SH11X5" || thisAction.lotCode == "GX11X5" || thisAction.lotCode == "HBK3" ||
								thisAction.lotCode == "HEBK3" || thisAction.lotCode == "JSSB3" || thisAction.lotCode == "BJK3" || thisAction.lotCode == "JXK3" || thisAction.lotCode == "AHK3" || thisAction.lotCode == "GSK3" ||
								thisAction.lotCode == "SHHK3" || thisAction.lotCode == "FFK3" || thisAction.lotCode == "WFK3" || thisAction.lotCode == "JPK3" || thisAction.lotCode == "KRK3" || thisAction.lotCode == "HKK3" || thisAction.lotCode == "AMK3" || thisAction.lotCode == "GXK3" || thisAction.lotCode == "SFSC"){
							$numRow.find(".ballNum:even").trigger(onClickEventString);
						} else {
							$numRow.find(".ballNum:odd").trigger(onClickEventString);
						}
						break;
					case "even":
						if(thisAction.lotCode == "BJSC" || thisAction.lotCode == "XYFT"  || thisAction.lotCode == "LXYFT" || thisAction.lotCode == "GD11X5" || thisAction.lotCode == "SD11X5" || thisAction.lotCode == "JX11X5" || thisAction.lotCode == "SH11X5" || thisAction.lotCode == "GX11X5" || thisAction.lotCode == "HBK3" ||
								thisAction.lotCode == "HEBK3" || thisAction.lotCode == "JSSB3" || thisAction.lotCode == "BJK3" || thisAction.lotCode == "JXK3" || thisAction.lotCode == "AHK3" || thisAction.lotCode == "GSK3" ||
								thisAction.lotCode == "SHHK3" || thisAction.lotCode == "FFK3" || thisAction.lotCode == "WFK3" || thisAction.lotCode == "JPK3" || thisAction.lotCode == "KRK3" || thisAction.lotCode == "HKK3" || thisAction.lotCode == "AMK3" || thisAction.lotCode == "GXK3" || thisAction.lotCode == "SFSC"){
							$numRow.find(".ballNum:odd").trigger(onClickEventString);
						} else {
							$numRow.find(".ballNum:even").trigger(onClickEventString);
						}
						break;
					case "clear":
						break;
					}
				});
			},
			anyBtnInit: function(){
				/*
				var d = baseAction.Info,
					b = baseAction.Function;
					*/
				$("body").delegate("#bpRandom", onClickEventString, function () {
					baseAction.Component.ball.reset();
					var c = thisAction.randomNum.gen(baseAction.Info.playType, baseAction.Info.betType, baseAction.Info.subid);
					var e = $("#gameContent").find(".numRow");
					$.each(c, function (b, c) {
						$.map(c, function (c) {
							if(onClickEventString == "touchend"){
								e.eq(b).find('[data-num="' + c + '"].ballNum').trigger(onClickEventString).trigger("click");
							} else {
								e.eq(b).find('[data-num="' + c + '"].ballNum').trigger(onClickEventString);
							}
						});
					});
				});
			
				$("body").delegate("#beishuInput", "change", function(){
					baseAction.Component.footer.betCount(baseAction.Component.ball.calc());
				}).delegate("#beishuInput", "keyup", function(){
					var thisvalue = $(this).val();
					if(thisvalue && !isNaN(thisvalue)){
						thisvalue = thisvalue < 1 ? 1 : thisvalue;
					}else{
						thisvalue = "";
					}
					$(this).val(thisvalue).trigger("change");
				}).delegate("#beishuInput", "blur", function(){
					var thisvalue = $(this).val();
					if(!thisvalue || isNaN(thisvalue)){
						thisvalue = 1;
					}
					$(this).val(thisvalue).trigger("change");
				});
				$("body").delegate("#cashUnit", "change", function(){
					// var chowArr = {1: "元", 10: "角", 100: "分"};
					// $(".cashUnitCnShow").text(chowArr[$(this).val()]);
					baseAction.Component.footer.betCount(baseAction.Component.ball.calc());
				});
				$("body").delegate(".jianbtn.btnjiaj", onClickEventString, function(){
					var val = $(this).siblings(".jiajianinpt").val();
					val++;
					$(this).siblings(".jiajianinpt").val(val).trigger("change");
				});
				$("body").delegate(".jiabtn.btnjiaj", onClickEventString, function(){
					var val = $(this).siblings(".jiajianinpt").val();
					if(val >1){
						val--;
						$(this).siblings(".jiajianinpt").val(val).trigger("change");
					}
				});

				$("body").delegate(".money-unit", onClickEventString, function(){
					$(".money-unit").removeClass("acitve");
					$(this).addClass("acitve");
					$("#cashUnit").val($(this).data("unit")).trigger("change");
				});
			},
			playTypeSelectInit: function(){
				var d = baseAction.Info,
					b = baseAction.Function,
					f = baseAction.Component;
				$("body").delegate("#bpBetTypeSel", "change", function () {
					var c = b.betClassParser.getSubClass($(this).data("select"));
					if (0 == c) $.dialog({
						title: "系统讯息",
						content: "无法取得玩法资料，请再试一次",
						ok: function () {
							
						}, okValue: "确定"
					});
					else {
						var playCodeFilterArr = ["q2zx_ds", "h2zx_ds", "q3zx_ds", "z3zx_ds", "h3zx_ds", "q4zx_ds", "h4zx_ds", "wxzx_ds", "zhx_ds", "", "", "", "", "", "", "", ""];
						var e = _.template('<span value="{{bettype}}.{{subid}}" data-betType="{{bettype}}" data-subid="{{subid}}" data-groupId="{{groupId}}" data-playId="{{playId}}" data-playCode="{{playCode}}" data-lotType="{{lotType}}">{{name}}</span> '),
							f = "";
						$.map(c, function (b) {
							if($.inArray(b.playCode, playCodeFilterArr) < 0){
								f += e(b);
							}
						});
						c = $(f);
						$("#bpSubidSel").html(c);
			
						$(".xialaitem span").click(function(){
							$(this).addClass("cur").siblings().removeClass("cur");

							var oWidth = document.documentElement.clientWidth || document.body.clientWidth;
							var oHeight = document.documentElement.clientHeight || document.body.clientHeight;

							var sc = $(".xialabox").scrollLeft();
							var sl = $(this).offset().left;
							var nn = $(this).width();

							var xm = (oWidth - nn) / 2;
							var chaz = sl - xm;
							var mm = sc + chaz;
			
							$(".xialabox").animate({
								scrollLeft: mm
							}, 600);
						})

						c.on(onClickEventString, function(){
							$(this).addClass("cur").siblings().removeClass("cur");
							$("#bpSubidSel").data("select", $(this).attr("value")).trigger("change");
						});
			
						// d.prevPlayInfo.gameType == d.gameType ? c.filter('[data-betType="' + d.prevPlayInfo.betType + '"][data-subid="' + d.prevPlayInfo.subid + '"]').trigger("click") : c.eq(0).trigger("click");
						c.eq(0).trigger(onClickEventString);
					}
				});
				$("body").delegate("#bpSubidSel", "change", function () {
					d.betType = 1 * $(this).children("span.cur").attr("data-betType");
					d.subid = 1 * $(this).children("span.cur").attr("data-subid");
					d.groupId = 1 * $(this).children("span.cur").attr("data-groupId");
					d.playId = 1 * $(this).children("span.cur").attr("data-playId");
					d.playCode = $(this).children("span.cur").attr("data-playCode");
					d.lotType = 1 * $(this).children("span.cur").attr("data-lotType");
					d.prevPlayInfo.betType = d.betType;
					d.prevPlayInfo.subid = d.subid;
					d.prevPlayInfo.groupId = d.groupId;
					d.prevPlayInfo.playId = d.playId;
					d.prevPlayInfo.playCode = d.playCode;
					d.prevPlayInfo.lotType = d.lotType;
					d.prevPlayInfo.gameType = thisAction.lotCode;
			
					var $gameContent = $("#gameContent");
					var resurce = $.getStrResouce(stationUrl + "/template/betPage/" + (d.lotType == 1 ? 2 : d.lotType) + "." + d.playCode + ".html?v=1121");
					$gameContent.html(resurce);

					$("#bonusOdds").html("加载中..");
					$("#rakeback").html("加载中..");
					$._ajax({url: stationUrl + "/lottery/getPlayInfo.do", data: {playId: d.playId}, async: !0}, function (k, m, r, aaaa) {
						if(k){
							$("#playDetailDesc").html(r.detailDesc);
							$("#playWinExample").html(r.winExample);
							$("#bonusOdds").html(r.bonusOdds ? r.bonusOdds : 0);
							if(r.showRakeback){
								$("#rakeback").html(r.rakeback ? r.rakeback : 0);
								$(".showRakeback").show();
							} else {
								$(".showRakeback").hide();
							}
						}
					});

					// f.calcView();
					f.ball.reset();
					d.betAmount = 0;
					d.ballNum = null;
					d.ballWrongNum = null;
					f.footer.showRandomBtn(!$("#gameContent").has("textarea").length);
				});
			}, 
			gameMenuInit: function(){
				var b = baseAction.Info,
					f = baseAction.Function;
				var thisGameMenu = gamemenu[thisAction.lotCode].gamemenu;
				if (thisGameMenu.length) {
					baseAction.Info.playType = thisAction.GameDB.gameTypeToPlayType(thisAction.lotCode);
					f.betClassParser.setClass(thisGameMenu);
					var e = f.betClassParser.getClass();
					var playGroupFilterArr = ["ethdx", "", "", "", "", ""];
					var l = _.template('<span data-value="{{id}}" data-playgroupcode="{{playGroupCode}}">{{title}}</span> '),
						m = "";
					$.map(e, function (b) {
						if($.inArray(b.playGroupCode, playGroupFilterArr) < 0){
							m += l(b)
						}
					});
					e = $(m);
					$("#bpBetTypeSel").html(e);

					$(".tabqiein span").click(function(){
						$(this).addClass("cur").siblings().removeClass("cur");

						var oWidth = document.documentElement.clientWidth || document.body.clientWidth;
						var oHeight = document.documentElement.clientHeight || document.body.clientHeight;

						var sc = $(".tabqie").scrollLeft();
						var sl = $(this).offset().left;
						var nn = $(this).width();

						var xm = (oWidth - nn) / 2;
						var chaz = sl - xm;
						var mm = sc + chaz;

						$(".tabqie").animate({
							scrollLeft: mm
						}, 600);
					})

					b.prevPlayInfo.gameType == thisAction.lotCode && (h = f.betClassParser.find(b.prevPlayInfo.betType, b.prevPlayInfo.subid), e.filter('[data-value="' + h + '"]').trigger(onClickEventString));

					e.on(onClickEventString, function(){
						$(this).addClass("cur").siblings().removeClass("cur");
						$("#bpBetTypeSel").data("select", $(this).data("value")).trigger("change");
					});
					e.eq(0).trigger(onClickEventString);
				} else {
					$.dialog({
						title: "系统讯息",
						content: "此彩类未开放任何玩法",
						ok: function () {
							window.history.go(-1);
						}, okValue: "确定"
					})
				}
			},
			betBtnInit: function(){
				// var $bpBetBtn = $("#bpBetBtn"), $pushBetListBtn = $("#pushBetListBtn"), $resetSelectBtn = $("#resetSelectBtn");

				var bpBetFunc = function () {
					// window.location.href = "do_bet.do";
					var betString = thisAction.buyCart.getBetString(thisAction.lotCode);

					if(betString.length){
						$.router.loadPage("#buyCartPage");
					} else {
						$.alert("请先添加正确的投注号码！");
					}
				}
				var pushBetListFunc = function(){
					var e = function () {
						// var ballNum = baseAction.Component.ball.get();
						var betCount = baseAction.Component.ball.calc();
						// View.footer.betCount(betCount);

						if(betCount){
							var unitCash = 2; // 单注价格
							var oddsWaterIdx = 0; // what?
							var odds = 0; // 赔率?
							var beishu = $("#beishuInput").val();
							var cashUnit = $("#cashUnit").val(); // 价格模式 1 - 元， 10 - 角， 100 - 分， 1E3 - 厘
							try{
								thisAction.buyCart.add(thisAction.lotCode, baseAction.Info.betType, baseAction.Info.subid, unitCash, oddsWaterIdx, betCount, cashUnit, baseAction.Info.ballNum, odds, baseAction.Info.groupId, baseAction.Info.playId, baseAction.Info.playCode, baseAction.Info.lotType, beishu);
								baseAction.Component.ball.reset();
							} catch (e) {
								console.log(e);
								$.alert(e);
							}
						}
					};
					_.isEmpty(baseAction.Info.ballWrongNum) ? e() : $.dialog({
						title: "以下号码有误，已自动过滤",
						content: baseAction.Info.ballWrongNum.join(", "),
						ok: function () {
							e()
						},
						okValue: "确定"
					});
				}

				var readyCanelFunc = function() {
					$(".beet-odds-tips, .tips-bg").hide();
				}

				$("body").delegate("#pushBetListAndBpBetBtn", onClickEventString, function () {
					if($("#beishuInput").val() < 1){
						$.alert("倍数不能为空");
					} else {
						readyCanelFunc();
						pushBetListFunc();
						bpBetFunc();
					}
				});

				// $("body").delegate("#bpBetBtn", onClickEventString, bpBetFunc);

				// $("body").delegate("#pushBetListBtn", onClickEventString, pushBetListFunc);

				$("body").delegate("#resetSelectBtn", onClickEventString, function(){
					baseAction.Component.ball.reset();
				});
				$("body").delegate("#toznzhuihbtn", onClickEventString, function () {
					$.router.loadPage("#doIntelligentSelectionPage");
				});

				$("body").delegate("#betReadyBtn", onClickEventString, function () {
					if($("#betZhushu").text() > 0) {
						$(".beet-odds-tips, .tips-bg").show();
					} else {
						$.alert("请选择正确的投注号码");
					}
				});

				$("body").delegate("#betReadyCanelBtn", onClickEventString, function () {
					baseAction.Component.ball.reset();
					readyCanelFunc();
				});
			},
			BetClassParser: function(){
				var betClassParser = {};
				betClassParser.setClass = function (b) {
					this.data = b;
					return this
				};
				betClassParser.getClass = function () {
					var b = [];
					$.each(this.data, function (c, d) {
						b.push({
							id: c,
							title: d.title,
							playGroupCode: d.playGroupCode,
							lotteryType: d.lotteryType
						})
					});
					return b
				};
				betClassParser.getSubClass = function (b) {
					b *= 1;
					return isNaN(b) ? !1 : _.isEmpty(this.data[b]) ? !1 : this.data[b].data || !1
				};
				betClassParser.find = function (b, c) {
					var d = !1;
					$.each(this.data, function (e, k) {
						$.each(k.data, function (k, m) {
							if (m.bettype == b && m.subid ==
								c) return d = e, !1
						});
						if (!1 !== d) return !1
					});
					return d
				}
				return betClassParser;
			},
			LotteryCountDown: function(lotCode){
				/*
				var d = baseAction.Info,
					b = baseAction.Function,
					f = baseAction.Component,
					*/
				var g = {
						$round: $("#bpRound"),
						$countDownMsg: $("#bpCountDown"),
					};
				g.stop = function () {
					clearTimeout(this.timeout);
					clearTimeout(this.updateTimeout);
					_.isObject(this.xhr) && this.xhr.abort && this.xhr.abort();
					return this
				};
				g.update = g.start = function () {
					this.$round = $("#bpRound");
					this.$countDownMsg = $("#bpCountDown");
					var c = this;
					this.xhr = $._ajax({url: stationUrl + "/lottery/getCountDown.do", data: {lotCode: lotCode}}, function (b, e, f, g) {
						if(b) {
							_.isEmpty(f) ? $.dialog({
								title: "系统讯息",
								content: e,
								okValue: "确定",
								ok: function () {
									// goBack()
								}
							}) : (c.time.serverTime = f.serverTime, c.$round.html(f.qiHao ? f.qiHao : "无期数"), baseAction.Info.round.now = {
								startBet: f.startbet || 0,
								stopBet: f.activeTime || 0,
								round: f.qiHao || 0,
								lottyID: f.lottyid || 0
							}, c.tick())
						} else {
							0 !== g.status && 0 !== g.readyState && $.dialog({
								title: "系统讯息",
								content: e,
								okValue: "确定",
								ok: function () {
									// goBack()
								}
							})
						}
					})
				};
				g.tick = function () {
					clearTimeout(this.timeout);
					var b = this,
						c = this.time.getCountDown(baseAction.Info.round.now.startBet),
						e = this.time.getCountDown(baseAction.Info.round.now.stopBet);
					if(0 <= c.seconds) {
						this.$round.html(baseAction.Info.round.now.round);
						// this.$countDownMsg.html("开放倒数：<span>" + c.h + "</span> 时 <span>" + c.m + "</span> 分 <span>" + c.s + "</span> 秒");
						var h1 = c.h.length > 1 ? c.h.substr(0,1) : 0;
						var h2 = c.h.substr(c.h.length - 1,1);
						var m1 = c.m.length > 1 ? c.m.substr(0,1) : 0;
						var m2 = c.m.substr(c.m.length - 1,1);
						var s1 = c.s.length > 1 ? c.s.substr(0,1) : 0;
						var s2 = c.s.substr(c.s.length - 1,1);
						this.$countDownMsg.html("<span>" + h1 + "</span><span>" + h2 + "</span><span class='time-kong'></span><span>" + m1 + "</span><span>" + m2 + "</span><span class='time-kong'></span><span>" + s1 + "</span><span>" + s2 + "</span>");
						this.timeout = setTimeout(function () {
							b.tick.apply(b)
						}, c.ms)
					} else {
						if(0 <= e.seconds){
							this.$round.html(baseAction.Info.round.now.round);
							// this.$countDownMsg.html(/*"截止倒数：<span>" + e.h + "</span> 时 <span>" +*/"<span>" + e.m + "</span> 分 <span>" + e.s + "</span> 秒");
							var h1 = e.h.length > 1 ? e.h.substr(0,1) : 0;
							var h2 = e.h.substr(e.h.length - 1,1);
							var m1 = e.m.length > 1 ? e.m.substr(0,1) : 0;
							var m2 = e.m.substr(e.m.length - 1,1);
							var s1 = e.s.length > 1 ? e.s.substr(0,1) : 0;
							var s2 = e.s.substr(e.s.length - 1,1);
							this.$countDownMsg.html("<span>" + h1 + "</span><span>" + h2 + "</span><span class='time-kong'></span><span>" + m1 + "</span><span>" + m2 + "</span><span class='time-kong'></span><span>" + s1 + "</span><span>" + s2 + "</span>");
							this.timeout = setTimeout(function () {
								b.tick.apply(b)
							}, e.ms);
						} else {
							if(0 == baseAction.Info.round.next.lottyID) {
								this.$countDownMsg.text("停止下注"), this.dialog = $.dialog({
									title: "系统讯息",
									content: "您好，本期" + baseAction.Info.round.now.round + "期的投注已截止，请重新确认投注下一期！",
									okValue: "确定",
									ok: function () {
										location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
									},
									canel: function () {
										location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
									}
								})
							} else {
								this.dialog = $.dialog({
									title: "系统讯息",
									content: "第 " + baseAction.Info.round.now.round + "期已停止下注",
									okValue: "确定",
									ok: function () {}
								});
								baseAction.Info.round.now = baseAction.Info.round.next;
								baseAction.Info.round.next = {
									startBet: 0,
									stopBet: 0,
									round: 0,
									lottyID: 0
								};
								this.timeout = setTimeout(function () {
									b.tick.apply(b)
								}, e.ms);
								this.updateTimeout = setTimeout(function () {
									b.update.call(b)
								}, 1E3 * _.random(1, 10))
							}
						}
					}
				}

				g.time = {
					getOffset : function () {
						var h = eval(sessionStorage.getItem("timeOffset"));
						return function () {
							(null === h || isNaN(h)) && 
							$._ajax({url: stationUrl + "/system/getServerTime.do", async: !1}, function (k, m, r) {
								k && (h = new Date - new Date(r.serverTime.replace(/-/g, "/")), sessionStorage.setItem("timeOffset", h))
							});
							return h
						}
					}(),
					getServerTime : function () {
						return new Date(new Date - this.getOffset())
					},
					getCountDown : function (h) {
						"string" == typeof h && (h = new Date(h.replace(/-/g, "/")));
						if (!(h instanceof Date) || isNaN(h.getTime())) return !1;
						h = (new Date(h - this.getServerTime())).getTime() / 1E3;
						return {
							h: ("0" + Math.floor(Math.abs(h) / 3600)).slice(-2),
							m: ("0" + Math.floor(Math.abs(h) % 3600 / 60)).slice(-2),
							s: ("0" + Math.floor(Math.abs(h) % 60)).slice(-2),
							ms: Math.floor(h % 1 * 1E3),
							seconds: h
						}
					}
				}
				return g;
			},
			haomaHistoryInit: function(){
				$(".dat-time").off("click.toggle").on("click.toggle", function(e) {
					$$.Function.stopPropagation(e);
					$(".shangqixiala").toggle();
				});
				var lotteryResultByLotCode = function(){
					$._ajax({url: stationUrl + "/lottery/lotteryResultByLotCode.do", data: {code: thisAction.lotCode, rows: 5, page: 1}}, function (status, message, data, result) {
						var tplHtml = $("#doPNHaomaHistoryLiTemplate").html();
						var $haomahistoryUl = $("#haomahistoryUl");
						$haomahistoryUl.html(_.template(tplHtml)({list: data}));

						if(data && data.length > 0){
							var aaa = data[0];
							$("#last_period").html(aaa.qiHao);
							// var haoMaArr = aaa.haoMa.split(",");
							$("#firstHaoma").empty();
							if(aaa.haoMaList){
								_.map(aaa.haoMaList, function(haoma){
									$("#firstHaoma").append('<em class="hong hongshu">' + haoma + '</em> ');
								});
								setTimeout(lotteryResultByLotCode, 1000 * 6);
							} else {
								$("#firstHaoma").append('<em class="hong hongshu">等待开奖</em> ');
								setTimeout(lotteryResultByLotCode, 1000 * 6);
							}
						}
					});
				}
				lotteryResultByLotCode();
			},
			orderCountShowInit : function(){
				$(".small_menu_box").off(onClickEventString, "#goBuyCartBtn").on(onClickEventString, "#goBuyCartBtn", function () {
					var betString = thisAction.buyCart.getBetString(thisAction.lotCode);

					if(betString.length){
						$.router.loadPage("#buyCartPage");
					} else {
						$.alert("请先添加正确的投注号码！");
					}
				});
			}
		});
	},
	GameDB : {
		gameTypeToPlayType: function (e) { // 重要
			/*
			e *= 1;
			var c = {
					1: [1, 2, 10], // [ 福彩3D, 体彩P3, 上海时时乐]
					2: [11, 12, 13, 14, 15, 35, 36, 37], // [ 天津时时彩, 重庆时时彩, 江西时时彩, 新疆时时彩, 黑龙江时时彩, 三分彩, 五分彩, 分分彩]
					3: [24], // [ 北京赛车 ]
					4: [25, 26], // [ 江苏快三, 安徽快3]
					5: [28, 29, 30, 31] // [ 山东11选5, 上海11选5, 江西11选5, 广东11选5]
				},
				d;
			for (d in c)
				if (-1 !== c[d].indexOf(e)) return 1 * d;
			return !1
			*/
			/*
			e *= 1;
			var c = {
					1: [14, 13, 27],
					2: [26, 6, 11, 10, -1, 8, 9, 7],
					3: [28],
					4: [30, 29],
					5: [17, 16, 15, 18]
				};
			for (var d in c)
				if (-1 !== c[d].indexOf(e)) return 1 * d;
			return !1
			*/

			var c = {
					0: [ "JSSB3", "AHK3", "HBK3", "SHHK3", "HEBK3", "GXK3","BJK3","JXK3","GSK3","FFK3","WFK3","JPK3","KRK3","HKK3","AMK3"],
					1: ["FC3D", "PL3"],
					2: ["EFC", "FFC", "WFC","HKWFC","AMWFC","SFC","ESFC", "CQSSC", "XJSSC", "TJSSC",'TXFFC',"HNFFC","HNWFC","AZXY5"],
					3: ["BJSC", "XYFT","LXYFT","SFSC","SFFT"],
					4: [30, 29],
					5: ["SH11X5", "GX11X5", "JX11X5", "SD11X5", "GD11X5"],
					7: ["PCEGG","JND28"]
				};
			for (var d in c) {
				if (-1 !== c[d].indexOf(e)) return 1 * d;
			}
			return -999;
		}, getGamePlayName: function (e, c, d) {
			return {
				"0.1.1": "和值",
				"0.2.2": "大小单双",
				"0.2.3": "三同号通选",
				"0.2.4": "三同号单选",
				"0.2.5": "三连号通选",
				"0.2.6": "二同号复式",
				"0.3.7": "三不同号",
				"0.4.8": "二不同号",
				"1.1.12": "\u524d\u4e8c\u76f4\u9009",
				"1.1.23": "\u540e\u4e8c\u76f4\u9009",
				"1.1.1123": "\u5b9a\u4f4d\u80c6",
				"1.2.12": "\u524d\u4e8c\u5927\u5c0f\u5355\u53cc",
				"1.2.23": "\u540e\u4e8c\u5927\u5c0f\u5355\u53cc",
				"1.4.212": "\u524d\u4e8c\u7ec4\u9009",
				"1.4.223": "\u540e\u4e8c\u7ec4\u9009",
				"1.4.1123": "\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"1.4.2123": "\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"1.5.123": "\u4e09\u661f\u7ec4\u9009(\u7ec4\u4e09)",
				"1.6.123": "\u4e09\u661f\u7ec4\u9009(\u7ec4\u516d)",
				"1.7.123": "\u4e09\u661f\u7ec4\u9009(\u548c\u503c)",
				"1.8.123": "\u4e09\u661f\u76f4\u9009(\u548c\u503c)",
				"1.9.123": "\u4e09\u661f\u76f4\u9009(\u590d\u5f0f)",
				"1.10.123": "\u4e09\u661f\u76f4\u9009(\u5355\u5f0f)",
				"2.1.12": "\u524d\u4e8c\u76f4\u9009(\u590d\u5f0f)",
				"2.1.20": "\u4efb\u4e8c\u76f4\u9009(\u590d\u5f0f)",
				"2.1.30": "\u4efb\u4e09\u76f4\u9009(\u590d\u5f0f)",
				"2.1.40": "\u4efb\u56db\u76f4\u9009(\u590d\u5f0f)",
				"2.1.45": "\u540e\u4e8c\u76f4\u9009(\u590d\u5f0f)",
				"2.1.123": "\u524d\u4e09\u76f4\u9009(\u590d\u5f0f)",
				"2.1.234": "\u4e2d\u4e09\u76f4\u9009(\u590d\u5f0f)",
				"2.1.345": "\u540e\u4e09\u76f4\u9009(\u590d\u5f0f)",
				"2.1.2345": "\u540e\u56db\u76f4\u9009(\u590d\u5f0f)",
				"2.1.12345": "\u4e94\u661f\u76f4\u9009(\u590d\u5f0f)",
				"2.1.112345": "\u5b9a\u4f4d\u80c6",
				"2.2.12": "\u524d\u4e8c\u7ec4\u9009(\u548c\u503c)",
				"2.2.20": "\u4efb\u4e8c\u7ec4\u9009(\u548c\u503c)",
				"2.2.30": "\u4efb\u4e09\u7ec4\u9009(\u548c\u503c)",
				"2.2.45": "\u540e\u4e8c\u7ec4\u9009(\u548c\u503c)",
				"2.2.123": "\u524d\u4e09\u7ec4\u9009\u548c\u503c",
				"2.2.234": "\u4e2d\u4e09\u7ec4\u9009\u548c\u503c",
				"2.2.345": "\u540e\u4e09\u7ec4\u9009\u548c\u503c",
				"2.3.12": "\u524d\u4e8c\u76f4\u9009(\u548c\u503c)",
				"2.3.20": "\u4efb\u4e8c\u76f4\u9009(\u548c\u503c)",
				"2.3.30": "\u4efb\u4e09\u76f4\u9009(\u548c\u503c)",
				"2.3.45": "\u540e\u4e8c\u76f4\u9009(\u548c\u503c)",
				"2.3.123": "\u524d\u4e09\u76f4\u9009(\u548c\u503c)",
				"2.3.234": "\u4e2d\u4e09\u76f4\u9009(\u548c\u503c)",
				"2.3.345": "\u540e\u4e09\u76f4\u9009(\u548c\u503c)",
				"2.4.12": "\u524d\u4e8c\u76f4\u9009(\u8de8\u5ea6)",
				"2.4.45": "\u540e\u4e8c\u76f4\u9009(\u8de8\u5ea6)",
				"2.4.123": "\u524d\u4e09\u76f4\u9009(\u8de8\u5ea6)",
				"2.4.234": "\u4e2d\u4e09\u76f4\u9009(\u8de8\u5ea6)",
				"2.4.345": "\u540e\u4e09\u76f4\u9009(\u8de8\u5ea6)",
				"2.5.20": "\u4efb\u4e8c\u7ec4\u9009(\u590d\u5f0f)",
				"2.5.123": "\u524d\u4e09\u76f4\u9009(\u7ec4\u5408)",
				"2.5.212": "\u524d\u4e8c\u7ec4\u9009(\u590d\u5f0f)",
				"2.5.234": "\u4e2d\u4e09\u76f4\u9009(\u7ec4\u5408)",
				"2.5.245": "\u540e\u4e8c\u7ec4\u9009(\u590d\u5f0f)",
				"2.5.345": "\u540e\u4e09\u76f4\u9009(\u7ec4\u5408)",
				"2.5.1123": "\u524d\u4e09\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.1345": "\u540e\u4e09\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.2123": "\u524d\u4e09\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.2345": "\u540e\u4e09\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.11234": "\u524d\u56db\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.12345": "\u540e\u56db\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.21234": "\u524d\u56db\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.22345": "\u540e\u56db\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.112345": "\u4e94\u661f\u4e00\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.212345": "\u4e94\u661f\u4e8c\u7801\u4e0d\u5b9a\u4f4d",
				"2.5.312345": "\u4e94\u661f\u4e09\u7801\u4e0d\u5b9a\u4f4d",
				"2.6.12": "\u524d\u4e8c\u7ec4\u9009(\u5305\u80c6)",
				"2.6.45": "\u540e\u4e8c\u7ec4\u9009(\u5305\u80c6)",
				"2.6.123": "\u524d\u4e09\u7ec4\u9009(\u5305\u80c6)",
				"2.6.234": "\u4e2d\u4e09\u7ec4\u9009(\u5305\u80c6)",
				"2.6.345": "\u540e\u4e09\u7ec4\u9009(\u5305\u80c6)",
				"2.7.30": "\u4efb\u4e09\u7ec4\u9009(\u7ec4\u4e09\u590d\u5f0f)",
				"2.7.123": "\u524d\u4e09\u7ec4\u9009(\u7ec4\u4e09)",
				"2.7.234": "\u4e2d\u4e09\u7ec4\u9009(\u7ec4\u4e09)",
				"2.7.345": "\u540e\u4e09\u7ec4\u9009(\u7ec4\u4e09)",
				"2.8.30": "\u4efb\u4e09\u7ec4\u9009(\u7ec4\u516d\u590d\u5f0f)",
				"2.8.123": "\u524d\u4e09\u7ec4\u9009(\u7ec4\u516d)",
				"2.8.234": "\u4e2d\u4e09\u7ec4\u9009(\u7ec4\u516d)",
				"2.8.345": "\u540e\u4e09\u7ec4\u9009(\u7ec4\u516d)",
				"2.9.123": "\u524d\u4e09\u5176\u4ed6(\u548c\u503c\u5c3e\u6570)",
				"2.9.234": "\u4e2d\u4e09\u5176\u4ed6(\u548c\u503c\u5c3e\u6570)",
				"2.9.345": "\u540e\u4e09\u5176\u4ed6(\u548c\u503c\u5c3e\u6570)",
				"2.10.12": "\u524d\u4e8c\u5927\u5c0f\u5355\u53cc",
				"2.10.45": "\u540e\u4e8c\u5927\u5c0f\u5355\u53cc",
				"2.10.123": "\u524d\u4e09\u5927\u5c0f\u5355\u53cc",
				"2.10.345": "\u540e\u4e09\u5927\u5c0f\u5355\u53cc",
				"2.11.123": "\u524d\u4e09\u5176\u4ed6(\u7279\u6b8a\u53f7\u7801)",
				"2.11.234": "\u4e2d\u4e09\u5176\u4ed6(\u7279\u6b8a\u53f7\u7801)",
				"2.11.345": "\u540e\u4e09\u5176\u4ed6(\u7279\u6b8a\u53f7\u7801)",
				"2.14.12": "\u524d\u4e8c\u76f4\u9009(\u5355\u5f0f)",
				"2.14.20": "\u4efb\u4e8c\u76f4\u9009(\u5355\u5f0f)",
				"2.14.30": "\u4efb\u4e09\u76f4\u9009(\u5355\u5f0f)",
				"2.14.40": "\u4efb\u56db\u76f4\u9009(\u5355\u5f0f)",
				"2.14.45": "\u540e\u4e8c\u76f4\u9009(\u5355\u5f0f)",
				"2.14.123": "\u524d\u4e09\u76f4\u9009(\u5355\u5f0f)",
				"2.14.234": "\u4e2d\u4e09\u76f4\u9009(\u5355\u5f0f)",
				"2.14.345": "\u540e\u4e09\u76f4\u9009(\u5355\u5f0f)",
				"2.14.2345": "\u540e\u56db\u76f4\u9009(\u5355\u5f0f)",
				"2.14.12345": "\u4e94\u661f\u76f4\u9009(\u5355\u5f0f)",
				"2.15.12": "\u524d\u4e8c\u7ec4\u9009(\u5355\u5f0f)",
				"2.15.20": "\u4efb\u4e8c\u7ec4\u9009(\u5355\u5f0f)",
				"2.15.30": "\u4efb\u4e09\u7ec4\u9009(\u6df7\u5408\u7ec4\u9009)",
				"2.15.45": "\u540e\u4e8c\u7ec4\u9009(\u5355\u5f0f)",
				"2.15.123": "\u524d\u4e09\u7ec4\u9009(\u6df7\u5408\u7ec4\u9009)",
				"2.15.234": "\u4e2d\u4e09\u7ec4\u9009(\u6df7\u5408\u7ec4\u9009)",
				"2.15.345": "\u540e\u4e09\u7ec4\u9009(\u6df7\u5408\u7ec4\u9009)",
				"2.17.30": "\u4efb\u4e09\u7ec4\u9009(\u7ec4\u4e09\u5355\u5f0f)",
				"2.18.30": "\u4efb\u4e09\u7ec4\u9009(\u7ec4\u516d\u5355\u5f0f)",
				"2.19.4": "\u4efb\u56db\u7ec4\u9009(4)",
				"2.19.6": "\u4efb\u56db\u7ec4\u9009(6)",
				"2.19.12": "\u4efb\u56db\u7ec4\u9009(12)",
				"2.19.24": "\u4efb\u56db\u7ec4\u9009(24)",
				"2.99.612345": "总和-大小单双",
				"2.99.6120": "龙虎斗",
				"2.99.6999": "豹子",
				"2.99.6123": "顺子",
				"2.99.6112": "对子",
				"2.99.6126": "半顺",
				"2.99.6169": "杂六",
				"3.1.1": "\u524d\u4e00\u76f4\u9009\u590d\u5f0f",
				"3.1.10": "\u5b9a\u4f4d\u80c6",
				"3.1.12": "\u524d\u4e8c\u76f4\u9009\u590d\u5f0f",
				"3.1.123": "\u524d\u4e09\u76f4\u9009\u590d\u5f0f",
				"3.14.12": "\u524d\u4e8c\u76f4\u9009\u5355\u5f0f",
				"3.14.123": "\u524d\u4e09\u76f4\u9009\u5355\u5f0f",
				"3.3.100": "大小单双",
				"3.3.200": "冠亚和值",
				"4.30.3600": "\u4e09\u540c\u53f7\u901a\u9009",
				"4.31.21600": "\u4e09\u540c\u53f7\u5355\u9009",
				"4.32.1350": "\u4e8c\u540c\u53f7\u590d\u9009",
				"4.33.7200": "\u4e8c\u540c\u53f7\u5355\u9009",
				"4.34.3600": "\u4e09\u4e0d\u540c\u53f7(\u6807\u51c6)",
				"4.35.720": "\u4e8c\u4e0d\u540c\u53f7(\u6807\u51c6)",
				"4.36.900": "\u4e09\u8fde\u53f7\u901a\u9009",
				"4.37.21600": "\u548c\u503c",
				"4.53.3600": "\u4e09\u4e0d\u540c\u53f7(\u80c6\u62d6)",
				"4.54.720": "\u4e8c\u4e0d\u540c\u53f7(\u80c6\u62d6)",
				"5.55.0": "\u4efb\u9009\u4e00\u4e2d\u4e00(\u590d\u5f0f)",
				"5.56.0": "\u4efb\u9009\u4e8c\u4e2d\u4e8c(\u590d\u5f0f)",
				"5.57.0": "\u4efb\u9009\u4e09\u4e2d\u4e09(\u590d\u5f0f)",
				"5.58.0": "\u4efb\u9009\u56db\u4e2d\u56db(\u590d\u5f0f)",
				"5.59.0": "\u4efb\u9009\u4e94\u4e2d\u4e94(\u590d\u5f0f)",
				"5.60.0": "\u4efb\u9009\u516d\u4e2d\u4e94(\u590d\u5f0f)",
				"5.61.0": "\u4efb\u9009\u4e03\u4e2d\u4e94(\u590d\u5f0f)",
				"5.62.0": "\u4efb\u9009\u516b\u4e2d\u4e94(\u590d\u5f0f)",
				"5.55.1": "\u4efb\u9009\u4e00\u4e2d\u4e00(\u5355\u5f0f)",
				"5.56.1": "\u4efb\u9009\u4e8c\u4e2d\u4e8c(\u5355\u5f0f)",
				"5.57.1": "\u4efb\u9009\u4e09\u4e2d\u4e09(\u5355\u5f0f)",
				"5.58.1": "\u4efb\u9009\u56db\u4e2d\u56db(\u5355\u5f0f)",
				"5.59.1": "\u4efb\u9009\u4e94\u4e2d\u4e94(\u5355\u5f0f)",
				"5.60.1": "\u4efb\u9009\u516d\u4e2d\u4e94(\u5355\u5f0f)",
				"5.61.1": "\u4efb\u9009\u4e03\u4e2d\u4e94(\u5355\u5f0f)",
				"5.62.1": "\u4efb\u9009\u516b\u4e2d\u4e94(\u5355\u5f0f)",
				"5.63.0": "\u5b9a\u4f4d\u80c6",
				"5.64.13": "\u524d\u4e09\u4e0d\u5b9a\u4f4d",
				"5.64.24": "\u4e2d\u4e09\u4e0d\u5b9a\u4f4d",
				"5.64.35": "\u540e\u4e09\u4e0d\u5b9a\u4f4d",
				"5.65.2012": "\u524d\u4e8c\u76f4\u9009\u590d\u5f0f",
				"5.65.2045": "\u540e\u4e8c\u76f4\u9009\u590d\u5f0f",
				"5.65.2112": "\u524d\u4e8c\u76f4\u9009\u5355\u5f0f",
				"5.65.2145": "\u540e\u4e8c\u76f4\u9009\u5355\u5f0f",
				"5.66.2012": "\u524d\u4e8c\u7ec4\u9009\u590d\u5f0f",
				"5.66.2045": "\u540e\u4e8c\u7ec4\u9009\u590d\u5f0f",
				"5.66.2112": "\u524d\u4e8c\u7ec4\u9009\u5355\u5f0f",
				"5.66.2145": "\u540e\u4e8c\u7ec4\u9009\u5355\u5f0f",
				"5.67.3013": "\u524d\u4e09\u76f4\u9009\u590d\u5f0f",
				"5.67.3024": "\u4e2d\u4e09\u76f4\u9009\u590d\u5f0f",
				"5.67.3035": "\u540e\u4e09\u76f4\u9009\u590d\u5f0f",
				"5.67.3113": "\u524d\u4e09\u76f4\u9009\u5355\u5f0f",
				"5.67.3124": "\u4e2d\u4e09\u76f4\u9009\u5355\u5f0f",
				"5.67.3135": "\u540e\u4e09\u76f4\u9009\u5355\u5f0f",
				"5.68.3013": "\u524d\u4e09\u7ec4\u9009\u590d\u5f0f",
				"5.68.3024": "\u4e2d\u4e09\u7ec4\u9009\u590d\u5f0f",
				"5.68.3035": "\u540e\u4e09\u7ec4\u9009\u590d\u5f0f",
				"5.68.3113": "\u524d\u4e09\u7ec4\u9009\u5355\u5f0f",
				"5.68.3124": "\u4e2d\u4e09\u7ec4\u9009\u5355\u5f0f",
				"5.68.3135": "\u540e\u4e09\u7ec4\u9009\u5355\u5f0f",
				"5.71.1": "\u4efb\u9009\u4e8c\u4e2d\u4e8c(\u80c6\u62d6)",
				"5.71.2": "\u4efb\u9009\u4e09\u4e2d\u4e09(\u80c6\u62d6)",
				"5.71.3": "\u4efb\u9009\u56db\u4e2d\u56db(\u80c6\u62d6)",
				"5.71.4": "\u4efb\u9009\u4e94\u4e2d\u4e94(\u80c6\u62d6)",
				"5.71.5": "\u4efb\u9009\u516d\u4e2d\u4e94(\u80c6\u62d6)",
				"5.71.6": "\u4efb\u9009\u4e03\u4e2d\u4e94(\u80c6\u62d6)",
				"5.71.7": "\u4efb\u9009\u516b\u4e2d\u4e94(\u80c6\u62d6)",
				"5.71.812": "\u524d\u4e8c\u7ec4\u9009\u80c6\u62d6",
				"5.71.845": "\u540e\u4e8c\u7ec4\u9009\u80c6\u62d6",
				"5.71.913": "\u524d\u4e09\u7ec4\u9009\u80c6\u62d6",
				"5.71.924": "\u4e2d\u4e09\u7ec4\u9009\u80c6\u62d6",
				"5.71.935": "\u540e\u4e09\u7ec4\u9009\u80c6\u62d6",

				"7.1.1": "三星复式",
				"7.1.2": "前二复式",
				"7.1.3": "后二复式",
				"7.2.4": "定位胆",
				"7.2.5": "和值大小单双",
				"7.3.6": "不定位胆",
				"7.4.7": "前二组选",
				"7.4.8": "后二组选",
				"7.5.9": "三星组选",
				"7.6.10": "和值"
			}[e + "." + c + "." + d] || ""
		}
	},
	buyCart : {
		order: {},
		add: function (gameType, c, d, b, f, g, h, k, l, groupId, playId, playCode, lotType, beishu) {
			!_.isArray(k) && (k = [k]);
			// gameType *= 1;
			c *= 1;
			var playType = $$.Action.DoPickNumber.GameDB.gameTypeToPlayType(gameType);
			if (-999 == playType) throw "彩种错误";
			this.order[gameType] ||
				(this.order[gameType] = {
					idx: 1,
					data: {}
				});
			if (this.isExist(gameType, c, d, b, f, g, h, k, l)) throw console.log(gameType, c, d, b, f, g, h, k, l), "此投注号码已存在，请重新选择!!";
			if (2 == playType && 11 == c) {
				k = _.flatten(k);
				l = l.split(",");
				if (1 < k.length) {
					var m = this;
					$.map(k, function (n) {
						m.add(gameType, c, d, b, f, g / k.length, h, [n], l[3 - n])
					});
					return this
				}
				l = l[3 - k[0]]
			} else if (4 == playType && 37 == c) {
				var p = function (b) {
					switch (1 * b) {
					case 3:
					case 18:
						return 0;
					case 4:
					case 17:
						return 1;
					case 5:
					case 16:
						return 2;
					case 6:
					case 15:
						return 3;
					case 7:
					case 14:
						return 4;
					case 8:
					case 13:
						return 5;
					case 9:
					case 12:
						return 6;
					case 10:
					case 11:
						return 7;
					default:
						throw "rate error";
					}
				};
				l = l.split(",");
				k = _.flatten(k);
				if (1 < k.length) return m = this, $.map(k, function (n) {
					m.add(gameType, c, d, b, f, g / k.length, h, [n], l[p(n)])
				}), this;
				l = l[p(k[0])]
			}
			this.order[gameType].data[this.order[gameType].idx] = {
				gameType: gameType,
				betType: c,
				subid: d,
				cash: b,
				rate: f,
				betCount: g,
				cashUnit: h,
				ball: k,
				odds: l,
				groupId: groupId,
				playId: playId,
				playCode: playCode,
				lotType: lotType,
				beishu: beishu
			};
			return this.order[gameType].idx++
		}, del: function (e, c) {
			delete this.order[e].data[c];
			return this
		}, clear: function (e) {
			if (this.order[e])
				for (var c in this.order[e].data) delete this.order[e].data[c];
			return this
		}, isExist: function (e, c, d, b, f, g, h, k) {
			!_.isArray(k) && (k = [k]);
			// e *= 1;
			c *= 1;
			var playType = $$.Action.DoPickNumber.GameDB.gameTypeToPlayType(e);
			if (-999 == playType) throw "\u5f69\u79cd\u9519\u8bef";
			if (!this.order[e]) return !1;
			if (2 == playType && 11 == c || 4 == playType && 37 == c)
				if (k = _.flatten(k), 1 < k.length) {
					var l = this,
						m = !1;
					$.map(k, function (k) {
						m = l.isExist(e, c, d, b, f, g, h, [k]) || !1
					});
					return m
				}
			for (var p in this.order[e].data) {
				var n = this.order[e].data[p];
				if (n.gameType == e && n.betType == c && n.subid == d && n.cash == b && n.rate == f && n.betCount == g && n.cashUnit == h &&
						$$.Action.DoPickNumber.ballToBetString(e, c, d, n.ball) == $$.Action.DoPickNumber.ballToBetString(e, c, d, k)) return !0;
			}
			return !1
		}, get: function (e, c) {
			// e *= 1;
			c *= 1;
			return e && c && this.order[e] && this.order[e].data[c] ? this.order[e].data[c] : e && !c && this.order[e] ? this.order[e].data : !1
		}, getTotalCost: function (e) {
			e = this.get(e);
			var c = 0,
				d;
			for (d in e) c += 1 / e[d].cashUnit * e[d].cash * e[d].betCount * e[d].beishu;
			return 1 * c.toFixed(3)
		}, getTotalZhushu: function (e) {
			e = this.get(e);
			var c = 0,
				d;
			for (d in e) c += e[d].betCount;
			return 1 * c
		}, getBetString: function (e, c) {
			var d = [];
			$.map(this.get(e) || [], function (b) {
				var e = [b.gameType, b.groupId, b.playId, b.cash, b.rate, b.betCount, b.cashUnit, $$.Action.DoPickNumber.ballToBetString(b.gameType, b.betType, b.subid, b.ball)];
				// c && e.push(b.odds);
				e.push(b.odds);
				e.push(b.beishu);
				d.push(e.join("|"))
			});
			return d
		}, getNewBetString: function (e, c) {
			var d = [];
			$.map(this.get(e) || [], function (b) {
				var betNum = $$.Action.DoPickNumber.ballToBetString(b.gameType, b.betType, b.subid, b.ball);
				var betNumArr = betNum.split(",");
				for(var key in betNumArr){
					var value = betNumArr[key];
					betNumArr[key] = value ? value : "-";
				}
				betNum = betNumArr.join(",");
				var e = [b.gameType, b.playCode, b.cashUnit, b.beishu, betNum];
				c && e.push(b.odds);
				d.push(e.join("|"))
			});
			return d
		}
	},
	ballCalc : {
		calc: function (e, c, d, b) { // (playType, betType, subid, ballNum)
			if (!$.isArray(b)) throw "输入必须为阵列";
			e *= 1;
			c *= 1;
			d *= 1;
			b = b.slice(0);
			var f = 0,
				g = this;
			switch (e) {
			case 0: // 快三
				switch (c) {
				case 1:
					f = this.byIndex(b, "0.num3DirectSum"); // 和值
					break;
				case 2:
					f = this.count(b); // 和值大小单双, 三同号通选, 三同号单选, 三连号通选, 二同号复式
					break;
				case 3:
					f = this.combine(b, 3);
					break;
				case 4:
					f = this.combine(b, 2);
					break;
				}
				break;
			case 1:
				switch (c) {
				case 1:
					f = 1123 == d ? this.count(b) : this.mul(b);
					break;
				case 2:
					f = this.mul(b);
					break;
				case 4:
					if (-1 !== $.inArray(d, [212, 223])) f = this.combine(b, 2);
					else if (1123 == d) f = this.combine(b, 1);
					else if (2123 == d) f = this.combine(b, 2);
					else throw "error";
					break;
				case 5:
					f = 2 * this.combine(b, 2);
					break;
				case 6:
					f = this.combine(b, 3);
					break;
				case 7:
					f = this.byIndex(b, "2.num3Sum");
					break;
				case 8:
					f = this.byIndex(b, "2.num3DirectSum");
					break;
				case 9:
					f = this.mul(b);
					break;
				default:
					throw "error";
				}
				break;
			case 2: // 时时彩
				switch (c) {
				case 1:
					-1 !== $.inArray(d, [20, 30, 40]) ? (b = this.toCount(b), b = this.toCombine(b, d / 10), $.map(b, function (b) {
						f += g.mul(b)
					})) : f = 112345 == d ? this.count(b) : this.mul(b);
					break;
				case 2:
					if (-1 != $.inArray(d, [12, 45])) f = this.byIndex(b, "2.num2Sum");
					else if (-1 != $.inArray(d, [123, 234, 345])) f = this.byIndex(b, "2.num3Sum");
					else if (20 == d) f = this.combine(b[0], 2) * this.byIndex(b[1], "2.num2Sum");
					else if (30 == d) f =
						this.combine(b[0], 3) * this.byIndex(b[1], "2.num3Sum");
					else throw "error";
					break;
				case 3:
					-1 !== $.inArray(d, [12, 45]) ? f = this.byIndex(b, "2.num2DirectSum") : -1 !== $.inArray(d, [123, 234, 345]) ? f = this.byIndex(b, "2.num3DirectSum") : 20 == d ? f = this.combine(b[0], 2) * this.byIndex(b[1], "2.num2DirectSum") : 30 == d && (f = this.combine(b[0], 3) * this.byIndex(b[1], "2.num3DirectSum"));
					break;
				case 4:
					if (-1 !== $.inArray(d, [12, 45])) f = this.byIndex(b, "2.num2Cross");
					else if (-1 !== $.inArray(d, [123, 234, 345])) f = this.byIndex(b, "2.num3Cross");
					else throw "error";
					break;
				case 5:
					if (20 == d) f = this.combine(b[0], 2) * this.combine(b[1], 2);
					else if (-1 !== $.inArray(d, [123, 234, 345])) f = 3 * this.mul(b);
					else if (-1 !== $.inArray(d, [212, 245])) f = this.combine(b, 2);
					else if (-1 !== $.inArray(d, [1123, 1234, 1345, 11234, 12345, 112345])) f = this.combine(b, 1);
					else if (-1 !== $.inArray(d, [2123, 2345, 21234, 22345, 212345])) f = this.combine(b, 2);
					else if (312345 == d) f = this.combine(b, 3);
					else throw "error";
					break;
				case 6:
					if (-1 !== $.inArray(d, [12, 45])) f = _.isEmpty(_.flatten(b)) ? 0 : 9;
					else if (-1 !== $.inArray(d, [123, 234, 345])) f =
						_.isEmpty(_.flatten(b)) ? 0 : 54;
					else throw "error";
					break;
				case 7:
					f = 30 == d ? this.combine(b[0], 3) * this.combine(b[1], 2) * 2 : 2 * this.combine(b, 2);
					break;
				case 8:
					f = 30 == d ? this.combine(b[0], 3) * this.combine(b[1], 3) : this.combine(b, 3);
					break;
				case 9:
					f = this.count(b);
					break;
				case 10:
					f = this.mul(b);
					break;
				case 11:
					f = this.count(b);
					break;
				case 14:
				case 15:
				case 17:
				case 18:
					switch (d) {
					case 20:
						f = this.combine(b[0], 2);
						break;
					case 30:
						f = this.combine(b[0], 3);
						break;
					case 40:
						f = this.combine(b[0], 4);
						break;
					default:
						throw "error";
					}
					break;
				case 19:
					if (4 ==
						d) {
						var h = 0;
						$.map(b[1], function (c) {
							h += _.without(b[2], c).length
						});
						f = h * this.combine(b[0], 4)
					} else if (12 == d) h = 0, $.map(b[1], function (c) {
						h += g.combine(_.without(b[2], c), 2)
					}), f = h * this.combine(b[0], 4);
					else if (6 == d) f = this.combine(b[0], 4) * this.combine(b[1], 2);
					else if (24 == d) f = this.combine(b[0], 4) * this.combine(b[1], 4);
					else throw "error";
				case 99:
					f = this.count(b);
					break;
				}
				break;
			case 3: // 北京塞车
				switch (d) {
				case 10:
					return this.count(b);
				case 200:
					return this.byIndex(b, "3.num3DirectSum"); // 和值
				default :
					return this.mulNoRepeat(b);
				}
				break;
			case 4:
				switch (c) {
				case 30:
					return this.count(b);
				case 31:
					return this.count(b);
				case 32:
					return this.count(b);
				case 33:
					return this.mulNoRepeat(b);
				case 34:
					return this.combine(b, 3);
				case 35:
					return this.combine(b, 2);
				case 36:
					return this.count(b);
				case 37:
					return this.count(b);
				case 53:
					return d = this.toCount(b), dan = d[0] || 0, tou = d[1] || 0, 0 == dan ? 0 : this.combine(tou, 3 - dan);
				case 54:
					return this.mulNoRepeat(b)
				}
				break;
			case 5:
				switch (c) {
				case 55:
					return this.combine(b, 1);
				case 56:
					return this.combine(b, 2);
				case 57:
					return this.combine(b, 3);
				case 58:
					return this.combine(b, 4);
				case 59:
					return this.combine(b, 5);
				case 60:
					return this.combine(b, 6);
				case 61:
					return this.combine(b, 7);
				case 62:
					return this.combine(b, 8);
				case 63:
				case 64:
					return this.count(b);
				case 65:
				case 67:
					return this.mulNoRepeat(b);
				case 66:
					return this.combine(b, 2);
				case 68:
					return this.combine(b, 3);
				case 71:
					return e = 0, e = -1 != [812, 845].indexOf(d) ? 2 : -1 != [913, 924, 935].indexOf(d) ? 3 : d + 1, b[0].length ? this.combine(b[1], e - b[0].length) : 0
				}
				break;
			case 7: // PC蛋蛋
				// 计算注数
				switch (c) {
				case 1:
					f = this.mul(b); // 三星复式、前二复式、后二复式
					break;
				case 2:
					f = this.count(b); // 定位胆、和值大小单双
					break;
				case 3:
					f = this.combine(b, 1); // 不定位胆
					break;
				case 4:
					f = this.combine(b, 2); // 前二组选、后二组选
					break;
				case 5:
					f = this.combine(b, 3); // 三星组六
					break;
				case 6:
					f = this.byIndex(b, "2.num3DirectSum"); // 和值
					break;
				}
				break;
			default:
				throw "彩种大类错误";
			}
			return f
		}, mul: function (e, c) {
			!_.isNumber(c) && (c = 0);
			if (!$.isArray(e)) throw "\u8f93\u5165\u5fc5\u987b\u4e3a\u9635\u5217";
			if (_.isEmpty(e)) return 0;
			var d = 1;
			$.map(e, function (b) {
				d = $.isArray(b) ? d * (b.length || (c-- ? 1 : 0)) : d * (b || (c-- ? 1 : 0))
			});
			return d
		}, mulNoRepeat: function (e, c) {
			c = 1 * c || 0;
			if (!$.isArray(e)) throw "\u8f93\u5165\u5fc5\u987b\u4e3a\u9635\u5217";
			if (_.isEmpty(e)) return 0;
			var d = [],
				b = 0;
			$.map(e, function (b) {
				$.isArray(b) || (b = [b]);
				if (0 == b.length && 0 >= c) return !1;
				b = _.flatten(b);
				if (0 == d.length) $.map(b, function (b) {
					d.push([b])
				});
				else {
					var e = [];
					$.map(b, function (b) {
						$.map(d, function (c) {
							c = c.slice();
							c.push(b);
							e.push(c)
						})
					});
					d = e
				}
			});
			$.map(d, function (d) {
				_.uniq(d).length +
					c >= e.length && b++
			});
			return b
		}, count: function (e) {
			if (!$.isArray(e)) throw "\u8f93\u5165\u5fc5\u987b\u4e3a\u9635\u5217";
			return _.flatten(e).length
		}, toCount: function (e) {
			var c = this;
			return $.map(e, function (d) {
				return c.count(d)
			})
		}, combine: function (e, c) {
			var d = function (b) {
				for (var c = 1, d = 2; d <= b; d++) c *= d;
				return c
			};
			_.isArray(e) && (e = _.flatten(e).length);
			return c > e ? 0 : c == e ? 1 : d(e) / (d(c) * d(e - c))
		}, toCombine: function (e, c) {
			for (var d = [
				[]
			], b = [], f = 0, g = e.length; f < g; ++f)
				for (var h = 0, k = d.length; h < k; ++h)(d[h].length < c - 1 ? d : b).push(d[h].concat([e[f]]));
			return b
		}, byIndex: function (e, c) {
			if (!$.isArray(e)) throw "\u8f93\u5165\u5fc5\u987b\u4e3a\u9635\u5217";
			e = _.flatten(e);
			var d = {
				"2.num2Sum": [0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1],
				"2.num3Sum": [0, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1],
				"2.num3DirectSum": [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1],
				"2.num2DirectSum": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
				"2.num2Cross": [10, 18, 16, 14, 12, 10, 8, 6, 4, 2],
				"2.num3Cross": [10, 54, 96, 126, 144, 150, 144, 126, 96, 54],
				// 2017-03-19 new add
				"0.num3DirectSum": [0, 0, 0, 1, 3, 6, 10, 15, 21, 25, 27, 27, 25, 21, 15, 10, 6, 3, 1],
				// 2017-03-19 new add
				"3.num3DirectSum": [0, 0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 8, 8, 6, 6, 4, 4, 2, 2]
			};
			if (void 0 == d[c]) throw "\u65e0\u6548\u7684 index \u540d\u79f0: " + c;
			var b = 0;
			$.map(e, function (e) {
				void 0 == d[c][e] && console.error("\u65e0\u6548\u7684\u7d22\u5f15 %s", e);
				b += d[c][e] || 0
			});
			return b
		}
	},
	randomNum : {
		genRandArr: function (e, c, d, b, f) {
			f = $.isNumeric(f) ? [f] : _.flatten(f);
			if (e > Math.abs(d - c)) throw "\u65e0\u6cd5\u8ba1\u7b97";
			for (var g = [], h = 100; e;) {
				if (0 >= h--) throw "\u65e0\u6cd5\u4ea7\u751f\u6307\u5b9a\u7684\u8d44\u6599\u7ec4\u5408";
				var k = _.random(c, d);
				b && (k = String("00" + k).slice(-1 * String("" + d).length)); - 1 === $.inArray(k, f) && -1 === $.inArray(k, g) && (g.push(k), e--)
			}
			g.sort();
			return g
		}, genArr: function (e, c, d, b, f, g) {
			if (c > e) throw "\u65e0\u6cd5\u4ea7\u751f\u53f7\u7801";
			for (var h = [], k = 0, k = 1; k <= e; k++) h.push([]);
			for (; c;) k = _.random(0, e - 1), _.isEmpty(h[k]) && (h[k] = this.genRandArr(d, b, f, g), c--);
			return h
		}, gen: function (e, c, d) { // (playType, betType, subid)
			c *= 1;
			d *= 1;
			var b = [];
			switch (1 * e) {
			case 0:
				// 随机投注号码
				switch (c + "." + d) {
				case "1.1": // 和值
					b = this.genArr(1, 1, 1, 3, 18);
					break;
				case "2.2": // 大小单双
					break;
				case "2.3": // 三同号通选
					break;
				case "2.4": // 三同号单选
					break;
				case "2.5": // 三连号通选
					break;
				case "2.6": // 二同号复式
					break;
				case "3.7": // 三不同号
					b = this.genArr(1, 1, 3, 1, 6);
					break;
				case "4.8": // 二不同号
					b = this.genArr(1, 1, 2, 1, 6);
					break;
				}
				break;
			case 1:
				switch (c) {
				case 1:
					b = 1123 == d ? this.genArr(3, 1, 1, 0, 9) : this.genArr((d + "").length, (d + "").length, 1, 0, 9);
					break;
				case 2:
					b = this.genArr(2, 2, 1, 1, 4);
					break;
				case 4:
					if (-1 !== $.inArray(d, [212, 223])) b = this.genArr(1, 1, 2, 0, 9);
					else if (1123 == d) b = this.genArr(1, 1, 1, 0, 9);
					else if (2123 == d) b = this.genArr(1, 1, 2, 0, 9);
					else throw "error";
					break;
				case 5:
					b = this.genArr(1, 1, 2, 0, 9);
					break;
				case 6:
					b =
						this.genArr(1, 1, 3, 0, 9);
					break;
				case 7:
					b = this.genArr(1, 1, 1, 1, 26);
					break;
				case 8:
					b = this.genArr(1, 1, 1, 0, 27);
					break;
				case 9:
					b = this.genArr((d + "").length, (d + "").length, 1, 0, 9);
					break;
				default:
					throw "error";
				}
				break;
			case 2:
				switch (c) {
				case 1:
					if(-1 !== $.inArray(d, [6123, 6345, 612, 645])){
						var ballStrArr = ["大", "小", "单", "双"];
						var knmbknmb = (d + "").length - 1;
						var randomArr = this.genArr(knmbknmb, knmbknmb, 1, 0, 3);
						for(var i = 0; i < randomArr.length; i++){
							for(var j = 0; j < randomArr.length; j++){
								randomArr[i][j] = ballStrArr[randomArr[i][j]];
							}
						}
						b = randomArr;
					} else {
						b = -1 !== $.inArray(d, [20, 30, 40]) ? this.genArr(5, d / 10, 1, 0, 9) : 112345 == d ? this.genArr(5, 1, 1, 0, 9) : this.genArr((d + "").length, (d + "").length, 1, 0, 9);
					}
					break;
				case 2:
					if (-1 != $.inArray(d, [12, 45])) b = this.genArr(1, 1, 1, 1, 17);
					else if (-1 != $.inArray(d, [123, 234, 345])) b = this.genArr(1, 1, 1, 1, 26);
					else if (20 ==
						d) b = [this.genRandArr(2, 0, 4), this.genRandArr(1, 1, 17)];
					else if (30 == d) b = [this.genRandArr(3, 0, 4), this.genRandArr(1, 1, 26)];
					else throw "error";
					break;
				case 3:
					-1 !== $.inArray(d, [12, 45]) ? b = this.genArr(1, 1, 1, 0, 18) : -1 !== $.inArray(d, [123, 234, 345]) ? b = this.genArr(1, 1, 1, 0, 27) : 20 == d ? b = [this.genRandArr(2, 0, 4), this.genRandArr(1, 0, 18)] : 30 == d && (b = [this.genRandArr(3, 0, 4), this.genRandArr(1, 0, 27)]);
					break;
				case 4:
					b = this.genArr(1, 1, 1, 0, 9);
					break;
				case 5:
					if (20 == d) b = [this.genRandArr(2, 0, 4), this.genRandArr(2, 0, 9)];
					else if (-1 !== $.inArray(d, [123, 234, 345])) b = this.genArr(3, 3, 1, 0, 9);
					else if (-1 !== $.inArray(d, [212, 245])) b = this.genArr(1, 1, 2, 0, 9);
					else if (-1 !== $.inArray(d, [1123, 1234, 1345, 11234, 12345, 112345])) b = this.genArr(1, 1, 1, 0, 9);
					else if (-1 !== $.inArray(d, [2123, 2345, 21234, 22345, 212345])) b = this.genArr(1, 1, 2, 0, 9);
					else if (312345 == d) b = this.genArr(1, 1, 3, 0, 9);
					else throw "error";
					break;
				case 6:
					b = this.genArr(1, 1, 1, 0, 9);
					break;
				case 7:
					b = 30 == d ? [this.genRandArr(3, 0, 4), this.genRandArr(2, 0, 9)] : this.genArr(1, 1, 2, 0, 9);
					break;
				case 8:
					b = 30 == d ? [this.genRandArr(3, 0, 4),
						this.genRandArr(3, 0, 9)
					] : this.genArr(1, 1, 3, 0, 9);
					break;
				case 9:
					b = this.genArr(1, 1, 1, 0, 9);
					break;
				case 10:
					b = -1 !== $.inArray(d, [12, 45]) ? this.genArr(2, 2, 1, 1, 4) : this.genArr(3, 3, 1, 1, 4);
					break;
				case 11:
					b = this.genArr(1, 1, 1, 1, 3);
					break;
				case 19:
					if (4 == d) b = this.genRandArr(1, 0, 9), b = [this.genRandArr(4, 0, 4), b, this.genRandArr(1, 0, 9, !1, b)];
					else if (12 == d) b = this.genRandArr(1, 0, 9), b = [this.genRandArr(4, 0, 4), b, this.genRandArr(2, 0, 9, !1, b)];
					else if (6 == d) b = [this.genRandArr(4, 0, 4), this.genRandArr(2, 0, 9)];
					else if (24 == d) b = [this.genRandArr(4,
						0, 4), this.genRandArr(4, 0, 9)];
					else throw "error";
				}
				break;
			case 3:
				if (10 == d) b = this.genArr(10, 1, 1, 1, 10, !0);
				else if (-1 !== [1, 2, 3, 12, 123].indexOf(d))
					for (b = [], e = 1; e <= (d + "").length; e++) b.push(this.genRandArr(1, 1, 10, !0, _.flatten(b)));
				else throw "error";
				break;
			case 4:
				switch (c) {
				case 30:
					b = [
						["111,222,333,444,555,666"]
					];
					break;
				case 31:
					b = [
						[
							[111, 222, 333, 444, 555, 666][_.random(0, 5)]
						]
					];
					break;
				case 32:
					b = [
						[
							[11, 22, 33, 44, 55, 66][_.random(0, 5)]
						]
					];
					break;
				case 33:
					b.push(this.genRandArr(1, 1, 6, 0, b));
					b.push(this.genRandArr(1, 1, 6, 0, b));
					break;
				case 34:
					b = this.genArr(1, 1, 3, 1, 6, 0);
					break;
				case 35:
					b = this.genArr(1, 1, 2, 1, 6, 0);
					break;
				case 36:
					b = [
						["123,234,345,456"]
					];
					break;
				case 37:
					b = this.genArr(1, 1, 1, 3, 18, 0);
					break;
				case 53:
					d = 3;
					e = _.random(1, d - 1);
					tou = d - e;
					b.push(this.genRandArr(e, 1, 6, !0));
					b.push(this.genRandArr(tou, 1, 6, !0, b[0]));
					break;
				case 54:
					b.push(this.genRandArr(1, 1, 6, 0, b)), b.push(this.genRandArr(1, 1, 6, 0, b))
				}
				break;
			case 5:
				switch (c) {
				case 55:
				case 56:
				case 57:
				case 58:
				case 59:
				case 60:
				case 61:
				case 62:
					b = this.genArr(1, 1, c - 54, 1, 11, !0);
					break;
				case 63:
					b = this.genArr(5,
						1, 1, 1, 11, !0);
					break;
				case 64:
					b = this.genArr(1, 1, 1, 1, 11, !0);
					break;
				case 65:
					for (e = 1; 2 >= e; e++) b.push(this.genRandArr(1, 1, 11, !0, b));
					break;
				case 66:
					b = this.genArr(1, 1, 2, 1, 11, !0);
					break;
				case 67:
					for (e = 1; 3 >= e; e++) b.push(this.genRandArr(1, 1, 11, !0, b));
					break;
				case 68:
					b = this.genArr(1, 1, 3, 1, 11, !0);
					break;
				case 71:
					d = 900 < d ? 3 : 800 < d ? 2 : d + 1, e = _.random(1, d - 1), tou = d - e, b.push(this.genRandArr(e, 1, 11, !0)), b.push(this.genRandArr(tou, 1, 11, !0, b[0]))
				}
				break;
			case 7:
				// 随机投注号码
				switch (d) {
				case 1: // 三星复式
					b = this.genArr(3, 3, 1, 0, 9);
					break;
				case 2: // 前二复式
				case 3: // 后二复式
					b = this.genArr(2, 2, 1, 0, 9);
					break;
				case 4: // 定位胆
					b = this.genArr(3, 1, 1, 0, 9);
					break;
				case 5: // 和值大小单双
					
					break;
				case 6: // 不定位胆
					b = this.genArr(1, 1, 1, 0, 9);
					break;
				case 7: // 前二组选
				case 8: // 后二组选
					b = this.genArr(1, 1, 2, 0, 9);
					break;
				case 9: // 三星组六
					b = this.genArr(1, 1, 3, 0, 9);
					break;
				case 10: // 和值
					b = this.genArr(1, 1, 1, 0, 27);
					break;
				}
				break;
			default:
				throw "\u5f69\u79cd\u5927\u7c7b\u9519\u8bef";
			}
			return b
		}
	},
	ballToBetString : (function () {
		var innerFunc = {};
		innerFunc.direct = function (e) {
			var c = [];
			$.map(e, function (d) {
				c.push(_.isArray(d) ? d.join("") : d + "")
			});
			return c.join(",").replace(/\s/g, "")
		};
		innerFunc.join = function (e, c) {
			return _.flatten(e).join(c || ",").replace(/\s/g, "")
		};
		innerFunc.any = function (e, c) {
			var d = e.shift().join("");
			return this.join(e) + "@" + c + "_" + d
		};
		innerFunc.anyNew = function (ballNum) {
			var g = function (b, c, d) {
				var e = [];
				_.isArray(c) || (c = [c]);
				for (var f in c) {
					var g = b[c[f]];
					g && e.push(g)
				}
				return d ? e : e.join(",")
			},
			e = ["\u4e07", "\u5343", "\u767e", "\u5341", "\u4e2a"];
			return g(e, ballNum[0]) + "@" + ballNum[1].join(",");
		};
		innerFunc.anyMulti = function (e, c) {
			var d = e.shift().join("");
			return this.direct(e) + "@" + c + "_" + d
		};

		return function (c, d, b, f) {
			// c *= 1;
			d *= 1;
			b *= 1;
			f = f.slice();
			switch ($$.Action.DoPickNumber.GameDB.gameTypeToPlayType(c)) {
			case 0:
				switch (b) {
				case 2: // 和值大小单双
					return innerFunc.direct(f);
				default: // 
					return innerFunc.join(f);
				}
				break;
			case 1:
				switch (d) {
				case 1:
				case 2:
					return innerFunc.direct(f);
				case 4:
					return [212, 245].indexOf(b), innerFunc.join(f);
				case 5:
				case 6:
				case 7:
				case 8:
					return innerFunc.join(f);
				case 9:
					return innerFunc.direct(f);
				case 10:
					return innerFunc.join(f);
				default:
					return !1
				}
			case 2:
				switch (d) {
				case 1:
					return innerFunc.direct(f);
				case 2:
					return 20 == b ? innerFunc.any(f, 2) : 30 == b ? innerFunc.any(f, 3) : innerFunc.join(f);
				case 3:
					return 20 == b ? innerFunc.any(f, 2) : 30 == b ? innerFunc.any(f, 3) : innerFunc.join(f);
				case 4:
					return innerFunc.join(f);
				case 5:
					if (20 == b) return innerFunc.any(f, 2);
					if (-1 !== [123, 234, 345].indexOf(b)) return innerFunc.direct(f);
					[212, 245].indexOf(b);
					return innerFunc.join(f);
				case 6:
					return innerFunc.join(f);
				case 7:
				case 8:
					return 30 == b ? innerFunc.anyNew(f) : innerFunc.join(f);
				case 9:
					return 30 == b ? innerFunc.any(f, 3) : innerFunc.join(f);
				case 10:
					return innerFunc.direct(f);
				case 11:
					return innerFunc.join(f);
				case 14:
					return 20 == b ? innerFunc.any(f, 2) : 30 == b ? innerFunc.any(f, 3) : 40 == b ? innerFunc.any(f, 4) : innerFunc.join(f);
				case 15:
					return 20 == b ? innerFunc.any(f, 2) : 30 == b ? innerFunc.any(f, 3) : innerFunc.direct(f);
				case 17:
				case 18:
					return innerFunc.any(f, 3);
				case 19:
					return -1 !== [12, 4].indexOf(b) ? innerFunc.anyMulti(f, 4) : innerFunc.any(f, 4);
				case 99: // 大小单双－总和
					return innerFunc.direct(f);
				default:
					return !1
				}
			case 3:
				switch (b) {
				case 200: // 冠亚和值
					return innerFunc.join(f);
				default: // 
				return innerFunc.direct(f);
				}
				break;
			case 4:
				switch (d) {
				case 30:
					return [111, 222, 333, 444, 555, 666].join();
				case 31:
				case 32:
					return innerFunc.join(f);
				case 33:
					return innerFunc.direct(f);
				case 34:
				case 35:
					return innerFunc.join(f);
				case 36:
					return [123, 234, 345, 456].join();
				case 37:
					return _.flatten(f).join("");
				case 53:
				case 54:
					return innerFunc.direct(f)
				}
				break;
			case 5:
				switch (d) {
				case 55:
				case 56:
				case 57:
				case 58:
				case 59:
				case 60:
				case 61:
				case 62:
					return innerFunc.join(f);
				case 63:
					return innerFunc.direct(f);
				case 64:
					return innerFunc.join(f);
				case 65:
				case 67:
					return innerFunc.direct(f);
				case 66:
				case 68:
					return innerFunc.join(f);
				case 71:
					return innerFunc.direct(f)
				}
			case 7:
				switch (b) {
				case 1: // 三星复式
					return innerFunc.direct(f);
				case 2: // 前二复式
					return innerFunc.direct(f);
				case 3: // 后二复式
					return innerFunc.direct(f);
				case 4: // 定位胆
					return innerFunc.direct(f);
				case 5: // 和值大小单双
					return innerFunc.direct(f);
				case 6: // 不定位胆
					return innerFunc.join(f);
				case 7: // 前二组选
					return innerFunc.join(f);
				case 8: // 后二组选
					return innerFunc.join(f);
				case 9: // 三星组选
					return innerFunc.join(f);
				case 10: // 和值
					return innerFunc.join(f);
				}
			}
		}
	})()
};

$$.Action.BuyCart = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			View: {},
			Fn: {},
			Tpl: {},
			init: function (){
				thisAction.lotCode = $("#lotCode").val();
				$('body,html').animate({scrollTop: 0},0);
				this.orderInit();
			},
			reinit: function(){

				this.View.orderList.clean();
				this.View.footer.flushCost();
				var k = $$.Action.DoPickNumber.buyCart.get(thisAction.lotCode);
				for (var l in k) this.View.orderList.add(l, k[l]);
			},
			destroy: function(){
				
			},
			orderInit: function(){
				var d = this.View,
					b = this.Fn;
				var f = this.Tpl,
					h = $$.Action.DoPickNumber.GameDB.gameTypeToPlayType(thisAction.lotCode);

				f.order = _.template($("#boBetLiTemplate").html());
				d.orderList = {
					$: $("#orderList")
				};
				d.orderList.clean = function () {
					this.$.children(":gt(0)").remove();
					this.$.children(":eq(0)").show()
				};
				d.orderList.add = function (b, c, d) {
					var e = $$.Action.DoPickNumber.GameDB.gameTypeToPlayType(thisAction.lotCode);
					b = {
						orderID: b,
						ball: thisAction.ballToViewString(e, c.betType, c.subid, c.ball),
						gameName: $$.Action.DoPickNumber.GameDB.getGamePlayName(e, c.betType, c.subid),
						betCount: c.betCount,
						cash: 1 * (1 / c.cashUnit * c.cash * c.betCount).toFixed(3),
						beishu: 1 * c.beishu,
						cashUnit: 1 * c.cashUnit,
						animation: d ? "add" : ""
					};
					$(f.order(b)).insertAfter(this.$.children().eq(0));
					this.$.children().eq(0).hide()
				};
				d.orderList.remove = function (b) {
					this.$.children('[data-orderID="' + b + '"]').remove();
					0 < this.$.children(":gt(0)").length ? this.$.children(":eq(0)").hide() : this.$.children(":eq(0)").show()
				};
				d.footer = {};
				d.footer.flushCost =
					function () {
						var bcTotalMoney = $$.Action.DoPickNumber.buyCart.getTotalCost(thisAction.lotCode);
						var bcTotalCount = $$.Action.DoPickNumber.buyCart.getTotalZhushu(thisAction.lotCode);

						$("#bcTotalMoney").text(bcTotalMoney);
						$("#bcTotalCount").text(bcTotalCount);

						/*
						var bcZhuihaoTotalMoney = 0;
						var zhuihaoQishu = $("#bet_more").val();
						for(var i = 0; i < zhuihaoQishu; i++){
							bcZhuihaoTotalMoney += bcTotalMoney * (i +1);
						}
						$("#bcZhuihaoTotalMoney").text(bcZhuihaoTotalMoney);
						*/
					};

				$("body").off("click.randomNum", "#bcSelectOne").on("click.randomNum", "#bcSelectOne", function () {
					$._ajax({url: stationUrl + "/lottery/getGamemenu.do", data: {lotCode: thisAction.lotCode}}, function (b, e, f, aaa) {
						var g = [];
						if (b) {
							switch (h) {
							case 0:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										-1 == [10].indexOf(1 * b.bettype) && g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								});
								break;
							case 1:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										-1 == [10].indexOf(1 * b.bettype) && g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								});
								break;
							case 2:
								$.map(f.gamemenu, function (b) {
									$.map(b.data,
										function (b) {
											-1 == [14, 15, 17, 18].indexOf(1 * b.bettype) && g.push({
												betType: b.bettype,
												subid: b.subid,
												playid: b.playid,
												groupId: b.groupId,
												playCode: b.playCode,
												lotType: b.lotType
											})
										})
								});
								break;
							case 3:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										-1 == [14].indexOf(1 * b.bettype) && g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								});
								break;
							case 4:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										(38 > 1 * b.bettype || 53 <= 1 * b.bettype) && g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								});
								break;
							case 5:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										if (-1 != [56, 57, 58, 59, 60, 61, 62].indexOf(1 * b.bettype) &&
											1 == b.subid || 71 != b.bettype && "1" == ("" + b.subid)[1]) return !0;
										g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								})
								break;
							case 7:
								$.map(f.gamemenu, function (b) {
									$.map(b.data, function (b) {
										-1 == [10].indexOf(1 * b.bettype) && g.push({
											betType: b.bettype,
											subid: b.subid,
											playid: b.playid,
											groupId: b.groupId,
											playCode: b.playCode,
											lotType: b.lotType
										})
									})
								});
								break;
							}
							var gggameinfo = g;
							if (0 != gggameinfo.length) {
								var g = gggameinfo[_.random(0, gggameinfo.length - 1)], k = $$.Action.DoPickNumber.randomNum.gen(h, g.betType, g.subid);
								while(!k.length){
									g = gggameinfo[_.random(0, gggameinfo.length - 1)], k = $$.Action.DoPickNumber.randomNum.gen(h, g.betType, g.subid);
									console.log(k);
								}

								if(k.length){
									var unitCash = 2; // 单注价格
									var oddsWaterIdx = 0;
									var betCount = $$.Action.DoPickNumber.ballCalc.calc(h, g.betType, g.subid, k);
									var cashUnit = 1;
									var ballNum = k;
									var odds = 0;
									var beishu = 1;
									b = $$.Action.DoPickNumber.buyCart.add(thisAction.lotCode, g.betType, g.subid, unitCash, oddsWaterIdx, betCount, cashUnit, ballNum, odds, g.groupId, g.playId, g.playCode, g.lotType, beishu);
									d.orderList.add(b, $$.Action.DoPickNumber.buyCart.get(thisAction.lotCode, b), !0);
									d.footer.flushCost();
								}
							}
						}
					});
				});
				$("#bcClear").off("click.clear").on("click.clear", function () {
					_.isEmpty($$.Action.DoPickNumber.buyCart.get(thisAction.lotCode)) || $.dialog({
						title: "系统提示",
						content: "确定要清空购彩车?",
						cancelValue: "返回",
						cancel: function () {},
						okValue: "确定",
						ok: function () {
							$$.Action.DoPickNumber.buyCart.clear(thisAction.lotCode);
							d.orderList.clean();
							d.footer.flushCost()
						}
					});
				});

				$("body").off("click.bet", "#bcBet").on("click.bet", "#bcBet", function () {
					if (!_.isEmpty($$.Action.DoPickNumber.buyCart.get(thisAction.lotCode))) {
						// var b = 1 * JB.user.balance.data.balance || 0;
						var b = 100000000000000000;
						if((parseFloat($("#bcTotalMoney").text()) + 0 || 0) >= b) {
							$.dialog({
								title: "系统讯息",
								content: "下注金额超过帐户可用余额！！",
								okValue: "确定",
								ok: function () {}
							})
						} else {
							$._ajax(stationUrl + "/lotteryBet/token.do", function (e, g, l, aaa) {
								$.dialog({
									title: "系统提示",
									content: "确定投注?",
									cancelValue: "返回",
									cancel: function () {},
									okValue: "确定",
									ok: function () {
										var data = {
											lotCode: thisAction.lotCode,
											token: l,
											qiHao: $$.ActionInstance.DoPickNumber.Info.round.now.round,
											bets: $$.Action.DoPickNumber.buyCart.getNewBetString(thisAction.lotCode)
										};
										$.ajax({
											type : "POST",
											url : stationUrl + "/lotteryBet/doBets.do",
											contentType : "application/x-www-form-urlencoded",
											data : {
												data: JSON.stringify(data)
											},
											success : function(result) {
												if(result.success == false){
													// $.alertB(result.msg);
													$.dialog({
														title: "系统讯息",
														content: "下注失败 <br />" + (result.msg ? result.msg : ""),
														okValue: "确定",
														ok: function () {
															location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
														}
													});
												} else if(result.success == true){
													$$.Action.DoPickNumber.buyCart.clear(thisAction.lotCode);
													d.orderList.clean();
													d.footer.flushCost();
													$.dialog({
														title: "系统讯息",
														content: "下注成功",
														okValue: "继续投注",
														ok: function () {
															location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
														},
														canelValue: "查看记录",
														canel: function () {
															location.href = stationUrl + "/betting_record_lottery.do?backUrl=" + stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
														}
													})
												}
											}
										});
									}
								})
							});
						}
					}
				});

				$("#orderList").off("click.del", "em.roundDeleBtn").on("click.del", "em.roundDeleBtn", function () {
					var b = $(this).parents("li").attr("data-orderID");
					$$.Action.DoPickNumber.buyCart.del(thisAction.lotCode, b);
					d.footer.flushCost();
					// $(this).parents("li").remove();
					setTimeout(function () {
						d.orderList.remove(b)
					}, 200)
				});

				d.orderList.clean();
				d.footer.flushCost();
				var k = $$.Action.DoPickNumber.buyCart.get(thisAction.lotCode), l;
				for (l in k) d.orderList.add(l, k[l]);

				// e.find("nav > .title, #betTypeText").text("\u52a0\u8f7d\u4e2d...");
				// $("#bpRound, #bpCountDown").text("\u66f4\u65b0\u4e2d ...");
				// loading.show();
				/*
				JB.bet.getSetting(function (b, c, d) {
					loading.hide();
					b ? 1 * d.quick_bet_flag ? $("#bcFuzzyBet").show() : $("#bcFuzzyBet").hide() : dialog({
						title: "\u7cfb\u7edf\u8baf\u606f",
						content: c,
						okValue: "\u786e\u5b9a",
						ok: function () {
							goBack()
						}
					}).showModal()
				});
				b.countDown.start(thisAction.lotCode)
				*/
			}
		};
	},
	ballToViewString : function (e, c, d, b) {
		c *= 1;
		d *= 1;
		var f = function (b, c) {
				var d = [],
					e;
				for (e in b) {
					var f = _.isArray(b[e]) ? b[e].join("") : b[e];
					d.push(f)
				}
				return c ? d : d.join(",")
			},
			g = function (b, c, d) {
				var e = [];
				_.isArray(c) || (c = [c]);
				for (var f in c) {
					var g = b[c[f]];
					g && e.push(g)
				}
				return d ? e : e.join(",")
			};
		switch (1 * e) {
		case 0:
			switch (c + "." + d) {
			case "2.2": // 和值大小单双
				return _.flatten(b).join("")
			default :
				return 1 == b.length ? _.flatten(b).join(",") : f(b)
			}
			break;
		case 1:
			e = ["\u767e", "\u5341", "\u4e2a"];
			var h = ["", "\u5927", "\u5c0f", "\u5355", "\u53cc"];
			switch (c + "." + d) {
			case "2.12":
			case "2.23":
				return [g(h, b[0], !0).join(""), g(h, b[1], !0).join("")].join();
			default:
				return 1 == b.length ?
					_.flatten(b).join(",") : f(b)
			}
		case 2:
			e = ["\u4e07", "\u5343", "\u767e", "\u5341", "\u4e2a"];
			var h = ["", "\u5927", "\u5c0f", "\u5355", "\u53cc"],
				k = ["", "\u987a\u5b50", "\u5bf9\u5b50", "\u8c79\u5b50"];
			switch (c + "." + d) {
			case "19.4":
			case "19.12":
				return f([b[1], b[2]]) + "@" + g(e, b[0]);
			case "7.30":
			case "8.30":
				return g(e, b[0]) + "@" + b[1].join(",");
			case "5.20":
			case "19.6":
			case "19.24":
				return g(e, b[0]) + "|" + f([b[1]]);
			case "2.20":
			case "2.30":
			case "3.20":
			case "3.30":
			case "14.20":
			case "14.30":
			case "14.40":
			case "15.20":
			case "15.30":
			case "17.30":
			case "18.30":
				return g(e,
					b[0], !0).join("") + "|" + b[1].join(",");
			case "15.123":
			case "15.234":
			case "15.345":
				return g(k, b);
			case "10.12":
			case "10.45":
				return [g(h, b[0], !0).join(""), g(h, b[1], !0).join("")].join();
			case "10.123":
			case "10.345":
				return [g(h, b[0], !0).join(""), g(h, b[1], !0).join(""), g(h, b[2], !0).join("")].join();
			case "11.123":
			case "11.234":
			case "11.345":
				return g(k, b);
			case "99.612345": // 和值大小单双
				return _.flatten(b).join("")
			default:
				return 1 == b.length ? _.flatten(b).join(",") : f(b)
			}
		case 3:
			switch (c + "." + d) {
			case "3.200": // 冠亚和值
				return _.flatten(b).join(",");
			default :
				return e = "\u51a0; \u4e9a; \u5b63; \u56db; \u4e94; \u516d; \u4e03; \u516b; \u4e5d; \u5341".split(";"), f(b).replace(/\,*$/, "");
			}
			break;
		case 4:
			d = [0, 11, 22, 33, 44, 55, 66];
			switch (c) {
			case 30:
				return "\u4e09\u540c\u53f7\u901a\u9009";
			case 31:
			case 32:
				return _.flatten(b).join(",");
			case 33:
				return [g(d, b[0], !0).join(""), b[1].join("")].join();
			case 34:
			case 35:
				return _.flatten(b).join("");
			case 36:
				return "\u4e09\u8fde\u53f7\u901a\u9009";
			case 37:
				return "\u548c\u503c" + _.flatten(b)[0];
			case 53:
			case 54:
				return f(b)
			}
			break;
		case 5:
			switch (c) {
			case 55:
			case 56:
			case 57:
			case 58:
			case 59:
			case 60:
			case 61:
			case 62:
			case 64:
				return _.flatten(b).join(",");
			case 63:
				return f(b);
			case 65:
				return f(b);
			case 66:
				return 2100 < d ? _.flatten(b).join(",") : f(b);
			case 67:
				return f(b);
			case 68:
				return 3100 < d ? _.flatten(b).join(",") : f(b);
			case 71:
				return f(b)
			}
		case 7:
			switch (c + "." + d) {
			case "1.1": // 三星复式
			case "1.2": // 前二复式
			case "1.3": // 后二复式
			case "2.4": // 定位胆
			case "3.6": // 不定位胆
			case "4.7": // 前二组选
			case "4.8": // 后二组选
			case "5.9": // 三星组选
			case "6.10": // 和值
				return 1 == b.length ? _.flatten(b).join(",") : f(b)
			case "2.5": // 和值大小单双
				return _.flatten(b).join("")
			}
		}
		return !1
	}
};

$$.Action.DoIntelligentSelection = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			View: {},
			Fn: {},
			Tpl: {},
			Data: {roundList: []},
			init: function (){
				thisAction.lotCode = $("#lotCode").val();

				$('body,html').animate({scrollTop: 0},0);

				this.fbOrderViewInit();
				this.fbBetInit();
				this.loadOrderList();
			},
			reinit: function(){
				this.loadOrderList();
			},
			destroy: function(){
				
			},
			loadOrderList: function() {
				var d = this.View,
				b = this.Fn,
				f = this.Data,
				g = this.Tpl;

				// $("#fbRound, #fbCountDown").text("\u8f7d\u5165\u4e2d ...");
				d.OrderList.clean();
				0 == $$.Action.DoPickNumber.buyCart.getBetString(thisAction.lotCode, 1).length ? $.dialog({
					title: "系统提示",
					content: "购彩车是空的",
					okValue: "返回",
					ok: function () {
						// goBack()
						location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
					},
					canel: function(){
						location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
					}
				}) : (f.cartTrace = thisAction.BetTrace.calcProfit($$.Action.DoPickNumber.buyCart.getBetString(thisAction.lotCode,
					1)), $._ajax({url: stationUrl + "/lottery/getQiHaoForChase.do", data: {lotCode: thisAction.lotCode}}, function (e, g, l, aaa) {
						e ? 0 == l.length ? $.dialog({
							title: "系统提示",
							content: "无期数可供追号",
							okValue: "确定",
							ok: function () {
								// goBack()
								location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
							},
							canel: function(){
								location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
							}
						}) : (f.roundList = l, f.cartTrace.error && $.dialog({
							title: "系统提示",
							content: f.cartTrace.error,
							okValue: "确定",
							ok: function () {}
						}), d.OrderList.renderView(Math.min(10, f.roundList.length)).calcView()) : $.dialog({
							title: "系统提示",
							content: g,
							okValue: "确定",
							ok: function () {
								goBack()
							}
						})
					}));
			},
			fbOrderViewInit: function () {
				var d = this.View,
					b = this.Data,
					f = this.Tpl;
				d.OrderList = {
					$: $("#fbOrderList"),
					$qihaoCount: $("#qihaoCount")
				};
				d.OrderList.clean = function () {
					this.$.empty()
				};
				d.OrderList.renderView = function (c) {
					this.clean();
					c = Math.min(1 * c, b.roundList.length);
					for (var d = "", e = 0; e < c; e++) d += f.list({
						num: e + 1,
						zhuihaoBeishu: e + 1
					});
					this.$qihaoCount.val(c);
					this.$.html(d);
					this.flushRound();
					return this
				};
				d.OrderList.calcView = function () {
					var c = 0;
					this.$.find("tr").each(function (d, e) {
						var l = 1 * $(this).find('input[type="number"]').val() || 0,
							m = {};
						0 == l ? m = {
							betCost: "--",
							betTotalCost: "--",
							betMinEarn: "--",
							betMaxEarn: "--",
							betMinEarnPercent: "--",
							betMaxEarnPercent: "--"
						} : 0 == b.cartTrace.minProfit ? (l *= b.cartTrace.totalCost, c += l, m = {
							betCost: 1 * l.toFixed(2),
							betTotalCost: 1 * c.toFixed(2),
							betMinEarn: "--",
							betMaxEarn: "--",
							betMinEarnPercent: "--",
							betMaxEarnPercent: "--"
						}) : (m = thisAction.BetTrace.generatorBetTrace(1, 1, l, b.cartTrace.totalCost, [b.cartTrace.minProfit, b.cartTrace.maxProfit], c, 1), m = m.betTraceData[0], c = m.betTotalCost, $.each(m, function (b, c) {
							try {
								m[b] = 1 * c.toFixed(2)
							} catch (d) {}
						}));
						// m.betRate = m.betRate * (d + 1); // 赔率 // 2016-01-30 4739475847947 ADD
						// m.betCost = m.betRate * m.betCost; // 金额 // 2016-01-30 4739475847947 ADD
						$(this).find(".betInfo").html(f.detail(m))
					});
					this.flushRound();
					$("#fbCount").text(this.$.find("tr").length);
					$("#fbCash").text(1 * c.toFixed(2));
					return this
				};
				d.OrderList.setRate = function (b) {
					this.$.find('input[type="number"]').each(function (c) {
						$(this).val(b[c] || 0)
					});
					this.calcView();
					return this
				};
				d.OrderList.getRate = function () {
					var b = [];
					this.$.find('input[type="number"]').each(function () {
						b.push(1 * $(this).val())
					});
					return b
				};
				d.OrderList.flushRound = function () {
					this.$.find("tr").each(function (c) {
						void 0 != b.roundList[c] ? $(this).find(".round").text(b.roundList[c]) :
							$(this).remove()
					});
					return this;
				}
			},
			fbBetInit: function () {
				var d = this.View,
					b = this.Fn,
					f = this.Data,
					g = this.Tpl;
				g.list = _.template($("#doIntelligentSelectionTrTemplate").html());
				g.detail = _.template($("#doIntelligentSelectionDetialTemplate").html());

				$("#fbOrderList").off("change.calcView", "input").on("change.calcView", "input", function (b) {
					var c = 1 * $(this).val();
					isNaN(c) ? $(this).val(1) : "change" == b.type &&
						0 > c && $(this).val(0);
					d.OrderList.calcView()
				});
				$("#qihaoCount").off("change.renderView").on("change.renderView", function(){
					d.OrderList.renderView(Math.min($(this).val(), f.roundList.length)).calcView()
				});
				$("#fbBet").off("click.tobet").on("click.tobet", function () {
					// var b = 1 * JB.user.balance.data.balance || 0;
					var b = 100000000000000000;
					if((parseFloat($("#fbCash").text()) + 0 || 0) >= b) {
						$.dialog({
							title: "系统讯息",
							content: "下注金额超过帐户可用余额！！",
							okValue: "确定",
							ok: function () {
								location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
							}
						})
					} else {
						$._ajax(stationUrl + "/lotteryBet/token.do", function (e, g, l, aaa) {
							$.dialog({
								title: "系统提示",
								content: "确定投注?",
								cancelValue: "取消",
								cancel: function () {},
								okValue: "确定",
								ok: function () {
									var b = $("#fbDlgStopBet").prop("checked"),
										e = [];
									e.push(b ? 1 : 0);
									$.each(d.OrderList.getRate(), function (b, c) {
										e.push(f.roundList[b] + "|" + c);
									});
									
									var data = {
										lotCode: thisAction.lotCode,
										token: l,
										qiHao: $$.ActionInstance.DoPickNumber.Info.round.now.round,
										bets: $$.Action.DoPickNumber.buyCart.getNewBetString(thisAction.lotCode),
										tractData: e
									};
									$.ajax({
										type : "POST",
										url : stationUrl + "/lotteryBet/doBets.do",
										data : {
											data: JSON.stringify(data)
										},
										success : function(result) {
											if(result.success == false){
												// $.alertB(result.msg);
												$.dialog({
													title: "系统讯息",
													content: "下注失败 <br />" + (result.msg ? result.msg : ""),
													okValue: "确定",
													ok: function () {
														location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
													}
												});
											} else if(result.success == true){
												$.dialog({
													title: "系统讯息",
													content: "下注成功",
													okValue: "继续投注",
													ok: function () {
														location.href = stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
													},
													canelValue: "查看记录",
													canel: function () {
														location.href = stationUrl + "/betting_record_lottery.do?backUrl=" + stationUrl + "/do_pick_number.do?lotCode=" + thisAction.lotCode;
													}
												})
											}
										}
									});
								}
							})
						});
					}
				});
			}
		};
	},
	BetTrace : {
			buyitemsinfo: [],
			profitArr: {},
			temp: [],
			maxProfit: 0,
			minProfit: 0,
			totalCost: 0,
			beforeCost: 0,
			expandArray: function (e) {
				var c, d, b, f, g, h, k, l, m, p, n;
				if (0 === e.length) return !1;
				e = e.slice();
				n = e.splice(0, 1)[0];
				n instanceof Array || (n = n.toString().split(","));
				g = 0;
				for (l = e.length; g < l; g++)
					for (b = e[g], d = n.slice(), n = [], h = 0, m = b.length; h < m; h++)
						for (f = b[h], k = 0, p = d.length; k < p; k++) c = d[k], n.push("" + c + f);
				return n
			}, combineProfitArray: function (e, c, d) {
				var b, f, g;
				if (!(c instanceof Array)) return !1;
				f = 0;
				for (g = c.length; f < g; f++) b =
					c[f], null !== b && void 0 !== b && (e.hasOwnProperty(b) ? e[b] += d : e[b] = d);
				return e
			}, addPrefix: function (e, c) {
				var d;
				switch (typeof c) {
				case "string":
				case "number":
					return "" + e + c;
				default:
					if (c instanceof Array) {
						c = c.slice();
						for (d in c) c[d] instanceof Array && (c[d] = c[d].join("")), c[d] = "" + e + c[d];
						return c
					}
					return !1
				}
			}, generatorCombine3: function (e) {
				var c, d;
				if (3 !== e.length) return alert("generatorCombine3 \u4f20\u5165\u957f\u5ea6\u9519\u8bef"), !1;
				e = e.slice();
				c = this.expandArray(e);
				e.splice(0, 1);
				d = this.expandArray(e);
				e.splice(0,
					1);
				e = this.expandArray(e);
				return [c, d, e]
			}, generatorGroup: function (e, c) {
				var d, b, f, g, h, k, l, m, p, n, t, r, w, x, u, A, C, y, v, z, q, F, H, E, G, K, O, R, P;
				null == e && (e = 2);
				null == c && (c = []);
				P = [];
				e = parseInt(e);
				c = c instanceof Array ? c.slice() : c.splt(",");
				if (1 === e) return c;
				if (2 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = 0, r = c.length; m < r; m++) x = c[m], P.push("" + w + x);
				else if (3 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = p = 0, t = c.length; 0 <= t ? p < t : p > t; m = 0 <= t ? ++p : --p)
							for (x = c[m], r = n = l = m + 1, g = c.length; l <= g ? n < g : n > g; r = l <= g ? ++n : --n) m = c[r],
								P.push("" + w + x + m);
				else if (4 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = t = 0, n = c.length - 2; 0 <= n ? t < n : t > n; m = 0 <= n ? ++t : --t)
							for (x = c[m], r = l = g = m + 1, y = c.length - 1; g <= y ? l < y : l > y; r = g <= y ? ++l : --l)
								for (m = c[r], p = h = A = r + 1, v = c.length; A <= v ? h < v : h > v; p = A <= v ? ++h : --h) r = c[p], P.push("" + w + x + m + r);
				else if (5 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = n = 0, l = c.length - 3; 0 <= l ? n < l : n > l; m = 0 <= l ? ++n : --n)
							for (x = c[m], r = g = h = m + 1, y = c.length - 2; h <= y ? g < y : g > y; r = h <= y ? ++g : --g)
								for (m = c[r], p = A = v = r + 1, k = c.length - 1; v <= k ? A < k : A > k; p = v <= k ? ++A : --A)
									for (r = c[p],
										t = C = z = p + 1, d = c.length; z <= d ? C < d : C > d; t = z <= d ? ++C : --C) p = c[t], P.push("" + w + x + m + r + p);
				else if (6 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = g = 0, l = c.length - 4; 0 <= l ? g < l : g > l; m = 0 <= l ? ++g : --g)
							for (x = c[m], r = A = y = m + 1, h = c.length - 3; y <= h ? A < h : A > h; r = y <= h ? ++A : --A)
								for (m = c[r], p = C = v = r + 1, k = c.length - 2; v <= k ? C < k : C > k; p = v <= k ? ++C : --C)
									for (r = c[p], t = F = z = p + 1, d = c.length - 1; z <= d ? F < d : F > d; t = z <= d ? ++F : --F)
										for (p = c[t], n = H = q = t + 1, b = c.length; q <= b ? H < b : H > b; n = q <= b ? ++H : --H) t = c[n], P.push("" + w + x + m + r + p + t);
				else if (7 === e)
					for (; c.length >= e;)
						for (w = c.splice(0,
							1)[0], m = y = 0, g = c.length - 5; 0 <= g ? y < g : y > g; m = 0 <= g ? ++y : --y)
							for (x = c[m], r = v = h = m + 1, A = c.length - 4; h <= A ? v < A : v > A; r = h <= A ? ++v : --v)
								for (m = c[r], p = z = k = r + 1, C = c.length - 3; k <= C ? z < C : z > C; p = k <= C ? ++z : --z)
									for (r = c[p], t = d = F = p + 1, q = c.length - 2; F <= q ? d < q : d > q; t = F <= q ? ++d : --d)
										for (p = c[t], n = b = H = t + 1, E = c.length - 1; H <= E ? b < E : b > E; n = H <= E ? ++b : --b)
											for (t = c[n], l = f = G = n + 1, K = c.length; G <= K ? f < K : f > K; l = G <= K ? ++f : --f) n = c[l], P.push("" + w + x + m + r + p + t + n);
				else if (8 === e)
					for (; c.length >= e;)
						for (w = c.splice(0, 1)[0], m = g = 0, y = c.length - 6; 0 <= y ? g < y : g > y; m = 0 <= y ? ++g : --g)
							for (x =
								c[m], r = h = A = m + 1, v = c.length - 5; A <= v ? h < v : h > v; r = A <= v ? ++h : --h)
								for (m = c[r], p = k = C = r + 1, z = c.length - 4; C <= z ? k < z : k > z; p = C <= z ? ++k : --k)
									for (r = c[p], t = d = F = p + 1, q = c.length - 3; F <= q ? d < q : d > q; t = F <= q ? ++d : --d)
										for (p = c[t], n = b = H = t + 1, E = c.length - 2; H <= E ? b < E : b > E; n = H <= E ? ++b : --b)
											for (t = c[n], l = f = G = n + 1, K = c.length - 1; G <= K ? f < K : f > K; l = G <= K ? ++f : --f)
												for (n = c[l], u = l = O = l + 1, R = c.length; O <= R ? l < R : l > R; u = O <= R ? ++l : --l) u = c[u], P.push("" + w + x + m + r + p + t + n + u);
				else alert("\u957f\u5ea6\u7ec4\u9009 err " + e);
				return P
			}, anyDirect: function (e, c) {
				var d, b, f, g, h, k, l, m, p, n,
					t, r, w, x, u, A, C, y, v, z, q, F, H, E, G, K, O, R, P, S, U, Q, T;
				G = [];
				if (2 === e)
					for (w = f = 0, n = c.length - e; 0 <= n ? f <= n : f >= n; w = 0 <= n ? ++f : --f) {
						if (null !== c[w])
							for (t = c[w], h = 0, m = t.length; h < m; h++)
								for (d = t[h], x = g = r = w + 1, u = c.length; r <= u ? g < u : g > u; x = r <= u ? ++g : --g)
									if (null !== c[x])
										for (A = c[x], l = 0, p = A.length; l < p; l++) b = A[l], k = "" + w + x, G[k] instanceof Array || (G[k] = []), G[k].push("" + d + b)
					} else if (3 === e)
						for (w = p = 0, z = c.length - e; 0 <= z ? p <= z : p >= z; w = 0 <= z ? ++p : --p) {
							if (null !== c[w])
								for (q = c[w], n = 0, g = q.length; n < g; n++)
									for (d = q[n], x = t = F = w + 1, H = c.length - 1; F <= H ? t < H : t > H; x =
										F <= H ? ++t : --t)
										if (null !== c[x])
											for (E = c[x], r = 0, l = E.length; r < l; r++)
												for (b = E[r], h = u = C = x + 1, y = c.length; C <= y ? u < y : u > y; h = C <= y ? ++u : --u)
													if (null !== c[h])
														for (v = c[h], A = 0, m = v.length; A < m; A++) f = v[A], k = "" + w + x + h, G[k] instanceof Array || (G[k] = []), G[k].push("" + d + b + f)
						} else if (4 === e)
							for (w = K = 0, r = c.length - e; 0 <= r ? K <= r : K >= r; w = 0 <= r ? ++K : --K) {
								if (null !== c[w])
									for (u = c[w], O = 0, l = u.length; O < l; O++)
										for (d = u[O], x = R = A = w + 1, C = c.length - 2; A <= C ? R < C : R > C; x = A <= C ? ++R : --R)
											if (null !== c[x])
												for (y = c[x], P = 0, m = y.length; P < m; P++)
													for (b = y[P], h = S = v = x + 1, z = c.length -
														1; v <= z ? S < z : S > z; h = v <= z ? ++S : --S)
														if (null !== c[h])
															for (q = c[h], U = 0, p = q.length; U < p; U++)
																for (f = q[U], t = Q = F = h + 1, H = c.length; F <= H ? Q < H : Q > H; t = F <= H ? ++Q : --Q)
																	if (null !== c[t])
																		for (E = c[t], T = 0, n = E.length; T < n; T++) g = E[T], k = "" + w + x + h + t, G[k] instanceof Array || (G[k] = []), G[k].push("" + d + b + f + g)
							} else alert("\u4efb" + e + "\u76f4\u9009\u9519\u8bef");
				return G
			}, anyGroup: function (e, c, d, b) {
				var f, g;
				null == e && (e = 2);
				null == c && (c = 2);
				null == d && (d = []);
				null == b && (b = "12345");
				g = [];
				e = parseInt(e);
				c = parseInt(c);
				d instanceof Array || (d = d.split(","));
				b = b.match(/\d/g);
				if (b.length < e) return alert("\u7ec4\u9009\u590d\u5f0f\u53c2\u6570\u9519\u8bef"), !1;
				d = this.generatorGroup(c, d);
				f = this.generatorGroup(e, b);
				e = 0;
				for (b = f.length; e < b; e++) c = f[e], g[c] = d;
				return g
			}, anySUM: function (e, c, d) {
				var b, f, g;
				null == e && (e = 2);
				null == c && (c = []);
				null == d && (d = "12345");
				g = [];
				e = parseInt(e);
				c instanceof Array || (c = c.split(","));
				for (b in c) c[b] instanceof Array && (c[b] = c[b].join(""));
				d = d.match(/\d/g);
				if (d.length < e) return alert("\u7ec4\u9009\u590d\u5f0f\u53c2\u6570\u9519\u8bef"), !1;
				f = this.generatorGroup(e,
					d);
				e = 0;
				for (b = f.length; e < b; e++) d = f[e], g[d] = c;
				return g
			}, multipleWinning: function (e, c) {
				var d, b, f, g, h, k;
				for (f in e) this.temp[f] instanceof Array || (this.temp[f] = []), this.combineProfitArray(this.temp[f], e[f], c);
				k = h = 0;
				for (f in this.temp) {
					b = [];
					for (g in this.temp[f]) b.push(this.temp[f][g]);
					d = Math.max.apply(null, b);
					b = Math.min.apply(null, b); - Infinity === d && (d = 0);
					Infinity === b && (b = 0);
					h += d;
					k += b
				}
				this.profitArr.maxProfit = h;
				this.profitArr.minProfit = k;
				return {
					maxProfit: h,
					minProfit: k
				}
			}, calcProfit: function (e) {
				var c, d, b,
					f, g;
				this.profitArr = {};
				this.temp = [];
				if (!$.isArray(e) || 0 === e.length) return !1;
				e.sort();
				this.buyitemsinfo.sort();
				if (e.join() !== this.buyitemsinfo.join()) {
					this.buyitemsinfo = e.slice();
					if (0 === e.length) return {
						minProfit: this.minProfit = 0,
						maxProfit: this.maxProfit = 0,
						totalCost: this.totalCost = 0
					};
					for (b in e) {
						d = e[b].split("|");
						f = {};
						f.gametype = d[0];
						f.bettype = d[1];
						f.subid = d[2].toString();
						f.cash = parseInt(d[3]) / parseInt(d[6]);
						f.totalCost = d[5] * f.cash * d[9];
						f.rate = d[8].match(",") ? d[8].split(",") : d[8];
						f.balls =
							d[7];
						f.ballArgs = [];
						f.balls.match("@") && (d = f.balls.split("@"), f.balls = d[0], d[1] && (f.ballArgs = d[1].split("_")));
						f.balls = f.balls.split(",");
						d = -1 !== [24, 28, 29, 30, 31].indexOf(f.gametype) ? /\d\d/g : /\d/g;
						for (c in f.balls) f.balls[c] = f.balls[c].match(d);
						(25 === f.gametype || 26 === f.gametype) && 37 <= f.bettype && 52 >= f.bettype && (f.bettype = 37);
						e[b] = f
					}
					f = [];
					b = [];
					d = this.totalCost = 0;
					for (g = e.length; d < g; d++) c = e[d], f.push(c.gametype), b.push(c.bettype + "," + c.subid), this.totalCost += c.totalCost;
					if (1 < $.unique(f).length) return {
						minProfit: this.minProfit = !1,
						maxProfit: this.maxProfit = !1,
						totalCost: this.totalCost
					};
					if (1 < $.unique(b).length) return {
						minProfit: this.minProfit = !1,
						maxProfit: this.maxProfit = !1,
						totalCost: this.totalCost,
						error: "您选择的多种玩法无法计算盈利，将以 -- 呈现。"
					};

					// 重要
					/*
							// 捷豹
							{
							1: [1, 2, 10], // [ 福彩3D, 体彩P3, 上海时时乐]
							2: [11, 12, 13, 14, 15, 35, 36, 37], // [ 天津时时彩, 重庆时时彩, 江西时时彩, 新疆时时彩, 黑龙江时时彩, 三分彩, 五分彩, 分分彩]
							3: [24], // [ 北京赛车 ]
							4: [25, 26], // [ 江苏快三, 安徽快3]
							5: [28, 29, 30, 31] // [ 山东11选5, 上海11选5, 江西11选5, 广东11选5]
							}
							// 本地
							{
							1: [14, 13, 27],
							2: [26, 6, 11, 10, -1, 8, 9, 7],
							3: [28],
							4: [30, 29],
							5: [17, 16, 15, 18]
							}
					*/
					switch (f[0]) {
					case "FC3D":
					case "PL3":
					case "SHSSL":
						this.calc3Ssc(e);
						break;
					case "TJSSC":
					case "CQSSC":
					case "JXSSC":
					case "XJSSC":
					case "HLJSSC":
					case "EFC":
					case "FFC":
					case "WFC":
					case "HKWFC":
					case "AMWFC":
					case "SFC":
					case "ESFC":
						this.calcSsc(e);
						break;
					case "BJSC":
					case "XYFT":
					case "LXYFT":
						this.calcPK10(e);
						break;
					case "JSK3":
					case "AHK3":
						this.calcQuick3(e);
						break;
					case "SD11X5":
					case "SH11X5":
					case "GX11X5":
					case "JX11X5":
					case "GD11X5":
						this.calc11Select5(e);
						break;
					case "JSSB3":
					case "HBK3":
					case "AHK3":
					case "SHHK3":
					case "HEBK3":
					case "GXK3":
					case "BJK3":
					case "GSK3":
					case "FFK3":
					case "WFK3":
					case "JPK3":
					case "KRK3":
					case "HKK3":
					case "AMK3":
					case "JXK3":
						break;
					default:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "此彩种暂时无法追号。"
						}
					}
					if (0 === this.profitArr.length) this.minProfit = this.maxProfit = 0;
					else {
						this.maxProfit = Number.MIN_VALUE;
						this.minProfit = Number.MAX_VALUE;
						for (c in this.profitArr) e = this.profitArr[c], this.maxProfit = Math.max(this.maxProfit, e), this.minProfit = Math.min(this.minProfit, e);
						this.maxProfit =
							parseFloat(this.maxProfit.toFixed(5));
						this.minProfit = parseFloat(this.minProfit.toFixed(5))
					}
					this.profitArr = {};
					console.debug(this.temp);
					this.temp = []
				}
				return {
					minProfit: this.minProfit,
					maxProfit: this.maxProfit,
					totalCost: this.totalCost
				}
			}, calc3Ssc: function (e) {
				var c, d, b, f, g;
				d = 0;
				for (f = e.length; d < f; d++) switch (b = e[d], c = parseInt(b.bettype), g = parseInt(b.subid), c) {
				case 1:
					switch (g) {
					case 1123:
						this.multipleWinning(b.balls, b.cash * b.rate);
						break;
					case 12:
					case 23:
						this.combineProfitArray(this.profitArr, this.expandArray(b.balls),
							b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 2:
					switch (g) {
					case 12:
					case 23:
						this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 4:
					switch (g) {
					case 212:
					case 223:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate);
						break;
					case 1123:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					case 2123:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 5:
					this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate);
					break;
				case 6:
					this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate);
					break;
				case 7:
					this.combineProfitArray(this.profitArr, this.addPrefix("3#", b.balls), b.cash * b.rate[0]);
					this.combineProfitArray(this.profitArr, this.addPrefix("6#", b.balls), b.cash * b.rate[1]);
					break;
				case 8:
					this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
					break;
				case 9:
					this.combineProfitArray(this.profitArr,
						this.expandArray(b.balls), b.cash * b.rate);
					break;
				case 10:
					return {
						minProfit: this.minProfit = !1,
						maxProfit: this.maxProfit = !1,
						totalCost: this.totalCost,
						error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
					};
				default:
					return !1
				}
				return !0
			}, calcPK10: function (e) {
				var c, d, b, f, g;
				d = 0;
				for (f = e.length; d < f; d++) switch (b = e[d], c = parseInt(b.bettype), g = parseInt(b.subid), c) {
				case 1:
					switch (g) {
					case 1:
						this.combineProfitArray(this.profitArr, b.balls[0], b.cash * b.rate);
						break;
					case 12:
					case 123:
						this.combineProfitArray(this.profitArr,
							this.expandArray(b.balls), b.cash * b.rate);
						break;
					case 10:
						this.multipleWinning(b.balls, b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 14:
					return {
						minProfit: this.minProfit = !1,
						maxProfit: this.maxProfit = !1,
						totalCost: this.totalCost,
						error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
					};
				default:
					return !1
				}
				return !0
			}, calcSsc: function (e) {
				var c, d, b, f, g;
				d = 0;
				for (f = e.length; d < f; d++) switch (b = e[d], c = parseInt(b.bettype), g = parseInt(b.subid), c) {
				case 1:
					switch (g) {
					case 12345:
					case 2345:
					case 123:
					case 234:
					case 345:
					case 12:
					case 45:
						this.combineProfitArray(this.profitArr,
							this.expandArray(b.balls), b.cash * b.rate);
						break;
					case 112345:
						this.multipleWinning(b.balls, b.cash * b.rate);
						break;
					case 20:
						this.multipleWinning(this.anyDirect(2, b.balls), b.cash * b.rate);
						break;
					case 30:
						this.multipleWinning(this.anyDirect(3, b.balls), b.cash * b.rate);
						break;
					case 40:
						this.multipleWinning(this.anyDirect(4, b.balls), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 2:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, this.addPrefix("3#", b.balls), b.cash * b.rate[0]);
						this.combineProfitArray(this.profitArr,
							this.addPrefix("6#", b.balls), b.cash * b.rate[1]);
						break;
					case 12:
					case 45:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					case 20:
					case 30:
						this.multipleWinning(this.anySUM(b.ballArgs[0], b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 3:
					switch (g) {
					case 12:
					case 45:
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					case 20:
					case 30:
						this.multipleWinning(this.anySUM(b.ballArgs[0], b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 4:
					switch (g) {
					case 12:
					case 45:
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 5:
					switch (g) {
					case 123:
					case 234:
					case 345:
						c = this.generatorCombine3(b.balls);
						this.combineProfitArray(this.profitArr, c[0], b.cash * b.rate[0]);
						this.combineProfitArray(this.profitArr, c[1], b.cash * b.rate[1]);
						this.combineProfitArray(this.profitArr, c[2], b.cash * b.rate[2]);
						break;
					case 212:
					case 245:
						this.combineProfitArray(this.profitArr,
							this.generatorGroup(2, b.balls), b.cash * b.rate);
						break;
					case 1123:
					case 1345:
					case 11234:
					case 12345:
					case 112345:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					case 2123:
					case 2345:
					case 21234:
					case 22345:
					case 212345:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate);
						break;
					case 312345:
						this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate);
						break;
					case 20:
						this.multipleWinning(this.anyGroup(b.ballArgs[0], 2, b.balls, b.ballArgs[1]),
							b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 6:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, this.addPrefix("3#", b.balls), b.cash * b.rate[0]);
						this.combineProfitArray(this.profitArr, this.addPrefix("6#", b.balls), b.cash * b.rate[1]);
						break;
					case 12:
					case 45:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 7:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate);
						break;
					case 30:
						this.multipleWinning(this.anyGroup(b.ballArgs[0], 2, b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 8:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate);
						break;
					case 30:
						this.multipleWinning(this.anyGroup(b.ballArgs[0], 3, b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 9:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 10:
					switch (g) {
					case 12:
					case 45:
					case 123:
					case 345:
						this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 11:
					switch (g) {
					case 123:
					case 234:
					case 345:
						this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				case 14:
				case 15:
				case 17:
				case 18:
					return {
						minProfit: this.minProfit = !1,
						maxProfit: this.maxProfit = !1,
						totalCost: this.totalCost,
						error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
					};
				case 19:
					switch (g) {
					case 24:
						this.multipleWinning(this.anyGroup(b.ballArgs[0], 4, b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					case 12:
						b.balls = this.expandArray([b.balls[0], this.generatorGroup(2, b.balls[1])]);
						this.multipleWinning(this.anySUM(b.ballArgs[0], b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					case 6:
						this.multipleWinning(this.anyGroup(b.ballArgs[0], 2, b.balls, b.ballArgs[1]), b.cash * b.rate);
						break;
					case 4:
						this.multipleWinning(this.anySUM(b.ballArgs[0], this.expandArray(b.balls), b.ballArgs[1]), b.cash * b.rate);
						break;
					default:
						return !1
					}
					break;
				default:
					return !1
				}
				return !0
			}, calcQuick3: function (e) {
				var c, d, b, f;
				d = 0;
				for (f = e.length; d < f; d++) switch (b = e[d], c = parseInt(b.bettype), parseInt(b.subid), c) {
				case 30:
				case 31:
				case 32:
				case 36:
					this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
					break;
				case 33:
					this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate);
					break;
				case 34:
					this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate);
					break;
				case 35:
					this.combineProfitArray(this.profitArr,
						this.generatorGroup(2, b.balls), b.cash * b.rate);
					break;
				case 37:
					this.combineProfitArray(this.profitArr, [b.balls[0].join("")], b.cash * b.rate);
					break;
				case 53:
					1 === b.balls[0].length ? b.balls[1] = this.generatorGroup(2, b.balls[1]) : b.balls[0] = b.balls[0].join("");
					this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate);
					break;
				case 54:
					this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate);
					break;
				default:
					return !1
				}
				return !0
			}, calc11Select5: function (e) {
				var c, d, b, f, g;
				d = 0;
				for (f =
					e.length; d < f; d++) switch (b = e[d], c = parseInt(b.bettype), g = parseInt(b.subid), c) {
				case 55:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(1, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 56:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash *
							b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 57:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 58:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(4, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 59:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(5, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 60:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(6, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 61:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr,
							this.generatorGroup(7, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 62:
					switch (g) {
					case 0:
						this.combineProfitArray(this.profitArr, this.generatorGroup(8, b.balls), b.cash * b.rate);
						break;
					case 1:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						}
					}
					break;
				case 63:
					this.multipleWinning(b.balls, b.cash * b.rate);
					break;
				case 64:
					this.combineProfitArray(this.profitArr, b.balls, b.cash * b.rate);
					break;
				case 65:
					switch (g) {
					case 2112:
					case 2145:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						};
					default:
						this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate)
					}
					break;
				case 66:
					switch (g) {
					case 2112:
					case 2145:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						};
					default:
						this.combineProfitArray(this.profitArr, this.generatorGroup(2, b.balls), b.cash * b.rate)
					}
					break;
				case 67:
					switch (g) {
					case 3113:
					case 3124:
					case 3135:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						};
					default:
						this.combineProfitArray(this.profitArr, this.expandArray(b.balls), b.cash * b.rate)
					}
					break;
				case 68:
					switch (g) {
					case 3113:
					case 3124:
					case 3135:
						return {
							minProfit: this.minProfit = !1,
							maxProfit: this.maxProfit = !1,
							totalCost: this.totalCost,
							error: "\u5355\u5f0f\u6ce8\u5355\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002"
						};
					default:
						this.combineProfitArray(this.profitArr, this.generatorGroup(3, b.balls), b.cash * b.rate)
					}
					break;
				case 71:
					switch (g) {
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
						this.combineProfitArray(this.profitArr,
							this.expandArray([b.balls[0], this.generatorGroup(g, b.balls[1])]), b.cash * b.rate);
						break;
					case 812:
					case 845:
						this.combineProfitArray(this.profitArr, this.expandArray([b.balls[0], this.generatorGroup(1, b.balls[1])]), b.cash * b.rate);
						break;
					case 913:
					case 924:
					case 935:
						this.combineProfitArray(this.profitArr, this.expandArray([b.balls[0], this.generatorGroup(2, b.balls[1])]), b.cash * b.rate)
					}
					break;
				default:
					return !1
				}
				return !0
			}, _maxLoopCount: 1E5,
			calcWinCash: function (e, c, d, b) {
					e *= d;
					b += e;
					c = c * d - b;
					d = c.toFixed(2) / b;
					d *= 100;
					return isNaN(d) ?
						(console.debug("下注金额: %s, 累计投入:%s, 盈利:%s, 盈利率: %s", e, b, c, d), console.error("算不出来"), !1) : {
							betCost: e,
							betTotalCost: b,
							betEarn: c,
							betEarnPercent: d
						}
				}, calcByMinEarnPercent: function (e, c, d, b, f) {
					var g, h, k, l;
					null == f && (f = 1);
					if (0 === c) return !1;
					parseFloat(void 0);
					f = g = h = f = Math.abs(parseInt(f)) || 1;
					for (k = this._maxLoopCount; h <= k ? g <= k : g >= k; f = h <= k ? ++g : --g) {
						l = this.calcWinCash(e, c, f, d);
						if (!1 === l) break;
						if (l.betEarnPercent >= b) return f
					}
					return !1
				},
				calcByMinEarn: function (e, c, d, b) {
					b = parseFloat(b);
					return e > c ? !1 : d + b <= c - e ? 1 : Math.ceil((d + b) / (c - e))
				}, generatorBetTrace: function (e, c, d, b, f, g, h, k, l) {
					var m, p, n, t, r, w, x;
					null == e && (e = 1);
					null == c && (c = 10);
					null == d && (d = 1);
					null == b && (b = 0);
					null == f && (f = 0);
					null == g && (g = 0);
					null == h && (h = 1);
					null == k && (k = 1);
					null == l && (l = 1);
					e = parseInt(e);
					b = parseFloat(b);
					g = parseFloat(g);
					c = parseInt(c);
					p = [];
					x = {
						error: "",
						betTraceData: p
					};
					if (!1 === f) return x.error = "您选择的玩法无法计算盈利，将以 -- 呈现。",
						x;
					f instanceof Array || (f = [f, f]);
					t = Math.max.apply(null, f);
					r = Math.min.apply(null, f);
					m = 1;
					n = f = 0;
					for (w = c; 0 <= w ? f < w : f > w; n = 0 <= w ? ++f : --f) {
						switch (e) {
						case 1:
							m = 1;
							break;
						case 2:
							m = this.calcByMinEarnPercent(b, r, g, h, m);
							break;
						case 3:
							m = n < k ? this.calcByMinEarnPercent(b, r, g, l, m) : this.calcByMinEarnPercent(b, r, g, h, m);
							break;
						case 4:
							m = this.calcByMinEarn(b, r, g, h);
							break;
						default:
							// alert("追号模式错误"), m = !1 // 2016-01-30 272792791729719279 注释
							m = 1; // 2016-01-30 272792791729719279 ADD
						}
						if (!1 === m) break;
						m = Math.max(m, d);
						n = this.calcWinCash(b, r, m, g);
						g = t === r ? n : this.calcWinCash(b, t, m, g);
						if (2 <=
							e && 4 >= e && (0 > n.betEarnPercent || 0 > g.betEarnPercent)) return p;
						n = {
							betRate: m || 0,
							betCost: b * m || 0,
							betTotalCost: n.betTotalCost || 0,
							betMinEarn: 1 * n.betEarn.toFixed(2) || 0,
							betMinEarnPercent: 1 * n.betEarnPercent.toFixed(2) || 0,
							betMaxEarn: 1 * g.betEarn.toFixed(2) || 0,
							betMaxEarnPercent: 1 * g.betEarnPercent.toFixed(2) || 0
						};
						n.betEarnStr = n.betMinEarn === n.betMaxEarn ? parseFloat(Math.floor(1E3 * n.betMinEarn) / 1E3) : parseFloat(Math.floor(1E3 * n.betMinEarn) / 1E3) + " \u81f3 " + parseFloat(Math.floor(1E3 * n.betMaxEarn) / 1E3);
						n.betEarnPercentStr =
							n.betMinEarnPercent === n.betMaxEarnPercent ? parseFloat(Math.floor(100 * n.betMinEarnPercent) / 100) + "%" : parseFloat(Math.floor(100 * n.betMinEarnPercent) / 100) + "% \u81f3 " + parseFloat(Math.floor(100 * n.betMaxEarnPercent) / 100) + "%";
						g = n.betTotalCost;
						m = n.betRate || 1;
						p.push(n)
					}
					0 === p.length ? x.error = "\u60a8\u9009\u62e9\u7684\u73a9\u6cd5\u65e0\u6cd5\u8ba1\u7b97\u76c8\u5229\uff0c\u5c06\u4ee5 -- \u5448\u73b0\u3002" : p.length !== c && (x.error = "\u60a8\u8f93\u5165\u7684\u8d44\u6599\u8fc7\u5927\uff0c\u6700\u591a\u53ea\u80fd\u751f\u6210 " +
						p.length + " \u671f\u3002");
					return x
				}
		}
};

$$.Function.getMsgCount = function(){
	$._ajax({url: stationUrl + "/common/getMsgCount.do", cache: false}, function (status, message, data, result) {
		if(data && data > 0){
			var msgCount = data;
			if(data > 9){
				msgCount = "9+";
			}
			$("#websiteSmsP_bm").html(msgCount).show();
		}
	});
}

$$.Function.checkLogin = function(){
	var mobile_alk_chk = $.cookie('mobile_alk_chk');
	if(mobile_alk_chk != null && mobile_alk_chk){
		$.ajax({
			type : "POST",
			url : stationUrl + "/alklogin.do",
			data : {
				_alk_chk: mobile_alk_chk
			},
			success : function(result) {
				if(result.code == 400){
					$.alert(result.message, function(e) {
						//location.href = stationUrl + "/login.html";
						location.href = stationUrl + "/login.do";
						return;
					});
				} else if(result.code == 200){
					location.reload();
				}
			}
		});
	} else {
		$.alert("请先登录！", function(e) {
			//location.href = stationUrl + "/login.html";
			location.href = stationUrl + "/login.do";
			return;
		});
	}
}

$$.Function.stopPropagation = function (e) {
	var e = e || event;
	if (e && e.stopPropagation) {
		e.stopPropagation();

	} else {
		e.cancelBubble = true;
	}
}

$(function(){
	"use strict";

	var pageInitF = function (){
		$(document).on("pageInit", "#indexPage", function(e, pageId, $page) {
			var IndexAction = $$.Action.Index.getInstance();
			IndexAction.init();
			$$.Function.getMsgCount();
		});
		$(document).on("pageInit", "#drawNoticePage", function(e, pageId, $page) {
			var DrawNotice = $$.Action.DrawNotice.getInstance();
			DrawNotice.init();
			$$.Function.getMsgCount();
		});
		$(document).on("pageInit", "#gameCenterPage", function(e, pageId, $page) {
			var GameCenter = $$.Action.GameCenter.getInstance();
			GameCenter.init();
			$$.Function.getMsgCount();
		});
		$(document).on("pageInit", "#personalCenterPage", function(e, pageId, $page) {
			var logined = !1;
			if($("#logined").length > 0){
				logined = !0;
			}
			if(logined == !1){
				$$.Function.checkLogin();
			}
			var PersonalCenter = $$.Action.PersonalCenter.getInstance();
			PersonalCenter.init();
			$$.Function.getMsgCount();
		});
		$(document).on("pageInit", "#doPickNumberPage", function(e, pageId, $page) {
			var logined = !1;
			if($("#logined").length > 0){
				logined = !0;
			}
			if(logined == !1){
				$$.Function.checkLogin();
			}
			$$.ActionInstance.DoPickNumber = $$.Action.DoPickNumber.getInstance();
			$$.ActionInstance.DoPickNumber.init();
		});
		$(document).on("pageInit", "#buyCartPage", function(e, pageId, $page) {
			$$.ActionInstance.BuyCart = $$.Action.BuyCart.getInstance();
			$$.ActionInstance.BuyCart.init();
		});
		$(document).on("pageInit", "#doIntelligentSelectionPage", function(e, pageId, $page) {
			$$.ActionInstance.DoIntelligentSelection = $$.Action.DoIntelligentSelection.getInstance();
			$$.ActionInstance.DoIntelligentSelection.init();
		});
	}

	var pageReinitF = function() {
		$(document).on("pageReinit", "#doPickNumberPage", function(e, pageId, $page) {
			if($$.ActionInstance.DoPickNumber){
				$$.ActionInstance.DoPickNumber.reinit();
			}
		});
		$(document).on("pageReinit", "#buyCartPage", function(e, pageId, $page) {
			if($$.ActionInstance.BuyCart){
				$$.ActionInstance.BuyCart.reinit();
			}
		});
		$(document).on("pageReinit", "#doIntelligentSelectionPage", function(e, pageId, $page) {
			if($$.ActionInstance.DoIntelligentSelection){
				$$.ActionInstance.DoIntelligentSelection.reinit();
			}
		});
	}

	pageInitF();
	pageReinitF();
	$.init();
});

// other
// 真人页面跳转函数
function go(url, type,obj,className) {
	var $it=$(obj);
	if($it.data("disabled")==1){
		return;
	}
	$it.data("disabled",1);
	if(className){
		$it.removeClass(className).css("color","#bbb");
	}
	var sw = '',
	csw = '',
	sh = '',
	csh = '',
	ctop = '',
	cleft = '';

	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;
	var tempUrl = url;
	/* if(type=='3'||type==3){ */
	var result = null;
	$.ajax({
		url : url,
		async : false,
		success : function(json) {
			result = json;
		}
	});
	if(!result.success){
		// $it.removeData("disabled");
        $it.data("disabled",0);
		if(className){
			$it.addClass(className).css("color","");
		}
		alert(result.msg);
		return;
	}
	if(identification == "WebBrowser"){
//		if (type === '2') {
//			var windowOpen = window.open("" ,'_blank', 'width=' + csw
//					+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
//					+ ',scrollbars=no,location=1,resizable=yes');
//			var new_doc = windowOpen.document.open("text/html","replace");
//			new_doc.write(result.html);
//			new_doc.close();
//		} else {
			var windowOpen = window.open(result.url ,'_blank', 'width=' + csw
					+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
					+ ',scrollbars=no,location=1,resizable=yes');
//		}
	} else {
//		if (type === '2') {
			/*
			var nwaiting = plus.nativeUI.showWaiting();//显示原生等待框
			var webviewContent = plus.webview.create(url, "realGame", {top:"42px",bottom:"0px"});
			webviewContent.addEventListener("loaded", function() { //注册新webview的载入完成事件
				nwaiting.close(); //新webview的载入完毕后关闭等待框
				webviewContent.show("slide-in-right", 200); //把新webview窗体显示出来，显示动画效果为速度200毫秒的右侧移入动画
			}, false);
			*/
//			var windowOpen = window.open("" ,'_blank', 'width=' + csw
//					+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
//					+ ',scrollbars=no,location=1,resizable=yes');
//			var new_doc = windowOpen.document.open("text/html","replace");
//			new_doc.write(result.html);
//			new_doc.close();
//		} else {
			var nwaiting = plus.nativeUI.showWaiting();//显示原生等待框
			var webviewContent = plus.webview.create(result.url, "realGame", {top:"42px",bottom:"0px"});
			webviewContent.addEventListener("loaded", function() { //注册新webview的载入完成事件
				nwaiting.close(); //新webview的载入完毕后关闭等待框
				webviewContent.show("slide-in-right", 200); //把新webview窗体显示出来，显示动画效果为速度200毫秒的右侧移入动画
			}, false);
//		}
	}
	// $it.removeData("disabled");
    $it.data("disabled",0);
	if(className){
		$it.addClass(className).css("color","");
	}
}
