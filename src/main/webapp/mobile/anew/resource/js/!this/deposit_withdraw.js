
"use strict";

function ViewModel (){
	return {
		Info : {}, Function : {}, Component : {}, Action : {}
	};
}

function ActionModel (){
	var model = new ViewModel();
	return $.extend({
		
	}, model);
}
window.$$ = window.Application = $.extend({ActionInstance: {}}, new ViewModel());

$$.Action.PaySelectWay1 = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){

				function xinghao(inn){
					var out = "";
					for(var i = 0; i < inn.length - 1; i++){
						out += "*";
					}
					return out + inn.substr(-1, 1);
				}

				$._ajax(stationUrl + "/deposit/config.do", function (status, message, data, result) {
					var fasts = [];
					var banks = [];
					var onlines = [];
					if(data.fastFlag == 'on'){
						$._ajax({url: stationUrl + "/deposit/fastpay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#fastpaysTemplate").html();
							var $fastpayContent = $("#fastpayContent");
							fasts = data.fasts;
							$fastpayContent.html(_.template(tplHtml)({list: fasts}));
						});
						
						
					}
					if(data.bankFlag == 'on'){
						$._ajax({url: stationUrl + "/deposit/bankpay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#bankpaysTemplate").html();
							var $bankpayContent = $("#bankpayContent");
							banks = data.banks;
							$bankpayContent.html(_.template(tplHtml)({list: banks}));
						});
					}
					if(data.onlineFlag == 'on'){
						$._ajax({url: stationUrl + "/deposit/onlinepay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#onlinepaysTemplate").html();
							var $onlinepayContent = $("#onlinepayContent");
							onlines = data.onlines;
							$onlinepayContent.html(_.template(tplHtml)({list: onlines}));
						});
					}
					
					if(!fasts.length && !banks.length){
						$(".offlinepay").hide();
					}
					var config = {};
					$(".pwradio").on("change", function(){
						var $this = $(this);
						var payaction = $this.attr("payaction");
						var index = $this.attr("index");
						var czhtml = '';
						var money = $("#money").val();
						var payId = $("input.pwradio:checked").val();
						var iconCss = $("input.pwradio:checked").attr("iconCss");
						var paytype = $("input.pwradio:checked").attr("paytype");
						var min = parseFloat($("input.pwradio:checked").attr("min")||-1,10);
						var max = parseFloat($("input.pwradio:checked").attr("max")||-1,10);
						if(!payId){
							$.alert("请选择充值方式");
							return;
						}
						if(showPayInfo){
							$("#next").html("确定充值");
						}else{
							$("#next").html("下一步");
						}
						if(payaction == "fastpay"){
							config = {tplHtmlSelector: "#fastpayInputInfoTemplate", data: fasts, dataIndex: index, payaction: "fastpay", desc: data.fastDesc};
							if(showPayInfo){
								var tplHtml = $("#fastpaySuccessTemplate").html();
								czhtml += _.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money,payDesc:'<li style="font-size:20px;color:red;">充值成功后，请填写充值金额，并点击确定充值，否则无法为您上分！</li>'}));
							}
							fastPayfun(czhtml,config);
						} else if(payaction == "bankpay"){
							config = {tplHtmlSelector: "#bankpayInputInfoTemplate", data: banks, dataIndex: index, payaction: "bankpay", desc: data.bankDesc};
							if(showPayInfo){
								var tplHtml = $("#bankpaySuccessTemplate").html();
								czhtml += _.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money,payDesc:"<li style='color:red;font-size:20px;'>（转账后请输入金额提交订单）</li>"}));
							}
							fastPayfun(czhtml,config);
						} else if(payaction == "onlinepay"){
							$("#next").html("下一步");
							config = {tplHtmlSelector: "#onlinepayInputInfoTemplate", data: onlines, dataIndex: index, payaction: "onlinepay", desc: data.onlineDesc};
							$("#showDescDiv").html(config.desc);
							var $inputInfoContent = $("#inputInfoContent");
							var tplHtml = $(config.tplHtmlSelector).html();
							$inputInfoContent.html(_.template(tplHtml)(config.data[config.dataIndex]));
						}
					});
					
					$("#next").on("click", function(){
						var money = $("#money").val();
						var payId = $("input.pwradio:checked").val();
						var iconCss = $("input.pwradio:checked").attr("iconCss");
						var paytype = $("input.pwradio:checked").attr("paytype");
						var min = parseFloat($("input.pwradio:checked").attr("min")||-1,10);
						var max = parseFloat($("input.pwradio:checked").attr("max")||-1,10);
						if(!payId){
							$.alert("请选择充值方式");
							return;
						}
						if(money=='' || isNaN(money)){
							$.alert("充值金额格式错误");
							return;
						}
						money=parseFloat(money,10);
						if(min!=-1 && money < min){
							$.alert("充值金额不能小于" + min + "元");
							return;
						}
						if(max!=-1 && money > max){
							$.alert("充值金额不能大于" + max + "元");
							return;
						}

						if(config.payaction == "fastpay"){
							var $bankC=$("#bankCards");
							if($bankC.length){
								var bankCards = $bankC.val();
								if(!bankCards || bankCards==''){
									$.alert("付款账号不能为空");
									return;
								}
							}
							$.ajax({
								type : "POST",
								url : stationUrl + "/deposit/fastpaySubmit.do",
								contentType : "application/x-www-form-urlencoded",
								data : {
									money: $("#money").val(),
									bankCards: $("#bankCards").val(),
									payId: payId
								},
								success : function(result) {
									if(result.success == false){
										$.alert(result.msg);
									} else if(result.success == true){
										$.alert("充值申请已提交，请尽快完成充值！", function(){
											var tplHtml = $("#fastpaySuccessTemplate").html();
											var $kakakakaContent = $("#kakakakaContent");
											$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money,payDesc:''})));
											$.popup('.popup-topayaction');
										});
									}
								}
							});
						} else if(config.payaction == "bankpay"){
							var $dep=$("#depositor");
							if($dep.length){
								var depositor =$dep.val();
								if(!depositor || depositor==''){
									$.alert("存款人姓名不能为空");
									return;
								}
							}
							$.ajax({
								type : "POST",
								url : stationUrl + "/deposit/bankpaySubmit.do",
								data : {
									money: money,
									depositor: $("#depositor").val(),
									bankId: payId
								},
								success : function(result) {
									if(result.success == false){
										$.alert(result.msg);
									} else if(result.success == true){
										$.alert("充值申请已提交，线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！", function(){
											var tplHtml = $("#bankpaySuccessTemplate").html();
											var $kakakakaContent = $("#kakakakaContent");
											$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money,payDesc:''})));
											$.popup('.popup-topayaction');
										});
									}
								}
							});
						} else if(config.payaction == "onlinepay"){
							var account = !1;
							switch (identification) {
							case "Android":
							case "IOS":
								account = loginaccount;
								break;
							default:
							};

							topay(money, payId, iconCss, paytype, function(result){
								if(result.success == false){
									$.alert(result.msg);
								} else if(result.success == true){
									if(result && result.data && result.data.formParams){
										var tplHtml = $("#toPayTemplate").html();
										var $kakakakaContent = $("#kakakakaContent");
										result.data.identification = identification;
										$kakakakaContent.html(_.template(tplHtml)(result.data));
										$.popup('.popup-topayaction');
									}else{
										$.alert("系统发生错误");
									}
								}
							}, account, !1, identification);
						}
					});
				});
			},
			reinit: function(){
				
			},
			destroy: function(){
				
			}
		};
	}
};

//快速充值
function fastPayfun(p,config){
	$("#showDescDiv").html(config.desc);
	var $inputInfoContent = $("#inputInfoContent");
	var tplHtml = $(config.tplHtmlSelector).html();
	p += _.template(tplHtml)(config.data[config.dataIndex]);
	$('#inputInfoContent').html(p);
}

$$.Action.PaySelectWay2 = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
			},
			reinit: function(){
				
			},
			destroy: function(){
				
			}
		};
	}
};

$$.Action.Withdraw = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){
			},
			reinit: function(){
				
			},
			destroy: function(){
				
			}
		};
	}
};

$(function(){
	"use strict";

	var pageInitF = function (){
		$(document).on("pageInit", "#paySelectWay1Page", function(e, pageId, $page) {
			$$.ActionInstance.PaySelectWay1 = $$.Action.PaySelectWay1.getInstance();
			$$.ActionInstance.PaySelectWay1.init();
		});
		$(document).on("pageInit", "#paySelectWay2Page", function(e, pageId, $page) {
			$$.ActionInstance.PaySelectWay2 = $$.Action.PaySelectWay2.getInstance();
			$$.ActionInstance.PaySelectWay2.init();
		});
		$(document).on("pageInit", "#withdrawPage", function(e, pageId, $page) {
			$$.ActionInstance.Withdraw = $$.Action.Withdraw.getInstance();
			$$.ActionInstance.Withdraw.init();
		});
	}

	var pageReinitF = function() {
		$(document).on("pageReinit", "#paySelectWay1Page", function(e, pageId, $page) {
			if($$.ActionInstance.PaySelectWay1){
				$$.ActionInstance.PaySelectWay1.reinit();
			}
		});
		$(document).on("pageReinit", "#paySelectWay2Page", function(e, pageId, $page) {
			if($$.ActionInstance.PaySelectWay2){
				$$.ActionInstance.PaySelectWay2.reinit();
			}
		});
		$(document).on("pageReinit", "#withdrawPage", function(e, pageId, $page) {
			if($$.ActionInstance.Withdraw){
				$$.ActionInstance.Withdraw.reinit();
			}
		});
	}

	pageInitF();
	pageReinitF();
	$.init();
});
