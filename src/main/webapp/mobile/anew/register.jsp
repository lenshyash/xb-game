﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width">
    <link rel="stylesheet" href="${base }/mobile/anew/resource/new/css/global.css?ver=4.4" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        body.login-bg{padding-top:0px!important;}
    </style>
        <title>${_title }--注册</title>
    </head>
<body class="login-bg">
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="window.history.go(-1);">reveal</button>
        </div>
        <h1 class="ui-toolbar-title">快速注册</h1>
    </div>
</div>
    <div class="login">
        <form id="register_form">
        <ul>
            <li>
                <span class="logi">用户账号</span><input type="text" id="accountno" name="uid" placeholder="用户名为5-11个英文字母或数字">
            </li>
            <li>
                <span class="logi">登录密码</span><input type="password" id="password1" name="pwd" placeholder="密码为6-16个英文字母或数字">
            </li>
            <li id="reg_tb_after">
                <span class="logi">重复密码</span><input type="password" id="password2" placeholder="请再次输入密码">
            </li>
            <li>
                <span class="logi">验&nbsp;&nbsp;证&nbsp;&nbsp;码</span>
                <input type="text" id="code" name="captcha" placeholder="请输入验证码" style="width:50%;position:position;">
                <img id="regCode" width="70" height="35" title="重新获取验证码" style="cursor:pointer;position:absolute;right:10px;margin-top:10px;height:30px;" src="${base}/regVerifycode.do" onclick="reloadImg();">
            </li>
        </ul>
        </form>
        <button class="login-btn login-btn-no" id="register_btn">立即注册</button>
        <c:if test="${not empty testAccount && testAccount eq 'on'}">
        	<a href="${base }/mobile/swregpage.do" class="reg-btn" id="login_demo" style="margin-top: 12px; font-size:16px;">免费试玩</a>
        </c:if>
        <button class="reg-btn" onclick="location.href='${base}/mobile/login.do'">返回登录</button>
        <button class="reg-btn" onclick="location.href='${base}/mobile/index.do'">返回首页</button>
        <br>
        <div class="login-p">
        <c:if test="${kfSwitch eq 'on'}">
         	<c:if test="${empty mobileOpenOutlinkType || mobileOpenOutlinkType eq 'blank' }">
           		<a class="fl" href="${kfUrl }" target="_blank">在线客服</a>
           	</c:if>
          	<c:if test="${mobileOpenOutlinkType eq 'href' }">
           		<a class="fl" href="${kfUrl }">在线客服</a>
           	</c:if>
          	<c:if test="${mobileOpenOutlinkType eq 'inner' }">
           		<a class="fl" href="${base }/mobile/kefuDesc.do">在线客服</a>
           	</c:if>
        </c:if>
        <a class="fr" href="${base }/mobile/login.do">已有帐号，直接登录</a></div>
        <div class="login-p" style="color: #fe0103;">1.用户帐号请输入5-11个英文字母或数字，不能用中文。</div>
        <div class="login-p" style="color: #fe0103;">2.登录密码请输入6-16个英文字母或数字。</div>
    </div>

    
<style>
    .center {text-align: center}
</style>

<div class="beet-odds-tips round" id="tip_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="tip_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que" style="width: 100%;" onclick="tipOk()"><span>确定</span></button>
    </div>
</div>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>
<script src="${base }/mobile/anew/resource/new/js/jquery-2.1.4.js?ver=1.0"></script>
<script type="text/javascript" src="${base}/regconf.do"></script>
<script type="text/javascript" src="${base }/mobile/anew/resource/new/js/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<c:if test="${isWechatPop eq 'on'}"><jsp:include page="${base }/mobile/wechat/wechat.jsp"></jsp:include></c:if>
<script>
    var func;
    function tipOk() {
        $('#tip_pop').hide();
        $('#tip_bg').hide();
    }
    function msgAlert (msg,funcParm) {
        $('div#tip_pop_content').html(msg);
        $('div#tip_pop').show();
        $('div#tip_bg').show();
        func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
    function reloadImg(){
    	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
    	$("#regCode").attr("src", url);
    }
    var screenHeight = window.screen.height
    $("body").height(screenHeight)
</script>

<script id="regconflst_tpl" type="text/html">
{{each data as tl}}<li><span class="logi">{{tl.name}}</span>
{{if tl.type==1}}<input type="text" placeholder="请输入{{tl.name}}" name="{{tl.key}}" id="{{tl.key}}">{{/if}}
{{if tl.type==6}}<input type="password" name="{{tl.key}}" id="{{tl.key}}" placeholder="请输入{{tl.name}}">{{/if}}
{{$addValidateFiled tl}}
</li>{{/each}}
</script> 

<script type="text/javascript">
var linkCode= 0;
$(function() {
	
	initRegConf();
	var arrcookie = document.cookie.split("; ");
	$.each(arrcookie,function(index,item){
		if(item.indexOf('linkCode') >= 0){
			linkCode = item.substring(9)
			$("#promoCode").attr("disabled","disabled");
			$("#promoCode").val(linkCode);
			$("#promoCode").css({'margin-left':'25px','width':'59px','height':'28px'});
			$("#wrongok").show()
		}
	})
	
});

var fileds = [];
var validateFiled = [];

function initRegConf() {
	for (var i = 0; i < $_regconf.length; i++) {
		var conf = $_regconf[i];
		if (conf.source && (conf.type == 2 || conf.type == 3 || conf.type == 4)) {
			conf.sourceLst = conf.source.split(",");
		}
	}
	var eachdata = {
		"data" : $_regconf
	};
	var html = template('regconflst_tpl', eachdata);
	$("#verifycode_div").before(html);
	$("#reg_tb_after").after(html);
}

template.helper('$addValidateFiled', function(conf) {
	if (conf.requiredVal == 2 || conf.validateVal == 2) {
		validateFiled.push(conf);
	}
});

function reset() {
	$('form')[0].reset();
}

function validate() {
	for (var i = 0; i < validateFiled.length; i++) {
		var filed = validateFiled[i];
		var val = getVal(filed);
		if(filed.regex){
			filed.regex = filed.regex.replace(/\\+/g,'\\');
			var regex = new RegExp(filed.regex);
		}

		if (filed.requiredVal == 2) {
			if (!val) {
				RC.alert(filed.name + "必须输入！",filed.key);
				return false;
			}
		}

		if (filed.validateVal == 2) {
			if (!regex.test(val)) {
				RC.alert(filed.name + "格式错误！",filed.key);
				return false;
			}
		}
	}
	return true;
}

function getVal(filed) {
	var val = "";
	if (filed.type == 1 || filed.type == 2 || filed.type == 5
			|| filed.type == 6) {
		val = $("#" + filed.key).val();
	} else if (filed.type == 3) {
		val = $("input[name='" + filed.key + "']:checked").val();
	} else if (filed.type == 4) {
		var vals = [];
		$("input[name='" + filed.key + "']:checked").each(function() {
			vals.push(this.value);
		})
		val = vals.join(",");
	}
	return val;
}

function getCommitData() {
	var data = {};
	for (var i = 0; i < $_regconf.length; i++) {
		var filed = $_regconf[i];
		var val = getVal(filed);
		data[filed.key] = val;
	}
	return data;
}
</script>
<script>
    $('#register_btn').click(function(event) {
    	var account = $('form#register_form input#accountno').val().trim();
        var password1 = $('form#register_form input#password1').val().trim();
        var password2 = $('form#register_form input#password2').val().trim();
        var code = $('form#register_form input#code').val().trim();//验证码
        if(!account){
        	msgAlert("用户名不能为空!");
       	 return false;
        }
        if(!password1){
        	msgAlert("密码不能为空!");
       	 return false;
        }
        if(password1.length<6){
        	msgAlert("密码不能小于6位!");
       	 return false;
        }
        if(!password2){
        	msgAlert("确认密码不能为空!");
       	 return false;
        }
        if(password1 != password2){
         	msgAlert("两次密码不一致!");
       	 return false;
        }
        if(!code){
        	msgAlert("验证码不能为空!");
       	 return false;
        }
        
        var data = getCommitData();
        
        //可能网络原因或者手机型号原因,刷新一下
        if(!account || !password1 || !password2 || !code){
        	location.href = location.href;
        }
        
        data["account"] = account;
        data["password"] = password1;
    	data["rpassword"] = password2;
        data["verifyCode"] = code;
        
        var targetUrl = '${base}/mobile/index.do';//跳转的页面
    	var isGuest = $('#isGuest').val();//是否是试玩注册
    	var stationB = $('#stationB').val();
    	var url = '${base}/register.do';
    	if(isGuest){
    		url = '${base}/registerTestGuest.do';
    	}
      	$.ajax({
     		url:url,
     		data : "data="+JSON.encode(data),
     		type:"POST",
     		contentType : "application/x-www-form-urlencoded",
    		success : function(result, textStatus, xhr) {
    			var ceipstate = xhr.getResponseHeader("ceipstate")
    			if (!ceipstate || ceipstate == 1) {// 正常响应
    				if(!result.success){
    					msgAlert(result.msg);
    				}else{
    					msgAlert("注册成功!");
    					location.href = targetUrl;
    				}
    			} else if (ceipstate == 2) {// 后台异常
    				$('form#register_form input#code').val('');
    				msgAlert("后台异常，请联系管理员!");
    				reloadImg();
    			} else if (ceipstate == 3) { // 业务异常
    				$('form#register_form input#code').val('');
    				msgAlert(result.msg);
    				reloadImg();
    			}
    		}
     	});
    });
</script>
<div id="weOther" style="position: absolute;top:0;left: 0;right: 0;bottom: 0;background: rgba(0,0,0,0.8);z-index: 99999;display: none">
    <img src="${base }/mobile/v3/images/wxOther1.png" alt="" style="width: 100%;height: 100%;">
</div>
<script>
    var browser = {
        versions: function() {
            var a = navigator.userAgent,
                b = navigator.appVersion;
            return {
                trident: a.indexOf("Trident") > -1,
                presto: a.indexOf("Presto") > -1,
                webKit: a.indexOf("AppleWebKit") > -1,
                gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
                mobile: !!a.match(/AppleWebKit.*Mobile.*/),
                ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
                android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
                iPhone: a.indexOf("iPhone") > -1,
                iPad: a.indexOf("iPad") > -1,
                webApp: a.indexOf("Safari") == -1
            }
        }(),
        language: (navigator.browserLanguage || navigator.language).toLowerCase()
    };
    $(function () {
        if (browser.versions.android) {
            //Android
            $("#weOther").find('img').attr('src','${base }/mobile/v3/images/wxOther2.png')
        } else if (browser.versions.ios) {
            //ios
            $("#weOther").find('img').attr('src','${base }/mobile/v3/images/wxOther1.png')
        }
    })
    var wx= (function(){
            return navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1
        }
    )();
    if(wx){
        // alert("是微信");
        $("#weOther").show()
    }else {
        // alert("不是微信");
    }
</script>
</body>
</html>