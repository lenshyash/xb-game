<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back"><a href="personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> <span class="vbktl">积分兑换</span></div>
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<div class="" id="innerbox">
		<div class="shangqkaij"><span class="fl co777">当前账户：${account}</span><div class="cl"></div></div>
		<div>
			<table border="0" cellpadding="3" cellspacing="3" width="100%">
				<colgroup><col width="25%"><col width="75%"></colgroup>
				<tbody>
					<tr style="height: 40px;line-height: 40px;">
						<td class="tar">账户余额：</td>
						<td id="cur_money_id"></td>
					</tr>
					<tr style="height: 40px;line-height: 40px;">
						<td class="tar">积分：</td>
						<td id="cur_score_id">${userInfo111.score}</td>
					</tr>
					<tr style="height: 40px;line-height: 40px;">
						<td class="tar">兑换类型： </td>
						<td><select id="exchangeType" style="width:150px;border: 1px solid #ddd;">
							<option selected="selected" value="">----请选择类型----</option>
							<option value="1">现金兑换积分</option>
							<option value="2">积分兑换现金</option>
						</select><div id="exchange_remark_span"></div></td>
					</tr>
					<tr style="height: 40px;line-height: 40px;">
						<td class="tar">兑换额度：</td>
						<td><input class="text-input" id="amount" name="amount" style="border: 1px solid #ddd;" type="text"></td>
					</tr>
					<tr style="height:40px;line-height:40px;"><td></td>
						<td><a id="scoreExchangeBtn" href="javascript: void(0);" style="height:40px;line-height:40px;text-align:center;" class="xiayibubtns">立即兑换</a></td>
					</tr>
				</tbody>
			</table>
		<div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script>
$(function(){
	var configs=${configs},selectedType;
	$("#exchangeType").change(function() {
		var $it=$(this),selval = $it.children('option:selected').val();
		var selHtml = $it.children('option:selected').html().split("兑换");
		if (selval > 0) {
			selectedType = getExcTypeByType(selval);
			if (selectedType) {
				$("#exchange_remark_span").html(
						selectedType.numerator + selHtml[0]
						+ "可兑换"
						+ selectedType.denominator
						+ selHtml[1] + "(兑换比例："
						+ selectedType.numerator + ":"
						+ selectedType.denominator
						+ ")");
			} else {
				$("#exchange_remark_span").html("暂时未开放！");
			}
		} else {
			$("#exchange_remark_span").html("");
		}
	});
	function getExcTypeByType(val) {
		if (configs) {
			for (var i = 0; i < configs.length; i++) {
				if (val == configs[i].type) {
					return configs[i];
				}
			}
		}
		return null;
	}
	//制保留2位小数，如：2，会在2后面补上00.即2.00 
	 function toDecimal2(x) {
	 	if (isNaN(x)) {
	 		return false;
	 	}
	 	var s = x.toString();
	 	var rs = s.indexOf('.');
	 	if (rs < 0) {
	 		rs = s.length;
	 		s += '.';
	 	}else{
	 		s=s.substring(0,rs)+"."+s.substring(rs+1,rs+3);
	 	}
	 	while (s.length <= rs + 2) {
	 		s += '0';
	 	}
	 	return s;
	}
	$("#cur_money_id").html(toDecimal2("${userInfo111.money}"));
	$("#scoreExchangeBtn").click(function(){
		$("#scoreExchangeBtn").attr("disabled", "disabled");
		if (!$("#exchangeType").val()) {
			layer.msg("请选择兑换类型！", {
				icon : 2
			});
			$("#scoreExchangeBtn").removeAttr("disabled");
			return;
		}
		var amount = $("#amount").val();
		if (!amount) {
			layer.msg("额度不能为空！", {
				icon : 2
			});
			$("#scoreExchangeBtn").removeAttr("disabled");
			return;
		}
		if (amount > selectedType.maxVal) {
			layer.msg("额度必须小等于" + selectedType.maxVal, {
				icon : 2
			});
			$("#selectedType").removeAttr("disabled");
			return;
		}
		if (amount < selectedType.minVal) {
			layer.msg("额度必须大等于" + selectedType.minVal, {
				icon : 2
			});
			$("#selectedType").removeAttr("disabled");
			return;
		}
		$.ajax({
			url : "${base}/center/banktrans/exchange/exchange.do",
			data : {
				typeId : selectedType.id,
				amount : amount
			},
			success : function(result) {
				if (!result.success) {
					layer.msg(result.msg, {
						icon : 2
					});$("#selectedType").removeAttr("disabled");
					return;
				} else {
					layer.alert("兑换成功", {
						icon : 1,
						offset : [ '30%' ]
					}, function(index) {
						$.ajax({
							url : "${base}/center/banktrans/exchange/udata.do",
							success : function(result) {
								$("#cur_money_id").html(toDecimal2(result.money));
								$("#cur_score_id").html(result.score);
							}
						});
						$("#selectedType").removeAttr("disabled");
						$("#amount").val("");
						layer.close(index);
					})
				}
			}
		});
	});
});
</script>
</body>
</html>