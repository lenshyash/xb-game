﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp"%>
<%
	Long stationId = StationUtil.getStation().getId();
	pageContext.setAttribute("isMBComOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_mobile_recharge));
	pageContext.setAttribute("mobileTopmenuActivitiesAction", StationConfigUtil.get(stationId, StationConfig.mobile_topmenu_activities_action));

	pageContext.setAttribute("iosExamine", StationConfigUtil.get(stationId, StationConfig.onoff_mobile_ios_examine));
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="${base }/images/favicon.ico" onerror="javascript:this.href='${base }/images/logo.ico';">
<link rel="shortcut icon" href="${base }/images/logo.ico" onerror="javascript:this.href='${base }/images/favicon.ico';">
<link rel="apple-touch-icon-precomposed" href="${base }/images/favicon.ico">  
<link rel="apple-touch-icon-precomposed" href="${base }/images/logo.ico">  
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="include/css.jsp"></jsp:include>
<script type='text/javascript' src='${base}/mobile/anew/resource/js/jquery/3.1.1/jquery.min.js'></script>
<script type="text/javascript">
var test = window.location.href;
var arr = test.split("#");
if(arr && arr.length > 1 && '/user/bank' == arr[1]){
	window.location.href='${base}/mobile/pay_select_way.do';
}
</script>
</head>
<body>
	<div class="page page-current" id="indexPage">
		<span id="colorOn" style="display:none;">1</span>
		<header class="bar bar-nav">
			<div class="logo">
			<c:if test="${not empty mobileLogoUrl}">
							<img style="position:absolute;height:2.2rem;" src="${mobileLogoUrl }" />
					</c:if>
					<c:if test="${empty mobileLogoUrl}">
							<a href="javascript: location.reload();" class="external">${_title }</a>
					</c:if>
			</div>
			<div id="loginStatusBar" class="login_status_bar">
				<p:login login="false">
					 <em class="iconfont icon-user8"></em><a href="${station }/login.do" class="external">登录</a>
					 <c:if test="${isReg eq 'on'}">
					 	<a href="${station }/regpage.do" class="external app_no_reg"> | 注册</a>
					 </c:if>
				</p:login>
				<p:login login="true">
					<input id="logined" type="hidden" />
					<a href="${station }/personal_center.do">
						<div class="user_logined_img">
							<img src="${station }/anew/resource/images/touxiang.png">
						</div> <span> <em class="iconfont icon-jindous2x"></em><span id="login_mem_cash">${loginMember.money }</span>
					</span>
					</a>
				</p:login>
			</div>
			<!-- 判断如果域名是 bet365lk那么久显示副标题 不是就不显示 -->
				<div id="reserveTitle" style="display:none;position:absolute;top:1.7rem;color:#fff;font-size:14px;">
					------2018世界杯官方指定网投
				</div>

		</header>
		<nav class="bar bar-tab higher">
			<a class="tab-item active external" href="${station }/index.do">
				<span class="icon icon-home"></span>
				<span class="tab-label">投注大厅</span>
			</a>
			<c:if test="${isCpOnOff=='on'}">
			<a class="tab-item" href="${station }/game_center.do">
				<span class="icon icon-app"></span>
				<span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
			</a>
			<a class="tab-item" href="${station }/draw_notice.do">
				<span class="icon icon-gift"></span>
				<span class="tab-label">开奖公告</span>
			</a>
			</c:if>
			<a class="tab-item" href="${station }/personal_center.do">
				<span class="icon icon-me"><span id="websiteSmsP_bm" class="badge" style="display: none;">9+</span></span>
				<span class="tab-label">个人中心</span>
			</a>
		</nav>
		<div class="content reserveTitle">
						<script>
						if(window.location.host.indexOf("bet365lk.com") != -1 ){
							$("#reserveTitle").show();
							$(".bar-nav").height('3rem');
							$(".reserveTitle").css('top','3rem');
						}
				</script>
			<c:if test="${!empty agentLunBos }">
				<div class="swiper-container lunbo" data-space-between='10'
					data-pagination='.swiper-pagination' data-autoplay="7000">
					<div class="swiper-wrapper">
						<c:forEach items="${agentLunBos }" var="item" varStatus="status">
							<div class="swiper-slide">
							<c:choose>
								<c:when test="${item.titleUrl != '' && isActive}">
								<a href="${station}/active.do" class="external" ><img src="${item.titleImg}"></a>
								</c:when>
								<c:otherwise>
								<a href="javascript:void(0);" class="external"><img src="${item.titleImg}"></a>
								</c:otherwise>
							</c:choose>
							</div>
						</c:forEach>
					</div>
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div>
			</c:if>
			<div class="row no-gutter notice">
				<span class="tips_title">公告</span>
				<marquee id="noticeContent" scrollamount="3" class="tips_content font_color_gray"></marquee>
			</div>
			<!-- no-gutter: 无间距 -->
			<c:if test="${iosExamine ne 'off' }">
				<div class="row no-gutter quickMenu">
					<div class="col-25">
						<a class="qMenuItem color_huang"
							href="${station }/personal_center.do"><icon
								class="iconfont ft34">&#xe675;</icon>存／取款</a>
					</div>
					<div class="col-25">
						<a class="qMenuItem color_hong"
							href="${station }/personal_center.do"><icon
								class="iconfont pb2">&#xe64c;</icon>投注记录</a>
					</div> 
					<%-- <div class="col-25">
						<a external class="qMenuItem color_hong" href="${station }/live_game.do" style="padding-top: 33px;">
							<img src="${base }/mobile/anew/resource/images/livedemo.png" style="position: absolute;margin-left: 17px;margin-top: -28px;"/>
						额度转换</a>
					</div>--%>
					<div class="col-25">
						<c:if test="${isActive }">
							<a class="qMenuItem color_lv external" href="${station }/active.do"><icon
									class="iconfont pb2">&#xe630;</icon>优惠活动</a>
						</c:if>
						<c:if test="${!isActive }">
							<a class="qMenuItem color_lv external" href="${station }/appDownload.do"><icon
									class="iconfont pb2">&#xe630;</icon>A P P下载</a>
						</c:if>
							
					</div>
					<div class="col-25">
						<c:choose>
							<c:when test="${not empty mobileOpenOutlinkType && mobileOpenOutlinkType eq 'blank'}">
								<a class="qMenuItem color_lan external" href="${kfUrl }" target="_blank"><icon class="iconfont pb2">&#xe654;</icon>在线客服</a>
							</c:when>
							<c:otherwise>
								<a class="qMenuItem color_lan external" id="customerServiceUrlLink" href="javascript: void(0);"><icon class="iconfont pb2">&#xe654;</icon>在线客服</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:if>
		
		
		<div class="tabBox" style="margin:auto;">
			<div class="hd">
				<h3><a href="javascript:void(0);" style="color: #967f7f;">彩种大厅</a><span>LOTTERY</span></h3>
			</div>
		</div>
		<div class="row no-gutter index_margin">	
		<!-- 彩种游戏 -->
		<c:if test="${v eq 'v1'}">
			<jsp:include page="include/games_cp.jsp"></jsp:include>
			<jsp:include page="include/games_ty.jsp"></jsp:include>
			<jsp:include page="include/games_zr.jsp"></jsp:include>
		</c:if>
		<c:if test="${v eq 'v2'}">
			<jsp:include page="include/games_zr.jsp"></jsp:include>
			<jsp:include page="include/games_ty.jsp"></jsp:include>
			<jsp:include page="include/games_cp.jsp"></jsp:include>
			
		</c:if>
		<c:if test="${v eq 'v3'}">
			<jsp:include page="include/games_ty.jsp"></jsp:include>
			<jsp:include page="include/games_cp.jsp"></jsp:include>
			<jsp:include page="include/games_zr.jsp"></jsp:include>
			
		</c:if>
		<c:if test="${v eq 'v4'}">
			<jsp:include page="include/games_tab_category.jsp"></jsp:include>
		</c:if>
		</div>
	
			<script type="text/javascript">
				function toLogin(){
					location.href = '${base}/mobile/login.do'
				}
			</script>
			
			<c:if test="${not empty onOffSignIn && 'on'  eq onOffSignIn}">
				<div class="m_hot" style="margin-top:10px;">
					<c:if test="${!isLogin }"><a href="javascript:toLogin();" external></c:if><c:if test="${isLogin }"><a href="${base }/mobile/signIn.do" external></c:if>
						<img src="${base }/mobile/anew/resource/images/qd.jpg" width="100%">
					</a>
				</div>
			</c:if>
			<c:if test="${not empty onOffTurnlate && 'on'  eq onOffTurnlate}">
				<div class="m_hot" style="margin-top:10px;">
					<c:if test="${!isLogin }"><a href="javascript:toLogin();" external></c:if><c:if test="${isLogin }"><a href="${base }/mobile/turnlate.do" external></c:if>
						<img src="${base }/mobile/anew/resource/images/zp.jpg" width="100%">
					</a>
				</div>
			</c:if>
			
			<!-- 底部工具栏 -->
			<c:if test="${not empty onOffToolUtil && onOffToolUtil eq 'on'}">
				<jsp:include page="include/footer_util.jsp"/>
			</c:if>
		</div>
	</div>
	<jsp:include page="include/js.jsp"></jsp:include>
	<script type="text/html" id="loginStatusBarTemplate" style="display: none;">
	{#
		if(logined == !0){
	#}
						<a href="${station }/personal_center.do">
							<div class="user_logined_img">
								<img src="${station }/anew/resource/images/touxiang.png"></div>
							</div>
									<span>
									<em class="iconfont icon-jindous2x"></em><span >{{money}}</span>
									</span>
						</a>
	{#
		} else {
	#}
						<a href="${station }/login.do" class="external">
						<em class="iconfont icon-user8"></em>登录/注册
						</a>
	{#
		}
	#}
	</script>

	<!-- 统计代码 -->
	<div style="display: none;">
		${accessStatisticsCode }
		<!-- APP独立统计代码判断 -->
		<c:if test="${isAppASCOnoff eq 'on' and isAppTerminal }">
			${appAccessStatisticsCode }
		</c:if>
	</div>
	
	<!-- 红包开始 -->
	<c:if test="${onOffRedBag eq 'on'}">
	<style type="text/css">
		.red-bag-float{position:absolute;height:110px;width:90px;z-index: 999999;right: 0px;top: 50%;}
	</style>
	<div class="red-bag-float">
		 <a href="${base }/mobile/redPackage.do" external>
		 	<img src="${base }/common/images/redpacket/rt-ad-m.png" width="100%" height="100%"/>
		 </a>
		 <div style="width: 1.5rem;height: 1.5rem;position: absolute;bottom: 0px;margin-left: 39%;z-index: 9999999;" onclick="hideRedBag();"></div>
	</div>
	<script type="text/javascript">
		function hideRedBag(){
			$('.red-bag-float').hide();
		}
	</script>
	</c:if>
	<!-- 红包结束 -->
	<c:if test="${domainFolder == 'a09502'}"><jsp:include page="${base }/member/a09502/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder == 'x00204'}"><jsp:include page="${base }/member/x00204/mobileArticle.jsp" /></c:if>
	<c:if test="${domainFolder != 'a09502' && domainFolder != 'x00204'}"><jsp:include page="resource/new/noticeAlert/noticeAlert.jsp"/></c:if>
	<%-- <jsp:include page="resource/new/noticeAlert/noticeAlert.jsp"/> --%>
	<!-- 添加到主屏幕快捷方式 -->
	<c:if test="${not empty addToHomeScreen &&  addToHomeScreen ne 'off'}">
	<script type='text/javascript' src='${station }/anew/resource/addToHome/js/addtohomescreen.js'></script>
	<link rel="stylesheet" href="${station }/anew/resource/addToHome/css/addtohomescreen.css">
	<script>
		addToHomescreen({
			displayPace: ${addToHomeScreen} //显示时长
		});
	</script>
	</c:if>
</body>
</html>