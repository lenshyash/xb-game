<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<link rel="stylesheet" href="${station }/anew/resource/light7/css/light7.min.css">
<link rel="stylesheet" href="${station }/anew/resource/css/light7_reset.css">
<link rel="stylesheet" href="${station }/anew/resource/css/base.css?v=1">
</head>
<body>
	<div class="page page-current" id="drawNoticePage">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left" href="javascript: history.go(-1);">
				<span class="icon icon-left"></span>返回
			</a>
			<a class="title">${lotName }</a>
		</header>
		<div class="content">
			<div class="row no-gutter">
				<ul class="draw_notice_ul">
					<c:forEach items="${lotteryResults }" var="item" varStatus="status">
						<c:if test="${item!=null && item.haoMaList!=null}"><li>
							<a href="javascript: void(0);" class="external">
								<div class="">
									第${item.qiHao}期 
								</div>
								<div class="${ (lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC') ? 'haoma_pk10' : 'haoma' }">
									<c:set value="0" var="sum"/><c:set value="0" var="haveNum"/>
									<c:forEach items="${item.haoMaList }" var="haomaItem" varStatus="itemIndex">
										<c:if test="${lotCode == 'CQSSC' || lotCode == 'TJSSC' || lotCode == 'XJSSC' || lotCode == 'FFC' || lotCode == 'EFC' || lotCode == 'WFC'}">
											<c:if test="${haomaItem=='?'}"><c:set value="1" var="haveNum"/></c:if>
											<c:if test="${haomaItem!='?'}"><c:set value="${sum + haomaItem}" var="sum"></c:set></c:if>
										</c:if>
										<c:if test="${(lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC') && (itemIndex.index == 0 || itemIndex.index == 1)}">
											<c:if test="${haomaItem=='?'}"><c:set value="1" var="haveNum"/></c:if>
											<c:if test="${haomaItem!='?'}"><c:set value="${sum + haomaItem}" var="sum"></c:set></c:if>
										</c:if>
										<c:if test="${lotCode == 'GSK3' || lotCode == 'JSSB3' || lotCode == 'BJK3' || lotCode == 'HBK3' || lotCode == 'JXK3' || lotCode == 'AHK3' || lotCode == 'SHHK3' || lotCode == 'FFK3' || lotCode == 'WFK3' || lotCode == 'JPK3' || lotCode == 'KRK3' || lotCode == 'HEBK3' || lotCode == 'GXK3' || lotCode == 'GZK3'}">
											<c:if test="${haomaItem=='?'}"><c:set value="1" var="haveNum"/></c:if>
											<c:if test="${haomaItem!='?'}"><c:set value="${sum + haomaItem}" var="sum"></c:set></c:if>
										</c:if>
										<span class="${ (lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC') ? 'pk10 b' : '' }${ (lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC') ? haomaItem : '' }">${ (lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC') ? '' : haomaItem }</span>
									</c:forEach>
									<c:if test="${(lotCode == 'CQSSC' || lotCode == 'TJSSC' || lotCode == 'XJSSC' || lotCode == 'FFC' || lotCode == 'EFC' || lotCode == 'WFC')&&haveNum==0}">
										=&nbsp;<span>${sum}</span>
										<span>${sum>=23?'大':'小'}</span>
										<span>${sum%2==0?'双':'单'}</span>
									</c:if>
									<c:if test="${(lotCode == 'BJSC' || lotCode == 'XYFT' || lotCode == 'SFSC')&&haveNum==0}">
										=&nbsp;<span>${sum}</span>
										<c:choose>
											<c:when test="${version == 1 || version == 3}">
												<span>${sum>11?'大':sum<11?'小':'和'}</span>
												<span>${sum==11?'和':sum%2==0?'双':'单'}</span>
											</c:when>
											<c:otherwise>
												<span>${sum>11?'大':'小'}</span>
												<span>${sum%2==0?'双':'单'}</span>
											</c:otherwise>
										</c:choose>
									</c:if>
									<c:if test="${lotCode == 'GSK3' || lotCode == 'JSSB3' || lotCode == 'BJK3' || lotCode == 'HBK3' || lotCode == 'JXK3' || lotCode == 'AHK3' || lotCode == 'SHHK3' || lotCode == 'FFK3' || lotCode == 'WFK3' || lotCode == 'JPK3' || lotCode == 'KRK3' || lotCode == 'HEBK3' || lotCode == 'GXK3' || lotCode == 'GZK3'}">
										=&nbsp;<span>${sum}</span>
										<span>${sum>10?'大':'小'}</span>
										<span>${sum%2==0?'双':'单'}</span>
									</c:if>
								</div>
							</a>
						</li></c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>