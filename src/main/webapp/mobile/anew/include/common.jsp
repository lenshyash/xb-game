﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.game.core.SystemConfig" %>
<%@page import="com.game.util.StationUtil" %>
<%@page import="com.game.constant.StationConfig" %>
<%@page import="org.springframework.context.ApplicationContext" %>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@page import="com.game.service.AgentBaseConfigService" %>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.game.util.UserUtil" %>
<%
	String base = request.getContextPath();
	String station = request.getContextPath() + SystemConfig.MOBILE_CONTROL_PATH;
	pageContext.setAttribute("base", base);
	pageContext.setAttribute("station", station);

	if (UserUtil.isLogin()) {
		pageContext.setAttribute("account", UserUtil.getUserAccount());
		pageContext.setAttribute("login", true);
	} else {
		pageContext.setAttribute("login", false);
	}

	String website_name = (String) session.getAttribute("website_name");
	if(StringUtils.isEmpty(website_name)){
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		AgentBaseConfigService agentBaseConfigService = (AgentBaseConfigService) ctx.getBean("agentBaseConfigServiceImpl");

		Long stationId = StationUtil.getStationId();
		website_name = agentBaseConfigService.getSettingValueByKey(StationConfig.basic_info_website_name.name(), stationId);
		session.setAttribute("website_name", website_name);
	}
%>
<%@include file="check_ip.jsp" %>

<script type="text/javascript">
	var baseUrl = "${base}";
	var stationUrl = "${station}";
	var isLogin = '${isLogin}';
</script>