<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.content-block {
    padding: .01rem;
    margin: .25rem 0;
}
.buttons-tab .button {
    font-size: 0.9rem;
}
.buttons-tab .button.active {
    color: red;
    border-color: red;
}
</style>
 <div class="buttons-tab">
   <c:if test="${isCpOnOff=='on'}">
  	 <a href="#tab1" class="tab-link active button">彩票</a>
   </c:if>
   <c:if test="${isTyOnOff=='on' && isGuest != true}">
   	 <a href="#tab2" class="tab-link button">体育</a>
   </c:if>
   <c:if test="${isDzOnOff eq 'on'}">
   	 <a href="#tab3" class="tab-link button">电子</a>
   </c:if>
   <c:if test="${isZrOnOff=='on' && isGuest != true}">
   	 <a href="#tab4" class="tab-link button">真人</a>
   </c:if>
   <c:if test="${isChessOnOff=='on' && isGuest != true}">
   	 <a href="#tab5" class="tab-link button">棋牌</a>
   </c:if>
 </div>
 <div class="content-block">
   <div class="tabs">
     <div id="tab1" class="tab active">
       <div class="content-block">
        	<jsp:include page="games_cp.jsp"/>
       </div>
     </div>
     <div id="tab2" class="tab">
       <div class="content-block">
         	<jsp:include page="games_ty.jsp"/>
       </div>
       <div style="width:100%;float:left;font-size:12px;color:red;padding:5px 8px;display:none;" id="sb_message">
       *温馨提示：皇冠体育可直接下注，沙巴体育和BB体育需要在个人中心进行额度转换才能下注。
       </div>
       <div id="sjb_module" style="float:left;;width:100%;text-align: left;padding-left:30px;display:none;" >
				<input id="vip_level" type="button" value="世界杯赛程" style="border:none; padding:5px 8px; border-radius: 5px;cursor: pointer;margin-left:-23px;margin-top:15px;" />
		</div>
       <div id="shijiebei" style="position: fixed;top:0;left:0;right:0;bottom:0;background: rgba(0, 0, 0, 0.5);overflow: auto;z-index:99999;display:none;">
			<img src="${base }/mobile/v3/images/shijiebei.jpg" style="width:100%;">
			<img src="${base }/mobile/v3/images/close.png" style="position:fixed;top:50px;right:10px;cursor: pointer;" id="close">
		</div>
		<script>
			if(window.location.host.indexOf("bet365lk.com") != -1 || window.location.host.indexOf("bet365lh.com") != -1 || window.location.host.indexOf("bet365vr.com") != -1){
				$("#sjb_module").show()
			}
			$("#vip_level").click(function(){
	  			$("#shijiebei").show(200)
	  		})
	  		$("#close").click(function(){
	  			$("#shijiebei").hide(200)
	  		})
		</script>
     </div>
     <div id="tab3" class="tab">
       <div class="content-block">
         	<jsp:include page="games_cate_dz.jsp"/>
       </div>
     </div>
     <div id="tab4" class="tab">
       <div class="content-block">
         	<jsp:include page="games_cate_zr.jsp"/>
       </div>
     </div>
     <div id="tab5" class="tab">
       <div class="content-block">
         	<jsp:include page="games_cate_qp.jsp"/>
       </div>
     </div>
   </div>
 </div>