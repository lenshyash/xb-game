﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<script type='text/javascript' src='${station }/anew/resource/js/jquery/3.1.1/jquery.min.js'></script>
	<script type="text/javascript" src="${station }/anew/resource/js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="${station }/script/base64.min.js"></script>
	<script type='text/javascript'>
		$.config = {
			//router: false, // 禁用路由功能
		};
		
		$(function(){
			var onOffAppReg = '${onOffAppReg}';
			var regSwitch = localStorage.show_reg_switch;
			if(regSwitch){
				if('off' != onOffAppReg){
					$('.app_no_reg').show();
				}else{
					$('.app_no_reg').hide();
				}
			}else{
				$('.app_no_reg').show();
			}
		
		});
	</script>

	<script type='text/javascript' src='${station }/anew/resource/light7/js/light7.min.js?v=1'></script>
	<script type='text/javascript' src='${station }/anew/resource/light7/js/light7-swiper.js'></script>
	<script type='text/javascript' src='${station }/anew/resource/js/!this/light7-reset.js'></script>
	<script type="text/javascript" src="${station }/script/underscore/underscore-min.js"></script>
	<script type='text/javascript' src='${station }/anew/resource/js/common.js?v=1'></script>
	<script type="text/javascript" src="${station }/gamemenu_script.do"></script>
	<script type='text/javascript' src='${station }/anew/resource/js/!this/fastclick.js?v=1.0'></script>
	<script type='text/javascript' src='${station }/anew/resource/js/!this/main.js?v=1.262'></script>
	<!-- 修改头部颜色 -->
	<script>
window.onload=function(){ 
	var jsp = "<span id='mobileheadcolor' style='display:none;'>${mobileHeadColor }</span>"
	$("body").append(jsp)	
	mobileheadcolor();
} 
function mobileheadcolor(){
	var color = $("#mobileheadcolor").text()
	var of = $("#colorOn").text()
	if(of == 1){
		localStorage.setItem("colorB",color);	
	}else{
		color = localStorage.getItem("colorB")
	}
	var jdb = '<style>.bar-nav{background:'+color+'}</style>'
	$(".bar-nav").css("background",color)
	$(".top").css("background",color)
	$("body").append(jdb)
}
</script>
<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/mobile/chatMobileModule.jsp" ></jsp:include>
</c:if>