<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		
<c:if test="${isCpOnOff=='on'}">
		<c:forEach items="${bcLotterys }" var="item" varStatus="status">
			<div class="index_lottery_item">
				<c:choose>
					<c:when test="${item.code != 'LHC' && item.code != 'SFLHC' && item.code != 'TMLHC' && item.code != 'WFLHC' && item.code != 'HKMHLHC'}">
						<a href="${station }/${version == 2 ? 'v2/lottery.do?lotCode=' : 'do_pick_number.do?lotCode='}${item.code}" class="external">
							<div class="lottery_img">
								<img src="${station }/anew/resource/images/${item.code}.png" />
							</div>
							<div class="lottery_info">
								<div class="lottery_name">${item.name}</div>
							</div>
						</a>
					</c:when>
					<c:when test="${item.code == 'LHC' || item.code == 'SFLHC' || item.code == 'TMLHC' || item.code == 'WFLHC' || item.code == 'HKMHLHC'}">
						<a href="${station }/lhc/index.do?lotCode=${item.code }" class="external">
							<div class="lottery_img">
								<img src="${station }/anew/resource/images/${item.code}.png" />
							</div>
							<div class="lottery_info">
								<div class="lottery_name">${item.name}</div>
							</div>
						</a>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
				<div class="cl"></div>
			</div>
		</c:forEach>
</c:if>
<c:if test="${isVrOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardVr.do', '97');"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/vr.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">VR娱乐游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>