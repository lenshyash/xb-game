<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<c:if test="${isDzOnOff eq 'on' }">
	<!-- 新增普通版捕鱼 -->
	<c:if test="${isByOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=by"></c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();" </c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/buyu.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">捕鱼</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<!-- end -->
	<c:if test="${isAgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=ag"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/agdz.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">AG电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isMgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=mg"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/mg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MG电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isPtOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=pt"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/pt.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">PT电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>

	</c:if>
	<c:if test="${isQtOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=qt"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/qt.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">QT电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isBbinOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardBbin.do?type=game', '2')"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/bbin.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BBIN电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isBgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="javascript:go('${base}/forwardBg.do?type=2&h5=1', '98')"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/bg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">BG电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isTtgOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=ttg"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/ttg.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">TTG电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isMwOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=mw"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/mw.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">MW电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isCq9OnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=cq9"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/cq9.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">CQ9电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isJdbOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=jdb"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/jdb.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">JDB电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	<c:if test="${isIsbOnOff eq 'on' && isGuest != true}">
			<div class="index_lottery_item">
			 <a 
			 <c:if test="${isLogin}">href="${base }/mobile/egame/index.do?code=isb"</c:if>
			 <c:if test="${!isLogin}">href="javascript:void(0);" onclick="toLogin();"</c:if> class="external">
									<div class="lottery_img">
										<img src="${base }/mobile/anew/resource/new/images/isb.png" />
									</div>
									<div class="lottery_info">
										<div class="lottery_name">ISB电子游戏</div>
									</div>
			</a>
			<div class="cl"></div>
			</div>
	</c:if>
	</c:if>
