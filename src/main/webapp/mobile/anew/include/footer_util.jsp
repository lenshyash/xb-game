<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<c:if test="${isActive }">
		<div class="row no-gutter contact">
			<dl style="width: 33.33%">
				<a class="external" href="http://www.jiathis.com/share" target="_blank">
					<dt>
						<i class="icon icon-share"></i>
					</dt>
					<dd>分享</dd>
				</a>
			</dl>
			<dl style="width: 33.33%">
				<a class="external" href="${base}/?toPC=1">
					<dt>
						<i class="icon icon-computer"></i>
					</dt>
					<dd>电脑版</dd>
				</a>
			</dl>
			<dl style="width: 33.33%">
				<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
					<dt>
						<i class="icon icon-refresh"></i>
					</dt>
					<dd>刷新</dd>
				</a>
			</dl>
		</div>
	</c:if>
	<c:if test="${!isActive }">
		<div class="row no-gutter contact">
			<dl style="width: 25%">
				<a class="external" href="http://www.jiathis.com/share" target="_blank">
					<dt>
						<i class="icon icon-share"></i>
					</dt>
					<dd>分享</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="${base}/?toPC=1">
					<dt>
						<i class="icon icon-computer"></i>
					</dt>
					<dd>电脑版</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="${station }/active.do">
					<dt>
						<i class="iconfont pb2">&#xe654;</i>
					</dt>
					<dd>优惠活动</dd>
				</a>
			</dl>
			<dl style="width: 25%">
				<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
					<dt>
						<i class="icon icon-refresh"></i>
					</dt>
					<dd>刷新</dd>
				</a>
			</dl>
		</div>
	</c:if>