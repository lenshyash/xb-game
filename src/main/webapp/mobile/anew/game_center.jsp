<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<jsp:include page="include/css.jsp"></jsp:include>
</head>
<body>
	<div class="page page-current" id="gameCenterPage">
		<header class="bar bar-nav">
			<!-- 
			<a class="button button-link button-nav pull-left back" href="${station }/anew/index.do">
				<span class="icon icon-left"></span>返回
			</a>
			 -->
			<a class="title"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></a>
			<span class="pull-right">
				<div class="ui-toolbar-right ui-head">
					<a class="head-list" val="1" href="javascript:;"></a>
					<a class="head-icon" val="2" href="javascript:;"></a>
				</div>
			</span>
		</header>
		<nav class="bar bar-tab higher">
			<a class="tab-item external" href="${station }/index.do">
				<span class="icon icon-home"></span>
				<span class="tab-label">投注大厅</span>
			</a>
			<c:if test="${isCpOnOff=='on'}">
			<a class="tab-item active external" href="${station }/game_center.do">
				<span class="icon icon-app"></span>
				<span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
			</a>
			<a class="tab-item" href="${station }/draw_notice.do">
				<span class="icon icon-gift"></span>
				<span class="tab-label">开奖公告</span>
			</a>
			</c:if>
			<a class="tab-item" href="${station }/personal_center.do">
				<span class="icon icon-me"><span id="websiteSmsP_bm" class="badge" style="display: none;">9+</span></span>
				<span class="tab-label">个人中心</span>
			</a>
		</nav>
		<div class="content game_center">
			<div class="row no-gutter lott-menu">
				<ul>
					<li class="all cur" data-cat="0"></li>
					<li class="gaopin" data-cat="1"></li>
					<li class="dipin" data-cat="2"></li>
					<input type="hidden" id="lottypetype" value="0" />
				</ul>
			</div>
			<div class="row no-gutter lot-content">
				<div class="lottery-list-erect">
					<!-- 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋 -->
					<ul class="lotteryContent">
						<c:forEach items="${bcLotterys }" var="item">
							<li class="game_category_${(item.type == '4' || item.type == '54' || item.type == '6') ? 2 : 1}" lotcode="${item.code}">
								<c:choose>
									<c:when test="${item.code != 'LHC' && item.code != 'SFLHC' && item.code != 'TMLHC' && item.code != 'WFLHC' && item.code != 'HKMHLHC' }">
										<a href="${station }/${version == 2 ? 'v2/lottery.do?lotCode=' : 'do_pick_number.do?lotCode=' }${item.code }" class="external">
											<div class="hot-icon">
												<img class="" src="${station }/anew/resource/images/${item.code}.png">
											</div>
											<div class="erect-right">
												<div>
													<span class="pull-right result_qihao">第<span id="last_period_${item.code}">00000000000</span>期</span>
													<span class="hot-text lot_name">${item.name}</span>
												</div>
												<div class="last-time y_gutter" id="last_haoma_${item.code}">尚未开盘</div>
												<div class="y_gutter">
													<span class="pull-right bpCountDown_${item.code}" id="bpCountDown_${item.code}">00:00:00</span>
													<span class="stop_qihao">距<span id="bpRound_${item.code}">000000</span>期截止还有</span>
												</div>
											</div>
										</a>
									</c:when>
									<c:otherwise>
										<a href="${station }/lhc/index.do?lotCode=${item.code }" class="external">
											<div class="hot-icon">
												<img class="" src="${station }/anew/resource/images/${item.code}.png">
											</div>
											<div class="erect-right">
												<div>
													<span class="pull-right result_qihao">第<span id="last_period_${item.code}">00000000000</span>期</span>
													<span class="hot-text lot_name">${item.name}</span>
												</div>
												<div class="last-time y_gutter" id="last_haoma_${item.code}">尚未开盘</div>
												<div class="y_gutter">
													<span class="pull-right bpCountDown_${item.code}" id="bpCountDown_${item.code}">00:00:00</span>
													<span class="stop_qihao">距<span id="bpRound_${item.code}">000000</span>期截止还有</span>
												</div>
											</div>
										</a>
									</c:otherwise>
								</c:choose>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="lottery-list" style="display: none;">
					<ul class="lotteryContent">
						<c:forEach items="${bcLotterys }" var="item">
							<li class="game_category_${(item.type == '4' || item.type == '54' || item.type == '6') ? 2 : 1}" >
								<c:choose>
									<c:when test="${item.code != 'LHC' && item.code != 'SFLHC' && item.code != 'TMLHC' && item.code != 'WFLHC' && item.code != 'HKMHLHC' }">
										<a href="${station }/${version == 2 ? 'v2/lottery.do?lotCode=' : 'do_pick_number.do?lotCode=' }${item.code }" class="external">
											<div class="hot-icon">
												<img src="${station }/anew/resource/images/${item.code}.png">
											</div>
											<p class="hot-text">${item.name}</p>
											<p class="last-time">
												<span class="bpCountDown_${item.code}">00:00:00</span>
											</p>
										</a>
									</c:when>
									<c:otherwise>
										<a href="${station }/lhc/index.do?lotCode=${item.code }" class="external">
											<div class="hot-icon">
												<img src="${station }/anew/resource/images/${item.code}.png">
											</div>
											<div class="hot-text">${item.name}</div>
											<div class="last-time y_gutter">
												<span class="bpCountDown_${item.code}">00:00:00</span>
											</div>
										</a>
									</c:otherwise>
								</c:choose>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="include/js.jsp"></jsp:include>
</body>
</html>