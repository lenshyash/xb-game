<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp" %>
<%
Long stationId = StationUtil.getStation().getId();
String isZrOnOff = StationConfigUtil.get(stationId, StationConfig.onoff_zhen_ren_yu_le);
pageContext.setAttribute("onoffChangeMoney", StationConfigUtil.get(stationId, StationConfig.onoff_change_money));
pageContext.setAttribute("iosExamine", StationConfigUtil.get(stationId, StationConfig.onoff_mobile_ios_examine));
int i=2;
String czWidth = "100%";
if(isZrOnOff!=null && "on".equals(isZrOnOff)){
	i++;
}
if(i==2){
	czWidth = "50%";
}
if(i==3){
	czWidth = "33.3%";
}
pageContext.setAttribute("czWidth", czWidth);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<jsp:include page="include/css.jsp"></jsp:include>
</head>

<body>
	<div class="page page-current" id="personalCenterPage">
		<p:login login="true">
			<input id="logined" type="hidden" />
		</p:login>
		<nav class="bar bar-tab higher">
			<a class="tab-item external" href="${station }/index.do">
				<span class="icon icon-home"></span>
				<span class="tab-label">投注大厅</span>
			</a>
			<c:if test="${isCpOnOff=='on'}">
			<a class="tab-item" href="${station }/game_center.do">
				<span class="icon icon-app"></span>
				<span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
			</a>
			<a class="tab-item" href="${station }/draw_notice.do">
				<span class="icon icon-gift"></span>
				<span class="tab-label">开奖公告</span>
			</a>
			</c:if>
			<a class="tab-item active external" href="${station }/personal_center.do">
				<span class="icon icon-me"><span id="websiteSmsP_bm" class="badge" style="display: none;">9+</span></span>
				<span class="tab-label">个人中心</span>
			</a>
		</nav>
		<div class="content personal_center_content">
			<div class="usertou">
				<div class="userminc">
					<a href="${station }/personal_info.do" class="external" >
						<div class="user_logined_img"><img src="${station }/anew/resource/images/touxiang.png"/></div>
						<span class="account">${account }</span>
					</a>
					<span id="vip_message" class="account vip_level" style="color:yellow;margin-left:10px;font-weight: 600;"></span>
					<a id="logoutBtn" href="javascript: void(0);" class="pull-right">
						<!-- <em class="glyphicon glyphicon-log-out"></em> -->
						退出
						<em class="iconfont icon_loginout"></em>
					</a>
				</div>
				<div style="width:100%;height:2.6rem;margin-top:5px;display:none;border-bottom:1px solid #b50003;" id="p_level">
						<div style="padding-left:5%">VIP等级:<span class="vip_level"></span></div>
						<div style="padding-left:10%">升级充值所需:<span id="vip_need"></span></div>
						<div style="padding-left:5%">下一级:<span id="vip_next"></span></div>
						<div style="padding-left:10%">晋级赠送彩金:<span id="vip_gif"></span></div>
				</div>
					<script>
		$.ajax({
			url : "${base}/center/member/meminfo/mobieLevel.do",
			data : {},
			type : "post",
			dataType : 'json',
			success : function(j) {
				console.log(j)
				if(j != null) {
					$(".vip_level").html(j.current.toUpperCase())
					$("#vip_next").html(j.next.toUpperCase())
					$("#vip_need").html(j.need)
					$("#vip_gif").html(j.gifMoney)
				}
			}
		});
		$("#vip_message").click(function(){
			$("#p_level").toggle(200)
		})	
	</script>
				<style>
					#p_level div{
						width:40%;
						float: left;
						color:#fff;
						white-space: nowrap;
					}
					#p_level div span{
						color:yellow;
						padding-left:5px;
					}
				</style>
				<div class="doudoubox">
					<em class="iconfont icon-shujuku jindouicon"></em>
					可用余额： 
					<span id="login_mem_cash">${loginMember.money }</span>
					<c:if test="${iosExamine ne 'off' }">元</c:if>
					<c:if test="${iosExamine eq 'off' }">好运币</c:if>
				</div>
			</div>
			<c:if test="${isGuest != true && iosExamine ne 'off' }">
				<div class="qmenu">
					<a href="${station }/pay_select_way.do" class="chongzhitkuan external" style="width: ${czWidth}">
						<em class="iconfont icon-qia"></em>充值
					</a>
					<a href="${station }/withdraw_money.do" class="chongzhitkuan external" style="width: ${czWidth}">
						<em class="iconfont icon-qukuanji1"></em>提款
					</a>
					<c:if test="${isZrOnOff=='on' || isDzOnOff eq 'on' || isChessOnOff eq 'on' || isThdLotOnOff eq 'on' || isTsOnOff eq 'on'}">
						<a href="${station }/live_game.do" class="chongzhitkuan external" style="width: ${czWidth}">
							<em class="iconfont icon-qukuanji1"></em>额度转换
						</a>
					</c:if>
				</div>
			</c:if>
			<ul class="userlb">
				<c:if test="${isCpOnOff=='on'}">
				<li><a href="${station }/betting_record_lottery.do" class="external" ><em class="iconfont icon-lianxijilu icontublb"></em> 彩票投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isThdLotOnOff=='on'}">
				<li><a href="${station }/betting_record_lottery_third.do" class="external" ><em class="iconfont icon-sfcp icontublb"></em> 三方彩票记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isLhcOnOff=='on'}">
				<li><a href="${station }/betting_record_sixmark.do" class="external" ><em class="iconfont icon-61 icontublb"></em> 六合投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isTyOnOff=='on' && isGuest != true}">
				<li><a href="${base}/mobile/sports/hg/goOrderPage.do" class="external" ><em class="iconfont icon-zuqiu1 icontublb"></em> 体育投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isZrOnOff=='on' && isGuest != true}">
				<li><a href="${station }/betting_record_live.do" class="external" ><em class="iconfont icon-jilu1 icontublb"></em> 真人投注记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isChessOnOff=='on' && isGuest != true}">
				<li><a href="${station }/betting_record_chess.do" class="external" ><em class="iconfont icon-qipai icontublb"></em> 棋牌游戏记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isDzOnOff=='on' && isGuest != true}">
				<li><a href="${station }/betting_record_egame.do" class="external" ><em class="iconfont icon-iconwtbappwenjian icontublb"></em> 电子游戏记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${iosExamine ne 'off' && onoffChangeMoney=='on' && isGuest != true}">
				<li><a href="${station }/mnyrecord.do" class="external" ><em class="iconfont icon-record icontublb"></em> 用户账变记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${iosExamine ne 'off'}">
				<li><a href="${station }/account_details.do" class="external" ><em class="iconfont icon-jilu icontublb"></em> 账户明细记录 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<c:if test="${isExChgOnOff=='on' && isGuest != true}">
				<li><a href="${station }/scoreChange.do" class="external" ><em class="iconfont icon-bean-fill icontublb"></em> 积分兑换 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
				<li><a href="javascript:;" class="denglumima-btn"><em class="iconfont icon-21 icontublb"></em> 登陆密码修改 <em class="iconfont icon-right iconjtlb"></em></a></li>
				<c:if test="${iosExamine ne 'off' && isGuest != true }">
				<li><a href="javascript:;" class="qukanmima-btn" ><em class="iconfont icon-jilu01 icontublb"></em> 取款密码修改 <em class="iconfont icon-right iconjtlb"></em></a></li>
				</c:if>
			</ul>
			<ul class="userlb">
				<li><a href="${station }/website_message.do" class="external" >
				<em class="iconfont icon-email icontublb"></em> 
				<span>我的站内信 
					<icon class="iconfont icon-yuanquan1" style="color: #cd0005; font-size: 0.95rem;position:relative;top:-0.1rem;"></icon>
					<span style="font-size: 0.65rem; position: relative; color: #ffffff;left:-0.95rem;top:-0.1rem;" id="zhanneixin">${indexMsgCount }</span></span>
				<em class="iconfont icon-right iconjtlb"></em></a></li>
			</ul>
		</div>
		<!-- 弹出层 -->
		<div class="tanchuceng denglumima-tan">
			<div class="tanchuin">
				<div class="tangbiaoti">登陆密码修改</div>
				<div class="tannr">
					<ul class="xiugaimimaul">
						<li>
							<div class="xiugaimmbt">输入旧密码：</div>
							<div>
								<input id="oldpassword" type="password" class="xiugaimminpt" placeholder="请输入" value="" />
							</div>
						</li>
						<li>
							<div class="xiugaimmbt">输入新密码：</div>
							<div>
								<input id="newpassword" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="" />
							</div>
						</li>
						<li>
							<div class="xiugaimmbt">重复新密码：</div>
							<div>
								<input id="confirmpass" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="" />
							</div>
						</li>
						
					</ul>
					<div class="tannrbtn">
						<a id="passwordDialogCloseBtn" href="javascript: void(0);" class="close-tan" >取 消</a>
						<a id="passwordDialogOkBtn" href="javascript: void(0);" class="" >确 定</a>
						
					</div>
				</div>
			</div>
		</div>
	
		<!-- 弹出层 -->
		<div class="tanchuceng qukuanmian-tan">
			<div class="tanchuin">
				<div class="tangbiaoti">取款密码修改</div>
				<div class="tannr">
					<ul class="xiugaimimaul">
						<li>
							<div class="xiugaimmbt">输入旧密码：</div>
							<div>
								<input id="oldcashpasswd" type="password" class="xiugaimminpt" placeholder="请输入" value="" />
							</div>
						</li>
						<li>
							<div class="xiugaimmbt">输入新密码：</div>
							<div>
								<input id="newcashpasswd" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="" />
							</div>
						</li>
						<li>
							<div class="xiugaimmbt">重复新密码：</div>
							<div>
								<input id="confirmcashpw" type="password" class="xiugaimminpt" placeholder="密码需包英文数字" value="" />
							</div>
						</li>
						
					</ul>
					<div class="tannrbtn">
						<a id="cashpassDialogCloseBtn" href="javascript: void(0);" class="close-tan" >取 消</a>
						<a id="cashpassDialogOkBtn" href="javascript: void(0);" class="" >确 定</a>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="include/js.jsp"></jsp:include>
<script>
		$.ajax({
			url : "${base}/center/member/meminfo/mobieLevel.do",
			data : {},
			type : "post",
			dataType : 'json',
			success : function(j) {
				console.log(j)
				if(j != null) {
					$(".vip_level").html(j.current.toUpperCase())
					$("#vip_next").html(j.next.toUpperCase())
					$("#vip_need").html(j.need)
					$("#vip_gif").html(j.gifMoney)
				}
			}
		});
		$("#vip_message").click(function(){
			$("#p_level").toggle(200)
		})	
	</script>
	<style>
	#openChat{
		width:40px!important;
		top:60px!important;
	}
</style>
<script>
	$("#openChat img").attr('src','${base}/common/template/member/center/img/1516676959037063.png')
</script>
</body>
</html>