<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta http-equiv="Access-Control-Allow-Origin" content="*">
<link rel="shortcut icon" href="${base }/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="${base }/images/favicon.ico"> 
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1028735795,2203027842&fm=27&gp=0.jpg">
<link rel="stylesheet" href="${ares}/js/light7/css/light7.min.css">
<link rel="stylesheet" href="${ares}/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${ares}/css/common.css?v=1.0">
<link rel="stylesheet" href="${ares}/css/swiper-3.4.2.min.css">
<%--彩票css先独立，防止冲突，开发完成后合并到common.css  --%>
<link rel="stylesheet" href="${ares}/css/lottery.css">
<script type="text/javascript">
M = {
	base : '${base}',
	folder : "${stationFolder}",
	caipiao_version : '${caipiao_version}',
	version : '${version}',
	isLogin : '${isLogin}',
	res : '${m}',
}
</script>
<script type='text/javascript' src='${base}/mobile/v3/js/jquery-2.1.4.js' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/light7/js/light7.min.js' charset='utf-8'></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript">
	var baseUrl = '${base}';
</script>
</head>
<body>
	<div class="page" id="page_toRegChange_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base }/mobile/#/index3"> <span class="icon icon-left"></span> 返回
			</a>
			<a class="title">充值</a>
		</header>
		<%-- 这里是页面内容区 --%>
		<link rel="stylesheet"
			href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
		<div class="content">
			<div class="card">
				<div class="card-content">
					<div class="card-content-inner chongzhi_top">
						帐号：<em>${loginMember.account}</em>&nbsp;&nbsp;&nbsp;&nbsp; 余额：<em>${loginMember.money}元</em>
					</div>
				</div>
			</div>
			<c:if test="${dptMap.get('onlineFlag') eq 'on'}">
				<div class="card">
					<div class="card-header card_bot"
						style="background: rgb(245, 245, 245);">
						<em class="icon icoTop"></em><span>线上充值</span>
					</div>
					<div class="card-content">
						<div class="card-content-inner">
							<div class="list-block media-list">
								<ul class="radioClass">
									<c:choose>
										<c:when test="${onlineMap.size() == 0}">
											<li
												style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
										</c:when>
										<c:otherwise>
											<c:forEach items="${onlineMap}" var="line" varStatus="lines">
												<li><span
													<c:choose>
														<c:when test="${not empty line.icon}">style="background: url('${line.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '1'}">style="background: url('${base}/common/template/lottery/jiebao/images/banks.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '5'}">style="background: url('${base}/common/template/lottery/jiebao/images/qqpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '6'}">style="background: url('${base}/common/template/lottery/jiebao/images/jdpay.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '7'}">style="background: url('${base}/common/template/lottery/jiebao/images/baidu.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '8'}">style="background: url('${base}/common/template/lottery/jiebao/images/union.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '9'}">style="background: url('${base}/common/template/lottery/jiebao/images/unionpay.png') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '11'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:when test="${line.payType == '12'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:otherwise>
															class="icons ${line.iconCss}"
														</c:otherwise>
													</c:choose>>${line.payName}</span>
													<input id="radio${line.id}" name="radio" type="radio"
													value="${line.id}" min="${line.min}" max="${line.max}"
													iconcss="${line.iconCss}" pays_type="${line.payType}"
													pay_type="online"> <label for="radio${line.id}"><font
														style="font-size: .8rem;">最小充值金额<strong
															style="color: red;">${line.min}</strong>元
													</font></label>
													<c:if test="${fn:length(line.payDesc) > 0}">
													<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${line.payDesc}">${line.payDesc}</marquee>
													</c:if>
												</li>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			<c:if
				test="${dptMap.get('fastFlag') eq 'on' || dptMap.get('bankFlag') eq 'on'}">
				<div class="card">
					<div class="card-header card_bot"
						style="background: rgb(245, 245, 245);">
						<em class="icon icoBot"></em><span>线下充值</span>
					</div>
					<div class="card-content">
						<div class="card-content-inner">
							<div class="list-block media-list">
								<ul class="radioClass xxClass">
									<c:choose>
										<c:when test="${fastMap.size() == 0 && bankMap.size() == 0}">
											<li
												style="line-height: 2rem; text-align: center; font-size: .7rem;">暂时没有配置支付方式</li>
										</c:when>
										<c:otherwise>
											<c:if test="${fastMap.size() > 0}">
												<c:forEach items="${fastMap}" var="fast" varStatus="fasts">
													<li><span
														<c:choose>
												<c:when test="${not empty fast.icon}">style="background: url('${fast.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${fast.iconCss}"
												</c:otherwise>
											</c:choose>>${fast.payName}</span>
														<input id="radio${fast.id}" name="radio" type="radio"
														value="${fast.id}" min="${fast.min}" max="${fast.max}"
														flabel="${fast.frontLabel}" pay_type="fast"> <label
														for="radio${fast.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
																style="color: red;">${fast.min}</strong>元
														</font></label>
														<c:if test="${fn:length(fast.qrcodeDesc) > 0}">
														<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${fast.qrcodeDesc}">${fast.qrcodeDesc}</marquee>
														</c:if>
													</li>
												</c:forEach>
											</c:if>
											<c:if test="${bankMap.size() > 0}">
												<c:forEach items="${bankMap}" var="bank" varStatus="banks">
													<li><span title="${bank.payName}"
														<c:choose>
												<c:when test="${not empty bank.icon}">style="background: url('${bank.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${bank.iconCss}"
												</c:otherwise>
											</c:choose>>${bank.payName}</span>
														<input id="radio${bank.id}" name="radio" type="radio"
														value="${bank.id}" min="${bank.min}" max="${bank.max}"
														pay_type="bank"> <label for="radio${bank.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
																style="color: red;">${bank.min}</strong>元
														</font></label>
														<c:if test="${fn:length(bank.bankDesc) > 0}">
															<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${bank.bankDesc}">${bank.bankDesc}</marquee>
														</c:if>
													</li>
												</c:forEach>
											</c:if>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			<div class="list-block czList hide" id="online">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoMoney"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">充值金额</div>
								<div class="item-input">
									<input type="number" id="money" placeholder="请输入金额"
										style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="list-block czList hide" id="bank">
			</div>
			<div class="list-block czList hide" id="fast">
			</div>
			<div class="card card-desc hide">
				<div class="card-content">
					<div class="card-content-inner"></div>
				</div>
			</div>
			<div class="content-block">
				<div class="row zf-btn">
					<div class="col-100">
						<a href="javascript:void(0);" id="btn"
							class="button button-big button-fill button-danger">下一步</a>
					</div>
				</div>
			</div>
		</div>
		<div style="display:none;" id="onlineDesc">${empty dptMap.get('onlineDesc')?'及时自动到账，推荐':dptMap.get('onlineDesc')}</div>
		<div style="display:none;" id="fastDesc">${empty dptMap.get('fastDesc')?'支持微信/支付宝二维码扫描':dptMap.get('fastDesc')}</div>
		<div style="display:none;" id="bankDesc">${empty dptMap.get('bankDesc')?'支持网银转账，ATM转账，银行柜台汇款':dptMap.get('bankDesc')}</div>
		<jsp:include page="../v3/personal/popup/online_cz_popup.jsp"></jsp:include>
		<jsp:include page="../v3/personal/popup/fast_and_bank_popup.jsp"></jsp:include>
		<jsp:include page="../v3/personal/popup/template_pay.jsp"></jsp:include>
		<script type="text/javascript" src="${base}/common/js/onlinepay/pay.js?v=6.2673"></script>
		<script type="text/javascript" src="${ares}/js/pay_change.js?v=1.213"></script>
 		<script type="text/javascript">
			var hostUrl1="${hostUrl1}";
			var baseUrl = '${base}';
			function onlineSubmit(){
				$("#onlinePayForm").submit();
			}
		</script>
	</div>
	<jsp:include page="../v3/include/templateTpl.jsp"></jsp:include>
<script type="text/javascript" src="${ares}/js/layer_mobile/layer.js" charset="utf-8"></script>
<script type="text/javascript" src="${ares}/js/fastclick.js" charset="utf-8"></script>
<script type='text/javascript' src='${ares}/js/touchSlide.js' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/common.js?v=6.2' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/third.js?v=1.12' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/mobile.js?v=${caipiao_version}6' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/lotterys.js?v=1' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/lottery_zhuihao.js?v=1' charset='utf-8'></script>
<script type='text/javascript' src='${ares}/js/jquery.ajax.js?v=1' charset='utf-8'></script>
<script type="text/javascript" src="${ares}/js/swiper-3.4.2.jquery.min.js"></script>
</body>
</html>