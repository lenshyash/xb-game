<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<jsp:include page="include/css.jsp"></jsp:include>

</head>
<body>
	<div class="page page-current" id="drawNoticePage">
		<header class="bar bar-nav">
			<!-- 
			<a class="button button-link button-nav pull-left back" href="${station }/anew/index.do">
				<span class="icon icon-left"></span>返回
			</a>
			 -->
			<a class="title">开奖公告</a>
		</header>
		<nav class="bar bar-tab higher">
			<a class="tab-item external" href="${station }/index.do">
				<span class="icon icon-home"></span>
				<span class="tab-label">投注大厅</span>
			</a>
			<c:if test="${isCpOnOff=='on'}">
			<a class="tab-item" href="${station }/game_center.do">
				<span class="icon icon-app"></span>
				<span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
			</a>
			<a class="tab-item active external" href="${station }/draw_notice.do">
				<span class="icon icon-gift"></span>
				<span class="tab-label">开奖公告</span>
			</a>
			</c:if>
			<a class="tab-item" href="${station }/personal_center.do">
				<span class="icon icon-me"><span id="websiteSmsP_bm" class="badge" style="display: none;">9+</span></span>
				<span class="tab-label">个人中心</span>
			</a>
		</nav>
		<div class="content draw_notice_content">
			<div class="row no-gutter">
				<ul class="draw_notice_ul">
					<c:forEach items="${bcLotteryDatas }" var="item" varStatus="status">
						<li>
							<a href="${station }/draw_notice_details.do?lotCode=${item.lotCode}" class="external" >
								<div class="">
									${item.lotName}
									<em class="qihao">第${item.qihao}期</em>
								</div>
								<div class="${ (item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC') ? 'haoma_pk10' : 'haoma' }">
									<c:set value="${fn:split(item.haoma, ',') }" var="haomaArr" />
									<c:set value="0" var="sum"/>
									<c:forEach items="${haomaArr }" var="haomaItem" varStatus="itemIndex">
										<c:if test="${item.lotCode == 'CQSSC' || item.lotCode == 'TJSSC' || item.lotCode == 'XJSSC' || item.lotCode == 'FFC' || item.lotCode == 'EFC' || item.lotCode == 'WFC'}">
											<c:set value="${sum + haomaItem}" var="sum"></c:set>
										</c:if>
										<c:if test="${item.lotType == 0}">
											<c:set value="${sum + haomaItem}" var="sum"></c:set>
										</c:if>
										<c:if test="${(item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC') && (itemIndex.index == 0 || itemIndex.index == 1)}">
											<c:set value="${sum + haomaItem}" var="sum"></c:set>
										</c:if>
										<span class="${ (item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC') ? 'pk10 b' : '' }${ (item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC') ? haomaItem : '' }">${ (item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC') ? '' : haomaItem }</span>
									</c:forEach>
									<c:if test="${item.lotCode == 'CQSSC' || item.lotCode == 'TJSSC' || item.lotCode == 'XJSSC' || item.lotCode == 'FFC' || item.lotCode == 'EFC' || item.lotCode == 'WFC'}">
										=&nbsp;<span>${sum}</span>
										<span>${sum>=23?'大':'小'}</span>
										<span>${sum%2==0?'双':'单'}</span>
									</c:if>
									<c:if test="${item.lotType == 0}">
										=&nbsp;<span>${sum}</span>
										<span>${sum>10?'大':'小'}</span>
										<span>${sum%2==0?'双':'单'}</span>
									</c:if>
									<c:if test="${item.lotCode == 'BJSC' || item.lotCode == 'XYFT' || item.lotCode == 'SFSC'}">
										=&nbsp;<span>${sum}</span>
										<c:choose>
											<c:when test="${version == 1 || version == 3}">
												<span>${sum>11?'大':sum<11?'小':'和'}</span>
												<span>${sum==11?'和':sum%2==0?'双':'单'}</span>
											</c:when>
											<c:otherwise>
												<span>${sum>11?'大':'小'}</span>
												<span>${sum%2==0?'双':'单'}</span>
											</c:otherwise>
										</c:choose>
									</c:if>
								</div>
							</a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<jsp:include page="include/js.jsp"></jsp:include>
	<style>
	#openChat{
		width:40px!important;
		top:5px!important;
	}
</style>
<script>
	$("#openChat img").attr('src','${base}/common/template/member/center/img/1516676959037063.png')
</script>
</body>
</html>