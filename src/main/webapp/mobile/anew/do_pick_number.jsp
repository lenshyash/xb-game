<%@page import="com.game.util.SpringUtil"%>
<%@ page import="com.game.util.StationConfigUtil"%>
<%@ page import="com.game.model.lottery.LotteryEnum"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp" %>
<%@page import="com.game.service.lottery.BcLotteryService" %>
<%@page import="com.game.model.lottery.BcLottery" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<jsp:include page="include/css.jsp"></jsp:include>
</head>
<body>
	<!-- doPickNumberPage start -->
	<div class="page page-current" id="doPickNumberPage">
		<p:login login="true">
			<input id="logined" type="hidden" />
		</p:login>
		<input id="lotCode" type="hidden" value="${lotCode }">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${station }/index.do">
				<span class="icon icon-left"></span>返回
			</a>
			<a class="title">${lotName }</a>
			<div class="ui-bett-right">
				<a class="bett-head" href="javascript:;"></a>
			</div>
		</header>
		<div class="beet-rig" style="display: none;">
			<ul>
				<li><a href="${station}/betting_record_lottery.do" class="external">投注记录</a></li>
				<li><a href="${station}/draw_notice_details.do?lotCode=${lotCode}" class="external">最近开奖</a></li>
				<!-- 
				<li><a href="javascript: void(0);">敬请期待</a></li>
				<li><a href="javascript: void(0);">敬请期待</a></li>
				 -->
			</ul>
		</div>
		<nav class="bar bar-tab bett-foot">
			<button id="resetSelectBtn" class="btn-none">清空</button>
			<div class="beet-foot-txt">
				<p>
					<span id="betZhushu" class="bpBetCount">0</span>注&nbsp;&nbsp;<span class="orderTotalCost" class="red">0</span>元
				</p>
			</div>
			<button id="betReadyBtn" class="btn-add"><c:if test="${iosExamine eq 'off' }">模拟</c:if>投注</button>
		</nav>
		<div class="content">
			<div class="row no-gutter lot-tip">
				<div class="lot-time">
					<p class="qihao">
						距<span id="bpRound">20170111090</span>期截止
					</p>
					<div class="time-late" id="bpCountDown">
						<span id="time_h1">0</span> <span id="time_h2">0</span> <span
							class="time-kong"></span> <span id="time_m1">0</span> <span
							id="time_m2">0</span> <span class="time-kong"></span> <span
							id="time_s1">0</span> <span id="time_s2">0</span>
					</div>
				</div>
				<div class="dat-time">
					<p class="qihao">
						<span id="last_period">20170111089</span>期
					</p>
					<div class="wait-lot">
						<span id="firstHaoma" class="shangqihaoma-xal">
							<!-- 
							<em class="hong hongshu">09</em>
							<em class="hong hongshu">08</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							<em class="hong hongshu">03</em>
							 -->
						</span>
						<em class="iconfont icon-down"></em>
					</div>
					<div class="shangqixiala">
						<ul class="wangqilu" id="haomahistoryUl">
							<li>
								<div class="qishunq">
									<span>20160509069期</span> <em class="xyuandain"></em>
								</div>
								<div class="naxhaoma">
									<em class="">9</em> <em class="">0</em> <em class="">3</em>
								</div>
							</li>
							<li class="cur">
								<div class="qishunq">
									<span>20160509069期</span> <em class="xyuandain"></em>
								</div>
								<div class="naxhaoma">
									<em class="">9</em> <em class="">0</em> <em class="">3</em>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row no-gutter tabqie">
				<div id="bpBetTypeSel" class="tabqiein">
					<span class="cur">加载中..</span>
				</div>
			</div>
			<div class="row no-gutter xialabox">
				<div id="bpSubidSel" class="xialaitem">
					<span class="cur">加载中..</span>
				</div>
			</div>
			<div class="row no-gutter small_menu_box">
				<a href="javascript: void(0);" class="button button-dark pull-left open-popup open-playdesc" data-popup=".popup-playdesc" >玩法说明</a>
				<a id="bpRandom" href="javascript: void(0);" class="button button-dark pull-right" ><em class="iconfont icon-ttpodicon"></em>机选</a>
				<a id="goBuyCartBtn" href="javascript: void(0);" class="button button-dark pull-right buyCartCount" style="display: none;">返回购彩清单(<span id="orderCountShow">0</span>)</a>
			</div>
			<div class="row no-gutter game-content" id="gameContent">
			</div>
		</div>
	</div>
	<div class="popup popup-playdesc">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left close-popup" href="javascript: void(0);">
				<span class="icon icon-left"></span>关闭
			</a>
			<a class="title">玩法说明</a>
		</header>
		<div class="content">
			<div class="card">
				<div class="card-header">玩法介绍</div>
				<div class="card-content">
					<div class="card-content-inner" id="playDetailDesc"></div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">投注说明</div>
				<div class="card-content">
					<div class="card-content-inner" id="playWinExample"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="beet-odds-tips pop-win" style="display: none;">
		<div class="odds-tips-tit">
			<label>注单设定</label>
		</div>
		<!-- 滑动赔率 -->
		<div class="slidebg">
			<div class="slide-top">
				<div class="pull-left" id="prize_div">
					<label for="amount">奖金：</label><span id="bonusOdds">加载中..</span>
				</div>
				<div class="pull-right showRakeback">
					<label for="amount">返利：</label><span id="rakeback">加载中..</span>%</span>
				</div>
			</div>
		</div>
		<div class="beet-odds-info">
			<div>
				<span>倍数</span>
				<span class="maohao">：</span>
				<input class="beet-money-int" id="beishuInput" type="text" value="1" placeholder="" maxlength="7">
				<span>倍</span>
			</div>
			<div class="beet-money">
				<span>单位</span>
				<span class="maohao">：</span>
				<c:if test="${lotteryBetModel / 100 >= 1}">
				<a class="money-unit money-yuan acitve" data-unit="1" href="javascript:;">元</a>
				</c:if>
				<c:if test="${(lotteryBetModel % 100) / 10 >= 1}">
					<a class="money-unit" data-unit="10" href="javascript:;">角</a>
				</c:if>
				<c:if test="${lotteryBetModel % 10 == 1}">
					<a class="money-unit" data-unit="100" href="javascript:;">分</a>
				</c:if>
				<input id="cashUnit" type="hidden" value="1" />
			</div>
			<div>
				下注注数<span class="maohao">：</span><span class="bpBetCount">0</span>注
			</div>
			<div>
				下注总额<span class="maohao">：</span><span class="orderTotalCost">0</span>元
			</div>
			<button id="betReadyCanelBtn" class="odds-btn-none">取消</button>
			<button id="pushBetListAndBpBetBtn" class="odds-btn-ture">提交</button>
		</div>
	</div>
	<div class="tips-bg pop-win" style="display: none;"></div>
	<script type="text/html" id="doPNHaomaHistoryLiTemplate" style="display: none;">
					{#
					_.map(list, function(item, index){
					#}
						<li class="{# (index % 2) == 0 ? false : print('cur'); #}">
							<div class="qishunq">
								<span>{{item.qiHao}}期</span> <em class="xyuandain"></em>
							</div>
							<div class="naxhaoma">
								{#
								if(!item.haoMaList){
									item.haoMaList = ["等", "待", "开", "奖"];
								}
								_.map(item.haoMaList, function(haomaItem){
								#}
								<em class="">{{haomaItem}}</em>
								{#
								});
								#}
							</div>
						</li>
					{#
					});
					#}
	</script>
	<!-- doPickNumberPage end -->

	<!-- buyCartPage start -->
	<div class="page" id='buyCartPage'>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${station }/do_pick_number.do?lotCode=${lotCode }">
				<span class="icon icon-left"></span>返回
			</a>
			<a class='title'>${lotName } 购彩列表</a>
		</header>
		<nav class="bar bar-tab bett-foot">
			<c:if test="${lottery_order_chase_switch ne 'off' }">
			<button id="toznzhuihbtn" class="btn-none zhinegzhuihaobtn">智能追号</button>
			</c:if>
			<div class="beet-foot-txt">
				<p>
					<span id="bcTotalCount">0</span>注<span id="bcTotalMoney">0</span>元
				</p>
			</div>
			<button id="bcBet" class="btn-add"><c:if test="${iosExamine eq 'off' }">模拟</c:if>投注</button>
		</nav>
		<div class="content buycart-content">
			<div class="row no-gutter tzdansanbtn">
				<div class="tzdansanbtnitem"><a href="${station }/do_pick_number.do?lotCode=${lotCode }" class="back" ><em class="iconfont icon-plus"></em>再选一注</a></div>
				<div class="tzdansanbtnitem"><a id="bcSelectOne" href="javascript: void(0);" class="" ><em class="iconfont icon-plus"></em>机选一注</a></div>
				<div class="tzdansanbtnitem"><a id="bcClear" href="javascript: void(0);" class="" >清空列表</a></div>
			</div>
			<div class="tiaobox">
				<div class="tiaoneibox">
					<ul class="tiaoneiul" id="orderList">
						<li> 
							<a href="javascript: void(0);" class="" >投注号码为空</a>
						</li>
					</ul>
					<div class="xieyi-a">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="boBetLiTemplate" style="display: none;">
					{# var unitModel = {1: "元", 10: "角", 100: "分"}; var cashUnitCn = unitModel[cashUnit]#}
					<li data-orderID="{{orderID}}">
						<div class="haoma">
							{{ball}}
						</div>
						<div>{{beishu}}倍 {{cashUnitCn}}模式</div>
						<div>{{gameName}} {{betCount}}注 {{cash}}元</div>
						<em class="iconfont roundDeleBtn">&#xe655;</em>
					</li>
	</script>
	<!-- buyCartPage end -->

	<!-- doIntelligentSelectionPage start -->
	<div class="page" id='doIntelligentSelectionPage'>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${station }/do_pick_number.do?lotCode=${lotCode }">
				<span class="icon icon-left"></span>返回
			</a>
			<a class='title'>智能追号</a>
		</header>
		<div class="bar zhuhaobox">
			<input id="fbDlgStopBet" type="checkbox" class="vm" value="1" checked="checked">
			中奖后停止追号
			&nbsp;&nbsp;&nbsp;
			追 
			<div class="btngroup">
				<em class="jiabtn btnjiaj">-</em>
				<input id="qihaoCount" type="text" class="jiajianinpt" value="1">
				<em class="jianbtn btnjiaj">+</em>
			</div> 
			期
		</div>
		<nav class="bar bar-tab bett-foot">
			<div class="beet-foot-txt">
				<p>
					<em id="fbCount">1</em>期 <em id="fbCash" class="hong">2</em>元
				</p>
			</div>
			<button id="fbBet" class="btn-add"><c:if test="${iosExamine eq 'off' }">模拟</c:if>投注</button>
		</nav>
		<div class="content doIntelligentSelectionContent">
			<table class="zhuihaotable">
				<thead>
					<tr class="thtrbox">
						<td width="15%">序号</td>
						<td width="35%">倍数</td>
						<td width="60%">累计投入</td>
					</tr>
				</thead>
				<tbody id="fbOrderList">
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/html" id="doIntelligentSelectionTrTemplate" style="display: none;">
				<tr class="tbtrbox">
					<td>{{num}}</td>
					<td>
						<div class="btngroup">
							<em class="jiabtn btnjiaj">-</em>
							<input type="number" class="jiajianinpt" value="{{zhuihaoBeishu}}">
							<em class="jianbtn btnjiaj">+</em>
						</div>
					</td>
					<td class="betInfo"></td>
				</tr>
	</script>
	<script type="text/html" id="doIntelligentSelectionDetialTemplate" style="display: none;">
		<div>
			<span>期数</span>
			<span class="round"></span>
		</div>
		<div>
			<span>投入金额</span>
			<span class="betCost hong">{{betCost}}</span>元
		</div>
		<div>
			<span>累计投入</span>
			<span class="betTotalCost hong">{{betTotalCost}}</span>元
		</div>
	</script>
	<!-- doIntelligentSelectionPage end -->

	<jsp:include page="include/js.jsp"></jsp:include>
</body>
</html>