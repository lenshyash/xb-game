import {Toast} from 'vant';
import {Dialog} from 'vant';

//自定义通用组件
(function(window) {
	'use strict';
	var u = {};
  //注册免费试玩开关
  u.getGuestSwitch = function(cthis){
    $sky.ajax('get','native/getGuestSwitch',{},(ret)=>{
      if(ret.success == true){
        ret.content.onFlag === 'on'? cthis.$store.state.GuestSwitch = true: cthis.$store.state.GuestSwitch = false;
        ret.content.memberRegisterOnFlag === 'off'? cthis.$store.state.showRegister = false: cthis.$store.state.showRegister = true;
        ret.content.memberRegisterOnFlag === 'off'? cthis.$store.commit('showRegister', false): cthis.$store.commit('showRegister', true);
      }else{
        Toast.fail(ret.msg);
      }
    })
  };
	u.getHttp = function(){
	  if(window.location.hostname == '127.0.0.1' || window.location.hostname == 'localhost'|| window.location.hostname == '192.168.31.78'){
      // return 'https://x002.xb336.com/'; // 本地环境
      return '/api/'; // 后台代理(登陆接口不能用)
    }else {
      return '/'; // 正式环境
    }
	};
  u.getHttp2 = function(){
    if(window.location.hostname == '127.0.0.1' || window.location.hostname == 'localhost'|| window.location.hostname == '192.168.31.78'){
      // return 'https://x002.xb336.com/'; // 本地环境
      return '/api/'; // 后台代理(登陆接口不能用)
    }else {
      return '/'; // 正式环境
    }
  };
  //ajax组件
  u.ajax = function(method, path, param,callback) {
    var url = $sky.getHttp() + path + '.do';
    param = param && typeof param === 'object' ? param : new Object();
    var config = {
      url: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      transformRequest: [function(param) {
        return Qs.stringify(param);
      }]
    };
    // post请求时需要设定Content-Type
    if(method === 'post') {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      config.data = param;
    } else if(method === 'get') {
      config.params = param;
    }
    axios(config).then((ret)=>{
      callback(ret.data)
    }).catch((err)=>{
      console.log(err);
      console.log('获取' + path + '异常');
    });
  };
  //ajax组件不加.do
  u.ajaxHelp = function(method, path, param,callback) {
    var url = $sky.getHttp() + path ;
    param = param && typeof param === 'object' ? param : new Object();
    var config = {
      url: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      withCredentials: true,
      transformRequest: [function(param) {
        return Qs.stringify(param);
      }]
    };
    // post请求时需要设定Content-Type
    if(method === 'post') {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      config.data = param;
    } else if(method === 'get') {
      config.params = param;
    }
    axios(config).then((ret)=>{
      callback(ret.data)
    }).catch((err)=>{
      console.log(err);
      console.log('获取' + path + '异常');
    });
  };
  //ajax组件不加.do
  u.ajaxNb = function(method, path, param,callback) {
    var url = $sky.getHttp2() + path;
    param = param && typeof param === 'object' ? param : new Object();
    var config = {
      url: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Origin': 'https://chat.xbxb555.com',
      },
      transformRequest: [function(param) {
        return Qs.stringify(param);
      }]
    };
    // post请求时需要设定Content-Type
    if(method === 'post') {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      config.data = param;
    } else if(method === 'get') {
      config.params = param;
    }
    axios(config).then((ret)=>{
      callback(ret.data)
    }).catch((err)=>{
      //Toast('该浏览器暂不支持此功能');
      // Toast('您已被拉黑,无法连接聊天室');
      // Toast(JSON.stringify(err));
      console.log('获取' + path + '异常');
    });
  };
  //ajax组件不加.do
  u.ajax2 = function(method, path, param,callback) {
    var url = $sky.getHttp2() + path;
    param = param && typeof param === 'object' ? param : new Object();
    var config = {
      url: url,
      method: method,
      emulateJSON: true,
      credentials: true,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Origin': '*',
      },
      transformRequest: [function(param) {
        return Qs.stringify(param);
      }]
    };
    // post请求时需要设定Content-Type
    if(method === 'post') {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      config.data = param;
    } else if(method === 'get') {
      config.params = param;
    }
    axios(config).then((ret)=>{
      callback(ret.data)
    }).catch((err)=>{
      //Toast('该浏览器暂不支持此功能');
      // Toast(JSON.stringify(err));
      // Dialog({ message: JSON.stringify(err) });
      console.log(err)
      console.log('获取' + path + '异常');
    });
  };
  //ajax组件不加.do
  u.ajax3 = function(method, path, param,callback) {
    var url = $sky.getHttp2() + path;
    var config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    };
    // post请求时需要设定Content-Type
    if(method === 'post') {
      config.headers['Content-Type'] = 'multipart/form-data';
      // config.data = param;
    } else if(method === 'get') {
      config.params = param;
    }
    axios.post(url,param,config).then((ret)=>{
      callback(ret.data)
    }).catch((err)=>{
      alert(err);
      console.log('获取' + path + '异常');
    });
  };
  //获取公告信息
  u.getPlatformConfig = function(){
    $sky.ajax2('get', 'admin/agent/getPlatformConfig', {}, (ret) => {
      if (ret.success) {
        ret.data? this.notice = ret.data:'';
      }
    })
  };
  //获取彩种分类列表
  u.getLoctterys = function(lotGroup,callback){//1=时时彩，2=低频彩，3=PK10，5=11选5，6=香港彩，7=快三，8=快乐十分，9=PC蛋蛋
    $sky.ajax('post','native/getLoctterys',{lotGroup:lotGroup},(ret)=>{
      if(ret.success){
        callback(ret.content)
      }else{
        alert(ret.msg || '彩种列表错误');
      }
    })
  };
  //快速入款提交数据接口
  u.fastpaySubmit = function(params,callback){
    $sky.ajax('post','native/fastpaySubmit',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '快速入款提交数据接口错误');
      }
    })
  };
  //银行入款提交数据接口
  u.bankpaySubmit = function(params,callback){
    $sky.ajax('post','native/bankpaySubmit',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '银行入款提交数据接口错误');
      }
    })
  };
  //获取指定第三方的跳转提交数据
  u.getOnlineSubmitData = function(params,callback){
    $sky.ajax('post','native/getOnlineSubmitData',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '获取指定第三方的跳转提交数据错误');
      }
    })
  };
  //在线支付提交数据接口
  u.onlinePay = function(params,callback){
    $sky.ajax('post','native/onlinePay',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '银行入款提交数据接口错误');
      }
    })
  };
  //免增强跳转到第三方支付页
  u.payRedirect = function(params,callback){
    $sky.ajax('post','native/payRedirect',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '免增强跳转到第三方支付页接口错误');
      }
    })
  };
  //增强跳转到第三方支付页
  u.getWecahtQrcode = function(params,callback){
    $sky.ajax('post','native/getWecahtQrcode',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '增强处理接口错误');
      }
    })
  };
  //获取支付列表
  u.getPaySelectWay = function(lotGroup,callback){
    $sky.ajax('post','native/paySelectWay',(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '获取支付列表错误');
      }
    })
  };
  //获取号码生肖波色
  u.getZodiacsToHaoMa = function(params,callback){
    $sky.ajax('post','native/getZodiacsToHaoMa',params,(ret)=>{
      if(ret.success){
        callback(ret)
      }else{
        alert(ret.msg || '获取号码生肖波色错误');
      }
    })
  };
	//获取指定彩种历史开奖记录
	u.getResultDetai = function(lotCode,callback){
		$sky.ajax('post', 'native/open_result_detail', {
			lotCode: lotCode,
		}, (ret) => {
			if(ret.success) {
				callback(ret)
			} else {
				alert(ret.msg || '开奖记录异常');
			}
		})
	};
  u.getDrawNoticeDetails = function(lotCode,callback){
    $sky.ajax('post', 'native/draw_notice_details_data', {
      lotCode: lotCode,
    }, (ret) => {
      if(ret.success) {
        callback(ret)
      } else {
        alert(ret.msg || '开奖记录异常');
      }
    })
  };
	//获取最新一期开奖数据
	u.getLotLast = function(lotCode,qiHao,callback){
		$sky.ajax('post', 'native/lotLast', {
			lotCode: lotCode,
			qiHao: qiHao
		}, (ret) => {
			if(ret.success) {
				callback(ret.last)
			} else {
				alert(ret.msg || '最新一期开奖数据异常');
			}
		})
	};
	//获取离投注结果的倒计时开始时间
	u.getCountDown = function(lotCode,callback){
		$sky.ajax('post', 'native/getCountDown', {
			lotCode: lotCode,
      version: 5,
		}, (ret) => {
			if(ret.success) {
				callback(ret)
			} else {
				alert(ret.msg || '获取倒计时异常');
			}
		},(err)=>{
      callback(err)
    })
	};
	//获取当前时间
  u.getDateFull=function(dataTime) {
    if(!dataTime){
      return
    }
    if(typeof (dataTime) === 'number'){
      var date = new Date(dataTime);
    }else if(typeof (dataTime) === 'string') {
      var date = new Date(dataTime.replace(/-/g, '/'));
    }
    var Month = (date.getUTCMonth()+1) < 10 ? '0' + (date.getUTCMonth()+1) : date.getUTCMonth()+1;
    var UTCDate = date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate();
    var Hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var Minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var currentdate = date.getFullYear() + '-' + Month + '-' + UTCDate + ' ' + Hours + ":" + Minutes;
    return currentdate;
  };
	//cookie通用组件**s设置Cookie
	u.setCookie = function(key, val, time) {
		var date = new Date(); //获取当前时间
		var expiresDays = time; //将date设置为n天以后的时间
		date.setTime(date.getTime() + expiresDays * 24 * 3600 * 1000); //格式化为cookie识别的时间
		document.cookie = key + "=" + val + ";expires=" + date.toGMTString(); //设置cookie
	}
	//获取Cookie
	u.getCookie = function(key) {
		var getCookie = document.cookie.replace(/[ ]/g, ""); //获取cookie，并且将获得的cookie格式化，去掉空格字符
		var arrCookie = getCookie.split(";") //将获得的cookie以"分号"为标识 将cookie保存到arrCookie的数组中
		var tips; //声明变量tips
		for(var i = 0; i < arrCookie.length; i++) { //使用for循环查找cookie中的tips变量
			var arr = arrCookie[i].split("="); //将单条cookie用"等号"为标识，将单条cookie保存为arr数组
			if(key == arr[0]) { //匹配变量名称，其中arr[0]是指的cookie名称，如果该条变量为tips则执行判断语句中的赋值操作
				tips = arr[1]; //将cookie的值赋给变量tips
				break; //终止for循环遍历
			}
		}
		return tips;
	};
  //兼容手机端的日期转换
  u.getdate = function (n) {
    var now = new Date();
    now.setDate(now.getDate() - n);
    let y = now.getFullYear(),
      m = now.getMonth() + 1,
      d = now.getDate();
    return y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d);
  };
	//删除Cookie
	u.removeCookie = function(key) {
		var date = new Date(); //获取当前时间
		date.setTime(date.getTime() - 10000); //将date设置为过去的时间
		document.cookie = key + "=v; expires =" + date.toGMTString(); //设置cookie
	};
	//设置带有过期时间的localStorage
	u.setStorage = function(key, value) {
		var curTime = new Date().getTime();
		localStorage.setItem(key, JSON.stringify({
			data: value,
			time: curTime
		}));
	};
	//获取localStorage并判断是否过期
	u.getStorage = function(key, exp) {
		if(localStorage.getItem(key)) {
			var data = localStorage.getItem(key);
			var dataObj = JSON.parse(data);
			if(new Date().getTime() - dataObj.time > exp) { //已过期
				localStorage.removeItem(key); //删除过期缓存
				return false;
			} else { //未过期
				return true;
			}
		} else {
			return false;
		}
	};
	//环境URL切换：1,生产环境,2开发环境,3本地环境
	u.getUrl = function() {
		var science = 3;
		if(science === 1) {
			return 'https://' + window.location.host;
		} else if(science === 2) {
			return window.g.ApiUrl;
		} else if(science === 3) {
			return 'http://192.168.0.140:6060';
		}
	};
	//掉字符串所有空格
	u.trimAll = function(str) {
		return str.replace(/\s*/g, '');
	};
	u.getBcLotteryRecords = function(data, callback){
    $sky.ajax('post', 'native/get_lottery_order', data, (ret) => {
      if(ret) {
        callback(ret)
      } else {
      }
    })
  };
  // 投注
  u.betInfoV2 = function(betInfo, callback){
    $sky.ajax('post','native/dolotBet',betInfo,(ret)=>{
      callback(ret)
    })
  };

  // 年月日格式化
  Date.prototype.Format = function(fmt)
  { //author: meizz
    var o = {
      "M+" : this.getMonth()+1,                 //月份
      "d+" : this.getDate(),                    //日
      "h+" : this.getHours(),                   //小时
      "m+" : this.getMinutes(),                 //分
      "s+" : this.getSeconds(),                 //秒
      "q+" : Math.floor((this.getMonth()+3)/3), //季度
      "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
      fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
      if(new RegExp("("+ k +")").test(fmt))
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
  };

  //获取当前时间
  u.getDate=function(dataTime) {
    if(!dataTime){
      return
    }
    if(typeof (dataTime) === 'number'){
      return new Date(dataTime).Format("yyyy-MM-dd")
    }else if(typeof (dataTime) === 'string') {
      return new Date(dataTime.replace(/-/g, '/')).Format("yyyy-MM-dd")
    }
  };

  //获取帐户余额等相关信息
  u.meminfo = function(cthis) {
    $sky.ajax('get', 'meminfo', {}, (ret) => {
      if(ret.login) {
        cthis.meminfoDetails = ret;
      } else {
        if(!ret.login){
          cthis.$router.push({
            path: '/login',
            query: {}
          })
        }
      }
    })
  };

  //获取会员当日统计信息
  u.getMemberLotCount = function (This) {
    let begin = This.startTime;
    let end = This.endTime;
    let params = {
      account: $sky.getCookie('username'),
      begin: begin,
      end: end,
      agent: '',
      // searchType:2,
    };
    $sky.ajax('post', 'native/todayTeamStatic', params, (ret) => {
      if (ret.success) {
        let cthis = This;
        ret.content.forEach(function (item) {
          switch (item.onFlag) {
            case "lotteryTotal":
              cthis.detailsData.lotteryTotal = item.switchDesc;
              break;
            case "lotteryAward":
              cthis.detailsData.lotteryAward = item.switchDesc;
              break;
            case "todayWinAmount":
              cthis.detailsData.todayWinAmount = item.switchDesc;
              break;
          }
        })
      } else {
        if (!ret.success) {
          This.$router.push({
            path: '/login',
            query: {}
          })
        }
      }
    })
  }

  // 对象深拷贝函数
  u.deepCopy = function(object) {
    let obj = new Object();
    for (let k in object) {
      obj[k] = object[k]
    }
    obj.isActive = false;
    return obj
  };
	window.$sky = u;
})(window);



