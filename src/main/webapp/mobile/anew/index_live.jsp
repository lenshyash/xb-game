﻿<%@page import="com.game.util.StationConfigUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" uri="/permission"%>
<%@include file="include/common.jsp"%>
<%
	Long stationId = StationUtil.getStation().getId();
	pageContext.setAttribute("mobileTopmenuActivitiesAction", StationConfigUtil.get(stationId, StationConfig.mobile_topmenu_activities_action));

	pageContext.setAttribute("defaultLotCode", StationConfigUtil.get(stationId, StationConfig.basic_info_lottery_code));

	pageContext.setAttribute("iosExamine", StationConfigUtil.get(stationId, StationConfig.onoff_mobile_ios_examine));
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<jsp:include page="include/css.jsp"></jsp:include>
</head>
<body>
	<div class="page page-current" id="indexPage">
		<header class="bar bar-nav">
			<div class="logo">
				<a href="javascript: location.reload();" class="external">${website_name }</a>
			</div>
			<div id="loginStatusBar" class="login_status_bar">
				<p:login login="false">
					<a href="${station }/login.do" class="external"> <em class="iconfont icon-user8"></em>登录 | 注册
					</a>
				</p:login>
				<p:login login="true">
					<input id="logined" type="hidden" />
					<a href="${station }/personal_center.do">
						<div class="user_logined_img">
							<img src="${station }/anew/resource/images/touxiang.png">
						</div> <span> <em class="iconfont icon-jindous2x"></em><span>${loginMember.money }</span>
					</span>
					</a>
				</p:login>
			</div>
		</header>
		<nav class="bar bar-tab higher">
			<a class="tab-item active external" href="${station }/index.do">
				<span class="icon icon-home"></span>
				<span class="tab-label">投注大厅</span>
			</a>
			<c:if test="${isCpOnOff=='on'}">
			<a class="tab-item" href="${station }/game_center.do">
				<span class="icon icon-app"></span>
				<span class="tab-label"><c:if test="${iosExamine eq 'off' }">模拟投注</c:if><c:if test="${iosExamine ne 'off' }">购彩大厅</c:if></span>
			</a>
			<a class="tab-item" href="${station }/draw_notice.do">
				<span class="icon icon-gift"></span>
				<span class="tab-label">开奖公告</span>
			</a>
			</c:if>
			<a class="tab-item" href="${station }/personal_center.do">
				<span class="icon icon-me"><span id="websiteSmsP_bm" class="badge" style="display: none;">9+</span></span>
				<span class="tab-label">个人中心</span>
			</a>
		</nav>
		<div class="content">
			<c:if test="${!empty agentLunBos }">
				<div class="swiper-container lunbo" data-space-between='10'
					data-pagination='.swiper-pagination' data-autoplay="7000">
					<div class="swiper-wrapper">
						<c:forEach items="${agentLunBos }" var="item" varStatus="status">
							<div class="swiper-slide">
								<img src="${item.titleImg}">
							</div>
						</c:forEach>
					</div>
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div>
			</c:if>
			<div class="row no-gutter notice">
				<span class="tips_title">公告</span>
				<marquee id="noticeContent" scrollamount="3" class="tips_content font_color_gray"></marquee>
			</div>
			<!-- no-gutter: 无间距 -->
			<c:if test="${iosExamine ne 'off' }">
				<div class="row no-gutter quickMenu">
					<div class="col-25">
							<a class="qMenuItem color_huang"
								href="${station }/personal_center.do"><icon
									class="iconfont ft34">&#xe675;</icon>存／取款</a>
					</div>
					<div class="col-25">
						<a class="qMenuItem color_hong"
							href="${station }/personal_center.do"><icon
								class="iconfont pb2">&#xe64c;</icon>投注纪录</a>
					</div>
					<div class="col-25">
						<c:choose>
							<c:when test="${mobileTopmenuActivitiesAction=='2'}">
								<a class="qMenuItem color_lv external" href="${station }/appDownload.do"><icon
										class="iconfont pb2">&#xe630;</icon>A P P下载</a>
							</c:when>
							<c:otherwise>
								<a class="qMenuItem color_lv external" href="${station }/active.do"><icon
										class="iconfont pb2">&#xe630;</icon>优惠活动</a>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-25">
						<a class="qMenuItem color_lan external" id="customerServiceUrlLink" href="javascript: void(0);"><icon class="iconfont pb2">&#xe654;</icon>在线客服</a>
					</div>
				</div>
			</c:if>
			<c:if test="${isZrOnOff=='on' && isGuest != true}">
			<div class="content-block-title index-block-title">
				<em class="iconfont icon-yuanquan2"></em>真人娱乐
			</div>
			<div class="row no-gutter index_margin"><div id="liveGame">
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardAg.do?h5=1&amp;gameType=11', '1',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/ag.png"></div>
									<div>
										<div class="cpbt">AG娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardAg.do?h5=0&amp;gameType=6', '1',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/ag.png"></div>
									<div>
										<div class="cpbt">捕鱼王</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardBbin.do?type=live', '2',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/bb.png"></div>
									<div>
										<div class="cpbt">BBIN娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardVr.do', '97',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/vr.png"></div>
									<div>
										<div class="cpbt">VR娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardKy.do', '12',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/ky.png"></div>
									<div>
										<div class="cpbt">KY棋牌</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardBg.do?type=2&h5=1', '98',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/bg.png"></div>
									<div>
										<div class="cpbt">BG娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript:void(0)" onclick="go('${base}/forwardBbin.do?type=game', '2',this);" target="_blank" class="external">
									<div class="cptu"><img src="images/bb.png"></div>
									<div>
										<div class="cpbt">BBIN电子</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript: go('${base}/forwardMg.do?gameType=1&amp;gameid=59627&amp;h5=1', '3', this);" target="_blank" class="external">
									<div class="cptu"><img src="images/mg.png"></div>
									<div>
										<div class="cpbt">MG娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="${base}/mobile/egame/mg.do" class="external">
									<div class="cptu"><img src="images/mg.png"></div>
									<div>
										<div class="cpbt">MG电子</div>
									</div>
								</a>
							</div>
							<!--
							<div class="cpbox">
								<a href="javascript: $.alert('敬请期待');" class="external" >
									<div class="cptu"><img src="images/mg.png"/></div>
									<div>
										<div class="cpbt">MG娱乐</div>
									</div>
								</a>
							</div>
							<div class="cpbox">
								<a href="javascript: $.alert('敬请期待');" class="external" >
									<div class="cptu"><img src="images/mg.png"/></div>
									<div>
										<div class="cpbt">MG电子</div>
									</div>
								</a>
							</div>
							-->
					</div>
			</div>
			</c:if>
			<c:if test="${isCpOnOff=='on'}">
			<div class="content-block-title index-block-title">
				<em class="iconfont icon-yuanquan2"></em>彩票游戏
			</div>
			<div class="card index_margin">
				<a href='${ (defaultLotCode == "LHC" || defaultLotCode == "SFLHC") ? "lhc/index.do?lotCode=" : (version == "2" ? "v2/lottery.do?lotCode=" : "do_pick_number.do?lotCode=") }${defaultLotCode }' class="external">
					<img src="${station }/anew/resource/images/lottery_banner.png" class="index_banner" />
					<div class="index_card_description">
						时时彩、快乐<br/>十分、十一选五
					</div>
				</a>
			</div>
			</c:if>
			<c:if test="${isTyOnOff=='on' && isGuest != true}">
			<div class="content-block-title index-block-title">
				<em class="iconfont icon-yuanquan2"></em>体育赛事
			</div>
			<div class="card index_margin">
				<a href="${station }/sports/hg/index.do" class="external"> <img
					src="${station }/anew/resource/images/sports_banner.png"
					class="index_banner" />
					<div class="index_card_description">
						汇集全球各种精<br>彩赛事、欧洲杯等
					</div>
				</a>
			</div>
			</c:if>
			<c:if test="${iosExamine != 'off' }">
			<div class="row no-gutter contact">
				<dl style="width: 33.33%">
					<a class="external" href="http://www.jiathis.com/share" target="_blank">
						<dt>
							<i class="icon icon-share"></i>
						</dt>
						<dd>分享</dd>
					</a>
				</dl>
				<dl style="width: 33.33%">
					<a class="external" href="${base}/?toPC=1">
						<dt>
							<i class="icon icon-computer"></i>
						</dt>
						<dd>电脑版</dd>
					</a>
				</dl>
				<!-- 
				<dl>
					<a class="external" href="javascript:void(0)" onclick="javascript:alert('请联系客服人员！');">
						<dt>
							<i class="iconfont pb2">&#xe654;</i>
						</dt>
						<dd>在线客服</dd>
					</a>
				</dl>
				 -->
				<dl style="width: 33.33%">
					<a class="external" href="javascript:void(0)" onclick="javascript:window.location.reload();">
						<dt>
							<i class="icon icon-refresh"></i>
						</dt>
						<dd>刷新</dd>
					</a>
				</dl>
			</div>
			</c:if>
		</div>
	</div>

	<jsp:include page="include/js.jsp"></jsp:include>

	<script type="text/html" id="loginStatusBarTemplate" style="display: none;">
	{#
		if(logined == !0){
	#}
						<a href="${station }/personal_center.do">
							<div class="user_logined_img">
								<img src="${station }/anew/resource/images/touxiang.png"></div>
							</div>
									<span>
									<em class="iconfont icon-jindous2x"></em><span >{{money}}</span>
									</span>
						</a>
	{#
		} else {
	#}
						<a href="${station }/login.do" class="external">
						<em class="iconfont icon-user8"></em>登录/注册
						</a>
	{#
		}
	#}
	</script>
	<!-- 统计代码 -->
	<div style="display: none;">
		${accessStatisticsCode }
	</div>
</body>
</html>