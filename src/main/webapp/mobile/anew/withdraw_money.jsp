<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="include/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${website_name }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- 
<jsp:include page="include/css.jsp"></jsp:include>
 -->
<link rel="stylesheet" href="${station }/anew/resource/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${station }/anew/resource/light7/css/light7.min.css">
<link rel="stylesheet" href="${station }/anew/resource/light7/css/light7-swiper.min.css">
<link rel="stylesheet" href="${station }/anew/resource/css/light7_reset.css">
<link rel="stylesheet" href="${station }/anew/resource/css/base.css">



<link rel="stylesheet" href="${station }/anew/resource/css/deposit_withdraw.css">
</head>
<body>
	<div class="page page-current" id="withdrawPage">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left external" href="javascript: history.go(-1);">
				<span class="icon icon-left"></span>返回
			</a>
			<a class="title">提款</a>
		</header>
		<div class="content withdrawContent">
		</div>
	</div>

	<script type='text/javascript' src='${station }/anew/resource/js/jquery/3.1.1/jquery.min.js'></script>
	<script type="text/javascript" src="${station }/anew/resource/js/jquery/jquery.cookie.js"></script>
	<script type='text/javascript'>
		$.config = {
			// router: false, // 禁用路由功能
		};
	</script>

	<script type='text/javascript' src='${station }/anew/resource/light7/js/light7.min.js'></script>
	<script type='text/javascript' src='${station }/anew/resource/light7/js/light7-swiper.min.js'></script>
	<script type='text/javascript' src='${station }/anew/resource/js/!this/light7-reset.js'></script>
	<script type="text/javascript" src="${station }/script/underscore/underscore-min.js"></script>
	<script type='text/javascript' src='${station }/anew/resource/js/common.js'></script>




	<script type='text/javascript' src='${station }/anew/resource/js/!this/deposit_withdraw.js?v=2.2'></script>
</body>
</html>