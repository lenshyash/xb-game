<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	var baseUrl = "${base}";
	var stationUrl = "${station}";
	var isLogin = '${isLogin}';
</script>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title }</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" href="${station }/anew/resource/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${station }/anew/resource/light7/css/light7.min.css">
<link rel="stylesheet" href="${station }/anew/resource/light7/css/light7-swiper.min.css">
<link rel="stylesheet" href="${station }/anew/resource/css/light7_reset.css">
<link rel="stylesheet" href="${station }/anew/resource/css/base.css">
</head>
<body>
	<div class="page page-current" id="paySelectWay1Page">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left" href="javascript: void(0);" onclick="window.history.go(-1);">
				<span class="icon icon-left"></span>返回
			</a>
		</header>
		<div class="chongzhitishi">
			<iframe name="browserFrame" src="${kfUrl }" width="100%" height="100%"></iframe>
		</div>
	</div>
</body>
</html>