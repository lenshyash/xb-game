﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport"  content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2" type="text/css" />
</head>
<body >
<div class="top">
	<div class="inner">
		
		<div class="back">
			<a href="betting_record_sports.do" style="color: #fff;"><em class="iconfont icon-left"></em></a>
			<span class="vbktl">体育投注记录</span>
		</div>
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<!-- <div class="gantanhao"><a href="07-帮助说明.html" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
		<div class="cl"></div>
		
	</div>
</div>
<div class="cl h44"></div>
<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
<div class="" id="innerbox"> 
	<div class="shangqkaij tac">
		<span class="tac co000">投注记录明细</span>
		
		
		<div class="cl"></div>
	</div>
	<div class="bgf6 ov">
		<table class="tymingxtab">
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">交易时间</div>
				</td>
				<td>
					<div class="tymingxtabdiv">2016-05-08  13:50:42</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赛事</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						05-08南韩K经典联赛（滚球）独赢 <br>
						
						首尔<em class="hong">VS</em>浦项制铁<em class="hong">首尔</em>@ <em class="hong">7.7</em>
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">类型</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						足球
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赔率</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						7.7
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">下注金额（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						50

					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">可赢金额（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						0

					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">输赢（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						0

					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">状态</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						正常

					</div>
				</td>
				
			</tr>
			
		</table>
		<!-- 
		<table class="tymingxtab">
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">交易时间</div>
				</td>
				<td>
					<div class="tymingxtabdiv">2016-05-08  13:50:42</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赛事</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						05-08南韩K经典联赛（滚球）独赢 <br>
						
						首尔<em class="hong">VS</em>浦项制铁<em class="hong">首尔</em>@ <em class="hong">7.7</em>
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">类型</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						足球
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">赔率</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						7.7
					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">下注金额（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						50

					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">可赢金额（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						0

					</div>
				</td>
				
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">输赢（元）</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						0
					</div>
				</td>
			</tr>
			<tr>
				<td class="bttd">
					<div class="tymingxtabdiv">状态</div>
				</td>
				<td>
					<div class="tymingxtabdiv">
						正常

					</div>
				</td>
			</tr>
		</table>
		 -->
		<div class="cl h80"></div>
	</div>
	
	
	
	
</div>

</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
</html>
<%@include file="include/check_login.jsp" %>