﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>${website_name }</title>
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="stylesheet" rev="stylesheet" href="style/fonts/iconfont.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/reset.css"
	type="text/css" />
<link rel="stylesheet" rev="stylesheet" href="style/global.css?v=2"
	type="text/css" />
</head>
<body>
	<div class="top">
		<div class="inner">
			<div class="back">
				<a href="personal_center.do" style="color: #fff;"><em class="iconfont icon-left"></em></a> <span class="vbktl">体育投注记录</span>
			</div>
			<!-- <div class="gantanhao"><a href="07-帮助说明.jsp" class="cw" ><em class="iconfont icon-iconfonticonfontinfo"></em></a></div> -->
			<div class="cl"></div>
		</div>
	</div>
	<div class="cl h44"></div>
	<!-- id=innerbox 标签外面 不放任何标签  为自适应标签   -->
	<div class="" id="innerbox">
		<div class="touzhubox">
			<div id="selectQueryType" class="touzhuqie" data-current="all">
				<span data-querytype="all" class="cur" style="width: 25%;">全部记录 </span> 
				<span data-querytype="win" class="" style="width: 25%;">中奖记录 </span> 
				<span data-querytype="init" class="" style="width: 25%;">未开奖 </span> 
				<span data-querytype="canel" class="" style="width: 25%;">未成功订单 </span> 
			</div>
			<div class="cl"></div>
			<div class="touzhusjbox">
				<div class="fl pl22 pt12">
					<div id="queryDatetime" class="touzhusj selectComponent" data-value="">
						<div class="fangsjbox">时间</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="today">今天</li>
								<li data-value="yesterday">昨天</li>
								<li data-value="week">近一周</li>
								<li data-value="month">近30天</li>
								<li data-value="monthbefore">30天以前</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="fl pl22 pt12">
					<div id="queryBallcode" class="touzhusj selectComponent" data-value="">
						<div class="fangsjbox">球种筛选</div>
						<em class="iconfont icon-down downem"></em>
						<div class="sjxialabox">
							<ul>
								<li data-value="FT">足球</li>
								<li data-value="BK">篮球</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="cl"></div>
			<div class="caizhongtable">
				<table>
					<tr class="caizhongtabletr">
						<td>彩种</td>
						<td>投注时间</td>
						<td>投注金额</td>
						<td>当前状态</td>
						<td>操作</td>
					</tr>
					<tbody id="recordContent">
					</tbody>
				</table>
			</div>
			<div class="cl"></div>
			<div class="cl h80"></div>
		</div>
	</div>
</body>

<script type="text/javascript" src="script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="script/global.js"></script>
<script type="text/javascript" src="script/common.js"></script>
<script type="text/javascript" src="script/$this/betting_record_sports.js"></script>
</html>
<script type="text/html" id="recoredTemplate" style="display: none;">
					{# 
						_.map(data, function(item){
					 #}
					<tr class="">
						<td>{{item.ballname}}</td>
						<td>{{item.bettime}}</td>
						<td>{{item.betMoney}}元</td>
						<td>{{item.betStatus}}
						</td>
						<td><a href="betting_record_sports_details.do" class="lanx">查看详情</a></td>
					</tr>
					{#
						});
					#}
					<!-- 
					<tr class="">
						<td>足球</td>
						<td>16-05-09 12:12</td>
						<td>10.00元</td>
						<td>中奖<em class="hong">26.00</em>元
						</td>
						<td><a href="betting_record_sports_details.do" class="lanx">查看详情</a></td>
					</tr>
					 -->
</script>
<%@include file="include/check_login.jsp" %>