﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Peak Brain Training - Level Up Your Brain!</title>
	<link rel="icon" href="https://get.peak.net/images/peaklogo.png" type="image/png">
	<!-- Pure CSS Framework -->
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/grid-old-ie.css">
    <![endif]-->
    <!--[if gt IE 8]>
        <link rel="stylesheet" href="css/grid.css">
    <!--<![endif]-->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- scripts -->
    <script src="scripts/lory.min.js" type="text/javascript"></script>
    <script>
    	document.addEventListener('DOMContentLoaded', function () {
        var multiSlides = document.querySelector('.js_multislides');

        lory(multiSlides, {
            infinite: 3,
            slidesToScroll: 3
        });
    });
    </script>
</head>
<body>
	<!-- Header -->
	<div class="header">
	    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
	        <div class="container pure-g">
	        	<div class="pure-u-md-1-3 pure-u-8-24">
	        		<a class="pure-menu-heading logo" href="#">Peak</a>
	        	</div>
	        	<div class="pure-u-md-1-3 pure-hidden-sm"></div>
			</div>
	    </div>
	</div>

	<!-- hero image -->
	<div class="splash-container pure-g">
	    <div class="container">
		    <div class="splash pure-u-md-12-24 pure-u-1" id="download">
		    	<h1 class="splash-head">PLAY, SMARTER.</h1>
		    	<p class="splash-subhead">
		    		Discover what you can do with Peak, the number 1 app to challenge your brain.
		    	</p>
		    	<p>Push your cognitive skills, train harder and use your time better with fun, stimulating games and workouts.</p>
			</div>
		   	<div class="app-preview pure-u-md-12-24 pure-u-1">
		       	<img src="img/peak-hero-visual.png" class="pure-img" alt="Peak Brain Training">
		    </div>
		</div>
	</div>
	<!-- reviews -->
	<div class="strip-background pure-hidden-sm pure-hidden-xs">
		<div class="container slider js_multislides multislides">
			<div class="frame js_frame">
				<ul class="slides js_slides">
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Daily routine with Peak</h4>
								<p>Fun, easy and now with the latest update better overview over the process.</p>
							</div>
							<span class="author">Spjuver, App Store</span>
						</li>
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Love it</h4>
								<p>Has really helped improve my focus and mental agility and would recommend to people of all ages!</p>
							</div>
							<span class="author">LHinch, App Store</span>
						</li>
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Fun and Motivational</h4>
								<p>It's a really fun game that makes you want to try harder than your last result in a game.</p>
							</div>
							<span class="author">Kr_sexton, App Store</span>
						</li>
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Fun and stimulating!</h4>
								<p>The games are well designed and much fun to play. I look forward to each day's selection of challenges.</p>
							</div>
							<span class="author">SoraDW, App Store</span>
						</li>
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Great app</h4>
								<p>Great app for keeping you on your toes. Cleverly formulated games to push you and test your agility.</p>
							</div>
							<span class="author">Walkman2399, App Store</span>
						</li>
						<li class="">
							<div class="ppl-quote">
								<span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
								<h4>Awesome!</h4>
								<p>The games are fun and challenging, and also incredibly addictive. It's definitely worth the download!</p>
							</div>
							<span class="author">Lenz3119, App Store</span>
						</li>
				</ul>
			</div>
					<span class="js_prev prev">
		                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#666" d="M302.67 90.877l55.77 55.508L254.575 250.75 358.44 355.116l-55.77 55.506L143.56 250.75z"></path></g></svg>
		            </span>
		            <span class="js_next next">
		                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#666" d="M199.33 410.622l-55.77-55.508L247.425 250.75 143.56 146.384l55.77-55.507L358.44 250.75z"></path></g></svg>
		            </span>
		</div>

	</div>

	<!-- coach -->
	<div class="coach strip-background">
		<div class="pure-g container">
			<div class="pure-u-md-10-24 pure-u-1 img-container">
				<img src="img/peak-coach-visual.png" class="pure-img" alt="Train with Coach">
			</div>
			<div class="pure-u-md-2-24 pure-u-1 pure-hidden-xs">
			</div>
			<div class="pure-u-md-12-24 pure-u-1">
				<h2>Meet Coach, your Personal Trainer in Peak.</h2>
				<p>Everyone is different and Coach is here to help you reach your Peak goals. Coach will challenge you with new workouts, track your progress and show you where and how you can improve, keeping you motivated and pushing you further.</p>
			</div>
		</div>
	</div>

	<!-- science -->
	<div class="science">
		<div class="pure-g container">
			<div class="pure-u-md-12-24 pure-u-1">
				<h2>Innovation Matters</h2>
				<p>We love games at Peak and we are passionate about research too. We work with scientists studying the impact of video games from Cambridge, Yale, UCL and King’s College London and we bring our customers games built using this research.</p>
			</div>
			<div class="pure-u-md-2-24 pure-u-1 pure-hidden-xs">
			</div>
			<div class="pure-u-md-10-24 pure-u-1 img-container">
				<img src="img/peak-research-visual.png" class="pure-img" alt="We <3 working with universities!">
			</div>
		</div>
	</div>

	<!-- Pro -->
	<div class="pro-upsell strip-background">
		<div class="pure-g container">
			<div class="pure-u-md-10-24 pure-u-1 img-container">
				<img src="img/peak-pro-visual.png" class="pure-img" alt="Get Peak Pro">
			</div>
			<div class="pure-u-md-2-24 pure-u-1 pure-hidden-xs">
			</div>
			<div class="pure-u-md-12-24 pure-u-1">
				<h2>Reach Your Goals Faster with Peak Pro</h2>
				<p>Unlock the full potential of Peak with Pro and get dozens of tailored workouts, insightful analysis of your performance to help you go further, and access to the complete catalogue of more than 40 games and activities.</p>
				<!-- <a class="pure-button pure-button-primary pro-button">Get Pro</a> -->
			</div>
		</div>
	</div>

	<!-- advanced training -->
	<div class="advanced-training">
		<div class="pure-g container">
			<div class="pure-u-md-12-24 pure-u-1">
				<h2>Advanced Training Programmes</h2>
				<p>Our Advanced Training Programmes are the next step in cognitive exercise. We’ve partnered with scientists from leading institutions including Yale and Cambridge to provide you with training programmes that challenge very specific skills, like memory and attention. These programmes are more focused but the effort is worth it.</p>
				<!-- <a class="pure-button pure-button-primary pure-hidden-md pure-hidden-lg pure-hidden-xl">Open in App</a> -->
			</div>
			<div class="pure-u-md-2-24 pure-u-1 pure-hidden-xs">
			</div>
			<div class="pure-u-md-10-24 pure-u-1 img-container">
				<div class="lower-shadow"></div>
				<img src="img/peak-adv-training-visual.png" class="pure-img" alt="Meet our Advanced Trainings!">
			</div>
		</div>
	</div>

</body>
</html>