<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String base = request.getContextPath();
	request.setAttribute("base",base);
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>创建或者关联账户信息</title>
	<link rel="stylesheet" rev="stylesheet" href="${base}/mobile/script/bootstrap/css/bootstrap.min.css" type="text/css" />
</head>
<body>
	<div>
	  <!-- Nav tabs -->
	  <ul id="myTabs" class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#add" aria-controls="add" role="tab" data-toggle="tab" style="font-size:18px;">创建新账号</a></li>
	    <li role="presentation"><a href="#bind" aria-controls="bind" role="tab" data-toggle="tab" style="font-size:18px;">关联已有账号</a></li>
	  </ul>
	
	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="add">
	    	<form method="post"  id="form1">
				<div class="row" style="margin-top:20px;">
					<div class=" col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
						<fieldset>
							<div class="input-group input-group-lg">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
								<input name="username" type="text" class="form-control" placeholder="用户名">
							</div>
							<div class="clearfix"></div><br>
							<div class="input-group input-group-lg">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
								<input name="password" type="password" class="form-control" placeholder="密码">
							</div>
							<div class="clearfix"></div><br>
							<div class="input-group input-group-lg">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
								<input name="repassword" type="password" class="form-control" placeholder="确认密码">
							</div>
							<div class="clearfix"></div><br>
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<button type="button" class="btn btn-primary btn-lg btn-block" id="addBtn"><i class="glyphicon glyphicon-hand-right"></i> 创 建</button>
								</div>
							</div>
						</fieldset>
					</div>
				</div> 
				
		    </form>
	    </div>
	    
	    <div role="tabpanel" class="tab-pane" id="bind">
	    	 <form method="post"  id="form2">
				<div  class="row" style="margin-top:20px;">
					<div class="col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
						<fieldset>
							<div class="input-group input-group-lg">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
								<input name="username" type="text" class="form-control" placeholder="用户名">
							</div>
							<div class="clearfix"></div><br>
							<div class="input-group input-group-lg">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
								<input name="password" type="password" class="form-control" placeholder="密码">
							</div>
							<div class="clearfix"></div><br>
							<div class="clearfix"></div><br>
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<button type="button" class="btn btn-primary btn-lg btn-block" id="bindBtn"><i class="glyphicon glyphicon-hand-right"></i> 关 联</button>
								</div>
							</div>
						</fieldset>
					</div>
				</div> 
   			 </form>
	    </div>
	  </div>
	</div>
</body>
</html>

<script type="text/javascript" src="<%=base%>/mobile/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<%=base%>/common/template/member/core.js"></script>
<script type="text/javascript" src="<%=base%>/mobile/script/bootstrap/js/bootstrap.min.js"></script>

<script>
   $("#addBtn").bind("click",function(){
	   var $form = $("#form1");
	   var account = $.trim($form.find("input[name=username]").val());
	   if(account == ""){
		   alert("请输入账号");
		   return;
	   }
	   
	   if(account.length < 5 || account.length > 11){
		   alert("账号必须在6-11位");
		   return;
	   }
	   var pwd = $form.find("input[name=password]").val();
	   if(pwd == ""){
		   alert("请输入密码");
		   return;
	   }
	   if(pwd.length < 6 || pwd.length > 16 ){
		   alert("账号必须在6-16位");
		   return;
	   }
	   var repwd = $form.find("input[name=repassword]").val();
	   if(pwd != repwd){
		   alert("前后密码不一致");
		   $form.find("repassword").val("");
		   $form.find("repassword").foucus();
		   return;
	   }
	   addAccount(1,account,pwd);
   });
	
   $("#bindBtn").bind("click",function(){
	   var $form = $("#form2");
	   var account = $.trim($form.find("input[name=username]").val());
	   if(account == ""){
		   alert("请输入账号");
		   return;
	   }

	   var pwd = $form.find("input[name=password]").val();
	   
	   if(pwd == ""){
		   alert("请输入密码");
		   return;
	   }
	   
	   addAccount(2,account,pwd);
   });
   
   
   function addAccount(type,account,pwd) {
       $.ajax({
           url: "<%=request.getContextPath()%>/mobile/wechat/addAccount.do",
           data: { 
           	account: account, 
           	pwd: pwd,  
           	type: type 
           },
           success: function (data) {
               window.location.href = "<%=request.getContextPath()%>/m/index.do";
           }
       });
   }
</script>