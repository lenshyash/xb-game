<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../include/common.jsp"%>
<%@ page import="com.game.model.lottery.LotteryEnum"%>
<!DOCTYPE html>
<html class="ui-mobile">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<title>${website_name }</title>
<link href="${station }/lhc/content/bootstrap.min.css" rel="stylesheet" />
<link href="${station   }/lhc/content/layout.css?v=5" rel="stylesheet" />
<script type="text/javascript" src="${station }/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${station }/script/underscore/underscore-min.js"></script>
<script type="text/javascript" src="${station }/script/global.js"></script>
<script type="text/javascript" src="${station }/script/common.js"></script>
</head>
<body>
	<div class="bg-info header-logo">
		<div class="pull-left register">
			<a href="${station }/index.do"> <span class="glyphicon glyphicon-home fontsize"></span></a>
		</div>
		<div class="pull-right register">
			<a class="btn" style="background: #DBE6E9; font-weight: bold; border-radius: 10px;" onclick="caizhongxz()"> 彩种选择 <span class="caret"></span></a>
		</div>
		<div style="clear: both"></div>
	</div>
	<div class="body-banner" id="caizhong" style="display: none;"></div>
	<script>
		$(function(){
			$._ajax({url: stationUrl + "/lottery/getAllActiveLot.do", data: {version: 2}}, function (status, message, data, result) {
				var tplHtml = $("#lotteryItemTemplate").html();
				var $caizhong = $("#caizhong").empty();
				$caizhong.html(_.template(tplHtml)({list: data}));
			});
		});
	</script>
	<script type="text/html" style="display: none;" id="lotteryItemTemplate">
		<table>
			{#
				_.each(list, function(item, index, list){
			#}
			{# print((index == 0) ? '<tr>' : ''); #}
			{# print((index != 0 && index % 3 == 0 && index != list.length - 1) ? '</tr><tr>' : ''); #}
				<td><a data-gname="{{item.code}}" class="{{item.code == '${lotCode}' ? 'gameActive' : ''}}" href="{# if( item.code != 'LHC' && item.code != 'SFLHC') { print('${station}/v2/lottery.jsp?lotCode=' + item.code ); } else { print('${station}/lhc/index.do?lotCode=' + item.code); } #}"><b>{{item.name}}</b><br /></a></td>
			{# print((index == list.length - 1) ? '</tr>' : ''); #}
			{#
				});
			#}
		</table>
	</script>
	<link href="${station }/v2/style/BJSC.css" rel="stylesheet" />
	<style>
		span[class^=span_ball_] {
			height: 26px;
			width: 26px;
			display: inline-block;
			color: #fff;
			padding-top: 2px;
		}
	</style>
	<div class="bjkcgame postionre">
		<div class="text-center paddingtopbotton5 result-time-bg">
			第 <span class="text-danger" id="lastResultTerm"></span> 期开奖结果&nbsp;&nbsp;&nbsp;&nbsp; <a href="${station }/draw_notice_details.do?lotCode=${lotCode}" class="btn btn-info btn-sm">历史开奖</a>
		</div>
		<div class="text-center result-con" id="lastResultHaoma"></div>
		<div class="text-center paddingtopbotton5 result-time-bg">
			<span class="fontwidth text-red">${lotName }</span>&nbsp;&nbsp;&nbsp;余额:&nbsp;<span class="text-red" id="createshow">${loginMember.money }</span>&nbsp; <a href="${station }/betting_record_lottery.do" class="btn btn-info btn-sm">下注记录</a>
		</div>
		<div class="text-center paddingtopbotton5 result-time-bg">
			<span class="text-danger memRound"></span> 期封盘时间：<span class="text-right text-red fengpantime"></span> <span class="reciprocal_lhc">00:00:00</span>
		</div>
		<div id="markSixPlayGroupsDiv" class="gamename"></div>
		<script type="text/javascript">
			$(function(){
				$._ajax({url: stationUrl + "/v2/getPlayGroups.do", data: {lotCode: "${lotCode}"}, async: !0}, function (status, message, data, result) {
					var tpl = $("#markSixPlayGroupsTemplate").html();
					$("#markSixPlayGroupsDiv").html(_.template(tpl)({list: data}));

					//设置LHC主选单的显示隐藏事件
					$("#markSixPlayGroupsDiv label").each(function() {
						$(this).click(function() {
							$(this).addClass('selected').siblings('label').removeClass('selected');
							
							var groupid = $(this).data("groupid");
							var groupcode = $(this).data("groupcode");
							$._ajax({url: stationUrl + "/lottery/getPlays.do", async: !0, data: {groupId: groupid}}, function (status, message, data, result) {
								var tpl = $("#markSixPlayTemplate").html();
								$("#playButtonDiv").html(_.template(tpl)({list: data}));

								$("#playButtonDiv button").unbind();
								$("#playButtonDiv button").each(function(){
									var $this = $(this);
									$this.click(function(){
										var playcode = $this.data("playcode");
										var lottype = $this.data("lottype");
										var minBonusOdds = $this.data("minbonusodds");
										var maxBonusOdds = $this.data("maxbonusodds");
										var detaildesc = decodeURIComponent(escape(atob($this.data("detaildesc"))));
										var winexample = $this.data("winexample");

										$("#playDetailDesc").html(detaildesc);
										$("#playWinExample").html(winexample);

										var resurce = $.getStrResouce(stationUrl + "/v2/template/" + lottype + "/" + groupcode + "/" + playcode + ".html?v=3.21");

										getGamesOdds(playcode, groupcode, lottype, resurce, minBonusOdds, maxBonusOdds);
										KuaiJieShuRu();

										$this.addClass('active').siblings('button').removeClass('active');
										$(".Content_TeMa input[type=text]").val('');//清空文本框数据
										$(".Content_TeMa .betmoney").val('');//清空金额
										$(".qingkong").click();
										if(((("zuxuansan" == groupcode || "zuxuanliu" == groupcode) && (lottype == 9 || lottype == 15))) || 
												(("lianma" == groupcode) && lottype == 12)){
											$(".kuaijie").hide();
										} else {
											$(".kuaijie").show();
										}
									});
								});
								$("#playButtonDiv button:eq(0)").click();
							});
						});
					});
					$("#markSixPlayGroupsDiv label:eq(0)").click();//第一个点击
				});
			});
		</script>
		<script id="markSixPlayGroupsTemplate" type="text/html" style="display: none;">
				{#
				_.map(list, function(item, index){
				if(!((item.code == "renxuan" && item.lotType == 14) || (item.code == "zuxuan" && item.lotType == 14) ||  (item.code == "zuxuanfs" && item.lotType == 14) || (item.code == "zhixuan" && item.lotType == 14))){
				#}
				<label class="{{index == 0 ? 'selected' : ''}}" data-groupid="{{item.id }}" data-groupcode="{{item.code}}">{{item.name}}</label>
				{#
				}
				});
				#}
		</script>
		<script id="markSixPlayTemplate" type="text/html" style="display: none;">
				{#
				_.map(list, function(item, index){
				#}
				<button type="button" class="btn btn-default {{index == 0 ? 'active' : ''}}" data-playcode="{{item.code}}" data-lottype="{{item.lotType}}" data-minbonusodds="{{item.minBonusOdds}}" data-maxbonusodds="{{item.maxBonusOdds}}" data-detaildesc="{{btoa(unescape(encodeURIComponent(item.detailDesc)))}}" data-winexample="{{item.winExample}}">{{item.name}}</button>
				{#
				});
				#}
		</script>

		<div class="fengpan hide">封盘中</div>
		<div class="gamename kuaijie">
			<div class="form-group" style="margin-bottom: 5px;">
				<div class="input-group">
					<span class="input-group-addon form_span">快捷金额</span> <input type="text" class="form-control" id="kjmoney" placeholder="输入后点击赔率"> <span class="input-group-addon form_span qingkong">清空</span> <span class="input-group-addon form_span" id="kjbet">下注</span>
				</div>
			</div>
		</div>

		<!-- 页面内容 -->
		<!--特码生肖-->
		<div class="Content_TeMa all_Content">
			<div id="playButtonDiv" class="btn-group result-time-bg btn-group-sm"></div>
			<div style="background-color: #F7FBFD;">
				<button class="btn btn-default btn-sm pull-right" data-toggle="modal" onclick="showPlayDesc();">玩法说明</button>
				<button type="button" class="btn btn-default btn-sm pull-right"  data-toggle="modal" style="margin-right:10px;" onclick="showToday();">今日输赢</button>
				<div style="clear: both"></div>
			</div>
			<div class="ball-content">
			</div>
		</div>

		<div class="modal fade" id="playDescModal" tabindex="-1" role="dialog" aria-labelledby="playDescModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="playDescModalLabel">玩法说明</h4>
					</div>
					<div class="modal-body">
						<div><span><b>玩法介绍</b></span></div>
						<div id="playDetailDesc"></div>
						<div id="playWinExample"></div>
						<div style="margin: 20px 0px"></div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" value="" id="Credit" />
	</div>
	<script>
		var currqh = ""; 
		$(function() {
			//初始化：取开奖结果
			execGetLastResult();
		});

		function newFixed(num, len) {
			var add = 0;
			var s, temp;
			var s1 = num + "";
			var start = s1.indexOf(".");
			if (s1.substr(start + len + 1, 1) >= 5)
				add = 1;
			var temp = Math.pow(10, len);
			s = Math.floor(num * temp) + add;
			return s / temp;
		}

		//上一期开奖结果
		function GetLastResult() {
			$.ajax({
				 type: 'POST',
				url : stationUrl + '/v3/last_data.do',
				data: {
					lotCode: "${lotCode}",
					qiHao : currqh
				},
				success : function(data) {
					console.log(data);
					if (!data.success) {
						return;
					}
					var result = data.last;
					$("#lastResultTerm").html(result.qiHao);
					var haoma = result.haoMa;
					var haomaArr = haoma.split(",");
					var $lastResultHaoma = $("#lastResultHaoma");
					$lastResultHaoma.empty();
					for(var i = 0; i < haomaArr.length; i++){
						var item = haomaArr[i];
						$lastResultHaoma.append('<span id="d_ball' + (i + 1) +'">' + item + '</span> ');
					}
				}
			});
		}

		function execGetLastResult() {
			GetGameOpenInfo();

			setInterval("GetLastResult()", 1000 * 20);
			//开盘信息
			setInterval("GetGameOpenInfo()", 1000 * 20);
		}

		//取开盘信息
		function GetGameOpenInfo() {
			$.ajax({
				url : '${station }/v2/getGameOpenInfo.do',
				data: {
					lotCode: "${lotCode}"
				},
				async : true,
				error : function(result) {
				},
				success : function(result) {
					if (!result.success) {
						Modal.alert({
							msg : result.msg
						}).on(function() {
							// location.href = location.href;
							// location.href = stationUrl + "/v2/index.jsp";
							location.href = stationUrl + "/index.do";
						});
						return;
					} else {
						var Current_Term = result.data.dropDate;
						currqh = result.data.dropDate-1;
						var CloseTime = result.data.closeTime;
						var IsOpen = result.data.isOpen;
						if (IsOpen) {
							$(".fengpan").addClass("hide");
							CloseTime = CloseTime.substring(0, CloseTime.length - 3);
							$(".fengpantime").html(CloseTime);//封盘时间
							$(".memRound").html(Current_Term);//新的开奖期数
							GetLastResult();
						} else {
							$(".fengpan").removeClass("hide");
							$("input[type=text]").val('');
							$("input[type=checkbox]").removeAttr("checked");
							$(".qingkong").click();
						}
					}
				}
			});
		}

		//提交下注按钮点击事件
		function submintClick(lotType, groupCode, playCode, playName) {
			var inputVal = $(".ball-content input[type=text]");
			var validate = "";
			var allje = 0;
			var con;
			inputVal.each(function () {
				var je = $(this).val();
				if (je != "") {
					var ex = /^[1-9]\d*$/
					if (!ex.test(je)) {
						validate = "金额无效！";
						con = $(this);
						return false;
					} else {
						allje += je * 1;
					}
				}
			});
			var Credit = $("#Credit").val() * 1;
			if (validate != "") {
				Modal.alert({ msg: validate }).on(function () {
					$(con).focus();
					return;
				});
				return;
			} else if (allje == 0) {
				Modal.alert({ msg: '请先下注!' }).on(function () {
					return;
				});
				return;
			}
			var qishu = $(".memRound").html();
			var betData = {"qiHao": qishu, "lotCode": "${lotCode}", "groupCode": groupCode, "playCode": playCode, "lotType": lotType};
			var data = [];
			var zhucount = 0;
			var htmls = "";
			inputVal.each(function () {
				var je = $(this).val();
				if (je != "") {
					zhucount++;
					var name = $(this).attr("name");
					var rate = $(this).attr("rate");
					var oddsId = $(this).attr("oddsId");
					if (!rate || rate == 0) {
						Modal.alert({ msg: '赔率0 ,无法下注!' }).on(function () {
							return false;
						});
					}
					var betprefix = $(this).parents("table").attr("betprefix");
					var pname = betprefix ? betprefix + "--" + name : name;
					data.push({"name": pname, "money": je, "oddsId": oddsId, "rate": rate});
					htmls += "<tr><td>" + playName + "</td><td>" + pname + "</td><td>" + je + "</td></tr>";
				}
			});
			if (data.length > 0) {
				betData.data = data;
				$("#myModal1 .counts").html(zhucount);
				$("#myModal1 .jinge").html(allje);
				$("#betDetail").html(htmls);
				$("#myModal1 .tijiao").attr("onclick", "submitBetGame('" + JSON.stringify(betData) + "')");
				$(".ball-content input[type=text]").val('');
				$("#myModal1").modal("show");
				$(".qingkong").click();
			}
		}

		//取赔率的方法
		function getGamesOdds(playCode, groupcode, lotType, template, minBonusOdds, maxBonusOdds) {
			var data = {
				"playCode" : playCode,
				"lotType": lotType
			};
			$('#myModal').modal('show');
			$.ajax({
				type : "post",
				url : stationUrl + "/v2/getOdds.do",
				data : data,
				async : false,
				error : function(dt) {
				},
				success : function(res) {
					if(res.success){
						var list = res.data ? res.data : [];
						if(((playCode == "erzi_qiansan" || playCode == "erzi_zhongsan" || playCode == "erzi_housan" || 
								playCode == "sanzi_qiansan" || playCode == "sanzi_zhongsan" || playCode == "sanzi_housan" || 
								groupcode == "erzidingwei" || groupcode == "sanzidingwei" || 
								groupcode == "zuxuansan" || groupcode == "zuxuanliu") && lotType == 9) || 
									(("lianma" == groupcode) && lotType == 12) || (("sanzizuhe" == groupcode || "sanzidingwei" == groupcode || "erzidingwei" == groupcode || groupcode == 'zuxuanliu' || groupcode == 'zuxuansan') && lotType == 15)){
							$(".ball-content").html(_.template(template)({list: list, playCode: playCode, minBonusOdds: minBonusOdds, maxBonusOdds: maxBonusOdds, lotCode: "${lotCode}"}));

						} else {
							$(".ball-content").html(template);
							for(var i = 0; i < list.length; i++){
								var item = list[i];
								var $inputa = $(".ball-content input[rakeBack=" + item.rakeBack + "]");
								$inputa.parent().prev().html(item.odds);
								$inputa.attr({rate: item.odds, oddsId: item.id});
							}
						}
						$("input[type=text]").onlyNum();//所有文本框只能输入数字
						$("input[type=reset]").click(function () { $(".qingkong").click(); });
					} else {
						Modal.alert({
							msg : res.msg
						});
					}
				},
				complete : function(data) {
					var r = $("[rate='0.0']");
					r.parent().hide();
					r.parent().prev().hide();
					r.parent().prev().prev().hide();
					$('#myModal').modal('hide');
				}
			});
		}

		function getZhushu(minSelected, nowChecked){
			var a=1,b=1;
			if(minSelected == 0 || nowChecked == 0){return 1;}
			for(var i = 0; i < minSelected; i++){
				a = a * (minSelected - i);
			}
			for(var i = nowChecked; i > nowChecked - minSelected; i--){
				b = b * i;
			}
			return b / a;
		}
	</script>

	<script src="${station }/lhc/scripts/bootstrap.min.js"></script>
	<script src="${station }/lhc/scripts/bootstrap_alert.js"></script>
	<!-- system modal start弹窗html -->
	<div id="ycf-alert" class="modal">
		<div class="modal-dialog modal-sm linecentermodel" style="width: 250px; height: 108px;">
			<div class="modal-content">
				<div class="modal-body bg-info" style="border-radius: 5px; padding: 10px;">
					<p style="font-size: 15px; font-weight: bold;">[Message]</p>
				</div>
				<div class="modal-footer" style="padding: 5px; text-align: center;">
					<button type="button" class="btn btn-primary ok" data-dismiss="modal">[BtnOk]</button>
					<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<img class="center-block linecentermodel" width="40" height="40" src="${station }/lhc/imgs/load.gif" />
	</div>

	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="margin: 35% auto 0; width: 90%">
			<div class="modal-content">
				<div class="modal-header bg-info"style="max-height:220px;overflow:auto;padding: 10px; border-radius: 5px;">
					<h5 class="modal-title">
						下注明细 <span class="badge pull-right text-white counts">0</span>
					</h5>
				</div>
				<div class="modal-body" style="padding: 5px;">
					<table class="table table-striped">
						<thead>
							<tr class="bg-warning">
								<td>类型</td>
								<td>号码</td>
								<td>金额</td>
							</tr>
						</thead>
						<tbody id="betDetail"></tbody>
					</table>
				</div>
				<div class="modal-footer" style="padding: 6px;">
					<span class="text-primary">金额:<span class="jinge ">0.0</span>&nbsp;&nbsp;&nbsp;
					</span>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary tijiao">确认</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<script type="text/javascript">
	function showToday(){
		var isLogin = '${isLogin}';
		if(!isLogin || isLogin == 'false'){
			return;
		}
		$('#myModal').modal('show');
		$.ajax({
			type : "post",
			url : '${station }/v2/winOrLost.do',
			success : function(j) {
				if(j.success){
					$('#myModal').modal('hide');
					$('#allWinAmount').html(j.allWinAmount);
					$('#yingkuiAmount').html(j.yingkuiAmount);
					$('#allBetAmount').html(j.allBetAmount);
					$("#modalWinOrLost").modal("show");
				}
			}
		});
	}
	
	function showPlayDesc(){
		$('#playDescModal').modal({
//					keyboard: true
		});
	}
	</script>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="modalWinOrLost" tabindex="-1" role="dialog" aria-labelledby="modalWinOrLostLabel" aria-hidden="true">
		<div class="modal-dialog" style="margin: 35% auto 0; width: 90%">
			<div class="modal-content">
				<div class="modal-header bg-info" style="padding: 10px; border-radius: 5px;">
					<h5 class="modal-title">
						<span class="glyphicon glyphicon-pushpin" style="font-size:17px;font-weight:bold;">今日输赢</span>
						<span class="pull-right" style="font-size:17px;font-weight:bold;">余额：<font color="red">${loginMember.money}</font>元</span>
					</h5>
				</div>
				<div class="modal-body" style="padding: 5px;">
					<table class="table table-striped">
						<thead>
							<tr class="bg-warning">
								<th>今日消费</td>
								<th>今日中奖</td>
								<th>今日盈亏</td>
							</tr>
						</thead>
						<tbody id="winOrLostDetail">
							<tr>
								<td style="font-size:17px;font-weight:bold;"><font color="red" id="allBetAmount">0.00</font>元</td>
								<td style="font-size:17px;font-weight:bold;"><font color="red" id="allWinAmount">0.00</font>元</td>
								<td style="font-size:17px;font-weight:bold;"><font color="red" id="yingkuiAmount">0.00</font>元</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer" style="padding: 6px;">
					<span class="text-primary pull-left" style="line-height:33px;">
					<a href="${base }/mobile/pay_select_way.do">充值</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="${base }/mobile/withdraw_money.do">提款</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="${base }/mobile/betting_record_lottery.do">投注记录</a>
					</span>
					<button type="button" class="btn btn-default" data-dismiss="modal">知道了</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<!--system modal END弹窗html-->
	<script>
		//此方法解决JS的sort()排序方法在IE9/safari兼容性问题
		!function(window) {
			var ua = window.navigator.userAgent.toLowerCase(), reg = /msie|applewebkit.+safari/;
			if (reg.test(ua)) {
				var _sort = Array.prototype.sort;
				Array.prototype.sort = function(fn) {
					if (!!fn && typeof fn === 'function') {
						if (this.length < 2)
							return this;
						var i = 0, j = i + 1, l = this.length, tmp, r = false, t = 0;
						for (; i < l; i++) {
							for (j = i + 1; j < l; j++) {
								t = fn.call(this, this[i], this[j]);
								r = (typeof t === 'number' ? t : !!t ? 1 : 0) > 0 ? true
										: false;
								if (r) {
									tmp = this[i];
									this[i] = this[j];
									this[j] = tmp;
								}
							}
						}
						return this;
					} else {
						return _sort.call(this);
					}
				};
			}
		}(window);

		var tt;
		function caizhongxz() {
			$("#caizhong").slideToggle(500);
			clearTimeout(tt);
			if ($("#caizhong").is(":visible")) {
				tt = setTimeout(function() {
					$("#caizhong").slideUp(500);
				}, 1000 * 25);
			}
		}

		var SysSecond_lhc = 0;
		$(function() {
			QingKong();
			$("#kjmoney").attr("oninput", "kjmoneyChange()");
			$("#kjbet").click(function(){ $("input[type=button][value=确认下注]").click(); });

			setInterval("SetRemainTime()", 1000);
			if ($("#caizhong").is(":visible")) {
				tt = setTimeout(function() {
					$("#caizhong").slideUp(500);
				}, 1000 * 25);
			}
		});
		
		//余额实时更新
		var isLogin = '${isLogin}';
		if(isLogin == 'true'){
			var balanceTimer = function(){
				var t1;
				function balance() {
					$.ajax({
						url :  "${base}/meminfo.do",
						success : function(j) {
							if(j.login){//如果已登录状态
								$("#Credit").val(fmoney(j.money,2)).attr("Credit",fmoney(j.money,2));
								$("#createshow").html(fmoney(j.money,2)).attr("createshow",fmoney(j.money,2));
							}
							
						}
					});
				}
				//格式化成两位小数
				function fmoney(s, n) { 
					n = n > 0 && n <= 20 ? n : 2; 
					s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
					var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
					t = ""; 
					for (i = 0; i < l.length; i++) { 
					t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "" : ""); 
					} 
					return t.split("").reverse().join("") + "." + r; 
					} 
				
				return {
					start : function() {
						var a = this;
						clearTimeout(t1);
						balance();
						t1 = setTimeout(function(){a.start()},Math.floor(Math.random() * 1000 + 1) + 5000);
					},
					stop  : function() {
						clearTimeout(t1);
					}
				};
			}();
			$(function(){
				balanceTimer.start();
			})
		}
		
		//取账户额度
// 		function GetCredit() {
// 			if ($("#Credit").length > 0) {
// 				$._ajax({url: stationUrl + "/member/meminfo.do", async: !1}, function (status, message, data, result) {
// 					userMoney = data.money;
// 					$("#Credit").val(userMoney);
// 					$("#createshow").html(userMoney);
// 				});
// 			}
// 		}

		//将时间减去1秒，计算天、时、分、秒
		function SetRemainTime() {
			if (SysSecond_lhc > 0) {
				var second = Math.floor(SysSecond_lhc % 60); // 计算秒
				var minite = Math.floor((SysSecond_lhc / 60) % 60); //计算分
				var hours = Math.floor(SysSecond_lhc / 60 / 60);
				if (second < 10) {
					second = "0" + second
				}
				if (minite < 10) {
					minite = "0" + minite
				}
				SysSecond_lhc = SysSecond_lhc - 1;
				$(".reciprocal_lhc").html(hours + ":" + minite + ":" + second);
			} else {
				$(".reciprocal_lhc").html("暂停下注");
				GetSingleGameOpen();
			}
		}

		function GetSingleGameOpen() {
			$.ajax({
				type : "post",
				url : '${station }/v2/getGameOpenInfo.do',
				data: {
					lotCode: "${lotCode}"
				},
				async : true,
				error : function(result) {
				},
				success : function(result) {
					if (!result.success) {
						return;
					}
					SysSecond_lhc = result.data.closeSecond;
				}
			});
		}

		//快捷输入
		function kjmoneyChange() {
			$("td.inputed").next("td").children("input[type=text]").val($("#kjmoney").val());
		}

		//快捷输入
		function KuaiJieShuRu() {
			$("td.text-red").each(function() {
				$(this).click(function() {
					$(this).prev("td").toggleClass("inputed");
					$(this).toggleClass("inputed");
					$(this).next("td").toggleClass("inputed");
					var kj = $("#kjmoney").val();
					if ($(this).attr("class").indexOf("inputed") > -1) {
						$(this).next("td").children("input[type=text]").val(kj);
					} else {
						$(this).next("td").children("input[type=text]").val('');
					}
				});
			});
			$("td.text-red + td").each(function() {
				$(this).click(function() {
					$(this).prev("td.text-red").click();
				});
			});
			$("td.text-red").prev("td").each(function() {
				$(this).click(function() {
					$(this).next("td.text-red").click();
				});
			});
		}

		//清空文本框
		function QingKong() {
			$(".qingkong").click(function() {
				$("input[type=text]").val('');
				$("td.inputed").removeClass("inputed");
				$("td input[type=checkbox]").removeAttr("checked");
			});
		}

		//提交下注
		function submitBetGame(params) {
			if($('#myModal1').attr('bet')){
				return;
			}
			$('#myModal1').attr('bet','bet');
			$("#myModal1").modal("hide");
			var data = JSON.parse(params);
			$('#myModal').modal('show');
			$.ajax({
				type : "post",
				url : baseUrl + "/lotteryV2Bet/dolotV2Bet.do",
				data : {
					data: JSON.stringify(data.data),
					lotCode: data.lotCode,
					groupCode: data.groupCode,
					playCode: data.playCode,
					lotType: data.lotType,
					qiHao: data.qiHao,
					stId: "${stationId}"
				},
				async : true,
				error : function() {
					Modal.alert({
						msg : '网络连接超时,请重试!'
					}).on(function(e) {
						return;
					});
				},
				success : function(data) {
					if (!data.success) {
						var tips = '请先登录!',needLogin=true;
						if(data.msg != '登陆超时,请重新登陆系统'){
							tips = data.msg;
							needLogin=false;
						}
						Modal.alert({
							msg : tips
						}).on(function(e) {
							if(needLogin){
								location.href = '${base}/mobile/login.do';
							}
							return;
						});
					} else {
						Modal.alert({
							msg : "您的注单已成功提交下注，请至下注记录查看!"
						}).on(function(e) {
// 							GetCredit();//取账户额度
							return;
						});
					}
				},
				complete : function(data) {
					$('#myModal').modal('hide');
					$('#myModal1').removeAttr('bet');
				}
			});
		}
	</script>
	<style>
		#myModal1 .modal-footer .btn{
			width: 20%;
		}
		.modal{
			position: absolute;
		}
		#ycf-alert{
			position : fixed;
		}
	</style>
</body>
</html>
<%@include file="../include/check_login.jsp"%>