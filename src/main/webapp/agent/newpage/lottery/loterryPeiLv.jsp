<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="lottery_pei_lv_con_wrap_id">
	<ul class="nav nav-tabs czGroup mb10px">
		<li class="active"><a href="#" data-toggle="tab" class="lotName" lid="8">pk10</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="9">时时彩</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="10">快三</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="11">PC蛋蛋</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="12">快乐十分</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="14">11选5</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="15">排列三/福彩3D</a></li>
     	<c:if test="${version == 2 && onoffSystemLoterryV2Peilv eq 'on'}">
     	<li><a href="#" data-toggle="tab" class="lotName" lid="929">系统时时彩</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="828">系统赛车</a></li>
     	<li><a href="#" data-toggle="tab" class="lotName" lid="102">系统快三</a></li>
     	</c:if>
	</ul>
	<div class="form-group">
		<ul class="nav nav-pills playTab mb10px"></ul>
		<ul class="nav nav-pills playSmallTab mb10px"></ul>
		<button class="btn btn-primary pull-left open-dialog" url="${base }/agent/lotPeiLv/ruleSetting.do?id={[name='platTypeId']:}">玩法规则设置</button>
		<button class="btn btn-danger pull-left open-dialog" url="${base }/agent/lotPeiLv/minSetting.do?playCode={[name='playCode']:}&lotType={[name='lotType']:}" style="margin-left: 10px;">最小金额批量设置</button>
		<button class="btn btn-warning pull-left open-dialog" url="${base }/agent/lotPeiLv/maxSetting.do?playCode={[name='playCode']:}&lotType={[name='lotType']:}" style="margin-left: 10px;">最大金额批量设置</button>
		<button class="btn btn-success pull-left lottery_pei_lv_save_all" style="margin-left: 20px;">全部保存</button>
	</div>
	<form class="fui-search table-tool" method="post">
		<input name="platTypeId" type="hidden"/>
		<input name="playCode" type="hidden"/>
		<input name="lotType" type="hidden" value="8"/>
	</form>
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/lottery/lotteryPeiLv.js?v=1.2'],function($lotteryPeiLv){
	$lotteryPeiLv.render("lottery_pei_lv_con_wrap_id");
});
</script>