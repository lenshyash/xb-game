<%@ page language="java" pageEncoding="UTF-8"%>
<div id="mark_six_pei_lv_con_wrap_id">
	<ul class="nav nav-tabs mb10px">
		<li style="margin-top: 7px;margin-right: 10px;">
			<select class="form-control" id="lottery_xgc">
			</select>
		</li>
		<li class="active"><a href="#" data-toggle="tab" class="lotName" oname="peilv">赔率设置</a></li>
		<li><a href="#" data-toggle="tab" class="lotName" oname="dalei">玩法大类设置</a></li>
		<li><a href="#" data-toggle="tab" class="lotName" oname="xiaolei">玩法小类设置</a></li>
	</ul>
	<div class="form-group play-info-wap">
		<ul class="nav nav-pills playTab mb10px"></ul>
		<ul class="nav nav-pills playSmallTab mb10px"></ul>
		<button class="btn btn-danger pull-left open-dialog" url="${base }/agent/cp/markSix/minSetting.do?playCode={[name='playCode']:}&lotType={#lottery_xgc:6}">最小金额批量设置</button>
		<button class="btn btn-warning pull-left open-dialog" url="${base }/agent/cp/markSix/maxSetting.do?playCode={[name='playCode']:}&lotType={#lottery_xgc:6}" style="margin-left: 10px;">最大金额批量设置</button>
		<button class="btn btn-success pull-left lottery_pei_lv_save_all" style="margin-left: 20px;">全部保存</button>
	</div>
	<form class="fui-search table-tool" method="post">
		<input name="playCode" type="hidden"/>
		<input type="hidden" name="groupId">
	</form>
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/lottery/markSixManage.js?v=1.0.7'],function($msm){
	$.ajax({
		url:'${base}/agent/cp/lottery/list.do',
		data:{page:1,rows:100},
		success:function(res){
			var html = '';
			if(res.total>0){
				$.each(res.rows,function(i,j){
					if(j.type == 6 || j.type == 66){
						html += '<option value="'+j.type+'">'+j.name+'</option>';
					}
				})
				$("#lottery_xgc").html(html)
				$msm.render();
			}
		}
	});
});
</script>