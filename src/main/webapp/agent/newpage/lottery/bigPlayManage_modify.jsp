<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotTypeGroup/update.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" value="${playGroup.id }" name="id">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">玩法大类修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">玩法分组名称：</td>
						<td><input type="text" name="name" value="${playGroup.name }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td><input name="sortNo" class="form-control" value="${playGroup.sortNo }"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>