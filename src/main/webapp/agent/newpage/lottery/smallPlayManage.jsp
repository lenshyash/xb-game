<%@ page language="java" pageEncoding="UTF-8"%>
<ul class="nav nav-tabs mb10px" id="lottery_small_play_list_czGroup_id"></ul>
<form class="fui-search table-tool" method="post" id="lottery_small_play_list_form_id">
	<div class="form-inline">
		<div class="form-group">
			<div class="input-group">
				<input type="hidden" name="groupId">
				<ul class="nav nav-pills smallTab"></ul>
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script>var replaceFont = '${replaceFont}';</script>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#lottery_small_play_list_form_id"),curLotType;

	(function getBigGroup(){
		$.ajax({
			url : "${base}/agent/cp/lotTypeGroup/getBigGroup.do",
			success : function(j) {
				var col = '';
				for(var i in j){
					if(i==0){
 						col+='<li class="active">';
 						getBigType(j[i]);
					}else{
						col+='<li>';
					}
					
					col+='<a href="#" data-toggle="tab" class="lotName" lottype="'+j[i]+'">'+getLotteryTypeNameA(j[i],replaceFont)+'</a></li>';
				}
				$("#lottery_small_play_list_czGroup_id").html(col).find('.lotName').click(function(){
					getBigType($(this).attr("lottype"));
				});
			}
		});
	})();
	function getBigType(lotType){
		curLotType=lotType;
		$.ajax({
			url : "${base}/agent/cp/lotType/bigList.do?lotType="+lotType,
			type : "get",
			dataType : 'json',
			success : function(r) {
				if(r==null || r==""){
					$form.find("[name='groupId']").val("");
					$form.find(".smallTab").html('');
				}else{
					var html="",active='';
					for(var i=0;i<r.length;i++){
						if(i==0){
							$form.find("[name='groupId']").val(r[0].id);
							active=" active";
						}else{
							active="";
						}
						html+='<li class="smallType'+active+'"><a href="#" class="lotGroupId" groupid="'+r[i].id+'">'+r[i].name+'</a></li>';
					}
					$form.find(".smallTab").html(html);
					addTable();
					$form.find('.lotGroupId').click(function(){
						$form.find('.smallType').removeClass("active");
						$(this).parent().addClass("active");
						$form.find("[name='groupId']").val($(this).attr("groupid"));
						if(curTable){
							curTable.bootstrapTable('refresh');
						}
					});
				}
			}
		});
	}
	var curTable=null;
	function getColumns(){
		var columns=[{
			field : 'name',
			title : '玩法小类名称',
			align : 'center',
			width : '200',
			valign : 'middle'
		}];
		if("${v3}"=="on"&& curLotType>=50 && curLotType<100){
			columns.push({
				field : 'maxBonusOdds',
				title : '最大中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'bottom',
				formatter : maxMInputFormatter
			});
		}
		if(curLotType<8 || curLotType>50){
			columns.push({
				field : 'minBonusOdds',
				title : '最小中奖金额(元)',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : minMInputFormatter
			},{
				field : 'maxNumber',
				title : '最高注数',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : maxNInputFormatter
			});
		}
		columns.push({
			field : 'sortNo',
			title : '排序',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : sortNoInputFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		});
		return columns;
	}
	function addTable(){
		if(curTable){
			curTable.bootstrapTable('refreshOptions', {
				columns : getColumns(),
				pageNumber : 1
			});
			return;
		}
		Fui.addBootstrapTable({
			url : '${base}/agent/cp/lotType/list.do',
			columns :getColumns(),
			onLoadSuccess:function($table){
				curTable=$table;
				$table.find(".doSave").click(function(){
					var $it=$(this),$tr=$it.parents("tr"),maxBonusOdds=null;
					if("${v3}"=="on"&& curLotType>=50 && curLotType<100){
						maxBonusOdds = $tr.find("[name='maxBonusOdds']").val();
						if(!/^[\d]+(\.[\d]{1,2})?$/.test(maxBonusOdds)){
							layer.msg("最大中奖金额格式错误，最多只能包含2位小数点");
							return;
						}
					}
					var blp = {
						'id' : $it.attr("rowid"),
						'maxBonusOdds' : maxBonusOdds,
						'minBonusOdds' : $tr.find("[name='minBonusOdds']").val(),
						'maxNumber' :  $tr.find("[name='maxNumber']").val(),
						'sortNo' :  $tr.find("[name='sortNo']").val()
					};
					if($tr.find("[name='minBonusOdds']").length&&(!blp.minBonusOdds || !/^[\d]+(\.[\d]{1,2})?$/.test(blp.minBonusOdds))){
						layer.msg("最小中奖金额格式错误，最多只能包含2位小数点");
						return;
					}
					$.ajax({
						url : "${base}/agent/cp/lotType/update.do",
						data : blp,
						success : function(result){
							layer.msg(result.msg||"保存成功");
						}
					})
				});
			}
		});
	}
	function maxMInputFormatter(value, row, index){
		return '<input name="maxBonusOdds" class="form-control" type="text" value="'+Fui.toDecimal(value,2)+'"/>';
	}
	function minMInputFormatter(value, row, index){
		return '<input name="minBonusOdds" class="form-control" type="number" value="'+Fui.toDecimal(value,2)+'"/>';
	}
	function maxNInputFormatter(value, row, index){
		return '<input name="maxNumber" class="form-control" type="number" value="'+(value||0)+'"/>';
	}
	function sortNoInputFormatter(value, row, index){
		return '<input  name="sortNo" class="form-control" type="number" value="'+(value||0)+'"/>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotType/updateStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="doSave" href="#" title="保存" rowid="'+row.id+'"><i class="glyphicon glyphicon-ok"></i></a>   '
				+'<a class="open-dialog" href="${base}/agent/cp/lotType/modify.do?id='+row.id+'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	function getLotteryTypeNameA(value,replaceFont) {
		var cn = '';
		switch (value-0) {
		case 2:
		case 9:
		case 52:
			cn = '时时彩';
			break;
		case 3:
		case 8:
		case 53:
		case 158:
			cn = '北京赛车(快开)';
			break;
		case 4:
		case 54:
		case 15:
			cn = '排列三/福彩3D';
			break;
		case 5:
		case 55:
		case 14:
			cn = '11选5';
			break;
		case 6:
			cn = '香港彩';
			break;
		case 7:
		case 11:
		case 57:
		case 161:
			cn = 'PC蛋蛋';
			break;
		case 10:
		case 58:
		case 100:
			cn = '快3';
			break;
		case 12:
			cn = '快乐十分';
			break;
		case 1:
		case 51:
			cn = '系统彩';
			if(replaceFont.length){
				cn = replaceFont.toString();
			}
			break;
		case 66:
			cn = '十分六合彩';
			break;
		case 929:
			cn = '系统时时彩';
			break;
		case 828:
			cn = '系统赛车';
			break;
		case 102:
			cn = '系统快3';
			break;
		}
		return cn;
	}
});	
</script>