<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotRoom/saveRoom.do" class="form-submit" id="lottery_room_formId">
<input type="hidden" value="${room.id}" name="id"/>
<input type="hidden" value="${room.lotType}" name="lotType">
<input type="hidden" value="${room.stationId}" name="stationId"/>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加房间（<span style="color:red;">*号为必填选项</span>）</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">*房间名称：</td>
						<td class="text-left" width="30%"><input type="text" name="name" value="${room.name}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">房间标识：</td>
						<td class="text-left" width="30%"><input type="text" name="roomHouse" value="${room.roomHouse}" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle" width="20%">房间简介：</td>
						<td class="text-left" colspan="3">
							<script name="brief" id="agent_room_add_editor" type="text/plain" style="width:100%;height:100px;"></script>
						</td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">*初始人数：</td>
						<td class="text-left" width="30%"><input type="text" name="initialCount" value="${room.initialCount}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">*最大人数：</td>
						<td class="text-left" width="30%"><input type="text" name="maxCount" value="${room.maxCount}" class="form-control"/></td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">*所属彩种：</td>
						<td class="text-left" width="30%">
							<select name="lotCode" class="form-control"><option value>请选择彩种</option>
								<c:forEach items="${blList}" var="bl">
									<option value="${bl.code}" ${bl.code eq room.lotCode?'selected':''}>${bl.name}</option>
								</c:forEach>
							</select>
						</td>
						<td width="20%" class="text-right media-middle">房间图片：</td>
						<td class="text-left" width="30%"><input type="text" name="roomImg" value="${room.roomImg}" class="form-control"/></td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">*投注状态：</td>
						<td class="text-left" width="30%">
							<select name="betStatus" class="form-control">
								<option value="2" ${room.betStatus eq 2?'selected':''}>开启</option>
								<option value="1" ${room.betStatus eq 1?'selected':''}>关闭</option>
							</select>
						</td>
						<td width="20%" class="text-right media-middle">*机器人状态：</td>
						<td class="text-left" width="30%">
							<select name="robotStatus" class="form-control">
								<option value="2" ${room.robotStatus eq 2?'selected':''}>开启</option>
								<option value="1" ${room.robotStatus eq 1?'selected':''}>关闭</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">进入房间条件（金额）：</td>
						<td class="text-left" width="30%"><input type="text" name="maxMoney" value="${room.maxMoney}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">进入房间条件（等级）：</td>
						<td class="text-left" width="30%">
							<select name="levelId" class="form-control">
								<c:forEach items="${levelList}" var="level">
									<option value="${level.id}" ${room.levelId eq level.id?'selected':''}>${level.levelName}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">发言条件（金额）：</td>
						<td class="text-left" width="30%"><input type="text" name="speakMoney" value="${room.speakMoney}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">*房间状态：</td>
						<td class="text-left" width="30%">
							<select name="status" class="form-control">
								<option value="2" ${room.status eq 2?'selected':''}>启用</option>
								<option value="1" ${room.status eq 1?'selected':''}>禁用</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">暗语：</td>
						<td class="text-left" width="30%"><input type="text" name="argot" value="${room.argot}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">*排序：</td>
						<td class="text-left" width="30%"><input type="text" name="sortNO" value="${room.sortNO}" class="form-control"/></td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">*机器人数：</td>
						<td class="text-left" width="30%"><input type="text" name="robot" value="${room.robot}" class="form-control"/></td>
						<td width="20%" class="text-right media-middle">房间管理员：</td>
						<td class="text-left" width="30%"><input type="text" name="roomAdmin" value="${room.roomAdmin}" class="form-control"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
<c:if test="${not empty room.id}">
<div class="hidden edit_content">${room.brief}</div></c:if>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#lottery_room_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_room_add_editor');
	})
	var ue = UE.getEditor('agent_room_add_editor');
	<c:if test="${not empty room.id}">
	ue.ready( function( editor ) {
		ue.setContent($form.find(".edit_content").html());
		$form.find(".edit_content").remove();
	});
	</c:if>
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.brief=ue.getContent();
		//params.modelStatus=$form.find("[name='status']").val();
		return params;
	});
})
</script>