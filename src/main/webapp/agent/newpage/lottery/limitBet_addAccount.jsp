<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<form action="${base}/agent/cp/lotteryData/addLimitBetAccount.do" class="form-submit" id="lottery_result_manager_add_limit_bet_account_form_id">--%>
<div class="modal-dialog" id="lottery_result_manager_add_limit_bet_account_form_id">
	<input type="hidden" value="${param.id}" name="bcId">
<input type="hidden" name="lotCode">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加限制投注帐号</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">彩种名称：</td>
						<td><input type="text" name="lotName" disabled="disabled" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">期号：</td>
						<td><input name="qiHao" class="form-control"disabled="disabled" value="${param.qiHao}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle" style="color: #286090;">限制会员帐号：</td>
						<td> <textarea class="form-control" rows="3" placeholder="限制多个会员禁止投注则使用英文逗号(,)分隔" name="limitBetAccount">${limitBetAccount}</textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle" style="color: #f0ad4e;">取消限制帐号：</td>
						<td> <textarea class="form-control" rows="3" placeholder="取消限制多个会员禁止投注则使用英文逗号(,)分隔" name="limitBetAccountCall"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary resultLimit">保存限制</button>
			<button class="btn btn-warning resultLimitCall">取消限制</button>
		</div>
	</div>
</div>
<%--</form>--%>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#lottery_result_manager_add_limit_bet_account_form_id"),
		$pcode=$("#lottery_lottery_result_manage_form_id").find("[name='code']");
	$form.find("[name='lotCode']").val($pcode.val());
	$form.find("[name='lotName']").val($pcode.attr("lotname"));
	$('.resultLimit').click(function () {
		$.ajax({
			url:"${base}/agent/cp/lotteryData/addLimitBetAccount.do",
			data:{bcId:'${param.id}',lotCode:$pcode.val(),limitBetAccount:$form.find("[name='limitBetAccount']").val()},
			success:function (res) {
				if (res.success){
					layer.msg(res.msg || '添加成功');
					$('.fui-close').trigger('click')
				}else {
					layer.msg(res.msg);
				}
			}
		})
	})
	$('.resultLimitCall').click(function () {
		$.ajax({
			url:"${base}/agent/cp/lotteryData/cancelLimitBetAccount.do",
			data:{bcId:'${param.id}',lotCode:$pcode.val(),limitBetAccount:$form.find("[name='limitBetAccountCall']").val()},
			success:function (res) {
				if (res.success){
					$("[name='limitBetAccount']").val(res.cancelAccount)
					layer.msg(res.msg || '取消限制成功');
					$form.find("[name='limitBetAccountCall']").val('')
					<%--$("[name='limitBetAccount']").val(${limitBetAccount})--%>
				}else {
					layer.msg(res.msg);
				}
			}
		})
	})

});
</script>