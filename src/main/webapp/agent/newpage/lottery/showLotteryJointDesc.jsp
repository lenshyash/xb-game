<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style type="text/css">
.modal-body {
	max-height: 520px;
	overflow-x: auto;
}

.table-bordered th, .table-bordered td {
	text-align: center;
}
</style>
<div class="modal-dialog" style="width:1000px;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">合买订单详情</h4>
		</div>
		<div class="modal-body">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseOne">合买订单详情</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td class="text-center" colspan="6">订单号:${joint.programId}</td>
									</tr>
									<tr>
										<td width="13%" class="text-right">账号:</td>
										<td width="20%">${joint.account }</td>
										<td width="13%" class="text-right">彩种|玩法:</td>
										<td width="20%">${joint.lotteryName}</td>
										<td width="13%" class="text-right">下注时间:</td>
										<td width="21%"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
												value="${joint.createTime}" /></td>
									</tr>
									<tr>
										<td class="text-right">封盘时间:</td>
										<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
												value="${joint.endTime}" /></td>
										<td class="text-right">总金额:</td>
										<td><fmt:formatNumber pattern="#,##0.00" value="${joint.totalMoney}" />元</td>
										<td class="text-right">总份数:</td>
										<td>${joint.branchNum}份</td>
									</tr>
									<tr>
										<td class="text-right">提成%:</td>
										<td>${joint.commission}%</td>
										<td class="text-right">公开状态:</td>
										<td>${joint.isOpen eq 1?'完全公开':joint.isOpen eq 2?'截止后公开':'完全保密'}</td>
										<td class="text-right">是否保底:</td>
										<td>${joint.them eq 0?'未保底':joint.them}</td>
									</tr>
									<tr>
										<td class="text-right">期号:</td>
										<td>${joint.qiHao}</td>
										<td class="text-right">中奖金额:</td>
										<td>
											<c:if test="${not empty joint.winTotalMoney}">
												<fmt:formatNumber pattern="#,##0.00" value="${joint.winTotalMoney}" />元
											</c:if>
											<c:if test="${empty joint.winTotalMoney }">0元</c:if>
										</td>
										<td class="text-right">中奖注数:</td>
										<td>${not empty joint.winNum?joint.winNum:'--'}</td>
									</tr>
									<tr>
										<td class="text-right">购买注数:</td>
										<td>${joint.buyNum}</td>
										<td class="text-right">状态:</td>
										<td><c:choose>
												<c:when test="${joint.type==1}">
													<span class="label label-primary">可认购</span>
												</c:when>
												<c:when test="${joint.type==2}">
													<span class="label label-success">满员</span>
												</c:when>
												<c:when test="${joint.type==3}">
													<span class="label label-danger">已截止</span>
												</c:when>
												<c:when test="${joint.type==4}">
													<span class="label label-info">完成</span>
												</c:when>
												<c:when test="${joint.type==5}">
													<span class="label label-success">方案失效</span>
												</c:when>
											</c:choose></td>
										<td class="text-right">盈亏:</td>
										<td><fmt:formatNumber pattern="#,##0.00" value="${joint.winTotalMoney-joint.totalMoney}" />元</td>
									</tr>
									<tr>
										<td class="text-right" style="line-height: 65px;">方案描述:</td>
										<td colspan="5"><textarea class="form-control" rows="3">${joint.describe}</textarea>
										</td>
										<%--<td class="text-right" style="line-height: 65px;">方案号码:</td>
										<td colspan="2">
											<c:forEach items="${orderList}" varStatus="i" var="o">[${o.lotName}-${o.playName}]&nbsp;${o.haoMa}<br></c:forEach>
										</td> --%>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseTwo">合买投注订单详情 </a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-bordered">
								<thead>
									<tr class="danger">
										<th>排序</th>
										<th>玩法</th>
										<th>投注订单号</th>
										<th>倍数</th>
										<th>本期投入</th>
										<th>出票状态</th>
										<th>下注号码</th>
										<th>开奖号码</th>
										<th>中奖情况</th>
										<th>总金额</th>
										<th>每份金额</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${orderList}" var="order" varStatus="orderIndex">
										<tr ${orderIndex.index%2!=0?'class="active"':'' }>
											<td>${orderIndex.index+1}</td>
											<td>${order.playName}</td>
											<td>${order.orderId}</td>
											<td>${order.multiple}</td>
											<td>${order.buyMoney}</td>
											<td>${joint.type eq 1?'出票中':joint.type eq 2 || joint.type eq 3 || joint.type eq 4?'出票成功':joint.type eq 5?'出票失败':'出票失败(撤单)' }</td>
											<td>${order.haoMa}</td>
											<td>${order.lotteryHaoMa eq null?'未开奖':order.lotteryHaoMa}</td>
											<td>${order.status eq 1?'未开奖':order.status eq 2?'已中奖':order.status eq 3?'未中奖':order.status eq 4?'撤单':order.status eq 5?'回滚成功':order.status eq 6?'回滚异常':order.status eq 7?'开奖异常':order.status eq 8?'和':'合买失效'}</td>
											<td>${order.status eq 2 && order.winMoney ne null?order.winMoney:'--'}</td>
											<td>
												<c:choose>
													<c:when test="${order.status eq 2 && order.winMoney ne null}">${order.jointWinOneMoney}</c:when>
													<c:otherwise>--</c:otherwise>
												</c:choose>
												</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseThree">参与合买详情</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-bordered">
								<thead>
									<tr class="danger">
										<th>用户名</th>
										<th>认购份数</th>
										<th>认购总金额</th>
										<th>已认购金额</th>
										<th>所占比例</th>
										<th>认购时间</th>
										<th>分得奖金</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${pateList}" var="pate" varStatus="pateIndex">
										<tr ${pateIndex.index%2!=0?'class="active"':''}>
											<td>${pate.account}</td>
											<td>${pate.branchNum}份</td>
											<td><fmt:formatNumber pattern="#,##0.00"
													value="${pate.totalMoney }" />元</td>
											<td><fmt:formatNumber pattern="#,##0.00"
													value="${pate.totalMoney }" />元</td>
											<%--${pate.branchNum/jointPur.branchNum} --%>
											<td><fmt:formatNumber value="${pate.branchNum/joint.branchNum}" type="percent"
													maxFractionDigits="2" minFractionDigits="2" /></td>
											<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
													value="${pate.createTime}" /></td>
											<td><c:if test="${empty pate.winMoney}">0</c:if> <c:if
													test="${not empty pate.winMoney}">
													<fmt:formatNumber pattern="#,##0.00"
														value="${pate.winMoney }" />
												</c:if>元</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
