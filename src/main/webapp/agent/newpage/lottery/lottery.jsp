<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="关键字">
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lottery/list.do',
		columns : [ {
			field : 'name',
			title : '彩种名称',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'ago',
			title : '开封盘时间差(秒)',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			field : 'viewGroup',
			title : '显示时分组',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : groupViewCZ
		}, {
			field : 'status',
			title : '彩种状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'sortNo',
			title : '排序',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'limitAccount',
			title : '限制会员',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'showFlag',
			title : '下注页优先展示',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : showFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lottery/closeOrOpen.do?id="+row.id+"&status="});
	}
	function showFormatter(value, row, index) {
		if(value==2){
			return "快捷下注";
		}else{
			return "正常下注";
		}
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/cp/lottery/modify.do?id='+row.id+'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
});
</script>