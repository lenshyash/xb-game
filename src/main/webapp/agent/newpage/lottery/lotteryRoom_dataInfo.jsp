<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	#datagrid_tb{
		overflow-x:auto;
		max-height:400px;
		display: inline-block;
	}
</style>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">通知信息列表(*注：修改时，使用[xxxx]此参数必须保留，否则无法正常显示相应的数据)</h4>
		</div>
		<div class="modal-body">
			<c:if test="${isInitData > 0}">
				<button class="btn btn-primary pull-left" onclick="initFormData(${roomId});">初始化遗漏信息</button>
			</c:if>
			<table class="fui-default-table" id="datagrid_tb"></table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotRoom/dataInfoAll.do?roomId=${roomId}',
		columns : [ {
			field : 'content',
			title : '信息内容',
			align : 'center',
			width : '400',
			valign : 'middle'
		}, {
			field : 'typeName',
			title : '信息类型说明',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'roomName',
			title : '所属房间',
			align : 'center',
			width : '150',
			valign : 'middle',
		}, {
			field : 'status',
			title : '信息状态',
			align : 'center',
			valign : 'middle',
			width : '100',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			valign : 'middle',
			width : '100',
			formatter : operateFormatter
		} ]
	});
	
	function statusFormatter(value, row, index){
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotRoom/updateStatusDataInfo.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/cp/lotRoom/modifyDataInfo.do?id='+row.id+'" title="修改">修改</a>  ']
				.join('');
	}
});

function dataInfoRefresh(json,$form){
	if(json.success){
		$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
		if(!json.unclose){
			var closeBtn=$form.find(".fui-close");
			if(closeBtn.length){
				closeBtn.eq(0).click();
			}else{
				layer.closeAll();
			}
		}
		if(json.closeTabId){
			Fui.tab.close(json.closeTabId);
		}
	}else{
		layer.msg(json.msg);
	}
	if(json.resultCallback)json.resultCallback(json);
}

function initFormData(roomId){
	$.ajax({
		url:"${base}/agent/cp/lotRoom/initFormData.do",
		data:{roomId:roomId},
		success:function(res){
			if(res.success){
				layer.msg("初始化成功");
				$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
			}
		}
	})
}
</script>