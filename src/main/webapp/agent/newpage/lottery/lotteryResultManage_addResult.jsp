<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotteryData/saveResult.do" class="form-submit" id="lottery_result_manager_add_result_form_id">
<div class="modal-dialog"><input type="hidden" value="${param.id }" name="bcId">
<input type="hidden" value="${param.resultType }" name="resultType">
<input type="hidden" value="${param.lotCode }" name="lotCode">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">开奖结果修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">彩种名称：</td>
						<td><input type="text" name="lotName" disabled="disabled" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">期号：</td>
						<td><input name=qiHao class="form-control"disabled="disabled" value="${param.qiHao }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开奖号码：</td>
						<td><input name="haoMa" class="form-control" value="${param.haoMa }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">随机号码：</td>
						<td><span id="nums"></span>
						<input type="button" value="更换一组" id="change" class="btn btn-default"/>
						<input type="button" id="define" value="确定" class="btn btn-default"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">总和大小单双</td>
						<td><span id="abc"></span></td>
					</tr>
					<tr>
						<td colspan="2" id="lottery_result_manager_add_result_desc_id"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<style>
#change {
    background-color: #f44336e0;
    color: #f9f7f7;
    border-color: #ccc;
}
#define {
    background-color: #79ac3efc;
   	color: #f9f7f7;
    border-color: #ccc;
}

</style>


<script type="text/javascript">

requirejs(['jquery'],function(){
	var num=0;
	var repeat = true;
	var min=0;
	var max=0;
	var arr;
	var numStr;
	var addZero=false;
	var sorted = false;
	var $form=$("#lottery_result_manager_add_result_form_id"),
		$pcode=$("#lottery_lottery_result_manage_form_id").find("[name='code']");
// 	$form.find("[name='lotCode']").val($pcode.val());
	$form.find("[name='lotName']").val($pcode.attr("lotname"));
	var code="${param.lotCode}";
	switch(code){
	case 'EFC':
	case 'FFC':
	case 'WFC':
	case 'HKWFC':
	case 'AMWFC':
	case 'SFC':
	case 'ESFC':
	case 'AMWFC':
		num = 5;
		min=0;
		max=9;
		repeat=true;
		addZero = false;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是5个0～9的数字组成，且必须使用半角逗号隔开，如0,9,3,6,0");
		break;
	case 'SFLHC':
	case 'TMLHC':
	case 'WFLHC':
	case 'HKMHLHC':
		num = 7;
		min=1;
		max=49;
		repeat=false;
		addZero=true;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是7个01~49的不重复数字组成，且必须使用半角逗号隔开，如03,29,13,25,18,15,06");
		break;
	case 'FFK3':
	case 'WFK3':
	case 'JPK3':
	case 'KRK3':
	case 'HKK3':
	case 'AMK3':
	case 'SFK3':
	case 'ESK3':
	case 'TMK3':
		num = 3;
		min=1;
		max=6;
		repeat=true;
		addZero=false;
		if(${k3Sored}){
			sorted = true;
		}
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是3个1～6的数字组成，且必须使用半角逗号隔开，如1,4,3");
		break;
	case 'SF28':
		num = 3;
		min=0;
		max=9;
		repeat=true;
		addZero=false;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是3个0～9的数字组成，且必须使用半角逗号隔开，如1,4,3");
		break;
		
	case 'SFSC':
	case 'FFSC':
	case 'WFSC':
	case 'WFFT':
	case 'LBJSC':
	case 'SFFT':
	case 'XSFSC':
	case 'XWFSC':
		num = 10;
		min=1;
		max=10;
		repeat=false;
		addZero=true;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是10个01~10的数字组成，且必须使用半角逗号隔开，如10,07,01,04,05,03,09,02,06,08");
		break;
	case 'WF3D':
	case 'FF3D':
		num = 3;
		min=0;
		max=9;
		repeat=true;
		addZero=false;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是3个0~9的数字组成，且必须使用半角逗号隔开，如1,9,5");
		break;
	case 'FF11X5':
	case 'SF11X5':
	case 'WF11X5':
		num = 5;
		min=1;
		max=11;
		repeat=false;
		addZero=false;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是5个01~11的数字组成，且必须使用半角逗号隔开，如10,06,05,11,02");
		break;	
	case 'SFKLSF':
	case 'WFKLSF':
		num = 8;
		min=1;
		max=20;
		repeat=false;
		addZero=true;
		$form.find("#lottery_result_manager_add_result_desc_id").html("开奖号码必须是8个01~20的数字组成，且必须使用半角逗号隔开，如16,12,07,10,04,06,14,01");
		break;		
	}
	if(repeat){
		arr = randomNum(num,min,max,sorted);
	}else{
		arr = my_ran(num,min,max);
	}
	if(addZero){
		numStr = spliceNums(arr);
	}else{
		numStr = arr;
	}
	qwer(arr);
	$("#nums").text(numStr);
	$('#change').click(function () {
		if(repeat){
			arr = randomNum(num,min,max,sorted);
		}else{
			arr = my_ran(num,min,max);
		}
		
		if(addZero){
			numStr = spliceNums(arr);
		}else{
			numStr = arr;
		}
		qwer(arr);
		$("#nums").text(numStr);
	});
	$('#define').click(function () {
		$("input[name='haoMa']").val($("#nums").text());
	});
});

function spliceNums(arr){
	var str = "";
	for(i=0;i<arr.length;i++){
		if(i==arr.length-1){
			str+=PrefixInteger(arr[i],2);
		}else{
			str+=PrefixInteger(arr[i],2)+',';
		}
		
	}
	return str;
}
function PrefixInteger(num, length) {
	 return (Array(length).join('0') + num).slice(-length);
}

function my_ran(n,min,max){
	  var arr=[];
	  var arr2=[];
	  for(i=0;i<max-min+1;i++){
	    arr[i]=i+min;
	  }
	  for(var j,x,i=arr.length;i;j=parseInt(Math.random()*i),x=arr[--i],arr[i]=arr[j],arr[j]=x);
	  for(i=0;i<n;i++){
	    arr2[i]=arr[i];
	  }
		return arr2;
	}
function randomNum(n,min,max,sorted){
	  var arr=[];
	  for(i=0;i<n;i++){
		arr[i]=parseInt(Math.random()*(max-min+1)+min);
	  }
	  if(sorted){
		  arr.sort();
	  }
	return arr;
}
function qwer(arr) {
	var sum = 0;
	var sdStr = "";
	var sizeStr = "";
	var text = "";
	var warning = "";
	//计算总数
	var code="${param.lotCode}";
	//计算大小
	switch (code) {
	case 'EFC':
	case 'FFC':
	case 'WFC':
	case 'HKWFC':
	case 'AMWFC':
	case 'SFC':
	case 'ESFC':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,23);
		break;
	case 'SFLHC':
	case 'TMLHC':
	case 'WFLHC':
	case 'HKMHLHC':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,175);
		break;
	case 'FFK3':
	case 'WFK3':
	case 'JPK3':
	case 'KRK3':
	case 'HKK3':
	case 'AMK3':
	case 'SFK3':
	case 'ESK3':
	case 'TMK3':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,11);
		warning = k3warning(arr);
		break;
	case 'SF28':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,14);
		break;
	case 'SFSC':
	case 'FFSC':
	case 'WFSC':
	case 'WFFT':
	case 'LBJSC':
	case 'SFFT':
	case 'XSFSC':
	case 'XWFSC':
		sum = pk10sum(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,11);
		warning = pk10warning();
		break;
		
	case 'WF3D':
	case 'FF3D':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,14);
		break;
	case 'FF11X5':
	case 'SF11X5':
	case 'WF11X5':
		sum = sum1(arr);
		sdStr = singleDoubleCheck(sum);
		sizeStr = sizeCheck(sum,30);
		break;
	case 'SFKLSF':
	case 'WFKLSF':
		sum = "";
		warning = "<span class='text-danger'> 注：无总和玩法</span>";
		break;
	default:
		break;
	}
	text = sum +" "+sdStr+" "+sizeStr+warning;
	$("#abc").html(text);
}
//计算总和
function sum1(arr) {
	var sum = 0;
	for (var i = 0; i < arr.length; i++) {
		sum = sum + arr[i];
	}
	return sum;
}
//普通计算大小
function sizeCheck(num,checkNum){
	var result="";
	if(num!=0){
		if(num>=checkNum){
			result = "大";
		}else{
			result = "小";
		}
	}
	return result;
}
//普通计算单双
function singleDoubleCheck(num){
	if(num==0){
		return "";
	}
	return num%2==0?"双":"单";
}
//快3提醒
function k3warning(arr) {
	if(arr[0]==arr[1] && arr[1]==arr[2] && arr[0]==arr[2]){
		return "<span class='text-danger'> 注：三个号码都相同</span>";
	}else{
		return "";
	}
}
//赛车类提醒
function pk10warning() {
	return "<span class='text-danger'> 注：PK10类只计算冠亚和</span>";
	
}
//赛车类计算总和
function pk10sum(arr) {
	var sum = 0;
	var sum = arr[0]+arr[1];
	return sum;
}
</script>