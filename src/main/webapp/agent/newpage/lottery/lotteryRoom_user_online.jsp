<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotRoom/saveRobot.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">在线人数列表(*不包含机器人在内，只显示真实用户)</h4>
		</div>
		<div class="modal-body">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">总人数：${onlineCount}</h3>
				</div>
				<div class="panel-body">
					<c:choose>
						<c:when test="${onlineMap.size()>0 }">
							<c:forEach items="${onlineMap}" var="map">
								<button type="button" class="btn btn-default btn-sm" style="color: rgb(212, 106, 64);">
									 <span class="glyphicon glyphicon-user"></span> ${map}
								</button>
							</c:forEach>
						</c:when>
						<c:otherwise>
							该房间暂无会员！
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
</form>