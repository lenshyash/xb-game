<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" format="YYYY-MM-DD HH:mm:ss" value="<c:if test="${not empty param.begin}">${param.begin}</c:if><c:if test="${empty param.begin}">${startTime}</c:if>" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" name="account" value="${param.account}" placeholder="会员账号">
				</div>
				<div class="input-group">
					<label class="sr-only" for="type">类型</label> 
					<select class="form-control" name="type">
						<option value="">全部类型</option>
						<option value="1">可认购</option>
						<option value="2">满员</option>
						<option value="3">已截止</option>
						<option value="4">完成</option>
						<option value="5">方案失效</option>
					</select>
				</div>
				<div class="input-group">
					<label class="sr-only" for="czType">彩种类型</label> 
					<select class="form-control" name="code" id="game_record_lottery_type">
						<option value="" selected>全部彩种</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name=endTime class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="<c:if test="${not empty param.end}">${param.end}</c:if><c:if test="${empty param.end}">${endTime}</c:if>" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="programId" placeholder="订单号">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotteryJoint/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [ 
		   {
			sortable:true,
			field : 'programId',
			title : '订单号',
			align : 'center',
			valign : 'middle',
			formatter : orderFormatter
		}, {
			field : 'account',
			title : '投注账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'lotteryName',
			title : '彩种名称',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : wsFormatter
		}, {
			field : 'qiHao',
			title : '期号',
			align : 'center',
			width : '100',
		},{
			field : 'createTime',
			title : '投注时间',
			width : '150',
			align : 'center',
			formatter : Fui.formatDatetime
		},{
			field : 'endTime',
			title : '截止时间',
			width : '150',
			align : 'center',
			formatter : Fui.formatDatetime
		}, {
			field : 'branchNum',
			title : '总份数(份)',
			align : 'center',
			width : '50',
		},{
			field : 'them',
			title : '是否保底(份)',
			align : 'center',
			width : '50',
			formatter : themFormatter
		},{
			field : 'commission', 
			title : '提成(%)',
			align : 'center',
			width : '50',
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'totalMoney',
			title : '投注金额(元)',
			align : 'center',
			width : '50',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.buyMoney != null && row.status < 4){
						r = r+row.buyMoney;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.buySum ? aggsData.buySum.toFixed(2) : "0.00";
			}
		},{
			sortable:true,
			field : 'winTotalMoney',
			title : '中奖金额(元)',
			align : 'center',
			width : '50',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.winMoney != null){
						r = r+row.winMoney;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.winSum ? aggsData.winSum.toFixed(2) : "0.00";
			}
		},{
			sortable:true,
			field : 'lossMoney',
			title : '亏损金额(元)',
			align : 'center',
			width : '50',
			formatter : lossMoneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var buySum=0,winSum=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.buyMoney != null){
						buySum = buySum+row.buyMoney;
					}
					if(row.winMoney!=null){
						winSum=winSum+row.winMoney;
					}
				}
				
				var lossSum=buySum-winSum;
				return lossSum.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				
				return aggsData.buySum-aggsData.winSum ? (aggsData.buySum-aggsData.winSum).toFixed(2) : "0.00";
			}
		},
		 {
			field : 'type',
			title : '状态',
			width : '100',
			align : 'center',
			formatter : statusFormatter
		}]
	});
	
	function lossMoneyFormatter(value,row,index){
		var buy=row.buyMoney;
		var win=row.winMoney;
		
		if (isNaN(win)) { 
			return "未开奖";
		}
		var loss=buy-win;
		　
		
		return loss;
	}
	
	function themFormatter(value,row,index){
		if(value == 0){
			return "未保底";
		}
		return value;
	}
		
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.accountId,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
		
	function wsFormatter(value, row, index) {
		if(value!=null && value.length>10){
			return  value.substring(0, 10)+"...";
		}else{
			return value;
		}
	}
	
	function orderFormatter(value, row, index){
		if(value){
			var s= '<a class="open-dialog" href="${base}/agent/cp/lotteryJoint/showLotteryJointDesc.do?programId='+value+'">'+value;
			s=s+'</a>';
			return s;
		}else{
			return value;
		}
	}
		
	function statusFormatter(value, row, index) {
		var col = '';
		switch(row.type){
		case 1:
			col = '<span class="label label-primary" >可认购</span>';
			break;
		case 2:
			col = '<span class="label label-success" >满员</span>';
			break;
		case 3:
			col = '<span class="label label-danger" >已截止</span>';
			break;
		case 4:
			col = '<span class="label label-info" >完成</span>';
			break;
		case 5:
			col = '<span class="label label-success">方案失效</span>';
			break;
		}
		return col;
	}
	$.ajax({
		url:"${base}/agent/gameRecord/czGroup.do",
		data:"GET",
		DataType:"json",
		success:function(j){
			var col = '<option value="" selected="selected">全部彩种</option>';
			for(var i in j){
				if(j[i].code!='LHC' && j[i].code != 'SFLHC' && j[i].code != 'TMLHC' && j[i].code != 'WFLHC' && j[i].code != 'HKMHLHC' && j[i].code != 'AMLHC'){
					col+='<option value="'+j[i].code+'">'+j[i].name+'</option>';
				}
			}
			$('#game_record_lottery_type').html(col);
		}
			
	});
});
</script>