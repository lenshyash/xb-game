<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="lottery_mark_six_con_wrap_id">
    <ul class="nav nav-tabs mb10px">
        <li class="active"><a href="#" data-toggle="tab" oname="record">六合记录</a></li>
        <li><a href="#" data-toggle="tab" oname="tongji">六合统计</a></li>
    </ul>
    <form class="fui-search table-tool" method="post" id="lhcrawrd_form_id">
        <div class="form-group fui-data-wrap">
            <div class="form-inline">
                <div class="input-group">
                    <input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="startTime"
                           value="<c:if test="${not empty param.begin}">${param.begin}</c:if><c:if test="${empty param.begin}">${startTime}</c:if>"
                           placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback"
                                                     aria-hidden="true"></span>
                </div>
                <button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="account" value="${param.account}"
                               placeholder="会员账号">
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" name="qihao" placeholder="投注期号">
                    </div>
                    <div class="input-group">
                        <select class="form-control" name="status">
                            <option value="" selected>全部类型</option>
                            <option value="1">等待开奖</option>
                            <option value="2">已中奖</option>
                            <option value="3">未中奖</option>
                            <option value="4">撤单</option>
                            <option value="5">派奖回滚成功</option>
                            <option value="6">回滚异常的</option>
                            <option value="7">开奖异常</option>
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary search-btn fui-date-search" type="button">查询</button>
                <c:if test="${exportPerm}">
                    <button class="btn btn-primary exportBtn" type="button">导出</button>
                </c:if>
            </div>
            <div class="form-inline mt5px">
                <div class="input-group">
                    <input type="text" name=endTime class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss"
                           value="<c:if test="${not empty param.end}">${param.end}</c:if><c:if test="${empty param.end}">${endTime}</c:if>"
                           placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback"
                                                     aria-hidden="true"></span>
                </div>
                <button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
                <button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
                <div class="input-group">
                    <input type="text" class="form-control" name="orderCode" placeholder="订单号">
                    <input type="hidden" value="LHC" class="form-control" name="code" placeholder="订单号">
                </div>
                <div class="input-group">
                    <select class="form-control" id="groupCodeById">
                        <option value="" selected>--全部大类--</option>
                        <c:forEach items="${groupList}" var="group">
                            <option value="${group.id}">${group.name}</option>
                        </c:forEach>
                    </select>
                </div>
             <div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
                <div class="input-group" style="display:none;">
                    <select class="form-control" name="playCode" id="playCodeById">
                    </select>
                </div>
            </div>
        </div>
    </form>
    <table class="fui-default-table"></table>
    <table border="1" class="table table-hover table-striped tongji-table hidden"></table>
</div>

<c:if test="${exportPerm}">
    <div class="hidden">
        <form id="lhcrecord_export_fm_id" action="${base}/agent/gameRecord/lhcExport.do" target="_blank">
            <input type="hidden" name="startTime"/>
            <input type="hidden" name="account"/>
            <input type="hidden" name="qihao"/>
            <input type="hidden" name="status"/>
            <input type="hidden" name="endTime"/>
            <input type="hidden" name="orderCode"/>
            <input type="hidden" name="groupCode"/>
            <input type="hidden" name="code"/>
        </form>
    </div>
</c:if>

<script type="text/javascript">
    requirejs(['${base}/agent/js/lottery/lhcGameRecord.js?v=1.32'], function ($ele) {
        $ele.render();
    });


    <c:if test="${exportPerm}">$(".exportBtn").click(function () {
        var $form1 = $("#lhcrecord_export_fm_id");
        var $form=$("#lhcrawrd_form_id");
        $form1.find("[name='startTime']").val($form.find("[name='startTime']").val());
        $form1.find("[name='account']").val($form.find("[name='account']").val());
        $form1.find("[name='qihao']").val($form.find("[name='qihao']").val())
        $form1.find("[name='status']").val($form.find("[name='status']").val())
        $form1.find("[name='endTime']").val($form.find("[name='endTime']").val())
        $form1.find("[name='orderCode']").val($form.find("[name='orderCode']").val())
        $form1.find("[name='groupCode']").val($("#groupCodeId").val())
        $form1.find("[name='code']").val($form.find("[name='code']").val())
        $form1.submit();
    });
    </c:if>

</script>