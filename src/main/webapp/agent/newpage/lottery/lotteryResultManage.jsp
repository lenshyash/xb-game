<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
官方彩：<ul class="nav nav-tabs mb10px" id="lottery_lottery_result_manage_group_id"></ul>
<c:if test="${!empty replaceFont }">${replaceFont }：</c:if><c:if test="${empty replaceFont }">系统彩：</c:if><ul class="nav nav-tabs mb10px" id="lottery_lottery_result_manage_group_id_sys"></ul>
<form class="fui-search table-tool" method="post" id="lottery_lottery_result_manage_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" value="${curDate}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="form-group">
				<div class="input-group"><input type="hidden" name="code">
					<input type="text" class="form-control" name="qihao" placeholder="期号">
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary fui-date-search" id="member_backwater_record_list_fan_shui_id">批量预设</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" value="${curDate}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<c:if test="${countDown eq 'on' }"><div class="alert alert-secondary" style="display: inline-flex;margin-bottom: unset;background: #383d41ad;padding: 7px;color: #ffffff" id="qihaoCountDown">
				正在加载中....
			</div></c:if>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
	var lotteryDatas={
		title:"",
		qiHao:"",
		time:"",
		code:'',
		timer:'',
		countDown:'${countDown}'

	}
	$(function(){
		$("#fui_tab_43").click(function () {
			clearInterval(lotteryDatas.timer)
		})
	})
requirejs(['jquery','bootstrap','Fui'],function(){
	//start 批量预设
	var $fanDianBtn=$("#member_backwater_record_list_fan_shui_id")
			,$form=$("#member_backwater_record_list_fform_id");
	$fanDianBtn.click(function(){
		var $table=$fanDianBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
				,count = list.length
				,bcId = ''
				,lotCode='' , fsa=null,it=null;
		if(count <= 0){
			layer.msg('请选择需要预设的记录!');
			return false;
		}else{
			for(var i=0;i<count;i++){
				it = list[i];
				lotCode = it.lotCode;
				if(it.id){
						bcId=it.id+","+bcId;
				}
			}
			var index=layer.confirm('确定要对选择的记录进行批量预设吗？', function() {
				$.ajax({
					url:"${base}/agent/cp/lotteryData/addResultBatch.do",
					data:{lotCode:lotCode,
						bcId:bcId
					},
					success:function(result){
						layer.msg(result.msg);

					}
				});
				layer.close(index);
				$table.bootstrapTable('refresh');
			});
		}
		addTable();
	});
	// end
	var $form=$("#lottery_lottery_result_manage_form_id");
	$.ajax({
		url : "${base}/agent/cp/lotteryData/czGroup.do",
		type : "get",
		dataType : 'json',
		success : function(i) {
			var col = "";
			var sysCol = "";
			for ( var j in i) {
				if(i[j].isSys){
					if (j == 0) {
						sysCol += '<li class="active"><a href="#" data-toggle="tab" class="lotcode" code="'+i[j].code+'" title="' + i[j].name + '">'+ i[j].name + '</a></li>';
						$form.find('[name="code"]').val(i[j].code).attr("lotname",i[j].name);
						if(lotteryDatas.countDown == 'on'){
							newOpenDate(i[j].code)
						}
					} else {
						sysCol += '<li><a href="#" data-toggle="tab" class="lotcode" code="'+i[j].code+'" title="' + i[j].name + '">' + i[j].name + '</a></li>';
					}
				}else{
					if (j == 0) {
						col += '<li class="active"><a href="#" data-toggle="tab" class="lotcode" code="'+i[j].code+'" title="' + i[j].name + '">'+ i[j].name + '</a></li>';
						$form.find('[name="code"]').val(i[j].code).attr("lotname",i[j].name);
						if(lotteryDatas.countDown == 'on'){
							newOpenDate(i[j].code)
						}
					} else {
						col += '<li><a href="#" data-toggle="tab" class="lotcode" code="'+i[j].code+'" title="' + i[j].name + '">' + i[j].name + '</a></li>';
					}
				}
			}
			addTable();

			$("#lottery_lottery_result_manage_group_id_sys").html(sysCol);
			$("#lottery_lottery_result_manage_group_id").html(col);
			$("#lottery_lottery_result_manage_group_id").find('.lotcode').click(function() {
				lotteryDatas.code = $(this).attr("code")
				lotteryDatas.title = $(this).attr("title")
				if(lotteryDatas.countDown == 'on'){
					newOpenDate(lotteryDatas.code)
				}
				$form.find('[name="code"]').val($(this).attr("code")).attr("lotname",$(this).attr("title"));
				$("#lottery_lottery_result_manage_group_id_sys li").removeAttr("class");
				var $table=$form.parents(".fui-box").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refreshOptions',{pageNumber:1});
				}
			});
			$("#lottery_lottery_result_manage_group_id_sys").find('.lotcode').click(function() {
				lotteryDatas.code = $(this).attr("code")
				lotteryDatas.title = $(this).attr("title")
				if(lotteryDatas.countDown == 'on'){
					newOpenDate(lotteryDatas.code)
				}
				$form.find('[name="code"]').val($(this).attr("code")).attr("lotname",$(this).attr("title"));
				$("#lottery_lottery_result_manage_group_id li").removeAttr("class");
				var $table=$form.parents(".fui-box").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refreshOptions',{pageNumber:1});
				}
			});
		}
	});
	function addTable(){
		Fui.addBootstrapTable({
			url : '${base}/agent/cp/lotteryData/list.do',
			columns : [ {
				field : 'qiHao',
				title : '期号',
				align : 'center',
				width : '180',
				valign : 'bottom',
				sortable:true
			}, {
				field : 'haoMa',
				title : '开奖结果',
				align : 'center',
				width : '500',
				valign : 'middle',
				formatter : haoMaxxxx
			}, {
				field : 'openStatus',
				title : '开盘状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : openState
			}, {
				field : 'endTime',
				title : '开奖时间',
				align : 'center',
				width : '400',
				valign : 'middle',
				formatter : Fui.formatDatetime
			}, {
				field : 'openTime',
				title : '派奖时间',
				align : 'center',
				width : '400',
				valign : 'middle',
				formatter : Fui.formatDatetime
			},{
				field : 'sumBuyMoney',
				title : '下注总额',
				align : 'center',
				width : '400',
				valign : 'middle',
				formatter:moneyTotal
			},{
				field : 'sumWinMoney',
				title : '中奖总额',
				align : 'center',
				width : '400',
				valign : 'middle',
				formatter:moneyTotal
			/* },{
				field : 'sumRollMoney',
				title : '返点总额',
				align : 'center',
				width : '400',
				valign : 'middle',
				formatter:moneyTotal */
			},{
				field : 'tzState',
				title : '投注状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter:touZhuState
			},{
				title : '操作',
				align : 'center',
				width : '300',
				valign : 'middle',
				formatter : operateFormatter
			},{
				title : '选择预设',
				value :"选择预设",
				align : 'center',
				width : '300',
				valign:'middle',
				checkbox : true
			}]
		});
	}
	function haoMaxxxx(value,row){
		isFC=(row.lotCode == "EFC"||row.lotCode == "WFC"||row.lotCode == "HKWFC"||row.lotCode == "AMWFC"||row.lotCode == "FFC"||row.lotCode == "SFLHC"||row.lotCode == "TMLHC"||row.lotCode == "SFSC"||row.lotCode == "FFSC"||row.lotCode == "FFK3"||row.lotCode == "WFK3"||row.lotCode == "JPK3"||row.lotCode == "KRK3"||row.lotCode == "HKK3"||row.lotCode == "AMK3"||row.lotCode == "SFK3"||row.lotCode == "ESK3"||row.lotCode == "WFSC"||row.lotCode == "WFFT"||row.lotCode == "LBJSC"||row.lotCode == "SFFT"
				||row.lotCode == "SF28"||row.lotCode == "TMK3"||row.lotCode == "WFLHC"||row.lotCode == "HKMHLHC"||row.lotCode == "HKMHLHC"
				||row.lotCode == "ESFC"||row.lotCode == "SFC"||row.lotCode == "FF3D"||row.lotCode == "FF11X5"||row.lotCode == "SF11X5"||row.lotCode == "WF11X5"||row.lotCode == "WF3D"||row.lotCode == "WFKLSF"||row.lotCode == "SFKLSF"||row.lotCode == "XSFSC"||row.lotCode == "XWFSC")
		if(isFC){
			if(value && value.length > 0){
				var str = qwer(value,row.lotCode);
				console.log(value);
				console.log(row.lotCode);
				console.log(str);
				return '<span>'+value+'&nbsp'+':总和大小单双:'+'&nbsp'+str+'</span>';
			}else{
				return '-'
			}
		}else{
			return value;
		}


	}

	//开奖状态
	function openState(value){
		switch (value) {
		case 1:
			return '<span class="label label-danger">未开奖</span>';
			break;
		case 2:
			return '<span class="label label-primary">未派奖</span>';
			break;
		case 3:
			return '<span class="label label-success">已开奖</span>';
			break;
		case 4:
			return '<span class="label label-warning">未开奖完</span>';
			break;
		case 5:
			return '<span class="label label-info">已经取消</span>';
			break;
		case 6:
			return '<span class="label label-default">已经回滚</span>';
			break;
		}
	}
	function moneyTotal(value, row, index) {
		if(row.tzState == 1 && value*1 > 0 ){
			return value;
		}else{
			return '-';
		}
	}
	function touZhuState(value){
		switch (value) {
		case 1:
			return '<span class="label label-success">已结束</span>';
			break;
		case 2:
			return '<span class="label label-danger">未开始</span>';
			break;
		case 3:
			return '<span class="label label-primary">投注中</span>';
			break;
		}
	}
	function operateFormatter(value, row, index) {
		var col= '',isFC=(row.lotCode == "EFC"||row.lotCode == "WFC"||row.lotCode == "HKWFC"||row.lotCode == "AMWFC"||row.lotCode == "FFC"||row.lotCode == "SFLHC"||row.lotCode == "TMLHC"||row.lotCode == "SFSC"||row.lotCode == "FFSC"||row.lotCode == "FFK3"||row.lotCode == "WFK3"||row.lotCode == "JPK3"||row.lotCode == "KRK3"||row.lotCode == "HKK3"||row.lotCode == "AMK3"||row.lotCode == "SFK3"||row.lotCode == "ESK3"||row.lotCode == "WFSC"||row.lotCode == "WFFT"||row.lotCode == "LBJSC"||row.lotCode == "SFFT"||row.lotCode == "SF28"||row.lotCode == "TMK3"||row.lotCode == "WFLHC"||row.lotCode == "HKMHLHC"||row.lotCode == "HKMHLHC"
			||row.lotCode == "ESFC"||row.lotCode == "SFC"||row.lotCode == "FF3D"||row.lotCode == "FF11X5"||row.lotCode == "SF11X5"||row.lotCode == "WF11X5"||row.lotCode == "WF3D"||row.lotCode == "WFKLSF"||row.lotCode == "SFKLSF"||row.lotCode == "XSFSC"||row.lotCode == "XWFSC")
		if(isFC){
			switch (row.openStatus) {
				case 1://未开奖
					if(row.tzState==1){//投注结束
						col+='<a class="open-dialog" href="${base}/agent/cp/lotteryData/addResult.do?resultType=1&id='+row.id+'&qiHao='+row.qiHao+'&lotCode='+row.lotCode+'&haoMa='+(row.haoMa||'')+'">添加结果</a>&nbsp;';
			 			col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=1&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要取消期号为“'+row.qiHao+'”？">单期取消</a>&nbsp;';
					}else{
						if(row.tzState==3){
							col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=5&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要锁定期号为“'+row.qiHao+'”？">锁定</a>&nbsp;';
							
						}
						col+='<a class="open-dialog" href="${base}/agent/cp/lotteryData/addResult.do?resultType=-1&id='+row.id+'&qiHao='+row.qiHao+'&lotCode='+row.lotCode+'&haoMa='+(row.haoMa||'')+'">预设结果</a>&nbsp;';
						col+='<a class="open-dialog" href="${base}/agent/cp/lotteryData/toLimitBetAccount.do?id='+row.id+'&qiHao='+row.qiHao+'&limitBetAccount='+(row.limitBetAccount||'')+'">限制投注</a>&nbsp;';
						if(row.haoMa && row.haoMa.length > 0){
							col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/cancelResult.do?id='+row.id+'" title="确定要撤销结果为“'+row.qiHao+'”？">撤销结果</a>&nbsp;';
						}
					}
					return col;
				case 2://未派奖
				case 6://已经回滚
					col+='<a class="open-dialog" href="${base}/agent/cp/lotteryData/addResult.do?resultType=1&id='+row.id+'&qiHao='+row.qiHao+'&lotCode='+row.lotCode+'&haoMa='+(row.haoMa||'')+'">添加结果</a>&nbsp;';
					col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=3&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要重新结算期号为“'+row.qiHao+'”？">重新结算</a>&nbsp;';
		 			return col;
				case 3://已开奖
					col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=4&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要派奖回滚期号为“'+row.qiHao+'”？">派奖回滚</a>&nbsp;';
					return col;
				case 4://未开奖完
					col+='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=3&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要重新结算期号为“'+row.qiHao+'”？">重新结算</a>&nbsp;';
					return col;
				case 5://已经取消
					return col;
			}
		}else{
			if(row.openStatus==1 && row.tzState==1){
				col='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=1&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要取消期号为“'+row.qiHao+'”？">单期取消</a>&nbsp;';
			}
			if(row.lotCode !='LHC' && row.lotCode != "FC3D" && row.lotCode != 'PL3'){
				if(row.openStatus==1 && (row.tzState == 3 || row.tzState == 2)){
					col='<a class="open-dialog" href="${base}/agent/cp/lotteryData/toLimitBetAccount.do?id='+row.id+'&qiHao='+row.qiHao+'&limitBetAccount='+(row.limitBetAccount||'')+'">限制投注</a>&nbsp;';
				}
			}
			if(row.openStatus==3){
				col='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=4&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要派奖回滚期号为“'+row.qiHao+'”？">派奖回滚</a>&nbsp;';
			}
			if(row.openStatus==6){
				col='<a class="todo-ajax" href="${base}/agent/cp/lotteryData/czOperator.do?type=3&id='+row.id+'&lotCode='+row.lotCode+'" title="确定要重新结算期号为“'+row.qiHao+'”？">重新结算</a>&nbsp;';
			}
			return col;
		}
	}
	//获取最新一期的投注
	function newOpenDate(code){
		clearInterval(lotteryDatas.timer)
		$("#qihaoCountDown").text("正在加载中....")
		let time = new Date()
		let Year = time.getFullYear()
		let Month = time.getMonth() + 1  < 10 ? "0" + time.getMonth() : time.getMonth()
		let Dates = time.getDate() < 10 ? "0" + time.getDate() : time.getDate()
		let todayTime = Year + '-' + Month + '-' + Dates;
		$.ajax({
			url:"${base}agent/cp/lotteryData/getCountDown.do",
			data:{code:code},
			type:"POST",
			success:function (res) {
				clearInterval(lotteryDatas.timer)
				if (res.success){
					lotteryDatas.qiHao = res.content.qiHao
					let differTime = parseInt((res.content.endTime - res.content.serverTime )/1000)
					let day=0,hours=0,Minutes = 0,Secodes=0,titleTime=''
					if(differTime >= 24*60*60){ //天 到 时
						day = parseInt(differTime / 24*60*60)
						hours =  parseInt((differTime - (day * 24*60*60)) / (60*60))
						Minutes = parseInt((differTime - (day * 24*60*60) - (hours * 60*60)) / (60))
						Secodes = parseInt(differTime - (day * 24*60*60) - (hours * 60*60) - (Minutes * 60))
						countDown(day,hours,Minutes,Secodes)
					}else if (differTime < 24*60*60 && differTime > 60*60){ //时 到分
						hours =  parseInt((differTime - (day * 24*60*60)) / (60*60))
						Minutes = parseInt((differTime - (day * 24*60*60) - (hours * 60*60)) / 60)
						Secodes = parseInt(differTime - (day * 24*60*60) - (hours * 60*60) - (Minutes * 60) )
						countDown(day,hours,Minutes,Secodes)
					}else if (differTime < 60*60){ //分 到 秒
						Minutes = parseInt((differTime - (day * 24*60*60) - (hours * 60*60)) / 60)
						Secodes = parseInt(differTime - (day * 24*60*60) - (hours * 60*60) - (Minutes *60))
						countDown(day,hours,Minutes,Secodes)
					}

				}
			}
		})
	}
	function countDown(day,hours,Minutes,Secodes){
		lotteryDatas.timer = setInterval(()=>{
			if (day!=0 || hours!=0 || Minutes!=0 || Secodes!=0){
				Secodes --
				if (Secodes <= 0 && Minutes > 0) {
					Minutes--
					Secodes = 59
					if (Minutes <= 0 && hours > 0) {
						hours--
						Minutes = 59
						Secodes = 59
						if (hours <= 0 && day > 0) {
							day--
							hours = 23
							Minutes = 59
							Secodes = 59
							if (day <= 0) {
								day = 0
							}
						}
					}
			}

			}else if (day==0 && hours==0 && Minutes==0 && Secodes==0){
				newOpenDate(lotteryDatas.code)
				// clearInterval(lotteryDatas.timer)
			}
			if (hours!=0  && day == 0){
				$("#qihaoCountDown").text("当前期号" + lotteryDatas.qiHao + ",距离投注截至还有"+hours +"时"+Minutes +"分"+Secodes+"秒")
			}else if (Minutes!=0 && day == 0 &&  hours == 0) {
				$("#qihaoCountDown").text("当前期号" + lotteryDatas.qiHao + ",距离投注截至还有"+Minutes +"分"+Secodes+"秒")
			}else if ( Secodes!=0 && day == 0 &&  hours == 0 &&  Minutes==0){
				$("#qihaoCountDown").text("当前期号" + lotteryDatas.qiHao + ",距离投注截至还有"+Secodes + "秒")
			}else if (day != 0 ){
				$("#qihaoCountDown").text("当前期号" + lotteryDatas.qiHao + ",距离投注截至还有"+day +"天"+hours +"时"+Minutes +"分"+Secodes+"秒")
			}


		},1000)

	}

	function qwer(str,code) {
		console.log("str:"+str)
		var arr = str.split(',');
		console.log("arr:"+arr)
		var sum = 0;
		var sdStr = "";
		var sizeStr = "";
		var text = "";
		var warning = "";
		//计算总数
		//计算大小
		switch (code) {
			case 'EFC':
			case 'FFC':
			case 'WFC':
			case 'HKWFC':
			case 'AMWFC':
			case 'SFC':
			case 'ESFC':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,23);
				break;
			case 'SFLHC':
			case 'TMLHC':
			case 'WFLHC':
			case 'HKMHLHC':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,175);
				break;
			case 'FFK3':
			case 'WFK3':
			case 'JPK3':
			case 'KRK3':
			case 'HKK3':
			case 'AMK3':
			case 'SFK3':
			case 'ESK3':
			case 'TMK3':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,11);
				warning = k3warning(arr);
				break;
			case 'SF28':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,14);
				break;
			case 'SFSC':
			case 'FFSC':
			case 'WFSC':
			case 'WFFT':
			case 'LBJSC':
			case 'SFFT':
			case 'XSFSC':
			case 'XWFSC':
				sum = pk10sum(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,11);
				warning = pk10warning();
				break;

			case 'WF3D':
			case 'FF3D':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,14);
				break;
			case 'FF11X5':
			case 'SF11X5':
			case 'WF11X5':
				sum = sum1(arr);
				sdStr = singleDoubleCheck(sum);
				sizeStr = sizeCheck(sum,30);
				break;
			case 'SFKLSF':
			case 'WFKLSF':
				sum = "";
				warning = "<span class='text-danger'> 注：无总和玩法</span>";
				break;
			default:
				break;
		}
		// console.log("sum:"+sum);
		// console.log("sdStr:"+sdStr);
		// console.log("sizeStr:"+sizeStr);
		// console.log("warning:"+warning);
		text = sum +" "+sdStr+" "+sizeStr+warning;
		return text;
	}
//计算总和
	function sum1(arr) {
		var sum = 0;
		for (var i = 0; i < arr.length; i++) {
			sum = sum + Number(arr[i]);
		}
		return sum;
	}
//普通计算大小
	function sizeCheck(num,checkNum){
		var result="";
		if(num!=0){
			if(num>=checkNum){
				result = "大";
			}else{
				result = "小";
			}
		}
		return result;
	}
//普通计算单双
	function singleDoubleCheck(num){
		if(num==0){
			return "";
		}
		return num%2==0?"双":"单";
	}
//快3提醒
	function k3warning(arr) {
		if(arr[0]==arr[1] && arr[1]==arr[2] && arr[0]==arr[2]){
			return "<span class='text-danger'> 注：三个号码都相同</span>";
		}else{
			return "";
		}
	}
//赛车类提醒
	function pk10warning() {
		return "<span class='text-danger'> 注：PK10类只计算冠亚和</span>";

	}
	//赛车类计算总和
	function pk10sum(arr) {
		var sum = 0;
		var sum = arr[0]+arr[1];
		return sum;
	}
});
</script>