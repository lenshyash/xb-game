<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lottery/update.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" value="${lottery.id }" name="id">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">彩种修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">彩种名称：</td>
						<td><input type="text" name="name" value="${lottery.name }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">显示时分组编号：</td>
						<td>
							<select name="viewGroup" class="form-control">
								<option value="1" ${lottery.viewGroup eq 1?'selected':''}>时时彩</option>
								<option value="2" ${lottery.viewGroup eq 2?'selected':''}>低频彩</option>
								<option value="3" ${lottery.viewGroup eq 3?'selected':''}>PK10</option>
								<option value="5" ${lottery.viewGroup eq 5?'selected':''}>11选5</option>
								<option value="6" ${lottery.viewGroup eq 6?'selected':''}>香港彩</option>
								<option value="7" ${lottery.viewGroup eq 7?'selected':''}>快三</option>
								<option value="8" ${lottery.viewGroup eq 8?'selected':''}>快乐十分</option>
								<option value="9" ${lottery.viewGroup eq 9?'selected':''}>PC蛋蛋</option>
								</select>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td><input name=sortNo class="form-control digits" value="${lottery.sortNo }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">图标地址：</td>
						<td><input type="text" name=imgUrl class="form-control" value="${lottery.imgUrl }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">右上角gif</td>
						<td><select name="gifUrl" class="form-control">
						<option value="">(不显示)</option>
						<option value="https://tpxb.me/xb/8.gif" <c:if test="${lottery.gifUrl eq 'https://tpxb.me/xb/8.gif'}">selected</c:if>>火</option>
						<option value="https://tpxb.me/xb/5.gif" <c:if test="${lottery.gifUrl eq 'https://tpxb.me/xb/5.gif'}">selected</c:if>>热</option>
						<option value="https://tpxb.me/xb/6.gif" <c:if test="${lottery.gifUrl eq 'https://tpxb.me/xb/6.gif'}">selected</c:if>>躺赢</option>
						<option value="https://tpxb.me/xb/7.gif" <c:if test="${lottery.gifUrl eq 'https://tpxb.me/xb/7.gif'}">selected</c:if>>活动</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制会员(多个会员用,隔开)：</td>
						<td><input name=limitAccount class="form-control" value="${lottery.limitAccount }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">热门彩种</td>
						<td><select name="hotGame" class="form-control">
						<option value="1" <c:if test="${lottery.hotGame eq 1}">selected</c:if>>普通彩种</option>
						<option value="2" <c:if test="${lottery.hotGame eq 2}">selected</c:if>>热门彩种</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">下注页优先展示</td>
						<td><select name="showFlag" class="form-control">
							<option value="1" <c:if test="${lottery.showFlag eq 1}">selected</c:if>>普通下注</option>
							<option value="2" <c:if test="${lottery.showFlag eq 2}">selected</c:if>>快捷下注</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>