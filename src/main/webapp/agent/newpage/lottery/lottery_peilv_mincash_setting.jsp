<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/markSix/cashSetting.do" class="form-submit">
	<div class="modal-dialog"><input type="hidden" name="playCode" value="${playCode}">
		<div class="modal-content"><input type="hidden" name="lotType" value="${lotType}">
			<div class="modal-header"><input type="hidden" name="type" value="2">
				<button type="button" class="close fui-close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">最小下注金额设置</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td width="30%" class="text-right media-middle">最小金额：</td>
							<td><input name="cash" class="form-control required digits" min="0" type="text"/></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default fui-close" data-dismiss="modal">关闭</button>
				<button class="btn btn-primary">保存</button>
			</div>
		</div>
	</div>
</form>