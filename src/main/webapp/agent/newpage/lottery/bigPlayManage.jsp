<%@ page language="java" pageEncoding="UTF-8"%>
<form id="lottery_big_play_manager_form_id" class="fui-search" method="post">
	<ul class="nav nav-tabs czGroup mb10px"></ul>
	<input type="hidden" name="lotType" value="1">
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#lottery_big_play_manager_form_id");
	(function getBigGroup(){
		$.ajax({
			url : "${base}/agent/cp/lotTypeGroup/getBigGroup.do",
			success : function(j) {
				var col = '';
				var replaceFont = '${replaceFont}';
				for(var i in j){
					if(i==0){
 						col+='<li class="active">';
 						$form.find("[name='lotType']").val(j[i]);
					}else{
						col+='<li>';
					}
					col+='<a href="#" data-toggle="tab" class="lotName" lottype="'+j[i]+'">'+getLotteryTypeNameA(j[i],replaceFont)+'</a></li>';
				}
				$form.find('.czGroup').html(col);
				$form.find('.lotName').click(function(){
					$form.find("[name='lotType']").val($(this).attr("lottype"));
					var $table=$form.parents(".fui-box").data("bootstrapTable");
					if($table && $table.length){
						$table.bootstrapTable('refresh');
					}
				});
				addTable();
			}
		});
	})();
	function addTable(){
		Fui.addBootstrapTable({
			url : '${base}/agent/cp/lotTypeGroup/list.do',
			columns : [{
				field : 'name',
				title : '玩法分组名称',
				align : 'center',
				width : '180',
				valign : 'bottom'
			}, {
				field : 'sortNo',
				title : '排序',
				align : 'center',
				width : '200',
				valign : 'middle'
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '200',
				valign : 'middle',
				formatter : statusFormatter
			}, {
				title : '操作',
				align : 'center',
				width : '50',
				valign : 'middle',
				formatter : operateFormatter
			} ]
		});
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotTypeGroup/updateStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/cp/lotTypeGroup/modify.do?id='+row.id+'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	function getLotteryTypeNameA(value,replaceFont) {
		var cn = '';
		switch (value-0) {
		case 2:
		case 9:
		case 52:
			cn = '时时彩';
			break;
		case 3:
		case 8:
		case 53:
		case 158:
			cn = '北京赛车(快开)';
			break;
		case 4:
		case 54:
		case 15:
			cn = '排列三/福彩3D';
			break;
		case 5:
		case 55:
		case 14:
			cn = '11选5';
			break;
		case 6:
			cn = '香港彩';
			break;
		case 7:
		case 11:
		case 57:
		case 161:
			cn = 'PC蛋蛋';
			break;
		case 10:
		case 58:
		case 100:
			cn = '快3';
			break;
		case 12:
			cn = '快乐十分';
			break;
		case 1:
		case 51:
			cn = '系统彩';
			if(replaceFont.length){
				cn = replaceFont.toString();
			}
			break;
		case 66:
			cn = '十分六合彩';
			break;
		case 929:
			cn = '系统时时彩';
			break;
		case 828:
			cn = '系统赛车';
			break;
		case 102:
			cn = '系统快3';
			break;
		}
		return cn;
	}
	
	function getLotteryTypeNameB(value,row,index) {
		var cn = '';
		switch (value-0) {
		case 2:
		case 9:
		case 52:
			cn = '时时彩';
			break;
		case 3:
		case 8:
		case 53:
		case 158:
			cn = '北京赛车(快开)';
			break;
		case 4:
		case 54:
		case 15:
			cn = '排列三/福彩3D';
			break;
		case 5:
		case 55:
		case 14:
			cn = '11选5';
			break;
		case 6:
			cn = '香港彩';
			break;
		case 7:
		case 11:
		case 57:
		case 161:
			cn = 'PC蛋蛋';
			break;
		case 10:
		case 58:
		case 100:
			cn = '快3';
			break;
		case 12:
			cn = '快乐十分';
			break;
		case 1:
		case 51:
			cn = '系统彩';
			break;
		case 66:
			cn = '十分六合彩';
			break;
		case 929:
			cn = '系统时时彩';
			break;
		case 828:
			cn = '系统赛车';
			break;
		case 102:
			cn = '系统快3';
			break;
		}
		return cn;
	}
});	
</script>