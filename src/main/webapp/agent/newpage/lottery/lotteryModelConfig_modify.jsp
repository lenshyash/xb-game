<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/lotteryModel/save.do" class="form-submit">
<div class="modal-dialog">
	<input name="lotId" type="hidden" value="${rollback.lotId }"/>
	<input name="id" type="hidden" value="${rollback.id }"/>
	<input name=lotCode type="hidden" value="${rollback.lotCode }"/>
	<input name="lotType" type="hidden" value="${rollback.lotType }"/>
	<input name="lotName" type="hidden" value="${rollback.lotName }"/>
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改开奖模式
			</h4>
			<div style="color: red;" class="platform">
				注意：
				1、中奖百分比指中奖金额是总投注的百分之多少，如设置70%，会员总投注100，总中奖金额则小于或等于70；
				<br>
				2、投注会员数量太少请勿开启百分比模式，会员可能一直不中奖
			</div>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="text-right media-middle">彩种名称：</td>
						<td>${rollback.lotName}</td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td><select name="lotteryMode" class="form-control">
							<option value="1"<c:if test="${rollback.lotteryMode==1}"> selected</c:if>>随机</option>
							<option value="2"<c:if test="${rollback.lotteryMode==2}"> selected</c:if>>中奖百分比</option>
<%-- 							<option value="3"<c:if test="${rollback.lotteryMode==3}"> selected</c:if>>正式站推送</option> --%>
						</select></td>
					</tr>
					<tr>
						<td width="5%" class="text-right media-middle">百分比(%)：</td>
						<td width="5%"><input name="percentage" class="form-control" type="text" value="${rollback.percentage }"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>