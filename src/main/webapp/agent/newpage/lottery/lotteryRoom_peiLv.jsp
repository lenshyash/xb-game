<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	#datagrid_tb{
		overflow-x:auto;
		height:400px;
		display: inline-block; 
	}
</style>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">赔率列表[注：和局大于xxxx/和局小于xxxx,请把相应金额设置在最大金额内]</h4>
		</div>
		<div class="modal-body">
			<button class="btn btn-danger pull-left" onclick="SettingMoney(this);" type="2">最小金额批量设置</button>
			<button class="btn btn-warning pull-left" onclick="SettingMoney(this);" type="1"  style="margin-left: 10px;">最大金额批量设置</button>
			<button class="btn btn-success pull-left" onclick="saveAll();" style="margin-left: 20px;">全部保存</button>
			<table class="fui-default-table" id="datagrid_tb"></table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<%--<button class="btn btn-primary">保存</button> --%>
		</div>
	</div>
</div>
<script type="text/javascript">
var roomId = ${id},lotType=${lotType};
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotRoom/rPeiLv.do?roomId=${id}&lotType=${lotType}',
		columns : [ {
			field : 'name',
			title : '号码',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : nameFormatter
		}, {
			field : 'odds',
			title : '赔率',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : oddsInputFormatter
		}, {
			field : 'minBetAmmount',
			title : '最小下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : minBetInputFormatter
		}, {
			field : 'maxBetAmmount',
			title : '最大下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : maxBetInputFormatter
		}, {
			field : 'sortNo',
			title : '排序',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : sortNoInputFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function nameFormatter(value,row,index){
		if(row.isNowYear >= 6 && row.lotType == 161){
			return value + row.maxBetAmmount;
		}
		return value;
	}
	
	function oddsInputFormatter(value, row, index) {
		return '<input name="odds" class="form-control" type="text" value="'+value+'"/>';
	}
	function minBetInputFormatter(value, row, index) {
		if(row.isNowYear >= 6 && row.lotType == 161){
			return '<input name="minBetAmmount" class="form-control" type="hidden" value="'+value+'"/>';
		}
		return '<input name="minBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function maxBetInputFormatter(value,row,index){
		return '<input name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function sortNoInputFormatter(value, row, index) {
		return '<input name="sortNo" class="form-control" type="text" value="'+value+'"/>';
	}
	function operateFormatter(value, row, index) {
		return '<a onclick="updatePeiLv(this);" href="javascript://" id="'+row.id+'">保存</a>&nbsp;';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/cp/lotRoom/closeOrOpen.do?id="+row.id+"&status="});
	}
	
});

//单行保存
function updatePeiLv(source){
	source = $(source);
	var $tr = source.parents("tr");
	var data = {
			'id' : source.attr("id"),
			'odds' : $tr.find("input[name=odds]").val(),
			'minBetAmmount' : $tr.find("input[name=minBetAmmount]").val(),
			'maxBetAmmount' : $tr.find("input[name=maxBetAmmount]").val(),
			'sortNo' : $tr.find("input[name=sortNo]").val()
	}
	$.ajax({
		url:'${base}/agent/cp/lotRoom/update.do',
		data:data,
		success:function(result){
			if(result.success){
				$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
				layer.msg(result.msg||'修改成功');
			}
		}
	})
}

//全部保存
function saveAll(){
	var $table = $("#datagrid_tb");
	var odds = '',sortNO = '',minBetAmount='',maxBetAmount='',ids='';
	$.each($table.find("tbody tr"),function(i,j){
		j = $(j);
		odds += j.find('input[name=odds]').val() + ",";
		sortNO += j.find("input[name=sortNo]").val() + ",";
		minBetAmount += j.find("input[name=minBetAmmount]").val() + ",";
		maxBetAmount += j.find("input[name=maxBetAmmount]").val() + ",";
		ids += j.find('a').attr("id") + ",";
	})
	var bc = {
			"odds" : odds,	
			"maxBetAmmount" : maxBetAmount,
			"minBetAmmount" : minBetAmount,
			"sortNo" : sortNO,
			"id" : ids,
			"lotType":lotType
		};
	$.ajax({
		url : "${base}/agent/cp/lotRoom/saveAll.do",
		data : bc,
		success : function(j) {
			layer.msg(j.msg||'数据保存成功!');
			$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
		}
	});
}

//1最大，2最小
function SettingMoney(source){
	source = $(source);
	layer.prompt({"title":source.text()},function(val, index){
		if(val*1 <0){
			layer.msg("金额不能小于0");
			return;
		}
		var bc = {
			"lotType":lotType,
			"type":source.attr("type"),
			"cash":val,
			"roomId":roomId
		};
		$.ajax({
			url:'${base}/agent/cp/lotRoom/cashSetting.do',
			data:bc,
			dataType:'json',
			success:function(result){
				layer.msg("设置成功!");
				layer.close(index);
				$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
			}
		})
	});
}
</script>