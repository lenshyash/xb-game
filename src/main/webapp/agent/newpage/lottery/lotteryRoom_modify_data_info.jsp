<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotRoom/updateDataInfo.do" class="form-submit" data-callback="dataInfoRefresh">
<div class="modal-dialog">
	<input type="hidden" value="${dataInfo.id}" name="id"/>
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改通知信息</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="text-right media-middle">所属房间：</td>
						<td class="text-left"><input type="text" disabled="disabled" value="${dataInfo.roomName}" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">信息类型：</td>
						<td class="text-left"><input type="text" disabled="disabled" value="${dataInfo.typeName}" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">信息状态：</td>
						<td class="text-left">
							<select name="status" class="form-control">
									<option value="2" ${dataInfo.status == 2?'selected':''}>启用</option>
									<option value="1" ${dataInfo.status == 1?'selected':''}>禁用</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="30%" class="text-right media-middle">信息内容：</td>
						<td class="text-left"><textarea style="height:220px;" name="content" class="form-control"><c:out value="${dataInfo.content}" escapeXml="true"></c:out></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>