<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/lotPeiLv/ruleUpdate.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" value="${playType.id }" name="id">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">玩法小类修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">玩法介绍：</td>
						<td><input type="text" name="playMethod" value="${playType.playMethod }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">中奖范例：</td>
						<td><input name="winExample" class="form-control" value="${playType.winExample }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">详细介绍：</td>
						<td><textarea name="detailDesc" class="form-control">${playType.detailDesc }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>