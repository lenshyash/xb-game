<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotRoom/saveRobot.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加机器人</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">机器人名称：</td>
						<td class="text-left"><textarea placeholder="多个帐号用英文逗号(,)隔开" name="account" class="form-control"></textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">所属房间：</td>
						<td class="text-left">
							<select name="roomId" class="form-control"><option value>请选择房间号</option>
								<c:forEach items="${roomList}" var="room">
									<option value="${room.id}">${room.name}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<%-- <tr>
						<td class="text-right media-middle">机器人头像：</td>
						<td class="text-left"><input type="text" name="imgId" class="form-control" placeholder="允许填写1~111数字"/></td>
					</tr> --%>
					<tr>
						<td class="text-right media-middle">金额范围：</td>
						<td class="text-left"><input type="text" style="width:40%;display:inline-block;" name="betMinMoney" class="form-control" placeholder="投注最小金额" />&nbsp;--&nbsp;
							<input style="width:40%;display:inline-block;" type="text" name="betMaxMoney" class="form-control" placeholder="投注最大金额"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>