<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	#datagrid_tb{
		overflow-x:auto;
		max-height:400px;
		display: inline-block;
	}
</style>
<form action="${base}/agent/cp/lotRoom/saveRobot.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">机器人列表</h4>
		</div>
		<div class="modal-body">
			<table class="fui-default-table" id="datagrid_tb"></table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<%--<button class="btn btn-primary">保存</button> --%>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
var memberLevel = [];
$(function(){
	$.ajax({
		url:'${base}/agent/system/level/list.do',
		success:function(res){
			memberLevel = res.rows;
		}
	})
});
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotRoom/robotAll.do?roomId=${roomId}',
		columns : [ {
			field : 'account',
			title : '机器人名称',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter: accountInputFormatter
		}, {
			field : 'roomName',
			title : '房间名称',
			align : 'center',
			width : '200',
			valign : 'bottom'
		}, {
			field : 'accountLevel',
			title : '机器人分层',
			align : 'center',
			width : '200',
			valign : 'bottom',
			formatter : accountLevelFormatter
		},{
			field : 'imgId',
			title : '图片路径/图片ID',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter: imgIdInputFormatter
		}, {
			field : 'betMinMoney',
			title : '最小投注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
		},{
			field : 'betMaxMoney',
			title : '最大投注金额',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	
	
	function accountLevelFormatter(value,row,index){
		var html = '<select robot_id="'+row.id+'" class="form-control" onchange="memberLevelChange(this);">';
		$.each(memberLevel,function(i,j){
			if(value == j.id){
				html += '<option value="'+j.id+'" selected>'+j.levelName+'</option>';
			}else{
				html += '<option value="'+j.id+'">'+j.levelName+'</option>';			
			}
		})
		html += '</select>';
		return html;
	}
	
	function accountInputFormatter(value, row, index) {
		if(row.status == 2){
			return value;
		}
		return '<input name="account" class="form-control" type="text" value="'+value+'"/>';
	}
	
	function imgIdInputFormatter(value,row,index){
		if(row.status == 2){
			return value;
		}
		return '<input name="imgId" class="form-control" type="text" value="'+value+'"/>';
	}
	
	function operateFormatter(value, row, index) {
		if(row.status == 1){
			return [ '<a onclick="updateRobot(this);" href="javascript://" id="'+row.id+'">保存</a>&nbsp;'].join('');
		}
		return ['<a onclick="delRobot(this);" href="javascript://" id="'+row.id+'" title="确定要删除该机器人？">删除</a>'].join('');
	}
	
});

//单行保存
function updateRobot(source){
	source = $(source);
	var $tr = source.parents("tr");
	var data = {
			'id' : source.attr("id"),
			'account' : $tr.find("input[name=account]").val(),
			'imgId' : $tr.find("input[name=imgId]").val()
	}
	$.ajax({
		url:'${base}/agent/cp/lotRoom/updateRobot.do',
		data:data,
		success:function(result){
			if(result.success){
				$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
				layer.msg(result.msg||'修改成功');
			}
		}
	})
}

function delRobot(source){
	source = $(source);
	layer.confirm(source.attr("title"), function(index){
		$.ajax({
			url:'${base}/agent/cp/lotRoom/delRobot.do',
			data:{id:source.attr("id")},
			success:function(res){
				if(res.success){
					$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
					layer.close(index);
				}
			}
		})
	},function(index){
		layer.close(index);
	})
}

function memberLevelChange(source){
	source = $(source);
	$.ajax({
		url:'${base}/agent/cp/lotRoom/robotMemberLevel.do',
		data:{id:source.attr("robot_id"),levelId:source.val()},
		success:function(res){
			if(res.success){
				$("#datagrid_tb").bootstrapTable('refreshOptions',{pageNumber:1});
				layer.msg("保存成功");
			}else{
				layer.msg(result.msg);
			}
		}
	})
}
</script>