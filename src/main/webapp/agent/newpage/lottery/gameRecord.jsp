<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" format="YYYY-MM-DD HH:mm:ss" value="<c:if test="${not empty param.begin}">${param.begin}</c:if><c:if test="${empty param.begin}">${startTime}</c:if>" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" name="account" value="${param.account}" placeholder="会员账号">
				</div>
				<div class="input-group">
					<label class="sr-only" for="type">类型</label>
					<select class="form-control" name="status">
						<option value="">全部类型</option>
						<option value="1">等待开奖</option>
						<option value="2" <c:if test="${param.type eq 2 }">selected</c:if> >已中奖</option>
						<option value="3">未中奖</option>
						<option value="4">撤单</option>
						<option value="5">派奖回滚成功</option>
						<option value="6">回滚异常的</option>
						<option value="7">开奖异常</option>
						<option value="8">和局</option>
						<option value="9">合买失效</option>
						<option value="10">和局中奖</option>
					</select>
				</div>
				<div class="input-group">
					<label class="sr-only" for="czType">彩种类型</label>
					<select class="form-control" name="code" id="game_record_lottery_type">
						<option value="">全部彩种</option>
					</select>
				</div>
				<div class="input-group">
					<label class="sr-only" for="czType">会员等级</label>
					<select class="form-control" name="level" id="game_record_lottery_type">
						<option value="">会员等级</option>
						<c:forEach items="${levels}" var="l">
							<option value="${l.id }"<c:if test="${(not empty level && level==l.id)}">selected</c:if>>
									${l.levelName }</option></c:forEach>
					</select>
				</div>
				<div class="input-group">
					<select class="form-control" name="reportType">
						<option value="">所有报表类型</option>
						<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
						<option value="2">引导</option>
					</select>
				</div>
				<c:if test="${version eq 4}">
					<div class="input-group">
						<label class="sr-only">投注房间</label>
						<select class="form-control" name="roomId">
							<option value="" selected>全部房间</option>
							<c:forEach items="${roomList}" var="room" varStatus="">
								<option value="${room.id}">${room.name}</option>
							</c:forEach>
						</select>
					</div>
				</c:if>

				<c:if test="${groupSwitch eq 'on' }">
					<div class="input-group">
						<input type="text" class="form-control" size="10" name="groupName" placeholder="群组名称" value="${groupName }">
					</div>
				</c:if>
			</div>
			<button class="btn btn-primary fui-date-search chax ">查询</button>
			<div class="form-group autoRefresh">
				<label>
					<input  type="checkbox">
				</label>
				<label for="name">自动刷新</label>
				<select class="form-control">
					<option>5</option>
					<option>10</option>
					<option>20</option>
					<option>30</option>
					<option>60</option>
					<option>120</option>
				</select>
			</div>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name=endTime class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="<c:if test="${not empty param.end}">${param.end}</c:if><c:if test="${empty param.end}">${endTime}</c:if>" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="qihao" placeholder="投注期号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="orderCode" placeholder="订单号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" size="10" name="agentName" placeholder="所属代理" value="${agentName }">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="min" size="6" placeholder="最小投注额" oninput = "value=value.replace(/[^\d]/g,'')">
			</div>
			-
			<div class="input-group">
				<input type="text" class="form-control" name="max"  size="6" placeholder="最大投注额" oninput = "value=value.replace(/[^\d]/g,'')">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="betIp" placeholder="投注IP">
			</div>
			<div class="input-group" style="display: none;text-align: center;font-size: 15px;font-weight: bold;" id="buyAccountCountSumMoneny">
				<strong class="input-group" >总计:<span >投注人数:【<span style="color: #46986c" id="buyAccountCount"> 0</span>】</span> <span style="margin-right: 15px">投注金额:【<span style="color: #46986c" id="buySum"> 0</span>】</span></strong>
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div id="mjs:tip" class="tip" style="position:absolute;left:0;top:0;display:none;"></div>
<script type="text/javascript">

	$(function(){
		var  times=''
		var time =''
		$('.autoRefresh input').change(function(){
			if ($('.autoRefresh input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.autoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else{
				autoRefreshs(time,false)
			}
		})

		$('.autoRefresh select').change(function(){
			if ($('.autoRefresh input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.autoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else {
				autoRefreshs(time,false)
			}
		})

		function autoRefreshs(time,statu){
			if (statu == true){
				times = setInterval(function(){
					$('.btn.btn-primary.fui-date-search.chax').trigger('click')
				},time * 1000)
			}else {
				clearInterval(times)
			}

		}
	})
	requirejs(['jquery','bootstrap','Fui'],function(){
		initParams();
		Fui.addBootstrapTable({
			url : '${base}/agent/gameRecord/list.do',
			showPageSummary:true,
			showAllSummary:true,
			showFooter : true,
			columns : [
				{
					sortable:true,
					field : 'orderId',
					title : '订单号',
					align : 'center',
					valign : 'middle',
					formatter : orderFormatter
				}, {
					field : 'account',
					title : '投注账号',
					align : 'center',
					valign : 'middle',
					formatter : accountFormatter
				},{
					field : 'agentName',
					title : '所属代理',
					align : 'center',
					valign : 'middle',
				}, {
					title : '彩种名称',
					align : 'center',
					width : '100',
					valign : 'middle',
					formatter : czFormatter
				}, {
					field : 'qiHao',
					title : '期号',
					align : 'center',
					valign : 'middle'
				}, {
					field : 'playName',
					title : '玩法名称',
					align : 'center',
					width : '100',
					valign : 'middle'
				}, {
					field : 'haoMa',
					title : '投注号码',
					align : 'center',
					width : '90',
					valign : 'middle',
					formatter : wsFormatter
				}, {
					sortable:true,
					field : 'createTime',
					title : '投注时间',
					width : '150',
					align : 'center',
					formatter : Fui.formatDatetime
				}, {
					field : 'buyZhuShu',
					title : '注数',
					align : 'center',
					valign : 'middle',
					formatter : buyZSFormatter
				},{
					field : 'multiple',
					title : '倍数',
					align : 'center',
					valign : 'middle',
				},{
					field : 'model',
					title : '模式',
					align : 'center',
					width : '50',
					valign : 'middle',
					formatter : modeFormatter,
					pageSummaryFormat:function(rows,aggsData){
						return "小计:";
					},
					allSummaryFormat:function(rows,aggsData){
						return "总计:";
					}
				},{
					sortable:true,
					field : 'buyMoney',
					title : '投注金额(元)',
					align : 'center',
					width : '50',
					valign : 'middle',
					pageSummaryFormat:function(rows,aggsData){
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.buyMoney != null && row.status <= 4){
								r = r+row.buyMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00"
						}
						return aggsData.buySum ? aggsData.buySum.toFixed(2) : "0.00";
					}
				},{
					sortable:true,
					field : 'winMoney',
					title : '中奖金额(元)',
					align : 'center',
					width : '50',
					valign : 'middle',
					formatter : winMoneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						var r=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.winMoney != null){
								r = r+row.winMoney;
							}
						}
						return r.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00"
						}
						return aggsData.winSum ? aggsData.winSum.toFixed(2) : "0.00";
					}
				} ,{
					field : 'betIp',
					title : '投注IP',
					width : '100',
					align : 'center',
					valign : 'middle',
				} ,{
					field : 'betIp',
					title : 'ip信息',
					width : '100',
					align : 'center',
					valign : 'middle',
					formatter:paramsInfo
				} ,{
					field : 'remark',
					title : '会员备注',
					width : '100',
					align : 'center',
					valign : 'middle'
				},{
					sortable:true,
					field : 'lossMoney',
					title : '亏损金额(元)',
					align : 'center',
					valign : 'middle',
					width : '50',
					formatter : lossMoneyFormatter,
					pageSummaryFormat:function(rows,aggsData){
						buyCountSUm(aggsData)
						var buySum=0,winSum=0,row;
						for(var i=rows.length-1;i>=0;i--){
							row=rows[i];
							if(row.buyMoney != null && row.status < 4){
								buySum = buySum+row.buyMoney;
							}
							if(row.winMoney!=null && row.status < 4){
								winSum=winSum+row.winMoney;
							}
						}

						var lossSum=buySum-winSum;
						return lossSum.toFixed(2);
					},
					allSummaryFormat:function(rows,aggsData){
						if(!aggsData){
							return "0.00";
						}else if(isNaN(aggsData.buySum)){
							return "0.00";
						}else if(isNaN(aggsData.winSum)){
							return (aggsData.buySum).toFixed(2);
						}


						return aggsData.buySum-aggsData.winSum ? (aggsData.buySum-aggsData.winSum).toFixed(2) : "0.00";
					}
				}<c:if test="${version eq 4}">,{
					field : 'roomName',
					title : '房间名称',
					width : '120',
					align : 'center',
					valign : 'middle',
					formatter : roomIdFormatter
				}</c:if>
				,{
					field : 'status',
					title : '状态',
					width : '100',
					align : 'center',
					valign : 'middle',
					formatter : statusFormatter
				}],
			onLoadSuccess:function (data){
				if ($('.no-records-found').length){
					$('#buyAccountCountSumMoneny').css('display','none')
				}
			}

		});
		function buyCountSUm(aggsData){
			$(function (){
				if (aggsData){
					$('#buyAccountCountSumMoneny').css('display','inline-block')
					if('buyAccountCount' in aggsData && aggsData.buyAccountCount != 0){
						$('#buyAccountCount').parents('span').css('display','inline-block')
						$('#buyAccountCount').text(aggsData.buyAccountCount)
					}else{
						$('#buyAccountCount').parents('span').css('display','none')
					}
					if('buySum' in aggsData && aggsData.buySum != 0){
						$('#buySum').parents('span').css('display','inline-block')
						$('#buySum').text(aggsData.buySum)
					}else{
						$('#buySum').parents('span').css('display','none')
					}
					if(!(('buyAccountCount' in aggsData) && ('buySum' in aggsData))){
						$('#buyAccountCountSumMoneny').css('display','none')
					}
				}else {
					$('#buyAccountCountSumMoneny').css('display','none')
				}
			})
		}

		function initParams(){
			var j = ${czGroup};
			var searchCode = '${param.code}';
			var col = '<option value="" ';
			if(!searchCode){
				col +=' selected="selected"';
			}
			col +='>全部彩种</option>';

			col+='<option value="common" ';
			if(searchCode == 'common'){
				col +=' selected="selected"';
			}
			col+='>官方彩</option>';
			col+='<option value="sysNoSF" ';
			if(searchCode == 'sysNoSF'){
				col +=' selected="selected"';
			}
			col+='>系统彩(不含系统六合)</option>';
			col+='<option value="sysAll">系统彩(含系统六合)</option>';
			if('${replaceFont}'.length){
				col  = col.replace(/系统彩/g,'${replaceFont}');
				col  = col.replace(/系统/g,'${replaceFont}');
			}
			for(var i in j){
				if(j[i].code!='LHC'){
					col+='<option value="'+j[i].code+'"';
					if(searchCode == j[i].code){
						col +=' selected="selected"';
					}
					col+='>'+j[i].name+'</option>';
				}
			}

			$('#game_record_lottery_type').html(col);
		}
		function accountFormatter(value, row, index) {
			if(${warnFlag} && row.abnormalFlag==1){
				return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.accountId,'" title="查看详情"><span class="text-danger">',value,'</span></a>&nbsp;<span class="label label-danger">警</span>' ].join('');
			}
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.accountId,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
		}

		function buyZSFormatter(value,row,index){
			if(row.lotType == 66 || row.lotType == 12){	//暂时只需要考虑lhc和sfc,后续添加第一版本和第三版本
				if(row.status == 2 && value > 1){//有中奖才需要计算注数
					return ['<a class="open-dialog" href="${base}/agent/gameRecord/strToArray.do?orderId='+row.orderId+'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');;
				}
			}
			return value;
		}

		function wsFormatter(value, row, index) {
			var text = value;
			if(value!=null && value.length>10){
				text= value.substring(0, 10)+"...";
			}else{
				text= value;
			}
			return "<a href='#' tips='"+row.haoMa+"' onmouseover='tip.start(this)'>"+text+"</a>";
		}
		function paramsInfo(value,row,index){
			var text = value;
			var sText = '请稍等'
			if (text !=null && text !=' '){
				//http://whois.pconline.com.cn/jsFunction.jsp?callback=jsShow&ip=117.150.74.71 函数
				//https://whois.pconline.com.cn/ipJson.jsp?json=true&ip=
				$.ajax({
					type : "get",
					url:'https://whois.pconline.com.cn/ipJson.jsp?json=true&ip='+value,
					success:function(data){
						console.log(data)
						try{
							if ('addr' in data){
								if (data.addr !=' '){
									sText = data.addr
									return "<a href='#' >"+text+"</a>"
								}else{
									if (data.pro !=' '){
										sText = data.pro
										return  "<a href='#' >"+text+"</a>"
									}else {
										sText = '未知地址'
									}
								}
							}
						}catch (e) {
							console.log(e)
						}

					},
					error:function(res){
						throw new Error(res+value)
						// console.log(res+value)
					}

				})
			}

		}
		function orderFormatter(value, row, index){
			if(value){
				var s= '<a class="open-dialog" href="${base}/agent/gameRecord/showLotteryOrderDesc.do?orderId='+value+'">'+value;
				if(row.zhuiHao!='1'){
					s=s+"(追)";
				}
				if(row.jointPurchase!='1'){
					s=s+"(合)";
				}
				s=s+'</a>';
				return s;
			}else{
				if(row.zhuiHao!='1'){
					value=value+"(追)";
				}
				if(row.jointPurchase!='1'){
					value=value+"(合)";
				}
				return value;
			}
		}

		function czFormatter(value, row, index) {
			return cz(row.lotCode);
		}

		function roomIdFormatter(value,row,index){
			if(!value){
				return "-";
			}else{
				return value;
			}
		}

		function modeFormatter(value, row, index) {
			var col = '';
			switch(value){
				case 1:
					col = '元';
					break;
				case 10:
					col = '角';
					break;
				case 100:
					col = '分';
					break;
			}
			return col;
		}

		function winMoneyFormatter(value,row,index){
			if(row.status == 8){	//为和局时
				return '退还本金';
			}
			return value;
		}

		function lossMoneyFormatter(value,row,index){
			var buy=row.buyMoney;
			var win=row.winMoney;

			if (isNaN(win)) {
				return "未开奖";
			}
			var loss=buy-win;
			return loss.toFixed(2);
		}

		function statusFormatter(value, row, index) {
			var col = '';
			switch(row.status){
				case 1:
					col = '<span class="label label-primary" >等待开奖</span>';
					if(row.tzStatus == 1){
						col=col+'<div><a class="todo-ajax" href="${base}/agent/gameRecord/awardOrder.do?orderId='+row.orderId
								+"&lotCode="+row.lotCode+"&account="+row.account+'" title="确定要将订单['+row.orderId+']派奖？"><i class="glyphicon glyphicon-usd"></i>手动补派奖</a></div>';
					}
					if("${bcChgRole}" === "on"){
						col=col+'<div><a class="open-dialog" href="${base}/agent/gameRecord/showLotteryOrderDesc.do?orderId='+row.orderId+'">改单</a></div>';
					}
					break;
				case 2:
					col = '<span class="label label-success" >已中奖</span>';
					break;
				case 3:
					col = '<span class="label label-danger" >未中奖</span>';
					break;
				case 4:
					col = '<span class="label label-info" >撤单</span>';
					break;
				case 5:
					col = '<span class="label label-success">派奖回滚成功</span>';
					break;
				case 6:
					col = '<span class="label label-warning">回滚异常的</span>';
					break;
				case 7:
					col = '<span class="label label-warning">开奖异常</span>';
					break;
				case 8:
					col = '<span class="label label-success">和局</span>';
					break;
				case 9:
					col = '<span class="label label-success">合买失效</span>';
					break;
				case 10:
					col = '<span class="label label-success">和局中奖</span>';
					break;
			}
			return col;
		}

		function cz(obj){
			var lotName = '';
			var j = ${czGroup};
			for(var i in j){
				if(j[i].code == obj){
					lotName = j[i].name;
				}
			}
			return lotName;
		}
	});
</script>

<script type="text/javascript">
	var tip={$:function(ele){
			if(typeof(ele)=="object")
				return ele;
			else if(typeof(ele)=="string"||typeof(ele)=="number")
				return document.getElementById(ele.toString());
			return null;
		},
		mousePos:function(e){
			var x,y;
			var e = e||window.event;
			return{x:e.clientX+document.documentElement.scrollLeft-200,
				y:e.clientY+document.documentElement.scrollTop-120};
		},
		start:function(obj){
			// debugger;
			var self = this;
			var t = self.$("mjs:tip");
			obj.onmousemove=function(e){
				var mouse = self.mousePos(e);
				t.style.left = mouse.x + 10 + 'px';
				t.style.top = mouse.y + 10 + 'px';
				t.innerHTML = obj.getAttribute("tips");
				t.style.display = '';
			};
			obj.onmouseout=function(){
				t.style.display = 'none';
			};
		}
	}
</script>

<style type="text/css">
	a,a:visited{color:#3366cc;text-decoration:none;}
	a:hover{color:#f60;text-decoration:underline;}
	.tip{
		width: 200px;
		padding: 8px;
		background: #e32b1dd4;
		color: white;
		font-size: 16px;
		font-weight: bold;
		border-radius: 5px;
	}
	}
</style>
