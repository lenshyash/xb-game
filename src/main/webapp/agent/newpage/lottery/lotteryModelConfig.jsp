<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-inline">
		<div class="form-group">
			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="彩种名称">
			</div>
			<div class="input-group">
				<select class="form-control" name="type">
					<option value="">选择游戏类型</option>
					<c:forEach items="${cpType}" var="type">
						<option value="${type.code }">${type.name }</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<button class="btn btn-primary fui-date-search">查询</button>
		<div style="color: red;" class="platform">
			注意：
			1、中奖百分比指中奖金额是总投注的百分之多少，如设置70%，会员总投注100，总中奖金额则小于或等于70；
			<br>
			2、投注会员数量太少请勿开启百分比模式，会员可能一直不中奖
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color: red;" class="platform">备注：独立设置的彩票开奖模式为第一优先级，如未设置则按系统设置中对应的开关配置进行开奖
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/lotteryModel/list.do',
		columns : [{
			field : 'lotName',
			title : '彩种名称',
			align : 'center',
			width : '80',
		},
		{
			field : 'lotCode',
			title : '彩种编号',
			align : 'center',
			width : '80'
		},
		{
			field : 'lotteryMode',
			title : '彩种模式 ',
			align : 'center',
			width : '80',
			formatter:lotteryModelbFormatter
		},{
			field : 'percentage',
			title : '百分比',
			align : 'center',
			width : '80',
			formatter:bfbFormatter
		},{
			title : '操作',
			align : 'center',
			width : '80',
			formatter : operateFormatter
		}]
	});
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function lotteryModelbFormatter(value, row, index) {
		if(value==1){
			return '<span class="label label-success" >随机</span>';
		}else if(value==2){
			return '<span class="label label-info" >百分比</span>';
		}else if(value==3){
			return '<span class="label label-warning">正式站推送</span>';
		}
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/lotteryModel/modify.do?id=',row.lotId,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a> ' ]
				.join('');
	}
});
</script>