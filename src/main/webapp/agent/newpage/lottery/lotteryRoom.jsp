<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<%--<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="关键字">
			</div>
			<button class="btn btn-primary">查询</button> --%>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/cp/lotRoom/addRoom.do">新增房间</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/cp/lotRoom/addRobot.do">新增机器人</button>
			<%--<button class="btn btn-primary open-dialog cached" url="${base}/agent/cp/lotRoom/initDataInfo.do">初始化通知信息</button> --%>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotRoom/list.do',
		columns : [ {
			field : 'name',
			title : '房间名称',
			align : 'center',
			valign : 'middle'
	/* 	}, {
			field : 'brief',
			title : '房间简介',
			align : 'center',
			valign : 'bottom',
			formatter : briefFormatter */
		}, {
			field : 'lotType',
			title : '所属彩种',
			align : 'center',
			valign : 'middle',
			formatter : getLotteryCodeName
		}, {
			field : 'status',
			title : '房间状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		},{
			field : 'betStatus',
			title : '投注状态',
			align : 'center',
			valign : 'middle',
			formatter : betStatusFormatter
		},{
			field : 'robotStatus',
			title : '机器人状态',
			align : 'center',
			valign : 'middle',
			formatter : robotStatusFormatter
		},{
			field : 'sortNO',
			title : '排序',
			align : 'center',
			valign : 'middle'
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotRoom/updateStatus.do?id="+row.id+"&type=1&status="});
	}
	function betStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotRoom/updateStatus.do?id="+row.id+"&type=2&status="});
	}
	function briefFormatter(value,row,index){
		if(value && value.length>30){
			return value.substring(0,30) + "...";
		}else{
			return value;
		}
	}
	function robotStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/cp/lotRoom/updateStatus.do?id="+row.id+"&type=3&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/cp/lotRoom/modify.do?id='+row.id+'" title="修改">修改</a>  ',
				'<a class="open-dialog" href="${base}/agent/cp/lotRoom/roomPeiLv.do?id='+row.id+'&lotType='+row.lotType+'" title="赔率设置"> 赔率 </a>',
				'<a class="open-dialog" href="${base}/agent/cp/lotRoom/roomRobot.do?id='+row.id+'" title="机器人设置">机器人</a>&nbsp;',
				'<a class="open-dialog" href="${base}/agent/cp/lotRoom/roomUserOnline.do?id='+row.id+'" title="在线人数">在线人数</a>&nbsp;',
				'<a class="open-dialog" href="${base}/agent/cp/lotRoom/dataInfo.do?id='+row.id+'" title="通知信息配置">通知信息</a>&nbsp;']
				.join('');
	}
	function getLotteryCodeName(value,row,index){
		switch(row.lotCode){
			case "PCEGG":
				return "PC蛋蛋";
			case "JND28":
				return "加拿大28";
			case "BJSC":
				return "北京赛车";
			case "XYFT":
				return "幸运飞艇";
			case "LXYFT":
				return "老幸运飞艇";
			case "AZXY10":
				return "澳洲幸运10";
			case "SFSC":
				return "极速赛车";
			case "SFFT":
				return "极速飞艇";
			case "WFSC":
				return "五分赛车";
			case "WFFT":
				return "五分飞艇";
			case "LBJSC":
				return "老北京赛车";
			case "XSFSC":
				return "新极速赛车";
			case "XWFSC":
				return "幸运赛车";
			case "AHK3":
				return "安徽快三";
			case "FFK3":
				return "极速快三";
			case "GSK3":
				return "甘肃快三";
			case "GXK3":
				return "广西快三";
			case "GZK3":
				return "贵州快三";
			case "HBK3":
				return "湖北快三";
			case "HEBK3":
				return "河北快三";
			case "JSSB3":
				return "江苏骰宝";
			case "JXK3":
				return "江西快三";
			case "SHHK3":
				return "上海快三";
			case "WFK3":
				return "幸运快三";
			case "JPK3":
				return "日本快三";
			case "KRK3":
				return "韩国快三";
			case "HKK3":
				return "香港快三";
			case "AMK3":
				return "澳门快三";
			case "BJK3":
				return "北京快三";
		}
	}
});

</script>
<%--,'<a class="todo-ajax" href="${base}/agent/cp/lotRoom/delRoom.do?id='+row.id+'" title="确定要删除该房间？（注：会同时清空机器人-赔率-通知信息）">删除</a>' --%>