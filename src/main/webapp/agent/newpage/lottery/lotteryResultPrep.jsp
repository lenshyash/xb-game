<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" value="<c:if test="${not empty param.begin}">${param.begin}</c:if><c:if test="${empty param.begin}">${startTime}</c:if>" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" name="qiHao" placeholder="预设期号" value="${param.qiHao}">
				</div>
				<div class="input-group">
					<label class="sr-only" for="czType">彩种类型</label> 
					<select class="form-control" name="lotCode" id="game_record_lottery_type2">
						<option value="">全部彩种</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name=endTime class="form-control fui-date" value="<c:if test="${not empty param.end}">${param.end}</c:if><c:if test="${empty param.end}">${endTime}</c:if>" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
					<input type="text" class="form-control" name="operator" value="${param.operator}" placeholder="预设操作人">
				</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	initParams();
	Fui.addBootstrapTable({
		url : '${base}/agent/cp/lotteryPrep/list.do',
		columns : [ 
		   {
			field : 'lotCode',
			title : '彩种名称',
			align : 'center',
			valign : 'middle',
			formatter : czFormatter
		},{
			field:'qiHao',
			title:'预设期号',
			align:'center',
			valign:'middle'
		},{
			field:'haoMa',
			title:'预设号码',
			align:'center',
			valign:'middle'
		},{
			field:'result',
			title:'形态',
			align:'center',
			valign:'middle',
			
		},{
			field:'createTime',
			title:'预设时间',
			align:'center',
			valign:'middle',
			formatter:Fui.formatDatetime
		},{
			field:'operator',
			title:'预设操作人',
			align:'center',
			valign:'middle'
		}]
	});
	
	function czFormatter(value, row, index) {
		return cz(row.lotCode);
	}
	
	function cz(obj){
		var lotName = '';
		var j = ${czGroup2};
		for(var i in j){
			if(j[i].code == obj){
				lotName = j[i].name;
			}
		}
		return lotName;
	}
	
	function initParams(){
		var j = ${czGroup2};
		
		var searchCode = '${param.code}';
		var col = '<option value="" ';
		if(!searchCode){
			col +=' selected="selected"';
		}
		col +='>全部彩种</option>';
		
		// col+='<option value="common" ';
		// if(searchCode == 'common'){
		// 	col +=' selected="selected"';
		// }
		
		// col+='<option value="sysNoSF" ';
		// if(searchCode == 'sysNoSF'){
		// 	col +=' selected="selected"';
		// }

		if('${replaceFont}'.length){
			col  = col.replace(/系统彩/g,'${replaceFont}');
			col  = col.replace(/系统/g,'${replaceFont}');
		}
		for(var i in j){
			if(j[i].code!='LHC'){
				col+='<option value="'+j[i].code+'"';
				if(searchCode == j[i].code){
					col +=' selected="selected"';
				}
				col+='>'+j[i].name+'</option>';
			}
		}
		
		$('#game_record_lottery_type2').html(col);
	}
	
	
});
</script>