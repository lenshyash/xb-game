<%@ page language="java" pageEncoding="utf-8"%>
<div style="font-size:16px;color:red;">当“系统设置-》网站基本设定-前台访问白名单”开启以后，会禁止国外Ip访问站点，但是如果将国外IP加入该功能，则该ip可以继续访问该站点</div>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/front/white/add.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/front/white/list.do",
		columns : [ {
			field : 'ip',
			title : 'IP',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		}]
	});
	
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/front/white/updateStatus.do?id="+row.id+"&status="});
	}
	
	function operateFormatter(value, row, index) {
		return [ '<a class="todo-ajax" href="${base}/agent/front/white/delete.do?id=',row.id,'" title="确定要删除“',row.ip,'”?">删除</a>' ]
				.join('');
	}
});
</script>