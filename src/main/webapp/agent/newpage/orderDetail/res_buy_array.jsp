<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/cp/lotRoom/saveRobot.do" class="form-submit">
<style type="text/css">
	.panel-body{
	max-height:400px;
	overflow-x:auto; }
	.badge:empty{display:block;}
	.list-group li{float:left;width:50%;}
	.list-group li.win{background-color: #337ab7;color:#ffffff;}
	.list-group li.topWin{background-color: #f9f9f9;}
</style>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">注数列表(*打勾为用户所中奖的号码)</h4>
		</div>
		<div class="modal-body">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">总投注数：${order.buyZhuShu}，中奖注数：${order.winZhuShu}</h3>
					<ul class="list-group" style="margin-bottom:40px;">
						<c:if test="${not empty order.lhcSx}">
							<li class="list-group-item topWin">开奖号码：${order.lotteryHaoMa}</li>
							<li class="list-group-item topWin">开奖生肖：${order.lhcSx}</li>
						</c:if>
						<c:if test="${empty order.lhcSx}">
							<li class="list-group-item topWin" style="width:100%">开奖号码：${order.lotteryHaoMa}</li>
						</c:if>
					</ul>
				</div>
				<div class="panel-body">
					<ul class="list-group">
						<c:forEach items="${resArray}" var="array" varStatus="index">
					    	<li class="list-group-item ${array.isWin?'win':''}">
					    	<c:if test="${array.isWin}"><span class="badge glyphicon glyphicon-ok"></span></c:if>
						 	<c:if test="${!array.isWin}"><span class="badge glyphicon glyphicon-remove"></span></c:if>
					    	<span class="text-danger">${index.index+1}.</span>&nbsp;&nbsp;${array.toArray}</li>
					    </c:forEach>
					</ul>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
</form>