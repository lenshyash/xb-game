<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">体育订单详情</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tr>
					<td class="text-center" colspan="4">订单号:${order.bettingCode}</td>
				</tr>
				<tr>
					<td width="20%" class="text-right">账号</td>
					<td width="30%" class="text-left">${order.memberName }</td>
					<td width="20%"  class="text-right">投注时间</td>
					<td width="30%"><fmt:formatDate pattern="MM月dd日 HH:mm:ss" value="${order.bettingDate}" /></td>
				</tr>
				<tr>
					<td class="text-right">球类</td>
					<td><c:choose>
						<c:when test="${order.sportType==1}">足球</c:when>
						<c:when test="${order.sportType==2}">篮球</c:when>
						<c:otherwise>其他</c:otherwise>
					</c:choose></td>
					<td class="text-right">类型</td>
					<td>${order.typeNames}</td>
				</tr>
				<tr>
					<td class="text-right">下注金额</td>
					<td><fmt:formatNumber pattern="#,##0.00" value="${order.bettingMoney }"/></td>
					<td class="text-right">提交状态</td>
					<td><c:choose>
						<c:when test="${order.bettingStatus==1}"><font color='blue'>待确认</font></c:when>
						<c:when test="${order.bettingStatus==2}">已确认</c:when>
						<c:when test="${order.bettingStatus==3}"><font color='red'>取消<c:if test="${not empty order.statusRemark && order.statusRemark!=''}">(${order.statusRemark})</c:if></font></c:when>
						<c:when test="${order.bettingStatus==4}"><font color='red'>取消</font></c:when>
					</c:choose></td>
				</tr>
				<tr>
					<td class="text-right">结算状态</td>
					<td><c:choose>
						<c:when test="${order.balance==1}"><font color='blue'>未结算</font></c:when>
						<c:when test="${order.balance==2||order.balance==5||order.balance==6}"><font color='green'>已结算</font></c:when>
						<c:when test="${order.balance==3}"><font color='red'>结算失败</font></c:when>
						<c:when test="${order.balance==4}"><font color='red'>比赛腰斩</font></c:when>
					</c:choose></td>
					<td class="text-right">派彩金额</td>
					<td><c:if test="${not empty order.bettingResult }">
					<c:choose>
						<c:when test="${order.bettingResult>0}"><font color='green'>${order.bettingResult }</font></c:when>
						<c:otherwise>${order.bettingResult}</c:otherwise>
					</c:choose>
					</c:if></td>
				</tr>
				<tr>
					<td class="text-right">赛事</td>
					<td colspan="3"><div id="memmnyrd_sport_match_info_divId"></div></td>
				</tr>
			</table>	
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
<script>
requirejs(['jquery'],function(){
	function getMatchInfo(){
		if(${order.mix} != 2){
			return toBetHtml($.parseJSON('${order.remark}'));
		}
		var html = "";
		var arr = $.parseJSON('${order.remark}')
		for(var i=0;i<arr.length;i++){
			if(i != 0){
				html += "<div style='border-bottom:1px #303030 dotted;'></div>";
			}
			html += toBetHtml(arr[i]);
		}
		return html;
	}
	function toBetHtml(item){
		var con = item.con;
		if(con.indexOf("vs") == -1){
			con = '<span class="text-danger">'+ con +'</span>';
		}
		var homeFirst = !(item.homeStrong === false);//主队是否在前
		var scoreStr = "";
		
		
		if(${order.gameTimeType} == 1){
			if(homeFirst){
				scoreStr = "&nbsp;<font color='red'><b>(${order.scoreH}:${order.scoreC})</b></font>";
			}else{
				scoreStr = "&nbsp;<font color='red'><b>(${order.scoreC}:${order.scoreH})</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if(item.half === true && ${order.mix} == 2){
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}
		
		var html  = item.league +"<br/>" + 
					home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
					"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
		var balance = ${order.mix} != 2 ? ${order.balance} : item.balance;
		var bt = ${order.bettingStatus};
		if(balance == 4){
			html = "<s style='color:red;'>" + html+"(赛事腰斩)</s>"
		}else if(bt == 3 || bt == 4){
			html = "<s style='color:red;'>" + html+"(${order.statusRemark})</s>"
		}else if(balance == 2 || balance == 5 || balance == 6){
			var mr = ${order.mix} != 2 ? '${order.result}':item.matchResult;
			if(homeFirst){
				html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
			}else{
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>("+ss[1]+":"+ss[0]+")</font>";
			}
		}			
		return html;
	}
	$("#memmnyrd_sport_match_info_divId").html(getMatchInfo());
});
</script>