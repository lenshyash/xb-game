<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/gameRecord/changeOrder.do" class="form-submit">
<input type="hidden" name="orderId" value="${order.orderId }" />
<input type="hidden" name="lotCode" value="${order.lotCode }" />
<input type="hidden" name="account" value="${order.account }" />
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">六合彩订单详情</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="text-center" colspan="4">订单号:${order.orderId}</td>
					</tr>
					<tr>
						<td width="20%" class="text-right">账号:</td>
						<td width="30%">${order.account }</td>
						<td width="20%" class="text-right">中奖注数:</td>
						<td width="30%">${order.winZhuShu}</td>
					</tr>
					<tr>
						<td class="text-right">下注时间:</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${order.createTime}" /></td>
						<td class="text-right">投注注数:</td>
						<td>${order.buyZhuShu}</td>
					</tr>
					<tr>
						<td class="text-right">彩种:</td>
						<td><c:choose>
							<c:when test="${order.lotCode=='LHC'}">六合彩</c:when>
							<c:when test="${order.lotCode=='TMLHC'}">三分六合彩</c:when>
							<c:when test="${order.lotCode=='SFLHC'}">十分六合彩</c:when>
							<c:when test="${order.lotCode=='WFLHC'}">五分六合彩</c:when>
							<c:when test="${order.lotCode=='HKMHLHC'}">香港马会六合彩</c:when>
							<c:when test="${order.lotCode=='AMLHC'}">澳门六合彩</c:when>
							</c:choose></td>
						<td class="text-right">状态:</td>
						<td><c:choose>
							<c:when test="${order.status==1}"><span class="label label-primary" >等待开奖</span></c:when>
							<c:when test="${order.status==2}"><span class="label label-success" >已中奖</span></c:when>
							<c:when test="${order.status==3}"><span class="label label-danger" >未中奖</span></c:when>
							<c:when test="${order.status==4}"><span class="label label-info" >撤单</span></c:when>
							<c:when test="${order.status==5}"><span class="label label-success">派奖回滚成功</span></c:when>
							<c:when test="${order.status==6}"><span class="label label-warning">回滚异常的</span></c:when>
							<c:when test="${order.status==7}"><span class="label label-warning">开奖异常</span></c:when>
							<c:when test="${order.status==8}"><span class="label label-success">和局</span></c:when>
						</c:choose></td>
					</tr>
					<tr>
						<td class="text-right">期号:</td>
						<td>${order.qiHao}</td>
						<td class="text-right">投注总额:</td>
						<td><input type="number" class="form-control" id="orderBuyMoney" name="money" value="${order.buyMoney }" placeholder="小于两位的小数"></td>
					</tr>
					<tr>
						<td class="text-right">玩法:</td>
						<td>${order.playName}</td>
						<td class="text-right">赔率:</td>
						<td>${order.peilv}</td>
					</tr>
					<tr>
						<td class="text-right">开奖号码:</td>
						<td>${order.lotteryHaoMa}<br>
							${order.lhcSx}
						</td>
						<td class="text-right">中奖金额:</td>
						<td><c:if test="${order.status==8}"><fmt:formatNumber pattern="#,##0.00" value="${order.buyMoney }"/></c:if>
							<c:if test="${order.status!=8}">
								<c:if test="${not empty order.winMoney}"><fmt:formatNumber pattern="#,##0.000" value="${order.winMoney }"/></c:if>
								<c:if test="${empty order.winMoney}">0.00</c:if>
							</c:if></td>
					</tr>
					<c:if test="${!isMulti &&( order.playCode=='tm_b' || order.playCode=='zm_b')}"><tr>
						<td class="text-right" id="mutil">反水状态:</td>
						<td><c:choose>
							<c:when test="${order.rollBackStatus==1||order.rollBackStatus==2}">还未反水</c:when>
							<c:when test="${order.rollBackStatus==4}">已经反水</c:when>
							<c:when test="${order.rollBackStatus==3}">反水回滚</c:when>
						</c:choose></td>
						<td class="text-right">反水金额:</td>
						<td><c:if test="${not empty order.rollBackMoney}"><fmt:formatNumber pattern="#,##0.000" value="${order.rollBackMoney}"/></c:if>
							<c:if test="${empty order.rollBackMoney}">还未反水</c:if></td>
					</tr></c:if>
					<tr>
						<td class="text-right">盈亏:</td>
						<td colspan="3"><c:if test="${order.status!=2 && order.status!=3 && order.status!=6}">0.00</c:if>
							<c:if test="${order.status==2 || order.status==3 || order.status==6}">
								<c:if test="${not empty order.yingKui}"><fmt:formatNumber pattern="#,##0.000" value="${order.yingKui }"/></c:if>
								<c:if test="${empty order.yingKui}">0.00</c:if>
							</c:if></td>
					</tr>
					
					<tr>
						<td class="text-right">赔率玩法:</td>
						<td><select name="markSixId" class="form-control">
							<c:forEach items="${sixList}" var="six"><option value="${six.id }" <c:if test="${(not empty markSix && markSix==six.id)}">selected</c:if> >${six.name }</option></c:forEach>
						</select></td>
					</tr>
					<tr>
						<td class="text-right" style="line-height:65px;">下注内容:</td>
						<td class="text-right" colspan="3">
							<textarea class="form-control" rows="3" name="content">${order.haoMa}</textarea>
						</td>
					</tr>
					<tr><td class="text-center info" colspan="4">
						<span class="glyphicon glyphicon-info-sign"></span> 温馨提示:(中奖号码,既有本命年(0尾),又有非本命年(非0尾) 可能会出现2种赔率的情况)
					</td>
					</tr>
					<c:if test="${bcChgRole == 'on'}">
					<tr>
						<td class="text-right">修改说明:</td>
						<td colspan="3"><span class="text-danger">修改注单不会修改注单赔率，请注意修改前后的玩法赔率值，玩法赔率可参考彩票赔率设置V2。请先修改下注内容，如出现号码修改不合法请参考彩票赔率设置V2对赔率玩法进行修改，谨慎使用！！</span></td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<c:if test="${bcChgRole == 'on'}">
				<button class="btn btn-primary">改单</button>
			</c:if>
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
</form>