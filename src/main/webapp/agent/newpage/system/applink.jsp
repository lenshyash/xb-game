<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/applink/add.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<h3><span class="label label-info">温馨提示:未设置模板独立链接，会采用站点默认配置链接</h3>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url :'${base}/agent/system/applink/list.do',
		columns : [ {
			field : 'folder',
			title : '模板号',
			align : 'center',
			width : '200',
			valign : 'middle'

		}, {
			field : 'iosDlUrl',
			title : 'IOSApp下载地址',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter:hideLinkFormatter
		}, {
			field : 'androidDlUrl',
			title : 'AndroidApp下载地址',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter:hideLinkFormatter
		}, {
			field : 'iosQrUrl',
			title : 'IOSApp二维码地址',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter:hideLinkFormatter
		}, {
			field : 'androidQrUrl',
			title : 'AndroidApp二维码地址',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter:hideLinkFormatter
		}, {
			field : 'kfUrl',
			title : '在线客服url地址',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter:hideLinkFormatter
		},{
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		},  {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		}]
	});
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/applink/zhopenClose.do?id="+row.id+"&modelStatus="});
	}
	function hideLinkFormatter (value, row, index) {
		if(!value){
			return "";
		}
		if(value.length > 25){
			value = value.substring(0,25)+"...";
		}
		return value;
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/applink/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/applink/delete.do?id=',row.id,'" title="确定要删除该记录吗？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
});
</script>