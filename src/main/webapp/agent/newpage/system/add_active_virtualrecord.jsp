<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/active/saveVirtualrecord.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增虚拟记录</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="10%" class="text-right media-middle">记录条数：</td>
						<td width="20%" class="text-left">
							<input type="text" name="records" class="form-control"/>
						</td>
					</tr>
					<tr>
						<td width="10%" class="text-right media-middle">活动名称：</td>
						<td width="20%" class="text-left">
							<select name="activeType" class="form-control">
								<option value="1" selected></option>
							</select>
						</td>
						<td width="15%" class="text-right media-middle">活动类型：</td>
						<td width="20%" class="text-left">
							<select name="activeType" class="form-control">
								<option value="1" selected>幸运大转盘</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="%10" class="text-right media-middle">奖项类型：</td>
						<td width="20%" class="text-left">
							<select name="award_type" class="form-control">
								<option value="1" selected>不中奖</option>
								<option value="2" selected>现金</option>
								<option value="3" >奖品</option>
								<option value="4" >积分</option>
							</select>
						</td>
						<td width="10" class="text-right media-middle">奖项名称：</td>
						<td>	
							<input type="text" name="productName" class="form-control"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>