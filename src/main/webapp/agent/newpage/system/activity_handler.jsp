<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/agentactivity/handlerRecord.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" name="id" value="${record.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">优惠活动申请记录</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">申请人：</td>
						<td class="text-left">${record.account }</td>
					</tr>
					<tr>
						<td class="text-right">活动名称：</td>
						<td class="text-left">${record.activityName }</td>
					</tr>
					<tr>
						<td class="text-right">申请选项：</td>
						<td class="text-left">${record.activityOption }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">处理结果：</td>
						<td class="text-left"><select name="status" class="form-control">
								<option value="2" selected>成功</option>
								<option value="3">失败</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">处理说明：</td>
						<td class="text-left"><textarea id="remark" name="remark" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>