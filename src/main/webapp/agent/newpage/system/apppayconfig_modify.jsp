<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/apppayconfig/editSaveNew.do" class="form-submit">
<div class="modal-dialog"><input name="id" type="hidden" value="${apconfig.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改APP资料</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">版本号：</td>
						<td class="text-left"><input type="text" name="version" class="form-control" value="${apconfig.version}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">名称：</td>
						<td class="text-left"><input type="text" name="name" class="form-control" value="${apconfig.name}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">URL：</td>
						<td class="text-left"><input type="text" name="url" class="form-control" value="${apconfig.url}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="status" class="form-control">
							<option value="1"<c:if test="${apconfig.status==1 }">selected</c:if>>开启</option>
							<option value="0"<c:if test="${apconfig.status==0 }">selected</c:if>>关闭</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>