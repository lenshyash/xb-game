<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/sign/saveRule.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增签到规则</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">连续签到天数：</td>
						<td class="text-left"><input type="text" name="days" class="form-control digits" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">奖励积分：</td>
						<td class="text-left"><input name="score" class="form-control digits" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">规则类型：</td>
						<td class="text-left">
						<!-- <select name="signType" ><option value="1">循环赠送(取最大效益)</option><option value="2">硬性要求(天数相等)</option> </select> -->
						<input type="radio" value="1" name="signType" checked>循环赠送(取最大效益)
							<input type="radio"  name="signType" value="2" >硬性要求(天数相等)
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">规则类型：</td>
						<td class="text-left">
							<input type="radio" name="signClear" value="1" checked>不清零
							<input type="radio" name="signClear"  value="2" >清零
						 </td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>