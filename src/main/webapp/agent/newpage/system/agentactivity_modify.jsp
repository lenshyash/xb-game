<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/agentactivity/eidtSaveNew.do" class="form-submit" id="agent_artivity_edit_formId">
<div class="modal-dialog modal-lg"><input name="id" type="hidden" value="${activity.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改优惠活动</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">标题：</td>
						<td width="35%" class="text-left"><input type="text" name="title" class="form-control" value="${activity.title}"/></td>
						<td width="15%" class="text-right media-middle">标题图片：</td>
						<td width="35%" class="text-left"><input type="text" name="titleImgUrl" value="${activity.titleImgUrl}"class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="updateTime" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${activity.updateTime}" />"placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="overTime" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${activity.overTime}" />"placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="modelStatus" class="form-control">
							<option value="2"<c:if test="${activity.modelStatus==2 }">selected</c:if>>启用</option>
							<option value="1"<c:if test="${activity.modelStatus==1 }">selected</c:if>>禁用</option>
						</select></td>
						<td class="text-right media-middle">排序：</td>
						<td class="text-left"><input type="text" name="paiXu" value="${activity.paiXu}"class="form-control number"/></td>
					</tr>
					<tr>
						<c:if test="${not empty dlst}">
						<tr>
							<td class="text-right media-middle">绑定模板：</td>
							<td class="text-left">
								<select name="folderCode" class="form-control">
									<option value="">共用</option>
									<c:forEach items="${dlst }" var = "d">
										<option value="${d.folder }" <c:if test="${activity.folderCode eq d.folder }">selected="selected"</c:if>> ${d.folder }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:if>
					</tr>
					<tr>
						<td class="text-right media-middle">自助申请：</td>
						<td class="text-left"><select name="applyFlag" class="form-control">
							<option value="1"<c:if test="${activity.applyFlag==1}">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${activity.applyFlag==2}">selected</c:if>>启用</option>
						</select></td>
						<td class="text-right media-middle">申请选项：</td>
						<td class="text-left"><textarea name="applySelected" class="form-control" placeholder="英文逗号隔开每个活动选项 例：投注100返1元,投注300返3元">${activity.applySelected}</textarea> </td>
					</tr>
						<td class="text-left" colspan="4"><script name="content" id="agent_artivity_edit_editor" type="text/plain" style="width:860px;height:300px;"></script></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div><div class="hidden edit_content">${activity.content}</div>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#agent_artivity_edit_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_artivity_edit_editor');
	})
	var ue = UE.getEditor('agent_artivity_edit_editor');
	ue.ready( function( editor ) {
		ue.setContent($form.find(".edit_content").html());
		$form.find(".edit_content").remove();
	});
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.content=ue.getContent();
		return params;
	});
})
</script>