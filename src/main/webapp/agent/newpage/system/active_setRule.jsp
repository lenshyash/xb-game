<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/active/saveAward.do" class="form-submit" closeTabId="active_set_rule_id" id="active_set_rule_formId_id">
<div class="panel panel-default"><input type="hidden" name="activeId" value="${active.id }">
	<div class="panel-heading">
		<span>${active.activeName }设置</span>
	</div>
	<div class="panel-body">
		<table class="table table-bordered">
			<tbody>
				<tr style="background-color: #f5f5f5;">
					<td width="15%" class="text-center">配置项目</td>
					<td width="35%">配置值</td>
					<td width="50%">效果图</td>
				</tr>
				<tr>
					<td class="text-center media-middle">奖项数量</td>
					<td class="text-left media-middle"><select class="form-control awardCount"></select></td>
					<td class="text-left"><canvas class="awardImgCav" height="160"></canvas></td>
				</tr>
			</tbody>
		</table>
		<div class="text-center">
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
requirejs(["text!${base}/agent/system/product/combo.do",'${base}/agent/js/active_setRule.js','jquery'],function(products,setRule){
	$.get("${base}/agent/system/active/awards.do?activeId=${active.id}",function(awardDatas){
		setRule.render(products,awardDatas);
	},"json");
});
</script>