<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<button id='registerConfig_huiYuan_tabId' class="btn btn-primary open-tab" url="${base}/agent/system/registerconf/index.do?platform=2">代理注册</button>
</div>
<table id="registerConfig_huiYuan_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'registerConfig_huiYuan_datagrid_tb',
		sidePagination: 'client',
		pagination:false,
		url :'${base}/agent/system/registerconf/newList.do?platform=1',
		columns : [ {
			field : 'name',
			title : '属性名称',
			align : 'center',
			width : '200',
			valign : 'middle',
		}, {
			field : 'show',
			title : '显示',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'show')}
		}, {
			field : 'required',
			title : '必输',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'required')}
		}, {
			field : 'validate',
			title : '验证',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'validate')}
		}, {
			field : 'unique',
			title : '唯一',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'unique')}
		}  ]
	});
	function showFormatter(value, row, field) {
		return Fui.statusFormatter({val:value,onText:"是",offText:"否",url:"${base}/agent/system/registerconf/updateProp.do?id="+row.id+'&prop='+field+"&value="});
	}
});
</script>