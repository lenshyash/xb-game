<%@ page language="java" pageEncoding="utf-8"%>
<div id="sys_registerconfig_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="huiyuan">会员注册</a></li>
		<li><a href="#" data-toggle="tab" oname="daili">代理注册</a></li>
	</ul>
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/registerconfig.js?v=2.112'],function($registerconfig){
	$registerconfig.render();
});
</script>