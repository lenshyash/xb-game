<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="user_manager_form_id">
		<div class="form-group">
			<div class="form-inline">
				<button class="btn btn-primary open-dialog" url="${base}/agent/system/user/add.do">新增</button>
				
				<input type="text" class="form-control" name="account" placeholder="用户账号" style="width:120px;">

							<select id="a_permission_groupId" name="groupId" class="form-control">
				<option value="0">请选择组别</option>
				<c:forEach items="${groups2 }" var="at"><option value="${at.id}">${at.name}</option></c:forEach>
			</select>
		
			<select class="form-control" name="status">
				<option value="">状态</option>
				<option value="1">禁用</option>
				<option value="2">启用</option>
			</select>
		
				
				<button class="btn btn-primary">查询</button>
			</div>
		</div>
</form>
<table id="usermanager_table_id"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var groups=${groups};
	var $form=$("#user_manager_form_id");
	Fui.addBootstrapTable({
		id : 'usermanager_table_id',
		url : '${base}/agent/system/user/newList.do',
		queryParams : function(params){
			var keyName = $form.find("[name='keyName']").val()
			,value = $form.find("[name='keyword']").val();
		params[keyName] = value;
			return params
		},
		columns : [ {
			field : 'account',
			title : '用户账号',
			align : 'center',
			valign : 'middle',
		},  {
			field : 'groupId',
			title : '组别',
			align : 'center',
			valign : 'middle',
			formatter : groupFormatter
		},{
			field : 'agentName',
			title : '创建人',
			align : 'center',
			valign : 'middle',
		},{
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle',
		}, {
			field : 'accountStatus',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function groupFormatter(value, row, index) {
		if (!groups) {
			return "";
		}
		for (var i = 0; i < groups.length; i++) {
			if (value == groups[i].id) {
				return groups[i].name;
			}
		}
		return "未分组";
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/user/updStatus.do?id="+row.id+"&accountStatus="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/system/user/modify.do?id='+row.id+'" title="修改">修改</a> '
				+'<a class="open-dialog" href="${base}/agent/system/user/modifyPwd.do?id='+row.id+'" title="修改密码">修改密码</a> '
		+'<a class="todo-ajax" href="${base}/agent/system/user/delete.do?id='+row.id+'" title="确定要删除“'+row.account+'”？">删除</a>';
	}
});
</script>