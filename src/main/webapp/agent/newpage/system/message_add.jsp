<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/message/send.do" class="form-submit" id="message_add_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">发送站内信</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">接收人：</td>
						<td class="text-left"><select name="type" class="form-control">
								<option value="1" selected>个人</option>
								<option value="2">群发</option>
							</select></td>
					</tr>
					<tr class="accountT1">
						<td class="text-right media-middle">接收者：</td>
						<td class="text-left"><input type="text" class="form-control" name="account" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">站内信标题：</td>
						<td class="text-left"><input type="text" class="form-control" name="title" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">站内信内容：</td>
						<td class="text-left"><textarea class="form-control" name="message"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#message_add_form_id");
	$form.on("change","select[name='type']",function(){
		if($(this).val()==2){
			$form.find(".accountT1").hide();
		}else{
			$form.find(".accountT1").show();
		}
	});
});
</script>