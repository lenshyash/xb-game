<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/chat/import.do" class="form-submit" id="level_import_form_id">
	<div class="modal-dialog">
		<input type="hidden" value="${token }" name="token">
		<input type="hidden" value="${platformId }" name="platformId">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
				<h4 class="modal-title">成员导入</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td width="30%" class="text-right media-middle">聊天室编号：</td>
							<td class="text-left">${platformId}</td>
						</tr>
						<tr>
							<td width="30%" class="text-right media-middle">已有会员：</td>
							<td class="text-left"><span class="icoFontlist">${accountStr!=''?accountStr:'暂无会员'}</span></td>
						</tr>
						<tr id="commontr">
							<td class="text-right media-middle">会员,代理混合：</td>
							<td class="text-left"><textarea cols="120" rows="15"
									class="form-control" name="members"
									placeholder="可以同时多个,空格、逗号或者回车隔开"></textarea></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default fui-close">关闭</button>
				<button class="btn btn-primary">保存</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	requirejs([ 'jquery' ], function() {
		var $form=$("#level_import_form_id");
		/*var $selType = $form.find("[name='importType']");
		 $selType.change(function(){
			var type = $(this).val();
			if(type == 2){
				$("#dltr").show();
				$("#commontr").hide();
			}else{
				$("#dltr").hide();
				$("#commontr").show();
			}
		}); */
	});
</script>

<style>
.icoFontlist:hover
{
    white-space:normal; 
    overflow:auto;

}
.icoFontlist{
    display: inline-block;
	  max-width:365px; 
	  word-wrap:break-word; 
	  text-overflow:ellipsis; 
	  white-space:nowrap; 
	  overflow:hidden; 
}
</style>