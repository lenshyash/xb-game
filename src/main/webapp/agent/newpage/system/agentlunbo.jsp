<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentlunbo/add.do">新增</button>
	</div>
</div>
<table id="agentlunbo_datagrid_tb"></table>
<h3><span class="label label-info">温馨提示:手机轮播图片建议规格为宽度:375px,高度160px;手机端轮播图片没配置将默认读取电脑端轮播图片!</h3>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'agentlunbo_datagrid_tb',
		url :'${base}/agent/system/agentlunbo/list.do',
		columns : [ {
			field : 'title',
			title : '轮播标题',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'code',
			title : '轮播类型',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : articleType
		}, {
			field : 'titleImg',
			title : '轮播图片地址',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			field : 'titleUrl',
			title : '轮播链接地址',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			field : 'modelStatus',
			title : '活动状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'updateTime',
			title : '发布时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'overTime',
			title : '过期时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	//格式化公告类型
	function articleType(value){
		switch(value){
		case 1:
			return '首页轮播';
		case 5:
			return '手机轮播';
		case 2:
			return '头部图片';
		case 3:
			return '新闻图片';
		case 4:
			return '导航图片';
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/agentlunbo/zhopenClose.do?id="+row.id+"&modelStatus="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/agentlunbo/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/agentlunbo/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	function contentFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>15){
			return value.substr(0, 15)+'...';
		}else{
			return value;
		}
	}
});
</script>