<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/group/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改组别</h4>
		</div><input name="id" type="hidden" value="${group.id}">
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">组别名称：</td>
						<td width="35%" class="text-left"><input name="name" value="${group.name }" class="form-control required" type="text"></td>
					</tr>
					<c:if test="${isSuperAgent}">
					<tr>
						<td class="text-right media-middle">类型：</td>
						<td class="media-middle"><label class="radio-inline"><input type="radio" name="type" value="1"<c:if test="${group.type==1}">checked</c:if>>对租户隐藏</label>&nbsp;
						<label class="radio-inline"><input type="radio" name="type" value="2"<c:if test="${group.type==2}">checked</c:if>>租户可编辑</label>&nbsp;
						<label class="radio-inline"><input type="radio" name="type" value="3"<c:if test="${group.type==3}">checked</c:if>>租户只读</label></td>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
