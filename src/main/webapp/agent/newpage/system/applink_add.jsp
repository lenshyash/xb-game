<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/applink/addSaveNew.do" class="form-submit" id="agent_article_add_formId">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<c:if test="${not empty dlst}">
					<tr>
						<td class="text-right media-middle">绑定模板：</td>
						<td class="text-left" colspan='3'>
							<select name="folder" class="form-control">
								<c:forEach items="${dlst }" var = "d">
									<option value="${d.folder }">${d.folder }</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					</c:if>
					<tr>
						<td width="15%" class="text-right media-middle">IOS版本app下载地址：</td>
						<td width="35%" class="text-left"><input type="text" name="iosDlUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">Android版本app下载地址：</td>
						<td width="35%" class="text-left"><input type="text" name="androidDlUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">IOS版本app二维码地址：</td>
						<td width="35%" class="text-left"><input type="text" name="iosQrUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">Android版本app二维码地址：</td>
						<td width="35%" class="text-left"><input type="text" name="androidQrUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">在线客服url地址：</td>
						<td width="35%" class="text-left"><input type="text" name="kfUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left" colspan='3'><select name="status" class="form-control">
								<option value="2" selected>启用</option>
								<option value="1">禁用</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
/* require(['ueditor_zh'], function(){
	var $form=$("#agent_article_add_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_article_add_editor');
	})
	$form.find("[name='code']").change(function(){
		if($(this).val()==14){
			$form.find(".isShowTc").removeClass("hidden");
		}else{
			$form.find(".isShowTc").addClass("hidden");
		}
	});
	var ue = UE.getEditor('agent_article_add_editor');
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.content=ue.getContent();
		params.modelStatus=$form.find("[name='status']").val();
		return params;
	});
}) */
</script>