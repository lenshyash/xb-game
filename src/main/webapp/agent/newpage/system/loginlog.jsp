<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="login_log_rd_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${startTime} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="输入账号查询">
			</div>
			<input type="text" class="form-control" name="loginOs" placeholder="操作系统">
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary exportBtn" type="button">导出</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime} 23:59:59" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="ip" placeholder="输入操作ip查询">
			</div>
			<input type="text" class="form-control" name="deviceId" placeholder="设备ID">
			<input type="text" class="form-control" name="deviceType" placeholder="设备类型">
		</div>
	</div>
</form>
<div class="hidden">
	<form id="login_log_rd_export_fmId" action="${base}/agent/system/loginLog/export.do" target="_blank" method="post">
		<input type="hidden" name="account"/> 
		<input type="hidden" name="loginOs"/> 
		<input type="hidden" name="ip"/>
		<input type="hidden" name="begin"/>
		<input type="hidden" name="end"/>
	</form>
</div>
<table id="login_log_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'login_log_datagrid_tb',
		url : '${base}/agent/system/loginLog/list.do',
		columns : [ {
			field : 'id',
			title : '编号',
			align : 'center',
			valign : 'bottom'
		}, {
			field : 'account',
			title : '账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'accountType',
			title : '用户类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'content',
			title : '操作记录',
			align : 'center',
			valign : 'middle',
			formatter : remarkFormatter
		}, {
			field : 'accountIp',
			title : 'IP',
			align : 'center',
			valign : 'middle',
			sortable:true
		}, {
			field : 'address',
			title : 'IP地址',
			align : 'center',
			valign : 'middle'
		},  {
			field : 'domain',
			title : '登录域名',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'loginOs',
			title : '操作系统',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'deviceId',
			title : '设备ID',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'deviceType',
			title : '设备类型',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'userAgent',
			title : '请求userAgent',
			align : 'center',
			valign : 'middle',
			formatter : contentFormatter
		}, {
			field : 'createDatetime',
			title : '操作时间',
			align : 'center',
			width : '200px',
			valign : 'middle',
			formatter : Fui.formatDatetime
		} ]
	});
	function remarkFormatter(value, row, index) {
		return [ "<p class='text-danger'>", "</p>" ].join(value);
	}
	
	function contentFormatter(value, row, index) {
		if (value && value.length > 10) {
			return "<div title='"+value+"'>"+value.substring(0, 30) + "...<div>";
		}
		return value;
	}
	
	var typeNames={
		1 : "会员",
		2 : "公司超级管理员",
		3 : "公司管理员",
		4 : "代理",
		5 : "总代理",
		6 : "试玩账号",
		9 : "引导账号"
	};
	function typeFormatter(value, row, index) {
		return [ '<span class="text-primary">', '</span>' ]
				.join(typeNames[value]);
	}
	var $form=$("#login_log_rd_form_id");
	$form.find(".exportBtn").click(function(){
		var $form1=$("#login_log_rd_export_fmId");
		$form1.find("[name='account']").val($form.find("[name='account']").val());
		$form1.find("[name='loginOs']").val($form.find("[name='loginOs']").val());
		$form1.find("[name='ip']").val($form.find("[name='ip']").val())
		$form1.find("[name='begin']").val($form.find("[name='begin']").val())
		$form1.find("[name='end']").val($form.find("[name='end']").val())
		$form1.submit();
	});

});


</script>