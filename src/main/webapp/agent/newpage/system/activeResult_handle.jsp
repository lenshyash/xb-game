<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/active/handlerRecord.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" name="id" value="${record.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">大转盘中奖记录</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">获奖者：</td>
						<td class="text-left">${record.account }</td>
					</tr>
					<tr>
						<td class="text-right">类型：</td>
						<td class="text-left"><c:if test="${record.awardType==2 }">现金</c:if><c:if test="${record.awardType==4 }">积分</c:if><c:if test="${record.awardType==3 }">奖品</c:if></td>
					</tr>
					<tr>
						<td class="text-right">奖品名称：</td>
						<td class="text-left">${record.productName }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">处理结果：</td>
						<td class="text-left"><select name="status" class="form-control">
								<option value="2" selected>成功</option>
								<option value="3">失败</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">处理说明：</td>
						<td class="text-left"><textarea id="remark" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>