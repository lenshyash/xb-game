<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/level/save.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" value="${level.id }" name="id">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加等级</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">等级名称：</td>
						<td class="text-left"><input type="text" name="levelName" value="${level.levelName }" class="form-control" /></td>
					</tr>
					<c:if test="${level.levelDefault!=2 }"><tr>
						<td class="text-right media-middle">存款金额：</td>
						<td class="text-left"><input name="depositMoney" class="form-control" value="${level.depositMoney }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">晋级彩金：</td>
						<td><input type="text" name="giftMoney" class="form-control" value="${level.giftMoney}" placeholder="不填或填0不赠送"/></td>
					</tr></c:if>
					<tr>
						<td class="text-right media-middle">等级图标：</td>
						<td><input type="text" name="icon" class="form-control" value="${level.icon}"placeholder="等级图片网址"/></td>
					</tr>
					
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td class="text-left"><textarea name="remark" class="form-control">${level.remark }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>