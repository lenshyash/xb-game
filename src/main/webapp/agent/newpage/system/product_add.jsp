<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/product/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增商品</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="35%" class="text-right media-middle">商品名称：</td>
						<td class="text-left"><input type="text" name="productName" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">商品面值：</td>
						<td class="text-left"><input type="text" name="price" class="form-control money"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">商品描述：</td>
						<td class="text-left"><textarea name="productDesc" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>