<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/user/updateGroup.do" class="form-submit">
<div class="modal-dialog">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
		<h4 class="modal-title">修改用户</h4>
	</div><input type="hidden" name="id" value="${agentUser.id }">
	<div class="modal-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td width="40%" class="text-right media-middle">用户账号：</td>
					<td class="text-left"><input name="account" class="form-control" type="text" value="${agentUser.account }"readonly></span></td>
				</tr>
				<tr>
					<td class="text-right media-middle">用户分组：</td>
					<td class="text-left"><select name="groupId" class="form-control">
						<c:forEach items="${groups }" var="g"><option value="${g.id }"<c:if test="${agentUser.groupId==g.id }">selected</c:if>>${g.name }</option></c:forEach>
					</select></td>
				</tr>
				<tr>
					<td width="40%" class="text-right media-middle">备注：</td>
					<td class="text-left"><input name="remark" class="form-control" type="text" value="${agentUser.remark }" /></span></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default fui-close">关闭</button>
		<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
