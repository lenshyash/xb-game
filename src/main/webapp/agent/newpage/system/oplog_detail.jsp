<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">日志详情</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">用户名：</td>
						<td class="text-left"><span>${log.account }</span></td>
					</tr>
					<tr>
						<td class="text-right media-middle">日志类型：</td>
						<td class="text-left"><span>${typeName }</span></td>
					</tr>
					<tr>
						<td class="text-right media-middle">操作时间：</td>
						<td class="text-left"><span><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${log.createDatetime}" /></span></td>
					</tr>
					<tr>
						<td class="text-right media-middle">操作IP：</td>
						<td class="text-left"><span>${log.accountIp }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">操作记录：</td>
						<td class="text-left"><textarea class="form-control"><c:out value ="${log.logContent }" escapeXml="true"/></textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">参数：</td>
						<td class="text-left"><textarea class="form-control"><c:out value ="${log.params }" escapeXml="true"/></textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">操作备注：</td>
						<td class="text-left"><textarea class="form-control"><c:out value ="${log.remark }" escapeXml="true"/></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
