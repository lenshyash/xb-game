<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/apppayconfig/add.do">新增</button>
	</div>
</div>
<table id="apppay_config_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'apppay_config_datagrid_tb',
		url : '${base}/agent/system/apppayconfig/list.do',
		columns : [ {
			field : 'version',
			title : '钥匙版本号',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'name',
			title : '钥匙名称',
			align : 'center',
			width : '180',
			valign : 'middle'
		}, {
			field : 'url',
			title : '钥匙URL',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			field : 'status',
			title : '应用开关',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function contentFormatter(value, row, index){
		if(value == null || value ==""){
			return value;
		}
		if(value.length>5){
			return value.substr(0, 5)+'...';
		}else{
			return value;
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,onText:"开启",offText:"关闭",onVal:1,offVal:0,url:"${base}/agent/system/apppayconfig/zhopenClose.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/apppayconfig/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/apppayconfig/delete.do?id=',row.id,'" title="确定要删除“',row.name,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
});
</script>