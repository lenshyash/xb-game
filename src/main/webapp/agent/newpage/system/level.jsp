<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" type="button" url="${base}/agent/system/level/add.do">新增</button>
		<button class="btn btn-primary todo-ajax" type="button" url="${base}/agent/system/level/reStat.do">重新统计成员数量</button>
	</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/system/level/list.do",
		columns : [ {
			field : 'levelName',
			title : '等级名称',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'depositMoney',
			title : '存款金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'giftMoney',
			title : '晋级彩金',
			align : 'center',
			valign : 'middle',
			formatter : giftFormatter
		}, {
			field : 'memberCount',
			title : '成员数量',
			align : 'center',
			valign : 'middle',
			formatter:memCountFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function memCountFormatter(value,row,index){
		if(value && value>0){
			return '<a class="open-tab" href="${base}/agent/member/manager/index.do?level='+row.id+'" title="全部会员管理">'+value+'</a>';
		}
		return value;
	}
	function giftFormatter(value, row, index) {
		if (value && value > 0) {
			return moneyFormatter(value);
		}
		return '不赠送';
	}
	function moneyFormatter(value, row, index) {
		if (value && value > 0) {
			return '<span class="text-danger">'+value+'</span>';
		}
		return value;
	}
	function statusFormatter(value, row, index) {
		if(row.levelDefault==2){
			return "启用";
		}
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/level/updStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		var s='<a class="open-dialog" href="${base}/agent/system/level/modify.do?id='+row.id+'" title="修改">修改</a>  '
			+'<a class="open-dialog" href="${base}/agent/system/level/move.do?id='+row.id+'" title="成员转移"> 成员转移 </a>  '
			+'<a class="open-dialog" href="${base}/agent/system/level/importpg.do?id='+row.id+'" title="成员导入"> 成员导入 </a>  ';
		if(row.levelDefault!=2){
			s=s+'<a class="todo-ajax" href="${base}/agent/system/level/lvlDel.do?id='+row.id+'" title="确定要删除“'+row.levelName+'”？">删除</a>';
		}
		return s;
	}
});
</script>