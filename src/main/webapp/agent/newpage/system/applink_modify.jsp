<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/applink/eidtSaveNew.do" class="form-submit" id="agent_article_edit_formId">
<div class="modal-dialog modal-lg"><input name="id" type="hidden" value="${link.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">模板名称：</td>
						<td width="35%" class="text-left"><input type="text" name="folder" value="${link.folder }" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">IOS版本app下载地址：</td>
						<td width="35%" class="text-left"><input type="text" name="iosDlUrl" value="${link.iosDlUrl }" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">Android版本app下载地址：</td>
						<td width="35%" class="text-left"><input type="text" name="androidDlUrl" value="${link.androidDlUrl }" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">IOS版本app二维码地址：</td>
						<td width="35%" class="text-left"><input type="text" name="iosQrUrl" value="${link.iosQrUrl }" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">Android版本app二维码地址：</td>
						<td width="35%" class="text-left"><input type="text" name="androidQrUrl" value="${link.androidQrUrl }" class="form-control"/></td>
					</tr>
					<tr>
						<td width="15%" class="text-right media-middle">在线客服url地址：</td>
						<td width="35%" class="text-left"><input type="text" name="kfUrl" value="${link.kfUrl }" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left" colspan='3'><select name="status" value="${link.status }"class="form-control">
								<option value="2">启用</option>
								<option value="1">禁用</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
<div class="hidden edit_content">${article.content}</div>
</form>
<script>

   $(function(){
      $("[name='status']").val(${link.status});
   })
/* require(['ueditor_zh'], function(){
	var $form=$("#agent_article_edit_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_article_edit_editor');
	})
	var $code=$form.find("[name='code']");
	$code.change(function(){
		if($(this).val()==14){
			$form.find(".isShowTc").removeClass("hidden");
		}else{
			$form.find(".isShowTc").addClass("hidden");
		}
	});
	$code.val("${article.code}").change().prop("disabled",true);
	var ue = UE.getEditor('agent_article_edit_editor');
	ue.ready( function( editor ) {
		ue.setContent($form.find(".edit_content").html());
		$form.find(".edit_content").remove();
	});
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.content=ue.getContent();
		return params;
	});
}) */
</script>