<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="operation_log_rd_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${startTime} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="输入用户账号查询">
			</div>
			<div class="input-group">
				<label class="sr-only" for="type">搜索类型</label> <select class="form-control" name="type" id="op_log_stype_id"></select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary exportBtn" type="button">导出</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime} 23:59:59" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="content" placeholder="操作内容">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="ip" placeholder="输入操作ip查询">
			</div>
		</div>
	</div>
</form>
<div class="hidden">
	<form id="operation_log_rd_export_fmId" action="${base}agent/system/oplog/export.do" target="_blank" method="post">
		<input type="hidden" name="account"/> 
		<input type="hidden" name="type"/> 
		<input type="hidden" name="content"/>
		<input type="hidden" name="ip"/>
		<input type="hidden" name="begin"/>
		<input type="hidden" name="end"/>
	</form>
</div>
<table id="op_log_datagrid_tb"></table>
<script id="op_log_combolst_tpl" type="text/html">
	<option value="0">请选择类型</option>
	{{each data as tl}}
			<option value="{{tl.id}}">{{tl.name}}</option>
	{{/each}}
</script>
<script type="text/javascript">
requirejs(['template','jquery','bootstrap','Fui'],function(template){
	var types = {};
	$.ajax({
		url : "${base}/agent/system/oplog/typecombo.do",
		success : function(result) {
			for (var i = 0; i < result.length; i++) {
				types[result[i].id] = result[i].name;
			}
			$("#op_log_stype_id").html(template('op_log_combolst_tpl',  {"data" : result}));
		}
	});
	Fui.addBootstrapTable({
		id : 'op_log_datagrid_tb',
		url : '${base}/agent/system/oplog/list.do',
		columns : [ {
			field : 'account',
			title : '账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'logType',
			title : '日志类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'logContent',
			title : '操作记录',
			align : 'center',
			valign : 'middle',
			formatter:function(value, row, index){
				if(value){
					return  value.replace(/&/g, '&amp;')
		            .replace(/"/g, '&quot;')
		            .replace(/'/g, '&#39;')
		            .replace(/</g, '&lt;')
		            .replace(/>/g, '&gt;');
				}
				return "-";
			}
		}, {
			field : 'accountIp',
			title : 'IP',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'address',
			title : 'IP地址',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'createDatetime',
			title : '操作时间',
			align : 'center',
			width : '200px',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function typeFormatter(value, row, index) {
		return types[value];
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/system/oplog/detail.do?id='+row.id+'">详情</a>';
	}
	var $form=$("#operation_log_rd_form_id");
	$form.find(".exportBtn").click(function(){
		var $form1=$("#operation_log_rd_export_fmId");
		$form1.find("[name='account']").val($form.find("[name='account']").val());
		$form1.find("[name='type']").val($form.find("[name='type']").val());
		$form1.find("[name='content']").val($form.find("[name='content']").val());
		$form1.find("[name='ip']").val($form.find("[name='ip']").val())
		$form1.find("[name='begin']").val($form.find("[name='begin']").val())
		$form1.find("[name='end']").val($form.find("[name='end']").val())
		$form1.submit();
	});
});
</script>