<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/user/updpwd.do" class="form-submit">
<div class="modal-dialog">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
		<h4 class="modal-title">修改用户密码</h4>
	</div><input type="hidden" name="id" value="${agentUser.id }">
	<div class="modal-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td width="40%" class="text-right media-middle">用户账号：</td>
					<td class="text-left"><input name="account" class="form-control" type="text" value="${agentUser.account }"readonly></span></td>
				</tr>
				<tr>
					<td class="text-right media-middle">新密码：</td>
					<td class="text-left"><input type="password" class="form-control" name="pwd" /></td>
				</tr>
				<tr>
					<td class="text-right media-middle">确认密码：</td>
					<td class="text-left"><input type="password" class="form-control" name="rpwd" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default fui-close">关闭</button>
		<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
