<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="form-inline">
			<button id="a_permission_save" class="btn btn-primary" disabled="disabled">保存</button>
			<select id="a_permission_groupId" class="form-control">
				<option value="0">请选择组别</option>
				<c:forEach items="${groups }" var="at"><option value="${at.id}">${at.name}</option></c:forEach>
			</select>
		</div>
	</div>
	<div class="panel-body" id="a_permission_menus"></div>
</div>
<script id="a_permission_menus_tpl" type="text/html">
	{{each menus.nodes as fstNode}}
	<div class="checkbox">
		<label>
  			<input type="checkbox" id="{{IDheader+fstNode.curNode.id}}" parent="{{fstNode.curNode.parentId}}" value="{{fstNode.curNode.id}}" {{checkLst[fstNode.curNode.id]}}> {{fstNode.curNode.name}}
		</label>
		<div class="checkbox secCheckBox">
			{{each fstNode.nodes as secNode}}
				<label>
  					<input type="checkbox" id="{{IDheader+secNode.curNode.id}}" parent="{{secNode.curNode.parentId}}" value="{{secNode.curNode.id}}" {{checkLst[secNode.curNode.id]}}> {{secNode.curNode.name}}
				</label>
				<div class="checkbox thdCheckBox">
					{{each secNode.nodes as thdNode}}
						<label checkbox-inline >
  							<input type="checkbox" class="end" id="{{IDheader+thdNode.curNode.id}}" parent="{{thdNode.curNode.parentId}}" value="{{thdNode.curNode.id}}" {{checkLst[thdNode.curNode.id]}}> {{thdNode.curNode.name}}
						</label>
					{{/each}}
				</div>
			{{/each}}
		</div>
	</div>
	{{/each}}
</script>
<script type="text/javascript">
requirejs(['template','jquery'], function(template) {
	var $saveBtn=$("#a_permission_save")
		,$menus=$("#a_permission_menus")
		,$groupId=$("#a_permission_groupId")
		,IDheader = "a_permission_menu_node_";	
	$groupId.change(function(){
		$menus.html("");
		$saveBtn.prop("disabled", "disabled")
		var selval = $(this).val();
		if (selval > 0) {
			loadMenuByAccountId(selval);
		}
	});
	/**
	 *加载租户权限菜单
	 */
	function loadMenuByAccountId(groupId) {
		$.ajax({
			url : "${base}/agent/system/permission/getGroupPermission.do",
			data : {groupId : groupId},
			success : function(data) {
				var checklst = {},pm=data.pm;
				for (var i = 0; i < pm.length; i++) {
					checklst[pm[i].menuId] = "checked=\"checked\"";
				}
				data.checkLst = checklst;
				data.IDheader = IDheader;

				$menus.html(template('a_permission_menus_tpl', data));
				$saveBtn.removeProp("disabled");

				//绑定向上和向下的级联操作
				$(":checkbox",$menus).change(function() {
					if('end' == $(this).attr('class') && !$(this).is(':checked')){
						return;
					}
					checkMenu(this);
				});
			}
		});
	}

	function checkMenu(chk) {
		sonMenu(chk);
		parentMenu(chk);
	}

	function sonMenu(chk) {
		var val = $(chk).val();
		var curChecked = chk.checked;
		//点击当前checkbox下级的处理
		var others = $(":checkbox[parent='" + val + "']",$menus);
		if (!others) {
			return
		}
		var len = others.length;
		others.each(function() {
			this.checked = curChecked;
			//递归调用
			sonMenu(this);
		});
	}

	function parentMenu(chk) {
		var curChecked = chk.checked;
		var par = $(chk).attr("parent");
		if(par==0){
			return;
		}
		var others = $(":checkbox[parent='" + par + "']:checked",$menus);//同级别其他元素
		if (!others) {
			return
		}
		var len = others.length;
		//点击当前checkbox上级的处理
		if ((len == 0 && !curChecked) || (len == 1 && curChecked)) {
			var chkson = document.getElementById(IDheader + par);
			chkson.checked = curChecked;
			//递归调用
			parentMenu(chkson);
		}
	}

	$saveBtn.click(function(){
		var ids = [];
		$(":checkbox:checked",$menus).each(function() {
			ids.push($(this).val());
		});

		$.ajax({
			url : "${base}/agent/system/permission/savePermission.do",
			data : {
				groupId : $groupId.val(),
				ids : ids.join(",")
			},
			success : function(result) {
				layer.msg(result.msg||"保存成功");
			}
		});
	});
});
</script>