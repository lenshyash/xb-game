<%@ page language="java" pageEncoding="utf-8"%>
<div id="sys_red_packet_list_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="huodong">红包管理</a></li>
		<li><a href="#" data-toggle="tab" oname="zhongjiang">抢红包记录</a></li>
		<li><a href="#" data-toggle="tab" oname="xuni">虚拟记录</a></li>
	</ul>
<form class="fui-search table-tool" method="post" id="agent_redpacket_form_id">
	<div class="form-group zhongjiang hidden">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime }" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
	<div class="form-group huodong">
		<button class="btn btn-primary batchDelete">批量删除</button>
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/redPacket/addRedPacket.do">发红包</button>
	</div>
</form>
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/redPacketList.js?v=2.42'],function($redPacketList){
	$redPacketList.render();
});
</script>