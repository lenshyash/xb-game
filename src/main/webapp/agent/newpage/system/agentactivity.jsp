<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentactivity/add.do">新增</button>
	</div>
</div>
<table id="agentactivity_datagrid_tb"></table>
<h3><span class="label label-info">温馨提示:我们强烈建议将优惠活动详情做成图片，以便给用户带来更好的体验。</h3>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'agentactivity_datagrid_tb',
		url : '${base}/agent/system/agentactivity/list.do',
		columns : [ {
			field : 'title',
			title : '活动标题',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'titleImgUrl',
			title : '标题图片地址',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			field : 'modelStatus',
			title : '活动状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'updateTime',
			title : '发布时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'overTime',
			title : '过期时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		}]
	});
	function contentFormatter(value, row, index){
		if(value == null || value ==""){
			return value;
		}
		if(value.length>5){
			return value.substr(0, 5)+'...';
		}else{
			return value;
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/agentactivity/zhopenClose.do?id="+row.id+"&modelStatus="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/agentactivity/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/agentactivity/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
});
</script>