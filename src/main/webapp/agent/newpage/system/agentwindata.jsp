<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentwindata/add.do">新增</button>
	</div>
</div>
<table id="agentlunbo_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'agentlunbo_datagrid_tb',
		url :'${base}/agent/system/agentwindata/list.do',
		columns : [ {
			field : 'account',
			title : '中奖账户',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'winMoney',
			title : '中奖金额',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'winDate',
			title : '中奖时间',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'type',
			title : '游戏名称/彩种名称',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		}  ]
	});
	function operateFormatter(value, row, index) {
		return ['<a class="todo-ajax" href="${base}/agent/system/agentwindata/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
});
</script>