<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentkefu/add.do">新增</button>
	</div>
</div>
<table id="agentkefu_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'agentkefu_datagrid_tb',
		url :'${base}/agent/system/agentkefu/list.do',
		columns : [ {
			field : 'name',
			title : '名字',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'type',
			title : '客服类型',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : kefuType
		}, {
			field : 'headUrl',
			title : '头像图片地址',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			field : 'haoMa',
			title : '号码',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	//格式化公告类型
	function kefuType(value){
		switch(value){
		case 1:
			return 'QQ';
		case 2:
			return '微信';

		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/agentkefu/openCloseH.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/agentkefu/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/agentkefu/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	function contentFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>15){
			return value.substr(0, 15)+'...';
		}else{
			return value;
		}
	}
});
</script>