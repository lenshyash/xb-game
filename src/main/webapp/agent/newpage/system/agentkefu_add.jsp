<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/agentkefu/addSaveNew.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增轮播图片</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="35%" class="text-right media-middle">名字：</td>
						<td class="text-left"><input type="text" name="name" class="form-control"/></td>
					</tr>
					<tr>
						<td width="35%" class="text-right media-middle">客服类型：</td>
						<td class="text-left">
							<select name="type" class="form-control">
								<option value="1">QQ</option>	
								<option value="2">微信</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">头像图片地址：</td>
						<td class="text-left"><input type="text" name="headUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">号码：</td>
						<td class="text-left"><input type="text" name="haoMa" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="status" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">禁用</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>