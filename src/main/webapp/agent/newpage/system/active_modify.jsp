<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/active/save.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content"><input type="hidden" name="id" value="${active.id }">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增活动</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">活动名称：</td>
						<td width="35%" class="text-left"><input type="text" name="activeName" class="form-control" value="${active.activeName }"/></td>
						<td width="15%" class="text-right media-middle">活动类型：</td>
						<td width="35%" class="text-left"><select name="activeType" class="form-control"><option value="1"<c:if test="${active.activeType==1 }">selected</c:if>>幸运大转盘</option></select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">消耗积分：</td>
						<td class="text-left"><input name="score" class="form-control number" value="${active.score }" /></td>
						<td class="text-right media-middle">每日次数：</td>
						<td class="text-left"><input name="playCount" class="form-control digits"  value="${active.playCount }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${active.beginDatetime}" />" name="begin" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${active.endDatetime}" />" name="end" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">图片路径：</td>
						<td class="text-left"><input type="text" name="imgPath" class="form-control" value="${active.imgPath}"/></td>
						<td class="text-right media-middle">抽奖资格：</td>
						<td class="text-left"><textarea name="activeRole" class="form-control">${active.activeRole}</textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">活动规则：</td>
						<td class="text-left"><textarea name="activeHelp" class="form-control">${active.activeHelp}</textarea></td>
						<td class="text-right media-middle">活动声明：</td>
						<td class="text-left"><textarea name="activeRemark" class="form-control">${active.activeRemark}</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>