<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/agentarticle/eidtSaveNew.do" class="form-submit" id="agent_article_edit_formId">
<div class="modal-dialog modal-lg"><input name="id" type="hidden" value="${article.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">标题：</td>
						<td width="35%"  class="text-left"><input type="text" name="title" class="form-control" value="${article.title }"/></td>
						<td width="15%"  class="text-right media-middle">类型：</td>
						<td class="text-left">
							<select name="code" class="form-control">
								<option value="1">关于我们</option>
								<option value="2">取款帮助</option>
								<option value="3">存款帮助</option>
								<option value="4">联盟方案</option>
								<option value="5">联盟协议</option>
								<option value="6">联系我们</option>
								<option value="7">常见问题</option>
								<option value="8">玩法介绍</option>
								<option value="9">彩票公告</option>
								<option value="10">视讯公告</option>
								<option value="11">体育公告</option>
								<option value="12">电子公告</option>
								<option value="13">最新公告</option>
								<option value="15">签到公告</option>
								<option value="14">页面弹窗</option>
								<option value="19">手机弹窗</option>
								<option value="16">最新资讯</option>
								<option value="17">签到规则</option>
								<option value="18">新手教程</option>
								<option value="21">最新优惠</option>
								<option value="20">责任条款</option>
								<option value="22">红包规则</option>
								<option value="23">博彩责任</option>
								<option value="24">代理说明</option>
								<option value="25">试玩弹窗</option>
								<option value="26">游戏公告</option>
							</select>
						</td>
					</tr>
					<tr class="hidden isShowTc">
						<td class="text-right media-middle">页面展示位置：</td>
						<td class="text-left" colspan='3'>
							<div>
	   							<label class="checkbox-inline">
	   								<input type="checkbox"<c:if test="${article.index }"> checked="checked"</c:if> name="index" value="true"> 首页
	   							</label>
	   					 		<label class="checkbox-inline">
	   								<input type="checkbox"<c:if test="${article.reg }"> checked="checked"</c:if> name="reg" value="true"> 会员注册页
	   							</label>
	   							<label class="checkbox-inline">
	   								<input type="checkbox"<c:if test="${article.mutil }"> checked="checked"</c:if> name="mutil" value="true"> 代理注册页
	   							</label>
	   							<label class="checkbox-inline">
	   								<input type="checkbox"<c:if test="${article.cp }"> checked="checked"</c:if> name="cp" value="true"> 彩票投注页
	   							</label>
							</div>
						</td>
					</tr>
					<tr class="hidden isShowTc">
						<td class="text-right media-middle">弹窗大小：</td>
						<td class="text-left" colspan='3'>
							<div class="row">
							  <div class="col-md-4">
							    <div class="input-group">
								  <span class="input-group-addon">宽度</span>
								  <input name="frameWidth" class="form-control digits" type="text" value="${article.frameWidth }" placeholder="默认为550">
								  <span class="input-group-addon">px</span>
								</div>
							  </div>
							  <div class="col-md-1 text-center"style="margin-top:7px;">X</div>
							  <div class="col-md-4">
							    <div class="input-group">
								  <span class="input-group-addon">高度</span>
								  <input name="frameHeight" class="form-control digits" type="text" value="${article.frameHeight }" placeholder="默认为550">
								  <span class="input-group-addon">px</span>
								</div>
							  </div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="updateTime" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${article.updateTime}" />" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="overTime" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${article.overTime}" />" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left" colspan='3'><select name="modelStatus" class="form-control">
							<option value="2"<c:if test="${article.modelStatus==2 }">selected</c:if>>启用</option>
							<option value="1"<c:if test="${article.modelStatus==1 }">selected</c:if>>禁用</option>
						</select></td>
					</tr>
					<c:if test="${not empty dlst}">
						<tr>
							<td class="text-right media-middle">绑定模板：</td>
							<td class="text-left" colspan='3'>
								<select name="folderCode" class="form-control">
									<option value="">共用</option>
									<c:forEach items="${dlst }" var = "d">
										<option value="${d.folder }" <c:if test="${article.folderCode eq d.folder }">selected="selected"</c:if>>${d.folder }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:if>
					<tr>
						<td width="15%" class="text-right media-middle">排序</td>
						<td width="35%" class="text-left"><input type="text" name="sort" class="form-control" value="${article.sort}"/></td>
					</tr>
					<tr>
						<td class="text-left" colspan="4"><script name="content" id="agent_article_edit_editor" type="text/plain" style="width:860px;height:300px;"></script></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
<div class="hidden edit_content">${article.content}</div>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#agent_article_edit_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_article_edit_editor');
	})
	var $code=$form.find("[name='code']");
	$code.change(function(){
		if($(this).val()==14){
			$form.find(".isShowTc").removeClass("hidden");
		}else{
			$form.find(".isShowTc").addClass("hidden");
		}
	});
	$code.val("${article.code}").change().prop("disabled",true);
	var ue = UE.getEditor('agent_article_edit_editor');
	ue.ready( function( editor ) {
		ue.setContent($form.find(".edit_content").html());
		$form.find(".edit_content").remove();
	});
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.content=ue.getContent();
		return params;
	});
})
</script>