<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/message/update.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" name="id" value="${message.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">编辑站内信</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">站内信标题：</td>
						<td class="text-left"><input type="text" class="form-control" name="title" value="${message.title }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">站内信内容：</td>
						<td class="text-left"><textarea class="form-control" name="message">${message.message }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>