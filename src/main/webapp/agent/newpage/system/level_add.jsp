<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/level/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加等级</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">等级名称：</td>
						<td><input type="text" name="levelName" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">存款金额：</td>
						<td><input name="depositMoney" class="form-control" placeholder="晋级需要存款金额" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">晋级彩金：</td>
						<td><input type="text" name="giftMoney" class="form-control" placeholder="不填或填0不赠送"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">等级图标：</td>
						<td><input type="text" name="icon" class="form-control" placeholder="等级图片网址"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td><textarea name="remark" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>