<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="system_agentarticle_form_id">
	<div class="form-group">
		<div class="form-inline">
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentarticle/add.do">新增</button>
			<div class="input-group">
				<select name="code" class="form-control">
					<option value="0">请选择类型</option>
					<option value="1">关于我们</option>
					<option value="2">取款帮助</option>
					<option value="3">存款帮助</option>
					<option value="4">联盟方案</option>
					<option value="5">联盟协议</option>
					<option value="6">联系我们</option>
					<option value="7">常见问题</option>
					<option value="8">玩法介绍</option>
					<option value="9">彩票公告</option>
					<option value="10">视讯公告</option>
					<option value="11">体育公告</option>
					<option value="12">电子公告</option>
					<option value="13">最新公告</option>
					<option value="15">签到公告</option>
					<option value="14">页面弹窗</option>
					<option value="19">手机弹窗</option>
					<option value="16">最新资讯</option>
					<option value="17">签到规则</option>
					<option value="18">新手教程</option>
					<option value="21">最新优惠</option>
					<option value="20">责任条款</option>
					<option value="22">红包规则</option>
					<option value="23">博彩责任</option>
					<option value="24">代理说明</option>
					<option value="25">试玩弹窗</option>
					<option value="26">游戏公告</option>
				</select>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<h3><span class="label label-info">温馨提示:手机弹窗图片最佳效果规格为宽度:280px,高度200px;</h3>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url :'${base}/agent/system/agentarticle/list.do',
		columns : [ {
			field : 'title',
			title : '公告名称',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'code',
			title : '公告类型',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : articleType
		}, {
			field : 'modelStatus',
			title : '公告状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'updateTime',
			title : '开始时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'overTime',
			title : '结束时间',
			align : 'center',
			width : '300',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'sort',
			title : '排序',
			align : 'center',
			width : '300',
			valign : 'middle'
		},  {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		}]
	});
	//格式化公告类型
	function articleType(value){
		switch(value){
		case 1:
			return '关于我们';
			break;
		case 2:
			return '取款帮助';
			break;
		case 3:
			return '存款帮助';
			break;
		case 4:
			return '联盟方案';
			break;
		case 5:
			return '联盟协议';
			break;
		case 6:
			return '联系我们';
			break;
		case 7:
			return '常见问题';
			break;
		case 8:
			return '玩法介绍';
			break;
		case 9:
			return '彩票公告';
			break;
		case 10:
			return '视讯公告';
			break;
		case 11:
			return '体育公告';
			break;
		case 12:
			return '电子公告';
			break;
		case 13:
			return '最新公告';
			break;
		case 14:
			return '页面弹框';
			break;
		case 15:
			return '签到公告';
			break;
		case 16:
			return '最新资讯';
			break;
		case 17:
			return '签到规则';
			break;
		case 18:
			return '新手教程';
			break;
		case 19:
			return '手机弹窗';
			break;
		case 20:
			return '责任条款';
			break;
		case 21:
			return '最新优惠';
			break;
		case 22:
			return '红包规则';
			break;
		case 23:
			return '博彩责任';
			break;
		case 24:
			return '代理说明';
			break;
		case 25:
			return '试玩弹窗';
			break;
		case 26:
			return '游戏公告';
			break;
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/agentarticle/zhopenClose.do?id="+row.id+"&modelStatus="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/agentarticle/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/agentarticle/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	var $formId = $("#system_message_form_id"),$table = $("#message1_datagrid_tb");
	function refresh(){
		var $table=$formId.parents(".fui-box").data("bootstrapTable");
		if($table && $table.length){
			$table.bootstrapTable('refresh');
		}
	}
});
</script>