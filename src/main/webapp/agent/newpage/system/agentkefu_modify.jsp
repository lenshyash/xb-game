<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/agentkefu/editSaveNew.do" class="form-submit" id="agent_kefu_edit_formId">
<div class="modal-dialog"><input name="id" type="hidden" value="${kefu.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改轮播图片</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="35%" class="text-right media-middle">名字：</td>
						<td class="text-left"><input type="text" name="name" class="form-control"value="${kefu.name }"/></td>
					</tr>
					<tr>
						<td width="35%" class="text-right media-middle">客服类型：</td>
						<td class="text-left">
							<select name="type" class="form-control">
								<option value="1">QQ</option>
								<option value="2">微信</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">头像图片地址：</td>
						<td class="text-left"><input type="text" name="headUrl" class="form-control"value="${kefu.headUrl }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">号码：</td>
						<td class="text-left"><input type="text" name="haoMa" class="form-control"value="${kefu.haoMa }"/></td>
					</tr>

						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="status" class="form-control">
							<option value="2"<c:if test="${kefu.status==2 }">selected</c:if>>启用</option>
							<option value="1"<c:if test="${kefu.status==1 }">selected</c:if>>禁用</option>
						</select></td> 
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#agent_kefu_edit_formId");
	var $code=$form.find("[name='type']");
	$code.val("${kefu.type}");
// 	$code.val("${lunbo.code}").change().prop("disabled",true);
})
</script>