<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/user/save.do" class="form-submit">
<div class="modal-dialog modal-lg">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
		<h4 class="modal-title">新增用户</h4>
	</div>
	<div class="modal-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td width="15%" class="text-right media-middle">用户账号：</td>
					<td width="35%" class="text-left"><input name="account" class="form-control required" type="text"></span></td>
					<td width="15%" class="text-right media-middle">用户分组：</td>
					<td width="35%" class="text-left"><select name="groupId" class="form-control">
						<c:forEach items="${groups }" var="g"><option value="${g.id }">${g.name }</option></c:forEach>
					</select></td>
				</tr>
				<tr class="pwd_tr">
					<td class="text-right media-middle">密码：</td>
					<td class="text-left"><input name="pwd" class="form-control required" type="password" /></td>
					<td class="text-right media-middle">确认密码：</td>
					<td class="text-left"><input name="rpwd" class="form-control required" type="password" /></td>
				</tr>

				<tr>
					<td class="text-right media-middle">状态：</td>
					<td class="text-left"><select name="accountStatus" class="form-control">
							<option value="1">禁用</option>
							<option value="2" selected>启用</option>
					</select></td>
					<td class="text-right media-middle">备注：</td>
					<td class="text-left"><input name="remark" class="form-control" type="text" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default fui-close">关闭</button>
		<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
