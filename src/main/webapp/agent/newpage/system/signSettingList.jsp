<%@ page language="java" pageEncoding="utf-8"%>
<div id="sys_sign_setting_list_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="setting">签到设置</a></li>
		<li><a href="#" data-toggle="tab" oname="record">签到记录</a></li>
	</ul>
	<div class="table-tool">
		<div class="form-group setting-tool">
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/sign/add.do">新增</button>
		</div>
	</div>
	<table class="fui-default-table"></table>
</div>
<div id="remark_div" style="padding: 10px;">
	<span>签到规则：</span><br>
	<div style="color:blue;">循环赠送：比如设置了连续4天和连续8天，当会员签到16天时且没有设置16天的配置时，就取8天的配置<br>
	硬性要求：设置N天，当签到达到N天时才会取该配置<br>
	</div>
	<span class="text-danger">可以根据要求灵活设置，两种配置可以共存</span>
</div>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/signSettingList.js?v=2'],function($ele){
	$ele.render();
});
</script>