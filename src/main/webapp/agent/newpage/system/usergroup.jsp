<%@ page language="java" pageEncoding="UTF-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/group/add.do" >新增</button>
	</div>
</div>
<table id="usergroup_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'usergroup_datagrid_tb',
		url : '${base}/agent/system/group/list.do',
		columns : [ {
			field : 'name',
			title : '组别名称',
			align : 'center',
			width : '30',
			valign : 'middle',
		}, {
			title : '操作',
			align : 'center',
			width : '30',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/group/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a> ',
				'<a class="todo-ajax" href="${base}/agent/system/group/del.do?id=',row.id,'" title="确定要删除',row.name,'">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a> ' ]
				.join('');
	}
});
</script>