<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/agentwindata/addSaveNew.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增中奖数据</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="35%" class="text-right media-middle">中奖账号：</td>
						<td class="text-left"><input type="text" name="account" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">中奖金额：</td>
						<td class="text-left"><input type="text" name="winMoney" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">中奖类型（游戏名称/彩种名称）：</td>
						<td class="text-left"><input type="text" name="type" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">中奖时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="winDate" placeholder="中奖时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>