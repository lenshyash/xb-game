<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="system_message_form_id">
	<div class="form-group">
		<div class="form-inline">
			<button class="btn btn-primary batchDelete">批量删除</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/message/add.do">新增</button>
			<div class="input-group">
				<input type="text" class="form-control" name="title" placeholder="标题名称">
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table id="message1_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'message1_datagrid_tb',
		url : '${base}/agent/system/message/list.do',
		columns : [ {
			field : 'checkbox',
			title : '<input type="checkbox" class="check_all">',
			align : 'center',
			vilign : 'middle',
			formatter : operateCheckboxMatter
		},{
			field : 'title',
			title : '站内信标题',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/message/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/message/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	
	function operateCheckboxMatter(value,row,index){
		return '<input type="checkbox" value="'+row.id+'">';
	}
	
	var $formId = $("#system_message_form_id"),$table = $("#message1_datagrid_tb");
	
	$formId.on("click","button.batchDelete",function(e){
		//取消原始的事件
		e.preventDefault();
		var ids='';
		$table.find('tbody input:checkbox:checked').each(function(i,j){
			j = $(j);
			ids += j.val() + ",";
		})
		if(!ids){
			layer.msg('请勾选要删除的选项');
			return;
		}
		ids = ids.substring(0,ids.length-1);
		layer.confirm('确定要删除选中的选项？',{btn:['确定','取消']},function(index){
			$.ajax({
				url:'${base}/agent/system/message/batchDelete.do',
				data:{batchId:ids},
				success:function(res){
					if(res.success){
						layer.msg('删除成功');
						$table.find('input:checked').prop('checked',false);
						refresh();
					}else{
						layer.msg(res.msg);
					}
					layer.close(index);
				}
			})			
		},function(){
			$table.find('input:checked').prop('checked',false);
		})
	})
	
	$table.on("click","input.check_all",function(){
		var $this = $(this),isChecked = $this.prop('checked');
		$table.find('tbody input:checkbox').prop('checked',isChecked);
	});
	
	function refresh(){
		var $table=$formId.parents(".fui-box").data("bootstrapTable");
		if($table && $table.length){
			$table.bootstrapTable('refresh');
		}
	}
	
});
</script>