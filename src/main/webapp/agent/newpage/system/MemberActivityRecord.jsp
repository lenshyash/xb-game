<%@ page language="java" pageEncoding="utf-8"%>
<div id="sys_activity_record_list_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="huodong">优惠活动管理</a></li>
		<li><a href="#" data-toggle="tab" oname="zhongjiang">优惠活动申请</a></li>
		
	</ul>
<form class="fui-search table-tool" method="post" id="agent_redpacket_form_id">
	<div class="form-group zhongjiang hidden">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime }" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
	<div class="form-group huodong">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/agentactivity/add.do">新增</button>
	</div>
</form>
	<table class="fui-default-table"></table>
	<span class="text-danger">优惠活动的申请备注会员可在申请列表查看</span>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/memberActivityRecord.js?v=2.42'],function($redPacketList){
	$redPacketList.render();
});
</script>