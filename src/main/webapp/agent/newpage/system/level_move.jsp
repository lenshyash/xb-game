<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/level/lvlMove.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" value="${level.id }" name="curId">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">成员转移</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">原等级名称：</td>
						<td class="text-left">${level.levelName }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">转到等级：</td>
						<td class="text-left"><select name="nextId" class="form-control">
						<c:forEach items="${allLevel }" var="l">
							<c:if test="${l.id!=level.id }"><option value="${l.id }">${l.levelName }</option></c:if>
						</c:forEach></select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>