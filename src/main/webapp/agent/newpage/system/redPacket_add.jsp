<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/redPacket/save.do" class="form-submit">
<input type="hidden" id="redPacketCreateType" name="createType" value="1">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">发红包</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">标题：</td>
						<td width="20%"><input name="title" class="form-control" /></td>
						<td width="20%" class="text-right media-middle">金额：</td>
						<td width="30%"><input name="totalMoney" class="form-control money" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">个数：</td>
						<td><input name="totalNumber" class="form-control digits" /></td>
						<td width="20%" class="text-right media-middle">最小金额：</td>
						<td width="30%"><input name="minMoney" class="form-control money" value="0.01"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">打码量倍数：</td>
						<td><input name="betRate" class="form-control money" value="1"/></td>
						<td class="text-right media-middle">限制当天IP次数：</td>
						<td><input name="ipNumber" class="form-control digits" value="1"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" id="begin" format="YYYY-MM-DD HH:mm:ss" name="begin" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" id="end" format="YYYY-MM-DD HH:mm:ss" name="end" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制当天充值：</td>
						<td colspan="3">
							<!-- <label class="radio-inline">
							  <input type="radio" name="todayDeposit" value=2>需要当天有充值
							</label>
							<label class="radio-inline">
							  <input type="radio" name="todayDeposit" value="1" checked>不限制
							</label> -->
							<input name="todayDeposit" class="form-control money" value="1" placeholder="不填或0则不限制"/>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }">${le1.levelName }</label>  </c:forEach>
						<div style="color:#a94442;margin-top:3px;">全部选择和全部不选是一样，表示所有等级都可以使用</div>
						</td>
						
					</tr>
					
					<tr class="create_record_tr">
						<td class="text-center media-middle" colspan="4"><a href="#"><div style="float: top;" id="redPacketBtnDesc">创建虚拟红包记录</div><div style="float: bottom;"><span id="redPacketBtn_span" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div></a></td>
					</tr>
					<tr class="create_record hidden">
						<td class="text-right media-middle">记录条数：</td>
						<td><input type="text" class="form-control" name="recordNum"/></td>
						<td class="text-right media-middle">奖金区间：</td>
						<td class="text-left form-inline"><input type="text" name="beginMoney" class="form-control" style="width: 30%;"/>-<input type="text" name="endMoney" class="form-control" style="width: 40%;"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){ 
	 var today = new Date().format("yyyy-MM-dd");
	 $("#begin").val(today+" 00:00:00");
	 $("#end").val(today+" 23:59:59");
}); 

requirejs(['jquery'],function(){
	$(".create_record_tr").click(function(){
		var type = $("#redPacketCreateType").val();
		if(1 == type){
			$(".create_record").removeClass("hidden");
			$("#redPacketBtnDesc").html("收起");
			$("#redPacketBtn_span").removeClass("glyphicon-chevron-down");
			$("#redPacketBtn_span").addClass("glyphicon-chevron-up");
			$("#redPacketCreateType").val(2);
		}else{
			$("#redPacketBtnDesc").html("创建虚拟红包记录");
			$("#redPacketBtn_span").removeClass("glyphicon-chevron-up");
			$("#redPacketBtn_span").addClass("glyphicon-chevron-down");
			$(".create_record").addClass("hidden");
			$("#redPacketCreateType").val(1)
		}
	});
});
</script>
<script type="text/javascript">
function max1fn() {
	$("#min2").attr("value",parseInt($("#max1").val())+1);
}
function max2fn() {
	$("#min3").attr("value",parseInt($("#max2").val())+1);
}function max3fn() {
	$("#min4").attr("value",parseInt($("#max3").val())+1);
}function max4fn() {
	$("#min5").attr("value",parseInt($("#max4").val())+1);
}
</script>