<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/active/updateVirtualrecord.do" class="form-submit">
<div class="modal-dialog">
	<input type="hidden" name="id" value="${virtualRecord.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">大转盘中奖记录</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">获奖者：</td>
						<td class="text-left">
							<input type="text" name="account" class="form-control" value="${virtualRecord.account }"/>
						</td>
					</tr>
					<tr>
						<td class="text-right">类型：</td>
						<td class="text-left">
							<select name="awardType" class="form-control">
								<c:choose> 
								     <c:when test="${virtualRecord.awardType == '1' }">  
								     	<option value="1" selected>不中奖</option>
									 </c:when>      
								     <c:otherwise>      
								     	<option value="1">不中奖</option>
									 </c:otherwise> 
								</c:choose>
								<c:choose> 
								     <c:when test="${virtualRecord.awardType == '2' }">  
								     	 <option value="2" selected>现金</option>
									 </c:when>      
								     <c:otherwise>      
									     <option value="2">现金</option>
									 </c:otherwise> 
								</c:choose>
								<c:choose> 
								     <c:when test="${virtualRecord.awardType == '3' }">  
								     	 <option value="3" selected>奖品</option>
									 </c:when>      
								     <c:otherwise>      
									     <option value="3">奖品</option>
									 </c:otherwise> 
								</c:choose>
								<c:choose> 
								     <c:when test="${virtualRecord.awardType == '4' }">  
								     	 <option value="4" selected>积分</option>
									 </c:when>      
								     <c:otherwise>      
									     <option value="4">积分</option>
									 </c:otherwise> 
								</c:choose>
							</select>
						</td>
					</tr>
					<tr>
						<td class="text-right">奖品名称：</td>
						<td class="text-left">
							<input type="text" name="productName" class="form-control" value="${virtualRecord.productName }"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>