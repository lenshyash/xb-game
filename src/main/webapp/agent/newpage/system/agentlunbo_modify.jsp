<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/system/agentlunbo/editSaveNew.do" class="form-submit" id="agent_lunbo_edit_formId">
<div class="modal-dialog"><input name="id" type="hidden" value="${lunbo.id}">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改轮播图片</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="35%" class="text-right media-middle">标题：</td>
						<td class="text-left"><input type="text" name="title" class="form-control"value="${lunbo.title }"/></td>
					</tr>
					<tr>
						<td width="35%" class="text-right media-middle">轮播类型：</td>
						<td class="text-left">
							<select name="code" class="form-control">
								<option value="1">首页轮播</option>
								<option value="5">手机轮播</option>
								<option value="2">头部图片</option>
								<option value="3">新闻图片</option>
								<option value="4">导航图片</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">标题图片地址：</td>
						<td class="text-left"><input type="text" name="titleImg" class="form-control"value="${lunbo.titleImg }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">标题链接地址：</td>
						<td class="text-left"><input type="text" name="titleUrl" class="form-control"value="${lunbo.titleUrl }"/></td>
					</tr>
					<c:if test="${not empty dlst}">
						<tr>
							<td class="text-right media-middle">绑定模板：</td>
							<td class="text-left">
								<select name="folderCode" class="form-control">
									<option value="">共用</option>
									<c:forEach items="${dlst }" var = "d">
										<option value="${d.folder }" <c:if test="${lunbo.folderCode eq d.folder }">selected="selected"</c:if>>${d.folder }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:if>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="updateTime"value="<fmt:formatDate pattern="yyyy-MM-dd" value="${lunbo.updateTime}" />" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="overTime"value="<fmt:formatDate pattern="yyyy-MM-dd" value="${lunbo.overTime}" />" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td class="text-left"><input type="text" name="paiXu" value="${lunbo.paiXu}" class="form-control digits"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="modelStatus" class="form-control">
							<option value="2"<c:if test="${lunbo.modelStatus==2 }">selected</c:if>>启用</option>
							<option value="1"<c:if test="${lunbo.modelStatus==1 }">selected</c:if>>禁用</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#agent_lunbo_edit_formId");
	var $code=$form.find("[name='code']");
	$code.val("${lunbo.code}");
// 	$code.val("${lunbo.code}").change().prop("disabled",true);
})
</script>