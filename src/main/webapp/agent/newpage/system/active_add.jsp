<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/active/save.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增活动</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">活动名称：</td>
						<td width="35%" class="text-left"><input type="text" name="activeName" class="form-control"/></td>
						<td width="15%" class="text-right media-middle">活动类型：</td>
						<td width="35%" class="text-left"><select name="activeType" class="form-control"><option value="1" selected>幸运大转盘</option></select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">消耗积分：</td>
						<td class="text-left"><input name="score" class="form-control number" /></td>
						<td class="text-right media-middle">每日次数：</td>
						<td class="text-left"><input name="playCount" class="form-control digits" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="end" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">图片路径：</td>
						<td class="text-left"><input type="text" name="imgPath" class="form-control" /></td>
						<td class="text-right media-middle">抽奖资格：</td>
						<td class="text-left"><textarea name="activeRole" class="form-control"></textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">活动规则：</td>
						<td class="text-left"><textarea name="activeHelp" class="form-control"></textarea></td>
						<td class="text-right media-middle">活动声明：</td>
						<td class="text-left"><textarea name="activeRemark" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>