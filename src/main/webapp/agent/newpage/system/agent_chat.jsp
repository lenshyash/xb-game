<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
<%-- 	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" type="button" url="${base}/agent/system/level/add.do">新增</button>
		<button class="btn btn-primary todo-ajax" type="button" url="${base}/agent/system/level/reStat.do">重新统计成员数量</button>
	</div> --%>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/system/chat/list.do",
		columns : [ {
			field : 'name',
			title : '名称',
			align : 'center',
			valign : 'middle',
		}, {
			field : 'platformId',
			title : '聊天室编号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'token',
			title : '聊天室token',
			align : 'center',
			valign : 'bottom'
		}, {
			field : 'domainUrl',
			title : '聊天室域名',
			align : 'center',
			valign : 'bottom'
		},{
			field : 'platformId',
			title : '超管账号',
			align : 'center',
			valign : 'middle',
			formatter : superformatter
		},{
			field : 'platformPass',
			title : '密码',
			align : 'center',
			valign : 'middle'
		},{
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function superformatter(value, row, index) {
		return value+'admin';
	}
/* 	function statusFormatter(value, row, index) {
		if(row.levelDefault==2){
			return "启用";
		}
		return Fui.statusFormatter({val:value,url:"${base}/agent/system/level/updStatus.do?id="+row.id+"&status="});
	} */
	function operateFormatter(value, row, index) {
		var s='<a class="open-dialog" href="${base}/agent/system/chat/removpg.do?token='+row.token+'&platformId='+row.platformId+'" title="成员转移"> 成员移除 </a>  '
			+'<a class="open-dialog" href="${base}/agent/system/chat/importpg.do?platformId='+row.platformId+'&token='+row.token+'" title="成员导入"> 成员导入 </a>  ';
		return s;
	}
});
</script>