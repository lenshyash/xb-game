<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/product/add.do">新增</button>
	</div>
</div>
<table id="product_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		id : 'product_datagrid_tb',
		url : "${base}/agent/system/product/list.do",
		columns : [ {
			field : 'productName',
			title : '商品名称',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'price',
			title : '商品面值',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/system/product/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="${base}/agent/system/product/delete.do?id=',row.id,'" title="确定要删除“',row.productName,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
});
</script>