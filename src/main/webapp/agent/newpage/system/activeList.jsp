<%@ page language="java" pageEncoding="utf-8"%>
<div id="sys_active_list_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="huodong">活动管理</a></li>
		<li><a href="#" data-toggle="tab" oname="zhongjiang">中奖记录</a></li>
		<li><a href="#" data-toggle="tab" oname="xunijilu">虚拟记录</a></li>
	</ul>
<form class="fui-search table-tool" method="post">
	<div class="form-group zhongjiang hidden">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="type">
					<option value="0">全部类型</option>
					<option value="4">积分</option>
					<option value="2">现金</option>
					<option value="3">奖品</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime }" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
	<div class="form-group huodong">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/system/active/addActive.do">新增</button>
	</div>
	<div class="form-group xunijilu hidden">
<!-- 		<div class="form-inline xunijilu1 " style="margin-top: 29px;"> -->
<!-- 			<div class="input-group"> -->
<!-- 				<select class="form-control" name="type"> -->
<!-- 					<option value="0">全部类型</option> -->
<!-- 					<option value="4">积分</option> -->
<!-- 					<option value="2">现金</option> -->
<!-- 					<option value="3">奖品</option> -->
<!-- 				</select> -->
<!-- 			</div> -->
<!-- 			<div class="input-group"> -->
<!-- 				<input type="text" class="form-control" name="account" placeholder="会员账号"> -->
<!-- 			</div> -->
<!-- 			<div class="input-group"> -->
<!-- 				<input type="text" class="form-control fui-date" name="begin" value="" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span> -->
<!-- 			</div> -->
<!-- 			<div class="input-group"> -->
<!-- 				<input type="text" name="end" class="form-control fui-date" value="" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span> -->
<!-- 			</div> -->
<!-- 			<button class="btn btn-primary" style="margin-left: 51px;">查询</button> -->
<!-- 			<br/> -->
<!-- 		</div>  -->
		<button class="btn btn-primary open-dialog cached" style="margin-top: 10px;margin-bottom: 10px;" url="${base}/agent/system/active/addActiveVirtualrecord.do" >新增</button>
	</div>
</form>
	<table class="fui-default-table"></table>
</div>
<h3><span class="label label-info">温馨提示:大转盘图片的最佳效果规格为宽度:438px,高度435px;</h3>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/activeList.js'],function($activeList){
	$activeList.render();
});
</script>