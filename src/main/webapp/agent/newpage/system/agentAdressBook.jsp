<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="system_agent_adress_book_form_id">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号" style="width:120px;">
			</div>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary exportBtn" type="button">导出</button>
		</div>
	</div>
</form>
<div class="hidden">
	<form id="adress_book_export_fm_id" action="${base}/agent/system/agentAdressBook/export.do" target="_blank">
		<input type="hidden" name="account"/> 
		<input type="hidden" name="status"/> 
		<input type="hidden" name="begin"/>
		<input type="hidden" name="end"/>
		<input type="hidden" name="type"/>
	</form>
</div>
<table id="agentAdressBook_datagrid_tb"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	
	var $form=$("#system_agent_adress_book_form_id");
	$form.find(".exportBtn").click(function(){
		var $form1=$("#adress_book_export_fm_id");
		$form1.find("[name='account']").val($form.find("[name='account']").val());
		$form1.find("[name='status']").val($form.find("[name='status']").val())
		$form1.find("[name='begin']").val($form.find("[name='begin']").val())
		$form1.find("[name='end']").val($form.find("[name='end']").val())
		$form1.find("[name='type']").val($form.find("[name='type']").val())
		$form1.submit();
	});
	
	Fui.addBootstrapTable({
		id : 'agentAdressBook_datagrid_tb',
		url :'${base}/agent/system/agentAdressBook/list.do',
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'contactName',
			title : '联系人姓名',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'contactPhone',
			title : '联系人电话',
			align : 'center',
			width : '180',
			valign : 'bottom'
		} ]
	});
});
</script>