<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/agentactivity/addSaveNew.do" class="form-submit" id="agent_artivity_add_formId">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">新增优惠活动</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">标题：</td>
						<td width="35%" class="text-left"><input type="text" name="title" class="form-control"/></td>
						<td width="15%" class="text-right media-middle">标题图片：</td>
						<td width="35%" class="text-left"><input type="text" name="titleImgUrl" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" value="${startDate }" name="updateTime" placeholder="开始时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
							<input type="text" class="form-control fui-date" name="overTime" value="${endDate }" placeholder="结束时间"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="status" class="form-control">
								<option value="2" selected>启用</option>
								<option value="1">禁用</option>
						</select></td>
						<td class="text-right media-middle">排序：</td>
						<td class="text-left"><input type="text" name="paiXu" class="form-control number"/></td>
					</tr>
					<c:if test="${not empty dlst}">
						<tr>
							<td class="text-right media-middle">绑定模板：</td>
							<td class="text-left">
								<select name="folderCode" class="form-control">
									<option value="">共用</option>
									<c:forEach items="${dlst }" var = "d">
										<option value="${d.folder }">${d.folder }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:if>
					<tr>
						<td class="text-right media-middle">自助申请：</td>
						<td class="text-left"><select name="applyFlag" class="form-control">
							<option value="2">启动</option>
							<option value="1" selected="selected">禁用</option>
						</select></td>
						<td class="text-right media-middle">申请选项：</td>
						<td class="text-left"><textarea name="applySelected" class="form-control" placeholder="英文逗号隔开每个活动选项 例：投注100返1元,投注300返3元"></textarea> </td>
					</tr>
					<tr>
						<td class="text-left" colspan="4"><script name="content" id="agent_artivity_add_editor" type="text/plain" style="width:860px;height:300px;"></script></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
require(['ueditor_zh'], function(){
	var $form=$("#agent_artivity_add_formId");
	$form.find(".fui-close").data("cancelFun",function(){
		UE.delEditor('agent_artivity_add_editor');
	})
	var ue = UE.getEditor('agent_artivity_add_editor');
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		params.content=ue.getContent();
		params.modelStatus=$form.find("[name='status']").val();
		return params;
	});
})
</script>