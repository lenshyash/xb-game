<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="modal-dialog" id="tran_log_hand_dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">手动更新(<c:if test="${isAdd}">从本平台转入第三方平台</c:if><c:if test="${!isAdd}">从第三方平台转回本平台</c:if>)</h4>
		</div>
		<div class="modal-body">
			<form class="tran_log_form">
				<span class="text-danger"></span><br/>
				<button type="button" class="btn btn-primary get-data-btn">获取记录最新状态</button>
				<span class="orderStatus"> </span>
				<input type="hidden" name="tranId" value="${tranId}"/>
				<div class="radio">
				    <label>
				      <input type="radio"  class = "status-success" name="status" value="1"> <font color="green">交易成功</font><c:if test="${!isAdd}">(补款${money}元)</c:if> 
				    </label>
				 </div>
				<div class="radio">
				    <label>
				      <input type="radio" class = "status-fairue" name="status" value="2"> <font color="red">交易失败</font><c:if test="${isAdd}">(补款${money}元)</c:if>
				    </label>
				 </div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary save-data-btn">保存</button>
			<button type="button" class="btn btn-default fui-close">取消</button>
		</div>
	</div>
</div>

<script type="text/javascript">
requirejs(['jquery'],function(){
	var $win = $("#tran_log_hand_dialog");
	var $form = $win.find(".tran_log_form");
	$win.find(".get-data-btn").bind("click",function(){
		var $btn = $(this);
		var $msgSpan = $form.find(".orderStatus").html("读取订单状态..."); 
		$btn.attr("disabled","disabled");
		$.ajax({
			url:'${base}/agent/rtl/getOrderStatus.do',
			type:'POST',
			dataType:"json",
			cache: false,
			data:{
				id : $form.find("input[name='tranId']").val()
			},
			success:function(data){
				var cls = data.status ? "status-success" : "status-fairue";
				$form.find("."+cls).click();
				$msgSpan.html("订单状态:"+(data.status ?"<font color='green'>成功</font>":"<font color='red'>失败</font>"));
			},
			errorFn:function(){
				$msgSpan.html("订单状态读取失败");
			},
			complete:function(){
				 $btn.removeAttr("disabled");
			}
		});
	});
	
	$win.find(".save-data-btn").bind("click",function(){
		var status = $form.find("input[name='status']:checked").val();
		if(!status){
			layer.msg('请选择订单状态');
			return;
		}
		$.ajax({
			url:'${base}/agent/rtl/updStatus.do',
			type:'POST',
			dataType:"json",
			cache: false,
			data:{
				id : $form.find("input[name='tranId']").val(),
				status :status
			},
			success:function(data){
				layer.closeAll();
				layer.msg('订单更新成功');
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	});
});
</script>