<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" name="username"  class="form-control" name="username" placeholder="三方账户名称">
			</div>
			<div class="input-group">
				<input type="text" name="account"  class="form-control" name="account" placeholder="本地账户名称">
			</div>
			<div class="input-group">
				<select class="form-control" name="gameType">
					<option value="">全部类型</option>
					<option value="1">AG</option>
					<option value="2">BBIN</option>
					<option value="3">MG</option>
					<option value="4">QT</option>
					<option value="5">ALLBET</option>
					<option value="6">PT</option>
					<option value="7">OG</option>
					<option value="8">DS</option>
					<option value="9">CQ9</option>
					<option value="11">JDB</option>
					<option value="12">KY</option>
					<option value="10">IBC</option>
					<option value="92">TTG</option>
					<option value="95">MW</option>
					<option value="96">ISB</option>
                    <option value="99">M8</option>
                    <option value="991">M8H</option>
                    <option value="98">BG</option>
                    <option value="97">VR</option>
                    <option value="13">EBET</option>
                    <option value="14">TH</option>
                    <option value="16">KX</option>
				</select>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/ruser/list.do',
		columns : [{
			field : 'platformType',
			title : '三方游戏类型',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter:gameTypeFormatter
		},{
			field : 'account',
			title : '三方账户名称',
			align : 'center',
			width : '180',
			valign : 'middle'
		}, {
			field : 'password',
			title : '三方账户密码',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'memberAccount',
			title : '本地系统账户',
			align : 'center',
			width : '200',
			valign : 'middle'
		},{
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}]
	});
	function gameTypeFormatter(value, row, index) {
		switch(value-0){
		case 1: return "AG";
		case 2: return "BBIN";
		case 3: return "MG";
		case 4:return "QT";
		case 5:return "ALLBET";
		case 6:return "PT";
		case 7:return "OG";
		case 8:return "DS";
		case 9:return "CQ9";
		case 10:return "IBC";
		case 11:return "JDB";
		case 92:return "TTG";
		case 95:return "MW";
		case 96:return "ISB";
        case 99:return "M8";
        case 991:return "M8H";
        case 98:return "BG";
        case 97:return "VR";
        case 13:return "EBET";
        case 14:return "TH";
        case 16:return "KX";
		}
		return "";
	}
});
</script>