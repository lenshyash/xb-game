<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="real_game_user_balance_div_id">
	<div class="form-inline">
		<div class="input-group">
			<input type="text"  name="account" class="form-control" placeholder="用户帐号">
		</div>
		<button class="btn btn-primary search-btn" type="button">查询</button>
		<button class="btn btn-primary reset" type="button">重置</button>
	</div>
	<div class="memmnyope-tb hidden" style="margin-top:10px;">
		<table class="table table-bordered">
              <tbody>
			<tr>
				<td class="text-right active">会员账号${isAgOnOff}：</td>
                <td colspan="2"><span class="text-primary account-span"></span></td>
			</tr>
			<tr>
				<td class="text-right active">账号余额：</td>
                <td colspan="2"><span class="text-danger money-span"></span></td>
			</tr>  
			<c:if test="${isAgOnOff=='on'}"><tr>
				<td width="20%" class="text-right active">AG余额：</td>
				<td width="20%"><span class="text-danger balance-1">0</span></td>
    			<td width="60%">&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="1">刷新</a>
    			&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="1" data-transtype="1">转出金额</a>
    			&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="1" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isBbinOnOff=='on'}"><tr>
				<td class="text-right active">BBIN余额：</td>
				<td><span class="text-danger balance-2">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="2">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="2" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="2" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isMgOnOff=='on'}"><tr>
				<td class="text-right active">MG余额：</td>
				<td><span class="text-danger balance-3">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="3">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="3" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="3" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isPtOnOff=='on'}"><tr>
				<td width="20%" class="text-right active">PT余额：</td>
				<td width="20%"><span class="text-danger balance-4">0</span></td>
    			<td width="60%">&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="4">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="4" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="4" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isQtOnOff=='on'}"><tr>
				<td class="text-right active">QT余额：</td>
				<td><span class="text-danger balance-5">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="5">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="5" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="5" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isAbOnOff=='on'}"><tr>
				<td class="text-right active">Allbet余额：</td>
				<td><span class="text-danger balance-6">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="6">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="6" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="6" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isOgOnOff=='on'}"><tr>
				<td class="text-right active">OG余额：</td>
				<td><span class="text-danger balance-7">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="7">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="7" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="7" data-transtype="2">转手金额</a></td>
			</tr></c:if>
			<c:if test="${isDsOnOff=='on'}"><tr>
				<td class="text-right active">DS余额：</td>
				<td><span class="text-danger balance-8">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="8">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="8" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="8" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isCq9OnOff=='on'}"><tr>
				<td class="text-right active">CQ9余额：</td>
				<td><span class="text-danger balance-9">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="9">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="9" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="9" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isJdbOnOff=='on'}"><tr>
				<td class="text-right active">JDB余额：</td>
				<td><span class="text-danger balance-11">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="11">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="11" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="11" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isTtgOnOff=='on'}"><tr>
				<td class="text-right active">TTG余额：</td>
				<td><span class="text-danger balance-92">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="92">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="92" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="92" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isMwOnOff=='on'}"><tr>
				<td class="text-right active">MW余额：</td>
				<td><span class="text-danger balance-95">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="95">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="95" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="95" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isIsbOnOff=='on'}"><tr>
				<td class="text-right active">ISB余额：</td>
				<td><span class="text-danger balance-96">0</span></td>
				<td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="96">刷新</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="96" data-transtype="1">转出金额</a>
				&nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="96" data-transtype="2">转入金额</a></td>
			</tr></c:if>
			<c:if test="${isM8OnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">M8余额：</td>
                <td><span class="text-danger balance-99">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="99">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="99" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="99" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
			<c:if test="${isM8HOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">M8H余额：</td>
                <td><span class="text-danger balance-991">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="991">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="991" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="991" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
			<c:if test="${isIbcOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">沙巴余额：</td>
                <td><span class="text-danger balance-10">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="10">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="10" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="10" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
			<c:if test="${isBgOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">BG余额：</td>
                <td><span class="text-danger balance-98">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="98">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="98" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="98" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
			<c:if test="${isVrOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">VR余额：</td>
                <td><span class="text-danger balance-97">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="97">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="97" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="97" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
			<c:if test="${isKyOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">KY余额：</td>
                <td><span class="text-danger balance-12">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="12">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="12" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="12" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
            <c:if test="${isEbetOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">EBET余额：</td>
                <td><span class="text-danger balance-13">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="13">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="13" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="13" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
            <c:if test="${isThOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">Th余额：</td>
                <td><span class="text-danger balance-14">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="14">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="14" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="14" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
             <c:if test="${isKxOnOff=='on'}"><tr>
			<tr>
                <td class="text-right active">Kx余额：</td>
                <td><span class="text-danger balance-16">0</span></td>
                <td>&nbsp;<a href="javascript:void(0)" class="refresh" data-thirdid="16">刷新</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="16" data-transtype="1">转出金额</a>
                &nbsp;&nbsp;<a href="javascript:void(0)" class="retrieve" data-thirdid="16" data-transtype="2">转入金额</a></td>
            </tr>
            </c:if>
		</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $con=$("#real_game_user_balance_div_id")
		,$account=$con.find("[name='account']")
		,curAccountId=0;
	$con.find(".search-btn").click(function(){
		var acc=$account.val();
		if(!acc){
			layer.tip("请输入会员帐号！",$account);
			return false;
		}
		$.ajax({
			url : "${base}/agent/rgub/memmny.do",
			data : {account : acc},
			success : function(data) {
				if(data.account){
					$con.find(".memmnyope-tb").removeClass("hidden");
					$con.find(".account-span").html(data.account);
					$con.find(".money-span").html(data.money);
					curAccountId = data.accountId;
					<c:if test="${isAgOnOff=='on'}">refresh(1);</c:if>
					<c:if test="${isBbinOnOff=='on'}">refresh(2);</c:if>
					<c:if test="${isMgOnOff=='on'}">refresh(3);</c:if>
					<c:if test="${isPtOnOff=='on'}">refresh(4);</c:if>
					<c:if test="${isQtOnOff=='on'}">refresh(5);</c:if>
					<c:if test="${isAbOnOff=='on'}">refresh(6);</c:if>
					<c:if test="${isOgOnOff=='on'}">refresh(7);</c:if>
					<c:if test="${isDsOnOff=='on'}">refresh(8);</c:if>
					<c:if test="${isCq9OnOff=='on'}">refresh(9);</c:if>
					<c:if test="${isIbcOnOff=='on'}">refresh(10);</c:if>
					<c:if test="${isJdbOnOff=='on'}">refresh(11);</c:if>
					<c:if test="${isTtgOnOff=='on'}">refresh(92);</c:if>
					<c:if test="${isMwOnOff=='on'}">refresh(95);</c:if>
					<c:if test="${isIsbOnOff=='on'}">refresh(96);</c:if>
					<c:if test="${isM8OnOff=='on'}">refresh(99);</c:if>
					<c:if test="${isM8HOnOff=='on'}">refresh(991);</c:if>
					<c:if test="${isBgOnOff=='on'}">refresh(98);</c:if>
					<c:if test="${isVrOnOff=='on'}">refresh(97);</c:if>
					<c:if test="${isEbetOnOff=='on'}">refresh(13);</c:if>
					<c:if test="${isThOnOff=='on'}">refresh(14);</c:if>
					<c:if test="${isKxOnOff=='on'}">refresh(16);</c:if>
				}else{
					layer.msg(data.msg||"该用户不存在");
				}
			}
		});
	});
	$con.find(".reset").click(function(){
		curAccountId=0;
		$con.find(".memmnyope-tb").addClass("hidden");
		$account.val("");
	});
	$con.find(".refresh").click(function(){
		if(!curAccountId || curAccountId==0){
			layer.msg("请选择用户");
			return false;
		}
		refresh($(this).data("thirdid"));
	});
	function refresh(thirdId) {
		var $bal=$con.find(".balance-"+thirdId).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />")
		$.ajax({
	        url:"${base}/agent/rgub/getBalance.do",
	        type:'POST',
	        data:{gameType:thirdId,accountId:curAccountId},
	        success:function(json){
				if (json.msg) {
					$bal.html(json.msg);
				}else {
					$bal.html(json.balance);
				}
	        },
	        errorFn:function(json){
	        	$bal.html(json.msg);
	        },
	        error:function(){
	        	$bal.html("链接异常");
	        }
	    });
		
	}
	$con.find(".retrieve").click(function(){
		if(!curAccountId || curAccountId==0){
			layer.msg("请选择用户");
			return false;
		}
		var thirdId=$(this).data("thirdid")
			,$bal=$con.find(".balance-"+thirdId)
			,m=$bal.html(),transType=$(this).data("transtype");
		if(transType == 2 ){
			m = $con.find(".money-span").html();
		}
		if(!/^[\d]+(\.[\d]+)?$/.test(m)){
			layer.msg("余额格式不对");
			return false;
		}
		$.ajax({
	        url:"${base}/agent/rgub/retrieveBalance.do",
	        type:'POST',
	        data:{gameType:thirdId,accountId:curAccountId,money:m,transType:transType},
	        success:function(json){
				if (json.msg) {
					$bal.html(json.msg);
				}else {
					refresh(thirdId);
				}
	        },
	        errorFn:function(json){
	        	$bal.html(json.msg);
	        },
	        error:function(){
	        	$bal.html("链接异常");
	        }
	    });
	});
});
</script>