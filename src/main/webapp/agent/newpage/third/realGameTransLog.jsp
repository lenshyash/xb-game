<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post" id="real_game_trans_log_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" value="${curDate}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="用户账号名">
			</div>
			<div class="input-group">
				<select class="form-control" name="gameType">
					<option value="" selected>全部类型</option>
					<option value="1">AG</option>
					<option value="2">BBIN</option>
					<option value="3">MG</option>
					<option value="4">QT</option>
					<option value="5">ALLBET</option>
					<option value="6">PT</option>
					<option value="7">OG</option>
					<option value="8">DS</option>
					<option value="9">CQ9</option>
					<option value="11">JDB</option>
					<option value="12">KY</option>
					<option value="10">IBC</option>
					<option value="92">TTG</option>
					<option value="95">MW</option>
					<option value="96">ISB</option>
                    <option value="991">M8H</option>
                    <option value="99">M8</option>
                    <option value="98">BG</option>
                    <option value="97">VR</option>
                    <option value="13">EBET</option>
                    <option value="14">TH</option>
                    <option value="16">KX</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary refrash-trasn-limit" type="button">更新真人额度</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" value="${curDate}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<select class="form-control" name="transStatus">
					<option value=""selected>全部状态</option>
					<option value="1">转账成功</option>
					<option value="2">转账失败</option>
					<option value="3" class="text-danger">处理中</option>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" name="transType">
					<option value=""selected>全部转入方式</option>
					<option value="2">从系统转入三方</option>
					<option value="1">从三方转入系统</option>
				</select>
			</div>
		</div>
		
		<div class="form-inline mt5px">查看额度请选择相应游戏类型：&nbsp;&nbsp;
			总额度:<span class="maxquto" style="color: blue">0</span>&nbsp;&nbsp;
			已使用额度:<span class="useQuota" style="color: green" >0</span> 
			&nbsp;&nbsp;剩余额度:<span  class="remainQuota" style="color: red">0</span> 
		</div>
		
		</br>
		<div class="unknow_order_tip" style="font-size:18px;color:blue;cursor:pointer;display:none;">有&nbsp;<font color='red' class="orders_count">n</font>&nbsp;笔未处理订单，请及时手动处理(点击过滤查询)</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	
	var $form=$("#real_game_trans_log_form_id")
		,$gameType=$form.find("[name='gameType']");
	$gameType.on("change",function(){
		var v=$(this).val();
		if(!v || v==""){
			return;
		}
		$.ajax({
			url : "${base}/agent/rtl/getTrasnLimitCache.do",
			data : {gameType:v},
			success : function(result) {
				try{
					var maxquto = result.maxQuota;
					$form.find(".useQuota").text(result.useQuota);
					if(maxquto==-1||maxquto=='-1'){
						$form.find(".maxquto").text('无限');
						$form.find(".remainQuota").text('无限')
					}else{
						$form.find(".maxquto").text(maxquto);
						$form.find(".remainQuota").text(result.remainingQuota);
					}
				}catch(e){
				}
			}
		});
	});
	$form.find(".refrash-trasn-limit").click(function(){
		$gameType.change();
	});
	Fui.addBootstrapTable({
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		url : '${base}/agent/rtl/list.do',
		onLoadSuccess:function(table,data){
			var count = 0;
			if(data.aggsData && data.aggsData.unkonwCount){
				count = data.aggsData.unkonwCount;
			}
			console.info(count);
			var $div = table.parents(".fui-box").find(".unknow_order_tip");
			if(count == 0){
				$div.css("display","none");
			}else{
				$div.find(".orders_count").html(count);
				$div.css("display","block");
			}
			
			if(!$div.attr("has_bind_event")){
				$div.bind("click",function(){
					$form.find("select[name='transStatus']").val("3");
					table.bootstrapTable('refreshOptions', {
						pageNumber : 1
					});
				});
				$div.attr("has_bind_event","true")
			}
		},
		columns : [{
			field : 'gameType',
			title : '类型',
			align : 'center',
			valign : 'bottom',
			formatter:gameTypeFormatter
		},
		 {
			field : 'transType',
			title : '转账类型',
			align : 'center',
			width : '200',
			valign : 'bottom',
			formatter:transTypeFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},
		{
			field : 'account',
			title : '本地账户',
			align : 'center',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return "系统转出:"+getTotal(rows,"transMoney",2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return "系统转出:"+(aggsData.sysOutTotal ? aggsData.sysOutTotal.toFixed(2) : "0.00");
			}
		}, 
		{
			field : 'transId',
			title : '本地转账单号',
			align : 'center',
			width : '180',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return "转入系统:"+getTotal(rows,"transMoney",1);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return "转入系统:"+(aggsData.sysInTotal ? aggsData.sysInTotal.toFixed(2) : "0.00");
			}
		}, 
		{
			field : 'transMoney',
			title : '转账金额',
			align : 'center',
			width : '180',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return "差额(转出-转入):"+getTotal(rows,"transMoney",3);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return "差额(转出-转入):"+((aggsData.sysOutTotal||0) - (aggsData.sysInTotal||0));
			}
		}, 
		{
			field : 'transStatus',
			title : '转账状态',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : transStatusFormatter
		}, 
		{
			field : 'createDatetime',
			title : '转账时间',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : Fui.formatDatetime
		}, 
		{
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	
	function getTotal(rows,itemKey,type){
		var total = 0;
		var total2 = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(r.transType == 1){
				total +=r[itemKey]||0;
			}else if(r.transType == 2){
				total2 +=r[itemKey]||0;
			}
		}
		if(type == 1){
			return total||0;
		}else if(type == 2){
			return total2||0;
		}else{
			return (total2||0)-(total||0);
		}
	}
	function gameTypeFormatter(value, row, index) {
		if(value==1){
			return "AG";
		}else if(value==2){
			return "BBIN";
		}else if(value==3){
			return "MG";
		}else if(value == 4){
			return "QT";
		}else if(value == 5){
			return "ALLBET";
		}else if(value == 6){
			return "PT";
		}else if(value == 7){
			return "OG";
		}else if(value == 8){
			return "DS";
		}else if(value == 9){
			return "CQ9";
		}else if(value == 10){
			return "IBC";
		}else if(value == 11){
			return "JDB";
		}else if(value == 92){
			return "TTG";
		}else if(value == 95){
			return "MW";
		}else if(value == 96){
			return "ISB";
		}else if(value == 99){
            return "M8";
		}else if(value == 991){
            return "M8H";
		}else if(value == 97){
            return "VR";
		}else if(value == 12){
            return "KY";
		}else if(value == 98){
            return "BG";
		}else if(value == 13){
            return "EBET";
		}else if(value == 14){
            return "TH";
		}else if(value == 16){
            return "KX";
		}else{
			return "";
		}
	}
	function transTypeFormatter(value, row, index) {
		var mesg = "",gameType = row.gameType;
		switch(row.gameType-0){
		case 1:mesg="AG";break;
		case 2:mesg="BBIN";break;
		case 3:mesg="MG";break;
		case 4:mesg="QT";break;
		case 5:mesg="ALLBET";break;
		case 6:mesg="PT";break;
		case 7:mesg="OG";break;
		case 8:mesg="DS";break;
		case 9:mesg="CQ9";break;
		case 10:mesg="IBC";break;
		case 11:mesg="JDB";break;
		case 92:mesg="TTG";break;
		case 95:mesg="MW";break;
		case 96:mesg="ISB";break;
        case 99:mesg="M8";break;
        case 991:mesg="M8H";break;
        case 98:mesg="BG";break;
        case 97:mesg="VR";break;
        case 12:mesg="KY";break;
        case 13:mesg="EBET";break;
        case 14:mesg="TH";break;
        case 16:mesg="KX";break;
		}
		if(value==1){
			return "从"+mesg+'转账到系统账户';
		}else{
			return "从系统账户转账到"+mesg;
		}
	}
	function transStatusFormatter(value, row, index){
		switch(value-0){
		case 1:return "<span class='text-success'>成功</span>";
		case 2:return "<span class='text-danger'>失败</span>";
		case 3:return "<span class='text-primary'>处理中</span>";
		}
		return "";
	}
	function operateFormatter(value, row, index) {
		if(row.transStatus != 3){
			return "";
		}
		return '<a class="open-dialog" href="${base}/agent/rtl/showHandler.do?id='+row.id+'">手动处理</a>';
	}

});
</script>