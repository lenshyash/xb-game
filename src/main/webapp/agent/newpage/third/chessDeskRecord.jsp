<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="" class="form-submit" unReloadTable="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">天豪棋牌录像url</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped" style="clear: both">
				<tbody>
					<tr>
						<td width="20%" class="text-right">${deskRecordUrl }</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>
</form>
