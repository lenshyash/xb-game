<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="startTime" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<select name="dateType" class="form-control" >
					<option value="2" selected>投注时间</option>
					<option value="1" >结算时间</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="account"value="${param.account }" placeholder="用户账号名">
			</div>
			<div class="input-group">
				<select class="form-control" name="gameType">
					<option value="">选择游戏类型</option>
					<option value="2">BBIN</option>
					<option value="10">IBC</option>
					<option value="99">M8</option>
					<option value="991">M8H</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<div class="form-group CastAutoRecord">
				<label>
					<input  type="checkbox">
				</label>
				<label for="name">自动刷新</label>
				<select class="form-control">
					<option>5</option>
					<option>10</option>
					<option>20</option>
					<option>30</option>
					<option>60</option>
					<option>120</option>
				</select>
			</div>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<select class="form-control" name="resStatus">
					<option value="">选择结算状态</option>
					<option value="1">未结算</option>
					<option value="2">全赢</option>
					<option value="3">全输</option>
					<option value="4">赢一半</option>
					<option value="5">输一半</option>
					<option value="6">和局</option>
					<option value="9">已结算</option>
				</select>
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="padding: 10px;">
	<span class="text-primary">温馨提示：</span><span class="text-danger">投注、派奖为0的是</span>
	<span class="text-success">免费游戏</span>;<br>
	<span class="text-primary">有效投注为打码量</span>
</div>
<script type="text/javascript">
$(function(){
	var  times=''
	var  time =''
	$('.CastAutoRecord input').change(function(){
		if ($('.CastAutoRecord input').is(":checked")){
			// debugger
			autoRefreshs(time,false)
			time =$('.CastAutoRecord option:selected').text()
			autoRefreshs(time,true)
		}else{
			autoRefreshs(time,false)
		}
	})

	$('.CastAutoRecord select').change(function(){
		if ($('.CastAutoRecord input').is(":checked")){
			// debugger
			autoRefreshs(time,false)
			time =$('.CastAutoRecord option:selected').text()
			autoRefreshs(time,true)
		}else {
			autoRefreshs(time,false)
		}
	})

	function autoRefreshs(time,statu){
		if (statu == true){
			times = setInterval(function(){
				$('.btn.btn-primary.fui-date-search').trigger('click')
			},time *1000 )
		}else {
			clearInterval(times)
		}

	}
})



requirejs(['jquery','bootstrap','Fui'],function(){
	function getTotal(rows,itemKey,scale){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			total +=r[itemKey]||0;
		}
		return total.toFixed(scale)+"";
	}
	function getLoss(rows,win,bet,scale){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			total +=(r[bet] - r[win])||0;
		}
		return total.toFixed(scale) +"";
	}
	Fui.addBootstrapTable({
		url : '${base}/agent/rspbet/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'platformType',
			title : '类型',
			align : 'center',
			valign : 'middle',
		},{
			field : 'account',
			title : '注单账户',
			align : 'center',
			valign : 'middle'
		},{
			field : 'bettingCode',
			title : '注单号',
			align : 'center',
			valign : 'middle'
		},{
			field : 'gameName',
			title : '体育/投注类型',
			align : 'center',
			valign : 'middle',
			formatter : SportType
		},{
			field : 'league',
			title : '联赛名称 / 队伍 / 赔率',
			align : 'center',
			valign : 'middle',
			formatter : contentFormatter
		}, {
			title : '注单结果',
			align : 'center',
			valign : 'middle',
			formatter : scoreFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			field : 'bettingMoney',
			title : '投注金额',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"bettingMoney",2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.bettingMoneyCount ? aggsData.bettingMoneyCount.toFixed(2) : "0.00";
			}
		}, {
			field : 'realBettingMoney',
			title : '有效投注金额',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"realBettingMoney",4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.0000"
				}
				return aggsData.realBettingMoneyCount ? aggsData.realBettingMoneyCount.toFixed(4) : "0.0000";
			}
		},{
			sortable:true,
			field : 'winMoney',
			title : '中奖金额',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"winMoney",4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.0000"
				}
				return aggsData.winMoneyCount ? aggsData.winMoneyCount.toFixed(4) : "0.0000";
			}
		},{
			field : 'league',
			title : '盈亏金额',
			align : 'center',
			valign : 'middle',
			formatter : lossMoney,
			pageSummaryFormat : function(rows, aggsData) {
				return getLoss(rows, "winMoney","bettingMoney", 4);
			},
			allSummaryFormat : function(rows, aggsData) {
				if (!aggsData) {
					return "0.0000"
				}
				return aggsData.winMoneyCount ? (aggsData.bettingMoneyCount - aggsData.winMoneyCount)
						.toFixed(4)
						: "0.0000";
			}
		},{
			field : 'bettingTime',
			title : '投注时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		},{
			sortable:true,
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}]
	});
	function SportType(value, row, index){
		var type = row.gameName + '<br />' +row.playName
		return type
	}
	function lossMoney(value, row, index){
		var money;
		if(row.realBettingMoney == 0){
			money = 0
		}else{
			money = (row.bettingMoney - row.winMoney)
		}
		return money.toFixed(4) + ''
	}
	function contentFormatter(value, row, index) {
		var html = "";
		if(typeof(row.parlayData) != 'undefined'){
			try{
				row.parlayData = JSON.parse(row.parlayData)
			}catch(err){
			}
			for(var i = 0; i < row.parlayData.length; i++){
				html += "联赛  " + row.parlayData[i].leagueName;
				if (row.parlayData[i].homeTeam == row.parlayData[i].awayTeam && row.parlayData[i].homeTeam != "") {
					html += row.parlayData[i].homeTeam;
				} else if (row.parlayData[i].homeTeam != "") {
					if(row.parlayData[i].score == undefined){
						row.parlayData[i].score = ''
					}else{
						row.parlayData[i].score = "("+row.parlayData[i].score+")"
					}
					html += "<br>主队 "+row.parlayData[i].homeTeam+" -VS- "+"客队 "+row.parlayData[i].awayTeam + ' <b style="color:red;">'+row.parlayData[i].score+'</b>'
				}
				if(row.parlayData[i].resScore == undefined){
					row.parlayData[i].resScore = ''
				}else{
					row.parlayData[i].resScore = "("+row.parlayData[i].resScore+")"
				}
				if(row.parlayData[i].playContent == undefined){
					row.parlayData[i].playContent = ''
				}
				html +="<br><b style='color:red;'>"+ row.parlayData[i].playName +"</b><br><b style='color:blue;'>"+ row.parlayData[i].playContent +"</b> @<font color =red > "+row.parlayData[i].odds+"</font> <span style='color:blue;'>"+row.parlayData[i].resScore+"</span><br><br>" ;
			}
		}else{
			html += "联赛  " + row.league;
			if (row.homeTeam == row.awayTeam && row.homeTeam != "") {
				html += row.homeTeam;
			} else if (row.homeTeam != "") {
				if(row.score == undefined){
					row.score = ''
				}else{
					row.score = "("+row.score+")"
				}
				html += "<br>主队 "+row.homeTeam+" -VS- "+"客队 "+row.awayTeam + ' <b style="color:red;">'+row.score+'</b>'
			}
			if(row.resScore == undefined){
				row.resScore = ''
			}else{
				row.resScore = "("+row.resScore+")"
			}
			html +="<br><b style='color:red;'>"+ row.info +"</b> @<font color =red > "+row.odds+"<font> <span style='color:blue;'>"+row.resScore+"</span>" ;
		}
		return html;
	}
	
	function scoreFormatter(value, row, index) {
		var html = "注单状态:";
		if(row.resStatus == 1){
			html += "未结算";
		}else{
			if(row.resStatus == 2){
				html += "全赢";
			}else if(row.resStatus == 3){
				html += "全输";
			}else if(row.resStatus == 4){
				html += "赢一半";
			}else if(row.resStatus == 5){
				html += "输一半";
			}else if(row.resStatus == 6){
				html += "和局";
			}else if(row.resStatus == 9){
				html += "已结算";
			}else if(row.resStatus == 10){
				html += '取消注单';
			}
		}
		
		return html;
	}
});
</script>