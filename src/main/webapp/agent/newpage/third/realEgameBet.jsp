<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="startTime" value="${startTime }" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${param.account}" placeholder="用户账号名">
			</div>
			<div class="input-group">
				<select class="form-control" name="gameType">
					<option value="">全部类型</option>
					<option value="1">AG</option>
					<option value="2">BBIN</option>
					<option value="3">MG</option>
					<option value="4">QT</option>
					<option value="6">PT</option>
					<option value="9">CQ9</option>
					<option value="11">JDB</option>
					<option value="92">TTG</option>
					<option value="95">MW</option>
					<option value="96">ISB</option>
					<option value="98">BG</option>
					<option value="13">EBET</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime }" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="padding: 10px;">
	<span class="text-primary">温馨提示：</span><span class="text-danger">投注、派奖为0的是</span>
	<span class="text-success">免费游戏</span>;<br>
	<span class="text-primary">有效投注为打码量</span>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	function getTotal(rows,itemKey,scale){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			total +=r[itemKey]||0;
		}
		return total.toFixed(scale)+"";
	}
	Fui.addBootstrapTable({
		url : '${base}/agent/rebet/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'type',
			title : '电子游艺游戏类型',
			align : 'center',
			valign : 'bottom',
			formatter : gameTypeFormat
		},{
			field : 'account',
			title : '注单账户',
			align : 'center',
			valign : 'bottom'
		}, {
			field : 'bettingCode',
			title : '投注单号',
			align : 'center',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			title : '局号',
			align : 'center',
			valign : 'middle',
			formatter : juHaoFormatter
		}, {
			field : 'gameType',
			title : '游戏名称',
			align : 'center',
			valign : 'bottom'
		},{
			field : 'bettingMoney',
			title : '投注金额',
			align : 'center',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"bettingMoney",2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.bettingMoneyCount ? aggsData.bettingMoneyCount.toFixed(2) : "0.00";
			}
		}, {
			field : 'realBettingMoney',
			title : '有效投注金额',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"realBettingMoney",4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.0000"
				}
				return aggsData.realBettingMoneyCount ? aggsData.realBettingMoneyCount.toFixed(4) : "0.0000";
			}
		}, {
			sortable:true,
			field : 'winMoney',
			title : '中奖金额',
			align : 'center',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"winMoney",4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.0000"
				}
				return aggsData.winMoneyCount ? aggsData.winMoneyCount.toFixed(4) : "0.0000";
			}
		},{
			title : '亏损金额',
			align : 'center',
			field : 'bettingLoss',
			width : '100',
			valign : 'middle',
			sortable:true,
			formatter:function lossMoneyFormatter(value,row,index){
				var buy=row.bettingMoney;
				var win=row.winMoney;
				
				if (isNaN(win)) { 
					return "未开奖";
				}
				var loss=buy-win;
				return loss.toFixed(2);
			},
			pageSummaryFormat:function(rows,aggsData){
				var bettingMoney=0,winMoney=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.bettingMoney != null){
						bettingMoney = bettingMoney+row.bettingMoney;
					}
					if(row.winMoney!=null){
						winMoney=winMoney+row.winMoney;
					}
				}
				
				var lossSum=bettingMoney-winMoney;
				return lossSum.toFixed(4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}else if(isNaN(aggsData.winMoneyCount)){
					return (aggsData.bettingMoneyCount).toFixed(4);
				}
				
				return aggsData.bettingMoneyCount-aggsData.winMoneyCount ? (aggsData.bettingMoneyCount-aggsData.winMoneyCount).toFixed(4) : "0.00";
			}
		},{
			field : 'bettingTime',
			title : '投注时间',
			align : 'center',
			valign : 'bottom',
			formatter : Fui.formatDatetime
		},{
			sortable:true,
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}]
	});
	function gameTypeFormat(value, row, index) {
		switch(value-0){
		case 1: return "AG电子游戏";
		case 2: return "BBIN电子游戏";
		case 3: return "MG电子游戏";
		case 4:return "QT";
		case 5:return "ALLBET";
		case 6:return "PT";
		case 7:return "OG";
		case 8:return "DS";
		case 9:return "CQ9";
		case 11:return "JDB";
		case 92:return "TTG";
		case 95:return "MW";
		case 96:return "ISB";
		case 98:return "BG";
		case 13:return "EBET";
		}
		return "";
	}
	function juHaoFormatter(value,row,index){
		if(row.type==3){
			return row.tableCode;
		}else if(row.type==2){
			return "";
		}else{
			if(row.gameTypeCode=="HUNTER"){
				return row.playType;
			}
			return row.tableCode;
		}
	}
});
</script>