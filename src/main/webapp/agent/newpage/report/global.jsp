<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form class="table-tool" method="post" id="report_global_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agent" placeholder="代理及下线查询" value="${agent }">
			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
			<button class="btn btn-primary search-btn fui-date-search" type="button">查询</button>
			<button class="btn btn-primary refresh-btn" type="button">刷新</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${saccount }" placeholder="会员帐号">
			</div>
			<div class="input-group" style="width: 266px;">
			<input type="text" class="form-control" name="filterAccount"  placeholder="查询过滤账号（必须用英文逗号隔开）">
			</div>
		</div>
	</div>
</form>
<table id="report_global_datagrid_tb_base"></table>
<br>
<table id="report_global_datagrid_tb_base_ex"></table>
<br>
<table id="report_global_datagrid_tb_self"></table>
<br>
<table id="report_global_datagrid_tb_other"></table>
<br>
<table id="report_global_datagrid_tb_cunqu"></table>
<br>
<div id="remark_div" style="padding: 10px;">
	<span>输赢公式：</span><span class="text-danger">投注-派奖=输赢</span><br> <span>全部输赢公式：</span><span class="text-danger">各类游戏输赢总和-代理返点-会员反水=全部输赢</span><br> <span>派奖总计：</span> <span
		class="text-danger">因为派奖是定时执行任务，会出现时间临界点情况，当存在24点前的投注，会造成些许误差</span>
	<div style="color:blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/report/report_global.js?v=3.8'],function(globalReport){
	globalReport.render('${sport}','${real}','${dianZi}','${lottery}','${markSix}','${thirdSports}','${thirdLottery}','${chess}','${balanceGem}','${dj}');
});
var replaceFont = '${replaceFont}';
</script>
<script>
</script>