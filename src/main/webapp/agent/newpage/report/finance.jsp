<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form class="fui-search table-tool" method="post" id="report_finance_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agent" placeholder="代理及下线查询">
			</div>
			<div class="input-group">
				<label class="sr-only" for="account">单用户查询</label> <input type="text" class="form-control" name="account" placeholder="单用户查询">
			</div>
			<button class="btn btn-primary search-btn fui-date-search">查询</button>
			<button class="btn btn-primary reset" type="button">重置</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#report_finance_form_id");
	Fui.addBootstrapTable({
		url :'${base}/agent/report/finance/list.do',
		columns : [ {
			field : 'account',
			title : '用户账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'accountType',
			title : '用户类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'depositTotal',
			title : '存款总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'withdrawTotal',
			title : '提款总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : withdrawFormatter
		}, {
			field : 'rebateTotal',
			title : '反水总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'rebateAgentTotal',
			title : '返点总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'depositGiftMoney',
			title : '存款赠送总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'registerGiftMoney',
			title : '注册赠送总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'withdrawGiftAmount',
			title : '扣除彩金总计',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'manualDepositTotal',
			title : '手动加款',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'manualWithdrawTotal',
			title : '手动扣款',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}]
		,onLoadSuccess:function(d,r){
			if(r && r.msg){
				layer.msg(r.msg);
			}
			$form.find(".search-btn").prop("disabled",false).html('查询');
			$form.find(".reset").prop("disabled",false).html('重置');
			return false;
		}
		,queryParams:function(params){
			$form.find(".search-btn").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			$form.find(".reset").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			return params;
		}
	});
	function withdrawFormatter(value, row, index) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		return [ '<span class="text-danger">', '</span>' ].join(Fui.toDecimal(Math.abs(value),2));
	}

	function typeFormatter(value, row, index) {
		var name = '未知';
		switch(value){
		case 1:
			name = '会员';
			break;
		case 2:
			name = '租户超级管理员';
			break;
		case 3:
			name = '租户管理员';
			break;
		case 4:
			name = '代理';
			break;
		case 5:
			name = '总代理';
			break;
		}
		return name;
	}

	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ]
					.join(Fui.toDecimal(value,2));
		}
		return [ '<span class="text-primary">', '</span>' ].join(Fui.toDecimal(value,2));
	}
});
</script>