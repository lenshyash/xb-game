<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="report_comprehensive_container_id">
	<div class="panel panel-default">
		<div class="panel-heading">
		<form class="table-tool" method="post" id="comprehensive_form">
		<div class="form-group fui-data-wrap" style="margin-bottom: 0px;">
			<div class="form-inline">
				<div class="input-group">盈亏统计  &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="refresh()" value="刷新"></div>
				<div class="input-group">
						<select class="form-control" name="reportType" id ="reportType" style="width: 105px;">
							<option value="">报表类型</option>
							<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
							<option value="2">引导</option>
						</select>
				</div>
			</div>
		</div>
		</form>
		</div>
		
		<div class="panel-body">
			<table width="100%">
				<tr>
					<td width="25%"><canvas class="comTdy" width="200" height="160"></canvas></td>
					<td width="25%"><canvas class="comYes" width="200" height="160"></canvas></td>
					<td width="50%">
						<table>
							<tr>
								<td width="10%" class="text-center"><h4 class="text-muted">今日投注</h4></td>
								<td></td>
								<td width="10%" class="text-center" ><h4 class="text-muted">昨日投注</h4></td>
							</tr>
							<tr>
								<td class="text-center" ><h3 class="text-success betNumTdy">0.00</h3></td>
								<td></td>
								<td class="text-center"><h3 class="text-success betNumYes">0.00</h3></td>
							</tr>
							<tr>
								<td class="text-center"><h4 class="text-muted">今日盈亏</h4></td>
								<td></td>
								<td class="text-center"><h4 class="text-muted">昨日盈亏</h4></td>
							</tr>
							<tr>
								<td class="text-center"><h3 class="bunkoTdy">0.00</h3></td>
								<td></td>
								<td class="text-center"><h3 class="bunkoYes">0.00</h3></td>
							</tr>
							<tr>
								<td class="text-center"><h4 class="text-muted">首冲</h4></td>
								<td class="text-center"><h4 class="text-muted">二充</h4></td>
								<td class="text-center"><h4 class="text-muted">三充</h4></td>
							</tr>
							<tr>
								<td class="text-center"><h3 class="firCom">0.00</h3></td>
								<td class="text-center"><h3 class="secCom">0.00</h3></td>
								<td class="text-center"><h3 class="thiCom">0.00</h3></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<form class="table-tool" method="post" id="comprehensive_form_id">
				<div class="form-group fui-data-wrap">
					<div class="form-inline">
						<div class="input-group">
							<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期" format="YYYY-MM"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="input-group">
							<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="线束日期" format="YYYY-MM"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
						<button class="btn btn-primary search-btn fui-date-search" type="button">查询</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-striped text-center allyear-tb"></table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">统计概况</div>
		<div class="panel-body">
			<table id="report_comprehensive_datagrid_tb"></table>
		</div>
	</div>
	<div style="padding: 10px;">
		<span>盈亏公式：</span><span class="text-danger">投注-派奖-代理返点-会员反水=盈亏</span><br> 
		<span class="text-primary">统计比较消耗系统性能，统计结果将缓存1分钟</span><br>
<%-- 		<c:if test="${reportSwitch eq 'on' }"> --%>
<!-- 			<span class="text-danger">统计概况管理只统计报表类型是普通的数据</span><br> -->
<%-- 		</c:if> --%>
	</div>
</div>
<script type="text/javascript">

function refresh(){
	window.location.href="${base}/agent/report/comprehensive/index.do";
}
requirejs(['${base}/agent/js/report/report_comprehensive.js?v=6.96'],function(compre){
	compre.render(${isDuLiCaiPiao});
});
</script>