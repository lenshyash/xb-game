<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form class="fui-search table-tool" method="post" id="report_total_statistic_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agentName" placeholder="代理及下线查询">
			</div>
			<div class="input-group">
				<select name="searchType" class="form-control">
					<option value="1">直属下级</option>
					<option value="2">所有下级</option>
				</select>
			</div>
			<button class="btn btn-primary search-btn fui-date-search">查询</button>
			<button class="btn btn-primary reset" type="button">重置</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${saccount }" placeholder="会员名称">
			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color: red;">
	备注：盈亏是针对我们计算的，负数代表我们亏钱
	<div style="padding-left: 42px;">
		会员盈亏＝有效投注－中奖金额－会员反水(有会员反水情况)；<br> 代理的盈亏＝负的代理返点；<br> 代理的团队盈亏＝有效投注－中奖金额－代理返点－会员反水(有会员反水情况)，包括代理自身返点；
		<div style="color: blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
	</div>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#report_total_statistic_form_id");
	Fui.addBootstrapTable({
		url : '${base}/agent/report/totalStatisticTwo/list.do',
		showPageSummary : true,
		showAllSummary : true,
		showFooter : true,
		columns : [ {
			field : 'agentName',
			title : '用户账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter,
			pageSummaryFormat : function(rows, aggsData) {
				return "小计:";
			},
			allSummaryFormat : function(rows, aggsData) {
				return "总计:";
			}
		}, {
			field : 'accountType',
			title : '类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'bettingAmount',
			title : '投注总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'bettingAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'bettingAmountTotal');
			}
		}, {
			field : 'awardAmount',
			title : '中奖总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'awardAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'awardAmountTotal');
			}
		}, {
			field : 'depositAmount',
			title : '存款总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositAmountTotal');
			}
		}, {
			field : 'depositGiftAmount',
			title : '存款赠送总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositGiftAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositGiftAmountTotal');
			}
		},{
			field : 'depositTimes',
			title : '存款次数',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositTimes');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositTimesTotal');
			}
		}, {
			field : 'registerGiftAmount',
			title : '注册赠送总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'registerGiftAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'registerGiftAmountTotal');
			}
		}, {
			field : 'withdrawAmount',
			title : '提款总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'withdrawAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'withdrawAmountTotal');
			}
		}, {
			field : 'rebateAmount',
			title : '反水总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'rebateAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'rebateAmountTotal');
			}
		}, {
			field : 'rebateAgentAmount',
			title : '返点总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'rebateAgentAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'rebateAgentAmountTotal');
			}
		}, {
			field : 'manualDepositAmount',
			title : '手动加款',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'manualDepositAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'manualDepositAmountTotal');
			}
		}, {
			field : 'manualWithdrawAmount',
			title : '手动扣款',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'manualWithdrawAmount');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'manualWithdrawAmountTotal');
			}
		}, {
			field : 'profit',
			title : '盈亏',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'profit');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'profitTotal');
			}
		}, {
			field : 'teamProfit',
			title : '团队盈亏',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'teamProfit');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'teamProfitTotal');
			}
		},
		{
			field : 'betCountTotal',
			title : '投注总人数',
			align : 'center',
			valign : 'middle'
		},{
			field : 'firstDepositTotal',
			title : '首冲总人数',
			align : 'center',
			valign : 'middle'
		},{
			field : 'gameShare',
			title : '返点值',
			align : 'center',
			valign : 'middle'
		}, {
			title : '上下级',
			align : 'center',
			valign : 'middle',
			formatter : optFormatter
		},
		{	field : 'agentName2',
			title : '上上级代理',
			align : 'center',
			valign : 'middle',
			hidden:true,
			visible: false
		}],
		onLoadSuccess : function(d) {
			$('.upLevel').click(function(){
				$form.find("[name='account']").val('');
				$form.find("[name='agentName']").val(this.id);
				var $table=$form.parents(".fui-box:first").data("bootstrapTable");
				if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
			});
			$('.nextLevel').click(function(){
				$form.find("[name='account']").val('');
				$form.find("[name='agentName']").val(this.id);
				var $table=$form.parents(".fui-box:first").data("bootstrapTable");
				if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
			});
			$form.find(".search-btn").prop("disabled",false).html('查询');
			$form.find(".reset").prop("disabled",false).html('重置');
			return false;
		}
		,queryParams:function(params){
			$form.find(".search-btn").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			$form.find(".reset").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			return params;
		}
	});

function getTotal(rows,itemKey){
	var total = 0;
	for(var i=0;i<rows.length;i++){
		var r = rows[i];
		if(!r[itemKey]){
			continue;
		}
		total += r[itemKey];
	}
	return aggsMoneyFormatter(total.toFixed(2));
}

function optFormatter(value, row, index) {
	value = row.agentId;
	var col = "";
	value = row.accountType;
	if (value && value != null) {
		col += '<a href="javascript:void(0)" class="uplevel"><span class="text-success upLevel" id="'+row.agentName2+'">上级</span></a>';
	}
	if (value == 4) {
		col += '<a href="javascript:void(0)" class="downlevel"><span class="text-success nextLevel" id="'+row.agentName+'">下级</span></a>';
	}
	return col;
}

function accountFormatter(value, row, index) {
	return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.agentId,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
}

var memType = {
		1 : "会员",
		2 : "租户超级管理员",
		3 : "租户管理员",
		4 : "代理",
		5 : "总代理"
	};

function typeFormatter(value, row, index) {
	return memType[value];
}
function moneyFormatter(value, row, index) {
	if (value === undefined || value == 0) {
		return "<span class='text-primary'>-</span>";
	}
	if (value > 0) {
		return [ '<span class="text-danger">', '</span>' ].join(value
				.toFixed(2));
	}
	return [ '<span class="text-primary">', '</span>' ].join(value
			.toFixed(2));
}

	function totalFormatter(aggsData,key){
		var money = "0.00";
		if(aggsData && aggsData[key]){
			money = aggsData[key].toFixed(2);
		}
		return aggsMoneyFormatter(money);
	}

	function aggsMoneyFormatter(value){
		if (value && value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
});
</script>