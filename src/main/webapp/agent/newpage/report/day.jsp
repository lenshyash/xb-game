<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form class="fui-search table-tool" method="post" id="report_day_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agent" placeholder="代理及下线查询">
			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
			<button class="btn btn-primary search-btn fui-date-search">查询</button>
			<button class="btn btn-primary reset" type="button">重置</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<label class="sr-only" for="account">单用户查询</label> <input type="text" class="form-control" name="account" value="${saccount }" placeholder="单用户查询">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#report_day_form_id");
	Fui.addBootstrapTable({
		url :'${base}/agent/report/day/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [ {
			field : 'statDate',
			title : '统计日期',
			align : 'center',
			valign : 'middle',
			formatter:Fui.formatDate,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'registerCount',
			title : '注册人数',
			align : 'center',
			valign : 'middle'
			,pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.registerCount != null){
						r = r+row.registerCount;
					}
				}
				return r;
			},
// 			allSummaryFormat:function(rows,aggsData){
// 				if(!aggsData){
// 					return "0.00"
// 				}
// 				return aggsData.depositTotalSum ? aggsData.depositTotalSum.toFixed(2) : "0.00";
// 			}
		},{
			field : 'todayfirstcom',
			title : '今日注册且首充人数(历史)',
			align : 'center',
			valign : 'middle',
			formatter :function(value, row, index){
				return value+"("+row.previousfirstcom+")";
			}
		},{
			field : 'firstcom',
			title : '首充人数',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.firstcom != null){
						r = r+row.firstcom;
					}
				}
				return r;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.firsttotal ? aggsData.firsttotal : "0";
			}
		},{
			field : 'secondcom',
			title : '二充人数',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.secondcom != null){
						r = r+row.secondcom;
					}
				}
				return r;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.secondtotal ? aggsData.secondtotal : "0";
			}
		},{
			field : 'thirdcom',
			title : '三充人数',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.thirdcom != null){
						r = r+row.thirdcom;
					}
				}
				return r;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.thirdtotal ? aggsData.thirdtotal : "0";
			}
		},{
			field : 'firstDepositAmount',
			title : '首充金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.firstDepositAmount != null){
						r = r+row.firstDepositAmount;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.firstDepositAmountSum ? aggsData.firstDepositAmountSum.toFixed(2) : "0.00";
			}
		},{
			field : 'firstWithdraw',
			title : '首提人数',
			align : 'center',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.firstWithdraw != null){
						r = r+row.firstWithdraw;
					}
				}
				return r;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.firstWithdrawSum ? aggsData.firstWithdrawSum : "0";
			}
		},{
			field : 'firstWithdrawAmount',
			title : '首提金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.firstWithdrawAmount != null){
						r = r+row.firstWithdrawAmount;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.firstWithdrawAmountSum ? aggsData.firstWithdrawAmountSum.toFixed(2) : "0.00";
			}
		},{
			field : 'depositTotal',
			title : '存款总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.depositTotal != null){
						r = r+row.depositTotal;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.depositTotalSum ? aggsData.depositTotalSum.toFixed(2) : "0.00";
			}
		}, {
			field : 'withdrawTotal',
			title : '提款总计',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.withdrawTotal != null){
						r = r+row.withdrawTotal;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.withdrawTotalSum ? aggsData.withdrawTotalSum.toFixed(2) : "0.00";
			}
		}, {
			field : 'manualDepositTotal',
			title : '手动加款',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.manualDepositTotal != null){
						r = r+row.manualDepositTotal;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.manualDepositTotalSum ? aggsData.manualDepositTotalSum.toFixed(2) : "0.00";
			}
		}, {
			field : 'manualWithdrawTotal',
			title : '手动扣款',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.manualWithdrawTotal != null){
						r = r+row.manualWithdrawTotal;
					}
				}
				return r.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.manualWithdrawTotalSum ? aggsData.manualWithdrawTotalSum.toFixed(2) : "0.00";
			}
		}, {
			field : 'loss',
			title : '日亏损',
			align : 'center',
			valign : 'middle',
			formatter:function(value,row, index){
				var depositTotal = row.depositTotal;
				var withdrawTotal = row.withdrawTotal;
				var manualDepositTotal = row.manualDepositTotal;
				var manualWithdrawTotal = row.manualWithdrawTotal;
				console.log(depositTotal,withdrawTotal,manualDepositTotal,manualWithdrawTotal);
				
				var result=depositTotal+manualDepositTotal+withdrawTotal+manualWithdrawTotal;
				
				return result.toFixed(4);
			},
			pageSummaryFormat:function(rows,aggsData){
				var r=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row != null){
						var depositTotal = row.depositTotal;
						var withdrawTotal = row.withdrawTotal;
						var manualDepositTotal = row.manualDepositTotal;
						var manualWithdrawTotal = row.manualWithdrawTotal;
						
						var result=depositTotal+manualDepositTotal+withdrawTotal+manualWithdrawTotal;
						r=r+result;
					}
				}
				return r.toFixed(4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.lossSum ? aggsData.lossSum.toFixed(4) : "0.0000";
			}
		}]
		,onLoadSuccess:function(d,r){
			if(r && r.msg){
				layer.msg(r.msg);
			}
			$form.find(".search-btn").prop("disabled",false).html('查询');
			$form.find(".reset").prop("disabled",false).html('重置');
			return false;
		}
		,queryParams:function(params){
			$form.find(".search-btn").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			$form.find(".reset").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			return params;
		}
	});
	function withdrawFormatter(value, row, index) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		return [ '<span class="text-danger">', '</span>' ].join(Fui.toDecimal(Math.abs(value),2));
	}

	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ]
					.join(Fui.toDecimal(value,2));
		}
		return [ '<span class="text-primary">', '</span>' ].join(Fui.toDecimal(value,2));
	}
});
</script>