<%@ page language="java" pageEncoding="utf-8"%>
<form class="table-tool" method="post" id="report_member_data_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${begin}" format="YYYY-MM-DD HH:mm:ss" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account }" placeholder="会员帐号">
			</div>
			<button class="btn btn-primary search-btn fui-date-search" type="button">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${end}" format="YYYY-MM-DD HH:mm:ss" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<div id="remark_div" style="margin-bottom: 10px;">
	<div style="color:blue;">统计比较消耗系统性能，统计结果将缓存1分钟。<a href="${base}/agent/report/risk/index.do" class="open-tab" title="系统风险评估">点击查看更多风险评估</a></div>
</div>
<table id="report_member_data_tb_base"></table>
<br>
<table id="report_member_data_tb_deposit"></table>
<br>
<table id="report_member_data_tb_draw"></table>
<br>
<table id="report_member_data_tb_gift"></table>
<br>
<table id="report_member_data_tb_bet"></table>
<br>
<script type="text/javascript">
requirejs(['${base}/agent/js/report/member_data.js?v=5'],function(report){
	report.render('${sport}','${real}','${dianZi}','${markSix}','${chess}');
});
</script>