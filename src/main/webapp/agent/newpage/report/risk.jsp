<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ul class="nav nav-tabs mb10px" id="report_risk_nav_tabs_warp_id">
	<li id="yunying" class="yunying active"><a href="#" data-toggle="tab" oname="yunying">运营分析</a></li>
	<li id="cunqu" class="cunqu"><a href="#" data-toggle="tab" oname="cunqu">存取款分析</a></li>
	<li id="lottery" class="lottery hidden"><a href="#" data-toggle="tab" oname="lottery">彩票分析</a></li>
	<li id="sports" class="sports hidden"><a href="#" data-toggle="tab" oname="sports">体育分析</a></li>
	<li id="real" class="real hidden"><a href="#" data-toggle="tab" oname="real">真人分析</a></li>
	<li id="egame" class="egame hidden"><a href="#" data-toggle="tab" oname="egame">电子分析</a></li>
	<li id="thirdSports" class="thirdSports hidden"><a href="#" data-toggle="tab" oname="thirdSports">三方体育分析</a></li>
	<li id="thirdLottery" class="thirdLottery hidden"><a href="#" data-toggle="tab" oname="thirdLottery">三方彩票分析</a></li>
	<li id="chess" class="chess hidden"><a href="#" data-toggle="tab" oname="chess">棋牌分析</a></li>
	<li id="esports" class="esports hidden"><a href="#" data-toggle="tab" oname="esports">电竞分析</a></li>
	<li id="hunter" class="hunter hidden"><a href="#" data-toggle="tab" oname="hunter">捕鱼王分析</a></li>
	<li id="markSix" class="markSix hidden"><a href="#" data-toggle="tab" oname="markSix">六合彩分析</a></li>
	<li id="sysLottery" class="sysLottery hidden"><a href="#" data-toggle="tab" oname="sysLottery"><c:if test="${!empty replaceFont }">${replaceFont }</c:if><c:if test="${empty replaceFont }">系统彩</c:if>分析</a></li>
	<li id="sfMarkSix" class="sfMarkSix hidden"><a href="#" data-toggle="tab" oname="sfMarkSix">十分六合彩分析</a></li>
	<li id="conversion" class="conversion hidden"><a href="#" data-toggle="tab" oname="conversion">转化分析</a></li>
</ul>
<form class="fui-search table-tool" method="post" id="report_risk_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<label class="sr-only" for="account">单用户查询</label> <input type="text" class="form-control" name="account" value="${account }" placeholder="单用户查询">
			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
			<button class="btn btn-primary search-btn fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<label class="sr-only" for="agent">代理及下级查询</label> <input type="text" class="form-control" name="agent" placeholder="代理及下线查询" value="${agent }">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['${base}/agent/js/report/report_risk.js?v=2.76'],function($risk){
	var navName = '${navName}';
	$risk.render(${gameOnOff},navName);
});
</script>
<script>var replaceFont = '${replaceFont }';</script>