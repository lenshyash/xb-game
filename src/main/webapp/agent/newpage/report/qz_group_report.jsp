<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="report_day_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${startTime} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<select name="lotCode" class="form-control">
					<c:forEach items="${lots }" var="lot">
						<option value="${lot.code }" selected="selected">${lot.name }</option>
					</c:forEach>
					<!-- <option value="SFSC" selected="selected">急速赛车</option> 
					<option value="EFC">二分彩</option> -->
				</select>
			</div>
			<button class="btn btn-primary search-btn fui-date-search">查询</button>
			<button class="btn btn-primary reset" type="button">重置</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime} 23:59:59" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<!-- <button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button> -->
			<div class="input-group">
				<label class="sr-only" for="agent">输入群组名称查询</label> <input type="text" class="form-control" name="groupName" placeholder="输入群组名称查询" value="${groupName }">
			</div>
			<div class="input-group">
				<label class="sr-only" for="agent">输入期号查询</label> <input type="text" class="form-control" name="qihao" placeholder="输入期号查询">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color: red;">
		<div style="color: blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
	</div>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#report_day_form_id");
	Fui.addBootstrapTable({
		url :'${base}/agent/report/group/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [ {
			field : 'qiHao',
			title : '期号',
			align : 'center',
			valign : 'middle'
		},{
			field : 'buyMoney',
			title : '投注金额',
			align : 'center',
			valign : 'middle'
		},{
			field : 'buyCount',
			title : '投注人数',
			align : 'center',
			valign : 'middle'
		},{
			field : 'qzGroupName',
			title : '群组名称',
			align : 'center',
			valign : 'middle'
		}]
		,onLoadSuccess:function(d,r){
			if(r && r.msg){
				layer.msg(r.msg);
			}
			$form.find(".search-btn").prop("disabled",false).html('查询');
			$form.find(".reset").prop("disabled",false).html('重置');
			return false;
		}
		,queryParams:function(params){
			$form.find(".search-btn").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			$form.find(".reset").prop("disabled",true).html('<img src="${base}/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
			return params;
		}
	});
});
</script>