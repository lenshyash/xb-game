<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
		<title>登录管理后台</title>
		<link id="bootstrap-css" rel="stylesheet" href="${base}/common/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="${base}/common/css/loginStyle.css" />
	</head>
<body>
<div class="ch-container">
	<div class="row">
		<div class="row">
			<div class="col-md-12 center login-header">
				<h2>登录管理后台</h2>
			</div>
		</div> 
		<div class="row">
			<div class="well col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
				<fieldset>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
						<input id="username" type="text" class="form-control" placeholder="用户名">
					</div>
					<div class="clearfix"></div><br>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
						<input id="password" type="password" class="form-control" placeholder="密码">
					</div><div class="clearfix"></div><br>
					<c:if test="${otp!=null && otp=='on'}">
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
						<input id="otpPwd" type="text" class="form-control" placeholder="动态口令" maxlength="6">
					</div>
					<div class="clearfix"></div><br></c:if>
					<c:if test="${googleFlag}">
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
						<input id="googleCode" type="text" class="form-control" placeholder="谷歌验证码" maxlength="6">
					</div>
					<div class="clearfix"></div><br></c:if>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-check red"></i></span>
						<input id="verifyCode" class="form-control" placeholder="验证码" type="text" maxlength="6">
						<span class="input-group-addon"><img id="verifyImg" src="${base}/agent/verifycode.do" style="cursor: pointer;" border="0" width="89" height="38" alt="点击刷新验证码"></span>
					</div>
					<div class="clearfix"></div><br>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<button type="submit" class="btn btn-primary btn-lg btn-block" id="loginBtn"><i class="glyphicon glyphicon-hand-right"></i> 登 录</button>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<c:if test="${empty showOtpKey || showOtpKey=='on'}"><div style="width:245px;margin:0 auto;"><a href="${base}/oneTimePwd.html?v=2.3" target="_blank" style="font-size:24px;color:#ff0000;">登录动态口令功能说明</a></div></c:if>
		<c:if test="${googleFlag}">
			<div style="width:600px;margin:0 auto;">
				<span style="font-size:19px;color:red">温馨提示：站点已开启谷歌验证码校验，请下载Authenticator(谷歌身份验证器)联系客服绑定验证链接后，获取验证码登陆</span>
			</div>
		</c:if>
	</div>
</div>
<div class="modal fade" id="cardModel" tabindex="-1" role="dialog" aria-labelledby="logoutLabel" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="logoutLabel">密保卡</h4>
			</div>
			<div class="modal-body">
				<form id="cardForm" class="form-horizontal">
					<div class="row">
					  <div class="col-xs-4">
					    <label class="col-sm-4 control-label"></label>
					    <div class="col-sm-6">
					      <input class="form-control" type="text" name="p">
					    </div>
					  </div>
					  <div class="col-xs-4">
					    <label class="col-sm-4 control-label"></label>
					    <div class="col-sm-6">
					      <input class="form-control" type="text" name="p">
					    </div>
					  </div>
					  <div class="col-xs-4">
					    <label class="col-sm-4 control-label"></label>
					    <div class="col-sm-6">
					      <input class="form-control" type="text" name="p">
					    </div>
					  </div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="login4Card">确认</button>
			</div>
		</div>
	</div>
</div>
</body></html>
<script src="${base}/common/jquery/jquery-1.12.4.min.js"></script>
<script src="${base}/common/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${base}/common/js/layer3/layer.min.js"></script>
<script>
$(function() {
	var $username=$("#username"),$pwd=$("#password"),$verifyCode=$("#verifyCode");
	function doLogin() {
		var account = $username.val().trim()
			,pwd = $pwd.val()
			,verifyCode=$verifyCode.val();
		if (account == '') {
			layer.tips("请输入账号",$username);
			$username.focus();
			return;
		}
		if (pwd == '') {
			layer.tips("请输入密码",$pwd);
			$pwd.focus();
			return;
		}
		$.ajax({
			type:"POST",
			url : "${base}/agent/login.do",
			data : {
				account : account,
				password : pwd,
				verifyCode : verifyCode,
				otpPwd:$("#otpPwd").val(),
				googleCode:$("#googleCode").val()
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					if(data.hasCard){
						showCardWin(data.points);
						refreshVerifyCode();
						return;
					}
					window.location.href = "${base}/agent";
				} else {// 后台异常
					layer.msg(data.msg||"后台异常，请联系管理员!");
					refreshVerifyCode();
				}
			}
		});
	}
	function refreshVerifyCode() {
		$("#verifyImg").attr("src","${base}/agent/verifycode.do?time=" + (new Date()).getTime());
		$verifyCode.val("");
	}
	$verifyCode.keyup(function(event){
	  	if(event.keyCode ==13){
		  	doLogin();
	  	}
	});
	
	$("#verifyImg").click(function(){
		refreshVerifyCode();
	})
	$username.keyup(function(event){
	  if(event.keyCode ==13){
		  if(!$pwd.val()){
			  $pwd.focus();
			  return;
		  }
		  if(!$verifyCode.val()){
			  $verifyCode.focus();
			  return;
		  }
		  doLogin();
	  }
	});
	$pwd.keyup(function(event){
	  if(event.keyCode ==13){
		  if(!$verifyCode.val()){
			  $verifyCode.focus();
			  return;
		  }
		  doLogin();
	  }
	});
	$("#loginBtn").click(function(){
		doLogin();
		return false;
	});
	function showCardWin(points){
		$("#cardModel").modal('show');
		$form = $("#cardForm");
		$form[0].reset();
		$form.find(".control-label").each(function(index,element){
			this.innerHTML = points[index];
		});
	}
	
	$("#login4Card").click(function(){
		$form = $("#cardForm");
		var code = "";
		$form.find("input[name=p]").each(function(index,element){
			var v = $.trim(this.value);
			code += v;
		});
		if(code.length != 3){
			layer.msg("请输入密保卡");
			return;
		}
		$.ajax({
			url : "${base}/agent/login4Card.do",
			data : {
				code : code
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					window.location.href = "${base}/agent";
				} else{// 后台异常
					layer.msg(data.msg||"后台异常，请联系管理员!");
				}
			}
		});
	});
});
</script>