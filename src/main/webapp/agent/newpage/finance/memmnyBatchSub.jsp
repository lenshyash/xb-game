<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/memmnyope/batchSubMoney.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">批量扣款</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">会员账号：</td>
						<td width="70%"><textarea name="accounts" style="height:150px;width:100%;"></textarea>
							<br>数据格式：多个账号以空格、半角逗号或者换行进行分隔
						</td>
					</tr>
					<tr>
						<td class="text-right active media-middle">操作金额：</td>
	                   	<td><input type="text" class="form-control" name="money" /></td>
					</tr>
					<tr>
						<td class="text-right active media-middle">操作原因：</td>
	                    <td><textarea class="form-control" name="remark"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>