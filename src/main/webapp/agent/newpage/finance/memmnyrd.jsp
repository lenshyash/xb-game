<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account}" placeholder="会员账号">
			</div>
			<select class="form-control" name="accountType">
				<option value="">账号类型</option>
				<option value="1">会员账号</option>
				<option value="9">引导账号</option>
				<option value="4">代理账号</option>
			</select>
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1"  <c:if test="${rdo}">selected</c:if>>普通</option>
				<option value="2">引导</option>
			</select>
			<div class="input-group">
				<label class="sr-only" for="type">类型</label> <select class="form-control" name="type" id="memmnyrd_type_id">
					<option value="0">全部类型</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="orderId" placeholder="订单号">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime}" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="agentName" value="${agentName}" placeholder="代理及下线查询">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="remark" value="${remark}" placeholder="备注内容">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="operatorName" value="${operatorName}" placeholder="操作员">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['text!${base}/agent/dictionary/money/record/type.do','jquery','bootstrap','Fui'],function(typeData){
	var $type=$("#memmnyrd_type_id");
	typeData=(function(data) {
		var map = {};
		if (data) {
			var h='<option value="0">全部类型</option>';
			for (var i = 0; i < data.length; i++) {
				if (!data[i]) {
					continue;
				}
				//if(data[i].type != '10' && data[i].type != '14' && data[i].type != '12') { //反水返点回滚隐藏
                    h+='<option value="'+data[i].type+'"';
                    if("${param.moneyType}"==data[i].type){
                        h+='selected=selected';
                    }
                    h+='>'+data[i].name+"</option>";
                    map[data[i].type] = data[i].name;
				//}
			}
			$type.html(h);
		}
		return map;
	}($.parseJSON(typeData)));
	if("${param.param}"=='sdkk'){
		$type.val('2');
	}else if("${param.param}"=='sdjk'){
		$type.val('1');
	}
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/memmnyrd/list.do',
		columns : [ {
			field : 'id',
			title : '编号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'type',
			title : '变动类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'beforeMoney',
			title : '变动前金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'money',
			title : '变动金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'afterMoney',
			title : '变动后余额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'createDatetime',
			title : '变动时间(系统)',
			align : 'center',
			valign : 'middle',
			width : '150px',
			formatter : Fui.formatDatetime
		}, {
			field : 'orderId',
			title : '订单号',
			align : 'center',
			valign : 'middle',
			width : '150px',
			formatter : orderFormatter
		}, {
			field : 'operatorName',
			title : '操作员',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle',
			formatter : remarkFormatter
		} ]
	});
	function typeFormatter(value, row, index) {
		if(value == 132){	//撤单时判断详情是否有和局.
			if(row.remark && row.remark.indexOf('和局')>=0){
				return "和局，退还本金";
			}
		}
		return typeData[value];
	}
	function remarkFormatter(value, row, index) {
		if (!value) {
			return "";
		}
		if (value.length < 20) {
			return value;
		}
		return [ '<a class="open-text" dialog-text="'+value+'" dialog-title="备注详情" title="'+value+'">', '</span>' ].join(value.substr(0,20)+"...");
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function orderFormatter(value, row, index){
		if(value &&value.indexOf("S")==0){
			return '<a class="open-dialog" href="${base}/agent/finance/memmnyrd/showSportOrderDesc.do?orderId='+value+'">'+value+'</a>';
		}
		if(value && (value.indexOf("L")==0 || value.indexOf("M")==0 )){
			if(row.accountType!=9){
				return '<a class="open-dialog" href="${base}/agent/finance/memmnyrd/showLotteryOrderDesc.do?orderId='+value+'">'+value+'</a>';
			}
		}
		return value;
	}
});
</script>