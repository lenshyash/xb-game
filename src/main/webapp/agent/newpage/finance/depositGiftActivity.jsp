<%@ page language="java" pageEncoding="utf-8"%>
<div>请使用新的<a class="open-tab" href="${base}/agent/finance/mnyComStrategy/index.do" title="新存款赠送策略">存款赠送策略</a>，旧版本的策略过1个月将被删除。</div>
<div>如果旧版本跟新版本的策略同时存在，则优先使用旧版本。</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/depositGiftActivity/list.do',
		columns : [ {
			field : 'depositType',
			title : '存款类型',
			align : 'center',
			valign : 'middle',
			formatter : dtFormatter
		}, {
			field : 'giveType',
			title : '赠送方式',
			align : 'center',
			valign : 'middle',
			formatter : gtFormatter
		}, {
			field : 'valueType',
			title : '赠送类型',
			align : 'center',
			valign : 'middle',
			formatter : vtFormatter
		}, {
			field : 'depositCount',
			title : '存款频率',
			align : 'center',
			valign : 'middle',
			formatter : dcFormatter
		}, {
			field : 'giveValue',
			title : '赠送额度',
			align : 'center',
			valign : 'bottom'
		}, {
			field : 'rollBackRate',
			title : '赠送比例',
			align : 'center',
			valign : 'bottom',
			formatter : rateFormatter
		}, {
			field : 'upperLimit',
			title : '赠送上限',
			align : 'center',
			valign : 'bottom',
			formatter : limitFormatter
		}, {
			field : 'betMultiple',
			title : '流水倍数',
			align : 'center',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'startTime',
			title : '开始时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'endTime',
			title : '结束时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDate
		}, {
			field : 'memo',
			title : '备注',
			align : 'center',
			valign : 'bottom',
			formatter : contentFormatter
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function dtFormatter(value, row, index) {
		if (value == 5) {
			return "在线支付";
		} else if (value == 6) {
			return "快速入款";
		} else if (value == 7) {
			return "一般入款";
		}
	}
	function gtFormatter(value, row, index) {
		if (value == 1) {
			return "固定数额";
		} else if (value == 2) {
			return "浮动比例";
		}
	}
	function vtFormatter(value, row, index) {
		if (value == 1) {
			return "彩金";
		} else if (value == 2) {
			return "积分";
		}
	}

	function limitFormatter(value, row, index) {
		var valueName = "";
		if (row.valueType == 1) {
			valueName = "彩金";
		} else if (row.valueType == 2) {
			valueName = "积分";
		}
		if (value == 0) {
			return "无上限";
		}
		return "最大赠送" + valueName + "：" + value;
	}

	function dcFormatter(value, row, index) {
		if (!value || value == 0) {
			return "每次";
		} else if (value == 1) {
			return "首充";
		}
		return "第" + value + "次";
	}

	function rateFormatter(value, row, index) {
		return value + "%";
	}
	function contentFormatter(value, row, index) {
		if (value == null || value == "") {
			return [ value ].join('');
		}
		if (value.length > 15) {
			return [ value.substr(0, 15) + '...' ].join('');
		} else {
			return [ value ].join('');
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/depositGiftActivity/zhopenClose.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/finance/depositGiftActivity/modify.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="${base}/agent/finance/depositGiftActivity/delete.do?id=',row.id,'" title="确定要删除？">删除</a>' ]
				.join('');
	}
});
</script>