<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account }" placeholder="用户账号">
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="reportType" id="reportType">
						<option value="">报表类型</option>
						<option value="1">普通类型</option>
						<option value="2">引导类型</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="accountType" id="accountType">
						<option value="">会员类型</option>
						<option value="1">会员</option>
						<option value="4">代理</option>
						<option value="9">引导</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>
<div style="padding: 10px;">
	<div style="color:red;">查询条件中的日期只针对活动开始时间进行查询</div>
</div>
<table class="fui-default-table"></table>

<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/finance/balanceGem/list.do",
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [   {
			field : 'account',
			title : '会员账号',
			align : 'center',
			formatter : accountFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'balanceGemMoney',
			title : '余额宝余额',
			align : 'center',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"balanceGemMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.bgMoney ? aggsData.bgMoney.toFixed(6) : "0.00";
			}
		},{
			field : 'balanceGemIncome',
			title : '累计收益',
			align : 'center',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"balanceGemIncome");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.bgIncome ? aggsData.bgIncome.toFixed(6) : "0.00";
			}
		},{
			title : '操作',
			align : 'center',
			formatter : operateFormatter
		}]
	});
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-tab" href="${base}/agent/balanceGemRecord/index.do?account='+row.account+'" title="余额宝今日记录">查看今日账变</a>';
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey] ){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(6)+"";
	}
});
</script>