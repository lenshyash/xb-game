<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/finance/mnyComStrategy/update.do" class="form-submit" id="mny_com_strategy_update_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改存款赠送活动</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${com.id}">
			<div style="color:#a94442;margin-top:5px;">同一个支付方式，同一时间内，同一个充值金额范围内，同一个会员等级，同种赠送频率不能存在2条赠送策略</div><table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">存款类型：</td>
						<td width="30%"><select name="depositType" class="form-control">
							<option value="5"<c:if test="${com.depositType==5}">selected</c:if>>在线支付</option>
							<option value="6"<c:if test="${com.depositType==6}">selected</c:if>>快速入款</option>
							<option value="7"<c:if test="${com.depositType==7}">selected</c:if>>一般入款</option>
							<option value="12"<c:if test="${com.depositType==12}">selected</c:if>>手动加款</option>
							<c:if test="${sysApiDeposit}"><option value="13"<c:if test="${com.depositType==13}">selected</c:if>>系统接口入款</option></c:if>
						</select></td>
						<td width="20%" class="text-right media-middle">赠送方式：</td>
						<td width="30%"><select name="giftType" class="form-control">
							<option value="1"<c:if test="${com.giftType==1}">selected</c:if>>固定数额</option>
							<option value="2"<c:if test="${com.giftType==2}">selected</c:if>>浮动比例</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">赠送类型：</td>
						<td><select name="valueType" class="form-control">
							<option value="1"<c:if test="${com.valueType==1}">selected</c:if>>彩金</option>
							<option value="2"<c:if test="${com.valueType==2}">selected</c:if>>积分</option>
						</select></td>
						<td class="text-right media-middle">赠送频率：</td>
						<td><select name="depositCount" class="form-control"<c:if test="${com.depositType==12}">style="display:none;"</c:if>>
							<option value="0"<c:if test="${com.depositCount==0}">selected</c:if>>每次充值</option>
							<option value="1"<c:if test="${com.depositCount==1}">selected</c:if>>首充</option>
							<option value="444"<c:if test="${com.depositCount==444}">selected</c:if>>每日首充</option>
							<option value="2"<c:if test="${com.depositCount==2}">selected</c:if>>第二次充值</option>
							<option value="3"<c:if test="${com.depositCount==3}">selected</c:if>>第三次充值</option>
							<option value="222"<c:if test="${com.depositCount==222}">selected</c:if>>前两次充值</option>
							<option value="333"<c:if test="${com.depositCount==333}">selected</c:if>>前三次充值</option>
						</select><div id="depositCount12" style='<c:if test="${com.depositType!=12}">display:none;</c:if>margin-top:7px;'>每次充值</div></td>
					</tr>
					<tr class="<c:if test="${com.giftType==2}">hidden </c:if>giftValue">
						<td class="text-right media-middle">赠送额度：</td>
						<td colspan="3"><input type="text" name="giftValue" class="form-control money" value="${com.giftValue}"/></td>
					</tr>
					<tr class="<c:if test="${com.giftType==1}">hidden </c:if>rollBackRate">
						<td class="text-right media-middle">赠送比例（％）：</td>
						<td><input type="text" name="rollBackRate" class="form-control money"value="${com.giftValue}" placeholder="不要输入%"/></td>
						<td class="text-right media-middle">赠送上限：</td>
						<td><input type="text" name="upperLimit" class="form-control money"value="${com.upperLimit}" placeholder="0表示无上限, 100表示最大赠送100" /></td>
					</tr>
					<tr">
						<td class="text-right media-middle">充值金额大于等于：</td>
						<td><input type="text" name="minMoney" class="form-control money"value="${com.minMoney}" placeholder="空或者0表示不限制"/></td>
						<td class="text-right media-middle">充值金额小于等于：</td>
						<td><input type="text" name="maxMoney" class="form-control money"value="${com.maxMoney}" placeholder="空或者0表示不限制"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">流水倍数：</td>
						<td colspan="3"><input type="text" name="betMultiple"value="${com.betMultiple}" class="form-control money" placeholder="(充值金额+赠送)x流水倍数=出款需要达到的投注量" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始日期：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" name="startDate" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${com.startDatetime}" />" placeholder="活动开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束日期：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" name="endDate" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${com.endDatetime}" />"  placeholder="活动结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levelList}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${levelSet.contains(le1.id)}">checked</c:if>>${le1.levelName }</label>  </c:forEach>
						<div style="color:#a94442;margin-top:3px;">全部选择和全部不选是一样，表示所有等级都可以使用</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td colspan="3"><input type="text" name="memo" class="form-control" value="${com.memo}"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#mny_com_strategy_update_form_id")
		,$rollBackRate=$form.find(".rollBackRate")
		,$giftValue=$form.find(".giftValue");
	$form.find("[name='giftType']").change(function(){
		var selval = $(this).val();
		$rollBackRate.addClass("hidden");
		$giftValue.addClass("hidden");
		if (selval == 1) {
			$giftValue.removeClass("hidden");
		} else if (selval == 2) {
			$rollBackRate.removeClass("hidden");
		}
	});$form.find("[name='depositType']").change(function(){
		var val = $(this).val();
		if(val==12){
			$form.find("[name='depositCount']").hide().val(0);
			$form.find("#depositCount12").show();
		}else{
			$form.find("[name='depositCount']").show();
			$form.find("#depositCount12").hide();
		}
	});
});
</script>