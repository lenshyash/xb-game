<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="pay_online_rd_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${startTime} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<label class="sr-only" for="type">类型</label><select class="form-control" name="type">
					<option value="0">全部类型</option>
					<option value="5">在线支付</option>
					<option value="6">快速入款</option>
					<option value="7">一般入款</option>
					<option value="1">手动加款</option>
				</select>
			</div>
			<div class="input-group">
				<label class="sr-only" for="payName">支付种类</label> 
				<select class="form-control" name="payName"data-live-search="true">
					<option value="0">支付种类</option>
				</select>
			</div>
			<div class="input-group">
				<label class="sr-only" for="status">状态</label><select class="form-control" name="status">
					<option value="0">全部状态</option>
					<option value="4" class="text-success">未处理</option>
					<option value="1" class="text-success">处理中</option>
					<option value="2" class="text-primary">充值成功</option>
					<option value="3" class="text-danger">充值失败</option>
					<option value="5" class="text-default">已过期</option>
				</select>
			</div>
			<div class="input-group">
				<label class="sr-only" for="status">处理方式</label><select class="form-control" id="handlerType" name="handlerType">
					<option value="0">全部方式</option>
					<option value="2" class="text-success">系统处理</option>
					<option value="1" class="text-danger">手动处理</option>
				</select>
			</div>
			
			<select class="form-control" name="accountType">
				<option value="">会员类型</option>
				<option value="1">会员</option>
				<option value="9">引导</option>
				<option value="4">代理</option>
			</select>
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1">普通报表</option>
				<option value="2">引导报表</option>
			</select>
			<select class="form-control" name="comTimes">
				<option value="">存款类型</option>
				<option value="1" class="text-warning">首次存款</option>
				<option value="2" class="text-danger">二次存款</option>
				<option value="3" class="text-success">三次存款</option>
			</select>
			<button class="btn btn-primary fui-date-search rechargeAutoRefresh">查询</button>
			<c:if test="${exportPerm}"><button class="btn btn-primary exportBtn" type="button">导出</button></c:if>
			<div class="form-group rechargeAutoRefresh">
				<label>
					<input  type="checkbox">
				</label>
				<label for="name">自动刷新</label>
				<select class="form-control">
					<option>5</option>
					<option>10</option>
					<option>20</option>
					<option>30</option>
					<option>60</option>
					<option>120</option>
				</select>
			</div>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime} 23:59:59" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" style="width:120px;" value="${saccount }" placeholder="会员名称">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="orderNo" style="width:200px;" value="${sorderNo }" placeholder="充值订单号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="agentName" style="width:120px;" placeholder="所属代理">
			</div>
			
			<div class="input-group">
				<input type="text" class="form-control" name="operator" style="width:120px;" value="${operator }" placeholder="操作员">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="minMoney" style="width:100px;" value="${minMoney }" placeholder="最小金额">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="maxMoney" style="width:100px;" value="${maxMoney }" placeholder="最大金额">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="opDesc" style="width:200px;" value="${opDesc }" placeholder="操作说明">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="remark" style="width:200px;" value="${remark }" placeholder="备注">
			</div>
		</div>
	</div>
</form>
<c:if test="${exportPerm}"><div class="hidden">
	<form id="pay_online_rd_export_fmId" action="${base}/agent/finance/payonlinerd/export.do" target="_blank" method="post">
		<input type="hidden" name="account"/> 
		<input type="hidden" name="type"/> 
		<input type="hidden" name="payName"/>
		<input type="hidden" name="status"/>
		<input type="hidden" name="handlerType"/>
		<input type="hidden" name="begin"/>
		<input type="hidden" name="end"/>
		<input type="hidden" name="opDesc"/>
	</form>
</div></c:if>
<table class="fui-default-table"></table>
<style>
	#prompt {
    	background-color: #f44336e0;
    	color: #f9f7f7;
    	border-color: #ccc;
	}
</style>
<script type="text/javascript">
	$(function(){
		var  times=''
		var time =''
		$('.rechargeAutoRefresh input').change(function(){
			if ($('.rechargeAutoRefresh input').is(":checked")){
				//debugger
				autoRefreshs(time,false)
				time =$('.rechargeAutoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else{
				autoRefreshs(time,false)
			}
		})

		$('.rechargeAutoRefresh select').change(function(){
			if ($('.rechargeAutoRefresh input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.rechargeAutoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else {
				autoRefreshs(time,false)
			}
		})

		function autoRefreshs(time,statu){
			if (statu == true){
				times = setInterval(function(){
					$('.btn.btn-primary.fui-date-search.rechargeAutoRefresh').trigger('click')
				},time *1000)
			}else {
				clearInterval(times)
			}

		}
	})
	function copy(e){
		//创建一个input框
		var input = document.createElement("input");
		//将指定的DOM节点添加到body的末尾
		document.body.appendChild(input);
		//设置input框的value值
		input.setAttribute("value", e);
		//选取文本域中的内容
		input.select();
		//copy的意思是拷贝当前选中内容到剪贴板
		//document.execCommand（）方法操纵可编辑内容区域的元素
		//返回值为一个Boolean，如果是 false 则表示操作不被支持或未被启用
		if (document.execCommand("copy")) {
			document.execCommand("copy");
			layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
		}
		//删除这个节点
		document.body.removeChild(input);

		// // Url2.select(); // 选择对象
		// document.execCommand("Copy"); // 执行浏览器复制命令
		// layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
	}
requirejs(['zeroclipboard','jquery','bootstrap','Fui','bootstrapSelect'],function(ZeroClipboard){
	var ht = '${handlerType}';
	if(ht){
		$("#handlerType").val('');
	}
	var $form=$("#pay_online_rd_form_id");
	<c:if test="${exportPerm}">$form.find(".exportBtn").click(function(){
		var $form1=$("#pay_online_rd_export_fmId");
		$form1.find("[name='account']").val($form.find("[name='account']").val());
		$form1.find("[name='orderNo']").val($form.find("[name='orderNo']").val());
		$form1.find("[name='status']").val($form.find("[name='status']").val())
		$form1.find("[name='begin']").val($form.find("[name='begin']").val())
		$form1.find("[name='end']").val($form.find("[name='end']").val())
		$form1.find("[name='type']").val($form.find("[name='type']").val())
		$form1.find("[name='handlerType']").val($form.find("[name='handlerType']").val())
		$form1.find("[name='payName']").val($form.find("[name='payName']").val())
		$form1.find("[name='opDesc']").val($form.find("[name='opDesc']").val())
		$form1.submit();
	});</c:if>
	$form.find("[name='type']").change(function() {
		var val=this.value;
		if(val==0){
			$form.find("[name='payName']").html('<option value="0">支付种类</option>');
			return;
		}
		$.ajax({
			url:'${base}/agent/finance/payonlinerd/payType.do',
			data:{type:val},
			dataType:'json',
			type:'post',
			success:function(j){
				var col = '<option value="0">支付种类</option>';
				if(j){
					for(var i in j){
						col += '<option id="'+j[i].id+'" value="'+j[i].name+'">'+j[i].name+'</option>';
					}
					$form.find("[name='payName']").html(col);
					$form.find("[name='payName']").selectpicker('destroy').selectpicker();
				}
			}
		});
	});
	Fui.addBootstrapTable({
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		url : "${base}/agent/finance/payonlinerd/list.do",
		columns : [ {
			field : 'orderNo',
			title : '会员/单号',
			align : 'center',
			valign : 'middle',
			formatter : orderFormatter
		}, {
			field : 'levelName',
			title : '会员等级',
			align : 'center',
			valign : 'middle',
		},{
			field : 'agentName',
			title : '所属代理',
			align : 'center',
			valign : 'middle',
		}, {
			field : 'accountType',
			title : '会员类型',
			align : 'center',
			valign : 'middle',
			formatter : accountTypeFormatter
		}, {
			field : 'reportType',
			title : '报表类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		},{
			field : 'payName',
			title : '支付平台',
			align : 'center',
			valign : 'middle',
			formatter : banktypeFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			field : 'money',
			title : '付款金额(/元)',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'money');;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.totalMoney ? aggsData.totalMoney.toFixed(2) : "0.00";
			}
		}, {
			field : 'userName',
			title : '存款人姓名',
			align : 'center',
			valign : 'middle',
			formatter : userNameFormatter
		}, {
			field : 'payAccount',
			title : '支付账号/姓名',
			align : 'center',
			valign : 'middle',
			formatter : function(value, row, index){
				if(row.depositor){
					return (value?value:"-")+"<br><span class='label label-info' style='font-size:100%'>"+row.depositor+"</span>";
				}else{
					return (value?value:"-");
				}
			}
		}, {
			sortable:true,
			field : 'createDatetime',
			title : '申请时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'modifyDatetime',
			title : '处理时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'type',
			title : '类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'stauts',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'lockFlag',
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		}, {
			field : 'modifyUser',
			title : '操作员',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'opDesc',
			title : '操作说明',
			align : 'center',
			valign : 'middle',
			formatter : descFormatter
		}, {
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle',
			formatter : remarkFormatter
		} ],
		onLoadSuccess:function($table){
			var client = new ZeroClipboard($table.find('.copy-btn'));
			client.on( "ready", function (event) {
				client.on( "copy", function (event) {
					var t= $(event.target).attr("account");
					ZeroClipboard.setData( "text/plain",t);
					layer.msg('复制成功,请按ctrl+v粘贴:<br/>'+t,{time:1000});
				});
			});
		}
	});
	function orderFormatter(value, row, index) {
		var html="";
		var text = '';
		text+='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button" onclick="copy(\''+row.account+'\')">复制</span></c:if>'
//		if(row.firstDeposit==2){
//			html=" <button class='btn btn-default' id='prompt'>首充</button>"
//		}
		if (row.comTimes == 1){
			text +="<span class='label label-warning'>首充</span>"
		}
		if (row.comTimes == 2){
			text +="<span class='label label-danger'>二充</span>"
		}
		if (row.comTimes == 3){
			text +="<span class='label label-success'>三充</span>"
		}
		if(${warnFlag} && row.abnormalFlag==1){
			text += '&nbsp;<span class="label label-danger">警</span>';
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.memberId,'" title="查看详情"><span class="text-danger">',row.account,'</span></a>'+text+'' ].join('')+"<br>"+value+html;
	}
	// function pasteFormatter(value, row, index) {
	// 	return row.linkUrl + '&nbsp;<span class="label label-primary copy-btn" role="button" account="'+row.account+'">复制</span>';
	// }
	var payOnlineType={
			1 : "人工加款",
			2 : "人工扣款",
			3 : "在线取款失败",
			4 : "在线取款",
			5 : "在线支付",
			6 : "快速入款",
			7 : "一般入款",
			8 : "数字货币入款",
			9 : "二级代理反水加钱",
			10 : "二级代理反水扣钱",
			11 : "二级代理反点加钱",
			12 : "二级代理反点扣钱",
			13 : "多级代理反点加钱",
			14 : "一多级代理反点扣钱",
			15 : "三方额度转入系统额度",
			16 : "系统额度转入三方额度",
			130 : "彩票投注",
			87 : "站内转入"
		};
	function typeFormatter(value, row, index) {
		var text ='';
		if(row.handlerType){
			text = handerTypeCons[row.handlerType]
		}
		return payOnlineType[value]+text;
	}
	function userNameFormatter(value, row, index) {
		var text = '';
		text='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button"  onclick="copy(\''+row.userName+'\')">复制</span></c:if>'
		var result = row.userName;
		if(value){
			result =  value;
		}
		return row.userName+"<br>"+text+"<br>";
	}
	function descFormatter(value, row, index) {
		var enterIndex = 7;
		if (value && value.length > enterIndex) {
			var count = Math.ceil(value.length / enterIndex);
			var strArray = [];
			for (var i = 0; i < count; i++) {
				strArray.push(value.substr(i * enterIndex, enterIndex));
			}
			return strArray.join('<br />');
		}
		return value;
	}
	function moneyFormatter(value, row, index) {
		var text=''
		text+='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button"  id ="copyMoney"  onclick="copy(\''+value+'\')">复制</span></c:if>'
		if (value === undefined) {
			return "0.00";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>'].join(Fui.toDecimal(value,2))+text;
		}
		return [ '<span class="text-primary">', '</span>' ].join("0.00");

	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey] || r.stauts != 2){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(2)+"";
	}
	
	var handerTypeCons={
			1 : '<br><span class="text-danger">手动处理</span>',
			2 :''
		};
	function statusFormatter(value, row, index) {
		if(value == 1 && row.lockFlag == 1){
			return '<span class="text-success">未处理</span>';
		}
		switch(value){
		case 1:return '<span class="text-success">处理中</span>';
		case 2:return '<span class="text-primary">充值成功</span>';
		case 3: return '<span class="text-danger">充值失败</span>';
		case 5:return '<span class="text-default">已过期</span>';
		case 4:return '<span class="text-success">未处理</span>';
		}
	}
	function operateFormatter(value, row, index) {
		if (value == 1||value == '1') {
			<c:choose>
				<c:when test="${straightFailed eq 'on'}">
					return<c:if test="${rechargePerm}">['<a class="todo-ajax" href="${base}/agent/finance/payonlinerd/lock.do?lockFlag=2&id=',row.id,'" title="锁定">锁定</a> ', '<a class="open-dialog" href="${base}/agent/finance/payonlinerd/straightFailed.do?id=',row.id,'" title="失败">失败</a>'].join('')</c:if>;
				</c:when>
				<c:otherwise>
					return<c:if test="${rechargePerm}">['<a class="todo-ajax" href="${base}/agent/finance/payonlinerd/lock.do?lockFlag=2&id=',row.id,'" title="锁定">锁定</a>'].join('')</c:if>;
				</c:otherwise>
			</c:choose>
		} else if (value == 2||value == '2') {
			if(baseInfo.account==row.modifyUser || (2==${USER_SESSION_AGENT.accountType})){
				<c:choose>
					<c:when test="${straightFailed eq 'on'}">
						return ['<a class="open-dialog" href="${base}/agent/finance/payonlinerd/handler.do?id=',row.id,'" title="成功">成功</a> ',
						'<a class="todo-ajax" href="${base}/agent/finance/payonlinerd/lock.do?lockFlag=1&id=',row.id,'" title="取消锁定">取消锁定</a>  ',
						'<a class="open-dialog" href="${base}/agent/finance/payonlinerd/payonlinerdFailed.do?id=',row.id,'" title="失败">失败</a>' ].join('');
					</c:when>
					<c:otherwise>
						return ['<a class="todo-ajax" href="${base}/agent/finance/payonlinerd/lock.do?lockFlag=1&id=',row.id,'" title="取消锁定">取消锁定</a>  ',
						'<a class="open-dialog" href="${base}/agent/finance/payonlinerd/handler.do?id=',row.id,'" title="处理申请">处理申请</a>' ].join('');
					</c:otherwise>
				</c:choose>
			}
			return "已被锁定";
		}else if((value == 3 || value == '3')&&row.status==3 ){
			if(baseInfo.account==row.modifyUser || (2==${USER_SESSION_AGENT.accountType})){
				return ['<a class="todo-ajax" href="${base}/agent/finance/payonlinerd/reback.do?id=',row.id,'" title="确认回滚该订单？">回滚</a>  ' ].join('');
			}
		} 
		if('${isCancle}' && 'on' == '${isCancle}' && (row.status == 2 ||value == '2') && row.type != 1){
			return ['<a class="open-dialog" href="${base}/agent/finance/payonlinerd/cancle.do?id=',row.id,'" title="撒消记录">撒消记录</a>' ].join('');
		}
		return "已操作";
	}
	
	function reportFormatter(value, row, index) {
		var reportTypes = {"1":"普通","2":"引导"};
		switch (value) {
		case 2:
			return [ '<span class="text-danger">', '</span>' ].join(reportTypes[value]);
			break;

		default:
			return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
			break;
		}
		
	}
	
	function accountTypeFormatter(value, row, index) {
		var reportTypes = {"1":"会员","4":"代理","9":"引导"};
		switch (value) {
		case 9:
			return [ '<span class="text-danger">', '</span>' ].join(reportTypes[value]);
			break;

		default:
			return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
			break;
		}
		
	}
	function remarkFormatter(value, row, index) {
		var content = value;
		if(content && content.length > 6){
			content = content.substring(0,6)+"...";
			
		}if(!content || content.length == 0) {
			content = "添加备注";
		}
		return ['<a class="open-dialog" href="${base}/agent/finance/payonlinerd/updateRemark.do?id=',row.id,'" title="'+value+'"><span class="text-danger">',content,'</span></a>' ].join('');
	}
	var bankType={
			ALIPAY : "支付宝",
			QQPAY : "QQ钱包",
			WEIXIN : "微信支付",
			JDPAY : "京东钱包",
			UNION : "银联钱包",
			BAIDU : "百度钱包",
			WEIXINH5BARCODE : "微信H5",
			ALIH5BARCODE : "支付宝H5",
			QQPAYBARCODE : "QQH5",
			UNIONPAY : "银联快捷",
			ALIPDD : "支付宝PDD",
			ABC: "农业银行",
			BOC: "中国银行",
			CCB: "建设银行",
			CMB: "招商银行",
			CEB: "光大银行",
			CGB: "广发银行",
			PAB: "平安银行",
			CIB: "兴业银行",
			HXB: "华夏银行",
			SHB: "上海银行",
			GDB: "广发银行",
			HZB: "杭州银行",
			BOB: "北京银行",
			NBB: "宁波银行", 
			BEA: "东亚银行", 
			CZB: "浙商银行", 
			SDB: "深圳发展银行", 
			BOS: "上海银行", 
			POST:"中国邮政",
			SRCB:"上海农商银行", 
			BOBJ:"北京银行", 
			BONB:"南京银行", 
			HXBC:"华夏银行", 
			PABC:"平安银行", 
			ABCD:"农业银行", 
			CCBD:"建设银行",
			CEBD:"光大银行", 
			NJCB:"南京银行", 
			ABOC:"农业银行",
			ICBC:"工商银行",
			BOCO:"交通银行",
			COMM:"交通银行",
			CNCB:"中信银行",
			PSBS:"中国邮政",
			PSBC:"中国邮政",
			SPDB:"上海浦东发展银行",
			CMBC:"民生银行",
			BCCB:"北京银行",
			NBCB:"宁波银行",
			CBHB:"渤海银行",
			BCOM:"交通银行",
			CEBB:"光大银行",
			PINGANBANK:"平安银行",
			CMBCHIN:"招商银行", 
			CTTIC:"中信银行",
			SPABANK:"平安银行",
			ECITIC:"中信银行",
			HKBEA:"东亚银行", 
			BJBANK:"北京银行", 
			BJRCB:"北京农商银行", 
			CITIC:"中信银行", 
			CEBBANK:"光大银行", 
			HXBANK:"华夏银行", 
			SHBANK:"上海银行", 
			BOCSH:"中国银行", 
			BOCOM:"交通银行", 
			PINGAN:"平安银行", 
			ICBCD:"工商银行",  
			CMBCD:"招商银行", 
			PSBCD:"中国邮政"
		};
	
	function banktypeFormatter(value,row,index){
		if(row.bankAccount){
			return value+"<br>"+row.bankAccount;
		}else{
			if(undefined != row.bankCode && null != row.bankCode){
				if(undefined != bankType[row.bankCode]){
					value = value+"<br><span class=\"label label-primary\" style=\"font-size:90%\">"+bankType[row.bankCode]+"</span>";
				}else{
					value = value+"<br><span class=\"label label-primary\" style=\"font-size:90%\">网银</span>";
				}
			}
		}
		return value;
	}
});
</script>