<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/mnyscorerd/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">加减积分</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">操作类型：</td>
						<td><select name="type" class="form-control">
							<option value="2" selected>添加积分</option>
							<option value="1">扣除积分</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员账号：</td>
						<td><input type="text" name="accountName" class="form-control" placeholder="会员账号" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">积分：</td>
						<td><input type="text" name="score" class="form-control digits" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">操作理由：</td>
						<td><input type="text" name="remark" class="form-control" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>