<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="money_chg_form_id" method="post">
	<div class="form-inline">
		<div class="input-group form-inline">
			<input type="text" class="form-control" name="searchText" placeholder="转出会员">
		</div>
		<div class="input-group form-inline">
			<input type="text" class="form-control" name="searchText2" placeholder="转入会员">
		</div>
		<button class="btn btn-primary select-btn" type="button">查询</button>
		<button class="btn btn-primary reset-btn" type="button">重置</button>
	</div>
	<div class="hidden memmny-div" style="margin-top:10px;"><input type="hidden" name="id"/><input type="hidden" name="id2"/>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td width="15%" class="text-right active">转出会员：</td>
                    <td width="35%" class="text-primary account-td"></td>
					<td width="15%" class="text-right active">转入会员：</td>
                    <td width="35%" class="text-primary account-td2"></td>
				</tr>
				<tr>
					<td class="text-right active">账号余额：</td>
                    <td class="text-danger money-td"></td>
					<td class="text-right active">账号余额：</td>
                    <td class="text-danger money-td2"></td>
				</tr>  
				<tr>
					<td class="text-right active media-middle">操作金额：</td>
                   	<td colspan="3"><input type="text" class="form-control" name="money" /></td>
				</tr>
				<tr class="mnyope-dml">
					<td class="text-right active media-middle">是否计算打码量：</td>
                    <td colspan="3"><label class="radio-inline"><input type="radio" name="checkBetNum" value="2" checked="checked"/>是</input></label>
                    	<label class="radio-inline"><input type="radio" name="checkBetNum" value="1"/>否</input></label>
				</tr>
				<tr class="mnyope-dmlbs">
					<td class="text-right active media-middle">打码量倍数：</td>
                   	<td colspan="3"><input type="text" class="form-control" name="betNumMultiple" value="${consumeRate}" /></td>
				</tr>
				<tr>
					<td class="text-right active media-middle">操作原因：</td>
                    <td colspan="3"><textarea class="form-control" name="remark"></textarea></td>
				</tr>
				<tr>
					<td colspan="4" class="text-center"><button class="btn btn-primary" id="affirm">确认</button></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#money_chg_form_id")
		,$searchText=$form.find("[name='searchText']")
		,$searchText2=$form.find("[name='searchText2']");
	function search(){
		var acc=myTrim($searchText.val());
		var acc2=myTrim($searchText2.val());
		if(acc==''){
			layer.msg("转出会员不能为空");
			return false;
		}
		if(acc2==''){
			layer.msg("转入会员不能为空");
			return false;
		}
		
		if(acc==acc2){
			layer.msg("同会员不能进行此操作");
			return false;
		}
		$.ajax({
			url:"${base}/agent/finance/moneychg/memmny.do?accountout="+acc+"&accountin="+acc2,
			type:"GET",
			success:function(data){
				reset();
				$searchText.val(acc);
				$form.find("[name='id']").val(data.outAcc.accountId);
				$form.find("[name='id2']").val(data.inAcc.accountId);
				$form.find(".account-td").html(data.outAcc.account);
				$form.find(".account-td2").html(data.inAcc.account);
				$form.find(".money-td").html(data.outAcc.money);
				$form.find(".money-td2").html(data.inAcc.money);
				$form.find(".memmny-div").removeClass("hidden");
			},
			dataType:"JSON",
			errorFn:function(){
				reset();
				$searchText.val(acc);
				$searchText2.val(acc2);
			}
		});
	}
	$form.find(".select-btn").click(search);
	$form.find(".reset-btn").click(reset)
	$searchText.keydown(function(e){
		if(e.keyCode==13){
			search();
			return false;
		}
	});
	function reset() {
		$searchText.val("");
		$form.find("[name='id']").val("");
		$form.find(".account-td").html("");
		$form.find(".money-td").html("");
		$form.find("[name='id2']").val("");
		$form.find(".account-td2").html("");
		$form.find(".money-td2").html("");
		$form.find("[name='money']").val("");
		$form.find("[name='remark']").val("");
		$form.find(".memmny-div").addClass("hidden")
		$form.find("[name='checkBetNum'][value='2']").click();
	}
	function myTrim(x) {
	    return x.replace(/^\s+|\s+$/gm,'');
	}
	$form.find("[name='checkBetNum']").change(function(){
		var val=$(this).val();
		if(val==1){
			$form.find(".mnyope-dmlbs").addClass("hidden");
		}else{
			$form.find(".mnyope-dmlbs").removeClass("hidden");
			$form.find("[name='betNumMultiple']").val('${consumeRate}');
		}
	});
	var saveExe=false;
	$form.submit(function(){
		$('#affirm').attr("disabled",true);
		if(saveExe){
			layer.msg("加减款申请已提交，请等一会再加款");
			return false;
		}
		saveExe=true;
		$.ajax({
			url : "${base}/agent/finance/moneychg/save.do",
			data : $form.serialize(),
			success : function(data) {
				layer.msg(data.msg||"操作成功！");
				search();
				saveExe=false;
			},
			error:function(data){
				layer.msg(data.msg||"操作失败！");
				saveExe=false;
			},
			errorFn:function(data){
				layer.msg(data.msg||"操作失败！");
				saveExe=false;
			},
			complete:function(){
				$('#affirm').removeAttr("disabled");
			}
		});
		
		return false;
	})
});
</script>