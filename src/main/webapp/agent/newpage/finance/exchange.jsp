<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="type">
					<option value="0" selected>全部类型</option>
					<option value="1">现金兑换积分</option>
					<option value="2">积分兑换现金</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="兑换配置名称">
			</div>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/finance/exchange/add.do">新增</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/exchange/list.do',
		columns : [ {
			field : 'name',
			title : '兑换配置名称',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'type',
			title : '支付类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			title : '兑换比例',
			align : 'center',
			valign : 'bottom',
			formatter : ratioFormatter
		}, {
			field : 'minVal',
			title : '单次兑换最小值',
			align : 'center',
			valign : 'bottom'
		}, {
			field : 'maxVal',
			title : '单次兑换最大值',
			align : 'center',
			valign : 'bottom'
		}, 
		<c:if test="${betNumShowFlag}">
		 {
			field : 'betNum',
			title : '打码量',
			align : 'center',
			valign : 'bottom'
		}, 
		</c:if>
		{
			field : 'status',
			title : '站点状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		}
		]
	});
	function typeFormatter(value, row, index) {
		if (value == 1) {
			return "<span class='text-primary'>现金兑换积分</span>";
		} else if (value == 2) {
			return "<span class='text-primary'>积分兑换现金</span>";
		}
		return value;
	}
	function ratioFormatter(value, row, index) {
		return row.numerator + ":" + row.denominator;
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/exchange/updStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/finance/exchange/modify.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="${base}/agent/finance/exchange/delete.do?id=',row.id,'" title="确定要删除“',row.name,'”？">删除</a>' ]
				.join('');
	}
});
</script>