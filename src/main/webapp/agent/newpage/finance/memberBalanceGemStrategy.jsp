<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD" name="begin" placeholder="开始日期"><span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/balanceGemStrategy/add.do">新增</button>
			<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/balanceGemStrategy/rule.do">配置规则</button>
			
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD" placeholder="结束日期"><span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
		</div>
	</div>
</form>
<div style="padding: 10px;">
	<div style="color:red;">查询条件中的日期只针对活动开始时间进行查询</div>
</div>
<table class="fui-default-table"></table>

<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/finance/balanceGemStrategy/list.do",
		columns : [   {
			field : 'benchmarkAnnualYield',
			title : '基准年化收益率',
			align : 'center',
			formatter : percentageFormatter
		},{
			field : 'hongyunAnnualYield',
			title : '红运年化收益率',
			align : 'center',
			formatter : percentageFormatter
		},{
			field : 'betMultiple',
			title : '流水倍数',
			align : 'center',
			formatter : betMulFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			formatter : statusFormatter
		}, {
			title : '存款金额',
			align : 'center',
			formatter : comMoneyFormatter
		}, {
			title : '操作',
			align : 'center',
			formatter : operateFormatter
		} ]
	});
	function betMulFormatter(value,row,index){
		if(!value)return "0";
		return Fui.toDecimal(value,2);
	}
	function percentageFormatter(value,row,index){
		if(!value)return "0";
		return value+"%";
	}
	function comMoneyFormatter(value, row, index){
		var min=row.minMoney,max=row.maxMoney;
		if(!min && !max){
			return "不限制";
		}
		var un = "元";
		if (row.valueType == 2) {
			un = "积分";
		}
		if(!min && max){
			return "小于等于"+max+un;
		}
		if(min && !max){
			return "大于"+min+un;
		}
		return "大于"+min+un+"<br>且小于等于"+max+un;
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/balanceGemStrategy/updStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/finance/balanceGemStrategy/modify.do?id='+row.id+'" title="修改">修改</a>    '+
				'<a class="todo-ajax" href="${base}/agent/finance/balanceGemStrategy/delete.do?id='+row.id+'" title="确定要删除？">删除</a>';
	}
});
</script>