<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form action="${base}/agent/finance/payonlinerd/doHandler.do" class="form-submit" id="pay_online_rd_hand_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">处理会员充值申请</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="id" value='${com.id }'>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">会员账号：</td>
						<td>${com.account }
							<c:if test="${onCopy }">
							<input type="text" style="position:absolute;z-index:-2;" id="copyAccount" value="${com.account }">
							<span class="label label-primary copy-btn" style="line-height: 38px;"  onclick="copy('copyAccount')">复制</span>
							</c:if>
							</td>
					</tr>
					<script>
						console.log('${com.payAccount}');
					</script>
					<tr>
						<td class="text-right">存款人姓名：</td>
						<td>${empty com.depositor?com.payAccount:com.depositor }
							<c:if test="${onCopy }">
							<input type="text" style="position:absolute;z-index:-2;" id="copyDepositor" value="${empty com.depositor?com.payAccount:com.depositor }">
							<span class="label label-primary copy-btn" style="line-height: 38px;"  onclick="copy('copyDepositor')">复制</span>
							</c:if></td>
					</tr>
					<tr>
						<td class="text-right">转账类型：</td>
						<td>${com.payName }</td>
					</tr>
					<c:if test="${com.type==7}"><tr>
						<td class="text-right">银行卡号：</td>
						<td>${card }
						<c:if test="${onCopy }"><input type="text" style="position:absolute;z-index:-2;" id="copyCard1" value="${card }">
							<span class="label label-primary copy-btn" style="line-height: 38px;"  onclick="copy('copyCard1')">复制</span></c:if></td>
					</tr>
					<tr>
						<td class="text-right">银行卡姓名：</td>
						<td>${name}</td>
					</tr></c:if>
					<c:if test="${com.type==6}"><tr>
						<td class="text-right">微信号|支付宝：</td>
						<td>${card }<c:if test="${onCopy }"><input type="text" style="position:absolute;z-index:-2;" id="copyCard2" value="${card }">
							<span class="label label-primary copy-btn" style="line-height: 38px;"  onclick="copy('copyCard2')">复制</span></c:if></td>
					</tr>
					<tr>
						<td class="text-right">账号持有人：</td>
						<td>${name}</td>
					</tr></c:if>
					<tr>
						<td class="text-right media-middle">实收金额：</td>
						<td>
							<c:if test="${com.stauts == 1 }"><input id="allVirtual" name="money"class="form-control"value="${com.money }"/></c:if>
							<c:if test="${com.stauts != 1 }"><fmt:formatNumber value="${com.money }" pattern="#,##0.00"/></c:if></td>
					</tr>
					<tr id="virtualTr1" style="display: none">
						<td class="text-right">兑换汇率：</td>
						<td><input name="rate" class="form-control" value="${com.rate }" /></td>
					</tr>
					<tr id="virtualTr2"  style="display: none">
						<td class="text-right">充值数量：</td>
						<td><input name="virtualCurrencyAmount" class="form-control" value="${com.virtualCurrencyAmount }" /></td>
					</tr>
					<tr>
						<td class="text-right">手续费：</td>
						<td><input name="fee" class="form-control" value="0" /></td>
					</tr>
					<tr>
						<td class="text-right">处理结果：</td>
						<td>					<c:choose>
						<c:when test="${straightFailed eq 'on'}">
							<select name="status" class="form-control">
								<option value="1" selected>批准</option>
							</select>
						</c:when>
						<c:otherwise>
							<select name="status" class="form-control">
								<option value="1" selected>批准</option>
								<option value="2">未通过</option>
							</select>
						</c:otherwise>
					</c:choose></td>
					</tr>
					<tr class="opDesc_tr">
						<td class="text-right">处理说明：</td>
						<td><textarea name="opDesc" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>
<script>
function copy(id){
	var Url2=document.getElementById(id);
	Url2.select(); // 选择对象
	document.execCommand("Copy"); // 执行浏览器复制命令
	layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
}
requirejs(['jquery'],function(){
	var $form=$("#pay_online_rd_hand_form_id");let type=${com.type};

/* 	$form.on("change","[name='status']",function(){
		if($(this).val()==2){
			$form.find(".opDesc_tr").removeClass("hidden");
		}else{
			$form.find(".opDesc_tr").addClass("hidden");
		}
	}); */
});
$(function(){
	var payName='${com.payName}';
	var $form=$("#pay_online_rd_hand_form_id");
	if(payName=='数字货币'){
		$("#virtualTr1").show();
		$("#virtualTr2").show();
	}
	$form.on("change","[name='rate']",function(){
		var rate = $form.find("[name='rate']").val();
		var count = $form.find("[name='virtualCurrencyAmount']").val();
		$form.find("[name='money']").val(rate*count);
	});
});
</script>