<%@ page language="java" pageEncoding="utf-8"%>
<form id="bet_num_ope_form_id" method="post" action="${base}/agent/finance/memmnyope/save.do">
	<div class="form-inline">
		<div class="input-group">
			<input type="text" class="form-control" name="searchText" placeholder="输入用户名查询">
		</div>
		<button class="btn btn-primary select-btn" type="button">查询</button>
		<button class="btn btn-primary reset-btn" type="button">重置</button>
	</div>
	<div class="hidden memmny-div" style="margin-top:10px;"><input type="hidden" name="id"/>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td width="30%" class="text-right active">会员账号：</td>
                    <td class="text-primary account-td"></td>
				</tr>
				<tr>
					<td class="text-right active">出款所需打码量：</td>
                    <td><span class="text-danger draw-need"></span><span class="text-primary">(会员当前打码量：<span class="text-danger bet-num"></span>)</span></td>
				</tr>  
				<tr>
					<td class="text-right active media-middle">操作类型：</td>
                    <td><select name="type" class="form-control"><option value="1" selected>人工增加</option><option value="2">人工扣除</option></select></td>
				</tr>
				<tr>
					<td class="text-right active media-middle">操作数额：</td>
                   	<td><input type="text" class="form-control" name="betNum" /></td>
				</tr>
				<tr>
					<td class="text-right active media-middle">操作原因：</td>
                    <td><textarea class="form-control" name="remark"></textarea></td>
				</tr>
				<tr>
					<td colspan="2" class="text-center"><button class="btn btn-primary">确认</button></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#bet_num_ope_form_id")
		,$searchText=$form.find("[name='searchText']");
	function search(){
		var acc=$searchText.val();
		if(acc==''){
			layer.msg("会员账号不能为空");
			return false;
		}
		$.ajax({
			url:"${base}/agent/finance/betnumope/meminfo.do",
			type:"POST",
			data:{"account":acc},
			success:function(data){
				switch (data.accountType) {
				case 1:
					name = "会员账号"
					break;
				case 4:
					name = "代理账号"
					break;
				case 9:
					name = "引导账号"
					break;
				case 5:
					name = "总代账号";
					break;
				default:
					name = "普通账号";
					break;
				}
				reset();
				$searchText.val(acc);
				$form.find("[name='id']").val(data.id);
				$form.find(".account-td").html(data.account+"(<span style='color:red'>"+name+"</span>)");
				$form.find(".draw-need").html(data.drawNeed);
				$form.find(".bet-num").html(data.betNum);
				$form.find(".memmny-div").removeClass("hidden");
			},
			dataType:"JSON",
			errorFn:function(){
				reset();
				$searchText.val(acc);
			}
		});
	}
	$form.find(".select-btn").click(search);
	$form.find(".reset-btn").click(reset);
	$searchText.keydown(function(e){
		if(e.keyCode==13){
			search();
			return false;
		}
	});
	function reset() {
		$searchText.val("");
		$form.find("[name='id']").val("");
		$form.find(".account-td").html("");
		$form.find(".draw-need").html('');
		$form.find(".bet-num").html('');
		$form.find("[name='betNum']").val("");
		$form.find("[name='type']").val(1).change();
		$form.find("[name='remark']").val("");
		$form.find(".memmny-div").addClass("hidden")
	}
	var saveExe=false;
	$form.submit(function(){
		if(saveExe){
			layer.msg("加减打码量申请已提交，请等一会再加款");
			return false;
		}
		saveExe=true;
		$.ajax({
			url : "${base}/agent/finance/betnumope/save.do",
			data : $form.serialize(),
			success : function(data) {
				layer.msg("操作成功！");
				search();
				saveExe=false;
			},
			error:function(){
				layer.msg("操作失败！");
				saveExe=false;
			},
			errorFn:function(){
				layer.msg("操作失败！");
				saveExe=false;
			}
		});
		return false;
	})
});
</script>