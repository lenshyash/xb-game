<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form id="memdrawrd_hand_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">处理申请</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="id" value="${draw.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="17%" class="text-right">会员账号：</td>
						<td width="30%">${draw.account }</td>
						<td width="23%" class="text-right">存取类型：</td>
						<td width="30%"><c:choose>
						   	<c:when test="${draw.type == 2}">人工扣款</c:when>
						    <c:when test="${draw.type == 4}">在线取款</c:when>
						   <c:otherwise>未知</c:otherwise>
						</c:choose></td>
					</tr>
					<tr>
						<td class="text-right">收款银行：</td>
						<td>${draw.bankName }</td>
						<td class="text-right">收款户名：</td>
						<td class="text-left"><span>${draw.userName }</span></td>
					</tr>
					<tr>
						<td class="text-right">收款银行帐号：</td>
						<td class="text-left">
						<c:if test="${accountInfo.cardNoStatus == 1 }"><span>${draw.cardNo }</span></c:if>
						<c:if test="${accountInfo.cardNoStatus == 2 }"><span style="color:red">${draw.cardNo }</span>
							<span class="label label-danger" id="cardNoStatus_span">此卡黑名单</span>
						</c:if></td>
						<td class="text-right">取款时间：</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${draw.createDatetime}" /></td>
					</tr>
					<tr>
						<td class="text-right">出款需要投注量：</td>
						<td>${account.drawNeed }</td>
						<td class="text-right">有效投注额：</td>
						<td>${account.betNum }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">是否可以取款：</td>
						<td class="media-middle">
							<c:if test="${account.drawNeed<=account.betNum }"><span class="text-success">是</span></c:if>
							<c:if test="${account.drawNeed>account.betNum }"><span class="text-danger">否</span></c:if></td>
						<td class="text-right media-middle">申请金额：</td>
						<td class="text-left"><h4 class="text-danger" style="float: left;"><fmt:formatNumber value="${draw.drawMoney }" pattern="#,##0.00"/></h4></td>
					</tr>
					<tr>
						<td class="text-right media-middle">取款总额（次数）：</td>
						<td class="text-left"><fmt:formatNumber value="${dailyData.withdrawAmount }" pattern="#,##0.00"/>（${dailyData.withdrawTimes }次）</a></td>
						<td class="text-right media-middle">充值总额（次数）：</td>
						<td class="media-middle"><fmt:formatNumber value="${dailyData.depositAmount }" pattern="#,##0.00"/>（${dailyData.depositTimes }次）</td>
					</tr>
					<tr>
						<td class="text-right media-middle">手动处理充值金额（次数）：</td>
						<td class="text-left"><h4 class="text-danger" style="float: left;">
							<fmt:formatNumber value="${dailyData.depositHandlerArtificial }" pattern="#,##0.00"/>（${dailyData.depositHandlerArtificialTimes }次）</h4>
							<c:if test="${dailyData.depositHandlerArtificial > 0 }">
								<a target="_blank" href="${base}/agent/finance/payonlinerd/index.do?begin=${dailyData.startTime}" title="详情"><span class="label label-primary" style="line-height: 38px;">详情</span></a>
							</c:if>
						</td>
						<td class="text-right media-middle">用户注册时间：</td>
						<td class="media-middle">
						<c:if test="${createDate <=3 }">
							<span class="text-danger">
						</c:if>
						<c:if test="${createDate <=7 && createDate > 3 }">
							<span class="text-warring">
						</c:if>
						<c:if test="${createDate >7 }">
							<span class="text-default">
						</c:if>
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${account.createDatetime}" />（${createDate}天）<span></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员备注信息：</td>
						<td><c:out value="${account.remark}" escapeXml="true"/></td>
						<td class="text-right media-middle">处理结果：</td>
						<td><select name="status" class="form-control">
							<option value="1" selected>批准</option>
							<option value="2">未通过</option>
						</select></td>
					</tr>
					<c:if test="${!empty payments and paymentPerm}">
						<tr class="eremark">
							<td class="text-right">代付方式：</td>
							<td colspan="3"><select name="payId" class="form-control">
							<option value="">不使用代付</option>
								<c:forEach items="${payments}" var="pay"><option value="${pay.id }">${pay.payName}(${pay.merchantCode })</option></c:forEach>
						</select></td>
						</tr>
					</c:if>
					<tr class="eremark">
						<td class="text-right">备注：</td>
						<td colspan="3"><input name="remark" class="form-control"></td>
					</tr>
					<c:if test="${needPassword == 'on' }">
					<tr class="eremark">
						<td class="text-right">代付密码：</td>
						<td colspan="3"><input name="paymentPassword" class="form-control"></td>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<span class="text-danger" style="float: left;">提示：充值、提款数据来源于最后一次会员成功取款的时间(<font color="black">${dailyData.startTime}</font>)到当前时间的数据</span>
			<button class="btn btn-danger open-tab fui-close" type="button" data-refresh="true" title="会员数据概况"
			url="${base}/agent/report/memberData/index.do?account=${draw.account}&begin=${dailyData.startDateTime}&end=${curDateTime}">查看打码详情</button>
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
requirejs([,'jquery','bootstrap','Fui'],function(){
	var $form=$("#memdrawrd_hand_form_id");
	/* $form.on("change","[name='status']",function(){
		if($(this).val()==2){
			$form.find(".eremark").removeClass("hidden");
		}else{
			$form.find(".eremark").addClass("hidden");
		}
	}); */
	$form.submit(function(){
		var cardNoStatus = '${accountInfo.cardNoStatus}';
		var cardNo = '${draw.cardNo }';
		
		if(cardNoStatus && cardNoStatus == '2'){
			layer.confirm('该会员银行卡号'+cardNo+'已经被禁用，是否继续出款？', {
				btn : [ '继续','取消' ]
			}, function() {
				confirmCommit();
			});
		}else{
			confirmCommit();
		}
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/finance/memdrawrd/doHandler.do",
			data : $form.serialize(),
			success : function(result) {
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
});
</script>