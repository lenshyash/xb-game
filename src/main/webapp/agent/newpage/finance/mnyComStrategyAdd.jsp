<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/mnyComStrategy/addSave.do" class="form-submit" id="mny_com_strategy_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加存款赠送活动</h4>
		</div>
		<div class="modal-body">
			<div style="color:#a94442;margin-top:5px;">同一个支付方式，同一时间内，同一个充值金额范围内，同一个会员等级，同种赠送频率不能存在2条赠送策略</div><table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">存款类型：</td>
						<td width="30%"><select name="depositType" class="form-control">
							<option value="5" selected>在线支付</option>
							<option value="6">快速入款</option>
							<option value="7">一般入款</option>
							<option value="12">手动加款</option>
							<c:if test="${sysApiDeposit}"><option value="13">系统接口入款</option></c:if>
						</select></td>
						<td width="20%" class="text-right media-middle">赠送方式：</td>
						<td width="30%"><select name="giftType" class="form-control">
							<option value="1" selected>固定数额</option>
							<option value="2">浮动比例</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">赠送类型：</td>
						<td><select name="valueType" class="form-control">
							<option value="1" selected>彩金</option>
							<option value="2">积分</option>
						</select></td>
						<td class="text-right media-middle">赠送频率：</td>
						<td><select name="depositCount" class="form-control">
							<option value="0" selected>每次充值</option>
							<option value="1">首充</option>
							<option value="444">每日首充</option>
							<option value="2">第二次充值</option>
							<option value="3">第三次充值</option>
							<option value="222">前两次充值</option>
							<option value="333">前三次充值</option>
						</select><div id="depositCount12" style="display:none;margin-top:7px;">每次充值</div></td>
					</tr>
					<tr class="giftValue">
						<td class="text-right media-middle">赠送额度：</td>
						<td colspan="3"><input type="text" name="giftValue" class="form-control money" /></td>
					</tr>
					<tr class="hidden rollBackRate">
						<td class="text-right media-middle">赠送比例（％）：</td>
						<td><input type="text" name="rollBackRate" class="form-control money" placeholder="不要输入%"/></td>
						<td class="text-right media-middle">赠送上限：</td>
						<td><input type="text" name="upperLimit" class="form-control money" placeholder="0表示无上限, 100表示最大赠送100" /></td>
					</tr>
					<tr">
						<td class="text-right media-middle">充值金额大于等于：</td>
						<td><input type="text" name="minMoney" class="form-control money" placeholder="默认为0" value="0"/></td>
						<td class="text-right media-middle">充值金额小于等于：</td>
						<td><input type="text" name="maxMoney" class="form-control money" placeholder="默认为1000万" value="10000000"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">流水倍数：</td>
						<td colspan="3"><input type="text" name="betMultiple" class="form-control money" placeholder="(充值金额+赠送)x流水倍数=出款需要达到的投注量" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始日期：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" name="startDate" value="${startDate}" placeholder="活动开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
						<td class="text-right media-middle">结束日期：</td>
						<td><div class="input-group">
							<input type="text" class="form-control fui-date required" name="endDate" value="${endDate}"  placeholder="活动结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员等级：</td>
						<td colspan="3"><c:forEach items="${levelList}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }">${le1.levelName }</label>  </c:forEach>
						<div style="color:#a94442;margin-top:3px;">全部选择和全部不选是一样，表示所有等级都可以使用</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td colspan="3"><input type="text" name="memo" class="form-control" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#mny_com_strategy_add_form_id")
		,$rollBackRate=$form.find(".rollBackRate")
		,$giftValue=$form.find(".giftValue");
	$form.find("[name='giftType']").change(function(){
		var selval = $(this).val();
		$rollBackRate.addClass("hidden");
		$giftValue.addClass("hidden");
		if (selval == 1) {
			$giftValue.removeClass("hidden");
		} else if (selval == 2) {
			$rollBackRate.removeClass("hidden");
		}
	});
	$form.find("[name='depositType']").change(function(){
		var val = $(this).val();
		if(val==12){
			$form.find("[name='depositCount']").hide().val(0);
			$form.find("#depositCount12").show();
		}else{
			$form.find("[name='depositCount']").show();
			$form.find("#depositCount12").hide();
		}
	});
});
</script>