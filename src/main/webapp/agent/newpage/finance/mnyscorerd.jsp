<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" value="${startTime}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account}" placeholder="会员账号">
			</div>
			<div class="input-group">
				<label class="sr-only" for="type">类型</label> <select class="form-control" name="type" id="mnyscorerd_type_id">
					<option value="0">全部类型</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/mnyscorerd/add.do">加减积分</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" value="${endTime}" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['text!${base}/agent/dictionary/score/record/type.do','jquery','bootstrap','Fui'],function(typeData){
	var $type=$("#mnyscorerd_type_id");
	typeData=(function(data) {
		var map = {};
		if (data) {
			var h='<option value="0">全部类型</option>';
			for (var i = 0; i < data.length; i++) {
				if (!data[i]) {
					continue;
				}
				h+='<option value="'+data[i].type+'">'+data[i].name+"</option>";
				map[data[i].type] = data[i].name;
			}
			$type.html(h);
		}
		return map;
	}($.parseJSON(typeData)));
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/mnyscorerd/list.do',
		columns : [ {
			field : 'id',
			title : '编号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'type',
			title : '变动类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'beforeScore',
			title : '变动前积分',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'score',
			title : '变动积分',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'afterScore',
			title : '变动后积分',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'createDatetime',
			title : '变动时间(系统)',
			align : 'center',
			valign : 'middle',
			width : '150px',
			formatter : Fui.formatDatetime
		}, {
			field : 'remark',
			title : '备注',
			align : 'left',
			valign : 'middle'
		} ]
	});
	function typeFormatter(value, row, index) {
		return typeData[value];
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}

});
</script>