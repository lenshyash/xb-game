<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form action="${base}/agent/finance/payonlinerd/doCancle.do" class="form-submit" id="pay_online_rd_cancle_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">撒消会员充值记录</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="id" value='${com.id }'>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">会员账号：</td>
						<td class="text-left">${com.account }</td>
					</tr>
					<tr>
						<td class="text-right">存款人姓名：</td>
						<td class="text-left">${com.depositor }</td>
					</tr>
					<tr>
						<td class="text-right">转账类型：</td>
						<td class="text-left">${com.payName }</td>
					</tr>
					<tr>
						<td class="text-right media-middle">实收金额：</td>
						<td class="text-left">
							<c:if test="${com.stauts == 1 }"><input name="money"class="form-control"value="${com.money }"/></c:if>
							<c:if test="${com.stauts != 1 }"><fmt:formatNumber value="${com.money }" pattern="#,##0.00"/></c:if></td>
					</tr>
					<tr class="opDesc_tr">
						<td class="text-right">处理说明：</td>
						<td class="text-left"><textarea name="opDesc" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>
<script>
requirejs(['jquery'],function(){
	var $form=$("#pay_online_rd_cancle_form_id");
	/**
	$form.on("change","[name='status']",function(){
		if($(this).val()==2){
			$form.find(".opDesc_tr").removeClass("hidden");
		}else{
			$form.find(".opDesc_tr").addClass("hidden");
		}
	});**/
});
</script>