<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form action="${base}/agent/finance/memdrawrd/doPaymentHandler.do" class="form-submit" id="pay_online_rd_hand_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">处理代付提款申請</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="id" value='${id }'>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="text-right">处理结果：</td>
						<td><select name="status" class="form-control">
								<option value="2" selected>批准</option>
								<option value="3">未通过</option>
						</select></td>
					</tr>
					<tr class="opDesc_tr">
						<td class="text-right">处理说明：</td>
						<td><textarea name="opDesc" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>
<script>
requirejs(['jquery'],function(){
	var $form=$("#pay_online_rd_hand_form_id");
/* 	$form.on("change","[name='status']",function(){
		if($(this).val()==2){
			$form.find(".opDesc_tr").removeClass("hidden");
		}else{
			$form.find(".opDesc_tr").addClass("hidden");
		}
	}); */
});
</script>
