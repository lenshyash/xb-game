<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/depositset/savefast.do" class="form-submit" id="depositset_fast_edit_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">编辑快速入款方式</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${fast.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">快速入款方式：</td>
						<td width="30%"><select name="payPlatformId" class="form-control">
							<c:forEach items="${payCombos }" var="pc"><option value="${pc.id }"<c:if test="${pc.id==fast.payPlatformId }">selected</c:if>>${pc.name }</option></c:forEach>
						</select></td>
						<td width="15%" class="text-right media-middle">账号：</td>
						<td><input placeholder="请输入正确的账号" name="payAccount" value="${fast.payAccount }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">账户姓名：</td>
						<td><input placeholder="请输入正确的账户姓名" name="payUserName" value="${fast.payUserName }" class="form-control" /></td>
						<td class="text-right media-middle">二维码图片：</td>
						<td><input placeholder="请输入正确的二维码图片" type="text" value="${fast.qrCodeImg }" name="qrCodeImg" class="form-control regexp" regexp="[^<]+" tip="请输入图片地址"></td>
					</tr>
					<tr id="virtualTr1" style="display:none">
						<td class="text-right media-middle">钱包类型：</td>
						<td><input placeholder="请输入正确的钱包类型" name="walletType"   value="${fast.walletType }" class="form-control" /></td>
						<td class="text-right media-middle">当前兑换汇率：</td>
						<td><input placeholder="请输入准确的货币汇率，即1数字货币可兑换多少等额人民币"  value="${fast.rate }" name="rate" class="form-control money" /></td>
					</tr>
					<tr id="virtualTr2" style="display:none">
						<td class="text-right media-middle">货币交易平台地址：</td>
						<td colspan="3"><input placeholder="请输入正确的交易平台地址" type="text"  value="${fast.platformAddress }" name="platformAddress" class="form-control regexp" regexp="[^<]+" tip="请输入图片地址"></td>
					</tr>

					<tr>
						<td class="text-right media-middle">最小支付金额：</td>
						<td><input placeholder="请输入正确的最小支付金额" name="min" value="${fast.min }" class="form-control money" /></td>
						<td class="text-right media-middle">最大支付金额：</td>
						<td><input placeholder="请输入正确的最大支付金额" name="max" value="${fast.max }" class="form-control money" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td class="text-left"><input placeholder="排序越小,排的越前面" name="sortNo" value="${fast.sortNo }" class="form-control" type="number"/></td>
						<td class="text-right media-middle">图标地址：</td>
						<td><input placeholder="请输入正确的图标地址" name="icon" value="${fast.icon }" class="form-control regexp" regexp="[^<]+" tip="请输入图标地址"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="1"<c:if test="${1==fast.status }">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${2==fast.status }">selected</c:if>>启用</option>
						</select></td>
						<td class="text-right media-middle">前端显示名称：</td>
						<td><input placeholder="为空时，关闭此项功能" name="frontLabel" value="${fast.frontLabel }"class="form-control regexp" regexp="[^<]+" tip="请输入前端显示名称"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${not empty glevelIds && glevelIds.contains(le1.id) }">checked</c:if>>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">扫码说明：</td>
						<td colspan="3"><textarea name="qrcodeDesc" class="form-control">${fast.qrcodeDesc }</textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付时间段：</td>
						<td>
							<input type="text" class="form-control fui-date" name="payStarttime" format="HH:mm:ss" style="width:48%;float:left;" value="${fast.payStarttime}" placeholder="开始日期">-	
							<input type="text" class="form-control fui-date" name="payEndtime" format="HH:mm:ss" style="width: 48%;float:right;" value="${fast.payEndtime}" placeholder="结束日期">
						</td>
						<td class="text-right media-middle">随机显示：</td>
						<td>
							<select name="randomStatus" class="form-control">
								<option value="1"<c:if test="${1==fast.randomStatus }">selected</c:if>>禁用</option>
								<option value="2"<c:if test="${2==fast.randomStatus }">selected</c:if>>启用</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<span style="float:left;" class="text-danger">温馨提示：开启随机显示的相同充值类型仅出现一个，禁用照常显示</span>
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	$(function(){
		var $form=$("#depositset_fast_edit_form_id"),
		$payp=$form.find("[name='payPlatformId']");
		if($payp.find("option:selected").text()=="数字货币"){
			$("#virtualTr1").show();
			$("#virtualTr2").show();
			$("#accountTd").text("钱包地址:");
		}
	});

</script>