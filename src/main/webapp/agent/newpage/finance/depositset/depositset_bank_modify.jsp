<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/depositset/savebank.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">编辑银行入款</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${bank.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">一般入款方式：</td>
						<td class="text-left"><select name="payPlatformId" class="form-control">
							<c:forEach items="${payCombos }" var="pc"><option value="${pc.id }"<c:if test="${pc.id==bank.payPlatformId }">selected</c:if>>${pc.name }</option></c:forEach>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">银行卡号：</td>
						<td class="text-left"><input placeholder="请输入正确的银行卡号" name="bankCard"  value="${bank.bankCard }"class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">持卡人姓名：</td>
						<td class="text-left"><input placeholder="请输入正确的持卡人姓名" name="creatorName" value="${bank.creatorName }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">银行地址：</td>
						<td class="text-left"><input placeholder="请输入正确的银行地址" name="bankAddress"  value="${bank.bankAddress }"class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最小支付金额：</td>
						<td class="text-left"><input placeholder="请输入正确的银行地址" name="min"  value="${bank.min }"class="form-control money" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最大支付金额：</td>
						<td class="text-left"><input placeholder="请输入正确的最大支付金额" name="max" value="${bank.max }" class="form-control money" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">图标地址：</td>
						<td class="text-left"><input placeholder="请输入正确的图标地址" name="icon" value="${bank.icon }" class="form-control regexp" regexp="[^<]+" tip="请输入图标地址"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td class="text-left"><input placeholder="排序越小,排的越前面" name="sortNo" value="${bank.sortNo }" class="form-control" type="number"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${not empty glevelIds && glevelIds.contains(le1.id) }">checked</c:if>>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td class="text-left"><select name="status" class="form-control">
							<option value="1"<c:if test="${1==bank.status }">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${2==bank.status }">selected</c:if>>启用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">入款备注：</td>
						<td colspan="3"><textarea name="bankDesc" class="form-control">${bank.bankDesc }</textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付时间段：</td>
						<td>
							<input type="text" class="form-control fui-date" name="payStarttime" format="HH:mm:ss" style="width:48%;float:left;" value="${bank.payStarttime}" placeholder="开始日期">-	
							<input type="text" class="form-control fui-date" name="payEndtime" format="HH:mm:ss" style="width: 48%;float:right;" value="${bank.payEndtime}" placeholder="结束日期">
						</td>	
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>