<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-tab" url="${base}/agent/finance/depositset/index.do">在线支付设置</button>
		<button id='depositset_fast_tabId' class="btn btn-primary open-tab" url="${base}/agent/finance/depositset/index.do?type=3">快速入款设置</button>
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/finance/depositset/addBank.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/finance/depositset/banklst.do",
		columns :[ {
			field : 'payName',
			title : '银行名称',
			align : 'center',
			width : '140px',
			valign : 'middle'
		}, {
			field : 'bankCard',
			title : '银行卡号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'creatorName',
			title : '开户名',
			align : 'center',
			width : '200px',
			valign : 'middle'
		}, {
			field : 'bankAddress',
			title : '开户行地址',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'min',
			title : '最小金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'max',
			title : '最大金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'icon',
			title : '图标地址',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		}]
	});
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/depositset/updBankStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/finance/depositset/modifyBank.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="${base}/agent/finance/depositset/delbank.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
});
</script>