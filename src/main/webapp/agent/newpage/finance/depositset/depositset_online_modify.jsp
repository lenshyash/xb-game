<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/depositset/saveonline.do" class="form-submit" id="depositset_online_modify_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改在线支付方式</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${online.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">在线支付方式：</td>
						<td width="30%"><select name="payPlatformId" class="form-control selectpicker" data-live-search="true">
							<c:forEach items="${payCombos }" var="pc" varStatus="stat1"><option value="${pc.id }" iconCss="${pc.iconCss}"<c:if test="${online.payPlatformId==pc.id}">selected</c:if>>${pc.name }</option></c:forEach>
						</select></td>
						<td width="20%" class="text-right media-middle">商户编码：</td>
						<td width="30%"><input placeholder="请输入正确的商户编码" name="merchantCode" value="${online.merchantCode }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">商户密匙/商户识别码：</td>
						<td colspan="3"><input placeholder="商户密匙/商户识别码/智付3.0私钥" name="merchantKey" value="${online.merchantKey }" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付端账号：</td>
						<td colspan="3"><input placeholder="国付宝终端号/融宝卖家邮箱/环迅交易账号/智付3.0公钥" name="account" value="${online.account }" class="form-control"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">APPID：</td>
						<td><input placeholder="付啦支付APPID" name="appid" value="${online.appid }" class="form-control"></td>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="1"<c:if test="${online.status==1}">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${online.status==2}">selected</c:if>>启用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最小支付金额：</td>
						<td><input placeholder="请输入正确的最小支付金额" name="min" value="${online.min }" class="form-control money" /></td>
						<td class="text-right media-middle">最大支付金额：</td>
						<td><input placeholder="请输入正确的最大支付金额" name="max" value="${online.max }" class="form-control money" /></td>
					</tr>
					<tr><td class="text-right media-middle">接口域名：</td>
						<td><input placeholder="请输入正确的接口域名" type="text" name="url" value="${online.url }" class="form-control"></td>
						<td class="text-right media-middle">支付图标地址：</td>
						<td><input placeholder="请输入正确的支付图标地址" name="icon" value="${online.icon }" class="form-control regexp" regexp="[^<]+" tip="请输入图标地址"/></td>
					</tr>
					<tr class="hidden third-pay">
						<td class="text-right media-middle">第三方支付方式：</td>
						<td colspan="3">
							<select name="payType" class="form-control">
								<option value="1"<c:if test="${online.payType==1}">selected</c:if>>收银台</option>
								<option value="3"<c:if test="${online.payType==3}">selected</c:if>>单微信</option>
								<option value="4"<c:if test="${online.payType==4}">selected</c:if>>单支付宝（请确认商户支持此方式在选择）</option>
								<option value="5"<c:if test="${online.payType==5}">selected</c:if>>单QQ钱包（请确认商户支持此方式在选择）</option>
								<option value="6"<c:if test="${online.payType==6}">selected</c:if>>单京东扫码（请确认商户支持此方式在选择）</option>
								<option value="7"<c:if test="${online.payType==7}">selected</c:if>>百度钱包（请确认商户支持此方式在选择）</option>
								<option value="8"<c:if test="${online.payType==8}">selected</c:if>>银联扫码（请确认商户支持此方式在选择）</option>
								<option value="9"<c:if test="${online.payType==9}">selected</c:if>>银联快捷（请确认商户支持此方式在选择）</option>
								<option value="10"<c:if test="${online.payType==10}">selected</c:if>>QQ条码（请确认商户支持此方式在选择）</option>
								<option value="11"<c:if test="${online.payType==11}">selected</c:if>>微信H5条形码（请确认商户支持此方式在选择）</option>
								<option value="12"<c:if test="${online.payType==12}">selected</c:if>>支付宝H5条形码（请确认商户支持此方式在选择）</option>
								<option value="13"<c:if test="${online.payType==13}">selected</c:if>>支付宝PDD（请确认商户支持此方式在选择）</option>
							</select>
						</td>
					<tr class="pay-gateway">
						<td class="text-right media-middle">支付网关：</td>
						<td colspan="3"><input placeholder="请输入正确的支付网关" type="text" name="payGetway" value="${online.payGetway }" class="form-control"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付通道编码：</td>
						<td colspan="3"><input placeholder="请输入正确的支付通道编码" name="payChannel" value="${online.payChannel }" class="form-control"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">是否默认：</td>
						<td><select name="def" class="form-control">
								<option value="1"<c:if test="${online.def==1}">selected</c:if>>否</option>
								<option value="2"<c:if test="${online.def==2}">selected</c:if>>是</option>
						</select></td>
						<td class="text-right media-middle">排序：</td>
						<td><input placeholder="排序越小排的越前面" name="sortNo" value="${online.sortNo }" class="form-control" type="number"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${not empty glevelIds && glevelIds.contains(le1.id) }">checked</c:if>>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">随机金额开关</td>
						<td><select name="randomFlag" class="form-control">
							<option value="1"<c:if test="${online.randomFlag==1}">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${online.randomFlag==2}">selected</c:if>>启用</option>
						</select></td>
						<td colspan="3"><input placeholder="注意请用英文逗号隔开每个金额，如0.11,0.12,0.21" name="randomAmount" class="form-control" value="${online.randomAmount }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">固定金额开关</td>
						<td><select name="fixedFlag" class="form-control">
							<option value="1"<c:if test="${online.fixedFlag==1}">selected</c:if>>禁用</option>
							<option value="2"<c:if test="${online.fixedFlag==2}">selected</c:if>>启用</option>
						</select></td>
						<td colspan="3"><input placeholder="注意请用英文逗号隔开每个金额，如100,200,300" name="fixedAmount" class="form-control" value="${online.fixedAmount }"/></td>
					</tr>
					<tr class="hidden third-pay">
						<td class="text-right media-middle">显示类型：</td>
						<td>
							<select name="showType" class="form-control">
								<option value="all" <c:if test="${online.showType=='all'}">selected</c:if>>所有终端都显示</option>
								<option value="pc" <c:if test="${online.showType=='pc'}">selected</c:if>>pc端显示</option>
								<option value="mobile" <c:if test="${online.showType=='mobile'}">selected</c:if>>手机端显示</option>
								<option value="app" <c:if test="${online.showType=='app'}">selected</c:if>>app端显示</option>
							</select>
						</td>
						<td class="text-right media-middle">支付时间段：</td>
						<td>
							<input type="text" class="form-control fui-date" name="payStarttime" format="HH:mm:ss" style="width:48%;float:left;" value="${online.payStarttime}" placeholder="开始日期">-	
							<input type="text" class="form-control fui-date" name="payEndtime" format="HH:mm:ss" style="width: 48%;float:right;" value="${online.payEndtime}" placeholder="结束日期">
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">入款备注：</td>
						<td colspan="3"><textarea name="payDesc" class="form-control">${online.payDesc }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#depositset_online_modify_form_id");
	$form.find("[name='payPlatformId']").change(function(){
		var $it=$(this)
			,payId = $it.val()
			,iconCss = $it.find("option[value=" + payId +"]").attr("iconCss");
		/*
		switch(iconCss){
		case "dinpay30": // 智付支付3.0
		case "zhihpay": // 智汇付
		case "duodebao": // 多得宝
		case "suhuibao": // 速汇宝
		case "wzhifu": // W支付
		case "yifubao": // 易付宝
		case "tonghuika":// 通汇卡
		case "hebaopay":// 荷包支付
		case "woozf":// 沃支付
		case "yinbao": // 银宝支付
		case "ankuaipay": // 安快支付
		case "xinyingpay": // 信盈支付
		case "nowtopay": // 立刻付
		case "nixiaoka": // 你销卡
		case "kcpay": // 卡诚支付
		case "ekapay": // 千网支付
		case "luobopay": // 萝卜支付
		case "qixunpay": // 启迅支付
		case "xunbaoshangwu": // 讯宝商务
		case "danbaopay": // 钱海支付
		case "xunhuibao": // 迅汇宝(金安付扫码支付)
		case "xunfutong": // 迅付通
		case "qifupay": // 启付支付
		case "qingyifu": // 轻易付
		case "easypay": // 易付
		case "r1pay": // 融e付
		case "mobao": // 魔宝支付
		case "xinqidian": // 新起点
		case "renxinpay": // 仁信支付
		case "yunxunzhifu": // 云迅支付
		case "gaotongzhifu": // 高通支付
		case "antopay": // 云安付
		case "xinhuipay": // 信汇支付
		case "yuanbaozhifu": // 元宝支付
		case "iyibank": // 爱益支付
		case "fuqpay": // 付乾支付
		case "miaofupay": // 舒付支付
		case "shufupay": // 秒付支付
		case "shanfu": // 闪付支付
		case "fulapay": // 付啦支付
		case "shangyinxin": // 商银信
		case "ufuzhifu": // U付支付
		case "juhepay": // 聚合支付
		case "juhezaixian": // 聚合在线
		case "qifpay": // 旗付支付
		case "baifupay": // 百付支付
		case "shunfupay": // 瞬付支付
		case "newepay": // 新e付
		case "aimisenpay": // 艾米森
		case "xinmapay": // 新码支付
		case "tianchuangpay": // 天创支付
		case "juhepufa": // 聚合浦发
		case "sanwupay": // 三五支付
		case "ludepay": // 路德支付
		case "tianjinchuangxingou": // 天津创新购
		case "xingfupay": // 星付支付
		case "zaixianbao": // 在线宝
		case "changchengzhifu": // 长城支付
		case "shunbaopay": // 顺宝支付
		case "dpayhk": // DPAY支付
		case "haiopay": // 海鸥闪付
		case "zeshengpay": // 泽圣支付
		case "ak47pay": // AK47支付
		case "baifutong": // 百付通
		case "ulinepay": // 优畅支付
		case "yunmafuPay":	//云码付
		case "yinbangpay": // 银邦支付
		case "juheminsheng": // 聚合民生
		case "juherongzhifu": // 融智付聚合平台
		case "quanyinpay": // 全银支付
		case "chengbaopay": //诚宝支付
		case "heshengpay": //合生支付
		case "dingfengpay": // 鼎峰支付
		case "okpay": // OK支付
		case "eshidai": // E时代
		case "gmstone": // 讯通宝
		case "shuangchengpay": // 双城支付
		case "rujin8": // 金银宝
		case "ztbaopay": // 智通宝支付
		case "wefupay": // 微付
		case "pay591": // 591支付
		case "xunjietong": // 迅捷通
		case "yuypay": // 钰盈支付
		case "spdbweb": // 深圳浦发
		case "baolepay": // 宝乐支付
		case "dcpay": //得成支付
		case "shenhx": //深海星
		case "cmbcpos": //民生银行
		case "shkpay": //时刻宝
		case "mmpay": // mmpay
		case "foupang": // 嘟嘟叫
		case "xjpay": // 香蕉支付
		case "yizhibank": // 易支付
		case "shengtongkeji": // 盛通科技
		case "xingheyitong": // 星和易通
		case "bifupay": // 必付支付
			$form.find(".third-pay").removeClass("hidden");
			break;
		default :
			$form.find(".third-pay").addClass("hidden");
			break;
		}
		*/
		$form.find(".third-pay").removeClass("hidden");
	}).change();
});
</script>