<%@ page language="java" pageEncoding="utf-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-tab" url="${base}/agent/finance/depositset/index.do">在线支付设置</button>
		<button id='depositset_bank_tabId' class="btn btn-primary open-tab" url="${base}/agent/finance/depositset/index.do?type=2">银行入款设置</button>
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/finance/depositset/addFast.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/finance/depositset/fastlst.do",
		columns : [ {
			field : 'payName',
			title : '接口名称',
			align : 'center',
			width : '140px',
			valign : 'middle'
		}, {
			field : 'payCom',
			title : '接口所属公司',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'payAccount',
			title : '支付账号',
			align : 'center',
			width : '200px',
			valign : 'middle'
		}, {
			field : 'payUserName',
			title : '账户姓名',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'min',
			title : '最小金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'max',
			title : '最大金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'qrCodeImg',
			title : '二维码路径',
			align : 'center',
			valign : 'middle',
			formatter : contentSizeFormatter
		}, {
			field : 'icon',
			title : '图标地址',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function contentSizeFormatter(value, row, index) {
		if (value && value.length > 30) {
			return value.substring(0, 30) + "...";
		}
		return value;
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/depositset/updFastStatus.do?id="+row.id+"&status="});
	}
	function defFormatter(value, row, index) {
		if (value === 2) {
			return [ '<span class="text-success">', '</span>' ].join("是");
		}
		return [ '<span class="text-danger">', '</span>' ].join("否");
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/finance/depositset/modifyFast.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="${base}/agent/finance/depositset/delfast.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
});
</script>