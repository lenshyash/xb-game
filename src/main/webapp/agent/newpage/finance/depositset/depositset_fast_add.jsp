<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/depositset/savefast.do" class="form-submit" id="depositset_fast_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加快速入款方式</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="15%" class="text-right media-middle">快速入款方式：</td>
						<td width="30%"><select name="payPlatformId" class="form-control">
							<c:forEach items="${payCombos }" var="pc"><option value="${pc.id }">${pc.name }</option></c:forEach>
						</select></td>
						<td width="15%" class="text-right media-middle" id="accountTd">账号：</td>
						<td><input placeholder="请输入正确的账号" name="payAccount" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">账户姓名：</td>
						<td><input placeholder="请输入正确的账户姓名" name="payUserName" class="form-control" /></td>
						<td class="text-right media-middle">二维码图片：</td>
						<td><input placeholder="请输入正确的二维码图片" type="text" name="qrCodeImg" class="form-control regexp" regexp="[^<]+" tip="请输入图片地址"></td>
					</tr>
					<tr id="virtualTr1" style="display:none">
						<td class="text-right media-middle">钱包类型：</td>
						<td><input placeholder="请输入正确的钱包类型" name="walletType" class="form-control" /></td>
						<td class="text-right media-middle">当前兑换汇率：</td>
						<td><input placeholder="请输入准确的货币汇率，即1数字货币可兑换多少等额人民币" name="rate" class="form-control money" /></td>
					</tr>
					<tr id="virtualTr2" style="display:none">
						<td class="text-right media-middle">货币交易平台地址：</td>
						<td colspan="3"><input placeholder="请输入正确的交易平台地址" type="text" name="platformAddress" class="form-control regexp" regexp="[^<]+" tip="请输入图片地址"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最小支付金额：</td>
						<td><input placeholder="请输入正确的最小支付金额" name="min" class="form-control money" /></td>
						<td class="text-right media-middle">最大支付金额：</td>
						<td><input placeholder="请输入正确的最大支付金额" name="max" class="form-control money" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td><input placeholder="排序越小,排的越前面" name="sortNo" class="form-control" type="number"/></td>
						<td class="text-right media-middle">图标地址：</td>
						<td><input placeholder="请输入正确的图标地址" name="icon" class="form-control regexp" regexp="[^<]+" tip="请输入图标地址"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="1">禁用</option>
							<option value="2" selected>启用</option>
						</select></td>
						<td class="text-right media-middle">前端显示名称：</td>
						<td><input placeholder="为空时，关闭此项功能" name="frontLabel" class="form-control regexp" regexp="[^<]+" tip="请输入前端显示名称"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"checked>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">扫码说明：</td>
						<td colspan="3"><textarea name="qrcodeDesc" class="form-control"></textarea></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付时间段：</td>
						<td>
							<input type="text" class="form-control fui-date" name="payStarttime" format="HH:mm:ss" style="width:48%;float:left;" value="00:00:00" placeholder="开始日期">-	
							<input type="text" class="form-control fui-date" name="payEndtime" format="HH:mm:ss" style="width: 48%;float:right;" value="23:59:59" placeholder="结束日期">
						</td>
						<td class="text-right media-middle">随机显示：</td>
						<td>
							<select name="randomStatus" class="form-control">
								<option value="1" selected>禁用</option>
								<option value="2">启用</option>
							</select>
						</td>
					</tr>	
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<span style="float:left;" class="text-danger">温馨提示：开启随机显示的相同充值类型仅出现一个，禁用照常显示</span>
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#depositset_fast_add_form_id"),
		$payp=$form.find("[name='payPlatformId']"),
		$flabel=$form.find("[name='frontLabel']");
	$payp.change(function(){
		$flabel.val($payp.find(":selected").text()+"帐号");
		if($payp.find("option:selected").text()=="数字货币"){
			$("#virtualTr1").show();
			$("#virtualTr2").show();
			$("#accountTd").text("钱包地址:");
		}else{
			$("#virtualTr1").hide();
			$("#virtualTr2").hide();
		}

	}).change();
});
</script>