<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="finance_depositset_con_warp_id">
	<ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="online">在线支付设置</a></li>
		<li><a href="#" data-toggle="tab" oname="bank">银行入款设置</a></li>
		<li><a href="#" data-toggle="tab" oname="fast">快速入款设置</a></li>
		<li><a href="#" data-toggle="tab" oname="virtual">虚拟支付设置</a></li>
	</ul>
	
	<form class="fui-search table-tool" method="post">
		<div class="form-group zaixianzhifu">
			<div class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" name="name" placeholder="接口名称">
				</div>
				<div class="input-group">
					<select class="form-control" name="status">
						<option value="0">全部状态</option>
						<option value="2">启用</option>
						<option value="1">禁用</option>
					</select>
				</div>
				<button class="btn btn-primary">查询</button>
			</div>
		</div>
		<div class="form-group kuaisurukuan hidden">
			<div class="form-inline" >
				<div class="input-group">
					<select class="form-control" name="payPlatformId">
						<option value="0">支付类型</option>
						<c:forEach items="${payCombos }" var="pc"><option value="${pc.id }">${pc.name }</option></c:forEach>
					</select>
				</div>
				<button class="btn btn-primary">查询</button>
			</div>
		</div>
	</form>
	<div class="table-tool">
		<div class="form-group">
			<button class="btn btn-primary open-dialog add-btn1" url="${base}/agent/finance/depositset/addOnline.do">新增</button>
		</div>
	</div>
	
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/finance/depositset.js?v=2.2'],function($depositset){
	$depositset.render();
});
</script>