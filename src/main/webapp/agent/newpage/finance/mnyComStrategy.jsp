<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD" name="begin" placeholder="开始日期"><span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<div class="input-group">
				<select class="form-control" name="depositType">
					<option value="0">全部支付类型</option>
					<option value="5">在线支付</option>
					<option value="6">快速入款</option>
					<option value="7">一般入款</option>
					<option value="12">手动加款</option>
					<c:if test="${sysApiDeposit}"><option value="13">系统接口入款</option></c:if>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" name="giftType">
					<option value="0">全部赠送方式</option>
					<option value="1">固定数额</option>
					<option value="2">浮动比例</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/mnyComStrategy/add.do">新增</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD" placeholder="结束日期"><span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<div class="input-group">
				<select class="form-control" name="valueType">
					<option value="0">全部赠送类型</option>
					<option value="1">彩金</option>
					<option value="2">积分</option>
				</select>
			</div>
		</div>
	</div>
</form>
<div style="padding: 10px;">
	<div style="color:red;">查询条件中的日期只针对活动开始时间进行查询</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : "${base}/agent/finance/mnyComStrategy/list.do",
		columns : [ {
			field : 'depositType',
			title : '存款类型',
			align : 'center',
			formatter : dtFormatter
		}, {
			field : 'giftType',
			title : '赠送方式',
			align : 'center',
			formatter : gtFormatter
		}, {
			field : 'valueType',
			title : '赠送类型',
			align : 'center',
			formatter : vtFormatter
		}, {
			field : 'depositCount',
			title : '赠送频率',
			align : 'center',
			formatter : dcFormatter
		}, {
			field : 'giftValue',
			title : '赠送额度',
			align : 'center',
			formatter : giftValueFormatter
		}, {
			field : 'upperLimit',
			title : '赠送上限',
			align : 'center',
			formatter : limitFormatter
		}, {
			field : 'betMultiple',
			title : '流水倍数',
			align : 'center',
			formatter : betMulFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			formatter : statusFormatter
		}, {
			title : '存款金额',
			align : 'center',
			formatter : comMoneyFormatter
		}, {
			field : 'startDatetime',
			title : '开始时间',
			align : 'center',
			formatter : Fui.formatDate
		}, {
			field : 'endDatetime',
			title : '结束时间',
			align : 'center',
			formatter : Fui.formatDate
		}, {
			title : '操作',
			align : 'center',
			formatter : operateFormatter
		} ]
	});
	function dtFormatter(value, row, index) {
		if(!value){
			return "-";
		}
		switch(value){
		case 5:return "在线支付";
		case 6:return "快速入款";
		case 7:return "一般入款";
		case 12:return "手动加款";
		case 13:return "系统接口入款";
		}
	}
	function gtFormatter(value, row, index) {
		if (value == 2) {
			return "浮动比例";
		}
		return "固定数额";
	}
	function vtFormatter(value, row, index) {
		if (value == 2) {
			return "积分";
		}
		return "彩金";
	}
	function giftValueFormatter(value,row,index){
		if(!value){
			return "-";
		}
		if(row.giftType==2){
			return value+"%";
		}else{
			if(row.valueType==2){
				return value+"积分";
			}
			return value+"元";
		}
	}
	function limitFormatter(value, row, index) {
		if(row.giftType==2){
			if (value == 0) {
				return "无上限";
			}
			var un = "元";
			if (row.valueType == 2) {
				un = "积分";
			}
			return "最大赠送" + value + un;
		}
		return "-";
	}
	function betMulFormatter(value,row,index){
		if(!value)return "0";
		return Fui.toDecimal(value,2);
	}
	function dcFormatter(value, row, index) {
		if (!value || value == 0) {
			return "每次";
		}
		switch(value){
		case 1:return "首充";
		case 444:return "每日首充";
		case 2:return "第二次充值";
		case 3:return "第三次充值";
		case 222:return "前两次充值";
		case 333:return "前三次充值";
		}
	}
	function comMoneyFormatter(value, row, index){
		var min=row.minMoney,max=row.maxMoney;
		if(!min && !max){
			return "不限制";
		}
		var un = "元";
		if (row.valueType == 2) {
			un = "积分";
		}
		if(!min && max){
			return "小于等于"+max+un;
		}
		if(min && !max){
			return "大于"+min+un;
		}
		return "大于"+min+un+"<br>且小于等于"+max+un;
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/finance/mnyComStrategy/updStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/finance/mnyComStrategy/modify.do?id='+row.id+'" title="修改">修改</a>    '+
				'<a class="todo-ajax" href="${base}/agent/finance/mnyComStrategy/delete.do?id='+row.id+'" title="确定要删除？">删除</a>';
	}
});
</script>