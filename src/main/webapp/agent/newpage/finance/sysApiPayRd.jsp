<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${curDate} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="orderId" placeholder="订单号">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${curDate} 23:59:59" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/sysApiPayRd/list.do',
		columns : [{
			field : 'account',
			title : '会员账号',
			align : 'center'
		}, {
			field : 'createDatetime',
			title : '充值时间',
			align : 'center',
			formatter : Fui.formatDatetime
		}, {
			field : 'ip',
			title : 'IP',
			align : 'center'
		}, {
			field : 'ipStr',
			title : 'ip地址',
			align : 'center'
		}, {
			field : 'orderId',
			title : '订单号',
			align : 'center'
		}, {
			field : 'remark',
			title : '备注',
			align : 'center',
			formatter : remarkFormatter
		}]
	});
	function remarkFormatter(value,row,index){
		if(!value){return "-";}
		if (value.length > 10) {
			return '<a href="javascript:void(0)" class="open-text" dialog-text="'+value+'">'+value.substr(0, 10) + '...</a>';
		} else {
			return value;
		}
	}
});
</script>