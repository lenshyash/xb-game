<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/exchange/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加兑换策略</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">配置类型：</td>
						<td class="text-left"><select name="type" id ="type" class="form-control">
								<option value="1" selected>现金兑换积分</option>
								<option value="2">积分兑换现金</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">配置名称：</td>
						<td class="text-left"><input type="text" class="form-control" name="name" /></td>
					</tr>
					<c:if test="${betNumShowFlag eq 'on' }">
						<tr style ="display:none" id ="betNum">
							<td class="text-right media-middle">打码量：</td>
							<td class="text-left"><input type="text" class="form-control" name="betNum" /></td>
						</tr>
					</c:if>
					<tr>
						<td class="text-right media-middle">兑换比例：</td>
						<td class="text-left form-inline"><input type="text" name="numerator" class="form-control"/>:<input type="text" name="denominator" class="form-control"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">单次兑换最小值：</td>
						<td class="text-left"><input type="text" class="form-control" name="minVal" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">单次兑换最大值：</td>
						<td class="text-left"><input type="text" class="form-control" name="maxVal" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
<script>
	$(function() {// 初始化内容
		$("select#type").click(function(){
			var type = $(this).val();
			if(type == 2){
				$("#betNum").show();
				$()
			}else{
				$("#betNum").hide();
				$("input[name='betNum']").val("");
			}
		});
	})
</script>
</form>