<%@ page language="java" pageEncoding="utf-8"%>
<div id="finance_payment_con_warp_id">
	<form class="fui-search table-tool" method="post">
		<div class="form-group zaixiandaifu">
			<div class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" name="name" placeholder="接口名称">
				</div>
				<div class="input-group">
					<select class="form-control" name="status">
						<option value="0">全部状态</option>
						<option value="2">启用</option>
						<option value="1">禁用</option>
					</select>
				</div>
				<button class="btn btn-primary">查询</button>
				<button class="btn btn-primary open-dialog add-btn1" url="${base}/agent/finance/payment/addPayment.do">新增</button>
			</div>
		</div>
	</form>
	<table class="fui-default-table"></table>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/finance/payment.js?v=2.3'],function($payment){
	$payment.render();
});
</script>