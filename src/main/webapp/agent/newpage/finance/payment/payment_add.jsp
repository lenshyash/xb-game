<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/payment/savepayment.do" class="form-submit" id="payment_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加代付方式</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">代付方式：</td>
						<td width="30%"><select name="payPlatformId" class="form-control selectpicker" data-live-search="true">
							<c:forEach items="${payCombos }" var="pc" varStatus="stat1"><option value="${pc.id }" iconCss="${pc.iconCss}"<c:if test="${stat1.index==0}">selected</c:if>>${pc.name }</option></c:forEach>
						</select></td>
						<td width="20%" class="text-right media-middle">商户编码：</td>
						<td width="30%"><input placeholder="请输入正确的商户编码" name="merchantCode" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">商户密匙/商户识别码：</td>
						<td colspan="3"><input placeholder="商户密匙/商户识别码/智付3.0私钥" name="merchantKey" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">支付端账号：</td>
						<td colspan="3"><input placeholder="国付宝终端号/融宝卖家邮箱/环迅交易账号/智付3.0公钥" name="account" class="form-control"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">APPID：</td>
						<td><input placeholder="付啦支付APPID" name="appid" class="form-control"></td>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="1">禁用</option>
							<option value="2" selected>启用</option>
						</select></td>
						
						
					</tr>
<!-- 					<tr> -->
<!-- 						<td class="text-right media-middle">最小支付金额：</td> -->
<!-- 						<td><input placeholder="请输入正确的最小支付金额" name="min" class="form-control money" /></td> -->
<!-- 						<td class="text-right media-middle">最大支付金额：</td> -->
<!-- 						<td><input placeholder="请输入正确的最大支付金额" name="max" class="form-control money" /></td> -->
<!-- 					</tr> -->
<!-- 					<tr><td class="text-right media-middle">接口域名：</td> -->
<!-- 						<td><input placeholder="请输入正确的接口域名" type="text" name="url" class="form-control"></td> -->
<!-- 						<td class="text-right media-middle">支付图标地址：</td> -->
<%-- 						<td><input placeholder="请输入正确的支付图标地址" name="icon" class="form-control regexp" regexp="[^<]+" tip="请输入图标地址"/></td> --%>
<!-- 					</tr> -->

					<tr>
						<td class="text-right media-middle">接口域名：</td>
						<td><input placeholder="请输入正确的接口域名" type="text" name="url" class="form-control"></td>
					</tr>
					<tr class="pay-gateway">
						<td class="text-right media-middle">代付网关：</td>
						<td colspan="3"><input placeholder="请输入正确的代付网关" type="text" name="payGetway" class="form-control"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">排序：</td>
						<td ><input placeholder="排序越小排的越前面" name="sortNo" class="form-control" type="number"></td>
						<td class="text-right media-middle">是否回调：</td>
						<td><select name="isNotity" class="form-control">
							<option value="1">是</option>
							<option value="0" selected>否</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#payment_add_form_id");
	$form.find("[name='payPlatformId']").change(function(){
		var $it=$(this)
			,payId = $it.val()
			,iconCss = $it.find("option[value=" + payId +"]").attr("iconCss");
		$form.find(".third-pay").removeClass("hidden");
	}).change();
});
</script>