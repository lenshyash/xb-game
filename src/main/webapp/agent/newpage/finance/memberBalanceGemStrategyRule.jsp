<%@ page language="java" pageEncoding="utf-8"%>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">余额宝策略说明</h4>
		</div>
		<div class="modal-body">
			<div style="line-height:23px">
		     	<span>余额宝规则：</span><br>
		     
		         <p>
		             1. 余额宝按天计息，当天23:59:59前转入，第二天可享基准收益+红运收益，第三天起只享基准收益；
		         </p>
		         <p>
		             2. 收益发放时间：早上08:00后陆续发放到账；
		         </p>
		         <p>
		             3. 有效份额（会员可能发生多次存取，发放收益时，会根据有效份额进行结算）
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;基准有效份额：当日余额宝余额-次日08:00前转出或提现金额（优先扣除非有效份额部分）；
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;红运有效份额：当日转入金额-次日08:00前转出或提现金额（优先扣除非有效份额部分）；
		         </p>
		         <p>
		             4. 年化收益率(%)：近7日平均收益率的年化值；
		         </p>
		         <p>
		             5. 万份收益(元)：1万元存1天可得的收益=10000*年化收益率/360；
		         </p>
		         <p>
		             6. 每日收益=基准收益 + 红运收益；
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;基准收益=基准有效份额*基准万份收益/10000；
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;红运收益=红运有效份额*红运万份收益/10000；
		         </p>
		         <p>
		             7. 余额宝是复利的，每日收益会存入余额宝，用来计算次日基准收益；
		         </p>
		         <p>
		             例：今天23:50转入10000元，基准万份收益1.0000元，红运万份收益10.0000元，
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;第二天所得收益=10000*1.0000/10000+10000*10.0000/10000=11.00元
		         </p>
		         <p>
		             &nbsp;&nbsp;&nbsp;第三天所得收益=10011*1.0000/10000=1.0011元
		         </p>
		          <p>
		             8. 流水倍数：计算会员当日余额宝返利的流水打码量
		         </p>
		     </div>
		</div>
	</div>
</div>
