<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form action="${base}/agent/finance/payonlinerd/doHandler.do" class="form-submit" id="pay_online_rd_hand_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">处理会员充值申请</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="id" value='${com.id }'>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">会员账号：</td>
						<td>${com.account }</td>
					</tr>
					<tr>
						<td class="text-right">存款人姓名：</td>
						<td>${com.depositor }</td>
					</tr>
					<tr>
						<td class="text-right">转账类型：</td>
						<td>${com.payName }</td>
					</tr>
					<c:if test="${com.type==7}"><tr>
						<td class="text-right">银行卡号：</td>
						<td>${card }</td>
					</tr>
					<tr>
						<td class="text-right">银行卡姓名：</td>
						<td>${name}</td>
					</tr></c:if>
					<c:if test="${com.type==6}"><tr>
						<td class="text-right">微信号|支付宝：</td>
						<td>${card }</td>
					</tr>
					<tr>
						<td class="text-right">账号持有人：</td>
						<td>${name}</td>
					</tr></c:if>
					<tr>
						<td class="text-right media-middle">实收金额：</td>
						<td>
							<c:if test="${com.stauts == 1 }"><input class="form-control"value="${com.money }"/></c:if>
							<c:if test="${com.stauts != 1 }"><fmt:formatNumber value="${com.money }" pattern="#,##0.00"/></c:if></td>
					</tr>
					<tr>
						<td class="text-right">手续费：</td>
						<td><input class="form-control" value="0" /></td>
					</tr>
					<tr>
						<td class="text-right">处理结果：</td>
						<td><span class="text-danger">处理为失败</span></td>
					</tr>
					<tr class="opDesc_tr">
						<td class="text-right">处理说明：</td>
						<td><textarea name="opDesc" class="form-control"></textarea></td>
					</tr>
					<tr>
						<input type="hidden" name="status" value="2">
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>
<script>
requirejs(['jquery'],function(){
	var $form=$("#pay_online_rd_hand_form_id");
/* 	$form.on("change","[name='status']",function(){
		if($(this).val()==2){
			$form.find(".opDesc_tr").removeClass("hidden");
		}else{
			$form.find(".opDesc_tr").addClass("hidden");
		}
	}); */
});
</script>