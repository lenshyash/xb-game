<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="memmny_ope_form_id" method="post">
	<div class="form-inline">
		<div class="input-group">
			<input type="text" class="form-control" name="searchText" placeholder="输入用户名查询">
		</div>
		<button class="btn btn-primary select-btn" type="button">查询</button>
		<button class="btn btn-primary reset-btn" type="button">重置</button>
		<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/memmnyope/showBatchAdd.do">批量加款</button>
		<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/memmnyope/showBatchSub.do">批量扣款</button>
	</div>
	<div style="padding:5px;color:#2b1323;">
		<c:forEach items="${strategies}" var="s"><div>${s.desc}</div></c:forEach>
	</div>
	<div class="hidden memmny-div" style="margin-top:10px;"><input type="hidden" name="id"/>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td width="30%" class="text-right active">会员账号：</td>
                    <td class="text-primary account-td"></td>
				</tr>
				<tr>
					<td width="30%" class="text-right active">会员姓名：</td>
					<td class="text-primary account-name-td"></td>
				</tr>
				<tr>
					<td class="text-right active">账号余额：</td>
                    <td class="text-danger money-td"></td>
				</tr>  
				<tr>
					<td class="text-right active media-middle">操作类型：</td>
                    <td><label class="radio-inline"><input type="radio" name="type" value="1" checked="checked"/>人工加款</input></label>
                    	<label class="radio-inline"><input type="radio" name="type" value="2"/>人工扣款</input></label>
                    	<label class="radio-inline"><input type="radio" name="type" value="80"/>赠送彩金</input></label>
                    	<label class="radio-inline"><input type="radio" name="type" value="81"/>扣除彩金</input></label></td>
				</tr>
				<tr>
					<td class="text-right active media-middle">操作金额：</td>
                   	<td><input type="text" class="form-control" name="money" /></td>
				</tr>
				<tr class="mnyope-dml">
					<td class="text-right active media-middle">是否计算打码量：</td>
                    <td><label class="radio-inline"><input type="radio" name="checkBetNum" value="2" checked="checked"/>是</input></label>
                    	<label class="radio-inline"><input type="radio" name="checkBetNum" value="1"/>否</input></label>
                    </td>
				</tr>
				<!-- <tr class="mnyope-dmlbs hidden"> -->
				<tr class="mnyope-dmlbs">
					<td class="text-right active media-middle">打码量倍数：</td>
                   	<td><input type="text" class="form-control" name="betNumMultiple" value="${consumeRate}" /></td>
				</tr>
				<tr>
					<td class="text-right active media-middle">操作原因：</td>
                    <td><textarea class="form-control" name="remark"></textarea></td>
				</tr>
				<tr>
					<td colspan="2" class="text-center"><button class="btn btn-primary" id="affirm">确认</button></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#memmny_ope_form_id")
		,$searchText=$form.find("[name='searchText']");
	function search(){
		var acc=$searchText.val();
		if(acc==''){
			layer.msg("会员账号不能为空");
			return false;
		}
		$.ajax({
			url:"${base}/agent/finance/memmnyope/memmny.do",
			type:"POST",
			data:{"account":acc},
			success:function(data){
				reset();
				$searchText.val(acc);
				$form.find("[name='id']").val(data.accountId);
				$form.find(".money-td").html(data.money);
				var name ="";
				var html= "";
				switch(data.accountType){
					case 1:
						name ="真实会员"
						break;
					case 4:
						name ="真实代理"
						break;
					case 9:
						name ="引导会员"
						break;
					case 5:
						name = "总代账号";
						break;
					default:
						name = "普通账号";
						break;
				}
				if(data.reportType==1){
					name=name+",普通报表";
				}else if(data.reportType==2){
					name=name+",引导报表";
				}
				html = data.account+"(<span style='color:red'>"+name+"</span>)"
				$form.find(".account-td").html(html);
                $form.find(".account-name-td").html(data.userName);
				$form.find(".memmny-div").removeClass("hidden");
			},
			dataType:"JSON",
			errorFn:function(){
				reset();
				$searchText.val(acc);
			}
		});
	}
	$form.find(".select-btn").click(search);
	$form.find(".reset-btn").click(reset)
	$searchText.keydown(function(e){
		if(e.keyCode==13){
			search();
			return false;
		}
	});
	function reset() {
		$searchText.val("");
		$form.find("[name='id']").val("");
		$form.find(".account-td").html("");
		$form.find(".money-td").html("");
		$form.find("[name='money']").val("");
		
		$form.find("[name='remark']").val("");
		$form.find(".memmny-div").addClass("hidden")
		$form.find("[name='giveMoney']").val("");
		$form.find("[name='checkBetNum'][value='2']").click();
	}
	$form.find("[name='type']").change(function(){
		var val=$(this).val();
		if(val==1 || val == 80){
			$form.find(".mnyope-dml").removeClass("hidden");
			$form.find("[name='checkBetNum'][value='1']").prop("checked");
		}else{
			$form.find(".mnyope-dml").addClass("hidden");
			$form.find("[name='checkBetNum'][value='1']").click();
		}
	});
	$form.find("[name='checkBetNum']").change(function(){
		var val=$(this).val();
		if(val==1){
			$form.find(".mnyope-dmlbs").addClass("hidden");
		}else{
			$form.find(".mnyope-dmlbs").removeClass("hidden");
			$form.find("[name='betNumMultiple']").val('${consumeRate}');
		}
	});
	var saveExe=false;
	$form.submit(function(){

		/* alert("确认要加款吗？"); */
		$('#affirm').attr("disabled",true);
		
		if(saveExe){
			layer.msg("加减款申请已提交，请等一会再加款");
			return false;
		}
		saveExe=true;
		$.ajax({
			url : "${base}/agent/finance/memmnyope/save.do",
			data : $form.serialize(),
			success : function(data) {
				layer.msg(data.msg||"操作成功！");
				search();
				saveExe=false;
			},
			error:function(data){
				layer.msg(data.msg||"操作失败！");
				saveExe=false;
			},
			errorFn:function(data){
				layer.msg(data.msg||"操作失败！");
				saveExe=false;
			},
			complete:function(){
				$('#affirm').removeAttr("disabled");
			}
		});
		
		return false;
	})
});
</script>