<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="memdrawrd_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="begin" format="YYYY-MM-DD HH:mm:ss" value="${startTime} 00:00:00" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>

			<div class="input-group">
				<label class="sr-only" for="status">状态</label>
				<select class="form-control" name="status">
					<option value="0" class="text-warning">全部记录</option>
					<option value="4" class="text-success">未处理</option>
					<option value="1" class="text-success">处理中</option>
					<option value="2" class="text-primary">提款成功</option>
					<option value="110" class="text-primary">提款成功(代付)</option>
					<option value="111" class="text-primary">提款成功(手动)</option>
					<option value="3" class="text-danger">提款失败</option>
					<option value="5" class="text-default">已过期</option>
					<option value="6" class="text-default">等待代付通知</option>
				</select>
			</div>
			<div class="input-group">
				<label class="sr-only" for="md_leibie">用户类别</label>
				<select class="form-control" name="type">
					<option value="0" class="text-warning" selected>全部</option>
					<option value="1" class="text-primary">会员</option>
					<option value="4" class="text-warning">代理</option>
					<option value="5" class="text-warning">总代</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${saccount}" placeholder="会员名称">
			</div>
			
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1" >普通报表</option>
				<option value="2">引导报表</option>
			</select>
			<select class="form-control" name="drawTimes">
				<option value="">提款类型</option>
				<option value="1" class="text-warning">首次提款</option>
				<option value="2" class="text-danger">二次提款</option>
				<option value="3" class="text-success">三次提款</option>
			</select>
			<select class="form-control" name="payId">
				<option value="">代付选择</option>
				<c:forEach items="${payments}" var="pay"><option value="${pay.id }">${pay.payName}</option></c:forEach>
			</select>
			<button class="btn btn-primary fui-date-search CastAutoRefresh">查询</button>
			<c:if test="${exportPerm}"><button class="btn btn-primary exportBtn" type="button">导出</button></c:if>
			<div class="form-group CastAutoRefresh">
				<label>
					<input  type="checkbox">
				</label>
				<label for="name">自动刷新</label>
				<select class="form-control">
					<option>5</option>
					<option>10</option>
					<option>20</option>
					<option>30</option>
					<option>60</option>
					<option>120</option>
				</select>
			</div>
			<div class="form-inline mt5px">
				<div class="input-group">
					<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${endTime} 23:59:59" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
				</div>
				<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
				<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
				<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
				<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
				<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
				<input type="text" class="form-control" name="agentName" placeholder="所属代理">
				<div class="input-group">
					<input type="text" class="form-control" name="minMoney" style="width:100px;" value="${minMoney }" placeholder="最小金额">
				</div>
				<div class="input-group">
					<input type="text" class="form-control" name="maxMoney" style="width:100px;" value="${maxMoney }" placeholder="最大金额">
				</div>
				<div class="input-group">
					<input type="text" class="form-control" name="operator" style="width:120px;" value="${operator }" placeholder="操作员">
				</div>
				<input type="text" class="form-control" name="remark" placeholder="备注">
			</div>
		</div>
	</div>
</form>
<table id="memdrawrd_datagrid_tb"></table>
<c:if test="${exportPerm}"><div class="hidden">
	<form id="memdrawrd_export_fm_id" action="${base}/agent/finance/memdrawrd/export.do" target="_blank">
		<input type="hidden" name="account"/> 
		<input type="hidden" name="status"/> 
		<input type="hidden" name="begin"/>
		<input type="hidden" name="end"/>
		<input type="hidden" name="type"/>
	</form>
</div></c:if>
<style>
	#prompt {
    	background-color: #f44336e0;
    	color: #f9f7f7;
    	border-color: #ccc;
	}
</style>
<script type="text/javascript">


	$(function(){
		var  times=''
		var time =''
		$('.CastAutoRefresh input').change(function(){
			if ($('.CastAutoRefresh input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.CastAutoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else{
				autoRefreshs(time,false)
			}
		})

		$('.CastAutoRefresh select').change(function(){
			if ($('.CastAutoRefresh input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.CastAutoRefresh option:selected').text()
				autoRefreshs(time,true)
			}else {
				autoRefreshs(time,false)
			}
		})

		function autoRefreshs(time,statu){
			if (statu == true){
				times = setInterval(function(){
					$('.btn.btn-primary.fui-date-search.CastAutoRefresh').trigger('click')
				},time *1000 )
			}else {
				clearInterval(times)
			}

		}
		function copy(e){
			//创建一个input框
			var input = document.createElement("input");
			//将指定的DOM节点添加到body的末尾
			document.body.appendChild(input);
			//设置input框的value值
			input.setAttribute("value", e);
			//选取文本域中的内容
			input.select();
			//copy的意思是拷贝当前选中内容到剪贴板
			//document.execCommand（）方法操纵可编辑内容区域的元素
			//返回值为一个Boolean，如果是 false 则表示操作不被支持或未被启用
			if (document.execCommand("copy")) {
				document.execCommand("copy");
				layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
			}
			//删除这个节点
			document.body.removeChild(input);

			// // Url2.select(); // 选择对象
			// document.execCommand("Copy"); // 执行浏览器复制命令
			// layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
		}
	})
	function copy(e){
		//创建一个input框
		var input = document.createElement("input");
		//将指定的DOM节点添加到body的末尾
		document.body.appendChild(input);
		//设置input框的value值
		input.setAttribute("value", e);
		//选取文本域中的内容
		input.select();
		//copy的意思是拷贝当前选中内容到剪贴板
		//document.execCommand（）方法操纵可编辑内容区域的元素
		//返回值为一个Boolean，如果是 false 则表示操作不被支持或未被启用
		if (document.execCommand("copy")) {
			document.execCommand("copy");
			layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
		}
		//删除这个节点
		document.body.removeChild(input);

		// // Url2.select(); // 选择对象
		// document.execCommand("Copy"); // 执行浏览器复制命令
		// layer.msg('复制成功,请按ctrl+v粘贴',{time:1000});
	}
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#memdrawrd_form_id");
	<c:if test="${exportPerm}">$form.find(".exportBtn").click(function(){
		var $form1=$("#memdrawrd_export_fm_id");
		$form1.find("[name='account']").val($form.find("[name='account']").val());
		$form1.find("[name='status']").val($form.find("[name='status']").val())
		$form1.find("[name='begin']").val($form.find("[name='begin']").val())
		$form1.find("[name='end']").val($form.find("[name='end']").val())
		$form1.find("[name='type']").val($form.find("[name='type']").val())
		$form1.submit();
	});</c:if>
	Fui.addBootstrapTable({
		id : 'memdrawrd_datagrid_tb',
		url : '${base}/agent/finance/memdrawrd/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [ {
			field : 'orderNo',
			title : '会员/单号',
			align : 'center',
			valign : 'middle',
			formatter : orderFormatter
		},{
			field : 'accounttype',
			title : '会员类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		},{
			field : 'levelName',
			title : '会员等级',
			align : 'center',
			valign : 'middle'
		},{
			field : 'agentname',
			title : '所属代理',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'reportType',
			title : '报表类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		},{
			field : 'userName',
			title : '收款户名',
			align : 'center',
			valign : 'middle',
			formatter : cardNoName
		}, {
			field : 'cardNo',
			title : '收款银行账号',
			align : 'center',
			valign : 'middle',
			formatter : cardNoFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			field : 'drawMoney',
			title : '提款金额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"drawMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.drawMoney ? aggsData.drawMoney.toFixed(2) : "0.00";
			}
		}, {
			sortable:true,
			field : 'createDatetime',
			title : '申请时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'modifyDatetime',
			title : '处理时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'lockFlag',
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		}, {
			field : 'modifyUser',
			title : '操作员',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'memberremark',
			title : '会员备注信息',
			align : 'center',
			valign : 'middle'
		} , {
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle'
		} ]
	});
	function orderFormatter(value, row, index) {
		var html="";
		var text=''
		var copy = '';
		copy='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button"  onclick="copy(\''+row.account+'\')">复制</span></c:if>'
//		if(row.firstDraw==2){
//			html=" <button class='btn btn-default' id='prompt'>首提</button>"
//		}
		if (row.drawTimes == 1){
			text +="<span class='label label-warning'>首提</span>"
		}
		if (row.drawTimes == 2){
			text +="<span class='label label-danger'>二提</span>"
		}
		if (row.drawTimes == 3){
			text +="<span class='label label-success'>三提</span>"
		}
		if(${warnFlag} && row.abnormalFlag==1){
			text += '&nbsp;<span class="label label-danger">警</span>';
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.memberId,'" title="查看详情"><span class="text-danger">',row.account,'</span></a>',copy,text+'<br>',value].join('')+html;
	}
	function cardNoFormatter(value, row, index) {
		var text=''
		text+='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button" onclick="copy(\''+value+'\')">复制</span></c:if>'
		return row.bankName+"<br>"+value+text+"<br>"+row.bankAddress;
	}
	function cardNoName(value, row, index) {
		var text=''
		text+='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button" onclick="copy(\''+value+'\')">复制</span></c:if>'
		return row.userName+"<br>"+text+"<br>";
	}
	function moneyFormatter(value, row, index) {
		var text=''
		text+='<c:if test="${onoffcopy eq 'on'}"><span class="label label-primary copy-btn" role="button"  id ="copyMoney"  onclick="copy(\''+value+'\')">复制</span></c:if>'
		if (value === undefined) {
			return "0.00";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>'].join(Fui.toDecimal(value,2))+text;
		}
		return [ '<span class="text-primary">', '</span>' ].join("0.00");

	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey] ){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(2)+"";
	}
	var allStatus = {
			4 : '<span class="text-success">未处理</span>',
			1 : '<span class="text-success">处理中</span>',
			2 : '<span class="text-primary">提款成功</span>',
			3 : '<span class="text-danger">提款失败</span>',
			5 : '<span class="text-default">已过期</span>',
			6 : '<span class="text-default">等待中</span>'
		};
	function statusFormatter(value, row, index) {
		if(value == 1 && row.lockFlag == 1){
			return allStatus[4];
		} 
		return allStatus[value];
	}
	function typeFormatter(value,row,index){
		if(value == 1){
			return "会员";
		}else if(value == 4){
			return "代理";
		}else if(value == 5){
			return "总代";
		}
	}
	
	function reportFormatter(value, row, index) {
		var reportTypes = {"1":"普通","2":"引导"};
		switch (value) {
		case 2:
			return [ '<span class="text-danger">', '</span>' ].join(reportTypes[value]);
			break;

		default:
			return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
			break;
		}
		
	}
	
	function operateFormatter(value, row, index) {
		if (value == 1) {
			return <c:if test="${withdrawalPerm}">['<a class="todo-ajax" href="${base}/agent/finance/memdrawrd/lock.do?lockFlag=2&id=',row.id,'" title="锁定">锁定</a>'].join('')</c:if>;
		} else if (value == 2) {
			if(baseInfo.account==row.modifyUser){
				return ['<a class="todo-ajax" href="${base}/agent/finance/memdrawrd/lock.do?lockFlag=1&id=',row.id,'" title="取消锁定">取消锁定</a>  ',
					      '<a class="open-dialog" href="${base}/agent/finance/memdrawrd/handler.do?id=',row.id,'" title="处理申请">处理申请</a>' ].join('');
			}
			return "已被锁定";
		}else if((value == 3 || value == '3')&&row.status==2 && row.payId == null && 'on' != '${onRollBackClose}'){
			if(baseInfo.account==row.modifyUser || (2==${USER_SESSION_AGENT.accountType})){
				return <c:if test="${rollBack}">['<a class="todo-ajax" href="${base}/agent/finance/memdrawrd/rollBack.do?id=',row.id,'" title="确认回滚该订单？">回滚</a>  ' ].join('')</c:if>;
			}
		}else if((value == 3 || value == '3')&&row.status==3 && row.payId == null && 'on' != '${onRollBackClose}'){
			if(baseInfo.account==row.modifyUser || (2==${USER_SESSION_AGENT.accountType})){
				return <c:if test="${rollBack}">['<a class="todo-ajax" href="${base}/agent/finance/memdrawrd/failedRollBack.do?id=',row.id,'" title="确认回滚该订单？">回滚</a>  ' ].join('')</c:if>;
			}
		} else if(value == 4){
			return ['<a class="open-dialog" href="${base}/agent/finance/memdrawrd/paymentHandler.do?id=',row.id,'" title="手动处理">手动处理</a>' ].join('');
		}
		if('${isCancle}' && 'on' == '${isCancle}' && (row.status == 2 ||value == '2')){
			return ['<a class="open-dialog" href="${base}/agent/finance/memdrawrd/cancle.do?id=',row.id,'" title="撒消记录">撒消记录</a>' ].join('');
		}
		return "已操作";
	}


});
</script>