<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/finance/balanceGemStrategy/update.do" class="form-submit" id="balance_gem_strategy_update_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改余额宝策略</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${gem.id}">
			<div style="color:#a94442;margin-top:5px;">同一个余额范围内不能存在2条策略  （如有重叠取较新策略计算）
				<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/finance/balanceGemStrategy/rule.do">配置规则</button>
			</div>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="text-right media-middle">基准年化收益率：</td>
						<td colspan="3">
						<input  class="input w70" type="text" name="benchmarkAnnualYield" class="form-control money" placeholder="默认为0" value="${gem.benchmarkAnnualYield}" onkeyup="calcWanProfit(this,0)" datatype="/^([1-9]\d*|[0-9]\d*\.\d{1,4}|0)$/" errormsg="大于等于0, 最多4位小数" nullmsg="大于等于0, 最多4位小数"/>%
						<span>万份收益=<span id="wanProfit_0"></span>0</span>
						</td>
						 
					</tr>
					<tr>
						<td class="text-right media-middle">红运年化收益率：</td>
						<td colspan="3">
						<input  class="input w70" type="text" name="hongyunAnnualYield" class="form-control money" placeholder="默认为0" value="${gem.hongyunAnnualYield}" onkeyup="calcWanProfit(this,1)" datatype="/^([1-9]\d*|[0-9]\d*\.\d{1,4}|0)$/" errormsg="大于等于0, 最多4位小数" nullmsg="大于等于0, 最多4位小数" />%
						<span>万份收益=<span id="wanProfit_1"></span>0</span>
						</td>
					</tr>
					<tr>
						<td class="text-right media-middle">余额大于等于：</td>
						<td><input type="text" name="minMoney" class="form-control money"value="${gem.minMoney}" placeholder="空或者0表示不限制"/></td>
						<td class="text-right media-middle">余额小于等于：</td>
						<td><input type="text" name="maxMoney" class="form-control money"value="${gem.maxMoney}" placeholder="空或者0表示不限制"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">流水倍数：</td>
						<td colspan="3"><input type="text" name="betMultiple"value="${gem.betMultiple}" class="form-control money" placeholder="转出金额x流水倍数=出款需要达到的投注量" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levelList}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${levelSet.contains(le1.id)}">checked</c:if>>${le1.levelName }</label>  </c:forEach>
						<div style="color:#a94442;margin-top:3px;">全部选择和全部不选是一样，表示所有等级都可以使用</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td colspan="3"><input type="text" name="memo" class="form-control" value="${gem.remark}"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#balance_gem_strategy_update_form_id")
		,$rollBackRate=$form.find(".rollBackRate")
		,$giftValue=$form.find(".giftValue");
	$form.find("[name='giftType']").change(function(){
		var selval = $(this).val();
		$rollBackRate.addClass("hidden");
		$giftValue.addClass("hidden");
		if (selval == 1) {
			$giftValue.removeClass("hidden");
		} else if (selval == 2) {
			$rollBackRate.removeClass("hidden");
		}
	});$form.find("[name='depositType']").change(function(){
		var val = $(this).val();
		if(val==12){
			$form.find("[name='depositCount']").hide().val(0);
			$form.find("#depositCount12").show();
		}else{
			$form.find("[name='depositCount']").show();
			$form.find("#depositCount12").hide();
		}
	});
});
$(function(){ 
	var val1 = $("input[name='benchmarkAnnualYield']").val();
	var val2 = $("input[name='hongyunAnnualYield']").val();
	calcWanProfit(val1,0);
	calcWanProfit(val2,1);
}); 

function calcWanProfit(obj, tag) {
    var value = typeof (obj) == "object" ? obj.value : obj;
    var rate = isNaN(value) ? 0 : Number(value);
    var profit = (rate / 100 * 10000 / 360).toFixed(8);
    profit = profit.substr(0, profit.length - 4);
    $("#wanProfit_" + tag).text(profit);
    return profit;
}
</script>