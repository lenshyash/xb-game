<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${base}/agent/finance/depositGiftActivity/eidtSaveNew.do" class="form-submit" id="depositGiftActivity_modify_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加存款赠送活动</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${gift.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">存款类型：</td>
						<td width="30%" class="text-left"><select name="depositType" class="form-control">
								<option value="5"<c:if test="${gift.depositType==5 }">selected</c:if>>在线支付</option>
								<option value="6"<c:if test="${gift.depositType==6 }">selected</c:if>>快速入款</option>
								<option value="7"<c:if test="${gift.depositType==7 }">selected</c:if>>一般入款</option>
						</select></td>
						<td width="20%" class="text-right media-middle">赠送方式：</td>
						<td width="30%" class="text-left"><select name="giveType" class="form-control">
								<option value="1"<c:if test="${gift.giveType==1 }">selected</c:if>>固定数额</option>
								<option value="2"<c:if test="${gift.giveType==2 }">selected</c:if>>浮动比例</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">赠送类型：</td>
						<td class="text-left"><select name="valueType" class="form-control">
								<option value="1"<c:if test="${gift.valueType==1 }">selected</c:if>>彩金</option>
								<option value="2"<c:if test="${gift.valueType==2 }">selected</c:if>>积分</option>
						</select></td>
						<td class="text-right media-middle">存款频率：</td>
						<td class="text-left"><input type="text" name="depositCount"value="${gift.depositCount }" class="form-control digits" placeholder="0表示每次, 1表示首充, N表示第N次" /></td>
					</tr>
					<tr class="hidden giveValue">
						<td class="text-right media-middle">赠送额度：</td>
						<td class="text-left" colspan="3"><input type="text" name="giveValue"value="${gift.giveValue }" class="form-control" /></td>
					</tr>
					<tr class="hidden rollBackRate">
						<td class="text-right media-middle">赠送比例（％）：</td>
						<td class="text-left"><input type="text" name="rollBackRate"value="${gift.rollBackRate }" class="form-control" /></td>
						<td class="text-right media-middle">赠送上限：</td>
						<td class="text-left"><input type="text" name="upperLimit"value="${gift.upperLimit }" class="form-control" placeholder="0表示无上限, N表示最大赠送N" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">流水倍数：</td>
						<td class="text-left" colspan="3"><input type="text" name="betMultiple"value="${gift.betMultiple }" class="form-control" placeholder="(充值金额+赠送)x流水倍数=出款需要达到的投注量" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">开始时间：</td>
						<td class="text-left"><div class="input-group">
								<input type="text" class="form-control fui-date" name="startTime"value="<fmt:formatDate pattern="yyyy-MM-dd" value="${gift.startTime}" />" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
							</div></td>
						<td class="text-right media-middle">结束时间：</td>
						<td class="text-left"><div class="input-group">
								<input type="text" class="form-control fui-date" name="endTime" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${gift.endTime}" />"placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
							</div></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td class="text-left" colspan="3"><input type="text" name="memo"value="${gift.memo }" class="form-control" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#depositGiftActivity_modify_form_id")
		,$rollBackRate=$form.find(".rollBackRate")
		,$giveValue=$form.find(".giveValue");
	$form.find("[name='giveType']").change(function(){
		var selval = $(this).val();
		$rollBackRate.addClass("hidden");
		$giveValue.addClass("hidden");
		if (selval == 1) {
			$giveValue.removeClass("hidden");
		} else if (selval == 2) {
			$rollBackRate.removeClass("hidden");
		}
	}).change();
});
</script>