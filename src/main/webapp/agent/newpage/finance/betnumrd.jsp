<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="begin" value="${begin}" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
            <button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
            <button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>

			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account}" placeholder="会员账号">
			</div>
			<div class="input-group">
				<label class="sr-only" for="type">类型</label> <select class="form-control" name="type">
					<option value="0">全部类型</option>
					<option value="1">彩票投注</option>
					<option value="2">真人投注</option>
					<option value="3">电子游艺投注</option>
					<option value="4">体育投注</option>
					<option value="5">六合彩投注</option>
					<option value="90">棋牌</option>
					<option value="10">人工增加</option>
					<option value="11">人工扣除</option>
					<option value="12">注册赠送</option>
					<option value="13">抢红包</option>
					<option value="21">反水</option>
					<option value="22">存款</option>
					<option value="87">站内额度转入</option>
					<option value="23">接口存款</option>
					<option value="102">余额宝返利</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="end" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" value="${end}" placeholder="结束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
            <button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
            <button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/finance/betnumrd/list.do',
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'type',
			title : '变动类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'beforeNum',
			title : '变动前打码量',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'betNum',
			title : '变动打码量',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'afterNum',
			title : '变动后打码量',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'createDatetime',
			title : '变动时间',
			align : 'center',
			valign : 'middle',
			width : '150px',
			formatter : Fui.formatDatetime
		}, {
			field : 'bizDatetime',
			title : '投注时间',
			align : 'center',
			valign : 'middle',
			width : '150px',
			formatter : Fui.formatDatetime
		}, {
			field : 'beforeDrawNeed',
			title : '变动前提款所需',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'drawNeed',
			title : '变动提款所需',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'afterDrawNeed',
			title : '变动后提款所需',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'operatorName',
			title : '操作员',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'remark',
			title : '备注',
			align : 'left',
			valign : 'middle'
		} ]
	});
	function typeFormatter(value, row, index) {
		switch(value-0){
		case 0:return "全部类型";
		case 1:return "彩票投注";
		case 2:return "真人投注";
		case 3:return "电子游艺投注";
		case 4:return "体育投注";
		case 5:return "六合彩投注";
		case 10:return "人工增加";
		case 11:return "人工扣除";
		case 12:return "注册赠送";
		case 13:return "抢红包";
		case 21:return "反水";
		case 22:return "存款";
		case 23: return "接口存款";
		case 87: return "站内额度转入";
		case 90: return "棋牌";
		case 91: return "三方彩票";
		case 99: return "三方体育";
		}
		return "";
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
});
</script>