<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/finance/memmnyope/batchAddMoney.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">批量加款【没使用存款策略】</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">会员账号：</td>
						<td width="70%"><textarea name="accounts" style="height:150px;width:100%;"></textarea>
							<br>每行的数据格式：会员账号,金额,赠送彩金   使用逗号或者空格隔开，赠送彩金可以为空
							<br>如：aaaaaa 300 1  <span style="font-size:10px;color:red">使用空格隔开</span>
							<br>	&nbsp; &nbsp;  &nbsp;  &nbsp;bbbbbb,200,0.8   <span style="font-size:10px;color:red">使用逗号隔开</span>
							<br>	&nbsp; &nbsp;  &nbsp;  &nbsp;cccccc 100   <span style="font-size:10px;color:red">使用空格隔开，没有赠送彩金</span>
							<br>	&nbsp; &nbsp;  &nbsp;  &nbsp;dddddd 0 12   <span style="font-size:10px;color:blue">使用空格隔开，只赠送彩金</span>
						</td>
					</tr>
					<tr>
						<td class="text-right active media-middle">打码量倍数：</td>
	                   	<td><input type="text" class="form-control money" name="betNumMultiple" value="1"/></td>
					</tr>
					<tr>
						<td class="text-right active media-middle">赠送彩金打码量倍数：</td>
	                   	<td><input type="text" class="form-control money" name="giftBetNumMultiple" value="1"/></td>
					</tr>
					<tr>
						<td class="text-right active media-middle">操作原因：</td>
	                    <td><textarea class="form-control" name="remark"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>