<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form method="post" action="${base}/agent/login.do" unReloadTable='true' class="form-submit form-horizontal" id="login_dialog_form_id">
<div class="modal-dialog fui-box">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">登录超时，重新登录</h4>
		</div>
		<div class="modal-body">
			<fieldset>
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
					<input name="account" type="text" class="form-control" placeholder="用户名" readonly>
				</div>
				<div class="clearfix"></div><br>
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
					<input name="password" type="password" class="form-control required" placeholder="密码" minlength="6">
				</div>
				<div class="clearfix"></div><br>
				<c:if test="${googleFlag}">
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
					<input name="googleCode" type="text" class="form-control" placeholder="谷歌验证码" maxlength="6">
				</div>
				<div class="clearfix"></div><br>
				</c:if>
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-check red"></i></span>
					<input name="verifyCode" id="loginDialog_verifyCode" class="form-control required" placeholder="验证码" type="text" minlength="4" maxlength="5">
					<span class="input-group-addon"><img id="verifyImg" src="#" border="0" width="89" height="38" alt="点击刷新验证码"></span>
				</div>
			</fieldset>
		</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-primary">登 录</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function() {
	var $form=$("#login_dialog_form_id");
	$form.find("[name='account']").val(baseInfo.account);
	$form.data("errorcallback",function(){
		$("#verifyImg").attr("src","${base}/agent/verifycode.do?time=" + (new Date()).getTime());
		$("#loginDialog_verifyCode").val("");
	}).data("resultCallback",function(json){
		if(Fui.config.loginSuccessFn){
			Fui.config.loginSuccessFn(json);
		}
	});
	$("#verifyImg").click(function(){
		$(this).attr("src","${base}/agent/verifycode.do?time=" + (new Date()).getTime());
		$("#loginDialog_verifyCode").val("");
	}).click();
});
</script>