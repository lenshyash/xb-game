<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/member/search/list.do',
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'money',
			title : '账号余额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		},
		<c:if test="${scoreShowFlag}">
		{
			field : 'score',
			title : '积分余额',
			align : 'center',
			valign : 'middle'
		},
		</c:if>
		{
			field : 'userName',
			title : '会员姓名',
			align : 'center',
			valign : 'middle'
		},{
			field : 'levelGroup',
			title : '会员等级',
			align : 'center',
			valign : 'middle',
			formatter : levelFormatter
		},{
			field : 'createDatetime',
			title : '注册时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		},{
			field : 'lastLoginDatetime',
			title : '最后登录时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'lastLoginIp',
			title : '最后登录ip',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'online',
			title : '在线情况',
			align : 'center',
			valign : 'middle',
			formatter : onlineFormatter
		}, {
			field : 'accountStatus',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter,
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/member/manager/updStatus.do?id="+row.id+"&accountStatus="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/member/manager/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp; '+
			'<a class="open-dialog" href="${base}/agent/member/manager/modifyPwd.do?id='+row.id+'" title="修改密码">修改密码</a>';
	}
	var levels ={};
	<c:forEach items="${levels}" var="l">levels['${l.id}']='${l.levelName}';</c:forEach>
	function levelFormatter(value, row, index) {
		return [ '<span class="text-primary">', '</span>' ].join(levels[value]);
	}
	function onlineFormatter(value, row, index) {
		if (value === 2) {
			return '<span class="text-danger label label-success">在线</span>';
		}
		return '<span class="text-muted label label-default">离线</span>';
	}
});
</script>