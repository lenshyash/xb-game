<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="member_manager_form_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<input type="hidden" name="abnormalFlag" value="${abnormalFlag}"/>
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startDate" placeholder="开始日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号" style="width:120px;">
			</div>
			<div class="input-group">
				<select name="levelGroup" class="form-control"><option value="">所有等级</option><c:forEach items="${levels}" var="l"><option value="${l.id }"<c:if test="${(not empty level && level==l.id)}">selected</c:if>>${l.levelName }</option></c:forEach></select>
			</div>
			<div class="input-group">
				<select class="form-control" name="keyName">
					<option value="userName">会员姓名</option>
					<option value="phone">手机号码</option>
					<option value="qq">QQ</option>
					<option value="email">邮箱</option>
					<option value="wechat">微信</option>
					<option value="lastLoginIp">最后登录IP</option>
					<option value="regIp">注册IP</option>
					<option value="registerUrl">注册来源</option>
					<option value="cardNo">银行账号</option>
					<option value="remark">备注</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="keyword" placeholder="关键字" style="width:120px;">
			</div>
			<div class="input-group">
				<select class="selectpicker form-control" id="select_id2" data-live-search="true" title="选择层级" data-hide-disabled="true" multiple name="select_name">
					<option value="1">层级1</option>
					<option value="2">层级2</option>
					<option value="3">层级3</option>
					<option value="4">层级4</option>
					<option value="5">层级5</option>
					<option value="6">层级6</option>
					<option value="7">层级7</option>
					<option value="8">层级8</option>
					<option value="9">层级9</option>
				</select>
			</div>
			<input type="text" id="input_select_id2" style="display:none;" name="levelArray"/>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary open-dialog cached" type="button" url="${base}/agent/member/manager/add.do">新增</button>
			<c:if test="${moveFlag eq 'on' }">
				<button class="btn btn-primary open-dialog cached" url="${base}/agent/member/manager/movepage.do">批量移动代理</button>
			</c:if>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" placeholder="线束日期"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="agentName" placeholder="所属上级" style="width:120px;">
			</div>
			<select class="form-control" name="depositStatus">
				<option value="">充值情况</option>
				<option value="1">有充值</option>
				<option value="2">从未充值</option>
			</select>
			<select class="form-control" name="accountType">
				<option value="">账号类型</option>
				<option value="1">会员账号</option>
				<option value="9">引导账号</option>
			</select>
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
				<option value="2">引导</option>
			</select>
			<c:if test="${warnFlag}">
				<select class="form-control" name="abnormalFlag">
					<option value="">预警类型</option>
					<option value="2" <c:if test="${(not empty abnormalFlag && abnormalFlag==0)}">selected</c:if>>普通</option>
					<option value="1" <c:if test="${(not empty abnormalFlag && abnormalFlag==1)}">selected</c:if> >预警</option>
				</select>
			</c:if>
			<div class="input-group">
				<input type="text" class="form-control" name="moneyMin" placeholder="账户余额大于" style="width:120px;">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="unloginDay" placeholder="最近多少天没登录">
			</div>
			<c:if test="${not empty score20 && score20 eq 'on'}">
				<a class="btn btn-primary todo-ajax" href="${base}/agent/member/manager/scoreToZero.do" title="确定要将所有用户积分归零？">所有用户积分归零</a>
			</c:if>
		</div>
	</div>
</form>
<div style="padding: 10px;">
	<div style="color:red;">查询条件中的日期只针对会员注册日期进行查询</div>
</div>
<table class="fui-default-table"></table>
<script type="text/javascript">
$('#select_id2').on('changed.bs.select', function(e) {
    $("#input_select_id2").val($(this).val());
});
</script>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#member_manager_form_id");
	Fui.addBootstrapTable({
		url : '${base}/agent/member/manager/list.do',
		queryParams : function(params){
			var keyName = $form.find("[name='keyName']").val()
				,value = $form.find("[name='keyword']").val();
			if(keyName == 'levelGroup'){
				value = $form.find("[name='slevelGroup']").val();
			}
			params[keyName] = value;
			return params
		},//参数
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'agentName',
			title : '所属代理',
			align : 'center',
			valign : 'middle',
			formatter : agentFormatter
		}, {
			field : 'money',
			title : '账号余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		},
		<c:if test="${memberRateFlag}"> {
			field : 'rate',
			title : '自身赔率点',
			align : 'center',
			valign : 'middle'}, </c:if>
		<c:if test="${scoreShowFlag}">
		{
			field : 'score',
			title : '积分余额',
			align : 'center',
			sortable:true,
			valign : 'middle'
		}, 
		</c:if>
		{
			field : 'userName',
			title : '会员姓名',
			align : 'center',
			valign : 'middle'
		},{
			field : 'levelGroup',
			title : '会员等级',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : levelFormatter
		},{
			field : 'phone',
			title : '电话/手机号',
			align : 'center',
			valign : 'middle'
		},{
			field : 'qq',
			title : 'qq/微信',
			align : 'center',
			valign : 'middle',
			formatter :function(value, row, index){
				var qq = value;
				var wechat = row.wechat;
				if(qq == null || qq == undefined || qq == ''){
					qq = "-";
				}
				if(wechat == null || wechat == undefined || wechat == ''){
					wechat = "-";
				}
				return qq+"<br>"+wechat;
			}
		},{
			field : 'reportType',
			title : '报表类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		},{
			field : 'createDatetime',
			title : '注册时间',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){
				if(row.registerUrl){
					return Fui.formatDatetime(value)+"<br>"+row.registerUrl;
				}else{
					return Fui.formatDatetime(value)||"";
				}
			}
		}, {
			field : 'lastLoginDatetime',
			title : '最后登录',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){
				if(row.lastLoginIp){
					return row.lastLoginIp+"<br>"+Fui.formatDatetime(value)+"<br>"+row.lastLoginIpAddress;
				}else{
					return row.lastLoginIp||"";
				}
			}
		},{
                field : 'registerIp',
                title : '注册IP',
                align : 'center',
                valign : 'middle',
                formatter :function(value, row, index){
                    if(row.registerIp){
                        return row.registerIp+"<br>"+row.registerIpAddress;
                    }else{
                        return value||"";
                    }
                }
            }, {
			field : 'online',
			title : '在线情况',
			width:'50',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : onlineFormatter
		}, {
			field : 'accountStatus',
			title : '状态',
			align : 'center',
			width:'80',
			sortable:true,
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '查看',
			align : 'center',
			valign : 'middle',
			width:'120',
			formatter : fastViewFormatter
		}, {
			title : '操作',
			align : 'center',
			valign : 'middle',
			width:'120',
			formatter : operateFormatter
		},{
			field : 'remark',
			title : '备注内容',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : remarkFormatter
		} ]
	});
	function accountFormatter(value, row, index) {
		if(${warnFlag} && row.abnormalFlag==1){
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>&nbsp;<span class=\'label label-danger\'>警</span>' ].join('');
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function agentFormatter(value,row,index){
		return ['<a class="open-tab" href="${base}/agent/agent/manager/index.do?parents=',row.parents,'" title="查看代理所属下级">',value,'</a>' ].join('');
	}
	function remarkFormatter(value, row, index) {
		var content = value;
		if(content && content.length > 6){
			content = content.substring(0,6)+"...";
			
		}if(!content || content.length == 0 || content.trim() ==0) {
			content = "添加备注";
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/updateRemark.do?id=',row.id,'" remark="'+value+'"><span class="text-danger">',content,'</span></a>' ].join('');
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		var f = parseFloat(value);
		f=Math.round(f*100) / 100;
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(f);
		}
		return [ '<span class="text-primary">', '</span>' ].join(f);
	}
	var levels ={};
	<c:forEach items="${levels}" var="l">levels['${l.id}']='${l.levelName}';</c:forEach>
	function levelFormatter(value, row, index) {
		return [ '<span class="text-primary">', '</span>' ].join(levels[value]);
	}
	function onlineFormatter(value, row, index) {
		if (value === 2) {
			return '<span class="text-danger label label-success">在线</span>';
		}
		return '<span class="text-muted label label-default">离线</span>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/member/manager/updStatus.do?id="+row.id+"&accountStatus="});
	}
	function reportFormatter(value, row, index) {
		var reportTypes = {"1":"普通","2":"引导"};
		return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
	}
	function fastViewFormatter(value, row, index) {
		return '<a class="open-tab" href="${base}/agent/finance/memmnyrd/index.do?account='+row.account+'" title="会员账变管理">账变</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/global/index.do?account='+row.account+'" title="全局报表管理">报表</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/risk/index.do?account='+row.account+'&navName=cunqu" title="系统风险评估">财务</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/memberData/index.do?account='+row.account+'" title="会员数据概况">概况</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/gameRecord/index.do?account='+row.account+'" title="彩票投注">投注</a>';
	}
	function operateFormatter(value, row, index) {
		if(row.accountType==1 || row.accountType==9){
			var a = '<a class="open-dialog" href="${base}/agent/member/manager/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp;';
		}else if(row.accountType==4){
			var a = '<a class="open-dialog" href="${base}/agent/agent/manager/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp;';
		}
		var s =a+'<a class="open-dialog" href="${base}/agent/member/manager/modifyPwd.do?id='+row.id+'" title="修改密码">修改密码</a>';
		var r='';
		<c:if test="${ changeDail eq 'on'}">
			 r='<a class="open-dialog" href="${base}/agent/member/manager/modifyMemberToDaili.do?accountId='+row.id+'" title="转代理">转代理</a> &nbsp;' 
		</c:if>
		<c:if test="${ warnFlag}">
			if(row.abnormalFlag==1){
				s += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=0&accountId='+row.id+'&account='+row.account+'" title="取消告警">取消告警</a>';
			}else{
				s += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=1&accountId='+row.id+'&account='+row.account+'" title="设置告警">设为告警</a>';
			}
		</c:if>
		return r+s;
	}
});
</script>