<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="member_manager_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加会员</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<input type="hidden" name="modifyType" value="1">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">登录账号：</td>
						<td width="30%" class="text-left"><input type="text" class="form-control" name="account" /></td>
						<td width="20%" class="text-right media-middle">指定代理：</td>
						<td width="30%" class="text-left"><input type="text" name="agentName" class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">密码：</td>
						<td class="text-left"><input type="password" class="form-control" name="pwd" /></td>
						<td class="text-right media-middle">确认密码：</td>
						<td class="text-left"><input type="password" class="form-control" name="rpwd" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员姓名：</td>
						<td class="text-left"><input type="text" class="form-control" name="userName" /></td>
						<td class="text-right media-middle">电话：</td>
						<td class="text-left"><input type="text" class="form-control phone" name="phone" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">邮箱：</td>
						<td class="text-left"><input type="text" class="form-control email" name="email" /></td>
						<td class="text-right media-middle">QQ：</td>
						<td class="text-left"><input type="text" class="form-control" name="qq" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">微信号：</td>
						<td class="text-left"><input type="text" class="form-control" name="wechat" /></td>
						<td class="text-right media-middle">取现银行：</td>
						<td class="text-left"><input name="bankName" class="form-control" type="text" placeholder="请输入银行名称"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">银行账号：</td>
						<td class="text-left"><input type="text" class="form-control" name="cardNo" /></td>
						<td class="text-right media-middle">银行地址：</td>
						<td class="text-left"><input type="text" class="form-control" name="bankAddress" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员等级：</td>
						<td><select name="levelGroup" class="form-control">
						<c:forEach items="${levels }" var="l"><option value="${l.id }">${l.levelName }</option></c:forEach>
						</select></td>
						<td class="text-right media-middle">会员状态：</td>
						<td class="text-left"><select name="accountStatus" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">禁用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员类型：</td>
						<td class="text-left"><select id="accountType" name="accountType" class="form-control">
							<option value="1">会员</option>
							<option value="9">引导</option>
						</select></td>
						<td class="text-right media-middle">报表类型：</td>
						<td class="text-left"><select name="reportType" class="form-control">
							<option value="1">普通</option>
							<option value="2">引导</option>
						</select></td>
						<input type="hidden" id="reportType" name="reportType" value="1">
					</tr>
					<c:if test="${memberRateFlag}"><tr>
						<td class="text-right media-middle">自身返点数：</td>
						<td><input type="text" class="form-control" name="rate" value="0"/></td>
						<td class="media-middle" colspan="2">自身返点数可设置返点区间：<span id="min_span"></span> - <span id="max_span"></span></td>
					</tr></c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#member_manager_add_form_id");
	$form.submit(function(){
		var password = $form.find("[name='pwd']").val()
			,rpassword = $form.find("[name='rpwd']").val()
			,rate = $form.find("[name='rate']").val();
		if (!password) {
			layer.msg("密码不能为空！");
			return false;
		}
		if (!rpassword) {
			layer.msg("确认密码不能为空！");
			return false;
		}
		if (password !== rpassword) {
			layer.msg("两次密码不一致！");
			return false;
		}
		
		<c:if test="${memberRateFlag}">
			if (minRate != -1 && maxRate != -1 && (rate > maxRate || minRate > rate)) {
				layer.msg("自身返点超超过限制！");
				return false;
			}
		</c:if>
		
		$.ajax({
			url : "${base}/agent/member/manager/checkBankCard.do",
			data : {cardNo :$form.find("[name='cardNo']").val()},
			success : function(result) {
				if(result){
					layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
						btn : [ '继续','取消' ]
					}, function() {
						confirmCommit();
					});
				}else{
					confirmCommit();
				}
			}
		});
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/member/manager/save.do",
			data : $form.serialize(),
			success : function(result) {
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
	
	<c:if test="${memberRateFlag}">
		var lastAcc = "";
		var minRate = -1;
		var maxRate = -1;
		$form.find("[name='agentName']").blur(function(){
			var accountName = this.value;
			if(lastAcc == accountName){
				return;
			}
			$.ajax({
				url : "${base}/agent/member/manager/multidata.do",
				data : {
					agentName : accountName
				},
				success : function(result) {
					lastAcc = accountName;
					minRate = 0;
					maxRate = result.curDynamicRate;
					$("#min_span").text(minRate);
					$("#max_span").text(maxRate);
				}
			});
		});
	</c:if>
});
</script>