<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<style>
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {padding:2px}
</style>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">查看会员详细信息</h4>
		</div>
		<div class="modal-body" id="view_detail_manager_member">
			<table class="table table-bordered table-striped">
				<input type="hidden" id="id" value="${map.id }"/>
				<tbody>
					<tr>
						<th width="18%" class="text-right">登录账号：</th>
						<td width="30%">${map.account }
							<c:if test="${onoffcopy eq 'on'}">
								<span class="label label-primary copy-btn-moble" role="button" account="${map.account }">复制</span>
							</c:if>
						</td>
						<th width="18%" class="text-right">所属上级：</th>
						<td>${parentNames }</td>
					</tr>
					<tr>
						<th class="text-right">会员姓名：</th>
						<td>${map.userName }</td>
						<th class="text-right">类别：</th>
						<td><c:if test="${map.accountType==1 }">会员</c:if><c:if test="${map.accountType==4 }">代理</c:if></td>
					</tr>
					<tr>
						<th class="text-right">会员余额：</th>
						<td><fmt:formatNumber pattern="#,##0.00" value="${map.money }"/></td>
						<th class="text-right">积分：</th>
						<td>${map.score}</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">余额宝余额：</th>
						<td>${map.balanceGemMoney}</td>
					</tr>
					<c:if test="${third}">
					<tr class="advanced hidden">
						<th class="text-right">第三方余额：</th>
						<td colspan="3"><c:if test="${isBBIN}">
								BBIN余额:<span class="text-danger balance-2">0</span>
		                    	<a href="javascript:void(0)"
									class="refresh" data-thirdid="2">刷新</a>
							</c:if> <c:if test="${isAG}">
								AG余额:<span class="text-danger balance-1">0</span>
		                        <a href="javascript:void(0)"
									class="refresh" data-thirdid="1">刷新</a>
							</c:if> <c:if test="${isMG}">
								MG余额:<span class="text-danger balance-3">0</span>
		                    	<a href="javascript:void(0)" class="refresh"
									data-thirdid="3">刷新</a>
							</c:if> <c:if test="${isPT}">
								PT余额:<span class="text-danger balance-4">0</span>
			                    <a href="javascript:void(0)" class="refresh"
									data-thirdid="4">刷新</a>
							</c:if> <c:if test="${isQT}">
								QT余额:<span class="text-danger balance-5">0</span>
			                    <a href="javascript:void(0)" class="refresh"
									data-thirdid="5">刷新</a>
							</c:if> <c:if test="${isAB}">
								Allbet余额:<span class="text-danger balance-6">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="6">刷新</a>
							</c:if> <c:if test="${isOG}">
								OG余额:<span class="text-danger balance-7">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="7">刷新</a>
							</c:if> <c:if test="${isDS}">
								DS余额:<span class="text-danger balance-8">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="8">刷新</a>
							</c:if><c:if test="${isCQ9}">
								CQ9余额:<span class="text-danger balance-9">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="9">刷新</a>
			               	</c:if>
			               	<c:if test="${isJDB}">
								JDB余额:<span class="text-danger balance-11">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="11">刷新</a>
			               	</c:if>
			               	<c:if test="${isTTG}">
								TTG余额:<span class="text-danger balance-92">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="92">刷新</a>
			               	</c:if>
			               	<c:if test="${isMW}">
								MW余额:<span class="text-danger balance-95">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="95">刷新</a>
			               	</c:if>
			               	<c:if test="${isISB}">
								ISB余额:<span class="text-danger balance-96">0</span>
			                    <a href="javascript:void(0)" class="refresh" data-thirdid="96">刷新</a>
			               	</c:if>
							<c:if test="${isM8}">
								M8余额:<span class="text-danger balance-99">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="99">刷新</a>
							</c:if>
							<c:if test="${isM8H}">
								M8H余额:<span class="text-danger balance-991">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="991">刷新</a>
							</c:if>
							<c:if test="${isIBC}">
								沙巴余额:<span class="text-danger balance-10">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="10">刷新</a>
							</c:if>
							<c:if test="${isVR}">
								VR余额:<span class="text-danger balance-97">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="97">刷新</a>
							</c:if>
							<c:if test="${isKY}">
								KY余额:<span class="text-danger balance-12">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="12">刷新</a>
							</c:if>
							<c:if test="${isEBET}">
								EBET余额:<span class="text-danger balance-13">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="13">刷新</a>
							</c:if>
							<c:if test="${isBg}">
								BG余额:<span class="text-danger balance-98">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="98">刷新</a>
							</c:if>
							<c:if test="${isTH}">
								TH余额:<span class="text-danger balance-14">0</span>
			                    <a href="javascript:void(0)"
									class="refresh" data-thirdid="14">刷新</a>
							</c:if>
							<c:if test="${isKX}">
								KX余额:<span class="text-danger balance-16">0</span>
								<a href="javascript:void(0)"
								   class="refresh" data-thirdid="16">刷新</a>
							</c:if>
						</td>
					</tr>
					</c:if>
					<tr class="advanced hidden">
						<th class="text-right">打码量：</th>
						<td>${map.betNum}</td>
						<th class="text-right">出款所需打码量：</th>
						<td>${map.drawNeed }</td>
					</tr>
					<tr>
						<th class="text-right">状态：</th>
						<td><c:if test="${map.accountStatus==2 }">启用</c:if><c:if test="${map.accountStatus!=2 }">禁用</c:if></td>
						<th class="text-right">会员等级：</th>
						<td><c:forEach items="${levels }" var="l"><c:if test="${l.id==map.levelGroup }">${l.levelName }</c:if></c:forEach></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">电话：</th>
						<td id="phone">${map.phone }</td>
						<th class="text-right">微信号：</th>
						<td id="wechat">${map.wechat }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">QQ：</th>
						<td id="qq">${map.qq }</td>
						<th class="text-right">邮箱：</th>
						<td id="email">${map.email }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">银行地址：</th>
						<td id="bankAddress">${map.bankAddress }</td>
						<th class="text-right">银行账号：</th>
						<td id="cardNo">${map.cardNo }
						<c:if test="${map.cardNo ne '' }">
							<c:if test="${map.cardNoStatus == 1 }">
								<span class="label label-success" id="cardNoStatus_span">可用</span>
							</c:if>
							<c:if test="${map.cardNoStatus == 2 }">
								<span class="label label-danger" id="cardNoStatus_span">禁用</span>
							</c:if>
						</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">银行卡所在省：</th>
						<td id="qq">${map.province }</td>
						<th class="text-right">银行卡所在市：</th>
						<td id="email">${map.city }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">取现银行：</th>
						<td id="bankName">${map.bankName }</td>
						<th class="text-right">注册时间：</th>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.createDatetime}" /></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">注册IP：</th>
						<td>${map.registerIp }</td>
						<th class="text-right">注册来源：</th>
						<td>${map.registerUrl }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">注册系统：</th>
						<td>${map.registerOs }</td>
						<th class="text-right">最后登录时间：</th>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.lastLoginDatetime }" />&nbsp&nbsp&nbsp&nbsp&nbsp${map.lastLoginIp }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">总提款次数：</th>
						<td><c:if test="${!empty map.countDraw }">${map.countDraw }</c:if><c:if test="${empty map.countDraw }">-</c:if></td>

						<th class="text-right">总提款金额：</th>
						<td><c:if test="${!empty map.totalDrawMoney }"><fmt:formatNumber pattern="#,##0.00" value="${map.totalDrawMoney }"/></c:if><c:if test="${empty map.totalDrawMoney }">-</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">总充值次数：</th>
						<td><c:if test="${!empty map.countCom }">${map.countCom }</c:if><c:if test="${empty map.countCom }">-</c:if></td>

						<th class="text-right">总充值金额：</th>
						<td><c:if test="${!empty map.totalComMoney }"><fmt:formatNumber pattern="#,##0.00" value="${map.totalComMoney }"/></c:if><c:if test="${empty map.totalComMoney }">-</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">首次提款：</th>
						<td><c:if test="${!empty map.firstDrawDate and !empty map.firstDrawDate}"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.firstDrawDate}" />提款<font style="color:crimson"><fmt:formatNumber pattern="#,##0.00" value="${map.firstDrawMoney }"/></font>元</c:if> <c:if test="${empty map.firstDrawDate and empty map.firstDrawDate}">-</c:if></td>
						<th class="text-right">最高提款：</th>
						<td><c:if test="${!empty map.maxDrawDate and !empty map.maxDrawMoney}"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.maxDrawDate}" />提款<font style="color:crimson"><fmt:formatNumber pattern="#,##0.00" value="${map.maxDrawMoney }"/></font>元</c:if><c:if test="${empty map.maxDrawDate and empty map.maxDrawMoney}">-</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">首次充值：</th>
						<td><c:if test="${!empty map.firstComDate and !empty map.firstComDate}"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.firstComDate}" />充值<font style="color:crimson"><fmt:formatNumber pattern="#,##0.00" value="${map.firstComMoney }"/></font>元</c:if> <c:if test="${empty map.firstComDate and empty map.firstComDate}">-</c:if></td>
						<th class="text-right">最高充值：</th>
						<td><c:if test="${!empty map.maxComDate and !empty map.maxComMoney}"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${map.maxComDate}" />充值<font style="color:crimson"><fmt:formatNumber pattern="#,##0.00" value="${map.maxComMoney }"/></font>元</c:if><c:if test="${empty map.maxComDate and empty map.maxComMoney}">-</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">今日提款次数：</th>
						<td><c:if test="${!empty map.todayDrawTimes }">${map.todayDrawTimes }</c:if><c:if test="${empty map.todayDrawTimes }">-</c:if></td>
						
						<th class="text-right">今日提款金额：</th>
						<td><c:if test="${!empty map.todayDrawMoney }"><fmt:formatNumber pattern="#,##0.00" value="${map.todayDrawMoney }"/></c:if><c:if test="${empty map.todayDrawMoney }">-</c:if></td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">今日充值次数：</th>
						<td><c:if test="${!empty map.todayComTimes }">${map.todayComTimes }</c:if><c:if test="${empty map.todayComTimes }">-</c:if></td>
						
						<th class="text-right">今日充值金额：</th>
						<td><c:if test="${!empty map.todayComMoney }"><fmt:formatNumber pattern="#,##0.00" value="${map.todayComMoney }"/></c:if><c:if test="${empty map.todayComMoney }">-</c:if></td>
					</tr>
					<tr>
						<th class="text-right">备注：</th>
						<td colspan="3">${map.remark }</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
<script type="text/javascript">
requirejs(['zeroclipboard','jquery','bootstrap','Fui'],function(ZeroClipboard){
	var $con=$("#view_detail_manager_member");
	var $initThirdId = [];
	$con.find(".refresh").click(function(){
		refresh($(this).data("thirdid"));
	});
	
	$con.find(".refresh").each(function(){
		$initThirdId.push($(this).data("thirdid"))
	});

	var client = new ZeroClipboard($('.copy-btn-moble'));
	client.on( "ready", function (event) {
		client.on( "copy", function (event) {
			var t= $(event.target).attr("account");
			ZeroClipboard.setData( "text/plain",t);
			layer.msg('复制成功,请按ctrl+v粘贴:<br/>'+t,{time:1000});
		});
	});

	<c:if test="${viewPerm}">
	$(".advanced").removeClass("hidden");
	function refresh(thirdId) {
		var $bal=$con.find(".balance-"+thirdId).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />")
		$.ajax({
	        url:"${base}/agent/rgub/getBalance.do",
	        type:'POST',
	        data:{gameType:thirdId,accountId:'${map.id}'},
	        success:function(json){
				if (json.msg) {
					$bal.html(json.msg);
				}else {
					$bal.html(json.balance);
				}
	        },
	        errorFn:function(json){
	        	$bal.html(json.msg);
	        },
	        error:function(){
	        	$bal.html("链接异常");
	        }
	    });
	}
	
	setTimeout(function(){for (var i = 0; i < $initThirdId.length; i++) {
		refresh($initThirdId[i]);
	}},200);

	</c:if>
});

</script>