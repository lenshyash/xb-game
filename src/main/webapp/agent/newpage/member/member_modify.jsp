<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="member_manager_modify_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改会员</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${member.id }">
		<input type="hidden" name="modifyType" value="1">
		<input type="hidden" name="moveFlag" value="1">
		<input type="hidden" id="oldAgentName" value="${member.agentName }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">登录账号：</td>
						<td width="30%"><input type="text" class="form-control" name="account" disabled value="${member.account }"/></td>
						<td width="20%" class="text-right media-middle">指定代理：</td>
						<td width="30%"><input type="text" name="agentName" value="${member.agentName }"class="form-control" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员姓名：</td>
						<td><input type="text" class="form-control" disabled="disabled" name="userName" value="${memberInfo.userName }"/></td>
						<td class="text-right media-middle">会员等级：</td>
						<td><select name="levelGroup" class="form-control"  <c:if test="${empty modifyLevel or !modifyLevel }">disabled</c:if> >
						<c:forEach items="${levels }" var="l"><option value="${l.id }"<c:if test="${l.id==member.levelGroup }">selected</c:if>>${l.levelName }</option></c:forEach>
						</select></td>
					</tr>
					<c:if test="${modifyPerm}"><tr class="advanced_tr">
						<td class="text-center" colspan="4"><a href="#"><div style="float: top;">更多内容</div><div style="float: bottom;"><span id="advanced_span" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div></a>
						</td> 
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">电话：</td>
						<td><input type="text" class="form-control phone" name="phone" value="${memberInfo.phone }"/></td>
						<td class="text-right media-middle">微信号：</td>
						<td class="text-left"><input type="text" class="form-control" name="wechat" value="${memberInfo.wechat }" /></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">邮箱：</td>
						<td><input type="text" class="form-control email" name="email" value="${memberInfo.email }"/></td>
						<td class="text-right media-middle">QQ：</td>
						<td><input type="text" class="form-control" name="qq" value="${memberInfo.qq }"/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">取现银行：</td>
						<td><input name="bankName" value="${memberInfo.bankName }"class="form-control" type="text" placeholder="请输入银行名称"></td>
						<td class="text-right media-middle">银行账号：</td>
						<td><input type="text" class="form-control" name="cardNo" value="${memberInfo.cardNo }" placeholder="请输入银行卡号"/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">银行卡所在省：</td>
						<td><input name="province" value="${memberInfo.province }"class="form-control" type="text" placeholder="请输入银行卡所在省份"></td>
						<td class="text-right media-middle">银行卡所在市：</td>
						<td><input type="text" class="form-control" name="city" value="${memberInfo.city }" placeholder="请输入银行卡所在城市"/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">银行地址：</td>
						<td><input type="text" class="form-control" name="bankAddress" value="${memberInfo.bankAddress }"/></td>
						<td class="text-right media-middle">银行卡黑名单：</td>
						<td><select name="cardNoStatus" class="form-control">
						<option value="1" <c:if test="${memberInfo.cardNoStatus==1}">selected</c:if>>启用</option><option value="2" <c:if test="${memberInfo.cardNoStatus==2}">selected</c:if>>禁用</option></select></td>
					</tr></c:if>
					<tr>
						<td class="text-right media-middle">会员状态：</td>
						<td><select name="accountStatus" class="form-control">
							<option value="2"<c:if test="${member.accountStatus==2}">selected</c:if>>启用</option>
							<option value="1"<c:if test="${member.accountStatus==1}">selected</c:if>>禁用</option>
						</select></td>
						<td class="text-right media-middle">注册IP：</td>
						<td class="media-middle">${member.registerIp}</td>
					</tr>
					<tr>
					<td class="text-right">注册来源：</td>
						<td>${member.registerUrl}</td>
						<td class="text-right">注册系统：</td>
						<td>${member.registerOs}</td>
					</tr>
					<tr>
						<td class="text-right media-middle">会员类型：</td>
						<td class="text-left media-middle">
							<c:if test="${member.accountType==1}">普通<input type="hidden" value="1" name="accountType"></c:if>
							<c:if test="${member.accountType==9}">引导<input type="hidden" value="9" name="accountType"></c:if>
						</td>
						<td class="text-right media-middle">报表类型：</td>
						<td class="text-left media-middle">
							<c:if test="${member.reportType==1}">普通<input type="hidden" value="1" name="reportType"></c:if>
							<c:if test="${member.reportType==2}">引导<input type="hidden" value="2" name="reportType"></c:if>
						</td>
					</tr>
					<c:if test="${memberRateFlag}"><tr>
						<td class="text-right media-middle">自身返点数：</td>
						<td><input type="text" class="form-control" name="rate" value="${member.rate }" <c:if test="${empty modifyRate or !modifyRate }">disabled</c:if>/></td>
						<td class="media-middle" colspan="2">自身返点数可设置返点区间：<span id="min_span">${multiAgent1.dynamicRateMin}</span> - <span id="max_span">${multiAgent1.dynamicRateMax }</span></td>
					</tr></c:if>
					<tr>
						<td class="text-right">聊天室放红包限制：</td>
						<td colspan="3">
						<input type="radio" name="redpacketLimit" <c:if test="${member.redpacketLimit==2}">checked="checked"</c:if> value="2"/>允许
						<input type="radio" name="redpacketLimit" <c:if test="${member.redpacketLimit==1}">checked="checked"</c:if>value="1"/>禁止
						</td>
					</tr>
					<tr>
						<td class="text-right">所属上级：</td>
						<td >${parentNames }</td>
						<td class="text-right">聊天室token：</td>
						<td><input type="text" class="form-control" name="chatToken" value="${member.chatToken }"/></td>
					</tr>
					<tr>
						<td class="text-right">备注内容：</td>
						<td colspan="3"><textarea class="form-control" name="remark">${member.remark }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#member_manager_modify_form_id");
	
	$form.submit(function(){
		var oldAgentName= $("#oldAgentName").val();
		var agentName = $form.find("[name='agentName']").val();
		
		var rate = $form.find("[name='rate']").val();
		<c:if test="${memberRateFlag}">
			if (minRate != -1 && maxRate != -1 && (rate > maxRate || minRate > rate)) {
				layer.msg("自身返点超超过限制！");
// 				return false;
			}
		</c:if>
		
		if(oldAgentName && oldAgentName != agentName){
			layer.confirm('修改上级代理，是否同时转移该会员数据？', {
				btn : [ '是','否' ]
			}, function() {
				$form.find("[name='moveFlag']").val(2);
				commitForm();
			}, function() {
				commitForm();
			});
		}else{
			commitForm();
		}
		
		return false;
	});
	
	function commitForm(){
		if($form.find("[name='modifyType']").val() == 2){
			$.ajax({
				url : "${base}/agent/member/manager/checkBankCard.do",
				data : {id:$form.find("[name='id']").val(),cardNo :$form.find("[name='cardNo']").val()},
				success : function(result) {
					if(result){
						layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
							btn : [ '继续','取消' ]
						}, function() {
							confirmCommit();
						});
					}else{
						confirmCommit();
					}
				}
			});
		}else{
			confirmCommit();
		}
	}
	
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/member/manager/save.do",
			data : $form.serialize(),
			success : function(result) {
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
	
	<c:if test="${modifyPerm}">$(".advanced_tr").click(function(){
		$(".advanced_tr").addClass("hidden");
		$(".advanced").removeClass("hidden");
		$form.find("[name='modifyType']").val(2);
	});</c:if>
	
	<c:if test="${onlyReadUserName}">
		$("input[name='userName']").removeAttr("disabled");
	</c:if>
	<c:if test="${memberRateFlag}">
		var lastAcc = "";
		var minRate = -1;
		var maxRate = -1;
		$form.find("[name='agentName']").blur(function(){
			var accountName = this.value;
			if(lastAcc == accountName){
				return;
			}
			$.ajax({
				url : "${base}/agent/member/manager/multidata.do",
				data : {
					agentName : accountName
				},
				success : function(result) {
					lastAcc = accountName;
					minRate = 0;
					maxRate = result.curDynamicRate;
					$("#min_span").text(minRate);
					$("#max_span").text(maxRate);
				}
			});
		});
	</c:if>
});
</script>