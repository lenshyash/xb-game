<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="agent_manager_add_form_id5">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">会员转代理</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td><input type="hidden" class="form-control" name="accountId" value="${account.id }"/></td>
					<tr>
						<td class="text-right media-middle">会员账号：</td>
						<td><input type="text" class="form-control" name="accountId" readonly="readonly" value="${account.account }"/></td>
					</tr>
					<c:if test="${multiAgent.multiAgent== 'on' && multiAgent.mergeRebate ne 'on'}"><tr>
						<td class="text-right media-middle">返点数：</td>
						<td><input type="text" class="form-control" name="rebateNum" value="0.0" readonly="readonly"/></td>
						<td><span class="text-danger">会员转代理后返点数会调整为0 如有需要可自行调整</span></td>
						
					</tr></c:if>
					<c:if test="${multiAgent.dynamicRate== 'on'}"><tr>
						<td class="text-right media-middle">赔率值：</td>
						<td><input type="text" class="form-control" name="dynamicRate" value="${multiAgent.dynamicRateMax }" readonly="readonly"/></td>
						<td><span class="text-danger">会员转代理后赔率值还是预设值 如有需要可自行调整</span></td>
					</tr></c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#agent_manager_add_form_id5");
	
	$form.submit(function(){
		$.ajax({
			url : "${base}/agent/agent/manager/checkBankCard.do",
			data : {cardNo :$form.find("[name='cardNo']").val()},
			success : function(result) {
				if(result){
					layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
						btn : [ '继续','取消' ]
					}, function() {
						confirmCommit();
					});
				}else{
					confirmCommit();
				}
			}
		});
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/member/manager/changeMemberToDaili.do",
			data : $form.serialize(),
			success : function(result) {
				console.log(result);
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
});
</script>