<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="member_online_form_id">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="input-group">
				<select class="form-control" name="level">
					<option value="">危险等级</option>
					<option value="1">普通</option>
					<option value="2">高危</option>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" name="status">
					<option value="">状态</option>
					<option value="1">失效</option>
					<option value="2">生效</option>
				</select>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div>
<span class="text-danger" style="float: left;">提示:</span></br>
<span class="text-danger" style="float: left;">1.预警用户是指系统检测出七天以内注册存在异常充值、异常套利、异常账变的用户</span></br>
<span class="text-danger" style="float: left;">2.预警高危等级是指预警用户在十分钟之内存在提款、投注、额度转换等资金操作</span>
</div>

<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#member_online_form_id");
	Fui.addBootstrapTable({
		url : '${base}/agent/member/warning/list.do',
		queryParams : function(params){
			return params
		},//参数
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		},{
			field : 'level',
			title : '级别',
			align : 'center',
			valign : 'middle',
			formatter : accountLevelFormat
		}, {
			field : 'accountStatus',
			title : '用户状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter,
		}, {
			field : 'status',
			title : '预警状态',
			align : 'center',
			valign : 'middle',
			formatter : warnStatusFormatter,
		},{
			field : 'createTime',
			title : '创建时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		},{
			field : 'lastLoginIp',
			title : '最后登录ip',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'lastLoginDatetime',
			title : '最后登录时间',
			align : 'center',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'remark',
			title : '描述',
			align : 'center',
			valign : 'middle',
		} , {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.accountId,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		var f = parseFloat(value);
		f=Math.round(f*100) / 100;
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(f);
		}
		return [ '<span class="text-primary">', '</span>' ].join(f);
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/member/manager/updStatus.do?id="+row.id+"&accountStatus="});
	}
	function warnStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/member/warning/updStatus.do?id="+row.id+"&accountId="+row.accountId+"&status="});
	}
	function accountLevelFormat(value,row,index){
		var val = "";
		if(value==1){
			val = "普通";
		}else if(value==2){ddd
			val = "高危";
		}
		return val;
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-tab" href="${base}/agent/finance/memmnyrd/index.do?account='+row.account+'" title="查看账变">查看账变</a> &nbsp; ';
// 		'<a class="todo-ajax" href="${base}/agent/member/manager/forced.do?id='+row.id+'" title="确定要强制下线吗？">强制下线</a> &nbsp;';
		
	}
});
</script>