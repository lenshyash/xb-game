<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="member_online_form_id">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="keyName">
					<option value="account">会员账号</option>
					<option value="agentName">所属上级</option>
					<option value="children">所有下级</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="keyword" placeholder="关键字">
			</div>
			<div class="input-group">
				<select class="form-control" name="accountType">
					<option value="">账号类型</option>
					<option value="1">会员账号</option>
					<option value="4">代理账号</option>
					<option value="9">引导账号</option>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" name="reportType">
					<option value="">报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
				</select>
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#member_online_form_id");
	Fui.addBootstrapTable({
		url : '${base}/agent/member/manager/ollist.do',
		queryParams : function(params){
			var keyName = $form.find("[name='keyName']").val()
				,value = $form.find("[name='keyword']").val();
			params[keyName] = value;
			return params
		},//参数
		columns : [ {
			field : 'account',
			title : '会员账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'money',
			title : '账号余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		},
		<c:if test="${isBgemOnOff}">
		{
			field : 'balanceGemMoney',
			title : '余额宝余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, 
		</c:if>
		{
			field : 'agentName',
			title : '所属代理',
			align : 'center',
			valign : 'middle'
		},
		<c:if test="${scoreShowFlag}">
		{
			field : 'score',
			title : '积分余额',
			align : 'center',
			valign : 'middle'
		}, 
		</c:if>
		<c:if test="${agentShowFlag}">
		{
			field : 'accountType',
			title : '账号类型',
			align : 'center',
			valign : 'middle',
			formatter : accountTypeFormat
		}, 
		</c:if>
		{
			field : 'userName',
			title : '会员姓名',
			align : 'center',
			valign : 'middle'
		}, {
			sortable:true,
			field : 'createDatetime',
			title : '注册时间',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'accountStatus',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter,
		}, {
			field : 'lastLoginIp',
			title : '最后登录ip',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'lastLoginDevice',
			title : '最后登录设备',
			align : 'center',
			valign : 'middle',
			formatter : deviceFormatter
		}, {
			field : 'lastLoginIpAddress',
			title : '最后登录ip地址',
			align : 'center',
			valign : 'middle'
		},
		<c:if test="${warnFlag}">
			{
				field : 'lastLoginIpAddress',
				title : '告警状态',
				align : 'center',
				valign : 'middle',
				formatter : warningFormatter
			},
		</c:if>
			{
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function accountFormatter(value, row, index) {
		if(${warnFlag} && row.abnormalFlag==1){
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>&nbsp;<span class=\'label label-danger\'>警</span>' ].join('');
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		var f = parseFloat(value);
		f=Math.round(f*100) / 100;
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(f);
		}
		return [ '<span class="text-primary">', '</span>' ].join(f);
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/member/manager/updStatus.do?id="+row.id+"&accountStatus="});
	}
	function accountTypeFormat(value,row,index){
		var val = "";
		if(value==1){
			val = "会员";
		}else if(value==4){
			val = "代理";
		}else if(value==5){
			val = "总代";
		}
		return val;
	}
	function operateFormatter(value, row, index) {
		var result = "";
		if(row.accountType ==1 ){
			result =  '<a class="open-dialog" href="${base}/agent/member/manager/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp; '+
			'<a class="todo-ajax" href="${base}/agent/member/manager/forced.do?id='+row.id+'" title="确定要强制下线吗？">强制下线</a> &nbsp; '+
			'<a class="open-tab" href="${base}/agent/report/memberData/index.do?begin=${beginTime}&end=${endTime}&account='+row.account+'" title="会员数据概况">概况</a>&nbsp;';
		}else{
			result = '<a class="open-dialog" href="${base}/agent/member/manager/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp; '+
			'<a class="todo-ajax" href="${base}/agent/member/manager/forced.do?id='+row.id+'" title="确定要强制下线吗？">强制下线</a> &nbsp;';
		}
		if(${warnFlag}){
			if( row.abnormalFlag==1){
				result += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=0&accountId='+row.id+'&account='+row.account+'" title="取消告警">取消告警</a>';
			}else{
				result += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=1&accountId='+row.id+'&account='+row.account+'" title="设置告警">设为告警</a>';
			}
		}
		return result;

		
	}
	function deviceFormatter(value, row, index) {
		var a = row.lastLoginDevice;

		if(a=="PC电脑"){
			a = "<span class='label label-primary'>"+a+"</span>";
		}else if(a=="android设备"){
			a = "<span class='label label-success'>"+a+"</span>";
		}else if(a=="苹果设备"){
			a = "<span class='label label-danger'>"+a+"</span>";
		} 
		return a;
	}

	function warningFormatter(value, row, index) {
		var a = '';
		if(row.iosFlag=='1'){
			a += "&nbsp;&nbsp;<span class='label label-danger'>变更设备</span>";
		}
		if(row.ipFlag=='1'){
			a += "&nbsp;&nbsp;<span class='label label-danger'>变更ip</span>";
		}
		if(row.domainFlag=='1'){
			a += "&nbsp;&nbsp;<span class='label label-danger'>变更域名</span>";
		}
		if(a==''){
			a = "<span class='label label-success'>正常</span>"
		}
		return a;
	}
});
</script>