<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="member_permission_myform_id">
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="form-inline">
			<input type="text" class="form-control" name="searchAccount" placeholder="会员账号">
			<button class="btn btn-primary search-btn" type="button">查询</button>
		</div>
	</div>
	<div class="panel-body hidden"><input type="hidden" name="accountId"/>
		<table class="table table-bordered" style="margin-top:10px;">
        <tbody>	
			<c:if test="${realgame}">
			<tr>
				<td width="20%" class="text-right active">真人转账：</td>
                   <td class="form-inline">
					<label class="radio"><input type="radio" name="realGameTransfer" value="2"> 开启</label>
					<label class="radio"><input type="radio" name="realGameTransfer" value="1"> 关闭</label>
					</td>
			</tr>
			</c:if>
			<tr><td colspan="2" class="text-center"><button class="btn btn-primary">保存</button></td></tr>
		</tbody>
		</table>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'], function() {
	var $form=$("#member_permission_myform_id")
		,$searchAccount=$form.find("[name='searchAccount']")
		,$searchBtn=$form.find(".search-btn")
		,accountId=null;
	$searchBtn.click(function(){
		var acc=$searchAccount.val();
		if(acc==''){
			layer.msg("会员账号不能为空");
			return false;
		}
		$.ajax({
			url:"${base}/agent/member/permission/getPermission.do?account="+acc,
			type:"GET",
			success:function(data){
				if(data.accountId){
					$form.find(".panel-body").removeClass("hidden");
					accountId=data.accountId;
					$form.find("[name='accountId']").val(data.accountId);
					var val = data.realGameTransfer == 1 ? 1 : 2;
					$form.find("input[name='realGameTransfer'][value="+val+"]").click(); 
				}else{
					$form.find(".panel-body").addClass("hidden");
				}
			},
			dataType:"JSON",
			errorFn:function(){
				$form.find(".panel-body").addClass("hidden");
			}
		});
	});
	$form.submit(function(){
		if(!accountId){
			layer.msg("会员账号不能为空");
			return false;
		}
		$.ajax({
			url : "${base}/agent/member/permission/savePermissionNew.do",
			data : $form.serialize(),
			success : function(data) {
				layer.msg(data.msg||"操作成功!");
			}
		});
		return false;
	});
});
</script>