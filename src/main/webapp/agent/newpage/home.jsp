<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>${_title }</title>
    <meta name="renderer" content="webkit">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<link rel="stylesheet" href="${base}/common/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${base}/common/css/main.min.css?v=1.32">
	<script>var baseInfo={account:"${USER_SESSION_AGENT.account}",baseUrl:"${base}",loginDialogUrl:"${base}/agent/loginDialog.do",indexUrl:"${base}/agent",navUrl:"${base}/agent/nav1.do",_home_ams:{ams :${_home_ams}}};</script>
	<script src="${base}/common/js/require.js"></script>
	<script>
	window.UEDITOR_HOME_URL="${base}/common/js/UEditor/";
	<c:if test="${not empty agent_mush_reset_pwd && agent_mush_reset_pwd}">baseInfo.mustUpdatePwd=true;</c:if>
	require.config({
		baseUrl: '${base}/common',
		paths: {
			jquery: 'jquery/jquery-1.12.4.min'
			,jquery_validation:'js/jquery-validate/jquery.validate.min'
			,bootstrap:'bootstrap/3.3.7/js/bootstrap.min'
			,bootstrap_editable:'bootstrap/editable/js/bootstrap-editable.min'
			,bootstrapTable:'bootstrap-table/bootstrap-table.min'
			,moment:'js/moment/moment.min'
			,moment_zh:'js/moment/locale/zh-cn'
			,datetimepicker: 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min'
			,layer:'js/layer3/layer.min'
			,Fui:'js/fui/fui.js?v=1.1.4'
			,fui_table:'js/fui/fui-table.js?v=1.0.5'
			,fui_form:'js/fui/fui-form.js?v=1.0.2'
			,fui_datetime:'js/fui/fui-datetime.js?v=1.0.1'
			,fui_switch:'js/fui/fui-switch.js?v=1.0.1'
			,template:'js/artTemplate/template'
			,bootstrapSwitch:'bootstrap/switch/js/bootstrap-switch.min'
			,ueditor: 'js/UEditor/ueditor.all.min'
			,ueditor_zh:'js/UEditor/zh-cn'
			,zeroclipboard:'js/UEditor/third-party/zeroclipboard/ZeroClipboard.min'
			,Chart:'bootstrap/chart/2.4.0/Chart.min'
			,bootstrapSelect:"bootstrap/select/js/bootstrap-select.min"
		},
		map: {
	      '*': {
	        'css': 'js/require-lib/css.min'
	        ,'text': 'js/require-lib/text'
	      } 
	    },
		shim: {
			'bootstrap':['jquery']
			,'bootstrap_editable':['jquery','bootstrap','datetimepicker','css!${base}/common/bootstrap/editable/css/bootstrap-editable.css']
			,'bootstrapTable':['jquery','bootstrap','css!${base}/common/bootstrap/table/css/bootstrap-table.min.css']
	    	,'datetimepicker':['jquery','css!${base}/common/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css']
	    	,'bootstrapSwitch':['jquery','css!${base}/common/bootstrap/switch/css/bootstrap-switch.min.css']
	    	,'bootstrapSelect':['css!${base}/common/bootstrap/select/css/bootstrap-select.min.css']
	    	,'ueditor': {
	            deps: ['zeroclipboard', '${base}/common/js/UEditor/ueditor.config.js'],
	            exports: 'UE',
	            init:function(ZeroClipboard){
	                //导出到全局变量，供ueditor使用
	                window.ZeroClipboard = ZeroClipboard;
	            }
	        }
	    	,'ueditor_zh':['ueditor']
		}
	});
	require(["${base}/common/js/main.js?v=1.2.4111"],function(){
		require(["${base}/agent/js/agent.js?=v1.94"]);
	});
	</script>
</head>
<body>
<nav class="navbar navbar-default navbar-fui" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand logo-wrap"><i class="logo"></i>管理后台</span>
    </div>
    <div class="navbar-collapse collapse">
    	<ul class="nav navbar-nav navbar-right" id="show_chat">
		</ul>
    	<ul class="nav navbar-nav">
    		<li><div class="admin-side-toggle"><i class="fa fa-bars"></i></div></li>
    	</ul>
	    <ul class="nav navbar-nav navbar-right">
	    	<!-- 预警用户调整好之后再打开 -->
			<c:if test="${warnOnoff }">
			<li id="warning_li" class="nav-item"><a href="${base}/agent/member/manager/index.do?abnormalFlag=1" id='online-user-tabid' class="open-tab" title="告警用户" data-refresh="true">告警用户 <span class="badge" id="warning_span">0</span></a><i class="bottom-border"></i></li>
			</c:if>
			<li id="online_li" class="nav-item"><a href="${base}/agent/member/manager/online.do" id='online-user-tabid' class="open-tab" title="在线用户" data-refresh="true">在线 <span class="badge" id="online_span">0</span></a><i class="bottom-border"></i></li>
			<li id="deposit_li" class="nav-item"><a href="${base}/agent/finance/payonlinerd/index.do" class="open-tab" title="会员充值记录" data-refresh="true">充值 <span class="badge" id="deposit_span">0</span></a><i class="bottom-border"></i></li>
			<li id="withdraw_li" class="nav-item"><a href="${base}/agent/finance/memdrawrd/index.do" class="open-tab" title="会员提款记录" data-refresh="true">提款 <span class="badge" id="withdraw_span">0</span></a><i class="bottom-border"></i></li>
			<li id="sound_li"class="nav-item">
				<a href="javascript:void(0)" id="home_soundicon" class="glyphicon glyphicon-volume-up" value="up" title="关闭声音"></a>
				<i class="bottom-border"></i>
			</li>
			<li id="sound_li"class="nav-item">
				<div class="nitem-wrap" title="警铃提示周期：单位秒">
					<select id="home_waringCycle" title="警铃提示周期：单位秒" class="form-control">
						<option value="10" selected>10</option>
						<option value="30">30</option>
						<option value="60">60</option>
						<option value="120">120</option>
					</select>
				</div>
				<i class="bottom-border"></i>
			</li>
			<li id="sound_li" class="nav-item">
				<div class="nitem-wrap" title="警铃提示音">
					<select id="home_sourceType" title="警铃提示音" class="form-control">
						<option value="1" selected>铃声</option>
						<option value="2">语音</option>
						<option value="3">苹果</option>
					</select>
				</div>
				<i class="bottom-border"></i>
			</li>
			<li class="dropdown hidden-xs nav-item"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">${USER_SESSION_AGENT.account}&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li><a href="${base}/agent/modifyPwd.do" class="open-dialog">修改密码</a></li>
					<li class="divider"></li>
					<li><a tabindex="-1" href="${base}/agent/logout.do">退出</a></li>
				</ul>
				<i class="bottom-border"></i>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right" id="show_quoto">
		</ul>
    </div>
  </div>
</nav>
<div id="fui_sidebar" class="fui-sidebar">
	<div class="sidebar-scroll navbar-default" id="fui_sidebar_nars"></div>
</div>
<div class="fui-content" id="fui_content_id">
	<div class="fui-tab-wrap" id="fui_tab_wrap_id"></div>
</div>

<div class="fui-footer" id="fui_footer_id">
	<div class="fui-fmain">2017 © <a href="${base}">本公司版权所有</a> 本公司技术支持</div>
	<div id="stopScorll"  style="position: absolute;top:5px;right:5px;cursor: pointer;color:red;font-weight: 600;">停止滚动</div>
</div>

<c:if test="${not empty content_page && content_page!=''}">
<script type="text/html" class="direct_open_url" url="${base}${content_url}"><!--<jsp:include page="${content_page}"></jsp:include>//--></script>
</c:if>

</body></html>
<script src="${base}/common/js/UEditor/ueditor.config.js"></script>
<script src="${base}/common/js/UEditor/ueditor.all.min.js"></script>
<script src="${base}/common/js/UEditor/ueditor.parse.min.js"></script>
<script src="${base}/common/js/UEditor/zh-cn.js"></script>
<style>
.chat-list li a{
	color: #767070;
    font-size: 16px;
    font-weight: bold;
}
</style>
