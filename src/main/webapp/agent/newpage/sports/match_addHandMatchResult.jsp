<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="sport_match_add_hand_match_result_form_id" method="post" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title"></h4>
		</div>
		<div class="modal-body">
			<c:if test="${smr.status!=2 }">
			<input type="hidden" name="id" value="${smr.id }"/>
			<input type="hidden" name="matchId" value="${param.matchId }"/>
			</c:if>
			<table class="table table-bordered table-striped">
				<tr>
					<td colspan="4">
						<div class="checkbox" >
						   	<label><input type="checkbox" name="cutGameChk"> 赛事腰斩</label>
		  				</div>
					</td>
				</tr>
			<c:if test="${param.sportType==1 }">
				<tr>
					<td width="20%" class="text-right media-middle">上半场:</td>
					<td width="35%"><input class="form-control digits" type="text" name="scoreH1H" value="${smr.scoreH1H}"/></td>
					<td width="10%" class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td width="35%"><input class="form-control digits" type="text" name="scoreH1G" value="${smr.scoreH1G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">全场:</td>
					<td><input class="form-control digits" type="text" name="scoreFullH" value="${smr.scoreFullH}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreFullG" value="${smr.scoreFullG}"/></td>
				</tr>
			</c:if>
			<c:if test="${param.sportType==2 }">
				<tr>
					<td width="20%" class="text-right media-middle">第一节:</td>
					<td width="35%"><input class="form-control digits" type="text" name="scoreTime1H" value="${smr.scoreTime1H}"/></td>
					<td width="10%" class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td width="35%"><input class="form-control digits" type="text" name="scoreTime1G" value="${smr.scoreTime1G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">第二节</td>
					<td><input class="form-control digits" type="text" name="scoreTime2H" value="${smr.scoreTime2H}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreTime2G" value="${smr.scoreTime2G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">第三节</td>
					<td><input class="form-control digits" type="text" name="scoreTime3H" value="${smr.scoreTime3H}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreTime3G" value="${smr.scoreTime3G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">第四节</td>
					<td><input class="form-control digits" type="text" name="scoreTime4H" value="${smr.scoreTime4H}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreTime4G" value="${smr.scoreTime4G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">上半场</td>
					<td><input class="form-control digits" type="text" name="scoreH1H" value="${smr.scoreH1H}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreH1G" value="${smr.scoreH1G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">下半场</td>
					<td><input class="form-control digits" type="text" name="scoreH2H" value="${smr.scoreH2H}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreH2G" value="${smr.scoreH2G}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">加时赛</td>
					<td><input class="form-control digits" type="text" name="scoreAddH" value="${smr.scoreAddH}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreAddG" value="${smr.scoreAddG}"/></td>
				</tr>
				<tr>
					<td class="text-right media-middle">全场:</td>
					<td><input class="form-control digits" type="text" name="scoreFullH" value="${smr.scoreFullH}"/></td>
					<td class="text-center media-middle">&nbsp;:&nbsp;</td>
					<td><input class="form-control digits" type="text" name="scoreFullG" value="${smr.scoreFullG}"/></td>
				</tr>
			</c:if>
			</table>
		</div>
		<div class="modal-footer">
			<c:if test="${smr.status!=2 }"><button type="button" class="btn btn-primary save">保存</button>
			<button type="button" class="btn btn-primary saveAndBalance">保存并结算</button></c:if>
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#sport_match_add_hand_match_result_form_id");
	$form.find("[name='cutGameChk']").click(function(){
		if(this.checked){
			$form.find("input[type=text]").prop("disabled","disabled");
		}else{
			$form.find("input[type=text]").removeProp("disabled");
		}
	})<c:if test="${smr.status==2 }">.click().prop("disabled","disabled")</c:if>;
<c:if test="${smr.status!=2 }">
	$form.find(".save").click(function(){
		$form.attr("action","${base}/agent/sports/match/saveHandMatchResult.do");
		$form.submit();
	});
	$form.find(".saveAndBalance").click(function(){
		$form.attr("action","${base}/agent/sports/match/saveHandResultAndBalance.do");
		$form.submit();
	});
	$form.data("paramFn",function(){
		var paramArr=$form.serializeArray()
		var params={};
		$.each( paramArr, function(i, field){
			params[field.name]=field.value;
		});
		return {json:JSON.stringify(params),cutGame: $form.find("[name='cutGameChk']")[0].checked};
	});
</c:if>
});
</script>