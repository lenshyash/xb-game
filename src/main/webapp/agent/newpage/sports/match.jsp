<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="beginDate" placeholder="开始日期" value="${startTime }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<select name="sportType" class="form-control" >
					<option value="0">--所有球类--</option>
					<option value="1">足球</option>
					<option value="2">篮球</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="homeTeam" placeholder="主队名称">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" value="${param.matchId}" name="matchId" placeholder="赛事编号">
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" placeholder="结束日期" value="${endTime }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<select name="resultStatus" class="form-control" >
					<option value="0">--赛果状态--</option>
					<option value="1">无赛果</option>
					<option value="2">系统赛果</option>
					<option value="3">手结赛果</option>
				</select>
			</div>
			<div class="input-group">
				<select name="billingStatus" class="form-control" >
					<option value="0">--结算状态--</option>
					<option value="1">投注未结算</option>
					<option value="6">半场投注未结算</option>
					<option value="2">所有未结算</option>
					<option value="3">系统结算</option>
					<option value="4">非系统结算</option>
					<option value="5">系统半场结算</option>
				</select>
			</div>
<%--			<div class="input-group">--%>
<%--				<select name="typeNames" class="form-control" >--%>
<%--					<option value="">--投注赛事筛选--</option>--%>
<%--					<option value="上半">上半</option>--%>
<%--				</select>--%>
<%--			</div>--%>
			<div class="input-group">
				<input type="text" class="form-control" name="guestTeam" placeholder="客队名称">
			</div>
			<button class="btn btn-success reset" type="button" style="margin-left:173px;">重置</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div id="remark_div" style="padding: 10px;">
	<span class="text-primary">温馨提示：</span><span class="text-danger">手动结算优先级别:手动输入赛果 > 系统赛果 > 比分网赛果</span>
</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/sports/match/listNew.do',
		columns : [{
			title : '类型',
			align : 'center',
			width : '80',
			valign : 'middle',
			field : 'sportTypeId',
			formatter:function(value,row, index){
				if(value == 1){
					return "足球";
				}else if(value == 2){
					return "篮球";
				}
				return "其他";
			}
		},{
			title : '联赛名称',
			align : 'center',
			width : '200',
			field : 'league',
			valign : 'middle'
		},{
			title : '主队',
			align : 'center',
			width : '200',
			field : 'hTeam',
			valign : 'middle'
		},{
			title : '客队',
			align : 'center',
			width : '200',
			field : 'gTeam',
			valign : 'middle'
		},{
			sortable:true,
			title : '开赛时间',
			align : 'center',
			width : '160',
			field : 'startTime',
			valign : 'middle',
			formatter:function(value,row, index){
				return Fui.formatDatetime(value + 60 * 60 * 12 * 1000);
			}
		},{
			sortable:true,
			title : '结算状态',
			align : 'center',
			width : '160',
			field : 'startTime',
			valign : 'middle',
			formatter:function(value,row, index){
				var text = '';
				if(row.handResultId!=null){
					text = '<font color="green">手动结算</font>';
				}else if(row.matchResultGid!=null){
					if(row.balance==1){
						text = '<font color="blue">未结算</font>';
					}else if(row.balance == 2){
						text = '<font color="green">系统结算</font>';
					}else if(row.balance == 3){
						text = '<font color="yellow">结算中</font>';
					}else if(row.balance == 4){
						text = '<font color="red">结算出错</font>';
					}else if(row.balance == 5){
						text = '<font color="orange">系统半场结算</font>';
					}
				}else if(row.bfwResultId!=null){
					text = '<font color="orange">比分网</font>';
				}else{
					text = '<font color="blue">未结算</font>';
				}
				return text;
			}
		},{
			title : '比分网赛果',
			align : 'center',
			width : '100',
			field : 'bfwResultId',
			valign : 'middle',
			formatter:function(value,row, index){
				if(value){
					return "<a class='open-dialog' href='${base}/agent/sports/match/showBfwMatchResult.do?bfwResultId="+value+"&matchId="+row.id+"&sportType="+row.sportTypeId+"'dialog-title='"+row.hTeam + "&nbsp;vs.&nbsp;" + row.gTeam+"'>查看</a>";
				}
				return "<font style = 'color:gray;'>无赛果</font>"
			}
		},{
			title : '系统赛果',
			align : 'center',
			width : '100',
			field : 'matchResultGid',
			valign : 'middle',
			formatter:function(value,row, index){
				if(value){
					return "<a class='open-dialog' href='${base}/agent/sports/match/showSystemMatchResult.do?gid="+value+"'>查看</a>";
				}
				return "<font style = 'color:gray;'>无赛果</font>"
			}
		},{
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : function(value, row, index) {
				var css = row.handResultId ? "":'style = "color:gray;"'
					title=row.hTeam + "&nbsp;vs.&nbsp;" + row.gTeam;
				return[
				 '<a class="open-dialog" ',css,' href="${base}/agent/sports/match/showHandMatchResult.do?matchId=',row.id,'&sportType=',row.sportTypeId,
						 '" dialog-title="',title,'">赛果</a>',
				 '&nbsp;|&nbsp;',  
				 '<a class="todo-ajax" href="${base}/agent/sports/match/balanceMatch.do?matchId=',row.id,'" title="是否确认要结算赛事“',title,'”?">结算</a>',
				 '&nbsp;|&nbsp;', 
				 '<a class="todo-ajax" style="color:red;" href="${base}/agent/sports/match/rollbackMatch.do?matchId=',row.id,'" title="是否确认要回滚赛事“',title,'”的所有结算注单?">回滚</a>'
				].join('');
			}
		}]
	});
});
</script>