<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title"></h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
			<c:if test="${param.sportType==1 }">
				<tr>
					<td width="30%" class="text-right">上半场:</td>
					<td width="70%"><c:choose><c:when test="${not empty smr.scoreH1H && smr.scoreH1H.matches('[0-9]+')}">${smr.scoreH1H} : ${smr.scoreH1G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">全场</td>
					<td><c:choose><c:when test="${not empty smr.scoreFullH && smr.scoreFullH.matches('[0-9]+')}">${smr.scoreFullH} : ${smr.scoreFullG}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
			</c:if>
			<c:if test="${param.sportType==2 }">
				<tr>
					<td width="30%" class="text-right">第一节:</td>
					<td width="70%"><c:choose><c:when test="${not empty smr.scoreTime1H && smr.scoreTime1H.matches('[0-9]+')}">${smr.scoreTime1H} : ${smr.scoreTime1G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">第二节</td>
					<td><c:choose><c:when test="${not empty smr.scoreTime2H && smr.scoreTime2H.matches('[0-9]+')}">${smr.scoreTime2H} : ${smr.scoreTime2G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">第三节</td>
					<td><c:choose><c:when test="${not empty smr.scoreTime3H && smr.scoreTime3H.matches('[0-9]+')}">${smr.scoreTime3H} : ${smr.scoreTime3G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">第四节</td>
					<td><c:choose><c:when test="${not empty smr.scoreTime4H && smr.scoreTime4H.matches('[0-9]+')}">${smr.scoreTime4H} : ${smr.scoreTime4G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">上半场</td>
					<td><c:choose><c:when test="${not empty smr.scoreH1H && smr.scoreH1H.matches('[0-9]+')}">${smr.scoreH1H} : ${smr.scoreH1G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">下半场</td>
					<td><c:choose><c:when test="${not empty smr.scoreH2H && smr.scoreH2H.matches('[0-9]+')}">${smr.scoreH2H} : ${smr.scoreH2G}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">加时赛</td>
					<td><c:choose><c:when test="${not empty smr.scoreAddH && smr.scoreAddH.matches('[0-9]+')}">${smr.scoreAddH} : ${smr.scoreAddG}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
				<tr>
					<td class="text-right">全场</td>
					<td><c:choose><c:when test="${not empty smr.scoreFullH && smr.scoreFullH.matches('[0-9]+')}">${smr.scoreFullH} : ${smr.scoreFullG}</c:when><c:otherwise>&nbsp;&nbsp;&nbsp;&nbsp; <font color='gray'>-</font> </c:otherwise></c:choose></td>
				</tr>
			</c:if>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>