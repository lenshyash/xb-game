<%@ page language="java" pageEncoding="utf-8"%>
<form class="fui-search table-tool" method="post" id="sports_records_form_id1">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="beginDate" placeholder="开始日期" value="${startTime }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<select name="dateType" class="form-control" >
					<option value="1" >结算时间</option>
					<option value="2" selected>投注时间</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" name="bettingCode" class="form-control" placeholder="注单号"></span>
			</div>
			<div class="input-group">
				<select name="sportType" class="form-control" >
					<option value="0"selected>--所有球类--</option>
					<option value="1">足球</option>
					<option value="2">篮球</option>
				</select>
			</div>
			
			<div class="input-group">
				<input type="text" class="form-control" name="homeTeam" placeholder="主队名称">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="agentName" value="${agentName}" placeholder="代理账号">
			</div>
			<button class="btn btn-primary recordRecord">查询</button>
			<div class="form-group CastAutoRecord">
				<label>
					<input  type="checkbox">
				</label>
				<label for="name">自动刷新</label>
				<select class="form-control">
					<option>5</option>
					<option>10</option>
					<option>20</option>
					<option>30</option>
					<option>60</option>
					<option>120</option>
				</select>
			</div>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" placeholder="结束日期" value="${endTime }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="memberAccount" value="${account}" placeholder="会员账号">
			</div>
			<div class="input-group">
				<select name="balanceStatus" class="form-control" >
					<option value="0" selected>--结算状态--</option>
					<option value="1">未结算</option>
					<option value="2">已结算</option>
					<option value="3">赛事腰斩</option>
				</select>
			</div>
			<div class="input-group">
				<select name="resultStatus" class="form-control" >
					<option value="0" selected>--输赢状态--</option>
					<option value="1">全输</option>
					<option value="2">输一半</option>
					<option value="3">平</option>
					<option value="4">赢一半</option>
					<option value="5">全赢</option>
				</select>
			</div>
			<div class="input-group">
				<select name="bettingStatus" class="form-control" >
					<option value="0" selected>--提交状态--</option>
					<option value="1">待确认</option>
					<option value="2">已确认</option>
					<option value="3">系统取消</option>
					<option value="4">手动取消</option>
				</select>
			</div>
			<div class="input-group">
				<select name="typeNames" class="form-control" >
					<option value="" selected>--投注赛事类型--</option>
					<option value="上半">上半</option>
					<option value="全场">全场</option>
					<option value="综合过关">混合过关</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="guestTeam" placeholder="客队名称">
			</div>
			<button class="btn btn-success reset" type="button">重置</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
	$(function(){
		var  times=''
		var  time =''
		$('.CastAutoRecord input').change(function(){
			if ($('.CastAutoRecord input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.CastAutoRecord option:selected').text()
				autoRefreshs(time,true)
			}else{
				autoRefreshs(time,false)
			}
		})

		$('.CastAutoRecord select').change(function(){
			if ($('.CastAutoRecord input').is(":checked")){
				// debugger
				autoRefreshs(time,false)
				time =$('.CastAutoRecord option:selected').text()
				autoRefreshs(time,true)
			}else {
				autoRefreshs(time,false)
			}
		})

		function autoRefreshs(time,statu){
			if (statu == true){
				times = setInterval(function(){
					$('.btn.btn-primary.recordRecord').trigger('click')
				},time *1000 )
			}else {
				clearInterval(times)
			}

		}
	})

requirejs(['jquery','bootstrap','Fui'],function(){
	var $form=$("#sports_records_form_id1");
	Fui.addBootstrapTable({
		url : '${base}/agent/sports/record/listNew.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [ {
			title : '赛事编号/单号/投注日期',
			align : 'center',
			width : '200',
			valign : 'middle',
			sortable:true,
			field : 'id',
			formatter:function(value,row, index){
				var bd = new Date(row.bettingDate);
				var arr = [];
				if(row.matchId){
					arr.push('<a href="${base}/agent/sports/match/index.do?matchId='+row.matchId+'" class="open-tab" title="赛事结果" data-refresh="true">' +row.matchId + '</a>');
				}
				arr.push(row.bettingCode);
				arr.push(bd.format("MM月dd日,hh:mm:ss"));
				return arr.join("<br>");
			}
		}, {
			field:'memberName',
			title : '会员',
			align : 'center',
			width : '100',
			valign : 'bottom'
		},{
			title : '投注类型',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter:function(value,row, index){
				var st = row.sportType
				var html = "";
				if(st == 1){
					html += "足球";
				}else if(st == 2){
					html += "篮球";
				}
			
				if(row.gameTimeType == 1){
					html += " - 滚球";
				}else if(row.gameTimeType == 2){
					html += " - 今日";
				}else if(row.gameTimeType == 3){
					html += " - 早盘";
				}
				
				html += "<br/>";
				if(row.mix == 2){
					return html + "混合过关";
				}
				var ts = row.typeNames;
				if(!ts){
					return html;
				}
				return html + ts.replace("-","<br>");
			}
		}, {
			title : '赛事ID/投注项',
			align : 'center',
			width : '300',
			valign : 'middle',
			field : 'remark',
			formatter:function(value,row, index){
				if(row.mix != 2){
					return toBetHtml($.parseJSON(value),row);
				}
				var html = getOpenUrl(row);
				var arr = $.parseJSON(value)
				for(var i=0;i<arr.length;i++){
					if(i != 0){
						html += "<div style='border-bottom:1px #303030 dotted;'></div>";
					}
					html += toBetHtml(arr[i],row);
				}
				return html;
			},
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		}, {
			field : 'bettingMoney',
			title : '投注额',
			align : 'center',
			width : '80',
			valign : 'middle',
			formatter:function(value,row, index){
				return "<span class='text-danger'>"+value+"</span>";
			},
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'bettingMoney');;
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.totalBetMoney ? aggsData.totalBetMoney.toFixed(2) : "0.00";
			}
		}, {
			title : '提交状态',
			align : 'center',
			field : 'bettingStatus',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				if(value == 1){
					return "<font color='blue'>待确认</font><br><a class='todo-ajax' href='${base}/agent/sports/record/doCheckOrder.do?orderId="+row.id+"' title='确定要确认订单："+row.bettingCode+"?'>系统确认</a> <a class='todo-ajax' href='${base}/agent/sports/record/doCheckOrder.do?handlerType=2&orderId="+row.id+"' title='确定要确认订单："+row.bettingCode+"?'>手动确认</a>";
				}
				if(value == 2){
					return "已确认";
				}
				if(value == 3){
					return "<font color='red'>系统取消</font>";
				}
				if(value == 4){
					return "<font color='red'>手动取消</font>";
				}
			}
		},{
			title : '结算状态',
			align : 'center',
			field : 'balance',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				if(value == 1){
					return "<font color='blue'>未结算</font>";
				}
				if(value == 2){
					return "<font color='green'>已结算</font>";
				}
				if(value == 3){
					return "<font color='red'>结算失败</font>";
				}
				
				if(value == 4){
					return "<font color='red'>比赛腰斩</font>";
				}
				
				if(value == 5 ){
					return "<font color='green'>手动结算</font>";
				}
				if(value == 6){
					return "<font color='green'>比分网结算</font>";
				}
			}
		},{
			title : '派彩金额',
			align : 'center',
			field : 'bettingResult',
			width : '100',
			valign : 'middle',
			sortable:true,
			formatter:function(value,row, index){
				if(!value && value != 0){
					return value;
				}
				if(value > 0){
					return "<span class='text-danger'>"+value+"</span>";
				}
				return "<span class='text-primary'>"+value+"</span>";
			},
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'bettingResult');
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.totalBetResult ? aggsData.totalBetResult.toFixed(2) : "0.00";
			}
		},{
			title : '亏损金额',
			align : 'center',
			field : 'bettingLoss',
			width : '100',
			valign : 'middle',
			sortable:true,
			formatter:lossMoneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var bettingMoney=0,bettingResult=0,row;
				for(var i=rows.length-1;i>=0;i--){
					row=rows[i];
					if(row.bettingMoney != null){
						bettingMoney = bettingMoney+row.bettingMoney;
					}
					if(row.bettingResult!=null){
						bettingResult=bettingResult+row.bettingResult;
					}
				}
				
				var lossSum=bettingMoney-bettingResult;
				return lossSum.toFixed(2);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0"
				}
				return aggsData.totalBetMoney-aggsData.totalBetResult ? (aggsData.totalBetMoney-aggsData.totalBetResult).toFixed(2) : (aggsData.totalBetMoney).toFixed(2);
			}
		},{
			title : '操作',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter:function(value,row, index){
				if(row.balance == 1 && (row.bettingStatus == 1 || row.bettingStatus == 2 )){
					return  '<a class="open-dialog" href="${base}/agent/sports/record/showCancelOrder.do?orderId='+row.id+'">取消注单</a>';
				}
				return "-";
			}
		}]
	});
	
	function lossMoneyFormatter(value,row,index){
		var buy=row.bettingMoney;
		var win=row.bettingResult;
		
		if (isNaN(win)) { 
			return "未开奖";
		}
		var loss=buy-win;
		return loss.toFixed(2);
	}
	
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(2)+"";
	}
	
	function getOpenUrl(row){
		if(!row.gid || 'undefined' == row.gid){
			return "";
		}
		
// 		var openUrl = "https://www.99814.com/changes.aspx?id=";
		var openUrl = "https://5.99814.com/TPLWEB/gunqiu/game_more.aspx?pcaction=result&mid="+row.gid;
		if(row.sportType == 2){
			//openUrl = "https://www.99814.com/game/Basket/changes.aspx?id=";
			openUrl = "https://www.99814.com/Basketball/changes/"+row.gid+".html";
		}
		return "<a href="+openUrl+" target=\"_blank\">"+row.gid+"</a><br>";
	}
	
	function toBetHtml(item,row){
		var con = item.con;
		if(con.indexOf("vs") == -1){
			con = '<span class="text-danger">'+ con +'</span>';
		}
		var homeFirst = !(item.homeStrong === false);//主队是否在前
		var scoreStr = "";
		
		if(row.gameTimeType == 1){
			if(homeFirst){
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH +":" + row.scoreC + ")</b></font>";
			}else{
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC +":" + row.scoreH + ")</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if(item.half === true && row.mix == 2){
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}
		
		var html = "";
		if(row.mix != 2){
			html +=getOpenUrl(row);
		}
		html += item.league +"<br/>" + 
					home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
					"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
		var balance = row.mix != 2 ? row.balance : item.balance;	
		var bt = row.bettingStatus;
		if(balance == 4){
			html = "<s style='color:red;'>" + html+"(赛事腰斩)</s>"
		}else if(bt == 3 || bt == 4){
			html = "<s style='color:red;'>" + html+"("+row.statusRemark+")</s>"
		}else if(balance == 2 || balance == 5 || balance == 6){
			var mr = row.mix != 2 ? row.result:item.matchResult;
			if(homeFirst){
				html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
			}else{
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>("+ss[1]+":"+ss[0]+")</font>";
			}
		}
		if(item.startTime){
			var time = item.startTime + 12 * 60 * 60 * 1000;
			html = "<div style='cursor:pointer;' title='开赛时间(北京):"+Fui.formatDatetime(time)+"'>"+html+"</div>";
		}
		return html;
	}
});
</script>
