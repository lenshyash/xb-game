<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/sports/record/cancelOrder.do" class="form-submit" id="sports_record_cancel_order_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">取消注单</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tr>
					<td width="20%" class="text-right media-middle">类型:</td>
					<td width="80%">
						<select name="remark" class="form-control">
							<option value="">----请选择----</option>
							<option value="比分变动">比分变动</option>
							<option value="赛事腰斩">赛事腰斩</option>
							<option value="赔率异常">赔率异常</option>
							<option value="非法注单">非法注单</option>
							<option value="-1">其他</option>
						</select>
					</td>
				</tr>
				
				<tr class="hidden reason-wrap">
					<td class="text-right media-middle">原因:</td>
					<td>
						<textarea class="form-control" name="reason"></textarea>
					</td>
				</tr>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#sports_record_cancel_order_form_id");
	$form.find("[name='remark']").change(function(){
		var v=$(this).val();
		if(v==-1){
			$form.find(".reason-wrap").removeClass("hidden");
		}else{
			$form.find(".reason-wrap").addClass("hidden");
		}
	});
	$form.data("paramFn",function(){
		var msg = $form.find("[name='remark']").val();
		if(!msg){
			layer.msg("请选择类型");
			return false;
		}
		if(msg == -1){
			msg = $form.find("[name='reason']").val();
			if(!msg){
				layer.msg("请输入原因");
				return false;
			}
		}
		return {orderId:${param.orderId },statusRemark:msg};
	});
});
</script>