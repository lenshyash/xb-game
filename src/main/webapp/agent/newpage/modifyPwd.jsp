<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/system/user/updloginpwd.do" class="form-submit" unReloadTable="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改密码</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped" style="clear: both">
				<tbody>
					<tr>
						<td width="20%" class="text-right">用户名：</td>
						<td width="80%" class="text-left"><input type="text" class="form-control" value="${username}"disabled="disabled" /></td>
					</tr>
					<tr>
						<td class="text-right">旧密码：</td>
						<td class="text-left"><input type="password" class="form-control" name="opwd" /></td>
					</tr>
					<tr>
						<td class="text-right">新密码：</td>
						<td class="text-left"><input type="password" class="form-control" name="pwd" /></td>
					</tr>
					<tr>
						<td class="text-right">确认密码：</td>
						<td class="text-left"><input type="password" class="form-control" name="rpwd" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button type="submit" class="btn btn-primary">保存</button>
		</div>
		
	</div>
</div>
</form>
