<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="agent_manager_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加群组</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<caption class="text-center">
						<font style="color: red;">请注意，群组名称添加后不能修改</font> 
					</caption>
					<tr>
						<td class="text-right media-middle">群组名称：</td>
						<td><input type="text" class="form-control" name="groupName" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">备注：</td>
						<td><input type="text" class="form-control" name="remark" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#agent_manager_add_form_id");
	
	$form.submit(function(){
		$.ajax({
			url : "${base}/agent/agent/manager/checkBankCard.do",
			data : {cardNo :$form.find("[name='cardNo']").val()},
			success : function(result) {
				if(result){
					layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
						btn : [ '继续','取消' ]
					}, function() {
						confirmCommit();
					});
				}else{
					confirmCommit();
				}
			}
		});
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/agent/qzGroup/save.do",
			data : $form.serialize(),
			success : function(result) {
				console.log(result);
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
});
</script>