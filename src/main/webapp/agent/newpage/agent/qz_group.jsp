<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="agent_manager_form_id2">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="keyName">
					<option value="groupName">群组名称</option>
				</select>
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="keyword" placeholder="关键字">
			</div>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/agent/qzGroup/add.do">新增群</button>
			<button class="btn btn-primary back-btn hidden" type="button">返回</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var history=[],curParents=null
		,$form=$("#agent_manager_form_id2");
	
	<c:if test="${not empty param.parents}">curParents="${param.parents}";history.push(curParents);</c:if>
	$form.find(".back-btn").click(function(){
		var last = history.pop();
		if (!last) {
			layer.msg("当前已经是最顶级了");
			curParents=null;
			return false;
		}
		if(curParents==last){
			curParents=history[history.length-1];
		}else{
			curParents = last;
		}
		var $table=$form.parents(".fui-box:first").data("bootstrapTable");
		if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/agent/qzGroup/list.do',
		queryParams : function(params){
			var value = $form.find("[name='keyword']").val();
			if(value){
				params[$form.find("[name='keyName']").val()] = value;
				curParents = null;
			}else{
				if(curParents){
					params["parents"] = curParents;
					$form.find(".back-btn").removeClass("hidden");
				}
			}
			if(!curParents){
				$form.find(".back-btn").addClass("hidden");
				history=[];
			}
			return params
		},
		onLoadSuccess:onLoadSuccess,
		columns : [ {
			field : 'id',
			title : '群组id',
			align : 'center',
			valign : 'middle'
		},
		{
			field : 'name',
			title : '群组姓名',
			align : 'center',
			valign : 'middle'
		}
		,{
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		},{
			title : '查看',
			align : 'center',
			valign : 'middle',
			width:'250',
			formatter : fastViewFormatter
		},{
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle'
		}]
	});
	function onLoadSuccess($table){
		$table.find(".dodownup").click(function(){
			var $table=$form.parents(".fui-box:first").data("bootstrapTable");
			if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
			return false;
		});
	}
	
	function fastViewFormatter(value, row, index) {
		return '<a class="open-tab" href="${base}/agent/agent/manager/index.do?groupName='+row.name+'" title="群下成员">群下成员</a> &nbsp;'
		+'<a class="open-tab" href="${base}/agent/report/group/index.do?groupName='+row.name+'&navName=cunqu" title="群组报表">群组报表</a> &nbsp;'
		+'<a class="open-tab" href="${base}/agent/gameRecord/index.do?groupName='+row.name+'&navName=cunqu" title="群组订单">群组订单</a> &nbsp;'
		;
	}
	function operateFormatter(value, row, index) {
		var s = '<a class="open-dialog" href="${base}/agent/agent/qzGroup/setAccountGroup.do?groupName='+row.name+'" title="">归并客户到群</a>  '
		+'<a class="open-dialog" href="${base}/agent/agent/qzGroup/setCloseGroup.do?groupName='+row.name+'" title="">解散群组</a>  '
				;
		if(row.accountStatus == 3){
			var r= '<a class="todo-ajax" href="${base}/agent/agent/manager/updStatus.do?id='+row.id+'&accountStatus=2" data-url="${base}/agent/agent/manager/updStatus.do?id='+row.id+'&accountStatus=1&remark=noExamine" title="代理审核">审核</a>  ' + s;
		}else if(row.accountStatus == 1 && row.remark == 'noExamine'){
			var r= '';
		}else{
			var r= s;
		}
		return r;
	}
});
</script>