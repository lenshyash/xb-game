<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/agent/manager/moveAgent.do" method="post"class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="setGeneralLabel">批量移动代理</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">上级代理：</td>
						<td><input type="text" class="form-control" name="agentName" value="" placeholder="仅能填写一个"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">子代理：</td>
						<td><textarea cols="120" rows="15" class="form-control" name="agents" placeholder="可以同时多个,空格、逗号隔开"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default  fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>