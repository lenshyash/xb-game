<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">查看代理详细信息</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<input type="hidden" id="id" value="${map.id }" />
				<tbody>
					<tr>
						<th width="18%" class="text-right">登录账号：</th>
						<td width="20%">${map.account }</td>
						<th width="18%" class="text-right">所属上级：</th>
						<td>${parentNames }</td>
					</tr>
					<tr>
						<th class="text-right">账号姓名：</th>
						<td>${map.userName }</td>
						<th class="text-right">类别：</th>
						<td><c:if test="${map.accountType==5 }">总代理</c:if><c:if test="${map.accountType==4 }">代理</c:if></td>
					</tr>
					<tr>
						<th class="text-right">状态：</th>
						<td><c:if test="${map.accountStatus==2 }">启用</c:if><c:if test="${map.accountStatus!=2 }">禁用</c:if></td>
						<th class="text-right">会员等级：</th>
						<td id="bankName"><c:forEach items="${levels }" var="l"><c:if test="${l.id==map.levelGroup }">${l.levelName }</c:if></c:forEach></td>
					</tr>
					<tr >
						<th class="text-right">账号余额：</th>
						<td><fmt:formatNumber pattern="#,##0.00" value="${map.money }"/></td>
						<th class="text-right">余额宝余额：</th>
						<td>${map.balanceGemMoney}</td>
					</tr>
					<tr>
						<th class="text-right">打码量：</th>
						<td>${map.betNum}</td>
						<th class="text-right">出款所需打码量：</th>
						<td>${map.drawNeed }</td>
					</tr>
					<tr class="advanced_tr">
						<td class="text-center" colspan="4"><a href="#"><div style="float: top;">更多内容</div><div style="float: bottom;"><span id="advanced_span" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div></a>
						</td> 
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">电话：</th>
						<td id="phone">${map.phone }</td>
						<th class="text-right">微信号：</th>
						<td id="wechat">${map.wechat }</td>
					</tr>
					<tr class="advanced hidden">
						<th class="text-right">QQ：</th>
						<td id="qq">${map.qq }</td>
						<th class="text-right">邮箱：</th>
						<td id="email">${map.email }</td>
					</tr>
					<tr >
						<th class="text-right">银行账号：</th>
						<td id="cardNo" colspan="3">${map.cardNo }
						<c:if test="${map.cardNo ne '' }">
							<c:if test="${map.cardNoStatus == 1 }">
								<span class="label label-success" id="cardNoStatus_span">可用</span>
							</c:if>
							<c:if test="${map.cardNoStatus == 2 }">
								<span class="label label-danger" id="cardNoStatus_span">禁用</span>
							</c:if>
						</c:if></td>
					</tr>
					<tr >
						<th class="text-right">取现银行：</th>
						<td id="bankName">${map.bankName }</td>
						<th class="text-right">银行地址：</th>
						<td id="bankAddress">${map.bankAddress }</td>
					</tr>
					<tr>
						<th class="text-right">银行卡所在省：</th>
						<td id="qq">${map.province }</td>
						<th class="text-right">银行卡所在市：</th>
						<td id="email">${map.city }</td>
					</tr>
					<tr>
						<td class="text-right">注册IP：</td>
						<td>${map.registerIp }</td>
						<td class="text-right">注册来源：</td>
						<td>${map.registerUrl }</td>
					</tr>
					<tr>
						<td class="text-right">注册系统：</td>
						<td<c:if test="${multiAgent1.multiAgent== 'off'}"> colspan="3"</c:if>>${map.registerOs }</td>
						<c:if test="${multiAgent1.multiAgent== 'on' && multiAgent.mergeRebate ne 'on'}">
						<td class="text-right">返点数：</td>
						<td>${multiAgent1.curRebateNum }</td>
						</c:if>
					</tr>
					<c:if test="${multiAgent1.profitShare== 'on'}">
					<tr>
						<td class="text-right">占成数：</td>
						<td colspan="3">${multiAgent1.curProfitShare }</td>
					</tr>
					</c:if>
										<tr>
						<th class="text-right">备注：</th>
						<td colspan="3">${map.remark }</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
		</div>
	</div>
</div>
<script  type="text/javascript">
	$(".advanced_tr").click(function(){
		$.ajax({
			url : "${base}/agent/agent/manager/advanced.do",
			data : {id:$("#id").val()},
			success : function(result) {
				$(".advanced_tr").addClass("hidden");
				$(".advanced").removeClass("hidden");
				$("#phone").html(result.phone);
				$("#wechat").html(result.wechat);
				$("#email").html(result.email);
				$("#qq").html(result.qq);
			}
		});
	});
</script>