<%@ page language="java" pageEncoding="UTF-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/agent/proxyRebateTactics/add.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<div style="color: red;">备注：彩票佣金是根据流水计算。电子、真人、体育佣金是根据盈利计算</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/agent/proxyRebateTactics/list.do',
		columns : [ {
			field : 'effectMember',
			title : '会员数量',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : hyslFormatter
		}, {
			field : 'profitMin',
			title : '盈利金额',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : yljeFormatter
		}, {
			field : 'sportsGame',
			title : '体育佣金比例',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'realGame',
			title : '真人佣金比例',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'videoGame',
			title : '电子游戏佣金比例',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'lotteryGame',
			title : '彩票佣金比例',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '65',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '60',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/agent/proxyRebateTactics/closeOrOpen.do?id="+row.id+"&status="});
	}
	function hyslFormatter(value, row, index){
		return '最低有效会员 '+value+ ' 人';	
	}
	function yljeFormatter(value, row, index){
		return value+ ' 元以上';	
	}
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/agent/proxyRebateTactics/modify.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="${base}/agent/agent/proxyRebateTactics/delete.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
});
</script>