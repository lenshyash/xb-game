<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="member_rollback_third_modify_form_id">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改真人电子反水</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">类型：</td>
						<td><select name="type" class="form-control">
								<option value="1"<c:if test="${rollback.type==1}">selected</c:if>>真人</option>
								<option value="2"<c:if test="${rollback.type==2}">selected</c:if>>电子</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">返水比例(%)：</td>
						<td><input name="rate" class="form-control required money" type="text" value="${rollback.rate}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">有效投注(大于等于)：</td>
						<td><input name="qualified" class="form-control required money" type="text" value="${rollback.qualified}"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最高返水上限：</td>
						<td><input name="upperLimit" class="form-control required money" type="text" value="${rollback.upperLimit}"/></td>
					</tr>
					<tr><td class="text-right media-middle">打码量倍数：</td>
						<td><input name="multiple" class="form-control" type="text" value="${rollback.multiple}"/></td> 
					</tr><tr>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
								<option value="2"<c:if test="${rollback.status==1}">selected</c:if>>启用</option>
								<option value="1"<c:if test="${rollback.status==1}">selected</c:if>>停用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">描述：</td>
						<td><textarea name="memo" class="form-control">${rollback.memo}</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#member_rollback_third_modify_form_id");
	$form.submit(function(){
		var rate =$form.find("[name='rate']").val()
			,qualified = $form.find("[name='qualified']").val()
			,upperLimit = $form.find("[name='upperLimit']").val();
		if (rate == "") {
			layer.msg('反水比例不能为空!');
			return false;
		}
		if (qualified == "") {
			layer.msg('有效投注不能为空!');
			return false;
		}
		if (upperLimit == "") {
			layer.msg('返水上限不能为空!');
			return false;
		}
		var data={"rate":rate
				,"id":${rollback.id}
				,"qualified":qualified
				,"upperLimit":upperLimit
				,"type": $form.find("[name='type']").val()
				,"multiple": $form.find("[name='multiple']").val()
				,"status": $form.find("[name='status']").val()
				,"memo":$form.find("[name='memo']").val()};
		$.ajax({
			url : "${base}/agent/agent/memberRollBack/saveStrategy.do",
			data : {data : JSON.stringify(data)},
			success : function(result) {
				layer.closeAll();
				if(result.success){
					layer.msg("保存成功！");
					var $table=$(".fui-box.active").data("bootstrapTable");
					if($table && $table.length){
						$table.bootstrapTable('refresh');
					}
				}else{
					layer.msg(result.msg||"保存失败！");
				}
			}
		});
		return false;
	});
});
</script>