<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="agent_manager_add_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加代理</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<input type="hidden" name="modifyType" value="1">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">登录账号：</td>
						<td width="30%"><input type="text" class="form-control" name="account" /></td>
						<td width="20%" class="text-right media-middle">账号状态：</td>
						<td width="30%"><select name="accountStatus" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">禁用</option>
						</select></td>
					</tr>
					<c:if test="${multiAgent.multiAgent== 'off'}">
					<tr>
						<td class="text-right media-middle">账号类型：</td>
						<td colspan="3"><select name="accountType" class="form-control">
								<option value="4" selected>代理</option>
								<option value="5">总代理</option>
						</select></td>
					</tr></c:if>
					<tr>
						<td class="text-right media-middle">密码：</td>
						<td><input type="password" class="form-control" name="pwd" /></td>
						<td class="text-right media-middle">确认密码：</td>
						<td><input type="password" class="form-control" name="rpwd" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">代理姓名：</td>
						<td><input type="text" class="form-control" name="userName" /></td>
						<td class="text-right media-middle">电话：</td>
						<td><input type="text" class="form-control phone" name="phone" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">邮箱：</td>
						<td><input type="text" class="form-control email" name="email" /></td>
						<td class="text-right media-middle">QQ：</td>
						<td><input type="text" class="form-control" name="qq" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">取现银行：</td>
						<td><input name="bankName" class="form-control" type="text" placeholder="请输入银行名称"></td>
						<td class="text-right media-middle">银行账号：</td>
						<td><input type="text" class="form-control" name="cardNo" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">银行地址：</td>
						<td colspan="3"><input type="text" class="form-control" name="bankAddress" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">代理等级：</td>
						<td ><select ame="levelGroup" class="form-control">
						<c:forEach items="${levels }" var="l"><option value="${l.id }">${l.levelName }</option></c:forEach>
						</select></td>
						<td class="text-right media-middle">报表类型：</td>
						<td class="text-left"><select name="reportType" class="form-control">
							<option value="1">普通</option>
							<option value="2">引导</option>
						</select></td>
					</tr>
					<c:if test="${multiAgent.multiAgent== 'on' && multiAgent.mergeRebate ne 'on'}"><tr>
						<td class="text-right media-middle">返点数：</td>
						<td><input type="text" class="form-control" name="rebateNum" /></td>
						<td class="media-middle" colspan="2">返点数必须小于等于：${multiAgent.rebateMaxNum }</td>
					</tr></c:if>
					<c:if test="${multiAgent.profitShare== 'on'}"><tr>
						<td class="text-right media-middle">占成数：</td>
						<td><input type="text" class="form-control" name="profitShare" /></td>
						<td class="media-middle" colspan="2">占成数必须小于等于：${multiAgent.profitShareMax }</td>
					</tr></c:if>
					<c:if test="${multiAgent.dynamicRate== 'on'}"><tr>
						<td class="text-right media-middle">赔率值：</td>
						<td><input type="text" class="form-control" name="dynamicRate" /></td>
						<td class="media-middle" colspan="2">赔率值必须小于等于：${multiAgent.dynamicRateMax }</td>
					</tr></c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#agent_manager_add_form_id");
	$form.submit(function(){
		var password = $form.find("[name='pwd']").val()
			,rpassword = $form.find("[name='rpwd']").val();
		if (!password) {
			layer.msg("密码不能为空！");
			return false;
		}
		if (!rpassword) {
			layer.msg("确认密码不能为空！");
			return false;
		}
		if (password !== rpassword) {
			layer.msg("两次密码不一致！");
			return false;
		}
		$.ajax({
			url : "${base}/agent/agent/manager/checkBankCard.do",
			data : {cardNo :$form.find("[name='cardNo']").val()},
			success : function(result) {
				if(result){
					layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
						btn : [ '继续','取消' ]
					}, function() {
						confirmCommit();
					});
				}else{
					confirmCommit();
				}
			}
		});
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/agent/manager/save.do",
			data : $form.serialize(),
			success : function(result) {
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
});
</script>