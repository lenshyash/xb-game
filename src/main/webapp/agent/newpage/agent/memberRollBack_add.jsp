<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/agent/memberRollBack/addSave.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加会员反水</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">彩票返水比例(%)：</td>
						<td width="30%"><input name="lotteryGame" class="form-control required money" type="text"/></td>
						<td width="20%" class="text-right media-middle">体育返水比例(%)：</td>
						<td width="30%"><input name="sportsGame" class="form-control required money" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">有效投注：</td>
						<td><input name="profitBet" class="form-control required money" type="text"/></td>
						<td class="text-right media-middle">打码量倍数：</td>
						<td><input name="multiple" class="form-control" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">六合彩特码B返水比例(%)：</td>
						<td><input name="markSixTmb" class="form-control" type="text"/></td>
						<td class="text-right media-middle">六合彩正码B返水比例(%)：</td>
						<td><input name="markSixZmb" class="form-control" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最高返水上限：</td>
						<td><input name="upperLimit" class="form-control required money" type="text"/></td>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">停用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">描述：</td>
						<td colspan="3"><textarea name="memo" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>