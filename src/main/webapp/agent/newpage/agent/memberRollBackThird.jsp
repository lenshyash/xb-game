<%@ page language="java" pageEncoding="UTF-8"%>
<div class="table-tool">
	<div class="form-group">
		<button class="btn btn-primary open-tab" url="${base }/agent/agent/memberRollBack/index.do">会员返水策略设置</button>
		<button class="btn btn-primary open-dialog cached" url="${base}/agent/agent/memberRollBack/addThird.do">新增</button>
	</div>
</div>
<table class="fui-default-table"></table>
<span style="color: red;">备注：如果要反水从0开始，请将有效投注设置成0（推荐设置为1）</span>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<span style="color: red;">会员返水是根据当天的有效投注计算</span>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/agent/memberRollBack/strategys.do',
		columns : [ {
			field : 'type',
			title : '类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'rate',
			title : '返水比例',
			align : 'center',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'qualifiedFmt',
			title : '有效投注',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'upperLimit',
			title : '最高返水上限',
			align : 'center',
			width : '100',
			valign : 'middle'
		}, {
				field : 'multiple',
				title : '打码量倍数',
				align : 'center',
				width : '100',
				valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'memo',
			title : '描述',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : memoFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '50',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	function typeFormatter(value, row, index) {
		if (value == 1) {
			return "真人";
		} else if (value == 2) {
			return "电子";
		}
		return "";
	}
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/agent/memberRollBack/updStatus.do?id="+row.id+"&status="});
	}
	function memoFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>8){
			return value.substr(0, 8)+'。。';
		}else{
			return value;
		}
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/agent/memberRollBack/modifyThird.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="${base}/agent/agent/memberRollBack/delStrategy.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
});
</script>