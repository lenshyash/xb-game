<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="agent_manager_form_id">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="keyName">
					<option value="account">代理账号</option>
					<option value="agentName">所属上级</option>
					<option value="userName">代理姓名</option>
					<option value="phone">手机号码</option>
					<option value="qq">QQ</option>
					<option value="wechat">微信</option>
					<option value="email">邮箱</option>
					<option value="lastLoginIp">最后登录IP</option>
					<option value="regIp">注册IP</option>
					<option value="registerUrl">注册来源</option>
					<option value="cardNo">银行账号</option>
					<option value="remark">备注</option>
				</select>
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="keyword" placeholder="关键字">
			</div>
			<div class="input-group">
				<select name="searchType" class="form-control">
					<option value="">级联查询选择</option>
					<option value="1">直属下级</option>
					<option value="2">所有下级</option>
				</select>
			</div>
			<div class="input-group">
				<select name="levelGroup" class="form-control"><option value="">所有等级</option><c:forEach items="${levels}" var="l"><option value="${l.id }"<c:if test="${(not empty level && level==l.id)}">selected</c:if>>${l.levelName }</option></c:forEach></select>
			</div>
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
				<option value="2">引导</option>
			</select>
			<select class="form-control" name="accountStatus">
				<option value="">状态</option>
				<option value="1">禁用</option>
				<option value="2">启用</option>
				<option value="3">未审核</option>
			</select>
			<div class="input-group">
				<select class="selectpicker form-control" id="select_id" data-live-search="true" title="选择层级" data-hide-disabled="true" multiple name="select_name">
					<option value="1">层级1</option>
					<option value="2">层级2</option>
					<option value="3">层级3</option>
					<option value="4">层级4</option>
					<option value="5">层级5</option>
					<option value="6">层级6</option>
					<option value="7">层级7</option>
					<option value="8">层级8</option>
					<option value="9">层级9</option>
				</select>
			</div>
			<c:if test="${warnFlag}">
				<select class="form-control" name="abnormalFlag">
					<option value="">预警类型</option>
					<option value="2" <c:if test="${(not empty abnormalFlag && abnormalFlag==0)}">selected</c:if>>普通</option>
					<option value="1" <c:if test="${(not empty abnormalFlag && abnormalFlag==1)}">selected</c:if> >预警</option>
				</select>
			</c:if>
<input type="text" id="input_select_id" style="display:none;" name="levelArray"/>

			<c:if test="${groupSwitch eq 'on' }">
			<div class="input-group">
				<input type="text" class="form-control" size="10" name="qzGroupName" placeholder="群组名称" value="${groupName }">
			</div>
			</c:if>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/agent/manager/add.do">新增</button>
			<c:if test="${moveFlag eq 'on' }">
				<button class="btn btn-primary open-dialog cached" url="${base}/agent/agent/manager/movepage.do">批量移动代理</button>
			</c:if>
			<button class="btn btn-primary back-btn hidden" type="button">返回</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>

<script type="text/javascript">
$('#select_id').on('changed.bs.select', function(e) {
    $("#input_select_id").val($(this).val());
});
</script>

<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var history=[],curParents=null
		,$form=$("#agent_manager_form_id");
	
	<c:if test="${not empty param.parents}">curParents="${param.parents}";history.push(curParents);</c:if>
	$form.find(".back-btn").click(function(){
		var last = history.pop();
		if (!last) {
			layer.msg("当前已经是最顶级了");
			curParents=null;
			return false;
		}
		if(curParents==last){
			curParents=history[history.length-1];
		}else{
			curParents = last;
		}
		var $table=$form.parents(".fui-box:first").data("bootstrapTable");
		if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/agent/manager/list.do',
		queryParams : function(params){
			var value = $form.find("[name='keyword']").val();
			if(value){
				params[$form.find("[name='keyName']").val()] = value;
				curParents = null;
			}else{
				if(curParents){
					params["parents"] = curParents;
					$form.find(".back-btn").removeClass("hidden");
				}
			}
			if(!curParents){
				$form.find(".back-btn").addClass("hidden");
				history=[];
			}
			return params
		},
		onLoadSuccess:onLoadSuccess,
		columns : [ {
			field : 'account',
			title : '代理账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'money',
			title : '账号余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		},
		<c:if test="${agentMulti && !mergeFlag}"> {
			field : 'gameShare',
			title : '返点数',
			align : 'center',
			valign : 'middle'},</c:if>
		<c:if test="${profitShareFlag=='on'}"> {
			field : 'profitShare',
			title : '占成数',
			align : 'center',
			valign : 'middle'},</c:if>
		<c:if test="${memberRateFlag}"> {
			field : 'rate',
			title : '自身赔率点',
			align : 'center',
			valign : 'middle'},</c:if>
		{
			field : 'userName',
			title : '代理姓名',
			align : 'center',
			valign : 'middle'
		},{
			field : 'levelGroup',
			title : '会员等级',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : levelFormatter
		},{
			field : 'phone',
			title : '电话/手机号',
			align : 'center',
			valign : 'middle'
		},{
			field : 'qq',
			title : 'qq/微信',
			align : 'center',
			valign : 'middle',
			formatter :function(value, row, index){
				var qq = value;
				var wechat = row.wechat;
				if(qq == null || qq == undefined || qq == ''){
					qq = "-";
				}
				if(wechat == null || wechat == undefined || wechat == ''){
					wechat = "-";
				}
				return qq+"<br>"+wechat;
			}
		},{
			field : 'reportType',
			title : '报表类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		}, {
			field : 'accountType',
			title : '用户类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'createDatetime',
			title : '注册时间',
			align : 'center',
			width : '200',
			sortable:true,
			valign : 'middle',
			formatter :function(value, row, index){
				if(row.registerUrl){
					return Fui.formatDatetime(value)+"<br>"+row.registerUrl;
				}else{
					return Fui.formatDatetime(value)||"";
				}
			}
			/* formatter : Fui.formatDatetime */
		},{
            field : 'registerIp',
            title : '注册IP',
            align : 'center',
            valign : 'middle',
            formatter :function(value, row, index){
                if(row.registerIp){
                    return row.registerIp;
                }else{
                    return value||"";
                }
            }
        }, {
			field : 'lastLoginDatetime',
			title : '最后登录',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){
				if(row.lastLoginIp){
					return row.lastLoginIp+"<br>"+Fui.formatDatetime(value);
				}else{
					return row.lastLoginIp||"";
				}
			}
		}, {
			field : 'changeDailiTime',
			title : '转代理时间',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){return Fui.formatDatetime(value);}
		},{
			title : '层级',
			align : 'center',
			valign : 'middle',
			formatter : updownFormatter
		}, {
			field : 'accountStatus',
			title : '状态',
			align : 'center',
			sortable:true,
			valign : 'middle',
			formatter : statusFormatter,
		},/* {
			field : 'lastLoginIp',
			title : '最后登录ip',
			align : 'center',
			valign : 'middle'
		}, */  {
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		},{
			title : '查看',
			align : 'center',
			valign : 'middle',
			width:'120',
			formatter : fastViewFormatter
		},{
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle',
			formatter : remarkFormatter
		}]
	});
	function onLoadSuccess($table){
		$table.find(".dodownup").click(function(){
			var $it=$(this)
				,id=$it.attr("rid")
				,agentId=$it.attr("agentId")
				,parents=$it.attr("parents")
				,cur = "";
			if($it.hasClass("up")){
				if (parents) {
					cur = parents.replace(agentId + ",", "");
					if (cur == ",") {
						cur = "";
					}
				}
			}else{
				if (parents) {
					cur = parents + id + ",";
				} else {
					cur = "," + id + ",";
				}
			}
			if (cur == ",") {
				cur = "";
			}
			if (!history.length || history[history.length - 1] != cur) {
				history.push(cur);
			}
			curParents = cur;
			$form.find("[name='keyword']").val("");
			var $table=$form.parents(".fui-box:first").data("bootstrapTable");
			if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
			return false;
		});
	}
	
	 $('#select_id').on('changed.bs.select', function(e) {
	     $("#input_select_id").val($(this).val());
	 });
	
	function accountFormatter(value, row, index) {
		if(row.accountType==1 && ${warnFlag} && row.abnormalFlag==1){
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
		} else if(row.accountType==1){
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>&nbsp;<span class=\'label label-danger\'>警</span>' ].join('');
		} else if(row.accountType==4 && ${warnFlag} && row.abnormalFlag==1){
			return ['<a class="open-dialog" href="${base}/agent/agent/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>&nbsp;<span class=\'label label-danger\'>警</span>' ].join('');
		} else if(row.accountType==4){
			return ['<a class="open-dialog" href="${base}/agent/agent/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
		}
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		var f = parseFloat(value);
		f=Math.round(f*100) / 100;
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(f);
		}
		return [ '<span class="text-primary">', '</span>' ].join(f);
	}
	var levels ={};
	<c:forEach items="${levels}" var="l">levels['${l.id}']='${l.levelName}';</c:forEach>
	function levelFormatter(value, row, index) {
		return [ '<span class="text-primary">', '</span>' ].join(levels[value]);
	}
	function reportFormatter(value, row, index) {
		var reportTypes = {"1":"普通","2":"引导"};
		return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
	}
	var types={
			1 : "会员",
			2 : "租户超级管理员",
			3 : "租户管理员",
			4 : "代理",
			5 : "总代理",
			9 : "引导"
		};
	function typeFormatter(value, row, index) {
		return ['<span class="text-primary">','</span>'].join(types[value]);
	}
	function statusFormatter(value, row, index) {
		if(value == 3){
			return ['<span class="text-primary">未审核</span>'].join();
		}else if(row.accountStatus == 1 && row.remark == 'noExamine'){
			return ['<span style="color:red;" class="text-primary">审核未通过</span>'].join();
		}else{
			return Fui.statusFormatter({val:value,url:"${base}/agent/agent/manager/updStatus.do?id="+row.id+"&accountStatus="});
		}
	}
	function updownFormatter(value, row, index) {
		var btns ='';
		if (row.accountType == 4) {
			if (row.agentId && row.level >= 2) {
				btns+='<a class="up dodownup" href="javascript:void(0)" agentId="'+row.agentId+'" rid="'+row.id+'" parents="'+row.parents+'" title="上级">上级</a>  ';
			}
			btns+='<a class="down dodownup" href="javascript:void(0)" agentId="'+row.agentId+'" rid="'+row.id+'" parents="'+row.parents+'" title="下级">下级</a>  ';
		}
		return btns;
	}
	
	function remarkFormatter(value, row, index) {
		var content = value;
		if(content && content.length > 6){
			content = content.substring(0,6)+"...";
			
		}if(!content || content.length == 0) {
			content = "添加备注";
		}
		return ['<a class="open-dialog" href="${base}/agent/member/manager/updateRemark.do?id=',row.id,'" remark="'+value+'"><span class="text-danger">',content,'</span></a>' ].join('');
	}
	
	function fastViewFormatter(value, row, index) {
		return '<a class="open-tab" href="${base}/agent/finance/memmnyrd/index.do?agentName='+row.account+'" title="会员账变管理">账变</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/global/index.do?agent='+row.account+'" title="全局报表管理">报表</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/risk/index.do?agent='+row.account+'&navName=cunqu" title="系统风险评估">财务</a> &nbsp;'
			+'<a class="open-tab" href="${base}/agent/report/memberData/index.do?account='+row.account+'" title="会员数据概况">概况</a>';
	}
	function operateFormatter(value, row, index) {
		var s = '<a class="open-dialog" href="${base}/agent/agent/manager/modify.do?id='+row.id+'" title="修改">修改</a>  '+
				'<a class="open-dialog" href="${base}/agent/agent/manager/modifyPwd.do?id='+row.id+'" title="修改密码">修改密码</a>  ';
		if(row.accountStatus == 3){
			var r= '<a class="todo-ajax" href="${base}/agent/agent/manager/updStatus.do?id='+row.id+'&accountStatus=2" data-url="${base}/agent/agent/manager/updStatus.do?id='+row.id+'&accountStatus=1&remark=noExamine" title="代理审核">审核</a>  ' + s;
		}else if(row.accountStatus == 1 && row.remark == 'noExamine'){
			var r= '';
		}else{
			var r= s;
		}
		<c:if test="${!agentMulti}">
		if(row.accountType==4){
			if(row.agentId){
				r+='<a class="todo-ajax" href="${base}/agent/agent/manager/delGeneral.do?id='+row.id+'" title="去除总代">去除总代</a>';
			}else{
				r+='<a class="open-dialog" href="${base}/agent/agent/manager/modifyZongDai.do?id='+row.id+'" title="指定总代">指定总代</a>';
			}
		}</c:if>
		<c:if test="${ warnFlag}">
		if(row.abnormalFlag==1){
			r += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=0&accountId='+row.id+'&account='+row.account+'" title="取消告警">取消告警</a>';
		}else{
			r += '<a class="todo-ajax" href="${base}/agent/member/manager/warningRevise.do?flag=1&accountId='+row.id+'&account='+row.account+'" title="设置告警">设为告警</a>';
		}
		</c:if>
		return r;
	}
});
</script>