<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/agent/manager/setGeneral.do" method="post"class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="setGeneralLabel">指定总代</h4>
		</div>
		<div class="modal-body">
			<input type="hidden" name="agentId" value="${member.id }">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">用户名：</td>
						<td><input type="text" class="form-control" name="agentName" value="${member.account }"disabled="disabled" /></td>
					</tr>
					<tr>
						<td class="text-right media-middle">总代理：</td>
						<td><input type="text" class="form-control" name="generalAgent" value="${member.agentName }"/></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default  fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div></form>