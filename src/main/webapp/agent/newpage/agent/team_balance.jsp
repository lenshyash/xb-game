<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="agent_manager_form_id2">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="keyName">
					<option value="account">用户账号</option>
				</select>
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="keyword" placeholder="关键字">
			</div>
			<div class="input-group">
				<select name="levelGroup" class="form-control"><option value="">所有等级</option><c:forEach items="${levels}" var="l"><option value="${l.id }"<c:if test="${(not empty level && level==l.id)}">selected</c:if>>${l.levelName }</option></c:forEach></select>
			</div>
			<select class="form-control" name="reportType">
				<option value="">报表类型</option>
				<option value="1"  <c:if test="${rdo}">selected</c:if>>普通</option>
				<option value="2">引导</option>
			</select>
			<select class="form-control" name="accountStatus">
				<option value="">状态</option>
				<option value="1">禁用</option>
				<option value="2">启用</option>
				<option value="3">未审核</option>
			</select>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary back-btn hidden" type="button">返回</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color:blue;">统计比较消耗系统性能，统计结果将缓存1分钟</div>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	var history=[],curParents=null
		,$form=$("#agent_manager_form_id2");
	
	<c:if test="${not empty param.parents}">curParents="${param.parents}";history.push(curParents);</c:if>
	$form.find(".back-btn").click(function(){
		var last = history.pop();
		if (!last) {
			layer.msg("当前已经是最顶级了");
			curParents=null;
			return false;
		}
		if(curParents==last){
			curParents=history[history.length-1];
		}else{
			curParents = last;
		}
		var $table=$form.parents(".fui-box:first").data("bootstrapTable");
		if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/report/teamBalance/list.do',
		queryParams : function(params){
			var value = $form.find("[name='keyword']").val();
			if(value){
				params[$form.find("[name='keyName']").val()] = value;
				curParents = null;
			}else{
				if(curParents){
					params["parents"] = curParents;
					$form.find(".back-btn").removeClass("hidden");
				}
			}
			if(!curParents){
				$form.find(".back-btn").addClass("hidden");
				history=[];
			}
			return params
		},
		onLoadSuccess:onLoadSuccess,
		columns : [ {
			field : 'account',
			title : '代理账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter
		}, {
			field : 'teamSumMoney',
			title : '团队余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		},
		{
			field : 'userName',
			title : '代理姓名',
			align : 'center',
			valign : 'middle'
		},{
			field : 'levelGroup',
			title : '会员等级',
			align : 'center',
			valign : 'middle',
			formatter : levelFormatter
		},{
			field : 'reportType',
			title : '报表类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		}, {
			field : 'accountType',
			title : '用户类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		} ,{
			title : '层级',
			align : 'center',
			valign : 'middle',
			formatter : updownFormatter
		}, {
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle'
		}]
	});
	function onLoadSuccess($table){
		$table.find(".dodownup").click(function(){
			var $it=$(this)
				,id=$it.attr("rid")
				,agentId=$it.attr("agentId")
				,parents=$it.attr("parents")
				,cur = "";
			if($it.hasClass("up")){
				if (parents) {
					cur = parents.replace(agentId + ",", "");
					if (cur == ",") {
						cur = "";
					}
				}
			}else{
				if (parents) {
					cur = parents + id + ",";
				} else {
					cur = "," + id + ",";
				}
			}
			if (cur == ",") {
				cur = "";
			}
			if (!history.length || history[history.length - 1] != cur) {
				history.push(cur);
			}
			curParents = cur;
			$form.find("[name='keyword']").val("");
			var $table=$form.parents(".fui-box:first").data("bootstrapTable");
			if($table && $table.length)$table.bootstrapTable('refreshOptions',{pageNumber:1});
			return false;
		});
	}
	function accountFormatter(value, row, index) {
		if(row.accountType==1){
			return ['<a class="open-dialog" href="${base}/agent/member/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
		}
		return ['<a class="open-dialog" href="${base}/agent/agent/manager/view.do?id=',row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		var f = parseFloat(value);
		f=Math.round(f*100) / 100;
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(f);
		}
		return [ '<span class="text-primary">', '</span>' ].join(f);
	}
	var levels ={};
	<c:forEach items="${levels}" var="l">levels['${l.id}']='${l.levelName}';</c:forEach>
	function levelFormatter(value, row, index) {
		return [ '<span class="text-primary">', '</span>' ].join(levels[value]);
	}
	function reportFormatter(value, row, index) {
		var reportTypes = {"1":"普通","2":"引导"};
		return [ '<span class="text-primary">', '</span>' ].join(reportTypes[value]);
	}
	var types={
			1 : "会员",
			2 : "租户超级管理员",
			3 : "租户管理员",
			4 : "代理",
			5 : "总代理"
		};
	function typeFormatter(value, row, index) {
		return ['<span class="text-primary">','</span>'].join(types[value]);
	}
	function updownFormatter(value, row, index) {
		var btns ='';
		if (row.accountType == 4) {
			if (row.agentId && row.level >= 2) {
				btns+='<a class="up dodownup" href="javascript:void(0)" agentId="'+row.agentId+'" rid="'+row.id+'" parents="'+row.parents+'" title="上级">上级</a>  ';
			}
			btns+='<a class="down dodownup" href="javascript:void(0)" agentId="'+row.agentId+'" rid="'+row.id+'" parents="'+row.parents+'" title="下级">下级</a>  ';
		}
		return btns;
	}
});
</script>