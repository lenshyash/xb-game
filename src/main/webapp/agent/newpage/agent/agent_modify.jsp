<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="agent_manager_modify_form_id">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改代理</h4>
		</div>
		<div class="modal-body"><input type="hidden" name="id" value="${member.id }">
			<input type="hidden" name="modifyType" value="1">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">登录账号：</td>
						<td width="30%"><input type="text" class="form-control" name="account" value="${member.account }"disabled/></td>
						<td class="text-right media-middle">代理姓名：</td>
						<td><input type="text" class="form-control" name="userName" value="${memberInfo.userName }" <c:if test="${empty modifyName or !modifyName }">disabled</c:if> /></td>
					</tr>
					<c:if test="${multiAgent1.multiAgent== 'off'}">
					<tr>
						<td class="text-right media-middle">账号类型：</td>
						<td colspan="3"><select name="accountType" class="form-control" disabled>
								<option value="4"<c:if test="${member.accountType==4}">selected</c:if>>代理</option>
								<option value="5"<c:if test="${member.accountType==5}">selected</c:if>>总代理</option>
						</select></td>
					</tr></c:if>
					<tr class="advanced_tr">
						<td class="text-center" colspan="4"><a href="#"><div style="float: top;">更多内容</div><div style="float: bottom;"><span id="advanced_span" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div></a>
						</td> 
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">电话：</td>
						<td><input type="text" class="form-control phone" name="phone" value="${memberInfo.phone }"/></td>
						<td class="text-right media-middle">微信号：</td>
						<td class="text-left"><input type="text" class="form-control" name="wechat" value="${memberInfo.wechat }" /></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">邮箱：</td>
						<td><input type="text" class="form-control email" name="email" value="${memberInfo.email }"/></td>
						<td class="text-right media-middle">QQ：</td>
						<td><input type="text" class="form-control" name="qq" value="${memberInfo.qq }"/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">取现银行：</td>
						<td><input name="bankName" value="${memberInfo.bankName }"class="form-control" type="text" placeholder="请输入银行名称"></td>
						<td class="text-right media-middle">银行账号：</td>
						<td><input type="text" class="form-control" name="cardNo" value="${memberInfo.cardNo }" placeholder="请输入银行卡号""/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">银行卡所在省：</td>
						<td><input name="province" value="${memberInfo.province }"class="form-control" type="text" placeholder="请输入银行卡所在省份"></td>
						<td class="text-right media-middle">银行卡所在市：</td>
						<td><input type="text" class="form-control" name="city" value="${memberInfo.city }" placeholder="请输入银行卡所在城市"/></td>
					</tr>
					<tr class="advanced hidden">
						<td class="text-right media-middle">银行地址：</td>
						<td><input type="text" class="form-control" name="bankAddress" value="${memberInfo.bankAddress }"/></td>
						<td class="text-right media-middle">银行卡黑名单：</td>
						<td><select name="cardNoStatus" class="form-control">
						<option value="1" <c:if test="${memberInfo.cardNoStatus==1}">selected</c:if>>启用</option><option value="2" <c:if test="${memberInfo.cardNoStatus==2}">selected</c:if>>禁用</option></select></td>
					</tr>
					<tr>
						<td width="20%" class="text-right media-middle">账号状态：</td>
						<td width="30%"><select name="accountStatus" class="form-control">
							<option value="2"<c:if test="${member.accountStatus==2}">selected</c:if>>启用</option>
							<option value="1"<c:if test="${member.accountStatus==1}">selected</c:if>>禁用</option>
						</select></td>
						<td class="text-right media-middle">注册IP：</td>
						<td class="media-middle">${member.registerIp}</td>
					</tr>
					<tr>
						<td class="text-right">聊天室放红包限制：</td>
						<td colspan="3">
						<input type="radio" name="redpacketLimit" <c:if test="${member.redpacketLimit==2}">checked="checked"</c:if> value="2"/>允许
						<input type="radio" name="redpacketLimit" <c:if test="${member.redpacketLimit==1}">checked="checked"</c:if>value="1"/>禁止
						</td>
					</tr>
					<tr>
						<td class="text-right">注册来源：</td>
						<td>${member.registerUrl}</td>
						<td class="text-right">注册系统：</td>
						<td>${member.registerOs}</td>
					</tr>
					<c:if test="${multiAgent1.multiAgent== 'on' && multiAgent1.mergeRebate ne 'on'}"><tr>
						<td class="text-right media-middle">返点数：</td>
						<td><input type="text" class="form-control" name="rebateNum" value="${multiAgent1.curRebateNum }" <c:if test="${empty modifyRate or !modifyRate }">disabled</c:if> /></td>
						<td class="media-middle" colspan="2">返点数可设置返点区间：${multiAgent1.rebateMinNum} - ${multiAgent1.rebateMaxNum }</td>
					</tr></c:if>
					<c:if test="${multiAgent1.profitShare== 'on'}"><tr>
						<td class="text-right media-middle">占成数：</td>
						<td><input type="text" class="form-control" name="profitShare" value="${multiAgent1.curProfitShare }" <c:if test="${empty modifyRate or !modifyRate }">disabled</c:if> /></td>
						<td class="media-middle" colspan="2">占成数可设置返点区间：${multiAgent1.profitShareMin} - ${multiAgent1.profitShareMax }</td>
					</tr></c:if>
					<c:if test="${multiAgent1.dynamicRate== 'on'}"><tr>
						<td class="text-right media-middle">赔率值：</td>
						<td><input type="text" class="form-control" name="dynamicRate" value="${multiAgent1.curDynamicRate }" <c:if test="${empty modifyRate or !modifyRate }">disabled</c:if> /></td>
						<td class="media-middle" colspan="2">赔率值可设置返点区间：${multiAgent1.dynamicRateMin} - ${multiAgent1.dynamicRateMax }</td>
					</tr></c:if>
					
					
					<tr>
						<td class="text-right media-middle">代理等级：</td>
						<td><select name="levelGroup" class="form-control" <c:if test="${empty modifyLevel or !modifyLevel }">disabled</c:if> >
						<c:forEach items="${levels }" var="l"><option value="${l.id }"<c:if test="${l.id==member.levelGroup }">selected</c:if>>${l.levelName }</option></c:forEach>
						</select></td>
						<td class="text-right media-middle">报表类型：</td>
						<td class="text-left media-middle">
							<c:if test="${member.reportType==1}">普通<input type="hidden" value="1" name="reportType"></c:if>
							<c:if test="${member.reportType==2}">引导<input type="hidden" value="2" name="reportType"></c:if>
						</td>
					</tr>
					<tr>
						<td class="text-right">所属上级：</td>
						<td colspan="3">${parentNames }</td>
					</tr>
										<tr>
						<td class="text-right">备注内容：</td>
						<td colspan="3"><textarea class="form-control" name="remark">${member.remark }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#agent_manager_modify_form_id");
	$form.submit(function(){
		if($form.find("[name='modifyType']").val() == 2){
			$.ajax({
				url : "${base}/agent/agent/manager/checkBankCard.do",
				data : {cardNo :$form.find("[name='cardNo']").val()},
				success : function(result) {
					if(result){
						layer.confirm('该银行卡已经绑定过了，是否继续绑定？', {
							btn : [ '继续','取消' ]
						}, function() {
							confirmCommit();
						});
					}else{
						confirmCommit();
					}
				}
			});
		}else{
			confirmCommit();
		}
		return false;
	});
	function confirmCommit(){
		$.ajax({
			url : "${base}/agent/agent/manager/save.do",
			data : $form.serialize(),
			success : function(result) {
				layer.closeAll();
				layer.msg("保存成功！");
				var $table=$(".fui-box.active").data("bootstrapTable");
				if($table && $table.length){
					$table.bootstrapTable('refresh');
				}
			}
		});
	}
	$(".advanced_tr").click(function(){
		$.ajax({
			url : "${base}/agent/agent/manager/advanced.do",
			data : {id:$form.find("[name='id']").val()},
			success : function(result) {
				$(".advanced_tr").addClass("hidden");
				$(".advanced").removeClass("hidden");
				$form.find("[name='phone']").val(result.phone);
				$form.find("[name='wechat']").val(result.wechat);
				$form.find("[name='email']").val(result.email);
				$form.find("[name='qq']").val(result.qq);
				$form.find("[name='cardNo']").val(result.cardNo);
				$form.find("[name='bankName']").val(result.bankName);
				$form.find("[name='cardNoStatus']").val(result.cardNoStatus);
				$form.find("[name='bankAddress']").val(result.bankAddress);
				$form.find("[name='province']").val(result.province);
				$form.find("[name='city']").val(result.city);
				$form.find("[name='modifyType']").val(2);
			}
		});
	});
});
</script>