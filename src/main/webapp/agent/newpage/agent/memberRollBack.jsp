<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="sys_member_rollback_con_warp_id">
	<c:if test="${zhenRenKaiGuan=='on' }"><ul class="nav nav-tabs mb10px">
		<li class="active"><a href="#" data-toggle="tab" oname="platform">彩票体育反水设置</a></li>
		<li><a href="#" data-toggle="tab" oname="third">真人反水策略设置</a></li>
	</ul></c:if>
	<div class="table-tool">
		<div class="form-group">
			<button class="btn btn-primary open-dialog cached add-btn1" url="${base}/agent/agent/memberRollBack/add.do">新增</button>
		</div>
	</div>
	<table class="fui-default-table"></table>
	<div style="color: red;" class="platform">备注：会员返水是根据当天的有效投注计算</div>
	<div class="hidden third"><span style="color: red;">备注：如果要反水从0开始，请将有效投注设置成0（推荐设置为1）</span>
		<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span style="color: red;">会员返水是根据当天的有效投注计算</span>
	</div>
</div>
<script type="text/javascript">
requirejs(['${base}/agent/js/system/memberRollBack.js'],function($depositset){
	$depositset.render();
});
</script>