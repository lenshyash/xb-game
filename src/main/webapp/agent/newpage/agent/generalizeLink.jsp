<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group">
		<div class="form-inline">
			<div class="input-group">
				<select class="form-control" name="type">
					<option selected="selected" value="0">全部</option>
					<c:if test="${trunOnoff ne 'on' }">
					<option value="2">会员</option>
					</c:if>
					<option value="1">代理</option>
				</select>
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="userAccount" placeholder="帐号">
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="linkKey" placeholder="推广链接">
			</div>
			<button class="btn btn-primary">查询</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['zeroclipboard','jquery','bootstrap','Fui'],function(ZeroClipboard){
	Fui.addBootstrapTable({
		url : '${base}/agent/generalizeLink/list.do',
		columns : [{
			field : 'userAccount',
			title : '账号',
			align : 'center',
			width : '100',
			valign : 'middle'
		},{
			field : 'type',
			title : '注册类型',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : typeFormatter
		},<c:if test="${isDuLiCaiPiao}"> {
			field : 'cpRolling',
			title : '彩票返点数',
			align : 'center',
			width : '100',
			valign : 'middle'
		},</c:if>
		<c:if test="${dynamicRate}"> {
			field : 'dynamicRate',
			title : '赔率点',
			align : 'center',
			width : '100',
			valign : 'middle'
		},</c:if>  {
			field : 'linkKey',
			title : '推广链接',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : pasteFormatter,
			class:'copyBtns'
		},{
			field : 'linkKey',
			title : '推广码',
			align : 'center',
			width : '100',
			valign : 'middle'
		},{
			field : 'status',
			title : '状态',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : statusFormatter
		}]
		,onLoadSuccess:function($table){
			var client = new ZeroClipboard($table.find('.copy-btn'));
			client.on( "ready", function (event) {
				client.on( "copy", function (event) {
					var t= $(event.target).attr("linkUrl");
					ZeroClipboard.setData( "text/plain",t);
					layer.msg('复制成功,请按ctrl+v粘贴:<br/>'+t,{time:1000});
				});
			});
		}
	});
	function typeFormatter(value, row, index) {
		if(row.type ==1){
			return '<span class="label label-info">代理</span>';
		}else{
			return '<span class="label label-success">会员</span>';
		}
	}
	function pasteFormatter(value, row, index) {
		return row.linkUrl + '&nbsp;<span class="label label-primary copy-btn" role="button" linkUrl="'+row.linkUrl+'">复制</span>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/generalizeLink/updateStatusById.do?id="+row.id+"&status="});
	}
})
</script>