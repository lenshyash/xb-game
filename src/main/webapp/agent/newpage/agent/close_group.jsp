<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="member_modify_pwd_form_id2">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">解散群</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right">群组名称：</td>
						<input type="hidden" name="groupName" value="${groupName }">
						<td>${groupName }</td>
					</tr>

				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#member_modify_pwd_form_id2");
	$form.submit(function(){
		var upd_url = "${base}/agent/agent/qzGroup/closeGroup.do"
			,groupName = $form.find("[name='groupName']").val();
		$.ajax({
			url : upd_url,
			data : {
				groupName : groupName 
			},
			success : function(result) {
				layer.closeAll();
				layer.msg("解散成功！");
			}
		});
		return false;
	});
});
</script>