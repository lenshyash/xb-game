<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/agent/proxyRebateTactics/addSave.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加代理返点策略</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">会员数量：</td>
						<td width="30%"><input name="effectMember" class="form-control required digits" type="text"/></td>
						<td width="20%" class="text-right media-middle">盈利金额：</td>
						<td width="30%"><input name="profitMin" class="form-control required money" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">体育佣金比例：</td>
						<td><input name="sportsGame" class="form-control required money" type="text"/></td>
						<td class="text-right media-middle">彩票佣金比例：</td>
						<td><input name="lotteryGame" class="form-control required money" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">真人佣金比例：</td>
						<td><input name="realGame" class="form-control required money" type="text"/></td>
						<td class="text-right media-middle">电子游戏佣金比例：</td>
						<td><input name="videoGame" class="form-control required money" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td colspan="3"><select name="status" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">停用</option>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>