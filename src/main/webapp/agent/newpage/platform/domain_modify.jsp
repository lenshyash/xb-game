<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/domain/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">绑定域名</h4>
		</div>
		<div class="modal-body"><input type="hidden" value="${domain.id}" name="id">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="30%" class="text-right media-middle">域名：</td>
						<td><input type="text" class="form-control" name="domain" value="${domain.domain}"></td>
					</tr>
					<tr>
						<td class="text-right media-middle">默认代理：</td>
						<td><input type="text" class="form-control" name="agentName" value="${domain.agentName}"/></td>
					</tr>
					<tr>
						<td class="text-right">前台默认主页：</td>
						<td><input type="text" class="form-control" name="defaultHome" value="${domain.defaultHome}"/>
							<div>/regpage.do  注册页面</div>
							<div>/loginPage.do  登录页面，需要您站点有该页面</div>
							<div>/index/loginInterface.do  登录页面，需要您站点有该页面</div>
							<div>forward:/toContinue.do  点击继续访问的页面，需要您的站点有该页面</div>
							<div>/index2.html  空页面</div>
							<div>forward:/lotteryV3/index.do  第三版彩票，设置后浏览器中不显示/lotteryV3/index.do</div>
							<div>/mobile/index.do  直接显示手机版本</div>
							<div>/down.do  应用下载页面</div>
							<div>/netNavV3.do  导航页，需要您有该页面</div>
							<div>/index/xljc.do  线路检测页面</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>