<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-inline">
		<div class="form-group">
			<div class="input-group">
				<select class="form-control" name="type">
					<option value="">选择游戏类型</option>
					<option value="1">彩票</option>
					<c:if test="${zhenRenKaiGuan=='on' }"><option value="2">真人</option>
					<option value="3">电子</option>
					<option value="99">三方体育</option></c:if>
					<option value="4">体育</option>
					<option value="5">六合特码B</option>
					<option value="6">六合正码B</option>
					<option value="11"><c:if test="${!empty replaceFont }">${replaceFont }</c:if><c:if test="${empty replaceFont }">系统彩</c:if></option>
					<option value="15">十分六合彩特码B</option>
					<option value="16">十分六合彩正码B</option>
					<option value="91">三方彩票</option>
					<option value="90">棋牌</option>
					<option value="92">电竞</option>
				</select>
			</div>
		</div>
		<button class="btn btn-primary fui-date-search">查询</button>
		<button class="btn btn-primary open-dialog cached" type="button" url="${base}/agent/strategy/add.do">新增</button>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color: red;" class="platform">备注：会员返水是根据当天的有效投注计算
	<br>系统每天凌晨统计前一天会员的有效投注（已派奖的投注金额）；统计完才能进行反水
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/strategy/list.do',
		columns : [{
			field : 'type',
			title : '游戏类型',
			align : 'center',
			width : '80',
			formatter:gameTypeFormatter
		},
		<c:if test="${isV4}">
			{
				field : 'roomName',
				title : '房间名称',
				align : 'center',
				width : '100'
			},
		</c:if>
		{
			field : 'rate',
			title : '反水比例 ',
			align : 'center',
			width : '80',
			formatter:bfbFormatter
		},{
			field : 'qualified',
			title : '有效投注',
			align : 'center',
			width : '80'
		},{
			field : 'multiple',
			title : '打码量倍数',
			align : 'center',
			width : '100',
		}, {
			field : 'upperLimit',
			title : '最高返水上限',
			align : 'center',
			width : '100',
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '100',
			formatter : statusFormatter
		}, {
			field : 'memo',
			title : '描述',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : memoFormatter
		},{
			title : '操作',
			align : 'center',
			width : '80',
			formatter : operateFormatter
		}]
	});
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/strategy/closeOrOpen.do?id="+row.id+"&status="});
	}
	function memoFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>8){
			return value.substr(0, 8)+'。。';
		}else{
			return value;
		}
	}
	function gameTypeFormatter(value, row, index) {
		if(value==1){
			return "彩票";
		}else if(value==2){
			return "真人";
		}else if(value==3){
			return "电子";
		}else if(value==4){
			return "体育";
		}else if(value==5){
			return "六合特码B";
		}else if(value==6){
			return "六合正码B";
		}else if(value==99){
			return "三方体育";
		}else if(value==91){
			return "三方彩票";
		}else if(value==90){
			return "棋牌";
		}else if(value==11){
			var a = '${replaceFont }';
			if('${replaceFont}'.length){
				return a;
			}
			return "系统彩";
		}else if(value==15){
			return "十分六合彩特码B";
		}else if(value==16){
			return "十分六合彩正码B";
		}else if(value==92){
			return "电竞";
		}
		return "";
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/strategy/modify.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="${base}/agent/strategy/delete.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
});
</script>