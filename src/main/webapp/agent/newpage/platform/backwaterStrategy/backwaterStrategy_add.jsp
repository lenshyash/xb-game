<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/strategy/save.do" class="form-submit">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">添加会员按日反水</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">类型</td>
						<td width="30%"><select class="form-control" name="type">
							<option value="1" selected>彩票</option>
							<c:if test="${zhenRenKaiGuan=='on' }"><option value="2">真人</option>
							<option value="3">电子</option>
							<option value="99">三方体育</option></c:if>
							<option value="4">体育</option>
							<option value="5">六合特码B</option>
							<option value="6">六合正码B</option>
							<option value="11"><c:if test="${!empty replaceFont }">${replaceFont }</c:if><c:if test="${empty replaceFont }">系统彩</c:if></option>
							<option value="15">十分六合彩特码B</option>
							<option value="16">十分六合彩正码B</option>
							<option value="91">三方彩票</option>
							<option value="90">棋牌</option>
							<option value="92">电竞</option>
						</select></td>
						<td width="20%" class="text-right media-middle">返水比例(%)：</td>
						<td width="30%"><input name="rate" class="form-control required money" type="text"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">有效投注(大于等于)：</td>
						<td><input name="qualified" class="form-control required money" type="text"/></td>
						<td class="text-right media-middle">打码量倍数：</td>
						<td><input name="multiple" class="form-control money" type="text" placeholder="空或者0代表不限制打码量"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最高返水上限：</td>
						<td><input name="upperLimit" class="form-control money" type="text" placeholder="空或者0代表不限制"/></td>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="2" selected>启用</option>
							<option value="1">停用</option>
						</select></td>
					</tr>
					<c:if test="${isV4}">
						<tr>
							<td class="text-right media-middle">所属房间：</td>
							<td colspan="3">
							<select name="roomId" class="form-control">
								<c:forEach items="${roomList}" var="room">
									<option value="${room.id}">${room.name}</option>
								</c:forEach>
							</select></td>
						</tr>
					</c:if>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"checked>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">描述：</td>
						<td colspan="3"><textarea name="memo" class="form-control"></textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>