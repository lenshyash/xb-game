<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/strategy/update.do" class="form-submit">
<div class="modal-dialog modal-lg"><input type="hidden" name="id" value="${rollback.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改会员按日反水策略</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">类型</td>
						<td width="30%"><select class="form-control" name="type" disabled>
							<option value="1"<c:if test="${rollback.type==1 }"> selected</c:if>>彩票</option>
							<c:if test="${zhenRenKaiGuan=='on' }"><option value="2"<c:if test="${rollback.type==2}"> selected</c:if>>真人</option>
							<option value="3"<c:if test="${rollback.type==3}"> selected</c:if>>电子</option>
							<option value="99"<c:if test="${rollback.type==99}"> selected</c:if>>三方体育</option></c:if>
							<option value="4"<c:if test="${rollback.type==4}"> selected</c:if>>体育</option>
							<option value="5"<c:if test="${rollback.type==5}"> selected</c:if>>六合特码B</option>
							<option value="6"<c:if test="${rollback.type==6}"> selected</c:if>>六合正码B</option>
							<option value="11"<c:if test="${rollback.type==11}"> selected</c:if>>系统彩</option>
							<option value="15"<c:if test="${rollback.type==15}"> selected</c:if>>十分六合彩特码B</option>
							<option value="16"<c:if test="${rollback.type==16}"> selected</c:if>>十分六合彩正码B</option>
							<option value="91"<c:if test="${rollback.type==91}"> selected</c:if>>三方彩票</option>
							<option value="90"<c:if test="${rollback.type==90}"> selected</c:if>>棋牌</option>
							<option value="92"<c:if test="${rollback.type==92}"> selected</c:if>>电竞</option>
						</select></td>
						<td width="20%" class="text-right media-middle">返水比例(%)：</td>
						<td width="30%"><input name="rate" class="form-control required money" type="text" value="${rollback.rate }"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">有效投注(大于等于)：</td>
						<td><input name="qualified" class="form-control required money" type="text"value="${rollback.qualified }"/></td>
						<td class="text-right media-middle">打码量倍数：</td>
						<td><input name="multiple" class="form-control money" type="text"value="${rollback.multiple }" placeholder="空或者0代表不限制打码量"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">最高返水上限：</td>
						<td><input name="upperLimit" class="form-control required money" type="text"value="${rollback.upperLimit }" placeholder="空或者0代表不限制"/></td>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="2"<c:if test="${rollback.status==2}"> selected</c:if>>启用</option>
							<option value="1"<c:if test="${rollback.status==1}"> selected</c:if>>停用</option>
						</select></td>
					</tr>
					<c:if test="${isV4}">
						<tr>
							<td class="text-right media-middle">所属房间：</td>
							<td colspan="3">
							<select name="roomId" class="form-control" disabled="disabled">
								<c:forEach items="${roomList}" var="room">
									<option value="${room.id}" ${rollback.roomId == room.id?'selected':''}>${room.name}</option>
								</c:forEach>
							</select></td>
						</tr>
					</c:if>
					<tr>
						<td class="text-right media-middle">限制使用等级：</td>
						<td colspan="3"><c:forEach items="${levels1}" var="le1"><label class="checkbox-inline"><input type="checkbox" name="groupLevelId" value="${le1.id }"<c:if test="${not empty glevelIds && glevelIds.contains(le1.id) }">checked</c:if>>${le1.levelName }</label>  </c:forEach></td>
					</tr>
					<tr>
						<td class="text-right media-middle">描述：</td>
						<td colspan="3"><textarea name="memo" class="form-control">${rollback.memo }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>