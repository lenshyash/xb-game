<%@ page language="java" pageEncoding="UTF-8"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date"  format="YYYY-MM-DD HH:mm:ss"  name="startTime" placeholder="开始日期" value="${startTime } 00:00:00"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="${account }" placeholder="会员账号">
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="type" id="type">
						<option value="">选择类型</option>
						<option value="1">余额宝返利</option>
						<option value="2">余额宝转入</option>
						<option value="3">余额宝转出</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="reportType" id="reportType">
						<option value="">报表类型</option>
						<option value="1">普通类型</option>
						<option value="2">引导类型</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="accountType" id="accountType">
						<option value="">会员类型</option>
						<option value="1">会员</option>
						<option value="4">代理</option>
						<option value="9">引导</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss"  placeholder="结束日期" value="${endTime } 23:59:59"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="orderNo" placeholder="订单号">
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/balanceGemRecord/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'account',
			title : '账号',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : accountFormatter
		},{
			field : 'orderNo',
			title : '单号',
			align : 'center',
			width : '180',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'money',
			title : '变动金额',
			align : 'center',
			width : '180',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"money");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.totalMoney ? aggsData.totalMoney.toFixed(6) : "0.00";
			}
		},{
			field : 'beforeMoney',
			title : '变动前金额',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'backMoney',
			title : '变动后金额',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'type',
			title : '变动类型',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : typeFormatter
		},{
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '280',
			valign : 'middle',
			formatter : Fui.formatDatetime
		},{
			field : 'remark',
			title : '备注',
			align : 'center',
			width : '280',
			valign : 'middle'
		}]
	});
	function typeFormatter(value, row, index) {
		switch(value){
			case 1:
				return '余额宝返利';
			case 2:
				return '余额宝转入';
			case 3:
				return '余额宝转出';
		}
	}
	function accountFormatter(value, row, index) {
		return ['<a class="open-tab" href="${base}/agent/finance/balanceGem/index.do?account=',row.account,'" title="余额宝详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey] ){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(6)+"";
	}
});
</script>