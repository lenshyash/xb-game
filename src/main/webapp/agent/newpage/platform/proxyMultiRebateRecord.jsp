<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" placeholder="开始日期" value="${curDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="代理账号">
			</div>
			<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="rebateType" id="rebateType">
							<option value="">选择返点类型</option>
							<option value="1">多级代理返点</option>
							<option value="2">动态赔率返点</option>
							<c:if test="${fixedRebate }">
							<option value="3">固定百分比返点</option>
							</c:if>
						</select>
					</div>
			</div>
			
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" placeholder="结束日期" value="${curDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/pmrr/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'account',
			title : '当前代理账户',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'child',
			title : '下级账户',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'betId',
			title : '注单ID',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'betMoney',
			title : '投注金额',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'rebateType',
			title : '返点类型',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : typeFormatter
		},{
			field : 'childRebateRate',
			title : '下级返点数',
			align : 'center',
			width : '180',
			valign : 'middle'
		},{
			field : 'selfRebateRate',
			title : '自身返点数',
			align : 'center',
			width : '180',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'rebateMoney',
			title : '返点金额',
			align : 'center',
			width : '180',
			valign : 'middle',
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"rebateMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.totalMoney ? aggsData.totalMoney.toFixed(2) : "0.00";
			}
		},{
			field : 'createDatetime',
			title : '反点时间',
			align : 'center',
			width : '280',
			valign : 'middle',
			formatter : Fui.formatDatetime
		},
		{
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		} ]
	});
	function statusFormatter(value, row, index) {
		return '<span class="label label-success">已反</span>&nbsp;&nbsp;'
			+'<a class="todo-ajax label label-info" href="${base}/agent/pmrr/cancel.do?betId='+row.betId+'" title="确定回滚？">回滚</a>';
	}
	
	function typeFormatter(value, row, index) {
		switch(row.rebateType){
			case 1:
				return "多级代理返点";
			case 2:
				return "动态赔率返点";
			case 3:
				return "固定百分比返点";
		}
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey] ){
				continue;
			}
			total += r[itemKey];
		}
		return total.toFixed(2)+"";
	}
});
</script>