<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/agentLotteryBetLimit/save.do" class="form-submit">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">绑定域名</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td><select name="type" class="form-control" onchange="getPlayGroup()" id="allActive">
						<option value="" selected="selected">--全部大类--</option>
						<option value="8" >PK10</option>
						<option value="9" >时时彩</option>
						<option value="66" >系统六合彩</option>
						<option value="6" >六合彩</option>
						<option value="15" >低频彩</option>
						</select></td>
						<td>
							<select id="code" class="form-control" name="code" onchange="change()">
							</select>
						</td>
					</tr>
					
					<tr>
						<td width="30%" class="text-right media-middle">限制个数：</td>
						<td><input type="text" name="betLimit" class="form-control" /></td>
					</tr>
					<tr>
						<td width="30%" class="text-right media-middle">限制类型：</td>
						<td><select name="limitType" class="form-control">
						<option value="1" selected>单号1-10/整合类限制</option>
						<option value="2" selected>纯数字类限制</option>
						</select></td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<input type="hidden" value="" name="groupName" id="groupName">
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>
<script>
	function getPlayGroup() {
        
		var lotterytype = $("select[name='type'] option:selected").attr("value");
	    $.ajax({
	        url: "${base}/agent/agentLotteryBetLimit/getPlayGroup.do",
	        data: {type: lotterytype},
	        dataType: "JSON",
	        success: function (resdata) {
	        	var html = '<option></option>';
	        	$.each(resdata,function(i,j){
	                var opt = '<option value="'+j.code+'">'+j.name+'</option>';
	                html += opt;
	            })
	            $("#code").html(html);
	        }
	    })
	}
	
	function change() {
		var groupName=$("#code").find("option:selected").text(); 
		$("#groupName").attr("value",groupName);
	}
</script>
