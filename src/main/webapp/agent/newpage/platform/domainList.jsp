<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group keyword">
				<input type="text" class="form-control" name="domain" placeholder="域名">
			</div>
			<div class="input-group keyword">
				<input type="text" class="form-control" name="agentName" placeholder="所属代理">
			</div>
			<button class="btn btn-primary">查询</button>
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/domain/add.do">新增</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/domain/list.do',
		columns : [ {
			field : 'domain',
			title : '域名',
			align : 'center'
		},{
			field : 'agentName',
			title : '默认代理',
			align : 'center'
		},{
			field : 'remark',
			title : '备注',
			align : 'center',
			formatter : remarkFormatter
		},{
			field : 'status',
			title : '域名状态',
			align : 'center',
			formatter : statusFormatter
		},{
			field : 'defaultHome',
			title : '前台默认主页',
			align : 'center'
		},{
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			formatter : Fui.formatDatetime
		}, {
			title : '操作',
			align : 'center',
			formatter : operateFormatter
		} ]
	});
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/domain/updStatus.do?id="+row.id+"&status="});
	}
	function operateFormatter(value, row, index) {
		return '<a class="open-dialog" href="${base}/agent/domain/modify.do?id='+row.id+'" title="修改">修改</a> &nbsp; '
			+'<a class="todo-ajax" href="${base}/agent/domain/delete.do?id='+row.id+'" title="确定要删除域名“'+row.domain+'”">删除</a>';
	}
	function remarkFormatter(value, row, index) {
		var content = value;
		if(content && content.length > 6){
			content = content.substring(0,6)+"...";

		}if(!content || content.length == 0) {
			content = "添加备注";
		}
		return ['<a class="open-dialog" href="${base}/agent/domain/updateRemark.do?id=',row.id,'" title="'+value+'"><span class="text-danger">',content,'</span></a>' ].join('');
	}
});
</script>