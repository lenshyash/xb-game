<%@ page language="java" pageEncoding="UTF-8"%>
<button class="btn btn-success pull-right" style="position:relative;top:50px;" id="member_rollback_record_list_fan_shui_id">确认反水</button>
<form class="fui-search table-tool" method="post" id="member_rollback_record_list_fform_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startTime" placeholder="开始日期" value="${startDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="betType" id="betType">
						<option value="">选择游戏类型</option>
						<option value="1">彩票</option>
						<option value="2">真人</option>
						<option value="3">电子游艺</option>
						<option value="4">体育</option>
						<option value="5">六合彩</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
			<button class="btn btn-primary open-dialog" type="button" url="${base}/agent/mrbr/handRollBack.do">批量返水</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endTime" class="form-control fui-date" placeholder="结束日期" value="${endDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<div class="right-btn-table"><table class="fui-default-table"></table></div>
<div id="remark_div" style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">全选时,只会针对<strong>已结算</strong>和<strong>已回滚</strong>的用户进行返水!</span>
	<br/><span class="glyphicon glyphicon-info-sign">小&nbsp;&nbsp;计：</span><span class="text-danger">小计只对当前页所有状态进行小计!</span>
	<br/><span class="glyphicon glyphicon-info-sign">总&nbsp;&nbsp;计：</span><span class="text-danger">总计只对已结算和已回滚状态进行总计!</span>
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $fanDianBtn=$("#member_rollback_record_list_fan_shui_id")
		,$form=$("#member_rollback_record_list_fform_id");
	$fanDianBtn.click(function(){
		var $table=$fanDianBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
			,count = list.length
			,ids='' , it=null;
		if(count <= 0){
			layer.msg('请选择需要反水的记录!');
  			return false;
		}else{
			for(var i=0;i<count;i++){
				it = list[i];
				if(it.id){
		        	ids=ids+it.id+',';
	        	}
			}
			ids=ids.substring(0,ids.length-1);
			layer.confirm('确定要对选择的记录进行返水？', function() {
				$.ajax({
					url:"${base}/agent/mrbr/dealRollIds.do",
					data:{"ids":ids
						,startTime:$form.find("[name='startTime']").val()
						,endTime:$form.find("[name='endTime']").val()},
					success:function(j){
						layer.msg(j.msg)
						$table.bootstrapTable('refresh');
					}
				});
			});
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/mrbr/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'betType',
			title : '游戏类型',
			align : 'center',
			width : '60',
			valign : 'bottom',
			formatter:gameTypeFormatter
		},{
			field : 'account',
			title : '注单账户',
			align : 'center',
			width : '120',
			valign : 'bottom',
			formatter:accountFormatter
		},{
			field : 'betOrderId',
			title : '注单号',
			align : 'center',
			width : '180',
			valign : 'bottom',
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'betMoney',
			title : '投注金额',
			align : 'center',
			width : '60',
			valign : 'bottom',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"betMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.00"
				}
				return aggsData.bettingMoneyCount ? Fui.toDecimal(aggsData.bettingMoneyCount,2) : "0.00";
			}
		}, {
			field : 'rollBackMoney',
			title : '反水金额',
			align : 'center',
			width : '60',
			valign : 'bottom',
			formatter : fsdFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var total = 0;
				for(var i=0;i<rows.length;i++){
					var r = rows[i];
					if(!r["rollBackMoney"] || r.rollBackStatus != 4){
						continue;
					}
					total += r["rollBackMoney"];
				}
				return Fui.toDecimal(total,4);
			},
			allSummaryFormat:function(rows,aggsData){
				if(!aggsData){
					return "0.0000"
				}
				return aggsData.returnMoneyCount ? Fui.toDecimal(aggsData.returnMoneyCount,4) : "0.0000";
			}
		}, {
			field : 'rollBackRate',
			title : '反水比例',
			align : 'center',
			width : '60',
			valign : 'bottom',
			formatter : numFormatter
		}, 
		{
			field : 'rollBackStatus',
			title : '反水状态',
			align : 'center',
			width : '60',
			valign : 'bottom',
			formatter : statusFormatter
		},
		{
			field : 'betOrderDatetime',
			title : '订单时间',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : Fui.formatDatetime
		},{
			title : '操作',
			align : 'center',
			width : '80',
			valign : 'middle',
			formatter : optFormatter
		},
		{
			checkbox : true
		}]
		,onLoadSuccess:function($con){
			$con.find(".unbatch_r").each(function(){
				$con.find('input[data-index="'+$(this).attr("tindex")+'"]').attr('type','hidden');
			});
		}
	});
	function accountFormatter(value, row, index) {
		if(row.rollBackStatus != 2 && row.rollBackStatus != 3){
			return '<span class="unbatch_r" tindex="'+index+'">'+value+'</span>';
		}
		return value;
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return Fui.toDecimal(total,2);
	}
	function gameTypeFormatter(value, row, index) {
		if(value==1){
			return "彩票";
		}else if(value==2){
			return "真人";
		}else if(value==3){
			return "电子游艺";
		}else if(value==4){
			return "体育";
		}else if(value==5){
			return "六合彩";
		}else{
			return "";
		}
	}
	function numFormatter(value, row, index){
		if(value){
			return Fui.toDecimal(value,2);
		}
		return "";
	}
	//返水金额
	function fsdFormatter(value, row, index){
		if(value){
			if(row.rollBackStatus == 4){
				return "<font color='blue'>"+ Fui.toDecimal(value,4)+"</font>";
			}
			return Fui.toDecimal(value,4);
		}else{
			return '0.0000';
		}
	}
	function statusFormatter(value, row, index) {
		if(value==1){
			return '<span class="label label-danger">未反水</span>';
		}else if(value==2){
			return '<span class="label label-danger">未反水</span>';
		}else if(value==3){
			return '<span class="label label-warning">已回滚</span>';
		}else if(value==4){
			return '<span class="label label-success">已反水</span>';
		}else{
			return '<span class="label label-info">状态未知</span>';
		}
	}
	function optFormatter(value, row, index) {
		var value = row.rollBackStatus;
		if(value==1 || value==2){
			return '<a class="todo-ajax label label-primary" href="${base}/agent/mrbr/manualRollbackMoney.do?betType='+row.betType+'&betId='+row.betId+'&stationId='+row.stationId+'" title="确定要手动反水？"><i class="glyphicon glyphicon-wrench"></i>手动反水</a>';
		}else if(value==3){
			return '<a class="todo-ajax label label-warning" href="${base}/agent/mrbr/remanualRollbackMoney.do?betType='+row.betType+'&betId='+row.betId+'&stationId='+row.stationId+'" title="确定要重新反水？"><i class="glyphicon glyphicon-refresh"></i>重新反水</a>';
		}else if(value==4){
			return '<a class="todo-ajax label label-danger" href="${base}/agent/mrbr/cancel.do?betType='+row.betType+'&betId='+row.betId+'&stationId='+row.stationId+'" title="确定要回滚？"><i class="glyphicon glyphicon-repeat"></i>回滚</a>';
		}else{
			return '<span class="label label-info">状态未知</span>';
		}
	}
});
</script>