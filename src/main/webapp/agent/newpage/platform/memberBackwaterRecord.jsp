<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<button class="btn btn-success pull-right" style="position:relative;top:50px;" id="member_backwater_record_list_fan_shui_id">确认反水</button>
<form class="fui-search table-tool" method="post" id="member_backwater_record_list_fform_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startDate" placeholder="开始日期" value="${startDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="会员账号">
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="betType">
						<option value="">选择游戏类型</option>
						<option value="1">彩票</option>
						<c:if test="${zhenRenKaiGuan=='on' }"><option value="2">真人</option>
						<option value="3">电子</option></c:if>
						<option value="4">体育</option>
						<option value="99">三方体育</option>
						<option value="5">六合特码B</option>
						<option value="6">六合正码B</option>
						<option value="11"><c:if test="${!empty replaceFont }">${replaceFont }</c:if><c:if test="${empty replaceFont }">系统彩</c:if></option>
						<option value="15">十分六合彩特码B</option>
						<option value="16">十分六合彩正码B</option>
						<option value="91">三方彩票</option>
						<option value="90">棋牌</option>
						<option value="92">电竞</option>
					</select>
				</div>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" placeholder="结束日期" value="${endDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="status">
						<option value="">选择反水状态</option>
						<option value="1">未反水</option>
						<option value="3">已回滚</option>
						<option value="4">已反水</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="reportType">
					<option value="">所有报表类型</option>
					<option value="1" <c:if test="${rdo}">selected</c:if>>普通</option>
					<option value="2">引导</option>
					</select>
				</div>
			</div>
			<c:if test="${isV4}">
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="backwaterRoomId">
						<option value="">选择所属房间</option>
						<c:forEach items="${roomList}" var="room">
							<option value="${room.id }">${room.name }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			</c:if>
		</div>
	</div>
</form>
<div class="right-btn-table"><table class="fui-default-table" id="member_backwater_record_list_table_id"></table></div>
<div style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">今日记录需要明天才能反水!</span>
	<br/><span class="text-danger">在“系统设置-><a href="${base }/agent/strategy/index.do" class="open-tab">会员按日反水策略</a>”设置了反水策略，系统每天凌晨统计前2天的反水信息，您才能进行反水操作!</span>
<!-- 	<br/><span class="text-danger">系统彩使用彩票按日反水策略!</span> -->
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $fanDianBtn=$("#member_backwater_record_list_fan_shui_id")
		,$form=$("#member_backwater_record_list_fform_id");
	$fanDianBtn.click(function(){
		var $table=$fanDianBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
			,count = list.length
			,moneys='',money='' , fsa=null,it=null;
		if(count <= 0){
			layer.msg('请选择需要反水的记录!');
  			return false;
		}else{
			for(var i=0;i<count;i++){
				it = list[i];
				if(it.id){
		        	fsa=$table.find("input[name='fanShuiAmount"+it.id+"']");
		        	if(fsa.length){
		        		money=fsa.val();
		        		if(!money || !/^[0-9]+(\.[0-9]{1,4})?$/.test(money)){
							layer.msg("请输入正确的反水金额");
							fsa.focus();
							return false;
						}
			        	moneys=moneys+it.id+":"+money+",";
		        	}
	        	}
			}
			moneys=moneys.substring(0,moneys.length-1);
			layer.confirm('确定要对选择的记录进行返水？', function() {
				$.ajax({
					url:"${base}/agent/backwater/dealRollIds.do",
					data:{moneys:moneys
						,startDate:$form.find("[name='startDate']").val()
						,endDate:$form.find("[name='endDate']").val()},
					success:function(j){
						layer.msg(j.msg)
						$table.bootstrapTable('refresh');
					}
				});
			});
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/backwater/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'betType',
			title : '游戏类型',
			align : 'center',
			width : '60',
			valign:'middle',
			formatter:gameTypeFormatter
		},
		<c:if test="${isV4}">
		{
			field : 'roomName',
			title : '房间名称',
			align : 'center',
			valign:'middle',
			width : '80'
		},
		</c:if>
		{
			field : 'account',
			title : '注单账户',
			align : 'center',
			width : '120',
			valign:'middle',
			formatter:accountFormatter
		},{
			field : 'agentName',
			title : '上级代理',
			align : 'center',
			width : '120',
			valign:'middle'
		},{
			field : 'betDate',
			title : '下注日期',
			align : 'center',
			width : '100',
			valign:'middle',
			formatter : Fui.formatDate,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'betMoney',
			title : '有效投注金额',
			align : 'center',
			width : '60',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"betMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.betMoney) ? Fui.toDecimal(aggsData.betMoney,4) : "0.0000";
			}
		},{
			field : 'winMoney',
			title : '中奖金额',
			align : 'center',
			width : '60',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"winMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.winMoney) ? Fui.toDecimal(aggsData.winMoney,4) : "0.0000";
			}
		}, {
			field : 'backwaterMoney',
			title : '反水金额',
			align : 'center',
			width : '60',
			formatter : fsdFormatter,
			pageSummaryFormat:function(rows,aggsData){
				var total = 0;
				var bwm = 0;
				for(var i=0;i<rows.length;i++){
					var r = rows[i];
					if(r["backwaterStatus"] == 1 || r["backwaterStatus"] == 3){
						bwm = r["backwaterRate"] * r["betMoney"] /100;
					}else{
						bwm = r["backwaterMoney"];
					}
					
					total += bwm;
				}
				return Fui.toDecimal(total,4);
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.backwaterMoney) ? Fui.toDecimal(aggsData.backwaterMoney,4) : "0.0000";
			}
		}, {
			field : 'backwaterRate',
			title : '反水比例',
			align : 'center',
			width : '60',
			valign:'middle',
			formatter : numFormatter
		}, {
			field : 'backwaterStatus',
			title : '反水状态',
			align : 'center',
			width : '60',
			valign:'middle',
			formatter : statusFormatter
		}, {
			field : 'operator',
			title : '操作员',
			width : '80',
			valign:'middle',
			align : 'center'
		},{
			title : '操作',
			align : 'center',
			width : '80',
			valign:'middle',
			formatter : optFormatter
		},{
			valign:'middle',
			checkbox : true
		}]
		,onLoadSuccess:function($con){
			$con.find(".unbatch_r").each(function(){
				$con.find('input[data-index="'+$(this).attr("tindex")+'"]').attr('type','hidden');
			});
			$con.find(".doFanShui").click(function(){
				var url=$(this).attr("params")
					,id=$(this).attr("aid")
					,money=$con.find("input[name='fanShuiAmount"+id+"']").val();
				if(!money || !/^[0-9]+(\.[0-9]{1,4})?$/.test(money)){
					layer.msg("请输入正确的反水金额");
					return false;
				}
				$.ajax({
					url:url+"&money="+money,
					success:function(j){
						layer.msg(j.msg)
						$fanDianBtn.parents(".fui-box").data("bootstrapTable").bootstrapTable('refresh');
					}
				});
			});
		}
	});
	function accountFormatter(value, row, index) {
		if(row.backwaterStatus==4){
			return '<span class="unbatch_r" tindex="'+index+'">'+value+'</span>';
		}
		return value;
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return Fui.toDecimal(total,2);
	}
	function gameTypeFormatter(value, row, index) {
		if(value==1){
			return "彩票";
		}else if(value==2){
			return "真人";
		}else if(value==3){
			return "电子";
		}else if(value==4){
			return "体育";
		}else if(value==5){
			return "六合特码B";
		}else if(value==6){
			return "六合正码B";
		}else if(value==11){
			return "系统彩";
		}else if(value==15){
			return "十分六合彩特码B";
		}else if(value==16){
			return "十分六合彩正码B";
		}else if(value==99){
			return "三方体育";
		}else if(value==91){
			return "三方彩票";
		}else if(value==90){
			return "棋牌";
		}else if(value==92){
			return "电竞";
		}
		return "";
	}
	function numFormatter(value, row, index){
		if(value){
			return Fui.toDecimal(value,2);
		}
		return "";
	}
	//返水金额
	function fsdFormatter(value, row, index){
		var bwm = row["backwaterMoney"];
		var desc = "";
		if(row["backwaterStatus"] == 1 || row["backwaterStatus"] == 3){
			bwm = row["backwaterRate"] * row["betMoney"] /100;
			desc = ["<span class='label label-warning'>","</span>"].join("预算");
		}else{
			return bwm+["<span class='label label-success'>","</span>"].join("实反");
		}
		if(bwm){
			return '<div class="form-inline"><input class="form-control" name="fanShuiAmount'+row.id+'" type="text" value="'+Fui.toDecimal(bwm,4)+'" >'+desc+'</div>';
		}
		return '<input class="form-control" name="fanShuiAmount'+row.id+'" type="text" value="" >';

	}
	function statusFormatter(value, row, index) {
		if(value==1 || value==2){
			return '<span class="label label-danger">未反水</span>';
		}else if(value==3){
			return '<span class="label label-warning">已回滚</span>';
		}else if(value==4){
			return '<span class="label label-success">已反水</span>';
		}else if(value==8){
			return '<span class="label label-default">过期未领取</span>';
		}else if(value==9){
			return '<span class="label label-primary">待领取</span>';
		}
		return "";
	}
	function optFormatter(value, row, index) {
		var value = row.backwaterStatus;
		var roomHref='';
		if(row.backwaterRoomId && row.backwaterRoomId>0){
			roomHref = "&roomId="+row.backwaterRoomId;
		}
		if(value==1 || value==2){
			if(row.betDate<Fui.formatDate(new Date())){
				return '<a class="doFanShui label label-primary" href="javascript:void(0)" aid="'+row.id+'" params="${base}/agent/backwater/manualRollback.do?betType='+row.betType+'&account='+row.account+'&betDate='+Fui.formatDate(row.betDate)+''+roomHref+'" title="确定要手动反水？"><i class="glyphicon glyphicon-wrench"></i>手动反水</a>';
			}else{
				return '<span class="unbatch_r" tindex="'+index+'">今日记录</span>';
			}
		}else if(value==3){
			return '<a class="doFanShui label label-warning" href="javascript:void(0)" aid="'+row.id+'" params="${base}/agent/backwater/manualRollback.do?betType='+row.betType+'&account='+row.account+'&betDate='+Fui.formatDate(row.betDate)+''+roomHref+'" title="确定要重新反水？"><i class="glyphicon glyphicon-refresh"></i>重新反水</a>';
		}else if(value==4){
			return '<a class="todo-ajax label label-danger" href="${base}/agent/backwater/cancel.do?betType='+row.betType+'&account='+row.account+'&betDate='+Fui.formatDate(row.betDate)+''+roomHref+'" title="确定要回滚？"><i class="glyphicon glyphicon-repeat"></i>回滚</a>';
// 		}else if(value==9){
// 			return '<a class="todo-ajax label label-default" href="${base}/agent/backwater/doExpired.do?rId='+row.id+'" title="确定要回滚？"><i class="glyphicon glyphicon-remove"></i>作废</a>';
		}
		return "";
	}
});
</script>