<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<button class="btn btn-primary open-dialog cached" url="${base}/agent/agentLotteryBetLimit/add.do">新增</button>
		</div>
	</div>
</form>
<table class="fui-default-table"></table>
<span class="text-danger">
备注：PK10单号1-10，及低频彩1-3球，时时彩的整合里的选择类型为“单号1-10/整合类限制”，
时时彩的万位、千位、百位、十位、个位的选择类型是“纯数字类限制”，
六合彩及系统六合彩的特码、正码、正特码的选择类型是“纯数字类限制”，
请勿设置以上未列到的玩法限制 如不了解限制投注的配置建议咨询客服</span>
<script type="text/javascript">
requirejs(['jquery','bootstrap','Fui'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/agentLotteryBetLimit/list.do',
		columns : [ {
			field : 'lotType',
			title : '彩种类型',
			align : 'center',
			valign : 'middle',
			formatter : reportFormatter
		},{
			field : 'limitType',
			title : '限制类型',
			align : 'center',
			valign : 'middle',
			formatter : limitFormatter
		},{
			field : 'groupName',
			title : '玩法限制',
			align : 'center',
			valign : 'middle'
		},{
			field : 'betLimit',
			title : '限制投注数',
			align : 'center',
			valign : 'middle'
		},{
			field : 'status',
			title : '状态',
			align : 'center',
			width:'80',
			valign : 'middle',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	});
	
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/agentLotteryBetLimit/updateStatus.do?id="+row.id+"&status="});
	}
	function reportFormatter(value, row, index) {
		var reportTypes = {"8":"PK10","9":"时时彩","66":"系统六合彩","6":"六合彩","15":"低频彩"};
		return [ '<span">', '</span>' ].join(reportTypes[value]);
	}
	function limitFormatter(value, row, index) {
		var reportTypes = {"1":"单号1-10/整合类限制","2":"纯数字类限制"};
		return [ '<span">', '</span>' ].join(reportTypes[value]);
	}
	function operateFormatter(value, row, index) {
		return '<a class="todo-ajax" href="${base}/agent/agentLotteryBetLimit/delete.do?id='+row.id+'" title="确定要删除">删除</a>';
	}
});
</script>