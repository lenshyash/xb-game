<%@ page language="java" pageEncoding="UTF-8"%>
<button class="btn btn-success pull-right" style="position:relative;top:50px;" id="proxy_rebate_record_list_fan_dian_id">确认返点</button>
<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startDate" placeholder="开始日期" value="${startDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="代理账号">
			</div>
			<div class="input-group">
				<select class="form-control" name="searchType">
					<option value="">选择查询层级</option>
					<option value="1">直属下级</option>
					<option value="2">所有下级</option>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" name="status" id="status">
					<option value="">选择返点状态</option>
					<option value="1">未返</option>
					<option value="2">已反</option>
				</select>
			</div>
			
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" placeholder="结束日期" value="${endDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
		</div>
	</div>
</form>
<div class="right-btn-table"><table class="fui-default-table"></table></div>
<div id="remark_div" style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">代理下的会员投注体育、真人、电子没有返水，将不能进行返点</span>
	<br/><span class="text-danger">全选时,只会针对<strong>未返点</strong>的代理进行返点!</span>
	<br/><span class="glyphicon glyphicon-info-sign">小&nbsp;&nbsp;计：</span><span class="text-danger">小计只对当前页所有状态进行小计!</span>
</div>
<script id="proxy_rebate_record_list_tpl" type="text/html">
<form action="${base}/agent/prr/toProfit.do" class="form-submit" id="proxy_rebate_record_to_profit_formId"><div class="modal-dialog">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
		<h4 class="modal-title">返点确认</h4>
	</div>
	<div class="modal-body">
		<input type="hidden" name="pointIds" value="{{pointIds}}"/>
		<input type="hidden" name="accountId" value="{{accountId}}"/>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td class="text-center" colspan="4" >返点代理:<span>{{accsName}}</span></td>
				</tr>
					<tr>
						<td width="20%" class="text-right toRed" >体育：</td>
						<td width="40">
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="sportsProfit" value="{{sportsProfit}}" type="text" disabled="disabled"class="form-control" placeholder="体育盈利">
							</div>
						</td>
						<td width ="40">	
							<div class ="input-group">		
								<span class="input-group-addon">投注</span>
								<input name="sportsBet" value="{{sportsBet}}" type="text" disabled="disabled"class="form-control" placeholder="体育盈利" style="width: 120px;">
							</div>	
						</td>
						<td width="40">
							<div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="sportsPoint" value="0" type="text" class="form-control" placeholder="体育返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toBlue">真人：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="realProfit" value="{{realProfit}}" type="text" disabled="disabled"class="form-control" placeholder="真人盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="realBet" value="{{realBet}}" type="text" disabled="disabled"class="form-control" placeholder="真人盈利">
							</div>
						</td>
						<td><div class="input-group">
									<span class="input-group-addon">返点</span>
								<input name="realPoint" value="0" type="text" class="form-control" placeholder="真人返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toGreen">电子：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="egameProfit" value="{{egameProfit}}" type="text" disabled="disabled"class="form-control" placeholder="电子盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="egameBet" value="{{egameBet}}" type="text" disabled="disabled"class="form-control" placeholder="电子盈利">
							</div>
						</td>
						<td><div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="egamePoint" value="0" type="text" class="form-control" placeholder="电子返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toGreen">棋牌：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="chessProfit" value="{{chessProfit}}" type="text" disabled="disabled"class="form-control" placeholder="棋牌盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="chessBet" value="{{chessBet}}" type="text" disabled="disabled"class="form-control" placeholder="棋牌盈利">
							</div>
						</td>
						<td><div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="chessPoint" value="0" type="text" class="form-control" placeholder="电子返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toGreen">电竞：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="esportsProfit" value="{{esportsProfit}}" type="text" disabled="disabled"class="form-control" placeholder="电竞盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="esportsBet" value="{{esportsBet}}" type="text" disabled="disabled"class="form-control" placeholder="电竞盈利">
							</div>
						</td>
						<td><div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="esportsPoint" value="0" type="text" class="form-control" placeholder="电竞返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toRed">彩票：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="lotteryProfit" value="{{lotteryProfit}}" type="text" disabled="disabled"class="form-control" placeholder="彩票盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="lotteryBet" value="{{lotteryBet}}" type="text" disabled="disabled"class="form-control" placeholder="彩票盈利">
							</div>
						</td>
						<td><div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="lotteryPoint" value="0" type="text" class="form-control" placeholder="彩票返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
					<tr>
						<td class="text-right toRed">六合彩：</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">盈利</span>
								<input name="markSixProfit" value="{{markSixProfit}}" type="text" disabled="disabled"class="form-control" placeholder="六合彩盈利">
							</div>
						</td>
						<td>
							<div class="input-group">
								<span class="input-group-addon">投注</span>
								<input style="width: 120px;" name="markSixBet" value="{{markSixBet}}" type="text" disabled="disabled"class="form-control" placeholder="六合彩盈利">
							</div>
						</td>
						<td><div class="input-group">
								<span class="input-group-addon">返点</span>
								<input name="markSixPoint" value="0" type="text" class="form-control" placeholder="六合彩返点">
								<span class="input-group-addon">%</span>
							</div>					
						</td>
					</tr>
				<tr>
					<td colspan="4">
						<span class="glyphicon glyphicon-info-sign">返点公式：</span><span class="text-danger">返点金额 = 投注额(盈利) * 返点数 / 100</span>
						<span style="float:right"><button class="btn btn-success calc-btn" type="button">计算</button></span>
						<br/><span class="glyphicon glyphicon-info-sign">体育返点金额：</span><span class="toBlue sCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">真人返点金额：</span><span class="toBlue rCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">电子返点金额：</span><span class="toBlue eCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">彩票返点金额：</span><span class="toBlue lCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">六合彩返点金额：</span><span class="toBlue msCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">棋牌返点金额：</span><span class="toBlue cCash"></span>
						<br/><span class="glyphicon glyphicon-info-sign">电竞返点金额：</span><span class="toBlue esCash"></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default fui-close">关闭</button>
		<button class="btn btn-primary">保存</button>
	</div>
</div></div>
</form>
</script>
<script type="text/javascript">
requirejs(['template','jquery','Fui'],function(template){
	var $fanDianBtn=$("#proxy_rebate_record_list_fan_dian_id");
	$fanDianBtn.click(function(){
		var $table=$fanDianBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections');
		var count = list.length;
		if(count <= 0){
			layer.msg('请选择需要返点的代理!');
  			return false;
		}else{
			var accs = ''
				,accsName = ''
				,pids = ''
				,lbetAmount = 0
				,lprofitAmount = 0
				,rbetAmount = 0
				,rprofitAmount = 0
				,ebetAmount = 0
				,eprofitAmount = 0
				,sbetAmount = 0
				,sprofitAmount = 0
				,msbetAmount = 0
				,msprofitAmount = 0
				,cbetAmount = 0
				,cprofitAmount = 0
				,esbetAmount = 0
				,esprofitAmount = 0
				,it =null;
			for(var i=0;i<count;i++){
				it = list[i];
				if(it.status == 1){
					if(accs && it.accountId != accs){
						layer.msg('只能对同一个代理执行批量操作!');
						return false;
					}else{
						accs = it.accountId;
						accsName = it.account;
						if(it.lotteryBetAmount)lbetAmount += it.lotteryBetAmount;
						if(it.lotteryProfitAmount)lprofitAmount += it.lotteryProfitAmount;
						if(it.realBetAmount)rbetAmount += it.realBetAmount;
						if(it.realProfitAmount)rprofitAmount += it.realProfitAmount;
						if(it.egameBetAmount)ebetAmount += it.egameBetAmount;
						if(it.egameProfitAmount)eprofitAmount += it.egameProfitAmount;
						if(it.sportsBetAmount)sbetAmount += it.sportsBetAmount;
						if(it.sportsProfitAmount)sprofitAmount += it.sportsProfitAmount;
						if(it.markSixBetAmount)msbetAmount += it.markSixBetAmount;
						if(it.markSixProfitAmount)msprofitAmount += it.markSixProfitAmount;
						if(it.chessBetAmount)cbetAmount += it.cgameBetAmount;
						if(it.chessProfitAmount)cprofitAmount += it.chessProfitAmount;
						if(it.esportsProfitAmount)esprofitAmount += it.esportsProfitAmount;
						pids = pids + it.id + ',';
					}
				}
			}
			
			 if(accs){
				 var d={accountId:accs
					,pointIds:pids
					,accsName:accsName
					,lotteryBet:lbetAmount
					,lotteryProfit:lprofitAmount
					,realBet:rbetAmount
					,realProfit:rprofitAmount
					,egameBet:ebetAmount
					,egameProfit:eprofitAmount
					,sportsBet:sbetAmount
					,sportsProfit:sprofitAmount
					,chessBet:cbetAmount
					,chessProfit:cprofitAmount
					,markSixBet:msbetAmount
					,markSixProfit:msprofitAmount
					,esportsBet:esbetAmount
					,esportsProfit:esprofitAmount
				};
				Fui.openDialog({
					content:template('proxy_rebate_record_list_tpl', d)
					,callback:function(p){
						var $f=p.find("#proxy_rebate_record_to_profit_formId");
						$f.find(".calc-btn").click(function(){
							
							var lPoint = $f.find("[name='lotteryPoint']").val();
							var msPoint = $f.find("[name='markSixPoint']").val();
							var rPoint = $f.find("[name='realPoint']").val();
							var ePoint = $f.find("[name='egamePoint']").val();
							var sPoint = $f.find("[name='sportsPoint']").val();
							var cPoint = $f.find("[name='chessPoint']").val();
							var esPoint = $f.find("[name='esportsPoint']").val();
							var lotteryHandler = "lotteryBet";
							var sportsHandler = "sportsProfit";
							var markSixHandler = "markSixBet";
							if('${rebateType}' && 'profit'  == '${rebateType}'){
								lotteryHandler = "lotteryProfit";
								//sportsHandler = "sportsProfit";
								markSixHandler = "markSixProfit";
							}
							
							if(!lPoint || !rPoint || !ePoint || !sPoint || !msPoint || !cPoint || !esPoint){
								layer.msg('请填写完整的返点数再进行计算!');
							}else{
								$f.find('.lCash').html(Fui.toDecimal($f.find("[name='"+lotteryHandler+"']").val()*1 * lPoint*1 /100,3));
								$f.find('.rCash').html(Fui.toDecimal($f.find("[name='realProfit']").val()*1 * rPoint*1 /100,3));
								$f.find('.eCash').html(Fui.toDecimal($f.find("[name='egameProfit']").val()*1 * ePoint*1 /100,3));
								$f.find('.cCash').html(Fui.toDecimal($f.find("[name='chessProfit']").val()*1 * cPoint*1 /100,3));
								$f.find('.esCash').html(Fui.toDecimal($f.find("[name='esportsProfit']").val()*1 * esPoint*1 /100,3));
								$f.find('.sCash').html(Fui.toDecimal($f.find("[name='"+sportsHandler+"']").val()*1 * sPoint*1 /100,3));
								$f.find('.msCash').html(Fui.toDecimal($f.find("[name='"+markSixHandler+"']").val()*1 * msPoint*1 /100,3));
							}
						});
					}
				});
			}else{
				layer.msg('不能对已经返点的代理进行操作!');
				return false;
			}
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/prr/list.do',
		showPageSummary:true,
		showAllSummary:false,
		showFooter : true,
		columns : [{
			field : 'account',
			title : '代理账号',
			align : 'center',
			width : '80'
		}, {
			field : 'statDate',
			title : '统计日期',
			align : 'center',
			width : '80',
			formatter : Fui.formatDate,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			}
		}, {
			field : 'sportsBetAmount',
			title : '体育返点统计',
			align : 'center',
			width : '80',
			valign : 'bottom',
			formatter : sbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"sports");
			}
		}, {
			field : 'realBetAmount',
			title : '真人返点统计',
			align : 'center',
			width : '80',
			valign : 'bottom',
			formatter : rbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"real");
			}
		}, {
			field : 'egameBetAmount',
			title : '电子返点统计',
			align : 'center',
			width : '80',
			valign : 'bottom',
			formatter : ebaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"egame");
			}
		}, {
			field : 'chessBetAmount',
			title : '棋牌返点统计',
			align : 'center',
			width : '80',
			valign : 'bottom',
			formatter : cbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"chess");
			}
		}, {
			field : 'esportsBetAmount',
			title : '电竞返点统计',
			align : 'center',
			width : '80',
			valign : 'bottom',
			formatter : esbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"esports");
			}
		},{
			field : 'lotteryBetAmount',
			title : '彩票返点统计',
			align : 'center',
			width : '80',
			formatter : lbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"lottery");
			}
		},{
			field : 'lotteryBetAmount',
			title : '六合彩返点统计',
			align : 'center',
			width : '80',
			formatter : msbaFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getXjTotal(rows,"markSix");
			}
		}, {
			field : 'sumProfit',
			title : '总返点',
			align : 'center',
			width : '80',
			formatter : profitFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotalProfit(rows);
			}
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '80',
			formatter : statusFormatter
		},{
			checkbox : true
		}]
	});
	function lbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+(row.lotteryProfitAmount?row.lotteryProfitAmount.toFixed(2):'0.00')+'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+(row.lotteryBetAmount?row.lotteryBetAmount.toFixed(2):'0.00')+'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+(row.lotteryRebate?row.lotteryRebate.toFixed(2):'0.00')+'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+(row.lotteryRebateAmount?row.lotteryRebateAmount.toFixed(2):'0.00')+'</th></tr></table>';
		return col;
	}
	function msbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+(row.markSixProfitAmount?row.markSixProfitAmount.toFixed(2):'0.00')+'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+(row.markSixBetAmount?row.markSixBetAmount.toFixed(2):'0.00')+'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+(row.markSixRebate?row.markSixRebate.toFixed(2):'0.00')+'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+(row.markSixRebateAmount?row.markSixRebateAmount.toFixed(2):'0.00')+'</th></tr></table>';
		return col;
	}
	function rbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+(row.realProfitAmount?row.realProfitAmount.toFixed(2):'0.00') +'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+(row.realBetAmount?row.realBetAmount.toFixed(2):'0.00') +'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+(row.realRebate?row.realRebate.toFixed(2):'0.00') +'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+(row.realRebateAmount?row.realRebateAmount.toFixed(2):'0.00') +'</th></tr></table>';
		return col;
	}
	function ebaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+ (row.egameProfitAmount?row.egameProfitAmount.toFixed(2):'0.00') +'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+ (row.egameBetAmount?row.egameBetAmount.toFixed(2):'0.00') +'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+ (row.egameRebate?row.egameRebate.toFixed(2):'0.00') +'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+ (row.egameRebateAmount?row.egameRebateAmount.toFixed(2):'0.00') +'</th></tr></table>';
		return col;
	}
	function cbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+ (row.chessProfitAmount?row.chessProfitAmount.toFixed(2):'0.00') +'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+ (row.chessBetAmount?row.chessBetAmount.toFixed(2):'0.00') +'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+ (row.chessRebate?row.chessRebate.toFixed(2):'0.00') +'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+ (row.chessRebateAmount?row.chessRebateAmount.toFixed(2):'0.00') +'</th></tr></table>';
		return col;
	}
	function esbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+ (row.esportsProfitAmount?row.esportsProfitAmount.toFixed(2):'0.00') +'</th>';
		col += '<tr class="toRed"><th>投注</th><th>'+ (row.esportsBetAmount?row.esportsBetAmount.toFixed(2):'0.00') +'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+ (row.esportsRebate?row.esportsRebate.toFixed(2):'0.00') +'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+ (row.esportsRebateAmount?row.esportsRebateAmount.toFixed(2):'0.00') +'</th></tr></table>';
		return col;
	}
	function sbaFormatter(value, row, index) {
		var col = '';
		col += '<table><tr class="toRed"><th>盈利</th><th>'+ (row.sportsProfitAmount?row.sportsProfitAmount.toFixed(2):'0.00')+'</th>';
		col += '<table><tr class="toRed"><th>投注</th><th>'+ (row.sportsBetAmount?row.sportsBetAmount.toFixed(2):'0.00')+'</th>';
		col += '</tr><tr class="toBlue"><th>返点率</th><th>'+ (row.sportRebate?row.sportRebate.toFixed(2):'0.00')+'</th></tr>';
		col += '<tr class="toGreen"><th>返点</th><th>'+ (row.sportRebateAmount?row.sportRebateAmount.toFixed(2):'0.00')+'</th></tr></table>';
		return col;
	}
	function profitFormatter(value, row, index) {
		if(row.status==1){
			return "";
		}
		var total=0;
		if(row['lotteryRebateAmount'])total += row['lotteryRebateAmount'];
		if(row['realRebateAmount'])total += row['realRebateAmount'];
		if(row['egameRebateAmount'])total += row['egameRebateAmount'];
		if(row['sportRebateAmount'])total += row['sportRebateAmount'];
		if(row['markSixRebateAmount'])total += row['markSixRebateAmount'];
		if(row['chessRebateAmount'])total += row['chessRebateAmount'];
		if(row['esportsRebateAmount'])total += row['esportsRebateAmount'];
		return '<span class="toBlue"><b>'+total.toFixed(3)+""+'</b></span>';
	}
	function getTotalProfit(rows){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(r.status==1){
				continue;
			}
			if(r['lotteryRebateAmount'])total += r['lotteryRebateAmount'];
			if(r['realRebateAmount'])total += r['realRebateAmount'];
			if(r['egameRebateAmount'])total += r['egameRebateAmount'];
			if(r['sportRebateAmount'])total += r['sportRebateAmount'];
			if(r['markSixRebateAmount'])total += r['markSixRebateAmount'];
			if(r['chessRebateAmount'])total += r['chessRebateAmount'];
			if(r['esportsRebateAmount'])total += r['esportsRebateAmount'];
		}
		return total.toFixed(3);
	}
	function statusFormatter(value, row, index){
		if(value == 1){
			return '<span class="text-primary">未返</span>';
		}else if(value == 2){
			return '<span class="text-danger">已返</span>';
		}else{
			return '<span class="text-muted">未知</span>';
		}
	}
	//小计
	function getXjTotal(rows,type){
		var a = 0 ,b = 0 ,c = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(type == 'lottery'){
				if(r['lotteryBetAmount'])a += r['lotteryBetAmount'];
				if(r['lotteryProfitAmount'])b += r['lotteryProfitAmount'];
				if(r['lotteryRebateAmount'])c += r['lotteryRebateAmount'];
			}else if(type == 'real'){
				if(r['realBetAmount'])a += r['realBetAmount'];
				if(r['realProfitAmount'])b += r['realProfitAmount'];
				if(r['realRebateAmount'])c += r['realRebateAmount'];
			}else if(type == 'egame'){
				if(r['egameBetAmount'])a += r['egameBetAmount'];
				if(r['egameProfitAmount'])b += r['egameProfitAmount'];
				if(r['egameRebateAmount'])c += r['egameRebateAmount'];
			}else if(type == 'sports'){
				if(r['sportsBetAmount'])a += r['sportsBetAmount'];
				if(r['sportsProfitAmount'])b += r['sportsProfitAmount'];
				if(r['sportRebateAmount'])c += r['sportRebateAmount'];
			}else if(type == 'markSix'){
				if(r['markSixBetAmount'])a += r['markSixBetAmount'];
				if(r['markSixProfitAmount'])b += r['markSixProfitAmount'];
				if(r['markSixRebateAmount'])c += r['markSixRebateAmount'];
			}else if(type == 'chess'){
				if(r['chessBetAmount'])a += r['chessBetAmount'];
				if(r['chessProfitAmount'])b += r['chessProfitAmount'];
				if(r['chessRebateAmount'])c += r['chessRebateAmount'];
			}else if(type == 'esports'){
				if(r['esportsBetAmount'])a += r['esportsBetAmount'];
				if(r['esportsProfitAmount'])b += r['esportsProfitAmount'];
				if(r['esportsRebateAmount'])c += r['esportsRebateAmount'];
			}
			
		}
		return '<span class="toRed"><b>盈利:'+b.toFixed(2)+'</b></span><br/><span class="toRed"><b>投注:'+a.toFixed(2)+'</b></span><br/><span class="toGreen"><b>返点:'+c.toFixed(2)+'</b></span>'; 
	}
});
</script>