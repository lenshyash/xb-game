<%@ page language="java" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">会员反水</h4>
		</div>
		<div class="modal-body">
			<form class="fui-search table-tool" method="post" id="hui_yuan_fan_shui_que_ren_form_id">
				<div class="form-group fui-data-wrap">
					<div class="form-inline">
						<div class="input-group">
							<input type="text" class="form-control fui-date" name="startTime" placeholder="开始日期" value="${curDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
						<div class="input-group">
							<input type="text" class="form-control" name="account" placeholder="会员账号">
						</div>
						<button class="btn btn-primary fui-date-search">查询</button>
						<button class="btn btn-primary fanshui" type="button">确定返水</button>
					</div>
					<div class="form-inline mt5px">
						<div class="input-group">
							<input type="text" name="endTime" class="form-control fui-date" placeholder="结束日期" value="${curDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
						</div>
						<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
						<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
						<div class="form-group">
							<div class="input-group">
								<select class="form-control" name="betType" id="betType">
									<option value="">选择游戏类型</option>
									<option value="1">彩票</option>
									<option value="2">真人</option>
									<option value="3">电子游艺</option>
									<option value="4">体育</option>
									<option value="5">六合彩</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</form>
			<table class="fui-default-table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $form=$("#hui_yuan_fan_shui_que_ren_form_id"),$fanDianBtn=$form.find(".fanshui");
	$fanDianBtn.click(function(){
		var $table=$fanDianBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
			,count = list.length;
		if(count <= 0){
			layer.msg('请选择需要反水的记录!');
				return false;
		}else{
			layer.confirm('确定要对选择的记录进行返水？', function() {
				var accs='',startTime=$form.find("[name='startTime']").val()
					,endTime=$form.find("[name='endTime']").val()
				for(var i=0;i<count;i++){
					accs=accs+list[i].account+',';
				}
				accs=accs.substring(0,accs.length-1);
				layer.msg('反水命令已发出，由于反水比较耗性能，需要等一会才能完成');
				var $tabTable1=$(".fui-box.tab-pane.active").data("bootstrapTable");
				$.ajax({
					url:"${base}/agent/mrbr/dealRollAcc.do",
					data:{"endTime":endTime,"startTime":startTime,"accounts":accs},
					success:function(j){
						layer.msg(j.msg)
						$table.bootstrapTable('refresh');
						if($tabTable1.length){
							$tabTable1.bootstrapTable('refresh');
						}
					}
				});
			});
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/mrbr/rollList.do',
		height:560,
		columns : [ {
			checkbox : true
		},{
			field : 'account',
			title : '会员账户',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			field : 'betMoney',
			title : '投注金额',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter:cashFormatter
		}, {
			field : 'rollBackMoney',
			title : '反水金额',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter:fsdFormatter
		}]
	});
	function cashFormatter(value, row, index) {
		if(value>0){
			return Fui.toDecimal(value,2);	
		}else{
			return "0.00";
		}
	}
	function fsdFormatter(value, row, index){
		if(value){
			return Fui.toDecimal(value,4);	
		}else{
			return '0.0000';
		}
	}
});
		//确定返水
		function confire(){
			var accs = '';
			$('input[name="btSelectItem"]').each( function() {
		        if($(this).prop("checked")){
		        	accs = accs + $('#acc_'+$(this).attr('data-index')).text() + ','
		        }
		    });
			if(accs.indexOf(',')!=-1){
				accs=accs.substring(0,accs.length-1);
			 	var b = $('#begin').val();
			 	var e = $('#end').val();
				if(b && e){
					$.ajax({
						url:"${base}/agent/mrbr/dealRollAcc.do",
						data:{
							"accounts":accs,
							"startTime":b,
							"endTime":e
						},
						success:function(j){
							layer.alert(j.msg, {
						  		icon: 1,
						  		skin: 'layer-ext-moon'
						  	})
						  	search();
						}
					});
				}
			}else{
	        	layer.alert('请先选中需要返水的会员!', {
	  			  icon: 1,
	  			  skin: 'layer-ext-moon'
	  			})
	        }
		}
</script>
