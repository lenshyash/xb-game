<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="agent_profit_share_record_list_fform_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startDate" placeholder="开始日期" value="${startDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="代理账号">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" placeholder="结束日期" value="${endDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="form-group">
				<div class="input-group">
					<select class="form-control" name="status">
						<option value="">选择统计状态</option>
						<option value="1">未统计</option>
						<option value="4">已统计</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</form>
<table class="fui-default-table" id="agent_profit_share_record_list_table_id"></table>
<div style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">今日记录需要明天才能操作!请计算占成之前，先确保已经处理反水数据。</span>
	<br/><span class="text-danger">在“系统设置-><a href="${base }/agent/profitsharestrategy/index.do" class="open-tab">代理按日占成策略</a>”设置了占成策略，系统每天凌晨统计前2天的占成信息，您才能进行占成操作!</span>
</div>
<script type="text/javascript">
requirejs(['jquery','Fui'],function(template){
	var $profitShareBtn=$("#agent_profit_share_record_list_id")
		,$form=$("#agent_profit_share_record_list_fform_id");
	Fui.addBootstrapTable({
		url : '${base}/agent/profitsharerecord/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'account',
			title : '代理账户',
			align : 'center',
			valign:'middle',
			formatter:accountFormatter
		},{
			field : 'betDate',
			title : '下注日期',
			align : 'center',
			width : '100',
			valign:'middle',
			formatter : Fui.formatDate,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'betMoney',
			title : '总有效投注金额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"betMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.betMoney) ? Fui.toDecimal(aggsData.betMoney,4) : "0.0000";
			}
		},{
			field : 'winMoney',
			title : '总派奖金额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"winMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.winMoney) ? Fui.toDecimal(aggsData.winMoney,4) : "0.0000";
			}
		},{
			field : 'platformFee',
			title : '总平台抽取费',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"platformFee");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.platformFee) ? Fui.toDecimal(aggsData.platformFee,4) : "0.0000";
			}
		},{
			field : 'giveMoney',
			title : '总赠送金额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"giveMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.giveMoney) ? Fui.toDecimal(aggsData.giveMoney,4) : "0.0000";
			}
		},{
			field : 'rebateMoney',
			title : '会员反水',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"rebateMoney");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.rebateMoney) ? Fui.toDecimal(aggsData.rebateMoney,4) : "0.0000";
			}
		},{
			field : 'thirdDeposit',
			title : '三方充值总额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"thirdDeposit");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.thirdDeposit) ? Fui.toDecimal(aggsData.thirdDeposit,4) : "0.0000";
			}
		}, {
			field : 'profitShareStatus',
			title : '统计状态',
			align : 'center',
			valign:'middle',
			formatter : statusFormatter
		}]
	});
	function accountFormatter(value, row, index) {
		if(row.backwaterStatus==4){
			return '<span class="unbatch_r" tindex="'+index+'">'+value+'</span>';
		}
		return value;
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return Fui.toDecimal(total,2);
	}
	function numFormatter(value, row, index){
		var content = "";
		if(value || value==0){
			content = Fui.toDecimal(value,2);
		}
		return '<a class="open-dialog" href="${base}/agent/profitsharerecord/info.do?recordId='+row.id+'">'+content+'</a>';
	}
	function statusFormatter(value, row, index) {
		if(value==1 || value==2){
			return '<span class="label label-danger">未统计</span>';
		}else if(value==4){
			return '<span class="label label-success">已统计</span>';
		}
		return "";
	}
});
</script>