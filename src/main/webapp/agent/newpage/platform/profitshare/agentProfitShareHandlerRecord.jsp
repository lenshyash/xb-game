<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<button class="btn btn-success pull-right" style="position:relative;top:50px;" id="agent_profit_share_handler_record_list_confirm_id">确认发放</button>
<form class="fui-search table-tool" method="post" id="agent_profit_share_handler_record_list_fform_id">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control fui-date" name="startDate" placeholder="开始日期" value="${startDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='today'>今日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='yesterday'>昨日</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisWeek'>本周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayBefore'>前一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthBefore'>前一月</button>
			<div class="input-group">
				<input type="text" class="form-control" name="account" placeholder="代理账号">
			</div>
			<div class="input-group">
				<input type="text" class="form-control" name="agentName" placeholder="上级代理">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
		<div class="form-inline mt5px">
			<div class="input-group">
				<input type="text" name="endDate" class="form-control fui-date" placeholder="结束日期" value="${endDate }"> <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
			</div>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastWeek'>上周</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='thisMonth'>本月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='lastMonth'>上月</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='dayAfter'>后一天</button>
			<button type="button" class="btn btn-default fui-date-btn" data-target='monthAfter'>后一月</button>
			<div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" name="parentNames" placeholder="代理及线下查询">
				</div>
				<div class="input-group">
					<select class="form-control" name="status">
						<option value="">选择占成记录状态</option>
						<option value="1">未发放</option>
						<option value="2">已发放</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="right-btn-table"><table class="fui-default-table" id="agent_profit_share_handler_record_list_table_id"></table></div>
<div style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">今日记录需要明天才能操作!请计算占成之前，先确保已经处理反水数据。</span>
	<br/><span class="text-danger">在“系统设置-><a href="${base }/agent/profitsharestrategy/index.do" class="open-tab">代理按日占成策略</a>”设置了占成策略，系统每天凌晨统计前2天的占成信息，您才能进行占成操作!</span>
</div>
<script type="text/javascript">
requirejs(['jquery','Fui'],function(template){
	var $profitShareBtn=$("#agent_profit_share_handler_record_list_confirm_id")
		,$form=$("#agent_profit_share_handler_record_list_fform_id");
	$profitShareBtn.click(function(){
		var $table=$profitShareBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
			,count = list.length,ids=''
			,moneys='',money='' , fsa=null,it=null;
		if(count <= 0){
			layer.msg('请选择需要确认的记录!');
  			return false;
		}else{
			for(var i=0;i<count;i++){
				it = list[i];
				if(it.id){
		        	fsa=$table.find("input[name='confirmAmount"+it.id+"']");
		        	if(fsa.length){
		        		money=fsa.val();
		        		if(!money || !/^[0-9]+(\.[0-9]{1,4})?$/.test(money)){
							layer.msg("请输入正确的金额");
							fsa.focus();
							return false;
						}
		        		ids = ids+it.id+",";
			        	moneys=moneys+money+",";
		        	}
	        		}
			}
			ids=ids.substring(0,ids.length-1);
			moneys=moneys.substring(0,moneys.length-1);
			layer.confirm('确定要对选择的记录进行发放？', function() {
				$.ajax({
					url:"${base}/agent/profitsharehandlerrecord/doConfirm.do",
					data:{
						ids:ids,
						moneys:moneys
					},
					success:function(j){
						layer.msg(j.msg)
						$table.bootstrapTable('refresh');
					}
				});
			});
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/profitsharehandlerrecord/list.do',
		showPageSummary:true,
		showAllSummary:true,
		showFooter : true,
		columns : [{
			field : 'account',
			title : '代理账户',
			align : 'center',
			valign:'middle'
		},{
			field : 'agentName',
			title : '上级代理',
			align : 'center',
			valign:'middle'
		},{
			field : 'parentNames',
			title : '代理线',
			align : 'center',
			valign:'middle',
			formatter:accountFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return "小计:";
			},
			allSummaryFormat:function(rows,aggsData){
				return "总计:";
			}
		},{
			field : 'money',
			title : '占成金额',
			align : 'center',
			valign:'middle',
			formatter : profitMoneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,"money");
			},
			allSummaryFormat:function(rows,aggsData){
				return (aggsData && aggsData.money) ? Fui.toDecimal(aggsData.money,4) : "0.0000";
			}
		}, {
			field : 'status',
			title : '发放状态',
			align : 'center',
			width : '80',
			valign:'middle',
			formatter : statusFormatter
		},{
			field : 'createDatetime',
			title : '创建时间',
			align : 'center',
			width : '150',
			valign:'middle',
			formatter : Fui.formatDatetime
		},{
			field : 'modifyDatetime',
			title : '最后变动时间',
			align : 'center',
			width : '150',
			valign:'middle',
			formatter : Fui.formatDatetime
		}, {
			field : 'operator',
			title : '操作员',
			valign:'middle',
			align : 'center'
		}, {
			field : 'remark',
			title : '备注',
			align : 'center',
			valign : 'middle',
			formatter : remarkFormatter
		},{
			valign:'middle',
			checkbox : true
		}]
	});
	function remarkFormatter(value, row, index) {
		if (!value) {
			return "";
		}
		if (value.length < 20) {
			return value;
		}
		return [ '<a class="open-text" dialog-text="'+value+'" dialog-title="备注详情" title="'+value+'">', '</span>' ].join(value.substr(0,20)+"...");
	}
	function accountFormatter(value, row, index) {
		if(!value){
			return "-";
		}
		return value.substring(1,value.length-1).split(",").join(">");
	}
	//返水金额
	function profitMoneyFormatter(value, row, index){
		var bwm = row["money"];
		var desc = "";
		if(row["status"] == 2 ){
			return Fui.toDecimal(bwm,4);
		}
		if(bwm){
			return '<div class="form-inline"><input class="form-control" name="confirmAmount'+row.id+'" type="text" value="'+Fui.toDecimal(bwm,4)+'" ></div>';
		}
		return '<input class="form-control" name="confirmAmount'+row.id+'" type="text" value="" >';

	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return Fui.toDecimal(total,2);
	}
	function statusFormatter(value, row, index) {
		if(value==1){
			return '<span class="label label-danger">未发放</span>';
		}else if(value==2){
			return '<span class="label label-success">已发放</span>';
		}
		return "";
	}
});
</script>