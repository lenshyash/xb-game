<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/agent/profitsharestrategy/update.do" class="form-submit">
<div class="modal-dialog"><input type="hidden" name="id" value="${profitShare.id }">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改代理按日占成策略</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td width="20%" class="text-right media-middle">类型</td>
						<td width="80%"><select class="form-control" name="type">
							<c:if test="${lotteryOnoff eq 'on' }"><option value="1" <c:if test="${profitShare.type eq 1 }">selected="selected" </c:if>>彩票</option> </c:if>
							<c:if test="${zrOnoff eq 'on' }"><option value="2" <c:if test="${profitShare.type eq 2 }">selected="selected" </c:if>>真人</option> </c:if>
							<c:if test="${dzOnoff eq 'on' }"><option value="3" <c:if test="${profitShare.type eq 3 }">selected="selected" </c:if>>电子</option> </c:if>
							<c:if test="${sportOnoff eq 'on' }"><option value="4" <c:if test="${profitShare.type eq 4 }">selected="selected" </c:if>>体育</option> </c:if>
							<c:if test="${thirdSportOnoff eq 'on' }"><option value="5" <c:if test="${profitShare.type eq 5 }">selected="selected" </c:if>>三方体育</option> </c:if>
							<c:if test="${thirdLotteryOnoff eq 'on' }"><option value="6" <c:if test="${profitShare.type eq 6 }">selected="selected" </c:if>>三方彩票</option> </c:if>
							<c:if test="${chessOnoff eq 'on' }"><option value="7" <c:if test="${profitShare.type eq 7 }">selected="selected" </c:if>>棋牌</option> </c:if>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">平台费率：</td>
						<td><input name="platformFeeRate" class="form-control required" type="text" value="${profitShare.platformFeeRate }" placeholder="请填写平台费率，默认为0"/></td>
					</tr>
					<tr>
						<td class="text-right media-middle">费率类型：</td>
						<td><select name="platformFeeType" class="form-control">
							<option value="1" <c:if test="${profitShare.platformFeeType eq 1 }">selected="selected" </c:if> >盈利</option>
							<option value="2" <c:if test="${profitShare.platformFeeType eq 2 }">selected="selected" </c:if> >流水</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">状态：</td>
						<td><select name="status" class="form-control">
							<option value="2" <c:if test="${profitShare.status eq 2 }">selected="selected" </c:if> >启用</option>
							<option value="1" <c:if test="${profitShare.status eq 1 }">selected="selected" </c:if>>停用</option>
						</select></td>
					</tr>
					<tr>
						<td class="text-right media-middle">描述：</td>
						<td><textarea name="memo" class="form-control">${profitShare.memo }</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>