<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close fui-close" aria-hidden="true">&times;</button>
		<h4 class="modal-title">占成明细</h4>
	</div>
	<div class="modal-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td class="text-center" colspan="3">代理:<span>${row.account }</span></td>
					<td class="text-center" colspan="3">日期:<span>${row.betDate }</span></td>
				</tr>
				<tr>
					<td width="10%" class="text-right" >&nbsp;</td>
					<td class="text-center" >投注</td>
					<td class="text-center" >派奖</td>
					<td class="text-center" >盈利</td>
					<td class="text-center" >反水</td>
					<td class="text-center" >费率</td>
				</tr>
				<c:if test="${sportOnoff eq 'on' }">
				<tr>
					<td width="10%" class="text-right toRed" >体育：</td>
					<td width ="20%">	
						<div class ="input-group">		
							<input value="${row.sportBetMoney }" type="text" disabled="disabled" class="form-control" placeholder="体育投注">
						</div>	
					</td>
					<td width="20%">
						<div class="input-group">
							<input value="${row.sportWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="体育派奖">
						</div>
					</td>
					<td width="20%">
						<div class="input-group">
							<input value="${row.sportBetMoney-row.sportWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="体育盈利">
						</div>					
					</td>
					<td width="15%">
						<div class="input-group">
							<input value="${row.sportRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="体育反水">
						</div>					
					</td>
					<td width="15%">
						<div class="input-group">
							<input value="${row.sportPlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="体育费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<c:if test="${zrOnoff eq 'on' }">
				<tr>
					<td class="text-right toBlue">真人：</td>
					<td>
						<div class="input-group">
							<input value="${row.liveBetMoney}" type="text" disabled="disabled"class="form-control" placeholder="真人投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.liveWinMoney}" type="text" disabled="disabled"class="form-control" placeholder="真人派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input value="${row.liveBetMoney-row.liveWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="真人盈利">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.liveRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="真人反水">
						</div>					
					</td>
					<td>
						<div class="input-group">
							<input value="${row.livePlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="真人费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<c:if test="${dzOnoff eq 'on' }">
				<tr>
					<td class="text-right toGreen">电子：</td>
					<td>
						<div class="input-group">
							<input value="${row.egameBetMoney}" type="text" disabled="disabled"class="form-control" placeholder="电子投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.egameWinMoney}" type="text" disabled="disabled"class="form-control" placeholder="电子派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input value="${row.egameBetMoney-row.egameWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="电子盈利">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.egameRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="电子反水">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.egamePlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="电子费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<c:if test="${lotteryOnoff eq 'on' }">
				<tr>
					<td class="text-right toRed">彩票：</td>
					<td>
						<div class="input-group">
							<input value="${row.lotteryBetMoney}" type="text" disabled="disabled"class="form-control" placeholder="彩票投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.lotteryWinMoney}" type="text" disabled="disabled"class="form-control" placeholder="彩票派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input value="${row.lotteryBetMoney-row.lotteryWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="彩票盈利">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.lotteryRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="彩票反水">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.lotteryPlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="彩票费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<c:if test="${thirdSportOnoff eq 'on' }">
				<tr>
					<td class="text-right toRed">三方体育：</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdSportBetMoney}" type="text" disabled="disabled"class="form-control" placeholder="三方体育投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdSportWinMoney}" type="text" disabled="disabled"class="form-control" placeholder="三方体育派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input value="${row.thirdSportBetMoney-row.thirdSportWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="三方体育盈利">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.thirdSportRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="三方体育反水">
						</div>					
					</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdSportPlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="三方体育费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<c:if test="${thirdLotteryOnoff eq 'on' }">
				<tr>
					<td class="text-right toRed">三方彩票：</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdLotteryBetMoney}" type="text" disabled="disabled"class="form-control" placeholder="三方彩票投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdLotteryWinMoney}" type="text" disabled="disabled"class="form-control" placeholder="三方彩票派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input name="sportsPoint" value="${row.thirdLotteryBetMoney-row.thirdLotteryWinMoney }" type="text" disabled="disabled" class="form-control" placeholder="三方彩票盈利">
						</div>					
					</td>
					<td>
						<div class="input-group">
							<input value="${row.thirdLotteryRebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="三方彩票反水">
						</div>					
					</td>
					<td >
						<div class="input-group">
							<input value="${row.thirdLotteryPlatformFee }" type="text" disabled="disabled" class="form-control" placeholder="三方彩票费率">
						</div>					
					</td>
				</tr>
				</c:if>
				<tr>
					<td class="text-right toRed">总计：</td>
					<td>
						<div class="input-group">
							<input value="${row.betMoney}" type="text" disabled="disabled"class="form-control" placeholder="总投注">
						</div>
					</td>
					<td>
						<div class="input-group">
							<input value="${row.winMoney}" type="text" disabled="disabled"class="form-control" placeholder="总派奖">
						</div>
					</td>
					<td >
						<div class="input-group">
							<input value="${row.betMoney-row.winMoney }" type="text" disabled="disabled" class="form-control" placeholder="总盈利">
						</div>					
					</td>
					<td>
						<div class="input-group">
							<input value="${row.rebateMoney }" type="text" disabled="disabled" class="form-control" placeholder="总反水">
						</div>					
					</td>
					<td>
						<div class="input-group">
							<input value="${row.platformFee }" type="text" disabled="disabled" class="form-control" placeholder="总费率">
						</div>					
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default fui-close">关闭</button>
	</div>
</div></div>