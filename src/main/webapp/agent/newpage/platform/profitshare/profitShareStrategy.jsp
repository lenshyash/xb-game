<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post">
	<div class="form-inline">
		<div class="form-group">
			<div class="input-group">
				<select class="form-control" name="type">
					<option value="">选择游戏类型</option>
					<option value="1">彩票</option>
					<option value="2">真人</option>
					<option value="3">电子</option>
					<option value="4">体育</option>
					<option value="5">三方体育</option>
					<option value="6">三方彩票</option>
					<option value="7">棋牌</option>
				</select>
			</div>
		</div>
		<button class="btn btn-primary fui-date-search">查询</button>
		<button class="btn btn-primary open-dialog cached" type="button" url="${base}/agent/profitsharestrategy/add.do">新增</button>
	</div>
</form>
<table class="fui-default-table"></table>
<div style="color: red;" class="platform">备注：代理占成是根据当天的有效投注计算
	<br>系统每天凌晨统计前一天代理线的有效投注（已派奖的投注金额）；统计完才能进行占成统计
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	Fui.addBootstrapTable({
		url : '${base}/agent/profitsharestrategy/list.do',
		columns : [{
			field : 'type',
			title : '游戏类型',
			align : 'center',
			width : '80',
			formatter:gameTypeFormatter
		},
		{
			field : 'platformFeeRate',
			title : '平台费率',
			align : 'center',
			formatter:bfbFormatter
		},
		{
			field : 'platformFeeType',
			title : '费率类型',
			align : 'center',
			formatter:feeTypeFormatter
		},
		 {
			field : 'memo',
			title : '描述',
			align : 'center',
			valign : 'middle',
			formatter : memoFormatter
		},{
			field : 'status',
			title : '状态',
			align : 'center',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			width : '80',
			formatter : operateFormatter
		}]
	});
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/profitsharestrategy/closeOrOpen.do?id="+row.id+"&status="});
	}
	function memoFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>8){
			return value.substr(0, 8)+'。。';
		}else{
			return value;
		}
	}
	function gameTypeFormatter(value, row, index) {
		if(value==1){
			return "彩票";
		}else if(value==2){
			return "真人";
		}else if(value==3){
			return "电子";
		}else if(value==4){
			return "体育";
		}else if(value==5){
			return "三方体育";
		}else if(value==6){
			return "三方彩票";
		}else if(value==7){
			return "棋牌";
		}
		return "";
	}
	function feeTypeFormatter(value, row, index) {
		if(value==1){
			return "盈利";
		}else if(value==2){
			return "流水";
		}
		return "";
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="${base}/agent/profitsharestrategy/modify.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="${base}/agent/profitsharestrategy/delete.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
});
</script>