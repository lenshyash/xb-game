<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form class="fui-search table-tool" method="post" id="agent_profit_share_handler_form_id">
<input type="hidden" name="account" value="${account }">
<input type="hidden" name="startDate" value="${startDate }">
<input type="hidden" name="endDate" value="${endDate }">
<button class="btn btn-success" id="agent_profit_share_confirm_btn_id">确认占成</button>
</form>
<table class="fui-default-table" id="agent_profit_share_handler_talbe_id"></table>
<div style="padding: 10px;">
	<span class="glyphicon glyphicon-info-sign">温馨提示：</span><span class="text-danger">代理占成=（总盈利-总平台费率-总会员反水-三方充值总额X三方费率点数）X自身占成数</span>
</div>
<script type="text/javascript">
requirejs(['jquery'],function(){
	var $confirmBtn=$("#agent_profit_share_confirm_btn_id")
		,$form=$("#agent_profit_share_handler_form_id");
	$confirmBtn.click(function(){
		var $table=$confirmBtn.parents(".fui-box").data("bootstrapTable");
		if(!$table || !$table.length){
			return false;
		}
		var list = $table.bootstrapTable('getSelections')
			,count = list.length
			,moneys='',accountIds='',money='' , fsa=null,it=null;
		if(count <= 0){
			layer.msg('请选择需要确认占成的记录!');
  			return false;
		}else{
			for(var i=0;i<count;i++){
				it = list[i];
				if(it.accountId){
			        	fsa=$table.find("input[name='depositFeeAmount"+it.id+"']");
			        	if(fsa.length){
			        		money=fsa.val();
			        		if((!money || !/^[0-9]+(\.[0-9]{1,4})?$/.test(money)) && money != 0){
							layer.msg("请输入正确的点数");
							fsa.focus();
							return false;
						}
				        	moneys=moneys+money+",";
				        	accountIds=accountIds+it.accountId+",";
				        	
			        	}
	        		}
			}
			moneys=moneys.substring(0,moneys.length-1);
			accountIds=accountIds.substring(0,accountIds.length-1);
			layer.confirm('确定要对选择的记录确认占成？', function() {
				$.ajax({
					url:"${base}/agent/profitsharerecord/doProfitShare.do",
					data:{accountIds:accountIds
						,fees:moneys
						,startDate:$form.find("[name='startDate']").val()
						,endDate:$form.find("[name='endDate']").val()},
					success:function(j){
						layer.msg(j.msg)
						$table.bootstrapTable('refresh');
					}
				});
			});
		}
	});
	Fui.addBootstrapTable({
		url : '${base}/agent/profitsharerecord/handlerList.do',
		columns : [{
			valign:'middle',
			checkbox : true
		},{
			field : 'account',
			title : '注单账户',
			align : 'center',
			valign:'middle',
		},{
			field : 'betMoney',
			title : '有效投注金额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter
		},{
			field : 'winMoney',
			title : '中奖金额',
			align : 'center',
			valign:'middle',
			formatter : numFormatter
		},{
			title : '盈利金额',
			align : 'center',
			valign:'middle',
			formatter : ylFormatter
		},{
			field : 'rebateMoney',
			title : '会员反水',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
		},{
			field : 'platformFee',
			title : '平台费率',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
		},{
			field : 'thirdDeposit',
			title : '第三方充值',
			align : 'center',
			valign:'middle',
			formatter : numFormatter,
		}, {
			title : '第三方充值费率点数',
			align : 'center',
			formatter : fsdFormatter,
		}]
	});
	function ylFormatter(value, row, index){
		return Fui.toDecimal((row.betMoney-row.winMoney),2);
	}
	function numFormatter(value, row, index){
		if(value){
			return Fui.toDecimal(value,2);
		}
		return "";
	}
	//三方充值费率
	function fsdFormatter(value, row, index){
		return '<input class="form-control" name="depositFeeAmount'+row.id+'" type="text" value="0" >';

	}
});
</script>