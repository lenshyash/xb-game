define(['jquery','Fui'],function(){
	// 开奖提示语音
	var deposit=null
		,withdraw=null
		,cacheName='SoundTable'
		,waringCycle=10
		,sourceFlag='on'
		,sourceType='1'
		,waringCycleKey='waringCycle_'+baseInfo.account
		,sourceFlagKey='sourceFlag_'+baseInfo.account
		,sourceTypeKey='sourceType_'+baseInfo.account
		,checkNavCountTimer;
	$("#home_soundicon").click(function(){
		var it=$(this);
		if(it.hasClass("glyphicon-volume-up")){
			Fui.setCache(cacheName,{key:sourceFlagKey,value:'off'});
			it.removeClass("glyphicon-volume-up").addClass("glyphicon-volume-off").attr("value", "off").attr("title", "打开声音");
			sourceFlag='off';
		}else{
			Fui.setCache(cacheName,{key:sourceFlagKey,remove:true});
			it.removeClass("glyphicon-volume-off").addClass("glyphicon-volume-up").attr("value", "on").attr("title", "关闭声音");
			sourceFlag='on';
		}
	});
	var cacheSound = Fui.setCache(cacheName);
	if(cacheSound){
		if(cacheSound[waringCycleKey]){
			waringCycle=cacheSound[waringCycleKey];
			$("#home_waringCycle").val(waringCycle);
		}
		if(cacheSound[sourceFlagKey]){
			sourceFlag=cacheSound[sourceFlagKey];
			if(sourceFlag=='off'){
				$("#home_soundicon").removeClass("glyphicon-volume-up").addClass("glyphicon-volume-off").attr("value", "off").attr("title", "打开声音");
			}
		}
		if(cacheSound[sourceTypeKey]){
			sourceType=cacheSound[sourceTypeKey];
			$("#home_sourceType").val(sourceType);
		}
	}
	$("#home_waringCycle").change(function(){
		waringCycle=$(this).val();
		Fui.setCache(cacheName,{key:waringCycleKey,value:waringCycle});
		clearTimeout(checkNavCountTimer);
		checkNavCountTimer=setTimeout(checknavcount, 1000 * waringCycle);
	});
	$("#home_sourceType").change(function(){
		sourceType=$(this).val();
		Fui.setCache(cacheName,{key:sourceTypeKey,value:sourceType});
		changeSound();
	});
	function playDepositSound(){
		if(sourceFlag=='on'){
			try{
				if(!deposit){
					deposit=new Audio;
					deposit.type="audio/mp3";
					deposit.src=baseInfo.baseUrl+getDepositSoundPath();
				}
				deposit.pause(),
				deposit.currentTime=0,
				deposit.play()
			}catch(a){}
		}
		
	}
	
	function changeSound(){
		if(!deposit){
			deposit=new Audio;
			deposit.type="audio/mp3";
			deposit.src=baseInfo.baseUrl+getDepositSoundPath();
		}else{
			deposit.src=baseInfo.baseUrl+getDepositSoundPath();
		}
		
		if(!withdraw){
			withdraw=new Audio;
			withdraw.type="audio/mp3";
			withdraw.src=baseInfo.baseUrl+getWithDrawSoundPath();
		}else{
			withdraw.src=baseInfo.baseUrl+getWithDrawSoundPath();
		}
	}
	function playWithdrawSound(){
		if(sourceFlag=='on'){
			try{
				if(!withdraw){
					withdraw=new Audio;
					withdraw.type="audio/mp3";
					withdraw.src=baseInfo.baseUrl+getWithDrawSoundPath();
				}
				withdraw.pause(),
				withdraw.currentTime=0,
				withdraw.play()
			}catch(a){}
		}
	}
	
	function getWithDrawSoundPath(){
		var sp = "/common/sound/";
		if(sourceType && sourceType == 2){
			sp += "new/1.wav";
		}else if(sourceType && sourceType == 3){
			sp += "ring/yy_withdrawal.mp3";
		}else{
			sp += "1.wav";
		}
		return sp;
	}
	
	function getDepositSoundPath(){
		var sp = "/common/sound/";
		if(sourceType && sourceType == 2){
			sp += "new/2.wav";
		}else if(sourceType && sourceType == 3){
			sp += "ring/yy_recharge.mp3";
		}else{
			sp += "2.mp3";
		}
		return sp;
	}
	var success_play = false;
	function checknavcount() {
		$.ajax({
			url : baseInfo.baseUrl+'/agent/checknavcount.do',
			success : function(data) {
				var depositCount = data.depositCount;
				var withdrawCount = data.withdrawCount;
				var warningUser = data.warningUser;
				$("#online_span").html(data.onlineUser);
				$("#deposit_span").html(depositCount);
				$("#withdraw_span").html(withdrawCount);
				$("#warning_span").html(warningUser);
				if (depositCount > 0) {
					setTimeout(playDepositSound, 1000);
				}
				if (withdrawCount > 0) {
					setTimeout(playWithdrawSound, 5000);
				}
				var qhtml = '';
				var len = 0;
				if(data.quotos && data.quotos.length >0){
					var q = null;
					len = data.quotos.length;
					if(len > 5){
						qhtml +='<li class="dropdown nav-item" id="real_money_menu_header_li">';
						qhtml +='		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">';
						//第一层名称
						qhtml +='真人余额';
						qhtml +='		<span class="caret"></span>';
						qhtml +='</a>';
						qhtml +='		<ul class="dropdown-menu">';
						for (var i = 0; i < data.quotos.length; i++) {
							q = data.quotos[i];
							//2循环
							qhtml +='		<li><a href="javascript:void(0);" title="'+q.name+'">'+q.name+' <span class="badge">'+q.value.remainingQuota+'</span></a></li>';
						}
						qhtml +='		</ul>';
						qhtml +='	</li>';
					}else{
						for (var i = 0; i < data.quotos.length; i++) {
							q = data.quotos[i];
							qhtml += '<li class="nav-item"><a href="javascript:void(0);" title="'+q.name+'">'+q.name+' <span class="badge">'+q.value.remainingQuota+'</span></a></li>';
						}
					}
				}
				if(qhtml){
					$("#show_quoto").html(qhtml);
					if(len>5){
						$("#real_money_menu_header_li").hover(function() {
					     $(this).addClass('open');
					    }, function() {
					     $(this).removeClass('open');
					    });
					}
				}
			},
			errorFn:function(data,ceipstate){
				if(ceipstate==4){
					clearTimeout(checkNavCountTimer);
				}
			}
		});
		checkNavCountTimer=setTimeout(checknavcount, 1000 * waringCycle);
	}
	checknavcount();
	//获取租户公告
	var fmain= $("#fui_footer_id").find(".fui-fmain"),stopScroll=false,preTop=0,scrollNoticesTimer=null,stopNum=0;
	function getNotices(){
		$.ajax({
			url : baseInfo.baseUrl+'/agent/getNotice.do',
			success : function(data) {
				if(data && $.isArray(data)){
					var h='';
					for(var s in data){
						h=h+"<div class='notices' title='点击查看详情'>"+data[s]+"</div>";
					}
					fmain.html(h+h+h).mouseover(function(){stopScroll=true}).mouseout(function(){if(stopNum==0){stopScroll=false}});
					fmain.find(".notices").click(function(){
						var it=$(this),layerIndex=layer.open({
						  type: 1
						  ,content: '<div style="padding:20px 30px 10px;color:red;font-size:20px;">'+it.html()+'</div>'
						  ,btn: '关 闭'
						  ,btnAlign: 'c' //按钮居中
						  ,shade: 0 //不显示遮罩
						  ,yes: function(){
						    layer.close(layerIndex);
						  }
						});
					});
					clearInterval(scrollNoticesTimer);
					scrollNoticesTimer=setInterval(scrollNotices,100);
					$(window).resize();
				}
			}
		});
		setTimeout(getNotices, 600000);//10分钟获取一次
		$("#stopScorll").click(function(){
			if(stopNum == 0){
				stopNum = 1;
				stopScroll = true
				$("#stopScorll").text('开启滚动').css("color","#a0a3a3")
				fmain.prop("scrollTop",27);
			}else{
				stopNum = 0;
				stopScroll = false
				$("#stopScorll").text('停止滚动').css("color","red")
			}

		})
	}
	function scrollNotices(){
		if(stopScroll==true)
			return;
	  	preTop=fmain.prop("scrollTop");
	  	fmain.prop("scrollTop",preTop+1);
	  	if(preTop==fmain.prop("scrollTop")){
	  		fmain.prop("scrollTop",27);
	  	}
	}
	getNotices();
	
	function showChat() {
		$.ajax({
			url : baseInfo.baseUrl+'/agent/chatlist.do',
			success : function(data) {
				var qhtml = '';
				var url  = data.domain
				var str = url.substring(0,5)
				if(str != 'https'){
					data.domain = 'https://' + data.domain
				}
				if(data.rows && data.rows.length >0){
					var q = null;
					qhtml +='<li class="dropdown nav-item" id="chat_menu_header_li">';
					qhtml +='<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">';
					//第一层名称
					qhtml +='<span style="color: red;font-weight: bold;font-size: 18px;">聊天室列表</span>';
					qhtml +='<span class="caret"></span>';
					qhtml +='</a>';
					qhtml +='<ul class="dropdown-menu chat-list">';
					for (var i = 0; i < data.rows.length; i++) {
						q = data.rows[i];
						//2循环http://xbchat.com:8080/chat/admin/agent/dologin?platformId=x001_1&name=x001_1admin&passwo rd=0qXjO7
						qhtml +='<li><a  target="_blank"  href="'+data.domain+'/admin/agent/login?plId='+q.platformId+'&name='+q.platformId+'admin&pass='+q.platformPass+'&admin=true" title="点击跳转后台">'+q.name+'(<span style="color:#03A9F4">'+q.platformId+'</span>)</a></li>';
					}
					qhtml +='</ul>';
					qhtml +='</li>';
				}
				if(qhtml){
					$("#show_chat").html(qhtml);
					$("#chat_menu_header_li").hover(function() {
				     $(this).addClass('open');
				    }, function() {
				     $(this).removeClass('open');
				    });
				}
			}
		});
	}
	showChat();

	Fui.config.loginSuccessFn=function(){
		checknavcount();
		showChat();
		
	}
	if(baseInfo.mustUpdatePwd){
		layer.confirm('您的密码极其简单，请马上修改密码！', {icon: 3,title:'提示'},function(index){
		    layer.close(index);
		    Fui.openDialog({url:baseInfo.baseUrl+'/agent/modifyPwd.do'});
		  }
		);
	}
});