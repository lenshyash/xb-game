define(['template','Chart','jquery','bootstrap','Fui'],function(template,Chart){
	var source='<tr class="appendtr">'
		+'<td class="text-center media-middle" rowspan="{{data.length}}">奖品和概率</td>'
		+'{{each data as award i}}'
		+'{{if i > 0}}<tr class="appendtr">{{/if}}'
		+'<td colspan="2" class="form-inline">'
		+'<div class="form-group" style="color: {{colors[i]}}"> {{award}}</div>'
		+'<div class="form-group"><select name="awardType"class="form-control"><option value="1" selected>不中奖</option><option value="2">现金</option><option value="4">积分</option><option value="3">奖品</option></select></div>'
		+'<div class="form-group"><div class="input-group"><div class="input-group-addon">名称</div><input type="text" name="awardName"class="form-control" style="width:100px"></input></div></div>'
		+'<div class="form-group hidden awardProudctWrap"><div class="input-group"><div class="input-group-addon">奖品</div><select name="productId"class="form-control awardProudct">'
		+'{{each products as p j}}<option value="{{p.id}}">{{p.productName}}</option>{{/each}}'
		+'</select></div></div>'
		+'<div class="form-group hidden awardMoneyWrap"><div class="input-group"><div class="input-group-addon">面值</div><input type="text" name="awardMoney"class="form-control awardMoney" style="width:60px"></div></div>'
		+'<div class="form-group"><div class="input-group"><div class="input-group-addon">概率基数</div><input type="text" name="chance" class="form-control" style="width:60px"></div></div>'
		+'<div class="form-group">概率：<span class="text-primary chance_span"></span></div>'
		+'</td></tr>{{/each}}';
	return {
		render:function(products,awardDatas){
			var $form=$("#active_set_rule_formId_id"),
				$table=$form.find("table")
				,$awardCount=$table.find(".awardCount")
				,$awardImg=$table.find(".awardImgCav")
				,awardImgChart=null;
			products=$.parseJSON(products);
			if($.type(awardDatas) === 'string'){
				awardDatas=$.parseJSON(awardDatas);
			}
			
			//奖项类型事件
			$table.on("change","select[name='awardType']",function(){
				var $it=$(this),val=$it.val(),$parent=$it.parents("td:first");
				if (val == 2 || val == 4) {
					$parent.find(".awardProudctWrap").addClass("hidden");
					$parent.find(".awardMoneyWrap").removeClass("hidden");
				} else if (val == 3) {
					$parent.find(".awardProudctWrap").removeClass("hidden");
					$parent.find(".awardMoneyWrap").addClass("hidden");
				} else {
					$parent.find(".awardProudctWrap").addClass("hidden");
					$parent.find(".awardMoneyWrap").addClass("hidden");
				}
			});
			
			
			var h='';
			for(var i=5;i<21;i++){
				h+='<option value="'+i+'">'+i+'</option>';
			}
			$awardCount.html(h).change(function(){
				var val = $(this).val()
					,cavDatas = []
					,cavLables = []
					,cavColors = []
					,lableFix = "奖项";
				for (var i = 1; i <= val; i++) {
					cavLables.push(lableFix + i)
					cavDatas.push(1);
					cavColors.push(randomColor());
				}
				initAwardImg(cavLables, cavDatas, cavColors);
				$(".appendtr",$table).remove();
				$table.append(template.compile(source)({"data" : cavLables,"colors" : cavColors,products:products}));
				setAwardData();
				balance();
			});
			$awardCount.val(awardDatas.length||5).change();
			//设置活动的奖项内容
			function setAwardData() {
				if (awardDatas) {
					var award = null
						,$appendtrs=$table.find(".appendtr")
						,$tr=null
						,trlen=$appendtrs.length;
					for (var i = 0; i < awardDatas.length; i++) {
						if(i>=trlen){
							return;
						}
						award = awardDatas[i];
						award.awardIndex = i;
						$tr=$appendtrs.eq(i);
						$tr.find("[name='awardType']").val(award.awardType).change();
						$tr.find("[name='awardName']").val(award.awardName);
						$tr.find("[name='productId']").val(award.productId);
						$tr.find("[name='awardMoney']").val(award.awardValue);
						$tr.find("[name='chance']").val(award.chance);
					}
				}
			}
			//绑定自动结算概率
			$table.on("blur","input[name='chance']",function(){
				var $it=$(this),val=$it.val();
				if (!isNaN(val)) {
					$it.val(Math.abs(val));
					balance();
				}
			});
			//初始化环形图
			function initAwardImg(lables, datas, colors) {
				if (!awardImgChart) {
					awardImgChart = new Chart($awardImg, {
						type : 'doughnut',
						data : {
							labels : lables,
							datasets : [{data : datas,backgroundColor : colors}],
							options : {}
						}
					});
				} else {
					awardImgChart.data.labels = lables;
					awardImgChart.data.datasets[0].data = datas;
					awardImgChart.data.datasets[0].backgroundColor = colors;
					awardImgChart.update();
				}
			}
		
			//随机颜色
			function randomColor() {
				return '#'+ ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).substr(-6);
			}
			//通过奖项基数计算概率
			function balance() {
				var awardCount = $awardCount.val()
					,total = 0
					,single = 0
					,$appendtrs=$table.find(".appendtr")
					,$tr=null;
				if (awardCount) {
					for (var i = 0; i < awardCount; i++) {
						$tr=$appendtrs.eq(i);
						single = $tr.find("input[name='chance']").val();
						if (single) {
							total = accAdd(total, single * 1);
						}
					}
					for (var i = 0; i < awardCount; i++) {
						$tr=$appendtrs.eq(i);
						single = $tr.find("input[name='chance']").val();
						if (single) {
							$tr.find(".chance_span").html(Fui.toDecimal(accDiv(single * 100, total),2)+ "%");
						} else {
							$tr.find(".chance_span").html("0.00%");
						}
					}
				}
			}
			//除法
			function accDiv(arg1, arg2) {
				var t1 = 0, t2 = 0, r1, r2;
				try {
					t1 = arg1.toString().split(".")[1].length
				} catch (e) {
				}
				try {
					t2 = arg2.toString().split(".")[1].length
				} catch (e) {
				}
				with (Math) {
					r1 = Number(arg1.toString().replace(".", ""))
					r2 = Number(arg2.toString().replace(".", ""))
					return accMul((r1 / r2), pow(10, t2 - t1));
				}
			}
			//加法   
			function accAdd(arg1, arg2) {
				var r1, r2, m;
				try {
					r1 = arg1.toString().split(".")[1].length
				} catch (e) {
					r1 = 0
				}
				try {
					r2 = arg2.toString().split(".")[1].length
				} catch (e) {
					r2 = 0
				}
				m = Math.pow(10, Math.max(r1, r2))
				return (arg1 * m + arg2 * m) / m
			}
		
			//乘法  
			function accMul(arg1, arg2) {
				var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
				try {
					m += s1.split(".")[1].length
				} catch (e) {
				}
				try {
					m += s2.split(".")[1].length
				} catch (e) {
				}
				return Number(s1.replace(".", "")) * Number(s2.replace(".", ""))
						/ Math.pow(10, m)
			}
			$form.data("paramFn",function(){
				var data=getAwardDatas();
				var params={
					"activeId" : $form.find("[name='activeId']").val(),
					"data" : JSON.stringify(data)
				};
				return params;
			}).data("resultCallback",function(json){
				if(json.success){
					awardDatas=getAwardDatas();
				}
			});
			//得到页面的奖项内容集合
			function getAwardDatas() {
				var awards = []
					,award = null
					,$appendtrs=$table.find(".appendtr")
					,$tr=null
					,trlen=$appendtrs.length;
		
				if (!trlen) {
					return awards;
				}
		
				for (var i = 0; i < trlen; i++) {
					$tr=$appendtrs.eq(i);
					award = {};
					award.awardIndex = i + 1;
					award.awardName = $tr.find("[name='awardName']").val();
					award.awardType = $tr.find("[name='awardType']").val();
					award.chance = $tr.find("[name='chance']").val();
					award.awardValue = $tr.find("[name='awardMoney']").val();
					award.productId = $tr.find("[name='productId']").val();
					awards.push(award);
				}
				return awards;
			}
		}
	}
});