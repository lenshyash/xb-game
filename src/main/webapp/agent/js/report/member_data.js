define(['jquery','bootstrap','Fui','fui_table'],function(){
	return {
		render:function(sport,real,dianZi,markSix,chess){
			var $form=$("#report_member_data_form_id");
			var td1=td2=td3=td4=td5=null;
			function addTables(data){
				if(td1){
					td1.bootstrapTable('load', data);
					td2.bootstrapTable('load', data);
					td3.bootstrapTable('load', data);
					td4.bootstrapTable('load', data);
					td5.bootstrapTable('load', data);
					return;
				}
				var giftColumns=[{
					field : 'giveTotal',
					title : '存款赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : depositGiftFormatter
				}, {
					field : 'giveRegisterTotal',
					title : '注册赠送总计',
					align : 'center',
					valign : 'middle',
					formatter : registerGiftFormatter
				},{
					field : 'activeAwardTotal',
					title : '活动中奖总计',
					align : 'center',
					valign : 'middle',
					formatter : activeAwardFormatter
				}]
				,betColumns=[{
						field : 'lotteryTotal',
						title : '彩票下注总计',
						align : 'center',
						formatter : cpxzFormatter
//					}, {
//						field : 'lotteryAward',
//						title : '彩票派奖总计',
//						align : 'center',
//						formatter : cpxzFormatter
//					},{
//						field : 'lotteryProfit',
//						title : '彩票输赢',
//						align : 'center',
//						valign : 'middle',
//						formatter : moneyFormatter
				}];
				
					giftColumns.push({
						field : 'rebateTotal',
						title : '反水总计',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					});
					if (markSix === 'on') {
						betColumns.push({
							field : 'markSixTotal',
							title : '六合下注总计',
							align : 'center',
							formatter : lhcFormatter
						});
//						betColumns.push({
//							field : 'markSixAward',
//							title : '六合派奖总计',
//							align : 'center',
//							formatter : moneyFormatter
//						});
//						betColumns.push({
//							field : 'markSixProfit',
//							title : '六合输赢',
//							align : 'center',
//							formatter : moneyFormatter
//						});
					}
					if (sport === 'on') {
						betColumns.push({
							field : 'sportTotal',
							title : '体育下注',
							align : 'center',
							formatter : tyFormatter
						});
//						betColumns.push({
//							field : 'sportAward',
//							title : '体育派奖',
//							align : 'center',
//							formatter : moneyFormatter
//						});
//						betColumns.push({
//							field : 'sportProfit',
//							title : '体育输赢',
//							align : 'center',
//							formatter : moneyFormatter
//						});
					}
					if (real === 'on') {
						betColumns.push({
							field : 'realTotal',
							title : '真人下注',
							align : 'center',
							formatter : realFormatter
						});
//						betColumns.push({
//							field : 'realAward',
//							title : '真人派奖',
//							align : 'center',
//							formatter : moneyFormatter
//						});
//						betColumns.push({
//							field : 'realProfit',
//							title : '真人输赢',
//							align : 'center',
//							formatter : moneyFormatter
//						});
					}
					if (dianZi === 'on') {
						betColumns.push({
							field : 'egameTotal',
							title : '电子下注',
							align : 'center',
							formatter : electronFormatter
						});
//						betColumns.push({
//							field : 'egameAward',
//							title : '电子派奖',
//							align : 'center',
//							formatter : moneyFormatter
//						});
//						betColumns.push({
//							field : 'egameProfit',
//							title : '电子输赢',
//							align : 'center',
//							formatter : moneyFormatter
//						});
					}
					if (chess === 'on') {
						betColumns.push({
							field : 'chessTotal',
							title : '棋牌下注',
							align : 'center',
							formatter : chessFormatter
						});
//						betColumns.push({
//							field : 'egameAward',
//							title : '电子派奖',
//							align : 'center',
//							formatter : moneyFormatter
//						});
//						betColumns.push({
//							field : 'egameProfit',
//							title : '电子输赢',
//							align : 'center',
//							formatter : moneyFormatter
//						});
					}
					betColumns.push({
						title : '所有下注总计',
						align : 'center',
						formatter : function(value, row, index) {
							var v=0;
							if(row.lotteryTotal){
								v=v+row.lotteryTotal;
							}
							if(row.markSixTotal){
								v=v+row.markSixTotal;
							}
							if(row.sportTotal){
								v=v+row.sportTotal;
							}
							if(row.realTotal){
								v=v+row.realTotal;
							}
							if(row.egameTotal){
								v=v+row.egameTotal;
							}
							if(row.chessTotal){
								v=v+row.chessTotal;
							}
							return '<span class="text-primary">'+Fui.toDecimal(v,2)+'</span>';
						}
					});
				
					giftColumns.push({
						field : 'rebateAgentTotal',
						title : '代理返点总计',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					});
				
				td1=$("#report_member_data_tb_base").bootstrapTable({
					data:data,
					striped : true,
					columns : [{
						field : 'account',
						title : '账号',
						align : 'center'
					}, {
						field : 'agentName',
						title : '所属代理',
						align : 'center'
					}, {
						field : 'registerDatetime',
						title : '注册时间',
						align : 'center',
						formatter : Fui.formatDatetime
					}, {
						field : 'balance',
						title : '余额',
						align : 'center',
						formatter : moneyFormatter
					}, {
						field : 'betNum',
						title : '当前打码量',
						align : 'center',
						formatter : betNumFormatter
					}, {
						field : 'drawNeed',
						title : '出款所需打码量',
						align : 'center',
						formatter : betNumFormatter
					}]
				});
				td2=$("#report_member_data_tb_deposit").bootstrapTable({data:data,striped : true,columns :[{
						field : 'depositTotal',
						title : '在线存款总计',
						align : 'center',
						formatter : cunkuanFormatter
					}, {
						field : 'manualDepositTotal',
						title : '手动加款总计',
						align : 'center',
						formatter : sdjkFormatter
					}, {
						field : 'depositHandlerArtificial',
						title : '手动确认充值金额',
						align : 'center',
						formatter : handlerDepositFormatter
					}, {
						field : 'lastDepositMoney',
						title : '最后充值金额',
						align : 'center',
						formatter : cunkuanFormatter
					}, {
						field : 'lastDepositTime',
						title : '最后充值时间',
						align : 'center',
						formatter : Fui.formatDatetime
					}, {
						field : 'lastDepositDesc',
						title : '最后充值简介',
						align : 'center'
				}]});
				td3=$("#report_member_data_tb_draw").bootstrapTable({data:data,striped : true,columns :[{
						field : 'withdrawTotal',
						title : '在线提款总计',
						align : 'center',
						formatter : qukuanFormatter
					}, {
						field : 'manualWithdrawTotal',
						title : '手动扣款总计',
						align : 'center',
						formatter : sdkkFormatter
					}, {
						field : 'withdrawGiveTotal',
						title : '扣除彩金总计',
						align : 'center',
						formatter : sdkkFormatter
					}, {
						field : 'lastDrawMoney',
						title : '最后提款金额',
						align : 'center',
						formatter : qukuanFormatter
					}, {
						field : 'lastDrawTime',
						title : '最后提款时间',
						align : 'center',
						formatter : Fui.formatDatetime
					}, {
						field : 'lastDrawDesc',
						title : '最后提款简介',
						align : 'center'
				}]});
				td4=$("#report_member_data_tb_gift").bootstrapTable({data:data,striped : true,columns : giftColumns});
				
					td5=$("#report_member_data_tb_bet").bootstrapTable({data:data,striped : true,columns :betColumns});
				
			}
			$form.find(".search-btn").click(function(){
				var it=$(this);
				if(it.prop("disabled")==true){
					return false;
				}
				it.prop("disabled",true).html('<img src="'+baseInfo.baseUrl+'/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
				var account=$form.find("[name='account']").val();
				if(!account){
					it.prop("disabled",false).html('查询');
					layer.msg("请输入会员帐号");
					return false;
				}
				$.ajax({
					url : baseInfo.baseUrl+"/agent/report/memberData/list.do",
					data : $form.serialize(),
					success : function(result) {
						it.prop("disabled",false).html('查询');
						if(result.msg){
							layer.msg(result.msg);
							return;
						}
						addTables([result]);
					},
					errorFn:function(){
						it.prop("disabled",false).html('查询');
					}
				});
				return false;
			});
			if($form.find("[name='account']").val()){
				$form.find(".search-btn").click();
			}
			if(!$form.parents(".fui-box").data("open-tabl1")){
				$form.parents(".fui-box").data("open-tabl1",1);
				$form.parents(".fui-box").on("click",".open-tab1",function(){
					var it = $(this)
						,url=it.attr("href")||it.attr("url")
						,option
						,item=null;
					url = unescape(url).replaceTmBySelector($form);
					item=Fui.navbar.getMenuItem(url)
					if(item){
						option = {
							id:it.attr("id")||item.id
							,title:item.title
							,url:url
							,refresh:true
						};
					}else{
						option = {
							id:it.attr("id")
							,title:it.attr("title")||it.text()
							,url:url
							,refresh:true
						};
					}
					Fui.tab.tabAdd(option);
					event.preventDefault();
				});
			}
			function moneyFormatter(value, row, index) {
				if (value === undefined) {
					return "<span class='text-primary'>0.00</span>";
				}
				if (value > 0) {
					return '<span class="text-danger">'+Fui.toDecimal(value,2)+'</span>';
				}
				return '<span class="text-primary">'+Fui.toDecimal(value,2)+'</span>';
			}
			//打码量记录
			function betNumFormatter(value,row,index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/betnumrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//存款关联
			function cunkuanFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/payonlinerd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
		
			//取款关联
			function qukuanFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memdrawrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//手动加款
			function sdjkFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&param=sdjk'>"+moneyFormatter(value)+"</a>";
			}
			//手动扣款
			function sdkkFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&param=sdkk'>"+moneyFormatter(value)+"</a>";
			}
			//彩票下注
			function cpxzFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/gameRecord/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//体育下注关联
			function tyFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/sports/record/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//lhc下注关联
			function lhcFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/gameRecord/lhcIndex.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//真人游戏记录 
			function realFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/rbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//电子
			function electronFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/rebet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}'>"+moneyFormatter(value)+"</a>";
			}
			//棋牌
			function chessFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/rbet/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&gameType=12'>"+moneyFormatter(value)+"</a>";
			}
			//手动确认关联
			function handlerDepositFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/payonlinerd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&handlerType=1'>"+moneyFormatter(value)+"</a>";
			}
			
			//充值赠送关联
			function depositGiftFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&moneyType=80'>"+moneyFormatter(value)+"</a>";
			}
			
			//注册赠送关联
			function registerGiftFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&moneyType=79'>"+moneyFormatter(value)+"</a>";
			}
			
			//活动中奖关联
			function activeAwardFormatter(value, row, index){
				return "<a class='open-tab1' href='"+baseInfo.baseUrl+"/agent/finance/memmnyrd/index.do?account={[name=account]:}&begin={[name=begin]:}&end={[name=end]:}&moneyType=18'>"+moneyFormatter(value)+"</a>";
			}
		}
	}
});