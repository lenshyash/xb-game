define(['jquery','bootstrap','Fui','fui_table'],function(){
	var listUrl = baseInfo.baseUrl+"/agent/report/risk/list.do"
		,accountUrl = baseInfo.baseUrl+"/agent/report/risk/account.do"
		,conversionUrl = baseInfo.baseUrl+"/agent/report/risk/conversion.do"
		,dcCols = [ {
			field : 'account',
			title : '用户账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter,
			pageSummaryFormat : function(rows, aggsData) {
				return "小计:";
			},
			allSummaryFormat : function(rows, aggsData) {
				return "总计:";
			}
		}, {
			field : 'userName',
			title : '用户姓名',
			align : 'center',
			valign : 'middle'
		},{
			field : 'accountType',
			title : '用户类型',
			align : 'center',
			valign : 'middle',
			formatter : typeFormatter
		}, {
			field : 'money',
			title : '现有余额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			field : 'agentName',
			title : '所属上级',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'newTotalDeposit',
			title : '历史总存',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'newTotalWithdraw',
			title : '历史总提',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'createDatetime',
			title : '注册时间',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){
				return Fui.formatDatetime(value);
			}
		}, {
			field : 'lastLoginDatetime',
			title : '最后登录',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter :function(value, row, index){
				return Fui.formatDatetime(value);
			}
		}, {
			field : 'depositTimes',
			title : '存款次数',
			align : 'center',
			valign : 'middle',
			sortable:true,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositTimes');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositTimesTotal');
			}
		}, {
			field : 'withdrawTimes',
			title : '取款次数',
			align : 'center',
			valign : 'middle',
			sortable:true,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'withdrawTimes');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'withdrawTimesTotal');
			}
		}, {
			field : 'depositTotal',
			title : '存款额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositAmountTotal');
			}
		}, {
			field : 'withdrawTotal',
			title : '取款额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'withdrawTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'withdrawAmountTotal');
			}
		}, {
			field : 'depositArtificialTotal',
			title : '手动加款额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'depositArtificialTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'depositArtificialAmountTotal');
			}
		}, {
			field : 'withdrawArtificialTotal',
			title : '手动扣款额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,'withdrawArtificialTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,'withdrawArtificialAmountTotal');
			}
		}, {
			field : 'profit',
			title : '投注盈亏',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter
		}, {
			title : '存取款盈亏额',
			align : 'center',
			valign : 'middle',
			sortable:true,
			formatter : dcMoneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getCQBunkoPage(rows,'depositTotal','withdrawTotal','depositArtificialTotal','withdrawArtificialTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return getCQBunkoAll(aggsData,'depositAmountTotal','withdrawAmountTotal','depositArtificialAmountTotal','withdrawArtificialAmountTotal');
			}
		}];
	var accountCols = [{
		field : 'account',
		title : '用户账号',
		align : 'center',
		valign : 'middle',
		formatter : accountFormatter
	},{
		field : 'accountType',
		title : '用户类型',
		align : 'center',
		valign : 'middle',
		formatter : typeFormatter
	}, {
		field : 'createDatetime',
		title : '注册时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'lastLoginDatetime',
		title : '最后登录时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		title : '未登录时间',
		align : 'center',
		valign : 'middle',
		formatter : lastDateFormatter
	}, {
		field : 'registerIp',
		title : '注册IP',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'lastLoginIp',
		title : '最后登录IP',
		align : 'center',
		valign : 'middle'
	} ];
	var conversionCols = [{
		field : 'statDate',
		title : '日期',
		align : 'center',
		valign : 'middle',
		pageSummaryFormat : function(rows, aggsData) {
			return "小计:";
		},
		allSummaryFormat : function(rows, aggsData) {
			return "总计:";
		}
	},{
		field : 'registerNum',
		title : '注册总数',
		align : 'center',
		valign : 'middle',
		pageSummaryFormat:function(rows,aggsData){
			var xj = 0;
			if(rows){
				for (var i = 0; i < rows.length; i++) {
					xj += rows[i].registerNum;
				}
			}
			return xj;
		},
		allSummaryFormat:function(rows,aggsData){
			if(aggsData && aggsData["registerNumTotal"]){
				return aggsData["registerNumTotal"];
			}
			return 0;
		}
	}, {
		field : 'appNum',
		title : 'APP下载量',
		align : 'center',
		valign : 'middle',
		pageSummaryFormat:function(rows,aggsData){
			var xj = 0;
			if(rows){
				for (var i = 0; i < rows.length; i++) {
					xj += rows[i].appNum;
				}
			}
			return xj;
		},
		allSummaryFormat:function(rows,aggsData){
			if(aggsData && aggsData["appNumTotal"]){
				return aggsData["appNumTotal"];
			}
			return 0;
		}
	}];
	var userType={
			1 : "会员",
			2 : "租户超级管理员",
			3 : "租户管理员",
			4 : "代理",
			5 : "总代理"
		};
	function typeFormatter(value, row, index) {
		return userType[value];
	}
	
	function moneyFormatter(value) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(Fui.toDecimal(value,2));
		}
		return [ '<span class="text-primary">', '</span>' ].join(Fui.toDecimal(value,2));
	}
	function dcMoneyFormatter(value, row, index) {
		return moneyFormatter(row.depositTotal - row.withdrawTotal+row.depositArtificialTotal-row.withdrawArtificialTotal);
	}
	function getTotal(rows,itemKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[itemKey]){
				continue;
			}
			total += r[itemKey];
		}
		return moneyFormatter(total.toFixed(2));
	}
	function getBunkoTotal(rows,prefix){
		var data = {};
		for(var i=0;i<rows.length;i++){
			var r =rows[i];
			if(data[prefix+'BetTotal'] && r[prefix+'BetTotal']){
				data[prefix+'BetTotal'] += r[prefix+'BetTotal'];
			}else if(r[prefix+'BetTotal']){
				data[prefix+'BetTotal'] = r[prefix+'BetTotal'];
			}
			if(data[prefix+'WinTotal'] && r[prefix+'WinTotal']){
				data[prefix+'WinTotal'] += r[prefix+'WinTotal'];
			}else if(r[prefix+'WinTotal']){
				data[prefix+'WinTotal'] = r[prefix+'WinTotal'];
			}
		}
		return data;
	}
	function getWinRateTotal(rows,prefix){
		var data = {};
		for(var i=0;i<rows.length;i++){
			var r =rows[i];
			if(data[prefix+'BetTimesTotal'] && r[prefix+'BetTimesTotal']){
				data[prefix+'BetTimesTotal'] += r[prefix+'BetTimesTotal'];
			}else if(r[prefix+'BetTimesTotal']){
				data[prefix+'BetTimesTotal'] = r[prefix+'BetTimesTotal'];
			}
			if(data[prefix+'WinTimesTotal'] && r[prefix+'WinTimesTotal']){
				data[prefix+'WinTimesTotal'] += r[prefix+'WinTimesTotal'];
			}else if(r[prefix+'WinTimesTotal']){
				data[prefix+'WinTimesTotal'] = r[prefix+'WinTimesTotal'];
			}
		}
		return data;
	}
	function totalFormatter(aggsData,key){
		var money = "0.00";
		if(aggsData && aggsData[key]){
			money = aggsData[key].toFixed(2);
		}
		return moneyFormatter(money);
	}
	
	function getBunkoPage(rows,fstKey,secKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[fstKey] || !r[secKey]){
				continue;
			}
			total += r[fstKey] - r[secKey];
		}
		return moneyFormatter(total);
	}
	
	function getCQBunkoPage(rows,fstKey,secKey,thdKey,fourKey){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(!r[fstKey] || !r[secKey]|| !r[thdKey]|| !r[fourKey]){
				continue;
			}
			total += r[fstKey] - r[secKey]+ r[thdKey]-r[fourKey];
		}
		return moneyFormatter(total);
	}
	
	function getCQBunkoAll(aggsData,fstKey,secKey,thdKey,fourKey){
		var total = 0;
		var fstNum = 0;
		var secNum = 0;
		var thdNum = 0;
		var fourNum = 0;
		if(!aggsData){
			return total;
		}
		if(aggsData[fstKey]){
			fstNum =  aggsData[fstKey];
		}
		if(aggsData[secKey]){
			secNum = aggsData[secKey];
		}
		if(aggsData[thdKey]){
			thdNum = aggsData[thdKey];
		}
		if(aggsData[fourKey]){
			fourNum = aggsData[fourKey];
		}
		total = fstNum - secNum+thdNum-fourNum;
		return moneyFormatter(total);
	}
	
	function getBunkoAll(aggsData,fstKey,secKey){
		var total = 0;
		var fstNum = 0;
		var secNum = 0;
		if(!aggsData){
			return total;
		}
		if(!aggsData[fstKey] && !aggsData[secKey]){
			return total;
		}
		if(aggsData[fstKey]){
			fstNum = aggsData[fstKey];
		}
		if(aggsData[secKey]){
			secNum = aggsData[secKey];
		}
		total = fstNum - secNum;
		return moneyFormatter(total);
	}
	
	function lastDateFormatter(value, row, index) {
		var nowTimes = new Date().getTime();
		var lastLoginDatetime  = row.lastLoginDatetime;
		if(!lastLoginDatetime){
			lastLoginDatetime = row.createDatetime;
		}
		return ~~((nowTimes - lastLoginDatetime) / (24 * 60 * 60 * 1000)) +"天";
	}
	var gameNameMap={
			"lottery":"彩票",
			"sports":"体育",
			"real":"真人",
			"egame":"电子",
			"thirdSports":"三方体育",
			"thirdLottery":"三方彩票",
			"chess":"棋牌",
			"esports":"电竞",
			"hunter":"捕鱼王",
			"markSix":"六合",
			"sysLottery":"系统彩", 
			"sfMarkSix":"十分六合"};
	if(replaceFont.length){
		gameNameMap.sysLottery = replaceFont;
	}
	function initCols(prefix){
		var gameName = gameNameMap[prefix];
		return [ {
			field : 'account',
			title : '用户账号',
			align : 'center',
			valign : 'middle',
			formatter : accountFormatter,
			pageSummaryFormat : function(rows, aggsData) {
				return "小计:";
			},
			allSummaryFormat : function(rows, aggsData) {
				return "总计:";
			}
		},{
			field : 'userName',
			title : '用户姓名',
			align : 'center',
			valign : 'middle'
		},  {
			field : prefix+'BetTotal',
			title : gameName+'投注额',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,prefix+'BetTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,prefix+'BetTotal');
			}
		}, {
			field : prefix+'WinTotal',
			title : gameName+'中奖额',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,prefix+'WinTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,prefix+'WinTotal');
			}
		}, {
			field : prefix+'Bunko',
			title : gameName+'盈亏额',
			valign : 'middle',
			sortable:true,
			formatter : function(value, row, index){
				return bunkoFormatter(value, row, prefix);
			},
			pageSummaryFormat:function(rows,aggsData){
				return bunkoFormatter('', getBunkoTotal(rows,prefix), prefix);
			},
			allSummaryFormat:function(rows,aggsData){
				return getBunkoAll(aggsData,prefix+'BetTotal',prefix+'WinTotal');
			}
		}, {
			field : prefix+'BetTimesTotal',
			title : gameName+'投注次数',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,prefix+'BetTimesTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,prefix+'BetTimesTotal');
			}
		}, {
			field : prefix+'WinTimesTotal',
			title : gameName+'中奖次数',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,prefix+'WinTimesTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,prefix+'WinTimesTotal');
			}
		}, {
			field : prefix+'WinRate',
			title : gameName+'中奖率',
			valign : 'middle',
			sortable:true,
			formatter : function(value, row, index){
				return handlerWinRate(value, row, prefix);
			},
			pageSummaryFormat:function(rows,aggsData){
				return handlerWinRate('', getWinRateTotal(rows,prefix), prefix);
			},
			allSummaryFormat:function(rows,aggsData){
				try{
					var data = {};
					data[prefix+'BetTimesTotal']=aggsData[prefix+'BetTimesTotal'];
					data[prefix+'WinTimesTotal']=aggsData[prefix+'WinTimesTotal'];
					return handlerWinRate('', data, prefix);
				}catch(e){}
			}
		}, {
			field : prefix+'RebateTotal',
			title : gameName+'反水',
			valign : 'middle',
			sortable:true,
			formatter : moneyFormatter,
			pageSummaryFormat:function(rows,aggsData){
				return getTotal(rows,prefix+'RebateTotal');;
			},
			allSummaryFormat:function(rows,aggsData){
				return totalFormatter(aggsData,prefix+'RebateTotal');
			}
		} ];
	}
	function bunkoFormatter(value, row, prefix){
		var betTotal = row[prefix+'BetTotal']
			,winTotal = row[prefix+'WinTotal'];
		if(!betTotal){
			betTotal = 0;
		}
		if(!winTotal){
			winTotal = 0;
		}
		return moneyFormatter(betTotal - winTotal);
	}
	
	function handlerWinRate(value, row, prefix){
		try{
			var betTimes = row[prefix+'BetTimesTotal']
				,winTimes = row[prefix+'WinTimesTotal'];
			if(!betTimes){
				betTotal = 0;
			}
			if(!winTimes){
				winTotal = 0;
			}
			if (betTimes == 0 && winTimes > 0) {
				return "<span class='text-danger'>100%</span>";
			}
			if (winTimes == 0) {
				return "<span class='text-success'>0</span>"
			}
			var winRate = Fui.toDecimal((~~(winTimes / betTimes*10000))/100 ,2);//乘于10000取整再除于100
			if (winRate > 70) {
				return "<span class='text-danger'>"+winRate+"%</span>";
			}
			if (winRate > 50) {
				return "<span class='text-warning'>"+winRate+"%</span>";
			}
			if (winRate == 0) {
				return "<span class='text-primary'>0</span>";
			}
			return "<span class='text-success'>"+winRate+"%</span>";
		}catch(e){}
	}
	
	function getOptions(name){
		var options = {};
		if (name == "cunqu") {
			options= {
				columns : dcCols,
				url : listUrl+"?type=cunqu",
				pageNumber : 1,
				sortName:undefined,
				showPageSummary : true,
				showAllSummary : true,
				showFooter : true
			};
		} else if (name == "yunying") {
			options= {
				columns : accountCols,
				url : accountUrl,
				pageNumber : 1,
				sortName:undefined,
				showPageSummary : false,
				showAllSummary : false,
				showFooter : false
			};
		} else if (name == "conversion") {
			options= {
				columns : conversionCols,
				url : conversionUrl,
				pageNumber : 1,
				sortName:undefined,
				showPageSummary : true,
				showAllSummary : true,
				showFooter : true
			};
		} else {
			options= {
				columns : initCols(name),
				url : listUrl+"?type="+name,
				pageNumber : 1,
				sortName:undefined,
				showPageSummary : true,
				showAllSummary : true,
				showFooter : true,
			};
		} 
		return options;
	}
	
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/member/manager/view.do?id=',row.accountId!=null?row.accountId:row.id,'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');
	}
	
	function liActive(name){
		$("#report_risk_nav_tabs_warp_id li").removeClass("active");
		$("#"+name).addClass("active");
	}
	return {
		render:function(gameOnOff,initName){
			if(!initName){
				initName = "yunying";
			}
			var $form=$("#report_risk_form_id")
				,$tabs=$("#report_risk_nav_tabs_warp_id")
				,curBootstrapTable=null;
			for(var key in gameOnOff){
				$tabs.find("."+key).removeClass("hidden");
			}
			$tabs.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				var options = getOptions(name);
				curBootstrapTable.bootstrapTable('refreshOptions', options);
			});
			liActive(initName);
			var initOptions = getOptions(initName);
			accountUrl = initOptions['url'];
			accountCols = initOptions['columns'];
			Fui.addBootstrapTable({
				url : accountUrl,
				showPageSummary : true,
				showAllSummary : true,
				showFooter : true,
				columns : accountCols,
				onLoadSuccess : function(d,r) {
					if (r && r.msg) {
						layer.msg(r.msg);
					}
					$form.find(".search-btn").prop("disabled",false).html('查询');
					return false;
				},
				queryParams:function(params){
					$form.find(".search-btn").prop("disabled",true).html('<img src="'+baseInfo.baseUrl+'/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
					return params;
				}
				,onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});