define(['jquery','bootstrap','Fui','bootstrap_editable'],function(){
	function typeFormmater(value,row,index){
		switch(value-0){
		case 1:return "AG";
		case 2:return "BBIN";
		case 3:return "MG";
		case 4:return "QT";
		case 5:return "ALLBET";
		case 6:return "PT";
		case 7:return "OG";
		case 8:return "DS";
		case 9:return "CQ9";
		case 10:return "IBC";
		case 11:return "JDB";
		case 92:return "TTG";
		case 95:return "MW";
		case 96:return "ISB";
        case 99:return "M8";
        case 98:return "BG";
        case 97:return "VR";
        case 12:return "KY";
		}
	}
	function moneyFormmater(value, row, index){
		if(value !=undefined){
			return value.toFixed(2);
		}
		return '-'
	}
	function dangerMoneyFormmater(value, row, index){
		if(value !=undefined){
			return '<span class="label label-danger">'+value.toFixed(2)+'</span>';
		}
		return '-'
	}
	function warnMoneyFormmater(value, row, index){
		if(value !=undefined){
			return '<span class="label label-warning">'+value.toFixed(2)+'</span>';
		}
		return '-'
	}
	function successMoneyFormmater(value, row, index){
		if(value !=undefined){
			return '<span class="label label-success">'+value.toFixed(2)+'</span>';
		}
		return '-'
	}
	function primaryMoneyFormmater(value, row, index){
		if(value !=undefined){
			return '<span class="label label-primary">'+value.toFixed(2)+'</span>';
		}
		return '-'
	}
	function infoMoneyFormmater(value, row, index){
		if(value !=undefined){
			return '<span class="label label-info">'+value.toFixed(2)+'</span>';
		}
		return '-'
	}
	function getTotal(rows,bet,win){
		var total = 0;
		for(var i=0;i<rows.length;i++){
			var r = rows[i];
			if(r[bet]){
				total +=r[bet];
			}
			if(win && r[win]){
				total -=r[win];
			}
		}
		return total.toFixed(2)+"";
	}
	var columns=[{
		field : 'type',
		title : '游戏类型',
		width : '120',
		align : 'center',
		valign : 'middle',
		formatter:typeFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return "<span class='text-primary'>小计:</span>";
		}
	},{
		field : 'statProfitAmount',
		title : '统计输赢',
		align : 'center',
		width : '100',
		valign : 'middle',
		formatter:dangerMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'statProfitAmount');
		}
	}, {
		field : 'allProfitAmount',
		title : '总输赢',
		align : 'center',
		width : '120',
		valign : 'middle',
		formatter : warnMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'allProfitAmount');
		}
	}, {
		field : 'allBetAmount',
		title : '总投注',
		width : '60',
		align : 'center',
		valign : 'middle',
		formatter : primaryMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'allBetAmount');
		}
	}, {
		field : 'allWinAmount',
		title : '总派奖',
		width : '80',
		align : 'center',
		valign : 'middle',
		formatter : infoMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'allWinAmount');
		}
	}, {
		title : '真人投注',
		align : 'center',
		width : '80',
		valign : 'middle',
		field : 'realBetAmount',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'realBetAmount');
		}
	}, {
		title : '真人派奖',
		align : 'center',
		width : '80',
		valign : 'middle',
		field : 'realWinAmount',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'realWinAmount');
		}
	}, {
		title : '真人输赢',
		align : 'center',
		width : '80',
		valign : 'middle',
		field : 'realProfitAmount',
		formatter : successMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'realProfitAmount');
		}
	}, {
		field : 'egameBetAmount',
		title : '电子投注',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'egameBetAmount');
		}
	}, {
		field : 'egameWinAmount',
		title : '电子派奖',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'egameWinAmount');
		}
	}, {
		title : '电子输赢',
		align : 'center',
		width : '80',
		valign : 'middle',
		field : 'egameProfitAmount',
		formatter : successMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'egameProfitAmount');
		}
	}, {
		field : 'hunterBetAmount',
		title : '捕鱼投注',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'hunterBetAmount');
		}
	}, {
		field : 'hunterWinAmount',
		title : '捕鱼派奖',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'hunterWinAmount');
		}
	}, {
		field:'hunterProfitAmount',
		title : '捕鱼输赢',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : successMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'hunterProfitAmount');
		}
	}, {
		field : 'jackpotAmount',
		title : '捕鱼Jackpot',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter :function(value, row, index){
			if(value !=undefined && value >= 0){
				return '<span class="label label-success">'+value.toFixed(2)+'</span>';
			}
			return '-'
		},
		pageSummaryFormat:function(rows,aggsData){
			var total = 0,r;
			for(var i=0;i<rows.length;i++){
				r = rows[i]['jackpotAmount'];
				if(r && r > 0){
					total +=r;
				}
			}
			return total.toFixed(2)+"";
		}
	}, {
		field : 'xinBetAmount',
		title : 'XIN投注',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'xinBetAmount');
		}
	}, {
		field : 'xinWinAmount',
		title : 'XIN派奖',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : moneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'xinWinAmount');
		}
	}, {
		field : 'xinProfitAmount',
		title : 'XIN输赢',
		align : 'center',
		width : '80',
		valign : 'middle',
		formatter : successMoneyFormmater,
		pageSummaryFormat:function(rows,aggsData){
			return getTotal(rows,'xinProfitAmount');
		}
	}];
	return {
		render:function(type){
			Fui.addBootstrapTable({
				showPageSummary:true,
				showFooter : true,
				pagination:false,
				sidePagination:'client',
				columns:columns,
				url:baseInfo.baseUrl +'/agent/report/third/list.do'
			});
		}
	};
});