define(['template','Chart','jquery','bootstrap','Fui','fui_table'],function(template,Chart){
	var source='<tr><th width="8%" class="text-center">月份</th>'
		+'<th width="{{if !duLiCaiPiao}}9%{{else}}30%{{/if}}" class="text-center">投注额</th>'
		+'<th width="{{if !duLiCaiPiao}}9%{{else}}30%{{/if}}" class="text-center">彩票盈亏</th>'
		+'{{if !duLiCaiPiao}}<th width="8%" class="text-center">体育盈亏</th>'
		+'<th width="8%" class="text-center">真人盈亏</th>'
		+'<th width="8%" class="text-center">电子盈亏</th>'
		+'<th width="8%" class="text-center">棋牌盈亏</th>'
		+'<th width="8%" class="text-center">三方彩票盈亏</th>'
		+'<th width="8%" class="text-center">三方体育盈亏</th>'
		+'<th width="8%" class="text-center">六合彩盈亏</th>'
		+ '{{/if}}'
		// +'<th width="5%" class="text-center">首充</th>'
		// +'<th width="5%" class="text-center">二充</th>'
		// +'<th width="5%" class="text-center">三充</th>'
		+'</tr>'

		+'{{each allyear as month}}<tr>'
		+'<td>{{month.ym}}</td>'
		+'<td>{{if month.betAmount}}{{month.betAmount.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.lotteryProfit}}{{month.lotteryProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'{{if !duLiCaiPiao}}<td>{{if month.sportProfit}}{{month.sportProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.realGameProfit}}{{month.realGameProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.electronProfit}}{{month.electronProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.chessGameProfit}}{{month.chessGameProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.thirdLotteryGameProfit}}{{month.thirdLotteryGameProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.thirdSportsGameProfit}}{{month.thirdSportsGameProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>'
		+'<td>{{if month.markSixProfit}}{{month.markSixProfit.toFixed(2)}}{{else}}0.00{{/if}}</td>{{/if}}'
		// +'<td>{{month.firstcom}}</td>'
		// +'<td>{{month.secondcom}}</td>'
		// +'<td>{{month.thirdcom}}</td>'
		+'</tr>{{/each}}';
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return "<span class='text-primary'>0.00</span>";
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ]
					.join(Fui.toDecimal(value,2));
		}
		return [ '<span class="text-primary">', '</span>' ]
				.join(Fui.toDecimal(value,2));
	}
	function initChart(curData,$con) {
		var comTdy = $con.find(".comTdy").get(0).getContext("2d")
			,comYes = $con.find(".comYes").get(0).getContext("2d");
		var dataTdy = {
			labels : [ "今日中奖额", "今日盈亏" ],
			datasets : [ {
				data : [ curData.awardAmount||'0.00', curData.bunkoTdy||'0.00' ],
				backgroundColor : [ "#FF6384", "#36A2EB" ],
				hoverBackgroundColor : [ "#FF6384", "#36A2EB" ]
			} ]
		};
		var dataYes = {
			labels : [ "昨日中奖额", "昨日盈亏" ],
			datasets : [ {
				data : [ curData.winAmountYes||'0.00', curData.bunkoYes||'0.00' ],
				backgroundColor : [ "#FF6384", "#36A2EB" ],
				hoverBackgroundColor : [ "#FF6384", "#36A2EB" ]
			} ]
		};
		var options = {}

		var comTdyChart = new Chart(comTdy, {
			type : 'pie',
			data : dataTdy,
			options : options
		});
		var comYesChart = new Chart(comYes, {
			type : 'pie',
			data : dataYes,
			options : options
		});
	}
	var $form=$("#comprehensive_form_id");
	$form.find(".search-btn").click(function(){
		var it=$(this);
		if(it.prop("disabled")==true){
			return false;
		}
		it.prop("disabled",true).html('<img src="'+baseInfo.baseUrl+'/common/js/layer/skin/default/loading-2.gif" width="26" height="20">');
		var $con=$("#report_comprehensive_container_id"),$table=null;
		var reportType = $("#reportType").val();
		var begin = $("input[name='begin']").val();
		var end = $("input[name='end']").val();
		$.ajax({
			url : baseInfo.baseUrl+"/agent/report/comprehensive/yearlist.do",
			data : {
				'reportType':reportType,
				'begin':begin,
				'end':end
			},
			success : function(result) {
				it.prop("disabled",false).html('查询');
				if(result.msg){
					layer.msg(result.msg);
					return;
				}
				$con.find(".allyear-tb").html(template.compile(source)(result));
			},
			errorFn:function(){
				it.prop("disabled",false).html('查询');
			}
		});
		return false;
	});
	
	
	return {
		render:function(duLiCaiPiao){
			var $con=$("#report_comprehensive_container_id"),$table=null;
			var reportType = $("#reportType").val();
			$.ajax({
				url : baseInfo.baseUrl+"/agent/report/comprehensive/list.do",
				data : {
					'reportType':reportType
				},
				success : function(result) {
					if(result.msg){
						layer.msg(result.msg);
						return;
					}
					var data = [];
					data.push(result);
					$con.find(".betNumTdy").html(Fui.toDecimal(result.betAmount||0,2));
					$con.find(".betNumYes").html(Fui.toDecimal(result.betAmountYes||0,2));
					if (result.bunkoTdy > 0) {
						$con.find(".bunkoTdy").addClass("text-danger").html(Fui.toDecimal(result.bunkoTdy||0,2));
					}else{
						$con.find(".bunkoTdy").addClass("text-primary").html(Fui.toDecimal(result.bunkoTdy||0,2));
					}
					if (result.bunkoYes > 0) {
						$con.find(".bunkoYes").addClass("text-danger").html(Fui.toDecimal(result.bunkoYes||0,2));
					}else{
						$con.find(".bunkoYes").addClass("text-primary").html(Fui.toDecimal(result.bunkoYes||0,2));
					}
					$con.find(".firCom").addClass("text-success").html(Fui.toDecimal(result.firstcom||0,0));
					$con.find(".secCom").addClass("text-success").html(Fui.toDecimal(result.secondcom||0,0));
					$con.find(".thiCom").addClass("text-success").html(Fui.toDecimal(result.thirdcom||0,0));

					addTable(data);
					initChart(result,$con);
					result.duLiCaiPiao=duLiCaiPiao;
					$con.find(".allyear-tb").html(template.compile(source)(result));
				}
			});
			
			function addTable(data){
				if($table){
					$table.bootstrapTable("load",data);
					return;
				}
				$table=$("#report_comprehensive_datagrid_tb").bootstrapTable({
					data : data,
					columns : [ {
						field : 'accountCount',
						title : '用户总数',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'agentCount',
						title : '代理',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'memberCount',
						title : '会员',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'registerAccountYes',
						title : '昨日注册',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'registerAccount',
						title : '今日注册',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'betAmount',
						title : '今日投注',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'awardAmount',
						title : '今日中奖',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'depositAmount',
						title : '今日充值',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'withdrawAmount',
						title : '今日提款',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'agentRebate',
						title : '今日返点',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'memberRebate',
						title : '今日反水',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'depositCountYes',
						title : '昨日存款人数',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'depositCount',
						title : '今日存款人数',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'firstDepositYes',
						title : '昨日首充人数',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'firstDeposit',
						title : '今日首充人数',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}, {
						field : 'online',
						title : '在线会员数',
						align : 'center',
						valign : 'middle'
					}, {
						field : 'allBalance',
						title : '余额总额',
						align : 'center',
						valign : 'middle',
						formatter : moneyFormatter
					}]
				});
			}
			var $formA=$("#comprehensive_form");
			$formA.find("#reportType").change(function(duLiCaiPiao){
				var reportType = $("#reportType").val();
				var begin = $("input[name='begin']").val();
				var end = $("input[name='end']").val();
				$.ajax({
					url : baseInfo.baseUrl+"/agent/report/comprehensive/list.do",
					data : {
						'reportType':reportType,
						'begin':begin,
						'end':end
					},
					success : function(result) {
						if(result.msg){
							layer.msg(result.msg);
							return;
						}
						var data = [];
						data.push(result);
						$con.find(".betNumTdy").html(Fui.toDecimal(result.betAmount||0,2));
						$con.find(".betNumYes").html(Fui.toDecimal(result.betAmountYes||0,2));
						if (result.bunkoTdy > 0) {
							$con.find(".bunkoTdy").addClass("text-danger").html(Fui.toDecimal(result.bunkoTdy||0,2));
						}else{
							$con.find(".bunkoTdy").addClass("text-primary").html(Fui.toDecimal(result.bunkoTdy||0,2));
						}
						if (result.bunkoYes > 0) {
							$con.find(".bunkoYes").addClass("text-danger").html(Fui.toDecimal(result.bunkoYes||0,2));
						}else{
							$con.find(".bunkoYes").addClass("text-primary").html(Fui.toDecimal(result.bunkoYes||0,2));
						}
						addTable(data,$table);
						initChart(result,$con);
						result.duLiCaiPiao=duLiCaiPiao;
						$con.find(".allyear-tb").html(template.compile(source)(result));
					}
				});
				return false;
			});
			
		}
	};
});