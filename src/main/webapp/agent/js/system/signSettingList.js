define(['jquery','bootstrap','Fui','fui_table'],function(){
var listUrl= baseInfo.baseUrl+"/agent/system/sign/list.do"
	,recordListUrl= baseInfo.baseUrl+"/agent/system/sign/getSignRecords.do"
	,cols=[ {
		field : 'days',
		title : '连续签到天数',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'score',
		title : '奖励积分',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'signType',
		title : '规则类型',
		align : 'center',
		valign : 'middle',
		formatter : signTypeFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : operateFormatter
	} ],
	recodeCols=[{
		field : 'memberName',
		title : '会员',
		align : 'center',
		valign : 'middle'
	},{
		field : 'signDays',
		title : '连续签到天数',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'score',
		title : '奖励积分',
		align : 'center',
		valign : 'middle'
	},{
		field : 'signDate',
		title : '签到时间',
		align : 'center',
		valign : 'middle',
		formatter:Fui.formatDatetime
	}];
function operateFormatter(value, row, index) {
	return [ '<a class="open-dialog" href="',baseInfo.baseUrl,'/agent/system/sign/modify.do?id=',row.id,'" title="修改">',
			'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
			'<a class="todo-ajax" href="',baseInfo.baseUrl,'/agent/system/sign/delRule.do?id=',row.id,'" title="确定要删除？">',
			'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
			.join('');
}
function signTypeFormatter(value, row, index) {
	if(value && value == '1'){
		return "循环赠送(取最大效益)";
	}
	return "硬性要求(天数相等)";
}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_sign_setting_list_con_warp_id");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "setting":
					$con.find(".setting-tool").removeClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url : listUrl
					});
					break;
				case "record":
					$con.find(".setting-tool").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : recodeCols,
						url : recordListUrl
					});
					break;
				}
			});
			Fui.addBootstrapTable({
				url : listUrl,
				columns : cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});