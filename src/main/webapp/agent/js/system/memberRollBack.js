define(['jquery','bootstrap','Fui','fui_table'],function(){
	var addUrl = baseInfo.baseUrl+"/agent/agent/memberRollBack/add.do"
		,addThirdUrl = baseInfo.baseUrl+"/agent/agent/memberRollBack/addThird.do"
		,listUrl= baseInfo.baseUrl+"/agent/agent/memberRollBack/list.do"
		,thirdListUrl= baseInfo.baseUrl+"/agent/agent/memberRollBack/strategys.do"
		,cols =[{
			field : 'lotteryGame',
			title : '彩票返水比例',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'sportsGame',
			title : '体育返水比例',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'markSixTmb',
			title : '六合彩特码B比例',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'markSixZmb',
			title : '六合彩正码B比例',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : bfbFormatter
		}, {
			field : 'profitBet',
			title : '有效投注',
			align : 'center',
			width : '100',
			valign : 'middle'
		}, {
				field : 'multiple',
				title : '打码量倍数',
				align : 'center',
				width : '100',
				valign : 'middle'
		}, {
			field : 'upperLimit',
			title : '最高返水上限',
			align : 'center',
			width : '100',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '100',
			valign : 'middle',
			formatter : statusFormatter
		}, {
			field : 'memo',
			title : '描述',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : memoFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '50',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	,thirdCols=[ {
		field : 'type',
		title : '类型',
		align : 'center',
		valign : 'middle',
		formatter : typeFormatter
	}, {
		field : 'rate',
		title : '返水比例',
		align : 'center',
		valign : 'middle',
		formatter : bfbFormatter
	}, {
		field : 'qualifiedFmt',
		title : '有效投注',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'upperLimit',
		title : '最高返水上限',
		align : 'center',
		width : '100',
		valign : 'middle'
	}, {
			field : 'multiple',
			title : '打码量倍数',
			align : 'center',
			width : '100',
			valign : 'middle'
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : thirdStatusFormatter
	}, {
		field : 'memo',
		title : '描述',
		align : 'center',
		width : '200',
		valign : 'middle',
		formatter : memoFormatter
	}, {
		title : '操作',
		align : 'center',
		width : '50',
		valign : 'middle',
		formatter : thirdOperateFormatter
	} ];
	function bfbFormatter(value, row, index){
		if(value==undefined){
			value=0;
		}
		return value+ '%';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/agent/memberRollBack/closeOrOpen.do?id="+row.id+"&status="});
	}
	function memoFormatter(value, row, index){
		if(value==null || value==""){
			return '';
		}
		if(value.length>8){
			return value.substr(0, 8)+'。。';
		}else{
			return value;
		}
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/agent/memberRollBack/modify.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/agent/memberRollBack/delete.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
	function typeFormatter(value, row, index) {
		if (value == 1) {
			return "真人";
		} else if (value == 2) {
			return "电子";
		}
		return "";
	}
	function thirdStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:"${base}/agent/agent/memberRollBack/updStatus.do?id="+row.id+"&status="});
	}
	function thirdOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/agent/memberRollBack/modifyThird.do?id=',row.id,'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/agent/memberRollBack/delStrategy.do?id=',row.id,'" title="确定要删除？"><i class="glyphicon glyphicon-remove"></i></a>' ]
				.join('');
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_member_rollback_con_warp_id"),$addBtn=$con.find(".add-btn1");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "platform":
					$addBtn.attr("url",addUrl);
					$con.find(".third").addClass("hidden");
					$con.find(".platform").removeClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url : listUrl,
						pageNumber : 1
					});
					break;
				case "third":
					$addBtn.attr("url",addThirdUrl);
					$con.find(".third").removeClass("hidden");
					$con.find(".platform").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : thirdCols,
						url : thirdListUrl,
						pageNumber : 1
					});
					break;
				}
			});
			Fui.addBootstrapTable({
				url : listUrl,
				columns : cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});