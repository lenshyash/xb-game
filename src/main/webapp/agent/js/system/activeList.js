define(['jquery','bootstrap','Fui','fui_table'],function(){
var zjhtml = $(".zhongjiang").html(),xnhtml = $(".xunijilu1").html();
var listUrl= baseInfo.baseUrl+"/agent/system/active/list.do"
	,recordListUrl= baseInfo.baseUrl+"/agent/system/active/records.do"
	,virtualrecordListUrl = baseInfo.baseUrl+"agent/system/active/virtualrecord.do"
	,cols=[ {
		field : 'activeName',
		title : '活动名称',
		align : 'center',
		valign : 'middle',
		formatter : ruleFormatter
	}, {
		field : 'activeType',
		title : '活动类型',
		align : 'center',
		valign : 'middle',
		formatter : activeTypeFormatter
	}, {
		field : 'score',
		title : '消耗积分',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'playCount',
		title : '每日参与次数',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'beginDatetime',
		title : '开始时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'endDatetime',
		title : '结束时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'createDatetime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : statusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : operateFormatter
	} ],
	recodeCols=[ {
		field : 'account',
		title : '获奖者',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'awardType',
		title : '奖项类型',
		align : 'center',
		valign : 'middle',
		formatter : awardTypeFormatter
	}, {
		field : 'productName',
		title : '奖项名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'createDatetime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : awardStatusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : awardOpFormatter
	}, {
		field : 'remark',
		title : '备注说明',
		align : 'center',
		valign : 'middle'
	} ],
	recodeVitualrecordCols=[
	{
		field : 'account',
		title : '获奖者',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'awardType',
		title : '奖项类型',
		align : 'center',
		valign : 'middle',
		formatter : virtualAwardTypeFormatter
	}, {
		field : 'productName',
		title : '奖项名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'createDatetime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	},
	{
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : awardvOpFormatter
	} ];

	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return '';
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function ruleFormatter(value, row, index) {
		return '<a class="open-tab" id="active_set_rule_id" data-refresh="true" href="'+baseInfo.baseUrl+'/agent/system/active/setRule.do?id='+row.id+'" title="规则设置">'+ value + '</a>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/system/active/updStatus.do?id="+row.id+"&status="})
	}
	function activeTypeFormatter(value, row, index) {
		if (value == 1) {
			return "幸运大转盘";
		}
		return value;
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="',baseInfo.baseUrl,'/agent/system/active/modifyActive.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="',baseInfo.baseUrl,'/agent/system/active/delete.do?id=',row.id,'" title="确定要删除“',row.activeName,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	function awardTypeFormatter(value, row, index) {
		if (value === 3) {
			return "<span class='text-primary'>奖品</span>";
		}else if(value === 4){
			return "<span class='text-primary'>积分</span>";
		}
		return "<span class='text-primary'>现金</span>";
	}
	function virtualAwardTypeFormatter(value, row, index) {
		if (value === 3) {
			return "<span class='text-primary'>奖品</span>";
		}else if(value === 4){
			return "<span class='text-primary'>积分</span>";
		}else if(value == 2){
			return "<span class='text-primary'>现金</span>";
		}
		return "<span class='text-primary'>不中奖</span>";
	}
	function awardStatusFormatter(value, row, index) {
		if (value === 1) {
			return '<span class="text-success">处理中</span>';
		} else if (value === 2) {
			return '<span class="text-primary">处理成功</span>';
		}
		return '<span class="text-danger">处理失败</span>';
	}
	function awardOpFormatter(value, row, index) {
		if (row.status != 1) {
			return "";
		}
		return '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/system/active/handle.do?id='+row.id+'" title="处理">处理</a>'
	}
	function awardvOpFormatter(value, row, index) {
		if (row.status != 1) {
			return "";
		}
		return [
		'<a class="open-dialog" href="',baseInfo.baseUrl,'/agent/system/active/editActiveVirtualrecord.do?id=',row.id,'" title="修改">',
		'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
		'<a class="todo-ajax" href="',baseInfo.baseUrl,'/agent/system/active/delVirtualrecord.do?id=',row.id,'" title="确定要删除 ?"',row.activeName,'"？">',
		'<i class="glyphicon glyphicon-remove"></i>', '</a>'].join('');
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_active_list_con_warp_id");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "huodong":
					$con.find(".zhongjiang").addClass("hidden");
					$con.find(".xunijilu").addClass("hidden");
					$con.find(".huodong").removeClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url : listUrl
					});
					break;
				case "zhongjiang":
//					$('.zhongjiang').html(zjhtml)
//					$('.xunijilu1').html('')
					$con.find(".zhongjiang").removeClass("hidden");
					$con.find(".huodong").addClass("hidden");
					$con.find(".xunijilu").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : recodeCols,
						url : recordListUrl
					});
					break;
				case "xunijilu":
//					$('.zhongjiang').html('')
//					$('.xunijilu1').html(xnhtml)
					$con.find(".xunijilu").removeClass("hidden");
					$con.find(".huodong").addClass("hidden");
//					$con.find(".zhongjiang").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						url : virtualrecordListUrl,
						columns : recodeVitualrecordCols
					});
					break;
				}
			});
			Fui.addBootstrapTable({
				url : listUrl,
				columns : cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});