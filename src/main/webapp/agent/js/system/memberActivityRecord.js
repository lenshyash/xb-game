define(['jquery','bootstrap','Fui','fui_table'],function(){
var listUrl= baseInfo.baseUrl+"/agent/system/agentactivity/list.do"
	,recordListUrl= baseInfo.baseUrl+"/agent/system/agentactivity/listApplyRecord.do"
	,fictitiousListUrl= baseInfo.baseUrl+"/agent/system/redPacket/fictitious.do"
	,cols=[ {
		field : 'title',
		title : '活动标题',
		align : 'center',
		width : '200',
		valign : 'middle'
	}, {
		field : 'titleImgUrl',
		title : '标题图片地址',
		align : 'center',
		width : '180',
		valign : 'bottom',
		formatter : contentFormatter
	}, {
		field : 'modelStatus',
		title : '活动状态',
		align : 'center',
		width : '200',
		valign : 'middle',
		formatter : statusFormatter
	}, {
		field : 'updateTime',
		title : '发布时间',
		align : 'center',
		width : '300',
		valign : 'middle',
		formatter : Fui.formatDate
	}, {
		field : 'overTime',
		title : '过期时间',
		align : 'center',
		width : '300',
		valign : 'middle',
		formatter : Fui.formatDate
	}, {
		title : '操作',
		align : 'center',
		width : '200',
		valign : 'middle',
		formatter : operateFormatter
	} ],
	recodeCols=[  {
		field : 'account',
		title : '获奖者',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'activityName',
		title : '优惠活动名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'activityOption',
		title : '申请选项',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'createTime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : recordStatusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : awardOpFormatter
	}, {
		field : 'remark',
		title : '备注说明',
		align : 'center',
		valign : 'middle'
	}],
	fictitiousCols=[];
	//
	function contentFormatter(value, row, index){
		if(value == null || value ==""){
			return value;
		}
		if(value.length>5){
			return value.substr(0, 5)+'...';
		}else{
			return value;
		}
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/system/agentactivity/zhopenClose.do?id="+row.id+"&modelStatus="});
	}
	function operateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/system/agentactivity/modify.do?id=',row.id,'" title="修改">',
				'<i class="glyphicon glyphicon-pencil"></i>', '</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/system/agentactivity/delete.do?id=',row.id,'" title="确定要删除“',row.title,'”？">',
				'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
				.join('');
	}
	//
	function recordStatusFormatter(value, row, index) {
		if (value === 1) {
			return '<span class="text-success">处理中</span>';
		} else if (value === 2) {
			return '<span class="text-primary">处理成功</span>';
		}
		return '<span class="text-danger">处理失败</span>';
	}
	function awardOpFormatter(value, row, index) {
		if (row.status != 1) {
			return "";
		}
		return '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/system/agentactivity/handler.do?id='+row.id+'" title="处理">处理</a>'
	}
	//
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_activity_record_list_con_warp_id");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "huodong":
					$con.find(".zhongjiang").addClass("hidden");
					$con.find(".huodong").removeClass("hidden");
					$con.find(".xuni").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url : listUrl
					});
					break;
				case "zhongjiang":
					$con.find(".zhongjiang").removeClass("hidden");
					$con.find(".huodong").addClass("hidden");
					$con.find(".xuni").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : recodeCols,
						url : recordListUrl
					});
					break;
				case "xuni":
					$con.find(".xuni").removeClass("hidden");
					$con.find(".zhongjiang").addClass("hidden");
					$con.find(".huodong").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : fictitiousCols,
						url : fictitiousListUrl
					});
					break;
				}
				
			});
			Fui.addBootstrapTable({
				url : listUrl,
				columns : cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
					var $formId = $("#agent_redpacket_form_id");
					
					$formId.on("click","button.batchDelete",function(e){
						//取消原始的事件
						e.preventDefault();
						var ids='';
						curBootstrapTable.find('tbody input:checkbox:checked').each(function(i,j){
							j = $(j);
							ids += j.val() + ",";
						})
						if(!ids){
							layer.msg('请勾选要删除的选项');
							return;
						}
						ids = ids.substring(0,ids.length-1);
						layer.confirm('确定要删除选中的选项？',{btn:['确定','取消']},function(index){
							$.ajax({
								url:baseInfo.baseUrl+'/agent/system/redPacket/batchDelete.do',
								data:{batchId:ids},
								success:function(res){
									if(res.success){
										layer.msg('删除成功');
										curBootstrapTable.find('input:checked').prop('checked',false);
										curBootstrapTable.bootstrapTable('refreshOptions', {
											columns : cols,
											url : listUrl
										});
									}else{
										layer.msg(res.msg);
									}
								}
							})			
						},function(){
							curBootstrapTable.find('input:checked').prop('checked',false);
						})
					})
					
					curBootstrapTable.on("click","input.check_all",function(){
						var $this = $(this),isChecked = $this.prop('checked');
						curBootstrapTable.find('tbody input:checkbox').prop('checked',isChecked);
					});
				}
			});
		}
	}
});