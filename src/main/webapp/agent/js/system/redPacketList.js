define(['jquery','bootstrap','Fui','fui_table'],function(){
var listUrl= baseInfo.baseUrl+"/agent/system/redPacket/list.do"
	,recordListUrl= baseInfo.baseUrl+"/agent/system/redPacket/records.do"
	,fictitiousListUrl= baseInfo.baseUrl+"/agent/system/redPacket/fictitious.do"
	,cols=[ {
		field : 'checkbox',
		title : '<input type="checkbox" class="check_all">',
		align : 'center',
		vilign : 'middle',
		formatter : operateCheckboxMatter
	},{
		field : 'id',
		title : '红包ID',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'title',
		title : '红包标题',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'totalMoney',
		title : '红包金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'totalNumber',
		title : '红包个数',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'minMoney',
		title : '最小金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'ipNumber',
		title : '限制当天IP次数',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'todayDeposit',
		title : '当天充值',
		align : 'center',
		valign : 'middle',
		formatter : depositFormatter
	}, {
		field : 'levelNames',
		title : '等级分层限制',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'beginDatetime',
		title : '开始时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'endDatetime',
		title : '结束时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : statusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : operateFormatter
	} ],
	recodeCols=[  {
		field : 'redPacketId',
		title : '红包ID',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'account',
		title : '用户名',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'redPacketName',
		title : '红包名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'money',
		title : '红包金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'createDatetime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}, {
		field : 'ip',
		title : 'IP',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : recordStatusFormatter
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : recordOpFormatter
	}],
	fictitiousCols=[ {
		field : 'redPacketName',
		title : '红包名称',
		align : 'center',
		valign : 'middle'
	},{
		field : 'account',
		title : '用户名',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'money',
		title : '红包金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'createDatetime',
		title : '创建时间',
		align : 'center',
		valign : 'middle',
		formatter : Fui.formatDatetime
	}];
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return '';
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function depositFormatter(value,row,index){
		if(value){
			return '<span class="text-primary">得充值</span>&nbsp;'+moneyFormatter(value);
		}
		return '<span class="text-danger">不限制</span>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/system/redPacket/updStatus.do?id="+row.id+"&status="})
	}
	
	function recordStatusFormatter(value, row, index) {
		if (value === 1) {
			return '<span class="text-success">处理中</span>';
		} else if (value === 2) {
			return '<span class="text-primary">处理成功</span>';
		}
		return '<span class="text-danger">处理失败</span>';
	}
	function operateFormatter(value, row, index) {
		return '<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/system/redPacket/delete.do?id='+row.id+'" title="确定要删除“'+row.title+'”？">删除</a>';
	}
	function recordOpFormatter(value, row, index) {
		if(row.status != 1){
			return "已处理";
		}
		return '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/system/redPacket/handle.do?id='+row.id+'" title="处理">处理</a>';
	}
	function operateCheckboxMatter(value,row,index){
		return '<input type="checkbox" value="'+row.id+'">';
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_red_packet_list_con_warp_id");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "huodong":
					$con.find(".zhongjiang").addClass("hidden");
					$con.find(".huodong").removeClass("hidden");
					$con.find(".xuni").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url : listUrl
					});
					break;
				case "zhongjiang":
					$con.find(".zhongjiang").removeClass("hidden");
					$con.find(".huodong").addClass("hidden");
					$con.find(".xuni").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : recodeCols,
						url : recordListUrl
					});
					break;
				case "xuni":
					$con.find(".xuni").removeClass("hidden");
					$con.find(".zhongjiang").addClass("hidden");
					$con.find(".huodong").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : fictitiousCols,
						url : fictitiousListUrl
					});
					break;
				}
				
			});
			Fui.addBootstrapTable({
				url : listUrl,
				columns : cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
					var $formId = $("#agent_redpacket_form_id");
					
					$formId.on("click","button.batchDelete",function(e){
						//取消原始的事件
						e.preventDefault();
						var ids='';
						curBootstrapTable.find('tbody input:checkbox:checked').each(function(i,j){
							j = $(j);
							ids += j.val() + ",";
						})
						if(!ids){
							layer.msg('请勾选要删除的选项');
							return;
						}
						ids = ids.substring(0,ids.length-1);
						layer.confirm('确定要删除选中的选项？',{btn:['确定','取消']},function(index){
							$.ajax({
								url:baseInfo.baseUrl+'/agent/system/redPacket/batchDelete.do',
								data:{batchId:ids},
								success:function(res){
									if(res.success){
										layer.msg('删除成功');
										curBootstrapTable.find('input:checked').prop('checked',false);
										curBootstrapTable.bootstrapTable('refreshOptions', {
											columns : cols,
											url : listUrl
										});
									}else{
										layer.msg(res.msg);
									}
								}
							})			
						},function(){
							curBootstrapTable.find('input:checked').prop('checked',false);
						})
					})
					
					curBootstrapTable.on("click","input.check_all",function(){
						var $this = $(this),isChecked = $this.prop('checked');
						curBootstrapTable.find('tbody input:checkbox').prop('checked',isChecked);
					});
				}
			});
		}
	}
});