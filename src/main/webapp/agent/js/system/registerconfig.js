define(['jquery','bootstrap','Fui','fui_table'],function(){
	var daiLiListUrl= baseInfo.baseUrl+"/agent/system/registerconf/newList.do?platform=2"
		,huiYuanListUrl= baseInfo.baseUrl+"/agent/system/registerconf/newList.do?platform=1"
		,huiYuanCols = [ {
			field : 'name',
			title : '属性名称',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : nameInputFormatter
		}, {
			field : 'showVal',
			title : '显示',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'show')}
		}, {
			field : 'requiredVal',
			title : '必输',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'required')}
		}, {
			field : 'validateVal',
			title : '验证',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'validate')}
		}, {
			field : 'uniqueVal',
			title : '唯一',
			align : 'center',
			width : '180',
			valign : 'bottom',
			formatter : function(value, row, index){return showFormatter(value, row, 'unique')}
		}  , {
			field : 'remindText',
			title : '提示文本',
			align : 'center',
			width : '180',
			valign : 'middle',
			formatter : remindTextInputFormatter
		} ,{
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		} ]
	,daiLiCols=[ {
		field : 'name',
		title : '属性名称',
		align : 'center',
		width : '180',
		valign : 'middle',
		formatter : nameInputFormatter
	}, {
		field : 'showVal',
		title : '显示',
		align : 'center',
		width : '180',
		valign : 'bottom',
		formatter : function(value, row, index){return showFormatter(value, row, 'show')}
	}, {
		field : 'requiredVal',
		title : '必输',
		align : 'center',
		width : '180',
		valign : 'bottom',
		formatter : function(value, row, index){return showFormatter(value, row, 'required')}
	}, {
		field : 'validateVal',
		title : '验证',
		align : 'center',
		width : '180',
		valign : 'bottom',
		formatter : function(value, row, index){return showFormatter(value, row, 'validate')}
	}, {
		field : 'uniqueVal',
		title : '唯一',
		align : 'center',
		width : '180',
		valign : 'bottom',
		formatter : function(value, row, index){return showFormatter(value, row, 'unique')}
	} , {
		field : 'remindText',
		title : '提示文本',
		align : 'center',
		width : '180',
		valign : 'middle',
		formatter : remindTextInputFormatter
	},{
		title : '操作',
		align : 'center',
		width : '150',
		valign : 'middle',
		formatter : operateFormatter
	}];
	function operateFormatter(value, row, index) {
		return '<a class="save saveRow" rowid="'+row.id+'" href="javascript:void(0);" title="保存"><i class="glyphicon glyphicon-edit"></i>保存</a>';
	}
	function nameInputFormatter(value, row, index) {
		if(!value){
			value ="";
		}
		return '<input name="name" class="form-control" type="text" value="'+value+'"/>';
	}
	function remindTextInputFormatter(value, row, index) {
		if(!value){
			value ="";
		}
		return '<input name="remindText" class="form-control" type="text" value="'+value+'"/>';
	}
	
	function showFormatter(value, row, field) {
		if(row[field]!=2){
			if("unique"!=field){
				return "-";
			}else if(row["uniqueness"]!=2){
				return "-";
			}
		}
		return Fui.statusFormatter({val:value,onText:"是",offText:"否",url:baseInfo.baseUrl+"/agent/system/registerconf/updateProp.do?id="+row.id+'&prop='+field+"&value="});
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#sys_registerconfig_con_warp_id");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "huiyuan":
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : huiYuanCols,
						url : huiYuanListUrl,
						sidePagination: 'client',
						pagination:false
					});
					break;
				case "daili":
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : daiLiCols,
						url : daiLiListUrl,
						sidePagination: 'client',
						pagination:false
					});
					break;
				}
			});
			Fui.addBootstrapTable({
				sidePagination: 'client',
				pagination:false,
				url : huiYuanListUrl,
				columns : huiYuanCols,
				onLoadSuccess:function(d){
					//单行保存
					d.find('a.saveRow').click(function(){
		 				var $it=$(this)
		 					,id = $it.attr('rowid')
		 					,$tr=$it.parents("tr")
		 					,name  = $tr.find('[name="name"]').val()//名称
		 					,remindText=$tr.find('[name="remindText"]').val()//提示文字
	 					$.ajax({
	 						url : baseInfo.baseUrl+"/agent/system/registerconf/updateProp.do",
	 						data : {
		 						'id' : id,
		 						'name' : name,
		 						'remindText' : remindText
		 					},
	 						success : function(result) {
	 							layer.msg(result.msg||'修改成功');
	 						}
	 					})
					});
				},
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});