define(['template','moment','jquery','bootstrap','Fui','bootstrap_editable'],function(template,moment){
	var source='{{each gps as gp}}<div class="panel panel-default">'
		+'<div class="panel-heading"><a role="button" data-toggle="collapse" href="#config_list_collapse_{{gp.id}}"'
		+'aria-expanded="true" aria-controls="config_list_collapse_{{gp.id}}">{{gp.name}}</a></div>'
		+'<div class="collapse in" id="config_list_collapse_{{gp.id}}"><div class="panel-body">'
		+'<table class="table table-bordered table-striped">'
		+'<tbody>{{each gp.sons as conf}}'
		+'<tr><td width="30%" class="text-center">{{conf.name}}</td>'
		+'<td width="70%"><a style="word-break: break-all;max-width:50%;display:inline-block;" href="#" id="{{conf.configId}}" data-type="{{conf.type}}" data-value="{{conf.value}}" data-pk="{{conf.id}}"'
		+'data-title="{{conf.title}}" data-source="{{conf.source}}"{{conf.expand}}></a></td></tr>{{/each}}'
		+'</tbody></table></div></div></div>{{/each}}';
	window.moment=moment;
	function handlerData(data){
		var gps = [];
		var groupMap = {
			0 : {
				id : 0,
				name : "其他",
				sons : []
			}
		};
		for (var i = 0; i < data.confs.length; i++) {
			var c = data.confs[i];
			if(c.type && c.type=='datetime'){
				c.type='combodate';
			}
			if (c.groupId) {
				if (groupMap[c.groupId]) {
					groupMap[c.groupId].sons.push(c);
				} else {
					var gp = {
						id : c.groupId,
						name : c.groupName,
						sons : []
					};
					groupMap[c.groupId] = gp;
					gp.sons.push(c);
					gps.push(gp);
				}

			} else {
				groupMap[0].sons.push(c);
			}
		}
		if (groupMap[0].sons.length > 0) {
			gps.push(groupMap[0]);
		}
		data.gps = gps;
	}
	function setParams(params) {
		if ($(this).attr("data-type") === 'checklist') {
			params.value = params.value.join(",");
		}
		params.configId = $(this).attr("id");
		return params;
	}
	function initXeditable(data){
		var $content=$("#config_list_content_id").html(template.compile(source)(data));
		$content.find("a").each(function() {
			var options = {
					url : baseInfo.baseUrl+'/agent/system/baseconfig/save.do',
					emptytext : '无配置',
					params : setParams,
					placement : 'right',
					ajaxOptions:{
						dataType:"json",
						success:function(json){
							layer.msg(json.msg||"修改成功");
						}
					}
				},type = $(this).attr("data-type")
				,source = $(this).attr("data-source")
				,val = $(this).attr("data-value");
			if (val !== undefined && val !== "") {
				$(this).html(val.replace(/</g,"&lt;"));
			}
			if (type === 'select') {
				options.source = source;
				var arr = evil(source);
				for (var i = 0; i < arr.length; i++) {
					var obj = arr[i];
					if (obj[val] !== undefined) {
						$(this).html(obj[val]);
						break;
					}
				}
			} else if (type === 'combodate') {
				var fmt = 'HH:mm'
					,cdconf={
						format : fmt
		                ,minYear: 2015
		                ,maxYear: 2030
		                ,minuteStep: 1
					};
				if (source) {
					var src = evil("(" + source + ")");
					for ( var key in src) {
						if (key != "format") {
							cdconf[key] = src[key];
						}
					}
				}
				options.template = fmt;
				options.format = fmt;
				options.viewformat = fmt;
				options.combodate = cdconf;
			} else if (type === 'textarea') {
				options.placement = 'top';
			}
			$(this).editable(options);
		});
	}
	return {
		render:function(){
			$.get(baseInfo.baseUrl+"/agent/system/baseconfig/list.do",function(data){
				handlerData(data);
				initXeditable(data);
			},"json");
		}
	}
});
// 重写 eval
function evil(fn) {
	try {
		var Fn = Function;  //一个变量指向Function，防止有些前端编译工具报错
		return new Fn('return ' + fn)();
	} catch (e) {
		console.log(e)
	}
}