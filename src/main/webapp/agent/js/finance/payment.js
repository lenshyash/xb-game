define(['jquery','bootstrap','Fui','fui_table'],function(){
	var addOnlineUrl = baseInfo.baseUrl+"/agent/finance/payment/addPayment.do"
		,onlineListUrl= baseInfo.baseUrl+"/agent/finance/payment/paymentlst.do"
	,onlineCols=[ {
		field : 'payName',
		title : '接口名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'payCom',
		title : '所属公司',
		align : 'center',
		valign : 'middle'
	}
//	, {
//		field : 'min',
//		title : '最小金额',
//		align : 'center',
//		valign : 'middle',
//		formatter : moneyFormatter
//	}, {
//		field : 'max',
//		title : '最大金额',
//		align : 'center',
//		valign : 'middle',
//		formatter : moneyFormatter
//	}
//	, {
//		field : 'url',
//		title : '商户域名',
//		align : 'center',
//		valign : 'middle',
//		formatter : contentSizeFormatter
//	}
	, {
		field : 'merchantCode',
		title : '商户编码',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'merchantKey',
		title : '商户密钥',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'account',
		title : '支付账号',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}
//	, {
//		field : 'payType',
//		title : '支付方式',
//		align : 'center',
//		valign : 'middle',
//		formatter : payTypeFormatter
//	}, {
//		field : 'showType',
//		title : '显示类型',
//		align : 'center',
//		valign : 'middle',
//		formatter : showTypeFormatter
//	}
	, {
		field : 'payGetway',
		title : '支付网关',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : onlineStatusFormatter
	}
//	, {
//		field : 'def',
//		title : '是否默认',
//		align : 'center',
//		valign : 'middle',
//		formatter : defFormatter
//	}
	, {
		field : 'sortNo',
		title : '排序',
		align : 'center',
		valign : 'middle'
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : onlineOperateFormatter
	} ];
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function contentSizeFormatter(value, row, index) {
		if (value && value.length > 15) {
			return value.substring(0, 15) + "...";
		}
		return value;
	}
	function payTypeFormatter(value, row, index) {
		var payTypeShow = "-";
		if(value == "1"){
			payTypeShow = "收银台";
		} else if(value == "2"){
			payTypeShow = "银行直连";
		} else if(value == "3"){
			payTypeShow = "单微信";
		} else if(value == "4"){
			payTypeShow = "单支付宝";
		} else if(value == "5"){
			payTypeShow = "单QQ钱包";
		} else if(value == "6"){
			payTypeShow = "单京东扫码";
		} else if(value == "7"){
			payTypeShow = "单百度钱包";
		}else if(value == "8"){
			payTypeShow = "单银联扫码";
		}else if(value == "9"){
			payTypeShow = "银联快捷";
		}else if(value == "10"){
			payTypeShow = "QQ扫码";
		}else if(value == "11"){
			payTypeShow = "微信H5条形码";
		}else if(value == "12"){
			payTypeShow = "支付宝H5条形码";
		}
		return payTypeShow;
	}
	function showTypeFormatter(value, row, index) {
		var showTypeShow = "-";
		if(value == "all"){
			showTypeShow = "所有终端显示";
		} else if(value == "pc"){
			showTypeShow = "pc端显示";
		} else if(value == "mobile"){
			showTypeShow = "手机端显示";
		} else if(value == "app"){
			showTypeShow = "app端显示";
		}
		return showTypeShow;
	}
	function onlineStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/finance/payment/updPaymentStatus.do?id="+row.id+"&status="});
	}
	function defFormatter(value, row, index) {
		if (value === 2) {
			return [ '<span class="text-success">', '</span>' ].join("是");
		}
		return [ '<span class="text-danger">', '</span>' ].join("否");
	}
	function onlineOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/finance/payment/modifyPayment.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/finance/payment/delpayment.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#finance_payment_con_warp_id"),$addBtn=$con.find(".add-btn1");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				$con.find(".zaixiandaifu").removeClass("hidden");
				$addBtn.attr("url",addOnlineUrl);
				curBootstrapTable.bootstrapTable('refreshOptions', {
					columns : onlineCols,
					url : onlineListUrl,
					pageNumber : 1
				});
			});
			Fui.addBootstrapTable({
				url : onlineListUrl,
				columns : onlineCols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});