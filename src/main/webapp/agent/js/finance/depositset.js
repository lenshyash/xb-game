define(['jquery','bootstrap','Fui','fui_table'],function(){
	var addBankUrl = baseInfo.baseUrl+"/agent/finance/depositset/addBank.do"
		,addFastUrl = baseInfo.baseUrl+"/agent/finance/depositset/addFast.do"
		,addOnlineUrl = baseInfo.baseUrl+"/agent/finance/depositset/addOnline.do"
		,addVirtualUrl = baseInfo.baseUrl+"/agent/finance/depositset/addVirtual.do"
		,bankListUrl= baseInfo.baseUrl+"/agent/finance/depositset/banklst.do"
		,fastListUrl= baseInfo.baseUrl+"/agent/finance/depositset/fastlst.do"
		,onlineListUrl= baseInfo.baseUrl+"/agent/finance/depositset/onlinelst.do"
		,virtualListUrl= baseInfo.baseUrl+"/agent/finance/depositset/virtuallst.do"
		,bankCols = [ {
			field : 'payName',
			title : '银行名称',
			align : 'center',
			width : '140px',
			valign : 'middle'
		}, {
			field : 'bankCard',
			title : '银行卡号',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'creatorName',
			title : '开户名',
			align : 'center',
			width : '200px',
			valign : 'middle'
		}, {
			field : 'bankAddress',
			title : '开户行地址',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'min',
			title : '最小金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'max',
			title : '最大金额',
			align : 'center',
			valign : 'middle',
			formatter : moneyFormatter
		}, {
			field : 'sortNo',
			title : '排序',
			align : 'center',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			valign : 'middle',
			formatter : bankStatusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : bankOperateFormatter
		}]
	,fastCols=[ {
		field : 'payName',
		title : '接口名称',
		align : 'center',
		width : '140px',
		valign : 'middle'
	}, {
		field : 'payCom',
		title : '接口所属公司',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'payAccount',
		title : '支付账号',
		align : 'center',
		width : '200px',
		valign : 'middle'
	}, {
		field : 'payUserName',
		title : '账户姓名',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'min',
		title : '最小金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'max',
		title : '最大金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'qrCodeImg',
		title : '二维码路径',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : fastStatusFormatter
	}, {
		field : 'sortNo',
		title : '排序',
		align : 'center',
		valign : 'middle'
	}, {
		title : '操作',
		align : 'center',
		width : '200',
		valign : 'middle',
		formatter : fastOperateFormatter
	} ],
	virtualCols=[{
		field : 'payName',
		title : '接口名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'payCom',
		title : '所属公司',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'min',
		title : '最小金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'max',
		title : '最大金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'url',
		title : '商户域名',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'merchantCode',
		title : '商户编码',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'merchantKey',
		title : '商户密钥',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'account',
		title : '支付账号',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'showType',
		title : '显示类型',
		align : 'center',
		valign : 'middle',
		formatter : showTypeFormatter
	}, {
		field : 'payGetway',
		title : '支付网关',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'payChannel',
		title : '支付渠道编码',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : virtualStatusFormatter
	}, {
		field : 'sortNo',
		title : '排序',
		align : 'center',
		valign : 'middle'
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : virtualOperateFormatter
	}  ]
	,onlineCols=[ {
		field : 'payName',
		title : '接口名称',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'payCom',
		title : '所属公司',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'min',
		title : '最小金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'max',
		title : '最大金额',
		align : 'center',
		valign : 'middle',
		formatter : moneyFormatter
	}, {
		field : 'url',
		title : '商户域名',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'merchantCode',
		title : '商户编码',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'merchantKey',
		title : '商户密钥',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'account',
		title : '支付账号',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'payType',
		title : '支付方式',
		align : 'center',
		valign : 'middle',
		formatter : payTypeFormatter
	}, {
		field : 'showType',
		title : '显示类型',
		align : 'center',
		valign : 'middle',
		formatter : showTypeFormatter
	}, {
		field : 'payGetway',
		title : '支付网关',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'payChannel',
		title : '支付渠道编码',
		align : 'center',
		valign : 'middle',
		formatter : contentSizeFormatter
	}, {
		field : 'status',
		title : '状态',
		align : 'center',
		valign : 'middle',
		formatter : onlineStatusFormatter
	}, {
		field : 'def',
		title : '是否默认',
		align : 'center',
		valign : 'middle',
		formatter : defFormatter
	}, {
		field : 'sortNo',
		title : '排序',
		align : 'center',
		valign : 'middle'
	}, {
		title : '操作',
		align : 'center',
		valign : 'middle',
		formatter : onlineOperateFormatter
	} ];
	function moneyFormatter(value, row, index) {
		if (value === undefined) {
			return value;
		}
		if (value > 0) {
			return [ '<span class="text-danger">', '</span>' ].join(value);
		}
		return [ '<span class="text-primary">', '</span>' ].join(value);
	}
	function contentSizeFormatter(value, row, index) {
		if (value && value.length > 15) {
			return value.substring(0, 15) + "...";
		}
		return value;
	}
	function payTypeFormatter(value, row, index) {
		var payTypeShow = "-";
		if(value == "1"){
			payTypeShow = "收银台";
		} else if(value == "2"){
			payTypeShow = "银行直连";
		} else if(value == "3"){
			payTypeShow = "单微信";
		} else if(value == "4"){
			payTypeShow = "单支付宝";
		} else if(value == "5"){
			payTypeShow = "单QQ钱包";
		} else if(value == "6"){
			payTypeShow = "单京东扫码";
		} else if(value == "7"){
			payTypeShow = "单百度钱包";
		}else if(value == "8"){
			payTypeShow = "单银联扫码";
		}else if(value == "9"){
			payTypeShow = "银联快捷";
		}else if(value == "10"){
			payTypeShow = "QQ扫码";
		}else if(value == "11"){
			payTypeShow = "微信H5条形码";
		}else if(value == "12"){
			payTypeShow = "支付宝H5条形码";
		}else if(value == "13"){
			payTypeShow = "支付宝拼多多";
		}
		return payTypeShow;
	}
	function showTypeFormatter(value, row, index) {
		var showTypeShow = "-";
		if(value == "all"){
			showTypeShow = "所有终端显示";
		} else if(value == "pc"){
			showTypeShow = "pc端显示";
		} else if(value == "mobile"){
			showTypeShow = "手机端显示";
		} else if(value == "app"){
			showTypeShow = "app端显示";
		}
		return showTypeShow;
	}
	function onlineStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/finance/depositset/updOnlineStatus.do?id="+row.id+"&status="});
	}
	function virtualStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/finance/depositset/updVirtualStatus.do?id="+row.id+"&status="});
	}
	function defFormatter(value, row, index) {
		if (value === 2) {
			return [ '<span class="text-success">', '</span>' ].join("是");
		}
		return [ '<span class="text-danger">', '</span>' ].join("否");
	}
	function onlineOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/finance/depositset/modifyOnline.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/finance/depositset/delonline.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
	function virtualOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/finance/depositset/modifyVirtual.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/finance/depositset/delvirtual.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
	function fastStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/finance/depositset/updFastStatus.do?id="+row.id+"&status="});
	}
	function fastOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/finance/depositset/modifyFast.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/finance/depositset/delfast.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
	function bankStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/finance/depositset/updBankStatus.do?id="+row.id+"&status="});
	}
	function bankOperateFormatter(value, row, index) {
		return [ '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/finance/depositset/modifyBank.do?id=',row.id,'" title="修改">修改</a>  ',
				'<a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/finance/depositset/delbank.do?id=',row.id,'" title="确定要删除“',row.payName,'”？">删除</a>' ]
				.join('');
	}
	return {
		render:function(){
			var curBootstrapTable=null,$con=$("#finance_depositset_con_warp_id"),$addBtn=$con.find(".add-btn1");
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "online":
					$con.find(".zaixianzhifu").removeClass("hidden");
					$con.find(".kuaisurukuan").addClass("hidden");
					$addBtn.attr("url",addOnlineUrl);
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : onlineCols,
						url : onlineListUrl,
						pageNumber : 1
					});
					break;
				case "bank":
					$con.find(".zaixianzhifu").addClass("hidden");
					$con.find(".kuaisurukuan").addClass("hidden");
					$addBtn.attr("url",addBankUrl);
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : bankCols,
						url : bankListUrl,
						pageNumber : 1
					});
					break;
				case "fast":
					$con.find(".zaixianzhifu").addClass("hidden");
					$con.find(".kuaisurukuan").removeClass("hidden");
					$addBtn.attr("url",addFastUrl);
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : fastCols,
						url : fastListUrl,
						pageNumber : 1
					});
					break;
				case "virtual":
					$con.find(".zaixianzhifu").addClass("hidden");
					$con.find(".kuaisurukuan").removeClass("hidden");
					$addBtn.attr("url",addVirtualUrl);
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : virtualCols,
						url : virtualListUrl,
						pageNumber : 1
					});
					break;
				}
			});
			Fui.addBootstrapTable({
				url : onlineListUrl,
				columns : onlineCols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});