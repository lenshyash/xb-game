define(['jquery','bootstrap','Fui','fui_table'],function(){
	var cols = [ {
			field : 'name',
			title : '号码',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'odds',
			title : '赔率',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : oddsInputFormatter
		}, {
			field : 'minBetAmmount',
			title : '最小下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : minBetInputFormatter
		}, {
			field : 'maxBetAmmount',
			title : '最大下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : maxBetInputFormatter
		}, {
			field : 'sortNo',
			title : '序号',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : sortNoInputFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		}];
	function oddsInputFormatter(value, row, index) {
		return '<input name="odds" class="form-control" type="text" value="'+value+'"/>';
	}
	function minBetInputFormatter(value, row, index) {
		return '<input name="minBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function maxBetInputFormatter(value,row,index){
		return '<input name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function sortNoInputFormatter(value, row, index) {
		return '<input name="sortNo" class="form-control" type="text" value="'+value+'"/>';
	}
	function operateFormatter(value, row, index) {
		return '<a class="save saveRow" rowid="'+row.id+'" href="javascript:void(0);" title="保存"><i class="glyphicon glyphicon-edit"></i>保存</a>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/cp/markSix/closeOrOpen.do?id="+row.id+"&status="});
	}
	function getLotPlayGroup(lotType,$con) {
		$.ajax({
			url : baseInfo.baseUrl+"/agent/lotPeiLv/bigList.do?lotType="+lotType,
			type : "get",
			dataType : 'json',
			success : function(r) {
				if(r==null || r==""){
					$con.find(".playTab").html('');
					$con.find(".playSmallTab").html('');
					return;
				}
				var html="";
				for(var i=0;i<r.length;i++){
					if(i==0){
						html+='<li class="active">';
						getLotPlay(r[0].id,$con);
					}else{
						html+='<li>';
					}
					html+='<a href="#" data-toggle="tab" class="play-group" gid="'+r[i].id+'">'+r[i].name+'</a></li>';
				}
				$con.find(".playTab").html(html).find('.play-group').click(function(){
					getLotPlay($(this).attr("gid"),$con);
				});
			}
		});
	}
	function getLotPlay(groupId,$con){
		$.ajax({
			url:baseInfo.baseUrl+'/agent/lotPeiLv/list.do?groupId='+groupId,
			dataType:'json',
			success:function(i){
				var html = "",obj=null;
				for(var x in i.rows){
					obj=i.rows[x];
					if(x==0){
						html+='<li class="active">';
						$con.find("[name='playCode']").val(obj.code);
						$con.find("[name='platTypeId']").val(obj.id);
					}else{
						html+='<li>';
					}
					html+='<a href="#" data-toggle="tab" class="lot-play" lottype="'+obj.lotType+'" playcode="'+obj.code+'">'+obj.name+'</a></li>';
				}
				$con.find(".playSmallTab").html(html).find(".lot-play").click(function(){
					var $it=$(this);
					$con.find("[name='lotType']").val($it.attr("lottype"));
					$con.find("[name='playCode']").val($it.attr("playcode"));
					refresh($con);
				});
				refresh($con);
			}
		})
	}
	function refresh($con){
		var $table=$con.data("bootstrapTable");
		if($table && $table.length){
			$table.bootstrapTable('refresh');
		}else{
			Fui.addBootstrapTable({
				url : baseInfo.baseUrl+'/agent/cp/markSix/markSixList.do',
				columns : cols,
				onLoadSuccess:function(d){
					//单行保存
					d.find('a.saveRow').click(function(){
		 				var $it=$(this)
		 					,id = $it.attr('rowid')
		 					,$tr=$it.parents("tr")
		 					,odds  = $tr.find('[name="odds"]').val()//赔率
		 					,minBetAmmount=$tr.find('[name="minBetAmmount"]').val()//最小金额
		 					,maxBetAmmount=$tr.find('[name="maxBetAmmount"]').val()
		 					,sortNo=$tr.find('[name="sortNo"]').val();
	 					$.ajax({
	 						url : baseInfo.baseUrl+"/agent/cp/markSix/update.do",
	 						data : {
		 						'id' : id,
		 						'odds' : odds,
		 						'minBetAmmount' : minBetAmmount,
		 						'maxBetAmmount' : maxBetAmmount,
		 						'sortNo' : sortNo
		 					},
	 						success : function(result) {
	 							layer.msg(result.msg||'修改成功');
	 						}
	 					})
					});
				},
				onCreatedSuccessFun:function($table){
					$con.data("bootstrapTable",$table);
				}
			});
		}
	}
	return {
		render:function(conId){
			var $con=$("#"+conId),lotType='';
			$con.find("a.lotName").click(function(){
				lotType=$(this).attr("lid");
				$con.find("[name='lotType']").val(lotType);
				getLotPlayGroup(lotType,$con);
			});
			$con.find(".active a.lotName").click();
			//全部保存
			$con.find('.lottery_pei_lv_save_all').click(function(){
		 		layer.confirm('确定要保存本页面所有更改？', function() {
	 				var odds = ''//赔率
	 					,maxBetAmmount = ''//最大金额
	 					,minBetAmmount = ''//最小金额
	 					,playCode =$con.find('.playSmallTab>li.active>a').attr('playcode')
	 					,playCodes=''//玩法名称
	 					,sortNo = ''//排序
	 					,ids = '';
	 				$con.find('a.saveRow').each(function(){
						var $it=$(this)
		 					,id = $it.attr('rowid')
		 					,$tr=$it.parents("tr");
	 					odds  += $tr.find('[name="odds"]').val() + ',';
	 					minBetAmmount += $tr.find('[name="minBetAmmount"]').val() + ',';
	 					maxBetAmmount += $tr.find('[name="maxBetAmmount"]').val() + ',';
	 					playCodes += playCode+ ',';
	 					sortNo += $tr.find('[name="sortNo"]').val() + ',';
	 					ids += id + ',';
					});
	 				var bc = {
		 					"odds" : odds,	
		 					"maxBetAmmount" : maxBetAmmount,
		 					"minBetAmmount" : minBetAmmount,
		 					"playCode" : playCodes,
		 					"sortNo" : sortNo,
		 					"id" : ids,
		 					"lotType":lotType
		 				};
		 				$.ajax({
		 					url : baseInfo.baseUrl+"/agent/cp/markSix/saveAll.do",
		 					data : bc,
		 					success : function(j) {
		 						layer.msg(j.msg||'数据保存成功!', {icon: 1});
		 					}
		 				});
	 			});
			});
		}
	}
});