define(['jquery','bootstrap','Fui','fui_table'],function(){
	var cols = [{
		sortable:true,
		field : 'orderId',
		title : '订单号',
		align : 'center',
		valign : 'middle',
		formatter : orderFormatter
	}, {
		field : 'account',
		title : '投注账号',
		align : 'center',
		valign : 'middle',
		formatter : accountFormatter
	}, {
		title : '彩种名称',
		align : 'center',
		width : '100',
		valign : 'middle',
		formatter : function(value, row, index) {
			return '六合彩';
		}
	}, {
		field : 'qiHao',
		title : '期号',
		align : 'center',
		valign : 'middle'
	}, {
		field : 'playName',
		title : '玩法名称',
		align : 'center',
		width : '100',
		valign : 'middle'
	}, {
		field : 'haoMa',
		title : '投注号码',
		align : 'center',
		valign : 'middle',
		width : '90',
		formatter : wsFormatter
	}, {
		field : 'createTime',
		title : '投注时间',
		width : '150',
		align : 'center',
		formatter : Fui.formatDatetime
	}, {
		field : 'buyZhuShu',
		title : '注数',
		align : 'center',
		valign : 'middle',
		formatter : buyZSFormatter
	},{
		field : 'multiple',
		title : '倍数',
		align : 'center',
		valign : 'middle',
	},{
		field : 'model',//1元  10角 100分';
		title : '模式',
		align : 'center',
		valign : 'middle',
		width : '50',
		formatter : modeFormatter,
		pageSummaryFormat:function(rows,aggsData){
			return "小计:";
		},
		allSummaryFormat:function(rows,aggsData){
			return "总计:";
		}
	},{
		field : 'buyMoney',
		title : '投注金额(元)',
		align : 'center',
		valign : 'middle',
		width : '50',
		pageSummaryFormat:function(rows,aggsData){
			var r=0,row;
			for(var i=rows.length-1;i>=0;i--){
				row=rows[i];
				if(row.buyMoney != null && row.status < 4){
					r = r+row.buyMoney;
				}
			}
			return r.toFixed(2);
		},
		allSummaryFormat:function(rows,aggsData){
			if(!aggsData){
				return "0.00"
			}
			return aggsData.buySum ? aggsData.buySum.toFixed(2) : "0.00";
		}
	},{
		sortable:true,
		field : 'winMoney',
		title : '中奖金额(元)',
		align : 'center',
		valign : 'middle',
		width : '50',
		pageSummaryFormat:function(rows,aggsData){
			var r=0,row;
			for(var i=rows.length-1;i>=0;i--){
				row=rows[i];
				if(row.winMoney != null){
					r = r+row.winMoney;
				}
			}
			return r.toFixed(2);
		},
		allSummaryFormat:function(rows,aggsData){
			if(!aggsData){
				return "0.00"
			}
			return aggsData.winSum ? aggsData.winSum.toFixed(2) : "0.00";
		}
	},{
		sortable:true,
		field : 'lossMoney',
		title : '亏损金额(元)',
		align : 'center',
		valign : 'middle',
		width : '50',
		formatter : lossMoneyFormatter,
		pageSummaryFormat:function(rows,aggsData){
			var buySum=0,winSum=0,row;
			for(var i=rows.length-1;i>=0;i--){
				row=rows[i];
				if(row.buyMoney != null&& row.status < 4){
					buySum = buySum+row.buyMoney;
				}
				if(row.winMoney!=null&& row.status < 4){
					winSum=winSum+row.winMoney;
				}
			}
			
			var lossSum=buySum-winSum;
			return lossSum.toFixed(2);
		},
		allSummaryFormat:function(rows,aggsData){
			if(!aggsData){
				return "0.00";
			}else if(isNaN(aggsData.buySum)){
				return "0.00";
			}else if(isNaN(aggsData.winSum)){
				return (aggsData.buySum).toFixed(2);
			}
			
			
			return aggsData.buySum-aggsData.winSum ? (aggsData.buySum-aggsData.winSum).toFixed(2) : "0.00";
		}
	},{
		field : 'status',
		title : '状态',
		width : '100',
		align : 'center',
		valign : 'middle',
		formatter : statusFormatter
	}];
	
	function lossMoneyFormatter(value,row,index){
		var buy=row.buyMoney;
		var win=row.winMoney;
		
		if (isNaN(win)) { 
			return "未开奖";
		}
		var loss=buy-win;
		　
		return loss.toFixed(2);
	}
	
	
	function buyZSFormatter(value,row,index){
		if(row.status == 2 && value > 1){//有中奖才需要计算注数
			return ['<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/gameRecord/strToArray.do?orderId='+row.orderId+'" title="查看详情"><span class="text-danger">',value,'</span></a>' ].join('');;
		}
		return value;
	}
	
	
	function accountFormatter(value, row, index) {
		return ['<a class="open-dialog" href="',baseInfo.baseUrl,'/agent/member/manager/view.do?id=',row.accountId,'" title="查看详情"><span class="text-danger">',value,'</span></a>'].join('');
	}
	
	//位数格式化
	function wsFormatter(value, row, index) {
		var span = ['<span title="'+value+'">','</span>']
		if(value!=null && value.length>10){
			return  span.join(value.substring(0, 10)+"...");
		}else{
			return span.join(value);
		}
	}
	
	//格式化订单详情		
	function orderFormatter(value, row, index){
		if(value){
			return '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/gameRecord/showLotteryOrderDesc.do?orderId='+value+'">'+value+'</a>';
		}else{
			return value;
		}
	}
	
	function modeFormatter(value, row, index) {
		var col = '';
		switch(value){
		case 1:
			col = '元';
			break;
		case 10:
			col = '角';
			break;
		case 100:
			col = '分';
			break;
		}
		return col;
	}
	
	function statusFormatter(value, row, index) {
		var col = '';
		switch(row.status){
		case 1:
			col = '<span class="label label-primary" >等待开奖</span>';
			if(row.tzStatus == 1){
				col=col+'<div><a class="todo-ajax" href="'+baseInfo.baseUrl+'/agent/gameRecord/awardOrder.do?orderId='+row.orderId
				+"&lotCode="+row.lotCode+"&account="+row.account+'" title="确定要将订单['+row.orderId+']派奖？"><i class="glyphicon glyphicon-usd"></i>派奖</a></div>';
			}
			break;
		case 2:
			col = '<span class="label label-success" >已中奖</span>';
			break;
		case 3:
			col = '<span class="label label-danger" >未中奖</span>';
			break;
		case 4:
			col = '<span class="label label-info" >撤单</span>';
			break;
		case 5:
			col = '<span class="label label-success">派奖回滚成功</span>';
			break;
		case 6:
			col = '<span class="label label-warning">回滚异常的</span>';
			break;
		case 7:
			col = '<span class="label label-warning">开奖异常</span>';
			break;
		case 8:
			col = '<span class="label label-success">和局</span>';
			break;
		}
		if("${bcChgRole}" === "on"){
			col=col+'<div><a class="open-dialog" href="${base}/agent/gameRecord/showLotteryOrderDesc.do?orderId='+row.orderId+'">改单</a></div>';
		}
		return col;
	}
	//六合统计拼号码
	function tongJiTableHtml(){
		var html = '',num = 1;
		for(var i = 0;i<7;i++){
			html += '<tr>';
			for(var j = 0;j<7;j++){
				html += tongJiTrHtml(num);
				num++;	
			}
			html += '</tr>';
		}
		//集成大小单双
		html += '<tr>';
		html += tongJiTrHtml('大');
		html += tongJiTrHtml('小');
		html += tongJiTrHtml('单');
		html += tongJiTrHtml('双');
		html += tongJiTrHtml('合大');
		html += tongJiTrHtml('合小');
		html += tongJiTrHtml('合单');
		html += '</tr><tr>';
		html += tongJiTrHtml('合双');
		html += tongJiTrHtml('尾数大');
		html += tongJiTrHtml('尾数小');
		html += tongJiTrHtml('红波');
		html += tongJiTrHtml('绿波');
		html += tongJiTrHtml('蓝波');
		html += '</tr>';
		return html;
	}
	//六合统计号码
	function tongJiTrHtml(num){
		var col = '';
		col += '<td style="text-align:left;">号码:'+(num<10?'0':'')+num+'<br/>';
		col += '<font class="toGreen">特A投注:<span class="tatz'+(num<10?(num+' tatz0'+num):num)+'">0.00</span></font><br/>';
		col += '<font class="toGreen">特B投注:<span class="tbtz'+(num<10?(num+' tbtz0'+num):num)+'">0.00</span></font><br/>';
		col += '<font class="toRed">特A中奖:<span class="tazj'+(num<10?(num+' tazj0'+num):num)+'">0.00</span></font><br/>';
		col += '<font class="toRed">特B中奖:<span class="tbzj'+(num<10?(num+' tbzj0'+num):num)+'">0.00</span></font></td>';
		return col;
	}
	return {
		render:function(){
			var curBootstrapTable=null,curTab="record"
				,$con=$("#lottery_mark_six_con_wrap_id")
				,$form=$con.find(".fui-search")
				,$searchBtn=$con.find(".search-btn")
				,$tongjiTable=$con.find('.tongji-table');
			$con.find("a[oname]").click(function(){
				var $it=$(this),name=$it.attr("oname");
				switch(name){
				case "record":
					curTab="record";
					$tongjiTable.addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {pageNumber : 1});
					break;
				case "tongji":
					$con.find('.fixed-table-container').addClass("hidden");
					$con.find(".columns-right").addClass("hidden");
					$tongjiTable.removeClass("hidden");
					curTab="tongji";
					$searchBtn.click();
					break;
				}
			});
			$con.on("change","#groupCodeById",function(){
				var $this = $(this),html='',pcode='';
				if(!$this.val()){
					$con.find("#playCodeById").html('').parent().hide();
					return;
				}
				$.ajax({
					url:baseInfo.baseUrl+"/agent/gameRecord/getPlayByList.do",
					data:{groupId:$this.val()},
					success:function(j){
						if(j.success){
							$.each(j.playList,function(i,d){
								pcode += d.code + "|";
								html += '<option value="'+d.code+'">'+d.name+'</option>'
							})
							html = '<option value="'+pcode.substring(0,pcode.length-1)+'">--全部小类--</option>' + html;
							$con.find("#playCodeById").html(html).parent().show();
						}
					}
				})
			})
			$searchBtn.click(function(){
				if(curTab=="record"){
					$form.submit();
				}else{
					$.ajax({
						url:baseInfo.baseUrl+"/agent/gameRecord/getTmByCode.do",
						data:$form.serialize(),
						success:function(j){
							$tongjiTable.html(tongJiTableHtml());
							for(var i in j){
								var bm = '0.00',wm = '0.00'
									,hm = j[i].haoMa//投注号码
									,pc = j[i].playCode;//玩法编码 tm_a/tm_b
								if(j[i].buyMoney){
									bm = Fui.toDecimal(j[i].buyMoney,2);//投注金额
								}
								if(j[i].winMoney){
									wm = Fui.toDecimal(j[i].winMoney,2);//中奖金额
								}
								if(pc == 'tm_a'){
									$tongjiTable.find('.tatz'+hm).html(bm);
									$tongjiTable.find('.tazj'+hm).html(wm);
								}else if(pc == 'tm_b'){
									$tongjiTable.find('.tbtz'+hm).html(bm);
									$tongjiTable.find('.tbzj'+hm).html(wm);
								}
							}
						}
					});
				}
			});
			Fui.addBootstrapTable({
				url : baseInfo.baseUrl+'/agent/gameRecord/list.do',
				showPageSummary:true,
				showAllSummary:true,
				showFooter : true,
				columns :cols,
				onCreatedSuccessFun:function($table){
					curBootstrapTable=$table;
				}
			});
		}
	}
});