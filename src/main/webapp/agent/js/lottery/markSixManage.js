define(['jquery','bootstrap','Fui','fui_table'],function(){
	var cols = [ {
			field : 'name',
			title : '号码',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'odds',
			title : '赔率',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : oddsInputFormatter
		}, {
			field : 'minBetAmmount',
			title : '最小下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : minBetInputFormatter
		}, {
			field : 'maxBetAmmount',
			title : '最大下注金额',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : maxBetInputFormatter
		}, {
			field : 'sortNo',
			title : '序号',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : sortNoInputFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : statusFormatter
		},{
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : operateFormatter
		}],
		bigCols=[ {
			field : 'lotType',
			title : '彩票类型',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : getLotteryTypeName
		}, {
			field : 'name',
			title : '玩法分组名称',
			align : 'center',
			width : '180',
			valign : 'bottom'
		}, {
			field : 'sortNo',
			title : '序号',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : bigStatusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '50',
			valign : 'middle',
			formatter : bigOperateFormatter
		} ],
		smallCols=[{
			field : 'name',
			title : '玩法小类名称',
			align : 'center',
			width : '200',
			valign : 'middle'
		}, {
			field : 'sortNo',
			title : '序号',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : sortNoInputFormatter
		}, {
			field : 'status',
			title : '状态',
			align : 'center',
			width : '200',
			valign : 'middle',
			formatter : smallStatusFormatter
		}, {
			title : '操作',
			align : 'center',
			width : '150',
			valign : 'middle',
			formatter : smallOperateFormatter
		}];
	function oddsInputFormatter(value, row, index) {
		return '<input name="odds" class="form-control" type="text" value="'+value+'"/>';
	}
	function minBetInputFormatter(value, row, index) {
		return '<input name="minBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function maxBetInputFormatter(value,row,index){
		return '<input name="maxBetAmmount" class="form-control" type="text" value="'+value+'"/>';
	}
	function sortNoInputFormatter(value, row, index) {
		return '<input name="sortNo" class="form-control" type="text" value="'+value+'"/>';
	}
	function operateFormatter(value, row, index) {
		return '<a class="save saveRow" rowid="'+row.id+'" href="javascript:void(0);" title="保存"><i class="glyphicon glyphicon-edit"></i>保存</a>';
	}
	function statusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/cp/markSix/closeOrOpen.do?id="+row.id+"&status="});
	}
	function bigStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/cp/lotTypeGroup/updateStatus.do?id="+row.id+"&status="});
	}
	function bigOperateFormatter(value, row, index) {
		return '<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/cp/lotTypeGroup/modify.do?id='+row.id+'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	function smallStatusFormatter(value, row, index) {
		return Fui.statusFormatter({val:value,url:baseInfo.baseUrl+"/agent/cp/markSix/updateStatus.do?id="+row.id+"&status="});
	}
	function smallOperateFormatter(value, row, index) {
		return '<a class="doSave" href="#" title="保存" rowid="'+row.id+'"><i class="glyphicon glyphicon-ok"></i></a>   '
				+'<a class="open-dialog" href="'+baseInfo.baseUrl+'/agent/cp/markSix/modify.do?id='+row.id+'" title="修改"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	function getLotPlayGroup(lotType,$con,callback) {
		$.ajax({
			url : baseInfo.baseUrl+"/agent/cp/markSix/bigList.do?lotType="+lotType,
			type : "get",
			dataType : 'json',
			success : function(r) {
				if(r==null || r==""){
					$con.find(".playTab").html('');
					$con.find(".playSmallTab").html('');
					return;
				}
				var html="";
				for(var i=0;i<r.length;i++){
					if(i==0){
						html+='<li class="active">';
						getLotPlay(r[0].id,$con,callback);
					}else{
						html+='<li>';
					}
					html+='<a href="#" data-toggle="tab" class="play-group" gid="'+r[i].id+'">'+r[i].name+'</a></li>';
				}
				$con.find(".playTab").html(html).find('.play-group').click(function(){
					getLotPlay($(this).attr("gid"),$con,callback);
				});
			}
		});
	}
	function getLotPlay(groupId,$con,callback){
		$.ajax({
			url:baseInfo.baseUrl+'/agent/cp/markSix/list.do?groupId='+groupId,
			dataType:'json',
			success:function(i){
				var html = "",obj=null;
				for(var x in i.rows){
					obj=i.rows[x];
					if(x==0){
						html+='<li class="active">';
						$con.find("[name='playCode']").val(obj.code);
					}else{
						html+='<li>';
					}
					html+='<a href="#" data-toggle="tab" class="lot-play" lottype="'+obj.lotType+'" playcode="'+obj.code+'">'+obj.name+'</a></li>';
				}
				$con.find(".playSmallTab").html(html).find(".lot-play").click(function(){
					var $it=$(this);
					$con.find("[name='lotType']").val($it.attr("lottype"));
					$con.find("[name='playCode']").val($it.attr("playcode"));
					callback();
				});
				callback();
			}
		})
	}
	function peiLvLoadSuccessFn(d){
		//单行保存赔率设置
		d.find('a.saveRow').click(function(){
			var $it=$(this)
				,id = $it.attr('rowid')
				,$tr=$it.parents("tr")
				,odds  = $tr.find('[name="odds"]').val()//赔率
				,minBetAmmount=$tr.find('[name="minBetAmmount"]').val()//最小金额
				,maxBetAmmount=$tr.find('[name="maxBetAmmount"]').val()
				,sortNo=$tr.find('[name="sortNo"]').val();
				$.ajax({
					url : baseInfo.baseUrl+"/agent/cp/markSix/update.do",
					data : {
						'id' : id,
						'odds' : odds,
						'minBetAmmount' : minBetAmmount,
						'maxBetAmmount' : maxBetAmmount,
						'sortNo' : sortNo
					},
					success : function(result) {
						layer.msg(result.msg||'修改成功');
					}
				})
		});
		d.find(".doSave").click(function(){
			var $it=$(this),$tr=$it.parents("tr");
			var blp = {
				'id' : $it.attr("rowid"),
				'sortNo' :  $tr.find("[name='sortNo']").val()
			};
			$.ajax({
				url : baseInfo.baseUrl+"/agent/cp/markSix/updateType.do",
				data : blp,
				success : function(result){
					layer.msg(result.msg||"保存成功");
				}
			})
		});
	}
	return {
		render:function(){
			var $con=$("#mark_six_pei_lv_con_wrap_id"),curBootstrapTable=null,$lottery_xgc=$("#lottery_xgc"),type=$lottery_xgc.val();
			$lottery_xgc.change(function(){
				type=$lottery_xgc.val();
				$con.find(".nav-tabs li.active>a").click();
			});
			$con.on("click","a.lotName",function(){
				var n=$(this).attr("oname");
				$con.find(".play-info-wap").removeClass("hidden");
				if('peilv'==n){
					$con.find(".play-info-wap").find(".btn").removeClass("hidden");
					$con.find(".play-info-wap").find(".playSmallTab").removeClass("hidden");
					getLotPlayGroup(type,$con,initTable);
				}else if(n=='dalei'){
					$con.find(".play-info-wap").addClass("hidden");
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : bigCols,
						url : baseInfo.baseUrl+'/agent/cp/lotTypeGroup/list.do?lotType='+type,
						pageNumber : 1
					});
				}else if(n=='xiaolei'){
					$con.find(".play-info-wap").find(".btn").addClass("hidden");
					$con.find(".play-info-wap").find(".playSmallTab").addClass("hidden");
					$.ajax({
						url : baseInfo.baseUrl+"/agent/cp/markSix/bigList.do?lotType="+type,
						type : "get",
						dataType : 'json',
						success : function(r) {
							if(r==null || r==""){
								$con.find("[name='groupId']").val("");
								$con.find(".playTab").html('');
							}else{
								var html="",active='';
								for(var i=0;i<r.length;i++){
									if(i==0){
										$con.find("[name='groupId']").val(r[0].id);
										active=" active";
									}else{
										active="";
									}
									html+='<li class="smallType'+active+'"><a href="#" class="lotGroupId" groupid="'+r[i].id+'">'+r[i].name+'</a></li>';
								}
								$con.find(".playTab").html(html);
								curBootstrapTable.bootstrapTable('refreshOptions', {
									columns : smallCols,
									url : baseInfo.baseUrl+'/agent/cp/markSix/list.do',
									pageNumber : 1
								});
								$con.find('.lotGroupId').click(function(){
									$con.find('.smallType').removeClass("active");
									$(this).parent().addClass("active");
									$con.find("[name='groupId']").val($(this).attr("groupid"));
									if(curBootstrapTable){
										curBootstrapTable.bootstrapTable('refreshOptions', {
											columns : smallCols,
											url : baseInfo.baseUrl+'/agent/cp/markSix/list.do',
											pageNumber : 1
										});
									}
								});
							}
						}
					});
				}
			});
			function initTable(){
				if(curBootstrapTable){
					curBootstrapTable.bootstrapTable('refreshOptions', {
						columns : cols,
						url :  baseInfo.baseUrl+'/agent/cp/markSix/markSixList.do?lotType='+type,
						pageNumber : 1
					});
					return true;
				}
				Fui.addBootstrapTable({
					url : baseInfo.baseUrl+'/agent/cp/markSix/markSixList.do?lotType='+type,
					columns : cols,
					onLoadSuccess:peiLvLoadSuccessFn,
					onCreatedSuccessFun:function($table){
						curBootstrapTable=$table;
					}
				});
			}
			getLotPlayGroup(type,$con,initTable);
			//全部保存
			$con.find('.lottery_pei_lv_save_all').click(function(){
		 		layer.confirm('确定要保存本页面所有更改？', function() {
	 				var odds = ''//赔率
	 					,maxBetAmmount = ''//最大金额
	 					,minBetAmmount = ''//最小金额
	 					,playCode =$con.find('.playSmallTab>li.active>a').attr('playcode')
	 					,playCodes=''//玩法名称
	 					,sortNo = ''//排序
	 					,ids = '';
	 				$con.find('a.saveRow').each(function(){
						var $it=$(this)
		 					,id = $it.attr('rowid')
		 					,$tr=$it.parents("tr");
	 					odds  += $tr.find('[name="odds"]').val() + ',';
	 					minBetAmmount += $tr.find('[name="minBetAmmount"]').val() + ',';
	 					maxBetAmmount += $tr.find('[name="maxBetAmmount"]').val() + ',';
	 					playCodes += playCode+ ',';
	 					sortNo += $tr.find('[name="sortNo"]').val() + ',';
	 					ids += id + ',';
					});
	 				var bc = {
		 					"odds" : odds,	
		 					"maxBetAmmount" : maxBetAmmount,
		 					"minBetAmmount" : minBetAmmount,
		 					"playCode" : playCodes,
		 					"sortNo" : sortNo,
		 					"id" : ids
		 				};
		 				$.ajax({
		 					url : baseInfo.baseUrl+"/agent/cp/markSix/saveAll.do",
		 					data : bc,
		 					success : function(j) {
		 						layer.msg(j.msg||'数据保存成功!', {icon: 1});
		 					}
		 				});
	 			});
			});
		}
	}
});