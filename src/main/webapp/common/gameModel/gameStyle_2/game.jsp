<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/include/base.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<title>${_title}</title>
</head>
<body>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<%-- <link href="${base}/common/gameModel/gameStyle_2/css/style.css" rel="stylesheet" type="text/css" /> --%>
<div class="content-ti">
    <div class="inner inner2">
      <div class="h38"></div>
      <ul class="ticai" id="lotterys" style="width: 1000px;margin: auto;">
      </ul>
      <div class="h17"></div>
    </div>
  </div>
  <input type="hidden" value="${domainFolder}" id="domainFolder" />
  <script>
  $(function(){
		lottery();
    if ($("#domainFolder").val() == "a010101") {
      $(".content-ti").css("margin","0 auto")
    }
	});


	var lottery = function(){
	    $.ajax({
	       'url' : "${base}/getConfig/getLotList.do?timeStamp=" + new Date().getTime(),
	        dataType : 'json',
	        success : function(list) {
	          var col = '';
	          for(var i=0;i<list.length;i++){
	              var value = list[i];
	              col+='<li class="bgColor" style="border-radius: 10px;">';
	              if (value.imgUrl != null){
                    col+='<img style="width:104px;height:104px;" src="'+value.imgUrl+'" alt="logo" class="fl">';
                  }else {
                    col+='<img style="width:104px;height:104px;" src="${base}/common/gameModel/gameStyle_2/images/'+value.code+'.png" alt="" class="fl">';
                  }
	              col+='<div class="fl tizi"><h6>'+value.name+'</h6><a href="javascript:void(0)" id="'+value.code+'" class="tac aLoginCheck" style="opacity: 1;"><img src="${base}/common/gameModel/gameStyle_2/images/liji.png?=664" alt=""></a>';
	              col+='</div></li>';
	          }
	          $('#lotterys').html(col);
	          $('.bgColor').css("background-color","${bgColor}");
// 	          if("${ticai}"){
// 	        	  $('.ticai').css("width","984px;margin: auto;");
// 	          }else{
// 	        	  $('.ticai').css("margin","0 auto");
// 	          }

	          $('.aLoginCheck').click(function(){
	        	  if(${isLogin}){
	        		 var code = this.id;
					 if(${lotteryVersion eq 'jiebao'}){
						 parent.location.href="${base}/lottery/index.do?lotCode="+code;
					 }else{
						 location.href="${base}/lottery/index.do?isShowTopNav=abc&lotCode="+code;
					 }
	        	  }else{
	        		  alert('请先登陆后再进行游戏');
	        	  }

	          });
	        }
	      });
	}
  </script>
<style>
.isOffTop{
	display: none;
}
</style>
  <style>
.main-bg{background-color: #dddfe4;overflow: hidden;zoom:1;}
.top{height:36px;background:#031a2e;border-bottom:1px solid #063053}
.huang{color:#fce00f}
a{text-decoration:none}
.top a{color:#fff}
.h99{height: 99px;}
.header{background:#04223c;height:99px;border-top:1px solid #0e0e0e;border-bottom:1px solid #05355f}
.hlogin{float:right;width:736px}
.dinline{display:inline-block}
.ml5{margin-left: 5px;}
.mg-con{padding: 5px 18px 5px 18px;}
.l{float: left;}
.h5{height: 5px;}
.dn{display: none;}
.h413{height: 413px;}
.tr{text-align: right;}
.swff {position: relative;}
.swf {position: absolute;top: -337px;left: -222px;z-index: 999;width: 100%;}
.pos{position: absolute;left:50%;margin-left: -960px;}
/*==清除浮动==*/
.clearfix:before, .clearfix:after { content:""; display:table;}
.clearfix:after { clear:both;}
.clearfix { *zoom:1;}
/*== 用户登录 ==*/
.form-yzm-img{position: absolute;right: 0px;top:0px;}
.hd-login{position: absolute;width: 626px;height: 31px;right: 0px;top:10px;}
.hd-login-box{height: 23px;width: 100%;}
.input-box li.wid{background:#011c34;border:1px solid #01325d;border-radius:12px;color:#fff;padding:0 10px;width:110px;font-size:12px;outline: none;float: left;margin-right: 3px;}
.input-box li .intxt{width: 110px;background: transparent;height:22px;line-height: 22px;color: #fff;border:none;}
.denglu{width:51px;height:31px;background:url(${base}/common/gameModel/gameStyle_2/images/denglu.png) no-repeat center top;border:none;outline:0}
.hd-nav{position: absolute;right: 0px;bottom: 0px;}
.nav{height:56px;}
.nav li{float:left;text-align:center;width:92px}
.nav li a{color:#fff;display:block;line-height:56px;text-align:center;height:56px;border-radius:5px;font-size: 15px;}
.nav li.cur a{background:#05355f}
.nav li a:hover{background:#05355f}
.nav li a span{display:block;padding-top:16px;font-family: "微软雅黑";font-weight: bold;}
.nav li a p{font-size:12px;line-height:20px;display:block;-webkit-transform:scale(0.8);}
.rel{position: relative;}
/*==副導覽列==*/
#LS-navs,#LS-nav,#game_nav,#phone_nav{position:absolute;top:58px;z-index:9999;border:1px solid #405858;}
#LS-navs a,#LS-nav a,#game_nav a,#phone_nav a{background:#04223c;color:#fff;height:28px;line-height:28px;overflow:hidden;display:inline;float:left;text-decoration:none;text-align:center;position:relative;padding: 0px 10px;}
#LS-navs a:hover,#LS-nav a:hover,#game_nav a:hover,#phone_nav a:hover{color:#ff0;}
/* news */
 .com-news {width:1036px;height:38px;margin:0 auto;}
.com-news .notice {font-size: 12px;height: 38px;position: relative;width: 1036px;}
.Gong .Con {float: left;height: 38px;line-height: 38px;overflow: hidden;width: 890px;}
.Gong .Con ul {height: 38px;width: 890px;}
.Gong .Con ul li {clear: both;height: 38px;line-height: 38px;}
.msg-content {height: 38px;line-height: 38px;overflow: hidden;position: relative;width: 805px;}
.msg-content span {cursor: pointer;margin-left: 700px;white-space: nowrap;}
.com-news .notice h2 {display:block;width:132px;height:38px;float:left;}
.com-news .prevnext {width:62px;height:28px;position:absolute;top:5px;right:20px;}
.com-news .prevnext span {display:block;width:28px;height:28px;float:left;cursor:pointer;}
.com-news .prevnext span.prev{background:url(${base}/common/gameModel/gameStyle_2/images/leftbtn.png) no-repeat left center;margin-right: 5px;}
.com-news .prevnext span.next{float:right;background:url(${base}/common/gameModel/gameStyle_2/images/rightbtn.png) no-repeat right center;}

.banner-wrap{height:413px;}
#sliders{list-style:none; width:100%; height:100%; margin:0; padding:0; }
#sliders li{width:100%; height:413px; display:none; position:absolute;left:50%;margin-left: -960px;}
#naviSlider{list-style:none;  margin:0 auto; width:auto; text-align:center; margin-top:3px; *margin-top:5px;    }
#naviSlider li{height:5px; width:20px;cursor:pointer; display:inline-block;*display:inline; *zoom:1; background: url(${base}/common/gameModel/gameStyle_2/images/radio-off.jpg) no-repeat;cursor: pointer;margin-right: 5px; }
#naviSlider li.on{background: url(${base}/common/gameModel/gameStyle_2/images/radio-on.jpg) no-repeat;}
.rad-pos{position: absolute;bottom: 10px;right:382px;}
/* footer */
.ft-pic{background: url(${base}/common/gameModel/gameStyle_2/images/fimg.png) no-repeat;width: 637px;height: 42px;position: relative;}
.opear-pos{width: 90px;height: 27px;position: absolute;left:102px;top:8px;}
.footer{
  background:#011c34;
}
.fnavul{
  float:left;
  padding:16px;
  padding-right:18px;
  padding-bottom:0px;
  border-right:1px solid #023560;
  width: 130px;
  padding-top:0px;
  border-left:1px solid #000000;
}
.fnavul .tou{
  color:#fff54e;
  font-size: 18px;
  font-weight: bold;
  border-bottom:1px solid #003866;
  line-height:42px;
}
.fnavul .fnavitem  {
  padding-left:18px;
  background:url(${base}/common/gameModel/gameStyle_2/images/baidian.png) no-repeat center left;
}
.fnavul .fnavitem a{
  color: white;
  font-size: 12px;
}
.fdao{
  color: white;
  font-size: 13px;
}
.fdao a{
  color: white;
  font-size: 12px;
}
.fdao a:hover{
  text-decoration:underline;
}


.contentindex{
  background:#e7e8ec;
}
.indextu a{
  height: 293px;
  margin-right:24px;
  position:relative;
  display: inline-block;
  border:3px solid #297CFF;
}
.indextu a:hover{
  border:3px solid #fffc00;
}
.indextu a p.jinruzhe{
  position:absolute;
  left: 15px;
  top: 13px;
  display:none;
}
.indextu a:hover p.jinruzhe{
  display:block;
}
.hong{
  color:red;
}

.xlgnav a{
  padding:0px 8px;
}

.shixunul li{
  display:inline-block;
  width: 305px;
  margin:0 18px;
  margin-bottom:28px;
  *display: inline;
  zoom:1;
}
.shixunul li a.imga{
  display:block;
  height: 166px;
}
.shixunul li a.imga img{
  height: 166px;
}
.shixunul li .jinruguize{
  height: 34px;
  background: url(${base}/common/gameModel/gameStyle_2/images/jinruguize.png) no-repeat center top;
  text-align:center;
}
.shixunul li .jinruguize  a{
  color: white;
  padding:0px 22px;
  line-height:34px;
}
.shixunul li .jinruguize  a:hover{
  color: yellow;
}
.yhx2{
  display:none;
}



/*05-彩票游戏*/
.banner-ti{
  height:209px ;
}
.content-ti{
/*   background: #E7E8EC; */
  overflow: hidden;
  width: 1000px;
}
.inner2{
/*   background: #DDDFE4; */
  overflow: hidden;
}
.ticai{
/*   width: 984px; */
/*   margin:0 auto; */
  overflow: hidden;
}
.ticai li{
  float: left;
  width: 297px;
  padding-left: 7px;
  margin:0 12px;
  margin-bottom: 19px;
/*   background: url(${base}/common/gameModel/gameStyle_2/images/lili.png) no-repeat top center; */
/*   background-color:firebrick; */
  height: 112px;
  padding-top: 7px;

}

.tizi{
  float: left;
  width: 151px;
  padding-left: 18px;
  padding-top: 6px;
}
.tizi h6{
  height: 31px;
  background: url(${base}/common/gameModel/gameStyle_2/images/lih.png) no-repeat top center;
  font-size: 16px;
  line-height: 30px;
  color: #fff518;
  margin-bottom: 8px;
  text-align: center;

}
.tizi a{
  display: block;
  line-height: 76px;
}
/*05-彩票游戏*/
/*06-电子游艺*/
.gameul li{width: 317px;height: 246px;float: left;margin: 10px 0 10px 20px;}

/*07-免费注册*/
.banner-mian{
  height: 209px;

}
.left-er{
  float: left;
  width: 266px;
}
.leftin{
  width: 208px;
  margin:0 auto;
  background: url(${base}/common/gameModel/gameStyle_2/images/left-er.png) no-repeat top center;

}
.side {
  padding-top: 2px;
  background: url(${base}/common/gameModel/gameStyle_2/images/sidexa.png) no-repeat top right;
  margin-left: 25px;
  width: 175px;
}
.side li{
  background: url(${base}/common/gameModel/gameStyle_2/images/sidexa.png) no-repeat bottom right;
  padding-bottom: 2px;

}
.side li a{
  display: block;
  font-size: 16px;
  line-height: 38px;
  height: 39px;
  color: white;
  text-align: center;
}
.side li a:link , .side li a:visited{}
.side li a:hover{
  color: #08548c;
  background: url(${base}/common/gameModel/gameStyle_2/images/sia.png) no-repeat right center;
}
.right-er{
  float: left;
  width: 770px;
  background: #EEEEF1;
  min-height:630px;
    height:auto !important;
    height:630px;
    overflow:visible;
}

.riin {
    margin: 0 auto;
    width: 705px;
}

.riin h5 {
    color: #000;
    font-size: 20px;
    line-height: 36px;
}

.duan {
    padding-bottom: 20px;
}
.duan p {
    color: #000;
    line-height: 22px;
}
/*07-免费注册*/
/*== 文章样式 ==*/
.col-or{color:#FF8C00;}
.h-til {color:#FF8C00;font-size:12px;margin:10px 0;font-weight: bold;}
.h-ytil {color:#FF8C00;font-size:15px;margin:10px 0;font-weight: bold;}
.mtab-menual{padding-left:18px;font-family: "微软雅黑";}
.mtab-menual li.mtab a,.mtab-menual li a:hover{background:#fff100;color:#ff0000;width: 117px;height: 25px;line-height: 25px;text-align: center;display: block}
.mtab-menual li {height: 25px;line-height: 25px;text-align: center;width: 117px;background:#04223c;color: #fff;float: left;margin-right: 5px;cursor: pointer;}
.mtab-menual li a{color: #fff;}
.articles-con {padding: 10px;}
.articles-con h3{font-size: 20px;line-height: 30px;}
.articles-con h1 {font-weight: normal;margin-bottom: 10px;padding-left: 15px;}
.articles-con p{line-height: 28px;}
.right-er table {border-collapse: collapse;border: 1px solid #000;border-radius: 2px;border-spacing: 0;margin: 0 auto;text-align: center;width: 670px;color:#fff;}
.right-er table th{font-weight:bold;background-color:#969ba0}
.right-er table th, .right-er table td {border-bottom: 1px solid #000;border-right: 1px solid #000;text-align: center;color: #000;}
.icon-attr{width: 18px;height: 16px;display: block;float: left;margin: 5px 10px 0px 0px;}
.list-icon-a{background: url(${base}/common/gameModel/gameStyle_2/images/abount/abount-a-icon.png) no-repeat;}
.list-icon-q{background: url(${base}/common/gameModel/gameStyle_2/images/abount/abount-q-icon.png) no-repeat;}
/*==注册==*/
.formular {color: #fff;line-height: 1.5;margin: 0 auto;text-align: left;width: 750px;color: #000;}
.formular p label, .formular p input, .formular p select, .formular p img, .formular p span {display: inline-block;float: left;}
.formular .star {color: #000;float: none;}
.formular fieldset {border: 1px solid #777778;margin: 0 auto;color: #000;margin: 15px 0px 0px 15px;padding: 15px 0px 15px 20px;}
.formular legend {color: #000;font-size: 14px;font-weight: bold;padding: 0px 10px;}
.formular p {height: 29px;line-height: 28px;}
.formular p label {text-align: right;width: 135px;}
.formular p select {background:#4f5052;border: 1px solid #96876a;color: #fff;height: 24px;line-height: 24px;margin-top: 5px;margin-right: 10px;}
.formular p input{width: 180px;padding-left: 10px;height: 26px;background: #B0B0B0;border:1px solid white;border-radius: 6px;outline: none;font-size: 14px;line-height: 26px;}
.formular p.memCash_text {margin: 0 0 0 135px;}
.formular .mar {margin: 11px 0 0 34px;}
.HiddenInput,#HiddenInput {height: 0;visibility: hidden; width: 0;}
.formular p input.HiddenInput{height: 0;visibility: hidden; width: 0;}
.reg-select {border: 0 none;margin-right: 10px;padding: 0;}
.reg-agree{margin: 10px 0px;padding-left: 35px;color: #000;}
.reg-btn-attr{width: 70px;height: 36px;font-size: 20px;line-height: 36px;text-align: center;color: white;margin:0 7px;border:none;cursor: pointer;outline: none;background: #07528B;}
.reg-input-text{width: 160px;}
.reg-agree a{color: #000;}



/*== 客服 ==*/
.floatDiv a{ display: block;
    font-size: 0;
    overflow: hidden;
    position: relative;
    text-align: center;
    }

#kinMaxShow .KMSPrefix_kinMaxShow_button li {
    background: url("${base}/common/gameModel/gameStyle_2/images/radio-on.jpg") no-repeat scroll 0 top;
    border: 0 none;
    cursor: pointer;
    float: left;
    height: 5px;
    margin-right: 5px;
    width: 20px;
}
#kinMaxShow .KMSPrefix_kinMaxShow_button li.KMSPrefix_kinMaxShow_focus {
    background: url("${base}/common/gameModel/gameStyle_2/images/radio-off.jpg") no-repeat scroll;
    border: 0 none;
    cursor: default;
}
/*==
  网站公用css 文件
==*/
/*==IE 6.0 背景图片缓存操作==*/
/* .tabcell{display:table-cell;} */
/* html{
  color:#000;
  font-size:14px;
  font-weight:400;
  line-height:24px;
  font-family:STHeiti,Arial,"Microsoft Yahei","Hiragino Sans GB","Simsun,sans-self"}
  blockquote,body,code,dd,div,dl,dt,fieldset,form,h1,h2,h3,h4,h5,h6,input,legend,li,ol,p,pre,td,textarea,th,ul{
    margin:0;padding:0;font-size:14px;font-weight:400;line-height:24px;font-family:STHeiti,Arial,"Microsoft Yahei","Hiragino Sans GB","Simsun,sans-self"}
    table{border-collapse:collapse;border-spacing:0}
    fieldset,img{border:0}address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:400}
    ul{list-style:none}
    caption,th{text-align:left}
    h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:400}
    q:after,q:before{content:''}
    abbr,acronym{border:0;font-variant:normal}
    sup{vertical-align:text-top}
    sub{vertical-align:text-bottom}
    input,select,textarea{font-family:inherit;font-size:inherit;font-weight:inherit}
    legend{color:#000} */
    .co000{color:#000}.co222{color:#222}.co333{color:#333}.co444{color:#444}.co555{color:#555}.co666{color:#666}.co777{color:#777}.co888{color:#888}
    .co999{color:#999}.coaaa{color:#aaa}.cobbb{color:#bbb}.coccc{color:#ccc}.coddd{color:#ddd}.coeee{color:#eee}.cofff{color:#fff}
    .bgwhite{background:#fff}
    .bgccc{background:#ccc}
    .bgf2{background:#f2f2f2}
    .rst{padding:0;margin:0}
    #yui3-css-stamp.cssreset{display:none}
    .fwb{font-weight:700}
    .cl{clear:both}
    .fl{float:left}
    .fr{float:right}
    .cw{color:#fff}
    .cr{color:red}
    .tac{text-align:center}
    .tar{text-align:right}
    .tal{text-align:left}
    .ov{overflow:hidden}
    .f12{font-size:12px}
    .f13{font-size:13px}
    .f14{font-size:14px}
    .f15{font-size:15px}
    .f16{font-size:16px}
    .f17{font-size:17px}
    .f18{font-size:18px}
    .f19{font-size:19px}
    .f20{font-size:20px}
    .f21{font-size:21px}
    .f22{font-size:22px}
    .f23{font-size:23px}
    .f24{font-size:24px}
    .f25{font-size:25px}
    .f26{font-size:26px}
    .f27{font-size:27px}
    .f28{font-size:28px}
    .f29{font-size:29px}
    .f30{font-size:30px}
    .f31{font-size:31px}
    .f32{font-size:32px}
    .f33{font-size:33px}
    .f34{font-size:34px}
    .f35{font-size:35px}
    .f36{font-size:36px}
    .f37{font-size:37px}
    .f38{font-size:38px}
    .f39{font-size:39px}
    .f40{font-size:40px}
    .f41{font-size:41px}
    .f42{font-size:42px}
    .f43{font-size:43px}
    .f44{font-size:44px}.f45{font-size:45px}.f46{font-size:46px}.f47{font-size:47px}.f48{font-size:48px}.f49{font-size:49px}
    .w1{width:1px}.w2{width:2px}.w3{width:3px}.w4{width:4px}.w5{width:5px}.w6{width:6px}.w7{width:7px}.w8{width:8px}.w9{width:9px}
    .w10{width:10px}.w11{width:11px}.w12{width:12px}.w13{width:13px}.w14{width:14px}.w15{width:15px}.w16{width:16px}.w17{width:17px}
    .w18{width:18px}.w19{width:19px}.w20{width:20px}.w21{width:21px}.w22{width:22px}.w23{width:23px}.w24{width:24px}.w25{width:25px}
    .w26{width:26px}.w27{width:27px}.w28{width:28px}.w29{width:29px}.w30{width:30px}.w31{width:31px}.w32{width:32px}.w33{width:33px}
    .w34{width:34px}.w35{width:35px}.w36{width:36px}.w37{width:37px}.w38{width:38px}.w39{width:39px}.w40{width:40px}.w41{width:41px}
    .w42{width:42px}.w43{width:43px}.w44{width:44px}.w45{width:45px}.w46{width:46px}.w47{width:47px}.w48{width:48px}.w49{width:49px}
    .w50{width:50px}.w51{width:51px}.w52{width:52px}.w53{width:53px}.w54{width:54px}.w55{width:55px}.w56{width:56px}.w57{width:57px}
    .w58{width:58px}.w59{width:59px}.w60{width:60px}.w61{width:61px}.w62{width:62px}.w63{width:63px}.w64{width:64px}.w65{width:65px}
    .w66{width:66px}.w67{width:67px}.w68{width:68px}.w69{width:69px}.w70{width:70px}.w71{width:71px}.w72{width:72px}.w73{width:73px}
    .w74{width:74px}.w75{width:75px}.w76{width:76px}.w77{width:77px}.w78{width:78px}.w79{width:79px}.w80{width:80px}.w81{width:81px}
    .w82{width:82px}.w83{width:83px}.w84{width:84px}.w85{width:85px}.w86{width:86px}.w87{width:87px}.w88{width:88px}.w89{width:89px}
    .w90{width:90px}.w91{width:91px}.w92{width:92px}.w93{width:93px}.w94{width:94px}.w95{width:95px}.w96{width:96px}.w97{width:97px}
    .w98{width:98px}.w99{width:99px}.w100{width:100px}.w101{width:101px}.w102{width:102px}.w103{width:103px}.w104{width:104px}.w105{width:105px}.w106{width:106px}
    .w107{width:107px}.w108{width:108px}.w109{width:109px}.w110{width:110px}.w111{width:111px}.w112{width:112px}.w113{width:113px}.w114{width:114px}.w115{width:115px}
    .w116{width:116px}.w117{width:117px}.w118{width:118px}.w119{width:119px}.w120{width:120px}.w121{width:121px}.w122{width:122px}.w123{width:123px}.w124{width:124px}
    .w125{width:125px}.w126{width:126px}.w127{width:127px}.w128{width:128px}.w129{width:129px}.w130{width:130px}.w131{width:131px}.w132{width:132px}.w133{width:133px}
    .w134{width:134px}.w135{width:135px}.w136{width:136px}.w137{width:137px}.w138{width:138px}.w139{width:139px}.w140{width:140px}.w141{width:141px}.w142{width:142px}
    .w143{width:143px}.w144{width:144px}.w145{width:145px}.w146{width:146px}.w147{width:147px}.w148{width:148px}.w149{width:149px}.w150{width:150px}.w151{width:151px}
    .w152{width:152px}.w153{width:153px}.w154{width:154px}.w155{width:155px}.w156{width:156px}.w157{width:157px}.w158{width:158px}.w159{width:159px}.w160{width:160px}
    .w161{width:161px}.w162{width:162px}.w163{width:163px}.w164{width:164px}.w165{width:165px}.w166{width:166px}.w167{width:167px}.w168{width:168px}.w169{width:169px}
    .w170{width:170px}.w171{width:171px}.w172{width:172px}.w173{width:173px}.w174{width:174px}.w175{width:175px}.w176{width:176px}.w177{width:177px}.w178{width:178px}
    .w179{width:179px}.w180{width:180px}.w181{width:181px}.w182{width:182px}.w183{width:183px}.w184{width:184px}.w185{width:185px}.w186{width:186px}.w187{width:187px}
    .w188{width:188px}.w189{width:189px}.w190{width:190px}.w191{width:191px}.w192{width:192px}.w193{width:193px}.w194{width:194px}.w195{width:195px}.w196{width:196px}
    .w197{width:197px}.w198{width:198px}.w199{width:199px}.w200{width:200px}.w201{width:201px}.w202{width:202px}.w203{width:203px}.w204{width:204px}.w205{width:205px}
    .w206{width:206px}.w207{width:207px}.w208{width:208px}.w209{width:209px}.w210{width:210px}.w211{width:211px}.w212{width:212px}.w213{width:213px}.w214{width:214px}
    .w215{width:215px}.w216{width:216px}.w217{width:217px}.w218{width:218px}.w219{width:219px}.w220{width:220px}.w221{width:221px}.w222{width:222px}.w223{width:223px}
    .w224{width:224px}.w225{width:225px}.w226{width:226px}.w227{width:227px}.w228{width:228px}.w229{width:229px}.w230{width:230px}.w231{width:231px}.w232{width:232px}
    .w233{width:233px}.w234{width:234px}.w235{width:235px}.w236{width:236px}.w237{width:237px}.w238{width:238px}.w239{width:239px}.w240{width:240px}.w241{width:241px}
    .w242{width:242px}.w243{width:243px}.w244{width:244px}.w245{width:245px}.w246{width:246px}.w247{width:247px}.w248{width:248px}.w249{width:249px}.w250{width:250px}
    .w251{width:251px}.w252{width:252px}.w253{width:253px}.w254{width:254px}.w255{width:255px}.w256{width:256px}.w257{width:257px}.w258{width:258px}.w259{width:259px}
    .w260{width:260px}.w261{width:261px}.w262{width:262px}.w263{width:263px}.w264{width:264px}.w265{width:265px}.w266{width:266px}.w267{width:267px}.w268{width:268px}
    .w269{width:269px}.w270{width:270px}.w271{width:271px}.w272{width:272px}.w273{width:273px}.w274{width:274px}.w275{width:275px}.w276{width:276px}.w277{width:277px}
    .w278{width:278px}.w279{width:279px}.w280{width:280px}.w281{width:281px}.w282{width:282px}.w283{width:283px}.w284{width:284px}.w285{width:285px}.w286{width:286px}
    .w287{width:287px}.w288{width:288px}.w289{width:289px}.w290{width:290px}.w291{width:291px}.w292{width:292px}.w293{width:293px}.w294{width:294px}.w295{width:295px}
    .w296{width:296px}.w297{width:297px}.w298{width:298px}.w299{width:299px}.h1{height:1px}.h2{height:2px}.h3{height:3px}.h4{height:4px}.h5{height:5px}.h6{height:6px}
    .h7{height:7px}.h8{height:8px}.h9{height:9px}.h10{height:10px}.h11{height:11px}.h12{height:12px}.h13{height:13px}.h14{height:14px}.h15{height:15px}.h16{height:16px}
    .h17{height:17px}.h18{height:18px}.h19{height:19px}.h20{height:20px}.h21{height:21px}.h22{height:22px}.h23{height:23px}.h24{height:24px}.h25{height:25px}.h26{height:26px}
    .h27{height:27px}.h28{height:28px}.h29{height:29px}.h30{height:30px}.h31{height:31px}.h32{height:32px}.h33{height:33px}.h34{height:34px}.h35{height:35px}.h36{height:36px}
    .h37{height:37px}.h38{height:38px}.h39{height:39px}.h40{height:40px}.h41{height:41px}.h42{height:42px}.h43{height:43px}.h44{height:44px}.h45{height:45px}.h46{height:46px}
    .h47{height:47px}.h48{height:48px}.h49{height:49px}.h50{height:50px}.h51{height:51px}.h52{height:52px}.h53{height:53px}.h54{height:54px}.h55{height:55px}.h56{height:56px}
    .h57{height:57px}.h58{height:58px}.h59{height:59px}.h60{height:60px}.h61{height:61px}.h62{height:62px}.h63{height:63px}.h64{height:64px}.h65{height:65px}.h66{height:66px}
    .h67{height:67px}.h68{height:68px}.h69{height:69px}.h70{height:70px}.h71{height:71px}.h72{height:72px}.h73{height:73px}.h74{height:74px}.h75{height:75px}.h76{height:76px}
    .h77{height:77px}.h78{height:78px}.h79{height:79px}.h80{height:80px}.h81{height:81px}.h82{height:82px}.h83{height:83px}.h84{height:84px}.h85{height:85px}.h86{height:86px}
    .h87{height:87px}.h88{height:88px}.h89{height:89px}.h90{height:90px}.h91{height:91px}.h92{height:92px}.h93{height:93px}.h94{height:94px}.h95{height:95px}.h96{height:96px}
    .h97{height:97px}.h98{height:98px}.h99{height:99px}.h100{height:100px}.h101{height:101px}.h102{height:102px}.h103{height:103px}.h104{height:104px}.h105{height:105px}
    .h106{height:106px}.h107{height:107px}.h108{height:108px}.h109{height:109px}.h110{height:110px}.h111{height:111px}.h112{height:112px}.h113{height:113px}.h114{height:114px}
    .h115{height:115px}.h116{height:116px}.h117{height:117px}.h118{height:118px}.h119{height:119px}.h120{height:120px}.h121{height:121px}.h122{height:122px}.h123{height:123px}
    .h124{height:124px}.h125{height:125px}.h126{height:126px}.h127{height:127px}.h128{height:128px}.h129{height:129px}.h130{height:130px}.h131{height:131px}.h132{height:132px}
    .h133{height:133px}.h134{height:134px}.h135{height:135px}.h136{height:136px}.h137{height:137px}.h138{height:138px}.h139{height:139px}.h140{height:140px}.h141{height:141px}
    .h142{height:142px}.h143{height:143px}.h144{height:144px}.h145{height:145px}.h146{height:146px}.h147{height:147px}.h148{height:148px}.h149{height:149px}.h150{height:150px}
    .pt1{padding-top:1px}.pt2{padding-top:2px}.pt3{padding-top:3px}.pt4{padding-top:4px}.pt5{padding-top:5px}.pt6{padding-top:6px}.pt7{padding-top:7px}.pt8{padding-top:8px}
    .pt9{padding-top:9px}.pt10{padding-top:10px}.pt11{padding-top:11px}.pt12{padding-top:12px}.pt13{padding-top:13px}.pt14{padding-top:14px}.pt15{padding-top:15px}
    .pt16{padding-top:16px}.pt17{padding-top:17px}.pt18{padding-top:18px}.pt19{padding-top:19px}.pt20{padding-top:20px}.pt21{padding-top:21px}.pt22{padding-top:22px}
    .pt23{padding-top:23px}.pt24{padding-top:24px}.pt25{padding-top:25px}.pt26{padding-top:26px}.pt27{padding-top:27px}.pt28{padding-top:28px}.pt29{padding-top:29px}
    .pt30{padding-top:30px}.pt31{padding-top:31px}.pt32{padding-top:32px}.pt33{padding-top:33px}.pt34{padding-top:34px}.pt35{padding-top:35px}.pt36{padding-top:36px}
    .pt37{padding-top:37px}.pt38{padding-top:38px}.pt39{padding-top:39px}.pt40{padding-top:40px}.pt41{padding-top:41px}.pt42{padding-top:42px}.pt43{padding-top:43px}
    .pt44{padding-top:44px}.pt45{padding-top:45px}.pt46{padding-top:46px}.pt47{padding-top:47px}.pt48{padding-top:48px}.pt49{padding-top:49px}.pt50{padding-top:50px}
    .pt51{padding-top:51px}.pt52{padding-top:52px}.pt53{padding-top:53px}.pt54{padding-top:54px}.pt55{padding-top:55px}.pt56{padding-top:56px}.pt57{padding-top:57px}
    .pt58{padding-top:58px}.pt59{padding-top:59px}.pt60{padding-top:60px}.pt61{padding-top:61px}.pt62{padding-top:62px}.pt63{padding-top:63px}.pt64{padding-top:64px}
    .pt65{padding-top:65px}.pt66{padding-top:66px}.pt67{padding-top:67px}.pt68{padding-top:68px}.pt69{padding-top:69px}.pt70{padding-top:70px}.pt71{padding-top:71px}
    .pt72{padding-top:72px}.pt73{padding-top:73px}.pt74{padding-top:74px}.pt75{padding-top:75px}.pt76{padding-top:76px}.pt77{padding-top:77px}.pt78{padding-top:78px}
    .pt79{padding-top:79px}.pt80{padding-top:80px}.pt81{padding-top:81px}.pt82{padding-top:82px}.pt83{padding-top:83px}.pt84{padding-top:84px}.pt85{padding-top:85px}
    .pt86{padding-top:86px}.pt87{padding-top:87px}.pt88{padding-top:88px}.pt89{padding-top:89px}.pt90{padding-top:90px}.pt91{padding-top:91px}.pt92{padding-top:92px}
    .pt93{padding-top:93px}.pt94{padding-top:94px}.pt95{padding-top:95px}.pt96{padding-top:96px}.pt97{padding-top:97px}.pt98{padding-top:98px}.pt99{padding-top:99px}
    .pt100{padding-top:100px}.l1{line-height:1px}.l2{line-height:2px}.l3{line-height:3px}.l4{line-height:4px}.l5{line-height:5px}.l6{line-height:6px}.l7{line-height:7px}
    .l8{line-height:8px}.l9{line-height:9px}.l10{line-height:10px}.l11{line-height:11px}.l12{line-height:12px}.l13{line-height:13px}.l14{line-height:14px}.l15{line-height:15px}
    .l16{line-height:20px}.l17{line-height:17px}.l18{line-height:18px}.l19{line-height:19px}.l20{line-height:20px}.l21{line-height:21px}.l22{line-height:22px}.l23{line-height:23px}.l24{line-height:24px}.l25{line-height:25px}.l26{line-height:26px}.l27{line-height:27px}.l28{line-height:28px}.l29{line-height:29px}.l30{line-height:30px}.l31{line-height:31px}.l32{line-height:32px}.l33{line-height:33px}.l34{line-height:34px}.l35{line-height:35px}.l36{line-height:36px}.l37{line-height:37px}
    .l38{line-height:38px}.l39{line-height:39px}.l40{line-height:40px}.l41{line-height:41px}.l42{line-height:42px}.l43{line-height:43px}.l44{line-height:44px}.l45{line-height:45px}.l46{line-height:46px}.l47{line-height:47px}.l48{line-height:48px}.l49{line-height:49px}.l50{line-height:50px}.l51{line-height:51px}.l52{line-height:52px}.l53{line-height:53px}.l54{line-height:54px}.l55{line-height:55px}.l56{line-height:56px}.l57{line-height:57px}.l58{line-height:58px}.l59{line-height:59px}.l60{line-height:60px}.l61{line-height:61px}.l62{line-height:62px}.l63{line-height:63px}.l64{line-height:64px}.l65{line-height:65px}.l66{line-height:66px}.l67{line-height:67px}.l68{line-height:68px}.l69{line-height:69px}.l70{line-height:70px}.l71{line-height:71px}.l72{line-height:72px}.l73{line-height:73px}.l74{line-height:74px}.l75{line-height:75px}.l76{line-height:76px}.l77{line-height:77px}.l78{line-height:78px}.l79{line-height:79px}.mr1{margin-right:1px}.mr2{margin-right:2px}.mr3{margin-right:3px}.mr4{margin-right:4px}.mr5{margin-right:5px}.mr6{margin-right:6px}.mr7{margin-right:7px}.mr8{margin-right:8px}.mr9{margin-right:9px}.mr10{margin-right:10px}.mr11{margin-right:11px}.mr12{margin-right:12px}.mr13{margin-right:13px}.mr14{margin-right:14px}.mr15{margin-right:15px}.mr16{margin-right:16px}.mr17{margin-right:17px}.mr18{margin-right:18px}.mr19{margin-right:19px}.mr20{margin-right:20px}.mr21{margin-right:21px}.mr22{margin-right:22px}.mr23{margin-right:23px}.mr24{margin-right:24px}.mr25{margin-right:25px}.mr26{margin-right:26px}.mr27{margin-right:27px}.mr28{margin-right:28px}.mr29{margin-right:29px}.mr30{margin-right:30px}.mr31{margin-right:31px}.mr32{margin-right:32px}.mr33{margin-left:33px}.mr34{margin-right:34px}.mr35{margin-right:35px}.mr36{margin-right:36px}.mr37{margin-right:37px}.mr38{margin-right:38px}.mr39{margin-right:39px}.mr40{margin-right:40px}.mr41{margin-right:41px}.mr42{margin-right:42px}.mr43{margin-right:43px}.mr44{margin-right:44px}.mr45{margin-right:45px}.mr46{margin-right:46px}.mr47{margin-right:47px}.mr48{margin-right:48px}.mr49{margin-right:49px}.mr50{margin-right:50px}.mr51{margin-right:51px}.mr52{margin-right:52px}.mr53{margin-right:53px}
    .mr54{margin-right:54px}.mr55{margin-right:55px}.mr56{margin-right:56px}.mr57{margin-right:57px}.mr58{margin-right:58px}.mr59{margin-right:59px}.mr60{margin-right:60px}
    .mr61{margin-right:61px}.mr62{margin-right:62px}.mr63{margin-right:63px}.mr64{margin-right:64px}.mr65{margin-right:65px}.mr66{margin-right:66px}.mr67{margin-right:67px}
    .mr68{margin-right:68px}.mr69{margin-right:69px}.mr70{margin-right:70px}.mr71{margin-right:71px}.mr72{margin-right:72px}.mr73{margin-right:73px}.mr74{margin-right:74px}
    .mr75{margin-right:75px}.mr76{margin-right:76px}.mr77{margin-right:77px}.mr78{margin-right:78px}.mr79{margin-right:79px}.mr80{margin-right:80px}.pl1{padding-left:1px}
    .pl2{padding-left:2px}.pl3{padding-left:3px}.pl4{padding-left:4px}.pl5{padding-left:5px}.pl6{padding-left:6px}.pl7{padding-left:7px}.pl8{padding-right:8px}
    .pl9{padding-left:9px}.pl10{padding-left:10px}.pl11{padding-left:11px}.pl12{padding-left:12px}.pl13{padding-left:13px}.pl14{padding-left:14px}
    .pl15{padding-left:15px}.pl16{padding-left:16px}.pl17{padding-left:17px}.pl18{padding-left:18px}.pl19{padding-left:19px}.pl20{padding-left:20px}
    .pl21{padding-left:21px}.pl22{padding-left:22px}.pl23{padding-left:23px}.pl24{padding-left:24px}.pl25{padding-left:25px}.pl26{padding-left:26px}
    .pl27{padding-left:27px}.pl28{padding-left:28px}.pl29{padding-left:29px}.pl30{padding-left:30px}.pl31{padding-left:31px}.pl32{padding-left:32px}
    .pl33{padding-left:33px}.pl34{padding-left:34px}.pl35{padding-left:35px}.pl36{padding-left:36px}.pl37{padding-left:37px}.pl38{padding-left:38px}
    .pl39{padding-left:39px}.pl40{padding-left:40px}.pl41{padding-left:41px}.pl42{padding-left:42px}.pl43{padding-left:43px}.pl44{padding-left:44px}
    .pl45{padding-left:45px}.pl46{padding-left:46px}.pl47{padding-left:47px}.pl48{padding-left:48px}.pl49{padding-left:49px}.pl50{padding-left:50px}
    .pl51{padding-left:51px}.pl52{padding-left:52px}.pl53{padding-left:53px}.pl54{padding-left:54px}.pl55{padding-left:55px}.pl56{padding-left:56px}
    .pl57{padding-left:57px}.pl58{padding-left:58px}.pl59{padding-left:59px}.pl60{padding-left:60px}.pl61{padding-left:61px}.pl62{padding-left:62px}
    .pl63{padding-left:63px}.pl64{padding-left:64px}.pl65{padding-left:65px}.pl66{padding-left:66px}.pl67{padding-left:67px}.pl68{padding-left:68px}
    .pl69{padding-left:69px}.pr1{padding-right:1px}.pr2{padding-right:2px}.pr3{padding-right:3px}.pr4{padding-right:4px}.pr5{padding-right:5px}
    .pr6{padding-right:6px}.pr7{padding-right:7px}.pr8{padding-right:8px}.pr9{padding-right:9px}.pr10{padding-right:10px}.pr11{padding-right:11px}.pr12{padding-right:12px}
    .pr13{padding-right:13px}.pr14{padding-right:14px}.pr15{padding-right:15px}.pr16{padding-right:16px}.pr17{padding-right:17px}.pr18{padding-right:18px}
    .pr19{padding-right:19px}.pr20{padding-right:20px}.pr21{padding-right:21px}.pr22{padding-right:22px}.pr23{padding-right:23px}.pr24{padding-right:24px}
    .pr25{padding-right:25px}.pr26{padding-right:26px}.pr27{padding-right:27px}.pr28{padding-right:28px}.pr29{padding-right:29px}.pr30{padding-right:30px}
    .pr31{padding-right:31px}.pr32{padding-right:32px}.pr33{padding-right:33px}.pr34{padding-right:34px}.pr35{padding-right:35px}.pr36{padding-right:36px}
    .pr37{padding-right:37px}.pr38{padding-right:38px}.pr39{padding-right:39px}.vm{vertical-align: middle !important;}
*{margin: 0px;padding: 0px;}
html{_filter:expression(document.execCommand("BackgroundImageCache",false,true));}
html,body{margin:0;padding:0}
body {color:#000;font-size:14px;font-family:"Microsoft Yahei";}
a{text-decoration:none;cursor:pointer; behavior:expression(this.onFocus=this.blur());  /* for IE ==*/color:#9b9b9b;}
a:hover{color:#FF0;}
*:focus { outline: none; }
ul li{ list-style-type:none;}
img{border: none;}
ul{margin: 0px;padding: 0px;}
/*==清除浮动==*/
.clearfix:before, .clearfix:after { content:""; display:table;}
.clearfix:after { clear:both;}
.clearfix { *zoom:1;}
.cur{cursor: pointer;}
/*==表单验证样式==*/
/*== 密码强度 ==*/
.top_testresult{font-weight: bold;font-size:12px;font-family: arail,helvetica,san-serif;color:#FFF;padding:0;margin:0 0 0 0;}
.top_testresult span{padding:0 6px;margin:0;height: 26px;line-height: 26px;}
.top_shortPass{background:#FF0000;}
.top_badPass{background:#5A025A;}
.top_goodPass{background:#0029AA;}
.top_strongPass{background:#128511;}
/*== us by validation ==*/
.FormError {position: absolute;top: 300px;left: 300px;display: block;z-index: 500;}
.FormError .FormErrorC {width: 100%;background: #ee0101;position:relative;z-index:501;color: #fff;font-family: tahoma;font-size: 11px;line-height: 14px;border: 2px solid #ddd;box-shadow: 0 0 6px #000;-moz-box-shadow: 0 0 6px #000;-webkit-box-shadow: 0 0 6px #000;padding: 4px 10px 4px 10px;border-radius: 6px;-moz-border-radius: 6px;-webkit-border-radius: 6px;}
.FormError .FormErrorA {width: 15px;margin: -2px 0 0 13px;position:relative;z-index: 506;}
.FormError .FormErrorABottom {box-shadow: none;-moz-box-shadow: none;-webkit-box-shadow: none;margin: 0px 0 0 12px;top:2px;}
.FormError .FormErrorA div {border-left: 2px solid #ddd;border-right: 2px solid #ddd;box-shadow: 0 2px 3px #444;-moz-box-shadow: 0 2px 3px #444;-webkit-box-shadow: 0 2px 3px #444;font-size: 0px;height: 1px;background: #ee0101;margin: 0 auto;line-height: 0;font-size: 0;display: block;}
.FormError .FormErrorABottom div {box-shadow: none;-moz-box-shadow: none;-webkit-box-shadow: none;}
/*==use by jquery ui dialog==*/
.ui-dialog-content{padding: 5px 1px;}
.ui-dialog-title{font-size:14px;}
#Dialog{font-size:12px;}
#Dialog p {line-height:16px;margin:10px 0px 10px 10px;height: auto;}
#SendForm input[type=text], #SendForm input[type=password]{border:1px solid #6A4444;-moz-border-radius: 2px;-webkit-border-radius: 2px;border-radius: 2px;box-shadow: 0 0 2px #6A4444;-moz-box-shadow: 0 0 2px #6A4444;-webkit-box-shadow: 0 0 3px #6A4444;}
#SendForm input[type=text]:hover, #SendForm input[type=password]:hover{border:1px solid #6A4444;}
#SendForm input[type=text]:focus, #SendForm input[type=password]:focus{border:1px solid #6A4444;}
  </style>
</body>
</html>
