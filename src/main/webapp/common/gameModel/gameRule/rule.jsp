<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${_title}</title>
<link href="${base }/common/gameModel/gameRule/css/style.css"
	rel="stylesheet" type="text/css" />
<style type="text/css">
html {
	overflow-y: scroll;
}

body {
	font-size: 12px;
	color: #000000;
	line-height: 18px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	margin: 0;
}

h2 {
	border-top: 3px solid #8e1914;
	margin: 0;
	padding: 0 0 0 23px;
	font-size: 12px;
	color: #fff;
	font-weight: normal;
	background: #ff6600
		url(${base }/common/gameModel/gameRule/images/rule_icon1.gif) left
		no-repeat;
	height: 25px;
	line-height: 25px;
	display: block;
	width: 94%;
}

.rule_from02 {
	padding-left: 40px;
}

.rule_from02 h2 {
	margin-left: -30px;
}

.rule_from02 p, .rule_from02 li {
	width: 94%;
}

#LT table, #D3 table, #P3 table, #T3 table, #SS table, #JX table, #CQ table,
	#TJ table {
	width: 100%;
	border: 1px solid #525252;
	border-collapse: collapse;
	margin-left: 1px;
	*margin-left: 0;
}

#LT table td, #D3 table td, #P3 table td, #T3 table td, #JX table td,
	#CQ table td, #TJ table td {
	padding: 4px;
	border: 1px solid #525252;
}

ul {
	margin: 7px 0;
	padding: 0;
}

p {
	margin: 0;
	padding: 0;
	clear: both;
}

ol {
	list-style: decimal;
}

li {
	display: list-item;
}

.subtitle2 {
	text-align: center;
}

.style1 {
	color: #FF0000
}

.style7 {
	color: #060;
}

.style9 {
	color: #0000FF
}

.style8 {
	color: #006633
}
</style>
</head>
<body>
	<div id="mainbody">
		<div id="page_header">
			<div id="header_title">
				<img
					src="${base }/common/gameModel/gameRule/images/title_lottery.jpg"
					width="1000" height="100" />
			</div>
			<!--header_title end-->
			<!--遊戲項目按鈕STAR-->
			<div id="gamecon">
				<div align="center">
					<!--滚动图片 start-->
					<div class="rollphotos">
						<div class="blk_29">
							<div class="LeftBotton" id="LeftArr"></div>
							<div class="Cont" id="ISL_Cont_1">
								<!-- 图片列表 begin -->
								<div class="box">
									<a href="#" class="FC3D">3D彩</a>
								</div>
								<c:if test="${isLhc }">
								<div class="box">
									<a href="#" class="LHC">六合彩</a>
								</div>
								</c:if>
								<div class="box">
									<a href="#" class="PL3">排列三</a>
								</div>
								<div class="box">
									<a href="#" class="kuaisan">快三</a>
								</div>
								<div class="box">
									<a href="#" class="pcdd">PC蛋蛋</a>
								</div>
								<div class="box">
									<a href="#" class="CQSSC">重庆时时彩</a>
								</div>
								<div class="box">
									<a href="#" class="TJSSC">天津时时彩</a>
								</div>
								<div class="box">
									<a href="#" class="JX11X5">江西11选5</a>
								</div>
								<div class="box">
									<a href="#" class="SH11X5">上海11选5</a>
								</div>
								<div class="box">
									<a href="#" class="BJSC">北京PK10</a>
								</div>
								<!-- 图片列表 end -->
							</div>
							<div class="RightBotton" id="RightArr"></div>
						</div>
						<!--blk_29-->
					</div>
					<!--rollphotos-->
				</div>
				<!--center-->
			</div>
			<!--gamecon-->
			<!--遊戲項目按鈕END-->
		</div>
		<!--page_header end-->
		<div id="page_container">
			<div id="game_ruleArea">
			<div id="FC3D" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_D3.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_D3.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							福彩3D以一个3位自然数为投注号码的彩票，投注者从000-999的数字中选择一个3位数进行投注，包括直选、组选、大小单双等多种玩法。
						</p>

						<h2>玩法介绍</h2>

						<dl>
							<dt>◎定位胆</dt>
							<dd>
								<ul>
									<li>从百位、十位、个任意位置上任意选择1个或1个以上号码。
 									投注方案：百位1
									开奖号码：百位1，<p>即中定位胆百位。
 									从百位、十位、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
									</li>
								</ul>
							</dd>

							<dt>◎前二</dt>
							<dd>
								<ul>
									<li>
从百位、十位中的“大、小、单、双”中至少各选一个组成一注。
投注方案：大单
开奖号码：百位与十位“大单”，即中前二大小单双。<p>
对百位、十位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，<p>所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎后二</dt>
							<dd>
								<ul>
									<li>
 从十位、个位中的“大、小、单、双”中至少各选一个组成一注。
 投注方案：大单
开奖号码：十位与个位“大单”，<p>即中后二大小单双
对十位、个位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，<p>所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎前二直选</dt>
							<dd>
								<ul>
									<li>
从百位、十位各选一个号码组成一注。
投注方案：58
开奖号码：前二位58，即中前二直选<p>
从百位、十位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，<p>且顺序一致，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎后二直选</dt>
							<dd>
								<ul>
									<li>
从十位、个位各选一个号码组成一注。
投注方案：58
开奖号码：后二位58，即中后二直选。
从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎前二组选</dt>
							<dd>
								<ul>
									<li>
 从百、十位号选一个号码组成一注。
 投注方案：58
开奖号码：前二位58或是85（顺序不限，不含对子）即中前二组选。<p>
从0-9中选2个号码组成一注，所选号码与开奖号码的万位、千位相同（不含对子号），顺序不限，即中奖。
									</li>
								</ul>
							</dd>

							<dt>◎后二组选</dt>
							<dd>
								<ul>
									<li>
从十、个位号选一个号码组成一注。
投注方案：58
开奖号码：后二位58或是85（顺序不限，不含对子）即中后二组选。<p>
从0-9中选2个号码组成一注，所选号码与开奖号码的十位、个位相同（不含对子号），顺序不限，即中奖。
									</li>
								</ul>
							</dd>

							<dt>◎一码不定位</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择1个以上号码。
投注方案：1
开奖号码：至少出现1个1，即中一码不定位。<p>
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎二码不定位</dt>
							<dd>
								<ul>
									<li>
 从0-9中任意选择2个以上号码。
投注方案：12
开奖号码：至少出现1和2各一个，即中三星二码不定位。<p>
从0-9中选择2个号码，每注由2号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎组三</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择2个或2个以上号码。
投注方案:588
开奖号码:588（顺序不限），即中三星组选三。<p>
从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎组六</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择3个或3个以上号码。
投注方案:258
开奖号码:852（顺序不限），即中三星组选六。<p>
从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</li>
								</ul>
							</dd>
						</dl>

						<dl>
							<dt>◎直选</dt>
							<dd>
								<ul>
									<li>
从百位、十位、个位各选一个号码组成一注。
投注方案:345
开奖号码:345，即中三星直选。<p>
从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
									</li>
								</ul>
							</dd>
					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
			
				<div id="LHC" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_LT.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_LT.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<h2>一般规则说明</h2>

						<ol>
							<li>客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
							<li>如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
							<li>客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
							<li>为了避免出现争议，请务必在下注之后检查“ 下注状况 ”。</li>
							<li>开奖后接受的投注，将被视为无效。</li>
							<li>任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
							<li>公布赔率时出现的任何打字错误或非故意人为失误，本公司将保留改正错误和按正确赔率结算注单的权力。</li>
							<li>公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
							<li>如本公司察觉客户投注状况异常时，有权即时中止客户投注；在本公司中止下注前，客户之所有投注仍属有效，不得要求取消或延迟交收，以及不得有任何异议。</li>
							<li>如因在本网站投注触犯当地法律，本公司概不负责。</li>
							<li>『六合彩』当次确认下注时间与上次确认下注时间10秒钟内：出现同群组且内容相同2笔以上之注单，本公司只承认一笔有效注单，其余一律注销。<br />
								(举例1：十秒内出现快速下注特别号01,02,03同群组同内容的注单两组以上,本公司只保留一组有效,其余将注销)<br />
								(举例2：十秒内出现快速下注特别号01,02,03 和快速下注特别号01,02,因群组号码不同,则认定两组皆为有效注单)
							</li>
						</ol>

						<h2>六合彩规则说明</h2>

						<div id="rcont">
				<div class="rbar">
					<h1>
						<span style="background: url('${base}/common/template/lottery/lecai/images/gclogo/LHC.png') 20px 9px  no-repeat; padding-left:70px;background-size:32px;"><font id="lotName" color="red">六合彩</font>游戏规则</span>
					</h1>
				</div>
				<div class="ruleContent">
					<strong>快捷导航：</strong>
					<div class="trend-content">
						
							
								<span><a href="#zysm">重要声明</a></span>
								<span><a href="#tbhmpsgzsm">特别号码盘势规则说明</a></span>
								<span><a href="#zhengma">正码</a></span>
								<span><a href="#zhengtema">正特码</a></span>
								<span><a href="#zonghedanshuang">总合单双</a></span>
								<span><a href="#zonghedaxiao">总合大小</a></span>
								<span><a href="#shengxiaosebo">生肖色波</a></span>
								<span><a href="#yixiao">一肖</a></span>
								<span><a href="#hexiao">合肖</a></span>
								<span><a href="#banbo">半波</a></span>
								<span><a href="#zhengxiao">正肖</a></span>
								<span><a href="#qisebo">七色波</a></span>
								<span><a href="#tematoushu">特码头数</a></span>
								<span><a href="#temaweishu">特码尾数</a></span>
								<span><a href="#zhengteweishu">正特尾数</a></span>
								<span><a href="#zongxiao">总肖</a></span>
								<span><a href="#lianxiao">连肖</a></span>
								<span><a href="#lianwei">连尾</a></span>
								<span><a href="#zixuanbuzhong">自选不中</a></span>
							
							
						
					</div>
				</div>
				
<strong id="zysm">重要声明</strong>
<ul>
	<li>客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
	<li>如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
	<li>客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
	<li>为了避免出现争议，请务必在下注之后检查“ 下注状况 ”。</li>
	<li>开奖后接受的投注，将被视为无效。</li>
	<li>任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
	<li>公布赔率时出现的任何打字错误或非故意人为失误，本公司将保留改正错误和按正确赔率结算注单的权力。</li>
	<li>公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
	<li>如本公司察觉客户投注状况异常时，有权即时中止客户投注；在本公司中止下注前，客户之所有投注仍属有效，不得要求取消或延迟交收，以及不得有任何异议。</li>
	<li>如因在本网站投注触犯当地法律，本公司概不负责。</li>
	<li>『六合彩』当次确认下注时间与上次确认下注时间10秒钟内：出现同群组且内容相同2笔以上之注单，本公司只承认一笔有效注单，其余一律注销。<br>
		(举例1：十秒内出现快速下注特别号01,02,03同群组同内容的注单两组以上,本公司只保留一组有效,其余将注销)<br>
		(举例2：十秒内出现快速下注特别号01,02,03 和快速下注特别号01,02,因群组号码不同,则认定两组皆为有效注单)
	</li>
</ul>

<strong>六合彩规则说明</strong><p>以下所有投注皆含本金。</p>
<ul>
	<li><strong id="tbhmpsgzsm">特别号码盘势规则说明</strong><br> 香港六合彩公司当期开出的最后一码为特别号或特码。<br>
		特码 单 码：假设投注号码为开奖号码之特别号，视为中奖，其余情形视为不中奖。<br> 特码 大
		小：开出之特码大于或等于25为特码大， 小于或等于24为特码小，开出49为大。<br> 特码 单
		双：特码为双数叫特双，如8、16；特码为单数叫特单，如21、35，开出49为单。<br>
		特码和数大小：以特码个位和十位数字之和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为大。<br>
		特码和数单双：以特码个位和十位数字之和来判断单双，如01，12，32为和单，02，11，33为和双，开出49号为单。<br>
		特码尾数大小：以特别号尾数若0尾~4尾为小、5尾~9尾为大；如01、32、44为特尾小；如05、18、19为特尾大，开出49号为大<br>
		特码 半
		特：以特别号大小与特别号单双游戏为一个投注组合；当期特别号开出符合投注组合，即视为中奖；若当期特码开出49号，则视为大；其余情形视为不中奖
	</li>
	<li><strong id="lianmian">两面</strong><br>指大、小；单、双。</li>
	<li>每期六合彩开奖球数共七粒(包括特码)。</li>
	<li><strong id="zhengma">正码</strong><br>香港六合彩公司当期开出之前6个号码叫正码。每一个号码为一投注组合，假如投注号码为开奖号码之
		正码，视为中奖，其馀情形视为不中奖。</li>
	<li><strong>正码1-6盘势规则说明</strong><br>
		香港六合彩公司当期开出之前6个号码叫正码。第一时间出来的叫正码1，依次为正码2、正码3…… 正码6(并不以号码大小排序)。<br>
		大 小： 以指定出现正码的位置与号码大于或等于25为大，小于或等于24为小，开出49为大。<br> 单 双：
		以指定出现正码的位置与号码为单数或双数下注，开出49为单。<br> 和数大小：
		以指定出现正码的位置与号码个位和十位数字总和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为大。<br>
		和数单双： 以指定出现正码的位置与号码个位和十位数字总和来判断单双，开出49号为单。<br> 色 波：
		以指定出现正码的位置的球色下注，开奖之球色与下注之颜色相同时，视为中奖，其余情形视为不中奖。<br>
		尾数大小：以指定出现正码的位置与号码尾数大小下注，若0尾~4尾为小、5尾~9尾为大。如01、32、44为正尾小；如05、18、19为正尾大，开出49号为大
	</li>
	<li><strong id="zhengtema">正码特 </strong> <br>正码特是指
		正1特、正2特、正3特、正4特、正5特、正6特：其下注的正码特号与现场摇珠开出之正码其开奖顺序及开奖号码相同，视为中奖，
		如现场摇珠第一个正码开奖为49号，下注第一个正码特为49则视为中奖，其它号码视为不中奖。</li>
	<li><strong id="zonghedanshuang">总和单双 </strong><br>
		所有七个开奖号码的分数总和是单数叫(总分单)，如分数总和是115、183；分数总和是双数叫(总
		分双)，如分数总和是108、162。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖 。</li>
	<li><strong id="zonghedaxiao">总和大小 </strong><br>
		所有七个开奖号码的分数总和大于或等于175为总分大；分数总和小于或等于174为总分小。
		如开奖号码为02、08、17、28、39、46、25，分数总和是165，则总分小。假如投注组合符合 中奖结果，视为中奖，其馀情形视为不中奖。
	</li>
	<li><p>
			<strong id="lianma">连码 ：</strong>
		</p>

		<table>
			<tbody>
				<tr>
					<td bgcolor="#FCF7CB" align="center" width="8%" valign="top">
						1·</td>
					<td bgcolor="#FDFBE3" width="96%"><span class="style2">四全中：
					</span>所投注的每四个号码为一组合，若四个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖。如06、07、08、09四个都是开奖号码之正码，视为中奖，如三个正码加上一个特别号码视为不中奖。
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFE3CA" align="center" width="8%" valign="top">
						2·</td>
					<td bgcolor="#FFF7F0" width="96%"><span class="style2">三中二：
					</span>所投注的每三个号码为一组合，若其中2个是开奖号码中的正码，即为三中二，视为中奖；
						若3个都是开奖号码中的正码，即为三中二之中三，其馀情形视为不中奖，如06、07、08
						为一组合，开奖号码中有06、07两个正码，没有08，即为三中二，按三中二赔付；如开奖
						号码中有06、07、08三个正码，即为三中二之中三，按中三赔付；如出现1个或没有，视 为不中奖 。</td>
				</tr>
				<tr>
					<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
						</td>
					<td bgcolor="#FFF7F0" width="96%"><span class="style2">三中二之中三：
					</span>所投注的每三个号码为一组合，若三个号码都是开奖号码之正码或特码，视为中奖，其馀情形视为 不中奖。如06、07、08三个都是开奖号码之正码或特码，视为中奖，如两个正码加上一个特别号 码视为中奖 。</td>
				</tr>
				<tr>
					<td bgcolor="#FCF7CB" align="center" valign="top">3·</td>
					<td bgcolor="#FDFBE3"><span class="style2">三全中：</span>
						所投注的每三个号码为一组合，若三个号码都是开奖号码之正码，视为中奖，其馀情形视为
						不中奖。如06、07、08三个都是开奖号码之正码，视为中奖，如两个正码加上一个特别号 码视为不中奖 。</td>
				</tr>
				<tr>
					<td bgcolor="#FFE3CA" align="center" valign="top">4·</td>
					<td bgcolor="#FFF7F0"><span class="style2">二全中：</span>
						所投注的每二个号码为一组合，二个号码都是开奖号码之正码，视为中奖，其馀情形视为不 中奖（含一个正码加一个特别号码之情形）。</td>
				</tr>
				<tr>
					<td bgcolor="#FCF7CB" align="center" valign="top">5·</td>
					<td bgcolor="#FDFBE3"><span class="style2">二中特：</span>
						所投注的每二个号码为一组合，二个号码都是开奖号码之正码，叫二中特之中二；若其中一
						个是正码，一个是特别号码，叫二中特之中特；其馀情形视为不中奖 。</td>
				</tr>
				<tr>
					<td bgcolor="#FFE3CA" align="center" valign="top">6·</td>
					<td bgcolor="#FFF7F0"><span class="style2">特串：</span>
						所投注的每二个号码为一组合，其中一个是正码，一个是特别号码，视为中奖，其馀情形视为 不中奖（含二个号码都是正码之情形） 。</td>
				</tr>
				<tr>
					<td bgcolor="#FCF7CB" align="center" valign="top">7.</td>
					<td bgcolor="#FDFBE3">依照二全中·二中特·特串 此3种玩法规则,来进行‘生肖对碰’或‘尾数对碰’<br>
						例一：<br> 选择‘二全中’,之后进行‘生肖对碰’选择龙(2，14，26，38)以及蛇 (1，13，25，37，49)<br>
						则会产生20组号码组合：<br> [2,1] [2,13] [2,25] [2,37] [2,49]<br>
						[14,1] [14,13] [14,25] [14,37] [14,49]<br> [26,1] [26,13]
						[26,25] [26,37] [26,49]<br> [38,1] [38,13] [38,25] [38,37]
						[38,49]<br> 例二：<br>
						选择‘二中特’,之后进行‘尾数对碰’选择0(10，20，30，40)以及5(5，15，25，35，45)<br>
						则会产生20组号码组合：<br> [10,5] [10,15] [10,25] [10,35] [10,45]<br>
						[20,5] [20,15] [20,25] [20,35] [20,45]<br> [30,5] [30,15]
						[30,25] [30,35] [30,45]<br> [40,5] [40,15] [40,25] [40,35]
						[40,45]
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFE3CA" align="center" valign="top">8.</td>
					<td bgcolor="#FFF7F0">肖串尾数:选择一主肖，可扥0-9尾的球． 以三全中的肖串尾数为例：
						选择【兔】当主肖(号码03,15,27,39)并且扥9尾数．因为9尾数中的39已在主肖中出现，将不重复组合．
						故兔主肖扥9尾数共可组合出24种组合(二个尾数号码+一个主肖号码的组合)．</td>
				</tr>
			</tbody>
		</table></li>
	<li><p>
			<strong id="shengxiaosebo">生肖色波 ：</strong>分为生肖与色波两种玩法
		</p>

		<table>
			<tbody>
				<tr>
					<td bgcolor="#FFE3CA" align="center" width="8%" valign="top">
						1·</td>
					<td bgcolor="#FFF7F0" width="96%"><span class="style2">生肖：
					</span><br> 生肖顺序为 鼠 &gt;牛 &gt;虎 &gt;兔 &gt;龙 &gt;蛇 &gt;马 &gt;羊 &gt;猴
						&gt;鸡 &gt;狗 &gt;猪<br>
						如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇 01 , 13 , 25
						, 37 , 49<br> 马 12 , 24 , 36 , 48<br> 羊 11 , 23 , 35 ,
						47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 , 21 , 33 , 45<br>
						狗 08 , 20 , 32 , 44<br> 猪 07 , 19 , 31 , 43<br> 鼠 06 ,
						18 , 30 , 42<br> 牛 05 , 17 , 29 , 41<br> 虎 04 , 16 , 28
						, 40<br> 兔 03 , 15 , 27 , 39<br> 龙 02 , 14 , 26 , 38<br>
						若当期特别号，落在下注生肖范围内，视为中奖 。</td>
				</tr>
				<tr>
					<td bgcolor="#FCF7CB" align="center" valign="top">2·</td>
					<td bgcolor="#FDFBE3"><span class="style2">色波：</span>
						以特别号的球色下注，开奖的球色与下注的颜色相同，视为中奖，球色号码分布如下:<br> <span class="style1">01 </span> <span class="style8">11 21 </span> <span class="style9">31 41</span><br> <span class="style1">02</span>
						<span class="style1">12</span> <span class="style8">22 32</span> <span class="style9">42</span><br> <span class="style9">03</span> <span class="style1">13 23</span> <span class="style8">33 43</span><br>
						<span class="style9">04 14</span> <span class="style1">24
							34</span> <span class="style7">44</span><br> <span class="style8">05</span>
						<span class="style9">15 25</span> <span class="style1">35
							45</span><br> <span class="style8">06 16 </span> <span class="style9">26 36</span> <span class="style1">46</span><br>
						<span class="style1">07</span> <span class="style8">17 27</span> <span class="style9">37 47</span><br> <span class="style1">08
							18</span> <span class="style8">28 38</span> <span class="style9">48</span><br>
						<span class="style9">09</span> <span class="style1">19 29</span> <span class="style8">39 49 </span><br> <span class="style9">10
							20</span> <span class="style1">30 40</span></td>
				</tr>
			</tbody>
		</table></li>
	<li><p>
			<strong id="yixiao">一肖 ：</strong>
		</p>

		<table>
			<tbody>
				<tr>
					<td bgcolor="#FFE3CA" align="center" width="8%" valign="top">
						1·</td>
					<td bgcolor="#FFF7F0" width="96%"><span class="style2">一肖：</span>
						将生肖依序排列，例如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇 01
						, 13 , 25 , 37, 49<br> 马 12 , 24 , 36 , 48<br> 羊 11 , 23
						, 35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 , 21 , 33 ,
						45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 , 31 , 43<br>
						鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 , 41<br> 虎 04 ,
						16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br> 龙 02 , 14 , 26
						, 38<br> 只要当期号码(所有正码与最后开出的特码)，落在下注生肖范围内，则视为中奖。<span style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span>。</td>
				</tr>
			</tbody>
		</table></li>
	<li><strong id="hexiao">合肖</strong><br>
		选1~11生肖(排列如同生肖)为一组合，并选择开奖号码的特码是否在此组合内，若选择是"中"(可选择2~11个生肖)且开奖号码的特码亦在此组合内，即视为中奖；
	</li>
	<li><strong id="banbo">半波</strong><br>
		以特码色波和特单，特双，特大，特小为一个投注组合，当期特码开出符合投注组合，即视为中奖；
		若当期特码开出49号，则视为和大；其馀情形视为不中奖。</li>
	<li><strong id="zhengxiao">正肖 </strong><br>依据开出的所有正码为主。若当期6个正码，只要有1个落在下注生肖范围内，视为中奖。如超过1个正码落在下注生肖范围内
		，派彩将倍增！<br> 如：下注＄100.-正肖蛇赔率1.88<br> 6个正码开出01，派彩为＄88.-<br>
		6个正码开出01，13，派彩为＄88.-<br> 6个正码开出01，13，25，派彩为＄88.-<br>
		6个正码开出01，13，25，37，派彩为＄88.-<br> 6个正码开出01，13，25，37，49，派彩为＄88.-<span style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span></li>
	<li><strong id="qisebo">七色波 </strong><br> 以开出的7个色波，那种颜色最多为中奖。
		开出的6个正码各以1个色波计，特别号以1.5个色波计。而以下3种结果视为和局。<br> 6个正码开出3蓝3绿，而特别码是1.5红<br>
		6个正码开出3蓝3红，而特别码是1.5绿<br> 6个正码开出3绿3红，而特别码是1.5蓝<br>
		如果出现和局，所有投注红，绿，蓝七色波的金额将全数退回，会员也可投注和局。</li>
	<li><strong id="tematoushu">特码头数 </strong><br>指特别号所属头数的号码<br> "0"头
		：01，02，03，04，05，06，07，08，09<br> "1"头
		：10，11，12，13，14，15，16，17，18，19<br> "2"头
		：20，21，22，23，24，25，26，27，28，29<br> "3"头
		：30，31，32，33，34，35，36，37，38，39<br> "4"头
		：40，41，42，43，44，45，46，47，48，49<br> 例如 ：开奖结果特别号码是21则
		"2"头为中奖，其他头数都不中奖</li>
	<li><strong id="temaweishu">特码尾数 </strong><br>指特别号所属尾数的号码<br> "1"尾
		：01，11，21，31，41<br> "2"尾 ：02，12，22，32，42<br> "3"尾
		：03，13，23，33，43<br> "4"尾 ：04，14，24，34，44<br> "5"尾
		：05，15，25，35，45<br> "6"尾 ：06，16，26，36，46<br> "7"尾
		：07，17，27，37，47<br> "8"尾 ：08，18，28，38，48<br> "9"尾
		：09，19，29，39，49<br> "0"尾 ：10，20，30，40<br> 例如 ：开奖结果特别号码是21则
		"1"尾为中奖，其他尾数都不中奖</li>
	<li><strong id="zhengteweishu">正特尾数</strong><br>
		**只要当期号码(所有正码及开出来的特码)，含所属尾数的一个或多个号码，但派彩只派一次，即不论同尾数号码出现一个或多个号码都只派彩一次。<br>
		"1"尾：01.11.21.31.41 "2"尾：02.12.22.32.42 "3"尾：03.13.23.33.43<br>
		"4"尾：04.14.24.34.44 "5"尾：05.15.25.35.45 "6"尾：06.16.26.36.46<br>
		"7"尾：07.17.27.37.47 "8"尾：08.18.28.38.48 "9"尾：09.19.29.39.49<br>
		"0"尾：10.20.30.40<br> 例如 ：开奖结果正码是11、31、42、44、35、32特别号码是 21
		则"1"尾"2"尾"4"尾"5"尾都为中奖，其他尾数都不中奖。
	</li>
	<li><strong id="zongxiao">总肖</strong><br>
		当期号码(所有正码与最后开出的特码)开出的不同生肖总数，与所投注之预计开出之生肖总数和(不用指定特定生肖)，则视为中奖，其余情形视为不中奖。例如：如果'当期号码为19、24、12、34、40、39
		特别号：49，总计六个生肖，若选总肖【6】则为中奖 (请注意：49号亦算输赢，不为和)</li>
	<li><strong id="zongxiaodanshuang">总肖单双</strong><br>
		当期号码(所有正码与最后开出的特码)开出的不同生肖总数若为单数则为单；若为双数则为双。例如当期号码为19、24、12、34、40、39.特码：49，则总计六个生肖，若下双则中奖。
	</li>
	<li><strong id="lianxiao">连肖</strong><br>
		挑选2~6生肖(排列如同生肖)为一个组合，当期号码(所有正码与最后开出的特码)坐落于投注时所勾选之生肖组合所属号码内，所勾选之生肖皆至少有中一个号码，则视为中奖，其余情视为不中奖<span style="color: red">(请注意49亦算输赢，不为和)</span>例如：如果当期号码为19、24、12、34、40、39.特码：49，所勾选三个生肖(称为三肖碰)，若所有生肖的所属号码内至少一个出现于当期号码，则视为中奖。
	</li>
	<li><strong id="lianwei">连尾</strong><br>
		挑选2〜6个尾数为一组合，当期号码（所有正码与最后出的特码）坐落于投注时所勾选之尾数组合所属号码内，则视为中奖，其余情形视为不中奖<span style="color: red">(请注意49亦算输赢，不为和)</span><br>
		举例1：下注2尾碰，勾选0,1，当期七个号码若0尾及1尾皆有开出，视为中奖<br>
		举例2：下注2尾碰，勾选0,1,2三种尾数，会出现三种组合（组合一：0,1）（组合二：0,2）（组合三：1,2），当期七个号码若开出其中一种组合所选的两个尾数，视为中奖。
	</li>
	<li><strong id="zixuanbuzhong">自选不中奖</strong><br>
		挑选5~12个号码为一个组合，当期号码(所有正码与最后开出的特码)皆没有坐落于投注时所挑选之号码组合内，则视为中奖，若是有任何一个当期号码开在所挑选的号码组合情形视为不中奖。<br>
		例如当期号码为19,24,17,34,40,39,特别号49，所挑选5个号码(称为五不中)，若所挑选的号码内皆沒有坐落于当期号码，则为中奖
	</li>
</ul>
			</div>
					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				
				<div id="PL3" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_P3.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_P3.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							P3投注区分为百位、十位和个位，各位置号码范围为0〜9。每期从各位置上开出1个号码作为中奖号码，即开奖号码为3位数。排列三玩法即是竞猜3位开奖号码。
						</p>

							<h2>玩法介绍</h2>

						<dl>
							<dt>◎定位胆</dt>
							<dd>
								<ul>
									<li>从百位、十位、个任意位置上任意选择1个或1个以上号码。
 									投注方案：百位1
									开奖号码：百位1，<p>即中定位胆百位。
 									从百位、十位、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
									</li>
								</ul>
							</dd>

							<dt>◎前二</dt>
							<dd>
								<ul>
									<li>
从百位、十位中的“大、小、单、双”中至少各选一个组成一注。
投注方案：大单
开奖号码：百位与十位“大单”，即中前二大小单双。<p>
对百位、十位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，<p>所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎后二</dt>
							<dd>
								<ul>
									<li>
 从十位、个位中的“大、小、单、双”中至少各选一个组成一注。
 投注方案：大单
开奖号码：十位与个位“大单”，<p>即中后二大小单双
对十位、个位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，<p>所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎前二直选</dt>
							<dd>
								<ul>
									<li>
从百位、十位各选一个号码组成一注。
投注方案：58
开奖号码：前二位58，即中前二直选<p>
从百位、十位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，<p>且顺序一致，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎后二直选</dt>
							<dd>
								<ul>
									<li>
从十位、个位各选一个号码组成一注。
投注方案：58
开奖号码：后二位58，即中后二直选。
从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎前二组选</dt>
							<dd>
								<ul>
									<li>
 从百、十位号选一个号码组成一注。
 投注方案：58
开奖号码：前二位58或是85（顺序不限，不含对子）即中前二组选。<p>
从0-9中选2个号码组成一注，所选号码与开奖号码的万位、千位相同（不含对子号），顺序不限，即中奖。
									</li>
								</ul>
							</dd>

							<dt>◎后二组选</dt>
							<dd>
								<ul>
									<li>
从十、个位号选一个号码组成一注。
投注方案：58
开奖号码：后二位58或是85（顺序不限，不含对子）即中后二组选。<p>
从0-9中选2个号码组成一注，所选号码与开奖号码的十位、个位相同（不含对子号），顺序不限，即中奖。
									</li>
								</ul>
							</dd>

							<dt>◎一码不定位</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择1个以上号码。
投注方案：1
开奖号码：至少出现1个1，即中一码不定位。<p>
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎二码不定位</dt>
							<dd>
								<ul>
									<li>
 从0-9中任意选择2个以上号码。
投注方案：12
开奖号码：至少出现1和2各一个，即中三星二码不定位。<p>
从0-9中选择2个号码，每注由2号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎组三</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择2个或2个以上号码。
投注方案:588
开奖号码:588（顺序不限），即中三星组选三。<p>
从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
									</li>
								</ul>
							</dd>

							<dt>◎组六</dt>
							<dd>
								<ul>
									<li>
从0-9中任意选择3个或3个以上号码。
投注方案:258
开奖号码:852（顺序不限），即中三星组选六。<p>
从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</li>
								</ul>
							</dd>
						</dl>

						<dl>
							<dt>◎直选</dt>
							<dd>
								<ul>
									<li>
从百位、十位、个位各选一个号码组成一注。
投注方案:345
开奖号码:345，即中三星直选。<p>
从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
									</li>
								</ul>
							</dd>
					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="CQSSC" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_CQ.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_CQ.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
						<!-- 时时彩包括重庆时时彩、新疆时时彩等玩法，并自主研发了分分彩，二分彩、五分彩等趣味性更高，频次更快的时时彩游戏。 -->
时时彩属于高频彩，投注区分为万位、千位、百位、十位和个位，各位号码范围为0~9，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
						</p>

					
<h2>玩法简介</h2>
<dl>
<dt>◎五星复式</dt><dd>
从万、千、百、十、个位各选一个号码组成一注。 
从万位、千位、百位、十位、个位选择一个5位数号码组成一注，
<p>
所选号码与开奖号码全部相同，且顺序一致，即为中奖。
投注方案：13456
开奖号码:13456，即中五星直选。
</p>
</dd>
<dl>
<dt>◎前四复式</dt><dd>
从万、千、百、十位号选一个号码组成一注。 
从万位、千位、百位、十位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。
<p>
投注方案：3456
开奖号码:万位、千位、百位、十位分别为3456，即中前四复式。
</p>
</dd>
<dl>
<dt>◎前三复式</dt><dd>
从万位、千位、百位各选一个号码组成一注。 
从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
<p>
投注方案：345
开奖号码:前三位 345，即中前三直选。
</p>
</dd>
<dl>
<dt>◎后三复式</dt><dd>
从百位、十位、个位各选一个号码组成一注。 
从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。
<p>
投注方案： 345
开奖号码:后三位 345，即中后三直选。
</p>
</dd>
<dl>
<dt>◎后四复式</dt><dd>
从千、百、十、个位号选一个号码组成一注。
从千位、百位、十位、个位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。
<p>
 投注方案：3456
开奖号码:千位、百位、十位、个位分别为3456，即中后四复式。
</p>

</dd>
<dl>
<dt>◎中三复式</dt><dd>
从千位、百位、十位各选一个号码组成一注。 
从千位、百位、十位中选择一个3位数号码组成一注，所选号码与开奖号码中3位相同，且顺序一致，即为中奖。<p>
投注方案：345
开奖号码:中三位 345，即中中三直选。
</p>
</dd>
<dl>
<dt>◎前三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
从0-9选择2个数字组成二注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
<p>
投注方案：588
开奖号码:前三位 588（顺序不限），即中前三组三。
</p>
</dd>
<dl>
<dt>◎前三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
 从0-9选择3个数字组成一注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
 <p>
投注方案：258
开奖号码:前三位 852（顺序不限），即中前三组六。
</p>
</dd>
<dl>
<dt>◎中三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
 从0-9选择2个数字组成二注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
 <p>
投注方案： 588
开奖号码:中三位 588（顺序不限），即中中三组三。
</p>
</dd>
<dl>
<dt>◎中三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
 从0-9选择3个数字组成一注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
 <p>
投注方案：258
开奖号码:中三位 852（顺序不限），即中中三组选六。
</p>
</dd>
<dl>
<dt>◎后三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
<p>
投注方案：588
开奖号码:后三位 588（顺序不限），即中后三组三。
</p>
</dd>
<dl>
<dt>◎后三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
<p>
投注方案： 258
开奖号码:后三位 852（顺序不限），即中后三组六。
</p>
</dd>
<dl>
<dt>◎前二复式</dt><dd>
从万位、千位各选一个号码组成一注。
从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
<p>
投注方案：58
开奖号码：前二位58，即中前二复式。
</p>
</dd>
<dl>
<dt>◎后二复式</dt><dd>
从十位、个位各选一个号码组成一注。
从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。
<p>
投注方案：58
开奖号码：后二位58，即中后二复式。
</p>
</dd>
<dl>
<dt>◎前二和值</dt><dd>
从0-18中任意选择1个或1个以上号码。
所选数值等于开奖号码万位、千位二个数字相加之和，即为中奖。
<p>
投注方案：和值1
开奖号码：前二位01或10，即中前二星和值。
</p>
</dd>
<dl>
<dt>◎后二和值</dt><dd>
从0-18中任意选择1个或1个以上号码。
所选数值等于开奖号码十位、个位二个数字相加之和，即为中奖。
<p>
投注方案：和值1
开奖号码：后二位01或10，即中后二和值。
</p>
</dd>
<dl>
<dt>◎前三一码</dt><dd>
从0-9中任意选择1个以上号码。 
 从0-9中选择1个号码，每注由1号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。
 <p>
 投注方案： 1
开奖号码：前三位至少出现1个1，即中前三一码。
</p>
</dd>
<dl>
<dt>◎中三一码</dt><dd>
从0-9中任意选择1个以上号码。 
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。
<p>
投注方案：1
开奖号码：中三位至少出现1个1，即中三一码。
</p>
</dd>
<dl>
<dt>◎后三一码</dt><dd>
从0-9中任意选择1个以上号码。 
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。
<p>
投注方案：1
开奖号码：后三位至少出现1个1，即中后三一码。
</p>
</dd>
<dl>
<dt>◎任二复式</dt><dd>
从万位、千位、百位、十位、个位中至少两位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少两位上个选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
<p>投注方案：万位5，百位8
开奖号码：51812，即中任二复式。
</p>
</dd>
<dl>
<dt>◎任三复式</dt><dd>
从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
<p>投注方案： 万位5，百位8，个位2
开奖号码：51812，即中任三复式。
</p>
</dd>
<dl>
<dt>◎任三组三</dt><dd>
从万、千、百、十、个中至少选择三个位置,号码区至少选择两个号码构成一注。 
从万位、千位、百位、十位、个位中至少选择三个位置，<p>号码区至少选择两个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。
<p>投注方案：位置选择万位、十位、个位，选择号码12
开奖号码：11812，即中任三组三。
</p>
</dd>
<dl>
<dt>◎任三组六</dt><dd>
从万、千、百、十、个中至少选择三个位置,号码区至少选择三个号码构成一注。 
从万位、千位、百位、十位、个位中至少选择三个位置，<p>号码区至少选择三个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，<p>且顺序不限，即为中奖。
投注方案： 位置选择万位、十位、个位，选择号码512
开奖号码：512，即中任三组六。
</p>
</dd>
<dl>
<dt>◎任四复式</dt><dd>
从万、千、百、十、个中至少四位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少四位上各选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
</p><p>投注方案：万位5，千位1、百位8，十位1
开奖号码：51812，即中任四复式。
</p>
</dd>
<dl>
<dt>◎定位胆</dt><dd>
从万、千、百、十、个位号选一个号码组成一注。 
从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，<p>所选号码与相同位置上的开奖号码一致，即为中奖。
</p>
投注方案：万位1
开奖号码：万位1，即中定位胆万位。

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="JX" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_JX.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_JX.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							江西时时彩由中国福利彩票发行管理中心组织，由江西省福利彩票发行中心在所辖区域内承销。江西时时彩开奖时间：官网~每日早上9点~晚上11点10分(每10分钟一期)，共84期。本公司江西时时彩具体游戏规则如下：<br />
							<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong>
						</p>

						<h2>选号玩法</h2>

						<dl>
							<dt>◎一字定位</dt>
							<dd>
								<ul>
									<li>于万仟佰拾个任选一位，自0~9任选1个号进行投注，当开奖结果与所选的定位与号码相同且顺序一致时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎二字定位</dt>
							<dd>
								<ul>
									<li>于万仟佰拾个任选二位，自0~9任选2个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎三字定位</dt>
							<dd>
								<ul>
									<li>于前三、中三、后三任选三位，自0~9任选3个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎一字组合</dt>
							<dd>
								<ul>
									<li>全五：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。如超过1颗球落在所选号码内，派彩将倍增<br />
										※举例：下注一字【5号】＄100，一字賠率2.05<br /> 五颗球开出1，2，3，4，5 派彩为＄105<br />
										五顆球开出1，2，3，5，5 派彩为＄210<br /> 五顆球开出1，2，5，5，5 派彩为＄315
									</li>
									<li>前三：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位]任一数与所选的号码相同时，即为中奖。</li>
									<li>中三：0~9任选1个号进行投注，当开奖结果[仟位、佰位、拾位]任一数与所选的号码相同时，即为中奖。</li>
									<li>后三：0~9任选1个号进行投注，当开奖结果[佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎二字组合</dt>
							<dd>
								<ul>
									<li>于前三、中三、后三0~9任选2个号进行投注，当开奖结果任二数与所选的号码相同时，即为中奖。<br />
										※举例：投注者购买后三二字组合，选择2个相同号码如为11，当期开奖结果如为xx11x、xx1x1、xxx11、皆视为中奖。（x=0~9任一数）<br />
										※举例：投注者购买后三二字组合，选择2个不同号码如为12，当期开奖结果如为xx12x、xx1x2、xx21x、xx2x1、xxx12、xxx21皆视为中奖。（x=0~9任一数）
									</li>
									<li>【附注】：以上二例赔率不同</li>
								</ul>
							</dd>

							<dt>◎三字组合</dt>
							<dd>
								<ul>
									<li>从0-9十个数字中任选三个不同的数字对指定位置进行投注，若所选择的号码与当期开奖号码所指定之位置相同（顺序不限），即为中奖，若是开出豹子如000、111、222、333、444、555、666、777、888、999算不中奖。</li>
								</ul>
							</dd>

							<dt>◎组选三</dt>
							<dd>
								<ul>
									<li>前三：会员可以挑选5~10个号码，当开奖结果[万位、仟位、佰位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[万位、仟位、佰位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br />
										※例如：112、344，若是开出豹子则不算中奖。<br /> ※备注："豹子"为三字同号，例如：111、222
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>中三：会员可以挑选5~10个号码，当开奖结果[仟位、佰位、拾位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[仟位、佰位、拾位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br />
										※例如：112、344，若是开出豹子则不算中奖。<br /> ※备注："豹子"为三字同号，例如：111、222
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>后三：会员可以挑选5~10个号码，当开奖结果[佰位、拾位、个位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[佰位、拾位、个位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>※例如：112、344，若是开出豹子则不算中奖。</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>※备注："豹子"为三字同号，例如：111、222</li>
								</ul>
							</dd>

							<dt>◎组选六</dt>
							<dd>
								<ul>
									<li>前三：会员可以挑选4~8个号码，当开奖结果[万位、仟位、佰位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br />
										※例如：如果是选择(1、2、3、4)，则开奖结果[万位、仟位、佰位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>中三：会员可以挑选4~8个号码，当开奖结果[仟位、佰位、拾位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br />
										※例如：如果是选择(1、2、3、4)，则开奖结果[仟位、佰位、拾位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>后三：会员可以挑选4~8个号码，当开奖结果[佰位、拾位、个位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。
									</li>
								</ul>
							</dd>

							<dt>◎跨度</dt>
							<dd>
								<ul>
									<li>前三：以开奖结果[万位、仟位、佰位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br />
										※举例：开奖结果为3、4、8、7、6。中奖的跨度为5。（最大号码8减最小号码3=5）。
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>中三：以开奖结果[仟位、佰位、拾位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br />
										※举例：开奖结果为3、4、8、7、6。中奖的跨度为4。（最大号码8减最小号码4=4）。
									</li>
								</ul>
							</dd>

							<dd>
								<ul>
									<li>后三：以开奖结果[佰位、拾位、个位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br />
										※举例：开奖结果为3、4、8、7、6。中奖的跨度为2。（最大号码8减最小号码6=2）。
									</li>
								</ul>
							</dd>
						</dl>

						<h2>和数玩法</h2>

						<dl>
							<dt>◎前三和数</dt>
							<dd>
								<ul>
									<li>开奖结果前三三位数的总和值与若投注前三数字的总和值与相同时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎中三和数</dt>
							<dd>
								<ul>
									<li>开奖结果中三三位数的总和值与若投注中三数字的总和值与相同时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎后三和数</dt>
							<dd>
								<ul>
									<li>开奖结果后三三位数的总和值与若投注后三数字的总和值与相同时，即为中奖。</li>
								</ul>
							</dd>

							<dt>◎前三和数尾数</dt>
							<dd>
								<ul>
									<li>以开奖号码的总和尾数，作为中奖依据。会员可以选择0~9的任一号码。<br />
										※举例：开奖结果为3、4、5、0、0。前三总和为12，尾数为2。
									</li>
								</ul>
							</dd>

							<dt>◎中三和数尾数</dt>
							<dd>
								<ul>
									<li>以开奖号码的总和尾数，作为中奖依据。会员可以选择0~9的任一号码。<br />
										※举例：开奖结果为0、3、4、5、0。中三总和为12，尾数为2。
									</li>
								</ul>
							</dd>

							<dt>◎后三和数尾数</dt>
							<dd>
								<ul>
									<li>以开奖号码的总和尾数，作为中奖依据。会员可以选择0~9的任一号码。<br />
										※举例：开奖结果为0、0、3、4、5。后三总和为12，尾数为2。
									</li>
								</ul>
							</dd>
						</dl>

						<h2>两面玩法</h2>

						<dl>
							<dt>◎单双玩法说明</dt>
							<dd>
								<ul>
									<li><p>开奖结果万位、仟位、佰位、拾位或个位数字为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”，当投注位数单双与开奖结果的位数单双相符时，即为中奖。
											※举例：投注者购买佰位单，当期开奖结果如为20130（1为单），则视为中奖。</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">单</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">双</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">1、 3、 5、 7、 9</td>
												<td bgcolor="#FFF7F0" class="point" align="center">0、
													2、 4、 6、 8</td>
											</tr>
										</table></li>
									<li><p>
											开奖结果万仟位、万佰位、万拾位、万个位、仟佰位、仟拾位、仟个位、佰拾位、拾个位或佰个位数字总和的个位数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”，当投注和数单双与开奖结果的和数单双相符时，即为中奖。<br />
											※举例：投注者购买佰拾和数单，当期开奖结果如为20290（佰2+拾9+个0=11为单），则视为中奖。
										</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">单</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">双</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">1、3、5、7、9、11、13、15、17</td>
												<td bgcolor="#FFF7F0" class="point" align="center">
													0、2、4、6、8、10、12、14、16、18</td>
											</tr>
										</table></li>
									<li><p>
											开奖结果前三、中三或后三数字总和的为1、3、5、7、9、11、13、15、17、19、21、21、23、25、27时为“单”，若为0、2、4、6、8、10、12、14、16、18、20、22、24、26时为“双”，当投注和数单双与开奖结果的和数单双相符时，即为中奖。<br />
											※举例：投注者购买"(后三)和数单"，当期开奖结果如为20290（佰2+拾9+个0=11为单），则视为中奖。
										</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">单</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">双</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">1、3、5、7、9、11、13、15、 <br />
													17、19、21、23、25、27
												</td>
												<td bgcolor="#FFF7F0" class="point" align="center">
													0、2、4、6、8、10、12、14、 <br /> 16、18、20、22、24、26
												</td>
											</tr>
										</table></li>
								</ul>
							</dd>

							<dt>◎大小玩法说明</dt>
							<dd>
								<ul>
									<li>开奖结果万位、仟位、佰位、拾位或个位数字为5、6、7、8、9时为“大”，若为0、1、2、3、4时为“小”，当投注位数大小与开奖结果的位数大小相符时，即为中奖。<br />
										※举例：投注者购买佰位小，当期开奖结果如为20352（3为小），则视为中奖。
									</li>
									<li><p>开奖结果万仟位、万佰位、万拾位、万个位、仟佰位、仟拾位、仟个位、佰拾位、拾个位或佰个位数字总和的尾数为5、6、7、8、9时为“大”，若为0、1、2、3、4时为“小”，当投注和数大小与开奖结果的和数大小相符时，即为中奖。
											※举例：投注者购买和数佰拾位小，当期开奖结果如为20491（佰4+拾9=13，尾数3为小），则视为中奖。</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">大</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">小</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">5、6、7、8、9</td>
												<td bgcolor="#FFF7F0" class="point" align="center">
													0、1、2、3、4</td>
											</tr>
										</table></li>
									<li><p>
											开奖结果前三、中三或后三数字总和的为14、15、16、17、18、19、20、21、22、23、24、25、26、27时为“大”，
											若为0、1、2、3、4、5、6、7、8、9、10、11、12、13时为“小”，当投注和数大小与开奖结果的和数大小相符时，即为中奖。<br />
											※举例：投注者购买"(后三)和数大"，当期开奖结果如为20976（佰9+拾7+个6=22为大），则视为中奖。
										</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">大</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">小</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">14、15、16、17、18、19、20、21、 <br />
													22、23、24、25、26、27
												</td>
												<td bgcolor="#FFF7F0" class="point" align="center">
													0、1、2、3、4、5、6、 <br /> 7、8、9、10、11、12、13
												</td>
											</tr>
										</table></li>
									<li><p>
											开奖结果所有号码总和的为23、24、25、26、27、28、29、30、31、32、33、34、35、36、37、38、39、40、41、42、43、44、45时为“大”，
											若为0、1、2、3、4、5、6、7、8、9、10、11、12、13、14、15、16、17、18、19、20、21、22时为“小”，当投注和数大小与开奖结果的和数大小相符时，即为中奖。<br />
											※举例：投注者购买"总大"，当期开奖结果如为20976（万2+仟0+佰9+拾7+个6=24为大），则视为中奖。
										</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">大</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">小</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">23、24、25、26、27、28、29、30、31、32、33、 <br />
													34、35、36、37、38、39、40、41、42、43、44、45
												</td>
												<td bgcolor="#FFF7F0" class="point" align="center">
													0、1、2、3、4、5、6、7、8、9、10、11、12、 <br />
													13、14、15、16、17、18、19、20、21、22
												</td>
											</tr>
										</table></li>
								</ul>
							</dd>

							<dt>◎质合玩法说明</dt>
							<dd>
								<ul>
									<li>开奖结果万位、仟位、佰位、拾位或个位数字为1、2、3、5、7时为“质数”，若为0、4、6、8、9时为“合数”，当投注位数质合与开奖结果的位数质合相符时，即为中奖。<br />
										※举例：投注者购买个位质，当期开奖结果如为20957（7为质），则视为中奖。
									</li>
									<li>开奖结果万仟位、万佰位、万拾位、万个位、仟佰位、仟拾位、仟个位、佰拾位、拾个位或佰个位数字总和的尾数为1、2、3、5、7时为“质数”，若为0、4、6、8、9时为“合数”，当投注号码与开奖结果的质合相符时，即为中奖。
										※举例：投注者购买佰拾位合，当期开奖结果如为20957（佰9+拾5=14，尾数4为合数），则视为中奖。</li>
									<li><p>开奖结果前三、中三或后三数字总和的尾数为1、2、3、5、7时为“质数”，若为0、4、6、8、9时为“合数”，当投注号码与开奖结果的质合相符时，即为中奖。
											※举例：投注者购买后三質，当期开奖结果如为20957（佰9+拾5+个7=21，尾数1为質数），则视为中奖。</p>

										<table>
											<tr>
												<th width="50%" valign="top" bgcolor="#AFAFE4"
													align="center" class="subtitle2">质</th>
												<th width="50%" bgcolor="#AFAFE4" align="center"
													class="subtitle2">合</th>
											</tr>
											<tr>
												<td valign="top" bgcolor="#FFF7F0" class="point"
													align="center">1、 2、 3、 5、 7</td>
												<td bgcolor="#FFF7F0" class="point" align="center">0、
													4、 6、 8、 9</td>
											</tr>
										</table></li>
								</ul>
							</dd>
						</dl>

						<h2>龙虎玩法</h2>

						<dl>
							<dt>◎龙虎玩法说明</dt>
							<dd>
								<ul>
									<li>龙虎游戏规则：龙虎是以开奖结果的五个数字作为基准，取任意位置（万、千、百、拾、个）的数字进行组合大小比对的一种玩法；<BR />
										当投注龙/虎时，开奖结果为和局，那么押注龙/虎视为不中奖；<BR />
										当投注"和"时，开奖结果为龙/虎，投注“和”视为不中奖；<BR /> 举例：开奖结果为：2,1,3,5,2
										万为龙、千为龙虎时：结果 龙(2）大于虎（1），即为开龙；如万为龙，个为虎时，结果一样大，即为开和局！<BR />
										说明：龙1 VS 虎2 即为万为龙，千为虎；龙2 VS 虎4 即为千为龙，拾为虎；<BR /> 注：总龙虎即龙1 VS 虎5
									</li>
								</ul>
							</dd>

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="TJSSC" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_TJ.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_TJ.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
						<!-- 时时彩包括重庆时时彩、新疆时时彩等玩法，并自主研发了分分彩，二分彩、五分彩等趣味性更高，频次更快的时时彩游戏。 -->
时时彩属于高频彩，投注区分为万位、千位、百位、十位和个位，各位号码范围为0~9，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
						</p>

					
<h2>玩法简介</h2>
<dl>
<dt>◎五星复式</dt><dd>
从万、千、百、十、个位各选一个号码组成一注。 
从万位、千位、百位、十位、个位选择一个5位数号码组成一注，
<p>
所选号码与开奖号码全部相同，且顺序一致，即为中奖。
投注方案：13456
开奖号码:13456，即中五星直选。
</p>
</dd>
<dl>
<dt>◎前四复式</dt><dd>
从万、千、百、十位号选一个号码组成一注。 
从万位、千位、百位、十位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。
<p>
投注方案：3456
开奖号码:万位、千位、百位、十位分别为3456，即中前四复式。
</p>
</dd>
<dl>
<dt>◎前三复式</dt><dd>
从万位、千位、百位各选一个号码组成一注。 
从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
<p>
投注方案：345
开奖号码:前三位 345，即中前三直选。
</p>
</dd>
<dl>
<dt>◎后三复式</dt><dd>
从百位、十位、个位各选一个号码组成一注。 
从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。
<p>
投注方案： 345
开奖号码:后三位 345，即中后三直选。
</p>
</dd>
<dl>
<dt>◎后四复式</dt><dd>
从千、百、十、个位号选一个号码组成一注。
从千位、百位、十位、个位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。
<p>
 投注方案：3456
开奖号码:千位、百位、十位、个位分别为3456，即中后四复式。
</p>

</dd>
<dl>
<dt>◎中三复式</dt><dd>
从千位、百位、十位各选一个号码组成一注。 
从千位、百位、十位中选择一个3位数号码组成一注，所选号码与开奖号码中3位相同，且顺序一致，即为中奖。<p>
投注方案：345
开奖号码:中三位 345，即中中三直选。
</p>
</dd>
<dl>
<dt>◎前三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
从0-9选择2个数字组成二注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
<p>
投注方案：588
开奖号码:前三位 588（顺序不限），即中前三组三。
</p>
</dd>
<dl>
<dt>◎前三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
 从0-9选择3个数字组成一注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
 <p>
投注方案：258
开奖号码:前三位 852（顺序不限），即中前三组六。
</p>
</dd>
<dl>
<dt>◎中三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
 从0-9选择2个数字组成二注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
 <p>
投注方案： 588
开奖号码:中三位 588（顺序不限），即中中三组三。
</p>
</dd>
<dl>
<dt>◎中三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
 从0-9选择3个数字组成一注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
 <p>
投注方案：258
开奖号码:中三位 852（顺序不限），即中中三组选六。
</p>
</dd>
<dl>
<dt>◎后三组三</dt><dd>
从0-9中任意选择2个或2个以上号码。
从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
<p>
投注方案：588
开奖号码:后三位 588（顺序不限），即中后三组三。
</p>
</dd>
<dl>
<dt>◎后三组六</dt><dd>
从0-9中任意选择3个或3个以上号码。
从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。
<p>
投注方案： 258
开奖号码:后三位 852（顺序不限），即中后三组六。
</p>
</dd>
<dl>
<dt>◎前二复式</dt><dd>
从万位、千位各选一个号码组成一注。
从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
<p>
投注方案：58
开奖号码：前二位58，即中前二复式。
</p>
</dd>
<dl>
<dt>◎后二复式</dt><dd>
从十位、个位各选一个号码组成一注。
从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。
<p>
投注方案：58
开奖号码：后二位58，即中后二复式。
</p>
</dd>
<dl>
<dt>◎前二和值</dt><dd>
从0-18中任意选择1个或1个以上号码。
所选数值等于开奖号码万位、千位二个数字相加之和，即为中奖。
<p>
投注方案：和值1
开奖号码：前二位01或10，即中前二星和值。
</p>
</dd>
<dl>
<dt>◎后二和值</dt><dd>
从0-18中任意选择1个或1个以上号码。
所选数值等于开奖号码十位、个位二个数字相加之和，即为中奖。
<p>
投注方案：和值1
开奖号码：后二位01或10，即中后二和值。
</p>
</dd>
<dl>
<dt>◎前三一码</dt><dd>
从0-9中任意选择1个以上号码。 
 从0-9中选择1个号码，每注由1号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。
 <p>
 投注方案： 1
开奖号码：前三位至少出现1个1，即中前三一码。
</p>
</dd>
<dl>
<dt>◎中三一码</dt><dd>
从0-9中任意选择1个以上号码。 
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。
<p>
投注方案：1
开奖号码：中三位至少出现1个1，即中三一码。
</p>
</dd>
<dl>
<dt>◎后三一码</dt><dd>
从0-9中任意选择1个以上号码。 
从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。
<p>
投注方案：1
开奖号码：后三位至少出现1个1，即中后三一码。
</p>
</dd>
<dl>
<dt>◎任二复式</dt><dd>
从万位、千位、百位、十位、个位中至少两位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少两位上个选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
<p>投注方案：万位5，百位8
开奖号码：51812，即中任二复式。
</p>
</dd>
<dl>
<dt>◎任三复式</dt><dd>
从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
<p>投注方案： 万位5，百位8，个位2
开奖号码：51812，即中任三复式。
</p>
</dd>
<dl>
<dt>◎任三组三</dt><dd>
从万、千、百、十、个中至少选择三个位置,号码区至少选择两个号码构成一注。 
从万位、千位、百位、十位、个位中至少选择三个位置，<p>号码区至少选择两个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。
<p>投注方案：位置选择万位、十位、个位，选择号码12
开奖号码：11812，即中任三组三。
</p>
</dd>
<dl>
<dt>◎任三组六</dt><dd>
从万、千、百、十、个中至少选择三个位置,号码区至少选择三个号码构成一注。 
从万位、千位、百位、十位、个位中至少选择三个位置，<p>号码区至少选择三个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，<p>且顺序不限，即为中奖。
投注方案： 位置选择万位、十位、个位，选择号码512
开奖号码：512，即中任三组六。
</p>
</dd>
<dl>
<dt>◎任四复式</dt><dd>
从万、千、百、十、个中至少四位上各选1个号码组成一注。 
从万位、千位、百位、十位、个位中至少四位上各选1个号码组成一注，<p>所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。
</p><p>投注方案：万位5，千位1、百位8，十位1
开奖号码：51812，即中任四复式。
</p>
</dd>
<dl>
<dt>◎定位胆</dt><dd>
从万、千、百、十、个位号选一个号码组成一注。 
从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，<p>所选号码与相同位置上的开奖号码一致，即为中奖。
</p>
投注方案：万位1
开奖号码：万位1，即中定位胆万位。

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="JX11X5" style="display: none;">
					<div id="game_title">
						<div id="title_left">
						</div>
						<!--title_left end-->
						<div id="title">
						</div>
						<!--title end-->
						<div id="title_right">
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<div class="rule_from02">
						<p>
							彩票在线平台提供广东、江西、上海、山东等多城市的彩票玩法，玩法多，乐趣多。11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
						</p>

						<h2>玩法说明</h2>

						<dl>
							<dt>◎前三复式 </dt>
							<dd>
								<ul>
									<li>   
 从万位、千位、百位中至少各选择1个号码组成一注。  
从万位、千位、百位至少个选1个号码组成一注，<p>所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
投注方案：03 04 05
开奖号码:前三位03 04 05，即中前三复式。</li>
								</ul>
							</dd>

							<dt>◎前三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的万位、千位、百位相同，顺序不限，即为中奖。
 投注方案：02 05 08
开奖号码:前三位08 05 02（顺序不限），即中前三组选。</li>
								</ul>
							</dd>

							<dt>◎中三复式</dt>
							<dd>
								<ul>
									<li>
 从千位、百位、十位中至少各选择1个号码组成一注。 
从千位、百位、十位至少个选1个号码组成一注，所选号码与开奖号码中3位相同，<p>且顺序一致，即为中奖。
 投注方案：03 04 05
开奖号码:中三位03 04 05，即中中三复式。</li>
								</ul>
							</dd>

							<dt>◎中三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的千位、<p>百位、十位相同，顺序不限，即为中奖。
 投注方案：02 05 08
开奖号码:中三位08 05 02（顺序不限），即中中三组选。</li>
								</ul>
							</dd>

							<dt>◎后三直选</dt>
							<dd>
								<ul>
									<li>
 从百位、十位、个位中至少各选择1个号码组成一注。 
从百位、十位、个位至少个选1个号码组成一注，所选号码与开奖号码后3位相同，<p>且顺序一致，即为中奖。
投注方案： 03 04 05
开奖号码:后三位03 04 05，即中后三直选。</li>
								</ul>
							</dd>

						</dl>

						<dl>
							<dt>◎后三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的百位、<p>十位、个位相同，顺序不限，即为中奖。
投注方案：02 05 08
开奖号码:后三位08 05 02（顺序不限），即中后三组选。</li>
								</ul>
							</dd>

							<dt>◎前二复式</dt>
							<dd>
								<ul>
									<li>
 从万位、千位中至少各选择1个号码组成一注。 
从万位、千位至少个选1个号码组成一注，所选号码与开奖号码前2位相同，<p>且顺序一致，即为中奖。
投注方案：05 08
开奖号码:前二位05 08，即中前二复式。</li>
								</ul>
							</dd>

							<dt>◎前二组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码组成一注。 
从0-11中任意选择2个或2个以上号码组成一注，<p>所选号码与开奖号码的万位、千位相同，顺序不限，即为中奖。
 投注方案：05 08<p>
开奖号码:前二位08 05 （顺序不限），即中前二组选。</li>
								</ul>
							</dd>

							<dt>◎后二直选</dt>
							<dd>
								<ul>
									<li>
 从十位、个位中至少各选择1个号码组成一注。 
从十位、个位至少个选1个号码组成一注，<p>所选号码与开奖号码前2位相同，且顺序一致，即为中奖。
投注方案：05 08<p>
开奖号码:后二位05 08，即中后二直选。</li>
								</ul>
							</dd>
							
							
							
							<dt>◎后二组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码组成一注。 
从0-11中任意选择2个或2个以上号码组成一注，<p>所选号码与开奖号码的十位、个位相同，顺序不限，即为中奖。
投注方案：05 08<p>
开奖号码:后二位08 05 （顺序不限），即中后二组选。</li>
								</ul>
								
								
								
							
							
							<dt>◎前三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。
投注方案：1<p>
开奖号码:前三位至少出现1个1，即中前三一码不定位。</li>
								</ul>
								
								
							
							
							<dt>◎中三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。
投注方案:1<p>
开奖号码:中三位至少出现1个1，即中中三一码不定位。</li>
								</ul>
								
								
							
							
							<dt>◎后三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。
投注方案：1<p>
开奖号码:后三位至少出现1个1，即中后三一码不定位。</li>
								</ul>
								
								
							
							<dt>◎定位胆</dt>
							<dd>
								<ul>
									<li>
 从万位、千位、百位、十位、个任意位置上任意选择1个或1个以上号码。 <p>
从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，<p>所选号码与相同位置上的开奖号码一致，即为中奖。
投注方案：万位1<p>
开奖号码:万位1，即中定位胆万位。。</li>
								</ul>
								
								
							
							<dt>◎任一中一</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个或1个以上号码。 
从01-11共11号码中选择1个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05<p>
开奖号码:08 04 11 05 03，即中任一中一。</li>
								</ul>
								
								
								
							
							<dt>◎任二中二</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码。 
从01-11共11号码中选择2个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04<p>
开奖号码:08 04 11 05 03，即中任二中二。</li>
								</ul>
								
								
								
							
							<dt>◎任三中三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码。 
从01-11共11号码中选择3个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04 11<p>
开奖号码:08 04 11 05 03，即中任三中三。</li>
								</ul>
								
								
								
								
							
							<dt>◎任四中四</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择4个或4个以上号码。 
从01-11共11号码中选择4个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案： 05 04 08 03<p>
开奖号码:08 04 11 05 03，即中任四中四。</li>
								</ul>
								
								
								
								
							
							<dt>◎任五中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择5个或5个以上号码。 
从01-11共11号码中选择5个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04 11 03 08<p>
开奖号码:08 04 11 05 03，即中任五中五。。</li>
								</ul>
								
								
								
							
							<dt>◎任六中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择6个或6个以上号码。 
从01-11共11号码中选择6个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 10 04 11 03 08<p>
开奖号码:08 04 11 05 03，即中任六中五。</li>
								</ul>
								
								
							
							<dt>◎任七中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择7个或7个以上号码。 
从01-11共11号码中选择7个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案： 05 10 04 11 03 08 09<p>
开奖号码:08 04 11 05 03，即中任七中五。</li>
								</ul>
								
								
							
							<dt>◎任八中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择8个或8个以上号码。 
从01-11共11号码中选择8个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
 投注方案：05 10 04 11 03 08 09 01<p>
开奖号码:08 04 11 05 03，即中任八中五。</li>
								</ul>
								
								
								
							</dd>

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="SH11X5" style="display: none;">
					<div id="game_title">
						<div id="title_left">
						</div>
						<!--title_left end-->
						<div id="title">
						</div>
						<!--title end-->
						<div id="title_right">
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<div class="rule_from02">
						<p>
							彩票在线平台提供广东、江西、上海、山东等多城市的彩票玩法，玩法多，乐趣多。11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
						</p>

						<h2>玩法说明</h2>

						<dl>
							<dt>◎前三复式 </dt>
							<dd>
								<ul>
									<li>   
 从万位、千位、百位中至少各选择1个号码组成一注。  
从万位、千位、百位至少个选1个号码组成一注，<p>所选号码与开奖号码前3位相同，且顺序一致，即为中奖。
投注方案：03 04 05
开奖号码:前三位03 04 05，即中前三复式。</li>
								</ul>
							</dd>

							<dt>◎前三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的万位、千位、百位相同，顺序不限，即为中奖。
 投注方案：02 05 08
开奖号码:前三位08 05 02（顺序不限），即中前三组选。</li>
								</ul>
							</dd>

							<dt>◎中三复式</dt>
							<dd>
								<ul>
									<li>
 从千位、百位、十位中至少各选择1个号码组成一注。 
从千位、百位、十位至少个选1个号码组成一注，所选号码与开奖号码中3位相同，<p>且顺序一致，即为中奖。
 投注方案：03 04 05
开奖号码:中三位03 04 05，即中中三复式。</li>
								</ul>
							</dd>

							<dt>◎中三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的千位、<p>百位、十位相同，顺序不限，即为中奖。
 投注方案：02 05 08
开奖号码:中三位08 05 02（顺序不限），即中中三组选。</li>
								</ul>
							</dd>

							<dt>◎后三直选</dt>
							<dd>
								<ul>
									<li>
 从百位、十位、个位中至少各选择1个号码组成一注。 
从百位、十位、个位至少个选1个号码组成一注，所选号码与开奖号码后3位相同，<p>且顺序一致，即为中奖。
投注方案： 03 04 05
开奖号码:后三位03 04 05，即中后三直选。</li>
								</ul>
							</dd>

						</dl>

						<dl>
							<dt>◎后三组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码组成一注。 
从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的百位、<p>十位、个位相同，顺序不限，即为中奖。
投注方案：02 05 08
开奖号码:后三位08 05 02（顺序不限），即中后三组选。</li>
								</ul>
							</dd>

							<dt>◎前二复式</dt>
							<dd>
								<ul>
									<li>
 从万位、千位中至少各选择1个号码组成一注。 
从万位、千位至少个选1个号码组成一注，所选号码与开奖号码前2位相同，<p>且顺序一致，即为中奖。
投注方案：05 08
开奖号码:前二位05 08，即中前二复式。</li>
								</ul>
							</dd>

							<dt>◎前二组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码组成一注。 
从0-11中任意选择2个或2个以上号码组成一注，<p>所选号码与开奖号码的万位、千位相同，顺序不限，即为中奖。
 投注方案：05 08<p>
开奖号码:前二位08 05 （顺序不限），即中前二组选。</li>
								</ul>
							</dd>

							<dt>◎后二直选</dt>
							<dd>
								<ul>
									<li>
 从十位、个位中至少各选择1个号码组成一注。 
从十位、个位至少个选1个号码组成一注，<p>所选号码与开奖号码前2位相同，且顺序一致，即为中奖。
投注方案：05 08<p>
开奖号码:后二位05 08，即中后二直选。</li>
								</ul>
							</dd>
							
							
							
							<dt>◎后二组选</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码组成一注。 
从0-11中任意选择2个或2个以上号码组成一注，<p>所选号码与开奖号码的十位、个位相同，顺序不限，即为中奖。
投注方案：05 08<p>
开奖号码:后二位08 05 （顺序不限），即中后二组选。</li>
								</ul>
								
								
								
							
							
							<dt>◎前三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。
投注方案：1<p>
开奖号码:前三位至少出现1个1，即中前三一码不定位。</li>
								</ul>
								
								
							
							
							<dt>◎中三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。
投注方案:1<p>
开奖号码:中三位至少出现1个1，即中中三一码不定位。</li>
								</ul>
								
								
							
							
							<dt>◎后三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个以上号码。 
从0-11中选择1个号码，每注由1个号码组成，<p>只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。
投注方案：1<p>
开奖号码:后三位至少出现1个1，即中后三一码不定位。</li>
								</ul>
								
								
							
							<dt>◎定位胆</dt>
							<dd>
								<ul>
									<li>
 从万位、千位、百位、十位、个任意位置上任意选择1个或1个以上号码。 <p>
从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，<p>所选号码与相同位置上的开奖号码一致，即为中奖。
投注方案：万位1<p>
开奖号码:万位1，即中定位胆万位。。</li>
								</ul>
								
								
							
							<dt>◎任一中一</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择1个或1个以上号码。 
从01-11共11号码中选择1个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05<p>
开奖号码:08 04 11 05 03，即中任一中一。</li>
								</ul>
								
								
								
							
							<dt>◎任二中二</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择2个或2个以上号码。 
从01-11共11号码中选择2个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04<p>
开奖号码:08 04 11 05 03，即中任二中二。</li>
								</ul>
								
								
								
							
							<dt>◎任三中三</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择3个或3个以上号码。 
从01-11共11号码中选择3个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04 11<p>
开奖号码:08 04 11 05 03，即中任三中三。</li>
								</ul>
								
								
								
								
							
							<dt>◎任四中四</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择4个或4个以上号码。 
从01-11共11号码中选择4个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案： 05 04 08 03<p>
开奖号码:08 04 11 05 03，即中任四中四。</li>
								</ul>
								
								
								
								
							
							<dt>◎任五中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择5个或5个以上号码。 
从01-11共11号码中选择5个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 04 11 03 08<p>
开奖号码:08 04 11 05 03，即中任五中五。。</li>
								</ul>
								
								
								
							
							<dt>◎任六中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择6个或6个以上号码。 
从01-11共11号码中选择6个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案：05 10 04 11 03 08<p>
开奖号码:08 04 11 05 03，即中任六中五。</li>
								</ul>
								
								
							
							<dt>◎任七中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择7个或7个以上号码。 
从01-11共11号码中选择7个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
投注方案： 05 10 04 11 03 08 09<p>
开奖号码:08 04 11 05 03，即中任七中五。</li>
								</ul>
								
								
							
							<dt>◎任八中五</dt>
							<dd>
								<ul>
									<li>
 从01-11中任意选择8个或8个以上号码。 
从01-11共11号码中选择8个号码进行购买，<p>只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
 投注方案：05 10 04 11 03 08 09 01<p>
开奖号码:08 04 11 05 03，即中任八中五。</li>
								</ul>
								
								
								
							</dd>

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				
				
				<div id="CX" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_CX.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_CX.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							重庆幸运农场（以下简称幸运农场）经国家财政部批准，由中国福利彩票发行管理中心推出的一种全新快速开奖游戏。<br />重庆幸运农场每天开97期，每期间隔10分钟。北京时间（GMT+8）每天白天从上午10：00到次日2:00。
							本公司重庆幸运农场具体游戏规则如下：<br />
						</p>

						<h2>单码/双面规则说明</h2>

						<dl>
							<dt>◎单码</dt>
							<dd>
								<ul>
									<li>第一球、第二球、第三球、第四球、第五球、第六球、第七球、第八球：指下注的球号与现场滚球开出之号码其开奖顺序及开奖号码相同，视为中奖，如现场滚球第一球开奖为20号，下注第一球为20则视为中奖，其它号码视为不中奖。</li>
								</ul>
							</dd>

							<dt>◎大小</dt>
							<dd>
								<ul>
									<li>开出的号码大于或等于11为大，小于或等于10为小 。</li>
								</ul>
							</dd>

							<dt>◎单双</dt>
							<dd>
								<ul>
									<li>号码为双数叫双，如08、16；号码为单数叫单，如19、05 。</li>
								</ul>
							</dd>

							<dt>◎和数单双</dt>
							<dd>
								<ul>
									<li>正码个位和十位数字之和来判断胜负，如01、12、16为和单；02、11、20为和双 。</li>
								</ul>
							</dd>

							<dt>◎尾数大小</dt>
							<dd>
								<ul>
									<li>开出之正码尾数大于或等于5为尾大，小于或等于4为尾小。</li>
								</ul>
							</dd>

						</dl>

						<h2>总和规则说明</h2>

						<dl>
							<dt>◎总和大小</dt>
							<dd>
								<ul>
									<li>所有8个开奖号码总和值小于84为总小；总和值大于84为总大，若总和值刚好等于84算和局(不计算输赢)。</li>
								</ul>
							</dd>

							<dt>◎总和单双</dt>
							<dd>
								<ul>
									<li>所有开奖号码数字加总值是单数为和单，如11、21；加总值是双数为和双，如22、40。</li>
								</ul>
							</dd>

							<dt>◎总和尾数大小</dt>
							<dd>
								<ul>
									<li>所有开奖号码数字加总值大于或等于5为总尾大，小于或等于4为总尾小。</li>
								</ul>
							</dd>

							<dt>◎龙虎</dt>
							<dd>
								<ul>
									<li>龙：第一球中奖号码大于第八球的中奖号码。如第一球开出14第八球开出09。虎：第一球中奖号码小于第八球的中奖号码。如第一球开出09第八球开出14。</li>
								</ul>
							</dd>

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>
				<div id="BJSC" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_PK.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_PK.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							“PK拾” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10 个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2 号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。
						</p>

						<h2>玩法说明</h2>

						<dl>
							<dt>◎定位胆</dt>
							<dd>
								<ul>
									<li>
从第一名到第十名任意位置上选择1个或1个以上号码。 
从第一名到第十名任意位置上至少选择1个以上号码，<p>所选号码与相同位置上的开奖号码一致，即为中奖。
投注方案： 第一名01
开奖号码:第一名01，即中定位胆第一名。</li>
								</ul>
							</dd>
							
							<dt>◎前三复式</dt>
							<dd>
								<ul>
									<li>
从第一名、第二名、第三名中各选择1个号码组成一注。 
选号与开奖号码按位猜对3位即中奖。
 <p>投注方案： 01 02 03
开奖号码:01 02 03 04 05 06 07 08 09 10
即中前三复式</li>
								</ul>
							</dd>
							
							<dt>◎前二复式</dt>
							<dd>
								<ul>
									<li>
从第一名、第二名中至少选择1个号码组成一注。 
选号与开奖号码按位猜对2位即中奖。
  <p>投注方案：01 02
开奖号码:01 02 03 04 05 06 07 08 09 10
即中前二复式。</li>
								</ul>
							</dd>
							
							<dt>◎前一复式</dt>
							<dd>
								<ul>
									<li>

从第一名中至少选择1个号码组成一注。
选号与开奖号码按位猜对1位即中奖。
投注方案：01<p>
开奖号码:01 02 03 04 05 06 07 08 09 10
即中前一直选。</li>
								</ul>
							</dd>

							

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>

				<div id="KL" style="display: none;">
					<div id="game_title">
						<div id="title_left">
							<img src="${base }/common/gameModel/gameRule/images/rule.jpg"
								width="385" height="80" />
						</div>
						<!--title_left end-->
						<div id="title">
							<img
								src="${base }/common/gameModel/gameRule/images/GameName_KL.jpg"
								width="469" height="80" />
						</div>
						<!--title end-->
						<div id="title_right">
							<img
								src="${base }/common/gameModel/gameRule/images/Gamelogo_KL.jpg"
								width="146" height="80" />
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<!--rule_from01 end-->
					<div class="rule_from02">
						<p>
							北京快乐8
							是依照北京福彩网发行的北京快乐8的官方开奖结果所规划的游戏。由1至80的号码中随机摇出20个数字作为开奖号码，依这20个号码变化成各式不同的玩法，在根据猜中的号码个数或玩法可以获得不同等级的奖金。<br />
							此游戏的开奖时间和开奖号码完全与北京福彩网发行的北京快乐8同步，每日从早上9点至晚上12点，每五分钟开奖一次，每日开奖179期。如开奖时间异动以中国福利彩票管理中心公告为准。<br />
							本公司北京快乐8具体游戏规则如下：<br />
						</p>

						<h2>规则说明</h2>

						<dl>
							<dt>◎选号</dt>
							<dd>
								<ul>
									<li>选一：投注的1个号码与当期摇出的20个号码中的任1个号码相同，则中奖。<br />
										选二：投注的2个号码与当期摇出的20个号码中的任2个号码相同，则中奖。<br />
										选三：投注的3个号码为一组合，若其中2个是开奖中的号码，即为三中二，视为中奖；若3个都是开奖中的号码，即为三中三，其余情形视为不中奖。<br />
										选四：投注的4个号码为一组合，若其中2个是开奖中的号码，即为四中二，视为中奖；若其中3个是开奖中的号码，即为四中三；若4个都是开奖中的号码，即为四中四，其余情形视为不中奖。<br />
										选五：投注的5个号码为一组合，若其中3个是开奖中的号码，即为五中三，视为中奖；若其中4个号码是开奖中的号码，即为五中四；若5个都是开奖中的号码，即为五中五，其余情形视为不中奖。
									</li>
								</ul>
							</dd>

							<dt>◎和值</dt>
							<dd>
								<ul>
									<li>以所有开出的全部20个号码加起来的和值来判定。<br />
										总单/双：20个号码加总的和值为单，叫做和单；20个号码加总的和值为双，叫做和双。<br />
										总大/小：20个号码加总的和值大于810，为和大；20个号码加总的和值小于810，则为和小。<br />
										和值810：20个号码加总的和值等于810，叫和值810。<br />
										※举例：开奖号码为1，2，3，4，5，6，7，8，9，10，11，12，13，14，15，16，17，18，19，20；那么此20个开奖号码的和值总和为210，则为小，为双。则投注小和双者中奖。投注大、单、和值810者不中奖。
									</li>
								</ul>
							</dd>

							<dt>◎上中下盘</dt>
							<dd>
								<ul>
									<li>上下盘：开奖号码1至40为上盘号码，41至80为下盘号码。<br />
										开出的20个号码中：如上盘号码（1-40）在此局开出号码数目占多数时，此局为上盘；<br />
										下盘号码（41-80）在此局开出号码数目占多数时，此局为下盘；<br />
										上盘号码（1－40）和下盘号码（41-80）在此局开出的数目相同时（各10个数字），此局为中盘。<br />
										※举例：此局开出1，2，3，4，5，6，7，8，9，10，11，12，13，14，15，16，17，18，19，20.
										此局为上盘。<br />
										※举例：此局开出41，42，43，44，45，46，47，48，49，50，51，52，53，54，55，56，57，58，59，60
										此局为下盘。<br /> ※举例：此局开出
										1，2，3，4，5，6，7，8，9，10，41，42，43，44，45，46，47，48，49，50 此局为中盘。
									</li>
								</ul>
							</dd>

							<dt>◎奇偶和盘</dt>
							<dd>
								<ul>
									<li>开奖号码中1，3，5，7，…，75，77，79为奇数号码，2，4，6，8，……，76，78，80为偶数号码。
										当期开出的20个中奖号码中，如奇数号码数目占多数时（超过10个），则为奇盘，投注奇者中奖；
										偶数号码占多数时（超过10个），则为偶盘，投注偶者中奖； 如果奇数和偶数号码数目相同时（均为
										10个），则为和，投注和者中奖。<br />
										※举例：此期开出1，3，5，7，9，11，13，15，17，19，21，22，24，26，28，30，32，34，46，68，
										其中奇数11个偶数9个，此期为奇盘。<br />
										※举例：此期开出2，4，6，8，10，12，14，16，44，48，66，68，25，27，31，35，37，39，41，55，
										其中偶数12个奇数8个，此期为偶盘。<br />
										※举例：此期开出2，4，6，8，10，12，14，16，18，20，41，43，45，47，49，51，53，55，57，59，
										其中奇数10个偶数10个，此期为和。
									</li>
								</ul>
							</dd>

							<dt>◎五行</dt>
							<dd>
								<ul>
									<li>开出的20个号码的总和分在5个段，以金、木、水、火、土命名：金（210～695）、木（696～763）、水（764～855）、火（856～923）和土（924～1410）。<br />
										※举例：开奖号码为01、04、05、10、11、13、20、27、30、32、33、36、40、47、54、59、61、64、67、79，总和是693，则总分数在210－695段内，则开出的是「金」。下注「金」为赢，反之则输。
									</li>
								</ul>
							</dd>

						</dl>

					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>

				


				<div id="pcdd" style="display: none;">
					<div id="game_title">
						<div id="title_left">
						</div>
						<!--title_left end-->
						<div id="title">
						</div>
						<!--title end-->
						<div id="title_right">
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<div class="rule_from02">
						<h2>玩法说明</h2>

						<dl>
							<div id="rcont">
				<div class="ruleContent">
					<strong>快捷导航：</strong>
					<div class="trend-content">
						
							
							
								<span><a href="#jj">简介</a></span>
								
									
										
											<span><a href="#q2zx_fs">前二复式</a></span>
										
									
								
									
										
											<span><a href="#q2zx">前二组选</a></span>
										
									
								
									
										
											<span><a href="#h2zx_fs">后二复式</a></span>
										
									
								
									
										
											<span><a href="#dwd">定位胆</a></span>
										
									
								
									
										
											<span><a href="#dxds">大小单双</a></span>
										
									
								
									
										
											<span><a href="#hz">和值</a></span>
										
									
								
									
										
											<span><a href="#h2zx">后二组选</a></span>
										
									
								
									
										
											<span><a href="#bdw">不定位胆</a></span>
										
									
								
							
						
					</div>
				</div>
				
<strong class="p1" id="jj">简介：</strong>
<p>
	<span>PC蛋蛋（幸运28）开奖结果来源于国家福利彩票北京快乐8(官网)开奖号码，从早上9:05至23:55，每5分钟一期不停开奖。&nbsp;</span>
</p>
<p>
	<span>北京快乐8每期开奖共开出20个数字，PC蛋蛋将这20个开奖号码按照由小到大的顺序依次排列；取其1-6位开奖号码相加，和值的末位数作为PC蛋蛋开奖第一个数值；
		取其7-12位开奖号码相加，和值的末位数作为PC蛋蛋开奖第二个数值，取其13-18位开奖号码相加，和值的末位数作为PC蛋蛋开奖第三个数值；三个数值相加即为PC蛋蛋最终的开奖结果。</span>
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0" bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">PC蛋蛋(幸运28)</td>
			<td bgcolor="#FFFFFF">9:00—23:55(北京时间)</td>
			<td bgcolor="#FFFFFF">001-179</td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
<p>
	<span><br></span>
</p>
<strong id="dwd">
	<span>定位胆</span>
</strong>
<p>
	<span>从第一、第二、第三位号选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：第一位1开奖号码：第一位1，即中定位胆第一位。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="bdw">
	<span>不定位胆</span>
</strong>
<p>
	<span>选一个号码组成一注。</span>
</p>
<p>
	<span>从0-9中选择1个号码，每注由1号码组成，只要开奖号码的第一位、第二位、第三位中包含所选号码，即为中奖。</span>
</p>
<p>
	<span>投注方案：12开奖号码：至少出现1和2各一个，即中不定位胆。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="sxzx">
	<span>三星组选</span>
</strong>
<p>
	<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。</span>
</p>
<p>
	<span>投注方案：2 6 9 开奖号码:2 6 9（顺序不限），即中三星组选。</span>
</p>
<p>
	<span><br></span>
</p>
<strong>
	<span id="sxfs">三星复式</span>
</strong>
<p>
	<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案： 345 &nbsp;开奖号码: 345，即中三星直选。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="q2zx">
	<span>前二组选</span>
</strong>
<p>
	<span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的第一位、第二位相同，顺序不限，即为中奖。</span>
</p>
<p>
	<span>投注方案：4 7 开奖号码:前二位4 7 （顺序不限），即中前二组选。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="q2zx_fs">
	<span>前二复式</span>
</strong>
<p>
	<span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：58开奖号码：前二位58，即中前二复式</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="h2zx">
	<span>后二组选</span>
</strong>
<p>
	<span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的第二位、第三位相同，顺序不限，即为中奖。</span>
</p>
<p>
	<span>投注方案：4 7 开奖号码:后二位4 7 （顺序不限），即中后二组选。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="h2zx_fs">
	<span>后二复式</span>
</strong>
<p>
	<span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：58开奖号码：后二位58，即中后二复式。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="hz">
	<span>和值</span>
</strong>
<p>
	<span>从0-27中任意选择1个或1个以上号码。</span>
</p>
<p>
	<span>所选数值等于开奖号码相加之和，即为中奖。</span>
</p>
<p>
	<span>投注方案：和值1开奖号码：001或010或100，即中和值。</span>
</p>
<p>
	<span><br></span>
</p>
<strong id="dxds">
	<span>大小单双</span>
</strong>
<p>
	<span>和值的数字为大小单双</span>
</p>
<p>
	<span>和值数字0-13为小，14-27为大，奇数为单，偶数为双</span>
</p>
<p>
	<span>所选大小单双与开奖和值相符，即为中奖</span>
</p>
<p>
	<br>
</p>
			</div>
						</dl>
					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>




				<div id="kuaisan" style="display: none;">
					<div id="game_title">
						<div id="title_left">
						</div>
						<!--title_left end-->
						<div id="title">
						</div>
						<!--title end-->
						<div id="title_right">
						</div>
						<!--title_right end-->
					</div>
					<!--game_title end-->
					<div class="rule_title">
						<img
							src="${base }/common/gameModel/gameRule/images/game_title01.gif"
							width="232" height="32" />
					</div>
					<!--rule_title end-->
					<div class="rule_from01"></div>
					<div class="rule_from02">
					<h2>玩法介绍</h2>
				<div class="ruleContent">
					<strong>快捷导航：</strong>
					<div class="trend-content">
						
							
							
								<span><a href="#jj">简介</a></span>
								
									
										
											<span><a href="#hz">和值</a></span>
										
									
								
									
										
											<span><a href="#dxds">大小单双</a></span>
										
									
								
									
										
											<span><a href="#sthtx">三同号通选</a></span>
										
									
								
									
										
											<span><a href="#sthdx">三同号单选</a></span>
										
									
								
									
										
											<span><a href="#sbtx">三不同号</a></span>
										
									
								
									
										
											<span><a href="#slhtx">三连号通选</a></span>
										
									
								
									
										
											<span><a href="#ethfx">二同号复选</a></span>
										
									
								
									
										
											<span><a href="#ethdx">二同号单选</a></span>
										
									
								
									
										
											<span><a href="#ebth">二不同号</a></span>
										
									
								
							
						
					</div>
				</div>
				
<strong class="p1" id="jj">简介：</strong>




<p class="p1">
	「安徽快三」由中国福利彩票发行管理中心组织销售、安徽省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0" bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">安徽快三</td>
			<td bgcolor="#FFFFFF">08:50-22:00(北京时间) </td>
			<td bgcolor="#FFFFFF">001-080 </td>
			<td bgcolor="#FFFFFF">每10分钟</td>
		</tr>
	</tbody>
</table>




<p class="p2">
	<br>
</p>
<p class="p2">
	<br>
</p>
<strong class="p1" id="hz">和值投注</strong>
<p class="p1">是指对三个号码的和值进行投注，包括“和值3”至“和值18”投注。&nbsp;</p>
<p class="p1">投注方案：15</p>
<p class="p1">开奖号码:555</p>
<p class="p2">
	<br>
</p>
<strong class="p1" id="dxds">大小单双</strong>
<p class="p1">是指对三个号码的总和进行投注，总和大于10为大，小于等于10为小&nbsp;</p>
<p class="p1">投注方案：单</p>
<p class="p1">开奖号码:111</p>
<p class="p2">
	<br>
</p>
<strong class="p1" id="sthtx">三同号通选（即全包）</strong>
<p class="p1">是指对所有相同的三个号码（111、222、…、666）进行投注；&nbsp;</p>
<p class="p2">
	<br>
</p>
<strong class="p1" id="sthdx">三同号单选</strong>
<p class="p1">是指从所有相同的三个号码（111、222、…、666）中任意选择一组号码进行投注。</p>
<p>
	<br>
</p>
<strong class="p1" id="ethfx">二同号复选</strong>
<p class="p1">是指对三个号码中两个指定的相同号码和一个任意号码进行投注；</p>
<p>
	<br>
</p>
<strong class="p1" id="ethdx">二同号单选</strong>
<p class="p1">是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注</p>
<p>
	<br>
</p>
<strong class="p1" id="sbth">三不同号</strong>
<p class="p1">是指对三个各不相同的号码进行投注。</p>
<p>
	<br>
</p>
<strong class="p1" id="ebth">二不同号</strong>
<p class="p1">是指对三个号码中两个指定的不同号码和一个任意号码进行投注</p>
<p>
	<br>
</p>
<strong class="p1" id="slhtx">三连号通选（即全包）</strong>
<p class="p1">是指对所有三个相连的号码（仅限：123、234、345、456）进行投注。</p>
<p>
	<br>
</p>
			</div>
						
					</div>
					<!--rule_from02 end-->
					<div class="rule_from03"></div>
					<!--rule_from03 end-->
					<a href="#" class="btn_top"></a>
				</div>


			</div>
			<!--game_ruleArea end-->
		</div>
		<!--page_container end-->
	</div>
	<!--mainbody end-->
	<div id="page-footer">
		<div id="footer">&ensp;&ensp;&ensp;</div>
	</div>
	<!--page-footer end-->
	<script src="${base }/common/gameModel/gameRule/js/jquery.js"
		type="text/javascript"></script>
	<script src="${base }/common/gameModel/gameRule/js/ScrollPic.js"
		type="text/javascript"></script>
	<script type="text/javascript">
	<!--//--><![CDATA[//><!--
		var scrollPic_02 = new ScrollPic();
		scrollPic_02.scrollContId = "ISL_Cont_1"; //内容容器ID
		scrollPic_02.arrLeftId = "LeftArr";//左箭头ID
		scrollPic_02.arrRightId = "RightArr"; //右箭头ID

		scrollPic_02.frameWidth = 966;//显示框宽度
		scrollPic_02.pageWidth = 138; //翻页宽度

		scrollPic_02.speed = 10; //移动速度(单位毫秒，越小越快)
		scrollPic_02.space = 10; //每次移动像素(单位px，越大越快)
		scrollPic_02.autoPlay = false; //自动播放
		scrollPic_02.autoPlayTime = 3; //自动播放间隔时间(秒)

		scrollPic_02.itemcount = $(".box").length;

		scrollPic_02.initialize(); //初始化

		if ($(".box").length <= 7) {
			$("#LeftArr, #RightArr").hide();
		}
		$('#${code}').show();
		$(".box a").click(function() {
			$("#game_ruleArea > div").hide();
			$("#" + $(this).attr("class")).show();
		});
	</script>
</body>
</html>