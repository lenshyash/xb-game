<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${_title }</title>
</head>
<c:if test="${isSysOnOff eq 'on' }">
	<script type="text/javascript">window.location.href="${base}/index.do";</script>
</c:if>
<body style="width: 100%;margin: 4% auto;">
	<div style="text-align: -webkit-center;">
		<img alt=".." src="${base}/common/erro/images/systemmaintenance.jpg"><br/>
		<span>
			${system_maintenance }
		</span>
		<br/><br/><br/><br/>
		<span><a href="${kfUrl }" target="_blank">在线客服</a></span>
	</div>
</body>
</html>