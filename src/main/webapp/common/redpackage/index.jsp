<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>抢红包-【${_title}】</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico">
<link href="${base}/common/redpackage/css/bao.css" rel="stylesheet" type="text/css">
<script src="${base}/common/redpackage/js/jquery.js"></script>
<script src="${base}/common/redpackage/js/loadhb.js"></script>
</head>
<body>
	<input type="hidden" id="base" value="${base}"/>
	<input type="hidden" id="redPacketId" />
	<div id="msg">
		<dl>
			<dd>×</dd>
			<dt>
				<img src="${base}/common/redpackage/images/bighongbao.png" width="150"
					height="215">
			</dt>
		</dl>
		<span></span>
	</div>
	<div class="pop-login" style="display:none;z-index: 9999;">
		<div class="mask"></div>
		<div class="wrapper" style="height:374px;">
		<div class="title">会员登录<span class="close">X</span></div>
	    <div class="con">
	        <form name="popup_login_form" id="popup_login_form" method="post">
	        <div class="br txt">
				<label>账号：</label><span style="text-indent: -2000px;">帐号</span>
				<input name="Username" id="account" type="text">
				<p id="popup_username_msg"></p>
			</div>
			<div class="br pas">
				<label>密码：</label><span>请输入您的密码</span>
				<input name="Password" id="password" type="password" maxlength="16">
	            <p id="popup_password_msg"></p>
			</div>
			<div class="br pas" id="verifyHtml1" style=";padding-left: 60px; border-left-width: 0px; margin-left: -10px;">
				<label>验证码：</label>
			 	<input name="verify" id="verifyCode" type="text" maxlength="5" style="margin-top:0px;width:100px;">
			 	
			 	<img style="cursor:pointer;width:150px;height:40px;margin-bottom: -13px;" src="${base }/verifycode.do" onclick="refreshVerifyCode(this)" id="vPic">
	       		<p id="geetest_verify_msg"></p>
	       </div>
			<div class="br sub">
				<input type="button" value="登录" id="popupLoginButton" onclick="doLogin();">
	        </div>
	        </form>
	    </div>
	  </div>
	</div>
	<div class="hongbao">
<%-- 		<img src="${base}/images/hongbao.png" style="width: 90px; top: -217px; left: 463px" onclick="qing()"> --%>
	</div>
	<div class="banner">
		<ul id="scrollobj" onmouseover="_stop()" onmouseout="_start()">
			<li><strong>暂无记录</strong></li>
		</ul>
	</div>
	<div class="main1">
		<div class="cenbox">
			
		</div>
	</div>
	<div class="footbox">
		<div class="cenbox" id="redgz">
			
		</div>
	</div>
	<div class="hidebox"></div>
</body>
</html>