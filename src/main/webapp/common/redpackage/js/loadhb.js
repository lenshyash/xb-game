var imgwidth = 100// 图片宽度
var imgheight = 143// 图片高度
var hongbaoCount = 10;// 每次红包个数
var loadindex = 0;
var loadtime = 3000;// 多少毫秒掉1次红包
var slideSpeed = new Array(8000, 12000)// 红包掉落速度（毫秒），取此之间的随机数
var base;
var redjl;
var _timer;
$(function() {
	base = $("#base").val();
	redgz();

	redPackage();// 读取红包活动

	$("#msg").click(function() {
		closeMsg()
	})

	$(".close").click(function() {
		$(".pop-login").css("display", "none");
	});
})

// 点击抢红包
function qiang() {
	var id = $("#redPacketId").val();
	$.ajax({
		url : base + '/center/redpacket/grab.do',
		type : 'POST',
		data : {
			redPacketId : id
		},
		success : function(json, status, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				showMsg("恭喜您抢到" + json + "元红包");
			} else {// 后台异常
				if (ceipstate == 3) {
					showMsg(json.msg);
				} else if (ceipstate == 4) {
					$(".pop-login").css("display", "block");
				} else {
					showMsg("抢红包失败");
				}
			}
		}
	});
}

// 加载抢红包记录
function redMsgList(id) {

	$.ajax({
		url : base + "/center/redpacket/marquee.do",
		data : {
			redPacketId : id
		},
		success : function(result) {
			if (result.length > 0) {
				var txt = "";
				result.forEach(function(e) {
					txt += "<li><small>" + estObj.dispTime(e.createDatetime)
							+ "</small><span>" + e.account.substr(0, 4)
							+ "***</span>抢到<strong>" + e.money
							+ "元红包</strong></li>";
				});
				$("#scrollobj").html(txt);
				_timer = setInterval("scroll(document.getElementById('scrollobj'))", 40);
			} else {
				$("#scrollobj").html("<p><span>暂无记录</span></p>");
			}
		}
	});
}

// 红包活动
function redPackage() {
	$.ajax({
		url : base + "/center/redpacket/progress.do",
		success : function(result) {
			redList = result;
			if (redList == null) {
				redjl = "-1";
				showMsg("暂无抢红包活动");
			} else {
				redMsgList(redList.id);
				var times = new Date().getTime();
				if (redList && times < redList.endDatetime) {
					var redBegTimes = redList.beginDatetime;
					var redEndTimes = redList.endDatetime;
					if (times > redBegTimes) {
						if (times > redEndTimes || redList.totalNumber == 0) {
							showMsg("抢红包活动已经结束");
							return;
						}
						LoadHongBao();// 首次打开网页 先执行1次下红包
						setInterval(LoadHongBao, loadtime)// 定时执行下红包
						$("#redPacketId").val(redList.id);
					} else {
						showMsg("抢红包活动暂未开始");
					}

				}
			}
		}
	});
}

// 获取验证码
function refreshVerifyCode() {
	var time = (new Date()).getTime();
	$("#vPic").attr("src", base + "/verifycode.do?time=" + time);
}

// 红包规则
function redgz() {
	$.ajax({
		url : base + "/getConfig/getArticle.do",
		data : {
			code : 22
		},
		type : "post",
		dataType : 'json',
		success : function(r) {
			var col = '';
			for (var i = 0; i < r.length; i++) {
				col += "<h1>" + r[i].title + "</h1>";
				col += r[i].content;
			}
			$("#redgz").html(col);
		}
	});
}

// 会员登录
function doLogin() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	var verifyCode = $("#verifyCode").val();
	if (!verifyCode) {
		alert("请输入验证码！");
		return;
	}
	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				parent.location.reload();
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reVerifyCode();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reVerifyCode();
			}
		}
	});
}

// 生成指定范围随机数
function FunMath(startSize, endSize) {
	return Math.floor(Math.random() * (endSize - startSize + 1) + startSize);
}

// 掉红包
function LoadHongBao() {
	loadindex++;
	var width = $(window).width();
	var height = $(window).height();
	var hongbaostr = "";
	for (i = 0; i < hongbaoCount; i++) {
		hongbaostr += "<img src=\""
				+ base
				+ "/common/redpackage/images/hongbao.png\" class=\"red_sp\" style=\"width:"
				+ FunMath(50, imgwidth) + "px; top:-" + FunMath(imgheight, 500)
				+ "px; left:" + FunMath(0, width - imgwidth)
				+ "px\" onclick=\"qiang()\" />"
	}
	$(".hongbao").append(hongbaostr)
	for (i = 0; i < hongbaoCount; i++) {
		$(".hongbao img").eq((loadindex - 1) * i).animate({
			top : height
		}, FunMath(slideSpeed[0], slideSpeed[1]), function() {
			$(this).attr("src", "").hide()
		})
	}
}

var estObj = {
	pre0 : function(num) {
		if (num < 10) {
			num = '0' + num;
		}
		return num;
	},
	/* 即時時間顯示 */
	dispTime : function(time) {
		var dateObj = new Date(time), p0 = estObj.pre0, Y = dateObj
				.getFullYear(), Mh = dateObj.getMonth() + 1, D = p0(dateObj
				.getDate()), H = p0(dateObj.getHours()), M = p0(dateObj
				.getMinutes()), S = p0(dateObj.getSeconds());

		if (Mh > 12) {
			Mh = 01;
		} else if (Mh < 10) {
			Mh = '0' + Mh;
		}

		return Y + "年" + Mh + "月" + D + "日  " + H + ":" + M + ":" + S;
	}
};

/* 往上 */
function scroll(obj) {
	var tmp = (obj.scrollTop)++;
	if (obj.scrollTop == tmp) {
		obj.innerHTML += obj.innerHTML;
	}
	if (obj.scrollTop >= obj.firstChild.offsetWidth) {
		obj.scrollTop = 0;
	}
}


function _stop() {
	if (_timer != null) {
		clearInterval(_timer);
	}
}
function _start() {
	if(redjl != "-1"){
		_timer = setInterval("scroll(document.getElementById('scrollobj'))", 40);
	}
}

function showMsg(str) {
	$("#msg span").html(str)
	$("#msg").fadeIn(200);
}

function closeMsg() {
	$("#msg").fadeOut(200);
}