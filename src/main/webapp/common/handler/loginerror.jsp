<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	var error = '${errorCode}';
	var path = '${homepage}';
	var isLocaApp = false;
	if (error == 1) {
		alert("账号在其他地方登录!");
	} else if (error == 2) {
// 		alert("您被管理员强制下线!");
		alert("登录超时!");
	} else if (error == 3) {
		alert("您的IP发生变动!");
	}
	window.location.href = '${base}' + path;
</script>