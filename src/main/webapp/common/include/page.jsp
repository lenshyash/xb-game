<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div style="text-align: right; margin-right: 23px;" id="page"></div>
<script id="page_tpl" type="text/html">
	{{if hasPre }}
		<a href="javascript:void(0)" onclick="prePage({{prePage }})">上一页</a>
	{{/if }}
	{{if !hasPre}}
		<span>上一页</span>
	{{/if}}
	{{$pageNumber totalPageCount currentPageNo}}
	{{if currentPageNo < totalPageCount }}
		<a href="javascript:void(0)" class="special" onclick="nextPage({{$nextPage currentPageNo}})">下一页</a>
	{{/if}}
	{{if currentPageNo >= totalPageCount }}
		<span>下一页</span>
	{{/if}}
	共{{totalPageCount }}页
	每页条数 <select id="jump_page_size">{{$pageSize pageSize totalPageCount}}</select>
	  到第 <input type="text" id="jump_page" name="page" value="{{currentPageNo}}" style="width:40px;"> 页 <input type="button"
		onclick="javascript:checkandsubmit({{totalPageCount }});" value="确定" class="btn_pagedh">
</script>
<script type="text/javascript">
	function prePage(pageNo) {
		$("#jump_page").val(pageNo);
		pagesubmit();
	}

	function nextPage(pageNo) {
		$("#jump_page").val(pageNo);
		pagesubmit();
	}

	function gopage(pageNo) {
		$("#jump_page").val(pageNo);
		pagesubmit();
	}

	function pagesubmit() {
		callback();
	}
	function checkandsubmit(total) {
		if ($("#jump_page").val() > total) {
			alert("超过最大页数:" + total + "!");
			return;
		}
		pagesubmit();
	}

	var callback = function() {
	};

	function initPage(page, fn) {
		var html = template('page_tpl', page);
		$("#page").html(html);
		callback = fn;
	}

	template
			.helper(
					'$pageNumber',
					function(totalPage, currentPageNo) {
						var html = "";
						var showCount = 10;//总共展示页
						var showFlag = totalPage > showCount;
						var front = 0;
						var page = 0;
						var end = totalPage -currentPageNo-showCount/2;
						if (showFlag){
							html = " <span>" + currentPageNo + "</span>";
							if( end <0){
								end = Math.abs(end);
							}else{
								end = 0;
							}
							
							for (front = 1; front < 5+end; front++) {
								page = ~~currentPageNo-front;
								if(page == 0){
									break;
								}
								html = "<a href='javascript:void(0)' onclick='gopage("
										+ page + ")'>" + page + "</a> "+html;
							}
							
							for (var p = 1; p <= 10-front; p++) {
								page = ~~currentPageNo+p;
								if(page > totalPage){
									break;
								}
								html += " <a href='javascript:void(0)' onclick='gopage("
										+ page + ")'>" + page + "</a> ";
							}
							if(page < totalPage && currentPageNo != totalPage){
								html += " <span>...</span>";
							}
							
						} else {
							for (var p = 1; p <= totalPage; p++) {
								if (currentPageNo == p) {
									html += " <span>" + p + "</span>";
								} else {
									html += " <a href='javascript:void(0)' onclick='gopage("
										+ p + ")'>" + p + "</a>";
								}
							}
						}
						
						return html;
					});
	template.helper('$pageSize', function(pageSize, count) {
		var html = "";
		var select = "";
		for (var q = 5; q <= 20; q = q + 5) {
			select = "";
			if ((pageSize == q && count > 0) || (count == 0 && q == 10)) {
				select = "selected='true'";
			}
			html += "<option value='"+q+"' "+select+">" + q + "</option>";
		}
		return html;
	});
	
	template.helper('$nextPage', function(currentNo) {
		return ~~currentNo+1;
	});
</script>