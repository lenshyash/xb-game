<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>

<title>${_title }-幸运大转盘抽奖</title>
<link rel="shortcut icon" href="${base }/images/logo.ico"
	type="image/x-icon" />
<link rel="stylesheet" rev="stylesheet"
	href="${base}/common/css/active2/reset.css" type="text/css" />
<link rel="stylesheet" rev="stylesheet"
	href="${base}/common/css/active2/global.css" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${base}/common/css/active2/YsCss.css" />
<link rel="stylesheet" type="text/css"
	href="${base}/common/js/layer/skin/layer.css" />
<script type="text/javascript"
	src="${base}/common/js/active2/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript"
	src="${base}/common/js/active2/jQueryRotate.2.2.js"></script>
<script type="text/javascript" src="${base}/common/js/active2/YsJs.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>

<style>
.luckturn {
	margin: 0 auto;
}

.luckpointer {
	cursor: pointer;
	-webkit-background-size: 255px 255px;
	-moz-background-size: 255px 255px;
	background-size: 255px 255px;
	-webkit-border-radius: 124px;
	border-radius: 124px;
	-webkit-transition: -webkit-transform 2s;
	-moz-transition: -moz-transform 2s;
	-o-transition: -o-transform 2s;
	-ms-transition: -ms-transform 2s;
}
</style>

<body>
	<span id="dzp_zp" style="position: absolute; top: 70px;">&nbsp;</span>
	<span id="dzp_rules" style="position: absolute; top: 845px;">&nbsp;</span>
	<span id="dzp_rhzj" style="position: absolute; top: 0px;">&nbsp;</span>
	<div id="PGetMsgId"
		style="font: 12px&amp;amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; #39; 微软雅黑 &amp;amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; #39; ,&amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; #39; 宋体 &amp;amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; amp; #39;; left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: fixed; z-index: 1988; display: none;">
		<div
			style="background: rgba(0, 0, 0, 0.7); height: 100%; z-index: 1997; padding: 100px 0 0 0;">
			<div
				style="background: #FFF; border: #000 1px solid; width: 60%; margin: 0 auto;">
				<ul
					style="height: 30px; line-height: 30px; padding-left: 10px; background: #BDC6CD; color: #5D696E; border-bottom: #B6BEC5 1px solid; font-weight: bold;">
					中奖纪录查询
				</ul>
				<ul class="PGetMsgIdList"
					style="font-weight: bold; background-color: #D8D8D8; border-top: #BDBDBD 1px solid; margin-bottom: 0;">
					<li style="width: 30%;">中奖账户</li>
					<li style="width: 69.75%;">奖品名称</li>
				</ul>
				<ul id="PGetMsgList" class="PGetMsgIdList2"
					style="width: 100%; height: 310px; margin: 0 0 0px 0; padding-top: 0; overflow: hidden; overflow-y: auto;">

				</ul>
				<ul class="PGetMsgIdList1" id="PGetPageList"
					style="font-weight: bold; background-color: #D8D8D8; margin-bottom: 0;">
					<li class=""><a link="1" href="javascript:void(0);"
						onclick="orderSelectPage(this)"><span>第一页</span></a></li>
					<li class=""><a link="1" href="javascript:void(0);"
						onclick="orderSelectPage(this)"><span>上一页</span></a></li>
					<li calss=""><a link="2" href="javascript:void(0);"
						onclick="orderSelectPage(this)"><span>下一页</span></a></li>
					<li calss=""><a link="" href="javascript:void(0);"
						onclick="orderSelectPage(this)"><span>尾页</span></a></li>
				</ul>
				<ul
					style="height: 38px; line-height: 30px; border-top: #B6BEC5 1px solid; text-align: right; background-color: #F6F6F6;">
					<input type="button"
						style="width: 50px; height: 23px; margin: 8px 15px 0 0;"
						value="确认" onclick="PGetMsgIdH()">
				</ul>
			</div>
		</div>
	</div>
	<div class="header">
		<div class="inner ov">
			<div class="logo fl">
				<h1>
					<a href=""><%-- ${_title } --%></a>
				</h1>
			</div>
			<div class="fr nav">
				<ul class="ov" style="color: white;">
					<li><a href="${base }/index.do" target="_blank">官方首页</a></li>
					<li><a href="javascript:void(0)" onclick="loginfrom()">登录</a></li>
					<li><a target="_blank" id="customerServiceUrlLink"
						href="${kfUrl }">在线客服</a></li>
					<li><a href="#zp" title="抽奖转盘">抽奖转盘</a></li>
					<li><a href="#gz" title="抽奖规则">抽奖规则</a></li>
					<li><a href="javascript:getMyRecord(1);" title="中奖查询">中奖查询</a></li>
					<li><span class="btn-span">帐号：<strong>${loginMember.account}</strong></span>
						<span class="btn-span">余额：<strong id="Balance">${loginMember.money}</strong></span>
						<span class="btn-span">积分：<strong id="adminJf">${userScore}</strong></span>
						<input type="hidden" id="hdjf" value="" /></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="cont1">
		<div class="inner ov">
			<div class="left mr22">
				<div class="h100"></div>
				<div class="h100"></div>
				<div class="h100"></div>
				<div class="h50"></div>
				<div class="luntop" style="overflow: hidden;">
					<ul id="infolistmsg" style="top: -70px;">
						<li><span class="mr22">woa**** </span>现金18元<span class="fr">08-02</span></li>
					</ul>
				</div>
			</div>
			<div class="md fl cw">
				<div class="h100"></div>
				<div class="h100"></div>
				<div class="h100"></div>
				<div class="h22"></div>
				<div id="hdzg"></div>
				<div class="h50"></div>
				<div class="h22"></div>
				<div class="btns">
					<a href="javascript:getMyRecord(1);" title="中奖查询"><img
						src="${base}/common/images/active2/btn1.png" alt=""></a>
				</div>

			</div>
			<div class="right fr">
				<div class="h50"></div>
				<a name="zp"></a>
				<div class="panzi">
					<div
						style="position: absolute; width: 438px; height: 435px; z-index: 998;">
						<table style="width: 438px; height: 435px;" border="0"
							cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td align="center" valign="middle"><img
										src="${base}/common/images/active2/panxin.png"
										class="luckpointer" alt="点击抽奖" style="cursor: pointer;"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<img id="cjimg" class="luckturn" src="#" width="435" height="445"
						alt="" style="z-index: 997; transform: rotate(0deg);">
				</div>
			</div>
		</div>
	</div>
	<div class="cont2">
		<div class="inner">
			<div class="h22"></div>
			<div class="img pl50" style="display: none;">
				<img src="${base}/common/images/active2/img1.png" alt="">
			</div>
			<div class="h22"></div>
			<div class="ov psr" style="display: none;">
				<div class="leftbtn">
					<img src="${base}/common/images/active2/leftbtn.png" alt="">
				</div>
				<div class="rightbtn">
					<img src="${base}/common/images/active2/rightbtn.png" alt="">
				</div>
			</div>
			<div class="h32"></div>
			<a name="gz"></a>
			<div class="img pl50">
				<img src="${base}/common/images/active2/img2.png" alt="">
			</div>
			<div class="h12"></div>
			<div class="nr">
				<ol id="hdgz">
				</ol>
			</div>
			<div class="h12"></div>
			<div class="img pl50">
				<img src="${base}/common/images/active2/img3.png" alt="">
			</div>
			<div class="h12"></div>
			<div class="nr">
				<ol id="hdsm">
				</ol>
			</div>
			<div class="h32"></div>
		</div>
	</div>

	<div id="loginfrom" class="pop-login" style="display: none;">
		<div class="wrapper" style="height: 374px;">
			<div class="title">会员登录</div>
			<div class="con">
				<form name="popup_login_form" id="popup_login_form" method="post">
					<div class="br txt">
						<label>账号：</label><span style="text-indent: -2000px;">帐号</span> <input
							name="Username" id="account" type="text">
							<p id="popup_username_msg"></p>
					</div>
					<div class="br pas">
						<label>密码：</label><span>请输入您的密码</span> <input name="Password"
							id="password" type="password" maxlength="16">
							<p id="popup_password_msg"></p>
					</div>
					<div class="br pas" id="verifyHtml1"
						style="display:; padding-left: 60px; border-left-width: 0px; margin-left: -10px;">
						<label>验证码：</label> <input name="verify" id="verifyCode"
							type="text" maxlength="5" style="margin-top: 0px; width: 100px;">
							<img
							style="cursor: pointer; width: 150px; height: 40px; margin-bottom: -12px;"
							src="${base}/verifycode.do" onclick="refreshVerifyCode(this)"
							id="vPic">
								<p id="geetest_verify_msg"></p>
					</div>
					<div class="br sub">
						<input type="button" value="登录" id="popupLoginButton"
							onclick="doLogin();">
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
     window.onload=function(){
    	 var num = '${domainFolder}';
 	// 	if(num = 'b05201'){
 	// 		$('#cjimg').css('transform','rotate(20deg)');
 	// 	};
    	// 判断终端类型
         var browser={
         versions:function(){
         var u = navigator.userAgent, app = navigator.appVersion;
         return {//移动终端浏览器版本信息
         trident: u.indexOf('Trident') > -1, //IE内核
         presto: u.indexOf('Presto') > -1, //opera内核
         webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
         gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
         mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
         ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
         android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
         iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
         iPad: u.indexOf('iPad') > -1, //是否iPad
         webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
         weixin: u.indexOf('MicroMessenger') > -1, //是否微信
         qq: u.match(/\sQQ/i) == " qq" //是否QQ
         };
         }(),
         language:(navigator.browserLanguage || navigator.language).toLowerCase()
         };

         if(browser.versions.mobile || browser.versions.ios || browser.versions.android ||
         browser.versions.iPhone || browser.versions.iPad){
         //移动
		 $("body").css("width","1140px")
         }else{
         //PC

         }
    }
     </script>
	<style>
.pop-login {
	z-index: 1000;
}

.pop-login, .pop-login .mask {
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
}

.error {
	height: 25px;
	line-height: 25px;
	padding-left: 22px;
	font-weight: normal;
	color: #e30000;
	background: url("../images/sprite.png") no-repeat 0px -1247px;
}

.right-icon, .valid {
	height: 25px;
	line-height: 28px;
	padding-left: 22px;
	font-weight: normal;
	color: #018951;
	background: url("../images/sprite.png") no-repeat 0px -1215px;
}

.pop-login .mask {
	background-color: #000;
	opacity: 0.3;
	/* IE 4-9 */
	filter: alpha(opacity = 30);
	/*This works in IE 8 & 9 too*/
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	/*IE4-IE9*/
	filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=30);
}

.pop-login .wrapper {
	position: absolute;
	left: 50%;
	top: 50%;
	width: 460px;
	height: 314px;
	margin-left: -230px;
	margin-top: -177px;
	background-color: #fff;
}

.pop-login .title {
	position: relative;
	height: 55px;
	margin-left: 0;
	padding-bottom: 0;
	border-bottom: 1px solid #e5e5e5;
	background-color: #e7f6fb;
	color: #006fbb;
	padding-left: 22px;
	line-height: 55px;
	font-size: 20px;
}

.pop-login .close {
	position: absolute;
	display: block;
	right: 0;
	top: 0;
	height: 55px;
	width: 55px;
	text-align: center;
	line-height: 55px;
	font-family: arial, sans-serif;
	cursor: pointer;
}

.pop-login .close:hover {
	background-color: #fff;
}

.pop-login .close:active {
	background-color: #cddfe5;
}

.pop-login .con {
	position: relative;
	padding-top: 27px;
	padding-left: 1px;
	margin: 0 63px;
}

#verifyHtml {
	padding-top: 5px;
}

.pop-login .br {
	position: relative;
	padding-bottom: 15px;
	padding-left: 50px;
}

.pop-login .con label {
	position: absolute;
	left: 0;
	top: 0;
	display: block;
	height: 40px;
	line-height: 40px;
	color: #000;
	font-size: 14px;
}

.pop-login .other span {
	color: #006fbb;
	float: left;
	height: 24px;
	line-height: 24px;
	padding-right: 5px;
}

.pop-login .other {
	padding-top: 0;
}

.pop-login .other a {
	display: block;
	float: left;
}

.other .qq, .other .wb {
	width: 29px;
	height: 24px;
	border: 1px solid #e5e6e8;
	background: url("../images/sprite.png") no-repeat -32px -734px;
}

.other .wb {
	background-position: -2px -734px;
	border-right: 0;
}

.pop-login .txt input, .pop-login .pas input {
	position: relative;
	z-index: 2;
	height: 20px;
	padding: 12px 8px 6px;
	width: 258px;
	border: 1px solid #c9c9c9;
	border-radius: 3px;
	*margin-left: -63px;
	border: 1px \9;
	height: 21px \9;
	width: 228px \9;
}

.pop-login .pas input {

}

.pop-login .checkbox span {
	position: relative;
	left: 3px;
	top: -2px;
	color: #a3a3a3;
}

.pop-login .checkbox a {
	position: absolute;
	right: 0;
	color: #a3a3a3;
}

.pop-login .checkbox a:hover {
	color: #e30000;
}

.pop-login .sub input {
	height: 40px;
	width: 138px;
	line-height: 40px;
	text-align: center;
	color: #fff;
	background-color: #34495e;
	font-size: 18px;
	border: 0;
	border-radius: 3px;
	*margin-left: -63px;
	outline: none;
}

.pop-login .sub input:hover {
	background-color: #57667b;
}

.pop-login .sub input:active {
	background-color: #2e4154;
}

.pop-login .sub a {
	color: #e10000;
	margin-left: 85px;
}

.pop-login .sub  .forget-password {
	top: 13px;
	position: absolute;
	left: 120px;
	width: 65px;
	color: #b3b3b3;
}

.pop-login .sub  .forget-password:hover {
	color: #e30000;
}

.pop-login .sub a:hover {
	text-decoration: underline;
}

.pop-login .checkbox a:hover {
	color: #006fbb;
	text-decoration: underline;
}

.pop-login .txt span, .pop-login .pas span {
	position: absolute;
	left: 50px;
	top: 0;
	z-index: 0;
	display: block;
	width: 258px;
	height: 40px;
	line-height: 42px;
	padding: 0 8px;
	color: #b0b0b0;
	border-radius: 3px;
	text-align: left;
	border: 1px solid #c9c9c9 \9;
	background: url("../images/sprite.png") no-repeat 248px -443px;
}

.pop-login .pas span {
	background-position: 248px -407px;
}

.pop-login .txt input:focus, .pop-login .pas input:focus {
	border: 1px solid #006fbb;
	border: 0 \9;
}
</style>

	<div class="footer">
		<div class="inner">
			<div class="tac l50" style="color: #a9a9a9;">Copyright ©
				${_title } Reserved</div>
			<div class="tac"></div>
		</div>
	</div>

	<script>
		function loginfrom() {
			layer.open({
				type : 1,
				title : '',
				shadeClose : true,
				shade : 0.8,
				area : [ '480px', '400px' ],
				content : $('#loginfrom')
			});
		}

		function refreshVerifyCode() {
			var time = (new Date()).getTime();
			$("#vPic").attr("src", "${base}/verifycode.do?time=" + time);
		}

		function doLogin() {
			var account = $.trim($("#account").val());
			var password = $("#password").val();
			var verifyCode = $("#verifyCode").val();
			if (!account) {
				alert("用户名不能为空！");
				return;
			}
			if (!password) {
				alert("密码不能为空！");
				return;
			}
			if (!verifyCode) {
				alert("请输入验证码！");
				return;
			}
			var data = {};
			data["password"] = password;
			data["account"] = account;
			data["verifyCode"] = verifyCode;
			$.ajax({
				url : "${base}/login.do",
				type : "post",
				data : data,
				success : function(result, textStatus, xhr) {
					var ceipstate = xhr.getResponseHeader("ceipstate")
					if (!ceipstate || ceipstate == 1) {// 正常响应
						//parent.location.href = '${base}' || '/';
						parent.location.reload();
					} else if (ceipstate == 2) {// 后台异常
						alert("后台异常，请联系管理员!");
						reVerifyCode();
					} else if (ceipstate == 3) { // 业务异常
						alert(result.msg);
						reVerifyCode();
					}
				}
			});
		}

		$(function() {
			initTurnTable();
			bindAwardImg();
		});
		var base = "${base}";
		var deg = 0;
		var awardCount = 0;
		var turnCount = 4;//至少转4圈
		var trunning = false;
		var activeId = 0;
		function bindAwardImg() {
			$(".luckpointer").click(function() {
				if (!trunning) {
					trunning = true;
					award();
				}
			})
		}
		function transform() {
			deg += (360 * turnCount);
			$(".luckpointer").css("-webkit-transform",
					"rotateZ(" + deg + "deg)");
			$(".luckpointer").css("-moz-transform", "rotateZ(" + deg + "deg)");
			$(".luckpointer").css("-o-transform", "rotateZ(" + deg + "deg)");
			$(".luckpointer").css("-ms-transform", "rotateZ(" + deg + "deg)");
			$(".luckpointer").css("transform", "rotateZ(" + deg + "deg)");
		}

		function award() {
			$.ajax({
				url : base + "/center/active/award.do",
				data : {
					"activeId" : activeId
				},
				success : function(result) {
					deg = getDeg(result.index)
					transform();
					setTimeout(function() {
						alert(result.awardName);
						var a = $("#adminJf").text();
						var b = $("#hdjf").val();
						var c = parseInt(a) - parseInt(b);
						if (c && c >= 0) {
							$("#adminJf").text(c.toFixed(2));
						}
						trunning = false;
					}, 2000);

				}
			});
		}

		function getDeg(index) {
			var singleDeg = Math.round(360 / awardCount);
			var halfSingle = Math.round(singleDeg / 2);
			var before = deg + (360 - (deg % 360));
			var cur = (singleDeg * (index - 1) + halfSingle);
			return before + cur;
		}

		function initTurnTable() {
			$.ajax({
				url : base + "/center/active/activedata.do",
				success : function(result) {
					if (result != null) {
						awardCount = result.awardCount;
						activeId = result.id;
						var imgPath = result.imgPath;
						if (imgPath.indexOf("http:") == -1
								&& imgPath.indexOf("https:") == -1) {
							imgPath = '${base}' + imgPath;
						}

						$(".luckturn").attr("src", imgPath);
						var hdgz=result.activeHelp;
						hdgz=hdgz.replace(/该文本需要替换网站名称/g,"${_title}");
						$("#hdgz").html(hdgz);
						var hdsm=result.activeRemark;
						hdsm=hdsm.replace(/该文本需要替换网站名称/g,"${_title}");
						$("#hdsm").html(hdsm);
						var hdzg=result.activeRole;
						hdzg=hdzg.replace(/该文本需要替换网站名称/g,"${_title}");
						$("#hdzg").html(hdzg);
						if (result.score && result.score >= 0) {
							$("#hdjf").val(result.score);
						}
						initLastRecord();
					} else {
						alert('活动未开启，详询在线客服');
						initLastRecord();
					}
				}
			});
		}

		function initLastRecord() {
			$.ajax({
				url : base + "/center/active/lastrd.do",
				data : {
					activeId : activeId
				},
				success : function(result) {
					var record = [];
					var c = '';

					if (!result || result.length <= 0) {
						return;
					}
					for (var i = 0; i < result.length; i++) {
						record = result[i];
						c += "<li><span class='mr22'>"
								+ getAdminName(record.account) + " </span>"
								+ record.productName + "<span class='fr'>"
                                + getDate(record.create_datetime) + "</span></li>";
					}
					$("#infolistmsg").html(c);
					lunTopFn();
				}
			});
		}

		function getMyRecord(page) {
			$('#PGetMsgId').show();
			$.ajax({
				url : base + "/center/active/records.do",
				data : {
					activeId : activeId,
					page : page,
					rows : 10
				},
				success : function(result) {
					var html = '';
					if (!result || result.rows.length <= 0) {
						return;
					}
					for (var i = 0; i < result.rows.length; i++) {
						record = result.rows[i];
						html += '<li style="width: 30%;">' + record.account
								+ '</li>';
						html += '<li style="width: 69.75%;">'
								+ record.productName + '</li>';
					}
					$("#PGetMsgList").html(html);
					var w = Math.ceil(result.total / 10);
					pageChuLi(w, page);
				}
			});
		}

		function orderSelectPage(el) {
			var cl = $(el).parent().attr("class");
			if (cl == "nocur") {
				return false;
			}
			var page = $(el).attr("link");
			getMyRecord(page);
		}

		function PGetMsgIdH() {
			$("#PGetMsgId").hide();
		}

		(function($) {
			// 备份jquery的ajax方法
			var _ajax = $.ajax;
			window.$ajax = _ajax;
			// 重写jquery的ajax方法
			$.ajax = function(opt) {
				if (!opt.dataType) {
					opt.dataType = "json";
				}
				if (!opt.type) {
					opt.type = "post";
				}
				// 备份opt中error和success方法
				var fn = {
					error : function(XMLHttpRequest, textStatus, errorThrown) {
					},
					success : function(data, textStatus, xhr) {
					}
				}
				if (opt.error) {
					fn.error = opt.error;
				}
				if (opt.success) {
					fn.success = opt.success;
				}

				// 扩展增强处理
				var _opt = $.extend(opt, {
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						var statusCode = XMLHttpRequest.status;
						// 错误方法增强处理
						if (statusCode == 404) {
							alert("[" + opt.url + "] 404 not found");
						} else {
							fn.error(XMLHttpRequest, textStatus, errorThrown);
						}
					},
					success : function(data, textStatus, xhr) {
						var ceipstate = xhr.getResponseHeader("ceipstate")
						if (ceipstate == 1) {// 正常响应
							fn.success(data, textStatus, xhr);
						} else if (ceipstate == 2) {// 后台异常
							alert("后台异常，请联系管理员!");
						} else if (ceipstate == 3) { // 业务异常
							alert(data.msg);
						} else if (ceipstate == 4) { // 未登录
							loginfrom();
						} else if (ceipstate == 5) {// 没有权限
							alert("没有权限");
						} else if (ceipstate == 6) {// 登录异常

							if (data.msg == '暂无活动') {
								alert('活动未开启，详询在线客服');
							} else {
								alert(data.msg);
							}

							top.location.href = base + "/loginError.do";
						} else {
							fn.success(data, textStatus, xhr);
						}
					}
				});
				_ajax(_opt);
			};
		})(jQuery);
	</script>

</body>
</html>
