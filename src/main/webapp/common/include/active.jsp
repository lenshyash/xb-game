<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
.luckturn {
	width: 572px;
	height: 572px;
	margin: 0 auto;
}

.luckpointer {
	width: 104px;
	height: 255px;
	margin: 0 auto;
	position: relative;
	top: 160px;
	cursor: pointer;
	-webkit-background-size: 255px 255px;
	-moz-background-size: 255px 255px;
	background-size: 255px 255px;
	-webkit-border-radius: 124px;
	border-radius: 124px;
	-webkit-transition: -webkit-transform 2s;
	-moz-transition: -moz-transform 2s;
	-o-transition: -o-transform 2s;
	-ms-transition: -ms-transform 2s;
	background: no-repeat url('${base}/common/images/active/zhuan.png') left
		top;
}
</style>
<div class="luckturn">
	<div class="luckpointer"></div>
</div>
<div id="ruleHelp"></div>
<div id="lastRecord"></div>
<div id="myRecord"></div>
<a href="javascript:void(0)" onclick="getMyRecord();">我的中奖记录</a>
<script>
	$(function() {
		initTurnTable();
		bindAwardImg();
		initLastRecord();
	});
	var base = "${base}";
	var deg = 0;
	var awardCount = 0;
	var turnCount = 4;//至少转4圈
	var trunning = false;
	var activeId = 0;
	function bindAwardImg() {
		$(".luckpointer").click(function() {
			if (!trunning) {
				trunning = true;
				award();
			}
		})
	}
	function transform() {
		deg += (360 * turnCount);
		$(".luckpointer").css("-webkit-transform", "rotateZ(" + deg + "deg)");
		$(".luckpointer").css("-moz-transform", "rotateZ(" + deg + "deg)");
		$(".luckpointer").css("-o-transform", "rotateZ(" + deg + "deg)");
		$(".luckpointer").css("-ms-transform", "rotateZ(" + deg + "deg)");
		$(".luckpointer").css("transform", "rotateZ(" + deg + "deg)");
	}

	function award() {
		$.ajax({
			url : base + "/center/active/award.do",
			data : {
				"activeId" : activeId
			},
			success : function(result) {
				deg = getDeg(result.index)
				transform();
				setTimeout(function() {
					alert(result.awardName);
					trunning = false;
				}, 2000);

			}
		});
	}

	function getDeg(index) {
		var singleDeg = Math.round(360 / awardCount);
		var halfSingle = Math.round(singleDeg / 2);
		var before = deg + (360 - (deg % 360));
		var cur = (singleDeg * (index - 1) + halfSingle);
		return before + cur;
	}

	function initTurnTable() {
		$.ajax({
			url : base + "/center/active/activedata.do",
			success : function(result) {
				awardCount = result.awardCount;
				activeId = result.id;
				var imgPath = result.imgPath;
				if (imgPath.indexOf("http:") == -1
						&& imgPath.indexOf("https:") == -1) {
					imgPath = '${base}' + imgPath;
				}

				$(".luckturn").attr(
						"style",
						"background: no-repeat url('" + imgPath
								+ "') left top;");
				$("#ruleHelp").html(result.activeHelp);

			}
		});
	}

	function initLastRecord() {
		$.ajax({
			url : base + "/center/active/lastrd.do",
			data : {
				activeId : activeId
			},
			success : function(result) {
				var record = [];
				for (var i = 0; i < result.length; i++) {
					record = result[i];
					$("#lastRecord")
							.append(
									"<br>" + record.account + "获得"
											+ record.productName);
				}
			}
		});
	}

	function getMyRecord() {
		$.ajax({
			url : base + "/center/active/records.do",
			data : {
				activeId : activeId
			},
			success : function(result) {
				for (var i = 0; i < result.rows.length; i++) {
					record = result.rows[i];
					$("#myRecord")
							.append(
									"<br>" + record.account + "获得"
											+ record.productName);
				}
			}
		});
	}
</script>