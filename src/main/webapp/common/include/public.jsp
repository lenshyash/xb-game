<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div style="position: fixed; left: 0; bottom: 0;display: flex;align-items:flex-end;z-index:99999;">
		<c:if test="${isZpOnOff eq 'on'}" >
			<jsp:include page="/common/include/turnlate.jsp"></jsp:include>
		</c:if>
		<c:if test="${isQdOnOff eq 'on'}">
			<jsp:include page="/common/include/exchange.jsp"></jsp:include>
		</c:if>
	</div>
	
<script type="text/javascript">
	<jsp:include page="/common/modelCommon/index.jsp"></jsp:include>
</script>

