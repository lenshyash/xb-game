
"use strict";

function ViewModel (){
	return {
		Info : {}, Function : {}, Component : {}, Action : {}
	};
}

function ActionModel (){
	var model = new ViewModel();
	return $.extend({
		
	}, model);
}

window.$$ = window.Application = $.extend({ActionInstance: {}}, new ViewModel());

$$.Action.QuickRecharge = {
	name: "",
	getInstance: function(){
		var baseAction = new ActionModel();
		var thisAction = this;
		return {
			init: function (){

				function xinghao(inn){
					var out = "";
					for(var i = 0; i < inn.length - 1; i++){
						out += "*";
					}
					return out + inn.substr(-1, 1);
				}

				$._ajax(baseUrl + "/mobile/deposit/config.do", function (status, message, data, result) {
					var fasts = [];
					var banks = [];
					var onlines = [];
					if(data.fastFlag == 'on'){
						$._ajax({url: baseUrl + "/mobile/deposit/fastpay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#fastpaysTemplate").html();
							var $fastpayContent = $("#fastpayContent");
							fasts = data.fasts;
							$fastpayContent.html(_.template(tplHtml)({list: fasts}));
						});
					}
					if(data.bankFlag == 'on'){
						$._ajax({url: baseUrl + "/mobile/deposit/bankpay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#bankpaysTemplate").html();
							var $bankpayContent = $("#bankpayContent");
							banks = data.banks;
							$bankpayContent.html(_.template(tplHtml)({list: banks}));
						});
					}
					if(data.onlineFlag == 'on'){
						$._ajax({url: baseUrl + "/mobile/deposit/onlinepay.do", async: !1}, function (status, message, data, result) {
							var tplHtml = $("#onlinepaysTemplate").html();
							var $onlinepayContent = $("#onlinepayContent");
							onlines = data.onlines;
							$onlinepayContent.html(_.template(tplHtml)({list: onlines}));
						});
					}

					// var fastIndex = false;
					// var bankIndex = false;
					// var onlineIndex = false;
					var config = {};
					$(".pwradio").on("change", function(){
						var $this = $(this);
						var payaction = $this.attr("payaction");
						var index = $this.attr("index");

						if(payaction == "fastpay"){
							config = {tplHtmlSelector: "#fastpayInputInfoTemplate", data: fasts, dataIndex: index, payaction: "fastpay", desc: data.fastDesc};
						} else if(payaction == "bankpay"){
							config = {tplHtmlSelector: "#bankpayInputInfoTemplate", data: banks, dataIndex: index, payaction: "bankpay", desc: data.bankDesc};
						} else if(payaction == "onlinepay"){
							config = {tplHtmlSelector: "#onlinepayInputInfoTemplate", data: onlines, dataIndex: index, payaction: "onlinepay", desc: data.onlineDesc};
						}
						$("#showDescDiv").html(config.desc);
						var $inputInfoContent = $("#inputInfoContent");
						var tplHtml = $(config.tplHtmlSelector).html();
						$inputInfoContent.html(_.template(tplHtml)(config.data[config.dataIndex]));
					});

					$("#next").on("click", function(){
						var account = $("#account").val();
						var confireaccount = $("#confireaccount").val();
						var money = $("#money").val();
						var payId = $("input.pwradio:checked").val();
						var iconCss = $("input.pwradio:checked").attr("iconCss");
						var paytype = $("input.pwradio:checked").attr("paytype");
						var min = parseFloat($("input.pwradio:checked").attr("min")||-1,10);
						var max = parseFloat($("input.pwradio:checked").attr("max")||-1,10);
						if(!account){
							$.alert("请输入会员账号");
							return;
						}
						if(account != confireaccount){
							$.alert("两次输入的账号不一致");
							return;
						}
						if(!payId){
							$.alert("请选择充值方式");
							return;
						}
						if(money=='' || isNaN(money)){
							$.alert("充值金额格式错误");
							return;
						}
						money=parseFloat(money,10);
						if(min!=-1 && money < min){
							$.alert("充值金额不能小于" + min + "元");
							return;
						}
						if(max!=-1 && money > max){
							$.alert("充值金额不能大于" + max + "元");
							return;
						}

						if(config.payaction == "fastpay"){
							$.ajax({
								type : "POST",
								url : baseUrl + "/mobile/deposit/fastpaySubmit.do",
								data : {
									money: money,
									// depositor: $("#depositor").val(),
									bankCards: $("#bankCards").val(),
									payId: payId
								},
								success : function(result) {
									if(result.success == false){
										$.alert(result.msg);
									} else if(result.success == true){
										$.alert("充值申请已提交，请尽快完成充值！", function(){
											var tplHtml = $("#fastpaySuccessTemplate").html();
											var $kakakakaContent = $("#kakakakaContent");
											$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money})));
											$.popup('.popup-topayaction');
										});
									}
								}
							});
						} else if(config.payaction == "bankpay"){
							$.ajax({
								type : "POST",
								url : baseUrl + "/mobile/deposit/bankpaySubmit.do",
								data : {
									money: money,
									depositor: $("#depositor").val(),
									bankId: payId
								},
								success : function(result) {
									if(result.success == false){
										$.alert(result.msg);
									} else if(result.success == true){
										$.alert("充值申请已提交，线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！", function(){
											var tplHtml = $("#bankpaySuccessTemplate").html();
											var $kakakakaContent = $("#kakakakaContent");
											$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money})));
											$.popup('.popup-topayaction');
										});
									}
								}
							});
						} else if(config.payaction == "onlinepay"){
							topay(money, payId, iconCss, paytype, function(result){
								if(result.success == false){
									$.alert(result.msg);
								} else if(result.success == true){
									if(result && result.data && result.data.formParams){
										var tplHtml = $("#toPayTemplate").html();
										var $kakakakaContent = $("#kakakakaContent");
										$kakakakaContent.html(_.template(tplHtml)(result.data));
										$.popup('.popup-topayaction');
									}else{
										$.alert("系统发生错误");
									}
								}
							}, account);
						}
					});
				});
			},
			reinit: function(){
				
			},
			destroy: function(){
				
			}
		};
	}
};

$(function(){
	"use strict";

	var pageInitF = function (){
		$(document).on("pageInit", "#quickRechargePage", function(e, pageId, $page) {
			$$.ActionInstance.QuickRecharge = $$.Action.QuickRecharge.getInstance();
			$$.ActionInstance.QuickRecharge.init();
		});
	}

	var pageReinitF = function() {
		$(document).on("pageReinit", "#quickRechargePage", function(e, pageId, $page) {
			if($$.ActionInstance.QuickRecharge){
				$$.ActionInstance.QuickRecharge.reinit();
			}
		});
	}

	pageInitF();
	pageReinitF();
	$.init();
});
