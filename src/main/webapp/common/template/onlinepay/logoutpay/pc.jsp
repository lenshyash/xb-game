<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta name="renderer" content="webkit" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>${_title }-快速充值中心</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" href="${base }/common/template/onlinepay/logoutpay/css/pc.css" />
<link rel="stylesheet" href="${base }/common/css/core/bank/css/bank.css?v=1.3">

<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">

</head>
<body>

	<div class="header">
		<div class="online-service">
			<img src="${base }/common/template/onlinepay/logoutpay/images/header_04.png" align="" />
		</div>
		<div class="topbar">
			<a href="${base }">首页</a>&nbsp;&nbsp;
		</div>
	</div>
	<div class="content">
		<a
			href="${kfUrl }"
			target=_blank class="service"> 在线客服</a>
		<div class="item" style="margin-right: 10px;">
			<div class="hd">
				<div class="btnbox">
					<div class="hd" style="width: 905px; height: 43px">
						<h2>${_title }</h2>
						<b><span class="txt"><font size="3" color="#000000">请输入正确的会员账号，以确保可以正常充值到账。如有问题请联系<a href="${kfUrl }" target=_blank style="color: #FF0000">【在线客服】</a></font></span></b>
					</div>
				</div>
			</div>
		</div>
		<!--提示文字-->
		<div></div>
		<!--提示文字-->
		<div id="kakakakaContent" style="margin-top: 70px">
			<table class="content-table">
				<tr>
					<td class="title">会员账号：</td>
					<td class="inputtd">
						<input name="account" id="account"
						type="text" value="" class="table-input"
						placeholder="请填写${_title }会员账号，否则无法正常充值！！！" /></td>
					<td align="center"><font color="#FF0000">*必填</font></td>
				</tr>
				<tr>
					<td class="title">确认账号：</td>
					<td><input name="confireaccount" id="confireaccount" type="text"
						value="" class="table-input" placeholder="请再次填写${_title }会员账号,！" /></td>
					<td align="center"><font color="#FF0000">*必填</font></td>
				</tr>
				<tr>
					<td class="title">支付类型：</td>
					<td>
						<ul class="payWayUl" id="onlinepayContent">
						</ul>
					</td>
					<td align="center"><font color="#FF0000">*必选</font></td>
				</tr>
				<tr>
					<td class="title">确认额度：</td>
					<td><input type="text" name="money" id="money"
						class="table-input"
						placeholder="微信/支付宝（1-5000元）大额存款可分多次存入【无需提交存款信息，全自动，秒到账】" /></td>
					<td align="center"><font color="#FF0000">*必填</font></td>
				</tr>
				<!-- 
				<tr>
					<td class="title">存款时间：</td>
					<td><input name="P_Time" id="P_Time" type="text"
						value="17-02-27 12:03:40" class="table-input" /></td>
					<td align="center">无需填写</td>
				</tr>
				 -->
			</table>
			<div class="form-btn">
				<a id="next" href="javascript: void(0);">确认支付</a>
			</div>
		</div>
		<p class="tips">
			<span style="color: #f00;">温馨提示：</span>为了避免掉单情况的发生，请您在支付完成后，需等"支付成功"页面跳转出来,
			再关闭页面，以免掉单！感谢配合！！！ <br>支付成功后，若3分钟内未能及时到达您的会员账号请联系<a
				href="${kfUrl }"
				target=_blank style="color: #f00">【在线客服】</a>咨询；<br>${_title }祝您游戏愉快，盈利多多！O(∩_∩)O
		</p>
	</div>
	<div class="copyright">
		<!-- 
		Copyright &copy; ${_title } Reserved E-mail：kf@777.cm 電話：00853-63606677
		 -->
		<p>${_title }所提供的产品和服务，是由澳门特别行政区 The Macao Special Administrative
			Region.授权和监管.选择我们，您将拥有可靠的资金保障和优质的服务.
	</div>

	<script type="text/javascript">
		var baseUrl = "${base }";
		var hostUrl1="${hostUrl1}";
	</script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/js/jquery/3.1.1/jquery.min.js'></script>
	<script type="text/javascript" src="${base }/mobile/script/underscore/underscore-min.js"></script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/js/common.js'></script>
	<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>

	<script type="text/javascript">
		$(function(){
			$._ajax(baseUrl + "/mobile/deposit/config.do", function (status, message, data, result) {
				var fasts = [];
				var banks = [];
				var onlines = [];
				if(data.fastFlag == 'on'){
					$._ajax({url: baseUrl + "/mobile/deposit/fastpay.do", async: !1}, function (status, message, data, result) {
						var tplHtml = $("#fastpaysTemplate").html();
						var $fastpayContent = $("#fastpayContent");
						fasts = data.fasts;
						$fastpayContent.html(_.template(tplHtml)({list: fasts}));
					});
				}
				if(data.bankFlag == 'on'){
					$._ajax({url: baseUrl + "/mobile/deposit/bankpay.do", async: !1}, function (status, message, data, result) {
						var tplHtml = $("#bankpaysTemplate").html();
						var $bankpayContent = $("#bankpayContent");
						banks = data.banks;
						$bankpayContent.html(_.template(tplHtml)({list: banks}));
					});
				}
				if(data.onlineFlag == 'on'){
					$._ajax({url: baseUrl + "/mobile/deposit/onlinepay.do", async: !1}, function (status, message, data, result) {
						var tplHtml = $("#onlinepaysTemplate").html();
						var $onlinepayContent = $("#onlinepayContent");
						onlines = data.onlines;
						$onlinepayContent.html(_.template(tplHtml)({list: onlines}));
					});
				}
				var config = {};
				$(".pwradio").on("change", function(){
					var $this = $(this);
					var payaction = $this.attr("payaction");
					var index = $this.attr("index");

					if(payaction == "fastpay"){
						config = {tplHtmlSelector: "#fastpayInputInfoTemplate", data: fasts, dataIndex: index, payaction: "fastpay", desc: data.fastDesc};
					} else if(payaction == "bankpay"){
						config = {tplHtmlSelector: "#bankpayInputInfoTemplate", data: banks, dataIndex: index, payaction: "bankpay", desc: data.bankDesc};
					} else if(payaction == "onlinepay"){
						config = {tplHtmlSelector: "#onlinepayInputInfoTemplate", data: onlines, dataIndex: index, payaction: "onlinepay", desc: data.onlineDesc};
					}
				});

				$("#next").on("click", function(){
					var account = $("#account").val();
					var confireaccount = $("#confireaccount").val();
					var money = $("#money").val();
					var payId = $("input.pwradio:checked").val();
					var iconCss = $("input.pwradio:checked").attr("iconCss");
					var paytype = $("input.pwradio:checked").attr("paytype");
					var min = parseFloat($("input.pwradio:checked").attr("min")||-1,10);
					var max = parseFloat($("input.pwradio:checked").attr("max")||-1,10);
					if(!account){
						alert("请输入会员账号");
						return;
					}
					if(account != confireaccount){
						alert("两次输入的账号不一致");
						return;
					}
					if(!payId){
						alert("请选择充值方式");
						return;
					}
					if(money=='' || isNaN(money)){
						alert("充值金额格式错误");
						return;
					}
					money=parseFloat(money,10);
					if(min!=-1 && money < min){
						alert("充值金额不能小于" + min + "元");
						return;
					}
					if(max!=-1 && money > max){
						alert("充值金额不能大于" + max + "元");
						return;
					}

					if(config.payaction == "fastpay"){
						$.ajax({
							type : "POST",
							url : baseUrl + "/mobile/deposit/fastpaySubmit.do",
							data : {
								money: money,
								// depositor: $("#depositor").val(),
								bankCards: $("#bankCards").val(),
								payId: payId
							},
							success : function(result) {
								if(result.success == false){
									alert(result.msg);
								} else if(result.success == true){
									alert("充值申请已提交，请尽快完成充值！", function(){
										var tplHtml = $("#fastpaySuccessTemplate").html();
										var $kakakakaContent = $("#kakakakaContent");
										$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money})));
										$.popup('.popup-topayaction');
									});
								}
							}
						});
					} else if(config.payaction == "bankpay"){
						$.ajax({
							type : "POST",
							url : baseUrl + "/mobile/deposit/bankpaySubmit.do",
							data : {
								money: money,
								depositor: $("#depositor").val(),
								bankId: payId
							},
							success : function(result) {
								if(result.success == false){
									alert(result.msg);
								} else if(result.success == true){
									alert("充值申请已提交，请尽快完成充值！", function(){
										var tplHtml = $("#bankpaySuccessTemplate").html();
										var $kakakakaContent = $("#kakakakaContent");
										$kakakakaContent.html(_.template(tplHtml)($.extend(config.data[config.dataIndex], {orderNo: result.data, money: money})));
										$.popup('.popup-topayaction');
									});
								}
							}
						});
					} else if(config.payaction == "onlinepay"){
						topay(money, payId, iconCss, paytype, function(result){
							if(result.success == false){
								alert(result.msg);
							} else if(result.success == true){
								if(result && result.data && result.data.formParams){
									var tplHtml = $("#toPayTemplate").html();
									var $kakakakaContent = $("#kakakakaContent");
									$kakakakaContent.html(_.template(tplHtml)(result.data));
								}else{
									alert("系统发生错误");
								}
							}
						}, account);
					}
				});
			});
		});
	</script>

	<script type="text/html" id="onlinepaysTemplate" style="display: none;">
			{#
			_.map(list, function(item, index){
			#}
			<li>
				<label >
					<input type="radio" class="pwradio" name="usserradio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payaction="onlinepay" paytype="{{item.payType}}" index="{{index}}" />
					{#
							if(item.icon){
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'' + item.icon + '\');">' + item.payName + '</span>');
							} else if(item.payType == "3"){ // 微信
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/weixin.jpg\');">' + item.payName + '</span>');
							} else if(item.payType == "4"){ // 支付宝
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/zhifubao.jpg\');">' + item.payName + '</span>');
							} else if(item.payType == "5"){ // qq钱包
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/qqpay.png\');">' + item.payName + '</span>');
							} else {
								print('<span title="' + item.payName + '" class="bankicon ' + item.iconCss + ' pwicon">' + item.payName + '</span>');
							}
					#}
					<span class="pwtips" >最小充值金额<font class="hong">{{item.min}}</font>元</span>
				</label>
			</li>
			{#
			});
			#}
	</script>
	<script type="text/html" id="toPayTemplate" style="display: none;">
		<form action="{{formAction}}" method="post" target="_blank">
			{#
			_.map(formParams, function(value, key){
			#}
				<input type="hidden" name="{{key}}" value="{{value}}"/>
			{#
			});
			#}
			<ul class="popup_template_ul topay">
				<li>
					<span class="ititle">订单号： </span><span>{{orderId}}</span>
				</li>
				<li>
					<span class="ititle">会员账号： </span><span>{{account}}</span>
				</li>
				<li>
					<span class="ititle">充值金额： </span><span>{{amount}}</span>
				</li>
			</ul>
			<button class="nextbtn" type='submit'>确认送出</button>
		</form>
	</script>
</body>
</html>