<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title }-快速充值中心</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<link rel="stylesheet" href="${base }/mobile/anew/resource/css/iconfont/iconfont.css">
<link rel="stylesheet" href="${base }/mobile/anew/resource/light7/css/light7.min.css">
<link rel="stylesheet" href="${base }/mobile/anew/resource/css/light7_reset.css">
<link rel="stylesheet" href="${base }/mobile/anew/resource/css/base.css">

<link rel="stylesheet" href="${base }/common/css/core/bank/css/bank.css?v=1.3">


<link rel="stylesheet" href="${base }/mobile/anew/resource/css/deposit_withdraw.css">
</head>
<body>
	<!-- /* quickRechargePage start */ -->
	<div class="page page-current" id="quickRechargePage">
		<header class="bar bar-nav">
			<!-- 
			<a class="button button-link button-nav pull-left external" href="javascript: history.go(-1);">
				<span class="icon icon-left"></span>返回
			</a>
			 -->
			<a class="title">充值</a>
		</header>
		<div class="chongzhitishi">
			<em class="iconfont icon-iconfonticonfontinfo"></em> 充值无手续费
		</div>
		<div class="content paySelectWay1Content">
			<div class="row no-gutter chongzhitl">
				<em class="iconfont icon-yuanquan1 hong"></em>
				会员账号
			</div>
			<div class="row inputinfo_content chongzhiinptu">
				<input id="account" type="text" value="">
			</div>
			<div class="row no-gutter chongzhitl">
				<em class="iconfont icon-yuanquan1 hong"></em>
				确认账号
			</div>
			<div class="row inputinfo_content chongzhiinptu">
				<input id="confireaccount" type="text" value="">
			</div>
			<div class="row no-gutter chongzhitl">
				<em class="iconfont icon-yuanquan1 hong"></em>
				请选择充值方式进入下一步
			</div>
			<div class="row no-gutter payWay">
				<p class="pwtitle">线上充值</p>
				<ul class="payWayUl" id="onlinepayContent">
					<!-- 
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="6" iconcss="yifubao" min="1" max="1234567876543" payaction="onlinepay" paytype="1" index="0">
							<span title="易付宝" class="bankicon yifubao pwicon">易付宝</span>
							<span class="pwtips">最小充值金额<em class="hong">1</em>元</span>
						</label>
					</li>
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="6" iconcss="yifubao" min="1" max="1234567876543" payaction="onlinepay" paytype="1" index="0">
							<span title="易付宝" class="bankicon yifubao pwicon">易付宝</span>
							<span class="pwtips">最小充值金额<em class="hong">1</em>元</span>
						</label>
					</li>
					 -->
				</ul>
			</div>
			<div class="row no-gutter payWay">
				<!-- <p class="pwtitle">线下充值</p> -->
				<ul class="payWayUl" id="fastpayContent">
					<!-- 
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="13" iconcss="alipay" min="1" max="13434" payaction="fastpay" index="0">
							<span title="支付宝" class="bankicon alipay pwicon">支付宝</span>
							<span class="pwtips">最小充值金额<font class="hong">1</font>元</span>
						</label>
					</li>
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="14" iconcss="wechat" min="1" max="1111111111" payaction="fastpay" index="1">
							<span title="微信" class="bankicon wechat pwicon">微信</span>
							<span class="pwtips">最小充值金额<font class="hong">1</font>元</span>
						</label>
					</li>
					 -->
				</ul>
				<ul class="payWayUl" id="bankpayContent">
					<!-- 
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="5" iconcss="bankicbc" min="1" max="111111" payaction="bankpay" index="0">
							<span title="工商银行" class="bankicon bankicbc pwicon">工商银行</span>
							<span class="pwtips">最小充值金额<font class="hong">1</font>元</span>
						</label>
					</li>
					<li>
						<label>
							<input type="radio" class="pwradio" name="usserradio" value="6" iconcss="bankcmb" min="1" max="11111111" payaction="bankpay" index="1">
							<span title="招商银行" class="bankicon bankcmb pwicon">招商银行</span>
							<span class="pwtips">最小充值金额<font class="hong">1</font>元</span>
						</label>
					</li>
					 -->
				</ul>
			</div>
			<div id="showDescDiv" class="row no-gutter showdesc">
				<!-- 提醒：入款帐号会有变更，请每次存款前先核对入款帐号，感谢您的支持和配合！ -->
			</div>
			<div id="inputInfoContent" class="row inputinfo_content">
			</div>
			<div class="row netx_content">
				<a id="next" href="javascript: void(0);" class="nextbtn">下一步</a>
			</div>
		</div>
	</div>
	<div class="popup popup-topayaction">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left close-popup" href="javascript: void(0);">
				<span class="icon icon-left"></span>关闭
			</a>
			<a class="title">充值</a>
		</header>
		<div class="content popup-topayaction-content">
			<div class="row no-gutter userinfo">
				<span>
					当前账户：${account }
				</span>
			</div>
			<div id="kakakakaContent" class="row no-gutter payWay">
			</div>
		</div>
	</div>
	<script type="text/html" id="fastpaysTemplate" style="display: none;">
			{#
			_.map(list, function(item, index){
			#}
			<li>
				<label>
					<input type="radio" class="pwradio" name="usserradio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payaction="fastpay" index="{{index}}" />
					{#
							if(!item.icon){
								print('<span title="' + item.payName + '" class="bankicon ' + item.iconCss + ' pwicon">' + item.payName + '</span>');
							} else {
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'' + item.icon + '\');">' + item.payName + '</span>');
							}
					#}
					<span class="pwtips" >最小充值金额<font class="hong">{{item.min}}</font>元</span>
					<marquee style="color: red;margin-left: 15px;margin-top: 5px;margin-bottom: -10px;" scrollamount="3" title="{{item.qrcodeDesc}}">{{item.qrcodeDesc}}</marquee>
				</label>
			</li>
			{#
			});
			#}
	</script>
	<script type="text/html" id="bankpaysTemplate" style="display: none;">
			{#
			_.map(list, function(item, index){
			#}
			<li>
				<label >
					<input type="radio" class="pwradio" name="usserradio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payaction="bankpay" index="{{index}}" />
					{#
							if(!item.icon){
								print('<span title="' + item.payName + '" class="bankicon ' + item.iconCss + ' pwicon">' + item.payName + '</span>');
							} else {
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'' + item.icon + '\');">' + item.payName + '</span>');
							}
					#}
					<span class="pwtips" >最小充值金额<font class="hong">{{item.min}}</font>元</span>
										<marquee style="color: red;margin-left: 15px;margin-top: 5px;margin-bottom: -10px;" scrollamount="3" title="{{item.bankDesc}}">{{item.bankDesc}}</marquee>
				</label>
			</li>
			{#
			});
			#}
	</script>
	<script type="text/html" id="onlinepaysTemplate" style="display: none;">
			{#
			_.map(list, function(item, index){
			#}
			<li>
				<label >
					<input type="radio" class="pwradio" name="usserradio" value="{{item.id}}" iconCss="{{item.iconCss}}" min="{{item.min}}" max="{{item.max}}" payaction="onlinepay" paytype="{{item.payType}}" index="{{index}}" />
					{#
							if(item.icon){
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'' + item.icon + '\');">' + item.payName + '</span>');
							} else if(item.payType == "3"){ // 微信
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/weixin.jpg\');">' + item.payName + '</span>');
							} else if(item.payType == "4"){ // 支付宝
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/zhifubao.jpg\');">' + item.payName + '</span>');
							} else if(item.payType == "5"){ // qq钱包
								print('<span title="' + item.payName + '" class="bankicon pwicon" style="background-size: 4.5rem 1.4rem; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/qqpay.png\');">' + item.payName + '</span>');
							} else {
								print('<span title="' + item.payName + '" class="bankicon ' + item.iconCss + ' pwicon">' + item.payName + '</span>');
							}
					#}
					<span class="pwtips" >最小充值金额<font class="hong">{{item.min}}</font>元</span>
					<marquee style="color: red;margin-left: 15px;margin-top: 5px;margin-bottom: -10px;" scrollamount="3" title="{{item.payDesc}}">{{item.payDesc}}</marquee>
				</label>
			</li>
			{#
			});
			#}
	</script>
	<script type="text/html" id="fastpayInputInfoTemplate" style="display: none;">
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 请输入充值金额 <a href="javascript: void(0);" class="zuidichongz">充值最低金额{{min}}元</a>
		</div>
		<div class="chongzhiinptu">
			<input id="money" type="text" value="">
		</div>
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> <span>{{payName}}</span>账号 <span class="fastpaytips">请填写正确的{{payName}}账号，否则无法到帐</span>
		</div>
		<div class="chongzhiinptu">
			<input id="bankCards" type="text" value="">
		</div>
	</script>

	<script type="text/html" id="bankpayInputInfoTemplate" style="display: none;">
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 请输入充值金额 <a href="javascript: void(0);" class="zuidichongz">充值最低金额{{min}}元</a>
		</div>
		<div class="chongzhiinptu">
			<input id="money" type="text" value="">
		</div>
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 存款人姓名</a>
		</div>
		<div class="chongzhiinptu">
			<input id="depositor" type="text" value="">
		</div>
	</script>

	<script type="text/html" id="onlinepayInputInfoTemplate" style="display: none;">
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 请输入充值金额 <a href="javascript: void(0);" class="zuidichongz">充值最低金额{{min}}元</a>
		</div>
		<div class="chongzhiinptu">
			<input id="money" type="text" value="">
		</div>
	</script>


<script type="text/html" id="fastpaySuccessTemplate" style="display: none;">
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 请尽快完成充值</a>
		</div>

		<ul class="popup_template_ul">
			<li>
				<span class="ititle">充值方式： </span><span>{{payName}}</span>
			</li>
			<li>
				<span class="ititle">收款姓名： </span><span>{{payUserName}}</span>
			</li>
			<li>
				<span class="ititle">收款账号： </span><span>{{payAccount}}</span>
			</li>
			<li>
				<span class="ititle">订单号： </span><span>{{orderNo}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{money}}</span>
			</li>
			<li class="qrcode">
				<span class="ititle">二维码： </span><span>{# if(qrCodeImg.indexOf(".jpg") < 0 && qrCodeImg.indexOf(".jpeg") < 0 && qrCodeImg.indexOf(".png") < 0 && qrCodeImg.indexOf(".gif") < 0 && qrCodeImg.indexOf(".bmp") < 0){print(qrCodeImg);}else{print('<img src="' + qrCodeImg + '" alt="' + qrCodeImg + '">');} #}</span>
			</li>
			<li>
				<div class="tips">务必将此订单编号填写到附言里，否则无法入款成功。</div>
			</li>
		</ul>
</script>
<script type="text/html" id="bankpaySuccessTemplate" style="display: none;">
		<div class="chongzhitl">
			<em class="iconfont icon-yuanquan1 hong"></em> 请尽快完成充值</a>
		</div>

		<ul class="popup_template_ul">
			<li>
				<span class="ititle">充值银行： </span><span>{{payCom}}</span>
			</li>
			<li>
				<span class="ititle">收款姓名： </span><span>{{creatorName}}</span>
			</li>
			<li>
				<span class="ititle">收款账号： </span><span>{{bankCard}}</span>
			</li>
			<li>
				<span class="ititle">开户网点： </span><span>{{bankAddress}}</span>
			</li>
			<li>
				<span class="ititle">订单号： </span><span>{{orderNo}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{money}}</span>
			</li>
		</ul>
</script>
<script type="text/html" id="toPayTemplate" style="display: none;">
	<form action="{{formAction}}" method="get">
		{#
		_.map(formParams, function(value, key){
		#}
			<input type="hidden" name="{{key}}" value="{{value}}"/>
		{#
		});
		#}
		<ul class="popup_template_ul topay">
			<li>
				<span class="ititle">订单号： </span><span>{{orderId}}</span>
			</li>
			<li>
				<span class="ititle">会员账号： </span><span>{{account}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{amount}}</span>
			</li>
		</ul>
		<button class="nextbtn" type='submit'>确认送出</button>
	</form>
</script>
	<!-- /* quickRechargePage end */ -->

	<script type="text/javascript">
		var baseUrl = "${base }";
		var hostUrl1="${hostUrl1}";
	</script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/js/jquery/3.1.1/jquery.min.js'></script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/light7/js/light7.min.js'></script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/js/!this/light7-reset.js'></script>
	<script type="text/javascript" src="${base }/mobile/script/underscore/underscore-min.js"></script>
	<script type='text/javascript' src='${base }/mobile/anew/resource/js/common.js'></script>

	<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>


	<script type='text/javascript' src='${base }/common/template/onlinepay/logoutpay/js/qRechargeMobile.js'></script>
</body>
</html>