<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="telephone=no" name="format-detection" />
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
<meta name="format-detection" content="telephone=yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" type="text/css" href="${base }/common/template/onlinepay/scanpay/css/dinpay/style.css">
<link rel="stylesheet" type="text/css" href="${base }/common/template/onlinepay/scanpay/css/dinpay/code.css" />
	<script type="text/javascript" src="${base}/mobile/v3/personal/daili/js/clipboard.min.js"></script>
<style type="text/css">
.order_text li {
    display: list-item;
}
header {
    margin-top: 10px;
}
header .logo-title {
    width: initial!important;
}
.order_box {
    padding: 50px 0 0 0;
}
</style>
<title>${scanpayName }扫码支付</title>
</head>
<body>
	<div class="container">
		<div id="top_box">
			<div id="top">
				<header id="header">
					<article>
						<img src="${base }/common/template/onlinepay/scanpay/images/dinpay/logo_new.png"
							class="fl logo" />
						<div class="fl logo-title">${payType }</div>
					</article>
				</header>
			</div>
		</div>
		<article class="order_box">
			<div id="order_text" class="order_text">
				<ul class="order_list">
					<li><span>扫&nbsp;码&nbsp;方&nbsp;式：</span><p id="merchantOrderId">${scanpayName }</p></li>
					<li><span>订&nbsp;单&nbsp;个&nbsp;数：</span><p id="txMoney">${amount } 个</p></li>
					<li><span>商&nbsp;品&nbsp;名&nbsp;称：</span><p id="productName">${currency}</p></li>
					<li><span>充&nbsp;值&nbsp;提&nbsp;示：</span><p id="prompt">${payDesc}</p></li>
				</ul>
			</div>
		</article>
		<div id="main">

			<!--微信扫码-->
	
			<div class="li_choose" style="display: block">
				<div>
					<article class="bank_list" id="scanpay_listwxpayWap">
						<div class="erweima">
							<div class="hrefscanUrl"><span class="scanUrl">www.baidu.com</span> <span class="copyButton" data-clipboard-text="">复制</span></div>

							<div id="divCode" style="width: 200px; height: 200px; margin: 0 auto; overflow: hidden; text-align: center; border: 1px solid #ddd; z-index: 1;">
								<img src="" width="190px" height="190px" alt="二维码加载中。。。" />
							</div>
						</div>

						<div class="button">
							<div class="buttonSubmit" onclick="virtualSubmit()">
								<span >提交</span>
							</div>
						</div>

					</article>
	
					<div class="links">
						<p>
							<a href="https://pay.dinpay.com/wap/faq.jsp" target="_blank">支付有疑问？点击查看常见问题</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <script type="text/javascript" src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
	<script type="text/javascript" src="${base }/mobile/script/jquery.qrcode.js"></script>
	<script type="text/javascript" src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>

	<script type="text/javascript">
		function parameter(name){
			let url = window.location.href
			let parameterArr =  url.substring(url.indexOf('?')+1).split('&')
			for (i=0;i<parameterArr.length;i++){
				if (parameterArr[i].indexOf(name) > -1){
					return parameterArr[i].split('=')[1]
				}
			}
		}

		function virtualSubmit(){
			$('.buttonSubmit span').text('提交中..')
			$.ajax({
				type:'post',
				dataType:'json',
				url:'${base}/virtualpay/virtualSubmit.do',
				data:{
					payId:parameter('payId'),
					money:'${amount }'
				},
				success:function (res) {
					if (res.success){
						$('.buttonSubmit span').text('提交成功')
					}else {
						$('.buttonSubmit span').text(res.msg)
						alert(res.msg)
					}
				}
			})
		}
		if (parameter('virtual') && (parameter('virtual') == 'virtual')){
			if (parameter('payMethod') == 1){
				$.ajax({
					type:'post',
					dataType:'json',
					url: '${base }/virtualpay' + parameter('jumpUrl'),
					data:{
						payId:parameter('payId'),
						money:parameter('amount'),
						payMethod:parameter('payMethod'),
					},
					success:function (res) {
						if (res.success){
							var options = {
								render: 'image',
								ecLevel: 'H',
								minVersion: parseInt(1, 10),
								color: '#333333',
								bgColor: '#ffffff',
								text: res.data.formParams.scanUrl,
								size: parseInt(165, 10),
								radius: parseInt(40, 10) * 0.01,
								quiet: parseInt(2, 10),
								mode: parseInt(0, 10),
							};
							$("#divCode").empty().qrcode(options);
							$('.hrefscanUrl .scanUrl').text(res.data.formParams.scanUrl)
							$('.hrefscanUrl .copyButton').attr('data-clipboard-text',res.data.formParams.scanUrl)

							var clipboard1 = new ClipboardJS('.copyButton');
							clipboard1.on('success', function(e) {
								alert('复制成功')
								return false;
							});
							clipboard1.on('error', function(e) {
								return false;
							});
						}else {
							alert(res.msg)
						}
					}
				})
			}else{
				alert('暂未配置此方式')
			}
		}else {
			$.ajax({
				type : "POST",
				dataType: "json",
				url : "${base }/onlinepay/pay.do",
				data : {
					bankcode: "${bankcode}",
					amount: "${amount}",
					payId: "${payId}",
					account: "${account}",
					verifyCode:'${verifyCode}'
				},
				success : function(result) {
					if(result.success == false){
						alert(result.msg);
					} else if(result.success == true){
						if(result && result.data && result.data.formParams){
							var payReferer = result.data.payReferer;

							var redirectUrl = "";
							var redirectParams = {};
							_.map(result.data.formParams, function(value, key){
								if(key == "redirectUrl"){
									redirectUrl = value;
								} else {
									redirectParams[key] = value;
								}
							});
							$.ajax({
								type : "POST",
								dataType: "json",
								url : "${base}/onlinepay/utils/getWecahtQrcode.do",
								data : {
									iconCss: "${iconCss}",
									redirectUrl: redirectUrl,
									payId: "${payId}",
									redirectParams: JSON.stringify(redirectParams),
									payReferer: payReferer
								},
								success : function(result) {
									if(result.success == false){
										alert(result.msg);
									} else if(result.success == true){
										if("${iconCss}" == "lianlianspc" || "${iconCss}" == "ziyoupay" || "${iconCss}" == "jinyangpay"){
											$("#divCode img").attr("src", result.qrcodeUrl);
										} else {
											var options = {
												render: 'image',
												ecLevel: 'H',
												minVersion: parseInt(1, 10),
												color: '#333333',
												bgColor: '#ffffff',
												text: result.qrcodeUrl,
												size: parseInt(165, 10),
												radius: parseInt(40, 10) * 0.01,
												quiet: parseInt(2, 10),
												mode: parseInt(0, 10),
											};
											$("#divCode").empty().qrcode(options);
										}
									}
								}
							});
						}else{
							alert("系统发生错误");
						}
					}
				}
			});
		}

	</script>
</body>
</html>