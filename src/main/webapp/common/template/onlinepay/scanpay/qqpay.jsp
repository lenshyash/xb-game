<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="zh-cn">
<title>手机QQ扫码支付</title>
<link href="${base }/common/template/onlinepay/scanpay/css/pay.css" rel="stylesheet" />
<!-- <script type="text/javascript" src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
<script type="text/javascript" src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
<script type="text/javascript" src="${base }/mobile/script/jquery.qrcode.js"></script>
<script type="text/javascript" src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>
</head>
<body>
	<div class="body">
		<h1 class="mod-title">
			<span class="ico-wechat"> <img
				src="${base }/common/template/onlinepay/scanpay/images/QQ.png" width="41" />
			</span><span class="text">手机QQ扫码支付</span>
		</h1>
		<div class="mod-ct">
			<div class="order"></div>
			<div class="amount">
				<span>￥</span>${amount }
			</div>
			<div class="qr-image" style="" id="divCode">
				<!-- 
				<img
					src="http://dpos.qqjun.cn/Plus/QrCode?data=https%3a%2f%2fmyun.tenpay.com%2fmqq%2fpay%2fqrcode.html%3f_wv%3d1027%26_bid%3d2183%26t%3d6V5b6ad267f34971d90bc82f0d23013a"
					width="280px" height="270px"
					style="margin-left: 10px; margin-top: 20px" />
				 -->
				<img src="" width="280px" height="270px" style="margin-left: 10px; margin-top: 20px" alt="二维码加载中。。。" />
			</div>
			<!--detail-open 加上这个类是展示订单信息，不加不展示-->
			<div class="detail detail-open" id="orderDetail" style="">
				<dl class="detail-ct" style="display: block;">
					<!-- 
					<dt>交易单号</dt>
					<dd id="billId">RX1706051548888375436</dd>
					 -->
					<dt>创建时间</dt>
					<dd id="createTime">${createDate }</dd>
			</div>
			<div class="tip">
				<span class="dec dec-left"></span> <span class="dec dec-right"></span>
				<div class="ico-scan"></div>
				<div class="tip-text">
					<p>用手机QQ扫一扫</p>
					<p>扫描二维码完成支付</p>
				</div>
			</div>
		</div>
		<div class="foot">
			<div class="inner">
				<p></p>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$.ajax({
			type : "POST",
			dataType: "json",
			url : "${base }/onlinepay/pay.do",
			data : {
				bankcode: "${bankcode}",
				amount: "${amount}",
				payId: "${payId}",
				account: "${account}",
				verifyCode:'${verifyCode}'
			},
			success : function(result) {
				if(result.success == false){
					alert(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var payReferer = result.data.payReferer;

						var redirectUrl = "";
						var redirectParams = {};
						_.map(result.data.formParams, function(value, key){
							if(key == "redirectUrl"){
								redirectUrl = value;
							} else {
								redirectParams[key] = value;
							}
						});
						$.ajax({
							type : "POST",
							dataType: "json",
							url : "${base}/onlinepay/utils/getWecahtQrcode.do",
							data : {
								iconCss: "${iconCss}",
								redirectUrl: redirectUrl,
								payId: "${payId}",
								redirectParams: JSON.stringify(redirectParams),
								payReferer: payReferer
							},
							success : function(result) {
								if(result.success == false){
									alert(result.msg);
								} else if(result.success == true){
									if("${iconCss}" == "lianlianspc" || "${iconCss}" == "ziyoupay" || "${iconCss}" == "jinyangpay"){
										$("#divCode img").attr("src", result.qrcodeUrl);
									} else {
										var options = {
											render: 'image',
											ecLevel: 'H',
											minVersion: parseInt(1, 10),
											color: '#333333',
											bgColor: '#ffffff',
											text: result.qrcodeUrl,
											size: parseInt(165, 10),
											radius: parseInt(40, 10) * 0.01,
											quiet: parseInt(2, 10),
											mode: parseInt(0, 10),
										};
										$("#divCode").empty().qrcode(options);
									}
								}
							}
						});
					}else{
						alert("系统发生错误");
					}
				}
			}
		});
	</script>
</body>
</html>
