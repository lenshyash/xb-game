<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GB2312" />
    <title>虚拟货币支付</title>
    <link href="${base }/common/template/onlinepay/scanpay/css/style.css" rel="stylesheet" />
    <!-- <script type="text/javascript" src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
    <script type="text/javascript" src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
    <script type="text/javascript" src="${base }/mobile/script/jquery.qrcode.js"></script>
    <script type="text/javascript" src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>
    <script type="text/javascript" src="${base}/mobile/v3/personal/daili/js/clipboard.min.js"></script>
</head>
<body>
<div class="sweep">
    <div class="wrap">
        <div class="h100" id="res">
            <div class="m26" style="font-size: 14px;">
                <h1>
                    <div id="msg">订单已生成，请您尽快付款！</div>
                </h1>
                <div class="num">
                    <!-- <span>订单号:201701042118383625948700000商户订单号:3189979717224448</span> -->
                    <br /> <span>商品信息:<font color=#ff6400
                                            style="font-size: 15px;"></font></span> <span class="color1 ml16">请您在提交订单后
							<span class="orange">10分钟</span> 内完成支付，否则订单会自动取消。
						</span>
                    <br />
                    <div >
                        <span>入款提示:<font color=#ff6400
                                         style="font-size: 15px;"></font></span> <span class="color1 ml16">${payDesc}
                    </span>
                    </div>

                </div>
            </div>
        </div>
        <!--订单信息代码结束-->
        <!--扫描代码-->
        <div class="s-con" id="codem">
            <div class="title">
					<span class="blue" style="font-size: 20px;"><span>应付个数：</span><span
                            class="orange">${amount }</span> 个 <span style="color: red;">${currency}</span> <br>
					<span style="font-size: 12px;">此交易委托数字货币（中国）网络技术有限公司代收款</span>
            </div>
            <div class="hrefscanUrl"><span class="scanUrl">www.baidu.com</span> <span class="copyButton" data-clipboard-text="">复制</span></div>
            <div class="scan_zfb scan_zfb_virtual" id="divCode" >

                <img src="" width="190px" height="190px" alt="二维码加载中。。。" />
                <!-- <img src="http://wytj.9vpay.com/MakeQRCode.aspx?data=https%3a%2f%2fqr.alipay.com%2fbax04892mskgvcrlk6by0074" /> -->
            </div>
            <div class="prompt">
                <span>请使用${scanpayName }App进行扫码，并且完成支付</span>
            </div>
            <div class="question">
                <div class="new"></div>
            </div>
            <div class="button">
                <div class="buttonSubmit" onclick="virtualSubmit()">
                    <span >提交</span>
                </div>
            </div>

        </div>
        <!--扫描代码结束-->
        <!--底部代码-->
        <div class="s-foot">数字货币（中国）版权所有 2012-2020</div>

        <!--
        <a key="5834f79cefbfb06b9eb5da35" logo_size="83x30"
            logo_type="realname" href="http://www.anquan.org"><script
                src="//static.anquan.org/static/outer/js/aq_auth.js"></script></a> <a
            id="_pingansec_bottomimagesmall_shiming"
            href="http://si.trustutn.org/info?sn=377161209026042948560&certType=1"><img
            src="http://v.trustutn.org/images/cert/bottom_small_img.png" /></a>
         -->
        <!--底部代码结束-->
    </div>
</div>
<script type="text/javascript">
    function parameter(name){
        let url = window.location.href
        let parameterArr =  url.substring(url.indexOf('?')+1).split('&')

        for (i=0;i<parameterArr.length;i++){
            if (parameterArr[i].indexOf(name) > -1){
                return parameterArr[i].split('=')[1]
            }
        }
    }
    function virtualSubmit(){
        $('.buttonSubmit span').text('提交中..')
        $.ajax({
            type:'post',
            dataType:'json',
            url:'${base}/virtualpay/virtualSubmit.do',
            data:{
                payId:parameter('payId'),
                money:'${amount }'
            },
            success:function (res) {
                if (res.success){
                    $('.buttonSubmit span').text('提交成功')
                }else {
                    $('.buttonSubmit span').text(res.msg)
                    alert(res.msg)
                }
            }
        })
    }
    $(function () {
        if (parameter('virtual') && (parameter('virtual') == 'virtual')){
            if (parameter('payMethod') == 1){
                $.ajax({
                    type:'post',
                    dataType:'json',
                    url: '${base }/virtualpay'+ parameter('jumpUrl'),
                    data:{
                        payId:parameter('payId'),
                        money:parameter('amount'),
                        payMethod:parameter('payMethod'),
                    },
                    success:function (res) {
                        if (res.success){
                            var options = {
                                render: 'image',
                                ecLevel: 'H',
                                minVersion: parseInt(1, 10),
                                color: '#333333',
                                bgColor: '#ffffff',
                                text: res.data.formParams.scanUrl,
                                size: parseInt(165, 10),
                                radius: parseInt(40, 10) * 0.01,
                                quiet: parseInt(2, 10),
                                mode: parseInt(0, 10),
                            };
                            $("#divCode").empty().qrcode(options);
                            $('.hrefscanUrl .scanUrl').text(res.data.formParams.scanUrl)
                            $('.hrefscanUrl .copyButton').attr('data-clipboard-text',res.data.formParams.scanUrl)

                            var clipboard1 = new ClipboardJS('.copyButton');
                            clipboard1.on('success', function(e) {
                                alert('复制成功')
                                return false;
                            });
                            clipboard1.on('error', function(e) {
                                return false;
                            });
                        }else {
                            alert(res.msg)
                        }
                    }
                })
            }else{
                alert('暂未配置此方式')
            }
        }else {
            $.ajax({
                type : "POST",
                dataType: "json",
                url : "${base }/onlinepay/pay.do",
                data : {
                    bankcode: "${bankcode}",
                    amount: "${amount}",
                    payId: "${payId}",
                    account: "${account}",
                    verifyCode:'${verifyCode}'
                },
                success : function(result) {
                    if(result.success == false){
                        alert(result.msg);
                    } else if(result.success == true){
                        if(result && result.data && result.data.formParams){
                            var payReferer = result.data.payReferer;

                            var redirectUrl = "";
                            var redirectParams = {};
                            _.map(result.data.formParams, function(value, key){
                                if(key == "redirectUrl"){
                                    redirectUrl = value;
                                } else {
                                    redirectParams[key] = value;
                                }
                            });
                            $.ajax({
                                type : "POST",
                                dataType: "json",
                                url : "${base}/onlinepay/utils/getWecahtQrcode.do",
                                data : {
                                    iconCss: "${iconCss}",
                                    redirectUrl: redirectUrl,
                                    payId: "${payId}",
                                    redirectParams: JSON.stringify(redirectParams),
                                    payReferer: payReferer
                                },
                                success : function(result) {
                                    if(result.success == false){
                                        alert(result.msg);
                                    } else if(result.success == true){
                                        if("${iconCss}" == "lianlianspc" || "${iconCss}" == "ziyoupay" || "${iconCss}" == "jinyangpay" || "${iconCss}" == "haitian"||"${iconCss}" == "xinyifu"){
                                            $("#divCode img").attr("src", result.qrcodeUrl);
                                        } else if("${iconCss}" == "juhebosspay" ){
                                            document.write(result.qrcodeUrl);
                                        } else {
                                            var options = {
                                                render: 'image',
                                                ecLevel: 'H',
                                                minVersion: parseInt(1, 10),
                                                color: '#333333',
                                                bgColor: '#ffffff',
                                                text: result.qrcodeUrl,
                                                size: parseInt(165, 10),
                                                radius: parseInt(40, 10) * 0.01,
                                                quiet: parseInt(2, 10),
                                                mode: parseInt(0, 10),
                                            };
                                            $("#divCode").empty().qrcode(options);
                                        }
                                    }
                                }
                            });
                        }else{
                            alert("系统发生错误");
                        }
                    }
                }
            });
        }
    })
</script>
</body>
</html>