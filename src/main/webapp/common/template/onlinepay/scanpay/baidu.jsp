<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>欢迎使用百度钱包支付</title>
<link href="${base }/common/template/onlinepay/scanpay/css/css_new.css" rel="stylesheet" />
<!-- <script type="text/javascript" src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
<script type="text/javascript" src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
<script type="text/javascript" src="${base }/mobile/script/jquery.qrcode.js"></script>
<script type="text/javascript" src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>
</head>
<body>
	<div class="divhead">
		<div class="divtitle">
			<img style="height:80px;width:196px;" src="${base }/common/template/onlinepay/scanpay/images/baidu.jpg" />
		</div>
	</div>
	<div class="divbody">
		<div class="divcontentleft">订单提交成功，请您尽快付款！</div>
		<div class="divcontentright">
			应付金额：<font color="red">${amount }</font>
		</div>
		<div class="divcontent2">
			请您在提交订单后<font color="#FF7D00">5分钟</font>内完成支付，否则订单会自动取消
		</div>
		<!-- 
		<div class="divcontent3">
			<div class="divcontent3_left">
				订单号:201701042038738895678430000，<font color="red"></font>
			</div>
		</div>
		 -->
		<div class="divCode" id="divCode">
			<img src="" width="190px" height="190px" alt="二维码加载中。。。" />
			<!-- <img src="http://wytj.9vpay.com/MakeQRCode.aspx?data=weixin%3a%2f%2fwxpay%2fbizpayurl%3fpr%3dgGmwVln" width="190px" height="190px" /> -->
		</div>
		<div class="divdesc">请使用百度钱包“扫一扫”扫描二维码以完成支付</div>
	</div>
	<script type="text/javascript">
		$.ajax({
			type : "POST",
			dataType: "json",
			url : "${base }/onlinepay/pay.do",
			data : {
				bankcode: "${bankcode}",
				amount: "${amount}",
				payId: "${payId}",
				account: "${account}",
				verifyCode:'${verifyCode}'
			},
			success : function(result) {
				if(result.success == false){
					alert(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var payReferer = result.data.payReferer;

						var redirectUrl = "";
						var redirectParams = {};
						_.map(result.data.formParams, function(value, key){
							if(key == "redirectUrl"){
								redirectUrl = value;
							} else {
								redirectParams[key] = value;
							}
						});
						$.ajax({
							type : "POST",
							dataType: "json",
							url : "${base}/onlinepay/utils/getWecahtQrcode.do",
							data : {
								iconCss: "${iconCss}",
								redirectUrl: redirectUrl,
								payId: "${payId}",
								redirectParams: JSON.stringify(redirectParams),
								payReferer: payReferer
							},
							success : function(result) {
								if(result.success == false){
									alert(result.msg);
								} else if(result.success == true){
									if("${iconCss}" == "lianlianspc" || "${iconCss}" == "ziyoupay" || "${iconCss}" == "jinyangpay"){
										$("#divCode img").attr("src", result.qrcodeUrl);
									} else {
										var options = {
											render: 'image',
											ecLevel: 'H',
											minVersion: parseInt(1, 10),
											color: '#333333',
											bgColor: '#ffffff',
											text: result.qrcodeUrl,
											size: parseInt(165, 10),
											radius: parseInt(40, 10) * 0.01,
											quiet: parseInt(2, 10),
											mode: parseInt(0, 10),
										};
										$("#divCode").empty().qrcode(options);
									}
								}
							}
						});
					}else{
						alert("系统发生错误");
					}
				}
			}
		});
	</script>
</body>
</html>