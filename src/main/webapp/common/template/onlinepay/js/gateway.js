var time = 60;
var gateway = {
	init: function() {
		var _this = this;
		var tip='<div class="bankTip">以上数据仅供参考，具体以银行政策为准。</div>';
		var BANKS = {
			"ECITIC":"谷歌浏览器、safari浏览器", 
			"ICBC":"谷歌浏览器"
			};
		
		_this.objHover(".js-quick", ".js-list-bg");
		_this.objHover(".js-tab-jump", ".js-div-bg");
		$(".js-onclick-p1").on("click", function() {
			_this.showCenter(".js-pay-protocol");
		})
		$(".js-onclick-p2").on("click", function() {
			_this.showCenter(".js-card-protocol");
		});
		$(".js-onclick-p3").on("click", function() {
			_this.showCenter(".js-dai-protocol");
		})
		$(".pro_close").on("click", function() {
			$(".bg,.js-card-protocol,.js-pay-protocol,.js-dai-protocol").hide();
		})
		/*$(".js-get-time").on("click", function() {
			if($(".js-get-time").html()=="免费获取"){
				_this.messTime($(this));
			}
		})*/
		
		
		//显示订单和二维码效果
		$(".js-label-check").on("click", function() {
			if($(this).html().replace(/\s/gi, "")=='扫码支付'){
				payAndGetOrCode('zhbPay','qrcode1','scan2d1','dinpayLogo1',158,158);
			}
			if ($(this).next(".js-next-check").css("display") == "block") {
				$(this).next(".js-next-check").hide();
			} else {
				$(".js-next-check").hide();
				$(this).next(".js-next-check").show();
			}
				$(".maskOver").show();
		});
	
		$(".js-add-close,.maskOver").on("click", function() {
			$(".js-next-check").hide();
			$(".maskOver").hide();
		});

		$(".js-input-tips").on("click", function() {
            $(".gate-mainright").find(".js-input-tip").show();
			$(".js-ddquick").find(".js-input-tip").css("left","325px");
			$(".maskOver").show();
		});

		$(".js-input-close,.maskOver").on("click", function() {
			$(".gate-mainright").find(".js-input-tip").hide()
			$(".maskOver").hide();
		});
		if($("#scanPayLabel").length==0) 
		{ 
			$(".order-app").css('margin-top', '19px');
		} 
		// 协议复选框
		$(".rule-chbox").change(function () {
			obj = $(this).parent().parent().next();
			if($(this).attr("checked")){
				obj.css("background-color","#4e9fe5");
			}
			else {
				obj.css("background-color","#bbb");
			}
		})
		
		//页面默认支付方式(第一个显示)
		$(".gate-menu").find("ul li:eq(0)").addClass("selected");
		$(".gate-mainright").find("ul li:eq(0)").css("display", "block");
		
		//若默认支付方式为微信支付或智汇宝支付，则预支付并生成相应二维码
		var tabClass=$(".gate-menu").find("ul li:eq(0)").find("a").attr("class")
		if(tabClass=="icon04"){
			payAndGetOrCode('weixinPay','qrcode3','scan2d3','dinpayLogo3',240,240);
		}else if(tabClass=="icon02"){
			payAndGetOrCode('zhbPay','qrcode','scan2d','dinpayLogo',140,140);
		}else if(tabClass=="icon05"){
			payAndGetOrCode('alipayScan','qrcode4','scan2d4','dinpayLogo4',240,240);
		}
		//若订单金额长度大于9 则缩小字体size
		var money=$(".order-money").text();
		var length=money.replace(".", "").length;
		if(length>9){
			$(".order-money").css( {
				"font-size": "24px",
			});
		}
		//银行列表移上去显示隐藏效果
		$(".js-tab-jump").find("li").off("click").hover(function() {
			$(this).find("a").addClass("selected");
			$(this).find(".next-showli").show();
		}, function() {
			$(this).find("a").removeClass("selected");
			$(".next-showli").hide();
		});
	
		//显示银行列表效果
		if($('.js-tab-jump>li').length>12){
			$(".js-tab-jump>li:gt(10)").hide();
		}
		$(".js-list-more").on("click", function() {
			$(this).hide();
			$(".js-tab-jump").find("li").show();
		})
		if($('.js-pointcard>li').length>12){
			$(".js-pointcard>li:gt(10)").hide();
		}
		$(".js-pointcard-more").on("click", function() {
			$(this).hide();
			$(".js-pointcard").find("li").show();
		})

		// ---------- 快捷支付  begin --------------
		//已移至快捷js文件中
		//----------- 快捷支付  end -------------------
		
		//新手指引
		$(".js-click-showgate").on("click", function() {
			_this.queryNoviceDemoImage();
		})
		
		//新手指引页面事件初始化
		_this.noviceDemoInit();


	//切换支付方式
	$(".js-menu").find("li").on("click", function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		$(".js-gate-main").children("li").hide().eq($(this).index()).show();
		var dataId=$(this).attr("data-id");
		if (dataId == "cardsel") {
			$("#payquestion").show();
			$("#payquestion2,#payquestion3,#payquestion4,#payquestion5").hide();
		} else if (dataId == "selcard") {
			$("#payquestion2").show();
			$("#payquestion,#payquestion3,#payquestion4,#payquestion5").hide();
		}else if (dataId == "weixin") {
			$("#payquestion3").show();
			$("#payquestion,#payquestion2,#payquestion4,#payquestion5").hide();
		} else if (dataId == "zhb") {
			$("#payquestion4").show();
			$("#payquestion,#payquestion2,#payquestion3,#payquestion5").hide();
		}else if (dataId == "alipayScan") {
			$("#payquestion5").show();
			$("#payquestion,#payquestion2,#payquestion3,#payquestion4").hide();
		}else {
			$("#payquestion,#payquestion2,#payquestion3,#payquestion4,#payquestion5").hide();
		}
	});

        //选择其他银行
		$(".js-another").on("click", function() {
			$(".js-changetab2,.js-changetab3").hide();
			$(".js-changetab1").show();
		});
		
		//网银支付模拟radio
		$(".network-radio").find("label").on("click", function() {
			$(this).addClass("checkradio").siblings().removeClass("checkradio");
			var html = $(this).data("decript");
			if(html.length != 0){
				$(".js-netbank").html(html + tip);	
			}
		});


		//切换tab
		$(".js-change-menu").find("span").on("click", function() {
			$(this).addClass("selected").siblings().removeClass("selected");
			$(".js-tabbank").find(".js-hadquick").hide().eq($(this).index()).show();
		});

		selected_bankcode="";
		/***********点卡**************/
		//跳转
		/*
		$(".js-tab-jump").find("li").on("click", function() {
			selected_bankcode = $(this).data("bankcode");
			$("#change_pic").attr("src", cdn_path +"/ipp2/images/"+ selected_bankcode +".png");
			$("#change_pic").data("bankcode",selected_bankcode);
			
			var json_c = $(this).data("channels");
			var $menu = $(".change-menu");
			var $cardradio = $(".network-radio");
			var $cardradio_e = $(".js-radio");
			//快捷支付
			var expresslist = json_c[13];
			if(expresslist != undefined){
				$menu.find(".b-r-4:first").css("display", "block");
				$menu.find(".b-r-4:first").addClass("selected");
				
				$cardradio_e.find("label").each(function(){
					$(this).css("display","none");
					$(this).removeClass("checkradio");
				});
				
				for(var i in expresslist){
					var express_channel = expresslist[i];
					var card_type = express_channel.cardType;
					var channelId = express_channel.id;
					var payChannelId = _this.countpaychannel(express_channel);
					var channelCode = express_channel.channelCode;
					var bankCode = express_channel.bankCode;
					if(card_type=='2'){//信用卡
						$cardradio_e.find("label:eq(1)").css("display","inline-block");
						$cardradio_e.find("label:eq(1)").data("channelId",channelId);
						$cardradio_e.find("label:eq(1)").data("payChannelId",payChannelId);
						$cardradio_e.find("label:eq(1)").data("channelCode",channelCode);
						$cardradio_e.find("label:eq(1)").data("bankCode",bankCode);
					}else{ //借记卡
						$cardradio_e.find("label:eq(0)").css("display","inline-block");
						$cardradio_e.find("label:eq(0)").data("channelId",channelId);
						$cardradio_e.find("label:eq(0)").data("payChannelId",payChannelId);
						$cardradio_e.find("label:eq(0)").data("channelCode",channelCode);
						$cardradio_e.find("label:eq(0)").data("bankCode",bankCode);
					}
				}
				$cardradio_e.find("label").each(function(){
					var show = $(this).css("display");
					if(show != 'none'){
						$(this).addClass("checkradio");
						expressRadioChange($(this));
						return false;
					}
				});
				
				$(".js-ddbank").find(".js-ddquick").hide().eq(0).show();
				var express = new Express();
				express.fieldInit();//初始化快捷支付页签内的内容
			}else {
				$menu.find(".b-r-4:first").css("display", "none");
				$menu.find(".b-r-4:first").removeClass("selected");
			}
			
			//B2C支付
			var b2clist = json_c[10];
			//B2B支付
			var b2blist = json_c[20];
			if(b2clist!=undefined || b2blist!=undefined){
				$menu.find(".b-r-4:last").css("display", "block");
				if(expresslist == undefined){
					$menu.find(".b-r-4:last").addClass("selected");
					$(".js-ddbank").find(".js-ddquick").hide().eq(1).show();
				}else {
					$menu.find(".b-r-4:last").removeClass("selected");
				}
				
				$cardradio.find("label").each(function(){
					$(this).css("display","none");
					$(this).removeClass("checkradio");
				});
				
				if(b2clist!=undefined){
					for(var i in b2clist){
						var b2c_channel = b2clist[i];
						var card_type = b2c_channel.cardType;
						var paychannelid = _this.countpaychannel(b2c_channel);
						if(card_type=='1' || card_type=='0'){//借记卡
							$cardradio.find("label:eq(0)").css("display","inline-block");
							$cardradio.find("label:eq(0)").data("paychannelid",paychannelid);
							$cardradio.find("label:eq(0)").data("decript",b2c_channel.descript);
							$cardradio.find("label:eq(0)").data("verifyflag",b2c_channel.mobileVerify);
						}
						if(card_type=='2' || card_type=='0'){//信用卡
							$cardradio.find("label:eq(1)").css("display","inline-block");
							$cardradio.find("label:eq(1)").data("paychannelid",paychannelid);
							$cardradio.find("label:eq(1)").data("decript",b2c_channel.descript);
							$cardradio.find("label:eq(1)").data("verifyflag",b2c_channel.mobileVerify);
						}
					}
				}
				if(b2blist!=undefined){
					//企业网银radio显示
					$cardradio.find("label:eq(2)").css("display","inline-block");
					var b2b_channel = b2blist[0];
					var paychannelid = _this.countpaychannel(b2b_channel);
					$cardradio.find("label:eq(2)").data("paychannelid",paychannelid);
					$cardradio.find("label:eq(2)").data("decript",b2b_channel.descript);
					
				}
				
				$cardradio.find("label").each(function(){
					var show = $(this).css("display");
					if(show != 'none'){
						$(this).addClass("checkradio");
						var innerHtml = $(this).data("decript");
						if(innerHtml.length != 0){
							$(".js-netbank").html(innerHtml + tip);	
						}
						return false;
					}
				});
				
				//不支持的浏览器
				var getunsupport = BANKS[selected_bankcode];
				if(getunsupport!=undefined && getunsupport!=''){
					var unsupportbrowswer = '<p>浏览器兼容性提示：此银行不支持 <span>'+getunsupport+' </span>进行网银支付</p>';
					$("#unsupportbrowser").addClass("network-tips").html(unsupportbrowswer);
				}else{
					$("#unsupportbrowser").removeClass("network-tips").html('');
				}
				
				//B2C,B2B清空并隐藏新手错误信息提示
				$('#noviceDemoInfo').html('').hide(); 
			}else {
				$menu.find(".b-r-4:last").removeClass("selected");
				$menu.find(".b-r-4:last").css("display", "none");
			}
			
			
			$(".js-changetab1").hide();
			$(".js-changetab3").show();
			
			
		});
		*/

		/*
		$(".js-pointcard").find("li").on("click", function() {
			var bankcode = $(this).data("bankcode");
			var channelids = $(this).data("channelids");
			$("#dcard_payChannelId").val(channelids);
			var bankcode_src = cdn_path +"/ipp2/images/"+bankcode+".png";
			$("#selecteddcard").attr("src", bankcode_src);
			$(".js-pcard1").hide();
			$(".js-pcard2").show();
			
			var supportMoney = $(this).data("supportmoney");
			if(supportMoney != undefined){
				var array = supportMoney.split("|");
				var pinjie = '<option value="">请选择</option>';
				for(var i in array){
					pinjie += '<option value="' +array[i]+ '">' +array[i]+ '元</option>';
				}
				$("#selectsupportmoney").html(pinjie);
			}
			_this.cleardcardshow();
			
		})
		
		$(".js-point-another").on("click", function() {
			$(".js-pcard2").hide();
			$("#dcarddeclare").removeClass("ui-form-explain").removeClass("ui-form-explain-correct");
			$(".js-pcard1").show();
		})
		*/

		$(".netbank-div").find(".had-agree").on("click", function(){
			setCookie("hisBankCode", selected_bankcode, 24);
			
			var checkRadio = $(".network-radio").find(".checkradio")
			var paychannelids = checkRadio.data("paychannelid");
			var verifyflag = checkRadio.data("verifyflag");
			var array = paychannelids.split("|");
			var redoFlag = $("#redoFlag").val();
			if("1" == redoFlag){
				$("#bank_pay").attr("target", "_self");
			}else{
				if('20'==array[0] || '1'!=verifyflag){
					openDialog();
				}
				$("#bank_pay").attr("target", "_blank");
			}
			
			if('10'==array[0] && '1'==verifyflag){
				MobileVerify.fieldInit();
				_this.showWinCenter(".bg_b2c","#js-popwintwo");
			}else if('20'==array[0]){
				$("#payChannelId_b2b").val(paychannelids);
				$("#bank_pay_b2b").attr("target", "dinpay");
			    $("#bank_pay_b2b").submit();
			}else {
				$("#payChannelId_b2c").val(paychannelids);
			    $("#bank_pay").submit();
			}
			
		})
		//“更多银行”左侧配上向下的“多选择”箭头
		$(".more").on("mouseover", function() {
			$(this).find("#imgUnSelected").css('display', 'none');
			$(this).find("#imgSelected").css('display', 'inline');

		}).on("mouseout", function() {
			$(this).find("#imgSelected").css('display', 'none');
			$(this).find("#imgUnSelected").css('display', 'inline');

		});

	/*********智汇宝钱包***********/
	$(".js-balance").on("click", function() {
		clearForm();
		var id=this.getAttribute("id");
		if(id=='cardPay'){
			$("#chkCodeDiv").hide();
			$("#sendSmsCode").show();
		}else if($("#zhbMobile").val()==''){
			$("#sendSmsCode").hide();
			$("#chkCodeDiv").show();
		}
		var dataId = $(this).attr("data-id");
		$(this).addClass("sel-border").siblings().removeClass("sel-border");
		if (dataId == "card") {
			$(".js-card-input").show();
			$(".js-dd-select").addClass("hadshow");
			//	$(".js-dd-pack").hide();
			} else {
				$(this).siblings().removeClass("padding-add");
				$(".js-dd-list,.js-dd-change,.js-dd-option").hide();
				$(".js-dd-select").removeClass("hadshow");
				$(".js-dd-pays").show();
				$(".js-card-input").hide();
				$(".js-dd-pack").show();
				$(".js-bottom").show();
			}
		});
		function clearForm() {
			$("#_ocx_pay_password").css("display", "none");
			$("#chkNumber,#zhbPayPassword,#chkCode").val('').css( {
				"border" : "1px solid #ccc"
			});
			$("#zhbPayPwdMsg,#chkNumberMsg,#chkCodeErrorMsg").html('').removeClass("ui-form-explain-correct").removeClass("ui-form-explain");
			time=0;
		}

		$(".js-checkbox").on("click", function() {
		/*	if ($(this).attr("checked") == "checked") {
				$(".js-downsafe").show().html("请点此安装控件");
			} else {
				$(".js-downsafe").hide();
			}*/
		})

	/*	$(".js-dd-list li:gt(10)").hide();
		$(".js-dd-more").on("click", function() {
			$(this).hide();
			$(".js-dd-list").find("li").show();
		})
		$(".js-dd-list").find("li").off("click").hover(function() {
			$(this).find("a").addClass("selected");
			$(this).find(".next-showli").show();
		}, function() {
			$(this).find("a").removeClass("selected");
			$(".next-showli").hide();
		});
	*/
		//选择其他银行
		$(".js-dd-another").on("click", function() {
			$(this).parents(".js-balance").addClass("padding-add");
			$(".js-dd-list").show();

		});
		//点击银行跳转
		$(".js-dd-list").find("li").on("click", function() {
			$(".js-dd-pays").hide();
			$(".js-dd-list").hide();
			$(".js-dd-change").show();
			$(".js-bottom").hide();
		});
		//切换
		$(".js-dd-menu").find("span").on("click", function() {
			$(this).addClass("selected").siblings().removeClass("selected");
			$(".js-ddbank").find(".js-ddquick").hide().eq($(this).index()).show();
			if($(this).find('a').hasClass('net')){
				$('#noviceDemoInfo').html('').hide(); //B2C,B2B清空并隐藏新手错误信息提示
			}
		});
		$(".js-dd-other").on("click", function() {
			$(".js-dd-change").hide();
			$(".js-dd-list,.js-dd-pays").show();
			$(".js-bottom").show();
		})


		/**********************************/
		/*************银联支付**************/
		/**********************************/
		//显示银行列表效果
		$(".js-pu-jump>li:gt(10)").hide();
		$(".js-pu-more").on("click", function() {
			$(this).hide();
			$(".js-pu-jump").find("li").show();
		});

		$(".js-pu-jump").find("li").off("click").hover(function() {
			$(this).find("a").addClass("selected");
			$(this).find(".next-showli").show();
		}, function() {
			$(this).find("a").removeClass("selected");
			$(".next-showli").hide();
		});
		$(".js-pu-banklist").find("li").on("click", function() {
			$(".js-pu-banklist").hide();
			$(".js-putab1").show();
		});
		$(".js-pu-other").on("click", function() {
			$(".js-pu-banklist").show();
			$(".js-putab1,.js-union,.js-qu-ul").hide();
		});
		$(".js-qu-aa").on("click", function() {
			$(".js-changetab3,.js-changetab2,.js-pu-banklist,.js-putab1").hide();
			$(".js-changetab1").show();
		});
		$("#first").on("click", function() {
			$(".js-union").show();
			$(".js-putab1,.js-pu-banklist").hide();
		});
		
		//错误提示弹出框隐藏
		_this.closeErrorPop();
		
	},

	//鼠标移入移出动画效果
	objHover: function(hoverobj, hoverMask) {
		if ((navigator.userAgent.indexOf('MSIE') >= 0) && (navigator.userAgent.indexOf('Opera') < 0)) {
			$(hoverobj).hover(function() {
				$(hoverMask).show().css({
					"filter": "alpha(opacity=70)"
				});
			}, function() {
				$(hoverMask).hide().css({
					"filter": "alpha(opacity=0)"
				});
			});
		} else {
			$(hoverobj).hover(function() {
				$(hoverMask).show().animate({
					"opacity": 0.7
				}, 200);
			}, function() {
				$(hoverMask).hide().animate({
					"opacity": 0
				}, 200);
			})
		}
	},
	//弹窗显示
	showCenter: function(ObjWin) {
		if (ObjWin != null && typeof(ObjWin) != "undefined") {
			$(".bg").height($(document).height()).show();
			$(ObjWin).show();
		}
	},
	// 设置弹窗显示在屏幕中间
	showWinCenter:function(objBg,objWin){
		if(objBg != null && typeof(objBg) != "undefined"){
			$(objBg).show();
		}
		if(objWin != null && typeof(objWin) != "undefined"){
			var doc = $(window);
			var win = $(objWin);
			var height = (doc.height() - win.height()) / 2;
			var width = (doc.width() - win.width()) / 2;
			win.css({top: height+'px',left: width+'px'});
			win.show();
		}
	},
	//切换下一个
	toggleBanner: function(fx, page) {
		currentIndex = $('.js-showgate li:visible').index();

		if (fx == 'left' && currentIndex <= 0) {
			$(".left").hide();
		} else if (fx == 'right' && currentIndex >= (page - 1)) {

		} else if (fx == 'left') {
			if (currentIndex - 1 <= 0) {
				$(".left").hide();
			} else {
				$(".right,.left").show();
			}
			$('.js-showgate li').eq(currentIndex).hide().end().eq(currentIndex - 1).show();
		} else if (fx == 'right') {
			if (currentIndex + 2 == page) {
				$(".right").hide();
			} else {
				$(".right,.left").show();
			}
			$('.js-showgate li').eq(currentIndex).hide().end().eq(currentIndex + 1).show();
		}
	},
	//短信倒计时
	messTime: function(timeobj) {
		var t = setInterval(function() {
			if (time >= 1) {
				time--;
				timeobj.html(time + "秒后重新获取");
			} else {
				time == 60;
				timeobj.html("免费获取");
				clearInterval(t);
			}
		}, 1000);
	},
	countpaychannel: function(channel){
		var classid = channel.payclassId;
		var gatewayid = channel.gatewayId;
		var id = channel.id;
		var paychannelid = classid+"|"+gatewayid+"|"+id;
		return paychannelid;
	},
	cleardcardshow: function(){
		$("#selectsupportmoney:first").attr("selected","selected");
		$("#dcarddeclare").html("请您选择和订单金额相对应的面值");
		$("#dcardNo").css({"border":"1px solid #cccccc"}).val("");
		$("#dcardPass").css({"border":"1px solid #cccccc"}).val("");
		$("#dmemberID").css({"border":"1px solid #cccccc"}).val("");
		$("#dcardnotip").removeClass("ui-form-explain").removeClass("ui-form-explain-correct").html("");
		$("#cardpasstip").removeClass("ui-form-explain").removeClass("ui-form-explain-correct").html("");
		$("#memberIDtip").removeClass("ui-form-explain").removeClass("ui-form-explain-correct").html("");
	},
	//新手操作演示页面响应事件初始化
	noviceDemoInit: function(){
		var _this = this;
		var page = $('.js-showgate li').length;
		$('.left').click(function() {
			_this.toggleBanner('left', page);
		});
		$('.right').click(function() {
			_this.toggleBanner('right', page);
		})
		$(".js-gate-close").on("click", function() {
			$(".js-showgate,.bg").hide();
		})
	},
	//查询新手操作演示图片
	queryNoviceDemoImage: function(){
		var _this = this;
		var bankCode = $("#change_pic").data("bankcode");
		var path = "ipp2/novice";
		$.ajax({
			type: "post",
			url: "ipp2/query",
			dataType : "text",
			data: {"bankCode":bankCode,"path":path,"queryType":"queryImage"},
			success:function(result){
				var data = eval("("+result+")");
				if(data.status =="0"){
					var imgList = data.imgList;
					var noviceDemo = $('.js-showgate ul');
					var html = '';
					for(var i=0;i<imgList.length;i++) {
						var img = imgList[i];
						html +="<li><s class='left'></s><em class='close js-gate-close'></em>"+
							   "<p><img src='"+ path +"/"+ bankCode +"/"+ img +"'></p><s class='right'></s></li>"
					}
					noviceDemo.html(html);
					$('.js-showgate li').eq(0).show().siblings().hide();
					_this.showCenter(".js-showgate");
					_this.noviceDemoInit();
				}else{
					$('#noviceDemoInfo').html('暂无此银行的操作演示').show();
				}
			},
			error:function(response){
				alert(response.responseText);
			}
		});
	},
	/** 关闭错误提示弹出框 */
	 closeErrorPop: function() {
		$("#errorPopbox").hide();
		$(".bg").hide();
	},
	/** 显示错误提示弹出框 */
	 showErrorPop: function(errorCode,errorMsg) {
		var _this = this;
		var conTxt = $("#errorPopbox").find(".conTxt");
		var code = conTxt.find("p:eq(0)");
		var msg = conTxt.find("p:eq(1)");
		code.html("错误代码："+ errorCode);
		msg.html("错误信息："+ errorMsg);
		
		_this.showCenter("#errorPopbox");
	},
	/** 自动按比例显示图片，按比例压缩图片显示*/
	autoResizeImage: function(maxWidth, maxHeight, objImg) {
		var img = new Image();
		img.src = objImg.src;
		var hRatio;
		var wRatio;
		var Ratio = 1;
		var w = img.width;
		var h = img.height;
		wRatio = maxWidth / w;
		hRatio = maxHeight / h;
		if (maxWidth == 0 && maxHeight == 0) {
			Ratio = 1;
		} else if (maxWidth == 0) { //
			if (hRatio < 1)
				Ratio = hRatio;
		} else if (maxHeight == 0) {
			if (wRatio < 1)
				Ratio = wRatio;
		} else if (wRatio < 1 || hRatio < 1) {
			Ratio = (wRatio <= hRatio ? wRatio : hRatio);
		}
		if (Ratio < 1) {
			w = w * Ratio;
			h = h * Ratio;
		}
		objImg.height = h;
		objImg.width = w;
	}

}
$(function() {
	
	gateway.init();
	
	path_remote = $("#remoteUrlPath").val();
	cdn_path = $("#cdnPath").val();
});


//设置Cookie
function setCookie(name,value,expireHours){
	var cookieString=name+"="+escape(value);
	if(expireHours>0){
		var exp=new Date();
		exp.setTime(exp.getTime()+expireHours*60*60*1000);
		cookieString=cookieString+"; expires="+exp.toGMTString();
	} 
	document.cookie=cookieString;
}

var intime;
function openDialog(){
	//websocket连接推送网关
	openWebsocket();
    var winHeight=0;
    if (document.body && document.body.scrollHeight){
        winHeight = document.body.scrollHeight;
    }
    $("#fade").css("height",winHeight+"px");
    $("#fade").show();
    $("#light").show();
}

function openWebsocket(){
	var url=$("#pushUrl").val();
	var policyUrl=$("#policyUrl").val();
	if(!url){
		return;
	}
	var ws;
	if(policyUrl){
		try {
	        WebSocket.loadFlashPolicyFile(policyUrl);
	    } catch (e) {
	    }
	}
	try{
		ws = new WebSocket(url);
	}catch(e){
		console.log(e);
		return;
	}
	ws.onopen = function() {
		console.log('connect pushGateway success.')
	}
	ws.onmessage = function(e) {
		var data = eval("("+e.data+")");
		if(data.result == "0") {
			checkSuccess();
		}
	}
	ws.onclose = function(e) {
		if(e.code==1000){//正常关闭（超时、业务已完成）
			//do nothing
			console.log('connect close',e.code+":"+e.reason)
		}else if(e.code==1003){//服务器拒绝连接
			//do nothing
			console.log('connect close',e.code+":"+e.reason)
		}else{//重新连接
			setTimeout("openWebsocket()", 10000);
		}
	}
}
	


function postForm(data){
	var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = path_remote+'/ipp2/checksuccess.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","orderId");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.orderId);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txMoney");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txMoney);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txDate");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txDate);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","gateWayID");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.gateWayID);
			    turnForm.appendChild(newElement);
			    //新增
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","order_version");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.order_version);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","newmd5info");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.newmd5info);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","DinpayOrderId");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.DinpayOrderId);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_id");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_id);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_orderid");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_orderid);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_oamount");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_oamount);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_ocurrency");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_ocurrency);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_language");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_language);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_name");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_name);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_addr");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_addr);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_postcode");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_postcode);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_tel");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_tel);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_eml");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_eml);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","r_name");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.r_name);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","r_addr");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.r_addr);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","r_postcode");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.r_postcode);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","r_eml");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.r_eml);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","r_tel");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.r_tel);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_ocomment");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_ocomment);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","m_status");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.m_status);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","modate");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.modate);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","OrderMessage");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.OrderMessage);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","Digest");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.Digest);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","merchant_code");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.merchant_code);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","notify_type");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.notify_type);
			    turnForm.appendChild(newElement);
		
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","notify_id");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.notify_id);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","interface_version");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.interface_version);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","sign_type");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.sign_type);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","order_no");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.order_no);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","order_amount");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.order_amount);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","order_time");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.order_time);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","trade_no");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.trade_no);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","trade_time");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.trade_time);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","trade_status");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.trade_status);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","extra_return_param");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.extra_return_param);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","bank_seq_no");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.bank_seq_no);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","page_sign");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.page_sign);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","page_return_url");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.page_return_url);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","s_postcode");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.s_postcode);
			    turnForm.appendChild(newElement);
			    turnForm.submit();
}

function closeDialog1(){
    $("#fade").hide();
    $("#light_fail").hide();
}

function closeDialog(){
    $("#fade").hide();
    $("#light").hide();
}

function showPro(url){
	var wid = $(window).width();
	var hei = $(window).height();
	var lef = (wid - 600)/2+"px";
	var heii = (hei - 400)/2+"px";
	$(".terms").css({
		"left":lef,
		"top":heii
	})
	$(".bg").css({
	"opacity":0.7,
	"width":wid,
	"height":hei+200
	});
	$(".bg").css("display","block");
	document.getElementById('proess').style.display = "";
	$("#fade").hide();
    $("#light").hide();
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=0";
	$.ajax({
		type: "post",
		url: url,
		dataType : "text",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				postForm(data);
			} else {
				$(".bg").css("display","none");
				document.getElementById('proess').style.display = "none";		
				$("#fade").show();
                $("#light_fail").show();
			}
		}
	});
}

function checkPay(url){
	var wid = $(window).width();
	var hei = $(window).height();
	var lef = (wid - 600)/2+"px";
	var heii = (hei - 400)/2+"px";
	$(".terms").css({
		"left":lef,
		"top":heii
	})
	$(".bg").css({
	"opacity":0.7,
	"width":wid,
	"height":hei+200
	});
	$(".bg").css("display","block");
	document.getElementById('proess').style.display = "";
	clearInterval(intime);
	$("#fade").hide();
    $("#light").hide();
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=0";
	$.ajax({
		type: "post",
		url: url,
		dataType : "text",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				postForm(data);
			} else {
				$(".bg").css("display","none");
				document.getElementById('proess').style.display = "none";		
			}
		}
	});
}


function setQrCodeErrorPage(payType,qrcode,scan2d,dinpayLogo,width,height) {
	var html = '';
	var picUrl = cdn_path +'/ipp2/images/qrcodeError.jpg';
	var clickHtml='payAndGetOrCode(\''+payType+'\',\''+qrcode+'\',\''+scan2d+'\',\''+dinpayLogo+'\',\''+width+'\',\''+height+'\');';
	html = '<img id="qrcodeErrorPic" src="'+picUrl+'" onclick="'+clickHtml+'" class="zhb-qrcode-error" style="width: '+width+'px; height:'+height+'px"/>';
	$("#" + qrcode).children().remove();
	$("#" + dinpayLogo).css("display", "none");
	$("#" + qrcode).append(html);
}

function setLoading(qrcode,width,height) {
	// var html = '';
	// var picUrl = cdn_path +'/ipp2/images/wxloading.gif';
	// html = '<img id="qrcodeErrorPic" src="'+picUrl+'" onclick="" class="zhb-qrcode-error" style="width: '+width+'px; height:'+height+'px"/>';
	// $("#" + qrcode).children().remove();

	// $("#" + qrcode).append(html);
}

function setErrorPage(msg){
	var html="<div class='o_error' ><img src='" + cdn_path +"/ipp2/images/error.png' />";
		html=html + "<p>支付失败</p><div>" + msg + "</div>";
		$(".wx-img").html(html);
}	

function payAndGetOrCode(payType,qrcode,scan2d,dinpayLogo,width,height) {
	/*
	setLoading(qrcode,width,height);
	var url = $("#remoteUrlPath").val() +"/ipp2/GetQrCode";
	var payChannelId='';
	if(payType == 'weixinPay'||payType == 'alipayScan'){
		var txMoney = $("#amount").val() ;
		var error_msg = "";
		if(payType == 'weixinPay'){
			payChannelId = $("#wxPayChannelId").val();
			error_msg = "微信支付单笔订单金额不能超过<span>50000</span>元，请选择其他支付方式。"
		}else{
			payChannelId = $("#alipayScanChannelId").val();
			error_msg = "支付宝收款单笔订单金额不能超过<span>50000</span>元，请选择其他支付方式。"
		}
		if(txMoney > 50000){
			this.setErrorPage(error_msg);
			return ;
		}
	}else if(payType=='zhbPay'){
		payChannelId = $("#payChannelId").val();
	}else{
		setQrCodeErrorPage(payType,qrcode,scan2d,dinpayLogo, width, height);
		return;
	}
	var dataStr = "orderKey=" + $("#orderKey").val() + "&orderInfo="
	+ $("#orderInfo").val() + "&payType=" +payType + "&payChannelId="
	+ payChannelId;

	$.ajax( {
		type : "post",
		url : url,
		dataType : "text",
		data : dataStr,
	    timeout: 30000, //30s
		error : function(result){
		setQrCodeErrorPage(payType,qrcode,scan2d,dinpayLogo,width,height);
		return ;
	},
	success : function(result) { //请求成功时的返回函数
		var data = eval("(" + result + ")");
		var status = data.status;
		var errorCode=data.errorCode ;
		var url =  $("#remoteUrlPath").val()  + data.url;
		//如果不成功，显示错误信息或跳转到错误页面
		if(data.status != '0'){
			if(Common.checkToFailPage(data)){
				return;
			}
			setQrCodeErrorPage(payType,qrcode,scan2d,dinpayLogo, width, height);
			return ;
		}
		var qrCode = data.qrCode;
		if (qrCode != "") {

			$("#" + qrcode).html("");

			var oQRCode = new QRCode(qrcode, {
				width : width,
				height : height
			});
			oQRCode.clear();
			oQRCode.makeCode(qrCode);
			if(dinpayLogo!='dinpayLogo3' && dinpayLogo!='dinpayLogo4'){
				$("#" + dinpayLogo).css("display", "block");
			}
			openWebsocket();

		}
	}
	});
	*/
}


function checkSuccess(){
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=1";
	var url=path_remote+"/ipp2/PaymentCom";
	$.ajax({
		type: "post",
		url: url,
		dataType : "text",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				postForm(data);
			} 
			if (data.result == "2") {
				var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = path_remote + '/ipp2/payfail.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","errorCode");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.errorCode);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","errorMsg");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.errorMsg);
			    turnForm.appendChild(newElement);
			    turnForm.submit();
			}
		}
	});
}


