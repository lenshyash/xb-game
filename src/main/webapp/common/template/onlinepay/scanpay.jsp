<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<title>微信支付</title>
<!-- <script type="text/javascript" src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
<script type="text/javascript" src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
<script type="text/javascript" src="${base }/mobile/script/jquery.qrcode.js"></script>
<script type="text/javascript" src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>
<style>
* {
	margin: 0px;
	padding: 0px;
}

body {
	background: #fff;
	font-size: 13px;
	color: #333;
	font-family: Verdana, "宋体";
}

.wp {
	width: 1220px;
	margin: 0 auto;
}

.top {
	height: 100px;
	width: 100%;
	background-image: -webkit-linear-gradient(to right, #1360ad, #39b0f2);
	background-image: linear-gradient(to right, #1360ad, #39b0f2);
}

.qrbox {
	margin-top: 30px;
	border: 1px solid #c6c6c6;
	box-shadow: 0 0 2px #dcdcdc;
}

.qr {
	width: 294px;
	height: 333px;
	background: url(${base }/common/template/onlinepay/images/qrbox.gif);
	position: relative;
}

.gay {
	position: absolute;
	width: 230px;
	height: 230px;
	line-height: 230px;
	background: #fff;
	top: 40px;
	left: 32px;
}

.gay img {
	margin: 2px;
	width: 228px;
	height: 228px;
}

.footer {
	background: #cfcfcf;
	margin-top: 30px;
	line-height: 60px;
	height: 60px;
	text-align: center;
	width: 100%;
	position: fixed;
	bottom: 0;
	left: 0;
}
</style>
</head>
<body>
	<!--top-->
	<div class="top">
		<div class=wp>
			<div style="padding: 15px 0 0;">
				<img src="${base }/common/template/onlinepay/images/logo.png" />
			</div>
		</div>
	</div>
	<!--qrbox-->
	<div class="wp qrbox">
		<div style="padding: 10px;">
			<table style="width: 900px; margin: 0 auto;">
				<tr>
					<td align=center valign=middle>
						<div class="qr">
							<div class="gay" id="qrcodeDiv">二维码加载中。。。</div>
						</div>
					</td>
					<td width=400 align=center><img height="450"
						src="${base }/common/template/onlinepay/images/phone.jpg" /></td>
				</tr>
			</table>
		</div>
	</div>
	<!--footer-->
	<div class="footer">微信支付 Wechat.com</div>

	<script type="text/javascript">
		$.ajax({
			type : "POST",
			dataType: "json",
			url : "${base }/onlinepay/pay.do",
			data : {
				bankcode: "${bankcode}",
				amount: "${amount}",
				payId: "${payId}",
				account: "${account}"
			},
			success : function(result) {
				if(result.success == false){
					alert(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var payReferer = result.data.payReferer;

						var redirectUrl = "";
						var redirectParams = {};
						_.map(result.data.formParams, function(value, key){
							if(key == "redirectUrl"){
								redirectUrl = value;
							} else {
								redirectParams[key] = value;
							}
						});
						$.ajax({
							type : "POST",
							dataType: "json",
							url : "${base}/onlinepay/utils/getWecahtQrcode.do",
							data : {
								iconCss: "${iconCss}",
								redirectUrl: redirectUrl,
								payId: "${payId}",
								redirectParams: JSON.stringify(redirectParams),
								payReferer: payReferer
							},
							success : function(result) {
								if(result.success == false){
									alert(result.msg);
								} else if(result.success == true){
									var options = {
										render: 'image',
										ecLevel: 'H',
										minVersion: parseInt(1, 10),
										color: '#333333',
										bgColor: '#ffffff',
										text: result.qrcodeUrl,
										size: parseInt(165, 10),
										radius: parseInt(40, 10) * 0.01,
										quiet: parseInt(2, 10),
										mode: parseInt(0, 10),
									};
									$("#qrcodeDiv").empty().qrcode(options);
								}
							}
						});
					}else{
						alert("系统发生错误");
					}
				}
			}
		});
	</script>
</body>
</html>