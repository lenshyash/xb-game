<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>
<!DOCTYPE html>
<html>
<head>
<title>网上支付 方便 | 快捷 | 安全 | 周到</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="pile,piling,piling.js,stack,pages,scrolling,stacking,touch,fullpile,scroll,plugin,jquery" />
<meta name="Resource-type" content="Document" />
<meta http-equiv="X-UA-Compatible" content="IE=8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="stylesheet"
	href="${base }/common/template/onlinepay/css/public.css" />
<link rel="stylesheet"
	href="${base }/common/template/onlinepay/css/gateway.css?v=1.0.3" />
<!-- <script type="text/javascript"
	src="https://cdnpay.dinpay.com/pay/ipp2/js/jquery/jquery-1.8.2.min.js"></script> -->
<script type="text/javascript" 
	src="${base }/common/jquery/jquery-1.12.4.min.js?v=2"></script>
<script type="text/javascript"
	src="${base }/mobile/script/jquery.qrcode.js"></script>
<script type="text/javascript"
	src="${base }/common/template/onlinepay/js/underscore/underscore-min.js"></script>
<script type="text/javascript"
	src="${base }/common/template/onlinepay/js/common.js"></script>
<script type="text/javascript"
	src="${base }/common/template/onlinepay/js/gateway.js"></script>

<script type="text/javascript">
	if ("${success}" == "false") {
		alert("${msg }");
	}
</script>
</head>
<body>
	<input type="hidden" name="payId" id="payId" value="${payId }" />
	<input type="hidden" id="amount" name="amount" value="${amount }" />
	<!-- <input type="hidden" id="payCode" name="payCode" value="${payCode}" /> -->
	<!-- 报错回滚用，稳定后删除 -->
	<div class="code-tips js-tips">
		<div class="js-queen bw">
			<span id="marquee">温馨提示：在购买商品和支付时，请注意核对网址、收款商户、商品名称、支付金额等信息，警惕各种形式的网络欺骗行为！</span>
			<a class="mar-close js-close">x</a>
		</div>
		<div class="gate-head overh">
			<div class="w overh">
				<a class="fl gate-logo">
					<h1>
						<img src="${base }/common/template/onlinepay/images/banks/logo_new.png" 
							onload="gateway.autoResizeImage(0,this.height>56?56:this.height,this)" />
					</h1>
				</a> <span class="fl check-span">收银台</span>
			</div>
		</div>
	</div>
	<!--支付订单-->
	<div class="gate-orders w ">
		<div class="orders-left fl">
			<span class="fl orders-title">${payType }</span>
			<div class="order-center fr">
				<label class="js-label-check">查看详情</label>
				<div class="order-details b-r-2 js-next-check"
					style="display: none;">
					<ul>
						<!-- <li>订&nbsp;&nbsp;单&nbsp;号：<span>201610060947522663</span> <a class="js-add-close close order-close"></a></li> -->
						<li>订单金额：<span>${amount } 元</span></li>
						<li>商品名称：<span>在线支付</span></li>
					</ul>
				</div>
				<s class="trangle-s"></s>
			</div>

		</div>
		<div class="order-right">
			<div class="fr order-app">
				<p>
					<span class="order-money">${amount }</span>元
				</p>
			</div>
		</div>
	</div>
	<!--支付订单 end-->
	<div class="gate-main w">
		<div class="gate-menu">
			<ul class="menu-ul js-menu">
				<c:if test="${fun:length(bank) > 0}">
					<!-- B2C,B2B,快捷 -->
					<li class="selected" data-id="cardsel"><a class="icon01"></a></li>
				</c:if>
				<c:if test="${fun:length(wechat) > 0}">
					<!-- 微信扫码 -->
					<li data-id="selcard"><a class="icon04"></a></li>
				</c:if>
				<c:if test="${fun:length(alipay) > 0}">
					<!-- 支付宝收款 -->
					<li data-id="selcard"><a class="icon05"></a></li>
				</c:if>
				<c:if test="${fun:length(qqpay) > 0}">
					<!-- QQ钱包 -->
					<li data-id="selcard"><a class="icon02"></a></li>
				</c:if>
				<c:if test="${fun:length(jdpay) > 0}">
					<!-- 京东支付 -->
					<li data-id="selcard"><a class="icon03"></a></li>
				</c:if>
				<c:if test="${fun:length(baidu) > 0}">
					<!-- 百度钱包 -->
					<li data-id="selcard"><a class="icon07"></a></li>
				</c:if>
				<c:if test="${fun:length(union) > 0}">
					<!-- 银联扫码 -->
					<li data-id="selcard"><a class="icon08"></a></li>
				</c:if>
				<!-- 智汇宝 -->
				<!-- 1:上部扫码支付 0：右侧网页扫码 -->
				<!--
						<li onclick="payAndGetOrCode('zhbPay','qrcode','scan2d','dinpayLogo',140,140);" data-id="zhb">
							<a class="icon02" ></a>
						</li>
				 -->
				<!-- 点卡-->
				<!-- 
				<li data-id="selcard"><a class="icon03"></a></li>
				 -->
			</ul>
			<div class="clear"></div>
		</div>

		<div class="gate-mainright">
			<ul class="gatemain-ul js-gate-main">
				<c:if test="${fun:length(bank) > 0}">
					<!-- B2C,B2B,快捷 -->
					<li class="first-Li" style="display: block;">
						<!--payClassId只是针对快捷支付添加， 快捷查询签约信息需要根据payClassId值进行判断 --> <!----B2C,B2B,快捷支付---->
						<div class="gate-quickbank js-changetab1" style="display: block;">
							<div class="bank-list">
								<div class="list-bg js-list-bg"
									style="display: none; opacity: 0;"></div>
								<ul class="js-tab-jump" paymethod="bank">
									<!-- 第一个位置显示历史银行，银行图标上有"最近"标记-->
									<!-- 
								<li data-bankcode="UPOP_ORG" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/UPOP_ORG.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								 -->
									<!-- 
								<li data-bankcode="ICBC" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/ICBC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="ABC" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/ABC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="BOC" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/BOC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="CCB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/CCB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="CMB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/CMB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="BOCO" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/BCOM.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="CTTIC" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/ECITIC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="CEB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/CEBB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="PSBS" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/PSBC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="PINGANBANK" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/SPABANK.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="SPDB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/SPDB.png">
								</a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="CMBC" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/CMBC.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="CIB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/CIB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="HXB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/HXB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="BCCB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/BOB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡 信用卡</div>
								</li>
								<li data-bankcode="NBCB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/NBB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="SHB" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/SHB.png"></a>
									<div class="next-showli" style="display: none;">支持： 借记卡</div>
								</li>
								<li data-bankcode="HSBANK" data-channels=''>
									<a> <img src="https://cdnpay.dinpay.com/pay/ipp2/images/HSBANK.png"></a>
									<div class="next-showli" style="display: none;">支持： 企业网银</div>
								</li>
								 -->
									<c:forEach var="item" items="${bank }" varStatus="status">
										<li data-bankcode="${item.bankCode }" data-channels=''>
										<a>
											<c:if test="${item.iconUrl == '' }">
												${item.bankName }
											</c:if>
											<img src="${base }${item.iconUrl }">
										</a>
											<div class="next-showli" style="display: none;">支持：
												企业网银</div></li>
									</c:forEach>
									<a class="b-r-4 more js-list-more"> <img id="imgUnSelected"
										class="imgSelect"
										src="https://cdnpay.dinpay.com/pay/ipp2/images/select.png">
										<img id="imgSelected" class="imgSelect" style="display: none"
										src="https://cdnpay.dinpay.com/pay/ipp2/images/selected.png">
										更多银行
									</a>
								</ul>
							</div>
						</div> <!----选择或输入选择银行 end---->
					</li>
				</c:if>


				<c:if test="${fun:length(wechat) > 0 && !showWY}">
					<!-- 微信扫码(普通商户) -->
					<li class="first-Li" style="display: none;">
						<div class="js-paycard js-pcard1">
							<div class="bank-list">
								<ul class="js-pointcard" paymethod="wechat">
									<c:forEach var="item" items="${wechat }" varStatus="status">
										<li data-bankcode='${item.bankCode }'><a> <!--  <img src="https://cdnpay.dinpay.com/pay/ipp2/images/YDSZX.png">-->
												微信支付
										</a></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</li>
				</c:if>

				<!-- 微信扫码(叮叮商户) -->

				<c:if test="${fun:length(alipay) > 0 && !showWY}">
					<!-- 支付宝收款(普通商户) -->
					<li class="first-Li" style="display: none;">
						<div class="js-paycard js-pcard1">
							<div class="bank-list">
								<ul class="js-pointcard" paymethod="alipay">
									<c:forEach var="item" items="${alipay }" varStatus="status">
										<li data-bankcode='${item.bankCode }'><a><img
												src="${item.iconUrl }"></a></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</li>
				</c:if>

				<!-- 支付宝收款(叮叮商户) -->

				<c:if test="${fun:length(qqpay) > 0 && !showWY}">
					<!-- QQ钱包 -->
					<li class="first-Li" style="display: none;">
						<div class="js-paycard js-pcard1">
							<div class="bank-list">
								<ul class="js-pointcard" paymethod="qqpay">
									<c:forEach var="item" items="${qqpay }" varStatus="status">
										<li data-bankcode='${item.bankCode }'><a> <!-- <img src="${item.iconUrl }"> QQ钱包 -->${item.bankName }</a>
										</li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</li>
				</c:if>

				<c:if test="${fun:length(jdpay) > 0 && !showWY}">
					<!-- 京东支付 -->
					<li class="first-Li" style="display: none;">
						<div class="js-paycard js-pcard1">
							<div class="bank-list">
								<ul class="js-pointcard" paymethod="jdpay">
									<c:forEach var="item" items="${jdpay }" varStatus="status">
										<li data-bankcode='${item.bankCode }'><a> <!-- <img src="${item.iconUrl }"> 京东支付 -->${item.bankName }</a>
										</li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</li>
				</c:if>

				<!-- 点卡-->
			</ul>
		</div>
	</div>
	<div class="clear"></div>
	<c:if test="${fun:length(bank) > 0}">
		<div id="payquestion" class="que payquestion" style="display: block;">
			<h2>常见问题</h2>
			<dl>
				<dt>1、银行卡支付需不需要开通网上银行？</dt>
				<dd>答：快捷支付不需要开通网上银行，网银支付需要开通网上银行。</dd>
				<dt>2、银行卡支付支持哪些类型的卡？</dt>
				<dd>答：借记卡，信用卡。</dd>
				<dt>3、如何修改预留手机号码？</dt>
				<dd>答：请确认您是否在银行已经预留手机号码，如未预留或已遗忘，请携带身份证及银行卡至银行网点柜台，告知银行工作人员修改成您的常用手机号码。</dd>
				<dt>4、可以使用他人借记卡开通快捷支付么？</dt>
				<dd>答：不可以。为了您的支付账号和银行借记卡的安全，请使用本人银行卡开通快捷支付。</dd>
				<dt>5、各家银行的支付限制额度相同吗？</dt>
				<dd>答：不同的银行开放的支付额度不同，每家银行根据不同的用卡条件（短信验证码、Ukey）设定不同的支付额度。如需了解各家银行具体的额度，可通过点击银行图标进行查看。</dd>
				<dt>6、造成“支付被拒绝”的原因有哪些？</dt>
				<dd>
					答：(1) 所持银行卡尚未开通网上在线支付功能；<br /> (2) 所持银行卡已过期、作废、挂失；<br /> (3)
					所持银行卡内余额不足； <br /> (4) 输入银行卡卡号或密码不符； <br /> (5) 输入证件号不符； <br />
					(6) 银行系统数据传输出现异常；<br /> (7) 网络中断。
				</dd>
			</dl>
		</div>
	</c:if>
	<c:if test="${fun:length(wechat) > 0 && !showWY}">
		<div id="payquestion2" class="que payquestion"${fun:length(bank) > 0 ? '' : 'style="display: block;"'} }>
			<h2>常见问题</h2>
			<dl>
				<dt>1、长时间未加载显示二维码图片怎么办？</dt>
				<dd>答：请检测您电脑的网络连接是否正常，网络中断或者网速太慢可能会影响二维码图片的加载速度。可尝试刷新页面重新获取二维码。</dd>
				<dt>2、生成的二维码扫描不出来怎么办？</dt>
				<dd>答：请确保使用的是微信客户端的扫一扫功能进行扫描，同时要保证您手机的网络连接正常。</dd>
				<dt>3、点击刷新按钮还是没有生成微信二维码怎么办？</dt>
				<dd>答：可能是订单超过付款时间了，需要到商家网站重新下单支付。</dd>
			</dl>
		</div>
	</c:if>
	<c:if test="${fun:length(alipay) > 0 && !showWY}">
		<div id="payquestion3" class="que payquestion">
			<h2>常见问题</h2>
			<dl>
				<dt>1、长时间未加载显示二维码图片怎么办？</dt>
				<dd>答：请检测您电脑的网络连接是否正常，网络中断或者网速太慢可能会影响二维码图片的加载速度。可尝试刷新页面重新获取二维码。</dd>
				<dt>2、生成的二维码扫描不出来怎么办？</dt>
				<dd>答：请确保使用的是微信客户端的扫一扫功能进行扫描，同时要保证您手机的网络连接正常。</dd>
				<dt>3、点击刷新按钮还是没有生成微信二维码怎么办？</dt>
				<dd>答：可能是订单超过付款时间了，需要到商家网站重新下单支付。</dd>
			</dl>
		</div>
	</c:if>
	<c:if test="${fun:length(qqpay) > 0 && !showWY}">
		<div id="payquestion4" class="que payquestion">
			<h2>常见问题</h2>
			<dl>
				<dt>1、长时间未加载显示二维码图片怎么办？</dt>
				<dd>答：请检测您电脑的网络连接是否正常，网络中断或者网速太慢可能会影响二维码图片的加载速度。可尝试刷新页面重新获取二维码。</dd>
				<dt>2、生成的二维码扫描不出来怎么办？</dt>
				<dd>答：请确保使用的是微信客户端的扫一扫功能进行扫描，同时要保证您手机的网络连接正常。</dd>
				<dt>3、点击刷新按钮还是没有生成微信二维码怎么办？</dt>
				<dd>答：可能是订单超过付款时间了，需要到商家网站重新下单支付。</dd>
			</dl>
		</div>
	</c:if>
	<c:if test="${fun:length(jdpay) > 0 && !showWY}">
		<div id="payquestion5" class="que payquestion">
			<h2>常见问题</h2>
			<dl>
				<dt>1、长时间未加载显示二维码图片怎么办？</dt>
				<dd>答：请检测您电脑的网络连接是否正常，网络中断或者网速太慢可能会影响二维码图片的加载速度。可尝试刷新页面重新获取二维码。</dd>
				<dt>2、生成的二维码扫描不出来怎么办？</dt>
				<dd>答：请确保使用的是微信客户端的扫一扫功能进行扫描，同时要保证您手机的网络连接正常。</dd>
				<dt>3、点击刷新按钮还是没有生成微信二维码怎么办？</dt>
				<dd>答：可能是订单超过付款时间了，需要到商家网站重新下单支付。</dd>
			</dl>
		</div>
	</c:if>

	<!-- 错误提示弹出框 -->
	<div id="errorPopbox" class="hide">
		<a href="javascript:void(0) " class="closePop" title="关闭"
			onclick="gateway.closeErrorPop()"></a>
		<div class="errorInnerBox">
			<div class="inContent">
				<div class="conBox">
					<div class="conTxt">
						<h3>支付失败</h3>
						<p>错误代码：TIMEOUT_OR_ILLEGAL_REQUEST</p>
						<p>错误信息：非法请求或请求超时</p>
					</div>
				</div>
			</div>
			<div class="otherTxt">
				<span><a href="javascript:void(0)"
					onclick="gateway.closeErrorPop()">选择其他支付方式</a><i>|</i><a
					href="http://www.dinpay.com/faq_list.html?=#19F" target="_blank">查看常见问题</a></span>
			</div>
		</div>
	</div>

	<!-- 红色提示图标透明遮罩层-->
	<div class="maskOver"></div>

	<script type="text/javascript">
		$(function() {
			_.templateSettings = {
				evaluate : /\{#([\s\S]+?)#\}/g,
				interpolate : /\{\{(.+?)\}\}/g
			};
		});
		$(function() {
			var scanpayFilterArr = [ "xifupay", "mibeipay", "goldpayment", "aikulipay", "bcfapi", "caifubao", "duolabao", "ztbaoh5pay", "ifeepay", "aabill", "one2pay", "zdbbill", "magopay", "duodebao", "yuanbaozhifu", "jinanfu", "tongbaopay", "xunhuibao", "xunfutong", "qifupay", "shangyinxin", "tonghuika", "kcpay", "baifupay", "shunfupay", "jinhaizhe", "xinmapay", "woozf", "tianchuangpay", "ludepay", "hebaopay", "zaixianbao", "aimisenpay", "dpayhk", "zeshengpay", "qingyifu", "changchengzhifu", "tianjinchuangxingou", "yompay20", "ak47pay", "ufuzhifu", "juheminsheng", "dingfengpay", "eshidai", "gmstone", "ztbaopay", "wefupay", "shenhx", "xunjietong", "dcpay", "heshengpay", "cmbcpos", "mmpay", "foupang", "xjpay", "yizhibank", "xingfupay", "xingheyitong", "sf532", "ytbao", "caimao9", "xiipay", "xiih5pay",
					"atrustpay", "huihepay", "zhifuhui", "congfu","kumo","npay","yizhifupay","tianjifu","zhinengyun","baisheng","xunfei","shanfutong","yinxun","laifutone","xinbao","juejue","yizhifu","sulong","zhihuifu","baifu","feimiaofu","shuidi","yingxintong"]; // 
			var straightFilterArr = [ "zhuogepay", "goldpayment", "saaspay", "aikulih5pay", "aikulipay", "bcfh5api", "caifubao", "magoh5pay", "magopay", "tonghuika", "xinmapay", "hebaopay", "aimisenpay", "dcpay","shanfutong","xunfei","shuidi","yingxintong","yaopan","suda","kuaijiepay","weizhilian","daxiang","haitianh5","onegowangyin","chuangxinwangyin","mingshu","zhongxun","wantongpay","panda"]; // 
			$("li[data-bankcode]").click(function() {
				var bankcode = $(this).data("bankcode");
				var paymethod = $(this).parent("ul[paymethod]").attr("paymethod");
				$.ajax({
					type : "POST",
					dataType : "json",
					url : "${base }/onlinepay/pay.do",
					data : {
						amount : $("#amount").val(),
						payId : $("#payId").val(),
						// payCode:$("#payCode").val(), // 报错回滚用，稳定后删除
						bankcode : bankcode,
						account : "${account}",
						verifyCode : '${verifyCode}'
					},
					success : function(result) {
						if (result.success == false) {
							alert(result.msg);
						} else if (result.success == true) {
							if (result && result.data && result.data.formParams) {
								var payReferer = result.data.payReferer;

								var tplHtml = $("#toPayTemplate").html();
								var content = _.template(tplHtml)($.extend({
									paymethod : paymethod
								}, result.data));
								$(".gatemain-ul li:visible").html(content);

								if ((paymethod == "wechat" || paymethod == "alipay" || paymethod == "qqpay" || paymethod == "jdpay") && ($.inArray("${iconCss}", scanpayFilterArr) >= 0)) {
									$("#expressAgreePay" + paymethod).click(function() {
										var redirectUrl = "";
										var redirectParams = {};
										_.map(result.data.formParams, function(value, key) {
											if (key == "redirectUrl") {
												redirectUrl = value;
											} else {
												redirectParams[key] = value;
											}
										});
										$.ajax({
											type : "POST",
											dataType : "json",
											url : "${base}/onlinepay/utils/getWecahtQrcode.do",
											data : {
												iconCss : "${iconCss}",
												redirectUrl : redirectUrl,
												payId : $("#payId").val(),
												redirectParams : JSON.stringify(redirectParams),
												payReferer : payReferer
											},
											success : function(result) {
												if (result.success == false) {
													alert(result.msg);
												} else if (result.success == true) {
													var options = {
														render : 'image',
														ecLevel : 'H',
														minVersion : parseInt(1, 10),
														color : '#333333',
														bgColor : '#ffffff',
														text : result.qrcodeUrl,
														size : parseInt(165, 10),
														radius : parseInt(40, 10) * 0.01,
														quiet : parseInt(2, 10),
														mode : parseInt(0, 10),
													};
													$(".gatemain-ul li:visible").empty().qrcode(options);
												}
											}
										});
									});
								} else if (paymethod == "bank" && $.inArray("${iconCss}", straightFilterArr) >= 0) {
									$("#expressAgreePay" + paymethod).click(function() {
										var redirectUrl = "";
										var redirectParams = {};
										_.map(result.data.formParams, function(value, key) {
											if (key == "redirectUrl") {
												redirectUrl = value;
											} else {
												redirectParams[key] = value;
											}
										});
										$.ajax({
											type : "POST",
											dataType : "json",
											url : "${base}/onlinepay/utils/getWecahtQrcode.do",
											data : {
												iconCss : "${iconCss}",
												redirectUrl : redirectUrl,
												payId : $("#payId").val(),
												redirectParams : JSON.stringify(redirectParams),
												payReferer : payReferer
											},
											success : function(result) {
												if (result.success == false) {
													alert(result.msg);
												} else if (result.success == true) {
													if ("${iconCss}" == "xinmapay") {
														// document.write(result.qrcodeUrl);

														var sw = '';
														csw = '';
														sh = '';
														csh = '';
														ctop = '';
														cleft = '';

														sw = $(window.parent).width();
														sh = $(window.parent).height();
														csw = $(window.parent).width();
														csh = $(window.parent).height();
														ctop = 0;
														cleft = 0;

														var windowOpen = window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
														var new_doc = windowOpen.document.open("text/html", "replace");
														new_doc.write(result.qrcodeUrl);
														new_doc.close();
													} else {
														location.href = result.qrcodeUrl;
													}
												}
											}
										});
									});
								} else {
									$("#expressAgreePay" + paymethod).click(function() {
										$(this).parents("form").submit();
									});
								}
							} else {
								alert("系统发生错误");
							}
						}
					}
				});
			});
		});
	</script>
	<script type="text/html" id="toPayTemplate" style="display: none;">
		<form action="{{formAction}}" method="get" target=''>
			{#
			_.map(formParams, function(value, key){
			#}
				<input type="hidden" name="{{key}}" value="{{value}}"/>
			{#
			});
			#}
			<div class="had-quick new-quick js-changetab3" style="display: block;">
				<div class="js-ddbank ">
					<div class="js-ddquick" style="display: block;">
						<ul>
							<li class="radio js-radio">
								<span>订单号 </span>
								<div style="line-height: 30px;">
								<p class="fl new-input">
									{{orderId}}
								</p>
								<!-- <span class="fl ui-form-explain-correct" id="mobileError" style=""></span> -->
								</div>
							</li>
							<li>
								<span>会员账号</span>
								<div style="line-height: 30px;">
								<p class="fl new-input">
									{{account}}
								</p>
								<!-- <span class="fl ui-form-explain-correct" id="mobileError" style=""></span> -->
								</div>
							</li>
							<li>
								<span>充值金额 </span>
								<div style="line-height: 30px;">
									<p class="fl new-input">
										{{amount}}
									</p>
									<!-- <span class="fl ui-form-explain-correct" id="mobileError" style=""></span> -->
								</div>
							</li>
						</ul>
						<div class="had-rules-div">
							<button type='button' id="expressAgreePay{{paymethod}}" class="had-agree b-r-4" style="border: 0px;">确认信息并支付</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</script>
</body>
</html>