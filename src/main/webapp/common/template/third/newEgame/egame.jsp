<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html class="game zh-cn isLoginN ">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	/* 	background: #2E1109; */
}

#mainBody {
	width: 100%;
	height: 100%;
}
</style>
<link href="${base }/common/template/third/newEgame/css/standard.css"
	rel="stylesheet">
<link href="${base }/common/template/third/newEgame/css/ruby.css"
	rel="stylesheet">
<link
	href="${base }/common/template/third/newEgame/css/font-awesome.css"
	rel="stylesheet">
<script
	src="${base }/common/template/third/newEgame/js/jquery-1.7.2.min.js"></script>
<script
	src="${base }/common/template/third/newEgame/js/jquery.cookie.js"></script>
<script type="text/javascript"
	src="${base}/common/js/artTemplate/template.js"></script>
<script
	src="${base }/common/template/third/newEgame/js/game_newcasino_menu.js"></script>
<script
	src="${base }/common/template/third/newEgame/js/egameData_${egameVersion}.js?v=17"></script>
<script
	src="${base }/common/template/third/newEgame/js/common_v2.js?v=8"></script>
<style type="text/css" media="screen">
#ele-logo-wrap {
	visibility: hidden
}

.ele-view-mini-hiden {
	display: none;
}
</style>
<script type="text/javascript">
	var base = '${base}';
	function checkbrowse() {
		var ua = navigator.userAgent.toLowerCase();
		var is = (ua.match(/\b(chrome|opera|safari|msie|firefox)\b/) || [ '',
				'mozilla' ])[1];
		var r = '(?:' + is + '|version)[\\/: ]([\\d.]+)';
		var v = (ua.match(new RegExp(r)) || [])[1];
		jQuery.browser.is = is;
		jQuery.browser.ver = v;
		return {
			'is' : jQuery.browser.is,
			'ver' : jQuery.browser.ver
		}
	}
</script>
</head>
<body>
	<input id="userid" value="${isLogin }" type="hidden" />
	<jsp:include page="include/css.jsp" />
	<div id="mainBody">
		<div id="page-container">
			<div id="bodyContainer" class="clearfix">
				<div id="elenew-game-nav-wrap"
					class="elenew-game-nav-wrap gamenew-hall-23 clearfix">
					<div class="elenew-gn-btn">
						<div id="elenew-gn-icon" class="elenew-gn-icon"></div>
						<div class="elenew-gn-text-wrap gm-no-2">
							<span class="gm-icon"></span>
							<div id="elenew-gn-text">热门游戏</div>
							<i class="fa fa-angle-down"></i>
						</div>
						<div class="elenew-gn-wrap" style="display: none; height: 414px;">
							<div class="elenew-gn-innerwrap clearfix"
								style="position: absolute; top: 0px;">
								<div class="elenew-game-menu1 clearfix">
									<a data-id="gm-no-2" href="javascript:void(0);"
										class="elenew-gm-1  clearfix" data-num="40">
										<div class="ele-categories-border2 "></div> <span
										class="gm-icon gm-icon-2"></span> <span class="gm-name">热门游戏</span>
									</a>
								</div>
								<div class="elenew-gm-bottom clear"></div>
							</div>
						</div>
					</div>
					<div class="elenew-live-trace elenew-trace-hall-23">
						<span class="elenew-trace-title">电子游艺</span> <span
							class="elenew-trace-sub" style=""> <span
							class="trace-arrow">&gt;&gt;</span> <span class="trace-0">
								<span class="gamenew-trace gamenew-trace-0"> <span
									class="gamenew-trace-total"> <a
										href="javascript:void(0);" data-id="84" title="全部"
										id="all_game_conut"> <span class="gm-name">全部</span>
									</a>
								</span> <span>(<b class="gamenew-trace-num-0"
										id="all_game_count">0</b>)
								</span>
							</span>
						</span> <span class="trace-1"> <span>&gt;</span> 
							<!-- 添加捕鱼快捷 -->
							<span
								class="gamenew-trace gamenew-trace-1" id="by_egame_mid_menu"
								style="display: none;"> 
								<c:if test="${isAgDzOnOff eq 'on' || (empty isAgDzOnOff && isAgOnOff eq 'on')}">
									<span class="gamenew-trace-current" onclick="get_by_data('ag');">AG</span>
								</c:if>
								<c:if test="${isCq9OnOff eq 'on'}">
									<span class="gamenew-trace-current" onclick="get_by_data('cq9');">CQ9</span>
								</c:if>
								<c:if test="${isPtOnOff eq 'on'}">
									<span class="gamenew-trace-current" onclick="get_by_data('pt');">PT</span>
								</c:if>
								<c:if test="${isBbinOnOff eq 'on'}">
									<span class="gamenew-trace-current" onclick="get_by_data('bbin');">BBIN</span>
								</c:if>
								<c:if test="${isJdbOnOff eq 'on'}">
									<span class="gamenew-trace-current" onclick="get_by_data('jdb');">JDB</span>
								</c:if>
							</span>
							<!-- end -->
							<span
								class="gamenew-trace gamenew-trace-1" id="ag_egame_mid_menu"
								style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_ag_data('1');">最新游戏</span>
									<span class="gamenew-trace-current" onclick="get_ag_data('2');">老虎机</span>
									<span class="gamenew-trace-current" onclick="get_ag_data('3');">热门游戏</span>
									<span class="gamenew-trace-current" onclick="get_ag_data('4');">YOPLAY</span>
							</span>
							<span class="gamenew-trace gamenew-trace-1"
								id="mg_egame_mid_menu" style="display: none;"> 
									<span class="gamenew-trace-current" onclick="get_mg_data('1');">拉霸</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('4');">刮刮卡</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('5');">桌面游戏</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('6');">视讯扑克</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('8');">最新游戏</span>
									<!-- <span class="gamenew-trace-current" onclick="get_mg_data('1');">热门游戏</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('2');">最新游戏</span>
									<span class="gamenew-trace-current" onclick="get_mg_data('3');">搏击游戏</span> -->
									<!--  
						 	<span class="gamenew-trace-current" onclick="get_mg_data('2');">其他游戏</span>
						 	 <span class="gamenew-trace-current"
									onclick="get_mg_data('7');">热门游戏</span>-->
							</span> <span class="gamenew-trace gamenew-trace-1"
								id="pt_egame_mid_menu" style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_pt_data('桌牌游戏');">桌牌游戏</span>
									<span class="gamenew-trace-current"
									onclick="get_pt_data('老虎机');">老虎机</span> <span
									class="gamenew-trace-current" onclick="get_pt_data('街机游戏');">街机游戏</span>
									<span class="gamenew-trace-current"
									onclick="get_pt_data('视讯扑克');">视讯扑克</span> <span
									class="gamenew-trace-current" onclick="get_pt_data('刮刮卡');">刮刮卡</span>
							</span> <span class="gamenew-trace gamenew-trace-1"
								id="qt_egame_mid_menu" style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_qt_data('1');">最新游戏</span>
									<span class="gamenew-trace-current" onclick="get_qt_data('2');">老虎机</span>
									<span class="gamenew-trace-current" onclick="get_qt_data('3');">桌面游戏</span>
									<span class="gamenew-trace-current" onclick="get_qt_data('4');">热门游戏</span>
							</span> <span class="gamenew-trace gamenew-trace-1"
								id="cq9_egame_mid_menu" style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_cq9_data('1');">最新游戏</span>
							</span> 
							<span class="gamenew-trace gamenew-trace-1"
								id="isb_egame_mid_menu" style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_isb_data('1');">老虎机</span>
									<span class="gamenew-trace-current"
									onclick="get_isb_data('2');">卡牌游戏</span> <span
									class="gamenew-trace-current" onclick="get_isb_data('3');">视频扑克</span>
									<span class="gamenew-trace-current"
									onclick="get_isb_data('4');">轮盘游戏</span>
							</span>
							<span class="gamenew-trace gamenew-trace-1"
								id="jdb_egame_mid_menu" style="display: none;"> <span
									class="gamenew-trace-current" onclick="get_jdb_data('0');">老虎机</span>
									<span class="gamenew-trace-current" onclick="get_jdb_data('7');">捕鱼机</span> 
									<span class="gamenew-trace-current" onclick="get_jdb_data('9');">街机游戏</span>
							</span>
						</span> <span class="trace-2" style="display: none;"> <span>&gt;</span>
								<span class="gamenew-trace gamenew-trace-2"> <span
									class="gamenew-trace-option"> </span> <span
									class="gamenew-trace-current"></span> <span
									class="gamenew-trace-num">(<b
										class="gamenew-trace-num-2"></b>)
								</span>
							</span>
						</span>
						</span>
					</div>
					<div class="elenew-live-view clearfix">
						<span><a href="javascript:void(0);"
							class="elenew-viewbtn-block view-active" data-view="block"><i
								class="fa fa-th"></i></a></span> <span><a href="javascript:void(0);"
							class="elenew-viewbtn-mini" data-view="mini"><i
								class="fa fa-align-justify"></i></a></span> <i></i>
					</div>
					<div id="elenew-search-wrap">
						<div class="elenew-search-btn" tabindex="6"
							onclick="searchGame();">
							<i class="fa fa-search fa-2x"></i>
						</div>
						<div class="elenew-search-input" title="请输入游戏名称"
							style="width: 0px; padding-left: 0px;">
							<input type="search" name="elenew-search-game"
								id="elenew-search-game" placeholder="请输入游戏名称" tabindex="5">
							<input value="搜索" type="button" />
						</div>
					</div>
				</div>

				<jsp:include page="include/body.jsp" />


			</div>
		</div>
	</div>
	<script type="text/javascript">
		template.helper('$imgFmt', function(type, path) {
			if ("qt" == type || "pt" == type || "ag" == type) {
				return path;
			}
			if (path && path.indexOf('.') == -1) {
				path += ".png";
			}
			return path;
		});
		//搜索框特效
		function searchBtn() {
			var h = $(".elenew-search-input"), g = 268;
			if ($("body").width() < 769) {
				g = $("body").width() - $(".elenew-search-btn").width() - 15
			} else {
				if ($("body").width() < 960) {
					g = $("body").width() - $(".elenew-live-view").width() - 57
				}
			}
			if (h.width() === 0) {
				h.stop().animate({
					width : g,
					"padding-left" : 15
				}, function() {
					$("#elenew-search-game").focus()
				});
				$(this).addClass("search-active");
				return;
			}
		};

		$(function() {
			document.onkeydown = function(event_e) {
				if (window.event) {
					event_e = window.event;
				}
				var int_keycode = event_e.charCode || event_e.keyCode;
				if (int_keycode == 13) {
					searchGame();
					return;
				}
			}

			/* $("img")
					.lazyload(
							{
								placeholder : "${base }/common/template/third/newEgame/images/grey.gif",
								effect : "fadeIn",
								threshold : 20
							}); */

			$('.categories-item-link').click(function() {
				$('.categories-item-link').removeClass('current');
				$(this).addClass('current');
				var t = $(this).attr('data-id');
				$('.gamenew-trace-1').css('display', 'none');
				if(t == 'by'){
					$('#e_game_type').val('by');
					$('#by_egame_mid_menu').css('display', '');
				}else if (t == 'ag') {
					$('#e_game_type').val('ag');
					$('#ag_egame_mid_menu').css('display', '');
				} else if (t == 'mg') {
					$('#e_game_type').val('mg');
					$('#mg_egame_mid_menu').css('display', '');
				} else if (t == 'pt') {
					$('#e_game_type').val('pt');
					$('#pt_egame_mid_menu').css('display', '');
				} else if (t == 'qt') {
					$('#e_game_type').val('qt');
					$('#qt_egame_mid_menu').css('display', '');
				} else if (t == 'cq9') {
					$('#e_game_type').val('cq9');
					$('#cq9_egame_mid_menu').css('display', '');
				} else if (t == 'jdb') {
					$('#e_game_type').val('jdb');
					$('#jdb_egame_mid_menu').css('display', '');
				} else if (t == 'ttg') {
					$('#e_game_type').val('ttg');
					$('#ttg_egame_mid_menu').css('display', '');
				} else if (t == 'mw') {
					$('#e_game_type').val('mw');
					$('#mw_egame_mid_menu').css('display', '');
				} else if (t == 'isb') {
					$('#e_game_type').val('isb');
					$('#isb_egame_mid_menu').css('display', '');
				} else {
					return;
				}
			});

			searchBtn();

			$('.elenew-live-view a').click(
					function() {
						var b = $(this).data("view");
						$.cookie("liveListStyle", b);
						$(".elenew-live-view a").removeClass("view-active");
						$(this).addClass("view-active");
						$("#elenew-game-space")
								.removeClass("elenew-view-block").removeClass(
										"elenew-view-mini").addClass(
										"elenew-view-" + b);
						if ("mini" == b) {
							$(".elenew-game-name clearfix").removeClass(
									"ele-view-mini-hiden");
						} else {
							$(".elenew-game-name clearfix").addClass(
									"ele-view-mini-hiden");
						}
					});

			$("#elenew-game-space").on(
					"mouseenter",
					".elenew-view-mini .elenew-game-name",
					function() {
						$(this).closest(".elenew-game-layout").css("z-index",
								"2").find(".elenew-img-wrap").addClass(
								"mini-img-show")
					}).on(
					"mouseleave",
					".elenew-view-mini .elenew-game-name",
					function() {
						$(this).closest(".elenew-game-layout").css("z-index",
								"1").find(".elenew-img-wrap").removeClass(
								"mini-img-show")
					});
		});
	</script>
	<script src="${base }/common/js/jquery.lazyload.js"></script>
</body>
</html>
<script id="gameList_tpl" type="text/html">
{{each games as game}}
<div class="elenew-game-layout  elenew-html5-layout" style="float: left; z-index: 1;">
	<div class="elenew-img-wrap">
		<div class="elenew-img-innerwrap">
			<div class="mask-wrap">
				<div class="elenew-game-ctl-wrap inner-one ">
					<div class="elenew-game-ctl-links">
						<div class="elenew-block-title" title="{{game.DisplayName}}"></div>
						<a href="javascript:void(0)"
							onclick="toEgame('{{type}}','{{game.LapisId}}');"
							class="elenew-game-link ctl-btn-lite" title="进入游戏"><span>进入游戏</span></a>
						<div class="gamerule-btn-wrap">
							<a href="javascript:void(0)"
								onclick="toEgame('{{type}}','{{game.LapisId}}');"
								class="elenew-game-link ctl-btn-enter"><span>进入游戏</span></a> </a>
						</div>
					</div>
				</div>
				<a class="elenew-game-img {{imgCss}}" href="javascript:void(0)"> <img
					src="{{imgPath}}{{$imgFmt type game.ButtonImagePath}}">
				</a>
			</div>
			<div class="elenew-innerwrap">
				<h3>
					<div class="img-innerwrap-name">{{game.DisplayName}}</div>
				</h3>
			</div>
		</div>
		<div class="elenew-game-ctl-wrap">
			<div class="elenew-game-ctl-links clearfix">
				<div class="elenew-block-title" title="{{game.DisplayName}}"></div>
				<a href="javascript:void(0)"
					onclick="toEgame('{{type}}','{{game.LapisId}}');"
					class="elenew-game-link ctl-btn-lite" title="进入游戏"><span>进入游戏</span></a>
			</div>
		</div>
	</div>
	<div class="elenew-game-name clearfix ele-view-mini-hiden">
		<a href="#"
			class="elenew-game-tool tool-btn-favorite favorite-icon-N"> <i
			class="fa fa-star fa-lg"></i>
		</a>
		<h3 title="{{game.DisplayName}}">{{game.DisplayName}}</h3>
	</div>
</div>
{{/each}}
</script>
