"use strict";
$(document).ready(function() {
    var a = $("#ele-game-categories-inner"),
        d = $("#ele-game-categories-dropmenu"),
        f = $(".categories-dropmenu-inner").height(),
        e = $(".elenew-gn-btn"),
        b = false,
        c = false;
    $("#ele-categories-text").addClass($(".categories-item-link.current").attr("data-id")).text($(".categories-item-link.current").text());
    $("#ele-game-categories-inner").click(function() {
        if ($("body").width() > 768) {
            return
        }
        if ($("#ele-game-categories-inner").hasClass("drop-active")) {
            a.removeClass("drop-active");
            d.stop().animate({
                height: 0
            })
        } else {
            a.addClass("drop-active");
            d.stop().animate({
                height: $(".categories-dropmenu-inner").height()
            })
        }
    });
    $(".elenew-gn-wrap").show().height($(".elenew-gn-innerwrap").height()).hide();
    $(".elenew-gn-innerwrap").css({
        position: "absolute",
        top: "0"
    });
    e.mouseenter(function() {
        if ($("body").width() <= 768) {
            return
        }
        $("#elenew-gn-icon").addClass("gn-active");
        $(".elenew-gn-wrap").stop(true, true).delay(200).slideDown()
    }).mouseleave(function() {
        if ($("body").width() <= 768) {
            return
        }
        $("#elenew-gn-icon").removeClass("gn-active");
        $(".elenew-gn-wrap").stop(true, true).delay(300).slideUp()
    }).on("click", ".elenew-gm-1, .elenew-gm-2", function() {
        $("#elenew-gn-icon").removeClass("gn-active");
        $(".elenew-gn-wrap").stop().slideUp()
    });
    $(".elenew-gn-text-wrap").click(function() {
        if (!b) {
            b = true;
            return
        }
        if ($("body").width() > 768) {
            return
        }
        if ($("#elenew-gn-icon").hasClass("gn-active")) {
            $(".elenew-gn-mask").remove();
            $("#elenew-gn-icon").removeClass("gn-active");
            $(".elenew-gn-wrap").stop().slideUp()
        } else {
            $("#elenew-game-wrap").append('<div class="elenew-gn-mask"></div>');
            $("#elenew-gn-icon").addClass("gn-active");
            $(".elenew-gn-wrap").stop(true, true).delay(200).slideDown()
        }
    });
    $(".elenew-gm-1").click(function() {
        if (!b) {
            b = true;
            return
        }
        $(".elenew-gn-mask").remove()
    });
    $(".gamenew-trace-1, .gamenew-trace-2").mouseenter(function() {
        $(this).find(".gamenew-trace-option").stop(true, true).slideDown()
    }).mouseleave(function() {
        $(this).find(".gamenew-trace-option").stop(true, true).slideUp()
    });
    $(".gamenew-trace-0").find(".gamenew-trace-total").find("a").click(function() {
        $("#gm-no-" + $(this).data("id")).trigger("click")
    });
    $(".gamenew-trace-1").find(".gamenew-trace-option").find("a").click(function() {
        $("#gm-no-" + $(this).data("id")).trigger("click")
    });
    $(".gamenew-trace-2").find(".gamenew-trace-option").find("a").click(function() {
        $("#gm-subno-" + $(this).data("id")).trigger("click")
    });
    $(window).resize(function() {
        $(".elenew-gn-wrap").show().height($(".elenew-gn-innerwrap").height()).hide();
        a.removeClass("drop-active");
        var g = (($("body").width() <= 768) ? 0 : f);
        d.stop().css({
            height: g
        })
    })
});