var mgdata = [
	{
		"DisplayEnName": "Cricket Star Scratch",
		"DisplayName": "板球明星",
		"ButtonImagePath": "BTN_CricketStarScratch_ZH",
		"LapisId": "7076",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CricketStarScratch"
	},
	{
		"DisplayEnName": "Elven Gold",
		"DisplayName": "精灵黄金",
		"ButtonImagePath": "BTN_ElvenGold_ZH",
		"LapisId": "7074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ElvenGold"
	},
	{
		"DisplayEnName": "Hyper Gold",
		"DisplayName": "超猎黄金",
		"ButtonImagePath": "BTN_HyperGold_ZH",
		"LapisId": "7073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HyperGold"
	},
	{
		"DisplayEnName": "Legacy of Oz",
		"DisplayName": "奥兹Oz传奇",
		"ButtonImagePath": "BTN_LegacyofOz_ZH",
		"LapisId": "7072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LegacyofOz"
	},
	{
		"DisplayEnName": "Soccer Striker",
		"DisplayName": "足球前锋",
		"ButtonImagePath": "BTN_SoccerStriker_ZH",
		"LapisId": "7075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SoccerStriker"
	},
	{
		"DisplayEnName": "10000 Wishes",
		"DisplayName": "10000个愿望",
		"ButtonImagePath": "BTN_10000Wishes_ZH",
		"LapisId": "6742",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_10000Wishes"
	},
	{
		"DisplayEnName": "108 Heroes",
		"DisplayName": "108好汉",
		"ButtonImagePath": "BTN_108Heroes_ZH",
		"LapisId": "1302",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_108Heroes"
	},
	{
		"DisplayEnName": "108 Heroes Multiplier Fortunes",
		"DisplayName": "108好汉乘数财富",
		"ButtonImagePath": "BTN_108HeroesMF_ZH",
		"LapisId": "1897",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_108HeroesMF"
	},
	{
		"DisplayEnName": "5 Reel Drive",
		"DisplayName": "5卷的驱动器",
		"ButtonImagePath": "BTN_5ReelDrive1",
		"LapisId": "1035",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_5ReelDrive1"
	},
	{
		"DisplayEnName": "777 Mega Deluxe",
		"DisplayName": "777绝对豪华",
		"ButtonImagePath": "BTN_777MegaDeluxe_ZH",
		"LapisId": "6300",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_777MegaDeluxe"
	},
	{
		"DisplayEnName": "777 Royal Wheel",
		"DisplayName": "777皇家幸运轮",
		"ButtonImagePath": "BTN_777RoyalWheel_ZH",
		"LapisId": "6221",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_777RoyalWheel"
	},
	{
		"DisplayEnName": "8 Golden Skulls of Holly Roger Megaways ™",
		"DisplayName": "8个Holly Roger金骷髅Megaways™",
		"ButtonImagePath": "BTN_8GoldenSkullsofHollyRogerMegaways™_ZH",
		"LapisId": "6334",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_8GoldenSkullsofHollyRogerMegaways™"
	},
	{
		"DisplayEnName": "9 Blazing Diamonds",
		"DisplayName": "9颗炽热钻石",
		"ButtonImagePath": "BTN_9BlazingDiamonds_ZH",
		"LapisId": "6399",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9BlazingDiamonds"
	},
	{
		"DisplayEnName": "9 Masks of Fire",
		"DisplayName": "9个烈焰面具",
		"ButtonImagePath": "BTN_9MasksOfFire_ZH",
		"LapisId": "5363",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9MasksOfFire"
	},
	{
		"DisplayEnName": "9 Pots of Gold",
		"DisplayName": "9罐黄金",
		"ButtonImagePath": "BTN_9PotsofGold_ZH",
		"LapisId": "5619",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_9PotsofGold"
	},
	{
		"DisplayEnName": "A Dark Matter",
		"DisplayName": "黑暗阴影",
		"ButtonImagePath": "BTN_ADarkMatter_ZH",
		"LapisId": "5382",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ADarkMatter"
	},
	{
		"DisplayEnName": "A Tale of Elves",
		"DisplayName": "精灵传说",
		"ButtonImagePath": "BTN_ATaleofElves_ZH",
		"LapisId": "6218",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ATaleofElves"
	},
	{
		"DisplayEnName": "Action Ops Snow & Sable",
		"DisplayName": "秘密行动雪诺和塞布尔",
		"ButtonImagePath": "BTN_ActionOpsSnow&Sable_ZH",
		"LapisId": "2076",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ActionOpsSnow&Sable"
	},
	{
		"DisplayEnName": "Adventure Palace HD",
		"DisplayName": "冒险宫殿",
		"ButtonImagePath": "BTN_AdventurePalaceHD",
		"LapisId": "1010",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AdventurePalaceHD"
	},
	{
		"DisplayEnName": "Adventures Of Doubloon Island™",
		"DisplayName": "金币岛大冒险",
		"ButtonImagePath": "BTN_AdventuresOfDoubloonIsland_ZH",
		"LapisId": "6304",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AdventuresOfDoubloonIsland"
	},
	{
		"DisplayEnName": "Age of Conquest",
		"DisplayName": "征服时代",
		"ButtonImagePath": "BTN_AgeofConquest_ZH",
		"LapisId": "5618",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgeofConquest"
	},
	{
		"DisplayEnName": "Age of Discovery",
		"DisplayName": "史地大发现",
		"ButtonImagePath": "BTN_AgeofDiscovery3",
		"LapisId": "1246",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgeofDiscovery3"
	},
	{
		"DisplayEnName": "Agent Jane Blonde",
		"DisplayName": "特务珍金",
		"ButtonImagePath": "BTN_AgentJaneBlonde7",
		"LapisId": "1155",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgentJaneBlonde7"
	},
	{
		"DisplayEnName": "Agent Jane Blonde Returns",
		"DisplayName": "特工简布隆德归来",
		"ButtonImagePath": "BTN_AgentJaneBlondeReturns_ZH",
		"LapisId": "2088",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AgentJaneBlondeReturns"
	},
	{
		"DisplayEnName": "Alaskan Fishing",
		"DisplayName": "阿拉斯加捕捞",
		"ButtonImagePath": "BTN_AlaskanFishing1",
		"LapisId": "1004",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlaskanFishing1"
	},
	{
		"DisplayEnName": "Alchemist Stone",
		"DisplayName": "贤者之石",
		"ButtonImagePath": "BTN_AlchemistStone_ZH",
		"LapisId": "5926",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemistStone"
	},
	{
		"DisplayEnName": "Alchemy Blast",
		"DisplayName": "炼金爆炸",
		"ButtonImagePath": "BTN_AlchemyBlast",
		"LapisId": "5490",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemyBlast"
	},
	{
		"DisplayEnName": "Alchemy Fortunes",
		"DisplayName": "炼金财宝",
		"ButtonImagePath": "BTN_AlchemyFortunes_ZH",
		"LapisId": "6219",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AlchemyFortunes"
	},
	{
		"DisplayEnName": "Amazing Link Zeus",
		"DisplayName": "神奇连环宙斯",
		"ButtonImagePath": "BTN_AmazingLinkZeus_ZH",
		"LapisId": "6332",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AmazingLinkZeus"
	},
	{
		"DisplayEnName": "American Roulette",
		"DisplayName": "美国轮盘",
		"ButtonImagePath": "BTN_USRoulette1",
		"LapisId": "1248",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_USRoulette1"
	},
	{
		"DisplayEnName": "Ancient Fortunes : Poseidon Megaways™",
		"DisplayName": "古代财富Poseidon Megaways",
		"ButtonImagePath": "BTN_AncientFortunesPoseidonMegaways_ZH",
		"LapisId": "6747",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AncientFortunesPoseidonMegaways"
	},
	{
		"DisplayEnName": "Ancient Fortunes: Zeus",
		"DisplayName": "宙斯古代财富",
		"ButtonImagePath": "BTN_AncientFortunesZeus_ZH",
		"LapisId": "4111",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AncientFortunesZeus"
	},
	{
		"DisplayEnName": "Andar Bahar Royale",
		"DisplayName": "安达巴哈",
		"ButtonImagePath": "BTN_AndarBaharRoyale",
		"LapisId": "6372",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AndarBaharRoyale"
	},
	{
		"DisplayEnName": "Ariana",
		"DisplayName": "爱丽娜",
		"ButtonImagePath": "BTN_Ariana_ZH",
		"LapisId": "1021",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Ariana"
	},
	{
		"DisplayEnName": "Asian Beauty",
		"DisplayName": "亚洲美人",
		"ButtonImagePath": "BTN_AsianBeauty1",
		"LapisId": "1384",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AsianBeauty1"
	},
	{
		"DisplayEnName": "Assassin Moon",
		"DisplayName": "刺客之月",
		"ButtonImagePath": "BTN_AssassinMoon_ZH",
		"LapisId": "6254",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AssassinMoon"
	},
	{
		"DisplayEnName": "Astro Legends: Lyra and Erion",
		"DisplayName": "太空传奇莱拉和叶莉昂",
		"ButtonImagePath": "BTN_AstroLegendsLyraandErion",
		"LapisId": "5651",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AstroLegendsLyraandErion"
	},
	{
		"DisplayEnName": "Atlantis Rising",
		"DisplayName": "亚特兰蒂斯崛起",
		"ButtonImagePath": "BTN_AtlantisRising_ZH",
		"LapisId": "6952",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AtlantisRising"
	},
	{
		"DisplayEnName": "Augustus",
		"DisplayName": "奥古斯都",
		"ButtonImagePath": "BTN_Augustus_ZH",
		"LapisId": "6257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Augustus"
	},
	{
		"DisplayEnName": "Aurora Wilds",
		"DisplayName": "极光百搭",
		"ButtonImagePath": "BTN_AuroraWilds_ZH",
		"LapisId": "4328",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AuroraWilds"
	},
	{
		"DisplayEnName": "Avalon HD",
		"DisplayName": "阿瓦隆",
		"ButtonImagePath": "BTN_AvalonHD_ZH",
		"LapisId": "1013",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_AvalonHD"
	},
	// {
	// 	"DisplayEnName": "Badminton Hero",
	// 	"DisplayName": "热血羽球",
	// 	"ButtonImagePath": "BTN_BadmintonHero_ZH",
	// 	"LapisId": "2061",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_BadmintonHero"
	// },
	{
		"DisplayEnName": "Banana Odyssey",
		"DisplayName": "香蕉奥德赛",
		"ButtonImagePath": "BTN_BananaOdyssey",
		"LapisId": "4296",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BananaOdyssey"
	},
	{
		"DisplayEnName": "Bar Bar Black Sheep 5 Reel",
		"DisplayName": "黑绵羊咩咩叫5轴",
		"ButtonImagePath": "BTN_BarBarBlackSheep5Reel_ZH",
		"LapisId": "1788",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BarBarBlackSheep5Reel"
	},
	{
		"DisplayEnName": "Bars and Stripes",
		"DisplayName": "美国酒吧",
		"ButtonImagePath": "BTN_barsandstripes",
		"LapisId": "1257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_barsandstripes"
	},
	{
		"DisplayEnName": "Basketball Star",
		"DisplayName": "篮球巨星",
		"ButtonImagePath": "BTN_BasketballStar_ZH",
		"LapisId": "1159",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStar"
	},
	{
		"DisplayEnName": "Basketball Star Deluxe",
		"DisplayName": "篮球明星豪华版",
		"ButtonImagePath": "BTN_BasketballStarDeluxe_ZH",
		"LapisId": "4256",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStarDeluxe"
	},
	{
		"DisplayEnName": "Basketball Star on Fire",
		"DisplayName": "篮球巨星火力全开",
		"ButtonImagePath": "BTN_BasketballStarOnFire_ZH",
		"LapisId": "6184",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BasketballStarOnFire"
	},
	{
		"DisplayEnName": "Battle Royale",
		"DisplayName": "大逃杀",
		"ButtonImagePath": "BTN_BattleRoyale_ZH",
		"LapisId": "1995",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BattleRoyale"
	},
	{
		"DisplayEnName": "Beautiful Bones",
		"DisplayName": "美丽骷髅",
		"ButtonImagePath": "BTN_BeautifulBones_ZH",
		"LapisId": "1890",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BeautifulBones"
	},
	{
		"DisplayEnName": "Big Kahuna",
		"DisplayName": "征服钱海",
		"ButtonImagePath": "BTN_BigKahuna1",
		"LapisId": "1399",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BigKahuna1"
	},
	{
		"DisplayEnName": "Big Top",
		"DisplayName": "马戏篷",
		"ButtonImagePath": "BTN_BigTop1",
		"LapisId": "1133",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BigTop1"
	},
	{
		"DisplayEnName": "Bikini Party",
		"DisplayName": "比基尼派对",
		"ButtonImagePath": "BTN_BikiniParty_ZH",
		"LapisId": "1290",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BikiniParty"
	},
	{
		"DisplayEnName": "Blazing Mammoth",
		"DisplayName": "炽热猛犸象",
		"ButtonImagePath": "BTN_BlazingMammoth_ZH",
		"LapisId": "6957",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BlazingMammoth"
	},
	{
		"DisplayEnName": "Boat of Fortune",
		"DisplayName": "金龙财宝",
		"ButtonImagePath": "BTN_BoatofFortune_ZH",
		"LapisId": "5491",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BoatofFortune"
	},
	{
		"DisplayEnName": "Book of King Arthur",
		"DisplayName": "亚瑟王之书",
		"ButtonImagePath": "BTN_BookOFKingArthur_ZH",
		"LapisId": "6626",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookOFKingArthur"
	},
	{
		"DisplayEnName": "Book of Oz",
		"DisplayName": "Oz之书",
		"ButtonImagePath": "BTN_BookOfOz_ZH",
		"LapisId": "2075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookOfOz"
	},
	// {
	// 	"DisplayEnName": "Book of Oz Lock 'N Spin",
	// 	"DisplayName": "Oz之书锁定并旋转",
	// 	"ButtonImagePath": "BTN_BookOfOzLockNSpin_ZH",
	// 	"LapisId": "4329",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_BookOfOzLockNSpin"
	// },
	{
		"DisplayEnName": "Bookie of Odds",
		"DisplayName": "好运经纪人",
		"ButtonImagePath": "BTN_BookieOfOdds_ZH",
		"LapisId": "2089",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BookieOfOdds"
	},
	{
		"DisplayEnName": "Boom Pirates",
		"DisplayName": "炸弹海盗",
		"ButtonImagePath": "BTN_BoomPirates_ZH",
		"LapisId": "5695",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BoomPirates"
	},
	{
		"DisplayEnName": "Break Away",
		"DisplayName": "冰球突破",
		"ButtonImagePath": "BTN_BreakAway1",
		"LapisId": "1229",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAway1"
	},
	{
		"DisplayEnName": "Break Away Deluxe",
		"DisplayName": "冰球突破豪华版",
		"ButtonImagePath": "BTN_BreakAwayDeluxe_ZH",
		"LapisId": "2074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayDeluxe"
	},
	{
		"DisplayEnName": "Break Away Lucky Wilds",
		"DisplayName": "冰球突破幸运百搭",
		"ButtonImagePath": "BTN_BreakAwayLuckyWilds_ZH",
		"LapisId": "5325",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayLuckyWilds"
	},
	{
		"DisplayEnName": "Break Away Ultra",
		"DisplayName": "冰球突破终极全胜",
		"ButtonImagePath": "BTN_BreakAwayUltra_ZH",
		"LapisId": "6251",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakAwayUltra"
	},
	{
		"DisplayEnName": "Break Da Bank",
		"DisplayName": "抢银行",
		"ButtonImagePath": "BTN_BreakDaBank1",
		"LapisId": "1023",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBank1"
	},
	{
		"DisplayEnName": "Break Da Bank Again",
		"DisplayName": "银行爆破",
		"ButtonImagePath": "BTN_BreakDaBankAgain1",
		"LapisId": "1097",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBankAgain1"
	},
	{
		"DisplayEnName": "Break Da Bank Again Respin",
		"DisplayName": "银行爆破再旋转",
		"ButtonImagePath": "BTN_BreakDaBankAgainRespin_ZH",
		"LapisId": "4297",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BreakDaBankAgainRespin"
	},
	{
		"DisplayEnName": "Bridesmaids",
		"DisplayName": "伴娘",
		"ButtonImagePath": "BTN_bridesmaids",
		"LapisId": "1360",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_bridesmaids"
	},
	{
		"DisplayEnName": "Burning Desire",
		"DisplayName": "燃烧的欲望",
		"ButtonImagePath": "BTN_BurningDesire1",
		"LapisId": "1318",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BurningDesire1"
	},
	{
		"DisplayEnName": "Bush Telegraph",
		"DisplayName": "丛林摇摆",
		"ButtonImagePath": "BTN_BushTelegraph1",
		"LapisId": "1173",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BushTelegraph1"
	},
	{
		"DisplayEnName": "Bust the Bank",
		"DisplayName": "抢劫银行",
		"ButtonImagePath": "BTN_BustTheBank1",
		"LapisId": "1204",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_BustTheBank1"
	},
	{
		"DisplayEnName": "Candy Dreams",
		"DisplayName": "梦果子乐园",
		"ButtonImagePath": "BTN_CandyDreams_ZH",
		"LapisId": "1886",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CandyDreams"
	},
	{
		"DisplayEnName": "Carnaval",
		"DisplayName": "狂欢节",
		"ButtonImagePath": "BTN_Carnaval2",
		"LapisId": "1117",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Carnaval2"
	},
	{
		"DisplayEnName": "Carnaval Jackpot",
		"DisplayName": "嘉年华大奖",
		"ButtonImagePath": "BTN_CarnavalJackpot_ZH",
		"LapisId": "6303",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CarnavalJackpot"
	},
	{
		"DisplayEnName": "Cash Crazy",
		"DisplayName": "财运疯狂",
		"ButtonImagePath": "BTN_CashCrazy9",
		"LapisId": "1393",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashCrazy9"
	},
	{
		"DisplayEnName": "Cash of Kingdoms",
		"DisplayName": "富贵王国",
		"ButtonImagePath": "BTN_CashofKingdoms_ZH",
		"LapisId": "2067",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashofKingdoms"
	},
	{
		"DisplayEnName": "Cashapillar",
		"DisplayName": "昆虫派对",
		"ButtonImagePath": "BTN_Cashapillar1",
		"LapisId": "1366",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Cashapillar1"
	},
	{
		"DisplayEnName": "CashOccino",
		"DisplayName": "现金咖啡",
		"ButtonImagePath": "BTN_CashOccino_ZH",
		"LapisId": "1996",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CashOccino"
	},
	{
		"DisplayEnName": "Cashville",
		"DisplayName": "挥金如土",
		"ButtonImagePath": "BTN_Cashville1",
		"LapisId": "1197",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Cashville1"
	},
	{
		"DisplayEnName": "Centre Court",
		"DisplayName": "中心球场",
		"ButtonImagePath": "BTN_CentreCourt1",
		"LapisId": "1291",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CentreCourt1"
	},
	{
		"DisplayEnName": "Chicago Gold",
		"DisplayName": "芝加哥黄金",
		"ButtonImagePath": "BTN_ChicagoGold_ZH",
		"LapisId": "6302",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ChicagoGold"
	},
	{
		"DisplayEnName": "Classic 243",
		"DisplayName": "经典243",
		"ButtonImagePath": "BTN_Classic243",
		"LapisId": "1879",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Classic243"
	},
	{
		"DisplayEnName": "Cool Buck - 5 Reel",
		"DisplayName": "运财酷儿-5卷轴",
		"ButtonImagePath": "BTN_CoolBuck_5Reel_ZH",
		"LapisId": "1884",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CoolBuck_5Reel"
	},
	{
		"DisplayEnName": "Cool Wolf",
		"DisplayName": "酷狼",
		"ButtonImagePath": "BTN_CoolWolf3",
		"LapisId": "1084",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CoolWolf3"
	},
	{
		"DisplayEnName": "Couch Potato",
		"DisplayName": "沙发土豆",
		"ButtonImagePath": "BTN_CouchPotato2",
		"LapisId": "1327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CouchPotato2"
	},
	{
		"DisplayEnName": "Crazy Chameleons",
		"DisplayName": "疯狂变色龙",
		"ButtonImagePath": "BTN_CrazyChameleons1",
		"LapisId": "1202",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CrazyChameleons1"
	},
	{
		"DisplayEnName": "Cricket Star",
		"DisplayName": "板球明星",
		"ButtonImagePath": "BTN_CricketStar",
		"LapisId": "1075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CricketStar"
	},
	{
		"DisplayEnName": "Crystal Rift™",
		"DisplayName": "水晶裂谷",
		"ButtonImagePath": "BTN_CrystalRift",
		"LapisId": "2069",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CrystalRift"
	},
	{
		"DisplayEnName": "Cuoi and the Banyan Tree",
		"DisplayName": "阿贵与树",
		"ButtonImagePath": "BTN_CuoiAndTheBanyanTree_ZH",
		"LapisId": "5532",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_CuoiAndTheBanyanTree"
	},
	{
		"DisplayEnName": "Deadmau5",
		"DisplayName": "鼠来宝",
		"ButtonImagePath": "BTN_Deadmau5",
		"LapisId": "6186",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Deadmau5"
	},
	{
		"DisplayEnName": "Deck The Halls",
		"DisplayName": "闪亮的圣诞节",
		"ButtonImagePath": "BTN_DeckTheHalls1",
		"LapisId": "1234",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DeckTheHalls1"
	},
	{
		"DisplayEnName": "Deco Diamonds",
		"DisplayName": "德科钻石",
		"ButtonImagePath": "BTN_DecoDiamonds_ZH",
		"LapisId": "2047",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DecoDiamonds"
	},
	{
		"DisplayEnName": "Deuces Wild",
		"DisplayName": "万能两点",
		"ButtonImagePath": "BTN_DeucesWildPowerPoker1",
		"LapisId": "1415",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DeucesWildPowerPoker1"
	},
	{
		"DisplayEnName": "Diamond Empire",
		"DisplayName": "钻石帝国",
		"ButtonImagePath": "BTN_DiamondEmpire_ZH",
		"LapisId": "1949",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DiamondEmpire"
	},
	{
		"DisplayEnName": "Diamond King Jackpots",
		"DisplayName": "钻石之王",
		"ButtonImagePath": "BTN_DiamondKingJackpots_ZH",
		"LapisId": "6220",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DiamondKingJackpots"
	},
	{
		"DisplayEnName": "Divine Diamonds",
		"DisplayName": "神圣钻石",
		"ButtonImagePath": "BTN_DivineDiamonds_ZH",
		"LapisId": "6953",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DivineDiamonds"
	},
	{
		"DisplayEnName": "Dolphin Coast",
		"DisplayName": "海豚海岸",
		"ButtonImagePath": "BTN_DolphinCoast_ZH",
		"LapisId": "2065",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DolphinCoast"
	},
	{
		"DisplayEnName": "Dolphin Quest",
		"DisplayName": "寻访海豚",
		"ButtonImagePath": "BTN_DolphinQuest",
		"LapisId": "1309",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DolphinQuest"
	},
	{
		"DisplayEnName": "Double Wammy",
		"DisplayName": "双重韦密",
		"ButtonImagePath": "BTN_DoubleWammy1",
		"LapisId": "1102",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DoubleWammy1"
	},
	{
		"DisplayEnName": "Dragon Dance",
		"DisplayName": "舞龙",
		"ButtonImagePath": "BTN_DragonDance_ZH",
		"LapisId": "1037",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DragonDance"
	},
	{
		"DisplayEnName": "Dragon Shard",
		"DisplayName": "神龙碎片",
		"ButtonImagePath": "BTN_DragonShard_ZH",
		"LapisId": "4257",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DragonShard"
	},
	{
		"DisplayEnName": "Dragonz",
		"DisplayName": "幸运龙宝贝",
		"ButtonImagePath": "BTN_Dragonz_sc",
		"LapisId": "1424",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Dragonz_en"
	},
	{
		"DisplayEnName": "Dream Date",
		"DisplayName": "梦幻邂逅",
		"ButtonImagePath": "BTN_DreamDate_ZH",
		"LapisId": "1948",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_DreamDate"
	},
	{
		"DisplayEnName": "Eagles Wings",
		"DisplayName": "雄鹰之翼",
		"ButtonImagePath": "BTN_EaglesWings1",
		"LapisId": "1236",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EaglesWings1"
	},
	{
		"DisplayEnName": "Egyptian Tombs",
		"DisplayName": "埃及古墓",
		"ButtonImagePath": "BTN_EgyptianTombs_ZH",
		"LapisId": "6417",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EgyptianTombs"
	},
	{
		"DisplayEnName": "Emerald Gold",
		"DisplayName": "金翠绿",
		"ButtonImagePath": "BTN_EmeraldGold_ZH",
		"LapisId": "6627",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmeraldGold"
	},
	{
		"DisplayEnName": "EmotiCoins",
		"DisplayName": "表情金币",
		"ButtonImagePath": "BTN_EmotiCoins_ZH",
		"LapisId": "1895",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Emoticoins"
	},
	{
		"DisplayEnName": "Emperor of The Sea",
		"DisplayName": "青龙出海",
		"ButtonImagePath": "BTN_EmperorOfTheSea_ZH",
		"LapisId": "1882",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmperorOfTheSea"
	},
	{
		"DisplayEnName": "Emperor of the Sea Deluxe",
		"DisplayName": "青龙出海豪华版",
		"ButtonImagePath": "BTN_EmperoroftheSeaDeluxe_ZH",
		"LapisId": "6253",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_EmperoroftheSeaDeluxe"
	},
	{
		"DisplayEnName": "Exotic Cats",
		"DisplayName": "异域狂兽",
		"ButtonImagePath": "BTN_ExoticCats_ZH",
		"LapisId": "2060",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ExoticCats "
	},
	{
		"DisplayEnName": "Fire Forge",
		"DisplayName": "烈火锻金",
		"ButtonImagePath": "BTN_FireForge_ZH",
		"LapisId": "6330",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FireForge"
	},
	{
		"DisplayEnName": "Fish Party",
		"DisplayName": "派对鱼",
		"ButtonImagePath": "BTN_FishParty",
		"LapisId": "1113",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FishParty"
	},
	{
		"DisplayEnName": "Flower Fortunes Megaways™",
		"DisplayName": "花的幸运Megaways",
		"ButtonImagePath": "BTN_FlowerFortunesMegaways_ZH",
		"LapisId": "6305",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FlowerFortunesMegaways"
	},
	{
		"DisplayEnName": "Football Star",
		"DisplayName": "足球明星",
		"ButtonImagePath": "BTN_footballstar1",
		"LapisId": "1186",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_footballstar1"
	},
	{
		"DisplayEnName": "Football Star Deluxe",
		"DisplayName": "足球明星豪华版",
		"ButtonImagePath": "BTN_FootballStarDeluxe_ZH",
		"LapisId": "5488",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FootballStarDeluxe"
	},
	{
		"DisplayEnName": "Forbidden Throne",
		"DisplayName": "禁忌王座",
		"ButtonImagePath": "BTN_ForbiddenThrone",
		"LapisId": "1887",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ForbiddenThrone"
	},
	{
		"DisplayEnName": "Forgotten Island Megaways™",
		"DisplayName": "遗忘之岛Megaways",
		"ButtonImagePath": "BTN_ForgottenIslandMegaways_ZH",
		"LapisId": "6298",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ForgottenIslandMegaways"
	},
	{
		"DisplayEnName": "Fortune Girl",
		"DisplayName": "金库甜心",
		"ButtonImagePath": "BTN_FortuneGirl_ZH",
		"LapisId": "1888",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FortuneGirl"
	},
	{
		"DisplayEnName": "Fortunium",
		"DisplayName": "财富之都",
		"ButtonImagePath": "BTN_Fortunium_ZH",
		"LapisId": "1993",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Fortunium"
	},
	{
		"DisplayEnName": "Fruit Blast",
		"DisplayName": "水果大爆发",
		"ButtonImagePath": "BTN_FruitBlast_ZH",
		"LapisId": "1943",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FruitBlast"
	},
	{
		"DisplayEnName": "Fruit Vs Candy",
		"DisplayName": "水果VS糖果",
		"ButtonImagePath": "BTN_FruitVsCandy_ZH",
		"LapisId": "1878",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_FruitVsCandy"
	},
	{
		"DisplayEnName": "Galaxy Glider",
		"DisplayName": "星际骑手",
		"ButtonImagePath": "BTN_GalaxyGlider_ZH",
		"LapisId": "5696",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GalaxyGlider"
	},
	{
		"DisplayEnName": "Gems & Dragons",
		"DisplayName": "魔龙宝珠",
		"ButtonImagePath": "BTN_Gems&Dragons_ZH",
		"LapisId": "6256",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Gems&Dragons"
	},
	{
		"DisplayEnName": "Gems Odyssey",
		"DisplayName": "宝石奥德赛",
		"ButtonImagePath": "BTN_GemsOdyssey_ZH",
		"LapisId": "1945",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GemsOdyssey"
	},
	{
		"DisplayEnName": "Giant Riches",
		"DisplayName": "巨人财富",
		"ButtonImagePath": "BTN_GiantRiches",
		"LapisId": "1955",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GiantRiches"
	},
	{
		"DisplayEnName": "Girls With Guns-L-Jungle Heat",
		"DisplayName": "美女枪手丛林激战",
		"ButtonImagePath": "BTN_GirlswithGuns1",
		"LapisId": "1313",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GirlswithGuns1"
	},
	{
		"DisplayEnName": "Gnome Wood",
		"DisplayName": "矮木头",
		"ButtonImagePath": "BTN_GnomeWood_ZH",
		"LapisId": "1900",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GnomeWood"
	},
	{
		"DisplayEnName": "Gold Collector",
		"DisplayName": "淘金者",
		"ButtonImagePath": "BTN_GoldCollector_ZH",
		"LapisId": "6333",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldCollector"
	},
	{
		"DisplayEnName": "Gold Factory",
		"DisplayName": "黄金工厂",
		"ButtonImagePath": "BTN_GoldFactory_ZH",
		"LapisId": "1267",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldFactory"
	},
	{
		"DisplayEnName": "Goldaur Guardians",
		"DisplayName": "黄金守卫",
		"ButtonImagePath": "BTN_GoldaurGuardians_ZH",
		"LapisId": "6214",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldaurGuardians"
	},
	{
		"DisplayEnName": "Golden Era",
		"DisplayName": "黄金时代",
		"ButtonImagePath": "BTN_GoldenEra",
		"LapisId": "1041",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenEra"
	},
	{
		"DisplayEnName": "Golden Princess",
		"DisplayName": "黄金公主",
		"ButtonImagePath": "BTN_GoldenPrincess",
		"LapisId": "1190",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenPrincess"
	},
	{
		"DisplayEnName": "Golden Stallion",
		"DisplayName": "金色骏马",
		"ButtonImagePath": "BTN_GoldenStallion_ZH",
		"LapisId": "6301",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GoldenStallion"
	},
	{
		"DisplayEnName": "Gopher Gold",
		"DisplayName": "黄金囊地鼠",
		"ButtonImagePath": "BTN_GopherGold2",
		"LapisId": "1216",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_GopherGold2"
	},
	{
		"DisplayEnName": "Halloween",
		"DisplayName": "万圣劫",
		"ButtonImagePath": "BTN_Halloween",
		"LapisId": "1904",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Halloween"
	},
	{
		"DisplayEnName": "Halloweenies",
		"DisplayName": "万圣节",
		"ButtonImagePath": "BTN_Halloweenies1",
		"LapisId": "1047",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Halloweenies1"
	},
	{
		"DisplayEnName": "Happy Holidays",
		"DisplayName": "快乐假日",
		"ButtonImagePath": "BTN_HappyHolidays_ZH",
		"LapisId": "1072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HappyHolidays"
	},
	{
		"DisplayEnName": "Happy Monster Claw",
		"DisplayName": "开心娃娃机",
		"ButtonImagePath": "BTN_HappyMonsterClaw_ZH",
		"LapisId": "2101",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HappyMonsterClaw"
	},
	{
		"DisplayEnName": "Harveys",
		"DisplayName": "哈维斯的晚餐",
		"ButtonImagePath": "BTN_Harveys1",
		"LapisId": "1139",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Harveys1"
	},
	{
		"DisplayEnName": "Hidden Palace Treasures",
		"DisplayName": "大藏金",
		"ButtonImagePath": "BTN_HiddenPalaceTreasures_ZH",
		"LapisId": "5479",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HiddenPalaceTreasures"
	},
	{
		"DisplayEnName": "Hidden Treasures of River Kwai",
		"DisplayName": "桂河藏宝",
		"ButtonImagePath": "BTN_HiddenTreasuresOfRiverKwai_ZH",
		"LapisId": "5531",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HiddenTreasuresOfRiverKwai"
	},
	{
		"DisplayEnName": "High Society",
		"DisplayName": "上流社会",
		"ButtonImagePath": "BTN_HighSociety",
		"LapisId": "1163",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HighSociety"
	},
	{
		"DisplayEnName": "Highlander",
		"DisplayName": "时空英豪",
		"ButtonImagePath": "BTN_Highlander_ZH",
		"LapisId": "1909",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Highlander"
	},
	{
		"DisplayEnName": "Hip Hop Thai",
		"DisplayName": "泰嘻哈",
		"ButtonImagePath": "BTN_HipHopThai_ZH",
		"LapisId": "6074",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HipHopThai"
	},
	{
		"DisplayEnName": "Hippie Days",
		"DisplayName": "嬉皮时光",
		"ButtonImagePath": "BTN_HippieDays_ZH",
		"LapisId": "6071",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HippieDays"
	},
	{
		"DisplayEnName": "HitMan",
		"DisplayName": "终极杀手",
		"ButtonImagePath": "BTN_Hitman1",
		"LapisId": "1321",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Hitman1"
	},
	{
		"DisplayEnName": "Holly Jolly Penguins",
		"DisplayName": "圣诞企鹅",
		"ButtonImagePath": "BTN_HollyJollyPenguins_ZH",
		"LapisId": "1910",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HollyJollyPenguins"
	},
	{
		"DisplayEnName": "Hound Hotel",
		"DisplayName": "酷犬酒店",
		"ButtonImagePath": "BTN_HoundHotel_ZH",
		"LapisId": "1063",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HoundHotel"
	},
	{
		"DisplayEnName": "Huangdi - The Yellow Emperor",
		"DisplayName": "轩辕帝传",
		"ButtonImagePath": "BTN_Huangdi_TheYellowEmperor_zh",
		"LapisId": "1849",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Huangdi_TheYellowEmperor_en"
	},
	{
		"DisplayEnName": "Hyper Strike™",
		"DisplayName": "究极闪电",
		"ButtonImagePath": "BTN_HyperStrike™_ZH",
		"LapisId": "6327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_HyperStrike™"
	},
	{
		"DisplayEnName": "Immortal Romance",
		"DisplayName": "不朽情缘",
		"ButtonImagePath": "BTN_ImmortalRomance1",
		"LapisId": "1103",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ImmortalRomance1"
	},
	{
		"DisplayEnName": "Incan Adventure",
		"DisplayName": "挖到宝",
		"ButtonImagePath": "BTN_IncanAdventure_ZH",
		"LapisId": "5489",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_IncanAdventure"
	},
	{
		"DisplayEnName": "Ingots of Cai Shen",
		"DisplayName": "财神元宝",
		"ButtonImagePath": "BTN_Ingotsof CaiShen_ZH",
		"LapisId": "6258",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Ingotsof CaiShen"
	},
	{
		"DisplayEnName": "Isis",
		"DisplayName": "埃及女神伊西絲",
		"ButtonImagePath": "BTN_isis",
		"LapisId": "1250",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_isis"
	},
	{
		"DisplayEnName": "Joker Poker",
		"DisplayName": "小丑扑克",
		"ButtonImagePath": "BTN_JokerPoker1",
		"LapisId": "1418",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JokerPoker1"
	},
	{
		"DisplayEnName": "Joyful Joker Megaways",
		"DisplayName": "欢乐小丑 Megaways",
		"ButtonImagePath": "BTN_JoyfulJokerMegaways_ZH",
		"LapisId": "6955",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JoyfulJokerMegaways"
	},
	{
		"DisplayEnName": "Jungle Jim and the Lost Sphinx",
		"DisplayName": "丛林吉姆与失落的狮身人面像",
		"ButtonImagePath": "BTN_JungleJimandtheLostSphinx_ZH",
		"LapisId": "4340",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JungleJimandtheLostSphinx"
	},
	{
		"DisplayEnName": "Jungle Jim EL Dorado",
		"DisplayName": "丛林吉姆-黄金国",
		"ButtonImagePath": "BTN_Junglejim_zh",
		"LapisId": "1244",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JungleJim"
	},
	{
		"DisplayEnName": "Jurassic World",
		"DisplayName": "侏罗纪世界",
		"ButtonImagePath": "BTN_JurassicWorld",
		"LapisId": "1891",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_JurassicWorld"
	},
	{
		"DisplayEnName": "Karaoke Party",
		"DisplayName": "K歌乐韵",
		"ButtonImagePath": "BTN_KaraokeParty_ZH",
		"LapisId": "1053",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KaraokeParty"
	},
	{
		"DisplayEnName": "Kathmandu",
		"DisplayName": "卡萨缦都",
		"ButtonImagePath": "BTN_Kathmandu1",
		"LapisId": "1151",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Kathmandu1"
	},
	{
		"DisplayEnName": "King of the Ring",
		"DisplayName": "擂台王者",
		"ButtonImagePath": "BTN_KingoftheRing_ZH",
		"LapisId": "5477",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingoftheRing"
	},
	{
		"DisplayEnName": "King Tusk",
		"DisplayName": "大象之王",
		"ButtonImagePath": "BTN_KingTusk_ZH",
		"LapisId": "1908",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingTusk"
	},
	{
		"DisplayEnName": "Kings of Cash",
		"DisplayName": "现金之王",
		"ButtonImagePath": "BTN_KingsofCash1",
		"LapisId": "1400",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KingsofCash1"
	},
	{
		"DisplayEnName": "Kitty Cabana",
		"DisplayName": "凯蒂卡巴拉",
		"ButtonImagePath": "BTN_KittyCabana_ZH",
		"LapisId": "1286",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_KittyCabana"
	},
	{
		"DisplayEnName": "Ladies Nite",
		"DisplayName": "女仕之夜",
		"ButtonImagePath": "BTN_LadiesNite5",
		"LapisId": "1389",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadiesNite5"
	},
	{
		"DisplayEnName": "Ladies Nite 2 Turn Wild",
		"DisplayName": "淑女之夜2狂野变身",
		"ButtonImagePath": "BTN_LadiesNite2TurnWild_ZH",
		"LapisId": "5487",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadiesNite2TurnWild"
	},
	{
		"DisplayEnName": "Lady in Red",
		"DisplayName": "红衣女郎",
		"ButtonImagePath": "BTN_LadyInRed2",
		"LapisId": "1124",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LadyInRed2"
	},
	{
		"DisplayEnName": "Lara Croft® Temples and Tombs™",
		"DisplayName": "萝拉神庙古墓",
		"ButtonImagePath": "BTN_LaraCroftTemplesandTombs_ZH",
		"LapisId": "4110",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LaraCroftTemplesandTombs"
	},
	{
		"DisplayEnName": "Legend of the Moon Lovers",
		"DisplayName": "奔月传说",
		"ButtonImagePath": "BTN_LegendoftheMoonLovers_ZH",
		"LapisId": "4295",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LegendoftheMoonLovers"
	},
	{
		"DisplayEnName": "Life of Riches",
		"DisplayName": "富裕人生",
		"ButtonImagePath": "BTN_LifeOfRiches_zh",
		"LapisId": "1851",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LifeOfRiches_en"
	},
	{
		"DisplayEnName": "Lion's Pride",
		"DisplayName": "狮子的骄傲",
		"ButtonImagePath": "BTN_lionsPride",
		"LapisId": "1049",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_lionsPride"
	},
	{
		"DisplayEnName": "Liquid Gold",
		"DisplayName": "液体黄金",
		"ButtonImagePath": "BTN_LiquidGold",
		"LapisId": "1756",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LiquidGold"
	},
	{
		"DisplayEnName": "Loaded",
		"DisplayName": "炫富一族",
		"ButtonImagePath": "BTN_Loaded1",
		"LapisId": "1245",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Loaded1"
	},
	{
		"DisplayEnName": "Long Mu Fortunes",
		"DisplayName": "龙母财富",
		"ButtonImagePath": "BTN_LongMuFortunes_ZH",
		"LapisId": "5365",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LongMuFortunes"
	},
	{
		"DisplayEnName": "Lost Vegas",
		"DisplayName": "迷失拉斯维加斯",
		"ButtonImagePath": "BTN_LostVegas_zh",
		"LapisId": "1420",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LostVegas"
	},
	{
		"DisplayEnName": "Lucha Legends",
		"DisplayName": "摔角传奇",
		"ButtonImagePath": "BTN_LuchaLegends_ZH",
		"LapisId": "2066",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuchaLegends"
	},
	{
		"DisplayEnName": "Lucky Bachelors",
		"DisplayName": "幸运单身族",
		"ButtonImagePath": "BTN_LuckyBachelors_ZH",
		"LapisId": "4339",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyBachelors"
	},
	{
		"DisplayEnName": "Lucky Firecracker",
		"DisplayName": "招财鞭炮",
		"ButtonImagePath": "BTN_LuckyFirecracker_ZH",
		"LapisId": "1126",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyFirecracker"
	},
	{
		"DisplayEnName": "Lucky Koi",
		"DisplayName": "幸运锦鲤",
		"ButtonImagePath": "BTN_LuckyKoi",
		"LapisId": "1060",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyKoi"
	},
	{
		"DisplayEnName": "Lucky Leprechaun",
		"DisplayName": "幸运的小妖精",
		"ButtonImagePath": "BTN_LuckyLeprechaun",
		"LapisId": "1212",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyLeprechaun"
	},
	{
		"DisplayEnName": "Lucky Little Gods",
		"DisplayName": "宝贝财神",
		"ButtonImagePath": "BTN_LuckyLittleGods_ZH",
		"LapisId": "1944",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyLittleGods"
	},
	{
		"DisplayEnName": "Lucky Riches Hyperspins",
		"DisplayName": "幸运富豪",
		"ButtonImagePath": "BTN_LuckyRichesHyperspins_ZH",
		"LapisId": "5620",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyRichesHyperspins"
	},
	{
		"DisplayEnName": "Lucky Silat",
		"DisplayName": "黄金对决",
		"ButtonImagePath": "BTN_LuckySilat_ZH",
		"LapisId": "5483",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckySilat"
	},
	{
		"DisplayEnName": "Lucky Thai Lanterns",
		"DisplayName": "泰幸运水灯",
		"ButtonImagePath": "BTN_LuckyThaiLanterns_ZH",
		"LapisId": "5698",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyThaiLanterns"
	},
	{
		"DisplayEnName": "Lucky Twins",
		"DisplayName": "幸运双星",
		"ButtonImagePath": "BTN_luckyTwins_ZH",
		"LapisId": "1283",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwins"
	},
	{
		"DisplayEnName": "Lucky Twins Catcher",
		"DisplayName": "幸运双星接财神",
		"ButtonImagePath": "BTN_LuckyTwinsCatcher_ZH",
		"LapisId": "6070",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwinsCatcher"
	},
	{
		"DisplayEnName": "Lucky Twins Jackpot",
		"DisplayName": "幸运双星大奖",
		"ButtonImagePath": "BTN_LuckyTwinsJackpot_ZH",
		"LapisId": "5142",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyTwinsJackpot"
	},
	{
		"DisplayEnName": "Lucky Zodiac",
		"DisplayName": "幸运生肖",
		"ButtonImagePath": "BTN_LuckyZodiac_ZH",
		"LapisId": "1273",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_LuckyZodiac"
	},
	{
		"DisplayEnName": "Mad Hatters",
		"DisplayName": "疯狂的帽子",
		"ButtonImagePath": "BTN_MadHatters1",
		"LapisId": "1314",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MadHatters1"
	},
	{
		"DisplayEnName": "Magic of Sahara",
		"DisplayName": "魔力撒哈拉",
		"ButtonImagePath": "BTN_MagicOfSahara_ZH",
		"LapisId": "4288",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MagicOfSahara"
	},
	{
		"DisplayEnName": "Makan Makan",
		"DisplayName": "食在印尼",
		"ButtonImagePath": "BTN_MakanMakan_ZH",
		"LapisId": "6188",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MakanMakan"
	},
	{
		"DisplayEnName": "Maui Mischief™",
		"DisplayName": "酷太酒店",
		"ButtonImagePath": "BTN_MauiMischief™_ZH",
		"LapisId": "6329",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MauiMischief™"
	},
	{
		"DisplayEnName": "Mayan Princess",
		"DisplayName": "玛雅公主",
		"ButtonImagePath": "BTN_MayanPrincess1",
		"LapisId": "1343",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MayanPrincess1"
	},
	{
		"DisplayEnName": "Mega Money Multiplier",
		"DisplayName": "巨额现金乘数",
		"ButtonImagePath": "BTN_MegaMoneyMultiplier",
		"LapisId": "1885",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MegaMoneyMultiplier"
	},
	{
		"DisplayEnName": "Mega Money Rush",
		"DisplayName": "巨款大冲击",
		"ButtonImagePath": "BTN_MegaMoneyRush_ZH",
		"LapisId": "1942",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MegaMoneyRush"
	},
	{
		"DisplayEnName": "Mermaids Millions",
		"DisplayName": "海底世界",
		"ButtonImagePath": "BTN_MermaidsMillions1",
		"LapisId": "1308",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MermaidsMillions1"
	},
	{
		"DisplayEnName": "Moby Dick",
		"DisplayName": "白鲸记",
		"ButtonImagePath": "BTN_MobyDick",
		"LapisId": "1905",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MobyDick"
	},
	{
		"DisplayEnName": "Monster Blast",
		"DisplayName": "怪物爆炸",
		"ButtonImagePath": "BTN_MonsterBlast_ZH",
		"LapisId": "5697",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MonsterBlast"
	},
	// {
	// 	"DisplayEnName": "Monster Wheels",
	// 	"DisplayName": "怪物赛车",
	// 	"ButtonImagePath": "BTN_MonsterWheels_ZH",
	// 	"LapisId": "1903",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_MonsterWheels"
	// },
	{
		"DisplayEnName": "Munchkins",
		"DisplayName": "怪兽曼琪肯",
		"ButtonImagePath": "BTN_Munchkins2",
		"LapisId": "1008",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Munchkins2"
	},
	{
		"DisplayEnName": "Mystic Dreams",
		"DisplayName": "神秘梦境",
		"ButtonImagePath": "BTN_MysticDreams1",
		"LapisId": "1254",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_MysticDreams1"
	},
	{
		"DisplayEnName": "Neptune's Riches: Ocean of Wilds",
		"DisplayName": "海王星宝藏狂野之海",
		"ButtonImagePath": "BTN_NeptunesRiches_ZH",
		"LapisId": "6217",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_NeptunesRiches"
	},
	{
		"DisplayEnName": "Noble Sky",
		"DisplayName": "宏伟苍穹",
		"ButtonImagePath": "BTN_NobleSky_ZH",
		"LapisId": "6073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_NobleSky"
	},
	{
		"DisplayEnName": "Odin's Riches",
		"DisplayName": "奥丁的财宝",
		"ButtonImagePath": "BTN_OdinsRiches_ZH",
		"LapisId": "6956",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OdinsRiches"
	},
	{
		"DisplayEnName": "Oink Country Love",
		"DisplayName": "呼撸撸爱上乡下",
		"ButtonImagePath": "BTN_OinkCountryLove_ZH",
		"LapisId": "1896",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OinkCountryLove"
	},
	{
		"DisplayEnName": "Oni Hunter",
		"DisplayName": "鬼狩",
		"ButtonImagePath": "BTN_OniHunter_ZH",
		"LapisId": "6328",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OniHunter"
	},
	{
		"DisplayEnName": "Oni Hunter Plus",
		"DisplayName": "鬼狩加強版",
		"ButtonImagePath": "BTN_OniHunterPlus_ZH",
		"LapisId": "6959",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OniHunterPlus"
	},
	{
		"DisplayEnName": "Our Days",
		"DisplayName": "有你的校园",
		"ButtonImagePath": "BTN_OurDays_ZH",
		"LapisId": "2073",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_OurDays"
	},
	{
		"DisplayEnName": "Party Island",
		"DisplayName": "派对岛",
		"ButtonImagePath": "BTN_PartyIsland.png",
		"LapisId": "1737",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PartyIsland.png"
	},
	{
		"DisplayEnName": "Pets Go Wild",
		"DisplayName": "狂野宠物",
		"ButtonImagePath": "BTN_PetsGoWild_ZH",
		"LapisId": "4298",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PetsGoWild"
	},
	{
		"DisplayEnName": "Pho Win",
		"DisplayName": "越南必赢",
		"ButtonImagePath": "BTN_PhoWin_ZH",
		"LapisId": "5478",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PhoWin"
	},
	{
		"DisplayEnName": "Ping Pong Star",
		"DisplayName": "乒乓巨星",
		"ButtonImagePath": "BTN_PingPongStar_ZH",
		"LapisId": "5144",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PingPongStar"
	},
	{
		"DisplayEnName": "Pistoleras",
		"DisplayName": "持枪王者",
		"ButtonImagePath": "BTN_Pistoleras_ZH",
		"LapisId": "1160",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Pistoleras"
	},
	{
		"DisplayEnName": "Playboy",
		"DisplayName": "花花公子",
		"ButtonImagePath": "BTN_Playboy",
		"LapisId": "1188",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Playboy"
	},
	{
		"DisplayEnName": "Playboy Fortunes",
		"DisplayName": "花花公子财富",
		"ButtonImagePath": "BTN_PlayboyFortunes_ZH",
		"LapisId": "5528",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyFortunes"
	},
	{
		"DisplayEnName": "Playboy Gold",
		"DisplayName": "黄金花花公子",
		"ButtonImagePath": "BTN_PlayboyGold_ZH",
		"LapisId": "1946",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyGold"
	},
	{
		"DisplayEnName": "Playboy™ Gold Jackpots",
		"DisplayName": "花花公子黄金大奖",
		"ButtonImagePath": "BTN_PlayboyGoldJackpots_ZH",
		"LapisId": "4327",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PlayboyGoldJackpots"
	},
	{
		"DisplayEnName": "Poke The Guy",
		"DisplayName": "进击的猿人",
		"ButtonImagePath": "BTN_PokeTheGuy_ZH",
		"LapisId": "1954",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PokeTheGuy"
	},
	{
		"DisplayEnName": "Pollen Party",
		"DisplayName": "花粉之国",
		"ButtonImagePath": "BTN_PollenParty_ZH",
		"LapisId": "1881",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PollenParty"
	},
	{
		"DisplayEnName": "Pretty Kitty",
		"DisplayName": "漂亮猫咪",
		"ButtonImagePath": "BTN_prettykitty_zh",
		"LapisId": "1045",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_prettykitty_en"
	},
	{
		"DisplayEnName": "Pure Platinum",
		"DisplayName": "纯铂",
		"ButtonImagePath": "BTN_PurePlatinum1",
		"LapisId": "1312",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_PurePlatinum1"
	},
	{
		"DisplayEnName": "Queen of Alexandria™",
		"DisplayName": "亚历山大女王",
		"ButtonImagePath": "BTN_QueenofAlexandria_ZH",
		"LapisId": "6748",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_QueenofAlexandria"
	},
	{
		"DisplayEnName": "Queen of the Crystal Rays",
		"DisplayName": "水晶女皇",
		"ButtonImagePath": "BTN_QueenOfTheCrystalRays",
		"LapisId": "4289",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_QueenOfTheCrystalRays"
	},
	{
		"DisplayEnName": "Rabbit in the Hat",
		"DisplayName": "帽子里的兔子",
		"ButtonImagePath": "BTN_RabbitintheHat",
		"LapisId": "1275",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RabbitintheHat"
	},
	{
		"DisplayEnName": "Reel Gems",
		"DisplayName": "宝石转轴",
		"ButtonImagePath": "BTN_ReelGems1",
		"LapisId": "1207",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelGems1"
	},
	{
		"DisplayEnName": "Reel Gems Deluxe",
		"DisplayName": "宝石转轴豪华版",
		"ButtonImagePath": "BTN_ReelGemsDeluxe_ZH",
		"LapisId": "6069",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelGemsDeluxe"
	},
	{
		"DisplayEnName": "Reel Spinner",
		"DisplayName": "旋转大战",
		"ButtonImagePath": "BTN_ReelSpinner_zh",
		"LapisId": "1294",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelSpinner_en"
	},
	{
		"DisplayEnName": "Reel Strike",
		"DisplayName": "卷行使价",
		"ButtonImagePath": "BTN_ReelStrike1",
		"LapisId": "1157",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelStrike1"
	},
	// {
	// 	"DisplayEnName": "Reel Talent",
	// 	"DisplayName": "真正高手",
	// 	"ButtonImagePath": "BTN_ReelTalent_ZH",
	// 	"LapisId": "2070",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_ReelTalent"
	// },
	{
		"DisplayEnName": "Reel Thunder",
		"DisplayName": "雷电击",
		"ButtonImagePath": "BTN_ReelThunder2",
		"LapisId": "1293",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ReelThunder2"
	},
	{
		"DisplayEnName": "Relic Seekers",
		"DisplayName": "探陵人",
		"ButtonImagePath": "BTN_RelicSeekers_ZH",
		"LapisId": "4109",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RelicSeekers"
	},
	{
		"DisplayEnName": "Retro Reels",
		"DisplayName": "复古旋转",
		"ButtonImagePath": "BTN_RetroReels1",
		"LapisId": "1110",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RetroReels1"
	},
	{
		"DisplayEnName": "Retro Reels - Extreme Heat",
		"DisplayName": "复古卷轴-极热",
		"ButtonImagePath": "BTN_RRExtreme1",
		"LapisId": "1189",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRExtreme1"
	},
	{
		"DisplayEnName": "Retro Reels Diamond Glitz",
		"DisplayName": "复古卷轴钻石耀眼",
		"ButtonImagePath": "BTN_RRDiamondGlitz1",
		"LapisId": "1100",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRDiamondGlitz1"
	},
	{
		"DisplayEnName": "Rhyming Reels - Georgie Porgie",
		"DisplayName": "押韵的卷轴-乔治波尔吉",
		"ButtonImagePath": "BTN_RhymingReelsGeorgiePorgie",
		"LapisId": "1782",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RhymingReelsGeorgiePorgie"
	},
	{
		"DisplayEnName": "Rhyming Reels - Hearts & Tarts",
		"DisplayName": "童话转轴红心皇后",
		"ButtonImagePath": "BTN_RRHearts&Tarts1",
		"LapisId": "1009",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RRHearts&Tarts1"
	},
	{
		"DisplayEnName": "Riviera Riches",
		"DisplayName": "海滨财富",
		"ButtonImagePath": "BTN_RivieraRiches1",
		"LapisId": "1359",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RivieraRiches1"
	},
	// {
	// 	"DisplayEnName": "Robin of Sherwood",
	// 	"DisplayName": "雪伍德的罗宾汉",
	// 	"ButtonImagePath": "BTN_RobinofSherwood",
	// 	"LapisId": "1994",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_RobinofSherwood"
	// },
	{
		"DisplayEnName": "Romanov Riches",
		"DisplayName": "罗曼诺夫财富",
		"ButtonImagePath": "BTN_RomanovRiches_ZH",
		"LapisId": "2068",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RomanovRiches"
	},
	{
		"DisplayEnName": "Rugby Star",
		"DisplayName": "橄榄球明星",
		"ButtonImagePath": "BTN_RugbyStar_ZH",
		"LapisId": "1287",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RugbyStar"
	},
	{
		"DisplayEnName": "Rugby Star Deluxe",
		"DisplayName": "橄榄球明星豪华版",
		"ButtonImagePath": "BTN_RugbyStarDeluxe_ZH",
		"LapisId": "5447",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RugbyStarDeluxe"
	},
	{
		"DisplayEnName": "Santa Paws",
		"DisplayName": "圣诞之掌",
		"ButtonImagePath": "BTN_SantaPaws",
		"LapisId": "1785",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SantaPaws"
	},
	{
		"DisplayEnName": "Santas Wild Ride",
		"DisplayName": "圣诞老人的百搭摩拖车",
		"ButtonImagePath": "BTN_SantasWildRide1_ZH",
		"LapisId": "1095",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SantasWildRide1"
	},
	{
		"DisplayEnName": "Scrooge",
		"DisplayName": "小气财神",
		"ButtonImagePath": "BTN_Scrooge1_ZH",
		"LapisId": "1030",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Scrooge1"
	},
	{
		"DisplayEnName": "Secret Admirer",
		"DisplayName": "秘密崇拜者",
		"ButtonImagePath": "BTN_SecretAdmirer1",
		"LapisId": "1325",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SecretAdmirer1"
	},
	{
		"DisplayEnName": "Secret Romance",
		"DisplayName": "秘密爱慕者",
		"ButtonImagePath": "BTN_SecretRomance_1_ZH",
		"LapisId": "1877",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SecretRomance_en"
	},
	// {
	// 	"DisplayEnName": "Secrets of Sengoku",
	// 	"DisplayName": "战国奥义",
	// 	"ButtonImagePath": "BTN_SecretsofSengoku_ZH",
	// 	"LapisId": "5486",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_SecretsofSengoku"
	// },
	{
		"DisplayEnName": "Serengeti Gold",
		"DisplayName": "塞伦盖蒂黄金",
		"ButtonImagePath": "BTN_SerengetiGold_ZH",
		"LapisId": "6628",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SerengetiGold"
	},
	{
		"DisplayEnName": "Shamrock Holmes",
		"DisplayName": "三叶草福尔摩斯",
		"ButtonImagePath": "BTN_ShamrockHolmes_ZH",
		"LapisId": "6252",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShamrockHolmes"
	},
	{
		"DisplayEnName": "Shanghai Beauty",
		"DisplayName": "上海美人",
		"ButtonImagePath": "BTN_ShanghaiBeauty",
		"LapisId": "1421",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShanghaiBeauty"
	},
	{
		"DisplayEnName": "Sherlock of London",
		"DisplayName": "伦敦夏洛克",
		"ButtonImagePath": "BTN_SherlockofLondon",
		"LapisId": "4287",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SherlockofLondon"
	},
	// {
	// 	"DisplayEnName": "Shogun Of Time",
	// 	"DisplayName": "时界门之将军",
	// 	"ButtonImagePath": "BTN_ShogunOfTime_ZH",
	// 	"LapisId": "2086",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_ShogunOfTime"
	// },
	{
		"DisplayEnName": "Showdown Saloon",
		"DisplayName": "对决沙龙",
		"ButtonImagePath": "BTN_ShowdownSaloon_ZH",
		"LapisId": "2077",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ShowdownSaloon"
	},
	{
		"DisplayEnName": "Silver Fang",
		"DisplayName": "银芳",
		"ButtonImagePath": "BTN_SilverFang1",
		"LapisId": "1062",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverFang1"
	},
	{
		"DisplayEnName": "Silver Lioness 4x",
		"DisplayName": "纯银母狮4x",
		"ButtonImagePath": "BTN_SilverLioness4x",
		"LapisId": "4112",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverLioness4x"
	},
	{
		"DisplayEnName": "Silver Seas",
		"DisplayName": "银色之海",
		"ButtonImagePath": "BTN_SilverSeas_ZH",
		"LapisId": "6954",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverSeas"
	},
	{
		"DisplayEnName": "Silverback: Multiplier Mountain",
		"DisplayName": "银背乘数山",
		"ButtonImagePath": "BTN_SilverbackMultiplierMountain_ZH",
		"LapisId": "6255",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SilverbackMultiplierMountain"
	},
	{
		"DisplayEnName": "Sisters of Oz Jackpots",
		"DisplayName": "Oz姊妹累积奖金",
		"ButtonImagePath": "BTN_SistersofOzJackpots_ZH",
		"LapisId": "5925",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SistersofOzJackpots"
	},
	{
		"DisplayEnName": "Six Acrobats",
		"DisplayName": "杂技群英会",
		"ButtonImagePath": "BTN_SixAcrobats_ZH",
		"LapisId": "1892",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SixAcrobats"
	},
	{
		"DisplayEnName": "So Many Monsters",
		"DisplayName": "怪兽多多",
		"ButtonImagePath": "BTN_somanymonsters",
		"LapisId": "1001",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somanymonsters"
	},
	{
		"DisplayEnName": "So Much Candy",
		"DisplayName": "糖果多多",
		"ButtonImagePath": "BTN_somuchcandy",
		"LapisId": "1374",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somuchcandy"
	},
	{
		"DisplayEnName": "So Much Sushi",
		"DisplayName": "寿司多多",
		"ButtonImagePath": "BTN_somuchsushi",
		"LapisId": "1032",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_somuchsushi"
	},
	{
		"DisplayEnName": "Solar Wilds",
		"DisplayName": "太阳系百搭符号",
		"ButtonImagePath": "BTN_SolarWilds_ZH",
		"LapisId": "6331",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SolarWilds"
	},
	{
		"DisplayEnName": "Songkran Party",
		"DisplayName": "狂欢泼水节",
		"ButtonImagePath": "BTN_SongkranParty_ZH",
		"LapisId": "5480",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SongkranParty"
	},
	{
		"DisplayEnName": "Spring Break",
		"DisplayName": "春假",
		"ButtonImagePath": "BTN_SpringBreak2",
		"LapisId": "1002",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SpringBreak2"
	},
	{
		"DisplayEnName": "Stardust",
		"DisplayName": "星尘",
		"ButtonImagePath": "BTN_StarDust_ZH",
		"LapisId": "1404",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StarDust"
	},
	{
		"DisplayEnName": "Starlight Kiss",
		"DisplayName": "星梦之吻",
		"ButtonImagePath": "BTN_StarlightKiss",
		"LapisId": "1167",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StarlightKiss"
	},
	{
		"DisplayEnName": "Stash of the Titans",
		"DisplayName": "泰坦之藏匿",
		"ButtonImagePath": "BTN_StashoftheTitans1",
		"LapisId": "1320",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_StashoftheTitans1"
	},
	{
		"DisplayEnName": "Sterling Silver",
		"DisplayName": "纯银",
		"ButtonImagePath": "BTN_SterlingSilver3D1",
		"LapisId": "1170",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SterlingSilver3D1"
	},
	{
		"DisplayEnName": "Sugar Parade",
		"DisplayName": "糖果巡游",
		"ButtonImagePath": "BTN_SugarParade_ZH",
		"LapisId": "1893",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SugarParade"
	},
	{
		"DisplayEnName": "Summer Holiday",
		"DisplayName": "暑假",
		"ButtonImagePath": "BTN_SummerHoliday",
		"LapisId": "1784",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SummerHoliday"
	},
	{
		"DisplayEnName": "Summertime",
		"DisplayName": "夏天",
		"ButtonImagePath": "BTN_summertime",
		"LapisId": "1130",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_summertime"
	},
	{
		"DisplayEnName": "SunQuest",
		"DisplayName": "探索太阳",
		"ButtonImagePath": "BTN_SunQuest3",
		"LapisId": "1127",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SunQuest3"
	},
	{
		"DisplayEnName": "SunTide",
		"DisplayName": "太阳征程",
		"ButtonImagePath": "BTN_SunTide_Button_ZH",
		"LapisId": "1150",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SunTide_Button"
	},
	{
		"DisplayEnName": "Supe it Up",
		"DisplayName": "跑起来",
		"ButtonImagePath": "BTN_SupeItUp2",
		"LapisId": "1289",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SupeItUp2"
	},
	{
		"DisplayEnName": "Sure Win",
		"DisplayName": "必胜",
		"ButtonImagePath": "BTN_SureWin",
		"LapisId": "1711",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_SureWin"
	},
	{
		"DisplayEnName": "Tally Ho",
		"DisplayName": "泰利嗬",
		"ButtonImagePath": "BTN_TallyHo1",
		"LapisId": "1395",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TallyHo1"
	},
	// {
	// 	"DisplayEnName": "Tarzan",
	// 	"DisplayName": "泰山",
	// 	"ButtonImagePath": "BTN_Tarzan_Button_en",
	// 	"LapisId": "1847",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_Tarzan_Button_en"
	// },
	{
		"DisplayEnName": "Tarzan® and the Jewels of Opar ™",
		"DisplayName": "Tarzan® 和欧巴珠宝",
		"ButtonImagePath": "BTN_TarzanandtheJewelsofOpar_ZH",
		"LapisId": "6185",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TarzanandtheJewelsofOpar"
	},
	{
		"DisplayEnName": "Tasty Street",
		"DisplayName": "妹妹很饿",
		"ButtonImagePath": "BTN_TastyStreet_ZH",
		"LapisId": "1901",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TastyStreet"
	},
	{
		"DisplayEnName": "The Finer Reels of Life",
		"DisplayName": "至尊人生",
		"ButtonImagePath": "BTN_TheFinerReelsOfLife_ZH",
		"LapisId": "1200",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheFinerReelsOfLife"
	},
	{
		"DisplayEnName": "The Gold Swallow",
		"DisplayName": "金燕报恩",
		"ButtonImagePath": "BTN_TheGoldSwallow_ZH",
		"LapisId": "6075",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGoldSwallow"
	},
	{
		"DisplayEnName": "The Golden Mask Dance",
		"DisplayName": "黄金假面舞",
		"ButtonImagePath": "BTN_TheGoldenMaskDance_ZH",
		"LapisId": "5699",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGoldenMaskDance"
	},
	{
		"DisplayEnName": "The Great Albini",
		"DisplayName": "伟大魔术师",
		"ButtonImagePath": "BTN_TheGreatAlbini",
		"LapisId": "2087",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheGreatAlbini"
	},
	{
		"DisplayEnName": "The Heat Is On",
		"DisplayName": "热力四射",
		"ButtonImagePath": "BTN_TheHeatIsOn",
		"LapisId": "1883",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheHeatIsOn"
	},
	{
		"DisplayEnName": "The Immortal Sumo",
		"DisplayName": "永远的横纲",
		"ButtonImagePath": "BTN_TheImmortalSumo_ZH",
		"LapisId": "5530",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheImmortalSumo"
	},
	{
		"DisplayEnName": "The Incredible Balloon Machine",
		"DisplayName": "不可思议的气球机",
		"ButtonImagePath": "BTN_TheIncredibleBalloonMachine_ZH",
		"LapisId": "5448",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TheIncredibleBalloonMachine"
	},
	{
		"DisplayEnName": "The Phantom Of The Opera",
		"DisplayName": "歌剧魅影",
		"ButtonImagePath": "BTN_ThePhantomOfTheOpera_ZH",
		"LapisId": "1906",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThePhantomOfTheOpera"
	},
	{
		"DisplayEnName": "The Rat Pack",
		"DisplayName": "鼠帮",
		"ButtonImagePath": "BTN_RatPack1",
		"LapisId": "1398",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_RatPack1"
	},
	{
		"DisplayEnName": "The Twisted Circus",
		"DisplayName": "奇妙马戏团",
		"ButtonImagePath": "BTN_TwistedCircus",
		"LapisId": "1386",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TwistedCircus"
	},
	{
		"DisplayEnName": "Thunderstruck",
		"DisplayName": "雷神",
		"ButtonImagePath": "BTN_Thunderstruck1",
		"LapisId": "1028",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_Thunderstruck1"
	},
	{
		"DisplayEnName": "Thunderstruck II",
		"DisplayName": "雷神2",
		"ButtonImagePath": "BTN_ThunderstruckTwo1",
		"LapisId": "1330",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThunderstruckTwo1"
	},
	{
		"DisplayEnName": "Thunderstruck Wild Lightning",
		"DisplayName": "雷霆万钧之百搭闪电",
		"ButtonImagePath": "BTN_ThunderstruckWildLightning_ZH",
		"LapisId": "6988",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ThunderstruckWildLightning"
	},
	{
		"DisplayEnName": "Tigers Eye",
		"DisplayName": "虎眼",
		"ButtonImagePath": "BTN_tigersEye",
		"LapisId": "1232",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_tigersEye"
	},
	{
		"DisplayEnName": "Tiki Reward",
		"DisplayName": "提基奖励",
		"ButtonImagePath": "BTN_TikiReward_ZH",
		"LapisId": "6216",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TikiReward"
	},
	{
		"DisplayEnName": "Tiki Vikings™",
		"DisplayName": "蒂基维京",
		"ButtonImagePath": "BTN_TikiVikings™_ZH",
		"LapisId": "4290",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TikiVikings™"
	},
	{
		"DisplayEnName": "Titans of the Sun Hyperion",
		"DisplayName": "太阳神之许珀里翁",
		"ButtonImagePath": "BTN_TitansOfTheSunHyperion_ZH",
		"LapisId": "1208",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TitansOfTheSunHyperion"
	},
	{
		"DisplayEnName": "Titans of the Sun Theia",
		"DisplayName": "太阳神之忒伊亚",
		"ButtonImagePath": "BTN_TitansOfTheSunTheia_ZH",
		"LapisId": "1385",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TitansOfTheSunTheia"
	},
	{
		"DisplayEnName": "Tomb Raider",
		"DisplayName": "古墓丽影",
		"ButtonImagePath": "BTN_TombRaider2",
		"LapisId": "1122",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TombRaider2"
	},
	{
		"DisplayEnName": "Tomb Raider Secret of the Sword",
		"DisplayName": "古墓丽影秘密之剑",
		"ButtonImagePath": "BTN_TombRaiderSotS1",
		"LapisId": "1383",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TombRaiderSotS1"
	},
	{
		"DisplayEnName": "Treasure Dash",
		"DisplayName": "逃宝",
		"ButtonImagePath": "BTN_TreasureDash_ZH",
		"LapisId": "5143",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasureDash"
	},
	{
		"DisplayEnName": "Treasure Palace",
		"DisplayName": "财宝宫殿",
		"ButtonImagePath": "BTN_TreasurePalace",
		"LapisId": "1020",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasurePalace"
	},
	{
		"DisplayEnName": "Treasures of Lion City",
		"DisplayName": "海底宝城",
		"ButtonImagePath": "BTN_TreasuresOfLionCity_ZH",
		"LapisId": "4286",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_TreasuresOfLionCity"
	},
	{
		"DisplayEnName": "Untamed Giant Panda",
		"DisplayName": "大熊猫",
		"ButtonImagePath": "BTN_UntamedGiantPanda",
		"LapisId": "1222",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_UntamedGiantPanda"
	},
	// {
	// 	"DisplayEnName": "Village People® Macho Moves",
	// 	"DisplayName": "型男舞步",
	// 	"ButtonImagePath": "BTN_VillagePeople®MachoMoves_ZH",
	// 	"LapisId": "4271",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_VillagePeople®MachoMoves"
	// },
	{
		"DisplayEnName": "Vinyl Countdown",
		"DisplayName": "黑胶热舞",
		"ButtonImagePath": "BTN_VinylCountdown4_ZH",
		"LapisId": "1306",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_VinylCountdown4"
	},
	{
		"DisplayEnName": "Voila!",
		"DisplayName": "瞧！",
		"ButtonImagePath": "BTN_voila_2",
		"LapisId": "1241",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_voila_2"
	},
	{
		"DisplayEnName": "Wacky Panda",
		"DisplayName": "囧囧熊猫",
		"ButtonImagePath": "BTN_WackyPanda_ZH",
		"LapisId": "1911",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WackyPanda"
	},
	{
		"DisplayEnName": "Wanted Outlaws",
		"DisplayName": "歹徒通缉令",
		"ButtonImagePath": "BTN_WantedOutlaws_ZH",
		"LapisId": "6187",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WantedOutlaws"
	},
	{
		"DisplayEnName": "Western Gold",
		"DisplayName": "西域黄金",
		"ButtonImagePath": "BTN_WesternGold_ZH",
		"LapisId": "6072",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WesternGold"
	},
	{
		"DisplayEnName": "What A Hoot",
		"DisplayName": "猫头鹰乐园",
		"ButtonImagePath": "BTN_WhataHoot3",
		"LapisId": "1171",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WhataHoot3"
	},
	// {
	// 	"DisplayEnName": "Wicked Tales Dark Red",
	// 	"DisplayName": "黑暗故事神秘深红",
	// 	"ButtonImagePath": "BTN_WickedTalesDarkRed",
	// 	"LapisId": "2064",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WickedTalesDarkRed"
	// },
	{
		"DisplayEnName": "Wild Catch",
		"DisplayName": "荒野垂钓",
		"ButtonImagePath": "BTN_WildCatch_ZH",
		"LapisId": "1152",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WildCatch"
	},
	{
		"DisplayEnName": "Wild Orient",
		"DisplayName": "东方珍兽",
		"ButtonImagePath": "BTN_WildOrient_Button_ZH",
		"LapisId": "1164",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WildOrient_Button"
	},
	// {
	// 	"DisplayEnName": "Wild Scarabs",
	// 	"DisplayName": "百搭圣甲虫",
	// 	"ButtonImagePath": "BTN_WildScarabs_ZH",
	// 	"LapisId": "1992",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WildScarabs"
	// },
	// {
	// 	"DisplayEnName": "Win Sum Dim Sum",
	// 	"DisplayName": "开心点心",
	// 	"ButtonImagePath": "BTN_WinSumDimSum_zh",
	// 	"LapisId": "1345",
	// 	"typeid": "8",
	// 	"ButtonImagePathEn": "BTN_WinSumDimSum_en"
	// },
	{
		"DisplayEnName": "WP Forest Party JP",
		"DisplayName": "王牌森林舞会JP版",
		"ButtonImagePath": "BTN_WPForest Party_ZH",
		"LapisId": "5705",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_WPForest Party"
	},
	{
		"DisplayEnName": "Zombie Hoard",
		"DisplayName": "丧尸来袭",
		"ButtonImagePath": "BTN_ZombieHoard",
		"LapisId": "2085",
		"typeid": "8",
		"ButtonImagePathEn": "BTN_ZombieHoard"
	}
]

var mgNewGame = [{
	"DisplayName": "香蕉奥德赛",
	"DisplayEnName": "Banana Odyssey",
	"ButtonImagePath": "Banana_Odyssey_ZH.png",
	"LapisId": "SMG_bananaOdyssey",
	"typeid":"8"
},{
	"DisplayName": "再抢银行再旋转",
	"DisplayEnName": "Break da Bank Again Respin",
	"ButtonImagePath": "BreakDaBankAgainRespin.png",
	"LapisId": "SMG_breakDaBankAgainRespin",
	"typeid":"8"
},{
	"DisplayName": "奔月传说",
	"DisplayEnName": "Legend of the Moon Lovers",
	"ButtonImagePath": "LegendOfTheMoonLovers_Button_ZH.png",
	"LapisId": "SMG_LegendOftheMoonLovers",
	"typeid":"8"
},{
	"DisplayName": "帽中的兔子",
	"DisplayEnName": "Rabbit In The Hat",
	"ButtonImagePath": "RabbitInTheHat_GameButton.png",
	"LapisId": "SMG_rabbitinthehat",
	"typeid":"8"
},{
	"DisplayName": "魔力撒哈拉",
	"DisplayEnName": "Magic of Sahara",
	"ButtonImagePath": "MagicOfSahara_Button_Rollover_zh.png",
	"LapisId": "SMG_magicOfSahara",
	"typeid":"8"
},{
	"DisplayName": "伦敦的夏洛克",
	"DisplayEnName": "Sherlock Of London Online Slot",
	"ButtonImagePath": "Sherlock_of_London_ZH.png",
	"LapisId": "SMG_sherlockOfLondonOnlineSlot",
	"typeid":"8"
},{
	"DisplayName": "蒂基维京",
	"DisplayEnName": "Tiki Vikings",
	"ButtonImagePath": "TikiVikings_BTN_Rollover_ZH.png",
	"LapisId": "SMG_tikiVikings",
	"typeid":"8"
},{
	"DisplayName": "海底宝城",
	"DisplayEnName": "Treasures of Lion City",
	"ButtonImagePath": "TreasuresOfLionCity_BTN_Rollover_zh.png",
	"LapisId": "SMG_treasuresOfLionCity",
	"typeid":"8"
},{
	"DisplayName": "水晶射线女王",
	"DisplayEnName": "Queen of the Crystal Rays",
	"ButtonImagePath": "QueenOfTheCrystalRays_BTN_Rollover.png",
	"LapisId": "SMG_queenOfTheCrystalRays",
	"typeid":"8"
},{
	"DisplayName": "狂野宠物",
	"DisplayEnName": "Pets Go Wild",
	"ButtonImagePath": "PetsGoWild_BTN_Rollover.png",
	"LapisId": "SMG_petsGoWild",
	"typeid":"8"
},{
	"DisplayName": "型男舞步",
	"DisplayEnName": "Village People Macho Moves",
	"ButtonImagePath": "VillagePeople_BTN_Rollover_ZH.png",
	"LapisId": "SMG_villagePeople",
	"typeid":"8"
},{
		"DisplayName": "篮球巨星豪华版",
		"DisplayEnName": "Basketball Star Deluxe",
		"ButtonImagePath": "BasketballStar-Deluxe_BTN_Rollover_zh.png",
		"LapisId": "SMG_basketballStarDeluxe",
		"typeid":"8"
	},{
		"DisplayName": "神龙碎片",
		"DisplayEnName": "Dragon Shard",
		"ButtonImagePath": "DragonShard_BTN_Rollover_ZH.png",
		"LapisId": "SMG_dragonShard",
		"typeid":"8"
	},{
		"DisplayName": "Zeus古代财富",
		"DisplayEnName": "Ancient Fortunes: Zeus",
		"ButtonImagePath": "AncientFortunesZeus_BTN_Rollover_ZH.png",
		"LapisId": "SMG_ancientFortunesZeus",
		"typeid":"8"
	},{
		"DisplayName": "罗拉卡芙特之神庙古墓",
		"DisplayEnName": "Lara Croft: Temples and Tombs",
		"ButtonImagePath": "LaraCroftTemplesAndTombs_BTN_Rollover_ZH.png",
		"LapisId": "SMG_laraCroftTemplesAndTombs",
		"typeid":"8"
	},{
		"DisplayName": "探陵人",
		"DisplayEnName": "Relic Seekers",
		"ButtonImagePath": "RelicSeekers_BTN_Rollover_zh.png",
		"LapisId": "SMG_relicSeekers",
		"typeid":"8"
	},{
		"DisplayName": "银色雌狮4x",
		"DisplayEnName": "Silver Lioness 4x",
		"ButtonImagePath": "SilverLioness4x_Button.png",
		"LapisId": "SMG_silverLioness4x",
		"typeid":"8"
	},{
		"DisplayName": "特工简.布隆德归来",
		"DisplayEnName": "Agent Jane Blonde Returns",
		"ButtonImagePath": "AJBR_BTN_Rollover_ZH.png",
		"LapisId": "SMG_agentjaneblondereturns",
		"typeid":"8"
	},{
		"DisplayName": "好运经纪人",
		"DisplayEnName": "Bookie of Odds",
		"ButtonImagePath": "BookieOfOdds_BTN_Rollover_ZH.png",
		"LapisId": "SMG_bookieOfOdds",
		"typeid":"8"
	},{
		"DisplayName": "时界门之将军",
		"DisplayEnName": "Shogun of Time",
		"ButtonImagePath": "ShogunOfTime_BTN_Rollover_ZH.png",
		"LapisId": "SMG_shogunofTime",
		"typeid":"8"
	},{
		"DisplayName": "伟大魔术师",
		"DisplayEnName": "The Great Albini",
		"ButtonImagePath": "TheGreatAlbini_BTN_Rollover_ZH.png",
		"LapisId": "SMG_theGreatAlbini",
		"typeid":"8"
	},{
		"DisplayName": "丧尸来袭",
		"DisplayEnName": "Zombie Hoard",
		"ButtonImagePath": "ZombieHoard_Button_Rollover_ZH.png",
		"LapisId": "SMG_zombieHoard",
		"typeid":"8"
	},{
		"DisplayName": "开心娃娃机",
		"DisplayEnName": "Happy Monster Claw",
		"ButtonImagePath": "HappyMonsterClaw_BTN_Rollover_ZH.png",
		"LapisId": "SMG_happyMonsterClaw",
		"typeid":"8"
	},{
	    "DisplayName": "囧囧熊猫",
	    "DisplayEnName": "Wacky Panda",
	    "ButtonImagePath": "BTN_WackyPanda_ZH.png",
	    "LapisId": "SMG_wackyPanda",
	    "typeid":"8"
	},{
	    "DisplayName": "篮球巨星",
	    "DisplayEnName": "Basketball Star",
	    "ButtonImagePath": "BTN_BasketballStar_ZH.png",
	    "LapisId": "SMG_basketballStar",
	    "typeid":"8"
	}, {
	    "DisplayName": "猫头鹰乐园",
	    "DisplayEnName": "What A Hoot",
	    "ButtonImagePath": "BTN_WhataHoot3.png",
	    "LapisId": "SMG_whatAHoot",
	    "typeid":"8"
	},{
	    "DisplayName": "东方珍兽",
	    "DisplayEnName": "Wild Orient",
	    "ButtonImagePath": "BTN_WildOrient_Button_ZH.png",
	    "LapisId": "SMG_wildOrient",
	    "typeid":"8"
	},{
	    "DisplayName": "歌剧魅影",
	    "DisplayEnName": "The Phantom Of The Opera",
	    "ButtonImagePath": "BTN_ThePhantomOfTheOpera_ZH.png",
	    "LapisId": "SMG_thePhantomOfTheOpera",
	    "typeid":"8"
	}, {
	    "DisplayName": "热力四射",
	    "DisplayEnName": "The Heat Is On",
	    "ButtonImagePath": "BTN_TheHeatIsOn.png",
	    "LapisId": "SMG_theHeatIsOn",
	    "typeid":"8"
	}, {
	    "DisplayName": "妹妹很饿",
	    "DisplayEnName": "Tasty Street",
	    "ButtonImagePath": "BTN_TastyStreet_ZH.png",
	    "LapisId": "SMG_tastyStreet",
	    "typeid":"8"
	},  {
	    "DisplayName": "泰山",
	    "DisplayEnName": "Tarzan",
	    "ButtonImagePath": "Tarzan_Button_en.png",
	    "LapisId": "SMG_tarzan",
	    "typeid":"8"
	},{
	    "DisplayName": "糖果巡游",
	    "DisplayEnName": "Sugar Parade",
	    "ButtonImagePath": "BTN_SugarParade_ZH.png",
	    "LapisId": "SMG_sugarParade",
	    "typeid":"8"
	}, {
	    "DisplayName": "星尘",
	    "DisplayEnName": "Stardust",
	    "ButtonImagePath": "BTN_StarDust_ZH.png",
	    "LapisId": "SMG_stardust",
	    "typeid":"8"
	}, {
	    "DisplayName": "杂技群英会",
	    "DisplayEnName": "Six Acrobats",
	    "ButtonImagePath": "BTN_SixAcrobats_ZH.png",
	    "LapisId": "SMG_sixAcrobats",
	    "typeid":"8"
	}, {
	    "DisplayName": "上海美人",
	    "DisplayEnName": "Shanghai Beauty",
	    "ButtonImagePath": "BTN_ShanghaiBeauty.png",
	    "LapisId": "SMG_shanghaiBeauty",
	    "typeid":"8"
	}, {
	    "DisplayName": "秘密爱慕者",
	    "DisplayEnName": "Secret Romance",
	    "ButtonImagePath": "BTN_SecretRomance_1_ZH.png",
	    "LapisId": "SMG_secretRomance",
	    "typeid":"8"
	}, {
	    "DisplayName": "押韵的卷轴-心挞",
	    "DisplayEnName": "Rhyming Reels - Hearts & Tarts",
	    "ButtonImagePath": "BTN_RRHearts_Tarts1.png",
	    "LapisId": "SMG_retroReelsExtremeHeat",
	    "typeid":"8"
	}, {
	    "DisplayName": "进击的猿人",
	    "DisplayEnName": "Poke The Guy",
	    "ButtonImagePath": "BTN_PokeTheGuy.png",
	    "LapisId": "SMG_pokeTheGuy",
	    "typeid":"8"
	}, {
	    "DisplayName": "钻石帝国",
	    "DisplayEnName": "Diamond Empire",
	    "ButtonImagePath": "BTN_DiamondEmpire_ZH.png",
	    "LapisId": "SMG_diamondEmpire",
	    "typeid":"8"
	}, {
	    "DisplayName": "梦幻邂逅",
	    "DisplayEnName": "Dream Date",
	    "ButtonImagePath": "BTN_DreamDate_ZH.png",
	    "LapisId": "SMG_dreamDate",
	    "typeid":"8"
	}, {
	    "DisplayName": "108好汉乘数财富",
	    "DisplayEnName": "108 Heroes Multiplier Fortunes",
	    "ButtonImagePath": "BTN_108HeroesMF_ZH.png",
	    "LapisId": "SMG_108heroesMultiplierFortunes",
	    "typeid":"8"
	}, {
	    "DisplayName": "三國",
	    "DisplayEnName": "3 Empires",
	    "ButtonImagePath": "BTN_3Empire_ZH.png",
	    "LapisId": "SMG_3empires",
	    "typeid":"8"
	}, {
	    "DisplayName": "漂亮猫咪",
	    "DisplayEnName": "Pretty Kitty",
	    "ButtonImagePath": "BTN_prettykitty_zh.png",
	    "LapisId": "SMG_prettyKitty",
	    "typeid":"8"
	}, {
	    "DisplayName": "猴子基诺",
	    "DisplayEnName": "Monkey Keno",
	    "ButtonImagePath": "BTN_MonkeyKeno_ZH.png",
	    "LapisId": "SMG_MonkeyKeno",
	    "typeid":"8"
	},  {
	    "DisplayName": "侏罗纪世界",
	    "DisplayEnName": "Jurassic World",
	    "ButtonImagePath": "BTN_JurassicWorld.png",
	    "LapisId": "SMG_jurassicWorld",
	    "typeid":"8"
	},{
	    "DisplayName": "纯铂",
	    "DisplayEnName": "Pure Platinum",
	    "ButtonImagePath": "BTN_PurePlatinum1.png",
	    "LapisId": "SMG_purePlatinum",
	    "typeid":"8"
	}, {
	    "DisplayName": "拉美西斯的财富",
	    "DisplayEnName": "Ramesses Riches",
	    "ButtonImagePath": "BTN_RamessesRiches.png",
	    "LapisId": "SMG_RamessesRiches",
	    "typeid":"8"
	}, {
	    "DisplayName": "花粉之国",
	    "DisplayEnName": "Pollen Party",
	    "ButtonImagePath": "BTN_PollenParty_ZH.png",
	    "LapisId": "SMG_pollenParty",
	    "typeid":"8"
	},{
	    "DisplayName": "黄金花花公子",
	    "DisplayEnName": "Playboy Gold",
	    "ButtonImagePath": "BTN_PlayboyGold_ZH.png",
	    "LapisId": "SMG_playboyGold",
	    "typeid":"8"
	},{
	    "DisplayName": "躲猫猫",
	    "DisplayEnName": "Peek-a-boo",
	    "ButtonImagePath": "BTN_PeekABoo_ZH.png",
	    "LapisId": "SMG_peekABoo5Reel",
	    "typeid":"8"
	},{
	    "DisplayName": "呼噜噜爱上乡下",
	    "DisplayEnName": "Oink Country Love",
	    "ButtonImagePath": "BTN_OinkCountryLove_ZH.png",
	    "LapisId": "SMG_oinkCountryLove",
	    "typeid":"8"
	},{
	    "DisplayName": "怪物赛车",
	    "DisplayEnName": "Monster Wheels",
	    "ButtonImagePath": 	"MonsterWheels_BTN_Rollover_ZH.png",
	    "LapisId": "SMG_monsterWheels",
	    "typeid":"8"
	},{
	    "DisplayName": "巨款大冲击",
	    "DisplayEnName": "Mega Money Rush",
	    "ButtonImagePath": "BTN_MegaMoneyRush_ZH.png",
	    "LapisId": "SMG_megaMoneyRush",
	    "typeid":"8"
	},{
	    "DisplayName": "幸运生肖",
	    "DisplayEnName": "Lucky Zodiac",
	    "ButtonImagePath": "BTN_LuckyZodiac_ZH.png",
	    "LapisId": "SMG_luckyZodiac",
	    "typeid":"8"
	},{
	    "DisplayName": "幸运双星",
	    "DisplayEnName": "Lucky Twins",
	    "ButtonImagePath": "BTN_luckyTwins_ZH.png",
	    "LapisId": "SMG_luckyTwins",
	    "typeid":"8"
	},{
	    "DisplayName": "宝贝财神",
	    "DisplayEnName": "Lucky Little Gods",
	    "ButtonImagePath": "BTN_LuckyLittleGods_ZH.png",
	    "LapisId": "SMG_luckyLittleGods",
	    "typeid":"8"
	}, {
	    "DisplayName": "迷失拉斯维加斯",
	    "DisplayEnName": "Lost Vegas",
	    "ButtonImagePath": "BTN_LostVegas_zh.png",
	    "LapisId": "SMG_lostVegas",
	    "typeid":"8"
	},{
	    "DisplayName": "富裕人生",
	    "DisplayEnName": "Life of Riches",
	    "ButtonImagePath": "BTN_LifeOfRiches_zh.png",
	    "LapisId": "SMG_lifeOfRiches",
	    "typeid":"8"
	},{
	    "DisplayName": "忍者法宝",
	    "DisplayEnName": "Ninja Magic",
	    "ButtonImagePath": "BTN_NinjaMagic_zh.png",
	    "LapisId": "SMG_ninjamagic",
	    "typeid":"8"
	}];

var ptdata = [{
    "ButtonImagePath": "pt/pc/lmcs.png",
    "DisplayName": "龙门传说",
    "typeid": "老虎机",
    "LapisId": "gpas_dbond_pop"
},{
    "ButtonImagePath": "pt/pc/jfl.png",
    "DisplayName": "金福龙",
    "typeid": "老虎机",
    "LapisId": "gpas_jflong_pop"
},{
    "ButtonImagePath": "pt/pc/ymcl.png",
    "DisplayName": "野蛮丛林",
    "typeid": "老虎机",
    "LapisId": "gpas_sjungle_pop"
},{
    "ButtonImagePath": "pt/pc/ljxy.png",
    "DisplayName": "龙驾祥云",
    "typeid": "老虎机",
    "LapisId": "ljxy"
},{
    "ButtonImagePath": "pt/pc/ceby.png",
    "DisplayName": "嫦娥奔月",
    "typeid": "老虎机",
    "LapisId": "gpas_elady_pop"
},{
    "ButtonImagePath": "pt/pc/jinfu.png",
    "DisplayName": "金福幸运",
    "typeid": "老虎机",
    "LapisId": "gpas_xjinfu_pop"
},{
    "ButtonImagePath": "pt/pc/squeen.png",
    "DisplayName": "天之女王",
    "typeid": "老虎机",
    "LapisId": "gpas_squeen_pop"
},{
    "ButtonImagePath": "pt/pc/jqw.png",
    "DisplayName": "金钱蛙头奖奖池",
    "typeid": "老虎机",
    "LapisId": "pop_sw_jqw_ab_jp_skw"
},{
    "ButtonImagePath": "pt/pc/wsjc.png",
    "DisplayName": "狂热重转舞狮奖池",
    "typeid": "老虎机",
    "LapisId": "pop_sw_rm_ab_jp_skw"
},{
    "ButtonImagePath": "pt/pc/zctz.png",
    "DisplayName": "招财童子奖池",
    "typeid": "老虎机",
    "LapisId": "pop_shctz_ab_jp_skw"
},{
    "ButtonImagePath": "pt/pc/wotp.jpg",
    "DisplayName": "凤凰之道",
    "typeid": "老虎机",
    "LapisId": "wotp"
},{
    "ButtonImagePath": "pt/pc/ccccny.jpg",
    "DisplayName": "币币币",
    "typeid": "老虎机",
    "LapisId": "ccccny"
},{
    "ButtonImagePath": "pt/pc/anwild.jpg",
    "DisplayName": "狂野蟒蛇",
    "typeid": "老虎机",
    "LapisId": "anwild"
},{
    "ButtonImagePath": "pt/pc/tigc.jpg",
    "DisplayName": "虎爪",
    "typeid": "老虎机",
    "LapisId": "tigc"
},{
    "ButtonImagePath": "pt/pc/3cb.jpg",
    "DisplayName": "三张扑克牌3cb",
    "typeid": "桌牌游戏",
    "LapisId": "3cb"
},{
    "ButtonImagePath": "pt/pc/gpas_panthpays_pop.jpg",
    "DisplayName": "发财黑豹",
    "typeid": "老虎机",
    "LapisId": "gpas_panthpays_pop"
},{
    "ButtonImagePath": "pt/pc/gpas_lotj_pop.jpg",
    "DisplayName": "捷豹的传说",
    "typeid": "老虎机",
    "LapisId": "gpas_lotj_pop"
},{
    "ButtonImagePath": "pt/pc/gpas_fiesta_pop.jpg",
    "DisplayName": "记忆盛宴",
    "typeid": "老虎机",
    "LapisId": "gpas_fiesta_pop"
},{
    "ButtonImagePath": "pt/pc/gpas_rothr_pop.jpg",
    "DisplayName": "隐秘境界的突袭",
    "typeid": "老虎机",
    "LapisId": "gpas_rothr_pop"
},{
    "ButtonImagePath": "pt/pc/gpas_satsumo_pop.jpg",
    "DisplayName": "相扑君的逆袭",
    "typeid": "老虎机",
    "LapisId": "gpas_satsumo_pop"
},{
    "ButtonImagePath": "pt/pc/pop_sw_yxlb_skw.jpg",
    "DisplayName": "英雄吕布",
    "typeid": "老虎机",
    "LapisId": "pop_sw_yxlb_skw"
},{
    "ButtonImagePath": "pt/pc/pop_sw_dr_skw.jpg",
    "DisplayName": "龙的财富",
    "typeid": "老虎机",
    "LapisId": "pop_sw_dr_skw"
},{
    "ButtonImagePath": "pt/pc/7bal.jpg",
    "DisplayName": "真人7席百家乐",
    "typeid": "视讯扑克",
    "LapisId": "7bal"
}, {
    "ButtonImagePath": "pt/pc/hb.jpg",
    "DisplayName": "狂欢夜",
    "typeid": "老虎机",
    "LapisId": "hb"
}, {
    "ButtonImagePath": "pt/pc/ashadv.jpg",
    "DisplayName": "梦游仙境豪华版",
    "typeid": "老虎机",
    "LapisId": "ashadv"
}, {
    "ButtonImagePath": "pt/pc/ftsis.jpg",
    "DisplayName": "众神时代：命运姐妹",
    "typeid": "老虎机",
    "LapisId": "ftsis"
}, {
    "ButtonImagePath": "pt/pc/furf.jpg",
    "DisplayName": "众神时代：狂暴4",
    "typeid": "老虎机",
    "LapisId": "furf"
}, {
    "ButtonImagePath": "pt/pc/athn.jpg",
    "DisplayName": "众神时代：智慧女神",
    "typeid": "老虎机",
    "LapisId": "athn"
}, {
    "ButtonImagePath": "pt/pc/kolymp.jpg",
    "DisplayName": "众神时代：奥林匹斯之王",
    "typeid": "老虎机",
    "LapisId": "kolymp"
}, {
    "ButtonImagePath": "pt/pc/hrcls.jpg",
    "DisplayName": "众神时代：奥林匹斯王子",
    "typeid": "老虎机",
    "LapisId": "hrcls"
}, {
    "ButtonImagePath": "pt/pc/aogro.jpg",
    "DisplayName": "众神时代轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "aogro"
}, {
    "ButtonImagePath": "pt/pc/ashamw.jpg",
    "DisplayName": "野生亚马逊",
    "typeid": "老虎机",
    "LapisId": "ashamw"
}, {
    "ButtonImagePath": "pt/pc/arc.jpg",
    "DisplayName": "弓箭手",
    "typeid": "老虎机",
    "LapisId": "arc"
}, {
    "ButtonImagePath": "pt/pc/art.jpg",
    "DisplayName": "北极宝藏",
    "typeid": "老虎机",
    "LapisId": "art"
}, {
    "ButtonImagePath": "pt/pc/gtsatq.jpg",
    "DisplayName": "亚特兰蒂斯女王",
    "typeid": "老虎机",
    "LapisId": "gtsatq"
}, {
    "ButtonImagePath": "pt/pc/ba.jpg",
    "DisplayName": "百家乐",
    "typeid": "刮刮卡",
    "LapisId": "ba"
}, {
    "ButtonImagePath": "pt/pc/bal.jpg",
    "DisplayName": "真人百家乐",
    "typeid": "视讯扑克",
    "LapisId": "bal"
}, {
    "ButtonImagePath": "pt/pc/bs.jpg",
    "DisplayName": "白狮",
    "typeid": "老虎机",
    "LapisId": "bs"
}, {
    "ButtonImagePath": "pt/pc/bl.jpg",
    "DisplayName": "海滨嘉年华",
    "typeid": "老虎机",
    "LapisId": "bl"
}, {
    "ButtonImagePath": "pt/pc/bt.jpg",
    "DisplayName": "百慕大三角",
    "typeid": "老虎机",
    "LapisId": "bt"
}, {
    "ButtonImagePath": "pt/pc/mobbj.jpg",
    "DisplayName": "21点",
    "typeid": "刮刮卡",
    "LapisId": "bj_mh5"
}, {
    "ButtonImagePath": "pt/pc/bjl.jpg",
    "DisplayName": "真人21点",
    "typeid": "视讯扑克",
    "LapisId": "bjl"
}, {
    "ButtonImagePath": "pt/pc/bob.jpg",
    "DisplayName": "熊之舞",
    "typeid": "老虎机",
    "LapisId": "bob"
}, {
    "ButtonImagePath": "pt/pc/ashbob.jpg",
    "DisplayName": "魔豆赏金",
    "typeid": "老虎机",
    "LapisId": "ashbob"
}, {
    "ButtonImagePath": "pt/pc/bfb.jpg",
    "DisplayName": "犎牛闪电突击",
    "typeid": "老虎机",
    "LapisId": "bfb"
}, {
    "ButtonImagePath": "pt/pc/ct.jpg",
    "DisplayName": "船长的宝藏",
    "typeid": "老虎机",
    "LapisId": "ct"
}, {
    "ButtonImagePath": "pt/pc/cheaa.jpg",
    "DisplayName": "娱乐场同花顺",
    "typeid": "刮刮卡",
    "LapisId": "cheaa"
}, {
    "ButtonImagePath": "pt/pc/ctiv.jpg",
    "DisplayName": "猫王战赌城",
    "typeid": "老虎机",
    "LapisId": "ctiv"
}, {
    "ButtonImagePath": "pt/pc/catqc.jpg",
    "DisplayName": "猫后",
    "typeid": "老虎机",
    "LapisId": "catqc"
}, {
    "ButtonImagePath": "pt/pc/chao.jpg",
    "DisplayName": "超级 888",
    "typeid": "老虎机",
    "LapisId": "chao"
}, {
    "ButtonImagePath": "pt/pc/chl.jpg",
    "DisplayName": "狂野樱桃",
    "typeid": "老虎机",
    "LapisId": "chl"
}, {
    "ButtonImagePath": "pt/pc/ashcpl.jpg",
    "DisplayName": "宝箱满满",
    "typeid": "老虎机",
    "LapisId": "ashcpl"
}, {
    "ButtonImagePath": "pt/pc/cm.jpg",
    "DisplayName": "中式厨房",
    "typeid": "老虎机",
    "LapisId": "cm"
}, {
    "ButtonImagePath": "pt/pc/gtscnb.jpg",
    "DisplayName": "警察和土匪",
    "typeid": "老虎机",
    "LapisId": "gtscnb"
}, {
    "ButtonImagePath": "pt/pc/gtscbl.jpg",
    "DisplayName": "牛仔和外星人",
    "typeid": "老虎机",
    "LapisId": "gtscbl"
}, {
    "ButtonImagePath": "pt/pc/c7.jpg",
    "DisplayName": "疯狂之七",
    "typeid": "老虎机",
    "LapisId": "c7"
}, {
    "ButtonImagePath": "pt/pc/gtsdrdv_.jpg",
    "DisplayName": "无畏的戴夫",
    "typeid": "老虎机",
    "LapisId": "gtsdrdv "
}, {
    "ButtonImagePath": "pt/pc/mobdt.jpg",
    "DisplayName": "沙漠财宝",
    "typeid": "老虎机",
    "LapisId": "mobdt"
}, {
    "ButtonImagePath": "pt/pc/dnr.jpg",
    "DisplayName": "海豚之梦",
    "typeid": "老虎机",
    "LapisId": "dnr"
}, {
    "ButtonImagePath": "pt/pc/dlm.jpg",
    "DisplayName": "情圣博士",
    "typeid": "老虎机",
    "LapisId": "dlm"
}, {
    "ButtonImagePath": "pt/pc/gtsdgk.jpg",
    "DisplayName": "龙之国度",
    "typeid": "老虎机",
    "LapisId": "gtsdgk"
}, {
    "ButtonImagePath": "pt/pc/eas.jpg",
    "DisplayName": "复活节惊喜",
    "typeid": "老虎机",
    "LapisId": "eas"
}, {
    "ButtonImagePath": "pt/pc/esm.jpg",
    "DisplayName": "埃斯梅拉达",
    "typeid": "老虎机",
    "LapisId": "esm"
}, {
    "ButtonImagePath": "pt/pc/mobro.jpg",
    "DisplayName": "欧式轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "mobro"
}, {
    "ButtonImagePath": "pt/pc/evj.jpg",
    "DisplayName": "欢乐积宝彩池",
    "typeid": "老虎机",
    "LapisId": "evj"
}, {
    "ButtonImagePath": "pt/pc/ashfta.jpg",
    "DisplayName": "魔镜与公主",
    "typeid": "老虎机",
    "LapisId": "ashfta"
}, {
    "ButtonImagePath": "pt/pc/fcgz.jpg",
    "DisplayName": "翡翠公主",
    "typeid": "老虎机",
    "LapisId": "fcgz"
}, {
    "ButtonImagePath": "pt/pc/gtsflzt.jpg",
    "DisplayName": "飞龙在天",
    "typeid": "老虎机",
    "LapisId": "gtsflzt"
}, {
    "ButtonImagePath": "pt/pc/fkmj.jpg",
    "DisplayName": "疯狂麻将",
    "typeid": "老虎机",
    "LapisId": "fkmj"
}, {
    "ButtonImagePath": "pt/pc/ftg.jpg",
    "DisplayName": "五虎将",
    "typeid": "老虎机",
    "LapisId": "ftg"
}, {
    "ButtonImagePath": "pt/pc/foy.jpg",
    "DisplayName": "青春之泉",
    "typeid": "老虎机",
    "LapisId": "foy"
}, {
    "ButtonImagePath": "pt/pc/gtsfc.jpg",
    "DisplayName": "足球嘉年华",
    "typeid": "老虎机",
    "LapisId": "gtsfc"
}, {
    "ButtonImagePath": "pt/pc/fbr.jpg",
    "DisplayName": "终极足球",
    "typeid": "老虎机",
    "LapisId": "fbr"
}, {
    "ButtonImagePath": "pt/pc/fow.jpg",
    "DisplayName": "惊异之林",
    "typeid": "老虎机",
    "LapisId": "fow"
}, {
    "ButtonImagePath": "pt/pc/frtf.jpg",
    "DisplayName": "五福海盗",
    "typeid": "老虎机",
    "LapisId": "frtf"
}, {
    "ButtonImagePath": "pt/pc/fday.jpg",
    "DisplayName": "幸运日 ",
    "typeid": "老虎机",
    "LapisId": "fday"
}, {
    "ButtonImagePath": "pt/pc/frtln.jpg",
    "DisplayName": "幸运狮子",
    "typeid": "老虎机",
    "LapisId": "frtln"
}, {
    "ButtonImagePath": "pt/pc/fxf.jpg",
    "DisplayName": "狐媚宝藏",
    "typeid": "老虎机",
    "LapisId": "fxf"
}, {
    "ButtonImagePath": "pt/pc/fdt.jpg",
    "DisplayName": "德托里传奇",
    "typeid": "老虎机",
    "LapisId": "fdt"
}, {
    "ButtonImagePath": "pt/pc/fdtjg.jpg",
    "DisplayName": "德托里传奇积宝游戏",
    "typeid": "老虎机",
    "LapisId": "fdtjg"
}, {
    "ButtonImagePath": "pt/pc/fmn.jpg",
    "DisplayName": "水果狂热",
    "typeid": "老虎机",
    "LapisId": "fmn"
}, {
    "ButtonImagePath": "pt/pc/ashfmf.jpg",
    "DisplayName": "圆月财富",
    "typeid": "老虎机",
    "LapisId": "ashfmf"
}, {
    "ButtonImagePath": "pt/pc/fff.jpg",
    "DisplayName": "酷炫水果农场",
    "typeid": "老虎机",
    "LapisId": "fff"
}, {
    "ButtonImagePath": "pt/pc/fnfrj.jpg",
    "DisplayName": "酷炫水果",
    "typeid": "老虎机",
    "LapisId": "fnfrj"
}, {
    "ButtonImagePath": "pt/pc/fm.jpg",
    "DisplayName": "古怪猴子",
    "typeid": "老虎机",
    "LapisId": "fm"
}, {
    "ButtonImagePath": "pt/pc/ges.jpg",
    "DisplayName": "艺伎故事 ",
    "typeid": "老虎机",
    "LapisId": "ges"
}, {
    "ButtonImagePath": "pt/pc/glrj.jpg",
    "DisplayName": "角斗士积宝",
    "typeid": "老虎机",
    "LapisId": "glrj"
}, {
    "ButtonImagePath": "pt/pc/gemq.jpg",
    "DisplayName": "宝石女王",
    "typeid": "老虎机",
    "LapisId": "gemq"
}, {
    "ButtonImagePath": "pt/pc/grel.jpg",
    "DisplayName": "金色召集",
    "typeid": "老虎机",
    "LapisId": "grel"
}, {
    "ButtonImagePath": "pt/pc/glg.jpg",
    "DisplayName": "黄金体育竞技场",
    "typeid": "老虎机",
    "LapisId": "glg"
}, {
    "ButtonImagePath": "pt/pc/gos.jpg",
    "DisplayName": "黄金之旅",
    "typeid": "老虎机",
    "LapisId": "gos"
}, {
    "ButtonImagePath": "pt/pc/bib.jpg",
    "DisplayName": "海底探宝",
    "typeid": "老虎机",
    "LapisId": "bib"
}, {
    "ButtonImagePath": "pt/pc/gro.jpg",
    "DisplayName": "最强奥德赛",
    "typeid": "老虎机",
    "LapisId": "gro"
}, {
    "ButtonImagePath": "pt/pc/hlf.jpg",
    "DisplayName": "万圣节宝藏",
    "typeid": "老虎机",
    "LapisId": "hlf"
}, {
    "ButtonImagePath": "pt/pc/hlf2.jpg",
    "DisplayName": "万圣节宝藏 2",
    "typeid": "老虎机",
    "LapisId": "hlf2"
}, {
    "ButtonImagePath": "pt/pc/hh.jpg",
    "DisplayName": "鬼宅",
    "typeid": "老虎机",
    "LapisId": "hh"
}, {
    "ButtonImagePath": "pt/pc/ashhotj.jpg",
    "DisplayName": "丛林之心",
    "typeid": "老虎机",
    "LapisId": "ashhotj"
}, {
    "ButtonImagePath": "pt/pc/heavru.jpg",
    "DisplayName": "武则天",
    "typeid": "老虎机",
    "LapisId": "heavru"
}, {
    "ButtonImagePath": "pt/pc/hk.jpg",
    "DisplayName": "高速公路之王",
    "typeid": "老虎机",
    "LapisId": "hk"
}, {
    "ButtonImagePath": "pt/pc/gts50.jpg",
    "DisplayName": "炙热宝石",
    "typeid": "老虎机",
    "LapisId": "gts50"
}, {
    "ButtonImagePath": "pt/pc/hotktv.jpg",
    "DisplayName": "火热KTV",
    "typeid": "老虎机",
    "LapisId": "hotktv"
}, {
    "ButtonImagePath": "pt/pc/aztec.jpg",
    "DisplayName": "印加帝国头奖",
    "typeid": "老虎机",
    "LapisId": "aztec"
}, {
    "ButtonImagePath": "pt/pc/ir.jpg",
    "DisplayName": "浮冰流",
    "typeid": "老虎机",
    "LapisId": "ir"
}, {
    "ButtonImagePath": "pt/pc/irl.jpg",
    "DisplayName": "幸运爱尔兰",
    "typeid": "老虎机",
    "LapisId": "irl"
}, {
    "ButtonImagePath": "pt/pc/jpgt.jpg",
    "DisplayName": "奖金巨人",
    "typeid": "老虎机",
    "LapisId": "jpgt"
}, {
    "ButtonImagePath": "pt/pc/gtsje.jpg",
    "DisplayName": "玉皇大帝",
    "typeid": "老虎机",
    "LapisId": "gtsje"
}, {
    "ButtonImagePath": "pt/pc/gtsjxb.jpg",
    "DisplayName": "吉祥 8",
    "typeid": "老虎机",
    "LapisId": "gtsjxb"
}, {
    "ButtonImagePath": "pt/pc/jqw.jpg",
    "DisplayName": "金钱蛙",
    "typeid": "老虎机",
    "LapisId": "jqw"
}, {
    "ButtonImagePath": "pt/pc/gtsjhw.jpg",
    "DisplayName": "约翰韦恩",
    "typeid": "老虎机",
    "LapisId": "gtsjhw"
}, {
    "ButtonImagePath": "pt/pc/kkg.jpg",
    "DisplayName": "无敌金刚",
    "typeid": "老虎机",
    "LapisId": "kkg"
}, {
    "ButtonImagePath": "pt/pc/lndg.jpg",
    "DisplayName": "遍地黄金",
    "typeid": "老虎机",
    "LapisId": "lndg"
}, {
    "ButtonImagePath": "pt/pc/ght_a.jpg",
    "DisplayName": "烈焰钻石",
    "typeid": "老虎机",
    "LapisId": "ght_a"
}, {
    "ButtonImagePath": "pt/pc/kfp.jpg",
    "DisplayName": "六福兽",
    "typeid": "老虎机",
    "LapisId": "kfp"
}, {
    "ButtonImagePath": "pt/pc/rofl.jpg",
    "DisplayName": "真人法式轮盘",
    "typeid": "视讯扑克",
    "LapisId": "rofl"
}, {
    "ButtonImagePath": "pt/pc/longlong.jpg",
    "DisplayName": "龙龙龙",
    "typeid": "老虎机",
    "LapisId": "longlong"
}, {
    "ButtonImagePath": "pt/pc/lm.jpg",
    "DisplayName": "疯狂乐透",
    "typeid": "老虎机",
    "LapisId": "lm"
}, {
    "ButtonImagePath": "pt/pc/mgstk.jpg",
    "DisplayName": "魔力老虎机",
    "typeid": "老虎机",
    "LapisId": "mgstk"
}, {
    "ButtonImagePath": "pt/pc/gtsmrln.jpg",
    "DisplayName": "玛丽莲梦露",
    "typeid": "老虎机",
    "LapisId": "gtsmrln"
}, {
    "ButtonImagePath": "pt/pc/mfrt.jpg",
    "DisplayName": "幸运女士",
    "typeid": "老虎机",
    "LapisId": "mfrt"
}, {
    "ButtonImagePath": "pt/pc/ashlob.jpg",
    "DisplayName": "蒙提派森之万世魔星",
    "typeid": "老虎机",
    "LapisId": "ashlob"
}, {
    "ButtonImagePath": "pt/pc/mrcb.jpg",
    "DisplayName": "返利先生",
    "typeid": "老虎机",
    "LapisId": "mrcb"
}, {
    "ButtonImagePath": "pt/pc/nk.jpg",
    "DisplayName": "海王星王国",
    "typeid": "老虎机",
    "LapisId": "nk"
}, {
    "ButtonImagePath": "pt/pc/nian.jpg",
    "DisplayName": "年年有余",
    "typeid": "老虎机",
    "LapisId": "nian"
}, {
    "ButtonImagePath": "pt/pc/pmn.jpg",
    "DisplayName": "月亮下的黑豹",
    "typeid": "老虎机",
    "LapisId": "pmn"
}, {
    "ButtonImagePath": "pt/pc/pgv.jpg",
    "DisplayName": "企鹅度假",
    "typeid": "老虎机",
    "LapisId": "pgv"
}, {
    "ButtonImagePath": "pt/pc/bjp.jpg",
    "DisplayName": "多幅完美21点",
    "typeid": "刮刮卡",
    "LapisId": "bjp"
}, {
    "ButtonImagePath": "pt/pc/pnp.jpg",
    "DisplayName": "粉红豹",
    "typeid": "老虎机",
    "LapisId": "pnp"
}, {
    "ButtonImagePath": "pt/pc/pst.jpg",
    "DisplayName": "法老王的秘密",
    "typeid": "老虎机",
    "LapisId": "pst"
}, {
    "ButtonImagePath": "pt/pc/paw.jpg",
    "DisplayName": "三只小猪与大灰狼",
    "typeid": "老虎机",
    "LapisId": "paw"
}, {
    "ButtonImagePath": "pt/pc/gtspor.jpg",
    "DisplayName": "充裕财富",
    "typeid": "老虎机",
    "LapisId": "gtspor"
}, {
    "ButtonImagePath": "pt/pc/rodz_g.jpg",
    "DisplayName": "奖金美式轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "rodz_g"
}, {
    "ButtonImagePath": "pt/pc/ro_g.jpg",
    "DisplayName": "奖金欧式轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "ro_g"
}, {
    "ButtonImagePath": "pt/pc/phot.jpg",
    "DisplayName": "紫色狂热",
    "typeid": "老虎机",
    "LapisId": "phot"
}, {
    "ButtonImagePath": "pt/pc/qnw.jpg",
    "DisplayName": "权杖女王",
    "typeid": "老虎机",
    "LapisId": "qnw"
}, {
    "ButtonImagePath": "pt/pc/ririjc.jpg",
    "DisplayName": "日日进财",
    "typeid": "老虎机",
    "LapisId": "ririjc"
}, {
    "ButtonImagePath": "pt/pc/ririshc.jpg",
    "DisplayName": "日日生财",
    "typeid": "老虎机",
    "LapisId": "ririshc"
}, {
    "ButtonImagePath": "pt/pc/rky.jpg",
    "DisplayName": "洛奇",
    "typeid": "老虎机",
    "LapisId": "rky"
}, {
    "ButtonImagePath": "pt/pc/rng2.jpg",
    "DisplayName": "罗马荣光",
    "typeid": "老虎机",
    "LapisId": "rng2"
}, {
    "ButtonImagePath": "pt/pc/rol.jpg",
    "DisplayName": "真人轮盘",
    "typeid": "视讯扑克",
    "LapisId": "rol"
}, {
    "ButtonImagePath": "pt/pc/sfh.jpg",
    "DisplayName": "野生狩猎",
    "typeid": "老虎机",
    "LapisId": "sfh"
}, {
    "ButtonImagePath": "pt/pc/gtssmbr.jpg",
    "DisplayName": "桑巴之舞",
    "typeid": "老虎机",
    "LapisId": "gtssmbr"
}, {
    "ButtonImagePath": "pt/pc/ssp.jpg",
    "DisplayName": "圣诞老人奇袭",
    "typeid": "老虎机",
    "LapisId": "ssp"
}, {
    "ButtonImagePath": "pt/pc/samz.jpg",
    "DisplayName": "亚马逊之谜",
    "typeid": "老虎机",
    "LapisId": "samz"
}, {
    "ButtonImagePath": "pt/pc/shmst.jpg",
    "DisplayName": "神秘夏洛克",
    "typeid": "老虎机",
    "LapisId": "shmst"
}, {
    "ButtonImagePath": "pt/pc/sx.jpg",
    "DisplayName": "四象",
    "typeid": "老虎机",
    "LapisId": "sx"
}, {
    "ButtonImagePath": "pt/pc/sis.jpg",
    "DisplayName": "忍者风云",
    "typeid": "老虎机",
    "LapisId": "sis"
}, {
    "ButtonImagePath": "pt/pc/sib.jpg",
    "DisplayName": "银弹",
    "typeid": "老虎机",
    "LapisId": "sib"
}, {
    "ButtonImagePath": "pt/pc/ashsbd.jpg",
    "DisplayName": "辛巴达金航记",
    "typeid": "老虎机",
    "LapisId": "ashsbd"
}, {
    "ButtonImagePath": "pt/pc/srcg.jpg",
    "DisplayName": "欧莱里之黄金大田",
    "typeid": "老虎机",
    "LapisId": "srcg"
}, {
    "ButtonImagePath": "pt/pc/sol.jpg",
    "DisplayName": "幸运直击",
    "typeid": "老虎机",
    "LapisId": "sol"
}, {
    "ButtonImagePath": "pt/pc/gtsswk.jpg",
    "DisplayName": "孙悟空",
    "typeid": "老虎机",
    "LapisId": "gtsswk"
}, {
    "ButtonImagePath": "pt/pc/slion.jpg",
    "DisplayName": "超级狮子",
    "typeid": "老虎机",
    "LapisId": "slion"
}, {
    "ButtonImagePath": "pt/pc/cnpr.jpg",
    "DisplayName": "甜蜜派对",
    "typeid": "老虎机",
    "LapisId": "cnpr"
}, {
    "ButtonImagePath": "pt/pc/tpd2.jpg",
    "DisplayName": "泰国梦天堂",
    "typeid": "老虎机",
    "LapisId": "tpd2"
}, {
    "ButtonImagePath": "pt/pc/tht.jpg",
    "DisplayName": "泰国佛寺",
    "typeid": "老虎机",
    "LapisId": "tht"
}, {
    "ButtonImagePath": "pt/pc/dcv.jpg",
    "DisplayName": "海上寻宝",
    "typeid": "老虎机",
    "LapisId": "dcv"
}, {
    "ButtonImagePath": "pt/pc/ashglss.jpg",
    "DisplayName": "水晶鞋",
    "typeid": "老虎机",
    "LapisId": "ashglss"
}, {
    "ButtonImagePath": "pt/pc/gtsgme.jpg",
    "DisplayName": "大明帝国",
    "typeid": "老虎机",
    "LapisId": "gtsgme"
}, {
    "ButtonImagePath": "pt/pc/lvb.jpg",
    "DisplayName": "恋爱之船",
    "typeid": "老虎机",
    "LapisId": "lvb"
}, {
    "ButtonImagePath": "pt/pc/donq.jpg",
    "DisplayName": "唐吉诃德",
    "typeid": "老虎机",
    "LapisId": "donq"
}, {
    "ButtonImagePath": "pt/pc/tmqd.jpg",
    "DisplayName": "三个火枪手",
    "typeid": "老虎机",
    "LapisId": "tmqd"
}, {
    "ButtonImagePath": "pt/pc/ashtmd.jpg",
    "DisplayName": "三门问题",
    "typeid": "老虎机",
    "LapisId": "ashtmd"
}, {
    "ButtonImagePath": "pt/pc/topg.jpg",
    "DisplayName": "捍卫战士",
    "typeid": "老虎机",
    "LapisId": "topg"
}, {
    "ButtonImagePath": "pt/pc/trpmnk.jpg",
    "DisplayName": "三倍猴子",
    "typeid": "老虎机",
    "LapisId": "trpmnk"
}, {
    "ButtonImagePath": "pt/pc/er.jpg",
    "DisplayName": "开心假期",
    "typeid": "老虎机",
    "LapisId": "er"
}, {
    "ButtonImagePath": "pt/pc/vcstd.jpg",
    "DisplayName": "开心假期豪華版",
    "typeid": "老虎机",
    "LapisId": "vcstd"
}, {
    "ButtonImagePath": "pt/pc/gts52.jpg",
    "DisplayName": "维京狂热",
    "typeid": "老虎机",
    "LapisId": "gts52"
}, {
    "ButtonImagePath": "pt/pc/whk.jpg",
    "DisplayName": "白狮王",
    "typeid": "老虎机",
    "LapisId": "whk"
}, {
    "ButtonImagePath": "pt/pc/gtswg.jpg",
    "DisplayName": "野生动物大世界",
    "typeid": "老虎机",
    "LapisId": "gtswg"
}, {
    "ButtonImagePath": "pt/pc/ashwgaa.jpg",
    "DisplayName": "野生世界：北极大冒险",
    "typeid": "老虎机",
    "LapisId": "ashwgaa"
}, {
    "ButtonImagePath": "pt/pc/wis.jpg",
    "DisplayName": "狂野精灵",
    "typeid": "老虎机",
    "LapisId": "wis"
}, {
    "ButtonImagePath": "pt/pc/gtswng.jpg",
    "DisplayName": "纯金之翼",
    "typeid": "老虎机",
    "LapisId": "gtswng"
}, {
    "ButtonImagePath": "pt/pc/wlg.jpg",
    "DisplayName": "舞龙",
    "typeid": "老虎机",
    "LapisId": "wlg"
}, {
    "ButtonImagePath": "pt/pc/wlcsh.jpg",
    "DisplayName": "五路财神",
    "typeid": "老虎机",
    "LapisId": "wlcsh"
}, {
    "ButtonImagePath": "pt/pc/zcjb.jpg",
    "DisplayName": "招财进宝",
    "typeid": "老虎机",
    "LapisId": "zcjb"
}, {
    "ButtonImagePath": "pt/pc/zcjbjp.jpg",
    "DisplayName": "招财进宝积宝财池",
    "typeid": "老虎机",
    "LapisId": "zcjbjp"
}, {
    "ButtonImagePath": "pt/pc/zctz.jpg",
    "DisplayName": "招财童子",
    "typeid": "老虎机",
    "LapisId": "zctz"
}, {
    "ButtonImagePath": "pt/pc/ba.jpg",
    "DisplayName": "百家乐",
    "typeid": "视讯扑克",
    "LapisId": "ba"
}, {
    "ButtonImagePath": "pt/pc/cashfi.jpg",
    "DisplayName": "深海大赢家",
    "typeid": "老虎机",
    "LapisId": "cashfi"
}, {
    "ButtonImagePath": "pt/pc/mobro.jpg",
    "DisplayName": "欧式轮盘",
    "typeid": "视讯扑克",
    "LapisId": "mobro"
}, {
    "ButtonImagePath": "pt/pc/bjp.jpg",
    "DisplayName": "完美21点",
    "typeid": "视讯扑克",
    "LapisId": "bjp"
}, {
    "ButtonImagePath": "pt/pc/bjp.jpg",
    "DisplayName": "完美21点",
    "typeid": "桌牌游戏",
    "LapisId": "bjp"
}, {
    "ButtonImagePath": "pt/pc/ubjl.jpg",
    "DisplayName": "真人无限21点",
    "typeid": "视讯扑克",
    "LapisId": "ubjl"
}, {
    "ButtonImagePath": "pt/pc/ash3brg.jpg",
    "DisplayName": "三卡吹牛",
    "typeid": "桌牌游戏",
    "LapisId": "ash3brg"
}, {
    "ButtonImagePath": "pt/pc/ashvrtd.jpg",
    "DisplayName": "虚拟赛狗",
    "typeid": "街机游戏",
    "LapisId": "ashvrtd"
}, {
    "ButtonImagePath": "pt/pc/ashvrth.jpg",
    "DisplayName": "虚拟赛马",
    "typeid": "街机游戏",
    "LapisId": "ashvrth"
}, {
    "ButtonImagePath": "pt/pc/bal.jpg",
    "DisplayName": "迷你百家乐",
    "typeid": "视讯扑克",
    "LapisId": "bal"
}, {
    "ButtonImagePath": "pt/pc/bj_mh5.jpg",
    "DisplayName": "UK Blackjack Multihand 5",
    "typeid": "老虎机",
    "LapisId": "bjuk_mh5"
}, {
    "ButtonImagePath": "pt/pc/chel.jpg",
    "DisplayName": "真人娱乐场扑克",
    "typeid": "视讯扑克",
    "LapisId": "chel"
}, {
    "ButtonImagePath": "pt/pc/dctw.jpg",
    "DisplayName": "转骰子游戏",
    "typeid": "街机游戏",
    "LapisId": "dctw"
}, {
    "ButtonImagePath": "pt/pc/drts.jpg",
    "DisplayName": "飞镖投掷赌博游戏",
    "typeid": "街机游戏",
    "LapisId": "drts"
}, {
    "ButtonImagePath": "pt/pc/dual_rol.jpg",
    "DisplayName": "双轮盘赌",
    "typeid": "视讯扑克",
    "LapisId": "dual_rol"
}, {
    "ButtonImagePath": "pt/pc/fbm.jpg",
    "DisplayName": "狂热足球",
    "typeid": "刮刮卡",
    "LapisId": "fbm"
}, {
    "ButtonImagePath": "pt/pc/fosl.jpg",
    "DisplayName": "固定赔率老虎机",
    "typeid": "街机游戏",
    "LapisId": "fosl"
}, {
    "ButtonImagePath": "pt/pc/frr.jpg",
    "DisplayName": "法式轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "frr"
}, {
    "ButtonImagePath": "pt/pc/frr_g.jpg",
    "DisplayName": "法式奖金轮盘赌",
    "typeid": "桌牌游戏",
    "LapisId": "frr_g"
}, {
    "ButtonImagePath": "pt/pc/fsc.jpg",
    "DisplayName": "决赛比分",
    "typeid": "街机游戏",
    "LapisId": "fsc"
}, {
    "ButtonImagePath": "pt/pc/fth.jpg",
    "DisplayName": "财富山",
    "typeid": "老虎机",
    "LapisId": "fth"
}, {
    "ButtonImagePath": "pt/pc/gc.jpg",
    "DisplayName": "地妖之穴",
    "typeid": "老虎机",
    "LapisId": "gc"
}, {
    "ButtonImagePath": "pt/pc/ghl.jpg",
    "DisplayName": "猜扑克牌游戏",
    "typeid": "街机游戏",
    "LapisId": "ghl"
}, {
    "ButtonImagePath": "pt/pc/glr.jpg",
    "DisplayName": "角斗士",
    "typeid": "老虎机",
    "LapisId": "glr"
}, {
    "ButtonImagePath": "pt/pc/gts36.jpg",
    "DisplayName": "灯之轮",
    "typeid": "街机游戏",
    "LapisId": "gts36"
}, {
    "ButtonImagePath": "pt/pc/gts37.jpg",
    "DisplayName": "角斗士刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "gts37"
}, {
    "ButtonImagePath": "pt/pc/gts46.jpg",
    "DisplayName": "生命之神",
    "typeid": "老虎机",
    "LapisId": "gts46"
}, {
    "ButtonImagePath": "pt/pc/gts47.jpg",
    "DisplayName": "Lotto Madness Scratch",
    "typeid": "刮刮卡",
    "LapisId": "gts47"
}, {
    "ButtonImagePath": "pt/pc/gts5.jpg",
    "DisplayName": "视频轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "gts5"
}, {
    "ButtonImagePath": "pt/pc/gts51.jpg",
    "DisplayName": "幸运熊猫",
    "typeid": "老虎机",
    "LapisId": "gts51"
}, {
    "ButtonImagePath": "pt/pc/gtsaod.jpg",
    "DisplayName": "无罪或诱惑",
    "typeid": "老虎机",
    "LapisId": "gtsaod"
}, {
    "ButtonImagePath": "pt/pc/gtsfj.jpg",
    "DisplayName": "跃龙门",
    "typeid": "老虎机",
    "LapisId": "gtsfj"
}, {
    "ButtonImagePath": "pt/pc/gtsftf.jpg",
    "DisplayName": "足球迷",
    "typeid": "老虎机",
    "LapisId": "gtsftf"
}, {
    "ButtonImagePath": "pt/pc/gtsgoc.jpg",
    "DisplayName": "圣诞节幽灵",
    "typeid": "老虎机",
    "LapisId": "gtsgoc"
}, {
    "ButtonImagePath": "pt/pc/gtshwkp.jpg",
    "DisplayName": "超级高速公路之王",
    "typeid": "老虎机",
    "LapisId": "gtshwkp"
}, {
    "ButtonImagePath": "pt/pc/gtsjzc.jpg",
    "DisplayName": "爵士俱乐部",
    "typeid": "老虎机",
    "LapisId": "gtsjzc"
}, {
    "ButtonImagePath": "pt/pc/gtslgms.jpg",
    "DisplayName": "野生游戏",
    "typeid": "老虎机",
    "LapisId": "gtslgms"
}, {
    "ButtonImagePath": "pt/pc/gtspwzsc.jpg",
    "DisplayName": "战争特区刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "gtspwzsc"
}, {
    "ButtonImagePath": "pt/pc/gtsrkysc.jpg",
    "DisplayName": "洛基传奇刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "gtsrkysc"
}, {
    "ButtonImagePath": "pt/pc/gtsrng.jpg",
    "DisplayName": "罗马与荣耀",
    "typeid": "老虎机",
    "LapisId": "gtsrng"
}, {
    "ButtonImagePath": "pt/pc/gtsru.jpg",
    "DisplayName": "财富魔方",
    "typeid": "街机游戏",
    "LapisId": "gtsru"
}, {
    "ButtonImagePath": "pt/pc/gtssmdm.jpg",
    "DisplayName": "无敌金刚",
    "typeid": "老虎机",
    "LapisId": "gtssmdm"
}, {
    "ButtonImagePath": "pt/pc/gtssmdsc.jpg",
    "DisplayName": "无敌金刚刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "gtssmdsc"
}, {
    "ButtonImagePath": "pt/pc/gtsstg.jpg",
    "DisplayName": "苏丹的财富",
    "typeid": "老虎机",
    "LapisId": "gtsstg"
}, {
    "ButtonImagePath": "pt/pc/gtsttlsc.jpg",
    "DisplayName": "顶级王牌传奇刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "gtsttlsc"
}, {
    "ButtonImagePath": "pt/pc/head.jpg",
    "DisplayName": "硬币投掷游戏",
    "typeid": "街机游戏",
    "LapisId": "head"
}, {
    "ButtonImagePath": "pt/pc/hpb.jpg",
    "DisplayName": "快乐虫",
    "typeid": "老虎机",
    "LapisId": "hpb"
}, {
    "ButtonImagePath": "pt/pc/hr.jpg",
    "DisplayName": "打比日",
    "typeid": "街机游戏",
    "LapisId": "hr"
}, {
    "ButtonImagePath": "pt/pc/hsd.jpg",
    "DisplayName": "摊牌扑克",
    "typeid": "街机游戏",
    "LapisId": "hsd"
}, {
    "ButtonImagePath": "pt/pc/iceh.jpg",
    "DisplayName": "冰球游戏",
    "typeid": "老虎机",
    "LapisId": "iceh"
}, {
    "ButtonImagePath": "pt/pc/jb.jpg",
    "DisplayName": "丛林摇摆",
    "typeid": "老虎机",
    "LapisId": "jb"
}, {
    "ButtonImagePath": "pt/pc/jb_mh.jpg",
    "DisplayName": "多手对J高手",
    "typeid": "视讯扑克",
    "LapisId": "jb_mh"
}, {
    "ButtonImagePath": "pt/pc/jp.jpg",
    "DisplayName": "王牌扑克",
    "typeid": "视讯扑克",
    "LapisId": "jp"
}, {
    "ButtonImagePath": "pt/pc/kgdb.jpg",
    "DisplayName": "德比王",
    "typeid": "街机游戏",
    "LapisId": "kgdb"
}, {
    "ButtonImagePath": "pt/pc/kkgsc.jpg",
    "DisplayName": "金刚刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "kkgsc"
}, {
    "ButtonImagePath": "pt/pc/kn.jpg",
    "DisplayName": "基诺",
    "typeid": "街机游戏",
    "LapisId": "kn"
}, {
    "ButtonImagePath": "pt/pc/lom.jpg",
    "DisplayName": "爱情配对",
    "typeid": "刮刮卡",
    "LapisId": "lom"
}, {
    "ButtonImagePath": "pt/pc/lwh.jpg",
    "DisplayName": "轮盘旋转赌博游戏",
    "typeid": "街机游戏",
    "LapisId": "lwh"
}, {
    "ButtonImagePath": "pt/pc/mcb.jpg",
    "DisplayName": "Cash back先生",
    "typeid": "老虎机",
    "LapisId": "mcb"
}, {
    "ButtonImagePath": "pt/pc/mfbr.jpg",
    "DisplayName": "迷你终极足球",
    "typeid": "迷你游戏",
    "LapisId": "mfbr"
}, {
    "ButtonImagePath": "pt/pc/mfdt.jpg",
    "DisplayName": "迷你戴图理",
    "typeid": "迷你游戏",
    "LapisId": "mfdt"
}, {
    "ButtonImagePath": "pt/pc/mglr.jpg",
    "DisplayName": "迷你角斗士",
    "typeid": "迷你游戏",
    "LapisId": "mglr"
}, {
    "ButtonImagePath": "pt/pc/mnkt.jpg",
    "DisplayName": "霹雳神猴",
    "typeid": "街机游戏",
    "LapisId": "mnkt"
}, {
    "ButtonImagePath": "pt/pc/mro.jpg",
    "DisplayName": "迷你轮盘赌",
    "typeid": "街机游戏",
    "LapisId": "mro"
}, {
    "ButtonImagePath": "pt/pc/mro_g.jpg",
    "DisplayName": "轮盘赌(mini)",
    "typeid": "迷你游戏",
    "LapisId": "mro_g"
}, {
    "ButtonImagePath": "pt/pc/ms.jpg",
    "DisplayName": "魔幻吃角子老虎",
    "typeid": "老虎机",
    "LapisId": "ms"
}, {
    "ButtonImagePath": "pt/pc/pbro.jpg",
    "DisplayName": "弹球轮盘",
    "typeid": "街机游戏",
    "LapisId": "pbro"
}, {
    "ButtonImagePath": "pt/pc/pep.jpg",
    "DisplayName": "Pick 'em Poker ",
    "typeid": "桌牌游戏",
    "LapisId": "pep"
}, {
    "ButtonImagePath": "pt/pc/pg.jpg",
    "DisplayName": "牌九扑克",
    "typeid": "桌牌游戏",
    "LapisId": "pg"
}, {
    "ButtonImagePath": "pt/pc/pks.jpg",
    "DisplayName": "法老王国划刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "pks"
}, {
    "ButtonImagePath": "pt/pc/pl.jpg",
    "DisplayName": "舞线",
    "typeid": "老虎机",
    "LapisId": "pl"
}, {
    "ButtonImagePath": "pt/pc/po.jpg",
    "DisplayName": "对J高手",
    "typeid": "视讯扑克",
    "LapisId": "po"
}, {
    "ButtonImagePath": "pt/pc/pon_mh5.jpg",
    "DisplayName": "Pontoon Multihand 5 ",
    "typeid": "桌牌游戏",
    "LapisId": "pon_mh5"
}, {
    "ButtonImagePath": "pt/pc/pop.jpg",
    "DisplayName": "宾果",
    "typeid": "街机游戏",
    "LapisId": "pop"
}, {
    "ButtonImagePath": "pt/pc/pso.jpg",
    "DisplayName": "罚点球游戏",
    "typeid": "街机游戏",
    "LapisId": "pso"
}, {
    "ButtonImagePath": "pt/pc/pyrrk.jpg",
    "DisplayName": "拉美西斯的金字塔",
    "typeid": "老虎机",
    "LapisId": "pyrrk"
}, {
    "ButtonImagePath": "pt/pc/qop.jpg",
    "DisplayName": "金字塔女王",
    "typeid": "老虎机",
    "LapisId": "qop"
}, {
    "ButtonImagePath": "pt/pc/rcd.jpg",
    "DisplayName": "掷骰子赌博游戏",
    "typeid": "街机游戏",
    "LapisId": "rcd"
}, {
    "ButtonImagePath": "pt/pc/rd.jpg",
    "DisplayName": "红狗",
    "typeid": "桌牌游戏",
    "LapisId": "rd"
}, {
    "ButtonImagePath": "pt/pc/rnr.jpg",
    "DisplayName": "摇摆舞",
    "typeid": "老虎机",
    "LapisId": "rnr"
}, {
    "ButtonImagePath": "pt/pc/romw.jpg",
    "DisplayName": "多轮轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "romw"
}, {
    "ButtonImagePath": "pt/pc/rop.jpg",
    "DisplayName": "超级轮盘",
    "typeid": "桌牌游戏",
    "LapisId": "rop"
}, {
    "ButtonImagePath": "pt/pc/rop_g.jpg",
    "DisplayName": "奖金轮盘赌专家",
    "typeid": "桌牌游戏",
    "LapisId": "rop_g"
}, {
    "ButtonImagePath": "pt/pc/rps.jpg",
    "DisplayName": "石头、剪刀、纸游戏",
    "typeid": "街机游戏",
    "LapisId": "rps"
}, {
    "ButtonImagePath": "pt/pc/sb.jpg",
    "DisplayName": "骰宝",
    "typeid": "桌牌游戏",
    "LapisId": "sb"
}, {
    "ButtonImagePath": "pt/pc/sbl.jpg",
    "DisplayName": "真人骰子",
    "typeid": "视讯扑克",
    "LapisId": "sbl"
}, {
    "ButtonImagePath": "pt/pc/sc.jpg",
    "DisplayName": "保险箱探宝",
    "typeid": "老虎机",
    "LapisId": "sc"
}, {
    "ButtonImagePath": "pt/pc/sf.jpg",
    "DisplayName": "苏丹的财富",
    "typeid": "老虎机",
    "LapisId": "sf"
}, {
    "ButtonImagePath": "pt/pc/snsb.jpg",
    "DisplayName": "Sunset Beach",
    "typeid": "老虎机",
    "LapisId": "snsb"
}, {
    "ButtonImagePath": "pt/pc/sro.jpg",
    "DisplayName": "轮盘赌刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "sro"
}, {
    "ButtonImagePath": "pt/pc/ssa.jpg",
    "DisplayName": "圣诞刮刮乐",
    "typeid": "刮刮卡",
    "LapisId": "ssa"
}, {
    "ButtonImagePath": "pt/pc/ssl.jpg",
    "DisplayName": "转轴经典3",
    "typeid": "老虎机",
    "LapisId": "ssl"
}, {
    "ButtonImagePath": "pt/pc/str.jpg",
    "DisplayName": "异乎寻常",
    "typeid": "桌牌游戏",
    "LapisId": "str"
}, {
    "ButtonImagePath": "pt/pc/ta.jpg",
    "DisplayName": "三个朋友",
    "typeid": "老虎机",
    "LapisId": "ta"
}, {
    "ButtonImagePath": "pt/pc/tfs.jpg",
    "DisplayName": "顶级王牌-全星",
    "typeid": "老虎机",
    "LapisId": "tfs"
}, {
    "ButtonImagePath": "pt/pc/tglalcs.jpg",
    "DisplayName": "炼金术士的法术",
    "typeid": "老虎机",
    "LapisId": "tglalcs"
}, {
    "ButtonImagePath": "pt/pc/thtk.jpg",
    "DisplayName": "泰寺",
    "typeid": "老虎机",
    "LapisId": "thtk"
}, {
    "ButtonImagePath": "pt/pc/tob.jpg",
    "DisplayName": "对十高手",
    "typeid": "视讯扑克",
    "LapisId": "tob"
}, {
    "ButtonImagePath": "pt/pc/tp.jpg",
    "DisplayName": "三倍利润",
    "typeid": "老虎机",
    "LapisId": "tp"
}, {
    "ButtonImagePath": "pt/pc/tps.jpg",
    "DisplayName": "戴图理神奇7",
    "typeid": "街机游戏",
    "LapisId": "tps"
}, {
    "ButtonImagePath": "pt/pc/tqp.jpg",
    "DisplayName": "龙舌兰扑克",
    "typeid": "桌牌游戏",
    "LapisId": "tqp"
}, {
    "ButtonImagePath": "pt/pc/tr.jpg",
    "DisplayName": "热带滚筒",
    "typeid": "老虎机",
    "LapisId": "tr"
}, {
    "ButtonImagePath": "pt/pc/trl.jpg",
    "DisplayName": "真爱",
    "typeid": "老虎机",
    "LapisId": "trl"
}, {
    "ButtonImagePath": "pt/pc/ts.jpg",
    "DisplayName": "时空过客",
    "typeid": "老虎机",
    "LapisId": "ts"
}, {
    "ButtonImagePath": "pt/pc/tst.jpg",
    "DisplayName": "网球明星",
    "typeid": "老虎机",
    "LapisId": "tst"
}, {
    "ButtonImagePath": "pt/pc/ttc.jpg",
    "DisplayName": "顶级王牌-明星",
    "typeid": "老虎机",
    "LapisId": "ttc"
}, {
    "ButtonImagePath": "pt/pc/ttwfs.jpg",
    "DisplayName": "Top Trumps Football Stars 2014",
    "typeid": "老虎机",
    "LapisId": "ttwfs"
}, {
    "ButtonImagePath": "pt/pc/ub.jpg",
    "DisplayName": "丛林巫师",
    "typeid": "老虎机",
    "LapisId": "ub"
}, {
    "ButtonImagePath": "pt/pc/vbal.jpg",
    "DisplayName": "VIP百家乐",
    "typeid": "视讯扑克",
    "LapisId": "vbal"
}, {
    "ButtonImagePath": "pt/pc/wsffr.jpg",
    "DisplayName": "玩转华尔街",
    "typeid": "老虎机",
    "LapisId": "wsffr"
}, {
    "ButtonImagePath": "pt/pc/wv.jpg",
    "DisplayName": "野蛮海盗",
    "typeid": "桌牌游戏",
    "LapisId": "wv"
}];

var agdata = [ {
	"DisplayName" : "捕鱼王者",
	"ButtonImagePath" : "SB36.gif",
	"LapisId" : "6",
	"typeid" : "4"
},{
	"DisplayName" : "赌城之夜",
	"ButtonImagePath" : "SB56_ZH.png",
	"LapisId" : "SB56",
	"typeid" : "4"
},{
	"DisplayName" : "极品飙车",
	"ButtonImagePath" : "WH54_ZH.png",
	"LapisId" : "WHGWH54",
	"typeid" : "4"
},{
	"DisplayName" : "寿司大食客",
	"ButtonImagePath" : "WH26_ZH.png",
	"LapisId" : "WHGWH26",
	"typeid" : "4"
},{
	"DisplayName" : "皇家戏台",
	"ButtonImagePath" : "WH40_ZH.png",
	"LapisId" : "WHGWH40",
	"typeid" : "4"
},{
	"DisplayName" : "蒸汽战争",
	"ButtonImagePath" : "WH44_ZH.png",
	"LapisId" : "WHGWH44",
	"typeid" : "4"
},{
	"DisplayName" : "魅惑魔女",
	"ButtonImagePath" : "SB58_ZH.png",
	"LapisId" : "169",
	"typeid" : "4"
},{
	"DisplayName" : "橫行霸道",
	"ButtonImagePath" : "WH36_ZH.png",
	"LapisId" : "WHGWH36",
	"typeid" : "4"
},{
	"DisplayName" : "古惑仔",
	"ButtonImagePath" : "WH42_ZH.png",
	"LapisId" : "WHGWH42",
	"typeid" : "4"
},{
	"DisplayName" : "神奇宠物",
	"ButtonImagePath" : "WH55_ZH.png",
	"LapisId" : "WHGWH55",
	"typeid" : "4"
},{
	"DisplayName" : "魔龙",
	"ButtonImagePath" : "SB57_ZH.png",
	"LapisId" : "168",
	"typeid" : "4"
},{
	"DisplayName" : "财宝塔罗",
	"ButtonImagePath" : "WH19_ZH.png",
	"LapisId" : "WHGWH19",
	"typeid" : "4"
},{
	"DisplayName" : "埃及宝藏",
	"ButtonImagePath" : "WH28_ZH.png",
	"LapisId" : "WHGWH28",
	"typeid" : "4"
},{
	"DisplayName" : "封神演义",
	"ButtonImagePath" : "WH23_ZH.png",
	"LapisId" : "WHGWH23",
	"typeid" : "4"
},{
	"DisplayName" : "和风剧院",
	"ButtonImagePath" : "WH27_ZH.png",
	"LapisId" : "WHGWH27",
	"typeid" : "4"
},{
	"DisplayName" : "点石成金",
	"ButtonImagePath" : "WH30_ZH.png",
	"LapisId" : "WHGWH30",
	"typeid" : "4"
},{
	"DisplayName" : "圣女贞德",
	"ButtonImagePath" : "WH02_ZH.png",
	"LapisId" : "WHGWH02",
	"typeid" : "4"
},{
	"DisplayName" : "五狮进宝",
	"ButtonImagePath" : "WH07_ZH.png",
	"LapisId" : "WHGWH07",
	"typeid" : "4"
},{
	"DisplayName" : "发财熊猫",
	"ButtonImagePath" : "WH12_ZH.png",
	"LapisId" : "WHGWH12",
	"typeid" : "4"
},{
	"DisplayName" : "十二生肖",
	"ButtonImagePath" : "WH38_ZH.png",
	"LapisId" : "WHGWH38",
	"typeid" : "4"
},{
	"DisplayName" : "招财锦鲤",
	"ButtonImagePath" : "WH35_ZH.png",
	"LapisId" : "WHGWH35",
	"typeid" : "4"
},{
	"DisplayName" : "白雪公主",
	"ButtonImagePath" : "WH18_ZH.png",
	"LapisId" : "WHGWH18",
	"typeid" : "4"
},{
	"DisplayName" : "葫芦兄弟",
	"ButtonImagePath" : "WH20_ZH.png",
	"LapisId" : "WHGWH20",
	"typeid" : "4"
},{
	"DisplayName" : "内衣橄榄球",
	"ButtonImagePath" : "WH34_ZH.png",
	"LapisId" : "WHGWH34",
	"typeid" : "4"
},{
	"DisplayName" : "贪玩蓝月",
	"ButtonImagePath" : "WH32_ZH.png",
	"LapisId" : "WHGWH32",
	"typeid" : "4"
},{
	"DisplayName" : "多宝鱼虾蟹",
	"ButtonImagePath" : "SB55_ZH.png",
	"LapisId" : "166",
	"typeid" : "4"
},{
	"DisplayName" : "跳跳乐",
	"ButtonImagePath" : "WC01_ZH.png",
	"LapisId" : "WHGWC01",
	"typeid" : "4"
},{
	"DisplayName" : "永恒之吻",
	"ButtonImagePath" : "WH21_ZH.png",
	"LapisId" : "WHGWH21",
	"typeid" : "4"
},{
	"DisplayName" : "恐怖嘉年华",
	"ButtonImagePath" : "WH22_ZH.png",
	"LapisId" : "WHGWH22",
	"typeid" : "4"
},{
	"DisplayName" : "僵尸来袭",
	"ButtonImagePath" : "WH24_ZH.png",
	"LapisId" : "WHGWH24",
	"typeid" : "4"
},{
	"DisplayName" : "狂野女巫",
	"ButtonImagePath" : "WH29_ZH.png",
	"LapisId" : "WHGWH29",
	"typeid" : "4"
},{
	"DisplayName" : "嫦娥奔月",
	"ButtonImagePath" : "WH17_ZH.png",
	"LapisId" : "WHGWH17",
	"typeid" : "4"
},{
	"DisplayName" : "钻石女王",
	"ButtonImagePath" : "WA01_ZH.png",
	"LapisId" : "WHGWA01",
	"typeid" : "4"
},{
	"DisplayName" : "亚瑟王",
	"ButtonImagePath" : "WH06_ZH.png",
	"LapisId" : "WHGWH06",
	"typeid" : "4"
},{
	"DisplayName" : "爱丽丝大冒险",
	"ButtonImagePath" : "WH10_ZH.png",
	"LapisId" : "WHGWH10",
	"typeid" : "4"
},{
	"DisplayName" : "战火风云",
	"ButtonImagePath" : "WH11_ZH.png",
	"LapisId" : "WHGWH11",
	"typeid" : "4"
},{
	"DisplayName" : "穆夏女神",
	"ButtonImagePath" : "WH04_MuchaGoddesses.png",
	"LapisId" : "WHGWH04",
	"typeid" : "4"
},{
	"DisplayName" : "聚宝龙",
	"ButtonImagePath" : "TA1U_Fortune_Dragon.png",
	"LapisId" : "TAITA1U",
	"typeid" : "4"
},{
	"DisplayName" : "龙虎",
	"ButtonImagePath" : "EP02_DragonTiger.png",
	"LapisId" : "EP02",
	"typeid" : "4"
},{
	"DisplayName" : "冠军足球",
	"ButtonImagePath" : "WH03.png",
	"LapisId" : "WHGWH03",
	"typeid" : "4"
},{
	"DisplayName" : "王牌多手21点",
	"ButtonImagePath" : "TA1Q.png",
	"LapisId" : "TAITA1Q",
	"typeid" : "4"
},{
	"DisplayName" : "王者传说",
	"ButtonImagePath" : "SB51.png",
	"LapisId" : "SB51",
	"typeid" : "4"
}, {
	"DisplayName" : "骰宝",
	"ButtonImagePath" : "EP03.png",
	"LapisId" : "EP03",
	"typeid" : "4"
},{
	"DisplayName" : "刺激战场",
	"DisplayEnName" : "Battle Field",
	"FTP_gametype" : "YMBF",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymbf.png",
	"LapisId" : "YP826"
}, {
	"DisplayName" : "斗三公",
	"DisplayEnName" : "San Gong",
	"FTP_gametype" : "YMSG",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ymsg.png",
	"LapisId" : "YP825"
} , {
	"DisplayName" : "水果拉霸",
	"ButtonImagePath" : "FRU.gif",
	"LapisId" : "501",
	"typeid" : "3"
}, {
	"DisplayName" : "杰克高手",
	"ButtonImagePath" : "PKBJ.gif",
	"LapisId" : "502",
	"typeid" : "3"
}, {
	"DisplayName" : "极速幸运轮",
	"ButtonImagePath" : "TGLW.gif",
	"LapisId" : "507",
	"typeid" : "2"
}, {
	"DisplayName" : "太空漫游",
	"ButtonImagePath" : "SB01.gif",
	"LapisId" : "508",
	"typeid" : "3"
}, {
	"DisplayName" : "复古花园",
	"ButtonImagePath" : "SB02.gif",
	"LapisId" : "509",
	"typeid" : "3"
}, {
	"DisplayName" : "关东煮",
	"ButtonImagePath" : "SB03.jpg",
	"LapisId" : "510",
	"typeid" : "4"
}, {
	"DisplayName" : "牧场咖啡",
	"ButtonImagePath" : "SB04.jpg",
	"LapisId" : "511",
	"typeid" : "4"
}, {
	"DisplayName" : "甜一甜屋",
	"ButtonImagePath" : "SB05.jpg",
	"LapisId" : "512",
	"typeid" : "4"
}, {
	"DisplayName" : "日本武士",
	"ButtonImagePath" : "SB06.jpg",
	"LapisId" : "513",
	"typeid" : "4"
}, {
	"DisplayName" : "象棋老虎机",
	"ButtonImagePath" : "SB07.jpg",
	"LapisId" : "514",
	"typeid" : "4"
}, {
	"DisplayName" : "麻将老虎机",
	"ButtonImagePath" : "SB08.jpg",
	"LapisId" : "515",
	"typeid" : "4"
}, {
	"DisplayName" : "西洋棋老虎机",
	"ButtonImagePath" : "SB09.jpg",
	"LapisId" : "516",
	"typeid" : "4"
}, {
	"DisplayName" : "开心农场",
	"ButtonImagePath" : "SB10.jpg",
	"LapisId" : "517",
	"typeid" : "4"
}, {
	"DisplayName" : "夏日营地",
	"ButtonImagePath" : "SB11.jpg",
	"LapisId" : "518",
	"typeid" : "4"
}, {
	"DisplayName" : "海底漫游",
	"ButtonImagePath" : "SB12.jpg",
	"LapisId" : "519",
	"typeid" : "4"
}, {
	"DisplayName" : "鬼马小丑",
	"ButtonImagePath" : "SB13.jpg",
	"LapisId" : "520",
	"typeid" : "4"
}, {
	"DisplayName" : "机动乐园",
	"ButtonImagePath" : "SB14.jpg",
	"LapisId" : "521",
	"typeid" : "4"
}, {
	"DisplayName" : "惊吓鬼屋",
	"ButtonImagePath" : "SB15.jpg",
	"LapisId" : "522",
	"typeid" : "4"
}, {
	"DisplayName" : "疯狂马戏团",
	"ButtonImagePath" : "SB16.jpg",
	"LapisId" : "523",
	"typeid" : "4"
}, {
	"DisplayName" : "海洋剧场",
	"ButtonImagePath" : "SB17.jpg",
	"LapisId" : "524",
	"typeid" : "4"
}, {
	"DisplayName" : "水上乐园",
	"ButtonImagePath" : "SB18.jpg",
	"LapisId" : "525",
	"typeid" : "4"
}, {
	"DisplayName" : "空中战争",
	"ButtonImagePath" : "SB19.jpg",
	"LapisId" : "526",
	"typeid" : "4"
}, {
	"DisplayName" : "摇滚狂迷",
	"ButtonImagePath" : "SB20.jpg",
	"LapisId" : "527",
	"typeid" : "4"
}, {
	"DisplayName" : "越野机车",
	"ButtonImagePath" : "SB21.jpg",
	"LapisId" : "528",
	"typeid" : "4"
}, {
	"DisplayName" : "埃及奥秘",
	"ButtonImagePath" : "SB22.jpg",
	"LapisId" : "529",
	"typeid" : "4"
}, {
	"DisplayName" : "欢乐时光",
	"ButtonImagePath" : "SB23.png",
	"LapisId" : "530",
	"typeid" : "4"
}, {
	"DisplayName" : "侏罗纪",
	"ButtonImagePath" : "SB24.jpg",
	"LapisId" : "531",
	"typeid" : "4"
}, {
	"DisplayName" : "土地神",
	"ButtonImagePath" : "SB25.jpg",
	"LapisId" : "532",
	"typeid" : "4"
}, {
	"DisplayName" : "布袋和尚",
	"ButtonImagePath" : "SB26.jpg",
	"LapisId" : "533",
	"typeid" : "4"
}, {
	"DisplayName" : "正财神",
	"ButtonImagePath" : "SB27.jpg",
	"LapisId" : "534",
	"typeid" : "4"
}, {
	"DisplayName" : "武财神",
	"ButtonImagePath" : "SB28.jpg",
	"LapisId" : "535",
	"typeid" : "4"
}, {
	"DisplayName" : "偏财神",
	"ButtonImagePath" : "SB29.jpg",
	"LapisId" : "536",
	"typeid" : "4"
}, {
	"DisplayName" : "天空守护者",
	"ButtonImagePath" : "SB31.jpg",
	"LapisId" : "542",
	"typeid" : "3"
}, {
	"DisplayName" : "齐天大圣",
	"ButtonImagePath" : "SB32.gif",
	"LapisId" : "543",
	"typeid" : "2"
}, {
	"DisplayName" : "冰河世界",
	"ButtonImagePath" : "SB34.jpg",
	"LapisId" : "545",
	"typeid" : "4"
}, {
	"DisplayName" : "欧洲列强争霸",
	"ButtonImagePath" : "SB35.gif",
	"LapisId" : "547",
	"typeid" : "4"
}, {
	"DisplayName" : "上海百乐门",
	"ButtonImagePath" : "SB37.jpg",
	"LapisId" : "549",
	"typeid" : "2"
}, {
	"DisplayName" : "性感女僕",
	"ButtonImagePath" : "AV01.jpg",
	"LapisId" : "537",
	"typeid" : "3"
}, {
	"DisplayName" : "百搭二王",
	"ButtonImagePath" : "PKBD.gif",
	"LapisId" : "540",
	"typeid" : "2"
}, {
	"DisplayName" : "红利百搭",
	"ButtonImagePath" : "PKBB.gif",
	"LapisId" : "541",
	"typeid" : "2"
}, {
	"DisplayName" : "水果拉霸2",
	"ButtonImagePath" : "FRU2.gif",
	"LapisId" : "546",
	"typeid" : "2"
}, {
	"DisplayName" : "龙珠",
	"ButtonImagePath" : "XG01.jpg",
	"LapisId" : "600",
	"typeid" : "4"
}, {
	"DisplayName" : "金鱼",
	"ButtonImagePath" : "XG04.jpg",
	"LapisId" : "603",
	"typeid" : "2"
}, {
	"DisplayName" : "中国新年",
	"ButtonImagePath" : "XG05.jpg",
	"LapisId" : "604",
	"typeid" : "2"
}, {
	"DisplayName" : "阿里巴巴",
	"DisplayEnName" : "Alibaba",
	"category" : "老虎机",
	"ButtonImagePath" : "WH01.jpg",
	"LapisId" : "WHGWH01"
}, {
	"DisplayName" : "神奇宝石",
	"DisplayEnName" : "Mystic Gems",
	"category" : "老虎机",
	"ButtonImagePath" : "SB47.jpg",
	"LapisId" : "158"
}, {
	"DisplayName" : "XIN哥来了",
	"DisplayEnName" : "Mr XIN",
	"category" : "老虎机",
	"ButtonImagePath" : "SB50.jpg",
	"LapisId" : "161"
}, {
	"DisplayName" : "五行世界",
	"DisplayEnName" : "Five Elements",
	"category" : "老虎机",
	"ButtonImagePath" : "DTA8.jpg",
	"LapisId" : "DTGDTA8"
}, {
	"DisplayName" : "梦幻森林",
	"DisplayEnName" : "Fantasy Forest",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAB.jpg",
	"LapisId" : "DTGDTAB"
}, {
	"DisplayName" : "财神到",
	"DisplayEnName" : "Wealthy God Arriving",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAF.jpg",
	"LapisId" : "DTGDTAF"
}, {
	"DisplayName" : "新年到",
	"DisplayEnName" : "Happy New Year",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAG.jpg",
	"LapisId" : "DTGDTAG"
}, {
	"DisplayName" : "龙凤呈祥",
	"DisplayEnName" : "DragonPhoenix Prosper",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAQ.jpg",
	"LapisId" : "DTGDTAQ"
}, {
	"DisplayName" : "福禄寿",
	"DisplayEnName" : "FULUSHOU",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAT.jpg",
	"LapisId" : "DTGDTAT"
}, {
	"DisplayName" : "金龙珠",
	"DisplayEnName" : "The Dragon Pearl Gold",
	"category" : "老虎机",
	"ButtonImagePath" : "SB49.jpg",
	"LapisId" : "160"
}, {
	"DisplayName" : "猛龙传奇",
	"DisplayEnName" : "Legend of the Dragon",
	"category" : "老虎机",
	"ButtonImagePath" : "SB45.jpg",
	"LapisId" : "156"
}, {
	"DisplayName" : "英雄荣耀",
	"DisplayEnName" : "Glory of Heroes",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAR.jpg",
	"LapisId" : "DTGDTAR"
}, {
	"DisplayName" : "快乐农庄",
	"DisplayEnName" : "Happy Farm",
	"category" : "老虎机",
	"ButtonImagePath" : "DTB1.jpg",
	"LapisId" : "DTGDTB1"
}, {
	"DisplayName" : "封神榜",
	"DisplayEnName" : "Investiture of Gods",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAM.jpg",
	"LapisId" : "DTGDTAM"
}, {
	"DisplayName" : "摇滚之夜",
	"DisplayEnName" : "Rocknight",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAZ.jpg",
	"LapisId" : "DTGDTAZ"
}, {
	"DisplayName" : "金拉霸",
	"DisplayEnName" : "Gold Class",
	"category" : "老虎机",
	"ButtonImagePath" : "SC03.jpg",
	"LapisId" : "802"
}, {
	"DisplayName" : "街头烈战",
	"DisplayEnName" : "Street Battle",
	"category" : "老虎机",
	"ButtonImagePath" : "SX02.jpg",
	"LapisId" : "221"
}, {
	"DisplayName" : "赛亚烈战",
	"DisplayEnName" : "Saiyan Battle Z",
	"category" : "老虎机",
	"ButtonImagePath" : "DTA0.jpg",
	"LapisId" : "DTGDTA0"
}, {
	"DisplayName" : "欧洲轮盘（桌面版）",
	"DisplayEnName" : "Roulette Deluxe (desktop)",
	"category" : "其他",
	"ButtonImagePath" : "TA1L.jpg",
	"LapisId" : "TAITA1L"
}, {
	"DisplayName" : "龙舟竞渡",
	"DisplayEnName" : "Dragon Boat Festival",
	"category" : "老虎机",
	"ButtonImagePath" : "XG10.gif",
	"LapisId" : "209"
}, {
	"DisplayName" : "中秋佳节",
	"DisplayEnName" : "Moon Festival",
	"category" : "老虎机",
	"ButtonImagePath" : "XG11.gif",
	"LapisId" : "210"
}, {
	"DisplayName" : "韩风劲舞",
	"DisplayEnName" : "K-Pop",
	"category" : "老虎机",
	"ButtonImagePath" : "XG12.gif",
	"LapisId" : "211"
}, {
	"DisplayName" : "美女大格斗",
	"DisplayEnName" : "Fantasies",
	"category" : "老虎机",
	"ButtonImagePath" : "XG13.gif",
	"LapisId" : "212"
}, {
	"DisplayName" : "黄金对垒",
	"DisplayEnName" : "Kick Off",
	"category" : "老虎机",
	"ButtonImagePath" : "XG16.gif",
	"LapisId" : "215"
}, {
	"DisplayName" : "冰河世界",
	"DisplayEnName" : "Ice Crush",
	"category" : "老虎机",
	"ButtonImagePath" : "SB34.jpg",
	"LapisId" : "145"
}, {
	"DisplayName" : "糖果碰碰乐",
	"DisplayEnName" : "Candy Quest",
	"category" : "老虎机",
	"ButtonImagePath" : "SB33.gif",
	"LapisId" : "144"
}, {
	"DisplayName" : "大豪客",
	"DisplayEnName" : "High Roller",
	"category" : "老虎机",
	"ButtonImagePath" : "XG09.gif",
	"LapisId" : "208"
}, {
	"DisplayName" : "龙珠",
	"DisplayEnName" : "Dragons Pearl",
	"category" : "老虎机",
	"ButtonImagePath" : "XG01.jpg",
	"LapisId" : "200"
}, {
	"DisplayName" : "幸运8",
	"DisplayEnName" : "lucky 8",
	"category" : "老虎机",
	"ButtonImagePath" : "XG02.jpg",
	"LapisId" : "201"
}, {
	"DisplayName" : "闪亮女郎",
	"DisplayEnName" : "Bling Bling",
	"category" : "老虎机",
	"ButtonImagePath" : "XG03.jpg",
	"LapisId" : "202"
}, {
	"DisplayName" : "海盗王",
	"DisplayEnName" : "Pirates",
	"category" : "老虎机",
	"ButtonImagePath" : "XG06.jpg",
	"LapisId" : "205"
}, {
	"DisplayName" : "鲜果狂热",
	"DisplayEnName" : "Fruitmania",
	"category" : "老虎机",
	"ButtonImagePath" : "XG07.jpg",
	"LapisId" : "206"
}, {
	"DisplayName" : "小熊猫",
	"DisplayEnName" : "Red Panda",
	"category" : "老虎机",
	"ButtonImagePath" : "XG08.jpg",
	"LapisId" : "207"
}, {
	"DisplayName" : "森林舞会",
	"DisplayEnName" : "Forest Dance",
	"FTP_gametype" : "YFD",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfd.png",
	"LapisId" : "YP801"
}, {
	"DisplayName" : "奔驰宝马",
	"DisplayEnName" : "Benz",
	"FTP_gametype" : "YBEN",
	"category" : "单人游戏",
	"ButtonImagePath" : "yben.png",
	"LapisId" : "YP802"
}, {
	"DisplayName" : "極速赛马",
	"DisplayEnName" : "Run Horse",
	"FTP_gametype" : "YHR",
	"category" : "单人游戏",
	"ButtonImagePath" : "yhr.png",
	"LapisId" : "YP803"
}, {
	"DisplayName" : "猜猜乐",
	"DisplayEnName" : "Guess",
	"FTP_gametype" : "YGS",
	"category" : "单人游戏",
	"ButtonImagePath" : "ygs.png",
	"LapisId" : "YP804"
}, {
	"DisplayName" : "水果拉霸",
	"DisplayEnName" : "Fruit Slot",
	"FTP_gametype" : "YFR",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfr.png",
	"LapisId" : "YP805"
}, {
	"DisplayName" : "德州牛仔",
	"DisplayEnName" : "Texas Cowboy",
	"FTP_gametype" : "YDZ",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ydz.png",
	"LapisId" : "YP806"
}, {
	"DisplayName" : "飞禽走兽",
	"DisplayEnName" : "Birds & Beasts",
	"FTP_gametype" : "YBIR",
	"category" : "单人游戏",
	"ButtonImagePath" : "ybir.png",
	"LapisId" : "YP807"
}, {
	"DisplayName" : "水果派对",
	"DisplayEnName" : "Fruit Party",
	"FTP_gametype" : "YFP",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfp.png",
	"LapisId" : "YP808"
}, {
	"DisplayName" : "森林舞会多人版",
	"DisplayEnName" : "Forest Dance Multiplayer",
	"FTP_gametype" : "YMFD",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymfd.png",
	"LapisId" : "YP810"
}, {
	"DisplayName" : "水果拉霸多人版",
	"DisplayEnName" : "Fruit Slot Multiplayer",
	"FTP_gametype" : "YMFR",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymfr.png",
	"LapisId" : "YP813"
}, {
	"DisplayName" : "百人牛牛",
	"DisplayEnName" : "Yo Bull",
	"FTP_gametype" : "YMBN",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ymbn.png",
	"LapisId" : "YP817"
}, {
	"DisplayName" : "多宝水果拉霸",
	"DisplayEnName" : "Treasure Fruit slot",
	"FTP_gametype" : "YGFS",
	"category" : "单人游戏",
	"ButtonImagePath" : "ygfs.png",
	"LapisId" : "YP818"
}, {
	"DisplayName" : "彩金水果拉霸",
	"DisplayEnName" : "Jackpot Fruit Slot",
	"FTP_gametype" : "YJFS",
	"category" : "单人游戏",
	"ButtonImagePath" : "yjfs.png",
	"LapisId" : "YP820"
}, {
	"DisplayName" : "飞禽走兽多人版",
	"DisplayEnName" : "Birds and Beasts Multiplayer",
	"FTP_gametype" : "YMBI",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymbi.png",
	"LapisId" : "YP814"
}, {
	"DisplayName" : "牛牛对战",
	"DisplayEnName" : "Bull Battle",
	"FTP_gametype" : "YMBA",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ymba.png",
	"LapisId" : "YP821"
}, {
	"DisplayName" : "奔驰宝马多人版",
	"DisplayEnName" : "Benz Multiplayer",
	"FTP_gametype" : "YMBZ",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymbz.png",
	"LapisId" : "YP811"
}, {
	"DisplayName" : "动物狂欢",
	"DisplayEnName" : "Animal Carnival",
	"FTP_gametype" : "YMAC",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymac.png",
	"LapisId" : "YP823"
}, {
	"DisplayName" : "西游争霸",
	"DisplayEnName" : "Western Journey",
	"FTP_gametype" : "YMJW",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymjw.png",
	"LapisId" : "YP824"
}, {
	"DisplayName" : "翻倍炸金花",
	"DisplayEnName" : "Double Win 3 Cards",
	"FTP_gametype" : "YMJH",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymjh.png",
	"LapisId" : "YP822"
} ]

var agnewdata = [ {
	"DisplayName" : "赌城之夜",
	"ButtonImagePath" : "SB56_ZH.png",
	"LapisId" : "SB56",
	"typeid" : "4"
},{
	"DisplayName" : "极品飙车",
	"ButtonImagePath" : "WH54_ZH.png",
	"LapisId" : "WHGWH54",
	"typeid" : "4"
},{
	"DisplayName" : "寿司大食客",
	"ButtonImagePath" : "WH26_ZH.png",
	"LapisId" : "WHGWH26",
	"typeid" : "4"
},{
	"DisplayName" : "皇家戏台",
	"ButtonImagePath" : "WH40_ZH.png",
	"LapisId" : "WHGWH40",
	"typeid" : "4"
},{
	"DisplayName" : "蒸汽战争",
	"ButtonImagePath" : "WH44_ZH.png",
	"LapisId" : "WHGWH44",
	"typeid" : "4"
},{
	"DisplayName" : "魅惑魔女",
	"ButtonImagePath" : "SB58_ZH.png",
	"LapisId" : "169",
	"typeid" : "4"
},{
	"DisplayName" : "橫行霸道",
	"ButtonImagePath" : "WH36_ZH.png",
	"LapisId" : "WHGWH36",
	"typeid" : "4"
},{
	"DisplayName" : "古惑仔",
	"ButtonImagePath" : "WH42_ZH.png",
	"LapisId" : "WHGWH42",
	"typeid" : "4"
},{
	"DisplayName" : "神奇宠物",
	"ButtonImagePath" : "WH55_ZH.png",
	"LapisId" : "WHGWH55",
	"typeid" : "4"
},{
	"DisplayName" : "魔龙",
	"ButtonImagePath" : "SB57_ZH.png",
	"LapisId" : "168",
	"typeid" : "4"
},{
	"DisplayName" : "财宝塔罗",
	"ButtonImagePath" : "WH19_ZH.png",
	"LapisId" : "WHGWH19",
	"typeid" : "4"
},{
	"DisplayName" : "埃及宝藏",
	"ButtonImagePath" : "WH28_ZH.png",
	"LapisId" : "WHGWH28",
	"typeid" : "4"
},{
	"DisplayName" : "封神演义",
	"ButtonImagePath" : "WH23_ZH.png",
	"LapisId" : "WHGWH23",
	"typeid" : "4"
},{
	"DisplayName" : "和风剧院",
	"ButtonImagePath" : "WH27_ZH.png",
	"LapisId" : "WHGWH27",
	"typeid" : "4"
},{
	"DisplayName" : "点石成金",
	"ButtonImagePath" : "WH30_ZH.png",
	"LapisId" : "WHGWH30",
	"typeid" : "4"
},{
	"DisplayName" : "圣女贞德",
	"ButtonImagePath" : "WH02_ZH.png",
	"LapisId" : "WHGWH02",
	"typeid" : "4"
},{
	"DisplayName" : "五狮进宝",
	"ButtonImagePath" : "WH07_ZH.png",
	"LapisId" : "WHGWH07",
	"typeid" : "4"
},{
	"DisplayName" : "发财熊猫",
	"ButtonImagePath" : "WH12_ZH.png",
	"LapisId" : "WHGWH12",
	"typeid" : "4"
},{
	"DisplayName" : "十二生肖",
	"ButtonImagePath" : "WH38_ZH.png",
	"LapisId" : "WHGWH38",
	"typeid" : "4"
},{
	"DisplayName" : "招财锦鲤",
	"ButtonImagePath" : "WH35_ZH.png",
	"LapisId" : "WHGWH35",
	"typeid" : "4"
},{
	"DisplayName" : "白雪公主",
	"ButtonImagePath" : "WH18_ZH.png",
	"LapisId" : "WHGWH18",
	"typeid" : "4"
},{
	"DisplayName" : "葫芦兄弟",
	"ButtonImagePath" : "WH20_ZH.png",
	"LapisId" : "WHGWH20",
	"typeid" : "4"
},{
	"DisplayName" : "内衣橄榄球",
	"ButtonImagePath" : "WH34_ZH.png",
	"LapisId" : "WHGWH34",
	"typeid" : "4"
},{
	"DisplayName" : "贪玩蓝月",
	"ButtonImagePath" : "WH32_ZH.png",
	"LapisId" : "WHGWH32",
	"typeid" : "4"
},{
	"DisplayName" : "多宝鱼虾蟹",
	"ButtonImagePath" : "SB55_ZH.png",
	"LapisId" : "166",
	"typeid" : "4"
},{
	"DisplayName" : "跳跳乐",
	"ButtonImagePath" : "WC01_ZH.png",
	"LapisId" : "WHGWC01",
	"typeid" : "4"
},{
	"DisplayName" : "永恒之吻",
	"ButtonImagePath" : "WH21_ZH.png",
	"LapisId" : "WHGWH21",
	"typeid" : "4"
},{
	"DisplayName" : "恐怖嘉年华",
	"ButtonImagePath" : "WH22_ZH.png",
	"LapisId" : "WHGWH22",
	"typeid" : "4"
},{
	"DisplayName" : "僵尸来袭",
	"ButtonImagePath" : "WH24_ZH.png",
	"LapisId" : "WHGWH24",
	"typeid" : "4"
},{
	"DisplayName" : "狂野女巫",
	"ButtonImagePath" : "WH29_ZH.png",
	"LapisId" : "WHGWH29",
	"typeid" : "4"
},{
	"DisplayName" : "嫦娥奔月",
	"ButtonImagePath" : "WH17_ZH.png",
	"LapisId" : "WHGWH17",
	"typeid" : "4"
},{
	"DisplayName" : "钻石女王",
	"ButtonImagePath" : "WA01_ZH.png",
	"LapisId" : "WHGWA01",
	"typeid" : "4"
},{
	"DisplayName" : "亚瑟王",
	"ButtonImagePath" : "WH06_ZH.png",
	"LapisId" : "WHGWH06",
	"typeid" : "4"
},{
	"DisplayName" : "爱丽丝大冒险",
	"ButtonImagePath" : "WH10_ZH.png",
	"LapisId" : "WHGWH10",
	"typeid" : "4"
},{
	"DisplayName" : "战火风云",
	"ButtonImagePath" : "WH11_ZH.png",
	"LapisId" : "WHGWH11",
	"typeid" : "4"
},{
	"DisplayName" : "猛龙传奇",
	"DisplayEnName" : "Legend of the Dragon",
	"category" : "老虎机",
	"ButtonImagePath" : "SB45.jpg",
	"LapisId" : "156"
}, {
	"DisplayName" : "英雄荣耀",
	"DisplayEnName" : "Glory of Heroes",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAR.jpg",
	"LapisId" : "DTGDTAR"
}, {
	"DisplayName" : "快乐农庄",
	"DisplayEnName" : "Happy Farm",
	"category" : "老虎机",
	"ButtonImagePath" : "DTB1.jpg",
	"LapisId" : "DTGDTB1"
}, {
	"DisplayName" : "封神榜",
	"DisplayEnName" : "Investiture of Gods",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAM.jpg",
	"LapisId" : "DTGDTAM"
}, {
	"DisplayName" : "摇滚之夜",
	"DisplayEnName" : "Rocknight",
	"category" : "老虎机",
	"ButtonImagePath" : "DTAZ.jpg",
	"LapisId" : "DTGDTAZ"
}, {
	"DisplayName" : "金拉霸",
	"DisplayEnName" : "Gold Class",
	"category" : "老虎机",
	"ButtonImagePath" : "SC03.jpg",
	"LapisId" : "802"
}, {
	"DisplayName" : "街头烈战",
	"DisplayEnName" : "Street Battle",
	"category" : "老虎机",
	"ButtonImagePath" : "SX02.jpg",
	"LapisId" : "221"
}, {
	"DisplayName" : "赛亚烈战",
	"DisplayEnName" : "Saiyan Battle Z",
	"category" : "老虎机",
	"ButtonImagePath" : "DTA0.jpg",
	"LapisId" : "DTGDTA0"
}, {
	"DisplayName" : "欧洲轮盘（桌面版）",
	"DisplayEnName" : "Roulette Deluxe (desktop)",
	"category" : "其他",
	"ButtonImagePath" : "TA1L.jpg",
	"LapisId" : "TAITA1L"
}, {
	"DisplayName" : "龙舟竞渡",
	"DisplayEnName" : "Dragon Boat Festival",
	"category" : "老虎机",
	"ButtonImagePath" : "XG10.gif",
	"LapisId" : "209"
}, {
	"DisplayName" : "中秋佳节",
	"DisplayEnName" : "Moon Festival",
	"category" : "老虎机",
	"ButtonImagePath" : "XG11.gif",
	"LapisId" : "210"
}, {
	"DisplayName" : "韩风劲舞",
	"DisplayEnName" : "K-Pop",
	"category" : "老虎机",
	"ButtonImagePath" : "XG12.gif",
	"LapisId" : "211"
}, {
	"DisplayName" : "美女大格斗",
	"DisplayEnName" : "Fantasies",
	"category" : "老虎机",
	"ButtonImagePath" : "XG13.gif",
	"LapisId" : "212"
}, {
	"DisplayName" : "黄金对垒",
	"DisplayEnName" : "Kick Off",
	"category" : "老虎机",
	"ButtonImagePath" : "XG16.gif",
	"LapisId" : "215"
}, {
	"DisplayName" : "冰河世界",
	"DisplayEnName" : "Ice Crush",
	"category" : "老虎机",
	"ButtonImagePath" : "SB34.jpg",
	"LapisId" : "145"
}, {
	"DisplayName" : "糖果碰碰乐",
	"DisplayEnName" : "Candy Quest",
	"category" : "老虎机",
	"ButtonImagePath" : "SB33.gif",
	"LapisId" : "144"
}, {
	"DisplayName" : "大豪客",
	"DisplayEnName" : "High Roller",
	"category" : "老虎机",
	"ButtonImagePath" : "XG09.gif",
	"LapisId" : "208"
}, {
	"DisplayName" : "龙珠",
	"DisplayEnName" : "Dragons Pearl",
	"category" : "老虎机",
	"ButtonImagePath" : "XG01.jpg",
	"LapisId" : "200"
}, {
	"DisplayName" : "幸运8",
	"DisplayEnName" : "lucky 8",
	"category" : "老虎机",
	"ButtonImagePath" : "XG02.jpg",
	"LapisId" : "201"
}, {
	"DisplayName" : "闪亮女郎",
	"DisplayEnName" : "Bling Bling",
	"category" : "老虎机",
	"ButtonImagePath" : "XG03.jpg",
	"LapisId" : "202"
}, {
	"DisplayName" : "海盗王",
	"DisplayEnName" : "Pirates",
	"category" : "老虎机",
	"ButtonImagePath" : "XG06.jpg",
	"LapisId" : "205"
}, {
	"DisplayName" : "鲜果狂热",
	"DisplayEnName" : "Fruitmania",
	"category" : "老虎机",
	"ButtonImagePath" : "XG07.jpg",
	"LapisId" : "206"
}, {
	"DisplayName" : "小熊猫",
	"DisplayEnName" : "Red Panda",
	"category" : "老虎机",
	"ButtonImagePath" : "XG08.jpg",
	"LapisId" : "207"
} ]

var aghotdata = [ {
	"DisplayName" : "捕鱼王者",
	"ButtonImagePath" : "SB36.gif",
	"LapisId" : "6",
	"typeid" : "4"
}, {
	"DisplayName" : "赌城之夜",
	"ButtonImagePath" : "SB56_ZH.png",
	"LapisId" : "SB56",
	"typeid" : "4"
},{
	"DisplayName" : "极品飙车",
	"ButtonImagePath" : "WH54_ZH.png",
	"LapisId" : "WHGWH54",
	"typeid" : "4"
},{
	"DisplayName" : "寿司大食客",
	"ButtonImagePath" : "WH26_ZH.png",
	"LapisId" : "WHGWH26",
	"typeid" : "4"
},{
	"DisplayName" : "皇家戏台",
	"ButtonImagePath" : "WH40_ZH.png",
	"LapisId" : "WHGWH40",
	"typeid" : "4"
},{
	"DisplayName" : "蒸汽战争",
	"ButtonImagePath" : "WH44_ZH.png",
	"LapisId" : "WHGWH44",
	"typeid" : "4"
},{
	"DisplayName" : "魅惑魔女",
	"ButtonImagePath" : "SB58_ZH.png",
	"LapisId" : "169",
	"typeid" : "4"
},{
	"DisplayName" : "橫行霸道",
	"ButtonImagePath" : "WH36_ZH.png",
	"LapisId" : "WHGWH36",
	"typeid" : "4"
},{
	"DisplayName" : "古惑仔",
	"ButtonImagePath" : "WH42_ZH.png",
	"LapisId" : "WHGWH42",
	"typeid" : "4"
},{
	"DisplayName" : "神奇宠物",
	"ButtonImagePath" : "WH55_ZH.png",
	"LapisId" : "WHGWH55",
	"typeid" : "4"
},{
	"DisplayName" : "魔龙",
	"ButtonImagePath" : "SB57_ZH.png",
	"LapisId" : "168",
	"typeid" : "4"
},{
	"DisplayName" : "财宝塔罗",
	"ButtonImagePath" : "WH19_ZH.png",
	"LapisId" : "WHGWH19",
	"typeid" : "4"
},{
	"DisplayName" : "埃及宝藏",
	"ButtonImagePath" : "WH28_ZH.png",
	"LapisId" : "WHGWH28",
	"typeid" : "4"
},{
	"DisplayName" : "封神演义",
	"ButtonImagePath" : "WH23_ZH.png",
	"LapisId" : "WHGWH23",
	"typeid" : "4"
},{
	"DisplayName" : "和风剧院",
	"ButtonImagePath" : "WH27_ZH.png",
	"LapisId" : "WHGWH27",
	"typeid" : "4"
},{
	"DisplayName" : "点石成金",
	"ButtonImagePath" : "WH30_ZH.png",
	"LapisId" : "WHGWH30",
	"typeid" : "4"
},{
	"DisplayName" : "圣女贞德",
	"ButtonImagePath" : "WH02_ZH.png",
	"LapisId" : "WHGWH02",
	"typeid" : "4"
},{
	"DisplayName" : "五狮进宝",
	"ButtonImagePath" : "WH07_ZH.png",
	"LapisId" : "WHGWH07",
	"typeid" : "4"
},{
	"DisplayName" : "发财熊猫",
	"ButtonImagePath" : "WH12_ZH.png",
	"LapisId" : "WHGWH12",
	"typeid" : "4"
},{
	"DisplayName" : "十二生肖",
	"ButtonImagePath" : "WH38_ZH.png",
	"LapisId" : "WHGWH38",
	"typeid" : "4"
},{
	"DisplayName" : "招财锦鲤",
	"ButtonImagePath" : "WH35_ZH.png",
	"LapisId" : "WHGWH35",
	"typeid" : "4"
},{
	"DisplayName" : "白雪公主",
	"ButtonImagePath" : "WH18_ZH.png",
	"LapisId" : "WHGWH18",
	"typeid" : "4"
},{
	"DisplayName" : "葫芦兄弟",
	"ButtonImagePath" : "WH20_ZH.png",
	"LapisId" : "WHGWH20",
	"typeid" : "4"
},{
	"DisplayName" : "内衣橄榄球",
	"ButtonImagePath" : "WH34_ZH.png",
	"LapisId" : "WHGWH34",
	"typeid" : "4"
},{
	"DisplayName" : "贪玩蓝月",
	"ButtonImagePath" : "WH32_ZH.png",
	"LapisId" : "WHGWH32",
	"typeid" : "4"
},{
	"DisplayName" : "多宝鱼虾蟹",
	"ButtonImagePath" : "SB55_ZH.png",
	"LapisId" : "166",
	"typeid" : "4"
},{
	"DisplayName" : "跳跳乐",
	"ButtonImagePath" : "WC01_ZH.png",
	"LapisId" : "WHGWC01",
	"typeid" : "4"
},{
	"DisplayName" : "永恒之吻",
	"ButtonImagePath" : "WH21_ZH.png",
	"LapisId" : "WHGWH21",
	"typeid" : "4"
},{
	"DisplayName" : "恐怖嘉年华",
	"ButtonImagePath" : "WH22_ZH.png",
	"LapisId" : "WHGWH22",
	"typeid" : "4"
},{
	"DisplayName" : "僵尸来袭",
	"ButtonImagePath" : "WH24_ZH.png",
	"LapisId" : "WHGWH24",
	"typeid" : "4"
},{
	"DisplayName" : "狂野女巫",
	"ButtonImagePath" : "WH29_ZH.png",
	"LapisId" : "WHGWH29",
	"typeid" : "4"
},{
	"DisplayName" : "水果拉霸",
	"ButtonImagePath" : "FRU.gif",
	"LapisId" : "501",
	"typeid" : "3"
}, {
	"DisplayName" : "杰克高手",
	"ButtonImagePath" : "PKBJ.gif",
	"LapisId" : "502",
	"typeid" : "3"
}, {
	"DisplayName" : "极速幸运轮",
	"ButtonImagePath" : "TGLW.gif",
	"LapisId" : "507",
	"typeid" : "2"
}, {
	"DisplayName" : "太空漫游",
	"ButtonImagePath" : "SB01.gif",
	"LapisId" : "508",
	"typeid" : "3"
}, {
	"DisplayName" : "复古花园",
	"ButtonImagePath" : "SB02.gif",
	"LapisId" : "509",
	"typeid" : "3"
}, {
	"DisplayName" : "中秋佳节",
	"DisplayEnName" : "Moon Festival",
	"category" : "老虎机",
	"ButtonImagePath" : "XG11.gif",
	"LapisId" : "210"
}, {
	"DisplayName" : "韩风劲舞",
	"DisplayEnName" : "K-Pop",
	"category" : "老虎机",
	"ButtonImagePath" : "XG12.gif",
	"LapisId" : "211"
}, {
	"DisplayName" : "美女大格斗",
	"DisplayEnName" : "Fantasies",
	"category" : "老虎机",
	"ButtonImagePath" : "XG13.gif",
	"LapisId" : "212"
}, {
	"DisplayName" : "黄金对垒",
	"DisplayEnName" : "Kick Off",
	"category" : "老虎机",
	"ButtonImagePath" : "XG16.gif",
	"LapisId" : "215"
}, {
	"DisplayName" : "冰河世界",
	"DisplayEnName" : "Ice Crush",
	"category" : "老虎机",
	"ButtonImagePath" : "SB34.jpg",
	"LapisId" : "145"
}, {
	"DisplayName" : "糖果碰碰乐",
	"DisplayEnName" : "Candy Quest",
	"category" : "老虎机",
	"ButtonImagePath" : "SB33.gif",
	"LapisId" : "144"
}, {
	"DisplayName" : "大豪客",
	"DisplayEnName" : "High Roller",
	"category" : "老虎机",
	"ButtonImagePath" : "XG09.gif",
	"LapisId" : "208"
}, {
	"DisplayName" : "龙珠",
	"DisplayEnName" : "Dragons Pearl",
	"category" : "老虎机",
	"ButtonImagePath" : "XG01.jpg",
	"LapisId" : "200"
}, {
	"DisplayName" : "幸运8",
	"DisplayEnName" : "lucky 8",
	"category" : "老虎机",
	"ButtonImagePath" : "XG02.jpg",
	"LapisId" : "201"
}, {
	"DisplayName" : "闪亮女郎",
	"DisplayEnName" : "Bling Bling",
	"category" : "老虎机",
	"ButtonImagePath" : "XG03.jpg",
	"LapisId" : "202"
}, {
	"DisplayName" : "海盗王",
	"DisplayEnName" : "Pirates",
	"category" : "老虎机",
	"ButtonImagePath" : "XG06.jpg",
	"LapisId" : "205"
} ]

var youplay = [ {
	"DisplayName" : "森林舞会",
	"DisplayEnName" : "Forest Dance",
	"FTP_gametype" : "YFD",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfd.png",
	"LapisId" : "YP801"
}, {
	"DisplayName" : "奔驰宝马",
	"DisplayEnName" : "Benz",
	"FTP_gametype" : "YBEN",
	"category" : "单人游戏",
	"ButtonImagePath" : "yben.png",
	"LapisId" : "YP802"
}, {
	"DisplayName" : "極速赛马",
	"DisplayEnName" : "Run Horse",
	"FTP_gametype" : "YHR",
	"category" : "单人游戏",
	"ButtonImagePath" : "yhr.png",
	"LapisId" : "YP803"
}, {
	"DisplayName" : "猜猜乐",
	"DisplayEnName" : "Guess",
	"FTP_gametype" : "YGS",
	"category" : "单人游戏",
	"ButtonImagePath" : "ygs.png",
	"LapisId" : "YP804"
}, {
	"DisplayName" : "水果拉霸",
	"DisplayEnName" : "Fruit Slot",
	"FTP_gametype" : "YFR",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfr.png",
	"LapisId" : "YP805"
}, {
	"DisplayName" : "德州牛仔",
	"DisplayEnName" : "Texas Cowboy",
	"FTP_gametype" : "YDZ",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ydz.png",
	"LapisId" : "YP806"
}, {
	"DisplayName" : "飞禽走兽",
	"DisplayEnName" : "Birds & Beasts",
	"FTP_gametype" : "YBIR",
	"category" : "单人游戏",
	"ButtonImagePath" : "ybir.png",
	"LapisId" : "YP807"
}, {
	"DisplayName" : "水果派对",
	"DisplayEnName" : "Fruit Party",
	"FTP_gametype" : "YFP",
	"category" : "单人游戏",
	"ButtonImagePath" : "yfp.png",
	"LapisId" : "YP808"
}, {
	"DisplayName" : "森林舞会多人版",
	"DisplayEnName" : "Forest Dance Multiplayer",
	"FTP_gametype" : "YMFD",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymfd.png",
	"LapisId" : "YP810"
}, {
	"DisplayName" : "水果拉霸多人版",
	"DisplayEnName" : "Fruit Slot Multiplayer",
	"FTP_gametype" : "YMFR",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymfr.png",
	"LapisId" : "YP813"
}, {
	"DisplayName" : "百人牛牛",
	"DisplayEnName" : "Yo Bull",
	"FTP_gametype" : "YMBN",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ymbn.png",
	"LapisId" : "YP817"
}, {
	"DisplayName" : "多宝水果拉霸",
	"DisplayEnName" : "Treasure Fruit slot",
	"FTP_gametype" : "YGFS",
	"category" : "单人游戏",
	"ButtonImagePath" : "ygfs.png",
	"LapisId" : "YP818"
}, {
	"DisplayName" : "彩金水果拉霸",
	"DisplayEnName" : "Jackpot Fruit Slot",
	"FTP_gametype" : "YJFS",
	"category" : "单人游戏",
	"ButtonImagePath" : "yjfs.png",
	"LapisId" : "YP820"
}, {
	"DisplayName" : "飞禽走兽多人版",
	"DisplayEnName" : "Birds and Beasts Multiplayer",
	"FTP_gametype" : "YMBI",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymbi.png",
	"LapisId" : "YP814"
}, {
	"DisplayName" : "牛牛对战",
	"DisplayEnName" : "Bull Battle",
	"FTP_gametype" : "YMBA",
	"category" : "桌面游戏",
	"ButtonImagePath" : "ymba.png",
	"LapisId" : "YP821"
}, {
	"DisplayName" : "奔驰宝马多人版",
	"DisplayEnName" : "Benz Multiplayer",
	"FTP_gametype" : "YMBZ",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymbz.png",
	"LapisId" : "YP811"
}, {
	"DisplayName" : "动物狂欢",
	"DisplayEnName" : "Animal Carnival",
	"FTP_gametype" : "YMAC",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymac.png",
	"LapisId" : "YP823"
}, {
	"DisplayName" : "西游争霸",
	"DisplayEnName" : "Western Journey",
	"FTP_gametype" : "YMJW",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymjw.png",
	"LapisId" : "YP824"
}, {
	"DisplayName" : "翻倍炸金花",
	"DisplayEnName" : "Double Win 3 Cards",
	"FTP_gametype" : "YMJH",
	"category" : "多人游戏",
	"ButtonImagePath" : "ymjh.png",
	"LapisId" : "YP822"
} ];
   
var jdbdata = [
	{"ButtonImagePath":"WEB_Icon14027_250x203_cn.jpg","DisplayName":"好运777","typeid":"0","LapisId":"14027"},
	{"ButtonImagePath":"WEB_Icon14029_250x203_cn.jpg","DisplayName":"东方神兽","typeid":"0","LapisId":"14029"},
	{"ButtonImagePath":"WEB_Icon14030_250x203_cn.jpg","DisplayName":"三倍金刚","typeid":"0","LapisId":"14030"},
	{"ButtonImagePath":"WEB_Icon14033_250x203_cn.jpg","DisplayName":"飞鸟派对","typeid":"0","LapisId":"14033"},
	{"ButtonImagePath":"WEB_Icon14034_250x203_cn.jpg","DisplayName":"狗来富","typeid":"0","LapisId":"14034"},
	{"ButtonImagePath":"WEB_Icon14035_250x203_cn.jpg","DisplayName":"龙舞","typeid":"0","LapisId":"14035"},
	{"ButtonImagePath":"WEB_Icon14036_250x203_cn.jpg","DisplayName":"超级牛B","typeid":"0","LapisId":"14036"},
	{"ButtonImagePath":"WEB_Icon8001_250x203_cn.jpg","DisplayName":"幸运龙","typeid":"0","LapisId":"8001"},{"ButtonImagePath":"WEB_Icon8002_250x203_cn.jpg","DisplayName":"唐伯虎点秋香","typeid":"0","LapisId":"8002"},{"ButtonImagePath":"WEB_Icon8003_250x203_cn.jpg","DisplayName":"变脸","typeid":"0","LapisId":"8003"},{"ButtonImagePath":"WEB_Icon8004_250x203_cn.jpg","DisplayName":"悟空","typeid":"0","LapisId":"8004"},{"ButtonImagePath":"WEB_Icon8005_250x203_cn.jpg","DisplayName":"骆马大冒险","typeid":"0","LapisId":"8005"},{"ButtonImagePath":"WEB_Icon8006_250x203_cn.jpg","DisplayName":"台湾黑熊","typeid":"0","LapisId":"8006"},{"ButtonImagePath":"WEB_Icon8007_250x203_cn.jpg","DisplayName":"幸运麟","typeid":"0","LapisId":"8007"},{"ButtonImagePath":"WEB_Icon8014_250x203_cn.jpg","DisplayName":"招财狮","typeid":"0","LapisId":"8014"},{"ButtonImagePath":"WEB_Icon8015_250x203_cn.jpg","DisplayName":"月光秘宝","typeid":"0","LapisId":"8015"},{"ButtonImagePath":"WEB_Icon8016_250x203_cn.jpg","DisplayName":"上班族狂想曲","typeid":"0","LapisId":"8016"},{"ButtonImagePath":"WEB_Icon8017_250x203_cn.jpg","DisplayName":"过新年","typeid":"0","LapisId":"8017"},{"ButtonImagePath":"WEB_Icon8018_250x203_cn.jpg","DisplayName":"拿破仑","typeid":"0","LapisId":"8018"},{"ButtonImagePath":"WEB_Icon8019_250x203_cn.jpg","DisplayName":"文房四宝","typeid":"0","LapisId":"8019"},{"ButtonImagePath":"WEB_Icon8020_250x203_cn.jpg","DisplayName":"芝麻开门","typeid":"0","LapisId":"8020"},{"ButtonImagePath":"WEB_Icon8021_250x203_cn.jpg","DisplayName":"黄金香蕉帝国","typeid":"0","LapisId":"8021"},{"ButtonImagePath":"WEB_Icon8022_250x203_cn.jpg","DisplayName":"麻雀无双","typeid":"0","LapisId":"8022"},{"ButtonImagePath":"WEB_Icon8023_250x203_cn.jpg","DisplayName":"奥林匹亚神庙","typeid":"0","LapisId":"8023"},{"ButtonImagePath":"WEB_Icon8024_250x203_cn.jpg","DisplayName":"水晶王国","typeid":"0","LapisId":"8024"},{"ButtonImagePath":"WEB_Icon8025_250x203_cn.jpg","DisplayName":"神偷妙贼","typeid":"0","LapisId":"8025"},{"ButtonImagePath":"WEB_Icon8026_250x203_cn.jpg","DisplayName":"热舞教父","typeid":"0","LapisId":"8026"},{"ButtonImagePath":"WEB_Icon8027_250x203_cn.jpg","DisplayName":"料理厨王","typeid":"0","LapisId":"8027"},{"ButtonImagePath":"WEB_Icon8028_250x203_cn.jpg","DisplayName":"幸运淘金鼠","typeid":"0","LapisId":"8028"},{"ButtonImagePath":"WEB_Icon8029_250x203_cn.jpg","DisplayName":"奇幻糖果岛","typeid":"0","LapisId":"8029"},{"ButtonImagePath":"WEB_Icon8030_250x203_cn.jpg","DisplayName":"疯狂科学家","typeid":"0","LapisId":"8030"},{"ButtonImagePath":"WEB_Icon8031_250x203_cn.jpg","DisplayName":"金饺子","typeid":"0","LapisId":"8031"},{"ButtonImagePath":"WEB_Icon8034_250x203_cn.jpg","DisplayName":"金钱侠","typeid":"0","LapisId":"8034"},{"ButtonImagePath":"WEB_Icon8035_250x203_cn.jpg","DisplayName":"幸运凤","typeid":"0","LapisId":"8035"},{"ButtonImagePath":"WEB_Icon8036_250x203_cn.jpg","DisplayName":"龙王","typeid":"0","LapisId":"8036"},{"ButtonImagePath":"WEB_Icon8037_250x203_cn.jpg","DisplayName":"魔术秀","typeid":"0","LapisId":"8037"},{"ButtonImagePath":"WEB_Icon8044_250x203_cn.jpg","DisplayName":"江山美人","typeid":"0","LapisId":"8044"},{"ButtonImagePath":"WEB_Icon8046_250x203_cn.jpg","DisplayName":"关公","typeid":"0","LapisId":"8046"},{"ButtonImagePath":"WEB_Icon8047_250x203_cn.jpg","DisplayName":"变脸2","typeid":"0","LapisId":"8047"},{"ButtonImagePath":"WEB_Icon8048_250x203_cn.jpg","DisplayName":"芝麻开门2","typeid":"0","LapisId":"8048"},{"ButtonImagePath":"WEB_Icon8049_250x203_cn.jpg","DisplayName":"唐伯虎点秋香2","typeid":"0","LapisId":"8049"},{"ButtonImagePath":"WEB_Icon8999_250x203_cn.png","DisplayName":"直播变脸","typeid":"0","LapisId":"8999"},{"ButtonImagePath":"WEB_Icon14001_250x203_cn.jpg","DisplayName":"斗鸡","typeid":"0","LapisId":"14001"},{"ButtonImagePath":"WEB_Icon14002_250x203_cn.jpg","DisplayName":"玛雅大冒险","typeid":"0","LapisId":"14002"},{"ButtonImagePath":"WEB_Icon14003_250x203_cn.jpg","DisplayName":"屌丝熊猫","typeid":"0","LapisId":"14003"},{"ButtonImagePath":"WEB_Icon14004_250x203_cn.jpg","DisplayName":"塞尔达传说","typeid":"0","LapisId":"14004"},{"ButtonImagePath":"WEB_Icon14005_250x203_cn.jpg","DisplayName":"包大人","typeid":"0","LapisId":"14005"},{"ButtonImagePath":"WEB_Icon14006_250x203_cn.jpg","DisplayName":"亿万富翁","typeid":"0","LapisId":"14006"},{"ButtonImagePath":"WEB_Icon14007_250x203_cn.jpg","DisplayName":"一拳超人","typeid":"0","LapisId":"14007"},{"ButtonImagePath":"WEB_Icon14008_250x203_cn.jpg","DisplayName":"神龙大侠","typeid":"0","LapisId":"14008"},{"ButtonImagePath":"WEB_Icon14010_250x203_cn.jpg","DisplayName":"飞龙在天","typeid":"0","LapisId":"14010"},{"ButtonImagePath":"WEB_Icon14011_250x203_cn.jpg","DisplayName":"银河护卫队","typeid":"0","LapisId":"14011"},{"ButtonImagePath":"WEB_Icon14012_250x203_cn.jpg","DisplayName":"街头霸王","typeid":"0","LapisId":"14012"},{"ButtonImagePath":"WEB_Icon14013_250x203_cn.jpg","DisplayName":"春宵苦短","typeid":"0","LapisId":"14013"},{"ButtonImagePath":"WEB_Icon14015_250x203_cn.jpg","DisplayName":"星球大战","typeid":"0","LapisId":"14015"},{"ButtonImagePath":"WEB_Icon14016_250x203_cn.jpg","DisplayName":"王牌特工","typeid":"0","LapisId":"14016"},{"ButtonImagePath":"WEB_Icon14017_250x203_cn.jpg","DisplayName":"少女前线","typeid":"0","LapisId":"14017"},{"ButtonImagePath":"WEB_Icon14018_250x203_cn.jpg","DisplayName":"妲己","typeid":"0","LapisId":"14018"},{"ButtonImagePath":"WEB_Icon14019_250x203_cn.jpg","DisplayName":"宝石物语","typeid":"0","LapisId":"14019"},{"ButtonImagePath":"WEB_Icon14020_250x203_cn.jpg","DisplayName":"魔法乳神","typeid":"0","LapisId":"14020"},{"ButtonImagePath":"WEB_Icon14021_250x203_cn.jpg","DisplayName":"钱滚钱","typeid":"0","LapisId":"14021"},{"ButtonImagePath":"WEB_Icon14022_250x203_cn.jpg","DisplayName":"采矿土豪","typeid":"0","LapisId":"14022"},{"ButtonImagePath":"WEB_Icon14023_250x203_cn.jpg","DisplayName":"赌王扑克","typeid":"0","LapisId":"14023"},{"ButtonImagePath":"WEB_Icon14025_250x203_cn.jpg","DisplayName":"幸运赛车","typeid":"0","LapisId":"14025"},{"ButtonImagePath":"WEB_Icon14026_250x203_cn.jpg","DisplayName":"发大财","typeid":"0","LapisId":"14026"},{"ButtonImagePath":"WEB_Icon15001_250x203_cn.jpg","DisplayName":"金鸡报囍","typeid":"0","LapisId":"15001"},{"ButtonImagePath":"WEB_Icon15002_250x203_cn.jpg","DisplayName":"齐天大圣","typeid":"0","LapisId":"15002"},{"ButtonImagePath":"WEB_Icon15004_250x203_cn.jpg","DisplayName":"火牛阵","typeid":"0","LapisId":"15004"},{"ButtonImagePath":"WEB_Icon15006_250x203_cn.jpg","DisplayName":"印加帝国","typeid":"0","LapisId":"15006"},{"ButtonImagePath":"WEB_Icon15009_250x203_cn.jpg","DisplayName":"忍者大进击","typeid":"0","LapisId":"15009"},{"ButtonImagePath":"WEB_Icon15010_250x203_cn.jpg","DisplayName":"熊猫厨王","typeid":"0","LapisId":"15010"},{"ButtonImagePath":"WEB_Icon15011_250x203_cn.jpg","DisplayName":"后羿","typeid":"0","LapisId":"15011"},{"ButtonImagePath":"WEB_Icon15013_250x203_cn.jpg","DisplayName":"九尾狐","typeid":"0","LapisId":"15013"},{"ButtonImagePath":"WEB_Icon7001_250x203_cn.jpg","DisplayName":"龙王捕鱼","typeid":"7","LapisId":"7001"},{"ButtonImagePath":"WEB_Icon7002_250x203_cn.jpg","DisplayName":"龙王捕鱼2","typeid":"7","LapisId":"7002"},{"ButtonImagePath":"WEB_Icon7003_250x203_cn.jpg","DisplayName":"财神捕鱼","typeid":"7","LapisId":"7003"},{"ButtonImagePath":"WEB_Icon9001_250x203_cn.jpg","DisplayName":"小玛莉","typeid":"9","LapisId":"9001"},{"ButtonImagePath":"WEB_Icon9002_250x203_cn.jpg","DisplayName":"新年快乐","typeid":"9","LapisId":"9002"},{"ButtonImagePath":"WEB_Icon9003_250x203_cn.jpg","DisplayName":"飞禽走兽","typeid":"9","LapisId":"9003"},{"ButtonImagePath":"WEB_Icon9004_250x203_cn.jpg","DisplayName":"啤酒大亨","typeid":"9","LapisId":"9004"},{"ButtonImagePath":"WEB_Icon9006_250x203_cn.jpg","DisplayName":"花果山传奇","typeid":"9","LapisId":"9006"},{"ButtonImagePath":"WEB_Icon9007_250x203_cn.jpg","DisplayName":"超激发水果盘","typeid":"9","LapisId":"9007"}];
/*var ptdata=[{"ButtonImagePath":"pt/pc/asfa.png","DisplayName":"亚洲幻想","typeid":"老虎机","LapisId":"asfa"},{"ButtonImagePath":"pt/pc/heavru.jpg","DisplayName":"武则天","typeid":"老虎机","LapisId":"heavru"},{"ButtonImagePath":"pt/pc/gts51.jpg","DisplayName":"幸运熊猫","typeid":"老虎机","LapisId":"gts51"},{"ButtonImagePath":"pt/pc/sling.jpg","DisplayName":"四灵","typeid":"老虎机","LapisId":"sling"},{"ButtonImagePath":"pt/pc/slion.jpg","DisplayName":"超级狮子","typeid":"老虎机","LapisId":"slion"},{"ButtonImagePath":"pt/pc/haocs.jpg","DisplayName":"好事成双","typeid":"老虎机","LapisId":"haocs"},{"ButtonImagePath":"pt/pc/gts52.jpg","DisplayName":"狂躁的海盗","typeid":"老虎机","LapisId":"gts52"},{"ButtonImagePath":"pt/pc/bj_mh5.jpg","DisplayName":"二十一点","typeid":"桌牌游戏","LapisId":"bj_mh5"},{"ButtonImagePath":"pt/pc/savcas.jpg","DisplayName":"大草原现金","typeid":"老虎机","LapisId":"savcas"},{"ButtonImagePath":"pt/pc/frtln.jpg","DisplayName":"幸运狮子","typeid":"老虎机","LapisId":"frtln"},{"ButtonImagePath":"pt/pc/hotktv.jpg","DisplayName":"火热KTV","typeid":"老虎机","LapisId":"hotktv"},{"ButtonImagePath":"pt/pc/hb.jpg","DisplayName":"漫漫长夜","typeid":"老虎机","LapisId":"hb"},{"ButtonImagePath":"pt/pc/aogro.jpg","DisplayName":"神灵时代：轮盘","typeid":"老虎机","LapisId":"aogro"},{"ButtonImagePath":"pt/pc/ashamw.jpg","DisplayName":"狂野亚马逊","typeid":"老虎机","LapisId":"ashamw"},{"ButtonImagePath":"pt/pc/bja.jpg","DisplayName":"美式21点","typeid":"桌牌游戏","LapisId":"bja"},{"ButtonImagePath":"pt/pc/rodz.jpg","DisplayName":"美式轮盘","typeid":"桌牌游戏","LapisId":"rodz"},{"ButtonImagePath":"pt/pc/arc.jpg","DisplayName":"弓箭手","typeid":"老虎机","LapisId":"arc"},{"ButtonImagePath":"pt/pc/gtsatq.jpg","DisplayName":"亚特兰蒂斯女王","typeid":"老虎机","LapisId":"gtsatq"},{"ButtonImagePath":"pt/pc/ba.jpg","DisplayName":"百家乐","typeid":"桌牌游戏","LapisId":"ba"},{"ButtonImagePath":"pt/pc/bs.jpg","DisplayName":"白狮","typeid":"老虎机","LapisId":"bs"},{"ButtonImagePath":"pt/pc/bl.jpg","DisplayName":"海滨嘉年华","typeid":"老虎机","LapisId":"bl"},{"ButtonImagePath":"pt/pc/bt.jpg","DisplayName":"百慕大三角","typeid":"老虎机","LapisId":"bt"},{"ButtonImagePath":"pt/pc/psdbj.jpg","DisplayName":"21点专业版","typeid":"桌牌游戏","LapisId":"psdbj"},{"ButtonImagePath":"pt/pc/bob.jpg","DisplayName":"熊之舞","typeid":"老虎机","LapisId":"bob"},{"ButtonImagePath":"pt/pc/bfb.jpg","DisplayName":"野牛闪电战","typeid":"老虎机","LapisId":"bfb"},{"ButtonImagePath":"pt/pc/ct.jpg","DisplayName":"船长的宝藏","typeid":"老虎机","LapisId":"ct"},{"ButtonImagePath":"pt/pc/ctp2.jpg","DisplayName":"船长的宝藏专业版","typeid":"老虎机","LapisId":"ctp2"},{"ButtonImagePath":"pt/pc/cashfi.jpg","DisplayName":"深海大赢家","typeid":"街机游戏","LapisId":"cashfi"},{"ButtonImagePath":"pt/pc/cheaa.jpg","DisplayName":"德州扑克","typeid":"桌牌游戏","LapisId":"cheaa"},{"ButtonImagePath":"pt/pc/ctiv.jpg","DisplayName":"猫赌神","typeid":"老虎机","LapisId":"ctiv"},{"ButtonImagePath":"pt/pc/chao.jpg","DisplayName":"超级8","typeid":"老虎机","LapisId":"chao"},{"ButtonImagePath":"pt/pc/chl.jpg","DisplayName":"樱花之恋","typeid":"老虎机","LapisId":"chl"},{"ButtonImagePath":"pt/pc/ashcpl.jpg","DisplayName":"美女船长","typeid":"老虎机","LapisId":"ashcpl"},{"ButtonImagePath":"pt/pc/cm.jpg","DisplayName":"中国厨房","typeid":"老虎机","LapisId":"cm"},{"ButtonImagePath":"pt/pc/gtscnb.jpg","DisplayName":"警察抓小偷 ","typeid":"老虎机","LapisId":"gtscnb"},{"ButtonImagePath":"pt/pc/gtscbl.jpg","DisplayName":"牛仔与外星人 ","typeid":"老虎机","LapisId":"gtscbl"},{"ButtonImagePath":"pt/pc/c7.jpg","DisplayName":"疯狂之七","typeid":"老虎机","LapisId":"c7"},{"ButtonImagePath":"pt/pc/dt.jpg","DisplayName":"沙漠财宝","typeid":"老虎机","LapisId":"dt"},{"ButtonImagePath":"pt/pc/dnr.jpg","DisplayName":"海豚礁堡","typeid":"老虎机","LapisId":"dnr"},{"ButtonImagePath":"pt/pc/eas.jpg","DisplayName":"惊喜复活节 ","typeid":"老虎机","LapisId":"eas"},{"ButtonImagePath":"pt/pc/esmk.jpg","DisplayName":"埃斯梅拉达","typeid":"老虎机","LapisId":"esmk"},{"ButtonImagePath":"pt/pc/evj.jpg","DisplayName":"人人中头奖","typeid":"老虎机","LapisId":"evj"},{"ButtonImagePath":"pt/pc/fcgz.jpg","DisplayName":"翡翠公主","typeid":"老虎机","LapisId":"fcgz"},{"ButtonImagePath":"pt/pc/gtsflzt.jpg","DisplayName":"飞龙在天","typeid":"老虎机","LapisId":"gtsflzt"},{"ButtonImagePath":"pt/pc/fkmj.jpg","DisplayName":"疯狂麻将","typeid":"老虎机","LapisId":"fkmj"},{"ButtonImagePath":"pt/pc/gtsfpc.jpg","DisplayName":"魚蝦蟹","typeid":"桌牌游戏","LapisId":"gtsfpc"},{"ButtonImagePath":"pt/pc/fbr.jpg","DisplayName":"终极足球 ","typeid":"老虎机","LapisId":"fbr"},{"ButtonImagePath":"pt/pc/fow.jpg","DisplayName":"惊异之林","typeid":"老虎机","LapisId":"fow"},{"ButtonImagePath":"pt/pc/fday.jpg","DisplayName":"幸运日","typeid":"老虎机","LapisId":"fday"},{"ButtonImagePath":"pt/pc/fxf.jpg","DisplayName":"狐狸的宝藏","typeid":"老虎机","LapisId":"fxf"},{"ButtonImagePath":"pt/pc/foy.jpg","DisplayName":"青春之泉","typeid":"老虎机","LapisId":"foy"},{"ButtonImagePath":"pt/pc/fdt.jpg","DisplayName":"疯狂底特律七","typeid":"老虎机","LapisId":"fdt"},{"ButtonImagePath":"pt/pc/fdtjg.jpg","DisplayName":"疯狂底特律７- Double/弗兰基德托的魔术","typeid":"老虎机","LapisId":"fdtjg"},{"ButtonImagePath":"pt/pc/ashfmf.jpg","DisplayName":"月满梦圆 ","typeid":"老虎机","LapisId":"ashfmf"},{"ButtonImagePath":"pt/pc/fff.jpg","DisplayName":"酷炫水果农场 ","typeid":"老虎机","LapisId":"fff"},{"ButtonImagePath":"pt/pc/fnfrj.jpg","DisplayName":"酷炫水果 ","typeid":"老虎机","LapisId":"fnfrj"},{"ButtonImagePath":"pt/pc/fm.jpg","DisplayName":"古怪猴子","typeid":"老虎机","LapisId":"fm"},{"ButtonImagePath":"pt/pc/furf.jpg","DisplayName":"神灵时代：激情四","typeid":"老虎机","LapisId":"furf"},{"ButtonImagePath":"pt/pc/ges.jpg","DisplayName":"艺伎回忆录","typeid":"老虎机","LapisId":"ges"},{"ButtonImagePath":"pt/pc/gesjp.jpg","DisplayName":"艺伎回忆录奖池版","typeid":"老虎机","LapisId":"gesjp"},{"ButtonImagePath":"pt/pc/gemq.jpg","DisplayName":"宝石女王","typeid":"老虎机","LapisId":"gemq"},{"ButtonImagePath":"pt/pc/glr.jpg","DisplayName":"角斗士","typeid":"老虎机","LapisId":"glr"},{"ButtonImagePath":"pt/pc/glrj.jpg","DisplayName":"角斗士奖池版","typeid":"老虎机","LapisId":"glrj"},{"ButtonImagePath":"pt/pc/gos.jpg","DisplayName":"金色之旅","typeid":"老虎机","LapisId":"gos"},{"ButtonImagePath":"pt/pc/bib.jpg","DisplayName":"湛蓝深海","typeid":"老虎机","LapisId":"bib"},{"ButtonImagePath":"pt/pc/hlf.jpg","DisplayName":"万圣节财富 ","typeid":"老虎机","LapisId":"hlf"},{"ButtonImagePath":"pt/pc/hlf2.jpg","DisplayName":"万圣节财富2","typeid":"老虎机","LapisId":"hlf2"},{"ButtonImagePath":"pt/pc/hh.jpg","DisplayName":"鬼屋","typeid":"老虎机","LapisId":"hh"},{"ButtonImagePath":"pt/pc/ashhotj.jpg","DisplayName":"丛林之心","typeid":"老虎机","LapisId":"ashhotj"},{"ButtonImagePath":"pt/pc/hk.jpg","DisplayName":"高速公路之王","typeid":"老虎机","LapisId":"hk"},{"ButtonImagePath":"pt/pc/gtshwkp.jpg","DisplayName":"漂移之王专业版","typeid":"老虎机","LapisId":"gtshwkp"},{"ButtonImagePath":"pt/pc/gts50.jpg","DisplayName":"热力宝石","typeid":"老虎机","LapisId":"gts50"},{"ButtonImagePath":"pt/pc/aztec.jpg","DisplayName":"印加JP","typeid":"老虎机","LapisId":"aztec"},{"ButtonImagePath":"pt/pc/gtsirl.jpg","DisplayName":"幸运爱尔兰","typeid":"老虎机","LapisId":"gtsirl"},{"ButtonImagePath":"pt/pc/jpgt.jpg","DisplayName":"巨人传","typeid":"老虎机","LapisId":"jpgt"},{"ButtonImagePath":"pt/pc/gtsje.jpg","DisplayName":"玉皇大帝","typeid":"老虎机","LapisId":"gtsje"},{"ButtonImagePath":"pt/pc/gtsjxb.jpg","DisplayName":"吉祥8","typeid":"老虎机","LapisId":"gtsjxb"},{"ButtonImagePath":"pt/pc/jqw.jpg","DisplayName":"金钱蛙","typeid":"老虎机","LapisId":"jqw"},{"ButtonImagePath":"pt/pc/zeus.jpg","DisplayName":"神灵时代：奥林匹斯之王","typeid":"老虎机","LapisId":"zeus"},{"ButtonImagePath":"pt/pc/kkg.jpg","DisplayName":"金刚：世界的第八奇迹","typeid":"老虎机","LapisId":"kkg"},{"ButtonImagePath":"pt/pc/lndg.jpg","DisplayName":"金土地","typeid":"老虎机","LapisId":"lndg"},{"ButtonImagePath":"pt/pc/ght_a.jpg","DisplayName":"烈焰钻石","typeid":"老虎机","LapisId":"ght_a"},{"ButtonImagePath":"pt/pc/kfp.jpg","DisplayName":"六福兽","typeid":"老虎机","LapisId":"kfp"},{"ButtonImagePath":"pt/h5/7bal.png","DisplayName":"真人百家乐（7座）","typeid":"视讯扑克","LapisId":"7bal"},{"ButtonImagePath":"pt/pc/rofl.jpg","DisplayName":"实况法式轮盘赌","typeid":"视讯扑克","LapisId":"rofl"},{"ButtonImagePath":"pt/pc/longlong.jpg","DisplayName":"龙龙龙","typeid":"老虎机","LapisId":"longlong"},{"ButtonImagePath":"pt/pc/lm.jpg","DisplayName":"疯狂乐透","typeid":"老虎机","LapisId":"lm"},{"ButtonImagePath":"pt/pc/mgstk.jpg","DisplayName":"神奇的栈","typeid":"老虎机","LapisId":"mgstk"},{"ButtonImagePath":"pt/pc/gtsmrln.jpg","DisplayName":"玛丽莲·梦露 ","typeid":"老虎机","LapisId":"gtsmrln"},{"ButtonImagePath":"pt/pc/mcb.jpg","DisplayName":"钱先生 ","typeid":"老虎机","LapisId":"mcb"},{"ButtonImagePath":"pt/pc/nk.jpg","DisplayName":"海洋星王国","typeid":"老虎机","LapisId":"nk"},{"ButtonImagePath":"pt/pc/pmn.jpg","DisplayName":"豹月","typeid":"老虎机","LapisId":"pmn"},{"ButtonImagePath":"pt/pc/pl.jpg","DisplayName":"舞线","typeid":"老虎机","LapisId":"pl"},{"ButtonImagePath":"pt/pc/pgv.jpg","DisplayName":"企鹅假期","typeid":"老虎机","LapisId":"pgv"},{"ButtonImagePath":"pt/pc/pst.jpg","DisplayName":"法老的秘密","typeid":"老虎机","LapisId":"pst"},{"ButtonImagePath":"pt/pc/gtspor.jpg","DisplayName":"财富蓝海 ","typeid":"老虎机","LapisId":"gtspor"},{"ButtonImagePath":"pt/pc/hrcls.jpg","DisplayName":"神灵时代：奥林匹斯王子","typeid":"老虎机","LapisId":"hrcls"},{"ButtonImagePath":"pt/pc/photk.jpg","DisplayName":"紫热","typeid":"老虎机","LapisId":"photk"},{"ButtonImagePath":"pt/pc/qnw.jpg","DisplayName":"权杖女皇（原名魔杖女皇）","typeid":"老虎机","LapisId":"qnw"},{"ButtonImagePath":"pt/pc/ririjc.jpg","DisplayName":"日日进财","typeid":"老虎机","LapisId":"ririjc"},{"ButtonImagePath":"pt/pc/ririshc.jpg","DisplayName":"日日生财","typeid":"老虎机","LapisId":"ririshc"},{"ButtonImagePath":"pt/pc/gtsrng.jpg","DisplayName":"罗马荣光","typeid":"老虎机","LapisId":"gtsrng"},{"ButtonImagePath":"pt/pc/ro.jpg","DisplayName":"欧洲轮盘","typeid":"桌牌游戏","LapisId":"ro"},{"ButtonImagePath":"pt/pc/rol.jpg","DisplayName":"實況轮盘（分类）","typeid":"视讯扑克","LapisId":"rol"},{"ButtonImagePath":"pt/pc/sfh.jpg","DisplayName":"狂热野生动物园","typeid":"老虎机","LapisId":"sfh"},{"ButtonImagePath":"pt/pc/gtssmbr.jpg","DisplayName":"巴西桑巴 ","typeid":"老虎机","LapisId":"gtssmbr"},{"ButtonImagePath":"pt/pc/ssp.jpg","DisplayName":"圣诞惊喜","typeid":"老虎机","LapisId":"ssp"},{"ButtonImagePath":"pt/pc/samz.jpg","DisplayName":"亚马逊之谜","typeid":"老虎机","LapisId":"samz"},{"ButtonImagePath":"pt/pc/shmst.jpg","DisplayName":"福尔摩斯之谜","typeid":"老虎机","LapisId":"shmst"},{"ButtonImagePath":"pt/pc/sx.jpg","DisplayName":"四象","typeid":"老虎机","LapisId":"sx"},{"ButtonImagePath":"pt/pc/sis.jpg","DisplayName":"沉默的武士","typeid":"老虎机","LapisId":"sis"},{"ButtonImagePath":"pt/pc/sisjp.jpg","DisplayName":"沉默的武士奖池版","typeid":"老虎机","LapisId":"sisjp"},{"ButtonImagePath":"pt/pc/sib.jpg","DisplayName":"银弹","typeid":"老虎机","LapisId":"sib"},{"ButtonImagePath":"pt/pc/ashsbd.jpg","DisplayName":"辛巴达的黄金之旅","typeid":"老虎机","LapisId":"ashsbd"},{"ButtonImagePath":"pt/pc/sol.jpg","DisplayName":"好运连连","typeid":"老虎机","LapisId":"sol"},{"ButtonImagePath":"pt/pc/gtsswk.jpg","DisplayName":"孙悟空","typeid":"老虎机","LapisId":"gtsswk"},{"ButtonImagePath":"pt/pc/cnpr.jpg","DisplayName":"甜蜜派对","typeid":"老虎机","LapisId":"cnpr"},{"ButtonImagePath":"pt/pc/thtk.jpg","DisplayName":"泰国神庙","typeid":"老虎机","LapisId":"thtk"},{"ButtonImagePath":"pt/pc/gtsgme.jpg","DisplayName":"大明帝国","typeid":"老虎机","LapisId":"gtsgme"},{"ButtonImagePath":"pt/pc/lvb.jpg","DisplayName":"爱之船 ","typeid":"老虎机","LapisId":"lvb"},{"ButtonImagePath":"pt/pc/donq.jpg","DisplayName":"富有的唐吉可德","typeid":"老虎机","LapisId":"donq"},{"ButtonImagePath":"pt/pc/topg.jpg","DisplayName":"壮志凌云","typeid":"老虎机","LapisId":"topg"},{"ButtonImagePath":"pt/pc/ta.jpg","DisplayName":"三个朋友(义勇三奇侠)","typeid":"老虎机","LapisId":"ta"},{"ButtonImagePath":"pt/pc/trpmnk.jpg","DisplayName":"三倍猴子","typeid":"老虎机","LapisId":"trpmnk"},{"ButtonImagePath":"pt/pc/trl.jpg","DisplayName":"真爱","typeid":"老虎机","LapisId":"trl"},{"ButtonImagePath":"pt/pc/ub.jpg","DisplayName":"丛林巫师","typeid":"老虎机","LapisId":"ub"},{"ButtonImagePath":"pt/pc/er.jpg","DisplayName":"开心假期","typeid":"老虎机","LapisId":"er"},{"ButtonImagePath":"pt/pc/vcstd.jpg","DisplayName":"开心假期加强版","typeid":"老虎机","LapisId":"vcstd"},{"ButtonImagePath":"pt/pc/whk.jpg","DisplayName":"白狮子王","typeid":"老虎机","LapisId":"whk"},{"ButtonImagePath":"pt/pc/ashwgaa.jpg","DisplayName":"疯狂赌徒２/北极探险","typeid":"老虎机","LapisId":"ashwgaa"},{"ButtonImagePath":"pt/pc/wlg.jpg","DisplayName":"舞龙 ","typeid":"老虎机","LapisId":"wlg"},{"ButtonImagePath":"pt/pc/wlgjp.jpg","DisplayName":"舞龙奖池版","typeid":"老虎机","LapisId":"wlgjp"},{"ButtonImagePath":"pt/pc/wlcsh.jpg","DisplayName":"五路财神","typeid":"老虎机","LapisId":"wlcsh"},{"ButtonImagePath":"pt/pc/zcjb.jpg","DisplayName":"招财进宝 ","typeid":"老虎机","LapisId":"zcjb"},{"ButtonImagePath":"pt/pc/zcjbjp.jpg","DisplayName":"招财进宝奖池版","typeid":"老虎机","LapisId":"zcjbjp"},{"ButtonImagePath":"pt/pc/zctz.jpg","DisplayName":"招财童子","typeid":"老虎机","LapisId":"zctz"},{"ButtonImagePath":"pt/pc/plba.jpg","DisplayName":"实况累积百家乐","typeid":"视讯扑克","LapisId":"plba"},{"ButtonImagePath":"pt/pc/rodl.jpg","DisplayName":"实况VIP罗盘","typeid":"视讯扑克","LapisId":"rodl"},{"ButtonImagePath":"pt/pc/sbl.jpg","DisplayName":"实况骰宝（分在桌牌）","typeid":"视讯扑克","LapisId":"sbl"},{"ButtonImagePath":"pt/pc/ubjl.jpg","DisplayName":"实况无限制21点","typeid":"视讯扑克","LapisId":"ubjl"},{"ButtonImagePath":"pt/pc/vbal.jpg","DisplayName":"实况VIP百家乐","typeid":"视讯扑克","LapisId":"vbal"},{"ButtonImagePath":"pt/pc/spud.jpg","DisplayName":"金开钻O'Reilly's作物","typeid":"老虎机","LapisId":"spud"},{"ButtonImagePath":"pt/pc/rky.jpg","DisplayName":"洛基传奇","typeid":"老虎机","LapisId":"rky"},{"ButtonImagePath":"pt/pc/paw.jpg","DisplayName":"三只小猪与狼 ","typeid":"老虎机","LapisId":"paw"},{"ButtonImagePath":"pt/pc/mmy.jpg","DisplayName":"木乃伊刮刮乐","typeid":"老虎机","LapisId":"mmy"},{"ButtonImagePath":"pt/pc/ashbob.jpg","DisplayName":"杰克与魔豆","typeid":"老虎机","LapisId":"ashbob"},{"ButtonImagePath":"pt/pc/qop.jpg","DisplayName":"金字塔女王","typeid":"老虎机","LapisId":"qop"},{"ButtonImagePath":"pt/pc/grel.jpg","DisplayName":"金色召集 ","typeid":"老虎机","LapisId":"grel"},{"ButtonImagePath":"pt/pc/rodz.jpg","DisplayName":"美式轮盘","typeid":"桌牌游戏","LapisId":"rodz"},{"ButtonImagePath":"pt/pc/rodz_g.jpg","DisplayName":"美式奖金轮盘赌","typeid":"桌牌游戏","LapisId":"rodz_g"},{"ButtonImagePath":"pt/pc/gtsdrdv.jpg","DisplayName":"勇敢的大卫和拉神之眼 ","typeid":"老虎机","LapisId":"gtsdrdv"},{"ButtonImagePath":"pt/pc/pnp.jpg","DisplayName":"粉红豹","typeid":"老虎机","LapisId":"pnp"},{"ButtonImagePath":"pt/pc/ashfta.jpg","DisplayName":"白雪公主","typeid":"老虎机","LapisId":"ashfta"},{"ButtonImagePath":"pt/pc/glg.jpg","DisplayName":"黄金游戏　","typeid":"老虎机","LapisId":"glg"},{"ButtonImagePath":"pt/pc/gtsru.jpg","DisplayName":"魔方财富","typeid":"街机游戏","LapisId":"gtsru"},{"ButtonImagePath":"pt/pc/ashtmd.jpg","DisplayName":"交易时刻","typeid":"老虎机","LapisId":"ashtmd"},{"ButtonImagePath":"pt/pc/ms.jpg","DisplayName":"魔幻吃角子老虎","typeid":"老虎机","LapisId":"ms"},{"ButtonImagePath":"pt/pc/pfbj_mh5.jpg","DisplayName":"完美二十一点","typeid":"桌牌游戏","LapisId":"pfbj_mh5"},{"ButtonImagePath":"pt/pc/dt2.jpg","DisplayName":"沙漠财宝2","typeid":"老虎机","LapisId":"dt2"},{"ButtonImagePath":"pt/pc/ttc.jpg","DisplayName":"顶级王牌-明星 ","typeid":"老虎机","LapisId":"ttc"},{"ButtonImagePath":"pt/pc/fmn.jpg","DisplayName":"水果狂","typeid":"老虎机","LapisId":"fmn"},{"ButtonImagePath":"pt/pc/gtscb.jpg","DisplayName":"现金Blox","typeid":"老虎机","LapisId":"gtscb"},{"ButtonImagePath":"pt/pc/gtsjhw.jpg","DisplayName":"约翰·韦恩 ","typeid":"老虎机","LapisId":"gtsjhw"},{"ButtonImagePath":"pt/pc/ro_g.jpg","DisplayName":"欧式奖金轮盘","typeid":"视讯扑克","LapisId":"ro_g"},{"ButtonImagePath":"pt/pc/gtsdgk.jpg","DisplayName":"龙之王国 ","typeid":"老虎机","LapisId":"gtsdgk"},{"ButtonImagePath":"pt/pc/gtsjzc.jpg","DisplayName":"爵士俱乐部 ","typeid":"老虎机","LapisId":"gtsjzc"},{"ButtonImagePath":"pt/pc/athn.jpg","DisplayName":"神灵时代：智慧女神","typeid":"老虎机","LapisId":"athn"},{"ButtonImagePath":"pt/pc/gtsir.jpg","DisplayName":"极地冒险","typeid":"老虎机","LapisId":"gtsir"},{"ButtonImagePath":"pt/pc/ftsis.jpg","DisplayName":"神灵时代：命运姐妹","typeid":"老虎机","LapisId":"ftsis"},{"ButtonImagePath":"pt/pc/gtswng.jpg","DisplayName":"黄金之翼 ","typeid":"老虎机","LapisId":"gtswng"},{"ButtonImagePath":"pt/pc/dlm.jpg","DisplayName":"恋爱专家","typeid":"老虎机","LapisId":"dlm"},{"ButtonImagePath":"pt/pc/wis.jpg","DisplayName":"我心狂野","typeid":"老虎机","LapisId":"wis"},{"ButtonImagePath":"pt/pc/frtf.jpg","DisplayName":"五个海盗","typeid":"老虎机","LapisId":"frtf"},{"ButtonImagePath":"pt/pc/ashadv.jpg","DisplayName":"仙境冒险","typeid":"老虎机","LapisId":"ashadv"},{"ButtonImagePath":"pt/pc/aogs.jpg","DisplayName":"神灵时代","typeid":"老虎机","LapisId":"aogs"},{"ButtonImagePath":"pt/pc/ashlob.jpg","DisplayName":"布莱恩的一生","typeid":"老虎机","LapisId":"ashlob"},{"ButtonImagePath":"pt/pc/mfrt.jpg","DisplayName":"小姐财富","typeid":"老虎机","LapisId":"mfrt"},{"ButtonImagePath":"pt/pc/scs.jpg","DisplayName":"经典老虎机刮刮乐","typeid":"刮刮卡","LapisId":"scs"},{"ButtonImagePath":"pt/pc/tclsc.jpg","DisplayName":"三个小丑刮刮乐","typeid":"刮刮卡","LapisId":"tclsc"}];*/

var qtdata=[
	{"ButtonImagePath":"qt/v2/QS-wildtomeofthewoods.jpg","DisplayName":"森林宝典","typeid":"1","LapisId":"QS-wildtomeofthewoods"},
	{"ButtonImagePath":"qt/v2/QS-stickybanditswildreturn.jpg","DisplayName":"大盗归来","typeid":"1","LapisId":"QS-stickybanditswildreturn"},
	{"ButtonImagePath":"qt/v2/QS-primezone.jpg","DisplayName":"黄金地带","typeid":"1","LapisId":"QS-primezone"},
	{"ButtonImagePath":"qt/v2/LGB-dragonpalace.jpg","DisplayName":"龙宫","typeid":"1","LapisId":"LBG-dragonpalace"},
	{"ButtonImagePath":"qt/v2/LGB-pinatabucks.jpg","DisplayName":"皮纳塔雄鹿","typeid":"1","LapisId":"LBG-pinatabucks"},
	{"ButtonImagePath":"qt/v2/LGB-respinracer.png","DisplayName":"旋风赛车","typeid":"1","LapisId":"LBG-respinracer"},
	{"ButtonImagePath":"qt/v2/LBG-dolphingold.jpg","DisplayName":"金海豚","typeid":"1","LapisId":"LBG-dolphingold"},
	{"ButtonImagePath":"qt/v2/NLC-pixiesvspirates.jpg","DisplayName":"精灵大战海盗","typeid":"1","LapisId":"NLC-pixiesvspirates"},
	{"ButtonImagePath":"qt/v2/NLC-hot4cash.jpg","DisplayName":"热钱现金","typeid":"1","LapisId":"NLC-hot4cash"},
	{"ButtonImagePath":"qt/v2/NLC-thecreepycarnival.jpg","DisplayName":"恐怖嘉年华","typeid":"1","LapisId":"NLC-thecreepycarnival"},
	{"ButtonImagePath":"qt/v2/NLC-thorhammertime.jpg","DisplayName":"雷神之锤","typeid":"1","LapisId":"NLC-thorhammertime"},
	{"ButtonImagePath":"qt/v2/NLC-tractorbeam.jpg","DisplayName":"牵引光束","typeid":"1","LapisId":"NLC-tractorbeam"},
	{"ButtonImagePath":"qt/v2/NLC-mayanmagic.jpg","DisplayName":"玛雅神话","typeid":"1","LapisId":"NLC-mayanmagic"},
	{"ButtonImagePath":"qt/v2/GFG-fightingspirit.jpg","DisplayName":"打齐","typeid":"1","LapisId":"GFG-fightingspirit"},
	{"ButtonImagePath":"qt/v2/GFG-heavenlyquest.jpg","DisplayName":"天庭天职","typeid":"1","LapisId":"GFG-heavenlyquest"},
	{"ButtonImagePath":"qt/v2/GFG-armorofcourage.jpg","DisplayName":"勇气护甲","typeid":"1","LapisId":"GFG-armorofcourage"},
	{"ButtonImagePath":"qt/v2/GFG-longgui.jpg","DisplayName":"龙龟","typeid":"1","LapisId":"GFG-longgui"},
	{"ButtonImagePath":"qt/v2/GFG-nianwaandchunni.jpg","DisplayName":"年娃和春妮","typeid":"1","LapisId":"GFG-nianwaandchunni"},
	{"ButtonImagePath":"qt/v2/PUG-vikingclash.jpg","DisplayName":"维京争霸","typeid":"1","LapisId":"PUG-vikingclash"},
	{"ButtonImagePath":"qt/v2/PUG-wildswarm.jpg","DisplayName":"百搭蜂巢","typeid":"1","LapisId":"PUG-wildswarm"},
	{"ButtonImagePath":"qt/v2/PUG-blazeofra.jpg","DisplayName":"太阳神之焰","typeid":"1","LapisId":"PUG-blazeofra"},
	{"ButtonImagePath":"qt/v2/PUG-fatrabbit.jpg","DisplayName":"大胖兔","typeid":"1","LapisId":"PUG-fatrabbit"},
	{"ButtonImagePath":"qt/v2/PUG-humptydumpty.jpg","DisplayName":"蛋壳先生","typeid":"1","LapisId":"PUG-humptydumpty"},
	{"ButtonImagePath":"qt/v2/PUG-starfall.jpg","DisplayName":"星坠","typeid":"1","LapisId":"PUG-starfall"},
	{"ButtonImagePath":"qt/v2/PUG-tikitumble.jpg","DisplayName":"旋转神像","typeid":"1","LapisId":"PUG-tikitumble"},
	{"ButtonImagePath":"qt/v2/PUG-wildwheel.jpg","DisplayName":"幸运大转盘","typeid":"1","LapisId":"PUG-wildwheel"},
	{"ButtonImagePath":"qt/v2/MOB-fortune88.jpg","DisplayName":"财富88","typeid":"1","LapisId":"MOB-fortune88"},
	{"ButtonImagePath":"qt/v2/MOB-diamondbar.jpg","DisplayName":"钻石吧","typeid":"1","LapisId":"MOB-diamondbar"},
	{"ButtonImagePath":"qt/v2/MOB-lickyluck.jpg","DisplayName":"冰淇淋工厂","typeid":"1","LapisId":"MOB-lickyluck"},
	{"ButtonImagePath":"qt/v2/MOB-pandawilds.jpg","DisplayName":"熊猫百搭","typeid":"1","LapisId":"MOB-pandawilds"},
	{"ButtonImagePath":"qt/v2/MOB-newyearfortunes.jpg","DisplayName":"恭喜发财","typeid":"1","LapisId":"MOB-newyearfortunes"},
	{"ButtonImagePath":"qt/v2/MOB-legendarymulan.jpg","DisplayName":"花木兰","typeid":"1","LapisId":"MOB-legendarymulan"},
	{"ButtonImagePath":"qt/v2/MOB-goldenbuffalo.jpg","DisplayName":"黄金水牛","typeid":"1","LapisId":"MOB-goldenbuffalo"},
	{"ButtonImagePath":"qt/v2/MOB-lionexplorer.jpg","DisplayName":"狮子王","typeid":"1","LapisId":"MOB-lionexplorer"},
	{"ButtonImagePath":"qt/v2/MOB-sanxingfortunes.jpg","DisplayName":"三星报喜","typeid":"1","LapisId":"MOB-sanxingfortunes"},
	{"ButtonImagePath":"qt/v2/MOB-smileyveggies.jpg","DisplayName":"开心农场","typeid":"1","LapisId":"MOB-smileyveggies"},
	{"ButtonImagePath":"qt/v2/MOB-pharaohsofthenile.jpg","DisplayName":"尼罗河的法老","typeid":"1","LapisId":"MOB-pharaohsofthenile"},
	{"ButtonImagePath":"qt/v2/MOB-princecharming.jpg","DisplayName":"白马王子","typeid":"1","LapisId":"MOB-princecharming"},
	{"ButtonImagePath":"qt/v2/EVP-etlostsocks.jpg","DisplayName":"E.T. 袜子丢失","typeid":"1","LapisId":"EVP-etlostsocks"},
	{"ButtonImagePath":"qt/v2/IDS-treasureofhorus.jpg","DisplayName":"荷鲁斯宝藏","typeid":"1","LapisId":"IDS-treasureofhorus"},
	{"ButtonImagePath":"qt/v2/IDS-paint.jpg","DisplayName":"涂料","typeid":"1","LapisId":"IDS-paint"},
	{"ButtonImagePath":"qt/v2/BNG-fucaishen.jpg","DisplayName":"福财神","typeid":"1","LapisId":"BNG-fucaishen"},
	{"ButtonImagePath":"qt/v2/BPG-vikingsunleashedmegaways.jpg","DisplayName":"维京人之猛虎出笼","typeid":"1","LapisId":"BPG-vikingsunleashedmegaways"},
	{"ButtonImagePath":"qt/v2/BPG-primalmegaways.jpg","DisplayName":"原初之王","typeid":"1","LapisId":"BPG-primalmegaways"},
	{"ButtonImagePath":"qt/v2/BPG-journeyofthegods.jpg","DisplayName":"诸神之旅","typeid":"1","LapisId":"BPG-journeyofthegods"},
	{"ButtonImagePath":"qt/v2/ELK-thewiz.jpg","DisplayName":"巫师","typeid":"1","LapisId":"ELK-thewiz"},
	{"ButtonImagePath":"qt/v2/FNG-chickenstorm.jpg","DisplayName":"鸡暴","typeid":"1","LapisId":"FNG-chickenstorm"},
	{"ButtonImagePath":"qt/v2/QS-divinedreams.jpg","DisplayName":"神圣女神","typeid":"1","LapisId":"QS-divinedreams"},
	{"ButtonImagePath":"qt/v2/RG-robinhood.jpg","DisplayName":"侠盗罗宾汉","typeid":"1","LapisId":"RG-robinhood"},
	{"ButtonImagePath":"qt/v2/RG-goodfishes.jpg","DisplayName":"好鱼儿","typeid":"1","LapisId":"RG-goodfishes"},
	{"ButtonImagePath":"qt/v2/SBS-portals.jpg","DisplayName":"星际之门","typeid":"1","LapisId":"SBS-portals"},
	{"ButtonImagePath":"qt/v2/QS-rapunzelstower.jpg","DisplayName":"长发公主","typeid":"1","LapisId":"QS-rapunzelstower"},
	{"ButtonImagePath":"qt/v2/OGS-dragonwins.jpg","DisplayName":"巨龙之巅","typeid":"1","LapisId":"OGS-dragonwins"},
	{"ButtonImagePath":"qt/v2/OGS-coolbananas.jpg","DisplayName":"炫酷香蕉","typeid":"1","LapisId":"OGS-coolbananas"},
	{"ButtonImagePath":"qt/v2/OGS-jadeidol.jpg","DisplayName":"翡翠猴","typeid":"1","LapisId":"OGS-jadeidol"},
	{"ButtonImagePath":"qt/v2/EVP-atlantis.jpg","DisplayName":"亚特兰蒂斯","typeid":"1","LapisId":"EVP-atlantis"},
	{"ButtonImagePath":"qt/v2/EVP-epicgladiators.jpg","DisplayName":"角斗士大战僵尸","typeid":"1","LapisId":"EVP-epicgladiators"},
	{"ButtonImagePath":"qt/v2/EVP-football.jpg","DisplayName":"足球宝贝","typeid":"1","LapisId":"EVP-football"},
	{"ButtonImagePath":"qt/v2/EVP-naughtygirlscabaret.jpg","DisplayName":"顽皮女孩歌舞秀","typeid":"1","LapisId":"EVP-naughtygirlscabaret"},
	{"ButtonImagePath":"qt/v2/HAB-santasvillage.jpg","DisplayName":"圣诞乐村","typeid":"1","LapisId":"HAB-santasvillage"},
	{"ButtonImagePath":"qt/v2/OGS-crowningglory.jpg","DisplayName":"至高荣耀","typeid":"1","LapisId":"OGS-crowningglory"},
	{"ButtonImagePath":"qt/v2/OGS-foxinwinsaveryfoxinchristmas.jpg","DisplayName":"狐狸家族圣诞狂欢","typeid":"1","LapisId":"OGS-foxinwinsaveryfoxinchristmas"},
	{"ButtonImagePath":"qt/v2/OGS-jollysgifts.jpg","DisplayName":"乔莉的礼物","typeid":"1","LapisId":"OGS-jollysgifts"},
	{"ButtonImagePath":"qt/v2/OGS-samuraisplit.jpg","DisplayName":"古怪的武士","typeid":"1","LapisId":"OGS-samuraisplit"},
	{"ButtonImagePath":"qt/v2/OGS-notenoughkittens.jpg","DisplayName":"顽皮猫咪","typeid":"1","LapisId":"OGS-notenoughkittens"},
	{"ButtonImagePath":"qt/v2/NLC-kitchendramabbqfrenzy.jpg","DisplayName":"疯狂烧烤趴","typeid":"1","LapisId":"NLC-kitchendramabbqfrenzy"},
	{"ButtonImagePath":"qt/v2/ELK-hohotower.jpg","DisplayName":"HoHo大楼","typeid":"1","LapisId":"ELK-hohotower"},
	{"ButtonImagePath":"qt/v2/QS-mightyarthur.jpg","DisplayName":"亚瑟王","typeid":"1","LapisId":"QS-mightyarthur"},
	{"ButtonImagePath":"qt/v2/OGS-druidessgoldhq.jpg","DisplayName":"夺金巫师HQ","typeid":"1","LapisId":"OGS-druidessgoldhq"},
	{"ButtonImagePath":"qt/v2/OGS-huolongvalley.jpg","DisplayName":"喷火龙山谷","typeid":"1","LapisId":"OGS-huolongvalley"},
	{"ButtonImagePath":"qt/v2/OGS-slotsofmoney.jpg","DisplayName":"抓钱老虎机","typeid":"1","LapisId":"OGS-slotsofmoney"},
	{"ButtonImagePath":"qt/v2/EVP-aceround.jpg","DisplayName":"ACE枪王","typeid":"1","LapisId":"EVP-aceround"},
	{"ButtonImagePath":"qt/v2/EVP-elvenprincesses.jpg","DisplayName":"精灵公主","typeid":"1","LapisId":"EVP-elvenprincesses"},
	{"ButtonImagePath":"qt/v2/EVP-theemperorstomb.jpg","DisplayName":"皇帝陵墓","typeid":"1","LapisId":"EVP-theemperorstomb"},
	{"ButtonImagePath":"qt/v2/EVP-fruitburst.jpg","DisplayName":"水果爆裂","typeid":"1","LapisId":"EVP-fruitburst"},
	{"ButtonImagePath":"qt/v2/EVP-indianasquest.jpg","DisplayName":"印第安纳的任务","typeid":"1","LapisId":"EVP-indianasquest"},
	{"ButtonImagePath":"qt/v2/EVP-legendofra.jpg","DisplayName":"太阳神传说","typeid":"1","LapisId":"EVP-legendofra"},
	{"ButtonImagePath":"qt/v2/OGS-bonanza.jpg","DisplayName":"矿山探险","typeid":"4","LapisId":"OGS-bonanza"},
	{"ButtonImagePath":"qt/v2/QS-dragonshrine.jpg","DisplayName":"龙神殿","typeid":"4","LapisId":"QS-dragonshrine"},
	{"ButtonImagePath":"qt/v2/HAB-koigate.jpg","DisplayName":"鲤鱼门","typeid":"4","LapisId":"HAB-koigate"},
	{"ButtonImagePath":"qt/v2/ELK-route777.jpg","DisplayName":"公路777","typeid":"4","LapisId":"ELK-route777"},
	{"ButtonImagePath":"qt/v2/OGS-madmadmonkey.jpg","DisplayName":"疯狂的猴子","typeid":"4","LapisId":"OGS-madmadmonkey"},
	{"ButtonImagePath":"qt/v2/OGS-esqueletoexplosivo.jpg","DisplayName":"骷髅乐队","typeid":"4","LapisId":"OGS-esqueletoexplosivo"},
	{"ButtonImagePath":"qt/v2/QS-sakurafortune.jpg","DisplayName":"樱花侠女","typeid":"4","LapisId":"QS-sakurafortune"},
	{"ButtonImagePath":"qt/v2/QS-winsoffortune.jpg","DisplayName":"美猴王","typeid":"4","LapisId":"QS-winsoffortune"},
	{"ButtonImagePath":"qt/v2/HAB-facaishen.jpg","DisplayName":"发财神","typeid":"4","LapisId":"HAB-facaishen"},
	{"ButtonImagePath":"qt/v2/OGS-froggrog.jpg","DisplayName":"格罗格醉蛙","typeid":"4","LapisId":"OGS-froggrog"},
	{"ButtonImagePath":"qt/v2/ELK-hohotower.jpg","DisplayName":"HoHo大楼","typeid":"4","LapisId":"ELK-hohotower"},
	{"ButtonImagePath":"qt/v2/OGS-tokitime.jpg","DisplayName":"开心消消乐","typeid":"4","LapisId":"OGS-tokitime"},
	{"ButtonImagePath":"qt/v2/QS-secondstrike.jpg","DisplayName":"连环炮","typeid":"4","LapisId":"QS-secondstrike"},
	{"ButtonImagePath":"qt/v2/QS-mightyarthur.jpg","DisplayName":"亚瑟王","typeid":"4","LapisId":"QS-mightyarthur"},
	{"ButtonImagePath":"qt/v2/EVP-talismansoffortune.jpg","DisplayName":"转运护身符","typeid":"4","LapisId":"EVP-talismansoffortune"},
	{"ButtonImagePath":"qt/v2/OGS-pinkelephants.jpg","DisplayName":"粉象","typeid":"4","LapisId":"OGS-pinkelephants"},
	{"ButtonImagePath":"qt/v2/QS-stickybandits.jpg","DisplayName":"西部劫匪","typeid":"4","LapisId":"QS-stickybandits"},
	{"ButtonImagePath":"qt/v2/ELK-wildtoro.jpg","DisplayName":"卑鄙的斗牛士","typeid":"4","LapisId":"ELK-wildtoro"},
	{"ButtonImagePath":"qt/v2/QS-jewelblast.jpg","DisplayName":"宝石爆炸","typeid":"4","LapisId":"QS-jewelblast"},
	{"ButtonImagePath":"qt/v2/EVP-fruitburst.jpg","DisplayName":"水果爆裂","typeid":"4","LapisId":"EVP-fruitburst"},
	{"ButtonImagePath":"qt/v2/QS-phoenixsun.jpg","DisplayName":"凤凰的涅槃","typeid":"4","LapisId":"QS-phoenixsun"},
	{"ButtonImagePath":"qt/v2/NLC-wixx.jpg","DisplayName":"炫彩水晶","typeid":"4","LapisId":"NLC-wixx"},
	{"ButtonImagePath":"qt/v2/QS-goldlab.jpg","DisplayName":"黄金实验室","typeid":"4","LapisId":"QS-goldlab"},
	{"ButtonImagePath":"qt/v2/QS-kingcolossus.jpg","DisplayName":"国王巨像","typeid":"4","LapisId":"QS-kingcolossus"},
	{"ButtonImagePath":"qt/v2/HAB-fenghuang.jpg","DisplayName":"凤凰","typeid":"4","LapisId":"HAB-fenghuang"},
	{"ButtonImagePath":"qt/v2/EVP-theemperorstomb.jpg","DisplayName":"皇帝陵墓","typeid":"4","LapisId":"EVP-theemperorstomb"},
	{"ButtonImagePath":"qt/v2/OGS-wellofwonders.jpg","DisplayName":"魔幻森林","typeid":"4","LapisId":"OGS-wellofwonders"},
	{"ButtonImagePath":"qt/v2/QS-sevenshigh.jpg","DisplayName":"疯狂777","typeid":"4","LapisId":"QS-sevenshigh"},
	{"ButtonImagePath":"qt/v2/QS-geniestouch.jpg","DisplayName":"阿拉丁神灯","typeid":"4","LapisId":"QS-geniestouch"},
	{"ButtonImagePath":"qt/v2/ELK-hongkongtower.jpg","DisplayName":"香港摩天大楼","typeid":"4","LapisId":"ELK-hongkongtower"},
	{"ButtonImagePath":"qt/v2/OGS-1can2can.jpg","DisplayName":"丛林鹦鹉","typeid":"2","LapisId":"OGS-1can2can"},
	{"ButtonImagePath":"qt/v2/OGS-300shields.jpg","DisplayName":"斯巴达之盾","typeid":"2","LapisId":"OGS-300shields"},
	{"ButtonImagePath":"qt/v2/OGS-5knights.jpg","DisplayName":"圣骑士","typeid":"2","LapisId":"OGS-5knights"},
	{"ButtonImagePath":"qt/v2/OGS-adragonstory.jpg","DisplayName":"龙的传说","typeid":"2","LapisId":"OGS-adragonstory"},
	{"ButtonImagePath":"qt/v2/OGS-awhileonthenile.jpg","DisplayName":"尼罗河探险","typeid":"2","LapisId":"OGS-awhileonthenile"},
	{"ButtonImagePath":"qt/v2/OGS-andrethegiant.jpg","DisplayName":"巨人安德鲁","typeid":"2","LapisId":"OGS-andrethegiant"},
	{"ButtonImagePath":"qt/v2/OGS-aneveningwithhollymadison.jpg","DisplayName":"霍莉麦迪逊之夜","typeid":"2","LapisId":"OGS-aneveningwithhollymadison"},
	{"ButtonImagePath":"qt/v2/OGS-bangkoknights.jpg","DisplayName":"曼谷奇妙夜","typeid":"2","LapisId":"OGS-bangkoknights"},
	{"ButtonImagePath":"qt/v2/OGS-bigfoot.jpg","DisplayName":"寻找大脚怪","typeid":"2","LapisId":"OGS-bigfoot"},
	{"ButtonImagePath":"qt/v2/OGS-bingobillions.jpg","DisplayName":"百万宾果","typeid":"2","LapisId":"OGS-bingobillions"},
	{"ButtonImagePath":"qt/v2/OGS-blazinhot7s.jpg","DisplayName":"燃烧7's","typeid":"2","LapisId":"OGS-blazinhot7s"},
	{"ButtonImagePath":"qt/v2/OGS-bloodlorevampireclan.jpg","DisplayName":"吸血鬼家族","typeid":"2","LapisId":"OGS-bloodlorevampireclan"},
	{"ButtonImagePath":"qt/v2/OGS-bobby7s.jpg","DisplayName":"蠢贼鲍比","typeid":"2","LapisId":"OGS-bobby7s"},
	{"ButtonImagePath":"qt/v2/OGS-bucaneersbay.jpg","DisplayName":"海盗湾","typeid":"2","LapisId":"OGS-bucaneersbay"},
	{"ButtonImagePath":"qt/v2/OGS-butterflies.jpg","DisplayName":"蝴蝶物语","typeid":"2","LapisId":"OGS-butterflies"},
	{"ButtonImagePath":"qt/v2/OGS-californiagold.jpg","DisplayName":"加州掘金","typeid":"2","LapisId":"OGS-californiagold"},
	{"ButtonImagePath":"qt/v2/OGS-callofthecolosseum.jpg","DisplayName":"罗马竞技场","typeid":"2","LapisId":"OGS-callofthecolosseum"},
	{"ButtonImagePath":"qt/v2/OGS-cashstampede.jpg","DisplayName":"现金踩踏","typeid":"2","LapisId":"OGS-cashstampede"},
	{"ButtonImagePath":"qt/v2/OGS-casinomeister.jpg","DisplayName":"赌场大亨","typeid":"2","LapisId":"OGS-casinomeister"},
	{"ButtonImagePath":"qt/v2/OGS-charmsandwitches.jpg","DisplayName":"女巫的魔咒","typeid":"2","LapisId":"OGS-charmsandwitches"},
	{"ButtonImagePath":"qt/v2/OGS-cherryblossoms.jpg","DisplayName":"樱花恋","typeid":"2","LapisId":"OGS-cherryblossoms"},
	{"ButtonImagePath":"qt/v2/OGS-coolbananas.jpg","DisplayName":"炫酷香蕉","typeid":"2","LapisId":"OGS-coolbananas"},
	{"ButtonImagePath":"qt/v2/OGS-crocodopolis.jpg","DisplayName":"阿努比斯","typeid":"2","LapisId":"OGS-crocodopolis"},
	{"ButtonImagePath":"qt/v2/OGS-crowningglory.jpg","DisplayName":"至高荣耀","typeid":"2","LapisId":"OGS-crowningglory"},
	{"ButtonImagePath":"qt/v2/OGS-doctorlove.jpg","DisplayName":"爱情医生","typeid":"2","LapisId":"OGS-doctorlove"},
	{"ButtonImagePath":"qt/v2/OGS-doubleplaysuperbeth5hq.jpg","DisplayName":"新-超级赢家","typeid":"2","LapisId":"OGS-doubleplaysuperbeth5hq"},
	{"ButtonImagePath":"qt/v2/OGS-drloveonvacation.jpg","DisplayName":"爱情假日","typeid":"2","LapisId":"OGS-drloveonvacation"},
	{"ButtonImagePath":"qt/v2/OGS-dragondrop.jpg","DisplayName":"驯龙记","typeid":"2","LapisId":"OGS-dragondrop"},
	{"ButtonImagePath":"qt/v2/OGS-dragonwins.jpg","DisplayName":"巨龙之巅","typeid":"2","LapisId":"OGS-dragonwins"},
	{"ButtonImagePath":"qt/v2/OGS-druidessgoldhq.jpg","DisplayName":"夺金巫师HQ","typeid":"2","LapisId":"OGS-druidessgoldhq"},
	{"ButtonImagePath":"qt/v2/OGS-dynasty.jpg","DisplayName":"大清王朝","typeid":"2","LapisId":"OGS-dynasty"},
	{"ButtonImagePath":"qt/v2/OGS-easterndragon.jpg","DisplayName":"东海龙王","typeid":"2","LapisId":"OGS-easterndragon"},
	{"ButtonImagePath":"qt/v2/OGS-easyslider.jpg","DisplayName":"公路漂移","typeid":"2","LapisId":"OGS-easyslider"},
	{"ButtonImagePath":"qt/v2/OGS-egyptianrise.jpg","DisplayName":"埃及的崛起","typeid":"2","LapisId":"OGS-egyptianrise"},
	{"ButtonImagePath":"qt/v2/OGS-emeraldisle.jpg","DisplayName":"翡翠岛","typeid":"2","LapisId":"OGS-emeraldisle"},
	{"ButtonImagePath":"qt/v2/OGS-emperorsgarden.jpg","DisplayName":"皇庭踏青","typeid":"2","LapisId":"OGS-emperorsgarden"},
	{"ButtonImagePath":"qt/v2/OGS-enchantedmermaid.jpg","DisplayName":"人鱼公主","typeid":"2","LapisId":"OGS-enchantedmermaid"},
	{"ButtonImagePath":"qt/v2/OGS-extracash.jpg","DisplayName":"好多钞票","typeid":"2","LapisId":"OGS-extracash"},
	{"ButtonImagePath":"qt/v2/OGS-fairiesforest.jpg","DisplayName":"绿野仙踪","typeid":"2","LapisId":"OGS-fairiesforest"},
	{"ButtonImagePath":"qt/v2/OGS-fiestacubana.jpg","DisplayName":"热辣古巴","typeid":"2","LapisId":"OGS-fiestacubana"},
	{"ButtonImagePath":"qt/v2/OGS-firehawk.jpg","DisplayName":"印第安火鹰","typeid":"2","LapisId":"OGS-firehawk"},
	{"ButtonImagePath":"qt/v2/OGS-fortunesofthedead.jpg","DisplayName":"万圣节夺宝","typeid":"2","LapisId":"OGS-fortunesofthedead"},{"ButtonImagePath":"qt/v2/OGS-foxinwins.jpg","DisplayName":"狐狸家族","typeid":"2","LapisId":"OGS-foxinwins"},{"ButtonImagePath":"qt/v2/OGS-foxinwinsaveryfoxinchristmas.jpg","DisplayName":"狐狸家族圣诞狂欢","typeid":"2","LapisId":"OGS-foxinwinsaveryfoxinchristmas"},{"ButtonImagePath":"qt/v2/OGS-foxinwinsagain.jpg","DisplayName":"狐狸家族再临","typeid":"2","LapisId":"OGS-foxinwinsagain"},{"ButtonImagePath":"qt/v2/OGS-goldahoy.jpg","DisplayName":"黄金海盗","typeid":"2","LapisId":"OGS-goldahoy"},{"ButtonImagePath":"qt/v2/OGS-golden.jpg","DisplayName":"金鸡报喜","typeid":"2","LapisId":"OGS-golden"},{"ButtonImagePath":"qt/v2/OGS-goldenmane.jpg","DisplayName":"金鬃马","typeid":"2","LapisId":"OGS-goldenmane"},{"ButtonImagePath":"qt/v2/OGS-gloriousempirehqh5.jpg","DisplayName":"帝国荣耀H5 HQ","typeid":"2","LapisId":"OGS-gloriousempirehqh5"},{"ButtonImagePath":"qt/v2/OGS-giantgems.jpg","DisplayName":"巨型宝石","typeid":"2","LapisId":"OGS-giantgems"},{"ButtonImagePath":"qt/v2/OGS-gorillagowild.jpg","DisplayName":"疯狂大猩猩","typeid":"2","LapisId":"OGS-gorillagowild"},{"ButtonImagePath":"qt/v2/OGS-geniewild.jpg","DisplayName":"精灵仙子","typeid":"2","LapisId":"OGS-geniewild"},{"ButtonImagePath":"qt/v2/OGS-greatwildelk.jpg","DisplayName":"麋鹿之光","typeid":"2","LapisId":"OGS-greatwildelk"},{"ButtonImagePath":"qt/v2/OGS-hotroller.jpg","DisplayName":"激情转轮","typeid":"2","LapisId":"OGS-hotroller"},{"ButtonImagePath":"qt/v2/OGS-huolongvalley.jpg","DisplayName":"喷火龙山谷","typeid":"2","LapisId":"OGS-huolongvalley"},{"ButtonImagePath":"qt/v2/OGS-irisheyes.jpg","DisplayName":"爱尔兰之眼","typeid":"2","LapisId":"OGS-irisheyes"},{"ButtonImagePath":"qt/v2/OGS-irisheyes2.jpg","DisplayName":"爱尔兰之眼2","typeid":"2","LapisId":"OGS-irisheyes2"},{"ButtonImagePath":"qt/v2/OGS-jackpotjester50k.jpg","DisplayName":"小丑奖池","typeid":"2","LapisId":"OGS-jackpotjester50k"},{"ButtonImagePath":"qt/v2/OGS-jackpotjester50kh5hq.jpg","DisplayName":"小丑奖池H5 HQ","typeid":"2","LapisId":"OGS-jackpotjester50kh5hq"},{"ButtonImagePath":"qt/v2/OGS-jackpotjesterwildnudge.jpg","DisplayName":"头奖幸运小丑","typeid":"2","LapisId":"OGS-jackpotjesterwildnudge"},{"ButtonImagePath":"qt/v2/OGS-jacksbeanstalk.jpg","DisplayName":"杰克的豌豆唠","typeid":"2","LapisId":"OGS-jacksbeanstalk"},{"ButtonImagePath":"qt/v2/OGS-jadeidol.jpg","DisplayName":"翡翠猴","typeid":"2","LapisId":"OGS-jadeidol"},{"ButtonImagePath":"qt/v2/OGS-jamesdean.jpg","DisplayName":"詹姆斯·迪恩","typeid":"2","LapisId":"OGS-jamesdean"},{"ButtonImagePath":"qt/v2/OGS-joker10000.jpg","DisplayName":"小丑10k","typeid":"2","LapisId":"OGS-joker10000"},{"ButtonImagePath":"qt/v2/OGS-joker10000deluxe.jpg","DisplayName":"小丑10K 升级版","typeid":"2","LapisId":"OGS-joker10000deluxe"},{"ButtonImagePath":"qt/v2/OGS-jokerjester.jpg","DisplayName":"滑稽小丑","typeid":"2","LapisId":"OGS-jokerjester"},{"ButtonImagePath":"qt/v2/OGS-jollysgifts.jpg","DisplayName":"乔莉的礼物","typeid":"2","LapisId":"OGS-jollysgifts"},{"ButtonImagePath":"qt/v2/OGS-judgedredd.jpg","DisplayName":"特警判官","typeid":"2","LapisId":"OGS-judgedredd"},{"ButtonImagePath":"qt/v2/OGS-jukepot.jpg","DisplayName":"音乐盒","typeid":"2","LapisId":"OGS-jukepot"},{"ButtonImagePath":"qt/v2/OGS-kingkong.jpg","DisplayName":"金刚","typeid":"2","LapisId":"OGS-kingkong"},{"ButtonImagePath":"qt/v2/OGS-kingtiger.jpg","DisplayName":"虎王","typeid":"2","LapisId":"OGS-kingtiger"},{"ButtonImagePath":"qt/v2/OGS-lacucaracha.jpg","DisplayName":"狂野墨西哥","typeid":"2","LapisId":"OGS-lacucaracha"},{"ButtonImagePath":"qt/v2/OGS-lightninggems.jpg","DisplayName":"闪电宝石","typeid":"2","LapisId":"OGS-lightninggems"},{"ButtonImagePath":"qt/v2/OGS-lovebugs.jpg","DisplayName":"恋爱毛毛虫","typeid":"2","LapisId":"OGS-lovebugs"},{"ButtonImagePath":"qt/v2/OGS-madmadmonkey.jpg","DisplayName":"疯狂的猴子","typeid":"2","LapisId":"OGS-madmadmonkey"},{"ButtonImagePath":"qt/v2/OGS-maidomoney.jpg","DisplayName":"拜金女仆","typeid":"2","LapisId":"OGS-maidomoney"},{"ButtonImagePath":"qt/v2/OGS-manicmillions.jpg","DisplayName":"科学怪人","typeid":"2","LapisId":"OGS-manicmillions"},{"ButtonImagePath":"qt/v2/OGS-medusa.jpg","DisplayName":"美杜莎","typeid":"2","LapisId":"OGS-medusa"},{"ButtonImagePath":"qt/v2/OGS-medusa2h5hq.jpg","DisplayName":"新-美杜莎2","typeid":"2","LapisId":"OGS-medusa2h5hq"},{"ButtonImagePath":"qt/v2/OGS-merlinsmagicrespins.jpg","DisplayName":"梅林的魔法药剂","typeid":"2","LapisId":"OGS-merlinsmagicrespins"},{"ButtonImagePath":"qt/v2/OGS-merlinsmagicrespinschristmas.jpg","DisplayName":"圣诞版-梅林魔法","typeid":"2","LapisId":"OGS-merlinsmagicrespinschristmas"},{"ButtonImagePath":"qt/v2/OGS-merlinsmillionssuperbeth5hq.jpg","DisplayName":"新-梅林法宝","typeid":"2","LapisId":"OGS-merlinsmillionssuperbeth5hq"},{"ButtonImagePath":"qt/v2/OGS-missmidas.jpg","DisplayName":"蜜达斯小姐","typeid":"2","LapisId":"OGS-missmidas"},{"ButtonImagePath":"qt/v2/OGS-monsterwins.jpg","DisplayName":"怪兽大学","typeid":"2","LapisId":"OGS-monsterwins"},{"ButtonImagePath":"qt/v2/OGS-munchers.jpg","DisplayName":"血腥大棚","typeid":"2","LapisId":"OGS-munchers"},{"ButtonImagePath":"qt/v2/OGS-napoleonboneyparts.jpg","DisplayName":"骨头大帝拿破仑","typeid":"2","LapisId":"OGS-napoleonboneyparts"},{"ButtonImagePath":"qt/v2/OGS-oilmania.jpg","DisplayName":"狂热石油","typeid":"2","LapisId":"OGS-oilmania"},{"ButtonImagePath":"qt/v2/OGS-owleyesnova.jpg","DisplayName":"新-猫头鹰之眼","typeid":"2","LapisId":"OGS-owleyesnova"},{"ButtonImagePath":"qt/v2/OGS-pandamania.jpg","DisplayName":"熊猫团圆","typeid":"2","LapisId":"OGS-pandamania"},{"ButtonImagePath":"qt/v2/OGS-pizzaprize.jpg","DisplayName":"披萨锦标赛","typeid":"2","LapisId":"OGS-pizzaprize"},{"ButtonImagePath":"qt/v2/OGS-platinumpyramid.jpg","DisplayName":"金字塔","typeid":"2","LapisId":"OGS-platinumpyramid"},{"ButtonImagePath":"qt/v2/OGS-potioncommotion.jpg","DisplayName":"骚动药水","typeid":"2","LapisId":"OGS-potioncommotion"},{"ButtonImagePath":"qt/v2/OGS-prosperitytwin.jpg","DisplayName":"财运亨通","typeid":"2","LapisId":"OGS-prosperitytwin"},{"ButtonImagePath":"qt/v2/OGS-psycho.jpg","DisplayName":"神经兮兮","typeid":"2","LapisId":"OGS-psycho"},{"ButtonImagePath":"qt/v2/OGS-ramessesriches.jpg","DisplayName":"拉美西斯宝藏","typeid":"2","LapisId":"OGS-ramessesriches"},{"ButtonImagePath":"qt/v2/OGS-reelsoffortunetriplepay.jpg","DisplayName":"财富三部曲","typeid":"2","LapisId":"OGS-reelsoffortunetriplepay"},{"ButtonImagePath":"qt/v2/OGS-renegades.jpg","DisplayName":"赤色雄狼","typeid":"2","LapisId":"OGS-renegades"},{"ButtonImagePath":"qt/v2/OGS-robinhoodprinceoftweets.jpg","DisplayName":"绿林英雄—罗宾汉","typeid":"2","LapisId":"OGS-robinhoodprinceoftweets"},{"ButtonImagePath":"qt/v2/OGS-samuraisplit.jpg","DisplayName":"古怪的武士","typeid":"2","LapisId":"OGS-samuraisplit"},{"ButtonImagePath":"qt/v2/OGS-shaaarksuperbet.jpg","DisplayName":"鲨鱼出没","typeid":"2","LapisId":"OGS-shaaarksuperbet"},{"ButtonImagePath":"qt/v2/OGS-shangri-la.jpg","DisplayName":"香格里拉","typeid":"2","LapisId":"OGS-shangri-la"},{"ButtonImagePath":"qt/v2/OGS-slotsofmoney.jpg","DisplayName":"抓钱老虎机","typeid":"2","LapisId":"OGS-slotsofmoney"},{"ButtonImagePath":"qt/v2/OGS-snowflakes.jpg","DisplayName":"雪绒花","typeid":"2","LapisId":"OGS-snowflakes"},{"ButtonImagePath":"qt/v2/OGS-spanisheyes.jpg","DisplayName":"情定西班牙","typeid":"2","LapisId":"OGS-spanisheyes"},{"ButtonImagePath":"qt/v2/OGS-spinsorceress.jpg","DisplayName":"旋转女巫","typeid":"2","LapisId":"OGS-spinsorceress"},{"ButtonImagePath":"qt/v2/OGS-starmania.jpg","DisplayName":"星星点灯","typeid":"2","LapisId":"OGS-starmania"},{"ButtonImagePath":"qt/v2/OGS-supersafari.jpg","DisplayName":"丛林之王","typeid":"2","LapisId":"OGS-supersafari"},{"ButtonImagePath":"qt/v2/OGS-teddybearspicnic.jpg","DisplayName":"可爱泰迪","typeid":"2","LapisId":"OGS-teddybearspicnic"},{"ButtonImagePath":"qt/v2/OGS-thaisunrise.jpg","DisplayName":"泰国日出","typeid":"2","LapisId":"OGS-thaisunrise"},{"ButtonImagePath":"qt/v2/OGS-thebermudamysteries.jpg","DisplayName":"百慕大之谜","typeid":"2","LapisId":"OGS-thebermudamysteries"},{"ButtonImagePath":"qt/v2/OGS-thecodfather.jpg","DisplayName":"鳕鱼之父","typeid":"2","LapisId":"OGS-thecodfather"},{"ButtonImagePath":"qt/v2/OGS-thesnakecharmer.jpg","DisplayName":"耍蛇老爹","typeid":"2","LapisId":"OGS-thesnakecharmer"},{"ButtonImagePath":"qt/v2/OGS-thespinlab.jpg","DisplayName":"旋转实验室","typeid":"2","LapisId":"OGS-thespinlab"},{"ButtonImagePath":"qt/v2/OGS-theatreofnight.jpg","DisplayName":"午夜剧院","typeid":"2","LapisId":"OGS-theatreofnight"},{"ButtonImagePath":"qt/v2/OGS-titanstorm.jpg","DisplayName":"泰坦风暴","typeid":"2","LapisId":"OGS-titanstorm"},{"ButtonImagePath":"qt/v2/OGS-tootincarman.jpg","DisplayName":"卡车嘟嘟","typeid":"2","LapisId":"OGS-tootincarman"},{"ButtonImagePath":"qt/v2/OGS-unicornlegend.jpg","DisplayName":"独角兽传奇","typeid":"2","LapisId":"OGS-unicornlegend"},{"ButtonImagePath":"qt/v2/OGS-valhalla.jpg","DisplayName":"瓦尔哈拉","typeid":"2","LapisId":"OGS-valhalla"},{"ButtonImagePath":"qt/v2/OGS-venetianrose.jpg","DisplayName":"情定威尼斯","typeid":"2","LapisId":"OGS-venetianrose"},{"ButtonImagePath":"qt/v2/OGS-venicemagic.jpg","DisplayName":"魔力威尼斯","typeid":"2","LapisId":"OGS-venicemagic"},{"ButtonImagePath":"qt/v2/OGS-volcanoeruption.jpg","DisplayName":"火山爆发","typeid":"2","LapisId":"OGS-volcanoeruption"},{"ButtonImagePath":"qt/v2/OGS-wildcatcanyon.jpg","DisplayName":"野猫峡谷","typeid":"2","LapisId":"OGS-wildcatcanyon"},{"ButtonImagePath":"qt/v2/OGS-witchpickings.jpg","DisplayName":"美妙巫师","typeid":"2","LapisId":"OGS-witchpickings"},{"ButtonImagePath":"qt/v2/OGS-wilddodo.jpg","DisplayName":"渡渡鸟","typeid":"2","LapisId":"OGS-wilddodo"},{"ButtonImagePath":"qt/v2/OGS-wildplaysuperbet.jpg","DisplayName":"超级投注","typeid":"2","LapisId":"OGS-wildplaysuperbet"},{"ButtonImagePath":"qt/v2/OGS-wildrun.jpg","DisplayName":"极速奔跑","typeid":"2","LapisId":"OGS-wildrun"},{"ButtonImagePath":"qt/v2/OGS-wildwest.jpg","DisplayName":"狂野西部","typeid":"2","LapisId":"OGS-wildwest"},{"ButtonImagePath":"qt/v2/OGS-wolfpackpays.jpg","DisplayName":"狼群嚎叫","typeid":"2","LapisId":"OGS-wolfpackpays"},{"ButtonImagePath":"qt/v2/OGS-xingguardian.jpg","DisplayName":"星辰守护","typeid":"2","LapisId":"OGS-xingguardian"},{"ButtonImagePath":"qt/v2/OGS-bonanza.jpg","DisplayName":"矿山探险","typeid":"2","LapisId":"OGS-bonanza"},{"ButtonImagePath":"qt/v2/OGS-dangerhighvoltage.jpg","DisplayName":"霹雳娇娃","typeid":"2","LapisId":"OGS-dangerhighvoltage"},{"ButtonImagePath":"qt/v2/OGS-dragonborn.jpg","DisplayName":"龙腾","typeid":"2","LapisId":"OGS-dragonborn"},{"ButtonImagePath":"qt/v2/OGS-gold.jpg","DisplayName":"黄金","typeid":"2","LapisId":"OGS-gold"},{"ButtonImagePath":"qt/v2/OGS-queenofriches.jpg","DisplayName":"埃及艳后","typeid":"2","LapisId":"OGS-queenofriches"},{"ButtonImagePath":"qt/v2/OGS-starquest.jpg","DisplayName":"星际探索","typeid":"2","LapisId":"OGS-starquest"},{"ButtonImagePath":"qt/v2/OGS-whiterabbit.jpg","DisplayName":"白兔先生","typeid":"2","LapisId":"OGS-whiterabbit"},{"ButtonImagePath":"qt/v2/OGS-aladdinslegacy.jpg","DisplayName":"阿拉丁的遗产","typeid":"2","LapisId":"OGS-aladdinslegacy"},{"ButtonImagePath":"qt/v2/OGS-barsandbells.jpg","DisplayName":"酒吧门铃","typeid":"2","LapisId":"OGS-barsandbells"},{"ButtonImagePath":"qt/v2/OGS-dragon8s.jpg","DisplayName":"龙8","typeid":"2","LapisId":"OGS-dragon8s"},{"ButtonImagePath":"qt/v2/OGS-fortunesoftheamazon.jpg","DisplayName":"亚马逊财富","typeid":"2","LapisId":"OGS-fortunesoftheamazon"},{"ButtonImagePath":"qt/v2/OGS-gulliverstravels.jpg","DisplayName":"格力菲游记","typeid":"2","LapisId":"OGS-gulliverstravels"},{"ButtonImagePath":"qt/v2/OGS-leonidas.jpg","DisplayName":"莱奥尼达斯国王","typeid":"2","LapisId":"OGS-leonidas"},{"ButtonImagePath":"qt/v2/OGS-shogunshowdown.jpg","DisplayName":"决战将军","typeid":"2","LapisId":"OGS-shogunshowdown"},{"ButtonImagePath":"qt/v2/OGS-sinfulspins.jpg","DisplayName":"罪恶旋转","typeid":"2","LapisId":"OGS-sinfulspins"},{"ButtonImagePath":"qt/v2/OGS-thunderingzeus.jpg","DisplayName":"雷鸣宙斯","typeid":"2","LapisId":"OGS-thunderingzeus"},{"ButtonImagePath":"qt/v2/OGS-vampiresvswerewolves.jpg","DisplayName":"吸血鬼大战狼人","typeid":"2","LapisId":"OGS-vampiresvswerewolves"},{"ButtonImagePath":"qt/v2/OGS-angelstouch.jpg","DisplayName":"天使的触摸","typeid":"2","LapisId":"OGS-angelstouch"},{"ButtonImagePath":"qt/v2/OGS-astrocat.jpg","DisplayName":"阿斯特罗猫","typeid":"2","LapisId":"OGS-astrocat"},{"ButtonImagePath":"qt/v2/OGS-blazinggoddess.jpg","DisplayName":"炽热女神","typeid":"2","LapisId":"OGS-blazinggoddess"},{"ButtonImagePath":"qt/v2/OGS-chilligold.jpg","DisplayName":"黄金辣椒","typeid":"2","LapisId":"OGS-chilligold"},{"ButtonImagePath":"qt/v2/OGS-chilligoldx2.jpg","DisplayName":"黄金辣椒2","typeid":"2","LapisId":"OGS-chilligoldx2"},{"ButtonImagePath":"qt/v2/OGS-diamondtower.jpg","DisplayName":"钻石大厦","typeid":"2","LapisId":"OGS-diamondtower"},{"ButtonImagePath":"qt/v2/OGS-dolphingoldh5hq.jpg","DisplayName":"闪光海豚H5 HQ","typeid":"2","LapisId":"OGS-dolphingoldh5hq"},{"ButtonImagePath":"qt/v2/OGS-dolphingoldstellarjackpots.jpg","DisplayName":"闪光海豚-星空大战","typeid":"2","LapisId":"OGS-dolphingoldstellarjackpots"},{"ButtonImagePath":"qt/v2/OGS-dragonpalace.jpg","DisplayName":"龙宫","typeid":"2","LapisId":"OGS-dragonpalace"},{"ButtonImagePath":"qt/v2/OGS-druidessgold.jpg","DisplayName":"夺金巫师","typeid":"2","LapisId":"OGS-druidessgold"},{"ButtonImagePath":"qt/v2/OGS-fivepirates.jpg","DisplayName":"五个海盗","typeid":"2","LapisId":"OGS-fivepirates"},{"ButtonImagePath":"qt/v2/OGS-fortune8cat.jpg","DisplayName":"幸运招财猫","typeid":"2","LapisId":"OGS-fortune8cat"},{"ButtonImagePath":"qt/v2/OGS-frogsnflies.jpg","DisplayName":"捕蝇大赛","typeid":"2","LapisId":"OGS-frogsnflies"},{"ButtonImagePath":"qt/v2/OGS-losttemple.jpg","DisplayName":"消失的神庙","typeid":"2","LapisId":"OGS-losttemple"},{"ButtonImagePath":"qt/v2/OGS-moontemple.jpg","DisplayName":"月光神庙","typeid":"2","LapisId":"OGS-moontemple"},{"ButtonImagePath":"qt/v2/OGS-moremonkeys.jpg","DisplayName":"齐天大圣","typeid":"2","LapisId":"OGS-moremonkeys"},{"ButtonImagePath":"qt/v2/OGS-pandapow.jpg","DisplayName":"熊猫勇士","typeid":"2","LapisId":"OGS-pandapow"},{"ButtonImagePath":"qt/v2/OGS-pixiegold.jpg","DisplayName":"调皮精灵","typeid":"2","LapisId":"OGS-pixiegold"},{"ButtonImagePath":"qt/v2/OGS-redroo.jpg","DisplayName":"赤焰袋鼠","typeid":"2","LapisId":"OGS-redroo"},{"ButtonImagePath":"qt/v2/OGS-samuraiprincess.jpg","DisplayName":"武士公主","typeid":"2","LapisId":"OGS-samuraiprincess"},{"ButtonImagePath":"qt/v2/OGS-serengetidiamonds.jpg","DisplayName":"塞伦盖蒂之钻","typeid":"2","LapisId":"OGS-serengetidiamonds"},{"ButtonImagePath":"qt/v2/OGS-serengetilionsstellarjackpots.jpg","DisplayName":"塞伦盖蒂雄狮","typeid":"2","LapisId":"OGS-serengetilionsstellarjackpots"},{"ButtonImagePath":"qt/v2/OGS-silverlion.jpg","DisplayName":"万兽之王","typeid":"2","LapisId":"OGS-silverlion"},{"ButtonImagePath":"qt/v2/OGS-vikingfire.jpg","DisplayName":"维京之火","typeid":"2","LapisId":"OGS-vikingfire"},{"ButtonImagePath":"qt/v2/OGS-1429unchartedseas.jpg","DisplayName":"未知海洋探险-1429","typeid":"2","LapisId":"OGS-1429unchartedseas"},{"ButtonImagePath":"qt/v2/OGS-arcader.jpg","DisplayName":"神秘的巨石","typeid":"2","LapisId":"OGS-arcader"},{"ButtonImagePath":"qt/v2/OGS-babushkas.jpg","DisplayName":"俄罗斯老太","typeid":"2","LapisId":"OGS-babushkas"},{"ButtonImagePath":"qt/v2/OGS-barbershopuncut.jpg","DisplayName":"巴尔伯理发师","typeid":"2","LapisId":"OGS-barbershopuncut"},{"ButtonImagePath":"qt/v2/OGS-birdsonawire.jpg","DisplayName":"愤怒的小鸟","typeid":"2","LapisId":"OGS-birdsonawire"},{"ButtonImagePath":"qt/v2/OGS-esqueletoexplosivo.jpg","DisplayName":"骷髅乐队","typeid":"2","LapisId":"OGS-esqueletoexplosivo"},{"ButtonImagePath":"qt/v2/OGS-flux.jpg","DisplayName":"水晶宝藏","typeid":"2","LapisId":"OGS-flux"},{"ButtonImagePath":"qt/v2/OGS-froggrog.jpg","DisplayName":"格罗格醉蛙","typeid":"2","LapisId":"OGS-froggrog"},{"ButtonImagePath":"qt/v2/OGS-fruitwarp.jpg","DisplayName":"水果大战","typeid":"2","LapisId":"OGS-fruitwarp"},{"ButtonImagePath":"qt/v2/OGS-fullmoonromance.jpg","DisplayName":"满月传说","typeid":"2","LapisId":"OGS-fullmoonromance"},{"ButtonImagePath":"qt/v2/OGS-luchadora.jpg","DisplayName":"拳击场","typeid":"2","LapisId":"OGS-luchadora"},{"ButtonImagePath":"qt/v2/OGS-magicious.jpg","DisplayName":"魔术大师","typeid":"2","LapisId":"OGS-magicious"},{"ButtonImagePath":"qt/v2/OGS-notenoughkittens.jpg","DisplayName":"顽皮猫咪","typeid":"2","LapisId":"OGS-notenoughkittens"},{"ButtonImagePath":"qt/v2/OGS-pinkelephants.jpg","DisplayName":"粉象","typeid":"2","LapisId":"OGS-pinkelephants"},{"ButtonImagePath":"qt/v2/OGS-roastymcfryandtheflamebusters.jpg","DisplayName":"灭火特工队","typeid":"2","LapisId":"OGS-roastymcfryandtheflamebusters"},{"ButtonImagePath":"qt/v2/OGS-spectra.jpg","DisplayName":"光谱","typeid":"2","LapisId":"OGS-spectra"},{"ButtonImagePath":"qt/v2/OGS-sunsetdelight.jpg","DisplayName":"日落余晖","typeid":"2","LapisId":"OGS-sunsetdelight"},{"ButtonImagePath":"qt/v2/OGS-therift.jpg","DisplayName":"密室逃亡","typeid":"2","LapisId":"OGS-therift"},{"ButtonImagePath":"qt/v2/OGS-tokitime.jpg","DisplayName":"开心消消乐","typeid":"2","LapisId":"OGS-tokitime"},{"ButtonImagePath":"qt/v2/OGS-turningtotems.jpg","DisplayName":"旋转图腾","typeid":"2","LapisId":"OGS-turningtotems"},{"ButtonImagePath":"qt/v2/OGS-wellofwonders.jpg","DisplayName":"魔幻森林","typeid":"2","LapisId":"OGS-wellofwonders"},{"ButtonImagePath":"qt/v2/OGS-zoom.jpg","DisplayName":"水果幻灯片","typeid":"2","LapisId":"OGS-zoom"},{"ButtonImagePath":"qt/v2/HAB-12zodiacs.jpg","DisplayName":"十二生肖","typeid":"2","LapisId":"HAB-12zodiacs"},{"ButtonImagePath":"qt/v2/HAB-arcaneelements.jpg","DisplayName":"神秘元素","typeid":"2","LapisId":"HAB-arcaneelements"},{"ButtonImagePath":"qt/v2/HAB-birdofthunder.jpg","DisplayName":"雷鸟","typeid":"2","LapisId":"HAB-birdofthunder"},{"ButtonImagePath":"qt/v2/HAB-bombsaway.jpg","DisplayName":"炸弹追击","typeid":"2","LapisId":"HAB-bombsaway"},{"ButtonImagePath":"qt/v2/HAB-cakevalley.jpg","DisplayName":"蛋糕谷","typeid":"2","LapisId":"HAB-cakevalley"},{"ButtonImagePath":"qt/v2/HAB-coyotecrash.jpg","DisplayName":"狼贼夺宝","typeid":"2","LapisId":"HAB-coyotecrash"},{"ButtonImagePath":"qt/v2/HAB-dragonsrealm.jpg","DisplayName":"神龙之境","typeid":"2","LapisId":"HAB-dragonsrealm"},{"ButtonImagePath":"qt/v2/HAB-dragonsthrone.jpg","DisplayName":"龙之宝座","typeid":"2","LapisId":"HAB-dragonsthrone"},{"ButtonImagePath":"qt/v2/HAB-facaishen.jpg","DisplayName":"发财神","typeid":"2","LapisId":"HAB-facaishen"},{"ButtonImagePath":"qt/v2/HAB-fenghuang.jpg","DisplayName":"凤凰","typeid":"2","LapisId":"HAB-fenghuang"},{"ButtonImagePath":"qt/v2/HAB-firerooster.jpg","DisplayName":"公鸡王","typeid":"2","LapisId":"HAB-firerooster"},{"ButtonImagePath":"qt/v2/HAB-gangsters.jpg","DisplayName":"黑手党","typeid":"2","LapisId":"HAB-gangsters"},{"ButtonImagePath":"qt/v2/HAB-goldrush.jpg","DisplayName":"淘金疯狂","typeid":"2","LapisId":"HAB-goldrush"},{"ButtonImagePath":"qt/v2/HAB-indiancashcatcher.jpg","DisplayName":"印第安追梦","typeid":"2","LapisId":"HAB-indiancashcatcher"},{"ButtonImagePath":"qt/v2/HAB-jugglenaut.jpg","DisplayName":"惊喜秀","typeid":"2","LapisId":"HAB-jugglenaut"},{"ButtonImagePath":"qt/v2/HAB-koigate.jpg","DisplayName":"鲤鱼门","typeid":"2","LapisId":"HAB-koigate"},{"ButtonImagePath":"qt/v2/HAB-monstermashcash.jpg","DisplayName":"怪物聚集","typeid":"2","LapisId":"HAB-monstermashcash"},{"ButtonImagePath":"qt/v2/HAB-mysticfortune.jpg","DisplayName":"神秘宝藏","typeid":"2","LapisId":"HAB-mysticfortune"},{"ButtonImagePath":"qt/v2/HAB-oceanscall.jpg","DisplayName":"海洋之音","typeid":"2","LapisId":"HAB-oceanscall"},{"ButtonImagePath":"qt/v2/HAB-pandapanda.jpg","DisplayName":"双喜熊猫","typeid":"2","LapisId":"HAB-pandapanda"},{"ButtonImagePath":"qt/v2/HAB-rollingroger.jpg","DisplayName":"刺猬宝宝","typeid":"2","LapisId":"HAB-rollingroger"},{"ButtonImagePath":"qt/v2/HAB-romanempire.jpg","DisplayName":"罗马帝国","typeid":"2","LapisId":"HAB-romanempire"},{"ButtonImagePath":"qt/v2/HAB-ruffledup.jpg","DisplayName":"触电的小鸟","typeid":"2","LapisId":"HAB-ruffledup"},{"ButtonImagePath":"qt/v2/HAB-santasvillage.jpg","DisplayName":"圣诞乐村","typeid":"2","LapisId":"HAB-santasvillage"},{"ButtonImagePath":"qt/v2/HAB-scruffyscallywags.jpg","DisplayName":"欢乐帮","typeid":"2","LapisId":"HAB-scruffyscallywags"},{"ButtonImagePath":"qt/v2/HAB-shaolinfortunes.jpg","DisplayName":"少林宝藏","typeid":"2","LapisId":"HAB-shaolinfortunes"},{"ButtonImagePath":"qt/v2/HAB-sparta.jpg","DisplayName":"斯巴达","typeid":"2","LapisId":"HAB-sparta"},{"ButtonImagePath":"qt/v2/HAB-supertwister.jpg","DisplayName":"超级龙卷风","typeid":"2","LapisId":"HAB-supertwister"},{"ButtonImagePath":"qt/v2/HAB-thedeadescape.jpg","DisplayName":"死海逃生","typeid":"2","LapisId":"HAB-thedeadescape"},{"ButtonImagePath":"qt/v2/HAB-wickedwitch.jpg","DisplayName":"巫婆大财","typeid":"2","LapisId":"HAB-wickedwitch"},{"ButtonImagePath":"qt/v2/QS-bigbadwolf.jpg","DisplayName":"灰太狼","typeid":"2","LapisId":"QS-bigbadwolf"},{"ButtonImagePath":"qt/v2/QS-crystalqueen.jpg","DisplayName":"冰雪奇缘","typeid":"2","LapisId":"QS-crystalqueen"},{"ButtonImagePath":"qt/v2/QS-dragonshrine.jpg","DisplayName":"龙神殿","typeid":"2","LapisId":"QS-dragonshrine"},{"ButtonImagePath":"qt/v2/QS-fairygate.jpg","DisplayName":"仙境之门","typeid":"2","LapisId":"QS-fairygate"},{"ButtonImagePath":"qt/v2/QS-firestorm.jpg","DisplayName":"火暴","typeid":"2","LapisId":"QS-firestorm"},{"ButtonImagePath":"qt/v2/QS-geniestouch.jpg","DisplayName":"阿拉丁神灯","typeid":"2","LapisId":"QS-geniestouch"},{"ButtonImagePath":"qt/v2/QS-goldlab.jpg","DisplayName":"黄金实验室","typeid":"2","LapisId":"QS-goldlab"},{"ButtonImagePath":"qt/v2/QS-goldilocks.jpg","DisplayName":"金发姑娘","typeid":"2","LapisId":"QS-goldilocks"},{"ButtonImagePath":"qt/v2/QS-hiddenvalley.jpg","DisplayName":"神秘山谷","typeid":"2","LapisId":"QS-hiddenvalley"},{"ButtonImagePath":"qt/v2/QS-hotsync.jpg","DisplayName":"火热旋转","typeid":"2","LapisId":"QS-hotsync"},{"ButtonImagePath":"qt/v2/QS-illuminous.jpg","DisplayName":"嗨翻珠宝","typeid":"2","LapisId":"QS-illuminous"},{"ButtonImagePath":"qt/v2/QS-jewelblast.jpg","DisplayName":"宝石爆炸","typeid":"2","LapisId":"QS-jewelblast"},{"ButtonImagePath":"qt/v2/QS-kingcolossus.jpg","DisplayName":"国王巨像","typeid":"2","LapisId":"QS-kingcolossus"},{"ButtonImagePath":"qt/v2/QS-leprechaunhills.jpg","DisplayName":"精灵山","typeid":"2","LapisId":"QS-leprechaunhills"},{"ButtonImagePath":"qt/v2/QS-mayana.jpg","DisplayName":"玛雅人","typeid":"2","LapisId":"QS-mayana"},{"ButtonImagePath":"qt/v2/QS-mightyarthur.jpg","DisplayName":"亚瑟王","typeid":"2","LapisId":"QS-mightyarthur"},{"ButtonImagePath":"qt/v2/QS-phoenixsun.jpg","DisplayName":"凤凰的涅槃","typeid":"2","LapisId":"QS-phoenixsun"},{"ButtonImagePath":"qt/v2/QS-rapunzelstower.jpg","DisplayName":"长发公主","typeid":"2","LapisId":"QS-rapunzelstower"},{"ButtonImagePath":"qt/v2/QS-razortooth.jpg","DisplayName":"虎口狼牙","typeid":"2","LapisId":"QS-razortooth"},{"ButtonImagePath":"qt/v2/QS-royalfrog.jpg","DisplayName":"皇族青蛙","typeid":"2","LapisId":"QS-royalfrog"},{"ButtonImagePath":"qt/v2/QS-sakurafortune.jpg","DisplayName":"樱花侠女","typeid":"2","LapisId":"QS-sakurafortune"},{"ButtonImagePath":"qt/v2/QS-secondstrike.jpg","DisplayName":"连环炮","typeid":"2","LapisId":"QS-secondstrike"},{"ButtonImagePath":"qt/v2/QS-sevenshigh.jpg","DisplayName":"疯狂777","typeid":"2","LapisId":"QS-sevenshigh"},{"ButtonImagePath":"qt/v2/QS-sinbad.jpg","DisplayName":"辛巴达","typeid":"2","LapisId":"QS-sinbad"},{"ButtonImagePath":"qt/v2/QS-spinionsbeachparty.jpg","DisplayName":"小黄人海滩派对","typeid":"2","LapisId":"QS-spinionsbeachparty"},{"ButtonImagePath":"qt/v2/QS-stickybandits.jpg","DisplayName":"西部劫匪","typeid":"2","LapisId":"QS-stickybandits"},{"ButtonImagePath":"qt/v2/QS-sugartrail.jpg","DisplayName":"糖果传奇","typeid":"2","LapisId":"QS-sugartrail"},{"ButtonImagePath":"qt/v2/QS-theepicjourney.jpg","DisplayName":"史诗之旅","typeid":"2","LapisId":"QS-theepicjourney"},{"ButtonImagePath":"qt/v2/QS-thethreemusketeers.jpg","DisplayName":"三剑客","typeid":"2","LapisId":"QS-thethreemusketeers"},{"ButtonImagePath":"qt/v2/QS-thewildchase.jpg","DisplayName":"侠盗猎车手","typeid":"2","LapisId":"QS-thewildchase "},{"ButtonImagePath":"qt/v2/QS-titanthunder.jpg","DisplayName":"泰坦雷霆","typeid":"2","LapisId":"QS-titanthunder"},{"ButtonImagePath":"qt/v2/QS-treasureisland.jpg","DisplayName":"宝藏岛","typeid":"2","LapisId":"QS-treasureisland"},{"ButtonImagePath":"qt/v2/QS-winsoffortune.jpg","DisplayName":"美猴王","typeid":"2","LapisId":"QS-winsoffortune"},{"ButtonImagePath":"qt/v2/ELK-bloopers.jpg","DisplayName":"幕后花絮","typeid":"2","LapisId":"ELK-bloopers"},{"ButtonImagePath":"qt/v2/ELK-championsgoal.jpg","DisplayName":"足球宝贝","typeid":"2","LapisId":"ELK-championsgoal"},{"ButtonImagePath":"qt/v2/ELK-djwild.jpg","DisplayName":"狂野DJ","typeid":"2","LapisId":"ELK-djwild"},{"ButtonImagePath":"qt/v2/ELK-electricsam.jpg","DisplayName":"美女和野兽","typeid":"2","LapisId":"ELK-electricsam"},{"ButtonImagePath":"qt/v2/ELK-hidden.jpg","DisplayName":"古埃法老王","typeid":"2","LapisId":"ELK-hidden"},{"ButtonImagePath":"qt/v2/ELK-hohotower.jpg","DisplayName":"HoHo大楼","typeid":"2","LapisId":"ELK-hohotower"},{"ButtonImagePath":"qt/v2/ELK-hongkongtower.jpg","DisplayName":"香港摩天大楼","typeid":"2","LapisId":"ELK-hongkongtower"},{"ButtonImagePath":"qt/v2/ELK-ivanhoe.jpg","DisplayName":"劫后英雄传","typeid":"2","LapisId":"ELK-ivanhoe"},{"ButtonImagePath":"qt/v2/ELK-poltava.jpg","DisplayName":"波尔塔瓦之战","typeid":"2","LapisId":"ELK-poltava"},{"ButtonImagePath":"qt/v2/ELK-route777.jpg","DisplayName":"公路777","typeid":"2","LapisId":"ELK-route777"},{"ButtonImagePath":"qt/v2/ELK-samonthebeach.jpg","DisplayName":"夏日海滩","typeid":"2","LapisId":"ELK-samonthebeach"},{"ButtonImagePath":"qt/v2/ELK-tacobrothers.jpg","DisplayName":"塔科兄弟","typeid":"2","LapisId":"ELK-tacobrothers"},{"ButtonImagePath":"qt/v2/ELK-tacobrotherssavingchristmas.jpg","DisplayName":"塔科的圣诞使命","typeid":"2","LapisId":"ELK-tacobrotherssavingchristmas"},{"ButtonImagePath":"qt/v2/ELK-thelab.jpg","DisplayName":"神奇实验室","typeid":"2","LapisId":"ELK-thelab"},{"ButtonImagePath":"qt/v2/ELK-wildtoro.jpg","DisplayName":"卑鄙的斗牛士","typeid":"2","LapisId":"ELK-wildtoro"},{"ButtonImagePath":"qt/v2/NLC-creepycarnival.jpg","DisplayName":"惊悚嘉年华","typeid":"2","LapisId":"NLC-creepycarnival"},{"ButtonImagePath":"qt/v2/NLC-kitchendramabbqfrenzy.jpg","DisplayName":"疯狂烧烤趴","typeid":"2","LapisId":"NLC-kitchendramabbqfrenzy"},{"ButtonImagePath":"qt/v2/NLC-oktoberfest.jpg","DisplayName":"慕尼黑啤酒节","typeid":"2","LapisId":"NLC-oktoberfest"},{"ButtonImagePath":"qt/v2/NLC-spacearcade.jpg","DisplayName":"太空游乐场","typeid":"2","LapisId":"NLC-spacearcade"},{"ButtonImagePath":"qt/v2/NLC-sushimania.jpg","DisplayName":"寿司大餐","typeid":"2","LapisId":"NLC-sushimania"},{"ButtonImagePath":"qt/v2/NLC-wixx.jpg","DisplayName":"炫彩水晶","typeid":"2","LapisId":"NLC-wixx"},{"ButtonImagePath":"qt/v2/EVP-aceround.jpg","DisplayName":"ACE枪王","typeid":"2","LapisId":"EVP-aceround"},{"ButtonImagePath":"qt/v2/EVP-atlantis.jpg","DisplayName":"亚特兰蒂斯","typeid":"2","LapisId":"EVP-atlantis"},{"ButtonImagePath":"qt/v2/EVP-basketball.jpg","DisplayName":"激情篮球","typeid":"2","LapisId":"EVP-basketball"},{"ButtonImagePath":"qt/v2/EVP-chinesenewyear.jpg","DisplayName":"新春大吉","typeid":"2","LapisId":"EVP-chinesenewyear"},{"ButtonImagePath":"qt/v2/EVP-clashofpirates.jpg","DisplayName":"海盗之战","typeid":"2","LapisId":"EVP-clashofpirates"},{"ButtonImagePath":"qt/v2/EVP-egyptgods.jpg","DisplayName":"埃及众神","typeid":"2","LapisId":"EVP-egyptgods"},{"ButtonImagePath":"qt/v2/EVP-elvenprincesses.jpg","DisplayName":"精灵公主","typeid":"2","LapisId":"EVP-elvenprincesses"},{"ButtonImagePath":"qt/v2/EVP-epicgladiators.jpg","DisplayName":"角斗士大战僵尸","typeid":"2","LapisId":"EVP-epicgladiators"},{"ButtonImagePath":"qt/v2/EVP-football.jpg","DisplayName":"足球宝贝","typeid":"2","LapisId":"EVP-football"},{"ButtonImagePath":"qt/v2/EVP-fruitburst.jpg","DisplayName":"水果爆裂","typeid":"2","LapisId":"EVP-fruitburst"},{"ButtonImagePath":"qt/v2/EVP-greatwall.jpg","DisplayName":"长城探宝","typeid":"2","LapisId":"EVP-greatwall"},{"ButtonImagePath":"qt/v2/EVP-indianasquest.jpg","DisplayName":"印第安纳的任务","typeid":"2","LapisId":"EVP-indianasquest"},{"ButtonImagePath":"qt/v2/EVP-jewellerystore.jpg","DisplayName":"珠宝店","typeid":"2","LapisId":"EVP-jewellerystore"},{"ButtonImagePath":"qt/v2/EVP-journeytothewest.jpg","DisplayName":"西游记","typeid":"2","LapisId":"EVP-journeytothewest"},{"ButtonImagePath":"qt/v2/EVP-legendofra.jpg","DisplayName":"太阳神传说","typeid":"2","LapisId":"EVP-legendofra"},{"ButtonImagePath":"qt/v2/EVP-luckymahjongbox.jpg","DisplayName":"神秘麻将盒","typeid":"2","LapisId":"EVP-luckymahjongbox"},{"ButtonImagePath":"qt/v2/EVP-naughtygirlscabaret.jpg","DisplayName":"顽皮女孩歌舞秀","typeid":"2","LapisId":"EVP-naughtygirlscabaret"},{"ButtonImagePath":"qt/v2/EVP-redcliff.jpg","DisplayName":"赤壁之战","typeid":"2","LapisId":"EVP-redcliff"},{"ButtonImagePath":"qt/v2/EVP-robinhood.jpg","DisplayName":"罗宾汉","typeid":"2","LapisId":"EVP-robinhood"},{"ButtonImagePath":"qt/v2/EVP-talismansoffortune.jpg","DisplayName":"转运护身符","typeid":"2","LapisId":"EVP-talismansoffortune"},{"ButtonImagePath":"qt/v2/EVP-theemperorstomb.jpg","DisplayName":"皇帝陵墓","typeid":"2","LapisId":"EVP-theemperorstomb"},{"ButtonImagePath":"qt/v2/EVP-thelegendofshaolin.jpg","DisplayName":"少林传奇","typeid":"2","LapisId":"EVP-thelegendofshaolin"},{"ButtonImagePath":"qt/v2/EVP-vegasnights.jpg","DisplayName":"拉斯维加斯之夜","typeid":"2","LapisId":"EVP-vegasnights"},{"ButtonImagePath":"qt/v2/HAB-3handblackjack.jpg","DisplayName":"三手黑杰克","typeid":"3","LapisId":"HAB-3handblackjack"},{"ButtonImagePath":"qt/v2/HAB-3handblackjackdoubleexposure.jpg","DisplayName":"三手黑杰克双重曝光","typeid":"3","LapisId":"HAB-3handblackjackdoubleexposure"},{"ButtonImagePath":"qt/v2/HAB-baccarat.jpg","DisplayName":"美国百家乐","typeid":"3","LapisId":"HAB-baccarat"},{"ButtonImagePath":"qt/v2/HAB-baccaratzerocommission.jpg","DisplayName":"免佣百家乐","typeid":"3","LapisId":"HAB-baccaratzerocommission"},{"ButtonImagePath":"qt/v2/HAB-caribbeanholdem.jpg","DisplayName":"赌场德州扑克","typeid":"3","LapisId":"HAB-caribbeanholdem"},{"ButtonImagePath":"qt/v2/HAB-caribbeanstud.jpg","DisplayName":"加勒比扑克","typeid":"3","LapisId":"HAB-caribbeanstud"},{"ButtonImagePath":"qt/v2/HAB-europeanroulette.jpg","DisplayName":"欧洲轮盘","typeid":"3","LapisId":"HAB-europeanroulette"},{"ButtonImagePath":"qt/v2/HAB-sicbo.jpg","DisplayName":"骰宝","typeid":"3","LapisId":"HAB-sicbo"},{"ButtonImagePath":"qt/v2/HAB-threecardpoker.jpg","DisplayName":"三张牌扑克","typeid":"3","LapisId":"HAB-threecardpoker"},{"ButtonImagePath":"qt/v2/HAB-war.jpg","DisplayName":"战争","typeid":"3","LapisId":"HAB-war"},{"ButtonImagePath":"qt/v2/OGS-blackjackpromontecarlomultihand.jpg","DisplayName":"蒙特卡洛多手21点","typeid":"3","LapisId":"OGS-blackjackpromontecarlomultihand"},{"ButtonImagePath":"qt/v2/OGS-blackjackpromontecarlosinglehand.jpg","DisplayName":"蒙特卡洛单手21点","typeid":"3","LapisId":"OGS-blackjackpromontecarlosinglehand"},{"ButtonImagePath":"qt/v2/OGS-doublezeroroulette.jpg","DisplayName":"双重美式轮盘","typeid":"3","LapisId":"OGS-doublezeroroulette"},{"ButtonImagePath":"qt/v2/OGS-roulettemaster.jpg","DisplayName":"轮盘大师","typeid":"3","LapisId":"OGS-roulettemaster"},{"ButtonImagePath":"qt/v2/OGS-allamerican.jpg","DisplayName":"美国派","typeid":"3","LapisId":"OGS-allamerican"},{"ButtonImagePath":"qt/v2/OGS-blackjack.jpg","DisplayName":"二十一点","typeid":"3","LapisId":"OGS-blackjack"},{"ButtonImagePath":"qt/v2/OGS-deuceswild.jpg","DisplayName":"百搭二王","typeid":"3","LapisId":"OGS-deuceswild"},{"ButtonImagePath":"qt/v2/OGS-europeanroulette.jpg","DisplayName":"欧式轮盘","typeid":"3","LapisId":"OGS-europeanroulette"},{"ButtonImagePath":"qt/v2/OGS-jacksorbetter.jpg","DisplayName":"杰克扑克","typeid":"3","LapisId":"OGS-jacksorbetter"},{"ButtonImagePath":"qt/v2/OGS-jokerpoker.jpg","DisplayName":"小丑扑克","typeid":"3","LapisId":"OGS-jokerpoker"},{"ButtonImagePath":"qt/v2/OGS-nocommissionbaccarat.jpg","DisplayName":"无佣百家乐","typeid":"3","LapisId":"OGS-nocommissionbaccarat"},{"ButtonImagePath":"qt/v2/OGS-rouletteamerican.jpg","DisplayName":"美式轮盘","typeid":"3","LapisId":"OGS-rouletteamerican"},{"ButtonImagePath":"qt/v2/OGS-sidebetblackjack.jpg","DisplayName":"单投21点","typeid":"3","LapisId":"OGS-sidebetblackjack"}];

var bydata=[
	{"DisplayName":"捕鱼","ButtonImagePath":"by/RTG-fishcatch.png","LapisId": "RTG-fishcatch","typeid":"qt"},
	{"DisplayName":"水浒劈鱼","ButtonImagePath":"by/AT06.png","LapisId": "AT06","typeid":"cq9"},
	{"DisplayName":"欢乐捕鱼","ButtonImagePath":"by/AT05.png","LapisId": "AT05","typeid":"cq9"},
	{"DisplayName":"雷电战机","ButtonImagePath":"by/AT04.png","LapisId": "AT04","typeid":"cq9"},
	{"DisplayName":"一炮捕鱼","ButtonImagePath":"by/AT01.png","LapisId": "AT01","typeid":"cq9"},
	{"DisplayName":"皇金渔场","ButtonImagePath":"by/AB3_cn_1.png","LapisId": "AB3","typeid":"cq9"},
//    {"ButtonImagePath":"by/65_cn.png","DisplayName" : "黄金渔场","typeid" :'cq9',"LapisId":"AB1"},
	{"DisplayName":"捕鱼达人","ButtonImagePath":"by/30599.jpg","LapisId": "30599","typeid":"bbin"},
	{"DisplayName":"捕鱼达人2","ButtonImagePath":"by/30598.png","LapisId": "30598","typeid":"bbin"},
	{"DisplayName":"捕鱼大师","ButtonImagePath":"by/38001.jpg","LapisId": "38001","typeid":"bbin"},
	{"DisplayName":"富贵渔场","ButtonImagePath":"by/38002.jpg","LapisId": "38002","typeid":"bbin"},
	{"DisplayName":"龙王捕鱼","ButtonImagePath":"by/7001_250x203_cn.jpg","LapisId": "7001","typeid":"jdb"},
	{"DisplayName":"龙王捕鱼2","ButtonImagePath":"by/7002_250x203_cn.jpg","LapisId": "7002","typeid":"jdb"},
	{"DisplayName":"财神捕鱼","ButtonImagePath":"by/7003_250x203_cn.jpg","LapisId": "7003","typeid":"jdb"},
	{"DisplayName":"五龙捕鱼","ButtonImagePath":"by/7004_250x203_cn.jpg","LapisId": "7004","typeid":"jdb"},
	{"DisplayName":"捕鱼一路发","ButtonImagePath":"by/7005_250x203_cn.jpg","LapisId": "7005","typeid":"jdb"},
	{"DisplayName":"捕鱼王2D","ButtonImagePath":"by/HM2D.jpg","LapisId": "HM2D","typeid":"ag"},
	{"DisplayName":"捕鱼王3D","ButtonImagePath":"by/HM3D.png","LapisId": "HM3D","typeid":"ag"},
	{"DisplayName":"捕鱼乐园","ButtonImagePath":"by/HMFP.jpg","LapisId": "HMFP","typeid":"ag"},
	{"DisplayName":"捕鱼王者","ButtonImagePath":"by/SB36.gif","LapisId": "6","typeid":"ag"},
	{"DisplayName":"捕鸟联盟","ButtonImagePath":"by/SMF_BirdHunting.png","LapisId": "SMF_BirdHunting","typeid":"mg"},
	{"DisplayName":"捕鱼联盟","ButtonImagePath":"by/SMF_FishingJoy.png","LapisId": "SMF_FishingJoy","typeid":"mg"},
	{"DisplayName":"万达福娃捕鱼","ButtonImagePath":"by/SFG_WDFuWaFishing.png","LapisId": "SFG_WDFuWaFishing","typeid":"mg"},
	{"DisplayName":"深海大战","ButtonImagePath":"by/OceanRuler.png","LapisId": "pop_sw_or_skw","typeid":"pt"},
	{"DisplayName":"财神大海","ButtonImagePath":"by/Cai_Shen_Da_Hai.png","LapisId": "pop_a3250b6e_skw","typeid":"pt"},
	{"DisplayName":"好运农场","ButtonImagePath":"by/Fu_Farm.png","LapisId": "pop_sw_fufarm_skw","typeid":"pt"},
	{"DisplayName":"捕鱼多福","ButtonImagePath":"by/Fu_Fish.png","LapisId": "pop_swfufishintw_skw","typeid":"pt"},
	{"DisplayName":"捕鱼多福奖池","ButtonImagePath":"by/Fu_Fish_Jackpot.png","LapisId": "pop_swfufishjp_skw","typeid":"pt"},
    {"ButtonImagePath":"by/cashfi.jpg","DisplayName":"深海大赢家","typeid":"pt","LapisId":"cashfi"}
	];

// AG  神奇宝石、金拉霸、猛龙传奇、金龙珠
// MG   冰球突破、舞龙、幸运双星、宝石之轮
// CQ9 五福临门、CQ雷神、CQ9跳高高、鸿福齐天
// JDB 开运夺宝、飞鸟派对、金鸡福彩、财神宾果、东方神兽、变脸
// BBIN  连环夺宝、连环夺宝2、糖果派对、糖果派对2、糖果派对3
var allhotdata=[
	{
		"DisplayName" : "神奇宝石",
		"DisplayEnName" : "Mystic Gems",
		"category" : "老虎机",
		"ButtonImagePath" : "SB47.jpg",
		"LapisId" : "158",
		"typeid" : "ag"
	},{
		"DisplayName" : "金拉霸",
		"DisplayEnName" : "Gold Class",
		"category" : "老虎机",
		"ButtonImagePath" : "SC03.jpg",
		"LapisId" : "802",
		"typeid" : "ag"
	},{
		"DisplayName" : "猛龙传奇",
		"DisplayEnName" : "Legend of the Dragon",
		"category" : "老虎机",
		"ButtonImagePath" : "SB45.jpg",
		"LapisId" : "156",
		"typeid" : "ag"
	},{
		"DisplayName" : "金龙珠",
		"DisplayEnName" : "The Dragon Pearl Gold",
		"category" : "老虎机",
		"ButtonImagePath" : "SB49.jpg",
		"LapisId" : "160",
		"typeid" : "ag"
	},{
		"DisplayName": "幸运双星",
		"DisplayEnName": "Lucky Twins",
		"ButtonImagePath": "BTN_luckyTwins_ZH.png",
		"LapisId": "SMG_luckyTwins",
		"typeid":"mg"
	},{
		"ButtonImagePath" : "10_cn.png",
		"DisplayName" : "五福临门",
		"typeid" : 1,
		"LapisId" : "10",
		"typeid":"cq9"
	},{
		"ButtonImagePath" : "52_cn.png",
		"DisplayName" : "跳高高",
		"typeid" : 1,
		"LapisId" : "52",
		"typeid" : "cq9"
	},{
		"ButtonImagePath":"WEB_Icon14033_250x203_cn.jpg",
		"DisplayName":"飞鸟派对",
		"typeid":"0",
		"LapisId":"14033",
		"typeid" : "jdb"
	},{"ButtonImagePath":"WEB_Icon14029_250x203_cn.jpg",
		"DisplayName":"东方神兽",
		"typeid":"0",
		"LapisId":"14029",
		"typeid" : "jdb"
	},{"ButtonImagePath":"WEB_Icon8003_250x203_cn.jpg",
		"DisplayName":"变脸",
		"typeid":"0",
		"LapisId":"8003",
		"typeid" : "jdb"
	}
];


var cq9data = [{
	"ButtonImagePath" : "186_cn.png",
	"DisplayName" : "火之女王2",
	"typeid" : 1,
	"LapisId" : "186"
},{
	"ButtonImagePath" : "BU02_cn.png",
	"DisplayName" : "野蛮世界",
	"typeid" : 1,
	"LapisId" : "BU02"
},{
	"ButtonImagePath" : "BU05_cn.png",
	"DisplayName" : "武媚娘",
	"typeid" : 1,
	"LapisId" : "BU05"
},{
	"ButtonImagePath" : "185_cn.png",
	"DisplayName" : "龙珠",
	"typeid" : 1,
	"LapisId" : "185"
},{
	"ButtonImagePath" : "BT02_cn.png",
	"DisplayName" : "抢庄骰子牛牛",
	"typeid" : 1,
	"LapisId" : "BT02"
},{
	"ButtonImagePath" : "52_cn.png",
	"DisplayName" : "跳高高",
	"typeid" : 1,
	"LapisId" : "52"
}, {
	"ButtonImagePath" : "7_cn.png",
	"DisplayName" : "跳起来",
	"typeid" : 1,
	"LapisId" : "7"
},{
	"ButtonImagePath" : "179_cn.png",
	"DisplayName" : "跳高高2",
	"typeid" : 1,
	"LapisId" : "179"
}, {
	"ButtonImagePath" : "99_cn.png",
	"DisplayName" : "跳更高",
	"typeid" : 1,
	"LapisId" : "99"
},  {
	"ButtonImagePath" : "24_cn.png",
	"DisplayName" : "跳起来2",
	"typeid" : 1,
	"LapisId" : "24"
},{
	"ButtonImagePath" : "10_cn.png",
	"DisplayName" : "五福临门",
	"typeid" : 1,
	"LapisId" : "10"
},{
	"ButtonImagePath" : "111_cn.png",
	"DisplayName" : "飞起来",
	"typeid" : 1,
	"LapisId" : "111"
},{
	"ButtonImagePath" : "31_cn.png",
	"DisplayName" : "武圣",
	"typeid" : 1,
	"LapisId" : "31"
},{
	"ButtonImagePath" : "50_cn.png",
	"DisplayName" : "鸿福齐天",
	"typeid" : 1,
	"LapisId" : "50"
},{
	"ButtonImagePath" : "64_cn.png",
	"DisplayName" : "宙斯",
	"typeid" : 1,
	"LapisId" : "64"
},{
	"ButtonImagePath" : "AB3_cn.png",
	"DisplayName" : "皇金渔场",
	"typeid" : 1,
	"LapisId" : "AB3"
},{
	"ButtonImagePath" : "105_cn.png",
	"DisplayName" : "单手跳高高",
	"typeid" : 1,
	"LapisId" : "105"
},{
	"ButtonImagePath" : "205_cn.png",
	"DisplayName" : "蹦迪",
	"typeid" : 1,
	"LapisId" : "205"
},{
	"ButtonImagePath" : "AT01_cn.png",
	"DisplayName" : "一炮捕鱼",
	"typeid" : 1,
	"LapisId" : "AT01"
},{
	"ButtonImagePath" : "138_cn.png",
	"DisplayName" : "跳过来",
	"typeid" : 1,
	"LapisId" : "138"
},{
	"ButtonImagePath" : "69_cn.png",
	"DisplayName" : "发财神",
	"typeid" : 1,
	"LapisId" : "69"
},{
	"ButtonImagePath" : "75_cn.png",
	"DisplayName" : "发财神2",
	"typeid" : 1,
	"LapisId" : "160"
},{
	"ButtonImagePath" : "83huo_cn.png",
	"DisplayName" : "火之女王",
	"typeid" : 1,
	"LapisId" : "83"
},{
	"ButtonImagePath" : "89_cn.png",
	"DisplayName" : "雷神",
	"typeid" : 1,
	"LapisId" : "89"
},{
	"ButtonImagePath" : "153_cn.png",
	"DisplayName" : "六颗糖",
	"typeid" : 1,
	"LapisId" : "153"
},{
	"ButtonImagePath" : "183_cn.png",
	"DisplayName" : "野狼Disco",
	"typeid" : 1,
	"LapisId" : "183"
},{
	"ButtonImagePath" : "140_cn.png",
	"DisplayName" : "火烧连环船2",
	"typeid" : 1,
	"LapisId" : "140"
},{
	"ButtonImagePath" : "117_cn.png",
	"DisplayName" : "东方神起",
	"typeid" : 1,
	"LapisId" : "117"
},{
	"ButtonImagePath" : "9_cn.png",
	"DisplayName" : "钟馗运财",
	"typeid" : 1,
	"LapisId" : "9"
}, {
	"ButtonImagePath" : "15_cn.png",
	"DisplayName" : "金鸡报喜",
	"typeid" : 1,
	"LapisId" : "15"
}, {
	"ButtonImagePath" : "188_cn.png",
	"DisplayName" : "板球狂热",
	"typeid" : 1,
	"LapisId" : "188"
}, {
	"ButtonImagePath" : "79b_cn.png",
	"DisplayName" : "变色龙",
	"typeid" : 1,
	"LapisId" : "79"
},{
	"ButtonImagePath" : "115_cn.png",
	"DisplayName" : "冰雪女王",
	"typeid" : 1,
	"LapisId" : "115"
}, {
	"ButtonImagePath" : "29_cn.png",
	"DisplayName" : "水世界",
	"typeid" : 1,
	"LapisId" : "29"
}, {
	"ButtonImagePath" : "113_cn.png",
	"DisplayName" : "飞天财神",
	"typeid" : 1,
	"LapisId" : "113"
}, {
	"ButtonImagePath" : "8_cn.png",
	"DisplayName" : "甜蜜蜜",
	"typeid" : 1,
	"LapisId" : "8"
}, {
	"ButtonImagePath" : "203_cn.png",
	"DisplayName" : "嗨起來",
	"typeid" : 1,
	"LapisId" : "203"
},{
	"ButtonImagePath" : "109_cn.png",
	"DisplayName" : "单手跳起來",
	"typeid" : 1,
	"LapisId" : "109"
},{
	"ButtonImagePath" : "71_cn.png",
	"DisplayName" : "宙斯他爹",
	"typeid" : 1,
	"LapisId" : "154"
},{
	"ButtonImagePath" : "108_cn.png",
	"DisplayName" : "直式跳更高",
	"typeid" : 1,
	"LapisId" : "108"
},{
	"ButtonImagePath" : "74j_cn.png",
	"DisplayName" : "聚宝盆",
	"typeid" : 1,
	"LapisId" : "74"
},{
	"ButtonImagePath" : "173_cn.png",
	"DisplayName" : "666",
	"typeid" : 1,
	"LapisId" : "173"
},{
	"ButtonImagePath" : "58_cn.png",
	"DisplayName" : "金鸡报囍2",
	"typeid" : 1,
	"LapisId" : "58"
},{
	"ButtonImagePath" : "33_cn.png",
	"DisplayName" : "火烧连环船",
	"typeid" : 1,
	"LapisId" : "33"
},{
	"ButtonImagePath" : "79_cn.png",
	"DisplayName" : "大力神",
	"typeid" : 1,
	"LapisId" : "161"
},{
	"ButtonImagePath" : "182_cn.png",
	"DisplayName" : "雷神2",
	"typeid" : 1,
	"LapisId" : "182"
},{
	"ButtonImagePath" : "147_cn.png",
	"DisplayName" : "花开富贵",
	"typeid" : 1,
	"LapisId" : "147"
},{
	"ButtonImagePath" : "152_cn.png",
	"DisplayName" : "双飞",
	"typeid" : 1,
	"LapisId" : "152"
},{
	"ButtonImagePath" : "60_cn.png",
	"DisplayName" : "丛林舞会",
	"typeid" : 1,
	"LapisId" : "60"
},{
	"ButtonImagePath" : "142_cn.png",
	"DisplayName" : "火神",
	"typeid" : 1,
	"LapisId" : "142"
},{
	"ButtonImagePath" : "72h_cn.png",
	"DisplayName" : "好运年年",
	"typeid" : 1,
	"LapisId" : "72"
},{
	"ButtonImagePath" : "201_cn.png",
	"DisplayName" : "拳霸",
	"typeid" : 1,
	"LapisId" : "201"
},{
	"ButtonImagePath" : "16_cn.png",
	"DisplayName" : "五行",
	"typeid" : 1,
	"LapisId" : "16"
},{
	"ButtonImagePath" : "1_cn.png",
	"DisplayName" : "钻石水果王",
	"typeid" : 1,
	"LapisId" : "1"
},{
	"ButtonImagePath" : "187_cn.png",
	"DisplayName" : "咏春",
	"typeid" : 1,
	"LapisId" : "187"
},{
	"ButtonImagePath" : "12_cn.png",
	"DisplayName" : "金玉满堂",
	"typeid" : 1,
	"LapisId" : "12"
},{
	"ButtonImagePath" : "BU01_cn.png",
	"DisplayName" : "水浒传",
	"typeid" : 1,
	"LapisId" : "BU01"
},{
	"ButtonImagePath" : "143_cn.png",
	"DisplayName" : "发财福娃",
	"typeid" : 1,
	"LapisId" : "143"
},{
	"ButtonImagePath" : "116_cn.png",
	"DisplayName" : "梦游仙境",
	"typeid" : 1,
	"LapisId" : "116"
},{
	"ButtonImagePath" : "5_cn.png",
	"DisplayName" : "金大款",
	"typeid" : 1,
	"LapisId" : "5"
},{
	"ButtonImagePath" : "128_cn.png",
	"DisplayName" : "转大钱",
	"typeid" : 1,
	"LapisId" : "128"
},{
	"ButtonImagePath" : "78_cn.png",
	"DisplayName" : "哪吒再临",
	"typeid" : 1,
	"LapisId" : "163"
},{
	"ButtonImagePath" : "202_cn.png",
	"DisplayName" : "舞媚娘",
	"typeid" : 1,
	"LapisId" : "202"
},{
	"ButtonImagePath" : "124_cn.png",
	"DisplayName" : "锁象无敌",
	"typeid" : 1,
	"LapisId" : "124"
},{
	"ButtonImagePath" : "157_cn.png",
	"DisplayName" : "五形拳",
	"typeid" : 1,
	"LapisId" : "157"
},{
	"ButtonImagePath" : "4_cn.png",
	"DisplayName" : "森林泰后",
	"typeid" : 1,
	"LapisId" : "4"
},{
	"ButtonImagePath" : "78a_cn.png",
	"DisplayName" : "阿波罗",
	"typeid" : 1,
	"LapisId" : "78"
},{
	"ButtonImagePath" : "3_cn.png",
	"DisplayName" : "血之吻",
	"typeid" : 1,
	"LapisId" : "3"
},{
	"ButtonImagePath" : "AT05_cn.png",
	"DisplayName" : "欢乐捕鱼",
	"typeid" : 1,
	"LapisId" : "AT05"
},{
	"ButtonImagePath" : "67_cn.png",
	"DisplayName" : "赚金蛋",
	"typeid" : 1,
	"LapisId" : "67"
}, {
	"ButtonImagePath" : "46_cn.png",
	"DisplayName" : "狼月",
	"typeid" : 1,
	"LapisId" : "46"
},{
	"ButtonImagePath" : "57_cn.png",
	"DisplayName" : "神兽争霸",
	"typeid" : 1,
	"LapisId" : "57"
},{
	"ButtonImagePath" : "23_cn.png",
	"DisplayName" : "金元宝",
	"typeid" : 1,
	"LapisId" : "23"
},{
	"ButtonImagePath" : "150_cn.png",
	"DisplayName" : "寿星大发",
	"typeid" : 1,
	"LapisId" : "150"
},{
	"ButtonImagePath" : "54_cn.png",
	"DisplayName" : "火草泥马",
	"typeid" : 1,
	"LapisId" : "54"
},{
	"ButtonImagePath" : "17_cn.png",
	"DisplayName" : "祥狮献瑞",
	"typeid" : 1,
	"LapisId" : "17"
},{
	"ButtonImagePath" : "148_cn.png",
	"DisplayName" : "有如神柱",
	"typeid" : 1,
	"LapisId" : "148"
},{
	"ButtonImagePath" : "1074_cn.png",
	"DisplayName" : "聚宝盆JP",
	"typeid" : 1,
	"LapisId" : "1074"
},{
	"ButtonImagePath" : "19_cn.png",
	"DisplayName" : "风火轮",
	"typeid" : 1,
	"LapisId" : "19"
},{
	"ButtonImagePath" : "68_cn.png",
	"DisplayName" : "悟空偷桃",
	"typeid" : 1,
	"LapisId" : "68"
},{
	"ButtonImagePath" : "118_cn.png",
	"DisplayName" : "老司机",
	"typeid" : 1,
	"LapisId" : "118"
},{
	"ButtonImagePath" : "81j_cn.png",
	"DisplayName" : "金银岛",
	"typeid" : 1,
	"LapisId" : "81"
},{
	"ButtonImagePath" : "55_cn.png",
	"DisplayName" : "魔龙传奇",
	"typeid" : 1,
	"LapisId" : "55"
},{
	"ButtonImagePath" : "114_cn.png",
	"DisplayName" : "钻饱宝",
	"typeid" : 1,
	"LapisId" : "114"
},{
	"ButtonImagePath" : "112_cn.png",
	"DisplayName" : "盗法老墓",
	"typeid" : 1,
	"LapisId" : "112"
},{
	"ButtonImagePath" : "136_cn.png",
	"DisplayName" : "奔跑吧猛兽",
	"typeid" : 1,
	"LapisId" : "136"
},{
	"ButtonImagePath" : "122_cn.png",
	"DisplayName" : "印加祖玛",
	"typeid" : 1,
	"LapisId" : "122"
},{
	"ButtonImagePath" : "39_cn.png",
	"DisplayName" : "飞天",
	"typeid" : 1,
	"LapisId" : "39"
},{
	"ButtonImagePath" : "134_cn.png",
	"DisplayName" : "家里有矿",
	"typeid" : 1,
	"LapisId" : "134"
},{
	"ButtonImagePath" : "77h_cn.png",
	"DisplayName" : "火凤凰",
	"typeid" : 1,
	"LapisId" : "77"
},{
	"ButtonImagePath" : "146_cn.png",
	"DisplayName" : "九连宝灯",
	"typeid" : 1,
	"LapisId" : "146"
},{
	"ButtonImagePath" : "145_cn.png",
	"DisplayName" : "印金工厂",
	"typeid" : 1,
	"LapisId" : "145"
},{
	"ButtonImagePath" : "20_cn.png",
	"DisplayName" : "发发发",
	"typeid" : 1,
	"LapisId" : "20"
},{
	"ButtonImagePath" : "149_cn.png",
	"DisplayName" : "龙舟",
	"typeid" : 1,
	"LapisId" : "149"
},{
	"ButtonImagePath" : "51_cn.png",
	"DisplayName" : "嗨爆大马戏",
	"typeid" : 1,
	"LapisId" : "51"
},{
	"ButtonImagePath" : "63_cn.png",
	"DisplayName" : "寻龙诀",
	"typeid" : 1,
	"LapisId" : "63"
} ,{
	"ButtonImagePath" : "144_cn.png",
	"DisplayName" : "钻更多",
	"typeid" : 1,
	"LapisId" : "144"
} ,{
	"ButtonImagePath" : "70_cn.png",
	"DisplayName" : "万饱龙",
	"typeid" : 1,
	"LapisId" : "70"
},{
	"ButtonImagePath" : "104_cn.png",
	"DisplayName" : "海滨消消乐",
	"typeid" : 1,
	"LapisId" : "104"
},{
	"ButtonImagePath" : "42_cn.png",
	"DisplayName" : "福尔摩斯",
	"typeid" : 1,
	"LapisId" : "42"
},{
	"ButtonImagePath" : "2_cn.png",
	"DisplayName" : "棋圣",
	"typeid" : 1,
	"LapisId" : "2"
},{
	"ButtonImagePath" : "86n_cn.png",
	"DisplayName" : "牛逼快跑",
	"typeid" : 1,
	"LapisId" : "86"
},{
	"ButtonImagePath" : "184_cn.png",
	"DisplayName" : "六颗扭蛋",
	"typeid" : 1,
	"LapisId" : "184"
},{
	"ButtonImagePath" : "103_cn.png",
	"DisplayName" : "宝石配对",
	"typeid" : 1,
	"LapisId" : "103"
},{
	"ButtonImagePath" : "44_cn.png",
	"DisplayName" : "豪华水果王",
	"typeid" : 1,
	"LapisId" : "44"
},{
	"ButtonImagePath" : "26_cn.png",
	"DisplayName" : "777",
	"typeid" : 1,
	"LapisId" : "26"
},{
	"ButtonImagePath" : "88_cn.png",
	"DisplayName" : "金喜鹊桥",
	"typeid" : 1,
	"LapisId" : "88"
},{
	"ButtonImagePath" : "38_cn.png",
	"DisplayName" : "舞力全开",
	"typeid" : 1,
	"LapisId" : "38"
},{
	"ButtonImagePath" : "93_cn.png",
	"DisplayName" : "世界杯明星",
	"typeid" : 1,
	"LapisId" : "93"
},{
	"ButtonImagePath" : "66_cn.png",
	"DisplayName" : "火爆777",
	"typeid" : 1,
	"LapisId" : "66"
}, {
	"ButtonImagePath" : "221_cn.png",
	"DisplayName" : "狄仁杰四大天王",
	"typeid" : 1,
	"LapisId" : "221"
}, {
	"ButtonImagePath" : "130_cn.png",
	"DisplayName" : "偷金妹子",
	"typeid" : 1,
	"LapisId" : "130"
},{
	"ButtonImagePath" : "AR02_cn.png",
	"DisplayName" : "喵财进宝",
	"typeid" : 1,
	"LapisId" : "AR02"
},{
	"ButtonImagePath" : "AR39_cn.png",
	"DisplayName" : "僵尸的宝藏",
	"typeid" : 1,
	"LapisId" : "AR39"
},{
	"ButtonImagePath" : "102_cn.png",
	"DisplayName" : "水果派对",
	"typeid" : 1,
	"LapisId" : "102"
},{
	"ButtonImagePath" : "87_cn.png",
	"DisplayName" : "囍",
	"typeid" : 1,
	"LapisId" : "AR14"
},{
	"ButtonImagePath" : "86_cn.png",
	"DisplayName" : "贪吃猫",
	"typeid" : 1,
	"LapisId" : "AR28"
},{
	"ButtonImagePath" : "85_cn.png",
	"DisplayName" : "隔壁王二不曾偷",
	"typeid" : 1,
	"LapisId" : "AR09"
},{
	"ButtonImagePath" : "84_cn.png",
	"DisplayName" : "妈祖传奇",
	"typeid" : 1,
	"LapisId" : "AR22"
},{
	"ButtonImagePath" : "83_cn.png",
	"DisplayName" : "生肖传奇",
	"typeid" : 1,
	"LapisId" : "AR26"
},{
	"ButtonImagePath" : "82_cn.png",
	"DisplayName" : "财富幽灵",
	"typeid" : 1,
	"LapisId" : "AS04"
},{
	"ButtonImagePath" : "81_cn.png",
	"DisplayName" : "五运",
	"typeid" : 1,
	"LapisId" : "AR23"
},{
	"ButtonImagePath" : "80_cn.png",
	"DisplayName" : "八仙传奇",
	"typeid" : 1,
	"LapisId" : "AR25"
},{
	"ButtonImagePath" : "77_cn.png",
	"DisplayName" : "8级台风",
	"typeid" : 1,
	"LapisId" : "AR12"
},{
	"ButtonImagePath" : "76_cn.png",
	"DisplayName" : "守株待兔",
	"typeid" : 1,
	"LapisId" : "AR16"
},{
	"ButtonImagePath" : "74_cn.png",
	"DisplayName" : "马戏团连连发",
	"typeid" : 1,
	"LapisId" : "AR17"
},{
	"ButtonImagePath" : "73_cn.png",
	"DisplayName" : "疯狂马戏团",
	"typeid" : 1,
	"LapisId" : "AS02"
},{
	"ButtonImagePath" : "50_cn.png",
	"DisplayName" : "鸿福齐天",
	"typeid" : 1,
	"LapisId" : "50"
},{
	"ButtonImagePath" : "49_cn.png",
	"DisplayName" : "寂寞星球",
	"typeid" : 1,
	"LapisId" : "49"
}, {
	"ButtonImagePath" : "59_cn.png",
	"DisplayName" : "夏日猩情",
	"typeid" : 1,
	"LapisId" : "59"
},{
	"ButtonImagePath" : "32_cn.png",
	"DisplayName" : "通天神探狄仁杰",
	"typeid" : 1,
	"LapisId" : "32"
}, {
	"ButtonImagePath" : "36_cn.png",
	"DisplayName" : "夜店大亨",
	"typeid" : 1,
	"LapisId" : "36"
},{
	"ButtonImagePath" : "27_cn.png",
	"DisplayName" : "魔法世界",
	"typeid" : 1,
	"LapisId" : "27"
},{
	"ButtonImagePath" : "34_cn.png",
	"DisplayName" : "地鼠战役",
	"typeid" : 1,
	"LapisId" : "34"
}, {
	"ButtonImagePath" : "13_cn.png",
	"DisplayName" : "樱花妹子",
	"typeid" : 1,
	"LapisId" : "13"
},{
	"ButtonImagePath" : "22_cn.png",
	"DisplayName" : "庶务西游二课",
	"typeid" : 1,
	"LapisId" : "22"
},{
	"ButtonImagePath" : "47_cn.png",
	"DisplayName" : "法老宝藏",
	"typeid" : 1,
	"LapisId" : "47"
}, {
	"ButtonImagePath" : "21_cn.png",
	"DisplayName" : "野狼传说",
	"typeid" : 1,
	"LapisId" : "21"
}];

var isbdata=[{"ButtonImagePath":"904180.jpg","DisplayName":"超级幸运转轮","typeid":"1","LapisId":"904180"},{"ButtonImagePath":"909225.jpg","DisplayName":"小丑叠叠乐","typeid":"1","LapisId":"909225"},{"ButtonImagePath":"881573.jpg","DisplayName":"生命之最","typeid":"1","LapisId":"881573"},{"ButtonImagePath":"881604.jpg","DisplayName":"热辣超速","typeid":"1","LapisId":"881604"},{"ButtonImagePath":"882033.jpg","DisplayName":"4X10对高手","typeid":"3","LapisId":"882033"},{"ButtonImagePath":"904436.jpg","DisplayName":"自动超级旋转","typeid":"1","LapisId":"904436"},{"ButtonImagePath":"882026.jpg","DisplayName":"100X游戏","typeid":"3","LapisId":"882026"},{"ButtonImagePath":"881525.jpg","DisplayName":"爱丽丝的冒险","typeid":"1","LapisId":"881525"},{"ButtonImagePath":"881589.jpg","DisplayName":"超级男孩","typeid":"1","LapisId":"881589"},{"ButtonImagePath":"882032.jpg","DisplayName":"野蛮对决累积版","typeid":"3","LapisId":"882032"},{"ButtonImagePath":"882031.jpg","DisplayName":"10对高手累积版","typeid":"3","LapisId":"882031"},{"ButtonImagePath":"882027.jpg","DisplayName":"局末平分和小丑扑克","typeid":"3","LapisId":"882027"},{"ButtonImagePath":"881503.jpg","DisplayName":"百万分","typeid":"1","LapisId":"881503"},{"ButtonImagePath":"882009.jpg","DisplayName":"王牌扑克","typeid":"3","LapisId":"882009"},{"ButtonImagePath":"883014.jpg","DisplayName":"美式轮盘","typeid":"4","LapisId":"883014"},{"ButtonImagePath":"881527.jpg","DisplayName":"水果盒子","typeid":"1","LapisId":"881527"},{"ButtonImagePath":"904570.jpg","DisplayName":"少林旋转","typeid":"1","LapisId":"904570"},{"ButtonImagePath":"881595.jpg","DisplayName":"奢华罗马HD","typeid":"1","LapisId":"881595"},{"ButtonImagePath":"883007.jpg","DisplayName":"21点","typeid":"2","LapisId":"883007"},{"ButtonImagePath":"881550.jpg","DisplayName":"疯狂的红龙","typeid":"1","LapisId":"881550"},{"ButtonImagePath":"882021.jpg","DisplayName":"3X两点扑克","typeid":"3","LapisId":"882021"},{"ButtonImagePath":"881586.jpg","DisplayName":"海豚岛","typeid":"1","LapisId":"881586"},{"ButtonImagePath":"881501.jpg","DisplayName":"超级现金定时器","typeid":"1","LapisId":"881501"},{"ButtonImagePath":"883008.jpg","DisplayName":"梭哈","typeid":"2","LapisId":"883008"},{"ButtonImagePath":"881578.jpg","DisplayName":"气氛","typeid":"1","LapisId":"881578"},{"ButtonImagePath":"881508.jpg","DisplayName":"家有仙妻","typeid":"1","LapisId":"881508"},{"ButtonImagePath":"904573.jpg","DisplayName":"守护之眼","typeid":"1","LapisId":"904573"},{"ButtonImagePath":"900021.jpg","DisplayName":"珠宝甲壳虫","typeid":"1","LapisId":"900021"},{"ButtonImagePath":"881608.jpg","DisplayName":"樱桃三重奏","typeid":"1","LapisId":"881608"},{"ButtonImagePath":"881581.jpg","DisplayName":"最佳女巫","typeid":"1","LapisId":"881581"},{"ButtonImagePath":"883048.jpg","DisplayName":"多手21点3D","typeid":"2","LapisId":"883048"},{"ButtonImagePath":"882012.jpg","DisplayName":"小丑奖金","typeid":"3","LapisId":"882012"},{"ButtonImagePath":"882017.jpg","DisplayName":"拉斯维加斯王牌扑克","typeid":"3","LapisId":"882017"},{"ButtonImagePath":"883017.jpg","DisplayName":"欧式轮盘累积版","typeid":"4","LapisId":"883017"},{"ButtonImagePath":"883012.jpg","DisplayName":"欧式轮盘小注","typeid":"4","LapisId":"883012"},{"ButtonImagePath":"883028.jpg","DisplayName":"银色轮盘","typeid":"4","LapisId":"883028"},{"ButtonImagePath":"882008.jpg","DisplayName":"多倍扑克","typeid":"3","LapisId":"882008"},{"ButtonImagePath":"882015.jpg","DisplayName":"王牌扑克大啤","typeid":"3","LapisId":"882015"},{"ButtonImagePath":"881597.jpg","DisplayName":"彩云故事","typeid":"1","LapisId":"881597"},{"ButtonImagePath":"904500.jpg","DisplayName":"幸运三连线","typeid":"1","LapisId":"904500"},{"ButtonImagePath":"881250.jpg","DisplayName":"枪手","typeid":"1","LapisId":"881250"},{"ButtonImagePath":"882019.jpg","DisplayName":"双鬼牌","typeid":"3","LapisId":"882019"},{"ButtonImagePath":"881619.jpg","DisplayName":"小丑三重奏","typeid":"1","LapisId":"881619"},{"ButtonImagePath":"881600.jpg","DisplayName":"神奇7s老虎机","typeid":"1","LapisId":"881600"},{"ButtonImagePath":"883034.jpg","DisplayName":"里诺21点","typeid":"2","LapisId":"883034"},{"ButtonImagePath":"882022.jpg","DisplayName":"10X对决","typeid":"3","LapisId":"882022"},{"ButtonImagePath":"882005.jpg","DisplayName":"3X小丑游戏","typeid":"3","LapisId":"882005"},{"ButtonImagePath":"881565.jpg","DisplayName":"外星人的攻击","typeid":"1","LapisId":"881565"},{"ButtonImagePath":"881603.jpg","DisplayName":"森林狂欢会","typeid":"1","LapisId":"881603"},{"ButtonImagePath":"883029.jpg","DisplayName":"VIP21点","typeid":"2","LapisId":"883029"},{"ButtonImagePath":"883044.jpg","DisplayName":"奖金轮盘","typeid":"4","LapisId":"883044"},{"ButtonImagePath":"883027.jpg","DisplayName":"VIP轮盘","typeid":"4","LapisId":"883027"},{"ButtonImagePath":"882013.jpg","DisplayName":"小丑扑克","typeid":"3","LapisId":"882013"},{"ButtonImagePath":"907149.jpg","DisplayName":"双响迪斯科","typeid":"1","LapisId":"907149"},{"ButtonImagePath":"881585.jpg","DisplayName":"埃及旋转HD","typeid":"1","LapisId":"881585"},{"ButtonImagePath":"882004.jpg","DisplayName":"3X双游戏","typeid":"3","LapisId":"882004"},{"ButtonImagePath":"881511.jpg","DisplayName":"蒙娜丽莎的珠宝","typeid":"1","LapisId":"881511"},{"ButtonImagePath":"882003.jpg","DisplayName":"25X游戏","typeid":"3","LapisId":"882003"},{"ButtonImagePath":"881537.jpg","DisplayName":"幸运小妖精","typeid":"1","LapisId":"881537"},{"ButtonImagePath":"882023.jpg","DisplayName":"25X局末平分和小丑扑克","typeid":"3","LapisId":"882023"},{"ButtonImagePath":"883006.jpg","DisplayName":"百家乐","typeid":"2","LapisId":"883006"},{"ButtonImagePath":"883038.jpg","DisplayName":"娱乐场高低","typeid":"2","LapisId":"883038"},{"ButtonImagePath":"881516.png","DisplayName":"忍者大师","typeid":"1","LapisId":"881516"},{"ButtonImagePath":"882016.jpg","DisplayName":"鬼牌德州扑克","typeid":"3","LapisId":"882016"},{"ButtonImagePath":"883045.png","DisplayName":"美式百家乐","typeid":"2","LapisId":"883045"},{"ButtonImagePath":"881538.jpg","DisplayName":"奥林匹克奖池","typeid":"1","LapisId":"881538"},{"ButtonImagePath":"881611.jpg","DisplayName":"霓虹卷轴","typeid":"1","LapisId":"881611"},{"ButtonImagePath":"882007.jpg","DisplayName":"杰克高手","typeid":"3","LapisId":"882007"},{"ButtonImagePath":"882006.jpg","DisplayName":"3X游戏","typeid":"3","LapisId":"882006"},{"ButtonImagePath":"883024.jpg","DisplayName":"娱乐城德州扑克","typeid":"2","LapisId":"883024"},{"ButtonImagePath":"881592.jpg","DisplayName":"霓虹卷轴","typeid":"1","LapisId":"881592"},{"ButtonImagePath":"882002.jpg","DisplayName":"2野蛮对决","typeid":"3","LapisId":"882002"},{"ButtonImagePath":"883023.jpg","DisplayName":"大西洋21点","typeid":"2","LapisId":"883023"},{"ButtonImagePath":"891584.jpg","DisplayName":"超级现金定时器高清","typeid":"1","LapisId":"891584"},{"ButtonImagePath":"882014.jpg","DisplayName":"保龄扑克","typeid":"3","LapisId":"882014"},{"ButtonImagePath":"882034.jpg","DisplayName":"4X野蛮对决","typeid":"3","LapisId":"882034"},{"ButtonImagePath":"904177.jpg","DisplayName":"绝对超级旋转","typeid":"1","LapisId":"904177"},{"ButtonImagePath":"883021.jpg","DisplayName":"欧式轮盘","typeid":"4","LapisId":"883021"},{"ButtonImagePath":"881526.png","DisplayName":"幻觉2","typeid":"1","LapisId":"881526"},{"ButtonImagePath":"881609.jpg","DisplayName":"蒙娜丽莎的珠宝","typeid":"1","LapisId":"881609"},{"ButtonImagePath":"883011.jpg","DisplayName":"扑克追击","typeid":"2","LapisId":"883011"},{"ButtonImagePath":"881270.jpg","DisplayName":"臭虫世界","typeid":"1","LapisId":"881270"},{"ButtonImagePath":"881572.jpg","DisplayName":"快乐的小鸟","typeid":"1","LapisId":"881572"},{"ButtonImagePath":"904631.png","DisplayName":"旋转卷轴","typeid":"1","LapisId":"904631"},{"ButtonImagePath":"904217.jpg","DisplayName":"幸运四叶草","typeid":"1","LapisId":"904217"},{"ButtonImagePath":"904192.jpg","DisplayName":"增强+","typeid":"1","LapisId":"904192"},{"ButtonImagePath":"882010.jpg","DisplayName":"王牌扑克累积版","typeid":"3","LapisId":"882010"},{"ButtonImagePath":"883049.jpg","DisplayName":"3D梭哈","typeid":"2","LapisId":"883049"},{"ButtonImagePath":"904782.jpg","DisplayName":"AstroMagic","typeid":"1","LapisId":"904782"},{"ButtonImagePath":"883016.jpg","DisplayName":"多手21点","typeid":"2","LapisId":"883016"},{"ButtonImagePath":"904567.jpg","DisplayName":"百万分","typeid":"1","LapisId":"904567"},{"ButtonImagePath":"882001.jpg","DisplayName":"10X游戏","typeid":"3","LapisId":"882001"},{"ButtonImagePath":"881599.png","DisplayName":"尸骨传说、头骨的传说","typeid":"1","LapisId":"881599"},{"ButtonImagePath":"904615.jpg","DisplayName":"幸运发财树","typeid":"1","LapisId":"904615"},{"ButtonImagePath":"883032.jpg","DisplayName":"轮盘3D","typeid":"4","LapisId":"883032"},{"ButtonImagePath":"883020.jpg","DisplayName":"简单轮盘","typeid":"4","LapisId":"883020"},{"ButtonImagePath":"882018.jpg","DisplayName":"10对高手","typeid":"3","LapisId":"882018"},{"ButtonImagePath":"881588.jpg","DisplayName":"鬼火","typeid":"1","LapisId":"881588"},{"ButtonImagePath":"904612.jpg","DisplayName":"皇家现金","typeid":"1","LapisId":"904612"},{"ButtonImagePath":"881521.jpg","DisplayName":"捉鬼合家欢","typeid":"1","LapisId":"881521"},{"ButtonImagePath":"882020.jpg","DisplayName":"50X扑克游戏","typeid":"3","LapisId":"882020"},{"ButtonImagePath":"882030.jpg","DisplayName":"VIP王牌扑克","typeid":"3","LapisId":"882030"},{"ButtonImagePath":"882029.jpg","DisplayName":"拉斯维加斯扑克4UP","typeid":"3","LapisId":"882029"},{"ButtonImagePath":"882011.jpg","DisplayName":"视频扑克追击","typeid":"3","LapisId":"882011"},{"ButtonImagePath":"882028.jpg","DisplayName":"4X拉斯维加斯王牌扑克","typeid":"3","LapisId":"882028"},{"ButtonImagePath":"883033.jpg","DisplayName":"法式21点","typeid":"2","LapisId":"883033"},{"ButtonImagePath":"883037.jpg","DisplayName":"多手21点VIP","typeid":"2","LapisId":"883037"},{"ButtonImagePath":"904186.jpg","DisplayName":"疯狂钻石","typeid":"1","LapisId":"904186"},{"ButtonImagePath":"904183.jpg","DisplayName":"热辣超速","typeid":"1","LapisId":"904183"},{"ButtonImagePath":"883039.jpg","DisplayName":"21点超7多手","typeid":"2","LapisId":"883039"},{"ButtonImagePath":"881607.jpg","DisplayName":"机器人扣杀","typeid":"1","LapisId":"881607"}];

var ebetdata=[
	{"DisplayName":"亲爱的","ButtonImagePath":"ebet/reel-love.png","LapisId": "reel-love","typeid":"1"},
	{"DisplayName":"少林足球","ButtonImagePath":"ebet/shaolin-soccer.png","LapisId": "shaolin-soccer","typeid":"1"},
	{"DisplayName":"寻宝黄金城","ButtonImagePath":"ebet/treasures-aztec.jpg","LapisId": "treasures-aztec","typeid":"1"},
	{"DisplayName":"添好运","ButtonImagePath":"ebet/dim-sum-mania.jpg","LapisId": "dim-sum-mania","typeid":"1"},
	{"DisplayName":"火树赢花","ButtonImagePath":"ebet/wild-fireworks.png","LapisId": "wild-fireworks","typeid":"1"},
	{"DisplayName":"澳门壕梦","ButtonImagePath":"ebet/dreams-of-macau.png","LapisId": "dreams-of-macau","typeid":"1"},
	{"DisplayName":"凤凰传奇","ButtonImagePath":"ebet/phoenix-rises.png","LapisId": "phoenix-rises","typeid":"1"},
	{"DisplayName":"福运象财神","ButtonImagePath":"ebet/ganesha-fortune.png","LapisId": "ganesha-fortune","typeid":"1"},
	{"DisplayName":"埃及探秘宝典 ","ButtonImagePath":"ebet/egypts-book-mystery.png","LapisId": "egypts-book-mystery","typeid":"1"},
	{"DisplayName":"麻将胡了2","ButtonImagePath":"ebet/mahjong-ways2.png","LapisId": "mahjong-ways2","typeid":"1"},
	{"DisplayName":"比基尼天堂","ButtonImagePath":"ebet/bikini-paradise.png","LapisId": "bikini-paradise","typeid":"1"},
	{"DisplayName":"鼠鼠福福","ButtonImagePath":"ebet/fortune-mouse.png","LapisId": "fortune-mouse","typeid":"1"},
	{"DisplayName":"寻龙探宝","ButtonImagePath":"ebet/dragon-hatch.png","LapisId": "dragon-hatch","typeid":"1"},
	{"DisplayName":"麻将胡了","ButtonImagePath":"ebet/mahjong-ways.png","LapisId": "mahjong-ways","typeid":"1"},
	{"DisplayName":"龙虎争霸","ButtonImagePath":"ebet/dragon-tiger-luck.png","LapisId": "dragon-tiger-luck","typeid":"1"},
	{"DisplayName":"拳霸","ButtonImagePath":"ebet/muay-thai-champion.png","LapisId": "muay-thai-champion","typeid":"1"},
	{"DisplayName":"忍者vs武侍","ButtonImagePath":"ebet/ninja-vs-samurai.png","LapisId": "ninja-vs-samurai","typeid":"1"},
	{"DisplayName":"唐伯虎点秋香","ButtonImagePath":"ebet/flirting-scholar.png","LapisId": "flirting-scholar","typeid":"1"},
	{"DisplayName":"爱尔兰精灵","ButtonImagePath":"ebet/leprechaun-riches.png","LapisId": "leprechaun-riches","typeid":"1"},
	{"DisplayName":"块块赢","ButtonImagePath":"ebet/tiki-go.png","LapisId": "tiki-go","typeid":"1"},
	{"DisplayName":"赏金船长","ButtonImagePath":"ebet/captains-bounty.png","LapisId": "captains-bounty","typeid":"1"},
	{"DisplayName":"嘻游记","ButtonImagePath":"ebet/journey-to-the-wealth.png","LapisId": "journey-to-the-wealth","typeid":"1"},
	{"DisplayName":"冰雪大冲关","ButtonImagePath":"ebet/the-great-icescape.png","LapisId": "the-great-icescape","typeid":"1"},
	{"DisplayName":"重金摇滚","ButtonImagePath":"ebet/wild-inferno.png","LapisId": "wild-inferno","typeid":"1"},
	{"DisplayName":"双囍临门","ButtonImagePath":"ebet/double-fortune.png","LapisId": "double-fortune","typeid":"1"},
	{"DisplayName":"摸金校尉","ButtonImagePath":"ebet/tomb-of-treasure.png","LapisId": "tomb-of-treasure","typeid":"1"},
	{"DisplayName":"水果丛林","ButtonImagePath":"ebet/jungle-delight.png","LapisId": "jungle-delight","typeid":"1"},
	{"DisplayName":"三只猴子","ButtonImagePath":"ebet/three-monkeys.png","LapisId": "three-monkeys","typeid":"1"},
	{"DisplayName":"象财神","ButtonImagePath":"ebet/ganesha-gold.png","LapisId": "ganesha-gold","typeid":"1"},
	{"DisplayName":"皇上吉祥","ButtonImagePath":"ebet/emperors-favour.png","LapisId": "emperors-favour","typeid":"1"},
	{"DisplayName":"埃及寻宝","ButtonImagePath":"ebet/symbols-of-egypt.png","LapisId": "symbols-of-egypt","typeid":"1"},
	{"DisplayName":"金猪报财","ButtonImagePath":"ebet/piggy-gold.png","LapisId": "piggy-gold","typeid":"1"},
	{"DisplayName":"宝石侠-大宝剑","ButtonImagePath":"ebet/gem-saviour-sword.png","LapisId": "gem-saviour-sword","typeid":"1"},
	{"DisplayName":"圣诞欢乐送","ButtonImagePath":"ebet/santas-gift-rush.png","LapisId": "santas-gift-rush","typeid":"1"},
	{"DisplayName":"嘻哈熊猫","ButtonImagePath":"ebet/hip-hop-panda.png","LapisId": "hip-hop-panda","typeid":"1"},
	{"DisplayName":"舞狮进宝","ButtonImagePath":"ebet/prosperity-lion.png","LapisId": "prosperity-lion","typeid":"1"},
	{"DisplayName":"网红餐厅","ButtonImagePath":"ebet/restaurant-craze.png","LapisId": "restaurant-craze","typeid":"1"},
	{"DisplayName":"后羿射日","ButtonImagePath":"ebet/legend-of-hou-yi.png","LapisId": "legend-of-hou-yi","typeid":"1"},
	{"DisplayName":"万胜狂欢夜","ButtonImagePath":"ebet/mr-hallow-win.png","LapisId": "mr-hallow-win","typeid":"1"},
	{"DisplayName":"鱼跃龙门","ButtonImagePath":"ebet/dragon-legend.png","LapisId": "dragon-legend","typeid":"1"},
	{"DisplayName":"麻辣火锅","ButtonImagePath":"ebet/hotpot.png","LapisId": "hotpot","typeid":"1"},
	{"DisplayName":"逆袭的小红帽","ButtonImagePath":"ebet/hood-wolf.png","LapisId": "hood-wolf","typeid":"1"},
	{"DisplayName":"国王的召唤","ButtonImagePath":"ebet/summon-conquer.png","LapisId": "summon-conquer","typeid":"1"},
	{"DisplayName":"蒸汽朋克","ButtonImagePath":"ebet/steam-punk.png","LapisId": "steam-punk","typeid":"1"},
	{"DisplayName":"宝石侠","ButtonImagePath":"ebet/gem-saviour.png","LapisId": "gem-saviour","typeid":"1"},
	{"DisplayName":"巫师之书","ButtonImagePath":"ebet/wizdom-wonders.png","LapisId": "wizdom-wonders","typeid":"1"},
	{"DisplayName":"抓抓乐","ButtonImagePath":"ebet/plushie-frenzy.png","LapisId": "plushie-frenzy","typeid":"1"},
	{"DisplayName":"美杜莎1","ButtonImagePath":"ebet/medusa.png","LapisId": "medusa","typeid":"1"},
	{"DisplayName":"摇钱树","ButtonImagePath":"ebet/fortune-tree.png","LapisId": "fortune-tree","typeid":"1"},
	{"DisplayName":"豌豆精灵","ButtonImagePath":"ebet/peas-fairy.png","LapisId": "peas-fairy","typeid":"1"},
	{"DisplayName":"美杜莎2","ButtonImagePath":"ebet/medusa2.png","LapisId": "medusa2","typeid":"1"},
	{"DisplayName":"旺旺旺","ButtonImagePath":"ebet/win-win-won.png","LapisId": "win-win-won","typeid":"1"},
	{"DisplayName":"横财来啦","ButtonImagePath":"ebet/fortune-gods.png","LapisId": "fortune-gods","typeid":"1"},
	{"DisplayName":"夜戏貂蝉","ButtonImagePath":"ebet/diaochan.png","LapisId": "diaochan","typeid":"1"}
	];

var bbindata=[
	{"DisplayName":"连环夺宝2","ButtonImagePath":"bbin/5912.png","LapisId": "5912","typeid":"1"},
	{"DisplayName":"魔法元素","ButtonImagePath":"bbin/5910.png","LapisId": "5910","typeid":"1"},
	{"DisplayName":"开心消消乐","ButtonImagePath":"bbin/5909.png","LapisId": "5909","typeid":"1"},
	{"DisplayName":"糖果派对2","ButtonImagePath":"bbin/5908.png","LapisId": "5908","typeid":"1"},
	{"DisplayName":"趣味台球","ButtonImagePath":"bbin/5907.png","LapisId": "5907","typeid":"1"},
	{"DisplayName":"蒸气炸弹","ButtonImagePath":"bbin/5904.png","LapisId": "5904","typeid":"1"},
	{"DisplayName":"秦皇秘宝","ButtonImagePath":"bbin/5903.png","LapisId": "5903","typeid":"1"},
	{"DisplayName":"糖果派对","ButtonImagePath":"bbin/5902.png","LapisId": "5902","typeid":"1"},
	{"DisplayName":"连环夺宝","ButtonImagePath":"bbin/5901.png","LapisId": "5901","typeid":"1"},
	{"DisplayName":"经典高球","ButtonImagePath":"bbin/5839.png","LapisId": "5839","typeid":"1"},
	{"DisplayName":"喜福猴年","ButtonImagePath":"bbin/5837.png","LapisId": "5837","typeid":"1"},
	{"DisplayName":"喜福牛年","ButtonImagePath":"bbin/5835.png","LapisId": "5835","typeid":"1"},
	{"DisplayName":"霸王龙","ButtonImagePath":"bbin/5828.png","LapisId": "5828","typeid":"1"},
	{"DisplayName":"恶龙传说","ButtonImagePath":"bbin/5824.png","LapisId": "5824","typeid":"1"},
	{"DisplayName":"发大财","ButtonImagePath":"bbin/5823.png","LapisId": "5823","typeid":"1"},
	{"DisplayName":"航海时代","ButtonImagePath":"bbin/5810.png","LapisId": "5810","typeid":"1"},
	{"DisplayName":"凯萨帝国","ButtonImagePath":"bbin/5805.png","LapisId": "5805","typeid":"1"},
	{"DisplayName":"阿兹特克宝藏","ButtonImagePath":"bbin/5803.png","LapisId": "5803","typeid":"1"},
	{"DisplayName":"秘境冒险","ButtonImagePath":"bbin/5601.png","LapisId": "5601","typeid":"1"},
	{"DisplayName":"大红帽与小野狼","ButtonImagePath":"bbin/5407.png","LapisId": "5407","typeid":"1"},
	{"DisplayName":"沙滩排球","ButtonImagePath":"bbin/5404.png","LapisId": "5404","typeid":"1"},
	{"DisplayName":"夜市人生","ButtonImagePath":"bbin/5402.png","LapisId": "5402","typeid":"1"},
	{"DisplayName":"2014FIFA","ButtonImagePath":"bbin/5204.png","LapisId": "5204","typeid":"1"},
	{"DisplayName":"蒸氣王國","ButtonImagePath":"bbin/5178.png","LapisId": "5178","typeid":"1"},
	{"DisplayName":"野蛮战国","ButtonImagePath":"bbin/5176.png","LapisId": "5176","typeid":"1"},
	{"DisplayName":"电音之王","ButtonImagePath":"bbin/5174.png","LapisId": "5174","typeid":"1"},
	{"DisplayName":"招财进宝","ButtonImagePath":"bbin/5173.png","LapisId": "5173","typeid":"1"},
	{"DisplayName":"糖果派对-极速版","ButtonImagePath":"bbin/5171.png","LapisId": "5171","typeid":"1"},
	{"DisplayName":"海底传奇","ButtonImagePath":"bbin/5170.png","LapisId": "5170","typeid":"1"},
	{"DisplayName":"中国好声音","ButtonImagePath":"bbin/5169.png","LapisId": "5169","typeid":"1"},
	{"DisplayName":"金鸡报囍2020","ButtonImagePath":"bbin/5168.png","LapisId": "5168","typeid":"1"},
	{"DisplayName":"马到成功","ButtonImagePath":"bbin/5167.png","LapisId": "5167","typeid":"1"},
	{"DisplayName":"葫芦娃","ButtonImagePath":"bbin/5166.png","LapisId": "5166","typeid":"1"},
	{"DisplayName":"战神","ButtonImagePath":"bbin/5165.png","LapisId": "5165","typeid":"1"},
	{"DisplayName":"月狼","ButtonImagePath":"bbin/5163.png","LapisId": "5163","typeid":"1"},
	{"DisplayName":"开心蛋","ButtonImagePath":"bbin/5162.png","LapisId": "5162","typeid":"1"},
	{"DisplayName":"回转寿司","ButtonImagePath":"bbin/5161.png","LapisId": "5161","typeid":"1"},
	{"DisplayName":"雷神索尔","ButtonImagePath":"bbin/5160.png","LapisId": "5160","typeid":"1"},
	{"DisplayName":"疯狂麦斯","ButtonImagePath":"bbin/5159.png","LapisId": "5159","typeid":"1"},
	{"DisplayName":"三元四喜","ButtonImagePath":"bbin/5157.png","LapisId": "5157","typeid":"1"},
	{"DisplayName":"祖玛帝国","ButtonImagePath":"bbin/5156.png","LapisId": "5156","typeid":"1"},
	{"DisplayName":"东海龙宫","ButtonImagePath":"bbin/5155.png","LapisId": "5155","typeid":"1"},
	{"DisplayName":"初音大进击","ButtonImagePath":"bbin/5154.png","LapisId": "5154","typeid":"1"},
	{"DisplayName":"九尾狐","ButtonImagePath":"bbin/5153.png","LapisId": "5153","typeid":"1"},
	{"DisplayName":"埃及传奇","ButtonImagePath":"bbin/5152.png","LapisId": "5152","typeid":"1"},
	{"DisplayName":"Jenga","ButtonImagePath":"bbin/5151.png","LapisId": "5151","typeid":"1"},
	{"DisplayName":"宝石传奇","ButtonImagePath":"bbin/5150.png","LapisId": "5150","typeid":"1"},
	{"DisplayName":"糖果派","ButtonImagePath":"bbin/5146.png","LapisId": "5146","typeid":"1"},
	{"DisplayName":"步步高升","ButtonImagePath":"bbin/5145.png","LapisId": "5145","typeid":"1"},
	{"DisplayName":"情人夜","ButtonImagePath":"bbin/5144.png","LapisId": "5144","typeid":"1"},
	{"DisplayName":"糖果派对3","ButtonImagePath":"bbin/5143.png","LapisId": "5143","typeid":"1"},
	{"DisplayName":"魔光幻音","ButtonImagePath":"bbin/5142.png","LapisId": "5142","typeid":"1"},
	{"DisplayName":"斗牛赢家","ButtonImagePath":"bbin/5141.png","LapisId": "5141","typeid":"1"},
	{"DisplayName":"啤酒嘉年华","ButtonImagePath":"bbin/5140.png","LapisId": "5140","typeid":"1"},
	{"DisplayName":"熊猫乐园","ButtonImagePath":"bbin/5139.png","LapisId": "5139","typeid":"1"},
	{"DisplayName":"夹猪珠","ButtonImagePath":"bbin/5138.png","LapisId": "5138","typeid":"1"},
	{"DisplayName":"多福多财","ButtonImagePath":"bbin/5128.png","LapisId": "5128","typeid":"1"},
	{"DisplayName":"绝地求生","ButtonImagePath":"bbin/5127.png","LapisId": "5127","typeid":"1"},
	{"DisplayName":"奖金21点","ButtonImagePath":"bbin/5126.png","LapisId": "5126","typeid":"1"},
	{"DisplayName":"维加斯21点","ButtonImagePath":"bbin/5125.png","LapisId": "5125","typeid":"1"},
	{"DisplayName":"西班牙21点","ButtonImagePath":"bbin/5124.png","LapisId": "5124","typeid":"1"},
	{"DisplayName":"经典21点","ButtonImagePath":"bbin/5123.png","LapisId": "5123","typeid":"1"},
	{"DisplayName":"球球大作战","ButtonImagePath":"bbin/5122.png","LapisId": "5122","typeid":"1"},
	{"DisplayName":"奥林帕斯","ButtonImagePath":"bbin/5121.png","LapisId": "5121","typeid":"1"},
	{"DisplayName":"女娲补天","ButtonImagePath":"bbin/5120.png","LapisId": "5120","typeid":"1"},
	{"DisplayName":"神秘岛","ButtonImagePath":"bbin/5119.png","LapisId": "5119","typeid":"1"},
	{"DisplayName":"夜上海","ButtonImagePath":"bbin/5110.png","LapisId": "5110","typeid":"1"},
	{"DisplayName":"法式轮盘","ButtonImagePath":"bbin/5109.png","LapisId": "5109","typeid":"1"},
	{"DisplayName":"彩金轮盘","ButtonImagePath":"bbin/5108.png","LapisId": "5108","typeid":"1"},
	{"DisplayName":"美式轮盘","ButtonImagePath":"bbin/5107.png","LapisId": "5107","typeid":"1"},
	{"DisplayName":"三国","ButtonImagePath":"bbin/5106.png","LapisId": "5106","typeid":"1"},
	{"DisplayName":"欧式轮盘","ButtonImagePath":"bbin/5105.png","LapisId": "5105","typeid":"1"},
	{"DisplayName":"七夕","ButtonImagePath":"bbin/5100.png","LapisId": "5100","typeid":"1"},
	{"DisplayName":"金狗旺岁","ButtonImagePath":"bbin/5099.png","LapisId": "5099","typeid":"1"},
	{"DisplayName":"五福临门","ButtonImagePath":"bbin/5098.png","LapisId": "5098","typeid":"1"},
	{"DisplayName":"海底世界","ButtonImagePath":"bbin/5097.png","LapisId": "5097","typeid":"1"},
	{"DisplayName":"五行","ButtonImagePath":"bbin/5096.png","LapisId": "5096","typeid":"1"},
	{"DisplayName":"斗鸡","ButtonImagePath":"bbin/5095.png","LapisId": "5095","typeid":"1"},
	{"DisplayName":"金瓶梅2","ButtonImagePath":"bbin/5094.png","LapisId": "5094","typeid":"1"},
	{"DisplayName":"金瓶梅","ButtonImagePath":"bbin/5093.png","LapisId": "5093","typeid":"1"},
	{"DisplayName":"金鸡报喜","ButtonImagePath":"bbin/5090.png","LapisId": "5090","typeid":"1"},
	{"DisplayName":"红狗","ButtonImagePath":"bbin/5089.png","LapisId": "5089","typeid":"1"},
	{"DisplayName":"斗大","ButtonImagePath":"bbin/5088.png","LapisId": "5088","typeid":"1"},
	{"DisplayName":"圣兽传说","ButtonImagePath":"bbin/5084.png","LapisId": "5084","typeid":"1"},
	{"DisplayName":"钻石列车","ButtonImagePath":"bbin/5083.png","LapisId": "5083","typeid":"1"},
	{"DisplayName":"乐透转轮","ButtonImagePath":"bbin/5080.png","LapisId": "5080","typeid":"1"},
	{"DisplayName":"3D数字大转轮","ButtonImagePath":"bbin/5079.png","LapisId": "5079","typeid":"1"},
	{"DisplayName":"水果大转轮","ButtonImagePath":"bbin/5077.png","LapisId": "5077","typeid":"1"},
	{"DisplayName":"数字大转轮","ButtonImagePath":"bbin/5076.png","LapisId": "5076","typeid":"1"},
	{"DisplayName":"水果擂台","ButtonImagePath":"bbin/5069.png","LapisId": "5069","typeid":"1"},
	{"DisplayName":"酷搜马戏团","ButtonImagePath":"bbin/5068.png","LapisId": "5068","typeid":"1"},
	{"DisplayName":"大话西游","ButtonImagePath":"bbin/5067.png","LapisId": "5067","typeid":"1"},
	{"DisplayName":"足球拉霸","ButtonImagePath":"bbin/5066.png","LapisId": "5066","typeid":"1"},
	{"DisplayName":"筒子拉霸","ButtonImagePath":"bbin/5065.png","LapisId": "5065","typeid":"1"},
	{"DisplayName":"扑克拉霸","ButtonImagePath":"bbin/5064.png","LapisId": "5064","typeid":"1"},
	{"DisplayName":"水果拉霸","ButtonImagePath":"bbin/5063.png","LapisId": "5063","typeid":"1"},
	{"DisplayName":"龙在囧途","ButtonImagePath":"bbin/5062.png","LapisId": "5062","typeid":"1"},
	{"DisplayName":"超级7","ButtonImagePath":"bbin/5061.png","LapisId": "5061","typeid":"1"},
	{"DisplayName":"疯狂水果盘","ButtonImagePath":"bbin/5058.png","LapisId": "5058","typeid":"1"},
	{"DisplayName":"明星97","ButtonImagePath":"bbin/5057.png","LapisId": "5057","typeid":"1"},
	{"DisplayName":"爆骰","ButtonImagePath":"bbin/5054.png","LapisId": "5054","typeid":"1"},
	{"DisplayName":"斗魂","ButtonImagePath":"bbin/5046.png","LapisId": "5046","typeid":"1"},
	{"DisplayName":"森林舞会","ButtonImagePath":"bbin/5045.png","LapisId": "5045","typeid":"1"},
	{"DisplayName":"明星97II","ButtonImagePath":"bbin/5044.png","LapisId": "5044","typeid":"1"},
	{"DisplayName":"钻石水果盘","ButtonImagePath":"bbin/5043.png","LapisId": "5043","typeid":"1"},
	{"DisplayName":"鱼虾蟹","ButtonImagePath":"bbin/5039.png","LapisId": "5039","typeid":"1"},
	{"DisplayName":"幸运财神","ButtonImagePath":"bbin/5030.png","LapisId": "5030","typeid":"1"},
	{"DisplayName":"FIFA2010","ButtonImagePath":"bbin/5015.png","LapisId": "5015","typeid":"1"},
	{"DisplayName":"丛林","ButtonImagePath":"bbin/5014.png","LapisId": "5014","typeid":"1"},
	{"DisplayName":"传统","ButtonImagePath":"bbin/5013.png","LapisId": "5013","typeid":"1"},
	{"DisplayName":"外星争霸","ButtonImagePath":"bbin/5012.png","LapisId": "5012","typeid":"1"},
	{"DisplayName":"外星战记","ButtonImagePath":"bbin/5010.png","LapisId": "5010","typeid":"1"},
	{"DisplayName":"惑星战记","ButtonImagePath":"bbin/5005.png","LapisId": "5005","typeid":"1"}
	];

