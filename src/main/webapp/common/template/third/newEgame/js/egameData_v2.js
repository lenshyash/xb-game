var mgdata = [ {
	"ButtonImagePath" : "BTN_Junglejim_zh",
	"DisplayName" : "丛林吉姆-黄金国",
	"typeid" : 1,
	"LapisId" : "66481"
}, {
	"ButtonImagePath" : "BTN_108Empire_ZH.jpg",
	"DisplayName" : "108好汉",
	"typeid" : 1,
	"LapisId" : "59065"
}, {
	"ButtonImagePath" : "BTN_3Empire_ZH.jpg",
	"DisplayName" : "三國",
	"typeid" : 1,
	"LapisId" : "59067"
}, {
	"ButtonImagePath" : "BTN_5ReelDrive1",
	"DisplayName" : "5卷的驱动器",
	"typeid" : 1,
	"LapisId" : "28114"
}, {
	"ButtonImagePath" : "BTN_AcesAndEights1",
	"DisplayName" : "王牌和八分",
	"typeid" : 6,
	"LapisId" : "28116"
}, {
	"ButtonImagePath" : "BTN_AcesFacesPowerPoker1",
	"DisplayName" : "王牌和面孔",
	"typeid" : 6,
	"LapisId" : "28122"
}, {
	"ButtonImagePath" : "BTN_AcesFaces1",
	"DisplayName" : "王牌和面孔",
	"typeid" : 6,
	"LapisId" : "28128"
}, {
	"ButtonImagePath" : "BTN_AdventurePalaceHD",
	"DisplayName" : "宫廷历险",
	"typeid" : 1,
	"LapisId" : "28134"
}, {
	"ButtonImagePath" : "BTN_AgeofDiscovery3",
	"DisplayName" : "大航海时代",
	"typeid" : 1,
	"LapisId" : "28136"
}, {
	"ButtonImagePath" : "BTN_AgentJaneBlonde7",
	"DisplayName" : "美女秘探",
	"typeid" : 1,
	"LapisId" : "28138"
}, {
	"ButtonImagePath" : "BTN_AlaskanFishing1",
	"DisplayName" : "阿拉斯加钓鱼",
	"typeid" : 1,
	"LapisId" : "28144"
}, {
	"ButtonImagePath" : "BTN_AlaxeInZombieland",
	"DisplayName" : "亚历山大在尸乐园",
	"typeid" : 1,
	"LapisId" : "28148"
}, {
	"ButtonImagePath" : "BTN_AllAces1",
	"DisplayName" : "所有王牌扑克",
	"typeid" : 6,
	"LapisId" : "28150"
}, {
	"ButtonImagePath" : "BTN_AllAmerican1",
	"DisplayName" : "所有美国",
	"typeid" : 6,
	"LapisId" : "28152"
}, {
	"ButtonImagePath" : "BTN_USRoulette1",
	"DisplayName" : "美式轮盘",
	"typeid" : 5,
	"LapisId" : "28204"
}, {
	"ButtonImagePath" : "BTN_ArcticFortune1",
	"DisplayName" : "极寒鸿运",
	"typeid" : 1,
	"LapisId" : "28166"
}, {
	"ButtonImagePath" : "BTN_Ariana_ZH",
	"DisplayName" : "爱丽娜",
	"typeid" : 1,
	"LapisId" : "28174"
}, {
	"ButtonImagePath" : "BTN_AroundTheWorld",
	"DisplayName" : "环游世界",
	"typeid" : 1,
	"LapisId" : "28182"
}, {
	"ButtonImagePath" : "BTN_AsianBeauty1",
	"DisplayName" : "亚洲美人",
	"typeid" : 1,
	"LapisId" : "28188"
}, 
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_AtlanticCityBlackjack1",
	"DisplayName" : "大西洋城黄金21点",
	"typeid" : 5,
	"LapisId" : "28192"
},**/ 
{
	"ButtonImagePath" : "BTN_AvalonHD_ZH",
	"DisplayName" : "阿瓦隆",
	"typeid" : 1,
	"LapisId" : "28194"
}, {
	"ButtonImagePath" : "BTN_AvalonII",
	"DisplayName" : "阿瓦隆2",
	"typeid" : 1,
	"LapisId" : "28198"
}, {
	"ButtonImagePath" : "BTN_Baccarat2",
	"DisplayName" : "百家乐",
	"typeid" : 5,
	"LapisId" : "28234"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_Baccarat1",
	"DisplayName" : "百家乐黄金版",
	"typeid" : 5,
	"LapisId" : "28236"
}, {
	"ButtonImagePath" : "BTN_BasketballStar_ZH",
	"DisplayName" : "篮球巨星",
	"typeid" : 1,
	"LapisId" : "44751"
}, {
	"ButtonImagePath" : "BTN_BattlestarGalactica1",
	"DisplayName" : "太空堡垒",
	"typeid" : 1,
	"LapisId" : "28240"
}, {
	"ButtonImagePath" : "BTN_BeerFest1",
	"DisplayName" : "啤酒巨星",
	"typeid" : 4,
	"LapisId" : "28242"
}, {
	"ButtonImagePath" : "BTN_Belissimo17",
	"DisplayName" : "超级厨王",
	"typeid" : 2,
	"LapisId" : "28248"
},
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_BigFive1",
	"DisplayName" : "大五黄金21点",
	"typeid" : 5,
	"LapisId" : "28260"
},**/

 {
	"ButtonImagePath" : "BTN_BigBreak1",
	"DisplayName" : "大破",
	"typeid" : 1,
	"LapisId" : "28266"
}, {
	"ButtonImagePath" : "BTN_BigChef_ZH",
	"DisplayName" : "厨神",
	"typeid" : 1,
	"LapisId" : "28268"
}, {
	"ButtonImagePath" : "BTN_BigKahuna1",
	"DisplayName" : "征服钱海",
	"typeid" : 1,
	"LapisId" : "28270"
}, {
	"ButtonImagePath" : "BTN_BigKahuna_SL1",
	"DisplayName" : "征服钱海-蛇与梯子",
	"typeid" : 1,
	"LapisId" : "28272"
}, {
	"ButtonImagePath" : "BTN_BigTop1",
	"DisplayName" : "马戏篷",
	"typeid" : 1,
	"LapisId" : "28274"
}, {
	"ButtonImagePath" : "BTN_BikiniParty_ZH",
	"DisplayName" : "比基尼派对",
	"typeid" : 1,
	"LapisId" : "46497"
}, {
	"ButtonImagePath" : "BTN_BingoBonanza1",
	"DisplayName" : "宾果富豪",
	"typeid" : 4,
	"LapisId" : "28276"
}, {
	"ButtonImagePath" : "BTN_BonusDeucesWild1",
	"DisplayName" : "疯狂奖金局末平分",
	"typeid" : 6,
	"LapisId" : "28280"
}, {
	"ButtonImagePath" : "BTN_BonusDeucesWild1",
	"DisplayName" : "奖金扑克",
	"typeid" : 6,
	"LapisId" : "28282"
}, {
	"ButtonImagePath" : "BTN_BonusPokerDeluxe1",
	"DisplayName" : "豪华奖金扑克",
	"typeid" : 6,
	"LapisId" : "28284"
}, {
	"ButtonImagePath" : "BTN_BootyTime",
	"DisplayName" : "藏宝时间",
	"typeid" : 1,
	"LapisId" : "28286"
}, {
	"ButtonImagePath" : "BTN_BowledOver1",
	"DisplayName" : "击倒",
	"typeid" : 4,
	"LapisId" : "28290"
}, {
	"ButtonImagePath" : "BTN_BreakAway1",
	"DisplayName" : "摆脱",
	"typeid" : 1,
	"LapisId" : "28292"
}, {
	"ButtonImagePath" : "BTN_BreakDaBank1",
	"DisplayName" : "抢银行",
	"typeid" : 2,
	"LapisId" : "28296"
}, {
	"ButtonImagePath" : "BTN_BreakDaBankAgain1",
	"DisplayName" : "银行爆破",
	"typeid" : 1,
	"LapisId" : "28310"
}, {
	"ButtonImagePath" : "BTN_BrideZilla",
	"DisplayName" : "新娘吉拉",
	"typeid" : 1,
	"LapisId" : "29601"
}, {
	"ButtonImagePath" : "BTN_BubbleBonanza",
	"DisplayName" : "泡泡富豪",
	"typeid" : 7,
	"LapisId" : "29591"
}, {
	"ButtonImagePath" : "BTN_BunnyBoiler1",
	"DisplayName" : "兔子锅炉",
	"typeid" : 4,
	"LapisId" : "28278"
}, {
	"ButtonImagePath" : "BTN_BunnyBoilerGold1",
	"DisplayName" : "黄金兔子锅炉",
	"typeid" : 7,
	"LapisId" : "28330"
}, {
	"ButtonImagePath" : "BTN_BurningDesire1",
	"DisplayName" : "燃烧的欲望",
	"typeid" : 1,
	"LapisId" : "28332"
}, {
	"ButtonImagePath" : "BTN_BushTelegraph1",
	"DisplayName" : "丛林摇摆",
	"typeid" : 1,
	"LapisId" : "28336"
}, {
	"ButtonImagePath" : "BTN_BustTheBank1",
	"DisplayName" : "抢劫银行",
	"typeid" : 1,
	"LapisId" : "29555"
}, {
	"ButtonImagePath" : "BTN_Carnaval2",
	"DisplayName" : "狂欢节",
	"typeid" : 1,
	"LapisId" : "28350"
}, {
	"ButtonImagePath" : "BTN_CashClams2",
	"DisplayName" : "现金蚬",
	"typeid" : 2,
	"LapisId" : "28354"
}, {
	"ButtonImagePath" : "BTN_CashCrazy9",
	"DisplayName" : "财运疯狂",
	"typeid" : 2,
	"LapisId" : "28358"
}, {
	"ButtonImagePath" : "BTN_Cashanova1",
	"DisplayName" : "卡萨努瓦",
	"typeid" : 1,
	"LapisId" : "28364"
}, {
	"ButtonImagePath" : "BTN_Cashapillar1",
	"DisplayName" : "昆虫派对",
	"typeid" : 1,
	"LapisId" : "28368"
}, {
	"ButtonImagePath" : "BTN_Cashapillar1",
	"DisplayName" : "昆虫派对",
	"typeid" : 7,
	"LapisId" : "28372"
}, {
	"ButtonImagePath" : "BTN_Cashville1",
	"DisplayName" : "挥金如土",
	"typeid" : 1,
	"LapisId" : "28376"
}, {
	"ButtonImagePath" : "BTN_CentreCourt1",
	"DisplayName" : "中心球场",
	"typeid" : 1,
	"LapisId" : "28380"
}, {
	"ButtonImagePath" : "BTN_ChainMail_ZH",
	"DisplayName" : "锁子甲",
	"typeid" : 1,
	"LapisId" : "28384"
}, 
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_ClassicBlackjack1",
	"DisplayName" : "经典黄金21点",
	"typeid" : 5,
	"LapisId" : "28398"
}, **/

{
	"ButtonImagePath" : "BTN_CoolBuck1",
	"DisplayName" : "酷巴克",
	"typeid" : 2,
	"LapisId" : "28400"
}, {
	"ButtonImagePath" : "BTN_CoolWolf1",
	"DisplayName" : "酷狼",
	"typeid" : 1,
	"LapisId" : "28404"
}, {
	"ButtonImagePath" : "BTN_CosmicCat1",
	"DisplayName" : "宇宙猫",
	"typeid" : 2,
	"LapisId" : "28408"
}, {
	"ButtonImagePath" : "BTN_CouchPotato2",
	"DisplayName" : "沙发土豆",
	"typeid" : 2,
	"LapisId" : "28414"
}, {
	"ButtonImagePath" : "BTN_Crazy80s4",
	"DisplayName" : "疯狂的80年代",
	"typeid" : 1,
	"LapisId" : "28418"
}, {
	"ButtonImagePath" : "BTN_CrazyChameleons1",
	"DisplayName" : "疯狂的变色龙",
	"typeid" : 1,
	"LapisId" : "28422"
}, {
	"ButtonImagePath" : "BTN_CricketStar.jpg",
	"DisplayName" : "板球明星",
	"typeid" : 1,
	"LapisId" : "29561"
}, {
	"ButtonImagePath" : "BTN_Crocodopolis",
	"DisplayName" : "鳄鱼建城邦",
	"typeid" : 1,
	"LapisId" : "28424"
}, {
	"ButtonImagePath" : "BTN_CrownAndAnchor1",
	"DisplayName" : "国际鱼虾蟹骰宝",
	"typeid" : 7,
	"LapisId" : "28428"
}, {
	"ButtonImagePath" : "BTN_CryptCrusade1",
	"DisplayName" : "地穴的远征",
	"typeid" : 7,
	"LapisId" : "28432"
}, {
	"ButtonImagePath" : "BTN_CryptCrusadeGold1",
	"DisplayName" : "黄金地穴的远征",
	"typeid" : 7,
	"LapisId" : "28436"
}, {
	"ButtonImagePath" : "BTN_Cyberstud1",
	"DisplayName" : "网络扑克",
	"typeid" : 5,
	"LapisId" : "28442"
}, {
	"ButtonImagePath" : "BTN_DawnoftheBread1",
	"DisplayName" : "黎明的面包",
	"typeid" : 7,
	"LapisId" : "28448"
}, {
	"ButtonImagePath" : "BTN_DeckTheHalls1",
	"DisplayName" : "闪亮的圣诞节 ？",
	"typeid" : 1,
	"LapisId" : "28454"
}, {
	"ButtonImagePath" : "BTN_DeucesJokerPowerPoker1",
	"DisplayName" : "百搭小丑扑克",
	"typeid" : 6,
	"LapisId" : "28460"
}, {
	"ButtonImagePath" : "BTN_DeucesJoker1",
	"DisplayName" : "百搭小丑扑克",
	"typeid" : 6,
	"LapisId" : "28466"
}, {
	"ButtonImagePath" : "BTN_DeucesWildPowerPoker1",
	"DisplayName" : "万能两点",
	"typeid" : 6,
	"LapisId" : "28470"
}, {
	"ButtonImagePath" : "BTN_DeucesWildPowerPoker1",
	"DisplayName" : "万能两点",
	"typeid" : 6,
	"LapisId" : "28472"
}, {
	"ButtonImagePath" : "BTN_DinoMight1",
	"DisplayName" : "恐龙迪诺",
	"typeid" : 1,
	"LapisId" : "28476"
}, {
	"ButtonImagePath" : "BTN_DrLove",
	"DisplayName" : "医生的爱",
	"typeid" : 1,
	"LapisId" : "28478"
}, {
	"ButtonImagePath" : "BTN_Dogfather2",
	"DisplayName" : "狗爸爸",
	"typeid" : 1,
	"LapisId" : "28480"
}, {
	"ButtonImagePath" : "BTN_DolphinQuest",
	"DisplayName" : "寻访海豚",
	"typeid" : 1,
	"LapisId" : "28484"
}, {
	"ButtonImagePath" : "BTN_DoubleBonus1",
	"DisplayName" : "双重奖金扑克",
	"typeid" : 6,
	"LapisId" : "28486"
}, {
	"ButtonImagePath" : "BTN_DoubleDoubleBonus1",
	"DisplayName" : "换牌扑克",
	"typeid" : 6,
	"LapisId" : "28492"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_DoubleExposureBlackjack1",
	"DisplayName" : "双重黄金曝光",
	"typeid" : 5,
	"LapisId" : "28494"
}, {
	"ButtonImagePath" : "BTN_DoubleJokerPowerPoker1",
	"DisplayName" : "双百搭",
	"typeid" : 6,
	"LapisId" : "28498"
}, {
	"ButtonImagePath" : "BTN_DoubleJoker1",
	"DisplayName" : "双百搭",
	"typeid" : 6,
	"LapisId" : "28506"
}, {
	"ButtonImagePath" : "BTN_DoubleMagic1",
	"DisplayName" : "双魔",
	"typeid" : 2,
	"LapisId" : "28510"
}, {
	"ButtonImagePath" : "BTN_DoubleWammy1",
	"DisplayName" : "双重韦密",
	"typeid" : 2,
	"LapisId" : "28514"
}, {
	"ButtonImagePath" : "BTN_DrWattsUp1",
	"DisplayName" : "恐怖实验室",
	"typeid" : 1,
	"LapisId" : "28522"
}, {
	"ButtonImagePath" : "BTN_DragonDance_ZH",
	"DisplayName" : "舞龙",
	"typeid" : 1,
	"LapisId" : "46494"
}, {
	"ButtonImagePath" : "BTN_DragonsFortune1",
	"DisplayName" : "龙的鸿运",
	"typeid" : 4,
	"LapisId" : "28524"
}, {
	"ButtonImagePath" : "BTN_DroneWars",
	"DisplayName" : "熊峰战争",
	"typeid" : 1,
	"LapisId" : "28542"
}, {
	"ButtonImagePath" : "BTN_EaglesWings1",
	"DisplayName" : "老鹰的翅膀",
	"typeid" : 1,
	"LapisId" : "28562"
}, {
	"ButtonImagePath" : "BTN_ElectricDiva_zh",
	"DisplayName" : "雷电歌后",
	"typeid" : 1,
	"LapisId" : "61497"
}, {
	"ButtonImagePath" : "BTN_ElectroBingo1",
	"DisplayName" : "电宾果",
	"typeid" : 7,
	"LapisId" : "28566"
}, {
	"ButtonImagePath" : "BTN_Elementals2",
	"DisplayName" : "水果怪兽",
	"typeid" : 1,
	"LapisId" : "28568"
}, {
	"ButtonImagePath" : "BTN_EnchantedWoods1",
	"DisplayName" : "魔法森林",
	"typeid" : 1,
	"LapisId" : "28572"
},
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_EuroBlackjack1",
	"DisplayName" : "欧洲黄金21点",
	"typeid" : 5,
	"LapisId" : "28576"
},**/

 {
	"ButtonImagePath" : "BTN_GoldSeries_EuroRoulette1",
	"DisplayName" : "欧式黄金轮盘",
	"typeid" : 5,
	"LapisId" : "28580"
}, {
	"ButtonImagePath" : "BTN_FantasticSevens1",
	"DisplayName" : "奇妙7",
	"typeid" : 2,
	"LapisId" : "28582"
}, {
	"ButtonImagePath" : "BTN_FatLadySings1",
	"DisplayName" : "胖女人辛斯",
	"typeid" : 1,
	"LapisId" : "28584"
}, {
	"ButtonImagePath" : "BTN_FishParty1",
	"DisplayName" : "派对鱼",
	"typeid" : 1,
	"LapisId" : "28586"
}, {
	"ButtonImagePath" : "BTN_FlyingAce2",
	"DisplayName" : "超级飞行员",
	"typeid" : 2,
	"LapisId" : "28588"
}, {
	"ButtonImagePath" : "BTN_FoamyFortunes1",
	"DisplayName" : "泡沫财富",
	"typeid" : 4,
	"LapisId" : "28590"
}, {
	"ButtonImagePath" : "BTN_footballstar1",
	"DisplayName" : "足球明星",
	"typeid" : 1,
	"LapisId" : "28592"
}, {
	"ButtonImagePath" : "BTN_FortuneCookie7",
	"DisplayName" : "幸运曲奇",
	"typeid" : 2,
	"LapisId" : "28594"
}, {
	"ButtonImagePath" : "BTN_FourByFour1",
	"DisplayName" : "四乘四",
	"typeid" : 7,
	"LapisId" : "28596"
}, {
	"ButtonImagePath" : "BTN_FreezingFuzzballs1",
	"DisplayName" : "冻结模糊球",
	"typeid" : 7,
	"LapisId" : "28598"
}, {
	"ButtonImagePath" : "BTN_FrenchRoulette1",
	"DisplayName" : "法式轮盘",
	"typeid" : 5,
	"LapisId" : "28602"
}, {
	"ButtonImagePath" : "BTN_FrozenDiamonds",
	"DisplayName" : "急冻钻石",
	"typeid" : 1,
	"LapisId" : "66080"
}, {
	"ButtonImagePath" : "BTN_FruitSlots1",
	"DisplayName" : "水果老虎机",
	"typeid" : 2,
	"LapisId" : "28604"
}, {
	"ButtonImagePath" : "BTN_Galacticons",
	"DisplayName" : "银河舰队",
	"typeid" : 1,
	"LapisId" : "28606"
}, {
	"ButtonImagePath" : "BTN_GameSetandScratch1",
	"DisplayName" : "网球最终局",
	"typeid" : 4,
	"LapisId" : "28616"
}, {
	"ButtonImagePath" : "BTN_GeniesGems2",
	"DisplayName" : "精灵宝石",
	"typeid" : 1,
	"LapisId" : "28608"
}, {
	"ButtonImagePath" : "BTN_Germinator1",
	"DisplayName" : "细菌对对碰",
	"typeid" : 7,
	"LapisId" : "28614"
}, {
	"ButtonImagePath" : "BTN_GiftRap2",
	"DisplayName" : "礼品包装",
	"typeid" : 1,
	"LapisId" : "28618"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns2",
	"DisplayName" : "女孩与枪II",
	"typeid" : 1,
	"LapisId" : "29449"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns1",
	"DisplayName" : "女孩与枪",
	"typeid" : 1,
	"LapisId" : "28622"
}, {
	"ButtonImagePath" : "BTN_GoldCoast3",
	"DisplayName" : "黄金海岸",
	"typeid" : 2,
	"LapisId" : "28624"
}, {
	"ButtonImagePath" : "BTN_GoldFactory",
	"DisplayName" : "黄金工厂",
	"typeid" : 1,
	"LapisId" : "28626"
}, {
	"ButtonImagePath" : "BTN_GoldenDragon6",
	"DisplayName" : "黄金龙",
	"typeid" : 2,
	"LapisId" : "28628"
}, {
	"ButtonImagePath" : "BTN_GoldenEra",
	"DisplayName" : "黄金时代",
	"typeid" : 1,
	"LapisId" : "28630"
}, {
	"ButtonImagePath" : "BTN_GoldenGhouls1",
	"DisplayName" : "黄金食尸鬼",
	"typeid" : 4,
	"LapisId" : "28632"
}, {
	"ButtonImagePath" : "BTN_GoldenPrincess",
	"DisplayName" : "黄金公主",
	"typeid" : 1,
	"LapisId" : "28634"
}, {
	"ButtonImagePath" : "BTN_GoodToGo2",
	"DisplayName" : "疯狂赛道",
	"typeid" : 1,
	"LapisId" : "28636"
}, {
	"ButtonImagePath" : "BTN_GopherGold2",
	"DisplayName" : "黄金囊地鼠",
	"typeid" : 1,
	"LapisId" : "28638"
}, {
	"ButtonImagePath" : "BTN_GrannyPrix1",
	"DisplayName" : "老太太赛车",
	"typeid" : 7,
	"LapisId" : "28640"
}, {
	"ButtonImagePath" : "BTN_GreatGriffin1",
	"DisplayName" : "大狮鹫",
	"typeid" : 1,
	"LapisId" : "28642"
}, {
	"ButtonImagePath" : "BTN_HairyFairies1",
	"DisplayName" : "毛茸茸的仙女",
	"typeid" : 7,
	"LapisId" : "28646"
}, {
	"ButtonImagePath" : "BTN_Halloweenies1",
	"DisplayName" : "万圣节",
	"typeid" : 1,
	"LapisId" : "28648"
}, {
	"ButtonImagePath" : "BTN_Halloweenies1",
	"DisplayName" : "万圣节",
	"typeid" : 7,
	"LapisId" : "28650"
}, {
	"ButtonImagePath" : "BTN_HandToHandCombat1",
	"DisplayName" : "肉搏战",
	"typeid" : 4,
	"LapisId" : "28488"
}, {
	"ButtonImagePath" : "BTN_HappyHolidays_ZH",
	"DisplayName" : "快乐假日",
	"typeid" : 1,
	"LapisId" : "45397"
}, {
	"ButtonImagePath" : "BTN_HappyNewYear3",
	"DisplayName" : "新年快乐",
	"typeid" : 2,
	"LapisId" : "28490"
}, {
	"ButtonImagePath" : "BTN_Harveys1",
	"DisplayName" : "哈维斯的晚餐",
	"typeid" : 1,
	"LapisId" : "28664"
}, {
	"ButtonImagePath" : "BTN_HellBoy1",
	"DisplayName" : "地狱男爵",
	"typeid" : 1,
	"LapisId" : "28500"
}, {
	"ButtonImagePath" : "BTN_HellGrannies",
	"DisplayName" : "地狱阿嬷",
	"typeid" : 1,
	"LapisId" : "28502"
}, {
	"ButtonImagePath" : "BTN_Hexaline1",
	"DisplayName" : "六线",
	"typeid" : 7,
	"LapisId" : "28504"
}, {
	"ButtonImagePath" : "BTN_HighLimitBaccarat1",
	"DisplayName" : "高限制百家乐",
	"typeid" : 5,
	"LapisId" : "28508"
}, {
	"ButtonImagePath" : "BTN_EuroBlackjackHighLimit1",
	"DisplayName" : "欧式高限21点",
	"typeid" : 5,
	"LapisId" : "28668"
}, {
	"ButtonImagePath" : "BTN_HighSociety",
	"DisplayName" : "上流社会",
	"typeid" : 1,
	"LapisId" : "28516"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHHighSpeedPoker1",
	"DisplayName" : "高速扑克",
	"typeid" : 5,
	"LapisId" : "28518"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_HiLo13EuroBJ1",
	"DisplayName" : "13欧洲 21点黃金桌",
	"typeid" : 5,
	"LapisId" : "28520"
}, {
	"ButtonImagePath" : "BTN_Hitman1",
	"DisplayName" : "终极杀手",
	"typeid" : 1,
	"LapisId" : "28670"
}, {
	"ButtonImagePath" : "BTN_Hohoho1",
	"DisplayName" : "嗬嗬嗬",
	"typeid" : 1,
	"LapisId" : "28674"
}, {
	"ButtonImagePath" : "BTN_HotAsHades_ZH",
	"DisplayName" : "地府烈焰",
	"typeid" : 1,
	"LapisId" : "28574"
}, {
	"ButtonImagePath" : "BTN_HotInk1",
	"DisplayName" : "神奇墨水",
	"typeid" : 1,
	"LapisId" : "28540"
}, {
	"ButtonImagePath" : "BTN_HotShot3",
	"DisplayName" : "棒球直击",
	"typeid" : 1,
	"LapisId" : "28534"
}, {
	"ButtonImagePath" : "BTN_HoundHotel_ZH",
	"DisplayName" : "酷犬酒店",
	"typeid" : 1,
	"LapisId" : "28644"
}, {
	"ButtonImagePath" : "BTN_HouseofDragons",
	"DisplayName" : "龙之家",
	"typeid" : 1,
	"LapisId" : "28680"
}, {
	"ButtonImagePath" : "BTN_ImmortalRomance1",
	"DisplayName" : "不朽的爱情",
	"typeid" : 1,
	"LapisId" : "28548"
}, {
	"ButtonImagePath" : "BTN_InstantWinCardSelector1",
	"DisplayName" : "刮刮卡20合一",
	"typeid" : 1,
	"LapisId" : "29450"
}, {
	"ButtonImagePath" : "BTN_IrishEyes",
	"DisplayName" : "爱尔兰眼睛",
	"typeid" : 1,
	"LapisId" : "28552"
}, {
	"ButtonImagePath" : "BTN_JackpotExpress1",
	"DisplayName" : "累计奖金快车",
	"typeid" : 2,
	"LapisId" : "28558"
}, {
	"ButtonImagePath" : "BTN_JacksorBetterPowerPoker1",
	"DisplayName" : "千斤顶或更好",
	"typeid" : 6,
	"LapisId" : "28686"
}, {
	"ButtonImagePath" : "BTN_JacksOrBetter1",
	"DisplayName" : "千斤顶或更好",
	"typeid" : 6,
	"LapisId" : "28694"
}, {
	"ButtonImagePath" : "BTN_JekyllandHyde",
	"DisplayName" : "判若两人",
	"typeid" : 1,
	"LapisId" : "28660"
}, {
	"ButtonImagePath" : "BTN_JewelsoftheOrient1",
	"DisplayName" : "东方珠宝",
	"typeid" : 1,
	"LapisId" : "28652"
}, {
	"ButtonImagePath" : "BTN_JingleBells6",
	"DisplayName" : "铃儿响叮当",
	"typeid" : 2,
	"LapisId" : "28654"
}, {
	"ButtonImagePath" : "BTN_JokerPokerPowerPoker1",
	"DisplayName" : "小丑扑克",
	"typeid" : 6,
	"LapisId" : "28710"
}, {
	"ButtonImagePath" : "BTN_JokerPoker1",
	"DisplayName" : "小丑扑克",
	"typeid" : 6,
	"LapisId" : "28720"
}, {
	"ButtonImagePath" : "BTN_JurassicJackpot1",
	"DisplayName" : "侏罗纪大奖",
	"typeid" : 2,
	"LapisId" : "28724"
}, {
	"ButtonImagePath" : "BTN_KaraokeParty_ZH",
	"DisplayName" : "K歌乐韵",
	"typeid" : 1,
	"LapisId" : "65978"
}, {
	"ButtonImagePath" : "BTN_KaratePig1",
	"DisplayName" : "功夫小胖猪",
	"typeid" : 1,
	"LapisId" : "28726"
}, {
	"ButtonImagePath" : "BTN_Kathmandu1",
	"DisplayName" : "卡萨缦都",
	"typeid" : 1,
	"LapisId" : "28734"
}, {
	"ButtonImagePath" : "BTN_Keno3",
	"DisplayName" : "基诺",
	"typeid" : 7,
	"LapisId" : "28740"
}, {
	"ButtonImagePath" : "BTN_KingArthur",
	"DisplayName" : "亚瑟王",
	"typeid" : 1,
	"LapisId" : "28744"
}, {
	"ButtonImagePath" : "BTN_KingsofCash1",
	"DisplayName" : "现金之王",
	"typeid" : 1,
	"LapisId" : "28762"
}, {
	"ButtonImagePath" : "BTN_KittyCabana_ZH",
	"DisplayName" : "凯蒂卡巴拉",
	"typeid" : 1,
	"LapisId" : "28772"
}, {
	"ButtonImagePath" : "BTN_LadiesNite5",
	"DisplayName" : "女仕之夜",
	"typeid" : 1,
	"LapisId" : "28776"
}, {
	"ButtonImagePath" : "BTN_LadyInRed2",
	"DisplayName" : "红衣女郎",
	"typeid" : 1,
	"LapisId" : "28812"
}, {
	"ButtonImagePath" : "BTN_LeaguesOfFortune1",
	"DisplayName" : "财富阶级",
	"typeid" : 1,
	"LapisId" : "28780"
}, {
	"ButtonImagePath" : "BTN_Legacy2",
	"DisplayName" : "遗产L",
	"typeid" : 2,
	"LapisId" : "28786"
}, {
	"ButtonImagePath" : "BTN_Loaded1",
	"DisplayName" : "炫富一族",
	"typeid" : 1,
	"LapisId" : "28790"
}, {
	"ButtonImagePath" : "BTN_LooseCannon1",
	"DisplayName" : "海盗王",
	"typeid" : 1,
	"LapisId" : "28796"
}, {
	"ButtonImagePath" : "BTN_LouisianaDouble1",
	"DisplayName" : "路易斯安那双",
	"typeid" : 6,
	"LapisId" : "28814"
}, {
	"ButtonImagePath" : "BTN_LuckyFirecracker",
	"DisplayName" : "招财鞭炮",
	"typeid" : 1,
	"LapisId" : "28800"
}, {
	"ButtonImagePath" : "BTN_LuckyKoi",
	"DisplayName" : "幸运的锦鲤",
	"typeid" : 1,
	"LapisId" : "28794"
}, {
	"ButtonImagePath" : "BTN_LuckyLeprechaun",
	"DisplayName" : "幸运的小妖精",
	"typeid" : 1,
	"LapisId" : "28788"
}, {
	"ButtonImagePath" : "BTN_LuckyLeprechaunsLoot",
	"DisplayName" : "妖精的战利品",
	"typeid" : 1,
	"LapisId" : "28782"
}, {
	"ButtonImagePath" : "BTN_LuckyNumbers1",
	"DisplayName" : "幸运数字",
	"typeid" : 4,
	"LapisId" : "28778"
}, {
	"ButtonImagePath" : "BTN_luckyTwins_ZH",
	"DisplayName" : "幸运双星",
	"typeid" : 1,
	"LapisId" : "42981"
}, {
	"ButtonImagePath" : "BTN_LuckyWitch1",
	"DisplayName" : "幸运女巫",
	"typeid" : 1,
	"LapisId" : "28774"
}, {
	"ButtonImagePath" : "BTN_LuckyZodiac_ZH",
	"DisplayName" : "幸运生肖",
	"typeid" : 1,
	"LapisId" : "28770"
}, {
	"ButtonImagePath" : "BTN_MadHatters1",
	"DisplayName" : "疯狂的帽子",
	"typeid" : 1,
	"LapisId" : "28766"
}, {
	"ButtonImagePath" : "BTN_MaxDamageSlot",
	"DisplayName" : "终极破坏",
	"typeid" : 1,
	"LapisId" : "28758"
}, {
	"ButtonImagePath" : "BTN_MaxDamage1",
	"DisplayName" : "星战传奇",
	"typeid" : 7,
	"LapisId" : "28756"
}, {
	"ButtonImagePath" : "BTN_MayanBingo1",
	"DisplayName" : "玛雅宾果",
	"typeid" : 7,
	"LapisId" : "28748"
}, {
	"ButtonImagePath" : "BTN_MayanPrincess1",
	"DisplayName" : "玛雅公主",
	"typeid" : 1,
	"LapisId" : "28738"
}, {
	"ButtonImagePath" : "BTN_BreakdaBankAgainMegaSpin1",
	"DisplayName" : "银行爆破",
	"typeid" : 1,
	"LapisId" : "28706"
}, {
	"ButtonImagePath" : "BTN_MermaidsMillions1",
	"DisplayName" : "海底世界",
	"typeid" : 1,
	"LapisId" : "28716"
}, {
	"ButtonImagePath" : "BTN_MoneyMadMonkey1",
	"DisplayName" : "疯狂的猴子",
	"typeid" : 1,
	"LapisId" : "28712"
}, {
	"ButtonImagePath" : "BTN_MonkeyKeno_ZH",
	"DisplayName" : "猴子基诺",
	"typeid" : 7,
	"LapisId" : "66084"
}, {
	"ButtonImagePath" : "BTN_MonsterMania1",
	"DisplayName" : "怪物躁狂症",
	"typeid" : 1,
	"LapisId" : "28696"
}, {
	"ButtonImagePath" : "BTN_Moonshine1",
	"DisplayName" : "月光",
	"typeid" : 1,
	"LapisId" : "28708"
}, {
	"ButtonImagePath" : "BTN_MountOlympus",
	"DisplayName" : "奥林匹斯山",
	"typeid" : 1,
	"LapisId" : "28676"
}, {
	"ButtonImagePath" : "BTN_MugshotMadness1",
	"DisplayName" : "疯狂假面",
	"typeid" : 1,
	"LapisId" : "28666"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MultiWheelRoulette1",
	"DisplayName" : "复式黄金轮盘",
	"typeid" : 5,
	"LapisId" : "28816"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHEuroBlackjack2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28820"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHPerfectPairsBlackjack1",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28822"
}, {
	"ButtonImagePath" : "BTN_MHPremierBlackjack1",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28678"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHVegasDowntown2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28826"
}, {
	"ButtonImagePath" : "BTN_MumbaiMagic1",
	"DisplayName" : "孟买魔术",
	"typeid" : 4,
	"LapisId" : "28612"
}, {
	"ButtonImagePath" : "BTN_Munchkins2",
	"DisplayName" : "怪兽曼琪肯",
	"typeid" : 1,
	"LapisId" : "28610"
}, {
	"ButtonImagePath" : "BTN_MysticDreams1",
	"DisplayName" : "神秘梦境",
	"typeid" : 1,
	"LapisId" : "28600"
}, {
	"ButtonImagePath" : "BTN_MystiqueGrove",
	"DisplayName" : "神秘森林",
	"typeid" : 1,
	"LapisId" : "28578"
}, {
	"ButtonImagePath" : "BTN_NinjaMagic_zh",
	"DisplayName" : "忍者法宝",
	"typeid" : 1,
	"LapisId" : "61149"
}, {
	"ButtonImagePath" : "BTN_Octopays",
	"DisplayName" : "章鱼",
	"typeid" : 1,
	"LapisId" : "28828"
}, {
	"ButtonImagePath" : "BTN_OffsideandSeek1",
	"DisplayName" : "临门一脚",
	"typeid" : 4,
	"LapisId" : "28658"
}, {
	"ButtonImagePath" : "BTN_OrientalFortune2",
	"DisplayName" : "东方财富",
	"typeid" : 1,
	"LapisId" : "28564"
}, {
	"ButtonImagePath" : "BTN_ParadiseFound",
	"DisplayName" : "发现天堂",
	"typeid" : 1,
	"LapisId" : "28560"
}, {
	"ButtonImagePath" : "BTN_PeekABoo_ZH",
	"DisplayName" : "躲猫猫",
	"typeid" : 1,
	"LapisId" : "42979"
}, {
	"ButtonImagePath" : "BTN_PhantomCash",
	"DisplayName" : "幻影现金",
	"typeid" : 1,
	"LapisId" : "28570"
}, {
	"ButtonImagePath" : "BTN_PharaohBingo1",
	"DisplayName" : "法老宾果",
	"typeid" : 7,
	"LapisId" : "28550"
}, {
	"ButtonImagePath" : "BTN_PharoahsGems1",
	"DisplayName" : "隔离的宝石",
	"typeid" : 4,
	"LapisId" : "28842"
}, {
	"ButtonImagePath" : "BTN_PiggyFortunes",
	"DisplayName" : "小猪财富",
	"typeid" : 1,
	"LapisId" : "28556"
}, {
	"ButtonImagePath" : "BTN_Pistoleras_ZH",
	"DisplayName" : "持枪王者",
	"typeid" : 1,
	"LapisId" : "28546"
}, {
	"ButtonImagePath" : "BTN_Playboy1",
	"DisplayName" : "花花公子",
	"typeid" : 1,
	"LapisId" : "28858"
}, {
	"ButtonImagePath" : "BTN_PlundertheSea1",
	"DisplayName" : "掠夺之海",
	"typeid" : 4,
	"LapisId" : "28544"
}, {
	"ButtonImagePath" : "BTN_PokerPursuit1",
	"DisplayName" : "扑克追求",
	"typeid" : 6,
	"LapisId" : "28874"
}, {
	"ButtonImagePath" : "BTN_PollenNation1",
	"DisplayName" : "蜜蜂乐园",
	"typeid" : 1,
	"LapisId" : "28890"
}, {
	"ButtonImagePath" : "BTN_PremierBlackjackHiLo1",
	"DisplayName" : "21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28538"
}, {
	"ButtonImagePath" : "BTN_PremierBlackjackHiStreak1",
	"DisplayName" : "21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28536"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHBonusBlackjack2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28532"
}, {
	"ButtonImagePath" : "BTN_PremierRacing1",
	"DisplayName" : "超级赛马",
	"typeid" : 7,
	"LapisId" : "28530"
}, {
	"ButtonImagePath" : "BTN_PremierRoulette9",
	"DisplayName" : "轮盘",
	"typeid" : 5,
	"LapisId" : "28528"
}, {
	"ButtonImagePath" : "BTN_PremierRouletteDE1",
	"DisplayName" : "轮盘钻石版",
	"typeid" : 5,
	"LapisId" : "28526"
}, {
	"ButtonImagePath" : "BTN_PremierTrotting1",
	"DisplayName" : "超级马车赛",
	"typeid" : 7,
	"LapisId" : "28904"
}, {
	"ButtonImagePath" : "BTN_prettykitty_zh",
	"DisplayName" : "漂亮猫咪",
	"typeid" : 1,
	"LapisId" : "61147"
}, {
	"ButtonImagePath" : "BTN_PrimeProperty1",
	"DisplayName" : "优质物业",
	"typeid" : 1,
	"LapisId" : "28910"
}, {
	"ButtonImagePath" : "BTN_PurePlatinum1",
	"DisplayName" : "纯铂",
	"typeid" : 1,
	"LapisId" : "28512"
}, {
	"ButtonImagePath" : "BTN_RabbitintheHat",
	"DisplayName" : "帽子里的兔子",
	"typeid" : 1,
	"LapisId" : "28912"
}, {
	"ButtonImagePath" : "BTN_RacingForPinks",
	"DisplayName" : "为粉红而战",
	"typeid" : 1,
	"LapisId" : "28496"
}, {
	"ButtonImagePath" : "BTN_RammessesRiches",
	"DisplayName" : "拉美西斯的财富",
	"typeid" : 1,
	"LapisId" : "28916"
}, {
	"ButtonImagePath" : "BTN_RapidReels5",
	"DisplayName" : "急速转轮",
	"typeid" : 2,
	"LapisId" : "28482"
}, {
	"ButtonImagePath" : "BTN_RedHotDevil",
	"DisplayName" : "红唇诱惑",
	"typeid" : 1,
	"LapisId" : "28474"
}, {
	"ButtonImagePath" : "BTN_ReelGems1",
	"DisplayName" : "宝石迷阵",
	"typeid" : 1,
	"LapisId" : "28468"
}, {
	"ButtonImagePath" : "BTN_ReelSpinner_zh",
	"DisplayName" : "旋转大战",
	"typeid" : 1,
	"LapisId" : "61495"
}, {
	"ButtonImagePath" : "BTN_ReelStrike1",
	"DisplayName" : "卷行使价",
	"typeid" : 1,
	"LapisId" : "28464"
}, {
	"ButtonImagePath" : "BTN_ReelThunder2",
	"DisplayName" : "雷电击",
	"typeid" : 1,
	"LapisId" : "28462"
}, {
	"ButtonImagePath" : "BTN_RetroReels1",
	"DisplayName" : "复古旋转",
	"typeid" : 1,
	"LapisId" : "28458"
}, {
	"ButtonImagePath" : "BTN_RRExtreme1",
	"DisplayName" : "复古卷轴 - 极热",
	"typeid" : 1,
	"LapisId" : "28456"
}, {
	"ButtonImagePath" : "BTN_RRDiamondGlitz1",
	"DisplayName" : "复古卷轴钻石耀眼",
	"typeid" : 1,
	"LapisId" : "28452"
}, {
	"ButtonImagePath" : "BTN_RRHearts&Tarts1",
	"DisplayName" : "押韵的卷轴 - 心挞",
	"typeid" : 1,
	"LapisId" : "28450"
}, {
	"ButtonImagePath" : "BTN_RRKing1",
	"DisplayName" : "押韵的卷轴 - 老国王科尔",
	"typeid" : 1,
	"LapisId" : "28446"
}, {
	"ButtonImagePath" : "BTN_RivieraRiches1",
	"DisplayName" : "海滨财富",
	"typeid" : 1,
	"LapisId" : "28444"
}, {
	"ButtonImagePath" : "BTN_robojack1",
	"DisplayName" : "洛伯杰克",
	"typeid" : 1,
	"LapisId" : "28440"
}, {
	"ButtonImagePath" : "BTN_RocktheBoat18",
	"DisplayName" : "摇滚船",
	"typeid" : 2,
	"LapisId" : "28438"
}, {
	"ButtonImagePath" : "BTN_RollerDerby",
	"DisplayName" : "滚德比",
	"typeid" : 1,
	"LapisId" : "28434"
}, {
	"ButtonImagePath" : "BTN_RomanRiches11",
	"DisplayName" : "罗马财富",
	"typeid" : 2,
	"LapisId" : "28430"
}, {
	"ButtonImagePath" : "BTN_RugbyStar_ZH",
	"DisplayName" : "橄榄球明星",
	"typeid" : 1,
	"LapisId" : "28426"
}, {
	"ButtonImagePath" : "BTN_SambaBingo1",
	"DisplayName" : "萨巴宾果",
	"typeid" : 7,
	"LapisId" : "28420"
}, {
	"ButtonImagePath" : "BTN_SantasWildRide1",
	"DisplayName" : "圣诞老人的疯狂 ？",
	"typeid" : 1,
	"LapisId" : "28416"
}, {
	"ButtonImagePath" : "BTN_Scrooge1",
	"DisplayName" : "守财奴",
	"typeid" : 1,
	"LapisId" : "28412"
}, {
	"ButtonImagePath" : "BTN_SecretAdmirer1",
	"DisplayName" : "秘密崇拜者",
	"typeid" : 1,
	"LapisId" : "28410"
}, {
	"ButtonImagePath" : "BTN_SecretSanta",
	"DisplayName" : "圣诞老人的秘密",
	"typeid" : 1,
	"LapisId" : "28406"
}, {
	"ButtonImagePath" : "BTN_Serenity_ZH",
	"DisplayName" : "宁静",
	"typeid" : 1,
	"LapisId" : "45399"
}, {
	"ButtonImagePath" : "BTN_Shoot",
	"DisplayName" : "射击",
	"typeid" : 1,
	"LapisId" : "28402"
}, {
	"ButtonImagePath" : "BTN_SilverFang1",
	"DisplayName" : "银芳",
	"typeid" : 1,
	"LapisId" : "28396"
}, {
	"ButtonImagePath" : "BTN_SixShooterLooterGold1",
	"DisplayName" : "六位枪手掠夺者黄金版",
	"typeid" : 4,
	"LapisId" : "28394"
}, {
	"ButtonImagePath" : "BTN_SkullDuggery1",
	"DisplayName" : "骷髅陷阱",
	"typeid" : 1,
	"LapisId" : "28392"
}, {
	"ButtonImagePath" : "BTN_SlamFunk1",
	"DisplayName" : "猛撞恐惧",
	"typeid" : 4,
	"LapisId" : "28390"
}, {
	"ButtonImagePath" : "BTN_somanymonsters",
	"DisplayName" : "怪兽多多",
	"typeid" : 1,
	"LapisId" : "28388"
}, {
	"ButtonImagePath" : "BTN_somuchcandy",
	"DisplayName" : "糖果多多",
	"typeid" : 1,
	"LapisId" : "28386"
}, {
	"ButtonImagePath" : "BTN_somuchsushi",
	"DisplayName" : "寿司多多",
	"typeid" : 1,
	"LapisId" : "28382"
}, {
	"ButtonImagePath" : "BTN_SoccerSafari1",
	"DisplayName" : "动物足球",
	"typeid" : 1,
	"LapisId" : "28378"
}, {
	"ButtonImagePath" : "BTN_SpaceEvader1",
	"DisplayName" : "太空逃避物",
	"typeid" : 4,
	"LapisId" : "28370"
}, {
	"ButtonImagePath" : "BTN_SpaceEvaderGold1",
	"DisplayName" : "太空逃避物黄金版",
	"typeid" : 4,
	"LapisId" : "28366"
}, {
	"ButtonImagePath" : "BTN_SpanishBlackjack1",
	"DisplayName" : "西班牙21点",
	"typeid" : 5,
	"LapisId" : "28362"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_SpanishBlackjack1",
	"DisplayName" : "西班牙21点",
	"typeid" : 5,
	"LapisId" : "28360"
}, {
	"ButtonImagePath" : "BTN_SpectacularWheelOfWealth1",
	"DisplayName" : "财富之轮",
	"typeid" : 2,
	"LapisId" : "28356"
}, {
	"ButtonImagePath" : "BTN_Spingo",
	"DisplayName" : "我推",
	"typeid" : 7,
	"LapisId" : "28352"
}, {
	"ButtonImagePath" : "BTN_SpringBreak2",
	"DisplayName" : "春假",
	"typeid" : 1,
	"LapisId" : "28348"
}, {
	"ButtonImagePath" : "BTN_StarDust_ZH",
	"DisplayName" : "星尘",
	"typeid" : 1,
	"LapisId" : "45401"
}, {
	"ButtonImagePath" : "BTN_StarlightKiss",
	"DisplayName" : "星梦之吻",
	"typeid" : 1,
	"LapisId" : "28344"
}, {
	"ButtonImagePath" : "BTN_StarScape1",
	"DisplayName" : "星云",
	"typeid" : 1,
	"LapisId" : "28342"
}, {
	"ButtonImagePath" : "BTN_StashoftheTitans1",
	"DisplayName" : "泰坦之藏匿",
	"typeid" : 1,
	"LapisId" : "28340"
}, {
	"ButtonImagePath" : "BTN_SteamPunkHeroes",
	"DisplayName" : "蒸汽朋克英雄",
	"typeid" : 1,
	"LapisId" : "28338"
}, {
	"ButtonImagePath" : "BTN_SterlingSilver3D1",
	"DisplayName" : "纯银3D",
	"typeid" : 1,
	"LapisId" : "28334"
}, {
	"ButtonImagePath" : "BTN_SunQuest3",
	"DisplayName" : "探索太阳",
	"typeid" : 1,
	"LapisId" : "28328"
}, {
	"ButtonImagePath" : "BTN_SunTide_Button_ZH",
	"DisplayName" : "太阳征程",
	"typeid" : 1,
	"LapisId" : "50193"
}, {
	"ButtonImagePath" : "BTN_SupeItUp2",
	"DisplayName" : "跑起来",
	"typeid" : 1,
	"LapisId" : "28326"
}, {
	"ButtonImagePath" : "BTN_SuperBonusBingo1",
	"DisplayName" : "超级奖金宾果",
	"typeid" : 7,
	"LapisId" : "28322"
}, {
	"ButtonImagePath" : "BTN_SuperFun21Blackjack1",
	"DisplayName" : "超级有趣21",
	"typeid" : 5,
	"LapisId" : "28320"
}, {
	"ButtonImagePath" : "BTN_SuperZeroes1",
	"DisplayName" : "超级零点",
	"typeid" : 7,
	"LapisId" : "28318"
}, {
	"ButtonImagePath" : "BTN_SurfSafari",
	"DisplayName" : "动物冲浪",
	"typeid" : 1,
	"LapisId" : "28314"
}, {
	"ButtonImagePath" : "BTN_SweetHarvest1",
	"DisplayName" : "甜蜜的收获",
	"typeid" : 1,
	"LapisId" : "28312"
}, {
	"ButtonImagePath" : "BTN_TallyHo1",
	"DisplayName" : "泰利嗬",
	"typeid" : 1,
	"LapisId" : "28308"
}, {
	"ButtonImagePath" : "BTN_TensOrBetter1",
	"DisplayName" : "数万或更好",
	"typeid" : 6,
	"LapisId" : "28302"
}, {
	"ButtonImagePath" : "BTN_TensOrBetterPowerPoker1",
	"DisplayName" : "数万或更好",
	"typeid" : 6,
	"LapisId" : "28304"
}, {
	"ButtonImagePath" : "BTN_Terminator2_1",
	"DisplayName" : "终结者2",
	"typeid" : 1,
	"LapisId" : "28300"
}, {
	"ButtonImagePath" : "BTN_FinerReelsofLife",
	"DisplayName" : "好日子",
	"typeid" : 1,
	"LapisId" : "28294"
}, {
	"ButtonImagePath" : "BTN_TheLostPrincessAnastasia",
	"DisplayName" : "失落的阿纳斯塔西娅公主",
	"typeid" : 1,
	"LapisId" : "28246"
}, {
	"ButtonImagePath" : "BTN_RatPack1",
	"DisplayName" : "鼠包",
	"typeid" : 1,
	"LapisId" : "28238"
}, {
	"ButtonImagePath" : "BTN_TwistedCircus",
	"DisplayName" : "反转马戏团",
	"typeid" : 1,
	"LapisId" : "28232"
}, {
	"ButtonImagePath" : "BTN_ThroneOfEgypt1",
	"DisplayName" : "埃及王座",
	"typeid" : 1,
	"LapisId" : "28230"
}, {
	"ButtonImagePath" : "BTN_Thunderstruck1",
	"DisplayName" : "雷神",
	"typeid" : 1,
	"LapisId" : "28228"
}, {
	"ButtonImagePath" : "BTN_ThunderstruckTwo1",
	"DisplayName" : "雷神2",
	"typeid" : 1,
	"LapisId" : "28226"
}, {
	"ButtonImagePath" : "BTN_TigerVsBear",
	"DisplayName" : "熊虎之战",
	"typeid" : 1,
	"LapisId" : "28222"
}, {
	"ButtonImagePath" : "BTN_TitansOfTheSun_Hyperion_ZH",
	"DisplayName" : "太阳神之许珀里翁",
	"typeid" : 1,
	"LapisId" : "28112"
}, {
	"ButtonImagePath" : "BTN_TitansOfTheSun_Theia_ZH",
	"DisplayName" : "太阳神之忒伊亚",
	"typeid" : 1,
	"LapisId" : "28220"
}, {
	"ButtonImagePath" : "BTN_TombRaider2",
	"DisplayName" : "古墓丽影",
	"typeid" : 1,
	"LapisId" : "28218"
}, {
	"ButtonImagePath" : "BTN_TombRaiderSotS1",
	"DisplayName" : "古墓丽影2",
	"typeid" : 1,
	"LapisId" : "28216"
}, {
	"ButtonImagePath" : "BTN_TotemTreasure1",
	"DisplayName" : "图腾宝藏",
	"typeid" : 1,
	"LapisId" : "28214"
}, {
	"ButtonImagePath" : "BTN_Triangulation",
	"DisplayName" : "三角",
	"typeid" : 7,
	"LapisId" : "28212"
}, {
	"ButtonImagePath" : "BTN_TripleMagic1",
	"DisplayName" : "三魔法",
	"typeid" : 2,
	"LapisId" : "28210"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_TriplePocket1",
	"DisplayName" : "三人德州扑克",
	"typeid" : 5,
	"LapisId" : "28208"
}, {
	"ButtonImagePath" : "BTN_TurtleyAwesome1",
	"DisplayName" : "棒棒乌龟",
	"typeid" : 4,
	"LapisId" : "28206"
}, {
	"ButtonImagePath" : "BTN_UntamedBengalTiger1",
	"DisplayName" : "野性的孟加拉虎",
	"typeid" : 1,
	"LapisId" : "28202"
}, {
	"ButtonImagePath" : "BTN_UntamedCrownedEagle",
	"DisplayName" : "狂野之鹰",
	"typeid" : 1,
	"LapisId" : "28200"
}, {
	"ButtonImagePath" : "BTN_UntamedGiantPanda",
	"DisplayName" : "大熊猫",
	"typeid" : 1,
	"LapisId" : "28196"
}, {
	"ButtonImagePath" : "BTN_UntamedWolfPack1",
	"DisplayName" : "野性的狼群",
	"typeid" : 1,
	"LapisId" : "28190"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_VegasSingleDeck1",
	"DisplayName" : "拉斯维加斯21点单人黄金桌",
	"typeid" : 5,
	"LapisId" : "28186"
}, {
	"ButtonImagePath" : "BTN_VegasStripBlackjack1",
	"DisplayName" : "拉斯维加斯21点",
	"typeid" : 5,
	"LapisId" : "28180"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_VegasStrip1",
	"DisplayName" : "拉斯维加斯21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28184"
}, {
	"ButtonImagePath" : "BTN_VictorianVillian",
	"DisplayName" : "维多利亚的恶棍",
	"typeid" : 1,
	"LapisId" : "28176"
}, {
	"ButtonImagePath" : "BTN_VinylCountdown4",
	"DisplayName" : "乙烯基倒计时",
	"typeid" : 1,
	"LapisId" : "28172"
}, {
	"ButtonImagePath" : "BTN_WasabiSan4",
	"DisplayName" : "芥末寿司",
	"typeid" : 1,
	"LapisId" : "28168"
}, {
	"ButtonImagePath" : "BTN_WhackaJackpot1",
	"DisplayName" : "瓜分大奖",
	"typeid" : 4,
	"LapisId" : "28158"
}, {
	"ButtonImagePath" : "BTN_WhataHoot3",
	"DisplayName" : "猫头鹰乐园",
	"typeid" : 1,
	"LapisId" : "28178"
}, {
	"ButtonImagePath" : "BTN_WhatonEarth1",
	"DisplayName" : "地球生物",
	"typeid" : 1,
	"LapisId" : "28154"
}, {
	"ButtonImagePath" : "BTN_WheelOfWealth1",
	"DisplayName" : "水果财富",
	"typeid" : 2,
	"LapisId" : "28156"
}, {
	"ButtonImagePath" : "BTN_WheelofWealthSE1",
	"DisplayName" : "财富转轮特别版",
	"typeid" : 1,
	"LapisId" : "28146"
}, {
	"ButtonImagePath" : "BTN_WhiteBuffalo",
	"DisplayName" : "白水牛",
	"typeid" : 1,
	"LapisId" : "28140"
}, {
	"ButtonImagePath" : "BTN_WildCatch",
	"DisplayName" : "野生捕鱼",
	"typeid" : 1,
	"LapisId" : "28142"
}, {
	"ButtonImagePath" : "BTN_WildChampions1",
	"DisplayName" : "野生冠军",
	"typeid" : 4,
	"LapisId" : "28132"
}, {
	"ButtonImagePath" : "BTN_WildOrient_Button_ZH",
	"DisplayName" : "东方珍兽",
	"typeid" : 1,
	"LapisId" : "50194"
}, {
	"ButtonImagePath" : "BTN_WinSpinner1",
	"DisplayName" : "钱来运转",
	"typeid" : 2,
	"LapisId" : "29573"
}, {
	"ButtonImagePath" : "BTN_winsumdimsum_zh",
	"DisplayName" : "开心点心",
	"typeid" : 1,
	"LapisId" : "61499"
}, {
	"ButtonImagePath" : "BTN_WinningWizards1",
	"DisplayName" : "赢得向导",
	"typeid" : 1,
	"LapisId" : "28130"
}, {
	"ButtonImagePath" : "BTN_WitchesWealth1",
	"DisplayName" : "女巫的财富",
	"typeid" : 1,
	"LapisId" : "28126"
}, {
	"ButtonImagePath" : "BTN_ZanyZebra1",
	"DisplayName" : "燃尼巨蟒",
	"typeid" : 2,
	"LapisId" : "28124"
} ];


var mghotdata = [  
{
	"ButtonImagePath" : "BTN_GameSetandScratch1",
	"DisplayName" : "网球最终局",
	"typeid" : 4,
	"LapisId" : "28616"
}, {
	"ButtonImagePath" : "BTN_GeniesGems2",
	"DisplayName" : "精灵宝石",
	"typeid" : 1,
	"LapisId" : "28608"
}, {
	"ButtonImagePath" : "BTN_Germinator1",
	"DisplayName" : "细菌对对碰",
	"typeid" : 7,
	"LapisId" : "28614"
}, {
	"ButtonImagePath" : "BTN_GiftRap2",
	"DisplayName" : "礼品包装",
	"typeid" : 1,
	"LapisId" : "28618"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns2",
	"DisplayName" : "女孩与枪II",
	"typeid" : 1,
	"LapisId" : "29449"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns1",
	"DisplayName" : "女孩与枪",
	"typeid" : 1,
	"LapisId" : "28622"
}, {
	"ButtonImagePath" : "BTN_GoldCoast3",
	"DisplayName" : "黄金海岸",
	"typeid" : 2,
	"LapisId" : "28624"
}, {
	"ButtonImagePath" : "BTN_GoldFactory",
	"DisplayName" : "黄金工厂",
	"typeid" : 1,
	"LapisId" : "28626"
}, {
	"ButtonImagePath" : "BTN_GoldenDragon6",
	"DisplayName" : "黄金龙",
	"typeid" : 2,
	"LapisId" : "28628"
}, {
	"ButtonImagePath" : "BTN_GoldenEra",
	"DisplayName" : "黄金时代",
	"typeid" : 1,
	"LapisId" : "28630"
}, {
	"ButtonImagePath" : "BTN_GoldenGhouls1",
	"DisplayName" : "黄金食尸鬼",
	"typeid" : 4,
	"LapisId" : "28632"
}, {
	"ButtonImagePath" : "BTN_GoldenPrincess",
	"DisplayName" : "黄金公主",
	"typeid" : 1,
	"LapisId" : "28634"
}, {
	"ButtonImagePath" : "BTN_GoodToGo2",
	"DisplayName" : "疯狂赛道",
	"typeid" : 1,
	"LapisId" : "28636"
}, {
	"ButtonImagePath" : "BTN_GopherGold2",
	"DisplayName" : "黄金囊地鼠",
	"typeid" : 1,
	"LapisId" : "28638"
}, {
	"ButtonImagePath" : "BTN_GrannyPrix1",
	"DisplayName" : "老太太赛车",
	"typeid" : 7,
	"LapisId" : "28640"
}, {
	"ButtonImagePath" : "BTN_GreatGriffin1",
	"DisplayName" : "大狮鹫",
	"typeid" : 1,
	"LapisId" : "28642"
}
                ];

var agdata = [
            { "DisplayName": "捕鱼王者", "ButtonImagePath": "SB36.gif", "LapisId": "6" ,"typeid":"4"},
            { "DisplayName": "水果拉霸", "ButtonImagePath": "FRU.jpg", "LapisId": "501" ,"typeid":"3"},
            { "DisplayName": "杰克高手", "ButtonImagePath": "PKBJ.jpg", "LapisId": "502" ,"typeid":"3"},
            { "DisplayName": "极速幸运轮", "ButtonImagePath": "TGLW.jpg", "LapisId": "507","typeid":"2"},
            { "DisplayName": "太空漫游", "ButtonImagePath": "SB01.jpg", "LapisId": "508" ,"typeid":"3"},
            { "DisplayName": "复古花园", "ButtonImagePath": "SB02.jpg", "LapisId": "509" ,"typeid":"3"},
            { "DisplayName": "关东煮", "ButtonImagePath": "SB03.jpg", "LapisId": "510" ,"typeid":"4"},
            { "DisplayName": "牧场咖啡", "ButtonImagePath": "SB04.jpg", "LapisId": "511" ,"typeid":"4"},
            { "DisplayName": "甜一甜屋", "ButtonImagePath": "SB05.jpg", "LapisId": "512" ,"typeid":"4"},
            { "DisplayName": "日本武士", "ButtonImagePath": "SB06.jpg", "LapisId": "513" ,"typeid":"4"},
            { "DisplayName": "象棋老虎机", "ButtonImagePath": "SB07.jpg", "LapisId": "514" ,"typeid":"4"},
            { "DisplayName": "麻将老虎机", "ButtonImagePath": "SB08.jpg", "LapisId": "515" ,"typeid":"4"},
            { "DisplayName": "西洋棋老虎机", "ButtonImagePath": "SB09.jpg", "LapisId": "516" ,"typeid":"4"},
            { "DisplayName": "开心农场", "ButtonImagePath": "SB10.jpg", "LapisId": "517" ,"typeid":"4"},
            { "DisplayName": "夏日营地", "ButtonImagePath": "SB11.jpg", "LapisId": "518" ,"typeid":"4"},
            { "DisplayName": "海底漫游", "ButtonImagePath": "SB12.jpg", "LapisId": "519" ,"typeid":"4"},
            { "DisplayName": "鬼马小丑", "ButtonImagePath": "SB13.jpg", "LapisId": "520" ,"typeid":"4"},
            { "DisplayName": "机动乐园", "ButtonImagePath": "SB14.jpg", "LapisId": "521" ,"typeid":"4"},
            { "DisplayName": "惊吓鬼屋", "ButtonImagePath": "SB15.jpg", "LapisId": "522" ,"typeid":"4"},
            { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" ,"typeid":"4"},
            { "DisplayName": "海洋剧场", "ButtonImagePath": "SB17.jpg", "LapisId": "524" ,"typeid":"4"},
            { "DisplayName": "水上乐园", "ButtonImagePath": "SB18.jpg", "LapisId": "525" ,"typeid":"4"},
            { "DisplayName": "空中战争", "ButtonImagePath": "SB19.jpg", "LapisId": "526" ,"typeid":"4"},
            { "DisplayName": "摇滚狂迷", "ButtonImagePath": "SB20.jpg", "LapisId": "527" ,"typeid":"4"},
            { "DisplayName": "越野机车", "ButtonImagePath": "SB21.jpg", "LapisId": "528" ,"typeid":"4"},
            { "DisplayName": "埃及奥秘", "ButtonImagePath": "SB22.jpg", "LapisId": "529" ,"typeid":"4"},
            { "DisplayName": "欢乐时光", "ButtonImagePath": "SB23.png", "LapisId": "530" ,"typeid":"4"},
            { "DisplayName": "侏罗纪", "ButtonImagePath": "SB24.jpg", "LapisId": "531" ,"typeid":"4"},
            { "DisplayName": "土地神", "ButtonImagePath": "SB25.jpg", "LapisId": "532" ,"typeid":"4"},
            { "DisplayName": "布袋和尚", "ButtonImagePath": "SB26.jpg", "LapisId": "533" ,"typeid":"4"},
            { "DisplayName": "正财神", "ButtonImagePath": "SB27.jpg", "LapisId": "534" ,"typeid":"4"},
            { "DisplayName": "武财神", "ButtonImagePath": "SB28.jpg", "LapisId": "535" ,"typeid":"4"},
            { "DisplayName": "偏财神", "ButtonImagePath": "SB29.jpg", "LapisId": "536" ,"typeid":"4"},
            { "DisplayName": "天空守护者", "ButtonImagePath": "SB31.jpg", "LapisId": "542" ,"typeid":"3"},
            { "DisplayName": "齐天大圣", "ButtonImagePath": "SB32.gif", "LapisId": "543" ,"typeid":"2"},
            { "DisplayName": "糖果碰碰乐", "ButtonImagePath": "SB33.gif", "LapisId": "544" ,"typeid":"2"},
            { "DisplayName": "冰河世界", "ButtonImagePath": "SB34.jpg", "LapisId": "545" ,"typeid":"4"},
            { "DisplayName": "欧洲列强争霸", "ButtonImagePath": "SB35.gif", "LapisId": "547" ,"typeid":"4"},
            { "DisplayName": "上海百乐门", "ButtonImagePath": "SB37.jpg", "LapisId": "549" ,"typeid":"2"},
            { "DisplayName": "性感女僕", "ButtonImagePath": "AV01.jpg", "LapisId": "537" ,"typeid":"3"},
            { "DisplayName": "百搭二王", "ButtonImagePath": "PKBD.gif", "LapisId": "540" ,"typeid":"2"},
            { "DisplayName": "红利百搭", "ButtonImagePath": "PKBB.gif", "LapisId": "541" ,"typeid":"2"},
            { "DisplayName": "水果拉霸2", "ButtonImagePath": "FRU2.gif", "LapisId": "546" ,"typeid":"2"},
            { "DisplayName": "龙珠", "ButtonImagePath": "XG01.jpg", "LapisId": "200" ,"typeid":"4"},
            { "DisplayName": "幸运8", "ButtonImagePath": "XG02.jpg", "LapisId": "201" ,"typeid":"4"},
            { "DisplayName": "闪亮女郎", "ButtonImagePath": "XG03.jpg", "LapisId": "202" ,"typeid":"2"},
            { "DisplayName": "金鱼", "ButtonImagePath": "XG04.jpg", "LapisId": "203" ,"typeid":"2"},
            { "DisplayName": "中国新年", "ButtonImagePath": "XG05.jpg", "LapisId": "204" ,"typeid":"2"},
            { "DisplayName": "海盗王", "ButtonImagePath": "XG06.jpg", "LapisId": "205" ,"typeid":"4"},
            { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" ,"typeid":"4"},
            { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" ,"typeid":"4"}
        ];
   var aghotdata = [   { "DisplayName": "捕鱼王者", "ButtonImagePath": "SB36.gif", "LapisId": "6" },
                       { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" ,"typeid":"4"},
                       { "DisplayName": "海洋剧场", "ButtonImagePath": "SB17.jpg", "LapisId": "524" ,"typeid":"4"},
                       { "DisplayName": "水上乐园", "ButtonImagePath": "SB18.jpg", "LapisId": "525" ,"typeid":"4"},
                       { "DisplayName": "空中战争", "ButtonImagePath": "SB19.jpg", "LapisId": "526" ,"typeid":"4"},
                       { "DisplayName": "摇滚狂迷", "ButtonImagePath": "SB20.jpg", "LapisId": "527" ,"typeid":"4"},
                       { "DisplayName": "越野机车", "ButtonImagePath": "SB21.jpg", "LapisId": "528" ,"typeid":"4"},
                       { "DisplayName": "埃及奥秘", "ButtonImagePath": "SB22.jpg", "LapisId": "529" ,"typeid":"4"},
                       { "DisplayName": "欢乐时光", "ButtonImagePath": "SB23.png", "LapisId": "530" ,"typeid":"4"},
                       { "DisplayName": "侏罗纪", "ButtonImagePath": "SB24.jpg", "LapisId": "531" ,"typeid":"4"},
                       { "DisplayName": "土地神", "ButtonImagePath": "SB25.jpg", "LapisId": "532" ,"typeid":"4"},
                       { "DisplayName": "布袋和尚", "ButtonImagePath": "SB26.jpg", "LapisId": "533" ,"typeid":"4"},
                       { "DisplayName": "正财神", "ButtonImagePath": "SB27.jpg", "LapisId": "534" ,"typeid":"4"},
                       { "DisplayName": "武财神", "ButtonImagePath": "SB28.jpg", "LapisId": "535" ,"typeid":"4"},
                       { "DisplayName": "偏财神", "ButtonImagePath": "SB29.jpg", "LapisId": "536" ,"typeid":"4"},
                       { "DisplayName": "天空守护者", "ButtonImagePath": "SB31.jpg", "LapisId": "542" ,"typeid":"3"},
                       { "DisplayName": "齐天大圣", "ButtonImagePath": "SB32.gif", "LapisId": "543" ,"typeid":"2"},
                       { "DisplayName": "糖果碰碰乐", "ButtonImagePath": "SB33.gif", "LapisId": "544" ,"typeid":"2"},
                       { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" },
                       { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" }
        ];
   var agnewdata = [   { "DisplayName": "捕鱼王者", "ButtonImagePath": "SB36.gif", "LapisId": "6" },
                       { "DisplayName": "甜一甜屋", "ButtonImagePath": "SB05.jpg", "LapisId": "512" },
                       { "DisplayName": "机动乐园", "ButtonImagePath": "SB14.jpg", "LapisId": "521" },
                       { "DisplayName": "惊吓鬼屋", "ButtonImagePath": "SB15.jpg", "LapisId": "522" },
                       { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" },
                       { "DisplayName": "上海百乐门", "ButtonImagePath": "SB37.jpg", "LapisId": "549" },
                       { "DisplayName": "性感女僕", "ButtonImagePath": "AV01.jpg", "LapisId": "537" },
                       { "DisplayName": "龙珠", "ButtonImagePath": "XG01.jpg", "LapisId": "200" },
                       { "DisplayName": "幸运8", "ButtonImagePath": "XG02.jpg", "LapisId": "201" },
                       { "DisplayName": "闪亮女郎", "ButtonImagePath": "XG03.jpg", "LapisId": "202" },
                       { "DisplayName": "金鱼", "ButtonImagePath": "XG04.jpg", "LapisId": "203" },
                       { "DisplayName": "中国新年", "ButtonImagePath": "XG05.jpg", "LapisId": "204" },
                       { "DisplayName": "海盗王", "ButtonImagePath": "XG06.jpg", "LapisId": "205" },
                       { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" },
                       { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" }
                   ];

var jdbdata = [{"ButtonImagePath":"WEB_Icon8001_250x203_cn.jpg","DisplayName":"幸运龙","typeid":"0","LapisId":"8001"},{"ButtonImagePath":"WEB_Icon8002_250x203_cn.jpg","DisplayName":"唐伯虎点秋香","typeid":"0","LapisId":"8002"},{"ButtonImagePath":"WEB_Icon8003_250x203_cn.jpg","DisplayName":"变脸","typeid":"0","LapisId":"8003"},{"ButtonImagePath":"WEB_Icon8004_250x203_cn.jpg","DisplayName":"悟空","typeid":"0","LapisId":"8004"},{"ButtonImagePath":"WEB_Icon8005_250x203_cn.jpg","DisplayName":"骆马大冒险","typeid":"0","LapisId":"8005"},{"ButtonImagePath":"WEB_Icon8006_250x203_cn.jpg","DisplayName":"台湾黑熊","typeid":"0","LapisId":"8006"},{"ButtonImagePath":"WEB_Icon8007_250x203_cn.jpg","DisplayName":"幸运麟","typeid":"0","LapisId":"8007"},{"ButtonImagePath":"WEB_Icon8014_250x203_cn.jpg","DisplayName":"招财狮","typeid":"0","LapisId":"8014"},{"ButtonImagePath":"WEB_Icon8015_250x203_cn.jpg","DisplayName":"月光秘宝","typeid":"0","LapisId":"8015"},{"ButtonImagePath":"WEB_Icon8016_250x203_cn.jpg","DisplayName":"上班族狂想曲","typeid":"0","LapisId":"8016"},{"ButtonImagePath":"WEB_Icon8017_250x203_cn.jpg","DisplayName":"过新年","typeid":"0","LapisId":"8017"},{"ButtonImagePath":"WEB_Icon8018_250x203_cn.jpg","DisplayName":"拿破仑","typeid":"0","LapisId":"8018"},{"ButtonImagePath":"WEB_Icon8019_250x203_cn.jpg","DisplayName":"文房四宝","typeid":"0","LapisId":"8019"},{"ButtonImagePath":"WEB_Icon8020_250x203_cn.jpg","DisplayName":"芝麻开门","typeid":"0","LapisId":"8020"},{"ButtonImagePath":"WEB_Icon8021_250x203_cn.jpg","DisplayName":"黄金香蕉帝国","typeid":"0","LapisId":"8021"},{"ButtonImagePath":"WEB_Icon8022_250x203_cn.jpg","DisplayName":"麻雀无双","typeid":"0","LapisId":"8022"},{"ButtonImagePath":"WEB_Icon8023_250x203_cn.jpg","DisplayName":"奥林匹亚神庙","typeid":"0","LapisId":"8023"},{"ButtonImagePath":"WEB_Icon8024_250x203_cn.jpg","DisplayName":"水晶王国","typeid":"0","LapisId":"8024"},{"ButtonImagePath":"WEB_Icon8025_250x203_cn.jpg","DisplayName":"神偷妙贼","typeid":"0","LapisId":"8025"},{"ButtonImagePath":"WEB_Icon8026_250x203_cn.jpg","DisplayName":"热舞教父","typeid":"0","LapisId":"8026"},{"ButtonImagePath":"WEB_Icon8027_250x203_cn.jpg","DisplayName":"料理厨王","typeid":"0","LapisId":"8027"},{"ButtonImagePath":"WEB_Icon8028_250x203_cn.jpg","DisplayName":"幸运淘金鼠","typeid":"0","LapisId":"8028"},{"ButtonImagePath":"WEB_Icon8029_250x203_cn.jpg","DisplayName":"奇幻糖果岛","typeid":"0","LapisId":"8029"},{"ButtonImagePath":"WEB_Icon8030_250x203_cn.jpg","DisplayName":"疯狂科学家","typeid":"0","LapisId":"8030"},{"ButtonImagePath":"WEB_Icon8031_250x203_cn.jpg","DisplayName":"金饺子","typeid":"0","LapisId":"8031"},{"ButtonImagePath":"WEB_Icon8034_250x203_cn.jpg","DisplayName":"金钱侠","typeid":"0","LapisId":"8034"},{"ButtonImagePath":"WEB_Icon8035_250x203_cn.jpg","DisplayName":"幸运凤","typeid":"0","LapisId":"8035"},{"ButtonImagePath":"WEB_Icon8036_250x203_cn.jpg","DisplayName":"龙王","typeid":"0","LapisId":"8036"},{"ButtonImagePath":"WEB_Icon8037_250x203_cn.jpg","DisplayName":"魔术秀","typeid":"0","LapisId":"8037"},{"ButtonImagePath":"WEB_Icon8044_250x203_cn.jpg","DisplayName":"江山美人","typeid":"0","LapisId":"8044"},{"ButtonImagePath":"WEB_Icon8046_250x203_cn.jpg","DisplayName":"关公","typeid":"0","LapisId":"8046"},{"ButtonImagePath":"WEB_Icon8047_250x203_cn.jpg","DisplayName":"变脸2","typeid":"0","LapisId":"8047"},{"ButtonImagePath":"WEB_Icon8048_250x203_cn.jpg","DisplayName":"芝麻开门2","typeid":"0","LapisId":"8048"},{"ButtonImagePath":"WEB_Icon8049_250x203_cn.jpg","DisplayName":"唐伯虎点秋香2","typeid":"0","LapisId":"8049"},{"ButtonImagePath":"WEB_Icon8999_250x203_cn.jpg","DisplayName":"直播变脸","typeid":"0","LapisId":"8999"},{"ButtonImagePath":"WEB_Icon14001_250x203_cn.jpg","DisplayName":"斗鸡","typeid":"0","LapisId":"14001"},{"ButtonImagePath":"WEB_Icon14002_250x203_cn.jpg","DisplayName":"玛雅大冒险","typeid":"0","LapisId":"14002"},{"ButtonImagePath":"WEB_Icon14003_250x203_cn.jpg","DisplayName":"屌丝熊猫","typeid":"0","LapisId":"14003"},{"ButtonImagePath":"WEB_Icon14004_250x203_cn.jpg","DisplayName":"塞尔达传说","typeid":"0","LapisId":"14004"},{"ButtonImagePath":"WEB_Icon14005_250x203_cn.jpg","DisplayName":"包大人","typeid":"0","LapisId":"14005"},{"ButtonImagePath":"WEB_Icon14006_250x203_cn.jpg","DisplayName":"亿万富翁","typeid":"0","LapisId":"14006"},{"ButtonImagePath":"WEB_Icon14007_250x203_cn.jpg","DisplayName":"一拳超人","typeid":"0","LapisId":"14007"},{"ButtonImagePath":"WEB_Icon14008_250x203_cn.jpg","DisplayName":"神龙大侠","typeid":"0","LapisId":"14008"},{"ButtonImagePath":"WEB_Icon14010_250x203_cn.jpg","DisplayName":"飞龙在天","typeid":"0","LapisId":"14010"},{"ButtonImagePath":"WEB_Icon14011_250x203_cn.jpg","DisplayName":"银河护卫队","typeid":"0","LapisId":"14011"},{"ButtonImagePath":"WEB_Icon14012_250x203_cn.jpg","DisplayName":"街头霸王","typeid":"0","LapisId":"14012"},{"ButtonImagePath":"WEB_Icon14013_250x203_cn.jpg","DisplayName":"春宵苦短","typeid":"0","LapisId":"14013"},{"ButtonImagePath":"WEB_Icon14015_250x203_cn.jpg","DisplayName":"星球大战","typeid":"0","LapisId":"14015"},{"ButtonImagePath":"WEB_Icon14016_250x203_cn.jpg","DisplayName":"王牌特工","typeid":"0","LapisId":"14016"},{"ButtonImagePath":"WEB_Icon14017_250x203_cn.jpg","DisplayName":"少女前线","typeid":"0","LapisId":"14017"},{"ButtonImagePath":"WEB_Icon14018_250x203_cn.jpg","DisplayName":"妲己","typeid":"0","LapisId":"14018"},{"ButtonImagePath":"WEB_Icon14019_250x203_cn.jpg","DisplayName":"宝石物语","typeid":"0","LapisId":"14019"},{"ButtonImagePath":"WEB_Icon14020_250x203_cn.jpg","DisplayName":"魔法乳神","typeid":"0","LapisId":"14020"},{"ButtonImagePath":"WEB_Icon14021_250x203_cn.jpg","DisplayName":"钱滚钱","typeid":"0","LapisId":"14021"},{"ButtonImagePath":"WEB_Icon14022_250x203_cn.jpg","DisplayName":"采矿土豪","typeid":"0","LapisId":"14022"},{"ButtonImagePath":"WEB_Icon14023_250x203_cn.jpg","DisplayName":"赌王扑克","typeid":"0","LapisId":"14023"},{"ButtonImagePath":"WEB_Icon14025_250x203_cn.jpg","DisplayName":"幸运赛车","typeid":"0","LapisId":"14025"},{"ButtonImagePath":"WEB_Icon14026_250x203_cn.jpg","DisplayName":"发大财","typeid":"0","LapisId":"14026"},{"ButtonImagePath":"WEB_Icon15001_250x203_cn.jpg","DisplayName":"金鸡报囍","typeid":"0","LapisId":"15001"},{"ButtonImagePath":"WEB_Icon15002_250x203_cn.jpg","DisplayName":"齐天大圣","typeid":"0","LapisId":"15002"},{"ButtonImagePath":"WEB_Icon15004_250x203_cn.jpg","DisplayName":"火牛阵","typeid":"0","LapisId":"15004"},{"ButtonImagePath":"WEB_Icon15006_250x203_cn.jpg","DisplayName":"印加帝国","typeid":"0","LapisId":"15006"},{"ButtonImagePath":"WEB_Icon15009_250x203_cn.jpg","DisplayName":"忍者大进击","typeid":"0","LapisId":"15009"},{"ButtonImagePath":"WEB_Icon15010_250x203_cn.jpg","DisplayName":"熊猫厨王","typeid":"0","LapisId":"15010"},{"ButtonImagePath":"WEB_Icon15011_250x203_cn.jpg","DisplayName":"后羿","typeid":"0","LapisId":"15011"},{"ButtonImagePath":"WEB_Icon15013_250x203_cn.jpg","DisplayName":"九尾狐","typeid":"0","LapisId":"15013"},{"ButtonImagePath":"WEB_Icon7001_250x203_cn.jpg","DisplayName":"龙王捕鱼","typeid":"7","LapisId":"7001"},{"ButtonImagePath":"WEB_Icon7002_250x203_cn.jpg","DisplayName":"龙王捕鱼2","typeid":"7","LapisId":"7002"},{"ButtonImagePath":"WEB_Icon7003_250x203_cn.jpg","DisplayName":"财神捕鱼","typeid":"7","LapisId":"7003"},{"ButtonImagePath":"WEB_Icon9001_250x203_cn.jpg","DisplayName":"小玛莉","typeid":"9","LapisId":"9001"},{"ButtonImagePath":"WEB_Icon9002_250x203_cn.jpg","DisplayName":"新年快乐","typeid":"9","LapisId":"9002"},{"ButtonImagePath":"WEB_Icon9003_250x203_cn.jpg","DisplayName":"飞禽走兽","typeid":"9","LapisId":"9003"},{"ButtonImagePath":"WEB_Icon9004_250x203_cn.jpg","DisplayName":"啤酒大亨","typeid":"9","LapisId":"9004"},{"ButtonImagePath":"WEB_Icon9006_250x203_cn.jpg","DisplayName":"花果山传奇","typeid":"9","LapisId":"9006"},{"ButtonImagePath":"WEB_Icon9007_250x203_cn.jpg","DisplayName":"超激发水果盘","typeid":"9","LapisId":"9007"}];
var ptdata = [{"ButtonImagePath":"pt/pc/ash3brg.jpg","DisplayName":"3卡吹噓","typeid":"桌牌游戏","LapisId":"ash3brg"},{"ButtonImagePath":"pt/pc/ashamw.jpg","DisplayName":"Amazon Wild ","typeid":"老虎机","LapisId":"ashamw"},{"ButtonImagePath":"pt/pc/ashbob.jpg","DisplayName":"Bounty of the Beanstalk","typeid":"老虎机","LapisId":"ashbob"},{"ButtonImagePath":"pt/pc/ashcpl.jpg","DisplayName":"宝物箱中寻","typeid":"老虎机","LapisId":"ashcpl"},{"ButtonImagePath":"pt/pc/ashfmf.jpg","DisplayName":"满月财富","typeid":"老虎机","LapisId":"ashfmf"},{"ButtonImagePath":"pt/pc/ashfta.jpg","DisplayName":"Fairest of them All","typeid":"老虎机","LapisId":"ashfta"},{"ButtonImagePath":"pt/pc/ashhotj.jpg","DisplayName":"丛林心脏","typeid":"老虎机","LapisId":"ashhotj"},{"ButtonImagePath":"pt/pc/ashlob.jpg","DisplayName":"Monty Python′s Life of Brian","typeid":"老虎机","LapisId":"ashlob"},{"ButtonImagePath":"pt/pc/ashsbd.jpg","DisplayName":"辛巴达的金色航程","typeid":"老虎机","LapisId":"ashsbd"},{"ButtonImagePath":"pt/pc/ashvrtd.jpg","DisplayName":"虚拟赛狗","typeid":"街机游戏","LapisId":"ashvrtd"},{"ButtonImagePath":"pt/pc/ashvrth.jpg","DisplayName":"虚拟赛马","typeid":"街机游戏","LapisId":"ashvrth"},{"ButtonImagePath":"pt/pc/ashwgaa.jpg","DisplayName":"赌徒:北极探险","typeid":"老虎机","LapisId":"ashwgaa"},{"ButtonImagePath":"pt/pc/avng.jpg","DisplayName":"复仇者联盟","typeid":"老虎机","LapisId":"avng"},{"ButtonImagePath":"pt/pc/bal.jpg","DisplayName":"迷你百家乐","typeid":"视讯扑克","LapisId":"bal"},{"ButtonImagePath":"pt/pc/bib.jpg","DisplayName":"湛蓝深海","typeid":"老虎机","LapisId":"bib"},{"ButtonImagePath":"pt/pc/bj_mh5.jpg","DisplayName":"UK Blackjack Multihand 5","typeid":"老虎机","LapisId":"bjuk_mh5"},{"ButtonImagePath":"pt/pc/bs.jpg","DisplayName":"白狮","typeid":"老虎机","LapisId":"bs"},{"ButtonImagePath":"pt/pc/chel.jpg","DisplayName":"真人娱乐场扑克","typeid":"视讯扑克","LapisId":"chel"},{"ButtonImagePath":"pt/pc/cnpr.jpg","DisplayName":"甜蜜派对","typeid":"老虎机","LapisId":"cnpr"},{"ButtonImagePath":"pt/pc/ctiv.jpg","DisplayName":"拉斯维加斯的猫","typeid":"老虎机","LapisId":"ctiv"},{"ButtonImagePath":"pt/pc/dctw.jpg","DisplayName":"转骰子游戏","typeid":"街机游戏","LapisId":"dctw"},{"ButtonImagePath":"pt/pc/donq.jpg","DisplayName":"堂吉诃德的财富","typeid":"老虎机","LapisId":"donq"},{"ButtonImagePath":"pt/pc/drts.jpg","DisplayName":"飞镖投掷赌博游戏","typeid":"街机游戏","LapisId":"drts"},{"ButtonImagePath":"pt/pc/dual_rol.jpg","DisplayName":"双轮盘赌","typeid":"视讯扑克","LapisId":"dual_rol"},{"ButtonImagePath":"pt/pc/er.jpg","DisplayName":"开心假期","typeid":"老虎机","LapisId":"er"},{"ButtonImagePath":"pt/pc/fbm.jpg","DisplayName":"狂热足球","typeid":"刮刮卡","LapisId":"fbm"},{"ButtonImagePath":"pt/pc/fbr.jpg","DisplayName":"终极足球","typeid":"老虎机","LapisId":"fbr"},{"ButtonImagePath":"pt/pc/fcgz.jpg","DisplayName":"翡翠公主","typeid":"老虎机","LapisId":"fcgz"},{"ButtonImagePath":"pt/pc/fdt.jpg","DisplayName":"戴图理的神奇七","typeid":"老虎机","LapisId":"fdt"},{"ButtonImagePath":"pt/pc/fdtjg.jpg","DisplayName":"戴图理的神奇七大奖","typeid":"老虎机","LapisId":"fdtjg"},{"ButtonImagePath":"pt/pc/fff.jpg","DisplayName":"酷炫水果农场","typeid":"老虎机","LapisId":"fff"},{"ButtonImagePath":"pt/pc/fm.jpg","DisplayName":"古怪猴子","typeid":"老虎机","LapisId":"fm"},{"ButtonImagePath":"pt/pc/fmn.jpg","DisplayName":"水果狂","typeid":"老虎机","LapisId":"fmn"},{"ButtonImagePath":"pt/pc/fnf.jpg","DisplayName":"神奇四侠","typeid":"老虎机","LapisId":"fnf"},{"ButtonImagePath":"pt/pc/fnf50.jpg","DisplayName":"神奇四侠50条线","typeid":"老虎机","LapisId":"fnf50"},{"ButtonImagePath":"pt/pc/fnfrj.jpg","DisplayName":"趣味水果","typeid":"老虎机","LapisId":"fnfrj"},{"ButtonImagePath":"pt/pc/fosl.jpg","DisplayName":"固定赔率老虎机","typeid":"街机游戏","LapisId":"fosl"},{"ButtonImagePath":"pt/pc/fow.jpg","DisplayName":"惊异之林","typeid":"老虎机","LapisId":"fow"},{"ButtonImagePath":"pt/pc/foy.jpg","DisplayName":"青春之泉","typeid":"老虎机","LapisId":"foy"},{"ButtonImagePath":"pt/pc/frr.jpg","DisplayName":"法式轮盘","typeid":"桌牌游戏","LapisId":"frr"},{"ButtonImagePath":"pt/pc/frr_g.jpg","DisplayName":"法式奖金轮盘赌","typeid":"桌牌游戏","LapisId":"frr_g"},{"ButtonImagePath":"pt/pc/fsc.jpg","DisplayName":"决赛比分","typeid":"街机游戏","LapisId":"fsc"},{"ButtonImagePath":"pt/pc/fth.jpg","DisplayName":"财富山","typeid":"老虎机","LapisId":"fth"},{"ButtonImagePath":"pt/pc/fxf.jpg","DisplayName":"诙谐财富","typeid":"老虎机","LapisId":"fxf"},{"ButtonImagePath":"pt/pc/gc.jpg","DisplayName":"地妖之穴","typeid":"老虎机","LapisId":"gc"},{"ButtonImagePath":"pt/pc/ges.jpg","DisplayName":"艺伎故事","typeid":"老虎机","LapisId":"ges"},{"ButtonImagePath":"pt/pc/ghl.jpg","DisplayName":"猜扑克牌游戏","typeid":"街机游戏","LapisId":"ghl"},{"ButtonImagePath":"pt/pc/ghr.jpg","DisplayName":"恶灵骑士","typeid":"老虎机","LapisId":"ghr"},{"ButtonImagePath":"pt/pc/glg.jpg","DisplayName":"黄金游戏","typeid":"老虎机","LapisId":"glg"},{"ButtonImagePath":"pt/pc/glr.jpg","DisplayName":"角斗士","typeid":"老虎机","LapisId":"glr"},{"ButtonImagePath":"pt/pc/gos.jpg","DisplayName":"黄金之旅","typeid":"老虎机","LapisId":"gos"},{"ButtonImagePath":"pt/pc/grel.jpg","DisplayName":"金色召集","typeid":"老虎机","LapisId":"grel"},{"ButtonImagePath":"pt/pc/gts36.jpg","DisplayName":"灯之轮","typeid":"街机游戏","LapisId":"gts36"},{"ButtonImagePath":"pt/pc/gts37.jpg","DisplayName":"角斗士刮刮乐","typeid":"刮刮卡","LapisId":"gts37"},{"ButtonImagePath":"pt/pc/gts41.jpg","DisplayName":"神奇四侠刮刮乐","typeid":"刮刮卡","LapisId":"gts41"},{"ButtonImagePath":"pt/pc/gts43.jpg","DisplayName":"木乃伊迷城刮刮乐","typeid":"刮刮卡","LapisId":"gts43"},{"ButtonImagePath":"pt/pc/gts46.jpg","DisplayName":"生命之神","typeid":"老虎机","LapisId":"gts46"},{"ButtonImagePath":"pt/pc/gts47.jpg","DisplayName":"Lotto Madness Scratch","typeid":"刮刮卡","LapisId":"gts47"},{"ButtonImagePath":"pt/pc/gts49.jpg","DisplayName":"X战警刮刮乐","typeid":"刮刮卡","LapisId":"gts49"},{"ButtonImagePath":"pt/pc/gts5.jpg","DisplayName":"视频轮盘","typeid":"桌牌游戏","LapisId":"gts5"},{"ButtonImagePath":"pt/pc/gts50.jpg","DisplayName":"热力宝石","typeid":"老虎机","LapisId":"gts50"},{"ButtonImagePath":"pt/pc/gts51.jpg","DisplayName":"幸运熊猫","typeid":"老虎机","LapisId":"gts51"},{"ButtonImagePath":"pt/pc/gts52.jpg","DisplayName":"疯狂维京海盗","typeid":"老虎机","LapisId":"gts52"},{"ButtonImagePath":"pt/pc/gtsaod.jpg","DisplayName":"无罪或诱惑","typeid":"老虎机","LapisId":"gtsaod"},{"ButtonImagePath":"pt/pc/gtsbtg.jpg","DisplayName":"Battle Of  Gods","typeid":"老虎机","LapisId":"gtsbtg"},{"ButtonImagePath":"pt/pc/gtsfc.jpg","DisplayName":"足球狂欢节","typeid":"老虎机","LapisId":"gtsfc"},{"ButtonImagePath":"pt/pc/gtsfj.jpg","DisplayName":"跃龙门","typeid":"老虎机","LapisId":"gtsfj"},{"ButtonImagePath":"pt/pc/gtsflzt.jpg","DisplayName":"飞龙在天","typeid":"老虎机","LapisId":"gtsflzt"},{"ButtonImagePath":"pt/pc/gtsftf.jpg","DisplayName":"足球迷","typeid":"老虎机","LapisId":"gtsftf"},{"ButtonImagePath":"pt/pc/gtsghrsc.jpg","DisplayName":"恶灵骑士刮刮乐","typeid":"刮刮卡","LapisId":"gtsghrsc"},{"ButtonImagePath":"pt/pc/gtsgme.jpg","DisplayName":"大明帝国","typeid":"老虎机","LapisId":"gtsgme"},{"ButtonImagePath":"pt/pc/gtsgoc.jpg","DisplayName":"圣诞节幽灵","typeid":"老虎机","LapisId":"gtsgoc"},{"ButtonImagePath":"pt/pc/gtshlksc.jpg","DisplayName":"绿巨人刮刮乐","typeid":"刮刮卡","LapisId":"gtshlksc"},{"ButtonImagePath":"pt/pc/gtshwkp.jpg","DisplayName":"超级高速公路之王","typeid":"老虎机","LapisId":"gtshwkp"},{"ButtonImagePath":"pt/pc/gtsir.jpg","DisplayName":"浮冰流","typeid":"老虎机","LapisId":"gtsir"},{"ButtonImagePath":"pt/pc/gtsjhw.jpg","DisplayName":"约翰韦恩","typeid":"老虎机","LapisId":"gtsjhw"},{"ButtonImagePath":"pt/pc/gtsjxb.jpg","DisplayName":"吉祥8","typeid":"老虎机","LapisId":"gtsjxb"},{"ButtonImagePath":"pt/pc/gtsjzc.jpg","DisplayName":"爵士俱乐部","typeid":"老虎机","LapisId":"gtsjzc"},{"ButtonImagePath":"pt/pc/gtslgms.jpg","DisplayName":"野生游戏","typeid":"老虎机","LapisId":"gtslgms"},{"ButtonImagePath":"pt/pc/gtsmrln.jpg","DisplayName":"玛丽莲·梦露","typeid":"老虎机","LapisId":"gtsmrln"},{"ButtonImagePath":"pt/pc/gtspor.jpg","DisplayName":"非常幸运","typeid":"老虎机","LapisId":"gtspor"},{"ButtonImagePath":"pt/pc/gtspwzsc.jpg","DisplayName":"战争特区刮刮乐","typeid":"刮刮卡","LapisId":"gtspwzsc"},{"ButtonImagePath":"pt/pc/gtsrkysc.jpg","DisplayName":"洛基传奇刮刮乐","typeid":"刮刮卡","LapisId":"gtsrkysc"},{"ButtonImagePath":"pt/pc/gtsrng.jpg","DisplayName":"罗马与荣耀","typeid":"老虎机","LapisId":"gtsrng"},{"ButtonImagePath":"pt/pc/gtsru.jpg","DisplayName":"财富魔方","typeid":"街机游戏","LapisId":"gtsru"},{"ButtonImagePath":"pt/pc/gtssmbr.jpg","DisplayName":"激情桑巴","typeid":"老虎机","LapisId":"gtssmbr"},{"ButtonImagePath":"pt/pc/gtssmdm.jpg","DisplayName":"无敌金刚","typeid":"老虎机","LapisId":"gtssmdm"},{"ButtonImagePath":"pt/pc/gtssmdsc.jpg","DisplayName":"无敌金刚刮刮乐","typeid":"刮刮卡","LapisId":"gtssmdsc"},{"ButtonImagePath":"pt/pc/gtsspdsc.jpg","DisplayName":"蜘蛛侠刮刮乐","typeid":"刮刮卡","LapisId":"gtsspdsc"},{"ButtonImagePath":"pt/pc/gtsstg.jpg","DisplayName":"苏丹的财富","typeid":"老虎机","LapisId":"gtsstg"},{"ButtonImagePath":"pt/pc/gtsswk.jpg","DisplayName":"孙悟空","typeid":"老虎机","LapisId":"gtsswk"},{"ButtonImagePath":"pt/pc/gtstrmsc.jpg","DisplayName":"雷神刮刮乐","typeid":"刮刮卡","LapisId":"gtstrmsc"},{"ButtonImagePath":"pt/pc/gtsttlsc.jpg","DisplayName":"顶级王牌传奇刮刮乐","typeid":"刮刮卡","LapisId":"gtsttlsc"},{"ButtonImagePath":"pt/pc/gtswg.jpg","DisplayName":"赌徒","typeid":"老虎机","LapisId":"gtswg"},{"ButtonImagePath":"pt/pc/gtswng.jpg","DisplayName":"黄金翅膀","typeid":"老虎机","LapisId":"gtswng"},{"ButtonImagePath":"pt/pc/gtswvsc.jpg","DisplayName":"金刚狼刮刮乐","typeid":"刮刮卡","LapisId":"gtswvsc"},{"ButtonImagePath":"pt/pc/head.jpg","DisplayName":"硬币投掷游戏","typeid":"街机游戏","LapisId":"head"},{"ButtonImagePath":"pt/pc/hh.jpg","DisplayName":"鬼屋","typeid":"老虎机","LapisId":"hh"},{"ButtonImagePath":"pt/pc/hk.jpg","DisplayName":"高速公路之王","typeid":"老虎机","LapisId":"hk"},{"ButtonImagePath":"pt/pc/hlf.jpg","DisplayName":"万圣节财富","typeid":"老虎机","LapisId":"hlf"},{"ButtonImagePath":"pt/pc/hlk2.jpg","DisplayName":"绿巨人","typeid":"老虎机","LapisId":"hlk2"},{"ButtonImagePath":"pt/pc/hlk50.jpg","DisplayName":"绿巨人50条线","typeid":"老虎机","LapisId":"hlk50"},{"ButtonImagePath":"pt/pc/hpb.jpg","DisplayName":"快乐虫","typeid":"老虎机","LapisId":"hpb"},{"ButtonImagePath":"pt/pc/hr.jpg","DisplayName":"打比日","typeid":"街机游戏","LapisId":"hr"},{"ButtonImagePath":"pt/pc/hsd.jpg","DisplayName":"摊牌扑克","typeid":"街机游戏","LapisId":"hsd"},{"ButtonImagePath":"pt/pc/iceh.jpg","DisplayName":"冰球游戏","typeid":"老虎机","LapisId":"iceh"},{"ButtonImagePath":"pt/pc/irl.jpg","DisplayName":"爱尔兰运气","typeid":"老虎机","LapisId":"irl"},{"ButtonImagePath":"pt/pc/irm2.jpg","DisplayName":"钢铁人","typeid":"老虎机","LapisId":"irm2"},{"ButtonImagePath":"pt/pc/irm3.jpg","DisplayName":"钢铁人2","typeid":"老虎机","LapisId":"irm3"},{"ButtonImagePath":"pt/pc/irm3sc.jpg","DisplayName":"钢铁人2刮刮乐","typeid":"刮刮卡","LapisId":"irm3sc"},{"ButtonImagePath":"pt/pc/irm50.jpg","DisplayName":"钢铁人2 50线","typeid":"老虎机","LapisId":"irm50"},{"ButtonImagePath":"pt/pc/irmn3sc.jpg","DisplayName":"钢铁人3刮刮乐","typeid":"刮刮卡","LapisId":"irmn3sc"},{"ButtonImagePath":"pt/pc/jb.jpg","DisplayName":"丛林摇摆","typeid":"老虎机","LapisId":"jb"},{"ButtonImagePath":"pt/pc/jb_mh.jpg","DisplayName":"多手对J高手","typeid":"视讯扑克","LapisId":"jb_mh"},{"ButtonImagePath":"pt/pc/jp.jpg","DisplayName":"王牌扑克","typeid":"视讯扑克","LapisId":"jp"},{"ButtonImagePath":"pt/pc/jpgt.jpg","DisplayName":"奖金巨人","typeid":"老虎机","LapisId":"jpgt"},{"ButtonImagePath":"pt/pc/jqw.jpg","DisplayName":"金钱蛙","typeid":"老虎机","LapisId":"jqw"},{"ButtonImagePath":"pt/pc/kfp.jpg","DisplayName":"Liu Fu Shou","typeid":"老虎机","LapisId":"kfp"},{"ButtonImagePath":"pt/pc/kgdb.jpg","DisplayName":"德比王","typeid":"街机游戏","LapisId":"kgdb"},{"ButtonImagePath":"pt/pc/kkg.jpg","DisplayName":"无敌金刚","typeid":"老虎机","LapisId":"kkg"},{"ButtonImagePath":"pt/pc/kkgsc.jpg","DisplayName":"金刚刮刮乐","typeid":"刮刮卡","LapisId":"kkgsc"},{"ButtonImagePath":"pt/pc/kn.jpg","DisplayName":"基诺","typeid":"街机游戏","LapisId":"kn"},{"ButtonImagePath":"pt/pc/lm.jpg","DisplayName":"疯狂乐透","typeid":"老虎机","LapisId":"lm"},{"ButtonImagePath":"pt/pc/lndg.jpg","DisplayName":"Land of Gold","typeid":"老虎机","LapisId":"lndg"},{"ButtonImagePath":"pt/pc/lom.jpg","DisplayName":"爱情配对","typeid":"刮刮卡","LapisId":"lom"},{"ButtonImagePath":"pt/pc/lvb.jpg","DisplayName":"爱之船","typeid":"老虎机","LapisId":"lvb"},{"ButtonImagePath":"pt/pc/lwh.jpg","DisplayName":"轮盘旋转赌博游戏","typeid":"街机游戏","LapisId":"lwh"},{"ButtonImagePath":"pt/pc/mcb.jpg","DisplayName":"Cash back先生","typeid":"老虎机","LapisId":"mcb"},{"ButtonImagePath":"pt/pc/mfbr.jpg","DisplayName":"迷你终极足球","typeid":"迷你游戏","LapisId":"mfbr"},{"ButtonImagePath":"pt/pc/mfdt.jpg","DisplayName":"迷你戴图理","typeid":"迷你游戏","LapisId":"mfdt"},{"ButtonImagePath":"pt/pc/mglr.jpg","DisplayName":"迷你角斗士","typeid":"迷你游戏","LapisId":"mglr"},{"ButtonImagePath":"pt/pc/mgstk.jpg","DisplayName":"Magical Stacks","typeid":"老虎机","LapisId":"mgstk"},{"ButtonImagePath":"pt/pc/mmy.jpg","DisplayName":"木乃伊迷城","typeid":"老虎机","LapisId":"mmy"},{"ButtonImagePath":"pt/pc/mnkt.jpg","DisplayName":"霹雳神猴","typeid":"街机游戏","LapisId":"mnkt"},{"ButtonImagePath":"pt/pc/mro.jpg","DisplayName":"迷你轮盘赌","typeid":"街机游戏","LapisId":"mro"},{"ButtonImagePath":"pt/pc/mro_g.jpg","DisplayName":"轮盘赌(mini)","typeid":"迷你游戏","LapisId":"mro_g"},{"ButtonImagePath":"pt/pc/ms.jpg","DisplayName":"魔幻吃角子老虎","typeid":"老虎机","LapisId":"ms"},{"ButtonImagePath":"pt/pc/nian_k.jpg","DisplayName":"年年有余","typeid":"老虎机","LapisId":"nian_k"},{"ButtonImagePath":"pt/pc/nk.jpg","DisplayName":"海王星王国","typeid":"老虎机","LapisId":"nk"},{"ButtonImagePath":"pt/pc/op.jpg","DisplayName":"海洋公主","typeid":"老虎机","LapisId":"op"},{"ButtonImagePath":"pt/pc/paw.jpg","DisplayName":"小猪与狼","typeid":"老虎机","LapisId":"paw"},{"ButtonImagePath":"pt/pc/pbro.jpg","DisplayName":"弹球轮盘","typeid":"街机游戏","LapisId":"pbro"},{"ButtonImagePath":"pt/pc/pep.jpg","DisplayName":"Pick 'em Poker ","typeid":"桌牌游戏","LapisId":"pep"},{"ButtonImagePath":"pt/pc/pg.jpg","DisplayName":"牌九扑克","typeid":"桌牌游戏","LapisId":"pg"},{"ButtonImagePath":"pt/pc/pgv.jpg","DisplayName":"企鹅假期","typeid":"老虎机","LapisId":"pgv"},{"ButtonImagePath":"pt/pc/photk.jpg","DisplayName":"热紫","typeid":"老虎机","LapisId":"photk"},{"ButtonImagePath":"pt/pc/pks.jpg","DisplayName":"法老王国划刮刮乐","typeid":"刮刮卡","LapisId":"pks"},{"ButtonImagePath":"pt/pc/pl.jpg","DisplayName":"舞线","typeid":"老虎机","LapisId":"pl"},{"ButtonImagePath":"pt/pc/pmn.jpg","DisplayName":"黑豹之月","typeid":"老虎机","LapisId":"pmn"},{"ButtonImagePath":"pt/pc/pnp.jpg","DisplayName":"粉红豹","typeid":"老虎机","LapisId":"pnp"},{"ButtonImagePath":"pt/pc/po.jpg","DisplayName":"对J高手","typeid":"视讯扑克","LapisId":"po"},{"ButtonImagePath":"pt/pc/pon_mh5.jpg","DisplayName":"Pontoon Multihand 5 ","typeid":"桌牌游戏","LapisId":"pon_mh5"},{"ButtonImagePath":"pt/pc/pop.jpg","DisplayName":"宾果","typeid":"街机游戏","LapisId":"pop"},{"ButtonImagePath":"pt/pc/pso.jpg","DisplayName":"罚点球游戏","typeid":"街机游戏","LapisId":"pso"},{"ButtonImagePath":"pt/pc/pst.jpg","DisplayName":"法老王的秘密","typeid":"老虎机","LapisId":"pst"},{"ButtonImagePath":"pt/pc/pyrrk.jpg","DisplayName":"拉美西斯的金字塔","typeid":"老虎机","LapisId":"pyrrk"},{"ButtonImagePath":"pt/pc/qnw.jpg","DisplayName":"Queen of Wands","typeid":"老虎机","LapisId":"qnw"},{"ButtonImagePath":"pt/pc/qop.jpg","DisplayName":"金字塔女王","typeid":"老虎机","LapisId":"qop"},{"ButtonImagePath":"pt/pc/rcd.jpg","DisplayName":"掷骰子赌博游戏","typeid":"街机游戏","LapisId":"rcd"},{"ButtonImagePath":"pt/pc/rd.jpg","DisplayName":"红狗","typeid":"桌牌游戏","LapisId":"rd"},{"ButtonImagePath":"pt/pc/rky.jpg","DisplayName":"洛基传奇","typeid":"老虎机","LapisId":"rky"},{"ButtonImagePath":"pt/pc/rnr.jpg","DisplayName":"摇摆舞","typeid":"老虎机","LapisId":"rnr"},{"ButtonImagePath":"pt/pc/ro_g.jpg","DisplayName":"欧式奖金轮盘赌","typeid":"桌牌游戏","LapisId":"ro_g"},{"ButtonImagePath":"pt/pc/rodz_g.jpg","DisplayName":"美式奖金轮盘赌","typeid":"桌牌游戏","LapisId":"rodz_g"},{"ButtonImagePath":"pt/pc/rofl.jpg","DisplayName":"实况法式轮盘","typeid":"视讯扑克","LapisId":"rofl"},{"ButtonImagePath":"pt/pc/rol.jpg","DisplayName":"真人轮盘赌","typeid":"视讯扑克","LapisId":"rol"},{"ButtonImagePath":"pt/pc/rom.jpg","DisplayName":"奇迹轮盘","typeid":"桌牌游戏","LapisId":"rom"},{"ButtonImagePath":"pt/pc/romw.jpg","DisplayName":"多轮轮盘","typeid":"桌牌游戏","LapisId":"romw"},{"ButtonImagePath":"pt/pc/rop.jpg","DisplayName":"超级轮盘","typeid":"桌牌游戏","LapisId":"rop"},{"ButtonImagePath":"pt/pc/rop_g.jpg","DisplayName":"奖金轮盘赌专家","typeid":"桌牌游戏","LapisId":"rop_g"},{"ButtonImagePath":"pt/pc/rps.jpg","DisplayName":"石头、剪刀、纸游戏","typeid":"街机游戏","LapisId":"rps"},{"ButtonImagePath":"pt/pc/samz.jpg","DisplayName":"亚马逊的秘密","typeid":"老虎机","LapisId":"samz"},{"ButtonImagePath":"pt/pc/sb.jpg","DisplayName":"骰宝","typeid":"桌牌游戏","LapisId":"sb"},{"ButtonImagePath":"pt/pc/sbl.jpg","DisplayName":"真人骰子","typeid":"视讯扑克","LapisId":"sbl"},{"ButtonImagePath":"pt/pc/sc.jpg","DisplayName":"保险箱探宝","typeid":"老虎机","LapisId":"sc"},{"ButtonImagePath":"pt/pc/sf.jpg","DisplayName":"苏丹的财富","typeid":"老虎机","LapisId":"sf"},{"ButtonImagePath":"pt/pc/sfh.jpg","DisplayName":"非洲炙热","typeid":"老虎机","LapisId":"sfh"},{"ButtonImagePath":"pt/pc/shmst.jpg","DisplayName":"夏洛克的秘密","typeid":"老虎机","LapisId":"shmst"},{"ButtonImagePath":"pt/pc/sibhtml5sib.jpg","DisplayName":"银弹","typeid":"老虎机","LapisId":"sib html5 : sib"},{"ButtonImagePath":"pt/pc/sis.jpg","DisplayName":"沉默的武士","typeid":"老虎机","LapisId":"sis"},{"ButtonImagePath":"pt/pc/snsb.jpg","DisplayName":"Sunset Beach","typeid":"老虎机","LapisId":"snsb"},{"ButtonImagePath":"pt/pc/sol.jpg","DisplayName":"好运连胜","typeid":"老虎机","LapisId":"sol"},{"ButtonImagePath":"pt/pc/spidc.jpg","DisplayName":"蜘蛛侠：绿妖精的攻击","typeid":"老虎机","LapisId":"spidc"},{"ButtonImagePath":"pt/pc/srcg.jpg","DisplayName":"Spud o Reilly's Crops of Gold","typeid":"老虎机","LapisId":"srcg"},{"ButtonImagePath":"pt/pc/sro.jpg","DisplayName":"轮盘赌刮刮乐","typeid":"刮刮卡","LapisId":"sro"},{"ButtonImagePath":"pt/pc/ssa.jpg","DisplayName":"圣诞刮刮乐","typeid":"刮刮卡","LapisId":"ssa"},{"ButtonImagePath":"pt/pc/ssl.jpg","DisplayName":"转轴经典3","typeid":"老虎机","LapisId":"ssl"},{"ButtonImagePath":"pt/pc/ssp.jpg","DisplayName":"圣诞奇迹","typeid":"老虎机","LapisId":"ssp"},{"ButtonImagePath":"pt/pc/str.jpg","DisplayName":"异乎寻常","typeid":"桌牌游戏","LapisId":"str"},{"ButtonImagePath":"pt/pc/sx.jpg","DisplayName":"四象","typeid":"老虎机","LapisId":"sx"},{"ButtonImagePath":"pt/pc/ta.jpg","DisplayName":"三个朋友","typeid":"老虎机","LapisId":"ta"},{"ButtonImagePath":"pt/pc/tfs.jpg","DisplayName":"顶级王牌-全星","typeid":"老虎机","LapisId":"tfs"},{"ButtonImagePath":"pt/pc/tglalcs.jpg","DisplayName":"炼金术士的法术","typeid":"老虎机","LapisId":"tglalcs"},{"ButtonImagePath":"pt/pc/thtk.jpg","DisplayName":"泰寺","typeid":"老虎机","LapisId":"thtk"},{"ButtonImagePath":"pt/pc/tmqd.jpg","DisplayName":"三剑客和女王","typeid":"老虎机","LapisId":"tmqd"},{"ButtonImagePath":"pt/pc/tob.jpg","DisplayName":"对十高手","typeid":"视讯扑克","LapisId":"tob"},{"ButtonImagePath":"pt/pc/topg.jpg","DisplayName":"Top Gun","typeid":"老虎机","LapisId":"topg"},{"ButtonImagePath":"pt/pc/tp.jpg","DisplayName":"三倍利润","typeid":"老虎机","LapisId":"tp"},{"ButtonImagePath":"pt/pc/tpd2.jpg","DisplayName":"泰国天堂","typeid":"老虎机","LapisId":"tpd2"},{"ButtonImagePath":"pt/pc/tps.jpg","DisplayName":"戴图理神奇7","typeid":"街机游戏","LapisId":"tps"},{"ButtonImagePath":"pt/pc/tqp.jpg","DisplayName":"龙舌兰扑克","typeid":"桌牌游戏","LapisId":"tqp"},{"ButtonImagePath":"pt/pc/tr.jpg","DisplayName":"热带滚筒","typeid":"老虎机","LapisId":"tr"},{"ButtonImagePath":"pt/pc/trl.jpg","DisplayName":"真爱","typeid":"老虎机","LapisId":"trl"},{"ButtonImagePath":"pt/pc/trm.jpg","DisplayName":"强大的复仇者雷神","typeid":"老虎机","LapisId":"trm"},{"ButtonImagePath":"pt/pc/ts.jpg","DisplayName":"时空过客","typeid":"老虎机","LapisId":"ts"},{"ButtonImagePath":"pt/pc/tst.jpg","DisplayName":"网球明星","typeid":"老虎机","LapisId":"tst"},{"ButtonImagePath":"pt/pc/ttc.jpg","DisplayName":"顶级王牌-明星","typeid":"老虎机","LapisId":"ttc"},{"ButtonImagePath":"pt/pc/ttwfs.jpg","DisplayName":"Top Trumps Football Stars 2014","typeid":"老虎机","LapisId":"ttwfs"},{"ButtonImagePath":"pt/pc/ub.jpg","DisplayName":"丛林巫师","typeid":"老虎机","LapisId":"ub"},{"ButtonImagePath":"pt/pc/vbal.jpg","DisplayName":"VIP百家乐","typeid":"视讯扑克","LapisId":"vbal"},{"ButtonImagePath":"pt/pc/vcstd.jpg","DisplayName":"豪华的开心假期","typeid":"老虎机","LapisId":"vcstd"},{"ButtonImagePath":"pt/pc/whk.jpg","DisplayName":"白狮王","typeid":"老虎机","LapisId":"whk"},{"ButtonImagePath":"pt/pc/wis.jpg","DisplayName":"我心狂野","typeid":"老虎机","LapisId":"wis"},{"ButtonImagePath":"pt/pc/wlg.jpg","DisplayName":"舞龙","typeid":"老虎机","LapisId":"wlg"},{"ButtonImagePath":"pt/pc/wsffr.jpg","DisplayName":"玩转华尔街","typeid":"老虎机","LapisId":"wsffr"},{"ButtonImagePath":"pt/pc/wv.jpg","DisplayName":"野蛮海盗","typeid":"桌牌游戏","LapisId":"wv"},{"ButtonImagePath":"pt/pc/wvm.jpg","DisplayName":"金刚狼","typeid":"老虎机","LapisId":"wvm"},{"ButtonImagePath":"pt/pc/xmn.jpg","DisplayName":"X战警","typeid":"老虎机","LapisId":"xmn"},{"ButtonImagePath":"pt/pc/xmn50.jpg","DisplayName":"X战警50条线","typeid":"老虎机","LapisId":"xmn50"},{"ButtonImagePath":"pt/pc/zcjb.jpg","DisplayName":"招财进宝","typeid":"老虎机","LapisId":"zcjb"},{"ButtonImagePath":"pt/pc/zctz.jpg","DisplayName":"招财童子","typeid":"老虎机","LapisId":"zctz"}];
var qtdata=[{"ButtonImagePath":"qt/pc/DolphinGoldStellarJackpots.jpg","DisplayName":"闪光海豚-星空大战","typeid":"1","LapisId":"OGS-dolphingoldstellarjackpots"},{"ButtonImagePath":"qt/pc/WellofWonders.jpg","DisplayName":"魔幻森林","typeid":"1","LapisId":"OGS-wellofwonders"},{"ButtonImagePath":"qt/pc/LeprechaunHills.jpg","DisplayName":"精灵山","typeid":"1","LapisId":"QS-leprechaunhills"},{"ButtonImagePath":"qt/pc/KoiGate.jpg","DisplayName":"鲤鱼门","typeid":"1","LapisId":"HAB-koigate"},{"ButtonImagePath":"qt/pc/Snowflakes.jpg","DisplayName":"雪绒花","typeid":"1","LapisId":"OGS-snowflakes"},{"ButtonImagePath":"qt/pc/DragonShrine.jpg","DisplayName":"龙神殿","typeid":"4","LapisId":"QS-dragonshrine"},{"ButtonImagePath":"qt/pc/MadMadMonkey.jpg","DisplayName":"疯狂的猴子","typeid":"4","LapisId":"OGS-madmadmonkey"},{"ButtonImagePath":"qt/pc/Fenghuang.png","DisplayName":"凤凰","typeid":"4","LapisId":"HAB-fenghuang"},{"ButtonImagePath":"qt/pc/SecondStrike.jpg","DisplayName":"连环炮","typeid":"4","LapisId":"QS-secondstrike"},{"ButtonImagePath":"qt/pc/Bonanza.jpg","DisplayName":"矿山探险","typeid":"4","LapisId":"OGS-bonanza"},{"ButtonImagePath":"qt/pc/EsqueletoExplosivo.jpg","DisplayName":"骷髅乐队","typeid":"4","LapisId":"OGS-esqueletoexplosivo"},{"ButtonImagePath":"qt/pc/JewelBlast.jpg","DisplayName":"宝石爆炸","typeid":"4","LapisId":"QS-jewelblast"},{"ButtonImagePath":"qt/pc/HotSync.jpg","DisplayName":"火热旋转","typeid":"4","LapisId":"QS-hotsync"},{"ButtonImagePath":"qt/pc/PhoenixSun.jpg","DisplayName":"凤凰的涅槃","typeid":"4","LapisId":"QS-phoenixsun"},{"ButtonImagePath":"qt/pc/FrogGrog.jpg","DisplayName":"格罗格醉蛙","typeid":"4","LapisId":"OGS-froggrog"},{"ButtonImagePath":"qt/pc/FaCaiShen.png","DisplayName":"发财神","typeid":"4","LapisId":"HAB-facaishen"},{"ButtonImagePath":"qt/pc/WildToro.jpg","DisplayName":"卑鄙的斗牛士","typeid":"4","LapisId":"ELK-wildtoro"},{"ButtonImagePath":"qt/pc/SevensHigh.jpg","DisplayName":"疯狂777","typeid":"4","LapisId":"QS-sevenshigh"},{"ButtonImagePath":"qt/pc/GeniesTouch.jpg","DisplayName":"阿拉丁神灯","typeid":"4","LapisId":"QS-geniestouch"},{"ButtonImagePath":"qt/pc/FireRooster.png","DisplayName":"公鸡王","typeid":"4","LapisId":"HAB-firerooster"},{"ButtonImagePath":"qt/pc/TokiTime.jpg","DisplayName":"开心消消乐","typeid":"4","LapisId":"OGS-tokitime"},{"ButtonImagePath":"qt/pc/Poltava.jpg","DisplayName":"波尔塔瓦之战","typeid":"4","LapisId":"ELK-poltava"},{"ButtonImagePath":"qt/pc/SamontheBeach.jpg","DisplayName":"夏日海滩","typeid":"4","LapisId":"ELK-samonthebeach"},{"ButtonImagePath":"qt/pc/Golden.png","DisplayName":"金鸡报喜","typeid":"4","LapisId":"OGS-golden"},{"ButtonImagePath":"qt/pc/KoiGate.jpg","DisplayName":"鲤鱼门","typeid":"4","LapisId":"HAB-koigate"},{"ButtonImagePath":"qt/pc/OceansCall.jpg","DisplayName":"海洋之音","typeid":"4","LapisId":"HAB-oceanscall"},{"ButtonImagePath":"qt/pc/GoldLab.jpg","DisplayName":"黄金实验室","typeid":"4","LapisId":"QS-goldlab"},{"ButtonImagePath":"qt/pc/VolcanoEruption.jpg","DisplayName":"火山爆发","typeid":"4","LapisId":"OGS-volcanoeruption"},{"ButtonImagePath":"qt/pc/KingColossus.jpg","DisplayName":"国王巨像","typeid":"4","LapisId":"QS-kingcolossus"},{"ButtonImagePath":"qt/pc/DJWild.jpg","DisplayName":"狂野DJ","typeid":"4","LapisId":"ELK-djwild"},{"ButtonImagePath":"qt/pc/ElectricSam.jpg","DisplayName":"美女和野兽","typeid":"4","LapisId":"ELK-electricsam"},{"ButtonImagePath":"qt/pc/SilverLion.jpg","DisplayName":"万兽之王","typeid":"4","LapisId":"OGS-silverlion"},{"ButtonImagePath":"qt/pc/Razortooth.jpg","DisplayName":"虎口狼牙","typeid":"4","LapisId":"QS-razortooth"},{"ButtonImagePath":"qt/pc/AstroCat.jpg","DisplayName":"阿斯特罗猫","typeid":"4","LapisId":"OGS-astrocat"},{"ButtonImagePath":"qt/pc/Firestorm.jpg","DisplayName":"火暴","typeid":"4","LapisId":"QS-firestorm"},{"ButtonImagePath":"qt/pc/1can2can.jpg","DisplayName":"丛林鹦鹉","typeid":"2","LapisId":"OGS-1can2can"},{"ButtonImagePath":"qt/pc/300Shields.jpg","DisplayName":"斯巴达之盾","typeid":"2","LapisId":"OGS-300shields"},{"ButtonImagePath":"qt/pc/5Knights.jpg","DisplayName":"圣骑士","typeid":"2","LapisId":"OGS-5knights"},{"ButtonImagePath":"qt/pc/ADragonStory.jpg","DisplayName":"龙的传说","typeid":"2","LapisId":"OGS-adragonstory"},{"ButtonImagePath":"qt/pc/AWhileOnTheNile.jpg","DisplayName":"尼罗河探险","typeid":"2","LapisId":"OGS-awhileonthenile"},{"ButtonImagePath":"qt/pc/AndretheGiant.jpg","DisplayName":"巨人安德鲁","typeid":"2","LapisId":"OGS-andrethegiant"},{"ButtonImagePath":"qt/pc/AnEveningWithHollyMadison.jpg","DisplayName":"霍莉麦迪逊之夜","typeid":"2","LapisId":"OGS-aneveningwithhollymadison"},{"ButtonImagePath":"qt/pc/BangkokNights.jpg","DisplayName":"曼谷奇妙夜","typeid":"2","LapisId":"OGS-bangkoknights"},{"ButtonImagePath":"qt/pc/BigFoot.jpg","DisplayName":"寻找大脚怪","typeid":"2","LapisId":"OGS-bigfoot"},{"ButtonImagePath":"qt/pc/BingoBillion.jpg","DisplayName":"百万宾果","typeid":"2","LapisId":"OGS-bingobillions"},{"ButtonImagePath":"qt/pc/BloodloreVampireClan.jpg","DisplayName":"吸血鬼家族","typeid":"2","LapisId":"OGS-bloodlorevampireclan"},{"ButtonImagePath":"qt/pc/Bobby7s.jpg","DisplayName":"蠢贼鲍比","typeid":"2","LapisId":"OGS-bobby7s"},{"ButtonImagePath":"qt/pc/BuccaneersBay.jpg","DisplayName":"海盗湾","typeid":"2","LapisId":"OGS-bucaneersbay"},{"ButtonImagePath":"qt/pc/Butterflies.jpg","DisplayName":"蝴蝶物语","typeid":"2","LapisId":"OGS-butterflies"},{"ButtonImagePath":"qt/pc/CaliforniaGold.jpg","DisplayName":"加州掘金","typeid":"2","LapisId":"OGS-californiagold"},{"ButtonImagePath":"qt/pc/CalloftheColosseum.jpg","DisplayName":"罗马竞技场","typeid":"2","LapisId":"OGS-callofthecolosseum"},{"ButtonImagePath":"qt/pc/CashStampede.jpg","DisplayName":"现金踩踏","typeid":"2","LapisId":"OGS-cashstampede"},{"ButtonImagePath":"qt/pc/Casinomeister.jpg","DisplayName":"赌场大亨","typeid":"2","LapisId":"OGS-casinomeister"},{"ButtonImagePath":"qt/pc/CharmsandWitches.jpg","DisplayName":"女巫的魔咒","typeid":"2","LapisId":"OGS-charmsandwitches"},{"ButtonImagePath":"qt/pc/CherryBlossoms.jpg","DisplayName":"樱花恋","typeid":"2","LapisId":"OGS-cherryblossoms"},{"ButtonImagePath":"qt/pc/Crocodopolis.jpg","DisplayName":"阿努比斯","typeid":"2","LapisId":"OGS-crocodopolis"},{"ButtonImagePath":"qt/pc/DoctorLove.jpg","DisplayName":"爱情医生","typeid":"2","LapisId":"OGS-doctorlove"},{"ButtonImagePath":"qt/pc/DoublePlaySuperBet.jpg","DisplayName":"超级赢家","typeid":"2","LapisId":"OGS-doubleplaysuperbet"},{"ButtonImagePath":"qt/pc/DotorLoveOnVacation.jpg","DisplayName":"爱情假日","typeid":"2","LapisId":"OGS-drloveonvacation"},{"ButtonImagePath":"qt/pc/DragonDrop.jpg","DisplayName":"驯龙记","typeid":"2","LapisId":"OGS-dragondrop"},{"ButtonImagePath":"qt/pc/Dynasty.jpg","DisplayName":"大清王朝","typeid":"2","LapisId":"OGS-dynasty"},{"ButtonImagePath":"qt/pc/EasternDragon.jpg","DisplayName":"东海龙王","typeid":"2","LapisId":"OGS-easterndragon"},{"ButtonImagePath":"qt/pc/EasySlider.jpg","DisplayName":"公路漂移","typeid":"2","LapisId":"OGS-easyslider"},{"ButtonImagePath":"qt/pc/EgyptianRise.jpg","DisplayName":"埃及的崛起","typeid":"2","LapisId":"OGS-egyptianrise"},{"ButtonImagePath":"qt/pc/EmeraldIsle.jpg","DisplayName":"翡翠岛","typeid":"2","LapisId":"OGS-emeraldisle"},{"ButtonImagePath":"qt/pc/EmperorsGarden.jpg","DisplayName":"皇庭踏青","typeid":"2","LapisId":"OGS-emperorsgarden"},{"ButtonImagePath":"qt/pc/EnchantedMermaid.jpg","DisplayName":"人鱼公主","typeid":"2","LapisId":"OGS-enchantedmermaid"},{"ButtonImagePath":"qt/pc/ExtraCash.jpg","DisplayName":"好多钞票","typeid":"2","LapisId":"OGS-extracash"},{"ButtonImagePath":"qt/pc/FairiesForest.jpg","DisplayName":"绿野仙踪","typeid":"2","LapisId":"OGS-fairiesforest"},{"ButtonImagePath":"qt/pc/FiestaCubana.png","DisplayName":"热辣古巴","typeid":"2","LapisId":"OGS-fiestacubana"},{"ButtonImagePath":"qt/pc/FireHawk.jpg","DisplayName":"印第安火鹰","typeid":"2","LapisId":"OGS-firehawk"},{"ButtonImagePath":"qt/pc/FoxinWins.jpg","DisplayName":"狐狸家族","typeid":"2","LapisId":"OGS-foxinwins"},{"ButtonImagePath":"qt/pc/FoxinWinsAgain.jpg","DisplayName":"狐狸家族再临","typeid":"2","LapisId":"OGS-foxinwinsagain"},{"ButtonImagePath":"qt/pc/GoldAhoy.jpg","DisplayName":"黄金海盗","typeid":"2","LapisId":"OGS-goldahoy"},{"ButtonImagePath":"qt/pc/Golden.png","DisplayName":"金鸡报喜","typeid":"2","LapisId":"OGS-golden"},{"ButtonImagePath":"qt/pc/GloriousEmpire.jpg","DisplayName":"帝国荣耀","typeid":"2","LapisId":"OGS-gloriousempire"},{"ButtonImagePath":"qt/pc/GiantGems.jpg","DisplayName":"巨型宝石","typeid":"2","LapisId":"OGS-giantgems"},{"ButtonImagePath":"qt/pc/GorillaGoWild.jpg","DisplayName":"疯狂大猩猩","typeid":"2","LapisId":"OGS-gorillagowild"},{"ButtonImagePath":"qt/pc/GenieWild.jpg","DisplayName":"精灵仙子","typeid":"2","LapisId":"OGS-geniewild"},{"ButtonImagePath":"qt/pc/GreatWildElk.jpg","DisplayName":"麋鹿之光","typeid":"2","LapisId":"OGS-greatwildelk"},{"ButtonImagePath":"qt/pc/HotRoller.jpg","DisplayName":"激情转轮","typeid":"2","LapisId":"OGS-hotroller"},{"ButtonImagePath":"qt/pc/IrishEyes.jpg","DisplayName":"爱尔兰之眼","typeid":"2","LapisId":"OGS-irisheyes"},{"ButtonImagePath":"qt/pc/IrishEyes2.jpg","DisplayName":"爱尔兰之眼2","typeid":"2","LapisId":"OGS-irisheyes2"},{"ButtonImagePath":"qt/pc/JackpotJester50k.jpg","DisplayName":"小丑奖池","typeid":"2","LapisId":"OGS-jackpotjester50k"},{"ButtonImagePath":"qt/pc/JackpotJesterWildNudge.jpg","DisplayName":"头奖幸运小丑","typeid":"2","LapisId":"OGS-jackpotjesterwildnudge"},{"ButtonImagePath":"qt/pc/JacksBeanstalk.jpg","DisplayName":"杰克的豌豆唠","typeid":"2","LapisId":"OGS-jacksbeanstalk"},{"ButtonImagePath":"qt/pc/JamesDean.jpg","DisplayName":"詹姆斯·迪恩","typeid":"2","LapisId":"OGS-jamesdean"},{"ButtonImagePath":"qt/pc/JokerJester.jpg","DisplayName":"滑稽小丑","typeid":"2","LapisId":"OGS-jokerjester"},{"ButtonImagePath":"qt/pc/JudgeDredd.jpg","DisplayName":"特警判官","typeid":"2","LapisId":"OGS-judgedredd"},{"ButtonImagePath":"qt/pc/Jukepot.jpg","DisplayName":"音乐盒","typeid":"2","LapisId":"OGS-jukepot"},{"ButtonImagePath":"qt/pc/KingKong.jpg","DisplayName":"金刚","typeid":"2","LapisId":"OGS-kingkong"},{"ButtonImagePath":"qt/pc/KingTiger.jpg","DisplayName":"虎王","typeid":"2","LapisId":"OGS-kingtiger"},{"ButtonImagePath":"qt/pc/LaCucaracha.jpg","DisplayName":"狂野墨西哥","typeid":"2","LapisId":"OGS-lacucaracha"},{"ButtonImagePath":"qt/pc/LoveBugs.jpg","DisplayName":"恋爱毛毛虫","typeid":"2","LapisId":"OGS-lovebugs"},{"ButtonImagePath":"qt/pc/MadMadMonkey.jpg","DisplayName":"疯狂的猴子","typeid":"2","LapisId":"OGS-madmadmonkey"},{"ButtonImagePath":"qt/pc/MaidOMoney.jpg","DisplayName":"拜金女仆","typeid":"2","LapisId":"OGS-maidomoney"},{"ButtonImagePath":"qt/pc/ManicMillions.jpg","DisplayName":"科学怪人","typeid":"2","LapisId":"OGS-manicmillions"},{"ButtonImagePath":"qt/pc/Medusa.jpg","DisplayName":"美杜莎","typeid":"2","LapisId":"OGS-medusa"},{"ButtonImagePath":"qt/pc/Medusa2.jpg","DisplayName":"美杜莎2","typeid":"2","LapisId":"OGS-medusaii"},{"ButtonImagePath":"qt/pc/MerlinsMagicRespins.jpg","DisplayName":"梅林的魔法药剂","typeid":"2","LapisId":"OGS-merlinsmagicrespins"},{"ButtonImagePath":"qt/pc/MerlinsMagicRespinsChristmas.jpg","DisplayName":"圣诞版-梅林魔法","typeid":"2","LapisId":"OGS-merlinsmagicrespinschristmas"},{"ButtonImagePath":"qt/pc/MerlinsMillionsSuperbet.jpg","DisplayName":"梅林的法宝","typeid":"2","LapisId":"OGS-merlinsmillionssuperbet"},{"ButtonImagePath":"qt/pc/MissMidas.jpg","DisplayName":"蜜达斯小姐","typeid":"2","LapisId":"OGS-missmidas"},{"ButtonImagePath":"qt/pc/MonsterWins.jpg","DisplayName":"怪兽大学","typeid":"2","LapisId":"OGS-monsterwins"},{"ButtonImagePath":"qt/pc/Munchers.jpg","DisplayName":"血腥大棚","typeid":"2","LapisId":"OGS-munchers"},{"ButtonImagePath":"qt/pc/NapoleonBoneyParts.jpg","DisplayName":"骨头大帝拿破仑","typeid":"2","LapisId":"OGS-napoleonboneyparts"},{"ButtonImagePath":"qt/pc/OilMania.jpg","DisplayName":"狂热石油","typeid":"2","LapisId":"OGS-oilmania"},{"ButtonImagePath":"qt/pc/OwlEyes.jpg","DisplayName":"猫头鹰之眼","typeid":"2","LapisId":"OGS-owleyes"},{"ButtonImagePath":"qt/pc/Pandamania.jpg","DisplayName":"熊猫团圆","typeid":"2","LapisId":"OGS-pandamania"},{"ButtonImagePath":"qt/pc/PizzaPrize.jpg","DisplayName":"披萨锦标赛","typeid":"2","LapisId":"OGS-pizzaprize"},{"ButtonImagePath":"qt/pc/PotionCommotion.jpg","DisplayName":"骚动药水","typeid":"2","LapisId":"OGS-potioncommotion"},{"ButtonImagePath":"qt/pc/Psycho.jpg","DisplayName":"神经兮兮","typeid":"2","LapisId":"OGS-psycho"},{"ButtonImagePath":"qt/pc/RamessesRiches.jpg","DisplayName":"拉美西斯宝藏","typeid":"2","LapisId":"OGS-ramessesriches"},{"ButtonImagePath":"qt/pc/RobinHoodPrinceofTweets.jpg","DisplayName":"绿林英雄—罗宾汉","typeid":"2","LapisId":"OGS-robinhoodprinceoftweets"},{"ButtonImagePath":"qt/pc/ShaaarkSuperbet.jpg","DisplayName":"鲨鱼出没","typeid":"2","LapisId":"OGS-shaaarksuperbet"},{"ButtonImagePath":"qt/pc/Snowflakes.jpg","DisplayName":"雪绒花","typeid":"2","LapisId":"OGS-snowflakes"},{"ButtonImagePath":"qt/pc/SpanishEyes.jpg","DisplayName":"情定西班牙","typeid":"2","LapisId":"OGS-spanisheyes"},{"ButtonImagePath":"qt/pc/SpinSorceress.jpg","DisplayName":"旋转女巫","typeid":"2","LapisId":"OGS-spinsorceress"},{"ButtonImagePath":"qt/pc/Starmania.jpg","DisplayName":"星星点灯","typeid":"2","LapisId":"OGS-starmania"},{"ButtonImagePath":"qt/pc/SuperSafari.jpg","DisplayName":"丛林之王","typeid":"2","LapisId":"OGS-supersafari"},{"ButtonImagePath":"qt/pc/TeddybearsPicnic.jpg","DisplayName":"可爱泰迪","typeid":"2","LapisId":"OGS-teddybearspicnic"},{"ButtonImagePath":"qt/pc/TheBermudaMysteries.jpg","DisplayName":"百慕大之谜","typeid":"2","LapisId":"OGS-thebermudamysteries"},{"ButtonImagePath":"qt/pc/TheCodFather.jpg","DisplayName":"鳕鱼之父","typeid":"2","LapisId":"OGS-thecodfather"},{"ButtonImagePath":"qt/pc/TheSnakeCharmer.jpg","DisplayName":"耍蛇老爹","typeid":"2","LapisId":"OGS-thesnakecharmer"},{"ButtonImagePath":"qt/pc/TheSpinLab.jpg","DisplayName":"旋转实验室","typeid":"2","LapisId":"OGS-thespinlab"},{"ButtonImagePath":"qt/pc/TitanStorm.jpg","DisplayName":"泰坦风暴","typeid":"2","LapisId":"OGS-titanstorm"},{"ButtonImagePath":"qt/pc/TootinCarman.jpg","DisplayName":"卡车嘟嘟","typeid":"2","LapisId":"OGS-tootincarman"},{"ButtonImagePath":"qt/pc/UnicornLegend.jpg","DisplayName":"独角兽传奇","typeid":"2","LapisId":"OGS-unicornlegend"},{"ButtonImagePath":"qt/pc/VenetianRose.jpg","DisplayName":"情定威尼斯","typeid":"2","LapisId":"OGS-venetianrose"},{"ButtonImagePath":"qt/pc/VolcanoEruption.jpg","DisplayName":"火山爆发","typeid":"2","LapisId":"OGS-volcanoeruption"},{"ButtonImagePath":"qt/pc/WildcatCanyon.jpg","DisplayName":"野猫峡谷","typeid":"2","LapisId":"OGS-wildcatcanyon"},{"ButtonImagePath":"qt/pc/WitchPickings.jpg","DisplayName":"美妙巫师","typeid":"2","LapisId":"OGS-witchpickings"},{"ButtonImagePath":"qt/pc/WildWest.jpg","DisplayName":"狂野西部","typeid":"2","LapisId":"OGS-wildwest"},{"ButtonImagePath":"qt/pc/WolfpackPays.jpg","DisplayName":"狼群嚎叫","typeid":"2","LapisId":"OGS-wolfpackpays"},{"ButtonImagePath":"qt/pc/Bonanza.jpg","DisplayName":"矿山探险","typeid":"2","LapisId":"OGS-bonanza"},{"ButtonImagePath":"qt/pc/DragonBorn.jpg","DisplayName":"龙腾","typeid":"2","LapisId":"OGS-dragonborn"},{"ButtonImagePath":"qt/pc/Gold.jpg","DisplayName":"黄金","typeid":"2","LapisId":"OGS-gold"},{"ButtonImagePath":"qt/pc/QueenofRiches.jpg","DisplayName":"埃及艳后","typeid":"2","LapisId":"OGS-queenofriches"},{"ButtonImagePath":"qt/pc/StarQuest.jpg","DisplayName":"星际探索","typeid":"2","LapisId":"OGS-starquest"},{"ButtonImagePath":"qt/pc/AladdinsLegacy.jpg","DisplayName":"阿拉丁的遗产","typeid":"2","LapisId":"OGS-aladdinslegacy"},{"ButtonImagePath":"qt/pc/BarsandBells.jpg","DisplayName":"酒吧门铃","typeid":"2","LapisId":"OGS-barsandbells"},{"ButtonImagePath":"qt/pc/Dragon8S.png","DisplayName":"龙8","typeid":"2","LapisId":"OGS-dragon8s"},{"ButtonImagePath":"qt/pc/FortunesoftheAmazons.jpg","DisplayName":"亚马逊财富","typeid":"2","LapisId":"OGS-fortunesoftheamazon"},{"ButtonImagePath":"qt/pc/GulliversTrabels.jpg","DisplayName":"格力菲游记","typeid":"2","LapisId":"OGS-gulliverstravels"},{"ButtonImagePath":"qt/pc/Leonidas.jpg","DisplayName":"莱奥尼达斯国王","typeid":"2","LapisId":"OGS-leonidas"},{"ButtonImagePath":"qt/pc/ShogunShowdown.jpg","DisplayName":"决战将军","typeid":"2","LapisId":"OGS-shogunshowdown"},{"ButtonImagePath":"qt/pc/SinfulSpins.jpg","DisplayName":"罪恶旋转","typeid":"2","LapisId":"OGS-sinfulspins"},{"ButtonImagePath":"qt/pc/ThunderingZeus.jpg","DisplayName":"雷鸣宙斯","typeid":"2","LapisId":"OGS-thunderingzeus"},{"ButtonImagePath":"qt/pc/VampiresvsWerewolves.jpg","DisplayName":"吸血鬼大战狼人","typeid":"2","LapisId":"OGS-vampiresvswerewolves"},{"ButtonImagePath":"qt/pc/AngelsTouch.jpg","DisplayName":"天使的触摸","typeid":"2","LapisId":"OGS-angelstouch"},{"ButtonImagePath":"qt/pc/AstroCat.jpg","DisplayName":"阿斯特罗猫","typeid":"2","LapisId":"OGS-astrocat"},{"ButtonImagePath":"qt/pc/BlazingGoddess.jpg","DisplayName":"炽热女神","typeid":"2","LapisId":"OGS-blazinggoddess"},{"ButtonImagePath":"qt/pc/Chilligold.jpg","DisplayName":"黄金辣椒","typeid":"2","LapisId":"OGS-chilligold"},{"ButtonImagePath":"qt/pc/ChilliGold2.jpg","DisplayName":"黄金辣椒2","typeid":"2","LapisId":"OGS-chilligoldx2"},{"ButtonImagePath":"qt/pc/DiamondTower.jpg","DisplayName":"钻石大厦","typeid":"2","LapisId":"OGS-diamondtower"},{"ButtonImagePath":"qt/pc/DolphinGold.png","DisplayName":"闪光海豚","typeid":"2","LapisId":"OGS-dolphingold"},{"ButtonImagePath":"qt/pc/DolphinGoldStellarJackpots.jpg","DisplayName":"闪光海豚-星空大战","typeid":"2","LapisId":"OGS-dolphingoldstellarjackpots"},{"ButtonImagePath":"qt/pc/DragonPalace.jpg","DisplayName":"龙宫","typeid":"2","LapisId":"OGS-dragonpalace"},{"ButtonImagePath":"qt/pc/DruidessGold.jpg","DisplayName":"夺金巫师","typeid":"2","LapisId":"OGS-druidessgold"},{"ButtonImagePath":"qt/pc/Fortune8Cat.jpg","DisplayName":"幸运招财猫","typeid":"2","LapisId":"OGS-fortune8cat"},{"ButtonImagePath":"qt/pc/FrogsnFlies.jpg","DisplayName":"捕蝇大赛","typeid":"2","LapisId":"OGS-frogsnflies"},{"ButtonImagePath":"qt/pc/LostTemple.jpg","DisplayName":"消失的神庙","typeid":"2","LapisId":"OGS-losttemple"},{"ButtonImagePath":"qt/pc/MoonTemple.jpg","DisplayName":"月光神庙","typeid":"2","LapisId":"OGS-moontemple"},{"ButtonImagePath":"qt/pc/MoreMonkeys.jpg","DisplayName":"齐天大圣","typeid":"2","LapisId":"OGS-moremonkeys"},{"ButtonImagePath":"qt/pc/PixieGold.jpg","DisplayName":"调皮精灵","typeid":"2","LapisId":"OGS-pixiegold"},{"ButtonImagePath":"qt/pc/SerengetiDiamonds.jpg","DisplayName":"塞伦盖蒂之钻","typeid":"2","LapisId":"OGS-serengetidiamonds"},{"ButtonImagePath":"qt/pc/SilverLion.jpg","DisplayName":"万兽之王","typeid":"2","LapisId":"OGS-silverlion"},{"ButtonImagePath":"qt/pc/SamuraiPrincess.jpg","DisplayName":"武士公主","typeid":"2","LapisId":"OGS-samuraiprincess"},{"ButtonImagePath":"qt/pc/VikingFire.jpg","DisplayName":"维京之火","typeid":"2","LapisId":"OGS-vikingfire"},{"ButtonImagePath":"qt/pc/1429UnchartedSeas.jpg","DisplayName":"未知海洋探险-1429","typeid":"2","LapisId":"OGS-1429unchartedseas"},{"ButtonImagePath":"qt/pc/Arcader.jpg","DisplayName":"神秘的巨石","typeid":"2","LapisId":"OGS-arcader"},{"ButtonImagePath":"qt/pc/Babushkas.jpg","DisplayName":"俄罗斯老太","typeid":"2","LapisId":"OGS-babushkas"},{"ButtonImagePath":"qt/pc/BirdsOnAWire.jpg","DisplayName":"愤怒的小鸟","typeid":"2","LapisId":"OGS-birdsonawire"},{"ButtonImagePath":"qt/pc/EsqueletoExplosivo.jpg","DisplayName":"骷髅乐队","typeid":"2","LapisId":"OGS-esqueletoexplosivo"},{"ButtonImagePath":"qt/pc/Flux.jpg","DisplayName":"水晶宝藏","typeid":"2","LapisId":"OGS-flux"},{"ButtonImagePath":"qt/pc/FrogGrog.jpg","DisplayName":"格罗格醉蛙","typeid":"2","LapisId":"OGS-froggrog"},{"ButtonImagePath":"qt/pc/FruitWarp.jpg","DisplayName":"水果大战","typeid":"2","LapisId":"OGS-fruitwarp"},{"ButtonImagePath":"qt/pc/Magicious.jpg","DisplayName":"魔术大师","typeid":"2","LapisId":"OGS-magicious"},{"ButtonImagePath":"qt/pc/Spectra.jpg","DisplayName":"光谱","typeid":"2","LapisId":"OGS-spectra"},{"ButtonImagePath":"qt/pc/SunsetDelight.jpg","DisplayName":"日落余晖","typeid":"2","LapisId":"OGS-sunsetdelight"},{"ButtonImagePath":"qt/pc/TheRift.jpg","DisplayName":"密室逃亡","typeid":"2","LapisId":"OGS-therift"},{"ButtonImagePath":"qt/pc/TokiTime.jpg","DisplayName":"开心消消乐","typeid":"2","LapisId":"OGS-tokitime"},{"ButtonImagePath":"qt/pc/TurningTotems.jpg","DisplayName":"旋转图腾","typeid":"2","LapisId":"OGS-turningtotems"},{"ButtonImagePath":"qt/pc/WellofWonders.jpg","DisplayName":"魔幻森林","typeid":"2","LapisId":"OGS-wellofwonders"},{"ButtonImagePath":"qt/pc/Zoom.jpg","DisplayName":"水果幻灯片","typeid":"2","LapisId":"OGS-zoom"},{"ButtonImagePath":"qt/pc/12Zodiacs.png","DisplayName":"十二生肖","typeid":"2","LapisId":"HAB-12zodiacs"},{"ButtonImagePath":"qt/pc/ArcaneElements.png","DisplayName":"神秘元素","typeid":"2","LapisId":"HAB-arcaneelements"},{"ButtonImagePath":"qt/pc/BombsAway.png","DisplayName":"炸弹追击","typeid":"2","LapisId":"HAB-bombsaway"},{"ButtonImagePath":"qt/pc/CoyoteCrash.png","DisplayName":"狼贼夺宝","typeid":"2","LapisId":"HAB-coyotecrash"},{"ButtonImagePath":"qt/pc/DragonsRealm.png","DisplayName":"神龙之境","typeid":"2","LapisId":"HAB-dragonsrealm"},{"ButtonImagePath":"qt/pc/DragonsThrone.png","DisplayName":"龙之宝座","typeid":"2","LapisId":"HAB-dragonsthrone"},{"ButtonImagePath":"qt/pc/FaCaiShen.png","DisplayName":"发财神","typeid":"2","LapisId":"HAB-facaishen"},{"ButtonImagePath":"qt/pc/Fenghuang.png","DisplayName":"凤凰","typeid":"2","LapisId":"HAB-fenghuang"},{"ButtonImagePath":"qt/pc/FireRooster.png","DisplayName":"公鸡王","typeid":"2","LapisId":"HAB-firerooster"},{"ButtonImagePath":"qt/pc/Gangsters.png","DisplayName":"黑手党","typeid":"2","LapisId":"HAB-gangsters"},{"ButtonImagePath":"qt/pc/GoldRush.png","DisplayName":"淘金疯狂","typeid":"2","LapisId":"HAB-goldrush"},{"ButtonImagePath":"qt/pc/IndianCashCatcher.png","DisplayName":"印第安追梦","typeid":"2","LapisId":"HAB-indiancashcatcher"},{"ButtonImagePath":"qt/pc/Jugglenaut.jpg","DisplayName":"惊喜秀","typeid":"2","LapisId":"HAB-jugglenaut"},{"ButtonImagePath":"qt/pc/KoiGate.jpg","DisplayName":"鲤鱼门","typeid":"2","LapisId":"HAB-koigate"},{"ButtonImagePath":"qt/pc/MonsterMashCash.png","DisplayName":"怪物聚集","typeid":"2","LapisId":"HAB-monstermashcash"},{"ButtonImagePath":"qt/pc/MysticFortune.png","DisplayName":"神秘宝藏","typeid":"2","LapisId":"HAB-mysticfortune"},{"ButtonImagePath":"qt/pc/OceansCall.jpg","DisplayName":"海洋之音","typeid":"2","LapisId":"HAB-oceanscall"},{"ButtonImagePath":"qt/pc/RomanEmpire.png","DisplayName":"罗马帝国","typeid":"2","LapisId":"HAB-romanempire"},{"ButtonImagePath":"qt/pc/RuffledUp.png","DisplayName":"触电的小鸟","typeid":"2","LapisId":"HAB-ruffledup"},{"ButtonImagePath":"qt/pc/ShaolinFortunes.png","DisplayName":"少林宝藏","typeid":"2","LapisId":"HAB-shaolinfortunes"},{"ButtonImagePath":"qt/pc/Sparta.png","DisplayName":"斯巴达","typeid":"2","LapisId":"HAB-sparta"},{"ButtonImagePath":"qt/pc/SuperTwister.png","DisplayName":"超级龙卷风","typeid":"2","LapisId":"HAB-supertwister"},{"ButtonImagePath":"qt/pc/WickedWitch.png","DisplayName":"巫婆大财","typeid":"2","LapisId":"HAB-wickedwitch"},{"ButtonImagePath":"qt/pc/BigBadWolf.jpg","DisplayName":"灰太狼","typeid":"2","LapisId":"QS-bigbadwolf"},{"ButtonImagePath":"qt/pc/CrystalQueen.jpg","DisplayName":"冰雪奇缘","typeid":"2","LapisId":"QS-crystalqueen"},{"ButtonImagePath":"qt/pc/DragonShrine.jpg","DisplayName":"龙神殿","typeid":"2","LapisId":"QS-dragonshrine"},{"ButtonImagePath":"qt/pc/Firestorm.jpg","DisplayName":"火暴","typeid":"2","LapisId":"QS-firestorm"},{"ButtonImagePath":"qt/pc/GeniesTouch.jpg","DisplayName":"阿拉丁神灯","typeid":"2","LapisId":"QS-geniestouch"},{"ButtonImagePath":"qt/pc/GoldLab.jpg","DisplayName":"黄金实验室","typeid":"2","LapisId":"QS-goldlab"},{"ButtonImagePath":"qt/pc/HiddenValley.jpg","DisplayName":"神秘山谷","typeid":"2","LapisId":"QS-hiddenvalley"},{"ButtonImagePath":"qt/pc/HotSync.jpg","DisplayName":"火热旋转","typeid":"2","LapisId":"QS-hotsync"},{"ButtonImagePath":"qt/pc/Illuminous.jpg","DisplayName":"嗨翻珠宝","typeid":"2","LapisId":"QS-illuminous"},{"ButtonImagePath":"qt/pc/JewelBlast.jpg","DisplayName":"宝石爆炸","typeid":"2","LapisId":"QS-jewelblast"},{"ButtonImagePath":"qt/pc/KingColossus.jpg","DisplayName":"国王巨像","typeid":"2","LapisId":"QS-kingcolossus"},{"ButtonImagePath":"qt/pc/LeprechaunHills.jpg","DisplayName":"精灵山","typeid":"2","LapisId":"QS-leprechaunhills"},{"ButtonImagePath":"qt/pc/PhoenixSun.jpg","DisplayName":"凤凰的涅槃","typeid":"2","LapisId":"QS-phoenixsun"},{"ButtonImagePath":"qt/pc/Razortooth.jpg","DisplayName":"虎口狼牙","typeid":"2","LapisId":"QS-razortooth"},{"ButtonImagePath":"qt/pc/RoyalFrog.jpg","DisplayName":"皇族青蛙","typeid":"2","LapisId":"QS-royalfrog"},{"ButtonImagePath":"qt/pc/SecondStrike.jpg","DisplayName":"连环炮","typeid":"2","LapisId":"QS-secondstrike"},{"ButtonImagePath":"qt/pc/SevensHigh.jpg","DisplayName":"疯狂777","typeid":"2","LapisId":"QS-sevenshigh"},{"ButtonImagePath":"qt/pc/Sinbad.jpg","DisplayName":"辛巴达","typeid":"2","LapisId":"QS-sinbad"},{"ButtonImagePath":"qt/pc/Spinions.jpg","DisplayName":"小黄人海滩派对","typeid":"2","LapisId":"QS-spinionsbeachparty"},{"ButtonImagePath":"qt/pc/SugarTrail.jpg","DisplayName":"糖果传奇","typeid":"2","LapisId":"QS-sugartrail"},{"ButtonImagePath":"qt/pc/TheEpicJourney.jpg","DisplayName":"史诗之旅","typeid":"2","LapisId":"QS-theepicjourney"},{"ButtonImagePath":"qt/pc/TheThreeMusketeers.jpg","DisplayName":"三剑客","typeid":"2","LapisId":"QS-thethreemusketeers"},{"ButtonImagePath":"qt/pc/TheWildChase.jpg","DisplayName":"侠盗猎车手","typeid":"2","LapisId":"QS-thewildchase "},{"ButtonImagePath":"qt/pc/TitanThunder.jpg","DisplayName":"泰坦雷霆","typeid":"2","LapisId":"QS-titanthunder"},{"ButtonImagePath":"qt/pc/TreasureIsland.jpg","DisplayName":"宝藏岛","typeid":"2","LapisId":"QS-treasureisland"},{"ButtonImagePath":"qt/pc/Bloopers.jpg","DisplayName":"幕后花絮","typeid":"2","LapisId":"ELK-bloopers"},{"ButtonImagePath":"qt/pc/ChampionsGoal.jpg","DisplayName":"足球宝贝","typeid":"2","LapisId":"ELK-championsgoal"},{"ButtonImagePath":"qt/pc/DJWild.jpg","DisplayName":"狂野DJ","typeid":"2","LapisId":"ELK-djwild"},{"ButtonImagePath":"qt/pc/ElectricSam.jpg","DisplayName":"美女和野兽","typeid":"2","LapisId":"ELK-electricsam"},{"ButtonImagePath":"qt/pc/Poltava.jpg","DisplayName":"波尔塔瓦之战","typeid":"2","LapisId":"ELK-poltava"},{"ButtonImagePath":"qt/pc/SamontheBeach.jpg","DisplayName":"夏日海滩","typeid":"2","LapisId":"ELK-samonthebeach"},{"ButtonImagePath":"qt/pc/TacoBrothers.jpg","DisplayName":"塔科兄弟","typeid":"2","LapisId":"ELK-tacobrothers"},{"ButtonImagePath":"qt/pc/TacoBrothersSavingChristmas.jpg","DisplayName":"塔科的圣诞使命","typeid":"2","LapisId":"ELK-tacobrotherssavingchristmas"},{"ButtonImagePath":"qt/pc/TheLab.jpg","DisplayName":"神奇实验室","typeid":"2","LapisId":"ELK-thelab"},{"ButtonImagePath":"qt/pc/WildToro.jpg","DisplayName":"卑鄙的斗牛士","typeid":"2","LapisId":"ELK-wildtoro"},{"ButtonImagePath":"qt/pc/3HandBlackjack.jpg","DisplayName":"三手黑杰克","typeid":"3","LapisId":"HAB-3handblackjack"},{"ButtonImagePath":"qt/pc/3HandBlackjackDoubleExposure.jpg","DisplayName":"三手黑杰克双重曝光","typeid":"3","LapisId":"HAB-3handblackjackdoubleexposure"},{"ButtonImagePath":"qt/pc/Baccarat.jpg","DisplayName":"美国百家乐","typeid":"3","LapisId":"HAB-baccarat"},{"ButtonImagePath":"qt/pc/BaccaratZeroCommission.jpg","DisplayName":"免佣百家乐","typeid":"3","LapisId":"HAB-baccaratzerocommission"},{"ButtonImagePath":"qt/pc/CaribbeanHoldem.jpg","DisplayName":"赌场德州扑克","typeid":"3","LapisId":"HAB-caribbeanholdem"},{"ButtonImagePath":"qt/pc/CaribbeanStud.jpg","DisplayName":"加勒比扑克","typeid":"3","LapisId":"HAB-caribbeanstud"},{"ButtonImagePath":"qt/pc/EuropeanRoulette.jpg","DisplayName":"欧洲轮盘","typeid":"3","LapisId":"HAB-europeanroulette"},{"ButtonImagePath":"qt/pc/SicBo.jpg","DisplayName":"骰宝","typeid":"3","LapisId":"HAB-sicbo"},{"ButtonImagePath":"qt/pc/BlackjackProMonteCarloMultihand.jpg","DisplayName":"蒙特卡洛多手21点","typeid":"3","LapisId":"OGS-blackjackpromontecarlomultihand"},{"ButtonImagePath":"qt/pc/BlackjackProMonteCarloSinglehand.jpg","DisplayName":"蒙特卡洛单手21点","typeid":"3","LapisId":"OGS-blackjackpromontecarlosinglehand"},{"ButtonImagePath":"qt/pc/RouletteMaster.jpg","DisplayName":"轮盘大师","typeid":"3","LapisId":"OGS-roulettemaster"},{"ButtonImagePath":"qt/pc/AllAmerican.jpg","DisplayName":"美国派","typeid":"3","LapisId":"OGS-allamerican"},{"ButtonImagePath":"qt/pc/Blackjack.jpg","DisplayName":"二十一点","typeid":"3","LapisId":"OGS-blackjack"},{"ButtonImagePath":"qt/pc/DeucesWild.jpg","DisplayName":"百搭二王","typeid":"3","LapisId":"OGS-deuceswild"},{"ButtonImagePath":"qt/pc/EuropeanRoulette.jpg","DisplayName":"欧式轮盘","typeid":"3","LapisId":"OGS-europeanroulette"},{"ButtonImagePath":"qt/pc/JacksorBetter.jpg","DisplayName":"杰克扑克","typeid":"3","LapisId":"OGS-jacksorbetter"},{"ButtonImagePath":"qt/pc/JokerPoker.jpg","DisplayName":"小丑扑克","typeid":"3","LapisId":"OGS-jokerpoker"},{"ButtonImagePath":"qt/pc/NoCommissionBaccarat.jpg","DisplayName":"无佣百家乐","typeid":"3","LapisId":"OGS-nocommissionbaccarat"},{"ButtonImagePath":"qt/pc/RouletteAmerican.jpg","DisplayName":"美式轮盘","typeid":"3","LapisId":"OGS-rouletteamerican"},{"ButtonImagePath":"qt/pc/SideBetBlackjack.jpg","DisplayName":"单投21点","typeid":"3","LapisId":"OGS-sidebetblackjack"}];
var cq9data = [ {
	"ButtonImagePath" : "1_cn.png",
	"DisplayName" : "钻石水果王",
	"typeid" : 1,
	"LapisId" : "1"
}, {
	"ButtonImagePath" : "2_cn.png",
	"DisplayName" : "棋圣",
	"typeid" : 1,
	"LapisId" : "2"
}, {
	"ButtonImagePath" : "3_cn.png",
	"DisplayName" : "血之吻",
	"typeid" : 1,
	"LapisId" : "3"
}, {
	"ButtonImagePath" : "4_cn.png",
	"DisplayName" : "森林泰后",
	"typeid" : 1,
	"LapisId" : "4"
}, {
	"ButtonImagePath" : "5_cn.png",
	"DisplayName" : "金大款",
	"typeid" : 1,
	"LapisId" : "5"
}, {
	"ButtonImagePath" : "6_cn.png",
	"DisplayName" : "1945",
	"typeid" : 1,
	"LapisId" : "6"
}, {
	"ButtonImagePath" : "7_cn.png",
	"DisplayName" : "跳起来",
	"typeid" : 1,
	"LapisId" : "7"
}, {
	"ButtonImagePath" : "8_cn.png",
	"DisplayName" : "甜蜜蜜",
	"typeid" : 1,
	"LapisId" : "8"
}, {
	"ButtonImagePath" : "9_cn.png",
	"DisplayName" : "钟馗运财",
	"typeid" : 1,
	"LapisId" : "9"
}, {
	"ButtonImagePath" : "10_cn.png",
	"DisplayName" : "五福临门",
	"typeid" : 1,
	"LapisId" : "10"
}, {
	"ButtonImagePath" : "11_cn.png",
	"DisplayName" : "梦游仙境",
	"typeid" : 1,
	"LapisId" : "11"
}, {
	"ButtonImagePath" : "12_cn.png",
	"DisplayName" : "金玉满堂",
	"typeid" : 1,
	"LapisId" : "12"
}, {
	"ButtonImagePath" : "13_cn.png",
	"DisplayName" : "樱花妹子",
	"typeid" : 1,
	"LapisId" : "13"
}, {
	"ButtonImagePath" : "14_cn.png",
	"DisplayName" : "绝赢巫师",
	"typeid" : 1,
	"LapisId" : "14"
}, {
	"ButtonImagePath" : "15_cn.png",
	"DisplayName" : "金鸡报喜",
	"typeid" : 1,
	"LapisId" : "15"
}, {
	"ButtonImagePath" : "16_cn.png",
	"DisplayName" : "五行",
	"typeid" : 1,
	"LapisId" : "16"
}, {
	"ButtonImagePath" : "17_cn.png",
	"DisplayName" : "祥狮献瑞",
	"typeid" : 1,
	"LapisId" : "17"
}, {
	"ButtonImagePath" : "18_cn.png",
	"DisplayName" : "雀王",
	"typeid" : 1,
	"LapisId" : "18"
}, {
	"ButtonImagePath" : "19_cn.png",
	"DisplayName" : "风火轮",
	"typeid" : 1,
	"LapisId" : "19"
}, {
	"ButtonImagePath" : "20_cn.png",
	"DisplayName" : "发发发",
	"typeid" : 1,
	"LapisId" : "20"
}, {
	"ButtonImagePath" : "21_cn.png",
	"DisplayName" : "野狼传说",
	"typeid" : 1,
	"LapisId" : "21"
}, {
	"ButtonImagePath" : "22_cn.png",
	"DisplayName" : "庶务西游二课",
	"typeid" : 1,
	"LapisId" : "22"
}, {
	"ButtonImagePath" : "23_cn.png",
	"DisplayName" : "金元宝",
	"typeid" : 1,
	"LapisId" : "23"
}, {
	"ButtonImagePath" : "24_cn.png",
	"DisplayName" : "跳起来2",
	"typeid" : 1,
	"LapisId" : "24"
}, {
	"ButtonImagePath" : "25_cn.png",
	"DisplayName" : "扑克拉霸",
	"typeid" : 1,
	"LapisId" : "25"
}, {
	"ButtonImagePath" : "26_cn.png",
	"DisplayName" : "777",
	"typeid" : 1,
	"LapisId" : "26"
}, {
	"ButtonImagePath" : "27_cn.png",
	"DisplayName" : "魔法世界",
	"typeid" : 1,
	"LapisId" : "27"
}, {
	"ButtonImagePath" : "28_cn.png",
	"DisplayName" : "食神",
	"typeid" : 1,
	"LapisId" : "28"
}, {
	"ButtonImagePath" : "29_cn.png",
	"DisplayName" : "水世界",
	"typeid" : 1,
	"LapisId" : "29"
}, {
	"ButtonImagePath" : "30_cn.png",
	"DisplayName" : "三国序",
	"typeid" : 1,
	"LapisId" : "30"
}, {
	"ButtonImagePath" : "31_cn.png",
	"DisplayName" : "武圣",
	"typeid" : 1,
	"LapisId" : "31"
}, {
	"ButtonImagePath" : "32_cn.png",
	"DisplayName" : "通天神探狄仁杰",
	"typeid" : 1,
	"LapisId" : "32"
}, {
	"ButtonImagePath" : "33_cn.png",
	"DisplayName" : "火烧连环船",
	"typeid" : 1,
	"LapisId" : "33"
}, {
	"ButtonImagePath" : "34_cn.png",
	"DisplayName" : "地鼠战役",
	"typeid" : 1,
	"LapisId" : "34"
}, {
	"ButtonImagePath" : "35_cn.png",
	"DisplayName" : "疯狂哪吒",
	"typeid" : 1,
	"LapisId" : "35"
}, {
	"ButtonImagePath" : "36_cn.png",
	"DisplayName" : "夜店大亨",
	"typeid" : 1,
	"LapisId" : "36"
}, {
	"ButtonImagePath" : "38_cn.png",
	"DisplayName" : "舞力全开",
	"typeid" : 1,
	"LapisId" : "38"
}, {
	"ButtonImagePath" : "39_cn.png",
	"DisplayName" : "飞天",
	"typeid" : 1,
	"LapisId" : "39"
}, {
	"ButtonImagePath" : "40_cn.png",
	"DisplayName" : "镖王争霸",
	"typeid" : 1,
	"LapisId" : "40"
}, {
	"ButtonImagePath" : "41_cn.png",
	"DisplayName" : "水球大战",
	"typeid" : 1,
	"LapisId" : "41"
}, {
	"ButtonImagePath" : "42_cn.png",
	"DisplayName" : "福尔摩斯",
	"typeid" : 1,
	"LapisId" : "42"
}, {
	"ButtonImagePath" : "43_cn.png",
	"DisplayName" : "恭贺新喜",
	"typeid" : 1,
	"LapisId" : "43"
}, {
	"ButtonImagePath" : "44_cn.png",
	"DisplayName" : "豪华水果王",
	"typeid" : 1,
	"LapisId" : "44"
}, {
	"ButtonImagePath" : "45_cn.png",
	"DisplayName" : "超级发",
	"typeid" : 1,
	"LapisId" : "45"
}, {
	"ButtonImagePath" : "46_cn.png",
	"DisplayName" : "狼月",
	"typeid" : 1,
	"LapisId" : "46"
}, {
	"ButtonImagePath" : "47_cn.png",
	"DisplayName" : "法老宝藏",
	"typeid" : 1,
	"LapisId" : "47"
}, {
	"ButtonImagePath" : "48_cn.png",
	"DisplayName" : "莲",
	"typeid" : 1,
	"LapisId" : "48"
}, {
	"ButtonImagePath" : "49_cn.png",
	"DisplayName" : "寂寞星球",
	"typeid" : 1,
	"LapisId" : "49"
}, {
	"ButtonImagePath" : "50_cn.png",
	"DisplayName" : "洪福齐天",
	"typeid" : 1,
	"LapisId" : "50"
}, {
	"ButtonImagePath" : "51_cn.png",
	"DisplayName" : "嗨爆大马戏",
	"typeid" : 1,
	"LapisId" : "51"
}, {
	"ButtonImagePath" : "52_cn.png",
	"DisplayName" : "跳高高",
	"typeid" : 1,
	"LapisId" : "52"
}, {
	"ButtonImagePath" : "53_cn.png",
	"DisplayName" : "来电99",
	"typeid" : 1,
	"LapisId" : "53"
}, {
	"ButtonImagePath" : "55_cn.png",
	"DisplayName" : "魔龙传奇",
	"typeid" : 1,
	"LapisId" : "55"
}, {
	"ButtonImagePath" : "56_cn.png",
	"DisplayName" : "暗夜公爵",
	"typeid" : 1,
	"LapisId" : "56"
}, {
	"ButtonImagePath" : "57_cn.png",
	"DisplayName" : "神兽争霸",
	"typeid" : 1,
	"LapisId" : "57"
}, {
	"ButtonImagePath" : "58_cn.png",
	"DisplayName" : "金鸡报喜2",
	"typeid" : 1,
	"LapisId" : "58"
}, {
	"ButtonImagePath" : "59_cn.png",
	"DisplayName" : "夏日猩情",
	"typeid" : 1,
	"LapisId" : "59"
}, {
	"ButtonImagePath" : "62_cn.png",
	"DisplayName" : "非常钻",
	"typeid" : 1,
	"LapisId" : "62"
}, {
	"ButtonImagePath" : "63_cn.png",
	"DisplayName" : "寻龙诀",
	"typeid" : 1,
	"LapisId" : "63"
}, {
	"ButtonImagePath" : "65_cn.png",
	"DisplayName" : "黄金渔场",
	"typeid" : 1,
	"LapisId" : "65"
} ];
