function searchGame(){
	var v = $('#elenew-search-game').val();//搜索值
	var t = $('#e_game_type').val();//游戏类型(ag/mg/pt)
	var olddatabase = [];
	var newdatabase = [];
	if(!t){
		location.href = location.href;
	}
	if(!v){
		getdata(t);
	}else{
		if(t == 'ag'){
			olddatabase = agdata;
		}else if(t == 'mg'){
			olddatabase = mgdata;
		}else if(t == 'pt'){
			olddatabase = ptdata;
		}else if(t == 'cq9'){
			olddatabase = cq9data;
		}else if(t == 'jdb'){
			olddatabase = jdbdata;
		}else if(t == 'isb'){
			olddatabase = isbdata;
		}else if(t == 'qt'){
			olddatabase = getAgTypeData(qtdata,[1,2,3]);
		}else if(t == 'by'){
			olddatabase = bydata;
		}else if(t == 'mw'){
			olddatabase = mwdata;
		}else if(t == 'ttg'){
			olddatabase = ttgdata;
		}else{
			return;
		}
		$.each(olddatabase, function (index, content){
			if(content.DisplayName.indexOf(v) != -1){
				newdatabase.push(content);
			}
		});
		initEgameData(newdatabase,t);
	}
}

function getdata(type,tid) {
	var all_count = 0;
	var hot_count = 0;
	var database = [];
	if(!type){
		type = 'ag';
	}
	if(type == "ag"){//ag
	    database = agdata;
	    all_count = agdata.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'ag\');');
	} else if(type == "mg"){//ag
	    database = mgdata;
	    all_count = mgdata.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'mg\');');
	} else if(type == "pt"){//pt
	    database = ptdata;
	    all_count = ptdata.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'pt\');');
	} else if(type == "qt"){//qt
	    database = getAgTypeData(qtdata,[1,2,3]);
	    all_count = database.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'qt\');');
	}else if(type == "cq9"){//cq9
	    database = cq9data;
	    all_count = database.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'cq9\');');
	}else if(type == "jdb"){//jdb
		database = jdbdata;
		all_count = database.length;
		$('#all_game_conut').attr('href','javascript:getdata(\'jdb\');');
	}else if(type == "ttg"){//ttg
		database = ttgdata;
		all_count = database.length;
		$('#all_game_conut').attr('href','javascript:getdata(\'ttg\');');
	}else if(type == "mw"){//mw
		database = mwdata;
		all_count = database.length;
		$('#all_game_conut').attr('href','javascript:getdata(\'mw\');');
	}else if(type == "isb"){//isb
	    database = isbdata;
	    all_count = database.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'isb\');');
	}else if(type == "by"){
	    database = bydata;
	    all_count = database.length;
	    $('#all_game_conut').attr('href','javascript:getdata(\'by\');');
	}else{
		return;
	}
	$('#all_game_count').html(all_count);//总游戏
	initEgameData(database,type);
}

/**
 * @param database 数据
 * @param type 类型
 * @param searchVal 搜索值
 */
function initEgameData(database,type){

var path = "common/template/third/egame/images/v2";
var mgCss = "";
if("qt" == type || "pt" == type  || "by" == type){
	path = "common/template/third/newEgame/images/";
}else if("ag" == type){
	path = "common/template/third/egame/images/ag/";
}else if("cq9" == type){
	path = "common/template/third/newEgame/images/cq9/";
}else if("jdb" == type){
	path = "common/template/third/newEgame/images/jdb/";
}else if("isb" == type){
	path = "common/template/third/newEgame/images/isb/";
}else if("ttg" == type){
	path = "common/template/third/newEgame/images/ttg/";
}else if("mw" == type){
	path = "common/template/third/newEgame/images/mw/";
}/*else if("mg"==type){
	mgCss = "mg-game";
	path = "common/template/third/newEgame/images/mg/";
}*/else{
	mgCss = "mg-game";
	path = "common/template/third/egame/images/v2/";
}

var eachData = {
		type :type,
		games:database,
		imgPath : path,
		imgCss : mgCss
}

var html = template('gameList_tpl', eachData);
$("#elenew-game-space").html(html);
}

/** ag下级数据 **/
function get_ag_data(p){
	if(!p){
		return;
	}
	
	if(p == '1'){//最新游戏
		initEgameData(agnewdata,'ag');
	}else if(p == '2'){//老虎机
		initEgameData(getAgTypeData(agdata,4),'ag');
	}else if(p == '3'){//热门游戏
		initEgameData(aghotdata,'ag');
	}else if(p == '4'){//youplay
		initEgameData(youplay,'ag');
	}
}

/** mg下级数据 **/
function get_mg_data(p){
	if(!p){
		return;
	}else if(p== '8'){
		initEgameData(mgNewGame,'mg');
	}else{
		initEgameData(getAgTypeData(mgdata,p),'mg');
	}
	/*if(!p){
		return;
	}
	if(p == '1'){
		initEgameData(mgHotData,'mg');
	}else if(p == '2'){
		initEgameData(mgNewGame,'mg');
	}else if(p == '3'){
		initEgameData(mgBojiGame,'mg');
	}else{
		initEgameData(mgdata,'mg');
	}*/
}

/** qt下级数据 **/
function get_qt_data(q){
	if(!q){
		return;
	}
	initEgameData(getAgTypeData(qtdata,q),'qt');
}

/** pt下级数据 **/
function get_pt_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(ptdata,p),'pt');
}
/** cq9下级数据 **/
function get_cq9_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(cq9data,p),'cq9');
}
/** jdb下级数据 **/
function get_jdb_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(jdbdata,p),'jdb');
}
/** ttg下级数据 **/
function get_ttg_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(ttgdata,p),'ttg');
}
/** mw下级数据 **/
function get_mw_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(mwdata,p),'mw');
}

/** isb下级数据 **/
function get_isb_data(p){
	if(!p){
		return;
	}
	initEgameData(getAgTypeData(isbdata,p),'isb');
}
function get_by_data(p){
	if(!p){
		return;
	}
	if(p == 'ag'){//黄金鱼汤
		initEgameData(getAgTypeData(bydata,p),'by');
	}else if(p == 'cq9'){
		initEgameData(getAgTypeData(bydata,p),'by');
	}else if(p == 'pt'){
		initEgameData(getAgTypeData(bydata,p),'by');
	}else if(p == 'bbin'){
		initEgameData(getAgTypeData(bydata,p),'by');
	}else if(p == 'jdb'){
		initEgameData(getAgTypeData(bydata,p),'by');
	}
}
/** 根据类型过滤数据 **/
function getAgTypeData(database,type){
	
	var arryFlag = false;
	if(typeof type == "object"){
		arryFlag = true;
	}
	var newdatabase = [];
	$.each(database, function (index, content){
		if(arryFlag){
			for (t in type)
			{
				if(content.typeid == type[t]){
					newdatabase.push(content);
					break;
				}
			}
		}else{
			if(content.typeid == type){
				newdatabase.push(content);
			}
		}
		
	});
	return newdatabase;
}

function toEgame(type,p){
	var isLogin = $('#userid').val();
	if(!isLogin || isLogin == "false"){
		alert("您需要先登录才能游戏");
		return;
	}
	if(type == "ag"){//ag
		toAG(p);
	} else if(type == "mg"){//mg
		toMG(p);
	}else if(type == "pt"){
		toPT(p);
	}else if(type == "qt"){
		toQT(p);
	}else if(type == "cq9"){
		toCQ9(p);
	}else if(type == "jdb"){
		toJDB(p);
	}else if(type == "isb"){
		toISB(p);
	}else if(type == "mw"){
		toMW(p);
	}else if(type == "ttg"){
		toTTG(p);
	}else if (type == "by"){
		toBY(p);
	}else{
		return;
	}
}

(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					alert("[" + opt.url + "] 404 not found");
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					alert("没有权限");
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
})(jQuery);

function toMG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardMg.do",
		sync : false,
		data:{
			gameType : 3,
			gameid : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}

function toAG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardAg.do",
		sync : false,
		data:{
			h5 : 0,
			gameType : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}

function toPT(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardPt.do",
		sync : false,
		data:{
			h5 : 0,
			gameCode : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}
function toQT(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardQt.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}

function toCQ9(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardCq9.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}

function toJDB(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardJdb.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}

function toISB(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardIsb.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}
function toTTG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardTtg.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}
function toMW(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;
	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
	$.ajax({
		url : base + "/forwardMw.do",
		sync : false,
		data:{
			gameId : gameid
		},
		success : function(json) {
			if (json.success) {
				windowOpen.location.href=json.url;
			} else {
				alert(json.msg);
				return;
			}
		}
	});
}
function toBY(gameid){
	var toUrl = "";
	var data = {};
	if(gameid == 6){
		data  = {
			h5 : 0,
			gameType : gameid
		}
		toUrl = base + "/forwardAg.do";
	}else if(gameid == 'AB1'){
		data = {
			gameId : gameid
		}
		toUrl = base + "/forwardCq9.do";
		
	}else if(gameid == 'cashfi'){
		data = {
				h5 : 0,
				gameCode : gameid
			};
		toUrl = base + "/forwardPt.do";
		
	}else if(gameid == "30599" || gameid == "38001"){
		data = {
				gameType :gameid	
		}
		toUrl = base + '/forwardBbin.do' ;
	}else if(gameid == "50"){
		data = {
			gameId : gameid
		}
		toUrl = base + "/forwardMw.do";
	}else if(gameid == "7001" || gameid == "7002" || gameid == "7003"){
		data = {
				gameId : gameid
			}
			toUrl = base + "/forwardJdb.do";
		}
	  var sw = '';
	    csw = '';
	    sh = '';
	    csh = '';
	    ctop = '';
	    cleft = '';
	    sw = $(window.parent).width();
	    sh = $(window.parent).height();
	    csw = $(window.parent).width();
	    csh = $(window.parent).height();
	    ctop = 0;
	    cleft = 0;
		var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
		$.ajax({
			url : toUrl,
			sync : false,
			data:data,
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
}