<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${base }/common/css/noscroll.css" rel="stylesheet">
<style>
#elenew-game-wrap {
	position: relative;
	min-height: 480px;
	padding: 15px 0 180px;
}

/*Mg*/
#progressiveTickerall {
	display: none;
}

/*
** 遊戲圖 樣式
*/
.game .elenew-game-wrap {
	width: 1000px;
	margin: 0 auto;
}

.elenew-view-block .elenew-img-wrap {
	position: relative;
	background-color: #FFF;
	box-shadow: 0 1px 2px #C7C8C9;
}

.elenew-game-ctl-wrap, .elenew-game-ctl-tools, .elenew-game-ctl-links {
	width: 100%;
}

.elenew-game-img, .elenew-game-ctl-wrap {
	bottom: 0;
}

/* 遊戲小彩金 */
.mask-wrap .elenew-game-sub-jp {
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	background: url('${base }/common/template/third/newEgame/images/bg_black.png');
}

.mask-wrap .elenew-game-sub-jp .minorJP {
	display: none;
	height: 24px;
	line-height: 24px;
	width: 100%;
	color: #FF3300;
	font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
	font-size: 17px;
	font-weight: bold;
	background:
		url('${base }/common/template/third/newEgame/images/game_minor_m.png') 4px 4px
		no-repeat;
	padding: 0 10px 0 85px;
	margin: 0;
	box-sizing: border-box;
	text-align: right;
}

.mask-wrap .elenew-game-sub-jp .minorJP.JPMG, .mask-wrap .elenew-game-sub-jp .minorJP.JPIsb,
	.mask-wrap .elenew-game-sub-jp .minorJP.JPPT {
	background:
		url('${base }/common/template/third/newEgame/images/game_jackpot_m.png') 4px
		4px no-repeat;
	padding: 0 6px 0 76px;
}

.elenew-view-mini .mask-wrap .elenew-game-sub-jp .minorJP.JPMG,
	.elenew-view-mini .mask-wrap .elenew-game-sub-jp .minorJP.JPIsb,
	.elenew-view-mini .mask-wrap .elenew-game-sub-jp .minorJP.JPPT {
	width: 174px;
	background:
		url('${base }/common/template/third/newEgame/images/game_jackpot_m.png') #000
		10px 0 no-repeat;
}

/*
** 遊戲連結
*/
.elenew-game-link {
	display: block;
	height: 30px;
	line-height: 30px;
	text-align: center;
	text-decoration: none;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.elenew-game-ctl-links .ctl-btn-coming a {
	text-decoration: none;
}

.elenew-view-block .elenew-game-ctl-links .ctl-btn-enter,
	.elenew-view-block .elenew-game-ctl-links .ctl-btn-lite,
	.elenew-view-block .elenew-game-ctl-links .ctl-btn-coming {
	width: 82%;
	height: 38px;
	line-height: 38px;
	color: #FFF;
	font-size: 15px;
	margin: 38px auto 0;
	background: transparent;
	border: solid 1px #FFF;
}

.elenew-view-block .elenew-game-ctl-links .ctl-btn-enter:hover,
	.elenew-view-block .elenew-html5-layout .ctl-btn-lite:hover {
	background: #BD2004;
	border: solid 1px #E6391F;
}

.elenew-view-block .elenew-game-ctl-links .ctl-btn-coming:hover {
	background: #148E5E;
	border: solid 1px #15B777;
}

.elenew-view-block .ctl-btn-freetry {
	width: auto;
	max-width: 140px;
	font-size: 15px;
	height: 40px;
	line-height: 40px;
	margin: 0 auto;
	color: #DFDFDF;
	text-decoration: underline;
}

.elenew-view-block .ctl-btn-freetry:hover {
	color: #E6391F;
}

.elenew-view-block .ctl-btn-rule, .elenew-view-block .elenew-html5-layout .elenew-game-ctl-links .ctl-btn-enter,
	.elenew-view-block .tool-btn-favorite {
	width: 23px;
	height: 23px;
	background:
		url('${base }/common/template/third/newEgame/images/game_layout_icon.png')
		-23px 0 no-repeat;
	margin: 0 2px 0 0;
}

.elenew-view-block .elenew-html5-layout .elenew-game-ctl-links .ctl-btn-enter
	{
	border: none;
}

.elenew-view-block .ctl-btn-rule:hover, .elenew-view-block .elenew-html5-layout .elenew-game-ctl-links .ctl-btn-enter:hover,
	.elenew-view-block .tool-btn-favorite:hover {
	border: none;
}

.elenew-view-block .ctl-btn-rule:hover {
	background-position: -23px 100%;
}

.elenew-view-block .elenew-html5-layout .elenew-game-ctl-links a.ctl-btn-enter
	{
	background-position: 0 0;
}

.elenew-view-block .elenew-html5-layout .elenew-game-ctl-links a.ctl-btn-enter:hover
	{
	background-position: 0 100%;
}

.elenew-game-ctl-links .ctl-btn-coming .ctl-btn-rule {
	padding: 0 6px;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	background: none;
}

.elenew-game-ctl-links .ctl-btn-coming a.ctl-btn-rule:hover {
	background: none;
}

.elenew-view-block .favorite-icon-N {
	background-position: -46px 0;
}

.elenew-view-block .favorite-icon-N:hover, .elenew-view-block .favorite-icon-Y
	{
	background-position: -46px 100%;
}

.elenew-game-ctl-wrap.game-coming-wrap .elenew-block-btn-txt {
	display: none;
}

.elenew-view-block .ctl-btn-rule span {
	display: none;
}

/*
** 遊戲圖 共用樣式
** 圖片顯示效果 Smarty 設定 ELENEW_GAMEIMG_EFFECT
** 遊戲形象(固定/變動) : symbol_fixed / symbol_change
** 遊戲畫面(固定/變動) : screen_fixed / screen_change
*/
.elenew-game-layout {
	position: relative;
	float: left;
	width: 18%;
	margin: 0 1% 15px;
	box-sizing: border-box;
	transform: scale(1, 1);
	-webkit-transform: scale(1, 1);
	-o-transform: scale(1, 1);
	-moz-transform: scale(1, 1);
	z-index: 1;
	transition: transform 0.5s, top 0.5s, left 0.5s;
	-moz-transition: transform 0.5s, top 0.5s, left 0.5s;
	-webkit-transition: transform 0.5s, top 0.5s, left 0.5s;
	-o-transition: transform 0.5s, top 0.5s, left 0.5s;
}

.elenew-no-animate .elenew-game-layout {
	transition: transform 0s, top 0s, left 0s;
	-moz-transition: transform 0s, top 0s, left 0s;
	-webkit-transition: transform 0s, top 0s, left 0s;
	-o-transition: transform 0s, top 0s, left 0s;
}

.gamenew-filter-hide, .gamenew-group-hide {
	transform: scale(0, 0);
	-webkit-transform: scale(0, 0);
	-o-transform: scale(0, 0);
	-moz-transform: scale(0, 0);
	z-index: 0;
	display: none\0; /* IE8, IE9, IE10 */
}
/* hack for UB */
@media all and (-webkit-bb:0) {
	.elenew-game-layout {
		transform: none;
		-webkit-transform: none;
		transition: top 0.5s, left 0.5s;
		-webkit-transition: top 0.5s, left 0.5s;
	}
	.elenew-no-animate .elenew-game-layout {
		transition: top 0s, left 0s;
		-webkit-transition: top 0s, left 0s;
	}
	.gamenew-filter-hide, .gamenew-group-hide {
		display: none;
	}
}
/* only for IE10 */
@media screen and (-ms-high-contrast: active) , ( -ms-high-contrast :
	none) {
	.gamenew-filter-hide, .gamenew-group-hide {
		display: block;
	}
}

.elenew-game-name {
	position: relative;
}

.elenew-img-innerwrap h3 {
	height: 30px;
	line-height: 30px;
	text-align: center;
}

.elenew-img-innerwrap h3 .img-innerwrap-name {
	text-indent: 5px;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	font-size: 15px;
	color: #444;
	font-weight: bold;
}

.elenew-view-mini .elenew-img-innerwrap h3 .img-innerwrap-name {
	width: 136px;
}

.elenew-game-img {
	position: relative;
	display: block;
	text-decoration: none;
}

.elenew-game-img img {
	display: block;
	width:176px;
	height: 121px;
	margin: 0 auto;
}

.th .ctl-btn-rule {
	font-size: 16px;
}

.th .gamenew-hall-23 .ctl-btn-rule {
	font-size: 13px;
}

/*
** 共用-廳的icon
** 圖片預設 Smarty 設定 ELENEW_GAMEHALL_DEF
*/
.elenew-hall-icon {
	float: left;
	width: 34px;
	height: 34px;
}

.elenew-hall-icon:before {
	display: block;
	width: 23px;
	height: 23px;
	content: "";
	background:
		url('${base }/common/template/third/newEgame/images/game_list_icon.png') 0 0
		no-repeat;
	margin: 15px 0 0 15px;
}

.gamenew-hall-23 .elenew-hall-icon:before {
	background-position: -23px 0;
}

.gamenew-hall-28 .elenew-hall-icon:before {
	background-position: -46px 0;
}

.gamenew-hall-29 .elenew-hall-icon:before {
	background-position: -69px 0;
}

.gamenew-hall-20 .elenew-hall-icon:before {
	background-position: -92px 0;
}

.elenew-view-block.gamenew-hall-28 .elenew-hall-icon, .elenew-view-block.gamenew-hall-23 .elenew-hall-icon,
	.elenew-view-block.gamenew-hall-29 .elenew-hall-icon {
	display: none;
}

.elenew-view-block .elenew-hall-icon.elenew-hall-bbin {
	width: 0;
	margin-left: 5px;
}

/*
** 共用 狀態的icon
*/
.elenew-game-status {
	position: absolute;
	top: -2px;
	left: -2px;
	width: 79px;
	height: 63px;
}

.elenew-view-block .elenew-game-status {
	top: 0;
	left: 0;
}

.status-First {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_comingsoon_m.png')
		0 0 no-repeat;
}

.status-stayuned {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_staytuned_m.png')
		0 0 no-repeat;
}

.status-New {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_new_m.png')
		0 0 no-repeat;
}

.status-NewBonus {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_newbonus_m.png')
		0 0 no-repeat;
}

.status-Recommend {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_recommend_m.png')
		0 0 no-repeat;
}
/*
** 共用 操作功能列表
*/
.elenew-game-ctl-wrap {
	position: absolute;
	height: 0;
	overflow: hidden;
}

.elenew-game-ctl-links {
	display: none;
}

/*
** 共用 工具列的icon
*/
.tool-btn-comingsoon {
	color: #306FBD;
}

.elenew-img-wrap .tool-btn-favorite {
	z-index: 5;
}

.elenew-game-loading {
	display: none;
}

/******************block******************/
/* ctl-wrap */
.elenew-game-ctl-wrap {
	top: 0;
	display: none;
	width: 100%;
	height: 100%;
	background:
		url('${base }/common/template/third/newEgame/images/game_layout_bg.png')
		repeat;
}

.elenew-view-block .elenew-game-ctl-wrap {
	left: 0;
}

.elenew-game-ctl-links {
	display: block;
	margin: 20px auto 0;
	text-align: center;
}

.elenew-view-block .elenew-coming-layout .elenew-game-ctl-links {
	margin: 25px auto 0;
}

.elenew-game-ctl-links a {
	position: relative;
	display: block;
	text-align: center;
	font-size: 13px;
	color: #FFF;
	box-sizing: border-box;
}

.elenew-view-block .elenew-html5-layout a.ctl-btn-lite span,
	.elenew-view-block .elenew-game-ctl-links a.ctl-btn-enter span,
	.elenew-view-block .elenew-game-ctl-links a.ctl-btn-freetry span {
	display: block;
}

.elenew-view-block .elenew-html5-layout a.ctl-btn-enter span {
	display: none;
}

.elenew-block-title {
	display: none;
}

.elenew-view-block .elenew-block-title {
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	cursor: pointer;
}

.elenew-game-content {
	display: none;
	font-size: 13px;
	overflow: hidden;
	text-overflow: ellipsis;
	-webkit-box-orient: vertical;
}

.elenew-game-ctl-tools {
	position: absolute;
	bottom: 5px;
	right: 5px;
	width: 23px;
	height: 30px;
	font-size: 15px;
	z-index: 2;
}

.elenew-view-block .elenew-game-ctl-tools {
	display: none;
}

.elenew-view-block .elenew-hall-icon {
	margin-left: 4px;
}

/******************mini******************/
.elenew-view-mini .elenew-game-content {
	display: block;
	margin: 5px 0 7px;
	max-height: 68px;
	font-size: 13px;
	line-height: 17px;
	color: #888;
	font-weight: bold;
	display: -webkit-box;
	-webkit-line-clamp: 4;
}

.elenew-view-mini .elenew-game-ctl-wrap {
	right: 4px;
	z-index: 1;
	display: block;
	width: 178px;
	height: 100%;
	background: none;
}

.elenew-view-mini .elenew-game-ctl-links {
	border: none;
	margin: 10px 0 0 0;
	padding-top: 0;
}

.elenew-view-mini .elenew-game-ctl-links a {
	float: right;
	color: #FFF;
	width: 82px;
	height: 32px;
	line-height: 32px;
	background-color: #BD2004;
	border: solid 1px #E6391F;
	margin-right: 7px;
}

.elenew-view-mini .ctl-btn-coming a {
	background: #148E5E;
	border: solid 1px #15B777;
}

.elenew-view-mini .elenew-game-ctl-links a:hover {
	background-color: #801C1C;
	border: solid 1px #C11B00;
}

.elenew-view-mini .ctl-btn-coming a:hover {
	background: #075638;
	border: solid 1px #008F5C;
}

.elenew-view-mini .elenew-game-ctl-links a.ctl-btn-rule {
	color: #9F9F9F;
	background: #F4F4F4;
	border: solid 1px #E0E0E0;
}

.elenew-view-mini .elenew-game-ctl-links a.ctl-btn-rule:hover {
	color: #FFF;
	background: #C3C3C3;
}

.elenew-view-mini .ctl-btn-freetry {
	font-size: 13px;
}

.elenew-view-mini .elenew-hall-icon, .elenew-view-mini .tool-btn-favorite
	{
	float: left;
	width: 54px;
	height: 54px;
	background-color: #F1F1F1;
	border-right: 1px solid #E6E6E6;
}

.elenew-view-mini .elenew-hall-icon.elenew-hall-bbin {
	display: block;
}

.elenew-view-mini .elenew-img-innerwrap .elenew-hall-icon {
	display: none;
}

.elenew-view-mini .elenew-game-status {
	top: -5px;
	left: 6px;
}

.elenew-view-mini .trans-over .elenew-game-status {
	top: -1px;
}

.elenew-view-mini .favorite-icon-N {
	color: #D8D8D8;
}

.elenew-view-mini .favorite-icon-N:hover, .elenew-view-mini .favorite-icon-Y
	{
	color: #FDAC00;
}

.elenew-view-mini .elenew-game-layout {
	height: 56px;
	width: 48.8%;
	background-color: #FFF;
	border: solid 1px #D5D5D5;
	margin: 6px 0.6%;
}

.elenew-view-mini .elenew-game-layout:hover {
	background-color: #FFF;
}

.elenew-view-mini .elenew-game-layout.no-hover-effect {
	background-color: #F7F7F7;
}

.elenew-view-mini .elenew-img-innerwrap {
	display: none;
	position: absolute;
	z-index: 1;
	top: 74px;
	left: -9px;
	width: 182px;
	background:
		url('${base }/common/template/third/newEgame/images/game_list_over_y_s.png') 0
		0 repeat-y;
	padding: 4px 8px 0;
	color: #333;
}

.elenew-view-mini .elenew-img-innerwrap:before {
	content: '';
	display: block;
	position: absolute;
	top: -18px;
	left: 0;
	width: 198px;
	background:
		url('${base }/common/template/third/newEgame/images/game_list_over_top_s.png')
		0 0 no-repeat;
	height: 18px;
}

.elenew-view-mini .elenew-img-innerwrap:after {
	content: '';
	display: block;
	position: absolute;
	bottom: -12px;
	left: 0;
	background:
		url('${base }/common/template/third/newEgame/images/game_list_over_bottom_s.png')
		0 0 no-repeat;
	width: 198px;
	height: 12px;
}
/* 會超出底部改往上長 */
.elenew-view-mini .trans-over .elenew-img-innerwrap {
	top: auto;
	bottom: 74px;
	padding-top: 6px;
}

.elenew-view-mini .trans-over .elenew-img-innerwrap:before {
	top: -10px;
	height: 10px;
	background:
		url('/cl/tpl/template/images/element/newgame/game_mini_hover_bot_tr_mg.png')
		0 0 no-repeat;
}

.elenew-view-mini .trans-over .elenew-img-innerwrap:after {
	bottom: -24px;
	height: 24px;
	background:
		url('/cl/tpl/template/images/element/newgame/game_mini_hover_top_tr_mg.png')
		0 0 no-repeat;
}

.elenew-view-mini .elenew-img-wrap .elenew-game-img {
	margin: 0 auto 5px;
}

.elenew-view-mini .elenew-img-wrap {
	position: absolute;
	height: 100%;
	width: 100%;
}

.elenew-view-mini .elenew-img-wrap.mini-img-show .elenew-img-innerwrap {
	display: block;
}

.elenew-view-mini .elenew-game-name {
	display: block;
}

.elenew-view-mini .elenew-img-innerwrap h3 {
	display: block;
	height: 18px;
	line-height: 18px;
	font-size: 16px;
	margin: 7px 0;
}

.elenew-view-mini .elenew-game-name h3 {
	position: relative;
	float: left;
	max-width: 185px;
	height: 54px;
	line-height: 54px;
	padding: 0 32px 0 10px;
	box-sizing: border-box;
	color: #444;
	font-size: 14px;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	-webkit-box-orient: vertical;
}

.elenew-view-mini .elenew-game-tool {
	float: left;
	line-height: 54px;
	text-align: center;
	margin-right: 6px;
}

.elenew-view-mini .elenew-game-tool i {
	line-height: 21px;
}

.elenew-view-mini .elenew-img-wrap .elenew-game-tool {
	display: block;
	margin: 0 7px 0 0;
	line-height: 18px;
}

.elenew-view-mini .elenew-game-sub-jp {
	
}

.mini-status {
	display: none;
	width: 26px;
	height: 17px;
}

.elenew-view-mini .mini-status {
	display: block;
	position: absolute;
	right: 0;
	top: 50%;
	margin-top: -9px;
}

.mini-status-First {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_comingsoon_s.png')
		0 0 no-repeat;
}

.mini-status-staytuned {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_staytuned_m.png')
		0 0 no-repeat;
}

.mini-status-New {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_new_s.png')
		0 0 no-repeat;
}

.mini-status-NewBonus {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_newbonus_s.png')
		0 0 no-repeat;
}

.mini-status-Recommend {
	background:
		url('${base }/common/template/third/newEgame/images/lang/zh-cn/game_lbl_recommend_s.png')
		0 0 no-repeat;
}

.gamenew-filter-empty {
	transform-origin: center center;
	animation-name: shake-base;
	animation-duration: 100ms;
	animation-iteration-count: 3;
	animation-timing-function: ease-in-out;
	animation-delay: 0s;
	animation-play-state: running;
}

@
keyframes shake-base { 0% {
	transform: translate(2px, 1px) rotate(0deg);
}

10%
{
transform






:



 



translate






(-1
px
,
-2
px






)
rotate






(-1
deg




);
}
20%
{
transform






:



 



translate






(-3
px
,
0
px






)
rotate






(1
deg




);
}
30%
{
transform






:



 



translate






(0
px
,
2
px






)
rotate






(0
deg




);
}
40%
{
transform






:



 



translate






(1
px
,
-1
px






)
rotate






(1
deg




);
}
50%
{
transform






:



 



translate






(-1
px
,
2
px






)
rotate






(-1
deg




);
}
60%
{
transform






:



 



translate






(-3
px
,
1
px






)
rotate






(0
deg




);
}
70%
{
transform






:



 



translate






(2
px
,
1
px






)
rotate






(-1
deg




);
}
80%
{
transform






:



 



translate






(-1
px
,
-1
px






)
rotate






(1
deg




);
}
90%
{
transform






:



 



translate






(2
px
,
2
px






)
rotate






(0
deg




);
}
100%
{
transform






:



 



translate






(1
px
,
-2
px






)
rotate






(-1
deg




);
}
}
@
-webkit-keyframes shake-base { 0% {
	transform: translate(2px, 1px) rotate(0deg);
}

10%
{
transform






:



 



translate






(-1
px
,
-2
px






)
rotate






(-1
deg




);
}
20%
{
transform






:



 



translate






(-3
px
,
0
px






)
rotate






(1
deg




);
}
30%
{
transform






:



 



translate






(0
px
,
2
px






)
rotate






(0
deg




);
}
40%
{
transform






:



 



translate






(1
px
,
-1
px






)
rotate






(1
deg




);
}
50%
{
transform






:



 



translate






(-1
px
,
2
px






)
rotate






(-1
deg




);
}
60%
{
transform






:



 



translate






(-3
px
,
1
px






)
rotate






(0
deg




);
}
70%
{
transform






:



 



translate






(2
px
,
1
px






)
rotate






(-1
deg




);
}
80%
{
transform






:



 



translate






(-1
px
,
-1
px






)
rotate






(1
deg




);
}
90%
{
transform






:



 



translate






(2
px
,
2
px






)
rotate






(0
deg




);
}
100%
{
transform






:



 



translate






(1
px
,
-2
px






)
rotate






(-1
deg




);
}
}

/* game-login */
.game-login-wrap {
	position: fixed;
	z-index: 99999;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:
		url('/cl/tpl/template/images/element/newgame/login_area_bg.png')
		repeat;
}

.game-login-wrap .game-login-bg {
	position: absolute;
	top: 50%;
	left: 50%;
	width: 723px;
	height: 450px;
	margin: -225px 0 0 -362px;
	background:
		url('/cl/tpl/template/images/element/newgame/login_bg01.png');
}

.game-login-wrap .game-login-bg form {
	width: 100%;
	height: 100%;
	padding-left: 380px;
	padding-top: 65px;
	box-sizing: border-box;
	background:
		url('/cl/tpl/template/images/element/newgame/login_bg02.png') 50% 50%
		no-repeat;
}

.game-login-wrap form p {
	position: relative !important;
	display: block !important;
	float: none !important;
}

.game-login-wrap .glogin-title {
	background:
		url('/cl/tpl/template/images/element/newgame/login_top_icon.png') 0 0
		no-repeat;
	width: 250px;
	height: 30px;
	line-height: 30px;
	color: #34CAAE;
	font-size: 14px;
	text-indent: 39px;
	margin-bottom: 13px;
	text-shadow: 1px 1px 2px #000;
}

.game-login-wrap .login-unit {
	width: 248px !important;
	height: 28px !important;
	margin: 0 0 12px 0 !important;
	padding: 0 !important;
	background:
		url('/cl/tpl/template/images/element/newgame/loginInput_icon.png')
		#FFF no-repeat !important;
	border-radius: 3px;
	border: 1px solid #CCC !important;
}

.game-login-wrap .login-unit.login-unit-pwd {
	background-position: 0 -28px !important;
}

.game-login-wrap .login-unit.login-unit-chk {
	background-position: 0 -56px !important;
}

.game-login-wrap .login-input {
	float: left !important;
	position: static !important;
	width: 248px !important;
	height: 28px !important;
	padding: 6px 12px 6px 30px !important;
	margin: 0 !important;
	font-size: 13px !important;
	color: #555 !important;
	border: none !important;
	outline: none !important;
	background: none !important;
	line-height: 16px !important;
	box-sizing: border-box !important;
}

.game-login-wrap .login-input.login-acc {
	padding: 6px 48px 6px 30px !important;
}

.game-login-wrap .login-unit .login-placeholder {
	position: absolute;
	left: 33px;
	top: 0;
	line-height: 30px;
	color: #8C8C8C;
	cursor: text;
	font-family: arial;
}

.game-login-wrap .acc-unicode {
	position: absolute;
	right: 9px;
	top: 0;
	line-height: 27px;
	color: #555;
	font-size: 14px;
	font-family: arial;
}

#vPic-game {
	position: absolute;
	right: 5px;
	top: 5px;
	cursor: pointer;
	visibility: hidden;
}

.game-login-wrap .login-submit {
	float: none !important;
	position: static !important;
	background: none !important;
	width: 250px !important;
	height: 30px !important;
	line-height: normal !important;
	margin: 0 0 11px !important;
	padding: 0 !important;
	font-size: 13px !important;
	border-radius: 3px;
	border: 1px solid #FFF !important;
	box-sizing: border-box !important;
	color: #FFF !important;
	cursor: pointer !important;
}

.game-login-wrap .glogin-forget a {
	display: block;
	margin-bottom: 11px;
	width: 250px;
	height: 28px;
	line-height: 28px;
	color: #CCC;
	text-align: center;
}

.game-login-wrap .glogin-joinmem a {
	display: block;
	width: 250px;
	height: 32px;
	color: #FFF;
	text-decoration: none;
	text-align: center;
	line-height: 30px;
	font-size: 14px;
}

.game-login-wrap .glogin-joinmem .gjoin-btn {
	background: url('/cl/tpl/template/images/element/newgame/login_btn.png')
		no-repeat;
}

.game-login-wrap .glogin-joinmem .gfreeplay-btn {
	margin-top: 8px;
	background:
		url('/cl/tpl/template/images/element/newgame/freeplay_btn.png')
		no-repeat;
}

.game-login-wrap .glogin-joinmem a:hover {
	background-position: 0 100%;
}

.game-login-wrap .glogin-close {
	background:
		url('/cl/tpl/template/images/element/newgame/login_close.png')
		no-repeat;
	position: absolute;
	width: 28px;
	height: 28px;
	top: 55px;
	right: 55px;
	cursor: pointer;
}

/*view block 遮罩*/
.elenew-view-block .mask-wrap {
	position: relative;
	padding: 2px;
}

.elenew-view-mini .elenew-game-ctl-wrap.inner-one {
	display: none;
}

.elenew-view-block .mask-wrap:hover .elenew-game-ctl-wrap.inner-one {
	display: block;
	z-index: 2;
}

.mask-wrap .gamerule-btn-wrap {
	position: absolute;
	clear: both;
	float: right;
	bottom: 5px;
	right: 5px;
}

.mask-wrap .elenew-game-ctl-links .gamerule-btn-wrap  a {
	float: left;
	margin-right: 2px;
}

/* Mobile Qrcode*/
#game_qrcode {
	z-index: 1;
	float: left;
	width: 18%;
	height: 163px;
	text-align: center;
	background-image:
		url('${base }/common/template/third/newEgame/images/game_qrcode_bg.png');
	background-size: cover;
	box-sizing: border-box;
	margin: 0 1% 15px;
}

#game_qrcode img {
	max-width: 100%;
	height: auto;
}

.gamenew-qrcode-content, #game_qrcode h3, #game_qrcode p {
	position: absolute;
	left: 0;
	width: 100%;
}

.gamenew-qrcode-content {
	top: 23px;
	height: 105px;
	background:
		url('${base }/common/template/third/newEgame/images/game_qrcode_content.png')
		50% 50% no-repeat;
	padding-top: 14px;
}

.gamenew-qrcode-content img {
	max-width: 100%;
	height: auto;
	vertical-align: middle;
}

#game_qrcode .txt-yellow {
	top: 0;
	height: 34px;
	line-height: 34px;
	color: #F8BD33;
	font-size: 17px;
}

#game_qrcode p {
	bottom: 0;
	height: 30px;
	line-height: 30px;
	color: #FFF;
	font-size: 15px;
	font-weight: bold;
}

/*
** MG幸運之龍擂台賽 樣式
*/
.game .elenew-mggame-event-wrap {
	position: relative;
	margin: 15px auto 0;
	width: 1000px;
}

.mgevent-execute {
	position: relative;
	min-height: 300px;
	background:
		url('${base }/common/template/third/newEgame/images/spring_web_bg.jpg')
		#941B1D 0 0 no-repeat;
}

.mgevent-execute .mggame-event-link {
	position: absolute;
	top: 20px;
	right: 10px;
	left: auto;
	width: 150px;
	height: 40px;
	background:
		url('${base }/common/template/third/newEgame/images/spring_activity_web.png')
		0 0 no-repeat;
}

.mgevent-execute .mggame-event-gamelist {
	padding-top: 100px;
	margin: 0 auto;
	width: 1000px;
}

.mgevent-execute .mggame-event-gamelist .mggame-event-game {
	float: left;
	margin-top: 6px;
	width: 20%;
	height: 156px;
	text-decoration: none;
}

.mgevent-execute .mggame-event-gamelist .mggame-event-gamepic {
	margin: 0 auto;
	width: 145px;
	height: 136px;
	overflow: hidden;
}

.mgevent-execute .mggame-event-gamelist .mggame-event-gametxt {
	color: #FFF;
	font-size: 15px;
	text-align: center;
}

.mgevent-after .mggame-event-link {
	display: block;
	width: 455px;
	height: 95px;
	background:
		url('${base }/common/template/third/newEgame/images/mg_spring_result.png')
		0 0 no-repeat;
}
</style>
				<style>
#ele-game-categories-placeholder, #ele-game-categories-inner {
	height: 50px;
}

#ele-game-categories-inner {
	width: 1000px;
 	background-color: #2B2A2A;
	margin: 0 auto;
}

/* 上方bar fixed */
.fixed #ele-game-categories-fixed {
	position: fixed;
	top: 0px;
	z-index: 99;
	width: 100%;
	height: 50px;
}

.fixed #ele-game-categories-inner {
	width: 1000px;
}

.ele-categories-text-wrap {
	display: none;
}

#ele-game-categories-inner .ele-categories-item {
	position: relative;
	float: left;
}

.ele-categories-border {
	position: absolute;
	top: 0;
	right: 0;
	width: 1px;
	height: 50px;
	background-color: #3D3D3D;
	border-left: solid 1px #202020;
}

.ele-border-last {
	display: none;
}

#ele-game-categories-inner a {
	display: block;
	line-height: 50px;
	color: #ACACB2;
	font-size: 16px;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	text-overflow: ellipsis;
	overflow: hidden;
	padding: 0 8px;
	white-space: nowrap;
}

#ele-game-categories-inner a:hover, #ele-game-categories-inner .current
	{
	color: #DC3834;
	background-color: #212121;
}

#ele-game-categories-inner .ele-categories-line {
	height: 1px;
	background-color: #333;
}
.gamenew-trace-current:HOVER {
	color: red;
}
</style>
<input id="e_game_type" value="ag" type="hidden" />
<script type="text/javascript">
$(function(){
	var navBarElement = window.parent.document.getElementById("egameNavBar");
	var egameNavBar = null;
	if(navBarElement){
		egameNavBar = navBarElement.value;
	}
	
	var ag = '${isAgOnOff}';
	var mg = '${isMgOnOff}';
	var pt = '${isPtOnOff}';
	var qt = '${isQtOnOff}';
	var bbin = '${isBbinOnOff}';
	var cq9 = '${isCq9OnOff}';
	var jdb = '${isJdbOnOff}';
	var ttg = '${isTtgOnOff}';
	var mw = '${isMwOnOff}';
	var isb = '${isIsbOnOff}';
	var agDz = '${isAgDzOnOff}';
	var mgDz = '${isMgDzOnOff}';
	
	if(egameNavBar){
		if('bbin' == egameNavBar){
			pageInitGame(egameNavBar,1);
		}else{
			pageInitGame(egameNavBar);
		}
		
		return;
	}else if(agDz  == 'on' || (!agDz && ag == 'on') ){
		pageInitGame('ag');
		return;
	}else if(mgDz  == 'on' || (!mgDz && mg == 'on')){
		pageInitGame('mg');
		return;
	}else if(pt  == 'on'){
		pageInitGame('pt');
		return;
	}else if(qt  == 'on'){
		pageInitGame('qt');
		return;
	}else if(bbin  == 'on'){
		pageInitGame('bbin',1);
		return;
	}else if(cq9  == 'on'){
		pageInitGame('cq9');
		return;
	}else if(jdb  == 'on'){
		pageInitGame('jdb');
		return;
	}else if(ttg  == 'on'){
		pageInitGame('ttg');
		return;
	}else if(mw  == 'on'){
		pageInitGame('mw');
		return;
	}else if(isb  == 'on'){
		pageInitGame('isb');
		return;
	}else{
		alert('暂时没有游戏可玩!');
	}

});

function pageInitGame(type,needLogin){
	if(!needLogin){
		getdata(type);
		$('#ag_egame_mid_menu').css('display', '');
	}
	$('.ele-categories-item').find('a').removeClass('current');
	$('.ele-categories-item').find('a[data-id='+type+']').addClass('current');
	return;
}
</script>

				<div id="ele-game-categories-placeholder" class="fixed">
					<div id="ele-game-categories-fixed"
						style="left: 0px; min-width: 0px;">
						<div id="ele-game-categories-inner" style="width: 1000px;">
							<div class="ele-categories-text-wrap">
								<div id="ele-categories-text" class="categories-item-mgcasino">MG电子</div>
								<i class="fa fa-angle-down"></i>
							</div>
							<div id="ele-game-categories-dropmenu"
								class="ele-game-categories-dropmenu" style="height: 50px;">
								<div class="categories-dropmenu-inner clearfix">
									<!-- 新增捕鱼 -->
									<c:if test="${isByOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="by"
											class="categories-item-link categories-item-fishing"
											href="javascript:getdata('by');"
											title="捕鱼">捕鱼</a>
										<div class="ele-categories-border "></div>
									</div>
									</c:if>
									<!-- end -->
									<c:if test="${isAgDzOnOff eq 'on' || (empty isAgDzOnOff && isAgOnOff eq 'on')}">
									<div class="ele-categories-item">
										<a data-id="ag"
											class="categories-item-link categories-item-bbcasino"
											href="javascript:getdata('ag');"
											title="AG电子">AG电子</a>
										<div class="ele-categories-border "></div>
									</div>
									</c:if>
									<c:if test="${isCq9OnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="cq9"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('cq9');"
											title="CQ9电子">CQ9电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isJdbOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="jdb"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('jdb');"
											title="JDB电子">JDB电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isTtgOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="ttg"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('ttg');"
											title="TTG电子">TTG电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isMwOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="mw"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('mw');"
											title="MW电子">MW电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isIsbOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="isb"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('isb');"
											title="ISB电子">ISB电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isMgDzOnOff eq 'on' || (empty isMgDzOnOff && isMgOnOff eq 'on')}">
									<div class="ele-categories-item">
										<a data-id="mg"
											class="categories-item-link categories-item-mgcasino"
											href="javascript:getdata('mg');"
											title="MG电子">MG电子</a>
										<div class="ele-categories-border "></div>
									</div>
									</c:if>
									<c:if test="${isPtOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="pt"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('pt');"
											title="PT电子">PT电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isQtOnOff eq 'on'}">
									<div class="ele-categories-item">
										<a data-id="qt"
											class="categories-item-link categories-item-ptcasino "
											href="javascript:getdata('qt');"
											title="QT电子">QT电子</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isBbinDzOnOff eq 'on'}">
									<jsp:include page="/common/template/third/page/live_demo.jsp" />
									<div class="ele-categories-item">
										<a data-id="bbin"
											class="categories-item-link1 categories-item-ptcasino "
											<c:if test="${!isLogin }">href="javascript:alert('请先登录!');"</c:if>
											<c:if test="${isLogin }">href="javascript:go('${base}/forwardBbin.do?type=game', '2');"</c:if>
											title="BBIN">BBIN</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
									<c:if test="${isEbetOnOff eq 'on'}">
									<jsp:include page="/common/template/third/page/live_demo.jsp" />
									<div class="ele-categories-item">
										<a data-id="ebet"
											class="categories-item-link1 categories-item-ptcasino "
											<c:if test="${!isLogin }">href="javascript:alert('请先登录!');"</c:if>
											<c:if test="${isLogin }">href="javascript:go('${base}/forwardEbet.do?gameType=5');"</c:if>
											title="EBET">EBET</a>
										<div class="ele-categories-border ele-border-last"></div>
									</div>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
<style>
/*遊戲導覽列*/
.elenew-game-nav-wrap {
	position: relative;
	max-width: 1000px;
	box-sizing: border-box;
	padding-left: 10px;
	margin: 8px auto 0;
}

.elenew-gn-btn {
	float: left;
}

.elenew-gn-icon {
	width: 40px;
	height: 40px;
	cursor: pointer;
	background:
		url("${base }/common/template/third/newEgame/images/game_menu_show.png") 0 0
		no-repeat;
}

.elenew-gn-icon.gn-active, .elenew-gn-icon:hover {
	background-position: 0 100%;
}

.elenew-gn-text-wrap {
	display: none;
}

/*遊戲導覽列 - 下拉選單*/
.elenew-gn-wrap {
	position: absolute;
	top: 100%;
	left: 0;
	z-index: 4;
	display: none;
	width: 559px;
	background-color: #444;
}

.elenew-gn-innerwrap {
	width: 100%;
}

.elenew-game-menu1 {
	float: left;
	width: 359px;
	border-right: 1px solid #333;
	padding: 8px 13px 20px 14px;
	box-sizing: border-box;
}

.elenew-game-menu2 {
	float: left;
	width: 198px;
	padding: 22px 0 10px;
	color: #747474;
	border-left: 1px solid #555;
}

.ele-categories-border2, .ele-border-last2 {
	display: none;
}

/* mg gns isb 無下拉選單右方區塊 */
.gamenew-hall-23 .elenew-gn-wrap, .gamenew-hall-28 .elenew-gn-wrap,
	.gamenew-hall-29 .elenew-gn-wrap, .gamenew-hall-20 .elenew-gn-wrap {
	width: 360px;
}

.gamenew-hall-23 .elenew-gn-innerwrap, .gamenew-hall-28 .elenew-gn-innerwrap,
	.gamenew-hall-29 .elenew-gn-innerwrap, .gamenew-hall-20 .elenew-gn-innerwrap
	{
	background: none;
}

.gamenew-hall-23 .elenew-game-menu1, .gamenew-hall-28 .elenew-game-menu1,
	.gamenew-hall-29 .elenew-game-menu1, .gamenew-hall-20 .elenew-game-menu1
	{
	float: none;
	width: 360px;
	border-right: none;
}

.ele-mg-norakeback-hint {
	position: relative;
	top: -6px;
	padding: 0 10px 7px;
	color: #FFF250;
	font-size: 18px;
}

.elenew-gm-bottom {
	width: 100%;
	height: 8px;
	background-color: #18C49F;
}

.elenew-gm-1 {
	float: left;
	width: 100px;
	height: 90px;
	text-align: center;
	color: #747474;
	font-size: 12px;
	margin: 18px 5px;
	text-decoration: none;
}

.gm-hide-3, .gm-hide-35 {
	display: none;
}

.elenew-game-menu2 .elenew-gm-1 {
	float: none;
	display: block;
	margin: 0 auto 4px;
}

.elenew-gm-1.gm-active, .elenew-gm-1:hover, .elenew-game-menu2:hover .elenew-gm-1 .gm-name
	{
	color: #FFF;
}

.gm-icon {
	display: block;
	margin: 0 auto 3px;
	width: 70px;
	height: 70px;
	background:
		url("${base }/common/template/third/newEgame/images/game_menu_icon_m.png")
		-210px 0;
}

.elenew-gm-1.gm-active .gm-icon-1, .elenew-gm-1:hover .gm-icon-1 {
	background-position: -210px 100%;
}

.gm-icon-2 {
	background-position: -70px 0;
}

.elenew-gm-1.gm-active .gm-icon-2, .elenew-gm-1:hover .gm-icon-2 {
	background-position: -70px 100%;
}

.gm-icon-3 {
	background-position: -490px 0;
}

.elenew-gm-1.gm-active .gm-icon-3, .elenew-gm-1:hover .gm-icon-3,
	.elenew-game-menu2:hover .gm-icon-3 {
	background-position: -490px 100%;
}

.gm-icon-5 {
	background-position: -420px 0;
}

.elenew-gm-1.gm-active .gm-icon-5, .elenew-gm-1:hover .gm-icon-5 {
	background-position: -420px 100%;
}

.gm-icon-35 {
	background-position: 0 0;
}

.elenew-gm-1.gm-active .gm-icon-35, .elenew-gm-1:hover .gm-icon-35 {
	background-position: 0 100%;
}

.gm-icon-81 {
	background-position: -280px 0;
}

.elenew-gm-1.gm-active .gm-icon-81, .elenew-gm-1:hover .gm-icon-81 {
	background-position: -280px 100%;
}

.gm-icon-82 {
	background-position: -490px 0;
}

.elenew-gm-1.gm-active .gm-icon-82, .elenew-gm-1:hover .gm-icon-82 {
	background-position: -490px 100%;
}

.gm-icon-83 {
	background-position: -560px 0;
}

.elenew-gm-1.gm-active .gm-icon-83, .elenew-gm-1:hover .gm-icon-83 {
	background-position: -560px 100%;
}

.gm-icon-84 {
	background-position: -630px 0;
}

.elenew-gm-1.gm-active .gm-icon-84, .elenew-gm-1:hover .gm-icon-84 {
	background-position: -630px 100%;
}

.gamenew-hall-23 .gm-icon-84 {
	background-position: -840px 0;
}

.gamenew-hall-23 .elenew-gm-1.gm-active .gm-icon-84, .gamenew-hall-23 .elenew-gm-1:hover .gm-icon-84
	{
	background-position: -840px 100%;
}

.gamenew-hall-28 .gm-icon-84 {
	background-position: -910px 0;
}

.gamenew-hall-28 .elenew-gm-1.gm-active .gm-icon-84, .gamenew-hall-28 .elenew-gm-1:hover .gm-icon-84
	{
	background-position: -910px 100%;
}

.gamenew-hall-29 .gm-icon-84 {
	background-position: -980px 0;
}

.gamenew-hall-29 .elenew-gm-1.gm-active .gm-icon-84, .gamenew-hall-29 .elenew-gm-1:hover .gm-icon-84
	{
	background-position: -980px 100%;
}

.gamenew-hall-20 .gm-icon-84 {
	background-position: -1050px 0;
}

.gamenew-hall-20 .elenew-gm-1.gm-active .gm-icon-84, .gamenew-hall-20 .elenew-gm-1:hover .gm-icon-84
	{
	background-position: -1050px 100%;
}

.gm-icon-85 {
	background-position: -350px 0;
}

.elenew-gm-1.gm-active .gm-icon-85, .elenew-gm-1:hover .gm-icon-85 {
	background-position: -350px 100%;
}

.gm-icon-91 {
	background-position: -700px 0;
}

.elenew-gm-1.gm-active .gm-icon-91, .elenew-gm-1:hover .gm-icon-91 {
	background-position: -700px 100%;
}

.gm-icon-92 {
	background-position: -1120px 0;
}

.elenew-gm-1.gm-active .gm-icon-92, .elenew-gm-1:hover .gm-icon-92 {
	background-position: -1120px 100%;
}

.gm-icon-93 {
	background-position: -140px 0;
}

.elenew-gm-1.gm-active .gm-icon-93, .elenew-gm-1:hover .gm-icon-93 {
	background-position: -140px 100%;
}

.elenew-gm-2 {
	display: block;
	padding: 3px 14px 4px 24px;
	line-height: 27px;
	color: #747474;
	text-align: left;
	text-decoration: none;
}

.elenew-gm-2.gm-active, .elenew-gm-2:hover {
	color: #FFF;
}

.elenew-gm-2 .gm-num {
	float: right;
	width: 25px;
	height: 25px;
	text-align: center;
}

.elenew-gm-2.gm-active .gm-num, .elenew-gm-2:hover .gm-num {
	background-color: #18C49F;
	border-radius: 99em;
}

/*導覽列文字*/
.elenew-live-trace {
	float: left;
	line-height: 24px;
	font-size: 13px;
	margin-left: 16px;
}

.elenew-live-trace .elenew-trace-title {
	line-height: 40px;
	font-size: 24px;
	color: #FFF;
	vertical-align: middle;
}

.elenew-trace-sub {
	color: #FFF;
	vertical-align: middle;
	margin-left: 2px;
}

.trace-arrow {
	color: #FFF;
}

.gamenew-trace-num {
	cursor: pointer;
}

.gamenew-trace-num-0, .gamenew-trace-num-1, .gamenew-trace-num-2 {
	color: #ff0;
}

.nav-normal>a {
	height: 100%;
	box-sizing: border-box;
}

.nav-normal>a {
	padding: 0 25px;
	line-height: 19px;
	text-decoration: none;
}

.nav-icon {
	display: inline-block;
	vertical-align: text-top;
	width: 18px;
	height: 18px;
	transition: all 0.2s ease;
}

#elenew-search-wrap .elenew-search-btn {
	cursor: pointer;
	display: inline-block;
	width: 40px;
	height: 40px;
	border-radius: 20px;
	text-align: center;
	color: #FFF;
	background-color: #B5271D;
}

#elenew-search-wrap .elenew-search-btn.search-active {
	color: #FFF;
	background-color: #952A22;
}

#elenew-search-wrap .elenew-search-btn i {
	font-size: 24px;
	margin-top: 6px;
}

/*NEWS*/
.elenew-game-nav-wrap a i.nav-icon-news {
	position: absolute;
	top: 0;
	left: 0;
	display: inline-block;
	vertical-align: middle;
	width: 15px;
	height: 33px;
	transition: all 0.2s ease;
	background:
		url('${base }/common/template/third/newEgame/images/new_icon.gif') 0 0
		no-repeat;
}

/*
** 共用 遊戲搜尋
*/
#elenew-search-wrap {
	position: absolute;
	top: 0;
	right: 135px;
	width: 300px;
	height: 40px;
}

#elenew-search-wrap .elenew-search-input {
	float: right;
	width: 268px;
	height: 40px;
	line-height: 40px;
	margin-right: 17px;
	border-radius: 18px 0 0 18px;
	background-color: #FFF;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

#elenew-search-wrap .elenew-search-input input {
	width: 245px;
	height: 40px;
	line-height: 40px\9;
	border: 0;
	background-color: transparent;
	outline: none;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	color: #000;
}

#elenew-search-wrap ::-webkit-input-placeholder { /* WebKit browsers */
	color: #777;
}

#elenew-search-wrap :-ms-input-placeholder { /* Internet Explorer 10+ */
	color: #777;
}

#elenew-search-wrap ::-moz-input-placeholder { /* moz browsers */
	color: #777;
}

#elenew-search-wrap label {
	position: absolute;
	cursor: text;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	color: #777;
}

#elenew-search-wrap .elenew-search-btn {
	position: absolute;
	z-index: 2;
	top: 0;
	right: 0;
	height: 100%;
	cursor: pointer;
	outline: none;
}

@media all and (-webkit-bb:0) {
	input[type="search"]::-webkit-search-cancel-button {
		-webkit-appearance: none;
	}
}

/* view-menu */
.elenew-live-view {
	float: right;
	margin-top: 6px;
}

.elenew-live-view span {
	float: left;
}

.elenew-live-view span:before {
	display: inline-block;
	content: '';
	width: 1px;
	height: 23px;
	background-color: #CCC;
	border-right: solid 1px #FFF;
}

.elenew-live-view a {
	display: inline-block;
	width: 28px;
	height: 28px;
	font-size: 28px;
	line-height: 28px;
	padding-top: 1px;
	color: #A7281F;
	font-family: arial;
	margin: 0 15px;
}

.elenew-live-view a:hover, .elenew-live-view a.view-active {
	color: #F1333D;
}

/* html css */
.elenew-serch-noresult {
	color: #B5271D !important;
}

/* hall menu */
.gamenew-menu-hallmenu {
	position: relative;
	width: 100%;
	height: 100%;
	-webkit-perspective: 800px;
	-moz-perspective: 800px;
	-o-perspective: 800px;
	-ms-perspective: 800px;
	perspective: 800px;
}

.gamenew-hall-current {
	transition: transform 0.3s, box-shadow 0.3s;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-o-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform-origin: 50% 0%;
	-moz-transform-origin: 50% 0%;
	-o-transform-origin: 50% 0%;
	-ms-transform-origin: 50% 0%;
	transform-origin: 50% 0%;
}

.gamenew-hall-current.gamenew-hall-open {
	transform: rotateX(45deg);
	-webkit-transform: rotateX(45deg); /* Safari 和 Chrome */
	-moz-transform: rotateX(45deg); /* Firefox */
}

.gamenew-menu-hallmenu a {
	float: left;
	width: 180px;
	height: 100%;
	border-left: 1px solid #CF2E21;
	border-right: 1px solid #972117;
}

.gamenew-menu-hallmenu a:hover, .gamenew-menu-hallmenu a.gamenew-hall-current
	{
	position: relative;
	background: #6E1710;
	border-left: 1px solid #6E1710;
	border-right: 1px solid #6E1710;
}

.gamenew-menu-hallmenu a:hover:before, .gamenew-menu-hallmenu a.gamenew-hall-current:before
	{
	content: '';
	position: absolute;
	top: 0;
	left: -2px;
	width: 1px;
	height: 100%;
	background: #6E1710;
}

.gamenew-menu-hallmenu a:hover:after, .gamenew-menu-hallmenu a.gamenew-hall-current:after
	{
	content: '';
	position: absolute;
	top: 0;
	right: -2px;
	width: 1px;
	height: 100%;
	background: #6E1710;
}

.gamenew-menu-hallmenu a span {
	background:
		url('/cl/tpl/template/images/element/newgame/lang/zh-cn/game_menu_logo.png')
		50% 0 no-repeat;
	display: block;
	width: 100%;
	height: 100%;
}

.gamenew-trace-option a span {
	float: none;
}

.gamenew-trace {
	position: relative;
	padding: 0 5px;
}

.gamenew-trace-option {
	display: none;
	position: absolute;
	z-index: 3;
	top: 100%;
	left: 50%;
	height: auto;
	width: 80px;
	padding: 4px 0;
	margin-left: -33px;
	border-radius: 0 0 5px 5px;
	background: url('${base }/common/template/third/newEgame/images/bg_black.png');
}

.gamenew-trace-option a {
	display: block;
	line-height: 16px;
	color: #FFF;
	font-size: 13px;
	text-align: center;
	text-decoration: none;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.gamenew-trace-option a:hover {
	text-decoration: underline;
}

.gamenew-trace-current, .gamenew-trace-total {
	padding-right: 5px;
	cursor: pointer;
}

.gamenew-trace-2 .gamenew-trace-current {
	max-width: 215px;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.gamenew-trace-total a {
	color: #FFF;
	text-decoration: none;
}
</style>

<style>
     .mg-game {
        position: relative;
        width: 140px;
        height: 136px;
        margin: 0 auto;
        overflow: hidden;
        cursor:pointer;
       }

      .mg-game img {
          position: absolute;
          left: 0;
          width: 280px;
          height: 136px;
      }

      .mg-game:hover .mg-game img{
           left: -140px;
       }
</style>
<script>
	var dianziNum = $(".categories-dropmenu-inner").children(".ele-categories-item").length;
	var dianziWrapWidth = 991;
	$(".ele-categories-item").css("width",dianziWrapWidth/dianziNum);
</script>