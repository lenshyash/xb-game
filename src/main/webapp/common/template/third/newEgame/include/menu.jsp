<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="ele-game-categories-dropmenu" class="ele-game-categories-dropmenu"
	style="height: 50px;">
	    <div class="categories-dropmenu-inner clearfix">
	        <div class="ele-categories-item">
	            <a data-id="categories-item-bbcasino" class="categories-item-link categories-item-bbcasino "
	            href="/cl/?module=System&amp;method=Game&amp;gameHall=5" title="BB电子">
	                BB电子
	            </a>
	            <div class="ele-categories-border ">
	            </div>
	        </div>
	        <div class="ele-categories-item">
	            <a data-id="categories-item-mgcasino" class="categories-item-link categories-item-mgcasino "
	            href="/cl/?module=System&amp;method=Game&amp;gameHall=23" title="MG电子">
	                MG电子
	            </a>
	            <div class="ele-categories-border ">
	            </div>
	        </div>
	        <div class="ele-categories-item">
	            <a data-id="categories-item-gnscasino" class="categories-item-link categories-item-gnscasino "
	            href="/cl/?module=System&amp;method=Game&amp;gameHall=28" title="GNS电子">
	                GNS电子
	            </a>
	            <div class="ele-categories-border ">
	            </div>
	        </div>
	        <div class="ele-categories-item">
	            <a data-id="categories-item-isbcasino" class="categories-item-link categories-item-isbcasino "
	            href="/cl/?module=System&amp;method=Game&amp;gameHall=29" title="ISB电子">
	                ISB电子
	            </a>
	            <div class="ele-categories-border ">
	            </div>
	        </div>
	        <div class="ele-categories-item">
	            <a data-id="categories-item-ptcasino" class="categories-item-link categories-item-ptcasino current"
	            href="/cl/?module=System&amp;method=Game&amp;gameHall=20" title="PT电子">
	                PT电子
	            </a>
	            <div class="ele-categories-border ele-border-last">
	            </div>
	        </div>
	    </div>
</div>