<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>棋牌游戏</title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<link rel="stylesheet" href="${base }/common/template/third/chess/css/reset.css" />
<link rel="stylesheet" href="${base }/common/template/third/chess/css/index.css" />
<script src="${base }/common/template/third/chess/js/jquery-1.11.0.js" type="text/javascript"
	charset="utf-8"></script>
</head>
<body>
<%--<div class="flexWrap">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg01.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg02.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg03.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg04.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg05.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg06.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg07.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg08.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg09.png">--%>
<%--	<img src="${base}/common/template/third/chess/images/bg10.png">--%>
<%--</div>--%>
	<script type="text/javascript" src="${base }/common/template/third/chess/js/core.js" path=""></script>
	<script type="text/javascript">
		function go(url, type) {
			var userid = '${loginMember.account}';
			var kyOnOff = '${isKyOnOff}';

			if (userid == null || userid == '') {
				alert("请先登录系统");
				return;
			}

			if (type == 12 & kyOnOff == 'off') {
				alert('开元棋牌系统维护');
				return;
			}

			var sw = '';
			csw = '';
			sh = '';
			csh = '';
			ctop = '';
			cleft = '';

			sw = $(window.parent).width();
			sh = $(window.parent).height();
			csw = $(window.parent).width();
			csh = $(window.parent).height();
			ctop = 0;
			cleft = 0;
			var tempUrl = url;
			/* if(type=='3'||type==3){ */
			var windowOpen = window.open("", '_blank', 'width=' + csw
					+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
					+ ',scrollbars=no,resizable=yes,location=no');

			var result = null;
/* 			var ajaxFn = $ajax ? $ajax : $.ajax;//原生态ajax
			ajaxFn({
				url : url,
				dataType : "json",
				type : "post",
				async : false,
				success : function(json) {
					result = json;
				}
			});

			if (!result.success) {
				//alert(result.msg);
				var new_doc = windowOpen.document.open("text/html", "replace");
				new_doc.write("<script>alert('" + result.msg
						+ "');window.close();<\/script>");
				new_doc.close();
				return;
			} */
			/* windowOpen.location.href = result.url; */
			windowOpen.location.href = window.location.origin+"/index/openChess.do?url="+ url;
		}
		<c:if test="${isThOnOff eq 'on'}">
		$(function(){
			$(".game-list.kyTH").children('ul:eq(1)').css("display","none")
			$(".kythButton").children('.buttonTab:eq(0)').click(function () {
				$(this).hover()
				$(".game-list.kyTH").children('ul:eq(0)').fadeOut(500)
				$(".game-list.kyTH").children('ul:eq(1)').fadeIn(500)
			})
			$(".kythButton").children('.buttonTab:eq(1)').click(function () {
				$(".game-list.kyTH").children('ul:eq(1)').fadeOut(500)
				$(".game-list.kyTH").children('ul:eq(0)').fadeIn(500)
			})
		})
		//天豪棋牌
		$(function () {
			let arr =[
				{src:"${base }/common/template/third/chess/images/thlogopc.png",name:"天豪棋牌",OnClick:"go('/forwardTh.do?gameId=0', '12')",styles:"width: 163px;height: 163px;"},
				{src:"${base }/common/template/third/chess/images/1002.png",name:"炸金花",OnClick:"go('/forwardTh.do?gameId=1002', '12')"},
				{src:"${base }/common/template/third/chess/images/1015.png",name:"十三水",OnClick:"go('/forwardTh.do?gameId=1015', '12')"},
				{src:"${base }/common/template/third/chess/images/1018.png",name:"德州扑克",OnClick:"go('/forwardTh.do?gameId=1018', '12')"},
				{src:"${base }/common/template/third/chess/images/1027.png",name:"百人牌九",OnClick:"go('/forwardTh.do?gameId=1027', '12')"},
				{src:"${base }/common/template/third/chess/images/1097.png",name:"四人梭哈",OnClick:"go('/forwardTh.do?gameId=1097', '12')"},
				{src:"${base }/common/template/third/chess/images/1092.png",name:"极速炸金花",OnClick:"go('/forwardTh.do?gameId=1092', '12')"},
				{src:"${base }/common/template/third/chess/images/1062.png",name:"通比梭哈",OnClick:"go('/forwardTh.do?gameId=1062', '12')"},
				{src:"${base }/common/template/third/chess/images/1056.png",name:"运气牛牛",OnClick:"go('/forwardTh.do?gameId=1056', '12')"},
				{src:"${base }/common/template/third/chess/images/1031.png",name:"百人牛牛",OnClick:"go('/forwardTh.do?gameId=1031', '12')"},
				{src:"${base }/common/template/third/chess/images/1034.png",name:"对战牛牛",OnClick:"go('/forwardTh.do?gameId=1034', '12')"},
				{src:"${base }/common/template/third/chess/images/1036.png",name:"通比牛牛",OnClick:"go('/forwardTh.do?gameId=1036', '12')"},
				{src:"${base }/common/template/third/chess/images/1037.png",name:"通杀牛牛",OnClick:"go('/forwardTh.do?gameId=1037', '12')"},
				{src:"${base }/common/template/third/chess/images/1038.png",name:"抢庄牌九",OnClick:"go('/forwardTh.do?gameId=1038', '12')"},
				{src:"${base }/common/template/third/chess/images/1042.png",name:"抢庄牛牛",OnClick:"go('/forwardTh.do?gameId=1042', '12')"},
				{src:"${base }/common/template/third/chess/images/1055.png",name:"看牌抢牛",OnClick:"go('/forwardTh.do?gameId=1055', '12')"},
			]
			let html='' ;
				$.each(arr,function (index, value) {
					html += '<li>';
					html +=		'<div class="img">';
					html +=			'<img src="'+value.src+'" style="'+value.styles+'" />';
					html +=		'</div>';
					html +=		'<div class="txt">'+ value.name+'';
					html +=		'</div> <a href="javascript:void(0);"onclick="'+value.OnClick+'"class="btn"></a>';
					html +=		'</li>'
				})
			$('[class="th"]').html(html)

		})
		</c:if>
		//ky棋牌
		$(function () {
			let arr=[
				{src:"${base }/common/template/third/chess/images/game0.png",name:"开元棋牌",OnClick:"go('/forwardKy.do?gameId=0', '12')"},
				{src:"${base }/common/template/third/chess/images/game1.png",name:"抢庄牛牛",OnClick:"go('/forwardKy.do?gameId=830', '12')"},
				{src:"${base }/common/template/third/chess/images/game2.png",name:"炸金花",OnClick:"go('/forwardKy.do?gameId=220', '12')"},
				{src:"${base }/common/template/third/chess/images/game3.png",name:"极速炸金花",OnClick:"go('/forwardKy.do?gameId=230', '12')"},
				{src:"${base }/common/template/third/chess/images/game4.png",name:"德州扑克",OnClick:"go('/forwardKy.do?gameId=620', '12')"},
				{src:"${base }/common/template/third/chess/images/game5.png",name:"通比牛牛",OnClick:"go('/forwardKy.do?gameId=870', '12')"},
				{src:"${base }/common/template/third/chess/images/game6.png",name:"二八杠",OnClick:"go('/forwardKy.do?gameId=720', '12')"},
				{src:"${base }/common/template/third/chess/images/game7.png",name:"抢庄牌九",OnClick:"go('/forwardKy.do?gameId=730', '12')"},
				{src:"${base }/common/template/third/chess/images/game8.png",name:"三公",OnClick:"go('/forwardKy.do?gameId=860', '12')"},
				{src:"${base }/common/template/third/chess/images/game9.png",name:"21点",OnClick:"go('/forwardKy.do?gameId=600', '12')"},
				{src:"${base }/common/template/third/chess/images/game10.png",name:"押庄龙虎",OnClick:"go('/forwardKy.do?gameId=900', '12')"},
				{src:"${base }/common/template/third/chess/images/game12.png",name:"斗地主",OnClick:"go('/forwardKy.do?gameId=610', '12')"},
				{src:"${base }/common/template/third/chess/images/game16.png",name:"百家乐",OnClick:"go('/forwardKy.do?gameId=910', '12')"},
				{src:"${base }/common/template/third/chess/images/game14.png",name:"十三水",OnClick:"go('/forwardKy.do?gameId=630', '12')"},

			]
			let html='' ;
			$.each(arr,function (index, value) {
				html += '<li>';
				html +=		'<div class="img">';
				html +=			'<img src="'+value.src+'" />';
				html +=		'</div>';
				html +=		'<div class="txt">'+ value.name+'';
				html +=		'</div> <a href="javascript:void(0);"onclick="'+value.OnClick+'"class="btn"></a>';
				html +=		'</li>'
			})
			$('[class="ky"]').html(html)
		})
	</script>
	<div class="game-list kyTH">
		<div class="kythButton">
			<c:if test="${isThOnOff eq 'on'}">
			<div class="buttonTab ky">
			</div>
			<div class="buttonTab th">
			</div>
			</c:if>
		</div>
		<c:if test="${isThOnOff eq 'on'}">
		<ul class="th">

		</ul>
		</c:if>
		<ul class="ky">

		</ul>
	</div>
</body>
</html>
