<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="width:100%;height:100%;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>棋牌游戏</title>
<meta name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="full-screen" content="true" />
<meta name="screen-orientation" content="landscape" />
<meta name="x5-fullscreen" content="true" />
<meta name="360-fullscreen" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/GIF" href="https://gdocker.ky013.com/resource/favicon.ico" />
<script src="${base }/common/template/third/chess/js/jquery-1.11.0.js" type="text/javascript" charset="utf-8"></script>
<script>
   <!--
   if (top.location != self.location)
      top.location=self.location;
   //-->
   $(function(){
	   let webUrl = window.location.href
	   if(webUrl.includes('forwardKy') == true){
		   $("title").text('开元棋牌')
	   }else{
		   $("title").text('天豪棋牌')
	   }
   })

</script>
<script language="javascript">
    //防止页面后退
    history.pushState(null, null, document.URL);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
    });
</script>


<style>
body{margin:0; padding:0; width:100%;height:100%;}
</style>
</head>
<body>

<iframe id="myiframe" scrolling="no" frameborder="0" style="width:100%;height:100%;" allowfullscreen="true"></iframe>
<script>
$(function(){
	
	var result;
	var url = window.location.search.replace('?url=','');
	console.log(url)
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		async : false,
		success : function(json) {
			result = json;
		}
	});
	var sw = '';
	csw = '';
	sh = '';
	csh = '';
	ctop = '';
	cleft = '';

	sw = $(window.parent).width();
	sh = $(window.parent).height();
	csw = $(window.parent).width();
	csh = $(window.parent).height();
	ctop = 0;
	cleft = 0;

	if (!result.success) {
		var windowOpen = window.open("", '_blank', 'width=' + csw
				+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
				+ ',scrollbars=no,location=no,resizable=yes');
		//console.log(windowOpen);
		var new_doc = windowOpen.document.open("text/html", "replace");
		new_doc.write("<script>alert('" + result.msg
				+ "');window.close();<\/script>");
		new_doc.close();
		return;
	};
	var ifm= document.getElementById("myiframe"); 
	ifm.src = result.url;
	//console.log(result.url)

})
/* windowOpen.location.href = result.url; */
</script>
</body>
</html>