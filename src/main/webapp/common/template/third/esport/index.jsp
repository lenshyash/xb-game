<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>电子竞技</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
	<script src="${base }/common/template/third/esport/js/jquery-1.11.0.js" type="text/javascript" charset="utf-8"></script>
	<link href="${base }/common/template/third/esport/css/index.94433163603f8150be7b.css" rel="stylesheet">
</head>
<body>
<%--<script type="text/javascript" src="${base }/common/template/third/esport/js/core.js" path=""></script>--%>
<div class="">
    <div class="esport vsjn2a1JYtawGlTH0M4zR">
		<p class="esportTitle">
			<c:if test="${isIbcOnOff   == 'on'}">
					<span class="titleIBC _3cgYHDJ4vbz4_Q58PbOefs">IBC电竞</span>
			</c:if>
			<c:if test="${isKxOnOff == 'on'}">
				<span class="titleIBC _3cgYHDJ4vbz4_Q58PbOefs" onclick="kxSport()">凯旋电竞</span>
			</c:if>
        <div class="szbHPlt8ZU_vgOL5GUORi">
			<img src="${base }/common/template/third/esport/images/wenan.png" class="_1Fekfpcisl7cimjnX8NpP7 _18I75Q4uxPjA0pyNGho9ww" id="title">
			<img src="${base }/common/template/third/esport/images/zuoren.png" class="_36Bi_gvzaJu9ayX8Opt1zn _2ZiT5eoxXI_iURxOiB-aHD" id="person1">
			<img src="${base }/common/template/third/esport/images/youren.png" class="A1PZKK4noCa1YEB-TeO2s _2S9VaPIhwUUvMgYFmGQk9O" id="person2">
			<img src="${base }/common/template/third/esport/images/youxi_icon.png" class="_3p6s7E-XMsYfsh8AhQ_OG0" id="person3">
			<img src="${base }/common/template/third/esport/images/zuoxian.png" class="FGhHG0oq6EL97IHTUFQPT" id="person4">
			<img src="${base }/common/template/third/esport/images/youxian.png" class="_1TvQPt5AZEtCmrZVBk65no" id="person5">
			<c:if test="${isIbcOnOff   == 'on'}"><i class="Hd4SeivXfxYbJVpyvHeM2" id="playBtn" >立即投注</i></c:if>
		</div>
    </div>
	<script>
		$("#playBtn").click(function () {
			$.ajax({
				url:"${base}/native/forwardReal.do",
				type:"post",
				data:{playCode:"ibc",eSport:1},
				success:function (res) {
					if(!res.success){
						alert(res.msg) 
						return;
					}else {
						window.open(res.content.url, 'newwindow', 'height=936, width=1516, top=20, left=20, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no')
						// $("#playBtn").prop("href",res.content.url)
					}
				},
			})
		})
	</script>
	<script>
		$(function () {
			if(location.host == 'www.xb336.com' ||location.host == 'xb336.com'){
				$("#kxdj").show()
			}
		})
		function kxSport(){
			$.ajax({
				url:"${base}/forwardKx.do",
				type:"post",
				data:{h5:0},
				success:function (res) {
					if(!res.success){
						alert(res.msg)
						return;
					}else {
						let url = res.url.substring(0,res.url.length-2)
						res.url = url + '&host=' + location.host +'#/'
						window.open(res.url)
						// $("#playBtn").prop("href",res.content.url)
					}
				},
			})
		}
	</script>
</div>
</body>
</html>
