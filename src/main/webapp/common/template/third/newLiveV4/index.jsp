<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLiveV4/css/style.css">
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLiveV4/css/casino.css">
<script type="text/javascript"
	src="${base}/common/template/third/newLiveV4/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript"
	src="${base}/common/template/third/newLiveV4/js/gaming-box.js"></script>
</head>


<body>
	<div class="AboutUs_content_box">
		<div class="content">
			<div class="bwrap bg-5c3917">
				<div class="ele-live-wrap center-block" style="width: 900px">
					<!-- cards wrap frame -->
					<div class="ele-live-align">
						<div class="gallery">
							<li class="gallery-picture ag"></li>
							<li class="gallery-picture ds"></li>
							<li class="gallery-picture mg"></li>
							<li class="gallery-picture bbin"></li>
							<li class="gallery-picture ab"></li>
							<li class="gallery-picture og"></li>
							<li class="gallery-picture bg"></li>
							<li class="gallery-picture vr"></li>
						</div>

						<div class="btn-row" style="display: none;">
							<!-- 游戏规则 -->
<%--							<a href="javascript:;" target="_blank" id="egame_rule">游戏规则</a>--%>
							<!-- 进入游戏 -->
							<a href="javascript:;" id="locaUrl">进入游戏</a>
						</div>

						<div class="ele-live-notice">
							<h4>请滑动鼠标指针选择下方的博彩公司，祝您游戏愉快！</h4>
						</div>

						<!-- cards -->
						<div class="cards-box">
							
						</div>
						<!-- end cards -->
					</div>
				</div>
			</div>
		</div>
		<!-- mg bbin ag og ab  
		bg vr ds -->
		<script type="text/javascript">
			var base = '${base}';
			var clients_group = {
				0 : [ "ag",
						'<img src="'+base+'/common/template/third/newLiveV4/images/ag.jpg" alt="ag">',
					<c:if test="${isAgOnOff eq 'on'}">"go('${base}/forwardAg.do', '1')" </c:if>
					<c:if test="${isAgOnOff eq 'off'}">"alert('敬请期待')" </c:if>
				]

				,
				1 : [
						"ds",
						'<img src="'+base+'/common/template/third/newLiveV4/images/ds.jpg" alt="ds">',
					<c:if test="${isDsOnOff eq 'on'}">"go('${base}/forwardDs.do?gameType=1', '8')"</c:if>
					<c:if test="${isDsOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				2 : [ "mg",
						'<img src="'+base+'/common/template/third/newLiveV4/images/mg.jpg" alt="mg">',
					<c:if test="${isMgOnOff eq 'on'}">"go('${base}/forwardMg.do?gameType=1', '3')"</c:if>
					<c:if test="${isMgOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				3 : [
						"bbin",
						'<img src="'+base+'/common/template/third/newLiveV4/images/bbin.jpg" alt="bbin">',
                    <c:if test="${isBbinOnOff eq 'on'}">"go('${base}/forwardBbin.do?type=live', '2')"</c:if>
                    <c:if test="${isBbinOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				4 : [ "ab",
						'<img src="'+base+'/common/template/third/newLiveV4/images/ab.jpg" alt="ab">',
                    <c:if test="${isAbOnOff eq 'on'}">"go('${base}/forwardAb.do', '5')"</c:if>
                    <c:if test="${isAbOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				5 : [ "og",
						'<img src="'+base+'/common/template/third/newLiveV4/images/og.jpg" alt="og">' ,
                    <c:if test="${isOgOnOff eq 'on'}">"go('${base}/forwardOg.do?gameType=desktop', '7')"</c:if>
                    <c:if test="${isOgOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				6 : [ "bg",
						'<img src="'+base+'/common/template/third/newLiveV4/images/bg.jpg" alt="bg">' ,
                    <c:if test="${isBgOnOff eq 'on'}">"go('${base}/forwardBg.do?type=2', '98')"</c:if>
                    <c:if test="${isBgOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]

				,
				7 : [ "vr",
						'<img src="'+base+'/common/template/third/newLiveV4/images/vr.jpg" alt="vr">',
                    <c:if test="${isVrOnOff eq 'on'}">"go('${base}/forwardVr.do', '97')"</c:if>
                    <c:if test="${isVrOnOff eq 'off'}">"alert('敬请期待')"</c:if>
				]
			};
			var list = "<div class='card ag' style='left:0px'></div> "
					+ "<div class='card ds' style='left:112.5px'></div> "
					+ "<div class='card bbin' style='left:225px'></div> "
					+ "<div class='card mg' style='left:337.5px'></div> "
					+ "<div class='card ab' style='left:450px'></div> "
					+ "<div class='card og' style='left:562.5px'></div> "
					+ "<div class='card bg' style='left:675px'></div> "
					+ "<div class='card vr' style='left:787.5px'></div> "
			$('.cards-box').html(list);
		</script>
	</div>
	<jsp:include page="/common/template/third/page/live_demo.jsp" />
</body>
</html>