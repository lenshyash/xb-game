<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<link rel="stylesheet" type="text/css" href="${base}/common/template/third/newLiveV2/css/site.css">
<link rel="stylesheet" type="text/css" href="${base}/common/template/third/newLiveV2/css/live.css">
<script type="text/javascript"
	src="${base}/common/template/third/newLiveV2/js/jquery-1.8.3.min.js"></script>
</head>

<body>
<div class="wrapper">
	<section id="lobby" ng-controller="LobbiesCtrl" class="ng-scope">
	    <ul class="game-list">
	        <li class="bb hand" onclick="go('${base}/forwardBbin.do?type=live', '2');"></li>
	        <li class="ag hand" onclick="go('${base}/forwardAg.do', '1');"></li>
	        <li class="mg hand" onclick="go('${base}/forwardMg.do?gameType=1', '3');"></li>
	        <li class="ds hand" onclick="go('${base}/forwardDs.do?gameType=1', '8');"></li>
	        <li class="ab hand" onclick="go('${base}/forwardAb.do', '5');"></li>
	        <li class="og hand" onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"></li>
	        <li class="bg hand" onclick="go('${base}/forwardBg.do?type=2', '98');"></li>
	        <li class="vr hand" onclick="go('${base}/forwardVr.do', '97');"></li>
 	        <li class="more hand" onclick="alert('即将开放!');">
	    </ul>
	</section>
</div>
<style>
	.hand{
		cursor: pointer;
	}
</style>
	<jsp:include page="/common/template/third/page/live_demo.jsp" />
</body>
</html>