<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="sportTab">
	<c:if test="${isHgSysOnOff ne 'off'}">
	<div class="sport_btn" onclick="locaSport('hg')">
		<img
			src="${base }/common/template/sports/hg/images/sports/hgsport.png" />皇冠体育
	</div>
	</c:if>
	<c:if test="${isTsOnOff == 'on'}">
		<c:if test="${!isLogin }">
			<div class="sport_btn" onclick="locaSport('sb')">
				<img
					src="${base }/common/template/sports/hg/images/sports/Saba_logo.png" />沙巴体育
			</div>
		</c:if>
		<c:if test="${isLogin }">
			<div class="sport_btn" onclick="locaSport('sbgo')">
				<img
					src="${base }/common/template/sports/hg/images/sports/Saba_logo.png" />沙巴体育
			</div>
		</c:if>
	</c:if>
	<c:if test="${isBBTYOnOff == 'on'}">
	<div class="sport_btn" onclick="go('${base}/forwardBbin.do?type=ball')">
		<img
			src="${base }/common/template/sports/hg/images/sports/bb_sports.png" />BB体育
	</div>
	</c:if>
	<c:if test="${isTsOnOff == 'on' || isBBTYOnOff == 'on'}">
		<div id="sportPoint" style="color:red;line-height: 75px;font-size:16px">&nbsp;&nbsp;
			*<c:if test="${isTsOnOff == 'on'}">沙巴体育</c:if>  <c:if test="${isBBTYOnOff == 'on'}">BB体育</c:if>是三方体育，需要进行额度转换才能下注
		</div>
	</c:if>
</div>
<script>
	$(function(){
		var nem = '${domainFolder}';
		var doc = document.getElementById('sportPoint');
		if(nem == 'b05201'){
			doc.style.color = '#ffeb3b';
		}
	})
	function locaSport(e) {
		if (e == 'hg') {
			$("#mainframe").attr('src', base + '/sports/hg/index.do')
		}
		if (e == 'sb') {
			/* $("#mainframe").attr('src',
					'https://mkt.xinbozr.com/vender.aspx?lang=cs') */
					alert('请先登录系统')
		}
		if (e == 'sbgo') {
			$.ajax({
				url : base + '/forwardIbc.do',
				dataType : "json",
				type : "post",
				async : false,
				success : function(data) {
					var url = data.url
					window.open(url,"沙巴体育","1020","800");
				}
			});
		}
	}
</script>
<style>
#sportTab {
	height: 60px;
	width: 1000px;
	margin: 0 auto;
	text-align: left;
}

.sport_btn {
	float: left;
	width: 114px;
	height: 50px;
	border-radius: 3px;
	background: #f1f1f1;
	margin-left: 5px;
	cursor: pointer;
	transition: .5s;
	display: flex;
	align-items: center;
	justify-content: center;
	color:#000;
}

.sport_btn:hover {
	background: #AD6908;
	color: #fff;
}

.sport_btn img {
	width: 40px;	
	height: 40px;
	float: left;
	margin-right: 5px;
}
</style>
<jsp:include page="/common/template/third/page/live_demo.jsp" />