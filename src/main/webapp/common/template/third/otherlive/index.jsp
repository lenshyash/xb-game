<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="${base}/common/template/third/otherlive/css/game.css" media="screen" rel="stylesheet" type="text/css">
  <script src="${base}/common/template/third/otherlive/js/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="${base}/common/template/third/newEgame/js/common_v2.js?v=8"></script>
  <%-- <jsp:include page="include/head.jsp"></jsp:include>
  <jsp:include page="include/head2.jsp"></jsp:include> --%>
</head>
<body>
<!-- head end  -->
<input id="base" value="${base }" type="hidden">
<input id="userid" value="${isLogin }" type="hidden">
<script>
	var base = $("#base").val()
</script>
<div id="center">
  <div class="wrapc" id="wrapc">
	   <div id="hdpublic"></div>
	   <div style="clear:both;"></div>
	   <div id="wrapboxbg" style="background:url(${base}/common/template/third/otherlive/images/_gameimg/title_game.jpg) no-repeat center top #291715;">
		   <div id="wrapbox">
				<div id="wrapw">
				  <div id="wrapwhead"></div>
				  <div id="wrapwcent">
			  		<div class="gamemiddleboxcenter">
					</div>
				  </div>
				</div>

    <div class="newgame_menubox">
        <ul>
            <li><a href="#">PT真人(PT)</a></li>
            <li style="float: right;border-left: 2px solid #656262;"><a href="javascript:parent.location.reload()">返回</a></li>
        </ul>
    </div>
    <!--<div class="newgame_menu2">-->
        <!--<div class="newgame_menu2_text">游戏分类：</div>-->
        <!--<div class="newgame_menu2_list">-->
            <!--<a href="https://www.381550.com/mg/index/video">视屏扑克</a>-->
            <!--<a href="https://www.381550.com/mg/index/table">桌牌</a>-->
            <!--<a href="https://www.381550.com/mg/index/card">刮刮乐</a>-->
            <!--<a href="https://www.381550.com/mg/index/slot">老虎机</a>-->
            <!--<a href="https://www.381550.com/mg/index/other">其它</a>-->
        <!--</div>-->
    <!--</div>-->

	<div class="newgame_main">
		<div id="mgwrap">

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_ShowdownSaloon_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bal');" target="_blank">真人百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','7bal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_OurDays_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','7bal');" target="_blank">真人百家乐（7座）</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_BookOfOz_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bal');" target="_blank">迷你百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_ActionOpsSnow&amp;Sable_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bal');" target="_blank">金色百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bjl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_BreakAwayDeluxe_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bjl');" target="_blank">晚会百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','vbal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_EmperorOfTheSea_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','vbal');" target="_blank">VIP百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bjl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_luckyTwins_ZH.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bjl');" target="_blank">真人21点</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','rofl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_GoldSeries_MultiWheelRoulette1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','rofl');" target="_blank">实况法式轮盘赌</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','rol');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_WildChampions1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','rol');" target="_blank">實況轮盘（分类）</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','chel');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_WildCatch.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','chel');" target="_blank">实况Casino Hold'Em</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','hilol');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_WhiteBuffalo.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','hilol');" target="_blank">实况高低牌</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','aogrol');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_WhackaJackpot1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','aogrol');" target="_blank">神灵时代：轮盘 奖池</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bjl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_VictorianVillian.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bjl');" target="_blank">休闲二十一点贵宾版</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bal');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_TurtleyAwesome1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bal');" target="_blank">威望迷你百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','bjl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_GoldSeries_TriplePocket1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','bjl');" target="_blank">皇家二十一点</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','plba');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_Triangulation.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','plba');" target="_blank">实况累积百家乐</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','sbl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_TigerVsBear.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','sbl');" target="_blank">实况骰宝（分在桌牌）</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','ubjl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_ThroneOfEgypt1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','ubjl');" target="_blank">实况无限制21点</a></div>
		 </div>

		 <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('pt','rodl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_RatPack1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('pt','rodl');" target="_blank">实况VIP罗盘</a></div>
		 </div>

     <div class="mgbox">
			 <div class="mgimg"><a onclick="toEgame('ag','dtl');" target="_blank" style="background-image:url(${base}/common/template/third/otherlive/images/_gameimg/BTN_TheLostPrincessAnastasia1.png); background-repeat:no-repeat;"></a></div>
			 <div class="mgbtxt"><a onclick="toEgame('ag','dtl');" target="_blank">龙虎</a></div>
		 </div>
		</div>
	</div>
		</div>
   	</div>
  </div>
</div>
<%-- <jsp:include page="include/footer.jsp"></jsp:include> --%>
</body>
</html>
