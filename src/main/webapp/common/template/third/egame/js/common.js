var data = [ {
	"ButtonImagePath" : "BTN_Junglejim_zh",
	"DisplayName" : "丛林吉姆-黄金国",
	"typeid" : 1,
	"LapisId" : "66481"
}, {
	"ButtonImagePath" : "BTN_108Empire_ZH.jpg",
	"DisplayName" : "108好汉",
	"typeid" : 1,
	"LapisId" : "59065"
}, {
	"ButtonImagePath" : "BTN_3Empire_ZH.jpg",
	"DisplayName" : "三國",
	"typeid" : 1,
	"LapisId" : "59067"
}, {
	"ButtonImagePath" : "BTN_5ReelDrive1",
	"DisplayName" : "5卷的驱动器",
	"typeid" : 1,
	"LapisId" : "28114"
}, {
	"ButtonImagePath" : "BTN_AcesAndEights1",
	"DisplayName" : "王牌和八分",
	"typeid" : 6,
	"LapisId" : "28116"
}, {
	"ButtonImagePath" : "BTN_AcesFacesPowerPoker1",
	"DisplayName" : "王牌和面孔",
	"typeid" : 6,
	"LapisId" : "28122"
}, {
	"ButtonImagePath" : "BTN_AcesFaces1",
	"DisplayName" : "王牌和面孔",
	"typeid" : 6,
	"LapisId" : "28128"
}, {
	"ButtonImagePath" : "BTN_AdventurePalaceHD",
	"DisplayName" : "宫廷历险",
	"typeid" : 1,
	"LapisId" : "28134"
}, {
	"ButtonImagePath" : "BTN_AgeofDiscovery3",
	"DisplayName" : "大航海时代",
	"typeid" : 1,
	"LapisId" : "28136"
}, {
	"ButtonImagePath" : "BTN_AgentJaneBlonde7",
	"DisplayName" : "美女秘探",
	"typeid" : 1,
	"LapisId" : "28138"
}, {
	"ButtonImagePath" : "BTN_AlaskanFishing1",
	"DisplayName" : "阿拉斯加钓鱼",
	"typeid" : 1,
	"LapisId" : "28144"
}, {
	"ButtonImagePath" : "BTN_AlaxeInZombieland",
	"DisplayName" : "亚历山大在尸乐园",
	"typeid" : 1,
	"LapisId" : "28148"
}, {
	"ButtonImagePath" : "BTN_AllAces1",
	"DisplayName" : "所有王牌扑克",
	"typeid" : 6,
	"LapisId" : "28150"
}, {
	"ButtonImagePath" : "BTN_AllAmerican1",
	"DisplayName" : "所有美国",
	"typeid" : 6,
	"LapisId" : "28152"
}, {
	"ButtonImagePath" : "BTN_USRoulette1",
	"DisplayName" : "美式轮盘",
	"typeid" : 5,
	"LapisId" : "28204"
}, {
	"ButtonImagePath" : "BTN_ArcticFortune1",
	"DisplayName" : "极寒鸿运",
	"typeid" : 1,
	"LapisId" : "28166"
}, {
	"ButtonImagePath" : "BTN_Ariana_ZH",
	"DisplayName" : "爱丽娜",
	"typeid" : 1,
	"LapisId" : "28174"
}, {
	"ButtonImagePath" : "BTN_AroundTheWorld",
	"DisplayName" : "环游世界",
	"typeid" : 1,
	"LapisId" : "28182"
}, {
	"ButtonImagePath" : "BTN_AsianBeauty1",
	"DisplayName" : "亚洲美人",
	"typeid" : 1,
	"LapisId" : "28188"
},
/**{
	"ButtonImagePath" : "BTN_GoldSeries_AtlanticCityBlackjack1",
	"DisplayName" : "大西洋城黄金21点",
	"typeid" : 5,
	"LapisId" : "28192"
}**/
, {
	"ButtonImagePath" : "BTN_AvalonHD_ZH",
	"DisplayName" : "阿瓦隆",
	"typeid" : 1,
	"LapisId" : "28194"
}, {
	"ButtonImagePath" : "BTN_AvalonII",
	"DisplayName" : "阿瓦隆2",
	"typeid" : 1,
	"LapisId" : "28198"
}, {
	"ButtonImagePath" : "BTN_Baccarat2",
	"DisplayName" : "百家乐",
	"typeid" : 5,
	"LapisId" : "28234"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_Baccarat1",
	"DisplayName" : "百家乐黄金版",
	"typeid" : 5,
	"LapisId" : "28236"
}, {
	"ButtonImagePath" : "BTN_BasketballStar_ZH",
	"DisplayName" : "篮球巨星",
	"typeid" : 1,
	"LapisId" : "44751"
}, {
	"ButtonImagePath" : "BTN_BattlestarGalactica1",
	"DisplayName" : "太空堡垒",
	"typeid" : 1,
	"LapisId" : "28240"
}, {
	"ButtonImagePath" : "BTN_BeerFest1",
	"DisplayName" : "啤酒巨星",
	"typeid" : 4,
	"LapisId" : "28242"
}, {
	"ButtonImagePath" : "BTN_Belissimo17",
	"DisplayName" : "超级厨王",
	"typeid" : 2,
	"LapisId" : "28248"
}, 
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_BigFive1",
	"DisplayName" : "大五黄金21点",
	"typeid" : 5,
	"LapisId" : "28260"
},**/ 
{
	"ButtonImagePath" : "BTN_BigBreak1",
	"DisplayName" : "大破",
	"typeid" : 1,
	"LapisId" : "28266"
}, {
	"ButtonImagePath" : "BTN_BigChef_ZH",
	"DisplayName" : "厨神",
	"typeid" : 1,
	"LapisId" : "28268"
}, {
	"ButtonImagePath" : "BTN_BigKahuna1",
	"DisplayName" : "征服钱海",
	"typeid" : 1,
	"LapisId" : "28270"
}, {
	"ButtonImagePath" : "BTN_BigKahuna_SL1",
	"DisplayName" : "征服钱海-蛇与梯子",
	"typeid" : 1,
	"LapisId" : "28272"
}, {
	"ButtonImagePath" : "BTN_BigTop1",
	"DisplayName" : "马戏篷",
	"typeid" : 1,
	"LapisId" : "28274"
}, {
	"ButtonImagePath" : "BTN_BikiniParty_ZH",
	"DisplayName" : "比基尼派对",
	"typeid" : 1,
	"LapisId" : "46497"
}, {
	"ButtonImagePath" : "BTN_BingoBonanza1",
	"DisplayName" : "宾果富豪",
	"typeid" : 4,
	"LapisId" : "28276"
}, {
	"ButtonImagePath" : "BTN_BonusDeucesWild1",
	"DisplayName" : "疯狂奖金局末平分",
	"typeid" : 6,
	"LapisId" : "28280"
}, {
	"ButtonImagePath" : "BTN_BonusDeucesWild1",
	"DisplayName" : "奖金扑克",
	"typeid" : 6,
	"LapisId" : "28282"
}, {
	"ButtonImagePath" : "BTN_BonusPokerDeluxe1",
	"DisplayName" : "豪华奖金扑克",
	"typeid" : 6,
	"LapisId" : "28284"
}, {
	"ButtonImagePath" : "BTN_BootyTime",
	"DisplayName" : "藏宝时间",
	"typeid" : 1,
	"LapisId" : "28286"
}, {
	"ButtonImagePath" : "BTN_BowledOver1",
	"DisplayName" : "击倒",
	"typeid" : 4,
	"LapisId" : "28290"
}, {
	"ButtonImagePath" : "BTN_BreakAway1",
	"DisplayName" : "摆脱",
	"typeid" : 1,
	"LapisId" : "28292"
}, {
	"ButtonImagePath" : "BTN_BreakDaBank1",
	"DisplayName" : "抢银行",
	"typeid" : 2,
	"LapisId" : "28296"
}, {
	"ButtonImagePath" : "BTN_BreakDaBankAgain1",
	"DisplayName" : "银行爆破",
	"typeid" : 1,
	"LapisId" : "28310"
}, {
	"ButtonImagePath" : "BTN_BrideZilla",
	"DisplayName" : "新娘吉拉",
	"typeid" : 1,
	"LapisId" : "29601"
}, {
	"ButtonImagePath" : "BTN_BubbleBonanza",
	"DisplayName" : "泡泡富豪",
	"typeid" : 7,
	"LapisId" : "29591"
}, {
	"ButtonImagePath" : "BTN_BunnyBoiler1",
	"DisplayName" : "兔子锅炉",
	"typeid" : 4,
	"LapisId" : "28278"
}, {
	"ButtonImagePath" : "BTN_BunnyBoilerGold1",
	"DisplayName" : "黄金兔子锅炉",
	"typeid" : 7,
	"LapisId" : "28330"
}, {
	"ButtonImagePath" : "BTN_BurningDesire1",
	"DisplayName" : "燃烧的欲望",
	"typeid" : 1,
	"LapisId" : "28332"
}, {
	"ButtonImagePath" : "BTN_BushTelegraph1",
	"DisplayName" : "丛林摇摆",
	"typeid" : 1,
	"LapisId" : "28336"
}, {
	"ButtonImagePath" : "BTN_BustTheBank1",
	"DisplayName" : "抢劫银行",
	"typeid" : 1,
	"LapisId" : "29555"
}, {
	"ButtonImagePath" : "BTN_Carnaval2",
	"DisplayName" : "狂欢节",
	"typeid" : 1,
	"LapisId" : "28350"
}, {
	"ButtonImagePath" : "BTN_CashClams2",
	"DisplayName" : "现金蚬",
	"typeid" : 2,
	"LapisId" : "28354"
}, {
	"ButtonImagePath" : "BTN_CashCrazy9",
	"DisplayName" : "财运疯狂",
	"typeid" : 2,
	"LapisId" : "28358"
}, {
	"ButtonImagePath" : "BTN_Cashanova1",
	"DisplayName" : "卡萨努瓦",
	"typeid" : 1,
	"LapisId" : "28364"
}, {
	"ButtonImagePath" : "BTN_Cashapillar1",
	"DisplayName" : "昆虫派对",
	"typeid" : 1,
	"LapisId" : "28368"
}, {
	"ButtonImagePath" : "BTN_Cashapillar1",
	"DisplayName" : "昆虫派对",
	"typeid" : 7,
	"LapisId" : "28372"
}, {
	"ButtonImagePath" : "BTN_Cashville1",
	"DisplayName" : "挥金如土",
	"typeid" : 1,
	"LapisId" : "28376"
}, {
	"ButtonImagePath" : "BTN_CentreCourt1",
	"DisplayName" : "中心球场",
	"typeid" : 1,
	"LapisId" : "28380"
}, {
	"ButtonImagePath" : "BTN_ChainMail_ZH",
	"DisplayName" : "锁子甲",
	"typeid" : 1,
	"LapisId" : "28384"
}, 
/**{
	"ButtonImagePath" : "BTN_GoldSeries_ClassicBlackjack1",
	"DisplayName" : "经典黄金21点",
	"typeid" : 5,
	"LapisId" : "28398"
},**/
 {
	"ButtonImagePath" : "BTN_CoolBuck1",
	"DisplayName" : "酷巴克",
	"typeid" : 2,
	"LapisId" : "28400"
}, {
	"ButtonImagePath" : "BTN_CoolWolf1",
	"DisplayName" : "酷狼",
	"typeid" : 1,
	"LapisId" : "28404"
}, {
	"ButtonImagePath" : "BTN_CosmicCat1",
	"DisplayName" : "宇宙猫",
	"typeid" : 2,
	"LapisId" : "28408"
}, {
	"ButtonImagePath" : "BTN_CouchPotato2",
	"DisplayName" : "沙发土豆",
	"typeid" : 2,
	"LapisId" : "28414"
}, {
	"ButtonImagePath" : "BTN_Crazy80s4",
	"DisplayName" : "疯狂的80年代",
	"typeid" : 1,
	"LapisId" : "28418"
}, {
	"ButtonImagePath" : "BTN_CrazyChameleons1",
	"DisplayName" : "疯狂的变色龙",
	"typeid" : 1,
	"LapisId" : "28422"
}, {
	"ButtonImagePath" : "BTN_CricketStar.jpg",
	"DisplayName" : "板球明星",
	"typeid" : 1,
	"LapisId" : "29561"
}, {
	"ButtonImagePath" : "BTN_Crocodopolis",
	"DisplayName" : "鳄鱼建城邦",
	"typeid" : 1,
	"LapisId" : "28424"
}, {
	"ButtonImagePath" : "BTN_CrownAndAnchor1",
	"DisplayName" : "国际鱼虾蟹骰宝",
	"typeid" : 7,
	"LapisId" : "28428"
}, {
	"ButtonImagePath" : "BTN_CryptCrusade1",
	"DisplayName" : "地穴的远征",
	"typeid" : 7,
	"LapisId" : "28432"
}, {
	"ButtonImagePath" : "BTN_CryptCrusadeGold1",
	"DisplayName" : "黄金地穴的远征",
	"typeid" : 7,
	"LapisId" : "28436"
}, {
	"ButtonImagePath" : "BTN_Cyberstud1",
	"DisplayName" : "网络扑克",
	"typeid" : 5,
	"LapisId" : "28442"
}, {
	"ButtonImagePath" : "BTN_DawnoftheBread1",
	"DisplayName" : "黎明的面包",
	"typeid" : 7,
	"LapisId" : "28448"
}, {
	"ButtonImagePath" : "BTN_DeckTheHalls1",
	"DisplayName" : "闪亮的圣诞节 ？",
	"typeid" : 1,
	"LapisId" : "28454"
}, {
	"ButtonImagePath" : "BTN_DeucesJokerPowerPoker1",
	"DisplayName" : "百搭小丑扑克",
	"typeid" : 6,
	"LapisId" : "28460"
}, {
	"ButtonImagePath" : "BTN_DeucesJoker1",
	"DisplayName" : "百搭小丑扑克",
	"typeid" : 6,
	"LapisId" : "28466"
}, {
	"ButtonImagePath" : "BTN_DeucesWildPowerPoker1",
	"DisplayName" : "万能两点",
	"typeid" : 6,
	"LapisId" : "28470"
}, {
	"ButtonImagePath" : "BTN_DeucesWildPowerPoker1",
	"DisplayName" : "万能两点",
	"typeid" : 6,
	"LapisId" : "28472"
}, {
	"ButtonImagePath" : "BTN_DinoMight1",
	"DisplayName" : "恐龙迪诺",
	"typeid" : 1,
	"LapisId" : "28476"
}, {
	"ButtonImagePath" : "BTN_DrLove",
	"DisplayName" : "医生的爱",
	"typeid" : 1,
	"LapisId" : "28478"
}, {
	"ButtonImagePath" : "BTN_Dogfather2",
	"DisplayName" : "狗爸爸",
	"typeid" : 1,
	"LapisId" : "28480"
}, {
	"ButtonImagePath" : "BTN_DolphinQuest",
	"DisplayName" : "寻访海豚",
	"typeid" : 1,
	"LapisId" : "28484"
}, {
	"ButtonImagePath" : "BTN_DoubleBonus1",
	"DisplayName" : "双重奖金扑克",
	"typeid" : 6,
	"LapisId" : "28486"
}, {
	"ButtonImagePath" : "BTN_DoubleDoubleBonus1",
	"DisplayName" : "换牌扑克",
	"typeid" : 6,
	"LapisId" : "28492"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_DoubleExposureBlackjack1",
	"DisplayName" : "双重黄金曝光",
	"typeid" : 5,
	"LapisId" : "28494"
}, {
	"ButtonImagePath" : "BTN_DoubleJokerPowerPoker1",
	"DisplayName" : "双百搭",
	"typeid" : 6,
	"LapisId" : "28498"
}, {
	"ButtonImagePath" : "BTN_DoubleJoker1",
	"DisplayName" : "双百搭",
	"typeid" : 6,
	"LapisId" : "28506"
}, {
	"ButtonImagePath" : "BTN_DoubleMagic1",
	"DisplayName" : "双魔",
	"typeid" : 2,
	"LapisId" : "28510"
}, {
	"ButtonImagePath" : "BTN_DoubleWammy1",
	"DisplayName" : "双重韦密",
	"typeid" : 2,
	"LapisId" : "28514"
}, {
	"ButtonImagePath" : "BTN_DrWattsUp1",
	"DisplayName" : "恐怖实验室",
	"typeid" : 1,
	"LapisId" : "28522"
}, {
	"ButtonImagePath" : "BTN_DragonDance_ZH",
	"DisplayName" : "舞龙",
	"typeid" : 1,
	"LapisId" : "46494"
}, {
	"ButtonImagePath" : "BTN_DragonsFortune1",
	"DisplayName" : "龙的鸿运",
	"typeid" : 4,
	"LapisId" : "28524"
}, {
	"ButtonImagePath" : "BTN_DroneWars",
	"DisplayName" : "熊峰战争",
	"typeid" : 1,
	"LapisId" : "28542"
}, {
	"ButtonImagePath" : "BTN_EaglesWings1",
	"DisplayName" : "老鹰的翅膀",
	"typeid" : 1,
	"LapisId" : "28562"
}, {
	"ButtonImagePath" : "BTN_ElectricDiva_zh",
	"DisplayName" : "雷电歌后",
	"typeid" : 1,
	"LapisId" : "61497"
}, {
	"ButtonImagePath" : "BTN_ElectroBingo1",
	"DisplayName" : "电宾果",
	"typeid" : 7,
	"LapisId" : "28566"
}, {
	"ButtonImagePath" : "BTN_Elementals2",
	"DisplayName" : "水果怪兽",
	"typeid" : 1,
	"LapisId" : "28568"
}, {
	"ButtonImagePath" : "BTN_EnchantedWoods1",
	"DisplayName" : "魔法森林",
	"typeid" : 1,
	"LapisId" : "28572"
},
/**
{
	"ButtonImagePath" : "BTN_GoldSeries_EuroBlackjack1",
	"DisplayName" : "欧洲黄金21点",
	"typeid" : 5,
	"LapisId" : "28576"
},**/ 
{
	"ButtonImagePath" : "BTN_GoldSeries_EuroRoulette1",
	"DisplayName" : "欧式黄金轮盘",
	"typeid" : 5,
	"LapisId" : "28580"
}, {
	"ButtonImagePath" : "BTN_FantasticSevens1",
	"DisplayName" : "奇妙7",
	"typeid" : 2,
	"LapisId" : "28582"
}, {
	"ButtonImagePath" : "BTN_FatLadySings1",
	"DisplayName" : "胖女人辛斯",
	"typeid" : 1,
	"LapisId" : "28584"
}, {
	"ButtonImagePath" : "BTN_FishParty1",
	"DisplayName" : "派对鱼",
	"typeid" : 1,
	"LapisId" : "28586"
}, {
	"ButtonImagePath" : "BTN_FlyingAce2",
	"DisplayName" : "超级飞行员",
	"typeid" : 2,
	"LapisId" : "28588"
}, {
	"ButtonImagePath" : "BTN_FoamyFortunes1",
	"DisplayName" : "泡沫财富",
	"typeid" : 4,
	"LapisId" : "28590"
}, {
	"ButtonImagePath" : "BTN_footballstar1",
	"DisplayName" : "足球明星",
	"typeid" : 1,
	"LapisId" : "28592"
}, {
	"ButtonImagePath" : "BTN_FortuneCookie7",
	"DisplayName" : "幸运曲奇",
	"typeid" : 2,
	"LapisId" : "28594"
}, {
	"ButtonImagePath" : "BTN_FourByFour1",
	"DisplayName" : "四乘四",
	"typeid" : 7,
	"LapisId" : "28596"
}, {
	"ButtonImagePath" : "BTN_FreezingFuzzballs1",
	"DisplayName" : "冻结模糊球",
	"typeid" : 7,
	"LapisId" : "28598"
}, {
	"ButtonImagePath" : "BTN_FrenchRoulette1",
	"DisplayName" : "法式轮盘",
	"typeid" : 5,
	"LapisId" : "28602"
}, {
	"ButtonImagePath" : "BTN_FrozenDiamonds",
	"DisplayName" : "急冻钻石",
	"typeid" : 1,
	"LapisId" : "66080"
}, {
	"ButtonImagePath" : "BTN_FruitSlots1",
	"DisplayName" : "水果老虎机",
	"typeid" : 2,
	"LapisId" : "28604"
}, {
	"ButtonImagePath" : "BTN_Galacticons",
	"DisplayName" : "银河舰队",
	"typeid" : 1,
	"LapisId" : "28606"
}, {
	"ButtonImagePath" : "BTN_GameSetandScratch1",
	"DisplayName" : "网球最终局",
	"typeid" : 4,
	"LapisId" : "28616"
}, {
	"ButtonImagePath" : "BTN_GeniesGems2",
	"DisplayName" : "精灵宝石",
	"typeid" : 1,
	"LapisId" : "28608"
}, {
	"ButtonImagePath" : "BTN_Germinator1",
	"DisplayName" : "细菌对对碰",
	"typeid" : 7,
	"LapisId" : "28614"
}, {
	"ButtonImagePath" : "BTN_GiftRap2",
	"DisplayName" : "礼品包装",
	"typeid" : 1,
	"LapisId" : "28618"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns2",
	"DisplayName" : "女孩与枪II",
	"typeid" : 1,
	"LapisId" : "29449"
}, {
	"ButtonImagePath" : "BTN_GirlswithGuns1",
	"DisplayName" : "女孩与枪",
	"typeid" : 1,
	"LapisId" : "28622"
}, {
	"ButtonImagePath" : "BTN_GoldCoast3",
	"DisplayName" : "黄金海岸",
	"typeid" : 2,
	"LapisId" : "28624"
}, {
	"ButtonImagePath" : "BTN_GoldFactory",
	"DisplayName" : "黄金工厂",
	"typeid" : 1,
	"LapisId" : "28626"
}, {
	"ButtonImagePath" : "BTN_GoldenDragon6",
	"DisplayName" : "黄金龙",
	"typeid" : 2,
	"LapisId" : "28628"
}, {
	"ButtonImagePath" : "BTN_GoldenEra",
	"DisplayName" : "黄金时代",
	"typeid" : 1,
	"LapisId" : "28630"
}, {
	"ButtonImagePath" : "BTN_GoldenGhouls1",
	"DisplayName" : "黄金食尸鬼",
	"typeid" : 4,
	"LapisId" : "28632"
}, {
	"ButtonImagePath" : "BTN_GoldenPrincess",
	"DisplayName" : "黄金公主",
	"typeid" : 1,
	"LapisId" : "28634"
}, {
	"ButtonImagePath" : "BTN_GoodToGo2",
	"DisplayName" : "疯狂赛道",
	"typeid" : 1,
	"LapisId" : "28636"
}, {
	"ButtonImagePath" : "BTN_GopherGold2",
	"DisplayName" : "黄金囊地鼠",
	"typeid" : 1,
	"LapisId" : "28638"
}, {
	"ButtonImagePath" : "BTN_GrannyPrix1",
	"DisplayName" : "老太太赛车",
	"typeid" : 7,
	"LapisId" : "28640"
}, {
	"ButtonImagePath" : "BTN_GreatGriffin1",
	"DisplayName" : "大狮鹫",
	"typeid" : 1,
	"LapisId" : "28642"
}, {
	"ButtonImagePath" : "BTN_HairyFairies1",
	"DisplayName" : "毛茸茸的仙女",
	"typeid" : 7,
	"LapisId" : "28646"
}, {
	"ButtonImagePath" : "BTN_Halloweenies1",
	"DisplayName" : "万圣节",
	"typeid" : 1,
	"LapisId" : "28648"
}, {
	"ButtonImagePath" : "BTN_Halloweenies1",
	"DisplayName" : "万圣节",
	"typeid" : 7,
	"LapisId" : "28650"
}, {
	"ButtonImagePath" : "BTN_HandToHandCombat1",
	"DisplayName" : "肉搏战",
	"typeid" : 4,
	"LapisId" : "28488"
}, {
	"ButtonImagePath" : "BTN_HappyHolidays_ZH",
	"DisplayName" : "快乐假日",
	"typeid" : 1,
	"LapisId" : "45397"
}, {
	"ButtonImagePath" : "BTN_HappyNewYear3",
	"DisplayName" : "新年快乐",
	"typeid" : 2,
	"LapisId" : "28490"
}, {
	"ButtonImagePath" : "BTN_Harveys1",
	"DisplayName" : "哈维斯的晚餐",
	"typeid" : 1,
	"LapisId" : "28664"
}, {
	"ButtonImagePath" : "BTN_HellBoy1",
	"DisplayName" : "地狱男爵",
	"typeid" : 1,
	"LapisId" : "28500"
}, {
	"ButtonImagePath" : "BTN_HellGrannies",
	"DisplayName" : "地狱阿嬷",
	"typeid" : 1,
	"LapisId" : "28502"
}, {
	"ButtonImagePath" : "BTN_Hexaline1",
	"DisplayName" : "六线",
	"typeid" : 7,
	"LapisId" : "28504"
}, {
	"ButtonImagePath" : "BTN_HighLimitBaccarat1",
	"DisplayName" : "高限制百家乐",
	"typeid" : 5,
	"LapisId" : "28508"
}, {
	"ButtonImagePath" : "BTN_EuroBlackjackHighLimit1",
	"DisplayName" : "欧式高限21点",
	"typeid" : 5,
	"LapisId" : "28668"
}, {
	"ButtonImagePath" : "BTN_HighSociety",
	"DisplayName" : "上流社会",
	"typeid" : 1,
	"LapisId" : "28516"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHHighSpeedPoker1",
	"DisplayName" : "高速扑克",
	"typeid" : 5,
	"LapisId" : "28518"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_HiLo13EuroBJ1",
	"DisplayName" : "13欧洲 21点黃金桌",
	"typeid" : 5,
	"LapisId" : "28520"
}, {
	"ButtonImagePath" : "BTN_Hitman1",
	"DisplayName" : "终极杀手",
	"typeid" : 1,
	"LapisId" : "28670"
}, {
	"ButtonImagePath" : "BTN_Hohoho1",
	"DisplayName" : "嗬嗬嗬",
	"typeid" : 1,
	"LapisId" : "28674"
}, {
	"ButtonImagePath" : "BTN_HotAsHades_ZH",
	"DisplayName" : "地府烈焰",
	"typeid" : 1,
	"LapisId" : "28574"
}, {
	"ButtonImagePath" : "BTN_HotInk1",
	"DisplayName" : "神奇墨水",
	"typeid" : 1,
	"LapisId" : "28540"
}, {
	"ButtonImagePath" : "BTN_HotShot3",
	"DisplayName" : "棒球直击",
	"typeid" : 1,
	"LapisId" : "28534"
}, {
	"ButtonImagePath" : "BTN_HoundHotel_ZH",
	"DisplayName" : "酷犬酒店",
	"typeid" : 1,
	"LapisId" : "28644"
}, {
	"ButtonImagePath" : "BTN_HouseofDragons",
	"DisplayName" : "龙之家",
	"typeid" : 1,
	"LapisId" : "28680"
}, {
	"ButtonImagePath" : "BTN_ImmortalRomance1",
	"DisplayName" : "不朽的爱情",
	"typeid" : 1,
	"LapisId" : "28548"
}, {
	"ButtonImagePath" : "BTN_InstantWinCardSelector1",
	"DisplayName" : "刮刮卡20合一",
	"typeid" : 1,
	"LapisId" : "29450"
}, {
	"ButtonImagePath" : "BTN_IrishEyes",
	"DisplayName" : "爱尔兰眼睛",
	"typeid" : 1,
	"LapisId" : "28552"
}, {
	"ButtonImagePath" : "BTN_JackpotExpress1",
	"DisplayName" : "累计奖金快车",
	"typeid" : 2,
	"LapisId" : "28558"
}, {
	"ButtonImagePath" : "BTN_JacksorBetterPowerPoker1",
	"DisplayName" : "千斤顶或更好",
	"typeid" : 6,
	"LapisId" : "28686"
}, {
	"ButtonImagePath" : "BTN_JacksOrBetter1",
	"DisplayName" : "千斤顶或更好",
	"typeid" : 6,
	"LapisId" : "28694"
}, {
	"ButtonImagePath" : "BTN_JekyllandHyde",
	"DisplayName" : "判若两人",
	"typeid" : 1,
	"LapisId" : "28660"
}, {
	"ButtonImagePath" : "BTN_JewelsoftheOrient1",
	"DisplayName" : "东方珠宝",
	"typeid" : 1,
	"LapisId" : "28652"
}, {
	"ButtonImagePath" : "BTN_JingleBells6",
	"DisplayName" : "铃儿响叮当",
	"typeid" : 2,
	"LapisId" : "28654"
}, {
	"ButtonImagePath" : "BTN_JokerPokerPowerPoker1",
	"DisplayName" : "小丑扑克",
	"typeid" : 6,
	"LapisId" : "28710"
}, {
	"ButtonImagePath" : "BTN_JokerPoker1",
	"DisplayName" : "小丑扑克",
	"typeid" : 6,
	"LapisId" : "28720"
}, {
	"ButtonImagePath" : "BTN_JurassicJackpot1",
	"DisplayName" : "侏罗纪大奖",
	"typeid" : 2,
	"LapisId" : "28724"
}, {
	"ButtonImagePath" : "BTN_KaraokeParty_ZH",
	"DisplayName" : "K歌乐韵",
	"typeid" : 1,
	"LapisId" : "65978"
}, {
	"ButtonImagePath" : "BTN_KaratePig1",
	"DisplayName" : "功夫小胖猪",
	"typeid" : 1,
	"LapisId" : "28726"
}, {
	"ButtonImagePath" : "BTN_Kathmandu1",
	"DisplayName" : "卡萨缦都",
	"typeid" : 1,
	"LapisId" : "28734"
}, {
	"ButtonImagePath" : "BTN_Keno3",
	"DisplayName" : "基诺",
	"typeid" : 7,
	"LapisId" : "28740"
}, {
	"ButtonImagePath" : "BTN_KingArthur",
	"DisplayName" : "亚瑟王",
	"typeid" : 1,
	"LapisId" : "28744"
}, {
	"ButtonImagePath" : "BTN_KingsofCash1",
	"DisplayName" : "现金之王",
	"typeid" : 1,
	"LapisId" : "28762"
}, {
	"ButtonImagePath" : "BTN_KittyCabana_ZH",
	"DisplayName" : "凯蒂卡巴拉",
	"typeid" : 1,
	"LapisId" : "28772"
}, {
	"ButtonImagePath" : "BTN_LadiesNite5",
	"DisplayName" : "女仕之夜",
	"typeid" : 1,
	"LapisId" : "28776"
}, {
	"ButtonImagePath" : "BTN_LadyInRed2",
	"DisplayName" : "红衣女郎",
	"typeid" : 1,
	"LapisId" : "28812"
}, {
	"ButtonImagePath" : "BTN_LeaguesOfFortune1",
	"DisplayName" : "财富阶级",
	"typeid" : 1,
	"LapisId" : "28780"
}, {
	"ButtonImagePath" : "BTN_Legacy2",
	"DisplayName" : "遗产L",
	"typeid" : 2,
	"LapisId" : "28786"
}, {
	"ButtonImagePath" : "BTN_Loaded1",
	"DisplayName" : "炫富一族",
	"typeid" : 1,
	"LapisId" : "28790"
}, {
	"ButtonImagePath" : "BTN_LooseCannon1",
	"DisplayName" : "海盗王",
	"typeid" : 1,
	"LapisId" : "28796"
}, {
	"ButtonImagePath" : "BTN_LouisianaDouble1",
	"DisplayName" : "路易斯安那双",
	"typeid" : 6,
	"LapisId" : "28814"
}, {
	"ButtonImagePath" : "BTN_LuckyFirecracker",
	"DisplayName" : "招财鞭炮",
	"typeid" : 1,
	"LapisId" : "28800"
}, {
	"ButtonImagePath" : "BTN_LuckyKoi",
	"DisplayName" : "幸运的锦鲤",
	"typeid" : 1,
	"LapisId" : "28794"
}, {
	"ButtonImagePath" : "BTN_LuckyLeprechaun",
	"DisplayName" : "幸运的小妖精",
	"typeid" : 1,
	"LapisId" : "28788"
}, {
	"ButtonImagePath" : "BTN_LuckyLeprechaunsLoot",
	"DisplayName" : "妖精的战利品",
	"typeid" : 1,
	"LapisId" : "28782"
}, {
	"ButtonImagePath" : "BTN_LuckyNumbers1",
	"DisplayName" : "幸运数字",
	"typeid" : 4,
	"LapisId" : "28778"
}, {
	"ButtonImagePath" : "BTN_luckyTwins_ZH",
	"DisplayName" : "幸运双星",
	"typeid" : 1,
	"LapisId" : "42981"
}, {
	"ButtonImagePath" : "BTN_LuckyWitch1",
	"DisplayName" : "幸运女巫",
	"typeid" : 1,
	"LapisId" : "28774"
}, {
	"ButtonImagePath" : "BTN_LuckyZodiac_ZH",
	"DisplayName" : "幸运生肖",
	"typeid" : 1,
	"LapisId" : "28770"
}, {
	"ButtonImagePath" : "BTN_MadHatters1",
	"DisplayName" : "疯狂的帽子",
	"typeid" : 1,
	"LapisId" : "28766"
}, {
	"ButtonImagePath" : "BTN_MaxDamageSlot",
	"DisplayName" : "终极破坏",
	"typeid" : 1,
	"LapisId" : "28758"
}, {
	"ButtonImagePath" : "BTN_MaxDamage1",
	"DisplayName" : "星战传奇",
	"typeid" : 7,
	"LapisId" : "28756"
}, {
	"ButtonImagePath" : "BTN_MayanBingo1",
	"DisplayName" : "玛雅宾果",
	"typeid" : 7,
	"LapisId" : "28748"
}, {
	"ButtonImagePath" : "BTN_MayanPrincess1",
	"DisplayName" : "玛雅公主",
	"typeid" : 1,
	"LapisId" : "28738"
}, {
	"ButtonImagePath" : "BTN_BreakdaBankAgainMegaSpin1",
	"DisplayName" : "银行爆破",
	"typeid" : 1,
	"LapisId" : "28706"
}, {
	"ButtonImagePath" : "BTN_MermaidsMillions1",
	"DisplayName" : "海底世界",
	"typeid" : 1,
	"LapisId" : "28716"
}, {
	"ButtonImagePath" : "BTN_MoneyMadMonkey1",
	"DisplayName" : "疯狂的猴子",
	"typeid" : 1,
	"LapisId" : "28712"
}, {
	"ButtonImagePath" : "BTN_MonkeyKeno_ZH",
	"DisplayName" : "猴子基诺",
	"typeid" : 7,
	"LapisId" : "66084"
}, {
	"ButtonImagePath" : "BTN_MonsterMania1",
	"DisplayName" : "怪物躁狂症",
	"typeid" : 1,
	"LapisId" : "28696"
}, {
	"ButtonImagePath" : "BTN_Moonshine1",
	"DisplayName" : "月光",
	"typeid" : 1,
	"LapisId" : "28708"
}, {
	"ButtonImagePath" : "BTN_MountOlympus",
	"DisplayName" : "奥林匹斯山",
	"typeid" : 1,
	"LapisId" : "28676"
}, {
	"ButtonImagePath" : "BTN_MugshotMadness1",
	"DisplayName" : "疯狂假面",
	"typeid" : 1,
	"LapisId" : "28666"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MultiWheelRoulette1",
	"DisplayName" : "复式黄金轮盘",
	"typeid" : 5,
	"LapisId" : "28816"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHEuroBlackjack2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28820"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHPerfectPairsBlackjack1",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28822"
}, {
	"ButtonImagePath" : "BTN_MHPremierBlackjack1",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28678"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHVegasDowntown2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28826"
}, {
	"ButtonImagePath" : "BTN_MumbaiMagic1",
	"DisplayName" : "孟买魔术",
	"typeid" : 4,
	"LapisId" : "28612"
}, {
	"ButtonImagePath" : "BTN_Munchkins2",
	"DisplayName" : "怪兽曼琪肯",
	"typeid" : 1,
	"LapisId" : "28610"
}, {
	"ButtonImagePath" : "BTN_MysticDreams1",
	"DisplayName" : "神秘梦境",
	"typeid" : 1,
	"LapisId" : "28600"
}, {
	"ButtonImagePath" : "BTN_MystiqueGrove",
	"DisplayName" : "神秘森林",
	"typeid" : 1,
	"LapisId" : "28578"
}, {
	"ButtonImagePath" : "BTN_NinjaMagic_zh",
	"DisplayName" : "忍者法宝",
	"typeid" : 1,
	"LapisId" : "61149"
}, {
	"ButtonImagePath" : "BTN_Octopays",
	"DisplayName" : "章鱼",
	"typeid" : 1,
	"LapisId" : "28828"
}, {
	"ButtonImagePath" : "BTN_OffsideandSeek1",
	"DisplayName" : "临门一脚",
	"typeid" : 4,
	"LapisId" : "28658"
}, {
	"ButtonImagePath" : "BTN_OrientalFortune2",
	"DisplayName" : "东方财富",
	"typeid" : 1,
	"LapisId" : "28564"
}, {
	"ButtonImagePath" : "BTN_ParadiseFound",
	"DisplayName" : "发现天堂",
	"typeid" : 1,
	"LapisId" : "28560"
}, {
	"ButtonImagePath" : "BTN_PeekABoo_ZH",
	"DisplayName" : "躲猫猫",
	"typeid" : 1,
	"LapisId" : "42979"
}, {
	"ButtonImagePath" : "BTN_PhantomCash",
	"DisplayName" : "幻影现金",
	"typeid" : 1,
	"LapisId" : "28570"
}, {
	"ButtonImagePath" : "BTN_PharaohBingo1",
	"DisplayName" : "法老宾果",
	"typeid" : 7,
	"LapisId" : "28550"
}, {
	"ButtonImagePath" : "BTN_PharoahsGems1",
	"DisplayName" : "隔离的宝石",
	"typeid" : 4,
	"LapisId" : "28842"
}, {
	"ButtonImagePath" : "BTN_PiggyFortunes",
	"DisplayName" : "小猪财富",
	"typeid" : 1,
	"LapisId" : "28556"
}, {
	"ButtonImagePath" : "BTN_Pistoleras_ZH",
	"DisplayName" : "持枪王者",
	"typeid" : 1,
	"LapisId" : "28546"
}, {
	"ButtonImagePath" : "BTN_Playboy1",
	"DisplayName" : "花花公子",
	"typeid" : 1,
	"LapisId" : "28858"
}, {
	"ButtonImagePath" : "BTN_PlundertheSea1",
	"DisplayName" : "掠夺之海",
	"typeid" : 4,
	"LapisId" : "28544"
}, {
	"ButtonImagePath" : "BTN_PokerPursuit1",
	"DisplayName" : "扑克追求",
	"typeid" : 6,
	"LapisId" : "28874"
}, {
	"ButtonImagePath" : "BTN_PollenNation1",
	"DisplayName" : "蜜蜂乐园",
	"typeid" : 1,
	"LapisId" : "28890"
}, {
	"ButtonImagePath" : "BTN_PremierBlackjackHiLo1",
	"DisplayName" : "21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28538"
}, {
	"ButtonImagePath" : "BTN_PremierBlackjackHiStreak1",
	"DisplayName" : "21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28536"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_MHBonusBlackjack2",
	"DisplayName" : "多手21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28532"
}, {
	"ButtonImagePath" : "BTN_PremierRacing1",
	"DisplayName" : "超级赛马",
	"typeid" : 7,
	"LapisId" : "28530"
}, {
	"ButtonImagePath" : "BTN_PremierRoulette9",
	"DisplayName" : "轮盘",
	"typeid" : 5,
	"LapisId" : "28528"
}, {
	"ButtonImagePath" : "BTN_PremierRouletteDE1",
	"DisplayName" : "轮盘钻石版",
	"typeid" : 5,
	"LapisId" : "28526"
}, {
	"ButtonImagePath" : "BTN_PremierTrotting1",
	"DisplayName" : "超级马车赛",
	"typeid" : 7,
	"LapisId" : "28904"
}, {
	"ButtonImagePath" : "BTN_prettykitty_zh",
	"DisplayName" : "漂亮猫咪",
	"typeid" : 1,
	"LapisId" : "61147"
}, {
	"ButtonImagePath" : "BTN_PrimeProperty1",
	"DisplayName" : "优质物业",
	"typeid" : 1,
	"LapisId" : "28910"
}, {
	"ButtonImagePath" : "BTN_PurePlatinum1",
	"DisplayName" : "纯铂",
	"typeid" : 1,
	"LapisId" : "28512"
}, {
	"ButtonImagePath" : "BTN_RabbitintheHat",
	"DisplayName" : "帽子里的兔子",
	"typeid" : 1,
	"LapisId" : "28912"
}, {
	"ButtonImagePath" : "BTN_RacingForPinks",
	"DisplayName" : "为粉红而战",
	"typeid" : 1,
	"LapisId" : "28496"
}, {
	"ButtonImagePath" : "BTN_RammessesRiches",
	"DisplayName" : "拉美西斯的财富",
	"typeid" : 1,
	"LapisId" : "28916"
}, {
	"ButtonImagePath" : "BTN_RapidReels5",
	"DisplayName" : "急速转轮",
	"typeid" : 2,
	"LapisId" : "28482"
}, {
	"ButtonImagePath" : "BTN_RedHotDevil",
	"DisplayName" : "红唇诱惑",
	"typeid" : 1,
	"LapisId" : "28474"
}, {
	"ButtonImagePath" : "BTN_ReelGems1",
	"DisplayName" : "宝石迷阵",
	"typeid" : 1,
	"LapisId" : "28468"
}, {
	"ButtonImagePath" : "BTN_ReelSpinner_zh",
	"DisplayName" : "旋转大战",
	"typeid" : 1,
	"LapisId" : "61495"
}, {
	"ButtonImagePath" : "BTN_ReelStrike1",
	"DisplayName" : "卷行使价",
	"typeid" : 1,
	"LapisId" : "28464"
}, {
	"ButtonImagePath" : "BTN_ReelThunder2",
	"DisplayName" : "雷电击",
	"typeid" : 1,
	"LapisId" : "28462"
}, {
	"ButtonImagePath" : "BTN_RetroReels1",
	"DisplayName" : "复古旋转",
	"typeid" : 1,
	"LapisId" : "28458"
}, {
	"ButtonImagePath" : "BTN_RRExtreme1",
	"DisplayName" : "复古卷轴 - 极热",
	"typeid" : 1,
	"LapisId" : "28456"
}, {
	"ButtonImagePath" : "BTN_RRDiamondGlitz1",
	"DisplayName" : "复古卷轴钻石耀眼",
	"typeid" : 1,
	"LapisId" : "28452"
}, {
	"ButtonImagePath" : "BTN_RRHearts&Tarts1",
	"DisplayName" : "押韵的卷轴 - 心挞",
	"typeid" : 1,
	"LapisId" : "28450"
}, {
	"ButtonImagePath" : "BTN_RRKing1",
	"DisplayName" : "押韵的卷轴 - 老国王科尔",
	"typeid" : 1,
	"LapisId" : "28446"
}, {
	"ButtonImagePath" : "BTN_RivieraRiches1",
	"DisplayName" : "海滨财富",
	"typeid" : 1,
	"LapisId" : "28444"
}, {
	"ButtonImagePath" : "BTN_robojack1",
	"DisplayName" : "洛伯杰克",
	"typeid" : 1,
	"LapisId" : "28440"
}, {
	"ButtonImagePath" : "BTN_RocktheBoat18",
	"DisplayName" : "摇滚船",
	"typeid" : 2,
	"LapisId" : "28438"
}, {
	"ButtonImagePath" : "BTN_RollerDerby",
	"DisplayName" : "滚德比",
	"typeid" : 1,
	"LapisId" : "28434"
}, {
	"ButtonImagePath" : "BTN_RomanRiches11",
	"DisplayName" : "罗马财富",
	"typeid" : 2,
	"LapisId" : "28430"
}, {
	"ButtonImagePath" : "BTN_RugbyStar_ZH",
	"DisplayName" : "橄榄球明星",
	"typeid" : 1,
	"LapisId" : "28426"
}, {
	"ButtonImagePath" : "BTN_SambaBingo1",
	"DisplayName" : "萨巴宾果",
	"typeid" : 7,
	"LapisId" : "28420"
}, {
	"ButtonImagePath" : "BTN_SantasWildRide1",
	"DisplayName" : "圣诞老人的疯狂 ？",
	"typeid" : 1,
	"LapisId" : "28416"
}, {
	"ButtonImagePath" : "BTN_Scrooge1",
	"DisplayName" : "守财奴",
	"typeid" : 1,
	"LapisId" : "28412"
}, {
	"ButtonImagePath" : "BTN_SecretAdmirer1",
	"DisplayName" : "秘密崇拜者",
	"typeid" : 1,
	"LapisId" : "28410"
}, {
	"ButtonImagePath" : "BTN_SecretSanta",
	"DisplayName" : "圣诞老人的秘密",
	"typeid" : 1,
	"LapisId" : "28406"
}, {
	"ButtonImagePath" : "BTN_Serenity_ZH",
	"DisplayName" : "宁静",
	"typeid" : 1,
	"LapisId" : "45399"
}, {
	"ButtonImagePath" : "BTN_Shoot",
	"DisplayName" : "射击",
	"typeid" : 1,
	"LapisId" : "28402"
}, {
	"ButtonImagePath" : "BTN_SilverFang1",
	"DisplayName" : "银芳",
	"typeid" : 1,
	"LapisId" : "28396"
}, {
	"ButtonImagePath" : "BTN_SixShooterLooterGold1",
	"DisplayName" : "六位枪手掠夺者黄金版",
	"typeid" : 4,
	"LapisId" : "28394"
}, {
	"ButtonImagePath" : "BTN_SkullDuggery1",
	"DisplayName" : "骷髅陷阱",
	"typeid" : 1,
	"LapisId" : "28392"
}, {
	"ButtonImagePath" : "BTN_SlamFunk1",
	"DisplayName" : "猛撞恐惧",
	"typeid" : 4,
	"LapisId" : "28390"
}, {
	"ButtonImagePath" : "BTN_somanymonsters",
	"DisplayName" : "怪兽多多",
	"typeid" : 1,
	"LapisId" : "28388"
}, {
	"ButtonImagePath" : "BTN_somuchcandy",
	"DisplayName" : "糖果多多",
	"typeid" : 1,
	"LapisId" : "28386"
}, {
	"ButtonImagePath" : "BTN_somuchsushi",
	"DisplayName" : "寿司多多",
	"typeid" : 1,
	"LapisId" : "28382"
}, {
	"ButtonImagePath" : "BTN_SoccerSafari1",
	"DisplayName" : "动物足球",
	"typeid" : 1,
	"LapisId" : "28378"
}, {
	"ButtonImagePath" : "BTN_SpaceEvader1",
	"DisplayName" : "太空逃避物",
	"typeid" : 4,
	"LapisId" : "28370"
}, {
	"ButtonImagePath" : "BTN_SpaceEvaderGold1",
	"DisplayName" : "太空逃避物黄金版",
	"typeid" : 4,
	"LapisId" : "28366"
}, {
	"ButtonImagePath" : "BTN_SpanishBlackjack1",
	"DisplayName" : "西班牙21点",
	"typeid" : 5,
	"LapisId" : "28362"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_SpanishBlackjack1",
	"DisplayName" : "西班牙21点",
	"typeid" : 5,
	"LapisId" : "28360"
}, {
	"ButtonImagePath" : "BTN_SpectacularWheelOfWealth1",
	"DisplayName" : "财富之轮",
	"typeid" : 2,
	"LapisId" : "28356"
}, {
	"ButtonImagePath" : "BTN_Spingo",
	"DisplayName" : "我推",
	"typeid" : 7,
	"LapisId" : "28352"
}, {
	"ButtonImagePath" : "BTN_SpringBreak2",
	"DisplayName" : "春假",
	"typeid" : 1,
	"LapisId" : "28348"
}, {
	"ButtonImagePath" : "BTN_StarDust_ZH",
	"DisplayName" : "星尘",
	"typeid" : 1,
	"LapisId" : "45401"
}, {
	"ButtonImagePath" : "BTN_StarlightKiss",
	"DisplayName" : "星梦之吻",
	"typeid" : 1,
	"LapisId" : "28344"
}, {
	"ButtonImagePath" : "BTN_StarScape1",
	"DisplayName" : "星云",
	"typeid" : 1,
	"LapisId" : "28342"
}, {
	"ButtonImagePath" : "BTN_StashoftheTitans1",
	"DisplayName" : "泰坦之藏匿",
	"typeid" : 1,
	"LapisId" : "28340"
}, {
	"ButtonImagePath" : "BTN_SteamPunkHeroes",
	"DisplayName" : "蒸汽朋克英雄",
	"typeid" : 1,
	"LapisId" : "28338"
}, {
	"ButtonImagePath" : "BTN_SterlingSilver3D1",
	"DisplayName" : "纯银3D",
	"typeid" : 1,
	"LapisId" : "28334"
}, {
	"ButtonImagePath" : "BTN_SunQuest3",
	"DisplayName" : "探索太阳",
	"typeid" : 1,
	"LapisId" : "28328"
}, {
	"ButtonImagePath" : "BTN_SunTide_Button_ZH",
	"DisplayName" : "太阳征程",
	"typeid" : 1,
	"LapisId" : "50193"
}, {
	"ButtonImagePath" : "BTN_SupeItUp2",
	"DisplayName" : "跑起来",
	"typeid" : 1,
	"LapisId" : "28326"
}, {
	"ButtonImagePath" : "BTN_SuperBonusBingo1",
	"DisplayName" : "超级奖金宾果",
	"typeid" : 7,
	"LapisId" : "28322"
}, {
	"ButtonImagePath" : "BTN_SuperFun21Blackjack1",
	"DisplayName" : "超级有趣21",
	"typeid" : 5,
	"LapisId" : "28320"
}, {
	"ButtonImagePath" : "BTN_SuperZeroes1",
	"DisplayName" : "超级零点",
	"typeid" : 7,
	"LapisId" : "28318"
}, {
	"ButtonImagePath" : "BTN_SurfSafari",
	"DisplayName" : "动物冲浪",
	"typeid" : 1,
	"LapisId" : "28314"
}, {
	"ButtonImagePath" : "BTN_SweetHarvest1",
	"DisplayName" : "甜蜜的收获",
	"typeid" : 1,
	"LapisId" : "28312"
}, {
	"ButtonImagePath" : "BTN_TallyHo1",
	"DisplayName" : "泰利嗬",
	"typeid" : 1,
	"LapisId" : "28308"
}, {
	"ButtonImagePath" : "BTN_TensOrBetter1",
	"DisplayName" : "数万或更好",
	"typeid" : 6,
	"LapisId" : "28302"
}, {
	"ButtonImagePath" : "BTN_TensOrBetterPowerPoker1",
	"DisplayName" : "数万或更好",
	"typeid" : 6,
	"LapisId" : "28304"
}, {
	"ButtonImagePath" : "BTN_Terminator2_1",
	"DisplayName" : "终结者2",
	"typeid" : 1,
	"LapisId" : "28300"
}, {
	"ButtonImagePath" : "BTN_FinerReelsofLife",
	"DisplayName" : "好日子",
	"typeid" : 1,
	"LapisId" : "28294"
}, {
	"ButtonImagePath" : "BTN_TheLostPrincessAnastasia",
	"DisplayName" : "失落的阿纳斯塔西娅公主",
	"typeid" : 1,
	"LapisId" : "28246"
}, {
	"ButtonImagePath" : "BTN_RatPack1",
	"DisplayName" : "鼠包",
	"typeid" : 1,
	"LapisId" : "28238"
}, {
	"ButtonImagePath" : "BTN_TwistedCircus",
	"DisplayName" : "反转马戏团",
	"typeid" : 1,
	"LapisId" : "28232"
}, {
	"ButtonImagePath" : "BTN_ThroneOfEgypt1",
	"DisplayName" : "埃及王座",
	"typeid" : 1,
	"LapisId" : "28230"
}, {
	"ButtonImagePath" : "BTN_Thunderstruck1",
	"DisplayName" : "雷神",
	"typeid" : 1,
	"LapisId" : "28228"
}, {
	"ButtonImagePath" : "BTN_ThunderstruckTwo1",
	"DisplayName" : "雷神2",
	"typeid" : 1,
	"LapisId" : "28226"
}, {
	"ButtonImagePath" : "BTN_TigerVsBear",
	"DisplayName" : "熊虎之战",
	"typeid" : 1,
	"LapisId" : "28222"
}, {
	"ButtonImagePath" : "BTN_TitansOfTheSun_Hyperion_ZH",
	"DisplayName" : "太阳神之许珀里翁",
	"typeid" : 1,
	"LapisId" : "28112"
}, {
	"ButtonImagePath" : "BTN_TitansOfTheSun_Theia_ZH",
	"DisplayName" : "太阳神之忒伊亚",
	"typeid" : 1,
	"LapisId" : "28220"
}, {
	"ButtonImagePath" : "BTN_TombRaider2",
	"DisplayName" : "古墓丽影",
	"typeid" : 1,
	"LapisId" : "28218"
}, {
	"ButtonImagePath" : "BTN_TombRaiderSotS1",
	"DisplayName" : "古墓丽影2",
	"typeid" : 1,
	"LapisId" : "28216"
}, {
	"ButtonImagePath" : "BTN_TotemTreasure1",
	"DisplayName" : "图腾宝藏",
	"typeid" : 1,
	"LapisId" : "28214"
}, {
	"ButtonImagePath" : "BTN_Triangulation",
	"DisplayName" : "三角",
	"typeid" : 7,
	"LapisId" : "28212"
}, {
	"ButtonImagePath" : "BTN_TripleMagic1",
	"DisplayName" : "三魔法",
	"typeid" : 2,
	"LapisId" : "28210"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_TriplePocket1",
	"DisplayName" : "三人德州扑克",
	"typeid" : 5,
	"LapisId" : "28208"
}, {
	"ButtonImagePath" : "BTN_TurtleyAwesome1",
	"DisplayName" : "棒棒乌龟",
	"typeid" : 4,
	"LapisId" : "28206"
}, {
	"ButtonImagePath" : "BTN_UntamedBengalTiger1",
	"DisplayName" : "野性的孟加拉虎",
	"typeid" : 1,
	"LapisId" : "28202"
}, {
	"ButtonImagePath" : "BTN_UntamedCrownedEagle",
	"DisplayName" : "狂野之鹰",
	"typeid" : 1,
	"LapisId" : "28200"
}, {
	"ButtonImagePath" : "BTN_UntamedGiantPanda",
	"DisplayName" : "大熊猫",
	"typeid" : 1,
	"LapisId" : "28196"
}, {
	"ButtonImagePath" : "BTN_UntamedWolfPack1",
	"DisplayName" : "野性的狼群",
	"typeid" : 1,
	"LapisId" : "28190"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_VegasSingleDeck1",
	"DisplayName" : "拉斯维加斯21点单人黄金桌",
	"typeid" : 5,
	"LapisId" : "28186"
}, {
	"ButtonImagePath" : "BTN_VegasStripBlackjack1",
	"DisplayName" : "拉斯维加斯21点",
	"typeid" : 5,
	"LapisId" : "28180"
}, {
	"ButtonImagePath" : "BTN_GoldSeries_VegasStrip1",
	"DisplayName" : "拉斯维加斯21点黄金桌",
	"typeid" : 5,
	"LapisId" : "28184"
}, {
	"ButtonImagePath" : "BTN_VictorianVillian",
	"DisplayName" : "维多利亚的恶棍",
	"typeid" : 1,
	"LapisId" : "28176"
}, {
	"ButtonImagePath" : "BTN_VinylCountdown4",
	"DisplayName" : "乙烯基倒计时",
	"typeid" : 1,
	"LapisId" : "28172"
}, {
	"ButtonImagePath" : "BTN_WasabiSan4",
	"DisplayName" : "芥末寿司",
	"typeid" : 1,
	"LapisId" : "28168"
}, {
	"ButtonImagePath" : "BTN_WhackaJackpot1",
	"DisplayName" : "瓜分大奖",
	"typeid" : 4,
	"LapisId" : "28158"
}, {
	"ButtonImagePath" : "BTN_WhataHoot3",
	"DisplayName" : "猫头鹰乐园",
	"typeid" : 1,
	"LapisId" : "28178"
}, {
	"ButtonImagePath" : "BTN_WhatonEarth1",
	"DisplayName" : "地球生物",
	"typeid" : 1,
	"LapisId" : "28154"
}, {
	"ButtonImagePath" : "BTN_WheelOfWealth1",
	"DisplayName" : "水果财富",
	"typeid" : 2,
	"LapisId" : "28156"
}, {
	"ButtonImagePath" : "BTN_WheelofWealthSE1",
	"DisplayName" : "财富转轮特别版",
	"typeid" : 1,
	"LapisId" : "28146"
}, {
	"ButtonImagePath" : "BTN_WhiteBuffalo",
	"DisplayName" : "白水牛",
	"typeid" : 1,
	"LapisId" : "28140"
}, {
	"ButtonImagePath" : "BTN_WildCatch",
	"DisplayName" : "野生捕鱼",
	"typeid" : 1,
	"LapisId" : "28142"
}, {
	"ButtonImagePath" : "BTN_WildChampions1",
	"DisplayName" : "野生冠军",
	"typeid" : 4,
	"LapisId" : "28132"
}, {
	"ButtonImagePath" : "BTN_WildOrient_Button_ZH",
	"DisplayName" : "东方珍兽",
	"typeid" : 1,
	"LapisId" : "50194"
}, {
	"ButtonImagePath" : "BTN_WinSpinner1",
	"DisplayName" : "钱来运转",
	"typeid" : 2,
	"LapisId" : "29573"
}, {
	"ButtonImagePath" : "BTN_winsumdimsum_zh",
	"DisplayName" : "开心点心",
	"typeid" : 1,
	"LapisId" : "61499"
}, {
	"ButtonImagePath" : "BTN_WinningWizards1",
	"DisplayName" : "赢得向导",
	"typeid" : 1,
	"LapisId" : "28130"
}, {
	"ButtonImagePath" : "BTN_WitchesWealth1",
	"DisplayName" : "女巫的财富",
	"typeid" : 1,
	"LapisId" : "28126"
}, {
	"ButtonImagePath" : "BTN_ZanyZebra1",
	"DisplayName" : "燃尼巨蟒",
	"typeid" : 2,
	"LapisId" : "28124"
} ];
var hotdata = data;

$(function(){
	getdata(0);
	
})

function search(keywords) {
    $("#gamelist").html("");
    var game = "";
    var mgame = "";
    var userid = $("#userid").val();
    var count = 0;
    var image = "";
    $.each(data, function (index, content) {
    	if(!content)return;
        var key = content.DisplayName;
        if (key.indexOf(keywords) + 1 > 0) {
        	if(content.ButtonImagePath && content.ButtonImagePath.indexOf(".") != -1){
        		image = "v2/"+content.ButtonImagePath;
        	}else{
        		image = "v2/"+content.ButtonImagePath + ".png";
        	}
        	
            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName + "</div><div class=\"game_logo\" onclick=\"toMG('" + content.LapisId + "')\"><img src=\"common/template/third/egame/images/" +image+"\"></div></li>";
            count++;
        } else {
            game += "没有搜索到相关游戏";
        }
    });
    $("#gamelist").html(game);
    var allpage = Math.ceil(count / 15);
    gamelist(allpage, game);
}

function getdata(type) {
    $("#gamelist").html("");
	var game = "";
	var mgame = "";
	var userid = $("#userid").val();
	var count = 0;
	var image = "";
	var database = [];
	if (type == 8) {
	    database = hotdata;
	    type = "0";
	} else {
	    database = data;
	}
	$.each(database, function (index, content){
		if(!content)return;
	    var gametype = content.typeid;
	    if(content.ButtonImagePath && content.ButtonImagePath.indexOf(".")!= -1){
    		image = "v2/"+content.ButtonImagePath;
    	}else{
    		image = "v2/"+content.ButtonImagePath + ".png";
    	}
	    if (type == "123") {
	        if (gametype == "1" || gametype == "2" || gametype == "3") {
	            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName + "</div><div class=\"game_logo\" onclick=\"toMG('" + content.LapisId + "')\"><img src=\"common/template/third/egame/images/" + image + "\"></div></li>";
	            count++;
	        }
	    }
	    if (type == gametype || type == '0') {
		    game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">"+content.DisplayName+"</div><div class=\"game_logo\" onclick=\"toMG('"+content.LapisId+"')\"><img src=\"common/template/third/egame/images/" + image + "\"></div></li>";
		    count++;
	    }
	});
	$("#gamelist").html(game);
	var allpage = Math.ceil(count / 15);
	gamelist(allpage, game);
}
function gamelist(allpage, game) {
    $("#gamelists").html("");
    for (var i = 0; i < allpage; i++) {
        var id = "gamepage" + i;
        $("#gamelists").append("<ul id='" + id + "' class='gamelist' style='display:none'></ul>");
    }
    $("#gamelist li").each(function (i) {
        var id = "gamepage" + parseInt(i / 15);
        $(this).appendTo("#" + id);
    });
    $("#gamelist").html("");
    $("#pageCount").html(allpage);
    $("#StartPage").click(function () {
        topage(0, allpage);
    });
    $("#EndPage").click(function () {
        topage(allpage-1, allpage);
    });
    var curi = 0;
    topage(curi, allpage);
    $("#NextPage").bind("click", function () {
        curi++;
        if (curi >= allpage) {
            curi = eval(allpage-1);
        }
        topage(curi, allpage);
    });
    $("#PrevPage").bind("click", function () {
        curi--;
        if (curi <= 0) {
            curi = 0;
        }
        topage(curi, allpage);
    });
    
}
function topage(pageid,totalpage){
    $("#gamelists ul").hide();
    $("#gamepage" + pageid).show();
    var curpage = pageid + 1;
    $("#currentPage").html(curpage);
    
}
function prepage(pageid,totalpage){
    topage(pageid, totalpage);
}
function nextpage(pageid,totalpage){
    topage(pageid,totalpage);
}

(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					alert("[" + opt.url + "] 404 not found");
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					alert("没有权限");
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
})(jQuery);

function toMG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;

    var userid = $("#userid").val();
    if (!userid) {
        alert("您需要先登录才能游戏");
    }else{
    	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
    	$.ajax({
			url : "../forwardMg.do",
			sync : false,
			data:{
				gameType : 3,
				gameid : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
    }
}

function toAG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;

    var userid = $("#userid").val();
    if (userid =="") {
        alert("您需要先登录才能游戏");
    }else{
    	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
    	$.ajax({
			url : "../forwardAg.do",
			sync : false,
			data:{
				h5 : 0,
				gameType : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
    }
}