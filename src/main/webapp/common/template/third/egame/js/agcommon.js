var data = [
    { "DisplayName": "捕鱼王者", "ButtonImagePath": "SB36.gif", "LapisId": "6" ,"typeid":"3"},
    { "DisplayName": "水果拉霸", "ButtonImagePath": "FRU.jpg", "LapisId": "501" ,"typeid":"3"},
    { "DisplayName": "杰克高手", "ButtonImagePath": "PKBJ.jpg", "LapisId": "502" ,"typeid":"3"},
    { "DisplayName": "极速幸运轮", "ButtonImagePath": "TGLW.jpg", "LapisId": "507","typeid":"2"},
    { "DisplayName": "太空漫游", "ButtonImagePath": "SB01.jpg", "LapisId": "508" ,"typeid":"3"},
    { "DisplayName": "复古花园", "ButtonImagePath": "SB02.jpg", "LapisId": "509" ,"typeid":"3"},
    { "DisplayName": "关东煮", "ButtonImagePath": "SB03.jpg", "LapisId": "510" ,"typeid":"4"},
    { "DisplayName": "牧场咖啡", "ButtonImagePath": "SB04.jpg", "LapisId": "511" ,"typeid":"4"},
    { "DisplayName": "甜一甜屋", "ButtonImagePath": "SB05.jpg", "LapisId": "512" ,"typeid":"4"},
    { "DisplayName": "日本武士", "ButtonImagePath": "SB06.jpg", "LapisId": "513" ,"typeid":"4"},
    { "DisplayName": "象棋老虎机", "ButtonImagePath": "SB07.jpg", "LapisId": "514" ,"typeid":"4"},
    { "DisplayName": "麻将老虎机", "ButtonImagePath": "SB08.jpg", "LapisId": "515" ,"typeid":"4"},
    { "DisplayName": "西洋棋老虎机", "ButtonImagePath": "SB09.jpg", "LapisId": "516" ,"typeid":"4"},
    { "DisplayName": "开心农场", "ButtonImagePath": "SB10.jpg", "LapisId": "517" ,"typeid":"4"},
    { "DisplayName": "夏日营地", "ButtonImagePath": "SB11.jpg", "LapisId": "518" ,"typeid":"4"},
    { "DisplayName": "海底漫游", "ButtonImagePath": "SB12.jpg", "LapisId": "519" ,"typeid":"4"},
    { "DisplayName": "鬼马小丑", "ButtonImagePath": "SB13.jpg", "LapisId": "520" ,"typeid":"4"},
    { "DisplayName": "机动乐园", "ButtonImagePath": "SB14.jpg", "LapisId": "521" ,"typeid":"4"},
    { "DisplayName": "惊吓鬼屋", "ButtonImagePath": "SB15.jpg", "LapisId": "522" ,"typeid":"4"},
    { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" ,"typeid":"4"},
    { "DisplayName": "海洋剧场", "ButtonImagePath": "SB17.jpg", "LapisId": "524" ,"typeid":"4"},
    { "DisplayName": "水上乐园", "ButtonImagePath": "SB18.jpg", "LapisId": "525" ,"typeid":"4"},
    { "DisplayName": "空中战争", "ButtonImagePath": "SB19.jpg", "LapisId": "526" ,"typeid":"4"},
    { "DisplayName": "摇滚狂迷", "ButtonImagePath": "SB20.jpg", "LapisId": "527" ,"typeid":"4"},
    { "DisplayName": "越野机车", "ButtonImagePath": "SB21.jpg", "LapisId": "528" ,"typeid":"4"},
    { "DisplayName": "埃及奥秘", "ButtonImagePath": "SB22.jpg", "LapisId": "529" ,"typeid":"4"},
    { "DisplayName": "欢乐时光", "ButtonImagePath": "SB23.png", "LapisId": "530" ,"typeid":"4"},
    { "DisplayName": "侏罗纪", "ButtonImagePath": "SB24.jpg", "LapisId": "531" ,"typeid":"4"},
    { "DisplayName": "土地神", "ButtonImagePath": "SB25.jpg", "LapisId": "532" ,"typeid":"4"},
    { "DisplayName": "布袋和尚", "ButtonImagePath": "SB26.jpg", "LapisId": "533" ,"typeid":"4"},
    { "DisplayName": "正财神", "ButtonImagePath": "SB27.jpg", "LapisId": "534" ,"typeid":"4"},
    { "DisplayName": "武财神", "ButtonImagePath": "SB28.jpg", "LapisId": "535" ,"typeid":"4"},
    { "DisplayName": "偏财神", "ButtonImagePath": "SB29.jpg", "LapisId": "536" ,"typeid":"4"},
    { "DisplayName": "天空守护者", "ButtonImagePath": "SB31.jpg", "LapisId": "542" ,"typeid":"3"},
    { "DisplayName": "齐天大圣", "ButtonImagePath": "SB32.gif", "LapisId": "543" ,"typeid":"2"},
    { "DisplayName": "糖果碰碰乐", "ButtonImagePath": "SB33.gif", "LapisId": "544" ,"typeid":"2"},
    { "DisplayName": "冰河世界", "ButtonImagePath": "SB34.jpg", "LapisId": "545" ,"typeid":"4"},
    { "DisplayName": "欧洲列强争霸", "ButtonImagePath": "SB35.gif", "LapisId": "547" ,"typeid":"4"},
    { "DisplayName": "上海百乐门", "ButtonImagePath": "SB37.jpg", "LapisId": "549" ,"typeid":"2"},
    { "DisplayName": "性感女僕", "ButtonImagePath": "AV01.jpg", "LapisId": "537" ,"typeid":"3"},
    { "DisplayName": "百搭二王", "ButtonImagePath": "PKBD.gif", "LapisId": "540" ,"typeid":"2"},
    { "DisplayName": "红利百搭", "ButtonImagePath": "PKBB.gif", "LapisId": "541" ,"typeid":"2"},
    { "DisplayName": "水果拉霸2", "ButtonImagePath": "FRU2.gif", "LapisId": "546" ,"typeid":"2"},
    { "DisplayName": "龙珠", "ButtonImagePath": "XG01.jpg", "LapisId": "200" ,"typeid":"4"},
    { "DisplayName": "幸运8", "ButtonImagePath": "XG02.jpg", "LapisId": "201" ,"typeid":"4"},
    { "DisplayName": "闪亮女郎", "ButtonImagePath": "XG03.jpg", "LapisId": "202" ,"typeid":"2"},
    { "DisplayName": "金鱼", "ButtonImagePath": "XG04.jpg", "LapisId": "203" ,"typeid":"2"},
    { "DisplayName": "中国新年", "ButtonImagePath": "XG05.jpg", "LapisId": "204" ,"typeid":"2"},
    { "DisplayName": "海盗王", "ButtonImagePath": "XG06.jpg", "LapisId": "205" ,"typeid":"4"},
    { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" ,"typeid":"4"},
    { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" ,"typeid":"4"}
];
var hotdata = [
               { "DisplayName": "捕鱼王者", "ButtonImagePath": "SB36.gif", "LapisId": "6" },
               { "DisplayName": "水果拉霸", "ButtonImagePath": "FRU.jpg", "LapisId": "501" },
               { "DisplayName": "杰克高手", "ButtonImagePath": "PKBJ.jpg", "LapisId": "502" },
               { "DisplayName": "极速幸运轮", "ButtonImagePath": "TGLW.jpg", "LapisId": "507"},
               { "DisplayName": "太空漫游", "ButtonImagePath": "SB01.jpg", "LapisId": "508" },
               { "DisplayName": "复古花园", "ButtonImagePath": "SB02.jpg", "LapisId": "509" },
               { "DisplayName": "关东煮", "ButtonImagePath": "SB03.jpg", "LapisId": "510" },
               { "DisplayName": "牧场咖啡", "ButtonImagePath": "SB04.jpg", "LapisId": "511" },
               { "DisplayName": "甜一甜屋", "ButtonImagePath": "SB05.jpg", "LapisId": "512" },
               { "DisplayName": "日本武士", "ButtonImagePath": "SB06.jpg", "LapisId": "513" },
               { "DisplayName": "象棋老虎机", "ButtonImagePath": "SB07.jpg", "LapisId": "514" },
               { "DisplayName": "麻将老虎机", "ButtonImagePath": "SB08.jpg", "LapisId": "515" },
               { "DisplayName": "西洋棋老虎机", "ButtonImagePath": "SB09.jpg", "LapisId": "516" },
               { "DisplayName": "开心农场", "ButtonImagePath": "SB10.jpg", "LapisId": "517" },
               { "DisplayName": "夏日营地", "ButtonImagePath": "SB11.jpg", "LapisId": "518" },
               { "DisplayName": "海底漫游", "ButtonImagePath": "SB12.jpg", "LapisId": "519" },
               { "DisplayName": "鬼马小丑", "ButtonImagePath": "SB13.jpg", "LapisId": "520" },
               { "DisplayName": "机动乐园", "ButtonImagePath": "SB14.jpg", "LapisId": "521" },
               { "DisplayName": "惊吓鬼屋", "ButtonImagePath": "SB15.jpg", "LapisId": "522" },
               { "DisplayName": "疯狂马戏团", "ButtonImagePath": "SB16.jpg", "LapisId": "523" },
               { "DisplayName": "海洋剧场", "ButtonImagePath": "SB17.jpg", "LapisId": "524" },
               { "DisplayName": "水上乐园", "ButtonImagePath": "SB18.jpg", "LapisId": "525" },
               { "DisplayName": "空中战争", "ButtonImagePath": "SB19.jpg", "LapisId": "526" },
               { "DisplayName": "摇滚狂迷", "ButtonImagePath": "SB20.jpg", "LapisId": "527" },
               { "DisplayName": "越野机车", "ButtonImagePath": "SB21.jpg", "LapisId": "528" },
               { "DisplayName": "埃及奥秘", "ButtonImagePath": "SB22.jpg", "LapisId": "529" },
               { "DisplayName": "欢乐时光", "ButtonImagePath": "SB23.png", "LapisId": "530" },
               { "DisplayName": "侏罗纪", "ButtonImagePath": "SB24.jpg", "LapisId": "531" },
               { "DisplayName": "土地神", "ButtonImagePath": "SB25.jpg", "LapisId": "532" },
               { "DisplayName": "布袋和尚", "ButtonImagePath": "SB26.jpg", "LapisId": "533" },
               { "DisplayName": "正财神", "ButtonImagePath": "SB27.jpg", "LapisId": "534" },
               { "DisplayName": "武财神", "ButtonImagePath": "SB28.jpg", "LapisId": "535" },
               { "DisplayName": "偏财神", "ButtonImagePath": "SB29.jpg", "LapisId": "536" },
               { "DisplayName": "天空守护者", "ButtonImagePath": "SB31.jpg", "LapisId": "542" },
               { "DisplayName": "齐天大圣", "ButtonImagePath": "SB32.gif", "LapisId": "543" },
               { "DisplayName": "糖果碰碰乐", "ButtonImagePath": "SB33.gif", "LapisId": "544" },
               { "DisplayName": "冰河世界", "ButtonImagePath": "SB34.jpg", "LapisId": "545" },
               { "DisplayName": "欧洲列强争霸", "ButtonImagePath": "SB35.gif", "LapisId": "547" },
               { "DisplayName": "上海百乐门", "ButtonImagePath": "SB37.jpg", "LapisId": "549" },
               { "DisplayName": "性感女僕", "ButtonImagePath": "AV01.jpg", "LapisId": "537" },
               { "DisplayName": "百搭二王", "ButtonImagePath": "PKBD.gif", "LapisId": "540" },
               { "DisplayName": "红利百搭", "ButtonImagePath": "PKBB.gif", "LapisId": "541" },
               { "DisplayName": "水果拉霸2", "ButtonImagePath": "FRU2.gif", "LapisId": "546" },
               { "DisplayName": "龙珠", "ButtonImagePath": "XG01.jpg", "LapisId": "200" },
               { "DisplayName": "幸运8", "ButtonImagePath": "XG02.jpg", "LapisId": "201" },
               { "DisplayName": "闪亮女郎", "ButtonImagePath": "XG03.jpg", "LapisId": "202" },
               { "DisplayName": "金鱼", "ButtonImagePath": "XG04.jpg", "LapisId": "203" },
               { "DisplayName": "中国新年", "ButtonImagePath": "XG05.jpg", "LapisId": "204" },
               { "DisplayName": "海盗王", "ButtonImagePath": "XG06.jpg", "LapisId": "205" },
               { "DisplayName": "鲜果狂热", "ButtonImagePath": "XG07.jpg", "LapisId": "206" },
               { "DisplayName": "小熊猫", "ButtonImagePath": "XG08.jpg", "LapisId": "207" }
];

$(function(){
	getdata(0);
	
})

function search(keywords) {
    $("#gamelist").html("");
    var game = "";
    var mgame = "";
    var userid = $("#userid").val();
    var count = 0;
    $.each(data, function (index, content) {
        var key = content.DisplayName;
        if (key.indexOf(keywords) + 1 > 0) {
            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName + "</div><div style=\"cursor: pointer;\" onclick=\"toAg('" + content.LapisId + "')\"><img  width=\"126\" height=\"148\" src=\"common/template/third/egame/images/" + content.ButtonImagePath + "\"></div></li>";
            count++;
        } else {
            game += "没有搜索到相关游戏";
        }
    });
    $("#gamelist").html(game);
    var allpage = Math.ceil(count / 15);
    gamelist(allpage, game);
}

function getdata(type) {
    $("#gamelist").html("");
	var game = "";
	var mgame = "";
	var userid = $("#userid").val();
	var count = 0;
	var database = [];
	if (type == 8) {
	    database = hotdata;
	    type = "0";
	} else {
	    database = data;
	}
	$.each(database, function (index, content)
	{
	    var gametype = content.typeid;
	    if (type == "123") {
	        if (gametype == "1" || gametype == "2" || gametype == "3") {
	        	alert('dsa');
	            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName 
	            + "</div><div style=\"cursor: pointer;\" onclick=\"toAg('" 
	            + content.LapisId + "')\"><img width=\"126\" height=\"148\" src=\"common/template/third/egame/images/" + content.ButtonImagePath + "\"></div></li>";
	            count++;
	        }
	    }
	    if (type == gametype || type == '0' || gametype.indexOf(type) + 1 > 0) {
		    game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">"
		    	+content.DisplayName+"</div><div style=\"cursor: pointer;\" onclick=\"toAg('"
		    	+content.LapisId+"')\"><img  width=\"126\" height=\"148\" src=\"common/template/third/egame/images/"+content.ButtonImagePath+"\"></div></li>";
		    count++;
	    }
	});
	$("#gamelist").html(game);
	var allpage = Math.ceil(count / 15);
	gamelist(allpage, game);
}
function gamelist(allpage, game) {
    $("#gamelists").html("");
    for (var i = 0; i < allpage; i++) {
        var id = "gamepage" + i;
        $("#gamelists").append("<ul id='" + id + "' class='gamelist' style='display:none'></ul>");
    }
    $("#gamelist li").each(function (i) {
        var id = "gamepage" + parseInt(i / 15);
        $(this).appendTo("#" + id);
    });
    $("#gamelist").html("");
    $("#pageCount").html(allpage);
    $("#StartPage").click(function () {
        topage(0, allpage);
    });
    $("#EndPage").click(function () {
        topage(allpage-1, allpage);
    });
    var curi = 0;
    topage(curi, allpage);
    $("#NextPage").bind("click", function () {
        curi++;
        if (curi >= allpage) {
            curi = eval(allpage-1);
        }
        topage(curi, allpage);
    });
    $("#PrevPage").bind("click", function () {
        curi--;
        if (curi <= 0) {
            curi = 0;
        }
        topage(curi, allpage);
    });
    
}
function topage(pageid,totalpage){
    $("#gamelists ul").hide();
    $("#gamepage" + pageid).show();
    var curpage = pageid + 1;
    $("#currentPage").html(curpage);
    
}
function prepage(pageid,totalpage){
    topage(pageid, totalpage);
}
function nextpage(pageid,totalpage){
    topage(pageid,totalpage);
}
function toAg(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;

    var userid = $("#userid").val();
    if (userid =="") {
        alert("您需要先登录才能游戏");
    }else{
    	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
    	$.ajax({
			url : base+"/forwardAg.do",
			sync : false,
			data:{
				h5 : 0,
				gameType : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
    }
}