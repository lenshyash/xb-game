var data = [
    { "DisplayName": "冒险宫", "ButtonImagePath": "BTN_AdventurePalaceHD", "LapisId": "AdventurePalace" ,"typeid":"2"},
    { "DisplayName": "厨神", "ButtonImagePath": "BTN_BigChef_ZH", "LapisId": "BigChef" ,"typeid":"2"},
    { "DisplayName": "爱丽娜", "ButtonImagePath": "BTN_Ariana_ZH", "LapisId": "Ariana","typeid":"2"},
    { "DisplayName": "幸运小妖精", "ButtonImagePath": "BTN_LuckyLeprechaun", "LapisId": "LuckyLeprechaun" ,"typeid":"2"},
    { "DisplayName": "阿瓦隆", "ButtonImagePath": "BTN_AvalonHD_ZH", "LapisId": "Avalon" ,"typeid":"2"},
    { "DisplayName": "金公主", "ButtonImagePath": "BTN_GoldenPrincess", "LapisId": "GoldenPrincess","typeid":"2"},
    { "DisplayName": "兔子帽子", "ButtonImagePath": "BTN_RabbitintheHat", "LapisId": "RabbitInTheHat" ,"typeid":"2"},
    { "DisplayName": "持枪王者", "ButtonImagePath": "BTN_Pistoleras_ZH", "LapisId": "Pistoleras" ,"typeid":"2"},
    { "DisplayName": "凯蒂卡巴拉", "ButtonImagePath": "BTN_KittyCabana_ZH", "LapisId": "KittyCabana" ,"typeid":"2"},
    { "DisplayName": "酷犬酒店", "ButtonImagePath": "BTN_HoundHotel_ZH", "LapisId": "HoundHotel" ,"typeid":"2"},
    { "DisplayName": "锁子甲", "ButtonImagePath": "BTN_ChainMail_ZH", "LapisId": "ChainMailNew" ,"typeid":"2"},
    { "DisplayName": "地府烈焰", "ButtonImagePath": "BTN_HotAsHades_ZH", "LapisId": "HotAsHades" ,"typeid":"2"},
    { "DisplayName": "招财鞭炮", "ButtonImagePath": "BTN_LuckyFirecracker", "LapisId": "LuckyFirecracker" ,"typeid":"2"},
    { "DisplayName": "黄金时代", "ButtonImagePath": "BTN_GoldenEra", "LapisId": "GoldenEra" ,"typeid":"2"},
    { "DisplayName": "终极破坏", "ButtonImagePath": "BTN_MaxDamageSlot", "LapisId": "MaxDamageSlot" ,"typeid":"2"},
    { "DisplayName": "红唇诱惑", "ButtonImagePath": "BTN_RedHotDevil", "LapisId": "RedHotDevil" ,"typeid":"2"},
    { "DisplayName": "新年快乐", "ButtonImagePath": "BTN_HappyNewYear3", "LapisId": "HappyNewYear" ,"typeid":"1"},
    { "DisplayName": "怪兽总动员", "ButtonImagePath": "BTN_somanymonsters", "LapisId": "SoManyMonsters" ,"typeid":"1"},
    { "DisplayName": "糖果屋", "ButtonImagePath": "BTN_somuchcandy", "LapisId": "SoMuchCandy" ,"typeid":"5"},
    { "DisplayName": "寿司大餐", "ButtonImagePath": "BTN_somuchsushi", "LapisId": "SoMuchSushi" ,"typeid":"5"},
    { "DisplayName": "杰克机器人", "ButtonImagePath": "BTN_robojack1", "LapisId": "RoboJack","typeid":"2" },
    { "DisplayName": "海底狂欢", "ButtonImagePath": "BTN_FishParty1", "LapisId": "FishParty" ,"typeid":"2"},
    { "DisplayName": "魔鬼終結者2", "ButtonImagePath": "BTN_Terminator2_1", "LapisId": "Terminator2" ,"typeid":"5"},
    { "DisplayName": "蛮荒野狼", "ButtonImagePath": "BTN_CoolWolf1", "LapisId": "CoolWolf","typeid":"2" },
    { "DisplayName": "足球明星", "ButtonImagePath": "BTN_footballstar1", "LapisId": "FootballStar" ,"typeid":"2" },
    { "DisplayName": "冰原神枪女郎", "ButtonImagePath": "BTN_GirlswithGuns2", "LapisId": "GirlsWithGunsFrozenDawn" ,"typeid":"2"},
    { "DisplayName": "上流社会", "ButtonImagePath": "BTN_HighSociety", "LapisId": "HighSociety" ,"typeid":"2"},
    { "DisplayName": "神钓手", "ButtonImagePath": "BTN_WildCatch", "LapisId": "WildCatch" ,"typeid":"1"},
    { "DisplayName": "野性冠鹰", "ButtonImagePath": "BTN_UntamedCrownedEagle", "LapisId": "UntamedCrownedEagle","typeid":"3"},
    { "DisplayName": "虎月", "ButtonImagePath": "BTN_Tiger%20Moon", "LapisId": "TigerMoon" ,"typeid":"1"},
    { "DisplayName": "爱的收获", "ButtonImagePath": "BTN_SweetHarvest1", "LapisId": "SweetHarvest","typeid":"2"},
    { "DisplayName": "纯银3D", "ButtonImagePath": "BTN_SterlingSilver3D1", "LapisId": "SterlingSilver3d","typeid":"2"},
    { "DisplayName": "星光之吻", "ButtonImagePath": "BTN_StarlightKiss", "LapisId": "StarlightKiss","typeid":"2"},
    { "DisplayName": "克斯赛车", "ButtonImagePath": "BTN_RacingForPinks", "LapisId": "RacingForPinks" ,"typeid":"2"},
    { "DisplayName": "三只小猪", "ButtonImagePath": "BTN_PiggyFortunes", "LapisId": "PiggyFortunes" ,"typeid":"2"},
    { "DisplayName": "捉鬼大队", "ButtonImagePath": "BTN_ParadiseFound", "LapisId": "ParadiseFound" ,"typeid":"2"},
    { "DisplayName": "神秘格罗夫", "ButtonImagePath": "BTN_MysticDreams1", "LapisId": "MysticDreams","typeid":"3" },
    { "DisplayName": "散打大炮", "ButtonImagePath": "BTN_LooseCannon1", "LapisId": "LooseCannon" ,"typeid":"2"},
    { "DisplayName": "金钱赢家", "ButtonImagePath": "BTN_InItToWinIt", "LapisId": "InItToWinIt" ,"typeid":"2"},
    { "DisplayName": "高速扑克", "ButtonImagePath": "BTN_GoldSeries_MHHighSpeedPoker1", "LapisId": "HighSpeedPoker" ,"typeid":"6"},
    { "DisplayName": "瓦特博士", "ButtonImagePath": "BTN_DrWattsUp1", "LapisId": "DrWattsUp" ,"typeid":"2"},
    { "DisplayName": "海豚探秘", "ButtonImagePath": "BTN_DolphinQuest", "LapisId": "DolphinQuest" ,"typeid":"2"},
    { "DisplayName": "胸围银行", "ButtonImagePath": "BTN_BustTheBank1", "LapisId": "BustTheBank" ,"typeid":"2"},
    { "DisplayName": "求婚花嫁", "ButtonImagePath": "BTN_BootyTime", "LapisId": "Bridezilla" ,"typeid":"2"},
    { "DisplayName": "征服北极圈", "ButtonImagePath": "BTN_ArcticAgents", "LapisId": "ArcticAgents" ,"typeid":"2"},
    {"DisplayName":"丛林枪神女郎","ButtonImagePath":"BTN_GirlswithGuns1","LapisId":"GirlsWithGuns","typeid":"2"},
    {"DisplayName":"黑暗骑士黎明崛起","ButtonImagePath":"BTN_TheDarkKnightRises","LapisId":"TheDarkKnightRises","typeid":"3"},
    {"DisplayName":"阿瓦隆圣杯传","ButtonImagePath":"BTN_AvalonII","LapisId":"Avalon2","typeid":"2"},
    {"DisplayName":"幸运小妖精战利品","ButtonImagePath":"BTN_LuckyLeprechaunsLoot","LapisId":"LuckyLeprechaunsLoot","typeid":"2"},
    {"DisplayName":"幸运锦锂","ButtonImagePath":"BTN_LuckyKoi","LapisId":"LuckyKoi","typeid":"2"},
    {"DisplayName":"神秘圣诞老人","ButtonImagePath":"BTN_SecretSanta","LapisId":"SecretSanta","typeid":"2"},
    {"DisplayName":"花花公子","ButtonImagePath":"BTN_Playboy1","LapisId":"playboy","typeid":"2"},
    {"DisplayName":"路易斯扑克","ButtonImagePath":"BTN_LouisianaDouble1","LapisId":"LouisianaDouble","typeid":"6"},
    {"DisplayName":"双重王牌2","ButtonImagePath":"BTN_DoubleJoker1","LapisId":"DoubleJoker","typeid":"7"},
    {"DisplayName":"西部边境","ButtonImagePath":"BTN_WesternFrontier","LapisId":"WesternFrontier","typeid":"2"},
    {"DisplayName":"七海霸权","ButtonImagePath":"BTN_SovereignoftheSevenSeas","LapisId":"SovereignOfTheSevenSeas","typeid":"2"},
    {"DisplayName":"章鱼","ButtonImagePath":"BTN_Octopays","LapisId":"Octopays","typeid":"5"},
    {"DisplayName":"银河战舰","ButtonImagePath":"BTN_Galacticons","LapisId":"Galacticons","typeid":"2"},
    {"DisplayName":"摆脱","ButtonImagePath":"BTN_BreakAway1","LapisId":"BreakAway","typeid":"2"},
    {"DisplayName":"太空特務","ButtonImagePath":"BTN_BattlestarGalactica1","LapisId":"BattlestarGalactica","typeid":"2"},
    {"DisplayName":"人生经典","ButtonImagePath":"BTN_FinerReelsofLife","LapisId":"TheFinerReelsOfLife","typeid":"1"},
    {"DisplayName":"欧洲黄金21点","ButtonImagePath":"BTN_GoldSeries_EuroBlackjack1","LapisId":"EuropeanBJGold","typeid":"5"},
    {"DisplayName":"星际","ButtonImagePath":"BTN_StarScape1","LapisId":"Starscape","typeid":"1"},
    {"DisplayName":"视频扑克2","ButtonImagePath":"BTN_JacksorBetterPowerPoker1","LapisId":"Jackspwrpoker","typeid":"6"},
    {"DisplayName":"格斗女王","ButtonImagePath":"BTN_RRHeartsAndTarts1","LapisId":"RRQueenOfHearts","typeid":"2"},
    {"DisplayName":"泡泡富矿","ButtonImagePath":"BTN_BubbleBonanza","LapisId":"bubblebonanza","typeid":"7"},
    {"DisplayName":"三重扑克","ButtonImagePath":"BTN_GoldSeries_TriplePocket1","LapisId":"RubyTriplePocketPoker","typeid":"6"},
    {"DisplayName":"孟加拉虎","ButtonImagePath":"BTN_UntamedBengalTiger1","LapisId":"UntamedBengalTiger","typeid":"2"},
    {"DisplayName":"北洋财富","ButtonImagePath":"BTN_ArcticFortune1","LapisId":"ArcticFortune","typeid":"2"},
    {"DisplayName":"空手道猪","ButtonImagePath":"BTN_KaratePig1","LapisId":"KaratePig","typeid":"2"},
    {"DisplayName":"捣蛋斑马","ButtonImagePath":"BTN_ZanyZebra1","LapisId":"zebra","typeid":"1"},
    {"DisplayName":"女巫财富","ButtonImagePath":"BTN_WitchesWealth1","LapisId":"RubyWitchesWealth","typeid":"2"},
    {"DisplayName":"王者巫师","ButtonImagePath":"BTN_WinningWizards1","LapisId":"wwizards","typeid":"2"},
    {"DisplayName":"狂野冠军","ButtonImagePath":"BTN_WildChampions1","LapisId":"RubyWildChampions","typeid":"4"},
    {"DisplayName":"白水牛","ButtonImagePath":"BTN_WhiteBuffalo","LapisId":"WhiteBuffalo","typeid":"2"},
    {"DisplayName":"财气轮盘特版","ButtonImagePath":"BTN_WheelofWealthSE1","LapisId":"WheelOfWealthSE","typeid":"1"},
    {"DisplayName":"财富转轮","ButtonImagePath":"BTN_WheelOfWealth1","LapisId":"RubyWheelofWealth","typeid":"1"},
    {"DisplayName":"地球危机","ButtonImagePath":"BTN_WhatonEarth1","LapisId":"WhatonEarth","typeid":"1"},
    {"DisplayName":"猫头鹰","ButtonImagePath":"BTN_WhataHoot3","LapisId":"WhatAHoot","typeid":"2"},
    {"DisplayName":"打地鼠","ButtonImagePath":"BTN_WhackaJackpot1","LapisId":"RubyWhackAJackpot","typeid":"5"},
    {"DisplayName":"寿司大战","ButtonImagePath":"BTN_WasabiSan4","LapisId":"RubyWasabiSan","typeid":"2"},
    {"DisplayName":"心跳时刻","ButtonImagePath":"BTN_VinylCountdown4","LapisId":"VinylCountDown","typeid":"2"},
    {"DisplayName":"维多利亚时代雅贼","ButtonImagePath":"BTN_VictorianVillian","LapisId":"VictorianVillain","typeid":"3"},
    {"DisplayName":"拉斯维加斯黄金21点","ButtonImagePath":"BTN_GoldSeries_VegasStrip1","LapisId":"RubyVegasStripBlackjackGold","typeid":"5"},
    {"DisplayName":"拉斯维加斯21点","ButtonImagePath":"BTN_VegasStripBlackjack1","LapisId":"VegasStrip","typeid":"5"},
    {"DisplayName":"拉斯维加斯獨家黄金21点","ButtonImagePath":"BTN_GoldSeries_VegasSingleDeck1","LapisId":"RubyVegasSingleDeckBlackjackGold","typeid":"5"},
    {"DisplayName":"野性狼原","ButtonImagePath":"BTN_UntamedWolfPack1","LapisId":"UntamedWolfPack","typeid":"5"},
    {"DisplayName":"野性大猫熊","ButtonImagePath":"BTN_UntamedGiantPanda","LapisId":"UntamedGiantPanda","typeid":"5"},
    {"DisplayName":"忍者龟","ButtonImagePath":"BTN_TurtleyAwesome1","LapisId":"RubyTurtleyAwesome","typeid":"4"},
    {"DisplayName":"三重魔幻","ButtonImagePath":"BTN_TripleMagic1","LapisId":"TripleMagic","typeid":"3"},
    {"DisplayName":"三角魔组","ButtonImagePath":"BTN_Triangulation","LapisId":"Triangulation","typeid":"5"},
    {"DisplayName":"图腾宝藏","ButtonImagePath":"BTN_TotemTreasure1","LapisId":"RubyTotemTreasure","typeid":"1"},
    {"DisplayName":"古墓丽影2","ButtonImagePath":"BTN_TombRaiderSotS1","LapisId":"RubyTombRaiderII","typeid":"2"},
    {"DisplayName":"古墓丽影","ButtonImagePath":"BTN_TombRaider2","LapisId":"TombRaider","typeid":"2"},
    {"DisplayName":"虎熊争霸","ButtonImagePath":"BTN_TigerVsBear","LapisId":"TigerVsBear","typeid":"3"},
    {"DisplayName":"雷神索尔II","ButtonImagePath":"BTN_ThunderstruckTwo1","LapisId":"Thunderstruck2","typeid":"2"},
    {"DisplayName":"埃及王座","ButtonImagePath":"BTN_ThroneOfEgypt1","LapisId":"throneofegypt","typeid":"2"},
    {"DisplayName":"鼠帮","ButtonImagePath":"BTN_RatPack1","LapisId":"RubyTheRatPack","typeid":"2"},
    {"DisplayName":"摇滚时代","ButtonImagePath":"BTN_TheOsbournes1","LapisId":"RubyTheOsbournes","typeid":"1"},
    {"DisplayName":"真假公主","ButtonImagePath":"BTN_TheLostPrincessAnastasia","LapisId":"TheLostPrincessAnastasia","typeid":"3"},
    {"DisplayName":"尖子威力扑克","ButtonImagePath":"BTN_TensOrBetter1","LapisId":"TensorBetter","typeid":"6"},
    {"DisplayName":"尖子威力扑克","ButtonImagePath":"BTN_TensOrBetterPowerPoker1","LapisId":"TensorBetterPwrPoker","typeid":"6"},
    {"DisplayName":"英伦时光","ButtonImagePath":"BTN_TallyHo1","LapisId":"TallyHo","typeid":"2"},
    {"DisplayName":"冲浪野生动物园","ButtonImagePath":"BTN_SurfSafari","LapisId":"SurfSafari","typeid":"3"},
    {"DisplayName":"超级女警","ButtonImagePath":"BTN_SuperZeroes1","LapisId":"RubySuperZeroes","typeid":"4"},
    { "DisplayName": "超级乐21点", "ButtonImagePath": "BTN_SuperFun21Blackjack1", "LapisId": "SuperFun21" ,"typeid":"5"},
    { "DisplayName": "超级奖金宾果", "ButtonImagePath": "BTN_SuperBonusBingo1", "LapisId": "RubySuperBonusBingo"  ,"typeid":"5"},
    { "DisplayName": "超级马力", "ButtonImagePath": "BTN_SupeItUp2", "LapisId": "RubySupeItUp","typeid":"2" },
    { "DisplayName": "太阳探密", "ButtonImagePath": "BTN_SunQuest3", "LapisId": "SunQuest","typeid":"2"  },
    { "DisplayName": "蒸汽朋克英雄", "ButtonImagePath": "BTN_SteamPunkHeroes", "LapisId": "SteamPunkHeroes","typeid":"3" },
    { "DisplayName": "泰坦神庙", "ButtonImagePath": "BTN_StashoftheTitans1", "LapisId": "StashOfTheTitans" ,"typeid":"2"},
    { "DisplayName": "海滨嘉年华", "ButtonImagePath": "BTN_SpringBreak2", "LapisId": "SpringBreak" ,"typeid":"2"},
    { "DisplayName": "旋转魔球", "ButtonImagePath": "BTN_Spingo", "LapisId": "Spingo" ,"typeid":"5"},
    { "DisplayName": "华丽剧场", "ButtonImagePath": "BTN_SpectacularWheelOfWealth1", "LapisId": "Spectacular" ,"typeid":"3"},
    { "DisplayName": "西班牙黄金21点", "ButtonImagePath": "BTN_GoldSeries_SpanishBlackjack1", "LapisId": "RubySpanishBlackjackGold" ,"typeid":"5"},
    { "DisplayName": "西班牙21点", "ButtonImagePath": "BTN_SpanishBlackjack1", "LapisId": "Spanish" ,"typeid":"5"},
    { "DisplayName": "黄金太空探险", "ButtonImagePath": "BTN_SpaceEvaderGold1", "LapisId": "RubySpaceEvaderGold" ,"typeid":"5"},
    { "DisplayName": "太空探险", "ButtonImagePath": "BTN_SpaceEvader1", "LapisId": "RubySpaceEvader" ,"typeid":"5"},
    { "DisplayName": "动物足球大赛", "ButtonImagePath": "BTN_SoccerSafari1", "LapisId": "RubySoccerSafari" ,"typeid":"2"},
    { "DisplayName": "超级灌篮", "ButtonImagePath": "BTN_SlamFunk1", "LapisId": "RubySlamFunk" ,"typeid":"4"},
    { "DisplayName": "神鬼奇航", "ButtonImagePath": "BTN_SkullDuggery1", "LapisId": "SkullDuggery" ,"typeid":"2"},
    { "DisplayName": "西部抢金", "ButtonImagePath": "BTN_SixShooterLooterGold1", "LapisId": "RubySixShooterLooter" ,"typeid":"5"},
    { "DisplayName": "银坊", "ButtonImagePath": "BTN_SilverFang1", "LapisId": "SilverFang" ,"typeid":"2"},
    { "DisplayName": "发射！", "ButtonImagePath": "BTN_Shoot", "LapisId": "Shoot" ,"typeid":"2"},
    { "DisplayName": "暗恋", "ButtonImagePath": "BTN_SecretAdmirer1", "LapisId": "SecretAdmirer","typeid":"2" },
    { "DisplayName": "守财奴", "ButtonImagePath": "BTN_Scrooge1", "LapisId": "RubyScrooge","typeid":"2" },
    { "DisplayName": "暴走圣诞", "ButtonImagePath": "BTN_SantasWildRide1", "LapisId": "SantasWildRide" ,"typeid":"2"},
    { "DisplayName": "森巴宾果", "ButtonImagePath": "BTN_SambaBingo1", "LapisId": "SambaBingo" ,"typeid":"5"},
    { "DisplayName": "罗马宝藏", "ButtonImagePath": "BTN_RomanRiches11", "LapisId": "romanriches" ,"typeid":"1"},
    { "DisplayName": "速度滑轮", "ButtonImagePath": "BTN_RollerDerby", "LapisId": "RollerDerby" ,"typeid":"3"},
    { "DisplayName": "水岸摇滚秀", "ButtonImagePath": "BTN_RocktheBoat18", "LapisId": "RockTheBoat" ,"typeid":"1"},
    { "DisplayName": "金豪大亨", "ButtonImagePath": "BTN_RivieraRiches1", "LapisId": "MonteCarloRiches" ,"typeid":"2"},
    { "DisplayName": "老国王科尔", "ButtonImagePath": "BTN_RRKing1", "LapisId": "RROldKingCole" ,"typeid":"2"},
    { "DisplayName": "复古钻石老虎机", "ButtonImagePath": "BTN_RRDiamondGlitz1", "LapisId": "RetroReelsDiamondGlitz" ,"typeid":"2"},
    { "DisplayName": "火热老虎机", "ButtonImagePath": "BTN_RRExtreme1", "LapisId": "RetroReelsExtremeHeat" ,"typeid":"2"},
    { "DisplayName": "复古老虎机", "ButtonImagePath": "BTN_RetroReels1", "LapisId": "RetroReels" ,"typeid":"2"},
    { "DisplayName": "速食轮动", "ButtonImagePath": "BTN_ReelThunder2", "LapisId": "ReelThunder" ,"typeid":"2"},
    { "DisplayName": "海上驱动", "ButtonImagePath": "BTN_ReelStrike1", "LapisId": "ReelStrike" ,"typeid":"2"},
    { "DisplayName": "宝石连线", "ButtonImagePath": "BTN_ReelGems1", "LapisId": "ReelGems" ,"typeid":"2"},
    { "DisplayName": "抢滩现金", "ButtonImagePath": "BTN_RapidReels5", "LapisId": "RubyRapidReels" ,"typeid":"1"},
    { "DisplayName": "顶尖房仲", "ButtonImagePath": "BTN_PrimeProperty1", "LapisId": "RubyPrimeProperty" ,"typeid":"2"},
    { "DisplayName": "总理赛马", "ButtonImagePath": "BTN_PremierTrotting1", "LapisId": "PremierTrotting" ,"typeid":"7"},
    { "DisplayName": "总理钻石轮盘", "ButtonImagePath": "BTN_PremierRouletteDE1", "LapisId": "PremierRouletteDE" ,"typeid":"5"},
    { "DisplayName": "总理轮盘", "ButtonImagePath": "BTN_PremierRoulette9", "LapisId": "RubyPremierRoulette" ,"typeid":"5"},
    { "DisplayName": "皇家赛马", "ButtonImagePath": "BTN_PremierRacing1", "LapisId": "PremierRacing" ,"typeid":"7"},
    { "DisplayName": "总理欧元21点", "ButtonImagePath": "BTN_GoldSeries_MHBonusBlackjack2", "LapisId": "RubyPBJMultiHandBonus" ,"typeid":"5"},
    { "DisplayName": "总理连胜21点", "ButtonImagePath": "BTN_PremierBlackjackHiStreak1", "LapisId": "RubyPBJHighStreak" ,"typeid":"5"},
    { "DisplayName": "高低黄金21点", "ButtonImagePath": "BTN_PremierBlackjackHiLo1", "LapisId": "RubyPBJHiLo" ,"typeid":"5"},
    { "DisplayName": "花蜜国度", "ButtonImagePath": "BTN_PollenNation1", "LapisId": "PollenNation" ,"typeid":"2"},
    { "DisplayName": "神踪扑克", "ButtonImagePath": "BTN_PokerPursuit1", "LapisId": "PokerPursuit" ,"typeid":"6"},
    { "DisplayName": "海底宝物", "ButtonImagePath": "BTN_PlundertheSea1", "LapisId": "RubyPlunderTheSea" ,"typeid":"4"},
    { "DisplayName": "选球好手", "ButtonImagePath": "BTN_PharoahsGems1", "LapisId": "RubyPharaohsGems" ,"typeid":"1"},
    { "DisplayName": "法老王宝石", "ButtonImagePath": "BTN_PharaohBingo1", "LapisId": "PharaohBingo" ,"typeid":"7"},
    { "DisplayName": "发现天堂", "ButtonImagePath": "BTN_OrientalFortune2", "LapisId": "OrientalFortune" ,"typeid":"3"},
    { "DisplayName": "足球射门", "ButtonImagePath": "BTN_OffsideandSeek1", "LapisId": "RubyOffsideAndSeek" ,"typeid":"2"},
    { "DisplayName": "神秘格罗夫", "ButtonImagePath": "BTN_MystiqueGrove", "LapisId": "MystiqueGrove" ,"typeid":"2"},
    { "DisplayName": "梦奇金", "ButtonImagePath": "BTN_Munchkins2", "LapisId": "RubyMunchkins" ,"typeid":"2"},
    { "DisplayName": "孟买魔术师", "ButtonImagePath": "BTN_MumbaiMagic1", "LapisId": "RubyMumbaiMagic" ,"typeid":"2"},
    { "DisplayName": "维加斯到地21点", "ButtonImagePath": "BTN_GoldSeries_MHVegasDowntown2", "LapisId": "RubyMultiVegasDowntownBlackjackGold" ,"typeid":"7"},
    { "DisplayName": "尊爵21点", "ButtonImagePath": "BTN_MHPremierBlackjack1", "LapisId": "RubyPBJMultiHand" ,"typeid":"5"},
    { "DisplayName": "完美对子21点", "ButtonImagePath": "BTN_GoldSeries_MHPerfectPairsBlackjack1", "LapisId": "RubyMHPerfectPairs" ,"typeid":"5"},
    { "DisplayName": "黄金欧式21点", "ButtonImagePath": "BTN_GoldSeries_MHEuroBlackjack2", "LapisId": "MHEuropeanBJGold" ,"typeid":"5"},
    { "DisplayName": "黄金轮盘", "ButtonImagePath": "BTN_GoldSeries_MultiWheelRoulette1", "LapisId": "MultiWheelRouletteGold" ,"typeid":"5"},
    { "DisplayName": "罪犯登记", "ButtonImagePath": "BTN_MugshotMadness1", "LapisId": "MugshotMadness" ,"typeid":"5"},
    { "DisplayName": "奥林匹斯山", "ButtonImagePath": "BTN_MountOlympus", "LapisId": "MountOlympus","typeid":"2"},
    { "DisplayName": "麻法世界", "ButtonImagePath": "BTN_Moonshine1", "LapisId": "Moonshine" ,"typeid":"2"},
    { "DisplayName": "怪兽狂热", "ButtonImagePath": "BTN_MonsterMania1", "LapisId": "MonsterMania" ,"typeid":"4"},
    { "DisplayName": "败家金猿", "ButtonImagePath": "BTN_MoneyMadMonkey1", "LapisId": "RubyMoneyMadMonkey","typeid":"2"},
    { "DisplayName": "百万美人鱼", "ButtonImagePath": "BTN_MermaidsMillions1", "LapisId": "MermaidsMillions" ,"typeid":"2"},
    { "DisplayName": "米加盗抢银行", "ButtonImagePath": "BTN_BreakdaBankAgainMegaSpin1", "LapisId": "MSBreakDaBankAgain","typeid":"2"},
    { "DisplayName": "马雅公主", "ButtonImagePath": "BTN_MayanPrincess1", "LapisId": "MayanPrincess","typeid":"2"},
    { "DisplayName": "马雅宾果", "ButtonImagePath": "BTN_MayanBingo1", "LapisId": "MayanBingo" ,"typeid":"5"},
    { "DisplayName": "魔术魅力", "ButtonImagePath": "BTN_MagicCharms", "LapisId": "MagicCharms","typeid":"2" },
    { "DisplayName": "魔术箱", "ButtonImagePath": "BTN_MagicBoxes", "LapisId": "MagicBoxes","typeid":"2" },
    { "DisplayName": "疯狂帽商", "ButtonImagePath": "BTN_MadHatters1", "LapisId": "MadHatters" ,"typeid":"2"},
    { "DisplayName": "幸运女巫", "ButtonImagePath": "BTN_LuckyWitch1", "LapisId": "Luckywitch" ,"typeid":"2"},
    { "DisplayName": "幸运数字", "ButtonImagePath": "BTN_LuckyNumbers1", "LapisId": "RubyLuckyNumbers" ,"typeid":"2"},
    { "DisplayName": "嘻哈摇滚", "ButtonImagePath": "BTN_Loaded1", "LapisId": "Loaded" ,"typeid":"2"},
    { "DisplayName": "巨富遗产", "ButtonImagePath": "BTN_Legacy2", "LapisId": "RubyLegacy" ,"typeid":"1"},
    { "DisplayName": "海洋宝藏", "ButtonImagePath": "BTN_LeaguesOfFortune1", "LapisId": "LeaguesOfFortune" ,"typeid":"2"},
    { "DisplayName": "红衣女郎", "ButtonImagePath": "BTN_LadyInRed2", "LapisId": "LadyInRed" ,"typeid":"2"},
    { "DisplayName": "淑女之夜", "ButtonImagePath": "BTN_LadiesNite5", "LapisId": "LadiesNite" ,"typeid":"2"},
    { "DisplayName": "国王现金", "ButtonImagePath": "BTN_KingsofCash1", "LapisId": "KingsOfCash","typeid":"2" },
    { "DisplayName": "亚瑟王", "ButtonImagePath": "BTN_KingArthur", "LapisId": "RubyKingArthur" ,"typeid":"2" },
    { "DisplayName": "快乐彩", "ButtonImagePath": "BTN_Keno3", "LapisId": "Keno" ,"typeid":"7"},
    { "DisplayName": "加德满都", "ButtonImagePath": "BTN_Kathmandu1", "LapisId": "RubyKathmandu" ,"typeid":"2"},
    { "DisplayName": "侏罗纪彩金", "ButtonImagePath": "BTN_JurassicJackpot1", "LapisId": "jurassicjackpot" ,"typeid":"1"},
    { "DisplayName": "小丑扑克2", "ButtonImagePath": "BTN_JokerPoker1", "LapisId": "Jokerpok" ,"typeid":"6"},
    { "DisplayName": "小丑扑克", "ButtonImagePath": "BTN_JokerPokerPowerPoker1", "LapisId": "JokerPwrPoker" ,"typeid":"6"},
    { "DisplayName": "叮叮当", "ButtonImagePath": "BTN_JingleBells6", "LapisId": "RubyJingleBells" ,"typeid":"1"},
    { "DisplayName": "东洋宝石", "ButtonImagePath": "BTN_JewelsoftheOrient1", "LapisId": "JewelsOfTheOrient","typeid":"2" },
    { "DisplayName": "哲基尔和海德", "ButtonImagePath": "BTN_JekyllandHyde", "LapisId": "JekyllAndHyde" ,"typeid":"2"},
    { "DisplayName": "视频扑克", "ButtonImagePath": "BTN_JacksOrBetter1", "LapisId": "jacks" ,"typeid":"6"},
    { "DisplayName": "特快派彩", "ButtonImagePath": "BTN_JackpotExpress1", "LapisId": "jexpress" ,"typeid":"1"},
    { "DisplayName": "时来运转", "ButtonImagePath": "BTN_InstantWinCardSelector1", "LapisId": "IWCardSelector","typeid":"4" },
    { "DisplayName": "浪漫传奇", "ButtonImagePath": "BTN_ImmortalRomance1", "LapisId": "ImmortalRomance" ,"typeid":"2"},
    { "DisplayName": "龙宫", "ButtonImagePath": "BTN_HouseofDragons", "LapisId": "RubyHouseofDragons" ,"typeid":"2"},
    { "DisplayName": "棒球热", "ButtonImagePath": "BTN_HotShot3", "LapisId": "RubyHotShot" ,"typeid":"2"},
    { "DisplayName": "辣妹纹身", "ButtonImagePath": "BTN_HotInk1", "LapisId": "HotInk" ,"typeid":"2"},
    { "DisplayName": "欢乐圣诞", "ButtonImagePath": "BTN_Hohoho1", "LapisId": "HoHoHo" ,"typeid":"2"},
    { "DisplayName": "杀手", "ButtonImagePath": "BTN_Hitman1", "LapisId": "RubyHitman" ,"typeid":"2"},
    { "DisplayName": "黄金13欧洲21点", "ButtonImagePath": "BTN_GoldSeries_HiLo13EuroBJ1", "LapisId": "RubyHiLoBlackjackGold" ,"typeid":"5"},
    { "DisplayName": "欧洲21点", "ButtonImagePath": "BTN_EuroBlackjackHighLimit1", "LapisId": "HLEuropeanAdvBJ" ,"typeid":"5"},
    { "DisplayName": "极限百家乐", "ButtonImagePath": "BTN_HighLimitBaccarat1", "LapisId": "HighLimitBaccarat" ,"typeid":"5"},
    { "DisplayName": "六角连线", "ButtonImagePath": "BTN_Hexaline1", "LapisId": "Hexaline" ,"typeid":"7"},
    { "DisplayName": "地狱劲妈", "ButtonImagePath": "BTN_HellGrannies", "LapisId": "HellsGrannies" ,"typeid":"2"},
    { "DisplayName": "地狱怪客", "ButtonImagePath": "BTN_HellBoy1", "LapisId": "RubyHellBoy" ,"typeid":"2"},
    { "DisplayName": "帅哥晚宴", "ButtonImagePath": "BTN_Harveys1", "LapisId": "RubyHarveys" ,"typeid":"2"},
    { "DisplayName": "肉搏战", "ButtonImagePath": "BTN_HandToHandCombat1", "LapisId": "RubyHandToHandCombat" ,"typeid":"4"},
    { "DisplayName": "万圣狂欢2", "ButtonImagePath": "BTN_Halloweenies1", "LapisId": "RubyIWHalloweenies" ,"typeid":"4"},
    { "DisplayName": "万圣狂欢", "ButtonImagePath": "BTN_Halloweenies1", "LapisId": "RubyHalloweenies" ,"typeid":"2" },
    { "DisplayName": "美发师", "ButtonImagePath": "BTN_HairyFairies1", "LapisId": "RubyHairyFairies" ,"typeid":"4" },
    { "DisplayName": "伟大狮鹫", "ButtonImagePath": "BTN_GreatGriffin1", "LapisId": "GreatGriffin" ,"typeid":"2"},
    { "DisplayName": "赛车老奶奶", "ButtonImagePath": "BTN_GrannyPrix1", "LapisId": "RubyGrannyPrix" ,"typeid":"4"},
    { "DisplayName": "黄金鼠", "ButtonImagePath": "BTN_GopherGold2", "LapisId": "GopherGold" ,"typeid":"2"},
    { "DisplayName": "完美起跑", "ButtonImagePath": "BTN_GoodToGo2", "LapisId": "RubyGoodToGo" ,"typeid":"2"},
    { "DisplayName": "僵尸家族", "ButtonImagePath": "BTN_GoldenGhouls1", "LapisId": "RubyGoldenGhouls" ,"typeid":"4"},
    { "DisplayName": "金龙", "ButtonImagePath": "BTN_GoldenDragon6", "LapisId": "gdragon" ,"typeid":"1"},
    { "DisplayName": "黄金工厂", "ButtonImagePath": "BTN_GoldFactory1", "LapisId": "GoldFactory" ,"typeid":"2"},
    { "DisplayName": "黄金海岸", "ButtonImagePath": "BTN_GoldCoast3", "LapisId": "RubyGoldCoast" ,"typeid":"1"},
    { "DisplayName": "圣诞礼物", "ButtonImagePath": "BTN_GiftRap2", "LapisId": "GiftRap" ,"typeid":"2"},
    { "DisplayName": "矿怪连线", "ButtonImagePath": "BTN_Germinator1", "LapisId": "Germinator" ,"typeid":"7" },
    { "DisplayName": "神灯精灵宝石", "ButtonImagePath": "BTN_GeniesGems2", "LapisId": "geniesgems" ,"typeid":"2"},
    { "DisplayName": "网球刮刮乐", "ButtonImagePath": "BTN_GameSetandScratch1", "LapisId": "RubyGameSetAndScratch" ,"typeid":"4"},
    { "DisplayName": "水果老虎机", "ButtonImagePath": "BTN_FruitSlots1", "LapisId": "fruits" ,"typeid":"1"},
    { "DisplayName": "水果宾果", "ButtonImagePath": "BTN_FruitBingo1", "LapisId": "RubyFruitBingo" ,"typeid":"7"},
    { "DisplayName": "法国轮盘", "ButtonImagePath": "BTN_FrenchRoulette1", "LapisId": "FrenchRoulette" ,"typeid":"5"},
    { "DisplayName": "丢冰球", "ButtonImagePath": "BTN_FreezingFuzzballs1", "LapisId": "RubyFreezingFuzzballs" ,"typeid":"7"},
    { "DisplayName": "四块组合", "ButtonImagePath": "BTN_FourByFour1", "LapisId": "FourByFour" ,"typeid":"7"},
    { "DisplayName": "幸运饼乾", "ButtonImagePath": "BTN_FortuneCookie7", "LapisId": "FortuneCookie" ,"typeid":"1"},
    { "DisplayName": "錢進泡泡", "ButtonImagePath": "BTN_FoamyFortunes1", "LapisId": "RubyFoamyFortunes" ,"typeid":"7"},
    { "DisplayName": "飞行王牌", "ButtonImagePath": "BTN_FlyingAce2", "LapisId": "RubyFlyingAce" ,"typeid":"1"},
    { "DisplayName": "胖女士考辛斯", "ButtonImagePath": "BTN_FatLadySings1", "LapisId": "FatLadySings" ,"typeid":"2"},
    { "DisplayName": "神奇轮转7s", "ButtonImagePath": "BTN_FantasticSevens1", "LapisId": "fan7" ,"typeid":"1"},
    { "DisplayName": "欧洲黄金轮盘", "ButtonImagePath": "BTN_GoldSeries_EuroRoulette1", "LapisId": "EuroRouletteGold" ,"typeid":"5"},
    { "DisplayName": "魔法森林", "ButtonImagePath": "BTN_EnchantedWoods1", "LapisId": "EnchantedWoods","typeid":"2" },
    { "DisplayName": "元素精灵", "ButtonImagePath": "BTN_Elementals2", "LapisId": "rubyelementals" ,"typeid":"2"},
    { "DisplayName": "电动宾果", "ButtonImagePath": "BTN_ElectroBingo1", "LapisId": "ElectroBingo" ,"typeid":"7"},
    { "DisplayName": "老鹰之翼", "ButtonImagePath": "BTN_EaglesWings1", "LapisId": "EaglesWings" ,"typeid":"2"},
    { "DisplayName": "星际战争", "ButtonImagePath": "BTN_DroneWars", "LapisId": "DroneWars" ,"typeid":"2" },
    { "DisplayName": "圣龙赐福", "ButtonImagePath": "BTN_DragonsFortune1", "LapisId": "RubyDragonsFortune" ,"typeid":"7"},
    { "DisplayName": "双钻宝", "ButtonImagePath": "BTN_DoubleWammy1", "LapisId": "DoubleWammy" ,"typeid":"1"},
    { "DisplayName": "双重魔幻", "ButtonImagePath": "BTN_DoubleMagic1", "LapisId": "dm" ,"typeid":"1"},
    { "DisplayName": "双重王牌", "ButtonImagePath": "BTN_DoubleJokerPowerPoker1", "LapisId": "DoubleJokerPwrPoker" ,"typeid":"6"},
    { "DisplayName": "黄金双冒险翻牌", "ButtonImagePath": "BTN_GoldSeries_DoubleExposureBlackjack1", "LapisId": "RubyDoubleExposureBlackjackGold" ,"typeid":"6"},
    { "DisplayName": "双威王牌", "ButtonImagePath": "BTN_DoubleDoubleBonus1", "LapisId": "DoubleDoubleBonus" ,"typeid":"6"},
    { "DisplayName": "双倍红利扑克", "ButtonImagePath": "BTN_DoubleBonus1", "LapisId": "RubyDoubleBonusPoker" ,"typeid":"6"},
    { "DisplayName": "狗老大", "ButtonImagePath": "BTN_Dogfather2", "LapisId": "RubyDogfather" ,"typeid":"2"},
    { "DisplayName": "恐龙世界", "ButtonImagePath": "BTN_DinoMight1", "LapisId": "DinoMight" ,"typeid":"2"},
    { "DisplayName": "野蛮对决2", "ButtonImagePath": "BTN_DeucesWild1", "LapisId": "deuceswi" ,"typeid":"6"},
    { "DisplayName": "野蛮对决", "ButtonImagePath": "BTN_DeucesWildPowerPoker1", "LapisId": "DeucesWildPwrPoker" ,"typeid":"6"},
    { "DisplayName": "狂野扑克2", "ButtonImagePath": "BTN_DeucesJoker1", "LapisId": "DeucesandJoker" ,"typeid":"6"},
    { "DisplayName": "狂野扑克", "ButtonImagePath": "BTN_DeucesJokerPowerPoker1", "LapisId": "Deuces%26JokerPwrPoker" ,"typeid":"6"},
    { "DisplayName": "耶诞礼物大厅", "ButtonImagePath": "BTN_DeckTheHalls1", "LapisId": "RubyDeckTheHalls" ,"typeid":"3"},
    { "DisplayName": "僵尸面包", "ButtonImagePath": "BTN_DawnoftheBread1", "LapisId": "RubyDawnOfTheBread" ,"typeid":"4" },
    { "DisplayName": "网络扑克", "ButtonImagePath": "BTN_Cyberstud1", "LapisId": "Cyberstud" ,"typeid":"6"},
    { "DisplayName": "黄金墓穴探险", "ButtonImagePath": "BTN_CryptCrusadeGold1", "LapisId": "RubyCryptCrusadeGold" ,"typeid":"7"},
    { "DisplayName": "墓穴探险", "ButtonImagePath": "BTN_CryptCrusade1", "LapisId": "RubyCryptCrusade" ,"typeid":"7"},
    { "DisplayName": "王冠与锚", "ButtonImagePath": "BTN_CrownAndAnchor1", "LapisId": "CrownAndAnchor" ,"typeid":"7"},
    { "DisplayName": "疯狂变色龙", "ButtonImagePath": "BTN_CrazyChameleons1", "LapisId": "CrazyChameleons","typeid":"2" },
    { "DisplayName": "疯狂八零年代", "ButtonImagePath": "BTN_Crazy80s4", "LapisId": "RubyCrazy80s" ,"typeid":"2" },
    { "DisplayName": "慵懒土豆", "ButtonImagePath": "BTN_CouchPotato2", "LapisId": "CouchPotato" ,"typeid":"1"},
    { "DisplayName": "宇宙霹雳猫", "ButtonImagePath": "BTN_CosmicCat1", "LapisId": "cosmicc" ,"typeid":"1"},
    { "DisplayName": "酷巴克", "ButtonImagePath": "BTN_CoolBuck1", "LapisId": "coolbuck"  ,"typeid":"1"},
    { "DisplayName": "网球中心", "ButtonImagePath": "BTN_CentreCourt1", "LapisId": "RubyCentreCourt" ,"typeid":"2"},
    { "DisplayName": "土豪金", "ButtonImagePath": "BTN_Cashville1", "LapisId": "Cashville" ,"typeid":"2"},
    { "DisplayName": "现金虫刮刮卡", "ButtonImagePath": "BTN_Cashapillar1", "LapisId": "RubyIWCashapillar" ,"typeid":"4"},
    { "DisplayName": "现金虫欢", "ButtonImagePath": "BTN_Cashapillar1", "LapisId": "RubyCashapillar" ,"typeid":"2"},
    { "DisplayName": "伯爵功绩", "ButtonImagePath": "BTN_Cashanova1", "LapisId": "Cashanova" ,"typeid":"2"},
    { "DisplayName": "疯狂现金", "ButtonImagePath": "BTN_CashCrazy9", "LapisId": "cashcrazy" ,"typeid":"1"},
    { "DisplayName": "现金蚌", "ButtonImagePath": "BTN_CashClams2", "LapisId": "CashClams" ,"typeid":"1"},
    { "DisplayName": "狂欢节", "ButtonImagePath": "BTN_Carnaval2", "LapisId": "Carnaval" ,"typeid":"2"},
    { "DisplayName": "布希电报", "ButtonImagePath": "BTN_BushTelegraph1", "LapisId": "BushTelegraph","typeid":"2" },
    { "DisplayName": "燃烧的欲望", "ButtonImagePath": "BTN_BurningDesire1", "LapisId": "RubyBurningDesire" ,"typeid":"2"},
    { "DisplayName": "兔子跳跳跳", "ButtonImagePath": "BTN_BunnyBoilerGold1", "LapisId": "RubyBunnyBoilerGold" ,"typeid":"7"},
    { "DisplayName": "兔子锅炉", "ButtonImagePath": "BTN_BunnyBoiler1", "LapisId": "RubyBunnyBoiler" ,"typeid":"7"},
    { "DisplayName": "银行破坏家", "ButtonImagePath": "BTN_BreakDaBankAgain1", "LapisId": "RubyBreakDaBankAgain","typeid":"2"},
    { "DisplayName": "银行破坏家", "ButtonImagePath": "BTN_BreakDaBank1", "LapisId": "BreakDaBank" ,"typeid":"1"},
    { "DisplayName": "滚球王", "ButtonImagePath": "BTN_BowledOver1", "LapisId": "RubyBowledOver" ,"typeid":"7"},
    { "DisplayName": "宝藏时间", "ButtonImagePath": "BTN_BootyTime", "LapisId": "BootyTime" ,"typeid":"3"},
    { "DisplayName": "豪华奖金扑克", "ButtonImagePath": "BTN_BonusPokerDeluxe1", "LapisId": "RubyBonusPokerDeluxe" ,"typeid":"6"},
    { "DisplayName": "奖金扑克", "ButtonImagePath": "BTN_BonusPoker1", "LapisId": "RubyBonusPoker" ,"typeid":"6"},
    { "DisplayName": "狂野奖金", "ButtonImagePath": "BTN_BonusDeucesWild1", "LapisId": "RubyBonusDeucesWild" ,"typeid":"6"},
    { "DisplayName": "宾果富矿", "ButtonImagePath": "BTN_BingoBonanza1", "LapisId": "RubyBingoBonanza" ,"typeid":"7"},
    { "DisplayName": "马戏团", "ButtonImagePath": "BTN_BigTop1", "LapisId": "BigTop"  ,"typeid":"3"},
    { "DisplayName": "森林酋长", "ButtonImagePath": "BTN_BigKahuna_SL1", "LapisId": "RubyBigKahunaSnakesAndLadders" ,"typeid":"3"},
    { "DisplayName": "水果丛林", "ButtonImagePath": "BTN_BigKahuna1", "LapisId": "BigKahuna","typeid":"2" },
    { "DisplayName": "猴王大餐2", "ButtonImagePath": "BTN_InstantWin_BigBreak1", "LapisId": "RubyIWBigBreak" ,"typeid":"4"},
    { "DisplayName": "猴王大餐", "ButtonImagePath": "BTN_BigBreak1", "LapisId": "RubyBigBreak" ,"typeid":"2"},
    { "DisplayName": "5大赛黄金21点", "ButtonImagePath": "BTN_GoldSeries_BigFive1", "LapisId": "RubyBigFiveBlackjackGold" ,"typeid":"5"},
    { "DisplayName": "美味餐厅", "ButtonImagePath": "BTN_Belissimo17", "LapisId": "Belissimo" ,"typeid":"2"},
    { "DisplayName": "推啤酒大赛", "ButtonImagePath": "BTN_BeerFest1", "LapisId": "RubyBeerFest" ,"typeid":"7"},
    { "DisplayName": "黄金百家乐", "ButtonImagePath": "BTN_GoldSeries_Baccarat1", "LapisId": "BaccaratGold" ,"typeid":"6"},
    { "DisplayName": "百家乐", "ButtonImagePath": "BTN_Baccarat2", "LapisId": "Baccarat" ,"typeid":"5"},
    { "DisplayName": "阿瓦隆", "ButtonImagePath": "BTN_Avalon1", "LapisId": "RubyAvalon" ,"typeid":"2"},
    { "DisplayName": "西洋城黄金21点", "ButtonImagePath": "BTN_GoldSeries_AtlanticCityBlackjack1", "LapisId": "AtlanticCityBJGold" ,"typeid":"5"},
    { "DisplayName": "东方美人", "ButtonImagePath": "BTN_AsianBeauty1", "LapisId": "AsianBeauty" ,"typeid":"2"},
    { "DisplayName": "美式轮盘", "ButtonImagePath": "BTN_USRoulette1", "LapisId": "AmericanRoulette" ,"typeid":"5"},
    { "DisplayName": "美式扑克", "ButtonImagePath": "BTN_AllAmerican1", "LapisId": "RubyAllAmerican" ,"typeid":"6"},
    { "DisplayName": "王牌扑克", "ButtonImagePath": "BTN_AllAces1", "LapisId": "RubyAllAces" ,"typeid":"6"},
    { "DisplayName": "梦魇艾莉丝仙境", "ButtonImagePath": "BTN_AlaxeInZombieland", "LapisId": "AlaxeInZombieland" ,"typeid":"2"},
    { "DisplayName": "阿拉斯加冰钓", "ButtonImagePath": "BTN_AlaskanFishing1", "LapisId": "AlaskanFishing" ,"typeid":"2"},
    { "DisplayName": "美女特务", "ButtonImagePath": "BTN_AgentJaneBlonde7", "LapisId": "RubyAgentJaneBlonde"  ,"typeid":"2"},
    { "DisplayName": "大航海时代", "ButtonImagePath": "BTN_AgeofDiscovery3", "LapisId": "RubyAgeOfDiscovery"  ,"typeid":"2"},
    { "DisplayName": "野生探险", "ButtonImagePath": "BTN_AdventurePalace4", "LapisId": "RubyAdventurePalace" ,"typeid":"2"},
    { "DisplayName": "尖子扑克", "ButtonImagePath": "BTN_AcesFaces1", "LapisId": "acesfaces" ,"typeid":"6"},
    { "DisplayName": "视讯尖子扑克", "ButtonImagePath": "BTN_AcesFacesPowerPoker1", "LapisId": "AcesfacesPwrPoker" ,"typeid":"6"},
    { "DisplayName": "尖子和花牌", "ButtonImagePath": "BTN_AcesAndEights1", "LapisId": "RubyAcesAndEights" ,"typeid":"6"},
    { "DisplayName": "旋转快餐", "ButtonImagePath": "BTN_5ReelDrive1", "LapisId": "5ReelDrive4" ,"typeid":"2"},
    { "DisplayName": "桥牌老虎机", "ButtonImagePath": "BTN_JoyOfSix", "LapisId": "JoyOfSix" ,"typeid":"2"},
    { "DisplayName": "法老宾果", "ButtonImagePath": "BTN_PhantomCash", "LapisId": "PhantomCash" ,"typeid":"5"},
    { "DisplayName": "狂野马戏团", "ButtonImagePath": "BTN_TwistedCircus", "LapisId": "TheTwistedCircus" ,"typeid":"1"},
    { "DisplayName": "全力射击", "ButtonImagePath": "BTN_MaxDamage1", "LapisId": "MaxDamage" ,"typeid":"7"}

];
var hotdata = [
{"DisplayName":"招财鞭炮","ButtonImagePath":"BTN_LuckyFirecracker","LapisId":"LuckyFirecracker"},
{"DisplayName":"摆脱","ButtonImagePath":"BTN_BreakAway1","LapisId":"BreakAway"},
{"DisplayName":"狂野马戏团","ButtonImagePath":"BTN_TwistedCircus","LapisId":"TheTwistedCircus"},
{"DisplayName":"海滨嘉年华","ButtonImagePath":"BTN_SpringBreak2","LapisId":"SpringBreak"},
{"DisplayName":"淑女之夜","ButtonImagePath":"BTN_LadiesNite5","LapisId":"LadiesNite"},
{"DisplayName":"浪漫传奇","ButtonImagePath":"BTN_ImmortalRomance1","LapisId":"ImmortalRomance"},
{"DisplayName":"红唇诱惑","ButtonImagePath":"BTN_RedHotDevil","LapisId":"RedHotDevil"},
{"DisplayName":"欧洲黄金21点","ButtonImagePath":"BTN_GoldSeries_EuroBlackjack1","LapisId":"EuropeanBJGold"},
{"DisplayName":"黄金时代","ButtonImagePath":"BTN_GoldenEra","LapisId":"GoldenEra"},
{"DisplayName":"宝石连线","ButtonImagePath":"BTN_ReelGems1","LapisId":"ReelGems"},
{"DisplayName":"花花公子","ButtonImagePath":"BTN_Playboy1","LapisId":"Playboy"},
{"DisplayName":"持枪王者","ButtonImagePath":"BTN_Pistoleras_ZH","LapisId":"Pistoleras"},
{"DisplayName":"糖果屋","ButtonImagePath":"BTN_somuchcandy","LapisId":"SoMuchCandy"},
{"DisplayName":"败家金猿","ButtonImagePath":"BTN_MoneyMadMonkey1","LapisId":"RubyMoneyMadMonkey"},
{"DisplayName":"格斗女王","ButtonImagePath":"BTN_RRHeartsAndTarts1","LapisId":"RRQueenOfHearts"},
{"DisplayName":"复古老虎机","ButtonImagePath":"BTN_RetroReels1","LapisId":"RetroReels"},
{"DisplayName":"新年快乐","ButtonImagePath":"BTN_HappyNewYear3","LapisId":"HappyNewYear"},
{"DisplayName":"银行破坏家","ButtonImagePath":"BTN_BreakDaBankAgain1","LapisId":"RubyBreakDaBankAgain"},
{"DisplayName":"星光之吻","ButtonImagePath":"BTN_StarlightKiss","LapisId":"StarlightKiss"},
{"DisplayName":"胸围银行","ButtonImagePath":"BTN_BustTheBank1","LapisId":"BustTheBank"},
{"DisplayName":"寿司大餐","ButtonImagePath":"BTN_somuchsushi","LapisId":"SoMuchSushi"},
{"DisplayName":"足球明星","ButtonImagePath":"BTN_footballstar1","LapisId":"FootballStar"},
{"DisplayName":"终极破坏","ButtonImagePath":"BTN_MaxDamageSlot","LapisId":"MaxDamageSlot"},
{"DisplayName":"百万美人鱼","ButtonImagePath":"BTN_MermaidsMillions1","LapisId":"MermaidsMillions"},
{"DisplayName":"冰原神枪女郎","ButtonImagePath":"BTN_GirlswithGuns2","LapisId":"GirlsWithGunsFrozenDawn"},
{"DisplayName":"空手道猪","ButtonImagePath":"BTN_KaratePig1","LapisId":"KaratePig"},
{"DisplayName":"三只小猪","ButtonImagePath":"BTN_PiggyFortunes","LapisId":"PiggyFortunes"},
{"DisplayName":"上流社会","ButtonImagePath":"BTN_HighSociety","LapisId":"HighSociety"},
{"DisplayName":"复古钻石老虎机","ButtonImagePath":"BTN_RRDiamondGlitz1","LapisId":"RetroReelsDiamondGlitz"},
{"DisplayName":"英伦时光","ButtonImagePath":"BTN_TallyHo1","LapisId":"TallyHo"},
{"DisplayName":"爱丽娜","ButtonImagePath":"BTN_Ariana_ZH","LapisId":"Ariana"},
{"DisplayName":"古墓丽影2","ButtonImagePath":"BTN_TombRaiderSotS1","LapisId":"RubyTombRaiderII"},
{"DisplayName":"神钓手","ButtonImagePath":"BTN_WildCatch","LapisId":"WildCatch"},
{"DisplayName":"黄金轮盘","ButtonImagePath":"BTN_GoldSeries_MultiWheelRoulette1","LapisId":"MultiWheelRouletteGold"},
{"DisplayName":"布希电报","ButtonImagePath":"BTN_BushTelegraph1","LapisId":"BushTelegraph"},
{"DisplayName":"怪兽总动员","ButtonImagePath":"BTN_somanymonsters","LapisId":"SoManyMonsters"},
{"DisplayName":"米加盗抢银行","ButtonImagePath":"BTN_BreakdaBankAgainMegaSpin1","LapisId":"MSBreakDaBankAgain"},
{"DisplayName":"龙宫","ButtonImagePath":"BTN_HouseofDragons","LapisId":"RubyHouseofDragons"},
{"DisplayName":"魔鬼終結者2","ButtonImagePath":"BTN_Terminator2_1","LapisId":"Terminator2"},
{"DisplayName":"海豚探秘","ButtonImagePath":"BTN_DolphinQuest","LapisId":"DolphinQuest"},
{"DisplayName":"海底狂欢","ButtonImagePath":"BTN_FishParty1","LapisId":"FishParty"},
{"DisplayName":"幸运女巫","ButtonImagePath":"BTN_LuckyWitch1","LapisId":"Luckywitch"},
{"DisplayName":"丛林枪神女郎","ButtonImagePath":"BTN_GirlswithGuns1","LapisId":"GirlsWithGuns"},
{"DisplayName":"幸运锦锂","ButtonImagePath":"BTN_LuckyKoi","LapisId":"LuckyKoi"},
{"DisplayName":"爱的收获","ButtonImagePath":"BTN_SweetHarvest1","LapisId":"SweetHarvest"},
{"DisplayName":"蛮荒野狼","ButtonImagePath":"BTN_CoolWolf1","LapisId":"CoolWolf"},
{"DisplayName":"杰克机器人","ButtonImagePath":"BTN_robojack1","LapisId":"RoboJack"},
{"DisplayName":"花蜜国度","ButtonImagePath":"BTN_PollenNation1","LapisId":"PollenNation"},
{"DisplayName":"暗恋","ButtonImagePath":"BTN_SecretAdmirer1","LapisId":"SecretAdmirer"},
{"DisplayName":"泡泡富矿","ButtonImagePath":"BTN_BubbleBonanza","LapisId":"bubblebonanza"},
{"DisplayName":"美女特务","ButtonImagePath":"BTN_AgentJaneBlonde7","LapisId":"RubyAgentJaneBlonde"},
{"DisplayName":"阿瓦隆","ButtonImagePath":"BTN_Avalon1","LapisId":"RubyAvalon"},
{"DisplayName":"黄金工厂","ButtonImagePath":"BTN_GoldFactory1","LapisId":"GoldFactory"},
{"DisplayName":"阿瓦隆圣杯传","ButtonImagePath":"BTN_AvalonII","LapisId":"Avalon2"},
{"DisplayName":"雷神索尔II","ButtonImagePath":"BTN_ThunderstruckTwo1","LapisId":"Thunderstruck2"},
{"DisplayName":"大航海时代","ButtonImagePath":"BTN_AgeofDiscovery3","LapisId":"RubyAgeOfDiscovery"},
{"DisplayName":"六角连线","ButtonImagePath":"BTN_Hexaline1","LapisId":"Hexaline"},
{"DisplayName":"白水牛","ButtonImagePath":"BTN_WhiteBuffalo","LapisId":"WhiteBuffalo"},
{"DisplayName":"火热老虎机","ButtonImagePath":"BTN_RRExtreme1","LapisId":"RetroReelsExtremeHeat"},
{"DisplayName":"燃烧的欲望","ButtonImagePath":"BTN_BurningDesire1","LapisId":"RubyBurningDesire"},
{"DisplayName":"冒险宫","ButtonImagePath":"BTN_AdventurePalaceHD","LapisId":"AdventurePalace"},
{"DisplayName":"黑暗骑士黎明崛起","ButtonImagePath":"BTN_TheDarkKnightRises","LapisId":"TheDarkKnightRises"},
{"DisplayName":"征服北极圈","ButtonImagePath":"BTN_ArcticAgents","LapisId":"ArcticAgents"},
{"DisplayName":"散打大炮","ButtonImagePath":"BTN_LooseCannon1","LapisId":"LooseCannon"},
{"DisplayName":"哲基尔和海德","ButtonImagePath":"BTN_JekyllandHyde","LapisId":"JekyllAndHyde"},
{"DisplayName":"人生经典","ButtonImagePath":"BTN_FinerReelsofLife","LapisId":"TheFinerReelsOfLife"},
{"DisplayName":"金豪大亨","ButtonImagePath":"BTN_RivieraRiches1","LapisId":"MonteCarloRiches"},
{"DisplayName":"魔术魅力","ButtonImagePath":"BTN_MagicCharms","LapisId":"MagicCharms"},
{"DisplayName":"银行破坏家","ButtonImagePath":"BTN_BreakDaBank1","LapisId":"BreakDaBank"},
{"DisplayName":"纯银3D","ButtonImagePath":"BTN_SterlingSilver3D1","LapisId":"SterlingSilver3d"},
{"DisplayName":"幸运饼乾","ButtonImagePath":"BTN_FortuneCookie7","LapisId":"FortuneCookie"},
{"DisplayName":"神秘格罗夫","ButtonImagePath":"BTN_MysticDreams1","LapisId":"MysticDreams"},
{"DisplayName":"金钱赢家","ButtonImagePath":"BTN_InItToWinIt","LapisId":"InItToWinIt"},
{"DisplayName":"虎熊争霸","ButtonImagePath":"BTN_TigerVsBear","LapisId":"TigerVsBear"},
{"DisplayName":"罪犯登记","ButtonImagePath":"BTN_MugshotMadness1","LapisId":"MugshotMadness"},
{"DisplayName":"阿拉斯加冰钓","ButtonImagePath":"BTN_AlaskanFishing1","LapisId":"AlaskanFishing"},
{"DisplayName":"四块组合","ButtonImagePath":"BTN_FourByFour1","LapisId":"FourByFour"},
{"DisplayName":"虎月","ButtonImagePath":"BTN_Tiger%20Moon","LapisId":"TigerMoon"},
{"DisplayName":"瓦特博士","ButtonImagePath":"BTN_DrWattsUp1","LapisId":"DrWattsUp"},
{"DisplayName":"求婚花嫁","ButtonImagePath":"BTN_BootyTime","LapisId":"Bridezilla"},
{"DisplayName":"西部边境","ButtonImagePath":"BTN_WesternFrontier","LapisId":"WesternFrontier"},
{"DisplayName":"星际","ButtonImagePath":"BTN_StarScape1","LapisId":"Starscape"},
{"DisplayName":"太阳探密","ButtonImagePath":"BTN_SunQuest3","LapisId":"SunQuest"},
{"DisplayName":"克斯赛车","ButtonImagePath":"BTN_RacingForPinks","LapisId":"RacingForPinks"},
{"DisplayName":"狂野冠军","ButtonImagePath":"BTN_WildChampions1","LapisId":"RubyWildChampions"},
{"DisplayName":"三重魔幻","ButtonImagePath":"BTN_TripleMagic1","LapisId":"TripleMagic"},
{"DisplayName":"矿怪连线","ButtonImagePath":"BTN_Germinator1","LapisId":"Germinator"},
{"DisplayName":"抢滩现金","ButtonImagePath":"BTN_RapidReels5","LapisId":"RubyRapidReels"},
{"DisplayName":"东洋宝石","ButtonImagePath":"BTN_JewelsoftheOrient1","LapisId":"JewelsOfTheOrient"},
{"DisplayName":"泰坦神庙","ButtonImagePath":"BTN_StashoftheTitans1","LapisId":"StashOfTheTitans"},
{"DisplayName":"暴走圣诞","ButtonImagePath":"BTN_SantasWildRide1","LapisId":"SantasWildRide"},
{"DisplayName":"奥林匹斯山","ButtonImagePath":"BTN_MountOlympus","LapisId":"MountOlympus"},
{"DisplayName":"帅哥晚宴","ButtonImagePath":"BTN_Harveys1","LapisId":"RubyHarveys"},
{"DisplayName":"神秘圣诞老人","ButtonImagePath":"BTN_SecretSanta","LapisId":"SecretSanta"},
{"DisplayName":"财富转轮","ButtonImagePath":"BTN_WheelOfWealth1","LapisId":"RubyWheelofWealth"},
{"DisplayName":"慵懒土豆","ButtonImagePath":"BTN_CouchPotato2","LapisId":"CouchPotato"},
{"DisplayName":"宝藏时间","ButtonImagePath":"BTN_BootyTime","LapisId":"BootyTime"},
{"DisplayName":"野性冠鹰","ButtonImagePath":"BTN_UntamedCrownedEagle","LapisId":"UntamedCrownedEagle"},
{"DisplayName":"捣蛋斑马","ButtonImagePath":"BTN_ZanyZebra1","LapisId":"zebra"},
{"DisplayName":"女巫财富","ButtonImagePath":"BTN_WitchesWealth1","LapisId":"RubyWitchesWealth"},
{"DisplayName":"三角魔组","ButtonImagePath":"BTN_Triangulation","LapisId":"Triangulation"},
{"DisplayName":"埃及王座","ButtonImagePath":"BTN_ThroneOfEgypt1","LapisId":"throneofegypt"},
{"DisplayName":"真假公主","ButtonImagePath":"BTN_TheLostPrincessAnastasia","LapisId":"TheLostPrincessAnastasia"},
{"DisplayName":"幸运数字","ButtonImagePath":"BTN_LuckyNumbers1","LapisId":"RubyLuckyNumbers"},
{"DisplayName":"巨富遗产","ButtonImagePath":"BTN_Legacy2","LapisId":"RubyLegacy"},
{"DisplayName":"地狱劲妈","ButtonImagePath":"BTN_HellGrannies","LapisId":"HellsGrannies"},
{"DisplayName":"孟加拉虎","ButtonImagePath":"BTN_UntamedBengalTiger1","LapisId":"UntamedBengalTiger"},
{"DisplayName":"拉斯维加斯21点","ButtonImagePath":"BTN_VegasStripBlackjack1","LapisId":"VegasStrip"},
{"DisplayName":"现金蚌","ButtonImagePath":"BTN_CashClams2","LapisId":"CashClams"},
{"DisplayName":"马戏团","ButtonImagePath":"BTN_BigTop1","LapisId":"BigTop"},
{"DisplayName":"鼠帮","ButtonImagePath":"BTN_RatPack1","LapisId":"RubyTheRatPack"},
{"DisplayName":"海底宝物","ButtonImagePath":"BTN_PlundertheSea1","LapisId":"RubyPlunderTheSea"},
{"DisplayName":"法老王宝石","ButtonImagePath":"BTN_PharaohBingo1","LapisId":"PharaohBingo"},
{"DisplayName":"麻法世界","ButtonImagePath":"BTN_Moonshine1","LapisId":"Moonshine"},
{"DisplayName":"加德满都","ButtonImagePath":"BTN_Kathmandu1","LapisId":"RubyKathmandu"},
{"DisplayName":"水果丛林","ButtonImagePath":"BTN_BigKahuna1","LapisId":"BigKahuna"},
{"DisplayName":"银河战舰","ButtonImagePath":"BTN_Galacticons","LapisId":"Galacticons"},
{"DisplayName":"图腾宝藏","ButtonImagePath":"BTN_TotemTreasure1","LapisId":"RubyTotemTreasure"},
{"DisplayName":"动物足球大赛","ButtonImagePath":"BTN_SoccerSafari1","LapisId":"RubySoccerSafari"},
{"DisplayName":"发射！","ButtonImagePath":"BTN_Shoot","LapisId":"Shoot"},
{"DisplayName":"水果老虎机","ButtonImagePath":"BTN_FruitSlots1","LapisId":"fruits"},
{"DisplayName":"森林酋长","ButtonImagePath":"BTN_BigKahuna_SL1","LapisId":"RubyBigKahunaSnakesAndLadders"},
{"DisplayName":"野性大猫熊","ButtonImagePath":"BTN_UntamedGiantPanda","LapisId":"UntamedGiantPanda"},
{"DisplayName":"罗马宝藏","ButtonImagePath":"BTN_RomanRiches11","LapisId":"romanriches"},
{"DisplayName":"发现天堂","ButtonImagePath":"BTN_OrientalFortune2","LapisId":"OrientalFortune"},
{"DisplayName":"地狱怪客","ButtonImagePath":"BTN_HellBoy1","LapisId":"RubyHellBoy"},
{"DisplayName":"全力射击","ButtonImagePath":"BTN_MaxDamage1","LapisId":"MaxDamage"},
{"DisplayName":"财气轮盘特版","ButtonImagePath":"BTN_WheelofWealthSE1","LapisId":"WheelOfWealthSE"},
{"DisplayName":"蒸汽朋克英雄","ButtonImagePath":"BTN_SteamPunkHeroes","LapisId":"SteamPunkHeroes"},
{"DisplayName":"梦奇金","ButtonImagePath":"BTN_Munchkins2","LapisId":"RubyMunchkins"},
{"DisplayName":"辣妹纹身","ButtonImagePath":"BTN_HotInk1","LapisId":"HotInk"},
{"DisplayName":"神奇轮转7s","ButtonImagePath":"BTN_FantasticSevens1","LapisId":"fan7"},
{"DisplayName":"野生探险","ButtonImagePath":"BTN_AdventurePalace4","LapisId":"RubyAdventurePalace"},
{"DisplayName":"北洋财富","ButtonImagePath":"BTN_ArcticFortune1","LapisId":"ArcticFortune"},
{"DisplayName":"心跳时刻","ButtonImagePath":"BTN_VinylCountdown4","LapisId":"VinylCountDown"},
{"DisplayName":"维多利亚时代雅贼","ButtonImagePath":"BTN_VictorianVillian","LapisId":"VictorianVillain"},
{"DisplayName":"古墓丽影","ButtonImagePath":"BTN_TombRaider2","LapisId":"TombRaider"},
{"DisplayName":"华丽剧场","ButtonImagePath":"BTN_SpectacularWheelOfWealth1","LapisId":"Spectacular"},
{"DisplayName":"水果宾果","ButtonImagePath":"BTN_FruitBingo1","LapisId":"RubyFruitBingo"},
{"DisplayName":"幸运小妖精战利品","ButtonImagePath":"BTN_LuckyLeprechaunsLoot","LapisId":"LuckyLeprechaunsLoot"},
{"DisplayName":"忍者龟","ButtonImagePath":"BTN_TurtleyAwesome1","LapisId":"RubyTurtleyAwesome"},
{"DisplayName":"神秘格罗夫","ButtonImagePath":"BTN_MystiqueGrove","LapisId":"MystiqueGrove"},
{"DisplayName":"时来运转","ButtonImagePath":"BTN_InstantWinCardSelector1","LapisId":"IWCardSelector"},
{"DisplayName":"捉鬼大队","ButtonImagePath":"BTN_ParadiseFound","LapisId":"ParadiseFound"},
{"DisplayName":"摇滚时代","ButtonImagePath":"BTN_TheOsbournes1","LapisId":"RubyTheOsbournes"},
{"DisplayName":"超级女警","ButtonImagePath":"BTN_SuperZeroes1","LapisId":"RubySuperZeroes"},
{"DisplayName":"速度滑轮","ButtonImagePath":"BTN_RollerDerby","LapisId":"RollerDerby"},
{"DisplayName":"老国王科尔","ButtonImagePath":"BTN_RRKing1","LapisId":"RROldKingCole"},
{"DisplayName":"海洋宝藏","ButtonImagePath":"BTN_LeaguesOfFortune1","LapisId":"LeaguesOfFortune"},
{"DisplayName":"红衣女郎","ButtonImagePath":"BTN_LadyInRed2","LapisId":"LadyInRed"},
{"DisplayName":"国王现金","ButtonImagePath":"BTN_KingsofCash1","LapisId":"KingsOfCash"},
{"DisplayName":"电动宾果","ButtonImagePath":"BTN_ElectroBingo1","LapisId":"ElectroBingo"},
{"DisplayName":"桥牌老虎机","ButtonImagePath":"BTN_JoyOfSix","LapisId":"JoyOfSix"},
{"DisplayName":"高速扑克","ButtonImagePath":"BTN_GoldSeries_MHHighSpeedPoker1","LapisId":"HighSpeedPoker"},
{"DisplayName":"猫头鹰","ButtonImagePath":"BTN_WhataHoot3","LapisId":"WhatAHoot"},
{"DisplayName":"冲浪野生动物园","ButtonImagePath":"BTN_SurfSafari","LapisId":"SurfSafari"},
{"DisplayName":"超级灌篮","ButtonImagePath":"BTN_SlamFunk1","LapisId":"RubySlamFunk"},
{"DisplayName":"马雅公主","ButtonImagePath":"BTN_MayanPrincess1","LapisId":"MayanPrincess"},
{"DisplayName":"欧洲21点","ButtonImagePath":"BTN_EuroBlackjackHighLimit1","LapisId":"HLEuropeanAdvBJ"},
{"DisplayName":"耶诞礼物大厅","ButtonImagePath":"BTN_DeckTheHalls1","LapisId":"RubyDeckTheHalls"},
{"DisplayName":"疯狂现金","ButtonImagePath":"BTN_CashCrazy9","LapisId":"cashcrazy"},
{"DisplayName":"东方美人","ButtonImagePath":"BTN_AsianBeauty1","LapisId":"AsianBeauty"},
{"DisplayName":"打地鼠","ButtonImagePath":"BTN_WhackaJackpot1","LapisId":"RubyWhackAJackpot"},
{"DisplayName":"嘻哈摇滚","ButtonImagePath":"BTN_Loaded1","LapisId":"Loaded"},
{"DisplayName":"亚瑟王","ButtonImagePath":"BTN_KingArthur","LapisId":"RubyKingArthur"},
{"DisplayName":"杀手","ButtonImagePath":"BTN_Hitman1","LapisId":"RubyHitman"},
{"DisplayName":"万圣狂欢","ButtonImagePath":"BTN_Halloweenies1","LapisId":"RubyHalloweenies"},
{"DisplayName":"完美起跑","ButtonImagePath":"BTN_GoodToGo2","LapisId":"RubyGoodToGo"},
{"DisplayName":"圣龙赐福","ButtonImagePath":"BTN_DragonsFortune1","LapisId":"RubyDragonsFortune"},
{"DisplayName":"现金虫欢","ButtonImagePath":"BTN_Cashapillar1","LapisId":"RubyCashapillar"},
{"DisplayName":"法老宾果","ButtonImagePath":"BTN_PhantomCash","LapisId":"PhantomCash"},
{"DisplayName":"章鱼","ButtonImagePath":"BTN_Octopays","LapisId":"Octopays"},
{"DisplayName":"地球危机","ButtonImagePath":"BTN_WhatonEarth1","LapisId":"WhatonEarth"},
{"DisplayName":"超级马力","ButtonImagePath":"BTN_SupeItUp2","LapisId":"RubySupeItUp"},
{"DisplayName":"马雅宾果","ButtonImagePath":"BTN_MayanBingo1","LapisId":"MayanBingo"},
{"DisplayName":"小丑扑克","ButtonImagePath":"BTN_JokerPokerPowerPoker1","LapisId":"JokerPwrPoker"},
{"DisplayName":"特快派彩","ButtonImagePath":"BTN_JackpotExpress1","LapisId":"jexpress"},
{"DisplayName":"黄金海岸","ButtonImagePath":"BTN_GoldCoast3","LapisId":"RubyGoldCoast"},
{"DisplayName":"丢冰球","ButtonImagePath":"BTN_FreezingFuzzballs1","LapisId":"RubyFreezingFuzzballs"},
{"DisplayName":"老鹰之翼","ButtonImagePath":"BTN_EaglesWings1","LapisId":"EaglesWings"},
{"DisplayName":"黄金墓穴探险","ButtonImagePath":"BTN_CryptCrusadeGold1","LapisId":"RubyCryptCrusadeGold"},
{"DisplayName":"现金虫刮刮卡","ButtonImagePath":"BTN_Cashapillar1","LapisId":"RubyIWCashapillar"},
{"DisplayName":"猴王大餐2","ButtonImagePath":"BTN_InstantWin_BigBreak1","LapisId":"RubyIWBigBreak"},
{"DisplayName":"梦魇艾莉丝仙境","ButtonImagePath":"BTN_AlaxeInZombieland","LapisId":"AlaxeInZombieland"},
{"DisplayName":"旋转快餐","ButtonImagePath":"BTN_5ReelDrive1","LapisId":"5ReelDrive"},
{"DisplayName":"太空特務","ButtonImagePath":"BTN_BattlestarGalactica1","LapisId":"BattlestarGalactica"},
{"DisplayName":"视频扑克2","ButtonImagePath":"BTN_JacksorBetterPowerPoker1","LapisId":"Jackspwrpoker"},
{"DisplayName":"寿司大战","ButtonImagePath":"BTN_WasabiSan4","LapisId":"RubyWasabiSan"},
{"DisplayName":"拉斯维加斯黄金21点","ButtonImagePath":"BTN_GoldSeries_VegasStrip1","LapisId":"RubyVegasStripBlackjackGold"},
{"DisplayName":"超级乐21点","ButtonImagePath":"BTN_SuperFun21Blackjack1","LapisId":"SuperFun21"},
{"DisplayName":"超级奖金宾果","ButtonImagePath":"BTN_SuperBonusBingo1","LapisId":"RubySuperBonusBingo"},
{"DisplayName":"顶尖房仲","ButtonImagePath":"BTN_PrimeProperty1","LapisId":"RubyPrimeProperty"},
{"DisplayName":"选球好手","ButtonImagePath":"BTN_PharoahsGems1","LapisId":"RubyPharaohsGems"},
{"DisplayName":"魔术箱","ButtonImagePath":"BTN_MagicBoxes","LapisId":"MagicBoxes"},
{"DisplayName":"侏罗纪彩金","ButtonImagePath":"BTN_JurassicJackpot1","LapisId":"jurassicjackpot"},
{"DisplayName":"肉搏战","ButtonImagePath":"BTN_HandToHandCombat1","LapisId":"RubyHandToHandCombat"},
{"DisplayName":"錢進泡泡","ButtonImagePath":"BTN_FoamyFortunes1","LapisId":"RubyFoamyFortunes"},
{"DisplayName":"狗老大","ButtonImagePath":"BTN_Dogfather2","LapisId":"RubyDogfather"},
{"DisplayName":"网球中心","ButtonImagePath":"BTN_CentreCourt1","LapisId":"RubyCentreCourt"},
{"DisplayName":"美式轮盘","ButtonImagePath":"BTN_USRoulette1","LapisId":"AmericanRoulette"},
{"DisplayName":"尖子和花牌","ButtonImagePath":"BTN_AcesAndEights1","LapisId":"RubyAcesAndEights"},
{"DisplayName":"双重王牌2","ButtonImagePath":"BTN_DoubleJoker1","LapisId":"DoubleJoker"},
{"DisplayName":"七海霸权","ButtonImagePath":"BTN_SovereignoftheSevenSeas","LapisId":"SovereignOfTheSevenSeas"},
{"DisplayName":"三重扑克","ButtonImagePath":"BTN_GoldSeries_TriplePocket1","LapisId":"RubyTriplePocketPoker"},
{"DisplayName":"王者巫师","ButtonImagePath":"BTN_WinningWizards1","LapisId":"wwizards"},
{"DisplayName":"拉斯维加斯獨家黄金21点","ButtonImagePath":"BTN_GoldSeries_VegasSingleDeck1","LapisId":"RubyVegasSingleDeckBlackjackGold"},
{"DisplayName":"尖子威力扑克","ButtonImagePath":"BTN_TensOrBetterPowerPoker1","LapisId":"TensorBetterPwrPoker"},
{"DisplayName":"旋转魔球","ButtonImagePath":"BTN_Spingo","LapisId":"Spingo"},
{"DisplayName":"西部抢金","ButtonImagePath":"BTN_SixShooterLooterGold1","LapisId":"RubySixShooterLooter"},
{"DisplayName":"小丑扑克2","ButtonImagePath":"BTN_JokerPoker1","LapisId":"Jokerpok"},
{"DisplayName":"美发师","ButtonImagePath":"BTN_HairyFairies1","LapisId":"RubyHairyFairies"},
{"DisplayName":"双钻宝","ButtonImagePath":"BTN_DoubleWammy1","LapisId":"DoubleWammy"},
{"DisplayName":"土豪金","ButtonImagePath":"BTN_Cashville1","LapisId":"Cashville"},
{"DisplayName":"狂野奖金","ButtonImagePath":"BTN_BonusDeucesWild1","LapisId":"RubyBonusDeucesWild"},
{"DisplayName":"路易斯扑克","ButtonImagePath":"BTN_LouisianaDouble1","LapisId":"LouisianaDouble"},
{"DisplayName":"神鬼奇航","ButtonImagePath":"BTN_SkullDuggery1","LapisId":"SkullDuggery"},
{"DisplayName":"银坊","ButtonImagePath":"BTN_SilverFang1","LapisId":"SilverFang"},
{"DisplayName":"水岸摇滚秀","ButtonImagePath":"BTN_RocktheBoat18","LapisId":"RockTheBoat"},
{"DisplayName":"海上驱动","ButtonImagePath":"BTN_ReelStrike1","LapisId":"ReelStrike"},
{"DisplayName":"足球射门","ButtonImagePath":"BTN_OffsideandSeek1","LapisId":"RubyOffsideAndSeek"},
{"DisplayName":"孟买魔术师","ButtonImagePath":"BTN_MumbaiMagic1","LapisId":"RubyMumbaiMagic"},
{"DisplayName":"棒球热","ButtonImagePath":"BTN_HotShot3","LapisId":"RubyHotShot"},
{"DisplayName":"万圣狂欢2","ButtonImagePath":"BTN_Halloweenies1","LapisId":"RubyIWHalloweenies"},
{"DisplayName":"赛车老奶奶","ButtonImagePath":"BTN_GrannyPrix1","LapisId":"RubyGrannyPrix"},
{"DisplayName":"僵尸家族","ButtonImagePath":"BTN_GoldenGhouls1","LapisId":"RubyGoldenGhouls"},
{"DisplayName":"金龙","ButtonImagePath":"BTN_GoldenDragon6","LapisId":"gdragon"},
{"DisplayName":"法国轮盘","ButtonImagePath":"BTN_FrenchRoulette1","LapisId":"FrenchRoulette"},
{"DisplayName":"胖女士考辛斯","ButtonImagePath":"BTN_FatLadySings1","LapisId":"FatLadySings"},
{"DisplayName":"伯爵功绩","ButtonImagePath":"BTN_Cashanova1","LapisId":"Cashanova"},
{"DisplayName":"狂欢节","ButtonImagePath":"BTN_Carnaval2","LapisId":"Carnaval"},
{"DisplayName":"滚球王","ButtonImagePath":"BTN_BowledOver1","LapisId":"RubyBowledOver"},
{"DisplayName":"宾果富矿","ButtonImagePath":"BTN_BingoBonanza1","LapisId":"RubyBingoBonanza"},
{"DisplayName":"推啤酒大赛","ButtonImagePath":"BTN_BeerFest1","LapisId":"RubyBeerFest"},
{"DisplayName":"百家乐","ButtonImagePath":"BTN_Baccarat2","LapisId":"Baccarat"},
{"DisplayName":"尖子威力扑克","ButtonImagePath":"BTN_TensOrBetter1","LapisId":"TensorBetter"},
{"DisplayName":"森巴宾果","ButtonImagePath":"BTN_SambaBingo1","LapisId":"SambaBingo"},
{"DisplayName":"速食轮动","ButtonImagePath":"BTN_ReelThunder2","LapisId":"ReelThunder"},
{"DisplayName":"黄金欧式21点","ButtonImagePath":"BTN_GoldSeries_MHEuroBlackjack2","LapisId":"MHEuropeanBJGold"},
{"DisplayName":"疯狂帽商","ButtonImagePath":"BTN_MadHatters1","LapisId":"MadHatters"},
{"DisplayName":"叮叮当","ButtonImagePath":"BTN_JingleBells6","LapisId":"RubyJingleBells"},
{"DisplayName":"视频扑克","ButtonImagePath":"BTN_JacksOrBetter1","LapisId":"jacks"},
{"DisplayName":"伟大狮鹫","ButtonImagePath":"BTN_GreatGriffin1","LapisId":"GreatGriffin"},
{"DisplayName":"黄金鼠","ButtonImagePath":"BTN_GopherGold2","LapisId":"GopherGold"},
{"DisplayName":"网球刮刮乐","ButtonImagePath":"BTN_GameSetandScratch1","LapisId":"RubyGameSetAndScratch"},
{"DisplayName":"疯狂八零年代","ButtonImagePath":"BTN_Crazy80s4","LapisId":"RubyCrazy80s"},
{"DisplayName":"兔子跳跳跳","ButtonImagePath":"BTN_BunnyBoilerGold1","LapisId":"RubyBunnyBoilerGold"},
{"DisplayName":"西班牙黄金21点","ButtonImagePath":"BTN_GoldSeries_SpanishBlackjack1","LapisId":"RubySpanishBlackjackGold"},
{"DisplayName":"西班牙21点","ButtonImagePath":"BTN_SpanishBlackjack1","LapisId":"Spanish"},
{"DisplayName":"守财奴","ButtonImagePath":"BTN_Scrooge1","LapisId":"RubyScrooge"},
{"DisplayName":"总理赛马","ButtonImagePath":"BTN_PremierTrotting1","LapisId":"PremierTrotting"},
{"DisplayName":"总理钻石轮盘","ButtonImagePath":"BTN_PremierRouletteDE1","LapisId":"PremierRouletteDE"},
{"DisplayName":"神踪扑克","ButtonImagePath":"BTN_PokerPursuit1","LapisId":"PokerPursuit"},
{"DisplayName":"维加斯到地21点","ButtonImagePath":"BTN_GoldSeries_MHVegasDowntown2","LapisId":"RubyMultiVegasDowntownBlackjackGold"},
{"DisplayName":"怪兽狂热","ButtonImagePath":"BTN_MonsterMania1","LapisId":"MonsterMania"},
{"DisplayName":"快乐彩","ButtonImagePath":"BTN_Keno3","LapisId":"Keno"},
{"DisplayName":"圣诞礼物","ButtonImagePath":"BTN_GiftRap2","LapisId":"GiftRap"},
{"DisplayName":"神灯精灵宝石","ButtonImagePath":"BTN_GeniesGems2","LapisId":"geniesgems"},
{"DisplayName":"欧洲黄金轮盘","ButtonImagePath":"BTN_GoldSeries_EuroRoulette1","LapisId":"EuroRouletteGold"},
{"DisplayName":"魔法森林","ButtonImagePath":"BTN_EnchantedWoods1","LapisId":"EnchantedWoods"},
{"DisplayName":"双重魔幻","ButtonImagePath":"BTN_DoubleMagic1","LapisId":"dm"},
{"DisplayName":"双重王牌","ButtonImagePath":"BTN_DoubleJokerPowerPoker1","LapisId":"DoubleJokerPwrPoker"},
{"DisplayName":"野蛮对决2","ButtonImagePath":"BTN_DeucesWild1","LapisId":"deuceswi"},
{"DisplayName":"狂野扑克","ButtonImagePath":"BTN_DeucesJokerPowerPoker1","LapisId":"Deuces%26JokerPwrPoker"},
{"DisplayName":"网络扑克","ButtonImagePath":"BTN_Cyberstud1","LapisId":"Cyberstud"},
{"DisplayName":"墓穴探险","ButtonImagePath":"BTN_CryptCrusade1","LapisId":"RubyCryptCrusade"},
{"DisplayName":"奖金扑克","ButtonImagePath":"BTN_BonusPoker1","LapisId":"RubyBonusPoker"},
{"DisplayName":"美味餐厅","ButtonImagePath":"BTN_Belissimo17","LapisId":"Belissimo"},
{"DisplayName":"王牌扑克","ButtonImagePath":"BTN_AllAces1","LapisId":"RubyAllAces"},
{"DisplayName":"野性狼原","ButtonImagePath":"BTN_UntamedWolfPack1","LapisId":"UntamedWolfPack"},
{"DisplayName":"总理轮盘","ButtonImagePath":"BTN_PremierRoulette9","LapisId":"RubyPremierRoulette"},
{"DisplayName":"总理连胜21点","ButtonImagePath":"BTN_PremierBlackjackHiStreak1","LapisId":"RubyPBJHighStreak"},
{"DisplayName":"完美对子21点","ButtonImagePath":"BTN_GoldSeries_MHPerfectPairsBlackjack1","LapisId":"RubyMHPerfectPairs"},
{"DisplayName":"欢乐圣诞","ButtonImagePath":"BTN_Hohoho1","LapisId":"HoHoHo"},
{"DisplayName":"黄金13欧洲21点","ButtonImagePath":"BTN_GoldSeries_HiLo13EuroBJ1","LapisId":"RubyHiLoBlackjackGold"},
{"DisplayName":"飞行王牌","ButtonImagePath":"BTN_FlyingAce2","LapisId":"RubyFlyingAce"},
{"DisplayName":"星际战争","ButtonImagePath":"BTN_DroneWars","LapisId":"DroneWars"},
{"DisplayName":"黄金双冒险翻牌","ButtonImagePath":"BTN_GoldSeries_DoubleExposureBlackjack1","LapisId":"RubyDoubleExposureBlackjackGold"},
{"DisplayName":"僵尸面包","ButtonImagePath":"BTN_DawnoftheBread1","LapisId":"RubyDawnOfTheBread"},
{"DisplayName":"疯狂变色龙","ButtonImagePath":"BTN_CrazyChameleons1","LapisId":"CrazyChameleons"},
{"DisplayName":"酷巴克","ButtonImagePath":"BTN_CoolBuck1","LapisId":"coolbuck"},
{"DisplayName":"兔子锅炉","ButtonImagePath":"BTN_BunnyBoiler1","LapisId":"RubyBunnyBoiler"},
{"DisplayName":"豪华奖金扑克","ButtonImagePath":"BTN_BonusPokerDeluxe1","LapisId":"RubyBonusPokerDeluxe"},
{"DisplayName":"猴王大餐","ButtonImagePath":"BTN_BigBreak1","LapisId":"RubyBigBreak"},
{"DisplayName":"黄金百家乐","ButtonImagePath":"BTN_GoldSeries_Baccarat1","LapisId":"BaccaratGold"},
{"DisplayName":"西洋城黄金21点","ButtonImagePath":"BTN_GoldSeries_AtlanticCityBlackjack1","LapisId":"AtlanticCityBJGold"},
{"DisplayName":"尖子扑克","ButtonImagePath":"BTN_AcesFaces1","LapisId":"acesfaces"},
{"DisplayName":"视讯尖子扑克","ButtonImagePath":"BTN_AcesFacesPowerPoker1","LapisId":"AcesfacesPwrPoker"},
{"DisplayName":"黄金太空探险","ButtonImagePath":"BTN_SpaceEvaderGold1","LapisId":"RubySpaceEvaderGold"},
{"DisplayName":"太空探险","ButtonImagePath":"BTN_SpaceEvader1","LapisId":"RubySpaceEvader"},
{"DisplayName":"皇家赛马","ButtonImagePath":"BTN_PremierRacing1","LapisId":"PremierRacing"},
{"DisplayName":"总理欧元21点","ButtonImagePath":"BTN_GoldSeries_MHBonusBlackjack2","LapisId":"RubyPBJMultiHandBonus"},
{"DisplayName":"高低黄金21点","ButtonImagePath":"BTN_PremierBlackjackHiLo1","LapisId":"RubyPBJHiLo"},
{"DisplayName":"尊爵21点","ButtonImagePath":"BTN_MHPremierBlackjack1","LapisId":"RubyPBJMultiHand"},
{"DisplayName":"极限百家乐","ButtonImagePath":"BTN_HighLimitBaccarat1","LapisId":"HighLimitBaccarat"},
{"DisplayName":"元素精灵","ButtonImagePath":"BTN_Elementals2","LapisId":"rubyelementals"},
{"DisplayName":"双威王牌","ButtonImagePath":"BTN_DoubleDoubleBonus1","LapisId":"DoubleDoubleBonus"},
{"DisplayName":"双倍红利扑克","ButtonImagePath":"BTN_DoubleBonus1","LapisId":"RubyDoubleBonusPoker"},
{"DisplayName":"恐龙世界","ButtonImagePath":"BTN_DinoMight1","LapisId":"DinoMight"},
{"DisplayName":"野蛮对决","ButtonImagePath":"BTN_DeucesWildPowerPoker1","LapisId":"DeucesWildPwrPoker"},
{"DisplayName":"狂野扑克2","ButtonImagePath":"BTN_DeucesJoker1","LapisId":"DeucesandJoker"},
{"DisplayName":"王冠与锚","ButtonImagePath":"BTN_CrownAndAnchor1","LapisId":"CrownAndAnchor"},
{"DisplayName":"宇宙霹雳猫","ButtonImagePath":"BTN_CosmicCat1","LapisId":"cosmicc"},
{"DisplayName":"5大赛黄金21点","ButtonImagePath":"BTN_GoldSeries_BigFive1","LapisId":"RubyBigFiveBlackjackGold"},
{"DisplayName":"美式扑克","ButtonImagePath":"BTN_AllAmerican1","LapisId":"RubyAllAmerican"},
{"DisplayName":"地府烈焰","ButtonImagePath":"BTN_HotAsHades_ZH","LapisId":"HotAsHades"},
{"DisplayName":"厨神","ButtonImagePath":"BTN_BigChef_ZH","LapisId":"BigChef"},
{"DisplayName":"阿瓦隆","ButtonImagePath":"BTN_AvalonHD_ZH","LapisId":"Avalon"},
{"DisplayName":"金公主","ButtonImagePath":"BTN_GoldenPrincess","LapisId":"GoldenPrincess"},
{"DisplayName":"兔子帽子","ButtonImagePath":"BTN_RabbitintheHat","LapisId":"RabbitInTheHat"},
{"DisplayName":"幸运小妖精","ButtonImagePath":"BTN_LuckyLeprechaun","LapisId":"LuckyLeprechaun"},
{"DisplayName":"凯蒂卡巴拉","ButtonImagePath":"BTN_KittyCabana_ZH","LapisId":"KittyCabana"},
{"DisplayName":"锁子甲","ButtonImagePath":"BTN_ChainMail_ZH","LapisId":"ChainMailNew"},
{"DisplayName":"酷犬酒店","ButtonImagePath":"BTN_HoundHotel_ZH","LapisId":"HoundHotel"}
];

$(function(){
	getdata(0);
	
})

function search(keywords) {
    $("#gamelist").html("");
    var game = "";
    var mgame = "";
    var userid = $("#userid").val();
    var count = 0;
    $.each(data, function (index, content) {
        var key = content.DisplayName;
        if (key.indexOf(keywords) + 1 > 0) {
            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName + "</div><div class=\"game_logo\" onclick=\"toMG('" + content.LapisId + "')\"><img src=\"common/template/third/egame/images/" + content.ButtonImagePath + ".png\"></div></li>";
            count++;
        } else {
            game += "没有搜索到相关游戏";
        }
    });
    $("#gamelist").html(game);
    var allpage = Math.ceil(count / 15);
    gamelist(allpage, game);
}

function getdata(type) {
    $("#gamelist").html("");
	var game = "";
	var mgame = "";
	var userid = $("#userid").val();
	var count = 0;
	var database = [];
	if (type == 8) {
	    database = hotdata;
	    type = "0";
	} else {
	    database = data;
	}
	$.each(database, function (index, content)
	{
	    var gametype = content.typeid;
	    if (type == "123") {
	        if (gametype == "1" || gametype == "2" || gametype == "3") {
	            game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">" + content.DisplayName + "</div><div class=\"game_logo\" onclick=\"toMG('" + content.LapisId + "')\"><img src=\"common/template/third/egame/images/" + content.ButtonImagePath + ".png\"></div></li>";
	            count++;
	        }
	    }
	    if (type == gametype || type == '0' || gametype.indexOf(type) + 1 > 0) {
		    game += "<li class=\"ng-scope\"><div class=\"game_text ng-binding\">"+content.DisplayName+"</div><div class=\"game_logo\" onclick=\"toMG('"+content.LapisId+"')\"><img src=\"common/template/third/egame/images/"+content.ButtonImagePath+".png\"></div></li>";
		    count++;
	    }
	});
	$("#gamelist").html(game);
	var allpage = Math.ceil(count / 15);
	gamelist(allpage, game);
}
function gamelist(allpage, game) {
    $("#gamelists").html("");
    for (var i = 0; i < allpage; i++) {
        var id = "gamepage" + i;
        $("#gamelists").append("<ul id='" + id + "' class='gamelist' style='display:none'></ul>");
    }
    $("#gamelist li").each(function (i) {
        var id = "gamepage" + parseInt(i / 15);
        $(this).appendTo("#" + id);
    });
    $("#gamelist").html("");
    $("#pageCount").html(allpage);
    $("#StartPage").click(function () {
        topage(0, allpage);
    });
    $("#EndPage").click(function () {
        topage(allpage-1, allpage);
    });
    var curi = 0;
    topage(curi, allpage);
    $("#NextPage").bind("click", function () {
        curi++;
        if (curi >= allpage) {
            curi = eval(allpage-1);
        }
        topage(curi, allpage);
    });
    $("#PrevPage").bind("click", function () {
        curi--;
        if (curi <= 0) {
            curi = 0;
        }
        topage(curi, allpage);
    });
    
}
function topage(pageid,totalpage){
    $("#gamelists ul").hide();
    $("#gamepage" + pageid).show();
    var curpage = pageid + 1;
    $("#currentPage").html(curpage);
    
}
function prepage(pageid,totalpage){
    topage(pageid, totalpage);
}
function nextpage(pageid,totalpage){
    topage(pageid,totalpage);
}

(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					alert("[" + opt.url + "] 404 not found");
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					alert("没有权限");
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
})(jQuery);

function toMG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;

    var userid = $("#userid").val();
    if (!userid) {
        alert("您需要先登录才能游戏");
    }else{
    	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
    	$.ajax({
			url : "../forwardMg.do",
			sync : false,
			data:{
				gameType : 3,
				gameid : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
    }
}

function toAG(gameid) {
    var sw = '';
    csw = '';
    sh = '';
    csh = '';
    ctop = '';
    cleft = '';

    sw = $(window.parent).width();
    sh = $(window.parent).height();
    csw = $(window.parent).width();
    csh = $(window.parent).height();
    ctop = 0;
    cleft = 0;

    var userid = $("#userid").val();
    if (userid =="") {
        alert("您需要先登录才能游戏");
    }else{
    	var windowOpen= window.open("", '_blank', 'width=' + csw + ',height=' + csh + ',left=' + cleft + ',top=' + ctop + ',scrollbars=no,location=1,resizable=yes');
    	$.ajax({
			url : "../forwardAg.do",
			sync : false,
			data:{
				h5 : 0,
				gameType : gameid
			},
			success : function(json) {
				if (json.success) {
					windowOpen.location.href=json.url;
				} else {
					alert(json.msg);
					return;
				}
			}
		});
    }
}