<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- saved from url=(0030)https://988158.com/liveTest.do -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${_title }</title>
	<link rel="stylesheet" href="${base}/common/template/third/newLiveV6/css/reset.css">
	<link rel="stylesheet" href="${base}/common/template/third/newLiveV6/css/index.css">
	<script src="${base}/common/template/third/newLiveV6/js/jquery.min.js"></script>
	<script src="${base}/common/template/third/newLiveV6/js/slide.js"></script>
</head>
<body>
	<div class="w1000">
		<div class="liveMain">
			<div class="logo-slide">
				<div class="logo-bd">
					<div class="tempWrap" style="overflow:hidden; position:relative; width:870px"><ul style="width: 2436px; position: relative; overflow: hidden; padding: 0px; margin: 0px; left: -1044px;">
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardAg.do', '1');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_01.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardBbin.do?type=live', '2');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_02.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardMg.do?gameType=1', '3');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_03.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardAb.do', '5');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_04.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardDs.do?gameType=1', '8');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_05.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardOg.do?gameType=desktop', '7');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_06.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardBg.do?type=2', '98');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_08.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardVr.do', '97');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_09.jpg">
							</a>
						</li>
						<c:if test="${isPtOnOff eq 'on'}">
						<li style="float: left; width: 173px;">
							<a href="#" onclick="location.href='${base}/index/otherLive.do'">
								<img src="${base}/common/template/third/newLiveV6/images/logo_11.jpg">
							</a>
						</li>
						</c:if>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardEbet.do?gameType=5', '13');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_10.jpg">
							</a>
						</li>
						
					<li class="clone" style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardAg.do', '1');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_01.jpg">
							</a>
						</li><li class="clone" style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardBbin.do?type=live', '2');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_02.jpg">
							</a>
						</li><li class="clone" style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardMg.do?gameType=1', '3');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_03.jpg">
							</a>
						</li><li class="clone" style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardAb.do', '5');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_04.jpg">
							</a>
						</li><li class="clone" style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardDs.do?gameType=1', '8');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_05.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardOg.do?gameType=desktop', '7');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_06.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardBg.do?type=2', '98');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_08.jpg">
							</a>
						</li>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardVr.do', '97');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_09.jpg">
							</a>
						</li>
						<c:if test="${isPtOnOff eq 'on'}">
						<li style="float: left; width: 173px;">
							<a href="#" onclick="location.href='${base}/index/otherLive.do'">
								<img src="${base}/common/template/third/newLiveV6/images/logo_11.jpg">
							</a>
						</li>
						</c:if>
						<li style="float: left; width: 173px;">
							<a href="javascript:void(0);" onclick="go('${base}/forwardEbet.do?gameType=5', '13');">
								<img src="${base}/common/template/third/newLiveV6/images/logo_10.jpg">
							</a>
						</li>
						</ul></div>
				</div>
				<div class="prev"></div>
				<div class="next"></div>
			</div>
			<script type="text/javascript">
				$(".logo-slide").slide({
				                        titCell:".hd",
				                        mainCell:".logo-bd ul",
				                        autoPage:true,
				                        autoPlay:true,
				                        vis:5,
				                        scroll:1,
				                        effect:"leftLoop"
				 })
			</script>
			<div class="live-list">
				<ul>
					<li class="li1">
						<a href="javascript:void(0);" onclick="go('${base}/forwardAg.do', '1');">
							<img src="${base}/common/template/third/newLiveV6/images/bigimg1.jpg" alt="">
						</a>
					</li>
					<li class="li2">
						<a href="javascript:void(0);" onclick="go('${base}/forwardBbin.do?type=live', '2');">
							<img src="${base}/common/template/third/newLiveV6/images/bigimg2.jpg" alt="">
						</a>
					</li>
					<li class="li3">
						<a href="javascript:void(0);" onclick="go('${base}/forwardAg.do', '1');">
							<img src="${base}/common/template/third/newLiveV6/images/ag.jpg" alt="">
						</a>
					</li>
					<li class="li4">
						<a href="javascript:void(0);" onclick="go('${base}/forwardBbin.do?type=live', '2');">
							<img src="${base}/common/template/third/newLiveV6/images/bbin.jpg" alt="">
						</a>
					</li>
					<li class="li5">
						<a href="javascript:void(0);" onclick="go('${base}/forwardBg.do?type=2', '98');">
							<img src="${base}/common/template/third/newLiveV6/images/bg.png" alt="">
						</a>
					</li>
							<li class="li6">
								<a href="javascript:void(0);" onclick="go('${base}/forwardVr.do', '97');" >
									<img src="${base}/common/template/third/newLiveV6/images/vr.png" alt="">
								</a>
							</li>
					<li class="li7">
						<a href="javascript:void(0);" onclick="go('${base}/forwardDs.do?gameType=1', '8');">
							<img src="${base}/common/template/third/newLiveV6/images/ds.jpg" alt="">
						</a>
					</li>
					<li class="li8">
						<a href="javascript:void(0);" onclick="go('${base}/forwardAb.do', '5');">
							<img src="${base}/common/template/third/newLiveV6/images/ob.jpg" alt="">
						</a>
					</li>
					<li class="li9">
						<a href="javascript:void(0);" onclick="go('${base}/forwardOg.do?gameType=desktop', '7');">
							<img src="${base}/common/template/third/newLiveV6/images/og.jpg" alt="">
						</a>
					</li>
					<li class="li10">
						<a href="javascript:void(0);" onclick="go('${base}/forwardMg.do?gameType=1', '3');">
							<img src="${base}/common/template/third/newLiveV6/images/mg.jpg" alt="">
						</a>
					</li>
					<c:if test="${isPtOnOff eq 'on'}">
					<li class="li11">
						<a href="#" onclick="location.href='${base}/index/otherLive.do'">
							<img src="${base}/common/template/third/newLiveV6/images/pt.jpg" alt="">
						</a>
					</li>
					</c:if>
					<li class="li12">
						<a href="javascript:void(0);" onclick="go('${base}/forwardEbet.do?gameType=5', '13');">
							<img src="${base}/common/template/third/newLiveV6/images/ebet.jpg" alt="">
						</a>
					</li>
				</ul>
			</div>
			<div class="live-more">
				+更多平台  敬请期待
			</div>

		</div>
	</div>
<jsp:include page="/common/template/third/page/live_demo.jsp" />
<script>
	var iheight = $(".w1000").height()
	var iwidth = $(".w1000").width()
	$(function(){
		$('#mainframe', parent.document).height(iheight).width(iwidth)
	})
</script>
	</body></html>