<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${_title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="favicon.ico">
<link href="${base }/common/template/third/egame/css/style.css" rel="stylesheet" type="text/css">
<link href="${base }/common/template/third/egame/css/game.css" rel="stylesheet" type="text/css">
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script src="${base }/common/template/third/egame/js/textScroll.js"></script>
<script type="text/javascript" src="${base }/common/template/third/egame/js/commonV2.js?v=20170305" path="${base}"></script>
</head>
<body>
	<meta charset="utf-8">
	<div class="E02-displayWin E02-none" id="firstTime">
		<div class="loginWin">
			<div class="displayTitle clearfix">
				<div class="fl">
					<h3>登录游戏</h3>
				</div>
				<div class="fr">
					<button class="close"></button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			//公告栏
			$("#txtSliBox").textScroll();
			$('input.regInp').val('');
			$('em.regTips').on('click', function() {
				$(this).siblings('input.regInp').focus();
			});
			$('input.regInp').each(function() {
				$(this).focus(function() {
					$(this).siblings('em.regTips').hide();
					if ($(this).parent().hasClass('txt_div')) {
						$(this).css("background", "#ffe4e9");
						$(this).siblings("label").css("background", "#ffe4e9");
					}
				}).blur(function() {
					var thisVal = $(this).val();
					if ($(this).parent().hasClass('txt_div')) {
						$(this).css("background", "#e9e9e9");
						$(this).siblings("label").css("background", "#e9e9e9");
					}
					if (thisVal) {
						$(this).next('em.regTips').hide();
					} else {
						$(this).next('em.regTips').show();
					}
				});
			});
		});
	</script>

<input type="hidden" id="userid" name="userid" value="${loginMember.account}" />
	<div id="E02-game" style="padding: 0px;">
		<div class="game">
			<div class="choose E02-hr clearfix" style="padding-bottom: 60px;">
				<div class="E02-section">
					<div class="w100 clearfix">
						<div class="E02-search">
							<input type="text" id="searchname" class="search"
								placeholder="搜索游戏"><input type="button"
								class="searchBtn" id="searchbtn">
						</div>
					</div>
					<div class="leftsidebar_box">
						<ul>
							<li class="gameTitle">游戏平台</li>
							<c:if test="${not empty isAgNav}">
							<li class="allgame">
								<h5 data-plat="AG" id="agtitle" class="imnormal">AG</h5>
								<div class="left_tabbox" style="display: none;">
									<!--tabbtn end-->
									<div class="tabcon" id="fadecon">
										<div class="sublist">
											<ul class="clearfix gametypes" id="agTab">
												<li class="gameTitle">游戏类型</li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;1&quot;}" onClick="getdata(0,1);" class="imfocus isAgActive">全部</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;2&quot;}" onClick="getdata(3,1);" class="isAgActive">最新</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;6&quot;}" onClick="getdata(4,1);" class="isAgActive">老虎</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(8,1);" class="isAgActive">熱門遊戲 </a></li>
											</ul>
										</div>
									</div>
									<!--tabcon end-->

								</div>
							</li>
							<li class="allgame">
								<h5 data-plat="MG" id="mgtitle" class="imnormal">MG</h5>
								<div class="left_tabbox" style="display: none;">
									<!--tabbtn end-->
									<div class="tabcon" id="fadecon">
										<div class="sublist">
											<ul class="clearfix gametypes" id="mgTab">
												<li class="gameTitle">游戏类型</li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;1&quot;}" onClick="getdata(0,2);" class="isMgActive imfocus">全部</a></li>
												 <li style="width: 105px;"><a href="javascript:void(0);" onClick="getdata(1,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;2&quot;}">拉霸(3线老虎机)</a></li>
												 <li style="width: 105px;"><a href="javascript:void(0);" onClick="getdata(2,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;3&quot;}">拉霸(5线老虎机)</a></li>
												 <!-- <li style="width: 111px;"><a href="javascript:void(0);" onClick="getdata(3,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;4&quot;}">拉霸(其他老虎机)</a></li> -->
												 <li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;6&quot;}" onClick="getdata(4,2);">刮刮卡</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(5,2);">桌面游戏</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(6,2);">视讯扑克</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(7,2);">其他游戏</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(8,2);">熱門遊戲</a></li>
											</ul>
										</div>
									</div>
									<!--tabcon end-->

								</div>
							</li>
							</c:if>
							
							<c:if test="${empty isAgNav}">
							<li class="allgame">
								<h5 data-plat="MG" id="mgtitle" class="imnormal">MG</h5>
								<div class="left_tabbox" style="display: none;">
									<!--tabbtn end-->
									<div class="tabcon" id="fadecon">
										<div class="sublist">
											<ul class="clearfix gametypes" id="mgTab">
												<li class="gameTitle">游戏类型</li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;1&quot;}" onClick="getdata(0,2);" class="isMgActive imfocus">全部</a></li>
												 <li style="width: 105px;"><a href="javascript:void(0);" onClick="getdata(1,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;2&quot;}">拉霸(3线老虎机)</a></li>
												 <li style="width: 105px;"><a href="javascript:void(0);" onClick="getdata(2,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;3&quot;}">拉霸(5线老虎机)</a></li>
												 <!-- <li style="width: 111px;"><a href="javascript:void(0);" onClick="getdata(3,2);" class="isMgActive" data-option="{&quot;type&quot;:&quot;4&quot;}">拉霸(其他老虎机)</a></li> -->
												 <li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;6&quot;}" onClick="getdata(4,2);">刮刮卡</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(5,2);">桌面游戏</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(6,2);">视讯扑克</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(7,2);">其他游戏</a></li>
												<li><a href="javascript:void(0);" class="isMgActive"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(8,2);">熱門遊戲</a></li>
											</ul>
										</div>
									</div>
									<!--tabcon end-->

								</div>
							</li>
							
							<li class="allgame">
								<h5 data-plat="AG" id="agtitle" class="imnormal">AG</h5>
								<div class="left_tabbox" style="display: none;">
									<!--tabbtn end-->
									<div class="tabcon" id="fadecon">
										<div class="sublist">
											<ul class="clearfix gametypes" id="agTab">
												<li class="gameTitle">游戏类型</li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;1&quot;}" onClick="getdata(0,1);" class="imfocus isAgActive">全部</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;2&quot;}" onClick="getdata(3,1);" class="isAgActive">最新</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;6&quot;}" onClick="getdata(4,1);" class="isAgActive">老虎</a></li>
												<li><a href="javascript:void(0);"
													data-option="{&quot;type&quot;:&quot;5&quot;}" onClick="getdata(8,1);" class="isAgActive">熱門遊戲 </a></li>
											</ul>
										</div>
									</div>
									<!--tabcon end-->

								</div>
							</li>
							</c:if>

						</ul>
					</div>


				</div>
			</div>
			<div class="gameContent">
				<div class="E02-section">
					<div class="rightlist">
						<ul id="itemContainer" class="clearfix" style="min-height: 585px;width: 1050px;">
						</ul>
						<div class="holder pager E02-page" id="page_next" style="width: 500px;" onselectstart="return false;">
							<a data-pnum="pre" onselectstart="return false;" ng-click="PrevPage()" id="PrevPage" class="jp-previous jp-disabled"></a>
							<div id="fpages"></div>
<!-- 								<a data-pnum="1" onselectstart="return false;" class="jp-current fpage">1</a> -->
							<a data-pnum="next" onselectstart="return false;" id="NextPage" class="jp-next"></a>
						</div>
						
						<div class="holder pager"></div>
						<div class="E02-search">
							<input type="text" id="searchname_bottom" class="search"
								placeholder="搜索游戏"><input type="button"
								class="searchBtn" id="searchbtn_bottom">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="contunder"></div>
	<!---------------左侧筛选栏滑动选中特效开始------------->
	<script type="text/javascript">
		$(function() {//游戏平台选项卡
			$(".leftsidebar_box h5").addClass("imnormal");
			$(".leftsidebar_box h5")
					.click(
							function() {
								$('.leftsidebar_box').find('a').removeClass(
										'active');
								$(this).prop("class", "imfocus");
								$(this).parent().find('.left_tabbox').show();//父元素集合中的.left_tabbox移除样式
								$(this).parent().siblings()
										.find('.left_tabbox').hide();
								$(this).parent().siblings().find('h5').prop(
										"class", "imnormal");
							});
			var TV = $(this).prop("class");
			if (TV == "imfocus") {
				$(this).removeClass("imfocus");
				$(this).addClass("imnormal");
				$(this).parent().find('.left_tabbox').hide();
			}
		})
	</script>
	<!---------------结束------------->

	<!---------------筛选栏内部tab选项卡特效开始------------->
	<script type="text/javascript">
		$(document).ready(function($) {
			$(".leftsidebar_box").find("h5").eq(0).click();
			
			$('#mgTab>li>a').click(function() {
				$('.isMgActive').removeClass('imfocus');
				$(this).addClass('imfocus');
			});
			
			
			$('#agTab>li>a').click(function() {
				$('.isAgActive').removeClass('imfocus');
				$(this).addClass('imfocus');
			});
			
			$('#mgtitle').click(function() {
				$('.isMgActive').removeClass('imfocus');
				$('#mgTab>li>a:first').addClass('imfocus');
				getdata(0,2);
			});
			
			$('#agtitle').click(function() {
				$('.isAgActive').removeClass('imfocus');
				$('#agTab>li>a:first').addClass('imfocus');
				getdata(0,1);
			});
			
		});
	</script>
	<!---------------结束------------->

	<!-------------内部选项卡选择判断--------------->
	<script>

		function changescrolltop() {
			$('body').scrollTop(200);
			document.documentElement.scrollTop = document.documentElement.scrollTop + 200;
		}
	</script>
	<!---------------结束------------->

	<script>
		$(".left_tabbox .tabbtn li").click(function() {
			var MySib = $(this).siblings();
			$(this).addClass("selected");
			MySib.removeClass("selected");

		})

		function getUrlParam(key) {
			var reg = new RegExp("(^|$)" + key + "=([^$]*)(&|$)");
			var name = window.location.search.substr(1).match(reg);
			if (name != null)
				return unescape(name[2]);
			return null;
		}

		$('#searchname').keyup(function(e) {
			if (e.keyCode == 13) {
				$('#searchbtn').click();
			}
		});
		
		if(${not empty isAgNav}){
			getdata(0,1);
		}else{
			getdata(0,2);
		}
		

		$('#searchbtn_bottom').click(function() {
			$('#searchname').val($('#searchname_bottom').val());
			$('#searchbtn').click();
		});

		$('#searchbtn').click(function() {
			$('#searchname_bottom').val($('#searchname').val());
		});
	</script>
<style>
		.game_logo {
            position: relative;
            display: inline-block;
            width: 140px;
            height: 136px;
            margin: 0px 25px;
            overflow: hidden;
            cursor:pointer;
        }

        .game_logo img {
                position: absolute;
                left: 0;
                width: 280px;
                height: 136px;
            }

        .game_logo img:hover {
            left: -140px;
        }
</style>

</body>
</html>