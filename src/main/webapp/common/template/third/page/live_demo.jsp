<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script type="text/javascript">
	function go(url, type,target) {
		var userid = '${loginMember.account}';
		var bbinOnOff = '${isBbinDzOnOff}';
		var agOnOff = '${isAgOnOff}';
		var mgOnOff = '${isMgOnOff}';
		var abOnOff = '${isAbOnOff}';
		var ogOnOff = '${isOgOnOff}';
		var dsOnOff = '${isDsOnOff}';
		var bgOnOff = '${isBgOnOff}';
		var vrOnOff = '${isVrOnOff}';
		var agDzOnOff = '${isAgDzOnOff}';
		var mgDzOnOff = '${isMgDzOnOff}';
		var ebetOnOff = '${isEbetOnOff}';
		if (userid == null || userid == '') {
			alert("请先登录系统");
			return;
		}
		
		if(type==1 & agOnOff == 'off' & agDzOnOff != 'on'){
			alert('ag系统维护');
			return;
		}else if(type==2 & bbinOnOff == 'off'){
			alert('bbin系统维护');
			return;
		}else if(type==3 & mgOnOff == 'off'  & mgDzOnOff != 'on'){
			alert('mg系统维护');
			return;
		}else if(type==5 & abOnOff == 'off'){
			alert('ab系统维护');
			return;
		}else if(type==7 & ogOnOff == 'off'){
			alert('og系统维护');
			return;
		}else if(type==8 & dsOnOff == 'off'){
			alert('ds系统维护');
			return;
		}else if(type==98 & bgOnOff == 'off'){
			alert('bg系统维护');
			return;
		}else if(type==97 & vrOnOff == 'off'){
			alert('vr系统维护');
			return;
		}else if(type==13 & ebetOnOff == 'off'){
			alert('ebet系统维护');
			return;
		}
		
		var sw = '';
		csw = '';
		sh = '';
		csh = '';
		ctop = '';
		cleft = '';

		sw = $(window.parent).width();
		sh = $(window.parent).height();
		csw = $(window.parent).width();
		csh = $(window.parent).height();
		ctop = 0;
		cleft = 0;
		var tempUrl = url;
		/* if(type=='3'||type==3){ */
			console.log(window);
		var windowOpen = null;
		if (!target){
			windowOpen = window.open("" ,'_blank', 'width=' + csw
					+ ',height=' + csh + ',left=' + cleft + ',top=' + ctop
					+ ',scrollbars=no,location=1,resizable=yes');	
		}
		var result = null;
		var ajaxFn = $ajax ? $ajax : $.ajax;//原生态ajax
		ajaxFn({
			url : url,
			dataType:"json",
			type :"post",
			async : false,
			success : function(json) {
				result = json;
			}
		});
		
		if(!result.success){
			//alert(result.msg);
			var new_doc = windowOpen.document.open("text/html","replace");
		    new_doc.write("<script>alert('" + result.msg + "');window.close();<\/script>");
		    new_doc.close();
			return;
		}
		if ((type === '99' || type === '991') && target && target == 'self'){
			window.location.href = base+"/index/sport.do?sportUrl="+encodeURIComponent(encodeURIComponent(result.url));
		} else {
			windowOpen.location.href = result.url;
		}
	}
	
</script>