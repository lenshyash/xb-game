<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${_title }</title>
    <script type="text/javascript">var base="${base}";</script>
    <script type="text/javascript" src="${base }/common/template/third/egame/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="${base }/common/template/third/egame/js/agcommon.js" path="${base }"></script>
    <link rel="stylesheet" type="text/css" href="${base }/common/template/third/egame/css/agGame.css">
</head>
<body>
<input type="hidden" id="userid" name="userid" value="${userid }" />
<div class="MgGame" ng-controller="MgCtrl">
    <header id="header">
        <div id="search">
            <div id="form">
                <input type="text" ng-model="gamesearch" id="keywords" value="" onkeyup="search($(this).val())" />
                <button id="search-btn" type="button" onclick="search($('#keywords').val())"></button>
            </div>
        </div>
    </header>
    <nav>
        <ul id="gamenav">
            <li class="navtitle">ag游戏分类：</li>
            <li class="navall" onclick="getdata(0)">全部</li>
            <li class="navall" onclick="getdata(3)">最新</li>
            <li class="navall" onclick="getdata(4)">老虎</li>
            <li class="navall hot" onclick="getdata(8)">熱門遊戲 
            <i class="fa fa-caret-up"></i></li>

        </ul>
    </nav>
    <ul id="gamelist"></ul>
    <div id="gamelists">
    </div>

    <div class="pagination">
        <ul id="pager">
            <li ng-click="StartPage()" id="StartPage">首页</li>
            <li ng-click="PrevPage()" id="PrevPage">上一页</li>
            <li>page:<span ng-bind="currentPage+1" ng-class="ng-binding " id="currentPage"></span></li>
            <li id="NextPage">下一页</li>
            <li id="EndPage">尾页</li>
            <li>共<span ng-bind="pageCount()" ng-class="ng-binding" id="pageCount"></span>页</li>
        </ul>
    </div>
</div>
</body>
</html>