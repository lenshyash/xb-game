<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${_title }</title>
    <script type="text/javascript" src="${base }/common/template/third/egame/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="${base }/common/template/third/egame/js/common.js?v=201703051"></script>
    <link rel="stylesheet" type="text/css" href="${base }/common/template/third/egame/css/MGGame.css">
</head>
<body>
<input type="hidden" id="userid" name="userid" value="${userid }" />
<div class="MgGame" ng-controller="MgCtrl">
    <header id="header">
        <div id="search">
            <div id="form">
                <input type="text" ng-model="gamesearch" id="keywords" value="" onkeyup="search($(this).val())" />
                <button id="search-btn" type="button" onclick="search($('#keywords').val())"></button>
            </div>
        </div>
    </header>
    <nav>
        <ul id="gamenav">
            <li class="navtitle">游戏分类：</li>
            <li class="navall" onclick="getdata(0)">全部</li>
            <!-- ngRepeat: Cates in gameCategories -->
            <li ng-repeat="Cates in gameCategories" class="ng-scope">
                <span class="ng-binding" onclick="getdata(123)">
                    拉霸
                    <!-- ngIf: Cates.ChildList.length > 0 --><i class="fa fa-caret-down ng-scope" ng-if="Cates.ChildList.length > 0"></i><!-- end ngIf: Cates.ChildList.length > 0 -->
                </span>
                <!-- ngIf: Cates.ChildList.length > 0 --><ol class="nav_sub ng-scope" ng-if="Cates.ChildList.length > 0">
                    <!-- ngRepeat: cate in Cates.ChildList -->
                    <li ng-repeat="cate in Cates.ChildList" onclick="getdata(1)" class="ng-scope ng-binding">
                        3线老虎机
                    </li><!-- end ngRepeat: cate in Cates.ChildList -->
                    <li ng-repeat="cate in Cates.ChildList" onclick="getdata(2)" class="ng-scope ng-binding">
                        5线老虎机
                    </li><!-- end ngRepeat: cate in Cates.ChildList -->
                    <li ng-repeat="cate in Cates.ChildList" onclick="getdata(3)" class="ng-scope ng-binding">
                        其他老虎机
                    </li><!-- end ngRepeat: cate in Cates.ChildList -->
                </ol><!-- end ngIf: Cates.ChildList.length > 0 -->
            </li><!-- end ngRepeat: Cates in gameCategories -->
            <li ng-repeat="Cates in gameCategories" class="ng-scope">
                <span onclick="getdata(4)" class="ng-binding">
                    刮刮卡
                    <!-- ngIf: Cates.ChildList.length > 0 -->
                </span>
                <!-- ngIf: Cates.ChildList.length > 0 -->
            </li><!-- end ngRepeat: Cates in gameCategories -->
            <li ng-repeat="Cates in gameCategories" class="ng-scope">
                <span onclick="getdata(5)" class="ng-binding">
                    桌面游戏
                    <!-- ngIf: Cates.ChildList.length > 0 -->
                </span>
                <!-- ngIf: Cates.ChildList.length > 0 -->
            </li><!-- end ngRepeat: Cates in gameCategories -->
            <li ng-repeat="Cates in gameCategories" class="ng-scope">
                <span onclick="getdata(6)" class="ng-binding">
                    视讯扑克
                    <!-- ngIf: Cates.ChildList.length > 0 -->
                </span>
                <!-- ngIf: Cates.ChildList.length > 0 -->
            </li><!-- end ngRepeat: Cates in gameCategories -->
            <li ng-repeat="Cates in gameCategories" class="ng-scope">
                <span onclick="getdata(7)" class="ng-binding">
                    其他游戏
                    <!-- ngIf: Cates.ChildList.length > 0 -->
                </span>
                <!-- ngIf: Cates.ChildList.length > 0 -->
            </li><!-- end ngRepeat: Cates in gameCategories -->
            <li class="navall hot" onclick="getdata(8)">熱門遊戲 <i class="fa fa-caret-up"></i></li>

        </ul>
    </nav>
    <ul id="gamelist"></ul>
    <div id="gamelists">
    </div>

    <div class="pagination">
        <ul id="pager">
            <li ng-click="StartPage()" id="StartPage">首页</li>
            <li ng-click="PrevPage()" id="PrevPage">上一页</li>
            <li>page:<span ng-bind="currentPage+1" ng-class="ng-binding " id="currentPage"></span></li>
            <li id="NextPage">下一页</li>
            <li id="EndPage">尾页</li>
            <li>共<span ng-bind="pageCount()" ng-class="ng-binding" id="pageCount"></span>页</li>
        </ul>
    </div>
</div>
</body>
</html>