<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLiveV3/css/site.css">
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLiveV3/css/live.css?v=1.124">
<script type="text/javascript"
	src="${base}/common/template/third/newLiveV3/js/jquery-1.8.3.min.js"></script>
</head>


<body>
	<div class="wrapper">
		<div class="video_list">
			<ul style="width:100%;">
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardAg.do', '1');"> <span
						style="display: inline;">女优厅(AG)</span>
				</a></li>
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardDs.do?gameType=1', '8');"> <span
						style="display: inline;">太阳厅(DS)</span>
				</a></li>
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardBbin.do?type=live', '2');"> <span
						style="display: inline;">台湾厅(BBIN)</span>
				</a></li>
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardMg.do?gameType=1', '3');"> <span
						style="display: inline;">欧美厅(MG)</span>
				</a></li>
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardAb.do', '5');"> <span
						style="display: inline;">亚洲厅(AB)</span>
				</a></li>
				<li class="dark" style="width:16.6%"><a href="javascript:void(0);"
					onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"> <span
						style="display: inline;">东方厅(OG)</span>
				</a></li>
<!-- 				<li class="dark" style="width:12.5%"><a href="javascript:void(0);" -->
<%-- 					onclick="go('${base}/forwardBg.do?type=2', '98');"> <span --%>
<!-- 						style="display: inline;">大游厅(BG)</span> -->
<!-- 				</a></li> -->
<!-- 				<li class="dark" style="width:12.5%"><a href="javascript:void(0);" -->
<%-- 					onclick="go('${base}/forwardVr.do', '97');"> <span --%>
<!-- 						style="display: inline;">VR厅(VR)</span> -->
<!-- 				</a></li> -->
			</ul>
			<div class="clr"></div>
		</div>
		<section id="lobby" ng-controller="LobbiesCtrl" class="ng-scope"
			style="overflow: hidden;">
		<div class="accordion">
			<ul class="game-list">
				<li class="slide ng-animate" id="ag" onmousemove="selectLive('ag')"
					onclick="go('${base}/forwardAg.do', '1');"
					ng-class="{'active':selectLive=='ag'}" game-box="ag"  style='width: 126px;'></li>
				<li class="slide ng-animate" id="ds" onmousemove="selectLive('ds')"
					onclick="go('${base}/forwardDs.do', '8');"
					ng-class="{'active':selectLive=='ds'}" game-box="ds" style='width: 126px;'></li>
				<li class="slide ng-animate active" id="bb"
					onmousemove="selectLive('bb')"
					onclick="go('${base}/forwardBbin.do?type=live', '2');"
					ng-class="{'active':selectLive=='bb'}" game-box="bb" style='width: 126px;'></li>
				<li class="slide ng-animate" id="mg" onmousemove="selectLive('mg')"
					onclick="go('${base}/forwardMg.do?gameType=1', '3');"
					ng-class="{'active':selectLive=='mg'}" game-box="mg" style='width: 126px;'></li>
				<li class="slide ng-animate" id="ab" onmousemove="selectLive('ab')"
					onclick="go('${base}/forwardAb.do', '5');"
					ng-class="{'active':selectLive=='ab'}" game-box="ab" style='width: 126px;'></li>
				<li class="slide ng-animate" id="og" onmousemove="selectLive('og')"
					onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"
					ng-class="{'active':selectLive=='og'}" game-box="og" style='width: 126px;'></li>
<!-- 				<li class="slide ng-animate" id="bg" onmousemove="selectLive('bg')" -->
<%-- 					onclick="go('${base}/forwardBg.do?type=2', '98');" --%>
<!-- 					ng-class="{'active':selectLive=='bg'}" game-box="bg" style='width: 90.5px;'></li> -->
<!-- 				<li class="slide ng-animate" id="vr" onmousemove="selectLive('vr')" -->
<%-- 					onclick="go('${base}/forwardVr.do', '97');" --%>
<!-- 					ng-class="{'active':selectLive=='vr'}" game-box="vr" style='width: 90.5px;'></li> -->
			</ul>
		</div>
		</section>
	</div>
	<script type="text/javascript">
		function selectLive(id) {
			$(".ng-animate").removeClass("active");
			$("#" + id).addClass("active");
		}
	</script>
	<jsp:include page="/common/template/third/page/live_demo.jsp" />
</body>
</html>