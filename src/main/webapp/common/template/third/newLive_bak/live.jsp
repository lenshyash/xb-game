<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLive/css/style.css">
<script type="text/javascript"
	src="${base}/common/template/third/newLive/js/jquery-1.8.3.min.js"></script>
</head>

<body>
	<div class="live_page">
		<div class="video_wrap">
			<div class="video_list">
				<ul>
					<li class="dark"><a href="javascript:void(0);"
						onclick="go('${base}/forwardBbin.do?type=live', '2');"> <span
							style="display: inline;">台湾厅(BBIN)</span>
					</a></li>
					<li class="dark"><a href="javascript:void(0);"
						onclick="go('${base}/forwardAg.do', '1');"> <span
							style="display: inline;">女优厅(AG)</span>
					</a></li>
					<li class="dark"><a href="javascript:void(0);"
						onclick="go('${base}/forwardMg.do?gameType=1', '3');"> <span
							style="display: inline;">欧美厅(MG)</span>
					</a></li>
					<li class="dark"><a href="javascript:void(0);"
						onclick="go('${base}/forwardAb.do', '5');"> <span
							style="display: inline;">亚洲厅(AB)</span>
					</a></li>
				</ul>
				<script type="text/javascript">
					$(function() {
						$(".video_list a").hover(function() {
							$(this).children("span").fadeToggle();
						})
					})
				</script>
				<div class="clr"></div>
			</div>
			<div class="video_wrap">
				<div class="video_wrap_box1">
					<div class="video_item view view-tenth" style="cursor: pointer;">
						<a href="javascript:void(0)"
							onclick="go('${base}/forwardBbin.do?type=live', '2');"
							class="cover"> <img
							src="${base}/common/template/third/newLive/images/bb.jpg">
						</a> <a class="mask"
							onclick="go('${base}/forwardBbin.do?type=live', '2');"
							href="javascript:void(0)"> <img
							src="${base}/common/template/third/newLive/images/bb_h.jpg">
						</a>
					</div>
					<div class="video_item view view-tenth" style="cursor: pointer;">
						<a href="javascript:void(0)"
							onclick="go('${base}/forwardAg.do', '1');" class="cover"> <img
							src="${base}/common/template/third/newLive/images/ag.jpg">
						</a> <a class="mask" onclick="go('${base}/forwardAg.do', '1');"
							href="javascript:void(0)"> <img
							src="${base}/common/template/third/newLive/images/ag_h.jpg">
						</a>
					</div>
					<div class="video_item view view-tenth" style="cursor: pointer;">
						<a href="javascript:void(0)"
							onclick="go('${base}/forwardMg.do?gameType=1', '3');"
							class="cover"> <img
							src="${base}/common/template/third/newLive/images/mg.jpg">
						</a> <a class="mask"
							onclick="go('${base}/forwardMg.do?gameType=1', '3');"
							href="javascript:void(0)"> <img
							src="${base}/common/template/third/newLive/images/mg_h.jpg">
						</a>
					</div>
					<div class="video_item view view-tenth" style="cursor: pointer;">
						<a href="javascript:void(0)"
							onclick="go('${base}/forwardAb.do', '5');"
							class="cover"> <img
							src="${base}/common/template/third/newLive/images/ab.jpg">
						</a> <a class="mask"
							onclick="go('${base}/forwardAb.do', '5');"
							href="javascript:void(0)"> <img
							src="${base}/common/template/third/newLive/images/ab_h.jpg">
						</a>
					</div>
<!-- 					<div class="video_item view view-tenth"> -->
<!-- 						<a href="javascript:alert('敬请期待!');"> <img -->
<%-- 							src="${base}/common/template/third/newLive/images/qidai.png"></a> --%>
<!-- 					</div> -->
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/common/template/third/page/live_demo.jsp" />
</body>
</html>