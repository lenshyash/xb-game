<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0020)https://js67678.com/ -->
<html class="first zh-cn" lang="en">
<head>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/third/newLiveV7/css/video_css.css">
	<script type="text/javascript"
		src="${base}/common/template/third/newLiveV7/js/jquery.easing.1.3.min.js"></script>
<script src="${base}/common/template/third/newLiveV7/js/jquery.min.js"></script>
</head>
<body marginwidth="0" marginheight="0">

	<div class="video_wrap">
		<div class="video_list">
			<ul>
				<li class="dark"><a href="#this"
					onclick="go('${base}/forwardAg.do', '1');"><span
						style="display: inline;">女优厅(AG)</span></a></li>
				<li><a href="#this"
					onclick="go('${base}/forwardDs.do?gameType=1', '8');"><span
						style="display: inline;">太阳厅(DS)</span></a></li>
				<li class="dark"><a href="#this"
					onclick="go('${base}/forwardBbin.do?type=live', '2');"><span
						style="display: inline;">台湾厅(BBIN) </span></a></li>
				<li><a href="#this"
					onclick="go('${base}/forwardMg.do?gameType=1', '3');"><span
						style="display: inline;">欧美厅(MG)</span></a></li>

				<li class="dark"><a href="#this"
					onclick="go('${base}/forwardAb.do', '5');"><span
						style="display: inline;">亚洲厅(AB)</span></a></li>
				<li><a href="#this"
					onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"><span
						style="display: inline;">东方厅(OG) </span></a></li>
				<li class="dark"><a href="#this"
					onclick="go('${base}/forwardBg.do?type=2', '98');"><span
						style="display: inline;">大游厅(BG) </span></a></li>
				<li><a href="#this" onclick="go('${base}/forwardVr.do', '97');"><span
						style="display: inline;">VR厅(VR)</span></a></li>
			</ul>
			<script type="text/javascript">
				$(function() {
					$(".video_list a").hover(function() {
						$(this).children("span").fadeToggle();
					})
				})
			</script>
			<div class="clr"></div>
		</div>
		<div class="video_wrap">
			<div class="video_wrap_box1">

				<div class="video_item view view-tenth ag">
					<a href="#this" onclick="go('${base}/forwardAg.do', '1');"><img
						src="${base}/common/template/third/newLiveV7/images/ag.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardAg.do', '1');"></a>
				</div>
				<div class="video_item view view-tenth ds">
					<a href="#this"
						onclick="go('${base}/forwardDs.do?gameType=1', '8');"><img
						src="${base}/common/template/third/newLiveV7/images/ds.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardDs.do?gameType=1', '8');"></a>
				</div>
				<div class="video_item view view-tenth bbin">
					<a href="#this"
						onclick="go('${base}/forwardBbin.do?type=live', '2');"><img
						src="${base}/common/template/third/newLiveV7/images/bbin.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardBbin.do?type=live', '2');"></a>
				</div>
				<div class="video_item view view-tenth mg">
					<a href="#this"
						onclick="go('${base}/forwardMg.do?gameType=1', '3');"><img
						src="${base}/common/template/third/newLiveV7/images/mg.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardMg.do?gameType=1', '3');"></a>
				</div>

				<div class="video_item view view-tenth gd">
					<a href="#this" onclick="go('${base}/forwardAb.do', '5');"><img
						src="${base}/common/template/third/newLiveV7/images/ob.png"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardAb.do', '5');"></a>
				</div>
				<div class="video_item view view-tenth dg">
					<a onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"><img
						src="${base}/common/template/third/newLiveV7/images/og.png"></a>
					<a class="mask"
						onclick="go('${base}/forwardOg.do?gameType=desktop', '7');"></a>
				</div>
				<div class="video_item view view-tenth bg">
					<a href="#this" onclick="go('${base}/forwardBg.do?type=2', '98');"><img
						src="${base}/common/template/third/newLiveV7/images/bg.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardBg.do?type=2', '98');"></a>
				</div>
				<div class="video_item view view-tenth vr">
					<a href="#this" onclick="go('${base}/forwardVr.do', '97');"><img
						src="${base}/common/template/third/newLiveV7/images/vr.jpg"></a>
					<a class="mask" href="#this"
						onclick="go('${base}/forwardVr.do', '97');"></a>
				</div>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
		<jsp:include page="/common/template/third/page/live_demo.jsp" />
</body>
</html>