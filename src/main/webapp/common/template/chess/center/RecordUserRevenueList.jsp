<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "605";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
			<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>推广收益</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <form method="post" action="./RecordUserRevenueList.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KjGi7m9e+iwfqRWhsOgRIndEimrGHJfq0oSW2yCtPAMMcIv2RjS9MJ2jVyTAVhDe+h9TNbc9rYGS6UN+DJGqQgixCa6Q5SgKYJ0A+Jce8bS3i2A+ZTXDCFMftScwAWMjn2M8TBNvXPnaq33xSiN8U19VV7NIoBxUAT+JhlGKuy5zzxbQKyFdh2leBwkJyNr1wrkiBKITuhLHOkBpW9L2BlLhq66ZMDFYsh5PMuyzo7PkwGyqeHPfyBUOFNtbLoHV6OdYT61F84kQNC8NH+2isxY9UhkXDQMV84ucIJS2dp5WFPzLGgABmsrdO9Jn6BC8udGJRjPGGtx471uMDH440vTkl7L9PPCnu6Lj35SOXRUGEl9yBUzWPL0jT0tNBnrWtuiSgAYfzTIhKxY9qaBJffuVKfDU/P/dLZjg3Lvp3Lqb9Q36K267aiX4sBe8ZjtBGAm+Ak8FSlPdRbEDctR1m193MS/l541JFa4juMPlQN0sL4a/UNwIuoztyIkF7QFwVtsyhZ6Sk5WvctG5tMwiZOBi1lyvtkNVyxYmhK5Chsb4vWj0+/8KAoyoBffpPnLLSb0qrkMfmND5372yup33gxOJnnx6cduH2JHpM20uYrCWw095YYHNxNFs9b0=">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="51C79889">
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="QdBBKcxEErArkGpA1ibJKoE1h6Fpp2Jf4PYvEiYhUfmTLzu4GiBVYhkFIa0QJikKiFHfKFZEH1cl12TNuWwN1fAu3lmN21WMRrPdXx4Z5xjnbTEWBxnaAL6echSUzG34H6g/4k5UHeWb62K1QOBL2GwInr5Pvu8u1vyDjgivAuXXVliXEExJZU3qFNEuLhpXSCbylAqTLzwAWgrzBFXzBSnsnv255yChlQ0/hnbok5c75S7hZdPAtWyeb7TcwVBOrPA4up/vZ0TuAsYIbwV1FAJvaSei+oZf7Qf6Lj9k8kBacFR3xpaWn9r//p30igP4MtBouuJYYAEaOkDC1CDiXok8R0xnyDqT4sHeRA==">
</div>
                            <!--box_list-->
                            <div class="box_list">
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="height: 45px;">
                                    <tbody><tr>
                                        <td>
                                            贡献用户名：
                                            <input name="txtKey" type="text" id="txtKey" class="text_normal">
                                           
                                            
                                          
                                            
                                            <select name="ddlLevel" id="ddlLevel" class="select">
	<option selected="selected" value="0">选择下线级数</option>
	<option value="1">第1级下线</option>
	<option value="2">第2级下线</option>
	<option value="3">第3级下线</option>
	<option value="4">第4级下线</option>
	<option value="5">第5级下线</option>

</select>
                                            <input type="submit" name="btnQuery" value="查询" id="btnQuery" class="btn1"><br><br>
                                            注：只能查询最近30天的记录
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>
                                            <input type="button" id="btn11" value="今天" class="btn1" onclick="window.location.href='TodayUserRevenueList.aspx'">
                                            <input type="submit" name="btnQueryYD" value="昨天" id="btnQueryYD" class="btn1">
                                            <input type="submit" name="btnQueryTW" value="本周" id="btnQueryTW" class="btn1">
                                            <input type="submit" name="btnQueryYW" value="上周" id="btnQueryYW" class="btn1">
                                        </td>
                                    </tr>
                                </tbody></table>
                                <br>
                                <table width="100%" cellpadding="0" cellspacing="1" class="tb_list">
                                    <tbody><tr>
                                        <th>日期 </th>
                                        <th>贡献用户 </th>
                                        <th>贡献抽水 </th>
                                        <th>比例 </th>
                                        <th>我的抽水 </th>
                                        <th>统计时间 </th>
                                    </tr>
                                    
                                    <tr class="tdbg"><td colspan="8" align="center"><br>没有任何信息!<br><br></td></tr>
                                </tbody></table>
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="height: 45px;">
                                    <tbody>
                                        <tr>
                                            <td align="right">
                                                <div id="anpPage">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr>
		<td valign="bottom" nowrap="true" style="width:40%;">总记录：0　页码：1/1　每页：20</td><td valign="bottom" nowrap="true" style="width:60%;"><a disabled="disabled" style="margin-right:5px;">首页</a><a disabled="disabled" style="margin-right:5px;">上页</a><span style="margin-right:5px;font-weight:Bold;color:red;">1</span><a disabled="disabled" style="margin-right:5px;">下页</a><a disabled="disabled" style="margin-right:5px;">末页</a>&nbsp;&nbsp;<input type="text" value="1" disabled="disabled" name="anpPage_input" id="anpPage_input" onkeydown="ANP_keydown(event,'anpPage_btn');" style="width:30px;"><input type="submit" value="go" name="anpPage" id="anpPage_btn" disabled="disabled" onclick="if(ANP_checkInput('anpPage_input',1)){__doPostBack('anpPage','')} else{return false}"></td>
	</tr>
</tbody></table>
</div>


                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--box_list end-->
                            
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>

</form>
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>