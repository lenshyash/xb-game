<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
<div class="userinfo-edit-main">
	<div class="for-liucheng">
		<div class="liulist for-cur"></div>
		<div class="liulist"></div>
		<div class="liulist"></div>
		<div class="liulist"></div>
		<div class="liutextbox">
			<div class="liutext for-cur">
				<em>1</em><br> <strong>设置取款密码</strong>
			</div>
			<div class="liutext">
				<em>2</em><br> <strong>完善出款银行信息</strong>
			</div>
			<div class="liutext">
				<em>3</em><br> <strong>取款</strong>
			</div>
			<div class="liutext">
				<em>4</em><br> <strong>完成</strong>
			</div>
		</div>
	</div>
	<div class="tab-user">
		<table class="userTab" width="100%" cellspacing="0" cellpadding="0"
			border="0">
			<tbody>
				<tr>
					<td></td>
					<td colspan="2" style="line-height: 22px;">1. 密码须由<font
						color="red">最少6位数字</font>组成。建议您的新密码不要和登录密码相同。<br>
						2. 提款密码为提款时的凭证，请会员牢记提款密码（如忘记密码，可联系在线客服修改）。
					</td>
				</tr>
				<tr class="idcard-tr">
					<td class="title">密码：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="payPwd" placeholder="请输入提款密码" type="password"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title">确认密码：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="rPayPwd" placeholder="请确认提款密码" type="password"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="paypwd-tr paypwd-tr-input paypwd-tr-new">
					<td class="title"></td>
					<td colspan="2" class="tdinput"><div class="nextBtn"
							style="margin-top: 10px;">
							<a href="javascript:void(0);" onclick="Base.xinXi.setPayPwd();" id="sub-btn"></a>
						</div></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".userTab input").focus(function() {
			$(this).removeClass('text-error');
			$(this).parent().siblings('.tdtip').find("span").text("");
		})
	})
</script>