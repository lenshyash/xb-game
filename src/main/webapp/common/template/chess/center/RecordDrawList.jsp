<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>${_title }</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="/common/template/chess/center/js/jquery.min.js"></script> 
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
  <script type="text/javascript" src="/common/js/layer/layer.js"></script>
<script type="text/javascript" src="/common/template/chess/center/js/jquery.date_input.js"></script>
<link rel="stylesheet" href="/common/template/chess/center/css/date_input.css" type="text/css">
 </head> 
 <body> 
  <script>
		var leftPosition = "401";
		$($.date_input.initialize);
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	    #anpPage *{
	    	font-size:12px;
	    }
	    #anpPage a{
	    	cursor: pointer;
	    }
	    #resContent,#btn_menu *{
	    	font-size:12px;
	    }
	    #resContent tr:hover{
	    	background: #eee;
	    	cursor: pointer;
	    }
	    #resContent a{
	    	font-size:12px;
	    	cursor: pointer;
	    }
	    #resContent td a:hover{
	    	color:red;
	    }
	    .page *{
	    	cursor: pointer;
	    }

	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
      <div class="main_box_right"> 
       <div class="main_box_right_title common_nav"> 
        <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>存款记录</label></span> 
       </div> 
       <div class="main_box_right_con"> 
        <form id="form1">  
         <!--box_list--> 
         <div class="box_list"> 
          <table id="btn_menu" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="height: 45px;">
                <tbody>
                	<tr>
	                    <td width="13%" align="right">日期： </td>
	                    <td width="10%"><input type="text" name="date" id="startTime" class="date_input" style="width:120px;height:26px;"></td>
	                    <td width="3%"> &nbsp;至&nbsp; </td>
	                    <td width="20%"><input type="text" name="date" id="endTime" class="date_input"  style="width:120px;height:26px;"></td>
	                    <td width="10%">
	                    	<input type="button" name="btnQuery" value="查询" id="btnQuery" class="btn1" onclick="initRdsData(1)">
	                    </td>
	                    <td>
	                       
	                    </td>
                    </tr>
                 </tbody>
          </table>
          <table width="100%" cellpadding="0" cellspacing="1" class="tb_list"> 
           <tbody>
           	<thead>
	            <tr> 
	             <th>三方游戏类型</th>
			
				<th>注单号</th>
			
				<th>游戏局号</th>
			
				<th>投注金额</th>
			
				<th>输赢金额</th>
			
				<th>投注内容</th>
			
				<th>投注时间(北京)</th>
	            </tr> 
             </thead>
             <tbody id="resContent">
             	
             </tbody>
           </tbody>
          </table> 
          <div class="page" style="width:100%;">
               <div class="pageNav">
                    <div  class="pageNav">
                       <ul class="pagination">
                       </ul>
                   </div>
                </div>
           </div>
         </div> 
         <!--box_list end--> 
        </form> 
        <div class="main_box_right_bottom"> 
        </div> 
       </div> 
      </div> 
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
  <script>
  $(function(){
		initRdsData(1)
		$("#startTime").val(startTime)
		$("#endTime").val(endTime)
  })
  function initRdsData(p){
	    if(startTime){
		    startTime = $("#startTime").val()
			endTime = $("#endTime").val()
	    }else{
	    	startTime = today()
	    	endTime = today()
	    }

		var data = {
				startTime:startTime,
				endTime:endTime,
				pageNumber:p,
				pageSize:10,
				type: 0
		}
		$.ajax({
			url:'${base}/center/record/betrecord/chessrecord.do',
			data:data,
			type:'POST',
			success:function(res){
				res = JSON.parse(res)
				if(res.rows.length>=1){
					addTemplate(res)
					page = res.currentPageNo;
					lotteryPage(res.totalCount,res.totalPageCount)
					lastPage = res.totalPageCount
					$("#pageNum").html(res.currentPageNo)
				}else{
					$("#resContent").html('<tr class="tdbg"><td colspan="8" align="center"><br />没有任何信息!<br /><br /></td></tr>')
				}
				
			}
		})
		function addTemplate(res){				
			var html = '';
			$.each(res.list,function(index, item){
				html+='<tr><td>'+item.platformType+'</td><td>'+item.bettingCode+'</td><td>'+item.gameCode+'</td><td>'+item.money+'</td><td>'+item.bettingMoney+'</td><td>'+item.winMoney+'</td><td>'+item.thirdExtInfo+'</td><td>'+getMyDate(item.bettingTime)+'</td></tr>'
			})
			$("#resContent").html(html)
		}
	}
  </script>
 </body>
</html>