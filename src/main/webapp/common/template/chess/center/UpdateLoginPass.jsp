<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "105";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
   <div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>修改登录密码</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <form method="post" action="./UpdateLoginPass.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bEJoZmmHgKfe4RLDasUXJs2LC5S7X31AkWwafWfF9XuiIlWFnJW5wl8grZcSZW1EVQR9XRVFpnH2QC562my8aXE8+IBx/ZMi3VK4HMUJoRgYhJENJubEWmxs3XXAT774gEYjCdofpuMTT+Zn9uXJ0ZDbbR/fjM8YLl+fu+E5kFCawi0sUgVP9h65ZlrGLEq8ye/qx5PJLeYmgHDuIEFemNYShb2O3idNdn/Yn3feLdEvF5DBKHbMfLjP608=">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D1ECB26F">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="K9rYr4gQpSZXFQyaJNA6WNb+hV2UifUlVr4h25xKzAdkIA64kZUsSUeiyxYR9DLig8dPBPkQqHkp3VtqaFusmENa/zOaBA8JD6pSIHncYF7NHoMwAbvs1px8YXgfpgzIuSw8ySHb8sxh/OyEVGDYpCve8HwXZ+NrA8PvNrRG4oAkcQx4MfKi3wYajLddDxEy+ksm5X7KjEBmoJR6">
</div>
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>修改登录密码</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                            <tbody><tr>
                                                <th>原密码： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtPass" type="password" id="txtPass" class="text"></span> <span id="lblPass">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>新密码： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtNewPass" type="password" id="txtNewPass" class="text"></span><span id="lblNewPass"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>确认新密码： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtConPass" type="password" id="txtConPass" class="text"></span> <span id="lblConPass">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <input type="submit" name="btnUpdate" value="修改" id="btnUpdate" class="btn">&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="reset" name="Submit2" value="取消" class="btn">
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            </form>
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>