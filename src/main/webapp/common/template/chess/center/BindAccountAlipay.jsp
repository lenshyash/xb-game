<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "107";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
      <div class="main_box_right"> 
       <div class="main_box_right_title common_nav"> 
        <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>基本资料</label></span> 
       </div> 
    	<div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>
                                        1、先完善资料 2、再绑定支付宝账户</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                            <tbody><tr>
                                                <th>帐号：
                                                </th>
                                                <td>
                                                    <span id="lblAccounts">gosick</span>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        <form method="post" action="./BindAccountAlipay.aspx" id="formUserInfo">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6PPoXL6I+guq24zlig9OdLX2tjMA1vooeBDS/Bh5Ia/nB3AocA7w548q3LiSyB8H3wFasN9j1TMsVfm87IZoBFPPGHTH/v/b5Aiv0EerUyCcXksrYyk7q3/oqZL8bi96KgZk1Loc2skLt0ea5Pl3x7tLDdR2pRQTx+dDzRHrggxMWVh27FKKiDbV0K51ArMONH+ZtMo/UicCPiWehHUzzX919DyObYQSQANyX/BEayvsvW9tvkog2mdFEw/HT3SWWPl1fhoaQJrpMA4CYtIzihQRq+60H8fjzNOngSxiklpy7Yx9HMQUv9k0DqUG6DBnwH1VHob3dCgc3hhAwmuAAg+/YAu1OThMIheXU3wNkilx33VI3kNi3Cs8DA+FOZboo+ZnPg7JLPoEcm59+mhiPPN1QGRPyy3v+mJVMu1bvKCgdNamCX+A5NkKh0+IUNXSLVwoiScdORh9XsR1sqUYgb21aS0=">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1FF0382B">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="6iShRwx6rkVtm4ohg6ANf32lnWXuzc4HyNlZGPbAC1SWFU3MKUaQuE0waXIU31Hhf2BgweRiNA6gJ2sO3x0BoOVpljaUr/VzFzdrhpskx2TFjShdtsCALiCpBEWwHQtFh8adKGVRT/ZxH0XekAuoeOCDP5ooL/K5ikyPF9+AQ2XGSIc8SwfO+QnZ/WeDX9I1Tv+nR6ZbihL1QKu0OivBa6ZOdQCcpjXTeI7+9Q==">
</div>
                                            <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                                <tbody id="Tbody3">
                                                    <tr>
                                                        <th>真实姓名：
                                                        </th>
                                                        <td>
                                                            <span class="text1_bg">
                                                                <input name="txtName" type="text" value="小孩" maxlength="10" id="txtName" disabled="disabled" class="aspNetDisabled text1">
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>电子邮箱：
                                                        </th>
                                                        <td>
                                                            <span class="text1_bg">
                                                                <input name="txtEmail" type="text" maxlength="32" id="txtEmail" class="text1">
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>QQ号码：
                                                        </th>
                                                        <td>
                                                            <span class="text1_bg">
                                                                <input name="txtQQ" type="text" maxlength="15" id="txtQQ" class="text1">
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>联系电话：
                                                        </th>
                                                        <td>
                                                            <span class="text1_bg">
                                                                <input name="txtPhone" type="text" maxlength="15" id="txtPhone" class="text1">
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <input type="submit" name="btnUpdateInfo" value="完善资料" id="btnUpdateInfo" class="btn">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="prompt_content">
                                                <h2>温馨提示</h2>
                                                1、先完善资料后才能绑定支付宝账户信息。<br>
                                                2、该资料只能提交一次，若需要修改，请联系我们的客服人员。<br>

                                                
                                            </div>
                                        </form>
                                        
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
      </div> 
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>