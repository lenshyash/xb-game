var base = $("#base").val();
$(function(){
	// 所有文本只能输入数字
	$("#main").on("keydown","input[type=text]",function(e){
		var code = null;
		// 只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 // 48 - 57 上排数字键 96 - 105 数字键盘 8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {  
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	
	var navId = $("#navId").val(),header=$("#header_plus");
	if(navId ==0){header.find("h2>a").hide();$("#lotterysList").show();}
//	if(!navId){
//		$("header_box ul.nav-plus li").removeClass("on");
//	}else{
//		$("#header_box ul.nav-plus li:eq("+navId+")").addClass("on");
//	}
	
	header.on("mouseenter mouseleave","#lotterys",function(event){
		var t = $(this);
		if(navId==0){return;}	//如果是index，则不执行
		if(event.type == 'mouseenter'){
			$("#lotterysList").show();
			t.find("h2").addClass("on-lottery");
		}else{
			$("#lotterysList").hide();
			t.find("h2").removeClass("on-lottery");
		}
	}).on("click","#header_show_money",function(){
		var m = $(this).siblings("#wallet_container").find("input").val(),t=$(this).siblings("#wallet_container").find("#header_user_money"),
		text = $(this).text().trim();
		if(t.hasClass("dashed_bottom")){
			//说明已显示余额，需要用*代替
			$(this).text('显示'+text.substring(2,text.lengt));
			t.text("******");
			t.removeClass("dashed_bottom");
		}else{
			$(this).text('隐藏'+text.substring(2,text.lengt));
			t.text(m);
			t.addClass("dashed_bottom");
		}
	}).on("mouseover mouseout","#mylottery",function(event){
		var t = $(this);
		if(event.type == 'mouseover'){
			t.addClass("mylottery-on");
			t.find("#mylottery_dropdown").show();
		}else{
			t.removeClass("mylottery-on");
			t.find("#mylottery_dropdown").hide();
		}
	})
	
	$("#lotterysList").on("mouseover mouseout","ul li.allGames",function(event){
		var t= $(this);
		if(event.type == 'mouseover'){
			t.addClass("allGames-on");
			t.find(".open-btn").hide().siblings("div").show();
		}else{
			t.removeClass("allGames-on");
			t.find(".open-btn").show().siblings("div").hide();
		}
	})
	
	
	var main = $("#main");
	main.on("click","#a_show_money",function(){
		var t= $(this),m = t.parent().siblings("#user_money").val();
		t.html('<span class="colorRed">￥'+m+'</span>');
	}).on("mouseenter",".quick-tab-list li",function(){
		if($(this).hasClass("on")){return;}
		var t = $(this).attr("c");
		var type = $(this).attr("type");
		$(this).addClass("on").siblings().removeClass("on");
		//号码执行随机抽取并先跳动后显示
		Base._indexHaoMaRandom(type,t,$(this));
	}).on("click","#ssq_random",function(){
		var $on = main.find(".quick-tab-list li.on"),c=$on.attr("c"),type=$on.attr("type");
		Base._indexHaoMaRandom(type,c,$on);
	}).on('click','.tz_bei_add',function(){
		var multiple = $("#ssq_multiple"),mul=parseInt(multiple.val());
		if(mul < 999){
			multiple.val(mul+1);
			$("#ssq_amount").text((mul+1)*Base.price);
		}
	}).on('click',"#ssq_submit_index",function(){
		//首页下注
		var t = $(".qb-box-list .qb:visible"),numArr=[],cookieData = {lotCode:t.attr("c"),mul:$("#ssq_multiple").val(),lotType:t.attr("type")};
		t.find(".qb-selectnumber ul li").each(function(index,num){
			num = $(num).find("input");
			numArr[index] = num.val();
		})
		cookieData.num = numArr;
		$.cookie("indexBuyData", JSON.stringify(cookieData),{expires:30});
		window.location.href=base+'/lotteryV3/lotDetail.do?lotCode='+t.attr("c");
		
	}).on("click",".tab-box li",function(){
		$(this).addClass("on").siblings().removeClass("on");
		var c = $(this).attr("data-type");
		Base.getLotJointData(c);
	}).on("keyup blur","#buynum,#jq_match_list input.jointNum",function(){
		var t=$(this), om = t.attr("m"),bfl=t.attr("bfl").replace("%",""),surNum = t.attr("sur_num"),v=t.val();
		if(!v || v == 0){
			layer.tips("认购份数必须大于1份",t,{tips:[1,'#f13131']});
			t.val(1);
			v = 1;
		}
		surNum = parseInt(surNum);
		if(v>surNum){
			layer.tips("最多还可认购"+surNum+"份",t,{tips: [1, '#f13131']});
			t.val(surNum);
			v = surNum;
		}
		$("#permoney").text("￥"+(v*om).toFixed(2));
		$("#szBFL").text((v*bfl).toFixed(2)+"%");
	}).on("click","#addproject",function(){
		var t = $(this),p_id = t.attr("pro_id"),layerIndex,content="",nm=$("#header_user_money_hidden").val(),
			_input=$("#buynum"),num=_input.val(),sur_num = _input.attr("sur_num"),om=_input.attr("m"),bfl=_input.attr("bfl").replace("%","");
		if(num*om>nm){
			layer.msg("余额不足，请充值",{icon:2,offset:['30%']});
			return false;
		}
		buyJointPurchase(t.attr("p_id"),num,om,bfl,p_id);
	}).on("click","#jq_match_list div.show-details",function(){
		if(iL==1){loginBtn();return false;}
		var t = $(this),p_id = t.attr("pro_id"),layerIndex,content="",nm=$("#header_user_money_hidden").val(),
		_input=t.parent().siblings("td").find('input.jointNum'),num=_input.val(),sur_num = _input.attr("sur_num"),om=_input.attr("m"),bfl=_input.attr("bfl").replace("%","");
		if(num*om>nm){
			layer.msg("余额不足，请充值",{icon:2,offset:['30%']});
			return false;
		}
		buyJointPurchase(t.attr("p_id"),num,om,bfl,p_id);
	}).on("click",'.tz_bei_sub',function(){
		var multiple = $("#ssq_multiple"),mul=multiple.val();
		if(mul > 1){
			multiple.val(mul-1);
			$("#ssq_amount").text((mul-1)*Base.price);
		}
	}).on("keyup blur","#ssq_multiple",function(){
		var it = $(this),v=it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v <1){
			it.val(1);
    		return false;
    	}else{
    		$("#ssq_amount").text(parseInt(v)*Base.price);
    	}
	}).on("keypress",digitOnly);
	
	//login
	var log = $("#_form_login");
	log.on("click focus","#logVerifyCode",function(){
		$(this).removeClass('error').siblings('div[name=yzm_img_div]').show().siblings("div[name=tip_yzm_img]").hide();
		$('#reg-notice-icon-login').hide();
	})//.on("focus","#logVerifyCode",function(){
		//Base.verifyImg();
	//})
	.on("blur","#logVerifyCode",function(){
		var t = $(this),v = t.val(),_id=t.attr('id');
		if(!v){
			t.addClass('error');
			$('#reg-notice-icon-login').attr('class','reg-notice-icon').show();
			return;
		}
		$.ajax({
			url:base + '/lotteryV3/is_verify.do',
			data:{name:v},
			success:function(r){
				if(r.success){
					t.removeClass('error');
					$('#reg-notice-icon-login').attr('class','reg-notice-login-ok').show();
				}else{
					t.addClass('error');
					$('#reg-notice-icon-login').attr('class','reg-notice-icon').show();
				}
			}
		})
	})
	//register
	var reg = $("#reg_form_1");
	reg.on("blur",'input',function(){
		var t=$(this), val = t.val(),_unique = t.attr("unique"),_id= t.attr("id"),_t = t.attr("t"),key = t.parent().siblings('.info-left').text().replace("*","").replace("：","");
		if(!val){
			Base.vaildate.registerErrorHtml(t,key+'不能为空');
			return;
		}
		switch(_id){
			case "account":
				if(!(val.length>=5 && val.length<=11)){
					Base.vaildate.registerErrorHtml(t,key + '长度在5-11个字符之间');
					return ;
				}
				break;
			case "passwd":
				if(!(val.length>=6 && val.length<=16)){
					Base.vaildate.registerErrorHtml(t,key + '长度在6-16个字符之间');
					return;
				}
				break;
			case "conpasswd":
				if(!(val.length>=6 && val.length<=16)){
					Base.vaildate.registerErrorHtml(t,key + '长度在6-12个字符之间');
					return;
				}
				var v = $("#passwd").val();
				if(v != val){
					Base.vaildate.registerErrorHtml(t,key + '两次密码不一样');
					return;
				}
				break;
			case "receiptPwd":
				break;
			case "userName":
				var reg = /^[\u4e00-\u9fa5]+$/;
				if(!reg.test(val)){
					Base.vaildate.registerErrorHtml(t,key + '输入格式错误');
					return;
				}
				break;
		}
		if(!(!_unique) && _unique == 1){
			Base.vaildate.userExits(_id,val);
		}
		t.attr("s","ok").siblings('.reg-notice-ok').removeClass('hidn').parent().siblings("#_focus").hide();
	}).on("focus",'input',function(){
		$(this).siblings('.reg-notice-ok').addClass('hidn');
		$(this).removeClass("error").parent().siblings("#_focus").show().siblings("#_blur").hide();
	})
	
	
	//member
	var order = $("#betResultOrder");
	order.on("click",".BetInf",function(){
		var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode"),content="",sm="";
    	$.ajax({
    		url:base+'/lotteryV3/betInfo/getBcLotteryOrderDetail.do',
    		data:{
    			orderId : orderId,
    			lotCode : lotCode,
    			},
    		success:function(r){
    			if(r.winZhuShu == null || r.winZhuShu==''){
    				r.winZhuShu = 0;
    			}
    			if(r.winMoney == null || r.winMoney==''){
    				r.winMoney = 0;
    			}
    			if(r.rollBackMoney == null || r.rollBackMoney==''){
    				r.rollBackMoney = 0;
    			}
    			if(r.lotteryHaoMa == null || r.lotteryHaoMa==''){
    				r.lotteryHaoMa = '- -';
    			}
    			if(r.odds == null || r.odds == ''){
    				r.odds = 0;
    			}
    			//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
    			var yingkui = 0;
				if(r.status == 1 ){
					r.status='<span class="label label-1">未开奖</span>'
				}else if(r.status == 2 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='<span class="label label-2">已中奖</span>'
				}else if(r.status == 3 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='<span class="label label-3">未中奖</span>'
				}else if(r.status == 4 ){
					r.status='<span class="label label-2">已撤单</span>'
				}else if(r.status == 5 ){
					r.status='<span class="label label-4">回滚成功</span>'
				}else if(r.status == 6 ){
					r.status='<span class="label label-4">回滚异常</span>'
				}else if(r.status == 7 ){
					r.status='<span class="label label-4">开奖异常</span>'
				}else if(r.status == 8){
					r.status='<span  class="label label-4">和局</span>';
				}else if(r.status == 9){
					r.status='<span class="label label-4">合买失效</span>';
				}
				if(lotCode == 'LHC' || lotCode == 'SFLHC' || lotCode == 'TMLHC' || lotCode == 'WFLHC' || lotCode == 'HKMHLHC' || lotCode == 'AMLHC'){
					var teshuPL = "";
					if(r.minBonusOdds != null && r.minBonusOdds != 0){
						teshuPL = '<tr><th class="bg_black3">生肖本命年(0尾赔率)赔率</th><td colspan="3">'+r.minBonusOdds.toFixed(2)+'</td></tr>';
					}
					content = '<table width="100%" border="0" cellspacing="0" cellpadding="0"class="message_table" style="font-size: 12px; color: #000;"><tbody>' +
							'<tr class="bg_black3"><td colspan="4" class="open-title">订单号<span style="color: green">'+r.orderId+'</span></td></tr>'+
							'<tr><th class="bg_black3">帐号</th><td>'+r.account+'</td><th class="bg_black3">单注金额</th><td>'+(2.00/r.model).toFixed(2)+'</td></tr>'+
							'<tr><th class="bg_black3">下注时间</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数</th><td>'+r.buyZhuShu+'</td></tr>'+
							'<tr><th class="bg_black3">彩种</th><td>'+r.lotName+'</td><th class="bg_black3">基础赔率'+(LECAI.fanShui?"/反水":'')+'：</th><td>'+r.odds.toFixed(2)+(LECAI.fanShui?('/'+(r.rollBackRate||0)+'%'):"")+'</td></tr>'+
							'<tr><th class="bg_black3">期号</th><td>'+r.qiHao+'</td><th class="bg_black3">投注总额</th><td>'+r.buyMoney.toFixed(2)+'</td></tr>'+
							'<tr><th class="bg_black3">玩法</th><td>'+r.playName+'</td><th class="bg_black3">中奖注数</th><td>'+r.winZhuShu+'</td></tr>'+
							'<tr><th class="bg_black3">开奖号码</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖金额</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'+
							'<tr><th class="bg_black3">状态</th><td>'+r.status+'</td><th class="bg_black3">盈亏</th><td>'+yingkui.toFixed(2)+'</td></tr>'+
							'<tr>'+(LECAI.fanShui?('<th class="bg_black3">反水</th><td colspan="3">'+r.rollBackMoney.toFixed(2)+'</td>'):'')+'</tr>'+
							'<tr>'+teshuPL+'</tr>'+
							'<tr><td colspan="4">'+
							'<textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 50px; border: 1px solid #ccc; font-size: 12px;">'+r.haoMa+'</textarea>'+
							'</td></tr>'+
							'<tr><td style="color:red;" colspan="4">温馨提示:(连肖/合肖/尾数连--玩法中奖号码,既有本命年(0尾),又有非本命年(非0尾) 可能会出现2种赔率的情况)</td></tr></tbody></table>';
				}else{
					if(r.zhuiHao != 1){
	    				sm = "(追)";
	    			}
	    			if(r.jointPurchase != 1){
	    				sm = "(合)";
	    			}
					content = '<table width="100%" border="0" cellspacing="0" cellpadding="0"class="message_table" style="font-size: 12px; color: #000;"><tbody>' +
							'<tr class="bg_black3"><td colspan="4" class="open-title">订单号 <span style="color: green">'+r.orderId+''+sm+'</span></td></tr>'+
							'<tr><th class="bg_black3">帐号</th><td>'+r.account+'</td><th class="bg_black3">单注金额</th><td>'+(2.00/r.model).toFixed(2)+'</td></tr>'+
							'<tr><th class="bg_black3">下注时间</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数</th><td>'+r.buyZhuShu+'</td></tr>'+
							'<tr><th class="bg_black3">彩种</th><td>'+r.lotName+'</td><th class="bg_black3">倍数</th><td>'+r.multiple+'</td></tr>'+
							'<tr><th class="bg_black3">期号</th><td>'+r.qiHao+'</td><th class="bg_black3">投注总额</th><td>'+r.buyMoney.toFixed(2)+'</td></tr>'+
							'<tr><th class="bg_black3">玩法</th><td>'+r.playName+'</td><th class="bg_black3">奖金'+(LECAI.fanShui?"/反水":'')+'：</th><td>'+(r.odds/r.model).toFixed(2)+(LECAI.fanShui?('/'+(r.rollBackRate||0)+'%'):"")+'</td></tr>'+
							'<tr><th class="bg_black3">开奖号码</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖注数</th><td>'+r.winZhuShu+'</td></tr>'+
							'<tr><th class="bg_black3">状态</th><td>'+r.status+'</td><th class="bg_black3">中奖金额</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'+
							'<tr>'+(LECAI.fanShui?('<th class="bg_black3">反水</th><td>'+r.rollBackMoney.toFixed(2)+'</td>'):'')+'<th class="bg_black3">盈亏</th><td'+(LECAI.fanShui?">":' colspan="3">')+yingkui.toFixed(2)+'</td></tr>'+
							'<tr><td colspan="4">'+
							'<textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 50px; border: 1px solid #ccc; font-size: 12px;">'+r.haoMa+'</textarea>'+
							'</td></tr>'+
							'</tbody></table>';
				}
    			layer.alert(content,{title:"系统提示",area:['520px','auto'],offset : ['10%' ]});
    		}
    	})
	}).on("click",".cheDan",function(){
		var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode"),from=it.attr("from");
    	layer.confirm('您确定要撤销该订单？', {
    		  btn: ['确定','取消'],icon:3,offset : ['30%' ]
    		}, function(){
    			$.ajax({
    	    		url:base+'/lotteryBet/cancelOrder.do',
    	    		data:{
    	    			orderId : orderId,
    	    			lotCode : lotCode,
    	    			},
    	    		success:function(r){
    	    			if(r.success){
	    	    			layer.msg("撤单成功",{icon: 6});
	    	    			switch(from){
		    	    			case "member":if(typeof initRdsData!="undefined")initRdsData(curNavFlag);
		    	    				break;
		    	    			default:getBcLotteryOrder(1);
	    	    			}
    	    			}else{
    	    				layer.msg(r.msg,{icon:2});
    	    			}
    	    		}
    	    	})
    		}, function(i){
    			layer.close(i);
    		});
	})

	$("#regVerifyCode").focus(function(){
		var url = $("#base").val() + "/regVerifycode.do?timestamp=" + (new Date().getTime());
		$("#regVerifyImg").attr("src", url);
	})
});


var Base = {
		price:2,
		year:null,
		oneDay:86400,
		getLotJointData:function(c){
			var temp = "",data={};
			if(c){
				data={lotCode:c}
			}
			$.ajax({
				url:base + "/lotteryV3/joint/indexJoint.do",
				data:data,
				dataType:'json',
				type:'get',
				success:function(d){
					var bfb=0;
					if(d.length>0){
						$.each(d,function(i,item){
							temp += '<tr><td><span class="match-name match_'+item.lotCode+'">'+item.lotteryName.split("|")[0]+'</span></td>';
							temp += '<td>'+item.programId+'</td><td>'+item.account.substring(0,2)+'***</td>';
							temp += '<td>'+item.commission+'%</td><td>'+(item.totalMoney/item.branchNum).toFixed(2)+'元</td>';
							temp += '<td><a href="'+base+'/lotteryV3/joint/detail.do?programId='+item.id+'&amp;lotCode='+item.lotCode+'">查看</a></td>'
							bfb = (item.surNum/item.branchNum*100).toFixed(2);
							if(bfb > 100){
								bfb = "100.00";
							}
							temp += '<td style="text-align:left;padding-left:5px;"><span style="margin-left:0px;;color:000">'+bfb+'%</span> + <span style="margin-left:0px;color:#fff;background:#b90000;">'+(item.them!=0?(item.them/item.branchNum*100).toFixed(2):'0')+'%保</span>';
							temp += '<br><div style="width:120px;margin-top:3px;height:6px; font-size:0px; line-height:0px;border:1px solid #b90000;background:#fff;"><img src="'+base+'/common/template/lottery/lecai/images/joint/bar_space.gif" width="'+bfb+'%" height="6"></div></td>'
							temp += '<td class="bor js-home-lose sptd jq_sp"><span>'+(item.branchNum-item.surNum<0?'0':item.branchNum-item.surNum)+'份</span></td></td>';
							switch(item.type){
							case 1:
								if(item.branchNum - item.surNum == 0){
									temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">满员</td>';
								}else{
									temp += '<td class="bor js-home-lose sptd jq_sp"><input class="jointNum" type="text" sur_num="'+(item.branchNum-item.surNum)+'" m="'+item.oneMoney+'" bfl="'+(1/item.branchNum*100).toFixed(3)+'%" value="1"></td></td>';
									temp += '<td class="bor js-home-lose sptd jq_sp"><div class="show-details" pro_id="'+item.id+'" p_id="'+item.programId+'">购买</div></td>';
								}
								break;
							case 2:
								temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">满员</td>';
								break;
							case 3:
								temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">截止</td>';
								break;
							case 4:
								temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">完成</td>';
								break;
							case 5:
								temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">失效</td>';
								break;
							case 6:
								temp += '<td class="bor js-home-lose sptd jq_sp">--</td><td class="bor js-home-lose sptd jq_sp">撤单</td>';
								break;
							}
							temp += '</tr>';
						})
					}else{
						temp += '<tr><td colspan="10"><img src="'+base+'/common/template/lottery/lecai/images/joint/T1LAbBXlhnXXaMli7Z-341-110.jpg"></td></tr>';
					}
					$("#IndexJointPurchase").html(temp);
				}
			})
		},
		getLotCurrentData:function(c){
			var d = {lotCode:c,needLast:false}
			$.ajax({
				url: base + "/lotteryV3/lotterys.do",
				data:d,
				dataType:'json',
				type:'GET',
				success:function(res){
					Base.addCurData(res[c]);
					if(c=='LHC'){
						Base.year = res.year;
					}
					var on = $("#main .quick-tab-list li.on"),ac = on.attr("c"),type=on.attr("type");
					if(c == ac){
						Base._indexHaoMaRandom(type,c,on);
					}
				}
			})
		},
		addCurData:function(r){
			var c = r.lotCode, state=1;
			if(!r.id){
				state = 2;
			}
			$("#qb_"+c).find("#ssq_phase").html(r.qiHao);
			Base.timer(r.actionTime,c,state);
		},
		numberDay:function(d){
			if(Base.oneDay<d){
				return Math.floor(d/Base.oneDay);
			}
			return 0;
		},
		timer:function(t,c,s){
			var openTime = $.formatDate("yyyy-MM-dd hh:mm:ss",new Date(t),0);
			var nowTime = $.formatDate("yyyy-MM- dd hh:mm:ss",new Date(),0);
			var diff = $.baseTimerDiff(openTime,nowTime);
			Base._tick(diff,c,s);
		},
		_tick:function(d,c,s){
			var t = $(this),t1;
			t.stop(t1);
			if(d>0){
				var date = $.baseFormat(d,Base.numberDay(d));
				Base.setDateHtml(date,c);
				t1 = setTimeout(function(){
					d = d - 1;
					Base._tick(d,c,s);
				},1000);
			}
			if(d==0){
				//重新加载下一期
				Base.getLotCurrentData(c);
			}
		},
		setDateHtml:function(date,c){
			var d = date.split(":"),temp="",h=['时','分','秒'];
			for(var i=0;i<d.length;i++){
				temp += '<strong class="red">'+d[i]+'</strong>'+h[i]+'';
			}
			$("#qb_"+c).find("#ssq_sale_end_timer").html(temp);
		},
		stop:function(t){
			clearTimeout(t);
		},
		_indexHaoMaRandom:function(type,c,source){
			var num = 0,maxN = 0,minN = 0,needZero=false,t,x=0,temp="",repeat=false,_typeClass;
			switch(parseInt(type)){
			case 51:
			case 52:
			case 9:
				num = 5;
				minN = 0;
				maxN = 9;
				break;
			case 55:
				num = 3;
				minN = 1;
				maxN = 11;
				needZero = true;
				repeat = true;
				break;
			case 14:
				num = 5;
				minN = 1;
				maxN = 11;
				needZero = true;
				repeat = true;
				break;
			case 53:
			case 8:
				num = 3;
				minN = 1;
				maxN = 10;
				needZero = true;
				repeat = true;	//号码不能重复
				break;
			case 54:
			case 15:
				maxN = 9;
				num = 3;
				minN = 0;
				break;
			case 57:
				num = 3;
				minN = 0;
				maxN = 9;
				break;
			case 11:
				num = 5;
				minN = 0;
				maxN = 27;	//
				repeat = true;
				break;
			case 58:
				num = 3;
				minN = 3;
				maxN = 16;	//
				repeat = true;
				break;
			case 10:
				num = 3;
				minN = 4;
				maxN = 14;	//
				repeat = true;
				break;
			case 6:
			case 66:
				num = 7;
				minN = 1;
				maxN = 49;
				needZero = true;
				repeat = true;
				break;
			case 12:
				repeat = true;
				num = 5;
				minN = 1;
				needZero = true;
				maxN = 20;
				break;
			}
			for(var i=0;i<num;i++){
				temp += '<li class="index_kj_bg bg_icon"><input maxlength="2" value="" type="text" disabled="disabled">';
				if(parseInt(type) ==6 || parseInt(type)==66){
					temp += '<span></span>';
				}	
				temp += '</li>';
			}
			$("#qb_"+c).find(".qb-selectnum").html(temp);
			clearInterval(t);
			t = window.setInterval(function(){
				x++;
				var arr = new Array();
				for(var i=0;i<num;i++){
					var n = Base.diu_Randomize(minN,maxN);
					arr.push(n);
					if(needZero){n = Base.firstAddZero(n);}
					$("#qb_"+c).find(".qb-selectnum li:eq("+i+")>input").val(n).css({"color":parseInt(type)==6||parseInt(type)==66?'#000':"#fff"});
					if(parseInt(type) == 6 || parseInt(type) == 66){
						$("#qb_"+c).find(".qb-selectnum li:eq("+i+")").removeAttr("class").addClass("index_kj_bg mark_"+n).find("span").text(Base.lhcHaoMa.zodiacName(n,Base.year));
					}
				}
				if(repeat && (x == 6)){
					var arrs = arr.sort();
					for(var a=0;a<arr.length;a++){
						if(arrs[a] == arr[a+1]){	//最后一次号码重复重新执行
							x=5;
						}
					}
				}
				if(x == 6){clearInterval(t);}
			},50);
			source.parent().siblings(".qb-box-list").find("#qb_"+c+"").show().siblings(".qb").hide();
		},
		diu_Randomize:function(b,e){	//产生随机数
			if(!b && b!=0 || !e){return "?";}   
		    return Math.floor( ( Math.random() * e ) + b ); 
		},
		firstAddZero:function(n){
			n = parseInt(n);
			if(n<10){return "0"+n;}
			return n;
		},
		__ParseIntDecimal:function(a,b,l){	//a/b l次方
			var l = Math.pow(10,l);	//次方
			if(b==0){
				return Math.floor(a*l)/l;
			}
			return Math.floor(a/b*l)/l;
		},
		flashed:function(a,url){
			window.open(url,a);
		},
		verifyImgFalse:function(){
			var url = $("#base").val()+ "/verifycode.do?flag=false&timestamp=" + (new Date().getTime());
			$("#TopLogVerifyImg").attr("src", url);
		},
		verifyImg:function(){
			if($(this).attr("id")=="regVerifyCode"){
				var url = base + "/regVerifycode.do?timestamp=" + (new Date().getTime());
				$("#regVerifyImg").attr("src", url);
				return;	
			}
			var url = base + "/verifycode.do?timestamp=" + (new Date().getTime());
			$("#logVerifyImg").attr("src", url);
		},
		convertCurrency:function(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return layer.msg("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!",{icon:2}), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    },
	    alertfocue:function(msg,ele){
	    	layer.msg(msg,{icon:2});
			ele.focus();
			$("#sub-btn").removeAttr("disabled");
	    },
	    queRenCZXX:function(){
	    	$("#querenCZXX").show();
	    },
	    _goTo:function(url){
	    	window.location.href=url;
	    },
	    _goTo_open:function(url,nav){
	    	window.open(url,nav);
	    },
	    oneDayWinTotalAdd:function(num){
	    	 var hl = "",money=new Array(),name=new Array(),
	    	 cook = $.cookie('one_day_money');
	    	 if(cook == null || cook == ""){
		    	$(".news-bar-side table tr").each(function(index,html){
		    		money[index] = parseInt($(this).find(".p-r").text().replace("元",""));
		    		name[index] = $(this).find("td").eq(1).text().trim();
		    	})
		    	for(var j=money.length;j<num;j++){
		    		money[j] = Base.indexNum(50000,300000);
		    		name[j] = Base.randomWord(false,2,2);
		    	}
		    	money = money.sort(function(a,b){
		    		return b-a;
		    	})
		    	for(var i=1;i<=num;i++){
		    		hl += '<tr class="top"><td class="tc"><span class="top'+i+'_num">'+i+'</span></td><td class="tc">'+name[i-1]+'***</td><td class="tr p-r red">'+money[i-1]+'.00元</td></tr>';
		    	}
		    	$.cookie('one_day_money',hl,{expires:1});
		    }else{
		    	hl = cook;
		    }
	    	$(".news-bar-side table tbody").html(hl);
	    	
	    },
	    winOrderAdd:function(num,nameArr){
	    	var hl = "";
	    	$(".news-bar-zj li").each(function(index,html){
	    		hl +='<li>'+ $(this).html() + '</li>';
	    	})
	    	//添加虚假数据
	    	var name,lotName,money=0;
	    	nameArr = nameArr.substring(1,nameArr.length);
	    	nameArr = nameArr.split(",");
	    	for(var i=0;i<num;i++){
	    		name = Base.randomWord(false,2,2);
	    		lotName = nameArr[Base.indexNum(0,nameArr.length)];
	    		money = Base.indexNum(2000,100000);
	    		hl += '<li>'+name+'***&nbsp;喜中 <b style="green">'+lotName+'</b><span style="color:red">&nbsp;&nbsp;'+money+'.00元</span></li>';
	    	}
	    	$(".news-bar-zj").html(hl);
	    },
	    indexNum:function(min,max){
	    	return Base.diu_Randomize(min,max);
	    },
	    randomWord:function(type,min,max){
	    	 var str = "",
	         range = min,
	    	 arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
		     // 随机产生
		     if(type){
		         range = Math.round(Math.random() * (max-min)) + min;
		     }
		     for(var i=0; i<range; i++){
		         pos = Math.round(Math.random() * (arr.length-1));
		         str += arr[pos];
		     }
		     return str;
	    }
	    
};

Base.vaildate = function(){
	return {
		userExits:function(_id,_v){
			var url;
			if(_id == 'regVerifyCode'){	//验证
				url = base + '/lotteryV3/is_verify.do';
			}else{
				url = base + '/lotteryV3/user_exits.do';
				return;
			}
			$.ajax({
				url:url,
				data:{name:_v},
				success:function(r){
					if(r.success){
						$("#"+_id).attr("s","ok").siblings('.reg-notice-ok').removeClass('hidn').parent().siblings("#_focus").hide();
					}else{
						Base.vaildate.registerErrorHtml($("#"+_id),r.msg);
					}
				}
			})
		},
		userLogin:function(type){
			if(!type){
				var tipsType = 2;	//首页登录验证码
				var account = $("#username_login"),pwd = $("#passwd_login"),vc= $("#logVerifyCode"),data={};
			}else{
				var tipsType =1;	//头部验证码
				
				var account = $("#top_username_login"),pwd = $("#top_passwd_login"),vc= $("#top_logVerifyCode"),data={};
			}
			if(!account.val()){
				layer.tips("用户名不能为空",account,{tips:[tipsType,'#f13131']});
				account.focus();
				return false;
			}
			if(!pwd.val()){
				layer.tips("密码不能为空",pwd,{tips:[tipsType,'#f13131']});
				pwd.focus();
				return false;
			}
			if(!vc.val()){
				layer.tips("验证码不能为空",vc,{tips:[tipsType,'#f13131']});
				vc.focus();
				return false;
			}
			if(!account.val() && account.val().length <4){
				layer.tips("用户名不能小于4位数",account,{tips:[tipsType,'#f13131']});
				account.focus();
				return false;
			}
			if(!pwd.val() && pwd.val().length<6){
				layer.tips("密码不能小于6位数",pwd,{tips:[tipsType,'#f13131']});
				pwd.focus();
				return false;
			}
			data['account'] = account.val();
			data['password'] = pwd.val();
			data['verifyCode'] = vc.val();
/*			layer.load(1,{shade: [0.3,'#000'],skin:''});
*/			Base.submit.userLoginSub(data,account.attr("data"),type);
			return true;
		},
		registerErrorHtml:function(source,msg){
			source.addClass('error').parent().siblings('#_blur').show().html('<div class="reg-notice-icon"></div><font color="#e51937">'+msg+'</font>').siblings("#_focus").hide();
			source.attr("s","error");
		},
		userRegister:function(url){
			var account = $("#account"),pwd = $("#passwd"),cPwd = $("#conpasswd"),verifyCode=$("#regVerifyCode"),flag=true;
			$.map( [account,pwd,cPwd,verifyCode], function(n){
				var $it=$(n), s = $it.attr('s'),v=$it.val(),key= $it.parent().siblings('.info-left').text().replace("*","").replace("：","");
				if(s=='error' || !v){
					flag=false;
					var msg = !v?key+'不能为空':key+'填写有误';
					layer.tips(msg,$it);
					$it.focus();
					return flag;
				}
			});
			var check = $("#reg_form_1 input[name=reg_checkbox]");
			if(!$("#reg_form_1 input[name=reg_checkbox]").is(":checked")){
				layer.tips("请勾选服务协议!",check,{tips:1});
				return;
			}
			var status = $("#registerStatus").val();
			if(!status){	//用户注册
				if(flag){
					var data = getCommitData();
					data['account'] = account.val();
					data['password'] = pwd.val();
					data['rpassword'] = cPwd.val();
					data['verifyCode'] = verifyCode.val();
					layer.load(1,{shade: [0.3,'#000'],skin:''});
                    console.log(data)
					Base.submit.userRegisterSub(data,url);
				}
			}else if(status == 'agent'){	//代理注册
				var data = getCommitData();
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				layer.load(1,{shade: [0.3,'#000'],skin:''});
				Base.submit.dailiRegisterSub(data,url);
			}else{	//试玩注册
				var data = {};
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				layer.load(1,{shade: [0.3,'#000'],skin:''});
				Base.submit.userRegisterSub(data,url);
			}
		}
	}
}();


Base.submit = function(){
	return {
		userLoginSub:function(data,reUrl,type){
			$.ajax({
				url:base + "/login.do",
				data:data,
				success:function(res){
					layer.closeAll('loading');
					if(res.success){
						if(!reUrl){	//
							var recUrl = window.location.href.split('=')[1];
							recUrl = !recUrl?base+'/lotteryV3/index.do':recUrl;
							Base._goTo(recUrl);
						}else{
							window.location.reload();
						}
					}else{
						layer.msg(res.msg,{icon:5});
						if(type == 1 || type == "top"){
							Base.verifyImgFalse();
						}else{
							Base.verifyImg();
						}
					}
				}
			})
		},
		userRegisterSub:function(data,url){
			console.log(data)
			url=url || (base + '/register.do');
			$.ajax({
				url:url,
				data : {data:JSON.encode(data)},
				success:function(res){
					layer.closeAll('loading');
					if(res.success){
						layer.alert('注册成功',{icon:1,offset : ['30%' ]},function(index){
							location.href = base + '/lotteryV3/index.do';
							layer.close(index);
						},3000);
					}else{
						layer.msg(res.msg,{icon:5});
						$("#regVerifyCode").focus();
					}
				}
			})
		},
		dailiRegisterSub:function(data,url){
			url=url || (base + '/agtregister.do');
			$.ajax({
				url:url,
				data : {data:JSON.encode(data)},
				success:function(res){
					layer.closeAll('loading');
					if(res.success){
						layer.alert('注册成功',{icon:1,offset : ['30%' ]},function(index){
							location.href = base + '/daili';
							layer.close(index);
						},3000);
					}else{
						layer.msg(res.msg,{icon:5});
						$("#regVerifyCode").focus();
					}
				}
			})
		}
	}
}();



Base.onlines = function(){
	return {
		initOnlines:function(u){
			$.ajax({
				url : u,
				success : function(result) {
					var html = template('inputdata_tpl', result);
					$("#depostList").html(html);
					Base.onlines.bindIptData();
					pays = result.onlines;
				}
			});
		},
		bindIptData:function(){
			var $from = $("#depostList");
			$from.delegate( "input[name='amount']", 'keyup', function(){
		        /* 取出输入的金额,并转换成中文 */
		        $( "#amount_CH" ).text(Base.convertCurrency( $( this ).val() ) );
		    });
		    $("#onlines_deposit_div_id").find("ul li").click(function(e) {
		        var $radio = $(this).find('input[type=radio]');
		        if (!$radio.prop('checked')) {
		            $radio.trigger('change');
		        }
		    }).on('change', ':radio', function() {
		        var $it = $(this),maxV='';$it.prop('checked', true);
		       	$it.parent().parent().siblings('li').find('a').removeClass('hot');
		       	$it.parent().addClass('hot');
		       	if($it.attr("max")){
		       		maxV = '，最高<span id="max_amount" class="red">'+$it.attr("max")+'</span> 元';
		       	}
		       	$('.cz-limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+$it.attr("min")+'</span> 元 '+maxV+'）');
		       	$from.find('table').removeClass('hidn');
		    });
		    
		    $('#sub-btn').click(function() {
		        var $it=$(this),paySetting = $from.find("input[name='payId']:checked"),
		        payId = paySetting.val(),$input=$from.find("input[name='amount']"),
		        amount1 =$input.val(),
		        min = paySetting.attr("min"),
		        max = paySetting.attr("max");
		        $it.attr("disabled","disabled");
		        if(!payId){
		        	Base.alertfocue("请选择支付方式",$it);return false;
		        }
		        try{
		            min = parseFloat(min,10);
		        }catch(e){min = 0;}
		        try{
		            max = parseFloat(max,10);
		        }catch(e){max = 0;}
		        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
		        	Base.alertfocue("请输入充值正确金额",$input);
		            return false;
		        }
		        amount1 = parseFloat(amount1,10);
		        if(amount1<min){
		        	Base.alertfocue("充值金额必须不小于"+ min,$input);
		            return false;
		        }
		        if(max>0 && amount1 > max){
		        	Base.alertfocue("充值金额必须不大于"+ max,$input);
		            return false;
		        }
		        var iconCss = paySetting.attr("iconCss");
		        var payType = paySetting.attr("payType");
		        Base.onlines.dptcommit(amount1,payId,iconCss,payType);
		        return false;
			});
		},
		dptcommit:function(m,payId,iconCss,payType){
			Base.queRenCZXX();
			topay(m, payId, iconCss, payType, function(result){
				if(result.success == false){
					layer.msg(result.msg,{icon:2});
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toOnlinePayTemplate', result.data);
						$("#chargerinit").html(html);
					}else{
						layer.msg("系统发生错误",{icon:2});
					}
				}
			});
		}
	}
}();

Base.fasts = function(){
	var pays=[],isInitCopyed=false;
	return {
		initFasts:function(u){
			$.ajax({
				url : u,
				success : function(result) {
					var html = template('inputdata_tpl', result);
					$("#depostList").html(html);
					pays = result.fasts;
					Base.fasts.bindIptData();
				}
			});
		},
		bindIptData:function(p){
			var $from = $("#fastDepositTableId");
		    $from.on( 'keyup',"#amount",  function(){
		        /* 取出输入的金额,并转换成中文 */
		        $( "#amount_CH" ).text(Base.convertCurrency( $( this ).val() ) );
		    });
		    $('#depostList ul.ws_bank_new li').click(function(){
				var $it=$(this),flabel=$it.attr("flabel"),id=$it.find("input").val();
				if(!id)return;
				$it.find("input").prop('checked', true);
				if(flabel){
					$("#deposit_pay_type_id").show();
					$('.payType').html(flabel);
					payType = flabel;
				}else{
					$("#deposit_pay_type_id").hide();
				}
				$it.siblings('li').find('a').removeClass('hot');
				$it.find('a').addClass('hot');
				var curPay={};
				for (var i = 0; i < pays.length; i++) {
					if(pays[i].id == id){
						curPay = pays[i];
						break;
					}
				}
				$from.removeClass('hidn');
				if(showPayInfo){
					$("#pay_type_name").html(curPay.payName);
					if(curPay.qrCodeImg){
						$("#qrcode_img_wrap").removeClass("hidn");
						$("#qrcode_img_id").attr("src",curPay.qrCodeImg);
					}else{
						$("#qrcode_img_wrap").addClass("hidn");
					}
					$('#Bname_info').html(curPay.payUserName);
					$('#Baccount_info').html(curPay.payAccount);
				}
		       	$('#dan_bi_limit').html('（单笔充值限额：最低 <span class="red">'+curPay.min+'</span> 元 ，最高<span class="red">'+curPay.max+'</span> 元）');
		       	Base.fasts.curPay=curPay;
		       	if(!isInitCopyed){
		       		isInitCopyed=true;
		       		Base.fasts.bindCopy();
		       	}
			});
		},
		checkFastSub:function(obj){
			var $it=$(obj);
			if($it.attr("disabled")){
	       	return false;
	       }
	       $it.attr("disabled","disabled");
			var $from = $("#depostList") ,paySetting = $from.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=$('#amount'), amount1 =$input.val(),
	        bank_cards = $from.find("input[name='bank_cards']").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        if(!payId){
	        	Base.alertfocue("请选择支付方式",$it);return false;
	        }
			
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	Base.alertfocue("请输入充值正确金额",$input);
	            return false;
	        }
	        
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	Base.alertfocue("充值金额必须不小于"+ min,$input);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	Base.alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        if($("#deposit_pay_type_id:visible").length>0){
		        if (!bank_cards ||bank_cards.length==0) {
		        	Base.alertfocue("请输入存款帐号",$from.find("input[name='bank_cards']"));
		            return false;
		        }
	        }
			$.ajax({
				url : base+"/center/banktrans/deposit/dptcommitb.do",
				data : {
					money : amount1,
					bankCards : bank_cards,
					payId : payId
				},
				success : function(result) {
					if(!result.success && result.msg){
						layer.msg(result.msg,{icon:2});
						$it.removeAttr("disabled")
						return;
					}
					var curPay = {};
					for (var i = 0; i < pays.length; i++) {
						if(pays[i].id == payId){
							curPay = pays[i];
							break;
						}
					}
					curPay.money = amount1;
					curPay.orderNo = result;
					$("#chargerinit").html(template('toFastPayTemplate',curPay));
					Base.fasts.curPay=curPay;
					Base.fasts.bindCopy();
				}
			});
	        
		},
		bindCopy:function(){
			$("#Bname").zclip({
				path : base+'/common/js/pasteUtil/ZeroClipboard.swf',
				copy : function(){
                    return Base.fasts.curPay.payUserName;
                },
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
			$("#Baccount").zclip({
				path : base+'/common/js/pasteUtil/ZeroClipboard.swf',
				copy : function(){
                    return Base.fasts.curPay.payAccount;
                },
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
			$("#Boid").zclip({
				path : base+'/common/js/pasteUtil/ZeroClipboard.swf',
				copy : Base.fasts.curPay.orderNo+"",
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
		}
	}
}();

//返回到底部
function goBottom(){
	$('html,body').animate({'scrollTop':$(document).height()},600);
}

Base.banks = function(){
	var pays=[],isInitCopyed=false;
	return {
		initBanks:function(u){
			$.ajax({
				url : u,
				success : function(result) {
					var html = template('inputdata_tpl', result);
					$("#depostList").html(html);
					pays = result.banks;
					Base.banks.bindIptData();
				}
			});
		},
		bindIptData:function(){
			var $from = $("#bank_deposit_table_id");
		    $from.on('keyup',"#amount",function(){
		        /* 取出输入的金额,并转换成中文 */
		        $("#amount_CH").text(Base.convertCurrency( $( this ).val() ) );
		    });
		    $('#banks_deposit_divid ul.ws_bank_new li').click(function(){
		    	var $it = $(this),id=$it.find("input").val();
		    	if(!id)return false;
		    	var curBank={};
				for (var i = 0; i < pays.length; i++) {
					if(pays[i].id == id){
						curBank = pays[i];
						break;
					}
				}
				$from.removeClass('hidn');
				$it.find("input").prop('checked', true);
		    	$it.parent().parent().siblings('li').find('a').removeClass('hot');
		       	$it.parent().addClass('hot');
		       	if(showPayInfo){
			       	$("#cz_yin_hang_id").html(curBank.payName);
					$("#Bname_info").html(curBank.creatorName);
					$("#Baccount_info").html(curBank.bankCard+"（转账后请输入金额提交订单）");
					$("#kai_hu_wang_dian").html(curBank.bankAddress);
		       	}
				$("#dan_bi_limit").html('（单笔充值限额：最低 <span class="red">'+curBank.min+'</span> 元 ，最高<span class="red">'+curBank.max+'</span> 元）');
				Base.banks.curBank=curBank;
		       	if(!isInitCopyed){
		       		isInitCopyed=true;
		       		Base.banks.bindCopy();
		       	}
		    });
		},
		checkBankSub:function(obj){
			var $it=$(obj);
			if($it.attr("disabled")){
				return false;
			}
			var $from = $("#banks_deposit_divid"),bank = $from.find("input[name='bankId']:checked"),
	         $input=$("#amount"), amount1 =$input.val(),
	         depositor = $("#depositor").val(),
	         bankId=bank.val(), min = bank.attr("min"), max = bank.attr("max");
	    	 if(!bankId){
	    		 Base.alertfocue("请选择支付银行",$it);return false;
	         }
	    	 try{
    	        min = parseFloat(min,10);
    	    }catch(e){min = 0;}
    	    try{
    	        max = parseFloat(max,10);
    	    }catch(e){max = 0;}
    	    if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
    	    	Base.alertfocue("请输入充值正确金额",$input);
    	        return false;
    	    }
    	    amount1 = parseFloat(amount1,10);
    	    if(amount1<min){
    	    	Base.alertfocue("充值金额必须不小于"+ min,$input);
    	        return false;
    	    }
    	    if(max>0 && amount1 > max){
    	    	Base.alertfocue("充值金额必须不大于"+ max,$input);
    	        return false;
    	    }
	         if (!depositor || !/^[\u4e00-\u9fa5]+$/.test(depositor)) {
	        	Base.alertfocue("请输入正确的存款人姓名",$("#depositor"));
	            return false;
	        }
	         
			var cmtData = {
					money : amount1,
					depositor : depositor,
					bankId : bankId
				};
			
			$.ajax({
				url : base+"/center/banktrans/deposit/dptcommitc.do",
				data : cmtData,
				type:'post',
				success : function(result) {
					if(!result.success && result.msg){
						layer.msg(result.msg,{icon:2});
						$it.removeAttr("disabled")
						return;
					}
					var curBank = {};
					for (var i = 0; i < pays.length; i++) {
						if(pays[i].id == bankId){
							curBank = pays[i];
							break;
						}
					}
					curBank.money = amount1;
					curBank.orderNo = result;
					$("#chargerinit").html(template('toBankPayTemplate', curBank));
					Base.banks.curBank=curBank;
					Base.banks.bindCopy();
					
				}
			});
				
		},
		bindCopy:function(){
			$("#Bname").zclip({
		        path: base+'/common/js/pasteUtil/ZeroClipboard.swf',
		        copy : function(){
                    return Base.banks.curBank.creatorName;
                },
		        afterCopy: function() {
		            $("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		        }
		    });
		    $("#Baccount").zclip({
		    	path: base+'/common/js/pasteUtil/ZeroClipboard.swf',
		        copy : function(){
                    return Base.banks.curBank.bankCard;
                },
		        afterCopy: function() {
		            $("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		        }
		    });
		    $("#Boid").zclip({
		    	path: base+'/common/js/pasteUtil/ZeroClipboard.swf',
		        copy: curBank.orderNo+"",
		        afterCopy: function() {
		            $("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		        }
		    });
		}
	}
}();

Base.xinXi = function(){
	return {
		subUserName:function(){
			var n =  $("#userName"), myReg = /^[\u4e00-\u9fa5]+$/;
			if(!n.val()){
				Base.xinXi.message(n,'真实姓名不能为空');
				return;
			}
			if(!myReg.test(n.val())){
				Base.xinXi.message(n,'真实姓名格式错误');
				return;
			}
			$.ajax({
				url:base + '/center/member/meminfo/commitUserName.do',
				data:{userName:n.val()},
				success:function(res){
					if(res.success){
						layer.alert('设置成功',{icon:1,offset:['30%']},function(index){
							window.location.reload();
							layer.close(index);
						})
					}else{
						layer.msg(res.msg,{icon:2,offset:['30%']});
					}
				}
			})
		},
		setPayPwd:function(){
			var pwd = $("#payPwd"), rpwd = $("#rPayPwd");
			if (!pwd.val()) {
				pwd.addClass('text-error');
				pwd.parent().siblings(".tdtip").find('span')
						.text("密码不能为空");
				return;
			}
			if (!rpwd.val()) {
				rpwd.addClass('text-error');
				rpwd.parent().siblings(".tdtip").find('span').text(
						"确认密码不能为空");
				return;
			}
			if (pwd.val().length <= 5) {
				pwd.addClass('text-error');
				pwd.parent().siblings(".tdtip").find('span').text(
						"密码不能小于6个字符");
				return;
			}
			if (rpwd.val().length <= 5) {
				rpwd.addClass('text-error');
				rpwd.parent().siblings(".tdtip").find('span').text(
						"确认密码不能小于6个字符");
				return;
			}
			if (pwd.val() !== rpwd.val()) {
				rpwd.addClass('text-error');
				rpwd.parent().siblings(".tdtip").find('span').text(
						"两次密码不一样");
				return;
			}
			$.ajax({
				url : base+"/center/member/meminfo/repwd.do",
				data : {
					pwd : pwd.val(),
					rpwd : rpwd.val()
				},
				success : function(result) {
					if(result.success){
						layer.alert('设置取款密码成功',{icon:1,offset:['30%']},function(index){
							window.location.reload();
							layer.close(index);
						})
					}else{
						layer.msg(result.msg,{icon:2});
					}
				}
			});
		},
		updPwd:function(type){
			var content = "";
			if(type == 1){	//登录密码修改
				var oPwd = $("#updOLoginPwd"),pwd = $("#updLoginPwd"),rPwd = $("#updRLoginPwd");
			}else{	//提款密码修改
				var oPwd = $("#updOPayPwd"),pwd=$("#updPayPwd"),rPwd=$("#updRPayPwd");
			}
			if(!oPwd.val()){
				Base.xinXi.message(oPwd,'原密码不能为空');
				return;
			}
			if(!pwd.val()){
				Base.xinXi.message(pwd,'新密码不能为空');
				return;
			}
			if(!rPwd.val()){
				Base.xinXi.message(rPwd,'确认密码不能为空');
				return;
			}
			if (pwd.val().length <= 5) {
				Base.xinXi.message(pwd,'密码不能小于6个字符');
				return;
			}
			if (rPwd.val().length <= 5) {
				Base.xinXi.message(rPwd,'确认密码不能小于6个字符');
				return;
			}
			if (pwd.val() !== rPwd.val()) {
				Base.xinXi.message(pwd,'两次密码不一样');
				return;
			}
			if(type ==1){
				content = "修改登录密码成功";
			}else{
				content = "修改提款密码成功";
			}
			$.ajax({
				url : base + "/center/member/meminfo/newpwd.do",
				data : {
					opwd : oPwd.val(),
					pwd : pwd.val(),
					rpwd : rPwd.val(),
					updType : type
				},
				success : function(result) {
					if(result.success){
						layer.alert(content,{icon:1,offset:['30%']},function(index){
							window.location.reload();
							layer.close(index);
						})
					}else{
						layer.msg(result.msg,{icon:2});
					}
				}
			});
		},
		setBanks:function(){
			var bank = $("#bankId"),bankId = bank.val(),exp = /^([1-9][\d]{1,18}|x|X)$/;
			if(!bankId){
				Base.xinXi.message(bank,'银行卡号不能为空');
				return;
			}
			if(!exp.test(bankId)){
				Base.xinXi.message(bank,'请输入正确的银行帐号');
				return;
			}
			var param = {};
			param["bankName"] = $("#bankName").val();
			param["userName"] = $("#userName").text();
			param["province"] = $("#province").val();
			param["city"] = $("#city").val();
			param["bankAddress"] = $("#bankcardaddress").val();
			param["cardNo"] = bankId;
			$.ajax({
				url : base+"/center/banktrans/draw/commitbkInfo.do",
				data : param,
				success : function(result) {
					if(!result.success){
						layer.msg(result.msg,{icon:2});
					}else{
						layer.alert('设置银行卡信息成功!',{icon:1,offset:['30%']},function(index){
							window.location.reload();
							layer.close(index);
						})
					}
				}
			});
		},
		message:function(_id,msg){
			_id.addClass('text-error').parent().siblings(".tdtip").find('span').text(msg);
		}
	}
}();


var startYear=1804,lanArr = [3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],hongArr=[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
	lvArr = [5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49],zodiacArray = ["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"];
Base.lhcHaoMa = function(){
	return {
		bose:function(haoMa){	//lhc波色获取
			haoMa = parseInt(haoMa);
			if($.inArray(haoMa,hongArr) >= 0){
				return "1";
			}else if($.inArray(haoMa,lanArr) >= 0){
				return "2";
			}else if($.inArray(haoMa,lvArr) >= 0){
				return "4";
			}else{
				return "1";
			}
		},
		zodiacName:function(haoMa,year){
			haoMa = parseInt(haoMa);
			var arr = new Array(),s="",nowYear=new Date().getFullYear();
			if(!year){
				year = nowYear;
			}else {
				nowYear = year;
			}
			for(var i=0;i<nowYear;i++){
				arr = Base.lhcHaoMa.zodiacHaoMaArray(i+1);
				if(arr != null){
					s = zodiacArray[Base.lhcHaoMa.subtractYear(nowYear + i) % 12];
					if($.inArray(haoMa,arr) >= 0){
						return s;
					}
				}
			}
		},
		subtractYear:function(year){
			if(year < startYear){	// 如果年份小于起始的甲子年(startYear = 1804),则起始甲子年往前偏移
				startYear = startYear - (60 + 60 * ((startYear - startYear) / 60));// 60年一个周期
			}
			return year - startYear;
		},
		zodiacHaoMaArray:function(age){
			switch(age){
				case 12:
					return new Array(2,14,26,38);
				case 11:
					return new Array(3,15,27,39);
				case 10:
					return new Array(4,16,28,40);
				case 9:
					return new Array(5,17,29,41);
				case 8:
					return new Array(6,18,30,42);
				case 7:
					return new Array(7,19,31,43);
				case 6:
					return new Array(8,20,32,44);
				case 5:
					return new Array(9,21,33,45);
				case 4:
					return new Array(10,22,34,46);
				case 3:
					return new Array(11,23,35,47);
				case 2:
					return new Array(12,24,36,48);
				case 1:
					return new Array(1,13,25,37,49);
				default:
					return null;
			}
		}
	}
}();


function buyJointPurchase(proId,num,om,bfl,p_id){
	var content = '<div id="model_body"><table><tbody><tr><th width="25%">方案编号</th><th width="25%">购买份数</th><th width="25%">购买金额</th><th width="25%">所占比例</th></tr>';
	content += '<tr><td>'+proId+'</td><td>'+num+'</td><td><strong class="red">'+Base.__ParseIntDecimal(om*num,0,3)+'</strong>元</td><td>'+(num*bfl).toFixed(2)+'%</td></tr>';
	content += '</tbody></table></div>';
	layer.open({
		type : 1, //page层
		area : [ '550px', 'auto' ],
		skin:'layui-layer-rim',
		title : '参与合买明细(请确认信息)',
		shade : 0.6, //遮罩透明度
		scrollbar : false,
		offset : '150px',//距离顶部
		moveType : 0, //拖拽风格，0是默认，1是传统拖动
		shift : 1, //0-6的动画形式，-1不开启
		content : content,
		btn: ['确定', '取消'],
		yes:function(index){
			layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
			var data = {
					buyNum:num,
					programId:p_id
			}
			 $.ajax({
				 url:base + '/lotteryV3/jointpate.do',
    			 type: 'POST',
                 dataType: 'json',
                 data: data,
                 success:function(data){
     				if(data.success){
     					layer.msg(data.msg,{icon:1,offset:['30%']},function(){
     						window.location.reload();//刷新当前页面.
     					});
     				}else{
     					layer.msg(data.msg,{icon:2,offset:['30%']});
     				}
     			},
                 complete: function(data){
                	 layer.close(layerIndex);
                	 layer.close(index);
                 }
    		 });
		}
	});
}

function loginBtn(){
	$('a[data-reveal-id]').trigger('click');
}


function refresh(){
	var money = $("#header_user_money"),money1=$("input.user_now_money"),money2=$("#a_show_money");
	$.ajax({
		url:base + "/meminfo.do",
		success:function(res){
			if(res.login){
				money.html(res.money);
				money1.val(res.money);
				money2.find('span').text(res.money);
			}
		},
		beforeSend:function(){
			money.html("加载中...");
			money2.find('span').html("加载中...");
		} 
	})
}


//订单
//1今天，2昨天，3本周，4上周，5本月，6上月
function quickSelDate(dateVal){
	var diff = 0;
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();
	var week = date.getDay();
	 //一天的毫秒数  
    var millisecond = 1000 * 60 * 60 * 24;
    //减去的天数  
    var minusDay = week != 0 ? week - 1 : 6;
	var startTime = "yyyy-MM-dd";
	var endTime = "yyyy-MM-dd";
	var startDate = "";
	var endDate = "";
	switch(dateVal){
	case 1:
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,date,diff);
	break;
	case 2:
		date.setDate(date.getDate()-1);
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,date,diff);
	break;
	case 3:
        startDate = $.formatDate(startTime,new Date(date.getTime()-(minusDay*millisecond)),diff);
        endDate = $.formatDate(endTime,date,diff);
	break;
	case 4:
        //获得当前周的第一天
        var currentWeekDayOne = new Date(date.getTime() - (millisecond * minusDay));
        startDate = $.formatDate(startTime,new Date(currentWeekDayOne.getTime() - (millisecond * 7)),diff);
        endDate = $.formatDate(endTime,new Date(currentWeekDayOne.getTime() - millisecond),diff);
	break;
	case 5:
		date.setDate(1);
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,new Date(),diff);
	break;
	case 6:
		//获取上一月的第一天
		var priorMonthFirstDay = getPriorMonthFirstDay(year, month);
		//获得上一月的最后一天  
        var priorMonthLastDay = new Date(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth(), this.getMonthDays(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth()));
        startDate = $.formatDate(startTime,priorMonthFirstDay,diff);
		endDate = $.formatDate(endTime,priorMonthLastDay,diff);
	break;
	}
	$("#startTime").val(startDate);
	$("#endTime").val(endDate);
}




/**
 * 返回上一个月的第一天Date类型
 * @param year 年
 * @param month 月
 **/
function getPriorMonthFirstDay(year,month){
	//年份为0代表,是本年的第一月,所以不能减  
    if (month == 0) {
        month = 11; //月份为上年的最后月份  
        year--; //年份减1  
        return new Date(year, month, 1);
    }
    //否则,只减去月份  
    month--;
    return new Date(year, month, 1); ;
}


/**
 * 获得该月的天数
 * @param year年份
 * @param month月份
 * */
function getMonthDays(year,month){
     //本月第一天 1-31  
     var relativeDate = new Date(year, month, 1);
     //获得当前月份0-11  
     var relativeMonth = relativeDate.getMonth();
     //获得当前年份4位年  
     var relativeYear = relativeDate.getFullYear();

     //当为12月的时候年份需要加1  
     //月份需要更新为0 也就是下一年的第一个月  
     if (relativeMonth == 11) {
         relativeYear++;
         relativeMonth = 0;
     } else {
         //否则只是月份增加,以便求的下一月的第一天  
         relativeMonth++;
     }
     //一天的毫秒数  
     var millisecond = 1000 * 60 * 60 * 24;
     //下月的第一天  
     var nextMonthDayOne = new Date(relativeYear, relativeMonth, 1);
     //返回得到上月的最后一天,也就是本月总天数  
     return new Date(nextMonthDayOne.getTime() - millisecond).getDate();
 };
 
 
//制保留2位小数，如：2，会在2后面补上00.即2.00 
 function toDecimal2(x) {
 	var f = parseFloat(x);
 	if (isNaN(f)) {
 		return false;
 	}
 	var f = Math.round(x * 100) / 100;
 	var s = f.toString();
 	var rs = s.indexOf('.');
 	if (rs < 0) {
 		rs = s.length;
 		s += '.';
 	}
 	while (s.length <= rs + 2) {
 		s += '0';
 	}
 	return s;
 }
 
 
 function digitOnly(e){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
          return true;  
      } else {
     	 //layer.tips("请输入整数",$(this),{tips:[1,'#f13131']});
          return false;  
      }  
	}