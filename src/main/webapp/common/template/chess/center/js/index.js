var base = $("#base").val();
var startTime,endTime;
function getMyDate(str){
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth()+1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay) +' '+ getzf(oHour) +':'+ getzf(oMin) +':'+getzf(oSen);//最后拼接时间
    return oTime ;
};
function today(){
	var year = new Date().getYear()+1900, month = new Date().getMonth()+1;
	var today = year + '-' +month +'-'+((new Date().getDate()));
	return today;
}
function getzf(num){
    if(parseInt(num) < 10){
        num = '0'+num;
    }
    return num;
}
function showMessage(content){
	layer.open({
		  type: 0,
		  skin: 'layui-layer-demo', //样式类名
		  closeBtn: 0, //不显示关闭按钮
		  anim: 2,
		  shadeClose: true, //开启遮罩关闭
		  content: '<div style="padding:10px;font-size:14px;">'+content+'</div>'
		});
}
var chessType = {
	1:'<span style="color:blue">未读</span>',
	2:'<span style="color:#600">已读</span>'
}
function lotteryPage(num,count){
	$("#pageCount").html(num)
	var conutPage = count - page;
	if(conutPage > 3){
		conutPage = 3
	}
	var html = '';
	 if(count > 6){
		 html = '<li><a onclick="initRdsData(1)">首页</a></li><li><a id="nextPage" onclick="locaPage(0)">上一页</a></li>';
		 if(count >6 && page>3){
			 for(var i = (page-3) ; i <= (page+conutPage); i++){
					if(i == page){
						html+= '<li><a  style="background:#eee;" href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
					}else{
						html+= '<li><a href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
					}
				} 
		 }else{
			 for(var i = 1 ; i <= 6; i++){
					if(i == page){
						html+= '<li><a  style="background:#eee;" href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
					}else{
						html+= '<li><a href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
					}
				} 
		 }
	 }else if(count < 6 && count >2){
		 html = '<li><a onclick="initRdsData(1)">首页</a></li><li><a id="nextPage" onclick="locaPage(0)">上一页</a></li>';
		 for(var i = 1 ; i <= count; i++){
				if(i == page){
					html+= '<li><a  style="background:#eee;" href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
				}else{
					html+= '<li><a href="javascript:(0)" onclick="initRdsData('+i+')">'+i+'</a></li>'
				}
		}
	 }else{
		 html = '<li><a id="nextPage" onclick="locaPage(0)">上一页</a></li>'
	 }
	html+='<li><a id="lastPage" onclick="locaPage(1)">下一页</a></li>'
	$(".pagination").html(html);
}
function locaPage(num){
	if(num == 0){
		if((page-1) == 0){
			layer.tips('已经是第一页了', '#nextPage', {
				  tips: [1, '#e4393c'],
				  time: 2000
			});

			return false;
		}else{
			initRdsData(page-1)
		}
	}else{
		if(page == lastPage){
			layer.tips('已经是最后一页了', '#lastPage', {
				tips: [1, '#e4393c'],
				time: 2000
			});
			return false;
		}else{
			initRdsData(page+1)
		}
	}
}
function unExist(un){
	if(typeof(un)=='undefined'){
		un = ''
	}
	return un;
}
var statusType = {
		1:"人工加款",
		2:"人工扣款",
		3:"在线取款失败",
		4:"在线取款",
		5:"在线支付",
		6:"快速入款",
		7:"一般入款",
		9:"反水加钱",
		10:"反水回滚",
		11:"代理反点加钱",
		12:"代理反点回滚",
		13:"多级代理反点加钱",
		14:"多级代理反点回滚",
		15:"三方额度转入系统额度",
		16:"系统额度转入三方额度",
		17:"三方转账失败",
		18:"活动中奖",
		19:"现金兑换积分",
		20:"积分兑换现金",
		23:"系统接口加款",
		79:"注册赠送",
		80:"存款赠送",
		130:"彩票投注",
		131:"彩票派奖",
		132:"彩票撤单",
		133:"彩票派奖回滚",
		134:"参与彩票合买",
		135:"彩票合买满员",
		136:"彩票合买失效",
		137:"彩票合买撤单",
		138:"彩票合买截止",
		139:"彩票合买派奖",
		140:"六合彩投注",
		141:"六合彩派奖",
		142:"六合彩派奖回滚",
		143:"六合彩撤单",
		201:"体育投注",
		202:"体育派奖",
		203:"体育撤单",
		204:"体育派奖回滚"
	}
var qukuanType = {
		1:'处理中',
		2:'处理失败',
		2:'处理成功',
}
var userType = {
		1:'玩家',
		4:'代理'
}