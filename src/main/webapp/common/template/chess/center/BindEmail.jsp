<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "104";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
    	<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>绑定邮箱</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <form method="post" action="./BindEmail.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="obLoZoNNSyJa/RkSwXPFRzEdsLS1rozBaaMWZ096O53t85HZCCiicOzeabsyC/s6KJ018SMt2/zlVZxCIFKXnmwoNHevuc3+pr/3GXruRps3gAbK4bWArMDNscmwehf5Y/+cwD7NB2XILtYgZ7pfbgxNHb++/rrz1XMAl856odTAR+Bqrx+dKGJrYngjtIuUmgrmPMT0R6rjz4ihPP+nQe08ySIW2DLFwnDTKNgEb4vvrBm6usaaSLmDt1UAs750vitC96xcws+Qg3jbSrUg0yXNc3X1BPavHojW3u0e2/cNIpp5idDPo13EXYuVya5QgSaMbBDA1G8eqlvaWTVvaaK0KB8te50a4SQutUwZbTI81LCJZr0kviTaGXGDdALL1sHjqVrvtRAEpqbsfDlZNe2TsEBgb9IJXDbuO7+WwqqA91fvoibVfQ0vf6OolMv9ZtT/ynKJ9iCf7L/oXtgQ+Thz/WvDt+/K4ZmpqR8SmBpzJQvYPuYilhxfZUeNb8ZQ29RdBp34nnIaM/fzW9bSr7RDJYYXRT4wNT9biggsAIQRDJv6xAXaBgi84aKpNOJSyCk0PQuz++mj1sRxlPj3LxvYFcJuuRR77w7uQlAXo2G/RpPWu8guzZYWNDyTRrEV68L9QhZUXkAzmPvM/7Hcd+KSChzaqwfLW9MjcBeudIJAkwbOmaiQA9jmhi6bZP/MQN2+ii4viIsSAWQ4H7IvT68uBGM=">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4DB10F30">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="cHUNG6hXCuJHdJDsFcQT7FM9KgBSBImf5+LyHxG4uoMUOhU0XFVJVizihAmLVbyPb+mX6Yts51pjatpBwPC8YjpABXKPhDtWsQG63cQf29THzNS5NFN74mo4qPW6p6XoqLFBAcLmAkn0UFxm5WYdddYMuJlhYqkqWwtH4A==">
</div>
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>绑定邮箱</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        
                                        <table id="t_info" width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
	<tbody><tr>
		<th>帐号： </th>
		<td>
                                                    <span id="lblAccounts">gosick</span>
                                                </td>
	</tr>
	<tr>
		<th>Email： </th>
		<td>
                                                    <span class="text1_bg">
                                                        <input name="txtEmail" type="text" maxlength="32" id="txtEmail" class="text1" validate="{required:true,email:true}"></span>
                                                </td>
	</tr>
	<tr>
		<td>
                                                    &nbsp;
                                                </td>
		<td>
                                                    <input type="submit" name="btnUpdate" value="绑定" id="btnUpdate" class="btn">
                                                    
                                                </td>
	</tr>
</tbody></table>

                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>游戏下载地址</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <ul class="download_link_list">
                                            
                                                    <li><a href="https://0178.oss-cn-shenzhen.aliyuncs.com/LYGameFull.exe" title="高速下载" target="_blank">
                                                        高速下载</a></li>
                                                
                                                    <li><a href="https://pan.baidu.com/s/1vhcLQJ3llMh8JY3qgt05sA" title="百度网盘下载" target="_blank">
                                                        百度网盘下载</a></li>
                                                
                                                    <li><a href="https://www.lyqp.com/GameIntroduces.aspx" title="官网下载" target="_blank">
                                                        官网下载</a></li>
                                                
                                        </ul>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            </form>
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>