<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "201";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
<div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>申请密码保护</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <div id="divRight">
                                        </div>
                                        <form method="post" action="./ApplyPassProtect.aspx" id="frmProtect">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Xk+hTpQKNrPxSXwpcthvc+p09j7MbUUdaqlptyCVxNgD0zZwX1YIT0rmLxrq470OoCmdb+bXMfqaOKPbx+wV7PRhsTz3esHYNoA3zozAbbtJ58lL2B0fYLXt91INubNHf2J/TZbFyTYqkhS+ULbQgQk9u3NKoPyu2eY4i5AoxOEeoVTa05AhUm8d/Q4sEvRWhnyWx30STfyo4+Sn3/w2h1oqoWhJtgc4QSVnn1Tn0sOgEhGSG79HxZq07nbuiObzCdp/NLqBPsFQsikhWLET1/jorx6+wVvBzNEONudKOtOFMyN9BKYgsRDBO1CXd4Z5pjYMdACD755pwtX3Lx+/EBa5mH8Nn2CmbyZMutsnDXg6AlTuf4tqQ5Bil/aXOEeUrIxCPNbowb0r0KpkJqPvG7hYji/VVRyiC5ZFbf+cRGOAkL0xCmA+1rRhHqPptP1mdPC/90/t7g3cHGnqU7MgeW4haDbuoOM+IKMCX5a1Hb5L6bkRq+G5Hu2vPqMAUVXxJZEHLX0bTqsBQAsvP82vHY0QiX+3/WGRe+n3u0rfXpg4v/5Ou2RATZFEcMl2/Bl1XF906lPGXpAIrAkdGIHBfCFfyNGyV/nUcWLI24apoksMqjemYMfT3QI41MBgb1pq7s8Q3nNs5lWHdyHsgfKt9LOXRejttp4qS/dWg8mNUDvspbPeZtAVVTUHcpp4QoikQcw60mvwrggBDPCKGfoIHvYsfuUPxNX9aiv1K5t24t5kcRmRiRjj4RrM4R5W21MGONwYlt6LD7qvqWlcx3i1MiUnsPJdvt2s/X98haTPrSVG1SJc4SwcNaCtDsDRV2SDV+7vT8vAZDUNxwAfPSlCLml7qxNlSYlPUE6hZXlGgGAfwEpo0Ll51uS5JQ6rEBj7zER4SmE5TWd7XLT+krLYsi+pIY2p5UyKXsAiloXcR0j8A4paiHYjSHsawI+WwnS5BOFzViOnV+u5DyK236iThFhkd8jC8k1zvS5rixeibKM5VhqEfnlKhln9iANnmcAD2bEZ4aEvym1AnBAde2gW/8Zbxu09a464iBAE4LIt95VPIBsSHHXCYMHsyo7C8aeSojNxlagHEf2Lsaa9uMvzDWQ7iL2n8h/BIiGg0sekR7jZkBeRwkwafVjsgcEiF+uwvRQ2Iw1mDVKrLlqqHO2+8tlZc17OAuJZYGNzrbJ7Bkapn+OhAwdhcu5pUtCiXr4n1aDVdCcyhLwhX7DN29QtVeZIl8AWUeecHXIUUcrZZwhqFFyMaaeYpJZvm9wl7h51wRWJ5/cLUiieFin9Fe15YzmT/acz4ASM21cOrrDzw/1cg0cgieFy+Qw6og652WHVcMYXN4RpbhJnQ/WrLUNvLm4ndA0aqKEjsucssanxEGfDRwWlr6i8scX3+9gV5KaHfJ6q2iVlP/fdSvYtVKGx9WdTSINmiIaAc469u56SbGfjRJ+L32cdQgGcYt6QIlAnXlEsQ/s7Cs6SZ/4NgL3AZvMERpkL0fHpwbxtvjZxDYUH6EBOM3On8QDNiJzmmS5aA5xoBxrgi1KhweCjjQGK9LDgdTaq63m3d5jkwAqZMxni50cG747hFJxTxi2Mm1yn2hdbZxx+dl4kTnWdAwU8tukve9ze8XUmiKiZNkcOEXerRhPCqLVulXUQegax7GQ9BqEU0xOvsnJozp6Ii0c87LdFi902azzcp6ZbONNdATbB4W/VGi7btNxy4LX+U3UCsFP/C8RcmKn3MaU1v1sPPT7YbuCT4aJy47tjWHOCxLAVhAe5HvXTfMnRBIBl0hdJUxjG1VsxWOiJ6i1XeP3ou3z5diBA1CM15GX09wCkQrNeexnsMuLR5N5W1hjlEhE8bV5wym4voBfPdCvSLgR0vYiFv8dP24DyHirKMpWXgOgbde72ahnMPcvlj4KgyjDHUAmvFEJqT+atgoWauq32IN7fAQr6HAnuSvGkBuDPcMnLYVXK/IBvj3FlcaeOEGPgRQYi3RTqxHjKuSDApPIW24MUmZO5D4juyRTe4MQdKq55IFPlrDN4SGQ+VybqEIwLh36bTYnDAwHndwRcPS9l3aWx54K0yk2tf9Q1IVYcj7llG7sm+ja/IvhP9JXKb84Owinvb/xhnyzs9fULovN7vXWYZvpoeslokxp7SZ9D/5XzyNrVrZgDHouNgi9D/7jVU/NK773aQ2SB9HxBcOjCKbKYnWMQLON+JZT2bxrLpXGRMDv4TS76NMfzj+OtoqM5qEVrMYXrcKUNIxTFbcNiuvvhzoUlUr4Ffr/t69eY4Gwcl/EJRiD6fpICUtQ5odLWNubD42v5ub8tWStb8cNSMwIteltMfxMNUYWmynAJjl+Qltpt1Dy3ntzBtjoDhW/4Xs6Mh96HVusqaX5twfWE0U32S7TN/hf0VE8RpA5Q0J7dCfBt3K0jks/puNyIxH7scbGnIaoXQpYWsAlxz8KkUiP6R5n3L2qLue+qkRDLeFUx6+onYCWxCTAcchByKJrpkFsmT9EXuTmr+bWwgL6BbK/n22i11JSwG2N41xzgsK8+EBS9qKOrTPpug6dgcussIDeOuBnY7kDL6yGyHE8uoHlGpD/0Ys7VQmsDQ9dX4dfVf2VwraD+jA/x+7xHWjUC9rvEQcjJ5oXz1ZY4HQHDZX0ejr2RdQ2fJCtBZgN2NdFBMYbhFuS6TXCTfOnqAHdaIQXsCjS28sJHTMztE8ON3tdE00paDQIMKoYsf/8mmT7qcN0Fs0kXcKSWKj+PFiJKGUJXxasHasrU6kJfVzJjxDKO/MxEIhACUu1NCcGVkbPXK1tGpsFZmJ081YchlwDvutwhU/tpPIvDuBUyr5wWq8VBuHFJvYtkvuPz8qA1NblX14zEteY39HjOIYqHWH3FfVWoAy5uH/fE6WnbTv9EIVCh/RLsvKNFsWc23wIROH4iDEVixurmdfpFvtVzCAhvxaVBgGB/uBspfuupHmRPEFOmPUXY5LpPvM/ntx7HoZ29kqsWyNjFjTkm4Un5txQ2cVwC3qRehHfNfx+wHPFoY2FyW8GMVIq0VC1t9ckiQ8vEziUQjcnbECXtJ6xA7KCRrnSmlElO3Ajw6BLYkrdMFExLwSFRS6ns5squkOUvLo7QNLVgf4aZOgBWZC1SG4V+f1GpQzviSUQ/wyUDU9T3DvyDc2rsnpAmjNlx4QjWibh96o8OE1uAdVWfaRM7piFMm45G3g4zAa3qUS12cmxrHqmRY3mCywLUF1A/x9dJpyNG5d6oUqZz9zbeqBwJhn05ZnQce9FR+4E4S0eK+cG5I5Z+60hYJYdNjWf4m3vZtU6/n5wszaaxtvyQJH/2H2BQfNHqywVelbjucfk3UUQR/tYVZqsGhDkiFz5AbIyGYPbgNq8HdvlVD2j9RY8gmNNqH1NaF3Z5hzIPqNudiPbtWjr61uJgI4A4dKIAQWc5zb3QNh5VISLcjM+3+A7l5514e3iGWXQKU1bMFGhND0TvKiI0EVhv74LkvvKXzzdSHVdIbmcd3TGVJeWQdqVS84Ex7ZTnCg11m03o4q9aCATSUxvX0yiO4bwqYWAZ+R8OhD/4uYfH5iuYvxERe49JfeVlDdGsieZ1uT647isPqYE8e/FDZMlzu+XaDWQtRHXP80lStsLd4IduRGvDLUW4m/qGwQnmiBqnl5H7SA8QG/POlfvQAXRGjhrSx4ToyrQxcGQWUrW1obzedfU3I+G4+vVsQRXSb9mnjGP6FtCsvEbnNxGb/JTSHOd0fgtRQHnhgcM9jDxW3ItWGitrPy20XP0/njx/p+sBkgf1G/kvAd3vcK1h44kr6zh45EYwVpEf1w2NmsFnWi45dGFBj8+4oX+TgeFFiHbHZsRe9cdl2micqGpABCIMXJATsCKT4Q==">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E0FEC2D">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="pg+ti7X//sCtsaPnOySJrMt8D9bA8RID8fFAOyKNDvfSQnJ915sR3oNG7oa5Atc8ZZGI3WltMWGz4xR4XI3MOBLi4+rQKsjkYDgTQtJOliRXwoqYRCm3LX0nZK9wrWakX3KSiR41OLj8fFYWcjk8Bszn5xpuKsD/WgmP2zK1Skpegav4qZ+Px+SiAgdpkOCj04DHNYzDqewcNJgS+3ZcrDItVHzRLTXUPBCxix2qwf0Rw4nwBAH71zK84MwjQ2SYkmIF7vmIVz6sPeFDcoAjJTUnZa1u7rOm0cRTcTlresyTFLji3hMlUioJyB8r/Z7ayFivFp+BtDbKVS8U1ZWZnb1LbRqsxRBCZ7jW0C4KQereROjc9AZo1Mzkp2X7X8TgHX5rA4sxVdwdLA59sxvXFLRICuWKP8LKdiW5FzJ2llo/ABsLB2R0rvjURO3+ZSJGMOPau2wTRANFdt/d3sxcKqncOqvw3AIlhkJTIqV74VIs8T6NNwoqgJ+Fdr/UL9tEMpdlCW+yDC6E+LHWXzR5gOxW4p7L0hAUb4gdICAIiCSCaaJUh81CkwNNBMQRC0ImfPRH2G9X64JZjQTVa9IKjE3MXckujB+ZenfHwNxSYSYAI5gdOcrPnXKHpMow1kPQJ8Vlx1bhRaDtQ3gcxScCUd9Hutp0zcoCxYJqSxPCo6Yqypkvx7PtShKY0p7hI9H4G7kGqT6c/WYCu5ItyKhx7huXP474bw+imRleo351kdI4RpPthqLiYbCNSuj+JAkhUHdxvAwfoioELhR4+rUIqW7wGbvdlQSZsUUks0mECN4BSno8y7gbPGiVGQC2izlllD9iuf51IdmOBv97bUpyRlT5fmySEKPXZCyiXaRVfGXfOeSnhC1rPnajLYlxcRKpMadnGvUqDE0YzAFrnXGLz9qGO8mR+aDQRrtChTlDdndWwMVEqXcO00i6kDg1NujdtlsvcarxmIUoFiOYNBZxYo/oahSM+bcub9cyLldpe433tz7iPQ6RdQdVlE2pLPmRqG2KliXMkfyN1XL0vQhLMdcbiNXul3day+eiilcJ1ESW3ocUgP3FyCchsVOk+WxHHV5zFtp5cLbjVtv+wxpeR2xwsSCqSkneewaVtlTXPTcJn28pq4xauY/ehOXOxn9fOqc1BeeKbhSOY+Wi54ivNtmSzRRyWOtGfZzgITbwksBwzPM+tf3M8IJwUhwptCpfmJ4jp+CJGoV196Xoy+EhJFP6PeSTCV9vY92D6LhN6tjDPeCaH7e7w0zCTP0sA1laeuo7rPtLeNA9JRX3RKTfe4pB9FczgO6VoAaS3jqCP0xf+twR0ztiGhOnMYUEjPaNSE4bh1p21Z9CZ+JguLhkUMfuLB1khPCd23BYfQ==">
</div>
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                            <tbody><tr>
                                                <th>问题一： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <select name="ddlQuestion1" id="ddlQuestion1" class="select2">
	<option value="请选择密保问题">请选择密保问题</option>
	<option value="您母亲的姓名是？">您母亲的姓名是？</option>
	<option value="您父亲的姓名是？">您父亲的姓名是？</option>
	<option value="您配偶的姓名是？">您配偶的姓名是？</option>
	<option value="您的出生地是？">您的出生地是？</option>
	<option value="您高中班主任的姓名是？">您高中班主任的姓名是？</option>
	<option value="您初中班主任的姓名是？">您初中班主任的姓名是？</option>
	<option value="您小学班主任的姓名是？">您小学班主任的姓名是？</option>
	<option value="您的小学校名是？">您的小学校名是？</option>
	<option value="您的学号（或工号）是？">您的学号（或工号）是？</option>
	<option value="您母亲的生日是？">您母亲的生日是？</option>
	<option value="您父亲的生日是？">您父亲的生日是？</option>
	<option value="您配偶的生日是？">您配偶的生日是？</option>
	<option value="您最熟悉的童年好友名字是？">您最熟悉的童年好友名字是？</option>
	<option value="对您影响最大的人名字是？">对您影响最大的人名字是？</option>

</select>
                                                    </span><span id="lblQuestion1"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>答案： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtAnswer1" type="text" maxlength="30" id="txtAnswer1" class="text"></span><span id="lblAnswer1"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>问题二： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <select name="ddlQuestion2" id="ddlQuestion2" class="select2">
	<option value="请选择密保问题">请选择密保问题</option>
	<option value="您母亲的姓名是？">您母亲的姓名是？</option>
	<option value="您父亲的姓名是？">您父亲的姓名是？</option>
	<option value="您配偶的姓名是？">您配偶的姓名是？</option>
	<option value="您的出生地是？">您的出生地是？</option>
	<option value="您高中班主任的姓名是？">您高中班主任的姓名是？</option>
	<option value="您初中班主任的姓名是？">您初中班主任的姓名是？</option>
	<option value="您小学班主任的姓名是？">您小学班主任的姓名是？</option>
	<option value="您的小学校名是？">您的小学校名是？</option>
	<option value="您的学号（或工号）是？">您的学号（或工号）是？</option>
	<option value="您母亲的生日是？">您母亲的生日是？</option>
	<option value="您父亲的生日是？">您父亲的生日是？</option>
	<option value="您配偶的生日是？">您配偶的生日是？</option>
	<option value="您最熟悉的童年好友名字是？">您最熟悉的童年好友名字是？</option>
	<option value="对您影响最大的人名字是？">对您影响最大的人名字是？</option>

</select>
                                                    </span><span id="lblQuestion2"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>答案： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtAnswer2" type="text" maxlength="30" id="txtAnswer2" class="text"></span> <span id="lblAnswer2"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>问题三： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <select name="ddlQuestion3" id="ddlQuestion3" class="select2">
	<option value="请选择密保问题">请选择密保问题</option>
	<option value="您母亲的姓名是？">您母亲的姓名是？</option>
	<option value="您父亲的姓名是？">您父亲的姓名是？</option>
	<option value="您配偶的姓名是？">您配偶的姓名是？</option>
	<option value="您的出生地是？">您的出生地是？</option>
	<option value="您高中班主任的姓名是？">您高中班主任的姓名是？</option>
	<option value="您初中班主任的姓名是？">您初中班主任的姓名是？</option>
	<option value="您小学班主任的姓名是？">您小学班主任的姓名是？</option>
	<option value="您的小学校名是？">您的小学校名是？</option>
	<option value="您的学号（或工号）是？">您的学号（或工号）是？</option>
	<option value="您母亲的生日是？">您母亲的生日是？</option>
	<option value="您父亲的生日是？">您父亲的生日是？</option>
	<option value="您配偶的生日是？">您配偶的生日是？</option>
	<option value="您最熟悉的童年好友名字是？">您最熟悉的童年好友名字是？</option>
	<option value="对您影响最大的人名字是？">对您影响最大的人名字是？</option>

</select>
                                                    </span><span id="lblQuestion3"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>答案： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtAnswer3" type="text" maxlength="30" id="txtAnswer3" class="text"></span> <span id="lblAnswer3"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>证件类型： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <select name="ddlPassportType" id="ddlPassportType" class="select2">
	<option selected="selected" value="0">请选择证件类型</option>
	<option value="1">身份证</option>
	<option value="2">学生证</option>
	<option value="3">军官证</option>
	<option value="4">驾驶证</option>
	<option value="5">其他</option>

</select>
                                                    </span><span id="lblPassportType"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>证件号码： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtPassportID" type="text" maxlength="30" id="txtPassportID" class="text"></span><span id="lblPassportID"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>确认号码： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtConPassportID" type="text" maxlength="30" id="txtConPassportID" class="text"></span><span id="lblConPassportID"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>电子邮箱： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtEmail" type="text" maxlength="30" id="txtEmail" class="text"></span><span id="lblEmail"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>确认邮箱： </th>
                                                <td>
                                                    <span class="text_bg">
                                                        <input name="txtConEmail" type="text" maxlength="30" id="txtConEmail" class="text"></span><span id="lblConEmail"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <input type="submit" name="btnConfirm" value="确定" id="btnConfirm" class="btn">
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        </form>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>