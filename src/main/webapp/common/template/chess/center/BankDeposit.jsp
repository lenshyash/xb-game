<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <link rel="stylesheet"href="/common/template/chess/center/css/user_content.css"type="text/css">
  <script type="text/javascript" src="/common/template/chess/center/js/jquery-1.11.3.min.js"></script> 
  <script src="/common/template/chess/center/js/base.js"></script>
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "301";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>存款</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                               <div class="grid fixed c21 main">
									<div class="ucmain" style="border:none;">
										<div class="infobox" style="position: relative; z-index: 9">
											<div class="userinfo">
												<strong>尊敬的用户：</strong><span class="user" style="color: #d20f00">${loginMember.account}</span><strong>，您好！您当前的账户余额为：</strong>
												<span class="gift-wallet"
													style="color: #d20f00; font-weight: bold">${loginMember.money}</span><strong>${not empty cashName?cashName:'元'}</strong>
											</div>
											<div class="menu" style="width:auto;">
												<span id="querenCZXX" style="display: none;">确认充值信息 | </span> <a
													href="${base }/center/member/chess/BankDrawMoney.do" style="color:#e4393c;">提款</a> | <a href="${base}/center/record/hisrecord/cp3Page.do" style="color:#e4393c;">账户明细</a>
											</div>
						
										</div>
										<c:if test="${isGuest }">
										<div class="userinfo-edit-main">试玩账号不能充值</div>
										</c:if>
										<c:if test="${!isGuest }">
										<div class="czmain czMain" id="chargerinit">
											 <div class="navbox" style="position: relative; z-index: 5;">
												<span class="info">选择充值方式：</span>
												<ul class="tab-list" style="height: 80px;float: none;padding-left:0;">
													<c:if test="${onlineFlag eq 'on'}">
														<li class="" data="online"
															desc="${not empty onlineDesc?onlineDesc:'及时自动到账，推荐'}"><a
															href="javascript:void(0);">在线支付</a></li>
													</c:if>
													<c:if test="${fastFlag eq 'on' }">
														<li class="" data="fast"
															desc="${not empty fastDesc?fastDesc:'支持微信/支付宝二维码扫描'}"><a
															href="javascript:void(0);">快速支付</a></li>
													</c:if>
													<c:if test="${bankFlag eq 'on' }">
														<li class="" data="bank"
															desc="${not empty bankDesc?bankDesc:'支持网银转账，ATM转账，银行柜台汇款' }"><a
															href="javascript:void(0);">公司入款</a></li>
													</c:if>
												</ul>
												<style>
													.czMain .navbox li{
														width:120px!important;
													}
												</style>
												<div class="clear"></div>
											</div>
											<div class="online-tit">
												<span></span><font id="depostDesc"></font>
											</div>
											<div class="wy-box" id="depostList">
											</div>
										</div>
										</c:if>
									</div>
								</div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <c:if test="${!isGuest }"><jsp:include page="include/deposit.jsp"></jsp:include>
		<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>
		<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
		<script type="text/javascript">
		var pays = [];
		var baseUrl = '${base}';
		var hostUrl1="${hostUrl1}";
		var showPayInfo='${showPayInfo}';
		$(function(){
			init();
		})
		
		function init(){
			if(!$(".tab-list li").hasClass("on")){
				var t = $(".tab-list li").eq(0);
				t.addClass("on");
			}else{
				var t = $(".tab-list li.on");
			}
			var data=t.attr('data'),url = null;
			switch(data){
			case "online":
				url = '${base}/center/banktrans/deposit/dptinita.do';
				Base.onlines.initOnlines(url);
				break;
			case "fast":
				url = '${base}/center/banktrans/deposit/dptinitb.do';
				Base.fasts.initFasts(url);
				break;
			case "bank":
				url = '${base}/center/banktrans/deposit/dptinitc.do';
				Base.banks.initBanks(url);
				break;
			default:
				layer.msg("系统异常",{icon:2});
				break;
			}
			$("#depostDesc").html(t.attr('desc')).parent().attr('class',''+data+'-tit');
		}
		
		var tabli = $(".tab-list li");
		tabli.on("click",function(){
			$(this).addClass("on").siblings().removeClass("on");
			init();
		});
		
		function goTo(){
			window.location.href="${base}/center/record/hisrecord/cp3Page.do";
		}
		</script></c:if>
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>