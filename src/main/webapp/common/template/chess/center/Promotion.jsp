<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "601";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
			<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>推广管理</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>推广地址</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                                            <tbody><tr>
                                                <td width="127" rowspan="2" align="left">
                                                    <img src="/images/Score.jpg" width="105" height="102">
                                                </td>
                                                <td width="357" height="43" align="left">
                                                    <span class="copy_title">注册推广码地址：</span><br>
                                                    <span class="copy">
                                                        https://qipai6.lyqp.com/Register/1536846.html</span>
                                                </td>
                                                <td width="142" align="left">
                                                    <img id="clip_button" src="../images/copy.jpg" width="122" height="32" alt="点击复制推广码" style="cursor: pointer;" onclick="window.clipboardData.setData('text','https://qipai6.lyqp.com/Register/1536846.html');alert('复制成功！');">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="59" colspan="2" align="left" class="copy_content">
                                                    复制推广链接并粘贴给的好友。如果您的好友通过您的推广链接注册并游戏，您所推荐用户每赢一局抽水的10%提成将作为您的奖励，奖励无上限，多推多得。
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        <br>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <div class="box_list" id="isSpreader" style="display:none;">
                                <div class="box_list_title">
                                    <h2>推广设置</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w3">
                                              <tbody><tr>
                                                <th>推广域名：</th>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>页脚信息： </th>
                                                <td width="15%"><span class="text1_bg">
                                                   <input id="txtcontent" type="text" class="text1" value=""></span>
                                                </td>
                                                <td>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <th> </th>
                                                <td>
                                                    <input type="button" id="btnSave" value="保存" class="btn1"> 
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                          
                                        </tbody></table>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>今日概况</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w3">
                                            <tbody><tr>
                                                <th>进行游戏的下线用户数： </th>
                                                <td width="15%">
                                                    <span id="ltTodayUnderling">0</span>人
                                                </td>
                                                <td>
                                                    <input type="button" name="btn1" value="点击查看" class="btn1" onclick="window.location.href='TodayUnderlingPlayedList.aspx'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>下线抽水数： </th>
                                                <td>
                                                    <span id="ltTodayRevenue">0.00</span>元
                                                </td>
                                                <td>
                                                    <input type="button" name="Submit32" value="点击查看" class="btn1" onclick="window.location.href='TodayUserRevenueList.aspx'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>新注册的直属下线用户数： </th>
                                                <td>
                                                    <span id="ltTodayregCounts">0</span>人
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>推广总计</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w3">
                                            <tbody><tr>
                                                <th>直属下线用户总人数： </th>
                                                <td width="15%">
                                                    <span id="ltUnderlingCounts">0</span>人
                                                </td>
                                                <td>
                                                    <input type="button" name="Submit33" value="点击查看" class="btn1" onclick="window.location.href='UnderlingInfo.aspx'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>所有下线用户总人数： </th>
                                                <td width="15%">
                                                    <span id="ltAllUnderlingCounts">0</span>人
                                                </td>
                                                <td>
                                                    <input type="button" name="Submit44" value="点击查看" class="btn1" onclick="window.location.href='UnderlingInfo.aspx'">
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <th>抽水总金额： </th>
                                                <td>
                                                    <span id="lbTotalRevenue"></span>元
                                                </td>
                                                <td>
                                                    <input type="button" name="Submit322" value="点击查看" class="btn1" onclick="window.location.href='RecordUserRevenueList.aspx'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>抽水比例： </th>
                                                <td colspan="2">
                                                    第一级比例：10%
                                                    总比例：21%
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>