<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="/common/template/chess/center/js/jquery-1.11.3.min.js"></script> 
  <script src="/common/template/chess/center/js/base.js"></script>
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "103";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	    .box_list_con_view table * {
	    	font-size:12px;
	    }
	    .box_list_con_view table a {
	    	cursor: pointer;
	    }
	    .addBankText{
			display: none;
		}
		.addBankText input[type="text"]{
			width:200px;
			padding:5px 5px;
		}
		.updatePassword{
			display: none;
		}
		.updatePassword input[type="password"]{
			width:200px;
			padding:5px 5px;
		}
		.setPayPass{
			display: none;
		}
		.setPayPass input[type="password"]{
			width:200px;
			padding:5px 5px;
		}
		.updatePayPass{
			display: none;
		}
		.updatePayPass input[type="password"]{
			width:200px;
			padding:5px 5px;
		}
		.submitBtn {
			display: inline-block;
			line-height: 30px;
			color: #fff;
			height: 30px;
			padding: 0 15px;
			border-radius: 3px;
			font-size: 14px;
			margin-right: 10px;
			min-width: 55px;
			text-align: center;
			cursor: pointer;
			background: #2e4158;
			margin: 10px 0
		}
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
      <div class="main_box_right"> 
       <div class="main_box_right_title common_nav"> 
        <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>账户信息</label></span> 
       </div> 
       <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>
                                        1、先完善资料 2、再绑定信息</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                            <tbody><tr>
                                                <th>帐号：</th>
                                                <td>
                                                    <span id="lblAccounts">${loginMember.account}</span>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                            <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w2">
                                                <tbody id="Tbody3">
                                                <c:if test="${empty member.userName}">
                                                    <tr>
                                                        <th>真实姓名：</th>
                                                        <td style="color:#999">真实姓名必须与出款银行一致！一经填写，不可以自行修改。 </td>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <td> <input id="userName" type="text" placeholder="请输入真实姓名"> </td>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <td><input class="submitBtn" style="border:0;" value="确认" type="submit" onclick="Base.xinXi.subUserName();"></td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${not empty member.userName}">
                                                	 <tr>
                                                        <th>真实姓名：</th>
                                                        <td>${member.userName} </td>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <td>如需更改真实姓名请联系<a href="${kfUrl }" target="_balnk" style="color:#e4393c;">在线客服</a>。</td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${empty member.cardNo}">
                                                	 <tr>
                                                        <th>銀行卡：</th>
                                                        <td>暂未绑定请先<a class="e4393c" href="javascript:(0)" onclick="showBankText()">[添加银行卡]</a> </td>
                                                    </tr>
                                                    <tr>
                                                        <th>完善银行卡信息：</th>
                                                        <td>
                                                        	<select id="bankName" style="width: 200px;position: static;opacity: 1;padding:5px 5px;">
																<option value="建设银行">建设银行</option>
																<option value="工商银行" selected="selected">工商银行</option>
																<option value="农业银行">农业银行</option>
																<option value="中国邮政银行">中国邮政银行</option>
																<option value="中国银行">中国银行</option>
																<option value="中国招商银行">中国招商银行</option>
																<option value="中国交通银行">中国交通银行</option>
																<option value="中国民生银行">中国民生银行</option>
																<option value="中信银行">中信银行</option>
																<option value="中国兴业银行">中国兴业银行</option>
																<option value="浦发银行">浦发银行</option>
																<option value="平安银行">平安银行</option>
																<option value="华夏银行">华夏银行</option>
																<option value="广州银行">广州银行</option>
																<option value="BEA东亚银行">BEA东亚银行</option>
																<option value="广州农商银行">广州农商银行</option>
																<option value="顺德农商银行">顺德农商银行</option>
																<option value="北京银行">北京银行</option>
																<option value="杭州银行">杭州银行</option>
																<option value="温州银行">温州银行</option>
																<option value="上海农商银行">上海农商银行</option>
																<option value="中国光大银行">中国光大银行</option>
																<option value="渤海银行">渤海银行</option>
																<option value="浙商银行">浙商银行</option>
																<option value="晋商银行">晋商银行</option>
																<option value="汉口银行">汉口银行</option>
																<option value="上海银行">上海银行</option>
																<option value="广发银行">广发银行</option>
																<option value="深圳发展银行">深圳发展银行</option>
																<option value="东莞银行">东莞银行</option>
																<option value="宁波银行">宁波银行</option>
																<option value="南京银行">南京银行</option>
																<option value="北京农商银行">北京农商银行</option>
																<option value="重庆银行">重庆银行</option>
																<option value="广西农村信用社">广西农村信用社</option>
																<option value="吉林银行">吉林银行</option>
																<option value="江苏银行">江苏银行</option>
																<option value="成都银行">成都银行</option>
																<option value="尧都区农村信用联社">尧都区农村信用联社</option>
																<option value="浙江稠州商业银行">浙江稠州商业银行</option>
																<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
														</select>
														</td>
                                                    </tr>
                                                    <tr class="addBankText"><th></th>
							                         	<td><input class="text" d="userName" type="text" placeholder="请输入开卡人姓名"></td>
							                         </tr>
							                         <tr class="addBankText"><th></th>
							                         	<td><input class="text" d="province" type="text" placeholder="请输入开卡省份"></td>
							                         </tr>
							                         <tr class="addBankText"><th></th>
							                         	<td><input class="text" id="city" type="text" placeholder="请输入开卡城市"></td>
							                         </tr>
							                         <tr class="addBankText"><th></th>
							                         	<td><input class="text" id="bankcardaddress" type="text" placeholder="请输入开卡网点"></td>
							                         </tr>
							                         <tr class="addBankText"><th></th>
							                         	<td><input class="text" id="bankId" type="text" placeholder="请输入银行卡号"></td>
							                         </tr>
							                         <tr class="addBankText"><th></th>
							                        	<td><input class="submitBtn" style="border:0;" value="确认" type="submit" onclick="Base.xinXi.setBanks();"></td>
							                        </tr>
                                                </c:if>
                                                <c:if test="${not empty member.cardNo}">
						                         	<tr><th>银行卡：</th>
						                         		 <td>已绑定 |  ${member.bankName}</td>
						                         	</tr>
						                         	<tr><th></th>
						                         		 <td>如需换绑银行卡请联系<a href="${kfUrl }" target="_balnk" style="color:#e4393c;">在线客服</a>。</td>
						                         	</tr>
						                         </c:if>
						                         <tr><th>登录密码：</th>
						                            <td><a class="e4393c" onclick="updatePassword()">[修改]</a></td>
						                         </tr>
						                         <tr class="updatePassword"><th>登录密码修改：</th>
						                            <td style="color:#999;">请输入6-16个英文或数字，且符合0~9或a~z字元</td>
						                         </tr>
						                         <tr class="updatePassword"><th></th>
						                            <td><input class="text" id="updOLoginPwd" placeholder="请输入原登录密码" type="password"></td>
						                         </tr>
						                         <tr class="updatePassword"><th></th>
						                            <td><input class="text" id="updLoginPwd" placeholder="请输入新登录密码" type="password"></td>
						                         </tr>
						                         <tr class="updatePassword"><th></th>
						                            <td><input class="text" id="updRLoginPwd" placeholder="请再次输入密码" type="password"></td>
						                         </tr>
						                         <tr class="updatePassword"><th></th>
						                            <td><input class="submitBtn" style="border:0;" value="确认" onctrck="Base.xinXi.updPwd(1);" type="button"></td>
						                         </tr>
						                         <c:if test="${ empty member.receiptPwd }">
						                        	<tr><th>提款密码：</th>
						                        		<td>未设置 <a class="e4393c" href="javascript:(0)" onctrck="setPayPass()">[立即设置]</a></td>
						                        	</tr>
						                        	<tr><th></th>
						                        		<td>必须设置提款密码，才能进行提款！<span style="color: red;">不是您的银行卡密码，是网站的提款安全密码!</span></td>
						                        	</tr>
						                        	<tr class="setPayPass"><th>提款密码设置：</th>
						                        		<td><input class="text"  id="payPwd" placeholder="请输入提款密码，最少6位数字"  type="password"></td>
						                        	</tr>
						                        	<tr class="setPayPass"><th></th>
						                        		<td><input class="text" id="rPayPwd" placeholder="请再次输入" type="password"></td>
						                        	</tr>
						                        	<tr class="setPayPass"><th></th>
						                        		<td><input class="submitBtn" style="border:0;" value="确认" onctrck="Base.xinXi.setPayPwd();" type="button"></td>
						                        	</tr>
						                        </c:if>
						                        <c:if test="${not empty member.receiptPwd}">
						                        	<tr><th>提款密码：</th>
						                        		<td>已设置 <a class="e4393c" href="javascript:(0)" onctrck="updatePayPass()">[立即修改]</a></td>
						                        	</tr>
						                        	<tr><th></th>
						                        		<td style="color:#999;">提款密码需由6-16个字母和数字(A-Z和 0-9)组成</td>
						                        	</tr>
						                        	<tr class="updatePayPass"><th>提款密码修改：</th>
						                        		<td><input class="text"  id="updOPayPwd" placeholder="请输入原提款密码"   type="password"></td>
						                        	</tr>
						                        	<tr class="updatePayPass"><th></th>
						                        		<td><input class="text" id="updPayPwd" placeholder="请输入新提款密码" type="password"></td>
						                        	</tr>
						                        	<tr class="updatePayPass"><th></th>
						                        		<td><input class="text" id="updRPayPwd" placeholder="请再次输入密码" type="password"></td>
						                        	</tr>
						                        	<tr class="updatePayPass"><th></th>
						                        		<td><input class="submitBtn" style="border:0;" value="确认" onctrck="Base.xinXi.updPwd(2);" type="button"></td>
						                        	</tr>
						                        </c:if>
						                        <tr><th></th>
					                        		<td style="color:#999;"><span  style="color:red;">*</span>&nbsp;<strong>身份信息是您领取大奖的重要凭据！一经填写，不可自行修改。</strong></td>
					                        	</tr>
					                        	<tr><th></th>
					                        		<td style="color:#999;"><span  style="color:red;">*</span>&nbsp;<strong>账户余额仅可提款至同名银行卡中，确保资金安全。</strong></td>
					                        	</tr>
                                                </tbody>
                                            </table>                                     
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
      </div> 
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <script>
			function showBankText(){
				$(".addBankText").toggle(200)
			}
			function updatePassword(){
				$(".updatePassword").toggle(200)
			}
			function setPayPass(){
				$(".setPayPass").toggle(200)
			}
			function updatePayPass(){
				$(".updatePayPass").toggle(200)
			}
			
		</script>
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>