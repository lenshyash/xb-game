<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "602";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
			<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>抽水转换</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>抽水转换</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <form method="post" action="./FinanceRevenue.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v3ScXs6AybKXghoYn78vqqO9SulaUGbmAihvo6myiKvzT+tidUIDfK8Gc+oscpP1Q1c57HkaZyoXz/9FSSu86uEkZx6b3Nt/wJAnD3dZy1Vj/5LhczbZk6MR4wJ6v40Wr3kK2aKQmgVRC/ko6GYhT04TB1ztVhDq8YsJKEiiD0J+y3kZg53QAhDAdo5DvO5jmoFm1N3by3KD1oZo0uYZvJ/B5viB9/jiam+mjvlhu0wyo8X0RkbzQY0br5s9ul581CiMME36PtiuuFF7tuCyLtxpPH7i+KDE92YJ3WVltKiNjsuS">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B22A6A79">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="H6AZQVXmzkIVVWIghYzxpekLNTUOVOP6j7/RRFhSMXvl+taRFyfsJAJNde3JKUVUOvfwA0ps0J0TtCvOsYOaMcAvJdCp+2lfzDC3oAHtA/eVJhSY0Ch4w3OwPaLhdL4zgyJlp6KX51nKd+4XDyXb0Tnz/mNzD/Z29rxfHw==">
</div>
                                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w3">
                                            <tbody><tr>
                                                <th>抽水总金额： </th>
                                                <td>
                                                    <span id="lbTotalRevenue">0.00</span>元
                                                </td>
                                                <td>
                                                    <input type="button" name="Submit322" value="点击查看" class="btn1" onclick="window.location.href='RecordUserRevenueList.aspx'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>抽水比例： </th>
                                                <td colspan="2">
                                                    第一级比例：10%
                                                    总比例：21%
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>未转换的抽水总数： </th>
                                                <td>
                                                    <span id="lbRevenue">0.00</span>元
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <th>已转换的抽水总数： </th>
                                                <td>
                                                    元
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>转换数量： </th>
                                                <td colspan="2">
                                                    <span class="text2_bg">
                                                        <input name="txtMoney" type="text" maxlength="10" id="txtMoney" class="text2" onkeyup="if(isNaN(value))execCommand('undo');"></span>
                                                    <input type="submit" name="btnSubmit" value="转换" id="btnSubmit" class="btn1">
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        <div class="prompt_content">
                                            <h2>注意：</h2>
                                            <b class="hong">此操作是将抽水转换到你的保险柜余额中，游戏中不能进行抽水转换！</b>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>