<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "604";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
			<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>我的下线推广</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>我的下线推广</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                    <form method="post" action="./UnderlingInfo.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="tvAccounts_ExpandState" id="tvAccounts_ExpandState" value="n">
<input type="hidden" name="tvAccounts_SelectedNode" id="tvAccounts_SelectedNode" value="">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tUTwtKqR5VReXcbdip5vZmoRHG/CtEI+INy27DJAL6DLsSzXHkpbY5uA86Me9x6DSxMeFtmaXcycYBLCZTRhpFyC8eDZuZ8JbL4a2HxP9eHMCg+pYftEzu+7H7fD7etV4+jqgw6q7f4Gdz4LLaW3EaKSA0kNPvrsmLMkCxdWhk3xfIelZS/7XGHPs79T5ROyynlMet0NX+X+xosEfbPKvw0ayETtcUcaF84OhHHjXTt+OIXiw1oUwQPRedfhnTaJgVy7TW8/SCziE3vg9PNOj1flYoitvAOohoG/d8WK5EAqu5/jZPUkIMGm6MkNKn3oQUSvW4K5Uv8729TqcICt2Is9R9u0UuBUw2dtgtHQ+HUFeih3WGNpwo5Rxu5SQcUhpVTUT3vCvh7uelkN7yaLp3mb60WFRGjwp9/ou6StvW37j93EZU7xmhOgEEtigmpRpnactUuU4fSPud0StOvl4FsAX3W1r/JgBxxR94TwNP1jnsm2ZvrACHswQU9aCZ9YxyH80I//8+JVfpnaTgLqn78fFFugMG4oaPUg7Q==">
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=ucIKgGrz8ptgA1cGw16M3PajwtirmvyH8o4BcupcVqosyKj1uKbS5B4UYN_75c6ZOglExarFFXrJy6440&amp;t=636681891604795562" type="text/javascript"></script>


<script src="/WebResource.axd?d=mkeAQiMPYECW52MNxhQvVEYPwACe98KHaIrRFclcrpvxXQnV--MGaoOOCEmaTamp4fDIGwuYXi4Ii0JH0&amp;t=636681891604795562" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var tvAccounts_Data = null;//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DE5787B9">
</div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td width="35%" valign="top">
                                                    <table width="96%" border="0" cellpadding="0" cellspacing="1" bgcolor="#ffeddd">
                                                        <tbody><tr>
                                                            <td valign="top">
                                                                
                                                                <a href="#tvAccounts_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=hcKn0oTIOgk5Bwq4xdtTFj9dF4Bew5uQhyY0S4BWfniaII-ZMJNJOTVXjPJpGozFkJX0GK2KQL5DF6Vx0&amp;t=636681891604795562" width="0" height="0" style="border-width:0px;"></a><div id="tvAccounts" class="treeview" style="height:100%;width:100%;">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tbody><tr>
			<td><img src="/WebResource.axd?d=cesG0XAOGaZ4-bccDAMyPZ60JcsZHaSryCqcZ1WsZP8RVANnYLnKedwBm39y0FUPPNCVXyVxo5rO0EuFbfXNLhmuk5VzEDtpUJdijw2&amp;t=636681891604795562" alt=""></td><td>
			<a href="UnderlingList.aspx?param=1536846" target="iframe1" onclick="javascript:TreeView_SelectNode(tvAccounts_Data, this,'tvAccountst0');" id="tvAccountst0i" tabindex="-1"><img src="${base}/images/03.gif" alt="" style="border-width:0;"></a></td><td class="tvAccounts_2 tvAccounts_8" onmouseover="TreeView_HoverNode(tvAccounts_Data, this)" onmouseout="TreeView_UnhoverNode(this)" style="white-space:nowrap;">
			<a class="tvAccounts_0 tvAccounts_1 tvAccounts_7" href="" style="color:#000;" target="iframe1" onclick="javascript:TreeView_SelectNode(tvAccounts_Data, this,'tvAccountst0');" id="tvAccountst0">gosick(0人)</a></td>
		</tr><tr style="height:1px;">
			<td></td>
		</tr>
	</tbody></table>
</div><a id="tvAccounts_SkipLink"></a>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                </td>
                                                <td width="65%" valign="top">
                                                <style>
        td { line-height: 25px; padding: 3px; background: #ffeddd; font-size: 12px; }
        .btn { width: 66px; height: 27px; margin: 10px auto; background: url(../images/userBg.png) 0 -85px no-repeat; border: 0; color: #fff; font-weight: bold; cursor: pointer; text-align: center; }
        .btn1 { width: 80px; height: 28px; background-image: url(${base}/images/btn2_bg.jpg); border: 0 none; cursor: pointer; color: #fff; font-size: 14px; font-weight: bold; line-height: 20px; text-align: center; margin-left: 5px; }
        .textinput { height: 25px;border: 1px solid #cc9966;font-size: 12px; }
        .bg { background: #e3f0f9; border-left: 1px solid #fff; border-top: 1px solid #fff; font-weight: bold; }
        a { color: #0000ff; text-decoration: underline; }
        a:hover { color: #ee3f3f; text-decoration: none; }
    </style>
                                                    <form method="post" action="./UnderlingList.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hcdMSLzPsSLXFa7gk9GbB/OMYH3VOmvYN0lSzcAachcYTXYfgf3EkPD3wzMsY0jxAqRrrRotmOZnVhq7upvpnwowe1ZUfH1cC9Uv3pW7oOfr5t/1QT9PJhZ1zps5OWjRWQOZfTsSANABVU91ng3sqr6T4IRJ5AFctoGMaCKNM6UHo8+EUt/idsD2YjGiSCgdC9174g==">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D5169E1E">
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="nBKK3MY7RihYeuTgP1dGjTjpXRZeqUlCYe3iGsClYlq3BRXyeQh4e8WPqQVI4dp16T89EPDFucqhtQTy7+4eukc8I6i2LUNWOUslu5xSsx4jbZaSspYpVPIVP8yNjiwuS2ytb/qEYRb9ZAP/Lg4Q+eN/dLbScqhrGDp0lg==">
</div>
    <div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody><tr>
                <td width="15%" align="right">
                    用户名：
                </td>
                <td width="10%">
                    <input name="txtKey" type="text" maxlength="20" id="txtKey" class="textinput" style="width:120px;">
                </td>
                <td>
                    <input type="submit" name="btnQuery" value="查询" id="btnQuery" class="btn1">
                    <input type="button" name="btnRefresh" value="刷新" class="btn1" onclick="window.location.href=location.href;">
                </td>
            </tr>
        </tbody></table>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" class="tb_list">
            <tbody><tr>
                <th>用户名 </th>
                <th>姓名 </th>
                <th>注册时间 </th>
                <th>状态 </th>
            </tr>
            
            <tr class="tdbg"><td colspan="4" align="center"><br>没有任何信息!<br><br></td></tr>
        </tbody></table>
        <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
            <tbody>
                <tr>
                    <td align="right">
                        <div id="anpPage">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr>
		<td valign="bottom" nowrap="true" style="width:40%;">总记录：0　页码：1/1　每页：20</td><td valign="bottom" nowrap="true" style="width:60%;"><a disabled="disabled" style="margin-right:5px;">首页</a><a disabled="disabled" style="margin-right:5px;">上页</a><span style="margin-right:5px;font-weight:Bold;color:red;">1</span><a disabled="disabled" style="margin-right:5px;">下页</a><a disabled="disabled" style="margin-right:5px;">末页</a>&nbsp;&nbsp;<input type="text" value="1" disabled="disabled" name="anpPage_input" id="anpPage_input" onkeydown="ANP_keydown(event,'anpPage_btn');" style="width:30px;"><input type="submit" value="go" name="anpPage" id="anpPage_btn" disabled="disabled" onclick="if(ANP_checkInput('anpPage_input',1)){__doPostBack('anpPage','')} else{return false}"></td>
	</tr>
</tbody></table>
</div>


                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>

</form></iframe>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                        
<script type="text/javascript">
//<![CDATA[
var tvAccounts_ImageArray =  new Array('../images/commimages/01.gif', '../images/commimages/02.gif', '${base}/images/commimages/03.gif', '/WebResource.axd?d=4VEOqGzUM1-G5ImYsapqIyLTwz32KdN5xBB6B4OfKUeBIe9CIbGNuD0HVAIDi9DDt0AiSJRFOH1AYgqQFgBV2270uPQPuso9ZVpqOA2&t=636681891604795562', '/WebResource.axd?d=KDJfxbKjMFtf7ISNfuIgBJtGPFkOOiGY8Sqfz5O576Olejj7egmElRJ9nx5jNTgyU-11gJ9vKPIphUfW9dabnSMf70dq8uV8Dy3O3Q2&t=636681891604795562', '/WebResource.axd?d=1gKWJ7N_AyuvhU2_JelNuGJzOB8nrV5YgCQuDX684_92hB-3Ckclj7hl-EVTMJYD8uovsFyPH38fWko-CQOwT_KgkAmIUMhIlLr1ig2&t=636681891604795562', '/WebResource.axd?d=Gshny1N2B9DShL4578DR-nNFV-59FAX6zE1Eq_XcXeyBXKR2aambkASF0XPLBzTXrc6Bc72jI3OfFbpGSuSGa56MvJkZIP08ytmNhg2&t=636681891604795562', '/WebResource.axd?d=s--azikWe9XEvbAoq7Vq2PIpYTfw9wrGJuZ3fzlUHCrewax0XrZrwtn-sr50Pie3XTdUdX4VYUKsTPKXI32AM6wV2fkKCyxD6IwdpA2&t=636681891604795562', '/WebResource.axd?d=xsmtR1eN2KniyMMVmCf_SBq2VuSrSIVCmkq4tMnJEIGKUSOjfGb_2SbRoXq5wqshfaUd21ZaWxxvuDQleRXinFTGnF5Fcz75cygOiw2&t=636681891604795562', '/WebResource.axd?d=OCCz-OLeKCyUzh_GAV4T14FRXk6d_zIBPbWhP9pBL3ldv7WGGHkSSmajQ19qQUk96x0_YqYtf6s8mc5gUhLwkLSquOQJVwsezhrE0HqHd9d4wjHU0&t=636681891604795562', '/WebResource.axd?d=awTYbyWAxX12Va9BU0UQHw_UTfDQDRZBRsHVE1HKCL1w790MIAKKOHt-wiEF2IOI0494GDU6Lt3paPIALLVQiAFpLUfr3dVBAGIKQw2&t=636681891604795562', '/WebResource.axd?d=jI-RG9CqmN-ALlYJzKTpknwscKtEQx2Cg3bKzc7VUrEOo049dEYPIquS8ABscyjVzKUYZdy4wZI-NGKs_-MsoBLYX5XsGGwWcp4fpQ2&t=636681891604795562', '/WebResource.axd?d=TQXsjmGduDQ9srpKfHp-Zm3U7U5SaWDNcnio_tFVUdGbUWMJcfRqwqKOj9ODDKQ5au2rbQDle10mMw4iT7qSzi6qCzH2hU3Is63nHmaYYDi034JG0&t=636681891604795562', '/WebResource.axd?d=zICQOIDmokBMrQZBXQF1PfUIt6V0s-JmTWRN_kUaH4DW48opqBw2pogMYTw59Uaus7mrAGxZ22EUXzIrCbB5TzhMMMJuhn9fr84wgQ2&t=636681891604795562', '/WebResource.axd?d=H1IZQwfFlDqjX7JnUniTlyOVGDSg6CYXM3ld1xMYm8im7P-opsUG467mKDX5lpIIDEESyRqimXIuPpYzH7CIB2pjPd-R96AAY_OA3g2&t=636681891604795562', '/WebResource.axd?d=WeuesYfvWJ7Af8mEA3Hy1uYGO-HccNjDKKmAMpxS34gNbEoOjxNRzBvKqc4SDXpBqOgPAP6yCtEfVFz6FziSLbGoeYV-5KsvQyRnVjPNsuXHH4Nw0&t=636681891604795562', '/WebResource.axd?d=cesG0XAOGaZ4-bccDAMyPZ60JcsZHaSryCqcZ1WsZP8RVANnYLnKedwBm39y0FUPPNCVXyVxo5rO0EuFbfXNLhmuk5VzEDtpUJdijw2&t=636681891604795562', '/WebResource.axd?d=u_deUDe5AwQ71I_6PjpTsRQUkjJIXmVxdhd0_x2XTHZmQua0tmQbL3uYB9hVW8ld4O0HgjEzu4u7SDiSEUDjh-Oe6TLeFBXn7BDYaaa0Xykrhl_U0&t=636681891604795562', '/WebResource.axd?d=jxn3cvGj-RZRRxKUaxH_qI_RTHDpmzn-EIyZoLNCREbg3Y0HGwEkyinIQNwh4Xu5O3613rdvx16jH05_Lar4-rJJm9srPtw6dVjm7FR2DuRlbHNH0&t=636681891604795562');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
var tvAccounts_Data = new Object();
tvAccounts_Data.images = tvAccounts_ImageArray;
tvAccounts_Data.collapseToolTip = "Collapse {0}";
tvAccounts_Data.expandToolTip = "Expand {0}";
tvAccounts_Data.expandState = theForm.elements['tvAccounts_ExpandState'];
tvAccounts_Data.selectedNodeID = theForm.elements['tvAccounts_SelectedNode'];
tvAccounts_Data.hoverClass = 'tvAccounts_12';
tvAccounts_Data.hoverHyperLinkClass = 'tvAccounts_11';
(function() {
  for (var i=0;i<19;i++) {
  var preLoad = new Image();
  if (tvAccounts_ImageArray[i].length > 0)
    preLoad.src = tvAccounts_ImageArray[i];
  }
})();
//]]>
</script>
</form>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>