<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <link rel="stylesheet"href="/common/template/chess/center/css/user_content.css"type="text/css">
  <script type="text/javascript" src="/common/template/chess/center/js/jquery-1.11.3.min.js"></script> 
  <script src="/common/template/chess/center/js/base.js"></script>
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "303";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	    .yhjy-MMain td,th{
	    	padding: 5px 8px;
	    	text-align: center;
	    }
	    .yhjy-MMain *{
	    	font-size:12px;
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>额度转换</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                
                                <table border="1" style="margin-bottom: 8px; width:100%;" class="yhjy-MMain" >
			<form id="form1" name="form1" action="?save=ok" method="post"></form>
                <thead>
                    <tr>
                        <th nowrap="" style="width: 30%;">帐户</th>
                        <th nowrap="" style="width: 70%;">余额</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>系统余额 </td>
                        <td><span id="mainCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
                    </tr>
                    <c:if test="${isAgOnOff eq 'on'}">
                    <tr>
                        <td>AG余额</td>
                        <td>
	                        <a style="color:#ff0000;" href="javascript:void(0);" id="" onclick="getBalance('ag','AGCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="AGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        <a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ag')">一键转入</a>
	                        <a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ag','sys');">一键转出</a>
	                        <a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardAg.do', '1');">立即游戏</a></td>
                    </tr>
                    </c:if>
                    <c:if test="${isDsOnOff eq 'on'}">
	                    <tr>
	                        <td>DS余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ds','DSCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="DSCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ds')">一键转入</a>
	                        		<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ds','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardDs.do', '8');">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isBbinOnOff eq 'on'}">
                    <tr>
                        <td width="35%">BBIN余额</td>
                        <td class="65%">
	                        <a style="color:#ff0000;" href="javascript:void(0);" id="loadbbinCredit" onclick="getBalance('bbin','BBINCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="BBINCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                     	<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','bbin')">一键转入</a>
	                        	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('bbin','sys');">一键转出</a>
	                        <a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardBbin.do?type=live', '2');">立即游戏</a></td>
                    </tr>
                    </c:if>
                   <c:if test="${isMgOnOff eq 'on'}">
                    <tr>
                        <td>MG余额</td>
                        <td>
                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('mg','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','mg')">一键转入</a>
	                        	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('mg','sys');">一键转出</a>
                        		<a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardMg.do?gameType=1', '3');">立即游戏</a></td>
                    </tr>
                    </c:if>
                     <c:if test="${isPtOnOff eq 'on'}">
	                    <tr>
	                        <td>PT余额</td>
	                        <td>
		                        <a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('pt','PTCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="PTCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
		                        	<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','pt')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('pt','sys');">一键转出</a>
		                        <a style="color:#ff0000;" href="${base }/index/egame.do?param=pt" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isQtOnOff eq 'on'}">
	                    <tr>
	                        <td>QT余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('qt','QTCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="QTCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','qt')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('qt','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="${base }/index/egame.do?param=qt" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isAbOnOff eq 'on'}">
	                    <tr>
	                        <td>Allbet余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ab','ABCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="ABCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ab')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ab','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardAb.do', '5');">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isOgOnOff eq 'on'}">
	                    <tr>
	                        <td>OG余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('og','OGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="OGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','og')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('og','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="go('${base}/forwardOg.do?gameType=desktop', '7');">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isCq9OnOff eq 'on'}">
	                    <tr>
	                        <td>Cq9余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('cq9','CQ9Credit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="CQ9Credit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','cq9')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('cq9','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="${base }/index/egame.do?param=cq9" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isJdbOnOff eq 'on'}">
	                    <tr>
	                        <td>Jdb余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('jdb','JDBCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="JDBCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','jdb')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('jdb','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="${base }/index/egame.do?param=jdb" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isTtgOnOff eq 'on'}">
	                    <tr>
	                        <td>Ttg余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ttg','TTGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="TTGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ttg')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ttg','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="${base }/index/egame.do?param=ttg" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isMwOnOff eq 'on'}">
	                    <tr>
	                        <td>Mw余额</td>
	                        <td>
	                        		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('mw','MWCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MWCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                        		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','mw')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('mw','sys');">一键转出</a>
	                        		<a style="color:#ff0000;" href="${base }/index/egame.do?param=mw" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isIsbOnOff eq 'on'}">
	                    <tr>
	                        <td>Isb余额</td>
	                        <td>
	                       		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('isb','ISBCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="ISBCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
	                       		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','isb')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('isb','sys');">一键转出</a>
	                       		<a style="color:#ff0000;" href="${base }/index/egame.do?param=isb" id="loadAgCredit" onclick="">立即游戏</a></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isM8OnOff eq 'on'}">
                        <tr>
                            <td class="">M8余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('m8','M8Credit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="M8Credit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                            		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','m8');">一键转入</a>
                            		<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('m8','sys')">一键转出</a>
                            		<a style="color:#ff0000;" href="${base }/index/egame.do?param=m8" id="loadAgCredit" onclick="">立即游戏</a></td>
                        </tr>
                    </c:if>
                    <c:if test="${isM8HOnOff eq 'on'}">
                        <tr>
                            <td class="">M8H余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('m8h','M8HCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="M8HCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                            		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','m8h');">一键转入</a>
                            		<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('m8h','sys');">一键转出</a>
                            		<a style="color:#ff0000;" href="${base }/index/egame.do?param=m8h" id="loadAgCredit" onclick="">立即游戏</a></td>
                        </tr>
                    </c:if>
                    <c:if test="${isIbcOnOff eq 'on'}">
                        <tr>
                            <td class="">沙巴余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ibc','IBCCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="IBCCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                            		<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ibc');">一键转入</a>
                            		<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ibc','sys');">一键转出</a>
                            		<a style="color:#ff0000;" href="${base }/index/egame.do?param=ibc" id="loadAgCredit" onclick="">立即游戏</a></td>
                        </tr>
                    </c:if>
                    <c:if test="${isBgOnOff eq 'on'}">
                        <tr>
                            <td class="">BG余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('bg','BGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="BGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                        		    <a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','bg')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('bg','sys');">一键转出</a>
                                <a style="color:#ff0000;" href="" id="loadAgCredit" onclick="go('${base}/forwardBg.do?type=2', '98');">立即游戏</a></td>
                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${isVrOnOff eq 'on'}">
                        <tr>
                            <td class="">VR余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('vr','VRCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="VRCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                           	 	<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','vr')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('vr','sys');">一键转出</a>
                            		<a style="color:#ff0000;" href="" id="loadAgCredit" onclick="go('${base}/forwardVr.do', '97');">立即游戏</a></td>
                            	</td>
                        </tr>
                    </c:if>
                    <c:if test="${isKyOnOff eq 'on'}">
                        <tr>
                            <td class="">KY余额</td>
                            <td class="">
                            		<a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ky','KYCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="KYCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span>
                           	 	<a style="color:#ff0000;margin-left:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('sys','ky')">一键转入</a>
	                       	 	<a style="color:#ff0000;margin-left:10px;margin-right:10px;" href="javascript:void(0);" id="loadAgCredit" onclick="fastTrans('ky','sys');">一键转出</a>
                            		<a style="color:#ff0000;" href="" id="loadAgCredit" onclick="go('${base}/forwardKy.do', '12');">立即游戏</a></td>
                            	</td>
                        </tr>
                    </c:if>
                </tbody>
        	</table>
        	<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能转换额度</div></c:if>
			<c:if test="${userInfo.accountType!=6 }">
            <h2 class="MSubTitle">额度转换</h2>
            <table class="yhjy-MMain">
                <tbody>
                	<tr>
                        <td>转出：</td>
                        <td  style="text-align: left;">
                           <select name="source-transfer" class="MFormStyle" id="source-transfer"  style="width:180px;height:30px;">
								<option selected="selected" value="">----请选择钱包----</option>
								<option value="sys">系统额度</option>
								<c:if test="${isAgOnOff eq 'on'}">
									<option value="ag">AG额度</option>
								</c:if>
								<c:if test="${isDsOnOff eq 'on'}">
									<option value="ds">DS额度</option>
								</c:if>
								<c:if test="${isBbinOnOff eq 'on'}">
									<option value="bbin">BBIN额度</option>
								</c:if>
								<c:if test="${isMgOnOff eq 'on'}">
									<option value="mg">MG额度</option>
								</c:if>
								
								<c:if test="${isPtOnOff eq 'on'}">
									<option value="pt">PT额度</option>
								</c:if>
								<c:if test="${isQtOnOff eq 'on'}">
									<option value="qt">QT额度</option>
								</c:if>
								<c:if test="${isAbOnOff eq 'on'}">
									<option value="ab">Allbet额度</option>
								</c:if>
								<c:if test="${isOgOnOff eq 'on'}">
									<option value="og">OG额度</option>
								</c:if>
								<c:if test="${isCq9OnOff eq 'on'}">
									<option value="cq9">CQ9额度</option>
								</c:if>
								<c:if test="${isJdbOnOff eq 'on'}">
									<option value="jdb">JDB额度</option>
								</c:if>
								<c:if test="${isTtgOnOff eq 'on'}">
									<option value="ttg">TTG额度</option>
								</c:if>
								<c:if test="${isMwOnOff eq 'on'}">
									<option value="mw">MW额度</option>
								</c:if>
								<c:if test="${isIsbOnOff eq 'on'}">
									<option value="isb">ISB额度</option>
								</c:if>
								<c:if test="${isM8OnOff eq 'on'}">
                                    <option value="m8">M8额度</option>
                                </c:if>
								<c:if test="${isM8HOnOff eq 'on'}">
                                    <option value="m8h">M8H额度</option>
                                </c:if>
								<c:if test="${isIbcOnOff eq 'on'}">
                                    <option value="ibc">沙巴额度</option>
                                </c:if>
								<c:if test="${isBgOnOff eq 'on'}">
                                    <option value="bg">BG额度</option>
                                </c:if>
								<c:if test="${isVrOnOff eq 'on'}">
                                    <option value="vr">VR额度</option>
                                </c:if>
								<c:if test="${isKyOnOff eq 'on'}">
                                    <option value="ky">KY额度</option>
                                </c:if>
							</select>
                        </td>
                    </tr>
                    <tr>
                        <td>转入：</td>
                        <td   style="text-align: left;">
                           <select name="desc-transfer" id="desc-transfer"  style="width:180px;height:30px;">
								<option selected="selected" value="">----请选择钱包----</option>
								<option value="sys">系统额度</option>
								<c:if test="${isAgOnOff eq 'on'}">
									<option value="ag">AG额度</option>
								</c:if>
								<c:if test="${isDsOnOff eq 'on'}">
									<option value="ds">DS额度</option>
								</c:if>
								<c:if test="${isBbinOnOff eq 'on'}">
									<option value="bbin">BBIN额度</option>
								</c:if>
								<c:if test="${isMgOnOff eq 'on'}">
									<option value="mg">MG额度</option>
								</c:if>
								<c:if test="${isPtOnOff eq 'on'}">
									<option value="pt">PT额度</option>
								</c:if>
								<c:if test="${isQtOnOff eq 'on'}">
									<option value="qt">QT额度</option>
								</c:if>
								<c:if test="${isAbOnOff eq 'on'}">
									<option value="ab">Allbet额度</option>
								</c:if>
								<c:if test="${isOgOnOff eq 'on'}">
									<option value="og">OG额度</option>
								</c:if>
								<c:if test="${isCq9OnOff eq 'on'}">
									<option value="cq9">CQ9额度</option>
								</c:if>
								<c:if test="${isJdbOnOff eq 'on'}">
									<option value="jdb">JDB额度</option>
								</c:if>
								<c:if test="${isTtgOnOff eq 'on'}">
									<option value="ttg">TTG额度</option>
								</c:if>
								<c:if test="${isMwOnOff eq 'on'}">
									<option value="mw">MW额度</option>
								</c:if>
								<c:if test="${isIsbOnOff eq 'on'}">
									<option value="isb">ISB额度</option>
								</c:if>
								<c:if test="${isM8OnOff eq 'on'}">
                                    <option value="m8">M8额度</option>
                                </c:if>
								<c:if test="${isM8HOnOff eq 'on'}">
                                    <option value="m8h">M8H额度</option>
                                </c:if>
								<c:if test="${isIbcOnOff eq 'on'}">
                                    <option value="ibc">沙巴额度</option>
                                </c:if>
								<c:if test="${isBgOnOff eq 'on'}">
                                    <option value="bg">BG额度</option>
                                </c:if>
								<c:if test="${isVrOnOff eq 'on'}">
                                    <option value="vr">VR额度</option>
                                </c:if>
								<c:if test="${isKyOnOff eq 'on'}">
                                    <option value="ky">KY额度</option>
                                </c:if>
						   </select>
                        </td>
                    </tr>
                    <tr>
                        <td>金额：</td>
                        <td style="text-align: left;"><input id="amount" name="amount" class="inp" type="text"  style="width:180px;height:30px;"> <input value="确定" onclick="trans();" id="add" class="btn2" type="button"  style="border:none;border-radius: 5px;padding:7px 12px;cursor: pointer;"></td>
                    </tr>
                    <tr>
                    	<td></td>
                    	<td  style="text-align: left;"><span id="MSwitchResult" style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></td>
                    </tr>
                </tbody>
                <input type="hidden" name="v" id="v" value="${v}">
        	</table></c:if>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
function getBalance(type,id){
	var param = {};
	param["type"]=type;
	$("#"+id).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />");
	
	$ajax({
        url:'${base}/rc4m/getBalance.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
         	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(json.balance==-1 || json.balance=='-1'){
					$("#"+id).text("第三方账户还未开通");
				}else{
					$("#"+id).text(json.balance);
				}
			} else {// 后台异常
				$("#"+id).text("获取失败")
			}
        	
        }
    });
}

$(function(){
		getBalance("sys","mainCredit");
	<c:if test="${isMgOnOff eq 'on'}">
		getBalance("mg","MGCredit");
	</c:if>
	<c:if test="${isBbinOnOff eq 'on'}">
		 getBalance("bbin","BBINCredit");
	</c:if>
	<c:if test="${isAgOnOff eq 'on'}">
		 getBalance("ag","AGCredit");
	</c:if>
	
	<c:if test="${isPtOnOff eq 'on'}">
		 getBalance("pt","PTCredit");
	</c:if>
	<c:if test="${isQtOnOff eq 'on'}">
	 	getBalance("qt","QTCredit");
	</c:if>
	<c:if test="${isAbOnOff eq 'on'}">
		getBalance("ab","ABCredit");
	</c:if>
	<c:if test="${isOgOnOff eq 'on'}">
		getBalance("og","OGCredit");
	</c:if>
	<c:if test="${isDsOnOff eq 'on'}">
		getBalance("ds","DSCredit");
	</c:if>
	<c:if test="${isCq9OnOff eq 'on'}">
		getBalance("cq9","CQ9Credit");
	</c:if>
	<c:if test="${isJdbOnOff eq 'on'}">
		getBalance("jdb","JDBCredit");
	</c:if>
	<c:if test="${isTtgOnOff eq 'on'}">
		getBalance("ttg","TTGCredit");
	</c:if>
	<c:if test="${isMwOnOff eq 'on'}">
		getBalance("mw","MWCredit");
	</c:if>
	<c:if test="${isIsbOnOff eq 'on'}">
		getBalance("isb","ISBCredit");
	</c:if>
	<c:if test="${isM8OnOff eq 'on'}">
    		getBalance("m8","M8Credit");
	</c:if>
	<c:if test="${isM8HOnOff eq 'on'}">
    		getBalance("m8h","M8HCredit");
	</c:if>
	<c:if test="${isIbcOnOff eq 'on'}">
    		getBalance("ibc","IBCCredit");
	</c:if>
	<c:if test="${isBgOnOff eq 'on'}">
    		getBalance("bg","BGCredit");
	</c:if>
	<c:if test="${isVrOnOff eq 'on'}">
    		getBalance("vr","VRCredit");
	</c:if>
	<c:if test="${isKyOnOff eq 'on'}">
    		getBalance("ky","KYCredit");
	</c:if>
});

$(function(){
	$("#source-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			$("#desc-transfer").val("ag");
		}else{
			$("#desc-transfer").val("sys");
		}
	});
	
 	$("#desc-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			$("#source-transfer").val("ag");
		}else{
			$("#source-transfer").val("sys");
		}
	}); 
});

function trans(){
	$("#add").attr("disabled","disabled");
	var from = $("#source-transfer").val();
	var to = $("#desc-transfer").val();
	var v = $("#v").val();
	
	if(from==null || from==''){
		alert("转出类型不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	if(to==null || to==''){
		alert("转出类型不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	var quota = $("#amount").val();
	if(!quota){
		alert("余额不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	if(from=='sys'&& !/ag|bbin|mg|pt|qt|ab|og|ds|cq9|jdb|ttg|mw|isb|m8|ibc|m8h|bg|vr|ky/.test(to)){
		alert("转入类型错误");
		$("#add").removeAttr("disabled");
		return;
	}
	if(to=='sys'&& !/ag|bbin|mg|pt|qt|ab|og|ds|cq9|jdb|ttg|mw|isb|ibc|m8|m8h|bg|vr|ky/.test(from)){
		alert("转出类型错误");
		$("#add").removeAttr("disabled");
		return;
	}
	var param = {};
	param["changeFrom"]=from;
	param["changeTo"]=to;
	param["quota"]=quota;
	param["v"]=v;
	$ajax({
        url:'${base}/rc4m/thirdRealTransMoney.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
        	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
	        	if(json.success){
	        		getBalance("sys","mainCredit");
				if(to=='mg'||from=='mg'){
					getBalance("mg","MGCredit");
				}
				if(to=='ag'||from=='ag'){
					 getBalance("ag","AGCredit");
				}
				if(to=='bbin'||from=='bbin'){
					 getBalance("bbin","BBINCredit");
				}
				if(to=='qt'||from=='qt'){
					 getBalance("qt","QTCredit");
				}
				if(to=='pt'||from=='pt'){
					 getBalance("pt","PTCredit");
				}
				if(to=='ab'||from=='ab'){
					 getBalance("ab","ABCredit");
				}
				if(to=='og'||from=='og'){
					 getBalance("og","OGCredit");
				}
				if(to=='ds'||from=='ds'){
					 getBalance("ds","DSCredit");
				}
				if(to=='cq9'||from=='cq9'){
					 getBalance("cq9","CQ9Credit");
				}
				if(to=='jdb'||from=='jdb'){
					 getBalance("jdb","JDBCredit");
				}
				if(to=='ttg'||from=='ttg'){
					 getBalance("ttg","TTGCredit");
				}
				if(to=='mw'||from=='mw'){
					 getBalance("mw","MWCredit");
				}
				if(to=='isb'||from=='isb'){
					 getBalance("isb","ISBCredit");
				}
				if(to=='m8'||from=='m8'){
					 getBalance("m8","M8Credit");
				}
				if(to=='m8h'||from=='m8h'){
					 getBalance("m8h","M8HCredit");
				}
				if(to=='ibc'||from=='ibc'){
					 getBalance("ibc","IBCCredit");
				}
				if(to=='bg'||from=='bg'){
					 getBalance("bg","BGCredit");
				}
				if(to=='vr'||from=='vr'){
					 getBalance("vr","VRCredit");
				}
				if(to=='ky'||from=='ky'){
					 getBalance("ky","KYCredit");
				}
				alert("转账成功");
	        	}else{
	        		alert(json.msg);
	        	}
			}else {// 登录异常
				alert(json.msg);
			}
        	
        	$("#add").removeAttr("disabled");
        }
    });
}
/*快捷转账*/
function fastTrans(from,to){
	var v = $("#v").val();
	var param = {};
	param["changeFrom"]=from;
	param["changeTo"]=to;
	param["v"]=v;
	$ajax({
        url:'${base}/rc4m/fastThirdRealTransMoney.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
        	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
	        	if(json.success){
	        		getBalance("sys","mainCredit");
				if(to=='mg'||from=='mg'){
					getBalance("mg","MGCredit");
				}
				if(to=='ag'||from=='ag'){
					 getBalance("ag","AGCredit");
				}
				if(to=='bbin'||from=='bbin'){
					 getBalance("bbin","BBINCredit");
				}
				if(to=='qt'||from=='qt'){
					 getBalance("qt","QTCredit");
				}
				if(to=='pt'||from=='pt'){
					 getBalance("pt","PTCredit");
				}
				if(to=='ab'||from=='ab'){
					 getBalance("ab","ABCredit");
				}
				if(to=='og'||from=='og'){
					 getBalance("og","OGCredit");
				}
				if(to=='ds'||from=='ds'){
					 getBalance("ds","DSCredit");
				}
				if(to=='cq9'||from=='cq9'){
					 getBalance("cq9","CQ9Credit");
				}
				if(to=='jdb'||from=='jdb'){
					 getBalance("jdb","JDBCredit");
				}
				if(to=='ttg'||from=='ttg'){
					 getBalance("ttg","TTGCredit");
				}
				if(to=='mw'||from=='mw'){
					 getBalance("mw","MWCredit");
				}
				if(to=='isb'||from=='isb'){
					 getBalance("isb","ISBCredit");
				}
				if(to=='m8'||from=='m8'){
					 getBalance("m8","M8Credit");
				}
				if(to=='m8h'||from=='m8h'){
					 getBalance("m8h","M8HCredit");
				}
				if(to=='ibc'||from=='ibc'){
					 getBalance("ibc","IBCCredit");
				}
				if(to=='bg'||from=='bg'){
					 getBalance("bg","BGCredit");
				}
				if(to=='vr'||from=='vr'){
					 getBalance("vr","VRCredit");
				}
				if(to=='ky'||from=='ky'){
					 getBalance("ky","KYCredit");
				}
				alert("转账成功");
	        	}else{
	        		alert(json.msg);
	        	}
			}else {// 登录异常
				alert(json.msg);
			}
        	
        	$("#add").removeAttr("disabled");
        }
    });
}
</script></c:if>
<jsp:include page="/common/template/third/page/live_demo.jsp" />
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>