<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "102";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
      <div class="main_box_right"> 
       <div class="main_box_right_title common_nav"> 
        <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>基本资料</label></span> 
       </div> 
       <div class="main_box_right_con"> 
        <div class="box_list"> 
         <div class="box_list_title"> 
          <h2>推广</h2> 
         </div> 
         <div class="box_list_con"> 
          <div class="box_list_con_view"> 
           <table width="100%" border="0" cellspacing="1" cellpadding="0"> 
            <tbody>
             <tr> 
              <td width="127" rowspan="2" align="left"> <img src="/images/Score.jpg" width="105" height="102" /> </td> 
              <td width="357" height="43" align="left"> <span class="copy_title">注册推广码：</span><br /> </td> 
              <td width="142" align="left"> <img id="clip_button" src="/images/copy.jpg" width="122" height="32" alt="点击复制推广码" style="cursor: pointer;" /> </td> 
             </tr> 
             <tr> 
              <td height="59" colspan="2" align="left" class="copy_content"> 生成推广链接并粘贴给的好友。如果您的好友通过您的推广链接注册并游戏，您所推荐用户每赢一局您将获得奖励，奖励无上限，多推多得。 </td> 
             </tr> 
            </tbody>
           </table> 
           <br /> 
          </div> 
         </div> 
         <div class="box_list_bottom"> 
         </div> 
        </div> 
        <!-- <div class="box_list">
                                <div class="box_list_title">
                                    <h2>游戏下载地址</h2>
                                </div>
                                <div class="box_list_con">
                                    <div class="box_list_con_view">
                                        <ul class="download_link_list">
                                            
                                                    <li><a href="https://0178.oss-cn-shenzhen.aliyuncs.com/LYGameFull.exe" title="高速下载" target="_blank">
                                                        高速下载</a></li>
                                                
                                                    <li><a href="https://pan.baidu.com/s/1vhcLQJ3llMh8JY3qgt05sA" title="百度网盘下载" target="_blank">
                                                        百度网盘下载</a></li>
                                                
                                                    <li><a href="https://www.lyqp.com/GameIntroduces.aspx" title="官网下载" target="_blank">
                                                        官网下载</a></li>
                                                
                                        </ul>
                                    </div>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div> --> 
        <!--box_list--> 
        <div class="box_list"> 
         <div class="box_list_title"> 
          <h2>基本资料</h2> 
         </div> 
         <div class="box_list_con"> 
          <div class="box_list_con_view"> 
           <table id="memberinfo" width="100%" border="0" cellspacing="1" cellpadding="0" class="tb_info th_w3"> 
            <tbody> 
             <tr> 
              <th>用户名： </th> 
              <td> ${loginMember.account} </td> 
             </tr> 
             <tr> 
              <th>性别： </th> 
              <td> 男 </td> 
             </tr> 
             <tr> 
              <th>账户余额： </th> 
              <td> ${loginMember.money}${not empty cashName?cashName:'元'} </td> 
             </tr> 
             <tr> 
              <th>会员等级： </th> 
              <td> ${levelName} </td> 
             </tr> 
             <tr> 
              <th>今日棋牌消费： </th> 
              <td> 0 </td> 
             </tr> 
             <tr> 
              <th>今日棋牌中奖： </th> 
              <td> 0 </td> 
             </tr> 
             <tr> 
              <th>今日棋牌消费： </th> 
              <td> 11 </td> 
             </tr> 
             <tr> 
              <th>最近一次登录： </th> 
              <td> ${userInfo1.lastLoginDatetime} </td> 
             </tr> 
            </tbody>
           </table> 
           <br /> 
           <br /> 
          </div> 
         </div> 
         <div class="box_list_bottom"> 
         </div> 
        </div> 
        <!--box_list end--> 
        <div class="main_box_right_bottom"> 
        </div> 
       </div> 
      </div> 
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>