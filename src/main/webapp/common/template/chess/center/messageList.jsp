<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>${_title }</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="/common/template/chess/center/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
  <script type="text/javascript" src="/common/js/layer/layer.js"></script>
 </head> 
 <body> 
  <script>
		var leftPosition = "101";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	    #anpPage *{
	    	font-size:12px;
	    }
	    #anpPage a{
	    	cursor: pointer;
	    }
	    #resContent{
	    	font-size:12px;
	    }
	    #resContent tr:hover{
	    	background: #eee;
	    	cursor: pointer;
	    }
	    #resContent a{
	    	font-size:12px;
	    	cursor: pointer;
	    }
	    #resContent td a:hover{
	    	color:red;
	    }
	    .page *{
	    	cursor: pointer;
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
      <div class="main_box_right"> 
       <div class="main_box_right_title common_nav"> 
        <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>基本资料</label></span> 
       </div> 
       <div class="main_box_right_con"> 
        <form id="form1">  
         <!--box_list--> 
         <div class="box_list"> 
          <table width="100%" cellpadding="0" cellspacing="1" class="tb_list"> 
           <tbody>
           	<thead>
	            <tr> 
	             <th>状态 </th> 
	             <th>消息标题 </th> 
	             <th>日期 </th> 
	             <th>操作</th> 
	            </tr> 
             </thead>
             <tbody id="resContent">
             	
             </tbody>
           </tbody>
          </table> 
          <div class="page" style="width:100%;">
               <div class="pageNav">
                    <div  class="pageNav">
                       <ul class="pagination">
                       </ul>
                   </div>
                </div>
           </div>
         </div> 
         <!--box_list end--> 
        </form> 
        <div class="main_box_right_bottom"> 
        </div> 
       </div> 
      </div> 
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
  <script>
  	$(function(){
  		initRdsData(1)
  	})
  	function initRdsData(p) {
  		var html = '';
			$.ajax({
				url : base+'/center/news/message/list.do',
				dataType : 'json',
				type : 'post',
				data : {
					pageNumber : p,
					pageSize : 10
				},
				success : function(res) {
					if(res.list.length>=1){
						addTemplate(res)
						page = res.currentPageNo;
						lotteryPage(res.totalCount,res.totalPageCount);
						$("#pageNum").html(res.currentPageNo);
						lastPage = res.totalPageCount
					}else{
						$("#resContent").html('<tr class="tdbg"><td colspan="8" align="center"><br />没有任何信息!<br /><br /></td></tr>')
					}
				}
			})
		}
  	function addTemplate(res){
  		var html='';
  		$.each(res.list,function(index,item){
			html += '<tr class="tdbg"><td onclick="readMsg(&quot;'+item.id+'&quot;,&quot;'+item.message+'&quot;)">'+chessType[item.status]+'</td><td onclick="readMsg(&quot;'+item.id+'&quot;,&quot;'+item.message+'&quot;)">'+item.title+'</td><td onclick="readMsg(&quot;'+item.id+'&quot;,&quot;'+item.message+'&quot;)">'+getMyDate(item.createDatetime)+'</td><td><a onclick="delMsg('+item.id+')">删除</a></td><tr>';
		})
		$("#resContent").html(html)
  	}
  	function delMsg(id) {
		if (confirm("确认删除此信吗？")) {
			$.ajax({
				url : "${base}/center/news/message/batchNewDelete.do",
				data : {
					id : id
				},
				success : function(result) {
					initRdsData(page);
				}
			});
		}
	}
  	function readMsg(id,content) {
		$.ajax({
			url : "${base}/center/news/message/read.do",
			data : {
				id : id
			},
			success : function(result) {
				initRdsData(page);
				showMessage(content);
			}
		});
	}
  </script>
 </body>
</html>