<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<input type="hidden" value="${base }" id="base">
<div class="lee" style="height:100%;">
    <div class="member_act_title">
    </div>
    <div class="mem_t"><label class="ico_head">帐号服务</label><span class="ico_minus"></span></div>
    <ul>
        <li id="101" class="le1"><a href="${base }/center/member/chess/messagelist.do">系统消息</a></li>
        <li id="102" class="le1 "><a href="${base }/center/member/chess/userCenter.do">基本资料</a></li>
        <li id="103" class="le1"><a href="${base }/center/member/chess/UpdateInfo.do">账户信息</a></li>
    </ul>
   <%--  <div class="mem_t"><label class="ico_key">帐号安全</label><span class="ico_minus"></span></div>
     <ul>
        <li id="201" class="le1"><a href="${base }/center/member/chess/ApplyPassProtect.do">申请密码保护</a></li>
        <li id="202" class="le1"><a href="/User/UpdatePassProtect.aspx">修改密码保护</a></li>
        <li id="203" class="le1"><a href="/User/ResetLoginPass.aspx">找回登录密码</a></li>
        <li id="204" class="le1"><a href="/User/ResetInsurePass.aspx">找回保险柜密码</a></li>
    </ul> --%>
    <div class="mem_t"><label class="ico_head">保险柜服务</label><span class="ico_minus"></span></div>
    <ul>
        <li id="301" class="le1"><a href="${base }/center/member/chess/BankDeposit.do">充值</a></li>
        <li id="302" class="le1"><a href="${base }/center/member/chess/BankDrawMoney.do">提款</a></li>
        <li id="303" class="le1"><a href="${base }/center/member/chess/limitChange.do">额度转换</a></li>
        <li id="304" class="le1"><a href="${base }/center/member/chess/accountBreakdown.do">帐变记录</a></li>
        <li id="305" class="le1"><a href="${base }/center/member/chess/recharge.do">充值记录</a></li>
        <li id="306" class="le1"><a href="${base }/center/member/chess/accountDraw.do">取款记录</a></li>
    </ul>
    <div class="mem_t"><label class="ico_game">游戏服务</label><span class="ico_minus"></span></div>
    <ul>
        <li id="401" class="le1"><a href="${base }/center/member/chess/RecordDrawList.do">游戏记录</a></li>
    </ul>
    <%-- <div class="mem_t"><label class="ico_head">充值中心</label><span class="ico_minus"></span></div>
    <ul>
        <li id="501" class="le1"><a href="/User/TransAuto.aspx">网银自助转账</a></li>
        <li id="502" class="le1"><a href="/User/TransAutoList.aspx">网银自助回查</a></li>
        <li id="503" class="le1"><a href="/User/Transfer.aspx">存款信息提交</a></li>
        <li id="504" class="le1"><a href="/User/TransferList.aspx">存款信息回查</a></li>
        <li id="505" class="le1"><a href="/User/RecordPayList.aspx">充值记录</a></li>
        <li id="506" class="le1"><a href="/User/RecordRechargeCardList.aspx">充值卡记录</a></li>
        <li id="507" class="le1"><a href="/User/RecordGameLotteryList.aspx">抽奖记录</a></li>
         
        <li id="509" class="le1"><a href="/User/RecordApplyExchangeList.aspx">兑换记录</a></li> 
    </ul>
    <div class="mem_t"><label class="ico_head">推广中心</label><span class="ico_minus"></span></div>
    <ul>
        <li id="601" class="le1"><a href="${base }/center/member/chess/Promotion.do">推广管理</a></li>
        <li id="602" class="le1"><a href="${base }/center/member/chess/FinanceRevenue.do">抽水转换</a></li>
        <li id="603" class="le1"><a href="${base }/center/member/chess/UnderlingAccountList.do">我的下线</a></li>
        <li id="604" class="le1"><a href="${base }/center/member/chess/UnderlingInfo.do">我的推广</a></li>
        <li id="605" class="le1"><a href="${base }/center/member/chess/RecordUserRevenueList.do">收益报表</a></li>
        <li id="606" class="le1" style="display:none;"><a href="/User/RecSpreadList.aspx">有效推广</a></li>
    </ul> --%>
    <div>
        &nbsp;</div>
    <div class="member_bottom">
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        //Left Menu
        
        if (leftPosition >= 0) {
            var menu = $("div.lee li").each(function(i) {
                if ($(this).attr("id") == leftPosition) {
                    $(this).addClass("mem_action");
                }
            });
        }
//
        $("div.mem_t").click(function() {
            var dspan = $(this).find("span");
            var dul = $(this).next("ul");
            if (dspan.attr("class") == "ico_plus") {
                dspan.removeClass("ico_plus").addClass("ico_minus");
                dul.slideDown();
            }
            else {
                dspan.removeClass("ico_minus").addClass("ico_plus");
                dul.slideUp();
            }
        });        
    });
</script>
