<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 公用js，css -->
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/js/chosen/chosen.min.css" type="text/css">
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script src="${base}/common/js/contants.js?v=2"></script> 
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<link href="${base}/common/template/lottery/lecai/js/loading/loading.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/loading/loading.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>