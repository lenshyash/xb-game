<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <link rel="stylesheet"href="/common/template/chess/center/css/user_content.css"type="text/css">
  <script type="text/javascript" src="/common/template/chess/center/js/jquery-1.11.3.min.js"></script> 
  <script src="/common/template/chess/center/js/base.js"></script>
  <script type="text/javascript" src="/common/template/chess/center/js/index.js"></script>
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "302";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="${base }/index.do">首页</a> &gt; <a href="javascript:void(0)">用户中心</a> &gt; <label>取款</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <div class="box_list_title">
                                    <h2>取款</h2>
                                </div>
                                <div class="box_list_con">
                                   <c:if test="${isGuest }">
									<div class="userinfo-edit-main">试玩账号不能提款</div>
									</c:if>
									<c:if test="${!isGuest }">
									<!-- 取款密码设置页面 -->
									<c:if test="${type eq '1' }">
										<jsp:include page="drawStep/drawPwd.jsp"></jsp:include>
									</c:if>
									<!-- 信息填写页面 -->
									<c:if test="${type eq '2' }">
										<jsp:include page="drawStep/drawData.jsp"></jsp:include>
									</c:if>
									<!-- 提款页面 -->
									<c:if test="${type eq '3' }">
										<jsp:include page="drawStep/drawCommit.jsp"></jsp:include>
									</c:if>
									</c:if>
                                </div>
                                <div class="box_list_bottom">
                                </div>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>