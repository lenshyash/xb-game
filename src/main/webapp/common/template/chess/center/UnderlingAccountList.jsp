<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <title>个人中心</title> 
  <link type="text/css" rel="stylesheet" title="" href="${base }/css/layout.min.css" /> 
  <script type="text/javascript" src="${base }/js/jquery-1.11.3.min.js"></script> 
  <script type="text/javascript" src="${base }/js/index.js"></script> 
 </head> 
 <body> 
  <jsp:include page="/member/${stationFolder}/include/head.jsp"></jsp:include> 
  <jsp:include page="/common/modelCommon/index.jsp" />  
  <script>
		var leftPosition = "603";
	</script> 
  <style>
	    .menu_member a {
	        background: url(../../../images/menu_bg.jpg) -108px -38px no-repeat
	    }
	</style> 
  <jsp:include page="/member/${stationFolder}/include/page-head.jsp"></jsp:include> 
  <jsp:include page="/member/${stationFolder}/include/float.jsp"></jsp:include> 
  <div class="container"> 
   <div class="maincontent" style="height:1000px"> 
    <div class="maincontent_top"> 
    </div> 
    <div class="maincontent_min"> 
     <!-- main_box --> 
     <div class="main_box"> 
      <!-- left box --> 
      <jsp:include page="include/left-nav.jsp"></jsp:include> 
      <!-- left box end --> 
      <!-- member_box_right --> 
			<div class="member_box_right">
                        <div class="main_box_right_title common_nav">
                            <span>您的位置：<a href="/">首页</a> &gt; <a href="/User/InfoCenter.aspx">用户中心</a> &gt; <label>我的直属下线</label></span>
                        </div>
                        <div class="main_box_right_con">
                            <!--box_list-->
                            <div class="box_list">
                                <form method="post" action="./UnderlingAccountList.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5PdSFnR74B5E2bYOH/weHPjB0bh+DQ9EJsy5pyOmt2O7YUWcVSxBhrBPFHDqENFpjStUU4oCPda1MwwSgXV+jtQgA3c2FdhxItYxQndItXI0DqsYffMl/W6qj9jwA6b0QziLUFI0DOyKg7/LNLpk4UOlwZcwIRCyFZxU7/1Jn/SPFE4z6eiQmUO+M8CR27qj4p3EzID99MVPHPvhlaB6o9d7zFynIbB+Vf9wX3qkbclxcSK594Gvers+DJPq+wBM1217x300W6S6UjZxbtsWKkoSTSCKxDPiq+Xe9BR+sq81cRKRMtjvlJTfDMvjQ2HiMUd/+Q==">
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F59CB350">
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="nmwwd5qL89yJ8Gn02O/H5K44iK0+wUBKwbhYcbDPbMUind3p/ditoKi72oLafzN57tUSqAzZEqM46Vd9hCfOvJ/lJ6xA4BlExdTekcBJqQ6sWDG+kiDsEN1JuyLfnefPxJ8OkbyzsNeacUnCfRUTs7Z1h+pSTMXGUAB8NJEJDMvvrjHYE6KffMauabA=">
</div>
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="height: 45px;">
                                    <tbody><tr>
                                        <td width="13%" align="right">
                                            用户帐号：
                                        </td>
                                        <td width="10%">
                                            <input name="txtKey" type="text" maxlength="20" id="txtKey" class="text_normal">
                                        </td>
                                        <td>
                                            <input type="submit" name="btnQuery" value="查询" id="btnQuery" class="btn1">
                                            <input type="button" name="btnRefresh" value="刷新" class="btn1" onclick="window.location.href=location.href;">
                                            <input name="btnAddAccount" type="button" id="btnAddAccount" value="添加下线" class="btn1" onclick="window.location.href='AddAccount.aspx';" style="display: none;">
                                        </td>
                                    </tr>
                                </tbody></table>
                                <table width="100%" cellpadding="0" cellspacing="1" class="tb_list">
                                    <tbody><tr>
                                        <th>用户名 </th>
                                        <th>姓名 </th>
                                        <th>注册时间 </th>
                                        <th>状态 </th>
                                        <th>操作 </th>
                                    </tr>
                                    
                                    <tr class="tdbg"><td colspan="8" align="center"><br>没有任何信息!<br><br></td></tr>
                                </tbody></table>
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="height: 45px;">
                                    <tbody>
                                        <tr>
                                            <td align="right">
                                                <div id="anpPage">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr>
		<td valign="bottom" nowrap="true" style="width:40%;">总记录：0　页码：1/1　每页：20</td><td valign="bottom" nowrap="true" style="width:60%;"><a disabled="disabled" style="margin-right:5px;">首页</a><a disabled="disabled" style="margin-right:5px;">上页</a><span style="margin-right:5px;font-weight:Bold;color:red;">1</span><a disabled="disabled" style="margin-right:5px;">下页</a><a disabled="disabled" style="margin-right:5px;">末页</a>&nbsp;&nbsp;<input type="text" value="1" disabled="disabled" name="anpPage_input" id="anpPage_input" onkeydown="ANP_keydown(event,'anpPage_btn');" style="width:30px;"><input type="submit" value="go" name="anpPage" id="anpPage_btn" disabled="disabled" onclick="if(ANP_checkInput('anpPage_input',1)){__doPostBack('anpPage','')} else{return false}"></td>
	</tr>
</tbody></table>
</div>


                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>

</form>
                            </div>
                            <!--box_list end-->
                            <div class="main_box_right_bottom">
                            </div>
                        </div>
                    </div>
      <!-- member_box_right end --> 
     </div> 
     <!-- main_box end --> 
    </div> 
    <div class="maincontent_bottom"> 
    </div> 
   </div> 
   <div class="clearfloat"> 
   </div> 
  </div> 
  <jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>   
 </body>
</html>