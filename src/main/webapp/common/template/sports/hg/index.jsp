<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/sports/hg/include/base.jsp"></jsp:include>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>皇冠体育</title>
		<script language="javascript">
			var USA_TIME = <%=(System.currentTimeMillis() - 12 * 60 * 60 * 1000)%>;
		</script>
		<jsp:include page="/common/template/sports/hg/include/index/index_resource.jsp"></jsp:include>

		<style media=print type="text/css">
		.noprint{visibility:hidden}
		</style>
	</head>
	<body>
		<input value="${isHgWhOnOff}" type="hidden" class="isSport">
		<div id="sportWeihuHtml">
		<jsp:include page="/common/template/sports/hg/include/index/top.jsp"></jsp:include>
		<div style="width:100%;overflow:hidden;" id="phoneSports">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="240" valign="top">
						<jsp:include page="/common/template/sports/hg/include/index/left.jsp"></jsp:include>
					</td>

					<td class ="noprint" valign="top">
						<jsp:include page="/common/template/sports/hg/include/index/right.jsp"></jsp:include>
					</td>
				</tr>
			</table>
		</div>
		</div>
		<div style="display:none;margin-top: 10px;" id="sportsWeihu">
			<img alt="维护中" src="${base }/common/template/sports/hg/images/sports/sportsWeihu.png">
		</div>
		<script>
		var sport =$('.isSport').val();
		if(sport){
			if(sport == 'on'){
				$('#sportWeihuHtml').css('display','none');
				$('#sportsWeihu').css('display','block');
			}else if(sport == 'off'){
				
			}
		}	
		window.onload = function(){
			　　var browser={
					versions:function(){
						var u = navigator.userAgent, app = navigator.appVersion;
						return {//移动终端浏览器版本信息
						trident: u.indexOf('Trident') > -1, //IE内核
						presto: u.indexOf('Presto') > -1, //opera内核
						webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
						gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
						mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
						ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
						android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
						iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
						iPad: u.indexOf('iPad') > -1, //是否iPad
						webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
						weixin: u.indexOf('MicroMessenger') > -1, //是否微信
						qq: u.match(/\sQQ/i) == " qq" //是否QQ
						};
						}(),
						language:(navigator.browserLanguage || navigator.language).toLowerCase()
						};

						if(browser.versions.mobile || browser.versions.ios || browser.versions.android ||
						browser.versions.iPhone || browser.versions.iPad){
						//移动
						$("#phoneSports").css("overflow","visible")
						}else{
						//PC

						}
			}
</script>
	</body>
</html>

<script language="javascript">
	var isLogin = <%=org.jay.frame.util.SysUtil.isLogin()%>;
	$(function(){
		$("#right_content").height(940);
		$("#left_content").height(835);
		$("#right_content").width(window.screen.width - 240);
	})
	//var bodyH = $(window).height();
</script>
