<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/sports/hg/include/base.jsp"></jsp:include>


<
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="Robots" contect="none">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Sportsbook Rules</title>
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa.css" type="text/css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa_roul.css" type="text/css">

<script language="javascript" src="${base}/common/template/sports/hg/help/js/QA_sport.js"></script>
</head>
<body id="SPORT">
<div class="qa_head"><em>帮助</em><span class="close_box" onClick="window.history.go(-1)">关闭</span></div>
<div id="wrapper">
	
    <div id="qa_nav">
      <ul>
         <li class="sport"><a href="${base}/help/QA_sport.do">体育规则</a></li>
        <li class="rule"><a href="${base}/help/QA_roul.do">规则与条款</a></li>
        <li class="odd"><a href="${base}/help/QA_way.do">赔率计算列表</a></li>
      </ul>
    </div>
    
    <div id="main">
    <a name="top"></a>
    	<h1>选择体育项目 : 
    	  <label>
			<select name="jumpMenu" id="jumpMenu" onchange="chg_roul(this.options[this.selectedIndex].value);">
				<option value="${base}/help/QA_sport.do">一般体育说明</option>
				<option value="${base}/help/roul/outright.do">冠军</option>
				<option value="${base}/help/roul/parlays-multiples.do">综合过关/派彩结算</option>
				<option value="${base}/help/roul/roul_nf.do" selected="selected">美式足球</option>
				<option value="${base}/help/roul/basketball.do">篮球</option>
				<option value="${base}/help/roul/beach-soccer.do">沙滩足球</option>
				<option value="${base}/help/roul/football.do">足球</option>	
	  	    </select>
  	    </label>
    	
    	 
  	          
    	</h1>
        <div id="info"><!--资料区-->
    <p class="b sub">美式足球</p>
    <p class="b sub"><em>最後更新日期：04/03/2015</em></p>
    <p class="b sub">一般规则</p>
    <ul style="list-style:decimal;">
      <li>除非另有注明, 所有比赛至少要进行55分钟，投注的注单才被视为有效。 </li>
      <li>最终赛果是以全场四个时节後的比分为准（包括加时赛）。 </li>
      <li>单节/半场的投注，必须在比赛赛节完成後注单才被视为有效。 </li>
      <li>第四节投注不包括加时赛。 </li>
      <li>美国大学美式足球联赛场地规则：盘口指示的"主场"和"客场"信息仅供参考。无论原定场地是否更改为"主场"，"客场"或"中立场"，所有注单将保持有效。 </li>
      <li>除非个别玩法规则另有注明，赛事完场时间将包括加时赛。 </li>
      <li>如比赛在法定时间提前进行，在比赛开始前的投注依然有效，在比赛开始後的所有投注均视为无效(滚球投注另作别论)。 </li>
    </ul>
    <p class="b sub">投注类型</p>
    <ul style="margin-left: -27px;">
      <p class="b sub">独赢盘</p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队将在比赛胜出。盘口提供两支球队为投注选项。 </li>
      </ul>
      <p class="b sub">让分盘</p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队在盘口指定的让分数赢得某个时节或全场比赛。 </li>
      </ul>
      <p class="b sub">滚球让分盘</p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队在盘口指定的让分数赢得某个时节或全场比赛。 </li>
        <li>结算是以0-0的比分在比赛结束後按盘口开出的让分数做裁决。投注当时的比分对结算没有影响。 </li>
      </ul>
      <p class="b sub">大/小盘（总比分）</p>
      <ul style="list-style:decimal;">
        <li>预测赛事总比分将大於或小於在盘口指定的大/小盘分数。 </li>
      </ul>
      <p class="b sub">滚球大/小盘（总比分）</p>
      <ul style="list-style:decimal;">
        <li>预测赛事总比分将大於或小於在盘口指定的大/小盘分数。 </li>
        <li>结算是以0-0的比分在比赛结束後按盘口开出的让分数做裁决。投注当时的比分对结算没有影响。 </li>
      </ul>
      <p class="b sub">单/双</p>
      <ul style="list-style:decimal;">
        <li>预测赛事的总比分是单数或双数。加时赛将包括在内。 </li>
      </ul>
      <p class="b sub">半场/全场</p>
      <ul style="list-style:decimal;">
        <li>预测在全场四个时节後的半全场优胜球队。加时赛将包括在内。 </li>
        <li>此盘口也被称为'双重结果'。 </li>
      </ul>
      <p class="b sub">净胜分数</p>
      <ul style="list-style:decimal;">
        <li>投注的结算根据全场四个时节後两支球队比分的差别做裁决。加时赛将包括在内。 </li>
        <li>如果比赛在任何时间中断，所有注单将被取消。 </li>
      </ul>
      <p class="b sub">最先得分球队</p>
      <ul style="list-style:decimal;">
        <li>预测最先得分的球队。 </li>
        <li>如果赛事在有得分後中断，所有最先得分球队的注单将保持有效。 </li>
        <li>如果赛事在没有球队得分前中断，所有最先得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间以及加时赛内没有球队得分，所有最先得分球队的注单将被取消。 </li>
      </ul>
      <p class="b sub">最後得分球队</p>
      <ul style="list-style:decimal;">
        <li>预测最後得分的球队。 </li>
        <li>如果赛事中断， 所有最後得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间以及加时赛内没有球队得分，所有最後得分球队的注单将被取消。 </li>
      </ul>
      <p class="b sub">单节最高得分球队</p>
      <ul style="list-style:decimal;">
        <li>预测单节最高得分的球队。 </li>
        <li>加时赛不计算在内。 </li>
        <li>如果赛事中断，所有单节最高得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间内没有球队得分，所有单节最高得分球队的注单将被取消。 </li>
      </ul>
      <p class="b sub">每节最先获得10分的球队</p>
      <ul style="list-style:decimal;">
        <li>预测每节最先得10分的球队。 </li>
        <li>加时赛不计算在内。 </li>
        <li>如果赛事中断，所有每节最先获得10分的球队的注单将被取消.</li>
        <li>如果每节都没有球队获得10分，所有注单将被取消。 </li>
        <li>取决於赛事，玩法指定球队需最先获得的分数可能有变化，并且会清楚的显示在盘口。 </li>
      </ul>
    </ul>
    <div class="to_top"><a href="#top"><span>回最上层</span></a></div>
  </div>
  
    </div>
    
</div>
<div class="qa_foot"></div>
</body>
</html>
