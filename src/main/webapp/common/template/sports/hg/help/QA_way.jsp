<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/sports/hg/include/base.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--link rel="stylesheet" href="/style/member/mem_body_ft.css" type="text/css"-->
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa.css" type="text/css">

<title>赔率计算列表</title>
</head>

<body id="ODD">
<div class="qa_head"><em>帮助</em><span class="close_box" onClick="window.history.go(-1)">关闭</span></div>
<div id="wrapper">
	
    <div id="qa_nav">
      <ul>
        <li class="sport"><a href="${base}/help/QA_sport.do">体育规则</a></li>
        <li class="rule"><a href="${base}/help/QA_roul.do">规则与条款</a></li>
        <li class="odd"><a href="${base}/help/QA_way.do">赔率计算列表</a></li>
      </ul>
    </div>
    
    <div id="main">
        <div class="info">
    
			<table border="0" cellpadding="0" cellspacing="0" id="box">
              <tr>
					<td class="top">
						<h1><b>各类型盘口使用方法请参考以下表格：</b></h1>
                  
					</td>
              </tr>
              <tr>
					<td class="mem">
						<table width=300 border=0 cellpadding=1 cellspacing=0 class="game">
						  <tr>
							<th >盘口类型</th>
							<th >投注金额</th>
							<th >赔率</th>
							<th >赢</th>
							<th >输</th>
						  </tr>
						  <tr class="b_rig">
							<td class="b_hline">香港盘</td>
							<td>1000</td>
							<td>0.800</td>
							<td>800</td>
							<td>-1000</td>
						  </tr>
						  <tr class="b_rig">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>1.130</td>
							<td>1130</td>
							<td>-1000</td>
						  </tr>
						  <tr class="b_rig">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr class="b_rig">
							<td class="b_hline">欧洲盘</td>
							<td>1000</td>
							<td>1.800</td>
							<td>800</td>
							<td>-1000</td>
						  </tr>
						  <tr class="b_rig">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>2.130</td>
							<td>1130</td>
							<td>-1000</td>
						  </tr>
						</table>
					</td>
				</tr>
				<tr><td id="foot"><b>&nbsp;</b></td></tr>
            </table>
    
    </div>
			<div class="to_top"><a href="#top"><span>回最上层</span></a></div>
    
        
    </div>
    
</div>
<div class="qa_foot"></div>
</body>
</html>