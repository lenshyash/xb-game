<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/sports/hg/include/base.jsp"></jsp:include>


<html>
<head>
<meta name="Robots" contect="none">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Sportsbook Rules</title>
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa.css" type="text/css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa_roul.css" type="text/css">
<script language="javascript" src="${base}/common/template/sports/hg/help/js/QA_sport.js"></script>
</head>
<body id="SPORT">
<div class="qa_head"><em>帮助</em><span class="close_box" onClick="window.history.go(-1)">关闭</span></div>
<div id="wrapper">
	
    <div id="qa_nav">
      <ul>
        <li class="sport"><a href="${base}/help/QA_sport.do">体育规则</a></li>
        <li class="rule"><a href="${base}/help/QA_roul.do">规则与条款</a></li>
        <li class="odd"><a href="${base}/help/QA_way.do">赔率计算列表</a></li>
      </ul>
    </div>
    
    <div id="main">
    <a name="top"></a>
    	<h1>选择体育项目 : 
    	  <label>
				<select name="jumpMenu" id="jumpMenu" onchange="chg_roul(this.options[this.selectedIndex].value);">
   				<option value="${base}/help/QA_sport.do">一般体育说明</option>
				<option value="${base}/help/roul/outright.do">冠军</option>
				<option value="${base}/help/roul/parlays-multiples.do">综合过关/派彩结算</option>
				<option value="${base}/help/roul/roul_nf.do">美式足球</option>
				<option value="${base}/help/roul/basketball.do">篮球</option>
				<option value="${base}/help/roul/beach-soccer.do" selected="selected">沙滩足球</option>
				<option value="${base}/help/roul/football.do">足球</option>	
  	    </select>
  	    </label>
    	
    	 
  	          
    	</h1>
        <div id="info"><!--资料区-->
    <p class="b sub">沙滩足球</p>
    <p class="b sub"><em>最後更新日期：04/03/2015</em></p>
    <p class="b sub">一般规则</p>
    <ul style="list-style:decimal;">
      <li>如果比赛场地有变更，所有注单将被取消。 </li>
      <li>除非另有注明，所有沙滩足球投注的结算皆以球赛在3节（每节12分钟）完赛後的最终赛果为准。 </li>
      <li>如比赛在法定时间提前进行，在比赛开始前的投注依然有效，在比赛开始後的所有投注均视为无效(滚球投注另作别论)。 </li>
    </ul>
    <p class="b sub">投注类型</p>
    <ul style="margin-left: -27px;">
      <p class="b sub">1 X 2 </p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队将在比赛胜出。盘口提供两支球队和平局为投注选项。 </li>
      </ul>
      <p class="b sub">让球盘</p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队在盘口指定的让球数赢得某个时节或全场比赛。 </li>
      </ul>
      <p class="b sub">滚球让球盘</p>
      <ul style="list-style:decimal;">
        <li>预测哪一支球队在盘口指定的让球数赢得某个时节或全场比赛。 </li>
        <li>结算是以投注时到比赛/时节结束後的赛果做裁决。即是以赛事完场比分减去投注当时的比分。 </li>
      </ul>
      <p class="b sub">大/小盘</p>
      <ul style="list-style:decimal;">
        <li>预测赛事总入球数将大於或小於在盘口指定的大/小盘球数。 </li>
        <li>如果赛事中断前已有明确结果并且之後没有任何显着会影响赛事结果的情况，大/小盘注单才会被结算。若遇到任何其他情况，注单将一律被取消。 </li>
      </ul>
    </ul>
    </ul>
  <div class="to_top"><a href="#top"><span>回最上层</span></a></div>
</div><!--资料区 End-->
    </div>
    
</div>
<div class="qa_foot"></div>
</body>
</html>
