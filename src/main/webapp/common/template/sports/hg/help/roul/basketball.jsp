<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/sports/hg/include/base.jsp"></jsp:include>


<html>
<head>
<meta name="Robots" contect="none">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Sportsbook Rules</title>
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa.css" type="text/css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/help/css/mem_qa_roul.css" type="text/css">
<script language="javascript" src="${base}/common/template/sports/hg/help/js/QA_sport.js"></script>
</head>
<body id="SPORT">
<div class="qa_head"><em>帮助</em><span class="close_box" onClick="window.history.go(-1)">关闭</span></div>
<div id="wrapper">
	
    <div id="qa_nav">
      <ul>
        <li class="sport"><a href="${base}/help/QA_sport.do">体育规则</a></li>
        <li class="rule"><a href="${base}/help/QA_roul.do">规则与条款</a></li>
        <li class="odd"><a href="${base}/help/QA_way.do">赔率计算列表</a></li>
      </ul>
    </div>
    
    <div id="main">
    <a name="top"></a>
    	<h1>选择体育项目 : 
    	  <label>
			<select name="jumpMenu" id="jumpMenu" onchange="chg_roul(this.options[this.selectedIndex].value);">
   				<option value="${base}/help/QA_sport.do">一般体育说明</option>
				<option value="${base}/help/roul/outright.do">冠军</option>
				<option value="${base}/help/roul/parlays-multiples.do">综合过关/派彩结算</option>
				<option value="${base}/help/roul/roul_nf.do">美式足球</option>
				<option value="${base}/help/roul/basketball.do"  selected="selected">篮球</option>
				<option value="${base}/help/roul/beach-soccer.do">沙滩足球</option>
				<option value="${base}/help/roul/football.do">足球</option>	
  	    	</select>
  	    </label>
    	
    	 
  	          
    	</h1>
        <div id="info"><!--资料区-->
    <p class="b sub">篮球</p>
    <p class="b sub"><em>最後更新日期：04/03/2015</em></p>
    <p class="b sub">一般规则</p>
    <ul style="list-style:decimal">
      <li>如果比赛场地有变更，所有注单将被取消。 </li>
      <li>NBA/NBL篮球赛事必须至少进行43分钟，投注的注单才被视为有效。任何其他的篮球联赛或比赛，至少要进行35分钟，投注的注单才被视为有效，除非另有注明。 </li>
      <li>美国大学篮球联赛至少必须进行35分钟，投注的注单才被视为有效，除非另有注明。 </li>
      <li>如果赛事是在上半场中断，所有上半场的注单将被取消。如果赛事是在下半场中断所有上半场的投注保持有效，但所有下半场的注单将被取消，除非在个别玩法规则另有注明。 </li>
      <li>单节/半场的投注，比赛必须完成赛节注单才被视为有效，除非在个别玩法规则另有注明。 </li>
      <li>美国大学篮球联赛场地规则：盘口指示的"主场"和"客场"信息仅供参考。无论原定场地是否更改为"主场"，"客场"或"中立场"，所有注单将保持有效。 </li>
      <li>除非个别玩法规则另有注明，赛事完场时间将包括加时赛。 </li>
      <li>如比赛在法定时间提前进行，在比赛开始前的投注依然有效，在比赛开始後的所有投注均视为无效(滚球投注另作别论)。 </li>
      <li>所有赛事或下半场的投注都包括加时赛，除非另有注明。第四节投注不包括加时赛。</li>
    </ul>
    <p class="b sub">投注类型</p>
    <ul style="margin-left: -27px;">
      <p class="b sub">独赢盘</p>
      <ul style="list-style:decimal">
        <li>预测哪一支球队将在比赛胜出。盘口提供两支球队为投注选项。 </li>
        <li>赛事盘口包括全场、半场或单节投注。</li>
      </ul>
      <br />
      <p class="b sub">让球盘</p>
      <ul style="list-style:decimal">
        <li>预测哪一支球队在盘口指定的让分球数在半场/全场/赛事单节赢得比赛。 </li>
        <li>如果赛事在下半场取消或中断，所有上半场注单保持有效。 </li>
        <li>如果赛事在下半场取消或中断，所有下半场注单将被取消。 </li>
        <li>赛事盘口包括全场、半场或单节投注。</li>
      </ul>
      <br />
      <p class="b sub">滚球让球盘</p>
      <ul style="list-style:decimal">
        <li>预测哪一支球队在盘口指定的让分数里赢得半场/全场/赛事单节的比赛。 </li>
        <li>结算是以0-0的比分在比赛/时节结束後按盘口开出的让分球数做裁决。投注当时的比分对结算没有影响。 </li>
        <li>赛事盘口包括全场、半场或单节投注。</li>
      </ul>
      <br />
      <p class="b sub">大/小盘（总比分）</p>
      <ul style="list-style:decimal">
        <li>预测赛事总比分将大於或小於在盘口指定的大/小盘分数。 </li>
        <li>赛事盘口包括全场、半场或单节投注。</li>
        <li>如果赛事中断前已有明确结果，大/小盘注单才会被结算。若遇到任何其他情况，注单将一律被取消。 </li>
        <li>如果赛事在上半场中断，所有上半场注单将被取消，除非中断前已有明确结果。 </li>
        <li>如果赛事在下半场取消或中断，所有上半场注单保持有效。 </li>
        <li>如果赛事在下半场取消或中断，所有下半场注单将被取消，除非中断前已有明确结果 </li>
        <li>如果赛事中断, 所有时节的注单将被取消除非遇到以下情况：
          <ul style="list-style:lower-alpha">
            <li>投注的时节是在比赛中断前。 </li>
          </ul>
        </li>
      </ul>
      <br />
      <p class="b sub">滚球大/小盘（总比分）</p>
      <ul style="list-style:decimal">
        <li>预测赛事总比分将大於或小於在盘口指定的大/小盘分数。 </li>
        <li>结算是以0-0的比分在比赛/时节结束後按盘口开出的让分数做裁决。投注当时的比分对结算没有影响。 </li>
        <li>如果赛事中断前已有明确结果，大/小盘注单才会被结算。若遇到任何其他情况，注单将一律被取消。 </li>
      </ul>
      <br />
      <p class="b sub">大/小盘（主队或者客队的分数）</p>
      <ul style="list-style:decimal">
        <li>预测赛事主队/客队的总分数将大於或小於在盘口指定的大/小盘分数。</li>
        <li>如果赛事取消，所有的注单将会被认为无效，除非注单在中断前已经结算或者在中断前已有明确的结果。</li>
        <li>所有注单的结算都是依据相关体育机构的官方统计数据为准。</li>
      </ul>
      <br />
      <p class="b sub">单/双</p>
      <ul style="list-style:decimal">
        <li>预测赛事的总比分是单数或双数。加时赛将包括在内。 </li>
        <li>赛事盘口包括全场、半场或单节投注。</li>
      </ul>
      <br />
      <p class="b sub">最先得分球队</p>
      <ul style="list-style:decimal">
        <li>预测最先得分的球队。 </li>
        <li>如果赛事在有得分後中断，所有最先得分球队的注单将保持有效。 </li>
        <li>如果赛事在没有球队得分前中断，所有最先得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间以及加时赛内没有球队得分，所有最先得分球队的注单将被取消。 </li>
      </ul>
      <br />
      <p class="b sub">最後得分球队</p>
      <ul style="list-style:decimal">
        <li>预测最後得分的球队。 </li>
        <li>如果赛事中断， 所有最後得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间以及加时赛内没有球队得分，所有最後得分球队的注单将被取消。 </li>
      </ul>
      <br />
      <p class="b sub">单节最高得分球队</p>
      <ul style="list-style:decimal">
        <li>预测单节最高得分的球队。 </li>
        <li>加时赛不计算在内。 </li>
        <li>如果赛事中断，所有单节最高得分球队的注单将被取消。 </li>
        <li>如果赛事在4节完场时间以及加时赛内没有球队得分，所有最後单节最高得分球队的注单将被取消。 </li>
      </ul>
      <br />
      <p class="b sub">每节最先获得20分的球队</p>
      <ul style="list-style:decimal">
        <li>预测每节最先得20分的球队。 </li>
        <li>加时赛不计算在内。 </li>
        <li>如果赛事中断前已有明确结果并且之後没有任何显着会影响赛事结果的情况，注单才会被结算。若遇到任何其他情况，注单将一律被取消。 </li>
        <li>如果每节都没有球队获得20分，所有注单将被取消。 </li>
        <li>取决於赛事，玩法指定球队需最先获得的分数可能有变化，并且会清楚的显示在盘口。 </li>
      </ul>
      <br />
      <p class="b sub">篮球特别投注</p>
      <ul style="list-style:decimal">
        <li>预测比分，抢断，篮板，助攻，3分球等。 </li>
        <li>注单的结算将根据赛事（包括加时赛）官方冠军赛果为准。 </li>
        <li>双方球员/球队必须参与比赛，投注才被视为有效。 </li>
        <li>如果一方或双方球员/球队没有参与比赛，所有注单将被取消。 </li>
        <li>注单的结算将根据NBA或特别官方机构公布的结果为准，并且任何赛後更改的数据将被视为无效。 </li>
      </ul>
      <br />
      <p class="b sub">球队得分 – 最後一位数字</p>
      <ul style="list-style:decimal">
        <li>预测主队或客队最终得分的最後一位数字。</li>
        <li>赛果以官方公布的赛事结果为准，包括加时。</li>
      </ul>
      <br />
      <p class="b sub">梦幻篮球游戏规则</p>
      <ul style="list-style:decimal">
        <li>梦幻篮球游戏将从同一天的赛事中任意选择两个球队（且原定不是在同一场赛事比赛的球队）进行投注。 </li>
        <li>梦幻比赛赛果会根据球队真实的比分为准；梦幻比赛的让分数则以球队真实比分来计算。 </li>
        <li>梦幻赛中的两支球队必须在同一天比赛，投注才被视为有效。 </li>
        <li>如果球队的比赛时间和本公司网站显示的时间不同，所有涉及此球队的梦幻赛注单将被视为无效。</li>
        <li>梦幻赛投注将不考虑赛事实际进行的场地。 </li>
        <li>梦幻赛的举例如下:
          <ul style="list-style:lower-alpha">
            <li>波士顿凯尔特人101 – 98 芝加哥公牛，洛杉矶湖人118 – 101 奥兰多魔术</li>
            <li>梦幻赛1： 波士顿凯尔特人vs 洛杉矶湖人</li>
            <li>梦幻赛赛果：波士顿凯尔特人101 – 118洛杉矶湖人</li>
            <li>梦幻赛的结算会根据各球队原定比赛的真实得分为准。 </li>
          </ul>
        </li>
        <li>球队一定要实际完成原定比赛并且已在赛事的官方机构（例如：NBA）留下赛果记录，才能让涉及此球队的梦幻赛注单保持有效。如果球队没有完成原定比赛或最终赛果被官方否定，所有涉及此球队的梦幻赛注单将被取消。 </li>
        <li>所有梦幻赛都将按照篮球个别玩法的规则和标准裁决。 </li>
      </ul>
      <br />
    </ul>
 <div class="to_top"><a href="#top"><span>回最上层</span></a></div>
</div><!--资料区 End-->
    </div>
    
</div>
<div class="qa_foot"></div>
</body>
</html>
