
Basketball = function(gameType){
	this.gameType = gameType;
	this.init();	
};

Basketball.prototype = {
	getName:function(){
		if(this.isFuture()){
			return "篮球 - 早盘";
		}else if(this.isRunball()){
			return "篮球 - 滚球";
		}
		return "篮球";
	},
	formatOdds : function(data){
		var odd_f_type = this.getOddsType();
		if(this.isMain()){
			var R_ior  = HG.get_other_ioratio(odd_f_type, data.ior_RH  ,data.ior_RC);
			var OU_ior = HG.get_other_ioratio(odd_f_type, data.ior_OUH,data.ior_OUC);
			var OUH_ior = HG.get_other_ioratio(odd_f_type,data.ior_OUHO,data.ior_OUHU);
			var OUC_ior = HG.get_other_ioratio(odd_f_type,data.ior_OUCO,data.ior_OUCU);
	
			data.ior_RH = R_ior[0];
			data.ior_RC = R_ior[1];
			data.ior_OUH = OU_ior[0];
			data.ior_OUC = OU_ior[1];
			data.ior_OUHO = OUH_ior[0];
			data.ior_OUHU = OUH_ior[1];
			data.ior_OUCO = OUC_ior[0];
			data.ior_OUCU = OUC_ior[1];
		}
	},
	showRunballGameTime:function(rowData,isMobile){
		var se_now = rowData.nowSession;
		var str_se_now = "";
		var statusMap = {
			"OT":"加时",
			"HT":"半场",
			"H1":"上半场",
			"H2":"下半场",
			"Q1":"第一节",
			"Q2":"第二节",
			"Q3":"第三节",
			"Q4":"第四节"
		};
		if(statusMap[se_now]){
			str_se_now = statusMap[se_now];
		}

		var rb_time = "";
		if(se_now != "HT"){ //半场休息
			var lastT = rowData.lastTime * 1;
			if(isNaN(lastT) || lastT<0 ){
				lastT = 0;
			}
			var TimeM = Math.floor(lastT/60);
			var TimeS = lastT % 60;
			if(TimeM < 10 ){
				TimeM="0"+TimeM;
			}
			if(TimeS < 10){
				TimeS="0"+TimeS;
			}
			var rb_time = TimeM+":"+TimeS;
		}

		var scoreH = (rowData.lastGoal=="H") ? '<span class="bk_color">' + rowData.scoreH + '</span>' : rowData.scoreH;
		var scoreC = (rowData.lastGoal=="A") ? '<span class="bk_color">' + rowData.scoreC + '</span>' : rowData.scoreC;
		if(isMobile){
			return '<span>' + str_se_now + '</span>&nbsp;&nbsp;&nbsp;' + scoreH + '-' + scoreC;
		}
		return '<table border="0" cellpadding="0" cellspacing="0" class="rb_box_bk"><tbody> ' +
						' <tr><td class="rb_time_bk">'+str_se_now+'&nbsp;<span class="rb_time_color">'+rb_time+'</span></td></tr>'+
						' <tr><td class="rb_score_bk">'+scoreH+' - '+scoreC+'</td></tr>'+
					'</tbody></table>';
		
	},
	getBetInfoData:function(gameData,key){
		var odds = gameData[key];
		var gid = gameData.gid;
		if(!odds){
			return "投注项变动";
		}
		
		var data = {
			league:gameData.league,//联赛
			gid : gid,
			odds: odds,
			type : key,
			mid:gameData.matchId
		};
		if(this.isRunball()){//放入动态比分
			data.scoreH = gameData.scoreH;
			data.scoreC = gameData.scoreC;
		}
		var h = gameData.home;
		var g = gameData.guest;
		var homeFirst = true;//主队显示在前面
		
		if(this.isMain() || this.isMix()){
			//全场独赢
			if(key == "ior_MC" || key == "ior_MH" || key == "ior_MN"){
				if(this.isRunball()){
					data.title = "全场 滚球 - 独赢";
				}else{
					data.title = "全场 - 独赢";
				}
				data.team = h + " vs. " + g;
				var win = "";
				if(key == "ior_MH"){
					win = h;
				}else if(key == "ior_MC"){
					win = g;
				}else{
					win = "和局";
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}
			
			//半场独赢
			if(key == "ior_HMC" || key == "ior_HMH" || key == "ior_HMN"){
				if(this.isRunball()){
					data.title = "半场 滚球 - 独赢";
				}else{
					data.title = "半场 - 独赢";
				}
				
				data.team = h + " vs. " + g;
				var win = "";
				if(key == "ior_HMH"){
					win = h;
				}else if(key == "ior_HMC"){
					win = g;
				}else{
					win = "和局";
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_RC" || key == "ior_RH"){//全场让球
				data.title = "全场 - 让球";
				var rb = gameData["CON_RH"];//让球
				data.project = rb;
				if(rb.indexOf("-") == 0){//客让主
					homeFirst = false;
					data.team = g+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb.substring(1)+'&nbsp;</span>' + h ;
				}else{
					data.team = h+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb+'&nbsp;</span>' + g ;
				}
				var win = "";
				if(key == "ior_RH"){
					win = h;
				}else if(key == "ior_RC"){
					win = g;
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_HRC" || key == "ior_HRH"){//半场让球
				data.title = "半场 - 让球";
				var rb = gameData["CON_HRH"];
				data.project = rb;
				if(rb.indexOf("-") == 0){//客让主
					homeFirst = false;
					data.team = g+ '<span class="radio" style="background-color: transparent;">&nbsp;'+rb.substring(1)+'&nbsp;</span>' + h ;
				}else{
					data.team = h+ '<span class="radio" style="background-color: transparent;">&nbsp;'+rb+'&nbsp;</span>' + g ;
				}
				var win = "";
				if(key == "ior_HRH"){
					win = h;
				}else if(key == "ior_HRC"){
					win = g;
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_OUH" || key == "ior_OUC"){//全场大小球
				data.team = h + " vs. " + g;
				data.title = "全场 - 大小";
				if(key == "ior_OUH"){//大
					data.project = gameData["CON_OUH"];
					data.oddsDesc = this.formatOddsStr("大 " + gameData["CON_OUH"],odds,gid);
				}else{
					data.project = gameData["CON_OUC"];
					data.oddsDesc = this.formatOddsStr("小 " + gameData["CON_OUH"],odds,gid);
				}
			}else if(key == "ior_OUHO" || key == "ior_OUHU" || key == "MS_RATIO_ROUO" || key == "MS_IOR_ROUH" || key == "MS_RATIO_ROUO"){//半场大小球
				data.team = h + " vs. " + g;
				data.title = "主队 - 大小";
				if(key == "ior_OUHO"){//大
					data.project = gameData["CON_OUHO"];
					data.oddsDesc = this.formatOddsStr("大&nbsp;" + gameData["CON_OUHO"],odds,gid);
				}else{
					data.project = gameData["CON_OUHU"];
					data.oddsDesc = this.formatOddsStr("小&nbsp;" + gameData["CON_OUHU"],odds,gid);
				}
			}else if(key == "ior_OUCO" || key == "ior_OUCU" || key == "MS_RATIO_ROUU" || key == "MS_IOR_ROUC" || key == "MS_RATIO_ROUU"){//半场大小球
				data.team = h + " vs. " + g;
				data.title = "客队 - 大小";
				if(key == "ior_OUCO"){//大
					data.project = gameData["CON_OUCO"];
					data.oddsDesc = this.formatOddsStr("大&nbsp;" + gameData["CON_OUCO"],odds,gid);
				}else{
					data.project = gameData["CON_OUCU"];
					data.oddsDesc = this.formatOddsStr("小&nbsp;" + gameData["CON_OUCU"],odds,gid);
				}
			}
//			else if(key == "ior_EOO" || key == "ior_EOE"){//全场单双
//				data.team = h + " vs. " + g;
//				data.title = "全场 - 单双";
//				if(key == "ior_EOO"){//大
//					data.oddsDesc = this.formatOddsStr("单",odds,gid);
//				}else{
//					data.oddsDesc = this.formatOddsStr("双",odds,gid);
//				}
//			}
		}
		
		if(data.team !== undefined && this.isRunball()){
			data.team = data.team + "&nbsp;"+ this.getRunballScoreStr(homeFirst,gameData);
		}
		return data;
	},
	toCustomData:function(arr){
//		if(!this.isRunball()){
//			return arr;
//		}
//		var mmap = {};
//		for(var i=0;i<arr.length;i++){
//			var md = arr[i];
//			var mid = md.matchId;
//			if(mmap[mid]){
//				continue;
//			}
//			if(md.scoreH === ""){
//				continue;
//			}
//			mmap[mid] = md;
//		}
//		
//		for(var i=0;i<arr.length;i++){
//			var md = arr[i];
//			var mid = md.matchId;
//			if(md.scoreH === "" && mmap[mid]){
//				md.scoreH = mmap[mid].scoreH;
//				md.scoreC = mmap[mid].scoreC;
//			}
//		}
		return arr;
	}
};

//继承基本方法 必须放在 prototype 之后
extend(Basketball,SportFactory);
