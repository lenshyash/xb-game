BetItemKey = function (itemKey){
	this.itemKey = itemKey;
}

BetItemKey.prototype = {
		
	isFullWinOnly:function(){
		if(this.itemKey == "ior_MC" || this.itemKey == "ior_MH" || this.itemKey == "ior_MN"){
			return true;
		}
		return false;
	},
	isHalfWinOnly:function(){
		if(this.itemKey == "ior_HMC" || this.itemKey == "ior_HMH" || this.itemKey == "ior_HMN"){
			return true;
		}
		return false;
	},
	//篮球单队 总得分大小
	isSingleTeamBigSmall:function(){
		return this.isHomeTeamBigSmall() || this.isGuestTeamBigSmall();
	},
	isHomeTeamBigSmall:function(){
		return this.itemKey == "ior_OUHO" || this.itemKey == "ior_OUHU";
	},
	isGuestTeamBigSmall:function(){
		return this.itemKey == "ior_OUCO" || this.itemKey == "ior_OUCU";
	},
	isWinOnly:function(){
		return this.isHalfWinOnly() || this.isFullWinOnly();
	},
	isFullLetBall : function(){
		if(this.itemKey == "ior_RC" || this.itemKey == "ior_RH"){
			return true;
		}	
		return false;
	},
	isHalfLetBall : function(){
		if(this.itemKey == "ior_HRC" || this.itemKey == "ior_HRH"){
			return true;
		}
		return false;
	},
	isLetBall:function(){
		return this.isFullLetBall() || this.isHalfLetBall();
	},
	isFullBigSmall:function(){
		if(this.itemKey == "ior_OUH" || this.itemKey == "ior_OUC"){
			return true;
		}
		return false;
	},
	isHalfBigSmall:function(){
		if(this.itemKey == "ior_HOUH" || this.itemKey == "ior_HOUC"){//半场大小球
			return true;
		}
		return false;
	},
	isBigSmall:function(){
		return this.isFullBigSmall() || this.isHalfBigSmall() || this.isSingleTeamBigSmall();
	},
	isSingleDouble:function(){
		if(this.itemKey == "ior_EOO" || this.itemKey == "ior_EOE"){
			return true;
		}
		return false;
	}
}

GameKey = function(gameType){
	this.gameType = gameType;
	this.init();
}

GameKey.prototype = {
	init:function(){
		this.gameTypeArr = this.gameType.split("_");
	},
	isRunball:function(){//滚球
		return this.gameTypeArr[1] == "RB";
	},
	isToday:function(){ //今日
		return this.gameTypeArr[1] == "TD";
	},
	isFuture:function(){//早盘
		return this.gameTypeArr[1] == "FT";
	},
	isMix:function(){ //混合过关
		return this.gameTypeArr[2] == "MX";
	},
	isMain:function(){//独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双
		return this.gameTypeArr[2] == "MN";
	},
	isHalfTime:function(){ //半场波胆
		return this.gameTypeArr[2] == "HTI";
	},
	isFullTime:function(){//全场波胆
		return this.gameTypeArr[2] == "TI";
	},
	isTime:function(){//全场 或者 半场 波胆
		return this.isHalfTime() || this.isFullTime();
	},
	isBallCount:function(){//总入球
		return this.gameTypeArr[2] == "BC";
	},
	isHalfAndFull:function(){//半场输赢关系
		return this.gameTypeArr[2] == "HF";
	}
}

