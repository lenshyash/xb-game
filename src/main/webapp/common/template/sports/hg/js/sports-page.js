showSportsList = [
     "RB_FT_MN",
//     "RB_FT_TI",
//     "RB_FT_BC",
//     "RB_FT_HF",
     
     "RB_BK_MN",
     "TD_FT_MN",
     "TD_FT_TI",
     "TD_FT_BC",
     "TD_FT_HF",
     "TD_FT_MX",
     
     "TD_BK_MN",
     "TD_BK_MX",
     
     "FT_FT_MN",
     "FT_FT_TI",
     "FT_FT_BC",
     "FT_FT_HF",
     "FT_FT_MX",
     
     "FT_BK_MN",
     "FT_BK_MX"
];

PageViewData = [{
		name:'滚球',
		key : "RB",
		games:[{
			key:"FT",
			name:"足球",
			css:'ft',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			},{
				name : "波胆",
				key:'TI'
			},{
				name : "总入球",
				key : "BC"
			},{
				name : "半场 / 全场",
				key : "HF"
			}]
		},{
			key:"BK",
			name:"篮球",
			css:'bk',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			}]
		}]
	},{
		name:'今日赛事',
		key : "TD",
		games:[{
			key:"FT",
			name:"足球",
			css:'ft',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			},{
				name : "波胆",
				key:'TI'
			},{
				name : "总入球",
				key : "BC"
			},{
				name : "半场 / 全场",
				key : "HF"
			},{
				name : "综合过关",
				key:'MX'
			},{
				name:'冠军',
				key:'CH'
			}]
		},{
			key:"BK",
			name:"篮球",
			css:'bk',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			},{
				name : "综合过关",
				key:'MX'
			}]
		}]
	},{
		name:'早盘',
		key : "FT",
		games:[{
			key:"FT",
			name:"足球",
			css:'ft',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			},{
				name : "波胆",
				key:'TI'
			},{
				name : "总入球",
				key : "BC"
			},{
				name : "半场 / 全场",
				key : "HF"
			},{
				name : "综合过关",
				key:'MX'
				//dataKey:'TD_FT_MX'
			},{
				name:'冠军',
				key:'CH'
			}]
		},{
			key:"BK",
			name:"篮球",
			css:'bk',
			items:[{
				name : "独赢 ＆ 让球 ＆ 大小 ＆ 单 / 双",
				key:'MN'
			},{
				name : "综合过关",
				key:'MX'
			},{
				name:'冠军',
				key:'CH'
			}]
		}]
	}
];


function getNavDataMap(){
	if(window.navDataMap){
		return window.navDataMap;
	}
	window.navDataMap = {};
	for(var i = 0;i < showSportsList.length;i++){
		var key = showSportsList[i];
		var keys = key.split("_");
		navDataMap[keys[0]] = true;
		navDataMap[keys[0]+"_"+keys[1]] = true;
		navDataMap[key] = true;
	}
	return navDataMap;
}

function toMenuData(){
	
	if(window.wiewData){
		return wiewData;
	}
	
	var navDataMap = getNavDataMap();
	window.viewData = {
			
	};
	
	for(var i = 0; i< PageViewData.length; i++){
		var gameTime = PageViewData[i];
		if(!navDataMap[gameTime.key]){
			gameTime.display = false;
			continue;
		}
		var games = gameTime.games;
		for(var j = 0; j< games.length; j++){
			var game = games[j];
			var secKey = gameTime.key + "_" +game.key;
			if(!navDataMap[secKey]){
				continue;
			}
			var secArr = getNull2InitArr(viewData,gameTime.key);
			secArr.push({
				name : 	game.name,
				className : game.css,
				dataKey : secKey,
				key : game.key
			});
			var items = game.items;
			for(var z = 0; z< items.length; z++){
				var item = items[z];
				var thirdKey = secKey + "_" + item.key;
				if(!navDataMap[thirdKey]){
					continue;
				}
				var thirdArr = getNull2InitArr(viewData,secKey);
				thirdArr.push({
					name : 	item.name,
					key : item.key,
					dataKey : item.dataKey ? item.dataKey : thirdKey
				});
			}
		}
	}
	return viewData;
}

function getNull2InitArr(map,key){
	if(!map[key]){
		map[key] = [];
	}
	return map[key];
}

(function(){
	
	var selectDom = null;
	
	var selectGameTimeType = null;//RB：滚球  TD：今日  FT：早盘
	
	var selectSportType = null;//球类
	
	var selectDataType = null;
	
	var viewData = toMenuData();
	
	window.changeDay = function(ele,type){
		var jd = $(ele);
//		if(type == selectGameTimeType){
//			return;
//		}
		
		if(selectDom){ //选中状态改成未选中
			var cn = selectDom.attr("class");
			var index = cn.indexOf("_");
			if(index > -1){
				selectDom.attr("class",cn.substring(0,index));
			}
		}
		
		selectDom = jd;
		selectGameTimeType = type;
		var className = jd.attr("class");
		if(className.indexOf("_on") == -1){ //未选中改成选中
			jd.attr("class",className+"_on");
		}
		showSecondMenu(type);
		var sst = null;
		if(selectSportType){ //已选中球类
			var st = selectSportType.split("_")[1];
			var domId = "ST_" + type + "_" + st;
			sst = $("#" + domId);
		
		}
		if(!sst || sst.length == 0){ //选中第一个
			sst = $("#nav").find("a:first");
		}
		if(sst != null){
			sst.click();
		}
	}
	
	function showSecondMenu(type){
		var data = viewData[type];
		var html = template("second_navigation", {data: viewData[type]});
		$("#nav").html(html);
		refreshGameCount();
	}
	
	window.changeSportType = function(ele,key){
//		if(selectSportType == key){
//			return;
//		}
		$("#nav").find("a.type_on").removeClass("type_on");
		$(ele).addClass("type_on");
		selectSportType = key;
		showThirdMenu(key);
		
		var sst = null;
		if(selectDataType){ //已选中球类
			var keys = selectDataType.split("_");
			if(keys[1]  == selectSportType.split("_")[1]){
				var dt = keys[2];
				var domId = "GT_" + key + "_" + dt;
				sst = $("#" + domId);
			}
		}
		
		if(!sst || sst.length == 0){ //选中第一个
			sst = $("#type").find("a:first");
		}
		
		if(sst != null){
			sst.click();
		}
	}
	
	function showThirdMenu(key){
		var html = template("third_navigation", {data: viewData[key],type:key});
		$("#type").html(html);
	}
	
	window.changeDataType = function(ele,key){
//		if(selectDataType == key){
//			return;
//		}
		$("#type").find("a.type_on").removeClass("type_on");
		$(ele).addClass("type_on");
		selectDataType = key;
		//window.frames['sport_content_iframe'].location.href = base+'/data/ft_main?dataType=' + selectDataType; 
		document.getElementById("sport_content_iframe").contentWindow.location.href = base+'/sports/hg/goPage.do?dataType=' + selectDataType; 
	}
	
	window.showResult = function(){
		var types = selectDataType.split("_");
		var type = types[1];
		selectDataType = "result_"+type
		$("#type").find("a.type_on").removeClass("type_on");
		$("#type").find(".result a").removeClass("type_out").addClass("type_on");
		document.getElementById("sport_content_iframe").contentWindow.location.href= base + "/sports/hg/result.do?sportType="+type;
	}
	
})();




var $betRequest = function(opt){
	var fn ={
		success:opt.success,
		error:opt.error
	} 
	
	opt.success = function(data, textStatus,xhr){
    	var ceipstate = xhr.getResponseHeader("ceipstate")
    	if(ceipstate == 1){//正常响应
    		fn.success(data, textStatus,xhr);  
    	}else if(ceipstate == 2){//后台异常
    		showSportMsg("后台异常，请联系管理员!");
    	}else if(ceipstate == 3){ //业务异常
    		showSportMsg(data.msg);
    	}else if(ceipstate == 4){//未登陆异常
    		showSportMsg("用户未登陆，请先登陆");
    	}else if(ceipstate == 5){//没有权限
    		showSportMsg("权限不足");
    	}else{
    		fn.success(data, textStatus,xhr);  
    	}
	};
	opt.error = function(XMLHttpRequest, textStatus, errorThrown){  
    	var statusCode = XMLHttpRequest.status;
    	if(fn.error){
    		fn.error(XMLHttpRequest, textStatus, errorThrown);  
    		return;
    	}
    	alert("网络异常");
    }
	$post(opt);
};



(function(){

	var gameType = null;
	var betItems = [];//array
	var betMap = {}; //object
	var gidKey = "gid";
	
	var clearData = function(){
		betItems = [];
		betMap = {};
	}
	
	window.showSportMsg = function(msg) {
		clearData();
		var msg = '<img src="'
				+ base
				+ '/common/template/sports/hg/images/sports/order_none.jpg" width="216" height="22">'
				+ '<div class="ord" style="height:62px;padding-top:5px;">'
				+ '<div class="betBox">'
				+ '<span>'+msg+'</span><br/>'
				+ '<input style="margin-top:5px;" type="button" name="SUBMIT" value="确定" onclick="cancelBet()" class="yes">'
				+ '</div>' + '</div>';
		$("#bet_div").html(msg);
		stopTimer();
	};
	
	window.addBetItem = function(betInfo,gameData,viewData){
		var isMix = viewData.isMix;
		var gt = gameData.gameType;
		var gid = null;
		if(gt.indexOf("BK") == 0){//篮球
			gid = betInfo.mid;
			gidKey = "mid"
		}else{
			gid = betInfo.gid;
		}
		betInfo.deleteId = gid; //用于界面删除注单使用
		if(gt != gameType || isMix !== true){//非混合过关或者投注大类发生变化  重置数据  
			betItems = [];
			betMap = {};
		}
		if(isMix && betMap[gid]){//已经存在这场赛事
			if(betInfo.type == betMap[gid].type){ //点两次 就触发取消
				delBetItem(gid);
				return;
			}
			
			for(var i = 0;i <betItems.length;i++ ){
				if(betItems[i][gidKey] == gid){//replace
					betInfo.replace = true;
					betItems[i] = betInfo;
					showBetWin(viewData);
					betMap[gid] = betInfo;
					return;
				}
			}
		}
		betItems.push(betInfo);
		betMap[gid] = betInfo;
		gameType = gt;
		//展示投注界面
		showBetWin(viewData);
	}

	window.getBetItems = function(){
		return betItems;
	}

	window.getBetMap = function(){
		return betMap;
	}

	window.delBetItem = function(gid){
		var delItem = betMap[gid];
		$("#del" + gid).remove();
		if(!delItem){
			return;
		}
		delete betMap[gid];
		if(!betItems || betItems.length == 0){
			return;
		}
		var arr = [];
		for(var i = 0;i <betItems.length;i++ ){
			if(betItems[i][gidKey] != gid){
				arr.push(betItems[i]);
			}
		}
		if(arr.length == 0){
			cancelBet();
			return;
		}
		betItems = arr;
		refreshBetWinScroll();
	}

	window.showBetWin = function(viewData){
		//..
		showOrder();
		viewData.items = betItems;
		viewData.autoRefresh = auto_refresh;
		var html = template("BET_WIN", viewData);
		$("#bet_div").html(html);
		$("#bet_div").css("display","");
		$("#info_div").css("display","none");
		$("#pls_bet").css("display","none");
		$("#recentDiv").css("display","none");
	
		refreshOdds();
		stopTimer();
		if(auto_refresh){
			openTimer();
		}
		
		$("#acceptAutoFresh").bind("click",function(e){
			  e.stopPropagation();
			  startAutoFresh();
		});
		refreshBetWinScroll();
	}



	window.cancelBet = function(){//取消投注
		stopTimer();
		clearData();
		$("#bet_div").html("");
		$("#bet_div").css("display","none");
		$("#info_div").css("display","");
		$("#pls_bet").css("display","");
		refreshBetWinScroll();
	}


	var needKeys = ["gid","mid","odds","type","project","scoreH","scoreC"];

	function getNeedData(){
		var data = {
			plate:"H",
			gameType:gameType
		};
		var bis = [];
		for(var i=0;i<betItems.length;i++){
			var item = betItems[i];
			var bi = {};
			for(var j=0;j<needKeys.length;j++){
				if(item[needKeys[j]] !== undefined){
					bi[needKeys[j]] = item[needKeys[j]];
				}
			}
			bis.push(bi);
		}
		data["items"] = bis;
		return data;
	}

	function getMatchCache(gid){
		for(var i=0;i<betItems.length;i++){
			var bi = betItems[i];
			if(bi.gid == gid){
				return bi;
			}
		}
		return null;
	}

	window.submitOrder = function(){
		var data = getNeedData();
		var betMoney = $("#betMoney").val();
		if(!betMoney){
			alert("请输入有效投注金额");
			$("#betMoney").focus();
			return;
		}
		
		if(!checkBetMoney(betMoney)){
			return;
		}
		
		if(!confirm("是否确认要下注?")){
			return;
		}
		betMoney = ~~betMoney;
		var money = betMoney;
		var minMoney = parseInt($("#minMoney").text());
		if(money < minMoney){
			alert("单注最低金额不能少于"+minMoney);
			return;
		}
		data.money = money;
		
		$accept = $("#autoAccept");
		if($accept.length > 0 && $accept[0].checked ){
			data.acceptBestOdds = true;
		}
		
		var json = JSON.encode(data)
		$betRequest({
			url : base + "/sports/hg/bet/bet.do",
			data : {
				data : json
			},
			success:function(data){
				if(data.success){
					stopTimer();
					clearData();
					var betDiv = $("#bet_div");
					betDiv.find(".betdata").remove();
					betDiv.find(".auto").remove();
					$("#autoFreshTime").remove();
					$("#matchType").remove();
					betDiv.find(".betBox").html('<input type="button" name="PRINT" value="列印" onclick="printData()" class="print"><input type="button" name="FINISH" value="关闭" onclick="cancelBet()" class="close">');
					var successHtml = '<div class="fin_title" style="margin-bottom:2px;">' +
						'<p class="fin_acc">成功提交注单！</p><p class="fin_uid">注单号：'+data.code+'</p>' +
					'</div>';
					var mainDiv = betDiv.find(".main");
					
					mainDiv.prepend(successHtml);	
					mainDiv.append('<p class="fin_amount">交易金额：<span class="fin_gold">'+money+'</span></p>');
					betDiv.find(".betBox").addClass("noprint");//不打印按钮
					updateMemberInfo();//更新余额
				}else{//赔率变动
					var gid = data.gid;
					var cacheData = getMatchCache(gid);
					cacheData["odds"] = data.newOdds;
					var oddsDiv = $("#"+gid+"betodds");
					oddsDiv.html(data.newOdds);
					oddsDiv.css("background-color","yellow");
					alert("赔率变动");
				}
			}
		});
	};

	var refresh_timecount = 10;
	var refresh_timer = null;
//	var refreshing = false;//请求刷新中
	var auto_refresh = true;//自动刷新

	window.freshNewOdds = function(){
		refreshOdds();
	}

	window.startAutoFresh = function(){
	 	var checked = $("#acceptAutoFresh")[0].checked;
	 	if(checked){
	 		auto_refresh = true;
	 		openTimer();
	 	}else{
	 		auto_refresh = false;
	 		stopTimer();
	 		refresh_timecount = 10;
			$("#autoFreshTime").html(refresh_timecount);
	 	}
	}
	
	function doTimer(){
		if(!refresh_timer){
			return;
		}
		countRefreshTime();
	}
	
	function openTimer(){
		if(refresh_timer){
			return;
		}
		refresh_timecount = 10;
		$("#autoFreshTime").html(refresh_timecount);
		refresh_timer = setTimeout(doTimer,1000);
	}

	function stopTimer(){
		if(refresh_timer == null){
			return;
		}
		clearTimeout(refresh_timer);
		refresh_timer = null;
	}

	function refreshOdds(callbackConfig){
		var data = getNeedData();
		if(data.items.length == 0){
			return;
		}
		var json = JSON.encode(data);
		$post({
			url : base + "/sports/hg/bet/getOdds.do",
			data: {
				data : json
			},
			success:function(data, textStatus,xhr){
		    	var ceipstate = xhr.getResponseHeader("ceipstate")
		    	if(ceipstate == 4){//未登陆异常
		    		showSportMsg("用户未登陆，请先登陆");
		    		stopTimer();
		    		return;
		    	}
		    	if(ceipstate == 1){
		    		//更新赔率
		    		var arr = data.odds;
		    		for(var i= 0;i < arr.length; i++){
		    			var bi = arr[i];
						var gid = bi.gid;
						var cacheData = getMatchCache(gid);
						if(!cacheData){
							continue;
						}
						var oddsDiv = $("#"+gid+"betodds");
						if(cacheData["odds"] == bi.odds){
							oddsDiv.css("background-color","transparent");
						}else{
							oddsDiv.css("background-color","yellow");
							oddsDiv.html(bi.odds);
							cacheData["odds"] = bi.odds;
						}
		    		}
		    	}else{
		    		showSportMsg(data.msg);
		    		stopTimer();
		    		return;
		    	}
			},
			complete:function(){
				if(callbackConfig && callbackConfig["complete"]){
					callbackConfig["complete"]();
				}else{
					refresh_timecount = 10;
					$("#autoFreshTime").html(refresh_timecount);
				}
			}
		});
	}
	
	function countRefreshTime(){
		if(refresh_timecount <= 0){
			stopTimer();
			refreshOdds({
				complete:function(){
					if(betItems.length > 0){
						openTimer();
					}
				}
			});
			return;
		}
		--refresh_timecount;
		$("#autoFreshTime").html(refresh_timecount);
		refresh_timer = setTimeout(doTimer,1000);
		//doTimer();
	}
	
	function checkBetMoney(money) { 
	    var re = /^\d+(?=\.{0,1}\d+$|$)/ 
        if (!re.test(money)) { 
            alert("请输入有效投注");
            return false;
        }
	    return true;
	} 
	
	window.calWinMoney = function(inputEl){
		var val = $.trim(inputEl.value);
		if(val.length == 0){
			$("#winMoney").html("0");
			return;
		}
		
		if(betItems.length == 0){
			$("#winMoney").html("0");
			return;
		}
		var gameKey = new GameKey(gameType);
		var total = parseInt(val);
		var betMoney = total;
		for(var i = 0;i < betItems.length;i++){
			var bi = betItems[i];
			var odds = bi.odds * 1;
			var itemKey = new BetItemKey(bi.type); 
			if(gameKey.isMain() &&  (itemKey.isLetBall() || itemKey.isBigSmall())){
				odds += 1;
			}
			total = accMul(total,odds);
		}
		
		var result = HG.printf(total - betMoney,2);
		$("#winMoney").html(result);
		//return result;
	}
	
	function refreshBetWinScroll(){
		if(betItems.length > 6){
			$("#left_content").css("overflow-y","scroll");
		}else{
			$("#left_content").css("overflow-y","hidden");
		}
	}
	
})();

function accMul(arg1, arg2) {
	var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
	try {
		m += s1.split(".")[1].length
	} catch (e) {
		
	}
	try {
		m += s2.split(".")[1].length
	} catch (e) {
		
	}
	return Number(s1.replace(".", "")) * Number(s2.replace(".", ""))/ Math.pow(10, m)
}

function RepNumber(obj) {
	var reg = /^[\d]+$/g;
	if (!reg.test(obj.value)) {
		var txt = obj.value;
		txt.replace(/[^0-9]+/, function(char, index, val) {// 匹配第一次非数字字符
			obj.value = val.replace(/\D/g, "");// 将非数字字符替换成""
			var rtextRange = null;
			if (obj.setSelectionRange) {
				obj.setSelectionRange(index, index);
			} else {// 支持ie
				rtextRange = obj.createTextRange();
				rtextRange.moveStart('character', index);
				rtextRange.collapse(true);
				rtextRange.select();
			}
		})
	}
	calWinMoney(obj);
}

function updateMemberInfo(){
	$.ajax({
		url : base + "/meminfo.do",
		success:function(data, textStatus,xhr){
			if(data.login){
				$("#credit").html("人民币&nbsp;&nbsp;" + data.money);
				$("#sport_account").html("您好,"+data.account);
				window.isLogin = true;
			}else{
				$("#credit").html("未登录");
				$("#sport_account").html("您好,游客");
				window.isLogin = false;
			}
		}
	});
}

var printData = function(){
	window.print();
} 

$(function(){
	$("#game_type_td").click();
	updateMemberInfo();
	startTimeClock();
});

function startTimeClock() {
	var day = "";
	var month = "";
	var ampm = "";
	var ampmhour = "";
	var year = "";
	var myHours = "";
	var myMinutes = "";
	var mySeconds = "";
	var mydate = new Date(USA_TIME);
	USA_TIME = USA_TIME + 1000;
	
	mymonth = parseInt(mydate.getMonth() + 1) < 10 ? "0" + (mydate.getMonth() + 1) : mydate.getMonth() + 1;
	myday = mydate.getDate();
	if(myday < 10){
		myday = "0" + myday;
	}
	
	myyear = mydate.getYear();
	myHours = mydate.getHours();
	if(myHours < 10){
		myHours = "0" + myHours;
	}
	myMinutes = mydate.getMinutes();
	if(myMinutes < 10){
		myMinutes = "0" + myMinutes;
	}
	mySeconds = parseInt(mydate.getSeconds()) < 10 ? "0" + mydate.getSeconds(): mydate.getSeconds();
	year = (myyear > 200) ? myyear : 1900 + myyear;
	$("#sport_clock").html("(美东)" + year + "年" + mymonth + "月" + myday + "日&nbsp;" + myHours + ":" + myMinutes + ":" + mySeconds);
	setTimeout("startTimeClock()", 1000);
}

var refreshGameCount = function (gameCount){
	if(!gameCount){
		gameCount = window._gameCount;
	}else{
		window._gameCount = gameCount;
	}
	
	if(!gameCount){
		return;
	}
	
	for(var key in gameCount){
		var gc = gameCount[key] ? gameCount[key] : 0;
		$("#COUNT_"+key).html(gc);
	}
}

function showRecentOrder(){
	var orderBtn = $("#record_button");
	if(orderBtn.hasClass("ord_on")){
		return;
	}
	cancelBet();
	$("#info_div").css("display","none");
	$("#recentDiv").css("display","");
	$("#pls_bet").css("display","none");
	orderBtn.removeClass("record_btn").addClass("ord_on");
	$("#order_button").removeClass("ord_on").addClass("record_btn");
	if(isLogin){
		searchRecentOrder();
	}
}

function showOrder(){
	var recordBtn = $("#order_button");
	if(recordBtn.hasClass("ord_on")){
		return;
	}
	$("#info_div").css("display","");
	$("#bet_div").css("display","none");
	$("#recentDiv").css("display","none");
	$("#pls_bet").css("display","");
	recordBtn.removeClass("record_btn").addClass("ord_on");
	$("#record_button").removeClass("ord_on").addClass("record_btn");

}
//帐户历史
function targetHistoryDay(){
	if(!isLogin){
		alert("请先登录账号");
		return;
	}
	document.getElementById("sport_content_iframe").contentWindow.location.href = base +"/sports/hg/goHistoryTotalPage.do";
}
//交易状况
function targetRecent(){
	if(!isLogin){
		alert("请先登录账号");
		return;
	}
	document.getElementById("sport_content_iframe").contentWindow.location.href = base +"/sports/hg/goTodayOrdersPage.do";
}

function searchRecentOrder(){
	$.ajax({
		url : base + "/sports/hg/getTodayTenOrders.do",
		success:function(data, textStatus,xhr){
			template.config("escape", false);
			var html = template("tenOrderTpl",{orders:formatOrdersData(data.data)})
			$("#recentDiv").html(html);
		}
	});
}

function formatOrdersData(list){
	var result = [];
	for(var i=0;i<list.length;i++){
		var order = list[i];
		var item = JSON.decode(order.remark);
		var con = item.con;
		if(con.indexOf("vs") == -1){
			con = '<span class="text-danger">'+ con +'</span>';
		}else{
			con = '<font class="his_vs">vs </font>';
		}
		var homeFirst = order.homeTeam  == item.firstTeam;//主队是否在前
		var scoreStr = "";
		
		if(order.gameTimeType == 1){
			if(homeFirst){
				scoreStr = "&nbsp;<font color='red'><b>(" + order.scoreH +":" + order.scoreC + ")</b></font>";
			}else{
				scoreStr = "&nbsp;<font color='red'><b>(" + order.scoreC +":" + order.scoreH + ")</b></font>";
			}
		}	
		
		if(item.half === true && order.mix == 2){
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}
		
		result.push({
			odds:item.odds,
			con:con,
			scoreStr:scoreStr,
			firstTeam:item.firstTeam,
			lastTeam:item.lastTeam,
			result:item.result,
			typeNames:order.typeNames,
			bettingMoney:order.bettingMoney
		});
	}
	return result;
}

function showMoreNotice(){
	window.open(base+"/sports/notice.do",'sport_notice', 'width=850px,height=900px,scrollbars=yes,location=1,resizable=no');	
	//window.showModalDialog(base+"/sports/notice.do",window,"location:No;status:No;help:No;dialogWidth:600px;dialogHeight:900px;scroll:true;");   
}
