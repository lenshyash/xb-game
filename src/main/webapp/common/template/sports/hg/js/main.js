
var global = {
	iorpoints : 2,//赔率保留小数点后的个数	
	show_ior :100,
	myLeg:null, //联赛对应所有tr  每次刷新数据重新赋值
	gameMap:null,//赛事数据缓存 每次刷新数据重新赋值
	preGameMap:null,//上一次数据
	hiddenLeg:{},//玩家手动隐藏的数据  切换页码就重新生成
	sport_factory_map :{},
	gameType:null
}
//获取工厂
function getSportFactory(gameType){
	var gt = gameType ? gameType : global.gameType;
	if(!gt){
		return null;
	}
	var fac = global.sport_factory_map[gt];
	if(fac){
		return fac;
	}
	if(gt.indexOf('FT') == 0){
		fac = new Football(gt);
	}else if(gt.indexOf('BK') == 0){
		fac = new Basketball(gt);
	}
	if(fac){
		global.sport_factory_map[gt] = fac;
	}
	return fac;
}


function showLeg(leg){
	var myLeg = global.myLeg;
	var hiddenLeg = global.hiddenLeg;
	state = "LegClose";
	display = "none";
	if(hiddenLeg[leg] === true){
		var state = "LegOpen";
		var display = "";
	}
	for (var i=0;i<myLeg[leg].length;i++){
		showLegIcon(leg,state,myLeg[leg][i],display);
	}
	
	if(hiddenLeg[leg] === true){
		delete hiddenLeg[leg];
	}else{
		hiddenLeg[leg] = true;
	}
}

function showLegIcon(leg,state,gnumH,display){
	var  ary = document.getElementsByName(leg);
	for (var j=0;j<ary.length;j++){
		ary[j].innerHTML="<span id='"+state+"'></span>";
	}
	
	try{
		document.getElementById("TR3_"+gnumH).style.display=display;
	}catch(E){}
	try{
		document.getElementById("TR2_"+gnumH).style.display=display;
	}catch(E){}
	try{
		document.getElementById("TR1_"+gnumH).style.display=display;
	}catch(E){}
	try{
		document.getElementById("TR_"+gnumH).style.display=display;
	}catch(E){}
}

function toMapArr(headers,games,gameType){
	var arr = [];
	if(global.gameMap){//保存上一次数据
		global.preGameMap = global.gameMap;
	}
	global.gameMap = {};
	global.myLeg = {};
	var loveMap = getLoveMap(gameType); //收藏的赛事
	var sportFac = getSportFactory(gameType);//体育工厂			
	for (var i = 0; i < games.length; i++) {
		var map = {
			gameType:gameType 
		};
		for (var j = 0; j < headers.length; j++) {
			var key = headers[j];
			map[key] = games[i][j];
		}
		var leg = map["league"];
		map["hidden"] = global.hiddenLeg[leg] === true;
		var gid = map["gid"];
		if(leg !== undefined){
			var legArr = global.myLeg[leg];
			if(!legArr){
				legArr = [];
				global.myLeg[leg] = legArr;
			}
			legArr.push(gid);
		}
		var favKey = map[sportView["favGameIdKey"]];
		map["islove"] = (loveMap[favKey] === true);
		//formatOdds(map,gameType);
		sportFac.formatOdds(map);//转换赔率
		global.gameMap[gid] = map;
		arr.push(map);
	}
	return arr;
}

template.helper('$formatDate', function (gameType,rowData) {
	var sportFac = getSportFactory(gameType);//体育工厂	
	var str = sportFac.showGameTime(rowData);
	return str;
});


template.helper('$letBall', function (project,isHome) {
	var isHomeStrong = project.indexOf("-") == -1;
	if(isHome){
		if(isHomeStrong){
			return project;
		}
		return "";
	}else{
		if(isHomeStrong){
			return "";
		}else{
			return project.substring(1);
		}
	}
	return str;
});

template.helper('$appendBefore', function (content,appendStr) {
	if(content != ""){
		return appendStr + content;
	}
	return content;
});

template.helper('$teamName', function (teamName) {
	return teamName.replace("[中]","<font color=\"#005aff\">[中]</font>");
});


function toText(html){
	if(html.indexOf("<") == -1){
		return html;
	}
	var div = document.createElement("div");
	div.innerHTML = html;
	return $(div).text();
}

function getLoveMap(gameType){
	var gids = CookieUtil.getCookie(gameType);
	var map = {};
	if(!gids){
		return map;
	}
	var arr = gids.split(",");
	for(var i=0;i<arr.length;i++){
		map[arr[i]] = true;
	}
	return map;
};

template.helper('$showOdds', function (odds,oddsType,gid,project) {
	if(!odds){
		//console.info(odds);
		return "";
	}

	var content = "";
	var title = "";
	var keepData = global.gameMap[gid];
	
	switch(oddsType){
		case "ior_EOO":
			content = "单";
			title = "单";
			break;
		case "ior_EOE":
			content = "双";
			title = "双";
			break;
		case "ior_MH":
		case "ior_HMH":
		case "ior_RH":
		case "ior_HRH":
		case "MS_IOR_ROUH":
			title = toText(keepData["home"]);
			break;
		case "ior_MC":
		case "ior_HMC":
		case "ior_RC":
		case "ior_HRC":
		case "MS_IOR_ROUC":
			title = toText(keepData["guest"]);
			break;
		case "ior_HMN":
		case "ior_MN":
			title = "和局";
			break;
		case "ior_HOUH":
		case "ior_OUH":
		case "ior_OUHO":
		case "ior_OUCO":
		case "MS_IOR_ROUH":
			title = "大";
			break;
		case "ior_HOUC":
		case "ior_OUC":
		case "ior_OUHU":
		case "MS_IOR_ROUC":
			title = "小";
			break;
	}
	var css = chekOddsChange(odds,oddsType,gid); 
	var oddsStr = odds;
	if(sportView.gameType.indexOf("_MN") > -1 || sportView.gameType.indexOf("_MX") > -1){
		oddsStr = HG.printf(odds,global.iorpoints);//保留两位小数
	}
	return content + '<a href="javascript:void(0);" title="'+title+'" onclick=bet('+gid+',"'+oddsType+'","'+project+'",'+odds+');><font '+css+'>' +oddsStr+ '</font></a>';
});


function chekOddsChange (odds,oddsType,gid) {
	if(!odds || ! global.preGameMap){
		return "";
	}
	var keepData = global.preGameMap[gid];
	if(!keepData){
		return "";
	}
	var oldOdds = keepData[oddsType];
	if(oldOdds && oldOdds != odds){
		return '  style="background-color : yellow" ';
	}
	return "";
};

function mouseEnter_pointer(gid){
	$("#love_tag_"+gid).css("display","");
}

function mouseOut_pointer(gid){
	$("#love_tag_"+gid).css("display","none");
}

function addShowLoveI(gid,gameType){
	var gids = CookieUtil.getCookie(gameType);
	var arr = null;
	if(gids){
		arr = gids.split(",");
		var exists = false;
		for(var i=0;i<arr.length;i++){
			if(arr[i] == gid){
				exists = true;
				break;
			}
		}
		if(!exists){
			arr.push(gid);
		}
	}else{
		arr = [gid];
	}
	CookieUtil.addCookie(gameType,arr.join(","),1);
	$("#sp_" + gid).html("<div class=\"fov_icon_on\" style=\"cursor:hand\" title=\"移除\" onClick=\"chkDelshowLoveI('"+gid+"','"+gameType+"');\"></div>");
	showLoveStatus(gameType);
}



function chkDelshowLoveI(gid,gameType){
	var gids = CookieUtil.getCookie(gameType);
	if(!gids){
		return;
	}
	var arr = gids.split(",");
	var result = [];
	for(var i=0;i<arr.length;i++){
		if(arr[i] != gid){
			result.push(arr[i]);
		}
	}
	CookieUtil.addCookie(gameType,result.join(","),1);
	$("#sp_" + gid).html('<div id="love_tag_'+gid+'" class="fov_icon_out" style="cursor: pointer; display: none;" title="我的最爱" onclick="addShowLoveI('+gid+',\''+gameType+'\');"></div>');
	showLoveStatus(gameType);
}

var LOVE_STATUS = 1; // 1:无选中喜爱赛事  2:已选中  3：只显示喜爱赛事 

var icons = ["fav_num","showNull","showAll","showMy"];

function showLoveStatus(gameType){
	///var gt = global.gameType;
	var gids = CookieUtil.getCookie(gameType);
	var size = 0;
	if(gids){
		var arr = gids.split(",");
		size = arr.length;
	}
	if(LOVE_STATUS == 3){
		sportView.refresh();
	}
	if(size == 0){
		changeIconsStatus(["showNull"],true);
		LOVE_STATUS = 1;
	}else{
		if(LOVE_STATUS == 1){
			changeIconsStatus(["fav_num","showMy"],true);
			LOVE_STATUS = 2;
		}
		$("#live_num").html(size);
	}
}

function changeIconsStatus(arr,hidden){
	for(var i=0;i<icons.length;i++){
		var iconID = icons[i];
		var flag = false;
		for (var j = 0; j < arr.length; j++) {
			if(arr[j] == iconID){
				flag = true;
				$("#" + arr[j]).css("display",(!hidden)?"none" : "");//相反操作
				break;
			}
		}
		if(flag == false){
			$("#" + iconID).css("display",hidden?"none":"");
		}
	}
}

function showAllMatch(gameType){
	
	var gids = CookieUtil.getCookie(gameType);
	var size = 0;
	if(gids){
		var arr = gids.split(",");
		size = arr.length;
	}
	$("#showAll").css("display","none");
	if(size == 0){
		LOVE_STATUS = 1;
		$("#showNull").css("display","");
		$("#fav_num").css("display","none");
	}else{
		LOVE_STATUS = 2;
		$("#showMy").css("display","");
		$("#fav_num").css("display","");
	}
	sportView.refresh();
}
/**
 * 显示已收藏赛事
 * @param gameType
 */
function showLoveMatch(){
	$("#showMy").css("display","none");
	$("#showAll").css("display","");
	LOVE_STATUS = 3;
	$("#" + this.pageSelectorId).val("1");
	sportView.refresh();
}

function chkDelAllShowLoveI(gameType){
	CookieUtil.delCookie(gameType);
	$("#showNull").css("display","");
	$("#live_num").html("0");
	$("#fav_num").css("display","none");
	$("#showMy").css("display","none");
	$("#showAll").css("display","none");
	LOVE_STATUS = 1;
	sportView.refresh();
}

function pdChange(ele){
	window.location.href = base+'/sports/hg/goPage.do?dataType=' + $(ele).val(); 
}
var maxtime;
var oldgid;
var timeoutflag;
var oldsType;
$(function(){
	maxtime = 1;//设置至少10才能发送一次请求 
	oldgid = 0;
	timeoutflag = null;
	oldsType = "";
	
});
function bet(gid,oddsType,project,odds){
	if(oldgid==gid && oldsType == oddsType){
		if(maxtime>0){ 
			alert("请勿重复提交");
			--maxtime;
		}else{
			oldgid = 0;
			oldsType = "";
		}
	}else{
		var gameData = global.gameMap[gid];
		//console.info(gameData);
		var fac = getSportFactory(gameData.gameType);
		var data = fac.getBetInfoData(gameData,oddsType);
		if(!data){
			alert("数据异常");
			return;
		}
		if(typeof data == "string"){
			alert(data);
			return;
		}
		var isMix = fac.isMix();
		var viewData = {
			sport:fac.getName(),	
			isMix:isMix
		};
		 window.parent.addBetItem(data,gameData,viewData);//此处是一个会请求远程的ajax 异步操作;
		 maxtime = 1;//设置至少10才能发送一次请求 
		 oldgid = gid; 
		 oldsType = oddsType; 
	}
}
/*
function bet(gid,oddsType,project,odds){
	var gameData = global.gameMap[gid];
	//console.info(gameData);
	var fac = getSportFactory(gameData.gameType);
	var data = fac.getBetInfoData(gameData,oddsType);
	if(!data){
		alert("数据异常");
		return;
	}
	if(typeof data == "string"){
		alert(data);
		return;
	}
	var isMix = fac.isMix();
	var viewData = {
		sport:fac.getName(),	
		isMix:isMix
	};
	window.parent.addBetItem(data,gameData,viewData);//调用父页面addBetItem
	//alert(gid + " _ " + oddsType + " _ " + project + " _ " + odds);		
}*/


