	//获取工厂
	function getSportFactory(gameType){
		if(!window.sport_factory_map){
			window.sport_factory_map = {};
		}
		
		var gt = gameType ? gameType : global.gameType;
		if(!gt){
			return null;
		}
		var fac = sport_factory_map[gt];
		if(fac){
			return fac;
		}
		if(gt.indexOf('FT') == 0){
			fac = new Football(gt);
		}else if(gt.indexOf('BK') == 0){
			fac = new Basketball(gt);
		}
		if(fac){
			sport_factory_map[gt] = fac;
		}
		return fac;
	}


	var emptyFn = function(){}
	
	var extend = function(son,parent){
		for(var key in parent.prototype){
			if(son.prototype[key]){
				continue;
			}
			son.prototype[key] = parent.prototype[key];
		}
	}
	
	SportFactory = function(gameType){
		this.gameType = gameType;
		this.init();
	};
	
	SportFactory.prototype = {
		getRunballScoreStr:function(homeFirst,gameData){//获取滚球分数 html
			var homeScore = gameData["scoreH"];
			var guestScore =  gameData["scoreC"];
			if(homeFirst){
				return '<em id="liveGoals" class="bold">('+homeScore+':'+guestScore+')</em>';
			}
			return '<em id="liveGoals" class="bold">('+guestScore+':'+homeScore+')</em>';
		},
		formatOddsStr : function(desc,odds,gid){
			var betOddsId = gid + "betodds";
			return '<em>'+desc+'</em>&nbsp;@&nbsp;<strong class="light" id="'+betOddsId+'" style="background-color:transparent;">'+odds+'</strong>';
		},
		showGameTime:function(rowData){ //显示赛事时间
			if(this.isRunball()){//滚球
				return this.showRunballGameTime(rowData);
			}
			
			var time = rowData.openTime; 
			var live = rowData.live;
			if(!time){
				return "";
			}
			var date = new Date(time); 
			var hour = date.getHours(); //时，
			var min = date.getMinutes(); //分
			var str = "";
			if(!this.isToday() || this.isMix()){//非今日赛事显示月 日
				var month = date.getMonth() + 1;//月
				var day = date.getDate();//日
				str = (month > 9 ? month :"0"+month) + "-" + (day > 9 ? day :"0"+day);
			}
			str += "<br/>"+(hour > 9 ? hour :"0"+hour) + ":" + (min > 9 ? min :"0"+min);
			if(live){
				str += '<br/><font color="red">滚球</font>';
			}
			return str;
		},
		showMobileGameTime:function(rowData){//手机版开赛时间
			
			if(this.isRunball()){//滚球
				return this.showRunballGameTime(rowData,true);
			}
			
			var time = rowData.openTime; 
			var live = rowData.live;
			if(!time){
				return "";
			}
			var date = new Date(time); 
			var hour = date.getHours(); //时，
			var min = date.getMinutes(); //分
			var str = "";
			if(!this.isToday() || this.isMix()){//非今日赛事显示月 日
				var month = date.getMonth() + 1;//月
				var day = date.getDate();//日
				str = (month > 9 ? month :"0"+month) + "-" + (day > 9 ? day :"0"+day)+"&nbsp;";
			}
			str += (hour > 9 ? hour :"0"+hour) + ":" + (min > 9 ? min :"0"+min);
			if(this.isBallCount()){
				str += '<br/>';
			}else{
				str += '&nbsp;';
			}
			if(live){
				str += '<b class="text-red">滚球</b>';
			}
			return str;
		},
		showRunballGameTime:emptyFn,//子类实现
		formatOdds:emptyFn,//子类实现
		getOddsType:function(){ //获取盘口类别
			return "H";
		},
		getShowOddsHtml:function(){
			if(!odds){
				return "";
			}
			var content = "";
			var title = "";
			var keepData = global.gameMap[gid];
			switch(oddsType){
				case "ior_EOO":
					content = "单";
					title = "单";
					break;
				case "ior_EOE":
					content = "双";
					title = "双";
					break;
				case "ior_MH":
				case "ior_HMH":
				case "ior_RH":
				case "ior_HRH":
					title = toText(keepData["home"]);
					break;
				case "ior_MC":
				case "ior_HMC":
				case "ior_RC":
				case "ior_HRC":
					title = toText(keepData["guest"]);
					break;
				case "ior_HMN": 			
				case "ior_MN":
					title = "和局";
					break;	
				case "ior_HOUH":
				case "ior_OUH":
				case "ior_OUHO":
				case "ior_OUCO":
					title = "大";
					break;	
				case "ior_HOUC":
				case "ior_OUC":
				case "ior_OUHU":
				case "ior_OUCU":
					title = "小";
					break;	
			}
			var css = chekOddsChange(odds,oddsType,gid); 
			var oddsStr = odds;
			if(this.isMain() || this.isMix()){
				oddsStr = HG.printf(odds,global.iorpoints);//保留两位小数
			}
			return content + '<a href="javascript:void(0);" title="'+title+'" onclick=bet('+gid+',"'+oddsType+'","'+project+'",'+odds+');><font '+css+'>' +oddsStr+ '</font></a>';
		}
	}
	
	extend(SportFactory,GameKey);
	
	SportView = function(cfg){
		for(var key in cfg){
			this[key] = cfg[key];
		}
		this.init();
	}
	
	SportView.prototype = {
		id:null,//dom的id 渲染到对应的dom
		dataURL:base + "/sports/hg/getData.do",//请求数据的链接
		leagueURL : null,//获取联赛的连接
		title:'',
		getParams:emptyFn,//ajax参数回调
		formatViewData:emptyFn,
		tableTpl:null,//容器模板
		gameTpl:null,//模板ID
		gameType:null,//游戏类型
		tableWidth:714,
		
		autoRefreshTime:180,//自动刷新时间 单位秒
		showOddsTypeList:true,//显示盘口
		showRefresh:true,//显示刷新按钮
		showTimer:true,//显示倒计时自动刷新
		showLeagueFilter:true,//用于只看自己选中的联赛 赛事
		showSort:true,//显示排序下拉框
		showFav:true,//显示喜爱节目
		favGameIdKey:'gid',
		
		pageNo:1,//当前页码
		pageSize:1,//总页数
		oddsType:'H', // 默认香港盘口
		refreshBtnId:"refresh_btn",
		timerContextId:'refreshTime',
		pageTextId : 'pg_txt', //分页状态栏
		pageSelectorId : 'view_page_selector',
		leagueFilterBtnId : 'sel_league',
		oddsTypeSelectorId:'myoddType',
		sortSelectorId : 'SortSel',
		
		closeLegs:null,// private object格式  缩合起来的联赛
		showLegs:null,//private Map格式
		timer : null,//private 定时器
		dataRenderDomId : "showtable", //private string  赛事渲染的div
		init:function(){
			this.closeLegs = {};
			if(this.showLeagueFilter){
				this.showLegs = {};
			}
			this.initBaseView();
			this.bindEvent();
			this.refresh();
		},
		
		initBaseView:function(){
			if(window.global){
				window.global.gameType = this.gameType;
			}
			
			var data = {
				gameType:this.gameType,
				title:this.title,
				showOddsTypeList:this.showOddsTypeList,//显示盘口
				showRefresh:this.showRefresh,//显示刷新按钮
				showTimer:this.showTimer,//显示倒计时自动刷新
				showSort:this.showSort,
				showFav:this.showFav,
				oddsType:this.oddsType,
				width:this.tableWidth,
				showLeagueFilter:this.showLeagueFilter//用于只看自己选中的联赛 赛事
			};
			var fac = getSportFactory();
			if(fac.isTime()){
				var gts = this.gameType.split("_");
				var fulVal = gts[1] + '_' + gts[0] + '_TI';
				var halfVal = gts[1] + '_' + gts[0] + '_HTI';
				var isHalf=this.gameType.indexOf("_HTI")>0;
				data["timeData"] = [{
					name : "全场",
					value : fulVal ,
					select: !isHalf
				},{
					name : "上半场",
					value : halfVal,
					select: isHalf
				}];
			}
			
			//var now = new Date();	
			//data.dataRenderDomId = this.dataRenderDomId = this.id + "_" + now.getTime();
			
			if(this.showTimer){
				data.autoRefreshTime = this.autoRefreshTime;
			}
			
			if(this.showOddsTypeList){
				data.oddsTypes = this.getOddsTypes();
			}
			
			var html = template(this.tableTpl, data);
			$("#"+this.id).html(html);
			
			if(this.showFav){
				var gids = CookieUtil.getCookie(this.gameType);
				var size = 0;
				if(gids){
					var arr = gids.split(",");
					size = arr.length;
				}
				if(size > 0){
					$("#showNull").css("display","none");
					$("#showMy").css("display","");
					$("#live_num").html(size);
					$("#fav_num").css("display","");
				}
			}
		},
		initView:function(data){
			var tplData = data;
			if(this.formatViewData != emptyFn){
				tplData = this.formatViewData(data);
			}
			var html =  template(this.gameTpl, tplData);
			$("#"+this.dataRenderDomId).html(html);
		},
		stopTimer : function(){
			if(!this.timer || this.showTimer !== true){
				return;
			}
			clearInterval(this.timer);
			this.timer = null;
		},
		openTimer : function(){
			if(this.showTimer !== true){
				return;
			}
			var me = this;
			me["_autoRefreshTime"] = this.autoRefreshTime;
			
			var currentTimer = this.timer = setInterval(function(){
				if(me.timer == null){
					if(currentTimer != null){
						clearInterval(currentTimer);
					}
					return;
				} 
				if(me["_autoRefreshTime"] == 0 ){
					me.refresh.call(me);
				}
				var sec = --me["_autoRefreshTime"];
				$("#"+me.timerContextId).html(sec < 0 ? "0" : sec + "");
			},1000);
		},
		openFilterLeagueView:function(){
			alert("openFilterLeagueView");
		},
		bindEvent:function(){
			var me = this;
			//绑定刷新按钮事件
			if(this.refreshBtnId){
				$("#" + this.refreshBtnId).bind("click",function(){
					me.refresh.call(me);
				});
				$("#refresh_down").bind("click",function(){
					me.refresh.call(me);
				});
				
			}
			
			//绑定选中页码事件
			if(this.pageSelectorId){
				var pageSelector  = $("#" + this.pageSelectorId);
				pageSelector.bind("change",function(){
					me.goPage.call(me,pageSelector.val());
				});
			}
			
			//盘口改变
			if(this.showOddsTypeList){
				var oddsSelector  = $("#" + this.oddsTypeSelectorId);
				oddsSelector.bind("change",function(){
					me.changeOddsType.call(me,oddsSelector.val());
				});	
			}
			
			//选择只显示联赛
			if(this.showLeagueFilter){
				
				var $view = $("#legView");
				var $content = $view.find(".leg_mem");
				var filterBtns = $view.find(".filter_btn");
				$("#" + this.leagueFilterBtnId).bind("click",function(){
					$content.html("<div style='height:100px;line-height:100px;font-size:16px;color:red;' align='center'><b id='loading_info'>加载中...</b></div>");
					$view.css("display","");
					filterBtns.css("display","none");
					$.ajax({	
						url: base + "/sports/hg/getLeaues.do",
						data:{
							gameType : me.gameType
						},
						success:function(data){
							var max = 10;
							var length = data.leagues.length/ 3;
							if(length > max){
								if(length != parseInt(length)){
									length = parseInt(length) + 1;
								}
							}else{
								length = max;
							}
							filterBtns.css("display","");
							var arr = [];
							for(var i=0;i<length;i++){
								var start = i * 3;
								var end =  (i + 1) * 3;
								var legs = [];
								for(var j = start ;j< end ;j++){
									var legName = data.leagues[j];
									if(legName == undefined){
										legs.push(false);
									}else{
										legs.push({name:legName,checked:me.isCheckedLeg(legName)});
									}
								}
								arr.push(legs);
							}
							var html = template("TPL_LEGS",{trs:arr});
							$content.html(html);
							var $checkAll = $("#selectAll");
							$checkAll[0].checked = me.isSelectAllLeg();
							var $cs = $content.find("input[type=checkbox]");
							$cs.bind("click",function(){
								if(this.checked == false){
									$checkAll[0].checked = false;
								}else{
									$checkAll[0].checked = true;
									$cs.each(function(){
										if(this.checked == false){
											$checkAll[0].checked = false;
											return false; 
										}
									});
								}
							});
						},
						error:function(){
							$("#loading_info").html("网络错误,数据加载失败!");
						}
					});
				});
				
				$view.find(".close_btn").bind("click",function(){
					$view.css("display","none");
				});
				
				$view.find(".close_box").bind("click",function(){
					$view.css("display","none");
				});		
				
				filterBtns.bind("click",function(){
					var $cs = $content.find("input[type=checkbox]");
					var map = {};
					var count = 0;
					$cs.each(function(){
						if(this.checked){
							map[$(this).attr("leg")] = true;
							count++;
						}
					});
					if(count != $cs.length && count != 0){
						me.showLegs = map;
						$("#str_num").html(count);
						me.refresh();
					}else{
						me.showLegs = {};
						$("#str_num").html("全部");
						me.refresh();
					}
					$view.css("display","none");
				});
				
				$("#selectAll").bind("click",function(){
					var $cs = $content.find("input[type=checkbox]");
					var checkedCount = 0;
					$cs.each(function(){
						if(this.checked){
							++checkedCount;
						}
					});
					
					$cs.each(function(){
						if(checkedCount == $cs.length){
							this.checked = false;
						}else{
							this.checked = true;
						}
					});
				});
			}
			
			//数据排序方式
			if(this.showSort){
				var sortSelector  = $("#" + this.sortSelectorId);
				sortSelector.bind("change",function(){
					me.refresh.call(me);
				});	
			}
		},
		isSelectAllLeg:function(){
			var size = 0;
			for(var key in this.showLegs){
				++size;
			}
			if(size == 0){
				return true;
			}
			return false;
		},
		isCheckedLeg:function(leg){
			if(this.isSelectAllLeg()){
				return true;
			}
			return this.showLegs[leg] === true;
		},
		refresh:function(params){
			var p = params || {} 
			var pageSelector  = $("#" + this.pageSelectorId);
			var pageNo = ~~pageSelector.val();
			if(pageNo <= 0){
				pageNo = 1;
			}
			var arr = [];
			for(var key in this.showLegs){
				arr.push(key);
			}
			if(arr.length != 0){
				p.showLegs = JSON.encode(arr);
			}
			this.goPage(pageNo,p);
			this.pageNo = pageNo;
		},
		goPage:function(_pageNo,paramsObject){
			this.stopTimer();
			$("#"+ this.refreshBtnId).removeClass("refresh_btn");
			$("#"+ this.refreshBtnId).addClass("refresh_on");
			$("#refresh_down").removeClass("refresh_M_btn");
			$("#refresh_down").addClass("refresh_M_on");
			
			var params = paramsObject || {};
			params.pageNo = _pageNo;
			this.getParams(params);
			var me = this;
			$.ajax({
				data : params,
				url: this.dataURL,
				success:function(data){
					me.pageSize = data.pageCount;
					me.pageNo = _pageNo;
					me.initView(data);
					me._refreshPagingStatus(data);
					var fn = window.parent.refreshGameCount;
					if(fn){
						fn(data.gameCount);
					}
				},
				complete:function(data){
					$("#"+ me.refreshBtnId).removeClass("refresh_on");
					$("#"+ me.refreshBtnId).addClass("refresh_btn");
					$("#refresh_down").removeClass("refresh_M_on");
					$("#refresh_down").addClass("refresh_M_btn");
					me.openTimer();
				},
				error:function(){
					Msg.info("数据加载中...");
				}
			});
		},
		//改变盘口
		changeOddsType:function(oddsType){
			this.oddsType = oddsType;
			alert(oddsType);
		},
		changeSortType:function(sortType){
			alert("sortType:"+sortType);
		},
		getOddsTypes : function(){
			return [
	 	         ['H','香港盘'],
	 	         ['M','马来盘'],
	 	         ['I','印尼盘'],
	 	         ['E','欧洲盘']
	 		];
		},
		/**
		 * 刷新分页状态
		 * @param data
		 */
		_refreshPagingStatus : function(data){
			this._refreshPageSelector();
			//this.openTimer();
			$("#"+ this.pageTextId).html(this.pageNo+" / "+this.pageSize+" 页&nbsp;&nbsp;");
		},
		/**
		 * 刷新页码选择
		 */
		_refreshPageSelector:function(){
			var selector = $("#"+this.pageSelectorId);
			selector.html("");
			for(var i=0;i<this.pageSize;i++){
				var selectTag = "";
				if(i + 1 == this.pageNo){
					selectTag = " selected ";
				}
				selector.append("<option value='"+(i+1)+"' "+selectTag+">"+(i+1)+"</option>"); 
			}
		}
	}
	
	/**
	 * 皇冠帮助类
	 */
	var HG = {
		/*
		去正負號做小數第幾位捨去
		進來的值是小數值
		*/
		Decimal_point : function(tmpior,show){
			var sign="";
			sign =((tmpior < 0)?"Y":"N");
			tmpior = (Math.floor(Math.abs(tmpior) * show + 1 / show )) / show;
			return (tmpior * ((sign =="Y")? -1:1)) ;
		},	
		/**
		 * 轉換賠率
		 * @param odd_f
		 * @param H_ratio
		 * @param C_ratio
		 * @param showior
		 * @return
		 */
		chg_ior : function(odd_f,iorH,iorC){
			//console.log("1. "+odd_f+"<>"+iorH+"<>"+iorC+"<>"+showior);
			iorH = Math.floor((iorH*1000)+0.001) / 1000;
			iorC = Math.floor((iorC*1000)+0.001) / 1000;
			
			var ior=new Array();
			if(iorH < 11) iorH *=1000;
			if(iorC < 11) iorC *=1000;
			iorH=parseFloat(iorH);
			iorC=parseFloat(iorC);
			switch(odd_f){
			case "H":	//香港變盤(輸水盤)
				ior = this.get_HK_ior(iorH,iorC);
				break;
			case "M":	//馬來盤
				ior = this.get_MA_ior(iorH,iorC);
				break;
			case "I" :	//印尼盤
				ior = this.get_IND_ior(iorH,iorC);
				break;
			case "E":	//歐洲盤
				ior = this.get_EU_ior(iorH,iorC);
				break;
			default:	//香港盤
				ior[0]=iorH ;
				ior[1]=iorC ;
			}
			ior[0]/=1000;
			ior[1]/=1000;
			
			ior[0] = this.printf(this.Decimal_point(ior[0],global.show_ior),global.iorpoints);
			ior[1] = this.printf(this.Decimal_point(ior[1],global.show_ior),global.iorpoints);
			return ior;
		},
		
		/**
		 * 選擇多盤口時 轉換成該選擇賠率
		 * @param odd_type 	選擇盤口
		 * @param iorH		主賠率
		 * @param iorC		客賠率
		 * @param show		顯示位數
		 * @return		回傳陣列 0-->H  ,1-->C
		 */
		get_other_ioratio : function(odd_type, iorH, iorC){
			var out=new Array();
			if(iorH != "" && iorC != ""){
				out = this.chg_ior(odd_type,iorH,iorC);
			}else{
				out[0]=iorH;
				out[1]=iorC;
			}
			return out;
		},
		
		//香港输水盘	
		get_HK_ior : function (H_ratio,C_ratio){
			var out_ior=new Array();
			var line,lowRatio,nowRatio,highRatio;
			var nowType="";
			if (H_ratio <= 1000 && C_ratio <= 1000){
				out_ior[0]=Math.floor(H_ratio/10+0.0001)*10;;
				out_ior[1]=Math.floor(C_ratio/10+0.0001)*10;;
				return out_ior;
			}
			line=2000 - ( H_ratio + C_ratio );
			
			if (H_ratio > C_ratio){ 
				lowRatio=C_ratio;
				nowType = "C";
			}else{
				lowRatio = H_ratio;
				nowType = "H";
			}
			if (((2000 - line) - lowRatio) > 1000){
				//對盤馬來盤
				nowRatio = (lowRatio + line) * (-1);
			}else{
				//對盤香港盤
				nowRatio=(2000 - line) - lowRatio;	
			}
			
			if (nowRatio < 0){
				highRatio = Math.floor(Math.abs(1000 / nowRatio) * 1000) ;
			}else{
				highRatio = (2000 - line - nowRatio) ;
			}
			if (nowType == "H"){
				out_ior[0]=Math.floor(lowRatio/10+0.0001)*10;
				out_ior[1]=Math.floor(highRatio/10+0.0001)*10;
			}else{
				out_ior[0]=Math.floor(highRatio/10+0.0001)*10;
				out_ior[1]=Math.floor(lowRatio/10+0.0001)*10;
			}
			return out_ior;
		},
		/*
		  显示几位小数
		*/
		printf : function(vals,points){ //小數點位數
			vals=""+vals;
			var cmd=new Array();
			cmd=vals.split(".");
			if (cmd.length>1){
				var size = points-cmd[1].length;
				if(size > 0){//补0
					for (ii=0;ii<size;ii++)vals=vals+"0";
				}else if(points > 0){	//截取
					return   cmd[0]+ "." + cmd[1].substring(0,points);
				}
			}else{
				vals=vals+".";
				for (ii=0;ii<points;ii++)vals=vals+"0";
			}
			return vals;
		}
	};
 
	
	

	

