Football = function(gameType){
	this.gameType = gameType;
	this.init();
};

Football.prototype = {
	getName:function(){
		if(this.isFuture()){
			return "足球 - 早盘";
		}else if(this.isRunball()){
			return "足球 - 滚球";
		}
		return "足球";
	},
	showRunballGameTime:function(rowData,isMobile){
		var tmpset = '';
		var showretime = '';
		if(rowData.retimeset){
			tmpset = rowData.retimeset.split("^");
			tmpset[1] = tmpset[1].replace("<font style=background-color=red>","").replace("</font>","");
			showretime="";
			if(tmpset[0]=="Start"){//还没开赛
				showretime="-";
			}else if(tmpset[0]=="MTIME"){ //中场
				showretime = tmpset[1];
			}else{
				var tmpHtime = tmpset[0];
				var startChar = tmpHtime.charAt(0);
				if(startChar=="1"){
					tmpHtime= "上半场";
				}
				if(startChar == "2"){
					tmpHtime= "下半场";
				}
				showretime = tmpHtime+" <font class='rb_color'>"+tmpset[1]+"'</font>";
			}
		}

		/**
		 * 当前比赛最后入球的球队 
		 */
		var socreHtml = "";
		if(rowData.lastestscoreH != ''){ 
			socreHtml = "<font class=\"rb_score_time\" >"+rowData.scoreH+"</font>&nbsp;-&nbsp;<font class=\"rb_score\">"+rowData.scoreC+"</font>";
		}else if(rowData.lastestscoreC != ''){
			socreHtml = "<font class=\"rb_score\">" + rowData.scoreH+"</font>&nbsp;-&nbsp;<font class=\"rb_score_time\" >"+rowData.scoreC+"</font>";
		}else{
			socreHtml =  rowData.scoreH+"&nbsp;-&nbsp;"+rowData.scoreC;
		}
		if(isMobile){
			return showretime + "&nbsp;&nbsp;" + socreHtml;
		}
		var html = '<table class="rb_box_ft">' +
						'<tr><td class="rb_time">'+showretime+'</td></tr>'+
						'<tr><td class="rb_score">'+socreHtml+'</td></tr>' +
					'</table>';
		return html;
	},
	formatOdds:function(data){
		var odd_f_type = this.getOddsType();
		if(this.isMain()){
			var R_ior  = HG.get_other_ioratio(odd_f_type, data.ior_RH   , data.ior_RC );
			var OU_ior = HG.get_other_ioratio(odd_f_type, data.ior_OUH  , data.ior_OUC );
			var HR_ior = HG.get_other_ioratio(odd_f_type, data.ior_HRH  , data.ior_HRC );
			var HOU_ior= HG.get_other_ioratio(odd_f_type, data.ior_HOUH , data.ior_HOUC);
			
			if((data.ior_EOO != 0) && (data.ior_EOE != 0)){
				var EO_ior = HG.get_other_ioratio("H", data.ior_EOO*1-1 , data.ior_EOE*1-1);
				data.ior_EOO = HG.printf((EO_ior[0]*1000+1000)/ 1000,2) ; //赔率 + 1
				data.ior_EOE = HG.printf((EO_ior[1]*1000+1000)/ 1000,2); 
			}
			data.ior_RH=R_ior[0];
			data.ior_RC=R_ior[1];
			data.ior_OUH=OU_ior[0];
			data.ior_OUC=OU_ior[1];
			data.ior_HRH=HR_ior[0];
			data.ior_HRC=HR_ior[1];
			data.ior_HOUH=HOU_ior[0];
			data.ior_HOUC=HOU_ior[1];
		}
	},
	getBetInfoData:function(gameData,key){
		var odds = gameData[key];
		var gid = gameData.gid;
		if(!odds){
			return "投注项变动";
		}
		var data = {
			league:gameData.league,//联赛
			gid : gid,
			odds: odds,
			type : key
		};
		if(this.isRunball()){//放入动态比分
			data.scoreH = gameData.scoreH;
			data.scoreC = gameData.scoreC;
		}
		var h = gameData.home;
		var g = gameData.guest;
		var homeFirst = true;//主队显示在前面
		
		if(this.isMain() || this.isMix()){
			//全场独赢
			if(key == "ior_MC" || key == "ior_MH" || key == "ior_MN"){
				if(this.isRunball()){
					data.title = "全场 滚球 - 独赢";
				}else{
					data.title = "全场 - 独赢";
				}
				data.team = h + " vs. " + g;
				var win = "";
				if(key == "ior_MH"){
					win = h;
				}else if(key == "ior_MC"){
					win = g;
				}else{
					win = "和局";
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}
			
			//半场独赢
			if(key == "ior_HMC" || key == "ior_HMH" || key == "ior_HMN"){
				if(this.isRunball()){
					data.title = "半场 滚球 - 独赢";
				}else{
					data.title = "半场 - 独赢";
				}
				
				data.team = h + " vs. " + g;
				var win = "";
				if(key == "ior_HMH"){
					win = h;
				}else if(key == "ior_HMC"){
					win = g;
				}else{
					win = "和局";
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_RC" || key == "ior_RH"){//全场让球
				data.title = "全场 - 让球";
				var rb = gameData["CON_RH"];//让球
				data.project = rb;
				if(rb.indexOf("-") == 0){//客让主
					homeFirst = false;
					data.team = g+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb.substring(1)+'&nbsp;</span>' + h ;
				}else{
					data.team = h+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb+'&nbsp;</span>' + g ;
				}
				var win = "";
				if(key == "ior_RH"){
					win = h;
				}else if(key == "ior_RC"){
					win = g;
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_HRC" || key == "ior_HRH"){//半场让球
				data.title = "半场 - 让球";
				var rb = gameData["CON_HRH"];
				data.project = rb;
				if(rb.indexOf("-") == 0){//客让主
					homeFirst = false;
					data.team = g+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb.substring(1)+'&nbsp;</span>' + h ;
				}else{
					data.team = h+ '<span style="color:red;background-color: transparent;">&nbsp;'+rb+'&nbsp;</span>' + g ;
				}
				var win = "";
				if(key == "ior_HRH"){
					win = h;
				}else if(key == "ior_HRC"){
					win = g;
				}
				data.oddsDesc = this.formatOddsStr(win,odds,gid);
			}else if(key == "ior_OUH" || key == "ior_OUC"){//全场大小球
				data.team = h + " vs. " + g;
				data.title = "全场 - 大小";
				if(key == "ior_OUH"){//大
					data.project = gameData["CON_OUH"];
					data.oddsDesc = this.formatOddsStr("大 " + gameData["CON_OUH"],odds,gid);
				}else{
					data.project = gameData["CON_OUC"];
					data.oddsDesc = this.formatOddsStr("小 " + gameData["CON_OUH"],odds,gid);
				}
			}else if(key == "ior_HOUH" || key == "ior_HOUC"){//半场大小球
				data.team = h + " vs. " + g;
				data.title = "半场 - 大小";
				if(key == "ior_HOUH"){//大
					data.project = gameData["CON_HOUH"];
					data.oddsDesc = this.formatOddsStr("大&nbsp;" + gameData["CON_HOUH"],odds,gid);
				}else{
					data.project = gameData["CON_HOUC"];
					data.oddsDesc = this.formatOddsStr("小&nbsp;" + gameData["CON_HOUH"],odds,gid);
				}
			}else if(key == "ior_EOO" || key == "ior_EOE"){//全场单双
				data.team = h + " vs. " + g;
				data.title = "全场 - 单双";
				if(key == "ior_EOO"){//大
					data.oddsDesc = this.formatOddsStr("单",odds,gid);
				}else{
					data.oddsDesc = this.formatOddsStr("双",odds,gid);
				}
			}
		}else if(this.isTime()){ //波胆
			data.team = h + " vs. " + g;
			if(this.isFullTime()){//全场
				data.title = "全场 - 波胆";
			}else{//上半场
				data.title = "上半场 - 波胆";
			}
			var project = this.getTimeProjectName(key,this.isFullTime());
			data.oddsDesc = this.formatOddsStr(project,odds,gid);
		}else if(this.isBallCount()){
			data.team = h + " vs. " + g;
			data.title = "全场 - 总入球";
			var project = this.getBallCountProjectName(key);
			data.oddsDesc = this.formatOddsStr(project,odds,gid);
		}else if(this.isHalfAndFull()){
			data.team = h + " vs. " + g;
			data.title = "半场 / 全场";
			var project = this.getHalfAndFullProjectName(key,h,g);
			data.oddsDesc = this.formatOddsStr(project,odds,gid);
		}
		
		if(data.team !== undefined && this.isRunball()){
			data.team = data.team + "&nbsp;"+ this.getRunballScoreStr(homeFirst,gameData);
		}
		return data;
	},
	getTimeProjectName : function(key,isFull){//获取波胆 比分
		if(key == "ior_OVH"){
			return "其他比分";
		}
		//ior_H1C0
		//ior_H0C1
		return  key.charAt(5) + ":" + key.charAt(7);
	},
	getBallCountProjectName:function(key){
		if(key == "ior_OVER"){
			return "7或以上";
		}
		//ior_T01
		var len = key.length;
		return key[len-2] + "~" + key[len -1];
	},
	getHalfAndFullProjectName : function(key,home,guest){
		var map = {
			"H":home,
			"C":guest,
			"N":"和局"
		}
		var halfKey = key[key.length - 2];
		var fullKey = key[key.length - 1];
		return map[halfKey] + " / " + map[fullKey];
	}
}

//继承基本方法 必须放在 prototype 之后
extend(Football,SportFactory);
