ResultView = function(cfg){
	for(var key in cfg){
		this[key] = cfg[key];
	}
	this.init();
}

ResultView.prototype = {
	dataURL:base + "/sports/hg/getResult.do",//请求数据的链接	
	id:null,//dom的id 渲染到对应的dom
	title:'',
	tableTpl:null,//容器模板
	gameTpl:null,//模板ID
	tableWidth:714,
	dataRenderDomId : "showtable", 
	init:function(){
		this.initBaseView();
		this.bindEvent();
		this.refresh();
	},
	initBaseView:function(){
		this.dateStr = DateUtil.formatDate(this.now);
		var html = template(this.tableTpl, this);
		$("#"+this.id).html(html);
	},
	bindEvent:function(){
		var me = this;
		$("#searchBtn").bind("click",function(){
			me.refresh();
		});
	},
	refresh:function(){
		var me = this;
		$.ajax({	
			url: me.dataURL,
			data:{
				sportType : $("#selgtype").val(),
				date : $("#dateTime").val()
			},
			success:function(data){
				var html = template(me.gameTpl, data);
				$("#showtable").html(html);
			}
		});
	}
}
//是一个整数数字
function isNumber(score){
	if(score == undefined || score == null){
		return false;
	}
	var s = ~~score;
	if(s+"" == score){ 
		return true;
	}
	return false;
}

template.helper('$formatTime', function (time) {
	var date = new Date(time);
	return date.format("MM-dd <br> hh:mm");;
});

template.helper('$printClass', function (score) {
	if(!isNumber(score)){
		return "cal";
	}
	return "";
});

template.helper('$showAddTimeScore', function (score) {
	if(!isNumber(score)){
		return "-";
	}
	return score;
});
 
function openOrClose(leg,td){
	var $span = $(td).find("span[id=LegClose]");
	var isClosed = $span.length > 0;
	if(isClosed){
		$("tbody[name='" + leg +"']").css("display","");
		$("span[leg='"+leg+"']").attr("id","LegOpen");
	}else{
		$("tbody[name='" + leg +"']").css("display","none");
		$("span[leg='"+leg+"']").attr("id","LegClose");
	}
}