<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>皇冠体育</title>
	<link rel="stylesheet" href="${base}/common/template/sports/hg/css/orders/mem_body_his.css">
	<%@ include  file="/common/template/sports/hg/include/result/result_resource.jsp"%>
</head>
<body>
	<table border="0" cellpadding="0" cellspacing="0" id="box">
		<tbody>
			<tr>
				<td class="top">
					<h1>
						<em>交易状况</em>
					</h1>
				</td>
			</tr>
			<tr>
				<td class="mem">
					<h2>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="fav_bar">
							<tbody>
								<tr>
									<td id="pageNo" width="100px">
										&nbsp;&nbsp;<span id="pageStatus">1 / 1 页 </span>
										<select id="page" name="page" onchange="changePage()">
											<option selected="selected" value="1">1</option>
										</select>
									</td>

									<td class="his_refresh">
										<div onclick="refresh()">
											<font id="refreshTime"></font>
										</div>
									</td>
									<td>&nbsp;</td>

									<td class="right">
									<!-- 	<span onclick="" class="wag_btn" style="display: none;"> 
											您有<span class="wag_none"> (0) </span>张取消注单
										</span>  -->
										<span class="wag_btn2">您有<span class="wag_none"> (0) </span>单取消注单
										</span>
									</td>
								</tr>
							</tbody>
						</table>
					</h2>
					
					<!-- 渲染订单 -->
					<div id="ordersDiv">
						
						
					</div>
					

					<h3 id="page_bar">
						<div id="no_page" class="page_none">
							<span class="preious_btn">上一页</span> <span class="line">|</span> <span class="next_btn">下一页</span>
						</div>
					</h3>
				</td>
			</tr>
			<tr>
				<td id="foot"><b>&nbsp;</b></td>
			</tr>
		</tbody>
	</table>
	
</body>
</html>
<script type="text/html" id="DATA_TR">
<table border="0" cellspacing="0" width="100%" cellpadding="0" class="game">
	<tbody>
		{{if rows.length == 0 }}
		<tr>
			<td height="70" class="b_cen">您没有未结算的注单。如果本日已有交易，请查看帐户历史。</td>
		</tr>
		{{/if}}

{{if rows.length > 0 }}
		<tr>
			<th width="5%">编号</th>
			<th width="25%">注单号/投注日期</th>
			<th width="15%">投注类型</th>
			<th width="30%">选项</th>
			<th width="12.5%">投注额</th>
			<th width="12.5%">注单状态</th>
		</tr>
		{{each rows as order i}}
		<tr class="his_even center">
			<td>{{ i + startIndex + 1}}</td>
			<td align="center"><span class="his_wag">{{order.bettingCode}}</span><br>{{order.bettingDate}}<br>{{order.bettingStatus}}</td>
			<td>{{order.sportName}}<br>{{order.gameTimeType}}<br>{{order.typeNames}}</td>
			<td class="his_name">
				{{order.html}}
			</td>
			<td><span class="fin_gold">{{order.bettingMoney}}</span></td>
			<td>未结算</td>
		</tr>
		{{/each}}
		<tr class="sum_bar center">
			<td colspan="4" class="right bold">此页面统计:</td>
			<td>{{totalMoney}}</td>
			<td>&nbsp;</td>
		</tr>
{{/if}}
	</tbody>
</table>
</script>

<script language="javascript">


$(function(){
	refresh();
});

var pageSize = 15;
var pageNumber = 1;

function refresh(){
	$.ajax({
		data:{
			pageSize:pageSize,
			pageNumber:pageNumber
		},
		url : base + "/sports/hg/getTodayOrders.do",
		success:function(data, textStatus,xhr){
			template.config("escape", false);
			var list = toOrdersData(data);
			var html = template("DATA_TR",list);
			$("#ordersDiv").html(html);
		}
	});
}


var gameTimeType = {
	"1": "滚球",
	"2":"今日",
	"3":"早盘"
}


var bettingStatusMap = {
	"1" : "<font color='blue'>待确认</font>",
	"2" : "已确认",
	"3" : "<font color='red'>系统取消</font>",
	"4"  : "<font color='red'>系统取消</font>"
};

function toOrdersData(data){
	$("span .wag_none").html("("+ ( data.aggsData.rejectOrderCount || 0 )+")");
	var pageCount =  Math.ceil(data.total / pageSize) || 1;
	
	refreshPageCount(pageCount);
	var startIndex = (pageNumber - 1) * pageSize;
	var result = {
		startIndex:startIndex,
		rows:[]
	}
	var total = 0;
	for(var i = 0;i < data.rows.length;i++){
		var r = data.rows[i];
		var order = {
			bettingCode	: r.bettingCode,
			bettingDate : (new Date(r.bettingDate)).format("MM月dd日,hh:mm:ss"),
			sportName : getSportName(r.sportType),
			typeNames : r.typeNames,
			league:r.league,
			bettingStatus:bettingStatusMap[r.bettingStatus],
			bettingMoney:r.bettingMoney,
			gameTimeType:gameTimeType[r.gameTimeType]
		}
		
		var html = "";
		total += r.bettingMoney;
		if(r.mix != 2){
			html = toBetHtml(JSON.decode(r.remark),r);
		}else{
			var arr = JSON.decode(r.remark)
			for(var j=0;j<arr.length;j++){
				if(j != 0){
					html += "<div style='border-bottom:1px #303030 dotted;'></div>";
				}
				html += toBetHtml(arr[j],r);
			}
		}
		order.html = html;
		result.rows.push(order);
	}
	result.totalMoney = total;
	return result;
}

function getSportName(type){
	if(type == 1){
		return "足球";
	}
	if(type == 2){
		return "篮球";
	}
	return "其他";
}

function toBetHtml(item,row){
	var con = item.con;
	if(con.indexOf("vs") == -1){
		con = '<span class="text-danger">'+ con +'</span>';
	}
	var homeFirst = !(item.homeStrong === false);//主队是否在前
	var scoreStr = "";
	
	
	if(row.gameTimeType == 1){
		if(homeFirst){
			scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH +":" + row.scoreC + ")</b></font>";
		}else{
			scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC +":" + row.scoreH + ")</b></font>";
		}
	}
	var home = item.firstTeam;
	var guest = item.lastTeam;
	if(item.half === true && row.mix == 2){
		home = home + "<font color='gray'>[上半]</font>";
		guest = guest + "<font color='gray'>[上半]</font>";
	}
	
	var html  = item.league +"<br/>" + 
				home + "&nbsp;" + con + "&nbsp;" + guest + scoreStr + "<br/>" +
				"<font color='red'>"+item.result+ "</font>&nbsp;" +"@" + "&nbsp;<font color='red'>"+ item.odds +"</font>";
	var balance = row.mix != 2 ? row.balance : item.balance;	
	var bt = row.bettingStatus;
	if(balance == 4){
		html = "<s style='color:red;'>" + html+"(赛事腰斩)</s>"
	}else if(bt == 3 || bt == 4){
		html = "<s style='color:red;'>" + html+"("+row.statusRemark+")</s>"
	}else if(balance == 2 || balance == 5 || balance == 6){
		var mr = row.mix != 2 ? row.result:item.matchResult;
		if(homeFirst){
			html = html + "&nbsp;<font color='blue'>("+mr+")</font>";
		}else{
			var ss = mr.split(":");
			html = html + "&nbsp;<font color='blue'>("+ss[1]+":"+ss[0]+")</font>";
		}
	}			
	return html;
}

function refreshPageCount(pageCount){
	$("#pageStatus").html(pageNumber + " / " + pageCount +" 页");
	var $noPage = $("#no_page");
	if(pageCount > 1){
		$noPage.removeClass("page_none");
		$noPage.addClass("page_lis");
	}else{
		$noPage.removeClass("page_lis");
		$noPage.addClass("page_none");
	}
	
	if(pageCount > 1 && pageNumber > 1){
		$noPage.find(".preious_btn").attr("onclick","goPage("+(pageNumber-1)+")");
		$noPage.find(".preious_btn").css("cursor","pointer");
	}else{
		$noPage.find(".preious_btn").removeAttr("onclick");
		$noPage.find(".preious_btn").css("cursor","");
	}
	
	if(pageCount > pageNumber ){
		$noPage.find(".next_btn").attr("onclick","goPage("+(pageNumber+1)+")");
		$noPage.find(".next_btn").css("cursor","pointer");
	}else{
		$noPage.find(".next_btn").removeAttr("onclick");
		$noPage.find(".next_btn").css("cursor","");
	}
	
	$("#page option").remove();
	for(var i = 1;i <= pageCount;i++ ){
		if(i == pageNumber){
			$("#page").append("<option selected value='"+i+"'>"+i+"</option>");
		}else{
			$("#page").append("<option value='"+i+"'>"+i+"</option>");
		}
	}
}

function goPage(pageNo){
	pageNumber = pageNo;
	refresh();
}

function changePage(){
	pageNumber = $("#page").val();
	refresh();
}
</script>

