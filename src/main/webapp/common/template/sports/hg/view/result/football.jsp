<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include  file="/common/template/sports/hg/include/base.jsp"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>皇冠体育</title>
		<%@ include  file="/common/template/sports/hg/include/result/result_resource.jsp"%>
	</head>
<body id="MRSU">
	<div id="mytable"></div>	
</body>
</html>
<%@ include  file="/common/template/sports/hg/include/result/base_table_tpl.jsp"%>
<%@ include  file="/common/template/sports/hg/include/result/football.jsp"%>

<script>

var resultView = new ResultView({
	id : 'mytable',
	title:'足球赛果',
	resultTypes:[{name:'足球:赛果',key:'FT'}],
	tableTpl:'BASE_TABLE',
	gameTpl:'DATA_TR',
	now:<%=System.currentTimeMillis()%>
});
</script>