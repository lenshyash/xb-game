<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%
	String base = request.getContextPath();
	request.setAttribute("base",base);
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>皇冠体育</title>
		<%@ include  file="/common/template/sports/hg/include/jsp/sports.jsp"%>
		<%@ include  file="/common/template/sports/hg/include/jsp/football.jsp"%>
	</head>
<body>
	<!-- 选择只显示联赛 -->
	<%@ include  file="/common/template/sports/hg/include/league.jsp"%>
	<div id="mytable"></div>
</body>
</html>

<%@ include  file="/common/template/sports/hg/include/base_table_tpl.jsp"%>
<%@ include  file="/common/template/sports/hg/include/football_ballcount_tpl.jsp"%>

<script>

var sportView = new SportView({
	id : 'mytable',
	title:'早盘足球 : 总入球',
	tableTpl:'BASE_TABLE',
	gameTpl:'DATA_TR',
	gameType:"FT_FT_BC",
	showOddsTypeList:false,
	showFav:false,
	tableWidth:544,
	autoRefreshTime:180,
	formatViewData:function(data){
		var dataArr = toMapArr(data.headers,data.games,this.gameType);
		return {games:dataArr,gameType:this.gameType};
	},
	getParams:function(params){
		if(LOVE_STATUS == 3){
			var gids = CookieUtil.getCookie(this.gameType);
			params.gids = gids;
		}
		params.gameType = this.gameType;
		params.sortType = $("#" + this.sortSelectorId).val();
	}
});
</script>