<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	String base = request.getContextPath();
	request.setAttribute("base",base);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="Robots" contect="none">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>体育公告</title>
<link rel="stylesheet" href="${base}/common/template/sports/hg/css/result/mem_body_ft.css" type="text/css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/css/result/scroll.css" type="text/css"> 
<script src="${base}/common/js/artTemplate/template.js"></script>
<script src="${base}/common/js/jquery.min.js"></script>
<script src="${base}/common/template/sports/hg/js/core.js" path="${base}"></script>

</head>
<body id="MSG" class="bodyset">

<table border="0" cellpadding="0" cellspacing="0" id="box_top">
  <tr class="msg_top">
    <td>
    
    </td>
  </tr>
  <tr>
    <td class="mem his_top">
    <div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="menu_set">
        <tr class="table_main_settings_tr" style="height:30px;">
          <td id="page_no2"><span id="pg_txt"></span> <span  style="display: none;" id="t_pge"></span>
            <!--<span id="today" onClick="chg_date(0);">今日</span>-->
            	<span id="def" onClick="chg_date(0,this);">
            		<a href="#" ><font id="all" class="scr_on" color="#ffffff">全部</font></a>
            	</span> / 
            	<span onClick="chg_date(1,this);">
            		<a href="#" ><font id="today" color="#ffffff">今日</font></a>
            	</span> / 
            	<span onClick="chg_date(2,this);">
            		<a href="#" ><font id="yesterday" color="#ffffff">昨日</font></a>
            	</span> / 
            	<span onClick="chg_date(3,this);" >
            		<a href="#"><font id="before" color="#ffffff">昨日之前</font></a>
            	</span>
           </td> 	
          <td class="search">
          <!-- 
          	<input type="text" id="findField" name="" value="" class="ccroll_input">
         	 <input type="button"  id="findbutton" name="" onClick="FindNext();" value="搜寻" class="ccroll_btn">
         	 -->
          </td>
          <td class="rsu_refresh">
           <!-- <div onClick="reload_var();"><font id="refreshTime"></font></div>-->
           </td>
          <!--td width="50"><input type="submit" name="button" id="button" value="" onClick="reload_var();" class="his_refresh" /></td-->
        </tr>
      </table>
    </div>
     <table border="0" cellspacing="0" cellpadding="4" class="game">
       <tr>
         <th width="40">序号</th>
         <th width="70">日期</th>
         <th align="left" class="info">公告内容</th>
       </tr>
     </table>
     </td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="box">
  <tr>
    <td class="mem his_body">
    	<table border="0" cellspacing="0" cellpadding="4" class="game" id="noticeTable">
      	</table> 
	</td>
  </tr>
  <tr><td id="foot"><b>&nbsp;</b></td></tr>
</table>

</body>
</html>
<script type="text/html" id="ROWS_TPL">
	{{each data as val i}}
	 <tr class="{{if i%2 == 0}}color_bg2{{/if}}{{if i%2 == 1}}color_bg1{{/if}}" style="display: " onMouseOver="overbars(this,'color_bg3');" onMouseOut="outbars(this,'color_bg2')">
	    <td width="40" class="m_cen">{{i+1}}</td>
	    <td width="70" class="m_cen">{{$formatTime val.noticeDate}}</td>
	    <td class="m_lef">{{val.content}}</td>
	  </tr>
	{{/each}}
</script>

<script  language="javascript">

template.helper('$formatTime', function (time) {
		var date = new Date(time);
		return date.format("yy-MM-dd");;
	});

	function overbars(el,cls){
		$(el).attr("class",cls); 
	}
	
	function outbars(el,cls){
		$(el).attr("class",cls); 
	}
	function chg_date(type,el){
		$("#page_no2").find(".scr_on").removeClass("scr_on");
		$(el).find("font").addClass("scr_on");
		$.ajax({
			data:{
				type : type
			},
			url:'${base}/sports/getNotices.do',
			success:function(data){
				var content = template("ROWS_TPL",data);
				$("#noticeTable").html(content);
			}
		})
	}
	chg_date(0,$("#def")[0]);
</script>
