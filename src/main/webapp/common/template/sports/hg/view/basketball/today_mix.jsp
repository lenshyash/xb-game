<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%
	String base = request.getContextPath();
	request.setAttribute("base",base);
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>皇冠体育</title>
		<%@ include  file="/common/template/sports/hg/include/jsp/sports.jsp"%>
		<%@ include  file="/common/template/sports/hg/include/jsp/basketball.jsp"%>
	</head>
<body>
	<!-- 选择只显示联赛 -->
	<%@ include  file="/common/template/sports/hg/include/league.jsp"%>
	<div id="mytable"></div>
</body>
</html>

<%@ include  file="/common/template/sports/hg/include/base_table_tpl.jsp"%>
<%@ include  file="/common/template/sports/hg/include/basketball_main_tpl.jsp"%>

<script>

var sportView = new SportView({
	id : 'mytable',
	title:'今日篮球 / 美式足球 : 综合过关',
	tableTpl:'BASE_TABLE',
	gameTpl:'DATA_TR',
	gameType:"BK_TD_MX",
	autoRefreshTime:180,
	showFav:false,
	showOddsTypeList:false,
	formatViewData:function(data){
		var dataArr = toMapArr(data.headers,data.games,this.gameType);
		return {games:dataArr,gameType:this.gameType};
	},
	getParams:function(params){
		if(LOVE_STATUS == 3){
			var gids = CookieUtil.getCookie(this.gameType);
			params.gids = gids;
		}
		params.gameType = this.gameType;
		params.sortType = $("#" + this.sortSelectorId).val();
	}
});
</script>