<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>皇冠体育</title>
	<link rel="stylesheet" href="${base}/common/template/sports/hg/css/orders/mem_body_his.css">
	<%@ include  file="/common/template/sports/hg/include/result/result_resource.jsp"%>
	<style type="text/css">
	.color_bg1 { background-color:#E9DBC6;}
	.color_bg2 { background-color:#F2E9DD;}
	</style>
</head>
<body>
	<table border="0" cellpadding="0" cellspacing="0" id="box">
		<tbody>
			<tr>
				<td class="top">
					<h1>
						<em>帐户历史摘要</em>
					</h1>
				</td>
			</tr>
			<tr>
				<td class="mem">
					<h2>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="fav_bar">
							<tbody>
								<tr>
									<td>按体育查看记录:
										<form method="post" id="sel_gtype" style="display: inline;">
											<select style="width: 115px;" name="sportType" id="sportType" class="loteryId" onchange="queryGame(this.value)">
												<option value="">所有体育</option>
												<option value="1">足球</option>
												<option value="2">篮球&amp;美式足球</option>
											</select>&nbsp;&nbsp;
										</form>
									</td>
								</tr>
							</tbody>
						</table>
					</h2>
					<div id="dataContent">
					
					</div>	
		
				</td>
			</tr>
			<tr>
				<td id="foot"><b>&nbsp;</b></td>
			</tr>
		</tbody>
	</table>
</body>
</html>

<script type="text/html" id="DATA_TR">

<table border="0" cellspacing="0" cellpadding="0" class="game">
	<tbody>
		<tr>
			<th class="his_time">日期</th>
			<th class="his_wag">投注额</th>
			<th class="his_wag">派彩结果</th>
		</tr>
		{{each list as item i}}
		<tr class="color_bg{{ (i % 2) + 1 }} ">
			<td class="his_list_none" id="d_date">
				{{if item.bc != '-'}}
				<a style="cursor:pointer;font-weight:bold;" onclick="getHistoryDetail('{{item.date}}')">
				{{/if}}
				{{item.dateStr}}
				{{if item.bc != '-'}}
				</a>
				{{/if}}
			</td>
			<td class="his_td"><span class="fin_gold">{{item.bc}} </span></td>
			<td class="his_td">{{item.wc}}</td>
		</tr>
		{{/each}}
		
		<tr class="sum_bar right">
			<td class="center his_total">总计</td>
			<td class="his_total">{{totalBc}}</td>
			<td class="his_total">{{totalWc}}</td>
		</tr>
	</tbody>
</table>
</script>

<script language="javascript">

$(function(){
	refresh();
})


function queryGame(){
	refresh();
}

function refresh(){
	$.ajax({
		data:{
			sportType : $("#sportType").val()
		},
		url : base + "/sports/hg/getHistoryTotalData.do",
		success:function(data, textStatus,xhr){
			template.config("escape", false);
			var html = template("DATA_TR",toShowData(data.list))
			$("#dataContent").html(html);
		}
	});
}

var weekMap = ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"]

function toShowData(list){
	var result = {
		list : []	
	};
	var totalBc = 0;
	var totalWc = 0;
	for(var i=0;i<list.length;i++){
		var item = list[i];
		totalBc += item["betMoneyCount"] || 0;
		totalWc += item["resultCount"] || 0;
		result.list.push({
			date:item.dateStr,
			dateStr :toDateStr(item.dateStr) +"&nbsp;"+weekMap[item.week],
			bc : item["betMoneyCount"] || "-",
			wc : item["resultCount"] || "-"
		});
	}
	result.totalBc = totalBc.toFixed(2);
	result.totalWc = totalWc.toFixed(2);
	return result;
}

function toDateStr(str){
	var arr = str.split("-");
	return arr[1]+"月"+arr[2]+"日";
}

function getHistoryDetail(date){
	window.location.href = base + "/sports/hg/goHistoryDetail.do?date="+date+"&sportType="+$("#sportType").val();
}

</script>