<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="BASE_TABLE">
	<table border="0" cellpadding="0" cellspacing="0" id="box" style="width:{{width}}px">
		<tbody>
			<tr>
				<td class="top">
					<h1>
						<em>{{title}}</em>
						{{if timeData}}
						<select onchange="pdChange(this)">
							{{each timeData as item}}
							<option value="{{item.value}}" {{if item.select}} selected {{/if}}>{{item.name}}</option>
							{{/each}}
						</select>
						{{/if}}
					</h1>
			
				</td>
			</tr>
			<tr>
				<td class="mem">
					<h2>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="fav_bar">
							<tbody>
								<tr>
									<td id="page_no">
										<span id="pg_txt" class="pg_top">1 / 1 页&nbsp;&nbsp;
										
										</span>
										<select id="view_page_selector">
											<option value="1" selected="">1</option>
										</select>
									</td>
									
									<td id="tool_td">
										<table border="0" cellspacing="0" cellpadding="0"
											class="tool_box">
											<tbody>
												<tr>
													{{if showFav}}
													<td id="fav_btn">
														<div id="fav_num" title="清空" onclick="chkDelAllShowLoveI('{{gameType}}');" style="display: none;">
															<!--我的最爱场数-->
															<span id="live_num"></span>
														</div>
														<div id="showNull" title="无资料" class="fav_null" style="display: block;"></div>
														<div id="showAll" title="所有赛事" style="display: none;" class="fav_on" onclick="showAllMatch('{{gameType}}')"></div>
														<div id="showMy" title="我的最爱" class="fav_out" style="display: none;" onclick="showLoveMatch('{{gameType}}')"></div>
													</td>
													{{/if}}
													
													{{if showRefresh}}
													<td class="refresh_btn" id="refresh_btn">
														<!--秒数更新-->
														<div >
															<font id="refreshTime">{{autoRefreshTime}}</font>
														</div>
													</td>
													{{/if}}
													
													{{if showLeagueFilter}}
													<td class="leg_btn">
														<div id="sel_league">
															选择联赛 (<span id="str_num">全部</span>)
														</div>
													</td>
													{{/if}}
													
													{{if showSort}}
													<td id="SortGame" class="SortGame" name="SortGame">
														<select id="SortSel" >
															<option value="1">按时间排序</option>
															<option value="2">按联盟排序</option>
														</select>
													</td>
													{{/if}}
													
													{{if showOddsTypeList}}
													<td class="OrderType" id="Ordertype">
														<select id="myoddType">
															{{each oddsTypes as val}}
																<option value="{{val[0]}}" {{if val[0] == oddType}}selected{{/if}}>{{val[1]}}</option>
															{{/each}}
														</select>
													</td>
													{{/if}}
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</h2>
					<div id="showtable">
						
					</div>
			</tr>
			<tr>
				<td id="foot"><b>&nbsp;</b></td>
			</tr>
		</tbody>
	</table>

	<div id="refresh_down" class="refresh_M_btn" >
		<span>刷新</span>
	</div>
</script>
