<%@ page language="java" pageEncoding="UTF-8"%>


<script type="text/html" id="DATA_TR">

<table id="game_table" cellspacing="0" cellpadding="0" class="game">
    <tr>
		<th class="time">时间</th>
		<th class="team">赛事</th>
		<th class="h_f">主 / 主</th>
		<th class="h_f">主 / 和</th>
		<th class="h_f">主 / 客</th>
		<th class="h_f">和 / 主</th>
		<th class="h_f">和 / 和</th>
		<th class="h_f">和 / 客</th>
		<th class="h_f">客 / 主</th>
		<th class="h_f">客 / 和</th>
		<th class="h_f">客 / 客</th>
	</tr>
	{{if !games || games.length == 0}}
	<td colspan="20" class="no_game">您选择的项目暂时没有赛事。请修改您的选项或迟些再返回。</td> {{/if}} {{each games as val i}}

	<!-- 输出联赛 -->
	{{if i== 0 || games[i-1].league != val.league }}
	<tr>
		<td colspan="11" class="b_hline">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="legicon" onClick="showLeg('{{val.league}}')"><span class="showleg" name="{{val.league}}"> {{if val.hidden == false}} <span id="LegOpen"></span> {{/if}} {{if val.hidden}} <span id="LegClose"></span> {{/if}}
					</span></td>
					<td onClick="showLeg('{{val.league}}')" class="leg_bar">{{val.league}}</td>
				</tr>
			</table>
		</td>
	</tr>
	{{/if}}

	<tr id="TR_{{val.gid}}" {{if val.hidden}} style="display: none;"{{/if}}>
		<td class="b_cen">{{$formatDate gameType val}}</td>
		<td class="team_name">{{$teamName val.home}}<br>{{$teamName val.guest}}</td>
		<td class="b_cen">{{$showOdds val.ior_FHH 'ior_FHH' val.gid '胜胜'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FHN 'ior_FHN' val.gid '胜和'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FHC 'ior_FHC' val.gid '胜负'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FNH 'ior_FNH' val.gid '和胜'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FNN 'ior_FNN' val.gid '和和'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FNC 'ior_FNC' val.gid '和负'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FCH 'ior_FCH' val.gid '负胜'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FCN 'ior_FCN' val.gid '负和'}}</td>
		<td class="b_cen">{{$showOdds val.ior_FCC 'ior_FCC' val.gid '负负'}}</td>
	</tr>
	{{/each}}
</table>

</script>