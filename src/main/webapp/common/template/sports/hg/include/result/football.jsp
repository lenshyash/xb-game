<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">
<table border="0" cellspacing="0" cellpadding="0" class="game">
	<tbody>
		<tr>
			<th class="time">时间</th>
			<th class="rsu">赛果</th>
		</tr>
	</tbody>
</table>



<table border="0" cellspacing="0" cellpadding="0" class="game">
{{if !data || data.length == 0}}
	<tbody>
		<tr>
			<td class="no_game">没有赛果</td>
		</tr> 
	</tbody>	
{{/if}} 

{{each data as val i}}
	<!-- 输出联赛 -->
	{{if i== 0 || data[i-1].league != val.league }}
	<tbody>
		<tr>
			<td colspan="6" onclick="openOrClose('{{val.league}}',this)" class="b_hline">
				<span class="showleg"><span id="LegOpen" leg='{{val.league}}'></span></span>
				<span class="leg_bar">{{val.league}}</span>
			</td>
		</tr>
	</tbody>
	{{/if}}

	<tbody name="{{val.league}}">
		<tr class="b_cen" id="">
			<td rowspan="3" class="time">{{$formatTime val.startTime}}</td>
			<td class="team">比赛队伍</td>
			<td colspan="2" class="team_out_ft">
				<table border="0" cellpadding="0" cellspacing="0" class="team_main">
					<tbody>
						<tr class="b_cen">
							<td width="12" class="noBorder">&nbsp;</td>
							<td class="team_c_ft noBorder">{{val.homeTeam}}&nbsp;&nbsp;</td>
							<td class="vs noBorder">vs.</td>
							<td class="team_h_ft noBorder">{{val.guestTeam}}&nbsp;&nbsp;</td>
							<td width="12" class="noBorder">&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td class="more_td"></td>
		</tr>
		<tr class="hr">
			<td class="hr_title {{$printClass val.scoreH1H}}">半场</td>
			<td class="hr_main_ft {{$printClass val.scoreH1H}}"><span style="overflow: hidden;">{{val.scoreH1H}}</span></td>
			<td class="hr_main_ft {{$printClass val.scoreH1H}}"><span style="overflow: hidden;">{{val.scoreH1G}}</span></td>
			<td rowspan="2" class="more_td"></td>
		</tr>
		<tr class="full">
			<td class="full_title {{$printClass val.scoreFullH}}">全场</td>
			<td class="full_main_ft {{$printClass val.scoreFullH}}"><span style="overflow: hidden;">{{val.scoreFullH}} </span></td>
			<td class="full_main_ft {{$printClass val.scoreFullH}}"><span style="overflow: hidden;">{{val.scoreFullG}}</span></td>
		</tr>
	</tbody>
{{/each}}
</table>
</script>