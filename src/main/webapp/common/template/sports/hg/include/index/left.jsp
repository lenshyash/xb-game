<%@ page language="java"  pageEncoding="UTF-8"%>


<script type="text/html" id="tenOrderTpl">

{{if !orders || orders.length == 0}}
	<div class="show_info" align="center">
		<span class="show_top"></span>您没有未结算的交易。
	</div>
{{/if}}

{{if orders.length > 0}}
<div class="ord">
	<div class="title">
		<h1>最新十笔交易</h1>
		<div class="tiTimer" onclick="searchRecentOrder();"></div>
	</div>

	{{each orders as order}}
		<div class="show">
			<div class="tname">
				<em>
					<font class="his_h">{{order.firstTeam}}</font> 
					{{order.con}}
					<font class="his_a">{{order.lastTeam}}</font>{{order.scoreStr}}<br>
					<font color="black">{{order.typeNames}}</font><br>
					<font class="his_result">{{order.result}}</font>
					<font style="color: #000">&nbsp;@&nbsp;</font>
					<font class="his_odd">{{order.odds}}</font>
				</em>
				<b class="gold">
					<span class="fin_gold">RMB {{order.bettingMoney}}</span>
				</b>
			</div>
		</div>
	{{/each}}
</div>
{{/if}}
</script>




<script type="text/html" id="BET_WIN">
<div class="ord">
	<div class="title">
		<h1 id="matchType" style="padding-top: 0px;">
			{{sport}}<input type="hidden" id="mType" value="0">
		</h1>
		<div id="autoRefresh" onclick="freshNewOdds();">
			<span id="autoFreshTime">10</span><input title="勾选即可让赔率10秒刷新一次" style="cursor:pointer" type="checkbox" {{if autoRefresh}} checked="checked"{{/if}} id="acceptAutoFresh">
		</div>
	</div>
	<div class="main">
		<div id="matchDetail">
			{{each items as val}}
			<div id="del{{val.deleteId}}">
				<div class="leag" style="padding-right: 3px;">
					<span class="leag_txt">{{val.league}}</span>
					{{if isMix}}
					<span class="deletebtn">
						<input type="button" name="delteam1" value="" onclick="delBetItem({{val.deleteId}})" class="par">
					</span>
					{{/if}}
				</div>
				<div class="gametype">{{val.title}}</div>
				<div class="teamName">
					<span class="tName">{{#val.team}}</span>
				</div>
				<p class="team"  {{if val.replace}} style="background-color:rgb(255, 255, 0);" {{/if}}>
					{{#val.oddsDesc}}
				</p>
			</div>
			{{/each}}
		</div>

		<p class="auto">
			<input type="checkbox" id="autoAccept" ><span class="auto_info" title="在方格里打勾表示，如果投注单里的任何选项在确认投注时赔率变佳，系统会无提示的继续进行该下注。">自动接受较佳赔率</span>
		</p>

		{{if !isMix  || items.length >2 }}
		<div class="betdata" id="quataDetail">
			<p class="amount">
				交易金额：<input name="gold" type="text" onkeyup="RepNumber(this)" class="txt" id="betMoney" size="8" maxlength="10">
			</p>
			<p class="mayWin">
				<span class="bet_txt">可赢金额：</span><font id="winMoney">0.00</font>
			</p>
			<p class="minBet">
				<span class="bet_txt">单注最低：</span><font id="minMoney">${minBettingMoney}</font>
			</p>
			<p class="minBet">
				<span class="bet_txt">单注最高：</span><font id="maxMoney">${maxBettingMoney}</font>
			</p>
			<p class="minBet">
				<span class="bet_txt">单场最高：</span><font id="gameMaxMoney">${gameMaxBettingMoney}</font>
			</p>
			<!-- 单注最高：100,000 50-->
		</div>
		{{/if}}
	</div>
	<div class="betBox" class="noprint">
		<input type="button" name="btnCancel" value="取消" onclick="cancelBet()" class="no"><input id="submitButton" type="button" name="SUBMIT" value="确定交易" onclick="submitOrder()" class="yes">
	</div>
</div>
</script>


<div id="left_content" style="width:240px;background-color:red;overflow:hidden;float:left;background-color: #493721;">
	<div id="mem_box" class="noprint">
		<div id="mem_main">
			<span class="his"><a onclick="targetHistoryDay();return false;" style="cursor:pointer">帐户历史</a></span> | <span class="wag"><a onclick="targetRecent()" style="cursor:pointer">交易状况</a></span>
		</div>
		<div id="credit_main">
			<span id="credit">未登录</span><input name="" class="re_credit" value="" onclick="updateMemberInfo();" type="button">
		</div>
	</div>


	<div style="margin-left:10px;">
		<div id="menu" class="noprint">
			<div class="ord_on" id="order_button" style="width:99px;" onclick="showOrder(this)">交易单</div>
			<div class="record_btn" id="record_button" style="width:117px"  onclick="showRecentOrder(this)">最新十笔交易</div>
		</div>
	
		<div id="order_div" name="order_div" style="overflow-x: hidden;">
			<!-- plsdiv -->
			<div id="pls_bet" name="pls_bet" style="background-color: rgb(227, 207, 170); left: 0px; top: 0px; ">
				<img src="${base}/common/template/sports/hg/images/sports/order_none.jpg" height="22" width="216">
				<div style="width: 216; height: 63px; text-align: center; padding-top: 16px;">
					<font style="font: 12px Arial, Helvetica, sans-serif; font-weight: bold;">点击赔率便可将<br>选项加到交易单里。
					</font>
				</div>
			</div>
			<!-- bet -->
			<div id="bet_div" class="bet_info" name="bet_div" style="display: none;">
			</div>
	
			<!-- 最近交易 -->
			<div id="recentDiv" name="rec5_div" style="display: none;">
				<div class="show_info" align="center">
					<span class="show_top"></span>您没有未结算的交易。
				</div>
			</div>
	
			<br />
		
			<div id="info_div" name="info_div" style="top: 293px;">
				<div class="msg_box">
					<h2>
						公告<span class="more"><a href="javascript:void(0);" onclick="showMoreNotice();">更多</a></span>
					</h2>
					<div class="msg_main">
						<marquee scrollamount="1" direction="up" onmouseover="this.stop();" onmouseout="this.start();" height="300">
							<span id="real_msg">
								<ul style="padding: 15px;">
									${notices}
								</ul>
							</span>
						</marquee>
					</div>
				</div>
			</div>
		</div>
		
		<button id='phoneAuto' style='background:#A7A7A7;border:none;border-radius: 8px; padding:15px 70px;color:#fff'>适配手机版</button>
		<script>
		var browser={
				versions:function(){
				var u = navigator.userAgent, app = navigator.appVersion;
				return {//移动终端浏览器版本信息
				trident: u.indexOf('Trident') > -1, //IE内核
				presto: u.indexOf('Presto') > -1, //opera内核
				webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
				gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
				mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
				ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
				android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
				iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
				iPad: u.indexOf('iPad') > -1, //是否iPad
				webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
				weixin: u.indexOf('MicroMessenger') > -1, //是否微信
				qq: u.match(/\sQQ/i) == " qq" //是否QQ
				};
				}(),
				language:(navigator.browserLanguage || navigator.language).toLowerCase()
				};

				if(browser.versions.mobile || browser.versions.ios || browser.versions.android ||
				browser.versions.iPhone || browser.versions.iPad){
				//移动
				$("#phoneAuto").show()
				}else{
				//PC
				$("#phoneAuto").hide()
				}

		var sportHeightPhone = 0;
			$("#phoneAuto").click(function(){
				sportHeightPhone = $(document.getElementById('sport_content_iframe').contentWindow.document.body).find('#mytable').height()
				if(sportHeightPhone < 882){
					sportHeightPhone = 882;
				}
				$("#phoneSports").css("overflow","visible")
				$('#sportHeader2', window.parent.document).height(sportHeightPhone +150)
				$(document.getElementById('sport_content_iframe')).height(sportHeightPhone+150)
			})
		</script>
	</div>
</div>
