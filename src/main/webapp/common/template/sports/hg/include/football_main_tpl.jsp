<%@ page language="java" pageEncoding="UTF-8"%>


<script type="text/html" id="DATA_TR">
 <table cellspacing="0" cellpadding="0" class="game">
	   <tr>
	     <th nowrap  class="time">时间</th>
	     <th nowrap class="team">赛事</th>
	     <th nowrap class="h_1x2">独赢</th>
	     <th nowrap class="h_r">全场 - 让球</th>
	     <th nowrap class="h_ou">全场 - 大小</th>
	     <th nowrap class="h_oe">单双</th>
	     <th nowrap class="h_1x2">独赢</th>
	     <th nowrap class="h_r">半场 - 让球</th>
	     <th nowrap class="h_ou">半场 - 大小</th>
	   </tr>
	{{if !games || games.length == 0}}
		<td colspan="20" class="no_game">您选择的项目暂时没有赛事。请修改您的选项或迟些再返回。</td>
	{{/if}}
	{{each games as val i}}
		
		<!-- 输出联赛 -->
		{{if i== 0 || games[i-1].league != val.league }}
		<tr>
			<td colspan="9" class="b_hline">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="legicon" onClick="showLeg('{{val.league}}')">
							<span class="showleg"  name="{{val.league}}">	
								{{if val.hidden == false}}
								<span id="LegOpen"></span>
								{{/if}}

								{{if val.hidden}}
								<span id="LegClose"></span>
								{{/if}}
		 					</span>
						</td>
						<td onClick="showLeg('{{val.league}}')" class="leg_bar">{{val.league}}</td>
					</tr>
				</table>
			</td>
		</tr>
		{{/if}}
	  <tr id="TR_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}}>
	    <td rowspan="3" class="b_cen">{{$formatDate gameType val}}</td>
	    <td rowspan="2" class="team_name none" onmouseover="mouseEnter_pointer({{val.gid}});" onmouseout="mouseOut_pointer({{val.gid}});">{{$teamName val.home}}<br>{{$teamName val.guest}}</td>
	    <td class="b_cen">{{$showOdds val.ior_MH 'ior_MH' val.gid '全胜'}}</td>
	    <td class="b_rig"><span class="con">{{$letBall val.CON_RH true}}</span> <span class="ratio">{{$showOdds val.ior_RH 'ior_RH' val.gid '全让胜'}}</span></td>
	    <td class="b_rig"><span class="con">{{$appendBefore val.CON_OUH '大'}}</span> <span class="ratio">{{$showOdds val.ior_OUH 'ior_OUH' val.gid '全大球'}}</span></td>
	    <td class="b_cen">{{$showOdds val.ior_EOO 'ior_EOO' val.gid '单'}}</td>
	    <td class="b_1st">{{$showOdds val.ior_HMH 'ior_HMH' val.gid '半胜'}}</td>
	    <td class="b_1stR"><span class="con">{{$letBall val.CON_HRH true}}</span> <span class="ratio">{{$showOdds val.ior_HRH 'ior_HRH' val.gid '半让胜'}}</span></td>
	    <td class="b_1stR"><span class="con">{{$appendBefore val.CON_HOUH '大'}}</span> <span class="ratio">{{$showOdds val.ior_HOUH 'ior_HOUH' val.gid '半大球'}}</span></td>
	  </tr>
	  
	  <tr id="TR1_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}}>
	    <td class="b_cen">{{$showOdds val.ior_MC "ior_MC" val.gid '全负'}}</td>
	    <td class="b_rig"><span class="con">{{$letBall val.CON_RC false}}</span> <span class="ratio">{{$showOdds val.ior_RC 'ior_RC' val.gid '全让负'}}</span></td>
	    <td class="b_rig"><span class="con">{{$appendBefore val.CON_OUC '小'}}</span> <span class="ratio">{{$showOdds val.ior_OUC 'ior_OUC' val.gid '全小球'}}</span></td>
	    <td class="b_cen">{{$showOdds val.ior_EOE 'ior_EOE' val.gid '双'}}</td>
	    <td class="b_1st">{{$showOdds val.ior_HMC 'ior_HMC' val.gid '半负'}}</td>
	    <td class="b_1stR"><span class="con">{{$letBall val.CON_HRC false}}</span> <span class="ratio">{{$showOdds val.ior_HRC 'ior_HRC' val.gid '半让负'}}</span></td>
	    <td class="b_1stR"><span class="con">{{$appendBefore val.CON_HOUC '小'}}</span> <span class="ratio">{{$showOdds val.ior_HOUC 'ior_HOUC' val.gid '半小球'}}</span></td>
	  </tr>
	  
	  <tr id="TR2_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}} >
	    <td class="drawn_td" onmouseover="mouseEnter_pointer({{val.gid}});" onmouseout="mouseOut_pointer({{val.gid}});" >
			<table width="99%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">和局</td>
					<td class="hot_td">
						<span id="sp_{{val.gid}}">
						{{if val.islove}}
							<div id="" class="fov_icon_on" style="cursor:hand" title="移除" onClick="chkDelshowLoveI({{val.gid}},'{{gameType}}');"></div>
						{{/if}}

						{{if !val.islove}}
							<div id="love_tag_{{val.gid}}" class="fov_icon_out" style="cursor: pointer; display: none;" title="我的最爱" onclick="addShowLoveI({{val.gid}},'{{gameType}}'); "></div>
						{{/if}}
						</span>
					</td>
					<td class="hot_tv"></td>
				</tr>
			</table>
		</td>
	    <td class="b_cen">{{$showOdds val.ior_MN 'ior_MN' val.gid '全平'}}</td>
	    <td colspan="3" valign="top" class="b_cen"><span class="more_txt"><!-- *MORE* --></span></td>
	    <td class="b_1st" >{{$showOdds val.ior_HMN 'ior_HMN' val.gid '半平'}}</td>
	    <td colspan="3" valign="top" class="b_1st">&nbsp;</td>
	  </tr>
	{{/each}} 	
 </table>
</script>