<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="BASE_TABLE">
	<table border="0" cellpadding="0" cellspacing="0" id="box" style="width: {{width}}px;">
		<tbody>
			<tr>
				<td class="top">
					<h1>
						<em>{{title}}</em>
					</h1>
				</td>
			</tr>
			<tr>
				<td class="mem">
					<h2>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="fav_bar">
							<tbody>
								<tr>
									<td id="page_no">
										<select name="selgtype" id="selgtype" >
											{{each resultTypes as type}}
											<option value="{{type.key}}" >{{type.name}}</option>
											{{/each}}
										</select>
									</td>
									<td id="tool_td">
										<table border="0" cellspacing="0" cellpadding="0" class="tool_box">
											<tbody>
												<tr>
													<td class="searh">
													<input type="hidden" id="currPage" value="1">
													<input type="hidden" id="pageCount" value="12">
													<span class="rig">
													<!--
													<a id="prev" >前一天</a>
													<span id="nextB" style="display: inline;">/
														<a id="next" >下一天</a>
													</span> -->
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;选择日期 : 
													<input style="height:12px;" value="{{dateStr}}" onclick="WdatePicker({errDealMode:1,maxDate:'{{dateStr}}'})" id="dateTime" type="text" value="" size="10" maxlength="10" class="txt">
													<input type="button" id="searchBtn" value="查询" name="submit">
													</span>
													</td>
													<td class="rsu_refresh">
														<div>
															<font id="refreshTime"></font>
														</div>
													</td>
													<td class="OrderType"></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</h2>
					<div id="showtable">
					
					</div>
			</tr>
			<tr>
				<td id="foot"><b>&nbsp;</b></td>
			</tr>
		</tbody>
	</table>
</script>
