<%@ page language="java"  pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="right_content" style="float:left;">
	
	<div id="nav">

	</div>
	
	<div id="type">
		
	</div>
	
	<!-- 显示体育赛事信息 --> 
	<div id="sport_content" style="height:1000px;position:absolute;">
		<iframe id="sport_content_iframe"  frameborder="0" width="760" height="782" url="" name="bodyframe" style="background: #fff;overflow-y:scroll;  "></iframe>
	</div>
</div>
<script type="text/html" id="second_navigation">
	<ul class="level1">
		{{each data as val}}
		<li class="{{val.className}}">
			<span class="ball">
				<a id="ST_{{val.dataKey}}" onclick="changeSportType(this,'{{val.dataKey}}')" class="">{{val.name}}
					(<strong id="COUNT_{{val.dataKey}}" class="game_sum">0</strong>)
				</a>
			</span>
		</li>
		{{/each}}
	</ul>
</script>

<script type="text/html" id="third_navigation">
	<ul>
		{{each data as val}}
		<li class="re"><a id="GT_{{val.dataKey}}" onclick="changeDataType(this,'{{val.dataKey}}');">{{val.name}}</a></li>
		{{/each}}
		<li class="result"><a class="type_out" href="${base}/help/QA_sport.do" target="_blank">规则</a></li>
		<li class="result"><a id="result_class" class="type_out" onclick="showResult('{{type}}');">赛果</a></li>
	</ul>
</script>
<c:if test="${onoff_sports_basketBall_game  eq 'off'}">
<style>
.level1 .bk{
	display:none;
}
</style>
</c:if>