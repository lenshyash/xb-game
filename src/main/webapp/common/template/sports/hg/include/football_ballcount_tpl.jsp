<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">

<table id="game_table" cellspacing="0" cellpadding="0" class="game">
	<tr>
		<th class="time">时间</th>
		<th class="team">赛事</th>
		<th class="h_oe">0 - 1</th>
		<th class="h_oe">2 - 3</th>
		<th class="h_oe">4 - 6</th>
		<th class="h_oe">7或以上</th>
	</tr>
	{{if !games || games.length == 0}}
	<td colspan="20" class="no_game">您选择的项目暂时没有赛事。请修改您的选项或迟些再返回。</td> {{/if}} {{each games as val i}}

	<!-- 输出联赛 -->
	{{if i== 0 || games[i-1].league != val.league }}
	<tr>
		<td colspan="6" class="b_hline">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="legicon" onClick="showLeg('{{val.league}}')"><span class="showleg" name="{{val.league}}"> {{if val.hidden == false}} <span id="LegOpen"></span> {{/if}} {{if val.hidden}} <span id="LegClose"></span> {{/if}}
					</span></td>
					<td onClick="showLeg('{{val.league}}')" class="leg_bar">{{val.league}}</td>
				</tr>
			</table>
		</td>
	</tr>
	{{/if}}

	<tr id="TR_{{val.gid}}" {{if val.hidden}} style="display: none;"{{/if}}>
		<td class="b_cen">{{$formatDate gameType val}}</td>
		<td class="team_name">{{$teamName val.home}}<br>{{$teamName val.guest}}
		</td>
		<td class="b_cen">{{$showOdds val.ior_T01 'ior_T01' val.gid '0-1'}}</td>
		<td class="b_cen">{{$showOdds val.ior_T23 'ior_T23' val.gid '2-3'}}</td>
		<td class="b_cen">{{$showOdds val.ior_T46 'ior_T46' val.gid '4-6'}}</td>
		<td class="b_cen">{{$showOdds val.ior_OVER 'ior_OVER' val.gid '7-'}}</td>
	</tr>
	{{/each}}
</table>

</script>