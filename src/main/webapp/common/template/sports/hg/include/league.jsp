<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="TPL_LEGS">
<table border="0" cellspacing="1" cellpadding="0" class="leg_game">
	<tbody style="text-align:left;" id="league">
	{{each trs as tr}}
		<tr>
			{{each tr as leg}}
			{{if leg !== false}}
			<td class="league">
				<div>
					<input style="height:auto;" type="checkbox" name="select_leg" leg='{{leg.name}}' {{if leg.checked}}checked="checked"{{/if}}>
					<font style="font-size: 12px;" title="{{leg.name}}">{{leg.name}}</font>
				</div>
			</td>
			{{/if}}
			{{if leg === false}}
				<td class="league"></td>
			{{/if}}
			{{/each}}
		</tr>
	{{/each}}
	</tbody>
</table>
</script>

<div id="legView" style="top: 82px; left: 10px; z-index: 9;display:none;" class="legView">
	<div class="leg_head"></div>
	<div style="margin: 0; padding: 0 10px; text-align: center; background-color: #493721;">
		<table border="0" style="margin: 0 auto; width: 701px; background-color: #D5D4C8;" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="leg_top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td style="text-align: left;" width="30%">
										<h1>
											<input type="checkbox" id="selectAll">
											<label class="all_sel" for="selectAll">全选</label>
										</h1>
									</td>
									<td class="btn_td">
										<input type="button" value="取消" class="enter_btn close_btn">&nbsp;
										<input type="button" value="提交" class="enter_btn filter_btn">
									</td>
									<td class="close_td"><span class="close_box" >关闭</span></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<div class="leg_mem">
							<!-- content -->
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="btn_box">
		    <input type="button" value="取消" class="enter_btn close_btn">&nbsp;&nbsp;&nbsp;
		    <input type="button" value="提交" class="enter_btn filter_btn">
		</div>
	</div>
	<div class="leg_foot"></div>
</div>