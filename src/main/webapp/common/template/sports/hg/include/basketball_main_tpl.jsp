<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">
<table cellspacing="0" cellpadding="0" class="game">
	<tr>
		<th class="time">时间</th>
		<th class="team">赛事</th>
		<th class="h_m">独赢</th>
		<th class="h_r">让球</th>
		<th class="h_ou">大小</th>
		<!--th class="h_oe">单/双</th-->
		<th class="h_ouhc" colspan="2">球队得分: 大 / 小</th>
		<th class="h_ou">单节让球</th>
		<th class="h_ou">单节总入球</th>
	</tr>
	{{if !games || games.length == 0}}
	<td colspan="20" class="no_game">您选择的项目暂时没有赛事。请修改您的选项或迟些再返回。</td> {{/if}}
{{each games as val i}}
	<!-- 输出联赛 -->
	{{if i== 0 || games[i-1].league != val.league }}
	<tr>
		<td colspan="9" class="b_hline">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="legicon" onClick="showLeg('{{val.league}}')"><span class="showleg" name="{{val.league}}"> {{if val.hidden == false}} <span id="LegOpen"></span> {{/if}} {{if val.hidden}} <span id="LegClose"></span> {{/if}}
					</span></td>
					<td onClick="showLeg('{{val.league}}')" class="leg_bar">{{val.league}}</td>
				</tr>
			</table>
		</td>
	</tr>
	{{/if}}
	  <tr id="TR_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}} onmouseover="mouseEnter_pointer({{val.matchId}});" onmouseout="mouseOut_pointer({{val.matchId}});">
	    <td rowspan="2" class="b_cen">
			{{if i== 0 || games[i-1].matchId != val.matchId}}
				{{$formatDate gameType val}}
			{{/if}}
		</td>
	    <td rowspan="2" class="team_name">{{$teamName val.home}}
		  <span>
		 {{if i== 0 || games[i-1].matchId != val.matchId}}	 
			<table border="0" cellpadding="0" cellspacing="0" class="fav_tab">
				<tbody>
					<tr>
						<td class="hot_star">
							<span id="sp_{{val.matchId}}">
								{{if val.islove}}
									<div id="" class="fov_icon_on" style="cursor:hand" title="移除" onClick="chkDelshowLoveI({{val.matchId}},'{{gameType}}');"></div>
								{{/if}}
								{{if !val.islove}}
									<div id="love_tag_{{val.matchId}}" class="fov_icon_out" style="cursor: pointer; display: none;" title="我的最爱" onclick="addShowLoveI({{val.matchId}},'{{gameType}}'); "></div>
								{{/if}}
							</span>
						</td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
		 {{/if}}
		  </span><br>
	      <span class="bk_team_c">{{$teamName val.guest}} </span></td>
	    <td class="b_cen bc_h">{{$showOdds val.ior_MH 'ior_MH' val.gid '全胜'}}</td>
	    <td class="b_rig"><span class="con">{{$letBall val.CON_RH true}}</span> <span class="ratio">{{$showOdds val.ior_RH 'ior_RH' val.gid '全让胜'}}</span></td>
	    <td class="b_rig"><span class="con">{{$appendBefore val.CON_OUH '大'}}</span> <span class="ratio">{{$showOdds val.ior_OUH 'ior_OUH' val.gid '全大球'}}</span></td>
	    <td class="bg_light_bk"><span class="con">{{if val.CON_OUHO.length>0}}<font class="text_green">大</font>{{/if}}{{val.CON_OUHO}}</span><span class="ratio">{{$showOdds val.ior_OUHO 'ior_OUHO' val.gid '全主大球'}}</span></td>
	    <td class="bg_light_bk"><span class="con">{{if val.CON_OUHU.length>0}}<font class="text_brown">小</font>{{/if}}{{val.CON_OUHU}} </span><span class="ratio">{{$showOdds val.ior_OUHU 'ior_OUHU' val.gid '全主小球'}}</span></td>
	<td class="b_rig"><span class="con">{{$letBall val.MS_RATIO_REH true}}</span> <span class="ratio">{{$showOdds val.MS_IOR_ROUH 'MS_IOR_ROUH' val.gid '全让胜'}}</span></td>
	<td class="b_rig"><span class="con">{{$appendBefore val.MS_RATIO_ROUO '大'}}</span> <span class="ratio">{{$showOdds val.MS_IOR_ROUH 'MS_IOR_ROUH' val.gid '全大球'}}</span></td>
	</tr>
	  <tr id="TR1_{{val.gid}}" {{if val.hidden}} style="display: none;"{{/if}}  onmouseover="mouseEnter_pointer({{val.matchId}});" onmouseout="mouseOut_pointer({{val.matchId}});">
	  	<td class="b_cen bc_h">{{$showOdds val.ior_MC 'ior_MC' val.gid '全负'}}</td>
	    <td class="b_rig"><span class="con">{{$letBall val.CON_RC false}}</span> <span class="ratio">{{$showOdds val.ior_RC 'ior_RC' val.gid '全让负'}}</span></td>
	    <td class="b_rig"><span class="con">{{$appendBefore val.CON_OUC '小'}}</span> <span class="ratio">{{$showOdds val.ior_OUC 'ior_OUC' val.gid '全小球'}}</span></td>
	    <!--td class="b_cen">*RATIO_EOE*</td-->
	    <td class="bg_dark_bk"><span class="con">{{if val.CON_OUCO.length>0}}<font class="text_green">大</font>{{/if}}{{val.CON_OUCO}}</span><span class="ratio">{{$showOdds val.ior_OUCO 'ior_OUCO' val.gid '客全大球'}}</span></td>
	    <td class="bg_dark_bk"><span class="con">{{if val.CON_OUCU.length>0}}<font class="text_brown">小</font>{{/if}}{{val.CON_OUCU}}</span><span class="ratio">{{$showOdds val.ior_OUCU 'ior_OUCU' val.gid '客全小球'}}</span></td>
	<td class="b_rig"><span class="con">{{$letBall val.MS_RATIO_REC false}}</span> <span class="ratio">{{$showOdds val.MS_IOR_ROUC 'MS_IOR_ROUC' val.gid '全让负'}}</span></td>
	<td class="b_rig"><span class="con">{{$appendBefore val.MS_RATIO_ROUU '小'}}</span> <span class="ratio">{{$showOdds val.MS_IOR_ROUC 'MS_IOR_ROUC' val.gid '全小球'}}</span></td>
	  </tr>
	{{/each}}
</table>
</script>