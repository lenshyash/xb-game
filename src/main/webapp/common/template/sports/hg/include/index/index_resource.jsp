<%@ page language="java" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="${base}/common/template/sports/hg/css/index/sports.css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/css/index/mem_header_ft_cn.css">
<link rel="stylesheet" href="${base}/common/template/sports/hg/css/index/mem_order_sel.css?v=1">
<link rel="stylesheet" href="${base}/common/template/sports/hg/css/index/mem_order_ft.css">

<script src="${base}/common/js/jquery.min.js"></script>
<script src="${base}/common/template/sports/hg/js/core.js" path="${base}"></script>
<script src="${base}/common/js/artTemplate/template.js"></script>
<script src="${base}/common/template/sports/hg/js/key.js"></script>
<script src="${base}/common/template/sports/hg/js/sports.js"></script>
<script src="${base}/common/template/sports/hg/js/sports-page.js?v=20170110"></script>

