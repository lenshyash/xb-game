<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/html" id="DATA_TR">
<div>
	<table id="game_table" cellspacing="0" cellpadding="0" class="game">
		<tr>
			<th class="time">时间</th>
			<th class="team">赛事</th>
			<th class="h_pd_ft">1:0</th>
			<th class="h_pd_ft">2:0</th>
			<th class="h_pd_ft">2:1</th>
			<th class="h_pd_ft">3:0</th>
			<th class="h_pd_ft">3:1</th>
			<th class="h_pd_ft">3:2</th>
			<th class="h_pd_ft">4:0</th>
			<th class="h_pd_ft">4:1</th>
			<th class="h_pd_ft">4:2</th>
			<th class="h_pd_ft">4:3</th>
			<th class="h_pd_ft">0:0</th>
			<th class="h_pd_ft">1:1</th>
			<th class="h_pd_ft">2:2</th>
			<th class="h_pd_ft">3:3</th>
			<th class="h_pd_ft">4:4</th>
			<th class="h_pd_ft">其它</th>
		</tr>
		{{if !games || games.length == 0}}
		<td colspan="20" class="no_game">您选择的项目暂时没有赛事。请修改您的选项或迟些再返回。</td> {{/if}}
		
		
		{{each games as val i}}
			<!-- 输出联赛 -->
			{{if i== 0 || games[i-1].league != val.league }}
				<tr>
					<td colspan="20" class="b_hline">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="legicon" onClick="showLeg('{{val.league}}')">
									<span class="showleg" name="{{val.league}}"> 
										{{if val.hidden == false}} <span id="LegOpen"></span> {{/if}} 
										{{if val.hidden}} <span id="LegClose"></span> {{/if}}
									</span>
								</td>
								<td onClick="showLeg('{{val.league}}')" class="leg_bar">{{val.league}}</td>
							</tr>
						</table>
					</td>
				</tr>
			{{/if}}
			
			<tr id="TR_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}}>
				<td rowspan="2" class="b_cen">{{$formatDate gameType val}}</td>
				<td rowspan="2" class="team_name">{{$teamName val.home}}<br>{{$teamName val.guest}}</td>
				<td class="b_cen">{{$showOdds val.ior_H1C0 'ior_H1C0' val.gid '1:0'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H2C0 'ior_H2C0' val.gid '2:0'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H2C1 'ior_H2C1' val.gid '2:1'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H3C0 'ior_H3C0' val.gid '3:0'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H3C1 'ior_H3C1' val.gid '3:1'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H3C2 'ior_H3C2' val.gid '3:2'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H4C0 'ior_H4C0' val.gid '4:0'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H4C1 'ior_H4C1' val.gid '4:1'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H4C2 'ior_H4C2' val.gid '4:2'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H4C3 'ior_H4C3' val.gid '4:3'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_H0C0 'ior_H0C0' val.gid '0:0'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_H1C1 'ior_H1C1' val.gid '1:1'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_H2C2 'ior_H2C2' val.gid '2:2'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_H3C3 'ior_H3C3' val.gid '3:3'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_H4C4 'ior_H4C4' val.gid '4:4'}}</td>
				<td rowspan="2" class="b_cen">{{$showOdds val.ior_OVH 'ior_OVH' val.gid '+4'}}</td>
			</tr>
			<tr  id="TR1_{{val.gid}}" {{if val.hidden}} style="display:none;"{{/if}}>
				<td class="b_cen">{{$showOdds val.ior_H0C1 'ior_H0C1' val.gid '0:1'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H0C2 'ior_H0C2' val.gid '0:2'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H1C2 'ior_H1C2' val.gid '1:2'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H0C3 'ior_H0C3' val.gid '0:3'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H1C3 'ior_H1C3' val.gid '1:3'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H2C3 'ior_H2C3' val.gid '2:3'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H0C4 'ior_H0C4' val.gid '0:4'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H1C4 'ior_H1C4' val.gid '1:4'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H2C4 'ior_H2C4' val.gid '2:4'}}</td>
				<td class="b_cen">{{$showOdds val.ior_H3C4 'ior_H3C4' val.gid '3:4'}}</td>
			</tr>
		{{/each}}
	</table>
	
</div>
	
	
</script>