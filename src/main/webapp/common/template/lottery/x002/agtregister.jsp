<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>注册--${_title }</title>
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link type="image/x-icon" rel="shortcut icon" href="${base }/images/favicon.ico">
</head>
<body>
<input type="hidden" value="${base }" id="baseD"/>
<div id="app" class="index">
	<!-- 头部 -->
	<link href="${base}/common/template/member/register/normalize.min.css" rel="stylesheet">
	<link href="${base}/common/template/member/register/app.bf5421c24be386a0903b19aaa3b38d3d.css" rel="stylesheet">
	<link type="image/x-icon" rel="shortcut icon" href="/images/logo.ico">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/template/member/register/registerDl.js"></script>
	<script type="text/javascript" src="${base}/common/modelCommon/js/reAjax.js"></script>

	<div class="container registerPage">
		<h3  class="reg_tit">注册</h3>
		<p  class="reg_small"></p>
		<ul  class="submitContent mglr30 regMain">
		<script>
			$(function(){
				$('#accountnoB').focus()
				 var browser = {
			            versions: function() {
			                var a = navigator.userAgent,
			                    b = navigator.appVersion;
			                return {
			                    trident: a.indexOf("Trident") > -1,
			                    presto: a.indexOf("Presto") > -1,
			                    webKit: a.indexOf("AppleWebKit") > -1,
			                    gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
			                    mobile: !!a.match(/AppleWebKit.*Mobile.*/),
			                    ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
			                    android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
			                    iPhone: a.indexOf("iPhone") > -1,
			                    iPad: a.indexOf("iPad") > -1,
			                    webApp: a.indexOf("Safari") == -1
			                }
			            }(),
			            language: (navigator.browserLanguage || navigator.language).toLowerCase()
			        };
				 if (browser.versions.mobile ) {
					 location.href = getLinkCode()

			     }
			})
			function getLinkCode(){
				var linkCode;
				var url
				arrcookie = document.cookie.split("; ");
				$.each(arrcookie,function(index,item){
					if(item.indexOf('linkCode') >= 0){
						linkCode = item.substring(9)
						url = '${base}/mobile/v3/regpage.do?linkCode='+linkCode
					}
				})
				return url;
			}
		</script>
			<li >
				<span >账 号：</span>
				<input type="text" id="accountnoB" class="userInput" placeholder="用户名">
			</li>
			<li >
				<span >设置密码：</span>
				<input type="password" id="password1B" class="userInput" placeholder="密码">
			</li>
			<li>
				<span >确认密码：</span>
				<input type="password" id="password2B" class="userInput" placeholder="密码">
			</li>
			<li style="position: relative;">
				<span >邀请码：</span>
				<input type="text" class="userInput" placeholder="请输入邀请码" name="promoCode" tabindex="12" id="promoCode">
				<span id="wrong" style="display:none;position:absolute;padding:5px 0 0 10px;text-align: left;"><img style="width:20px;height:20px!important;" src="/common/template/member/register/wrong.png"/></span>
				<span id="wrongok" style="display:none;position:absolute;padding:5px 0 0 10px;text-align: left;"><img style="width:20px;height:20px!important;" src="/common/template/member/register/ok.png"/></span>
			</li>
			<case id="reg_tb_after">

			</case>
			<li >
				<span style="position:relative;top: -13px;">验证码：</span>
				<input type="text" id="codeB" class="userInput" placeholder="验证码" style="width: 150px;">
				<img src="${base}/verifycode.do" id="validCode" style="cursor: pointer;" onclick="validCodes();"/>
			</li>
			<!---->
			<li  style="margin-top: 23px;">
			<span ></span>
			<a class="mainColorBtn submitBtnBig ClickShade" onclick="regDl();">注 册</a>
			<a target="_blank" href="${kfUrl }" class="forgetPwd">忘记密码?</a></li>
		</ul>
	</div>

	<!-- 底部 -->
</div>
<script>
function regDl(){
	 var account = $('#accountnoB').val();
     var password1 = $('#password1B').val();
     var password2 = $('#password2B').val();
     var code = $('#codeB').val();//验证码
     if(!account){
    	 alert("用户名不能为空!");
    	 return false;
     }
     if(!password1){
    	 alert("密码不能为空!");
    	 return false;
     }
     if(password1.length<6){
    	 alert("密码不能小于6位!");
    	 return false;
     }
     if(!password2){
    	 alert("确认密码不能为空!");
    	 return false;
     }
     if(password1 != password2){
    	 alert("两次密码不一致!");
    	 return false;
     }
     if(!code){
    	 alert("验证码不能为空!");
    	 return false;
     }
     var isParam = $('#isParam').val();
     var data = getCommitData();
     data["account"] = account;
     data["password"] = password1;
 	 data["rpassword"] = password2;
     data["verifyCode"] = code;
     	$.ajax({
     		url:"${base}/commonRegistertwo.do",
     		/*data : {
				data : JSON.encode(data)
			},*/
     		data:data,
     		type:"POST",
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = '${base}/index.do';
					}
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					validCodes();
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					validCodes();
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					validCodes();
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					validCodes();
					alert("没有权限");
				} else if (ceipstate == 6) {// 登录异常
					alert(data.msg);
					top.location.href = base + "/loginError.do";
				} else {
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = '${base}/daili';
					}
					fn.success(data, textStatus, xhr);
				}
			}
     	});
}

document.onkeydown = function(event_e){
    if(window.event) {
        event_e = window.event;
    }

    var int_keycode = event_e.charCode||event_e.keyCode;
    if( int_keycode == '13' ) {
    	reg();
        return false;
    }
}

function validCodes(){
 	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
 	$("#validCode").attr("src", url);
}
</script>

<script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
		<li>
			{{if tl.name!="推广码"}}
				<span>{{tl.name}}：</span>
			{{/if}}
				{{if tl.type==1}}
					{{if tl.name!="推广码"}}
						<input type="text" class="userInput" placeholder="请输入{{tl.name}}" name="{{tl.key}}" tabindex="12" id="{{tl.key}}">
					{{/if}}
				{{/if}}
				{{if tl.type==2}}
						<select id="{{tl.key}}" tabindex="8" class="userInput">
						{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
						{{/if}}
					</select>
				{{/if}}
				{{if tl.type==3}}
					{{if tl.sourceLst}}
					<p>
					{{each tl.sourceLst as rad i}}
						<input type="radio" class="userInput" name="{{tl.key}}" tabindex="9" value="{{i+1}}">{{rad}}</input>
					{{/each}}
					</p>
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<input type="checkbox" class="userInput" name="{{tl.key}}" placeholder="请输入{{tl.name}}" tabindex="10" value="{{i+1}}">{{chk}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<textarea id="{{tl.key}}"></textarea>
				{{/if}}
				{{if tl.type==6}}
					<input type="password" class="userInput" name="{{tl.key}}" tabindex="11" id="{{tl.key}}" placeholder="请输入{{tl.name}}">
				{{/if}}
				</li>
				{{$addValidateFiled tl}}
			</li>
		{{/each}}
</script>
</body>
</html>