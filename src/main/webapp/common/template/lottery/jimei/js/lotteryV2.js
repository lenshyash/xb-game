(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			},
			successIntercept:function(data){}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}
		if (opt.successIntercept) {
			fn.successIntercept = opt.successIntercept;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					layer.msg("[" + opt.url + "] 404 not found",{icon: 5,offset:['40%'],area:['auto','70px']});
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
			    	layer.msg("网络异常，"+errorThrown,{icon: 5,offset:['40%'],area:['auto','70px']});
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					layer.msg("后台异常，请联系管理员!",{icon: 5,offset:['40%'],area:['auto','70px']});
					fn.successIntercept(data);
				} else if (ceipstate == 3) { // 业务异常
					layer.msg(data.msg,{icon: 5,offset:['40%'],area:['auto','70px']});
					fn.successIntercept(data);
				} else if (ceipstate == 4 || ceipstate == 5) {// 未登陆异常 或 没有权限
					layer.alert(data.msg, {closeBtn: 0,icon: 5,offset:['40%']},function(){
						window.location.href='/';	//后期定义好更改
					});
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		return _ajax(_opt);
	};
})(jQuery);


var dataArray = [],selectedLotLastQuartz, trendsPeilv = 1 ,trendsPeilvLength = 0;
$(function(){
	
	LotV2Time.stop();
	$("#lotteryV2Sort ul li.active").bind("click",LotV2.initTriggerHandler).triggerHandler("click");
	$("#lotteryV2_navs a.ng-lotttery.selected").bind("click",LotV2.initTriggerHandlerV).triggerHandler("click");
	
	if(!!window.ActiveXObject){// 判断ie版本是否小于10
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(navigator.userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion<10){
        	var nai=$("#not-allow-old-ie").show();
        	nai.find('.ie-version').text(fIEVersion);
        	nai.find('#ie-agent').text(navigator.userAgent);
        	return;
        }
	}
	$("body").on("keydown","input[type=text]",function(e){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {  
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	var lotterySort = $("#lotteryV2Sort");
	lotterySort.on("click","ul li",function(){
		var t = $(this);
		t.addClass("active").siblings().removeClass("active");
		JIMEI.code = null;	//点击的时候需要清除code值，否则会出现无法切换问题
		$("#lotteryV2Sort ul li.active").bind("click",LotV2.initTriggerHandler).triggerHandler("click");

	})
	
	
	var $lottery=$("#lotteryV2_nav li"),$main_view = $("#_main_view"),$lotteryV=$("#lotteryV2_navs"),$V010=$("#lotteryV2_v010"),$skinPanel=$("#skinPanel");		//彩种点击事件
	$lottery.on("click",function(){
		var dataOpen = $(this).attr("data-open");
		if(dataOpen){
			layer.alert("该彩种尚未开盘，请耐心等待",{icon:4,offset:['40%']});
			return;
		}
		var code = $(this).find("a").attr("lot_code");
		$(this).addClass("active").siblings().removeClass("active");
		LotV2.plaName = $(this).find("h4").text();
		LotV2.plaCode = code;
		LotV2.plaLotType = $(this).find("a").attr("lot_type");
		LotV2.init();
	});
	
	$lotteryV.on("click","a",function(){//v010
		if($(this).hasClass("moreLottery")){	//点击更多的话不执行
			return false;
		}
		var code = $(this).attr("lot_code");
		$(this).addClass("selected").siblings().removeClass("selected");
		LotV2.plaName = $(this).text();
		LotV2.plaCode = code;
		LotV2.plaLotType = $(this).attr("lot_type");
		$main_view.removeAttr("style").css({'width':'1022px','float':'left',"height":"100%"});
		$main_view.parent().find('.queueV').show();
		$(".dsLotto-category-more a.moreLottery").removeClass("selected").find("span.ui-icon-group__txt").html('更多玩法');
		if($(this).hasClass("ng-binding")){
			$(".dsLotto-category-more .ui-popup__body a").removeClass("selected");
		}
		//trigger初始化
		LotV2.initV();
	}).on({
		mouseenter:function(){$lotteryV.find('.dsLotto-category-more').addClass("active");},
		mouseleave:function(){$lotteryV.find(".dsLotto-category-more").removeClass("active");}
	},".dsLotto-category-more").on("click",".dsLotto-category-more>.ui-popup__body>ul>li",function(){
		$(this).siblings("li").find('a').removeClass("selected");
		$lotteryV.find(".dsLotto-category-more").removeClass("active");
		var val = $(this).find("a").text();
		$lotteryV.find("span.ui-icon-group__txt").text(val).parent().addClass("selected");
		$lotteryV.find("a.ng-binding").removeClass("selected");
	});
	
	$V010.on("click",".groupList>li",function(){	//v010
		LotV2.clearDataArrayModal();
		$(this).addClass("active").siblings().removeClass("active");
		LotV2.plaGroupCode = $(this).find("a").attr("lot_code");
		LotV2._stId = $(this).find("a").attr("stid");
		LotV2.plaGroupName = $(this).find("a").text();
		LotV2.addContent(LotV2.plaGroupCode);
		$main_view.removeAttr("style").css({'width':'1022px','float':'left',"height":"100%"});
		$main_view.parent().find('.queueV').show();
		LotV2.flushTime();
	})
	
	//换色
	$skinPanel.on("mouseenter",function(){
		$(this).addClass("skinHover").find("ul").show();
	}).on("mouseleave",function(){
		$(this).removeClass("skinHover").find("ul").hide();
	}).on("click","ul>li>a",function(){
		var color = $(this).attr("back");
		switch(color){
		case "red":
			$("body").removeAttr("class").addClass("skin_red");
			break;
		case "blue":
			$("body").removeAttr("class").addClass("skin_blue");
			break;
		case "gray":
			$("body").removeAttr("class").addClass("skin_gray");
			break;
		}
	})
	
	$main_view.on("mouseenter",".moreGroup",function(){
		$(this).find("ul").show();
	}).on("mouseleave",".moreGroup",function(){
		$(this).find("ul").hide();
	}).on("click"," #lotteryV2_group>ul>li",function(){  //大类点击事件
		LotV2.clearDataArrayModal();
		$(this).addClass("active").siblings().removeClass("active");
		LotV2.plaGroupCode = $(this).find("a").attr("lot_code");
		if(LotV2.plaGroupCode == 'zhixuan' && LotV2.plaLotType == 14){
			//将直选的第一个小类添加到playCode中
			LotV2.isCheckedRadio = true;
		}
		LotV2._stId = $(this).find("a").attr("stid");
		LotV2.addContent(LotV2.plaGroupCode);
		$(".moreGroup>a").removeClass("activeMore").html('更多玩法<span class="caret"></span>');
		$(".moreGroup>ul>li").show();
	}).on("click",".moreGroup>ul>li",function(){	//大类下拉列表点击
		LotV2.clearDataArrayModal();
		var val = $(this).find("a").html();
		$(this).parent().siblings("a").addClass("activeMore").html(val+'<span class="caret"></span>');
		$(this).parent().hide();
		$(this).hide().siblings().show();
		LotV2.plaGroupCode = $(this).find("a").attr("lot_code");
		LotV2._stId = $(this).find("a").attr("stid");
		LotV2.plaGroupName = $(this).find("a").text();
		LotV2.addContent(LotV2.plaGroupCode);
		$("#lotteryV2_group>ul>li").removeClass("active");
	}).on("click","#playV2Silder ul>li.ng-isolate-scope",function(){		//小类点击事件
		LotV2.clearDataArrayModal();
		$(this).addClass("active").siblings().removeClass("active");
		LotV2.plaPlayCode = $(this).find("a").attr("lot_code");
		LotV2.plaPlayName = $(this).find("a").text();
		$main_view.find("#"+LotV2.plaPlayCode).addClass("active").siblings().removeClass("active");
		if($(this).find("a").siblings().is("input")){
			LotV2.minSelected = $(this).find("a").siblings("input[name$=minSelected]").val();
			LotV2.maxSelected = $(this).find("a").siblings("input[name$=maxSelected]").val();
		}
	}).on("click","#playRadio>ul>li input[type=radio]",function(){	//radio
		var $val = $(this).val();
		$main_view.find("#hexiao"+$val).show().siblings().hide();
	}).on("click","#radioTrue>thead>tr>th input[type=radio]",function(){
		var t = $(this),val = t.val();
		if(LotV2.plaGroupCode == 'zhixuan'){
			//11选5操作
			$("#zhixuan_11x5 table").hide();
			switch(val){
				case "zhixuanqianer":
					$("#zhixuan_11x5 table:eq(0),#zhixuan_11x5 table:eq(1)").show();
					break;
				case "zhixuanhouer":
					$("#zhixuan_11x5 table:eq(3),#zhixuan_11x5 table:eq(4)").show();
					break;
				case "zhixuanqiansan":
					$("#zhixuan_11x5 table:eq(0),#zhixuan_11x5 table:eq(1),#zhixuan_11x5 table:eq(2)").show();
					break;
				case "zhixuanzhongsan":
					$("#zhixuan_11x5 table:eq(1),#zhixuan_11x5 table:eq(2),#zhixuan_11x5 table:eq(3)").show();
					break;
				case "zhixuanhousan":
					$("#zhixuan_11x5 table:eq(2),#zhixuan_11x5 table:eq(3),#zhixuan_11x5 table:eq(4)").show();
					break;
				default:
					alert('参数错误拉~');
					break;
			}
		}
		LotV2.clearDataArrayModal();
		LotV2.plaPlayCode = $(this).val();
		var index = $(this).parent().parent().index();
		var $oddsId = $("#radioTrue>tbody>tr>td:eq("+index+") input[name$=oddsId]").val();
		var $rate = $("#radioTrue>tbody>tr>td:eq("+index+") input[name$=rate]").val();
		LotV2.minSelected = $("#radioTrue>tbody>tr>td:eq("+index+") input[name$=minSelected]").val();
		LotV2.maxSelected = $("#radioTrue>tbody>tr>td:eq("+index+") input[name$=maxSelected]").val();
		LotV2.isCheckedRadio = true;
		LotV2.plaPlayName = $(this).parent().text();
		$main_view.find("table>tbody.table-data>tr>td input[type=checkbox]").each(function(index,item){
			$(item).siblings("input[name$=rate]").val($rate);
			$(item).siblings("input[name$=oddsId]").val($oddsId);
		})
	}).on("keyup","input[name=presetMoney]",function(){
		LotV2.dataHandle.clearDataObj();
		var nowSelected = 0;	//初始化选中个数
		LotV2.presetMoney = $(this).val();
		$main_view.find(".bar-bet input[name=presetMoney]").val($(this).val());
		//此处需要判断，如果有选中的文本框则需要进行预设值添加
		$main_view.find("#lotteryV2_content table>tbody>tr>td").each(function(index,item){
			if($(item).hasClass("danger") && $(item).children().is("input[type=text]")){
				//如果选中且是文本框，金额则需更改
				$(item).find("input[type=text]").val(LotV2.presetMoney);
				LotV2.dataHandle.addDataObj($(item).find("input[type=text]"));
			}else if($(item).hasClass("danger") && $(item).children().is("input[type=checkbox]")){
				//否则如果是checkbox
				if(LotV2.plaGroupCode == 'zhixuan' && LotV2.plaLotType == 14){
				}else{
					nowSelected++;
					LotV2.nowChecked = nowSelected; 
					LotV2.dataHandle.addDataCheckboxObj($(item).find("input[type=checkbox]"));
				}
			}
		});
	}).on("keyup","#lotteryV2_content input[type=text]",function(){		//直接复制金额时，添加选中状态
		var val = $(this).parent().attr("add-color");
		if($(this).val() != null && $(this).val() != 0){
			$(this).parent().parent().find("td[add-color="+val+"]").addClass("danger");
			//如果金额不为空且不为0则添加到对象中
			LotV2.dataHandle.addDataObj($(this));
		}else{
			$(this).parent().parent().find("td[add-color="+val+"]").removeClass("danger");
			//如果删除金额则删除数组中相应的对象
			LotV2.dataHandle.removeDataObj($(this));
		}
	}).on("mouseenter","table>tbody>tr>td",function(event){
		var val = $(this).attr("add-color");
		$(this).parent().find("td[add-color="+val+"]").addClass("warning");
		
	}).on("click","table>tbody>tr>td>input[type=checkbox]",function(e){
		$(this).attr("checked",false);
	}).on("click","table>tbody>tr>td>input[type=text]",function(e){
		e.stopPropagation();	//阻止table中的input触发click事件
		if(LotV2.presetMoney != ""){
			$(this).val(LotV2.presetMoney);
			var val = $(this).parent().attr("add-color");
			$(this).parent().parent().find("td[add-color="+val+"]").addClass("danger");
			LotV2.dataHandle.addDataObj($(this));
		}
	}).on("click","table>tbody>tr>td div.dropdown",function(e){	//xy28有下拉列表，需要阻止事件
		e.stopPropagation();
		if($(this).hasClass("open")){
			$(this).removeClass("open");
		}else{
			$(this).addClass("open")
		}
	}).on("click","table>tbody>tr>td div.dropdown ul li",function(){
		var val = "";
		var corrNum = $(this).attr("correspondnum");
		var name = $(this).parent().parent().siblings("input[name$=name]").val();
		$(this).addClass("active").siblings("li").removeClass("active")
		$(this).parent().siblings("a").find("i").attr("class","ball-c ball-c-xs ng-binding ball-c"+corrNum).html($(this).find("a").text());
		//将选中结果添加到对应的参数进去
		LotV2.dataHandle.xy28SelectedVal($(this).parent().parent());
	}).on("mouseleave","table>tbody>tr>td",function(){
		$(this).parent().find("td").removeClass("warning");
	}).on("click",".resetBtn",function(){
		//重置所有的文本框值
		LotV2.resetBtn();
	}).on("click","#qianShiQi",function(){		//前十期开奖结果
		if($(this).parent().hasClass("open")){
			$(this).parent().removeClass("open");
		}else{
			LotV2.addReslut();
			$(this).parent().addClass("open");
		}
	}).on("click","#lotteryV2-result .close",function(){
		$(this).parent().parent().parent().removeClass("open");
	}).on("change","#lotteryV2_rule",function(){	//彩种规则
		var value = $(this).val();
		$main_view.find(".rule-content #LT"+value).show().siblings("").hide();
	}).on("change","#lotResultList",function(){		//彩种历史开奖记录
		var val = $(this).val();
		$("#lotResultListForm").submit();
	}).on("click",".chosseAwardShowType>button",function(){	//前十期开奖结果分类
		LotV2.resultShowType = $(this).attr("showType");
		LotV2.resultShowTitle = $(this).attr("showTitle");
		$(this).addClass("btn-primary").siblings().removeClass("btn-primary");
		$(".open>ul>li .showTitle").find("b[showtitle="+LotV2.resultShowTitle+"]").removeClass("hidden").siblings("b").addClass("hidden");
		LotV2.addReslut();
	}).on("click","#tmsxBaoZhu li.group",function(){	//包组
		var type = $(this).attr("type");
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			LotV2.resetBtn();
		}else{
			LotV2.resetBtn();
			$(this).addClass("active").siblings(".group").removeClass("active");
			var doc = $("#lotteryV2_content table>tbody>tr");
			switch(type){
				case "yeshou":
				case "jiaqin":
					var val = $(this).attr("val");
					val = val.replace("[","").replace("]","");
					LotV2._yeshouOrJiaqin(val);
					break;
				case "shuang":
					LotV2._danOrShuang(2);
					break;
				case "dan":
					LotV2._danOrShuang(1);
					break;
				default:
					break;
			}
		}
	}).on("click","#zodiacBaoZhu li.zodiac",function(){
		var sum = 0;
		var val = $(this).find("a").text();
		$("#zodiacBaoZhu li.zodiac").each(function(index,item){
			if($(item).hasClass("active")){sum++;}
		})
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			LotV2.mark.selectedFalseShengXiao(val);
		}else{
			if(sum>=2){
				layer.msg("只能包选两组生肖",{icon:5,offset:['40%'],area:['auto','70px']});
				return;
			}
			if(LotV2.isRadio && !LotV2.isCheckedRadio){
				layer.msg("请选择类型",{icon:5,offset:['40%'],area:['auto','70px']});
				return;
			}
			$(this).addClass("active");
			LotV2.mark.selectedTrueShengXiao(val);
		}
		//alert(sum);
	}).on("click",".lotteryV2Btn",function(){
//		alert("彩种code="+LotV2.plaCode + ",大类玩法code="+LotV2.plaGroupCode + ",小类玩法code=" + LotV2.plaPlayCode + ",彩种type=" + LotV2.plaLotType + ",彩种name=" + LotV2.plaName);
		//alert("大类名称="+LotV2.plaGroupName + "，小类名称=" + LotV2.plaPlayName);
		//alert("多选最少选择个数="+LotV2.minSelected + ",--最多选择个数=" +LotV2.maxSelected + ",--当前选中个数=" + LotV2.nowChecked);
		//alert("是否有radio=" + LotV2.isRadio + "有的话是否有选中=" + LotV2.isCheckedRadio);
		//LotV2.dataHandle.showDataObjInfo("数据：");
		if(LotV2.plaGroupCode == 'zhixuan' && LotV2.plaLotType == 14){
			//组装dataArray
			LotV2.minSelected = 0;
			if(!$("#_main_view input[name=presetMoney]").val()){
				layer.msg("请输入投注的金额",{icon:5,offset:['40%'],area:['auto','70px']});
				return false;
			}
			var arr = [],arr1 = [],arr2 = [],tableNum=0;
			$("#zhixuan_11x5 table:visible").each(function(i,j){
				j = $(j);tableNum++;
				j.find('tr td.danger input[name$=id]').each(function(a,x){
					x = $(x);
					switch(i){
					case 0:
						arr[a] = x.val();
						break;
					case 1:
						arr1[a] = x.val();
						break;
					case 2:
						arr2[a] = x.val();
						break;
				}
				})
			})
			if(tableNum == 2){
				if(arr.length <= 0 || arr1.length<= 0){
					layer.msg("下注拖胆号码错误",{icon:5,offset:['40%'],area:['auto','70px']});
					return false;
				}
			}else{
				if(arr.length <= 0 || arr1.length<= 0 || arr2.length<=0){
					layer.msg("下注拖胆号码错误",{icon:5,offset:['40%'],area:['auto','70px']});
					return false;
				}
			}
			if(tableNum == 2){
				var arrs = [arr,arr1];
			}else{
				var arrs = [arr,arr1,arr2];
			}
			console.info(arrs);
			LotV2.dataHandle.add11X5Obj(arrs);
		}
		if(!LotV2.isLogin){
			layer.msg("请先登录",{icon:5,offset:['40%'],area:['auto','70px']});
			//可能不需要提示，会直接跳转到登录页面。后续在根据情况修改
			return;
		}
		if(dataArray.length == 0){
			layer.msg("无效投注",{icon:5,offset:['40%'],area:['auto','70px']});
			return;
		}
		if(LotV2.isRadio && !LotV2.isCheckedRadio){
			layer.msg("请选择类型",{icon:5,offset:['40%'],area:['auto','70px']});
			return;
		}
		if(LotV2.minSelected != 0 && (LotV2.nowChecked-1) < LotV2.minSelected){
			layer.msg("最少选择" + LotV2.minSelected + "个",{icon:5,offset:['40%'],area:['auto','70px']});
			return;
		}
		if(LotV2.plaCode == null || LotV2.plaGroupCode == null || LotV2.plaLotType == null || LotV2._stId == null){
			layer.msg("系统错误，请联系管理员",{icon:5,offset:['40%'],area:['auto','70px']});
			return;
		}
		if(LotV2.minSelected == 0 || LotV2.nowChecked == 1){
			$("#dataArrayModal .modal-body").css({'max-height':'420px','overflow-y':'auto','padding-bottom':'0'});
		}else{
			$("#dataArrayModal .modal-body").removeAttr("style");
		}
		$("#dataArrayModal .modal-body").html(LotV2.template.tem_xiaZhuDetail());
		$("#dataArrayModal").modal("toggle");
	});
	
	
	var $modal = $("#dataArrayModal");
	$modal.bind("click",function(){
		LotV2.resetBtn();
	}).on("click",".modal-dialog",function(e){
		e.stopPropagation();	//阻止冒泡事件
	}).on("keyup","table>tbody>tr>td>input[type=text]",function(){
		if(!$(this).parent().parent().find("input[type=checkbox]").is(":checked")){
			return;	//如果未选中状态则不需要计算
		}
		if($(this).val() == "" || $(this).val() == 0){
			LotV2.allMoney = 0;
			LotV2.dataHandle.removeDataObj($(this));	//删除对象
		}else{
			LotV2.dataHandle.addDataObj($(this));
			LotV2.allMoney = parseFloat($(this).val());
		}
		var money = 0;
		$(this).parent().parent().siblings("tr").find("input[type=text]").each(function(index,item){
				if($(item).parent().parent().find("input:checkbox").is(":checked")){	//如果是选中状态才计算金额
					if($(item).val() == 0 || $(item).val() == ""){
						money = 0;
					}else{
						money = $(item).val();
					}
					LotV2.allMoney += parseFloat(money);
				}
		});
		if(dataArray.length == 0){
			$modal.find(".text-success").remove();
			LotV2.allMoney = 0;
		}else{
			if(LotV2.nowMoney < parseFloat(LotV2.allMoney)){
				if(!$modal.find("b").hasClass("text-success")){
					var tem = LotV2.template.tem_judgeMoney(LotV2.nowMoney);
					$modal.find("table>tfoot>tr>td b.all").after(tem);
				}else{
					$modal.find("table>tfoot>tr>td b.all1").text(-(LotV2.nowMoney - parseFloat(LotV2.allMoney)));
				}
			}else{
				LotV2.isOK = true;
				$modal.find(".text-success").remove();
				$modal.find("table>tfoot>tr>td b.all1").text(-(LotV2.nowMoney - parseFloat(LotV2.allMoney)));
			}
		}
		if(LotV2.minSelected == 0 || LotV2.nowChecked == 1 || LotV2.plaGroupCode == 'zuxuansan' || LotV2.plaGroupCode == 'zuxuanliu'){
		}else{
			LotV2.allMoney = LotV2.allMoney * LotV2.mark.arrayTotal();
		}
		$modal.find("table>tfoot b.all").text(LotV2.allMoney);
		$modal.find("table>tfoot b.arrLength").text(LotV2.dataHandle.getDataArrayLength());
		$modal.find("table>tfoot span.all1").text(LotV2.allMoney);
	}).on("click","table>tbody>tr>td>input[type=checkbox]",function(){
		var val = 0;
		if(!$(this).is(":checked")){	//false++
			LotV2.dataHandle.removeDataObj($(this).parent().parent().find("td>input[type=text]"));
			val = $(this).parent().parent().find("td>input[type=text]").val();
			if(val == 0){return;}
			LotV2.allMoney = LotV2.allMoney - parseFloat(val);
		}else{	//true--
			val = $(this).parent().parent().find("td>input[type=text]").val();
			if(val == 0){return;}
			LotV2.dataHandle.addDataObj($(this).parent().parent().find("td>input[type=text]"));
			LotV2.allMoney = LotV2.allMoney + parseFloat(val);
		}
		if(dataArray.length == 0){
			$modal.find(".text-success").remove();
			LotV2.allMoney = 0;
		}else{
			if(LotV2.nowMoney < parseFloat(LotV2.allMoney)){
				if(!$modal.find("b").hasClass("text-success")){
					var tem = LotV2.template.tem_judgeMoney(LotV2.nowMoney);
					$modal.find("table>tfoot>tr>td b.all").after(tem);
				}else{
					$modal.find("table>tfoot>tr>td b.all1").text(-(LotV2.nowMoney - parseFloat(LotV2.allMoney)));
				}
			}else{
				LotV2.isOK = true;
				$modal.find(".text-success").remove();
				$modal.find("table>tfoot>tr>td b.all1").text(-(LotV2.nowMoney - parseFloat(LotV2.allMoney)));
			}
		}
		if(LotV2.minSelected == 0 || LotV2.nowChecked == 1 || LotV2.plaGroupCode == 'zuxuansan' || LotV2.plaGroupCode == 'zuxuanliu'){
		}else{
			LotV2.allMoney = LotV2.allMoney * LotV2.mark.arrayTotal();
		}
		$modal.find("table>tfoot b.all").text(LotV2.allMoney.toFixed(2));
		$modal.find("table>tfoot b.arrLength").text(LotV2.dataHandle.getDataArrayLength());
		$modal.find("table>tfoot span.all1").text(LotV2.allMoney.toFixed(2));
	}).on("click",".resetBtns",function(){
		result=[];
		radioData = ''
		LotV2.resetBtn();
	}).on("mouseenter",".fushiZhu a",function(){
		$(this).siblings(".popover-group-data-div").show();
	}).on("mouseleave",".fushiZhu a",function(){
		$(this).siblings(".popover-group-data-div").hide();
	}).on("click",".submitBtn",function(){
		result=[];
		radioData = ''
		if(dataArray.length == 0){
			layer.alert("无效投注",{icon:2,offset:['40%']});
			return;
		}
		if(!LotV2.isOK){
			layer.alert("投注金额大于余额",{icon:2,offset:['40%']});
			return;
		}
		if(LotV2.isOpen){
			layer.alert("该期号已停止下注",{icon:2,offset:['40%']});
			return;
		}
		if(LotV2.allMoney == 0){
			layer.alert("投注金额不能为空",{icon:2,offset:['40%']});
			return;
		}
		//验证金额是否合法
		var max = LotV2.dataHandle.validateMaxMoney();
		if(max != 0){
			layer.alert(max,{icon:2,offset:['40%']});
			return;
		}
		var min = LotV2.dataHandle.validateMinMoney();
		if(min != 0){
			layer.alert(min,{icon:2,offset:['40%']});
			return;
		}
		//号码验证合法性
		if(!LotV2.mark._validateBuyMa()){
			layer.alert("下注号码不合法",{icon:2,offset:['40%']});
			return;
		}
		//禁用按钮
		$(this).attr("disabled","disabled");
		LotV2.ajax.lotteryV2_Sub();
	})

	var $result = $(".rule-content table");
	$result.on("click",".pull-left",function(){
		var lotId;
		var len = $(this).siblings("span").length;
		var ind = $(this).siblings("span:visible").index();
		if(ind == 1){
			lotId = $(this).siblings('span:eq('+(len-1)+')').attr("id");
			$(this).siblings('span:eq('+(len-1)+')').show().siblings("span").hide();
		}else{
			lotId = $(this).siblings('span:eq('+(ind-2)+')').attr("id");
			$(this).siblings('span:eq('+(ind-2)+')').show().siblings("span").hide();
		}
		$result.find(".tit").hide().siblings("."+lotId).show();
	}).on("click",".pull-right",function(){
		var lotId;
		var len = $(this).siblings("span").length;
		var ind = $(this).siblings("span:visible").index();
		if(ind == len){
			lotId = $(this).siblings('span:eq(0)').attr("id");
			$(this).siblings('span:eq(0)').show().siblings("span").hide();
		}else{
			lotId = $(this).siblings('span:eq('+(ind)+')').attr("id");
			$(this).siblings('span:eq('+ind+')').show().siblings("span").hide();
		}
		$result.find(".tit").hide().siblings("."+lotId).show();
	})
	
	
	var $header = $("#header"),$iframe = $(".vframe"),$mains=$("#main");
	$header.on("click","#V2Rule",function(){
		var url = JIMEI.base+'/lotteryV2/rule.do?lotCode='+LotV2.plaCode;
		LotV2.c001_iframEvent(url,false);
	}).on("click","#historyResult",function(){
		var url = JIMEI.base + '/lotteryV2/resultList.do?lotCode='+LotV2.plaCode;
		LotV2.c001_iframEvent(url,false);
	}).on("click","#notHistoryData",function(){
		var url = JIMEI.base + '/lotteryV2/history.do?orderStatus=1';
		LotV2.c001_iframEvent(url,true);
	}).on("click","#okHistoryData",function(){
		var url = JIMEI.base + '/lotteryV2/history.do';
		LotV2.c001_iframEvent(url,true);
	}).on("click","#updatePWord",function(){
		var url = JIMEI.base + '/center/member/meminfo/pwdpgc.do';
		LotV2.c001_iframEvent(url,true);
	});
	$mains.on("click","#memberCore",function(){
		var url = JIMEI.base + "/center/member/meminfo/cPage.do";
		LotV2.c001_iframEvent(url,true);
	}).on("click","#onlineRecharge",function(){
		var url = JIMEI.base + "/center/banktrans/deposit/cPage.do";
		LotV2.c001_iframEvent(url,true);
	}).on("click","#onlineDraw",function(){
		var url = JIMEI.base + "/center/banktrans/draw/drawpgs.do";
		LotV2.c001_iframEvent(url,true);
	}).on("click","#historyrecord",function(){
		var url = JIMEI.base + "/center/record/hisrecord/cPage.do?type=3";
		LotV2.c001_iframEvent(url,true);
	}).on("click","#cashRecord",function(){
		var url = JIMEI.base + "/center/record/mnyrecord/cashRecord.do";
		LotV2.c001_iframEvent(url,true);
	}).on("click","#scoreChangeMenu",function(){
		LotV2.c001_iframEvent(JIMEI.base + "/center/banktrans/exchange/cpPage.do",true);
	}).on("click","#memberMoneyChange",function(){
		LotV2.c001_iframEvent(JIMEI.base + "/center/banktrans/mnychg/chgmnyPages.do",true);
	}).on("click","#conversion",function(){
		LotV2.c001_iframEvent(JIMEI.base + "/center/banktrans/mnychg/conversion.do",true);
	})
	
});

function clickAddColor(){
	var val = $(this).attr("add-color");
	if($(this).parent().find("td[add-color="+val+"]").hasClass("danger")){
		$(this).parent().find("td[add-color="+val+"]").removeClass("danger");
		if($(this).parent().find("td[add-color="+val+"]").children().is("input[type=text]")){
			$(this).parent().find("td[add-color="+val+"]>input[type=text]").val("");	//清除预设金额
			//删除对象
			LotV2.dataHandle.removeDataObj($(this).parent().find("td[add-color="+val+"]>input[type=text]"));
		}else{
			$(this).parent().find("td[add-color="+val+"]>input[type=checkbox]").prop("checked",false);
			//删除checkbox对象
			if(LotV2.plaGroupCode == 'zhixuan' && LotV2.plaLotType == 14){
				$(this).parent().parent().parent().siblings('table:visible').each(function(i,j){
					j = $(j);
					j.find('tr td[add-color='+val+']').removeAttr('data-type');
					j.find('tr td[add-color='+val+']>input[type=checkbox]').attr('disabled',false);
				});
			}else{
				LotV2.dataHandle.removeDataCheckboxObj($(this).parent().find("td[add-color="+val+"]>input[type=checkbox]"));
			}
		}
	}else{
		if(LotV2.isRadio && !LotV2.isCheckedRadio){
			layer.msg("请选择类型",{icon:5,offset:['40%'],area:['auto','70px']});
			return;
		}
		if($(this).parent().find("td[add-color="+val+"]").children().is("input[type=text]")){
			$(this).parent().find("td[add-color="+val+"]").addClass("danger");
			if(LotV2.presetMoney != "" ){
				$(this).parent().find("td[add-color="+val+"]>input[type=text]").val(LotV2.presetMoney);	//添加预设金额
			}
			//添加对象
			LotV2.dataHandle.addDataObj($(this).parent().find("td[add-color="+val+"]>input[type=text]"));
		}
		//如果是checkbox则改为选中状态
		if($(this).parent().find("td[add-color="+val+"]").children().is("input[type=checkbox]")){
			//添加checkbox对象
			if(LotV2.plaGroupCode == 'zhixuan' && LotV2.plaLotType == 14){
				if($(this).parent().find("td[add-color="+val+"] input").is(":disabled")){return;}
				$(this).parent().find("td[add-color="+val+"]>input[type=checkbox]").prop("checked",true);
				$(this).parent().find("td[add-color="+val+"]").addClass("danger");
				//相同的号码其余的需要禁用
				$(this).parent().parent().parent().siblings('table:visible').each(function(i,j){
					j = $(j);
					//j.find('tr td[add-color='+val+']').attr('data-type',1);
					j.find('tr td[add-color='+val+']>input[type=checkbox]').attr('disabled',true);
				});
			}else{
				if(LotV2.maxSelected != 0 && LotV2.nowChecked > LotV2.maxSelected){
					return false;
				}
				$(this).parent().find("td[add-color="+val+"]>input[type=checkbox]").prop("checked",true);
				$(this).parent().find("td[add-color="+val+"]").addClass("danger");
				LotV2.dataHandle.addDataCheckboxObj($(this).parent().find("td[add-color="+val+"]>input[type=checkbox]"));
			}
		}
	}
}

//刷新余额
function refreshMoney(){
	var account = $(".accountName"),money = $(".nowMoney"),moneyOld = money.attr("money");
	var station_money = window.top.$("#user_money");
	$.ajax({
		url:JIMEI.base + "/meminfo.do",
		success:function(res){
			if(res.login){
				account.html(res.account);
				money.html(res.money);
				money.attr("money",res.money);
				station_money.text(res.money);
				LotV2.nowMoney = parseFloat(res.money);
				LotV2.isLogin = true;
				if(JIMEI.template == 'v2_1'){
					if(moneyOld != res.money){
						refreshDayMoney();
					}
				}
				$('#isLogin').hide();
				$("#_bbsportBalance strong",parent.document).text(res.money);		
			}else{
				account.html("游客");
				money.html("0");
				$('#isLogin').show();
			}
		}
//		,
//		beforeSend:function(){
//			money.html("加载中...");
//		} 
	})
}

/**
 * 今日输赢
 */
function refreshDayMoney(){
	if(!LotV2.isLogin){return;}
	var money = $(".dayRWMOney");
	$.ajax({
		url:JIMEI.base + "/lotteryV2/memdayInfo.do",
		success:function(res){
			money.html(res.dayMoney.toFixed(2));
		},
		beforeSend:function(){
			money.html("加载中...");
		} 
	})
}

//设定十秒刷新前十期开奖结果根据大类来执行
function flushTimeVal(){
	var $flushTime = $("#flushTime");
	LotV2.flushTimeNum = LotV2.flushTimeNum - 1;
	if(LotV2.flushTimeNum == 0){
		LotV2.flushTimeNum = 11;
		//LotV2.ajax.lotteryV2_Op(false);
		refreshMoney();	//十秒刷新余额
	}else{
		$flushTime.html(LotV2.flushTimeNum + "&nbsp;秒");
	}
}

var LotV2 = {
		placeholderPresetMoney:'{presetMoney}',	//金额占位符
		placeholderPresetMoney1:'{presetMoney1}',
		plaCode:null,		//彩种类型
		plaGroupCode:null,	//大类玩法
		plaPlayCode:null,	//小类code
		plaLotType:null,	//玩法类型
		plaName:null,		//彩种名称
		plaGroupName:null, 	//玩法大类名称
		plaPlayName:null,	//玩法小类名称
		_stId:null,
		_interval:"",	
		presetMoney:"",
		flushTimeNum:10,
		diffs :57600,
		minSelected:0,
		maxSelected:0,
		nowChecked:1,
		isLogin:false,
		isOK:true,
		isCheckedRadio:false,
		isOpen:false,	//该期号是否可以下注
		isException:false,	//是否有异常发生
		isRadio:false,
		allMoney:0,
		nowMoney:0,
		qiHao:"",
		resultShowType:"hm",
		resultShowTitle:"default",
		resultArray:[],
		//初始化
		init:function(){
			refreshMoney();	//刷新余额
			LotV2.addContent(null);	//彩种内容加载
			LotV2.flushTime();	//时间倒计时
			LotV2.clearDataArrayModal();	//清除数组
			LotV2.ajax.lotteryV2_Op(true);
			$(".newOrderData").html("");
		},
		initV:function(){
			refreshMoney();	//刷新余额
			LotV2.flushTime();
			LotV2.clearDataArrayModal();	//清除数组
			LotV2.ajax.lotteryV2_Op(true);
			LotV2.addContent(null);	//彩种内容加载
			LotV2.ajax.getGroupList();
		},
		oneGroupCodeInit:function(){
			LotV2.plaGroupCode = $(this).find("a").attr("lot_code");
			LotV2.plaGroupName = $(this).find("a").text();
			LotV2._stId = $(this).find("a").attr("stid");
			switch(JIMEI.template){
			case "v2":
				$("#_main_view #lotteryV2_group>ul>li.active").unbind("click",LotV2.oneGroupCodeInit);
				break;
			case "v2_1":
				$("#lotteryV2_v010>ul>li.active").unbind("click",LotV2.oneGroupCodeInit);
				break;
			}
		},
		onePlayCodeInit:function(){
			if($(this).find("a").siblings().is("input[type=hidden]")){
				LotV2.minSelected = $(this).find("a").siblings("input[name$=minSelected]").val();
				LotV2.maxSelected = $(this).find("a").siblings("input[name$=maxSelected]").val();
			}
			LotV2.plaPlayName = $(this).find("a").text();
			LotV2.plaPlayCode = $(this).find("a").attr("lot_code");
			$("#_main_view #playV2Silder ul>li.active").unbind("click",LotV2.onePlayCodeInit);
		},
		initTriggerHandler:function(){
			var initCode = JIMEI.code,t= $(this),viewId = t.find("a").attr("group_view");
			$("#lotteryV2_nav ol li").removeAttr("class");
			if(initCode){
				viewId = $("#lotteryV2_nav ol li a[lot_code="+initCode+"]").parent().parent().attr("group_view");
				t.removeClass("active").parent().find('a[group_view='+viewId+']').parent().addClass("active");
			}
			$("#lotteryV2_nav ol[group_view="+viewId+"]").removeAttr("style").siblings().hide();
			var $eq0 = $("#lotteryV2_nav li:visible").eq(0);
			if(initCode){
				$eq0 = $("#lotteryV2_nav li a[lot_code="+initCode+"]").parent();
			}
			$eq0.addClass("active").siblings().removeClass("active");
			LotV2.plaName = $eq0.find("h4").text();
			LotV2.plaCode = $eq0.find("a").attr("lot_code");
			LotV2.plaLotType = $eq0.find("a").attr("lot_type");
			LotV2.init();
			//initSwiper();
			$("#lotteryV2Sort ul li.active").unbind("click",LotV2.initTriggerHandler);
		},
		initTriggerHandlerV:function(){
			LotV2.plaName = $(this).text();
			LotV2.plaCode = $(this).attr("lot_code");
			LotV2.plaLotType = $(this).attr("lot_type");
			LotV2.initV();
			$("#lotteryV2_navs>a.selected").unbind("click",LotV2.initTriggerHandlerV);
		},
		playOK:function(){
			$("#dataArrayModal").modal("hide");
			LotV2.resetBtn();
			//生成最新注单
			LotV2.ajax.lotteryV2_newOrder();
			refreshMoney();
			$("#dataArrayModal .resetBtns").unbind("click",LotV2.playOK);
		},
		addContent:function($playCode){
			$.ajax({
				url:''+JIMEI.base+'/member/'+JIMEI.folder+'/module/lottery/jimei/lot_play_type_'+LotV2.plaLotType+"_"+JIMEI.folder+'.html?v='+JIMEI.caipiao_version+'',
				type:"get",
				dataType:"html",
				success:function(data){
					var result = LotV2.template.Tem_splitContent($playCode,data);
					switch(JIMEI.template){
					case "v2":
						if($playCode == null){
							if(LotV2.presetMoney != ""){
								result = result.replace(LotV2.placeholderPresetMoney,LotV2.presetMoney).replace(LotV2.placeholderPresetMoney1,LotV2.presetMoney);
							}else{
								result = result.replace(LotV2.placeholderPresetMoney,"").replace(LotV2.placeholderPresetMoney1,"");
							}
							$("#_main_view").html(result);
						}else{
							$("#_main_view").find("input[name=presetMoney]").val(LotV2.presetMoney == ""?"":LotV2.presetMoney);
							$("#_main_view #lotteryV2_content").html(result);
						}
						
						break;
					case "v2_1":
						if(LotV2.presetMoney != ""){
							result = result.replace(LotV2.placeholderPresetMoney,LotV2.presetMoney).replace(LotV2.placeholderPresetMoney1,LotV2.presetMoney);
						}else{
							result = result.replace(LotV2.placeholderPresetMoney,"").replace(LotV2.placeholderPresetMoney1,"");
						}
						$("#_main_view").html(result);
						// 针对a06602站点进行的单独操作，不需要时课删除
						if(window.location.host == 'kl008888.com' || window.location.host == 'kl008999.com'){
							if(LotV2.plaCode == "TXFFC"){
								if($("#lotteryV2_content").find('table:eq(0)').find('thead').find('th').text() == '总和'){
									//a06602关闭整合玩法
									$("#lotteryV2_content").find('table:eq(0)').hide()
								}
								$("#_main_view").find("#lotteryV2_content table>tbody>tr>td").each(function(index,item){
									if($(item).text().indexOf("总和") != -1){
										var add = $(item).attr('add-color')
										$(item).parents('table').find("td[add-color='"+add+"']").hide()
									}
								})
							}
						}
						// ------------
						break;
					}
					//内容加载完执行事件
					LotV2.findIsRadio();
					if(LotV2.plaCode == "CQXYNC"){
						//替换号码图片
						LotV2.cqxyncHaoMaImg();
					}
					switch(JIMEI.template){
					case "v2":
						$("#_main_view #lotteryV2_group>ul>li.active").bind("click",LotV2.oneGroupCodeInit).triggerHandler("click");
						break;
					}
					$("#_main_view #playV2Silder ul>li.active").bind("click",LotV2.onePlayCodeInit).triggerHandler("click");
//					动态赔率
					if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC' || trendsPeilv == 'AMLHC'){
						
					}else{
						$("#_main_view").find("#lotteryV2_content table>tbody>tr>td b").each(function(index,item){
							$(item).html(formatDecimal($(item).html()))
						})
					}
					LotV2.basicBtn();
					//  动态修改iframe宽度
					var height = $(".frame").height() + 300;
					$('#mainframe', parent.document).height(height)
					
				},
				beforeSend:function(){
					$("#_loading_wrap").show()
				},
				complete:function(){
					$("#_loading_wrap").hide();
				}
			})
		},
		addTitle:function(qiHao){
			var $titleId = $("#lotteryV2_Title");
			switch(JIMEI.template){
				case "v2":
					$titleId.html(qiHao);
					break;
				case "v2_1":
					$titleId.html('<div>'+LotV2.plaName+'</div><div>'+qiHao+'期开奖</div>');
					break;
			}
		},
		addLotNameImg:function(imgUrl){
			if(imgUrl){
				var url= imgUrl ;
			}else{
				var url=JIMEI.base+"/common/template/lottery/jimei/images/logo/{code}.png?v=2";
			}
			var $img = $("#lottery_logoImg");
			if(JIMEI.template == "v2"){
				url = url.replace("{code}",LotV2.plaCode);
				$img.attr("src",url);
			}
		},
		addKaiJiangHaoMa:function(last){
			var $haoMa = $("#lotteryV2_haoMa"),haoMa=last.haoMa;
			switch(LotV2.plaLotType){
				case "8":
					$haoMa.removeAttr("class").addClass("T_PK10 L_BJPK10");
					break;
				case "11":
					$haoMa.removeAttr("class").addClass("T_PECGG");
					break;
				case "15":
					$haoMa.removeAttr("class").addClass("T_3D");
					break;
				case "14":
					$haoMa.removeAttr("class").addClass("T_11X5");
					break;
				case "9":
					$haoMa.removeAttr("class").addClass("T_SSC L_CQSSC")
					break;
				case "12":
					if(LotV2.plaCode == 'CQXYNC'){
						$haoMa.removeAttr("class").addClass("T_CQXYNC");
					}else{
						$haoMa.removeAttr("class").addClass("T_KLSF L_GDKLSF");
					}
					break;
				case "6":
				case "66":
					$haoMa.removeAttr("class").addClass("T_HK6 L_HK6");
					break;
				case "10":
					$haoMa.removeAttr("class").addClass("T_K3 L_GXK3");
					break;
			}
			var temp = LotV2.template.lot_KaiJiangHaoMa(last);
			$haoMa.html(temp);
		},
		addReslut:function(){
			var tem = LotV2.template.tem_result();
			if(tem == ""){return;}
			if($("#lotteryV2-result ul li").hasClass("_isExist")){
				$("#lotteryV2-result ul").find("._isExist").remove();
			}
			$("#lotteryV2-result ul li.afterResult").after(tem);
		},
		removeBtnAndDisabledTrue:function(){
			//删除提交按钮和禁用文本框
			$("#_main_view").find("input[type=text]").prop("disabled",true).val("");
			$("#_main_view").find("button.lotteryV2Btn").remove();
			$("#_main_view").find("input[type=checkbox]").prop("disabled",true).attr("checked",false);
			$("#_main_view").find("input[type=radio]").prop("disabled",true).attr("checked",false);
			$("#_main_view").find("table>tbody>tr>td").removeClass("danger");
			$("#_main_view").off("click","table>tbody>tr>td",clickAddColor);
			LotV2.presetMoney = "";
			LotV2.isCheckedRadio = false;
			LotV2.dataHandle.clearDataObj();
			LotV2.hideAndShowOdds("hide");
		},
		addBtnAndDisabledFalse:function(){
			$("#_main_view").off("click","table>tbody>tr>td").on("click","table>tbody>tr>td",clickAddColor);
			if(!$("#_main_view").find("button").hasClass("lotteryV2Btn")){
				$("#_main_view").find("input[type=text]").prop("disabled",false);
				$("#_main_view").find("input[type=checkbox]").prop("disabled",false);
				$("#_main_view").find("input[type=radio]").prop("disabled",false);
				$("#_main_view").find("button.resetBtn").after('<button type="button" class="btn btn-danger ml10 ng-scope lotteryV2Btn">提交注单</button>');
				LotV2.hideAndShowOdds("show");
			}
		},
		hideAndShowOdds:function(type){
			$("#_main_view").find("#lotteryV2_content table>tbody>tr>td b").each(function(index,item){
				switch(type){
					case "show":
						var odds = "";
						if(LotV2.isRadio){
							//有多选框	选择器层级关系会影响到赔率的加载
							if($(item).siblings().is("input")){
								odds = $(item).siblings("input[name$=rate]").val();
							}
						}else{
							//无多选框
							if($(item).parent().next().children().is("input")){
								odds = $(item).parent().next().children("input[name$=rate]").val();
							}
						}
						if(odds != ""){
//							动态赔率
//							$(item).html(odds);
							if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC'|| trendsPeilv == 'AMLHC'){
								$(item).html(odds)
							}else{
								$(item).html(formatDecimal(odds))
							}
						}
						LotV2.isOpen = false;
					break;
					case "hide":
						$(item).html("--");
						LotV2.isOpen = true;
					break;
				}
			})
		},
		checkBoxDisabledTrue:function(){
			if($("#_main_view").find("#lotteryV2_content div").hasClass("active")){
				$("#_main_view").find("#lotteryV2_content div.active table>tbody>tr>td").each(function(index,item){
					if(!$(item).hasClass("danger")){
						$(item).find("input[type=checkbox]").attr("disabled",true);
					}
				});
			}else{
				$("#_main_view").find("#lotteryV2_content table>tbody.table-data>tr>td").each(function(index,item){
					if(!$(item).hasClass("danger")){
						$(item).find("input[type=checkbox]").attr("disabled",true);
					}
				});
			}
		},
		checkBoxDisabledFalse:function(){
			if($("#_main_view").find("#lotteryV2_content div").hasClass("active")){
				$("#_main_view").find("#lotteryV2_content div.active table>tbody>tr>td").each(function(index,item){
					if(!$(item).hasClass("danger")){
						$(item).find("input[type=checkbox]").attr("disabled",false);
					}
				});
			}else{
				$("#_main_view").find("#lotteryV2_content table>tbody.table-data>tr>td").each(function(index,item){
					if(!$(item).hasClass("danger")){
						$(item).find("input[type=checkbox]").attr("disabled",false);
					}
				});
			}
		},
		flushTime:function(){
			LotV2.clearFlushTime();
			LotV2._interval = setInterval(flushTimeVal,1000);
		},
		clearFlushTime:function(){
			window.clearInterval(LotV2._interval);
			flushTimeNum = 10;
			return;
		},
		findIsRadio:function(){
			if($("#radioTrue input").is("input[type=radio]")){
				LotV2.isRadio = true;
			}else{
				LotV2.isRadio = false;
				LotV2.isCheckedRadio = false;
				LotV2.maxSelected = 0;
				LotV2.minSelected = 0;
			}
			return;
		},
		_isMinGtNowChecked:function(){
			if(LotV2.minSelected == 0 || LotV2.nowChecked == 1){
				return false;
			}
			if(LotV2.minSelected < LotV2.nowChecked){
				//触发事件
				if(LotV2.plaGroupCode == 'zuxuansan' || LotV2.plaGroupCode == 'zuxuanliu'){
					return true;
				}
			}
			return false;
		},
		_yeshouOrJiaqin:function(val){
			var doc = $("#lotteryV2_content table>tbody>tr");
			var arr = val.split(",");
			$.each(arr,function(index,v){
				doc.find("td[add-color="+v+"]").addClass("danger");
				if(LotV2.presetMoney != "" && LotV2.presetMoney != 0){
					doc.find("td[add-color="+v+"] input[type=text]").val(LotV2.presetMoney);
				}
				LotV2.dataHandle.addDataObj(doc.find("td[add-color="+v+"] input[type=text]"));
			})
		},
		_danOrShuang:function(val){
			var doc = $("#lotteryV2_content table>tbody>tr");
            var date = new Date();
            var nowYear = date.getFullYear();  //表示年份的四位数字
            if (date.getTime() < (new Date('2019-2-5 00:00:00')).getTime()) {
                nowYear = 2018;
            }
			var arr = [0,2,4,6,8,10],arr1 = [1,3,5,7,9,11];
			if(nowYear%2==0){
				//正常
				if(val == 1){
					$.each(arr,function(index,v){
						doc.find("td[add-color="+v+"]").addClass("danger");
						if(LotV2.presetMoney != "" && LotV2.presetMoney != 0){
							doc.find("td[add-color="+v+"] input[type=text]").val(LotV2.presetMoney);
						}
						LotV2.dataHandle.addDataObj(doc.find("td[add-color="+v+"] input[type=text]"));
					})
				}else{
					$.each(arr1,function(index,v){
						doc.find("td[add-color="+v+"]").addClass("danger");
						if(LotV2.presetMoney != "" && LotV2.presetMoney != 0){
							doc.find("td[add-color="+v+"] input[type=text]").val(LotV2.presetMoney);
						}
						LotV2.dataHandle.addDataObj(doc.find("td[add-color="+v+"] input[type=text]"));
					})
				}
			}else{
				//取反
				if(val == 1){
					$.each(arr1,function(index,v){
						doc.find("td[add-color="+v+"]").addClass("danger");
						if(LotV2.presetMoney != "" && LotV2.presetMoney != 0){
							doc.find("td[add-color="+v+"] input[type=text]").val(LotV2.presetMoney);
						}
						LotV2.dataHandle.addDataObj(doc.find("td[add-color="+v+"] input[type=text]"));
					})
				}else{
					$.each(arr,function(index,v){
						doc.find("td[add-color="+v+"]").addClass("danger");
						if(LotV2.presetMoney != "" && LotV2.presetMoney != 0){
							doc.find("td[add-color="+v+"] input[type=text]").val(LotV2.presetMoney);
						}
						LotV2.dataHandle.addDataObj(doc.find("td[add-color="+v+"] input[type=text]"));
					})
				}
			}
		},
		basicBtn:function(){
			if(LotV2.isException){
				LotV2.removeBtnAndDisabledTrue();
			}
		},
		firstAddZero:function(a){	//首位补0
			if(parseInt(a) < 10){
				return "0"+parseInt(a);
			}else{
				return a;
			}
		},
		c001_iframEvent:function(u,flag){
			if(!LotV2.isLogin && flag){
				//未登录返回首页登录
				window.location.href=JIMEI.base;
				return;
			}
			LotV2.clearFlushTime();	//清除定时器
			$("#_main_view").html('<iframe id="frame" frameborder="0" name="frame" src="'+u+'"></iframe>');
			$("#_main_view").removeAttr('style').css({'width':'auto',"height":"100%"});
			$(".vframe").find(".queueV").hide();
		},
		lotQueue:function(arr){
			var temp = LotV2.template.lotQueue(arr);
			$(".qq-client-list").html(temp);
			//根据加载的内容高度重新定位div
			var divH = $(".qq-client-content").height();
			$('.qq-client-content').animate({
                marginTop:-divH/2,
            }, 800 );
		},
		cqxyncHaoMaImg:function(){
			var $id = $("#lotteryV2_content table tbody");
			if($id.find("i").length>0){
				$id.find("i").each(function(index,item){
					var haoMa = $(item).text();
					$(item).attr("class","ball-cqxync cqxync-i"+haoMa);
					$(item).text("");
				})
			}
		},
		resetBtn:function(){	//重置按钮
			$("#_main_view").find("#lotteryV2_content input[type=text]").val("");
			//$("#basic-addon1").next().val("");	
			$(".bar-bet").find("#basic-addon1").val("").next().val("");
			$("#_main_view").find("input[type=checkbox]").prop("checked",false).attr("disabled",false);
			$("#_main_view").find("table>tbody>tr>td").removeClass("danger");
			$("#dataArrayModal .modal-body").html("");
			$(".ssc_odds").text("--");
			$("#zodiacBaoZhu ul li").removeClass("active");
			$("#tmsxBaoZhu ul li").removeClass("active");
			$(".submitBtn").removeAttr("disabled");
			LotV2.dataHandle.clearDataObj();
			LotV2.isOK = true;
			LotV2.isOpen = false;
			LotV2.nowChecked = 1;
			LotV2.presetMoney = '';
		},
		clearDataArrayModal:function(){	//每个大类小类点击事件先初始化
			LotV2.resetBtn();
			LotV2.maxSelected = 0;
			LotV2.minSelected = 0;
			LotV2.isCheckedRadio = false;
			LotV2.plaPlayName = null;
			LotV2.plaPlayCode = null;
			LotV2.resultShowType = "hm";
			LotV2.resultShowTitle = "default";
			return;
		},
		_clearIntervalLastOne:function(){
			clearInterval(selectedLotLastQuartz);
		}
	};


LotV2.ajax = function(){
	return {
		lotteryV2_Op:function(flag){
			if(!LotV2.plaCode){
				LotV2.plaCode=$("#lotteryV2_navs").find("a.selected").attr("lot_code");
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2/lotV2Op.do?lotCode="+LotV2.plaCode,
				success:function(res){
					if(!res.success){
						//说明有异常
						LotV2.isException = true;
						LotV2.clearFlushTime();	//停止定时器
						layer.alert(res.msg,{icon:2,offset:['40%']},function(index){
							LotV2.removeBtnAndDisabledTrue();	//关闭下注功能
							LotV2Time.stop();
							$("#lotteryV2_Title").text("");
							$("#lotteryV2_haoMa").text("");
							layer.close(index);
						});
					}else{
//						动态赔率
						if(res.rate){
							trendsPeilv = res.rate
						}
						if(LotV2.plaCode =='LHC' || LotV2.plaCode == 'SFLHC' || LotV2.plaCode == 'TMLHC' || LotV2.plaCode == 'WFLHC' || LotV2.plaCode == 'HKMHLHC'|| LotV2.plaCode == 'AMLHC'){
							trendsPeilv = LotV2.plaCode
						}
						LotV2.isException = false;
						LotV2Time.stop();
						LotV2.addTitle(res.last.qiHao);
						LotV2.addKaiJiangHaoMa(res.last);
						LotV2.addLotNameImg(res.imgUrl);
						LotV2Time.addQihaoAndDownTime(res.current,flag);
						LotV2.resultArray = res.history;
						LotV2.addReslut();
						LotV2.lotQueue(res.ommitList);
					}
				},
				beforeSend:function(){
					if(flag){
						$("#_loading_wrap").show();
					}else{
						$("#flushTime").html('<img src="'+JIMEI.base+'/common/template/lottery/jimei/images/loaderc.gif" style="height:20px;">');
					}
				},
				complete:function(){
					$("#_loading_wrap").hide();
				}
			})
		},
		lotteryV2_Sub:function(){
			$(".radioMonery").each(function(index){
				dataArray[index].money = $(this).val();
		    });
			var data = {
					data:JSON.stringify(dataArray),
					lotCode:LotV2.plaCode,
					groupCode:LotV2.plaGroupCode,
					playCode:LotV2.plaPlayCode==null?LotV2.plaGroupCode:LotV2.plaPlayCode,
					lotType:LotV2.plaLotType,
					qiHao:LotV2.qiHao,
					stId:LotV2._stId
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2Bet/dolotV2Bet.do",
				data:data,
				success:function(res){
					if(res.success){
						layer.msg(res.msg,{icon:6,offset:['40%'],area:['auto','70px']});
						LotV2.resetBtn();
						$("#dataArrayModal .resetBtns").bind('click',LotV2.playOK).triggerHandler("click");
					}else{
						layer.alert(res.msg,{icon:2,offset:['40%']});
						$(".submitBtn").removeAttr("disabled");
						$('#dataArrayModal').modal('hide')
						LotV2.resetBtn();
					}
				}
			})
		},
		lotteryV2_newOrder:function(){
			var data = {
					qiHao:LotV2.qiHao,
					lotCode:LotV2.plaCode
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2Bet/doOrder.do",
				data:data,
				success:function(res){
					if(res.success){
						var arr = [];
						arr = res.doOrder;
						//添加相应的数据和位置
						var temp = LotV2.template.newOrder(arr);
						$(".newOrderData").html(temp);
					}
				},
				complete:function(){
					$("#lotteryV2_v010>ul>li.active").bind("click",LotV2.oneGroupCodeInit).triggerHandler("click");
				}
			});
		},
		getGroupList:function(){
			var data = {
					type:LotV2.plaLotType
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2/getGroupList.do",
				data:data,
				success:function(res){
					//获取大类玩法
					if(!res.success){
						layer.msg(res.msg,{icon:6,offset:['40%'],area:['auto','70px']});
						return;
					}
					var temp = LotV2.template.playGroupList(res.playGroup);
					$(".groupList").html(temp);
				}
			})
		},
		lotLastOne:function(qiHao){
			var data = {
					qiHao:qiHao,
					lotCode:LotV2.plaCode
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2/lotLast.do",
				data:data,
				type:'GET',
				success:function(res){
					if(res.success){
						//说明上一期已开奖，刷新页面即可，停止定时器
						LotV2._clearIntervalLastOne();
						//开奖结果暂时使用这种方法，后期修改
						LotV2.ajax.lotteryV2_Op();
						var temp = LotV2.template.lot_KaiJiangHaoMa(res.last);
						$("#lotteryV2_haoMa").html(temp);
						//更新前十期开奖结果
						$.each(LotV2.resultArray,function(index,item){
							if(item.qiHao == res.last.qiHao){
								item.haoMa = res.last.haoMa;
							}
						})
					}
				}
			})
		},
		groupViewDown:function(code){
			$.ajax({
				url:JIMEI.base + '/lotteryV2/groupViewDown.do',
				data:{lotCode:code},
				success:function(res){
					for(var key in res){
						LotV2Time.groupViewTimer(res[key],key);
					}
//					
//					$.each(res,function(i,j){
//						//LotV2Time.groupViewTimer(j.actionTime,j,j.serverTime,j.ago);
//					});
				}
			})
		}
	}
}();

var LotV2Time = function(){
	var a = null,b=null,v=0,g=0,t1,t2,t3,c,margin=0,diff=60*60*24,l,f=true;
	return obj = {
			addQihaoAndDownTime:function(cur,flag){
				var t = this
				if(!cur){
					t.setTextQiHao("暂时无开奖期号");
					t.stop();
					return this
				}
				c = cur;
				f=flag;
				t.playOrder(c.qiHao);
				t.start();
				return this
			},
			playOrder:function(q){
				if(!f){return;}
				$(".newOrderData").html("");
				LotV2.qiHao = q;
				if(LotV2.isLogin){	//登录才需要获取最新注单
					LotV2.ajax.lotteryV2_newOrder();
				}
			},
			timeFormat:function(e){
				return new Date(e.replace(/-/g,"/"));
			},
			downTime:function(){
				a = this.timeFormat(c.serverTime);
				b = this.timeFormat(c.activeTime);
				return (new Date(b-a).getTime())/1E3;
			},
			timeFormatV2:function(e){
				return $.formatDate("hh:mm:ss",new Date(e*1E3),LotV2Time.diffTime());
			},
			diffTime:function(){
				if((v-g) > diff && l==0){
					return Math.floor(v/diff);
				}
				return 0;
			},
			start:function(){
				var a = this
				v = a.downTime();
				g = c.ago;
				l = typeof(c.diffs)=="undefined"?-100:parseInt(c.diffs/1E3);
				this.stop();
				this.tick();
				clearInterval(t1);
				t1 = window.setInterval(function(){
					LotV2Time.tick();
				},1000);
			},
			setTextQiHao:function(t){
				$("#curIssueNumberId").html(t);
			},
			setTextV2_1QiHao:function(q){
				$("#curIssueNumberId").html(q + "&nbsp;&nbsp;期");
			},
			setTextO:function(t){
				$("#openOrCloseName").html("距投注截止还有");
				$("#cutdown-clock-base .ocplate").html(t);
				$("#cutdown-clock-base .ocplates").addClass("ng-hide").html("").siblings(".ocplate").removeClass("ng-hide");
				return this
			},
			setTextOs:function(t){
				$("#openOrCloseName").html("距投注开始还有");
				$("#cutdown-clock-base .ocplates").html(t);
				$("#cutdown-clock-base .ocplate").html("该期号已停止下注");
				$("#cutdown-clock-base .ocplate").addClass("ng-hide").siblings(".ocplates").removeClass("ng-hide");
				return this
			},
			stop:function(){
				clearInterval(t1);
				LotV2Time.setTextQiHao("");
				LotV2Time.setTextO("");
				LotV2Time.setTextOs("");
				return this
			},
			tick:function(){
				var a = this;
				var vg = v-g , state = c.state;LotV2.qiHao = c.qiHao;
				switch(JIMEI.template){
					case "v2":
						a.setTextQiHao(c.qiHao);
						break;
					case "v2_1":
						a.setTextV2_1QiHao(c.qiHao);
						break;
				}
				if(v === !0)return;
				if(v > 0 && v > g && state == 1){
					var html = a.timer(vg);
					switch(JIMEI.template){
					case "v2":
						a.setTextO(LotV2Time.assembledTime(html,state));
						break;
					case "v2_1":
	                    a.setTextO("距离封盘时间：" + html);
						break;
					}
					LotV2.addBtnAndDisabledFalse();
				}else{
					if(l > -2){
						var html = a.timer(l);
						switch(JIMEI.template){
						case "v2":
							a.setTextOs(LotV2Time.assembledTime(html,state));
							break;
						case "v2_1":
	                        a.setTextOs("距离开盘时间：" + html);
							break;
						}
						l--;
						if(l == -2){	//六合彩封盘1800秒
							LotV2.ajax.lotteryV2_Op(true);
							a.stop();
							return;
						}
					}else{
						var html = a.timer(v);
						switch(JIMEI.template){
						case "v2":
							a.setTextOs(LotV2Time.assembledTime(html,state));
							break;
						case "v2_1":
	                        a.setTextOs("距离开盘时间：" + html);
							break;
						}
					}
					LotV2.removeBtnAndDisabledTrue();
					if(v == 0){
						LotV2.ajax.lotteryV2_Op(true);
						a.stop();
						return;
					}
				}
				v--;
			},
			timer:function(intDiff){
				var day=0,hour=0,hours=0,minute=0,second=0,time_html='';//时间默认值		
				if(intDiff > 0){
					day = Math.floor(intDiff / (60 * 60 * 24));
					hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
					hours = Math.floor(intDiff / (60*60));
					minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
					second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
				}
				if (hours <= 9 ) hours = '0' + hours;
				if (minute <= 9) minute = '0' + minute;
				if (second <= 9) second = '0' + second;
				time_html = hours + ':' + minute + ':' + second;
				return time_html;
			},
			assembledTime:function(t,state){
				var temp = "";
				switch(LotV2.plaCode){
					case "LHC":
					case "FC3D":
					case "PL3":
					case "HKMHLHC":
					case "AMLHC":
						for(var i=0;i<t.length;i++){
							temp += '<li><span>'+t[i]+'</span></li>';
						}
						break;
					default:
						if(state==1){
							for(var i=3;i<t.length;i++){
								temp += '<li><span>'+t[i]+'</span></li>';
							}
						}else{
							for(var i=0;i<t.length;i++){
								temp += '<li><span>'+t[i]+'</span></li>';
							}
						}
						break;
				}
				return temp;
			},
			groupViewTimer:function(objValue,code){	//aT封盘时间，sT当前时间
				if(!objValue){
					$("#Timer_"+code).html("目前尚未开盘");
					$("#Timer_"+code).parent().parent().attr("data-open",false);
					return;
				}
				var aT = objValue.activeTime;
				var sT = objValue.serverTime;
				var ago = objValue.ago,state = objValue.state;
				var diff = LotV2Time.timeDiff(aT,sT);
				var t1,diffs=typeof(objValue.diffs)=="undefined"?-100:parseInt(objValue.diffs/1E3);;
				LotV2Time._groupTick(diff,code,ago,state,t1,diffs);
				clearInterval(t1);
				t1 = window.setInterval(function(){
					diff--;
					if(diffs > -2){diffs --;}
					LotV2Time._groupTick(diff,code,ago,state,t1,diffs);
				},1000);
			},
			timeDiff:function(o,n){
				var a = LotV2Time.timeFormat(o);
				var b = LotV2Time.timeFormat(n);
				return (new Date(a-b).getTime())/1E3;
			},
			_groupTick:function(diff,code,ago,state,t1,diffs){
				if(diff > 0 && diff > ago && state == 1){
					var vg = diff - ago;
					var html = LotV2Time.timer(vg);
					$("#Timer_"+code).html(html);
				}else{
					if(diffs == -2){
						LotV2.ajax.groupViewDown(code);
						clearInterval(t1);
					}
					$("#Timer_"+code).html("封盘");
				}
				if(diff==0){
					LotV2.ajax.groupViewDown(code);
					clearInterval(t1);
				}
			}
		}
}();

//模版Html
LotV2.template = function(){
	return {
		lot_KaiJiangHaoMa:function(last){
			var template = "",haoMa=last.haoMa;
			LotV2._clearIntervalLastOne();
			if(haoMa.indexOf("?") >=0){	//说明没开将
				$("#lotteryV2_haoMa").removeClass("status--off").addClass("status--on");
				//执行开奖定时器
				selectedLotLastQuartz = window.setInterval(function(){
					LotV2.ajax.lotLastOne(last.qiHao);
				},5000);
			}else{
				$("#lotteryV2_haoMa").removeClass("status--on").addClass("status--off");
			}
			var ma = haoMa.split(",");
			switch(LotV2.plaLotType){
			case "8":
			case "828":
				//模版规格<i class="pk10 ball-bjsc pk10_8">8</i>
				var dent = "";
				for(var i=0;i<ma.length;i++){
					dent = ma[i] == "?"?"b"+(i+1):"b"+parseInt(ma[i]);
					template += '<span class="item_'+i+'"><b class="'+dent+'"></b></span>';
				}
				return template;
			case "9":
			case "929":
			case "14":
			case "15":
				//模版规格<i class="redball">9</i>
				var dent = "";
				for(var i=0;i<ma.length;i++){
					dent = ma[i] == "?"?"b"+(i+1):"b"+parseInt(ma[i]);
					template += '<span class="item_'+i+'"><b class="'+dent+'">'+ma[i]+'</b></span>';
				}
				return template;
			case "12":
				//模版规格<i class="redball">9</i>
				var dent = "";
				if(LotV2.plaCode == 'CQXYNC'){
					for(var i=0;i<ma.length;i++){
						dent = ma[i] == "?"?"b"+LotV2.firstAddZero(i+1):"b"+LotV2.firstAddZero(parseInt(ma[i]));
						template += '<span class="item_'+i+'"><b class="'+dent+'"></b></span>';
					}
				}else{
					for(var i=0;i<ma.length;i++){
						dent = ma[i] == "?"?"b"+LotV2.firstAddZero(i+1):"b"+LotV2.firstAddZero(parseInt(ma[i]));
						template += '<span class="item_'+i+'"><b class="'+dent+'">'+ma[i]+'</b></span>';
					}
				}
				return template;
			case "10":
			case "102":
				//模版规格<i class="ball-1"></i>
				var dent = "";
				for(var i=0;i<ma.length;i++){
					dent = ma[i] == "?"?"b"+(i+1):"b"+parseInt(ma[i]);
					template += '<span class="item_'+i+'"><b class="'+dent+'"></b></span>';
				}
				return template;
			case "11":	//幸运28
				//模版规格<i class="fix-ddcp ball-c ball-c1">6</i> = <i class="fix-ddcp ball-c ball-c1"></i>
				var sum = 0;
				var dent = 0;
				for(var i=0;i<ma.length;i++){
					ma[i] == "?" ? sum="26" : sum+=parseInt(ma[i]);
					dent = ma[i] == "?"?"b"+(i+1):"b"+parseInt(ma[i]);
					if(i==2){
						template += '<span class="item_'+i+'"><b class="'+dent+'"></b></span>';
					}else{
						template += '<span class="item_'+i+'"><b class="'+dent+'"></b></span><span class="fuhao_pcegg">+</span>';
					}
				}
				template += '<span class="fuhao_pcegg">=</span><span class="item_'+i+'"><b class="b'+sum+'"></b></span>'
				return template;
			case "6":
			case "66":
				var dent = "";
				var shengxiao = "";
				for(var i=0;i<ma.length;i++){
					dent =ma[i] == "?"?'b50':"b"+LotV2.firstAddZero(parseInt(ma[i]));
					name =ma[i] == "?"?"?":LotV2.mark.zodiacName(ma[i],last.year);
					template += '<span class="item_'+i+'"><b class="'+dent+'"></b><i>'+name+'</i></span>';
					if(i == 5){
						template += '<span class="plus">+</span>';
					}
				}
				//模版规格<span class="ball-c ball-gree">16<span class="z">蛇</span></span>*6  <li class="redball-plus"> + </li>  <span class="ball-c ball-gree">16<span class="z">蛇</span></span>
				//如果是六合彩则需要添加class=result-lhc
				return template;
			}
		},
		Tem_splitContent:function($code,html){
			var start="<!--lotteryV2_first_start-->",stop="<!--lotteryV2_first_stop-->",muchStart="<!--lotteryV2_much_start-->",muchStop="<!--lotteryV2_much_stop-->",result="";
			if($code != null){
				start = "<!--lotteryV2_" + $code + "_start-->";
				stop = "<!--lotteryV2_" + $code + "_stop-->";
			}
			switch(JIMEI.template){
			case "v2":
				if(html.split(muchStart).length > 0 && $code == null){//
					result = result + html.split(muchStart)[0];
				}
				break;
			case "v2_1":
				if(html.split(muchStart).length > 0){//
					result = result + html.split(muchStart)[0];
				}
				break;
			}
			if(html.split(start).length > 0){
				html = html.split(start)[1];
				if(html != null && html.split(stop).length > 0){
					result = result + html.split(stop)[0];
				}
			}
			switch(JIMEI.template){
			case "v2":
				if(html.split(muchStop).length > 0 && $code == null){ // && $code == null
					result = result + html.split(muchStop)[1];
				}
				break;
			case "v2_1":
				if(html.split(muchStop).length > 0){ // && $code == null
					result = result + html.split(muchStop)[1];
				}
				break;
			}
			return result;
		},
		tem_judgeMoney:function(nowMoney){
			LotV2.isOK = false;
			var template = '<b class="text-success">( 投注金额超出余额<span class="all1">'+(-(parseFloat(nowMoney)-parseFloat(LotV2.allMoney)))+'</span>圆,无法进行下注)'
			+'<button onclick="refreshMoney()" style="color:#333;" class="ng-binding">刷新余额</button></b>'
			return template;
		},
		tem_xiaZhuDetail:function(){
			LotV2.allMoney = 0; //防止其他地方未初始化
			if(dataArray.length == 0){
				return false;
			}
			var template = "",length= dataArray.length;
			var name = LotV2.plaGroupName;
			if(LotV2.plaPlayName != null){
				name = name + "--" + LotV2.plaPlayName;
			}
			template = '<table class="table table-bordered table-bet mb0"><thead><tr><th width="120">玩法类别</th><th width="180">号码</th><th width="80">赔率</th><th>金额</th><th width="100">确认</th></thead><tbody>';
			$.each(dataArray,function(index,item){
				if(typeof item.name == 'object' && item.name.constructor == Array){
					item.name = item.name.join(",");
				}
				template += '<tr class="ng-scope"><td class="ng-binding">'+name+'</td><td class="ng-binding">'+item.name+'';
					if(LotV2.minSelected != 0 && LotV2.maxSelected != 0){
						if(LotV2.plaGroupCode == 'zuxuansan' || LotV2.plaGroupCode == 'zuxuanliu'){
							//var a = LotV2.mark.arrayTotal();
							//item.rate = (parseFloat(item.rate) / a).toFixed(2);
							var sel = LotV2.plaPlayCode + (LotV2.nowChecked-1);
							item.rate = $("input[selected="+sel+"]").val();
							item.oddsId = $("input[selected="+sel+"]").attr("sid");
							LotV2.allMoney = parseFloat(item.money);
							if(item.money == 0){length = length - 1;}
						}else{
							template +=LotV2.template.tem_fushiZhu();
							length = parseFloat(item.money)==0?0:LotV2.mark.arrayTotal();
							LotV2.allMoney = length * parseFloat(item.money);
						}
					}else{
						LotV2.allMoney += parseFloat(item.money);
						if(item.money == 0){length = length - 1;}
					}
					if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC' || trendsPeilv == 'AMLHC'){
						
					}else{
						item.rate = formatDecimal(item.rate);
					}
				template += '</td><td class="ng-binding"><b class="text-danger ng-binding">'+item.rate+'</b></td>'
					+'<td class="ng-binding"><input type="text" maxlength="6" class="form-control ng-valid ng-dirty ng-valid-parse ng-touched radioMonery" value='+item.money+' />'
					+'<input type="hidden" name="rate" value='+item.rate+'>'
					+'<input type="hidden" name="oddsId" value='+item.oddsId+'>'
					+'<input type="hidden" name="name" value='+item.name+'>'
					+'<input type="hidden" name="max" value='+item.max+'>'
					+'<input type="hidden" name="min" value='+item.min+'>'
					+'<input type="hidden" name="id" value='+item.id+'></td>'
					+'<td class="ng-binding"><input type="checkbox" class="ng-pristine ng-untouched ng-valid" checked="checked" value='+item.id+'/></td>'
					+'</tr>';
			});
			template += '<tfoot><tr><td colspan="1">组数：<b class="text-danger arrLength">'+length+'</b></td><td colspan="4">总金额：<b class="text-danger all">'+LotV2.allMoney+'</b>'
            var yjf = $('.danweiIs').val();
			if(LotV2.nowMoney<parseFloat(LotV2.allMoney)){
				LotV2.isOK = false;	//false无法下注
				template += '<b class="text-success">( 投注金额超出余额<span class="all1">'+(-(LotV2.nowMoney-parseFloat(LotV2.allMoney)))+'</span>圆,无法进行下注)'
				+'<button onclick="refreshMoney()" style="color:#333;" class="ng-binding">刷新余额</button></b>&nbsp;&nbsp;'
				if(yjf == '111'){
                    template +='<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元<input onclick="radioMoney()" name="money" type="radio"  value="2" />角<input onclick="radioMoney()" value="3" name="money" type="radio" />分';
                } else if(yjf == '110'){
                    template +='<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元<input onclick="radioMoney()" name="money" type="radio"  value="2" />角';
                }
				+'<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元';
			}else{
				template += '&nbsp;&nbsp;'
				if(yjf == '111'){
                    template +='<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元<input onclick="radioMoney()" name="money" type="radio"  value="2" />角<input onclick="radioMoney()" value="3" name="money" type="radio" />分';
                } else if(yjf == '110'){
                    template +='<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元<input onclick="radioMoney()" name="money" type="radio"  value="2" />角';
                }
				+'<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元';
				LotV2.isOk=true;
			}
			template += '</td></tr></tfoot></tbody></table>';
			return template;
		},
		tem_fushiZhu:function(){
			if(LotV2.minSelected == 0 || LotV2.nowChecked == 1){
				return;
			}
			var template = "";
			var obj;
			var allArr;
			template = '<div style="position:relative;" class="ng-binding fushiZhu">'
				+'复式『'+LotV2.mark.arrayTotal()+'组』<a href="javascript:void(0);">查看明细</a><div class="popover-group-data-div ng-binding" style="display: none;">';
				$.each(dataArray,function(index,item){
					obj = LotV2.dataHandle.getDataObj(item.id);
				});
				if(LotV2.plaPlayCode == 'xuanerlianzhi' || LotV2.plaPlayCode == 'xuansanqianzhi' || LotV2.plaGroupCode == 'zhixuan'){
					allArr = LotV2.mark.combines(obj[0].name);
				}else{
					allArr = LotV2.mark.combine(obj[0].name);
				}	
				$.each(allArr,function(i,item){
					template += '<span class="popover-group-data-content">'+(i+1)+'.&nbsp;<span class="popover-group-data-content-num">'+this+'</span></span>'
				});
				template += '</div></div>';
				return template;
		},
		tem_result:function(){
			var template = "",haoMaArray=[],sum=0,zhi=[1,2,3,5,7],he=[0,4,6,8,9];
			if(LotV2.resultArray == null){
				return template;
			}
			switch(LotV2.plaLotType){
				// PK10
				case "8":
				case "828":
					$.each(LotV2.resultArray,function(index,item){
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px;display:flex;align-items: center" href="javascript:void(0)">'
							+'<span class="ng-binding" style="width:100px;">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
							switch(LotV2.resultShowType){
								case "hm":
									for(var i=0;i<haoMaArray.length;i++){
										var dent = haoMaArray[i] == "?"?"-":"pk10_"+parseInt(haoMaArray[i]);
										if(dent == "-"){
											template += '<i class="ball-bjsc '+dent+'">-</i>';
										}else{
											template += '<i class="pk10 ball-bjsc '+dent+'"></i>';
										}
									}
									break;
								case "gyh":
									var dxdsSetting = $("#dxdsSetting").val()
									console.log(dxdsSetting)
									var gyhHaoma = 0
									for(var i=0;i<2;i++){
										var dent = haoMaArray[i] == "?"?"-":"pk10_"+parseInt(haoMaArray[i]);
										if(dent == "-"){
											template += '<i class="ball-bjsc '+dent+'">-</i>';
										}else{
											gyhHaoma+=parseInt(haoMaArray[i])
										}
									}
									template += '<span style="font-size:18px;display: inline-block;width: 20px;">'+gyhHaoma+'</span>';
									var ma = gyhHaoma == "?"?"-":gyhHaoma >= 11?"大":"小";
									if(ma == "?"){
										dent = "red";
										mar = "bjsc-margin";
									}else if(ma == "大"){
										if(gyhHaoma == 11){
											if(dxdsSetting == 'draw'){
												ma = '和'
											}else if(dxdsSetting == 'dadan'){
												ma = '大'
											}else if(dxdsSetting == 'dahe'){
												ma = '大'
											}else if(dxdsSetting == 'xiaohe'){
												ma = '小'
											}else if(dxdsSetting == 'hedan'){
												ma = '和'
											}else if(dxdsSetting == 'xiaodan'){
												ma = '小'
											}
										}
										dent = "#2400fc";
									}else{
										dent = "#009024";
									}
									template += '<label class="bjscTitleAward '+mar+'" ><font color="'+dent+'">'+ma+'</font></label>'
									var ma2 = gyhHaoma == "?"?"-":gyhHaoma%2==0?"双":"单";
									if(ma2 == "?"){
										dent = "blue";
										mar = "bjsc-margin";
									}else if(ma=="单"){
										if(gyhHaoma == 11){
											if(dxdsSetting == 'draw'){
												ma="和"
											}else if(dxdsSetting == 'dadan'){
												ma="单"
											}else if(dxdsSetting == 'dahe'){
												ma="和"
											}else if(dxdsSetting == 'xiaohe'){
												ma="和"
											}else if(dxdsSetting == 'hedan'){
												ma="单"
											}else if(dxdsSetting == 'xiaodan'){
												ma="单"
											}
										}

										dent = "#b448d8";
									}else{
										dent = "red";
									}
									template += '<label class="bjscTitleAward '+mar+'" ><font color="'+dent+'">'+ma2+'</font></label>'
									break;
								case "dx":
									for(var i=0;i<haoMaArray.length;i++){
										var dent = "";
										var mar = "bjsc-margin1";
										var ma = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i]) > 5?"大":"小";
										if(ma == "?"){
											dent = "red";
											mar = "bjsc-margin";
										}else if(ma == "大"){
											dent = "#2400fc";
										}else{
											dent = "#009024";
										}
										template += '<label class="bjscTitleAward '+mar+'"><font color="'+dent+'">'+ma+'</font></label>'
									}
									break;
								case "ds":
									for(var i=0;i<haoMaArray.length;i++){
										var dent = "";
										var mar = "bjsc-margin1"
										var ma = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i])%2==0?"双":"单";
										if(ma == "?"){
											dent = "blue";
											mar = "bjsc-margin";
										}else if(ma=="单"){
											dent = "#b448d8";
										}else{
											dent = "red";
										}
										template += '<label class="bjscTitleAward '+mar+'"><font color="'+dent+'">'+ma+'</font></label>'
									}
									break;
								case "lh":
                                    for(var i=0;i<haoMaArray.length;i++){
                                        var dent = haoMaArray[i] == "?"?"-":"pk10_"+parseInt(haoMaArray[i]);
                                        if(dent == "-"){
                                            template += '<i class="ball-bjsc '+dent+'">-</i>';
                                        }else{
                                            template += '<i class="pk10 ball-bjsc '+dent+'"></i>';
                                        }
                                    }
                                    template +='<br>'
									var lhStyle = 'width:67px;font-size:18px;margin-right:0px;'
                                    for(var i=0;i < 5 ;i++){
                                        var dent = haoMaArray[i] == "?"?"-":"pk10_"+parseInt(haoMaArray[i]);
                                        if(dent == "-"){
                                            template += '<i class="ball-bjsc '+dent+'">-</i>';
                                        }else{
                                            switch (i) {
												case  0:
                                                    if(parseInt(haoMaArray[0])>parseInt(haoMaArray[9])){
                                                        template += '<span class="ball-bjsc " style="'+lhStyle+' color:red;">龙</span>';
                                                    }
                                                    if(parseInt(haoMaArray[0])<parseInt(haoMaArray[9])){
                                                        template += '<span class=" ball-bjsc" style="'+lhStyle+'color:#000000;">虎</span>';
                                                    }
                                                    if(parseInt(haoMaArray[0])==parseInt(haoMaArray[9])){
                                                        template += '<span class=" ball-bjsc " style="'+lhStyle+'color:#000000;">和</span>';
                                                    }
													break
												case  1:
                                                    if(parseInt(haoMaArray[1])>parseInt(haoMaArray[8])){
                                                        template += '<span class="ball-bjsc " style="'+lhStyle+' color:red;">龙</span>';
                                                    }
                                                    if(parseInt(haoMaArray[1])<parseInt(haoMaArray[8])){
                                                        template += '<span class=" ball-bjsc" style="'+lhStyle+'color:#000000;">虎</span>';
                                                    }
                                                    if(parseInt(haoMaArray[1])==parseInt(haoMaArray[8])){
                                                        template += '<span class=" ball-bjsc " style="'+lhStyle+'color:#000000;">和</span>';
                                                    }
													break
												case  2:
                                                    if(parseInt(haoMaArray[2])>parseInt(haoMaArray[7])){
                                                        template += '<span class="ball-bjsc " style="'+lhStyle+' color:red;">龙</span>';
                                                    }
                                                    if(parseInt(haoMaArray[2])<parseInt(haoMaArray[7])){
                                                        template += '<span class=" ball-bjsc" style="'+lhStyle+'color:#000000;">虎</span>';
                                                    }
                                                    if(parseInt(haoMaArray[2])==parseInt(haoMaArray[7])){
                                                        template += '<span class=" ball-bjsc " style="'+lhStyle+'color:#000000;">和</span>';
                                                    }
													break
												case  3:
                                                    if(parseInt(haoMaArray[3])>parseInt(haoMaArray[6])){
                                                        template += '<span class="ball-bjsc " style="'+lhStyle+' color:red;">龙</span>';
                                                    }
                                                    if(parseInt(haoMaArray[3])<parseInt(haoMaArray[6])){
                                                        template += '<span class=" ball-bjsc" style="'+lhStyle+'color:#000000;">虎</span>';
                                                    }
                                                    if(parseInt(haoMaArray[3])==parseInt(haoMaArray[6])){
                                                        template += '<span class=" ball-bjsc " style="'+lhStyle+'color:#000000;">和</span>';
                                                    }
													break
												case  4:
                                                    if(parseInt(haoMaArray[4])>parseInt(haoMaArray[5])){
                                                        template += '<span class="ball-bjsc " style="'+lhStyle+' color:red;">龙</span>';
                                                    }
                                                    if(parseInt(haoMaArray[4])<parseInt(haoMaArray[5])){
                                                        template += '<span class=" ball-bjsc" style="'+lhStyle+'color:#000000;">虎</span>';
                                                    }
                                                    if(parseInt(haoMaArray[4])==parseInt(haoMaArray[5])){
                                                        template += '<span class=" ball-bjsc " style="'+lhStyle+'color:#000000;">和</span>';
                                                    }
													break
                                            }
                                        }
                                    }
                                    break;
							}
						template += '</b></a></li>';
					})
					//template += '<li class="_isExist"><a href="http://www.bwlc.gov.cn/bulletin/search.jsp?id=2" class="officialAward" target="_blank">官网开奖</a></li>';
					return template;
					// 11选5
				case "9":
				case "929":
				case "14":
					var className = "ssc_ball",max=4,sumMax=22;
					if(LotV2.plaLotType == "14"){
						className = "syx5_ball";
						max = 5;
						sumMax = 30;
					}
					$.each(LotV2.resultArray,function(index,item){
						haoMaArray = item.haoMa.split(",");
						sum = 0;
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding" style="width:120px;">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						for(var i=0;i<haoMaArray.length;i++){
							sum = haoMaArray[i] == "?"?"-":sum+=parseInt(haoMaArray[i]);
						}
						var sumName = "";
                        if(LotV2.resultShowType == "lh"){
                        	var num1 = haoMaArray[0], num2 = haoMaArray[haoMaArray.length - 1]
                            if(parseInt(num1)>parseInt(num2)){
                                sumName = '<font color="red"><b>(龙)</b></font>';
                            }
                            if(parseInt(num1)<parseInt(num2)){
                                sumName = '<font color="#000000"><b>(虎)</b></font>';
                            }
                            if(parseInt(num1)==parseInt(num2)){
                                sumName = '<font color="#000000"><b>(和)</b></font>';
                            }
                        } else {
							var color = "blue",dax="?",dds="?";
							if(sum != "-"){
								color = sum>sumMax?"#2400fc":"#009024";
								dax = sum>sumMax?"大":"小";
								dds = sum % 2 ==0 ?"双":"单";
							}
							sumName += '<font color="'+color+'" style="width: 19px;"><b>('+dax+'</b></font>';
							sumName += '<font color="'+color+'" style="width: 19px;"><b>'+dds+')</b></font>';
						}
						switch(LotV2.resultShowType){
							case "hm":
								for(var i=0;i<haoMaArray.length;i++){
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i]);
									template +='<label class="cqsscTitleAward2 '+className+'"><i style="margin-left: 3px;margin-right: 3px;" class="redball-sm">'+name+'</i></label>';
								}
								break;
							case "dx":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"#b448d8":parseInt(haoMaArray[i])>max?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i])>max?"大":"小";
									template +='<label class="cqsscTitleAward2 ssc-margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "ds":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"#b448d8":parseInt(haoMaArray[i])%2==0?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i])%2==0?"双":"单";
									template +='<label class="cqsscTitleAward2 ssc-margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "zh":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"#b448d8":$.inArray(parseInt(haoMaArray[i]),zhi)>=0?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"-":$.inArray(parseInt(haoMaArray[i]),zhi)>=0?"质":"合";
									template +='<label class="cqsscTitleAward2 ssc-margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "lh":
                                for(var i=0;i<haoMaArray.length;i++){
                                    var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i]);
                                    template +='<label class="cqsscTitleAward2 '+className+'"><i style="margin-left: 3px;margin-right: 3px;" class="redball-sm">'+name+'</i></label>';
                                }
								break;
							case "qp":
								if(haoMaArray[0] != "?"){
									var b,s,lh,nn,pk=LotV2.mark._dezhoupuke(haoMaArray);
								}else{
									var b=s=lh=nn=pk="-";
								}
								template +='<label class="cqsscTitleAward2 ssc-margin"><font color="#CD661D">'+b+'</font></label>'
									+'<label class="cqsscTitleAward2 ssc-margin"><font color="#3A5FCD">'+s+'</font></label>'
									+'<label class="cqsscTitleAward2 ssc-margin"><font color="#9932CC">'+lh+'</font></label>'
									+'<label class="cqsscTitleAward2 ssc-margin"><font color="#CD6090">'+nn+'</font></label>'
									+'<label class="cqsscTitleAward2 ssc-margin"><font color="green">'+pk+'</font></label>';
								break;
						}
						template +='<label class="cqsscTitleAward2 '+className+'"><i style="margin-left: 3px;margin-right: 3px;" class="redball-sm">'+sum+'</i>'+sumName+'</label>';
						template += '</b></a></li>';
					})
					//template += '<li class="_isExist"><a href="'+LotV2.template.official_href()+'" class="officialAward" target="_blank">官网开奖</a></li>';
					return template;
				case "10":
				case "102":
					var className = "";
					$.each(LotV2.resultArray,function(index,item){
						sum=0;
						var baozi = true
						if(item.haoMa.indexOf("?")>=0){className="";}else{className="vertical-align: top; margin-top: 10px;";}
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding" style="'+className+'">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						for(var i=0;i<haoMaArray.length;i++){
							if (haoMaArray[0] != haoMaArray[i]){
								baozi = false
							}
							sum = haoMaArray[i] == "?"?"-":sum+=parseInt(haoMaArray[i]);
							var dent = haoMaArray[i] == "?"?'-':parseInt(haoMaArray[i]);
							if(dent == "-"){
								template += '<i style="display:inline-block;float:none;margin:0 10px;">-</i>';
							}else{
								template += '<i style="display:inline-block;float:none;margin:0 3px;" class="ball-'+dent+'"></i>';
							}
						}
						if (baozi == true && $('#k3baoziDrawShow').val() =='on' ){ //豹子
							var name = sum == "-" ? "-":'子';
							var oddEvenNumber = sum == "-" ? "-":'豹'
						}else{
							var name = sum == "-"?"-":sum>10?"大":"小";
							var oddEvenNumber = sum%2 == 0 ?"双":'单'
						}

						template += '<label class="jstbTitleAward ng-binding" style="'+(name=="-"?'margin:0':'')+'">'+sum+'</label>'
						template += '<label class="jstbTitleAward ng-binding" style="'+(name=="-"?'margin:0':'')+'">'+oddEvenNumber+'</label>'
							+ '<label class="jstbTitleAward ng-binding" style="'+(name=="-"?'margin:0':'')+'"><font color="blue"><b>'+name+'</b></font></label></b></a></li>';
					})
					return template;
					// 低频彩
				case "15":
					$.each(LotV2.resultArray,function(index,item){
						sum=0;
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						for(var i=0;i<haoMaArray.length;i++){
							sum = haoMaArray[i] == "?"?"-":sum+=parseInt(haoMaArray[i]);
							var dent = haoMaArray[i] == "?"?'-':parseInt(haoMaArray[i]);
							template += '<label class="pl3_ball"><i style="display:inline-block;float:none;margin:0 3px;" class="redball-sm">'+dent+'</i></label>';
						}
						var name = sum == "-"?"-":sum>=14?"大":"小";
						var lhName = '';
                        if(parseInt(haoMaArray[0])>parseInt(haoMaArray[2])){
                            lhName = '<span style="color:red">龙</span>'
                        }
                        if(parseInt(haoMaArray[0])<parseInt(haoMaArray[2])){
                            lhName = '<span style="color:#000000">虎</span>'
                        }
                        if(parseInt(haoMaArray[0])==parseInt(haoMaArray[2])){
                            lhName = '<span style="color:#000000">和</span>'
                        }
						template += '<label class="jstbTitleAward ng-binding">'+sum+'</label>'
							+ '<label class="jstbTitleAward ng-binding"><font color="blue"><b>'+name+'</b></font></label>'
							+'<label class="jstbTitleAward ng-binding"><b>'+lhName+'</b></label></b></a></li>';
					})
					return template;
				case "11":
					$.each(LotV2.resultArray,function(index,item){
						sum = 0;
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						for(var i=0;i<haoMaArray.length;i++){
							sum = haoMaArray[i] == "?"?"?":sum+=parseInt(haoMaArray[i]);
							var dent = LotV2.mark._pceggColor(haoMaArray[i]);
							if(i==2){
								template += '<i class="ball-c fix-dd-award ball-c'+dent+' ng-binding">'+haoMaArray[i]+'</i>';
							}else{
								template += '<i class="ball-c fix-dd-award ball-c'+dent+' ng-binding">'+haoMaArray[i]+'</i>+';
							}
						}
						template +='=&nbsp;<i class="ball-c fix-dd-award ng-binding ball-c'+LotV2.mark._pceggColor(sum)+'">'+sum+'</i></b></a></li>';
					})
					return template;
				case "6":
				case "66":
					$.each(LotV2.resultArray,function(index,item){
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding" style="width:85px;">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						switch(LotV2.resultShowType){
							case "hm":
								for(var i=0;i<haoMaArray.length;i++){
									template += '<label class="lhcTitleAward lhc-margin"><i class="redball ball-bjsc ">'+haoMaArray[i]+'</i></label>';
								}
								break;
							case "sx":
								for(var i=0;i<haoMaArray.length;i++){
									var name = haoMaArray[i] == "?"?"?":LotV2.mark.zodiacName(parseInt(haoMaArray[i]),item.year);
									template += '<label class="lhcTitleAward lhc-margin">'+name+'</label>';
								}
								break;
							case "dx":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"#b448d8":parseInt(haoMaArray[i])>24?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"?":parseInt(haoMaArray[i])>24?"大":"小";
									template += '<label class="lhcTitleAward lhc-margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "ds":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"#b448d8":parseInt(haoMaArray[i])%2==0?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"?":parseInt(haoMaArray[i])%2==0?"双":"单";
									template += '<label class="lhcTitleAward lhc-margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
						}
						template +='</b></a></li>';
					})
					return template;
					// 快乐十分
				case "12":
					$.each(LotV2.resultArray,function(index,item){
						sum=0;
						haoMaArray = item.haoMa.split(",");
						template += '<li class="ng-scope _isExist"><a style="padding-right: 5px" href="javascript:void(0)">'
							+'<span class="ng-binding" style="width:120px;">'+item.qiHao+'&nbsp;期</span><b class="text-danger ng-binding">'
						for(var i=0;i<haoMaArray.length;i++){
							sum = haoMaArray[i] == "?"?"-":sum += parseInt(haoMaArray[i]);
						}
						switch(LotV2.resultShowType){
							case "hm":
								for(var i=0;i<haoMaArray.length;i++){
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i]);
									if(LotV2.plaCode == "CQXYNC"){
										if(name != "-"){
											template += '<label class="gdklsfTitleAward2 cqxync_ball"><i class="ball-cqxync cqxync-i'+LotV2.firstAddZero(name)+' cqxyncText">'+name+'</i></label>';
										}else{
											template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font color="#009024">'+name+'</font></label>';
										}
									}else{
										template += '<label class="gdklsfTitleAward2 sfc_ball"><i class="redball">'+name+'</i></label>';
									}
								}
								break;
							case "ds":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"blue":parseInt(haoMaArray[i])%2==0?"red":"#b448d8";
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i])%2==0?"双":"单";
									template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "dx":
								for(var i=0;i<haoMaArray.length;i++){
									var dent = haoMaArray[i] == "?"?"red":parseInt(haoMaArray[i])>10?"#2400fc":"#009024";
									var name = haoMaArray[i] == "?"?"-":parseInt(haoMaArray[i])>10?"大":"小";
									template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "zfb":
								for(var i=0;i<haoMaArray.length;i++){
									var name = haoMaArray[i] == "?"?"-":LotV2.mark._sfc_zfb(parseInt(haoMaArray[i]));
									var dent = name == "?"?"red":name=="中"?"#CD6090":name=="发"?"blue":"#43CD80";
									template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
							case "fw":
								for(var i=0;i<haoMaArray.length;i++){
									var name = haoMaArray[i] == "?"?"-":LotV2.mark._sfc_dnxb(parseInt(haoMaArray[i]));
									var dent = name == "?"?"red":name=="东"?"#CD661D":name=="南"?"#3A5FCD":name=="西"?"#9932CC":"#CD6090";
									template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font color="'+dent+'">'+name+'</font></label>';
								}
								break;
                            case "lh":
                            	var lhstyle = 'width:60px;'
                                for(var i=0;i < 5;i++){
                                    var dent = haoMaArray[i] == "?"?"red":parseInt(haoMaArray[i])>10?"#2400fc":"#009024";
                                    if(dent == "-"){
                                        template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:red;">'+dent+'</font></label>';
                                    }else{
                                        switch (i) {
                                            case  0:
                                                if(parseInt(haoMaArray[0])>parseInt(haoMaArray[7])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:red;">龙</font></label>';
                                                }
                                                if(parseInt(haoMaArray[0])<parseInt(haoMaArray[7])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">虎</font></label>';
                                                }
                                                if(parseInt(haoMaArray[0])==parseInt(haoMaArray[7])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">和</font></label>';
                                                }
                                                break
                                            case  1:
                                                if(parseInt(haoMaArray[1])>parseInt(haoMaArray[6])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:red;">龙</font></label>';
                                                }
                                                if(parseInt(haoMaArray[1])<parseInt(haoMaArray[6])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">虎</font></label>';
                                                }
                                                if(parseInt(haoMaArray[1])==parseInt(haoMaArray[6])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">和</font></label>';
                                                }
                                                break
                                            case  2:
                                                if(parseInt(haoMaArray[2])>parseInt(haoMaArray[5])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:red;">龙</font></label>';
                                                }
                                                if(parseInt(haoMaArray[2])<parseInt(haoMaArray[5])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">虎</font></label>';
                                                }
                                                if(parseInt(haoMaArray[2])==parseInt(haoMaArray[5])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">和</font></label>';
                                                }
                                                break
                                            case  3:
                                                if(parseInt(haoMaArray[3])>parseInt(haoMaArray[4])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:red;">龙</font></label>';
                                                }
                                                if(parseInt(haoMaArray[3])<parseInt(haoMaArray[4])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">虎</font></label>';
                                                }
                                                if(parseInt(haoMaArray[3])==parseInt(haoMaArray[4])){
                                                    template += '<label class="gdklsfTitleAward2 hnklsf_margin"><font style="'+lhstyle+'color:#000000;">和</font></label>';
                                                }
                                                break
                                        }
                                    }
                                }
                                break;
							case "Zong":
								var dx="-",ds="-",wdx="-",lh="-",dent1=dent2=dent3=dent4="#3A5FCD",first=parseInt(haoMaArray[0]),last=parseInt(haoMaArray[haoMaArray.length-1]);
								if(sum != "-"){
									dx = sum>84?"大":"小";
									ds = sum%2==0?"双":"单";
									lh = first>last?"龙":"虎";
									wdx = sum%10>4?"尾大":"尾小";
									var dent1=sum>84?"#2400fc":"#009024";
									var dent2=sum%2==0?"red":"#b448d8";
									var dent3=first>last?"red":"#b448d8";
									var dent4=sum%10>4?"#2400fc":"#009024";
								}
								template += '<label class="gdklsfTitleAward2 hnklsf_margin1"><font color="'+dent1+'">'+dx+'</font></label>'
									+'<label class="gdklsfTitleAward2 hnklsf_margin1"><font color="'+dent2+'">'+ds+'</font></label>'
									+'<label class="gdklsfTitleAward2 hnklsf_margin1"><font color="'+dent4+'">'+wdx+'</font></label>'
									+'<label class="gdklsfTitleAward2 hnklsf_margin1"><font color="'+dent3+'">'+lh+'</font></label>';
								break;
						}
						template += '<label class="gdklsfTitleAward2 sfc_ball"><i class="redball">'+sum+'</i></label>';
						template +='</b></a></li>';
					})
					//template += '<li class="_isExist"><a target="_blank" class="officialAward" href="'+LotV2.template.official_href()+'">官网开奖</a></li>';
					return template;
				default:
					return template;
			}
		},
		official_href:function(){
			switch(LotV2.plaCode){
				case "CQSSC":
					return "http://www.cqcp.net/game/ssc/";
				case "TJSSC":
					return "http://www.tjflcpw.com";
				case "XJSSC":
					return "http://www.xjflcp.com/game/sscIndex";
				case "HNKLSF":
					return "http://www.hnflcp.com/";
				case "GDKLSF":
					return "http://kj.cjcp.com.cn/gdklsf/";
				case "CQXYNC":
					return "http://www.cp908.com";
				default:
					break;
			}
		},
		newOrder:function(arr){
			if(arr==null||arr.length==0){
				return "";
			}
			var sum=0,b=0,max=JIMEI.template=='v2'?'max-height:500px':'max-height:250px';
			var lotName = JIMEI.template=='v2'?$("#lotteryV2_nav li.active a").text():$("#lotteryV2_navs a.selected span").text();
			var template = '<div class="list-bet ng-scope"><div class="view-bet-list bet-periodNo"><div class="ng-binding">'+lotName+'<br>'+LotV2.qiHao+' 期  </div></div><div style="'+max+'; overflow-x: hidden;overflow-y:auto ">';
			$.each(arr,function(index,item){
				if(JIMEI.template=='v2'){
					if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC' || trendsPeilv == 'AMLHC'){}else{
						item.odds = formatDecimal(item.odds)
					}
					template += '<div class="view-bet ng-scope"><div class="view-bet-t  aside-block-top">'
						+'<p>注单号: <span style="color: green" class="ng-binding">'+item.orderId+'</span></p>'
						+'<p class="ng-binding"> '+item.playName+'&nbsp;'+item.haoMa+' @ <font class="text-danger ng-binding">'+item.odds+'</font></p>'
						+'<p class="ng-binding">下注金额: '+item.buyMoney+'</p>'
						+'<p class="ng-binding">可赢金额: '+((item.odds*item.buyMoney)-item.buyMoney).toFixed(2)+'</p></div></div>';
					}
				b++;
				sum+= item.buyMoney;
			})
			template += '<div style="background-color: #fff;margin-bottom: 10px;"><table class="table table-bordered table-bet">'
				+'<tbody><tr><td width="84px" style="text-align:right;">下注单数:</td><td style="font-weight: bold;" class="ng-binding">'+b+'笔</td>'
				+'</tr><tr><td style="text-align:right;">合计金额:</td><td style="font-weight: bold;" class="ng-binding">'+sum+'</td>'
				+'</tr><tr style="background:#e57388;"><td colspan="2" onclick="onclickDetail();">点击查看详情</td></tr>'
				+'</tbody></table></div></div></div>';
			return template;
		},
		playGroupList:function(arr){	//v010模版
			var template = "";
			var domainFolder = $("#domainFolder").val();
			$.each(arr,function(index,item){
				if(domainFolder == "a010101" && item.code == "xingyun28"){
					template += '<li class="ng-scope active"><a stid="'+item.stationId+'" lot_code="'+item.code+'" class="ng-binding">加拿大28</a></li>';
				}else if(index == 0){
					template += '<li class="ng-scope active"><a stid="'+item.stationId+'" lot_code="'+item.code+'" class="ng-binding">'+item.name+'</a></li>';
				}else{
					template += '<li class="ng-scope"><a stid="'+item.stationId+'" lot_code="'+item.code+'" class="ng-binding">'+item.name+'</a></li>';
				}
			})
			template += '<div class="clearfix"></div>';
			return template;
		},
		lotQueue:function(arr){
			var template = "";
			if(arr == null){return template;}
			$.each(arr,function(index,item){
				if(index == arr.length-1){
					template += '<div class="client-list" style="border-bottom:none;"><label>'+item.name+'-'+item.names+'</label><span>'+item.sortNum+'期</span></div>';
				}else{
					template += '<div class="client-list"><label>'+item.name+'-'+item.names+'</label><span>'+item.sortNum+'期</span></div>';
				}
			})
			return template;
		}
	}
}();

//数据处理
LotV2.dataHandle = function(){
	var param = "id";
	return {
		newObject:function(){		//创建对象
			return new Object();
		},
		addDataCheckboxObj:function(source){
			var obj = LotV2.dataHandle.newObject();
			var nameArr = new Array();
			if(LotV2.maxSelected != 0 && LotV2.nowChecked >= LotV2.maxSelected){
				LotV2.checkBoxDisabledTrue();
			}
			if(dataArray.length == 0){
				obj.money = LotV2.presetMoney==""?0:LotV2.presetMoney;
				obj.id = source.siblings("input[name$=id]").val();
				obj.oddsId = source.siblings("input[name$=oddsId]").val();
				obj.name = source.siblings("input[name$=name]").val();
				obj.rate = source.siblings("input[name$=rate]").val();
				obj.max = source.siblings("input[name$=max]").val();
				obj.min = source.siblings("input[name$=min]").val();
				obj.playCode = LotV2.plaPlayCode;
				if(LotV2.plaGroupCode == 'hx' || LotV2.plaGroupCode == 'wsl' || LotV2.plaGroupCode == 'lx'){
					obj.nowYear = source.siblings("input[name$=nowYear]").val();
				}
				dataArray.push(obj);
			}else{
				obj.id = source.siblings("input[name$=id]").val();
				obj.name = source.siblings("input[name$=name]").val();
				if(typeof(obj.id) != "undefined"){
					var obj1 = LotV2.dataHandle.getDataObj(obj.id),name;
					//rate = obj1[0].rate;
					name = obj1[0].name + "," + obj.name;
					if(isNaN(obj.name) || LotV2.plaGroupCode=='hx' || LotV2.plaGroupCode=='wsl' || LotV2.plaGroupCode == 'lx'){
						nowYear = obj1[0].nowYear;
						if(nowYear == 1){
							obj1[0].nowYear = 0;
							obj.oddsId = source.siblings("input[name$=oddsId]").val();
							obj1[0].oddsId = obj.oddsId;
						}
						obj1[0].name = name;
					}else{
						nameArr = name.split(",");
						obj1[0].name = nameArr.sort(function(a,b){
							return a-b;
						});
					}
				}
			}
			LotV2.nowChecked = LotV2.nowChecked + 1;
			if(LotV2._isMinGtNowChecked()){	//获取相应的赔率
				//var rate = LotV2.mark._zuxuansanOrliu(rate);
				var sel = LotV2.plaPlayCode+(LotV2.nowChecked-1);
				var rate = $("input[selected="+sel+"]").val();
				//alert(rate);
				if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC'|| trendsPeilv == 'AMLHC'){
					$(".ssc_odds").text(rate);
				}else{
					$(".ssc_odds").text(formatDecimal(rate));
				}
				//需要修改oddsId
			}
		},
		add11X5Obj:function(arrs){
			//console.info(doExchange(arrs));
			var resultArr = doExchange(arrs),index = $("#radioTrue th input:checked").attr('index'),$rate = $("#radioTrue td:eq("+index+") input[name$=rate]").val(),
			$oddsId = $("#radioTrue td:eq("+index+") input[name$=oddsId]").val();
			console.info(index);
			console.info($rate);
			console.info($oddsId);
			$.each(resultArr,function(i,j){
				console.info(j.join(','));
				obj = LotV2.dataHandle.newObject();
				obj.money = $("#_main_view input[name=presetMoney]").val();
				obj.id = i;
				obj.name = j.join(',');
				obj.rate = $rate;
				obj.oddsId = $oddsId;
				obj.playCode = $("#radioTrue th input:checked").val();
				dataArray.push(obj);
			})
		},
		addDataObj:function(source){	//添加对象值 source= $(this)本身
			var obj = LotV2.dataHandle.newObject();
			obj.money = source.val()==""?0:source.val();
			obj.id = source.siblings("input[name$=id]").val();
			obj.name = source.siblings("input[name$=name]").val();
			obj.oddsId = source.siblings("input[name$=oddsId]").val();
			obj.rate = source.siblings("input[name$=rate]").val();
			obj.max = source.siblings("input[name$=max]").val();
			obj.min = source.siblings("input[name$=min]").val();
			obj.playCode = LotV2.plaPlayCode;
			if(typeof(obj.id) == "undefined" ||typeof(obj.money) == "undefined" || typeof(obj.name)=="undefined"||typeof(obj.oddsId)=="undefined"||typeof(obj.rate)=="undefined"){
				return false;
			}
			LotV2.dataHandle.isExistDataObj(obj.id);
			dataArray.push(obj);
		},
		removeDataObj:function(source){
			var id = source.siblings("input[name$=id]").val();
			if(id != null && id != "undefined"){
				dataArray = LotV2.dataHandle.remove(id);
			}
		},
		removeDataCheckboxObj:function(source){
			var id = source.siblings("input[name$=id]").val();
			var name = source.siblings("input[name$=name]").val();
			var obj = LotV2.dataHandle.getDataObj(id);
			var newName = "",nameArr = new Array();
			if(obj != null){
				if(typeof obj[0].name == 'object' && obj[0].name.constructor == Array){
					var names = obj[0].name;
				}else{
					var names = obj[0].name.split(",");
				}
				if(names.length == 1){
					LotV2.resetBtn();
					return;		//如果长度为1的时候直接clear
				}
				for(var i=0;i<names.length;i++){
					if(names[i] != name){
						newName += names[i] + ",";
					}
				}
				
				//进行排序
				if(isNaN(obj.name)){
					obj[0].name=newName.substring(0,newName.length-1);
				}else{
					newName = newName.substring(0,newName.length-1);
					nameArr = newName.split(",");
					obj[0].name = nameArr.sort(function(a,b){
						return a-b;
					});
				}
				LotV2.nowChecked = LotV2.nowChecked - 1;
				LotV2.checkBoxDisabledFalse();
				if(LotV2._isMinGtNowChecked()){	//修改赔率
					//var rate = LotV2.mark._zuxuansanOrliu(obj[0].rate);
					var sel = LotV2.plaPlayCode+(LotV2.nowChecked-1);
					var rate = $("input[selected="+sel+"]").val();
					if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC' || trendsPeilv == 'AMLHC'){
						$(".ssc_odds").text(rate);
					}else{
						$(".ssc_odds").text(formatDecimal(rate));
					}
				}else{
					$(".ssc_odds").text("--");
				}
			}
		},
		editDataObj:function(id,money){
			var obj = LotV2.dataHandle.getDataObj(id);
			if(obj != null){
				obj[0].money = money;
			}
		},
		remove:function(value){	//数组，参数Id，Id=值
			return $.grep(dataArray,function(obj,i){
				return obj[param] != value;
			});
		},
		getDataArrayLength:function(){	//
			if(dataArray.length == 0){return 0;}
			var len = 0;
			if(LotV2.minSelected == 0 || LotV2.nowChecked == 1 || LotV2.plaGroupCode == 'zuxuansan' || LotV2.plaGroupCode == 'zuxuanliu'){
				$.each(dataArray,function(index,obj){
					if(obj.money == 0){
						len++;
					}
				})
				return dataArray.length - len;
			}else{
				return LotV2.mark.arrayTotal();
			}
		},
		getDataObj:function(value){
			return $.grep(dataArray,function(obj,i){
				return obj[param] == value;
			});
		},
		isExistDataObj:function(value){
			if(dataArray.length==0){return true;}
			$.each(dataArray,function(index,obj){
				if(obj.id==value){
					dataArray = LotV2.dataHandle.remove(value);
					return false;
				}
			});
			return true;
		},
		clearDataObj:function(){
			dataArray = [];
			LotV2.allMoney = 0;
		},
		validateMaxMoney:function(){
			var flag = 0;
			$.each(dataArray,function(index,obj){
				if(parseInt(obj.money) > parseInt(obj.max)){
					flag = '号码[<span style="color:red">'+obj.name+'</span>]投注金额大于['+obj.max+'元]无法下注';
					return false;
				}
			})
			return flag;
		},
		validateMinMoney:function(){
			var flag = 0;
			$.each(dataArray,function(index,obj){
				if(parseInt(obj.money) < parseInt(obj.min)){
					flag = '号码[<span style="color:red">'+obj.name+'</span>]投注金额小于['+obj.min+'元]无法下注';
					return false;
				}
			})
			return flag;
		},
		xy28SelectedVal:function(source){
			var val = "";
			$("#xy28Selected .dropdown").each(function(index,item){
				val = val + $(item).find("i").html() + "-";
			})
			val = val.substring(0,val.length-1);
			$("#xy28Selected").find("input[name$=name]").val(val);
		},
		//测试方法
		showDataObjInfo:function(info){
			alert("json格式：" + JSON.stringify(dataArray));
		}
	};
}();

LotV2.mark = function(){
	var startYear = 1804, red = [1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],blue=[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
	green = [5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49],zodiacArray = ["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"],
	zhong=[1,2,3,4,5,6,7],fa=[8,9,10,11,12,13,14],bai=[15,16,17,18,19,20],dong=[1,5,9,13,17],nan=[2,6,10,14,18],xi=[3,7,11,15,19],bei=[4,8,12,16,20],
	_words=["冠军","亚军","季军","第四名","第五名","第六名","第七名","第八名","第九名","第十名","大","小","单","双","龙","虎","和大","和小","和单","和双","大单","小单","大双","小双","极大","极小","红波","蓝波","绿波","豹子"],
	_sfc=["第一球","第二球","第三球","第四球","第五球","第六球","第七球","第八球","总大","总小","总单","总双","总尾大","总尾小","单","双","大","小","尾大","尾小","合单","合双","东","南","西","北","中","发","白"],
	_ssc=["万位","仟位","佰位","拾位","个位","万仟","万佰","万拾","万个","个万","仟佰","仟拾","仟个","佰拾","佰个","拾个","前三","中三","后三","总和大","总和小","总和单","总和双","龙","虎","和","大","小","单","双","质","合","庄","闲","和局","庄对","闲对","庄大","闲大","牛1","牛2","牛3","牛4","牛5","牛6","牛7","牛8","牛9","无点","牛牛","豹子","四张","葫芦","顺子","三张","两对","一对","杂牌","五离","左闲","右闲","左闲尾大","右闲尾大"],
	_lhc=["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪","0尾","1尾","2尾","3尾","4尾","5尾","6尾","7尾","8尾","9尾","大","小","单","双","合单","合双","合大","合小","尾数大","尾数小","红波","蓝波","绿波","总合大","总合小","总合单","总合双","红波单","红波双","红波大","红波小","红波合单","红波合双","蓝波单","蓝波双","蓝波大","蓝波小","蓝波合单","蓝波合双","绿波单","绿波双","绿波大","绿波小","绿波合单","绿波合双","正码一","正码二","正码三","正码四","正码五","正码六"],
	_pl3=["第一球","第二球","第三球","跨度","独胆","大","小","单","双","总和大","总和小","总和单","总和双","龙","虎","和","豹子","顺子","对子","半顺","杂六","佰","拾","个","佰拾和尾数","拾个和尾数","佰个和尾数","佰拾个和尾数","佰拾个和数","质","合","0~4","0~6","14~18","21~27","尾数"],
	_syx5=["第一球","第二球","第三球","第四球","第五球","和大","和小","和单","和双","尾大","尾小","龙","虎","大","小","单","双"];
	_gyh=["7,8,14,15","9,10,12,13","3,4,18,19","5,6,16,17"];
	return {
		vaildate:function(haoMa){
			haoMa = parseInt(haoMa);
			if($.inArray(haoMa,red) >= 0){
				return '';
			}else if($.inArray(haoMa,blue) >= 0){
				return 'blue';
			}else if($.inArray(haoMa,green) >= 0){
				return 'green';
			}
		},
		zodiacName:function(haoMa,year){
			haoMa = parseInt(haoMa);
			var arr = new Array(),s="";
			if(!year){
				var date = new Date();
				year = date.getFullYear();
                if (date.getTime() < (new Date('2020-1-25 00:00:00')).getTime()) {
                    year = 2019;
                }
			}
			for(var i=0;i<12;i++){
				arr = LotV2.mark.zodiacHaoMaArray(i+1);
				if(arr != null){
					s = zodiacArray[LotV2.mark.subtractYear(year + i) % 12];
					if($.inArray(haoMa,arr) >= 0){
						return s;
					}
				}
			}
		},
		zodicaHaoMaArray:function(val){
			var arr = new Array(),s="";
			var nowYear = JIMEI.currentLunarYear;
			if(!nowYear){
				nowYear = new Date().getFullYear();
			}
			for(var i=0;i<12;i++){
				s = zodiacArray[LotV2.mark.subtractYear(nowYear+i)%12];
				if(s==val){
					arr = LotV2.mark.zodiacHaoMaArray(i+1);
					return arr;
				}
			}
		},
		subtractYear:function(year){
			if(year < startYear){	// 如果年份小于起始的甲子年(startYear = 1804),则起始甲子年往前偏移
				startYear = startYear - (60 + 60 * ((startYear - startYear) / 60));// 60年一个周期
			}
			return year - startYear;
		},
		zodiacHaoMaArray:function(age){
			switch(age){
				case 12:
					return new Array(2,14,26,38);
				case 11:
					return new Array(3,15,27,39);
				case 10:
					return new Array(4,16,28,40);
				case 9:
					return new Array(5,17,29,41);
				case 8:
					return new Array(6,18,30,42);
				case 7:
					return new Array(7,19,31,43);
				case 6:
					return new Array(8,20,32,44);
				case 5:
					return new Array(9,21,33,45);
				case 4:
					return new Array(10,22,34,46);
				case 3:
					return new Array(11,23,35,47);
				case 2:
					return new Array(12,24,36,48);
				case 1:
					return new Array(1,13,25,37,49);
				default:
					return null;
			}
		},
		arrayTotal:function(){	//计算总注数,由于nowChecked初始值为1，所以需要减1
			var a=1,b=1;
			if(LotV2.minSelected == 0 || LotV2.nowChecked == 1){return 1;}
			for(var i=0;i<LotV2.minSelected;i++){
				a = a * (LotV2.minSelected-i);
			}
			for(var i=LotV2.nowChecked-1;i>(LotV2.nowChecked-1)-LotV2.minSelected;i--){
				b = b * i;
			}
			if(LotV2.plaPlayCode == 'xuanerlianzhi' || LotV2.plaPlayCode == 'xuansanqianzhi' || LotV2.plaGroupCode == 'zhixuan'){
				return b;
			}
			return b/a;
		},
		selectedTrueShengXiao:function(val){
			var arr = LotV2.mark.zodicaHaoMaArray(val);
			var doc = $("#lotteryV2_content table tbody.table-data>tr");
			$.each(arr,function(i,val){
				doc.find("td[add-color="+val+"]").addClass("danger");
				doc.find("td[add-color="+val+"] input[type=checkbox]").prop("checked",true);
				LotV2.dataHandle.addDataCheckboxObj(doc.find("td[add-color="+val+"] input[type=checkbox]"));
			})
		},
		selectedFalseShengXiao:function(val){
			var arr = LotV2.mark.zodicaHaoMaArray(val);
			var doc = $("#lotteryV2_content table tbody.table-data>tr");
			$.each(arr,function(i,val){
				doc.find("td[add-color="+val+"]").removeClass("danger");
				doc.find("td[add-color="+val+"] input[type=checkbox]").prop("checked",false);
				LotV2.dataHandle.removeDataCheckboxObj(doc.find("td[add-color="+val+"] input[type=checkbox]"));
			})
		},
		combines:function(haoMa){
			var array = [],arr1=[],haoMaArray=[],arrs="";
			if(typeof haoMa == 'object' && haoMa.constructor == Array){
				haoMaArray == haoMa;
			}else{
				haoMaArray = haoMa.split(",");
			}
			if(haoMaArray.length<LotV2.minSelected || haoMaArray == 1){
				return;
			}
			$.ajax({
				url:JIMEI.base + "/lotteryV2/plzh.do",
				data:{haoMa:haoMa,minS:LotV2.minSelected},
				success:function(res){
					array = res;
					for(var i=0;i<array.length;i++){
						arrs += '<span class="popover-group-data-content">'+(i+1)+'.&nbsp;<span class="popover-group-data-content-num">'+array[i]+'</span></span>';
					}
					$(".popover-group-data-div").html(arrs);
				}
			})
		},
		combine:function(haoMa){
			var array = [],arr1=[],haoMaArray=[],flag=true,tempFlag=false,sum=0,pos=0;
			if(typeof haoMa == 'object' && haoMa.constructor == Array){
				haoMaArray == haoMa;
			}else{
				haoMaArray = haoMa.split(",");
			}
			if(haoMaArray.length<LotV2.minSelected || haoMaArray == 1){
				return;
			}
			for(var i=0;i<haoMaArray.length;i++){
				arr1[i] = 0;
			}
			//初始化
			for(var i=0;i<LotV2.minSelected;i++){
				arr1[i] = 1;
			}
			do{
				sum = 0;pos=0;tempFlag=true;
				array.push(LotV2.mark.print(arr1,haoMaArray));
				for(var i=0;i<haoMaArray.length-1;i++){
					if(arr1[i]==1 && arr1[i+1]==0){
						arr1[i]=0;
						arr1[i+1]=1;
						pos=i;
						break;
					}
				}
				for(var i=0;i<pos;i++){
					if(arr1[i]==1){
						sum++;
					}
				}
				for(var i=0;i<pos;i++){
					if(i<sum){
	                    arr1[i]=1;
	                }else{
	                    arr1[i]=0;
	                }
				}
				
				for(var i= haoMaArray.length-LotV2.minSelected;i<haoMaArray.length;i++){
	                if(arr1[i]==0){
	                    tempFlag = false;
	                    break;
	                }
	            }
				
				if(tempFlag==false){
	                flag = true;
	            }else{
	                flag = false;
	            }
				
			}while(flag);
			if(haoMaArray.length != LotV2.minSelected){
				array.push(LotV2.mark.print(arr1,haoMaArray));
			}
			return array;
		},
		print:function(arr1,arr2){
			var array = [];
			var pos = 0;
			for(var i=0;i<arr1.length;i++){
				if(arr1[i]==1){
					array[pos] = arr2[i];
					pos++;
				}
			}
			return array;
		},
		_sfc_zfb:function(a){
			if($.inArray(a,zhong) >= 0){
				return "中";
			}
			if($.inArray(a,fa) >= 0){
				return "发";
			}
			if($.inArray(a,bai) >= 0){
				return "白";
			}
		},
		_sfc_dnxb:function(a){
			if($.inArray(a,dong) >=0){
				return "东";
			}
			if($.inArray(a,nan) >= 0){
				return "南";
			}
			if($.inArray(a,xi) >=0){
				return "西";
			}
			if($.inArray(a,bei) >=0){
				return "北";
			}
		},
		_zuxuansanOrliu:function(rate){	//返回当前选中的赔率
			var a = LotV2.mark.arrayTotal();
			return (parseFloat(rate)/a).toFixed(2);
		},
		_pceggColor:function(a){
			if(a=="?"){return 1;}
			a = parseInt(a);
			var red=[3,6,9,12,15,18,21,24],blue=[2,5,8,11,17,20,23,26],green=[1,4,7,10,16,19,22,25];
			return $.inArray(a,red)>=0?4:$.inArray(a,blue)>=0?3:$.inArray(a,green)>=0?2:1;
		},
		_validateBuyMa:function(){
			if(LotV2.plaLotType == null){return false;}
			var ha = [],res=true;
			$.each(dataArray,function(index,item){
				switch(LotV2.plaLotType){
					case "8":
					case "828":
						if(!res){	//如果循环中号码不合法直接过滤剩下的
							return;
						}
						var _ha = item.name.split("--");
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if($.inArray(_ha[i],_gyh)>=0){
								res = true;
							}else{
								if(isNaN(_ha[i])){
									res=$.inArray(_ha[i],_words)>=0?true:false;
								}else{
									res=parseInt(_ha[i])>19 || parseInt(_ha[i])<1?false:true;
								}
							}
						}
						break;
					case "10":
					case "102":
						if(!res){return;}
						var _ha = item.name.split("-");
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								var j = _ha[i].replace("点","");
								res=parseInt(j)>17 || parseInt(j)<4?false:true;
							}else{
								res=parseInt(_ha[i])>6 || parseInt(_ha[i])<1?false:true;
							}
						}
						break;
					case "11":
						if(!res){return;}
						var _ha = item.name.split("-");
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res=$.inArray(_ha[i],_words)>=0?true:false;
							}else{
								res=parseInt(_ha[i])>27 || parseInt(_ha[i])<0?false:true;
							}
						}
						break;
					case "12":
						if(!res){return;}
						if(LotV2.plaGroupCode == "lianma"){
							var _ha = item.name.split(",");
						}else{
							var _ha = item.name.split("--");
						}
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res=$.inArray(_ha[i],_sfc)>=0?true:false;
							}else{
								res=parseInt(_ha[i])>20 || parseInt(_ha[i])<1?false:true;
							}
						}
						break;
					case "9":
					case "929":
						if(!res){return;}
						if(LotV2.plaGroupCode == "zuxuansan" || LotV2.plaGroupCode == "zuxuanliu"){
							var _ha = item.name.split(",");
						}else{
							var _ha = item.name.split("--");
						}
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res=$.inArray(_ha[i],_ssc)>=0?true:false;
							}else{
								res=parseInt(_ha[i])>999 || parseInt(_ha[i])<0?false:true;
							}
						}
						break;
					case "6":
					case "66":
						if(!res){return;}
						if(LotV2.plaGroupCode == 'zm16'){
							var _ha = item.name.split("--");
						}else{
							var _ha = item.name.split(",");
						}
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res=$.inArray(_ha[i],_lhc)>=0?true:false;
							}else{
								res=parseInt(_ha[i])>49 || parseInt(_ha[i])<1?false:true;
							}
						}
						break;
					case "14":
						if(!res){return;}
						if(LotV2.plaGroupCode == '15qiu' || LotV2.plaGroupCode == 'shuangmianpan'){
							var _ha = item.name.split("--");
						}else{
							var _ha = item.name.split(",");
						}
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res = $.inArray(_ha[i],_syx5)>=0?true:false;
							}else{
								res = parseInt(_ha[i])>11 || parseInt(_ha[i])<1?false:true;
							}
						}
						break;
					case "15":
						if(!res){return;}
						if(LotV2.plaGroupCode == "zuxuansan" || LotV2.plaGroupCode == "zuxuanliu"){
							var _ha = item.name.split(",");
						}else{
							var _ha = item.name.split("--");
						}
						for(var i=0;i<_ha.length;i++){
							if(!res){return;}
							if(isNaN(_ha[i])){
								res=$.inArray(_ha[i],_pl3)>=0?true:false;
							}else{
								res=parseInt(_ha[i])>999 || parseInt(_ha[i])<0?false:true;
							}
						}
						break;
					default:
						res=false;
						break;
				}
			})
			return res;
		},
		_dezhoupuke:function(a){	//a号码数组
			var a = a.sort(),t=a[0],i=0;
			for(var i=0;i<a.length;i++){
				if(a[i] == t){
					i++;
				}
			}
			if(i == a.length){return "豹子";}
		}
	} 
}();


(function($){  
    $.formatDate = function(pattern,date,margin){  
        //如果不设置，默认为当前时间  
        if(!date) date = new Date();  
        if(typeof(date) ==="string"){  
             if(date=="")  date = new Date();  
              else  date = new Date(date.replace(/-/g,"/"));  
        }     
        /*补00*/  
        var toFixedWidth = function(value){  
             var result = 100+value;  
             return result.toString().substring(1);  
        };  
          
        /*配置*/  
        var options = {  
                regeExp:/(yyyy|M+|d+|h+|m+|s+|ee+|ws?|p)/g,  
                months: ['January','February','March','April','May',  
                         'June','July', 'August','September',  
                          'October','November','December'],  
                weeks: ['Sunday','Monday','Tuesday',  
                        'Wednesday','Thursday','Friday',  
                            'Saturday']  
        };  
          
        /*时间切换*/  
        var swithHours = function(hours){  
            return hours<12?"AM":"PM";  
        };
          
        /*配置值*/  
        var pattrnValue = {  
                "yyyy":date.getFullYear(),                      //年份  
                "MM":toFixedWidth(date.getMonth()+1),           //月份  
                "dd":toFixedWidth(date.getDate()),              //日期  
                "hh":toFixedWidth(date.getHours() + parseInt(margin*24)),             //小时  
                "mm":toFixedWidth(date.getMinutes()),           //分钟  
                "ss":toFixedWidth(date.getSeconds()),           //秒  
                "ee":options.months[date.getMonth()],           //月份名称  
                "ws":options.weeks[date.getDay()],              //星期名称  
                "M":date.getMonth()+1,  
                "d":date.getDate(),  
                "h":date.getHours(),  
                "m":date.getMinutes(),  
                "s":date.getSeconds(),  
                "p":swithHours(date.getHours())  
        };  
          
        return pattern.replace(options.regeExp,function(){  
               return  pattrnValue[arguments[0]];  
        });  
    };  
      
})(jQuery);



$(function(){
	var $table = $(".rule-content tr td");
    $table.on("click",".ceDan",function(){
    	var it = $(this),orderId=it.attr("order_id"),lotCode=it.attr("lot_code");
    	layer.confirm("您确定要撤单该注单？",{btn:['确定','取消'],offset:['40%']},function(){
    		$.post(JIMEI.base+"/lotteryV2Bet/cancelOrder.do",{orderId:orderId,lotCode:lotCode},function(data){
    			if(data.success){
    				layer.msg("撤单成功",{icon:6,offset:['40%']},function(){
    					setTimeout($("#lotteryV2OrderForm").submit(),1000);
    				});
    			}else{
    				layer.msg(data.msg,{icon:5});
    			}
    		});
    	},function(index){
    		layer.close(index);
    	});
    }).on("click",".showOrder",function(){
    	var it=$(this),orderId=it.attr("order_id"),lotCode=it.attr("lot_code"),teshuName="",data="",yingkui=0;
    	$.post(JIMEI.base+"/lotteryV2Bet/getLotteryV2OrderData.do",{orderId:orderId,lotCode:lotCode},function(dd){
    		if(!dd.success && dd.msg != null){
    			layer.msg(dd.msg,{icon:5,area:['auto','70px']});
    		}else{
    			data = dd.bcOrder;
    			if(data.winMoney == null || data.winMoney == ""){data.winMoney = 0;}
    			if(data.winZhuShu == null || data.winZhuShu == ""){data.winZhuShu = 0;}
    			if(data.rollBackMoney == null || data.rollBackMoney ==""){data.rollBackMoney = 0;}
    			if(data.lotteryHaoMa == null || data.lotteryHaoMa == ""){data.lotteryHaoMa = "- -";}
    			switch(data.status){
    			case 1:data.status = '<span class="label label-primary">未开奖</span>';break;
    			case 2:data.status = '<span class="label label-success">已中奖</span>';yingkui =(data.winMoney - data.buyMoney+data.rollBackMoney);break;
    			case 3:data.status = '<span class="label label-danger">未中奖</span>';yingkui =(data.winMoney - data.buyMoney+data.rollBackMoney);break;
    			case 4:data.status = '<span class="label label-info">撤单</span>';break;
    			case 5:data.status = '<span class="label label-danger">回滚成功</span>';break;
    			case 6:data.status = '<span class="label label-success">回滚异常</span>';break;
    			case 7:data.status = '<span class="label label-success">开奖异常</span>';break;
    			}
    			switch(dd.groupCode){
    			case "wsl":teshuName="0尾赔率：";break;
    			case "hx" :
    			case "lx" :teshuName="生肖本命年赔率：";break;
    			}
    			layer.alert(
        				'<table width="100%" border="0" cellspacing="0" cellpadding="0" class="message_table" style="font-size: 12px; color: #000;"><tbody>'
        				+'<tr class="bg_black3"><td colspan="4" class="open_title">订单号：<span style="color:green;">'+data.orderId+'</span></td></tr>'
        				+'<tr><th class="bg_black3">帐号：</th><td>'+data.account+'</td><th class="bg_black3">单注金额：</th><td>'+((data.buyMoney).toFixed(2)/data.buyZhuShu).toFixed(2)+'</td></tr>'
        				+'<tr><th class="bg_black3">下注时间：</th><td>'+data.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+data.buyZhuShu+'</td></tr>'
        				+'<tr><th class="bg_black3">彩种：</th><td>'+data.lotName+'</td><th class="bg_black3">投注总额：</th><td>'+((data.buyMoney).toFixed(2))+'</td></tr>'
        				+'<tr><th class="bg_black3">期号：</th><td>'+data.qiHao+'</td><th class="bg_black3">基础赔率'+(data.rollBackRate?('/反水'):'')+'：</th><td>'+data.minBonusOdds.toFixed(2)+(data.rollBackRate?('/'+data.rollBackRate+"%"):'')+'</td></tr>'
        				+'<tr><th class="bg_black3">玩法：</th><td>'+data.playName+'</td><th class="bg_black3">中奖注数：</th><td>'+data.winZhuShu+'</td></tr>'
        				+'<tr><th class="bg_black3">开奖号码：</th><td>'+data.lotteryHaoMa+'</td><th class="bg_black3">中奖金额：</th><td>'+data.winMoney.toFixed(2)+'</td></tr>'
        				+'<tr><th class="bg_black3">状态：</th><td>'+data.status+'</td><th class="bg_black3">盈亏：</th><td>'+yingkui.toFixed(2)+'</td></tr>'
        				+'<tr>'+(data.rollBackRate?('<th class="bg_black3">销售返点</th><td '+(dd.teshuOdds==null?('colspan="3"'):'')+'>'+data.rollBackMoney.toFixed(2)+'</td>'):'')+(dd.teshuOdds!=null?('<th class="bg_black3">'+teshuName+'</th><td '+(data.rollBackRate?'':('colspan="3"'))+'>'+dd.teshuOdds.toFixed(2)+'</td>'):'')+'</tr>'
        				+'<tr><td colspan="4"><textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 50px; border: 1px solid #ccc; font-size: 16px;">'+data.haoMa+'</textarea></td></tr>'
        				+"</tbody></table>"
        				,{title:"系统提示",area:['500px','auto'],offset:[JIMEI.template=='v2_1'?'0%':'10%']});
    		}
    	})
    });
    
	
	//表单提交
    $("#orderSearch").click(function(){
    	var start = $("#startDate").val()
    	var end = $("#endDate").val()
    	start = new Date(start).getTime()
    	end = new Date(end).getTime()
    	if(start > end){
    		alert('开始时间不能大于结束时间')
    		return false;
    	}
    	$("#lotteryV2OrderForm").submit();
    });
})
function selPagIngSize(pageSize){
	var $form = $("#lotResultListForm");
	$form.find("input[name=pageSize]").val(pageSize);
	$form.submit();
}

function selPagIng(pageNumber){
	var $form = $("#lotResultListForm");
	$form.find("input[name=pageNumber]").val(pageNumber);
	$form.submit();
}

function selPageSize(pageSize){
	var $form = $("#lotteryV2OrderForm");
	$form.find("input[name=pageSize]").val(pageSize);
	$form.submit();
}

function selPage(pageNumber){
	var $form = $("#lotteryV2OrderForm");
	$form.find("input[name=pageNumber]").val(pageNumber);
	$form.submit();
}

//订单
//1今天，2昨天，3本周，4上周，5本月，6上月
function quickSelDate(dateVal){
	var diff = 0;
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();
	var week = date.getDay();
	 //一天的毫秒数  
    var millisecond = 1000 * 60 * 60 * 24;
    //减去的天数  
    var minusDay = week != 0 ? week - 1 : 6;
	var startTime = "yyyy-MM-dd";
	var endTime = "yyyy-MM-dd";
	var startDate = "";
	var endDate = "";
	switch(dateVal){
	case 1:
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,date,diff);
	break;
	case 2:
		date.setDate(date.getDate()-1);
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,date,diff);
	break;
	case 3:
        startDate = $.formatDate(startTime,new Date(date.getTime()-(minusDay*millisecond)),diff);
        endDate = $.formatDate(endTime,date,diff);
	break;
	case 4:
        //获得当前周的第一天
        var currentWeekDayOne = new Date(date.getTime() - (millisecond * minusDay));
        startDate = $.formatDate(startTime,new Date(currentWeekDayOne.getTime() - (millisecond * 7)),diff);
        endDate = $.formatDate(endTime,new Date(currentWeekDayOne.getTime() - millisecond),diff);
	break;
	case 5:
		date.setDate(1);
		startDate = $.formatDate(startTime,date,diff);
		endDate = $.formatDate(endTime,new Date(),diff);
	break;
	case 6:
		//获取上一月的第一天
		var priorMonthFirstDay = getPriorMonthFirstDay(year, month);
		//获得上一月的最后一天  
        var priorMonthLastDay = new Date(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth(), this.getMonthDays(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth()));
        startDate = $.formatDate(startTime,priorMonthFirstDay,diff);
		endDate = $.formatDate(endTime,priorMonthLastDay,diff);
	break;
	}
	$("#startDate").val(startDate);
	$("#endDate").val(endDate);
}




/**
 * 返回上一个月的第一天Date类型
 * @param year 年
 * @param month 月
 **/
function getPriorMonthFirstDay(year,month){
	//年份为0代表,是本年的第一月,所以不能减  
    if (month == 0) {
        month = 11; //月份为上年的最后月份  
        year--; //年份减1  
        return new Date(year, month, 1);
    }
    //否则,只减去月份  
    month--;
    return new Date(year, month, 1); ;
}


/**
 * 获得该月的天数
 * @param year年份
 * @param month月份
 * */
function getMonthDays(year,month){
     //本月第一天 1-31  
     var relativeDate = new Date(year, month, 1);
     //获得当前月份0-11  
     var relativeMonth = relativeDate.getMonth();
     //获得当前年份4位年  
     var relativeYear = relativeDate.getFullYear();

     //当为12月的时候年份需要加1  
     //月份需要更新为0 也就是下一年的第一个月  
     if (relativeMonth == 11) {
         relativeYear++;
         relativeMonth = 0;
     } else {
         //否则只是月份增加,以便求的下一月的第一天  
         relativeMonth++;
     }
     //一天的毫秒数  
     var millisecond = 1000 * 60 * 60 * 24;
     //下月的第一天  
     var nextMonthDayOne = new Date(relativeYear, relativeMonth, 1);
     //返回得到上月的最后一天,也就是本月总天数  
     return new Date(nextMonthDayOne.getTime() - millisecond).getDate();
 };




//首页轮播公告
function scollPublicInfo(){
	var code = 13;//彩票公告
	$.ajax({
		url:''+JIMEI.base+"/cpSystem/list.do",
		data:{"code":code},
		dataType:"json",
		type:"GET",
		success:function(j){
			var data = j.rows;
			var col = '';
			for(var j in data){
				col+=data[j].content;
				break;
			}
			$('#newMsg').html(col);
		}
	});
}
function trendChart(){
	var url = JIMEI.base + "/lottery/trendChart/index.do?lotCode=" + LotV2.plaCode;
	window.open(url,"newChart");
}
function rule(){
	var url = "";
	if(JIMEI.caipiao == 'off'){
		url = JIMEI.base + "/lotteryV2/rule.do?lotCode=" + LotV2.plaCode;
		window.open(url,"newRule");
	}else{
		url=JIMEI.base + "/lotteryV2/rule.do?lotCode=" + LotV2.plaCode;
		window.open(url,"newRule");
	}
}
function result(){
	var url = "";
	if(JIMEI.caipiao == 'off'){
		url = JIMEI.base + "/lotteryV2/resultList.do?lotCode=" + LotV2.plaCode;
		window.open(url,"newResult");
	}else{
		url = JIMEI.base + "/lotteryV2/resultList.do?lotCode=" + LotV2.plaCode;
		window.open(url,"newResult");
	}
}

function doExchange(arr){
    var len = arr.length;
    // 当数组大于等于2个的时候
    if(len >= 2){
        // 第一个数组的长度
        var len1 = arr[0].length;
        // 第二个数组的长度
        var len2 = arr[1].length;
        // 2个数组产生的组合数
        var lenBoth = len1 * len2;
        //  申明一个新数组
        var items = new Array(lenBoth);
        // 申明新数组的索引
        var index = 0;
        for(var i=0; i<len1; i++){
            for(var j=0; j<len2; j++){
                if(arr[0][i] instanceof Array){
                    items[index] = arr[0][i].concat(arr[1][j]);
                }else{
                    items[index] = [arr[0][i]].concat(arr[1][j]);
                }
                index++;
            }
        }
        var newArr = new Array(len -1);
        for(var i=2;i<arr.length;i++){
            newArr[i-1] = arr[i];
        }
        newArr[0] = items;
        return doExchange(newArr);
    }else{
        return arr[0];
    }
}

function onclickDetail(){
	var sum=0,totalM=0;
	var data = {
			qiHao:LotV2.qiHao,
			lotCode:LotV2.plaCode
	}
	$.ajax({
		url:JIMEI.base + "/lotteryV2Bet/doOrder.do",
		data:data,
		success:function(res){
			if(res.success){
				var resHtml = '<table class="table table-bordered table-bet mb0"><thead><tr><th>玩法</th><th>期号/注单号</th><th>号码</th><th>赔率</th><th>投注金额/可赢金额</th>'+(res.isUnCancelOrder?'<th>操作</th>':'')+'</tr></thread><tbody>';
				$.each(res.doOrder,function(i,j){
					sum++;
					resHtml += '<tr>';
					resHtml += '<td>'+j.playName+'</td><td><b class="text-danger ng-binding">'+LotV2.qiHao+'</b>/<b class="text-success ng-binding">'+j.orderId+'</b></td><td>'+j.haoMa+'</td><td><b class="text-danger ng-binding">'+j.odds+'</b></td><td><b class="text-danger ng-binding">'+j.buyMoney+'</b>/<b class="text-success ng-binding">'+((j.buyMoney*j.odds)-j.buyMoney).toFixed(2)+'</b></td>'+(res.isUnCancelOrder && j.status == 1?'<td><a style="color:red;" onclick="betOrderCancel(this)" order_id="'+j.orderId+'" lot_code="'+LotV2.plaCode+'">撤单</a></td>':res.isUnCancelOrder && j.status == 4?'<td><b class="text-success">已撤单</b></td>':'<td></td>')+'';
					resHtml += '</tr>';
					totalM += parseFloat(j.buyMoney);
				})
				resHtml += '</tbody><tfoot><tr><td colspan="2">组数：<b class="text-danger arrLength">'+sum+'</b></td><td colspan="4">总金额：<b class="text-danger all">'+totalM+' </b> 元</td></tr></tfoot></table>&nbsp;&nbsp;'
//				+'<input name="money" value="1" onclick="radioMoney()" type="radio" checked="checked"/>元<input onclick="radioMoney()" name="money" type="radio"  value="2" />角<input onclick="radioMoney()" value="3" name="money" type="radio" />分';
				//弹窗
				$("#betCurOrderModel .modal-body").html(resHtml);
				$("#betCurOrderModel").modal("toggle");
			}
		}
	});
}

function betOrderCancel($this){
	$this = $($this);
	var orderId=$this.attr("order_id"),lotCode=$this.attr("lot_code");
	layer.confirm("您确定要撤单该注单？",{btn:['确定','取消'],offset:['40%']},function(){
		$.post(JIMEI.base+"/lotteryV2Bet/cancelOrder.do",{orderId:orderId,lotCode:lotCode},function(data){
			if(data.success){
				layer.msg("撤单成功",{icon:6,offset:['40%']});
				$this.parent().html('<b class="text-success">已撤单</b>');
			}else{
				layer.msg(data.msg,{icon:5});
			}
		});
	},function(index){
		layer.close(index);
	});
}
var result=[];
var radioData = ''
function that(that) {
    if(radioData == ''){
        if(that == 2){
            $(".radioMonery").each(function(){
                $(this).val($(this).val()/10)
            });
        }else if(that ==3){
            $(".radioMonery").each(function(){
                $(this).val($(this).val()/100)
            });
        }
    }else{
        var num =  radioData.unit - that
        if(num < 0){
            if(num  == -1){
                $(".radioMonery").each(function(){
                    $(this).val($(this).val()/10)
                });
            }else if(num == -2){
                $(".radioMonery").each(function(){
                    $(this).val($(this).val()/100)
                });
            }
        }
        if(num > 0){
            if(num  == 1){
                $(".radioMonery").each(function(){
                    $(this).val($(this).val()*10)
                });
            }else if(num == 2){
                $(".radioMonery").each(function(){
                    $(this).val($(this).val()*100)
                });
            }
        }
    }
    $(".radioMonery").each(function(index){
        result[index] = {
            money:$(this).val(),
        };
    });
    radioData = {
        unit:that,
        result:result
    };
    LotV2.allMoney = 0
    $(".radioMonery").each(function(index){
    	$(this).attr('value',parseFloat(radioData.result[index].money).toFixed(2))
    	LotV2.allMoney += parseFloat(radioData.result[index].money)
    });
    $(".all").text(LotV2.allMoney.toFixed(2))
	if(LotV2.allMoney <= LotV2.nowMoney){
		LotV2.isOK = true
	}
}
    function radioMoney(){
	    	var val = $('input[name="money"]:checked ').val();
	    	switch(val){
	        case "1":
	            that(val)
	            break;
	        case "2":
	            that(val)
	            break;
	        case "3":
	            that(val)
	            break;
	    }
    }
    function formatDecimal(num) {
		 num = $.trim(num).replace(/\s/g,"")
    	if((num+'').indexOf("/") != -1){
    		var sub = (num+'').indexOf("/")
            var num1 = num.substring(0,sub)
            var num2 = num.substring(sub + 1,num.length)
            if((num1+'').indexOf(".") == -1){
                num1 = num1 + '.00'
            }
    	    trendsPeilvLength = (num1 + '').length+1;
    	    num1 = (num1 * trendsPeilv) + '';
    	    num1 = num1.substring(0, trendsPeilvLength)
    	    if((num2+'').indexOf(".") == -1){
                num2 = num2 + '.00'
            }
    	    trendsPeilvLength = (num2 + '').length+1;
    	    num2 = (num2 * trendsPeilv) + '';
    	    num2 = num2.substring(0, trendsPeilvLength)
    	    num = num1 + '/' + num2
        } else if (num && !isNaN(num)) {
    		if((num+'').indexOf(".") == -1){
                num = num + '.00'
            }
    	    trendsPeilvLength = (num + '').length+1;
    	    num = (num * trendsPeilv) + '';
    	    num = num.substring(0, trendsPeilvLength)
        }
		return num;
    }