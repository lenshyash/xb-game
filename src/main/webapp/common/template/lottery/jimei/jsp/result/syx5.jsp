<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${not empty page}">
	<c:set var="pagIng" value="${page}"></c:set>
	<table class="awardList">
		<thead>
			<tr>
                <th rowspan="3">期数</th>
                <th rowspan="3">时间</th>
                <th colspan="12">彩球号码</th>
            </tr>
            <tr>
                <th rowspan="2">万</th>
                <th rowspan="2">仟</th>
                <th rowspan="2">佰</th>
                <th rowspan="2">拾</th>
                <th rowspan="2">个</th>
                <th colspan="3">和数</th>
                <th colspan="3" class="ng-binding">
                    <em class="glyphicon glyphicon-chevron-left pull-left"> </em>
                    <span id="ssc1">万</span>
                    <span id="ssc2" style="display:none;">仟</span>
                    <span id="ssc3" style="display:none;">佰</span>
                    <span id="ssc4" style="display:none;">拾</span>
                    <span id="ssc5" style="display:none;">个</span>
                    <span id="ssc6" style="display:none;">前三(和数)</span>
                    <span id="ssc7" style="display:none;">中三(和数)</span>
                    <span id="ssc8" style="display:none;">后三(和数)</span>
                    <em class="glyphicon glyphicon-chevron-right pull-right"> </em>
                </th>
            </tr>
            <tr>
                <th>前三</th>
                <th>中三</th>
                <th>后三</th>
                <th>单双</th>
                <th>大小</th>
                <th>质合</th>
            </tr>
		</thead>
		<tbody>
			<c:forEach items="${pagIng}" var="item" varStatus="i">
				<tr <c:if test="${i.index%2==0}">class="white"</c:if>>
					<td>${item.qiHao}</td>
					<td><fmt:formatDate value="${item.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
					<c:choose>
						<c:when test="${not empty item.haoMa}">
							<c:set var="qiansan" value="0"></c:set>
							<c:set var="zhongsan" value="0"></c:set>
							<c:set var="housan" value="0"></c:set>
							<c:forTokens items="${item.haoMa }" delims="," var="haoMa" varStatus="i">
								<c:if test="${i.index == 0 ||i.index==1 || i.index ==2 }"><c:set var="qiansan" value="${qiansan+haoMa}"></c:set></c:if>
								<c:if test="${i.index == 1 ||i.index==2 || i.index ==3 }"><c:set var="zhongsan" value="${zhongsan+haoMa}"></c:set></c:if>
								<c:if test="${i.index == 2 ||i.index==3 || i.index ==4 }"><c:set var="housan" value="${housan+haoMa}"></c:set></c:if>
								<td><em class="smallRedball">${haoMa}</em></td>
							</c:forTokens>
							<td><em class="smallBlueball">${qiansan}</em></td>
							<td><em class="smallBlueball">${zhongsan}</em></td>
							<td><em class="smallBlueball">${housan}</em></td>
							<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
								<c:choose>
									<c:when test="${i.first}">
										<td class="numberDis tit ssc${i.index+1}"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
										<td class="numberDis tit ssc${i.index+1}"><em class="${haoMa>4?'big':'small' }">${haoMa>4?'大':'小'}</em></td>
										<c:set var="flag" value="0"></c:set>
										<c:choose>
											<c:when test="${haoMa>1}">
												<c:forEach begin="2" end="${haoMa-1}" var="v">
													<c:if test="${haoMa%v==0}"><c:set var="flag" value="${flag+1}"></c:set></c:if>
												</c:forEach>
												<td class="numberDis tit ssc${i.index+1}"><em class="${flag==0?'big':'small' }">${flag==0?'质':'合' }</em></td>
											</c:when>
											<c:otherwise>
												<td class="numberDis tit ssc${i.index+1}"><em class="${haoMa==1?'big':'small' }">${haoMa==1?'质':'合' }</em></td>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<td class="numberDis tit ssc${i.index+1}" style="display:none;"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
										<td class="numberDis tit ssc${i.index+1}" style="display:none;"><em class="${haoMa>4?'big':'small' }">${haoMa>4?'大':'小'}</em></td>
										<c:set var="flag" value="0"></c:set>
										<c:choose>
											<c:when test="${haoMa>1}">
												<c:forEach begin="2" end="${haoMa-1}" var="v">
													<c:if test="${haoMa%v==0}"><c:set var="flag" value="${flag+1}"></c:set></c:if>
												</c:forEach>
												<td class="numberDis tit ssc${i.index+1}" style="display:none;"><em class="${flag==0?'big':'small' }">${flag==0?'质':'合' }</em></td>
											</c:when>
											<c:otherwise>
												<td class="numberDis tit ssc${i.index+1}" style="display:none;"><em class="${haoMa==1?'big':'small' }">${haoMa==1?'质':'合' }</em></td>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:forTokens>
							<c:set var="qiansanFlag" value="0"></c:set>
							<c:set var="zhongsanFlag" value="0"></c:set>
							<c:set var="housanFlag" value="0"></c:set>
							<!-- 前三 -->
							<td class="numberDis tit ssc6" style="display:none;"><em class="${qiansan%2==0?'dual':'odd' }">${qiansan%2==0?'双':'单'}</em></td>
							<td class="numberDis tit ssc6" style="display:none;"><em class="${qiansan>13?'big':'small' }">${qiansan>13?'大':'小'}</em></td>
							<c:if test="${qiansan != 0}">
								<c:forEach begin="2" end="${qiansan-1}" var="v">
									<c:if test="${qiansan%v==0 }"><c:set var="qiansanFlag" value="${qiansanFlag+1}"></c:set></c:if>
								</c:forEach>
							</c:if>
							<c:if test="${qiansan == 0}"><c:set var="qiansanFlag" value="${qiansanFlag+1}"></c:set></c:if>
							<td class="numberDis tit ssc6" style="display:none;"><em class="${qiansanFlag==0?'big':'small' }">${qiansanFlag==0?'质':'合' }</em></td>
							<!-- 中三 -->
							<td class="numberDis tit ssc7" style="display:none;"><em class="${zhongsan%2==0?'dual':'odd' }">${zhongsan%2==0?'双':'单'}</em></td>
							<td class="numberDis tit ssc7" style="display:none;"><em class="${zhongsan>13?'big':'small' }">${zhongsan>13?'大':'小'}</em></td>
							<c:if test="${zhongsan != 0}">
							<c:forEach begin="2" end="${zhongsan-1}" var="v">
								<c:if test="${zhongsan%v==0 }"><c:set var="zhongsanFlag" value="${zhongsanFlag+1}"></c:set></c:if>
							</c:forEach>
							</c:if>
							<c:if test="${zhongsan == 0}"><c:set var="zhongsanFlag" value="${zhongsanFlag+1}"></c:set></c:if>
							<td class="numberDis tit ssc7" style="display:none;"><em class="${zhongsanFlag==0?'big':'small' }">${zhongsanFlag==0?'质':'合' }</em></td>
							<!-- 后三 -->
							<td class="numberDis tit ssc8" style="display:none;"><em class="${housan%2==0?'dual':'odd' }">${housan%2==0?'双':'单'}</em></td>
							<td class="numberDis tit ssc8" style="display:none;"><em class="${housan>13?'big':'small' }">${housan>13?'大':'小'}</em></td>
							<c:if test="${housan != 0}">
							<c:forEach begin="2" end="${housan-1}" var="v">
								<c:if test="${housan%v==0 }"><c:set var="housanFlag" value="${housanFlag+1}"></c:set></c:if>
							</c:forEach>
							</c:if>
							<c:if test="${housan == 0 }"><c:set var="housanFlag" value="${housanFlag+1}"></c:set></c:if>
							<td class="numberDis tit ssc8" style="display:none;"><em class="${housanFlag==0?'big':'small' }">${housanFlag==0?'质':'合' }</em></td>
						</c:when>
						<c:otherwise>
							<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>