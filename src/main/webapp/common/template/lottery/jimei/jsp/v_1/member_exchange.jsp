<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/hisRecord.css?v=111" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/member.css?v=34341990dd" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>     
	<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
	<script src="${base}/common/js/contants.js?v=2"></script>  
</head>
<body>
	<div class="content">
	<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
	<div class="account-content-container">
	<div class="account-content"><div class="notification-list">
	<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能线上兑换</div></c:if>
	<c:if test="${userInfo.accountType!=6 }">
	<table class="table">
		<tbody>
			<tr><th width="100">类型</th><th>余额</th><th>操作</th></tr>
			<tr>
				<td>现金</td>
				<td><span class="nowMoney">0.00</span>&nbsp;&nbsp;RMB</td>
				<td rowspan="2"><input value="刷新" id="flushMoneyAndScoreBtn" class="btn btn-blue" type="button"></td>
			</tr>
			<tr>
				<td>积分</td>
				<td><span class="scoreCredit">0</span></td>
			</tr>
			<tr>
				<td>兑换类型： </td>
				<td colspan="2" style="text-align:left;"><select id="exchangeType" class="form-control">
					<option selected="selected" value="">----请选择类型----</option>
					<option value="1">现金兑换积分</option>
					<option value="2">积分兑换现金</option>
				</select><span id="exchange_remark_span"></span></td>
			</tr>
			<tr>
				<td>兑换额度：</td>
				<td colspan="2" style="text-align:left;"><input id="amount" name="amount" class="form-control" type="text"></td>
			</tr>
			<tr>
				<td colspan="3" style="text-align:left;padding-left:100px;">
					<input value="确定" id="exchangeBtnId" class="btn btn-blue" type="button" style="height:35px;width:100px;font-size:22px;">
					<div style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</div>
					<div style="font-size: 14px; font-weight: bold; color: #c00">积分可用于参与站点活动</div>
				</td>
			</tr>
		</tbody>
	</table>
	</c:if></div></div></div>
</body></html>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
$(function() {
	var types=[];
	$.ajax({
		url : "${base}/center/banktrans/exchange/exctypes.do",
		success : function(result) {
			types = result;
		}
	});
	function getExcTypeByType(val) {
		if (types) {
			for (var i = 0; i < types.length; i++) {
				if (val == types[i].type) {
					return types[i];
				}
			}
		}
		return null;
	}
	//制保留2位小数，如：2，会在2后面补上00.即2.00 
	 function toDecimal2(x) {
	 	if (isNaN(x)) {
	 		return false;
	 	}
	 	var s = x.toString();
	 	var rs = s.indexOf('.');
	 	if (rs < 0) {
	 		rs = s.length;
	 		s += '.';
	 	}else{
	 		s=s.substring(0,rs)+"."+s.substring(rs+1,rs+3);
	 	}
	 	while (s.length <= rs + 2) {
	 		s += '0';
	 	}
	 	return s;
	 }
	$("#flushMoneyAndScoreBtn").click(function(){
		$.ajax({
			url : "${base}/center/banktrans/exchange/udata.do",
			success : function(result) {
				$(".nowMoney").html(toDecimal2(result.money));
				$(".scoreCredit").html(result.score);
			}
		});
	}).click();
	
	$("#exchangeType").change(function() {
		var $it=$(this),$op=$it.children('option:selected'),
			selval = $it.val(),selHtml = $op.html().split("兑换");
		if (selval > 0) {
			var selectedType = getExcTypeByType(selval);
			if (selectedType) {
				$("#exchange_remark_span").html(selectedType.numerator + selHtml[0]
								+ "可兑换"
								+ selectedType.denominator
								+ selHtml[1] + "(兑换比例："
								+ selectedType.numerator + ":"
								+ selectedType.denominator
								+ ")");
			} else {
				$("#exchange_remark_span").html("暂时未开放！");
			}
		} else {
			$("#exchange_remark_span").html("");
		}
	});
	$("#exchangeBtnId").click(function() {
		var $it=$(this);
		if($it.attr("disabled")){
			return;
		}
		$it.attr("disabled", "disabled");
		var selval=$("#exchangeType").val();
		if (!selval) {
			layer.msg("请选择兑换类型！", {icon : 2});
			$it.removeAttr("disabled");
			return;
		}
		var amount = $("#amount").val();
		if (!amount) {
			layer.msg("额度不能为空！", {icon : 2});
			$it.removeAttr("disabled");
			return;
		}
		var selectedType = getExcTypeByType(selval);
		if (amount > selectedType.maxVal) {
			layer.msg("额度必须小等于" + selectedType.maxVal, {icon : 2});
			$it.removeAttr("disabled");
			return;
		}
		if (amount < selectedType.minVal) {
			layer.msg("额度必须大等于" + selectedType.minVal, {
				icon : 2
			});
			$it.removeAttr("disabled");
			return;
		}

		$.ajax({
			url : "${base}/center/banktrans/exchange/exchange.do",
			data : {
				typeId : selectedType.id,
				amount : amount
			},
			success : function(result) {
				if (!result.success) {
					layer.msg(result.msg, {
						icon : 2
					});$it.removeAttr("disabled");
					return;
				} else {
					layer.alert("兑换成功", {
						icon : 1,
						offset : [ '30%' ]
					}, function(index) {
						$("#flushMoneyAndScoreBtn").click();
						$it.removeAttr("disabled");
						layer.close(index);
					})
				}
			}
		});
	});
});
</script></c:if>