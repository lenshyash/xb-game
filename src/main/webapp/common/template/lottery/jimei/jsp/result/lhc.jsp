<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- LHC -->
<c:if test="${not empty page}">
	<c:set var="pagIng" value="${page}"></c:set>
	<table class="awardList">
        <thead>
        <tr>
            <th rowspan="3">期数</th>
            <th rowspan="3">时间</th>
            <th colspan="18">彩球号码</th>
        </tr>
        <tr>
            <th rowspan="2">正码一</th>
            <th rowspan="2">正码二</th>
            <th rowspan="2">正码三</th>
            <th rowspan="2">正码四</th>
            <th rowspan="2">正码五</th>
            <th rowspan="2">正码六</th>
            <th rowspan="2">特别号</th>
            <th colspan="3">总和</th>
            <th class="ng-binding" colspan="4">
                       <em class="glyphicon glyphicon-chevron-left pull-left"> </em>
                       <span id="lhc1">正码一</span>
                       <span id="lhc2" style="display:none;">正码二</span>
                       <span id="lhc3" style="display:none;">正码三</span>
                       <span id="lhc4" style="display:none;">正码四</span>
                       <span id="lhc5" style="display:none;">正码五</span>
                       <span id="lhc6" style="display:none;">正码六</span>
                       <span id="lhc7" style="display:none;">特别号</span>
                       <em class="glyphicon glyphicon-chevron-right pull-right"> </em>
	            </th>
        </tr>
        <tr>
        	<th>总和</th>
        	<th>总单双</th>
        	<th>总大小</th>
            <th>单双</th>
            <th>大小</th>
            <th>尾数 </th>
            <th>和数</th>
        </tr>
        </thead>
        <tbody>
        	<c:forEach items="${page}" var="item" varStatus="i">
				<tr <c:if test="${i.index%2==0}">class="white"</c:if>>
					<td>${item.qiHao}</td>
					<td><fmt:formatDate value="${item.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
					<c:choose>
						<c:when test="${not empty item.haoMa}">
							<c:set var="sum" value="0"></c:set>
							<c:forTokens items="${item.haoMa }" delims="," var="haoMa" varStatus="i">
								<c:set var="sum" value="${sum+haoMa}"></c:set>
								<td><em class="smallRedball mark_${haoMa}">${haoMa}</em></td>
							</c:forTokens>
							<td><em class="smallBlueball">${sum}</em></td>
							<td class="numberDis"><em class="${sum%2==0?'dual':'odd' }">${sum%2==0?'双':'单'}</em></td>
							<td class="numberDis"><em class="${sum>175?'big':'small' }">${sum>175?'大':'小'}</em></td>
							<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
								<c:choose>
									<c:when test="${i.first}">
											<td class="numberDis tit lhc${i.index+1}"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
											<td class="numberDis tit lhc${i.index+1}"><em class="${haoMa>25?'big':'small' }">${haoMa>25?'大':'小'}</em></td>
											<td class="tit lhc${i.index+1}"><em class="smallBlueball">${haoMa%10}</em></td>
											<td class="tit lhc${i.index+1}"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber><em class="smallBlueball">${heshu}</em></td>
									</c:when>
									<c:otherwise>
											<td class="numberDis tit lhc${i.index+1}" style="display:none;"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
											<td class="numberDis tit lhc${i.index+1}" style="display:none;"><em class="${haoMa>25?'big':'small' }">${haoMa>25?'大':'小'}</em></td>
											<td class="tit lhc${i.index+1}" style="display:none;"><em class="smallBlueball">${haoMa%10}</em></td>
											<td class="tit lhc${i.index+1}" style="display:none;"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber><em class="smallBlueball">${heshu}</em></td>
									</c:otherwise>
								</c:choose>
							</c:forTokens>
						</c:when>
						<c:otherwise>
							<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
        </tbody>
    </table>
</c:if>