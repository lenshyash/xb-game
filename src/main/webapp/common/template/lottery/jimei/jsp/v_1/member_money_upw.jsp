<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/draw.css" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
					<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
						<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能提款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }"><div class="membersubnavi">
                      <div class="subnavi">
                     	<a href="${base}/center/banktrans/mnychg/chgmnyPages.do" target="frame">资金转移</a>
                     </div>
               	</div>
				<div class="stepswithdraw">
                	<div class="substeps">
                    	<div class="substepitmgray1 substepitmblue1">
                        	<b>01</b>  设置提现密码
                        </div>
                        <div class="substepitmgray2">
                        	<b>02</b>  资金转移
                        </div>
                    </div>
                    <div class="line2"></div>
                </div>             	
               <div class="account-content-container">
					<div class="account-content">
						<h3 class="title"><small style="margin-left: 63px;">*提现密码规则：须由<font color="red">6-12个字母和数字(A-Z 和 0-9)</font>组成。 且您的新密码不能和现用的密码相同 </small> </h3>
						<div class="security-setting">
							<table class="table">
								<tbody>
									<tr>
										<td class="align-right-col">提现密码：</td>
										<td>
										<input id="password" class="form-control" type="password">
										</td>
									</tr>
									<tr>
										<td class="align-right-col">确认密码：</td>
										<td>
											<input id="rpassword" class="form-control" type="password">
										</td>
									</tr>

									<tr class="last-row">
										<td>&nbsp;</td>
										<td colspan="3">
											<button class="btn btn-blue" onclick="updpwd()">下一步</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div></c:if>
		  </div>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
	function updpwd(){
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if(!password){
			layer.alert("新密码不能为空",{icon:2});
			return;
		}
		if(!rpassword){
			layer.alert("确认密码不能为空",{icon:2});
			return;
		}
		if (password !== rpassword) {
			layer.alert("两次密码不一致！",{icon:2});
			return;
		}
		$.ajax({
			url : "${base}/center/member/meminfo/repwd.do",
			data : {
				pwd : password,
				rpwd : rpassword
			},
			success : function(result) {
				layer.alert("设置提现密码成功",{icon:1},function(index){
					//刷新父页
					window.location.reload();
					layer.close(index);
				});
			}
		});
		
	}
</script></c:if>
</body></html>