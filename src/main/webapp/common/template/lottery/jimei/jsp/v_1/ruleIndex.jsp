<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="${base }/images/logo.ico" type="image/x-icon">
<title>${website_name}</title>
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/lotteryV2.js?v=${caipiao_version}"></script>
<script type="text/javascript">
		JIMEI = {
				base:'${base}',
				folder:"${stationFolder}",
	    		caipiao_version : '${caipiao_version}',
	    		code:'${lotCode}',
	    		template:'v2_1',
		}
	</script>
</head>
<body class="skin_blue" style="font-size:12px;">
	<!-- 内容添加处.. -->
	<div id="_main_view">
		<jsp:include page="../include_v_1/rule_content.jsp"></jsp:include>
	</div>
</body>
</html>
