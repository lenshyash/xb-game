<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${base}/common/template/lottery/jimei/css/top.css?v=${caipiao_version}"  type="text/css">
<div class="header_v2">
    <div class="wrap clearfix">
        <div class="logo"><a href="${base}/lotteryV2/index.do"><img src="${base}${caipiaoLogo}"/></a></div>
        <ul class="navs">
            
            <li class="item all_kind">
                <a class="item_a" href="#lotterys"><span class="a_bg">全部彩种<i class="arrow"></i></span></a>
                <div class="drop_menu">
				    <div class="row clearfix">
				        <dl class="item clearfix">
			                <dt>时时彩</dt>
						    <dd>
					    		<c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
					    				<c:if test="${lot.type == 9}">
					    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
					    				</c:if>
					    		</c:forEach>
						    </dd>
				        </dl>
				        <dl class="item">
			                <dt>快3</dt>
			    			<dd>
				                <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
					    				<c:if test="${lot.type == 10}">
					    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
					    				</c:if>
					    		</c:forEach>
			    			</dd>
				        </dl>
				        <dl class="item">
			                <dt>香港彩</dt>
						    <dd>
						        <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
				    				<c:if test="${lot.type == 6}">
				    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
				    				</c:if>
						    	</c:forEach>
						    </dd>
				
				        </dl>
				    </div>
				    <div class="row clearfix row_2">
				        <dl class="item">
				                <dt>十分彩</dt>
							    <dd>
					                <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
				    				<c:if test="${lot.type == 12}">
				    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
				    				</c:if>
						    	</c:forEach>
							    </dd>
				        </dl>
				         <dl class="item">
				                <dt>PK10</dt>
							    <dd>
					               <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
				    				<c:if test="${lot.type == 8}">
				    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
				    				</c:if>
						    	</c:forEach>
							    </dd>
				        </dl>
				        <dl class="item">
				                <dt>其他</dt>
							    <dd>
					               <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
				    				<c:if test="${lot.type == 11}">
				    					 <a href="${base}/lotteryV2/index.do?lotCode=${lot.code}">${lot.name}</a>
				    				</c:if>
						    	</c:forEach>
							    </dd>
				        </dl>
				    </div>
                </div>
            </li>
            <li class="item"><a class="item_a" href="javascript:void(0);" onclick="result();">开奖结果</a></li>
            <li class="item"><a class="item_a" href="javascript:void(0);" onclick="trendChart();">走势图</a></li>
            <!-- <li class="item"><a class="item_a" href="#activity">优惠活动</a><div class="hot"></div></li> -->
            <li class="item usercenter">
                    <a class="item_a" href="javascript:void(0);">用户中心<i class="arrow"></i></a>
                    <div class="drop_menu">
                        <a href="${base}/center/member/meminfo/page.do" target="_blank">个人资料</a>
                        <a href="${base}/center/record/betrecord/page.do" target="_blank">彩票投注记录</a>
                        <a href="${base}/lotteryV2/history.do" target="_blank">历史注单</a>
                        <a href="${base}/center/record/hisrecord/page.do" target="_blank">充值记录</a>
                        <a href="${base}/center/record/hisrecord/page.do" target="_blank">取款记录</a>
                        <a id="op-loginout" href="${base}/logout.do">退出</a>
                    </div>
            </li>
            <!-- <li class="item"><a class="item_a" href="http://appdownload.dajin33.com/" target="_blank">手机APP</a></li> -->
        </ul>
        <div class="userarea">
                <dl class="userinfo clearfix" id="header-userinfo">
                    <dt><a href="#memberinfo" class="avatar"><img src="${base}/common/template/lottery/jimei/images/001.jpg" alt=""></a></dt>
                    <dd>
                        <div class="t1 name"><a href="" class="username accountName" id="account"></a></div>
                        <div class="t2 score">
                            <span class="money nowMoney" style="cursor: pointer;" id="nowMoney" onclick="refreshMoney()"></span> 元
                            <a href="javascript:;" class="refresh" onclick="refreshMoney()"></a>
                           <!--  <a href="javascript:;" class="hide_money" id="uss-fs">隐藏</a> -->
                        </div>
                    </dd>
                </dl>
                <a href="${base}/center/banktrans/deposit/page.do" title="充值" class="charge" target="_blank">充值</a>
                <a href="${base}/center/banktrans/draw/page.do" title="提现" class="withdraw" target="_blank">提现</a>
                <a href="javascript:void(0);" onclick="zxkf();" title="在线客服" class="kefu"></a>
                <a href="javascript:void(0);" onclick="zxkf();">在线客服</a>

        </div>
    </div>
</div>
<script type="text/javascript">
//在线客服
function zxkf(){
	var open = window.open();
	$.ajax({
		url : "${base}/curstinfo.do",
		type : "post",
		success : function(result) {
			var url = result.olserurl;
			if (url) {
				if (url.indexOf("http://") < 0) {
					url = "http://" + url;
				}
			}
			open.location = url;
		}
	});
}
</script>