<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/draw.css" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
				<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
					<div class="membersubnavi">
               		<div class="subnavi">
                     	<a href="${base}/center/banktrans/mnychg/chgmnyPages.do" target="frame">资金转移</a>
                     </div>
               	</div>
                <div class="stepswithdraw">
                	<div class="substeps">
                    	<div class="substepitmgray1">
                        	<b>01</b>  设置提现密码
                        </div>
                        <div class="substepitmgray2 substepitmblue2">
                        	<b>02</b>  资金转移
                        </div>
                    </div>
                    <div class="line2"></div>
                </div>
             	<style>
             		.account-content table td {
						    padding: 5px 0 5px 10px;
						}
						.form-control{width:50%;}
						.account-content-container {
							    padding: 20px 40px 10px;
							}
             	</style>
               <div class="account-content-container">
					<div class="account-content">
						<div class="security-setting">
							<table class="table" id="savebank">
									<tbody>
										<tr>
											<td class="align-right-col"><font color="red">*</font>转入会员：</td>
											<td>
												<input id="chgAccount" class="textfield1 form-control" name="chgAccount" type="text">
											</td>
										</tr>
										<tr>
											<td class="align-right-col"><font color="red">*</font>转入金额：</td>
											<td>
												<input id="chgMoney" class="textfield1 form-control" name="chgMoney" type="text">
											</td>
										</tr>
										<tr>
											<td class="align-right-col"><font color="red">*</font>提现密码：</td>
											<td>
												<input id="userQxpassword" class="textfield1 form-control" name="userQxpassword" type="password">
											</td>
										</tr>
										<tr class="last-row">
											<td>&nbsp;</td>
											<td colspan="3">
												<button id="drawcommit" class="btn btn-blue" onclick="drawcommit()">提交</button>
											</td>
										</tr>
									</tbody>	
							</table>
						</div>
					</div>
				</div>
		  </div>
<script type="text/javascript">
	var min = 0;
	var max = 0;
	var balance = 0;
	
	function drawcommit() {
		$("#drawcommit").attr("disabled","disabled");
		var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
		var m = $("#chgMoney").val();
		if (!exp.test(m)) {
			layer.alert("请输入正确的金额",{icon:2});
			$("#chgMoney").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		
		var chgAccount = $("#chgAccount").val();
		
		if (!chgAccount) {
			layer.alert("请输入正确的转入会员",{icon:2});
			$("#chgAccount").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		
		var userQxpassword = $("#userQxpassword").val();
		if (userQxpassword == null || userQxpassword == "") {
			layer.alert("提现密码不能为空",{icon:2});
			$("#userQxpassword").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		
		$.ajax({
			url : "${base}/center/banktrans/mnychg/moneychg.do",
			data : {
				account : chgAccount,
				money : m,
				password : userQxpassword
			},
			success : function(result) {
				if(!result.success){
					layer.alert(result.msg,{icon:2});
				}else{
					layer.alert("转入成功!",{icon:1},function(index){
						window.location.href = "${base}/center/banktrans/mnychg/chgmnyPages.do";
						layer.close(index);
					});
				}
			},
			complete:function(){
				$("#drawcommit").removeAttr("disabled");
			}
		});
	}
</script>
</body></html>