<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/draw.css" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
					<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
						<div class="membersubnavi">
                      		<div class="subnavi">
                            	<a href="${base}/center/banktrans/deposit/cPage.do" target="frame">充值</a>
                            </div>
                            <div class="subnavi">
                            	<span class="blue">提款</span>
                            	<div class="subnaviarrow"></div>
                            </div> 
                            <div class="subnavi">
                            	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                            </div>
               	</div>
                <div class="stepswithdraw">
                	<div class="substeps">
                    	<div class="substepitmgray1">
                        	<b>01</b>  设置提现密码
                        </div>
                        <div class="substepitmgray2">
                        	<b>02</b>  完善出款银行信息
                        </div>
                        <div class="substepitmgray2 substepitmblue2">
                        	<b>03</b>  取款
                        </div>
                    </div>
                    <div class="line2"></div>
                </div>
             	<style>
             		.account-content table td {
						    padding: 5px 0 5px 10px;
						}
						.form-control{width:50%;}
						.account-content-container {
							    padding: 20px 40px 10px;
							}
             	</style>
               <div class="account-content-container">
					<div class="account-content">
						<div class="security-setting">
							<table class="table" id="savebank">
									
							</table>
						</div>

					</div>
				</div>
		  </div>
<script type="text/javascript">
	var min = 0;
	var max = 0;
	var balance = 0;
	
	$(function(){
		initDrawdata();
	})
	
	function initDrawdata() {
		$.ajax({
			url : "${base}/center/banktrans/draw/drawdata.do",
			success : function(result) {
				min = result.min;
				max = result.max;
				balance = result.member.money;
				var html = template('draw_tpl', result);
				$("#savebank").append(html);
			}
		});
	}

	function drawcommit() {
		$("#drawcommit").attr("disabled","disabled");
		var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
		var m = $("#money").val();
		if (!exp.test(m)) {
			layer.alert("请输入正确的金额",{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		m = parseInt(m);
		if (m < min) {
			layer.alert("提现最小金额不能小于" + min,{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		if (m > balance) {
			layer.alert("余额不足",{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		if (max != 0 && max < m) {
			layer.alert("提现最大金额不能大于" + max,{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		var userQxpassword = $("#userQxpassword").val();
		if (userQxpassword == null || userQxpassword == "") {
			layer.alert("提现密码不能为空",{icon:2});
			$("#userQxpassword").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		$.ajax({
			url : "${base}/center/banktrans/draw/drawcommit.do",
			data : {
				money : m,
				repPwd : userQxpassword
			},
			success : function(result) {
				if(!result.success){
					layer.alert(result.msg,{icon:2});
				}else{
					layer.alert("提现信息已提交!",{icon:1},function(index){
						window.location.href = "${base}/center/record/hisrecord/cPage.do?type=2";
						layer.close(index);
					});
				}
			},
			complete:function(){
				$("#drawcommit").removeAttr("disabled");
			}
			
		});
	}
</script>
<script id="draw_tpl" type="text/html">
	<tbody>
		<tr style="line-height:30px">
			<td class="align-right-col">会员帐号：</td>
			<td>{{member.account}}</td>
			<td rowspan="7" width="50%">
				<div style="line-height: 28px;padding: 10px;">
				<b>提示信息:</b>
				<br> 
				1. 每天的取款处理时间为：<span style="font-size: 15px; color: #ffb400;">{{star}} 至 {{end}}</span>;
				<br>
				2. 取款10分钟内到账。(如遇高峰期，可能需要延迟到三十分钟内到帐);
				<br>
				3. 用户每日最小提现 <span style="color: #ffb400; font-size: 15px;">{{min}}</span> 元，最大提现 <span style="color: #ffb400; font-size: 15px;">{{max}}</span> 元;
				<br>
				4. 今日可取款 <span style="color: #ffb400; font-size: 15px;">{{wnum}}</span> 次，已取款 <span style="color: #ffb400; font-size: 15px;">{{curWnum}}</span> 次 ;
				<br>
				<b>消费比例:</b>
				<br>
				1. 出款需达投注量：<span style="color: #ffb400; font-size: 15px;">{{checkBetNum}}</span> ，  当前有效投注金额：<span style="color: #ffb400; font-size: 15px;">{{member.betNum}}</span>
				<br>
				2. 是否能取款：<span style="color: #ffb400; font-size: 15px;">{{drawFlag}}</span>
				</div>
			</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">真实姓名：</td>
			<td>{{$memberHidden member.userName 1}}</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">账户余额：</td>
			<td><strong style="color:red;">{{member.money}}</strong>RMB</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">出款银行：</td>
			<td>{{member.bankName}}</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">开户网点：</td>
			<td>{{member.bankAddress}}</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col"><font color="red">*</font>银行帐号：</td>
			<td>{{$memberHidden member.cardNo 4}}</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>提现金额：</td>
			<td>
				<input id="money" class="textfield1 form-control" name="money" type="text">
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>提现密码：</td>
			<td>
				<input id="userQxpassword" class="textfield1 form-control" name="userQxpassword" type="password">
			</td>
		</tr>
		<tr class="last-row">
			<td>&nbsp;</td>
			<td colspan="3">
				<button id="drawcommit" class="btn btn-blue" onclick="drawcommit()">提交</button>
			</td>
		</tr>
	</tbody>									
</script>
<script type="text/javascript">
template.helper('$memberHidden', function(value,len) {
	var val = value.substring(value.length-len,value.length),str="";
	for(var i=1;i<=value.length-len;i++){
		str+="*";
	}
	str = str + val;
	return str;
});
</script>
</body></html>