<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/onlinedeposit.css?v=438033200234" rel="stylesheet">
	<link rel="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
				<jsp:include page="../../include_v_1/base_head_info.jsp"></jsp:include>
				<div class="membersubnavi">
                      		<div class="subnavi">
                            	<span class="blue">充值</span>
                            	<div class="subnaviarrow"></div>
                            </div>
                            <div class="subnavi">
                            	<a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
                            </div> 
                            <div class="subnavi">
                            	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                            </div>
               	</div>
            <div class="steps" style="height:40px;">
			<div class="substeps">
				<div class="substepitmgray1">
					<b>01</b> 选择支付模式
				</div>
				<div class="substepitmgray2 substepitmblue2">
					<b>02</b> 填写付款人资料
				</div>
				<div class="substepitmgray2" id="sliderBuzhou">
					<b>03</b> 确认您的转账资料
				</div>
				<div class="substepitmgray2">
					<b>04</b> 完成
				</div>
			</div>
			</div>
			<style>
				.form-control{width:100%;}
				.account-content table td {
					    padding: 5px 0 5px 10px;
					}
				.table tbody td{border-bottom:1px solid #ffffff;}
				.table .last-row td{border:none;}
				.btn{padding:4px 25px;margin-right:40px;}
			</style>
			<div class="account-content" id="depositTabs">

					</div>
	</div>
<script type="text/javascript">
	var baseUrl = "${base}";
	var hostUrl1="${hostUrl1}";
</script>
<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2674"></script>
<script id="inputdata_tpl" type="text/html">
			{{if onlines && onlines.length>0}}
			<form id="onlinePayAFormId">
			<div class="subrow">
				<div class="subcol2">
					{{each onlines as online count}}
						{{if count > 0 && count %5 ==0}}
							<br>
						{{/if}}
						<div class="banklist marginbtm10">
							<input name="payId" data-fixedFlag="{{online.fixedFlag}}" data-randomFlag="{{online.randomFlag}}" data-fixedAmount="{{online.fixedAmount}}" data-randomAmount="{{online.randomAmount}}" min="{{online.min}}" max="{{online.max}}" iconCss="{{online.iconCss}}" payType="{{online.payType}}" value="{{online.id}}" class="bankradiobtn" type="radio">
							{{if online.icon}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}{{if online.icon}}{{online.icon}}{{/if}}" title="{{online.payName}}" width="123" height="47"></div>
							{{ else if online.payType == "1"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/banks.png" title="{{online.payName}}" height="47"  width="123"></div>
							{{ else if online.payType == "3"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/weixin.jpg" title="{{online.payName}}" height="47"  width="123"></div>
							{{ else if online.payType == "4"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/zhifubao.jpg" title="{{online.payName}}" height="47" width="123"></div>
							{{ else if online.payType == "5"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/qqpay.png" title="{{online.payName}}" height="47" width="123"></div>
							{{ else if online.payType == "6"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/jdpay.jpg" title="{{online.payName}}" height="40" width="123"></div>
							{{ else if online.payType == "7"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/baidu.jpg" title="{{online.payName}}" height="40" width="123"></div>
							{{ else if online.payType == "8"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/union.jpg" title="{{online.payName}}" height="40" width="123"></div>
							{{ else if online.payType == "9"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/unionpay.png" title="{{online.payName}}" height="40" width="123"></div>
							{{ else if online.payType == "11"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/weixin.jpg" title="{{online.payName}}" height="47"  width="123"></div>
							{{ else if online.payType == "12"}}
								<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/zhifubao.jpg" title="{{online.payName}}" height="47" width="123"></div>
							{{else}}
								<span title="{{online.payName}}" class="bankicon {{online.iconCss}}" style="width:123px;display:table-caption">{{online.payName}}</span>
							{{/if}}
						</div>
					{{/each}}
				</div>
			</div>
			<div class="security-setting hidn" id="dpt_info">
			<table class="table">
							<tbody>
								<tr style="line-height:30px">
									<td class="align-right-col">支付名称：</td>
									<td class="li-payCom"></td>
									<td></td>
								</tr>
								<tr style="line-height:30px">
									<td class="align-right-col">会员帐号：</td>
									<td>${loginMember.account}</td>
									<td></td>
								</tr>
								<tr>
									<td class="align-right-col"><font color="red">*</font>充值金额：</td>
									<td style="width:10%;">
										<input class="form-control" name="amount" type="text" id="amountMoney">
									</td>
									<td><span class="smalltxt">（单笔充值限额：最低 <span id="min_amount" class="red">10</span> 元，最高 <span id="max_amount" class="red">999999</span>）</span></td>
								</tr>
								<tr style="line-height:30px">
									<td class="align-right-col">充值金额(大写)：</td>
									<td><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
									<td></td>
								</tr>
								<tr id="pcInputMoney">

								</tr>
								<tr class="last-row">
									<td>&nbsp;</td>
									<td colspan="3">
										<button id="nextBtn" class="btn btn-blue" >下一步</button>
									</td>
								</tr>
							</tbody></table>
					</div>
				<style>
					#pcInputMoney span{
						width: 65px;
						color: #000000;
						margin-left: 20px;
					}
					#pcInputMoney span:hover{
						color: #f6383a;
						cursor: pointer;
					}
					.inputActive{
						color: #f6383a!important;
					}
				</style>
			</form>
			{{else}}
				<div style="margin-left:225px;font-weight: bold;"><span class="red">*</span>友情提示：暂时没有配置充值入口，请更换其他方式继续充值</div>
			{{/if}}
</script>
<script type="text/javascript">
    var rechargeHtml = '',rechargeNumber = 0;
	$(function(){
		initDptb();
	})
	var pays = [];
	function initDptb() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptinita.do",
			success : function(result) {
				var html = template('inputdata_tpl', result);
				$("#depositTabs").html(html);
				bindIptData();
				pays = result.onlines;
			}
		});
	}
	
	function bindIptData(){
		var convertCurrency=function(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return layer.alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    };
	    var $from = $("#onlinePayAFormId");
	    $from.delegate( "input[name='amount']", 'keyup', function(){
	        /* 取出输入的金额,并转换成中文 */
	        $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
		$("#pcInputMoney").click(function(){
			$( "#amount_CH" ).text(convertCurrency( $( this ).find('.inputActive').text() ) );
		})
	    $from.on('click', ".banklist", function(e) {
	        var $radio = $(this).find('input[type=radio]');
	        if (!$radio.prop('checked')) {
	            $radio.trigger('change');
	        }
	    }).on('change', ':radio', function() {
            try{
                var fixedFlag = $(this).attr('data-fixedFlag')
                var randomFlag = $(this).attr('data-randomFlag')
                var fixedAmount = $(this).attr('data-fixedAmount').split(',')
                var randomAmount = $(this).attr('data-randomAmount').split(',')
                $("#amountMoney").attr("disabled",false).css('background','#ffffff')
                $("#pcInputMoney").hide()
                rechargeHtml = ''
                if((fixedFlag == 2 && randomFlag == 1) || (fixedFlag == 2 && randomFlag == 2)){
                    rechargeHtml +='<td></td><td style="text-align: left">'
                    $.each(fixedAmount,function(index,item){
                        rechargeHtml+='<span>'+item+'</span>'
                    })
                    rechargeHtml +='</td><td></td>'
                    $("#pcInputMoney").html(rechargeHtml)
                    $("#pcInputMoney").show()
                    $("#amountMoney").attr("disabled","disabled").css('background','#eeeeee')
                    $("#pcInputMoney span").click(function () {
                        $(this).siblings().removeClass('inputActive')
                        $(this).addClass('inputActive')
                        $("#amountMoney").val($(this).html())
                    })
                } else if(fixedFlag == 1 && randomFlag == 2){
                    var index  = Math.floor(Math.random()*randomAmount.length)
                    rechargeNumber = randomAmount[index]
                }
            }catch (e) {

            }
	        var $it = $(this);$it.prop('checked', true);
	        $it.parent().addClass("curSubrows").siblings().removeClass("curSubrows");
	        $("#min_amount").html($it.attr("min"));
	        $("#max_amount").html($it.attr("max"));
	        $(".li-payCom").html($it.next().attr('title'))
	        $("#dpt_info").removeClass("hidn");
	    });
	    
	    $('#nextBtn').click(function() {
	        var $it=$(this),eleForm = $(this.form),paySetting = eleForm.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=eleForm.find("input[name='amount']"),
	        amount1 =$input.val(),
            amount1 = parseFloat(amount1) + parseFloat(rechargeNumber)
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");

	        // if ($it.attr('disabled') == 'disabled'){
			// 	jishiq()
			// }
	        // function jishiq(){
	        // 	var timess = 20;
	        // 	var times = setInterval(()=>{
			// 		$("#nextBtn").text('请等待' +timess-- + '秒再提交申请' )
			// 		if (timess == 0){
			// 			clearInterval(times)
			// 			$("#nextBtn").text("下一步" )
			// 		}
			// 	},1000)
			// }
	        if(!payId){
	        	alertfocue("请选择支付方式",$it);return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	alertfocue("请输入充值正确金额",$input);
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	alertfocue("充值金额必须不小于"+ min,$input);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        var iconCss = paySetting.attr("iconCss");
	        var payType = paySetting.attr("payType");
	        dptcommit(amount1,payId,iconCss,payType);
	        return false;
		});
	}

	function alertfocue(msg, ele) {
		layer.alert(msg,{icon:2});
		ele.focus();
		$("#nextBtn").removeAttr("disabled");
	}
	function dptcommit(m, payId, iconCss, payType) {

		setInterval(()=>{
			$("#nextBtn").removeAttr("disabled");
		},20000)

		topay(m, payId, iconCss, payType, function(result){
			if(result.success == false){
				alert(result.msg);
			} else if(result.success == true){
				if(result && result.data && result.data.formParams){
					var formHiddens = [];
					for(var key in result.data.formParams){
						var value = result.data.formParams[key];
						formHiddens.push({name: key, value: value});
					}
					sliderBuZhou();//	切换步骤导航
					result.data.formHiddens = formHiddens;
					var html = template('toPayTemplate', result.data);
					$("#depositTabs").html(html);
				}else{
					layer.alert("系统发生错误",{icon:2});
				}
			}
		});
	}
	
	function sliderBuZhou(){
		$("#sliderBuzhou").addClass("substepitmblue2").siblings().removeClass("substepitmblue2");
	}

</script>
<script type="text/html" style="display: none;" id="toPayTemplate">
<form action="{{formAction}}" method="post" target='_blank'>
	{{each formHiddens as item}}
		<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}
	<div class="security-setting" id="dpt_info">
			<table class="table">
							<tbody>
								<tr style="line-height:30px">
									<td class="align-right-col">订单号：</td>
									<td>{{orderId}}</td>
								</tr>
								<tr>
									<td class="align-right-col">会员帐号：</td>
									<td>{{account}}</td>
								</tr>
								<tr style="line-height:30px">
									<td class="align-right-col">充值金额：</td>
									<td><span class="bigtxt red">{{amount}}</span></td>
								</tr>
								<tr class="last-row">
									<td>&nbsp;</td>
									<td colspan="3">
										<button id="nextBtn" class="btn btn-blue">确认提交</button>
									</td>
								</tr>
							</tbody></table>
					</div>	
</form>
</script>
</body></html>