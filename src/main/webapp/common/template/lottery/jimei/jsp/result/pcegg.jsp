<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- PCEGG -->
<c:if test="${not empty page}">
	<c:set var="pagIng" value="${page}"></c:set>
	<table class="awardList">
		<thead>
			<tr>
	            <th rowspan="2">期数</th>
	            <th rowspan="2">时间</th>
	            <th colspan="15">彩球号码</th>
			</tr>
			<tr>
	            <th>一</th>
	            <th>二</th>
	            <th>三</th>
	            <th>和</th>
	            <th>单双</th>
	            <th>大小</th>
	            <th class="ng-binding">
                       <em class="glyphicon glyphicon-chevron-left pull-left"> </em>
                       <span id="jssb31" style="display:inline;">一</span>
                       <span id="jssb32" style="display:none;">二</span>
                       <span id="jssb33" style="display:none;">三</span>
                       <em class="glyphicon glyphicon-chevron-right pull-right"> </em>
	            </th>
	        </tr>
		</thead>
		<tbody>
			<c:forEach items="${page}" var="item" varStatus="i">
			<tr <c:if test="${i.index%2==0}">class="white"</c:if>>
				<td>${item.qiHao}</td>
				<td><fmt:formatDate value="${item.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
				<c:choose>
					<c:when test="${not empty item.haoMa}">
						<c:set var="sum" value="0"></c:set>
						<c:forTokens items="${item.haoMa }" delims="," var="haoMa" varStatus="i">
							<c:set var="sum" value="${sum+haoMa}"></c:set>
							<td><em class="smallRedball">${haoMa}</em></td>
						</c:forTokens>
						<td><em class="smallBlueball">${sum}</em></td>
						<td class="numberDis"><em class="${sum%2==0?'dual':'odd' }">${sum%2==0?'双':'单'}</em></td>
						<td class="numberDis"><em class="${sum>13?'big':'small' }">${sum>13?'大':'小'}</em></td>
						<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
							<c:choose>
								<c:when test="${i.first}">
									<td class="tit jssb3${i.index+1 }"><em class="smallRedball">${haoMa}</em></td>
								</c:when>
								<c:otherwise>
									<td class="tit jssb3${i.index+1 }" style="display:none;"><em class="smallRedball">${haoMa}</em></td>
								</c:otherwise>
							</c:choose>
						</c:forTokens>
					</c:when>
					<c:otherwise>
						<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
					</c:otherwise>
				</c:choose>
			</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>