<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/hisRecord.css?v=111" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>     
	<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
	<script src="${base}/common/js/contants.js?v=2"></script>  
</head>
<body>
			<div class="content">
				<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
				<div class="membersubnavi">
              		<div class="subnavi">
                    	<a href="${base}/center/banktrans/deposit/cPage.do" target="frame">充值</a>
                    </div>
                    <div class="subnavi">
                    	<a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
                    </div> 
                    <div class="subnavi">
                    	<span class="blue">交易历史</span>
                    	<div class="subnaviarrow"></div>
                    </div>
               	</div>
               	<div class="tabs">
					<div class="tab" attrType="1">充值记录</div>
					<div class="tab" attrType="2">取款记录</div>
					<div class="tab" attrType="3">帐变记录</div>
				</div>
               	<div class="chart_table_search_wrapper">
					<ul class="historyNav" style="height:40px;">
						<li class="attr_type_1">
				        	<select name="type" id="type" class="form-control">
				        		<option value="">所有类型</option>
				       			<option value="5">在线入款</option>
				       			<option value="6">快速入款</option>
				       			<option value="7">一般入款</option>
								<option value="801">虚拟货币</option>
							</select>
				        </li>
				        <li class="attr_type_3">
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" id="orderId" value="" type="text" placeholder="订单号"></div></div>
				        </li>
				        <li class="attr_type_1 attr_type_2">
				        	<select name="status" id="state" class="form-control">
				        		<option value="">所有状态</option>
				       			<option value="1">处理中</option>
				       			<option value="2">处理成功</option>
				       			<option value="3">处理失败</option>
							</select>
				        </li>
						<li>
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" name="startDate" id="startTime" value="${startTime}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
				        </li>
				        <li>
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" name="endDate" id="endTime" value="${endTime}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
				        </li>
				        <li>
				        		<button type="button" class="btn btn-red" onclick="search();">搜索</button>
				        </li>
		        		<div class="clearfix"></div>
		    		</ul>
				</div>
				<div class="records" id="record_tb">
        		</div>
        		<div id="pageGro" class="cb" style="display:none;">
        			<input type="hidden" value="" id="pageNumber">
					<div class="allTotal">共<font id="totalCount"></font>条&nbsp;&nbsp;&nbsp;当前第<font id="currentPageNo"></font>页</div>
					<div class="pageLoad"></div>
				</div>
		  </div>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/laypage.js"></script>
<script id="moneytype_tpl" type="text/html">
	{{each data as option}}
        <option value="{{option.id}}">{{option.name}}</option>
	{{/each}}
</script>
<script id="record_tpl" type="text/html">
		<table class="table">
				<tbody>
					<tr>
						{{each colmuns as col}}
							{{if col=='备注'}}
								<th width="550">{{col}}</th>
							{{else}}
								<th style="width:150px">{{col}}</th>
							{{/if}}
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "支付名称", "交易额度", "状态", "订单号", "说明" ],
		2 : [ "交易额度", "状态", "说明" ],
		3 : [ "变动前金额", "变动金额", "变动后余额", "订单号","备注" ]
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "payName", "money", "status", "orderNo", "opDesc" ],
		2 : [ "createDatetime", "type", "drawMoney", "status", "remark" ],
		3 : [ "createDatetime", "type", "beforeMoney", "money", "afterMoney", "orderId","remark"]
	}
	var navUrls = {
		1 : "${base}/center/record/hisrecord/comrd.do",
		2 : "${base}/center/record/hisrecord/drawrd.do",
		3 : "${base}/center/record/mnyrecord/list.do"
	}
	

	function search() {
		initRdsData(curNavFlag);
	}

	var curNavFlag = ${type};
	var first = true;
	$(function(){
		initRdsData(curNavFlag);
		first = false;
		$('#startTime').cxCalendar({format:'YYYY-MM-DD'});
		$('#endTime').cxCalendar({format:'YYYY-MM-DD'});
		$(".tp-ui-icon").click(function(){
			$(this).parent().find("input").focus();
		})
		switch(parseInt(curNavFlag)){
			case 1:
				$(".tabs").find("div:eq(0)").addClass("tabactive");
				$(".attr_type_3").hide();
				$(".attr_type_1").show();
				break;
			case 2:
				$(".tabs").find("div:eq(1)").addClass("tabactive");
				$(".attr_type_1,.attr_type_3").hide();
				$(".attr_type_2").show();
				break;
			case 3:
				$(".tabs").find("div:eq(2)").addClass("tabactive");
				$(".attr_type_1,attr_type_2").hide();
				$(".attr_type_3").show();
				break;
		}
		
		
		$(".tabs div").click(function(){
			var attrType = parseInt($(this).attr("attrType"));
			switch(attrType){
				case 1:
					$(".attr_type_3").hide();
					$(".attr_type_1").show();
					break;
				case 2:
					$(".attr_type_1,.attr_type_3").hide();
					$(".attr_type_2").show();
					break;
				case 3:
					$(".attr_type_1,attr_type_2").hide();
					$(".attr_type_3").show();
					break;
			}
			$("#pageNumber").val(1);	//重新初始化
			$(this).addClass("tabactive").siblings().removeClass("tabactive");
			initRdsData(attrType);
		})
	})
	
	template.helper('$formatter', function(content, key) {

		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		} else if (key == "type") {
			return GlobalTypeUtil.getTypeName(1, 1, content);
		} else if (key == "status") {
			if (curNavFlag == 1) {
				return GlobalTypeUtil.getTypeName(1, 2, content);
			} else if (curNavFlag == 2) {
				return GlobalTypeUtil.getTypeName(1, 3, content);
			}
		} else if(key.toUpperCase().indexOf("MONEY") != -1){
			if(content > 0){
				return '<font color="blue">'+content+'</font>';
			}else{
				return '<font color="red">'+content+'</font>';
			}
		}
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		if(!first){
			pageNumber = parseInt($("#pageNumber").val());
		}
		var type = 0;
		var status = $("#state").val();
		var orderId = $("#orderId").val();
		switch(parseInt(flag)){
			case 1:
				type = $("#type").val();
				break;
			case 2:
				$("#type").val('');
				break;
			case 3:
				type = $("#moneyType").val();
				orderId = $("#orderId").val();
				break;
		}
		$.ajax({
			type:"POST",
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : type,
				status:status,
				orderId:orderId
			},
			success : function(result) {
				layerPage(result);	//分页填充参数
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				var html = template('record_tpl', result);
				$("#record_tb").html(html);
			}
		});
	}
	
	function layerPage(res){
		var count = res.totalCount;
		if(count > 0){
			$("#pageGro").show();
		}else{
			$("#pageGro").hide();
			return;
		}
		$("#totalCount").text(res.totalCount);
		$("#currentPageNo").text(res.currentPageNo);
		$("#pageNumber").val(res.currentPageNo);
		//分页
		laypage({
			  cont: $('.pageLoad'),
			  pages: res.totalPageCount, //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
			  skin: '#777f89', //皮肤
			  curr: function(){ //通过url获取当前页，也可以同上（pages）方式获取
			    var page = res.currentPageNo;
			    return page;
			  }(), 
			  jump: function(e, first){ //触发分页后的回调
			    if(!first){ //一定要加此判断，否则初始时会无限刷新
			    	$("#pageNumber").val(e.curr);
			    	initRdsData(curNavFlag);
			    }
			  }
			});
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}
		return newCols;
	}
	
</script>
</body></html>