<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="header_nav_bg ng-scope">
<div class="header_nav header_nav1">
<header class="index-header ng-scope" id="ticket-base" style="height:120px;margin:0">
	<jsp:include page="base_top_nav.jsp"></jsp:include>
	<jsp:include page="top_nav.jsp"></jsp:include>
</header>
<nav class="navbar navbar-default index-navbar ng-scope" id="lotteryV2Sort">
	<ul class="nav navbar-nav nav-default" style="float:none;display:flex;">
		<c:forEach items="${lotteryGroupSort}" var="sort" varStatus="sortIndex">
		<c:set var="count" value="0"></c:set>
		<c:if test="${sort != 4 }">
			<c:forEach items="${lotteryV2List}" var="lottery">
				<c:if test="${lottery.viewGroup == sort}"><c:set var="count" value="${count+1}"></c:set></c:if>
			</c:forEach>
			<c:if test="${count > 0}">
				<li class="${sortIndex.index == 0?'active':''}">
					<a class="ng-binding" href="javascript:void(0);" group_view="${sort}">${sort==1?'时时彩':sort==2?'低频彩':sort==3?'PK10':sort==5?'11选5':sort==6?'香港彩':sort==7?'快三':sort==8?'快乐十分':'PC蛋蛋'}</a>
				</li>
			</c:if>
		</c:if>
		</c:forEach>
	</ul>
	<div class="menu-item swiper-container"  id="lotteryV2_nav">
		<c:forEach items="${lotteryGroupSort}" var="sort"
						varStatus="sortIndex">
			<c:set var="counts" value="0"></c:set>
			<c:forEach items="${lotteryV2List}" var="lottery">
				<c:if test="${lottery.viewGroup == sort}"><c:set var="counts" value="${counts+1}"></c:set></c:if>
			</c:forEach>
					<c:if test="${counts>0 }">
						<ol style="display: none;" group_view="${sort}">
							<c:forEach items="${lotteryV2List}" var="lottery"
								varStatus="lotIndex">
								<c:if test="${lottery.viewGroup == sort}">
									<li><a lot_code="${lottery.code }"
										lot_type="${lottery.type}">
											<h4>${lottery.name}</h4> <span id="Timer_${lottery.code}">00:00:00</span>
									</a></li>
								</c:if>
							</c:forEach>
						</ol>
					</c:if>
				</c:forEach>
				<script type="text/javascript">
					$(function() {
						LotV2.ajax.groupViewDown(null);
					})
				</script>
	</div> 
</nav>
</div>
</div>