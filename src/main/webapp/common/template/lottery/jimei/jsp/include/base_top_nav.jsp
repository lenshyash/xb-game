<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="margin-top:10px;"><a href="${base}/lotteryV2/index.do"><img src="${base}${caipiaoLogo}" style="width:166px;height:35px;" /></a>
<ul class="nav nav-top pull-right ng-scope">
    <li><a href="${base}/center/member/meminfo/page.do" class="ng-scope" target="newMemin">会员中心</a></li>
    <li><a href="${base}/lotteryV2/resultList.do" target="newResult" class="ng-scope">开奖结果</a></li>
    <li><a href="${base}/lotteryV2/history.do" target="newHistory" class="ng-scope">历史下注</a></li>
    <li><a style="cursor:pointer;" onClick="zxkf();" id="zxkf" target="_blank" class="ng-scope">在线客服</a></li>
    <c:if test="${caipiao == 'off'}">
        <li><a href="${base}/lotteryV2/index.do" class="ng-scope">返回首页</a></li>
    </c:if>
    <c:if test="${caipiao == 'on'}">
        <li><a href="${base}/logout.do" class="ng-scope">退出登录</a></li>
    </c:if>
</ul>
</div>
<script type="text/javascript">
//在线客服
function zxkf(){
	var open = window.open();
	$.ajax({
		url:"${base}/memInfo/common/siteInfo/base.do",
		dataType:"json",
		success:function(j){
			if(j.success){
				var url = j.item;
				if(url.indexOf('http://')==-1 && url.indexOf('https://')==-1){
					url = 'http://' + url;
				}
				open.location = url;
			}else{
				$('#zxkf').attr('href','#').removeAttr('target');
			}
		}
	});
}
</script>