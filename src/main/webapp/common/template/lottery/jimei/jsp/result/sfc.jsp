<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- SFC -->
<c:if test="${not empty page}">
	<c:set var="pagIng" value="${page}"></c:set>
	<table class="awardList">
		<thead>
			<tr>
	            <th rowspan="3">期数</th>
	            <th rowspan="3">时间</th>
	            <th colspan="18">彩球号码</th>
	        </tr>
	        <tr>
	            <th rowspan="2">第一球</th>
	            <th rowspan="2">第二球</th>
	            <th rowspan="2">第三球</th>
	            <th rowspan="2">第四球</th>
	            <th rowspan="2">第五球</th>
	            <th rowspan="2">第六球</th>
	            <th rowspan="2">第七球</th>
	            <th rowspan="2">第八球</th>
	            <th colspan="3">总和</th>
	            <th colspan="5" class="ng-binding">                
	            	<em class="glyphicon glyphicon-chevron-left pull-left"> </em>
	                	<span id="sfc1">第一球</span>
	                	<span id="sfc2" style="display:none;">第二球</span>
	                	<span id="sfc3" style="display:none;">第三球</span>
	                	<span id="sfc4" style="display:none;">第四球</span>
	                	<span id="sfc5" style="display:none;">第五球</span>
	                	<span id="sfc6" style="display:none;">第六球</span>
	                	<span id="sfc7" style="display:none;">第七球</span>
	                	<span id="sfc8" style="display:none;">第八球</span>
	                <em class="glyphicon glyphicon-chevron-right pull-right"> </em>
	           	</th>
	        </tr>
	        <tr>
	        	<th>总和</th>
	        	<th>和单双</th>
	        	<th>和大小</th>
	            <th>单双</th>
	            <th>大小</th>
	            <th>合数</th>
	            <th>中发白</th>
	            <th>方位</th>
	        </tr>
		</thead>
		<tbody>
			<c:forEach items="${pagIng}" var="item" varStatus="i">
				<tr <c:if test="${i.index%2==0}">class="white"</c:if>>
					<td>${item.qiHao}</td>
					<td><fmt:formatDate value="${item.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
					<c:choose>
						<c:when test="${not empty item.haoMa}">
							<c:set var="first" value="0"></c:set>
							<c:set var="last" value="0"></c:set>
							<c:set var="sum" value="0"></c:set>
							<c:forTokens items="${item.haoMa }" delims="," var="haoMa" varStatus="i">
								<c:set var="sum" value="${sum+haoMa}"></c:set>
								<c:if test="${i.first}"><c:set var="first" value="${haoMa}"></c:set></c:if>
								<c:if test="${i.last}"><c:set var="last" value="${haoMa}"></c:set></c:if>
								<c:choose>
								<c:when test="${lotCode == 'CQXYNC'}">
									<td><em class="ball-cqxync cqxync-i${haoMa}"></em></td>
								</c:when>
								<c:otherwise>
									<td><em class="smallRedball">${haoMa}</em></td>
								</c:otherwise>
								</c:choose>
							</c:forTokens>
							<td><em class="smallBlueball">${sum}</em></td>
							<td class="numberDis"><em class="${sum%2==0?'dual':'odd' }">${sum%2==0?'双':'单'}</em></td>
							<td class="numberDis"><em class="${sum>84?'big':'small' }">${sum>84?'大':'小'}</em></td>
							<!-- <td><span class="${first>last?'text-success':'text-danger'}">${first>last?'龙':'虎'}</span></td> -->
							<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
								<c:choose>
									<c:when test="${i.first}">
										<td class="numberDis tit sfc${i.index+1}"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
										<td class="numberDis tit sfc${i.index+1}"><em class="${haoMa>10?'big':'small' }">${haoMa>10?'大':'小'}</em></td>
										<td class="numberDis tit sfc${i.index+1}"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber><em class="${heshu%2==0?'dual':'odd' }">${heshu%2==0?'双':'单'}</em></td>
										<td class="numberDis tit sfc${i.index+1}"><span class="${haoMa<=7?'text-success':haoMa>7 && haoMa<=14?'text-danger':'text-info'} ng-scope">${haoMa<=7?'中':haoMa>7 && haoMa<=14?'发':'白'}</span></td>
										<td class="numberDis tit sfc${i.index+1}"><span class="${haoMa%4==1?'text-success':haoMa%4==2?'text-danger':haoMa%4==3?'text-info':'text-warning'}">${haoMa%4==1?'东':haoMa%4==2?'南':haoMa%4==3?'西':'北'}</span></td>
									</c:when>
									<c:otherwise>
										<td class="numberDis tit sfc${i.index+1}" style="display:none;"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
										<td class="numberDis tit sfc${i.index+1}" style="display:none;"><em class="${haoMa>10?'big':'small' }">${haoMa>10?'大':'小'}</em></td>
										<td class="numberDis tit sfc${i.index+1}" style="display:none;"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber><em class="${heshu%2==0?'dual':'odd' }">${heshu%2==0?'双':'单'}</em></td>
										<td class="numberDis tit sfc${i.index+1}" style="display:none;"><span class="${haoMa<=7?'text-success':haoMa>7 && haoMa<=14?'text-danger':'text-info'} ng-scope">${haoMa<=7?'中':haoMa>7 && haoMa<=14?'发':'白'}</span></td>
										<td class="numberDis tit sfc${i.index+1}" style="display:none;"><span class="${haoMa%4==1?'text-success':haoMa%4==2?'text-danger':haoMa%4==3?'text-info':'text-warning'}">${haoMa%4==1?'东':haoMa%4==2?'南':haoMa%4==3?'西':'北'}</span></td>
									</c:otherwise>
								</c:choose>
							</c:forTokens>
						</c:when>
						<c:otherwise>
							<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
							<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>