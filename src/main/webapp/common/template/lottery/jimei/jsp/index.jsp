<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>${website_name}</title>
	<link rel="shortcut icon" href="${base }/images/logo.ico" type="image/x-icon">
	<link rel="stylesheet" href="${base}/common/bootstrap/3.3.6/css/bootstrap.min.css"  type="text/css">
	<link rel="stylesheet" href="${base}/common/template/lottery/jimei/css/style.css?v=${caipiao_version}11"  type="text/css">
	<link href="${base }/common/css/noscroll.css" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
	<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jquery-browser.js"></script>
	<script src="${base}/common/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		JIMEI = {
				base:'${base}',
				folder:"${stationFolder}",
	    		caipiao_version : '${caipiao_version}',
	    		caipiao:'${caipiao}',
	    		code:'${lotCode}',
	    		template:'v2',
	    		currentLunarYear:${currentLunarYear}
		}
	</script>
	<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/lotteryV2.js?v=${caipiao_version}11.26"></script>
</head>
<input value="${yjf }" style="display:none;" class='danweiIs'>
<div class="noScript" id="not-allow-old-ie" style="display:none">
     <div>
         <h2>不支援的浏览器</h2>
         <p>
             亲，我们不支援 IE 瀏覽器 9 以下的版本！<br>
             您的浏览器为 IE<span class="ie-version"></span> 内核浏览器。
         </p>
         <br>
         瀏覽器資訊:
         <br>
         <p id="ie-agent"></p>
         <br>
         <a href="https://www.baidu.com/s?wd=360%E6%B5%8F%E8%A7%88%E5%99%A8" rel="nofollow" target="_blank">请使用360浏览器，请使用极速模式</a>
     </div>
 </div>
<body class="index-bg">
<!-- 头部 -->
<c:choose>
	<c:when test="${caipiao != 'off'}">
		<jsp:include page="include/other_head.jsp"></jsp:include>
	</c:when>
	<c:otherwise>
		<jsp:include page="include/head.jsp"></jsp:include>
	</c:otherwise>
</c:choose>
<!-- 主体内容 -->
<div class="frame ng-scope">
	<input type="hidden" value="${dxdsSetting}" id="dxdsSetting"/>
	<!-- content_left -->
	<jsp:include page="include/content_left.jsp"></jsp:include>
		<!-- content_right -->
	<jsp:include page="include/content_right.jsp"></jsp:include>
		<!-- loading -->
	<jsp:include page="include/load.jsp"></jsp:include>
<%--	<jsp:include page="/common/modelCommon/cpFrame.jsp"></jsp:include>--%>
	<div style="clear: both;"></div>
</div>
</body>
</html>