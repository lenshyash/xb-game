<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar index-sidebar ng-scope">
    <div class="user-info ng-scope">
	    <div class="user-line user-info-name aside-block-top" style="padding-left:5px">会员：<b class="text-danger ng-binding accountName" id="account"></b><a href="${base}/" target="_blank" id="isLogin" style="font-size: 8px;margin-left: 5px;">[立即登录]</a></div>
	    <div class="user-line user-info-money aside-block-bottom" style="padding-left:5px">余额：
	         <b id="nowMoney" onclick="refreshMoney()" style="cursor: pointer;text-decoration: underline;" class="text-danger ng-binding ng-scope nowMoney"></b>
	     </div>
    </div>
    <div class="user-link user-center-btn ng-scope">
        <a href="${base}/center/banktrans/deposit/page.do" class="bank-savings" target="_blank"></a>
        <a href="${base}/center/banktrans/draw/page.do" class="bank-take" target="_blank"></a>
       <!--  <a href="${base}/center/banktrans/mnychg/page.do" class="money-conversion" target="_blank"></a> -->
        <div class="clearfix"></div>
    </div>

<div class="news ng-scope" ng-if="SiteMessage">
    <div class="news-t text-center">最新公告</div>
    <div class="news-list">
    <marquee vspace="0" hspace="0" id="newMsg" class="ng-binding" align="right" behavior="scroll" style="height: 100%" direction="up" loop="-1" scrollamount="2" scrolldelay="20" onmouseout="this.start()" onmouseover="this.stop()">
    	<%-- ${publicInfo} --%>
    	${lotteryInfo }
    </marquee>
</div>
</div>
<div class="list-bet ng-scope">
    <div class="list-bet-t text-center">最新注单<c:if test="${caipiao == 'off' }"><a href="${base}/lotteryV2/history.do" target="newHistory">(历史下注)</a></c:if></div>
    <div class="list-bet ng-scope newOrderData"></div>
</div>
</div>