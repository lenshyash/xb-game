<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- BJSC -->
<c:if test="${not empty page}">
<c:set var="pagIng" value="${page}"></c:set>
<table class="awardList">
	<thead>
		<tr>
			<th rowspan="3">期数</th>
			<th rowspan="3">时间</th>
			<th colspan="18">彩球号码</th>
		</tr>
		<tr>
            <th rowspan="2">冠</th>
            <th rowspan="2">亚</th>
            <th rowspan="2">季</th>
            <th rowspan="2">四</th>
            <th rowspan="2">五</th>
            <th rowspan="2">六</th>
            <th rowspan="2">七</th>
            <th rowspan="2">八</th>
            <th rowspan="2">九</th>
            <th rowspan="2">十</th>
            <th colspan="3">冠亚和</th>
            <th colspan="3" class="ng-binding">
	            <em class="glyphicon glyphicon-chevron-left pull-left"></em>
	                <span id="bjsc1" style="display:inline;">冠</span>
	                <span id="bjsc2" style="display:none;">亚</span>
	                <span id="bjsc3" style="display:none;">季</span>
	                <span id="bjsc4" style="display:none;">四</span>
	                <span id="bjsc5" style="display:none;">五</span>
	                <span id="bjsc6" style="display:none;">六</span>
	                <span id="bjsc7" style="display:none;">七</span>
	                <span id="bjsc8" style="display:none;">八</span>
	                <span id="bjsc9" style="display:none;">九</span>
	                <span id="bjsc10" style="display:none;">十</span>
	            <em class="glyphicon glyphicon-chevron-right pull-right"></em>
            </th>
        </tr>
		<tr>
            <th>和数</th>
            <th>和单双</th>
            <th>和大小</th>
            <th>单双</th>
            <th>大小</th>
            <th>龙虎</th>
        </tr>
        <tbody>
        	<c:forEach items="${pagIng}" var="item" varStatus="i">
        	<tr <c:if test="${i.index%2==0}">class="white"</c:if>>
        		<td>${item.qiHao}</td>
        		<td><fmt:formatDate value="${item.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
        		<c:choose>
        			<c:when test="${not empty item.haoMa}">
        				<c:set var="sum" value="0"></c:set>
		        		<c:set var="haoMa6" value="0"></c:set>
		        		<c:set var="haoMa7" value="0"></c:set>
		        		<c:set var="haoMa8" value="0"></c:set>
		        		<c:set var="haoMa9" value="0"></c:set>
		        		<c:set var="haoMa10" value="0"></c:set>
		        		<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
        					<c:if test="${i.index == 0 || i.index == 1}">
        						<c:set var="sum" value="${sum+haoMa}"></c:set>
        					</c:if>
        					<c:if test="${i.index == 5}"><c:set var="haoMa6" value="${haoMa}"></c:set></c:if>
        					<c:if test="${i.index == 6}"><c:set var="haoMa7" value="${haoMa}"></c:set></c:if>
        					<c:if test="${i.index == 7}"><c:set var="haoMa8" value="${haoMa}"></c:set></c:if>
        					<c:if test="${i.index == 8}"><c:set var="haoMa9" value="${haoMa}"></c:set></c:if>
        					<c:if test="${i.index == 9}"><c:set var="haoMa10" value="${haoMa}"></c:set></c:if>
        					<td><em class="pk10 b${haoMa}"></em></td>
		        		</c:forTokens>
		        		<td><em class="${sum>11?'smallRedball':'smallBlueball'}">${sum}</em></td>
		        		<td class="numberDis"><em class="${sum%2==0?'dual':'odd' }">${sum%2==0?'双':'单'}</em></td>
		        		<td class="numberDis"><em class="${sum>11?'big':'small' }">${sum>11?'大':'小'}</em></td>
		        		<c:forTokens items="${item.haoMa}" delims="," var="haoMa" varStatus="i">
		        			<c:choose>
		        			<c:when test="${i.first}">
				        		<td class="numberDis tit bjsc${i.index+1}"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
				        		<td class="numberDis tit bjsc${i.index+1}"><em class="${haoMa>5?'big':'small' }">${haoMa>5?'大':'小'}</em></td>
				        		<c:choose>
					        		<c:when test="${i.index<5}">
					        			<td class="numberDis tit bjsc${i.index+1}">
					        				<c:if test="${i.index == 0 }"><em class="${haoMa>haoMa10?'dual':'odd' }">${haoMa>haoMa10?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 1 }"><em class="${haoMa>haoMa9?'dual':'odd' }">${haoMa>haoMa9?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 2 }"><em class="${haoMa>haoMa8?'dual':'odd' }">${haoMa>haoMa8?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 3 }"><em class="${haoMa>haoMa7?'dual':'odd' }">${haoMa>haoMa7?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 4 }"><em class="${haoMa>haoMa6?'dual':'odd' }">${haoMa>haoMa6?'龙':'虎'}</em></c:if>
					        			</td>
					        		</c:when>
					        		<c:otherwise>
					        			<td class="numberDis tit bjsc${i.index+1}"></td>
					        		</c:otherwise>
				        		</c:choose>
			        		</c:when>
			        		<c:otherwise>
			        			<td class="numberDis tit bjsc${i.index+1}" style="display:none;"><em class="${haoMa%2==0?'dual':'odd' }">${haoMa%2==0?'双':'单'}</em></td>
				        		<td class="numberDis tit bjsc${i.index+1}" style="display:none;"><em class="${haoMa>5?'big':'small' }">${haoMa>5?'大':'小'}</em></td>
				        		<c:choose>
					        		<c:when test="${i.index<5}">
					        			<td class="numberDis tit bjsc${i.index+1}" style="display:none;">
					        				<c:if test="${i.index == 0 }"><em class="${haoMa>haoMa10?'dual':'odd' }">${haoMa>haoMa10?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 1 }"><em class="${haoMa>haoMa9?'dual':'odd' }">${haoMa>haoMa9?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 2 }"><em class="${haoMa>haoMa8?'dual':'odd' }">${haoMa>haoMa8?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 3 }"><em class="${haoMa>haoMa7?'dual':'odd' }">${haoMa>haoMa7?'龙':'虎'}</em></c:if>
					        				<c:if test="${i.index == 4 }"><em class="${haoMa>haoMa6?'dual':'odd' }">${haoMa>haoMa6?'龙':'虎'}</em></c:if>
					        			</td>
					        		</c:when>
					        		<c:otherwise>
					        			<td class="numberDis tit bjsc${i.index+1}" style="display:none;"></td>
					        		</c:otherwise>
				        		</c:choose>
			        		</c:otherwise>
			        		</c:choose>
		        		</c:forTokens>
        			</c:when>
        			<c:otherwise>
        				<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
        				<td>?</td><td>?</td><td>?</td><td>?</td><td>?</td><td>?</td>
        			</c:otherwise>
        		</c:choose>
        	</tr>
        	</c:forEach>
        </tbody>
	</thead>
</table>
</c:if>