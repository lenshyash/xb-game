<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="${base }/images/logo.ico" type="image/x-icon">
<title>${website_name}</title>
<link rel="stylesheet" type="text/css" href="${base}/common/template/lottery/jimei/css/main.css?v=${caipiao_version}" />
<link rel="stylesheet" type="text/css" href="${base}/common/template/lottery/jimei/css/balls.css?v=${caipiao_version}" />
<link rel="stylesheet" type="text/css"  href="${base}/common/template/lottery/jimei/css/queueV.css?v=${caipiao_version}"/>
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<c:if test="${domainFolder != 'a010201' && domainFolder != 'a09502' && domainFolder != 'a010101'}">
		<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
	</c:if>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/lotteryV2.js?v=${caipiao_version}11.23"></script>
<link rel="stylesheet" href="${base}/common/bootstrap/3.3.6/css/bootstrap.min.css"  type="text/css">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jquery-browser.js"></script>
<link rel="stylesheet" href="${base}/common/template/lottery/jimei/css/style.css?v=${caipiao_version}"  type="text/css">
<script src="${base}/common/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
		JIMEI = {
				base:'${base}',
				folder:"${stationFolder}",
	    		caipiao_version : '${caipiao_version}',
	    		code:'${lotCode}',
	    		template:'v2_1',
	    		currentLunarYear:${currentLunarYear}
		}
	</script>
</head>
<body class="${empty bgColor?'skin_blue':bgColor}" style="font-size:12px;">
	<jsp:include page="../include_v_1/index_head.jsp"></jsp:include>
	<div id="main">
		<jsp:include page="../include_v_1/content_left.jsp"></jsp:include>
		<style type="text/css">
			.table-bet > thead > tr > th, .table-bet > tbody > tr > th, .table-bet > tfoot > tr > th {
			    color: #762d08;
			}
		</style>
		<div class="vframe">
			<div style="min-width:1200px;height:100%">
			<div style="width:1022px;float:left;height:100%" id="_main_view">
					<!-- 内容添加处.. -->
			</div>
			<div class="queueV">
				<div class="queueV_head"><span>两面长龙排行</span></div>
				<div class="qq-client-list">
					<!-- 长龙数据载入 -->
				</div>
			</div>
			</div>
		</div>
		<jsp:include page="../include_v_1/content_right.jsp"></jsp:include>
	</div>
	<div id="footer">
		<div class="info" style="padding-left:60px;">
		<div style="position:absolute;line-height: 30px;margin-left:-55px;">
			<img src="${base}/common/template/lottery/jimei/images/notice.png" style="width:25px;margin-top:-7px;" />
			公告
		</div>
			<marquee  scrollamount="1" direction=up id="notices" style="height:27px;" onmouseout="this.start()" onmouseover="this.stop()">
				${publicInfo }
			</marquee>
		</div>
		<a class="more" href="javascript:void(0)">更多</a>
	</div>
	<c:if test="${domainFolder == 'a010201'}"><jsp:include page="${base }/member/a010201/ArticleList.jsp"></jsp:include></c:if>
	<c:if test="${domainFolder == 'a010101'}"><jsp:include page="${base }/member/a010101/ArticleList.jsp"></jsp:include></c:if>
<%-- 	<c:if test="${domainFolder == 'a09502'}"><jsp:include page="${base }/member/a09502/ArticleList.jsp"></jsp:include></c:if> --%>
	<jsp:include page="/common/modelCommon/cpFrame.jsp"></jsp:include>
	<!-- 红包功能 -->
	<c:if test="${isRedPacket eq 'on'}">
		<script src="${base}/common/js/redpacket/redbag.js?v=1.2.2" path="${base}"></script>
	</c:if>
	<script>
	$(function(){
		scollPublicInfo();
		kf();
	});
</script>
</body>
</html>
