<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/onlinedeposit.css?v=43803320234" rel="stylesheet">
	<link rel="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
				<jsp:include page="../../include_v_1/base_head_info.jsp"></jsp:include>
				<div class="membersubnavi">
                      		<div class="subnavi">
                            	<span class="blue">充值</span>
                            	<div class="subnaviarrow"></div>
                            </div>
                            <div class="subnavi">
                            	<a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
                            </div> 
                            <div class="subnavi">
                            	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                            </div>
               	</div>
            <div class="steps" style="height:40px;">
			<div class="substeps">
				<div class="substepitmgray1">
					<b>01</b> 选择支付模式
				</div>
				<div class="substepitmgray2 substepitmblue2">
					<b>02</b> 填写付款人资料
				</div>
				<div class="substepitmgray2" id="sliderBuzhou">
					<b>03</b> 确认您的转账资料
				</div>
				<div class="substepitmgray2">
					<b>04</b> 完成
				</div>
			</div>
			</div>
			<style>
				.form-control{width:100%;}
				.account-content table td {
					    padding: 5px 0 5px 10px;
					}
				.table tbody td{border-bottom:1px solid #ffffff;}
				.table .last-row td{border:none;}
				.btn{padding:4px 25px;margin-right:40px;}
			</style>
			<div class="account-content" id="depositTabs">
					
			</div>
	</div>
<script id="inputdata_tpl" type="text/html">
{{if banks && banks.length>0}}
	<form id="onlinePayAFormId">
<div class="subrow">
	<div class="subcol2">
		{{each banks as bank count}}
			{{if count > 0 && count %4 ==0}}
				<br>
			{{/if}}
			<div class="banklist marginbtm10">
				<input name="bankId" min="{{bank.min}}" max="{{bank.max}}" iconCss="{{bank.iconCss}}" value="{{bank.id}}" class="bankradiobtn" type="radio">
				{{if bank.icon}}
					<div class="zhifuImg" title="{{bank.payName}}"><img src="${base}{{if bank.icon}}{{bank.icon}}{{/if}}" title="{{bank.payName}}" height="47"  width="123"></div>
				{{else}}
					<span title="{{bank.payName}}" class="bankicon {{bank.iconCss}}" style="width:123px;display:table-caption">{{bank.payName}}</span>
				{{/if}}
			</div>
		{{/each}}
	</div>
</div>
<div class="security-setting hidn" id="dpt_info">
<table class="table">
	<tbody>
		<tr style="line-height:30px">
			<td class="align-right-col">会员帐号：</td>
			<td>${loginMember.account}</td>
			<td></td>
			<td rowspan="9" width="50%">
			<div style="line-height: 28px;padding: 10px;">
			<b>自动充值用户须知:</b>
			<br> 
			1. 客服上班时间为：<span style="color: blue;">${kfsb}至${kfxb}</span>;
			<br>
			2. 转账成功后若超过五分钟未上分，请立即联系客服
			</div>
			</td>
		</tr>
<c:if test="${showPayInfo}"><tr style="line-height:30px">
	<td class="align-right-col">充值银行：</td>
	<td colspan="2" id="pay_type_name"></td>
</tr>
<tr>
	<td class="align-right-col">收款姓名：</td>
	<td id="Bname_info" colspan="2"></td>
</tr>
<tr>
	<td class="align-right-col">收款帐号：</td>
	<td id="Baccount_info" colspan="2"></td>
</tr>
<tr style="line-height:30px">
	<td class="align-right-col">开户网点：</td>
	<td colspan="2" id="bankAddress_id"></td>
</tr></c:if>
		<tr>
			<td class="align-right-col">充值金额：</td>
			<td style="width:10%;">
				<input class="form-control" id="amount" type="text">
			</td>
			<td id="dan_bi_limit"></td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">充值金额(大写)：</td>
			<td colspan="2"><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
		</tr>
		<tr>
			<td class="align-right-col">存款人姓名：</td>
			<td style="width:10%;"><input class="form-control" id="depositor" type="text"></td>
			<td></td>
		</tr>
		<tr class="last-row">
			<td>&nbsp;</td>
			<td colspan="2">
				<button id="nextBtn" class="btn btn-blue"><c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if></button>
			</td>
		</tr>
	</tbody></table>
<div class="wxts-div">
	<span class="red">&nbsp;贴心提醒：冲值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</span>
</div>
</div>
</form>
{{else}}
	<div style="margin-left:225px;font-weight: bold;"><span class="red">*</span>友情提示：暂时没有配置充值入口，请更换其他方式继续充值</div>
{{/if}}
</script>
<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
<script type="text/javascript">
var banks = [];
$(function(){
	var convertCurrency=function(a) {
        var e, g, c, h, l, m, q, t, r, y, w;
        a = a.toString();
        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
        a = a.replace(/,/g, "");
        a = a.replace(/^0+/, "");
        if (9.999999999999E10 < Number(a)) return layer.alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
        e = a.split(".");
        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
        h = ["", "\u62fe", "\u4f70", "\u4edf"];
        l = ["", "\u4e07", "\u4ebf"];
        m = ["", ""];
        g = "";
        if (0 < Number(a)) {
            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
            g += ""
        }
        if ("" != e)
            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
        "" == g && (g = "\u96f6");
        "" == e && (g += "");
        return "" + g
    };
	$.ajax({
		url : "${base}/center/banktrans/deposit/dptinitc.do",
		success : function(result) {
			var html = template('inputdata_tpl', result);
			$("#depositTabs").html(html);
			banks = result.banks;
			bindIptData();
		}
	});
	function bindIptData(){
	    var $from = $("#onlinePayAFormId");
	    $from.on('keyup',"#amount", function(){
	        /* 取出输入的金额,并转换成中文 */
	        $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
	    $('.banklist').click(function(){
			var $it=$(this),id=$it.find("input").val();
			$it.find("input").prop('checked', true);
			if(!id)return false;
			$it.addClass("curSubrows").siblings().removeClass("curSubrows");
	        $("#dpt_info").removeClass("hidn");
			for (var i = 0; i < banks.length; i++) {
				if(banks[i].id == id){
					curPay = banks[i];
					break;
				}
			}
			<c:if test="${showPayInfo}">
			$("#pay_type_name").html(curPay.payName);
			$("#bankAddress_id").html(curPay.bankAddress);
			$('#Bname_info').html(curPay.creatorName);
			$('#Baccount_info').html(curPay.bankCard+"<span style='color:red;font-size:15px;'>（转账后请输入金额提交订单）</span>");
			</c:if>
	       	$('#dan_bi_limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+curPay.min+'</span> 元 ，最高<span id="max_amount" class="red">'+curPay.max+'</span> 元）');
		});
	    $('#nextBtn').click(function() {
	    	 var $it=$(this),eleForm = $(this.form),bank = eleForm.find("input[name='bankId']:checked"),
	         bankId =bank.val(),
	         $input=eleForm.find("#amount"),
	         amount1 =$input.val(),
	         depositor = eleForm.find("#depositor").val(),
	         min = bank.attr("min"),max = bank.attr("max");
	    	 if($it.attr("disabled")){
	           	return false;
           }
           $it.attr("disabled","disabled");
	    	 if(!bankId){
	         	alertfocue("请选择支付银行",$it);return false;
	         }
	    	 try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	alertfocue("请输入正确金额",$input);
	            return false;
	        }
	         amount1 = parseFloat(amount1,10);
	         if(amount1<min){
	        	 alertfocue("充值金额必须不小于"+ min,$input);
	             return false;
	         }
	         if(max>0 && amount1 > max){
	        	alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        if (!depositor ) {
	        	alertfocue("请输入存款人姓名",eleForm.find("#depositor"));
	            return false;
	        }
	        $.ajax({
				url : "${base}/center/banktrans/deposit/dptcommitc.do",
				data : {
					money : amount1,
					depositor : depositor,
					bankId : bankId
				},
				method:"POST",
				success : function(result) {
					if(!result.success && result.msg){
						alert(result.msg);
						$('#nextBtn').removeAttr("disabled");
						return;
					}
					var curBank = {};
					for (var i = 0; i < banks.length; i++) {
						if(banks[i].id == bankId){
							curBank = banks[i];
							break;
						}
					}
					curBank.money =amount1;
					curBank.orderNo = result;
					$("#depositTabs").html(template('confirm_tpl', curBank));
					$("#sliderBuzhou").addClass("substepitmblue2").siblings().removeClass("substepitmblue2");
					bindCopy(curBank);
				}
			});
	        return false;
		});
	}
	function alertfocue(msg, ele) {
		alert(msg);
		ele.focus();
		$('#nextBtn').removeAttr("disabled");
	}
});
function Go(url){
	window.location.href=url;
}
function bindCopy(curBank) {
	$("#Bname").zclip({
        path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.creatorName,
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Baccount").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.bankCard,
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Bamount").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.money+"",
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Bamount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Boid").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.orderNo+"",
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
}
</script>

<script id="confirm_tpl" type="text/html">
<div class="security-setting" id="dpt_info">
<table class="table">
	<tbody>
		<tr style="line-height:30px">
			<td class="align-right-col">充值银行：</td>
			<td>{{payName}}</td>
			<td></td>
		</tr>
		<tr>
			<td class="align-right-col">收款姓名：</td>
			<td width="500">{{creatorName}}</td>
			<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="btn btn-red">复&nbsp;制</button>
				<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
				</div>
			</td>
		</tr>
		<tr>
			<td class="align-right-col">收款帐号：</td>
			<td width="500">{{bankCard}}</td>
			<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
			</div>
			</td>
		</tr>
		<tr style="line-height:30px">
			<td class="align-right-col">开户网点：</td>
			<td>{{bankAddress}}</td>
			<td></td>
		</tr>
		<tr>
			<td class="align-right-col">订单号：</td>
			<td width="500"><span class="blue">{{orderNo}}</span></td>
			<td style="text-align: left;position: relative;"><button id="Boid" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
		</div></td>
		</tr>
		<tr class="last-row">
			<td class="align-right-col">充值金额：</td>
			<td width="500"><span class="red">{{money}}</span></td>
			<td style="text-align: left;position: relative;"><button id="Bamount" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
		</div></td>
		</tr>
	</tbody></table>
</div>
<div class="wxts-div">
	<span class="red">&nbsp;贴心提醒：冲值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</span>
</div>
<div class="wxts-div">
	<button type="button" class="btn btn-red marginBtn" onclick="javascript:Go('${base}/center/record/hisrecord/cPage.do');">确定</button>
</div>
</script>
</body></html>