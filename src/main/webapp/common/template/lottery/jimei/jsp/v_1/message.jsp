<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/css/member.css?v=34341990dd" rel="stylesheet">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
</head>
<body>
<div class="content">
			<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
			<div class="membersubnavi">
           		<div class="subnavi">
                 	<a href="${base}/center/member/meminfo/cPage.do" target="frame">基本信息</a>
                 </div>
                 <!-- <div class="subnavi">
                 	<span>帐号设置</span>
                 </div> -->
                 <div class="subnavi">
                 	<span class="blue">站内信</span>
                 	<div class="subnaviarrow"></div>
                 </div>
            </div>
			<div class="account-content-container" id="record_tb">
					
			</div>
				<div id="pageGro" class="cb" style="display:none;">
        			<input type="hidden" value="" id="pageNumber">
					<div class="allTotal">共<font id="totalCount"></font>条&nbsp;&nbsp;&nbsp;当前第<font id="currentPageNo"></font>页</div>
					<div class="pageLoad" style="margin-left:0px;"></div>
				</div>
	</div>
	<script id="record_tpl" type="text/html">
	<div class="account-content">
						<h3 class="title"><span class="titleMsg">消息中心</span><div class="statusMsg">未读 <span class="badge" id="badge">0</span></div></h3>
						<div class="notification-list">
							<table class="table">
								<thead>
									<tr>
										<th bgcolor="#217eec">状态</th>
										<th>短信标题</th>
										<th>发布时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									{{each list as row i}}
									<tr>
										<td>{{$formatter row.status 'status'}}</td>
										<td><a href="javascript:void(0);" onclick="readMsg({{i}});">{{row.title}}</a></td>
										<td>{{$formatter row.createDatetime 'time'}}</td>
										<td><a href="javascript:void(0);" onclick="readMsg({{i}});">详情</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="delMsg({{row.userMessageId}});">删除</a></td>
									</tr>
									{{/each}}
								</tbody>
							</table>
						</div>
					</div>
</script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/laypage.js"></script>
	<script type="text/javascript">
	var okRead = 0;
	var pageSize = 10;
	var pageNumber = 1;
	var first = true;
	var pageDatas = [];
	$(function() {
		searchMsg();
		first = false;
	});

	function searchMsg() {
		okRead = 0;	//每次执行前初始化
		if (!first) {
			pageNumber = parseInt($("#pageNumber").val());
		}

		$.ajax({
			url : "${base}/center/news/message/list.do",
			data : {
				pageNumber : pageNumber,
				pageSize : pageSize
			},
			success : function(result) {
				pageDatas = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);
				layerPage(result);
			}
		});
	}
	
	function layerPage(res){
		var count = res.totalCount;
		if(count > 0){
			$("#pageGro").show();
		}else{
			$("#pageGro").hide();
			return;
		}
		$(".badge").text(okRead);
		$("#totalCount").text(res.totalCount);
		$("#currentPageNo").text(res.currentPageNo);
		$("#pageNumber").val(res.currentPageNo);
		//分页
		laypage({
			  cont: $('.pageLoad'),
			  pages: res.totalPageCount, //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
			  skin: '#777f89', //皮肤
			  curr: function(){ //通过url获取当前页，也可以同上（pages）方式获取
			    var page = res.currentPageNo;
			    return page;
			  }(), 
			  jump: function(e, first){ //触发分页后的回调
			    if(!first){ //一定要加此判断，否则初始时会无限刷新
			    	$("#pageNumber").val(e.curr);
			    	searchMsg();
			    }
			  }
			});
	}
	template.helper('$formatter', function(content, key) {
		if (key == 'time') {
			return DateUtil.formatDatetime(content);
		} else if (key == 'status') {
			if (content == '2') {
				return '<span style="color:blue">已读</span>';
			} else {
				okRead++;
				return '<span style="color:red">未读</span>';
			}
		}
	});

	function readMsg(index) {
		var row = pageDatas[index];
		$.ajax({
					url : "${base}/center/news/message/read.do",
					data : {
						id : row.id
					},
					success : function(result) {
						layer
								.open({
									type : 1, //page层
									area : [ '500px', '450px' ],
									title : row.title,
									shade : 0.6, //遮罩透明度
									scrollbar : false,
									offset : '20px',//距离顶部
									moveType : 0, //拖拽风格，0是默认，1是传统拖动
									shift : 1, //0-6的动画形式，-1不开启
									content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'
											+ row.message + '</div>'
								});
						searchMsg();
					}
				});

	}

	function delMsg(id) {
		layer.confirm("确定要删除此信吗？",{btn:['确定','取消']},function(){
			$.ajax({
				url : "${base}/center/news/message/delete.do",
				data : {
					id:id
				},
				success : function(result) {
					if(!result.success){
						layer.msg(result.msg,{icon:6});
					}else{
						layer.alert("删除成功！",{icon:1},function(index){
							searchMsg();
							layer.close(index);
						});
					}
				}
			});
		})
	}
</script>
</div>
</body>
</html>