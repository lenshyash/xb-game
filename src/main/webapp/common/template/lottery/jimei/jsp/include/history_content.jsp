<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<link href="${base}/common/template/lottery/jimei/css/history.css" rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/css/test.css" rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/js/assets/css/amazeui.min.css" rel="stylesheet">
<form action="${base}/lotteryV2/history.do" method="post" id="lotteryV2OrderForm">
<input type="hidden" name="pageSize" value="${page.pageSize}">
<input type="hidden" name="pageNumber" value="${page.currentPageNo}">
<section class="main-result" id="_main_view">
		<div class="title">
			<strong>最新下注记录</strong>
		</div>
		<div class="todayAward">
			<div class="detail"><strong>彩种分类：</strong>
				<div style="float:right;width:89%;">
				<c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
					<c:choose>
						<c:when test="${not empty param.lotCode && param.lotCode == lot.code}">
							<c:set var="lotName" value="${lot.name}"></c:set>
							<a javascript="void(0);" href="${base}/lotteryV2/history.do?lotCode=${lot.code}" class="on">${lot.name} </a>
						</c:when>
						<c:when test="${lot.code=='LHC'}">
							<c:if test="${mark_on_off == 'on'}">
								<a javascript="void(0);" href="${base}/lotteryV2/history.do?lotCode=${lot.code}">${lot.name} </a>
							</c:if>
						</c:when>
						<c:otherwise>
								<a javascript="void(0);" href="${base}/lotteryV2/history.do?lotCode=${lot.code}">${lot.name} </a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				</div>
			</div>
		</div>
		<div class="title">
			<strong><c:if test="${empty lotName }">全部</c:if><c:if test="${not empty lotName }">${lotName}</c:if></strong>
			<c:if test="${caipiao == 'on'}">
				<a class="betBtn" href="${base}/lotteryV2/index.do?lotCode=${param.lotCode}">立即投注</a>
			</c:if>
			<c:if test="${caipiao == 'off' }">
			<a class="betBtn" href="${base}/index/games.do?lotCode=${param.lotCode}">立即投注</a>
			</c:if>
			<div class="search_right">
					<input name="button" value="刷新" onclick="window.location.reload()" class="refresh_button" type="button">
			</div>
		</div>
		<div class="chart_table_search_wrapper">
			<ul class="historyNav" style="height:40px;">
		<li>
        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar"  name="startDate" id="startDate" value="<fmt:formatDate value="${startDate}" var="startTime" pattern="yyyy-MM-dd" />${startTime}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
        </li>
        <li>
        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar"  name="endDate" id="endDate" value="<fmt:formatDate value="${endDate}" var="endTime" pattern="yyyy-MM-dd" />${endTime}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
        </li>
        <li>
        	<span style="width:160px;">
        		<button type="button" class="btn" onclick="quickSelDate(1)">今天</button>
        		<button type="button" class="btn" onclick="quickSelDate(2)">昨天</button>
        		<button type="button" class="btn" onclick="quickSelDate(3)">本周</button>
        		<button type="button" class="btn" onclick="quickSelDate(4)">上周</button>
        		<button type="button" class="btn" onclick="quickSelDate(5)">本月</button>
        		<button type="button" class="btn" onclick="quickSelDate(6)">上月</button>
        	</span>
        </li>
        <li>
            <select name="lotCode" data-am-selected="{btnWidth: '150px', btnSize: 'lg',maxHeight: 300}">
            	<option value="all">全部彩种</option>
	            <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
	            	<c:choose>
	            		<c:when test="${not empty param.lotCode && param.lotCode == lot.code}">
	            			<option value="${lot.code}" selected="selected">${lot.name}</option>
	            		</c:when>
	            		<c:when test="${lot.code == 'LHC'}">
	            			<c:if test="${mark_on_off == 'on'}">
	            				<option value="${lot.code}">${lot.name}</option>
	            			</c:if>
	            		</c:when>
	            		<c:otherwise>
	            			<option value="${lot.code}">${lot.name}</option>
	            		</c:otherwise>
	            	</c:choose>
	            </c:forEach>
            </select>
        </li>
        <li>
        	<select data-am-selected="{btnWidth: '100px', btnSize: 'lg'}" name="orderStatus">
        		<option value="-1">开奖状态</option>
       			<option value="1" <c:if test="${param.orderStatus == 1}">selected="selected"</c:if>>未开奖</option>
       			<option value="2" <c:if test="${param.orderStatus == 2}">selected="selected"</c:if>>已中奖</option>
       			<option value="3" <c:if test="${param.orderStatus == 3}">selected="selected"</c:if>>未中奖</option>
       			<option value="4" <c:if test="${param.orderStatus == 4}">selected="selected"</c:if>>已撤单</option>
			</select>
        </li>
        <li>
       		<span>
       			<input class="form-control order-control" value="<c:if test="${not empty param.orderId }">${param.orderId}</c:if>" name="orderId" placeholder="输入订单号" maxlength="36" style="width:130px;">
       		</span>
        </li>
        <li>
        	<span>
        		<button type="button" class="btn btn-info orderSearch" id="orderSearch">订单查询</button>
        	</span>
        </li>
        <div class="clearfix"></div>
    </ul>
		</div>
		<div class="rule-content">
		<table class="awardList">
			<thead>
				<tr style="height:50px;">
					<th rowspan="2">注单号</th>
					<th rowspan="2">下注时间</th>
					<th rowspan="2">彩种</th>
					<th rowspan="2">玩法</th>
					<th rowspan="2">期号</th>
					<th rowspan="2">下注号码</th>
					<th rowspan="2">开奖号码</th>
					<th rowspan="2">总额</th>
					<th rowspan="2">中奖金额</th>
					<th rowspan="2">状态</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
	        	<c:when test="${not empty page.list}">
	        	<c:forEach var="order" items="${page.list}" varStatus="orIndex">
	                <tr <c:if test="${orIndex.index%2==0}">class="white"</c:if>>
	                    <td><a class="showOrder" order_id="${order.orderId}" lot_code="${order.lotCode}">${order.orderId}</a></td>
	                    <td><fmt:formatDate value="${order.createTime}" var="createTime" pattern="yyyy-MM-dd HH:mm:ss"/>${createTime}</td>
	                   	<td>${order.lotName}</td>
	                   	<td>${order.playName}</td>
	                   	<td>${order.qiHao}</td>
	                   	<td style="white-space:nowrap;overflow:hidden; text-overflow:ellipsis;">${order.haoMa}</td>
	                   	<td><c:choose><c:when test="${empty order.lotteryHaoMa}">- -</c:when><c:otherwise>${order.lotteryHaoMa}</c:otherwise></c:choose> </td>
	                   	<td><fmt:formatNumber value="${order.buyMoney}" var="buyMoney" pattern="0.00" maxFractionDigits="2" />${buyMoney}</td>
	                   	<td><c:choose><c:when test="${empty order.winMoney || order.winMoney == '0'}">0.00</c:when><c:otherwise><fmt:formatNumber var="winMoney" value="${order.winMoney}" pattern="0.00" maxFractionDigits="2" />${winMoney}</c:otherwise></c:choose></td>
	                   	<td>
	                   		<c:choose>
	                   			<c:when test="${order.status == 1}"><span class="label label-primary">未开奖</span><c:if test="${!unCancleOrder}"><a class="ceDan" order_id="${order.orderId}" lot_code="${order.lotCode}">(撤单)</a></c:if></c:when>
	                   			<c:when test="${order.status == 2}"><span class="label label-success">已中奖</span></c:when>
	                   			<c:when test="${order.status == 3}"><span class="label label-danger">未中奖</span></c:when>
	                   			<c:when test="${order.status == 4}"><span class="label label-info">撤单</span></c:when>
	                   			<c:when test="${order.status == 5}"><span class="label label-danger">回滚成功</span></c:when>
	                   			<c:when test="${order.status == 6}"><span class="label label-success">回滚异常</span></c:when>
	                   			<c:when test="${order.status == 7}"><span class="label label-success">开奖异常</span></c:when>
	                   			<c:when test="${order.status == 8}"><span class="label label-success">和局</span></c:when>
	                   		</c:choose>
	                   	</td>
	                </tr>
	           </c:forEach>
	           </c:when>
	           <c:otherwise>
	        	<tr>
	        		<td colspan="10">没有符合查询的资料...</td>
	        	</tr>
	        	</c:otherwise>
	        </c:choose>
	        <c:choose>
	        	<c:when test="${not empty page.list}">
	        		<tr class="white">
			        	<td colspan="7">小计</td>
			        	<td style="font-weight: bold;color:red;">${subBuyMoney}</td>
			        	<td style="font-weight: bold;color:red;">${subWinMoney}</td>
			        	<td></td>
			        </tr>
			        <tr class="white">
			        	<td colspan="7">总计</td>
			        	<td style="font-weight: bold;color:red;">${sumBuyMoney}</td>
			        	<td style="font-weight: bold;color:red;">${sumWinMoney}</td>
			        	<td></td>
			        </tr>
	        	</c:when>
	        </c:choose>
			</tbody>
		</table>
	</div>
	<c:if test="${page.totalCount > 0}">
	<div id="pageGro" class="cb">
		<input type="hidden" value="${page.totalPageCount}" id="hPage" />
		<div class="allTotal">共<font>${page.totalCount}</font>条&nbsp;&nbsp;&nbsp;当前第<font>${page.currentPageNo}</font>页</div>
		<div class="pageLoad"></div>
	</div>
	</c:if>
	</section>
</form>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/skin/laypage.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/assets/js/amazeui.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/laypage.js"></script>
<script type="text/javascript">
	$(function(){
		$('select').selected();
		$('#startDate').cxCalendar({format:'YYYY-MM-DD'});
		$('#endDate').cxCalendar({format:'YYYY-MM-DD'});
		
		$(".tp-ui-icon").click(function(){
			$(this).parent().find("input").focus();
		})
		
		//分页
		laypage({
			  cont: $('.pageLoad'),
			  pages: $("#hPage").val(), //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
			  skin: '#AF0000', //皮肤
			  curr: function(){ //通过url获取当前页，也可以同上（pages）方式获取
			    var page = $("input[name=pageNumber]").val();
			    return page;
			  }(), 
			  jump: function(e, first){ //触发分页后的回调
			    if(!first){ //一定要加此判断，否则初始时会无限刷新
			      //location.href = '${base}/lotteryV2/history.do?pageNumber='+e.curr;
			      $("input[name=pageNumber]").val(e.curr);
			      $("#lotteryV2OrderForm").submit();
			    }
			  }
			});
	})
</script>