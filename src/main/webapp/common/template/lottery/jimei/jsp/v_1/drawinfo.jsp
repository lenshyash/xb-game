<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/draw.css" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>       
</head>
<body>
			<div class="content">
				<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
				<div class="membersubnavi">
               		<div class="subnavi">
                     	<a href="${base}/center/banktrans/deposit/cPage.do" target="frame">充值</a>
                     </div>
                     <div class="subnavi">
                     	<span class="blue">提款</span>
                     	<div class="subnaviarrow"></div>
                     </div> 
                     <div class="subnavi">
                     	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                     </div>
               	</div>
                <div class="stepswithdraw">
                	<div class="substeps">
                    	<div class="substepitmgray1">
                        	<b>01</b>  设置提现密码
                        </div>
                        <div class="substepitmgray2 substepitmblue2">
                        	<b>02</b>  完善出款银行信息
                        </div>
                        <div class="substepitmgray2">
                        	<b>03</b>  取款
                        </div>
                    </div>
                    <div class="line2"></div>
                </div>
             	<style>
             		.account-content table td {
						    padding: 5px 0 5px 10px;
						}
						.form-control{width:50%;}
             	</style>
               <div class="account-content-container">
					<div class="account-content">
						<div class="security-setting">
							<table class="table" id="savebank">
								
							</table>
						</div>

					</div>
				</div>
		  </div>
<script type="text/javascript">
	$(function(){
		initDrawdata();
	})

	var checkflag = false;
	function as() {
		if (checkSubmit()) {
			if (!checkflag) {
				checkflag = true;
				submit()
			} else {
				layer.alert("请勿重复提交",{icon:2});
			}
		}
	}

	function initDrawdata() {
		$.ajax({
			url : "${base}/center/banktrans/draw/drawbkinfo.do",
			success : function(result) {
				var html = template('draw_tpl', result);
				$("#savebank").html(html);
				$("#bankName").change(function() {
					var isQita = this.value;
					if(isQita == 'qita'){
						$("#bankName2").show();
					}else{
						$("#bankName2").val('');
						$("#bankName2").hide();
					}
				});
			}
		});
	}

	function submit() {
		var cashBankname = $("#bankName").val();
		var sheng = $("#province").val();
		var city = $("#city").val();
		var bankcardaddress = $("#bankcardaddress").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var userName = $("#accountname").val();

		
		var qita = $('#bankName2').val();
		
		if(qita){
			cashBankname = qita;
		}else{
			if (!cashBankname) {
				alert("开户银行不能为空");
				return false;
			}else if(cashBankname == 'qita'){
				if(!qita){
					alert("开户银行不能为空");
					return false;
				}
			}
		}
		
		var param = {};
		param["bankName"] = cashBankname;
		param["userName"] = userName;
		param["province"] = sheng;
		param["city"] = city;
		param["bankAddress"] = bankcardaddress;
		param["cardNo"] = cashBankaccount;
		param["repPwd"] = cashPassword;
		$.ajax({
					url : "${base}/center/banktrans/draw/cmitbkinfo.do",
					data : param,
					type : 'POST',
					success : function(result) {
						if(!result.success){
							layer.alert(result.msg,{icon:2});
						}else{
							layer.alert("绑定成功啦",{icon:1},function(index){
								window.location.reload();
								layer.close(index);
							})
						}
					},
					complete:function(){
						checkflag = false;
					}
				});
	}

	function checkSubmit() {
		var cashBankname = $("#bankName").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var exp = /^([1-9][\d]{1,18}|x|X)$/;
		
		var qita = $('#bankName2').val();
		
		if(qita){
			cashBankname = qita;
		}else{
			if (!cashBankname) {
				alert("开户银行不能为空");
				return false;
			}else if(cashBankname == 'qita'){
				if(!qita){
					alert("开户银行不能为空");
					return false;
				}
			}
		}

		if (!cashBankaccount) {
			layer.alert("银行账号不能为空",{icon:2});
			return false;
		}
		if (!exp.test(cashBankaccount)) {
			layer.alert("请输入正确的银行账号",{icon:2});
			return false;
		}
		if (!cashPassword) {
			layer.alert("提现密码不能为空",{icon:2});
			return false;
		}
		return true;
	}
</script>
<script id="draw_tpl" type="text/html">
	<tbody>
		<tr>
			<td class="align-right-col">真实姓名：</td>
			<td>
				<input id="accountname" {{if userName}}disabled{{/if}} value="{{userName}}" class="form-control" type="text">
			</td>
			<td rowspan="7" width="50%">
				<div style="line-height: 24px;padding: 10px;">
				<b>备注:</b><br> 1. 标记有 <font color="red">*</font> 者为必填项目。<br> 2. 手机写取款 密码为取款 金额时的凭证请会员务必填写详细资料。
				</div>
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>出款银行：</td>
			<td>
				<select id="bankName" class="form-control">
					<option value="建设银行">建设银行</option>
					<option value="工商银行" selected="selected">工商银行</option>
					<option value="农业银行">农业银行</option>
					<option value="中国邮政银行">中国邮政银行</option>
					<option value="中国银行">中国银行</option>
					<option value="中国招商银行">中国招商银行</option>
					<option value="中国交通银行">中国交通银行</option>
					<option value="中国民生银行">中国民生银行</option>
					<option value="中信银行">中信银行</option>
					<option value="中国兴业银行">中国兴业银行</option>
					<option value="浦发银行">浦发银行</option>
					<option value="平安银行">平安银行</option>
					<option value="华夏银行">华夏银行</option>
					<option value="广州银行">广州银行</option>
					<option value="BEA东亚银行">BEA东亚银行</option>
					<option value="广州农商银行">广州农商银行</option>
					<option value="顺德农商银行">顺德农商银行</option>
					<option value="北京银行">北京银行</option>
					<option value="杭州银行">杭州银行</option>
					<option value="温州银行">温州银行</option>
					<option value="上海农商银行">上海农商银行</option>
					<option value="中国光大银行">中国光大银行</option>
					<option value="渤海银行">渤海银行</option>
					<option value="浙商银行">浙商银行</option>
					<option value="晋商银行">晋商银行</option>
					<option value="汉口银行">汉口银行</option>
					<option value="上海银行">上海银行</option>
					<option value="广发银行">广发银行</option>
					<option value="深圳发展银行">深圳发展银行</option>
					<option value="东莞银行">东莞银行</option>
					<option value="宁波银行">宁波银行</option>
					<option value="南京银行">南京银行</option>
					<option value="北京农商银行">北京农商银行</option>
					<option value="重庆银行">重庆银行</option>
					<option value="广西农村信用社">广西农村信用社</option>
					<option value="吉林银行">吉林银行</option>
					<option value="江苏银行">江苏银行</option>
					<option value="成都银行">成都银行</option>
					<option value="尧都区农村信用联社">尧都区农村信用联社</option>
					<option value="浙江稠州商业银行">浙江稠州商业银行</option>
					<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
					<option value="qita">其他</option>
				</select>
				<input class="form-control" name="bankName" placeholder="请输入银行名称" id="bankName2" style="display:none;"/>
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>省份：</td>
			<td>
				<input id="province" class="form-control" type="text">
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>城市：</td>
			<td>
				<input id="city" class="form-control" type="text">
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>开户网点：</td>
			<td>
				<input id="bankcardaddress" class="form-control" type="text">
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>银行帐号：</td>
			<td>
				<input id="bankId" class="form-control" type="text">
			</td>
		</tr>
		<tr>
			<td class="align-right-col"><font color="red">*</font>提现密码：</td>
			<td>
				<input id="cashPassword" class="form-control" type="password">
			</td>
		</tr>
		<tr class="last-row">
			<td>&nbsp;</td>
			<td colspan="3">
				<button class="btn btn-blue" onclick="as()">下一步</button>
			</td>
		</tr>
	</tbody>									
</script>
</body></html>