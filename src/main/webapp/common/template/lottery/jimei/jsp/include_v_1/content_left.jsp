<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="side_left" id="side">
<div class="user_info">
<div class="title">账户信息</div>

<div class="zhanghu">
<div class="info"><label>账号：</label><span class="accountName"></span></div>
<div class="info"><label>额度：</label><span class="balance nowMoney" money="0"></span><i onclick="refreshMoney();">&nbsp;</i></div>
<c:if test="${not empty isScore && isScore eq 'on'}">
	<div class="info"><label>积分：</label><span class="balance" money="0">${userInfo.score}</span></div>
</c:if>
<div class="info"><label>今日输赢：</label><span class="betting dayRWMOney">${dayRWMoney}</span><i onclick="refreshDayMoney();">&nbsp;</i></div>
</div>
</div>
<div class="betdone" id="drawOfficial">
<!-- <script>
$(function(){
	var te = '${domainFolder}';
	var tt = document.getElementById('kaijiangwang');
	if(te != 'a06603'){
		tt.style.display='none';
	};
});
</script>
<div class="title" id="kaijiangwang"><a href="https://1680380.com/">全国开奖网</a></div> -->
<div class="title"><a href="javascript:void(0)" id="memberCore">个人中心</a></div>
<div class="title"><a href="javascript:void(0)" id="onlineRecharge">会员存款</a></div>
<div class="title"><a href="javascript:void(0)" id="onlineDraw">在线提款</a></div>
<div class="title"><a href="javascript:void(0)" id="conversion">额度转换</a></div>
<c:if test="${not empty isChangeMoney && isChangeMoney eq 'on'}">
<div class="title"><a href="javascript:void(0)" id="historyrecord">帐变记录</a></div>
</c:if>
<c:if test="${not empty isExChgOnOff && isExChgOnOff eq 'on'}"><div class="title"><a href="javascript:void(0)" id="scoreChangeMenu">积分兑换</a></div></c:if>
<c:if test="${not empty isZJGLOnOff && isZJGLOnOff}">
<div class="title"><a href="javascript:void(0)" id="memberMoneyChange">资金管理</a></div>
</c:if>
</div><div class="betdone" id="lastBets">
<a href="${base }/down.do" target="_blank"><div class="title"><span>APP下载</span></div></a>
<%--<div class="title"><span>最新注单</span></div>--%>
<div class="list-bet newOrderData"></div>
</div>
<div style="display:none" id="betResultPanel">

<div class="control s0">
<a onclick="resetPanel()" href="javascript:void(0)">返 回</a>
</div>
<div id="betResultDrawNumber" class="title"></div>
<div class="bresults">
<ul class="bets" id="betReulstList"></ul>
<table class="total s0">
<tbody><tr><td class="label">下注金额</td><td id="betResultCount"></td></tr>
<tr><td class="label">合计金额</td><td id="betResultTotal"></td></tr>
</tbody></table>
</div>
</div>

</div>
<script type="text/javascript">
		// a02201专用
		var search = window.location.search;
		var router = search.split("?router=").join("");
		if (router) {
			setTimeout(function(){
				$('#'+router).trigger("click");
			},200)
		}
</script>
