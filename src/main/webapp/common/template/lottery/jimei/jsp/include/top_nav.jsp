<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/lottery/jimei/css/balls.css?v=${caipiao_version}" />
<style type="text/css">
#ticket-base-info .tb-sub {
	width: 75px;
	margin: 0;
}

#ticket-base-info img {
	width: 75px;
	height: 75px;
	position: absolute;
	top: 0px;
	left: 7px;
}
</style>
<div class="tb-body"
	<c:if test="${caipiao != 'off'}">style="padding-top:3px;"</c:if>>
	<div id="ticket-base-info" class="tb-b-column">
		<div class="tb-sub">
			<img id="lottery_logoImg" src="">
		</div>
	</div>
	<div class="tb-b-column" id="ticket-cutdown">
		<div class="tb-sub">
			<div class="sub tc-refer title">
				<span>第<em id="curIssueNumberId"></em>期
				</span> <span id="openOrCloseName"></span>
			</div>
			<div id="cutdown-clock-base" class="sub">
				<ul class="ocplate">
				</ul>
				<ul class="ocplates">
				</ul>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div id="ticker-drawing-last" class="tb-b-column">
		<div class="tb-sub menu1">
			<div class="sub title">
				<em>第<i id="lotteryV2_Title">577167</i>期
				</em> <a onclick="result();" javascript="void(0)"
					style="cursor: pointer;">开奖号码</a>
			</div>
			<div
				class="sub viewer ticker-drawing-viewer ticker-drawing-viewer-base">
				<a id="lotteryV2_haoMa"> </a>
			</div>
			<div class="sub option">
				<a class="tdl-o-chart" href="javascript:void(0);"
					onclick="trendChart();">号码走势<i>&nbsp;</i></a> <a
					class="tdl-o-remark" javascript="void(0)" onclick="rule();"
					style="cursor: pointer;">游戏说明<i>&nbsp;</i></a>
			</div>
		</div>
	</div>
</div>
