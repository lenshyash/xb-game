<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/hisRecord.css?v=111" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>     
	<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
	<script src="${base}/common/js/contants.js?v=2"></script> 
</head>
<body>
			<div class="content">
				<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
               	<div class="chart_table_search_wrapper">
					<ul class="historyNav" style="height:40px;">
						<li>
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" name="startDate" id="startTime" value="${now }" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
				        </li>
				        <li>
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" name="endDate" id="endTime" value="${now }" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
				        </li>
				        <li>
				        	<div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after tp-ui-padding"><input class="tpui-text kc-calendar" name="orderId" id="orderId" value="" type="text" placeholder="订单号"></div></div>
				        </li>
				        <li>
				        		<button type="button" class="btn btn-red" id="payQuery">搜索</button>
				        </li>
		        		<div class="clearfix"></div>
		    		</ul>
		    	</div>
				</div>
				<div class="records">
					<div  class="yhjy-tzjl">
						<div class="ml20">
						</div>
						<div id="record_tb" class="yhjy-edzh" style="height:500px;overflow:auto;text-align: center;"></div>
			          </div>
        		</div>
	<jsp:include page="/common/include/pageToJieBao.jsp"></jsp:include>
<!--         		<div id="pageGro" class="cb" style="display:none;"> -->
<!--         			<input type="hidden" value="" id="pageNumber"> -->
<!-- 					<div class="allTotal">共<font id="totalCount"></font>条&nbsp;&nbsp;&nbsp;当前第<font id="currentPageNo"></font>页</div> -->
<!-- 					<div class="pageLoad"></div> -->
<!-- 				</div> -->
		  </div>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<%-- <link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet"> --%>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/laypage.js"></script>

      <script id="record_tpl" type="text/html">
		<table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
				<thead>
					<tr class="bg_white2">
						{{each colmuns as col}}
							<td class="myWidth">{{col}}</td>
						{{/each}}
					</tr>
				</thead>
				<tbody id="content">
					{{each list as row index}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key index}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
        <script>
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "变动前金额", "变动金额", "变动后余额", "订单号","备注" ],
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "beforeMoney", "money", "afterMoney", "orderId","remark" ],
	}
	var navUrls = {
		1 : "${base}/center/record/mnyrecord/list.do",
	}
	var curNavFlag = 1;
	var curDates = [];
	$(function() {
    	$("#startTime").datepicker();
		$("#endTime").datepicker();
    	$('#payQuery').click(function(){
    		search();
    	});
		initRdsData(curNavFlag);
		first = false;
	})

	function search() {
		initRdsData(curNavFlag);
	}

	template.helper('$formatter', function(content, key,index) {
		if(!key || !content){
			return "";
		}
		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		}else if (key.toUpperCase().indexOf("MONEY") != -1) {
			if(content > 0){
				return '<font color="orange">'+content+'</font>';
			}else{
				return '<font color="red">'+content+'</font>';
			}
		} 
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		var a = $(".category").find("a");
		a.removeClass("current");
		if(flag==1){
			a.eq(0).addClass("current");
		}else if(flag==2){
			a.eq(1).addClass("current");
		}else if(flag==3){
			a.eq(2).addClass("current");
		}
		var orderId = $("#orderId").val();
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		var cur_ps = $("#jump_page_size").val();
		var cur_pn = $("#jump_page").val();
		if(cur_pn && cur_ps){
			pageNumber = cur_pn;
			pageSize = cur_ps;
		}
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}
		$.ajax({
			url : navUrls[flag],
			data : {
				begin : $("#startTime").val(),
				end : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				orderId :orderId
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				curDates = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					search();
				});
			}
		});
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}
        </script>
</body>
</html>