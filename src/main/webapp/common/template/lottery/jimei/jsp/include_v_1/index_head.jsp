<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${onoffMobileChat eq 'on'}">
	<jsp:include page="/common/template/member/chatIframe.jsp"></jsp:include>
</c:if>
<div id="header" class="header">
	<div class="logo">
		<a href="${base}" target="_blank">
		<img src="${base}${caipiaoLogo}" title="${website_name}" width="170"
			height="50"></a>
	</div>
	<input type="hidden" value="${domainFolder}" id="domainFolder" />
	<input type="hidden" id="dxdsSetting" value="${dxdsSetting }">
	<input type="hidden" id="k3baoziDrawShow" value="${k3baoziDrawShow}">
	<div class="top">
		<div class="menu" style="min-width:1340px;">
			<div class="menu1">
				<div id="lotteryV2_Title" class="draw_number"></div>
				<a target="_blank" id="lotteryV2_haoMa"> </a>
			</div>
			<div class="menu2" style="margin-right:100px;">
				<span><a id="notHistoryData" href="javascript:void(0);">未结明细</a></span>
				| <span><a href="javascript:void(0);" id="okHistoryData">今天已结</a></span>
				|
				<!-- <span><a href="history" target="frame">报表查询</a></span> | -->
				<span><a href="javascript:void(0);" id="historyResult">开奖结果</a></span>
				<br />
				<!--<span><a target="frame" href="info?lottery={lottery}">个人资讯</a></span> |-->
				<span><a id="updatePWord" href="javascript:void(0);">修改密码</a></span>
				| <span><a id="V2Rule" href="javascript:void(0);">游戏规则</a></span> |
				<span id="skinPanel"><a href="javascript:void(0)">更换皮肤</a>
					<ul style="display: none;">
						<li><i style="background: #dc2f39"></i><a back="red">红色</a></li>
						<li><i style="background: #5382bc"></i><a back="blue">蓝色</a></li>
						<li><i style="background: #cdcdcd"></i><a back="gray">灰色</a></li>
					</ul> </span>
			</div>
			<div id="cat_nav" style="border-left:#b3b3b3 1px solid;overflow:hidden;">
		       <ul class="ulEach">
		          <c:if test="${empty duLiCaiPiaoOpened || !duLiCaiPiaoOpened }">
		          <li><a href="${base }/lottery/index.do?lotCode=${lotCode}" class="current" pli="lotty">彩 票<span class="line"></span></a></li>
		            <c:if test="${zhenRenYuLeOpened }"><li><a href="${base }/index/real.do" target="_blank" pli="live" >真人视讯<span class="line"></span></a></li></c:if>
		            <c:if test="${dianZiYouYiOpened }"><li><a href="${base }/index/egame.do" target="_blank" pli="slot">电子游艺<span class="line"></span></a></li></c:if>
		            <c:if test="${isTyOnOff=='on' && isGuest != true}"><li><a href="${base }/index/sport.do" target="_blank" pli="slot">体育赛事<span class="line"></span></a></li></c:if>
		            <c:if test="${isKyOnOff=='on' && isGuest != true}"><li><a href="${base }/index/chess.do" target="_blank" pli="slot">棋牌游戏<span class="line"></span></a></li></c:if>
		             <c:if test="${isAgOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardAg.do', '1');" target="_blank" pli="slot">女优厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isBbinOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"   onclick="go('${base}/forwardBbin.do?type=live', '2');" target="_blank" pli="slot">台湾厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isMgOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardMg.do?gameType=1', '3');" target="_blank" pli="slot">欧美厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isDsOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardDs.do?gameType=1', '8');" target="_blank" pli="slot">太阳厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isAbOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardAb.do', '5');" target="_blank" pli="slot">亚洲厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isVrOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardVr.do', '97');" target="_blank" pli="slot">VR厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isOgOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardOg.do?gameType=desktop', '7');" target="_blank" pli="slot">东方厅<span class="line"></span></a></li></c:if>
		            <c:if test="${isBgOnOff=='on' && isGuest != true}"><li><a href="javascript:void(0)"  onclick="go('${base}/forwardBg.do?type=2', '98');" target="_blank" pli="slot">大游厅<span class="line"></span></a></li></c:if>
		          </c:if>
		       </ul>
       		 </div>
       		 <script>
	       		 var ul = $('.ulEach').find('li');
	       		 if(ul.length > 6){
	       			$('#cat_nav').mouseover(function(){
	          			 $(this).css({'height':'125px','background':'black','z-index':'999999'});
	          		 });
	          		$('#cat_nav').mouseout(function(){
	         			 $(this).css({'height':'64px','background':'none'});
	         		 });
	       		 }
       		 
       		 	var liLength = $('#cat_nav').find('li').length;
       		 	if(liLength <= 2){
       		 		$("#cat_nav").width('80px')
       		 	}else if(liLength > 2 && liLength <= 4 ){
       		 		$("#cat_nav").width('140px')
       		 	}else if(liLength > 4  ){
       		 		$("#cat_nav").width('200px')
       		 	}
       		 </script>
       		 <style>
       		 	#cat_nav {
	width: 198px;
	height: 64px;
	border-right: #b3b3b3 1px solid;
	position: absolute;
	left:965px;
}

#cat_nav ul {
	position: absolute;
	left: 11px;
	z-index: 1
}

#cat_nav ul li {
	height: 20px;
	line-height: 21px;
	font-size: 12px;
	color: #fff;
	text-align: center;
	margin: 8px auto 2px;
	position: relative;
	float:left;
}

#cat_nav ul li a {
	display: inline-block;
	width: 60px;
	height: 21px;
	line-height: 21px;
	font-size: 12px;
	color: #fff;
	text-align: center;
	padding: 1px 5px;
	text-decoration: none;
	white-space:nowrap;
}

#cat_nav ul li a:hover, #cat_nav ul li a.current {
	padding: 0 4px;
	border: 1px #fff solid;
	background-color: black;
	background-color: rgba(0, 0, 0, 0.9);
	-pie-background: rgba(0, 0, 0, 0.9);
}

#cat_nav ul li a.current .line {
	visibility: visible;
}

#cat_nav .line {
	position: absolute;
	left: -11px;
	top: 10px;
	width: 80px;
	height: 1px;
	display: inline-block;
	border-bottom: #b3b3b3 1px solid;
	z-index: -1;
	visibility: hidden
}
       		 </style>
			<div class="menu4">
				<a class="support" style="margin-left:120px;"></a>
			</div>
			<div class="menu3">
				<!-- 
  	<a href="${base}/logout.do" class="logout"></a>
  	 -->
				<a href="javascript:void(0);" onclick="dandu();" class="logout"></a>
			</div>
			<div style="clear: both;"></div>
		</div>
		<script type="text/javascript">
			//a011特殊使用
			function dandu() {
				var dd = $('#showdandu').val();
				/* parent.location.href = '${base}/logout.do'; */
				parent.location.href = '${base}/index.do';
				if (dd) {
					parent.location.href = parent.location.href;
				}
			}
			//在线客服
			function kf() {
				$.ajax({
					url : "${base}/memInfo/common/siteInfo/base.do",
					dataType : "json",
					success : function(j) {
						if (j.success) {
							var url = j.item;
							if (url.indexOf('http://') == -1
									&& url.indexOf('https://') == -1) {
								url = 'http://' + url;
							}
							$('.support').attr('target', '_blank').attr('href',
									url);
						} else {
							$('.support').attr('href', '#')
									.removeAttr('target');
						}
					}
				});
			}
		</script>
		<div class="lotterys" id="lotteryV2_navs" style="min-width:1300px;">
			<c:if test="${lotteryV2List.size()<=11 }">
				<c:set value="10" var="num"></c:set>
			</c:if>
			<c:if test="${lotteryV2List.size()>11}">
				<c:set value="9" var="num"></c:set>
			</c:if>
			<c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
				<c:if test="${lotIndex.index <= num}">
					<c:choose>
						<c:when test="${lot.code == lotCode}">
							<a class="ng-lotttery ng-binding selected" href="javascript:void(0);"
								lot_code="${lot.code}" lot_type="${lot.type }"><span>${lot.name }</span>
							</a>
						</c:when>
						<c:when test="${lot.code == 'LHC'}">
							<c:if test="${mark_on_off == 'on'}">
								<a class="ng-lotttery ng-binding" href="javascript:void(0);"
									lot_code="${lot.code}" lot_type="${lot.type }"><span>${lot.name}</span>
								</a>
							</c:if>
						</c:when>
						<c:otherwise>
							<a class="ng-lotttery ng-binding" href="javascript:void(0);"
								lot_code="${lot.code}" lot_type="${lot.type }"><span>${lot.name}</span>
							</a>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
				<c:if test="${lotteryV2List.size()>11}">
				<c:set var="activeLot" value=""></c:set>
				<div class="dsLotto-nav-setting">
					<div class="ds-ui-popup dsLotto-category-more">
						<div class="ui-popup__body dsLotto-category-more-popup__body"
							ng-class="">
							<ul class="array block cf">
								<c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
								<c:if test="${lotIndex.index > num}">
									<c:choose>
										<c:when test="${lot.code == lotCode}">
											<c:set var="activeLot" value="${lot.name}"></c:set>
											<li class="ng-scope">
												<a href="javascript:void(0);" lot_code="${lot.code}" lot_type="${lot.type }" class="ng-lotttery selected">${lot.name}</a>
											</li>
										</c:when>
										<c:when test="${lot.code == 'LHC'}">
										<c:if test="${mark_on_off == 'on'}">
											<li class="ng-scope">
												<a href="javascript:void(0);" lot_code="${lot.code}" lot_type="${lot.type }" class="ng-lotttery">${lot.name}</a>
											</li>
										</c:if>
										</c:when>
										<c:otherwise>
											<li class="ng-scope">
												<a href="javascript:void(0);" lot_code="${lot.code}" lot_type="${lot.type }" class="ng-lotttery">${lot.name}</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:if>
								</c:forEach>
							</ul>
						</div>
						<a href="javascript:void(0);"
							class="moreLottery ${empty activeLot?'':'selected'}" > <i class="ui-icon-group__icon"></i>
							<span class="ui-icon-group__txt"><c:if test="${empty activeLot}">更多彩种</c:if><c:if test="${not empty activeLot}">${activeLot}</c:if></span>
						</a>
					</div>
				</div>
			</c:if>
		</div>
		<div class="sub" id="lotteryV2_v010">
			<ul class="groupList ng-scope">
			</ul>
		</div>
	</div>
</div>
<c:if test="${domainFolder eq 'a010101'}">
	<script type="text/javascript">
		$("body").removeAttr("class").addClass("skin_red");
	</script>
</c:if>
<jsp:include page="/common/template/third/page/live_demo.jsp" />