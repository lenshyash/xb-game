<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<link href="${base}/common/template/lottery/jimei/css/test.css?v=2016122913288933" rel="stylesheet">
<section class="main-result" id="_main_view">
		<div class="title">
			<strong>最新开奖结果</strong>
			<!-- 
			<span><a href="" target="_blank" jcid="4311">如何购彩？</a>|<a href="" target="_blank" jcid="4312">如何领奖？</a></span>
			 -->
		</div>
		<div class="todayAward">
			<div class="detail"><strong>彩种分类：</strong>
				<div style="float:right;width:89%;">
				<c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
					<c:choose>
						<c:when test="${lot.code == lotCode}">
							<c:set var="lotName" value="${lot.name}"></c:set>
							<a javascript="void(0);" href="${base}/lotteryV2/resultList.do?lotCode=${lot.code}" class="on">${lot.name} </a>
						</c:when>
						<c:when test="${lot.code=='LHC'}">
							<c:if test="${mark_on_off == 'on'}">
								<a javascript="void(0);" href="${base}/lotteryV2/resultList.do?lotCode=${lot.code}">${lot.name} </a>
							</c:if>
						</c:when>
						<c:otherwise>
								<a javascript="void(0);" href="${base}/lotteryV2/resultList.do?lotCode=${lot.code}">${lot.name} </a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
			</div>
		</div>
		<div class="title">
			<strong>${lotName}</strong>
			<c:if test="${caipiao == 'on'}">
				<a class="betBtn" href="${base}/lotteryV2/index.do?lotCode=${lotCode}">立即投注</a>
			</c:if>
			<c:if test="${caipiao == 'off' }">
			<a class="betBtn" href="${base}/index/games.do?lotCode=${lotCode}">立即投注</a>
			</c:if>
			<div class="search_right">
					<input name="button" value="刷新" onclick="window.location.reload()" class="refresh_button" type="button">
			</div>
		</div>
		<div class="chart_table_search_wrapper">
		<div class="drawing-head">
			<div class="db-config">
                        <ul>
                        <c:choose>
                        	<c:when test="${lotCode eq 'LHC' || lotCode eq 'FC3D' || lotCode eq 'PL3'}">
	                        	 <li class="AwardDiff" daydiff="0"><a href="javascript:void(null)">本月</a></li>
	                             <li class="AwardDiff" daydiff="-1"><a href="javascript:void(null)">上月</a></li>
	                             <li class="AwardDiff" daydiff="-2"><a href="javascript:void(null)">前月</a></li>
	                        </c:when>
	                        <c:otherwise>
	                        	 <li class="AwardDiff" daydiff="0" ><a href="javascript:void(null)">今天</a></li>
	                             <li class="AwardDiff" daydiff="-1" ><a href="javascript:void(null)">昨天</a></li>
	                             <li class="AwardDiff" daydiff="-2" ><a href="javascript:void(null)">前天</a></li>
	                        </c:otherwise>
                        </c:choose>
                            <li>
                                <label>按日期${lotCode.indexOf('K3')}${lotCode }：</label>
                                <div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after"><input class="tpui-text kc-calendar  laydate-icon" data-icon-after="kc-calendar" id="startDate" value="${date}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
                            </li>
                        </ul>
                    </div>
		</div>
	</div>
		<div class="rule-content">
		<c:set var="pagIng" value=""></c:set>
		<c:choose>
			<c:when test="${lotCode eq 'BJSC' || lotCode eq 'XYFT'  || lotCode eq 'LXYFT'  || lotCode eq 'AZXY10' || lotCode eq 'SFSC' || lotCode eq 'FFSC' || lotCode eq 'WFSC' || lotCode eq 'WFFT' || lotCode eq 'LBJSC' || lotCode eq 'SFFT'|| lotCode eq 'XSFSC'|| lotCode eq 'XWFSC'}">
				<jsp:include page="../result/bjsc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'FC3D' || lotCode eq 'PL3'}">
				<jsp:include page="../result/pl3.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'PCEGG' || lotCode eq 'JND28'}">
				<jsp:include page="../result/pcegg.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'LHC' || lotCode eq 'SFLHC' || lotCode eq 'TMLHC' || lotCode eq 'WFLHC' || lotCode eq 'HKMHLHC'|| lotCode eq 'AMLHC'}">
				<jsp:include page="../result/lhc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'GDKLSF' || lotCode eq 'HNKLSF' || lotCode eq 'CQXYNC'}">
				<jsp:include page="../result/sfc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'GD11X5' || lotCode eq 'JX11X5' || lotCode eq 'SH11X5' || lotCode eq 'GX11X5' || lotCode eq 'SD11X5'}">
				<jsp:include page="../result/syx5.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'JSSB3' || lotCode.indexOf('K3')>0}">
				<jsp:include page="../result/k3.jsp"></jsp:include>
			</c:when>
			<c:otherwise>
				<jsp:include page="../result/ssc.jsp"></jsp:include>
			</c:otherwise>
		</c:choose>
		<c:if test="${empty page}">
			<table class="awardList"><tbody><tr><td><div style="padding:10px;font-size:14px;font-weight: bold;">暂无开奖数据</div></td></tr></tbody></table>
		</c:if>
	</div>
	</section>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<script type="text/javascript">

$(function(){
	var c = "${lotCode}";
	var d = "${date}";
	if(c=="LHC" || c=="FC3D" || c=="PL3"){
		$('#startDate').cxCalendar({format:'YYYY-MM'});
		if(d != null){
			d = d.split("-")[1];
			var diff = parseInt(d) - parseInt(getMonth());
			$(".drawing-head .db-config li").each(function(index,item){
				if(parseInt(diff) == parseInt($(item).attr("daydiff"))){
					$(this).addClass("active");
				}
			})
		}
	}else{
		$('#startDate').cxCalendar({format:'YYYY-MM-DD'});
		if(d != null){
			d = d.split("-")[2];
			var diff = parseInt(d)-parseInt(getDate());
			$(".drawing-head .db-config li").each(function(index,item){
				if(parseInt(diff) == parseInt($(item).attr("daydiff"))){
					$(this).addClass("active");
				}
			})
		}
	}
	$(".drawing-head").on("change","#startDate",function(){
		var val = $(this).val();
		window.location.href='${base}/lotteryV2/resultList.do?lotCode=${lotCode}&startDate='+val;
	}).on("click",".db-config li.AwardDiff",function(){
		var diff = $(this).attr("daydiff"),date;
		if(c == "LHC" || c == "FC3D" || c=="PL3"){
			date = getLHCDataStr(diff);
			console.info(date);
		}else{
			date = getDataStr(diff);
		}
		window.location.href='${base}/lotteryV2/resultList.do?lotCode=${lotCode}&startDate='+date;
	})
	$(".tp-ui-icon").click(function(){
			$(this).parent().find("input").focus();
		})
	
})

function getDataStr(diff){
	var date = new Date();
	date.setDate(date.getDate()+parseInt(diff));
	return $.formatDate("yyyy-MM-dd",date,0);
}

function getLHCDataStr(diff){
	var date = new Date(),year,month;
	year = date.getFullYear();
	month = (date.getMonth()+1)+parseInt(diff);
	if(month<10){month = "0"+month;}
	return year+"-"+month;
}

function getDate(){
	var date = new Date();
	return date.getDate();
}
function getMonth(){
	var date = new Date();
	return date.getMonth()+1;
}
</script>