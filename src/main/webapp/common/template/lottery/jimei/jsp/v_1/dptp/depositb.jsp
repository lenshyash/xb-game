<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/onlinedeposit.css?v=4333120234" rel="stylesheet">
	<link rel="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script> 
	<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>      
</head>
<body>
			<div class="content">
				<jsp:include page="../../include_v_1/base_head_info.jsp"></jsp:include>
				<div class="membersubnavi">
             		<div class="subnavi">
                   	<span class="blue">充值</span>
                   	<div class="subnaviarrow"></div>
                   </div>
                   <div class="subnavi">
                   	<a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
                   </div> 
                   <div class="subnavi">
                   	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                   </div>
               	</div>
            <div class="steps" style="height:40px;">
			<div class="substeps">
				<div class="substepitmgray1">
					<b>01</b> 选择支付模式
				</div>
				<div class="substepitmgray2 substepitmblue2">
					<b>02</b> 填写付款人资料
				</div>
				<div class="substepitmgray2" id="sliderBuzhou">
					<b>03</b> 确认您的转账资料
				</div>
				<div class="substepitmgray2">
					<b>04</b> 完成
				</div>
			</div>
			</div>
			<style>
				.form-control{width:100%;}
				.account-content table td { padding: 5px 0 5px 10px;}
				.table tbody td{border-bottom:1px solid #ffffff;}
				.table .last-row td{border:none;}
				.btn{padding:4px 25px;margin-right:40px;}
			</style>
			<div class="account-content" id="depositTabs">
			</div>
	</div>
<script id="inputdata_tpl" type="text/html">
{{if fasts && fasts.length > 0}}
<form id="onlinePayAFormId">
<div class="subrow">
	<div class="subcol2">
		{{each fasts as fast count}}
			{{if count > 0 && count %4 ==0}}
				
			{{/if}}
			<div class="banklist marginbtm10" id="{{fast.iconCss}}" flabel="{{fast.frontLabel}}" remarks="{{fast.qrcodeDesc}}">
				<input name="payId" min="{{fast.min}}" max="{{fast.max}}" iconCss="{{fast.iconCss}}" value="{{fast.id}}" class="bankradiobtn" type="radio">
				{{if fast.icon}}
					<div class="zhifuImg" title="{{fast.payName}}"><img src="${base}{{if fast.icon}}{{fast.icon}}{{/if}}" title="{{fast.payName}}" height="47" width="123"></div>
				{{else}}
					<span title="{{fast.payName}}" class="bankicon {{fast.iconCss}}" style="width:123px;display:table-caption">{{fast.payName}}</span>
				{{/if}}
			</div>
		{{/each}}
	</div>
</div>
<div class="security-setting hidn" id="dpt_info">
<table class="table">
<tbody>
	<tr style="line-height:30px" class="virtual_fast_info">
		<td class="align-right-col">会员帐号：</td>
		<td>${loginMember.account}</td>
		<td></td>
		<td rowspan="9" width="50%">
		<div style="line-height: 28px;padding: 10px;">
		<b>自动充值用户须知:</b>
		<br> 
		1. 客服上班时间为：<span style="color: blue;">${kfsb}至${kfxb}</span>;
		<br>
		2. 选择充值方式，填写充值个数，扫描二维码充值后，<span style="color: blue;">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即联系客服
		</div>
		</td>
	</tr>
	<tr style="line-height:30px" class="fast_virtual_info">
		<td class="align-right-col">会员帐号：</td>
		<td>${loginMember.account}</td>
		<td></td>
		<td rowspan="9" width="50%">
			<div style="line-height: 28px;padding: 10px;">
				<b>自动充值用户须知:</b>
				<br>
				1. 客服上班时间为：<span style="color: blue;">${kfsb}至${kfxb}</span>;
				<br>
				2. 选择充值方式，填写充值金额及其账户信息，扫描二维码充值后，<span style="color: blue;">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即联系客服
			</div>
		</td>
	</tr>
<c:if test="${showPayInfo}"><tr>
	<td style="text-align: right;">充值方式：</td>
	<td id="pay_type_name"></td>
	<td></td>
</tr>
<tr>
	<td style="text-align: right;">收款姓名：</td>
	<td id="Bname_info"></td>
	<td></td>
</tr>
<tr>
	<td style="text-align: right;">收款帐号：</td>
	<td id="Baccount_info"></td>
	<td></td>
</tr>
<tr>
	<td style="text-align: right;">二维码：</td>
	<td colspan="2"><img src="#" id="qrcode_img_id"style="width: 300px; height: 300px;">
	<div style="font-size:20px;color:red;margin-top:6px">扫码支付后，请填写充值金额，并点击确定充值，否则无法为您上分！</div></td>
</tr></c:if>
	<tr>
		<td class="align-right-col">充值金额：</td>
		<td style="width:10%;">
			<input class="form-control" id="amount" type="text">
		</td>
		<td id="dan_bi_limit" colspan="2"></td>
	</tr>
	<tr style="line-height:30px">
		<td class="align-right-col">充值金额(大写)：</td>
		<td colspan="2"><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
		<td colspan="2"><span>约等于：</span><span class="approximateRmb">0RMB</span></td>
	</tr>
	<tr class="referenceRateTr">
		<td style="text-align:right;">参考汇率：</td>
		<td class="referenceRate">1ust≈2RMB</td>
		<td colspan="2" id="virtual_qrcodeDesc"></td>
	</tr>
	<tr id="payType_trId" >
		<td class="align-right-col"><span class="payType">微信或者支付宝帐号</span>：</td>
		<td style="width:10%;">
			<input class="form-control" id="bank_cards" type="text">
		</td>
		<td><span class="smalltxt red">请填写正确的<span class="payType">支付宝或者微信帐号</span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>
	</tr>
	<tr class="last-row">
		<td>&nbsp;</td>
		<td colspan="3">
			<button id="nextBtn" class="btn btn-blue"><c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if></button>
		</td>
	</tr>
</tbody></table>
<div class="wxts-div">
	<span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
</div>
</div>
</form>
{{else}}
	<div style="margin-left:225px;font-weight: bold;"><span class="red">*</span>友情提示：暂时没有配置充值入口，请更换其他方式继续充值</div>
{{/if}}
</script>
<script type="text/javascript">
var pays = [],bank_cards_check=true,curPay={};
$(function(){
	var convertCurrency=function(a) {
        var e, g, c, h, l, m, q, t, r, y, w;
        a = a.toString();
        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
        a = a.replace(/,/g, "");
        a = a.replace(/^0+/, "");
        if (9.999999999999E10 < Number(a)) return layer.alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
        e = a.split(".");
        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
        h = ["", "\u62fe", "\u4f70", "\u4edf"];
        l = ["", "\u4e07", "\u4ebf"];
        m = ["", ""];
        g = "";
        if (0 < Number(a)) {
            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
            g += ""
        }
        if ("" != e)
            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
        "" == g && (g = "\u96f6");
        "" == e && (g += "");
        return "" + g
    };
	$.ajax({
		url : "${base}/center/banktrans/deposit/dptinitb.do",
		success : function(result) {
			var html = template('inputdata_tpl', result);
			$("#depositTabs").html(html);
			pays = result.fasts;
			bindIptData();
		}
	});
	function bindIptData(){
		var $from = $("#onlinePayAFormId");
	    $from.on( 'keyup',"#amount",  function(){
	        /* 取出输入的金额,并转换成中文 */
			$( ".approximateRmb" ).text(((parseFloat(Number($( '#amount' ).val()) * 1000) * parseFloat(Number(curPay.rate)* 1000)) / 1000000 )+'RMB');
			$( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
		$('.banklist').click(function(){
			var $it=$(this),flabel=$it.attr("flabel"),id=$it.find("input").val(),remarks=$it.attr("remarks");
			$it.find("input").prop('checked', true);
			if(!id)return false;
            if(remarks){
                $(".smalltxt").html(remarks)
            } else {
                $(".smalltxt").html('请填写正确的<span class="payType">支付宝或者微信帐号</span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）')
			}
			if(flabel){
				$("#payType_trId").show();
				$('.payType').html(flabel);
				bank_cards_check=true;
			}else{
				$("#payType_trId").hide();
				bank_cards_check=false;
			}
			$it.addClass("curSubrows").siblings().removeClass("curSubrows");
	        $("#dpt_info").removeClass("hidn");
			for (var i = 0; i < pays.length; i++) {
				if(pays[i].id == id){
					curPay = pays[i];
					break;
				}
			}
			$('#dan_bi_limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+curPay.min+'</span> 元 ，最高<span id="max_amount" class="red">'+curPay.max+'</span> 元）');

			<c:if test="${showPayInfo}">
			$("#pay_type_name").html(curPay.payName);
			$("#qrcode_img_id").attr("src",curPay.qrCodeImg);
			$('#Bname_info').html(curPay.payUserName);
			$('#Baccount_info').html(curPay.payAccount);
			$('#virtual_qrcodeDesc').text(curPay.qrcodeDesc)
			if (curPay.iconCss && curPay.iconCss =='virtual'){
				$('#pay_type_name').prev().text('钱包类型：')
				$('#pay_type_name').text(curPay.walletType)
				$('#Bname_info').parent().hide()
				$('#Baccount_info').prev().text('收款地址：')
				$('#qrcode_img_id').next().hide()
				$('.virtual_fast_info').show()
				$('.fast_virtual_info').hide()
				$('#dan_bi_limit').prev().prev().text(curPay.walletType + '数量：')
				$('#amount_CH').parent().prev().text('充值个数(大写)：')
				$('#amount_CH').next().text('个')
				$('#amount_CH').parent().attr('colspan','')
				$('.referenceRateTr').show()
				$('.approximateRmb').parent().show()
				$('.referenceRate').text(1 + curPay.walletType +'≈' + curPay.rate +'RMB')
				$('#payType_trId').hide()
				$('#dan_bi_limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+curPay.min+'</span> 个 ，最高<span id="max_amount" class="red">'+curPay.max+'</span> 个）');

			}else {
				$('#Bname_info').parent().show()
				$('#Baccount_info').prev().text('收款账号：')
				$('#qrcode_img_id').next().show()
				$('.fast_virtual_info').show()
				$('.virtual_fast_info').hide()
				$('#dan_bi_limit').prev().prev().text('充值金额：')
				$('#amount_CH').parent().prev().text('充值金额(大写)：')
				$('#amount_CH').next().text('元整')
				$('#amount_CH').parent().attr('colspan','2')
				$('.approximateRmb').parent().hide()
				$('.referenceRateTr').hide()
				$('#payType_trId').show()
			}
			</c:if>
		});
		
	    $('#nextBtn').click(function() {
	        var $it=$(this),eleForm = $("#onlinePayAFormId"),paySetting = eleForm.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=eleForm.find("#amount"),
	        amount1 =$input.val(),
	        bank_cards = eleForm.find("#bank_cards").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        if($it.attr("disabled")){
	           	return false;
           }
           $it.attr("disabled","disabled");
	        if(!payId){
	        	alertfocue("请选择支付方式",$it);return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	if (curPay.iconCss == 'virtual'){
					alertfocue("请输入正确个数",$input);
				}else {
					alertfocue("请输入正确金额",$input);
				}
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	if (curPay.iconCss == 'virtual'){
					alertfocue("充值个数必须不小于"+ min,$input);

				}else {
					alertfocue("充值金额必须不小于"+ min,$input);

				}
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	if (curPay.iconCss == 'virtual'){
					alertfocue("充值个数必须不大于"+ max,$input);
				}else {
					alertfocue("充值金额必须不大于"+ max,$input);
				}
	            return false;
	        }
	        if (bank_cards_check&&(!bank_cards ||bank_cards.length==0)) {
	        	if (curPay.iconCss != 'virtual'){
					alertfocue("请输入存款帐号",eleForm.find("#bank_cards"));
				}
	            return false;
	        }
	        $.ajax({
				url : "${base}/center/banktrans/deposit/dptcommitb.do",
				data : {
					money : amount1,
					bankCards : bank_cards,
					payId : payId
				},
				method:"POST",
				success : function(result) {
					if(!result.success && result.msg){
						layer.alert(result.msg,{icon:2});
						$('#nextBtn').removeAttr("disabled");
						return;
					}
					var curPay = {};
					for (var i = 0; i < pays.length; i++) {
						if(pays[i].id == payId){
							curPay = pays[i];
							break;
						}
					}
					curPay.money = amount1;
					curPay.orderNo = result;
					$("#depositTabs").html(template('confirm_tpl',curPay));
					$("#sliderBuzhou").addClass("substepitmblue2").siblings().removeClass("substepitmblue2");
					bindCopy(curPay);
				}
			});
	        return false;
		});
	}
	function alertfocue(msg, ele) {
		alert(msg);
		ele.focus();
		$('#nextBtn').removeAttr("disabled");
	}
});
function Go(url){
	window.location.href=url;
}
function bindCopy(curPay) {
	$("#Bname").zclip({
		path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
		copy : curPay.payUserName,
		afterCopy : function() {
			$("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		}
	});
	$("#Baccount").zclip({
		path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
		copy : curPay.payAccount,
		afterCopy : function() {
			$("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		}
	});
	$("#Bamount").zclip({
		path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
		copy : curPay.money+"",
		afterCopy : function() {
			$("<span id='msg'/>").insertAfter($('#Bamount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		}
	});
	$("#Boid").zclip({
		path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
		copy : curPay.orderNo+"",
		afterCopy : function() {
			$("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
		}
	});
}
</script>
<script id="confirm_tpl" type="text/html">
<div class="security-setting" id="dpt_info">
<table class="table">
	<tbody>
		<tr style="line-height:30px">
			<td class="align-right-col">充值方式：</td>
			<td>{{payName}}</td>
			<td></td>
		</tr>
		<tr>
			<td class="align-right-col">收款姓名：</td>
			<td width="500">{{payUserName}}</td>
			<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="btn btn-red">复&nbsp;制</button>
				<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
				</div>
			</td>
		</tr>
		<tr>
			<td class="align-right-col">收款帐号：</td>
			<td width="500">{{payAccount}}</td>
			<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
			</div>
			</td>
		</tr>
		<tr>
			<td class="align-right-col">订单号：</td>
			<td width="500"><span class="blue">{{orderNo}}</span></td>
			<td style="text-align: left;position: relative;"><button id="Boid" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
			</div>
			</td>
		</tr>
		<tr>
			<td class="align-right-col">充值金额：</td>
			<td width="500"><span class="red">{{money}}</span></td>
			<td style="text-align: left;position: relative;"><button id="Bamount" type="button" class="btn btn-red">复&nbsp;制</button>
			<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
		</div></td>
		</tr>
		<tr class="last-row">
			<td class="align-right-col">二维码：</td>
			<td><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 300px; height: 300px;"></td>
			<td></td>
		</tr>
	</tbody></table>
</div>
<div class="wxts-div">
	<span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
</div>
<div class="wxts-div">
	<button type="button" class="btn btn-red marginBtn" onclick="javascript:Go('${base}/center/record/hisrecord/cPage.do');">确定</button>
</div>
</script>
</body></html>