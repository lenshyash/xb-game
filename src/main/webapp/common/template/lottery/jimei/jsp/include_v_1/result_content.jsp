<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<link href="${base}/common/template/lottery/jimei/js/assets/css/amazeui.min.css" rel="stylesheet">
<link rel="stylesheet" href="${base}/common/bootstrap/3.3.6/css/bootstrap.min.css"  type="text/css">
<link href="${base}/common/template/lottery/jimei/css/test.css?v=20161229104889" rel="stylesheet">
<section class="main-result" id="_main_view">
		<div class="chart_table_search_wrapper">
		<div class="drawing-head">
			<div class="db-config" style="left:0">
                        <ul>
                        	<li>
					            <select id="lotteryResult" name="lotCode" data-am-selected="{btnWidth: '150px', btnSize: 'lg',maxHeight: 300}" placeholder="全部彩种">
					            	<option value=""></option>
						            <c:forEach items="${lotteryV2List}" var="lot" varStatus="lotIndex">
						            	<c:choose>
						            		<c:when test="${not empty param.lotCode && param.lotCode == lot.code}">
						            			<option value="${lot.code}" selected="selected">${lot.name}</option>
						            		</c:when>
						            		<c:when test="${lot.code == 'LHC'}">
						            			<c:if test="${mark_on_off == 'on'}">
						            				<option value="${lot.code}">${lot.name}</option>
						            			</c:if>
						            		</c:when>
						            		<c:otherwise>
						            			<option value="${lot.code}">${lot.name}</option>
						            		</c:otherwise>
						            	</c:choose>
						            </c:forEach>
					            </select>
					        </li>
                        <c:choose>
                        	<c:when test="${lotCode eq 'LHC'||lotCode eq 'FC3D' || lotCode eq 'PL3'}">
	                        	 <li class="AwardDiff" daydiff="0"><a href="javascript:void(null)">本月</a></li>
	                             <li class="AwardDiff" daydiff="-1"><a href="javascript:void(null)">上月</a></li>
	                             <li class="AwardDiff" daydiff="-2"><a href="javascript:void(null)">前月</a></li>
	                        </c:when>
	                        <c:otherwise>
	                        	 <li class="AwardDiff" daydiff="0" ><a href="javascript:void(null)">今天</a></li>
	                             <li class="AwardDiff" daydiff="-1" ><a href="javascript:void(null)">昨天</a></li>
	                             <li class="AwardDiff" daydiff="-2" ><a href="javascript:void(null)">前天</a></li>
	                        </c:otherwise>
                        </c:choose>
                            <li>
                                <label>按日期：</label>
                                <div class="tp-ui-item tp-ui-forminput tp-ui-text tp-ui-text-base"><div class="tp-ui-sub tp-ui-text-base tp-ui-iconat-after"><input class="tpui-text kc-calendar  laydate-icon" data-icon-after="kc-calendar" id="startDate" value="${date}" type="text"></div><div class="tp-ui-icon tp-ui-icon-after icon-kc-calendar"><i>&nbsp;</i></div></div>
                            </li>
                        </ul>
                    </div>
		</div>
	</div>
		<div class="rule-content">
		<c:set var="pagIng" value=""></c:set>
		<c:choose>
			<c:when test="${lotCode eq 'BJSC' || lotCode eq 'XYFT' || lotCode eq 'LXYFT' || lotCode eq 'AZXY10' || lotCode eq 'SFSC' || lotCode eq 'FFSC' || lotCode eq 'WFSC' || lotCode eq 'WFFT' || lotCode eq 'LBJSC' || lotCode eq 'SFFT'}">
				<jsp:include page="../result/bjsc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'FC3D' || lotCode eq 'PL3'|| lotCode eq 'FF3D' || lotCode eq 'WF3D'}">
				<jsp:include page="../result/pl3.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'PCEGG' || lotCode eq 'JND28' || lotCode eq'SF28'}">
				<jsp:include page="../result/pcegg.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'LHC' || lotCode eq 'SFLHC' || lotCode eq 'TMLHC' || lotCode eq 'WFLHC' || lotCode eq 'HKMHLHC'|| lotCode eq 'AMLHC'}">
				<jsp:include page="../result/lhc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'GDKLSF' || lotCode eq 'HNKLSF' || lotCode eq 'CQXYNC' || lotCode eq 'SFKLSF' || lotCode eq 'WFKLSF'}">
				<jsp:include page="../result/sfc.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'GD11X5' || lotCode eq 'JX11X5' || lotCode eq 'SH11X5' || lotCode eq 'GX11X5' || lotCode eq 'SD11X5' || lotCode eq 'FF11X5'|| lotCode eq 'SF11X5'|| lotCode eq 'WF11X5'}">
				<jsp:include page="../result/syx5.jsp"></jsp:include>
			</c:when>
			<c:when test="${lotCode eq 'JSSB3' || lotCode.indexOf('K3')>0}">
				<jsp:include page="../result/k3.jsp"></jsp:include>
			</c:when>
			<c:otherwise>
				<jsp:include page="../result/ssc.jsp"></jsp:include>
			</c:otherwise>
		</c:choose>
		<c:if test="${empty page}">
			<table class="awardList"><tbody><tr><td><div style="padding:10px;font-size:14px;font-weight: bold;">暂无开奖数据</div></td></tr></tbody></table>
		</c:if>
	</div>
	</section>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/assets/js/amazeui.min.js"></script>
<script type="text/javascript">

$(function(){
	$('select').selected();
	var c = "${lotCode}";
	var d = "${date}";
	if(c=="LHC" || c=="FC3D"||c=="PL3"){
		$('#startDate').cxCalendar({format:'YYYY-MM',position:'bottomRight'});
		if(d != null){
			d = d.split("-")[1];
			var diff = parseInt(d) - parseInt(getMonth());
			$(".drawing-head .db-config li").each(function(index,item){
				if(parseInt(diff) == parseInt($(item).attr("daydiff"))){
					$(this).addClass("active");
				}
			})
		}
	}else{
		$('#startDate').cxCalendar({format:'YYYY-MM-DD',position:'bottomRight'});
		if(d != null){
			d = d.split("-")[2];
			var diff = parseInt(d)-parseInt(getDate());
			$(".drawing-head .db-config li").each(function(index,item){
				if(parseInt(diff) == parseInt($(item).attr("daydiff"))){
					$(this).addClass("active");
				}
			})
		}
	}
	$(".main-result").on("change","#startDate",function(){
		var val = $(this).val();
		window.location.href='${base}/lotteryV2/resultList.do?lotCode=${lotCode}&startDate='+val;
	}).on("click",".db-config li.AwardDiff",function(){
		var diff = $(this).attr("daydiff"),date;
		if(c == "LHC" || c=="FC3D" || c=="PL3"){
			date = getLHCDataStr(diff);
		}else{
			date = getDataStr(diff);
		}
		window.location.href='${base}/lotteryV2/resultList.do?lotCode=${lotCode}&startDate='+date;
	}).on("change","#lotteryResult",function(){
		var val = $(this).val();
		window.location.href='${base}/lotteryV2/resultList.do?lotCode='+val+'';
	})
	$(".tp-ui-icon").click(function(){
			$(this).parent().find("input").focus();
		})
})

function getDataStr(diff){
	var date = new Date();
	date.setDate(date.getDate()+parseInt(diff));
	return $.formatDate("yyyy-MM-dd",date,0);
}

function getLHCDataStr(diff){
	var date = new Date(),year,month;
	year = date.getFullYear();
	month = (date.getMonth()+1)+parseInt(diff);
	if(month<10){month = "0"+month;}
	return year+"-"+month;
}

function getDate(){
	var date = new Date();
	return date.getDate();
}
function getMonth(){
	var date = new Date();
	return date.getMonth()+1;
}
</script>