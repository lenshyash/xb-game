<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/css/member.css?v=34341990dd" rel="stylesheet">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>
<body>
<div class="content">
			<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
			<div class="membersubnavi">
           		<div class="subnavi">
                 	<span class="blue">基本信息</span>
                 	<div class="subnaviarrow"></div>
                 </div>
                 <!-- <div class="subnavi">
                 	<span>帐号设置</span>
                 </div> -->
                 <div class="subnavi">
                 	<a href="${base}/center/news/message/cPage.do" target="frame">站内信</a>
                 </div>
            </div>
				<!-- <div class="account-content-container">
				<div class="account-content">
						<h3 class="title">个人资料 <small style="margin-left: 10px;">
						绑定真实姓名可提高帐号的安全性
						</small> </h3>
						<div class="security-setting">
							<table class="table">
								<tbody>
									<tr>
										<td class="align-right-col">姓名：</td>
										<td>
										<input id="userName" class="textfield1 form-control" type="text">
										</td>
									</tr>
									<tr>
										<td class="align-right-col">邮箱：</td>
										<td>
											<input id="email" class="textfield1 form-control" type="text">
										</td>
									</tr>

									<tr class="last-row">
										<td>&nbsp;</td>
										<td colspan="3">
											<button class="btn btn-blue" onclick="bandName()">保存</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>	 -->
				
	<!-- <div id="popup-verifyemail" style="display:none">
		<div class="popupinternal">
			<div class="steps">
				<div class="substeps">
					<ul>
						<li class="step1 active"><p>输入邮箱地址</p></li>
						<li class="step2"><p>填写验证码</p></li>
						<li><p>完成</p></li>
					</ul>
				</div>
				<table class="table">
					<tbody>
						<tr id="mailPage">
							<td style="width:17%;">邮箱地址：</td>
							<td><input class="form-control" id="txtEmail" type="text"></td>
							<td>邮箱可用于找回密码</td>
						</tr>
						<tr style="display:none" id="mailCodePage">
							<td style="width:17%;">验证码：</td>
							<td><input class="form-control" id="txtEmailCode" type="text"></td>
							<td><input class="get_mail submitbtn3" value="点击获取" style="width:160px;" id="get_mail" type="button"></td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">我们不会泄漏任何用户信息，以及发送骚扰邮件。</td>
						</tr>

						<tr>
							<td colspan="3" class="merged-cell">
							<input id="btnCheckMail" class="submitbtn3" value="下一步" onclick="checkUserMail();" type="button">
							<input id="btnCheckMailCode" class="submitbtn3" value="下一步" style="display:none" onclick="bindMail();" type="button">
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="popup-verifyeditemail" style="display:none">
		<div class="popupinternal">
			<div class="steps">
				<div class="substeps">
					<ul>
						<li class="step1"><p>输入邮箱地址</p></li>
						<li class="step2 active"><p>填写验证码</p></li>
						<li><p>完成</p></li>
					</ul>
				</div>
				<table class="table">
					<tbody>
						<tr>
							<td style="width:17%;">绑定邮箱：</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="width:17%;">验证码：</td>
							<td><input class="form-control" id="txtEmailCode1" type="text"></td>
							<td><input class="get_unbindmail submitbtn3" style="width:160px;" value="点击获取" type="button"></td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">我们不会泄漏任何用户信息，以及发送骚扰邮件。</td>
						</tr>

						<tr>
							<td colspan="3" class="merged-cell">
							<input id="btnEditMail" class="submitbtn3" value="下一步" onclick="editEmail();" type="button">
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="popup-verifymobile" style="display:none">
		<div class="popupinternal">
			<div class="steps">
				<div class="substeps">
					<ul>
						<li class="step1 active"><p>输入手机号码</p></li>
						<li class="step2"><p>填写验证码</p></li>
						<li><p>完成</p></li>
					</ul>
				</div>
				<table class="table">
					<tbody>
						<tr id="mobilePage">
							<td style="width:17%;">手机号码：</td>
							<td><input class="form-control" id="txtMobile" type="text"></td>
							<td></td>
						</tr>
						<tr style="display:none" id="mobileCodePage">
							<td style="width:17%;">验证码：</td>
							<td><input class="form-control" id="txtMobileCode" type="text"></td>
							<td><input class="get_mobile submitbtn3" value="点击获取" style="width:160px;" id="get_mobile" type="button"></td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">我们不会泄漏任何用户信息，以及发送骚扰短信。</td>
						</tr>

						<tr>
							<td colspan="3" class="merged-cell">
							<input id="btnCheckMobile" class="submitbtn3" value="下一步" onclick="checkUserMobile();" type="button">
							<input id="btnCheckMobileCode" class="submitbtn3" value="下一步" style="display:none" onclick="bindMobile();" type="button">
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="popup-verifyeditmobile" style="display:none">
		<div class="popupinternal">
			<div class="steps">
				<div class="substeps">
					<ul>
						<li class="step1"><p>输入手机号码</p></li>
						<li class="step2 active"><p>填写验证码</p></li>
						<li><p>完成</p></li>
					</ul>
				</div>
				<table class="table">
					<tbody>
						<tr>
							<td style="width:17%;">手机号码：</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="width:17%;">验证码：</td>
							<td><input class="form-control" id="txtMobileCode1" type="text"></td>
							<td><input class="get_unbindmobile submitbtn3" style="width:160px;" value="点击获取" type="button"></td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">我们不会泄漏任何用户信息，以及发送骚扰短信。</td>
						</tr>

						<tr>
							<td colspan="3" class="merged-cell">
							<input id="btnEditMobile" class="submitbtn3" value="下一步" onclick="editMobile();" type="button">
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="popup-verifywechat" style="display:none">
		<div class="popupinternal">
			<div class="steps">
				<div class="substeps">
					<ul>
						<li class="step1 active"><p>输入微信号</p></li>
					</ul>
				</div>
				<table class="table">
					<tbody>
						<tr id="wehcatPage">
							<td style="width:17%;">微信号：</td>
							<td><input class="form-control" id="txtWechat" type="text"></td>
							<td></td>
						</tr>
						<tr style="display:none" id="wechatCodePage">
							<td style="width:17%;">验证码：</td>
							<td><input class="form-control" id="txtWechatCode" type="text"></td>
							<td><input class="get_wechat submitbtn3" value="点击获取" style="width:160px;" id="get_wechat" type="button"></td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">我们不会泄漏任何用户信息，以及发送骚扰短信。</td>
						</tr>
						<tr>
							<td colspan="3" class="merged-cell">
							<input id="btnCheckWechat" class="submitbtn3" value="绑定" onclick="checkUserWechat();" type="button">
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="popup-verifycontact" style="display:none">
		<div class="popupinternal">
			<table class="table">
				<tbody>
					<tr>
						<td style="width:17%;">旧密码：</td>
						<td><input id="oldPassword" style="width: 279px;" class="form-control" type="password"></td>
					</tr>
					<tr>
						<td style="width:17%;">新密码：</td>
						<td><input name="password" style="width: 279px;" class="form-control" type="password"></td>
					</tr>
					<tr>
						<td style="width:17%;">确认密码：</td>
						<td><input id="ckPassword" style="width: 279px;" class="form-control" type="password"></td>
					</tr>
					<tr>
						<td colspan="2" class="merged-cell">
							<input class="submitbtn3" value="确认修改" onclick="changePassword()" type="button">
						</td>
					</tr>
				</tbody>
			</table>
		</div> -->
	</div>
	<script type="text/javascript">
		
	</script>
</div>
</body>
</html>