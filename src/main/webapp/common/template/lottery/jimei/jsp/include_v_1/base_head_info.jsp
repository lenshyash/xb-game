<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="memberheader">
	<div class="useravatar floatleft">
		<img src="${base}/common/template/lottery/jimei/images/user-icon.png" alt="" width="84" height="83">
	</div>
	<div class="memberinfo floatleft">
		<h1 class="floatleft">
			<script language="javaScript">
				now = new Date(), hour = now.getHours()
				if (hour < 6) {
					document.write("凌晨好")
				} else if (hour < 9) {
					document.write("早上好")
				} else if (hour < 12) {
					document.write("上午好")
				} else if (hour < 14) {
					document.write("中午好")
				} else if (hour < 17) {
					document.write("下午好")
				} else if (hour < 19) {
					document.write("傍晚好")
				} else if (hour < 22) {
					document.write("晚上好")
				} else {
					document.write("夜里好")
				}
			</script>
		 <span class="blue account">${loginMember.account}</span>
		</h1>
		<div class="balance floatleft" id="levelIcon" style="display:none;">
			<img alt="" src="" width="70" height="35">
		</div>
		<div class="balance floatleft">
			<div class="balancevalue floatleft">
				中心钱包 : <span class="blue"><span class="balanceCount">${loginMember.money}</span></span>
			</div>
			<div class="floatright margintop7 marginright10 marginleft5 pointer">
				<a onclick="flush()" href="javascript:;"><img src="${base}/common/template/lottery/jimei/images/btnrefresh.jpg" alt="" width="16" height="17"> </a>
			</div>
			<div class="balancevalue floatleft">
				<c:if test="${not empty isScore && isScore eq 'on'}">
					会员积分 : <span class="blue"><span class="balance" money="0">${userInfo.score}</span></span>
				</c:if>
			</div>
		</div>
		<div class="gap5"></div>
		<p>欢迎来到${_title}。专注彩票网投领导者，一路有你相伴，让梦想飞！</p>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		accountLevel();
	})
	
	function flush(){
		meminfo();
	}
	function meminfo(){
		var money = $(".balanceCount"),account=$(".account")
		$.ajax({
			url: "${base}/meminfo.do",
			success:function(res){
				if(res.login){
					account.html(res.account);
					money.html(res.money);
				}
			},
			beforeSend:function(){
				money.html("加载中...");
			} 
		})
	}
	
	function accountLevel(){
		$.ajax({
			url:"${base}/center/member/meminfo/accountLevel.do",
			success:function(res){
				if(res.success && res.icon){
					$("#levelIcon").show().find('img').attr("src",res.icon);
				}else{
					$("#levelIcon").hide();
				}
			}
		})
	}
</script>