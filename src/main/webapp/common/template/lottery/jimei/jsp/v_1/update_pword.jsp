<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/lotteryV2.js?v=${caipiao_version}"></script>
<link rel="stylesheet" href="${base}/common/bootstrap/3.3.6/css/bootstrap.min.css"  type="text/css">
<link href="${base}/common/template/lottery/jimei/js/assets/css/amazeui.min.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script src="${base}/common/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
		JIMEI = {
				base:'${base}',
	    		template:'v2_1',
		}
</script>
<style type="text/css">
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    vertical-align: middle;
	}
.form-group .am-selected-btn {
    font-size: 15px;
    padding: 11px 11px;
}
.am-selected {
    background: #ffffff none repeat scroll 0 0;
}
</style>
</head>
<body class="skin_blue" style="font-size:12px;">
	<!-- 内容添加处.. -->
	<div id="_main_view">
		<div class="form-group">
			<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title" id="editLabel">修改密码</h2>
				</div>
				<div class="modal-body">
					<input id="accountId" type="hidden">
					<table class="table table-bordered table-striped" style="clear: both">
						<tbody>
							<tr>
								<td class="text-right" width="15%">密码类型：</td>
								<td class="text-left" width="35%">
									<select id="updType" class="form-control" data-am-selected="{btnWidth: '150px', btnSize: 'lg'}">
											<option value="1">登录密码</option>
											<option value="2">取款密码</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="text-right" width="15%">旧密码：</td>
								<td class="text-left" width="35%">
								<input id="opassword" name="opassword" class="form-control" type="password" size="12" maxlength="12"> 
								</td>
							</tr>
							<tr>
								<td class="text-right" width="15%">新密码：</td>
								<td class="text-left" width="35%">
								<input id="password" name="password" class="form-control" type="password" size="12" maxlength="12"> 
								</td>
							</tr>
							<tr>
								<td class="text-right" width="15%">确认新密码：</td>
								<td class="text-left" width="35%">
								<input id="rpassword" name="rpassword" class="form-control" type="password" size="12" maxlength="12"> 
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearVal()">重置</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updpwd();">修改</button>
				</div>
			</div>
		</div>
	</div>
	</div>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/assets/js/amazeui.min.js"></script>
	<script type="text/javascript">
	function updpwd() {
		$('select').selected();
		var opassword = $("#opassword").val();
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if (!opassword) {
			layer.alert("旧密码不能为空！",{icon:2});
			return;
		}
		if (!password) {
			layer.alert("新密码不能为空！",{icon:2});
			return;
		}
		if (!rpassword) {
			layer.alert("确认不能为空！",{icon:2});
			return;
		}

		if (password !== rpassword) {
			layer.alert("两次密码不一致！",{icon:2});
			return;
		}
		layer.confirm("您确定修改密码？",{btn:['确定','取消']},function(){
			$.ajax({
				type: 'POST',
				url : "${base}/center/member/meminfo/newpwd.do",
				data : {
					opwd : opassword,
					pwd : password,
					rpwd : rpassword,
					updType : $("#updType").val()
				},
				success : function(result) {
					if(!result.success){
						layer.msg(result.msg,{icon:6});
					}else{
						layer.alert("修改成功！",{icon:1},function(index){
							//刷新父页面
							parent.location.reload();
							layer.close(index);
						});
					}
				}
			});
		},function(index){
			clearVal();
			layer.close(index);
		})
		
	}
	
	function clearVal(){
		$("#opassword").val('');
		$("#password").val('');
		$("#rpassword").val('');
	}
</script>
</body>
</html>
