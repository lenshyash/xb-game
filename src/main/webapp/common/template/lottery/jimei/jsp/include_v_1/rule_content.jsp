<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<link href="${base}/common/template/lottery/jimei/css/rule.css"
	rel="stylesheet">
<link href="${base}/common/template/lottery/jimei/css/test.css"
	rel="stylesheet">
<link
	href="${base}/common/template/lottery/jimei/js/assets/css/amazeui.min.css"
	rel="stylesheet">
<section class="main-result" id="_main_view">
	<div class="chart_table_search_wrapper">
		<div class="drawing-head">
			<div class="db-config" style="left: 0">
				<ul>
					<li><select id="lotteryRule"
						data-am-selected="{btnWidth: '150px', btnSize: 'lg',maxHeight: 300}"
						placeholder="全部彩种">
							<option value=""></option>
							<c:forEach items="${lotteryV2List}" var="lot"
								varStatus="lotIndex">
								<c:choose>
									<c:when
										test="${not empty param.lotCode && param.lotCode == lot.code}">
										<option value="${lot.code}" selected="selected">${lot.name}</option>
									</c:when>
									<c:when test="${lot.code == 'LHC'}">
										<c:if test="${mark_on_off == 'on'}">
											<option value="${lot.code}">${lot.name}</option>
										</c:if>
									</c:when>
									<c:otherwise>
										<option value="${lot.code}">${lot.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="rule-content">
		<div class="ng-scope BJSC XYFT LXYFT AZXY10 SFSC SFFT FFSC WFSC SFFT LBJSC WFFT" style="display: block;">
			<div class="LBJSCOPEN">
				<strong><span class="colorName">老北京赛车</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" align="center">
							游戏项目</th>
						<th class="subtitle2" width="25%" align="center">开奖时间</th>
						<th class="subtitle2" width="25%" align="center">每日期数</th>
						<th class="subtitle2" width="25%" align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">北京赛车</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							9:07&mdash;23:57(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-179</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">老北京赛车</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="BJSCOPEN">
				<strong><span class="colorName">北京赛车</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">北京赛车</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:30&mdash;23:50(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-44</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">北京赛车</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="SFFTOPEN">
				<strong><span class="colorName">极速飞艇</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" align="center">
							游戏项目</th>
						<th class="subtitle2" width="25%" align="center">开奖时间</th>
						<th class="subtitle2" width="25%" align="center">每日期数</th>
						<th class="subtitle2" width="25%" align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">极速飞艇</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">幸运飞艇</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="WFFTOPEN">
				<strong><span class="colorName">五分飞艇</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" align="center">
							游戏项目</th>
						<th class="subtitle2" width="25%" align="center">开奖时间</th>
						<th class="subtitle2" width="25%" align="center">每日期数</th>
						<th class="subtitle2" width="25%" align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">五分飞艇</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">幸运飞艇</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="XYFTOPEN">
				<strong><span class="colorName">幸运飞艇</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">幸运飞艇</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								13:04&mdash;04:04(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-180</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">幸运飞艇</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="LXYFTOPEN">
				<strong><span class="colorName">老幸运飞艇</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">老幸运飞艇</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								13:04:05&mdash;04:04:05(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-180</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">老幸运飞艇</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="AZXY10OPEN">
				<strong><span class="colorName">澳洲幸运10</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">澳洲幸运10</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								00:04:00&mdash;23:59:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">澳洲幸运10</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="SFSCOPEN">
				<strong><span class="colorName">极速赛车</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">极速赛车</td>
							<td class="point" bgcolor="#FFF7F0" align="center">全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">极速赛车</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="SFFTOPEN">
				<strong><span class="colorName">极速飞艇</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">极速飞艇</td>
							<td class="point" bgcolor="#FFF7F0" align="center">全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">极速飞艇</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="WFSCOPEN">
				<strong><span class="colorName">五分赛车</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">五分赛车</td>
							<td class="point" bgcolor="#FFF7F0" align="center">全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">五分赛车</span>具体游戏规则如下︰</strong><br>
			</div>
			<div class="FFSCOPEN">
				<strong><span class="colorName">疯狂赛车</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">疯狂赛车</td>
							<td class="point" bgcolor="#FFF7F0" align="center">全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-1152</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每1分15秒</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">疯狂赛车</span>具体游戏规则如下︰</strong><br>
			</div>
			<h2>两面盘（冠军~第十名）</h2>
			<dd>
				<ul>
					<li>大小∶开出之号码大於或等於6为大，小於或等於5为小。</li>
					<li>单双∶开出之号码为双数叫双，如4、8；号码为单数叫单，如5、9。</li>
					<li>冠军 龙/虎∶"第一名"之车号大於"第十名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
					<li>亚军 龙/虎∶"第二名"之车号大於"第九名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
					<li>季军 龙/虎∶"第三名"之车号大於"第八名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
					<li>第四名 龙/虎∶"第四名"之车号大於"第七名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
					<li>第五名 龙/虎∶"第五名"之车号大於"第六名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
				</ul>
			</dd>
			<h2>两面盘（冠亚和）: "冠军车号 + 亚军车号 = 冠亚和"</h2>
			<dd>
				<ul>
					<li>冠亚和大小∶以冠军车号和亚军车号之和大小来判断胜负。"冠亚和"大於11为大，小於11为小${dx11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
					<li>冠亚和单双∶以冠军车号和亚军车号之和单双来判断胜负，"冠亚和"为双数叫双，为单数叫单${ds11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
					<li>冠亚和指定:以指定冠军车号和亚军车号之和来判断胜负；冠亚车号总和可能出现结果为3~19，投中对应"冠亚和指定"数字视为中奖，其馀情形视为不中奖</li>
				</ul>
			</dd>
			<h2>冠军~第十名车号指定:</h2>
			<dd>
				<ul>
					<li>冠军~第十名车号指定: 每一个车号为一投注组合，开奖结果"投注车号"对应所投名次视为中奖，其馀情形视为不中奖。</li>
				</ul>
			</dd>
		</div>
		<div class="ng-scope PCEGG JND28 SF28" style="display: none">
			<div class="PCEGGOPEN">
			<strong><span class="colorName">PC蛋蛋</span>开奖时间：</strong>
			<table class="awardList">
				<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">
							PC蛋蛋</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							09:00&mdash;23:55(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-179</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
				</tbody>
			</table>
			<br> <stro class="colorName"ng> PC蛋蛋</strong>具体游戏规则如下︰</strong><br>
			<h2>游戏规则:</h2>
			<dd>
				<ul>
					<li>按北京快乐8的开奖结果为基础；</li>
					<li>将当期第三方开奖数据从小到大进行排序；</li>
					<li>排序后,分别按号码第 1~6 , 7~12 , 13~18 分为【1】【2】【3】三个区；</li>
					<li>把每个区的数字分别相加；</li>
					<li>三个数值相加即为幸运28最终的开奖结果。</li>
				</ul>
			</dd>
			</div>
			<div class="SF28OPEN">
			<strong><span class="colorName">极速28</span>开奖时间：</strong>
			<table class="awardList">
				<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">
							PC蛋蛋</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							09:00&mdash;23:55(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-179</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
				</tbody>
			</table>
			<br> <stro class="colorName"ng> 极速28</strong>具体游戏规则如下︰</strong><br>
			<h2>游戏规则:</h2>
			<dd>
				<ul>
					<li>按北京快乐8的开奖结果为基础；</li>
					<li>将当期第三方开奖数据从小到大进行排序；</li>
					<li>排序后,分别按号码第 1~6 , 7~12 , 13~18 分为【1】【2】【3】三个区；</li>
					<li>把每个区的数字分别相加；</li>
					<li>三个数值相加即为极速28最终的开奖结果。</li>
				</ul>
			</dd>
			</div>
			<div class="JND28OPEN">
			<strong><span class="colorName">加拿大28</span>开奖时间：</strong>
			<table class="awardList">
				<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
							开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0" align="center">
							加拿大28</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">不固定</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每3分半钟</td>
					</tr>
				</tbody>
			</table>
			<br> <stro class="colorName"ng> 加拿大28</strong>具体游戏规则如下︰</strong><br>
			<h2>游戏规则:</h2>
			<dd>
				<ul>
					<li>加拿大28采用加拿大彩票公司BCLC的开奖结果为基础；</li>
					<li>【BCLC官网：lotto.bclc.com】。每三分半一期，开奖时间全天。(每天维护时间为19:00~21:30，周一可能会有延迟)</li>
					<li>将当期第三方开奖数据从小到大进行排序；</li>
					<li>排序后,分别按号码第一区[第2/5/8/11/14/17位数字] 8,17,34,42,58,69，第二区[第3/6/9/12/15/18位数字] 14,22,39,48,63,72，第三区[第4/7/10/13/16/19位数字] 16,26,41,54,64,73</li>
					<li>把每个区的数字分别相加；</li>
					<li>三个数值相加即为加拿大28最终的开奖结果。</li>
				</ul>
			</dd>
			</div>
			<h2>投注规则:</h2>
			<c:if test="${!pceggDrawShow  }">
				<style>
					.hidePcegg{
						display:none;
					}
				</style>
			</c:if>
			<dd id="JND28">
				<ul>
					<li>大：三个位置的数值相加和大于等于14，15,16,17,18,19,20,21,22,23,24,25,26，27为大。
						<span class="hidePcegg">举例出14押大回本本金。</span>
					</li>
					<li>小：三个位置的数值相加和小于等于0，01,02,03,04,05,06,07,08,09,10,11,12，13为小。
						<span class="hidePcegg">举例出13押小回本本金。</span></li>
					<li>单：数字1,3,5,7,9,11,13,15,17,19,21,23,25,27为单。<span class="hidePcegg">举例出13押单回本本金。</span></li>
					<li>双：数字0,2,4,6,8,10,12,14,16,18,20,22,24,26为双。<span class="hidePcegg">举例出14押双回本本金。</span></li>
					<li>大单（三个数值和）：15,17,19,21,23,25,27为大单。</li>
					<li>小单（三个数值和）：01,03,05,07,09,11,13为小单。<span class="hidePcegg">举例买100元小单开13就回本金。</span></li>
					<li>大双（三个数值和）：14,16,18,20,22,24,26为大双。<span class="hidePcegg">举例买100元大双开14就回本金。</span></li>
					<li>小双（三个数值和）：00,02,04,06,08,10,12为小双。</li>
					<li>极大（三个数值和）：22,23,24,25,26,27为极大。</li>
					<li>极小（三个数值和）：00,01,02,03,04,05为极小。</li>
					<li>红波（三个数值和）：03,06，09,12,15,18,21,24为红波。</li>
					<li>绿波（三个数值和）：01,04,07,10,16,19,22，25为绿波。</li>
					<li>蓝波（三个数值和）：02,05,08,11,17,20,23,26为蓝波。</li>
					<li>购买波色如果三个数值和为0,13,14,27视为不中奖</li>
					<li>特码包三（三个数值和）：可選取三個數字投注，選取三個數字當中任何一個與開獎結果相同即中獎，（比如:开奖结果 15
						选择 11 15 20,那就视为中奖）。</li>
					<li>特码（三个数值和）：單選取一個數字投注。</li>
					<li>豹子：當期開出三個數字相同即為豹子（ 例
						0+0+0=0,1+1+1=3,2+2+2=6,3+3+3=9,4+4+4=12,5+5+5=15,6+6+6=18,7+7+7=21,8+8+8=24，9+9+9=27）。
					</li>
					<c:if test="${domainFolder == 'a101'}">
					<li>若开奖特别号码13、14大小单双赔率1.85倍、组合小单大双2.5倍（都是含本金的）</li>
					</c:if>
				</ul>
			</dd>
		</div>
		<input type="hidden" value="${domainFolder}" id="domainFolder" />
		<script type="text/javascript">
			if ($("#domainFolder").val() == "a010101") {
				$("#JND28 ul").append('<li style="list-style:none">同期组合只能投注俩组（ 例如大单，大双）投注组合俩组以上视为违规投注。</li>')
			}
		</script>
		<div class="ng-scope CQSSC TJSSC XJSSC FFC EFC WFC HNFFC HNWFC AZXY5 HKWFC AMWFC ESFC SFC"
			style="display: none;">
			<div class="CQSSCOPEN">
				<!-- <strong style="color: #c9171e">重庆时时彩游戏是依照中国重庆福利彩票发行中心统一发行的『时时彩』彩票的开奖数据为依据所规划的线上彩票游戏。</strong> -->
				<br> <strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br>
				<strong><span class="colorName">重庆时时彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point" rowspan="3 colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">重庆时时彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								0:30-3:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-09</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
						<tr>
							<td class="point" bgcolor="#FFF7F0" align="center">
								7:30-23:50(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">10-59</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
						<!-- <tr>
							<td class="point" bgcolor="#FFF7F0" align="center">
								00:05-01:55(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-023</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr> -->
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">重庆时时彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="TJSSCOPEN">
				<strong style="color: #c9171e;">天津时时彩由中国福利彩票发行管理中心组织，由天津市福利彩票发行中心在所辖区域内承销。</strong>

				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">天津时时彩</span>开奖时间：</strong>

				<table class="awardList">

					<tbody>
						<tr>

							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>

							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>

							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>

							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>

						</tr>

						<tr>

							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">天津时时彩</td>

							<td class="point" bgcolor="#FFF7F0" align="center">

								09:20-23:00(北京时间)</td>

							<td class="point" bgcolor="#FFF7F0" align="center">01-42</td>

							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>

						</tr>

					</tbody>
				</table>

				<br> <strong> 本公司<span class="colorName">天津时时彩</span>具体游戏规则如下： </strong> <br>
			</div>
			<div class="XJSSCOPEN">
				<strong style="color: #c9171e;">新疆时时彩由中国福利彩票发行管理中心组织，由新疆自治区福利彩票发行中心在所辖区域内承销。</strong><br>
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">新疆时时彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">新疆时时彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								10:20-2:00 (北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-48</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">新疆时时彩</span>具体游戏规则如下：</strong><br>
			</div>
			<div class="FFCOPEN">
				<!-- <strong style="color: #c9171e">分分彩是研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">分分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">分分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								0001-1440</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">分分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="EFCOPEN">
				<!-- <strong style="color: #c9171e">二分彩是研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">二分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">二分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-720</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每2分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">二分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="ESFCOPEN">
				<!-- <strong style="color: #c9171e">二分彩是研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">二分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">二十分彩</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时不间断</td>
						<td class="point" bgcolor="#FFF7F0" align="center">01-72</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">二十分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="SFCOPEN">
				<!-- <strong style="color: #c9171e">二分彩是研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">二分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">十分彩</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时不间断</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-144</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每2分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">十分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="HNFFCOPEN">
				<!-- <strong style="color: #c9171e">河内分分彩是境外研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">河内分分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">河内分分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">0001-1440</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">河内分分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="HNWFCOPEN">
				<!-- <strong style="color: #c9171e">河内五分彩是境外研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">河内五分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">河内五分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">河内五分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="AZXY5OPEN">
				<!-- <strong style="color: #c9171e">澳洲幸运5是境外研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">河内五分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">河内五分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">澳洲幸运5</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="HKWFCOPEN">
				<!-- <strong style="color: #c9171e">河内五分彩是境外研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">河内五分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">香港五分彩</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时不间断</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">香港五分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<div class="AMWFCOPEN">
				<!-- <strong style="color: #c9171e">河内五分彩是境外研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">河内五分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">澳门五分彩</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							全天24小时不间断</td>
						<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">澳门五分彩</span>具体游戏规则如下︰</strong> <br>
			</div>

			<div class="WFCOPEN">
				<!-- <strong style="color: #c9171e">五分彩是研发的趣味更多，频次更快的时时彩游戏</strong> <br> -->
				<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong><span class="colorName">五分彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">五分彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong>本公司<span class="colorName">五分彩</span>具体游戏规则如下︰</strong> <br>
			</div>
			<h2>选号玩法</h2>
			<dl>
				<dt>◎一字</dt>
				<dd>
					<ul>
						<li>全五：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。如超过1颗球落在所选号码内，派彩将倍增<br>
							※举例：下注一字【5号】＄100，一字賠率2.05，（第二注开始需要扣除本金）<br> 五颗球开出1，2，3，4，5
							派彩为＄205<br> 五顆球开出1，2，3，5，5 派彩为＄205+105=310<br>
							五顆球开出1，2，5，5，5 派彩为＄205+105+105=415
						</li>
						<li>前三：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位]任一数与所选的号码相同时，即为中奖。</li>
						<li>中三：0~9任选1个号进行投注，当开奖结果[仟位、佰位、拾位]任一数与所选的号码相同时，即为中奖。</li>
						<li>后三：0~9任选1个号进行投注，当开奖结果[佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。</li>
					</ul>
				</dd>
				<dt>◎二字</dt>
				<dd>
					<ul>
						<li>于前三、中三、后三0~9任选2个号进行投注，当开奖结果任二数与所选的号码相同时，即为中奖。<br>
							※举例：投注者购买二字后三，选择号码如为11，当期开奖结果如为xx11x、xx1x1、xxx11、皆视为中奖。（x=0~9任一数）<br>
							※举例：投注者购买二字后三，选择号码如为12，当期开奖结果如为xx12x、xx1x2、xx21x、xx2x1、xxx12、xxx21皆视为中奖。（x=0~9任一数）
						</li>
						<li>【附注】：以上二例赔率不同</li>
					</ul>
				</dd>
				<dt>◎三字</dt>
				<dd>
					<ul>
						<li>于前三、中三、后三0~9任选3个号进行投注，当开奖结果与所选的号码相同时（顺序不限），即为中奖。<br>
							若是开出豹子如000、111、222、333、444、555、666、777、888、999算不中奖。
						</li>
					</ul>
				</dd>
				<dt>◎二字定位</dt>
				<dd>
					<ul>
						<li>于万仟佰拾个任选二位，自0~9任选2个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
					</ul>
				</dd>

				<dt>◎三字定位</dt>
				<dd>
					<ul>
						<li>于前三、中三、后三任选三位，自0~9任选3个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
					</ul>
				</dd>
				<dt>◎组选三</dt>
				<dd>
					<ul>
						<li>
							前三：会员可以挑选5~10个号码，当开奖结果[万位、仟位、佰位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[万位、仟位、佰位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br>
							※例如：112、344，若是开出豹子则不算中奖。<br> ※备注："豹子"为三字同号，例如：111、222
						</li>
						<li>
							中三：会员可以挑选5~10个号码，当开奖结果[仟位、佰位、拾位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[仟位、佰位、拾位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br>
							※例如：112、344，若是开出豹子则不算中奖。<br> ※备注："豹子"为三字同号，例如：111、222
						</li>
						<li>
							后三：会员可以挑选5~10个号码，当开奖结果[佰位、拾位、个位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[佰位、拾位、个位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。
							<br>※例如：112、344，若是开出豹子则不算中奖。<br>
							※备注："豹子"为三字同号，例如：111、222
						</li>
					</ul>
				</dd>
				<dt>◎组选六</dt>
				<dd>
					<ul>
						<li>前三：会员可以挑选4~8个号码，当开奖结果[万位、仟位、佰位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br>
							※例如：如果是选择(1、2、3、4)，则开奖结果[万位、仟位、佰位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
						</li>
					</ul>
				</dd>

				<dd>
					<ul>
						<li>中三：会员可以挑选4~8个号码，当开奖结果[仟位、佰位、拾位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br>
							※例如：如果是选择(1、2、3、4)，则开奖结果[仟位、佰位、拾位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
						</li>
					</ul>
				</dd>

				<dd>
					<ul>
						<li>
							后三：会员可以挑选4~8个号码，当开奖结果[佰位、拾位、个位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。
							<br>※例如：如果是选择(1、2、3、4)，则开奖结果[仟位、佰位、拾位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
						</li>
					</ul>
				</dd>

				<dt>◎跨度</dt>
				<dd>
					<ul>
						<li>前三：以开奖结果[万位、仟位、佰位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的跨度为5。（最大号码8减最小号码3=5）。
						</li>
					</ul>
				</dd>
				<dd>
					<ul>
						<li>中三：以开奖结果[仟位、佰位、拾位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的跨度为4。（最大号码8减最小号码4=4）。
						</li>
					</ul>
				</dd>
				<dd>
					<ul>
						<li>后三：以开奖结果[佰位、拾位、个位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的跨度为2。（最大号码8减最小号码6=2）。
						</li>
					</ul>
				</dd>
			</dl>
			<h2>和数玩法</h2>
			<dl>
				<dt>◎前三和数</dt>
				<dd>
					<ul>
						<li>开奖结果前面三位数的总和值与投注数字相同时，即为中奖。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的和值为15。（3+4+8=15）。
						</li>
					</ul>
				</dd>
				<dt>◎中三和数</dt>
				<dd>
					<ul>
						<li>开奖结果中间三位数的总和值与投注数字相同时，即为中奖。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的和值为19。（4+8+7=19）
						</li>
					</ul>
				</dd>
				<dt>◎后三和数</dt>
				<dd>
					<ul>
						<li>开奖结果后面三位数的总和值与投注数字相同时，即为中奖。<br>
							※举例：开奖结果为3、4、8、7、6。中奖的和值为21。（8+7+6=21）
						</li>
					</ul>
				</dd>
				<dt>◎和尾数</dt>
				<dd>
					<ul>
						<li>于万仟佰拾个任选二位数加起来的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
							尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
							尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
							※举例：开奖结果为3、4、5、0、0。万千和为7，尾数为7，属于“大”、“单”、“质”。<br>
						</li>
					</ul>
				</dd>
				<dt>◎前三和数尾数</dt>
				<dd>
					<ul>
						<li>以开奖号码的前三位数和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
							尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
							尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
							※举例：开奖结果为3、4、5、6、7。前三和为12，尾数为2，属于“小”、“双”、“质”。<br>
						</li>
					</ul>
				</dd>
				<dt>◎中三数和尾数</dt>
				<dd>
					<ul>
						<li>以开奖号码的中间三位数的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
							尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
							尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
							※举例：开奖结果为3、4、5、6、7。中三和为15，尾数为5，属于“大”、“单”、“质”。<br>
						</li>
					</ul>
				</dd>

				<dt>◎后三和数尾数</dt>
				<dd>
					<ul>
						<li>以开奖号码的后三位数的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
							尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
							尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
							※举例：开奖结果为3、4、5、6、7。后三和为18，尾数为8，属于“大”、“双”、“合”。<br>
						</li>
					</ul>
				</dd>
			</dl>
			<h2>两面玩法</h2>
			<dl>
				<dt>◎整合</dt>
				<dd>
					<ul>
						<li>总和：所有开奖号码相加，作为中奖依据。会员可以选择总和大、总和小、总和单、总和双、龙、虎、和 的任一号码。<br>
							总和大于等于23为“总和大”，小于23为“总和小”<br> 总和为单数时为“总和单”，若为双数时为“总和双”<br>
							总和大于10，且十位数字大于个位数字时为“龙”；总和小于10，或者十位数字小于个位数字时为“虎”；总和大于10，且十位数字等于个位数字时为“和”<br>
							※举例：开奖结果为3、4、5、6、7。总和为25，属于“总和大”、“总和单”、“虎”。
						</li>
					</ul>
				</dd>
				<dt>◎整合单位玩法</dt>
				<dd>
					<ul>
						<li>于万仟佰拾个任选一位中，会员选择0-9、大、小、单、双、质、合 的任一号码。<br>
							尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
							尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
							※举例：开奖结果为3、4、5、6、7。万位为3，属于“小”、“单”、“质”。
						</li>
					</ul>
				</dd>
				<dt>◎万位、仟位、佰位、拾位、个位玩法</dt>
				<dd>
					<ul>
						<li>从0-9中任选一位号码进行投注，当投注数字与开奖结果相符时，即为中奖。<br>
							※举例：投注者购买佰位3，当期开奖结果如为20352，则视为中奖。
						</li>
						<li>
							开奖结果万位、仟位、佰位、拾位或个位数字为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”，当投注位数单双与开奖结果的位数单双相符时，即为中奖。
							<br>※举例：投注者购买佰位单，当期开奖结果如为20130（1为单），则视为中奖。
							<table width="100%">
								<tbody>
									<tr>
										<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">单</th>
										<th width="50%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">双</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">1、 3、 5、 7、 9</td>
										<td bgcolor="#FFF7F0" class="point" align="center">0、 2、
											4、 6、 8</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							开奖结果万位、仟位、佰位、拾位或个位数字为5、6、7、8、9时为“大”，若为0、1、2、3、4时为“小”，当投注位数大小与开奖结果的位数大小相符时，即为中奖。
							<br>※举例：投注者购买佰位小，当期开奖结果如为20352（3为小），则视为中奖。
							<table width="100%">
								<tbody>
									<tr>
										<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">大</th>
										<th width="50%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">小</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">5、6、7、8、9</td>
										<td bgcolor="#FFF7F0" class="point" align="center">
											0、1、2、3、4</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							开奖结果万位、仟位、佰位、拾位或个位数字为0、4、6、8、9时为“合”，若为1、2、3、5、7时为“质”，当投注位数大小与开奖结果的位数大小相符时，即为中奖。
							<br>※举例：投注者购买佰位质，当期开奖结果如为20352（3为质），则视为中奖。
							<table width="100%">
								<tbody>
									<tr>
										<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">质</th>
										<th width="50%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">合</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">1、2、3、5、7</td>
										<td bgcolor="#FFF7F0" class="point" align="center">
											0、4、6、8、9</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>总和大、总和小、总和单、总和双
							都是跟相邻的下一位数进行和计算，个位将与万位进行和计算。总和＝9是，为和局，退还投注金额 <br>※举例：投注者购买佰位总和小，当期开奖结果如为20352（佰位3+十位5
							＝8为总和小），则视为中奖。
							<table width="100%">
								<tbody>
									<tr>
										<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">总和大</th>
										<th width="50%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">总和小</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">10、11、12、13、14、15、16、17、18</td>
										<td bgcolor="#FFF7F0" class="point" align="center">
											0、1、2、3、4、5、6、7、8</td>
									</tr>
								</tbody>
							</table> <br>
						<table width="100%">
								<tbody>
									<tr>
										<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">总和单</th>
										<th width="50%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">总和双</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">1、3、5、7、9、11、13、15、17</td>
										<td bgcolor="#FFF7F0" class="point" align="center">
											0、2、4、6、8、10、12、14、16、18</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>龙、虎、和 都是跟相邻的下一位数进行比较，个位将与万位进行比较。<br>
							<table width="100%">
								<tbody>
									<tr>
										<th width="30%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">玩法</th>
										<th width="23%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">龙</th>
										<th width="23%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">虎</th>
										<th width="23%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">和</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">万位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">大于仟位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">小于仟位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">等于仟位</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">仟位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">大于佰位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">小于佰位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">等于佰位</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">佰位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">大于拾位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">小于拾位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">等于拾位</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">拾位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">大于个位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">小于个位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">等于个位</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">个位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">大于万位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">小于万位</td>
										<td bgcolor="#FFF7F0" class="point" align="center">等于万位</td>
									</tr>
								</tbody>
							</table> ※举例：投注者购买佰位虎，当期开奖结果如为20352（佰位3小于十位5为虎），则视为中奖。
						</li>
					</ul>
				</dd>
			</dl>
			<%--<h2>棋牌玩法</h2>
			<dl>
				<dt>◎百家乐玩法说明</dt>
				<dd>
					<ul>
						<li><p>百家乐游戏规则：百家乐是以开奖号码为基础，进行庄、闲对比的一种玩法！</p></li>
						<li><p>庄是指以开奖五个号码为基础，选择前两个数（万、千视为庄的前两张牌）。</p></li>
						<li><p>闲是指以开奖五个号码为基础，选择后两个数（十、个视为闲的前两张牌）。</p></li>
						<li><p>如第一轮未分出胜负需要再按牌例发第二轮牌，第三张牌闲先发，最多每方3张牌，谁最接近9点即为胜方，而相同点数即和局。</p></li>
						<li><p>如若投注庄/闲， 开奖结果为和局，那么不计输赢，并且退还本金，且不计水费,例如：00100。</p></li>
						<li>有一个天生赢家或者两个都大等于6(庄闲都不补)</li>
						<li>个位与百位和数的个位数，此张牌为第5张牌。万位与百位和数的个位数，此张牌为第6张牌</li>
						<li>如果闲家补牌，那么第5张牌给闲家，如果闲家不补牌，庄家补牌，那么第5张牌给庄家</li>
						<li>庄闲点数例图：
							<table>
								<tbody>
									<tr>
										<th width="25%" valign="top" bgcolor="#AFAFE4" align="center"
											class="subtitle2">闲2牌合计点数</th>
										<th width="25%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">闲家</th>
										<th width="25%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">庄2牌合计点数</th>
											<th width="25%" bgcolor="#AFAFE4" align="center"
											class="subtitle2">庄家</th>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">0</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">0</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">1</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">1</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">2</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">2</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">3</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">3</td>
										<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为8，庄无须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">4</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">4</td>
										<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为0，1，8，9，庄无须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">5</td>
										<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">5</td>
										<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为0，1，2，3，8，9，庄无须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">6</td>
										<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">6</td>
										<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为6，7，庄无须补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">7</td>
										<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
										<td bgcolor="#FFF7F0" class="point" align="center">7</td>
										<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">8或9</td>
										<td bgcolor="#FFF7F0" class="point" align="center">天生赢家，不须补牌</td>
										<td valign="top" bgcolor="#FFF7F0" class="point"
											align="center">8或9</td>
										<td bgcolor="#FFF7F0" class="point" align="center">天生赢家，不须补牌</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>大小/单双/质合，以庄闲的点数来进行判断结果。 0~4为小，5~9为大;
							1、3、5、7、9为单，0、2、4、6、8为双; 1、2、3、5、7为质数，0、4、6、8、9为合数。</li>
						<li>庄对/闲对 是指两家首发(第一轮)的两张牌为对子,不含第三张。如：开奖55842，投庄对则中奖，投闲对不中奖</li>
					</ul>
				</dd>
				<dt>◎德州扑克玩法说明</dt>
				<dd>
					<ul>
						<li>德州扑克是以开奖五个号码为基准，按德州扑克牌面组合进行投注的一种玩法！组合大小的押注点举例如下：</li>
						<li>豹子：开出的五个号码都相同，如22222、33333。</li>
						<li>四张：即五个开奖号码中有四个为一样，如00001、77797。</li>
						<li>葫芦：即五个开奖号码中三个相同（三条）及两个号码相同（一对），如77997、45544。</li>
						<li>顺子：是指开出的五个号码是一串顺序的数字。09213、65743。</li>
						<li>三张：开出的五个号码中三个相同，且余下的两个号码完全不同，如：87477、65455，</li>
						<li>两对：是指开奖五个号码，能组成两个对子，如：97789、01022。</li>
						<li>一对：是指开出的五个号码，能够组成一个对子，如：65877、01322。</li>
						<li>杂牌：是指开出的五个号码全部都不一样，不能够组成任意对子或顺子，(不包含五离) 如：06587、98763。</li>
						<li>五离：是指开出的五个号码不能够组成对子，并且没有任何相邻的两个数。如：28064，19573</li>
						<li>以上投注某个点后，如果开奖的号码结果组合正好与其吻合，按赔率赢得金额，如果开奖号码组合结果不能与投注点吻合，则输！</li>
					</ul>
				</dd>
				<dt>◎牛牛玩法说明</dt>
				<dd>
					<ul>
						<li>牛牛游戏规则：牛牛是开奖结果的五个号码为基准，在开奖的五个数字中若有任意三个数字相加之和为0或10的倍数，其余下另两个数字之和的个位数作为对奖基准。</li>
						<li>如开奖结果为00017即8点（牛8）,02818即9点（牛9），99219、88400则是10点（即牛牛）；</li>
						<li>如开奖结果的五个号码为01234、12859将视为无点（任三个数组合都无法组成0或10的倍数）；</li>
						<li>再如开奖结果的五个号码相同时如：11111、22222、33333（豹子）皆视为无点，但00000则视为牛牛</li>
						<li>当前投注牛3，开奖结果为01912（牛3）时视为中奖，投注其他游戏项皆视为不中奖！</li>
						<li>承以上规则</li>
						<li>当投注于A项“小”，开奖结果为牛1、牛2、牛3、牛4、牛5时，皆视为中奖，如开出无点
							那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
						<li>当投注于A项“大”，开奖结果为牛6、牛7、牛8、牛9、牛10时，皆视为中奖，如开出无点
							那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
						<li>当投注于B项“单”，开奖结果为牛1、牛3、牛5、牛7、牛9时，皆视为中奖，如开出无点
							那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
						<li>当投注于B项“双”，开奖结果为牛2、牛4、牛6、牛8、牛10时，皆视为中奖，如开出无点
							那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
					</ul>
				</dd>
				<dt>◎三公玩法说明</dt>
				<dd>
					<ul>
						<li>三公游戏规则：三公以开奖结果五个数字为基准，将左闲点数与右闲点数进行比对的一种玩法！</li>
						<li>左闲点数：取开奖五个数字的前三位（万、千、百）之和的个位数。</li>
						<li>右闲点数：取开奖五个数字的后三位（百、十、个）之和的个位数。</li>
						<li>如左闲点数为1，右闲点数为7，则投注右闲视为中奖！</li>
						<li>比对的点数中0点为10点视为最大，9点为次大，1点为最小！</li>
						<li>若开奖结果左闲点数与右闲点数相同时，则押左闲或右闲均不计输赢，并且退还本金，且不计水费；</li>
						<li>若投注和局，开奖结果为左闲大或右闲大时，视为不中奖！</li>
						<li>左闲点数/右闲点数的尾数：大小 0~4为小，5~9为大；单双 1、3、5、7、9为单，0、2、4、6、8为双；质合
							1、2、3、5、7为质数，0、4、6、8、9为合数。</li>
					</ul>
				</dd>
				<dt>◎龙虎斗玩法说明</dt>
				<dd>
					<ul>
						<li>龙虎游戏规则：龙虎斗是以开奖结果的五个数字作为基准，取任意位置（万、千、百、拾、个）的数字进行组合大小比对的一种玩法；</li>
						<li>当投注龙/虎时，开奖结果为和局，那么押注龙/虎视为不中奖，且计算有效金额退水；</li>
						<li>当投注"和"时，开奖结果为龙/虎，投注“和”视为不中奖；</li>
						<li>举例：开奖结果为：2,1,3,5,2 万为龙、千为龙虎时：结果
							龙(2）大于虎（1），即为开龙；如万为龙，个为虎时，结果一样大，即为开和局！</li>
					</ul>
				</dd>
			</dl> --%>
		</div>
		<div style="display: none;"
			class="ng-scope JSSB3 HKK3 HBK3 AHK3 SHHK3 HEBK3 GXK3 GZK3 BJK3 JXK3 GSK3 FFK3 WFK3 JPK3 KRK3 JLK3 SFK3 ESK3 TMK3 AMK3">
			<div class="JSSB3OPEN">
				<strong><span class="colorName">江苏骰宝（快3）</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">江苏骰宝（快3）</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								08:50&mdash;22:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">江苏骰宝（快3）</span>具体游戏规则如下︰</strong><br>
				<h2>江苏骰宝（快3）</h2>
			</div>
			<div class="JLK3OPEN">
				<strong><span class="colorName">吉林快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">吉林快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								08:30&mdash;22:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-082</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">吉林快三</span>具体游戏规则如下︰</strong><br>
				<h2>吉林快三</h2>
			</div>
			<div class="AHK3OPEN">
				<strong><span class="colorName">安徽快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">安徽快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:00&mdash;22:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-40</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">安徽快三</span>具体游戏规则如下︰</strong><br>
				<h2>安徽快三</h2>
			</div>
			<div class="HKK3OPEN">
				<strong><span class="colorName">香港快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">香港快三</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							00:20&mdash;00:00(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">01-72</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">安徽快三</span>具体游戏规则如下︰</strong><br>
				<h2>安徽快三</h2>
			</div>
			<div class="AMK3OPEN">
				<strong><span class="colorName">澳门快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
					<tr>
						<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
							align="center">游戏项目</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖时间</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">每日期数</th>
						<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
							align="center">开奖频率</th>
					</tr>
					<tr>
						<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
							align="center">澳门快三</td>
						<td class="point" bgcolor="#FFF7F0" align="center">
							00:20&mdash;00:00(北京时间)</td>
						<td class="point" bgcolor="#FFF7F0" align="center">01-72</td>
						<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
					</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">安徽快三</span>具体游戏规则如下︰</strong><br>
				<h2>安徽快三</h2>
			</div>
			<div class="SHHK3OPEN">
				<strong><span class="colorName">上海快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">上海快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								08:58&mdash;22:18(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">上海快三</span>具体游戏规则如下︰</strong><br>
				<h2>上海快三</h2>
			</div>
			<div class="HBK3OPEN">
				<strong><span class="colorName">湖北快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">湖北快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:10&mdash;22:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-078</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">湖北快三</span>具体游戏规则如下︰</strong><br>
				<h2>湖北快三</h2>
			</div>
			<div class="HEBK3OPEN">
				<strong><span class="colorName">河北快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">河北快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								08:50-22:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">河北快三</span>具体游戏规则如下︰</strong><br>
				<h2>河北快三</h2>
			</div>
			<div class="GXK3OPEN">
				<strong><span class="colorName">广西快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">广西快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:30-22:30(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-40</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">广西快三</span>具体游戏规则如下︰</strong><br>
				<h2>广西快三</h2>
			</div>
			<div class="GZK3OPEN">
				<strong><span class="colorName">贵州快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">贵州快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:30-22:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-39</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">贵州快三</span>具体游戏规则如下︰</strong><br>
				<h2>贵州快三</h2>
			</div>
			<div class="BJK3OPEN">
				<strong><span class="colorName">北京快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">北京快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:22-23:42(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-44</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">北京快三</span>具体游戏规则如下︰</strong><br>
				<h2>北京快三</h2>
			</div>
			<div class="JXK3OPEN">
				<strong><span class="colorName">江西快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">江西快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:15&mdash;22:55(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-42</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">江西快三</span>具体游戏规则如下︰</strong><br>
				<h2>江西快三</h2>
			</div>
			<div class="GSK3OPEN">
				<strong><span class="colorName">甘肃快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">甘肃快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								10:19&mdash;21:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">01-36</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">甘肃快三</span>具体游戏规则如下︰</strong><br>
				<h2>甘肃快三</h2>
			</div>
			<div class="FFK3OPEN">
				<strong><span class="colorName">极速快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">极速快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">0001-1440</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">极速快三</span>具体游戏规则如下︰</strong><br>
				<h2>极速快三</h2>
			</div>
			<div class="WFK3OPEN">
				<strong><span class="colorName">幸运快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">幸运快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">幸运快三</span>具体游戏规则如下︰</strong><br>
				<h2>幸运快三</h2>
			</div>
			<div class="JPK3OPEN">
				<strong><span class="colorName">日本快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">日本快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">日本快三</span>具体游戏规则如下︰</strong><br>
				<h2>日本快三</h2>
			</div>
			<div class="KRK3OPEN">
				<strong><span class="colorName">韩国快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">幸运快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">韩国快三</span>具体游戏规则如下︰</strong><br>
				<h2>韩国快三</h2>
			</div>
			<div class="TMK3OPEN">
				<strong><span class="colorName">三分快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">三分快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">十分快三</span>具体游戏规则如下︰</strong><br>
				<h2>十分快三</h2>
			</div>
			<div class="SFK3OPEN">
				<strong><span class="colorName">十分快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">十分快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-143</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">十分快三</span>具体游戏规则如下︰</strong><br>
				<h2>十分快三</h2>
			</div>
			<div class="ESK3OPEN">
				<strong><span class="colorName">二十分快三</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">二分快三</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9：00-22：00</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-39</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司<span class="colorName">十分快三</span>具体游戏规则如下︰</strong><br>
				<h2>十分快三</h2>
			</div>
			<dl>
			<c:if test="${!k3baoziDrawShow  }">
				<style>
					.hideKsan{
						display:none;
					}
				</style>
			</c:if>
				<dt>◎双面</dt>
				<dd>
					<ul>
						<li>以全部开出的三个号码、加起来的总和来判定。</li>
						<li>大小：三个开奖号码总和值11~17 为大；总和值4~10 为小；<span class="hideKsan">若三个号码相同、则不算中奖。</span></li>
						<li>单双：三个开奖号码总和值5,7,9,11,13,15,17为单；总和值4,6,8,10,12,14,16为双；<span class="hideKsan">若三个号码相同、则不算中奖。</span>
						</li>
					</ul>
				</dd>
				<dt>◎三军</dt>
				<dd>
					<ul>
						<li>三个开奖号码其中一个与所选号码相同时、即为中奖。举例：如开奖号码为1、1、3，则投注三军1或三军3皆视为中奖。 <font
							color="red">备注：不论当局指定点数出现几次，仅派彩一次(不翻倍)。 </font>
						</li>
					</ul>
				</dd>
				<dt>◎围骰/全骰</dt>
				<dd>
					<ul>
						<li>围骰：开奖号码三字同号、且与所选择的围骰组合相符时，即为中奖。</li>
						<li>全骰：全选围骰组合、开奖号码三字同号，即为中奖。</li>
					</ul>
				</dd>
				<dt>◎点数</dt>
				<dd>
					<ul>
						<li>开奖号码总和值为4、5、6、7、8、9、10、11、12、13、14、15、16、17
							时，即为中奖；若开出3、18，则不算中奖。举例：如开奖号码为1、2、3、总和值为6、则投注「6」即为中奖。</li>
					</ul>
				</dd>
				<dt>◎长牌</dt>
				<dd>
					<ul>
						<li>

							任选一长牌组合、当开奖结果任2码与所选组合相同时，即为中奖。举例：如开奖号码为1、2、3、则投注长牌12、长牌23、长牌13皆视为中奖。

						</li>
					</ul>
				</dd>
				<dt>◎短牌</dt>
				<dd>
					<ul>
						<li>

							开奖号码任两字同号、且与所选择的短牌组合相符时，即为中奖。举例：如开奖号码为1、1、3、则投注短牌1、1，即为中奖。</li>

					</ul>

				</dd>
				<dt>◎豹子</dt>
				<dd>
					<ul>
						<li>豹子：开奖的三个骰子点数相同为豹子，选择指定点数的豹子与开奖结果一致则中奖<span class="hideKsan">，其它不中奖</span>。</li>
						<li>如：投注豹子：666，开奖：666 则中奖<span class="hideKsan">，其它不中奖</span>。</li>

					</ul>

				</dd>
			</dl>
			<script>
				console.log('${k3baoziDrawShow  }')
			</script>
		</div>
		<div style="display: none; margin-left: 2px;" class="ng-scope SFLHC ">
			<div class="LHCOPEN"></div>
			<strong><span class="colorName">十分六合彩</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">十分六合彩</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								全天24小时不间断</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								001-144</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
			<h2>一般规则说明</h2>
			<ul>
				<li>客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
				<li>如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
				<li>客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
				<li>为了避免出现争议，请务必在下注之后检查“ 下注状况 ”。</li>
				<li>开奖后接受的投注，将被视为无效。</li>
				<li>任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
				<li>公布赔率时出现的任何打字错误或非故意人为失误，本公司将保留改正错误和按正确赔率结算注单的权力。</li>
				<li>公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
				<li>如本公司察觉客户投注状况异常时，有权即时中止客户投注；在本公司中止下注前，客户之所有投注仍属有效，不得要求取消或延迟交收，以及不得有任何异议。</li>
				<li>如因在本网站投注触犯当地法律，本公司概不负责。</li>
				<li>『六合彩』当次确认下注时间与上次确认下注时间10秒钟内：出现同群组且内容相同2笔以上之注单，本公司只承认一笔有效注单，其余一律注销。<br>
					(举例1：十秒内出现快速下注特别号01,02,03同群组同内容的注单两组以上,本公司只保留一组有效,其余将注销)<br>
					(举例2：十秒内出现快速下注特别号01,02,03 和快速下注特别号01,02,因群组号码不同,则认定两组皆为有效注单)
				</li>
			</ul>

			<h2>六合彩规则说明</h2>

			<ul>
				<li>以下所有投注皆含本金。</li>
				<li><strong>特别号码盘势规则说明</strong><br> 当期开出的最后一码为特别号或特码。<br>
					特码&#12288;单&#12288;码：假设投注号码为开奖号码之特别号，视为中奖，其余情形视为不中奖。<br>
					特码&#12288;大&#12288;小：开出之特码大于或等于25为特码大， 小于或等于24为特码小，开出49为和局。<br>
					特码&#12288;单&#12288;双：特码为双数叫特双，如8、16；特码为单数叫特单，如21、35，开出49为和局。<br>
					特码和数大小：以特码个位和十位数字之和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为和局。<br>
					特码和数单双：以特码个位和十位数字之和来判断单双，如01，12，32为和单，02，11，33为和双，开出49号为和局。<br>
					特码尾数大小：以特别号尾数若0尾~4尾为小、5尾~9尾为大；如01、32、44为特尾小；如05、18、19为特尾大，开出49号为和局<br>
					特码&#12288;半&#12288;特：以特别号大小与特别号单双游戏为一个投注组合；当期特别号开出符合投注组合，即视为中奖；若当期特码开出49号，则视和局；其余情形视为不中奖
				</li>
				<li><strong>两面：</strong>指大、小；单、双。</li>
				<li>每期六合彩开奖球数共七粒(包括特码)。</li>
				<li><strong>正码：</strong>当期开出之前6个号码叫正码。每一个号码为一投注组合，假如投注号码为开奖号码之
					正码，视为中奖，其馀情形视为不中奖。</li>
				<li><strong>正码1-6盘势规则说明</strong><br>
					当期开出之前6个号码叫正码。第一时间出来的叫正码1，依次为正码2、正码3…… 正码6(并不以号码大小排序)。<br>
					大&#12288;&#12288;小： 以指定出现正码的位置与号码大于或等于25为大，小于或等于24为小，开出49为大。<br>
					单&#12288;&#12288;双： 以指定出现正码的位置与号码为单数或双数下注，开出49为单。<br> 和数大小：
					以指定出现正码的位置与号码个位和十位数字总和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为大。<br>
					和数单双： 以指定出现正码的位置与号码个位和十位数字总和来判断单双，开出49号为单。<br>
					色&#12288;&#12288;波： 以指定出现正码的位置的球色下注，开奖之球色与下注之颜色相同时，视为中奖，其余情形视为不中奖。<br>
					尾数大小：以指定出现正码的位置与号码尾数大小下注，若0尾~4尾为小、5尾~9尾为大。如01、32、44为正尾小；如05、18、19为正尾大，开出49号为大
				</li>
				<li><strong>正码特 ：</strong> 正码特是指
					正1特、正2特、正3特、正4特、正5特、正6特：其下注的正码特号与现场摇珠开出之正码其开奖顺序及开奖号码相同，视为中奖，
					如现场摇珠第一个正码开奖为49号，下注第一个正码特为49则视为中奖，其它号码视为不中奖。</li>
				<li><strong>总和单双 ：</strong>
					所有七个开奖号码的分数总和是单数叫(总分单)，如分数总和是115、183；分数总和是双数叫(总
					分双)，如分数总和是108、162。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖 。</li>
				<li><strong>总和大小 ：</strong>
					所有七个开奖号码的分数总和大于或等于175为总分大；分数总和小于或等于174为总分小。
					如开奖号码为02、08、17、28、39、46、25，分数总和是165，则总分小。假如投注组合符合
					中奖结果，视为中奖，其馀情形视为不中奖。</li>
				<li><p>
						<strong>连码 ：</strong>
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">四中一：
								</span>所投注的每四个号码为一组合，只要有一个号码是正码，即为中奖，其余情形视为不中奖。</td>
							</tr>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									2·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">三中二：
								</span>所投注的每三个号码为一组合，若其中2个是开奖号码中的正码，即为三中二，视为中奖；其余视为不中奖 。</td>
							</tr>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									</td>
								<td bgcolor="#FFF7F0" width="96%"><span class="style2">三中二之中三：
								</span>所投注的每三个号码为一组合，若三个号码都是开奖号码之正码或特码，视为中奖，其馀情形视为 不中奖。如06、07、08三个都是开奖号码之正码或特码，视为中奖，如两个正码加上一个特别号 码视为中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">3·</td>
								<td bgcolor="#FDFBE3"><span class="style2">四全中：</span>
									所投注的每四个号码为一组合，若四个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖。
									如06、07、08、09四个都是开奖号码之正码，视为中奖，如三个正码加上一个特别号码视为不中奖。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">4·</td>
								<td bgcolor="#FDFBE3"><span class="style2">三全中：</span>
									所投注的每三个号码为一组合，若三个号码都是开奖号码之正码，视为中奖，其馀情形视为
									不中奖。如06、07、08三个都是开奖号码之正码，视为中奖，如两个正码加上一个特别号 码视为不中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFE3CA" align="center">5·</td>
								<td bgcolor="#FFF7F0"><span class="style2">二全中：</span>
									所投注的每二个号码为一组合，二个号码都是开奖号码之正码，视为中奖，其馀情形视为不 中奖（含一个正码加一个特别号码之情形）。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">6·</td>
								<td bgcolor="#FDFBE3"><span class="style2">二中特：</span>
									所投注的每二个号码为一组合，二个号码都是开奖号码之正码，叫二中特之中二；若其中一
									个是正码，一个是特别号码，叫二中特之中特；其馀情形视为不中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFE3CA" align="center">7·</td>
								<td bgcolor="#FFF7F0"><span class="style2">特串：</span>
									所投注的每二个号码为一组合，其中一个是正码，一个是特别号码，视为中奖，其馀情形视为 不中奖（含二个号码都是正码之情形） 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">8.</td>
								<td bgcolor="#FFF7F0">依照二全中·二中特·特串 此3种玩法规则,来进行‘生肖对碰’或‘尾数对碰’<br>
									例一：<br> 选择‘二全中’,之后进行‘生肖对碰’选择龙(2，14，26，38)以及蛇
									(1，13，25，37，49)<br> 则会产生20组号码组合：<br> [2,1] [2,13]
									[2,25] [2,37] [2,49]<br> [14,1] [14,13] [14,25] [14,37]
									[14,49]<br> [26,1] [26,13] [26,25] [26,37] [26,49]<br>
									[38,1] [38,13] [38,25] [38,37] [38,49]<br> 例二：<br>
									选择‘二中特’,之后进行‘尾数对碰’选择0(10，20，30，40)以及5(5，15，25，35，45)<br>
									则会产生20组号码组合：<br> [10,5] [10,15] [10,25] [10,35] [10,45]<br>
									[20,5] [20,15] [20,25] [20,35] [20,45]<br> [30,5] [30,15]
									[30,25] [30,35] [30,45]<br> [40,5] [40,15] [40,25] [40,35]
									[40,45]
								</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">9.</td>
								<td>肖串尾数:选择一主肖，可扥0-9尾的球． 以三全中的肖串尾数为例：
									选择【兔】当主肖(号码03,15,27,39)并且扥9尾数．因为9尾数中的39已在主肖中出现，将不重复组合．
									故兔主肖扥9尾数共可组合出24种组合(二个尾数号码+一个主肖号码的组合)．</td>
							</tr>
						</tbody>
					</table></li>
				<li><p>
						<strong>生肖色波 ：</strong>分为生肖与色波两种玩法
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">生肖：
								</span><br> 生肖顺序为 鼠 &gt;牛 &gt;虎 &gt;兔 &gt;龙 &gt;蛇 &gt;马 &gt;羊
									&gt;猴 &gt;鸡 &gt;狗 &gt;猪<br>
									如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇 01 , 13 ,
									25 , 37 , 49<br> 马 12 , 24 , 36 , 48<br> 羊 11 , 23 ,
									35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 , 21 , 33 ,
									45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 , 31 , 43<br>
									鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 , 41<br> 虎 04
									, 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br> 龙 02 , 14
									, 26 , 38<br> 若当期特别号，落在下注生肖范围内，视为中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">2·</td>
								<td bgcolor="#FDFBE3"><span class="style2">色波：</span>
									以特别号的球色下注，开奖的球色与下注的颜色相同，视为中奖，球色号码分布如下:<br> <span
									class="style1">01 </span> <span class="style8">11 21 </span> <span
									class="style9">31 41</span><br> <span class="style1">02</span>
									<span class="style1">12</span> <span class="style8">22
										32</span> <span class="style9">42</span><br> <span
									class="style9">03</span> <span class="style1">13 23</span> <span
									class="style8">33 43</span><br> <span class="style9">04
										14</span> <span class="style1">24 34</span> <span class="style7">44</span><br>
									<span class="style8">05</span> <span class="style9">15
										25</span> <span class="style1">35 45</span><br> <span
									class="style8">06 16 </span> <span class="style9">26 36</span>
									<span class="style1">46</span><br> <span class="style1">07</span>
									<span class="style8">17 27</span> <span class="style9">37
										47</span><br> <span class="style1">08 18</span> <span
									class="style8">28 38</span> <span class="style9">48</span><br>
									<span class="style9">09</span> <span class="style1">19
										29</span> <span class="style8">39 49 </span><br> <span
									class="style9">10 20</span> <span class="style1">30 40</span></td>
							</tr>
						</tbody>
					</table></li>
				<li><p>
						<strong>一肖 ：</strong>
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">一肖：</span>
									将生肖依序排列，例如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇
									01 , 13 , 25 , 37, 49<br> 马 12 , 24 , 36 , 48<br> 羊
									11 , 23 , 35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 ,
									21 , 33 , 45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 ,
									31 , 43<br> 鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 ,
									41<br> 虎 04 , 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br>
									龙 02 , 14 , 26 , 38<br>
									只要当期号码(所有正码与最后开出的特码)，落在下注生肖范围内，则视为中奖。<span
									style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span>。
								</td>
							</tr>
						</tbody>
					</table></li>
				<li><strong>合肖 ：</strong>
					选1~11生肖(排列如同生肖)为一组合，并选择开奖号码的特码是否在此组合内，若选择是"中"(可选择2~11个生肖)且开奖号码的特码亦在此组合内，即视为中奖；
				</li>
				<li><strong>超快速模式 ：</strong>
					在此模式下所投注的注单，若是赔率变动时，系统将不会有赔率已变动的讯息提示，而会以投注当时的最新赔率接受投注。
					例：使用超快速模式投注特别号，输入金额时，赔率为41.5倍，金额输入的过程中赔率已更新为41倍，金额输入完毕，确认投注时，注单上所显示的赔率将会是以41倍接受投注，当下投注画面上亦会更新为最新赔率。
				</li>
				<li><strong>半波 ：</strong>
					以特码色波和特单，特双，特大，特小为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和局；其馀情形视为不中奖。</li>
				<li><strong>半半波 ：</strong>
					以特码色波和特码单双及特码大小等三种游戏为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和大；其馀情形视为不中奖。</li>
				<li><strong>正肖 ：</strong>依据开出的所有正码为主。若当期6个正码，只要有1个落在下注生肖范围内，视为中奖。多个正码坐在下注生肖内，派彩不变。<span
					style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span></li>
				<li><strong>七色波 ：</strong> 以开出的7个色波，那种颜色最多为中奖。
					开出的6个正码各以1个色波计，特别号以1.5个色波计。而以下3种结果视为和局。<br>
					6个正码开出3蓝3绿，而特别码是1.5红<br> 6个正码开出3蓝3红，而特别码是1.5绿<br>
					6个正码开出3绿3红，而特别码是1.5蓝<br> 如果出现和局，所有投注红，绿，蓝七色波的金额将全数退回，会员也可投注和局。
				</li>
				<li><strong>特码头数 ：</strong>指特别号所属头数的号码<br> "0"头
					：01，02，03，04，05，06，07，08，09<br> "1"头
					：10，11，12，13，14，15，16，17，18，19<br> "2"头
					：20，21，22，23，24，25，26，27，28，29<br> "3"头
					：30，31，32，33，34，35，36，37，38，39<br> "4"头
					：40，41，42，43，44，45，46，47，48，49<br> 例如 ：开奖结果特别号码是21则
					"2"头为中奖，其他头数都不中奖</li>
				<li><strong>特码尾数 ：</strong>指特别号所属尾数的号码<br> "1"尾
					：01，11，21，31，41<br> "2"尾 ：02，12，22，32，42<br> "3"尾
					：03，13，23，33，43<br> "4"尾 ：04，14，24，34，44<br> "5"尾
					：05，15，25，35，45<br> "6"尾 ：06，16，26，36，46<br> "7"尾
					：07，17，27，37，47<br> "8"尾 ：08，18，28，38，48<br> "9"尾
					：09，19，29，39，49<br> "0"尾 ：10，20，30，40<br> 例如
					：开奖结果特别号码是21则 "1"尾为中奖，其他尾数都不中奖</li>
				<li>**正特尾数 ：
					**只要当期号码(所有正码及开出来的特码)，含所属尾数的一个或多个号码，但派彩只派一次，即不论同尾数号码出现一个或多个号码都只派彩一次。<br>
					"1"尾：01.11.21.31.41 "2"尾：02.12.22.32.42 "3"尾：03.13.23.33.43<br>
					"4"尾：04.14.24.34.44 "5"尾：05.15.25.35.45 "6"尾：06.16.26.36.46<br>
					"7"尾：07.17.27.37.47 "8"尾：08.18.28.38.48 "9"尾：09.19.29.39.49<br>
					"0"尾：10.20.30.40<br> 例如 ：开奖结果正码是11、31、42、44、35、32特别号码是 21
					则"1"尾"2"尾"4"尾"5"尾都为中奖，其他尾数都不中奖。
				</li>
				<%--<li>**总肖 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数，与所投注之预计开出之生肖总数和(不用指定特定生肖)，则视为中奖，其余情形视为不中奖。例如：如果'当期号码为19、24、12、34、40、39
					特别号：49，总计六个生肖，若选总肖【6】则为中奖 (请注意：49号亦算输赢，不为和)</li>
				<li>**总肖单双 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数若为单数则为单；若为双数则为双。例如当期号码为19、24、12、34、40、39.特码：49，则总计六个生肖，若下双则中奖。
				</li> --%>
				<li><strong>连肖 ：</strong>
					挑选2~6生肖(排列如同生肖)为一个组合，当期号码(所有正码与最后开出的特码)坐落于投注时所勾选之生肖组合所属号码内，所勾选之生肖皆至少有中一个号码，则视为中奖，其余情视为不中奖<span
					style="color: red">(请注意49亦算输赢，不为和)</span>例如：如果当期号码为19、24、12、34、40、39.特码：49，所勾选三个生肖(称为三肖碰)，若所有生肖的所属号码内至少一个出现于当期号码，则视为中奖。
				</li>
				<li><strong>连尾 ：</strong>
					挑选2〜6个尾数为一组合，当期号码（所有正码与最后出的特码）坐落于投注时所勾选之尾数组合所属号码内，则视为中奖，其余情形视为不中奖<span
					style="color: red">(请注意49亦算输赢，不为和)</span><br>
					举例1：下注2尾碰，勾选0,1，当期七个号码若0尾及1尾皆有开出，视为中奖<br>
					举例2：下注2尾碰，勾选0,1,2三种尾数，会出现三种组合（组合一：0,1）（组合二：0,2）（组合三：1,2），当期七个号码若开出其中一种组合所选的两个尾数，视为中奖。
				</li>
				<li><strong>自选不中奖 ：</strong>
					挑选5~12个号码为一个组合，当期号码(所有正码与最后开出的特码)皆没有坐落于投注时所挑选之号码组合内，则视为中奖，若是有任何一个当期号码开在所挑选的号码组合情形视为不中奖。<br>
					例如当期号码为19,24,17,34,40,39,特别号49，所挑选5个号码(称为五不中)，若所挑选的号码内皆沒有坐落于当期号码，则为中奖
				</li>
			</ul>

		</div>
		<div style="display: none; margin-left: 2px;" class="ng-scope TMLHC ">
			<div class="LHCOPEN"></div>
			<strong><span class="colorName">三分六合彩</span>开奖时间：</strong>
			<table class="awardList">
				<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
						align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
						align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
						align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
						align="center">三分六合彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时不间断</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						001-480</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
				</tr>
				</tbody>
			</table>
			<h2>一般规则说明</h2>
			<ul>
				<li>客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
				<li>如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
				<li>客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
				<li>为了避免出现争议，请务必在下注之后检查“ 下注状况 ”。</li>
				<li>开奖后接受的投注，将被视为无效。</li>
				<li>任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
				<li>公布赔率时出现的任何打字错误或非故意人为失误，本公司将保留改正错误和按正确赔率结算注单的权力。</li>
				<li>公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
				<li>如本公司察觉客户投注状况异常时，有权即时中止客户投注；在本公司中止下注前，客户之所有投注仍属有效，不得要求取消或延迟交收，以及不得有任何异议。</li>
				<li>如因在本网站投注触犯当地法律，本公司概不负责。</li>
				<li>『六合彩』当次确认下注时间与上次确认下注时间10秒钟内：出现同群组且内容相同2笔以上之注单，本公司只承认一笔有效注单，其余一律注销。<br>
					(举例1：十秒内出现快速下注特别号01,02,03同群组同内容的注单两组以上,本公司只保留一组有效,其余将注销)<br>
					(举例2：十秒内出现快速下注特别号01,02,03 和快速下注特别号01,02,因群组号码不同,则认定两组皆为有效注单)
				</li>
			</ul>

			<h2>六合彩规则说明</h2>

			<ul>
				<li>以下所有投注皆含本金。</li>
				<li><strong>特别号码盘势规则说明</strong><br> 当期开出的最后一码为特别号或特码。<br>
					特码&#12288;单&#12288;码：假设投注号码为开奖号码之特别号，视为中奖，其余情形视为不中奖。<br>
					特码&#12288;大&#12288;小：开出之特码大于或等于25为特码大， 小于或等于24为特码小，开出49为和局。<br>
					特码&#12288;单&#12288;双：特码为双数叫特双，如8、16；特码为单数叫特单，如21、35，开出49为和局。<br>
					特码和数大小：以特码个位和十位数字之和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为和局。<br>
					特码和数单双：以特码个位和十位数字之和来判断单双，如01，12，32为和单，02，11，33为和双，开出49号为和局。<br>
					特码尾数大小：以特别号尾数若0尾~4尾为小、5尾~9尾为大；如01、32、44为特尾小；如05、18、19为特尾大，开出49号为和局<br>
					特码&#12288;半&#12288;特：以特别号大小与特别号单双游戏为一个投注组合；当期特别号开出符合投注组合，即视为中奖；若当期特码开出49号，则视和局；其余情形视为不中奖
				</li>
				<li><strong>两面：</strong>指大、小；单、双。</li>
				<li>每期六合彩开奖球数共七粒(包括特码)。</li>
				<li><strong>正码：</strong>当期开出之前6个号码叫正码。每一个号码为一投注组合，假如投注号码为开奖号码之
					正码，视为中奖，其馀情形视为不中奖。</li>
				<li><strong>正码1-6盘势规则说明</strong><br>
					当期开出之前6个号码叫正码。第一时间出来的叫正码1，依次为正码2、正码3…… 正码6(并不以号码大小排序)。<br>
					大&#12288;&#12288;小： 以指定出现正码的位置与号码大于或等于25为大，小于或等于24为小，开出49为大。<br>
					单&#12288;&#12288;双： 以指定出现正码的位置与号码为单数或双数下注，开出49为单。<br> 和数大小：
					以指定出现正码的位置与号码个位和十位数字总和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为大。<br>
					和数单双： 以指定出现正码的位置与号码个位和十位数字总和来判断单双，开出49号为单。<br>
					色&#12288;&#12288;波： 以指定出现正码的位置的球色下注，开奖之球色与下注之颜色相同时，视为中奖，其余情形视为不中奖。<br>
					尾数大小：以指定出现正码的位置与号码尾数大小下注，若0尾~4尾为小、5尾~9尾为大。如01、32、44为正尾小；如05、18、19为正尾大，开出49号为大
				</li>
				<li><strong>正码特 ：</strong> 正码特是指
					正1特、正2特、正3特、正4特、正5特、正6特：其下注的正码特号与现场摇珠开出之正码其开奖顺序及开奖号码相同，视为中奖，
					如现场摇珠第一个正码开奖为49号，下注第一个正码特为49则视为中奖，其它号码视为不中奖。</li>
				<li><strong>总和单双 ：</strong>
					所有七个开奖号码的分数总和是单数叫(总分单)，如分数总和是115、183；分数总和是双数叫(总
					分双)，如分数总和是108、162。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖 。</li>
				<li><strong>总和大小 ：</strong>
					所有七个开奖号码的分数总和大于或等于175为总分大；分数总和小于或等于174为总分小。
					如开奖号码为02、08、17、28、39、46、25，分数总和是165，则总分小。假如投注组合符合
					中奖结果，视为中奖，其馀情形视为不中奖。</li>
				<li><p>
					<strong>连码 ：</strong>
				</p>

					<table>
						<tbody>
						<tr>
							<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
								1·</td>
							<td width="96%" bgcolor="#FFF7F0"><span class="style2">四中一：
								</span>所投注的每四个号码为一组合，只要有一个号码是正码，即为中奖，其余情形视为不中奖。</td>
						</tr>
						<tr>
							<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
								2·</td>
							<td width="96%" bgcolor="#FFF7F0"><span class="style2">三中二：
								</span>所投注的每三个号码为一组合，若其中2个是开奖号码中的正码，即为三中二，视为中奖；其余视为不中奖 。</td>
						</tr>
						<tr>
							<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
							</td>
							<td bgcolor="#FFF7F0" width="96%"><span class="style2">三中二之中三：
								</span>所投注的每三个号码为一组合，若三个号码都是开奖号码之正码或特码，视为中奖，其馀情形视为 不中奖。如06、07、08三个都是开奖号码之正码或特码，视为中奖，如两个正码加上一个特别号 码视为中奖 。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">3·</td>
							<td bgcolor="#FDFBE3"><span class="style2">四全中：</span>
								所投注的每四个号码为一组合，若四个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖。
								如06、07、08、09四个都是开奖号码之正码，视为中奖，如三个正码加上一个特别号码视为不中奖。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">4·</td>
							<td bgcolor="#FDFBE3"><span class="style2">三全中：</span>
								所投注的每三个号码为一组合，若三个号码都是开奖号码之正码，视为中奖，其馀情形视为
								不中奖。如06、07、08三个都是开奖号码之正码，视为中奖，如两个正码加上一个特别号 码视为不中奖 。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FFE3CA" align="center">5·</td>
							<td bgcolor="#FFF7F0"><span class="style2">二全中：</span>
								所投注的每二个号码为一组合，二个号码都是开奖号码之正码，视为中奖，其馀情形视为不 中奖（含一个正码加一个特别号码之情形）。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">6·</td>
							<td bgcolor="#FDFBE3"><span class="style2">二中特：</span>
								所投注的每二个号码为一组合，二个号码都是开奖号码之正码，叫二中特之中二；若其中一
								个是正码，一个是特别号码，叫二中特之中特；其馀情形视为不中奖 。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FFE3CA" align="center">7·</td>
							<td bgcolor="#FFF7F0"><span class="style2">特串：</span>
								所投注的每二个号码为一组合，其中一个是正码，一个是特别号码，视为中奖，其馀情形视为 不中奖（含二个号码都是正码之情形） 。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">8.</td>
							<td bgcolor="#FFF7F0">依照二全中·二中特·特串 此3种玩法规则,来进行‘生肖对碰’或‘尾数对碰’<br>
								例一：<br> 选择‘二全中’,之后进行‘生肖对碰’选择龙(2，14，26，38)以及蛇
								(1，13，25，37，49)<br> 则会产生20组号码组合：<br> [2,1] [2,13]
								[2,25] [2,37] [2,49]<br> [14,1] [14,13] [14,25] [14,37]
								[14,49]<br> [26,1] [26,13] [26,25] [26,37] [26,49]<br>
								[38,1] [38,13] [38,25] [38,37] [38,49]<br> 例二：<br>
								选择‘二中特’,之后进行‘尾数对碰’选择0(10，20，30，40)以及5(5，15，25，35，45)<br>
								则会产生20组号码组合：<br> [10,5] [10,15] [10,25] [10,35] [10,45]<br>
								[20,5] [20,15] [20,25] [20,35] [20,45]<br> [30,5] [30,15]
								[30,25] [30,35] [30,45]<br> [40,5] [40,15] [40,25] [40,35]
								[40,45]
							</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">9.</td>
							<td>肖串尾数:选择一主肖，可扥0-9尾的球． 以三全中的肖串尾数为例：
								选择【兔】当主肖(号码03,15,27,39)并且扥9尾数．因为9尾数中的39已在主肖中出现，将不重复组合．
								故兔主肖扥9尾数共可组合出24种组合(二个尾数号码+一个主肖号码的组合)．</td>
						</tr>
						</tbody>
					</table></li>
				<li><p>
					<strong>生肖色波 ：</strong>分为生肖与色波两种玩法
				</p>

					<table>
						<tbody>
						<tr>
							<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
								1·</td>
							<td width="96%" bgcolor="#FFF7F0"><span class="style2">生肖：
								</span><br> 生肖顺序为 鼠 &gt;牛 &gt;虎 &gt;兔 &gt;龙 &gt;蛇 &gt;马 &gt;羊
								&gt;猴 &gt;鸡 &gt;狗 &gt;猪<br>
								如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇 01 , 13 ,
								25 , 37 , 49<br> 马 12 , 24 , 36 , 48<br> 羊 11 , 23 ,
								35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 , 21 , 33 ,
								45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 , 31 , 43<br>
								鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 , 41<br> 虎 04
								, 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br> 龙 02 , 14
								, 26 , 38<br> 若当期特别号，落在下注生肖范围内，视为中奖 。</td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FCF7CB" align="center">2·</td>
							<td bgcolor="#FDFBE3"><span class="style2">色波：</span>
								以特别号的球色下注，开奖的球色与下注的颜色相同，视为中奖，球色号码分布如下:<br> <span
										class="style1">01 </span> <span class="style8">11 21 </span> <span
										class="style9">31 41</span><br> <span class="style1">02</span>
								<span class="style1">12</span> <span class="style8">22
										32</span> <span class="style9">42</span><br> <span
										class="style9">03</span> <span class="style1">13 23</span> <span
										class="style8">33 43</span><br> <span class="style9">04
										14</span> <span class="style1">24 34</span> <span class="style7">44</span><br>
								<span class="style8">05</span> <span class="style9">15
										25</span> <span class="style1">35 45</span><br> <span
										class="style8">06 16 </span> <span class="style9">26 36</span>
								<span class="style1">46</span><br> <span class="style1">07</span>
								<span class="style8">17 27</span> <span class="style9">37
										47</span><br> <span class="style1">08 18</span> <span
										class="style8">28 38</span> <span class="style9">48</span><br>
								<span class="style9">09</span> <span class="style1">19
										29</span> <span class="style8">39 49 </span><br> <span
										class="style9">10 20</span> <span class="style1">30 40</span></td>
						</tr>
						</tbody>
					</table></li>
				<li><p>
					<strong>一肖 ：</strong>
				</p>

					<table>
						<tbody>
						<tr>
							<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
								1·</td>
							<td width="96%" bgcolor="#FFF7F0"><span class="style2">一肖：</span>
								将生肖依序排列，例如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇
								01 , 13 , 25 , 37, 49<br> 马 12 , 24 , 36 , 48<br> 羊
								11 , 23 , 35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 ,
								21 , 33 , 45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 ,
								31 , 43<br> 鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 ,
								41<br> 虎 04 , 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br>
								龙 02 , 14 , 26 , 38<br>
								只要当期号码(所有正码与最后开出的特码)，落在下注生肖范围内，则视为中奖。<span
										style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span>。
							</td>
						</tr>
						</tbody>
					</table></li>
				<li><strong>合肖 ：</strong>
					选1~11生肖(排列如同生肖)为一组合，并选择开奖号码的特码是否在此组合内，若选择是"中"(可选择2~11个生肖)且开奖号码的特码亦在此组合内，即视为中奖；
				</li>
				<li><strong>超快速模式 ：</strong>
					在此模式下所投注的注单，若是赔率变动时，系统将不会有赔率已变动的讯息提示，而会以投注当时的最新赔率接受投注。
					例：使用超快速模式投注特别号，输入金额时，赔率为41.5倍，金额输入的过程中赔率已更新为41倍，金额输入完毕，确认投注时，注单上所显示的赔率将会是以41倍接受投注，当下投注画面上亦会更新为最新赔率。
				</li>
				<li><strong>半波 ：</strong>
					以特码色波和特单，特双，特大，特小为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和局；其馀情形视为不中奖。</li>
				<li><strong>半半波 ：</strong>
					以特码色波和特码单双及特码大小等三种游戏为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和大；其馀情形视为不中奖。</li>
				<li><strong>正肖 ：</strong>依据开出的所有正码为主。若当期6个正码，只要有1个落在下注生肖范围内，视为中奖。多个正码坐在下注生肖内，派彩不变。<span
						style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span></li>
				<li><strong>七色波 ：</strong> 以开出的7个色波，那种颜色最多为中奖。
					开出的6个正码各以1个色波计，特别号以1.5个色波计。而以下3种结果视为和局。<br>
					6个正码开出3蓝3绿，而特别码是1.5红<br> 6个正码开出3蓝3红，而特别码是1.5绿<br>
					6个正码开出3绿3红，而特别码是1.5蓝<br> 如果出现和局，所有投注红，绿，蓝七色波的金额将全数退回，会员也可投注和局。
				</li>
				<li><strong>特码头数 ：</strong>指特别号所属头数的号码<br> "0"头
					：01，02，03，04，05，06，07，08，09<br> "1"头
					：10，11，12，13，14，15，16，17，18，19<br> "2"头
					：20，21，22，23，24，25，26，27，28，29<br> "3"头
					：30，31，32，33，34，35，36，37，38，39<br> "4"头
					：40，41，42，43，44，45，46，47，48，49<br> 例如 ：开奖结果特别号码是21则
					"2"头为中奖，其他头数都不中奖</li>
				<li><strong>特码尾数 ：</strong>指特别号所属尾数的号码<br> "1"尾
					：01，11，21，31，41<br> "2"尾 ：02，12，22，32，42<br> "3"尾
					：03，13，23，33，43<br> "4"尾 ：04，14，24，34，44<br> "5"尾
					：05，15，25，35，45<br> "6"尾 ：06，16，26，36，46<br> "7"尾
					：07，17，27，37，47<br> "8"尾 ：08，18，28，38，48<br> "9"尾
					：09，19，29，39，49<br> "0"尾 ：10，20，30，40<br> 例如
					：开奖结果特别号码是21则 "1"尾为中奖，其他尾数都不中奖</li>
				<li>**正特尾数 ：
					**只要当期号码(所有正码及开出来的特码)，含所属尾数的一个或多个号码，但派彩只派一次，即不论同尾数号码出现一个或多个号码都只派彩一次。<br>
					"1"尾：01.11.21.31.41 "2"尾：02.12.22.32.42 "3"尾：03.13.23.33.43<br>
					"4"尾：04.14.24.34.44 "5"尾：05.15.25.35.45 "6"尾：06.16.26.36.46<br>
					"7"尾：07.17.27.37.47 "8"尾：08.18.28.38.48 "9"尾：09.19.29.39.49<br>
					"0"尾：10.20.30.40<br> 例如 ：开奖结果正码是11、31、42、44、35、32特别号码是 21
					则"1"尾"2"尾"4"尾"5"尾都为中奖，其他尾数都不中奖。
				</li>
				<%--<li>**总肖 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数，与所投注之预计开出之生肖总数和(不用指定特定生肖)，则视为中奖，其余情形视为不中奖。例如：如果'当期号码为19、24、12、34、40、39
					特别号：49，总计六个生肖，若选总肖【6】则为中奖 (请注意：49号亦算输赢，不为和)</li>
				<li>**总肖单双 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数若为单数则为单；若为双数则为双。例如当期号码为19、24、12、34、40、39.特码：49，则总计六个生肖，若下双则中奖。
				</li> --%>
				<li><strong>连肖 ：</strong>
					挑选2~6生肖(排列如同生肖)为一个组合，当期号码(所有正码与最后开出的特码)坐落于投注时所勾选之生肖组合所属号码内，所勾选之生肖皆至少有中一个号码，则视为中奖，其余情视为不中奖<span
							style="color: red">(请注意49亦算输赢，不为和)</span>例如：如果当期号码为19、24、12、34、40、39.特码：49，所勾选三个生肖(称为三肖碰)，若所有生肖的所属号码内至少一个出现于当期号码，则视为中奖。
				</li>
				<li><strong>连尾 ：</strong>
					挑选2〜6个尾数为一组合，当期号码（所有正码与最后出的特码）坐落于投注时所勾选之尾数组合所属号码内，则视为中奖，其余情形视为不中奖<span
							style="color: red">(请注意49亦算输赢，不为和)</span><br>
					举例1：下注2尾碰，勾选0,1，当期七个号码若0尾及1尾皆有开出，视为中奖<br>
					举例2：下注2尾碰，勾选0,1,2三种尾数，会出现三种组合（组合一：0,1）（组合二：0,2）（组合三：1,2），当期七个号码若开出其中一种组合所选的两个尾数，视为中奖。
				</li>
				<li><strong>自选不中奖 ：</strong>
					挑选5~12个号码为一个组合，当期号码(所有正码与最后开出的特码)皆没有坐落于投注时所挑选之号码组合内，则视为中奖，若是有任何一个当期号码开在所挑选的号码组合情形视为不中奖。<br>
					例如当期号码为19,24,17,34,40,39,特别号49，所挑选5个号码(称为五不中)，若所挑选的号码内皆沒有坐落于当期号码，则为中奖
				</li>
			</ul>

		</div>

		<div style="display: none; margin-left: 2px;"
			class="ng-scope LHC WFLHC AMLHC HKMHLHC">
			<div class="LHCOPEN"></div>
			<h2>一般规则说明</h2>
			<ul>
				<li>客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
				<li>如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
				<li>客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
				<li>为了避免出现争议，请务必在下注之后检查“ 下注状况 ”。</li>
				<li>开奖后接受的投注，将被视为无效。</li>
				<li>任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
				<li>公布赔率时出现的任何打字错误或非故意人为失误，本公司将保留改正错误和按正确赔率结算注单的权力。</li>
				<li>公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
				<li>如本公司察觉客户投注状况异常时，有权即时中止客户投注；在本公司中止下注前，客户之所有投注仍属有效，不得要求取消或延迟交收，以及不得有任何异议。</li>
				<li>如因在本网站投注触犯当地法律，本公司概不负责。</li>
				<li>『六合彩』当次确认下注时间与上次确认下注时间10秒钟内：出现同群组且内容相同2笔以上之注单，本公司只承认一笔有效注单，其余一律注销。<br>
					(举例1：十秒内出现快速下注特别号01,02,03同群组同内容的注单两组以上,本公司只保留一组有效,其余将注销)<br>
					(举例2：十秒内出现快速下注特别号01,02,03 和快速下注特别号01,02,因群组号码不同,则认定两组皆为有效注单)
				</li>
			</ul>

			<h2>六合彩规则说明</h2>

			<ul>
				<li>以下所有投注皆含本金。</li>
				<li><strong>特别号码盘势规则说明</strong><br> 当期开出的最后一码为特别号或特码。<br>
					特码&#12288;单&#12288;码：假设投注号码为开奖号码之特别号，视为中奖，其余情形视为不中奖。<br>
					特码&#12288;大&#12288;小：开出之特码大于或等于25为特码大， 小于或等于24为特码小，开出49为和局。<br>
					特码&#12288;单&#12288;双：特码为双数叫特双，如8、16；特码为单数叫特单，如21、35，开出49为和局。<br>
					特码和数大小：以特码个位和十位数字之和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为和局。<br>
					特码和数单双：以特码个位和十位数字之和来判断单双，如01，12，32为和单，02，11，33为和双，开出49号为和局。<br>
					特码尾数大小：以特别号尾数若0尾~4尾为小、5尾~9尾为大；如01、32、44为特尾小；如05、18、19为特尾大，开出49号为和局<br>
					特码&#12288;半&#12288;特：以特别号大小与特别号单双游戏为一个投注组合；当期特别号开出符合投注组合，即视为中奖；若当期特码开出49号，则视和局；其余情形视为不中奖
				</li>
				<li><strong>两面：</strong>指大、小；单、双。</li>
				<li>每期六合彩开奖球数共七粒(包括特码)。</li>
				<li><strong>正码：</strong>当期开出之前6个号码叫正码。每一个号码为一投注组合，假如投注号码为开奖号码之
					正码，视为中奖，其馀情形视为不中奖。</li>
				<li><strong>正码1-6盘势规则说明</strong><br>
					当期开出之前6个号码叫正码。第一时间出来的叫正码1，依次为正码2、正码3…… 正码6(并不以号码大小排序)。<br>
					大&#12288;&#12288;小： 以指定出现正码的位置与号码大于或等于25为大，小于或等于24为小，开出49为大。<br>
					单&#12288;&#12288;双： 以指定出现正码的位置与号码为单数或双数下注，开出49为单。<br> 和数大小：
					以指定出现正码的位置与号码个位和十位数字总和来判断胜负，和数大于或等于7为大，小于或等于6为小，开出49号为大。<br>
					和数单双： 以指定出现正码的位置与号码个位和十位数字总和来判断单双，开出49号为单。<br>
					色&#12288;&#12288;波： 以指定出现正码的位置的球色下注，开奖之球色与下注之颜色相同时，视为中奖，其余情形视为不中奖。<br>
					尾数大小：以指定出现正码的位置与号码尾数大小下注，若0尾~4尾为小、5尾~9尾为大。如01、32、44为正尾小；如05、18、19为正尾大，开出49号为大
				</li>
				<li><strong>正码特 ：</strong> 正码特是指
					正1特、正2特、正3特、正4特、正5特、正6特：其下注的正码特号与现场摇珠开出之正码其开奖顺序及开奖号码相同，视为中奖，
					如现场摇珠第一个正码开奖为49号，下注第一个正码特为49则视为中奖，其它号码视为不中奖。</li>
				<li><strong>总和单双 ：</strong>
					所有七个开奖号码的分数总和是单数叫(总分单)，如分数总和是115、183；分数总和是双数叫(总
					分双)，如分数总和是108、162。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖 。</li>
				<li><strong>总和大小 ：</strong>
					所有七个开奖号码的分数总和大于或等于175为总分大；分数总和小于或等于174为总分小。
					如开奖号码为02、08、17、28、39、46、25，分数总和是165，则总分小。假如投注组合符合
					中奖结果，视为中奖，其馀情形视为不中奖。</li>
				<li><p>
						<strong>连码 ：</strong>
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">四中一：
								</span>所投注的每四个号码为一组合，只要有一个号码是正码，即为中奖，其余情形视为不中奖。</td>
							</tr>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									2·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">三中二：
								</span>所投注的每三个号码为一组合，若其中2个是开奖号码中的正码，即为三中二，视为中奖；其余视为不中奖 。</td>
							</tr>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									</td>
								<td bgcolor="#FFF7F0" width="96%"><span class="style2">三中二之中三：
								</span>所投注的每三个号码为一组合，若三个号码都是开奖号码之正码或特码，视为中奖，其馀情形视为 不中奖。如06、07、08三个都是开奖号码之正码或特码，视为中奖，如两个正码加上一个特别号 码视为中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">3·</td>
								<td bgcolor="#FDFBE3"><span class="style2">四全中：</span>
									所投注的每四个号码为一组合，若四个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖。
									如06、07、08、09四个都是开奖号码之正码，视为中奖，如三个正码加上一个特别号码视为不中奖。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">4·</td>
								<td bgcolor="#FDFBE3"><span class="style2">三全中：</span>
									所投注的每三个号码为一组合，若三个号码都是开奖号码之正码，视为中奖，其馀情形视为
									不中奖。如06、07、08三个都是开奖号码之正码，视为中奖，如两个正码加上一个特别号 码视为不中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFE3CA" align="center">5·</td>
								<td bgcolor="#FFF7F0"><span class="style2">二全中：</span>
									所投注的每二个号码为一组合，二个号码都是开奖号码之正码，视为中奖，其馀情形视为不 中奖（含一个正码加一个特别号码之情形）。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">6·</td>
								<td bgcolor="#FDFBE3"><span class="style2">二中特：</span>
									所投注的每二个号码为一组合，二个号码都是开奖号码之正码，叫二中特之中二；若其中一
									个是正码，一个是特别号码，叫二中特之中特；其馀情形视为不中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFE3CA" align="center">7·</td>
								<td bgcolor="#FFF7F0"><span class="style2">特串：</span>
									所投注的每二个号码为一组合，其中一个是正码，一个是特别号码，视为中奖，其馀情形视为 不中奖（含二个号码都是正码之情形） 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">8.</td>
								<td bgcolor="#FFF7F0">依照二全中·二中特·特串 此3种玩法规则,来进行‘生肖对碰’或‘尾数对碰’<br>
									例一：<br> 选择‘二全中’,之后进行‘生肖对碰’选择龙(2，14，26，38)以及蛇
									(1，13，25，37，49)<br> 则会产生20组号码组合：<br> [2,1] [2,13]
									[2,25] [2,37] [2,49]<br> [14,1] [14,13] [14,25] [14,37]
									[14,49]<br> [26,1] [26,13] [26,25] [26,37] [26,49]<br>
									[38,1] [38,13] [38,25] [38,37] [38,49]<br> 例二：<br>
									选择‘二中特’,之后进行‘尾数对碰’选择0(10，20，30，40)以及5(5，15，25，35，45)<br>
									则会产生20组号码组合：<br> [10,5] [10,15] [10,25] [10,35] [10,45]<br>
									[20,5] [20,15] [20,25] [20,35] [20,45]<br> [30,5] [30,15]
									[30,25] [30,35] [30,45]<br> [40,5] [40,15] [40,25] [40,35]
									[40,45]
								</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">9.</td>
								<td>肖串尾数:选择一主肖，可扥0-9尾的球． 以三全中的肖串尾数为例：
									选择【兔】当主肖(号码03,15,27,39)并且扥9尾数．因为9尾数中的39已在主肖中出现，将不重复组合．
									故兔主肖扥9尾数共可组合出24种组合(二个尾数号码+一个主肖号码的组合)．</td>
							</tr>
						</tbody>
					</table></li>
				<li><p>
						<strong>生肖色波 ：</strong>分为生肖与色波两种玩法
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">生肖：
								</span><br> 生肖顺序为 鼠 &gt;牛 &gt;虎 &gt;兔 &gt;龙 &gt;蛇 &gt;马 &gt;羊
									&gt;猴 &gt;鸡 &gt;狗 &gt;猪<br>
									如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇 01 , 13 ,
									25 , 37 , 49<br> 马 12 , 24 , 36 , 48<br> 羊 11 , 23 ,
									35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 , 21 , 33 ,
									45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 , 31 , 43<br>
									鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 , 41<br> 虎 04
									, 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br> 龙 02 , 14
									, 26 , 38<br> 若当期特别号，落在下注生肖范围内，视为中奖 。</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FCF7CB" align="center">2·</td>
								<td bgcolor="#FDFBE3"><span class="style2">色波：</span>
									以特别号的球色下注，开奖的球色与下注的颜色相同，视为中奖，球色号码分布如下:<br> <span
									class="style1">01 </span> <span class="style8">11 21 </span> <span
									class="style9">31 41</span><br> <span class="style1">02</span>
									<span class="style1">12</span> <span class="style8">22
										32</span> <span class="style9">42</span><br> <span
									class="style9">03</span> <span class="style1">13 23</span> <span
									class="style8">33 43</span><br> <span class="style9">04
										14</span> <span class="style1">24 34</span> <span class="style7">44</span><br>
									<span class="style8">05</span> <span class="style9">15
										25</span> <span class="style1">35 45</span><br> <span
									class="style8">06 16 </span> <span class="style9">26 36</span>
									<span class="style1">46</span><br> <span class="style1">07</span>
									<span class="style8">17 27</span> <span class="style9">37
										47</span><br> <span class="style1">08 18</span> <span
									class="style8">28 38</span> <span class="style9">48</span><br>
									<span class="style9">09</span> <span class="style1">19
										29</span> <span class="style8">39 49 </span><br> <span
									class="style9">10 20</span> <span class="style1">30 40</span></td>
							</tr>
						</tbody>
					</table></li>
				<li><p>
						<strong>一肖 ：</strong>
					</p>

					<table>
						<tbody>
							<tr>
								<td width="4%" valign="top" bgcolor="#FFE3CA" align="center">
									1·</td>
								<td width="96%" bgcolor="#FFF7F0"><span class="style2">一肖：</span>
									将生肖依序排列，例如今年是蛇年，就以蛇为开始，依顺序将49个号码分为12个生肖(如下)，再以生肖下注。<br> 蛇
									01 , 13 , 25 , 37, 49<br> 马 12 , 24 , 36 , 48<br> 羊
									11 , 23 , 35 , 47<br> 猴 10 , 22 , 34 , 46<br> 鸡 09 ,
									21 , 33 , 45<br> 狗 08 , 20 , 32 , 44<br> 猪 07 , 19 ,
									31 , 43<br> 鼠 06 , 18 , 30 , 42<br> 牛 05 , 17 , 29 ,
									41<br> 虎 04 , 16 , 28 , 40<br> 兔 03 , 15 , 27 , 39<br>
									龙 02 , 14 , 26 , 38<br>
									只要当期号码(所有正码与最后开出的特码)，落在下注生肖范围内，则视为中奖。<span
									style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span>。
								</td>
							</tr>
						</tbody>
					</table></li>
				<li><strong>合肖 ：</strong>
					选1~11生肖(排列如同生肖)为一组合，并选择开奖号码的特码是否在此组合内，若选择是"中"(可选择2~11个生肖)且开奖号码的特码亦在此组合内，即视为中奖；
				</li>
				<li><strong>超快速模式 ：</strong>
					在此模式下所投注的注单，若是赔率变动时，系统将不会有赔率已变动的讯息提示，而会以投注当时的最新赔率接受投注。
					例：使用超快速模式投注特别号，输入金额时，赔率为41.5倍，金额输入的过程中赔率已更新为41倍，金额输入完毕，确认投注时，注单上所显示的赔率将会是以41倍接受投注，当下投注画面上亦会更新为最新赔率。
				</li>
				<li><strong>半波 ：</strong>
					以特码色波和特单，特双，特大，特小为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和局；其馀情形视为不中奖。</li>
				<li><strong>半半波 ：</strong>
					以特码色波和特码单双及特码大小等三种游戏为一个投注组合，当期特码开出符合投注组合，即视为中奖；
					若当期特码开出49号，则视为和大；其馀情形视为不中奖。</li>
				<li><strong>正肖 ：</strong>依据开出的所有正码为主。若当期6个正码，只要有1个落在下注生肖范围内，视为中奖。多个正码坐在下注生肖内，派彩不变。<span
					style="color: red; font-weight: bold">(请注意：49亦算输赢，不为和)</span></li>
				<li><strong>七色波 ：</strong> 以开出的7个色波，那种颜色最多为中奖。
					开出的6个正码各以1个色波计，特别号以1.5个色波计。而以下3种结果视为和局。<br>
					6个正码开出3蓝3绿，而特别码是1.5红<br> 6个正码开出3蓝3红，而特别码是1.5绿<br>
					6个正码开出3绿3红，而特别码是1.5蓝<br> 如果出现和局，所有投注红，绿，蓝七色波的金额将全数退回，会员也可投注和局。
				</li>
				<li><strong>特码头数 ：</strong>指特别号所属头数的号码<br> "0"头
					：01，02，03，04，05，06，07，08，09<br> "1"头
					：10，11，12，13，14，15，16，17，18，19<br> "2"头
					：20，21，22，23，24，25，26，27，28，29<br> "3"头
					：30，31，32，33，34，35，36，37，38，39<br> "4"头
					：40，41，42，43，44，45，46，47，48，49<br> 例如 ：开奖结果特别号码是21则
					"2"头为中奖，其他头数都不中奖</li>
				<li><strong>特码尾数 ：</strong>指特别号所属尾数的号码<br> "1"尾
					：01，11，21，31，41<br> "2"尾 ：02，12，22，32，42<br> "3"尾
					：03，13，23，33，43<br> "4"尾 ：04，14，24，34，44<br> "5"尾
					：05，15，25，35，45<br> "6"尾 ：06，16，26，36，46<br> "7"尾
					：07，17，27，37，47<br> "8"尾 ：08，18，28，38，48<br> "9"尾
					：09，19，29，39，49<br> "0"尾 ：10，20，30，40<br> 例如
					：开奖结果特别号码是21则 "1"尾为中奖，其他尾数都不中奖</li>
				<li>**正特尾数 ：
					**只要当期号码(所有正码及开出来的特码)，含所属尾数的一个或多个号码，但派彩只派一次，即不论同尾数号码出现一个或多个号码都只派彩一次。<br>
					"1"尾：01.11.21.31.41 "2"尾：02.12.22.32.42 "3"尾：03.13.23.33.43<br>
					"4"尾：04.14.24.34.44 "5"尾：05.15.25.35.45 "6"尾：06.16.26.36.46<br>
					"7"尾：07.17.27.37.47 "8"尾：08.18.28.38.48 "9"尾：09.19.29.39.49<br>
					"0"尾：10.20.30.40<br> 例如 ：开奖结果正码是11、31、42、44、35、32特别号码是 21
					则"1"尾"2"尾"4"尾"5"尾都为中奖，其他尾数都不中奖。
				</li>
				<%--<li>**总肖 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数，与所投注之预计开出之生肖总数和(不用指定特定生肖)，则视为中奖，其余情形视为不中奖。例如：如果'当期号码为19、24、12、34、40、39
					特别号：49，总计六个生肖，若选总肖【6】则为中奖 (请注意：49号亦算输赢，不为和)</li>
				<li>**总肖单双 : **
					当期号码(所有正码与最后开出的特码)开出的不同生肖总数若为单数则为单；若为双数则为双。例如当期号码为19、24、12、34、40、39.特码：49，则总计六个生肖，若下双则中奖。
				</li> --%>
				<li><strong>连肖 ：</strong>
					挑选2~6生肖(排列如同生肖)为一个组合，当期号码(所有正码与最后开出的特码)坐落于投注时所勾选之生肖组合所属号码内，所勾选之生肖皆至少有中一个号码，则视为中奖，其余情视为不中奖<span
					style="color: red">(请注意49亦算输赢，不为和)</span>例如：如果当期号码为19、24、12、34、40、39.特码：49，所勾选三个生肖(称为三肖碰)，若所有生肖的所属号码内至少一个出现于当期号码，则视为中奖。
				</li>
				<li><strong>连尾 ：</strong>
					挑选2〜6个尾数为一组合，当期号码（所有正码与最后出的特码）坐落于投注时所勾选之尾数组合所属号码内，则视为中奖，其余情形视为不中奖<span
					style="color: red">(请注意49亦算输赢，不为和)</span><br>
					举例1：下注2尾碰，勾选0,1，当期七个号码若0尾及1尾皆有开出，视为中奖<br>
					举例2：下注2尾碰，勾选0,1,2三种尾数，会出现三种组合（组合一：0,1）（组合二：0,2）（组合三：1,2），当期七个号码若开出其中一种组合所选的两个尾数，视为中奖。
				</li>
				<li><strong>自选不中奖 ：</strong>
					挑选5~12个号码为一个组合，当期号码(所有正码与最后开出的特码)皆没有坐落于投注时所挑选之号码组合内，则视为中奖，若是有任何一个当期号码开在所挑选的号码组合情形视为不中奖。<br>
					例如当期号码为19,24,17,34,40,39,特别号49，所挑选5个号码(称为五不中)，若所挑选的号码内皆沒有坐落于当期号码，则为中奖
				</li>
			</ul>

		</div>
		<div style="display: none;" class="ng-scope HNKLSF GDKLSF CQXYNC SFKLSF WFKLSF">
			<div class="HNKLSFOPEN">
				<strong><span class="colorName">湖南快乐十分</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">湖南快乐十分</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:00-23:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-084</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司湖南快乐十分具体游戏规则如下︰</strong>
			</div>
			<div class="GDKLSFOPEN">
				<strong><span class="colorName">广东快乐十分</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">广东快乐十分</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:00-23:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-084</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br>

				<p>本公司广东快乐十分具体游戏规则如下︰</p>
			</div>
			<div class="CQXYNCOPEN">
				<strong><span class="colorName">重庆幸运农场</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">重庆幸运农场</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								10:00-02:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">001-097</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
						</tr>
					</tbody>
				</table>
				<br>
				<p>本公司重庆幸运农场具体游戏规则如下︰</p>
			</div>
			
			<div class="SFKLSFOPEN">
				<strong><span class="colorName">三分快乐十分</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">三分快乐十分</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:00-23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">480</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
						</tr>
					</tbody>
				</table>
				<br>

				<p>本公司三分快乐十分具体游戏规则如下︰</p>
			</div>
			
			<div class="WFKLSFOPEN">
				<strong><span class="colorName">五分快乐十分</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
								align="center">游戏项目</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖时间</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">每日期数</th>
							<th class="subtitle2" width="25%" bgcolor="#AFAFE4"
								align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">五分快乐十分</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								09:00-23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">288</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br>

				<p>本公司五分快乐十分具体游戏规则如下︰</p>
			</div>
			
			<h2>两面盘玩法</h2>
			<dl>
				<dd>
					<ul>
						<li>大小∶开出之号码大於或等於11为大，小於或等於10为小</li>
						<li>单双∶开出号码为双数叫双，如8、16；号码为单数叫单，如19、5。</li>
						<li>尾大、小∶开出之尾数大於或等於5为尾大，小於或等於4为尾小。</li>
						<li>合单双∶以号码个位和十位数字之和来判断胜负，如01、12、16为合单；02、11、20为合双。</li>
						<li>中发白∶将20个号码分为三种类，分别为中、发、白（如下），再依三种类下注。若当期号码在相对应的球位落在下注三种类范围内，视为中奖。
							<br> 中∶开出之号码为01、02、03、04、05、06、07 <br>
							发∶开出之号码为08、09、10、11、12、13、14 <br> 白∶开出之号码为15、16、17、18、19、20
							<br>
						</li>
						<li>方位∶将20个号码分为四种类（如下），再以四种类下注。若当期号码在相应的球位落在下注四种类范围内，视为中奖。 <br>
							东∶开出之号码为01、05、09、13、17 <br> 南∶开出之号码为02、06、10、14、18<br>
							西∶开出之号码为03、07、11、15、19 <br> 北∶开出之号码为04、08、12、16、20 <br>
						</li>
					</ul>
				</dd>


				<!-- <h2>总和玩法</h2>
				<dd>
					<ul>
						<li>总大小∶所有8个开奖号码的数字总和值85到132为总大；所有8个开奖号码的数字总和值36到83为总分小。开奖号码84为和局。
							假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
						<li>总单双∶所有8个开奖号码的数字总和值是单数为总和单，如数字总和值是31、51；
							所有8个开奖号码的数字总和值是双数为总和双，如数字总和是42 、80；</li>
						<li>总尾大小∶所有8个开奖号码的数字总和数值的个位数大於或等於5为总尾大，小於或等於4为总尾小。</li>
						<li>龙虎∶以第一球的中奖号码和第八球的中奖号码做为对奖号码。 <br>
							龙∶开出之号码第一球的中奖号码大於第八球的中奖号码。如第一球开出14 第八球开出09；第一球开出05
							第八球开出01...中奖为龙。<br> 虎∶开出之号码第一球的中奖号码小於第八球的中奖号码。如第一球开出14
							第八球开出16；第一球开出05 第八球开出08...中奖为虎。<br>
						</li>
					</ul>
				</dd> -->
				<h2>连码玩法</h2>
				<dd>
					<ul>
						<li>选二任选∶指从01至20中任意选择2个号码对开奖号码中任意2个位置的投注。投注号码与开奖号码中任意2个位置的号码相符，即中奖。</li>
						<li>选二连组∶指从01至20中任意选择2个号码对开奖号码中按开奖顺序出现的2个连续位置的投注。
							投注号码与开奖号码中按开奖顺序出现的2个连续位置的号码相符（顺序不限），即中奖。</li>
						<li>选二连直∶指从01至20中任意选择2个号码对开奖号码中按开奖顺序出现的2个连续位置按位相符的投注。
							投注号码与开奖号码中按开奖顺序出现的2个连续位置的号码按位相符，即中奖。</li>
						<li>选三任选∶指从01至20中任意选择3个号码对开奖号码中任意3个位置的投注。投注号码与开奖号码中任意3个位置的号码相符，即中奖。</li>
						<li>选三前组∶指从01至20中任意选择3个号码对开奖号码中按开奖顺序出现的前3个连续位置的投注。
							投注号码与开奖号码中按开奖顺序出现的前3个位置的号码相符（顺序不限），即中奖。</li>
						<li>选三前直∶指从01至20中任意选择3个号码对开奖号码中按开奖顺序出现的前3个连续位置按位相符的投注。投注号码与开奖号码中按开奖顺序出现的前3个位置的号码按位相符，即中奖。</li>
						<li>选四任选∶指从01至20中任意选择4个号码，对开奖号码中任意4个位置的投注。投注号码与开奖号码中任意4个位置的号码相符，即中奖。</li>
						<li>选五任选∶指从01至20中任意选择5个号码，对开奖号码中任意5个位置的投注。投注号码与开奖号码中任意5个位置的号码相符，即中奖。</li>
					</ul>
				</dd>
			</dl>
		</div>

		<div class="ng-scope FC3D PL3 FF3D WF3D" style="display: none;">
			<div class="FC3DOPEN">
				<strong><span class="colorName">福彩3D</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">福彩3D</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								每天21:15(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">1期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每24小时</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司福彩3D具体游戏规则如下︰</strong><br>
			</div>
			<div class="PL3OPEN">
				<strong><span class="colorName">排列3</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">排列3</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								每天20:30(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">1期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每24小时</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司排列3具体游戏规则如下︰</strong><br>
			</div>
			<div class="FF3DOPEN">
				<strong><span class="colorName">极速3D</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">极速3D</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								每天00:00-23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">1440期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司极速3D具体游戏规则如下︰</strong><br>
			</div>
			<div class="WF3DOPEN">
				<strong><span class="colorName">五分3D</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">五分3D</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								每天00:00-23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">288期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司极速3D具体游戏规则如下︰</strong><br>
			</div>
			
			<h2>大小，单双</h2>
			<dd>
				<ul>
					<li>第一球、第二球、第三球：指下注的每一球与开出之号码其开奖顺序及开奖号码相同，视为中奖，如第一球开出号码
						8，下注第一球为 8 者视为中奖，其余情形视为不中奖。</li>
					<li>单双大小，根据相应单项投注第一球 ~ 第三球开出的球号，判断胜负。</li>
					<li>大小：根据相应单项投注的第一球 ~ 第三球开出的球号大于或等于 5 为大，小于或等于 4 为小。</li>
					<li>单双：根据相应单项投注的第一球 ~ 第三球开出的球号为双数则为双，如 2、6；球号为单数则为单，如 1、3。</li>
				</ul>
			</dd>
			<h2>总和</h2>
			<dd>
				<ul>
					<li>大小：根据相应单项投注的第一球 ~ 第三球开出的球号数字总和值大于或等于 14 为总和大，小于或等于 13
						为总和小。</li>
					<li>单双：根据相应单项投注的第一球 ~ 第三球开出的球号数字总和值是双数为总和双，数字总和值是单数为总和单。</li>
				</ul>
			</dd>
			<h2>三连特殊玩法： 豹子 > 顺子 > 对子 > 半顺 > 杂六</h2>
			<dd>
				<ul>
					<li>豹子：开奖号码的百位十位个位数字都相同。如中奖号码为：222、666、888...开奖号码的百位十位个位数字相同，则投注三连豹子者视为中奖，其它视为不中奖。</li>
					<li>顺子：开奖号码的百位十位个位数字都相连，不分顺序（数字9、0、1相连）。如中奖号码为：123、901、321、798...开奖号码的百位十位个位数字相连，则投注三连顺子者视为中奖，其它视为不中奖。</li>
					<li>对子：开奖号码的百位十位个位任意两位数字相同（不包括豹子）。如中奖号码为：001，288、696...开奖号码的百位十位个位有两位数字相同，则投注三连对子者视为中奖，其它视为不中奖。如果开奖号码为三连豹子，则三连对子视为不中奖。</li>
					<li>半顺：开奖号码的百位十位个位任意两位数字相连，不分顺序（不包括顺子、对子）。如中奖号码为：125、540、390、160...开奖号码的百位十位个位有两位数字相连，则投注三连半顺者视为中奖，其它视为不中奖。如果开奖号码为三连顺子、三连对子，则三连半顺视为不中奖。如开奖号码为：123、901、556、233...视为不中奖。</li>
					<li>杂六：不包括豹子、对子、顺子、半顺的所有开奖号码。如开奖号码为：157、268...开奖号码位数之间无关联性，则投注三连杂六者视为中奖，其它视为不中奖。</li>
				</ul>
			</dd>
			<h2>龙虎和特殊玩法： 龙 > 虎 > 和</h2>
			<dd>
				<ul>
					<li>龙：开奖第一球（百位）的号码 大于
						第三球（个位）的号码。如：6X2、7X6、9X8...开奖为龙，投注龙者视为中奖，其它视为不中奖。</li>
					<li>虎：开奖第一球（百位）的号码 小于
						第三球（个位）的号码。如：1X2、4X6、3X8...开奖为虎，投注虎者视为中奖，其它视为不中奖。</li>
					<li>和：开奖第一球（百位）的号码 等于
						第三球（个位）的号码。如：2X2、6X6、8X8...开奖为和，投注和者视为中奖，其它视为不中奖。</li>
				</ul>
			</dd>
			<h2>跨度</h2>
			<dd>
				<ul>
					<li>以开奖三个号码的最大差距(跨度)，作为中奖的依据。会员可以选择 0 ~ 9 的任一跨度。 。</li>
				</ul>
			</dd>
			<h2>独胆</h2>
			<dd>
				<ul>
					<li>会员可以选择 0 ~ 9 的任意一个号码，只要开出的3球中有下注的号码，即为中奖。 。</li>
				</ul>
			</dd>
		</div>
		<div class="ng-scope GD11X5 SD11X5 JX11X5 SH11X5 GX11X5 FF11X5 SF11X5 WF11X5"
			style="display: none;">
			<div class="GD11X5OPEn">
				<strong><span class="colorName">广东11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">广东11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:30--23:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								01--42期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司广东11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="SH11X5OPEN">
				<strong><span class="colorName">上海11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">上海11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:20--00:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								01--45期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司上海11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="GX11X5OPEN">
				<strong><span class="colorName">广西11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">广西11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:20--00:00(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								01--45期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司广西11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="SD11X5OPEN">
				<strong><span class="colorName">山东11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">山东11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:01--23:01(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								01--43期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司山东11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="JX11X5OPEN">
				<strong><span class="colorName">江西11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">江西11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								9:30--23:10(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								01--42期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司江西11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="FF11X5OPEN">
				<strong><span class="colorName">极速11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">极速11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								00:00--23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								1440期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司极速11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="SF11X5OPEN">
				<strong><span class="colorName">三分11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">三分11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								00:00--23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								480期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公司三分11选5具体游戏规则如下︰</strong><br>
			</div>
			<div class="WF11X5OPEN">
				<strong><span class="colorName">五分11选5</span>开奖时间：</strong>
				<table class="awardList">
					<tbody>
						<tr>
							<th class="subtitle2" width="25%" valign="top" align="center">
								游戏项目</th>
							<th class="subtitle2" width="25%" align="center">开奖时间</th>
							<th class="subtitle2" width="25%" align="center">每日期数</th>
							<th class="subtitle2" width="25%" align="center">开奖频率</th>
						</tr>
						<tr>
							<td class="point colorName" valign="middle" bgcolor="#FFF7F0"
								align="center">五分11选5</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								00:00--23:59(北京时间)</td>
							<td class="point" bgcolor="#FFF7F0" align="center">
								288期</td>
							<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
						</tr>
					</tbody>
				</table>
				<br> <strong> 本公五分11选5具体游戏规则如下︰</strong><br>
			</div>
			<h2>大小，单双</h2>
			<dd>
				<ul>
					<li>单码 - 指第一球、第二球、第三球、第四球、第五球出现的顺序与号码为派彩依据。</li>
					<li>单码：如现场滚球第一个开奖号码为10号，投注第一球为10号则视为中奖，其它号码视为不中奖。</li>
					<li>大小：开出的号码大于或等于6为大，小于或等于5为小，开出11为和 (不计算输赢)。</li>
					<li>单双：号码为双数叫双，如2、8；号码为单数叫单，如5、9；开出11为和 (不计算输赢)。</li>
				</ul>
			</dd>
			<h2>总和 - 以全部开出的5个号码，加起来的总和来判定</h2>
			<dd>
				<ul>
					<li>总和大小: 所有开奖号码数字加总值大于30为和大；总和值小于30为和小；若总和值等于30为和 (不计算输赢)。</li>
					<li>总和单双: 所有开奖号码数字加总值为单数叫和单，如11、31；加总值为双数叫和双，如12、42。若总和值等于29为和
						(不计算输赢)。</li>
					<li>总和尾数大小: 所有开奖号码数字加总值的尾数，大于或等于5为尾大，小于或等于4为尾小。</li>
				</ul>
			</dd>
			<h2>龙虎</h2>
			<dd>
				<ul>
					<li>龙：第一球开奖号码大于第五球开奖号码，如第一球开出10，第五球开出7。</li>
					<li>虎：第一球开奖号码小于第五球开奖号码，如第一球开出3，第五球开出7。</li>
				</ul>
			</dd>
			<h2>选号 - 选号玩法是由1~11号中，选出1~5个号码为一投注组合来进行投注。</h2>
			<dd>
				<ul>
					<li>任选一中一: 投注1个号码与当期开奖的5个号码中任1个号码相同，视为中奖。</li>
					<li>任选二中二: 投注2个号码与当期开奖的5个号码中任2个号码相同(顺序不限)，视为中奖。</li>
					<li>任选三中三: 投注3个号码与当期开奖的5个号码中任3个号码相同(顺序不限)，视为中奖。</li>
					<li>任选四中四: 投注4个号码与当期开奖的5个号码中任4个号码相同(顺序不限)，视为中奖。</li>
					<li>任选五中五: 投注5个号码与当期开奖的5个号码中5个号码相同(顺序不限)，视为中奖。</li>
					<li>任选六中五: 投注6个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
					<li>任选七中五: 投注7个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
					<li>任选八中五: 投注8个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
					<li>组选前二: 投注的2个号码与当期顺序开出的5个号码中的前面2个号码相同，视为中奖。</li>
					<li>组选后二: 投注的2个号码与当期顺序开出的5个号码中的后面2个号码相同，视为中奖。</li>
					<li>组选前三: 投注的3个号码与当期顺序开出的5个号码中的前面3个号码相同，视为中奖。</li>
					<li>组选中三: 投注的3个号码与当期顺序开出的5个号码中的中间3个号码相同，视为中奖。</li>
					<li>组选后三: 投注的3个号码与当期顺序开出的5个号码中的后面3个号码相同，视为中奖。</li>
					<li>直选前二: 投注的2个号码与当期顺序开出的5个号码中的前面2个号码相同且顺序一致，视为中奖。</li>
					<li>直选后二: 投注的2个号码与当期顺序开出的5个号码中的后面2个号码相同且顺序一致，视为中奖。</li>
					<li>直选前三: 投注的3个号码与当期顺序开出的5个号码中的前面3个号码相同且顺序一致，视为中奖。</li>
					<li>直选中三: 投注的3个号码与当期顺序开出的5个号码中的中间3个号码相同且顺序一致，视为中奖。</li>
					<li>直选后三: 投注的3个号码与当期顺序开出的5个号码中的后面3个号码相同且顺序一致，视为中奖。</li>
				</ul>
			</dd>
		</div>
	</div>
</section>
<script type="text/javascript"
	src="${base}/common/template/lottery/jimei/js/assets/js/amazeui.min.js"></script>
<script type="text/javascript">
	$(function() {
		var text = $('#lotteryRule option:selected').text();
		var val = $('#lotteryRule option:selected').val();
		$('select').selected();
		$("#lotteryRule").change(function() {
			val = $(this).val();
			text = $('#lotteryRule option:selected').text();
			$(".rule-content").find("." + val).show().siblings().hide();
			$(".rule-content").find("." + val + "OPEN").show().siblings("div").hide();
			$(".rule-content").find("." + val + "OPEN").find('.colorName').text(text)
		})
		$(".rule-content").find("." + val + "OPEN").find('.colorName').text(text)
		var c = "${lotCode}";
		if (c != null) {
			$(".rule-content").find("." + c).show().siblings().hide();
			$(".rule-content").find("." + c + "OPEN").show().siblings("div").hide();
		}
	})
</script>