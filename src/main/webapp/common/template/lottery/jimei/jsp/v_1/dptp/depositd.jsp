<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="${base}/common/template/lottery/jimei/css/c_style.css?v=8374329179" rel="stylesheet">
    <link href="${base}/common/template/lottery/jimei/css/onlinedeposit.css?v=4333120234" rel="stylesheet">
    <link rel="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3">
    <script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
    <script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
    <script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
</head>
<body>
<div class="content">
    <jsp:include page="../../include_v_1/base_head_info.jsp"></jsp:include>
    <div class="membersubnavi">
        <div class="subnavi">
            <span class="blue">充值</span>
            <div class="subnaviarrow"></div>
        </div>
        <div class="subnavi">
            <a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
        </div>
        <div class="subnavi">
            <a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
        </div>
    </div>
    <div class="steps" style="height:40px;">
        <div class="substeps">
            <div class="substepitmgray1">
                <b>01</b> 选择支付模式
            </div>
            <div class="substepitmgray2 substepitmblue2">
                <b>02</b> 填写付款人资料
            </div>
            <div class="substepitmgray2" id="sliderBuzhou">
                <b>03</b> 确认您的转账资料
            </div>
            <div class="substepitmgray2">
                <b>04</b> 完成
            </div>
        </div>
    </div>
    <style>
        .form-control{width:100%;}
        .account-content table td { padding: 5px 0 5px 10px;}
        .table tbody td{border-bottom:1px solid #ffffff;}
        .table .last-row td{border:none;}
        .btn{padding:4px 25px;margin-right:40px;}
    </style>
    <div class="account-content" id="depositTabs">
    </div>
</div>
<script type="text/javascript">
    var baseUrl = "${base}";
    var hostUrl1="${hostUrl1}";
</script>
<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2674"></script>

<script id="inputdata_tpl" type="text/html">
    {{if virtuals && virtuals.length > 0}}
    <form id="onlinePayAFormId">
        <div class="subrow">
            <div class="subcol2">
                {{each virtuals as virtual count}}
                {{if count > 0 && count %4 ==0}}

                {{/if}}
                <div class="banklist marginbtm10" id="{{virtual.iconCss}}" blockChainType="{{virtual.payChannel}}" courseVircoul="{{virtual.tipsShowUrl}}" courseVircoulOpenSrc="{{virtual.tipsRedirectUrl}}" referenceRate="{{virtual.rate}}" referenceRateConsult="1{{virtual.payChannel}}≈{{virtual.rate}}RMB" flabel="{{virtual.frontLabel}}" remarks="{{virtual.payDesc}}">
                    <input name="payId" min="{{virtual.min}}" max="{{virtual.max}}" iconCss="{{virtual.iconCss}}" value="{{virtual.id}}" class="bankradiobtn" type="radio">
                    {{if virtual.icon}}
                    <div class="zhifuImg" title="{{virtual.payName}}"><img src="${base}{{if virtual.icon}}{{virtual.icon}}{{/if}}" title="{{virtual.payName}}" height="47" width="123"></div>
                    {{else}}
                    <span title="{{virtual.payName}}" class="bankicon {{virtual.iconCss}}" style="width:123px;display:table-caption">{{virtual.payName}}</span>
                    {{/if}}
                </div>
                {{/each}}
            </div>
        </div>
        <div class="security-setting hidn" id="dpt_info">
            <table class="table">
                <tbody>
                <tr style="line-height:30px">
                    <td class="align-right-col">会员帐号：</td>
                    <td>${loginMember.account}</td>
                    <td></td>
                    <td rowspan="9" width="50%">
                        <div style="line-height: 28px;padding: 10px;">
                            <b>自动充值用户须知:</b>
                            <br>
                            1. 客服上班时间为：<span style="color: blue;">${kfsb}至${kfxb}</span>;
                            <br>
                            2. 选择充值方式，填写充值个数及其账户信息，扫描二维码充值后，<span style="color: blue;">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即联系客服
                        </div>
                    </td>
                </tr>
                <c:if test="${showPayInfo}"><tr>
                    <td style="text-align: right;">充值方式：</td>
                    <td id="pay_type_name"></td>
                    <td></td>
                </tr>
<%--                    <tr>--%>
<%--                        <td style="text-align: right;">收款姓名：</td>--%>
<%--                        <td id="Bname_info"></td>--%>
<%--                        <td></td>--%>
<%--                    </tr>--%>
<%--                    <tr>--%>
<%--                        <td style="text-align: right;">收款帐号：</td>--%>
<%--                        <td id="Baccount_info"></td>--%>
<%--                        <td></td>--%>
<%--                    </tr>--%>
                    <tr>
                        <td style="text-align: right;">二维码：</td>
                        <td colspan="2"><img src="#" id="qrcode_img_id"style="width: 300px; height: 300px;">
                            <div style="font-size:20px;color:red;margin-top:6px" class="qrcodeDesc">扫码支付后，请填写充值金额，并点击确定充值，否则无法为您上分！</div></td>
                    </tr></c:if>
                <tr>
                    <td class="align-right-col">充值个数：</td>
                    <td style="width:10%;">
                        <input class="form-control" id="amount" type="text">
                    </td>
                    <td id="dan_bi_limit"></td>
                </tr>
                <tr>
                    <td class="align-right-col">链类型：</td>
                    <td style="width:10%;" class="blockChainType">

                    </td>
                </tr>
                <tr>
                    <td class="align-right-col">参考汇率：</td>
                    <td style="width:10%;" class="referenceRateConsult" colspan="2">

                    </td>
                </tr>

                <tr style="line-height:30px">
                    <td class="align-right-col">充值个数(大写)：</td>
                    <td ><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">个</span></td>
                    <td >约等于：<span class="referenceRate"></span>RMB</td>
                </tr>
<%--                <tr id="payType_trId">--%>
<%--                    <td class="align-right-col"><span class="payType">微信或者支付宝帐号</span>：</td>--%>
<%--                    <td style="width:10%;">--%>
<%--                        <input class="form-control" id="bank_cards" type="text">--%>
<%--                    </td>--%>
<%--&lt;%&ndash;                    <td><span class="smalltxt red">请填写正确的<span class="payType">支付宝或者微信帐号</span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>&ndash;%&gt;--%>
<%--                </tr>--%>
                <tr class="last-row">
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <button id="nextBtn" class="btn btn-blue " ><c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if></button>
                    </td>
                </tr>
                </tbody></table>
            <div class="wxts-div">
                <span class="red">&nbsp;贴心提醒：<span class="smalltxt"></span></span>

<%--                <span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>--%>
            </div>
        </div>
    </form>
    {{else}}
    <div style="margin-left:225px;font-weight: bold;"><span class="red">*</span>友情提示：暂时没有配置充值入口，请更换其他方式继续充值</div>
    {{/if}}
</script>
<script type="text/javascript">
    var pays = [],bank_cards_check=true,curPay={};
    $(function(){
        var convertCurrency=function(a) {
            var e, g, c, h, l, m, q, t, r, y, w;
            a = a.toString();
            if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
            a = a.replace(/,/g, "");
            a = a.replace(/^0+/, "");
            if (9.999999999999E10 < Number(a)) return layer.alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
            e = a.split(".");
            1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
            c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
            h = ["", "\u62fe", "\u4f70", "\u4edf"];
            l = ["", "\u4e07", "\u4ebf"];
            m = ["", ""];
            g = "";
            if (0 < Number(a)) {
                for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
                g += ""
            }
            if ("" != e)
                for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
            "" == g && (g = "\u96f6");
            "" == e && (g += "");
            return "" + g
        };
        $.ajax({
            url : "${base}/center/banktrans/deposit/dptinitd.do",
            success : function(result) {
                var html = template('inputdata_tpl', result);
                $("#depositTabs").html(html);
                pays = result.virtuals;
                bindIptData();
            }
        });
        function bindIptData(){
            var $from = $("#onlinePayAFormId");
            $from.on( 'keyup',"#amount",  function(){
                /* 取出输入的金额,并转换成中文 */
                $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
            });
            $('.banklist').click(function(){
                var $it=$(this),flabel=$it.attr("flabel"),id=$it.find("input").val(),remarks=$it.attr("remarks");
                $it.find("input").prop('checked', true);
                if(!id)return false;
                //清空充值金额
                $( "#amount_CH" ).text('零')
                $('.referenceRate').text(0)
                //链类型
                $('.blockChainType').text($it.attr('blockChainType',))
                //参考汇率
                $('.referenceRateConsult').text($it.attr('referenceRateConsult'))
                //约等于
                $("#amount").bind("input propertychange",function(event){
                    let number=$it.attr('referenceRate')
                    let oneVirtual = $( "#amount" ).val()
                    let allVirtual = parseFloat(Number(number) * 1000) * parseFloat(Number(oneVirtual) * 1000) / 1000000
                    $('.referenceRate').text(allVirtual)

                })
                if(remarks){
                    $(".smalltxt").html(remarks)
                } else {
                    $(".smalltxt").html('请填写正确的<span class="payType">支付宝或者微信帐号</span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）')
                }
                if(flabel){
                    $("#payType_trId").show();
                    $('.payType').html(flabel);
                    bank_cards_check=true;
                }else{
                    $("#payType_trId").hide();
                    bank_cards_check=false;
                }
                $it.addClass("curSubrows").siblings().removeClass("curSubrows");
                $("#dpt_info").removeClass("hidn");
                for (var i = 0; i < pays.length; i++) {
                    if(pays[i].id == id){
                        curPay = pays[i];
                        break;
                    }
                }
                <c:if test="${showPayInfo}">
                $("#pay_type_name").html(curPay.payName);
                $("#qrcode_img_id").attr("src",curPay.qrCodeImg);
                $('#Bname_info').html(curPay.payUserName);
                // $('#Baccount_info').html(curPay.payAccount);
                </c:if>
                $('#dan_bi_limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+curPay.min+'</span> 个 ，最高<span id="max_amount" class="red">'+curPay.max+'</span> 个）');
            });

            $('#nextBtn').click(function() {
                var $it=$(this),eleForm = $("#onlinePayAFormId"),paySetting = eleForm.find("input[name='payId']:checked"),
                    payId = paySetting.val(),$input=eleForm.find("#amount"),
                    amount1 =$input.val(),
                    bank_cards = eleForm.find("#bank_cards").val(),
                    min = paySetting.attr("min"),
                    max = paySetting.attr("max");
                if($it.attr("disabled")){
                    return false;
                }
                $it.attr("disabled","disabled");
                if(!payId){
                    alertfocue("请选择支付方式",$it);return false;
                }
                try{
                    min = parseFloat(min,10);
                }catch(e){min = 0;}
                try{
                    max = parseFloat(max,10);
                }catch(e){max = 0;}
                if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
                    alertfocue("请输入正确个数",$input);
                    return false;
                }
                amount1 = parseFloat(amount1,10);
                if(amount1<min){
                    alertfocue("充值个数必须不小于"+ min,$input);
                    return false;
                }
                if(max>0 && amount1 > max){
                    alertfocue("充值个数必须不大于"+ max,$input);
                    return false;
                }
                // if (bank_cards_check&&(!bank_cards ||bank_cards.length==0)) {
                //     alertfocue("请输入存款帐号",eleForm.find("#bank_cards"));
                //     return false;
                // }
                $.ajax({
                    url : "${base}/center/banktrans/deposit/dptcommitd.do",
                    data : {
                        money : amount1,
                        depositor : bank_cards,
                        payId : payId
                    },
                    method:"POST",
                    success : function(result) {
                        if(!result.success && result.msg){
                            layer.alert(result.msg,{icon:2});
                            $('#nextBtn').removeAttr("disabled");
                            return;
                        }
                        var curPay = {};
                        for (var i = 0; i < pays.length; i++) {
                            if(pays[i].id == payId){
                                curPay = pays[i];
                                break;
                            }
                        }
                        curPay.money = amount1;
                        curPay.orderNo = result;
                        $("#depositTabs").html(template('confirm_tpl',curPay));
                        $("#sliderBuzhou").addClass("substepitmblue2").siblings().removeClass("substepitmblue2");
                        bindCopy(curPay);
                        $('#sub_btn_virtual').click(function () {
                            dptcommit(amount1,payId,curPay.iconCss,curPay.payType);
                        })
                    }
                });

                return false;
            });
        }
        function alertfocue(msg, ele) {
            alert(msg);
            ele.focus();
            $('#nextBtn').removeAttr("disabled");
        }
    });
    function Go(url){
        window.location.href=url;
    }
    function dptcommit(m, payId, iconCss, payType) {

        setInterval(()=>{
            $("#nextBtn").removeAttr("disabled");
        },20000)

        topayVirtual(m, payId, iconCss, payType, function(result){
            if(result.success == false){
                alert(result.msg);
            } else if(result.success == true){
                if(result && result.data && result.data.formParams){
                    var formHiddens = [];
                    for(var key in result.data.formParams){
                        var value = result.data.formParams[key];
                        formHiddens.push({name: key, value: value});
                    }
                    sliderBuZhou();//	切换步骤导航
                    result.data.formHiddens = formHiddens;
                    var html = template('toPayTemplate', result.data);
                    $("#depositTabs").html(html);
                }else{
                    layer.alert("系统发生错误",{icon:2});
                }
            }
        });
    }
    function bindCopy(curPay) {
        $("#Bname").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.payUserName,
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Baccount").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.payAccount,
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Bamount").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.money+"",
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Bamount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Boid").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.orderNo+"",
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
    }
</script>
<script id="confirm_tpl" type="text/html">
    <div class="security-setting" id="dpt_info">
        <table class="table">
            <tbody>
            <tr style="line-height:30px">
                <td class="align-right-col">充值方式：</td>
                <td>{{payName}}</td>
                <td></td>
            </tr>
<%--            <tr>--%>
<%--                <td class="align-right-col">收款姓名：</td>--%>
<%--                <td width="500">{{payUserName}}</td>--%>
<%--                <td style="text-align: left;position: relative;"><button id="Bname" type="button" class="btn btn-red">复&nbsp;制</button>--%>
<%--                    <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">--%>
<%--                    </div>--%>
<%--                </td>--%>
<%--            </tr>--%>
<%--            <tr>--%>
<%--                <td class="align-right-col">收款帐号：</td>--%>
<%--                <td width="500">{{payAccount}}</td>--%>
<%--                <td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="btn btn-red">复&nbsp;制</button>--%>
<%--                    <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">--%>
<%--                    </div>--%>
<%--                </td>--%>
<%--            </tr>--%>
            <tr>
                <td class="align-right-col">订单号：</td>
                <td width="500"><span class="blue">{{orderNo}}</span></td>
                <td style="text-align: left;position: relative;"><button id="Boid" type="button" class="btn btn-red">复&nbsp;制</button>
                    <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-right-col">充值个数：</td>
                <td width="500"><span class="red">{{money}}</span></td>
                <td style="text-align: left;position: relative;"><button id="Bamount" type="button" class="btn btn-red">复&nbsp;制</button>
                    <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
                    </div></td>
            </tr>
            <tr class="last-row">
                <td class="align-right-col">二维码：</td>
                <td><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 300px; height: 300px;"></td>
                <td></td>
            </tr>
            </tbody></table>
    </div>
    <div class="wxts-div">
        <span class="red">&nbsp;贴心提醒：收款信息和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
    </div>
    <div class="wxts-div">
        <button type="button" class="btn btn-blue marginBtn" id="sub_btn_virtual" >下一步</button>
    </div>
</script>
</body></html>