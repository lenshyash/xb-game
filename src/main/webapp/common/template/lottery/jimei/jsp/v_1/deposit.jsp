<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="${base}/common/template/lottery/jimei/css/c_style.css?v=83729179" rel="stylesheet">
	<link href="${base}/common/template/lottery/jimei/css/onlinedeposit.css?v=43803200234" rel="stylesheet">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>
<body>
			<div class="content">
				<jsp:include page="../include_v_1/base_head_info.jsp"></jsp:include>
				<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能在线存款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }"><div class="membersubnavi">
                      		<div class="subnavi">
                            	<span class="blue">充值</span>
                            	<div class="subnaviarrow"></div>
                            </div>
                            <div class="subnavi">
                            	<a href="${base}/center/banktrans/draw/drawpgs.do" target="frame">提款</a>
                            </div>
                            <div class="subnavi">
                            	<a href="${base}/center/record/hisrecord/cPage.do" target="frame">交易历史</a>
                            </div>
               	</div>
            <div class="steps">
			<div class="substeps">
				<div class="substepitmgray1 substepitmblue1">
					<b>01</b> 选择支付模式
				</div>
				<div class="substepitmgray2">
					<b>02</b> 填写付款人资料
				</div>
				<div class="substepitmgray2">
					<b>03</b> 确认您的转账资料
				</div>
				<div class="substepitmgray2">
					<b>04</b> 完成
				</div>
			</div>
			</div>
			<style>
				.incomeMoney{
					position: absolute;
				}
			</style>
			<div class="cunkuan-scy">
				<div id="online_div" class="hidn incomeMoney onOrder1">
					<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpga.do','aType')"><div class="scy-list scy-zxzf" id="zxzf">
							<span class="scy-link-txt">在线支付
								<p id="onlineDesc_div"  style="color: red;">及时自动到账，推荐</p>
							</span>
						</div></a>
					<!--scy-psd登人密码-->
				</div>
				<div id="fast_div" class="hidn incomeMoney onOrder2" style="margin-left: 350px;">
					<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpgb.do','bType');return false"><div class="scy-list scy-wxzf">
							<span class="scy-link-txt">快速入款
								<p id="fastDesc_div"  style="color: red;">支持微信/支付宝二维码扫描</p>
							</span>
						</div></a>
					<!--scy-info银行资料-->
				</div>
				<div id="bank_div" class="hidn incomeMoney onOrder3" style="margin-left: 700px;">
					<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpgc.do','cType');return false"><div class="scy-list scy-yhhk">
							<span class="scy-link-txt">公司入款
								<p id="bankDesc_div"  style="color: red;">支持网银转账，ATM转账，银行柜台汇款</p>
							</span>
						</div></a>
				</div>
				<div id="virtual_div" class="hidn incomeMoney onOrder4" style="margin-left: 1056px;">
					<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpgd.do','dType');return false"><div class="scy-list scy-virtual">
							<span class="scy-link-txt">虚拟币入款
								<p id="virtualDesc_div"  style="color: red;">支持虚拟币入款方式</p>
							</span>
					</div></a>
				</div>
		</div></c:if>
	</div>
			<input type="hidden" value="${isPaymentSort}" id="isPaymentSort">
			<script>
                var seqString = $("#isPaymentSort").val().split(',')
				var seqPx = 0
                for(var i = 0 ;i < seqString.length; i++){
					$(".onOrder"+seqString[i]+"").css('margin-left', seqPx + 'px')
					seqPx = seqPx + 350
				}
			</script>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
	$(function(){
		initOnlineDeposit();
	})

	function initOnlineDeposit(){
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptdata.do",
			success : function(result) {
				if (result.onlineFlag == 'on') {
					$("#online_div").removeClass("hidn");
					$("#onlineDesc_div").html(result.onlineDesc);
				}
				if (result.fastFlag == 'on') {
					$("#fast_div").removeClass("hidn");
					$("#fastDesc_div").html(result.fastDesc);
				}
				if (result.bankFlag == 'on') {
					$("#bank_div").removeClass("hidn");
					$("#bankDesc_div").html(result.bankDesc);
				}
				if (result.virtualFlag == 'on') {
					$("#virtual_div").removeClass("hidn");
					$("#virtualDesc_div").html(result.virtualDesc);
				}
			}
		});
	}

	//var sd = "${folder}";
	function Go(url,type){
		//if(sd != null && sd == "c003" && type=="bType"){
		//	window.open('http://payyycp.com',"otherPay");
		//}else{
			window.location.href=url+"?type="+type;
		//}
	}

</script></c:if>
</body></html>