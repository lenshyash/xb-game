!function(t,i){"function"==typeof define&&define.amd?define(["$"],i):"object"==typeof exports?module.exports=i():t.Loading=i(window.Zepto||window.jQuery||$)}(this,function(t){var i=function(){};return i.prototype={loadingTpl:'<div class="ui-loading"><div class="ui-loading-mask"></div><i></i></div>',stop:function(){t(this.target);this.loading.remove()},start:function(){var i=this,o=i.target,n=t(o),d=this.loading;d||(d=t(i.loadingTpl),t("body").append(d)),this.loading=d;var e=t(n).outerHeight(),h=t(n).outerWidth();"HTML"==t(o)[0].tagName&&(e=Math.max(t(o).height(),t(window).height()),h=Math.max(t(o).width(),t(window).width())),d.height(e).width(h),d.find("div").height(e).width(h),100>e&&d.find("i").height(e).width(e);var a=t(n).offset();d.css({top:a.top,left:a.left});var s=d.find("i"),g=e,l=h,w=0,f=0;"HTML"==t(o)[0].tagName?(g=t(window).height(),l=t(window).width(),w=(g-s.height())/2+t(window).scrollTop(),f=(l-s.width())/2+t(window).scrollLeft()):(w=(g-s.height())/2,f=(l-s.width())/2),s.css({top:w,left:f})},init:function(t){t=t||{},this.loadingTpl=t.loadingTpl||this.loadingTpl,this.target=t.target||"html",this.bindEvent()},bindEvent:function(){var i=this;t(this.target).on("stop",function(){i.stop()})}},i});
layer.config({skin: 'layui-layer-rim',offset:[200]});
var bet_result={};
(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			},
			successIntercept:function(data){}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}
		if (opt.successIntercept) {
			fn.successIntercept = opt.successIntercept;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				clockArea.stop();
		    	clockArea.stopReGetLotOp();
		    	balanceTimer.stop();
				// 错误方法增强处理
				if (statusCode == 404) {
					layer.msg("[" + opt.url + "] 404 not found",{icon: 5});
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
			    	layer.msg("网络异常，"+errorThrown,{icon: 5});
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					layer.msg("后台异常，请联系管理员!",{icon: 5});
					fn.successIntercept(data);
				} else if (ceipstate == 3) { // 业务异常
					layer.msg(data.msg,{icon: 5});
					fn.successIntercept(data);
				} else if (ceipstate == 4 || ceipstate == 5) {// 未登陆异常 或 没有权限
					clockArea.stop();
			    	clockArea.stopReGetLotOp();
			    	balanceTimer.stop();
					layer.alert(data.msg, {closeBtn: 0,icon: 5},function(){
						window.location.href=JIEBAO.base;
					});
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		return _ajax(_opt);
	};
})(jQuery);
$(function(){
	if(!!window.ActiveXObject){// 判断ie版本是否小于10
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(navigator.userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion<10){
        	var nai=$("#not-allow-old-ie").show();
        	nai.find('.ie-version').text(fIEVersion);
        	nai.find('#ie-agent').text(navigator.userAgent);
        	return;
        }
	}
	// 设置默认皮肤
    var a = Cookies.get("_theme"),
    e = Cookies.get("_themeColor"),
    g = Cookies.get("_themeHighlight"),
    c = Cookies.get("_themeHighlightColor");
	a = a?("#"+a):JIEBAO._theme;
	e = e?("#"+e):JIEBAO._themeColor;
	g = g?("#"+g):JIEBAO._themeHighlight;
	c = c?("#"+c):JIEBAO._themeHighlightColor;
	Core.changeColor(a, e, g, c);
    a = $(document).height();
    e = $("#slider_menu").height();
    (a-106) > e && $("#slider_menu").css("height", (a-114));
    $("body").on("click", ".game_select a", function() {// 选择彩种
    	var m =$("#slider_menu"),a=($(document.body).width() - 970)/2;
		if(m.hasClass("hideSide")){
			if(a>211){
				m.css("paddingLeft",a-211);
	    	}else{
	    		m.css("paddingLeft",0);
	    		$(".main_content").animate({"marginLeft":211},"fast");
	    	}
			m.removeClass("hideSide").addClass("showSide").show("fast")
		}else{
			m.removeClass("showSide").addClass("hideSide").hide("fast");
			$(".main_content").animate({"marginLeft":a},"fast");
		}
        return false;
    });
    function digitOnly(e){
    	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    	if (key!=8 && (key < 48 || key > 57)){    
    		layer.tips("请输入整数倍数",$(this),{tips: [1, '#000']});
    		return false;
    	}
    }
    $("#theme").bind('click', function() {// 更换皮肤按钮
    	var it = $(this);
        if (!it.hasClass('shows')) {
        	it.removeClass('shows hides').addClass('shows');
            $('#slider_thememenu').show().animate({'width':210,"display":"block"},"fast");
        } else {
            $(this).removeClass('shows hides').addClass('hides');
            $('#slider_thememenu').animate({'width':0,"display":"none"},"fast");
        }
        return false;
    });
    $("#themeContent li").on("click", function() {// 皮肤选择
        var e, g, c, h,a="background-color";
        e = $(this).find(".color1").css(a);
        h = $(this).find(".color2").css(a);
        g = $(this).find(".color3").css(a);
        c = $(this).find(".color3").attr("txtColor");
        Cookies.set("_theme", Core.rgb2hex(e).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeColor", Core.rgb2hex(h).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeHighlight", Core.rgb2hex(g).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeHighlightColor", Core.rgb2hex(c).replace("#","").toUpperCase(), {expires:7});
        Core.changeColor(e, h, g, c);
        $("#theme").click();
    });
    function switchGame(a){// 切换彩种
    	Core.curQiHao=null;
    	clearAllItem();
		$(".game_list li").removeClass("current");
		a.parent().addClass("current");
		var lotCode=a.attr("lotcode");
		JIEBAO.lotCode=lotCode;
		JIEBAO.lotType=a.attr("lottype");
		JIEBAO.lotName=a.text();
		$("#Bm_general").empty();
		$("#betplay_data").html('<div style="text-align:center;line-height: 264px;font-size: 100px;">加载中...</div>');
		$(".gp_name").text(a.text());
		$(".gp_name").attr("lotcode",JIEBAO.lotCode);
		$(".gp_name").attr("lottype",JIEBAO.lotType);
		$("#nowGP").trigger("click");
		$(".game_select a").click();
		if(lotCode=="FC3D"||lotCode=="PL3"){
			$("#runbets_T").hide();// 排列三和福彩3D不需要追号
		}else{
			$("#runbets_T").show();
		}
		var scsp=$("#saiCheShiPin").hide();// 赛车视频
		if(lotCode=='BJSC' && scsp.attr("status")=="on"){
    		scsp.show();
        }
		$("#betOrderWrap").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    }
    $("body").on("click", ".game_list li a", function() {// 切换彩种事件
        var a = $(this);
        if(!a.attr("lotcode"))return;
        if($("#BetNumberListContanior table").find("tr").length>0){// 如果当前下注列表中有下注记录，则提示
        	layer.confirm('投注項中有您所添加的注單項目，你确定取消注单，切换彩類吗？', {
        		btn: ['确定','取消'] // 按钮
    		}, function(index){
    			switchGame(a);
    			layer.close(index);
    			return true
    		});
        }else{
        	switchGame(a);
        } 
    });
    $(window).resize(function() {// 浏览器窗口大小改变事件
        $("#slider_menu").hasClass("showSide") && $(".game_select a").click();
        $("#slider_thememenu").css("width", 0).hide();
        $("a#theme").removeClass("shows hides").addClass("hides");
        $(".main_content").css({"marginLeft":"auto"});
    });
    
    // 玩法大类点击事件
    $("#Bm_general").on("click","a[p]",function(){
    	var $it = $(this),p=$it.attr("p");
    	$("#Bm_general").find("li").removeClass("current");
    	$it.parent().addClass("current");
    	Core.gamePlay.switchGroup(p);
    });
    // 彩票订单点击事件
    var lotBtns=$("#lotteryBtns"),$nowGP=$("#nowGP");
    lotBtns.on("click","a",function(){
    	lotBtns.find("a").removeClass("current");
    	$(this).addClass("current");
    	$nowGP.removeClass("current");
    });
    
    $nowGP.click(function(){// 选择彩种后面的彩种名称点击事件
    	$('#ocontainer').hide();
		$('#betContainer').show();
		lotBtns.find("a").removeClass("current");
		$nowGP.addClass("current");
		Core.getLotOp();
		Core.getPlayType();
    }).click();
    
    var $betPlayData=$("#betplay_data");
   
    $betPlayData.on("click","input[name='playId']",function(){ // 玩法小类点击事件
    	var $it = $(this),c=$it.attr("c"),t=$it.attr("t");
    	Core.gamePlay.switchPlay("play_"+t+"_"+c);
    }).on("click","input[name='data_miss']",function(){ // 冷热遗漏点击事件
    	var $it = $(this),v=$it.val(),ii=$betPlayData.find(".num_btn i"),ss=$betPlayData.find(".num_btn s");
    	if("miss"==v){
    		ii.show();
    		ss.hide();
    	}else{
    		ss.show();
    		ii.hide();
    	}
    }).on("mouseover",".row_con_c1_btm a",function(){// 玩法小类问好和示例hover事件
    	var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#000'],time:0});
    	}
    }).on("mouseout",".row_con_c1_btm a",function(){
    	layer.closeAll("tips");
    }).on("click","a[num]",function(){// 号码点击事件
		var it =$(this);
		if(!it.hasClass("selected")){
			it.addClass("selected");
		}else{
			it.removeClass("selected");
		}
		Core.betting.sumbetCount(it);
	}).on("click","a[k]",function(){// 全大小单双清点击事件
		var it =$(this),nums=it.parent().parent().find("a[num]"),k=parseInt(it.attr("k"));
		switch(k){
		case 1:// 全
			nums.addClass("selected");break;
		case 2:// 大
			var l = parseInt(nums.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n>=l)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 3:// 小
			var l = parseInt(nums.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n<l)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 4:// 单
			nums.each(function(i){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n%2==1)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 5:// 双
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n%2==0)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 6:// 清
			nums.removeClass("selected");
			break;
		}
		Core.betting.sumbetCount();
	}).on("change","input:checkbox",function(){
		var it=$(this).parent().parent(),
			a=it.find("input:checkbox:checked").length,
			b=parseInt(it.attr("wei")||0);
		$("#positioncount").text(a);
		var combine=function(b,d){
	    	var c=function(a){
	    		for(var b=1,c=2;c<=a;c++)
	    			b*=c;return b
	    		};
	    	return d>b?0:d==b?1:c(b)/(c(d)*c(b-d))
	    };
		$("#positioninfo").text(combine(a,b));
		Core.betting.sumbetCount();
	});
    
    // 近期开奖记录
    $("#lotHistory").hover(function(){
    	var lis = $("#lotHistory").find("li");
    	if(lis.length){
    		var a = lis.length * (lis.eq(0).outerHeight()+1);
    		$(this).css("height",a+"px");
    	}
    },function(){
    	$(this).css("height","104px");
    });
    // 倍数修改事件
    $("#buymultiple").on('keyup blur', function() {
    	var it = $(this),v=it.val();
    	if(v){
	    	if(!/^[\d]{1,8}$/.test(v) || v>10000000){
	    		layer.tips("倍数必须大于0，小于10,000,000",it,{tips: [1, '#000']});
	    		it.val(1);
	    		return false;
	    	}
    	}
    	if (bet_result.hasOwnProperty('betCount')) {
            var r = Core.__FloatMul(bet_result.betCount, v)*Core.price;
            r = Core.__FloatDiv(r, $("select#selectdollarunit").val());
            $("#allTotamounts").text(r);
        }
    }).on("keypress",digitOnly);
    // 模式修改元角分
    $("#selectdollarunit").on('change', function() {
        if (bet_result.hasOwnProperty('betCount')) {
            var r = Core.__FloatMul(bet_result.betCount, $("#buymultiple").val())*Core.price;
            r = Core.__FloatDiv(r, $(this).val());
            $("#allTotamounts").text(r);
        }
    });
    //单式文本输入框
    $betPlayData.on('keyup blur',".no_input_box textarea", function(evt) {
    	var it=$(this),val=it.val(),pattern=new RegExp(it.attr("_pattern")),type=it.attr("_type"),desc=it.attr("_desc")||"";
    	if(val){
    		var vs=val.split(/，|;|\r|\n| /);
        	for(var i =0;i<vs.length;i++){
        		if(vs[i] && !pattern.test(vs[i])){
        			if(evt.type=="blue"||evt.type=="focusout")layer.msg(vs[i]+"格式不对。"+desc);
        			Core.betting.sumTextareaBetCount(type,null);
        			return;
        		}
        	}
        	Core.betting.sumTextareaBetCount(type,vs);
    	}
    });
  // “添加”按钮的点击事件，将投注记录添加到列表中
    $("#seleall").click(function(){
        if (!bet_result.hasOwnProperty('betCount')) return false;
        if (bet_result.betCount <= 0) return false;;
        if ($betList.find("tr").length > Core.maxItem) return false;
        var allTotamounts = parseFloat($("#allTotamounts").text());// 投注金额
        if(allTotamounts && allTotamounts>0){
        	if(JIEBAO.maxBetMoney>0 && allTotamounts>JIEBAO.maxBetMoney){
        		layer.msg('温馨提示：下注金额必须小于'+JIEBAO.maxBetMoney,{icon: 5});
        		return false;
        	}
        	if(JIEBAO.minBetMoney>0 && allTotamounts<JIEBAO.minBetMoney){
        		layer.msg('温馨提示：下注金额必须大于'+JIEBAO.minBetMoney,{icon: 5});
        		return false;
        	}
        	var bettr = '',y = 0,
	         	ttp = $(".ch_radio td input[type=radio]:checked"),
	         	tth = ttp.parent().parent().prev().text(),
	         	playCode = ttp.attr("c"),
	         	coin = $("#selectdollarunit").val(),
	         	beiShu=$("#buymultiple").val(),
	         	rate=$("#gcrate").text();
            if (bet_result.errorTagStr != '') {
            	layer.msg('温馨提示： 以下号码有误，已进行自动过滤。'+bet_result.errorTagStr,{icon: 5});
            }
            var hm=(bet_result.hasOwnProperty('optional')&&bet_result.optional)?(bet_result.optional+"@"+bet_result.myarray):bet_result.myarray;
            // 彩票code｜玩法id｜货币单位（1=元，10=角，100=分）｜投注倍数|投注号码
            var buyinfo = JIEBAO.lotCode + '|' + playCode + '|' + coin + '|'+beiShu + '|' + hm;
            $betList.find("tr").each(function() {
                if ($(this).attr('buyinfo').toString() === buyinfo) {
                    y++;
                }
            });
            if (y) {
                layer.msg('此投注号码已存在，请重新选择!!',{icon: 5});
                return false;
            }
            bettr += '<tr buyinfo="' + buyinfo + '" c="'+ttp.attr("c")+'" m="'+allTotamounts+'" r="'+rate+'"><td>' + $('#GameTypename').text() + '</td>';
            bettr += '<td><span tips="' + tth + ':' + ttp.parent().text() + '">' + ttp.parent().text() + '</span></td>';
            bettr += '<td><span tips="' + hm + '">' + Core.strLimit(hm, 22) + '</span></td>';
            bettr += '<td>' + bet_result.betCount + '</td>';
        	bettr += '<td>' + rate;
        	if(JIEBAO.fanShui){
        		bettr += '/' + $("#rebateShow").text() ;
        	}
            bettr += '</td><td>' + allTotamounts + '</td>';
            bettr += '<td><a href="javascript:;"><i class="fa fa-times-circle fa-lg c_white3"></i></a></td></tr>';
            $betList.find("table tbody").append(bettr);
            $("#betplay_data a").removeClass('selected');
            Core.betting.resetSumbetCount();
            Core.countTotalBet();
            processBetTraceCore.updateBetData();
        } else {
            layer.msg('投注金额不能为0！',{icon: 5});
        }
    });
    var $betList=$("#BetNumberListContanior");
    $betList.on("mouseover","[tips]",function(){// 投注列表中，有tips属性的显示tips信息
    	var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#000'],skin:""});
    	}
    }).on("mouseout","[tips]",function(){
    	layer.closeAll("tips");
    });
    $betList.on('click', "a", function() {// 单注记录删除功能
    	var e=$(this);
    	layer.confirm('你确定删除该注单？', {
    		btn: ['确定','取消'] // 按钮
		}, function(index){
			e.parent().parent().remove();
	        Core.countTotalBet();
	        processBetTraceCore.updateBetData();
			layer.close(index);
			return true
		});
    });
    function clearAllItem(){
    	$betList.find("table tbody").empty();
        Core.countTotalBet();
        processBetTraceCore.updateBetData();
    }
    $("#clearallitem").on('click', function() {// 删除全部注单
    	if(!$betList.find("tr").length)return false;
    	layer.confirm('你确定清空注单？', {
    		btn: ['确定','取消'] // 按钮
		}, function(index){
			clearAllItem();
			layer.close(index);
			return true
		});
    });
    $("#runbets").on("click",function(){// 追号
    	var trs=$betList.find("tr");
    	if(!trs.length){
    		layer.msg('请先添加一注投注号码！',{icon: 5});
    		return false;
    	}
    	var betTrace = $("#betTrace").show();
    	$("#betOrderWrap").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    	$.ajax({
            type: "get",
            url: JIEBAO.base+"/lottery/getQiHaoForChase.do?lotCode="+JIEBAO.lotCode,
            cache: false,
            dataType: "json",
            beforeSend: function(XMLHttpRequest) {
            	betTrace.find("#betTractLoading").show();
            	betTrace.find("#betTractCon").hide();
            },
            success: function(json) {
            	if(!json.success){
            		betTrace.hide();
            		layer.msg(json.msg,{icon: 5});
            		return;
            	}
            	if(json.qiHaos && json.qiHaos.length>0){
            		processBetTraceCore.initView(json.qiHaos);
                    betTrace.find("#betTractLoading").hide();
                	betTrace.find("#betTractCon").show();
                	$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
            	}else{
            		layer.msg('今日已经没有期号可追号！',{icon: 5});
            	}
            }
        });
        return false;
    });
    $("#periodNumLeader").on("keyup blur",function(){// 追号期数修改事件
    	var it = $(this),v=it.val();
    	if(!/^[\d]{1,2}$/.test(v) || v>30 || v<1){
    		layer.tips("追号期数必须大于0，小于等于30",it,{tips: [1, '#000']});
			it.val(10);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#startBeeNum").on("keyup blur",function(){// 起始倍数修改事件
    	var it = $(this),v=it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v <1){
    		layer.tips("倍数必须为大于0的整数",it,{tips: [1, '#000']});
			it.val(1);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#betTraceData").on("blur", "input[type='text']", function(e) {// 追号列表中的倍数修改事件
        var it=$(this),v = it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v<1){
    		layer.tips("倍数必须为大于0的整数",it,{tips: [1, '#000']});
			it.val(1);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#creatGtjhBtn").on('click', function() {
    	var qiShu = $("#periodNumLeader").val(),beiShu=$("#startBeeNum").val();
    	if(!/^[\d]{1,2}$/.test(qiShu) || qiShu>30 || qiShu<1){
    		layer.tips("追号期数必须大于0，小于等于30",$("#periodNumLeader"),{tips: [1, '#000']});
    		return false;
    	}
    	if(!/^[\d]{1,8}$/.test(beiShu) || beiShu <1){
    		layer.tips("倍数必须为整数",$("#startBeeNum"),{tips: [1, '#000']});
    		return false;
    	}
    	processBetTraceCore.startQiShu=qiShu;
    	processBetTraceCore.startTimes=beiShu;// 起始倍数
        processBetTraceCore.generateBetView();
    });
    $("#betTraceSelectAll").click(function(){
    	if($(this).prop("checked")){
    		$("#betTraceData").find("input:checkbox").prop("checked",true);
    	}else{
    		$("#betTraceData").find("input:checkbox").prop("checked",false);
    	}
    	processBetTraceCore.calcView();
    });
    // 下注情况
    $("#nowbetinfo").on("click",function nowbetinfo(){
    	$("#betOrderWrap").show();
    	$("#betTrace").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    	var lotName=$("#nowGP").text().trim();
    	$("#betName").text(lotName);
    	var page = 1;
    	getBcLotteryOrder(page);
		$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
    });
    $("#main_wrap").on("click",".BetInf", function(){
    	var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode");
    	
    	$.ajax({
    		url:JIEBAO.base+'/lotteryBet/getBcLotteryOrderDetail.do',
    		data:{
    			orderId : orderId,
    			lotCode : lotCode,
    			},
    		success:function(r){
    			if(r.winZhuShu == null || r.winZhuShu==''){
    				r.winZhuShu = 0;
    			}
    			if(r.winMoney == null || r.winMoney==''){
    				r.winMoney = 0;
    			}
    			if(r.rollBackMoney == null || r.rollBackMoney==''){
    				r.rollBackMoney = 0;
    			}
    			if(r.lotteryHaoMa == null || r.lotteryHaoMa==''){
    				r.lotteryHaoMa = '- -';
    			}
    			//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
    			var yingkui = 0;
				if(r.status == 1 ){
					r.status='未开奖'
				}else if(r.status == 2 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='已中奖'
				}else if(r.status == 3 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='未中奖'
				}else if(r.status == 4 ){
					r.status='撤单'
				}else if(r.status == 5 ){
					r.status='回滚成功'
				}else if(r.status == 6 ){
					r.status='回滚异常'
				}else if(r.status == 7 ){
					r.status='开奖异常'
				}else if(r.status == 8){
					r.status ='和局';
				}
    			layer.alert(
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"class="message_table" style="font-size: 16px; color: #000;"><tbody>' +
    					'<tr class="bg_black3"><td colspan="4" class="open-title">订单号 ：<span style="color: green">'+r.orderId+'</span></td></tr>'+
    					'<tr><th class="bg_black3">帐号：</th><td>'+r.account+'</td><th class="bg_black3">单注金额：</th><td>'+(2.00/r.model).toFixed(2)+'</td></tr>'+
    					'<tr><th class="bg_black3">下注时间：</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+r.buyZhuShu+'</td></tr>'+
    					'<tr><th class="bg_black3">彩种：</th><td>'+r.lotName+'</td><th class="bg_black3">倍数：</th><td>'+r.multiple+'</td></tr>'+
    					'<tr><th class="bg_black3">期号：</th><td>'+r.qiHao+'</td><th class="bg_black3">投注总额：</th><td>'+r.buyMoney.toFixed(2)+'</td></tr>'+
    					'<tr><th class="bg_black3">玩法：</th><td>'+r.playName+'</td><th class="bg_black3">奖金'+(JIEBAO.fanShui?"/返点":'')+'：</th><td>'+r.minBonusOdds.toFixed(2)+(JIEBAO.fanShui?('/'+(r.rollBackRate||0)+'%'):"")+'</td></tr>'+
    					'<tr><th class="bg_black3">开奖号码：</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖注数：</th><td>'+r.winZhuShu+'</td></tr>'+
    					'<tr><th class="bg_black3">状态：</th><td>'+r.status+'</td><th class="bg_black3">中奖金额：</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'+
    					'<tr>'+(JIEBAO.fanShui?('<th class="bg_black3">销售返点：</th><td>'+r.rollBackMoney.toFixed(2)+'</td>'):'')+'<th class="bg_black3">盈亏：</th><td'+(JIEBAO.fanShui?">":' colspan="3">')+yingkui.toFixed(2)+'</td></tr>'+
    					'<tr><td colspan="4">'+
    					'<textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 100px; border: 1px solid #ccc; font-size: 16px;">'+r.haoMa+'</textarea>'+
    					'</td></tr>'+
    					'</tbody></table>'
    					,{title:"系统提示",area:['700px','auto'],offset : ['5%' ]});
    		}
    	})
    })
    //撤单
    $("#main_wrap").on("click",".cheDan", function(){
    	var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode"),from=it.attr("from");
    	layer.confirm('您确定要撤销该订单？', {
    		  btn: ['确定','取消'] 
    		}, function(){
    			$.ajax({
    	    		url:JIEBAO.base+'/lotteryBet/cancelOrder.do',
    	    		data:{
    	    			orderId : orderId,
    	    			lotCode : lotCode,
    	    			},
    	    		success:function(r){
    	    			layer.msg("撤单成功",{icon: 6});
    	    			switch(from){
    	    			case "ddcsx":if(typeof getOrderManager!="undefined")getOrderManager(1);
    	    			default:getBcLotteryOrder(1);
    	    			}
    	    		}
    	    	})
    		}, function(i){
    			layer.close(i);
    		});
    })
    
    $("#saiCheShiPin").on("click",function(){// 赛车视频
    	$("#betOrderWrap").hide();
    	$("#betTrace").hide();
    	var it=$(this),swf = it.attr("swf");
    	if(swf){
    		var w = $("#saiCheShiPinSwfWrap");
    		if(!it.attr("inited")){console.log(123)
        		it.attr("inited",true);
        		var html = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="940" height="543">'
                    +'<param name="movie" value="'+ swf+'">'
                    +'<param name="quality" value="High">'
                    +'<param name="_cx" value="12383">'
                    +'<param name="_cy" value="1588">'
                    +'<param name="FlashVars" value="">'
                    +'<param name="Src" ref="" value="'+ swf+'">'
                    +'<param name="WMode" value="Window">'
                    +'<param name="Play" value="-1">'
                    +'<param name="Loop" value="-1">'
                    +'<param name="SAlign" value="">'
                    +'<param name="Menu" value="-1">'
                    +'<param name="Base" value="">'
                    +'<param name="AllowScriptAccess" value="always">'
                    +'<param name="Scale" value="ShowAll">'
                    +'<param name="DeviceFont" value="0">'
                    +'<param name="EmbedMovie" value="0">'
                    +'<param name="BGColor" value="">'
                    +'<param name="SWRemote" value="">'
                    +'<param name="MovieData" value="">'
                    +'<embed src="'+ swf+'" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="940" height="543">'
                    +'</object>';
        		w.find("div").html(html);
        	}
    		w.show();
    		$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
    	}
    });
    $("#gamebuy").click(function(){
    	if(!Core.curQiHao){
    		layer.msg('当前期数为空，不能投注！',{icon: 5});
    		return false;
    	}
    	var trs=$betList.find("tr");
    	if(!trs.length){
    		layer.msg('至少添加一注投注号码！',{icon: 5});
    		return false;
    	}
    	if(trs.length> Core.maxItem){
    		layer.msg('投注号码数不能多于'+Core.maxItem+'！',{icon: 5});
    		return false;
    	}
    	 var tracecount = parseInt($("#lt_trace_count").text() || 0)||0,
    	 	allAmount=0,zhuiHao=(tracecount > 0 && !$('.r5_c2').is(':hidden')),
    	 	nowbalance = parseFloat($("#balance").attr("balance").replace(/,/g,"") || 0)||0;
         if (zhuiHao) {
        	 allAmount=(parseFloat($("#lt_trace_hmoney").text())||0);
             if (nowbalance < allAmount) {
            	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
                 return false;
             }
         } else {
        	 allAmount=(parseFloat($("#s_allTotamounts").text())||0);
             if (nowbalance < allAmount) {
            	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
                 return false;
             }
         }
         var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
         $.getJSON(JIEBAO.base+"/lotteryBet/token.do",function(data){
        	 if(data.success==0){
        		 layer.msg(data.msg,{icon: 5});
                 return false;
        	 }
        	 layer.close(layerIndex);
        	 var msg = '',myBuyArray = [],dtitle='系统提示',qiHao=Core.curQiHao,
        	 	postData={lotCode:JIEBAO.lotCode,token:data.token,qiHao:qiHao};
        	 $betList.find("tr").each(function(i, j) {
        		 myBuyArray.push($(this).attr('buyinfo'));
	         });
        	 postData.bets = myBuyArray;
        	 if (tracecount > 0 && !$('.r5_c2').is(':hidden')) {// 追号数据
        		 myBuyArray=[];
        		 myBuyArray.push($("#lt_trace_stop").prop("checked") ? 1 : 0);// 中奖是否停止追号
                 var $betTr = $("#betTraceData tr");
                 $betTr.each(function(i, j) {
                	 j=$(j);
                     if (j.find("input[type='checkbox']").prop("checked")) {
                    	 j=j.find("input[qh]");
                    	 myBuyArray.push(j.attr("qh")+"|"+j.val());
                     }
                 });
                 dtitle = '确定要追号 ' + tracecount + ' 期 ？';
                 msg += '<p>彩票种类 ： ' + $('#GameTypename').text() + '</p>';
                 msg += '<p>追号总期数 ： ' + tracecount + '</p>';
                 msg += '<p>追号总注数 ： ' + (parseInt($("#s_allTotbets").text()) * tracecount) + ' ( 注 )</p>';
                 msg += '<p>追号总金额 ： ' + allAmount + ' ( 元 )</p>';
                 postData.tractData=myBuyArray;
             } else {
                 msg += '<p>彩票种类 ： ' + $('#GameTypename').text() + '</p>';
                 msg += '<p>下注期号 ： 第 ' + qiHao + ' 期</p>';
                 msg += '<p>下注注数 ： ' + $('#s_allTotbets').text() + ' ( 注 )</p>';
                 msg += '<p>下注总额 ： ' + allAmount + ' ( 元 )</p>';
             }
        	 layer.confirm(msg, {title:dtitle,area:["400px"]}, function(){
        		 layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
// layer.msg('努力中', {icon: 1,offset:350},function(){layer.close(layerIndex);});
        		 $.ajax({url:JIEBAO.base+"/lotteryBet/doBets.do",
        			 type: 'POST',
                     dataType: 'json',
                     data: {data:JSON.stringify(postData)},
                     success: function(data) {
                    	 layer.close(layerIndex);
                    	 if(!data.success){
                    		 layer.msg(data.msg,{icon: 5});
                             return false;
                    	 }
                    	 layer.msg("下单成功",{icon: 6});
                    	 clearAllItem();
                         $("#betTrace").hide();
                     },
                     successIntercept: function(data){
                    	 layer.close(layerIndex);
                     }
        		 });
        	 }, function(i){
        		 layer.close(i);
        	 });
        	 if (tracecount > 0 && !$('.r5_c2').is(':hidden') && processBetTraceCore.thisOrderMaybeLose) {
            	 layer.alert('您的追号方案可能亏损，建议您修改方案！',{skin:'layui-layer-dialog layui-layer-rim',type:1,btn:["确定"],offset:150},function(i){
            		 layer.close(i);
            	 });
             }
         });
    });
});
// 下注情况数据
function getBcLotteryOrder(page){
	$.ajax({
		url:JIEBAO.base+'/lotteryBet/getBcLotteryOrder.do',
		data:{
			code :JIEBAO.lotCode,
			rows :10,
			page :page,
			},
		success:function(r){
			var betListHtml ='';
			var list = r.page.list;
			var results = r.page;
			if(list.length==0){
				var dateTime = new Date();
				var year=dateTime.getFullYear();
				var month=dateTime.getMonth()+1;
				var date=dateTime.getDate();
				if(month<10){
					month =  "0"+month;
				}else{
					month = month;
				}
				if(date<10){
					date = "0" +date;
				}else{
					date = date;
				}
				var riqi = year+"-"+month+"-"+date;
				$("#betListBetween").text('报表日期：'+riqi);
				betListHtml = '<tr><td colspan="11">没有符合查询的资料...</td></tr>';
				$("#betOrderTblFoot").hide();
			}else{
				for(var i=0;i<list.length;i++){
					if(list[i].lotteryHaoMa == null || list[i].lotteryHaoMa==''){
						list[i].lotteryHaoMa = '- -';
	    			}
					var dateTime = list[0].createTime.split(" ");
					var data = dateTime[0];
					$("#betListBetween").text('报表日期：'+data);
					betListHtml+='<tr>'
					betListHtml+='<td><a style="width: 100%;" class="BetInf" orderid="'+list[i].orderId+'"lotcode="'+list[i].lotCode+'">'+list[i].orderId+'</a></td>'
					betListHtml+='<td>'+list[i].createTime+'</td>'
					betListHtml+='<td>'+list[i].qiHao+'</td>'
					betListHtml+='<td>'+list[i].playName+'</td>'
					betListHtml+='<td>'+list[i].multiple+'</td>'
					betListHtml+='<td>'+list[i].lotteryHaoMa+'</td>'
					betListHtml+='<td>'+list[i].buyMoney+'</td>'
					if(list[i].winMoney != null){
						betListHtml+='<td>'+list[i].winMoney+'</td>'
					}else{
						betListHtml+='<td>0.00</td>'
					}
					// 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
					if(list[i].status == 1 ){
						betListHtml+='<td><span style="background-color: #428bca;border-radius: .25em;padding: 0.2em .6em 0.3em;">未开奖 </span></td>'
						if(!JIEBAO.unCancleOrder){
							betListHtml+='<td><a class="cheDan" orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'">撤单</a></td>'
						}else{
							betListHtml+='<td></td>';
						}
					}else if(list[i].status == 2 ){
						betListHtml+='<td><span style="background-color: #5cb85c;border-radius: .25em;padding: 0.2em .6em 0.3em;">已中奖 </span></td><td></td>'
					}else if(list[i].status == 3 ){
						betListHtml+='<td><span style="background-color: #d9534f;border-radius: .25em;padding: 0.2em .6em 0.3em;">未中奖 </span></td><td></td>'
					}else if(list[i].status == 4 ){
						betListHtml+='<td><span style="background-color: #5bc0de;border-radius: .25em;padding: 0.2em .6em 0.3em;">撤单 </span></td><td></td>'
					}else if(list[i].status == 5 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚成功 </span></td><td></td>'
					}else if(list[i].status == 6 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚异常 </span></td><td></td>'
					}else if(list[i].status == 7 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">开奖异常 </span></td><td></td>'
					}else if(list[i].status == 8){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">和局</span></td><td></td>'
					}
				}
				$("#betPageAmount").html(r.subBuyMoney);
				$("#betAmount").html(r.sumBuyMoney);
				$("#betOrderTblFoot").show();
			}
			$("#betList").html(betListHtml);
			pageChuLi(results,page,'xz');
			$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
		}
	})
}

var Core ={
	maxItem: 100,// 最大投注记录
	price: 2,// 每注2块钱
	changeColor:function (a, e, g, c) {// 修改皮肤样式
	    $("#jsStyle").remove();
	    return $("head").append("<style id='jsStyle'>body { background: " + a + 
	    		" !important; }#nav .current,#bettop_menu .current,#Bm_general > li.current > a,#Bm_optional > li.current > a,.open_result_no span," +
	    		"#srcListType li.current > a,#userinfotop_menu li.current > a,.selected,.row.r5 .current > a,.step > .current{ background-color:  " + e +
	            " !important; }.open_result_no > span,#gamebuy,.vote_total,.vote_total > div{ border-color: " + g + 
	            " !important; }.no_sum_title,.digits{ background-color: " +
	            g + " !important; }#gamebuy,.positionre > a,.arr_top > i{ color: " + g + " !important; }.row_con_c1_btm .fa-stack i{ color: " + g + 
	            " !important; } .row_con_c1_btm a { border-color: " + g + " !important; color: " + g + " !important; }.fa-customColor > i { color: " + c + 
	            " !important; } .fa-customColor > .fa-circle { color: " + g + " !important; }</style>")
	},
	rgb2hex:function(rgb){// 将rgb 转成16进制
		if(rgb.indexOf("#")!=0){ 
			rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/); 
			function hex(x) { 
				return ("0" + parseInt(x).toString(16)).slice(-2); 
			} 
			rgb= "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
		} 
		return rgb; 
	},
	strLimit: function(a, e) {
        e = e || 25;
        if (a.length < e) return a;
        var g = a;
        a.length > e && (g = g.substring(0, e) + "...");
        return g
    },
    countTotalBet: function() {
        var a = 0,e = 0;
        $("#BetNumberListContanior table tbody tr").each(function(g, c) {
            var h = Number(parseFloat($(this).find("td:eq(5)").text()));
            e = Core.__FloatAdd(e, h);
            h = Number(parseInt($(this).find("td:eq(3)").text()));
            a = Core.__FloatAdd(a, h)
        });
        $("#s_allTotamounts").text(parseFloat(e.toString().match(/\d+(\.\d{1,3})?/)[0] || 0));
        $("#s_allTotbets").text(a)
    },
	getLotLast:function(qiHao){// 获得最近一期开奖结果
		if(!qiHao)return;
		$.getJSON(JIEBAO.base+"/lottery/lotLast.do?lotCode="+JIEBAO.lotCode+"&qiHao="+qiHao, function(last){
			if(last){
				if(last.haoMa  && last.haoMa.indexOf("?")>-1){
					// 最近一期还没有开奖
					clockArea.reGetLotLast(last.qiHao);return;
				}
				clockArea.stopReGetLotOp();
				//Core.renderLast(last);
				Core.getLotOp();
				$("#betplay_data").find("input[name='playId']:checked").click();
			}
		});
	},
	renderLast:function(last){// 显示最近一期开奖结果
		var hms=last.haoMa.split(","),h="",noClass="balls_10",wenHao=false;
		if(hms[0]=="?"){
			wenHao=true;
			if(JIEBAO.lotCode=='PCEGG'){
				noClass="balls_7";
				h=h+'<span class="bg_black3_content">'+Core.getHaoMas()+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+Core.getHaoMas()+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+Core.getHaoMas()+'</span><span class="bg_black3_content">=</span><span class="bg_black3_content">'
					+ Core.getHaoMas(28) +'</span>';
			}else{
				noClass="balls_"+hms.length;
				for(var j=0,jlen=hms.length;j<jlen;j++){
					h=h+'<span class="bg_black3_content">'+Core.getHaoMas()+'</span>';
				}
			}
		}else{
			if(JIEBAO.lotCode=='PCEGG'){
				noClass="balls_7";
				h=h+'<span class="bg_black3_content">'+hms[0]+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+hms[1]+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+hms[2]+'</span><span class="bg_black3_content">=</span><span class="bg_black3_content">'
					+(hms[0]=="?"?"?":(parseInt(hms[0])+parseInt(hms[1])+parseInt(hms[2])))+'</span>';
			}else{
				noClass="balls_"+hms.length;
				for(var j=0,jlen=hms.length;j<jlen;j++){
					h=h+'<span class="bg_black3_content">'+hms[j]+'</span>';
				}
			}
		}
		$("#last_result_no").removeClass("balls_3 balls_5  balls_7 balls_10").addClass(noClass).html(h);
		if(wenHao){
			Core.scrollFun();
		}
		if(last.qiHao){
			$("#last_result_qihao").html("第"+last.qiHao+"期");
		}else{
			$("#last_result_qihao").html("无期数");
		}
	},
	scrollFun:function(){
		var uls = $("#last_result_no").find("ul");
		clearTimeout(Core.scrollFunTimeout);
		if(uls.length){
			function scrollFn(){
				uls.each(function(i){
					var $it=$(this),h=$it.height(),st=parseInt($it.css("top"));
					if(h+st>60){
						st = st-60;
					}else{
						st=0;
					}
					$it.css({"top":st});
				});
			}
			Core.scrollFunTimeout = setInterval(scrollFn,80);
			scrollFn();
		}
	},
	getHaoMas:function(max){
		if(max){
			var h="<ul>";
			for(var i=0;i<max;i++){
				h+="<li>"+i+"</li>";
			}
			h+="</ul>";
			return h;
		}
		switch(JIEBAO.lotType){
			case "3":
			case "5":
				var max=11;
				if(JIEBAO.lotType==5){
					max=12;
				}
				var h="<ul>";
				for(var i=1;i<max;i++){
					if(i<10){
						h+="<li>0"+i+"</li>";
					}else{
						h+="<li>"+i+"</li>";
					}
				}
				h+="</ul>";
				return h;
			default:
				var h="<ul>";
				for(var i=0;i<10;i++){
					h+="<li>"+i+"</li>";
				}
				h+="</ul>";
				return h;
		}
	},
	getLotOp:function(){// 获得当前彩票的倒计时，历史开奖结果和最近一期开奖结果
		if(!JIEBAO.lotCode)return;
		$.getJSON(JIEBAO.base+"/lottery/lotOp.do?lotCode="+JIEBAO.lotCode, function(json){
			if(json){clockArea.stopReGetLotOp();
				if(!json.success && json.msg){
					$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">'+json.msg+'</div>');
					$("#Bm_general").empty();
					var hms="?,?,?,?,?";
					switch(JIEBAO.lotCode){
					case "FC3D":
					case "PCEGG":
					case "PL3":hms="?,?,?";break;
					case "BJSC":hms="?,?,?,?,?,?,?,?,?,?";break;
					}
					json.last={"haoMa":hms};
				}else{
					if($("#Bm_general").find("li").length==0){
						if(json.palyGroups && json.palyGroups.length){// 玩法大类
							var arr = json.palyGroups;
							var h = "";
							for(var i = 0,len=arr.length;i<len;i++){
								h = h+'<li><a href="javascript:;" class="btn b0" p="group_'+arr[i].lotType+"_"+arr[i].code+'">'+arr[i].name+'</a></li>'
							}
							$("#Bm_general").html(h).find("a:first").click();
						}else{
							$("#Bm_general").html('<li class="current">该彩种没有玩法</li>');
						}
					}
					if(json.last && json.last.haoMa  && json.last.haoMa.indexOf("?")>-1){
						// 最近一期还没有开奖
						clockArea.reGetLotLast(json.last.qiHao);
					}
				}
				// 当前期信息，倒计时，期号
				Core.time.startTime(json.current);
				if(json.history&&json.history.length){// 最近开奖
					var arr=json.history;
					var h = "",obj=null,hms=null;
					for(var i = 0,len=arr.length;i<len;i++){
						obj = arr[i];
						hms=obj.haoMa.split(",");
						h=h+'<li class="bg_black3 hli ';
						switch(JIEBAO.lotCode){
						case 'PCEGG':
							h=h+'qs7"><span class="qh">'+obj.qiHao+':</span><span>'
								+hms[0]+'</span><span>+</span><span>'
								+hms[1]+'</span><span>+</span><span>'
								+hms[2]+'</span><span>=</span><span>'
								+(hms[0]=="?"?"?":(parseInt(hms[0])+parseInt(hms[1])+parseInt(hms[2])))+'</span>';
							break;
						default:
							h=h+'qs'+hms.length+'"><span class="qh">'+obj.qiHao+':</span>';
							for(var j=0,jlen=hms.length;j<jlen;j++){
								h=h+'<span>'+hms[j]+'</span>';
							}
						}
						h=h+'</li>';
					}
					$("#lotHistory > ul").html(h);
				}else{
					$("#lotHistory > ul").html("");
				}
				if(json.last){
					Core.renderLast(json.last);
				}
			}else{
				$("#betplay_data").html('<div style="text-align:center;height:270px;line-height: 270px;font-size: 100px;">系统发生错误，请联系客服</div>');
				$("#Bm_general").empty();
			}
		});
	},
	getPlayType:function(){
		if(Core.gamePlay.isLoaded(JIEBAO.lotType)){
			$("#Bm_general").find("a:first").click();
		}else{
			$.get(JIEBAO.base+"/member/"+JIEBAO.folder+"/module/lottery/lot_play_type_"+JIEBAO.lotType+ "_" + JIEBAO.folder +".html?v="+JIEBAO.caipiao_version, function(html){
				Core.gamePlay.strToTpl(JIEBAO.lotType,html);
				$("#Bm_general").find("a:first").click();
			},"html");
		}
	},
	__FloatAdd: function(a, e) {
        var g, c;
        try {
            g = a.toString().split(".")[1].length
        } catch (h) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (l) {
            c = 0
        }
        g = Math.pow(10, Math.max(g, c));
        return (a * g + e * g) / g
    },
    __FloatSubtraction: function(a, e) {
        var g, c, h;
        try {
            g = a.toString().split(".")[1].length
        } catch (l) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (m) {
            c =
                0
        }
        h = Math.pow(10, Math.max(g, c));
        return ((a * h - e * h) / h).toFixed(g >= c ? g : c)
    },
	__FloatMul: function(a, e) {
    	var g = 0,
            c = a.toString(),
            h = e.toString();
        try {
            g += c.split(".")[1].length
        } catch (l) {}
        try {
            g += h.split(".")[1].length
        } catch (m) {}
        return Number(c.replace(".", "")) * Number(h.replace(".", "")) / Math.pow(10, g)
    },
    __FloatDiv: function(a, e) {
        var g = 0,
            c = 0,
            h=a.toString(), l=e.toString();
        try {
            g = h.split(".")[1].length
        } catch (m) {}
        try {
            c = l.split(".")[1].length
        } catch (q) {}
        h = Number(h.replace(".", ""));
        l = Number(l.replace(".",""));
        return h / l * Math.pow(10, c - g)
    }
};
// 玩法小类管理
Core.gamePlay=function(){
	var l={},typeIsLoaded={};
	var E=function(b){
		b=b.split("\u263b");
		var k={},h;
		for(h in b){
			var l=b[h].split("\u263a");
			if(l.length!=2)continue;
			var s = l[1].split("\u263d"),r=[];
			for(h in s){
				var e=s[h].split("\u263e");
				if(e.length!=2)continue;
				r[r.length]=e[0];
				k[e[0]]=e[1];
			}
			k[l[0]]=r;
		}
		return k
	};
	return{
		strToTpl:function(a,b){
			l=$.extend(l,E(b));
			typeIsLoaded[a]=a;
			return this
		},
		isLoaded:function(a){
			return typeIsLoaded[a];
		},
		switchGroup:function(a){
			var r = l[a];
			if(r && r.length>0){
				Core.gamePlay.switchPlay(r[0]);
			}else{
				Core.betting.resetSumbetCount();
				$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">没有提供玩法</div>');
			}
		},
		switchPlay:function(a){
			Core.betting.resetSumbetCount();
			if(!a || !l[a]){
				$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">没有提供玩法</div>');
				return;
			}
			$("#betplay_data").html(l[a]);
			var playCode=$("#betplay_data").find("input[name='playId']:checked").attr("c");
			if(playCode){
				$.getJSON(JIEBAO.base+"/lottery/getRateOp.do?lotCode="+JIEBAO.lotCode+"&playCode="+playCode+"&lotType="+JIEBAO.lotType,function(json){
					if(json){
						$("#gcrate").html(json.peiLv);
						if(json.hasOwnProperty("fanShui")){
							$("#rebateShow").html(json.fanShui+"%");
						}
						if(json.hasOwnProperty("lengRe") || json.hasOwnProperty("yiLou")){
							var g = $("#betplay_data .num_btn"),e=json.yiLou,a=json.lengRe;
							if(e && e.length){
								for (var i=0,l=e.length;i<l;i++){
									for (var d=0,dlen=e[i].length;d<dlen;d++) g.eq(i).find("a[num]").eq(d).nextAll("i").text(e[i][d]);
								}
							}
							if(a && a.length){
								for (var i=0,l=a.length;i<l;i++){
									for (var d=0,dlen=a[i].length;d<dlen;d++) g.eq(i).find("a[num]").eq(d).nextAll("s").text(a[i][d]);
								}
							}
						}
					}
				});
			}
		}
	}
}();

// 倒计时
Core.time={};
(function(){
	var b=null,k=0,cur=null,
	p=function(){
		var b;
		try{b=window.performance.now()}catch(k){b=Date.now()}
		return b
	};
	Core.time.startTime=function(a){// 开始倒计时
		if(!a){
			clockArea.stop();
			clockArea.setExpireTime(0).setRound("").setStatus("尚未开盘");
			cur=null;Core.curQiHao=null;
			return;
		}
		cur=a;
		Core.curQiHao=a.qiHao;
		Core.time.setServerTime(cur.serverTime);
		clockArea.setRound(cur.qiHao).setStatus("开放下注").start();
		processBetTraceCore.generateBetView();
	}
	Core.time.setServerTime=function(h){// 设置服务器返回的当前时间
		b=new Date(h.replace(/-/g,"/"));
		k=p();return this
	};
	Core.time.getServerTime=function(){// 根据服务器返回的时间，计算服务器当前时间
		if(null==b)throw Error("还没有同步时间","Core.time.getServerTime");
		return new Date(b.getTime()+(p()-k));
	}
	Core.time.getCountDown=function(){// 获得倒计时秒数
		if(!cur || !cur.activeTime)!1;
		var b=new Date(cur.activeTime.replace(/-/g,"/"));
		if(!(b instanceof Date)||isNaN(b.getTime()))return !1;
		var k=0;
		try{k=this.getServerTime().getTime()}catch(h){k=Date.now()}
		b=(new Date(b-k)).getTime()/1E3;
		return b;
	};
})();

// 投注相关
Core.betting=function(){
	return {
		resetSumbetCount: function() {// 重置
			bet_result = {};
			$("#Immediate , #allTotamounts").text(0);
			$("#betplay_data").find(".no_input_box textarea").val("");
	    },
	    sumbetCount: function(eventEle) {// 计算投注注数和投注金额入口
			var c2 =$("#betplay_data").find(".row_con_c2"),
				_type=c2.attr("_type"),
				_row = c2.attr('_row'),
		        _column = c2.attr('_column'),
		        _val = c2.attr('_val');
	        bet_result = {};
	        switch (_type) {
	            case "1":
	                Core.betting.__Duplex(_row, _column,_val);
	                break;
	            case "3":
	                Core.betting.__Positioninggall(_row, _column);
	                break;
	            case "4":
	                Core.betting.__ElectionsAndValues(_row, _column);break;
	            case "5":
	                Core.betting.__Constitute(_row, _column, _val, c2.attr('_arbitrary'));
	                break;
	            case "8":
	                Core.betting.__Arbitrary_Duplex(_row, _column);break;
	            case "11":
	                Core.betting.__Nrepeat(_row, _column);break;
	            case "12":
	                Core.betting.__UseC(_row, _column);break;
	            case "13":
	            	 Core.betting._HuYi2Row(c2,eventEle);break;
	        }
	        bet_result.types = _type
	    },
	    sumTextareaBetCount: function(_type,_vals) {// 计算单式投注注数和投注金额入口
	    	bet_result = {};
	    	bet_result.types = _type;
	    	var zhuShu = 0, ma="";
	    	if(_vals){
	    		for(var i=0,len=_vals.length;i<len;i++){
		    		if(_vals[i]){
		        		ma=ma+";"+_vals[i];
		        		zhuShu++;
		    		}
		    	}
	    	}
	        bet_result.betCount = zhuShu;
	        bet_result.myarray =ma?ma.substring(1):"";
	        bet_result.errorTagStr = "";
	        bet_result.optional = "";
	        $("#Immediate").text(zhuShu);
	        var l = Core.price,s=$("#buymultiple").val(),t=$("#selectdollarunit").val();
	        l *=Core.__FloatMul(zhuShu,s);
            l = Core.__FloatDiv(l, t);
	        $("#allTotamounts").text(l);
	    },
	    _HuYi2Row:function(con,eventEle){
	    	var arr = ["",""],n=eventEle.attr("num");
	    	if(eventEle.hasClass("selected")){
	    		if(n.length==2){
		    		con.find("a[num='"+n.substring(1)+"']").removeClass("selected");
		    	}else{
		    		con.find("a[num='"+n+n+"']").removeClass("selected");
		    	}
	    	}
	    	con.find(".num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	arr[a] +=$(j).attr("num");
	            });
	        });
	    	var zhuShu=arr[0].length/2*arr[1].length;
	    	Core.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Duplex: function(_row, _column,_val) {
	    	_row = _row || 1;
	    	_column = _column || 1;
	        var arr = [],row = 0,column = 0,zhuShu = 1;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
	            		if("undefined" != typeof arr[a]){
	            			arr[a] += i;
	            		}else{
	            			arr[a] =i;
	            			row++;
	            		}
	            	}
	            })
	        });
	        if (row != _row || column < _column) {
	        	Core.betting.resetSumbetCount();
	        	return false;
	        }
	        for (var t in arr) zhuShu *= arr[t].length;
	        if(_val){
	        	zhuShu=zhuShu/_val;
	        }
	        Core.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Positioninggall: function(a, e) {
	        e = e || 1;
	        var g = 0,
	            c = 0,
	            h = 0,
	            l = [],q=JIEBAO.lotCode,
	            m = Array(parseInt(a || 5));
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            		}else{
	            			m[a] =i;
	            		}
	            	}
	            });
	            h++
	        });
	        if (0 > h || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        for (var t in m) g += m[t].length;
	        if ("BJSC"  == q || "SD11X5" == q || "SH11X5" == q || "GX11X5" == q || "JX11X5" == q || "GD11X5" == q||q=="XYFT"||q=="LXYFT") g /= 2;
	        Core.betting.__showAndSaveBet(g, m)
	    },
	    __ElectionsAndValues: function(a, e) {
	        var c = 0,
	            h = 0,
	            l = 0,
	            m = [];
	        switch (e) {
	            case "J_3":
	                var q = [10, 54, 96, 126, 144, 150, 144, 126, 96, 54];
	                break;
	            case "S_3":
	                q = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1];
	                break;
	            case "B_3":
	            	q = [0, 0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 8, 8, 6, 6, 4, 4, 2, 2];
	        		break;
	            case "G_3":
	                q = [-1, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1];
	                break;
	            case "J_2":
	                q = [10, 18, 16, 14, 12, 10, 8, 6, 4, 2];
	                break;
	            case "S_2":
	                q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
	                break;
	            case "G_2":
	                q = [-1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1];
	                break;
	            case "S_1":
	                q = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	                break;
	            case "K_3":
	                q = [0,0,0,1,3, 6, 10, 15, 21, 25, 27, 27, 25, 21, 15, 10, 6, 3, 1];
	                break;
	        }
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		h++;
	            		m.push(i);
	            	}
	            });
	            l++
	        });
	        if (l < a) return $("#Immediate , #allTotamounts").text(0), !1;
            for (var t in m) c += parseInt(q[m[t]]);
	        Core.betting.__showAndSaveBet(c, m)
	    },
	    __Constitute: function(_row, _column, _val, _arbitrary) {
	        var h = 1,l = 1,column = 0,row = 0,arr = [];
	        _arbitrary = _arbitrary || 1;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
            			arr.push(i);
	            	}
	            });
	            row++;
	        });
	        if (row != _row || column < _column) {
	        	Core.betting.resetSumbetCount();
	        	return false;
	        }
	        var a=0;
	        switch (_val) {
	            case "G_2":
	                for (a = arr.length; a > arr.length - _column; a--) h *= a;
	                break;
	            case "P_1":
	            case "P_2":
	            case "P_3":
	            case "G_3":
	            case "R_2":
	                for (a = 0; a < _column; a++) h *= arr.length - a;
	                for (a = _column; 0 < a; a--) l *= a;
	                h /= l;
	                break;
	        }
	        var r;
	        switch (_arbitrary) {
	            case 1:
	                break;
	            default:{
	            	h *= $(".no_frame span#positioninfo").text();
                    r = Core.betting.position_OptionalArr().join(",");
	            }
	        }
	        Core.betting.__showAndSaveBet(h, arr,false, r)
	    },
	    position_OptionalArr: function() {
	        var e = [];
	        $(".row_con_c2 input[name^='position_']").each(function() {
	            $(this).prop("checked") && e.push($(this).val())
	        });
	        return e
	    },
	    __Arbitrary_Duplex: function(a, e) {
	        var c = 0,
	            h = 0,
	            m = [],
	            q = [];
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            			q[a] += 1
	            		}else{
	            			m[a] =i;
	            			q[a] = 1;
	            			h++;
	            		}
	            	}
	            });
	        });
	        if (h < a || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        var g = Core.betting.__Elections(q, e);
	        while(m.length<5){
	        	m[m.length]="";
	        }
	        Core.betting.__showAndSaveBet(g, m)
	    },
	    __Elections: function(a, e) {
	        var g = 0,
	            c = $.map(a, function(a) {
	                if ("" !== a) return a
	            }),
	            c = Core.betting.__C_combine(c, e);
	        switch (e) {
	            case "2":
	                for (var h in c) g += c[h][0] * c[h][1];
	                break;
	            case "3":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2];
	                break;
	            case "4":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2] * c[h][3]
	        }
	        return g
	    },
	    __C_combine: function(a, e) {
	        for (var g = [[]], c = [], h = 0, l = a.length; h < l; ++h)
	            for (var m = 0,q = g.length; m < q; ++m)(g[m].length < e - 1 ? g : c).push(g[m].concat([a[h]]));
	        return c
	    },
	    __Nrepeat: function(a, e) {
	        var g = 1,
	            c = 0,
	            h = 0,
	            l = [],
	            m = [];
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a].push(i);
	            			l[a] += i
	            		}else{
	            			m[a]=[i];
	            			l[a] =i;
	            			h++;
	            		}
	            	}
	            })
	        });
	        if (h < a || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        switch (e) {
	            case "2":
	                for (var t in m) g *= Core.betting.___P(m[t].length, 1);
	                g -= Core.betting.arrayIntersection(m[0],m[1]).length;
	                break;
	            case "3":
	                g = m[0].length * m[1].length * m[2].length;
	                var q = Core.betting.arrayIntersection(m[0], m[1]).length * m[2].length;
	                var t = Core.betting.arrayIntersection(m[1], m[2]).length * m[0].length;
	                var r = Core.betting.arrayIntersection(m[2], m[0]).length * m[1].length,
	                    y = 2 * Core.betting.count(Core.betting.array_intersect(m[0], m[1], m[2])),
	                    g = g - q - t - r + y
	        }
	        Core.betting.__showAndSaveBet(g, l)
	    },
	    __UseC: function(a, e) {
	        var g = [],h = 0,l = 0,m = $("#betplay_data .row_con_c2").attr("_g11") || false;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		l++;
	            		g.push(i);
            			h++;
	            	}
	            })
	        });
	        if (m) {
	            if (h < a || l < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        } else if (h != a || l < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        m = Core.betting.__C_combine(g, e).length;
	        Core.betting.__showAndSaveBet(m, g)
	    },
	    arrayIntersection: function(a, e) {
	        for (var g = 0, c = 0, h = []; g < a.length && c < e.length;) a[g] < e[c] ? g++ : (a[g] > e[c] || (h.push(a[g]), g++), c++);
	        return h
	    },
	    ___P: function(a, e) {
	        return Core.betting.___factorial(a) / Core.betting.___factorial(a - e)
	    },
	    ___C: function(a, e) {
	        return e > a ? 0 : Core.betting.___P(a, e) / Core.betting.___factorial(e)
	    },
	    ___H: function(a, e) {
	        return Core.betting.___C(e + a - 1, a - 1)
	    },
	    ___factorial: function(a) {
	        return 1 == a || 0 == a ? 1 : a * Core.betting.___factorial(a -1)
	    },
	    array_intersect: function(a) {
	        var e = {},
	            g = arguments.length,
	            c = g - 1,
	            h = "",
	            l = {},
	            m = 0,
	            q = "";
	        a: for (h in a) b: for (m = 1; m < g; m++) {
	            l = arguments[m];
	            for (q in l)
	                if (l[q] === a[h]) {
	                    m === c && (e[h] = a[h]);
	                    continue b
	                }
	            continue a
	        }
	        return e
	    },
	    count: function(a, e) {
	        var g, c = 0;
	        if (null === a || "undefined" ===
	            typeof a) return 0;
	        if (a.constructor !== Array && a.constructor !== Object) return 1;
	        "COUNT_RECURSIVE" === e && (e = 1);
	        1 != e && (e = 0);
	        for (g in a) a.hasOwnProperty(g) && (c++, 1 != e || !a[g] || a[g].constructor !== Array && a[g].constructor !== Object || (c += this.count(a[g], 1)));
	        return c
	    },
	    __showAndSaveBet: function(zhuShu, arr,c, h) {// 显示并记录投注信息
	    	zhuShu = zhuShu || 0;
	    	arr = arr || [];
	        c = c || [];
	        h = h || "";
	        bet_result.betCount = zhuShu;
	        var ma="";
	        for(var i=0,len=arr.length;i<len;i++){
	        	if(arr[i]){
	        		ma=ma+","+arr[i];
	        	}else{
	        		ma=ma+",-";
	        	}
	        }
	        bet_result.myarray =ma.substring(1);
	        bet_result.errorTagStr = c.toString();
	        bet_result.optional = h
	        $("#Immediate").text(zhuShu);
	        var l = Core.price,s=$("#buymultiple").val(),t=$("#selectdollarunit").val();
	        l *=Core.__FloatMul(zhuShu,s);
            l = Core.__FloatDiv(l, t);
	        $("#allTotamounts").text(l);
	    }
	}
}();
// 倒计时
var clockArea=function(){
	var b=$("#lotClock"),
		a=b.find(".clock_no"),
		d=b.find("#nowroundsShow"),
		g=b.find(".status"),
		h=b.find(".circle .hour"),
		k=b.find(".circle .min"),
		l=b.find(".circle .sec"),t1,t2;
	return obj={
		setExpireTime:function(b){
			var c=Math.floor(b/3600),
			e=Math.floor(b%3600/60);
			b=Math.floor(b%60);
			a.text(("0"+c).slice(-2)+":"+("0"+e).slice(-2)+":"+("0"+b).slice(-2));
			h.css("transform","rotate("+15*c+"deg)");
			k.css("transform","rotate("+6*e+"deg)");
			l.css("transform","rotate("+6*b+"deg)");
			return this
		},
		setRound:function(a){
			d.text(a?"第 "+a+" 期":"无期数");
			return this
		},
		setStatus:function(a){
			g.text(a);
			return this
		},
		start:function(){
			this.stop();
			this.tick()
		},
		stop:function(){
			clearTimeout(t1);
			return this
		},
		tick:function(){
			var a=this;
			clearTimeout(t1);
			var z=$("#nowGP");
			if(z.length==0 || !z.hasClass("current"))return this;
			var  e=Core.time.getCountDown();
			if(e===!1)return;
			if(0<e)a.setExpireTime(Math.floor(e)),
				10>=e&&soundNotify.play();
			else{
				a.setStatus("停止下注");
				layer.alert("您好，第 "+Core.curQiHao+" 期 已截止，投注时请确认您选的期号。", {time:30000});
				setTimeout(function(){Core.getLotOp()},200);
			}
			t1=setTimeout(function(){a.tick()},1000);return this
		},
		reGetLotLast:function(qiHao){
			clearTimeout(t2);
			var z=$("#nowGP");
			if(z.length==0 || !z.hasClass("current"))return this;
			t2 = setTimeout(function(){Core.getLotLast(qiHao)},2500+Math.floor(Math.random()*1000));return this
		},
		stopReGetLotOp:function(){
			clearTimeout(t2);return this
		}
	}
}();
// 开奖提示语音
var soundNotify={};
$(function(){
	var aa=$("#soundNotification"),e=new Audio;
	e.canPlayType("audio/mpeg")?(e.type="audio/mpeg",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.mp3"):
	e.canPlayType("audio/ogg")?(e.type="audio/ogg",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.ogg"):
	(e.type="audio/mp4",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.m4a");
	soundNotify={
		a:aa,
		$wrap:aa.find(".fa-stack-2x"),
		mute:function(){
			this.$wrap.removeClass("fa-circle-thin").addClass("fa-ban");
			return this
		},
		unMute:function(){
			this.$wrap.removeClass("fa-ban").addClass("fa-circle-thin");
			return this
		},
		isMute:function(a){// 是否静音
			if(null==a){
				var b;
				try{
					b=JSON.parse(localStorage.getItem("muteTickSound")||!1)
				}catch(c){
					b=!1
				}
				return b
			}
			try{
				localStorage.setItem("muteTickSound",!!a)
			}catch(e){}
			this.renderView();
			return this
		},
		play:function(){
			if(!this.isMute()){
				try{
					e.pause(),
					e.currentTime=0,
					e.play()
				}catch(a){}
			}
			return this
		},
		stop:function(){
			e.pause();
			e.currentTime=0;
			return this
		},
		renderView:function(){
			if(this.isMute())this.mute();else this.unMute();return this
		}
	};
	aa.click(function(){
		soundNotify.isMute(!soundNotify.isMute());
	});
	soundNotify.renderView();
});
// 机选
$(function() {
	var rand=function(a, e) {// 随机数
        var g = arguments.length;
        if (0 === g) a = 0, e = 2147483647;
        else if (1 === g) throw Error("Warning: rand() expects exactly 2 parameters, 1 given");
        return Math.floor(Math.random() * (e - a + 1)) + a
    },
    in_array=function(a,e, g) {// 数据是否在数字里面
        var c = "";
        if (g){
        	for (c in e) {
                if (e[c] === a) return !0
            }
        }else{
        	for (c in e) {
                if (e[c] == a) return !0
            }
        }
        return !1
    };
    var h = {// 选择投注号码方法
        select: function(a, c) {
            null == c && (c = 1);
            if (c >= a.length) return a.click(), !0;
            var e;
            for (var g = 0; g < c; g++){
            	e = rand(0, a.length - 1);
            	if(g === c - 1){
            		a.eq(e).click() 
            	} else{
            		if("checkbox" === a.eq(e).attr("type")){
            			a.eq(e).prop("checked", !0)
            		} else{
            			a.eq(e).addClass("selected");
            		}
            	}
            	a.splice(e, 1)
            }
            return !0
        },
        selectByLine: function(a, c) {
            var e=this;
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a =parseInt(a);
            return $(c).each(function(c, g) {
                return e.select($(this).find("a[num]:not(.selected)"), a)
            })
        },
        selectChkbox: function(a) {
            null == a && (a = 1);
            a = parseInt(a);
            var c = $(".radio_select input").prop("checked", !1);
            return this.select(c, a)
        },
        selectXor: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a instanceof Array || (a = parseInt(a));
            var e = this;
            return $(c).each(function(g, h) {
                var l = $(this).find("a[num]:not(.selected)");
                $(c).find("a[num].selected").each(function(a, c) {
                    return l = l.filter(":not([num=" + $(this).attr("num") + "])")
                });
                return a instanceof Array ? e.select(l, a[g] || 1) : e.select(l, a)
            })
        },
        randSelector: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a = parseInt(a) || 1;
            var e = $(c).length - 1;
            if (e > a - 1) {
                var h = [],l=[],g;
                while(l.length<a){
                	g = rand(0, e);
                	if(!in_array(g, l)){
                		l.push(g);
                		h.push(c + ":eq(" + g + ")")
                	}
                }
                c = h.join(",")
            }
            return c
        }
    };
	$("#randomPlay").click(function() {// 机选
		$(".num_btn .selected").click();
        var $playId = $(".ch_radio td input[type=radio]:checked"),lotType=$playId.attr("t"),code=$playId.attr("c");
        switch (code) {
        case "dwd":
        	return h.selectByLine(1,h.randSelector(1));
        case "h3zux_zu3":
        case "q3zux_zu3":
        case "z3zux_zu3":
        case "bdw_2m":
        case "zux_z3":
        case "ebth":
        	return h.selectByLine(2);
        case "h3zux_zu6":
        case "q3zux_zu6":
        case "z3zux_zu6":
        case "zux_z6":
        case "sbtx":
        	return h.selectByLine(3);
        case "rxwf_r2zx_fs":
        	return h.selectByLine(1,h.randSelector(2));
        case "rxwf_r3zx_fs":
        	return h.selectByLine(1,h.randSelector(3));
        case "rxwf_r4zx_fs":
        	return h.selectByLine(1,h.randSelector(4));
        case "rxwf_r3zux_zu3":
        	h.selectChkbox(3);
        	return h.selectByLine(2);
        case "rxwf_r3zux_zu6":
        	h.selectChkbox(3);
        	return h.selectByLine(3);
        case "h2zx":
        case "q2zx":
        case "em_h2zux":
        case "em_q2zux":
        case "rxfs_rx2z2":
        	return h.selectXor(2);
        case "q3zx_fs":
        case "q2zx_fs":
        case "h3zx_fs":
        case "z3zx_fs":
        case "h2zx_fs":
        	if(lotType==3 ||lotType==5){// 北京赛车 //11选5
        		return h.selectXor();
        	}
        	return h.selectByLine();
        case "sxzx":
        case "h3zx":
        case "q3zx":
        case "z3zx":
        case "rxfs_rx3z3":
        	return h.selectXor(3);
        case "rxfs_rx4z4":
        	return h.selectXor(4);
        case "rxfs_rx5z5":
        	return h.selectXor(5);
        case "rxfs_rx6z5":
        	return h.selectXor(6);
        case "rxfs_rx7z5":
        	return h.selectXor(7);
        case "rxfs_rx8z5":
        	return h.selectXor(8);
        case "ethdx":
        	return h.selectXor();
        default:
        	return h.selectByLine();
        }
    });
});
var processBetTraceCore = function(){
	var $betTrace = $("#betTrace"),
		$totalQiShu=$betTrace.find("#lt_trace_count"),
		$totalMoney=$betTrace.find("#lt_trace_hmoney"),
		$betTraceData=$betTrace.find("#betTraceData"),
		$betList=$("#BetNumberListContanior");
	return{
	    generatedTraceData: false,// 是否已经生成并显示追号期号
	    thisOrderMaybeLose: false,// 可能出现亏损
	    qiHaos:[],// 可追号得期号
	    startQiShu:10,// 追号期数
	    startTimes:1,// 起始倍数
	    totalCost:0,// 总下注金额
	    profitArr:false,// 盈利情况
	    initView:function(d){// 保存当天可追号得期号，并显示追号期号
	    	this.qiHaos = d;
	    	this.generateBetView();
	    },
	    generateBetView: function() {// 生成并显示追号期号
	    	if(!Core.curQiHao || !this.qiHaos||this.qiHaos.length==0)return;
	        this.generatedTraceData = true;
	        var html = "",len = this.qiHaos.length,qh,cur=false;
            for (var a = 0,b=0; a < len && b<this.startQiShu;a++) {
            	qh = this.qiHaos[a];
            	if(qh<Core.curQiHao){
            		continue;
            	}
                html+= '<tr><td><input type="checkbox" value="' +qh + '"checked></td><td>';
                if (!cur) {
                	html+=qh+ '(当前期)';
                	cur = true;
                } else {
                	html+=qh;
                }
                html+='</td><td><input type="text" class="w40" qh="'+qh+'" value="'+((b+1)*this.startTimes)+
                	'" autocomplete="off" maxlength="4">倍</td><td class="c_red">--</td><td class="c_red">--</td><td class="c_red">--</td><td class="c_red">--</td></tr>';
                b++;
            }
            $betTraceData.html(html);
	        
	        $betTraceData.find('input[type="checkbox"]').unbind('change').bind('change', function() {
	            processBetTraceCore.calcView();
	        });
	        $betTraceData.find('input[type="text"]').unbind('keyup').bind('keyup', function() {
	            processBetTraceCore.calcView();
	        });
	        this.updateBetData();
	    },
	    updateBetData: function() {// 更新追号期号信息
	        if (!this.generatedTraceData) {
	            return false;
	        }
	        var trs=$betList.find("tr[buyinfo]"),tongJi=true;
	    	if(trs.length>0){
	    		var playCode="",buyInfos=[],cost=0,c,e;
	    		trs.each(function(){
		    		var it = $(this);
		    		if(playCode==""){
		    			playCode=it.attr("c");
		    		}else if(playCode!=it.attr("c")){
		    			tongJi=false;
		    		}
		    		cost +=(parseFloat(it.attr("m"),10)||0);
		    		c = it.attr("buyinfo").split("\|");
	    			e={};
	    			e.cash=parseInt(c[3])/parseInt(c[2]);
		    		e.totalCost=cost;
	    			e.rate=(parseFloat(it.attr("r"),10)||0);
	    			e.rateCash=e.cash*e.rate;
	    			e.balls=c[4];
	    			e.ballArgs=[];
	    			e.balls.match("@")&&(c=e.balls.split("@"),e.balls=c[1],e.ballArgs=c[0].split(","));
	    			e.balls=e.balls.split(",");
	    			c=(JIEBAO.lotType==3 || JIEBAO.lotType==5)?/\d\d|龙|虎/g:/\d|大|小|单|双/g;
	    			for(var d in e.balls)
	    				e.balls[d]=e.balls[d].match(c);
		    		buyInfos.push(e);
		    	});
	    		if(!tongJi){
		    		layer.msg('您选择的多种玩法无法计算盈利，将以 -- 呈现！',{icon: 5});
		    	}
	    		this.totalCost=cost;
    			processBetTraceCore.profitArr=tongJi?processBetTraceCore.calcProfit(buyInfos,playCode):false;// 计算盈利
	    		processBetTraceCore.calcView();
	        } else {// 还没有下注情况，不可以追号
	            this.totalCost = 0;
	            this.profitArr = false;
	            $betTrace.hide();
	            this.generatedTraceData = false;
	        }
	    },
	    calcView: function() {// 计算投注金额及其盈利情况，并显示
	        var traceCount = 0,amount=0;
	        this.thisOrderMaybeLose = false;
	        var _this = this,totalCost=_this.totalCost,beforeCount=0;
	        $betTraceData.find('tr').each(function(i, j) {
	            j = $(this).children();
	            var betRate = parseInt(j.eq(2).children().val())||0;// 赔率
	            if (j.eq(0).children().prop('checked') && betRate > 0) {
	                traceCount++;
	                i=betRate * totalCost;
                	beforeCount+=i;
                	j.eq(3).text(i.toFixed(2));
                    j.eq(4).text(beforeCount.toFixed(2));
	                if (_this.profitArr === false) {
	                    j.eq(5).text('--');
	                    j.eq(6).text('--');
	                } else {
	                    var calcRes = _this.generatorBetTrace(betRate, beforeCount);
                        j.eq(5).text(calcRes.earnStr);
                        j.eq(6).text(calcRes.percentStr);
                        if (calcRes.minEarn < 0) {
                            _this.thisOrderMaybeLose = true;
                        }
	                }
	            } else {
	                j.eq(3).text('--');
	                j.eq(4).text('--');
	                j.eq(5).text('--');
	                j.eq(6).text('--');
	            }
	        });
	        $totalQiShu.text(traceCount);// 追号期数
	        $totalMoney.text(beforeCount.toFixed(2));// 追号总金额
	    },
	    calcProfit:function(buyInfos,playCode){// 计算下注盈利
	    	var a,r={};
	    	this.temp=[];
	    	for(var i = 0,len=buyInfos.length;i<len;i++){
	    		a=buyInfos[i];
	    		switch(playCode){
		    	case "dwd":// 定位胆
// switch(JIEBAO.lotType){
// case '1'://系统彩
// case '2'://时时彩
		    		r=this.multipleWinning(a.balls,a.rateCash);
// break;
// }
	    			break;
		    	case "q2zx_fs":
		    	case "h2zx_hz":
		    	case "q2zx_hz":
		    	case "h2zx_fs":
		    	case "q3zx_fs":
		    	case "z3zx_fs":
		    	case "h3zx_fs":
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    		break;
		    	case "bdw_q31m":
		    	case "bdw_h31m":
		    	case "bdw_z31m":
		    		this.combineProfitArray(r,a.balls,a.rateCash);break;
		    	case "rxwf_r2zx_fs":
		    		r=this.multipleWinning(this.anyDirect(2,a.balls),a.rateCash);break;
		    	case "rxwf_r3zx_fs":
		    		r=this.multipleWinning(this.anyDirect(3,a.balls),a.rateCash);break;
		    	case "rxwf_r3zux_zu3":
		    		r=this.multipleWinning(this.anyGroup(3,2,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r3zux_zu6":
		    		r=this.multipleWinning(this.anyGroup(3,3,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r4zx_fs":
		    		r=this.multipleWinning(this.anyDirect(4,a.balls),a.rateCash);break;
		    	case "q3zux_zu3":
		    	case "h3zux_zu3":
		    	case "z3zux_zu3":
		    		this.combineProfitArray(r,this.generatorGroup(2,a.balls),a.rateCash);break;
		    	case "q3zux_zu6":
		    	case "h3zux_zu6":
		    	case "z3zux_zu6":
		    		this.combineProfitArray(r,this.generatorGroup(3,a.balls),a.rateCash);break;
		    	default:
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    	}
	    	}
    		var m=Number.MIN_VALUE,x=Number.MAX_VALUE,b;
			for(var d in r){
				b=r[d];
				m=Math.max(m,b);
				x=Math.min(x,b);
			}
			if(m!=Number.MIN_VALUE){
				return {maxProfit:parseFloat(m.toFixed(2)),minProfit:parseFloat(x.toFixed(2))};
	    	}
	    	return false;
	    },
	    generatorBetTrace:function(betRate, beforeCount){// 计算每一条追号得盈利
	    	var min = this.profitArr.minProfit,max = this.profitArr.maxProfit,minEarn=0,maxEarn=0,earnStr="",percentStr="";
	    	minEarn = min*betRate -beforeCount;
	    	maxEarn=max*betRate - beforeCount;
	    	earnStr = (minEarn==maxEarn)?minEarn.toFixed(2):minEarn.toFixed(2)+"至"+maxEarn.toFixed(2);
	    	percentStr = (minEarn==maxEarn)?((minEarn/beforeCount*100).toFixed(2)+"%"):((minEarn/beforeCount*100).toFixed(2)+"%")+"至"+((maxEarn/beforeCount*100).toFixed(2)+"%");
	    	return {earnStr:earnStr,percentStr:percentStr,minEarn:minEarn,maxEarn:maxEarn};
	    },
	    multipleWinning:function(b,d){
	    	var c,a,e,f,g,h;
	    	for(e in b){
	    		this.temp[e]instanceof Array||(this.temp[e]=[]),this.combineProfitArray(this.temp[e],b[e],d);
	    	}
    		h=g=0;
    		for(e in this.temp){
    			a=[];
    			for(f in this.temp[e])
    				a.push(this.temp[e][f]);
    			c=Math.max.apply(null,a);
    			a=Math.min.apply(null,a);
    			-Infinity===c&&(c=0);
    			Infinity===a&&(a=0);
    			g+=c;
    			h+=a
    		}
    		return{maxProfit:g,minProfit:h}
	    },
	    combineProfitArray:function(b,d,c){
	    	if(!(d instanceof Array))return!1;
	    	var a;
	    	for(var e= 0,f=d.length;e<f;e++){
	    		a=d[e],null!==a&&void 0!==a&&(b.hasOwnProperty(a)?b[a]+=c:b[a]=c);
	    	}
	    	return b
    	},
    	expandArray:function(b){
    		var d,c,a,e,f,g,h,k,l,n,m;
    		if(0===b.length)return!1;
    		b=b.slice();
    		m=b.splice(0,1)[0];
    		m instanceof Array||(m=m.toString().split(","));
    		f=0;
    		for(k=b.length;f<k;f++)
    			for(a=b[f],c=m.slice(),m=[],g=0,l=a.length;g<l;g++)
    				for(e=a[g],h=0,n=c.length;h<n;h++)
    					d=c[h],m.push(""+d+e);
    		return m
    	},
    	anyDirect:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G,J,L,K,M;
    		D=[];
    		if(2===b){
    			for(r=e=0,m=d.length-b;0<=m?e<=m:e>=m;r=0<=m?++e:--e){
    				if(null!==d[r]){
    					for(q=d[r],g=0,l=q.length;g<l;g++){
    						for(c=q[g],u=f=p=r+1,t=d.length;p<=t?f<t:f>t;u=p<=t?++f:--f){
    							if(null!==d[u]){
    								for(v=d[u],k=0,n=v.length;k<n;k++){
    									a=v[k];
    									h=""+r+u;
    									D[h]instanceof Array||(D[h]=[]);
    									D[h].push(""+c+a);
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(3===b){
    			for(r=n=0,z=d.length-b;0<=z?n<=z:n>=z;r=0<=z?++n:--n){
    				if(null!==d[r]){
    					for(B=d[r],m=0,f=B.length;m<f;m++){
    						for(c=B[m],u=q=C=r+1,A=d.length-1;C<=A?q<A:q>A;u=C<=A?++q:--q){
    							if(null!==d[u]){
    								for(E=d[u],p=0,k=E.length;p<k;p++){
    									for(a=E[p],g=t=x=u+1,w=d.length;x<=w?t<w:t>w;g=x<=w?++t:--t){
    										if(null!==d[g]){
    											for(y=d[g],v=0,l=y.length;v<l;v++){
    												e=y[v];
    												h=""+r+u+g;
    												D[h]instanceof Array||(D[h]=[]);
    												D[h].push(""+c+a+e);
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(4===b){
    			for(r=F=0,p=d.length-b;0<=p?F<=p:F>=p;r=0<=p?++F:--F){
    				if(null!==d[r]){
    					for(t=d[r],I=0,k=t.length;I<k;I++){
    						for(c=t[I],u=H=v=r+1,x=d.length-2;v<=x?H<x:H>x;u=v<=x?++H:--H){
    							if(null!==d[u]){
    								for(w=d[u],G=0,l=w.length;G<l;G++){
    									for(a=w[G],g=J=y=u+1,z=d.length-1;y<=z?J<z:J>z;g=y<=z?++J:--J){
    										if(null!==d[g]){
    											for(B=d[g],L=0,n=B.length;L<n;L++){
    												for(e=B[L],q=K=C=g+1,A=d.length;C<=A?K<A:K>A;q=C<=A?++K:--K){
    													if(null!==d[q]){
    														for(E=d[q],M=0,m=E.length;M<m;M++){
    															f=E[M];
    															h=""+r+u+g+q;
    															D[h]instanceof Array||(D[h]=[]);
    															D[h].push(""+c+a+e+f);
    														}
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else {
    			layer.msg("任"+b+"直选错误",{icon: 5});
    		}
    		return D
    	},
    	anyGroup:function(b,d,c,a){
    		var e,f;
    		null==b&&(b=2);
    		null==d&&(d=2);
    		null==c&&(c=[]);
    		null==a&&(a=['万','千','百','十','个']);
    		f=[];
    		b=parseInt(b);
    		d=parseInt(d);
    		c instanceof Array||(c=c.split(","));
    		if(a.length<b){
    			layer.msg("组选复式参数错误",{icon: 5});
    			return false;
    		}
    		c=this.generatorGroup(d,c);
    		e=this.generatorGroup(b,a);
    		b=0;
    		for(a=e.length;b<a;b++){
    			d=e[b];
    			f[d]=c;
    		}
    		return f
    	},
    	generatorGroup:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G;
    		null==b&&(b=2);
    		null==d&&(d=[]);
    		G=[];
    		b=parseInt(b);
    		d=d instanceof Array?d.slice():d.splt(",");
    		if(1===b)return d;
    		if(2===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=0,p=d.length;l<p;l++)
    					u=d[l],G.push(""+r+u);
    		else if(3===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=n=0,q=d.length;0<=q?n<q:n>q;l=0<=q?++n:--n)
    					for(u=d[l],p=m=k=l+1,f=d.length;k<=f?m<f:m>f;p=k<=f?++m:--m)l=d[p],G.push(""+r+u+l);
    		else if(4===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=q=0,m=d.length-2;0<=m?q<m:q>m;l=0<=m?++q:--q)
    					for(u=d[l],p=k=f=l+1,w=d.length-1;f<=w?k<w:k>w;p=f<=w?++k:--k)
    						for(l=d[p],n=g=v=p+1,y=d.length;v<=y?g<y:g>y;n=v<=y?++g:--g)p=d[n],G.push(""+r+u+l+p);
    		else if(5===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=m=0,k=d.length-3;0<=k?m<k:m>k;l=0<=k?++m:--m)
    					for(u=d[l],p=f=g=l+1,w=d.length-2;g<=w?f<w:f>w;p=g<=w?++f:--f)
    						for(l=d[p],n=v=y=p+1,h=d.length-1;y<=h?v<h:v>h;n=y<=h?++v:--v)
    							for(p=d[n],q=x=z=n+1,c=d.length;z<=c?x<c:x>c;q=z<=c?++x:--x)
    								n=d[q],G.push(""+r+u+l+p+n);
    		else if(6===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,k=d.length-4;0<=k?f<k:f>k;l=0<=k?++f:--f)
    					for(u=d[l],p=v=w=l+1,g=d.length-3;w<=g?v<g:v>g;p=w<=g?++v:--v)
    						for(l=d[p],n=x=y=p+1,h=d.length-2;y<=h?x<h:x>h;n=y<=h?++x:--x)
    							for(p=d[n],q=C=z=n+1,c=d.length-1;z<=c?C<c:C>c;q=z<=c?++C:--C)
    								for(n=d[q],m=A=B=q+1,a=d.length;B<=a?A<a:A>a;m=B<=a?++A:--A)q=d[m],G.push(""+r+u+l+p+n+q);
    		else if(7===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=w=0,f=d.length-5;0<=f?w<f:w>f;l=0<=f?++w:--w)
    					for(u=d[l],p=y=g=l+1,v=d.length-4;g<=v?y<v:y>v;p=g<=v?++y:--y)
    						for(l=d[p],n=z=h=p+1,x=d.length-3;h<=x?z<x:z>x;n=h<=x?++z:--z)
    							for(p=d[n],q=c=C=n+1,B=d.length-2;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-1;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										m=d[k],G.push(""+r+u+l+p+n+q+m);
    		else if(8===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,w=d.length-6;0<=w?f<w:f>w;l=0<=w?++f:--f)
    					for(u=d[l],p=g=v=l+1,y=d.length-5;v<=y?g<y:g>y;p=v<=y?++g:--g)
    						for(l=d[p],n=h=x=p+1,z=d.length-4;x<=z?h<z:h>z;n=x<=z?++h:--h)
    							for(p=d[n],q=c=C=n+1,B=d.length-3;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-2;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length-1;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										for(m=d[k],t=k=I=k+1,H=d.length;I<=H?k<H:k>H;t=I<=H?++k:--k)
    											t=d[t],G.push(""+r+u+l+p+n+q+m+t);
    		else layer.msg("长度组选错误"+b,{icon: 5});
    		return G
    	}
	}
}();