function getLocalTime(nS) {  
	var date = new Date(nS);
	var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '+ date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
   return dateFormat;     
}

// 从右边截取i位有效字符
String.prototype.Right = function(i) { // 为String对象增加一个Right方法
	return this.slice(this.length - i, this.length); // 返回值为 以“该字符串长度减i”为起始 到
														// 该字符串末尾 的截取字符串
};

// 数字金额实时转换成大写金额
	function convertCurrency(a) {
    var e, g, c, h, l, m, q, t, r, y, w;
    a = a.toString();
    if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
    a = a.replace(/,/g, "");
    a = a.replace(/^0+/, "");
    if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
    e = a.split(".");
    1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
    c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
    h = ["", "\u62fe", "\u4f70", "\u4edf"];
    l = ["", "\u4e07", "\u4ebf"];
    m = ["", ""];
    g = "";
    if (0 < Number(a)) {
        for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
        g += ""
    }
    if ("" != e)
        for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
    "" == g && (g = "\u96f6");
    "" == e && (g += "");
    return "" + g
}
	
	/**
	 * 将数值四舍五入(保留2位小数)后格式化成金额形式
	 * 
	 * @param num
	 *            数值(Number或者String)
	 * @return 金额格式的字符串,如'1,234,567.45'
	 * @type String
	 */  
	function formatCurrency(num) {  
	    num = num.toString().replace(/\$|\,/g,'');  
	    if(isNaN(num))  
	        num = "0";  
	    sign = (num == (num = Math.abs(num)));  
	    num = Math.floor(num*100+0.50000000001);  
	    cents = num%100;  
	    num = Math.floor(num/100).toString();  
	    if(cents<10)  
	    cents = "0" + cents;  
	    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)  
	    num = num.substring(0,num.length-(4*i+3))+','+  
	    num.substring(num.length-(4*i+3));  
	    return (((sign)?'':'-') + num + '.' + cents);  
	} 
	
	//验证姓名不能为空且一定为中文
	function checkName(rows,msg){
        if(rows.length!=0){
        	if(rows.match(/^[\u4e00-\u9fa5]+$/))
        		return true;
        	else 
        		layer.alert('请输入中文!');
        		return false;
        	}else{
        		layer.alert(msg);
        		return false;
        	}
        }
	
	//分页页数处理
    function pageChuLi(r,page,name){
    	var options = "";
    	//当前页数
    	$("."+name+"DqPage").attr("link",page);
    	$("."+name+"DqPage").text("当前页次"+page);
    	$("#"+name+"PaginationInfo").text("每页 10 条纪录 / 共 "+r.totalCount+" 条 / 共 "+r.totalPageCount+" 页");
    	var li = $("#"+name+"PaginationBar").find("li");
    	if(parseInt(page) == 1){
    		li.eq(2).removeClass("disabled");
    		li.eq(3).removeClass("disabled");
    		li.eq(0).addClass("disabled");
    		li.eq(1).addClass("disabled");
    	}
    	if(parseInt(page) > 1){
    		li.eq(0).removeClass("disabled");
    		li.eq(1).removeClass("disabled");
    	}
    	if(parseInt(page) > 1){
    		li.eq(2).removeClass("disabled");
    		li.eq(3).removeClass("disabled");
    	}
    	if( parseInt(page)==r.totalPageCount){
    		li.eq(2).addClass("disabled");
    		li.eq(3).addClass("disabled");
    	}
    	//上一页
    	li.eq(1).find("a").attr("link",parseInt(page)-1);
    	//下一页
    	li.eq(2).find("a").attr("link",parseInt(page)+1);
    	//最终页
    	li.eq(3).find("a").attr("link",r.totalPageCount);
		if(page == 1){
			for(var i=1;i<=r.totalPageCount;i++){
				options +='<option value="'+i+'">'+i+'</option>';
			}
			$("."+name+"Options").html(options);
		}
    }