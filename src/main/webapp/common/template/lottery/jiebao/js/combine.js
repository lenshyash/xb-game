/*! jQuery v1.12.3 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.3",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(!l.ownFirst)for(b in a)return k.call(a,b);for(b in a);return void 0===b||k.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(h)return h.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))},d.guid=a.guid=a.guid||n.guid++,d):void 0},now:function(){return+new Date},support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2])return A.find(a);this.length=1,this[0]=f}return this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=!0,c||j.disable(),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll)a.setTimeout(n.ready);else if(d.addEventListener)d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K);else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&d.documentElement}catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")}catch(b){return a.setTimeout(f,50)}J(),n.ready()}}()}return I.promise(b)},n.ready.promise();var L;for(L in n(l))break;l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))}),function(){var a=d.createElement("div");l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}a=null}();var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b},N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0;
}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),"object"!=typeof b&&"function"!=typeof b||(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}}),function(){var a;l.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,e;return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0}}();var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)Y(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)}();var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function fa(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}var ga=/<|&#?\w+;/,ha=/<tbody/i;function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)}function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;o>r;r++)if(g=a[r],g||0===g)if("object"===n.type(g))n.merge(q,g.nodeType?[g]:g);else if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];while(f--)i=i.lastChild;if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;while(f--)n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)}n.merge(q,i.childNodes),i.textContent="";while(i.firstChild)i.removeChild(i.firstChild);i=p.lastChild}else q.push(b.createTextNode(g));i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;while(g=q[r++])if(d&&n.inArray(g,d)>-1)e&&e.push(g);else if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;while(g=i[f++])_.test(g.type||"")&&c.push(g)}return i=null,p}!function(){var b,c,e=d.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1);e=null}();var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;function pa(){return!0}function qa(){return!1}function ra(){try{return d.activeElement}catch(a){}}function sa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)sa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=qa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(G)||[""],h=b.length;while(h--)f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);i;i=i.parentNode)p.push(i),m=i;m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)}o=0;while((i=p[o++])&&!b.isPropagationStopped())b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault());if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;try{e[q]()}catch(s){}n.event.triggered=void 0,m&&(e[h]=m)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)}:function(a,b,c){var d="on"+b;a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0}),n._data(c,"submit",!0))})},postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?("checkbox"!==this.type&&"radio"!==this.type||(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)}),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)}),n._data(b,"change",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)}}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)},one:function(a,b,c,d){return sa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function Ea(a){var b=ya.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Ga(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}}function Ha(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;o>m;m++)g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m);if(h)for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;h>m;m++)g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")));k=e=null}return a}function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=ea(f),h=ea(a),g=0;null!=(e=h[g]);++g)d[g]&&Ga(e,d[g]);if(b)if(c)for(h=h||ea(a),d=d||ea(f),g=0;null!=(e=h[g]);g++)Fa(e,d[g]);else Fa(a,f);return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;null!=(d=a[h]);h++)if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(ea(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0;if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get());return this.pushStack(e)}});var Ja,Ka={HTML:"block",BODY:"block"};function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function Ma(a){var b=d,c=Ka[a];return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c}var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Qa=d.documentElement;!function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f},boxSizingReliable:function(){return null==b&&k(),e},pixelMarginRight:function(){return null==b&&k(),c},pixelPosition:function(){return null==b&&k(),b},reliableMarginRight:function(){return null==b&&k(),g},reliableMarginLeft:function(){return null==b&&k(),h}});function k(){var k,l,m=d.documentElement;m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)}}}();var Ra,Sa,Ta=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0===g?g:g+""}):Qa.currentStyle&&(Ra=function(a){return a.currentStyle},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;function bb(a){if(a in ab)return a;var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;while(c--)if(a=_a[c]+b,a in ab)return a}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function db(a,b,c){var d=Ya.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)));return g}function fb(b,c,e){var f=!0,g="width"===c?b.offsetWidth:b.offsetHeight,h=Ra(b),i=l.boxSizing&&"border-box"===n.css(b,"boxSizing",!1,h);if(d.msFullscreenElement&&a.top!==a&&b.getClientRects().length&&(g=Math.round(100*b.getBoundingClientRect()[c])),0>=g||null==g){if(g=Sa(b,c,h),(0>g||null==g)&&(g=b.style[c]),Oa.test(g))return g;f=i&&(l.boxSizingReliable()||g===b.style[c]),g=parseFloat(g)||0}return g+eb(b,c,e||(i?"border":"content"),f,h)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)}):fb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ra(a);return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)}}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0}),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){
return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}):0))+"px":void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+V[d]+b]=f[d]||f[d-2]||f[0];return e}},Na.test(a)||(n.cssHooks[a+b].set=db)}),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ra(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return cb(this,!0)},hide:function(){return cb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()})}});function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)}n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=gb.propHooks[this.prop];return a&&a.get?a.get(this):gb.propHooks._default.get(this)},run:function(a){var b,c=gb.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this}},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;function lb(){return a.setTimeout(function(){hb=void 0}),hb=n.now()}function mb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=V[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(o))"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function pb(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(pb(k,j.opts.specialEasing);g>f;f++)if(d=qb.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return X(c.elem,a,U.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&kb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)}}),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(hb=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),hb=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(ib),ib=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value}();var rb=/\r/g,sb=/[\x20\t\r\n\f]+/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a)).replace(sb," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>-1)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var tb,ub,vb=n.expr.attrHandle,wb=/^(?:checked|selected)$/i,xb=l.getSetAttribute,yb=l.input;n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ub:tb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?yb&&xb||!wb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(xb?c:d)}}),ub={set:function(a,b,c){return b===!1?n.removeAttr(a,c):yb&&xb||!wb.test(c)?a.setAttribute(!xb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=vb[b]||n.find.attr;yb&&xb||!wb.test(b)?vb[b]=function(a,b,d){var e,f;return d||(f=vb[b],vb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,vb[b]=f),e}:vb[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),yb&&xb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):tb&&tb.set(a,b,c)}}),xb||(tb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},vb.id=vb.name=vb.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:tb.set},n.attrHooks.contenteditable={set:function(a,b,c){tb.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var zb=/^(?:input|select|textarea|button|object)$/i,Ab=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):zb.test(a.nodeName)||Ab.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var Bb=/[\t\r\n\f]/g;function Cb(a){return n.attr(a,"class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,Cb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,Cb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Cb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=Cb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+Cb(c)+" ").replace(Bb," ").indexOf(b)>-1)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Db=a.location,Eb=n.now(),Fb=/\?/,Gb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(Gb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var Hb=/#.*$/,Ib=/([?&])_=[^&]*/,Jb=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Kb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Lb=/^(?:GET|HEAD)$/,Mb=/^\/\//,Nb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Ob={},Pb={},Qb="*/".concat("*"),Rb=Db.href,Sb=Nb.exec(Rb.toLowerCase())||[];function Tb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Ub(a,b,c,d){var e={},f=a===Pb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Vb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Wb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Xb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Rb,type:"GET",isLocal:Kb.test(Sb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Qb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Vb(Vb(a,n.ajaxSettings),b):Vb(n.ajaxSettings,a)},ajaxPrefilter:Tb(Ob),ajaxTransport:Tb(Pb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;if(2===u){if(!k){k={};while(b=Jb.exec(g))k[b[1].toLowerCase()]=b[2]}b=k[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===u?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return u||(a=t[c]=t[c]||a,s[a]=b),this},overrideMimeType:function(a){return u||(l.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>u)for(b in a)r[b]=[r[b],a[b]];else w.always(a[w.status]);return this},abort:function(a){var b=a||v;return j&&j.abort(b),y(0,b),this}};if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Rb)+"").replace(Hb,"").replace(Mb,Sb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Nb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Sb[1]&&d[2]===Sb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Sb[3]||("http:"===Sb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Ub(Ob,l,c,w),2===u)return w;i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Lb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Fb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Ib.test(f)?f.replace(Ib,"$1_="+Eb++):f+(Fb.test(f)?"&":"?")+"_="+Eb++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Qb+"; q=0.01":""):l.accepts["*"]);for(e in l.headers)w.setRequestHeader(e,l.headers[e]);if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u))return w.abort();v="abort";for(e in{success:1,error:1,complete:1})w[e](l[e]);if(j=Ub(Pb,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u)return w;l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")},l.timeout));try{u=1,j.send(s,y)}catch(x){if(!(2>u))throw x;y(-1,x)}}else y(-1,"No Transport");function y(b,c,d,e){var k,s,t,v,x,y=c;2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Wb(l,w,d)),v=Xb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))}return w},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}});function Yb(a){return a.style&&a.style.display||n.css(a,"display")}function Zb(a){while(a&&1===a.nodeType){if("none"===Yb(a)||"hidden"===a.type)return!0;a=a.parentNode}return!1}n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Zb(a)},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var $b=/%20/g,_b=/\[\]$/,ac=/\r?\n/g,bc=/^(?:submit|button|image|reset|file)$/i,cc=/^(?:input|select|textarea|keygen)/i;function dc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||_b.test(a)?d(a,e):dc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)dc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)dc(c,a[c],b,e);return d.join("&").replace($b,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&cc.test(this.nodeName)&&!bc.test(a)&&(this.checked||!Z.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(ac,"\r\n")}}):{name:b.name,value:c.replace(ac,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?ic():d.documentMode>8?hc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&hc()||ic()}:hc;var ec=0,fc={},gc=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in fc)fc[a](void 0,!0)}),l.cors=!!gc&&"withCredentials"in gc,gc=l.ajax=!!gc,gc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;return{send:function(d,e){var f,g=b.xhr(),h=++ec;if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(f in b.xhrFields)g[f]=b.xhrFields[f];b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");for(f in d)void 0!==d[f]&&g.setRequestHeader(f,d[f]+"");g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;if(c&&(d||4===g.readyState))if(delete fc[h],c=void 0,g.onreadystatechange=n.noop,d)4!==g.readyState&&g.abort();else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);try{i=g.statusText}catch(k){i=""}f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404}j&&e(f,i,j,g.getAllResponseHeaders())},b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=fc[h]=c:c()},abort:function(){c&&c(void 0,!0)}}}});function hc(){try{return new a.XMLHttpRequest}catch(b){}}function ic(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var jc=[],kc=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=jc.pop()||n.expando+"_"+Eb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(kc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&kc.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(kc,"$1"+e):b.jsonp!==!1&&(b.url+=(Fb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,jc.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||d;var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var lc=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&lc)return lc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function mc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=mc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0),c.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Qa})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return Y(this,function(a,d,e){var f=mc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0;
})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return Y(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var nc=a.jQuery,oc=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=oc),b&&a.jQuery===n&&(a.jQuery=nc),n},b||(a.jQuery=a.$=n),n});

/*!
 * JavaScript Cookie v2.1.1
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	if (typeof define === 'function' && define.amd) {
		define(factory);
	} else if (typeof exports === 'object') {
		module.exports = factory();
	} else {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				return (document.cookie = [
					key, '=', value,
					attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
					attributes.path    && '; path=' + attributes.path,
					attributes.domain  && '; domain=' + attributes.domain,
					attributes.secure ? '; secure' : ''
				].join(''));
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var name = parts[0].replace(rdecode, decodeURIComponent);
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api(key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));


/*! layer-v2.3 弹层组件 License LGPL  http://layer.layui.com/ By 贤心 */
;!function(a,b){"use strict";var c,d,e={getPath:function(){var a=document.scripts,b=a[a.length-1],c=b.src;if(!b.getAttribute("merge"))return c.substring(0,c.lastIndexOf("/")+1)}(),enter:function(a){13===a.keyCode&&a.preventDefault()},config:{},end:{},btn:["&#x786E;&#x5B9A;","&#x53D6;&#x6D88;"],type:["dialog","page","iframe","loading","tips"]},f={v:"2.3",ie6:!!a.ActiveXObject&&!a.XMLHttpRequest,index:0,path:e.getPath,config:function(a,b){var d=0;return a=a||{},f.cache=e.config=c.extend(e.config,a),f.path=e.config.path||f.path,"string"==typeof a.extend&&(a.extend=[a.extend]),f.use("skin/layer.css",a.extend&&a.extend.length>0?function g(){var c=a.extend;f.use(c[c[d]?d:d-1],d<c.length?function(){return++d,g}():b)}():b),this},use:function(a,b,d){var e=c("head")[0],a=a.replace(/\s/g,""),g=/\.css$/.test(a),h=document.createElement(g?"link":"script"),i="layui_layer_"+a.replace(/\.|\//g,"");return f.path?(g&&(h.rel="stylesheet"),h[g?"href":"src"]=/^http:\/\//.test(a)?a:f.path+a,h.id=i,c("#"+i)[0]||e.appendChild(h),function j(){(g?1989===parseInt(c("#"+i).css("width")):f[d||i])?function(){b&&b();try{g||e.removeChild(h)}catch(a){}}():setTimeout(j,100)}(),this):void 0},ready:function(a,b){var d="function"==typeof a;return d&&(b=a),f.config(c.extend(e.config,function(){return d?{}:{path:a}}()),b),this},alert:function(a,b,d){var e="function"==typeof b;return e&&(d=b),f.open(c.extend({content:a,yes:d},e?{}:b))},confirm:function(a,b,d,g){var h="function"==typeof b;return h&&(g=d,d=b),f.open(c.extend({content:a,btn:e.btn,yes:d,btn2:g},h?{}:b))},msg:function(a,d,g){var i="function"==typeof d,j=e.config.skin,k=(j?j+" "+j+"-msg":"")||"layui-layer-msg",l=h.anim.length-1;return i&&(g=d),f.open(c.extend({content:a,time:3e3,shade:!1,skin:k,title:!1,closeBtn:!1,btn:!1,end:g},i&&!e.config.skin?{skin:k+" layui-layer-hui",shift:l}:function(){return d=d||{},(-1===d.icon||d.icon===b&&!e.config.skin)&&(d.skin=k+" "+(d.skin||"layui-layer-hui")),d}()))},load:function(a,b){return f.open(c.extend({type:3,icon:a||0,shade:.01},b))},tips:function(a,b,d){return f.open(c.extend({type:4,content:[a,b],closeBtn:!1,time:3e3,shade:!1,maxWidth:210},d))}},g=function(a){var b=this;b.index=++f.index,b.config=c.extend({},b.config,e.config,a),b.creat()};g.pt=g.prototype;var h=["layui-layer",".layui-layer-title",".layui-layer-main",".layui-layer-dialog","layui-layer-iframe","layui-layer-content","layui-layer-btn","layui-layer-close"];h.anim=["layer-anim","layer-anim-01","layer-anim-02","layer-anim-03","layer-anim-04","layer-anim-05","layer-anim-06"],g.pt.config={type:0,shade:.3,fix:!0,move:h[1],title:"&#x4FE1;&#x606F;",offset:"auto",area:"auto",closeBtn:1,time:0,zIndex:19891014,maxWidth:360,shift:0,icon:-1,scrollbar:!0,tips:2},g.pt.vessel=function(a,b){var c=this,d=c.index,f=c.config,g=f.zIndex+d,i="object"==typeof f.title,j=f.maxmin&&(1===f.type||2===f.type),k=f.title?'<div class="layui-layer-title" style="'+(i?f.title[1]:"")+'">'+(i?f.title[0]:f.title)+"</div>":"";return f.zIndex=g,b([f.shade?'<div class="layui-layer-shade" id="layui-layer-shade'+d+'" times="'+d+'" style="'+("z-index:"+(g-1)+"; background-color:"+(f.shade[1]||"#000")+"; opacity:"+(f.shade[0]||f.shade)+"; filter:alpha(opacity="+(100*f.shade[0]||100*f.shade)+");")+'"></div>':"",'<div class="'+h[0]+" "+(h.anim[f.shift]||"")+(" layui-layer-"+e.type[f.type])+(0!=f.type&&2!=f.type||f.shade?"":" layui-layer-border")+" "+(f.skin||"")+'" id="'+h[0]+d+'" type="'+e.type[f.type]+'" times="'+d+'" showtime="'+f.time+'" conType="'+(a?"object":"string")+'" style="z-index: '+g+"; width:"+f.area[0]+";height:"+f.area[1]+(f.fix?"":";position:absolute;")+'">'+(a&&2!=f.type?"":k)+'<div id="'+(f.id||"")+'" class="layui-layer-content'+(0==f.type&&-1!==f.icon?" layui-layer-padding":"")+(3==f.type?" layui-layer-loading"+f.icon:"")+'">'+(0==f.type&&-1!==f.icon?'<i class="layui-layer-ico layui-layer-ico'+f.icon+'"></i>':"")+(1==f.type&&a?"":f.content||"")+'</div><span class="layui-layer-setwin">'+function(){var a=j?'<a class="layui-layer-min" href="javascript:;"><cite></cite></a><a class="layui-layer-ico layui-layer-max" href="javascript:;"></a>':"";return f.closeBtn&&(a+='<a class="layui-layer-ico '+h[7]+" "+h[7]+(f.title?f.closeBtn:4==f.type?"1":"2")+'" href="javascript:;"></a>'),a}()+"</span>"+(f.btn?function(){var a="";"string"==typeof f.btn&&(f.btn=[f.btn]);for(var b=0,c=f.btn.length;c>b;b++)a+='<a class="'+h[6]+b+'">'+f.btn[b]+"</a>";return'<div class="'+h[6]+'">'+a+"</div>"}():"")+"</div>"],k),c},g.pt.creat=function(){var a=this,b=a.config,g=a.index,i=b.content,j="object"==typeof i;if(!c("#"+b.id)[0]){switch("string"==typeof b.area&&(b.area="auto"===b.area?["",""]:[b.area,""]),b.type){case 0:b.btn="btn"in b?b.btn:e.btn[0],f.closeAll("dialog");break;case 2:var i=b.content=j?b.content:[b.content||"http://layer.layui.com","auto"];b.content='<iframe scrolling="'+(b.content[1]||"auto")+'" allowtransparency="true" id="'+h[4]+g+'" name="'+h[4]+g+'" onload="this.className=\'\';" class="layui-layer-load" frameborder="0" src="'+b.content[0]+'"></iframe>';break;case 3:b.title=!1,b.closeBtn=!1,-1===b.icon&&0===b.icon,f.closeAll("loading");break;case 4:j||(b.content=[b.content,"body"]),b.follow=b.content[1],b.content=b.content[0]+'<i class="layui-layer-TipsG"></i>',b.title=!1,b.fix=!1,b.tips="object"==typeof b.tips?b.tips:[b.tips,!0],b.tipsMore||f.closeAll("tips")}a.vessel(j,function(d,e){c("body").append(d[0]),j?function(){2==b.type||4==b.type?function(){c("body").append(d[1])}():function(){i.parents("."+h[0])[0]||(i.show().addClass("layui-layer-wrap").wrap(d[1]),c("#"+h[0]+g).find("."+h[5]).before(e))}()}():c("body").append(d[1]),a.layero=c("#"+h[0]+g),b.scrollbar||h.html.css("overflow","hidden").attr("layer-full",g)}).auto(g),2==b.type&&f.ie6&&a.layero.find("iframe").attr("src",i[0]),c(document).off("keydown",e.enter).on("keydown",e.enter),a.layero.on("keydown",function(a){c(document).off("keydown",e.enter)}),4==b.type?a.tips():a.offset(),b.fix&&d.on("resize",function(){a.offset(),(/^\d+%$/.test(b.area[0])||/^\d+%$/.test(b.area[1]))&&a.auto(g),4==b.type&&a.tips()}),b.time<=0||setTimeout(function(){f.close(a.index)},b.time),a.move().callback()}},g.pt.auto=function(a){function b(a){a=g.find(a),a.height(i[1]-j-k-2*(0|parseFloat(a.css("padding"))))}var e=this,f=e.config,g=c("#"+h[0]+a);""===f.area[0]&&f.maxWidth>0&&(/MSIE 7/.test(navigator.userAgent)&&f.btn&&g.width(g.innerWidth()),g.outerWidth()>f.maxWidth&&g.width(f.maxWidth));var i=[g.innerWidth(),g.innerHeight()],j=g.find(h[1]).outerHeight()||0,k=g.find("."+h[6]).outerHeight()||0;switch(f.type){case 2:b("iframe");break;default:""===f.area[1]?f.fix&&i[1]>=d.height()&&(i[1]=d.height(),b("."+h[5])):b("."+h[5])}return e},g.pt.offset=function(){var a=this,b=a.config,c=a.layero,e=[c.outerWidth(),c.outerHeight()],f="object"==typeof b.offset;a.offsetTop=(d.height()-e[1])/2,a.offsetLeft=(d.width()-e[0])/2,f?(a.offsetTop=b.offset[0],a.offsetLeft=b.offset[1]||a.offsetLeft):"auto"!==b.offset&&(a.offsetTop=b.offset,"rb"===b.offset&&(a.offsetTop=d.height()-e[1],a.offsetLeft=d.width()-e[0])),b.fix||(a.offsetTop=/%$/.test(a.offsetTop)?d.height()*parseFloat(a.offsetTop)/100:parseFloat(a.offsetTop),a.offsetLeft=/%$/.test(a.offsetLeft)?d.width()*parseFloat(a.offsetLeft)/100:parseFloat(a.offsetLeft),a.offsetTop+=d.scrollTop(),a.offsetLeft+=d.scrollLeft()),c.css({top:a.offsetTop,left:a.offsetLeft})},g.pt.tips=function(){var a=this,b=a.config,e=a.layero,f=[e.outerWidth(),e.outerHeight()],g=c(b.follow);g[0]||(g=c("body"));var i={width:g.outerWidth(),height:g.outerHeight(),top:g.offset().top,left:g.offset().left},j=e.find(".layui-layer-TipsG"),k=b.tips[0];b.tips[1]||j.remove(),i.autoLeft=function(){i.left+f[0]-d.width()>0?(i.tipLeft=i.left+i.width-f[0],j.css({right:12,left:"auto"})):i.tipLeft=i.left},i.where=[function(){i.autoLeft(),i.tipTop=i.top-f[1]-10,j.removeClass("layui-layer-TipsB").addClass("layui-layer-TipsT").css("border-right-color",b.tips[1])},function(){i.tipLeft=i.left+i.width+10,i.tipTop=i.top,j.removeClass("layui-layer-TipsL").addClass("layui-layer-TipsR").css("border-bottom-color",b.tips[1])},function(){i.autoLeft(),i.tipTop=i.top+i.height+10,j.removeClass("layui-layer-TipsT").addClass("layui-layer-TipsB").css("border-right-color",b.tips[1])},function(){i.tipLeft=i.left-f[0]-10,i.tipTop=i.top,j.removeClass("layui-layer-TipsR").addClass("layui-layer-TipsL").css("border-bottom-color",b.tips[1])}],i.where[k-1](),1===k?i.top-(d.scrollTop()+f[1]+16)<0&&i.where[2]():2===k?d.width()-(i.left+i.width+f[0]+16)>0||i.where[3]():3===k?i.top-d.scrollTop()+i.height+f[1]+16-d.height()>0&&i.where[0]():4===k&&f[0]+16-i.left>0&&i.where[1](),e.find("."+h[5]).css({"background-color":b.tips[1],"padding-right":b.closeBtn?"30px":""}),e.css({left:i.tipLeft,top:i.tipTop})},g.pt.move=function(){var a=this,b=a.config,e={setY:0,moveLayer:function(){var a=e.layero,b=parseInt(a.css("margin-left")),c=parseInt(e.move.css("left"));0===b||(c-=b),"fixed"!==a.css("position")&&(c-=a.parent().offset().left,e.setY=0),a.css({left:c,top:parseInt(e.move.css("top"))-e.setY})}},f=a.layero.find(b.move);return b.move&&f.attr("move","ok"),f.css({cursor:b.move?"move":"auto"}),c(b.move).on("mousedown",function(a){if(a.preventDefault(),"ok"===c(this).attr("move")){e.ismove=!0,e.layero=c(this).parents("."+h[0]);var f=e.layero.offset().left,g=e.layero.offset().top,i=e.layero.outerWidth()-6,j=e.layero.outerHeight()-6;c("#layui-layer-moves")[0]||c("body").append('<div id="layui-layer-moves" class="layui-layer-moves" style="left:'+f+"px; top:"+g+"px; width:"+i+"px; height:"+j+'px; z-index:2147483584"></div>'),e.move=c("#layui-layer-moves"),b.moveType&&e.move.css({visibility:"hidden"}),e.moveX=a.pageX-e.move.position().left,e.moveY=a.pageY-e.move.position().top,"fixed"!==e.layero.css("position")||(e.setY=d.scrollTop())}}),c(document).mousemove(function(a){if(e.ismove){var c=a.pageX-e.moveX,f=a.pageY-e.moveY;if(a.preventDefault(),!b.moveOut){e.setY=d.scrollTop();var g=d.width()-e.move.outerWidth(),h=e.setY;0>c&&(c=0),c>g&&(c=g),h>f&&(f=h),f>d.height()-e.move.outerHeight()+e.setY&&(f=d.height()-e.move.outerHeight()+e.setY)}e.move.css({left:c,top:f}),b.moveType&&e.moveLayer(),c=f=g=h=null}}).mouseup(function(){try{e.ismove&&(e.moveLayer(),e.move.remove(),b.moveEnd&&b.moveEnd()),e.ismove=!1}catch(a){e.ismove=!1}}),a},g.pt.callback=function(){function a(){var a=g.cancel&&g.cancel(b.index,d);a===!1||f.close(b.index)}var b=this,d=b.layero,g=b.config;b.openLayer(),g.success&&(2==g.type?d.find("iframe").on("load",function(){g.success(d,b.index)}):g.success(d,b.index)),f.ie6&&b.IE6(d),d.find("."+h[6]).children("a").on("click",function(){var a=c(this).index();if(0===a)g.yes?g.yes(b.index,d):g.btn1?g.btn1(b.index,d):f.close(b.index);else{var e=g["btn"+(a+1)]&&g["btn"+(a+1)](b.index,d);e===!1||f.close(b.index)}}),d.find("."+h[7]).on("click",a),g.shadeClose&&c("#layui-layer-shade"+b.index).on("click",function(){f.close(b.index)}),d.find(".layui-layer-min").on("click",function(){f.min(b.index,g),g.min&&g.min(d)}),d.find(".layui-layer-max").on("click",function(){c(this).hasClass("layui-layer-maxmin")?(f.restore(b.index),g.restore&&g.restore(d)):(f.full(b.index,g),g.full&&g.full(d))}),g.end&&(e.end[b.index]=g.end)},e.reselect=function(){c.each(c("select"),function(a,b){var d=c(this);d.parents("."+h[0])[0]||1==d.attr("layer")&&c("."+h[0]).length<1&&d.removeAttr("layer").show(),d=null})},g.pt.IE6=function(a){function b(){a.css({top:f+(e.config.fix?d.scrollTop():0)})}var e=this,f=a.offset().top;b(),d.scroll(b),c("select").each(function(a,b){var d=c(this);d.parents("."+h[0])[0]||"none"===d.css("display")||d.attr({layer:"1"}).hide(),d=null})},g.pt.openLayer=function(){var a=this;f.zIndex=a.config.zIndex,f.setTop=function(a){var b=function(){f.zIndex++,a.css("z-index",f.zIndex+1)};return f.zIndex=parseInt(a[0].style.zIndex),a.on("mousedown",b),f.zIndex}},e.record=function(a){var b=[a.outerWidth(),a.outerHeight(),a.position().top,a.position().left+parseFloat(a.css("margin-left"))];a.find(".layui-layer-max").addClass("layui-layer-maxmin"),a.attr({area:b})},e.rescollbar=function(a){h.html.attr("layer-full")==a&&(h.html[0].style.removeProperty?h.html[0].style.removeProperty("overflow"):h.html[0].style.removeAttribute("overflow"),h.html.removeAttr("layer-full"))},a.layer=f,f.getChildFrame=function(a,b){return b=b||c("."+h[4]).attr("times"),c("#"+h[0]+b).find("iframe").contents().find(a)},f.getFrameIndex=function(a){return c("#"+a).parents("."+h[4]).attr("times")},f.iframeAuto=function(a){if(a){var b=f.getChildFrame("html",a).outerHeight(),d=c("#"+h[0]+a),e=d.find(h[1]).outerHeight()||0,g=d.find("."+h[6]).outerHeight()||0;d.css({height:b+e+g}),d.find("iframe").css({height:b})}},f.iframeSrc=function(a,b){c("#"+h[0]+a).find("iframe").attr("src",b)},f.style=function(a,b){var d=c("#"+h[0]+a),f=d.attr("type"),g=d.find(h[1]).outerHeight()||0,i=d.find("."+h[6]).outerHeight()||0;(f===e.type[1]||f===e.type[2])&&(d.css(b),f===e.type[2]&&d.find("iframe").css({height:parseFloat(b.height)-g-i}))},f.min=function(a,b){var d=c("#"+h[0]+a),g=d.find(h[1]).outerHeight()||0;e.record(d),f.style(a,{width:180,height:g,overflow:"hidden"}),d.find(".layui-layer-min").hide(),"page"===d.attr("type")&&d.find(h[4]).hide(),e.rescollbar(a)},f.restore=function(a){var b=c("#"+h[0]+a),d=b.attr("area").split(",");b.attr("type");f.style(a,{width:parseFloat(d[0]),height:parseFloat(d[1]),top:parseFloat(d[2]),left:parseFloat(d[3]),overflow:"visible"}),b.find(".layui-layer-max").removeClass("layui-layer-maxmin"),b.find(".layui-layer-min").show(),"page"===b.attr("type")&&b.find(h[4]).show(),e.rescollbar(a)},f.full=function(a){var b,g=c("#"+h[0]+a);e.record(g),h.html.attr("layer-full")||h.html.css("overflow","hidden").attr("layer-full",a),clearTimeout(b),b=setTimeout(function(){var b="fixed"===g.css("position");f.style(a,{top:b?0:d.scrollTop(),left:b?0:d.scrollLeft(),width:d.width(),height:d.height()}),g.find(".layui-layer-min").hide()},100)},f.title=function(a,b){var d=c("#"+h[0]+(b||f.index)).find(h[1]);d.html(a)},f.close=function(a){var b=c("#"+h[0]+a),d=b.attr("type");if(b[0]){if(d===e.type[1]&&"object"===b.attr("conType")){b.children(":not(."+h[5]+")").remove();for(var g=0;2>g;g++)b.find(".layui-layer-wrap").unwrap().hide()}else{if(d===e.type[2])try{var i=c("#"+h[4]+a)[0];i.contentWindow.document.write(""),i.contentWindow.close(),b.find("."+h[5])[0].removeChild(i)}catch(j){}b[0].innerHTML="",b.remove()}c("#layui-layer-moves, #layui-layer-shade"+a).remove(),f.ie6&&e.reselect(),e.rescollbar(a),c(document).off("keydown",e.enter),"function"==typeof e.end[a]&&e.end[a](),delete e.end[a]}},f.closeAll=function(a){c.each(c("."+h[0]),function(){var b=c(this),d=a?b.attr("type")===a:1;d&&f.close(b.attr("times")),d=null})};var i=f.cache||{},j=function(a){return i.skin?" "+i.skin+" "+i.skin+"-"+a:""};f.prompt=function(a,b){a=a||{},"function"==typeof a&&(b=a);var d,e=2==a.formType?'<textarea class="layui-layer-input">'+(a.value||"")+"</textarea>":function(){return'<input type="'+(1==a.formType?"password":"text")+'" class="layui-layer-input" value="'+(a.value||"")+'">'}();return f.open(c.extend({btn:["&#x786E;&#x5B9A;","&#x53D6;&#x6D88;"],content:e,skin:"layui-layer-prompt"+j("prompt"),success:function(a){d=a.find(".layui-layer-input"),d.focus()},yes:function(c){var e=d.val();""===e?d.focus():e.length>(a.maxlength||500)?f.tips("&#x6700;&#x591A;&#x8F93;&#x5165;"+(a.maxlength||500)+"&#x4E2A;&#x5B57;&#x6570;",d,{tips:1}):b&&b(e,c,d)}},a))},f.tab=function(a){a=a||{};var b=a.tab||{};return f.open(c.extend({type:1,skin:"layui-layer-tab"+j("tab"),title:function(){var a=b.length,c=1,d="";if(a>0)for(d='<span class="layui-layer-tabnow">'+b[0].title+"</span>";a>c;c++)d+="<span>"+b[c].title+"</span>";return d}(),content:'<ul class="layui-layer-tabmain">'+function(){var a=b.length,c=1,d="";if(a>0)for(d='<li class="layui-layer-tabli xubox_tab_layer">'+(b[0].content||"no content")+"</li>";a>c;c++)d+='<li class="layui-layer-tabli">'+(b[c].content||"no  content")+"</li>";return d}()+"</ul>",success:function(b){var d=b.find(".layui-layer-title").children(),e=b.find(".layui-layer-tabmain").children();d.on("mousedown",function(b){b.stopPropagation?b.stopPropagation():b.cancelBubble=!0;var d=c(this),f=d.index();d.addClass("layui-layer-tabnow").siblings().removeClass("layui-layer-tabnow"),e.eq(f).show().siblings().hide(),"function"==typeof a.change&&a.change(f)})}},a))},f.photos=function(b,d,e){function g(a,b,c){var d=new Image;return d.src=a,d.complete?b(d):(d.onload=function(){d.onload=null,b(d)},void(d.onerror=function(a){d.onerror=null,c(a)}))}var h={};if(b=b||{},b.photos){var i=b.photos.constructor===Object,k=i?b.photos:{},l=k.data||[],m=k.start||0;if(h.imgIndex=(0|m)+1,b.img=b.img||"img",i){if(0===l.length)return f.msg("&#x6CA1;&#x6709;&#x56FE;&#x7247;")}else{var n=c(b.photos),o=function(){l=[],n.find(b.img).each(function(a){var b=c(this);b.attr("layer-index",a),l.push({alt:b.attr("alt"),pid:b.attr("layer-pid"),src:b.attr("layer-src")||b.attr("src"),thumb:b.attr("src")})})};if(o(),0===l.length)return;if(d||n.on("click",b.img,function(){var a=c(this),d=a.attr("layer-index");f.photos(c.extend(b,{photos:{start:d,data:l,tab:b.tab},full:b.full}),!0),o()}),!d)return}h.imgprev=function(a){h.imgIndex--,h.imgIndex<1&&(h.imgIndex=l.length),h.tabimg(a)},h.imgnext=function(a,b){h.imgIndex++,h.imgIndex>l.length&&(h.imgIndex=1,b)||h.tabimg(a)},h.keyup=function(a){if(!h.end){var b=a.keyCode;a.preventDefault(),37===b?h.imgprev(!0):39===b?h.imgnext(!0):27===b&&f.close(h.index)}},h.tabimg=function(a){l.length<=1||(k.start=h.imgIndex-1,f.close(h.index),f.photos(b,!0,a))},h.event=function(){h.bigimg.hover(function(){h.imgsee.show()},function(){h.imgsee.hide()}),h.bigimg.find(".layui-layer-imgprev").on("click",function(a){a.preventDefault(),h.imgprev()}),h.bigimg.find(".layui-layer-imgnext").on("click",function(a){a.preventDefault(),h.imgnext()}),c(document).on("keyup",h.keyup)},h.loadi=f.load(1,{shade:"shade"in b?!1:.9,scrollbar:!1}),g(l[m].src,function(d){f.close(h.loadi),h.index=f.open(c.extend({type:1,area:function(){var e=[d.width,d.height],f=[c(a).width()-50,c(a).height()-50];return!b.full&&e[0]>f[0]&&(e[0]=f[0],e[1]=e[0]*d.height/d.width),[e[0]+"px",e[1]+"px"]}(),title:!1,shade:.9,shadeClose:!0,closeBtn:!1,move:".layui-layer-phimg img",moveType:1,scrollbar:!1,moveOut:!0,shift:5*Math.random()|0,skin:"layui-layer-photos"+j("photos"),content:'<div class="layui-layer-phimg"><img src="'+l[m].src+'" alt="'+(l[m].alt||"")+'" layer-pid="'+l[m].pid+'"><div class="layui-layer-imgsee">'+(l.length>1?'<span class="layui-layer-imguide"><a href="javascript:;" class="layui-layer-iconext layui-layer-imgprev"></a><a href="javascript:;" class="layui-layer-iconext layui-layer-imgnext"></a></span>':"")+'<div class="layui-layer-imgbar" style="display:'+(e?"block":"")+'"><span class="layui-layer-imgtit"><a href="javascript:;">'+(l[m].alt||"")+"</a><em>"+h.imgIndex+"/"+l.length+"</em></span></div></div></div>",success:function(a,c){h.bigimg=a.find(".layui-layer-phimg"),h.imgsee=a.find(".layui-layer-imguide,.layui-layer-imgbar"),h.event(a),b.tab&&b.tab(l[m],a)},end:function(){h.end=!0,c(document).off("keyup",h.keyup)}},b))},function(){f.close(h.loadi),f.msg("&#x5F53;&#x524D;&#x56FE;&#x7247;&#x5730;&#x5740;&#x5F02;&#x5E38;<br>&#x662F;&#x5426;&#x7EE7;&#x7EED;&#x67E5;&#x770B;&#x4E0B;&#x4E00;&#x5F20;&#xFF1F;",{time:3e4,btn:["&#x4E0B;&#x4E00;&#x5F20;","&#x4E0D;&#x770B;&#x4E86;"],yes:function(){l.length>1&&h.imgnext(!0,!0)}})})}},e.run=function(){c=jQuery,d=c(a),h.html=c("html"),f.open=function(a){var b=new g(a);return b.index}},"function"==typeof define?define(function(){return e.run(),f}):function(){e.run(),f.use("skin/layer.css")}()}(window);

!function(t,i){"function"==typeof define&&define.amd?define(["$"],i):"object"==typeof exports?module.exports=i():t.Loading=i(window.Zepto||window.jQuery||$)}(this,function(t){var i=function(){};return i.prototype={loadingTpl:'<div class="ui-loading"><div class="ui-loading-mask"></div><i></i></div>',stop:function(){t(this.target);this.loading.remove()},start:function(){var i=this,o=i.target,n=t(o),d=this.loading;d||(d=t(i.loadingTpl),t("body").append(d)),this.loading=d;var e=t(n).outerHeight(),h=t(n).outerWidth();"HTML"==t(o)[0].tagName&&(e=Math.max(t(o).height(),t(window).height()),h=Math.max(t(o).width(),t(window).width())),d.height(e).width(h),d.find("div").height(e).width(h),100>e&&d.find("i").height(e).width(e);var a=t(n).offset();d.css({top:a.top,left:a.left});var s=d.find("i"),g=e,l=h,w=0,f=0;"HTML"==t(o)[0].tagName?(g=t(window).height(),l=t(window).width(),w=(g-s.height())/2+t(window).scrollTop(),f=(l-s.width())/2+t(window).scrollLeft()):(w=(g-s.height())/2,f=(l-s.width())/2),s.css({top:w,left:f})},init:function(t){t=t||{},this.loadingTpl=t.loadingTpl||this.loadingTpl,this.target=t.target||"html",this.bindEvent()},bindEvent:function(){var i=this;t(this.target).on("stop",function(){i.stop()})}},i});
layer.config({skin: 'layui-layer-rim',offset:[200]});
jQuery.extend({  
    browser: function()   
    {  
        var  
        rwebkit = /(webkit)\/([\w.]+)/,  
        ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/,  
        rmsie = /(msie) ([\w.]+)/,  
        rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/,      
        browser = {},  
        ua = window.navigator.userAgent,  
        browserMatch = uaMatch(ua);  
  
        if (browserMatch.browser) {  
            browser[browserMatch.browser] = true;  
            browser.version = browserMatch.version;  
        }  
        return { browser: browser };  
    },  
});  
function uaMatch(ua)   
{  
        ua = ua.toLowerCase();  
  
        var match = rwebkit.exec(ua)  
                    || ropera.exec(ua)  
                    || rmsie.exec(ua)  
                    || ua.indexOf("compatible") < 0 && rmozilla.exec(ua)  
                    || [];  
  
        return {  
            browser : match[1] || "",  
            version : match[2] || "0"  
        };  
};
var bet_result={};
(function($) {
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			},
			successIntercept:function(data){}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}
		if (opt.successIntercept) {
			fn.successIntercept = opt.successIntercept;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				clockArea.stop();
		    	clockArea.stopReGetLotOp();
		    	balanceTimer.stop();
				// 错误方法增强处理
				if (statusCode == 404) {
					layer.msg("[" + opt.url + "] 404 not found",{icon: 5});
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
			    	layer.msg("网络异常，"+errorThrown,{icon: 5});
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					layer.msg("后台异常，请联系管理员!",{icon: 5});
					fn.successIntercept(data);
				} else if (ceipstate == 3) { // 业务异常
					layer.msg(data.msg,{icon: 5});
					fn.successIntercept(data);
				} else if (ceipstate == 4 || ceipstate == 5) {// 未登陆异常 或 没有权限
					clockArea.stop();
			    	clockArea.stopReGetLotOp();
			    	balanceTimer.stop();
					layer.alert(data.msg, {closeBtn: 0,icon: 5},function(){
						window.location.href=JIEBAO.base;
					});
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		return _ajax(_opt);
	};
})(jQuery);
$(function(){
	if(!!window.ActiveXObject){// 判断ie版本是否小于10
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(navigator.userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion<10){
        	var nai=$("#not-allow-old-ie").show();
        	nai.find('.ie-version').text(fIEVersion);
        	nai.find('#ie-agent').text(navigator.userAgent);
        	return;
        }
	}
	
	// 所有文本只能输入数字
	$("#betContainer").on("keydown","input[type=text]",function(e){
		var code = null;
		// 只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 // 48 - 57 上排数字键 96 - 105 数字键盘 8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {  
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	
	// 设置默认皮肤
    var a = Cookies.get("_theme"),
    e = Cookies.get("_themeColor"),
    g = Cookies.get("_themeHighlight"),
    c = Cookies.get("_themeHighlightColor");
	a = a?("#"+a):JIEBAO._theme;
	e = e?("#"+e):JIEBAO._themeColor;
	g = g?("#"+g):JIEBAO._themeHighlight;
	c = c?("#"+c):JIEBAO._themeHighlightColor;
	Core.changeColor(a, e, g, c);
    a = $(document).height();
    e = $("#slider_menu").height();
    (a-106) > e && $("#slider_menu").css("height", (a-114));
    $("body").on("click", ".game_select a", function() {// 选择彩种
    	var m =$("#slider_menu"),a=($(document.body).width() - 970)/2;
		if(m.hasClass("hideSide")){
			if(a>211){
				m.css("paddingLeft",a-211);
	    	}else{
	    		m.css("paddingLeft",0);
	    		$(".main_content").animate({"marginLeft":211},"fast");
	    	}
			m.removeClass("hideSide").addClass("showSide").show("fast")
		}else{
			m.removeClass("showSide").addClass("hideSide").hide("fast");
			$(".main_content").animate({"marginLeft":a},"fast");
		}
        return false;
    });
    function digitOnly(e){
    	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    	if (key!=8 && (key < 48 || key > 57)){
    		layer.tips("请输入整数倍数",$(this),{tips: [1, '#000']});
    		return false;
    	}
    }
    $("#theme").bind('click', function() {// 更换皮肤按钮
    	var it = $(this);
        if (!it.hasClass('shows')) {
        	it.removeClass('shows hides').addClass('shows');
            $('#slider_thememenu').show().animate({'width':210,"display":"block"},"fast");
        } else {
            $(this).removeClass('shows hides').addClass('hides');
            $('#slider_thememenu').animate({'width':0,"display":"none"},"fast");
        }
        return false;
    });
    $("#themeContent li").on("click", function() {// 皮肤选择
        var e, g, c, h,a="background-color";
        e = $(this).find(".color1").css(a);
        h = $(this).find(".color2").css(a);
        g = $(this).find(".color3").css(a);
        c = $(this).find(".color3").attr("txtColor");
        Cookies.set("_theme", Core.rgb2hex(e).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeColor", Core.rgb2hex(h).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeHighlight", Core.rgb2hex(g).replace("#","").toUpperCase(), {expires:7});
        Cookies.set("_themeHighlightColor", Core.rgb2hex(c).replace("#","").toUpperCase(), {expires:7});
        Core.changeColor(e, h, g, c);
        $("#theme").click();
    });
    function switchGame(a){// 切换彩种
    	Core.curQiHao=null;
    	clearAllItem();
		$(".game_list li").removeClass("current");
		a.parent().addClass("current");
		var lotCode=a.attr("lotcode");
		JIEBAO.lotCode=lotCode;
		JIEBAO.lotType=a.attr("lottype");
		JIEBAO.lotName=a.text();
		$("#Bm_general").empty();
		$("#betplay_data").html('<div style="text-align:center;line-height: 264px;font-size: 100px;">加载中...</div>');
		$(".gp_name").text(a.text());
		$(".gp_name").attr("lotcode",JIEBAO.lotCode);
		$(".gp_name").attr("lottype",JIEBAO.lotType);
		$("#nowGP").trigger("click");
		$(".game_select a").click();
		if(lotCode=="FC3D"||lotCode=="PL3" || lotCode == 'LHC' || lotCode == 'SFLHC'){
			$("#runbets_T").hide();// 排列三和福彩3D不需要追号
		}else{
			$("#runbets_T").show();
		}
		var scsp=$("#saiCheShiPin").hide();// 赛车视频
		if(lotCode=='BJSC' && scsp.attr("status")=="on"){
    		scsp.show();
        }
		$("#betOrderWrap").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    }
    $("body").on("click", ".game_list li a", function() {// 切换彩种事件
        var a = $(this);
        if(!a.attr("lotcode"))return;
        if($("#BetNumberListContanior table").find("tr").length>0){// 如果当前下注列表中有下注记录，则提示
        	layer.confirm('投注項中有您所添加的注單項目，你确定取消注单，切换彩類吗？', {
        		btn: ['确定','取消'] // 按钮
    		}, function(index){
    			switchGame(a);
    			layer.close(index);
    			return true
    		});
        }else{
        	switchGame(a);
        }
        if(JIEBAO.lotType == 6 ||JIEBAO.lotType == 66){
        	$("#okXgc").show().siblings("#notXgc").hide();
        	$("#closeBonus").hide();
        	$("#pvJJ").text("赔率");
        }else{
        	$("#okXgc").hide().siblings("#notXgc").show();
        	$("#pvJJ").text(JIEBAO.fanShui?'奖金/返点':'奖金');
        	$("#closeBonus").show();
        }
    });
    $(window).resize(function() {// 浏览器窗口大小改变事件
        $("#slider_menu").hasClass("showSide") && $(".game_select a").click();
        $("#slider_thememenu").css("width", 0).hide();
        $("a#theme").removeClass("shows hides").addClass("hides");
        $(".main_content").css({"marginLeft":"auto"});
    });
    
    // 玩法大类点击事件
    $("#Bm_general").on("click","a[p]",function(){
    	var $it = $(this),p=$it.attr("p");
    	$("#Bm_general").find("li").removeClass("current");
    	$it.parent().addClass("current");
    	Core.gamePlay.switchGroup(p);
    	Core.mark.resetClear();
//    	if(JIEBAO.lotType==6 || JIEBAO.lotType == 66){
//	    	var filterCode = ['lm','hx','qbz','wsl','lx'];
//	    	var p = p.split('_');
//	    	if($.inArray(p[2],filterCode)>=0){
//	    		//说明存在
//	    		$("#checkedDiv").show();
//	    	}else{
//	    		$("#checkedDiv").hide();
//	    	}
//    	}
    });
    // 彩票订单点击事件
    var lotBtns=$("#lotteryBtns"),$nowGP=$("#nowGP");
    lotBtns.on("click","a",function(){
    	lotBtns.find("a").removeClass("current");
    	$(this).addClass("current");
    	$nowGP.removeClass("current");
    });
    
    $nowGP.click(function(){// 选择彩种后面的彩种名称点击事件
    	$('#ocontainer').hide();
		$('#betContainer').show();
		lotBtns.find("a").removeClass("current");
		$nowGP.addClass("current");
		Core.getLotOp();
		Core.getPlayType();
		//导航切换
		if(JIEBAO.lotType == 6 || JIEBAO.lotType == 66){
			$("#okXgc").show().siblings("#notXgc").hide();
		}
    }).click();
    
    var $betPlayData=$("#betplay_data"), kjtz=$("#MoneyList");
   
    $betPlayData.on("click","input[name='playId']",function(){ // 玩法小类点击事件
    	var $it = $(this),c=$it.attr("c"),t=$it.attr("t");
    	Core.gamePlay.switchPlay("play_"+t+"_"+c);
    	Core.mark.resetClear();
    }).on("click","input[name='data_miss']",function(){ // 冷热遗漏点击事件
    	var $it = $(this),v=$it.val(),ii=$betPlayData.find(".num_btn i"),ss=$betPlayData.find(".num_btn s");
    	if("miss"==v){
    		ii.show();
    		ss.hide();
    	}else{
    		ss.show();
    		ii.hide();
    	}
    }).on("mouseover",".row_con_c1_btm a",function(){// 玩法小类问好和示例hover事件
    	var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#000'],time:0});
    	}
    }).on("mouseout",".row_con_c1_btm a",function(){
    	layer.closeAll("tips");
    }).on("click","a[num]",function(){// 号码点击事件
		var it =$(this);
		if(!it.hasClass("selected")){
			it.addClass("selected");
		}else{
			it.removeClass("selected");
		}
		Core.betting.sumbetCount(it);
	}).on("click","a[k]",function(){// 全大小单双清点击事件
		var it =$(this),nums=it.parent().parent().find("a[num]"),k=parseInt(it.attr("k"));
		switch(k){
		case 1:// 全
			nums.addClass("selected");break;
		case 2:// 大
			var l = parseInt(nums.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n>=l)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 3:// 小
			var l = parseInt(nums.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n<l)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 4:// 单
			nums.each(function(i){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n%2==1)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 5:// 双
			nums.each(function(){
				var o =$(this),n=parseInt(o.attr("num"),10);
				if(n%2==0)o.addClass("selected");
				else o.removeClass("selected");
			});
			break;
		case 6:// 清
			nums.removeClass("selected");
			break;
		}
		Core.betting.sumbetCount();
	}).on("change","input:checkbox",function(){
		var it=$(this).parent().parent(),
			a=it.find("input:checkbox:checked").length,
			b=parseInt(it.attr("wei")||0);
		$("#positioncount").text(a);
		var combine=function(b,d){
	    	var c=function(a){
	    		for(var b=1,c=2;c<=a;c++)
	    			b*=c;return b
	    		};
	    	return d>b?0:d==b?1:c(b)/(c(d)*c(b-d))
	    };
		$("#positioninfo").text(combine(a,b));
		Core.betting.sumbetCount();
		//六合彩专用
	}).on("click",".table_ball tbody td",function(e){	//该处以下属于六合彩事件
		e.stopPropagation();
    	var t=$(this), dataCurrent = t.attr("data-current"),isText = t.parent().find('td[data-current='+dataCurrent+'] input').attr('type') == 'text';;
    	if(isText){ //text
    		if(t.hasClass("active")){
    			t.parent().find('td[data-current='+dataCurrent+'] input[type=text]').val('');
    			t.parent().find('td[data-current='+dataCurrent+']').removeClass("active");
    			$("#MoneyList").hide();
    		}else{
    			t.parent().find('td[data-current='+dataCurrent+'] input[type=text]').focus();
    			t.parent().find('td[data-current='+dataCurrent+']').addClass("active");
    		}
    		Core.mark.betAll();
    	}else{		//checkbox
    		if(t.parent().find('td[data-current='+dataCurrent+'] input').is(":disabled")){return false;}
    		if(t.hasClass("active")){
    			t.parent().find('td[data-current='+dataCurrent+']').removeClass("active");
    			t.parent().find('td[data-current='+dataCurrent+'] input[type=checkbox]').prop("checked",false);
    		}else{
    			t.parent().find('td[data-current='+dataCurrent+']').addClass("active");
    			t.parent().find('td[data-current='+dataCurrent+'] input[type=checkbox]').prop("checked",true);
    		}
    		Core.mark.betCheckedAll();
    	}
    }).on("click focus",".table_ball tbody td input[type=text]",function(e){
    	e.stopPropagation();
    	var checkMoney = $("input[name=checked_money]").val(),t=$(this),dataCur=t.parent().attr("data-current");
    	if(!checkMoney){
    		var position=t.position();
        	pTop = position.top + 443;
        	pLeft = position.left + 55;
        	kjtz.css({ position: 'absolute','z-index': 1000000,'display':'block',"top":pTop,"left":pLeft}).attr("data-val",dataCur);
        	$("#BetMinMoneyLimit").text(t.attr("min"));
    		$("#BetMaxMoneyLimit").text(t.attr("max"));
    	}else{
    		$(this).val(checkMoney);
    		$betPlayData.find('.table_ball td[data-current='+dataCur+']').addClass("active");
    	}
    }).on("keyup",".table_ball tbody td input[type=text]",function(){
    	var t= $(this),tVal = t.val(),dataCur = t.parent().attr("data-current"),min=parseInt(t.attr("min")),max=parseInt(t.attr("max"));
    	$betPlayData.find('.table_ball td[data-current='+dataCur+']').addClass("active");
    	if(tVal){
    		tVal = parseInt(tVal);
    		if(min>0 && tVal<min){
    			layer.tips('温馨提示：下注金额必须大于'+min,t,{tips: [1, '#000']});
    			$betPlayData.find('.table_ball td[data-current='+dataCur+']').removeClass("active");
    		}
    		if(max>0 && tVal>max){
    			layer.tips('温馨提示：下注金额必须小于'+max,t,{tips: [1, '#000']});
    			$betPlayData.find('.table_ball td[data-current='+dataCur+']').removeClass("active");
    		}
    	}else{
    		layer.tips('温馨提示：下注金额必须大于'+min,t,{tips: [1, '#000']});
			$betPlayData.find('.table_ball td[data-current='+dataCur+']').removeClass("active");
    	}
    	Core.mark.betAll();
    	kjtz.hide();
    }).on("click",".table_ball tbody td input[type=checkbox]",function(e){
    	e.stopPropagation();
    	var t = $(this),dataCur = t.parent().attr("data-current");
    	if(t.is(":checked")){
    		t.parent().parent().find('td[data-current='+dataCur+']').addClass("active");
    	}else{
    		t.parent().parent().find('td[data-current='+dataCur+']').removeClass("active");
    	}
    	Core.mark.betCheckedAll();
    });
    
    //xgc重置
    var xgcBtn = $("#okXgc");
    xgcBtn.on("click","#reSetLottery",function(){
    	Core.mark.resetAll();
    }).on("click focus","input[name=checked_money]",function(){
    	var t = $(this),position=t.position(),dataVal=t.attr("name"),s;
    	pTop = position.top + 28;
    	pLeft = position.left - 168;
    	kjtz.css({ position: 'absolute','z-index': 1000000,'display':'block',"top":pTop,"left":pLeft}).attr("data-val",dataVal);
    	//lm_qbz 和多选
    	if($(".games tr").length>0){
    		var c = $(".games tr input[type=radio]:checked").attr("c");
    		s = $('.games tr td[c='+c+']');
    	}else{
    		s = $(".table_ball td input:eq(0)");
    	}
    	$("#BetMinMoneyLimit").text(s.attr("min"));
		$("#BetMaxMoneyLimit").text(s.attr("max"));
    }).on("keyup","input[name=checked_money]",function(){
    	var t= $(this),tVal = t.val(),s;
    	if($(".games tr").length>0){
    		var c = $(".games tr input[type=radio]:checked").attr("c");
    		s = $('.games tr td[c='+c+']');
    	}else{
    		s = $(".table_ball td input:eq(0)");
    	}
    	var min = parseInt(s.attr("min")),max=parseInt(s.attr("max"));
    	if(tVal){
    		tVal = parseInt(tVal);
    		if(min>0 && tVal<min){
    			layer.tips('温馨提示：下注金额必须大于'+min,t,{tips: [1, '#000']});
    		}
    		if(max>0 && tVal>max){
    			layer.tips('温馨提示：下注金额必须小于'+max,t,{tips: [1, '#000']});
    		}
    	}else{
    		layer.tips('温馨提示：下注金额必须大于'+min,t,{tips: [1, '#000']});
    	}
    	Core.mark.betCheckedAll();
    	kjtz.hide();
    });
    
    // 快捷投注
    kjtz.on("click","tbody td",function(){
    	var t = $(this),tVal = t.text().trim(),dataVal=kjtz.attr("data-val");
    	if(dataVal == 'checked_money'){
    		$("#okXgc").find('input[name='+dataVal+']').val(tVal);
    		if($(".table_ball td input[type=text").length>0){
	    		$(".table_ball td input[type=text]").each(function(i,j){
	    			j = $(j);
	    			if(j.val() && j.parent().hasClass("active")){
	    				j.val(tVal);
	    			}
	    		})
	    		Core.mark.betAll();
    		}else{
    			Core.mark.betCheckedAll();
    		}
    	}else{
    		//把值写入input
    		$betPlayData.find('.table_ball td[data-current='+dataVal+'] input[type=text]').val(tVal);
        	//把整个td改为选中状态
        	$betPlayData.find('.table_ball td[data-current='+dataVal+']').addClass("active");
        	Core.mark.betAll();
    	}
    	kjtz.hide();
    }).on("mouseleave",function(){
    	kjtz.hide();
    });
    //xgc投注
    $("#xgcSeleAll").click(function(){
    	if (!bet_result.hasOwnProperty('betCount') || bet_result.betCount <= 0){
    		layer.msg("请添加一组投注号码",{icon:5});
    		return false;
    	} 
    	if(!bet_result.hasOwnProperty('betMoney') || bet_result.betMoney <=0){
    		layer.msg("请输入投注金额",{icon:5});
    		return false;
    	}
    	var _table = $("#betplay_data .table_ball"),a_zs = $("#betAllZhuS").text(),a_m=parseFloat($("#betAllTotals").text());
    	if(!parseInt(a_zs)){
    		layer.msg("请添加一组投注号码",{icon:5});
    		return false;
    	}
    	if(!a_m){
    		layer.msg("请输入投注金额",{icon:5});
    		return false;
    	}
    	if(a_m && a_m>0){
        	var iType='',data=new Object(),
        	g_name = $("#Bm_general li.current a").text().trim(),	//大类
        	p_checked = $(".tab .play_tab input[type=radio]:checked"),	//选中小类
        	p_name = p_checked.parent().text().trim(),isChecked=false,p_code = p_checked.attr("c");
        	checked_hm='',odds='',oddsId=0,bettr='',zhushu=0,curLen=0;	//小类
        	_table.find('tbody tr input').each(function(index,item){
        		item = $(item);
                bettr = '';
        		if(item.attr('type') == 'text' && item.parent().hasClass("active")){
        			bettr += '<tr n="' + item.attr("num") + '" c="'+p_checked.attr("c")+'" m="'+item.val()+'" r="'+item.attr("data-id")+'"><td>' + $('#GameTypename').text() + '</td>';
                    bettr += '<td><span tips="' + g_name + ':' + p_name + '">' + p_name + '</span></td>';
                    bettr += '<td><span tips="' + item.attr("num") + '">' + Core.strLimit(item.attr("num"), 22) + '</span></td>';
                    bettr += '<td>1</td>';
                	bettr += '<td>' + item.attr("odds") + '</td>';
                    bettr += '<td>' + item.val() + '</td>';
                    bettr += '<td><a href="javascript:;"><i class="fa fa-times-circle fa-lg c_white3"></i></a></td></tr>';
                    $betList.find("table tbody").append(bettr);
                    return true;
        		}
    			if($(".games tr").length>0){	//lm_qbz
    				var games = $(".games tr"),isRadio = games.find('input[type=radio]:checked');
    				p_name = isRadio.parent().text().trim();
    				odds = games.find('td[c='+isRadio.attr("c")+']').attr("odds");
    				oddsId = games.find('td[c='+isRadio.attr("c")+']').attr("data-id");
    				p_code = isRadio.attr("c");
    				if(item.is(":checked")){
    					curLen ++;
    					zhushu = Core.mark.buyZhuShu(curLen,games.find('td[c='+isRadio.attr("c")+']').attr("min_s"),games.find('td[c='+isRadio.attr("c")+']').attr("max_s"));
    					checked_hm += item.attr("num") + ",";
    				}
    				isChecked = true;
    				return true;
    			}
    			if(item.is(":checked")){
    				curLen ++;
        			checked_hm += item.attr("num") + ",";	//号码
        			odds = item.attr("odds");
        			oddsId=item.attr("data-id");
        			zhushu = Core.mark.buyZhuShu(curLen,item.attr("min_s"),item.attr("max_s"));
        			isChecked = true;
    			}
        	});
        	if(isChecked){	//checked
        		checked_hm = checked_hm.substring(0,checked_hm.length-1);
        		bettr += '<tr n="' + checked_hm + '" c="'+p_code+'" m="'+$("#checkedDiv input[name=checked_money]").val()+'" r="'+oddsId+'"><td>' + $('#GameTypename').text() + '</td>';
                bettr += '<td><span tips="' + g_name + ':' + p_name + '">' + p_name + '</span></td>';
                bettr += '<td><span tips="' + checked_hm + '">' + Core.strLimit(checked_hm, 22) + '</span></td>';
                bettr += '<td>'+zhushu+'</td>';
            	bettr += '<td>' + odds + '</td>';
                bettr += '<td>' + $("#checkedDiv input[name=checked_money]").val()*zhushu + '</td>';
                bettr += '<td><a href="javascript:;"><i class="fa fa-times-circle fa-lg c_white3"></i></a></td></tr>';
                $betList.find("table tbody").append(bettr);
        	}
    		Core.mark.resetAll();
    		Core.countTotalBet();
    	}
    	
    })
    
    // 近期开奖记录
    $("#lotHistory").hover(function(){
    	var lis = $("#lotHistory").find("li");
    	if(lis.length){
    		var a = lis.length * (lis.eq(0).outerHeight()+1);
    		$(this).css("height",a+"px");
    	}
    },function(){
    	$(this).css("height","104px");
    });
    // 倍数修改事件
    $("#buymultiple").on('keyup blur', function() {
    	var it = $(this),v=it.val();
    	if(v){
	    	if(!/^[\d]{1,8}$/.test(v) || v>10000000){
	    		layer.tips("倍数必须大于0，小于10,000,000",it,{tips: [1, '#000']});
	    		it.val(1);
	    		return false;
	    	}
    	}
    	if (bet_result.hasOwnProperty('betCount')) {
            var r = Core.__FloatMul(bet_result.betCount, v)*Core.price;
            r = Core.__FloatDiv(r, $("select#selectdollarunit").val());
            $("#allTotamounts").text(r);
        }
    }).on("keypress",digitOnly);
    // 模式修改元角分
    $("#selectdollarunit").on('change', function() {
        if (bet_result.hasOwnProperty('betCount')) {
            var r = Core.__FloatMul(bet_result.betCount, $("#buymultiple").val())*Core.price;
            r = Core.__FloatDiv(r, $(this).val());
            $("#allTotamounts").text(r);
        }
    });
    //单式文本输入框
    $betPlayData.on('keyup blur',".no_input_box textarea", function(evt) {
    	var it=$(this),val=it.val(),pattern=new RegExp(it.attr("_pattern")),type=it.attr("_type"),desc=it.attr("_desc")||"";
    	if(val){
    		var vs=val.split(/，|;|\r|\n| /);
        	for(var i =0;i<vs.length;i++){
        		if(vs[i] && !pattern.test(vs[i])){
        			if(evt.type=="blue"||evt.type=="focusout")layer.msg(vs[i]+"格式不对。"+desc);
        			Core.betting.sumTextareaBetCount(type,null);
        			return;
        		}
        	}
        	Core.betting.sumTextareaBetCount(type,vs);
    	}
    });
  // “添加”按钮的点击事件，将投注记录添加到列表中
    $("#seleall").click(function(){
        if (!bet_result.hasOwnProperty('betCount')) return false;
        if (bet_result.betCount <= 0) return false;;
        if ($betList.find("tr").length > Core.maxItem) return false;
        var allTotamounts = parseFloat($("#allTotamounts").text());// 投注金额
        if(allTotamounts && allTotamounts>0){
        	if(JIEBAO.maxBetMoney>0 && allTotamounts>JIEBAO.maxBetMoney){
        		layer.msg('温馨提示：下注金额必须小于'+JIEBAO.maxBetMoney,{icon: 5});
        		return false;
        	}
        	if(JIEBAO.minBetMoney>0 && allTotamounts<JIEBAO.minBetMoney){
        		layer.msg('温馨提示：下注金额必须大于'+JIEBAO.minBetMoney,{icon: 5});
        		return false;
        	}
        	var bettr = '',y = 0,
	         	ttp = $(".ch_radio td input[type=radio]:checked"),
	         	tth = ttp.parent().parent().prev().text(),
	         	playCode = ttp.attr("c"),
	         	coin = $("#selectdollarunit").val(),
	         	beiShu=$("#buymultiple").val(),
	         	rate=$("#gcrate").text();
            if (bet_result.errorTagStr != '') {
            	layer.msg('温馨提示： 以下号码有误，已进行自动过滤。'+bet_result.errorTagStr,{icon: 5});
            }
            var hm=(bet_result.hasOwnProperty('optional')&&bet_result.optional)?(bet_result.optional+"@"+bet_result.myarray):bet_result.myarray;
            // 彩票code｜玩法id｜货币单位（1=元，10=角，100=分）｜投注倍数|投注号码
            var buyinfo = JIEBAO.lotCode + '|' + playCode + '|' + coin + '|'+beiShu + '|' + hm;
            $betList.find("tr").each(function() {
                if ($(this).attr('buyinfo').toString() === buyinfo) {
                    y++;
                }
            });
            if (y) {
                layer.msg('此投注号码已存在，请重新选择!!',{icon: 5});
                return false;
            }
            bettr += '<tr buyinfo="' + buyinfo + '" c="'+ttp.attr("c")+'" m="'+allTotamounts+'" r="'+rate+'"><td>' + $('#GameTypename').text() + '</td>';
            bettr += '<td><span tips="' + tth + ':' + ttp.parent().text() + '">' + ttp.parent().text() + '</span></td>';
            bettr += '<td><span tips="' + hm + '">' + Core.strLimit(hm, 22) + '</span></td>';
            bettr += '<td>' + bet_result.betCount + '</td>';
        	bettr += '<td>' + rate;
        	if(JIEBAO.fanShui){
        		bettr += '/' + $("#rebateShow").text() ;
        	}
            bettr += '</td><td>' + allTotamounts + '</td>';
            bettr += '<td><a href="javascript:;"><i class="fa fa-times-circle fa-lg c_white3"></i></a></td></tr>';
            $betList.find("table tbody").append(bettr);
            $("#betplay_data a").removeClass('selected');
            Core.betting.resetSumbetCount();
            Core.countTotalBet();
            processBetTraceCore.updateBetData();
        } else {
            layer.msg('投注金额不能为0！',{icon: 5});
        }
    });
    var $betList=$("#BetNumberListContanior");
    $betList.on("mouseover","[tips]",function(){// 投注列表中，有tips属性的显示tips信息
    	var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#000'],skin:""});
    	}
    }).on("mouseout","[tips]",function(){
    	layer.closeAll("tips");
    });
    $betList.on('click', "a", function() {// 单注记录删除功能
    	var e=$(this);
    	layer.confirm('你确定删除该注单？', {
    		btn: ['确定','取消'] // 按钮
		}, function(index){
			e.parent().parent().remove();
	        Core.countTotalBet();
	        processBetTraceCore.updateBetData();
			layer.close(index);
			return true
		});
    });
    function clearAllItem(){
    	$betList.find("table tbody").empty();
        Core.countTotalBet();
        processBetTraceCore.updateBetData();
    }
    $("#clearallitem").on('click', function() {// 删除全部注单
    	if(!$betList.find("tr").length)return false;
    	layer.confirm('你确定清空注单？', {
    		btn: ['确定','取消'] // 按钮
		}, function(index){
			clearAllItem();
			layer.close(index);
			return true
		});
    });
    $("#runbets").on("click",function(){// 追号
    	var trs=$betList.find("tr");
    	if(!trs.length){
    		layer.msg('请先添加一注投注号码！',{icon: 5});
    		return false;
    	}
    	var betTrace = $("#betTrace").show();
    	$("#betOrderWrap").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    	$.ajax({
            type: "get",
            url: JIEBAO.base+"/lottery/getQiHaoForChase.do?lotCode="+JIEBAO.lotCode,
            cache: false,
            dataType: "json",
            beforeSend: function(XMLHttpRequest) {
            	betTrace.find("#betTractLoading").show();
            	betTrace.find("#betTractCon").hide();
            },
            success: function(json) {
            	if(!json.success){
            		betTrace.hide();
            		layer.msg(json.msg,{icon: 5});
            		return;
            	}
            	if(json.qiHaos && json.qiHaos.length>0){
            		processBetTraceCore.initView(json.qiHaos);
                    betTrace.find("#betTractLoading").hide();
                	betTrace.find("#betTractCon").show();
                	$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
            	}else{
            		layer.msg('今日已经没有期号可追号！',{icon: 5});
            	}
            }
        });
        return false;
    });
    $("#periodNumLeader").on("keyup blur",function(){// 追号期数修改事件
    	var it = $(this),v=it.val();
    	if(!/^[\d]{1,2}$/.test(v) || v>30 || v<1){
    		layer.tips("追号期数必须大于0，小于等于30",it,{tips: [1, '#000']});
			it.val(10);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#startBeeNum").on("keyup blur",function(){// 起始倍数修改事件
    	var it = $(this),v=it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v <1){
    		layer.tips("倍数必须为大于0的整数",it,{tips: [1, '#000']});
			it.val(1);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#betTraceData").on("blur", "input[type='text']", function(e) {// 追号列表中的倍数修改事件
        var it=$(this),v = it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v<1){
    		layer.tips("倍数必须为大于0的整数",it,{tips: [1, '#000']});
			it.val(1);
    		return false;
    	}
    }).on("keypress",digitOnly);
    $("#creatGtjhBtn").on('click', function() {
    	var qiShu = $("#periodNumLeader").val(),beiShu=$("#startBeeNum").val();
    	if(!/^[\d]{1,2}$/.test(qiShu) || qiShu>30 || qiShu<1){
    		layer.tips("追号期数必须大于0，小于等于30",$("#periodNumLeader"),{tips: [1, '#000']});
    		return false;
    	}
    	if(!/^[\d]{1,8}$/.test(beiShu) || beiShu <1){
    		layer.tips("倍数必须为整数",$("#startBeeNum"),{tips: [1, '#000']});
    		return false;
    	}
    	processBetTraceCore.startQiShu=qiShu;
    	processBetTraceCore.startTimes=beiShu;// 起始倍数
        processBetTraceCore.generateBetView();
    });
    $("#betTraceSelectAll").click(function(){
    	if($(this).prop("checked")){
    		$("#betTraceData").find("input:checkbox").prop("checked",true);
    	}else{
    		$("#betTraceData").find("input:checkbox").prop("checked",false);
    	}
    	processBetTraceCore.calcView();
    });
    // 下注情况
    $("#nowbetinfo").on("click",function nowbetinfo(){
    	$("#betOrderWrap").show();
    	$("#betTrace").hide();
    	$("#saiCheShiPinSwfWrap").hide();
    	var lotName=$("#nowGP").text().trim();
    	$("#betName").text(lotName);
    	var page = 1;
    	getBcLotteryOrder(page);
		$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
    });
    $("#main_wrap").on("click",".BetInf", function(){
    	var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode");
    	
    	$.ajax({
    		url:JIEBAO.base+'/lotteryBet/getBcLotteryOrderDetail.do',
    		data:{
    			orderId : orderId,
    			lotCode : lotCode,
    			},
    		success:function(r){
    			if(r.winZhuShu == null || r.winZhuShu==''){
    				r.winZhuShu = 0;
    			}
    			if(r.winMoney == null || r.winMoney==''){
    				r.winMoney = 0;
    			}
    			if(r.rollBackMoney == null || r.rollBackMoney==''){
    				r.rollBackMoney = 0;
    			}
    			if(r.lotteryHaoMa == null || r.lotteryHaoMa==''){
    				r.lotteryHaoMa = '- -';
    			}
    			//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
    			var yingkui = 0;
				if(r.status == 1 ){
					r.status='未开奖'
				}else if(r.status == 2 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='已中奖'
				}else if(r.status == 3 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='未中奖'
				}else if(r.status == 4 ){
					r.status='撤单'
				}else if(r.status == 5 ){
					r.status='回滚成功'
				}else if(r.status == 6 ){
					r.status='回滚异常'
				}else if(r.status == 7 ){
					r.status='开奖异常'
				}else if(r.status == 8){
					r.status ='和局';
				}
				var content = '',teshuName='';
				if(lotCode == "LHC" || lotCode == 'SFLHC' || lotCode == 'WFLHC' || lotCode == 'WFLHC' || lotCode == 'HKMHLHC' || lotCode == 'AMLHC'){
					switch(r.groupCode){
	    			case "wsl":teshuName="0尾赔率：";break;
	    			case "hx" :
	    			case "lx" :teshuName="生肖本命年赔率：";break;
					}
					content = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="message_table" style="font-size: 12px; color: #000;"><tbody>'
	    				+'<tr class="bg_black3"><td colspan="4" class="open_title">订单号：<span style="color:green;">'+r.orderId+'</span></td></tr>'
	    				+'<tr><th class="bg_black3">帐号：</th><td>'+r.account+'</td><th class="bg_black3">单注金额：</th><td>'+((r.buyMoney).toFixed(2)/r.buyZhuShu).toFixed(2)+'</td></tr>'
	    				+'<tr><th class="bg_black3">下注时间：</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+r.buyZhuShu+'</td></tr>'
	    				+'<tr><th class="bg_black3">彩种：</th><td>'+r.lotName+'</td><th class="bg_black3">投注总额：</th><td>'+((r.buyMoney).toFixed(2))+'</td></tr>'
	    				+'<tr><th class="bg_black3">期号：</th><td>'+r.qiHao+'</td><th class="bg_black3">基础赔率'+(JIEBAO.fanShui?('/返点'):'')+'：</th><td>'+r.odds.toFixed(2)+(JIEBAO.fanShui?('/'+(+r.rollBackRate||0)+"%"):'')+'</td></tr>'
	    				+'<tr><th class="bg_black3">玩法：</th><td>'+r.playName+'</td><th class="bg_black3">中奖注数：</th><td>'+r.winZhuShu+'</td></tr>'
	    				+'<tr><th class="bg_black3">开奖号码：</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖金额：</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'
	    				+'<tr><th class="bg_black3">状态：</th><td>'+r.status+'</td><th class="bg_black3">盈亏：</th><td>'+yingkui.toFixed(2)+'</td></tr>'
	    				+'<tr>'+(JIEBAO.fanShui?('<th class="bg_black3">销售返点</th><td '+(r.minBonusOdds==0?('colspan="3"'):'')+'>'+r.rollBackMoney.toFixed(2)+'</td>'):'')+(r.minBonusOdds!=0?('<th class="bg_black3">'+teshuName+'</th><td '+(JIEBAO.fanShui?'':('colspan="3"'))+'>'+r.minBonusOdds.toFixed(2)+'</td>'):'')+'</tr>'
	    				+'<tr><td colspan="4"><textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 50px; border: 1px solid #ccc; font-size: 16px;">'+r.haoMa+'</textarea></td></tr>'
	    				+"</tbody></table>";
				}else{
					content = '<table width="100%" border="0" cellspacing="0" cellpadding="0"class="message_table" style="font-size: 16px; color: #000;"><tbody>' +
						'<tr class="bg_black3"><td colspan="4" class="open-title">订单号 ：<span style="color: green">'+r.orderId+'</span></td></tr>'+
						'<tr><th class="bg_black3">帐号：</th><td>'+r.account+'</td><th class="bg_black3">单注金额：</th><td>'+(2.00/r.model).toFixed(2)+'</td></tr>'+
						'<tr><th class="bg_black3">下注时间：</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+r.buyZhuShu+'</td></tr>'+
						'<tr><th class="bg_black3">彩种：</th><td>'+r.lotName+'</td><th class="bg_black3">倍数：</th><td>'+r.multiple+'</td></tr>'+
						'<tr><th class="bg_black3">期号：</th><td>'+r.qiHao+'</td><th class="bg_black3">投注总额：</th><td>'+r.buyMoney.toFixed(2)+'</td></tr>'+
						'<tr><th class="bg_black3">玩法：</th><td>'+r.playName+'</td><th class="bg_black3">奖金'+(JIEBAO.fanShui?"/返点":'')+'：</th><td>'+r.minBonusOdds.toFixed(2)+(JIEBAO.fanShui?('/'+(r.rollBackRate||0)+'%'):"")+'</td></tr>'+
						'<tr><th class="bg_black3">开奖号码：</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖注数：</th><td>'+r.winZhuShu+'</td></tr>'+
						'<tr><th class="bg_black3">状态：</th><td>'+r.status+'</td><th class="bg_black3">中奖金额：</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'+
						'<tr>'+(JIEBAO.fanShui?('<th class="bg_black3">销售返点：</th><td>'+r.rollBackMoney.toFixed(2)+'</td>'):'')+'<th class="bg_black3">盈亏：</th><td'+(JIEBAO.fanShui?">":' colspan="3">')+yingkui.toFixed(2)+'</td></tr>'+
						'<tr><td colspan="4">'+
						'<textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 100px; border: 1px solid #ccc; font-size: 16px;">'+r.haoMa+'</textarea>'+
						'</td></tr>'+
						'</tbody></table>';
				}
    			layer.alert(content,{title:"系统提示",area:['700px','auto'],offset : ['5%' ]});
    		}
    	})
    })
    //撤单
    $("#main_wrap").on("click",".cheDan", function(){
    	var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode"),from=it.attr("from");
    	layer.confirm('您确定要撤销该订单？', {
    		  btn: ['确定','取消'] 
    		}, function(){
    			$.ajax({
    	    		url:JIEBAO.base+'/lotteryBet/cancelOrder.do',
    	    		data:{
    	    			orderId : orderId,
    	    			lotCode : lotCode,
    	    			},
    	    		success:function(r){
    	    			layer.msg("撤单成功",{icon: 6});
    	    			switch(from){
    	    			case "ddcsx":if(typeof getOrderManager!="undefined")getOrderManager(1);
    	    			default:getBcLotteryOrder(1);
    	    			}
    	    		}
    	    	})
    		}, function(i){
    			layer.close(i);
    		});
    })
    
    $("#saiCheShiPin").on("click",function(){// 赛车视频
    	$("#betOrderWrap").hide();
    	$("#betTrace").hide();
    	var it=$(this),swf = it.attr("swf");
    	if(swf){
    		var w = $("#saiCheShiPinSwfWrap");
    		if(!it.attr("inited")){console.log(123)
        		it.attr("inited",true);
        		var html = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="940" height="543">'
                    +'<param name="movie" value="'+ swf+'">'
                    +'<param name="quality" value="High">'
                    +'<param name="_cx" value="12383">'
                    +'<param name="_cy" value="1588">'
                    +'<param name="FlashVars" value="">'
                    +'<param name="Src" ref="" value="'+ swf+'">'
                    +'<param name="WMode" value="Window">'
                    +'<param name="Play" value="-1">'
                    +'<param name="Loop" value="-1">'
                    +'<param name="SAlign" value="">'
                    +'<param name="Menu" value="-1">'
                    +'<param name="Base" value="">'
                    +'<param name="AllowScriptAccess" value="always">'
                    +'<param name="Scale" value="ShowAll">'
                    +'<param name="DeviceFont" value="0">'
                    +'<param name="EmbedMovie" value="0">'
                    +'<param name="BGColor" value="">'
                    +'<param name="SWRemote" value="">'
                    +'<param name="MovieData" value="">'
                    +'<embed src="'+ swf+'" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="940" height="543">'
                    +'</object>';
        		w.find("div").html(html);
        	}
    		w.show();
    		$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
    	}
    });
    $("#gamebuy").click(function(){
    	if(!Core.curQiHao){
    		layer.msg('当前期数为空，不能投注！',{icon: 5});
    		return false;
    	}
    	var trs=$betList.find("tr");
    	if(!trs.length){
    		layer.msg('至少添加一注投注号码！',{icon: 5});
    		return false;
    	}
    	if(trs.length> Core.maxItem){
    		layer.msg('投注号码数不能多于'+Core.maxItem+'！',{icon: 5});
    		return false;
    	}
    	if(JIEBAO.lotType == 6 || JIEBAO.lotType == 66){
    		//六合彩下注方法
    		markBetInfo();
    		return false;
    	}
    	 var tracecount = parseInt($("#lt_trace_count").text() || 0)||0,
    	 	allAmount=0,zhuiHao=(tracecount > 0 && !$('.r5_c2').is(':hidden')),
    	 	nowbalance = parseFloat($("#balance").attr("balance").replace(/,/g,"") || 0)||0;
         if (zhuiHao) {
        	 allAmount=(parseFloat($("#lt_trace_hmoney").text())||0);
             if (nowbalance < allAmount) {
            	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
                 return false;
             }
         } else {
        	 allAmount=(parseFloat($("#s_allTotamounts").text())||0);
             if (nowbalance < allAmount) {
            	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
                 return false;
             }
         }
         var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
         $.getJSON(JIEBAO.base+"/lotteryBet/token.do",function(data){
        	 if(data.success==0){
        		 layer.msg(data.msg,{icon: 5});
                 return false;
        	 }
        	 layer.close(layerIndex);
        	 var msg = '',myBuyArray = [],dtitle='系统提示',qiHao=Core.curQiHao,
        	 	postData={lotCode:JIEBAO.lotCode,token:data.token,qiHao:qiHao};
        	 $betList.find("tr").each(function(i, j) {
        		 myBuyArray.push($(this).attr('buyinfo'));
	         });
        	 postData.bets = myBuyArray;
        	 if (tracecount > 0 && !$('.r5_c2').is(':hidden')) {// 追号数据
        		 myBuyArray=[];
        		 myBuyArray.push($("#lt_trace_stop").prop("checked") ? 1 : 0);// 中奖是否停止追号
                 var $betTr = $("#betTraceData tr");
                 $betTr.each(function(i, j) {
                	 j=$(j);
                     if (j.find("input[type='checkbox']").prop("checked")) {
                    	 j=j.find("input[qh]");
                    	 myBuyArray.push(j.attr("qh")+"|"+j.val());
                     }
                 });
                 dtitle = '确定要追号 ' + tracecount + ' 期 ？';
                 msg += '<p>彩票种类 ： ' + $('#GameTypename').text() + '</p>';
                 msg += '<p>追号总期数 ： ' + tracecount + '</p>';
                 msg += '<p>追号总注数 ： ' + (parseInt($("#s_allTotbets").text()) * tracecount) + ' ( 注 )</p>';
                 msg += '<p>追号总金额 ： ' + allAmount + ' ( 元 )</p>';
                 postData.tractData=myBuyArray;
             } else {
                 msg += '<p>彩票种类 ： ' + $('#GameTypename').text() + '</p>';
                 msg += '<p>下注期号 ： 第 ' + qiHao + ' 期</p>';
                 msg += '<p>下注注数 ： ' + $('#s_allTotbets').text() + ' ( 注 )</p>';
                 msg += '<p>下注总额 ： ' + allAmount + ' ( 元 )</p>';
             }
        	 layer.confirm(msg, {title:dtitle,area:["400px"]}, function(){
        		 layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
// layer.msg('努力中', {icon: 1,offset:350},function(){layer.close(layerIndex);});
        		 $.ajax({url:JIEBAO.base+"/lotteryBet/doBets.do",
        			 type: 'POST',
                     dataType: 'json',
                     data: {data:JSON.stringify(postData)},
                     success: function(data) {
                    	 layer.close(layerIndex);
                    	 if(!data.success){
                    		 layer.msg(data.msg,{icon: 5});
                             return false;
                    	 }
                    	 layer.msg("下单成功",{icon: 6});
                    	 clearAllItem();
                         $("#betTrace").hide();
                     },
                     successIntercept: function(data){
                    	 layer.close(layerIndex);
                     }
        		 });
        	 }, function(i){
        		 layer.close(i);
        	 });
        	 if (tracecount > 0 && !$('.r5_c2').is(':hidden') && processBetTraceCore.thisOrderMaybeLose) {
            	 layer.alert('您的追号方案可能亏损，建议您修改方案！',{skin:'layui-layer-dialog layui-layer-rim',type:1,btn:["确定"],offset:150},function(i){
            		 layer.close(i);
            	 });
             }
         });
    });
    
    //六合彩下注
    function markBetInfo(){
    	var trs=$("#BetNumberListContanior").find("tr"),
		nowbalance = parseFloat($("#balance").attr("balance").replace(/,/g,"") || 0)||0,	//账户总余额
		allAmount=(parseFloat($("#s_allTotamounts").text())||0),	//下注金额
		playCode = $(".tab .play_tab input[type=radio]:checked").attr("c"),
		groupCode = $("#Bm_general li.current a").attr("p").split("_")[2];
		//判断金额是否足够
    	if (nowbalance < allAmount) {
	    	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
	         return false;
	    }
		var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
		$.getJSON(JIEBAO.base+"/lotteryBet/token.do",function(data){
			if(data.success==0){
        		 layer.msg(data.msg,{icon: 5});
                 return false;
        	}
			layer.close(layerIndex);
			var msg = '',myBuyArray = [],dtitle='系统提示',qiHao=Core.curQiHao;
			trs.each(function(index,item){
				item = $(item);
				myBuyArray.push({name:item.attr("n"),money:item.attr("m"),oddsId:item.attr("r"),playCode:item.attr("c")})
			});
			var postData = myBuyArray;
			var data = {
					lotCode:JIEBAO.lotCode,
					token:data.token,
					qiHao:qiHao,
					lotType:JIEBAO.lotType,
					groupCode:groupCode,
					playCode:playCode,
					data:JSON.stringify(postData)
			}
			msg += '<p>彩票种类 ： ' + $('#GameTypename').text() + '</p>';
            msg += '<p>下注期号 ： 第 ' + qiHao + ' 期</p>';
            msg += '<p>下注注数 ： ' + $('#s_allTotbets').text() + ' ( 注 )</p>';
            msg += '<p>下注总额 ： ' + allAmount + ' ( 元 )</p>';
            layer.confirm(msg, {title:dtitle,area:["400px"]}, function(){
        		 layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
        		 $.ajax({url:JIEBAO.base+"/lotteryBet/doMarkBet.do",
        			 type: 'POST',
                     dataType: 'json',
                     data:data,
                     success: function(data) {
                    	 layer.close(layerIndex);
                    	 if(!data.success){
                    		 layer.msg(data.msg,{icon: 5});
                             return false;
                    	 }
                    	 layer.msg("下单成功",{icon: 6});
                    	 clearAllItem();
                         $("#betTrace").hide();
                     },
                     successIntercept: function(data){
                    	 layer.close(layerIndex);
                     }
        		 });
        	 }, function(i){
        		 layer.close(i);
        	 });
		})
    }
});
// 下注情况数据
function getBcLotteryOrder(page){
	$.ajax({
		url:JIEBAO.base+'/lotteryBet/getBcLotteryOrder.do',
		data:{
			code :JIEBAO.lotCode,
			rows :10,
			page :page,
			},
		success:function(r){
			var betListHtml ='';
			var list = r.page.list;
			var results = r.page;
			if(list.length==0){
				var dateTime = new Date();
				var year=dateTime.getFullYear();
				var month=dateTime.getMonth()+1;
				var date=dateTime.getDate();
				if(month<10){
					month =  "0"+month;
				}else{
					month = month;
				}
				if(date<10){
					date = "0" +date;
				}else{
					date = date;
				}
				var riqi = year+"-"+month+"-"+date;
				$("#betListBetween").text('报表日期：'+riqi);
				betListHtml = '<tr><td colspan="11">没有符合查询的资料...</td></tr>';
				$("#betOrderTblFoot").hide();
			}else{
				for(var i=0;i<list.length;i++){
					if(list[i].lotteryHaoMa == null || list[i].lotteryHaoMa==''){
						list[i].lotteryHaoMa = '- -';
	    			}
					var dateTime = list[0].createTime.split(" ");
					var data = dateTime[0];
					$("#betListBetween").text('报表日期：'+data);
					betListHtml+='<tr>'
					betListHtml+='<td><a style="width: 100%;" class="BetInf" orderid="'+list[i].orderId+'"lotcode="'+list[i].lotCode+'">'+list[i].orderId+'</a></td>'
					betListHtml+='<td>'+list[i].createTime+'</td>'
					betListHtml+='<td>'+list[i].qiHao+'</td>'
					betListHtml+='<td>'+list[i].playName+'</td>'
					betListHtml+='<td>'+list[i].multiple+'</td>'
					betListHtml+='<td>'+list[i].lotteryHaoMa+'</td>'
					betListHtml+='<td>'+list[i].buyMoney+'</td>'
					if(list[i].winMoney != null){
						betListHtml+='<td>'+list[i].winMoney+'</td>'
					}else{
						betListHtml+='<td>0.00</td>'
					}
					// 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
					if(list[i].status == 1 ){
						betListHtml+='<td><span style="background-color: #428bca;border-radius: .25em;padding: 0.2em .6em 0.3em;">未开奖 </span></td>'
							if(!JIEBAO.unCancleOrder){
								betListHtml+='<td><a class="cheDan" orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'">撤单</a></td>'
							}else{
								betListHtml+='<td></td>';
							}
					}else if(list[i].status == 2 ){
						betListHtml+='<td><span style="background-color: #5cb85c;border-radius: .25em;padding: 0.2em .6em 0.3em;">已中奖 </span></td><td></td>'
					}else if(list[i].status == 3 ){
						betListHtml+='<td><span style="background-color: #d9534f;border-radius: .25em;padding: 0.2em .6em 0.3em;">未中奖 </span></td><td></td>'
					}else if(list[i].status == 4 ){
						betListHtml+='<td><span style="background-color: #5bc0de;border-radius: .25em;padding: 0.2em .6em 0.3em;">撤单 </span></td><td></td>'
					}else if(list[i].status == 5 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚成功 </span></td><td></td>'
					}else if(list[i].status == 6 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚异常 </span></td><td></td>'
					}else if(list[i].status == 7 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">开奖异常 </span></td><td></td>'
					}else if(list[i].status == 8){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">和局</span></td><td></td>'
					}
				}
				$("#betPageAmount").html(r.subBuyMoney);
				$("#betAmount").html(r.sumBuyMoney);
				$("#betOrderTblFoot").show();
			}
			$("#betList").html(betListHtml);
			pageChuLi(results,page,'xz');
			$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 600);
		}
	})
}

var Core ={
	maxItem: 100,// 最大投注记录
	price: 2,// 每注2块钱
	changeColor:function (a, e, g, c) {// 修改皮肤样式
	    $("#jsStyle").remove();
	    return $("head").append("<style id='jsStyle'>body { background: " + a + 
	    		" !important; }#nav .current,#bettop_menu .current,#Bm_general > li.current > a,#Bm_optional > li.current > a,.open_result_no span," +
	    		"#srcListType li.current > a,#userinfotop_menu li.current > a,.selected,.row.r5 .current > a,.step > .current{ background-color:  " + e +
	            " !important; }.open_result_no > span,#gamebuy,.vote_total,.vote_total > div{ border-color: " + g + 
	            " !important; }.no_sum_title,.digits{ background-color: " +
	            g + " !important; }#gamebuy,.positionre > a,.arr_top > i{ color: " + g + " !important; }.row_con_c1_btm .fa-stack i{ color: " + g + 
	            " !important; } .row_con_c1_btm a { border-color: " + g + " !important; color: " + g + " !important; }.fa-customColor > i { color: " + c + 
	            " !important; } .fa-customColor > .fa-circle { color: " + g + " !important; } #betplay_data table .head th{background-color: "+e+" !important;} .table_ball td.active{background-color:"+g+" !important;} .moneylist{background-color:"+e+" !important;border:2px solid "+g+";}</style>")
	},
	rgb2hex:function(rgb){// 将rgb 转成16进制
		if(rgb.indexOf("#")!=0){ 
			rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/); 
			function hex(x) { 
				return ("0" + parseInt(x).toString(16)).slice(-2); 
			} 
			rgb= "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
		} 
		return rgb; 
	},
	strLimit: function(a, e) {
        e = e || 25;
        if (a.length < e) return a;
        var g = a;
        a.length > e && (g = g.substring(0, e) + "...");
        return g
    },
    countTotalBet: function() {
        var a = 0,e = 0;
        $("#BetNumberListContanior table tbody tr").each(function(g, c) {
            var h = Number(parseFloat($(this).find("td:eq(5)").text()));
            e = Core.__FloatAdd(e, h);
            h = Number(parseInt($(this).find("td:eq(3)").text()));
            a = Core.__FloatAdd(a, h)
        });
        $("#s_allTotamounts").text(parseFloat(e.toString().match(/\d+(\.\d{1,3})?/)[0] || 0));
        $("#s_allTotbets").text(a);
    },
	getLotLast:function(qiHao){// 获得最近一期开奖结果
		if(!qiHao)return;
		$.getJSON(JIEBAO.base+"/lottery/lotLast.do?lotCode="+JIEBAO.lotCode+"&qiHao="+qiHao, function(last){
			if(last){
				if(last.haoMa  && last.haoMa.indexOf("?")>-1){
					// 最近一期还没有开奖
					clockArea.reGetLotLast(last.qiHao);return;
				}
				clockArea.stopReGetLotOp();
				//Core.renderLast(last);
				Core.getLotOp();
				$("#betplay_data").find("input[name='playId']:checked").click();
			}
		});
	},
	renderLast:function(last){// 显示最近一期开奖结果
		var hms=last.haoMa.split(","),h="",noClass="balls_10",wenHao=false;
		if(hms[0]=="?"){
			wenHao=true;
			if(JIEBAO.lotCode=='PCEGG'){
				noClass="balls_7";
				h=h+'<span class="bg_black3_content">'+Core.getHaoMas()+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+Core.getHaoMas()+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+Core.getHaoMas()+'</span><span class="bg_black3_content">=</span><span class="bg_black3_content">'
					+ Core.getHaoMas(28) +'</span>';
			}else if(JIEBAO.lotCode=='SFLHC' || JIEBAO.lotCode=='LHC' || JIEBAO.lotCode=='AMLHC'|| JIEBAO.lotCode=='WFLHC' || JIEBAO.lotCode=='TMLHC' || JIEBAO.lotCode=='HKMHLHC'){
				noClass = "balls_7";
				for(var j=0,jlen=hms.length;j<jlen;j++){
					if(j==jlen-1){
						h += '<font style="width:45px;">+</font>';
					}
					h += '<span class="bg_black3_content" style="width:45px;">'+Core.getHaoMas()+'</span>';
				}
			}else{
				noClass="balls_"+hms.length;
				for(var j=0,jlen=hms.length;j<jlen;j++){
					h=h+'<span class="bg_black3_content">'+Core.getHaoMas()+'</span>';
				}
			}
		}else{
			if(JIEBAO.lotCode=='PCEGG'){
				noClass="balls_7";
				h=h+'<span class="bg_black3_content">'+hms[0]+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+hms[1]+'</span><span class="bg_black3_content">+</span><span class="bg_black3_content">'
					+hms[2]+'</span><span class="bg_black3_content">=</span><span class="bg_black3_content">'
					+(hms[0]=="?"?"?":(parseInt(hms[0])+parseInt(hms[1])+parseInt(hms[2])))+'</span>';
			}else if(JIEBAO.lotCode=='SFLHC' || JIEBAO.lotCode=='LHC'  || JIEBAO.lotCode=='AMLHC' || JIEBAO.lotCode=='WFLHC' || JIEBAO.lotCode=='TMLHC' || JIEBAO.lotCode=='HKMHLHC'){
				noClass="balls_lhc";
				for(var j=0,jlen=hms.length;j<jlen;j++){
					if(j==jlen-1){
						h += '<font>+</font>';
					}
					h += '<span class="bg_black3_content ball_'+hms[j]+'">'+hms[j]+'<br><em style="font-size:14px;">'+Core.mark.zodiacName(hms[j])+'</em></span>';
				}
			}else{
				noClass="balls_"+hms.length;
				for(var j=0,jlen=hms.length;j<jlen;j++){
					h=h+'<span class="bg_black3_content">'+hms[j]+'</span>';
				}
			}
		}
		$("#last_result_no").removeClass("balls_3 balls_5 balls_lhc balls_7 balls_10").addClass(noClass).html(h);
		if(wenHao){
			Core.scrollFun();
		}
		if(last.qiHao){
			$("#last_result_qihao").html("第"+last.qiHao+"期");
		}else{
			$("#last_result_qihao").html("无期数");
		}
	},
	scrollFun:function(){
		var uls = $("#last_result_no").find("ul");
		clearTimeout(Core.scrollFunTimeout);
		if(uls.length){
			function scrollFn(){
				uls.each(function(i){
					var $it=$(this),h=$it.height(),st=parseInt($it.css("top"));
					if(h+st>60){
						st = st-60;
					}else{
						st=0;
					}
					$it.css({"top":st});
				});
			}
			Core.scrollFunTimeout = setInterval(scrollFn,80);
			scrollFn();
		}
	},
	getHaoMas:function(max){
		if(max){
			var h="<ul>";
			for(var i=0;i<max;i++){
				h+="<li>"+i+"</li>";
			}
			h+="</ul>";
			return h;
		}
		switch(JIEBAO.lotType){
			case "3":
			case "5":
				var max=11;
				if(JIEBAO.lotType==5){
					max=12;
				}
				var h="<ul>";
				for(var i=1;i<max;i++){
					if(i<10){
						h+="<li>0"+i+"</li>";
					}else{
						h+="<li>"+i+"</li>";
					}
				}
				h+="</ul>";
				return h;
			case "6":
			case "66":
				var max = 49;
				var h="<ul>";
				for(var i=1;i<=max;i++){
					if(i<10){
						h+='<li>0'+i+'</li>';
					}else{
						h+='<li>'+i+'</li>';
					}
				}
				h+= "</ul>";
				return h;
			default:
				var h="<ul>";
				for(var i=0;i<10;i++){
					h+="<li>"+i+"</li>";
				}
				h+="</ul>";
				return h;
		}
	},
	getLotOp:function(){// 获得当前彩票的倒计时，历史开奖结果和最近一期开奖结果
		if(!JIEBAO.lotCode)return;
		$.getJSON(JIEBAO.base+"/lottery/lotOp.do?lotCode="+JIEBAO.lotCode, function(json){
			if(json){clockArea.stopReGetLotOp();
				if(!json.success && json.msg){
					$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">'+json.msg+'</div>');
					$("#Bm_general").empty();
					var hms="?,?,?,?,?";
					switch(JIEBAO.lotCode){
					case "JSSB3":
					case "AHK3":
					case "HBK3":
					case "HEBK3":
					case "SHHK3":
					case "GXK3":
					case "BJK3":
					case "GSK3":
					case "JXK3":
					case "FFK3":
					case "WFK3":
					case "JPK3":
					case "KRK3":
					case "HKK3":
					case "AMK3":
					case "FC3D":
					case "PL3":
					case "PCEGG":
					case "JND28":hms="?,?,?";break;
					case "BJSC":
					case "XYFT":
					case "LXYFT":
					case "SFSC":hms="?,?,?,?,?,?,?,?,?,?";break;
					case "SFLHC":
					case "TMLHC":
					case "WFLHC":
					case "HKMHLHC":
					case "AMLHC":
					case "LHC":hms="?,?,?,?,?,?,?";break;
					}
					json.last={"haoMa":hms};
				}else{
					if($("#Bm_general").find("li").length==0){
						if(json.palyGroups && json.palyGroups.length){// 玩法大类
							var arr = json.palyGroups;
							var h = "";
							for(var i = 0,len=arr.length;i<len;i++){
								h = h+'<li><a href="javascript:;" class="btn b0" p="group_'+arr[i].lotType+"_"+arr[i].code+'">'+arr[i].name+'</a></li>'
							}
							$("#Bm_general").html(h).find("a:first").click();
						}else{
							$("#Bm_general").html('<li class="current">该彩种没有玩法</li>');
						}
					}
					if(json.last && json.last.haoMa  && json.last.haoMa.indexOf("?")>-1){
						// 最近一期还没有开奖
						clockArea.reGetLotLast(json.last.qiHao);
					}
				}
				// 当前期信息，倒计时，期号
				Core.time.startTime(json.current);
				if(json.history&&json.history.length){// 最近开奖
					var arr=json.history;
					var h = "",obj=null,hms=null;
					for(var i = 0,len=arr.length;i<len;i++){
						obj = arr[i];
						hms=obj.haoMa.split(",");
						h=h+'<li class="bg_black3 hli ';
						switch(JIEBAO.lotCode){
						case 'PCEGG':
							h=h+'qs7"><span class="qh">'+obj.qiHao+':</span><span>'
								+hms[0]+'</span><span>+</span><span>'
								+hms[1]+'</span><span>+</span><span>'
								+hms[2]+'</span><span>=</span><span>'
								+(hms[0]=="?"?"?":(parseInt(hms[0])+parseInt(hms[1])+parseInt(hms[2])))+'</span>';
							break;
						case 'SFLHC':
						case 'TMLHC':
						case 'WFLHC':
						case 'HKMHLHC':
						case 'LHC':
						case 'AMLHC':
							h=h+'qs10"><span class="qh">'+obj.qiHao+':</span>';
							for(var j=0,jlen=hms.length;j<jlen;j++){
								if(j == jlen-1){
									h+='<span>+</span>'
								}
								h=h+'<span>'+hms[j]+'</span>';
							}
							break;
						default:
							h=h+'qs'+hms.length+'"><span class="qh">'+obj.qiHao+':</span>';
							for(var j=0,jlen=hms.length;j<jlen;j++){
								h=h+'<span>'+hms[j]+'</span>';
							}
						}
						h=h+'</li>';
					}
					$("#lotHistory > ul").html(h);
				}else{
					$("#lotHistory > ul").html("");
				}
				if(json.last){
					Core.renderLast(json.last);
				}
			}else{
				$("#betplay_data").html('<div style="text-align:center;height:270px;line-height: 270px;font-size: 100px;">系统发生错误，请联系客服</div>');
				$("#Bm_general").empty();
			}
		});
	},
	getPlayType:function(){
		if(Core.gamePlay.isLoaded(JIEBAO.lotType)){
			$("#Bm_general").find("a:first").click();
		}else{
			$.get(JIEBAO.base+"/member/"+JIEBAO.folder+"/module/lottery/lot_play_type_"+JIEBAO.lotType+ "_" + JIEBAO.folder +".html?v="+JIEBAO.caipiao_version, function(html){
				Core.gamePlay.strToTpl(JIEBAO.lotType,html);
				$("#Bm_general").find("a:first").click();
			},"html");
		}
	},
	__FloatAdd: function(a, e) {
        var g, c;
        try {
            g = a.toString().split(".")[1].length
        } catch (h) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (l) {
            c = 0
        }
        g = Math.pow(10, Math.max(g, c));
        return (a * g + e * g) / g
    },
    __FloatSubtraction: function(a, e) {
        var g, c, h;
        try {
            g = a.toString().split(".")[1].length
        } catch (l) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (m) {
            c =
                0
        }
        h = Math.pow(10, Math.max(g, c));
        return ((a * h - e * h) / h).toFixed(g >= c ? g : c)
    },
	__FloatMul: function(a, e) {
    	var g = 0,
            c = a.toString(),
            h = e.toString();
        try {
            g += c.split(".")[1].length
        } catch (l) {}
        try {
            g += h.split(".")[1].length
        } catch (m) {}
        return Number(c.replace(".", "")) * Number(h.replace(".", "")) / Math.pow(10, g)
    },
    __FloatDiv: function(a, e) {
        var g = 0,
            c = 0,
            h=a.toString(), l=e.toString();
        try {
            g = h.split(".")[1].length
        } catch (m) {}
        try {
            c = l.split(".")[1].length
        } catch (q) {}
        h = Number(h.replace(".", ""));
        l = Number(l.replace(".",""));
        return h / l * Math.pow(10, c - g)
    }
};
// 玩法小类管理
Core.gamePlay=function(){
	var l={},typeIsLoaded={};
	var E=function(b){
		b=b.split("\u263b");
		var k={},h;
		for(h in b){
			var l=b[h].split("\u263a");
			if(l.length!=2)continue;
			var s = l[1].split("\u263d"),r=[];
			for(h in s){
				var e=s[h].split("\u263e");
				if(e.length!=2)continue;
				r[r.length]=e[0];
				k[e[0]]=e[1];
			}
			k[l[0]]=r;
		}
		return k
	};
	return{
		strToTpl:function(a,b){
			l=$.extend(l,E(b));
			typeIsLoaded[a]=a;
			return this
		},
		isLoaded:function(a){
			return typeIsLoaded[a];
		},
		switchGroup:function(a){
			var r = l[a];
			if(r && r.length>0){
				Core.gamePlay.switchPlay(r[0]);
			}else{
				Core.betting.resetSumbetCount();
				$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">没有提供玩法</div>');
			}
		},
		switchPlay:function(a){
			Core.betting.resetSumbetCount();
			if(!a || !l[a]){
				$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">没有提供玩法</div>');
				return;
			}
			$("#betplay_data").html(l[a]);
			var playCode=$("#betplay_data").find("input[name='playId']:checked").attr("c");
			if(playCode){
				$.getJSON(JIEBAO.base+"/lottery/getRateOp.do?lotCode="+JIEBAO.lotCode+"&playCode="+playCode+"&lotType="+JIEBAO.lotType,function(json){
					if(json){
						$("#gcrate").html(json.peiLv);
						if(json.hasOwnProperty("fanShui")){
							$("#rebateShow").html(json.fanShui+"%");
						}
						if(json.hasOwnProperty("lengRe") || json.hasOwnProperty("yiLou")){
							var g = $("#betplay_data .num_btn"),e=json.yiLou,a=json.lengRe;
							if(e && e.length){
								for (var i=0,l=e.length;i<l;i++){
									for (var d=0,dlen=e[i].length;d<dlen;d++) g.eq(i).find("a[num]").eq(d).nextAll("i").text(e[i][d]);
								}
							}
							if(a && a.length){
								for (var i=0,l=a.length;i<l;i++){
									for (var d=0,dlen=a[i].length;d<dlen;d++) g.eq(i).find("a[num]").eq(d).nextAll("s").text(a[i][d]);
								}
							}
						}
					}
				});
			}
		}
	}
}();

// 倒计时
Core.time={};
(function(){
	var b=null,k=0,cur=null,
	p=function(){
		var b;
		try{b=window.performance.now()}catch(k){b=Date.now()}
		return b
	};
	Core.time.startTime=function(a){// 开始倒计时
		if(!a){
			clockArea.stop();
			clockArea.setExpireTime(0).setRound("").setStatus("尚未开盘");
			cur=null;Core.curQiHao=null;
			return;
		}
		cur=a;
		Core.curQiHao=a.qiHao;
		Core.time.setServerTime(cur.serverTime);
		clockArea.setRound(cur.qiHao).setStatus("开放下注").start();
		processBetTraceCore.generateBetView();
	}
	Core.time.get_state=function(){
		return !cur.state?0:cur.state;
	}
	Core.time.setServerTime=function(h){// 设置服务器返回的当前时间
		b=new Date(h.replace(/-/g,"/"));
		k=p();return this
	};
	Core.time.getServerTime=function(){// 根据服务器返回的时间，计算服务器当前时间
		if(null==b)throw Error("还没有同步时间","Core.time.getServerTime");
		return new Date(b.getTime()+(p()-k));
	}
	Core.time.getCountDown=function(){// 获得倒计时秒数
		if(!cur || !cur.activeTime)!1;
		var b=new Date(cur.activeTime.replace(/-/g,"/"));
		if(!(b instanceof Date)||isNaN(b.getTime()))return !1;
		var k=0;
		try{k=this.getServerTime().getTime()}catch(h){k=Date.now()}
		b=(new Date(b-k)).getTime()/1E3;
		return b;
	};
})();

// 投注相关
Core.betting=function(){
	return {
		resetSumbetCount: function() {// 重置
			bet_result = {};
			$("#Immediate , #allTotamounts").text(0);
			$("#betplay_data").find(".no_input_box textarea").val("");
	    },
	    sumbetCount: function(eventEle) {// 计算投注注数和投注金额入口
			var c2 =$("#betplay_data").find(".row_con_c2"),
				_type=c2.attr("_type"),
				_row = c2.attr('_row'),
		        _column = c2.attr('_column'),
		        _val = c2.attr('_val');
	        bet_result = {};
	        switch (_type) {
	            case "1":
	                Core.betting.__Duplex(_row, _column, _val);
	                break;
	            case "3":
	                Core.betting.__Positioninggall(_row, _column);
	                break;
	            case "4":
	                Core.betting.__ElectionsAndValues(_row, _column);break;
	            case "5":
	                Core.betting.__Constitute(_row, _column, _val, c2.attr('_arbitrary'));
	                break;
	            case "8":
	                Core.betting.__Arbitrary_Duplex(_row, _column);break;
	            case "11":
	                Core.betting.__Nrepeat(_row, _column);break;
	            case "12":
	                Core.betting.__UseC(_row, _column);break;
	            case "13":
	            	 Core.betting._HuYi2Row(c2,eventEle);break;
	        }
	        bet_result.types = _type
	    },
	    sumTextareaBetCount: function(_type,_vals) {// 计算单式投注注数和投注金额入口
	    	bet_result = {};
	    	bet_result.types = _type;
	    	var zhuShu = 0, ma="";
	    	if(_vals){
	    		for(var i=0,len=_vals.length;i<len;i++){
		    		if(_vals[i]){
		        		ma=ma+";"+_vals[i];
		        		zhuShu++;
		    		}
		    	}
	    	}
	        bet_result.betCount = zhuShu;
	        bet_result.myarray =ma?ma.substring(1):"";
	        bet_result.errorTagStr = "";
	        bet_result.optional = "";
	        $("#Immediate").text(zhuShu);
	        var l = Core.price,s=$("#buymultiple").val(),t=$("#selectdollarunit").val();
	        l *=Core.__FloatMul(zhuShu,s);
            l = Core.__FloatDiv(l, t);
	        $("#allTotamounts").text(l);
	    },
	    _HuYi2Row:function(con,eventEle){
	    	var arr = ["",""],n=eventEle.attr("num");
	    	if(eventEle.hasClass("selected")){
	    		if(n.length==2){
		    		con.find("a[num='"+n.substring(1)+"']").removeClass("selected");
		    	}else{
		    		con.find("a[num='"+n+n+"']").removeClass("selected");
		    	}
	    	}
	    	con.find(".num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	arr[a] +=$(j).attr("num");
	            });
	        });
	    	var zhuShu=arr[0].length/2*arr[1].length;
	    	Core.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Duplex: function(_row, _column, _val) {
	    	_row = _row || 1;
	    	_column = _column || 1;
	        var arr = [],row = 0,column = 0,zhuShu = 1;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
	            		if("undefined" != typeof arr[a]){
	            			arr[a] += i;
	            		}else{
	            			arr[a] =i;
	            			row++;
	            		}
	            	}
	            })
	        });
	        if (row != _row || column < _column) {
	        	Core.betting.resetSumbetCount();
	        	return false;
	        }
	        for (var t in arr) zhuShu *= arr[t].length;
	        if(_val){
	        	zhuShu=zhuShu/_val;
	        }
	        Core.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Positioninggall: function(a, e) {
	        e = e || 1;
	        var g = 0,
	            c = 0,
	            h = 0,
	            l = [],q=JIEBAO.lotCode,
	            m = Array(parseInt(a || 5));
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            		}else{
	            			m[a] =i;
	            		}
	            	}
	            });
	            h++
	        });
	        if (0 > h || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        for (var t in m) g += m[t].length;
		        if ("BJSC"  == q ||"SFSC"  == q || "SD11X5" == q || "SH11X5" == q || "GX11X5" == q || "JX11X5" == q || "GD11X5" == q||q=="XYFT" || q=="LXYFT") g /= 2;
	        Core.betting.__showAndSaveBet(g, m)
	    },
	    __ElectionsAndValues: function(a, e) {
	        var c = 0,
	            h = 0,
	            l = 0,
	            m = [];
	        switch (e) {
	            case "J_3":
	                var q = [10, 54, 96, 126, 144, 150, 144, 126, 96, 54];
	                break;
	            case "S_3":
	                q = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1];
	                break;
	            case "B_3":
	            	q = [0, 0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 8, 8, 6, 6, 4, 4, 2, 2];
	        		break;
	            case "G_3":
	                q = [-1, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1];
	                break;
	            case "J_2":
	                q = [10, 18, 16, 14, 12, 10, 8, 6, 4, 2];
	                break;
	            case "S_2":
	                q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
	                break;
	            case "G_2":
	                q = [-1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1];
	                break;
	            case "S_1":
	                q = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	                break;
	            case "K_3":
	                q = [0,0,0,1,3, 6, 10, 15, 21, 25, 27, 27, 25, 21, 15, 10, 6, 3, 1];
	                break;
	        }
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		h++;
	            		m.push(i);
	            	}
	            });
	            l++
	        });
	        if (l < a) return $("#Immediate , #allTotamounts").text(0), !1;
            for (var t in m) c += parseInt(q[m[t]]);
	        Core.betting.__showAndSaveBet(c, m)
	    },
	    __Constitute: function(_row, _column, _val, _arbitrary) {
	        var h = 1,l = 1,column = 0,row = 0,arr = [];
	        _arbitrary = _arbitrary || 1;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
            			arr.push(i);
	            	}
	            });
	            row++;
	        });
	        if (row != _row || column < _column) {
	        	Core.betting.resetSumbetCount();
	        	return false;
	        }
	        var a=0;
	        switch (_val) {
	            case "G_2":
	                for (a = arr.length; a > arr.length - _column; a--) h *= a;
	                break;
	            case "P_1":
	            case "P_2":
	            case "P_3":
	            case "G_3":
	            case "R_2":
	                for (a = 0; a < _column; a++) h *= arr.length - a;
	                for (a = _column; 0 < a; a--) l *= a;
	                h /= l;
	                break;
	        }
	        var r;
	        switch (_arbitrary) {
	            case 1:
	                break;
	            default:{
	            	h *= $(".no_frame span#positioninfo").text();
                    r = Core.betting.position_OptionalArr().join(",");
	            }
	        }
	        Core.betting.__showAndSaveBet(h, arr,false, r)
	    },
	    position_OptionalArr: function() {
	        var e = [];
	        $(".row_con_c2 input[name^='position_']").each(function() {
	            $(this).prop("checked") && e.push($(this).val())
	        });
	        return e
	    },
	    __Arbitrary_Duplex: function(a, e) {
	        var c = 0,
	            h = 0,
	            m = [],
	            q = [];
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            			q[a] += 1
	            		}else{
	            			m[a] =i;
	            			q[a] = 1;
	            			h++;
	            		}
	            	}
	            });
	        });
	        if (h < a || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        var g = Core.betting.__Elections(q, e);
	        while(m.length<5){
	        	m[m.length]="";
	        }
	        Core.betting.__showAndSaveBet(g, m)
	    },
	    __Elections: function(a, e) {
	        var g = 0,
	            c = $.map(a, function(a) {
	                if ("" !== a) return a
	            }),
	            c = Core.betting.__C_combine(c, e);
	        switch (e) {
	            case "2":
	                for (var h in c) g += c[h][0] * c[h][1];
	                break;
	            case "3":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2];
	                break;
	            case "4":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2] * c[h][3]
	        }
	        return g
	    },
	    __C_combine: function(a, e) {
	        for (var g = [[]], c = [], h = 0, l = a.length; h < l; ++h)
	            for (var m = 0,q = g.length; m < q; ++m)(g[m].length < e - 1 ? g : c).push(g[m].concat([a[h]]));
	        return c
	    },
	    __Nrepeat: function(a, e) {
	        var g = 1,
	            c = 0,
	            h = 0,
	            l = [],
	            m = [];
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a].push(i);
	            			l[a] += i
	            		}else{
	            			m[a]=[i];
	            			l[a] =i;
	            			h++;
	            		}
	            	}
	            })
	        });
	        if (h < a || c < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        switch (e) {
	            case "2":
	                for (var t in m) g *= Core.betting.___P(m[t].length, 1);
	                g -= Core.betting.arrayIntersection(m[0],m[1]).length;
	                break;
	            case "3":
	                g = m[0].length * m[1].length * m[2].length;
	                var q = Core.betting.arrayIntersection(m[0], m[1]).length * m[2].length;
	                var t = Core.betting.arrayIntersection(m[1], m[2]).length * m[0].length;
	                var r = Core.betting.arrayIntersection(m[2], m[0]).length * m[1].length,
	                    y = 2 * Core.betting.count(Core.betting.array_intersect(m[0], m[1], m[2])),
	                    g = g - q - t - r + y
	        }
	        Core.betting.__showAndSaveBet(g, l)
	    },
	    __UseC: function(a, e) {
	        var g = [],h = 0,l = 0,m = $("#betplay_data .row_con_c2").attr("_g11") || false;
	        $("#betplay_data .row_con_c2 .num_btn").each(function(a, e) {
	            $(e).find("a.selected").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		l++;
	            		g.push(i);
            			h++;
	            	}
	            })
	        });
	        if (m) {
	            if (h < a || l < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        } else if (h != a || l < e) return $("#Immediate , #allTotamounts").text(0), !1;
	        m = Core.betting.__C_combine(g, e).length;
	        Core.betting.__showAndSaveBet(m, g)
	    },
	    arrayIntersection: function(a, e) {
	        for (var g = 0, c = 0, h = []; g < a.length && c < e.length;) a[g] < e[c] ? g++ : (a[g] > e[c] || (h.push(a[g]), g++), c++);
	        return h
	    },
	    ___P: function(a, e) {
	        return Core.betting.___factorial(a) / Core.betting.___factorial(a - e)
	    },
	    ___C: function(a, e) {
	        return e > a ? 0 : Core.betting.___P(a, e) / Core.betting.___factorial(e)
	    },
	    ___H: function(a, e) {
	        return Core.betting.___C(e + a - 1, a - 1)
	    },
	    ___factorial: function(a) {
	        return 1 == a || 0 == a ? 1 : a * Core.betting.___factorial(a -1)
	    },
	    array_intersect: function(a) {
	        var e = {},
	            g = arguments.length,
	            c = g - 1,
	            h = "",
	            l = {},
	            m = 0,
	            q = "";
	        a: for (h in a) b: for (m = 1; m < g; m++) {
	            l = arguments[m];
	            for (q in l)
	                if (l[q] === a[h]) {
	                    m === c && (e[h] = a[h]);
	                    continue b
	                }
	            continue a
	        }
	        return e
	    },
	    count: function(a, e) {
	        var g, c = 0;
	        if (null === a || "undefined" ===
	            typeof a) return 0;
	        if (a.constructor !== Array && a.constructor !== Object) return 1;
	        "COUNT_RECURSIVE" === e && (e = 1);
	        1 != e && (e = 0);
	        for (g in a) a.hasOwnProperty(g) && (c++, 1 != e || !a[g] || a[g].constructor !== Array && a[g].constructor !== Object || (c += this.count(a[g], 1)));
	        return c
	    },
	    __showAndSaveBet: function(zhuShu, arr,c, h) {// 显示并记录投注信息
	    	zhuShu = zhuShu || 0;
	    	arr = arr || [];
	        c = c || [];
	        h = h || "";
	        bet_result.betCount = zhuShu;
	        var ma="";
	        for(var i=0,len=arr.length;i<len;i++){
	        	if(arr[i]){
	        		ma=ma+","+arr[i];
	        	}else{
	        		ma=ma+",-";
	        	}
	        }
	        bet_result.myarray =ma.substring(1);
	        bet_result.errorTagStr = c.toString();
	        bet_result.optional = h
	        $("#Immediate").text(zhuShu);
	        var l = Core.price,s=$("#buymultiple").val(),t=$("#selectdollarunit").val();
	        l *=Core.__FloatMul(zhuShu,s);
            l = Core.__FloatDiv(l, t);
	        $("#allTotamounts").text(l);
	    }
	}
}();
// 倒计时
var clockArea=function(){
	var b=$("#lotClock"),
		a=b.find(".clock_no"),
		d=b.find("#nowroundsShow"),
		g=b.find(".status"),
		h=b.find(".circle .hour"),
		k=b.find(".circle .min"),
		l=b.find(".circle .sec"),t1,t2;
	return obj={
		setExpireTime:function(b){
			var c=Math.floor(b/3600),
			e=Math.floor(b%3600/60);
			b=Math.floor(b%60);
			a.text(("0"+c).slice(-2)+":"+("0"+e).slice(-2)+":"+("0"+b).slice(-2));
			h.css("transform","rotate("+15*c+"deg)");
			k.css("transform","rotate("+6*e+"deg)");
			l.css("transform","rotate("+6*b+"deg)");
			return this
		},
		setRound:function(a){
			d.text(a?"第 "+a+" 期":"无期数");
			return this
		},
		setStatus:function(a){
			g.text(a);
			return this
		},
		start:function(){
			this.stop();
			this.tick()
		},
		stop:function(){
			clearTimeout(t1);
			return this
		},
		tick:function(){
			var a=this;
			clearTimeout(t1);
			var z=$("#nowGP");
			if(z.length==0 || !z.hasClass("current"))return this;
			var  e=Core.time.getCountDown();
			var  s = Core.time.get_state();
			if(e===!1)return;
			if(0<e){
				a.setExpireTime(Math.floor(e)),
				10>=e&&soundNotify.play();
				if(s==1 && JIEBAO.lotType == 6){	//六合彩处于封盘状态
					a.setStatus("停止下注");
					$("#Bm_general").empty();
					$("#betplay_data").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">封盘中...</div>');
				}
			}else{
				if($("#betplay_data").text().trim() == '封盘中...'){
					layer.alert("您好，第 "+Core.curQiHao+" 期 已开放下注。", {time:30000});
				}else{
					layer.alert("您好，第 "+Core.curQiHao+" 期 已截止，投注时请确认您选的期号。", {time:30000});
				}
				a.setStatus("停止下注");
				setTimeout(function(){Core.getLotOp()},200);
			}
			t1=setTimeout(function(){a.tick()},1000);return this
		},
		reGetLotLast:function(qiHao){
			clearTimeout(t2);
			var z=$("#nowGP");
			if(z.length==0 || !z.hasClass("current"))return this;
			t2 = setTimeout(function(){Core.getLotLast(qiHao)},2500+Math.floor(Math.random()*1000));return this
		},
		stopReGetLotOp:function(){
			clearTimeout(t2);return this
		}
	}
}();
// 开奖提示语音
var soundNotify={};
$(function(){
	var aa=$("#soundNotification"),e=new Audio;
	e.canPlayType("audio/mpeg")?(e.type="audio/mpeg",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.mp3"):
	e.canPlayType("audio/ogg")?(e.type="audio/ogg",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.ogg"):
	(e.type="audio/mp4",e.src=JIEBAO.base+"/common/template/lottery/jiebao/sounds/tick.m4a");
	soundNotify={
		a:aa,
		$wrap:aa.find(".fa-stack-2x"),
		mute:function(){
			this.$wrap.removeClass("fa-circle-thin").addClass("fa-ban");
			return this
		},
		unMute:function(){
			this.$wrap.removeClass("fa-ban").addClass("fa-circle-thin");
			return this
		},
		isMute:function(a){// 是否静音
			if(null==a){
				var b;
				try{
					b=JSON.parse(localStorage.getItem("muteTickSound")||!1)
				}catch(c){
					b=!1
				}
				return b
			}
			try{
				localStorage.setItem("muteTickSound",!!a)
			}catch(e){}
			this.renderView();
			return this
		},
		play:function(){
			if(!this.isMute()){
				try{
					e.pause(),
					e.currentTime=0,
					e.play()
				}catch(a){}
			}
			return this
		},
		stop:function(){
			e.pause();
			e.currentTime=0;
			return this
		},
		renderView:function(){
			if(this.isMute())this.mute();else this.unMute();return this
		}
	};
	aa.click(function(){
		soundNotify.isMute(!soundNotify.isMute());
	});
	soundNotify.renderView();
});
// 机选
$(function() {
	var rand=function(a, e) {// 随机数
        var g = arguments.length;
        if (0 === g) a = 0, e = 2147483647;
        else if (1 === g) throw Error("Warning: rand() expects exactly 2 parameters, 1 given");
        return Math.floor(Math.random() * (e - a + 1)) + a
    },
    in_array=function(a,e, g) {// 数据是否在数字里面
        var c = "";
        if (g){
        	for (c in e) {
                if (e[c] === a) return !0
            }
        }else{
        	for (c in e) {
                if (e[c] == a) return !0
            }
        }
        return !1
    };
    var h = {// 选择投注号码方法
        select: function(a, c) {
            null == c && (c = 1);
            if (c >= a.length) return a.click(), !0;
            var e;
            for (var g = 0; g < c; g++){
            	e = rand(0, a.length - 1);
            	if(g === c - 1){
            		a.eq(e).click() 
            	} else{
            		if("checkbox" === a.eq(e).attr("type")){
            			a.eq(e).prop("checked", !0)
            		} else{
            			a.eq(e).addClass("selected");
            		}
            	}
            	a.splice(e, 1)
            }
            return !0
        },
        selectByLine: function(a, c) {
            var e=this;
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a =parseInt(a);
            return $(c).each(function(c, g) {
                return e.select($(this).find("a[num]:not(.selected)"), a)
            })
        },
        selectChkbox: function(a) {
            null == a && (a = 1);
            a = parseInt(a);
            var c = $(".radio_select input").prop("checked", !1);
            return this.select(c, a)
        },
        selectXor: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a instanceof Array || (a = parseInt(a));
            var e = this;
            return $(c).each(function(g, h) {
                var l = $(this).find("a[num]:not(.selected)");
                $(c).find("a[num].selected").each(function(a, c) {
                    return l = l.filter(":not([num=" + $(this).attr("num") + "])")
                });
                return a instanceof Array ? e.select(l, a[g] || 1) : e.select(l, a)
            })
        },
        randSelector: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".num_btn");
            a = parseInt(a) || 1;
            var e = $(c).length - 1;
            if (e > a - 1) {
                var h = [],l=[],g;
                while(l.length<a){
                	g = rand(0, e);
                	if(!in_array(g, l)){
                		l.push(g);
                		h.push(c + ":eq(" + g + ")")
                	}
                }
                c = h.join(",")
            }
            return c
        }
    };
	$("#randomPlay").click(function() {// 机选
		$(".num_btn .selected").click();
        var $playId = $(".ch_radio td input[type=radio]:checked"),lotType=$playId.attr("t"),code=$playId.attr("c");
        switch (code) {
        case "dwd":
        	return h.selectByLine(1,h.randSelector(1));
        case "h3zux_zu3":
        case "q3zux_zu3":
        case "z3zux_zu3":
        case "bdw_2m":
        case "zux_z3":
        case "ebth":
        	return h.selectByLine(2);
        case "h3zux_zu6":
        case "q3zux_zu6":
        case "z3zux_zu6":
        case "zux_z6":
        case "sbtx":
        	return h.selectByLine(3);
        case "rxwf_r2zx_fs":
        	return h.selectByLine(1,h.randSelector(2));
        case "rxwf_r3zx_fs":
        	return h.selectByLine(1,h.randSelector(3));
        case "rxwf_r4zx_fs":
        	return h.selectByLine(1,h.randSelector(4));
        case "rxwf_r3zux_zu3":
        	h.selectChkbox(3);
        	return h.selectByLine(2);
        case "rxwf_r3zux_zu6":
        	h.selectChkbox(3);
        	return h.selectByLine(3);
        case "h2zx":
        case "q2zx":
        case "em_h2zux":
        case "em_q2zux":
        case "rxfs_rx2z2":
        	return h.selectXor(2);
        case "q3zx_fs":
        case "q2zx_fs":
        case "h3zx_fs":
        case "z3zx_fs":
        case "h2zx_fs":
        	if(lotType==3 ||lotType==5){// 北京赛车 //11选5
        		return h.selectXor();
        	}
        	return h.selectByLine();
        case "sxzx":
        case "h3zx":
        case "q3zx":
        case "z3zx":
        case "rxfs_rx3z3":
        	return h.selectXor(3);
        case "rxfs_rx4z4":
        	return h.selectXor(4);
        case "rxfs_rx5z5":
        	return h.selectXor(5);
        case "rxfs_rx6z5":
        	return h.selectXor(6);
        case "rxfs_rx7z5":
        	return h.selectXor(7);
        case "rxfs_rx8z5":
        	return h.selectXor(8);
        case "ethdx":
        	return h.selectXor();
        default:
        	return h.selectByLine();
        }
    });
});
var processBetTraceCore = function(){
	var $betTrace = $("#betTrace"),
		$totalQiShu=$betTrace.find("#lt_trace_count"),
		$totalMoney=$betTrace.find("#lt_trace_hmoney"),
		$betTraceData=$betTrace.find("#betTraceData"),
		$betList=$("#BetNumberListContanior");
	return{
	    generatedTraceData: false,// 是否已经生成并显示追号期号
	    thisOrderMaybeLose: false,// 可能出现亏损
	    qiHaos:[],// 可追号得期号
	    startQiShu:10,// 追号期数
	    startTimes:1,// 起始倍数
	    totalCost:0,// 总下注金额
	    profitArr:false,// 盈利情况
	    initView:function(d){// 保存当天可追号得期号，并显示追号期号
	    	this.qiHaos = d;
	    	this.generateBetView();
	    },
	    generateBetView: function() {// 生成并显示追号期号
	    	if(!Core.curQiHao || !this.qiHaos||this.qiHaos.length==0)return;
	        this.generatedTraceData = true;
	        var html = "",len = this.qiHaos.length,qh,cur=false,bb=this.startTimes;
            for (var a = 0,b=0; a < len && b<this.startQiShu;a++) {
            	qh = this.qiHaos[a];
            	if(qh<Core.curQiHao){
            		continue;
            	}
                html+= '<tr><td><input type="checkbox" value="' +qh + '"checked></td><td>';
                if (!cur) {
                	html+=qh+ '(当前期)';
                	cur = true;
                } else {
                	html+=qh;
                }
                html+='</td><td><input type="text" class="w40" qh="'+qh+'" value="'+bb+
                	'" autocomplete="off" maxlength="4">倍</td><td class="c_red">--</td><td class="c_red">--</td><td class="c_red">--</td><td class="c_red">--</td></tr>';
                bb = bb * 2;
                b++;
            }
            $betTraceData.html(html);
	        
	        $betTraceData.find('input[type="checkbox"]').unbind('change').bind('change', function() {
	            processBetTraceCore.calcView();
	        });
	        $betTraceData.find('input[type="text"]').unbind('keyup').bind('keyup', function() {
	            processBetTraceCore.calcView();
	        });
	        this.updateBetData();
	    },
	    updateBetData: function() {// 更新追号期号信息
	        if (!this.generatedTraceData) {
	            return false;
	        }
	        var trs=$betList.find("tr[buyinfo]"),tongJi=true;
	    	if(trs.length>0){
	    		var playCode="",buyInfos=[],cost=0,c,e;
	    		trs.each(function(){
		    		var it = $(this);
		    		if(playCode==""){
		    			playCode=it.attr("c");
		    		}else if(playCode!=it.attr("c")){
		    			tongJi=false;
		    		}
		    		cost +=(parseFloat(it.attr("m"),10)||0);
		    		c = it.attr("buyinfo").split("\|");
	    			e={};
	    			e.cash=parseInt(c[3])/parseInt(c[2]);
		    		e.totalCost=cost;
	    			e.rate=(parseFloat(it.attr("r"),10)||0);
	    			e.rateCash=e.cash*e.rate;
	    			e.balls=c[4];
	    			e.ballArgs=[];
	    			e.balls.match("@")&&(c=e.balls.split("@"),e.balls=c[1],e.ballArgs=c[0].split(","));
	    			e.balls=e.balls.split(",");
	    			c=(JIEBAO.lotType==3 || JIEBAO.lotType==5)?/\d\d|龙|虎/g:/\d|大|小|单|双/g;
	    			for(var d in e.balls)
	    				e.balls[d]=e.balls[d].match(c);
		    		buyInfos.push(e);
		    	});
	    		if(!tongJi){
		    		layer.msg('您选择的多种玩法无法计算盈利，将以 -- 呈现！',{icon: 5});
		    	}
	    		this.totalCost=cost;
    			processBetTraceCore.profitArr=tongJi?processBetTraceCore.calcProfit(buyInfos,playCode):false;// 计算盈利
	    		processBetTraceCore.calcView();
	        } else {// 还没有下注情况，不可以追号
	            this.totalCost = 0;
	            this.profitArr = false;
	            $betTrace.hide();
	            this.generatedTraceData = false;
	        }
	    },
	    calcView: function() {// 计算投注金额及其盈利情况，并显示
	        var traceCount = 0,amount=0;
	        this.thisOrderMaybeLose = false;
	        var _this = this,totalCost=_this.totalCost,beforeCount=0;
	        $betTraceData.find('tr').each(function(i, j) {
	            j = $(this).children();
	            var betRate = parseInt(j.eq(2).children().val())||0;// 赔率
	            if (j.eq(0).children().prop('checked') && betRate > 0) {
	                traceCount++;
	                i=betRate * totalCost;
                	beforeCount+=i;
                	j.eq(3).text(i.toFixed(2));
                    j.eq(4).text(beforeCount.toFixed(2));
	                if (_this.profitArr === false) {
	                    j.eq(5).text('--');
	                    j.eq(6).text('--');
	                } else {
	                    var calcRes = _this.generatorBetTrace(betRate, beforeCount);
                        j.eq(5).text(calcRes.earnStr);
                        j.eq(6).text(calcRes.percentStr);
                        if (calcRes.minEarn < 0) {
                            _this.thisOrderMaybeLose = true;
                        }
	                }
	            } else {
	                j.eq(3).text('--');
	                j.eq(4).text('--');
	                j.eq(5).text('--');
	                j.eq(6).text('--');
	            }
	        });
	        $totalQiShu.text(traceCount);// 追号期数
	        $totalMoney.text(beforeCount.toFixed(2));// 追号总金额
	    },
	    calcProfit:function(buyInfos,playCode){// 计算下注盈利
	    	var a,r={};
	    	this.temp=[];
	    	for(var i = 0,len=buyInfos.length;i<len;i++){
	    		a=buyInfos[i];
	    		switch(playCode){
		    	case "dwd":// 定位胆
// switch(JIEBAO.lotType){
// case '1'://系统彩
// case '2'://时时彩
		    		r=this.multipleWinning(a.balls,a.rateCash);
// break;
// }
	    			break;
		    	case "q2zx_fs":
		    	case "h2zx_hz":
		    	case "q2zx_hz":
		    	case "h2zx_fs":
		    	case "q3zx_fs":
		    	case "z3zx_fs":
		    	case "h3zx_fs":
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    		break;
		    	case "bdw_q31m":
		    	case "bdw_h31m":
		    	case "bdw_z31m":
		    		this.combineProfitArray(r,a.balls,a.rateCash);break;
		    	case "rxwf_r2zx_fs":
		    		r=this.multipleWinning(this.anyDirect(2,a.balls),a.rateCash);break;
		    	case "rxwf_r3zx_fs":
		    		r=this.multipleWinning(this.anyDirect(3,a.balls),a.rateCash);break;
		    	case "rxwf_r3zux_zu3":
		    		r=this.multipleWinning(this.anyGroup(3,2,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r3zux_zu6":
		    		r=this.multipleWinning(this.anyGroup(3,3,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r4zx_fs":
		    		r=this.multipleWinning(this.anyDirect(4,a.balls),a.rateCash);break;
		    	case "q3zux_zu3":
		    	case "h3zux_zu3":
		    	case "z3zux_zu3":
		    		this.combineProfitArray(r,this.generatorGroup(2,a.balls),a.rateCash);break;
		    	case "q3zux_zu6":
		    	case "h3zux_zu6":
		    	case "z3zux_zu6":
		    		this.combineProfitArray(r,this.generatorGroup(3,a.balls),a.rateCash);break;
		    	default:
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    	}
	    	}
    		var m=Number.MIN_VALUE,x=Number.MAX_VALUE,b;
			for(var d in r){
				b=r[d];
				m=Math.max(m,b);
				x=Math.min(x,b);
			}
			if(m!=Number.MIN_VALUE){
				return {maxProfit:parseFloat(m.toFixed(2)),minProfit:parseFloat(x.toFixed(2))};
	    	}
	    	return false;
	    },
	    generatorBetTrace:function(betRate, beforeCount){// 计算每一条追号得盈利
	    	var min = this.profitArr.minProfit,max = this.profitArr.maxProfit,minEarn=0,maxEarn=0,earnStr="",percentStr="";
	    	minEarn = min*betRate -beforeCount;
	    	maxEarn=max*betRate - beforeCount;
	    	earnStr = (minEarn==maxEarn)?minEarn.toFixed(2):minEarn.toFixed(2)+"至"+maxEarn.toFixed(2);
	    	percentStr = (minEarn==maxEarn)?((minEarn/beforeCount*100).toFixed(2)+"%"):((minEarn/beforeCount*100).toFixed(2)+"%")+"至"+((maxEarn/beforeCount*100).toFixed(2)+"%");
	    	return {earnStr:earnStr,percentStr:percentStr,minEarn:minEarn,maxEarn:maxEarn};
	    },
	    multipleWinning:function(b,d){
	    	var c,a,e,f,g,h;
	    	for(e in b){
	    		this.temp[e]instanceof Array||(this.temp[e]=[]),this.combineProfitArray(this.temp[e],b[e],d);
	    	}
    		h=g=0;
    		for(e in this.temp){
    			a=[];
    			for(f in this.temp[e])
    				a.push(this.temp[e][f]);
    			c=Math.max.apply(null,a);
    			a=Math.min.apply(null,a);
    			-Infinity===c&&(c=0);
    			Infinity===a&&(a=0);
    			g+=c;
    			h+=a
    		}
    		return{maxProfit:g,minProfit:h}
	    },
	    combineProfitArray:function(b,d,c){
	    	if(!(d instanceof Array))return!1;
	    	var a;
	    	for(var e= 0,f=d.length;e<f;e++){
	    		a=d[e],null!==a&&void 0!==a&&(b.hasOwnProperty(a)?b[a]+=c:b[a]=c);
	    	}
	    	return b
    	},
    	expandArray:function(b){
    		var d,c,a,e,f,g,h,k,l,n,m;
    		if(0===b.length)return!1;
    		b=b.slice();
    		m=b.splice(0,1)[0];
    		m instanceof Array||(m=m.toString().split(","));
    		f=0;
    		for(k=b.length;f<k;f++)
    			for(a=b[f],c=m.slice(),m=[],g=0,l=a.length;g<l;g++)
    				for(e=a[g],h=0,n=c.length;h<n;h++)
    					d=c[h],m.push(""+d+e);
    		return m
    	},
    	anyDirect:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G,J,L,K,M;
    		D=[];
    		if(2===b){
    			for(r=e=0,m=d.length-b;0<=m?e<=m:e>=m;r=0<=m?++e:--e){
    				if(null!==d[r]){
    					for(q=d[r],g=0,l=q.length;g<l;g++){
    						for(c=q[g],u=f=p=r+1,t=d.length;p<=t?f<t:f>t;u=p<=t?++f:--f){
    							if(null!==d[u]){
    								for(v=d[u],k=0,n=v.length;k<n;k++){
    									a=v[k];
    									h=""+r+u;
    									D[h]instanceof Array||(D[h]=[]);
    									D[h].push(""+c+a);
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(3===b){
    			for(r=n=0,z=d.length-b;0<=z?n<=z:n>=z;r=0<=z?++n:--n){
    				if(null!==d[r]){
    					for(B=d[r],m=0,f=B.length;m<f;m++){
    						for(c=B[m],u=q=C=r+1,A=d.length-1;C<=A?q<A:q>A;u=C<=A?++q:--q){
    							if(null!==d[u]){
    								for(E=d[u],p=0,k=E.length;p<k;p++){
    									for(a=E[p],g=t=x=u+1,w=d.length;x<=w?t<w:t>w;g=x<=w?++t:--t){
    										if(null!==d[g]){
    											for(y=d[g],v=0,l=y.length;v<l;v++){
    												e=y[v];
    												h=""+r+u+g;
    												D[h]instanceof Array||(D[h]=[]);
    												D[h].push(""+c+a+e);
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(4===b){
    			for(r=F=0,p=d.length-b;0<=p?F<=p:F>=p;r=0<=p?++F:--F){
    				if(null!==d[r]){
    					for(t=d[r],I=0,k=t.length;I<k;I++){
    						for(c=t[I],u=H=v=r+1,x=d.length-2;v<=x?H<x:H>x;u=v<=x?++H:--H){
    							if(null!==d[u]){
    								for(w=d[u],G=0,l=w.length;G<l;G++){
    									for(a=w[G],g=J=y=u+1,z=d.length-1;y<=z?J<z:J>z;g=y<=z?++J:--J){
    										if(null!==d[g]){
    											for(B=d[g],L=0,n=B.length;L<n;L++){
    												for(e=B[L],q=K=C=g+1,A=d.length;C<=A?K<A:K>A;q=C<=A?++K:--K){
    													if(null!==d[q]){
    														for(E=d[q],M=0,m=E.length;M<m;M++){
    															f=E[M];
    															h=""+r+u+g+q;
    															D[h]instanceof Array||(D[h]=[]);
    															D[h].push(""+c+a+e+f);
    														}
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else {
    			layer.msg("任"+b+"直选错误",{icon: 5});
    		}
    		return D
    	},
    	anyGroup:function(b,d,c,a){
    		var e,f;
    		null==b&&(b=2);
    		null==d&&(d=2);
    		null==c&&(c=[]);
    		null==a&&(a=['万','千','百','十','个']);
    		f=[];
    		b=parseInt(b);
    		d=parseInt(d);
    		c instanceof Array||(c=c.split(","));
    		if(a.length<b){
    			layer.msg("组选复式参数错误",{icon: 5});
    			return false;
    		}
    		c=this.generatorGroup(d,c);
    		e=this.generatorGroup(b,a);
    		b=0;
    		for(a=e.length;b<a;b++){
    			d=e[b];
    			f[d]=c;
    		}
    		return f
    	},
    	generatorGroup:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G;
    		null==b&&(b=2);
    		null==d&&(d=[]);
    		G=[];
    		b=parseInt(b);
    		d=d instanceof Array?d.slice():d.splt(",");
    		if(1===b)return d;
    		if(2===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=0,p=d.length;l<p;l++)
    					u=d[l],G.push(""+r+u);
    		else if(3===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=n=0,q=d.length;0<=q?n<q:n>q;l=0<=q?++n:--n)
    					for(u=d[l],p=m=k=l+1,f=d.length;k<=f?m<f:m>f;p=k<=f?++m:--m)l=d[p],G.push(""+r+u+l);
    		else if(4===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=q=0,m=d.length-2;0<=m?q<m:q>m;l=0<=m?++q:--q)
    					for(u=d[l],p=k=f=l+1,w=d.length-1;f<=w?k<w:k>w;p=f<=w?++k:--k)
    						for(l=d[p],n=g=v=p+1,y=d.length;v<=y?g<y:g>y;n=v<=y?++g:--g)p=d[n],G.push(""+r+u+l+p);
    		else if(5===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=m=0,k=d.length-3;0<=k?m<k:m>k;l=0<=k?++m:--m)
    					for(u=d[l],p=f=g=l+1,w=d.length-2;g<=w?f<w:f>w;p=g<=w?++f:--f)
    						for(l=d[p],n=v=y=p+1,h=d.length-1;y<=h?v<h:v>h;n=y<=h?++v:--v)
    							for(p=d[n],q=x=z=n+1,c=d.length;z<=c?x<c:x>c;q=z<=c?++x:--x)
    								n=d[q],G.push(""+r+u+l+p+n);
    		else if(6===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,k=d.length-4;0<=k?f<k:f>k;l=0<=k?++f:--f)
    					for(u=d[l],p=v=w=l+1,g=d.length-3;w<=g?v<g:v>g;p=w<=g?++v:--v)
    						for(l=d[p],n=x=y=p+1,h=d.length-2;y<=h?x<h:x>h;n=y<=h?++x:--x)
    							for(p=d[n],q=C=z=n+1,c=d.length-1;z<=c?C<c:C>c;q=z<=c?++C:--C)
    								for(n=d[q],m=A=B=q+1,a=d.length;B<=a?A<a:A>a;m=B<=a?++A:--A)q=d[m],G.push(""+r+u+l+p+n+q);
    		else if(7===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=w=0,f=d.length-5;0<=f?w<f:w>f;l=0<=f?++w:--w)
    					for(u=d[l],p=y=g=l+1,v=d.length-4;g<=v?y<v:y>v;p=g<=v?++y:--y)
    						for(l=d[p],n=z=h=p+1,x=d.length-3;h<=x?z<x:z>x;n=h<=x?++z:--z)
    							for(p=d[n],q=c=C=n+1,B=d.length-2;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-1;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										m=d[k],G.push(""+r+u+l+p+n+q+m);
    		else if(8===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,w=d.length-6;0<=w?f<w:f>w;l=0<=w?++f:--f)
    					for(u=d[l],p=g=v=l+1,y=d.length-5;v<=y?g<y:g>y;p=v<=y?++g:--g)
    						for(l=d[p],n=h=x=p+1,z=d.length-4;x<=z?h<z:h>z;n=x<=z?++h:--h)
    							for(p=d[n],q=c=C=n+1,B=d.length-3;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-2;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length-1;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										for(m=d[k],t=k=I=k+1,H=d.length;I<=H?k<H:k>H;t=I<=H?++k:--k)
    											t=d[t],G.push(""+r+u+l+p+n+q+m+t);
    		else layer.msg("长度组选错误"+b,{icon: 5});
    		return G
    	}
	}
}();

//六合彩
Core.mark = function(){
	var startYear = 1804,zodiacArray = ["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"];
	return {
		betAll:function(){
			//计算当前小类里所下注的注数和总金额
			var _table = $("#navTab_content"),bet_zs=0,bet_amount=0,min=0,max=0,tVal=0;
			_table.find(".table_ball tbody tr td input[type=text]").each(function(index,item){
				item = $(item);min=parseInt(item.attr("min"));max=parseInt(item.attr("max")),tVal=parseInt(item.val());
				if(tVal && tVal>min && tVal<max){
					bet_zs++;
					bet_amount+=parseInt(item.val());
				}
			});
			bet_result.betCount = bet_zs;
			bet_result.betMoney = bet_amount;
			$("#betAllZhuS").text(bet_zs);
			$("#betAllTotals").text(bet_amount);
		},
		betCheckedAll:function(){
			//该处多选需要计算注数，需要计算投注金额
			var radio = $(".games tr.head input[type=radio]:checked"),_r=$('.games tr td[c='+radio.attr('c')+']'),
			min=_r.attr("min_s"),max=_r.attr("max_s"),minB=_r.attr("min"),maxB=_r.attr("max");
			var bet_amount = $("#okXgc input[name=checked_money]").val(),_table=$("#navTab_content"),bet_zs=0,len=0;
			_table.find(".table_ball tbody tr td input[type=checkbox]").each(function(index,item){
				item = $(item);
				if(!min && !max){ //过滤连码。全不中
					min = item.attr("min_s");
					max = item.attr("max_s");
					minB = item.attr("min");
					maxB = item.attr("max");
				}
				if(item.is(":checked")){
					len++;
					if(len >= min){	//如果选中注数小于最小选中个数
						//bet_zs = len;//此处需要计算注数
						bet_zs = Core.mark.buyZhuShu(len,min,max);
					}
					if(len >= max){	//禁用未选中的checkbox
						Core.mark.disabledCheckbox();
					}else{
						Core.mark.openDisabledCheckbox();
					}
				}
			});
			bet_amount = parseInt(bet_amount);
			if(bet_amount<minB || bet_amount>maxB){
				bet_zs=0;
			}
			bet_result.betCount = bet_zs;
			bet_result.betMoney = !bet_amount?0:bet_amount*bet_zs;
			$("#betAllZhuS").text(bet_zs);
			$("#betAllTotals").text(!bet_amount?0:bet_amount*bet_zs);
		},
		resetClear:function(){
			$("#betAllZhuS").text(0);
			$("#betAllTotals").text(0);
			bet_result = {};
		},
		resetAll:function(){
			//清空所有
			Core.mark.resetClear();
			$("#okXgc input").val('');
			$("#navTab_content .table_ball tbody tr td").removeClass("active").find("input[type=text]").val('');
			$("#navTab_content .table_ball tbody tr td").find("input[type=checkbox]").prop("checked",false).prop("disabled",false);
		},
		disabledCheckbox:function(){
			var tableS = $('#navTab_content .table_ball tr td');
			tableS.each(function(index,item){
				if(!($(item).hasClass("active"))){
					$(item).find("input[type=checkbox]").prop("disabled",true);
				}
			})
		},
		openDisabledCheckbox:function(){
			var tableS = $('#navTab_content .table_ball');
			tableS.find("input[type=checkbox]").prop("disabled",false);
		},
		buyZhuShu:function(curLen,min,max){
			var a=1,b=1;
			if(min == 0 || curLen == 0 || max==0){return 1;}
			for(var i=0;i<min;i++){
				a = a * (min-i);
			}
			for(var i=curLen;i>curLen-min;i--){
				b = b * i;
			}
			return b/a;
		},
		zodiacName:function(haoMa){
			haoMa = parseInt(haoMa);
			var arr = new Array(),s="",year = JIEBAO.currentLunarYear;
			if(!year){
				year=new Date().getFullYear();
			}
			for(var i=0;i<12;i++){
				arr = Core.mark.zodiacHaoMaArray(i+1);
				if(arr != null){
					s = zodiacArray[Core.mark.subtractYear(year + i) % 12];
					if($.inArray(haoMa,arr) >= 0){
						return s;
					}
				}
			}
		},
		zodicaHaoMaArray:function(val){
			var arr = new Array(),s="";
			var nowYear = JIEBAO.currentLunarYear;
			if(!nowYear){
				nowYear = new Date().getFullYear();
			}
			for(var i=0;i<12;i++){
				s = zodiacArray[Core.mark.subtractYear(nowYear+i)%12];
				if(s==val){
					arr = Core.mark.zodiacHaoMaArray(i+1);
					return arr;
				}
			}
		},
		subtractYear:function(year){
			if(year < startYear){	// 如果年份小于起始的甲子年(startYear = 1804),则起始甲子年往前偏移
				startYear = startYear - (60 + 60 * ((startYear - startYear) / 60));// 60年一个周期
			}
			return year - startYear;
		},
		zodiacHaoMaArray:function(age){
			switch(age){
				case 12:
					return new Array(2,14,26,38);
				case 11:
					return new Array(3,15,27,39);
				case 10:
					return new Array(4,16,28,40);
				case 9:
					return new Array(5,17,29,41);
				case 8:
					return new Array(6,18,30,42);
				case 7:
					return new Array(7,19,31,43);
				case 6:
					return new Array(8,20,32,44);
				case 5:
					return new Array(9,21,33,45);
				case 4:
					return new Array(10,22,34,46);
				case 3:
					return new Array(11,23,35,47);
				case 2:
					return new Array(12,24,36,48);
				case 1:
					return new Array(1,13,25,37,49);
				default:
					return null;
			}
		}
	}
}();

var balanceTimer = function(){
	var t1;
	function balance() {
		$.ajax({
			url : JIEBAO.base + "/meminfo.do",
			success : function(j) {
				if(j.login){//如果已登录状态
					$('#balance').html(fmoney(j.money,2) + " RMB").attr("balance",fmoney(j.money,2));
				}
				
			}
		});
	}
	function message() {
		$.ajax({
			url : JIEBAO.base + "/lottery/message.do",
			success : function(j) {
				$("#zhanneixin").html(j);
			}
		});
	}
	//格式化成两位小数
	function fmoney(s, n) { 
		n = n > 0 && n <= 20 ? n : 2; 
		s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
		var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
		t = ""; 
		for (i = 0; i < l.length; i++) { 
		t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : ""); 
		} 
		return t.split("").reverse().join("") + "." + r; 
		} 
	
	return {
		start : function() {
			var a = this;
			clearTimeout(t1);
			balance();
			message();
			t1 = setTimeout(function(){a.start()},Math.floor(Math.random() * 1000 + 1) + 5000);
		},
		stop  : function() {
			clearTimeout(t1);
		}
	};
}();
$(function() {
	
	//请求链接是否带有参数
	var url =  location.href;
	if(url.indexOf('param=')!=-1){
		var param = getQueryString('param');
		if(param == "m_account"){//账户中心
			load('/cpSystem/accountCenter.do');
		}else if(param == "m_save"){//在线存款
			load('/cpSystem/accountOnline.do');
		}else if(param == "m_get"){//在线取款
			load('/cpSystem/withdrawalsOnline.do');
		}
	}
	
	scollPublicInfo();
	
	function load(path) {
		var load = new Loading();
		load.init({
			target: "#main_wrap"
		});
		load.start();
		$('#main_wrap').load(JIEBAO.base + path,function(){load.stop();});
	}
	
	function containerLoad(path) {
		var load = new Loading();
		load.init({
			target: "#ocontainer"
		});
		load.start();
		$('#ocontainer').load(JIEBAO.base + path,function(){load.stop();}).show();
		$('#betContainer').hide();
	}
	balanceTimer.start();
	// 在线存款
	$("#b2 a").click(function() {
		load('/cpSystem/accountOnline.do');
	});

	// 在线取款
	$("#b3 a").click(function() {
		load('/cpSystem/withdrawalsOnline.do');
	});

	// 账户中心
	$("#b4 a").click(function() {
		load('/cpSystem/accountCenter.do');
	});

	// 玩法介绍
	$("#b5 a").click(function() {
		load('/cpSystem/introducePlay.do');
	});

	// 系统公告
	$("#publicInfo").click(function() {
		load('/cpSystem/publicInfo.do');
	});
	
	// 未读讯息
	$("#unreadSent").click(function() {
		load('/cpSystem/letters.do');
	});

	//开奖结果
	$("#result").click(function(){
		containerLoad('/cpSystem/lotteryResults.do?rows=10&page=1&lotCode='+JIEBAO.lotCode);
	});
	
	//奖金结果
	$("#bonus").click(function(){
		containerLoad('/cpSystem/lotteryBonus.do?lotCode='+JIEBAO.lotCode);
	});
	
	//订单管理
	$("#order_history").click(function(){
		containerLoad('/cpSystem/lotteryOrder.do');
	});
	
	//投注报表
	$("#sbet_paper").click(function(){
		containerLoad('/cpSystem/lotteryBetting.do');
	});
	
});
//打开走势图
function openZst(param){
	if(param){
		/*
		if(param.indexOf('http://')==-1){
			param = 'http://'+param;
		}
		*/
		window.open(param);
	}
}
	function getQueryString(key) {
		var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
		var result = window.location.search.substr(1).match(reg);
		return result ? decodeURI(result[2]) : null;
	};

	

$(function(){//日期快捷按钮事件
	function formatDate(a) {
	    var e = a.getFullYear(),g = a.getMonth() + 1;
	    a = a.getDate();
	    10 > g && (g = "0" + g);
	    10 > a && (a = "0" + a);
	    return e + "-" + g + "-" + a
	}
	var now = new Date(),
		nowDayOfWeek = now.getDay(),
		nowDay = now.getDate(),
	    nowMonth = now.getMonth(),
	    nowYear = now.getYear(),
	    nowYear = nowYear + (2E3 > nowYear ? 1900 : 0),
	    lastMonthDate = new Date;
	    lastMonthDate.setDate(1);
	    lastMonthDate.setMonth(lastMonthDate.getMonth() - 1);
	    var lastYear = lastMonthDate.getYear(),
	        lastMonth = lastMonthDate.getMonth();
	    nowDayOfWeek = (nowDayOfWeek==0?7:nowDayOfWeek);
	function getyesterdayDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - 1))
	}
	function getWeekStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay + 1 - nowDayOfWeek))
	}

	function getWeekEndDate() {
	    return formatDate(nowDay + (7 - nowDayOfWeek) > nowDay ? new Date(nowYear, nowMonth, nowDay) : new Date(nowYear, nowMonth, nowDay + (7 - nowDayOfWeek)))
	}

	function getLastWeekStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek - 6))
	}

	function getLastWeekEndDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek))
	}

	function getMonthStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, 1))
	}

	function getMonthEndDate() {
	    var a = getMonthDays(nowMonth) < nowDay ? new Date(nowYear, nowMonth, getMonthDays(nowMonth)) : new Date(nowYear, nowMonth, nowDay);
	    return formatDate(a)
	}

	function getLastMonthStartDate() {
	    return formatDate(new Date(nowYear, lastMonth, 1))
	}
	
	function getLastMonthEndDate() {
	    var a = new Date(nowYear, lastMonth, getMonthDays(lastMonth));
	    return formatDate(a)
	};
	function getMonthDays(a) {
	    return (new Date(nowYear, a + 1, 1) - new Date(nowYear, a, 1)) / 864E5
	}
	$("#main_wrap").on("click",".btn[aims][d]",function(){
		var it=$(this),d=it.attr("d")||'',aims=it.attr("aims").split(","),e='',h='';
		switch(d){
		case 'today':e = formatDate(new Date),h=e;break;
		case 'yesterday':e = getyesterdayDate(new Date),h=e;break;
		case "week":
            e = getWeekStartDate();h = getWeekEndDate();
        break;
	    case "lastweek":
	            e = getLastWeekStartDate();h = getLastWeekEndDate();
	        break;
	    case "month":
	            e = getMonthStartDate();h = getMonthEndDate();
	        break;
	    case "lastmonth":
	            e = getLastMonthStartDate();
	            h = getLastMonthEndDate();
		}
		$("#"+aims[0]).val(e);
		$("#"+aims[1]).val(h);
	});
});
//首页轮播公告
function scollPublicInfo(){
	var code = 13;//彩票公告
	$.ajax({
		url:JIEBAO.base+"/cpSystem/list.do",
		data:{"code":code},
		dataType:"json",
		type:"GET",
		success:function(j){
			var data = j.rows;
			var col = '';
			for(var j in data){
				col+=data[j].content;
				break;
			}
			$('#xitonggonggao_content').html(col);
		}
	});
}

function getLocalTime(nS) {  
	var date = new Date(nS);
	var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '+ date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
   return dateFormat;     
}

// 从右边截取i位有效字符
String.prototype.Right = function(i) { // 为String对象增加一个Right方法
	return this.slice(this.length - i, this.length); // 返回值为 以“该字符串长度减i”为起始 到
														// 该字符串末尾 的截取字符串
};

// 数字金额实时转换成大写金额
	function convertCurrency(a) {
    var e, g, c, h, l, m, q, t, r, y, w;
    a = a.toString();
    if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
    a = a.replace(/,/g, "");
    a = a.replace(/^0+/, "");
    if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
    e = a.split(".");
    1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
    c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
    h = ["", "\u62fe", "\u4f70", "\u4edf"];
    l = ["", "\u4e07", "\u4ebf"];
    m = ["", ""];
    g = "";
    if (0 < Number(a)) {
        for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
        g += ""
    }
    if ("" != e)
        for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
    "" == g && (g = "\u96f6");
    "" == e && (g += "");
    return "" + g
}
	
	/**
	 * 将数值四舍五入(保留2位小数)后格式化成金额形式
	 * 
	 * @param num
	 *            数值(Number或者String)
	 * @return 金额格式的字符串,如'1,234,567.45'
	 * @type String
	 */  
	function formatCurrency(num) {  
	    num = num.toString().replace(/\$|\,/g,'');  
	    if(isNaN(num))  
	        num = "0";  
	    sign = (num == (num = Math.abs(num)));  
	    num = Math.floor(num*100+0.50000000001);  
	    cents = num%100;  
	    num = Math.floor(num/100).toString();  
	    if(cents<10)  
	    cents = "0" + cents;  
	    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)  
	    num = num.substring(0,num.length-(4*i+3))+','+  
	    num.substring(num.length-(4*i+3));  
	    return (((sign)?'':'-') + num + '.' + cents);  
	} 
	
	//验证姓名不能为空且一定为中文
	function checkName(rows,msg){
        if(rows.length!=0){
        	if(rows.match(/^[\u4e00-\u9fa5]+$/))
        		return true;
        	else 
        		layer.alert('请输入中文!');
        		return false;
        	}else{
        		layer.alert(msg);
        		return false;
        	}
        }
	
	//分页页数处理
    function pageChuLi(r,page,name){
    	var options = "";
    	//当前页数
    	$("."+name+"DqPage").attr("link",page);
    	$("."+name+"DqPage").text("当前页次"+page);
    	$("#"+name+"PaginationInfo").text("每页 10 条纪录 / 共 "+r.totalCount+" 条 / 共 "+r.totalPageCount+" 页");
    	var li = $("#"+name+"PaginationBar").find("li");
    	if(parseInt(page) == 1){
    		li.eq(2).removeClass("disabled");
    		li.eq(3).removeClass("disabled");
    		li.eq(0).addClass("disabled");
    		li.eq(1).addClass("disabled");
    	}
    	if(parseInt(page) > 1){
    		li.eq(0).removeClass("disabled");
    		li.eq(1).removeClass("disabled");
    	}
    	if(parseInt(page) > 1){
    		li.eq(2).removeClass("disabled");
    		li.eq(3).removeClass("disabled");
    	}
    	if( parseInt(page)==r.totalPageCount){
    		li.eq(2).addClass("disabled");
    		li.eq(3).addClass("disabled");
    	}
    	//上一页
    	li.eq(1).find("a").attr("link",parseInt(page)-1);
    	//下一页
    	li.eq(2).find("a").attr("link",parseInt(page)+1);
    	//最终页
    	li.eq(3).find("a").attr("link",r.totalPageCount);
		if(page == 1){
			for(var i=1;i<=r.totalPageCount;i++){
				options +='<option value="'+i+'">'+i+'</option>';
			}
			$("."+name+"Options").html(options);
		}
    }