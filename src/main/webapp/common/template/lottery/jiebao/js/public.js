var balanceTimer = function(){
	var t1;
	function balance() {
		$.ajax({
			url : JIEBAO.base + "/meminfo.do",
			success : function(j) {
				if(j.login){//如果已登录状态
					$('#balance').html(fmoney(j.money,2) + " RMB").attr("balance",fmoney(j.money,2));
				}
				
			}
		});
	}
	function message() {
		$.ajax({
			url : JIEBAO.base + "/lottery/message.do",
			success : function(j) {
				$("#zhanneixin").html(j);
			}
		});
	}
	
	//格式化成两位小数
	function fmoney(s, n) { 
		n = n > 0 && n <= 20 ? n : 2; 
		s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
		var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1]; 
		t = ""; 
		for (i = 0; i < l.length; i++) { 
		t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : ""); 
		} 
		return t.split("").reverse().join("") + "." + r; 
		} 
	
	return {
		start : function() {
			var a = this;
			clearTimeout(t1);
			balance();
			message();
			t1 = setTimeout(function(){a.start()},Math.floor(Math.random() * 1000 + 1) + 5000);
		},
		stop  : function() {
			clearTimeout(t1);
		}
	};
}();
$(function() {
	
	//请求链接是否带有参数
	var url =  location.href;
	if(url.indexOf('param=')!=-1){
		var param = getQueryString('param');
		if(param == "m_account"){//账户中心
			load('/cpSystem/accountCenter.do');
		}else if(param == "m_save"){//在线存款
			load('/cpSystem/accountOnline.do');
		}else if(param == "m_get"){//在线取款
			load('/cpSystem/withdrawalsOnline.do');
		}
	}
	
	scollPublicInfo();
	
	function load(path) {
		var load = new Loading();
		load.init({
			target: "#main_wrap"
		});
		load.start();
		$('#main_wrap').load(JIEBAO.base + path,function(){load.stop();});
	}
	
	function containerLoad(path) {
		var load = new Loading();
		load.init({
			target: "#ocontainer"
		});
		load.start();
		$('#ocontainer').load(JIEBAO.base + path,function(){load.stop();}).show();
		$('#betContainer').hide();
	}
	balanceTimer.start();
	// 在线存款
	$("#b2 a").click(function() {
		load('/cpSystem/accountOnline.do');
	});

	// 在线取款
	$("#b3 a").click(function() {
		load('/cpSystem/withdrawalsOnline.do');
	});

	// 账户中心
	$("#b4 a").click(function() {
		load('/cpSystem/accountCenter.do');
	});

	// 玩法介绍
	$("#b5 a").click(function() {
		load('/cpSystem/introducePlay.do');
	});

	// 系统公告
	$("#publicInfo").click(function() {
		load('/cpSystem/publicInfo.do');
	});
	
	// 未读讯息
	$("#unreadSent").click(function() {
		load('/cpSystem/letters.do');
	});
	
	// 积分兑换
	$("#scoreChange").click(function() {
		load('/cpSystem/jifen.do');
	});

	//开奖结果
	$("#result").click(function(){
		containerLoad('/cpSystem/lotteryResults.do?rows=10&page=1&lotCode='+JIEBAO.lotCode);
	});
	
	//奖金结果
	$("#bonus").click(function(){
		containerLoad('/cpSystem/lotteryBonus.do?lotCode='+JIEBAO.lotCode);
	});
	
	//订单管理
	$("#order_history").click(function(){
		containerLoad('/cpSystem/lotteryOrder.do');
	});
	
	//投注报表
	$("#sbet_paper").click(function(){
		containerLoad('/cpSystem/lotteryBetting.do');
	});
	
});

	//打开走势图
	function openZst(param){
		if(param){
			/*
			if(param.indexOf('http://')==-1){
				param = 'http://'+param;
			}
			*/
			window.open(param);
		}
	}

	function getQueryString(key) {
		var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
		var result = window.location.search.substr(1).match(reg);
		return result ? decodeURI(result[2]) : null;
	};

	

$(function(){//日期快捷按钮事件
	function formatDate(a) {
	    var e = a.getFullYear(),g = a.getMonth() + 1;
	    a = a.getDate();
	    10 > g && (g = "0" + g);
	    10 > a && (a = "0" + a);
	    return e + "-" + g + "-" + a
	}
	var now = new Date(),
		nowDayOfWeek = now.getDay(),
		nowDay = now.getDate(),
	    nowMonth = now.getMonth(),
	    nowYear = now.getYear(),
	    nowYear = nowYear + (2E3 > nowYear ? 1900 : 0),
	    lastMonthDate = new Date;
	    lastMonthDate.setDate(1);
	    lastMonthDate.setMonth(lastMonthDate.getMonth() - 1);
	    var lastYear = lastMonthDate.getYear(),
	        lastMonth = lastMonthDate.getMonth();
	    nowDayOfWeek = (nowDayOfWeek==0?7:nowDayOfWeek);
	function getyesterdayDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - 1))
	}
	function getWeekStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay + 1 - nowDayOfWeek))
	}

	function getWeekEndDate() {
	    return formatDate(nowDay + (7 - nowDayOfWeek) > nowDay ? new Date(nowYear, nowMonth, nowDay) : new Date(nowYear, nowMonth, nowDay + (7 - nowDayOfWeek)))
	}

	function getLastWeekStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek - 6))
	}

	function getLastWeekEndDate() {
	    return formatDate(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek))
	}

	function getMonthStartDate() {
	    return formatDate(new Date(nowYear, nowMonth, 1))
	}

	function getMonthEndDate() {
	    var a = getMonthDays(nowMonth) < nowDay ? new Date(nowYear, nowMonth, getMonthDays(nowMonth)) : new Date(nowYear, nowMonth, nowDay);
	    return formatDate(a)
	}

	function getLastMonthStartDate() {
	    return formatDate(new Date(nowYear, lastMonth, 1))
	}
	
	function getLastMonthEndDate() {
	    var a = new Date(nowYear, lastMonth, getMonthDays(lastMonth));
	    return formatDate(a)
	};
	function getMonthDays(a) {
	    return (new Date(nowYear, a + 1, 1) - new Date(nowYear, a, 1)) / 864E5
	}
	$("#main_wrap").on("click",".btn[aims][d]",function(){
		var it=$(this),d=it.attr("d")||'',aims=it.attr("aims").split(","),e='',h='';
		switch(d){
		case 'today':e = formatDate(new Date),h=e;break;
		case 'yesterday':e = getyesterdayDate(new Date),h=e;break;
		case "week":
            e = getWeekStartDate();h = getWeekEndDate();
        break;
	    case "lastweek":
	            e = getLastWeekStartDate();h = getLastWeekEndDate();
	        break;
	    case "month":
	            e = getMonthStartDate();h = getMonthEndDate();
	        break;
	    case "lastmonth":
	            e = getLastMonthStartDate();
	            h = getLastMonthEndDate();
		}
		$("#"+aims[0]).val(e);
		$("#"+aims[1]).val(h);
	});
});
//首页轮播公告
function scollPublicInfo(){
	var code = 13;//彩票公告
	$.ajax({
		url:JIEBAO.base+"/cpSystem/list.do",
		data:{"code":code},
		dataType:"json",
		type:"GET",
		success:function(j){
			var data = j.rows;
			var col = '';
			for(var j in data){
				col+=data[j].content;
				break;
			}
			$('#xitonggonggao_content').html(col);
		}
	});
}