<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="lecai_v5">
	<c:if test="${param.code == 'GD11X5'}">
	<div class="GD11X5OPEn">
		<strong>广东11选5开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">广东11选5</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:30-23:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01--42期</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司广东11选5具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'SH11X5'}">
	<div class="SH11X5OPEN">
		<strong>上海11选5开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">上海11选5</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:00--00:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001--045期</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司上海11选5具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'GX11X5'}">
	<div class="GX11X5OPEN">
		<strong>广西11选5开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">广西11选5</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:00--00:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001--045期</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司广西11选5具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'SD11X5'}">
	<div class="SD11X5OPEN">
		<strong>山东11选5开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">山东11选5</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:01--23:01(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01--43期</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司山东11选5具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'JX11X5'}">
	<div class="JX11X5OPEN">
		<strong>江西11选5开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">江西11选5</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:30-23:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-42期</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司江西11选5具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<h2>大小，单双</h2>
	<dd>
		<ul>
			<li>单码 - 指第一球、第二球、第三球、第四球、第五球出现的顺序与号码为派彩依据。</li>
			<li>单码：如现场滚球第一个开奖号码为10号，投注第一球为10号则视为中奖，其它号码视为不中奖。</li>
			<li>大小：开出的号码大于或等于6为大，小于或等于5为小，开出11为和 (不计算输赢)。</li>
			<li>单双：号码为双数叫双，如2、8；号码为单数叫单，如5、9；开出11为和 (不计算输赢)。</li>
		</ul>
	</dd>
	<h2>总和 - 以全部开出的5个号码，加起来的总和来判定</h2>
	<dd>
		<ul>
			<li>总和大小: 所有开奖号码数字加总值大于30为和大；总和值小于30为和小；若总和值等于30为和 (不计算输赢)。</li>
			<li>总和单双: 所有开奖号码数字加总值为单数叫和单，如11、31；加总值为双数叫和双，如12、32。若总和值等于29为和
				(不计算输赢)。</li>
			<li>总和尾数大小: 所有开奖号码数字加总值的尾数，大于或等于5为尾大，小于或等于4为尾小。</li>
		</ul>
	</dd>
	<h2>龙虎</h2>
	<dd>
		<ul>
			<li>龙：第一球开奖号码大于第五球开奖号码，如第一球开出10，第五球开出7。</li>
			<li>虎：第一球开奖号码小于第五球开奖号码，如第一球开出3，第五球开出7。</li>
		</ul>
	</dd>
	<h2>选号 - 选号玩法是由1~11号中，选出1~5个号码为一投注组合来进行投注。</h2>
	<dd>
		<ul>
			<li>任选一中一: 投注1个号码与当期开奖的5个号码中任1个号码相同，视为中奖。</li>
			<li>任选二中二: 投注2个号码与当期开奖的5个号码中任2个号码相同(顺序不限)，视为中奖。</li>
			<li>任选三中三: 投注3个号码与当期开奖的5个号码中任3个号码相同(顺序不限)，视为中奖。</li>
			<li>任选四中四: 投注4个号码与当期开奖的5个号码中任4个号码相同(顺序不限)，视为中奖。</li>
			<li>任选五中五: 投注5个号码与当期开奖的5个号码中5个号码相同(顺序不限)，视为中奖。</li>
			<li>任选六中五: 投注6个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
			<li>任选七中五: 投注7个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
			<li>任选八中五: 投注8个号码中任5个号码与当期开奖的5个号码相同(顺序不限)，视为中奖。</li>
			<li>组选前二: 投注的2个号码与当期顺序开出的5个号码中的前面2个号码相同，视为中奖。</li>
			<li>组选后二: 投注的2个号码与当期顺序开出的5个号码中的后面2个号码相同，视为中奖。</li>
			<li>组选前三: 投注的3个号码与当期顺序开出的5个号码中的前面3个号码相同，视为中奖。</li>
			<li>组选中三: 投注的3个号码与当期顺序开出的5个号码中的中间3个号码相同，视为中奖。</li>
			<li>组选后三: 投注的3个号码与当期顺序开出的5个号码中的后面3个号码相同，视为中奖。</li>
			<li>直选前二: 投注的2个号码与当期顺序开出的5个号码中的前面2个号码相同且顺序一致，视为中奖。</li>
			<li>直选后二: 投注的2个号码与当期顺序开出的5个号码中的后面2个号码相同且顺序一致，视为中奖。</li>
			<li>直选前三: 投注的3个号码与当期顺序开出的5个号码中的前面3个号码相同且顺序一致，视为中奖。</li>
			<li>直选中三: 投注的3个号码与当期顺序开出的5个号码中的中间3个号码相同且顺序一致，视为中奖。</li>
			<li>直选后三: 投注的3个号码与当期顺序开出的5个号码中的后面3个号码相同且顺序一致，视为中奖。</li>
		</ul>
	</dd>
</div>