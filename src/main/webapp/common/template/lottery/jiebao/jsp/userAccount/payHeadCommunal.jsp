<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/jquery.page.js"></script>
<!-- 账户中心--头部导航栏 -->
<div class="top_menu bg_black5 clearfix"
	style="margin: 7px auto 0px; width: 970px; position: static; zoom: 1;"
	id="userinfotop_menu">
	<h2>账户中心</h2>
	<ul>
		<li id="curOver1"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="1" menucode="user_myaccount">我的帐户</a></li>
		<li id="curOver2"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="2" menucode="user_mybankcardlist">我的银行卡</a></li>
		<c:if test="${isPayRecord }">
		<li id="curOver6">
		<a href="javascript:void(0)" class="btn b0 userExtra"
			id="6" menucode="user_onlinePayClass">充值记录</a></li>
		</c:if>	
		<li id="curOver3"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="3" menucode="user_cashflowrecord">提现记录</a></li>
		<c:if test="${isStationMsg }">
		<li id="curOver4"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="4" menucode="user_letters">站內信息</a></li>
		</c:if>
		<c:if test="${isScore }">
		<li id="curOver8"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="8" menucode="user_jifen">积分兑换</a></li>
		</c:if>
		<c:if test="${not empty isChangeMoney && isChangeMoney eq 'on'}">
		<li id="curOver9"><a href="javascript:void(0)" class="btn b0 userExtra"
			id="9" menucode="user_cashRecord">账变记录</a></li>
		</c:if>
<!-- 		<li id="curOver5"><a href="javascript:void(0)" class="btn b0 userExtra" -->
<!-- 			id="5" menucode="user_chongZhiClass">转账记录</a></li> -->
<!-- 		<li id="curOver7"><a href="javascript:void(0)" class="btn b0 userExtra" -->
<!-- 			id="7" menucode="user_offersapply">优惠申请</a></li> -->
	</ul>
</div>
<script>

function load(path) {
	$('#main_wrap').load(JIEBAO.base + path);
}

$('.userExtra').click(function() {
	var curId = this.id;
	$('.curOver').removeClass("current");
// 	$(this).parent().addClass("current");
	switch (curId) {
	case "1":
		load('/cpSystem/accountCenter.do');
		break;
	case "2":
		load('/cpSystem/myBankCardPage.do');
		break;
	case "3":
		load('/cpSystem/cashFlowRecord.do');
		break;
	case "4":
		load('/cpSystem/letters.do');
		break;
// 	case "5":
// 		load('/cpSystem/chongZhiClass.do');
// 		break;
	case "6":
		load('/cpSystem/payRecordPage.do');
		break;
	case "9":
		load('/cpSystem/cashRecord.do');
		break;
	case "8":
		load('/cpSystem/jifen.do');//积分兑换
		break;
	}
});


function getLocalTime(nS) {  
	var date = new Date(nS);
	var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '+ date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
   return dateFormat;     
}

//从右边截取i位有效字符
String.prototype.Right = function(i) { //为String对象增加一个Right方法
	return this.slice(this.length - i, this.length); //返回值为 以“该字符串长度减i”为起始 到 该字符串末尾 的截取字符串
};
</script>

<!-- 分页样式 -->
<style>
*{ margin:0; padding:0; list-style:none;}
a{ text-decoration:none;}
a:hover{ text-decoration:none;}
.tcdPageCode{padding: 15px 20px;text-align: left;color: #ccc;}
.tcdPageCode a{display: inline-block;color: #428bca;display: inline-block;height: 25px;	line-height: 25px;	padding: 0 10px;border: 1px solid #ddd;	margin: 0 2px;border-radius: 4px;vertical-align: middle;}
.tcdPageCode a:hover{text-decoration: none;border: 1px solid #428bca;}
.tcdPageCode span.current{display: inline-block;height: 25px;line-height: 25px;padding: 0 10px;margin: 0 2px;color: #fff;background-color: #428bca;	border: 1px solid #428bca;border-radius: 4px;vertical-align: middle;}
.tcdPageCode span.disabled{	display: inline-block;height: 25px;line-height: 25px;padding: 0 10px;margin: 0 2px;	color: #bfbfbf;background: #f2f2f2;border: 1px solid #bfbfbf;border-radius: 4px;vertical-align: middle;}
.disabled{
	cursor:pointer;
}
</style>