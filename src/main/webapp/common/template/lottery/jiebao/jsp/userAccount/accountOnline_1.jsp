<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/util.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.zclip.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<!-- 在线支付-->
<div class="wrapper main2 bg_black3" id="uc_onlinedeposits_211_area">
    <div class="row bg_black3">
        <div class="row_top bg_black3">
            <h3 class="title_line">在线存款<span></span>在线支付</h3>
        </div>
        <div class="row_con content_900" id="uc_onlinecash211_form">
            <form>
                <div class="step">
                    <span class="btn b0 current">1.请填写付款人资料</span>
                    <i></i>
                    <span class="btn b0">2.请确认您的转帐资料</span>
                    <i></i>
                    <span class="btn b0">3.完成</span>
                </div>
                <div class="br_line"></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tables">
                    <colgroup><col width="30%"><col width="70%"></colgroup>
                    <tbody>
                        <tr>
                            <td colspan="2" class="radio_va positionre" id="uc_pay_list">
                            </td>
                        </tr>
                        <tr class="oot hide">
                            <th>会员帐号：</th>
                            <td id="accountnoID"></td>
                        </tr>
                        <tr class="oot hide">
                            <th>存入金额：</th>
                            <td class="positionre"><input name="step1_amount"
							id="step1_amount" type="text" value="0" class="w155 required">（单笔充值限额：最低&nbsp;<span
							class="c_yellow" id="min_amount">0</span>&nbsp;元）</td>
                        </tr>
                        <tr class="oot hide">
                            <th>存入金额（大写）：</th>
                            <td class="c_yellow"><span id="amount_CH">零</span>元整</td>
                        </tr>
                    </tbody>
                </table>
                <div class="br_line"></div>
                <div class="btn_submit">
                    <input type="button" id="next" class="btn pointer" value="下一步">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
	var baseUrl = "${base}";
	var hostUrl1="${hostUrl1}";
</script>
<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>
<script>
$(function() {
	$.ajax({
		url : "${base}/center/banktrans/deposit/dptinita.do",
		success : function(j) {
			$('#cus_startTime').html(j.start);
			$('#cus_endTime').html(j.end);
			$('#accountnoID').html(j.account);
			$('#accountnoID2').html(j.account);
			var col = '';
			for ( var fast in j.onlines) {
				if(j.onlines[fast].icon){
					col += '<label class="pointer"><input name="payId" payMin="'+j.onlines[fast].min+'" ico="'+j.onlines[fast].iconCss+'" class="required fastPay" min="'+j.onlines[fast].min+'" shUrl="'+j.onlines[fast].url+'" id="'+j.onlines[fast].id+'" max="'+j.onlines[fast].max+'" iconCss="'+j.onlines[fast].iconCss+'" payType="'+j.onlines[fast].payType+'" value="'+j.onlines[fast].payName+'" type="radio"><span title="'+j.onlines[fast].payName+'" class="icon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(' + j.onlines[fast].icon + ');">' + j.onlines[fast].payName + '</span></label>';
				} else if(j.onlines[fast].payType == "3"){
					col += '<label class="pointer"><input name="payId" payMin="'+j.onlines[fast].min+'" ico="'+j.onlines[fast].iconCss+'" class="required fastPay" min="'+j.onlines[fast].min+'" shUrl="'+j.onlines[fast].url+'" id="'+j.onlines[fast].id+'" max="'+j.onlines[fast].max+'" iconCss="'+j.onlines[fast].iconCss+'" payType="'+j.onlines[fast].payType+'" value="'+j.onlines[fast].payName+'" type="radio"><span title="'+j.onlines[fast].payName+'" class="icon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/weixin.jpg\');">' + j.onlines[fast].payName + '</span></label>';
				} else if(j.onlines[fast].payType == "4"){
					col += '<label class="pointer"><input name="payId" payMin="'+j.onlines[fast].min+'" ico="'+j.onlines[fast].iconCss+'" class="required fastPay" min="'+j.onlines[fast].min+'" shUrl="'+j.onlines[fast].url+'" id="'+j.onlines[fast].id+'" max="'+j.onlines[fast].max+'" iconCss="'+j.onlines[fast].iconCss+'" payType="'+j.onlines[fast].payType+'" value="'+j.onlines[fast].payName+'" type="radio"><span title="'+j.onlines[fast].payName+'" class="icon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/zhifubao.jpg\');">' + j.onlines[fast].payName + '</span></label>';
				} else if(j.onlines[fast].payType == "5"){
					col += '<label class="pointer"><input name="payId" payMin="'+j.onlines[fast].min+'" ico="'+j.onlines[fast].iconCss+'" class="required fastPay" min="'+j.onlines[fast].min+'" shUrl="'+j.onlines[fast].url+'" id="'+j.onlines[fast].id+'" max="'+j.onlines[fast].max+'" iconCss="'+j.onlines[fast].iconCss+'" payType="'+j.onlines[fast].payType+'" value="'+j.onlines[fast].payName+'" type="radio"><span title="'+j.onlines[fast].payName+'" class="icon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(\'${base}/common/template/lottery/jiebao/images/qqpay.png\');">' + j.onlines[fast].payName + '</span></label>';
				} else {
					col += '<label class="pointer"><input name="payId" payMin="'+j.onlines[fast].min+'" ico="'+j.onlines[fast].iconCss+'" class="required fastPay" min="'+j.onlines[fast].min+'" shUrl="'+j.onlines[fast].url+'" id="'+j.onlines[fast].id+'" max="'+j.onlines[fast].max+'" iconCss="'+j.onlines[fast].iconCss+'" payType="'+j.onlines[fast].payType+'" value="'+j.onlines[fast].payName+'" type="radio"><span title="'+j.onlines[fast].payName+'" class="icon '+j.onlines[fast].iconCss+'">' + j.onlines[fast].payName + '</span></label>';
				}
			}
			$('#uc_pay_list').html(col);
			$('.fastPay').click(function() {
				var val = $('input:radio[name="payId"]:checked');
				var payMin = val.attr("payMin");
				$('#min_amount').html(payMin);
				$('.oot').show();
			});
		}
	});

	$("body").delegate("[name='step1_amount']", 'keyup', function() {
		/* 取出输入的金额,并转换成 */
		$("#amount_CH").text(convertCurrency($(this).val()));
	});
	
	$('#step1_amount').click(function(){
		$('#step1_amount').val("");
	});
	
$('#next').click(function() {
var val = $('input:radio[name="payId"]:checked');
var uMoney = $('#step1_amount').val();//充值金额
var payId = val.attr("id");//payId
if (val.val() == null) {
	layer.alert('请先选择支付方式!');
	return false;
} else {
	if(!uMoney){
		layer.alert('单笔充值限额不能为空!');
		return false;
	}
	
	if (parseFloat(uMoney) < parseFloat(val.attr('min'))) {
		layer.alert('单笔充值限额不能小于' + val.attr('min'));
		return false;
} 
	
	if (parseFloat(uMoney) > parseFloat(val.attr('max'))) {

		layer.alert('单笔充值限额不能大于' + val.attr('max'));

		return false;
	}
}

var iconCss = val.attr("iconCss");
var payType = val.attr("payType");
/******校验成功,下一步 获取订单号******/
	topay(uMoney, payId, iconCss, payType, function(result){
		if(result.success == false){
			alert(result.msg);
		} else if(result.success == true){
			if(result && result.data && result.data.formParams){
				var formHiddens = [];
				for(var key in result.data.formParams){
					var value = result.data.formParams[key];
					formHiddens.push({name: key, value: value});
				}
				result.data.formHiddens = formHiddens;
				var html = template('toPayTemplate', result.data);
				$("#uc_onlinecash211_form").html(html);
			}else{
				alert("系统发生错误");
			}
		}
	});
});

});
</script>
<script type="text/html" style="display: none;" id="toPayTemplate">
<form action="{{formAction}}" method="post" target='_blank'>
	{{each formHiddens as item}}
	<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}

<div class="step">
<span class="btn b0">1.请填写付款人资料</span>
<i></i>
<span class="btn b0 current">2.请确认您的转帐资料</span>
<i></i><span class="btn b0">3.完成</span>
</div>
<div class="br_line"></div>
<table width="450" border="0" cellspacing="0" cellpadding="0" class="tables">
<colgroup><col width="30%"><col width="70%"></colgroup>
<tbody>
<tr><th>订单号：</th><td>{{orderId}}</td></tr>
<tr><th>会员帐号：</th><td>{{account}}</td></tr>
<tr><th>充值金额：</th><td>{{amount}}</td></tr>
</tbody>
</table>
<div class="br_line"></div>
<div class="btn_submit">
<button class="btn" type='submit'>确认送出</button>
</form>
</script>