<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/util.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.zclip.min.js"></script>
<!-- 一般存款-->
<div class="wrapper main" id="uc_onlinedeposits_011_area">
    <div class="row bg_black3">
        <div class="row_top bg_black3">
            <h3 class="title_line">在线存款<span></span>一般存款</h3></div>
        <div class="row_con content_900">
            <div class="step"><span class="btn b0 current">1.选择要存入的银行，填入您的帐号资料</span><i></i><span class="btn b0">2.完成</span></div>
            <div class="br_line"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tables">
                <colgroup>
                    <col width="15%"><col width="85%">
                </colgroup>
                <tbody>
                    <tr>
                        <td colspan="2" class="radio_va positionre" id="uc_banks_list">没有任何资料。</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="border_dot">
                                <p>充值与用须知：</p>
                                <p>客服上班时间为：<span class="c_yellow" id="cus_startTime"></span>至<span class="c_yellow" id="cus_endTime"></span></p>
                                <p>转账成功后若超过五分钟未上分，请立即联系客服</p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
         <div id="showBankDetail" style="display: none;">
             <table width="645" border="0" cellspacing="0" cellpadding="0" class="table_online">
                <colgroup>
                    <col width="140"><col width="265"><col width="240">
                </colgroup>
                <tbody>
                    <c:if test="${showPayInfo}"><tr>
                        <th>充值银行：</th>
                        <td id="bankName"></td>
                    </tr>
                    <tr>
                        <th>收款姓名：</th>
                        <td id="Bname_info"></td>
                    </tr>
                    <tr>
                        <th>收款帐号：</th>
                        <td id="Baccount_info"></td>
                    </tr>
                    <tr>
                        <th>开户网点：</th>
                        <td id="Barea"></td>
                    </tr></c:if>
                   	<tr>
                        <th>充值金额：</th>
                        <td class="positionre">
                            <input name="step1_amount" id="step1_amount" type="text" class="w155 required">
                            （单笔充值限额：最低&nbsp;<span class="c_yellow" id="min_amount"></span>&nbsp;元）</td>
                    </tr>
                    <tr>
                        <th nowrap>充值金额（大写）：</th>
                        <td class="c_yellow"><span id="amount_CH">零</span>元整</td>
                    </tr>
                    <tr>
                        <th>存款人姓名：</th>
                        <td class="positionre">
                            <input class="w155 required" id="step1_userName" name="depositor" type="text">
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="note_w2 w465"><span class="c_red"><span style="font-size: 15px">冲值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</span></span>
            </div>
            <p>&nbsp;</p>
            <div class="br_line"></div>
            <div class="btn_submit" style="height:68px;line-height:68px;text-align: center;">
            	<button class="btn pointer" id="tijiaobtn"><c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if></button>
            </div>
       		</div>
        </div>
    </div>
</div>
<div class="wrapper main" id="uc_onlinedeposits_012_area" style="display:none">
    <div class="row bg_black3">
        <div class="row_top bg_black3">
            <h3 class="title_line">在线存款<span></span>一般存款</h3></div>
        <div class="row_con content_900">
            <div class="step"><span class="btn b0 current">1.选择要存入的银行，填入您的帐号资料</span><i></i><span class="btn b0 current">2.完成</span></div>
            <div class="br_line"></div>
            <table width="645" border="0" cellspacing="0" cellpadding="0" class="table_online">
                <colgroup>
                    <col width="140"><col width="265"><col width="240">
                </colgroup>
                <tbody>
                    <tr>
                        <th>充值银行：</th>
                        <td id="bankName1"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>收款姓名：</th>
                        <td id="Bname_info1"></td>
                        <td class="positionre"><span class="btn b0" id="Bname"><i class="fa fa-files-o"></i> 复 制</span></td>
                    </tr>
                    <tr>
                        <th>收款帐号：</th>
                        <td id="Baccount_info1"></td>
                        <td class="positionre"><span class="btn b0" id="Baccount"><i class="fa fa-files-o"></i> 复 制</span></td>
                    </tr>
                    <tr>
                        <th>开户网点：</th>
                        <td id="Barea1"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>订单号：</th>
                        <td id="Boid_info1"></td>
                        <td class="positionre"><span class="btn b0" id="Boid"><i class="fa fa-files-o"></i> 复 制</span></td>
                    </tr>
                    <tr>
                        <th>充值金额：</th>
                        <td><span class="c_yellow" id="Bamount_info1"></span></td>
                        <td class="positionre"><span class="btn b0" id="Bamount"><i class="fa fa-files-o"></i> 复 制</span></td>
                    </tr>
                </tbody>
            </table>
            <div class="note_w2 w465"><span class="c_red"><span style="font-size: 15px">冲值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</span></span>
            </div>
            <p>&nbsp;</p>
            <div class="br_line"></div>
            <div class="btn_submit" style="height:68px;line-height:68px;text-align: center;">
            <button class="btn pointer" id="tijiaobtn2">提 交</button>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {
	$.ajax({
		url : "${base}/center/banktrans/deposit/dptinitc.do",
		type : "POST",
		success : function(j) {
			$('#cus_startTime').html(j.start);
			$('#cus_endTime').html(j.end);
			$('#min_amount').html(j.min);
			var col = '';
			for ( var fast in j.banks) {
				if(!j.banks[fast].icon){
					col += '<label class="pointer"><input name="payId" class="required fastPay" getUserName="'+j.banks[fast].creatorName+'" getUserAccount="'+j.banks[fast].bankCard+'" getBankPoint="'+j.banks[fast].bankAddress+'" min="'+j.banks[fast].min+'" id="'+j.banks[fast].id+'" max="'+j.banks[fast].max+'" value="'+j.banks[fast].payName+'" type="radio"><span title="'+j.banks[fast].payName+'" class="icon '+j.banks[fast].icon+' '+j.banks[fast].iconCss+'">' + j.banks[fast].payName + '</span></label>';
				} else {
					col += '<label class="pointer"><input name="payId" class="required fastPay" getUserName="'+j.banks[fast].creatorName+'" getUserAccount="'+j.banks[fast].bankCard+'" getBankPoint="'+j.banks[fast].bankAddress+'" min="'+j.banks[fast].min+'" id="'+j.banks[fast].id+'" max="'+j.banks[fast].max+'" value="'+j.banks[fast].payName+'" type="radio"><span title="'+j.banks[fast].payName+'" class="icon '+j.banks[fast].icon+' '+j.banks[fast].iconCss+'" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(' + j.banks[fast].icon + ');">' + j.banks[fast].payName + '</span></label>';
				}
			}
			$('#uc_banks_list').html(col);
			$('.fastPay').click(function() {
				var $pay=$(this);
				if (!$pay.val()) {
    				layer.alert('请先选择支付方式!');
    				return false;
    			}
				$('#showBankDetail').show();
				$('#min_amount').html($pay.attr("min"));
				<c:if test="${showPayInfo}">
   				$('#bankName').html($pay.val());//充值银行
   				$('#Bname_info').html($pay.attr('getUserName'));//收款姓名
   				$('#Baccount_info').html($pay.attr('getUserAccount')+"<span style='color:#fff;font-size:20px;'>（转账后请输入金额提交订单）</span>");//收款帐号
   				$('#Barea').html($pay.attr('getBankPoint'));//开户网点
   				</c:if>
			});
		}
	});
	
	$("body").delegate("#step1_amount", 'keyup', function() {
		/* 取出输入的金额,并转换成 */
		$("#amount_CH").text(convertCurrency($(this).val()));
	});
	
	$('#tijiaobtn').click(function(){
		var val = $('input:radio[name="payId"]:checked');
		if (val.val() == null) {
			layer.alert('请先选择支付方式!');
			return false;
		}
		var uMoney = $('#step1_amount').val();//充值金额
		if(uMoney == null){
			layer.alert('单笔充值不能为空!');
			return false;
		}
		if (parseFloat(uMoney) < parseFloat(val.attr('min'))) {
			layer.alert('单笔充值限额不能小于' + val.attr('min'));
			return false;
		} 
		if (parseFloat(uMoney) > parseFloat(val.attr('max'))) {
			layer.alert('单笔充值限额不能大于' + val.attr('max'));
			return false;
		}
		var uName = $('#step1_userName').val();//存款人姓名
		if(uName == null || uName == ""){
			layer.alert('存款人姓名不能为空!');
			return false;
		}  
		
		if(!uName.match(/^[\u4e00-\u9fa5]+$/)){
			layer.alert('存款人姓名必须为中文!');
			return false;
		}
		
		var params = {
				"money" :uMoney,
				"depositor" :uName,
				"bankId":val.attr('id')
		}
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptcommitc.do",
			data:params,
			type:"POST",
			success : function(j) {
				if(j && j.msg){
					layer.msg(j.msg);
					return;	
				}
				$('div#uc_onlinedeposits_011_area').hide();	
				$('div#uc_onlinedeposits_012_area').show();
				$('#bankName1').html(val.val());//充值银行
				$('#Bname_info1').html(val.attr('getUserName'));//收款姓名
				$('#Baccount_info1').html(val.attr('getUserAccount'));//收款帐号
				$('#Barea1').html(val.attr('getBankPoint'));//开户网点
				$('#Bamount_info1').html(uMoney);//充值金额
				$('#Boid_info1').html(j);
			}
		});
	});
	
	$("#tijiaobtn2").click(function(){
		layer.alert("订单已提交");
	});
	$('#Bname').click(function(){
		$('#Bname').zclip({
			path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
			copy:function(){
				return $('#Bname_info1').text();
				},
			    afterCopy:function(){/* 复制成功后的操作 */
			    	layer.alert('复制成功,内容是:'+$('#Bname_info1').text());
			      }
		});
	});
	$('#Baccount').click(function(){
		$('#Baccount').zclip({
			path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
			copy:function(){
				return $('#Baccount_info1').text();
				},
			    afterCopy:function(){/* 复制成功后的操作 */
			    	layer.alert('复制成功,内容是:'+$('#Baccount_info1').text());
			      }
		});
	});
	$('#Boid').click(function(){
		$('#Boid').zclip({
			path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
			copy:function(){
				return $('#Boid_info1').text();
				},
			    afterCopy:function(){/* 复制成功后的操作 */
			    	layer.alert('复制成功,内容是:'+$('#Boid_info1').text());
			      }
		});
	});
	$('#Bamount').click(function(){
		$('#Bamount').zclip({
			path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
			copy:function(){
				return $('#Bamount_info1').text();
				},
			    afterCopy:function(){/* 复制成功后的操作 */
			    	layer.alert('复制成功,内容是:'+$('#Bamount_info1').text());
			      }
		});
	});
});
</script>
<style>
.b0{
	cursor:pointer
}
</style>