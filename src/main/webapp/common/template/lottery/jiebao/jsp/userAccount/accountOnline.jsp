<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 在线存款-->
<div class="wrapper main" XX_XX="uc_onlinedeposits_area">
	<div class="row bg_black3" XX_XX="uc_user_area">
		<div class="row_top bg_black3">
			<h3 class="title_line">
				在线存款<span></span>
			</h3>
		</div>
		<div class="row_con content_944">
		<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能在线存款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">	<dl class="item_cat">
			<c:if test="${ isThird}">
				<div class="bg_black3" id="pay_third_show_id">
					<dt>
						<a href="javascript:;" class="btn b4 payThird">在线支付</a>
					</dt>
					<dd XX_XX="uc_content3"><strong>${ isThirdDesc}</strong></dd>
				</div>
			</c:if>
				<c:if test="${ isQuick}">
				<div class="bg_black3" id="pay_quick_show_id">
					<dt>
						<a href="javascript:;" class="btn b4 payQuick">快速存款</a>
					</dt>
					<dd XX_XX="uc_content2">
						<strong>${ isQuickDesc}</strong>
					</dd>
				</div>
				</c:if>
				<c:if test="${ isNormal}">
				<div class="bg_black3" id="pay_normal_show_id">
					<dt>
						<a href="javascript:;" class="btn b4 payNormal">一般存款</a>
					</dt>
					<dd XX_XX="uc_content1">
						<strong>${ isNormalDesc}</strong>
					</dd>
				</div>
				</c:if>
			</dl></c:if>
		</div>
	</div>
</div>
<c:if test="${userInfo.accountType!=6 }"><script>
	$(function() {
		$('.payThird').click(function(){//在线支付
			$('#main_wrap').load(JIEBAO.base + "/cpSystem/payThird.do");
		});
		$('.payQuick').click(function(){//快速存款
			$('#main_wrap').load(JIEBAO.base + "/cpSystem/payQuick.do");
		});
		$('.payNormal').click(function(){//一般存款
			$('#main_wrap').load(JIEBAO.base + "/cpSystem/payNormal.do");
		});
	});
</script></c:if>