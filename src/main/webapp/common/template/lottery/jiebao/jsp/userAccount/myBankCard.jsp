<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 我的银行卡 -->
<jsp:include page="payHeadCommunal.jsp" />
<div class="wrapper main2 bg_black3" id="uc_user_area">
	<div class="content_900 clear">
		<div class="tips">
			<p>
				<i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;使用提示：
			</p>
			<ul>
				<li>银行卡绑定成功后，平台任意区域都<span class="c_yellow">不会</span>出现您的完整银行帐号，开户姓名等信息。
				</li>
				<li>每个游戏帐户最多绑定&nbsp;<span class="c_yellow">1</span>&nbsp;张银行卡，您已经成功绑定&nbsp;<span
					class="c_yellow" id="bankCardCount">0</span>&nbsp;张。
				</li>
				<li>一个帐户只能绑定同一个开户人姓名的银行卡。</li>
			</ul>
		</div>
		<div class="br_line"></div>
		<div class="content_frame bg_black3 smallT">
			<h3 class="bg_white4">银行资讯</h3>
			<table width="860" border="0" cellspacing="0" cellpadding="0"
				class="tables_h">
				<thead>
					<tr>
						<th>姓名</th>
						<th>银行名称</th>
						<th>取款帐号</th>
						<!-- 
                                <th>省(or 国家)</th>
                                <th>县市</th>
                                 -->

						<th>开户银行</th>
					</tr>
				</thead>
				<tbody id="userCardList">
				</tbody>
			</table>
		</div>
		<div class="br_line" XX_XX="bindBankcard_line"></div>
		<div class="btn_submit2" XX_XX="bindBankcard">
		</div>
	</div>
</div>
<script>
	$(function() {
		$('#curOver2').addClass("current");
		$.ajax({
			url : "${base}/center/banktrans/draw/drawdata.do",
			success : function(j) {
				var col= '<tr>';
				col+='<td>';
				if(j.member.userName!=null){
					col+='**'+(j.member.userName).Right(1);
				}
				col+='</td><td>';
				if(j.member.bankName!=null){
					col+=j.member.bankName;
				}
				col+='</td><td>';
				if(j.member.cardNo!=null){
					col+='**'+(j.member.cardNo).Right(4)
				}
				col+='</td><td>';
				if(j.member.bankAddress!=null){
					col+=j.member.bankAddress;
				}
				col+='</td></tr>';
				$('#userCardList').html(col);
				$('#bankCardCount').html('1');
			}
		});
	});
</script>