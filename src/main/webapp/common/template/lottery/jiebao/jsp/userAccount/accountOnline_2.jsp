<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/util.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/pasteUtil/jquery.zclip.min.js"></script>
<!-- 快速存款-->
<div class="wrapper main" id="uc_onlinedeposits_111_area">
	<div class="row bg_black3">
		<div class="row_top bg_black3">
			<h3 class="title_line">
				在线存款<span></span>快速入款
			</h3>
		</div>
		<div class="row_con content_900">
			<div class="step">
				<span class="btn b0 current">1.选择要存入的方式，填入您的帐号资料</span><i></i><span class="btn b0">2.完成</span>
			</div>
			<div class="br_line"></div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="tables">
				<colgroup>
					<col width="15%">
					<col width="85%">
				</colgroup>
				<tbody>
					<tr>
						<td colspan="2" class="radio_va positionre" id="uc_banks_list">没有任何资料。</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="border_dot">
								<p>自动充值使用须知：</p>
								<p>
									客服上班时间为：<span class="c_yellow" id="cus_startTime"></span>至<span
										class="c_yellow" id="cus_endTime"></span> <br>选择充值方式，填写充值金额及其账户信息，扫描二维码充值后，<span
										class="c_yellow">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即客服联系
								</p>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		<!-- 先展示订单详情start -->
		<div class="row_con content_900" id="orderFastDetail" style="display: none;">
			<table width="645" border="0" cellspacing="0" cellpadding="0" class="table_online">
				<colgroup>
					<col width="140">
					<col width="265">
					<col width="240">
				</colgroup>
				<tbody>
				<c:if test="${showPayInfo}">
					<tr>
						<th>充值方式：</th>
						<td><span class="radio_va" id="bPayWay"></span></td>
					</tr>
					<tr>
						<th>收款姓名：</th>
						<td id="Bname_info"></td>
					</tr>
						
					<tr>
						<th>收款帐号：</th>
						<td id="Baccount_info"></td>
					</tr>
					<tr id="BQR_tr">
						<th>二维码：</th>
						<td id="BQR_code"></td>
					</tr>
				</c:if>
					<tr id="payName_step1_tr_id">
						<th><span class="payName_step1"></span>：</th>
						<td class="positionre"><input class="w155 required number"id="step1_userAccount" minlength="5" maxlength="20"name="bank_cards" type="text">
						<span class="note_w"><i class="ico ico_1"></i>&nbsp;请填写正确的<span class="payName_step1"></span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>
					</tr>
					<tr>
						<th>充值金额：</th>
						<td class="positionre"><input name="step1_amount" id="step1_amount" type="text" class="w155 required">
						（单笔充值限额：最低&nbsp;<span class="c_yellow" id="min_amount">0</span>&nbsp;元）</td>
					</tr>
					<tr>
						<th nowrap>充值金额（大写）：</th>
						<td class="c_yellow"><span id="amount_CH">零</span>元整</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td><span class="note c_red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap>如实填写<span class="payName_step1"></span>,否则无法快速入款。</snap></span></td>
					</tr>
				</tbody>
			</table>
			<div class="note_w">
				<span><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，
				<span class="c_yellow">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
			</div>
		</div>
		<!-- 先展示订单详情end -->
		<div id="showFinishBtn" style="display: none;">
			<div class="br_line"></div>
			<div class="btn_submit">
				<button class="btn" id="next_step1"><c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if></button>
			</div>
		</div>
		</div>
	</div>
</div>

<div class="wrapper main" id="uc_onlinedeposits_112_area" style="display: none">
	<div class="row bg_black3">
		<div class="row_top bg_black3">
			<h3 class="title_line">
				在线存款<span></span>快速入款
			</h3>
		</div>
		<div class="row_con content_900">
			<div class="step">
				<span class="btn b0 current">1.选择要存入的方式，填入您的帐号资料</span><i></i><span class="btn b0 current">2.完成</span>
			</div>
			<div class="br_line"></div>
			<table width="645" border="0" cellspacing="0" cellpadding="0" class="table_online">
				<colgroup>
					<col width="140">
					<col width="265">
					<col width="240">
				</colgroup>
				<tbody>
					<tr>
						<th>充值方式：</th>
						<td><span class="radio_va" id="bPayWay1"></span></td>
						<td></td>
					</tr>
					<tr>
						<th>收款姓名：</th>
						<td id="Bname_info1"></td>
						<td class="positionre"><span class="btn b0" id="Bname"><i class="fa fa-files-o"></i> 复 制</span></td>
					</tr>
					<tr>
						<th>收款帐号：</th>
						<td id="Baccount_info1"></td>
						<td class="positionre"><span class="btn b0" id="Baccount"><i class="fa fa-files-o"></i> 复 制</span></td>
					</tr>
					<tr>
						<th>订单号：</th>
						<td id="Boid_info1"></td>
						<td class="positionre"><span class="btn b0" id="Boid"><i class="fa fa-files-o"></i> 复 制</span></td>
					</tr>
					<tr>
						<th>充值金额：</th>
						<td><span class="c_yellow" id="Bamount_info1"></span></td>
						<td class="positionre"><span class="btn b0" id="Bamount"><i class="fa fa-files-o"></i> 复 制</span></td>
					</tr>
					<tr id="BQR_tr1">
						<th>二维码：</th>
						<td colspan="2" id="BQR_code1"></td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td colspan="2"><span class="note c_red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap>如实填写<span class="payName_step1"></span>,否则无法快速入款。</snap></span></td>
					</tr>
				</tbody>
			</table>
			<div class="note_w">
				<span><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="c_yellow">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		var step1_userAccount_check=true;
		$.ajax({
			url : "${base}/cpSystem/dptinitb.do",
			success : function(j) {
				$('#cus_startTime').html(j.start);
				$('#cus_endTime').html(j.end);
				var col = '',f11;
				for ( var fast in j.fasts) {
					f11=j.fasts[fast];
					col += '<label class="pointer"><input name="payId" flabel="'+(f11.frontLabel||'')+'" payAccount="'
						+f11.payAccount+'" payUserName="'+f11.payUserName+'" ico="'+f11.qrCodeImg+'" class="required fastPay" min="'
						+f11.min+'" id="'+f11.id+'" max="'+f11.max+'" value="'+f11.payName+'" type="radio"><span title="'
						+f11.payName+'" class="icon '+f11.iconCss;
					if(f11.icon){
						col += '" style="background-size: 120px 35px; text-indent: -9999px; background-image: url(' + f11.icon + ');';
					}
					col +='">'+ f11.payName + '</span></label>';
				}
				$('#uc_banks_list').html(col);
				$('.fastPay').click(function() {
					$('#min_amount').html(this.min||j.min);
					var f=$(this).attr("flabel");
					if(f){
						$('.payName_step1').html(f);
						$("#payName_step1_tr_id").show();
						step1_userAccount_check=true;
					}else{
						step1_userAccount_check=false;
						$("#payName_step1_tr_id").hide();
					}
					fastStep2($(this));
					$('#showFinishBtn').show();
				});
			}
		});

		function fastStep2($pay) {
			if (!$pay.attr("id")) {
				layer.alert('请先选择支付方式!');
				return false;
			}
			//展示订单详情
			$('#orderFastDetail').show();
			<c:if test="${showPayInfo}">
			$('#bPayWay').html($pay.val());
			$('#Bname_info').html($pay.attr('payUserName'));
			$('#Baccount_info').html($pay.attr('payAccount'));
			if(!$pay.attr("ico")){
				$("#BQR_code").html("还未设置二维码");
			}else{
				$('#BQR_code').html('<img src="'+$pay.attr("ico")+'" alt="'+$pay.attr("ico")+'" style="width: 300px; height: 300px;">'+
				'<div style="font-size:20px;color:#0000ff;margin-top:6px">扫码支付后，请填写充值金额，并完成充值，否则无法为您上分！</div>');
			}
			</c:if>
		};
		
		$('#next_step1').click(function(){
			var val = $('input:radio[name="payId"]:checked');
			var payId = val.attr("id");//payId
			var uMoney = $('#step1_amount').val();//充值金额
			var uAccount = $('#step1_userAccount').val();//账号
			if(!uMoney){
				layer.alert('单笔充值限额不能为空!');
				return false;
			}
			if (parseFloat(uMoney) < parseFloat(val.attr('min'))) {
				layer.alert('单笔充值限额不能小于' + val.attr('min'));
				return false;
			} 
			if (parseFloat(uMoney) > parseFloat(val.attr('max'))) {
				layer.alert('单笔充值限额不能大于' + val.attr('max'));
				return false;
			}
			if(step1_userAccount_check && (uAccount == null || uAccount=="")){
				layer.alert('账号不能为空!');
				return false;
			}
			var params = {
					"money" :uMoney,
					"bankCards" :uAccount,
					"payId":payId
			}
			$.ajax({
				url : "${base}/cpSystem/dptcommitb.do",
				data:params,
				type:"POST",
				success : function(j) {
					if(j && j.msg){
						layer.msg(j.msg);
						return;	
					}
					$('div#uc_onlinedeposits_111_area').hide();	
					$('div#uc_onlinedeposits_112_area').show();
					$('#bPayWay1').html(val.val());
					$('#Bname_info1').html(val.attr('payUserName'));
					$('#Baccount_info1').html(val.attr('payAccount'));
					$('#Boid_info1').html(j);
					$('#Bamount_info1').html(uMoney);
					if(!val.attr("ico")){
						$("#BQR_code1").html("还未设置二维码");
					}else{
						$('#BQR_code1').html('<img src="'+val.attr("ico")+'" alt="'+val.attr("ico")+'" style="width: 300px; height: 300px;">');
					}
				}
			});
		});
		
		$('#Bname').click(function(){
			$('#Bname').zclip({
				path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
				copy:function(){
					return $('#Bname_info1').text();
					},
				    afterCopy:function(){/* 复制成功后的操作 */
				    	layer.alert('复制成功,内容是:'+$('#Bname_info1').text());
				      }
			});
		});
		$('#Baccount').click(function(){
			$('#Baccount').zclip({
				path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
				copy:function(){
					return $('#Baccount_info1').text();
					},
				    afterCopy:function(){/* 复制成功后的操作 */
				    	layer.alert('复制成功,内容是:'+$('#Baccount_info1').text());
				      }
			});
		});
		$('#Boid').click(function(){
			$('#Boid').zclip({
				path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
				copy:function(){
					return $('#Boid_info1').text();
					},
				    afterCopy:function(){/* 复制成功后的操作 */
				    	layer.alert('复制成功,内容是:'+$('#Boid_info1').text());
				      }
			});
		});
		$('#Bamount').click(function(){
			$('#Bamount').zclip({
				path:JIEBAO.base+"/common/template/lottery/jiebao/js/pasteUtil/ZeroClipboard.swf",
				copy:function(){
					return $('#Bamount_info1').text();
					},
				    afterCopy:function(){/* 复制成功后的操作 */
				    	layer.alert('复制成功,内容是:'+$('#Bamount_info1').text());
				      }
			});
		});
	});
	$("body").delegate("[name='step1_amount']", 'keyup', function() {
		/* 取出输入的金额,并转换成 */
		$("#amount_CH").text(convertCurrency($(this).val()));
	});

</script>
<style>
.b0{
	cursor:pointer
}
</style>