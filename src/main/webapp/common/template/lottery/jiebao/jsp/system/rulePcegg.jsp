<%@ page language="java" pageEncoding="UTF-8"%>
<div>
<p>
    <span>简介</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>PC蛋蛋（幸运28）开奖结果来源于国家福利彩票北京快乐8(官网)开奖号码，从早上9:05至23:55，每5分钟一期不停开奖。&nbsp;</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>北京快乐8每期开奖共开出20个数字，PC蛋蛋将这20个开奖号码按照由小到大的顺序依次排列；取其1-6位开奖号码相加，和值的末位数作为PC蛋蛋开奖第一个数值； 取其7-12位开奖号码相加，和值的末位数作为PC蛋蛋开奖第二个数值，取其13-18位开奖号码相加，和值的末位数作为PC蛋蛋开奖第三个数值；三个数值相加即为PC蛋蛋最终的开奖结果。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>定位胆</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一、第二、第三位号选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位、第三位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：第一位1开奖号码：第一位1，即中定位胆第一位。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>不定位胆</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从0-9中选择1个号码，每注由1号码组成，只要开奖号码的第一位、第二位、第三位中包含所选号码，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：12开奖号码：至少出现1和2各一个，即中不定位胆。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>三星组选</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：2 6 9 开奖号码:2 6 9（顺序不限），即中三星组选。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>三星复式</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案： 345 &nbsp;开奖号码: 345，即中三星直选。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>前二组选</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的第一位、第二位相同，顺序不限，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：4 7 开奖号码:前二位4 7 （顺序不限），即中前二组选。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>前二复式</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：58开奖号码：前二位58，即中前二复式</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>后二组选</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的第二位、第三位相同，顺序不限，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：4 7 开奖号码:后二位4 7 （顺序不限），即中后二组选。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>后二复式</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：58开奖号码：后二位58，即中后二复式。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>和值</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>从0-27中任意选择1个或1个以上号码。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>所选数值等于开奖号码相加之和，即为中奖。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>投注方案：和值1开奖号码：001或010或100，即中和值。</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>大小单双</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>和值的数字为大小单双</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>和值数字0-13为小，14-27为大，奇数为单，偶数为双</span>
</p>
<p>
    <span><br/></span>
</p>
<p>
    <span>所选大小单双与开奖和值相符，即为中奖</span>
</p>
<p>
    <br/>
</p>
</div>