<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 订单管理-->
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<div class="wrapper main order" id="order_greview">
	<input type="hidden" id="orderType" value="order">
	<div class="row bg_black3">
		<div class="row_top bg_black3" id="srcListType">
			<ul>
				<li srclisttype="0" class="current"><a href="javascript:;"class="btn b0">订单查询</a></li>
				<c:if test="${!unChaseOrder}"><li srclisttype="1" class=""><a href="javascript:;" class="btn b0">追号查询</a></li></c:if>
				<c:if test="${!unCancleOrder}"><li srclisttype="2" class=""><a href="javascript:;" class="btn b0">订单撤单</a></li></c:if>
			</ul>
		</div>
		<div class="row_con">
			<div class="order_search">
				<form action="#" id="dingDanGuanLiFormId">
					<div class="pull-left losear">
						<span>种 类</span>
						<select class="w100" name="type" id="orderGameType"></select>
					</div>
					<div class="pull-left losear">
						<select name="status" id="status">
							<option value="">所有状态</option>
							<option value="1">未开奖</option>
							<option value="2">已派奖</option>
							<option value="3">未中奖</option>
							<option value="4">撤单</option>
						</select>
					</div>
					<div class="pull-left losear">
						<select name="model" id="model">
							<option value="">全部模式</option>
							<option value="1">元</option>
							<option value="10">角</option>
							<option value="100">分</option>
						</select>
					</div>
					<div class="pull-left losear">
						<input id="orderId" class="text6" type="text" name="orderId" placeholder="输入单号" maxlength="36">
					</div>
					<div class="pull-left sdc">
						<div class="sdt pull-left">
							<span>下单日期</span>
						</div>
						<div class="sdp pull-left">
							<div class="datepickerIconGroup dpt">
								<input placeholder="开始日期" class="datepicker" name="oStartTime" type="text" id="oStartTime" value="${now }">
								<i class="fa fa-calendar fa-lg dateimg c_black"></i></div>
							<div class="datepickerIconGroup">
								<input placeholder="结束日期" class="datepicker" name="oEndTime" type="text" id="oEndTime" value="${now }">
								<i class="fa fa-calendar fa-lg dateimg c_black"></i></div>
						</div>
						<div class="sdb pull-left">
							<ul>
								<li><input type="button" value="今 日" class="btn b1" d="today" aims="oStartTime,oEndTime"></li>
								<li><input type="button" value="本 周" class="btn b1" d="week" aims="oStartTime,oEndTime"></li>
								<li><input type="button" value="本 月" class="btn b1" d="month" aims="oStartTime,oEndTime"></li>
							</ul>
						</div>
						<div class="sdb pull-left">
							<ul>
								<li><input type="button" value="昨 日" class="btn b1" d="yesterday" aims="oStartTime,oEndTime"></li>
								<li><input type="button" value="上 周" class="btn b1" d="lastweek" aims="oStartTime,oEndTime"></li>
								<li><input type="button" value="上 月" class="btn b1" d="lastmonth" aims="oStartTime,oEndTime"></li>
							</ul>
						</div>
					</div>
					<div class="search_btn pull-left losear" style="position: static;">
						<span><a href="javascript:getOrderManager(1);" class="btn b1">
							<span class="fa-stack c_white3" style="float: left;">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-search fa-stack-1x fa-inverse"></i>
						</span>&nbsp;订单查询</a></span>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row bg_black3" id="betOrderWrap"
		style="position: static; zoom: 1;">
		<div class="row_top">
			<h3>
				※<span id="srcListTypeName">订单查询</span> &nbsp; 种类：<span id="orderBetName">全部</span>&nbsp;&nbsp;<span
					id="orderBetListBetween">报表日期：2016-06-07 - 2016-06-07</span><span
					class="tips2 btn bg_black3"><i
					class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心小提醒：点击注单号能查询详细投注明细</span>
			</h3>
		</div>
		<div class="row_con" style="padding-bottom: 5px;">
			<table width="944" border="0" cellspacing="0" cellpadding="0"
				class="cancel_order" id="betOrderTbl">
				<thead>
					<tr class="bg_white2">
						<th>注单号</th>
	                    <th>下注时间</th>
	                    <th>种类</th>
	                    <th>期 号</th>
	                    <th>玩 法</th>
	                    <th>倍 数</th>
	                    <th>开奖号码</th>
	<!--                         <th>单注额</th> -->
	                    <th>总 额</th>
	                    <th>中奖奖金</th>
	                    <th>状态</th>
	                    <th>操作</th>
					</tr>
				</thead>
				<tbody id="orderBetList">
					<tr>
						<td colspan="11">没有符合查询的资料...</td>
					</tr>
				</tbody>
				<tfoot id="orderBetOrderTblFoot">
					<tr>
						<td colspan="7" class="t_right">小计</td>
						<td id="orderBetPageAmount"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="7" class="t_right">总计</td>
						<td class="c_gray" id="orderBetAmount"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="row_btm bg_black2" id="betOrderTblTabs" style="padding: 0px 10px; line-height: 50px; ">
			<ul class="pagination" id="orderPaginationBar" style="margin:0px; float:left;">
            	<li class="disabled"><a link="1" href="javascript:void(0);" onclick="orderSelectPage(this)" ><span>第一页</span></a></a></li>
			    <li class="disabled"><a link="1" href="javascript:void(0);" onclick="orderSelectPage(this)" ><span>上一页</span></a></li>
			    <li class="disabled"><a link="2" href="javascript:void(0);" onclick="orderSelectPage(this)" ><span>下一页</span></a></li>
			    <li class="disabled"><a link="1" href="javascript:void(0);" onclick="orderSelectPage(this)" ><span>最终页</span></a></li>
			    <li class="disabled"><span class="orderDqPage" link="1">当前页次1</span></li>
				<li>
					<select class="orderOptions" onchange="orderSelect()">
						<option value="1">1</option>
					</select>
				</li>
             </ul>
			<ul class="pagination pull-right" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;">
            	<li class="disabled"><span id="orderPaginationInfo"></span></li>
            </ul>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$("#oStartTime").datepicker();
			$("#oEndTime").datepicker();
			getLottery();
			getOrderManager(1);
			
			$("#srcListType").on("click",".btn",function(){
				$("#srcListType li").removeClass("current");
				$(this).parent("li").addClass("current");
				var srclisttype = $(this).parent("li").attr("srclisttype");
				if(srclisttype==0){
					$("#status").val("");
					$("#orderType").val("order");
				}else if(srclisttype==1){
					$("#status").val("");
					$("#orderType").val("zhuiHao");
				}else if(srclisttype==2){
					$("#status").val("4");
					$("#orderType").val("cheDan");
				}
				getOrderManager(1);
			})
		})
		
		function orderSelect(){
		    var page = $(".orderOptions").val();
		    $(".orderOptions").val(page);
		    getOrderManager(page);
	    }
	    
	    function orderSelectPage(el){
	    	var cl = $(el).parent().attr("class");
	    	if(cl == "disabled"){
	    		return false;
	    	}
		    var page = $(el).attr("link");
		    getOrderManager(page);
	    }
		
		function getLottery(){
			$.ajax({
				url:'${base}/lottery/getLottery.do',
				data:'',
				success:function(r){
					var gameSelect = '<option value="">全部</option>';
					for(var i=0;i<r.length;i++){
						//if(r[i].name!='六合彩'){
							gameSelect +='<option type="'+r[i].type+'" value="'+r[i].code+'">'+r[i].name+'</option>';
						//}
					}
					$("#orderGameType").html(gameSelect);
				}
			})
		}
		
		function getOrderManager(page){
			var load = new Loading();
			load.init({
				target: "#orderBetList"
			});
			load.start();

			var zhuiHao = 1;
			var code = $("#orderGameType").val();
			var name = $("#orderGameType").find("option:selected").text();
			$("#orderBetName").text(name);
			var status = $("#status").val();
			var model = $("#model").val();
			var orderId = $("#orderId").val();
			var startTime =$("#oStartTime").val();
			var endTime =$("#oEndTime").val();
			var orderType = $("#orderType").val();
			if(orderType=="zhuiHao"){
				zhuiHao = 2;
			}else if(orderType=="cheDan"){
				status =4;
			}
			if(startTime!=""){
				startTime = startTime+' 00:00:00';
			}
			if(endTime!=""){
				endTime = endTime+' 23:59:59';
			}
			
			$.ajax({
				url:'${base}/lotteryBet/getBcLotteryOrder.do',
				data:{
					code : code,
					status : status,
					zhuiHao : zhuiHao,
					model : model,
					orderId : orderId,
					startTime : startTime,
					endTime : endTime,
					page : page,
					rows : 10
				},
				success:function(r){
					var orderBetList ='';
					var list = r.page.list;
					var results = r.page;
					if(list.length==0){
						var startTime =$("#oStartTime").val();
						var endTime =$("#oEndTime").val();
						var data="";
						if(startTime =="" || endTime==""){
							if(list && list[0] && list[0].createTime){
								var dateTime = list[0].createTime.split(" ");
								data = dateTime[0];
							}
						}else{
							data = startTime +"  " +endTime;
						}
						$("#orderBetListBetween").text('报表日期：'+data);
						$("#orderBetList").html('<tr><td colspan="11">没有符合查询的资料...</td></tr>');
						$("#orderBetOrderTblFoot").hide();
					}else{
						var startTime =$("#oStartTime").val();
						var endTime =$("#oEndTime").val();
						var data="";
						if(startTime =="" || endTime==""){
							if(list && list[0] && list[0].createTime){
								var dateTime = list[0].createTime.split(" ");
								data = dateTime[0];
							}
						}else{
							data = startTime +"  " +endTime;
						}
						$("#orderBetListBetween").text('报表日期：'+data);
						for(var i=0;i<list.length;i++){
							if(list[i].lotteryHaoMa == null || list[i].lotteryHaoMa==''){
								list[i].lotteryHaoMa = '- -';
			    			}else if(list[i].lotteryHaoMa.length>5){
								list[i].lotteryHaoMa = list[i].lotteryHaoMa.substr(0, 5)+'...';
			    			}
							orderBetList+='<tr>'
							orderBetList+='<td><a href="javascript:;" style="width: 100%;" class="BetInf"orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'">'+list[i].orderId+'</a></td>'
							orderBetList+='<td>'+list[i].createTime+'</td>'
							orderBetList+='<td>'+list[i].lotName+'</td>'
							orderBetList+='<td>'+list[i].qiHao+'</td>'
							orderBetList+='<td>'+list[i].playName+'</td>'
							orderBetList+='<td>'+list[i].multiple+'</td>'
							orderBetList+='<td>'+list[i].lotteryHaoMa+'</td>'
							orderBetList+='<td>'+list[i].buyMoney+'</td>'
							if(list[i].winMoney != null){
								orderBetList+='<td>'+list[i].winMoney+'</td>'
							}else{
								orderBetList+='<td>0.00</td>'
							}
							//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
							if(list[i].status == 1 ){
								orderBetList+='<td><span style="background-color: #428bca;border-radius: .25em;padding: 0.2em .6em 0.3em;">未开奖 </span></td>'
								<c:if test="${!unCancleOrder}">
									orderBetList+='<td><a class="cheDan" orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'" from="ddcsx">撤单</a></td>';
								</c:if><c:if test="${unCancleOrder}">orderBetList+='<td></td>';</c:if>
							}else if(list[i].status == 2 ){
								orderBetList+='<td><span style="background-color: #5cb85c;border-radius: .25em;padding: 0.2em .6em 0.3em;">已中奖 </span></td><td></td>'
							}else if(list[i].status == 3 ){
								orderBetList+='<td><span style="background-color: #d9534f;border-radius: .25em;padding: 0.2em .6em 0.3em;">未中奖 </span></td><td></td>'
							}else if(list[i].status == 4 ){
								orderBetList+='<td><span style="background-color: #5bc0de;border-radius: .25em;padding: 0.2em .6em 0.3em;">撤单 </span></td><td></td>'
							}else if(list[i].status == 5 ){
								orderBetList+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">派奖回滚成功 </span></td><td></td>'
									orderBetList+='<td></td>'
							}else if(list[i].status == 6 ){
								orderBetList+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚异常 </span></td><td></td>'
									orderBetList+='<td></td>'
							}else if(list[i].status == 7 ){
								orderBetList+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">开奖异常 </span></td><td></td>'
							}
						}
					$("#orderBetList").html(orderBetList);
					$("#orderBetPageAmount").html(r.subBuyMoney);
					$("#orderBetAmount").html(r.sumBuyMoney);
					pageChuLi(results,page,'order');
					}
					load.stop();
				}
			})
		}
	</script>
</div>
