<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="display:none;" class="moneylist" id="MoneyList">
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
    	<thead>
    		<tr>
            <th width="100%" colspan="5" class="moneylist-tit">
                <div>下注金额<span class="yellow">
                        (
                        <span id="BetMinMoneyLimit">0</span>元-<span id="BetMaxMoneyLimit">0</span>元)
                    </span>
                </div>
            </th>
        </tr>
    	</thead>
        <tbody>
        <tr>
            <td width="20%" class="MoneyListtd">10</td>
            <td class="MoneyListtd">20</td>
            <td class="MoneyListtd">30</td>
            <td class="MoneyListtd">40</td>
            <td class="MoneyListtd">50</td>
        </tr>
        <tr>
            <td width="20%" class="MoneyListtd">60</td>
            <td class="MoneyListtd">70</td>
            <td class="MoneyListtd">80</td>
            <td class="MoneyListtd">90</td>
            <td class="MoneyListtd">100</td>
        </tr>
        <tr>
            <td width="20%" class="MoneyListtd">200</td>
            <td class="MoneyListtd">300</td>
            <td class="MoneyListtd">400</td>
            <td class="MoneyListtd">500</td>
            <td class="MoneyListtd">600</td>
        </tr>
        <tr>
            <td width="20%" class="MoneyListtd">700</td>
            <td class="MoneyListtd">800</td>
            <td class="MoneyListtd">900</td>
            <td class="MoneyListtd">1000</td>
            <td class="MoneyListtd">2000</td>
        </tr>
        <tr>
            <td width="20%" class="MoneyListtd">3000</td>
            <td class="MoneyListtd">4000</td>
            <td class="MoneyListtd">5000</td>
            <td class="MoneyListtd">6000</td>
            <td class="MoneyListtd">7000</td>
        </tr>
    </tbody></table>
</div>