<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 玩法介绍 -->
<style>
	.message_tablex {
	    border-bottom: 1px solid #cfcfcf;
	    border-left: 1px solid #cfcfcf;
	    font-size: 13px;
	}
	.message_tablex th, .message_tablex td {
	    border-top: 1px solid #cfcfcf;
	    border-right: 1px solid #cfcfcf;
	    padding: 15px 5px;
	    line-height: 1em;
	    vertical-align: top;
	}
	.showDesc{
		color: white;
	}
	.textcenter{
		text-align: center;
	}
</style>
<div class="wrapper main2 bg_black3">
	<div class="content_944 clear">
		<div class="content_frame" id="content_frame">
			<form id="uc_letters_form">
				<table width="940" border="0" cellspacing="0" cellpadding="0"
					class="message_tablex bg_black3">
					<thead>
						<tr class="bg_white4">
							<th style="width:20%;font-size: initial;" class="textcenter">编 号</th>
							<th style="width:50%;font-size: initial;">标 题</th>
							<th style="width:30%;font-size: initial;" class="textcenter">发布时间</th>
						</tr>
					</thead>
					<tbody id="uc_content">
						<c:forEach items="${viewSet}" var="set" varStatus="setIndex">
							<c:set value="${set==1 || set==2?'时时彩':set==3?'北京赛车':set==4?'福彩3D/排列三':set==5?'11选5':set==6?'六合彩/十分六合彩':set==7?'pc蛋蛋':'快三'}" var="setName"></c:set>
							<tr>
								<td class="textcenter">${setIndex.index + 1}</td>
								<td>
									<a href="javascript:void(0)" title="${setName}" class="showDesc" onClick="showDesc(${set},'${setName}');">${setName}玩法介绍</a>
								</td>
								<td class="textcenter">
									<span class="now"></span>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<script>
	$(function() {
		$('.now').html('2010-08-08 12:00:00');
		
		var code = 8;//玩法介绍
		$.ajax({
			url:JIEBAO.base+"/cpSystem/list.do",
			data:{"code":code},
			dataType:"json",
			type:"GET",
			success:function(j){
				var data = j.rows;
				if(data==null || data ==""){
					$('#gameArar').show();
				}else{
					var col = '';
					for(var j in data){
						col+='<tr><td class="textcenter">'+data[j].id+'</td><td><a href="javascript:void(0)" title="'+data[j].title+'" class="showDesc" id="'+data[j].id+'">'+data[j].title+'</a></td><td class="textcenter">'+getLocalTime(data[j].updateTime)+'</td></tr>';
					}
					$('#uc_content').html(col);
					$('.showDesc').click(function(){
						playDesc(this.id,this.title);
					});
				}
			}
		});

	});
	
	//玩法详情
	function playDesc(id,title){
		layer.open({
			  type: 2,
			  title: title,
			  scrollbar: false,
			  shadeClose: true,
			  offset : ['10%'],
			  fadeIn : [300 , true],
			  shade: 0.8,
			  area: ['800px', '500px'],
			  content: JIEBAO.base +"/cpSystem/publicInfoDesc.do?id="+id
			}); 
	}
	
	//时间戳转时间
	function getLocalTime(nS) {  
		var date = new Date(nS);
		var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
	   return dateFormat;     
	} 
	
	function getNowFormatDate() {
	    var date = new Date();
	    var seperator1 = "-";
	    var seperator2 = ":";
	    var month = date.getMonth() + 1;
	    var strDate = date.getDate();
	    if (month >= 1 && month <= 9) {
	        month = "0" + month;
	    }
	    if (strDate >= 0 && strDate <= 9) {
	        strDate = "0" + strDate;
	    }
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
	    return currentdate;
	}
	
	function showDesc(param,msg){
		layer.open({
			  type: 2,
			  title: msg+"玩法介绍",
			  scrollbar: false,
			  shadeClose: true,
			  offset : ['10%'],
			  fadeIn : [300 , true],
			  shade: 0.8,
			  area: ['800px', '500px'],
			  content: JIEBAO.base +"/cpSystem/gameRule.do?param="+param
			}); 
	}
</script>
<style>
.pubOver:hover{
   background-color: gray;
}
</style>