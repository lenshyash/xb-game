<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 站内信息 -->
<jsp:include page="payHeadCommunal.jsp" />
<div class="wrapper main2 bg_black3" XX_XX="uc_user_area">
            <div class="content_944 clear">
                <ul class="message_menu hide">
                    <li class="current"><a href="#" class="btn b0"><i class="ico ico_18"></i>已收消息</a></li>
                    <li><a href="javascript:;" class="btn b0"><i class="ico ico_19"></i>已发消息</a></li>
                    <li><a href="javascript:;" class="btn b0"><i class="ico ico_20"></i>写消息</a></li>
                </ul>
                <div class="content_frame" id="content_frame">
                    <form id="uc_letters_form">
                        <table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
                            <thead>
                                <tr>
                                    <th colspan="5">
<!--                                         <div class="message_cat" id="messageStateSelect"><a href="javascript:;" class="current" messageState="0">所有</a>｜<a href="javascript:;" messageState="2">已读</a>｜<a href="javascript:;" messageState="1">未读</a></div> -->
                                    </th>
                                </tr>
                                <tr class="bg_white4">
                                    <th>状  态</th>
                                    <th>短信标题</th>
                                    <th>发布时间</th>
                                    <th>操  作</th>
                                </tr>
                            </thead>
                            <tbody id="uc_letters_tbody"></tbody>
                            <tfoot id="uc_letters_tfoot">
                                <tr>
                                    <td colspan="5" class="message_page">
                                        <div class="page" id="uc_letters_pagelink_area"></div>
                                        <div class="page_btn">
<!--                                             <button class="btn" id="uc_letters_submit" type="button"><i class="fa fa-times-circle fa-lg"></i>&nbsp;删 除</button> -->
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
<script>
$(function() {
	$('#curOver4').addClass("current");
	var msgCache={};
	function initf(){
		$.ajax({
			url:"${base}/center/news/message/list.do",
			data : {pageNumber : 1,pageSize : 10},
			dataType:"json",
			type:"POST",
			success:function(j){
				var data = j.list;
				var col = '';
				if(data){
					for(var i in data){
						var isRead = '未读';
						col += '<tr onmouseover="this.style.backgroundColor=\'#FA961E\'" onmouseout="this.style.backgroundColor=\'\'" align="center">';
						if(data[i].status == '2'){
							isRead = '已读';
						}
						col += '<td align="center"><font color="#FF0000">'+isRead+'</font></td>';
						col += '<td align="left"><a href="javascript:void(0);" mid="'+data[i].id+'">'+data[i].title+'</a></td>';
						col += '<td align="center">'+getLocalTime(data[i].createDatetime)+'</td>';
						col += '<td align="center"><a href="javascript:void(0);" umid="'+data[i].userMessageId+'" style="color: #00F">点击删除</a></td>';
						col += '</tr>';
						msgCache["id_"+data[i].id]=data[i];
					}
					var $tb=$('#uc_letters_tbody').html(col);
					$tb.find("a[mid]").click(function(){
						readMsg($(this).attr("mid"));
					});
					$tb.find("a[umid]").click(function(){
						delMsg($(this).attr("umid"));
					});
				}
			}
		});
	}
	initf();
	function readMsg(id) {
		var obj = msgCache["id_"+id];
		if(!obj){
			return;
		}
		$.ajax({url : "${base}/center/news/message/read.do",
			data : {id : id},
			success : function(result) {
				layer.open({
					type : 1, //page层
					area : [ '550px', '550px' ],
					title : obj.title,
					shade : 0.6, //遮罩透明度
					scrollbar : false,
					offset : '120px',//距离顶部
					moveType : 0, //拖拽风格，0是默认，1是传统拖动
					shift : 1, //0-6的动画形式，-1不开启
					content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'+ obj.message + '</div>'
				});
				initf();
			}
		});
	}

	function delMsg(id) {
		if (confirm("确认删除此信吗？")) {
			$.ajax({
				url : "${base}/center/news/message/delete.do",
				data : {id : id},
				success : function(result) {
					initf();
				}
			});
		}
	}
});
</script>