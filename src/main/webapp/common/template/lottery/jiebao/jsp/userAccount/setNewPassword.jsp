<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 设置取款密码 -->
<jsp:include page="payHeadCommunal.jsp" />
 <div class="wrapper main2 bg_black3" id="set_cash_password">
            <div class="content_900 clear">
                    <p class="tips"><i class="ico ico_1"></i>贴心小提醒：请先绑定您的提款密码后，再进行其他动作</p>
                    <div class="br_line"></div>
                    <c:if test="${userInfo.accountType==6 }"><div>试玩账号不能提款</div></c:if>
                   	<c:if test="${userInfo.accountType!=6 }"><table width="800" border="0" cellspacing="0" cellpadding="0" class="tables">
                        <tbody>
                            <tr style="height: 50px;">
                                <th><span class="c_red">＊</span>提款密码：</th>
                                <td class="positionre">
                                    <input class="w200 required" name="cashPassword" type="password">
                                </td>
                            </tr>
                            <tr style="height: 50px;">
                                <th><span class="c_red">＊</span>确认密码：</th>
                                <td class="positionre">
                                    <input class="w200 required" name="recashPassword" type="password">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="br_line"></div>
                    <div class="btn_submit2">
                        <button class="btn" id="set_cash_password_btn" type="button">确 认</button>
                    </div></c:if>
            </div>
        </div>
        <c:if test="${userInfo.accountType!=6 }"><script>
        $(function() {
            $('#set_cash_password_btn').click(function() {

                var cashPassword = $("input[name='cashPassword']").val();
                var recashPassword = $("input[name='recashPassword']").val();
//                 if(!cashPassword || !/^[a-zA-Z0-9_]{6,20}$/.test(cashPassword)){
//                     layer.alert("请输入6至20位提款密码，由数字，字母和下划线组成！");
//                     return false;
//                 }
                
                if(recashPassword!=cashPassword){
                	layer.alert("两次密码不一致！");
                    return false;
                }
        		var pwd = cashPassword;
        		var rpwd = recashPassword;
        		$.ajax({
        			url : "${base}/center/member/meminfo/repwd.do",
        			data : {
        				pwd : pwd,
        				rpwd : rpwd
        			},
        			success : function(result) {
        				layer.alert("修改成功！");
        				$('#main_wrap').load(JIEBAO.base + '/cpSystem/${path}.do');
        			}
        		});
            });
        });
        </script></c:if>