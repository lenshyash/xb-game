<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 设置银行信息 -->
<jsp:include page="payHeadCommunal.jsp" />
   <div class="wrapper main2 bg_black3">
            <div class="content_900 clear">
                    <p class="tips"><i class="ico ico_1"></i>贴心小提醒：请先绑定您的基本资料及银行资料后，再进行会员中心动作</p>
                    <div class="br_line"></div>
                     <c:if test="${userInfo.accountType==6 }"><div>试玩账号不能提款</div></c:if>
                   	<c:if test="${userInfo.accountType!=6 }">
                    <div class="content_frame smallT">
                        <table width="800" border="0" cellspacing="0" cellpadding="0" class="tables">
                            <tbody>
                                <tr style="height: 50px;">
                                    <th><span class="c_red">＊</span>持卡人姓名：</th>
                                    <td class="positionre">
                                        <input class="w200 required userInfo" name="truename" value="${member.userName }" type="text">
                                    </td>
                                    <td rowspan="11" class="note2">
                                        <div class="note2_con">
                                            <p>备注：</p>
                                            <p>1.标记有<span class="c_red">＊</span>者为必填项目。</p>
                                            <p>2.真实姓名须与持卡人姓名一致，否则不予出款。</p>
                                            <p>3.绑定的资料为忘记密码时的认证，请会员务必填写正确资料。</p>
                                            <p>4.资金密码为出款时唯一认证密码，请会员务必牢记，并且勿将
                                                <br>&nbsp;&nbsp;资金密码透漏给其他人。</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <th><span class="c_red">＊</span>出款银行名称：</th>
                                    <td class="positionre">
                                        <select name="bankName" id="bankName">
                                         <option value="建设银行">建设银行</option>
                                         <option value="工商银行" selected="selected">工商银行</option>
                                         <option value="农业银行">农业银行</option>
                                         <option value="中国邮政银行">中国邮政银行</option>
                                         <option value="中国银行">中国银行</option>
                                         <option value="中国招商银行">中国招商银行</option>
                                         <option value="中国交通银行">中国交通银行</option>
                                         <option value="中国民生银行">中国民生银行</option>
                                         <option value="中信银行">中信银行</option>
                                         <option value="中国兴业银行">中国兴业银行</option>
                                         <option value="浦发银行">浦发银行</option>
                                         <option value="平安银行">平安银行</option>
                                         <option value="华夏银行">华夏银行</option>
                                         <option value="广州银行">广州银行</option>
                                         <option value="BEA东亚银行">BEA东亚银行</option>
                                         <option value="广州农商银行">广州农商银行</option>
                                         <option value="顺德农商银行">顺德农商银行</option>
                                         <option value="北京银行">北京银行</option>
                                         <option value="杭州银行">杭州银行</option>
                                         <option value="温州银行">温州银行</option>
                                         <option value="上海农商银行">上海农商银行</option>
                                         <option value="中国光大银行">中国光大银行</option>
                                         <option value="渤海银行">渤海银行</option>
                                         <option value="浙商银行">浙商银行</option>
                                         <option value="晋商银行">晋商银行</option>
                                         <option value="汉口银行">汉口银行</option>
                                         <option value="上海银行">上海银行</option>
                                         <option value="广发银行">广发银行</option>
                                         <option value="深圳发展银行">深圳发展银行</option>
                                         <option value="东莞银行">东莞银行</option>
                                         <option value="宁波银行">宁波银行</option>
                                         <option value="南京银行">南京银行</option>
                                         <option value="北京农商银行">北京农商银行</option>
                                         <option value="重庆银行">重庆银行</option>
                                         <option value="广西农村信用社">广西农村信用社</option>
                                         <option value="吉林银行">吉林银行</option>
                                         <option value="江苏银行">江苏银行</option>
                                         <option value="成都银行">成都银行</option>
                                         <option value="尧都区农村信用联社">尧都区农村信用联社</option>
                                         <option value="浙江稠州商业银行">浙江稠州商业银行</option>
                                         <option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>                                     
                                         <option value="qita">其他</option>
                                        </select>
                                        <br/>
                                       	 <input class="w200 required userInfo" name="bankName" placeholder="请输入银行名称" id="bankName2" style="display:none;height: 28px;"/>
                                    </td>
                                </tr>
                                <tr id="this_way" style="height: 50px;">
                                    <th><span class="c_red">＊</span>出款银行帐号：</th>
                                    <td class="positionre">
                                        <input class="w200 required number userInfo" minlength="10" value="${member.cardNo }" name="bankno" type="text" XX_XX="checkme">
                                        <div class="c_red" XX_XX="checkmsg_bankno"></div>
                                    </td>
                                </tr>
                                <tr id="this_way" style="height: 50px;">
                                    <th>开户行网点：</th>
                                    <td class="positionre">
                                        <input class="w200 required number userInfo" minlength="10" value="${member.bankAddress }" id="bankcardaddress" name="bankcardaddress" type="text" XX_XX="checkme">
                                        <div class="c_red" XX_XX="checkmsg_bankno"></div>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <th><span class="c_red">＊</span>取款密码：</th>
                                    <td class="positionre">
                                        <input class="w200 required userInfo" name="wcode" type="password" autocomplete="false">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="br_line"></div>
                    <div class="btn_submit2">
                        <button class="btn" type="reset" id="chongshe">重 设</button>
                        <button class="btn" XX_XX="uc_setmyaccount_submit" type="button" id="newBankBtn">确 认</button>
                    </div></c:if>
            </div>
        </div>
        <c:if test="${userInfo.accountType!=6 }"><script>
        $(function() {
        	
    		$("#bankName").change(function() {
    			var isQita = this.value;
    			if(isQita == 'qita'){
    				$("#bankName2").show();
    			}else{
    				$("#bankName2").val('');
    				$("#bankName2").hide();
    			}
    		});

        	
        	$('#chongshe').click(function(){
        		$('.userInfo').val("");
        	});
        	
        	
        	 	var param = {};
        		$('#newBankBtn').click(function(){
        			var userName = $("input[name='truename']").val();
        			if(userName==null || userName==""){
        				layer.alert("银行账户名不能为空！");
        				return;
        			}
        			var province = $("input[name='country']").val();//县市、开户行网点可空
        			var cardNo = $("input[name='bankno']").val();
        			if(cardNo==null || cardNo==""){
        				layer.alert("出款银行帐号不能为空！");
        				return;
        			}
        			//取款密码
        			var repPwd = $("input[name='wcode']").val();
        			if(repPwd==null || repPwd==""){
        				layer.alert("取款密码不能为空！");
        				return;
        			}
//         			if(repPwd.length<6){
//         				layer.alert("取款密码不能小于6位！");
//         				return;
//         			}
        			
                    param["userName"] =userName;
                    var cashBankname = $("#bankName").val();
            		var qita = $('#bankName2').val();
            		if(qita){
            			cashBankname = qita;
            		}else{
            			if (!cashBankname) {
            				alert("开户银行不能为空");
            				return false;
            			}else if(cashBankname == 'qita'){
            				if(!qita){
            					alert("开户银行不能为空");
            					return false;
            				}
            			}
            		}
                    param["bankName"] = cashBankname;
                    param["province"] = province;
                    param["city"] = $("input[name='countrysecond']").val();
                    param["bankAddress"] = $("#bankcardaddress").val();
                    param["cardNo"] = cardNo;
                    param["repPwd"] = repPwd;
                	$.ajax({
            			url : "${base}/center/banktrans/draw/cmitbkinfo.do",
            			data : param,
            			type:"POST",
            			success : function(j) {
            				layer.alert("修改成功！");
            				$('#main_wrap').load(JIEBAO.base + '/cpSystem/${path}.do');
            			}
            		});
        		});
                return false;
        });
        </script></c:if>