<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 开奖结果 -->
<div class="wrapper main" style="position: static; zoom: 1;">
	<div class="row bg_black3">
		<div class="row_top result bg_black3">
			<h3>${lot.name }</h3>
		</div>
		<div class="row_con">
			<table width="944" border="0" cellspacing="0" cellpadding="0" class="cancel_order">
				<thead id="headList">
					<tr class="bg_white2">
						<th rowspan="2">期 数</th>
						<th rowspan="2">开奖日期</th>
						<th rowspan="2">开奖時間</th>
						<th rowspan="2">星 期</th>
						<th colspan="<c:if test="${lotEnum=='PCEGG' }">7</c:if><c:if test="${lotEnum!='PCEGG' }">${lotEnum.ballNums }</c:if>" class="w200">彩球号码</th>
					</tr>
					<tr class="bg_white2">
						<c:if test="${lotEnum.ballNums==3 }">
							<c:if test="${lotEnum=='PCEGG' }">
								<td>第一位</td>
								<td>＋</td>
								<td>第二位</td>
								<td>＋</td>
								<td>第三位</td>
								<td>＝</td>
								<td>和值</td>
							</c:if>
							<c:if test="${lotEnum!='PCEGG' }">
								<td>百</td>
								<td>十</td>
								<td>个</td>
							</c:if>
						</c:if>
						<c:if test="${lotEnum.ballNums==5 }">
							<td>万</td>
							<td>千</td>
							<td>百</td>
							<td>十</td>
							<td>个</td>
						</c:if>
						<c:if test="${lotEnum.ballNums==10 }">
							<td>冠</td>
							<td>亚</td>
							<td>季</td>
							<td>四</td>
							<td>五</td>
							<td>六</td>
							<td>七</td>
							<td>八</td>
							<td>九</td>
							<td>十</td>
						</c:if>
						<c:if test="${lotEnum.ballNums==7 }">
							<td>正码一</td>
							<td>正码二</td>
							<td>正码三</td>
							<td>正码四</td>
							<td>正码五</td>
							<td>正码六</td>
							<td>特码</td>
						</c:if>
					</tr>
				</thead>
				<tbody id="bodyList">
				<c:forEach items="${page.list }" var="l">
					<tr>
						<td>${l.qiHao }</td>
						<td>${l.date }</td>
						<td>${l.time }</td>
						<td>${l.weekday }</td>
						<c:choose>
							<c:when test="${lotEnum eq 'LHC' || lotEnum eq 'SFLHC' || lotEnum eq 'TMLHC' || lotEnum eq 'WFLHC' || lotEnum eq 'HKMHLHC' || lotEnum eq 'AMLHC'}">
								<c:forEach items="${l.haoMaList }" var="h">
									<c:if test="${h eq '?' }"><td>-</td></c:if>
									<c:if test="${h ne '?' }"><td><span class="mark_num mark_${h}">${h}</span></td></c:if>
								</c:forEach></c:when>
							<c:otherwise><c:forEach items="${l.haoMaList }" var="h"><td>${h}</td></c:forEach></c:otherwise>
						</c:choose>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<p>&nbsp;</p>
		</div>
		<c:if test="${page!=null && page.list!=null && page.totalPageCount>1}">
		<div class="row_btm bg_black2" style="padding: 0px 10px; line-height: 50px;">
			<ul class="pagination" id="lotResultsPaginationBar" style="margin: 0px; float: left;">
				<li<c:if test="${page.currentPageNo==1}"> class="disabled"</c:if>><a link="1" href="javascript:void(0);"><span>第一页</span></a></li>
				<li<c:if test="${page.currentPageNo==1}"> class="disabled"</c:if>><a link="${page.prePage }" href="javascript:void(0);"><span>上一页</span></a></li>
				<li<c:if test="${page.currentPageNo==page.totalPageCount}"> class="disabled"</c:if>><a link="${page.nextPage }" href="javascript:void(0);"><span>下一页</span></a></li>
				<li<c:if test="${page.currentPageNo==page.totalPageCount}"> class="disabled"</c:if>><a link="${page.totalPageCount }" href="javascript:void(0);"><span>最终页</span></a></li>
				<li><span class="dqpage">当前页次${page.currentPageNo}</span></li>
				<li><select class="rOptions">
					<c:forEach begin="1" end="${page.totalPageCount }" var="i"><option value="${i }"<c:if test="${i== page.currentPageNo}">selected</c:if>>${i }</option></c:forEach>
				</select></li>
			</ul>
			<ul class="pagination pull-right" style="margin: 0px; font-size: small; margin-right: 5px; margin-top: 2px;">
				<li class="disabled"><span>每页 ${page.pageSize} 条纪录 / 共 ${page.totalCount} 条 / 共 ${page.totalPageCount} 页</span></li>
			</ul>
		</div></c:if>
	</div>
	<script>
		$(function() {
			var bar=$("#lotResultsPaginationBar");
			bar.find("a[link]").click(function(){
				var load = new Loading();
				load.init({
					target: "#ocontainer"
				});
				load.start();
				$('#ocontainer').load(JIEBAO.base + '/cpSystem/lotteryResults.do?rows=10&lotCode='+JIEBAO.lotCode+"&page="+$(this).attr("link"),function(){load.stop();});
			});
			bar.find(".rOptions").change(function(){
				var load = new Loading();
				load.init({
					target: "#ocontainer"
				});
				load.start();
				$('#ocontainer').load(JIEBAO.base + '/cpSystem/lotteryResults.do?rows=10&lotCode='+JIEBAO.lotCode+"&page="+$(this).val(),function(){load.stop();});
			})
		})
	</script>
</div>