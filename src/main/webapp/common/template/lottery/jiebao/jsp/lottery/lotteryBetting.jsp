 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 投注报表-->
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<div>
	<input id="betDate" type="hidden">
	<input id="betDetailType" type="hidden">
</div>
<div class="wrapper main order" id="betOrderTable" style="width: 970px; margin: 0px auto;"
	xx_xx="sbet_paper_area">
	<div class="row bg_black3">
		<div class="row_con">
			<div class="order_report clearfix">
				<div class="search_cat" style="margin-top: 25px; zoom: 1;">
					<span>种 类</span> <select class="w280" name="select2" id="tzGameType">
					</select>
				</div>
				<div class="search_date bg_black3">
					<div class="search_date_title">
						<span>浏览日期</span>
					</div>
					<div class="search_date_pick">
						<span class="datepickerIconGroup">
						<input placeholder="开始日期" class="datepicker w200" name="bStartTime" type="text" id="bStartTime" value="${now }">
						<i class="fa fa-calendar fa-lg dateimg c_black"></i></span> <br> <br>
						<span class="datepickerIconGroup">
						<input placeholder="结束日期" class="datepicker w200" name="bEndTime" type="text" id="bEndTime" value="${now }">
						<i class="fa fa-calendar fa-lg dateimg c_black"></i></span>
					</div>
					<div class="search_date_btn">
						<ul>
							<li><input type="button" value="今 日" class="btn b1" d="today" aims="bStartTime,bEndTime"></li>
							<li><input type="button" value="昨 日" class="btn b1" d="yesterday" aims="bStartTime,bEndTime"></li>
							<li><input type="button" value="本 周" class="btn b1" d="week" aims="bStartTime,bEndTime"></li>
							<li><input type="button" value="上 周" class="btn b1" d="lastweek" aims="bStartTime,bEndTime"></li>
							<li><input type="button" value="本 月" class="btn b1" d="month" aims="bStartTime,bEndTime"></li>
							<li><input type="button" value="上 月" class="btn b1" d="lastmonth" aims="bStartTime,bEndTime"></li>
						</ul>
					</div>
				</div>
				<div class="search_btn" id="betOrderQuery">
					<span><a href="javascript:getBetTotal();" class="btn b1"><span
							class="fa-stack c_white3" style="float: left;"><i
								class="fa fa-circle fa-stack-2x"></i><i
								class="fa fa-search fa-stack-1x fa-inverse"></i></span>&nbsp;报表查询</a> </span>
				</div>
			</div>
			<div style="height: 25px; margin-top: -15px; text-align: center;">※请按查询送出※
				查询归属于 00:00:00 - 23:59:59 的注单报表。</div>
		</div>
	</div>
	<div class="row bg_black3" id="betOrderWrap"
		style="position: static; zoom: 1;">
		<div class="row_top bg_black3">
			<h3>
				※<span id="srcListTypeName">投注报表</span> &nbsp; 彩种：<span id="betName">全部</span>&nbsp;&nbsp;<span
					id="betBetListBetween">2016-05-01 - 2016-05-31</span><span
					class="tips2 btn bg_black3"><i
					class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心小提醒：点击注单号能查询详细投注明细</span>
			</h3>
		</div>
		<div class="row_con">
			<table width="944" border="0" cellspacing="0" cellpadding="0"
				class="cancel_order">
				<thead>
					<tr class="bg_white2">
						<th>报表日期</th>
						<th>有效笔数</th>
						<th>有效投注总额</th>
						<th>实际盈亏</th>
						<th>功能操作</th>
					</tr>
				</thead>
				<tbody id="betBetList">
				</tbody>
				<tfoot id="betTotalList">
				</tfoot>
			</table>
			<br>
		</div>
	</div>
</div>

<div class="wrapper main order" id="betOrderTableDetail" style="width: 970px; margin: 0px auto; display: none;"
	xx_xx="bet_paper_area">
	<div class="row bg_black3" id="betOrderWrap"
		style="position: static; zoom: 1;">
		<div class="row_top bg_black3">
			<h3>
				<span id="srcListTypeName" class="tips2 btn bg_black3"
					style="margin-left: -5px;">※ 搜尋內容</span>&nbsp;<span id="betNameDetail"
					class="tips2 btn bg_black3" style="margin-left: 5px;">全部</span>&nbsp;&nbsp;<span
					class="tips2 btn bg_black3" id="betListBetweenDetail"
					style="margin-left: 0px;">报表日期：2016-05-24</span><span
					class="tips2 btn bg_black3"><i
					class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心小提醒：点击注单号能查询详细投注明细</span>
			</h3>
		</div>
		<div class="row_con">
			<p>&nbsp;</p>
			<table width="944" border="0" cellspacing="0" cellpadding="0"
				class="cancel_order">
				<thead>
					<tr class="bg_white2">
						<th>注单号</th>
						<th>彩 种</th>
						<th>期 号</th>
						<th>玩 法</th>
						<th>内 容</th>
						<th>下注金额</th>
						<th>中奖金额</th>
						<th>实际输嬴</th>
						<th>状态</th>
					</tr>
				</thead>
				<tbody id="betListDetail">
				</tbody>
				<tfoot id="betOrderTblFootDetail">
				</tfoot>
			</table>
			<p>&nbsp;</p>
		</div>
		<div class="row_btm bg_black2" id="betOrderTblTabs" style="padding: 0px 10px; line-height: 50px; display: block;">
			<ul class="pagination" id="betPaginationBar" style="margin:0px; float:left;">
				<li class="disabled"><a link="1" href="javascript:void(0);" onclick="betSelectPage(this)" ><span>第一页</span></a></a></li>
				<li class="disabled"><a link="1" href="javascript:void(0);" onclick="betSelectPage(this)" ><span>上一页</span></a></li>
            	<li class="disabled"><a link="2" href="javascript:void(0);" onclick="betSelectPage(this)" ><span>下一页</span></a></li>
            	<li class="disabled"><a link="1" href="javascript:void(0);" onclick="betSelectPage(this)" ><span>最终页</span></a></li>
            	<li class="disabled"><span class="betDqPage" link="1">当前页次1</span></li>
				<li><select class="betOptions" onchange="betSelect()"><option value="1">1</option></select></li>
			</ul>
            <ul class="pagination pull-right" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;">
				<li class="disabled"><span id="betPaginationInfo"></span></li>
	      	</ul>
		</div>
	</div>
</div>

<script>
	$(function() {
		$("#bStartTime").datepicker();
		$("#bEndTime").datepicker();
		getLottery();
		getBetTotal();
		$("#betBetListBetween").html($("#bStartTime").val()+"    "+$("#bEndTime").val());
	})
	
	function betSelect(){
		    var page = $(".betOptions").val();
		    $(".betOptions").val(page);
		    getTZDetail(page);
	    }
	    
	    function betSelectPage(el){
	    	var cl = $(el).parent().attr("class");
	    	if(cl == "disabled"){
	    		return false;
	    	}
		    var page = $(el).attr("link");
		    getTZDetail(page);
	    }

	function getLottery() {
		$.ajax({
			url : '${base}/lottery/getLottery.do',
			data : '',
			success : function(r) {
				var gameSelect = '<option value="">全部</option>';
				for (var i = 0; i < r.length; i++) {
					//if(r[i].name!='六合彩'){
						gameSelect +='<option type="'+r[i].type+'" value="'+r[i].code+'">'+r[i].name+'</option>';
					//}
				}
				$("#tzGameType").html(gameSelect);
			}
		})
	}
	
	function getBetTotal(){
		var code = $("#tzGameType").val();
		var startTime =$("#bStartTime").val();
		var endTime =$("#bEndTime").val();
		if(startTime!=""){
			startTime = startTime+' 00:00:00';
		}
		if(endTime!=""){
			endTime = endTime+' 23:59:59';
		}
		
		$.ajax({
			url:'${base}/lottery/geTotal.do',
			data:{
				lotCode : code,
				startTime : startTime,
				endTime : endTime,
			},
			success:function(r){
				var betBetList ='';
				var bsTtotal = 0;
				var zeTtotal = 0;
				var ykTtotal = 0;
				if(r.length==0){
					betBetList +='<tr><td colspan="5">暂无数据</td></tr>';
					$("#betBetList").html(betBetList);
					$("#betTotalList").html("");
					$("#betBetListBetween").html($("#bStartTime").val()+"    "+$("#bEndTime").val());
				}else{
					for(var i=0;i<r.length;i++){
						if(r[i].actualYingKui == undefined){
							r[i].actualYingKui ='0';
						}
						bsTtotal += r[i].effectiveBiZhu;
						Math.formatFloat(zeTtotal += r[i].effectiveTouZhuTotal);
						ykTtotal += Number(r[i].actualYingKui);
						betBetList +='<tr>';
						betBetList +='<td id="date">'+r[i].date+'</td>';
						betBetList +='<td>'+r[i].effectiveBiZhu+'</td>';
						betBetList +='<td>'+r[i].effectiveTouZhuTotal+'</td>';
						betBetList +='<td>'+r[i].actualYingKui+'</td>';
						betBetList +='<td>';
						betBetList +='<input type="button" value="全 部" detail="0" class="btn b1" id="btn1">';
						betBetList +='<input type="button" value="中 奖" detail="1" class="btn b1" id="btn2">';
						betBetList +='<input type="button" value="撤 单" detail="2" class="btn b1" id="btn3">';
						betBetList +='<input type="button" value="有 效" detail="3" class="btn b1" id="btn4">';
						betBetList +='<input type="button" value="未 中 奖" detail="4" class="btn b1" id="btn5"></td>';
						betBetList +='</tr>';
					}
					var betTotalList ='<tr>';
					betTotalList +='<td>总计：</td>';
					betTotalList +='<td>'+bsTtotal+'</td>';
					betTotalList +='<td>'+zeTtotal+'</td>';
					betTotalList +='<td>'+ykTtotal+'</td>';
					betTotalList +='<td>-</td>';
					betTotalList +='</tr>';
				$("#betBetList").html(betBetList);
				$("#betTotalList").html(betTotalList);
				$("#betBetListBetween").html($("#bStartTime").val()+"    "+$("#bEndTime").val());
				}
			}
		})
	}
	
	//detail  0.全部   1.中奖   2.撤单  3.有效  4.未开奖
	$("#betBetList").on("click",".btn", function(){
		var val = $(this).val();
		var date = $(this).parent().prevAll("#date").text();
		var detailType = $(this).attr("detail");
		$("#betNameDetail").text(val);
		$("#betDate").val(date);
		$("#betDetailType").val(detailType);
		$("#betListBetweenDetail").html('报表日期：'+date);
		getTZDetail(1);
	})
	
	function getTZDetail(page){
		var date = $("#betDate").val();
		var detailType = $("#betDetailType").val();
		$.ajax({
			url : '${base}/lottery/getTZDetail.do',
			data : {
				date : date,
				detailType : detailType,
				rows :10,
				page :page,
				},
			success:function(r){
				var list = r.page.list;
				var betListDetail = "" ;
				for(var i=0;i<list.length;i++){
					if(list[i].haoMa.length>10){
						list[i].haoMa = list[i].haoMa.substr(0, 10)+'...';
					}
					betListDetail += '<tr><td id="betOrderId">'+list[i].orderId+'</td>';
					betListDetail += '<td>'+list[i].lotName+'</td>';
					betListDetail += '<td>'+list[i].qiHao+'</td>';
					betListDetail += '<td>'+list[i].playName+'</td>';
					betListDetail += '<td><a href="javascript:void(0);" style="width: 100%;" class="BetInf"orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'" >'+list[i].haoMa+'</a></td>';
					betListDetail += '<td>'+list[i].buyMoney+'</td>';
					betListDetail += '<td>'+list[i].winMoney+'</td>';
					betListDetail += '<td>'+list[i].yingKui+'</td>';
					//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
					if(list[i].status == 1 ){
						betListDetail+='<td><span style="background-color: #428bca;border-radius: .25em;padding: 0.2em .6em 0.3em;">未开奖 </span></td>'
					}else if(list[i].status == 2 ){
						betListDetail+='<td><span style="background-color: #d9534f;border-radius: .25em;padding: 0.2em .6em 0.3em;">已中奖 </span></td>'
					}else if(list[i].status == 3 ){
						betListDetail+='<td><span style="background-color: #5cb85c;border-radius: .25em;padding: 0.2em .6em 0.3em;">未中奖 </span></td>'
					}else if(list[i].status == 4 ){
						betListDetail+='<td><span style="background-color: #5bc0de;border-radius: .25em;padding: 0.2em .6em 0.3em;">撤单 </span></td>'
					}else if(list[i].status == 5 ){
						betListDetail+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚成功 </span></td>'
					}else if(list[i].status == 6 ){
						betListDetail+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">回滚异常 </span></td>'
					}else if(list[i].status == 7 ){
						betListDetail+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;">开奖异常</span></td>'
					}
					betListDetail += '</tr>';
				}
				$("#betListDetail").html(betListDetail);
				var betOrderTblFootDetail ='<tr class="bg_white2">';
				betOrderTblFootDetail +='<td colspan="5" style="text-align: right;">小计：</td>';
				betOrderTblFootDetail +='<td>'+r.subBuyMoney+'</td>';
				betOrderTblFootDetail +='<td>'+r.subWinMoney+'</td>';
				betOrderTblFootDetail +='<td>'+r.subYingKui+'</td>';
				betOrderTblFootDetail +='<td>--</td></tr>';
				betOrderTblFootDetail +='<tr class="bg_white2">';
				betOrderTblFootDetail +='<td colspan="5" style="text-align: right;">总计：</td>';
				betOrderTblFootDetail +='<td>'+r.sumBuyMoney+'</td>';
				betOrderTblFootDetail +='<td>'+r.sumWinMoney+'</td>';
				betOrderTblFootDetail +='<td>'+r.sumYingKui+'</td>';
				betOrderTblFootDetail +='<td>--</td></tr>';
				$("#betOrderTblFootDetail").html(betOrderTblFootDetail);
				$("#betOrderTable").hide();
				$("#betOrderTableDetail").show();
				pageChuLi(r.page,page,"bet");
			}
		})
	}
	
	//js精度处理
	Math.formatFloat = function(f, digit) { 
	    var m = Math.pow(10, digit); 
	    return parseInt(f * m, 10) / m; 
	} 
</script>