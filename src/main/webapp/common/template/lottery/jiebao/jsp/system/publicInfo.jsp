<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 系统公告 -->
<style>
	.message_tablex {
	    border-bottom: 1px solid #cfcfcf;
	    border-left: 1px solid #cfcfcf;
	    font-size: 13px;
	}
	.message_tablex th, .message_tablex td {
	    border-top: 1px solid #cfcfcf;
	    border-right: 1px solid #cfcfcf;
	    padding: 15px 5px;
	    line-height: 1em;
	    vertical-align: top;
	}
	.showDesc{
		color: white;
	}
	.textcenter{
		text-align: center;
	}
</style>
<div class="wrapper main2 bg_black3">
	<div class="content_944 clear">
		<div class="content_frame" id="content_frame">
			<form id="uc_letters_form">
				<table width="940" border="0" cellspacing="0" cellpadding="0"
					class="message_tablex bg_black3">
					<thead>
						<!-- 						<tr> -->
						<!-- 							<th colspan="5"> -->
						<!-- 								<div class="message_cat" id="messageStateSelect"> -->
						<!-- 									<a href="javascript:;" class="current" messageState="0">全部</a>｜<a -->
						<!-- 										href="javascript:;" messageState="2">未过期</a>｜<a -->
						<!-- 										href="javascript:;" messageState="1">已过期</a> -->
						<!-- 								</div> -->
						<!-- 							</th> -->
						<!-- 						</tr> -->
						<tr class="bg_white4">
							<th style="width:20%;font-size: initial;" class="textcenter">编 号</th>
							<th style="width:50%;font-size: initial;">标 题</th>
							<th style="width:30%;font-size: initial;" class="textcenter">发布时间</th>
						</tr>
					</thead>
					<tbody id="uc_content">

					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<script>
	$(function() {
			var code = 9;//彩票公告
			$.ajax({
				url:JIEBAO.base+"/cpSystem/list.do",
				data:{"code":code},
				dataType:"json",
				type:"GET",
				success:function(j){
					var data = j.rows;
					var col = '';
					for(var j in data){
						col+='<tr><td class="textcenter">'+data[j].id+'</td><td><a href="javascript:void(0)" title="'+data[j].title+'" class="showDesc" id="'+data[j].id+'">'+data[j].title+'</a></td><td class="textcenter">'+getLocalTime(data[j].updateTime)+'</td></tr>';
					}
					$('#uc_content').html(col);
					$('.showDesc').click(function(){
						playDesc(this.id,this.title);
					});
				}
			});
	});
	
	//详情
	function playDesc(id,title){
		layer.open({
			  type: 2,
			  title: title,
			  scrollbar: false,
			  shadeClose: true,
			  offset : ['10%'],
			  shade: 0.8,
			  area: ['800px', '500px'],
			  content: JIEBAO.base +"/cpSystem/publicInfoDesc.do?id="+id
			}); 
	}
	
	//时间戳转时间
	function getLocalTime(nS) {  
		var date = new Date(nS);
		var dateFormat=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
	   return dateFormat;     
	} 
</script>
<style>
.pubOver:hover{
   background-color: gray;
}
</style>