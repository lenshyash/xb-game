<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<!-- 提现记录 -->
<jsp:include page="payHeadCommunal.jsp" />
  <div class="wrapper main2 bg_black3" id="uc_user_area">
            <div class="content_944 clear">
             <form id="uc_cashflowrecord_form">
                    <div class="buy_cat">
                        <span class="input-select datepickerIconGroup"><input class="datepicker w200" value="${now }" name="std"type="text"id="uc_cashflowrecord_std_format"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select datepickerIconGroup"><input class="datepicker w200" value="${now }" name="end"type="text"id="uc_cashflowrecord_end_format"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select"><a href="javascript:;"class="btn b0"id="payQuery">查 询</a></span>
                    </div>
                </form>
                <div class="content_frame smallT">
                    <table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
                        <thead>
                            <tr class="bg_white2">
                                <td>订单号</td>
                                <td>提现金额</td>
                                <td>银行尾号</td>
                                <td>申请时间</td>
                                <td>状态</td>
                                <td>备注</td>
                            </tr>
                        </thead>
                        <tbody id="uc_cashflowrecord_tbody"></tbody>
                        <tfoot id="betOrderTblFoot">
		                    <tr id="betOrderTblTabs">
		                        <td colspan="7">
		                           <ul class="pagination pull-right tcdPageCode" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;"><span id="paginationInfo"></span></ul>
		                        </td>
		                    </tr>
		                </tfoot>
                    </table>
                </div>
            </div>
        </div>
<script>
	$(function() {
		$('#curOver3').addClass("current");
		$("#uc_cashflowrecord_std_format").datepicker();
		$("#uc_cashflowrecord_end_format").datepicker();
		list(1,startDate(),endDate());
    	total();
    	$('#payQuery').click(function(){
    		var startTime = startDate();
    		var endTime = endDate();
    		list(1,startTime,endTime);
    	});
	});
	
	   function startDate(){
       	var startTime = $("#uc_cashflowrecord_std_format").val();
       	if(startTime!=null && startTime!=""){
   			startTime = startTime+' 00:00:00';
   		}
       	return startTime;
       }
       
       function endDate(){
       	var endTime = $("#uc_cashflowrecord_end_format").val();
   		if(endTime!=null && endTime!=""){
   			endTime = endTime+' 23:59:59';
   		}
   		return endTime;
       }
	
	function list(pageNo,start,end){
    	var page = {
 				"rows" : 10,
				"page" :pageNo,
				"begin" :start,
				"end" :end
        	};
    	$.ajax({
			url:"${base}/cpSystem/drawrdList.do",
			data : page,
			type:"POST",
			success:function(j){
				var col = '';
				for(var data in j.rows){
					col+='<tr><td>'+j.rows[data].orderNo+'</td>';
					col+='<td>'+j.rows[data].drawMoney+'</td>';
					col+='<td>**'+(j.rows[data].cardNo).Right(4)+'</td>';
					col+='<td>'+getLocalTime(j.rows[data].createDatetime)+'</td>';
					if(j.rows[data].status==1){
						col+='<td style="color:orange">处理中</td>';
					}else if(j.rows[data].status==2){
						col+='<td style="color:green">处理成功</td>';
					}else if(j.rows[data].status==3){
						col+='<td style="color:red">处理失败</td>';
					}else if(j.rows[data].status==4){
						col+='<td style="color:gray">已取消</td>';
					}
					if(j.rows[data].remark!=null){
						col+='<td>'+j.rows[data].remark+'</td>';
					}else{
						col+='<td>-</td>';
					}
				
					col+='</tr>';
				}
				$('#uc_cashflowrecord_tbody').html(col);
			}
		});
	}
	
	  function total(){
      	$.ajax({
				url:"${base}/cpSystem/drawrdList.do",
				type:"POST",
				success:function(j){
					var pageTotal = j.total;
					pageTotal = Math.ceil(pageTotal/10);
		         	$("#paginationInfo").createPage({
	 	             pageCount:pageTotal,
	 	             current:1,
	 	             backFn:function(p){
	 	              list(p,startDate(),endDate());//p为页码
	 	             }
	 	         });
				}
			});
	  	}
</script>