<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 奖金对照 -->
<div class="wrapper main order" style="position: static; zoom: 1;">
	<div class="row bg_black3">
		<div class="row_con">
			<div class="bonus_search clearfix">
				<div class="bonus_cat">
					<span>种 类</span> <select class="w200" id="LBgameSelect">
						<c:forEach items="${lotteryList }" var="l">
							<c:if test="${l.code ne 'LHC' && l.code ne 'SFLHC' && l.code ne 'TMLHC' && l.code ne 'WFLHC' && l.code ne 'HKMHLHC' && l.code ne 'AMLHC'}">
								<option value="${l.code}" p="${l.type }" <c:if test="${l.code==lotCode }">selected</c:if>>${l.name }</option>
							</c:if>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row bg_black3">
		<div class="row_top bg_black3">
			<h3>※ <c:if test="${not empty lot }">${lot.name }</c:if>奖金查询 :</h3>
		</div>
		<c:if test="${not empty playList && playList.size() >0}"><c:set var="size" value="${playList.size() }"></c:set>
		<div class="row_con clearfix" style="overflow: auto; zoom: 1;">
			<table width="300" border="0" cellspacing="0" cellpadding="0" class="bonus_search_table">
				<tbody id="tb1">
					<tr>
						<th class="bg_white2">玩法名称</th>
						<th class="bg_black3">元模式奖金</th>
					</tr>
					<c:set var="len" value="${size/3 }"></c:set><c:if test="${size%3!=0 }"><c:set var="len" value="${len+1 }"></c:set></c:if>
					<c:forEach begin="0" end="${len-1 }" var="i">
					<c:set var="p" value="${playList.get(i) }"></c:set><tr>
						<th>${p.name }</th>
						<td>${p.minBonusOdds }</td>
					</tr></c:forEach>
				</tbody>
			</table>
			<table width="300" border="0" cellspacing="0" cellpadding="0" class="bonus_search_table">
				<tbody id="tb2">
					<tr>
						<th class="bg_white2">玩法名称</th>
						<th class="bg_black3">元模式奖金</th>
					</tr>
					<c:set var="len1" value="${size/3*2 }"></c:set><c:if test="${size%3==1 }"><c:set var="len1" value="${len1+1 }"></c:set></c:if>
					<c:forEach begin="${len}" end="${len1-1 }" var="i">
					<c:set var="p" value="${playList.get(i) }"></c:set><tr>
						<th>${p.name }</th>
						<td>${p.minBonusOdds }</td>
					</tr></c:forEach>
				</tbody>
			</table>
			<table width="300" border="0" cellspacing="0" cellpadding="0" class="bonus_search_table">
				<tbody id="tb3">
					<tr>
						<th class="bg_white2">玩法名称</th>
						<th class="bg_black3">元模式奖金</th>
					</tr>
					<c:forEach begin="${len1 }" end="${playList.size()-1 }" var="i">
					<c:set var="p" value="${playList.get(i) }"></c:set><tr>
						<th>${p.name }</th>
						<td>${p.minBonusOdds }</td>
					</tr></c:forEach>
				</tbody>
			</table>
		</div></c:if>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#LBgameSelect').on('change', function() {
				var load = new Loading();
				load.init({
					target: "#ocontainer"
				});
				load.start();
				$('#ocontainer').load(JIEBAO.base + '/cpSystem/lotteryBonus.do?lotCode='+$(this).val(),function(){load.stop();});
            });
		});
	</script>
</div>