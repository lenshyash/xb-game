<%@ page language="java" pageEncoding="UTF-8"%>
<div>
<p class="p1">
    简介：
</p>
<p class="p1">
    彩票在线平台提供江苏、湖北、上海、广西、安徽、河北等多城市的彩票玩法，玩法多，乐趣多。快三投注区号码范围为01〜06，每期开出3个号码作为中奖号码。快三玩法即是竞猜3位开奖号码的全部或部分号码。
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    和值投注 &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个号码的和值进行投注，包括“和值3”至“和值18”投注。 &nbsp;
</p>
<p class="p1">
    投注方案：和值15
</p>
<p class="p1">
    开奖号码:前三位555，即中奖。
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    大小单双 &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个号码的总和进行投注，总和大于10为大，小于等于10为小。 &nbsp;
</p>
<p class="p1">
    投注方案：单
</p>
<p class="p1">
    开奖号码:前三位122，即中奖。
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    三同号通选（即全包） &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对所有相同的三个号码（111、222、…、666）进行投注。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   三同号单选 &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指从所有相同的三个号码（111、222、…、666）中任意选择一组号码进行投注。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   二同号复选 &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个号码中两个指定的相同号码和一个任意号码进行投注。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   二同号单选 &nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   三不同号&nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个各不相同的号码进行投注。。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   二不同号&nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对三个号码中两个指定的不同号码和一个任意号码进行投注。 &nbsp;
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
   三连号通选（即全包）&nbsp; &nbsp;
</p>
<p class="p1">
    &nbsp;是指对所有三个相连的号码（仅限：123、234、345、456）进行投注。 &nbsp;
</p>
</div>