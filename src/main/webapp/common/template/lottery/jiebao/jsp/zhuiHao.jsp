<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row r5 bg_black3 hide" id="betTrace">
	<div class="row_top bg_black3">
		<ul>
			<li class="current"><a href="javascript:;" class="btn b0">常规追号</a></li>
		</ul>
		<span class="stop_no"><label>中奖后停止追号<input type="checkbox" id="lt_trace_stop"checked></label></span>
	</div>
	<div class="content_944" id="betTractLoading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>数据处理中，请稍候..</div>
	<div class="row_con clearfix hide" id="betTractCon">
		<div class="r5_c1 bg_black3">
			<div>
				总期数：<span id="lt_trace_count">10</span>&nbsp;期 ,追号总金额：<span class="text-danger" id="lt_trace_hmoney">0</span>元
			</div>
			<div class="normal_catch">
				<div>
					追号期数：&nbsp; <input id="periodNumLeader" type="text" class="w40" value="10" maxlength="2">
					 &nbsp;&nbsp; 起始倍数：&nbsp; <input id="startBeeNum" type="text" class="w40" value="1" maxlength="4">
					 &nbsp; <input type="button" id="creatGtjhBtn" class="btn b1 gbv" style="margin-left: 30px;" value="生成追号方案">
				</div>
			</div>
		</div>
		<div class="r5_c2 bg_black3">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="30"><input type="checkbox" id="betTraceSelectAll" checked></th>
						<th width="160">期 号</th>
						<th width="110">倍率</th>
						<th>当前投入</th>
						<th>累计投入</th>
						<th>盈利(元)</th>
						<th>盈利率</th>
					</tr>
				</thead>
				<tbody id="betTraceData">
				</tbody>
			</table>
		</div>
	</div>
</div>