<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/core.js" path="${base}"></script>
<!-- 帐变记录-->
<jsp:include page="payHeadCommunal.jsp" />
<div class="wrapper main2 bg_black3" id="uc_user_area">
            <div class="content_944 clear">
                <form id="uc_cashflowrecord_form">
                    <div class="buy_cat">
                        <span class="input-select datepickerIconGroup"><input class="datepicker w155" value="${now }" name="std"type="text" id="startTime"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select datepickerIconGroup"><input class="datepicker w155" value="${now }" name="end"type="text" id="endTime"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select">订单号:<input type="text" id="orderId" class="mycss" style="height: 30px;width: 150px;margin-left: 3px;" /></span>
                        <span class="input-select"><a href="javascript:;"class="btn b0" id="payQuery">查 询</a></span>
                    </div>
                </form>
                <div class="content_frame smallT">
                    <table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
 			<div  class="yhjy-tzjl">
			<div class="ml20">
			</div>
			<div id="record_tb" class="yhjy-edzh" style="height:500px;overflow:auto"></div>
          </div>
                        <tfoot id="betOrderTblFoot">
                            <tr id="betOrderTblTabs">
                                <td colspan="8">
                                   <ul class="pagination pull-right tcdPageCode" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;"><span id="paginationInfo">
                                   	<jsp:include page="/common/include/pageToJieBao.jsp"></jsp:include>
                                   </span></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <script id="record_tpl" type="text/html">
		<table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
				<thead>
					<tr class="bg_white2">
						{{each colmuns as col}}
							<td class="myWidth">{{col}}</td>
						{{/each}}
					</tr>
				</thead>
				<tbody id="content">
					{{each list as row index}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key index}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script id="moneytype_tpl" type="text/html">
	{{each data as option}}
        <option value="{{option.id}}">{{option.name}}</option>
	{{/each}}
</script>
        <script>
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "变动前金额", "变动金额", "变动后余额", "订单号","备注" ],
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "beforeMoney", "money", "afterMoney", "orderId","remark" ],
	}
	var navUrls = {
		1 : "${base}/center/record/mnyrecord/list.do",
	}
	var curNavFlag = 1;
	var moneyType = ${moneyType};
	var curDates = [];
	$(function() {
    	$('#curOver9').addClass("current");
    	$("#startTime").datepicker();
		$("#endTime").datepicker();
    	$('#payQuery').click(function(){
    		search();
    	});
// 		initMoneyType();
		initRdsData(curNavFlag);
		first = false;
	})

	function search() {
		initRdsData(curNavFlag);
	}
	
	function initMoneyType(){
		var data = [];
		for ( var key in moneyType) {
			if(10<key && key<15){
				continue;
			}
			if("off" == '${isZrOnOff}' && 14<key && key<16){
				continue;
			}
			if("off" == '${isDzOnOff}' && 14<key && key<16){
				continue;
			}
			if("off" == '${isCpOnOff}' && 129<key && key<134){
				continue;
			}
			if("off" == '${isLhcOnOff}' && 139<key && key<143){
				continue;
			}
			if("off" == '${isTyOnOff}' && 200<key && key<205){
				continue;
			}
			if("off" == '${isExChgOnOff}' && 18<key && key<21){
				continue;
			}
			
			data.push({"id":key,"name":moneyType[key]})
		}
		var eachdata = {
			"data" : data
		};
		var html = template('moneytype_tpl', eachdata);
		$("#moneyType").append(html);
	}

	template.helper('$formatter', function(content, key,index) {
		if(!key || !content){
			return "";
		}
		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		} else if (key == "type") {
			return moneyType[content];
		} else if (key.toUpperCase().indexOf("MONEY") != -1) {
			if(content > 0){
				return '<font color="orange">'+content+'</font>';
			}else{
				return '<font color="red">'+content+'</font>';
			}
		} 
		/* else if (key == "orderId") {
			content = "<a href='javascript:void(0);' onclick='mnyrdDetail("+index+");'><font color='blue'>"+content+"</font></a>";
		}  */
		
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		var a = $(".category").find("a");
		a.removeClass("current");
		if(flag==1){
			a.eq(0).addClass("current");
		}else if(flag==2){
			a.eq(1).addClass("current");
		}else if(flag==3){
			a.eq(2).addClass("current");
		}
		var orderId = $("#orderId").val();
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		var cur_ps = $("#jump_page_size").val();
		var cur_pn = $("#jump_page").val();
		if(cur_pn && cur_ps){
			pageNumber = cur_pn;
			pageSize = cur_ps;
		}
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}
		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				orderId :orderId
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				curDates = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					search();
				});
			}
		});
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}
        </script>
        <style>
        	.myWidth{
        		width: 52px;
        	}
        	.message_table th, .message_table td {
			    border-top: 1px solid #cfcfcf;
			    padding: 15px 5px;
			    text-align: center;
			    line-height: 1em;
			    vertical-align: top;
			    border-right: 1px solid #cfcfcf;
   	 		}
        </style>