<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 在线取款-->
<div class="wrapper main" XX_XX="uc_onlinecash_area">
	<div class="row bg_black3">
		<div class="row_top bg_black3">
			<h3 class="title_line">
				在线取款<span></span>
			</h3>
		</div>
		<div class="row_con content_944">
		<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能提款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }"><form XX_XX="uc_onlinecash_form">
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="85%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">提示信息</th>
								<td id="uc_tips">每天的提现处理时间为：<strong
									style="font-size: 15px; color: #ffb400;"> 早上 <span
										id="startTime"></span> 至 晚上 <span id="endTime"></span></strong>;<br>提现10分钟内到账。(如遇高峰期，可能需要延迟到三十分钟内到帐)<br>用户每日最小提现&nbsp<strong
									style="color: #ffb400; font-size: 15px;" id="minCash"></strong>&nbsp;元，最大提现&nbsp<strong
									style="color: #ffb400; font-size: 15px;" id="maxCash"></strong>&nbsp;元。<br>
									<span>今日可取款&nbsp;<strong style="color: #ffb400; font-size: 15px;" id="toFetch"></strong>&nbsp;次,已取款&nbsp;<strong style="color: #ffb400; font-size: 15px;" id="alreadyFetch"></strong>&nbsp;次</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="85%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">消费比例</th>
								<td id="custome_rate_wrap"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="40%">
							<col width="15%">
							<col width="30%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">会员账户</th>
								<td XX_XX="uc_accountno" id="account"></td>
								<th class="bg_white4">真实姓名</th>
								<td XX_XX="uc_accountname" id="realName"></td>
							</tr>
							<tr>
								<th class="bg_white4">帐户金额</th>
								<td XX_XX="uc_amount" id="amount"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="40%">
							<col width="15%">
							<col width="30%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">银行帐号</th>
								<td XX_XX="uc_bankAccount" id="bankAccount"></td>
								<th class="bg_white4">银行</th>
								<td XX_XX="uc_bankName" id="bankName"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="40%">
							<col width="15%">
							<col width="30%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">提现金额</th>
								<td class="positionre"><input name="amount" type="text"
									class="w200 required" value="0" id="qkCash"></td>
								<th class="bg_white4">提现金额大写</th>
								<td class="c_yellow"><span id="amount_CH">零</span>元整</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="dep_money bg_black3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="15%">
							<col width="85%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bg_white4">取款密码</th>
								<td class="positionre"><input name="wpass" type="password"
									class="w200 required" id="qkPwd"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="br_line"></div>
				<div class="btn_submit2">
					<button class="btn" XX_XX="uc_onlinecash_submit" type="button" id="qkBtn">提交</button>
				</div>
			</form></c:if>
		</div>
	</div>
</div>
<c:if test="${userInfo.accountType!=6 }"><script>
	$(function() {
		var maxCash = 0;
		var minCash = 0;
		$.ajax({
			url : "${base}/center/banktrans/draw/drawdata.do",
			success : function(j) {
				$('#startTime').html(j.star);
				$('#endTime').html(j.end);
				$('#minCash').html(j.min);
				$('#maxCash').html(j.max);
				maxCash = parseFloat(j.max);
				minCash = parseFloat(j.min);
				$('#account').html(j.member.account);
				$('#realName').html("**"+(j.member.userName).Right(1));
				$('#amount').html(j.member.money+" RMB");
				$('#custome_rate_wrap').html('出款需达投注量：<strong style="font-size:15px;color:#ffb400;margin-right:10px;">'+j.checkBetNum+'</strong>有效投注金额：<strong class="dmlStyle">'+j.member.betNum+'</strong>是否能取款：<strong class="dmlStyle">'+j.drawFlag+'</strong>');//消费比例
				$('#bankAccount').html("**"+(j.member.cardNo).Right(4));//银行账号
				$('#bankName').html(j.member.bankName);//银行
				$('#alreadyFetch').html(j.curWnum);
				$('#toFetch').html(j.wnum);
			}
		});
		
		
		$('#qkCash').click(function(){
			$('#qkCash').val("");
		});
		
		 $( "body" ).delegate( "[name='amount']", 'keyup', function(){
		        /* 取出输入的金额,并转换成 */
		        $( "#amount_CH" ).text( convertCurrency( $( this ).val() ) );
		    });
		 
		 var qkCachExp = /^[1-9]([\d]{0,7})?$/;
		 //点击提交
		 $('#qkBtn').click(function(){
			var repPwd = $('#qkPwd').val();
			var money =$('#qkCash').val();
			if(!qkCachExp.test(money)){
				layer.alert("取款金额必须是整数");
				return false;
			}
			money=parseInt(money);
			if(money < minCash){
				layer.alert("提现金额不能小于"+minCash);
				return false;
			}
			
			if(money > maxCash){
				layer.alert("提现金额不能大于"+maxCash);
				return false;
			}
			
			if(repPwd == null ||repPwd == ""){
				layer.alert("取款密码不能为空");
				return false;
			}
			
// 			if(repPwd.length<6){
// 				layer.alert("取款密码不能小于6位!");
// 				return false;
// 			}
			var params = {
					"repPwd" : repPwd,
					"money" :money
			}
			$('#qkBtn').attr("disabled","disabled");
			$.ajax({
				url : "${base}/center/banktrans/draw/drawcommit.do",
				data:params,
				success : function(j) {
					//跳转提现记录
					if(j.success){
	 					$('#main_wrap').load(JIEBAO.base + '/cpSystem/cashFlowRecord.do');
					}
				},
				complete:function(){
					$('#qkBtn').removeAttr("disabled","disabled");
				}
			});
			
		 });
		 
	
	});
	
	//从右边截取i位有效字符
	String.prototype.Right = function(i) { //为String对象增加一个Right方法
		return this.slice(this.length - i, this.length); //返回值为 以“该字符串长度减i”为起始 到 该字符串末尾 的截取字符串
	};
	
	
		//数字金额实时转换成大写金额
	 	function convertCurrency(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    }
</script>
<style>
.dmlStyle{
	font-size:15px;
	color:#ffb400;
	margin-right:10px;
}
</style></c:if>