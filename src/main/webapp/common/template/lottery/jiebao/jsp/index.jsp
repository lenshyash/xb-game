<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>${website_name }</title>
    <link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="${base}/common/js/layer/skin/layer.css" id="layui_layer_skinlayercss">
    <link href="${base}/common/template/lottery/jiebao/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="${base}/common/template/lottery/jiebao/css/lottery.min.css?v=${caipiao_version}" rel="stylesheet" type="text/css">
    <link href="${base}/common/template/lottery/jiebao/css/lhc.css?v=${caipiao_version}" rel="stylesheet" type="text/css">
	<!-- 银行卡图标样式 -->
    <link rel="stylesheet" href="${base}/common/css/core/bank/css/bank.css?v=1.3">
    <!--[if lte IE 8]>
    <link href="${base}/common/template/lottery/jiebao/css/ie_hack.css?v=${caipiao_version}" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script>
    	var JIEBAO={
    		base : '${base}',
    		folder:"${stationFolder}",
    		caipiao_version : '${caipiao_version}',
    		lotCode : '${lotCode}',
    		lotType:'${curLot.type}',
    		_theme : '${_theme}',
    		_themeColor : '${_themeColor}',
    		_themeHighlight : '${_themeHighlight}',
    		_themeHighlightColor : '${_themeHighlightColor}',
    		lengReYiLou:${lengReYiLou},
    		fanShui:${fanShui},
    		maxBetMoney:${maxBetMoney},
    		minBetMoney:${minBetMoney},
    		unCancleOrder:${unCancleOrder},
    		unChaseOrder:${unChaseOrder},
    		currentLunarYear:${currentLunarYear}
    	};
    </script>
    <noscript>
        <div class="noScript">
            <div>
                <h2>请开启浏览器的Javascript功能</h2>
                <p>亲，没它我们玩不转啊！求您了，开启Javascript吧！
                    <br/> 不知道怎么开启Javascript？那就请
                    <a href="http://www.baidu.com/s?wd=%E5%A6%82%E4%BD%95%E6%89%93%E5%BC%80Javascript%E5%8A%9F%E8%83%BD" rel="nofollow" target="_blank">猛击这里</a>！
                </p>
            </div>
        </div>
    </noscript>
</head>

<body>
<div class="noScript" id="not-allow-old-ie" style="display:none">
     <div>
         <h2>不支援的浏览器</h2>
         <p>
             亲，我们不支援 IE 瀏覽器 10 以下的版本！<br>
             您的浏览器为 IE<span class="ie-version"></span> 内核浏览器。
         </p>
         <br>
         瀏覽器資訊:
         <br>
         <p id="ie-agent"></p>
         <br>
         <a href="https://www.baidu.com/s?wd=360%E6%B5%8F%E8%A7%88%E5%99%A8" rel="nofollow" target="_blank">请使用360浏览器，请使用极速模式</a>
     </div>
 </div>
<!-- menu -->
<div class="hideSide" id="slider_menu">
    <div class="slider_con" id="lotsaleMenu"><c:set var="ddd" value="0"/>
    	<c:forEach items="${lotteryGroupSort }" var="ii">
    	<c:if test="${lotMap.containsKey(ii) }">
    		<h2><c:choose><c:when test="${ii == 1}">时时彩</c:when><c:when test="${ii == 2}">低频彩</c:when>
    		<c:when test="${ii == 3}">快开</c:when>
    		<c:when test="${ii == 5}">11选5</c:when><c:when test="${ii == 7}">快三</c:when>
    		<c:when test="${ii == 6}">香港彩</c:when></c:choose></h2>
			<ul class="game_list">
    		<c:forEach var="gl" items="${lotMap.get(ii)}">
    		<li ${lotCode == gl.code ? 'class="current"': ''}><a href="javascript:;" lotcode="${gl.code}" lottype="${gl.type}">${gl.name}</a></li>
    		</c:forEach></ul>
    		<c:set var="ddd" value="${ddd+1}"/>
    	</c:if>
    	<c:if test="${ddd==1 && not empty showThirdMenu && showThirdMenu=='on'}">
    	<div style="margin:20px 0 0 32px;"><a href="${base}/liveTest.do?v=3" target="_blank" style="font-size:16px;color:#fff;">真人视讯</a></div>
    	<div style="margin:20px 0 0 32px;"><a href="${base}/egame.do" target="_blank" style="font-size:16px;color:#fff;">电子游艺</a></div>
    	<div style="margin:20px 0 0 32px;"><a href="${base}/center/banktrans/mnychg/page.do" target="_blank" style="font-size:16px;color:#fff;">真人额度转换</a></div>
    	</c:if>
    	</c:forEach>
    </div>
</div>
<!-- Color menu -->
<div style="width: 0px;" id="slider_thememenu">
    <div style="width:210px;text-align:center">
        <h3>请选择配色</h3>
        <ul id="themeContent">
            <!-- 預設顏色 -->
            <li>
                <div class="color1" style="background: rgb(102, 101, 78) none repeat scroll 0% 0%; border-color: rgb(127, 126, 103);"></div>
                <div class="color2" style="background: rgb(171, 106, 29) none repeat scroll 0% 0%; border-color: rgb(196, 131, 54);"></div>
                <div class="color3" style="background: rgb(254, 255, 168) none repeat scroll 0% 0%; border-color: rgb(255, 255, 193);" txtcolor="#A3A39B"></div>
                <p>01</p>
            </li>
            <!-- 九號 -->
            <li>
                <div class="color1" style="background: rgb(60, 60, 60) none repeat scroll 0% 0%; border-color: rgb(85, 85, 85);"></div>
                <div class="color2" style="background: rgb(250, 150, 30) none repeat scroll 0% 0%; border-color: rgb(255, 175, 55);"></div>
                <div class="color3" style="background: rgb(250, 250, 0) none repeat scroll 0% 0%; border-color: rgb(255, 255, 25);" txtcolor="#202020"></div>
                <p>02</p>
            </li>
            <li>
                <div class="color1" style="background: rgb(88, 89, 91) none repeat scroll 0% 0%; border-color: rgb(113, 114, 116);"></div>
                <div class="color2" style="background: rgb(90, 201, 244) none repeat scroll 0% 0%; border-color: rgb(115, 226, 255);"></div>
                <div class="color3" style="background: rgb(48, 149, 216) none repeat scroll 0% 0%; border-color: rgb(73, 174, 241);" txtcolor="#FFFFFF"></div>
                <p>03</p>
            </li>
            <li>
                <div class="color1" style="background: rgb(197, 196, 160) none repeat scroll 0% 0%; border-color: rgb(222, 221, 185);"></div>
                <div class="color2" style="background: rgb(119, 80, 18) none repeat scroll 0% 0%; border-color: rgb(144, 105, 43);"></div>
                <div class="color3" style="background: rgb(238, 128, 40) none repeat scroll 0% 0%; border-color: rgb(255, 153, 65);" txtcolor="#FFFFFF"></div>
                <p>04</p>
            </li>
            <!-- 各廳顏色 -->
            <li>
                <!-- 星彩 -->
                <div class="color1" style="background: rgb(41, 46, 30) none repeat scroll 0% 0%; border-color: rgb(66, 71, 55);"></div>
                <div class="color2" style="background: rgb(150, 73, 203) none repeat scroll 0% 0%; border-color: rgb(175, 98, 228);"></div>
                <div class="color3" style="background: rgb(175, 187, 242) none repeat scroll 0% 0%; border-color: rgb(200, 212, 255);" txtcolor="#202020"></div>
                <p>05</p>
            </li>
            <li>
                <!-- 金巴黎 -->
                <div class="color1" style="background: rgb(234, 218, 162) none repeat scroll 0% 0%; border-color: rgb(255, 243, 187);"></div>
                <div class="color2" style="background: rgb(206, 108, 71) none repeat scroll 0% 0%; border-color: rgb(231, 133, 96);"></div>
                <div class="color3" style="background: rgb(255, 208, 70) none repeat scroll 0% 0%; border-color: rgb(255, 233, 95);" txtcolor="#202020"></div>
                <p>06</p>
            </li>
            <li>
                <!-- 創世紀 -->
                <div class="color1" style="background: rgb(43, 43, 43) none repeat scroll 0% 0%; border-color: rgb(68, 68, 68);"></div>
                <div class="color2" style="background: rgb(255, 20, 24) none repeat scroll 0% 0%; border-color: rgb(255, 45, 49);"></div>
                <div class="color3" style="background: rgb(243, 255, 20) none repeat scroll 0% 0%; border-color: rgb(255, 255, 45);" txtcolor="#202020"></div>
                <p>07</p>
            </li>
            <li>
                <!-- w彩票 -->
                <div class="color1" style="background: rgb(39, 39, 39) none repeat scroll 0% 0%; border-color: rgb(64, 64, 64);"></div>
                <div class="color2" style="background: rgb(126, 61, 118) none repeat scroll 0% 0%; border-color: rgb(151, 86, 143);"></div>
                <div class="color3" style="background: rgb(255, 255, 0) none repeat scroll 0% 0%; border-color: rgb(255, 255, 25);" txtcolor="#202020"></div>
                <p>08</p>
            </li>
            <li>
                <!-- 雲頂 -->
                <div class="color1" style="background: rgb(130, 0, 2) none repeat scroll 0% 0%; border-color: rgb(155, 25, 27);"></div>
                <div class="color2" style="background: rgb(250, 150, 30) none repeat scroll 0% 0%; border-color: rgb(255, 175, 55);"></div>
                <div class="color3" style="background: rgb(250, 250, 0) none repeat scroll 0% 0%; border-color: rgb(255, 255, 25);" txtcolor="#202020"></div>
                <p>09</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(128, 209, 153) none repeat scroll 0% 0%; border-color: rgb(153, 234, 178);"></div>
                <div class="color2" style="background: rgb(35, 51, 41) none repeat scroll 0% 0%; border-color: rgb(60, 76, 66);"></div>
                <div class="color3" style="background: rgb(104, 254, 242) none repeat scroll 0% 0%; border-color: rgb(129, 255, 255);" txtcolor="#202020"></div>
                <p>10</p>
            </li>
            <li>
                <!-- 皇冠 -->
                <div class="color1" style="background: rgb(197, 196, 160) none repeat scroll 0% 0%; border-color: rgb(222, 221, 185);"></div>
                <div class="color2" style="background: rgb(119, 80, 18) none repeat scroll 0% 0%; border-color: rgb(144, 105, 43);"></div>
                <div class="color3" style="background: rgb(238, 128, 40) none repeat scroll 0% 0%; border-color: rgb(255, 153, 65);" txtcolor="#202020"></div>
                <p>11</p>
            </li>
            <li>
                <!-- 彩票在線 -->
                <div class="color1" style="background: rgb(171, 142, 7) none repeat scroll 0% 0%; border-color: rgb(255, 255, 136);"></div>
                <div class="color2" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; border-color: rgb(25, 25, 25);"></div>
                <div class="color3" style="background: rgb(255, 255, 111) none repeat scroll 0% 0%; border-color: rgb(255, 255, 136);" txtcolor="#202020"></div>
                <p>12</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(63, 124, 171) none repeat scroll 0% 0%; border-color: rgb(88, 149, 196);"></div>
                <div class="color2" style="background: rgb(149, 176, 187) none repeat scroll 0% 0%; border-color: rgb(174, 201, 212);"></div>
                <div class="color3" style="background: rgb(189, 210, 239) none repeat scroll 0% 0%; border-color: rgb(214, 235, 255);" txtcolor="#202020"></div>
                <p>13</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(90, 177, 187) none repeat scroll 0% 0%; border-color: rgb(115, 202, 212);"></div>
                <div class="color2" style="background: rgb(30, 21, 42) none repeat scroll 0% 0%; border-color: rgb(55, 46, 67);"></div>
                <div class="color3" style="background: rgb(255, 234, 99) none repeat scroll 0% 0%; border-color: rgb(255, 255, 124);" txtcolor="#202020"></div>
                <p>14</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(170, 90, 125) none repeat scroll 0% 0%; border-color: rgb(195, 115, 150);"></div>
                <div class="color2" style="background: rgb(118, 0, 50) none repeat scroll 0% 0%; border-color: rgb(143, 25, 75);"></div>
                <div class="color3" style="background: rgb(234, 245, 106) none repeat scroll 0% 0%; border-color: rgb(255, 255, 131);" txtcolor="#202020"></div>
                <p>15</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(255, 136, 181) none repeat scroll 0% 0%; border-color: rgb(255, 161, 206);"></div>
                <div class="color2" style="background: rgb(106, 61, 55) none repeat scroll 0% 0%; border-color: rgb(131, 86, 80);"></div>
                <div class="color3" style="background: rgb(57, 148, 221) none repeat scroll 0% 0%; border-color: rgb(82, 173, 246);" txtcolor="#202020"></div>
                <p>16</p>
            </li>
            <li>
                <!-- 未定 -->
                <div class="color1" style="background: rgb(255, 185, 143) none repeat scroll 0% 0%; border-color: rgb(255, 210, 168);"></div>
                <div class="color2" style="background: rgb(229, 89, 53) none repeat scroll 0% 0%; border-color: rgb(254, 114, 78);"></div>
                <div class="color3" style="background: rgb(254, 0, 0) none repeat scroll 0% 0%; border-color: rgb(255, 25, 25);" txtcolor="#202020"></div>
                <p>17</p>
            </li>
            <li>
                <!-- 港龍 -->
                <div class="color1" style="background: rgb(248, 237, 219) none repeat scroll 0% 0%; border-color: rgb(255, 255, 244);"></div>
                <div class="color2" style="background: rgb(233, 86, 104) none repeat scroll 0% 0%; border-color: rgb(255, 111, 129);"></div>
                <div class="color3" style="background: rgb(72, 68, 65) none repeat scroll 0% 0%; border-color: rgb(97, 93, 90);" txtcolor="#202020"></div>
                <p>18</p>
            </li>
        </ul>
    </div>
</div>
<!--  header -->
<div class="container bg_gray7">
    <div id="header" class="wrapper">
        <h1 id="logo"<c:if test="${duLiCaiPiaoOpened }">style="left:35px"</c:if>><a href="${base }"><img <c:choose><c:when test="${not empty lotteryPageLogoUrl }">src="${lotteryPageLogoUrl }"</c:when><c:otherwise>src="${base}/common/template/lottery/jiebao/images/default_logo.png"</c:otherwise></c:choose> height="80" <c:if test="${duLiCaiPiaoOpened }">width="200"</c:if><c:if test="${!duLiCaiPiaoOpened }">width="170"</c:if>></a></h1>
        <div id="cat_nav"<c:if test="${empty duLiCaiPiaoOpened || !duLiCaiPiaoOpened }">style="border-left:#b3b3b3 1px solid"</c:if>>
       <ul>
          <c:if test="${empty duLiCaiPiaoOpened || !duLiCaiPiaoOpened }">
          <li><a href="${base }/lottery/index.do?lotCode=${lotCode}" class="current caipiaoTitle" pli="lotty">彩 票<span class="line"></span></a></li>
             <c:if test="${zhenRenYuLeOpened }"><li><a href="${base }/index/real.do" target="_blank" pli="live" >真人视讯<span class="line"></span></a></li></c:if>
             <c:if test="${dianZiYouYiOpened }"><li><a href="${base }/index/egame.do" target="_blank" pli="slot">电子游艺<span class="line"></span></a></li></c:if>
            <c:if test="${isTyOnOff=='on' && isGuest != true}"><li><a href="${base }/index/sport.do" target="_blank" pli="slot">体育赛事<span class="line"></span></a></li></c:if>
          </c:if>
       </ul>
			<script>
				var  url = window.location.host;
				if (url == "1zn23.com" || url == "wn5f5.com" || url == "wg45h.com" || url == "x2m7l.com" || url == "1cs3q.com" || url == "m62qa.com" || url == "0vg6g.com" || url == "2snv7.com" || url == "r7vzb.com" || url == "www.1zn23.com" || url == "www.wn5f5.com" || url == "www.wg45h.com" || url == "www.x2m7l.com" || url == "www.1cs3q.com" || url == "www.m62qa.com" || url == "www.0vg6g.com" || url == "www.2snv7.com" || url == "www.r7vzb.com" ){
					var oAs = document.getElementsByClassName("caipiaoTitle")[0];
					oAs.style.display="none"
				}
			</script>
        </div>
        <div id="nav">
            <ul>
                <li id="b1"><i></i><a <c:choose><c:when test="${empty duLiCaiPiaoOpened || !duLiCaiPiaoOpened}">href="${base}/"</c:when><c:otherwise>href="${base}/lottery/index.do"</c:otherwise></c:choose> class="btn">回首页</a></li>
                <li id="b2"><i></i><a href="javascript: void(0);" _layer="save" class="btn">在线存款</a></li>
                <li id="b3"><i></i><a href="javascript: void(0);" _layer="cash" class="btn">在线取款</a></li>
                <li id="b4"><i></i><a href="javascript: void(0);" _layer="info" class="btn">账户中心</a></li>
                <li id="b5"><i></i><a href="javascript:;" class="btn playRule">玩法介绍</a></li>
                <li id="b6"><i></i><a <c:choose><c:when test="${not empty customerServiceUrlLink }">href="${customerServiceUrlLink}"</c:when><c:otherwise>href="javascript: void(0);"</c:otherwise></c:choose> target="_blank" class="btn">客 服</a></li>
            </ul>
        </div>
        <div id="acc_info">
            <p>
                <span class="acc_id"><i></i><label>${user.account }</label></span> /
                <span class="money">  
                <c:if test="${not empty cashName}">
                	${cashName}
                </c:if>
                <c:if test="${empty cashName}">
                  	帐户金额 
                </c:if>
                <i id="balance">0.00</i></span> /
                <c:if test="${isScore}">
                <span>积分 <a href="javascript:;" id="scoreChange">${userInfo.score}</a></span> /
                </c:if>
<!--                 <span><i id="unreadSent"></i></span> / -->
                <span><i id="unreadSent" title="站内信息" class="fa fa-envelope-o"></i>(<span id="zhanneixin">0</span>)</span> /
                <a href="javascript:;" id="publicInfo">系统公告</a>
                <a href="${base }/logout.do" class="btn_logout btn b2">安全退出</a>
                <a href="javascript:;" class="btn_logout btn b2 hides" id="theme">更换皮肤</a>
            </p>
        </div>
    </div>
</div>
<!-- 系统公告 -->

<div class="main_content">
	<div class="row bg_black5" style="margin: 7px auto 0 auto;padding:5px 0px;overflow: auto;zoom: 1;">
		<div class="fa-stack c_white3 fa-customColor f_left" style="margin-left:10px;">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-bullhorn fa-stack-1x fa-inverse"></i>
		</div><div class="f_left"style="margin-top: 6px;">&nbsp;最新消息：</div>
		<marquee id="xitonggonggao_content" scrollamount="3" scrolldelay="150" direction="left" onmouseover="this.stop();" onmouseout="this.start();" style="cursor: pointer; width:800px;height:18px;margin:6px 0 0 14px;float:left;">
	        尊敬的会员：欢迎光临！
	    </marquee>
	</div>
	<div id="main_wrap">
		<!-- bettop_menu -->
		<div class="row r1 bg_black5" style="margin: 7px auto 0 auto;" id="bettop_menu">
			<c:choose>
				<c:when test="${lotSize eq 1 }">
				    <div class="game_title" style="margin-left:15px;">
				        <a href="javascript:;" class="btn b0 current" id="nowGP" style="font-size: 16px; width: 100px;">
				        	<i class="fa fa-bars"></i>&nbsp;
				        	<c:choose>
								<c:when test="${curLot.code eq 'BJSC'}">
									<b class="btn gp_name" lotcode="${curLot.code}" lottype="${curLot.type}">赛&nbsp;&nbsp;车</b>
								</c:when>
								<c:otherwise>
									<b class="btn gp_name" lotcode="${curLot.code}" lottype="${curLot.type}">${curLot.name}</b>
								</c:otherwise>
							</c:choose>
				        </a>
				    </div>
				</c:when>
				<c:otherwise>
					<div class="game_select"><a href="javascript:;" class="btn b1">&nbsp;<i class="fa fa-bars"></i> 选 择 彩 种</a></div>
				    <div class="game_title">
				        <i class="fa fa-caret-left fa-lg"></i>
				        <a href="javascript:;" class="btn b0 current" id="nowGP">
				            <b class="btn gp_name" lotcode="${curLot.code}" lottype="${curLot.type}">${curLot.name }</b>
				        </a>
				    </div>
				</c:otherwise>
			</c:choose>
		    <ul id="lotteryBtns">
		        <li><a href="javascript:;" id="order_history" class="btn b0">订单管理</a></li>
		        <li><a href="javascript:;" id="sbet_paper" class="btn b0">投注报表</a></li>
		        <li><a href="javascript:;" id="result" class="btn b0">开奖结果</a></li>
		        <li id="closeBonus" style="<c:if test="${lotCode eq 'LHC' || lotCode eq 'SFLHC' || lotCode eq 'TMLHC' || lotCode eq 'WFLHC' || lotCode eq 'HKMHLHC' || lotCode eq 'AMLHC'}">display:none;</c:if>"><a href="javascript:;" id="bonus" class="btn b0">奖金对照</a></li>
		        <c:if test="${not empty betZst}">
		        <li><a href="${betZst}" target="_blank" class="btn b0">走势图</a></li>
		        </c:if>
		    </ul>
		</div>
		<div class="container" id="betContainer">
			<div class="wrapper main hideMain">
			    <div class="row r2">
			        <div class="r2_c1 bg_black3" id="lotClock">
			            <div class="clock_ico" style="background-image: none;"><a href="javascript:;">
			            	<span id="soundNotification" class="fa-stack fs12" data-soundon="true">
				            	<i class="fa fa-volume-up fa-stack-1x"></i>
				            	<i class="fa fa-circle-thin fa-stack-2x"></i>
			            	</span></a></div>
			            <div class="divClock">
			                <div class="top"></div>
			                <div class="middle"></div>
			                <div class="circle">
			                    <div class="center">
			                        <div class="hour" style="transform: rotate(0.41665deg);">
			                            <div></div>
			                        </div>
			                        <div class="min" style="transform: rotate(4.9998deg);">
			                            <div></div>
			                        </div>
			                        <div class="sec" style="transform: rotate(299.988deg);">
			                            <div></div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="clock">
			            	<span class="clock_no">00:00:00</span>
			            	<span class="gp_name" style="float:left;" id="GameTypename">${curLot.name }</span>
			            	<span class="status">  开放下注  </span>
			            	<div class="clear" id="nowroundsShow">第 ? 期</div></div>
			        </div>
			        <div class="r2_c2 bg_black3" id="larbar_data" style="position: relative;">
			            <div class="r2_c2_rollout" style=" position: absolute; height: 100%;width: 100%;">
			                <div id="last_result_no" class="open_result_no <c:choose><c:when test="${lotCode!='PCEGG'}">balls_${curLot.balls }</c:when><c:otherwise>balls_7</c:otherwise></c:choose>">
			                <c:choose><c:when test="${lotCode=='PCEGG'}">
			                	<span class="bg_black3_content">?</span><span class="bg_black3_content">+</span><span class="bg_black3_content">?</span><span class="bg_black3_content">+</span><span class="bg_black3_content">?</span><span class="bg_black3_content">=</span><span class="bg_black3_content">?</span>
			               	</c:when><c:otherwise><c:forEach var="i" begin="1" end="${curLot.balls }"><span class="bg_black3_content">?</span></c:forEach></c:otherwise></c:choose>
				            </div>
				            <div class="open_result_txt"><span class="gp_name">${curLot.name }</span><span id="last_result_qihao">第 ? 期</span> 开奖结果</div>
			            </div>
			        </div>
			        <div class="r2_c3 bg_black3">
			        	<h3>近期开奖号码</h3>
			        	<div class="his"id="lotHistory"><ul></ul></div></div>
			    </div>
			    
			    <div class="row r3 bg_black3">
			        <div class="row_top bg_black3" id="betmenu_data">
			        	<ul id="Bm_general"></ul></div>
			        <div class="row_con" id="betplay_data"><div style="text-align:center;line-height: 264px;font-size: 100px;">加载中...</div></div>
			        <!-- ttc导航 -->
			        <div class="row_btm bg_black2" id="notXgc">
			            <div class="no_ranger" style="width:30px;"><span class="fa-stack fa-lg c_white3 fa-customColor" style="margin: 7px 0 0 0px;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span>
			            </div>
			            <div class="no_value" style="width: 220px;">
			                <div>
			                    <label>奖金<c:if test="${fanShui }">返点</c:if>：</label><span><i id="gcrate">0</i>元 <c:if test="${fanShui }">/ <span id="rebateShow">0%</span></c:if></span>
			                </div>
			            </div>
			            <div class="no_amount" style="width: 200px;">
			                <span class="fa-stack fa-lg c_white3 fa-customColor" style="margin: 5px 0 0 0px; float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-edit fa-stack-1x fa-inverse" style="margin: 7px 0px 0px 1px;"></i></span>&nbsp;<span>共选择&nbsp;<span id="Immediate">0</span>&nbsp;注
			                <br>共投注&nbsp;<span id="allTotamounts">0</span>&nbsp;元</span>
			            </div>
			            <div class="no_input" style="width: 260px;">
			                倍数：
			                <input name="buymultiple" type="text" value="1" id="buymultiple" maxlength="8">
			                <select name="selectdollarunit" id="selectdollarunit">
			                	<option value="1">元模式</option>
			                	<c:choose>
			                		<c:when test="${lotteryBetModel == '111' }">
			                			<option value="10">角模式</option>
			                			<option value="100">分模式</option>
			                		</c:when>
			                		<c:when test="${lotteryBetModel == '110' }">
			                			<option value="10">角模式</option>
			                		</c:when>
			                	</c:choose>
			                </select>
			            </div>
			            <div class="no_b2" style="width: 90px;"><a href="javascript:;" class="btn2" id="seleall"><span class="fa-stack fa-lg c_white3 fa-customColor"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-plus fa-stack-1x fa-inverse"></i></span> 添 加 </a></div>
			            <div class="no_b1"><a href="javascript:;" class="btn2" id="randomPlay"><span class="fa-stack fa-lg c_white3 fa-customColor"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-random fa-stack-1x fa-inverse"></i></span> 机 选 </a></div>
			        </div>
			        <!-- xgc导航 -->
			        <div class="row_btm bg_black2" id="okXgc" style="display:none;">
			        	<div id="checkedDiv" style="width:250px;" style="display:block;">
			        	 <span class="fa-stack fa-lg c_white3 fa-customColor" style="margin: 7px 0 0 0px;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span>
			            <div class="no_value" style="width: 180px;float:right;">
			                <div>
			                    <label>投注金额：<input type="text" name="checked_money" style="width:100px;"/></span>
			                </div>
			            </div>
			            </div>
			            <div class="no_amount" style="width: 250px;">
			                <span class="fa-stack fa-lg c_white3 fa-customColor" style="margin: 5px 0 0 0px; float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-edit fa-stack-1x fa-inverse" style="margin: 7px 0px 0px 1px;"></i></span>&nbsp;<span>共选择&nbsp;<span id="betAllZhuS">0</span>&nbsp;注
			                	共投注&nbsp;<span id="betAllTotals">0</span>&nbsp;元</span>
			            </div>
			            <div class="no_b2" style="width: 90px;"><a href="javascript:;" class="btn2" id="xgcSeleAll"><span class="fa-stack fa-lg c_white3 fa-customColor"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-plus fa-stack-1x fa-inverse"></i></span> 添 加 </a></div>
			            <div class="no_b1"><a href="javascript:;" class="btn2" id="reSetLottery"><span class="fa-stack fa-lg c_white3 fa-customColor"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-random fa-stack-1x fa-inverse"></i></span> 重 置 </a></div>
			        </div>
			    </div>
			    
			    <div class="row r4">
			        <div class="r4_c1 bg_black3">
			            <div class="row_top bg_black3">
			                <table class="780" border="0" cellspacing="0" cellpadding="0">
			                    <colgroup><col width="105"><col width="95"><col width="200"><col width="80"><col width="170"><col width="65"><col width="65"></colgroup><thead>
			                    <tr>
			                        <td>投注项</td>
			                        <td>下注类型</td>
			                        <td>号 码</td>
			                        <td>总注数</td>
			                        <c:if test="${lotCode eq 'LHC' || lotCode eq 'SFLHC' || lotCode eq 'TMLHC' || lotCode eq 'WFLHC' || lotCode eq 'HKMHLHC' || lotCode eq 'AMLHC'}"><td id="pvJJ">赔率</td></c:if>
			                        <c:if test="${lotCode ne 'LHC' && lotCode ne 'SFLHC' && lotCode ne 'TMLHC' && lotCode ne 'WFLHC' && lotCode ne 'HKMHLHC'&& lotCode ne 'AMLHC'}"><td id="pvJJ">奖金<c:if test="${fanShui }">/返点</c:if></td></c:if>
			                        <td>金 额</td>
			                        <td>删 除</td>
			                    </tr>
			                </thead>
			                </table>
			            </div>
			            <div class="row_con clearfix" id="BetNumberListContanior">
			                <table width="770" border="0" cellspacing="0" cellpadding="0">
			                    <colgroup><col width="105"><col width="95"><col width="200"><col width="80"><col width="170"><col width="65"><col width="65"></colgroup><tbody></tbody>
			                </table>
			            </div>
			            <div class="row_btm bg_black2">
			                <div class="btn_row">
			                    <ul>
			                        <c:if test="${!unChaseOrder}"><li class="hide" id="runbets_T"<c:if test="${lotCode!='FC3D' && lotCode!='PL3' && lotCode != 'LHC' && lotCode != 'SFLHC'}"> style="display: list-item;"</c:if>><a href="javascript:;" class="btn2" id="runbets"><span class="fa-stack c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i></span>&nbsp;我要追号</a></li></c:if>
			                        <li><a href="javascript:;" class="btn2" id="nowbetinfo"><span class="fa-stack c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-search fa-stack-1x fa-inverse"></i></span>&nbsp;下注情况</a></li>
			                        <li id="saiCheShiPin" swf="${saiCheSwfPath}" class="scsp<c:if test="${lotCode!='BJSC' || saiCheSwfStatus!='on'}"> hide</c:if>" status="${saiCheSwfStatus}"><a href="javascript:;" id="saiCheShiPinBtn" class="btn2"><span class="fa-stack c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-video-camera fa-stack-1x fa-inverse"></i></span>&nbsp;赛车视频</a></li>
			                        <li class="right"><a href="javascript:;" class="btn2" id="clearallitem"><span class="fa-stack c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span>&nbsp;删除全部</a></li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			        <div class="r4_c2 bg_black5">
			            <div class="vote"><a href="javascript:;" class="btn b3" id="gamebuy" jx-method="POST" jx-datatype="json" jx-template=""><span class="fa-stack c_white3 fa-customColor" style="font-size: 0.7em; left: -5px;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x fa-inverse"></i></span>确定投注</a></div>
			            <div class="arr_top"><i class="fa fa-caret-up fa-lg"></i></div>
			            <div class="vote_total">
			                <div><span class="fa-stack fa-lg c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ticket fa-stack-1x fa-inverse"></i></span><span class="total">总下注数：</span><span class="total"><span id="s_allTotbets">0</span></span>
			                </div>
			                <div><span class="fa-stack fa-lg c_white3 fa-customColor" style="float: left;"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span><span class="total">总下注金额：</span><span class="total"><span id="s_allTotamounts">0</span></span>
			                </div>
			            </div>
			        </div>
			    </div>
			    <c:if test="${!unChaseOrder}"><jsp:include page="zhuiHao.jsp"></jsp:include></c:if>
			    <jsp:include page="saiCheShiPin.jsp"></jsp:include>
			    <jsp:include page="xiaZhuQingKuang.jsp"></jsp:include>
			    <jsp:include page="markSix.jsp"></jsp:include>
			</div>
		</div>
		<div class="container hide" id="ocontainer"></div>
	</div>
</div>
</body>
</html>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/combine.min.js?v=${caipiao_version}a1"></script>
<jsp:include page="/common/modelCommon/cpFrame.jsp"></jsp:include>
<!-- 红包功能 -->
<c:if test="${isRedPacket eq 'on'}">
	<script src="${base}/common/js/redpacket/redbag.js?v=1.2.2" path="${base}"></script>
</c:if>