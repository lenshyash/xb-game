<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row bg_black3 order hide" id="betOrderWrap">
    <div class="row_top">
    	<input type="hidden" id="xzqkorder">
        <h3>※<span>订单查询</span> &nbsp; 彩种：<span id="betName">排列3</span>&nbsp;&nbsp;<span id="betListBetween">报表日期：2016-05-31 - 2016-05-31</span><span class="tips2 btn bg_black3"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心小提醒：点击注单号能查询详细投注明细</span></h3></div>
    <div class="row_con" style="padding-bottom: 5px;">
        <table width="944" border="0" cellspacing="0" cellpadding="0" class="cancel_order" id="betOrderTbl">
            <thead>
                <tr class="bg_white2">
                    <th>注单号</th>
                    <th>下注时间</th>
                    <th>期 号</th>
                    <th>玩 法</th>
                    <th>倍 数</th>
                    <th>开奖号码</th>
<!--                         <th>单注额</th> -->
                    <th>总 额</th>
                    <th>中奖奖金</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody id="betList"><tr><td colspan="10">没有符合查询的资料...</td></tr></tbody>
			<tfoot id="betOrderTblFoot">
                <tr>
                    <td colspan="6" class="t_right">小计</td>
                    <td id="betPageAmount">110.00</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="t_right">总计</td>
                    <td class="c_gray" id="betAmount">330.00</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr id="betOrderTblTabs">
                    <td colspan="10">
                        <ul class="pagination" id="xzPaginationBar" style="margin:0px; float:left;">
                        	<li class="disabled"><a link="1" href="javascript:void(0);" onclick="xzSelectPage(this)" ><span>第一页</span></a></a></li>
				            <li class="disabled"><a link="1" href="javascript:void(0);" onclick="xzSelectPage(this)" ><span>上一页</span></a></li>
				            <li class="disabled"><a link="2" href="javascript:void(0);" onclick="xzSelectPage(this)" ><span>下一页</span></a></li>
				            <li class="disabled"><a link="1" href="javascript:void(0);" onclick="xzSelectPage(this)" ><span>最终页</span></a></li>
				            <li class="disabled"><span class="xzDqPage" link="1">当前页次1</span></li>
							<li>
								<select class="xzOptions" onchange="xzSelect()">
									<option value="1">1</option>
								</select>
							</li>
                        </ul>
                        <ul class="pagination pull-right" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;">
                            <li class="disabled"><span id="xzPaginationInfo"></span></li>
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <script type="text/javascript">
	    function xzSelect(){
		    var page = $(".xzOptions").val();
		    $(".xzOptions").val(page);
		    getBcLotteryOrder(page);
	    }
	    
	    function xzSelectPage(el){
	    	var cl = $(el).parent().attr("class");
	    	if(cl == "disabled"){
	    		return false;
	    	}
		    var page = $(el).attr("link");
		    getBcLotteryOrder(page);
	    }
    </script>
</div>