<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 账户中心 -->
<jsp:include page="payHeadCommunal.jsp" />
<div class="wrapper main">
	<div class="wrapper main2 bg_black3">
		<div class="content_944 clear smallT_m">
			<div class="content_frame2 bg_black3 smallT">
				<h3 class="bg_white4">基本资料</h3>
				<form XX_XX="uc_myaccount_form">
					<input type="hidden" value="base_area" name="updatearea" />
					<table width="750" border="0" cellspacing="0" cellpadding="0"
						class="tables_h">
						<colgroup>
							<col width="20%">
							<col width="25%">
							<col width="30%">
							<col width="25%">
						</colgroup>
						<tbody>
							<tr>
								<th>帐号</th>
								<th>钱包余额</th>
								<th>昵称</th>
								<th>状态</th>
							</tr>
							<tr id="user_info_div">
								<td><span id="uc_account"></span></td>
								<td><span id="uc_balance"></span></td>
								<td><span id="uc_userName"></span></td>
								<td id="uc_status">正常</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

			<div class="col2 clearfix">
				<div class="content_frame3 bg_black3 f_left w465 smallT">
					<h3 class="bg_white4">修改登录密码</h3>
<!-- 					<form XX_XX="uc_myaccount_form"> -->
						<table width="400" border="0" cellspacing="0" cellpadding="0"
							class="tables">
							<colgroup>
								<col width="44%">
								<col width="56%">
							</colgroup>
							<tbody>
								<tr>
									<th>请输入旧登入密码</th>
									<td class="positionre"><input class="w225 required"
										id="oldPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>请输入新登入密码</th>
									<td class="positionre"><input class="w225 required"
										id="newPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>请确认新登入密码</th>
									<td class="positionre"><input class="w225 required"
										id="reNewPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>&nbsp;</th>
									<td>
										<div class="f_right">
											<button class="btn" id="passBtn" XX_XX="uc_myaccount_submit" type="button">提
												交</button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
<!-- 					</form> -->
				</div>
				<div class="content_frame3 bg_black3 f_right w465 smallT">
					<h3 class="bg_white4">修改取款密码</h3>
<!-- 					<form XX_XX="uc_myaccount_form"> -->
						<table width="400" border="0" cellspacing="0" cellpadding="0"
							class="tables">
							<colgroup>
								<col width="44%">
								<col width="56%">
							</colgroup>
							<tbody>
								<tr>
									<th>请输入旧取款密码</th>
									<td class="positionre"><input class="w225 required"
										id="oldBankPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>请输入新取款密码</th>
									<td class="positionre"><input class="w225 required"
										id="newBankPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>请确认新取款密码</th>
									<td class="positionre"><input class="w225 required"
										id="reNewBankPass" type="password" XX_XX="uc_value_for_clean">
									</td>
								</tr>
								<tr>
									<th>&nbsp;</th>
									<td>
										<div class="f_right">
											<button class="btn" XX_XX="uc_myaccount_submit" id="bankBtn" type="button">提
												交</button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
<!-- 					</form> -->
				</div>
			</div>

		</div>
	</div>
</div>
<script>
	$(function() {
		$('#curOver1').addClass("current");
			$.ajax({
			url : "${base}/center/banktrans/draw/drawdata.do",
			success : function(j) {
				$('#uc_account').html(j.member.account);
				$('#uc_balance').html(j.member.money);
				if(j.member.userName){
					$('#uc_userName').html('**'+(j.member.userName).Right(1));
				}
// 				$('#uc_status').html(j.member.status);
			}
		});
			
			//重置登陆密码
			$('#passBtn').click(function(){
				var oldPass = $('#oldPass').val();
				var newPass = $('#newPass').val();
				var reNewPass = $('#reNewPass').val();
				if(oldPass == null || oldPass ==""){
					layer.alert("旧密码不能为空");
					return false;
				}
				
				if(newPass == null || newPass ==""){
					layer.alert("新密码不能为空");
					return false;
				}
				
			    if(!newPass || !/^[a-zA-Z0-9_]{6,20}$/.test(newPass)){
	                layer.alert("请输入6至20位登录密码，由数字，字母和下划线组成！");
	                return false;
	            }
			    
			    if(newPass != reNewPass){
			    	layer.alert("两次密码不一致!");
					return false;
			    }
			    
        		var updType = 1;
        		$.ajax({
        			url : "${base}/center/member/meminfo/newpwd.do",
        			data : {
        				opwd : oldPass,
        				pwd : newPass,
        				rpwd : reNewPass,
        				updType :updType
        			},
        			success : function(result) {
        				if(result.success){
        					layer.alert("修改成功！");
            				$('#main_wrap').load(JIEBAO.base + '/cpSystem/accountCenter.do');
        				}
        			}
        		});
			});
			
			//重置提款密码
			$('#bankBtn').click(function(){
				var oldBankPass = $('#oldBankPass').val();
				var newBankPass = $('#newBankPass').val();
				var reNewBankPass = $('#reNewBankPass').val();
				var updType = 2;
				if(oldBankPass == null || oldBankPass ==""){
					layer.alert("旧密码不能为空");
					return false;
				}
				
				if(newBankPass == null || newBankPass ==""){
					layer.alert("新密码不能为空");
					return false;
				}
				
// 			    if(!newBankPass || !/^[a-zA-Z0-9_]{6,20}$/.test(newBankPass)){
// 	                layer.alert("请输入6至20位提款密码，由数字，字母和下划线组成！");
// 	                return false;
// 	            }
			    
			    if(newBankPass != reNewBankPass){
			    	layer.alert("两次密码不一致!");
					return false;
			    }
        		$.ajax({
        			url : "${base}/center/member/meminfo/newpwd.do",
        			data : {
        				opwd : oldBankPass,
        				pwd : newBankPass,
        				rpwd : reNewBankPass,
        				updType : updType
        			},
        			success : function(result) {
        				console.log(result);
        				if(result.success){
        					layer.alert("修改成功！");
            				$('#main_wrap').load(JIEBAO.base + '/cpSystem/accountCenter.do');
        				}
        			}
        		});
			});
	});
</script>
