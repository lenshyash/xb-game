<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 积分兑换 -->
<jsp:include page="payHeadCommunal.jsp" />
<script>
var base = "${base}";
</script>
<div class="wrapper main2 bg_black3" XX_XX="uc_user_area">
            <div class="content_944 clear">
                <div class="content_frame" id="content_frame">
                
                
      <div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="tit"><h3 class="bg_white4">用户余额</h3></div>
			<table class="tab tables_h" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th width="304">帐户</th>
						<th width="426">余额</th>
						<th width="150">操作</th>
					</tr>
					<tr>
						<td>现金</td>
						<td><span id="mnyCredit">0.00</span>&nbsp;&nbsp;RMB</td>
						<td rowspan="2">
<!-- 						<input id="loadCredit" value="刷新" onclick='initUserData()' style="margin-left: 15px;" type="button"> -->
						<button class="btn" id="loadCredit" onclick='initUserData()' type="button"><i class="fa fa-refresh"></i> 刷新</button>
						</td>
					</tr>
					<tr id="mmgid">
						<td>积分</td>
						<td><span id="scoreCredit">0.00</span>&nbsp;&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<div class="tit mt10">
					<h3 class="bg_white4">兑换</h3>
			</div>
			<ul class="tabLi mt10 tables_h">
				<li class="tables_h">兑换类型： <select id="exchangeType">
						<option selected="selected" value="">----请选择类型----</option>
						<option value="1">现金兑换积分</option>
						<option value="2">积分兑换现金</option>
				</select> <span id="exchange_remark_span"></span>
				</li>
				<li class="tables_h">兑换额度： <input id="amount" name="amount" class="inp" type="text"> 
<!-- 				<input value="确定" onclick="exchange();" id="excBtn" class="btn2" type="button">  -->
				<button class="btnStyle" id="excBtn" onclick='exchange()' type="button">确定</button>
				<span id="exchange_result_span"></span>
				</li>
				<li><span style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></li>
				<li><span style="font-size: 14px; font-weight: bold; color: #c00">积分可用于参与站点活动</span></li>
			</ul>
		</div>
		<div class="main_footer"></div>
	</div>
	
                </div>
            </div>
        </div>
<script>
	$(function() {
		$('#curOver8').addClass("current");
	});
</script>
<script type="text/javascript" src="${base }/common/template/member/center/js/memexchangedemo.js"></script>
<style>
.btnStyle{
	font-size: 13px;
    display: inline-block;
    height: 30px;
    line-height: 28px;
    color: #fff;
    background-color: transparent;
    text-align: center;
    padding: 0 25px;
    margin-left: 5px;
    border: 1px #fff solid;
    text-align: center;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    behavior: url("./public/css/PIE.htc");
    cursor: pointer;
}
</style>