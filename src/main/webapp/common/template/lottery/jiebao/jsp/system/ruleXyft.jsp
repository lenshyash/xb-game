<%@ page language="java" pageEncoding="UTF-8"%>
<div>
<p class="p1">
    简介：
</p>
<p class="p1">
    幸运飞艇是马耳他共和国瓦莱塔福利联合委员会独家发行的一款高频彩，其源于F1赛艇的彩票游戏，游戏由十架飞艇作为开奖号码，五分钟为一个开奖周期。
</p>
<p class="p2">
    <br/>
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    定位胆
</p>
<p class="p1">
    从第一名到第十名任意位置上选择1个或1个以上号码。&nbsp;
</p>
<p class="p1">
    从第一名到第十名任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。
</p>
<p class="p1">
    投注方案： 第一名01
</p>
<p class="p1">
    开奖号码:第一名01，即中定位胆第一名。
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    前三复式
</p>
<p class="p1">
    从第一名、第二名、第三名中各选择1个号码组成一注。&nbsp;
</p>
<p class="p1">
    选号与开奖号码按位猜对3位即中奖。
</p>
<p class="p1">
    &nbsp;投注方案： 01 02 03
</p>
<p class="p1">
    开奖号码:01 02 03 04 05 06 07 08 09 10
</p>
<p class="p1">
    即中前三复式
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    前二复式
</p>
<p class="p1">
    从第一名、第二名中至少选择1个号码组成一注。&nbsp;
</p>
<p class="p1">
    选号与开奖号码按位猜对2位即中奖。
</p>
<p class="p1">
    &nbsp;投注方案：01 02
</p>
<p class="p1">
    开奖号码:01 02 03 04 05 06 07 08 09 10
</p>
<p class="p1">
    即中前二复式。
</p>
<p class="p2">
    <br/>
</p>
<p class="p1">
    前一复式
</p>
<p class="p1">
    从第一名中至少选择1个号码组成一注。
</p>
<p class="p1">
    选号与开奖号码按位猜对1位即中奖。
</p>
<p class="p1">
    投注方案：01
</p>
<p class="p1">
    开奖号码:01 02 03 04 05 06 07 08 09 10
</p>
<p class="p1">
    即中前一直选。
</p>
<p>
    <br/>
</p>
<strong id="dxds">
	<span>冠亚和值--大小单双</span>
</strong>
<p>
	<span>和值的数字为大小单双</span>
</p>
<p>
	<span>和值数字3-10为小，12-19为大，奇数为单，偶数为双 ${dx11Setting } ${ds11Setting }</span>
</p>
<p>
	<span>所选大小单双与开奖和值相符，即为中奖</span>
</p>
<p>
	<br />
</p>
<strong id="gyhz">
	<span>冠亚和值--和值</span>
</strong>
<p>
	<span>从3-19中任意选择1个或1个以上号码。</span>
</p>
<p>
	<span>所选数值等于开奖号码前两位相加之和，即为中奖。</span>
</p>
<p>
	<span>投注方案：和值15开奖号码前两位：0807或0906或1005，即中冠亚和值。</span>
</p>
<p>
	<span><br /></span>
</p>
</div>