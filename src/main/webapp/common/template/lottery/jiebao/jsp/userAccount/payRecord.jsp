<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/jiebao/js/datePicker/jquery.ui.datepicker-zh-CN.js"></script>
<link href="${base}/common/template/lottery/jiebao/js/datePicker/jquery-ui.css" rel="stylesheet">
<!-- 充值记录-->
<jsp:include page="payHeadCommunal.jsp" />
<div class="wrapper main2 bg_black3" id="uc_user_area">
            <div class="content_944 clear">
            	<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能在线充值</div></c:if>
                <c:if test="${userInfo.accountType!=6 }">
                <form id="uc_cashflowrecord_form">
                    <div class="buy_cat">
                        <span class="input-select datepickerIconGroup"><input class="datepicker w200" value="${now }" name="std"type="text"id="uc_cashflowrecord_std_format"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select datepickerIconGroup"><input class="datepicker w200" value="${now }" name="end"type="text"id="uc_cashflowrecord_end_format"><i class="fa fa-calendar dateimg c_black"></i></span>&nbsp;
                        <span class="input-select"><a href="javascript:;"class="btn b0"id="payQuery">查 询</a></span>
                    </div>
                </form>
                <div class="content_frame smallT">
                    <table width="940" border="0" cellspacing="0" cellpadding="0" class="message_table bg_black3">
                        <thead>
                            <tr class="bg_white2">
                                <td>编号</td>
                                <td>充值金额</td>
                                <td>手续费</td>
                                <td>提交时间</td>
                                <td>交易类型</td>
                                <td>状态</td>
<!--                                 <td>备注</td> -->
                            </tr>
                        </thead>
                        <tbody id="uc_cashflowrecord_tbody">
                        
                        
                        </tbody>
                        <tfoot id="betOrderTblFoot">
                            <tr id="betOrderTblTabs">
                                <td colspan="7">
                                   <ul class="pagination pull-right tcdPageCode" id="pagecount" style="margin:0px;font-size: small;margin-right: 5px;margin-top: 2px;"><span id="paginationInfo"></span></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div></c:if>
            </div>
        </div>
        
        <c:if test="${userInfo.accountType!=6 }"><script>
        $(function() {
        	$('#curOver6').addClass("current");
        	$("#uc_cashflowrecord_std_format").datepicker();
    		$("#uc_cashflowrecord_end_format").datepicker();
        	list(1,startDate(),endDate());
        	total();
        	$('#payQuery').click(function(){
        		var startTime = startDate();
        		var endTime = endDate();
        		list(1,startTime,endTime);
        	});
        });
        
        function startDate(){
        	var startTime = $("#uc_cashflowrecord_std_format").val();
        	if(startTime!=null && startTime!=""){
    			startTime = startTime+' 00:00:00';
    		}
        	return startTime;
        }
        
        function endDate(){
        	var endTime = $("#uc_cashflowrecord_end_format").val();
    		if(endTime!=null && endTime!=""){
    			endTime = endTime+' 23:59:59';
    		}
    		return endTime;
        }
        
        function list(pageNo,start,end){
        	var page = {
	 				"rows" : 10,
					"page" :pageNo,
					"begin" :start,
					"end" :end
	        	};
        	
        	$.ajax({
				url:JIEBAO.base+"/cpSystem/payRecord.do",
				data : page,
				dataType:"json",
				success:function(j){
					var data = j.rows;
					var pageTotal = j.total;
					pageTotal = Math.ceil(pageTotal/10);
					var col = '';
					for(var j in data){
						col+='<tr><td>'+data[j].orderNo+'</td>';
						col+='<td>'+data[j].money+'</td>';
						if(data[j].fee!=null){
							col+='<td>'+data[j].fee+'</td>';
						}else{
							col+='<td>0.00</td>';
						}
						
						col+='<td>'+getLocalTime(data[j].createDatetime)+'</td>';
						col+='<td>'+data[j].payName+'</td>';
						if(data[j].status==2){
							col+='<td style="color:green">成功</td>';
						}else if(data[j].status==1){
							col+='<td style="color:orange">处理中</td>';
						}else{
							col+='<td style="color:red">处理失败</td>';
						}
// 						if(data[j].opDesc!=null){
// 							col+='<td>'+data[j].opDesc+'</td>';
// 						}else{
// 							col+='<td>-</td>';
// 						}
						
						col+='</tr>';
					}
					$('#uc_cashflowrecord_tbody').html(col);
				}
			});
        }
        
        function total(){
        	$.ajax({
				url:JIEBAO.base+"/cpSystem/payRecord.do",
				dataType:"json",
				type:"GET",
				success:function(j){
					var pageTotal = j.total;
					pageTotal = Math.ceil(pageTotal/10);
		         	$("#paginationInfo").createPage({
	 	             pageCount:pageTotal,
	 	             current:1,
	 	             backFn:function(p){
	 	              list(p,startDate(),endDate());//p为页码
	 	             }
	 	         });
				}
			});
        }
        </script></c:if>