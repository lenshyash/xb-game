$(function(){
    //撤单
    var $table = $(".pubTab tr td");
    $table.on("click",".ceDan",function(){
    	var it = $(this),orderId=it.attr("order_id"),lotCode=it.attr("lot_code");
    	layer.confirm("您确定要撤单该注单？",{btn:['确定','取消']},function(){
    		$.post(markSix.base+"/marksixbet/cancelOrder.do",{orderId:orderId,lotCode:lotCode},function(data){
    			if(data.success){
    				layer.msg("撤单成功",{icon:6},function(){
    					setTimeout($("#historyPost").submit(),1000);
    				});
    			}else{
    				layer.msg(data.msg,{icon:5});
    			}
    		});
    	},function(index){
    		layer.close(index);
    	});
    }).on("click",".showOrder",function(){
    	var it=$(this),orderId=it.attr("order_id"),lotCode=it.attr("lot_code"),teshuName="",data="",yingkui=0;
    	$.post(markSix.base+"/marksixbet/getMarkSixOrderDetail.do",{orderId:orderId,lotCode:lotCode},function(dd){
    		if(!dd.success && dd.msg != null){
    			layer.msg(dd.msg,{icon:5});
    		}else{
    			data = dd.bcOrder;
    			if(data.winMoney == null || data.winMoney == ""){data.winMoney = 0;}
    			if(data.winZhuShu == null || data.winZhuShu == ""){data.winZhuShu = 0;}
    			if(data.rollBackMoney == null || data.rollBackMoney ==""){data.rollBackMoney = 0;}
    			if(data.lotteryHaoMa == null || data.lotteryHaoMa == ""){data.lotteryHaoMa = "- -";}
    			switch(data.status){
    			case 1:data.status = "未开奖";break;
    			case 2:data.status = "已中奖";yingkui =(data.winMoney - data.buyMoney+data.rollBackMoney);break;
    			case 3:data.status = "未中奖";yingkui =(data.winMoney - data.buyMoney+data.rollBackMoney);break;
    			case 4:data.status = "撤单";break;
    			case 5:data.status = "回滚成功";break;
    			case 6:data.status = "回滚异常";break;
    			case 7:data.status = "开奖异常";break;
    			}
    			switch(dd.groupCode){
    			case "wsl":teshuName="0尾赔率：";break;
    			case "hx" :
    			case "lx" :teshuName="生肖本命年赔率：";break;
    			}
    			layer.alert(
        				'<table width="100%" border="0" cellspacing="0" cellpadding="0" class="message_table" style="font-size: 13px; color: #000;"><tbody>'
        				+'<tr class="bg_black3"><td colspan="4" class="open_title">订单号：<span style="color:green;">'+data.orderId+'</span></td></tr>'
        				+'<tr><th class="bg_black3">帐号：</th><td>'+data.account+'</td><th class="bg_black3">单注金额：</th><td>'+(data.buyMoney).toFixed(2)+'</td></tr>'
        				+'<tr><th class="bg_black3">下注时间：</th><td>'+data.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+data.buyZhuShu+'</td></tr>'
        				+'<tr><th class="bg_black3">彩种：</th><td>'+data.lotName+'</td><th class="bg_black3">投注总额：</th><td>'+((data.buyMoney)*(data.buyZhuShu)).toFixed(2)+'</td></tr>'
        				+'<tr><th class="bg_black3">期号：</th><td>'+data.qiHao+'</td><th class="bg_black3">基础赔率'+(data.rollBackRate?('/反水'):'')+'：</th><td>'+data.minBonusOdds.toFixed(2)+(data.rollBackRate?('/'+data.rollBackRate+"%"):'')+'</td></tr>'
        				+'<tr><th class="bg_black3">玩法：</th><td>'+data.playName+'</td><th class="bg_black3">中奖注数：</th><td>'+data.winZhuShu+'</td></tr>'
        				+'<tr><th class="bg_black3">开奖号码：</th><td>'+data.lotteryHaoMa+'</td><th class="bg_black3">中奖金额：</th><td>'+data.winMoney.toFixed(2)+'</td></tr>'
        				+'<tr><th class="bg_black3">状态：</th><td>'+data.status+'</td><th class="bg_black3">盈亏：</th><td>'+yingkui.toFixed(2)+'</td></tr>'
        				+'<tr>'+(data.rollBackRate?('<th class="bg_black3">销售返点</th><td '+(dd.teshuOdds==null?('colspan="3"'):'')+'>'+data.rollBackMoney.toFixed(2)+'</td>'):'')+(dd.teshuOdds!=null?('<th class="bg_black3">'+teshuName+'</th><td '+(data.rollBackRate?'':('colspan="3"'))+'>'+dd.teshuOdds.toFixed(2)+'</td>'):'')+'</tr>'
        				+'<tr><td colspan="4"><textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 50px; border: 1px solid #ccc; font-size: 16px;">'+data.haoMa+'</textarea></td></tr>'
        				+"</tbody></table>"
        				,{title:"系统提示",area:['500px','auto'],offset:['10%']});
    		}
    	})
    });
    
    
    //表单提交
    $("#orderSearch").click(function(){
    	$("#historyPost").submit();
    });
    
})


function selPageSize(pageSize){
	var $form = $("#historyPost");
	$form.find("input[name=pageSize]").val(pageSize);
	$form.submit();
}

function selPage(pageNumber){
	var $form = $("#historyPost");
	$form.find("input[name=pageNumber]").val(pageNumber);
	$form.submit();
}

//1今天，2昨天，3本周，4上周，5本月，6上月
function quickSelDate(dateVal){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();
	var week = date.getDay();
	 //一天的毫秒数  
    var millisecond = 1000 * 60 * 60 * 24;
    //减去的天数  
    var minusDay = week != 0 ? week - 1 : 6;
	var startTime = "yyyy-MM-dd 00:00:00";
	var endTime = "yyyy-MM-dd 23:59:59";
	var startDate = "";
	var endDate = "";
	switch(dateVal){
	case 1:
		startDate = $.formatDate(startTime,date);
		endDate = $.formatDate(endTime,date);
	break;
	case 2:
		date.setDate(date.getDate()-1);
		startDate = $.formatDate(startTime,date);
		endDate = $.formatDate(endTime,date);
	break;
	case 3:
        startDate = $.formatDate(startTime,new Date(date.getTime()-(minusDay*millisecond)));
        endDate = $.formatDate(endTime,date);
	break;
	case 4:
        //获得当前周的第一天
        var currentWeekDayOne = new Date(date.getTime() - (millisecond * minusDay));
        startDate = $.formatDate(startTime,new Date(currentWeekDayOne.getTime() - (millisecond * 7)));
        endDate = $.formatDate(endTime,new Date(currentWeekDayOne.getTime() - millisecond));
	break;
	case 5:
		date.setDate(1);
		startDate = $.formatDate(startTime,date);
		endDate = $.formatDate(endTime,new Date());
	break;
	case 6:
		//获取上一月的第一天
		var priorMonthFirstDay = getPriorMonthFirstDay(year, month);
		//获得上一月的最后一天  
        var priorMonthLastDay = new Date(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth(), this.getMonthDays(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth()));
        startDate = $.formatDate(startTime,priorMonthFirstDay);
		endDate = $.formatDate(endTime,priorMonthLastDay);
	break;
	}
	$("#startDate").val(startDate);
	$("#endDate").val(endDate);
}




/**
 * 返回上一个月的第一天Date类型
 * @param year 年
 * @param month 月
 **/
function getPriorMonthFirstDay(year,month){
	//年份为0代表,是本年的第一月,所以不能减  
    if (month == 0) {
        month = 11; //月份为上年的最后月份  
        year--; //年份减1  
        return new Date(year, month, 1);
    }
    //否则,只减去月份  
    month--;
    return new Date(year, month, 1); ;
}


/**
 * 获得该月的天数
 * @param year年份
 * @param month月份
 * */
function getMonthDays(year,month){
     //本月第一天 1-31  
     var relativeDate = new Date(year, month, 1);
     //获得当前月份0-11  
     var relativeMonth = relativeDate.getMonth();
     //获得当前年份4位年  
     var relativeYear = relativeDate.getFullYear();

     //当为12月的时候年份需要加1  
     //月份需要更新为0 也就是下一年的第一个月  
     if (relativeMonth == 11) {
         relativeYear++;
         relativeMonth = 0;
     } else {
         //否则只是月份增加,以便求的下一月的第一天  
         relativeMonth++;
     }
     //一天的毫秒数  
     var millisecond = 1000 * 60 * 60 * 24;
     //下月的第一天  
     var nextMonthDayOne = new Date(relativeYear, relativeMonth, 1);
     //返回得到上月的最后一天,也就是本月总天数  
     return new Date(nextMonthDayOne.getTime() - millisecond).getDate();
 };




(function($){  
    $.formatDate = function(pattern,date){  
        //如果不设置，默认为当前时间  
        if(!date) date = new Date();  
        if(typeof(date) ==="string"){  
             if(date=="")  date = new Date();  
              else  date = new Date(date.replace(/-/g,"/"));  
        }     
        /*补00*/  
        var toFixedWidth = function(value){  
             var result = 100+value;  
             return result.toString().substring(1);  
        };  
          
        /*配置*/  
        var options = {  
                regeExp:/(yyyy|M+|d+|h+|m+|s+|ee+|ws?|p)/g,  
                months: ['January','February','March','April','May',  
                         'June','July', 'August','September',  
                          'October','November','December'],  
                weeks: ['Sunday','Monday','Tuesday',  
                        'Wednesday','Thursday','Friday',  
                            'Saturday']  
        };  
          
        /*时间切换*/  
        var swithHours = function(hours){  
            return hours<12?"AM":"PM";  
        };  
          
        /*配置值*/  
        var pattrnValue = {  
                "yyyy":date.getFullYear(),                      //年份  
                "MM":toFixedWidth(date.getMonth()+1),           //月份  
                "dd":toFixedWidth(date.getDate()),              //日期  
                "hh":toFixedWidth(date.getHours()),             //小时  
                "mm":toFixedWidth(date.getMinutes()),           //分钟  
                "ss":toFixedWidth(date.getSeconds()),           //秒  
                "ee":options.months[date.getMonth()],           //月份名称  
                "ws":options.weeks[date.getDay()],              //星期名称  
                "M":date.getMonth()+1,  
                "d":date.getDate(),  
                "h":date.getHours(),  
                "m":date.getMinutes(),  
                "s":date.getSeconds(),  
                "p":swithHours(date.getHours())  
        };  
          
        return pattern.replace(options.regeExp,function(){  
               return  pattrnValue[arguments[0]];  
        });  
    };  
      
})(jQuery); 

