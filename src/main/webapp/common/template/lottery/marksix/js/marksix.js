var balanceTimer = function(){
	var t1;
	function balance() {
		$.ajax({
			url : markSix.base + "/meminfo.do",
			success : function(j) {
				if(j.login){//如果已登录状态
					$('#balance').html(j.money).attr("balance",j.money);
					return true;
				}else{return false;}
				
			}
		});
	}
	return {
		start : function() {
			var a = this;
			clearTimeout(t1);
			balance();
			t1 = setTimeout(function(){a.start()},Math.floor(Math.random() * 1000 + 1) + 10000);
		},
		stop  : function() {
			clearTimeout(t1);
		}
	};
}();

var CurState = 1;
$(function () {
	if(!!window.ActiveXObject){// 判断ie版本是否小于10
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(navigator.userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion<10){
        	var nai=$("#not-allow-old-ie").show();
        	nai.find('.ie-version').text(fIEVersion);
        	nai.find('#ie-agent').text(navigator.userAgent);
        	return;
        }
	}
	
    setInterval(ShowCurrentTime, 1000);
    SetTicketCase();
    SetTicketResult();
    ShowTMFastBet();
    balanceTimer.start();
    
    
  //LHC大类点击事件
    $("#type a").bind("click",function(){
    	var $it = $(this);
    	var groupId = $it.attr("group-id");
    	var code = $it.attr("group-code");
    	MarkInit.groupCode = code;
    	if(groupId == null || groupId == "" || groupId == "undefined"){
    		return;
    	}
    	$.get( markSix.base +"/marksix/lotPlayList.do",{groupId:groupId,code:code},function(data){
    		if(data.msg != null){
    			layer.msg(data.msg,{icon:5});
    			return;
    		}
    		$it.addClass("atuttonselected").siblings().removeClass("atuttonselected");
    		MinModule(data.playList);
    	});
    }).triggerHandler("click");
    
    
    //LHC小类点击事件
    var $minPlayList = $("#playMarkNav");
    $minPlayList.on("click","a",function(){
    	var $it = $(this);
    	$it.addClass("atuttonselected").siblings().removeClass("atuttonselected");
    	var code = $it.attr("data-code");
    	MarkInit.playCode = code;
    	MarkInit.resetClear();
    	if(code == null || code == "" || code == "undefined"){
    		layer.alert("没有相应的数据",{icon:2});
    		return;
    	}
    	$.get(markSix.base+"/member/"+markSix.folder+"/module/lottery/marksix/lot_play_type_"+code+'_'+markSix.folder+".html?v="+markSix.caipiao_version,function(data){
    		$("#betForm").html(data);
    		SetTicketCase();
    	});
    });
    
    
    //快捷金额投注
    var $money = $("#MoneyList");
    $money.on("mouseover mouseout","td.MoneyListtd",function(event){
    		if(event.type=='mouseover'){
    		$(this).css("background-color","#38290B");
    		}else{$(this).css("background-color","");}
    }).on("mouseleave",function(){$(this).hide()}).on("click","td.MoneyListtd",function(){
    	TextChange(this,$.trim($(this).html()));
    	$(bettext).val($.trim($(this).html()));
	   	$("#MoneyList").slideUp();
    });
    
  //下注提交前处理
    var $tcSubmit = $("#betForm");
    $tcSubmit.on("click",".tc input[type='button']",function(){
    	//判断多选框最少选择个数是否为0，如果为0则为input=type类型
    	var minSelected = $("#mgroupnum").val();
    	if(minSelected != 0){
    		checkedShowModule(minSelected);
    	}else{
    		//文本类
    		textShowModule();
    	}
    }).on("change",".pubTab input[type=text]",function(){
    	TextChange(this,$(this).val());
    }).on("click",".tc input[type=reset]",function(){
    	$tcSubmit.find("input[type=text]").val("0");
    	$tcSubmit.find("input[type=checkbox]").removeAttr("checked");
    }).on("keyup",".pubTab input[type=text]",function(){
    	$(this).val($(this).val().replace(/[^\d]/g, ''));
    }).on("click",".pubTab input[type=checkbox]",function(){
    	MarkInit.checkedClick($(this));
    });
    
});




var MarkInit = {
		groupCode:null,
		playCode:null,
		nowSelected:1,	//默认选中1
		checkedClick:function(source){
			var maxSelected = MarkInit.resGroupMaxSelected();
			if(maxSelected==0){return;}
			if(source.is(":checked")){ //++
				MarkInit.nowSelected = MarkInit.nowSelected + 1;
			}else{ //--
				MarkInit.nowSelected = MarkInit.nowSelected - 1;
			}
			if((MarkInit.nowSelected-1)>=maxSelected){
				//禁用多选框
				MarkInit.checkBoxDisabledTrue();
				return;
			}else{
				MarkInit.checkBoxDisabledFalse();
			}
		},
		resGroupMaxSelected:function(){
			switch(MarkInit.groupCode){
				case "lm":
					return 10;
				case "wsl":
				case "lx":
					return 6;
					break;
				case "qbz":
					return MarkInit.resPlayMaxSelected();
				default:
					return 0;
			}
		},
		resPlayMaxSelected:function(){
			switch(MarkInit.playCode){
				case "w_bz":
				case "liu_bz":
				case "qi_bz":
					return 10;
				case "ba_bz":
					return 11;
				case "jiu_bz":
					return 12;
				case "shi_bz":
				case "shiy_bz":
					return 13;
				case "shie_bz":
					return 14;
			}
		},
		checkBoxDisabledTrue:function(){
			$("#betForm").find(".pubTab tbody>tr>td input[type=checkbox]").each(function(index,item){
				if(!$(item).is(":checked")){
					$(item).attr("disabled",true);
				}
			})
		},
		checkBoxDisabledFalse:function(){
			$("#betForm").find(".pubTab tbody>tr>td input[type=checkbox]").attr("disabled",false);
		},
		resetClear:function(){
			MarkInit.nowSelected = 1;
			MarkInit.checkBoxDisabledFalse();
		}
}


var t1;
var onesFlag = false;
function downTime(){
	if(CurState==0){
		clearInterval(t1);
		return false;
	}
	var nowSeconds = parseInt($(".SecondsSpan").html()) - 1;
	if(nowSeconds < 0){
		nowSeconds = 59;
		SetTicketCase();	//未封盘之前1分钟处理一次
		var nowMinutes = parseInt($(".MinutesSpan").html()) -1;
		if(nowMinutes < 0){
			nowMinutes = 59;
			var nowHours=parseInt($(".HoursSpan").html()) -1;
			if(nowHours < 0){
				return false;
			}
		}
		 $(".MinutesSpan").html(LeftPad(nowMinutes));
	}else if(nowSeconds == 30){SetTicketCase();}
	$(".SecondsSpan").html(LeftPad(nowSeconds));
}

//设置动态时间
function ShowCurrentTime() {
    var now = new Date();
    var year = now.getFullYear();
    var month = parseInt(now.getMonth()) + 1;
    var day = now.getDate();
    var hours = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    $("#currentTime").html("" + year + "-" + LeftPad(month) + "-" + LeftPad(day) + " " + LeftPad(hours) + ":" + LeftPad(minutes) + ":" + LeftPad(seconds) + "");
}


///设置上一期结果
function SetTicketResult() {
    $.ajax({
        url: markSix.base +"/marksix/markLast.do",
        type: "GET",
        data: { lotCode: markSix.lotCode },
        success: function (data) {
        	var haoma = new Array();
        	if(!data.success && data.msg != null){
        		$("#currentDate").html("暂时没有开奖信息");
        		haoma = "?,?,?,?,?,?,?".split(",");
        	}else{
        		//$("#currentDate").html("上一期数");
            	$("#currentPeriod").html("第 <span class='colorDate'>"+data.qihao+"</span> 期开奖号码");
            	haoma = data.haoma.split(",");
        	}
        	ShowOpenNumber(0,haoma);
        }
    });
}
//递归显示开奖结果滚动
function ShowOpenNumber(cur, str) {
    var intervalID;
    intervalID = window.setInterval(function () {
        $('#ball-' + (cur + 1)).html("<strong>"+str[cur]+"</strong>");
        if ((cur + 1) == 7) {
            $('#ball-' + (cur + 1)).css("background-image", "url("+markSix.base+"/common/template/lottery/marksix/images/result_last_bg.gif)");
        }else{
            $('#ball-' + (cur + 1)).css("background-image", "url("+markSix.base+"/common/template/lottery/marksix/images/result_bg.gif)");
        }
        clearInterval(intervalID);
        if ((cur + 1) >= str.length) {
            return;
        }
        else {
            cur = cur + 1;
            ShowOpenNumber(cur, str);
        }
    }, cur == 0 ? 1000 : 500);
}


//checkbox下注总金额计算，注数*单注金额
function MoneyChange() {
    $('#totalMoney').html($('#betCount').html() * $('#BetMoney').val());
}



//下一期开奖时间
function SetTicketCase() {
	//clearInterval(t1);
    $.ajax({
        url: markSix.base + "/marksix/markOp.do",
        type: "GET",
        data: { lotCode:markSix.lotCode},
        cache: false,
        success: function (data) {
        		if(!data.success && data.msg != null){
        			$("#openStr").html("第0期开奖时间");
        			$("#openDate").html("暂没有下一期开奖时间");
        			$(".DaysSpan").html(LeftPad(0));
        			$(".HoursSpan").html(LeftPad(0));
        			$(".MinutesSpan").html(LeftPad(0));
        			$(".SecondsSpan").html(LeftPad(0));
        			CurState = 0;BetOpenOrClose(0);
        		}else{
        			$("#openDate").html(data.activeTime);
        			$('.DaysSpan').html(data.days);
        			$('.HoursSpan').html(LeftPad(data.hours));
        			$('.MinutesSpan').html(LeftPad(data.minutes));
        			$('.SecondsSpan').html(LeftPad(data.seconds));
        			$("#openStr").html("第"+data.activeQihao+"期开奖时间");
        			$("#QiHao").val(data.activeQihao);
        			clearInterval(t1);
        			t1 = window.setInterval("downTime()",1000);
        			switch(data.state){
	        			case 2:
	        				BetOpenOrClose(0);
	        				$("#DiffStr").html("距离开盘时间");
	        				break;
	        			case 3:
	        				BetOpenOrClose(0);
	        				$("#DiffStr").html("距离开盘时间");
	        				break;
	    				default:
	    					$("#DiffStr").html("距离封盘时间");
	    					if(onesFlag){BetOpenOrClose(1);}
	    					break;
        			}
        		}
        }
    });
}

//开封盘设置
function BetOpenOrClose(State) {
	var $betForm = $("#betForm");
    if (State == 1) {
    	$betForm.find("input[type=text]").attr("disabled",false).css("color","block").val("0");
    	$betForm.find("input[type=button]").attr("disabled",false);
    	$betForm.find("input[type=reset]").attr("disabled",false);
    	$betForm.find("input[type=checkbox]").attr("disabled",false).attr("title","");
    	$("#betfastMoney").attr("disabled",false).val("").css("color","block");
    	onesFlag = false;
    } else {
        Closebetviewlist();
        $betForm.find("input[type=text]").attr("disabled","disabled").css("color","red").val("封盘");
        $betForm.find("input[type=button]").attr("disabled","disabled");
        $betForm.find("input[type=reset]").attr("disabled","disabled");
        $betForm.find("input[type=checkbox]").attr("disabled","disabled").attr("title","封盘");
        $("#betfastMoney").attr("disabled","disabled").val("封盘").css("color","red");
        onesFlag = true;
    }
}
///不足两位补0
function LeftPad(Value) {
    var New = "" + Value;
    if (New.length < 2)
        return "0" + New;
    else
        return New
}


//------------------特码快速下注   开始
function ShowTMFastBet() {
    $('#ShowTMFastBet').css({ top: '30%', left: '50%', margin: '-' + ($('#ShowTMFastBet').height() / 2) + 'px 0 0 -' + ($('#ShowTMFastBet').width() / 2) + 'px' });
}
function LoadTMFastBet() {
    $('#ShowTMFastBet').show();
}
//------------------特码快速下注   结束


//小类导航模版
function MinModule(playList){
	var html="<table width='100%' cellspacing='0' cellpadding='0' border='0' class='tab_001'><tbody><tr><td><div class='btn-list'>";
	$.each(playList,function(i,item){
		if(i == 0){
			html = html + "<a class='atutton atuttonselected' data-code='"+item.code+"' href='javascript:;'>"+item.name+"</a>";
			$.get(markSix.base+"/member/"+markSix.folder+"/module/lottery/marksix/lot_play_type_"+item.code+'_'+markSix.folder+".html?v="+markSix.caipiao_version,function(data){
				$("#betForm").html(data);
				SetTicketCase();
			});
			MarkInit.playCode = item.code;
			MarkInit.resetClear();
		}else{
			html = html + "<a class='atutton' data-code='"+item.code+"' href='javascript:;'>"+item.name+"</a>";
		}
	});
	html = html + "</tbody></tr></td></div></table>"
	$("#playMarkNav").html(html);
}


//选中时下注预览
function checkedShowModule(minSelected){
	//定义checked提交
	var state = 1;
	var $checked = $("#betForm input[name=isChecked]:checked");
	var value = "";
	var code = "";
	var postModel = [];
	var a = 1;
	var b = 1;
	var selectedNum = 0;
	var marksixId= 0;
	var flag = false;
	$checked.each(function(index,item){
		var v = isNaN($(item).val())==true?$(item).val():parseInt($(item).val());
		value = value + v + ",";
		selectedNum++;
		if(!flag){
			if($(item).val() != "0尾" && $(item).attr("now_year") != 1){
				marksixId = $(item).attr("mark_six_id");
				flag = true;
			}
		}
	});
	if(selectedNum < minSelected){
		layer.msg("至少选择" + minSelected + "个有效号码",{icon:5});
			return;
	}else{
		//计算最大注数
		for(var i=0;i<minSelected;i++){
			a = a * (minSelected-i);
		}
		for(var i=selectedNum;i>selectedNum-minSelected;i--){
			b = b * i;
		}
		if(b/a >= 500){
			layer.msg("最多只能下500注!",{icon:5});
			return;
		}
		var max = MarkInit.resGroupMaxSelected();
		if(max != 0 && selectedNum>max){
			layer.msg("最多只能选中"+max+"个有效号码",{icon:5});
			return ;
		}
	}
	postModel.push({haoma:value.substring(0,value.length-1),money:0,markSixId:marksixId});

	var html = "<div class=\"betviewlist\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;\" width=\"100%\">" +
	"<tr><td colspan='2' class='title'>" + $('#mname').val() + "<a class=\"close-bvl\" href=\"javaScript:Closebetviewlist()\"></a></td></tr>"
	+ "<tr class=\"deepcor-item\"><td class='left-title'>选择号码：</td><td><span>";
	
	   html = html + value.substring(0,value.length-1) + "</span></td></tr>"
	   html = html + "<tr><td class='left-title'>注数：</td><td><label id='betCount'>"+b/a+"</label></td></tr>"
	   html = html + "<tr class=\"deepcor-item\"><td class='left-title'>总金额：</td><td><label id='totalMoney'>0</label></td></tr>"
	   html = html + "<tr><td class='left-title'>单注金额：</td><td><input id='BetMoney' class='inputText' name='BetMoney' type='text' maxlength='5' onclick='ShowMoneyList(this)' onkeyup='MoneyChange()'/></td></tr>";
	   html = html + '<tr class=\"deepcor-item\"><td class="betviewlist-tit" colspan="2"><a class="ok-bvl" href="javaScript:void(0)" style="border:0;cursor: pointer;"></a></td></tr></table></div>'
	   $('#modalContent').html(html);
	   BetViewListDisableForm(postModel,state);
}

//文本下注预览
function textShowModule(){
		//定义文本下注提交
		var state = 2;
		var num = 0;
	    var Money = 0;
	    var v;
	    var postModel = [];
	    var flag = true;
	    var html = ('<div class="betviewlist"><table cellspacing="0" cellpadding="0" style="border-collapse:collapse;" width="100%">');
	    html = html + '<tr><td class="betviewlist-tit" colspan="4">' + $('#mname').val() + '</td></tr>'
	                         + '<tr  class="bvlftr"><td>序号</td><td>名称</td><td>赔率</td><td>下注金额</td></tr>';

	    $('input[type=text]', '#betForm').each(function (index, item) {
	        if ($(item).val() !== null && $(item).val() !== "" && $(item).val() !== "0" && $(item).val() !== "封盘") {
	            if ($(item).attr('data-result') == "false") {
	                flag = false;
	            }
	            num++;
	            var name = $("span", $(item).parent().prev().prev()).html();
	            var odds = $("span", $(item).parent().prev()).html();
	            v = isNaN(name)==true?name:name;
	            Money = Money + parseFloat($(item).val());
	            html = html + "<tr " + ((num % 2) == 1 ? " class=\"deepcor-item\"" : "") + ">";
	            html = html + "<td style=\"border-left:0;\">" + num + "</td>";
	            html = html + "<td>" + v +"</td>";
	            html = html + "<td>" +  odds + "</td>";
	            html = html + "<td style=\"border-right:0;\">" + $(item).val() + "</td>";
	            html = html + "</tr>";
	            postModel.push({haoma:v,money:$(item).val(),markSixId:$(item).attr("mark_six_id")});
	        }
	    });
	    if(!flag){layer.msg("金额低于最低或高于最高下注金额",{icon:2});return false;}
	    if (num == 0) { layer.msg("请先下注!",{icon:2}); return false;}
	    html = html + "<tr  style='font-weight:bolder;'><td></td><td></td><td>合计</td><td>"+Money+"</td><input type='hidden' name='totalPourMoney' value="+Money+"></tr>";
	    html = html + '<tr><td class="betviewlist-tit" colspan="4"><a class="ok-bvl" href="javaScript:void(0)" style="border:0;cursor: pointer;"></a></td></tr></table><a class="close-bvl" href="javaScript:Closebetviewlist()"></a></div>'
	    $('#modalContent').html(html);
	    BetViewListDisableForm(postModel,state);
}


//下注预览结果数据执行
function BetViewListDisableForm(postModel,state) {
	Showbetviewlist();
    $("#betviewlist").show();
    $(".ok-bvl").bind("click", function () {
    	if($(this).attr("isExit") == "1"){	//使按钮失效
    		return;
    	}
    	$(this).attr("isExit","1");
    	$.ajax({
    		url : markSix.base + "/meminfo.do",
    		success : function(j) {
    			if(j.login){//如果已登录状态
    				postPourSubmit(postModel,state);
    			}else{
    			 	layer.msg("未登入无法提交",{icon:5});
                    $("#betviewlist").hide();
    			}
    			
    		}
    	});
    });
}

//*最终数据提交
function postPourSubmit(postModel,state){
	var qiHao = $("#QiHao").val();
	var lot_code = $(".blank input[name=lot_code]").val();		//大类
	var play_code = $(".blank input[name=play_code]").val();	//小类
	var totalMoney,nowBalance,postModelData = [];
	if(state == 1){
		var oneMoney = $("#BetMoney").val();
		if(oneMoney == "封盘"){layer.msg("已封盘",{icon:5});return;}
		if(oneMoney == null || oneMoney == 0){
			layer.msg("请输入单注金额!",{icon:5});
			$(".ok-bvl").removeAttr("isExit");	//多选状态时，金额不输入则取消按钮失效状态
			return;
		}
		totalMoney = $("#totalMoney").html();
		$.each(postModel,function(i,item){
			postModelData.push({haoma:item.haoma,money:oneMoney,markSixId:item.markSixId});
		})
	}else{
		postModelData = postModel;
		totalMoney = $("#betForm input[name=totalPourMoney]").val();
	}
	nowBalance = $("#balance").attr("balance");
	if(totalMoney > parseInt(nowBalance)){
		layer.msg("下注金额超过帐户可用余额！",{icon:5});
		return;
	}
	var postData = {qiHao:qiHao,lotCode:lot_code,playCode:play_code,state:state};
	postData.pour = postModelData;
	$.post(markSix.base+"/marksixbet/doMarkBets.do",{data:JSON.stringify(postData)},function(data){
			if(!data.success){
				Closebetviewlist();//出错，关闭预览窗口
				layer.msg(data.msg,{icon:5},function(){
					setTimeout(SetTicketCase(),500);
				});
				return;
			}
			layer.msg(data.msg,{icon:6});
			Closebetviewlist();
	})
//	alert(JSON.stringify(postData));
}

//取消下注预览窗口
function Closebetviewlist() {
    $('#betviewlist').hide();
    $("#modalContent").html("");
    $("input[type=text]").attr("readonly", false).val("0");
    $("input[type=checkbox]").attr("readonly", false).removeAttr("checked");
    $("button[type=reset]", $(".tc")).attr("disabled", false);
    MarkInit.resetClear();
}

//预览窗口css加载
function Showbetviewlist() {
    $('#betviewlist').css({ top: '30%', left: '50%', margin: '-' + ($('#betviewlist').height() / 2) + 'px 0 0 -' + ($('#betviewlist').width() / 2) + 'px' });
}



//------------------显示快速金额   开始
var bettext;
function ShowMoneyList(source,betMinMoney,betMaxMoney){
	 $('#BetMinMoneyLimit').html(betMinMoney);
	 $('#BetMaxMoneyLimit').html(betMaxMoney);
	 if($("#betviewlist").is(":hidden")){
		 $(source).val("0");
		 $(source).css({ 'border': "#ccc solid 1px" });
		 $(source).attr("data-result","true");
		 var position = $(source).position();
		 bettext = source;
		 $("#MoneyList").css({ position: 'absolute', 'top': position.top + 22, 'left': position.left - 210, 'z-index': 1000000 });
		 $("#MoneyList").slideDown();
	 }
}

function TextChange(source,value){
	var $value = parseInt(value);
	var min = $("#BetMinMoneyLimit").html();
	var max = $("#BetMaxMoneyLimit").html();
	if($value != 0 && $value >= min && $value <= max){
		$(source).css({ 'border': "1px solid #cccccc" });
		$(source).attr("data-result","true");
	}else{
		$(source).css({ 'border': "3px solid red" });
		$(source).attr("data-result","false");
		layer.msg("下注金额在" + min + "-" + max +"之间",{icon:2});
	}
}

