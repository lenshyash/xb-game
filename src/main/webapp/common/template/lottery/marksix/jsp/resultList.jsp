<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="x-ua-compatible" content="ie=7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>六合彩开奖结果</title>
    <link href="${base}/common/template/lottery/marksix/css/lhc_result.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="mainbody">
        <div id="page_header">
            <div id="header_title">
                <img src="${base}/common/template/lottery/marksix/images/result/title_lottery.jpg" width="1000" height="100" />
            </div><!--header_title end-->
            <!--遊戲項目按鈕STAR-->
            <div id="gamecon">
                <div align="center">
                    <!--滚动图片 start-->
                    <div class="rollphotos">
                        <div class="blk_29">
                            <!--<div class="LeftBotton" id="LeftArr"></div>-->
                            <div class="Cont" ><!-- id="ISL_Cont_1" -->
                                    <div class="box"><a href="javascript:;" >六合彩开奖结果</a></div>
                            </div>
                            <!-- <div class="RightBotton" id="RightArr"></div> -->
                        </div><!--blk_29-->
                    </div><!--rollphotos-->
                </div><!--center-->
            </div><!--gamecon-->
            <!--遊戲項目按鈕END-->
        </div><!--page_header end-->
        <div>
<div>
   <!--  <div class="wcont-tit">六合彩 开奖结果</div> -->
    
    <table class="pubTab">
        <thead>
            <tr>
                <th rowspan="2">期数</th>
                <th rowspan="2">开奖时间</th>
                <th rowspan="2">正码一</th>
                <th rowspan="2">正码二</th>
                <th rowspan="2">正码三</th>
                <th rowspan="2">正码四</th>
                <th rowspan="2">正码五</th>
                <th rowspan="2">正码六</th>
                <th rowspan="2">特别号</th>
                <th rowspan="2">总和</th>
                <th colspan="4" id="CurNum">
                        <a class="Prev" onclick="Prev();"></a>
                    <span style="display:normal;" id="tit_ZM7">特别号</span>
                    <span style="display:none;" id="tit_ZM1">正码一</span>
                    <span style="display:none;" id="tit_ZM2">正码二</span>
                    <span style="display:none;" id="tit_ZM3">正码三</span>
                    <span style="display:none;" id="tit_ZM4">正码四</span>
                    <span style="display:none;" id="tit_ZM5">正码五</span>
                    <span style="display:none;" id="tit_ZM6">正码六</span>
                    <a class="Next" onclick="Next();"></a>
                </th>
            </tr>
            <tr>
                <th>单双</th>
                <th>大小</th>
                <th>合数</th>
                <th>尾数</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach items="${page.list}" var="marksix" varStatus="six">
        		<c:choose>
        		<c:when test="${not empty marksix.haoMa}">
                <tr <c:if test="${six.index%2==0}">class="white"</c:if>>
                    <td>${marksix.qiHao}</td>
                    <td><fmt:formatDate value="${marksix.endTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss" />${startTime}</td>
                    <c:set var="allSum" value="0"></c:set>
                    <c:forTokens items="${marksix.haoMa}" var="haoMa" delims=",">
                    	<c:set var="allSum" value="${allSum+haoMa}" ></c:set>
                    	<td><img src="${base}/common/template/lottery/marksix/images/number/num${haoMa}.gif" /></td>
                    </c:forTokens>
                    <td>${allSum}</td>
                   	<c:forTokens items="${marksix.haoMa}" var="haoMa" delims="," varStatus="ma">
                   		<c:choose>
                   		<c:when test="${ma.last}">
		                    <td class="col_ZM${ma.index+1}"><c:choose><c:when test="${haoMa == 49}">和</c:when><c:when test="${haoMa%2==0}">双</c:when><c:otherwise>单</c:otherwise></c:choose></td>
		                    <td class="col_ZM${ma.index+1}"><c:choose><c:when test="${haoMa == 49}">和</c:when><c:when test="${haoMa>=25}">大</c:when><c:otherwise>小</c:otherwise></c:choose></td>
		                    <td class="col_ZM${ma.index+1}"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber>${heshu}</td>
		                    <td class="col_ZM${ma.index+1}">${haoMa%10}</td>
	                    </c:when>
	                    <c:otherwise>
	                    	<td style="display:none;" class="col_ZM${ma.index+1}"><c:choose><c:when test="${haoMa == 49}">和</c:when><c:when test="${haoMa%2==0}">双</c:when><c:otherwise>单</c:otherwise></c:choose></td>
	                        <td style="display:none;" class="col_ZM${ma.index+1}"><c:choose><c:when test="${haoMa == 49}">和</c:when><c:when test="${haoMa>=25}">大</c:when><c:otherwise>小</c:otherwise></c:choose></td>
	                        <td style="display:none;" class="col_ZM${ma.index+1}"><fmt:parseNumber var="heshu" value="${(haoMa/10)%10 + haoMa%10}" integerOnly="true"></fmt:parseNumber>${heshu}</td>
	                        <td style="display:none;" class="col_ZM${ma.index+1}">${haoMa%10}</td>
	                    </c:otherwise>
	                    </c:choose>
                    </c:forTokens>
                </tr>
                </c:when>
                <c:otherwise>
                	<tr >
		        		<td colspan="14">暂没有开奖记录...</td>
		        	</tr>
                </c:otherwise>
                </c:choose>
                </c:forEach>
        </tbody>
    </table>
    <c:if test="${page.totalCount >0}">
    <div id="ac-account-page">
        <ul>
            <li style="padding-top:4px;">
                共 <span>${page.totalPageCount}</span> 页 <span>${page.totalCount}</span> 条记录，当前第
                <select onchange="selPage(this.value)">
                	<c:forEach var="pageNO" begin="1" end="${page.totalPageCount}">
                		<option>${pageNO}</option>
                	</c:forEach>
                </select>
                页
            </li>
        </ul>
        <div class="clr"></div>
    </div>
    </c:if>
    </div>
<script type="text/javascript">
    var cur = 7;
    function Prev() {
        $('#tit_ZM' + cur).hide();
        $('.col_ZM' + cur).hide();
        cur--;
        if (cur <= 0) {
            cur = 7;
        }
        $('#tit_ZM' + cur).show();
        $('.col_ZM' + cur).show();
    }
    function Next() {
        $('#tit_ZM' + cur).hide();
        $('.col_ZM' + cur).hide();
        cur++;
        if (cur > 7) {
            cur = 1;
        }
        $('#tit_ZM' + cur).show();
        $('.col_ZM' + cur).show();
    }
</script>
        </div>
    </div>
    <script src="${base}/common/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="${base}/common/template/lottery/marksix/js/ScrollPic.js" type="text/javascript"></script>
    <script type="text/javascript">
            <!--//--><![CDATA[//><!--
            var scrollPic_02 = new ScrollPic();
            scrollPic_02.scrollContId   = "ISL_Cont_1"; //内容容器ID
            scrollPic_02.arrLeftId      = "LeftArr";//左箭头ID
            scrollPic_02.arrRightId     = "RightArr"; //右箭头ID

            scrollPic_02.frameWidth     = 966;//显示框宽度
            scrollPic_02.pageWidth      = 138; //翻页宽度

            scrollPic_02.speed          = 10; //移动速度(单位毫秒，越小越快)
            scrollPic_02.space          = 10; //每次移动像素(单位px，越大越快)
            scrollPic_02.autoPlay       = false; //自动播放
            scrollPic_02.autoPlayTime   = 3; //自动播放间隔时间(秒)

            scrollPic_02.itemcount = $(".box").length;

            scrollPic_02.initialize(); //初始化

            //--><!]]>
            if ($(".box").length <=7) {
                $("#LeftArr, #RightArr").hide();
            }
            var gtype = "";
            if (gtype != '') {
                $("#" + gtype).show();
            } else {
                $("#game_ruleArea div:first").show();
            }
            $(".box a").click(function(){
                $("#game_ruleArea > div").hide();
                $("#" + $(this).attr("class")).show();
            });
            function selPage(page) {
                location ="${base}/marksix/resultList.do?page="+page;
            }
    </script>
</body>
</html>

