<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="ShowTMFastBet" style="z-index: 10000; position: absolute; background-color: gray;display:none;">
<div class="ShowTMFastBet">
    <div class="stmfb-tit">
        <span>特码快速下注</span>
    </div>
    <!-- <select style="position:absolute;top:25px;left:10px;" id="AddType">
        <option value="0">追加</option>
        <option value="1">单独</option>
        <option value="2">交集</option>
    </select> -->

    <div class="TMnum">
        <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse; cursor: pointer; margin-top: 8px;" id="LHCTMFast">
            <tbody>
            <tr>
                <td id="LHCTMA01" mid="${markMap['01'].id}">01</td>
                <td id="LHCTMA02" mid="${markMap['02'].id}">02</td>
                <td id="LHCTMA03" mid="${markMap['03'].id}">03</td>
                <td id="LHCTMA04" mid="${markMap['04'].id}">04</td>
                <td id="LHCTMA05" mid="${markMap['05'].id}">05</td>
                <td id="LHCTMA06" mid="${markMap['06'].id}">06</td>
                <td id="LHCTMA07" mid="${markMap['07'].id}">07</td>
                <td id="LHCTMA08" mid="${markMap['08'].id}">08</td>
                <td id="LHCTMA09" mid="${markMap['09'].id}">09</td>
                <td id="LHCTMA10" mid="${markMap['10'].id}">10</td>
            </tr>
            <tr>
                <td id="LHCTMA11" mid="${markMap['11'].id}">11</td>
                <td id="LHCTMA12" mid="${markMap['12'].id}">12</td>
                <td id="LHCTMA13" mid="${markMap['13'].id}">13</td>
                <td id="LHCTMA14" mid="${markMap['14'].id}">14</td>
                <td id="LHCTMA15" mid="${markMap['15'].id}">15</td>
                <td id="LHCTMA16" mid="${markMap['16'].id}">16</td>
                <td id="LHCTMA17" mid="${markMap['17'].id}">17</td>
                <td id="LHCTMA18" mid="${markMap['18'].id}">18</td>
                <td id="LHCTMA19" mid="${markMap['19'].id}">19</td>
                <td id="LHCTMA20" mid="${markMap['20'].id}">20</td>
            </tr>
            <tr>
                <td id="LHCTMA21" mid="${markMap['21'].id}">21</td>
                <td id="LHCTMA22" mid="${markMap['22'].id}">22</td>
                <td id="LHCTMA23" mid="${markMap['23'].id}">23</td>
                <td id="LHCTMA24" mid="${markMap['24'].id}">24</td>
                <td id="LHCTMA25" mid="${markMap['25'].id}">25</td>
                <td id="LHCTMA26" mid="${markMap['26'].id}">26</td>
                <td id="LHCTMA27" mid="${markMap['27'].id}">27</td>
                <td id="LHCTMA28" mid="${markMap['28'].id}">28</td>
                <td id="LHCTMA29" mid="${markMap['29'].id}">29</td>
                <td id="LHCTMA30" mid="${markMap['30'].id}">30</td>
            </tr>
            <tr>
                <td id="LHCTMA31" mid="${markMap['31'].id}">31</td>
                <td id="LHCTMA32" mid="${markMap['32'].id}">32</td>
                <td id="LHCTMA33" mid="${markMap['33'].id}">33</td>
                <td id="LHCTMA34" mid="${markMap['34'].id}">34</td>
                <td id="LHCTMA35" mid="${markMap['35'].id}">35</td>
                <td id="LHCTMA36" mid="${markMap['36'].id}">36</td>
                <td id="LHCTMA37" mid="${markMap['37'].id}">37</td>
                <td id="LHCTMA38" mid="${markMap['38'].id}">38</td>
                <td id="LHCTMA39" mid="${markMap['39'].id}">39</td>
                <td id="LHCTMA40" mid="${markMap['40'].id}">40</td>
            </tr>
            <tr>
                <td id="LHCTMA41" mid="${markMap['41'].id}">41</td>
                <td id="LHCTMA42" mid="${markMap['42'].id}">42</td>
                <td id="LHCTMA43" mid="${markMap['43'].id}">43</td>
                <td id="LHCTMA44" mid="${markMap['44'].id}">44</td>
                <td id="LHCTMA45" mid="${markMap['45'].id}">45</td>
                <td id="LHCTMA46" mid="${markMap['46'].id}">46</td>
                <td id="LHCTMA47" mid="${markMap['47'].id}">47</td>
                <td id="LHCTMA48" mid="${markMap['48'].id}">48</td>
                <td id="LHCTMA49" mid="${markMap['49'].id}">49</td>
                <td></td>
            </tr>
        </tbody></table>
    </div>
    <div class="TMchoose">
        <div class="TMchooseSubOne">
            <div class="TMchooseSubOneL fl">
                <ul>
                    <li>
                        <a onclick="SelectNumber('dan'); return false;" href="">
                            单
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('shuang'); return false;" href="">
                            双
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('da'); return false;" href="">
                            大
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('xiao'); return false;" href="">
                            小
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('dadan'); return false;" href="">
                            大单
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('xiaodan'); return false;" href="">
                            小单
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('dashuang'); return false;" href="">
                            大双
                        </a>
                    </li>

                    <li><a onclick="SelectNumber('xiaoshuang'); return false;" href="">小双</a></li>

                </ul><div class="clr"></div>

            </div>
            <div class="TMchooseSubOneR fl">
                <ul>
                    <li>
                        <a onclick="SelectNumber('shu'); return false;" href="">
                            鼠
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('niu'); return false;" href="">
                            牛
                        </a>
                    </li>

                    <li>
                        <a onclick="SelectNumber('hu'); return false;" href="">
                            虎
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('tu'); return false;" href="">
                            兔
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('long'); return false;" href="">
                            龙
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('she'); return false;" href="">
                            蛇
                        </a>
                    </li>
                    <li><a onclick="SelectNumber('ma'); return false;" href="">马</a></li>
                    <li><a onclick="SelectNumber('yang'); return false;" href=""> 羊</a></li>
                    <li><a onclick="SelectNumber('hou'); return false;" href="">猴</a></li>
                    <li><a onclick="SelectNumber('ji'); return false;" href=""> 鸡</a></li>
                    <li><a onclick="SelectNumber('gou'); return false;" href=""> 狗 </a></li>
                    <li><a onclick="SelectNumber('zhu'); return false;" href="">猪</a></li>
                </ul><div class="clr"></div>

            </div>
        </div><div class="clr"></div>

        <div class="TMchooseSubTwo">
            <div class="TMchooseSubTwoL fl">
                <ul>
                    <li>
                        <a onclick="SelectNumber('hongbo'); return false;" style="color:red;" href="">
                            红波
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('hongda'); return false;" href="">
                            红大
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('hongxiao'); return false;" href="">
                            红小
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('hongdan'); return false;" href="">
                            红单
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('hongshuang'); return false;" href="">
                            红双
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('lanbo'); return false;" style="color:#537BFF;" href="">
                            蓝波
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('landa'); return false;" href="">
                            蓝大
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('lanxiao'); return false;" href="">
                            蓝小
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('landan'); return false;" href="">
                            蓝单
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('lanshuang'); return false;" href="">
                            蓝双
                        </a>
                    </li>
                </ul><div class="clr"></div>

            </div>
            <div class="TMchooseSubTwoR fl">
                <ul>
                    <li>
                        <a onclick="SelectNumber('lubo'); return false;" style="color:green;" href="">
                            绿波
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('luda'); return false;" href="">
                            绿大
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('luxiao'); return false;" href="">
                            绿小
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('ludan'); return false;" href="">
                            绿单
                        </a>
                    </li>
                    <li>
                        <a onclick="SelectNumber('lushuang'); return false;" href="">
                            绿双
                        </a>
                    </li>
                </ul><div class="clr"></div>
            </div>
        </div>
    </div>
    <div class="TMBetTotal">
        下注金额：<input type="text" onkeyup="kuaiMoneyChange()" maxlength="5" id="betfastMoney" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
        下注数量：<span id="betfastNum"></span>&nbsp;&nbsp;&nbsp;&nbsp;
        下注总金额：<span id="betfastTotalMoney"></span>
    </div>
    <a onclick="CloseTMFastBet(); return false;" href="" class="close-ShowTMFastBet"></a>
    <div class="btn-div">
        <a style="height:32px; line-height:32px;" onclick="btnTMFastBet(); return false;" href="" class="btn-submit">提交</a>
        <a style="height:32px; line-height:32px;" onclick="ClearTMFastBet(); return false;" href="" class="btn-cancel">重置</a>
        <span style="color:red; font-weight:bold;" id="BetFormResult"></span>
    </div>
</div>
<style type="text/css">
    .fastselect {
        background-color: #EE703F;
        color: white;
        font-weight: bold;
    }
</style>
<script type="text/javascript">
    var SX_Shu = ",09,21,33,45,";
    var SX_Niu = ",08,20,32,44,";
    var SX_Hu = ",07,19,31,43,";
    var SX_Tu = ",06,18,30,42,";
    var SX_Long = ",05,17,29,41,";
    var SX_She = ",04,16,28,40,";
    var SX_Ma = ",03,15,27,39,";
    var SX_Yang = ",02,14,26,38,";
    var SX_Hou = ",01,13,25,37,49,";
    var SX_Ji = ",12,24,36,48,";
    var SX_Gou = ",11,23,35,47,";
    var SX_Zhu = ",10,22,34,46,";


    var Single = ",01,03,05,07,09,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,";
    var Double = ",02,04,06,08,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,";
    var Big = ",25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,";
    var Small = ",01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,";

    var BigSingle = ",25,27,29,31,33,35,37,39,41,43,45,47,49,";
    var SmallSingle = ",01,03,05,07,09,11,13,15,17,19,21,23,";
    var BigDouble = ",26,28,30,32,34,36,38,40,42,44,46,48,";
    var SmallDouble = ",02,04,06,08,10,12,14,16,18,20,22,24,";

    var Red = ",01,02,07,08,12,13,18,19,23,24,29,30,34,35,40,45,46,";
    var Green = ",05,06,11,16,17,21,22,27,28,32,33,38,39,43,44,49,";
    var Blue = ",03,04,09,10,14,15,20,25,26,31,36,37,41,42,47,48,";

    var RedBig = ",29,30,34,35,40,45,46,";
    var RedSmall = ",01,02,07,08,12,13,18,19,23,24,";
    var RedSingle = ",01,07,13,19,23,29,35,45,";
    var RedDouble = ",02,08,12,18,24,30,34,40,46,";

    var GreenBig = ",27,28,32,33,38,39,43,44,49";
    var GreenSmall = ",05,06,11,16,17,21,22,";
    var GreenSingle = ",05,11,17,21,27,33,39,43,49,";
    var GreenDouble = ",06,16,22,28,32,38,44,";

    var BlueBig = ",25,26,31,36,37,41,42,47,48,";
    var BlueSmall = ",03,04,09,10,14,15,20,";
    var BlueSingle = ",03,09,15,25,31,37,41,47,";
    var BlueDouble = ",04,10,14,20,26,36,42,48,";

    $(function () {
        $("td", $("#LHCTMFast")).bind("click", function () {
            if ($(this).html() != "") {
                $(this).toggleClass("fastselect");
                NumChange();
            }

        });
    });
    function SelectNumber(type) {
        switch (type) {
            //鼠
            case "shu":
                SelectTD(SX_Shu);
                break;
                //牛
            case "niu":
                SelectTD(SX_Niu);
                break;
                //虎
            case "hu":
                SelectTD(SX_Hu);
                break;
                //兔
            case "tu":
                SelectTD(SX_Tu);
                break;
                //龙
            case "long":
                SelectTD(SX_Long);
                break;
                //蛇
            case "she":
                SelectTD(SX_She);
                break;
                //马
            case "ma":
                SelectTD(SX_Ma);
                break;
                //羊
            case "yang":
                SelectTD(SX_Yang);
                break;
                //猴
            case "hou":
                SelectTD(SX_Hou);
                break;
                //鸡
            case "ji":
                SelectTD(SX_Ji);
                break;
                //狗
            case "gou":
                SelectTD(SX_Gou);
                break;
                //猪
            case "zhu":
                SelectTD(SX_Zhu);
                break;

                //单
            case "dan":
                SelectTD(Single);
                break;
                //双
            case "shuang":
                SelectTD(Double);
                break;
                //大
            case "da":
                SelectTD(Big);
                break;
                //小
            case "xiao":
                SelectTD(Small);
                break;

                //大单
            case "dadan":
                SelectTD(BigSingle);
                break;
                //小单
            case "xiaodan":
                SelectTD(SmallSingle);
                break;
                //大双
            case "dashuang":
                SelectTD(BigDouble);
                break;
                //小双
            case "xiaoshuang":
                SelectTD(SmallDouble);
                break;

                //红波
            case "hongbo":
                SelectTD(Red);
                break;
                //蓝波
            case "lanbo":
                SelectTD(Blue);
                break;
                //绿波
            case "lubo":
                SelectTD(Green);
                break;

                // 红大
            case "hongda":
                SelectTD(RedBig);
                break;
                // 红小
            case "hongxiao":
                SelectTD(RedSmall);
                break;
                // 红单
            case "hongdan":
                SelectTD(RedSingle);
                break;
                // 红双
            case "hongshuang":
                SelectTD(RedDouble);
                break;

            case "luda":
                SelectTD(GreenBig);
                break;
                // 绿大
            case "luxiao":
                SelectTD(GreenSmall);
                break;
                // 绿小
            case "ludan":
                SelectTD(GreenSingle);
                break;
                // 绿单
            case "lushuang":
                SelectTD(GreenDouble);
                break;
                // 绿双


                // 蓝大
            case "landa":
                SelectTD(BlueBig);
                break;
                // 蓝小
            case "lanxiao":
                SelectTD(BlueSmall);
                break;
                // 蓝单
            case "landan":
                SelectTD(BlueSingle);
                break;
                // 蓝双
            case "lanshuang":
                SelectTD(BlueDouble);
                break;

                // 鼠
            case "shu":
                SelectTD(BlueBig);
                break;
        }
    }
    function SelectTD(selectNum) {
        $("td", $("#LHCTMFast")).each(function (index, item) {
            if (selectNum.indexOf("," + $(item).html() + ",") != -1) {
                $(item).addClass("fastselect");
            }
        });
        NumChange();
    }
    function NumChange() {
        var num = 0;
        $("td", $("#LHCTMFast")).each(function (index, item) {
            if ($(item).hasClass("fastselect")) {
                num++;
            }
        });
        $('#betfastNum').html(num);
        kuaiMoneyChange();
    }
    function kuaiMoneyChange() {
        var betfastMoney = parseInt($('#betfastMoney').val());
        var num = parseInt($('#betfastNum').html());
        if (!isNaN(betfastMoney) && !isNaN(num)) {
            $('#betfastTotalMoney').html(num * betfastMoney);
        }
    }
    function ClearTMFastBet() {
        $("td", $("#LHCTMFast")).each(function (index, item) {
            $(item).removeClass("fastselect");
        });
    }
    function CloseTMFastBet() {
        $('#ShowTMFastBet').hide();
        $("#betfastMoney").val("");
        $("#ShowTMFastBet").find("td").removeClass("fastselect");
        $("#betfastNum").html("");
        $("#betfastTotalMoney").html("");
    }
    function btnTMFastBet() {
        var keylist = "";
        var postModel = [],postData = [];
        var oneMoney = $("#betfastMoney").val();
        var markSixId= "";
        $("td", $("#LHCTMFast")).each(function (index, item) {
            if ($(item).hasClass("fastselect")) {
                keylist = keylist + $(item).html() + ",";
                markSixId = $(item).attr("mid");
                postModel.push({haoma:$(item).html(),money:oneMoney,markSixId:markSixId});
            }
        });
        if(oneMoney == "封盘"){alert("已封盘");return;}
        if (oneMoney == "" || oneMoney == 0) {
            alert("请输入下注金额");
        }else if (keylist <= 6) {
            alert("请选择下注号码");
        }else if (balanceTimer.start() == false) {
            alert("请先登录");
        }else {
        	postData = {state:2,qiHao:$("#QiHao").val(),playCode:"tm_a",lotCode:$(".blank input[name=lot_code]").val()};
        	postData.pour = postModel;
        	$.post(markSix.base+"/marksixbet/doMarkBets.do",{data:JSON.stringify(postData)},function(data){
    			if(!data.success){
    				layer.msg(data.msg,{icon:5});
    				return;
    			}
    			layer.msg("下注成功",{icon:6});
    			CloseTMFastBet();
    	})
        }
    }
</script>
</div>