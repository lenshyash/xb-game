<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>${website_name}</title>
    <link rel="stylesheet" href="${base}/common/js/layer/skin/layer.css" id="layui_layer_skinlayercss">
    <link href="${base}/common/template/lottery/marksix/css/marksix.css?v=${caipiao_version}" rel="stylesheet" type="text/css">
    <link href="${base}/common/template/lottery/marksix/css/reset.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
    <script>
    	var markSix={
    		base : '${base}',
    		folder:"${stationFolder}",
    		caipiao_version : '${caipiao_version}',
    		lotCode : '${lotCode}',
    		lotType:'${lotType}',
    		fanShui:'${fanShui}',
    	};
    </script>
</head>
<body>
<div class="noScript" id="not-allow-old-ie" style="display:none">
     <div>
         <h2>不支援的浏览器</h2>
         <p>
             亲，我们不支援 IE 瀏覽器 10 以下的版本！<br>
             您的浏览器为 IE<span class="ie-version"></span> 内核浏览器。
         </p>
         <br>
         瀏覽器資訊:
         <br>
         <p id="ie-agent"></p>
         <br>
         <a href="https://www.baidu.com/s?wd=360%E6%B5%8F%E8%A7%88%E5%99%A8" rel="nofollow" target="_blank">请使用360浏览器，请使用极速模式</a>
     </div>
 </div>
<div class="main row">
        <div id="nav">
            <div class="l_float" id="div_s2"></div>
            <div class="l_float" id="div_s1">
            <a class="a_s1h l_float" href="">六合彩</a>
            </div>
            <div class="l_float" id="div_s3"></div>
        </div>
        <div id="type">
        <c:forEach var="group" items="${lotteryGroup}" varStatus="g">
        	<a href="javascript:;" group-id="${group.id }" group-code="${group.code}" class='<c:choose><c:when test="${g.index ==0 }">atutton atuttonselected</c:when><c:otherwise>atutton</c:otherwise></c:choose>'>${group.name}</a>
        </c:forEach>
        </div>
		 <div class="nsleft">
		    <div name="info_div" id="info_div">
		        <div class="msgHead">
		            <h2 style=" text-align:center; font-weight:bold;">
		                    	欢迎  ${user.account } !
		            </h2>
		        </div>
		        <div class="msgHead">
		            <h2>当前时间：<span id="currentTime"></span></h2>
		        </div>
		        <div class="msgHead">
		            <h2>当前余额：￥<span id="balance"></span></h2>
		        </div>
	            <div style=" cursor:pointer;" onclick="LoadTMFastBet();" class="msgHead">
	                <h2>特码快速下注</h2>
	            </div>
		        <div class="msgHead">
		            <h2><a target="_blank" href="${base}/marksixbet/historyResult.do">历史下注记录</a></h2>
		        </div>
		        <div class="msgHead">
		            <h2><a target="_blank" href="${base}/marksixbet/resultList.do">开奖结果</a> </h2>
		        </div>
		        <div class="msgHead">
		            <h2><a target="_blank" href="${base }/getConfig/gameRule.do?code=LHC">玩法说明</a></h2>
		        </div>
		    </div>
		
		</div>
        <div class="content">
			<jsp:include page="kaiJiangHaoMa.jsp"></jsp:include>
			<div id="playMarkNav"></div>
			<div id="betForm"></div>
			<input type="hidden" id="QiHao" value="">
        </div>
    </div>
    <jsp:include page="kuaiJieXiaZhu.jsp"></jsp:include>
    <jsp:include page="teMaKuaiSuXiaZhu.jsp"></jsp:include>

</body>
</html>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/marksix/js/marksix.js?v=${caipiao_version}"></script>
