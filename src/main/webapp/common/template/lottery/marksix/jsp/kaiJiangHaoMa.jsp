<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
          <tr>
              <td>
                      <div class="resultNowWrap">
                          <div class="resultNowHead">
                              <h5><span class="colorDate" id="currentDate"></span><span id="currentPeriod"></span></h5>
                          </div>
                          <div class="resultNowM fl">
				             <ul class="resultList7">
				                 <li id="ball-1"></li>
				                 <li id="ball-2"></li>
				                 <li id="ball-3"></li>
				                 <li id="ball-4"></li>
				                 <li id="ball-5"></li>
				                 <li id="ball-6"></li>
				                 <li class="yellowbg" id="ball-7"></li>
				            </ul>
                          </div>
                          <div class="resultTime fr">
                              <p>&nbsp;</p>
                              <p><span id="openStr"></span></p>
                              <p class="restTime"><span id="openDate"></span></p>
                              <p><span id="DiffStr"></span></p>
                              <p style="margin-top:5px;" class="restTime"><span id="DiffTime">
								  <span style="background-color:#306D48; padding:5px; border-radius:5px;" class="DaysSpan"></span> 天 <span style="background-color: #306D48; padding: 5px; border-radius: 5px;" class="HoursSpan"></span>:<span style="background-color: #306D48; padding: 5px; border-radius: 5px;" class="MinutesSpan"></span>:<span style="background-color: #306D48; padding: 5px; border-radius: 5px;" class="SecondsSpan"></span>
								  </span>
							  </p>
                          	</div>
                        	</div>
                        </td>
                    </tr>
                </tbody>
            </table>