<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="x-ua-compatible" content="ie=7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>历史下注记录</title>
    <link href="${base}/common/template/lottery/marksix/css/lhc_result.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="${base}/common/js/layer/skin/layer.css" id="layui_layer_skinlayercss">
    <style>
    	.message_table{
    	    border-bottom: 1px solid #cfcfcf;
		    border-right: 1px solid #cfcfcf;
		    border-left: 1px solid #cfcfcf;
		}
    	.message_table th, .message_table td {
			border-top: 1px solid #cfcfcf;
			padding: 15px 5px;
			text-align: center;
			line-height: 1em;
			vertical-align: top;
		}
		.message_table .bg_black3{background: #999ca1;}
    </style>
    <script>
    	var markSix={
    		base : '${base}',
    		fanshui:${fanshui}
    	};
    </script>
</head>
<body>
    <div id="mainbody">
        <div id="page_header">
            <div id="header_title">
                <img src="${base}/common/template/lottery/marksix/images/result/title_lottery.jpg" width="1000" height="100" />
            </div><!--header_title end-->
            <!--遊戲項目按鈕STAR-->
            <div id="gamecon">
                <div align="center">
                    <!--滚动图片 start-->
                    <div class="rollphotos">
                        <div class="blk_29">
                            <!--<div class="LeftBotton" id="LeftArr"></div>-->
                            <div class="Cont" ><!-- id="ISL_Cont_1" -->
                                    <div class="box"><a href="javascript:;" >历史下注记录</a></div>
                            </div>
                            <!-- <div class="RightBotton" id="RightArr"></div> -->
                        </div><!--blk_29-->
                    </div><!--rollphotos-->
                </div><!--center-->
            </div><!--gamecon-->
            <!--遊戲項目按鈕END-->
        </div><!--page_header end-->
        <div>
	<form action="${base}/marksixbet/historyResult.do" method="post" id="historyPost">
	<input type="hidden" value="${page.pageSize }" name="pageSize"/>
	<input type="hidden" value="${page.currentPageNo }" name="pageNumber" />
 	<div class="wcont-tit">
		<div class="his_left his_pa">
			<select name="orderStatus">
			<option value="">所有状态</option>
			<option value="1" <c:if test="${param.orderStatus == 1}">selected="selected"</c:if>>未开奖</option>
			<option value="2" <c:if test="${param.orderStatus == 2}">selected="selected"</c:if>>已中奖</option>
			<option value="3" <c:if test="${param.orderStatus == 3}">selected="selected"</c:if>>未中奖</option>
		</select>
		</div>
		<div class="his_left his_pa">
			<input type="text" value="${param.orderId}" name="orderId" placeholder="输入订单号" maxlength="36"/>
		</div>
		<div class="his_left his_pa">下单日期</div>
		<div class="his_left his_pa">
			<input id="startDate" name="startDate" placeholder="输入开始时间" class="laydate-icon" value="${param.startDate}">
			<br>
			<input id="endDate" name="endDate" class="laydate-icon" placeholder="输入结束时间" value="${param.endDate}">
		</div>
		<div class="his_left his_pa date">
			<div class="selDate" onclick="quickSelDate(1)">今天</div>
			<div class="selDate" onclick="quickSelDate(2)">昨天</div>
			<div class="selDate" onclick="quickSelDate(3)">本周</div>
			<div class="selDate" onclick="quickSelDate(4)">上周</div>
			<div class="selDate" onclick="quickSelDate(5)">本月</div>
			<div class="selDate" onclick="quickSelDate(6)">上月</div>
		</div>
		<div class="his_left his_pa orderSearch" id="orderSearch">订单查询</div>
	</div>
    <table class="pubTab" style="table-layout:fixed;">
        <thead>
            <tr style="height:50px;">
                <th rowspan="2" style="width:120px;">注单号</th>
                <th rowspan="2" style="width:100px;">下注时间</th>
                <th rowspan="2" style="width:100px;">彩种玩法</th>
                <th rowspan="2" style="width:100px;">期号</th>
                <th rowspan="2" style="width:80px;">下注号码</th>
                <th rowspan="2" style="width:150px;">开奖号码</th>
                <th rowspan="2" style="width:70px;">总额</th>
                <th rowspan="2" style="width:70px;">中奖金额</th>
                <th rowspan="2" style="width:70px;">状态</th>
            </tr>
        </thead>
        <tbody>
        	<c:choose>
        	<c:when test="${not empty page.list}">
        	<c:forEach var="order" items="${page.list}" varStatus="orIndex">
                <tr <c:if test="${orIndex.index%2==0}">class="white"</c:if>>
                    <td><a class="showOrder" order_id="${order.orderId}" lot_code="${order.lotCode}">${order.orderId}</a></td>
                    <td><fmt:formatDate value="${order.createTime}" var="createTime" pattern="yyyy-MM-dd HH:mm:ss"/>${createTime}</td>
                   	<td>${order.lotName}-${order.playName}</td>
                   	<td>${order.qiHao}</td>
                   	<td style="white-space:nowrap;overflow:hidden; text-overflow:ellipsis;">${order.haoMa}</td>
                   	<td><c:choose><c:when test="${empty order.lotteryHaoMa}">- -</c:when><c:otherwise>${order.lotteryHaoMa}</c:otherwise></c:choose> </td>
                   	<td><fmt:formatNumber value="${order.buyMoney}" var="buyMoney" pattern="0.00" maxFractionDigits="2" />${buyMoney}</td>
                   	<td><c:choose><c:when test="${empty order.winMoney || order.winMoney == '0'}">0.00</c:when><c:otherwise><fmt:formatNumber var="winMoney" value="${order.winMoney}" pattern="0.00" maxFractionDigits="2" />${winMoney}</c:otherwise></c:choose></td>
                   	<td>
                   		<c:choose>
                   			<c:when test="${order.status == 1}">未开奖</c:when>
                   			<c:when test="${order.status == 2}">已中奖</c:when>
                   			<c:when test="${order.status == 3}">未中奖</c:when>
                   			<c:when test="${order.status == 4}">撤单</c:when>
                   			<c:when test="${order.status == 5}">回滚成功</c:when>
                   			<c:when test="${order.status == 6}">回滚异常</c:when>
                   			<c:when test="${order.status == 7}">开奖异常</c:when>
                   		</c:choose>
                   	</td>
                </tr>
           </c:forEach>
           </c:when>
           <c:otherwise>
        	<tr >
        		<td colspan="9">没有符合查询的资料...</td>
        	</tr>
        	</c:otherwise>
        </c:choose>
        </tbody>
    </table>
    <c:if test="${page.totalCount >0}">
	    <div id="ac-account-page">
	        <ul>
	            <li style="padding-top:4px;"> 共 <span>${page.totalPageCount}</span> 页 <span>${page.totalCount }</span> 条记录，当前第
	                <select onchange="selPage(this.value)">
	                	<c:forEach var="pageNO" begin="1" end="${page.totalPageCount}">
	                		<option <c:if test="${pageNO == page.currentPageNo}">selected="selected"</c:if>>${pageNO}</option>
	                	</c:forEach>
	                </select>
	                页&nbsp;&nbsp;每页
	                <select name="pageSize" onchange="selPageSize(this.value)">
	                	<option value="20" <c:if test="${page.pageSize == 20}">selected="selected"</c:if>>20</option>
	                	<option value="30" <c:if test="${page.pageSize == 30}">selected="selected"</c:if>>30</option>
	                	<option value="50" <c:if test="${page.pageSize == 50}">selected="selected"</c:if>>50</option>
	                </select>
	                <c:if test="${page.currentPageNo != 1 }">
	                	<a class="showOrder" href="javascript:;" onclick="selPage(${page.currentPageNo-1})">上一页</a>
	                </c:if>
	                <c:if test="${page.currentPageNo != page.totalPageCount }">
	                	<a class="showOrder" href="javascript:;" onclick="selPage(${page.currentPageNo+1})">下一页</a>
	                </c:if>
	                </li>
	        </ul>
	    </div>
    </c:if>
    </form>
</div>
</div>
    <script src="${base}/common/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
    <script type="text/javascript" src="${base}/common/template/lottery/marksix/js/historyResult.js?v=1"></script>
    <script type="text/javascript" src="${base}/common/template/lottery/marksix/js/laydate/laydate.js"></script>
    <script type="text/javascript">
    	var start = {
    			elem:"#startDate",
    			format:"YYYY-MM-DD 00:00:00",
    			choose:function(datas){
    				end.min =datas;
    			}
    	}
    	var end = {
    			elem:"#endDate",
    			format:"YYYY-MM-DD 23:59:59",
    			choose:function(datas){
    				start.max=datas;
    			}
    	}
    	laydate(start);
    	laydate(end);
    </script>
</body>
</html>

