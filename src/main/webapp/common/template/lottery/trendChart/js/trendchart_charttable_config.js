var trendchart_charttable_config = {
	"SSC_5StarBaseTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 2, 11 ],
			stroke : '#0170B6',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 12, 21 ],
			stroke : '#199745',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 22, 31 ],
			stroke : '#A95912',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 32, 41 ],
			stroke : '#EC8427',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 42, 51 ],
			stroke : '#413106',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 2, 11 ],
			color : 'yellow'
		}, {
			indexRange : [ 12, 21 ],
			color : 'blue'
		}, {
			indexRange : [ 22, 31 ],
			color : 'yellow'
		}, {
			indexRange : [ 32, 41 ],
			color : 'blue'
		}, {
			indexRange : [ 42, 51 ],
			color : 'yellow'
		} ]
	},
	"11X5_BaseTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 18, 23 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 24, 29 ],
			stroke : '#499495',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 30, 35 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 7, 17 ],
			color : 'yellow'
		} ]
	},
	"BJSC_BaseTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 13, 22 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 23, 32 ],
			stroke : '#499495',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 33, 42 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 3, 42 ],
			color : 'yellow'
		} ]
	},
	"BJSC_BaseTrend_Comprehensive" : {
		foldLineConfigList : [ {
			indexRange : [ 16, 25 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 26, 33 ],
			stroke : '#499495',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 3, 12 ],
			color : 'yellow'
		} ]
	},
	"BJSC_PositionTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 3, 12 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 13, 14 ],
			stroke : '#499495',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 15, 16 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 17, 18 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 19, 21 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 22, 24 ],
			stroke : '#E4A8A8',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 3, 12 ],
			color : 'yellow'
		} ]
	},
	"PL3_BaseTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 5, 14 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 15, 24 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 25, 34 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 5, 14 ],
			color : 'yellow'
		}, {
			indexRange : [ 15, 24 ],
			color : 'blue'
		}, {
			indexRange : [ 25, 34 ],
			color : 'yellow'
		} ]
	},
	"K3_BaseTrend" : {
		foldLineConfigList : [ {
			indexRange : [ 5, 10 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 11, 16 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		}, {
			indexRange : [ 17, 22 ],
			stroke : '#CC9900',
			strokeWidth : 2,
			opacity : 1
		} ],
		currentOmissionConfigList : [ {
			indexRange : [ 5, 10 ],
			color : 'yellow'
		}, {
			indexRange : [ 11, 16 ],
			color : 'yellow'
		}, {
			indexRange : [ 17, 22 ],
			color : 'yellow'
		}, {
			indexRange : [ 23, 25 ],
			color : 'blue'
		} ]
	},
	"KLSF_BaseTrend" : {
		currentOmissionConfigList : [ {
			indexRange : [ 2, 21 ],
			color : 'yellow'
		} ]
	}
};