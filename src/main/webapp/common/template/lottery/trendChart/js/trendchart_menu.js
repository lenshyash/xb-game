var trendchart_menu_1 = {
	"SSC": [{
		name: "五星基本走势",
		value: "5StarBaseTrend"
	}],
	"11X5": [{
		name: "基本走势",
		value: "BaseTrend"
	}],
	"BJSC": [{
		name: "基本走势",
		value: "BaseTrend"
	}, {
		name: "定位走势",
		value: "PositionTrend"
	}],
	"PL3": [{
		name: "直选走势",
		value: "BaseTrend"
	}],
	"K3": [{
		name: "基本走势",
		value: "BaseTrend"
	}],
	"KLSF": [{
		name: "基本走势",
		value: "BaseTrend"
	}]
};

var trendchart_menu_2 = {
		"SSC_5StarBaseTrend": [{
			name: "五星基本走势",
			value: "SSC_5StarBaseTrend"
		}],
		"11X5_BaseTrend": [{
			name: "任选基本",
			value: "11X5_BaseTrend"
		}],
		"BJSC_BaseTrend": [{
			name: "基本走势",
			value: "BJSC_BaseTrend"
		}, {
			name: "综合走势",
			value: "BJSC_BaseTrend_Comprehensive"
		}],
		"BJSC_PositionTrend": [{
			name: "冠军",
			value: "BJSC_PositionTrend"
		}, {
			name: "亚军",
			value: "BJSC_PositionTrend"
		}, {
			name: "季军",
			value: "BJSC_PositionTrend"
		}, {
			name: "第四名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第五名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第六名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第七名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第八名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第九名",
			value: "BJSC_PositionTrend"
		}, {
			name: "第十名",
			value: "BJSC_PositionTrend"
		}],
		"PL3_BaseTrend": [{
			name: "直选走势",
			value: "PL3_BaseTrend"
		}],
		"K3_BaseTrend": [{
			name: "基本走势",
			value: "K3_BaseTrend"
		}],
		"KLSF_BaseTrend": [{
			name: "基本走势",
			value: "KLSF_BaseTrend"
		}]
	};