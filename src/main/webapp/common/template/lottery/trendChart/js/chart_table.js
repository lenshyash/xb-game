//graph panel
(function () {
    var defaultColors = [
        '#FF0000',
        '#FF6600',
        '#BD5151',
        '#499495',
        '#91ABE1',
        '#1C74AA',
        '#FF00FF',
        '#666666'
    ];

    var defaultConfig = {
        id:"graphPanel",
        toolsId:"graphPanelTools",
        chartTableId:"chartTable",
        colors:defaultColors,
        strokeWidth:2,
        excludeDivIds:[]
    };

    window.GraphPanel = function GraphPanel(config) {
        this.config = $.extend(defaultConfig, config || {});
        this.currentCanvas = undefined;
        this.activate = false;                          //activation flag
        this.startDrawing
        this.colorIndex = 0;
        this.cached = [];
        this.tempCached = [];                           //for ie
        this.supportCanvas = !($.browser.msie && /^[678]\./.test($.browser.version));
        this.ie6Flag = $.browser.msie && parseInt($.browser.version, 10) == 6;
        this.container = $('body');
        this.canvasIdCounter = 0;

        this.fromX = 0;
        this.fromY = 0;
        this.toX = 0;
        this.toY = 0;

        this.graphPanel = $('#'+ this.config.id);
        this.graphPanelTools = $('#'+ this.config.toolsId);
        this.chartTable = $('#'+ this.config.chartTableId);


        if (!this.graphPanel[0] || !this.graphPanelTools[0] || !this.chartTable[0]) {
            return;
        }

        this.dragGraphPanelTools = false;
        var dragOffset = this.chartTable.offset();
        dragOffset.top += 5;
        dragOffset.left += 20;
        this.initialDragOffset = dragOffset;
        this.dragOffsetTop = this.initialDragOffset.top;
        this.dragOffset = {top:this.dragOffsetTop, left: this.initialDragOffset.left};

        this.mousedownTime;

        var _that = this;
        var _document = $(document);

        this.container.on('dblclick',function (event) {
            if (_that.validateDoubleClick(event)) {
                if (!_that.activate) {
                    _that.open();
                } else {
                    if (event.ctrlKey) {
                        _that.removeAll();
                    } else {
                        _that.back(true);
                    }
                }
            }
//            return false;
        });
        this.container.on('dblclick', 'canvas', function (event) {
            if (event.ctrlKey) {
                _that.removeAll();
            } else {
                _that.back(true);
            }
            return false;
        }).on('keyup', function (event) {
                var code = event.keyCode;
                if (code < 49 || code > 56){
                    return false;
                }
                var index = code - 49;
                _that.setColorIndex(index);
                return false;
            }).on('mousedown', function (event) {

                _that.mousedownTime = new Date().getTime();

                if (!_that.activate || !_that.validateClick(event)){
                    return ;
                }

                _that.setStartPosition(event.clientX, event.clientY);

                _that.setCurrentCanvas(document.createElement("canvas"));
                return false;
            }).on('mousemove', function (event) {

                if (_that.mousedownTime < 300){
                    return false;
                }

                if (!_that.activate){
                    return ;
                }
                _that.startDrawing = true;
                _that.setEndPosition(event.clientX, event.clientY);
                _that.clearCanvas();
                _that.drawLine();

                window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
                return false;
            }).on('mouseup', function (event) {
                _that.mousedownTime = new Date().getTime() - _that.mousedownTime;

                _that.setStartPosition(0, 0);
                _that.setEndPosition(0, 0);

                if (!_that.activate || !_that.startDrawing || !_that.validateClick(event)) {
                    _that.removeTempCanvas();
                    return false;
                }

                _that.pushLine();

                _that.removeTempCanvas();

                _that.startDrawing = false;
            });

        this.graphPanel.on('hover', function (event) {
            var _this = $(this);
            if (event.type == 'mouseenter') {
                _this.css('cursor', 'pointer');
                return;
            }

            if (event.type == 'mouseleave') {
                if (_that.activate) {
                    _this.css('cursor', 'crosshair');
                } else {
                    _this.css('cursor', 'auto');
                }
            }
        }).on('mousedown', function () {
                //for ie
                return false;
            }).on('mouseup', function () {
                //for ie
                return false;
            }).on('click', function () {
                if (_that.activate) {
                    _that.close();
                } else {
                    _that.setDragOffset(_that.initialDragOffset.top, _that.initialDragOffset.left);
                    _that.move();
                    _that.open();
                }
                return false;
            });

        this.graphPanelTools.on('hover', '.graph_panel_drag', function (hoverEvent) {
            var _this = $(this);
            if (hoverEvent.type == 'mouseenter') {

                _this.css('cursor', 'move');
                _this.find('span').show();

                _this.bind('mousedown', function (e) {
                    e.preventDefault();

                    _that.dragGraphPanelTools = true;
                    _that.activate = false;

                    _that.container.bind('mousemove', function (event) {

                        if (_that.dragGraphPanelTools){

                            if (_that.ie6Flag) {
                                _that.setDragOffsetTop(event.clientY);
                            }

                            _that.setDragOffset( event.clientY + _document.scrollTop(), event.clientX + _document.scrollLeft());
                            _that.move();
                            window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
                        }

                        event.stopPropagation();
                    });

                    _that.container.bind('mouseup', function (event) {

                        if (_that.dragGraphPanelTools){

                            _that.activate = true;
                            _that.dragGraphPanelTools = false;
                        }

                        event.stopPropagation();
                    });
                    e.stopPropagation();
                });
                return;
            }

            if (hoverEvent.type == 'mouseleave') {
                _this.css('cursor', 'crosshair');
                _this.find('span').hide();

                _this.unbind('mousedown');
            }
            hoverEvent.stopPropagation();
        }).on('hover', '.graph_panel_help', function (event) {
                var _this = $(this);
                if (event.type == 'mouseenter') {
                    _this.css('cursor', 'help');
                    _this.find('span').show();
                    return;
                }

                if (event.type == 'mouseleave') {
                    _this.find('span').hide();
                    _this.css('cursor', 'auto');
                }
            }).on('hover', '.graph_panel_line_colors li', function (event) {
                var _this = $(this);
                if (event.type == 'mouseenter') {
                    _this.css('cursor', 'pointer');
                    _this.bind('click', function () {
                        _that.setColorIndex(_this.index());
                    });
                    return;
                }

                if (event.type == 'mouseleave') {
                    _this.css('cursor', 'auto');
                    _this.unbind('click');
                }
            }).on('hover', '.graph_panel_back', function (event) {
                var _this = $(this);
                if (event.type == 'mouseenter') {
                    _this.addClass('graph_panel_back_hover');
                    _this.find('span').show();
                    _this.bind('click', function () {
                        _that.back();
                    });
                    return;
                }

                if (event.type == 'mouseleave') {
                    _this.find('span').hide();
                    _this.removeClass('graph_panel_back_hover');
                    _this.unbind('click');
                }
            }).on('hover', '.graph_panel_clear', function (event) {
                var _this = $(this);
                if (event.type == 'mouseenter') {
                    _this.addClass('graph_panel_clear_hover');
                    _this.find('span').show();
                    _this.bind('click', function () {
                        _that.removeAll();
                    });
                    return;
                }

                if (event.type == 'mouseleave') {
                    _this.find('span').hide();
                    _this.removeClass('graph_panel_clear_hover');
                    _this.unbind('click');
                }
            }).on('hover', '.graph_panel_close', function (event) {
                var _this = $(this);
                if (event.type == 'mouseenter') {
                    _this.addClass('graph_panel_close_hover');
                    _this.find('span').show();
                    _this.bind('click', function () {
                        _that.close();
                    });
                    return;
                }

                if (event.type == 'mouseleave') {
                    _this.find('span').hide();
                    _this.removeClass('graph_panel_close_hover');
                    _this.unbind('click');
                }
            });

        if (this.ie6Flag) {

            this.graphPanelTools.css('position','absolute');

            var scroll = function (){
                _that.setDragOffset(_that.dragOffsetTop + _document.scrollTop());
                _that.move();
            }
            $(window).on('scroll', scroll).on('resize', scroll);
        }
        this.move();
    }

    window.GraphPanel.prototype = {
        setDragOffset:function (top, left) {
            if (top) {
                this.dragOffset.top = top;
            }

            if (left) {
                this.dragOffset.left = left;
            }
        },
        setDragOffsetTop:function (top) {
            this.dragOffsetTop = top;
        },
        move:function () {
            this.graphPanelTools.offset(this.dragOffset);
        },
        open:function () {
            this.activate = true;
            this.startDrawing = false;
            this.container.css('cursor', 'crosshair');

            this.graphPanel.find('div').first().toggleClass('graph_panel_status_open');
            this.graphPanelTools.css('visibility', 'visible');
            this.container.children('canvas').show();
        },
        close:function () {
            this.activate = false;
            this.startDrawing = false;
            this.container.css('cursor', 'auto');

            this.graphPanel.find('div').first().toggleClass('graph_panel_status_open');
            this.graphPanelTools.css('visibility', 'hidden');
            this.container.children('canvas').hide();
        },
        setColorIndex:function (index) {
            this.colorIndex = index;
            $(".graph_panel_line_colors li").removeClass("graph_panel_line_color_choosed")
                .eq(index).addClass("graph_panel_line_color_choosed");
        },
        setStartPosition:function (x, y) {
            var _document = $(document);
            this.fromX = x + _document.scrollLeft();
            this.fromY = y + _document.scrollTop();
        },
        setEndPosition:function (x, y) {
            var _document = $(document);
            this.toX = x + _document.scrollLeft();
            this.toY = y + _document.scrollTop();
        },
        setCurrentCanvas:function (canvas) {
            this.currentCanvas = canvas;
            if ($.type(this.currentCanvas) != 'undefined') {

                if (!this.supportCanvas) {
                    G_vmlCanvasManager.initElement(this.currentCanvas);
                }

                this.currentCanvas.style.position = "absolute";
                this.currentCanvas.style.visibility = "visible";
                this.currentCanvas.style.top = "0px";
                this.currentCanvas.style.left = "0px";
                this.currentCanvas.style.width = this.container.width()+ "px";
                this.currentCanvas.style.height = this.container.height()+ "px";
                this.currentCanvas.style.zIndex = 9990;
                this.currentCanvas.setAttribute('width', this.container.width());
                this.currentCanvas.setAttribute('height', this.container.height());
                this.currentCanvas.setAttribute('id', 'graphLine' + this.canvasIdCounter++);

                if (!this.supportCanvas) {
                    this.container.append(this.currentCanvas);
                    this.tempCached.push(this.currentCanvas);
                }
            }
        },
        validateCurrentCanvas:function () {

            var result = false;

            if (this.currentCanvas && this.container.find('#' + this.currentCanvas.getAttribute("id"))[0]) {
                result = true;
            }

            return result;
        },
        validateClick:function (event) {

            var result = true;

            if (this.supportCanvas) {
                if (event.button != 0) {
                    result = false;
                }
            } else {
                if (event.button != 1) {
                    result = false;
                }
            }
            return result;
        },
        drawLine:function () {
            if ($.type(this.currentCanvas) != 'undefined') {

                if (this.fromX == 0 || this.fromY == 0 || this.toX == 0 || this.toY == 0) {
                    return;
                }

                var context = this.currentCanvas.getContext("2d");
                var color = this.config.colors[this.colorIndex];
                context.save();
                context.strokeStyle = color;
                context.fillStyle = this.config.colors[0] || color;

                context.lineWidth = this.config.strokeWidth;
                context.beginPath();
                context.moveTo(this.fromX, this.fromY);
                context.lineTo(this.toX, this.toY);
                context.closePath();
                context.stroke();
                context.restore();

                if (!this.supportCanvas) {
//                    this.tempCached.pop();
                } else {
                    this.container.append(this.currentCanvas);
                }
            }
        },
        pushLine:function () {
            this.tempCached.pop();

            if (this.validateCurrentCanvas()){
                this.cached.push(this.currentCanvas);
            }

//            if (this.supportCanvas) {
//               this.container.append(this.currentCanvas);
//            }

            this.setCurrentCanvas(undefined);
        },
        removeAll:function () {
            if (this.activate) {
                var tempCanvas;
                while ($.type(tempCanvas = this.cached.pop()) != 'undefined') {
                    this._removeHandler(tempCanvas);
                }
            }
        },
        back:function (close) {
            if (this.activate) {
                var previous = this.cached.pop();
                if ($.type(previous) != 'undefined') {
                    this._removeHandler(previous);
                } else {
                    if (close){
                        this.close();
                    }
                }
            }
        },
        clearCanvas:function () {
            if ($.type(this.currentCanvas) != 'undefined') {
                this.currentCanvas.getContext("2d").clearRect(0, 0, this.container.width(), this.container.height());
            }
        },
        removeTempCanvas:function () {
            if (this.supportCanvas) {
                if ($.type(this.currentCanvas) != 'undefined') {
                    $(this.currentCanvas).remove();
                }
            } else {
                var temp;
                while ($.type(temp = this.tempCached.pop()) != 'undefined'){
                    $(temp).remove();
                }
            }
        },
        _removeHandler:function (canvas) {
            var id = $(canvas).attr("id");
            this.container.find('#' + id).remove();
        },
        validateDoubleClick:function (event) {
            var result = true;
            var obj = document.elementFromPoint(event.clientX, event.clientY);
            if (obj != null && obj != document.body) {
                var parentDiv = $(obj).parents("div")[$(obj).parents("div").length - 1];
                var excludeDivIds = this.config.excludeDivIds;
                if (parentDiv && excludeDivIds.length > 0) {
                    for (var index in excludeDivIds) {
                        if (parentDiv.id == excludeDivIds[index]) {
                            result = false;
                            break;
                        }
                    }
                }
            }
            return result;
        }
    }
})();

(function () {
    var defaultIds = {
        chartTableId:'chartTable',
        phaseOrderId:'phaseOrder',
        omissionId:'omission',
        foldLineId:'foldLine',
        layerId:'layer',
        splitLineId:'splitLine',
        repeatNumberId:'repeatNumber',
        consecutiveNumberId:'consecutiveNumber',
        consecutiveNumberYId:'consecutiveNumberY',
        sideNumberId:'sideNumber',
        currentStatisticId:'currentStatistic',
        historyStatisticId:'historyStatistic'
    };

    var defaultClass = {
        omissionEntry:'omission_entry',
        hitClass:'omission_hit',
        repeatNumber:'repeat_number',
        sideNumber:'side_number',
        consecutiveNumber:'consecutive_number',
        threeConsecutiveNumber:'three_consecutive_number',
        emptyArea:'empty_area'
    };

    window.ChartTable = function ChartTable(params) {
        this.ids = $.extend(defaultIds, params.ids);
        this.omission = $('#' + this.ids.omissionId);
        this.foldLine = $('#' + this.ids.foldLineId);
        this.layer = $('#' + this.ids.layerId);
        this.splitLine = $('#' + this.ids.splitLineId);
        this.repeatNumber = $('#' + this.ids.repeatNumberId);
        this.consecutiveNumber = $('#' + this.ids.consecutiveNumberId);
        this.consecutiveNumberY = $('#' + this.ids.consecutiveNumberYId)
        this.sideNumber = $('#' + this.ids.sideNumberId);
        this.currentStatistic = $('#' + this.ids.currentStatisticId);
        this.historyStatistic = $('#' + this.ids.historyStatisticId);

        this.matrix = undefined;

        var _that = this;

        this.init = function () {
            _that.matrix = new Matrix(_that.ids.chartTableId, _that.ids.phaseOrderId,
                params.configList);
            _that.matrix.init();

            //bind event
            _that.omission.bind('click', _that.omissionEvent);

            _that.splitLine.bind('click', _that.splitLineEvent);

            _that.layer.bind('click', _that.layerEvent);

            _that.foldLine.bind('click', _that.foldLineEvent);

            _that.repeatNumber.bind('click', _that.repeatNumberEvent);

            _that.consecutiveNumber.bind('click', _that.consecutiveNumberEvent);

            _that.consecutiveNumberY.bind('click', _that.consecutiveNumberYEvent);

            _that.sideNumber.bind('click', _that.sideNumberEvent);

            _that.currentStatistic.bind('click', _that.statisticEvent);

            _that.historyStatistic.bind('click', _that.statisticEvent);
        };

        this.render = function () {

            if (_that.currentStatistic.prop('checked')) {
                $('.chart_table_tr_statistic').hide().slice(0, 4).show();
            }

            if (_that.historyStatistic.prop('checked')) {
                $('.chart_table_tr_statistic').hide().slice(4).show();
            }
            setTimeout(_that.splitLineEvent, 100);

            _that.threeConsecutiveNumberEvent();

            _that.emptyAreaEvent();
        };

        this.omissionEvent = function () {
            var isLayer = _that.layer.prop('checked');
            _that.matrix.omissionEvent(_that.omission.prop('checked'), isLayer);
        };

        this.splitLineEvent = function () {
            _that.matrix.splitLineEvent(_that.splitLine.prop('checked'));
            _that.foldLineEvent();
        };

        this.layerEvent = function () {
            var isOmission = _that.omission.prop('checked');
            _that.matrix.layerEvent(_that.layer.prop('checked'), isOmission);
        };

        this.foldLineEvent = function () {
            var index;
            if (_that.currentStatistic.prop('checked')) {
                index = 0;
                if (_that.splitLine.prop('checked')) {
                    index = 1;
                }
            }

            if (_that.historyStatistic.prop('checked')) {
                index = 2;
//                index = 0;
                if (_that.splitLine.prop('checked')) {
                    index = 3;
//                    index = 1;
                }
            }
            _that.matrix.foldLineEvent(_that.foldLine.prop('checked'), index);
        };

        this.repeatNumberEvent = function () {
            _that.matrix.repeatNumberEvent(_that.repeatNumber.prop('checked'));
        };

        this.consecutiveNumberEvent = function () {
            _that.matrix.consecutiveNumberEvent(_that.consecutiveNumber.prop('checked'));
        };

        this.consecutiveNumberYEvent = function () {
            _that.matrix.consecutiveNumberYEvent(_that.consecutiveNumberY.prop('checked'));
        };

        this.sideNumberEvent = function () {
            _that.matrix.sideNumberEvent(_that.sideNumber.prop('checked'));
        };

        this.statisticEvent = function () {
            if (_that.currentStatistic.prop('checked')) {
                $('.chart_table_tr_statistic').show().slice(4).hide();
            }

            if (_that.historyStatistic.prop('checked')) {
                $('.chart_table_tr_statistic').show().slice(0, 4).hide();
            }
            _that.foldLineEvent();
        };

        this.threeConsecutiveNumberEvent = function () {
            _that.matrix.threeConsecutiveNumberEvent(true);
        }

        this.emptyAreaEvent = function () {
            _that.matrix.emptyArea(true);
        }
    };

    function Matrix(chartTableId, phaseOrderId, configList) {
        this.chartTableId = chartTableId;
        //true: asc, false: desc
        this.phaseOrder = $('#' + phaseOrderId).hasClass('th_order_up');
        this.configList = configList;

        this._dataList = $('#' + chartTableId + ' tbody tr').not('.chart_table_tr_statistic').not('.chart_table_tr_statistic_current_omission');
        this._firstHeadThList = $('#' + chartTableId + ' thead tr:first th');
        this.columnLength = 0;
        this.rowLength = 0;

        this.borderRightIndexList = new Array();
        this.matrix = undefined;
        this.foldLineList = [new Array(), new Array(), new Array(), new Array()];


    }

    Matrix.prototype = {
        init:function () {
            this._columnLengthHandler();
            this._rowLengthHandler();
            this._borderRightHandler();
            this._currentOmissionHandler();
            this.matrix = new Array(this.columnLength);

            var foldLineConfigList = this.configList.foldLineConfigList;
            var repeatNumberConfigList = this.configList.repeatNumberConfigList;
            var consecutiveNumberConfigList = this.configList.consecutiveNumberConfigList;
            var consecutiveNumberYConfigList = this.configList.consecutiveNumberYConfigList;
            var sideNumberConfigList = this.configList.sideNumberConfigList;
            var threeConsecutiveNumberConfigList = this.configList.threeConsecutiveNumberConfigList;
            var emptyAreaConfigList = this.configList.emptyAreaConfigList;

            var _that = this;
            var matrixEntry;
            this._dataList.each(function (columnIndex) {
                var temp = new Array(_that.rowLength);
                $(this).children().each(function (rowIndex) {
                    matrixEntry = new MatrixEntry({
                        el:$(this),
                        columnIndex:columnIndex,
                        rowIndex:rowIndex,
                        foldLineConfig:_that._getConfig(rowIndex, foldLineConfigList),
                        repeatNumberConfig:_that._getConfig(rowIndex, repeatNumberConfigList),
                        consecutiveNumberConfig:_that._getConfig(rowIndex, consecutiveNumberConfigList),
                        consecutiveNumberYConfig:_that._getConfig(rowIndex, consecutiveNumberYConfigList),
                        sideNumberConfig:_that._getConfig(rowIndex, sideNumberConfigList),
                        threeConsecutiveNumberConfig:_that._getConfig(rowIndex, threeConsecutiveNumberConfigList),
                        emptyAreaConfig: _that._getConfig(rowIndex,emptyAreaConfigList)
                    });
                    matrixEntry.render();
                    temp[rowIndex] = matrixEntry;
                });
                _that.matrix[columnIndex] = temp;
            });
            this._repeatNumberHandler();
            this._consecutiveNumberHandler();
            this._sideNumberHandler();
            this._layerHandler();
            this._threeConsecutiveNumberHandler();

            if ($.type(emptyAreaConfigList) != "undefined") {
                this._emptyAreaHandler();
            }
            if ($.type(consecutiveNumberYConfigList) != "undefined") {
                this._consecutiveNumberYHandler();
            }
        },

        _loopMatrix:function (callback) {
            var matrix = this.matrix;
            var flag = $.type(callback) == 'function';
            for (var i = 0, columnLength = this.columnLength; i < columnLength; i++) {
                var rowList = matrix[i];
                for (var j = 0, rowLength = this.rowLength; j < rowLength; j++) {
                    var matrixEntry = rowList[j];
                    if (flag) {
                        var isBreak = callback(matrixEntry);
                        if ($.type(isBreak) == 'boolean') {
                            if (isBreak) {
                                break;
                            }
                        }
                    }
                }
            }
        },

        _loopMatrixByColumn:function (orderType, callback) {
            var matrix = this.matrix;
            var rowLength = this.rowLength;
            var columnLength = this.columnLength;
            var i;
            var j;
            var matrixEntry;
            var isBreak;
            var flag = $.type(callback) == 'function';
            if (orderType) {
                //asc
                for (i = 0; i < rowLength; i++) {
                    for (j = 0; j < columnLength; j++) {
                        matrixEntry = matrix[j][i];
                        if (flag) {
                            isBreak = callback(matrixEntry);
                            if ($.type(isBreak) == 'boolean') {
                                if (isBreak) {
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                //desc
                for (i = 0; i < rowLength; i++) {
                    j = columnLength;
                    while (j-- > 0) {
                        matrixEntry = matrix[j][i];
                        if (flag) {
                            isBreak = callback(matrixEntry);
                            if ($.type(isBreak) == 'boolean') {
                                if (isBreak) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        },

        _getConfig:function (rowIndex, configList) {
            if (!$.isEmptyObject(configList) && $.isArray(configList)) {
                for (var i = 0, length = configList.length; i < length; i++) {
                    var config = configList[i];
                    var indexRange = config.indexRange;
                    if (indexRange.length == 2 && indexRange[0] <= rowIndex && indexRange[1] >= rowIndex) {
                        return config;
                    }
                }
            }
            return undefined;
        },

        omissionEvent:function (display, isLayer) {
            var callback = function (matrixEntry) {
                matrixEntry.renderOmission(display, isLayer);
            };
            this._loopMatrix(callback);
        },

        splitLineEvent:function (display) {
            var chartTable = $('#' + this.chartTableId);
            chartTable.hide();
            var i = 4;
            var length = this.columnLength;
            while (i < length) {
                var rowList = this.matrix[i];
                for (var j = 0, rowLength = this.rowLength; j < rowLength; j++) {
                    var matrixEntry = rowList[j];
                    matrixEntry.renderBorderBottom(display);
                }
                i += 5;
            }
            chartTable.show();
        },

        layerEvent:function (display, isOmission) {
            var callback = function (matrixEntry) {
                matrixEntry.renderLayer(display, isOmission);
            };
            this._loopMatrix(callback);
        },

        foldLineEvent:function (display, index) {
            var foldLineList = this.foldLineList;
            if ($.isEmptyObject(foldLineList[index])) {
                this._foldLineHandler(index);
            }
            for (var k = 0, listLength = foldLineList.length; k < listLength; k++) {
                var flag = k == index;
                for (var i = 0, length = foldLineList[k].length; i < length; i++) {
                    if (display && flag) {
                        $(foldLineList[k][i]).show();
                    } else {
                        $(foldLineList[k][i]).hide();
                    }
                }
            }
        },

        repeatNumberEvent:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderRepeatNumber(display);
            };
            this._loopMatrix(callback);
        },

        consecutiveNumberEvent:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderConsecutiveNumber(display);
            };
            this._loopMatrix(callback);
        },

        consecutiveNumberYEvent:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderConsecutiveNumberY(display);
            };
            this._loopMatrix(callback);
        },

        sideNumberEvent:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderSideNumber(display);
            };
            this._loopMatrix(callback);
        },

        threeConsecutiveNumberEvent:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderThreeConsecutiveNumber(display);
            };
            this._loopMatrix(callback);
        },

        emptyArea:function (display) {
            var callback = function (matrixEntry) {
                matrixEntry.renderEmptyArea(display);
            }
            this._loopMatrix(callback);
        },

        _foldLineHandler:function (index) {
            var foldLineConfigList = this.configList.foldLineConfigList;

            if ($.isEmptyObject(foldLineConfigList) || $.type(foldLineConfigList) == 'undefined') {
                //do nothing
                return;
            }

            var ieFlag = $.browser.msie && /^[678]\./.test($.browser.version);

            var foldLineConfig = undefined;
            var indexRange = undefined;

            var previousMatrixEntry = undefined;
            var hitMatrixEntry = undefined;

            var tempArr = new Array();
            var tempIndex = 0;
            if (ieFlag) {
                var fragment = document.createDocumentFragment();
                if (!$.isEmptyObject(foldLineConfigList) && $.isArray(foldLineConfigList)) {
                    for (var i = 0, length = foldLineConfigList.length; i < length; i++) {

                        foldLineConfig = foldLineConfigList[i];
                        indexRange = foldLineConfig.indexRange;

                        previousMatrixEntry = undefined;
                        hitMatrixEntry = undefined;

                        for (var k = 0, columnLength = this.columnLength; k < columnLength; k++) {
                            var rowList = this.matrix[k];
                            for (var j = indexRange[0], rowLength = indexRange[1]; j <= rowLength; j++) {
                                hitMatrixEntry = rowList[j];
                                if (hitMatrixEntry.isHit && hitMatrixEntry.hasFoldLine()) {
                                    break;
                                }
                            }

                            if ($.type(hitMatrixEntry) != 'undefined') {
                                if ($.type(previousMatrixEntry) != 'undefined') {
                                    line = document.createElement("canvas");
                                    line.style.display = "none";
                                    fragment.appendChild(line);
                                    tempArr.push(line);
                                }

                                previousMatrixEntry = hitMatrixEntry;
                            }
                        }
                    }
                }
                $('#chartTableWrapper').append(fragment);
            }

            if (!$.isEmptyObject(foldLineConfigList) && $.isArray(foldLineConfigList)) {
                var foldLineList = this.foldLineList[index];
                for (var i = 0, length = foldLineConfigList.length; i < length; i++) {

                    foldLineConfig = foldLineConfigList[i];
                    indexRange = foldLineConfig.indexRange;

                    previousMatrixEntry = undefined;
                    hitMatrixEntry = undefined;
                    var previousOffset;
                    var hitOffset;
                    var hitFoldLineConfig;

                    var paperX = 0;
                    var paperY = 0;
                    var paperWidth = 0;
                    var paperHeight = 0;

                    var previousOffsetLeft;
                    var previousOffsetTop;
                    var hitOffsetLeft;
                    var hitOffsetTop;

                    var previousWidth;
                    var previousHeight;
                    var hitWidth;
                    var hitHeight;

                    var line;

                    var hitBorderLeftWidth;
                    var hitBorderTopWidth;
                    var previousBorderTopWidth;
                    var previousBorderLeftWidth;

                    var fromX, fromY, toX, toY;
                    for (var k = 0, columnLength = this.columnLength; k < columnLength; k++) {

                        var rowList = this.matrix[k];

                        for (var j = indexRange[0], rowLength = indexRange[1]; j <= rowLength; j++) {
                            hitMatrixEntry = rowList[j];
                            if (hitMatrixEntry.isHit && hitMatrixEntry.hasFoldLine()) {
                                hitOffset = hitMatrixEntry.el.position();
                                hitFoldLineConfig = hitMatrixEntry.foldLineConfig;

                                hitOffsetLeft = hitOffset.left;
                                hitOffsetTop = hitOffset.top;

                                hitWidth = hitMatrixEntry.el.width();
                                hitHeight = hitMatrixEntry.el.height();

                                hitBorderLeftWidth = parseInt(hitMatrixEntry.el.css("border-left-width"));
                                hitBorderTopWidth = parseInt(hitMatrixEntry.el.css("border-top-width"));
                                break;
                            }
                        }

                        if ($.type(hitMatrixEntry) != 'undefined') {
                            if ($.type(previousMatrixEntry) != 'undefined' && $.type(hitFoldLineConfig) != 'undefined') {
                                previousOffset = previousMatrixEntry.el.position();

                                paperX = 0;
                                paperY = 0;
                                paperWidth = 0;
                                paperHeight = 0;

                                if (previousOffsetLeft > hitOffsetLeft) {

                                    paperX = hitOffsetLeft + hitBorderLeftWidth + hitWidth - 4;
                                    paperY = previousOffsetTop + previousBorderTopWidth + previousHeight - 4;
                                    paperWidth = previousOffsetLeft - paperX + previousBorderLeftWidth + 4;
                                    paperHeight = hitOffsetTop - paperY + hitBorderTopWidth + 4;

                                    fromX = paperWidth - 1;
                                    fromY = 1;
                                    toX = 1;
                                    toY = paperHeight - 1;
                                }

                                if (previousOffsetLeft == hitOffsetLeft) {

                                    paperX = previousOffsetLeft + previousWidth / 2 + previousBorderLeftWidth;
                                    paperY = previousOffsetTop + previousBorderTopWidth + previousHeight - 4;
                                    paperWidth = foldLineConfig.strokeWidth * 2;
                                    paperHeight = hitOffsetTop - paperY + hitBorderTopWidth + 3;

                                    fromX = hitFoldLineConfig.strokeWidth;
                                    fromY = 1;
                                    toX = hitFoldLineConfig.strokeWidth;
                                    toY = paperHeight - 1;
                                }

                                if (previousOffsetLeft < hitOffsetLeft) {

                                    paperX = previousOffsetLeft + previousBorderLeftWidth + previousWidth - 4;
                                    paperY = previousOffsetTop + previousBorderTopWidth + previousHeight - 4;
                                    paperWidth = hitOffsetLeft - paperX + hitBorderLeftWidth + 4;
                                    paperHeight = hitOffsetTop - paperY + hitBorderTopWidth + 4;

                                    fromX = 1;
                                    fromY = 1;
                                    toX = paperWidth - 1;
                                    toY = paperHeight - 1;
                                }

                                if (paperHeight >= 0) {
                                    if (ieFlag) {
                                        line = tempArr[tempIndex++];
                                        G_vmlCanvasManager.initElement(line);
                                    } else {
                                        line = document.createElement("canvas");
                                        $('#chartTableWrapper').append(line);
                                    }
                                    line.width = paperWidth;
                                    line.height = paperHeight;
                                    line.style.position = "absolute";
                                    line.style.top = paperY + "px";
                                    line.style.left = paperX + "px";
                                    line.style.width = paperWidth + "px";
                                    line.style.height = paperHeight + "px";
                                    line.setAttribute('width', paperWidth);
                                    line.setAttribute('height', paperHeight);
                                    var context = line.getContext("2d");
                                    context.save();
                                    context.fillStyle = "#000000" || hitFoldLineConfig.stroke;
                                    context.strokeStyle = hitFoldLineConfig.stroke;
                                    context.lineWidth = hitFoldLineConfig.strokeWidth;
                                    context.beginPath();
                                    context.moveTo(fromX, fromY);
                                    context.lineTo(toX, toY);
                                    context.closePath();
                                    context.stroke();
                                    context.restore();

                                    foldLineList.push(line);
                                }
                            }

                            previousOffsetLeft = hitOffsetLeft;
                            previousOffsetTop = hitOffsetTop;

                            previousWidth = hitWidth;
                            previousHeight = hitHeight;

                            previousBorderTopWidth = hitBorderTopWidth;
                            previousBorderLeftWidth = hitBorderLeftWidth;

                            previousMatrixEntry = hitMatrixEntry;
                        }
                    }
                }
            }

            for (var i = 0; i < foldLineList.length; i++) {
                foldLineList[i].style.display = "block";
            }
        },

        _layerHandler:function () {
            var callback = function (matrixEntry) {
                if (matrixEntry.isOmissionEntry) {
                    if (matrixEntry.isHit) {
                        return true;
                    }
                } else {
                    return true;
                }
            };

            var matrix = this.matrix;
            var rowLength = this.rowLength;
            var columnLength = this.columnLength;
            var i;
            var j;
            var matrixEntry;
            var isBreak;
            var tempArray = new Array();
            var layer;
            if (this.phaseOrder) {
                //asc
                for (i = 0; i < rowLength; i++) {
                    j = columnLength;
                    tempArray.length = 0;
                    while (j-- > 0) {
                        matrixEntry = matrix[j][i];
                        isBreak = callback(matrixEntry);
                        if (isBreak) {
                            break;
                        }
                        tempArray.push(matrixEntry);
                    }
                    layer = tempArray.length;

                    if (layer > 0 && layer <= 11 && matrix.length <= 11) {
                        var entry = tempArray[0];

                        if (entry.hitValue > 5) {
                            layer = 6;
                        }

                        if (entry.hitValue > 11) {
                            layer = 12;
                        }
                    }

                    $.each(tempArray, function () {
                        this.setLayer(layer);
                    });
                }
            } else {
                //desc
                for (i = 0; i < rowLength; i++) {
                    tempArray.length = 0;
                    for (j = 0; j < columnLength; j++) {
                        matrixEntry = matrix[j][i];
                        isBreak = callback(matrixEntry);
                        if (isBreak) {
                            break;
                        }
                        tempArray.push(matrixEntry);
                    }
                    layer = tempArray.length;

                    if (layer > 0 && layer <= 11 && matrix.length <= 11) {
                        var entry = tempArray[0];

                        if (entry.hitValue > 5) {
                            layer = 6;
                        }

                        if (entry.hitValue > 11) {
                            layer = 12;
                        }
                    }

                    $.each(tempArray, function () {
                        this.setLayer(layer);
                    });
                }
            }
        },

        _repeatNumberHandler:function () {
            var hitValue = undefined;
            var callback = function (matrixEntry) {
                if ($.type(matrixEntry.repeatNumberConfig) != 'undefined') {
                    if (matrixEntry.isHit) {
                        if ($.type(hitValue) != 'undefined' && hitValue == matrixEntry.hitValue) {
                            matrixEntry.setRepeatNumber(true);
                        }
                        hitValue = matrixEntry.hitValue;
                    } else {
                        hitValue = undefined;
                    }
                } else {
                    return true;
                }
            };
            this._loopMatrixByColumn(this.phaseOrder, callback);
        },

        _consecutiveNumberHandler:function () {
            var previousEntry = undefined;
            var callback = function (matrixEntry) {
                if ($.type(matrixEntry.consecutiveNumberConfig) != 'undefined' && matrixEntry.isHit) {
                    if ($.type(previousEntry) != 'undefined') {
                        if (matrixEntry.columnIndex == previousEntry.columnIndex) {
                            if (matrixEntry.rowIndex == previousEntry.rowIndex + 1) {
                                matrixEntry.setConsecutiveNumber(true);
                            }
                        } else {
                            previousEntry = undefined;
                        }
                    }
                    previousEntry = matrixEntry;
                }
            };
            this._loopMatrix(callback);
        },

        _consecutiveNumberYHandler:function () {
            var matrix = this.matrix;
            var columnLength = this.columnLength;
            var rowLength = this.rowLength;
            var callback = function (matrixEntry) {
                var flag = true;
                for (var i = matrixEntry.columnIndex + 1; i < columnLength; i++) {
                    var rowList = matrix[i];
                    if (flag) {
                        flag = false;
                        for (var j = 0; j < rowLength; j++) {
                            var tempMatrixEntry = rowList[j];
                            if (tempMatrixEntry.isOmissionEntry) {
                                if (matrixEntry.el[0].innerHTML == tempMatrixEntry.el[0].innerHTML) {
                                    matrixEntry.setConsecutiveYNumber(true);
                                    tempMatrixEntry.setConsecutiveYNumber(true);
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    } else {
                        break;
                    }
                }
            }
            this._loopMatrix(callback)
        },

        _sideNumberHandler:function () {
            var _that = this;
            var callback = function (matrixEntry) {
                if ($.type(matrixEntry.sideNumberConfig) != 'undefined') {
                    if (matrixEntry.isHit) {
                        var columnIndex = matrixEntry.columnIndex;
                        var previousColumnIndex = _that.phaseOrder ? columnIndex - 1 : columnIndex + 1;
                        if (previousColumnIndex >= 0 && previousColumnIndex < _that.columnLength) {

                            var rowIndex = matrixEntry.rowIndex;
                            var hitValue = matrixEntry.hitValue;

                            var indexDifferenceList = [-1, 1];
                            for (var i = 0, length = indexDifferenceList.length; i < length; i++) {
                                var previousRowIndex = rowIndex - indexDifferenceList[i];
                                if (previousRowIndex > 0) {
                                    var previousMatrixEntry = _that.matrix[previousColumnIndex][previousRowIndex];
                                    if ($.type(previousMatrixEntry.sideNumberConfig) != 'undefined' &&
                                        previousMatrixEntry.isHit) {
                                        if (Math.abs(previousMatrixEntry.hitValue - hitValue) == 1) {
                                            matrixEntry.setSideNumber(true);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return true;
                }
            };
            this._loopMatrixByColumn(!this.phaseOrder, callback);
        },

        _columnLengthHandler:function () {
            this.columnLength = this._dataList.length;
        },

        _rowLengthHandler:function () {
            var _that = this;
            this._firstHeadThList.each(function () {
                var colspan = $(this).attr('colspan');
                _that.rowLength += ($.type(colspan) != 'undefined' && colspan > 0) ? parseInt(colspan) : 1;
            });
        },

        _borderRightHandler:function () {
            var tempCount = 0;
            var _that = this;
            this._firstHeadThList.each(function () {
                var colspan = $(this).attr('colspan');
                tempCount += ($.type(colspan) != 'undefined' && colspan > 0) ? parseInt(colspan) : 1;
                _that.borderRightIndexList.push(tempCount - 1);
            });
            this.borderRightIndexList.pop();


            $('#' + this.chartTableId + ' tbody tr').each(function () {
                $(this).children().each(function (rowIndex) {
                    if ($.inArray(rowIndex, _that.borderRightIndexList) > -1) {
                        $(this).addClass('border_right');
                    }
                });
            });
        },

        _currentOmissionHandler:function () {
            var currentOmissionConfigList = this.configList.currentOmissionConfigList;
            if ($.type(currentOmissionConfigList) != 'undefined') {
                var _that = this;
                $('.chart_table_tr_statistic_current_omission').children().each(function (index) {
                    var currentOmissionConfig = _that._getConfig(index, currentOmissionConfigList);
                    var _this = $(this);
                    if ($.type(currentOmissionConfig) != 'undefined') {
                        var value = parseInt(_this.text());
                        if (value > 75) {
                            _this.html(value + '<br/><img height="50px" width="8px" src="https://caipiao.baidu.com/trend/static/images/js/chart_table/' + currentOmissionConfig.color + '_pillar.gif">');
                        } else {
                            _this.html(value + '<br/><img height="' + value + 'px" width="8px" src="https://caipiao.baidu.com/trend/static/images/js/chart_table/' + currentOmissionConfig.color + '_pillar.gif">');
                        }
                    } else {
                        if (index > 0) {
                            _this.empty();
                        }
                    }
                });
            }
        },

        _threeConsecutiveNumberHandler:function () {
            var _that = this;

            var matrixColumnLength = this.columnLength;
            var matrixRowLength = this.rowLength;

            var rowIndex, columnIndex;

            var callbackHandler = function (matrixEntry, previousColumnIndex, previousRowIndex, nextColumnIndex, nextRowIndex) {

                if (previousColumnIndex >= 0 && previousRowIndex >= 0 &&
                    nextColumnIndex < matrixColumnLength && nextRowIndex < matrixRowLength) {

                    if (_that.matrix[previousColumnIndex][previousRowIndex].isHit && _that.matrix[nextColumnIndex][nextRowIndex].isHit) {

                        matrixEntry.setThreeConsecutiveNumber(true);

                        if ($.type(_that.matrix[previousColumnIndex][previousRowIndex].threeConsecutiveNumberConfig) != 'undefined') {
                            _that.matrix[previousColumnIndex][previousRowIndex].setThreeConsecutiveNumber(true);
                        }

                        if ($.type(_that.matrix[nextColumnIndex][nextRowIndex].threeConsecutiveNumberConfig) != 'undefined') {
                            _that.matrix[nextColumnIndex][nextRowIndex].setThreeConsecutiveNumber(true);
                        }
                    }
                }
            };

            var callback = function (matrixEntry) {

                if ($.type(matrixEntry.threeConsecutiveNumberConfig) != 'undefined' && matrixEntry.isHit) {

                    columnIndex = matrixEntry.columnIndex;
                    rowIndex = matrixEntry.rowIndex;

                    callbackHandler(matrixEntry, columnIndex - 1, rowIndex, columnIndex + 1, rowIndex);
                    callbackHandler(matrixEntry, columnIndex, rowIndex - 1, columnIndex, rowIndex + 1);
                    callbackHandler(matrixEntry, columnIndex - 1, rowIndex - 1, columnIndex + 1, rowIndex + 1);
                    callbackHandler(matrixEntry, columnIndex - 1, rowIndex + 1, columnIndex + 1, rowIndex - 1);
                }
            };
            this._loopMatrix(callback);
        },

        _emptyAreaHandler:function () {
            var _that = this;

            var callback = function (matrixEntry) {
                if ($.type(matrixEntry.emptyAreaConfig) != 'undefined' && !matrixEntry.isHit && !matrixEntry.isEmptyArea) {
                    var matrixEntryCached = new Array();
                    var indexRange = matrixEntry.emptyAreaConfig.indexRange;
//                    var columnLength = _that.columnLength;
//                    var columnIndex = 0;
                    var columnIndex = matrixEntry.columnIndex;
                    var missCount;

//                    while (columnIndex < columnLength) {
                        missCount = 0;
                        matrixEntryCached.length = 0;

                        for (var rowIndex = indexRange[0]; rowIndex <= indexRange[1]; rowIndex++) {
                            var matrixEntry = _that.matrix[columnIndex][rowIndex];
                            if (matrixEntry.isHit) {
                                break;
                            } else {
                                missCount++;
                                matrixEntryCached.push(matrixEntry);
                            }
                        }

                        if (missCount === (indexRange[1] - indexRange[0] + 1)) {
                            for(var index = 0; index < missCount; index++) {
                                matrixEntryCached[index].setEmptyArea(true);
                            }
                        }
//                        columnIndex++;
//                    }
                }
            }
            this._loopMatrix(callback);
        }
    };

    function MatrixEntry(params) {
        this.el = params.el;
        this.children = this.el.find('div').detach();
        this.columnIndex = params.columnIndex;
        this.rowIndex = params.rowIndex;

        this.hitValue = $.trim(this.el.text());
        this.isHit = this.el.hasClass(defaultClass['hitClass']) ? true : false;
        this.isOmissionEntry = this.el.hasClass(defaultClass['omissionEntry']);


        this.tdShowColor = this.el.css('color');
        this.tdHideColor = $.browser.msie ? '#F3F2F2' : 'transparent';
        this.currentTdCss = undefined;
        this.foldLineConfig = params.foldLineConfig;
        this.repeatNumberConfig = params.repeatNumberConfig;
        this.consecutiveNumberConfig = params.consecutiveNumberConfig;
        this.consecutiveNumberYConfig = params.consecutiveNumberYConfig;
        this.sideNumberConfig = params.sideNumberConfig;
        this.threeConsecutiveNumberConfig = params.threeConsecutiveNumberConfig;
        this.emptyAreaConfig = params.emptyAreaConfig;

        this.layerCss = undefined;
        this.isRepeatNumber = false;
        this.isConsecutiveNumber = false;
        this.isConsecutiveNumberY = false;
        this.isSideNumber = false;
        this.isThreeConsecutiveNumber = false;
        this.isEmptyArea = false;
    }

    MatrixEntry.prototype = {
        render:function () {
            if (this.isOmissionEntry && this.isHit && this.children.length > 0) {
                this.el.empty();
                this.el.append(this.children).append(this.hitValue);
            }
        },

        renderBorderBottom:function (display) {
            if (display) {
                this.el.addClass('border_bottom');
            } else {
                this.el.removeClass('border_bottom');
            }
        },

        renderOmission:function (display, isLayer) {
            var isEmptyArea = this.isEmptyArea;
            if (this.isOmissionEntry && !this.isHit) {
                if (isLayer) {
                    if ($.type(this.currentTdCss) == 'undefined') {
                        if (display) {
                            if(isEmptyArea){
                                this.el.removeAttr("style");
                            } else {
                                this.el.css('color', this.tdShowColor);
                            }
                        } else {
                            this.el.css('color', this.tdHideColor);
                        }
                    }
                } else {
                    if (display) {
                        if(isEmptyArea){
                            this.el.removeAttr("style");
                        } else {
                            this.el.css('color', this.tdShowColor);
                        }
                    } else {
                        this.el.css('color', this.tdHideColor);
                    }
                }
            }
        },

        renderLayer:function (display, isOmission) {
            if (this.isOmissionEntry && $.type(this.layerCss) != 'undefined') {
                if (display) {
                    this.currentTdCss = {'color':this.el.css('color'), 'background-color':this.el.css('background-color')};
                    this.el.css(this.layerCss);
                } else {
                    if (isOmission) {
                        if (this.isEmptyArea) {
                            this.el.removeAttr("style");
                        } else {
                            this.el.css(this.currentTdCss);
                            this.el.css( {'color': this.tdShowColor});
                        }
                    } else {
                        this.el.css(this.currentTdCss);
                        this.el.css( {'color': this.tdHideColor});
                    }
                }
            }
        },

        renderRepeatNumber:function (display) {
            if (this.isRepeatNumber) {
                var classes = this.repeatNumberConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['repeatNumber'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['repeatNumber'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        renderConsecutiveNumber:function (display) {
            if (this.isConsecutiveNumber) {
                var classes = this.consecutiveNumberConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['consecutiveNumber'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['consecutiveNumber'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        renderConsecutiveNumberY:function (display) {
            if (this.isConsecutiveNumberY) {
                var classes = this.consecutiveNumberYConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['consecutiveNumber'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['consecutiveNumber'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        renderSideNumber:function (display) {
            if (this.isSideNumber) {
                var classes = this.sideNumberConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['sideNumber'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['sideNumber'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        renderThreeConsecutiveNumber:function (display) {
            if (this.isThreeConsecutiveNumber) {
                var classes = this.threeConsecutiveNumberConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['threeConsecutiveNumber'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['threeConsecutiveNumber'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        renderEmptyArea:function (display) {
            if (this.isEmptyArea) {
                var classes = this.emptyAreaConfig.classes;
                var i;
                var length;
                var clazz;
                if (display) {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.addClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['emptyArea'];
                        this.el.addClass(clazz);
                    }
                } else {
                    if ($.type(classes) != 'undefined' && $.isArray(classes) && classes.length > 0) {
                        for (i = 0, length = classes.length; i < length; i++) {
                            clazz = classes[i];
                            this.el.removeClass(clazz);
                        }
                    } else {
                        clazz = defaultClass['emptyArea'];
                        this.el.removeClass(clazz);
                    }
                }
            }
        },

        setLayer:function (layer) {
            if (layer >= 1 && layer <= 5) {
                this.layerCss = {'color':'#5B0000', 'background-color':'#C8C8C8'};
            } else if (layer >= 6 && layer <= 11) {
                this.layerCss = {'color':'blue', 'background-color':'#D5D5D5'};
            } else {
                this.layerCss = {'color':'red', 'background-color':'#E7E7E7'};
            }
        },

        setRepeatNumber:function (isRepeatNumber) {
            this.isRepeatNumber = isRepeatNumber;
        },

        setConsecutiveNumber:function (isConsecutiveNumber) {
            this.isConsecutiveNumber = isConsecutiveNumber;
        },

        setConsecutiveYNumber:function (isConsecutiveNumberY) {
            this.isConsecutiveNumberY = isConsecutiveNumberY;
        },

        setSideNumber:function (isSideNumber) {
            this.isSideNumber = isSideNumber;
        },

        hasFoldLine:function () {
            return $.type(this.foldLineConfig) != 'undefined';
        },

        setThreeConsecutiveNumber:function (isThreeConsecutiveNumber) {
            this.isThreeConsecutiveNumber = isThreeConsecutiveNumber;
        },

        setEmptyArea:function (isEmptyArea) {
            this.isEmptyArea = isEmptyArea;
        }
    };
})();

(function () {
    var defaultIds = {
        chartTableId:'chartTable',
        outerRingId:'outerRing',
        diagonalId:'diagonal',
        fourAreaPointId:'fourAreaPoint',
        consecutiveNumberId:'consecutiveNumber',
        sixCoverId:'sixCover',
        omissionId:'omission'

    };

    var defaultClass = {
        paintDefault:'matrix_paint_default',
        paintFourArea:'matrix_paint_four_area',
        paintNumber:'matrix_paint_number'
    };
    window.ChartTableMatrix = function ChartTableMatrix(ids) {
        this.ids = $.extend(defaultIds, ids);
        this.matrixArr = new Array();
        this.outerRing = $('#' + this.ids.outerRingId);
        this.diagonal = $('#' + this.ids.diagonalId);
        this.fourArea = $('#' + this.ids.fourAreaPointId);
        this.consecutive = $('#' + this.ids.consecutiveNumberId);
        this.sixCover = $('#' + this.ids.sixCoverId);
        this.omission = $('#' + this.ids.omissionId);
        var _that = this;
        this.init = function () {
            $('#' + this.ids.chartTableId).children('tbody').children('tr').each(function () {
                $(this).children('td').each(function () {
                    var matrix = new Matrix($(this).children('table'));
                    matrix.init();
                    _that.matrixArr.push(matrix);
                });
            });

            //bind event
            _that.outerRing.bind('click', _that.outerRingEvent);
            _that.diagonal.bind('click', _that.diagonalEvent);
            _that.fourArea.bind('click', _that.fourAreaEvent);
            _that.consecutive.bind('click', _that.consecutiveEvent);
            _that.sixCover.bind('click', _that.sixCoverEvent);
            _that.omission.bind('click', _that.omissionEvent);
            _that.omissionEvent();
        };
        this.outerRingEvent = function () {
            if (_that.outerRing.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].outerRing();
                }
            }
        };
        this.diagonalEvent = function () {
            if (_that.diagonal.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].diagonal();
                }
            }
        };
        this.fourAreaEvent = function () {
            if (_that.fourArea.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].fourArea();
                }
            }
        };
        this.consecutiveEvent = function () {
            if (_that.consecutive.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].consecutive();
                }
            }
        };

        this.sixCoverEvent = function () {
            if (_that.sixCover.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].cover();
                }
            }
        };

        this.omissionEvent = function () {
            if (_that.omission.prop('checked')) {
                var matrixArr = _that.matrixArr;
                for (var i = 0, length = matrixArr.length; i < length; i++) {
                    matrixArr[i].revert();
                    matrixArr[i].omission();
                }
            }
        };
    };

    function Matrix(el) {
        this.el = el;
        this.matrix = undefined;
        this.rowLength = undefined;
        this.columnLength = undefined;
    }

    Matrix.prototype = {
        init:function () {
            var tempColumns = this.el.children('tbody').children('tr');
            this.columnLength = tempColumns.length - 1;
            var matrix = new Array();
            var _that = this;
            tempColumns.each(function () {
                var _this = $(this);
                if (_this.hasClass('tr_phase')) {
                    _that.rowLength = _this.children('td').attr('colspan');
                } else {
                    var row = new Array();
                    _this.children('td').each(function () {
                        row.push(new MatrixEntry($(this)));
                    });
                    matrix.push(row);
                }
            });
            this.matrix = matrix;
        },
        revert:function () {
            for (var i = 0; i < this.columnLength; i++) {
                for (var j = 0; j < this.rowLength; j++) {
                    this.matrix[i][j].revert();
                }
            }
        },
        outerRing:function () {
            var paintClass = defaultClass['paintDefault'];
            for (var i = 1; i < this.rowLength - 1; i++) {
                this.matrix[0][i].paint(paintClass, true);
                this.matrix[this.columnLength - 1][i].paint(paintClass, true);
            }
            for (i = 0; i < this.columnLength; i++) {
                this.matrix[i][0].paint(paintClass, true);
                this.matrix[i][this.rowLength - 1].paint(paintClass, true);
            }
        },
        diagonal:function () {
            var paintClass = defaultClass['paintDefault'];
            for (var i = 0, j = this.rowLength - 1; i < this.columnLength; i++, j--) {
                this.matrix[i][i].paint(paintClass, true);
                this.matrix[i][j].paint(paintClass, true);
            }
        },
        fourArea:function () {
            var paintClass = defaultClass['paintFourArea'];
            this.matrix[1][1].paint(paintClass, false);
            this.matrix[1][4].paint(paintClass, false);
            this.matrix[4][1].paint(paintClass, false);
            this.matrix[4][4].paint(paintClass, false);
        },
        consecutive:function () {
            var paintClass = defaultClass['paintNumber'];
            for (var i = 0; i < this.columnLength; i++) {
                for (var j = 0; j < this.rowLength; j++) {
                    if (this.matrix[i][j].isHit) {
                        if (j - 1 >= 0 && this.matrix[i][j - 1].isHit) {
                            this.matrix[i][j].paint(paintClass, false);
                        }
                        if (i - 1 >= 0 && this.matrix[i - 1][j].isHit) {
                            this.matrix[i][j].paint(paintClass, false);
                        }
                        if (j + 1 < this.rowLength && this.matrix[i][j + 1].isHit) {
                            this.matrix[i][j].paint(paintClass, false);
                        }
                        if (i + 1 < this.columnLength && this.matrix[i + 1][j].isHit) {
                            this.matrix[i][j].paint(paintClass, false);
                        }
                    }
                }
            }
        },
        cover:function () {
            var paintClass = defaultClass['paintNumber'];
            var i = 0, j = 0, x = 0, y = 0, counter = 0;
            while (i + 1 < this.columnLength) {
                while (j + 2 < this.rowLength) {
                    for (x = 0; x < 2; x++) {
                        for (y = 0; y < 3; y++) {
                            if (this.matrix[i + x][j + y].isHit) {
                                counter++;
                            }
                        }
                    }

                    if (counter >= 3) {
                        for (x = 0; x < 2; x++) {
                            for (y = 0; y < 3; y++) {
                                this.matrix[i + x][j + y].paint(paintClass, false);
                            }
                        }
                    }
                    counter = 0;
                    j++;
                }
                i++;
                j = 0;
            }

            i = 0;
            j = 0;
            counter = 0;
            while (i + 2 < this.columnLength) {
                while (j + 1 < this.rowLength) {
                    for (x = 0; x < 3; x++) {
                        for (y = 0; y < 2; y++) {
                            if (this.matrix[i + x][j + y].isHit) {
                                counter++;
                            }
                        }
                    }

                    if (counter >= 3) {
                        for (x = 0; x < 3; x++) {
                            for (y = 0; y < 2; y++) {
                                this.matrix[i + x][j + y].paint(paintClass, false);
                            }
                        }
                    }
                    counter = 0;
                    j++;
                }
                i++;
                j = 0;
            }
        },
        omission:function () {
            var paintClass = defaultClass['paintDefault'];
            var hitFlag = false;
            var i, j;
            for (i = 0; i < this.columnLength; i++) {
                for (j = 0; j < this.rowLength; j++) {
                    if (this.matrix[i][j].isHit) {
                        hitFlag = true;
                        break;
                    }
                }
                if (!hitFlag) {
                    for (j = 0; j < this.rowLength; j++) {
                        this.matrix[i][j].paint(paintClass, true);
                    }
                }
                hitFlag = false;
            }

            for (j = 0; j < this.rowLength; j++) {
                for (i = 0; i < this.columnLength; i++) {
                    if (this.matrix[i][j].isHit) {
                        hitFlag = true;
                        break;
                    }
                }
                if (!hitFlag) {
                    for (i = 0; i < this.columnLength; i++) {
                        this.matrix[i][j].paint(paintClass, true);
                    }
                }
                hitFlag = false;
            }
        }
    };

    function MatrixEntry(el) {
        this.el = el;
        this.paintClassCached = undefined;
        this.isBlank = el.html() == '';
        this.isHit = this.el.hasClass('red');
    }

    MatrixEntry.prototype = {
        paint:function (paintClass, isPaintBlank) {
            if (!this.isBlank) {
                this.paintClassCached = paintClass;
                this.el.addClass(paintClass);
            }

            if (this.isBlank && isPaintBlank) {
                this.paintClassCached = paintClass;
                this.el.addClass(paintClass);
            }
        },
        revert:function () {
            if ($.type(this.paintClassCached) != 'undefined') {
                this.el.removeClass(this.paintClassCached);
            }
        }
    };
})();

//search
function checkPhase(min,max){
    var startPhase = $("#startPhase").val();
    var endPhase = $("#endPhase").val();
    startPhase = $.trim(startPhase);
    endPhase = $.trim(endPhase);
    if(isEmpty(startPhase) || isEmpty(endPhase)){
        $("#startPhase").val("");
        $("#endPhase").val("");
        alert("期数不能为空！");
        return false;
    }
    if(isNaN(startPhase) || isNaN(endPhase)) {
        $("#startPhase").val("");
        $("#endPhase").val("");
        alert("请输入正确的期数值！");
        return false;
    }
    if(!isInteger(startPhase) || !isInteger(endPhase)){
        $("#startPhase").val("");
        $("#endPhase").val("");
        alert("请输入整数！");
        return false;
    }

    if(startPhase.length >= max ||startPhase.length <=min || endPhase.length >= max || endPhase.length <= min){
        $("#startPhase").val("");
        $("#endPhase").val("");
        alert("请输入正确的期数格式！");
        return false;
    }

    $("#startPhase").val(startPhase);
    $("#endPhase").val(endPhase);
    $("#searchForm").submit();
}

isEmpty = function(str) {
    if (str == "") {
        return true;
    }
    var regu = /^[ ]+$/;
    var re = new RegExp(regu);
    return re.test(str);
}

isInteger = function(str){
    var regu = /^\+?[0-9]+$/;
    var re = new RegExp(regu);
    return re.test(str);
}

