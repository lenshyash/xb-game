<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="${base }/common/template/lottery/trendChart/images/favicon_bdcp.ico?v=4.9.137" />
<!-- ok -->
<link rel="stylesheet" href="${base }/common/template/lottery/trendChart/css/common.css" type="text/css" media="screen" />
<style type="text/css">
	.red{
		color: red !important;
	}
</style>
<!-- ok -->
<script type="text/javascript" src="${base }/common/template/lottery/trendChart/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="${base }/common/template/lottery/trendChart/js/underscore/underscore-min.js"></script>
<script type="text/javascript">
	$.extend({
		getStrResouce: function (url){
			var resourceContent = "";
			$.ajax({
				type : "GET",
				url : url,
				dataType : "text",
				data : {},
				async : false, // 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。注意，同步请求将锁住浏览器，用户其它操作必须等待请求完成才可以执行。
				success : function(result) {
					resourceContent = result;
				}
			});
			return resourceContent;
		}
	});
	_.templateSettings = {
		evaluate : /\{#([\s\S]+?)#\}/g,
		interpolate : /\{\{(.+?)\}\}/g
	};

	// 对Date的扩展，将 Date 转化为指定格式的String
	// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
	// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
	// 例子：
	// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
	// (new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
	Date.prototype.format = function(fmt) { // author: meizz
		fmt = fmt || "yyyy-MM-dd HH:mm:ss";
		var o = {
			"M+" : this.getMonth() + 1, // 月份
			"d+" : this.getDate(), // 日
			"HH+" : this.getHours(), // 小时
			"m+" : this.getMinutes(), // 分
			"s+" : this.getSeconds(), // 秒
			"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
			"S" : this.getMilliseconds()
		// 毫秒
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
						: (("00" + o[k]).substr(("" + o[k]).length)));
		return fmt;
	}
</script>

<title>走势图_走势分析-彩票</title>
<!-- ok -->
<link rel="stylesheet" href="${base }/common/template/lottery/trendChart/css/hera.css" type="text/css" media="screen" />
<!-- ok -->
<link rel="stylesheet" href="${base }/common/template/lottery/trendChart/css/base.css" type="text/css" media="screen" />
<!-- ok -->
<link rel="stylesheet" href="${base }/common/template/lottery/trendChart/css/chart_table.css?v=111" type="text/css" media="screen" />
</head>

<body>
	<div class="main">
		<div class="top_new_nav">
			<!-- 
			<div class="szc_nav_container">
				<ul>
					<li class="szc_nav_bg"></li>
					<li id="ssq"><a href="/ssq/">分分彩</a></li>
					<li id="dlt"><a href="/dlt/">二分彩</a></li>
					<li id="fc3d"><a href="/fc3d/">五分彩</a></li>
					<li id="qxc"><a href="/qxc/">重庆时时彩</a></li>
					<li id="pl3"><a href="/pl3/">新疆时时彩</a></li>
					<li id="hd15x5"><a href="/hd15x5/">天津时时彩</a></li>
					<li id="qlc"><a href="/qlc/">北京塞车</a></li>
					<li id="pl5"><a href="/pl5/">广东11选5</a></li>
					<li id="df6j1"><a href="/df6j1/">山东11选5</a></li>
					<li id="tc22x5"><a href="/tc22x5/">江西11选5</a></li>
					<li id="bjtc33x7"><a href="/bjtc33x7/">上海11选5</a></li>
					<li id="nyfchc1"><a href="/nyfchc1/">福彩3D</a></li>
					<li id="nyfchc1"><a href="/nyfchc1/">排列3</a></li>
				</ul>
			</div>
			 -->

			<ul class="gpc_nav_container" style="border-bottom: 1px #DBDBDB solid;display: flex;">
				<li class="gpc_nav_bg"></li>
				<li class="gpc_ul_list" style="width:850px;">
					<div class="bottom_dashed">
						<ul>
							<li></li>
							<c:forEach items="${lotList }" var="lot" varStatus="sta">
							<c:if test="${sta.index<11 }"><li id="${lot.code }"><a href="${base }/lottery/trendChart/index.do?lotCode=${lot.code}">${lot.name}</a></li></c:if>
							</c:forEach>
						</ul>
					</div>
					<c:if test="${lotList.size()>11}"><ul>
						<li></li>
						<c:forEach items="${lotList }" var="lot" varStatus="sta">
						<c:if test="${sta.index>=11 }"><li id="${lot.code }"><a href="${base }/lottery/trendChart/index.do?lotCode=${lot.code}">${lot.name}</a></li></c:if>
						</c:forEach>
					</ul></c:if>
				</li>
			</ul>
		</div>
	</div>
	<script type="text/javascript">
		(function() {
			jQuery('#${lotCode}').addClass('on');
		})();
	</script>
	<div class="lottery_nav">
		<div class="nav_top">
			<h1>${lotName }走势图</h1>
			<ul>
				<li class="on"><h2><a href="javascript://">号码走势</a></h2></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="nav_main">
			<ul class="analysis_list" id="trendchart_menu_1"></ul>
			<div class="clear"></div>
		</div>
	</div>
	<div class="chart_table_search_wrapper">
		<div class="search_left">
			<ul id="trendchart_menu_2"></ul>
		</div>
		<div class="search_right">
			<form action="/pk10/firstPlaceTrend.action">
				<input name="phaseOrder" type="hidden" value="up"> <input
					name="onlyBody" type="hidden" value="false"> <input
					name="button" type="button" value="刷新"
					onclick="window.location.reload()" class="refresh_button">
			</form>
		</div>
		<div class="select_by_day">
			<span>按天数：</span> <select id="selectRecentDay" onchange="window.location.href='${base }/lottery/trendChart/index.do?lotCode=${lotCode}&recentDay=' + this.value + '&order=${order }';">
				<option value="">请选择</option>
				<option value="1" ${recentDay == 1 ? "selected" : ""}>最近一天</option>
				<option value="2" ${recentDay == 2 ? "selected" : ""}>最近两天</option>
				<option value="3" ${recentDay == 3 ? "selected" : ""}>最近三天</option>
				<option value="5" ${recentDay == 5 ? "selected" : ""}>最近五天</option>
			</select>
			<a href="${base }/lottery/trendChart/index.do?lotCode=${lotCode}&rows=30&order=${order }">最近30期</a>&nbsp;&nbsp;&nbsp;
			<a href="${base }/lottery/trendChart/index.do?lotCode=${lotCode}&rows=50&order=${order }">最近50期</a>&nbsp;&nbsp;&nbsp;
			<a href="${base }/lottery/trendChart/index.do?lotCode=${lotCode}&rows=100&order=${order }">最近100期</a>
		</div>
	</div>
	<div id="chartTableWrapper" class="chart_table_wrapper">
	</div>
	<div class="parameter_draw_wrapper">
	</div>

	<script id="trendchart_menu_1_temp" type="text/html" style="display: none;">
					{# 
						_.map(list, function(item, index){
					 #}
						<li val="{{item.value}}"><a href="javascript: void(0);">{{item.name}}</a></li>
						{{index != list.length - 1 ? '<li class="line">|</li>' : '' }}
					{#
						});
					#}
	</script>
	<script id="trendchart_menu_2_temp" type="text/html" style="display: none;">
					{# 
						_.map(list, function(item, index){
					 #}
						<a gamename="{{item.name}}" index="{{index}}" val="{{item.value}}" href="javascript: void(0);">{{item.name}}</a>
						{{index != list.length - 1 ? '|' : '' }}
					{#
						});
					#}
	</script>
	<script type="text/javascript" src="${base }/common/template/lottery/trendChart/js/chart_table.js"></script>
	<script type="text/javascript" src="${base }/common/template/lottery/trendChart/js/trendchart_menu.js"></script>
	<script type="text/javascript" src="${base }/common/template/lottery/trendChart/js/trendchart_charttable_config.js"></script>
	<script type="text/javascript">
		$(window).load(function() {

			var lotCode = "${lotCode}";
			if(lotCode == 'LHC'){
				return;
			}
			var lotType = "SSC";
			switch(lotCode){
			case "GD11X5":
			case "SD11X5":
			case "JX11X5":
			case "SH11X5":
			case "GX11X5":
				lotType = "11X5";
				break;
			case "BJSC":
			case "XYFT":
			case "LXYFT":
			case "SFSC":
				lotType = "BJSC";
				break;
			case "PL3":
			case "FC3D":
			case "PCEGG":
			case "JND28":
				lotType = "PL3";
				break;
			case "JSSB3":
			case "HEBK3":
			case "HBK3":
			case "SHHK3":
			case "AHK3":
			case "GXK3":
			case "GZK3":
			case "BJK3":
			case "GSK3":
			case "FFK3":
			case "WFK3":
			case "JPK3":
			case "KRK3":
			case "HKK3":
			case "AMK3":
			case "JXK3":
				lotType = "K3";
				break;
			case "HNKLSF":
			case "GDKLSF":
				lotType = "KLSF";
				break;
			}
			var menu1s = trendchart_menu_1[lotType];
			var trendchart_menu_1_temp = $("#trendchart_menu_1_temp").html();
			$("#trendchart_menu_1").html(_.template(trendchart_menu_1_temp)({list: menu1s}));
			$("#trendchart_menu_1 li[val]").on("click", function(){
				$(this).siblings("li[val]").removeClass("on");
				$(this).addClass("on");
				var menu2s = trendchart_menu_2[lotType + "_" + $(this).attr("val")];
				var trendchart_menu_2_temp = $("#trendchart_menu_2_temp").html();
				$("#trendchart_menu_2").html(_.template(trendchart_menu_2_temp)({list: menu2s}));

				$("#trendchart_menu_2 a[val]").on("click", function(){
					var $this = $(this);
					$this.siblings("a[val]").removeClass("red");
					$this.addClass("red");

					var resurce = $.getStrResouce("${base }/common/template/lottery/trendChart/template/" + $this.attr("val") + ".html?v=4");
					if(resurce){
						var templateArr = resurce.split("☽☻");

						$.ajax({
							type : "POST",
							url : "${base }/lottery/trendChart/lotteryOpenNum.do",
							data : {
								lotCode: '${lotCode}',
								page: 1,
								rows: '${rows}',
								recentDay: '${recentDay}',
								order: '${order}'
							},
							success : function(result) {
								if(!result||result.success==false || result.length==0){
									alert("开奖结果为空");return;
								}
								var params = {
									base: "${base}",
									lotCode: "${lotCode}",
									rows: "${rows}",
									recentDay: "${recentDay}",
									order: "${order}"
								};

								$("#chartTableWrapper").html(_.template(templateArr[0])($.extend(params, {list: result, gamename: $this.attr("gamename"), index: $this.attr("index")})));

								var configList = trendchart_charttable_config[$this.attr("val")];
								if(configList){
									var chartTable = new ChartTable({
										configList : configList
									});
									chartTable.init();
									chartTable.render();
								}
							}
						});
						$(".parameter_draw_wrapper").html(templateArr.length >= 2 ? templateArr[1] : "");
					}
				});
				$("#trendchart_menu_2 a[val]:eq(0)").trigger("click");
			});
			$("#trendchart_menu_1 li[val]:eq(0)").trigger("click");
		});
	</script>
</body>
</html>
