<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/user_content.css?v=111" type="text/css">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 推广链接</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label></div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}</span>
				</div>
				<a class="btn recharge normal" href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox"><div class="prizetitle">推广链接</div><div class="clear"></div></div>
				<div class="filter">
					<ul>
						<li>
							<div class="category">
							<input value="" id="yhType"  type="hidden"/>
							<select style="width: 100px;" id="getByType">
								<option selected="selected" value="0">全部</option>
								<c:if test="${trunOnoff ne 'on' }">
								<option value="2">会员</option>
								</c:if>
								<option value="1">代理</option>
							</select>
							</div>
						</li>
							<li>
							<div class="time-piece">
								<input class="ddbtn" type="button" onclick="search();" value="查询">
								<input class="ddbtn2"  type="button" onclick="addLink('');" value="添加推广链接">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>账号</td>
								<td>类型</td>
								<td>返点</td>
								<td>推广码</td>
								<td>推广链接</td>
								<td>短链接</td>
								<td>加密链接</td>
								<td>状态</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="9" class="nodate" height="30">暂无数据，<a href="${base}/lotteryV3/sport.do">立即投注</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot" style="display:none;">
							<tr>
								<td colspan="3" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td colspan="2">&nbsp;</td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
<script type="text/javascript">
var curNavFlag=null;
$(function() {
	$('#endTime,#startTime').cxCalendar({
		format : 'YYYY-MM-DD'
	});
	$("#selected_time").change(function() {
		quickSelDate(parseInt($(this).val()));
	});
	initRdsData();
	initCombo();
	$("#getByType").change(function() {
		var curVal = $(this).find("option:checked").attr("value");
		$("#yhType").val(curVal);
		$("#datagrid_tb").bootstrapTable('refresh');
	});
});
function search(){
	$("#pageNumber").val(1);
	initRdsData();
}
function initRdsData(){
	var load = new Loading();
	load.init({
		target:"#betResultOrder"
	})
	load.start();
	var pageNumber=$("#pageNumber").val();
	if(pageNumber==null || pageNumber == ''){
		pageNumber = 1;
	}
	pageSize=10;
	var data = {
		type: $("#yhType").val(),
		pageSize:pageSize,
		pageNumber:pageNumber,
		stationId: $("#stationId").val()
	}
	$.ajax({
		url:"${base}/daili/generalizeLink/list.do",
		data:data,
		success:function(res){
			if(res.success===false){
				alertMsg(res.msg||"发生错误");
				load.stop();
				return;
			}
			if(res.total == 0){
				//无数据
				temp = '<tr><td colspan="9" class="nodate" height="200">暂无数据</td></tr>';
				$("#betResultOrder").html(temp);
				$("#orderBetOrderTblFoot").hide();
				layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
			}else{
				addTemplate(res,pageNumber,pageSize);
			}
			load.stop();
		}
	})
}

function addTemplate(res,pageNumber,pageSize){
	var temp ="", bd=null;
	$.each(res.rows,function(index,item){
		bd = new Date(item.bettingDate);
		temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>'+item.userAccount +'</td>';
		temp += '<td>'+typeFormatter(item.type)+'</td>';
		temp += '<td>'+cpRollFormatter(item.cpRolling)+'</td>';
		temp += '<td>'+item.linkKey+'</td>';
		temp += '<td>'+pasteFormatter(item.linkUrl)+'</td>';
		temp += '<td>'+shortUrlFormatter(item)+'</td>';
		temp += '<td>'+encodeUrlFormatter(item)+'</td>';
		temp += '<td>'+statusFormatter(item)+'</td>';
		temp += '<td>'+operateFormatter(item)+'</td>';
	})
	$("#betResultOrder").html(temp);
	layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
	
	
}
//删除
function del(id) {
	layer.confirm('是否确定删除？', function() {
		$.ajax({
			url : "${base}/daili/generalizeLink/del.do",
			data : {
				id : id
			},
			success : function() {
				alert("删除成功！");
				window.location.reload();
			}
		});
	});

}
function operateFormatter(row) {
	return [ '<a class="eidt" onclick="addLink('+ row.linkKey+')" href="javascript:void(0)" title="修改">',
			'<i class="glyphicon glyphicon-pencil"></i>', '修改</a>  ',
			'<a class="del" onclick="del('+ row.id+')" href="javascript:void(0)" title="删除">',
			'<i class="glyphicon glyphicon-remove"></i>', '删除</a>  ' ]
			.join('');
}
function statusFormatter(row){
	if(row.status==1){
		return '禁用'+'<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')" style="color:red;" ><span id="status'+row.id+'" data-status="2"  style="color:red;">（启用）</span></a>';
	}else{
		return '启用'+'<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')"  style="color:red;"><span  id="status'+row.id+'" data-status="1"  style="color:red;">（禁用）</span></a>';
	}
}
function changeStatus(id){

	var status = $("#status"+id+"").attr("data-status");
	var txt="禁用成功";
	if(status==2){
		txt="启用成功";
	}
	$.ajax({
		url : "${base}/daili/generalizeLink/updateStatusById.do",
		data : {
			id : id,
			status:status
		},
		success : function() {
			alert(txt);
			window.location.reload();
		}
	});
}
function cpRollFormatter(value) {
	var flag =${isMulti}; 
	if (flag) {
		if(value!=null && value !='undifine'){
			return value;
		}else{
			return '-';
		}
	} else {
		return '-';
	}
}
function typeFormatter(value) {
	if (value == 1) {
		return '代理';
	} else {
		return '会员';
	}
}
function initCombo() {
	$.ajax({
		url : "${base}/daili/dlmnyrd/money/record/type.do",
		async : false,
		success : function(data) {
			var eachdata = {
				"data" : data
			};
			typeData = toTypeMap(data);
			var html = template('recordtype_tpl', eachdata);
			$("#type").append(html);
			var url =  location.href;
			if(url.indexOf('param=')!=-1){
				var obj = $('#type');
				if(url.indexOf('param=sdkk')!=-1){
					obj.find("option[value='2']").attr("selected",true); 
				}else if(url.indexOf('param=sdjk')!=-1){
					obj.find("option[value='1']").attr("selected",true); 
				}
			}
		}
	});
}
function toTypeMap(data) {
	var map = {};
	if (data) {
		for (var i = 0; i < data.length; i++) {
			if (!data[i]) {
				continue;
			}
			map[data[i].type] = data[i].name;
		}
	}
	return map;
}
function dateFormatter(value) {
	return DateUtil.formatDatetime(value);
}
function shortUrlFormatter(row) {
	var r ="";
	if(row.shortUrl1){
		r+=row.shortUrl1;
	}
	if(row.shortUrl2){
		r+="<div style='padding-top:4px'>"+row.shortUrl2+'</div>';
	}
	return r;
}
function encodeUrlFormatter(row) {
	var url="${domain}/vote_topic_"+row.linkKey + '.do';
	return url;
}
function pasteFormatter(value) {
	return value;
}
function addLink(linkeCode){
	
	layer.open({
		type : 2, //page层
		area : [ '530px', '320px' ],
		skin:'layui-layer-rim',
		title : $("h3.page_name").text()+'新增推广链接',
		shade : 0.6, //遮罩透明度
		scrollbar : false,
		offset : '100px',//距离顶部
		moveType : 0, //拖拽风格，0是默认，1是传统拖动
		shift : 0, //0-6的动画形式，-1不开启
		btn:['确实','取消'],
		content : '${base}/center/record/betrecord/leciUserLink.do?linkCode='+linkeCode,
		yes: function (index) {
		    var param = window["layui-layer-iframe" + index].callbackdata();
		    if(!param){
		    	return false;
		    }
			$.ajax({
				url : "${base}/daili/generalizeLink/updateOrSave.do",
				data : param,
				success : function(data) {
					if(data.success){
						alert("保存成功！");
						layer.close(index);
						window.location.reload();
					}else{
						layer.alert(data.msg);
					}
				}
			});

		}, success: function (layero, index) {
			window["layui-layer-iframe" + index].loading();
           /*  //传入参数，并赋值给iframe的元素
			$.ajax({
				url : "${base}/daili/generalizeLink/getRandCode.do",
				dataType : "text",
				success : function(randomCode) {
					if ("${isMulti}" == "true") {
						$('#tgType').attr('onmousedown', 'return true;');
						$('#dljz').val('多级代理');
					} else {
						$('#tgType').attr('onmousedown', 'return false;');
						$('#dljz').val('一级代理');
					}
					var url = "${url}";
					url = url.replace(/link998/, randomCode);
					$('#linkCode').val(randomCode);
					$('#urlLink').val(url);
					$('#rollPoint').val('');
					if ('${memberRate}' == 'on') {
						$("#tgType").val('1');
					}else{
						$('#tgType').val('2');
					}
					if('${memberRate}' != 'on'){
						$('#cpfds').hide();
					}
				} 

		});*/
        }

	});
}

</script>
<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
</body>
</html>