<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="dsLotto-result dsLotto-result--pk10  status--on">
	<div class="_scene-animation _scene-wait">
		<div class="_front-animation">
			<span class="_ele-motor"><em class="_ele-motor-mark">1</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">2</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">3</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">4</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">5</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">6</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">7</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">8</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">9</em></span> <span
				class="_ele-motor"><em class="_ele-motor-mark">10</em></span>
		</div>
		<div class="_counter-animation">
			<ul>
				<li>3</li>
				<li>2</li>
				<li>1</li>
				<li>0</li>
			</ul>
		</div>
	</div>
	<!-- / WAIT -->
	<div class="_scene-animation _scene-finish">
		<ul class="_group array cf">
			<li class="_item _item-1"><span class="_ele-number" id="last_result_0">1</span>
				<span class="_ele-mark">冠</span></li>
			<li class="_item _item-2"><span class="_ele-number" id="last_result_1">2</span>
				<span class="_ele-mark">亚</span></li>
			<li class="_item _item-3"><span class="_ele-number" id="last_result_2">3</span>
				<span class="_ele-mark">季</span></li>
			<li class="_item _item-4"><span class="_ele-number" id="last_result_3">4</span>
				<span class="_ele-mark">四</span></li>
			<li class="_item _item-5"><span class="_ele-number" id="last_result_4">5</span>
				<span class="_ele-mark">五</span></li>
			<li class="_item _item-6"><span class="_ele-number" id="last_result_5">6</span>
				<span class="_ele-mark">六</span></li>
			<li class="_item _item-7"><span class="_ele-number" id="last_result_6">7</span>
				<span class="_ele-mark">七</span></li>
			<li class="_item _item-8"><span class="_ele-number" id="last_result_7">8</span>
				<span class="_ele-mark">八</span></li>
			<li class="_item _item-9"><span class="_ele-number" id="last_result_8">9</span>
				<span class="_ele-mark">九</span></li>
			<li class="_item _item-10"><span class="_ele-number" id="last_result_9">10</span>
				<span class="_ele-mark">十</span></li>
		</ul>
	</div>
	<!-- / FINISH -->
	<div class="_scene-animation _scene-bg">
		<ul class="_group array cf">
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
			<li class="_item"></li>
		</ul>
	</div>
	<!-- / BG -->
</div>