<%@ page language="java" pageEncoding="UTF-8"%>
<strong class="p1" id="jj">简介：</strong>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${param.code == 'JSSB3'}">
<p class="p1">
	「江苏骰宝(快3)」由中国福利彩票发行管理中心组织销售、江苏省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">江苏骰宝(快三)</td>
			<td bgcolor="#FFFFFF">08:50-22:10(北京时间) </td>
			<td bgcolor="#FFFFFF">01-41 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'HBK3'}">
<p class="p1">
	「湖北快三」由中国福利彩票发行管理中心组织销售、湖北省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">湖北快三</td>
			<td bgcolor="#FFFFFF">09:20-22:00(北京时间) </td>
			<td bgcolor="#FFFFFF">01-39 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'AHK3'}">
<p class="p1">
	「安徽快三」由中国福利彩票发行管理中心组织销售、安徽省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">安徽快三</td>
			<td bgcolor="#FFFFFF">09:00-22:00(北京时间) </td>
			<td bgcolor="#FFFFFF">01-40 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'SHHK3'}">
<p class="p1">
	「上海快三」由中国福利彩票发行管理中心组织销售、上海市福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">上海快三</td>
			<td bgcolor="#FFFFFF">08:58-22:18 (北京时间) </td>
			<td bgcolor="#FFFFFF">01-41 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'JXK3'}">
<p class="p1">
	「江西快三」由中国福利彩票发行管理中心组织销售、江西省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">江西快三</td>
			<td bgcolor="#FFFFFF">09:15-22:55(北京时间) </td>
			<td bgcolor="#FFFFFF">01-42 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'HEBK3'}">
<p class="p1">
	「河北快三」由中国福利彩票发行管理中心组织销售、河北省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">河北快三</td>
			<td bgcolor="#FFFFFF">08:50-22:10(北京时间) </td>
			<td bgcolor="#FFFFFF">01-41 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'GXK3'}">
<p class="p1">
	「广西快三」由中国福利彩票发行管理中心组织销售、广西省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">广西快三</td>
			<td bgcolor="#FFFFFF">09:30-22:30(北京时间) </td>
			<td bgcolor="#FFFFFF">01-40 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'FFK3'}">
<!-- <p class="p1">
	极速快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	极速快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">极速快三</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">0001-1440</td>
			<td bgcolor="#FFFFFF">每1分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'WFK3'}">
<!-- <p class="p1">
	幸运快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	幸运快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">幸运快三</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">001-288</td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'JPK3'}">
<!-- <p class="p1">
	幸运快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	日本快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">幸运快三</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">001-288</td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>

<c:if test="${param.code == 'KRK3'}">
<!-- <p class="p1">
	幸运快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	韩国快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">幸运快三</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">001-288</td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>

<c:if test="${param.code == 'SFK3'}">
<!-- <p class="p1">
	十分快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	十分快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">十分快三</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">001-143</td>
			<td bgcolor="#FFFFFF">每10分钟</td>
		</tr>
	</tbody>
</table>
</c:if>

<c:if test="${param.code == 'ESK3'}">
<!-- <p class="p1">
	二十分快三是研发趣味性更高，频次更快的时时彩游戏 -->
<p class="p1">
	二十分快三属于高频彩，投注区分为百位、十位和个位，各位号码范围为1~6，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜3位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">二十分快三</td>
			<td bgcolor="#FFFFFF">9：00-22：00(北京时间) </td>
			<td bgcolor="#FFFFFF">001-39</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>

<c:if test="${param.code == 'BJK3'}">
<p class="p1">
	「北京快三」由中国福利彩票发行管理中心组织销售、广西省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">北京快三</td>
			<td bgcolor="#FFFFFF">09:22-23:42(北京时间) </td>
			<td bgcolor="#FFFFFF">01-44</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'GSK3'}">
<p class="p1">
	「甘肃快三」由中国福利彩票发行管理中心组织销售、广西省福利彩票发行中心承销</p>
<p class="p1">
	如开奖时间异动以中国福利彩票管理中心公告为准。 
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">甘肃快三</td>
			<td bgcolor="#FFFFFF">10:19~21:59(北京时间) </td>
			<td bgcolor="#FFFFFF">01-36</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<p class="p2">
	<br />
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="hz">和值投注</strong>
<p class="p1">是指对三个号码的和值进行投注，包括“和值3”至“和值18”投注。&nbsp;</p>
<p class="p1">投注方案：15</p>
<p class="p1">开奖号码:555</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dxds">大小单双</strong>
<p class="p1">是指对三个号码的总和进行投注，总和大于10为大，小于等于10为小&nbsp;</p>
<p class="p1">投注方案：单</p>
<p class="p1">开奖号码:111</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="sthtx">三同号通选（即全包）</strong>
<p class="p1">是指对所有相同的三个号码（111、222、…、666）进行投注；&nbsp;</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="sthdx">三同号单选</strong>
<p class="p1">是指从所有相同的三个号码（111、222、…、666）中任意选择一组号码进行投注。</p>
<p>
	<br />
</p>
<strong class="p1" id="ethfx">二同号复选</strong>
<p class="p1">是指对三个号码中两个指定的相同号码和一个任意号码进行投注；</p>
<p>
	<br />
</p>
<strong class="p1" id="ethdx">二同号单选</strong>
<p class="p1">是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注</p>
<p>
	<br />
</p>
<strong class="p1" id="sbth">三不同号</strong>
<p class="p1">是指对三个各不相同的号码进行投注。</p>
<p>
	<br />
</p>
<strong class="p1" id="ebth">二不同号</strong>
<p class="p1">是指对三个号码中两个指定的不同号码和一个任意号码进行投注</p>
<p>
	<br />
</p>
<strong class="p1" id="slhtx">三连号通选（即全包）</strong>
<p class="p1">是指对所有三个相连的号码（仅限：123、234、345、456）进行投注。</p>
<p>
	<br />
</p>