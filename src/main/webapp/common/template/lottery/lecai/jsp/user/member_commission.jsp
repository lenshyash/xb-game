<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/user_content.css?v=111" type="text/css">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 业绩提成</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label></div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}</span>
				</div>
				<a class="btn recharge normal" href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox"><div class="prizetitle">业绩提成</div><div class="clear"></div></div>
				<div class="filter">
					<ul>
						<li>
							<div class="category_time">
								<select id="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
							<li>
							<div class="time-piece">
								
								<input name="startTime" id="startTime" class="text text75" readonly="readonly" value="${startTime}" type="text" data-end-date="${endTime}">
								<span>至</span> <input name="endTime" id="endTime" class="text text75" readonly="readonly" value="${endTime}" type="text" data-end-date="${endTime}">
								<input class="ddbtn" type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>会员账号</td>
								<td>变动类型</td>
								<td>变动前金额</td>
								<td>变动金额</td>
								<td>变动后余额</td>
								<td>变动时间(系统)</td>
								<td>备注</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="7" class="nodate" height="30">暂无数据，<a href="${base}/lotteryV3/sport.do">立即投注</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot" style="display:none;">
							<tr>
								<td colspan="3" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td colspan="2">&nbsp;</td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
<script type="text/javascript">
var curNavFlag=null;
$(function() {
	$('#endTime,#startTime').cxCalendar({
		format : 'YYYY-MM-DD'
	});
	$("#selected_time").change(function() {
		quickSelDate(parseInt($(this).val()));
	});
	initRdsData();
	initCombo();
});
function search(){
	$("#pageNumber").val(1);
	initRdsData();
}
function initRdsData(){
	var load = new Loading();
	load.init({
		target:"#betResultOrder"
	})
	load.start();
	var pageNumber=$("#pageNumber").val();
	if(pageNumber==null || pageNumber == ''){
		pageNumber = 1;
	}
	pageSize=10;
	var data = {
		startTime:$("#startTime").val(),
		endTime:$("#endTime").val(),
		pageSize:pageSize,
		pageNumber:pageNumber
	}
	$.ajax({
		url:"${base}/daili/dlfh/list.do",
		data:data,
		success:function(res){
			if(res.success===false){
				alertMsg(res.msg||"发生错误");
				load.stop();
				return;
			}
			if(res.total == 0){
				//无数据
				temp = '<tr><td colspan="7" class="nodate" height="30">暂无数据</td></tr>';
				$("#betResultOrder").html(temp);
				$("#orderBetOrderTblFoot").hide();
				layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
			}else{
				addTemplate(res,pageNumber,pageSize);
			}
			load.stop();
		}
	})
}

function addTemplate(res,pageNumber,pageSize){
	var temp ="", bd=null;
	$.each(res.rows,function(index,item){
		bd = new Date(item.bettingDate);
		temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>'+item.account +'</td>';
		temp += '<td>'+typeFormatter(item.type)+'</td>';
		temp += '<td>'+item.beforeMoney+'</td>';
		temp += '<td>'+item.money+'</td>';
		temp += '<td>'+item.afterMoney+'</td>';
		temp += '<td>'+dateFormatter(item.createDatetime)+'</td>';
		temp += '<td>'+item.remark+'</td></tr>';
	})
	$("#betResultOrder").html(temp);/* 
	$("#orderBetAmount").html(res.aggsData.totalBetMoney||0);
	$("#orderWinAmount").html(res.aggsData.totalBetResult||0); */
	//$("#orderBetOrderTblFoot").show();
	layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
}
function toBetHtml(item, row) {
	var con = item.con;
	if (con.indexOf("vs") == -1) {
		con = '<span class="text-danger">' + con + '</span>';
	}
	var homeFirst = !(item.homeStrong === false);//主队是否在前
	var scoreStr = "";

	if (row.gameTimeType == 1) {
		if (homeFirst) {
			scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH + ":"
					+ row.scoreC + ")</b></font>";
		} else {
			scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC + ":"
					+ row.scoreH + ")</b></font>";
		}
	}
	var home = item.firstTeam;
	var guest = item.lastTeam;
	if (item.half === true && row.mix == 2) {
		home = home + "<font color='gray'>[上半]</font>";
		guest = guest + "<font color='gray'>[上半]</font>";
	}

	var html = item.league + "<br/>" + home + "&nbsp;" + con + "&nbsp;"
			+ guest + scoreStr + "<br/>" + "<font color='red'>"
			+ item.result + "</font>&nbsp;" + "@"
			+ "&nbsp;<font color='red'>" + item.odds + "</font>";
	var balance = row.mix != 2 ? row.balance : item.balance;
	if (balance == 4) {
		html = "<s style='color:red;'>" + html + "</s>"
	} else if (balance == 2 || balance == 5 || balance == 6) {
		var mr = row.mix != 2 ? row.result : item.matchResult;
		if (homeFirst) {
			html = html + "&nbsp;<font color='blue'>(" + mr + ")</font>";
		} else {
			var ss = mr.split(":");
			html = html + "&nbsp;<font color='blue'>(" + ss[1] + ":"
					+ ss[0] + ")</font>";
		}
	}
	return html;
}
function initCombo() {
	$.ajax({
		url : "${base}/daili/dlmnyrd/money/record/type.do",
		async : false,
		success : function(data) {
			var eachdata = {
				"data" : data
			};
			typeData = toTypeMap(data);
			var html = template('recordtype_tpl', eachdata);
			$("#type").append(html);
			var url =  location.href;
			if(url.indexOf('param=')!=-1){
				var obj = $('#type');
				if(url.indexOf('param=sdkk')!=-1){
					obj.find("option[value='2']").attr("selected",true); 
				}else if(url.indexOf('param=sdjk')!=-1){
					obj.find("option[value='1']").attr("selected",true); 
				}
			}
		}
	});
}
function typeFormatter(value) {

	return GlobalTypeUtil.getTypeName(1, 1, value);
}
function toTypeMap(data) {
	var map = {};
	if (data) {
		for (var i = 0; i < data.length; i++) {
			if (!data[i]) {
				continue;
			}
			map[data[i].type] = data[i].name;
		}
	}
	return map;
}
function dateFormatter(value) {
	return DateUtil.formatDatetime(value);
}
</script>
<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
</body>
</html>