<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../../include/baseScript.jsp"></jsp:include>
<div class="userinfo-edit-main">
	<div class="for-liucheng">
		<div class="liulist for-cur"></div>
		<div class="liulist for-cur"></div>
		<div class="liulist for-cur"></div>
		<div class="liulist"></div>
		<div class="liutextbox">
			<div class="liutext for-cur">
				<em>1</em><br> <strong>设置取款密码</strong>
			</div>
			<div class="liutext for-cur">
				<em>2</em><br> <strong>完善出款银行信息</strong>
			</div>
			<div class="liutext for-cur">
				<em>3</em><br> <strong>取款</strong>
			</div>
			<div class="liutext">
				<em>4</em><br> <strong>完成</strong>
			</div>
		</div>
	</div>
	<div class="tab-user">
		<table class="userTab" width="100%" cellspacing="0" cellpadding="0"
			border="0" id="savebank">
			
		</table>
	</div>
</div>
<script type="text/javascript">
	var min = 0;
	var max = 0;
	var balance = 0;
	
	$(function(){
		initDrawdata();
	})
	
	function initDrawdata() {
		$.ajax({
			url : "${base}/center/banktrans/draw/drawdata.do",
			success : function(result) {
				min = result.min;
				max = result.max;
				balance = result.member.money;
				var html = template('draw_tpl', result);
				$("#savebank").append(html);
			}
		});
	}

	function drawcommit() {
		$("#drawcommit").attr("disabled","disabled");
		var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
		var m = $("#money").val();
		if (!exp.test(m)) {
			layer.msg('请输入正确的金额',{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		m = parseInt(m);
		if (m < min) {
			layer.msg('取款最小金额不能小于'+min,{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		if (m > balance) {
			layer.msg('余额不足',{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		if (max != 0 && max < m) {
			layer.msg('取款最大金额不能大于'+max,{icon:2});
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		var userQxpassword = $("#userQxpassword").val();
		if (userQxpassword == null || userQxpassword == "") {
			layer.msg('取款密码不能为空',{icon:2});
			$("#userQxpassword").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		$.ajax({
			url : "${base}/center/banktrans/draw/drawcommit.do",
			data : {
				money : m,
				repPwd : userQxpassword
			},
			success : function(result) {
				if(!result.success){
					layer.msg(result.msg,{icon:2});
					new $.flavr({content:result.msg,autoclose:false});
				}else{
					layer.alert('取款信息已提交！',{icon:1,offset:['30%']},function(){
						window.location.href= '${base}/center/record/hisrecord/cpDfPage.do';
						layer.close(index);
					})
				}
			},
			complete:function(){
				$("#drawcommit").removeAttr("disabled");
			}
			
		});
	}
</script>
<script id="draw_tpl" type="text/html">
<tbody>
				<tr class="tr-clicked">
					<td></td>
					<td colspan="2" style="text-align: left; line-height: 22px;">
						<strong>提示信息</strong><br> 1. 每天的取款处理时间为：<font color="red">{{star}}</font>
						至 <font color="red">{{end}}</font>;<br> 2.
						取款3分钟内到账。(如遇高峰期，可能需要延迟到十分钟内到帐);<br> 3. 用户每日最小提现 <font
						color="red">{{min}}</font> 元，最大提现 <font color="red">{{max}}</font>
						元;<br> 4. 今日可取款 {{wnum}} 次，已取款 {{curWnum}} 次 ;<br> <strong>消费比例</strong><br>
						1. 出款需达投注量：<font color="red">{{checkBetNum}}</font> ， 当前有效投注金额：<font
						color="red">{{member.betNum}}</font> <br> 2. 是否能取款：<font
						color="red">{{drawFlag}}</font>
					</td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>会员帐号：</td>
					<td class="tdinput tdinput-width">{{member.account}}</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>真实姓名：</td>
					<td class="tdinput tdinput-width">{{member.userName}}</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>账户余额：</td>
					<td class="tdinput tdinput-width"><strong style="color:red;">{{member.money}}</strong>元</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>出款银行：</td>
					<td class="tdinput tdinput-width">{{member.bankName}}</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title">开户网点：</td>
					<td class="tdinput tdinput-width">{{member.bankAddress}}</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>银行帐号：</td>
					<td class="tdinput tdinput-width">{{member.cardNo}}</td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>提款金额：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="money" type="text"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>提款密码：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="userQxpassword" type="password"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="paypwd-tr paypwd-tr-input paypwd-tr-new">
					<td class="title"></td>
					<td colspan="2" class="tdinput"><div class="gmbtn">
							<input onclick="drawcommit()" class="sub_btn js_glisten normal"
								 type="button">
							<div class="btntip hidn">注：今日还剩取款次数为<font color="red"></font>次</div>
							<div class="clear"></div>
						</div></td>
				</tr>
			</tbody>
</script>