var dataInfo = [];
var cacheInfo = {};	//数据缓存
var layerIndex = "";
$(function(){
	
	Mark.init();
	//所有文本只能输入数字
	$(".lhcMain").on("keydown","input[type=text]",function(e){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {  
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	var main = $(".lhcMain");
	
	main.on("click","#bet_panel .tab li",function(){
		//清楚隐藏的table数据
		Mark.resetTableDataInfo();
		if(!_Core.isOpen){Mark.closeOrOpenOddsText();}
		_Core.playCode = $(this).attr("c");
		$(this).addClass("on").siblings().removeClass("on");
		$("#navTab_content div#"+$(this).attr('value')).show().siblings().hide();
	}).on({
		mouseover:function(){main.find('._setting').addClass("active");},
		mouseout:function(){main.find("._setting").removeClass("active");}
	},'._setting').on({
		mouseover:function(){$(this).addClass("active");},
		mouseout:function(){$(this).removeClass("active");}
	},'._rigBtn').on("click","._rigBtn ._kj_btn li",function(){
		var dataId = $(this).attr("dataid");
		if(dataId == 'zxzd' && !_Core.isLogin){
			_Core.utils._isLoginBtn();
			return false;
		}
		$(this).addClass("cur").siblings().removeClass("cur");
		$("._rigBtn").removeClass("active");
		main.find(".lhc_R div#"+dataId).removeClass('hidden').siblings("div:not(.user_R)").addClass("hidden");
	}).on({
		mouseover:function(){
			var haoMa = $(this).attr("hao_ma");
			if(!(!haoMa)){
				layer.tips(haoMa,$(this),{tips: [1, '#f13131'],time:0,area:['auto']});
			}
		},
		mouseout:function(){
			layer.closeAll('tips');
		}
	},"#betResultOrder font").on("click","._group ul li",function(){
		var c = $(this).attr("ng-class"),ysM=$(this).find('a').attr("value"),quick=main.find("input.quickmoney"),oldM=quick.val();
		$(this).addClass('active').siblings().removeClass('active');
		main.find("#_groupFooter ul li."+c).addClass("active").siblings().removeClass("active");
		if(!oldM){
			quick.val(ysM);
		}else{
			quick.val(parseInt(ysM)+parseInt(oldM));
		}
		quick.blur();
		Mark._setCookieValue('_group',$(this).parent().parent().html());
	}).on("change","._setting .__form select",function(){
		var c = $(this).parent().attr("class"),text=$(this).find("option:selected").text();
		$(this).find("option:selected").attr("selected","selected");
		main.find("._group ."+c+" a").text(text).attr("value",$(this).val());
		Mark._setCookieValue('_group',main.find("._group").html());
		Mark._setCookieValue('_form',$(this).parent().parent().html());
	}).on("click","#bet_panel .games tr th",function(){
		if(!_Core.isOpen){return false;}
		var r = $(this).find("input[type=radio]"),v = r.val();
		if(!(r.is(":checked"))){
			r.prop('checked',true);
			_Core.playCode = v;
			//如果是11选5的直选则需要切换投注内容
			if(_Core.type == 14 && _Core.groupCode == 'zhixuan'){
				$("#navTab_content div[id^="+v+"]").show().siblings().hide();
			}
			Mark.resetTableDataInfo();
		}
	}).on("click","#bet_panel .games tr th input",function(event){
		event.stopPropagation();
		if(!_Core.isOpen){return false;}
		if($(this).is(":checked")){
			$(this).prop("checked",true);
			_Core.playCode = $(this).val();
			if(_Core.type == 14 && _Core.groupCode == 'zhixuan'){
				$("#navTab_content div[id^="+$(this).val()+"]").show().siblings().hide();
			}
			Mark.resetTableDataInfo();
		}
	}).on({
		mouseover:function(){
			var cur = $(this).attr("data-current");
			$(this).parent().find('td[data-current='+cur+']').addClass('hover');
		},
		mouseout:function(){
			var cur = $(this).attr("data-current");
			$(this).parent().find('td[data-current='+cur+']').removeClass('hover');
		},
		click:function(){
			if(!_Core.isOpen){return false;}
			if(!_Core.playCode){
				Mark.msg.layerMsg('请先选中类型',2);
				return false;
			}
			var cur = $(this).attr("data-current");
			if($(this).hasClass('active')){
				$(this).parent().find('td[data-current='+cur+'] input[type=text]').val('');
				$(this).parent().find('td[data-current='+cur+'] input[type=checkbox]').prop("checked",false);
				$(this).parent().find('td[data-current='+cur+']').removeClass('active');
				if($(this).parent().find('td[data-current='+cur+'] input').attr("type") == 'checkbox'){
					Mark.betting.removeCheckedDataInfo($(this).parent().find('td[data-current='+cur+'] input').attr("num"));
				}else{
					Mark.betting.isExitsDataInfo($(this).parent().find('td[data-current='+cur+'] input').attr("num"));
				}
				return ;
			}
			if(Mark.maxS != 0 && Mark.curLength !=0 && Mark.curLength>=Mark.maxS){
				return false;
			}//判断checkbox
			_Core.utils.soundBetSelectedPlay();
			if(!(!Mark.ysMoney)){
				$(this).parent().find('td[data-current='+cur+'] input[type=text]').val(Mark.ysMoney);
			}
			if(_Core.groupCode == 'zhixuan' && _Core.type == 14){
				var sibTab = $(this).parent().parent().parent().siblings('table');
				if(sibTab.find('td[data-current='+cur+']').hasClass('active')){
					sibTab.find('td[data-current='+cur+'] input[type=checkbox]').prop("checked",false);
					sibTab.find('td[data-current='+cur+']').removeClass('active');
				}
			}
			$(this).parent().find('td[data-current='+cur+'] input[type=checkbox]').prop("checked",true);
			$(this).parent().find('td[data-current='+cur+']').addClass('active');
			Mark.betting.addDataBetInfo($(this).parent().find('td[data-current='+cur+'] input'));
		}
	},"#navTab_content table tr td").on("click keyup blur","#navTab_content table tr td input[type=text]",function(event){
		event.stopPropagation();
		var cur = $(this).parent().attr("data-current"), parentTD = $(this).parent().parent().find('td[data-current='+cur+']');
		var v = $(this).val();
		if(!v){
			if(!(!Mark.ysMoney)){
				$(this).val(Mark.ysMoney);
				if(!parentTD.hasClass("active")){
					_Core.utils.soundBetSelectedPlay();
				}
				parentTD.addClass("active");
				Mark.betting.addDataBetInfo($(this));
			}else{
				parentTD.removeClass("active");
			}
			Mark.betting.isExitsDataInfo($(this).attr("num"));
		}else{
			if(!parentTD.hasClass("active")){
				_Core.utils.soundBetSelectedPlay();
			}
			parentTD.addClass("active");
			Mark.betting.addDataBetInfo($(this));
		}
		//需要重新更新数据
	}).on("click","#navTab_content table tr td input[type=checkbox]",function(event){
		event.stopPropagation();
		if(!_Core.isOpen){return false;}
		var cur = $(this).parent().attr("data-current"),parentID = $(this).parent().parent().find('td[data-current='+cur+']');
		if(!$(this).is(":checked")){
			parentID.removeClass("active");
			$(this).prop("checked",false);
			Mark.betting.removeCheckedDataInfo($(this).attr("num"));
		}else{
			_Core.utils.soundBetSelectedPlay();
			parentID.addClass("active");
			Mark.betting.addDataBetInfo($(this));
		}
	}).on("keyup blur","input.quickmoney",function(){
		var m = $(this).val();
		if(!m){
			Mark.ysMoney = 0;
			main.find("input.quickmoney").val('');
			$("#navTab_content table tr td.active input").val('');
		}else{
			Mark.ysMoney = parseInt(m);
			main.find("input.quickmoney").val(Mark.ysMoney);
			//更改预设金额时，查询是否有选中的数据，有的话则替换为更改后的预设金额
			$("#navTab_content table tr td").each(function(index,item){
				if($(item).hasClass("active")){
					//存在
					$(this).find('input').val(Mark.ysMoney);
				}
			})
		}
		Mark.betting.updDateInfoMoney();
	}).on("click","button.commitbtn",function(){
		if(!_Core.isLogin){
			_Core.utils._isLoginBtn();	//未登录的话需要存储在cookie，登录后直接弹窗下注
			return false;
		}
		if(!_Core.isOpen){return false;}
		if(!dataInfo || dataInfo.length==0){
			Mark.msg.layerMsg("请选择投注内容",2);
			return false;
		}
		//数据判断....先省略
		if(Mark.minS != 0 || Mark.maxS != 0){
			if(dataInfo.length != 1){
				Mark.msg.layerAlert("");
				return false;
			}
			var len = (dataInfo[0].name).length;
			if(len<Mark.minS){
				Mark.msg.layerAlert("最少选中"+Mark.minS+"个下注项!",5);
				return false;
			}
			if(len>Mark.maxS){
				Mark.msg.layerAlert("最多只能选中"+Mark.maxS+"个下注项!",5);
				return false;
			}
		}
		var content = Mark._buyMessage();
		layer.open({
			type : 1, //page层
			area : [ '550px', 'auto' ],
			skin:'layui-layer-rim',
			title : '下注明细(请确认注单)',
			shade : 0.6, //遮罩透明度
			scrollbar : false,
			offset : '150px',//距离顶部
			moveType : 0, //拖拽风格，0是默认，1是传统拖动
			shift : 1, //0-6的动画形式，-1不开启
			content : content,
			btn: ['确定', '取消'],
			yes:function(index){
				layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
				Mark.submit(index);
			},
			btn2:function(){
				Mark.resetTableDataInfo();
			},
			cancel:function(){
				Mark.resetTableDataInfo();
			}
		});
		
	}).on("click","button.resetbtn",function(){
		if(!_Core.isOpen){return false;}
		Mark.resetAllDataInfo();
	}).on("click","#kjtz table tr td",function(){
		if(_Core.groupCode != 'tm' && _Core.groupCode != 'zm' && _Core.groupCode != 'ztm'){
			layer.msg("该方式不支持快捷投注",{icon:2,offset:['30%']});
			return false;
		}
		$(this).parent().parent().find('td').removeClass("active");
		var data = $(this).attr("data"),array=[],$cont = $("#navTab_content div:visible"),dataCur;
		switch(data){
		case "clear":
			Mark.resetTableDataInfo();
			break;
		default:
			Mark.resetTableDataInfo();
			$(this).addClass("active");
			array = data.split(",");
			_Core.utils.soundBetSelectedPlay();
			$.each(array,function(i,j){
				dataCur = $cont.find("span.mark_"+j).parent().attr("data-current");
				if(!(!Mark.ysMoney)){
					$cont.find('td[data-current='+dataCur+'] input[type=text]').val(Mark.ysMoney);
				}
				$cont.find('td[data-current='+dataCur+']').addClass('active');
				Mark.betting.addDataBetInfo($cont.find('td[data-current='+dataCur+'] input'));
			})
			break;
		}
	})
})


var Mark = {
	ysMoney:0,
	minS:0,
	maxS:0,
	curLength:0,
	init:function(){
		var v = $.cookie('_group'),form = $.cookie('_form');
		if(v){
			$(".lhcMain ._group").html(v);
		}
		if(form){
			$(".lhcMain .__form").html(form);
		}
	},
	_tempPlayCodeTrigger:function(){
		//
		if(_Core.type == 14 && _Core.groupCode == 'zhixuan'){
			//直选操作
			var betPanel = $("#bet_panel .games tr th:eq(1) input");
			betPanel.prop("checked",true);
			_Core.playCode = betPanel.val();
		}
		$("#bet_panel .tab li.on").bind("click",Mark._tempPlayCodeTriggerHandler).triggerHandler("click");
	},
	_tempPlayCodeTriggerHandler:function(){
		_Core.playCode = $(this).attr("c");
		if(!_Core.isOpen){
			Mark.closeOrOpenOddsText();
		}
		//快捷投注
		var data = _Core.indexBuyData;
		if(data){
			$.each(data.num,function(i,num){
				//重新调用方法进行switch判断
				Mark.indexBetHaoMaSwitch(i,num);
//				var dataCur = nav.find('input[num='+num+']').parent().attr("data-current");
//				nav.find('td[data-current='+dataCur+']').addClass("active");
//				//北京赛车需要加冠军--
//				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
			})
			$(".lhcMain input.quickmoney").val(data.mul);
			$(".lhcMain input.quickmoney").keyup();
		}
		$.cookie("indexBuyData",null);
		_Core.indexBuyData = null;
		$("#bet_panel .tab li.on").unbind("click",Mark._tempPlayCodeTriggerHandler);
	},
	indexBetHaoMaSwitch:function(index,num){
		var firstName = [],nav,dataCur;
		switch(_Core.type){
			case "8":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq('+index+')');
				firstName = ['冠军--','亚军--','季军--'];
				dataCur = nav.find('input[num='+firstName[index]+''+parseInt(num)+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "9":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq('+(index+1)+')');
				firstName = ['万位--','千位--','百位--','十位--','个位--'];
				dataCur = nav.find('input[num='+firstName[index]+''+num+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "10":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq(2)');
				dataCur = nav.find('input[num='+num+'点]').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "11":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq(2)');
				dataCur = nav.find('input[num='+num+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "12":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq(0)');
				dataCur = nav.find('input[num='+parseInt(num)+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "14":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq('+index+')');
				firstName = ['第一球--','第二球--','第三球--','第四球--','第五球--'];
				dataCur = nav.find('input[num='+firstName[index]+''+num+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
			case "15":
				nav = $('#navTab_content div[id^='+_Core.playCode+'] table:eq('+index+')');
				firstName = ['第一球--','第二球--','第三球--'];
				dataCur = nav.find('input[num='+firstName[index]+''+num+']').parent().attr("data-current");
				nav.find('td[data-current='+dataCur+']').addClass("active");
				Mark.betting.addDataBetInfo(nav.find('td[data-current='+dataCur+'] input'));
				break;
				break;
		}
	},
	resetAllDataInfo:function(){
		//后续累计添加需要清楚的数据
		$("#lhcContent input.quickmoney").val('');//预设金额清空
		Mark.ysMoney = null;
		Mark.resetTableDataInfo();
	},
	resetTableDataInfo:function(){	//清楚table选中数据
		$("#navTab_content table tr td").removeClass('active');
		$("#kjtz table tr td").removeClass("active");
		$("#navTab_content table tr td input").val('').prop("checked",false).prop("disabled",false);
		dataInfo = [];
		Mark.minS = 0;
		Mark.maxS = 0;
		Mark.curLength = 0;
		if(_Core.groupCode == 'zuxuansan' || _Core.groupCode == 'zuxuanliu'){
			$("#bet_panel .games tr td").text('--');
		}
	},
	_buyMessage:function(){
		var name=$("#top_tab_list li.on").text().trim(),names=$("#bet_panel .tab li.on").text().trim(), allM = 0,l=0,temp = '<div id="model_body"><table><thead><tr><th width="180">玩法类别</th><th width="150">号码</th><th width="80">赔率</th><th>金额</th><th width="100">确认</th></tr></thead><tbody>';
		if(_Core.groupCode =='lm' || _Core.groupCode =='qbz' || _Core.groupCode == 'lianma' || _Core.groupCode == 'renxuan' || _Core.groupCode == 'zuxuan' || _Core.groupCode == 'zuxuansan' || _Core.groupCode == 'zuxuanliu' || _Core.groupCode == 'zhixuan'){
			names = $("#bet_panel .games th input[type=radio]:checked").parent().text().trim();
		}
		$.each(dataInfo,function(index,item){
			if(item.type ==1){
				if(!isNaN(item.money) && item.money != 0){allM += item.money;l++}
			}else{
				item.name = item.name.join(",");
				l = Mark.betting.buyZhuShu();
				allM = item.money * l;
			}
			temp += '<tr>';
			if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC'){
				
			}else{
				item.odds = formatDecimal(item.odds)
			}
			temp += '<td>'+name+"-"+names+'</td><td>'+item.name+'</td><td><strong class="red">'+item.odds+'</strong></td><td><input type="text" ts="'+item.type+'" onkeydown="return Mark._onlyNumber(event,this);" onkeyup="Mark.betting.updDataInfoOnlyMoney(this);" maxlength="6" value="'+(isNaN(item.money)?'0':item.money)+'" odds="'+item.odds+'" num="'+item.name+'" data-id="'+item.oddsId+'"></td><td><input onclick="Mark.modelBodyCheck(this);" value="'+item.name+'" '+((isNaN(item.money) || item.money == 0)?'':'checked="checked"')+' type="checkbox"></td>';
			temp += '</tr>';
		})
		temp += '<tr><td>组数：<strong class="red" id="infoLength">'+l+'</strong></td><td colspan="4">总金额：<strong class="red" id="allM">'+allM+'</strong>'+(_Core.curMoney < allM?'<span class="notMoney" style="color:#3c763d">(余额不足，当前余额为<strong class="red refreshM">'+_Core.curMoney+'</strong><i onclick="refreshMoney()" class="img-login-refresh icon"></i>,请前往<a href="'+_Core.base+'/center/banktrans/deposit/cp3Page.do" target="_blank" style="color:red;text-decoration:underline">充值</a>)</span>':'')+'</td></tr>'
		if(_Core.groupCode == 'lx' || _Core.groupCode == 'wsl' || _Core.groupCode == 'hx'){
			temp += '<tr><td colspan="5" style="color:red;">*温馨提示:(中奖号码,既有本命年(0尾),又有非本命年(非0尾) 可能会出现2种赔率的情况)</td></tr>'
		}
		temp += '</tbody></table></div>';
		return temp;
	},
	disabledCheckbox:function(){
		var tableS = $('#navTab_content div table tr td');
		tableS.each(function(index,item){
			if(!($(item).hasClass("active"))){
				$(item).find("input[type=checkbox]").prop("disabled",true);
			}
		})
	},
	openDisabledCheckbox:function(){
		var tableS = $('#navTab_content div table');
		tableS.find("input[type=checkbox]").prop("disabled",false);
	},
	closeOrOpenOddsText:function(){
		var tab = $("#bet_panel table"), t_odds = tab.find("tr td.odds"),t_input= tab.find("tr input");
		t_odds.text('--');
		t_input.prop("disabled",true);
		
	},
	submit:function(index){
		//alert(_Core.code + "--" + _Core.playCode + "--" + _Core.groupCode + "--" + _Core.curQiHao + "--" + _Core.type);
		if(!_Core.code || !_Core.playCode || !_Core.curQiHao || !_Core.type){
			layer.close(layerIndex);
			Mark.msg.layerAlert('系统参数错误,请联系客服',5);
			return false;
		}
		var allM = $("#allM").text();
		if(dataInfo.length == 0){
			layer.close(layerIndex);
			Mark.msg.layerAlert("请选中下注内容",5);
			return false;
		}
		if(allM == 0){
			layer.close(layerIndex);
			Mark.msg.layerAlert("下注金额不能为空",5);
			return false;
		}
		if(_Core.curMoney < allM){
			layer.close(layerIndex);
			Mark.msg.layerAlert("当前用户余额不足",5);
			return false;
		}
		var data = {
				data:JSON.stringify(dataInfo),
				lotCode:_Core.code,
				groupCode:_Core.groupCode,
				playCode:_Core.playCode,
				lotType:_Core.type,
				qiHao:_Core.curQiHao,
		};
		$.ajax({
			url:_Core.base + "/lotteryV3/betInfo.do",
			data:data,
			success:function(res){
				if(res.success){
					layer.close(layerIndex);
					layer.close(index);
					Mark.msg.layerMsg(res.msg,6);
					Mark.resetTableDataInfo();
					_Core.refreshMoney();
					Mark.doOrder();
					//$("#dataArrayModal .resetBtns").bind('click',LotV2.playOK).triggerHandler("click");
				}else{
					layer.close(layerIndex);
					Mark.msg.layerAlert(res.msg,2);
					//btn.find(".layui-layer-btn0").removeAttr("disabled");
				}
			}
		})
	},
	_onlyNumber:function(e,t){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
			// Mark.betting.updDataInfoOnlyMoney($(t));
             return true;  
         } else {  
             return false;  
         }  
	},
	modelBodyCheck:function(t){
		if($(t).is(":checked")){
			var dataId = $(t).attr("dataId");
			Mark.betting.addDataBetInfo($(t).parent().parent().find("input[type=text]"));
		}else{
			//删除相应的数据
			Mark.betting.isExitsDataInfo($(t).val());
		}
		var type = $(t).parent().parent().find("input[type=text]").attr("ts");
		Mark.betting.newBuyDataInfo(type);	//重新计算注数和总金额
	},
	doOrder:function(){
		if(!_Core.isLogin){
			_Core.utils._isLoginBtn();
			return false;
		}
		var data = {
				qiHao:_Core.curQiHao,
				lotCode:_Core.code
		}
		$.ajax({
			url:_Core.base + "/lotteryV3/betInfo/doOrder.do",
			data:data,
			success:function(res){
				var arr =[],temp="",sum=0;
				if(res.success){
					arr = res.doOrder;
					if(arr == null || arr.length == 0){
						return temp;
					}
					temp += '<tr><td colspan="2" class="table_side"><strong>'+_Core.curQiHao+'期</strong></td></tr>'
					$.each(arr,function(index,item){
						var m="";
						if(item.haoMa.length > 10){
							m = item.haoMa;
							item.haoMa = item.haoMa.substring(0,8) + "...";
						}
						if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC'){
							
						}else{
							item.odds = formatDecimal(item.odds)
						}
						temp += '<tr><th class="wh">注单号</th><td class="green"><a class="BetInf" orderid="'+item.orderId+'" lotcode="'+item.code+'">'+item.orderId+'</a></td></tr>';
						temp += '<tr><th class="wh">彩种/赔率</th><td>'+item.playName+'@<font color="blue" hao_ma='+m+'>'+item.haoMa+'</font>/<strong class="red">'+item.odds+'</strong></td></tr>';
						temp += '<tr><th class="wh">下注金额</th><td><strong>'+item.buyMoney+'</strong></td></tr>';
						temp += '<tr><th class="wh">可赢金额</th><td><strong>'+((item.odds*item.buyMoney)-item.buyMoney).toFixed(2)+'</strong></td></tr>'
						sum += item.buyMoney;
					})
					temp += '<tr><th class="wh"><strong>下注单数</strong></th><td><strong>'+arr.length+'笔</strong></td></tr>';
					temp += '<tr><th class="wh"><strong>合计金额</strong></th><td><strong>'+sum+'</strong></td></tr>';
				}
				$("#betMarkResultOrder").html(temp);
			},
			complete:function(){
				//显示最新注单
				$(".lhcMain .lhc_R div#zxzd").removeClass('hidden').siblings("div:not(.user_R)").addClass("hidden");
			}
		})
	},
	lotQueue:function(){
		var temp = "",arr=[];
		var data ={code:_Core.code}
		$.ajax({
			url:_Core.base + "/lotteryV3/doQueue.do",
			data:data,
			success:function(queue){
				arr = queue.ommitQueue;
				if(arr != null && arr.length!=0){
					$.each(arr,function(index,item){
						temp += '<tr><th><a href="javascrip:void(0);" data-href="">'+item.name+' -- '+item.names+'</a></th><td>'+item.sortNum+' 期</td></tr>';
					})
					$("#ommitQueue").html(temp);
				}
			},
			complete:function(){
				//$(".lhcMain .lhc_R div#zxzd").removeClass('hidden').siblings("div:not(.user_R)").addClass("hidden");
			}
		})
	},
	_setCookieValue:function(name,value){
		$.cookie(name,value,{expires:30});	//raw:true加入编码
	},
	_getCookieValue:function(name){
		$.cookie(name);
	},
	_removeCookieValue:function(name){
		$.cookie(name,null);
	}
	
}

//数据处理
Mark.betting = function(){
	return {
		addDataBetInfo:function(source){	//选中时加入数组中
			var money = parseInt(source.val()), dataId = source.attr("data-id"),num = source.attr("num"),odds = source.attr("odds"),max = parseInt(source.attr("max")),min = parseInt(source.attr("min"));
			if(source.attr("type") == 'checkbox'){
				//checkbox
				Mark.betting.addCheckedDataInfo(source);
			}else if(source.attr("type") == 'text'){
				var betInfo = new Object();
				betInfo.name = num;
				betInfo.money = money;
				betInfo.oddsId = dataId;
				betInfo.odds = odds;
				betInfo.type = 1;		//type=text
				betInfo.playCode = _Core.playCode;
				Mark.betting.isExitsDataInfo(betInfo.name);
				dataInfo.push(betInfo);
			}
			//验证金额大小
//			if(!isNaN(money)){	//金额不为空时才判断
//				if(isNaN(max) || isNaN(min)){
//					Mark.msg.layerAlert('数据异常',5);
//					Mark.betting.isExitsDataInfo(num);
//					return false;
//				}
//				if(money > max){
//					Mark.msg.layerTips("下注金额不能超过最大金额["+max+"]",source);
//					Mark.betting.isExitsDataInfo(num);
//					return false;
//				}
//				if(money < min){
//					Mark.msg.layerTips('下注金额不能低于最小金额['+min+']',source);
//					Mark.betting.isExitsDataInfo(num);
//					return false;
//				}
//			}
		},
		addCheckedDataInfo:function(source){
			var c;
			if(_Core.groupCode == 'lm' || _Core.groupCode == 'qbz' || _Core.groupCode == 'lianma' || _Core.groupCode == 'renxuan' || _Core.groupCode == 'zuxuan'
				|| _Core.groupCode == 'zuxuansan' || _Core.groupCode == 'zuxuanliu' || _Core.groupCode == 'zhixuan'){
				c = $('#bet_panel .games tr td[c='+_Core.playCode+']');
				Mark.minS = c.attr("min_s");Mark.maxS = c.attr("max_s");
			}else{
				c = source;
				Mark.minS = c.attr("min_s");Mark.maxS = c.attr("max_s");
			}
			if(_Core.groupCode == 'zhixuan' && _Core.type == 14){
				Mark.betting.syx5Zhixuan();
				return;
			}
			if(dataInfo.length == 0){	//不存在则添加
				var betInfo = new Object();
				betInfo.name = new Array(source.attr("num"));
				if(!Mark.ysMoney){
					betInfo.money = 0;
				}else{
					betInfo.money = Mark.ysMoney;
				}
				betInfo.oddsId = c.attr("data-id");
				betInfo.odds = c.attr("odds");
				betInfo.type = 2;	//checkbox
				betInfo.playCode = _Core.playCode;
				dataInfo.push(betInfo);
			}else{	//存在则修改name
				Mark.betting.updDateInfoName(source.attr("num"));
			}
		},
		syx5Zhixuan:function(){
			var arr = [],arr1 = [], arr2 = [],tabNum = 0, divBet = $("#navTab_content div:visible table");
			divBet.each(function(i,j){
				j = $(j);tabNum++;
				j.find('tbody tr td.active input[type=checkbox]').each(function(a,b){
					b = $(b);
					switch(i){
						case 0:
							arr[a] = b.attr('num');
							break;
						case 1:
							arr1[a] = b.attr('num');
							break;
						case 2:
							arr2[a] = b.attr('num');
							break;
					}
				})
			})
			if(tabNum == 2){	//如果号码选择不合法的时候不需要执行
				if(arr.length <= 0 || arr1.length<= 0){return false;}
			}else{
				if(arr.length <= 0 || arr1.length<= 0 || arr2.length<=0){ return false;}
			}
			if(tabNum == 2){
				var arrs = [arr,arr1];
			}else{
				var arrs = [arr,arr1,arr2];
			}
			var resultArr = Mark.betting.doExchange(arrs);
			//加入dataInfo中
			if(resultArr.length > 0){
				var betPel = $("#bet_panel .games tr td[c="+_Core.playCode+"]"),money = $(".quickmoney").val();
				$.each(resultArr,function(i,j){
					var object = new Object();
					object.name = j.join(',');
					object.money = !money?0:money;
					object.oddsId = betPel.attr("data-id");
					object.odds = betPel.attr('odds');
					object.type = 1;		//type=text
					object.playCode = _Core.playCode;
					dataInfo.push(object);
				})
			}
		},
		isExitsDataInfo:function(name){	//判断数据是否存在
			if(dataInfo.length == 0){return false;}
			if(!name){return false;}
			$.each(dataInfo,function(index,item){
				if(item.name == name){
					dataInfo.splice(index,1);
					return false;
				}
			})
		},
		removeCheckedDataInfo:function(name){
			if(_Core.groupCode == 'zhixuan' && _Core.type == 14){
				Mark.betting.syx5Zhixuan();
				return false;
			}
			if(dataInfo.length != 1 || !name){return false;}
			if(dataInfo[0].name.length == 1){
				dataInfo = [];
				return false;
			}
			var i = $.inArray(name,dataInfo[0].name);
			if(i>=0){
				dataInfo[0].name.splice(i,1);
			}
			Mark.curLength = dataInfo[0].name.length;
			Mark.openDisabledCheckbox();
			Mark.betting.zuxuanInfo();
		},
		updDateInfoName:function(name){
			if(dataInfo.length != 1 || !name){return false;}
			if(Mark.maxs != 0 && Mark.curLength>=Mark.maxS){
				return false;	//已超过最大选项
			}
			dataInfo[0].name = dataInfo[0].name + "," + name;
			if(isNaN(parseInt(name))){	//字符串排序
				dataInfo[0].name = dataInfo[0].name.split(",").sort();
			}else{		//int排序
				dataInfo[0].name = dataInfo[0].name.split(",").sort(function(a,b){
					return a-b;
				})
			}
			Mark.curLength = dataInfo[0].name.length;
			if(Mark.maxS != 0 && Mark.curLength>=Mark.maxS){
				//禁用disabled
				Mark.disabledCheckbox();
			}
			Mark.betting.zuxuanInfo();
		},
		zuxuanInfo:function(){
			var c ;
			if(_Core.groupCode == 'zuxuansan' || _Core.groupCode == 'zuxuanliu'){
				if(Mark.curLength >= Mark.minS && Mark.curLength <= Mark.maxS){
					c = $('#bet_panel .games tr input[c='+_Core.playCode+''+Mark.curLength+']');
					dataInfo[0].oddsId = c.attr("data-id");
					dataInfo[0].odds = c.attr("odds");
					if(trendsPeilv =='LHC' || trendsPeilv == 'SFLHC' || trendsPeilv == 'TMLHC' || trendsPeilv == 'WFLHC' || trendsPeilv == 'HKMHLHC'){
						$('#bet_panel .games tr td[c='+_Core.playCode+']').text(c.attr("odds"));
					}else{
						$('#bet_panel .games tr td[c='+_Core.playCode+']').text(formatDecimal(c.attr("odds")));
					}
				}else{
					$('#bet_panel .games tr td[c='+_Core.playCode+']').text('--');
				}
			}
		},
		updDateInfoMoney:function(){	//批量修改金额
			if(dataInfo.length==0){return false;}
			$.each(dataInfo,function(index,item){
				item.money = Mark.ysMoney;
			})
		},
		newBuyDataInfo:function(type){
			var infoL = $("#infoLength"),allM = $("#allM"),m=0,l=0;
			if(dataInfo.length ==0){
				infoL.text(l);
				allM.text(m);
				if(allM.siblings().hasClass("notMoney")){
					$(".notMoney").remove();
				}
				return false;
			}
			$.each(dataInfo,function(index,item){
				if(!(!item.money)){
					l++;
					m += parseInt(item.money);
				}
			})
			if(type == 1){
				infoL.text(l);
			}else{
				l = Mark.betting.buyZhuShu();
				infoL.text(l);
				m = l*m;
			}
			if(_Core.curMoney < m){
				//触发余额不足提醒
				if(!allM.siblings().hasClass("notMoney")){
					allM.after('<span class="notMoney" style="color:#3c763d">(余额不足，当前余额为<strong class="red refreshM">'+_Core.curMoney+'</strong><i onclick="refreshMoney()" class="img-login-refresh icon"></i>,请前往<a href="'+_Core.base+'/center/banktrans/deposit/cp3Page.do" target="_blank" style="color:red;text-decoration:underline">充值</a>)</span>');
				}
			}else{
				$(".notMoney").remove();
			}
			allM.text(isNaN(m)?'0':m);
		},
		updDataInfoOnlyMoney:function(source){
			var allM = $("#allM"),m=$(source).val(),name=$(source).attr("num"),type = $(source).attr("ts");
			if(!name){return false;}
			if(!m){
				//取消选中，数组-1
				$(source).parent().parent().find("input[type=checkbox]").prop("checked",false);
				if(type == 1){
					Mark.betting.isExitsDataInfo(name);
				}else{
					dataInfo = [];
				}
			}else{
				$(source).parent().parent().find("input[type=checkbox]").prop("checked",true);
				Mark.betting.addDataBetInfo($(source));
			}
			Mark.betting.newBuyDataInfo(type);
		},
		buyZhuShu:function(){
			if(_Core.groupCode == 'zuxuansan' || _Core.groupCode == 'zuxuanliu' || _Core.groupCode == 'zhixuan'){
				return 1;
			}
			var a=1,b=1;
			if(Mark.minS == 0 || Mark.curLength == 0 || Mark.maxS==0){return 1;}
			for(var i=1;i<=Mark.curLength-Mark.minS;i++){
				a = a * i;
			}
			for(var i=Mark.curLength;i>Mark.minS;i--){
				b = b * i;
			}
			return b/a;
		},
		doExchange:function(arr){	//直选专用
		    var len = arr.length;
		    // 当数组大于等于2个的时候
		    if(len >= 2){
		        // 第一个数组的长度
		        var len1 = arr[0].length;
		        // 第二个数组的长度
		        var len2 = arr[1].length;
		        // 2个数组产生的组合数
		        var lenBoth = len1 * len2;
		        //  申明一个新数组
		        var items = new Array(lenBoth);
		        // 申明新数组的索引
		        var index = 0;
		        for(var i=0; i<len1; i++){
		            for(var j=0; j<len2; j++){
		                if(arr[0][i] instanceof Array){
		                    items[index] = arr[0][i].concat(arr[1][j]);
		                }else{
		                    items[index] = [arr[0][i]].concat(arr[1][j]);
		                }
		                index++;
		            }
		        }
		        var newArr = new Array(len -1);
		        for(var i=2;i<arr.length;i++){
		            newArr[i-1] = arr[i];
		        }
		        newArr[0] = items;
		        return Mark.betting.doExchange(newArr);
		    }else{
		        return arr[0];
		    }
		}
	}
}();


Mark.msg = function(){
	return {
		//1勾，2差,3问号，4锁，5哭脸，6笑脸，7感叹号
		layerAlert:function(msg,icon){
			layer.alert(msg,{icon:icon,offset:['30%']});
		},
		layerMsg:function(msg,icon){
			layer.msg(msg,{icon:icon});
		},
		layerTips:function(msg,_id){
			layer.tips(msg,_id,{tips:[1,'#f13131']});
		},
		layerConfirm:function(){
			
		}
	}
}();