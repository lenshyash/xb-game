<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>${_title}</title>
<jsp:include page="${MemberFolder }/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/L_slide.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/jquery.cookie.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/scroll.js"></script>
	<c:if test="${onoffMobileChat eq 'on'}">
		<jsp:include page="/common/template/member/chatIframe.jsp"></jsp:include>
	</c:if>
<%--	|| domainFolder== 'd003' ||domainFolder== 'd005'}--%>
	<c:choose>
		<c:when test="${domainFolder== 'd00528' || domainFolder== 'd003' || domainFolder== 'd005'}">
			<jsp:include page="/common/modelCommon/new/pop/version_d3/index.jsp"/>
		</c:when>
		<c:otherwise>
			<jsp:include page="/common/modelCommon/new/index.jsp"/>
		</c:otherwise>
	</c:choose>
</head>
<body>
<input type="hidden" value="${domainFolder}" id="domainId" />
<input type="hidden" value="${base}" id="base" />
	<c:set var="nameArr" value=""></c:set>
	<jsp:include page="${MemberFolder }/${stationFolder}/include/header.jsp"><jsp:param
			value="false" name="type" />
	</jsp:include>
	<div class="main-box clearfix auto990" id="main">
		<input type="hidden" value="0" id="navId" /> <input type="hidden"
			value="${isLogin eq false?1:2}" id="isLogin" />
		<div class="part-1 clearfix">
			<c:if test="${lotVersion == 3}">
				<jsp:include page="include/index_nav.jsp"></jsp:include>
			</c:if>
			<c:if test="${lotVersion == 5 }">
				<jsp:include page="version_2/index_nav_version.jsp"></jsp:include>
			</c:if>
			<div class="part-1-cont-left mr20">
				<div class="slide">
					<div class="wrap" id="Slide">
						<ul class="slidebox">
						</ul>
					</div>
				</div>
				<div class="quick-buy-box mt15">
					<div class="quick-tpis">
						<div style="float:left;">
						<i class="icon-lb"></i> <b>最新公告：</b></div>
						<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/wenzi_scroll.js"></script>
						<div style="float:left; height: 30px; width: 380px;" id="scroll">
							<ul>
								<li></li>
							</ul>
						</div>
					</div>
					<div class="quick-buy">
						<ul class="quick-tab-list">
							<c:forEach items="${lotList}" var="lot" varStatus="l">
								<c:set var="nameArr" value="${nameArr},${lot.name}"></c:set>
								<c:if test="${l.index<4}">
									<c:choose>
										<c:when test="${l.index == 0}">
											<li c="${lot.code }" type="${lot.type}" class="on"><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
												target="_blank">${lot.name}</a></li>
										</c:when>
										<c:otherwise>
											<li c="${lot.code }" type="${lot.type}"><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
												target="_blank">${lot.name}</a></li>
										</c:otherwise>
									</c:choose>
								</c:if>
							</c:forEach>
						</ul>
						<div class="qb-box-list">
							<!--快速购彩 ssq-->
							<c:forEach items="${lotList}" var="lot" varStatus="l">
								<c:if test="${l.index < 4}">
									<c:choose>
										<c:when test="${l.index == 0}">
											<div class="qb" id="qb_${lot.code}" c="${lot.code}" type="${lot.type}">
												<ul class="qb-info clearfix">
													<li>第<span id="ssq_phase"></span>期 截止：<span
														id="ssq_sale_end_timer" class="sale_end_timer"></span></li>
													<li class="bztz clearfi"><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
														target="_blank">手动选号</a> <!-- | <a
														href="javascript:alert('暂未开放');">幸运选号</a> --> | <a
														href="${base}/lotteryV3/trend.do?lotCode=${lot.code}"
														target="_blank">走势图</a></li>
												</ul>
												<div class="qb-selectnumber" style="position: relative">
													<ul class="qb-selectnum clearfix"></ul>
												</div>
											</div>
											<script type="text/javascript">
												Base.getLotCurrentData('${lot.code}');
											</script>
										</c:when>
										<c:otherwise>
											<div class="qb" id="qb_${lot.code}" c="${lot.code}" type="${lot.type}"
												style="display: none;">
												<ul class="qb-info clearfix">
													<li>第<span id="ssq_phase"></span>期 截止：<span
														id="ssq_sale_end_timer" class="sale_end_timer"></span></li>
													<li class="bztz clearfi"><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
														target="_blank">手动选号</a> <!--| <a
														href="javascript:alert('暂未开放');">幸运选号</a> -->| <a
														href="${base}/lotteryV3/trend.do?lotCode=${lot.code}"
														target="_blank">走势图</a></li>
												</ul>
												<div class="qb-selectnumber" style="position: relative">
													<ul class="qb-selectnum clearfix"></ul>
												</div>
											</div>
											<script type="text/javascript">
												Base.getLotCurrentData('${lot.code}');
											</script>
										</c:otherwise>
									</c:choose>
								</c:if>
							</c:forEach>
							<div class="qb-tz-box clearfix" style="">
								<c:if test="${lotVersion != 5}">
								<span class="fl-l bei-box clearfix"><a
									href="javascript:;" class="tz_bei_sub" data-type="ssq">−</a><input
									value="1" class="multiple" id="ssq_multiple" name="input"
									maxlength="3" data-type="ssq" type="text" /><a
									href="javascript:;" class="tz_bei_add" data-type="ssq">+</a><span
									class="mr10">倍</span>共 <strong class="money colorRed"
									id="ssq_amount">2</strong> ${not empty cashName?cashName:'元'}&nbsp;</span>
								</c:if>
								<c:if test="${lotVersion == 5 }">
								<span class="fl-l bei-box clearfix">
									单注：<input value="2" class="multiple" id="ssq_multiple" name="input"
									maxlength="5" data-type="ssq" type="text" style="float:initial;width:60px;border: 1px solid #ccc;" />&nbsp;元
								</span>
								</c:if>
								<span class="dg-btn-box"><a
									href="javascript://" class="change-btn" id="ssq_random"><i
										class="icon"></i> 换一注</a> <c:if test="${isLogin eq true}">
										<a href="javascript://" id="ssq_submit_index"
											class="dg-tz-btn icon"></a>
									</c:if> <c:if test="${isLogin eq false }">
										<a href="javascript://" data-reveal-id="loginBtn"
											class="dg-tz-btn icon"></a>
									</c:if> </span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="part-1-cont-right">
				<div class="login-box">
					<c:choose>
						<c:when test="${isLogin eq true }">
							<div class="login">
								<div class="login-user-info clearfix">
									<span class="user" id="span_user_username">Hi，${loginMember.account }
										<a href="#" target="_blank">
										<c:choose>
											<c:when test="${not empty loginMember.levelIcon&&loginMember.levelIcon!='' }">
												<img src="${loginMember.levelIcon }" style="height: 38px;width: 40px;">
											</c:when>
											<c:otherwise>
												<img src="${base}/common/template/lottery/lecai/images${MemberFolder }/cb_0.gif">
											</c:otherwise>
										</c:choose>
										</a>
									</span> <a href="${base}/logout.do" rel="nofollow"
										class="quit js-trigger-logout">退出</a>
								</div>
								<div class="info-cont clearfix">
									<span><a href="javascript://" class="balance colorRed"
										id="a_show_money" rel="nofollow">点击显示余额</a></span> <input
										id="user_money" class="user_now_money" value="${loginMember.money}" type="hidden">
									<span><a href="javascript://" class="ref"
										id="img_money_refresh"><i onclick="refresh()" class="icon"></i></a></span> <span><a
										href="${base}/center/banktrans/deposit/cp3Page.do"
										class="recharge-btn icon"></a></span>
								</div>
								<div class="info-list clearfix">
									<a href="${base}/center/record/betrecord/cp3Page.do?form=order"
										rel="nofollow" class="play-jl" target="_blank">投注记录</a> <span
										class="m14">|</span> <a
										href="${base}/center/record/betrecord/cp3Page.do?form=win"
										rel="nofollow" target="_blank">中奖记录</a> <span class="m14">|</span>
									<a href="${base}/center/banktrans/draw/cp3Page.do"
										class="withdraw-btn" target="_blank">提款</a>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="not-login">
								<div class="welcome">
									<span>Hi，欢迎来到${_title}</span>
								</div>
								<div class="login-btn-box">
									<a href="javascript://" class="icon login-btn"
										data-reveal-id="loginBtn" title="登录"></a>
										<c:if test="${isReg eq 'on'}"><a href="javascript://" class="icon reg-btn"
										data-reveal-id="registerBtn" title="免费注册"></a></c:if>
										<c:if test="${registerTestGuest }">
											<div class="sw-btn-box">
												<a href="${base}/lotteryV3/registerTestGuest.do" style="display:inline-block;margin-top:-6px;height:26px;">免费试玩</a>
											</div>
										</c:if>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="help-tab-box">
					<ul class="help-tab">
						<!-- <li class="on"><a href="javascript://" class="web-notice">网站公告</a></li> -->
						<li class="on"><a href="javascript://" class="user-help">购彩帮助</a></li>
					</ul>
					<ul class="web-notice-box help-ul" style="display: none;">
						<li><a href="#" title=""><span>最新公告</span></a></li>
						<li><a href="#" title=""><span>最新公告1</span></a></li>
						<li><a href="#" target="_blank">最新公告2</a></li>
					</ul>
					<ul class="user-help-box help-ul" style="">
						<li><a href="${base}/lotteryV3/help.do?code=100" target="_blank">如何注册${_title}会员？</a></li>
						<li><a href="${base}/lotteryV3/help.do?code=101" target="_blank">忘记登录密码了怎么办？</a></li>
						<c:choose>
							<c:when test="${domainFolder eq 'd00308'}">

							</c:when>
							<c:otherwise>
								<li><a href="${base}/lotteryV3/help.do?code=102" target="_blank">能注册多个帐号吗？</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
				<div class="phone-box">
					<c:choose>
						<c:when test="${domainFolder eq 'd00308'}">
							<div class="phone-box-text" style="color: red;">
								扫一扫手机版访问
							</div>
						</c:when>
						<c:otherwise>
							<div class="phone-box-text">
								<span class="red">手机购彩，轻轻松松变土豪！</span>
							</div>
						</c:otherwise>
					</c:choose>

					<div>
						<c:if test="${not empty wapQrCode&&wapQrCode!='' }">
							<img src="${wapQrCode }">
						</c:if>
					</div>
				</div>

			</div>
		</div>
		<div class="part-2 clearfix js-lazy">
			<div class="part-2-cont-left part-left">
				<div id="draw_box" class="draw-notice">
					<div class="draw-box">
						<div class="title-top">
							<h2 class="notice-tit">
								<i class="icon"></i><strong>开奖公告</strong>
							</h2>
							<ul class="notice-tab">
								<li class="tab_g on"><a href="javascript://"
									class="color333" title="高频">高频</a></li>
								<li class="tab_g"><a href="javascript://" class="color333"
									title="低频">低频</a></li>
								<li class="tab_o" style="margin-right: 0;"><a
									target="_blank" href="${base}/lotteryV3/draw.do" class="color333"
									title="更多">更多</a></li>
							</ul>
						</div>
						<script>
							$(function(){
								var base = $("#base").val()
								$.ajax({
									url:base +"/lotteryV3/getGaoDiPinData.do",
									type:"POST",
									success:function (result) {
										if(result.success){
											if (result.content.gaoPin.length > 0){
												let html ='';
												$.each(result.content.gaoPin,function(index,item){
													if (index <= 5){
														html+='<li>'
														html+=		'<span class="lot-name" style="margin-top: 12px;">'
														html+=			'<a href="${base}/lotteryV3/lotDetail.do?lotCode='+item.lotCode+'">'+item.lotName +'</a>'
														html+=		'</span>'
														html+=		'<span class="term" style="margin-top: 12px;">'+item.qiHao+'期</span>'
														html+=		'<span class="clear"></span>'
														html+=		'<div class="clear"></div>'
														let haoMaArr =result.content.gaoPin[index].haoMa.split(',')
														$.each(haoMaArr,function(index2,item2){
															html+='<div class="redball">'+item2+'</div>'
														})
														html+='<div class="fr">'
														html+=			'<span>'+item.date+'</span> |'
														html+=			'<a href="${base}/lotteryV3/trend.do?lotCode='+item.lotCode+'" target="_blank">走势</a> |'
														html+=			'<a href="${base}/lotteryV3/lotDetail.do?lotCode='+item.lotCode+'" target="_blank">投注</a>'
														html+=		'</div>'
														html+=		'<div class="clear" ></div></li><li class="li-line icon"></li>'
													}

												})
												$(".gaopingcai").html(html)
											}
											if (result.content.diPin.length > 0){
												let html ='';
												$.each(result.content.gaoPin,function(index,item){
													if (index <= 5){
														html+='<li>'
														html+=		'<span class="lot-name" style="margin-top: 12px;">'
														html+=			'<a href="${base}/lotteryV3/lotDetail.do?lotCode='+item.lotCode+'">'+item.lotName +'</a>'
														html+=		'</span>'
														html+=		'<span class="term" style="margin-top: 12px;">'+item.qiHao+'期</span>'
														html+=		'<span class="clear"></span>'
														html+=		'<div class="clear"></div>'
														let haoMaArr =result.content.gaoPin[index].haoMa.split(',')
														$.each(haoMaArr,function(index2,item2){
															html+='<div class="redball">'+item2+'</div>'
														})
														html+='<div class="fr">'
														html+=			'<span>'+item.date+'</span> |'
														html+=			'<a href="${base}/lotteryV3/trend.do?lotCode='+item.lotCode+'" target="_blank">走势</a> |'
														html+=			'<a href="${base}/lotteryV3/lotDetail.do?lotCode='+item.lotCode+'" target="_blank">投注</a>'
														html+=		'</div>'
														html+=		'<div class="clear" ></div></li><li class="li-line icon"></li>'
													}

												})
												$(".dipingCai").html(html)
											}
										}
									}
								})
							})

						</script>
						<div class="notice-main">
							<div class="draw-contents"
								style="display: block; height: 400px; position: relative;overflow:hidden;">
								<ul class="notice-list gaopingcai">
<%--									<c:forEach items="${gaoPin}" var="gao" varStatus="gaoIndex">--%>
<%--										<c:if test="${gaoIndex.index <=5}">--%>
<%--										<li><span class="lot-name" style="margin-top: 12px;"><a--%>
<%--												href="${base}/lotteryV3/lotDetail.do?lotCode=${gao.lotCode}">${gao.lotName }</a>--%>
<%--										</span>--%>
<%--											<span class="term" style="margin-top: 12px;">${gao.qiHao}期</span>--%>
<%--											<span class="clear"></span>--%>
<%--											<div class="clear"></div> <c:forTokens items="${gao.haoMa }"--%>
<%--												delims="," var="haoMa" varStatus="i">--%>
<%--												<div class="redball">${haoMa}</div>--%>
<%--												<c:if test="${i.last}">--%>
<%--													<br>--%>
<%--												</c:if>--%>
<%--											</c:forTokens>--%>
<%--											<div class="fr">--%>
<%--												<span>${gao.date}</span> | <a--%>
<%--													href="${base}/lotteryV3/trend.do?lotCode=${gao.lotCode}"--%>
<%--													target="_blank">走势</a> | <a--%>
<%--													href="${base}/lotteryV3/lotDetail.do?lotCode=${gao.lotCode}"--%>
<%--													target="_blank">投注</a>--%>
<%--											</div>--%>
<%--											<div class="clear"></div></li>--%>
<%--											</c:if>--%>
<%--										<li class="li-line icon"></li>--%>
<%--									</c:forEach>--%>
								</ul>
							</div>
							<div class="draw-contents"
								style="display: none; height: 400px; position: relative;overflow:hidden;">
								<ul class="notice-list dipingCai">
<%--									<c:forEach items="${diPin}" var="di" varStatus="i">--%>
<%--										<li><span class="lot-name" style="margin-top: 12px;"><a--%>
<%--												href="${base}/lotteryV3/lotDetail.do?lotCode=${di.lotCode}">${di.lotName }--%>
<%--											</a></span> <span class="term" style="margin-top: 12px;">${di.qiHao}期</span>--%>
<%--											<span class="clear"></span>--%>
<%--											<div class="clear"></div> <c:forTokens items="${di.haoMa}"--%>
<%--												delims="," var="haoMa" varStatus="i">--%>
<%--												<div class="redball">${haoMa}</div>--%>
<%--												<c:if test="${i.last}">--%>
<%--													<br>--%>
<%--												</c:if>--%>
<%--											</c:forTokens>--%>
<%--											<div class="fr">--%>
<%--												<span>${di.date}</span> | <a--%>
<%--													href="${base}/lotteryV3/trend.do?lotCode=${di.lotCode}"--%>
<%--													target="_blank">走势</a> | <a--%>
<%--													href="${base}/lotteryV3/lotDetail.do?lotCode=${di.lotCode}"--%>
<%--													target="_blank">投注</a>--%>
<%--											</div>--%>
<%--											<div class="clear"></div></li>--%>
<%--										<li class="li-line icon"></li>--%>
<%--									</c:forEach>--%>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
				<div class="part-2-cont-right part-left-2">
					<div class="lottery-news-box">
						<div class="news-title clearfix">
							<h2 class="news-tit fl">
								<strong>彩票资讯</strong>
							</h2>
							<div class="hot-news fr" style="width: 292px;">
								<a href="${base}/lotteryV3/news.do" target="_blank" class="mr8">更多资讯&gt;&gt;</a>
							</div>
						</div>
						<div class="news-bar">
							<div class="news-bar-content">
								<h3 class="news-bar-tit">
									<span style="padding-right: 8px;">今日热点</span>
										<c:forEach items="${articleList}" var="art" varStatus="a">
											<c:if test="${a.index == 0 || a.index == 1}">
												<a href="${base}/lotteryV3/news/detail.do?key=${art.id}" target="_blank" title="${art.title}">${art.title}</a>
											</c:if>
										</c:forEach>

								</h3>
								<ul class="news-bar-list news-bar-left clearfix">
									<c:forEach items="${articleList}" var="art" varStatus="a">
										<c:if test="${a.index>1}">
										<li>
											<a href="#" target="_blank" class="news-s">[新闻]</a>
											<a href="${base}/lotteryV3/news/detail.do?key=${art.id}"" target="_blank" title="${art.title}">${art.title}</a>
										</li>
										</c:if>
									</c:forEach>
								</ul>
								<jsp:include page="${MemberFolder }/${stationFolder}/include/index_news_bottom.jsp"></jsp:include>
							</div>
						</div>
					</div> 	
				</div>
				<div class="part-2-cont-right part-right">
					<div class="news-title clearfix">
							<h2 class="news-tit fl">
								<strong>最新中奖</strong>
							</h2>
						</div>
					<div class="news-bar-side myScroll" >
	          <ul class="news-bar-zj">
	          	<c:forEach items="${winOrder}" var="win">
	              	<li>${fn:substring(win.account,0,2)}***&nbsp;喜中 <b style="green">${win.lotCode}</b><span style="color:red">&nbsp;&nbsp;${win.money}元</span></li>
	              </c:forEach>
	              <c:if test="${winOrder.size()<40}">
									<script type="text/javascript">

										Base.winOrderAdd(40-${winOrder.size()},'${nameArr}');
									</script>
								</c:if>
	          </ul>
          </div>
          <div class="news-title clearfix">
						<h2 class="news-tit fl">
							<strong>中奖排行</strong>
						</h2>
					</div>
					<div class="news-bar-side">
                        <table width="100%" cellspacing="0" cellpadding="0">
				  <colgroup>
				  <col width="10%">
				  <col width="40%">
				  <col width="50%">
				  </colgroup>
				<tbody id="drawList">
					<%--<c:forEach items="${oneDayWinTotal}" var="total" varStatus="to">--%>
					<%--<tr class="top">--%>
						<%--<td class="tc"><span class="top${to.index+1}_num">${to.index+1}</span></td>--%>
						<%--<td class="tc">--%>
							<%--<c:choose>--%>
								<%--<c:when test="${oneDayWinTotal.size()<7}">--%>
									<%--${fn:substring(total.account,0,2)}--%>
								<%--</c:when>--%>
								<%--<c:otherwise>--%>
									<%--${fn:substring(total.account,0,2)}***--%>
								<%--</c:otherwise>--%>
							<%--</c:choose>--%>
						<%--</td>--%>
						<%--<td class="tr p-r red">--%>
							<%--<c:choose>--%>
								<%--<c:when test="${oneDayWinTotal.size()<7}">--%>
									<%--${total.result}--%>
								<%--</c:when>--%>
								<%--<c:otherwise>--%>
									<%--<fmt:formatNumber value="${total.result}" type="number" maxFractionDigits="2" minFractionDigits="2"/>--%>
								<%--</c:otherwise>--%>
							<%--</c:choose>--%>
						<%--元</td>--%>
					<%--</tr>--%>
					<%--</c:forEach>--%>
					<%--<c:if test="${oneDayWinTotal.size()<7}">--%>
						<%--<script type="text/javascript">--%>
							<%--Base.oneDayWinTotalAdd(8);--%>
						<%--</script>--%>
					<%--</c:if>--%>
				</tbody>
				</table>
						<script>
							$(function () {
								$.ajax({
									url: '${base}/getConfig/getWinData.do',
									dataType:'json',
									type:'post',
									data:{},
									success:(function (data) {
									    var html = ''
										$.each(data,function(index,item){
										    html += '<tr class="top">' +
												'<td class="tc"><span class="top'+(index + 1)+'_num">'+(index + 1)+'</span></td>' +
												'<td class="tc">'+item.account+'</td>' +
												'<td class="tr p-r red">'+item.winMoney+'元</td>' +
												'</tr>'
											if(index == 5)return false
										})
										$("#drawList").html(html)
                                    })
								})
                            })
						</script>
                    </div>
				</div>
		</div>
		<c:if test="${!isJoint}">
		<div class="part-3 clearfix js-lazy">
				<div class="part-2-cont-right part-right">
					<div class="jc-box">
						<div class="title-tab-box clearfix">
							<h2 class="news-tit fl mr10">
								<strong>合买专区</strong>
							</h2>
							<ul class="tab-box fl">
								<li class="fl mr10 on" data-type=""><a href="javascript://">所有彩种</a></li>
								<c:forEach items="${lotList}" varStatus="l" var="lot">
									<c:if
										test="${lot.code ne 'FC3D' && lot.code ne 'PL3' && lot.code ne 'LHC' && lot.code ne 'FFC' && lot.code ne 'EFC' && lot.code ne 'TJSSC' && lot.code ne 'SH11X5'}">
										<li class="fl mr10" data-type="${lot.code}"><a
											href="javascript://">${lot.name}</a></li>
									</c:if>
								</c:forEach>
							</ul>
							<div class="hot-news fr">
								<a href="${base}/lotteryV3/joint.do" target="_blank" class="mr8">更多合买&gt;&gt;</a>
							</div>
						</div>
						<div class="zqdc-box matchbox hidden" style="display: block;">
							<table cellpadding="0" cellspacing="0" class="zdqc-table"
								border="0" id="jq_match_list">
								<thead>
									<tr>
										<td width="75"><span>彩种</span></td>
										<td>方案编号</td>
										<td>发起人</td>
										<td>提成</td>
										<td>每份金额</td>
										<td>详情</td>
										<td width="13%">进度</td>
										<td width="66" class="bor">剩余份数</td>
										<td width="66" class="bor">认购份数</td>
										<td>操作</td>
									</tr>
								</thead>
								<tbody id="IndexJointPurchase">

								</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
		</c:if>
		<c:choose>
			<c:when test="${domainFolder eq 'd00308'}">

			</c:when>
			<c:otherwise>
				<div class="yhdfoot">
					<ul>
						<li><strong>安全保障</strong>多重安全机制全程保护</li>
						<li><strong>支付便捷</strong>微信、支付宝、网银、信用卡</li>
						<li><strong>彩种齐全</strong>支持多个彩种，任意购买</li>
						<li style="border:none;"><strong>领奖无忧</strong>奖金自动返至购彩账户</li>
					</ul>
				</div>
			</c:otherwise>
		</c:choose>

    </div>
	<script type="text/javascript">
		var iL = $("#isLogin").val();
		var isJoint = ${isJoint};
		$(function() {
			initLunBo();
			initGg();
			$('.myScroll').myScroll({
				speed: 40, //数值越大，速度越慢
				rowHeight: 25 //li的高度
			});
			if (!isJoint) {Base.getLotJointData();}
			$(".notice-tab li.tab_g").mouseover(
					function() {
						$(this).addClass("on").siblings().removeClass("on");
						var i = $(this).index();
						$(".notice-main").find('.draw-contents:eq(' + i + ')')
								.show().siblings().hide();
					})

			//自助服务
			$(".help-tab li").mouseover(function() {
				$(this).addClass("on").siblings().removeClass("on");
				var $class = $(this).find('a').attr("class");
				$(this).parent().siblings('ul').hide();
				$(this).parent().siblings('.' + $class + '-box').show();
			})
		})

		function initLunBo() {
			$.ajax({
				url : '${base}/getConfig/getLunBo.do',
				success : function(data) {
				if (data.length > 0) {
					var temp = "", i = 0;
					$.each(
						data,
						function(index, item) {
							i++;
							temp += '<li><a href="'+item.titleUrl+'"><img src="${base}'+item.titleImg+'" /></a></li>'
						})
						$("#Slide .slidebox").html(temp);
						//加载图片完成启动轮播定时器
						if (i > 1) {
							startQuartz();
						}
					}
				}
			})
		}

		function startQuartz() {
			$("#Slide").slide({
				affect : 4, //切换状态1-4
				time : 5000,
				speed : 1000,
				//dot_text:false,
			});
		}

		function initGg() {
			// 最新消息跑馬燈
			$.ajax({
				url : "${base}/getConfig/getArticle.do",
				data : {
					code : 13
				},
				type : "post",
				dataType : 'json',
				success : function(r) {
					var col = '';
					for (var i = 0; i < r.length; i++) {
						col += r[i].content + "&nbsp;&nbsp;&nbsp;";
					}
					$("#scroll ul li").html(col);
					$("#scroll").marquee({
				        direction:"left",
				        speed:30
				    });
				}
			});
		}
	</script>
	<c:if test="${domainFolder == 'x00204'}"><jsp:include page="${base }/member/x00204/ArticleList.jsp"></jsp:include></c:if>
	<c:if test="${domainFolder != 'x00204'}"><jsp:include page="include/dialog_gonggao.jsp"><jsp:param value="index" name="dialog_gg"/></jsp:include></c:if>
	<jsp:include page="${MemberFolder }/${stationFolder}/include/footers.jsp"></jsp:include>
	<!-- 统计 -->
	<div style="display: none">${accessStatisticsCode}</div>
	<!-- 红包功能 -->
	<c:if test="${isRedPacket eq 'on'}">
		<script src="${base}/common/js/redpacket/redbag.js?v=1.2.2" path="${base}"></script>
	</c:if>
	<div style="position: fixed; left: 0; bottom: 0;display: flex;align-items:flex-end;z-index:99999;">
		<c:if test="${isZpOnOff eq 'on'}" >
			<jsp:include page="/common/include/turnlate.jsp"></jsp:include>
		</c:if>
		<c:if test="${isQdOnOff eq 'on'}">
			<jsp:include page="/common/include/exchange.jsp"></jsp:include>
		</c:if>
	</div>
</body>
</html>
