<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.e4393c{
		color:#e4393c;
		margin-left:8px;
	}
	.e4393c:hover{
		color:#000;
		text-decoration: underline;
	}
	#userName{
		border: 1px #c5c5c5 solid;
	    width: 200px;
	    height: 24px;
	    margin: 5px 0;
	    line-height: 24px;
	    padding-left:10px;
	}
	.addBankText{
		display: none;
	}
	.addBankText input[type="text"]{
		width:200px;
		padding:5px 5px;
	}
	.updatePassword{
		display: none;
	}
	.updatePassword input[type="password"]{
		width:200px;
		padding:5px 5px;
	}
	.setPayPass{
		display: none;
	}
	.setPayPass input[type="password"]{
		width:200px;
		padding:5px 5px;
	}
	.updatePayPass{
		display: none;
	}
	.updatePayPass input[type="password"]{
		width:200px;
		padding:5px 5px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<input type="hidden" value="${domainFolder}" id="domainFolder" />
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">账号信息</div>
            <div class="userMain mgb10">
                <div class="fix tapli" style="padding-left:130px;">
                    <!--  <div class="selectHeadImg"><img src="images/4eda6ad3f56b4f17.jpg"  width="100" height="100" alt="" class="headImg"><a>修改头像</a></div>-->
                    <ul class="personalInfo submitContent">
                        <li><span>用户名：</span>
                            <ins style="font-weight:600;">${loginMember.account}</ins>
                        </li>
                        <c:if test="${empty member.userName}">
	                        <li><span>真实姓名：</span>
	                            <ins style="color:#999">真实姓名必须与出款银行一致！一经填写，不可以自行修改。</ins>
	                        </li>
	                        <li><span></span>
	                            <ins><input id="userName" type="text" placeholder="请输入真实姓名"></ins>
	                        </li>
	                        <li><span></span>
	                        	<ins><input class="submitBtn" style="border:0;" value="确认" type="submit" onclick="Base.xinXi.subUserName();"></ins>
	                        </li>
                         </c:if>
                         <c:if test="${not empty member.userName}">
                         	 <li><span>真实姓名：</span>
	                            <ins id="memberUserName">${member.userName}</ins>
	                         </li>
	                         <li class="realName"><span></span>
                         		 <ins>如需更改真实姓名请联系<a href="${kfUrl }" target="_balnk" style="color:#e4393c;">在线客服</a>。</ins>
                         	</li>
							 <c:if test="${_title == '永利娱乐官方平台'}">
							 <script>
								 $(function(){
									let name =  $("#memberUserName").text()
									function changeName(){
										let newName="";
										if (name.length === 2){
											newName = name.substr(0,1) + "*";
										}else if(name.length > 2){
											let char="";
											for (let i=0,len = name.length -2;i<len;i++){
												char +="*";
											}
											newName = name.substr(0, 1) + char + name.substr(-1, 1);
										}else {
											newName = name
										}
										return newName;
									 }
									 $("#memberUserName").text(changeName(name))
								 })

							 </script>
							 </c:if>
                         </c:if>
                         <c:if test="${empty member.cardNo}">
                         	 <li><span>银行卡：</span>
	                            <ins>暂未绑定请先<a class="e4393c" href="javascript:(0)" onclick="showBankText()">[添加银行卡]</a></ins>
	                         </li>
                         	 <li class="addBankText"><span>完善银行卡信息：</span>
	                            <ins>
	                            	<select id="bankName" style="width: 200px;position: static;opacity: 1;padding:5px 5px;">
												<option value="建设银行">建设银行</option>
												<option value="工商银行" selected="selected">工商银行</option>
												<option value="农业银行">农业银行</option>
												<option value="中国邮政银行">中国邮政银行</option>
												<option value="中国银行">中国银行</option>
												<option value="中国招商银行">中国招商银行</option>
												<option value="中国交通银行">中国交通银行</option>
												<option value="中国民生银行">中国民生银行</option>
												<option value="中信银行">中信银行</option>
												<option value="中国兴业银行">中国兴业银行</option>
												<option value="浦发银行">浦发银行</option>
												<option value="平安银行">平安银行</option>
												<option value="华夏银行">华夏银行</option>
												<option value="广州银行">广州银行</option>
												<option value="BEA东亚银行">BEA东亚银行</option>
												<option value="广州农商银行">广州农商银行</option>
												<option value="顺德农商银行">顺德农商银行</option>
												<option value="北京银行">北京银行</option>
												<option value="杭州银行">杭州银行</option>
												<option value="温州银行">温州银行</option>
												<option value="上海农商银行">上海农商银行</option>
												<option value="中国光大银行">中国光大银行</option>
												<option value="渤海银行">渤海银行</option>
												<option value="浙商银行">浙商银行</option>
												<option value="晋商银行">晋商银行</option>
												<option value="汉口银行">汉口银行</option>
												<option value="上海银行">上海银行</option>
												<option value="广发银行">广发银行</option>
												<option value="深圳发展银行">深圳发展银行</option>
												<option value="东莞银行">东莞银行</option>
												<option value="宁波银行">宁波银行</option>
												<option value="南京银行">南京银行</option>
												<option value="北京农商银行">北京农商银行</option>
												<option value="重庆银行">重庆银行</option>
												<option value="广西农村信用社">广西农村信用社</option>
												<option value="吉林银行">吉林银行</option>
												<option value="江苏银行">江苏银行</option>
												<option value="成都银行">成都银行</option>
												<option value="尧都区农村信用联社">尧都区农村信用联社</option>
												<option value="浙江稠州商业银行">浙江稠州商业银行</option>
												<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
										</select>
	                            </ins>
	                         </li>
	                           <li class="addBankText"><span></span>
	                         	<ins><input class="text" id=userName type="text" placeholder="请输入开卡人姓名"></ins>
	                         </li>
	                         <li class="addBankText"><span></span>
	                         	<ins><input class="text" id="province" type="text" placeholder="请输入开卡省份"></ins>
	                         </li>
	                         <li class="addBankText"><span></span>
	                         	<ins><input class="text" id="city" type="text" placeholder="请输入开卡城市"></ins>
	                         </li>
	                         <li class="addBankText"><span></span>
	                         	<ins><input class="text" id="bankcardaddress" type="text" placeholder="请输入开卡网点"></ins>
	                         </li>
	                         <li class="addBankText"><span></span>
	                         	<ins><input class="text" id="bankId" type="text" placeholder="请输入银行卡号"></ins>
	                         </li>
	                         <li class="addBankText"><span></span>
	                        	<ins><input class="submitBtn" style="border:0;" value="确认" type="submit" onclick="Base.xinXi.setBanks();"></ins>
	                        </li>
                         </c:if>
                         <c:if test="${not empty member.cardNo}">
                         	<li><span>银行卡：</span>
                         		 <ins>已绑定 |  ${member.bankName}</ins>
                         	</li>
                         	<li><span></span>
                         		 <ins>如需换绑银行卡请联系<a href="${kfUrl }" target="_balnk" style="color:#e4393c;">在线客服</a>。</ins>
                         	</li>
                         </c:if>
                        <li><span>登录密码：</span>
                            <ins><a class="e4393c" onclick="updatePassword()">[修改]</a></ins>
                        </li>
                        <li class="updatePassword"><span>登录密码修改：</span>
                            <ins style="color:#999;">请输入6-16个英文或数字，且符合0~9或a~z字元</ins>
                        </li>
                        <li class="updatePassword"><span></span>
                            <ins><input class="text" id="updOLoginPwd" placeholder="请输入原登录密码" type="password"></ins>
                        </li>
                        <li class="updatePassword"><span></span>
                            <ins><input class="text" id="updLoginPwd" placeholder="请输入新登录密码" type="password"></ins>
                        </li>
                        <li class="updatePassword"><span></span>
                            <ins><input class="text" id="updRLoginPwd" placeholder="请再次输入密码" type="password"></ins>
                        </li>
                        <li class="updatePassword"><span></span>
                            <ins><input class="submitBtn" style="border:0;" value="确认" onclick="Base.xinXi.updPwd(1);" type="button"></ins>
                        </li>
                        <c:if test="${ empty member.receiptPwd }">
                        	<li><span>提款密码：</span>
                        		<ins>未设置 <a class="e4393c" href="javascript:(0)" onclick="setPayPass()">[立即设置]</a></ins>
                        	</li>
                        	<li><span></span>
                        		<ins><span>必须设置提款密码，才能进行提款！<span style="color: red;">不是您的银行卡密码，是网站的提款安全密码!</span></span></ins>
                        	</li>
                        	<li class="setPayPass"><span>提款密码设置：</span>
                        		<ins><input class="text"  id="payPwd" placeholder="请输入提款密码，最少6位数字"  type="password"></ins>
                        	</li>
                        	<li class="setPayPass"><span></span>
                        		<ins><input class="text" id="rPayPwd" placeholder="请再次输入" type="password"></ins>
                        	</li>
                        	<li class="setPayPass"><span></span>
                        		<ins><input class="submitBtn" style="border:0;" value="确认" onclick="Base.xinXi.setPayPwd();" type="button"></ins>
                        	</li>
                        </c:if>
                        <c:if test="${not empty member.receiptPwd}">
                        	<li><span>提款密码：</span>
                        		<ins>已设置 <a class="e4393c" href="javascript:(0)" onclick="updatePayPass()">[立即修改]</a></ins>
                        	</li>
                        	<li><span></span>
                        		<ins style="color:#999;" id="domfol">提款密码需由6-16个字母和数字(A-Z和 0-9)组成</ins>
                        	</li>
                        	<li class="updatePayPass"><span>提款密码修改：</span>
                        		<ins><input class="text"  id="updOPayPwd" placeholder="请输入原提款密码"   type="password"></ins>
                        	</li>
                        	<li class="updatePayPass"><span></span>
                        		<ins><input class="text" id="updPayPwd" placeholder="请输入新提款密码" type="password"></ins>
                        	</li>
                        	<li class="updatePayPass"><span></span>
                        		<ins><input class="text" id="updRPayPwd" placeholder="请再次输入密码" type="password"></ins>
                        	</li>
                        	<li class="updatePayPass"><span></span>
                        		<ins><input class="submitBtn" style="border:0;" value="确认" onclick="Base.xinXi.updPwd(2);" type="button"></ins>
                        	</li>
                        </c:if>
                        	<li><span></span>
                        		<ins style="color:#999;"><span  style="color:red;">*</span>&nbsp;<strong>身份信息是您领取大奖的重要凭据！一经填写，不可自行修改。</strong></ins>
                        	</li>
                        	<li><span></span>
                        		<ins style="color:#999;"><span  style="color:red;">*</span>&nbsp;<strong>账户余额仅可提款至同名银行卡中，确保资金安全。</strong></ins>
                        	</li>
                    </ul>
                </div>
            </div>
        </div>
		</div>
		<script>
			function showBankText(){
				$(".addBankText").toggle(200)
			}
			function updatePassword(){
				$(".updatePassword").toggle(200)
			}
			function setPayPass(){
				$(".setPayPass").toggle(200)
			}
			function updatePayPass(){
				$(".updatePayPass").toggle(200)
			}
			$(function(){
				if ($("#domainFolder").val() == "x00210") {
					$("#domfol").text("提款密码需由6位数字(0-9)组成！")
				}
			})
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>
