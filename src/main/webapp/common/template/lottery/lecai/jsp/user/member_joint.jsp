<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：合买记录</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">
					您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label>
					<a id="user_level" href="javascript:void(0);" target="_blank"><img
						src="${base}/common/template/lottery/lecai/images/member/cb_0.gif"></a>
				</div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong
							id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}
					</span> <img id="balances_arrow"
						src="${base}/common/template/lottery/lecai/images/member/arrow_9x5_down.gif">
				</div>
				<a class="btn recharge normal"
					href="${base}/center/banktrans/deposit/cp3Page.do">充值</a> <span><a
					class="btn withdrawal normal"
					href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox">
					<ul class="list">
						<li class="on" scripttype="0"><span class="left"></span> <span
							class="bg content"><a href="javascript:void(0);">发起合买</a></span>
							<span class="right"></span></li>
						<li scripttype="1"><span class="left"></span> <span
							class="bg content"><a href="javascript:void(0);">参与合买</a></span>
							<span class="right"></span></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="filter">
					<ul>
						<li class="li_joint">
							<div class="category">
								<select name="code" style="width: 100px;" id="lotCode">
									<option value="">所有彩种</option>
									<c:forEach items="${lotList}" var="lot">
										<option value="${lot.code}"
											<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
									</c:forEach>
								</select>
							</div>
						</li>
						<li class="li_joint">
							<div class="typeDiv">
								<select name="type" style="width: 90px;" id="type">
									<option value="">所有状态</option>
									<option value="1">未满员</option>
									<option value="2">已满员</option>
									<option value="6">已撤单</option>
								</select>
							</div>
						</li>
						<li id="pateJoint">
							<div class="statusDiv">
								<select name="status" style="width: 100px;" id="status">
									<option value="">所有状态</option>
									<option value="1">认购成功</option>
									<option value="2">已中奖</option>
									<option value="6">已失效</option>
								</select>
							</div>
						</li>
						<li>
							<div class="category_time">
								<select name="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
						<li>
							<div class="time-piece">
								<input name="startTime" id="startTime" class="text text75"
									readonly="readonly" value="${startTime}" type="text"
									data-end-date="${endTime}"> <span>至</span> <input
									name="endTime" id="endTime" class="text text75"
									readonly="readonly" value="${endTime}" type="text"
									data-end-date="${endTime}"> <input class="btn normal"
									type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="detail_explain">
					<ul>
						<li>彩种：<span id="lotNames">所有彩种</span>&nbsp;&nbsp;报表日期：<span
							id="orderBetListBetween">${startTime}——${endTime}</span></li>
						<li>贴心小提醒：点击显示详情能查看详细的合买订单</li>
					</ul>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0"
						cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>方案编号</td>
								<td>发起时间</td>
								<td>方案金额</td>
								<td>方案提成</td>
								<td>每份金额</td>
								<td width="17%">进度</td>
								<td>中奖金额</td>
								<td>状态</td>
								<td>详情</td>
							</tr>
							<tr class="title" id="tr_joint_pate" style="display:none;">
								<td>方案编号</td>
								<td>参与时间</td>
								<td>认购份数</td>
								<td>认购金额</td>
								<td>所占比例</td>
								<td>中奖金额</td>
								<td>状态</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
						</tbody>
						<!-- <tfoot id="orderBetOrderTblFoot">
							<tr>
								<td colspan="7" style="text-align: right">小计</td>
								<td class="red" id="orderBetPageAmount"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="7" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tfoot> -->
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var curNavFlag = 0;
		$(function() {
			$(".category select,.statusDiv select,.modelDiv select,.category_time select,.typeDiv select").chosen({
				disable_search : true
			});
			$('#endTime,#startTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			$("#pateJoint").hide();
			initRdsData(curNavFlag);

			$(".navbox ul li").click(function() {
				curNavFlag = $(this).attr("scripttype");
				$(this).addClass("on").siblings().removeClass("on");
				if(curNavFlag == 0){
					$("#pateJoint").hide().siblings(".li_joint").show();
					$("#tr_joint_pate").hide().siblings('tr').show();
				}else{
					$("#pateJoint").show().siblings(".li_joint").hide();
					$("#tr_joint_pate").show().siblings('tr').hide();
				}
				$("#pageNumber").val(1);
				initRdsData(curNavFlag);
			})
			
			$("#betResultOrder").on("click","div",function(){
				var pid=$(this).attr("pro_id");
				if($(this).hasClass("show-details")){
					$(this).text("隐藏详情");
					$(this).attr("class","hide-details");
					$("#tr_"+pid).show();
				}else{
					$(this).text("显示详情");
					$(this).attr("class","show-details");
					$("#tr_"+pid).hide();
				}
			})

			$(".category_time select").change(function() {
				var v = $(this).val();
				quickSelDate(parseInt(v));
			})

		})

		function search() {
			$("#pageNumber").val(1); //初始化页数
			initRdsData(curNavFlag);
		}

		function initRdsData(flag) {
			var temp = "",colspan=9,url="${base}/lotteryV3/joint/userJoint.do", data = {
					code : $("#lotCode").val(),
					type : $("#type").val(),
					startTime : $("#startTime").val() + " 00:00:00",
					endTime : $("#endTime").val() + " 23:59:59",
					rows : 10,
					page : $("#pageNumber").val()
				};
			//报表日期
			if(flag == 1){
				colspan = 7;
				data = {
					status : $("#status").val(),
					startTime : $("#startTime").val() + " 00:00:00",
					endTime : $("#endTime").val() + " 23:59:59",
					rows : 10,
					page : $("#pageNumber").val()
				}
				url = "${base}/lotteryV3/jointpate/userPate.do";
			}
			$("#orderBetListBetween").html(
					$("#startTime").val() + "——" + $("#endTime").val());
			$("#lotNames").html($("#lotCode").find("option:selected").text());
			var load = new Loading();
			load.init({
				target : "#betResultOrder"
			})
			load.start();
			$.ajax({
				url : url,
				data : data,
				success : function(res) {
					if (res.page.totalCount == 0) {
						//无数据
						temp = '<tr><td colspan="'+colspan+'" class="nodate" height="200"><img src="${base}/common/template/lottery/lecai/images/joint/T1LAbBXlhnXXaMli7Z-341-110.jpg"></td></tr>';
						$("#betResultOrder").html(temp);
						$("#orderBetOrderTblFoot").hide();
						layerPage(res.page.totalCount,
								res.page.currentPageNo,
								res.page.totalPageCount);
					} else {
						if(flag==0){
							addTemplate(res);
						}else{
							addTemplatePate(res);
						}
					}
					load.stop();
				}
			})
		}
		
		function addTemplatePate(res){
			var temp = "",statusName="";
			$.each(res.page.list,function(index,item){
				switch (item.status) {
				case 1:
					statusName = '<span style="background-color: #428bca;">认购成功</span>';
					break;
				case 2:
					statusName = '<span style="background-color: #5cb85c;">已中奖</span>';
					break;
				case 3:
					statusName = '<span style="background-color: #d9534f;">认购失效</span>';
					break;
				}
				temp += '<tr '+ (index % 2 != 0 ? 'class="bgcolor"': '')+ '>';
				temp += '<td>'+item.programId+'</td>';
				temp += '<td>'+item.createTime+'</td>';
				temp += '<td><strong class="red">'+item.branchNum+'</strong>份</td>';
				temp += '<td><strong class="red">'+item.totalMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
				temp += '<td>'+item.szBFL+'%</td>';
				temp += '<td><strong class="red">'+(item.winMoney==null?0:item.winMoney)+'</strong>${not empty cashName?cashName:"元"}</td>';
				temp += '<td>' + statusName + '</td></tr>';
			})
			
			$("#betResultOrder").html(temp);
			layerPage(res.page.totalCount, res.page.currentPageNo,res.page.totalPageCount);
		}

		function addTemplate(res) {
			var temp = "", statusName = "";
			$.each(res.page.list,function(index, item) {
								switch (item.type) {
								case 1:
									statusName = '<span style="background-color: #428bca;">认购中</span>';
									break;
								case 2:
									statusName = '<span style="background-color: #5cb85c;">已满员</span>';
									break;
								case 3:
									statusName = '<span style="background-color: #d9534f;">已截止</span>';
									break;
								case 4:
									statusName = '<span style="background-color: #5bc0de;">已完成</span>';
									break;
								case 5:
									statusName = '<span style="background-color: #f0ad4e;">已失效</span>';
									break;
								case 6:
									statusName = '<span style="background-color: #f0ad4e;">已撤单</span>';
									break;
								}
								temp += '<tr '+ (index % 2 != 0 ? 'class="bgcolor"': '')+ '>';
								temp += '<td>'+item.programId+'</td>';
								temp += '<td>'+item.createTime+'</td>';
								temp += '<td><strong class="red">'+item.totalMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
								temp += '<td>'+item.commission+'%</td>';
								temp += '<td><strong class="red">'+item.oneMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
								temp += '<td style="text-align:left;padding-left:5px;">';
								temp += '<span style="margin-left:0px;;color:000;padding:6px;">'+(item.surNum/item.branchNum*100).toFixed(2)+'%</span> + <span style="margin-left:0px;color:#fff;background:#b90000;border-radius:0;">'+(item.them/item.branchNum*100).toFixed(2)+'%保</span><br>';
								temp += '<div style="width:120px;margin-top:3px;height:6px; font-size:0px; line-height:0px;border:1px solid #b90000;background:#fff;">';
								temp += '<img src="/game/common/template/lottery/lecai/images/joint/bar_space.gif" width="'+(item.surNum/item.branchNum*100).toFixed(2)+'%" height="6"></div></td>';
								temp += '<td><strong class="red">'+(item.winTotalMoney==null?'0':item.winTotalMoney)+'</strong>${not empty cashName?cashName:"元"}</td>';
								temp += '<td>' + statusName + '</td>';
								temp += '<td><div class="show-details" pro_id="'+item.programId+'">显示详情</div></td></tr>';
								temp += '<tr id="tr_'+item.programId+'" style="display:none;" class="details-bgcolor"><td id="td_'+item.programId+'" colspan="9" class="td-border-left">';
								temp += '<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tablelay eng"><tbody><tr><th>序号</th><th>玩法</th><th>投注期号</th><th>倍数</th><th>本期投入</th><th>累计投入</th><th>出票状态</th><th>开奖号码</th><th>中奖情况</th><th>总奖金</th><th>每份奖金</th></tr>';
								$.each(res[item.programId],function(index,it){
									temp += '<tr><td>'+(index+1)+'</td>';
									temp += '<td>'+it.playName+'</td>';
									temp += '<td>'+it.orderId+'</td>';
									temp += '<td>'+it.multiple+'</td>';
									temp += '<td>￥'+it.buyMoney+'${not empty cashName?cashName:"元"}</td>';
									temp += '<td>￥'+it.buyMoney+'${not empty cashName?cashName:"元"}</td>';
									temp += '<td><strong class="green eng">'+(item.type==1?'出票中':item.type==2 || item.type==3 || item.type==4?'出票成功':item.type==5?'出票失败(方案失效)':'出票失败(撤单)')+'</strong></td>';
									temp += '<td><strong class="red eng">'+(it.lotteryHaoMa==null?'未开奖':it.lotteryHaoMa)+'</strong></td>';
									temp += '<td><strong class="blue eng">'+(it.status==1?'未开奖':it.status==2?'已中奖':it.status==3?'未中奖':it.status==4?'已撤单':it.status==5?'回滚成功':it.status==6?'回滚异常':it.status==7?'开奖异常':it.status==8?'和':'合买失效')+'</strong></td>';
									temp += '<td>'+(it.status==2 && it.winMoney != null?it.winMoney:'--')+'</td>';
									temp += '<td>'+(it.status==2 && it.winMoney != null?'￥<strong class="red eng">'+(it.winMoney/item.branchNum).toFixed(3)+'</strong>${not empty cashName?cashName:"元"}':'--')+'</td></tr>';
								})
								temp += '</table></td></tr>';
							})

			$("#betResultOrder").html(temp);
			layerPage(res.page.totalCount, res.page.currentPageNo,res.page.totalPageCount);
		}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>
