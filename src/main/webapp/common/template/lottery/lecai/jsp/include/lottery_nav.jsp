<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/lottery_nav.css?v=${caipiao_version}"
	type="text/css">
<div class="gm_con">
	<div class="gm_con_to">
		<div style="margin-top: 5px;" class="unit_title">
			<div class="ut_l"></div>
			<div class="ut_r"></div>
		</div>
		<div class="gct_l">
			<div class="game-icon1">
				<img
					src="${base}/common/template/lottery/lecai/images/gclogo/${bly.code}.png"
					alt="">
			</div>
			<p class="time-title">
				<span id="openTitle">已开盘,距离投注截止还有</span> <span class="gdox"
					id="current_endtime" style="display: none;">2016-11-17
					15:09:00</span>
			</p>
			<div class="gct-time">
				<div class="gct-time-now">
					<div class="gct-time-now-l" id="count_down">
						<span class="leavehms"><span>0</span></span> <span
							class="leavehms"><span>0</span></span> <span class="interval">:</span>
						<span class="leavehms"><span>0</span></span> <span
							class="leavehms"><span>0</span></span> <span class="interval">:</span>
						<span class="leavehms"><span>0</span></span> <span
							class="leavehms"><span>0</span></span>
					</div>
				</div>
			</div>
			<h3 class="page_name"></h3>

			<div class="gct_now">
				<strong>第&nbsp;&nbsp;<span id="current_issue"
					class="color-green">无期号</span>&nbsp;&nbsp;期
				</strong> <br> 总共:&nbsp;&nbsp;<strong><span id="current_sale"
					class="color-green">${qishu}</span></strong>&nbsp;&nbsp;期<br> <a
					onclick="__openWin('lottery_hall','/trend/index.html?gameId=5&amp;periods=20');"
					target="_blank" class="bt01"><span class="zoushi"></span>号码走势</a>
			</div>
			<div class="clear"></div>
			<div class="gct_menu">
				<a class="gct_menu_yl" target="_blank"></a>
			</div>
		</div>
		<div id="showgd-box">
			<ul class="box-ul">
				<li><a attrid="#gd-box1" class="tabulous_active">近一期</a></li>
				<li><a class="" attrid="#gd-box2">近五期</a></li>
				<span class="tabulousclear"></span>
			</ul>
			<div id="tabs_container" style="height: 120px;" class="transition">
				<div id="gd-box1" style="position: absolute; top: 40px;"
					class="make_transist hideleft showleft">
					<p>
						<span class="page_name"></span> 第&nbsp;&nbsp;<b><span
							class="color-green" id="lt_gethistorycode"></span></b>&nbsp;&nbsp;期<span
							id="lt_opentimebox" style="display: none;">&nbsp;&nbsp; <span
							id="lt_opentimebox2"
							style="color: rgb(249, 206, 70); display: inline;"> <strong>&nbsp;&nbsp;正在开奖</strong>
						</span>
					</p>

					<ul class="lot-gd1" id="showcodebox"></ul>
				</div>
				<div id="gd-box2" class="hideleft make_transist"
					style="position: absolute; top: 40px;"></div>
			</div>
			<!--End tabs container-->
		</div>
		<div class="clear"></div>
	</div>
</div>