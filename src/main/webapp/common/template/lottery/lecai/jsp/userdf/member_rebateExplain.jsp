<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.userinfo-edit-main{
		border:none;
	}
	.searchDetail table th,td{
		font-size:12px;
	}
	.searchDetail table th,td{
		width:100px!important;
	}
	.searchDetail table{
		width:auto!important;
		overflow: auto;
	}
	.searchDetail .peilvPublic{
		float:left;
	}
	.searchDetail .peilvHead{
		width:220px;
		border-right:1px solid #dadada;
		position: absolute;
	}
	.searchDetail .peilvHead li{
		width:100%;
		height:36px;
		border-bottom:1px solid #dadada;
		background:#f3f3f3;
		text-align:center;
		line-height:39px;
		font-weight:600;
	}
	.searchDetail .peilvBody ul{
		float:left;
		width:100%;
		border-right:1px solid #dadada;
	}
	.searchDetail .peilvBody{
		width: 610px;
		padding-left:220px;
		box-sizing:content-box;
	}
	.searchDetail .peilvBody li{
		width:116px;
		height:35px;
		border-bottom:1px solid #dadada;
		text-align:center;
		line-height:39px;
		float:left;
		border-right:1px solid #dadada;
	}
	.searchDetail .peilvBody .heads{
		background:#f3f3f3;
	}
	.searchDetail::-webkit-scrollbar {/*滚动条整体样式*/
        width: 8px;     /*高宽分别对应横竖滚动条的尺寸*/
        height: 10px;
    }
	.searchDetail::-webkit-scrollbar-thumb {/*滚动条里面小方块*/
        border-radius: 10px;
         -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
        background: #535353;
    }
	.searchDetail::-webkit-scrollbar-track {/*滚动条里面轨道*/
        -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
        border-radius: 10px;
        background: #EDEDED;
    }
     #leftNav{
    	position: relative;
    	heihgt:36px;
    	width:219px;
    	z-index:9999;
    	display:none;
    }
     #leftNav:after {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        border-bottom: 34px solid #fff;
        border-right: 219px solid #f3f2f2;
        content: ""
    }
    #wanfa{
    	position: absolute;color: #000;z-index: 10;top: 11px;left: 4px;font-weight: 600;line-height:29px;
    }
    #fandian{
    	position: absolute;color: #000;z-index: 10;right: 4px;font-weight: 600;line-height:29px;
    }
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
				<jsp:include page="member_left_nav.jsp"></jsp:include>	
		<div class="userRight">
            <div class="userTitle mgb10">返点赔率表</div>
            <div class="userMain mgb10">
           			<ul class="searchFirst">
                    <li style="text-align:left;padding-left:20px;"><span>彩种：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
									<c:forEach items="${bigGroup}" var="lot">
										<option value="${lot.type}">${lot.name}</option>
									</c:forEach>
                                </select>
                            <em></em>
                         </ins>
                         <ins style="color:#e4393c">*由于数据量较多，如进行查询时出现卡顿请耐心等待或刷新页面</ins>
                    </li>
                </ul>
                <div id="leftNav" style="position: relative;"><div id="wanfa">玩法</div><div id="fandian">返点</div></div>
                <div class="searchDetail" style="overflow: auto;height:670px;position: relative;" id="div-table">
                    <div class="peilvHead peilvPublic">
                    	
                    </div>
                    <div class="peilvBody peilvPublic" >
                    	
                    </div>
                    <div class="loadingGif" style="padding: 100px 0px;display:none;text-align:center;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                </div>
            </div>
        </div>
		</div>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
	<script>
	$(function(){
		initRdsData($('#lotCode').val())
		$('#lotCode').change(function(){
            initRdsData($(this).val())
        })
	})
	$("#div-table").scroll(function(event){
		var t = document.getElementById("div-table").scrollTop
        , e = document.getElementById("div-table").scrollLeft
        $(".peilvHead").css('left',e+'px')
        $(".heads").css('top',t+'px')
        $(".peilvBody").css('padding-top','35px')
    });
	function initRdsData(type) {
		$(".peilvPublic").html('')
		$(".loadingGif").show()
		$("#leftNav").hide()
		 $(".peilvBody").css('padding-top','35px')
		$.ajax({
			url : '${base}/center/df/record/betrecord/getRebateOddsByLot.do',
			data : {
				lotType:type
			},
			success : function(res) {
				$(".loadingGif").hide()
				var headJson = []
				var head = '<ul style="position: relative;margin-top:36px;">';
				for(var i = 0; i < res.content.length; i++){
					head += '<li>'+res.content[i].name+'</li>'
				}
				head += '</ul>'
				$(".peilvHead").html(head)
				var head2 = '<ul class="heads"  style="position: absolute;top: 0px;">';
				$.each(res.content[0].items,function(index, item){
					headJson.push(index)
				})
				headJson.sort(sortNumber)
				$.each(headJson,function(index, item){
					head2 += '<li>'+item+'</li>'
				})
				$(".peilvBody").css('width',headJson.length*117+'px')
				head2 += '</ul>'
				var html = '';
				for(var i = 0; i < res.content.length; i++){
					var peilvJson = []
					html+='<ul>'
						$.each(res.content[i].items,function(index, item){
							peilvJson.push(item)
						})
						peilvJson.sort(sortNumber)
						$.each(peilvJson,function(index, item){
							html+='<li>'+item+'</li>'
						})
					html+='</ul>'
				}
				head2+=html
				$(".peilvBody").html(head2)
				$(".heads").css('width',headJson.length*117+'px')
				$("#leftNav").show()
			},
			error: function(res){
				
			}
		})
	}
	function sortNumber(a,b){
		  return b - a;
	}
	</script>
</body>
</html>