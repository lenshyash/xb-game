<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
	.userSelect{
		-webkit-appearance:menulist-button!important;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">取款日志</div>
            <div class="userMain mgb10">
                 <ul class="searchFirst zhangbian">
                 	<li><span> </span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   <option value="1">直属下级</option>
									<option value="2">所有下级</option>
                                </select>
                         </ins>
                    </li>
                	<li><span>类型：</span>
                        <ins class="selectIcon">
                                <select class=" userSelect" id="stype">
									<option value="0">全部类型</option>
									<option value="5">在线支付</option>
									<option value="6">快速入款</option>
									<option value="7">一般入款</option>
									<option value="1">人工加款</option>
								</select>
                         </ins>
                    </li>
                	<li><span>状态：</span>
                        <ins class="selectIcon">
                                <select class=" userSelect" id="sstatus">
								<option value="0">全部状态</option>
								<option value="1" class="text-success">处理中</option>
								<option value="2" class="text-primary">充值成功</option>
								<option value="3" class="text-danger">充值失败</option>
							</select>
                         </ins>
                    </li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                 </ul>
                 <ul class="searchFirst zhangbian">
                	<li class="_state" style="display: flex;align-items: center;padding-left:21px;">
                    	<span>&nbsp;</span>
                    	<%-- <input class="inputText" id="sagent" value="${sagent }" type="text" placeholder="订单号" style="background:#fff;height:30px;"> --%>
                    	<input class="inputText" id="saccount" value="${saccount }" type="text" placeholder="会员账号" style="background:#fff;height:30px;margin-left:10px;">
                    	<input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
       				</li>                    
                 </ul>
                <div class="searchDetail">
                    <table>
                       <thead>
							<tr class="title">
								<td>提款会员</td>
								<td>订单号</td>
								<td>开户行</td>
								<td>提款金额</td>
								<td>交易时间</td>
								<td>存取类型</td>
								<td>状态</td>
								<td>备注</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="7" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/sport.do">立即投注</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot" style="display:none;">
							<tr>
								<td colspan="3" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td colspan="2">&nbsp;</td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：账变记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
		        $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		        	initRdsData(1);
		        })
		        $('#stype').change(function(){
		        	initRdsData(1);
		        })
		        $('#sstatus').change(function(){
		        	initRdsData(1);
		        })
			})
			var currPage = 0;
		        function initRdsData(p){
					$(".notContent").hide()
			    	$(".loadingGif").show()
			    	if(!endTime){
						endTime = startTime
					}
					var data = {
						account: $("#saccount").val(),
						agent: $("#sagent").val(),
						searchType: $("#lotCode").val(),
						status: $("#sstatus").val(),
						type: $("#stype").val(),
						begin:startTime+' 00:00:00',
						end:endTime+' 23:59:59',
						pageSize:10,
						pageNumber:p
					}
					$.ajax({
						url:"${base}/daili/dldrawrd/list.do",
						data:data,
						success:function(res){
							$(".notContent").show()
					    	$(".loadingGif").hide()
							if(res.total == 0){
								//无数据
								$("#betResultOrder").html(noTakeNotes);
							}else{
								addTemplate(res);
								page = p;
								totalPage = Math.ceil((res.total/10))
								lotteryPage(res.total,totalPage)
								lastPage = totalPage
								$("#pageNum").html(p)
							}
						}
					})
				}
			function addTemplate(res){
				var temp ="", bd=null;
				$.each(res.rows,function(index,item){
					if(!item.remark){
						item.remark = '无备注'
					}else{
						item.remark = '<span onclick="showMessage(&quot;'+item.remark+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看备注</span>'
					}
					bd = new Date(item.bettingDate);
					temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>'+item.account +'</td>';
					temp += '<td>'+item.orderNo+'</td>';
					temp += '<td>'+item.bankName+'</td>';
					temp += '<td>'+item.drawMoney+'</td>';
					temp += '<td>'+dateFormatter(item.createDatetime)+'</td>';
					temp += '<td>'+typeFormatter(item.type)+'</td>';
					temp += '<td>'+statusFormatter(item.status)+'</td>';
					temp += '<td>'+item.remark+'</td>';
				})
				$("#betResultOrder").html(temp);/* 
				$("#orderBetAmount").html(res.aggsData.totalBetMoney||0);
				$("#orderWinAmount").html(res.aggsData.totalBetResult||0); */
				//$("#orderBetOrderTblFoot").show();
			}
			function statusFormatter(value) {
				return GlobalTypeUtil.getTypeName(1, 3, value);
			}

			function typeFormatter(value, row, index) {

				return GlobalTypeUtil.getTypeName(1, 1, value);
			}
			function toBetHtml(item, row) {
				var con = item.con;
				if (con.indexOf("vs") == -1) {
					con = '<span class="text-danger">' + con + '</span>';
				}
				var homeFirst = !(item.homeStrong === false);//主队是否在前
				var scoreStr = "";

				if (row.gameTimeType == 1) {
					if (homeFirst) {
						scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH + ":"
								+ row.scoreC + ")</b></font>";
					} else {
						scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC + ":"
								+ row.scoreH + ")</b></font>";
					}
				}
				var home = item.firstTeam;
				var guest = item.lastTeam;
				if (item.half === true && row.mix == 2) {
					home = home + "<font color='gray'>[上半]</font>";
					guest = guest + "<font color='gray'>[上半]</font>";
				}

				var html = item.league + "<br/>" + home + "&nbsp;" + con + "&nbsp;"
						+ guest + scoreStr + "<br/>" + "<font color='red'>"
						+ item.result + "</font>&nbsp;" + "@"
						+ "&nbsp;<font color='red'>" + item.odds + "</font>";
				var balance = row.mix != 2 ? row.balance : item.balance;
				if (balance == 4) {
					html = "<s style='color:red;'>" + html + "</s>"
				} else if (balance == 2 || balance == 5 || balance == 6) {
					var mr = row.mix != 2 ? row.result : item.matchResult;
					if (homeFirst) {
						html = html + "&nbsp;<font color='blue'>(" + mr + ")</font>";
					} else {
						var ss = mr.split(":");
						html = html + "&nbsp;<font color='blue'>(" + ss[1] + ":"
								+ ss[0] + ")</font>";
					}
				}
				return html;
			}
			function initCombo() {
				$.ajax({
					url : "${base}/daili/dlmnyrd/money/record/type.do",
					async : false,
					success : function(data) {
						var eachdata = {
							"data" : data
						};
						typeData = toTypeMap(data);
						var html = template('recordtype_tpl', eachdata);
						$("#type").append(html);
						var url =  location.href;
						if(url.indexOf('param=')!=-1){
							var obj = $('#type');
							if(url.indexOf('param=sdkk')!=-1){
								obj.find("option[value='2']").attr("selected",true); 
							}else if(url.indexOf('param=sdjk')!=-1){
								obj.find("option[value='1']").attr("selected",true); 
							}
						}
					}
				});
			}
			function toTypeMap(data) {
				var map = {};
				if (data) {
					for (var i = 0; i < data.length; i++) {
						if (!data[i]) {
							continue;
						}
						map[data[i].type] = data[i].name;
					}
				}
				return map;
			}
			function dateFormatter(value) {
				return DateUtil.formatDatetime(value);
			}
			</script>
			<script id="recordtype_tpl" type="text/html">
					{{each data as option}}
			        	<option value="{{option.type}}">{{option.name}}</option>
					{{/each}}
				</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>