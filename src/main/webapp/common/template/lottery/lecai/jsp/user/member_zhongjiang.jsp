<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：中奖记录</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">
					您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label>
					<a id="user_level" href="javascript:void(0);" target="_blank"
						title=""><!-- <img
						src="//static.lecai.com/img/caibei/level/cb_0.gif"> --></a>
				</div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong
							id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}
					</span>
				</div>
				<a class="btn recharge normal" value=" " href="${base}/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" value=" "
					href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="account">
				<div class="tit">
					<div class="account_switch" id="wallet_switch" style="min-width: 84.4px;">
						<label><strong id="overview" style="cursor: default;">账户总览</strong></label>
						<img src="${base}/common/template/lottery/lecai/images/member/arrow_9x5_down.gif">
					</div>
				</div>
				<ul class="clearfix" id="wallet_info">
					<li class="star">下注总额：<label id="buyAllMoney" class="red">${-allBetAmount}</label></li>
					<li>中奖总额：<label id="winAllMoney" class="red">${allWinAmount}</label></li>
					<li class="end">盈亏：<label id="yingKuiAllMoney" class="red">${yingkuiAmount}</label></li>
				</ul>
			</div>
			<div class="ordermain">
				<div class="navbox">
					<div class="prizetitle">中奖纪录</div>
					<div class="clear"></div>
				</div>
				<div class="filter">
					<ul>
						<li>
							<div class="category">
								<select name="code" class="chzn-done" id="lotCode">
										<option value="">所有彩种</option>
										<c:forEach items="${lotList}" var="lot">
											<option value="${lot.code}" <c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
										</c:forEach>
								</select>
							</div>
						</li>
						<li>
							<div class="category_time">
								<select name="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
							<li>
							<div class="time-piece">
								<input name="startTime" id="startTime" class="text text75"
									readonly="readonly" value="${startTime}" type="text" data-end-date="${endTime}">
								<span>至</span> <input name="endTime" id="endTime" class="text text75"
									readonly="readonly" value="${endTime}" type="text" data-end-date="${endTime}"> <input
									class="btn normal" type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>注单号|下注时间</td>
								<td>彩种|期号</td>
								<td>玩法</td>
								<td>倍数</td>
								<td>开奖号码</td>
								<td>下注内容</td>
								<td>投注金额</td>
								<td>中奖金额</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot">
							<tr>
								<td colspan="6" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var curNavFlag = 0;
		$(function(){
			$(".category select,.category_time select").chosen({disable_search:true});
			$('#startTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			$('#endTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			
			initRdsData(curNavFlag);
			
			$(".category_time select").change(function() {
				var v = $(this).val();
				quickSelDate(parseInt(v));
			})
		})
		
		function search(){
			$("#pageNumber").val(1);
			initRdsData(curNavFlag);
		}
		
		function initRdsData(flag){
			
			var load = new Loading();
			load.init({
				target:"#betResultOrder"
			})
			load.start();
			var data = {
					code: $("#lotCode").val(),
					startTime:$("#startTime").val()+" 00:00:00",
					endTime:$("#endTime").val()+" 23:59:59",
					status:2,
					rows:10,
					page:$("#pageNumber").val()
			}
			$.ajax({
				url:"${base}/lotteryV3/betInfo/getBcLotteryOrder.do",
				data:data,
				success:function(res){
					if(res.page.totalCount == 0){
						//无数据
						temp = '<tr><td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td></tr>';
						$("#betResultOrder").html(temp);
						$("#orderBetOrderTblFoot").hide();
						//$("#buyAllMoney,#winAllMoney,#yingKuiAllMoney").html("0.00");
						layerPage(res.page.totalCount,
								res.page.currentPageNo,
								res.page.totalPageCount);
					}else{
						addTemplate(res);
						//$("#buyAllMoney").html(res.page.aggsData.buySum);
						//$("#winAllMoney").html(res.page.aggsData.winSum);
						//$("#yingKuiAllMoney").html((res.page.aggsData.winSum - res.page.aggsData.buySum).toFixed(2));
					}
					load.stop();
				}
			})
		}
		
		function addTemplate(res){
			var temp ="";
			$.each(res.page.list,function(index,item){
				temp += '<tr '+(index%2!=0?'class="bgcolor"':'')+'><td><a href="javascript:void(0);" class="BetInf" orderid="'
					+item.orderId+'" lotcode="'+item.lotCode+'">'+item.orderId+'</a><br>'+item.createTime+'</td>';
				temp += '<td>'+item.lotName+'<br>'+item.qiHao+'</td>';
				temp += '<td>'+item.playName+'</td>';
				temp += '<td>'+item.multiple+'</td>';
				temp += '<td class="red">'+(!item.lotteryHaoMa?"- -":item.lotteryHaoMa)+'</td>';
				temp += '<td style="width:170px;word-break: break-all;text-align:center;">'+item.haoMa+'</td>';
				temp += '<td>'+item.buyMoney+'</td>';
				temp += '<td>'+item.winMoney+'</td></tr>';
			})
			$("#betResultOrder").html(temp);
			$("#orderBetOrderTblFoot").show();
			$("#orderBetAmount").html(res.sumBuyMoney);
			$("#orderWinAmount").html(res.sumWinMoney);
			layerPage(res.page.totalCount, res.page.currentPageNo, res.page.totalPageCount);
		}
		
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>