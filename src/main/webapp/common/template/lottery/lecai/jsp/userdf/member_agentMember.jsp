<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">会员管理</div>
            <div class="userMain mgb10">
                <ul class="searchFirst zhangbian" style="line-height: 50px;">
                	<li><span>&nbsp;</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="searchType">
                                   <option value="2">所有下级</option>
                                   <option value="1">直属下级</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
       				<li><span>用户类型：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   <option value="0">全部</option>
                                   <option value="4">代理</option>
                                   <option value="1">玩家</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                     <li class="_state" style="display: flex;align-items: center;">
                    	<span>账号：</span>
                    	<input class="inputText" id="account" value="" type="text" placeholder="会员账号" style="background:#fff;height:30px;margin-left:10px;">
                    	<input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
       				</li>
                    <li class="_time" id="selected_time">
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr class="searchTable">
                          	<th>账号</th>
                            <th>用户类型</th>
                            <th>余额</th>
                            <th>最后登录</th>
                            <th>最后登录ip</th>
                            <th>操作</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1);
		        $('.userSelect').change(function(){
		        	initRdsData(1);
		        })
			})
			function initRdsData(p,id,is){
				$(".Records_listCont").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
				var data = {
					searchType:$("#searchType").val(),
					account: $("#account").val(),
					accountType : $("#lotCode").val(),
					rows : 10,
					page : p
				}
				if(is == 0){
					data.agentParentId = id
				}else if(is == 1){
					data.agentId = id
				}
				$.ajax({
					url : '${base}/center/member/meminfo/accountList.do',
					data : data,
					success : function(res) {
						console.log(res)
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.totalCount == 0){
							//无数据
							var html = '';
							html = '<tr><td colspan="6">暂无下级,<a style="color:#4aa9db;" onclick="initRdsData(1)">查看所有会员</a></td></tr>'
							$(".Records_listCont").html(html)
							$(".page").hide()
						}else{
							addTemplate(res);
							$(".page").show()
							page = res.currentPageNo;
							lotteryPage(res.totalCount,res.totalPageCount)
							lastPage = res.totalPageCount
							$("#pageNum").html(res.currentPageNo)
							
						}
					}
				})
			}
			function addTemplate(res){		
				var html='',agent='' ,name = $("#agentName").val();
				$.each(res.list,function(index, item){
					if(item.agentId != 0 && item.agentName !=name && item.accountType != 1){
						agent = '<a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.agentId+'&quot;,1)">上级</a><a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.accountId+'&quot;,0)">下级</a>'
					}else if(item.agentId != 0 && item.agentName ==name && item.accountType != 1){
						agent = '<a style="font-size:12px;color:#4aa9db;" onclick="initRdsData(0,&quot;'+item.accountId+'&quot;,0)">下级</a>'
					}else{
						agent = ''
					}
					html+='<tr><td onclick="showMessage(&quot;'+item.parentNames+' > '+item.account+'&quot;)">'+item.account+'</td><td>'+userType[item.accountType]+'</td><td>'+item.money+'</td><td>'+getMyDate(item.lastLoginDatetime)+'</td><td>'+item.lastLoginIp+'</td><td style="text-align:left;"><div class="manage"><a href="${base}/center/df/record/betrecord/cpDfPage.do?form=order&account='+item.account+'" title="投注明细"  style="color:#4aa9db"><i class="iconfont"></i></a><a href="${base}/center/df/record/betrecord/teamChange.do?account='+item.account+'" title="交易明细"  style="color:#4aa9db"><i class="iconfont"></i></a>'+agent+'</div></td></tr>'
				})
				$(".Records_listCont").html(html)
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>