<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/header.css?v=${caipiao_version}"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/style.css?v=${caipiao_version}"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/layer_dialog.css?v=${caipiao_version}"
	type="text/css">
<script type="text/javascript">
	LECAI = {
		base : '${base}',
		folder : "${stationFolder}",
		caipiao_version : '${caipiao_version}',
		caipiao : '${caipiao eq "on"?true:false}',
		code : '${bly.code}',
		type : '${bly.type}',
		fanShui : '${fanShui eq "on"?true:false}',
	}
</script>
<div class="header-plus" id="header_plus">
	<input type="hidden" value="${base}" id="base" />
	<div class="head-simple-toptray-bd clearfix auto990 header">
		<div class="logo-box" style="width: 195px; padding: 0; float: left;">
			<a title="${_title}" href="${base}">
				<h1>${_title}</h1> <img title="${_title}" alt="${_title}"
				src="${base}/common/template/lottery/lecai/images/stationLogo/${empty indexLogo?folder:indexLogo}"
				height="36">
			</a>
		</div>
		<div class="toptray-right clearfix">
			<div class="toptray-right-r" style="float: right;">
			<c:choose>
					<c:when test="${isLogin eq true}">
				<ul id="toptray_login" class="hidden toptray clearfix"
					style="display: block;">
					<li>您好&nbsp;<span id="toptray_username">用户名：${loginMember.account}</span><a
						href="${base}/lotteryV3/logout.do"
						class="exit-btn ml14 color666 js-trigger-logout">退出</a></li>
					<li class="m14">|</li>
					<li class="header-show-money">
						<div id="wallet_container">
							<span class="span-login-rmb colorRed">￥</span> <span
								id="header_user_money" class="colorRed">*****</span><input
								id="header_user_money_hidden" value="${loginMember.money}"
								type="hidden">
						</div> <a href="javascript://" id="header_show_money" class="color666">显示余额</a>
						<i id="header_money_refresh" class="img-login-refresh icon"></i>
					</li>
					<li class="mylottery" id="mylottery"><a
						href="${base}/center/member/meminfo/cp3Page.do"
						class="my-lottery color666"><i class="nosign" id="header_sign"></i>会员中心<em
							class="icon"></em></a>
						<div id="mylottery_dropdown" style="display: none;"
							class="lot_list">
							<ul>
								<li><a
									href="${base}/center/record/betrecord/cp3Page.do?form=order">投注记录</a></li>
								<li><a
									href="${base}/center/record/betrecord/cp3Page.do?form=win">中奖记录</a></li>
								<li><a href="#">我的跟单</a></li>
								<li><a href="${base}/center/record/hisrecord/cp3Page.do">账户明细</a></li>

								<li><a
									href="${base}/center/member/meminfo/cp3Page.do?form=info">账户信息</a></li>
								<li><a
									href="#">积分兑换</a><span
									id="header_notpaid_counter_hd" class="notpaid"></span></li>
							</ul>
						</div></li>
					<li class="m14" style="margin-left: -5px">|</li>
					<li><a href="${base}/center/banktrans/deposit/cp3Page.do"
						class="mr10 color666 mr">充值</a><a
						href="${base}/center/banktrans/draw/cp3Page.do" class="color666">提款</a></li>
					<li class="toptray-r-hd ml14">
						<div id="unread_count_num">
							<a class="message-nums-hd color666"
								href="${base}/center/news/message/cp3Page.do">消息<em
								id="unread_num_new" class="icon"></em></a>
						</div>
					</li>
				</ul>
				</c:when>
				<c:otherwise>
				<ul id="toptray_not_login" class="toptray-plus clearfix">
					<li class="color666">hi，欢迎来到${_title}！&nbsp; <a
						href="${base}/lotteryV3/login.do"
						class="colorRed">登录</a> &nbsp; <a
						href="${base}/lotteryV3/register.do"
						class="colorRed">注册</a>
					</li>
					<li class="m14">|</li>
					<li class="mylottery"><a
						href="${base}/center/member/meminfo/cp3Page.do"
						class="my-lottery color666">会员中心<em class="icon"></em></a></li>
					<li><a href="${base}/center/banktrans/deposit/cp3Page.do"
						class="color666 mr10">充值</a> <a
						href="${base}/center/banktrans/draw/cp3Page.do" class="color666">提款</a>
					</li>
					<li class="m14">|</li>
				</ul>
				</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<div class="header-navbar-plus">
		<div class="auto990 clearfix" id="header_box">
			<div class="lottery-plus mr20" id="lotterys">
				<h2>
					全部彩种<a href="javascript://" class="dropdown-desc"
						style="margin-left: 50px;"><i class="icon arrow"></i></a>
				</h2>
			</div>
			<ul class="nav-plus clearfix">
				<li class="navw1"><a href="${base}/lotteryV3/index.do">首页</a></li>
				<li class="navw2 "><a href="${base}/lotteryV3/lottery.do">购彩大厅</a></li>
				<li class="navw2 "><a href="${base}/lotteryV3/joint.do">合买中心</a></li>
				<li class="navw2 "><a href="${base}/lotteryV3/draw.do" class="dropdown-desc">开奖公告</a></li>
				<li class="navw2 "><a href="${base}/lotteryV3/trend.do">走势图</a></li>
				<li class="navw2 " style="position: relative"><a href="${base}/lotteryV3/promotion.do">优惠活动<em class="hot-icon"></em></a></li>
				<li class="navw2 " style="position: relative"><a href="${base}/lotteryV3/mobile/show.do">手机够彩<em class="hot-icon"></em></a></li>
			</ul>
			<ul class="nav-right clearfix">
				<li><a href="javascript:alert('敬请期待');">代理</a></li>
				<li>·</li>
				<li><a href="${base}/lotteryV3/rule.do" target="_blank">玩法</a></li>
				<li>·</li>
				<li><a href="javascript://" onclick="zxkf();">在线客服</a></li>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/jquery.ajax.js"
	path="${base}"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/base.js?v=2.7"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/date.js"></script>
<script type="text/javascript">
//在线客服
//${base}/lotteryV3/service.do
function zxkf(){
	var open = window.open();
	$.ajax({
		url:"${base}/memInfo/common/siteInfo/base.do",
		dataType:"json",
		success:function(j){
			if(j.success){
				var url = j.item;
				if(url.indexOf('http://')==-1 && url.indexOf('https://')==-1){
					url = 'http://' + url;
				}
				open.location = url;
			}else{
				$('#zxkf').attr('href','#').removeAttr('target');
			}
		}
	});
}
</script>