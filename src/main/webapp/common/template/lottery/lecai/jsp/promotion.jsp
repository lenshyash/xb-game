<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>优惠活动中心-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/promotion.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body style="background: #eee;">
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<input type="hidden" value="6" id="navId" />
	<jsp:include page="/member/${stationFolder}/promotion.jsp"></jsp:include>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
	<script type="text/javascript">
		$(function(){
			$(".floorRright a").click(function(){
				var t = $(this),detail = t.parent().parent().parent().siblings(".floor").find(".floorDetail");
				detail.hide();
				if(t.parent().parent().siblings(".floorDetail").is(":visible")){
					t.parent().parent().siblings(".floorDetail").hide();
				}else{
					t.parent().parent().siblings(".floorDetail").show();
				}
			})
		})
	</script>
</body>
</html>