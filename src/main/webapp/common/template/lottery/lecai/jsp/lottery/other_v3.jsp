<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="area_content"></div>
<div class="ball_select yjf_bs">
	<div class="ball_select_text">
		<c:if test="${empty lotteryBetModel}">
			<ul class="choose-money">
				<li class="on" value="1">元</li>
				<li value="10">角</li>
				<li value="100">分</li>
			</ul>
		</c:if>
		<c:if test="${lotteryBetModel == '111'}">
			<ul class="choose-money">
				<li class="on" value="1">元</li>
				<li value="10">角</li>
				<li value="100">分</li>
			</ul>
		</c:if>
		<c:if test="${lotteryBetModel == '110'}">
			<ul class="choose-money">
				<li class="on" value="1">元</li>
				<li value="10">角</li>
			</ul>
		</c:if>
		<c:if test="${lotteryBetModel == '100'}">
			<ul class="choose-money">
				<li class="on" value="1">元</li>
			</ul>
		</c:if>
	</div>
	<div class="ball_select_text">
		<label>
			<div
				class="tp-ui-item tp-ui-forminput tp-ui-number tp-ui-number-depart">
				<div class="tp-ui-sub tp-ui-number-refer">
					<input id="beiShu" value="1" maxlength="8"
						class="random-selection myNumClass0" type="text">
				</div>
				<div
					class="tp-ui-sub tp-ui-handle-button tp-ui-number-option tp-ui-number-addto">
					<button type="button">
						<em>+</em>
					</button>
				</div>
				<div
					class="tp-ui-sub tp-ui-handle-button tp-ui-number-option tp-ui-number-reduce">
					<button type="button">
						<em>-</em>
					</button>
				</div>
			</div>
			<h4>倍</h4>
		</label>
	</div>
	<!-- <div class="ball_select_other clearball">
		<div class="zngl_tip_box">
			<span class="link_sel">可用余额<strong class="red" id="gamePoint2">0</strong>${not empty cashName?cashName:'元'}
				<a href="javascript:void(0);" onclick="_Core.refreshMoney();"
				class="refresh"></a>
			</span>
		</div>
	</div> -->
	<div class="ball_select_other">
		<div class="zngl_tip_box">
			<span class="link_sel">奖金<%-- <c:if test="${fanShui eq 'on'}">/返点</c:if> --%>：<strong
				class="red" id="peiLv">0</strong>${not empty cashName?cashName:'元'}&nbsp;<c:if
					test="${fanShui eq 'on' && rateRandomOnoff ne 'on'}">/&nbsp;<strong class="red"
						id="fanShui">0</strong>%</c:if>
			</span>
		</div>
	</div>
</div>
<div class="tz_box_but">
	<input type="button" id="add_to_list_zs_standard" value=""
		class="btn btn-add">
</div>
<!-- 投注区 -->
<div class="xhmain" id="tz_standard">
	<div class="tz_box">
		<div class="select_list" id="tz_standard_normal">
			<div class="select_list_body">
				<div class="select_list_head">
					<a id="removeAll" class="del_btn" href="javascript:;">清空</a> 总注数：<span
						class="red" id="all_pieces_count">0</span>&nbsp;注&nbsp;&nbsp;&nbsp;
					总金额：<span class="red" id="all_preces_money">0</span>&nbsp;${not empty cashName?cashName:'元'}&nbsp;&nbsp;&nbsp;
					<a id="random1" class="random_btn" href="javascript:;">随机来1注</a> <a
						id="random5" class="random_btn" href="javascript:;">随机来5注</a>
				</div>
				<ul class="bettingarea" id="multiple_select">
				</ul>
			</div>
		</div>
		<div class="xhmain" id="btn_tz_start">
			<div class="tz_box tz_box_big">
				<div id="btn_tz_state" class="tz_box_but">
					<div class="lot_submit">
						<input type="button" class="btn btn-sub" value="立即投注"
							id="submit_lottery">
						<input type="button" class="btn btn-sub" value="发起合买"
							id="joint_purchase" style="display:none;">
						<!-- <p>
							<input type="checkbox" name="agreement" id="agreement"
								checked="checked"> 我已经阅读并同意 <a href="javascript://"
								class="link-lightgray" target="_blank">《委托投注规则》</a>
						</p> -->
					</div>
				</div>
			</div>
		</div>
		<div class="lot_data" id="lot_data">
			<div class="lot_data_right">
				<c:choose>
					<c:when test="${bly.code eq 'FC3D' || bly.code eq 'PL3'}">
					</c:when>
					<c:otherwise>
						<c:if test="${!unChaseOrder}"><input type="checkbox" id="do_chase">
						<label for="do_chase" class="for_checkbox">我要追号</label></c:if>
						<c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}">
						<c:if test="${!isGuest && !isJoint}">
						<input type="checkbox" id="do_syndicate" /> 
						<label for="do_syndicate" class="for_checkbox">发起合买</label>
						</c:if></c:if>
					</c:otherwise>
				</c:choose>
				<input
					type="checkbox" id="do_orderdetail" checked="checked"/> <label for="do_orderdetail"
					class="for_checkbox for_checkbox_active">投注记录</label>
			</div>
		</div>
	</div>
</div>
<c:choose>
	<c:when test="${bly.code eq 'FC3D' || bly.code eq 'PL3'}">
	</c:when>
	<c:otherwise>
		<!--发起合买-->
		<div class="xhmain" id="do_syndicate_standard" style="display: none">
			<div class="tz_box">
				<div class="syndicate_hd_out">
					<i class="icon_up_dot"></i>
					<table class="table_list" id="syndicate_list" width="100%">
						<tbody>
							<tr class="st">
								<td rowspan="3" class="til" width="120"><span>合买设置</span></td>
								<td class="first text-right"><span class="t">我要分为：</span></td>
								<td class="first"><input name="parts_founder" class="text" id="numFen"
									type="text">份&nbsp;&nbsp; 每份<span class="red"
										id="level_parts_limited" style="margin-left: 0;">￥1.00</span>${not empty cashName?cashName:'元'}&nbsp;&nbsp;
									<input type="hidden" value="0" id="oneFenMoney"> 
									<div style="display: inline; position: relative;">
										<span class="gray">每份至少1${not empty cashName?cashName:'元'}</span>
									</div></td>
							</tr>
							<tr>
								<td class="text-right"><span class="t">我要提成：</span></td>
								<td><span id="syndicate_rebate" style="">税后盈利的 <select
										name="rebate" id="syndicate_rebate_select">
											<option value="0">0%</option>
											<option value="1">1%</option>
											<option value="2">2%</option>
											<option value="3">3%</option>
											<option value="4">4%</option>
											<option value="5">5%</option>
											<option value="6">6%</option>
											<option value="7">7%</option>
											<option value="8">8%</option>
										</select>
								</span>&nbsp;&nbsp;
									<div class="notebox_wrapper what_is_rebate_wrapper"
										style="display: inline; position: relative; z-index: 0;">
										<span class="gray">什么是提成？<i class="ssqIcon ssqIcon-ask"></i></span>
										<span class="notebox" id="rebate_tooltip"
											style="width: 400px; position: absolute; top: 15px; left: 0px; display: none;">
											为了奖励发起人发起合买方案，发起人可以设置0%-8%的比例作为奖金提成，可设置的最高比例与发起人的战绩挂钩。提成金额直接派发到用户的"现金账户"。<br>
											提成条件：1、合买方案中奖且盈利<br> 2、提成后剩余奖金金额需大于合买方案金额<br>
											提成计算：税后中奖金额×提成比例<br>
											示例：合买方案总金额1000元，税后中奖金额2000元，发起人设置提成比例10%，则发起人提成=2000×10%=200元；若税后中奖金额900元，则发起人无提成；若税后中奖金额1100元，发起人应提成=1100×10%=110元，此时剩余奖金金额990元，小于方案金额，则发起人无提成。
										</span>
									</div></td>
							</tr>
							<tr class="sh">
								<td class="text-right"><span class="t">是否公开：</span></td>
								<td><label><input name="public_status" type="radio"
										checked="checked" value="1">上传即公开</label> <label><input
										type="radio" name="public_status" value="2">截止后公开</label><label><input name="public_status" type="radio"
										checked="checked" value="3">完全保密</label></td>
							</tr>
							<tr>
							<td rowspan="2" class="til" width="120"><span>认购设置</span></td>
								<td class="text-right"><span class="t">我要认购：</span></td>
								<td><input name="parts_founder" class="text" value="1"
									type="text" id="myNumFen">份&nbsp;&nbsp;
									<div style="display: inline; position: relative;">
										<span class="red" id="myMoney">￥1.00</span>&nbsp;${not empty cashName?cashName:'元'}
										(<span class="gray" id="myBFL">50</span><span class="gray">%</span>)
									</div>&nbsp;&nbsp;<span class="gray">最低认购10%</span></td>
							</tr>
							<tr class="sh">
								<td class="text-right"><span class="hide_sp"><input type="checkbox" id="isbaodi" ></span><span class="t">我要保底：</span></td>
								<td><input name="parts_reserved" class="text" type="text" id="baodiNumFen" disabled="disabled">份&nbsp;&nbsp; <span class="red" id="baodiMoney">￥0.00</span>&nbsp;${not empty cashName?cashName:'元'}
									(<span class="gray" id="baodiBFL">0</span><span class="gray">%</span>)&nbsp;&nbsp;
									<div class="notebox_wrapper"
										style="display: inline; position: relative;">
										<span class="gray">全额保底<i class="ssqIcon ssqIcon-ask"></i></span> <span
											class="notebox"
											style="width: 400px; display: none; position: absolute; top: 15px; left: 0px;">发起人承诺合买截止后，如果方案未满员，发起人再认购先前承诺的金额以最大限度让方案成交。<br>使用保底时，系统会提前扣除保底资金，在合买截止时如果方案仍未满员的话，系统将会使用所扣除的保底资金去认购方案。<br>注：如果使用了发起人的保底资金还无法让方案满员则该方案不生效，系统退回所有保底资金。<br>如果在合买截止前方案已经满员，系统会把保底资金返还给发起人。
										</span>
									</div></td>
							</tr>
							<tr class="sh">
								<td class="til" width="120"><span>确认信息</span></td>
								<td colspan="2"><span class="t" style="margin-left:19px;">本次投注金额为<span class="red" id="nowTotalMoney">￥4.00</span>元</span>
									<span class="gray">&nbsp;&nbsp;*注：当每份金额无法被整除时，取三位小数，不足的由发起人来补足</span>
								</td>
							</tr>
							<tr class="st">
								<td class="til" width="120"><span>方案宣传</span></td>
								<td class="last text-right"><span class="t">方案描述：</span></td>
								<td class="last"><textarea name="description" class="desc"
										id="syndicate_plan_depict">该用户只想中大奖！</textarea>
										<br>
										<span class="gray">已输入<span id="readyZiFu">0</span>个字符，最多输入200个字符</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--我要追号-->
		<c:if test="${!unChaseOrder }"><div class="xhmain" id="do_chase_standard" style="display: none;">
			<div class="tz_box">
				<div class="zhuihao">
					<i class="icon_up_dot"></i>
					<div class="zhuihao_title">
						<div class="left">
							<span class="title"><em></em>常规追号</span> <select
								id="chase_select" name="chase_select">
								<option value="10" selected="selected">追10期</option>
								<option value="20">追20期</option>
								<option value="30">追30期</option>
								<option value="50">追50期</option>
								<option value="100">追100期</option>
							</select>&nbsp;&nbsp; <span class="title">起始倍数</span> <input
								id="chase_multiple_all" name="chase_multiple_all" type="text"
								class="text" maxlength="3" style="ime-mode: disabled;">
						</div>
						<span class="right"><label><input type="checkbox"
								id="win_operate" name="win_operate" checked> 中奖后停止</label></span>
						<div class="clear"></div>
					</div>
					<div class="zhuihao_box">
						<div class="zhuihao_scroll">
							<table class="zhuihao_list">
								<thead>
									<tr class="t">
										<td width="5%">序号</td>
										<td width="20%" align="left"><label><input
												id="chase_phase_all" name="chase_phase_all" type="checkbox"
												checked> 追号期数</label></td>
										<td width="15%">倍数</td>
										<td width="15%">当前投注</td>
										<td width="15%">累计投入</td>
										<td width="15%">盈利(${not empty cashName?cashName:'元'})</td>
										<td width="15%">盈利率</td>
									</tr>
								</thead>
								<tbody id="betTraceData">

								</tbody>
							</table>
						</div>
					</div>
					<div class="zhuihao_info">
						共追号<span class="red" id="chase_info_num">0</span>期，总投注金额<span
							class="red" id="chase_info_amount">0</span>${not empty cashName?cashName:'元'}
					</div>
				</div>
			</div>
		</div></c:if>
	</c:otherwise>
</c:choose>
<!-- 投注订单 -->
<div class="xhmain" id="do_orderdetail_standard" style="display: block;">
	<div class="tz_box">
		<div class="zhuihao orderdetail">
			<i class="icon_up_dot"></i>
			<div class="zhuihao_title">
				<div class="left">
					<span class="title"><em></em>订单查询</span>&nbsp;&nbsp;彩种：<span>${bly.name}</span>&nbsp;&nbsp;报表日期：<span
						id="betListBetween"></span> &nbsp;&nbsp;<span class="red">贴心小提醒：点击注单号能查询详细投注明细</span>
				</div>
				<div class="bh-refresh right" id="refreshBettingHistory">
					<a href="javascript://" onclick="getBcLotteryOrder(1);"><i>&nbsp;</i>刷新</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="zhuihao_box">
				<div class="zhuihao_scroll">
					<table class="zhuihao_list">
						<thead>
							<tr class="t">
								<td width="15%">注单号|下注时间</td>
								<td width="13%">期号|玩法</td>
								<td width="12%">倍数|总额</td>
								<td width="20%">下注内容</td>
								<td width="15%">开奖号码</td>
								<td width="13%">中奖奖金</td>
								<td width="7%">状态</td>
								<td width="5%" class="notbor">操作</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">

						</tbody>
						<tbody id="betOrderTblFoot" style="display: none;">
							<tr>
								<td colspan="2" class="t_right notbor">小计</td>
								<td id="betPageAmount" class="color-blue notbor"></td>
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" class="t_right notbor">总计</td>
								<td class="red notbor" id="betAmount"></td>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- 分页导入 -->
			<div class="zhuihao_info">
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
</div>
