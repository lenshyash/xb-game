<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="lecai_v5">
	<c:if test="${param.code == 'BJSC'}">
	<div class="BJSCOPEN">
		<strong>北京赛车开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">北京赛车</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:30-23:50(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-44</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司北京赛车具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'XYFT'}">
	<div class="XYFTOPEN">
		<strong>幸运飞艇开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">幸运飞艇</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						13:04—04:04(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-180</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司幸运飞艇具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'LXYFT'}">
	<div class="LXYFTOPEN">
		<strong>老幸运飞艇开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">
						游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">老幸运飞艇</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						13:04:05—04:04:05(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-180</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司老幸运飞艇具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'SFSC'}">
	<div class="SFSCOPEN">
		<strong>极速赛车开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" align="center">游戏项目</th>
					<th class="subtitle2" width="25%" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" align="center">每日期数</th>
					<th class="subtitle2" width="25%" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">极速赛车</td>
					<td class="point" bgcolor="#FFF7F0" align="center">全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司极速赛车具体游戏规则如下︰</strong><br>
	</div>
	</c:if>
	<h2>两面盘（冠军~第十名）</h2>
	<dd>
		<ul>
			<li>大小∶开出之号码大於或等於6为大，小於或等於5为小。</li>
			<li>单双∶开出之号码为双数叫双，如4、8；号码为单数叫单，如5、9。</li>
			<li>冠军 龙/虎∶"第一名"之车号大於"第十名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
			<li>亚军 龙/虎∶"第二名"之车号大於"第九名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
			<li>季军 龙/虎∶"第三名"之车号大於"第八名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
			<li>第四名 龙/虎∶"第四名"之车号大於"第七名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
			<li>第五名 龙/虎∶"第五名"之车号大於"第六名"之车号视为「龙」中奖、反之小於视为「虎」中奖，其馀情况视为不中奖。</li>
		</ul>
	</dd>
	<h2>两面盘（冠亚和）: "冠军车号 + 亚军车号 = 冠亚和"</h2>
	<dd>
		<ul>
			<li>冠亚和大小∶以冠军车号和亚军车号之和大小来判断胜负，"冠亚和"大於11为大，小於11为小${dx11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
			<li>冠亚和单双∶以冠军车号和亚军车号之和单双来判断胜负，"冠亚和"为双数叫双，为单数叫单${ds11Setting }。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖。</li>
			<li>冠亚和指定:以指定冠军车号和亚军车号之和来判断胜负；冠亚车号总和可能出现结果为3~19，投中对应"冠亚和指定"数字视为中奖，其馀情形视为不中奖</li>
		</ul>
	</dd>
	<h2>冠军~第十名车号指定:</h2>
	<dd>
		<ul>
			<li>冠军~第十名车号指定: 每一个车号为一投注组合，开奖结果"投注车号"对应所投名次视为中奖，其馀情形视为不中奖。</li>
		</ul>
	</dd>
</div>