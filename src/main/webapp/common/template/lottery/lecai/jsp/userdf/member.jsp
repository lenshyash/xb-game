<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.e4393c{
		color:#e4393c;
		margin-left:8px;
	}
	.e4393c:hover{
		color:#000;
		text-decoration: underline;
	}
	.tapli table th,td{
		font-size:12px;
		height:30px;
	}
	.tapli table {
	    table-layout: fixed;
	    margin: 0 auto;
	    text-align: center;
	    margin-bottom: 30px
	}
	.tapli table td, .evCall table th {
	    border: 1px solid #e6e6e6;
	    height: 32px
	}
	.tapli table th {
	    background-color: #f2f7ff
	}
	
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">个人信息</div>
            <div class="userMain mgb10">
                <div id="newTab" class="newTab">
                <a href="javascript:(0)" class="curr" data-id="ziliao">个人资料</a>
                    <a href="javascript:(0)" class="" data-id="dengji">等级头衔</a>
                    </div>
                    <script>
			            $(".newTab a").click(function () {
			            	var dataid = $(this).attr('data-id')
				            $(this).addClass('curr').siblings('a').removeClass('curr')
				            $(".tapli").hide()
				            $("."+dataid+"").show()
				        })
		            </script>
                <div class="fix tapli  ziliao evCall" style="padding-left:100px;">
                    <!--  <div class="selectHeadImg"><img src="images/4eda6ad3f56b4f17.jpg"  width="100" height="100" alt="" class="headImg"><a>修改头像</a></div>-->
                    <ul class="personalInfo submitContent">
                        <li><span>会员账号：</span>
                            <ins style="font-weight:600;">${loginMember.account}&nbsp;</ins>欢迎您！<a class="e4393c" href="${base }/center/banktrans/deposit/cpDfPage.do">[充值]</a>&nbsp;&nbsp;<a class="e4393c" href="${base }/center/banktrans/draw/cpDfPage.do">[提款]</a>
                        </li>
                        <li><span>账户余额：</span>
                            <ins>${loginMember.money}${not empty cashName?cashName:'元'}</ins><a class="e4393c" href="${base}/center/record/hisrecord/cpDfPage.do?type=3">[余额明细]</a>
                        </li>
                        <c:if test="${not empty mnyScore && mnyScore eq 'on'}">
	                        <li>
	                        	<span>我的积分：</span>
	                        	<ins>${userInfo1.score}</ins>
	                        	<c:if test="${not empty exchangeScore && exchangeScore eq 'on' }">
	                        		<a class="e4393c" href="${base}/center/banktrans/exchange/cpDfPage.do">[积分兑换]</a>
	                        	</c:if>
	                        </li>
	                    </c:if>
                        </li>
                        <li><span>最后登录时间：</span>
                            <ins>${userInfo1.lastLoginDatetime}</ins>
                        </li>
                        </li>
                        <li><span>今日彩票消费：</span>
                            <ins>${-allBetAmount}${not empty cashName?cashName:'元'}</ins><a class="e4393c" href="${base}/center/df/record/betrecord/cpDfPage.do?form=order">[彩票投注记录]</a>
                        </li>
                        <li>
                        	<span>今日彩票中奖：</span>
                            <ins>${allWinAmount}</ins><a class="e4393c" href="${base}/center/df/record/betrecord/cpDfPage.do?form=win">[彩票中奖记录]</a>
                        </li>
                        <li>
                        	<span>今日彩票盈亏：</span>
                            <ins>${yingkuiAmount}</ins>
                        </li>
                        <li><span>会员等级：</span>
                            <ins>${levelName}</ins>
                        </li>
                        <!-- <li><span>头衔：</span>
                            <ins>测试组</ins>
                        </li> -->
                        <!-- <li><span>昵称：</span> 我去</li> -->
                        <!-- <li>
                            <span></span> <a href="javascript:;" class="submitBtn">保存</a>
                        </li> -->
                    </ul>
                </div>
                <div class="fix tapli dengji" style="display:none">
                	<ul class="evCallInfo">
                        <li><img style="margin-bottom: 10px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAB/ZSURBVGiBTZpZjJ3ned9/7/JtZ19n5XA4JIekuImkREm2JEtetDhpFDtxEiMomgBp4qTIRVH0oulNL5qrJmibFknTFEbgNIkdp4Jj14otW7Zly5K1UDsp7tsMh7PP2c/51vftxRkpvXhxgIPvA97ne7b/8/8/4j+/e4d8qcjVN8+xs7REPpejtbnFxtItJqdnqO+ZQzkOWRSzdWeJS2++wa3z54kHfbI0JjMZSiqk46K0RkqJkBIhhANiwcKsgCLC5jFYi+0LKXsSbgy6veUsTgiKBbTnkaYpJk0JBz2UECwcP8LRB04xs3+WYrWMkALX02yurPGjb/6QxXsO8Bu/++vcvbXKH/3HP+G+R8+ytb3Oe2+cQ2uVoJMujDqUqxWS1OJ4Hvfc/wB+LkccRQw6bVYuX+T9l3/K6o1rSEBpjZCKXL6A0g4IAuDT1ppPWcPj1nJcCOsIwBgLxmKxCCnAGKyU+IX8YNTpvW+NedHzvB9mJnvRywWJdh06m5tceO1nbK1tceyhUywcO0ilUcXiUm5WOP3ofVx/7yq3llZYmJsh73tILPsOHmDpxnX0YGsTIzOqjTIqV2Pl2i3qzQZYQTQc0d5c48LPXubKm2+QjEZUanUczyNJEozJsMZ+3Jjst4HPA2WwgEAIgbUWYzJMmmGMRTvO+H/AZBnS0Xmp9UNCioe8wPt3FraHg+5XlXa/XJ2cfKe1Dv1Wi87GDjv1bZRWKF3GDzxm982wtrzK0vIK++ZmmZxoMugNWDh6kPmFfcg4hdrsHmbm59haXsYmCdYa4tGAa++c44W//t+899KPKVXLHH/oIaYWDpBZSxJFT5gs+7G15mXgN8dGsWuUBGtIwpBwMCAzGa7vIZWCXaOsHXtRuw6jYchoFDIxO1O3iN/P0vQtC98uVGsPDrotbl28QtQPscbS7/RIE4OQitn5Ge7eXiVOLPMH5onCGN93WTx6BPX07/w+WMnVd84TDWIC32f95k1e/tY3efOFF4jDISce/BgzCwfptTu0Njf22TT7ismyP7TGziMEAjE2SYw9lcYJw16POIrwghx+LocdW401BmMMSimEkAgpiEYjTJpRbTRQStPvdYWU8pDS+rfS1Ozvt1o/63S6g1qzzsTMFMYahBB4gc/S9VvMzc/jey4bW5vUJmosHFxEHX/8KaLegDROMUnMG89/lx/9/dfZXLpNc3aGI6dPY8TYC47j/t6w2/1WZ2freJqmSKWQQoIAoSRpnDIa9Bl0OggpKVSqOF6ANQawCCGIowipJFIqrACBwGQZSRihHE212aS700IIgRzH7b1ZEv+W57m3W1ud851Wi8ZkkyDwsQLSOCWNExYPLtAbDNCBplGrIUWWoRBsLC3x7f/1F7z+7W9g05jZ/QuUKiWiKGT/PUdz1YmJv1tduvVna7duBmkcjyug2jVKCKLBiN72DqNuFy/wKVYqKEdjTfaRUSZNMSZDyvF7WAsSkII0TRgOBlhjKVaqGGOw1uL6PlmWVV3X+arnun96+a2L+ntff47l60tIFBN7pnA8TSGfZ2ZuBsfRuEohAy/gwiuv8L2vfJkbb/2UJEnJF4skccjMwUXu/8zTtdTwwvuvvvKry1cuYY3B8wO00oBAIgh7A/o722BT8pUKuXIZpfU4j/4pCImjEVKNq6m1GRYDiN1yA2kc0+u0qdZrGGt3Q1aitMtoMMLznH9VKpe+sXxlKffez94hiROkVpQrJXKBS6mSx/U9PNdBVSbn+cmzf8/m0gfkKpMcOnM/+UqNE48+xqEz9zdXb9/6yevPf+fMncsX0dohVywhtYbdjz7q9Rh0dvACn0KlhuvnEEJirfn/8g6SOMakKV4QYIHMZMjdPItHQ7IkRXsuCEGt0aTf62Azg5SKzGSM+gOqjTqlauXQ+t21x+Mo/lq1WU0K5SKzk5NMlMu0Bn2UUjSrVVSvH7GzdpeZhcM8+NlfYGLfAounTrNn8XDu8ptvPf/yP3z99NrN6/i5PPlSGakVCLDWEvYGDDpbBIUSleYE0nEwu/lk7Tj8hJTYLCUaDnH9AK0dsjQBBFprAJLRiCRJcP0AKQV+LkBrPQ5NASbNiAZtipUKpXqd1s7O3mgYnUiz7NkjJ46Yg3NzlPIBSjsEvo+nPHQYRyzef5Z7P/4Ie48cG/enJBWvPvetr577/nMPjrpdCpU6fqEIQoC1CASjfp9BZ5tCuc7E3D5GowFZOMLasZewdteojGg4QkiFdt1xiKXZuKl/VCXtR94F6OzsUJ+YZHtzkyxLKVWrhP0ug14fk2X4uYD2oPVMGib/fU9z6neFsWx2uiAlmbGs72yjJw4c5MT9D3Lo9BmG/T7d9Q3e+N53/uD9l37wDGTMLR4jSTPiKERKBQhGgx6Dzjb5UpHFe++n322PL79bSLIsQ+426Hg0Ik0T8uXKuIeZFGvGCEQKSZKlZGmCNeN3pFIMe32qzSZKKZQSLN6zSD7v0+12kVimZyaRCCanJr4kEC++d/7y11I5LkRBMSATGerkU8+wePQ4yWjI1Tdf48W//9rpG+++8jcg1KHTD9Gc20uv0yGJQ5TWxMMRvdYmQSHPkfsfJVfIc/vapfFFlSRL0nHSa0Uax0SjEUG+gPZcrIEkCkHK3aqqSOOILI7RWuPmcgS5PJnJ0I6DFIJqvcrRe49SrRQZRQmNZp1D+2aJRiP6YQiufurylWt/3e12u2trayRZgut56D175ol6PV79zrd5+wffk2SDv/BzE44bBJz+9BO8/9qrCGvxgxyj/pB+ewM/l+PMY09y8Nhx/vHvvgbWIoUkSxKSKMILAoS1JKMQx3Fxgtw4r6wlS1KcIBiX/ywjS1LKtRrlap0wjnE9HyRkcUS+WKI2UUNqiXYknuuQ831mayWuhl3eeOsKzdmpYibMfw0875f7nR42yRCJQFZLZS6de50rb55DS/df5Euz9ytHc/8TT6Bcj+2Vuwz7feJRSL+1BRhOPvwJnvn1XyNwMpQAIfRHaMMLAhylGfUHIARBqYTJUoQFvZtDH04AWZbhuA6TM9NMz89SLJdwXB8pJHlpyKKQu8t3Wb69ys2rt9i6u8Ko38UVhuNzTYRU4w+U2F/qdbuf6bd6pFHM9Q+uIa+ef5ftjXX2LBzQbhD8e+U5FJt1Tj76GBfPvUEahaRJQntrgzQZceD0xzh8/0Oc+9mrvPHTV6jWGyAM/U6LWnOCIJdnMOihHE2l0URIidKK+ckKk9UcUmusBdd1KRQL5AsFer0+N65cZXtrCy/wGQ5H7K0XaBYc0jDmwrsf8MF7FyDLKBbyuFpycM8EpaIHQpBlKb1W7z9Eo5DhYATWIGNrKJYrbK+t/gqCReVoHnjyabY21lm5eoU4HDHqdcmSLrOHTnDi4UfZvLvKS995njAytDY3GLTbzC4sIIRgZ2MdpCBfKpFZSFPDTK3AY6cOsLnVQkmJlIIszeh1umysrnL39hKt7Rau5+K43rgSFvPsqQRIm9Lr9oiGEYHnkvNclMmYrBQ4tKeJyTLSKKHX6T5iLZ9YX14bp4anNO21dRzP/1J9aoqpvQssnrqPN1/4AaN2h2GnQxr3yZenOXr2QdwgoN/rMDO3l9WbNxkNB8wvHmLUH9Da3MRxXYJcgThO2dzcpuZLPvfQMTrtHhs7XaTjIKUiSVJsGmOSiDRJUNrBDfJYBEkUEeQ99s/WESYlimK0lhQKecqlHL4n8T3N6SP7cF1FNArpd/qYNPtSv90nHEXI1cuX0Z4zv3jmzCdy1SqnP/0pbly4wM3z7zDqdsjSEBAce/STHDr7II7S9Le2uHP9GrlCgcbMDNvr6wx6fZwgwM3lsAi2t1o0AsmvPX6SfZMVXnrn8q5RkjjL8F3FVCVP3nOxCLwgIJcvImyCyVIq1SKzkzXynkMaRgSeQ7lYoFQuUG0UUa5m/55JKpUSvW6XYW9IOAo/53pOsbWxg4ywzB899oXq9IyYPnCQUqXCS994lrDXBWGAhHs+/knOPP5J+u0W7/70x2ysLOEFPo7v0dnaJhqNcHwPz/eRSrKzsUlJWX71k6d55Ow9XN9pc31phXyhgBCC4XDE8X0TTFfzZAaQCi/wqVTLFLTFUTA5UaVSzpPzPWwUMVEtMdWs4HmKZqOAkw9wfB9jJb12lzgMCQdhzg+CZzo7bWR97zy16elPT+7dS3N2lnM/eIHtleVxmfY8ZhaOcuDYMZYufcBPv/ksty99MG7AStLZ2WHY7+Pl8+O+IyWd7Q5OFvPUA4f5/Gc/Rhq4vPD6BxgkrucyDGOaRZ+PHZnD15JhlOAGOfx8gYl6harK8F2X6akqnu+QmQyU4ND+WWZn6rjKUivn0IUCnQRWV9aJ+kNMmhJFEUrrp401yGqlqgX20WjQ4/b581x89VXyxSJCShzPo9Soc+vSRV57/h9ZvnoZpcfAtb21TRrHBPkcSIXBMgpDon6PR+49yBd+4RHqe5q8f/Mub797Cb9cIzUWk6V85sxBDu5tEGbjodMrFKjWG0yXfFwb06iVmWxUMAi2Wh1OHpvn2D37qNdKTFQD4jSlmyT0IoPpdbFZhjGGNE2xwj6cL5XQucA7EA2HhTe//32WLl5CK4HJLFiDn8+zdmeZOIxxtKbaaGItjPoDlNYoKcmyBGEtUgoG7TYnD07zyz/3MCfvO8y11S1efOkdwlFIvTbDcBRyemGSzz58Aqfo088sSE2uUmXfzCSzXsZSEnFkcZZKKc/1y7cpFX2eeeIsc9MTxNaS9xS3ltdIYotJYu5plvhAWqLMIIxFwEKukK/JQrlyaHNlhdsXP0ApQZplxKMhSmn8XB4pBH7goxwHL5cnyBfGDXYXEwJkxlBwFVMll8cfvIdDR+dJlOKd8zd45bULKL9ClqTM1/P86hNnWTy6j4EVDOMUERSZmZ7m2EyJmooIs4yH7jsEWLZ7fZ556kHO3nuQajVHmkWsrW0SjhJ8x6XmWRYaRaS1GGuwWLTSpElyRCLl4u0PLmJNhpKSaDggzcajn+M4SCHHo4gQ+Lk8juuSZekuggAlJNIa9jdKPPnIvdQmK6x1urz1/jVee+Miw2FEpVaiVnb5jc89wukHjuAUc6xutugOEipTM5xYmGShYEFY5qabnDiyQLc3YO/eST71yClq1SKdbo/lpRWwcOjAPuJwyOH5JkJYTGZ2B1qLUhqTpnv01bfequ3cvUOtMcHO5iYmTQGNsRa0M2acTIp0FW4QkIQhWZbhIajkfQDyrsv+6QonTi1y/NQh1jfbfPVvnuOtd29w8MA8buDyz3/pMT5x9ihhGhGGMddvrJBInxOH9nJmtkDRMVCv8KmFOQJPMwpTJuplCoHHxWt3+OFPznH44F4evO8Y3W6Her2MSgzDKyukWYrFYgVI7aK0X5bbK3eKcwcXyZcrRIP+R2N8lqYoxpgOQAqBo12045KkGVoL9s80aZR8Th3Zw0yzQqlSoljIkSYJwgpcCYVA8rmfe4hnnnyQMAnBGOIk4fZGl4nJBo8uTrC3LPHqZabmp5iZrpEkY8CczwVcuHqLb37zeRYm6jxw4git7RaO1tQaNTJgMBo3+MxY0sQgtUNzYb6o9x04opI4ot/pU6hU6Ld3EIJdMsWM4213ttJakxmXzFpcJdk/WWEzENx3cj9Zr0ch79Pt9bl5fYkHTx7kN7/4FO9eusErr7zOaNDlFz/7CXxPcfn6Kkmq+Pj+Gqf2VcjVcuhCHisE1likEJCkbPWHvPLSOZTw2Roqnn3xPN12i2bR4bGP30s9n2dlfZssjajVmxSKBWyWMbuwV2nlur2gWGJqz17WblwBMrAak2WMwsF4spWSNE0RSqI9bzzWS8lUtcjMZJm5+UmSYYlGrQhYTt6zHz8IaE7WmZltMlUp8qMfvc5fbbf5wuefZHW9zUOHp5kv+QQ5B10ooJQch78dT9Nhb8CPXzvPuzf6LK912TsIeObzT3Pk8H5e+clLfPlvn+OXnnyYtZ0d0izhyJ4GzYkGmxm4ntvTXr7U0lrR3dmg115DSg9rBFhDv9PDdVykVERRSGbNmLdwXBwlqJVz7Nk/RXOyBqnh/PnLXLm9wWbXMIhShI2pFgNySpKmGcvXlvnJi68xMdXggVMLDDe22Wm1mSwV0fkcwqQoqUijiLtLq7xyfo3NKE+sBe+9f52TJ27z5FOfpNXucfvaBUZxQjdMsRiOzU/y8cc+xgvnV+kP2x0tpbrd2lgjiUNqE7NjdM4IMHS2PUq1Ko6jMWlCEoUEhRL5Qh4tLIVCQH2yRqfT5wc/eoPQeNzpBFy63aHXaxP3dvBkxoMPHOPk40/x0vef59rlm5w6sUijWWctjLCZQakxkyWFxBqDsJZBCgNRot/vMz2/l3CqSaY1aRhy5MAM9rEzKCvYbvfJuQEPnD7G5MQkw/41htqs6rDfvZgmMbOL95AvVolefZFqucr0RAOLS2sY0R5mgGDY7lCqTVBpNJD9dXLFHCnwl3/9TYxX59TZ+7jx/jrDuIuWikx7DMOY7XbI6YceASUZ3DpPuVhEKEm5WUUphXYdBAYBxGky5gVLdRrzDu3BZRZOHMbNBeyfb1At+KSDNkcWplnfGEAc40rN4ePH+PFr57l5e5kgbl7VSRJdLVRqSa5Ud+q1OgvVlIV6nv2zk1y/s8Xyygbnrq6xMtIMOy0cm9KcmmZ0YwM38LDAjaU1fuGLTzM1NY09v4YTSMJBnyDnMb24yCDuETiKzz3zc1x7w0NqiTUZucJ4EhgjBosVFiUEnudSKFdw8inTR0/QmJ3lwHSFswuTuHbEqLtBIV9gLekwUQroDUYst/q88JM32Nnpb/pxuqKOPfSpVCr3KS8o7p1oVLh/sc6RWp4csLbeYmG6TpqlLG12SeOIA/tmKdUbDLstHj59gGqtxNTUJPlimTjKWB2m6HoTLRWzC/M8+vNPM9mocHRhlpxKKPrmI+9IJNJapAApxryyoyVaabTj8/7Vu4hciUCkPHnfYU4v1GjduYZJIrTjs7O5QZrE3FjbYW27z7vnrxIn2QvpKPyauvexp8HYfW6QfywzGVFrEy/q0+0PiaMRzWqFWjHgzmab1a0285M1ZqcmiZXDsb1lyqUC0xM1ijlJs9nk4rUVeomiNjvH5J4ZTh6e577909TzDkl3hTTqoaREjUUH1K5Cg5CoXT4EBPVyEWFS+ndvM+EZDkzmcLM26bCP6+ZIjKXbazM3N8WLP3uPN9+7hbESYc2fxaPwdXXyUz+Pq52+6/q/Peh26HZbOI5CpAnVfMBWP6FZLRK4mhtrOwiTsTg3Qa4+wWTFo1HOE8YJkoRaKcDGMTev3WZzYwMb9pgraJ76xBnady+T9jdwlBzzh7vnQxlKCoEWAi3GtJy1hn3TVY4emOLwXJmqZ9AWtOOjXYf2KOL7L77Gj147z+Ubmwi3gOd5xmbJlzCmq+MMmtXqG2FvcGXQ7xyKhhlCKu5vFlkZCjL6eJFlbt9+zrQSbi/dptva5MixJp3Q7OIUC0LRbW1x5sgUWsP5C1cpliUzBcP1t3+CyvpopcYA+iOBEBC7hmKRiF3mSYEQKGC6WgAhSDOLMeBqjXI9Lt64wh//2V8BNaqTs3iAELyE496xMkZvrm+RDwKw6V9kafLHWrkME8vlqEhqLMV8nkujmEIGZw/tpWR7dPsdmiVJYlKGUYxSY9hlECTxgMXpHLPlexBCUMwn6GysQArGJV2O7dqltAUKgZAWYwUWgaM1WHYRkN2FdBIhwPVcIgTLK2uApVifxPELmCwlM+n/EFKjRIZeuXULV8PM9NT/dBz3D6yydR34tEJFLl+kozWDcJvMGPKux+MPn8YtuIzSIYt7p0mzDK3kWNUUAmkhcBzyVQ8pP/LNrgfGIoXavfWHSgyM6W6sIYpiPNflQ53UCoGw42eNMLhaMRKazREgKmRWkGUWofRVYdL/YywI6aH8+h6G3Rauo+Lm1LSDkJ8ymSGJI1KTMuwPkF5+zD94UKkGzE6U8ISlOzRY7eG7/5QncpeTV1KMw06NSc0x7abQSqGVRkr10VwnhEQrBwEMhkO01jjKQexSdUrJ8coFgqCYY9tIXnjnJu0IgiBHv9cFY/6tVupty1jG1UpY2hvrXB32cYT4L83p2d/rtOJZxw8wWYpWEs9z2dpcJww8htbh5u1bBFphixMc8ix598O8UGMpz4K1Aosch54QH/2qXcPH6srYm9ZCliYMh13C4YByobQb3mOPKimRCNACXcyzs9pleWmZM2dP8sBDH+fiexfPnXvltb/aursOKIQENbHvHobdDqNOm1G3k5SrtevV5sQXpeOgpML1ApJwSGZA+hW2OyHL610ip47naLQZMjQed3ZCOoMQJQy+/tAr4y/9YXlXQn1UDcVHxo6NTOOILE3JBTlKu6D4wyPVOD/dnIsoV3j76l1++OI5Hrj/LPeeOp65vvP5TNiVNE0IB0Oy1KBmFu9l2Oti4mis9I9Gl6vN5kKhUj0lhCQcjsjShEKlhjUZo+EAXajh5osw6rAzSAmNg8Diaci7msDVuFrvVsEx+aPk+Pi+h/PhhYVACYmjJK7vUSgUyOXy6A/DV0nE7skwFJoNdmLLj16+QGR9Tpw8zvry0h/euHLpb+M4olQpIpRmOAxRe46cJotCRr0eSkC/06Hf7Xy/UC4/05icmEBY0tQQhyPCYQ+lJH4QYNKIOE6xBholj6lGgVLOJ+c5aAFCKRAOxoxnN600nucSxgkJYkzXaT0mUbVLmFqiKCIXjMcioQRWS1AKKyw65+PX6rxzdYXX3rrJmQceoFgKfnj7+vV/efXSB7a9uYHnexRrFayUqObeI3iey7DTwWYZWmn6rZ2kvbn5XGOq8Suz83tKkDHodLHGjPXjNEE6Y4K0ohOsVHSGZgxgJQS5EnhFokyASfFcheu4DPtD3jj3Pju9mDiF4TBmpxdya2Wbt986z93lFQ4dWUAoCUqCllgpkY6m0Gxwc7PHd396kW6kOXRw4YMkTv7Z9tZmf2t7iyxOGPX7RNEIJCgvVydXyKOkpLOzjVYa1/MYtNudW5cvvxD4/hcPHT0U5Ip5+v0BaZruVjNAKQbWo9Puw6BDo+wSlMqU5w7jFyvIUZdAWBIEVmreevkNLrz8LunOgP76DttLq9z64Dpv/+xNLrz1Hq6ruP+R+xAKjASrJMrR5GpV1kcZ3/zB+5y7sElQqi4PRoNPlorFtdbWOutrayRJQhyOuLu8xPrKHVQu3yBLUzKTEo0GgEFpB+04ZGm6cfX8+ee2NrZ+cXpuT6kxNUWcjEUCYzPSOCLstXGUpRdbbrdDZg8dp1YtYVsrqLDHyEhCHAb9iOXX32GxNklZa7wkxk0TygLK+QKiVKQ4UeLUffeMKQIBSilytSp3ezF/+39f59V3lskXq5dy+dynpXaW1ze2uLu0xKDbpd/v0Wm3GPYHZHGMKpQbmDQjjkZYk5Gl2a4wp5BK4Xrexs7G+rNX3j//6W6rNVlrNKnU6oAlTTOk1GTSQ3g5JqanEVoTdrawSUiifExQolhtsHPnLtHNJRztYCXYXfEPC9rReM0azUP7OLBvksxYXN+jODXFi69f4Y//5OtcXepQKFdeSuPwKaXlqiNSVDpEiZTt7U12tneIRiFZmmCsQRWqk1ibjYVsKTEY0iQbN9zdpS4pZcea7C931tYmlq9fu2/Q6VCqlKg1mijHwWQGKSxIhe84WAG6UMHJV/CDPFOzs2zevMzmrRU8a1C7UMpaSxqGDBKDv2eKw6cWaZR88qUi7RT+/Cvf5b/96T9wdyMy+XzpP7me/g3XdweFYpE9e2YIPEU4GnHj+i12trdJkwRjx+uDqlCZxpIBY9LR8Vwm98yhHY9eq421FqUVUooU5LezzLw9aLUe3ly5U95ZX8VkKcVKiUKlQpqk9IYhTqFEfaLJ/r2z7J1qMtq8Q9W3hEGeYSaIRzFJGJEJSbBnL41Tx5k7Ns+hvTVcL+ClC8v85bOv89zzbzIYmStae19ob7e+XKpXzNSeJsN+F4RgZWWV9Y0twtEAhcD1XLTrjhdBm3uOI1WGtQYhx9CoWKuz/8gJ4jDm5uVL7Kytoncpb+VoBDYQQvxrx/P/jZ/PNQrVKtMLC0zOzlKsVJHaYbJW5fiBvXhxh9XzrzBVDSjXakSJZLDTw8QZ+UqJ6lSdqckSgWu5eG2Zv/vuq7z4+lV22skd18v9UX+n/eeu78eNPTOcfOBeyrUSyzduI6Wh32oxGHRJRgOyKCLOUjJjyIxBTO07RTzqox2F0HK8/yQEpUqDxtQclUaVsN/n7s3btDY3kEqSKxbwfA+lnbxQ+ne063wpXyodLtcbNCabFCpl8oUyjvawYZdquk0tL9FpzMxEjVq9Tq1RY3q6gV/w2by7wbeef5lnv3eOlXb6zq1rS38mLF85et99cb/Tw/E9PvaZx5iem+Lu7dtsb24Tj/pEgzaj4XCcV5nB7K45SaUQ+48/PNbDPA/laYQUZGlKOApxVI7pfQvM7JtDO4rWxiatrW3SNEFYcH0XP1/E9X2UVg8IpT8X5IIngpx/Kl+t6Vq9QtlTVHw4UDTsbG0R72wzO9mgXK/g5Aqs7Qyi53/89rkXfnbh+UgE3ygUq+d3VtfRnsOnv/AF8gWfzCTU6jVGgw53l5bIkoxRr8No1CNLxxsJ4/2sFGNASoE4evYJtu7cJgpD3JyPcvV4d1cI+jt9Bt2Q6X1zHD17Ci/I0d5ukcYxwliSNBk3UCz5UokgXyCJY4aDYW5m39zRT5xdPDwdmD3b/bh6rCFLSklz+eLVftbubrque/PHF1Yv//Dc9avL66240pjgwKFD3F1aJUsNh07fy/57j1Kp5gn7HTburNDrtLDGkiUJ0aBHmqVIMZa90jQls9l4dJKS/wc5qEIC0LunYAAAAABJRU5ErkJggg=="></li>
                        <li><em>账</em>号：<span>${loginMember.account}</span>
                        </li>
                        <li><em>等</em>级：<span><img style="width: 35px;height: 16px;display: none" id="levelIcon" src="" alt="">&nbsp;&nbsp;${levelName}</span></li>
                        <li>成长值：<span>${allMoney}</span></li>
                        <li style="color: rgb(153, 153, 153);">每充值1元加1分</li>
                    </ul>
                    <div class="levelBar"><p class="u-progress"><span
                            class="pgbar" style="width: <fmt:formatNumber value="${allMoney/nextLevel.depositMoney.longValue()}" type="percent" maxFractionDigits="2" minFractionDigits="2"/>;"><span class="pging"></span></span></p>
                        <em class="point"></em>
                        <div class="levelBarInfo">
                        	<em>${levelName}</em>
                           <p><span style="color: rgb(241, 66, 65);">${allMoney}${nextLevel ne null?'/':''}</span>${nextLevel.depositMoney} </p><i>${nextLevel.levelName}</i>
                         </div>
                    </div>
                    <h6 style="padding-left:20px;">等级机制</h6>
                    <table width="80%" id="Gradelist">
                        
                    </table>
                </div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData()
				initRdsData2()
			})
			function initRdsData() {
				$.ajax({
					url : '${base}/center/member/meminfo/levelList.do',
					data : {},
					success : function(res) {
						var html = '<tr><th>等级</th> <th>成长值</th><th>晋级奖励(元)</th></tr>';
						$.each(res.rows,function(index, item) {
							if(item.giftMoney){
								
							}else{
								item.giftMoney = '暂无数据';
							}
							if(item.status == 2){
                                html+='<tr><td>'+item.levelName+'</td><td>'+item.depositMoney+'</td><td>'+item.giftMoney+'</td></tr>'
                            }
						})
						$("#Gradelist").html(html)
					}
				})
			}
			function initRdsData2() {
				$.ajax({
					url : '${base}/native/accountInfo.do',
					data : {},
					success : function(res) {
                        if(res.content.levelHead){
                            $("#levelIcon").attr('src',res.content.levelHead)
                            $("#levelIcon").show()
                        }
					}
				})
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>