<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>合买详情-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/joint.css"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/joint_detail.css"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-browser.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="main" id="main">
		<input type="hidden" value="2" id="navId" />
		<div class="qznav">
			<div class="s-logo"><img src="${base}/common/template/lottery/lecai/images/gclogo/${lotCode}.png" width="72" height="72"></div>
			<div class="det_h">
				<h2>${bjp.lotteryName}</h2>
				<p><span class="m_r25">此方案发起时间：<fmt:formatDate value="${bjp.createTime}" var="createTime" pattern="yyyy-MM-dd hh:mm:ss" />${createTime }</span><span class="m_r25">认购截止时间：<fmt:formatDate value="${bjp.endTime}" var="endTime" pattern="yyyy-MM-dd hh:mm:ss" />${endTime}</span><span>方案编号：${bjp.programId}</span></p>
			</div>
			<a href="#" class="m_link">返回合买列表&gt;&gt;</a>
			<div class="clear"></div>
		</div>

		<div class="box_top" style="display:block;">
			<div class="box_top_l"></div>
		</div>
		<div class="box_m">
			<div id="xx1">
				<div class="det_g_t">方案基本信息</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="buy_table">
					<tbody>
						<tr>
							<td class="td_title2">方案信息</td>
							<td class="con_content">发起人：<strong class="red eng">${fn:substring(bjp.account,0,2)}***</strong>
								<div id="fanandiv" class="tdbback" style="width: 99%;">
									<table cellspacing="0" cellpadding="0" border="0" width="100%"
										class="tablelay eng">
										<tbody>
											<tr>
												<th>方案编号</th>
												<th>彩种玩法</th>
												<th>总金额</th>
												<th>份数</th>
												<th>每份</th>
												<th>发起人提成</th>
												<th>保底金额</th>
												<th class="last_th">购买进度</th>
											</tr>
											<tr class="last_tr">
												<td>${bjp.programId}</td>
												<td>${bjp.lotteryName}</td>
												<td><strong class="red eng">${bjp.totalMoney}${not empty cashName?cashName:'元'}</strong></td>
												<td><strong class="red eng">${bjp.branchNum}份</strong></td>
												<td><strong class="red eng">${bjp.oneMoney}${not empty cashName?cashName:'元'}</strong></td>
												<td><strong class="red eng">${bjp.commission}%</strong></td>
												<td><strong class="red eng">${bjp.them*(bjp.totalMoney/bjp.branchNum)}${not empty cashName?cashName:'元'}（<fmt:formatNumber value="${bjp.them/bjp.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/>）</strong></td>
												<td class="last_td"><strong class="red eng"><fmt:formatNumber value="${bjp.surNum/bjp.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/></strong></td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td_title2">方案详情</td>
							<td class="con_content">
								<div id="fanandiv" class="tdbback" style="width: 99%;">
									<table cellspacing="0" cellpadding="0" border="0" width="100%"
										class="tablelay eng">
										<tbody>
											<tr>
												<th>序号</th>
												<th>玩法</th>
												<th>投注期号</th>
												<th>倍数</th>
												<th>本期投入</th>
												<th>累计投入</th>
												<th>出票状态</th>
												<th>开奖号码</th>
												<th>中奖情况</th>
												<th>总奖金</th>
												<th>每份奖金</th>
											</tr>
											<c:set value="0" var="allWinMoney"></c:set>
											<c:forEach items="${order}" var="o" varStatus="i">
												<tr>
													<c:if test="${o.status eq 2 && o.winMoney ne null}">
														<c:set value="${allWinMonwy + o.winMoney}" var="allWinMoney"></c:set>
													</c:if>
													<td>${i.index+1}</td>
													<td>${o.playName}</td>
													<td>${o.orderId}</td>
													<td>${o.multiple}</td>
													<td>￥${o.buyMoney}${not empty cashName?cashName:'元'}</td>
													<td>￥${o.buyMoney}${not empty cashName?cashName:'元'}</td>
													<td><strong class="green eng">${bjp.type eq 1?'出票中':bjp.type eq 2 || bjp.type eq 3 || bjp.type eq 4?'出票成功':bjp.type eq 5?'出票失败(方案失效)':'出票失败(撤单)' }</strong></td>
													<td><strong class="red eng">${o.lotteryHaoMa eq null?'未开奖':o.lotteryHaoMa}</strong></td>
													<td><strong class="blue eng">${o.status eq 1?'未开奖':o.status eq 2?'已中奖':o.status eq 3?'未中奖':o.status eq 4?'撤单':o.status eq 5?'回滚成功':o.status eq 6?'回滚异常':o.status eq 7?'开奖异常':o.status eq 8?'和':'合买失效'}</strong></td>
													<td><strong class="red eng">${o.status eq 2 && o.winMoney ne null?o.winMoney:'--'}</strong></td>
													<td><strong class="red eng">
														<c:choose>
															<c:when test="${o.status eq 2 && o.winMoney ne null}">${o.jointWinOneMoney}</c:when>
															<c:otherwise>--</c:otherwise>
														</c:choose>
														</strong>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td_title2 p_tb8">出票中奖</td>
							<td class="con_content p_tb8">
								<p>
									累计出票：<strong class="red eng">
									<c:if test="${bjp.branchNum ne bjp.surNum}">
										<fmt:formatNumber value="${bjp.surNum*bjp.oneMoney}" type="CURRENCY" maxFractionDigits="3" minFractionDigits="3"/>
									</c:if>
									<c:if test="${bjp.branchNum eq bjp.surNum}">
										${bjp.totalMoney}
									</c:if>
									${not empty cashName?cashName:'元'}</strong> 
									(注：累计出票为已认购份数*每份金额，当出票金额与总认购金额相等(不足时由发起人补足)，出票成功，否则出票失败)
								</p>
								<p>
									累计中奖：<strong class="red eng">￥${allWinMoney}${not empty cashName?cashName:'元'}</strong>
								</p>
							</td>
						</tr>
						<tr>
							<td class="td_title2 p_tb8">是否结算</td>
							<td class="con_content p_tb8"><p>${bjp.type eq 1?'未开奖':bjp.type eq 2 || bjp.type eq 3?'未结算':bjp.type eq 4?'已结算':bjp.type eq 5?'已失效':'已撤单'}</p></td>
						</tr>
						<tr>
							<td class="td_title2 p_tb8">状态描述</td>
							<td class="con_content p_tb8">
								<p>状态：已完成</p>
								<p>描述：${bjp.type eq 1?'出票中':bjp.type eq 2 || bjp.type eq 3 || bjp.type eq 4?'出票成功':bjp.type eq 5?'出票失败(方案失效)':'出票失败(撤单)' }</p>
							</td>
						</tr>
						<tr>
							<td class="td_title2 p_tb8">方案号码</td>
							<td class="con_content p_tb8">
								<div style="padding: 6px 0 6px 6px; border: #e0e2e1 solid 1px; width: 99%; overflow-y: auto;line-height:20px;max-height:100px;">
									<c:choose>
										<c:when test="${bjp.isOpen eq 1}">
											<c:forEach items="${order}" varStatus="i" var="o">[${o.lotName}-${o.playName}]&nbsp;${o.haoMa}<br></c:forEach>
										</c:when>
										<c:when test="${bjp.isOpen eq 2}">
											<c:choose>
											<c:when test="${bjp.type eq 3 || bjp.type eq 4}">
												<c:forEach items="${order}" varStatus="i" var="o">[${o.lotName}-${o.playName}]&nbsp;${o.haoMa}<br></c:forEach>
											</c:when>
											<c:otherwise>截止后公开</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>方案完全保密</c:otherwise>
									</c:choose>
								</div>
							</td>
						</tr>
						<c:if test="${bjp.type eq 1}">
						<tr>
							<td class="td_title2">我要认购</td>
							<td class="con_content">
								<c:if test="${isLogin eq true}">
								<div class="buy_btn">
									<a id="addproject" href="javascript:void 0" class="btn_buy_m" pro_id="${bjp.id}" p_id="${bjp.programId}"
										title="立即购买">立即购买</a>
								</div>
								<p>
									${loginMember.account}，您的账户余额为<strong class="red eng" id="nowMoney">${loginMember.money}</strong>${not empty cashName?cashName:'元'}【<b
										class="i-jb"></b><a target="_blank" href="${base}/center/banktrans/deposit/cp3Page.do">账户充值</a>】
								</p> 还可以认购 <span class="red eng">${bjp.branchNum-bjp.surNum}</span> 份，我要认购 <input
								name="buynum" id="buynum" type="text" class="mul" value="1" sur_num="${bjp.branchNum-bjp.surNum}" m="${bjp.oneMoney}" bfl="<fmt:formatNumber value="${1/bjp.branchNum}" type="percent" maxFractionDigits="3" minFractionDigits="3"/>">
								份，<span class="red eng"
								id="permoney"><fmt:formatNumber value="${bjp.oneMoney}" type="CURRENCY" maxFractionDigits="3" minFractionDigits="3"/></span>${not empty cashName?cashName:'元'}/<span class="red eng" id="szBFL"><fmt:formatNumber value="${1/bjp.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/></span>&nbsp;(金额/所占比例)
								<p></p>
								<p class="read">
									<span class="hide_sp"><input type="checkbox"
										checked="checked" id="agreement" value="1"></span>我已阅读并同意《<a
										href="javascript:void 0">用户合买代购协议</a>》
								</p>
								</c:if>
								<c:if test="${isLogin eq false}">未登录，请先&nbsp;<a href="javascript://" onclick="loginBtn();"><strong class="red">登录</strong></a></c:if>
							</td>
						</tr>
						</c:if>

					</tbody>
				</table>
			</div>
			<div id="xx2">
				<div class="det_g_t">方案分享信息</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="buy_table">
					<tbody>
						<tr>
							<td class="td_title2">方案宣传</td>
							<td class="con_content">
								<div class="copy_link" style="position: relative; width: 200px;">
									<a href="javascript:void(0);" onclick=""
										id="copystr" style="line-height: 28px;">点击复制方案地址</a>
								</div>
								<p class="gray">
									方案标题：百万大奖得主期待和你一起圆梦<br>
									方案描述：${bjp.describe}
								</p>
							</td>
						</tr>
						<tr class="last_tr">
							<td class="td_title2">合买用户</td>
							<td>
								<div class="yh_tab">
									<ul class="clearfix" id="joinTab">
										<li class="an_cur">总参与人数(${fn:length(pateList)})人</li>
										<li>您的认购记录</li>
									</ul>
								</div>
								<div id="show_list_div" style="display:;">
									<table class="hm_tb_t" width="100%" cellspacing="0"
										cellpadding="0" border="0" style="display: table;">
										<thead>
											<tr>
												<th>用户名</th>
												<th>认购总份数</th>
												<th>认购总金额</th>
												<th>已认购金额</th>
												<th>所占比例</th>
												<th>最后认购时间</th>
												<th>分得奖金</th>
												<th>中奖积分</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${pateList}" var="pate" varStatus="p">
											<tr class="my">
												<td>${fn:substring(pate.account,0,2)}***</td>
												<td><span class="eng">${pate.branchNum}</span> 份</td>
												<td><span class="eng">￥${pate.totalMoney} ${not empty cashName?cashName:'元'}</span></td>
												<td><span class="eng">￥${pate.totalMoney} ${not empty cashName?cashName:'元'}</span></td>
												<td><span class="eng"><fmt:formatNumber value="${pate.branchNum/bjp.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/></span></td>
												<td><span class="eng"><fmt:formatDate value="${pate.createTime}" var="pateCreateTime" pattern="yyyy-MM-dd hh:mm:ss" />${pateCreateTime }</span></td>
												<td><span class="eng">￥${pate.winMoney eq null?0.00:pate.winMoney}${not empty cashName?cashName:'元'}</span></td>
												<td><span class="eng">0 分</span></td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
									<table class="hm_tb_t" width="100%" cellspacing="0"
										cellpadding="0" border="0" style="display: none;">
										<thead>
											<tr>
												<th class="hm_name">用户名</th>
												<th>认购总份数</th>
												<th>认购总金额</th>
												<th>已认购金额</th>
												<th>所占比例</th>
												<th>最后认购时间</th>
												<th>分得奖金</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${isLogin eq false}">
												<tr class="my">
														<td colspan="5">未登录，请先<a href="javascript://" onclick="loginBtn();">登录</a></td>
													</tr>
											</c:if>
											<c:if test="${isLogin eq true}">
											<c:set value="0" var="len"></c:set>
											<c:forEach items="${pateList}" var="pate" varStatus="p">
													<c:if test="${pate.account == loginMember.account}">
														<c:set value="${len+1}" var="len"></c:set>
														<tr class="my">
															<td>${fn:substring(pate.account,0,2)}***</td>
															<td><span class="eng">${pate.branchNum}</span> 份</td>
															<td><span class="eng">￥${pate.totalMoney} ${not empty cashName?cashName:'元'}</span></td>
															<td><span class="eng">￥${pate.totalMoney} ${not empty cashName?cashName:'元'}</span></td>
															<td><span class="eng"><fmt:formatNumber value="${pate.branchNum/bjp.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/></span></td>
															<td><span class="eng"><fmt:formatDate value="${pate.createTime}" var="pateCreateTime" pattern="yyyy-MM-dd hh:mm:ss" />${pateCreateTime }</td>
															<td><span class="eng">￥${pate.winMoney eq null?0.00:pate.winMoney}${not empty cashName?cashName:'元'}</span></td>
														</tr>
													</c:if>
											</c:forEach>
											<c:if test="${len eq 0}">
												<tr class="my">
													<td colspan="5">您还没有认购记录！</td>
												</tr>
											</c:if>
											</c:if>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var base = "${base}";
		$(function(){
			$("#joinTab li").click(function(){
				$(this).addClass("an_cur").siblings().removeClass("an_cur");
				$("#show_list_div table").eq($(this).index()).show().siblings('table').hide();
			})
		})
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>