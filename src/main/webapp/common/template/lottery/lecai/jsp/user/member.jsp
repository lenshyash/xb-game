<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：会员中心</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="mylottery_section1">
				<div class="state">
					<div class="state_title title">账户全览</div>
					<div class="state_account">
						<div class="balance" style="display: inherit;">
							<strong>${loginMember.account}</strong> 欢迎您！您有<span><a href="${base}/center/news/message/cp3Page.do" class="minmessage">[0]</a></span>条消息未读
							<div class="btn_box">
								<a class="btn recharge normal" value=" " href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
								<span><a class="btn withdrawal normal" value=" "
									href="${base }/center/banktrans/draw/cp3Page.do">提款</a></span>
							</div>
						</div>
						<div class="balance" id="balance_box" style="padding: 0">
							账户余额：<span class="red" id="user_balance">${loginMember.money}</span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/hisrecord/cp3Page.do?type=3">[余额明细]</a>
							<c:if test="${not empty mnyScore && mnyScore eq 'on'}"> &nbsp;&nbsp;&nbsp;我的积分：<span class="red">${userInfo1.score}</span><c:if test="${not empty exchangeScore && exchangeScore eq 'on' }"><a href="${base}/center/banktrans/exchange/cp3Page.do"> [积分兑换]</a></c:if></c:if>
							&nbsp;&nbsp;&nbsp;最后登录时间：<font color="#ff8400">${userInfo1.lastLoginDatetime}</font>
						</div>
						<div class="dj">
							今日彩票消费：<span class="red">${-allBetAmount}</span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3Page.do?form=order">[彩票投注记录]</a>
							&nbsp;&nbsp;&nbsp; 今日彩票中奖：<span class="red">${allWinAmount}</span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3Page.do?form=win">[彩票中奖记录]</a>
							&nbsp;&nbsp;&nbsp; 今日彩票盈亏：<span class="red">${yingkuiAmount}</span>${not empty cashName?cashName:'元'}&nbsp;&nbsp;&nbsp;
						</div>
						<c:if test="${isTyOnOff eq 'on'}"><div class="dj">
							今日体育消费：<span class="red"><fmt:formatNumber value="${-daily.sportsBetAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3SportPage.do?form=order">[体育投注记录]</a>
							&nbsp;&nbsp;&nbsp; 今日体育中奖：<span class="red"><fmt:formatNumber value="${-daily.sportsWinAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3SportPage.do?form=win">[体育中奖记录]</a>
							&nbsp;&nbsp;&nbsp; 今日体育盈亏：<span class="red"><fmt:formatNumber value="${-daily.sportsBetAmount-daily.sportsBetAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}&nbsp;&nbsp;&nbsp;
						</div></c:if>
						<c:if test="${isZrOnOff eq 'on'}"><div class="dj">
							今日真人消费：<span class="red"><fmt:formatNumber value="${daily.realBetAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3RealPage.do?form=order">[真人投注记录]</a>
							&nbsp;&nbsp;&nbsp; 今日真人中奖：<span class="red"><fmt:formatNumber value="${daily.realWinAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3RealPage.do?form=win">[真人中奖记录]</a>
							&nbsp;&nbsp;&nbsp; 今日真人盈亏：<span class="red"><fmt:formatNumber value="${daily.realBetAmount-daily.realWinAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}&nbsp;&nbsp;&nbsp;
						</div></c:if>
						<c:if test="${isDzOnOff eq 'on'}"><div class="dj">
							今日电子消费：<span class="red"><fmt:formatNumber value="${daily.egameBetAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3EgamePage.do?form=order">[电子投注记录]</a>
							&nbsp;&nbsp;&nbsp; 今日电子中奖：<span class="red"><fmt:formatNumber value="${daily.egameWinAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}<a href="${base}/center/record/betrecord/cp3EgamePage.do?form=win">[电子中奖记录]</a>
							&nbsp;&nbsp;&nbsp; 今日电子盈亏：<span class="red"><fmt:formatNumber value="${daily.egameBetAmount-daily.egameWinAmount}" pattern="#0.0#"/></span>${not empty cashName?cashName:'元'}&nbsp;&nbsp;&nbsp;
						</div></c:if>
					</div>
					<c:if test="${isLevel eq true}">
					<ul class="state_caibei">
						<li>会员等级：<span class="red" id="user_credit_total"><a id="user_level" href="javascript:void(0);" target="_blank" title="会员等级：${levelName}">${levelName}</a></span>
						</li>
					</ul>
					<div class="caibei_level_box">
						<div id="level_progress_num" class="level_icon_box">
							<span style=""
								id="level_icon_right" class="level_icon_right">${nextLevel.levelName}</span>
							<div class="clear"></div>
						</div>
						<div class="level_progress_bar">
							<div id="level_progress_bar" style="width: <fmt:formatNumber value="${allMoney/nextLevel.depositMoney.longValue()}" type="percent" maxFractionDigits="2" minFractionDigits="2"/>;"
								class="now_progress_bar">
								<div class="caibei_value">
									<span id="level_progress_credit">${allMoney}${nextLevel ne null?'/':''}</span> <span
										id="nextlevel_progress_credit">${nextLevel.depositMoney}</span>
								</div>
							</div>
						</div>
					</div>
					<p id="need_caibei" class="need_caibei"><c:if test="${nextLevel ne null}">还需充值${diffMoney}即可升级到${nextLevel.levelName}</c:if></p>
					</c:if>
				</div>
				<div class="clear"></div>
			</div>
			<div class="mylottery_section3">
				<div class="new_lottery">
					<div class="new_lottery_title title">
						<a href="${base}/center/record/betrecord/cp3Page.do?form=order" class="edit">查看详情</a>
						<h1>最新投注</h1>
						<div class="clear"></div>
					</div>
					<table class="new_lottery_tab" id="new_lottery_tab" cellspacing="0"
						cellpadding="0">
						<thead>
							<tr>
								<td width="17%">彩种</td>
								<td width="17%">期号</td>
								<td width="17%">投注时间</td>
								<td>金额</td>
								<td>操作</td>
								<td width="23%">状态</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>