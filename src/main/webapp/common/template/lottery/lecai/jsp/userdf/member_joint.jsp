<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
	table.tablelay th{
		background:#4aa9db;
	}
	.show-details{
		background: none;
	}
	.hide-details{
		background: none;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">合买记录</div>
            <div class="userMain mgb10">
                <ul class="searchFirst">
                    <li><span>彩种：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                    <option value="">所有彩种</option>
									<c:forEach items="${lotList}" var="lot">
										<option value="${lot.code}"<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
									</c:forEach>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                    <li class="_state">
                    	<span>类型：
       						<a class="userSearch time active" value="">全部</a>
       						<a class="userSearch time " value="1">未满员</a>
       						<a class="userSearch time " value="2">已满员</a>
       						<a class="userSearch time " value="6">已撤单</a>
       					</span>
       				</li>
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr>
                            <th colspan="1">方案编号</th>
                            <th>发起时间</th>
                            <th>方案金额</th>
                            <th>方案提成</th>
                            <th>每份金额</th>
                            <th>进度</th>
                            <th>中奖金额</th>
                            <th>状态</th>
                            <th>详情</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
		    $(function () {
		    	initRdsData(1)
		        $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            lottereyStatus = $(this).attr('value')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		        	lottereyStatus = $(this).attr('value')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $(".Records_listCont").on("click","div",function(){
				var pid=$(this).attr("pro_id");
				if($(this).hasClass("show-details")){
					$(this).text("隐藏");
					$(this).attr("class","hide-details");
					$("#tr_"+pid).show();
				}else{
					$(this).text("查看");
					$(this).attr("class","show-details");
					$("#tr_"+pid).hide();
				}
			})
		    })
/* 		    var json = '{"page":{"currentPageNo":1,"hasNext":false,"hasPre":false,"list":[{"account":"cs123","accountId":623,"branchNum":10,"buyNum":1,"calNum":1,"commission":0,"createTime":"2018-10-21 12:20:28","describe":"该用户只想中大奖！","endTime":"2018-10-21 12:28:50","id":5,"isOpen":3,"lotCode":"CQSSC","lotType":52,"lotteryName":"重庆时时彩|定位胆","oneMoney":1.000,"programId":"J18102100053","qiHao":"20181021039","stationId":41,"status":1,"surNum":10,"them":9,"totalMoney":10.000,"type":4,"winNum":0,"winTotalMoney":0.000},{"account":"cs123","accountId":623,"branchNum":10,"buyNum":1,"calNum":1,"commission":3,"createTime":"2018-10-21 12:19:37","describe":"该用户只想中大奖！","endTime":"2018-10-21 12:28:50","id":4,"isOpen":3,"lotCode":"CQSSC","lotType":52,"lotteryName":"重庆时时彩|定位胆","oneMoney":1.000,"programId":"J18102100052","qiHao":"20181021039","stationId":41,"status":1,"surNum":10,"them":9,"totalMoney":10.000,"type":4,"winNum":0,"winTotalMoney":0.000}],"nextPage":1,"pageSize":20,"prePage":1,"results":[{"$ref":"$.page.list[0]"},{"$ref":"$.page.list[1]"}],"set":[{"$ref":"$.page.list[1]"},{"$ref":"$.page.list[0]"}],"start":0,"totalCount":2,"totalPageCount":1},"J18102100052":[{"account":"cs123","accountId":623,"ago":70,"buyMoney":10.00,"buyZhuShu":5,"createTime":"2018-10-21 12:19:37","haoMa":"1,2,3,4,5","id":12310,"jointPurchase":"J18102100052","logId":0,"lotCode":"CQSSC","lotName":"重庆时时彩","lotType":52,"lotteryHaoMa":"6,3,7,8,7","model":1,"multiple":1,"odds":17.000000,"openTime":"2018-10-21 12:31:46","orderId":"L18102100053","parents":",594,","playCode":"dwd","playName":"定位胆","qiHao":"20181021039","rollBackStatus":2,"roomId":0,"stationId":41,"status":3,"winMoney":0.00,"winZhuShu":0,"zhuiHao":"1"}],"J18102100053":[{"account":"cs123","accountId":623,"ago":70,"buyMoney":10.00,"buyZhuShu":5,"createTime":"2018-10-21 12:20:28","haoMa":"1,2,3,4,5","id":12311,"jointPurchase":"J18102100053","logId":0,"lotCode":"CQSSC","lotName":"重庆时时彩","lotType":52,"lotteryHaoMa":"6,3,7,8,7","model":1,"multiple":1,"odds":17.000000,"openTime":"2018-10-21 12:31:46","orderId":"L18102100054","parents":",594,","playCode":"dwd","playName":"定位胆","qiHao":"20181021039","rollBackStatus":2,"roomId":0,"stationId":41,"status":3,"winMoney":0.00,"winZhuShu":0,"zhuiHao":"1"}]}'
 */		    function initRdsData(p) {
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
					code : $("#lotCode").val(),
					type : lottereyStatus,
					startTime : startTime + " 00:00:00",
					endTime : endTime + " 23:59:59",
					rows : 10,
					page : p
				}
			$.ajax({
				url : '${base}/lotteryV3/joint/userJoint.do',
				data : data,
				success : function(res) {
					$(".notContent").show()
			    	$(".loadingGif").hide()
					if (res.page.totalCount == 0) {
						//无数据
								
					} else {
				    		page = res.page.currentPageNo;
							lotteryPage(res.page.totalCount，res.page.totalPageCount)
							lastPage = res.page.totalPageCount
							$("#pageNum").html(res.page.currentPageNo)
						if(p==0){
							$(".Records_listCont").html(addTemplate(res))
						}else{
							$(".Records_listCont").html(addTemplatePate(res))
						}
					}
				}
			})
		}
		    function addTemplate(res){
		    	var temp = "",statusName="";
		    	$.each(res.page.list,function(index, item) {
					switch (item.type) {
					case 1:
						statusName = '<span style="background-color: #428bca;color:#fff;border-radius:3px;padding:3px 5px;">认购中</span>';
						break;
					case 2:
						statusName = '<span style="background-color: #5cb85c;color:#fff;border-radius:3px;padding:3px 5px;">已满员</span>';
						break;
					case 3:
						statusName = '<span style="background-color: #d9534f;color:#fff;border-radius:3px;padding:3px 5px;">已截止</span>';
						break;
					case 4:
						statusName = '<span style="background-color: #5bc0de;color:#fff;border-radius:3px;padding:3px 5px;">已完成</span>';
						break;
					case 5:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">已失效</span>';
						break;
					case 6:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">已撤单</span>';
						break;
					}
					temp += '<tr '+ (index % 2 != 0 ? 'class="bgcolor"': '')+ '>';
					temp += '<td>'+item.programId+'</td>';
					temp += '<td>'+item.createTime+'</td>';
					temp += '<td><strong class="red">'+item.totalMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
					temp += '<td>'+item.commission+'%</td>';
					temp += '<td><strong class="red">'+item.oneMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
					temp += '<td style="text-align:left;padding-left:5px;">';
					temp += '<span style="margin-left:0px;;color:000;padding:6px;">'+(item.surNum/item.branchNum*100).toFixed(2)+'%</span> + <span style="margin-left:0px;color:#fff;background:#b90000;border-radius:0;">'+(item.them/item.branchNum*100).toFixed(2)+'%保</span><br>';
					temp += '<div style="width:120px;margin-top:3px;height:6px; font-size:0px; line-height:0px;border:1px solid #b90000;background:#fff;">';
					temp += '<img src="" width="'+(item.surNum/item.branchNum*100).toFixed(2)+'%" height="6"></div></td>';
					temp += '<td><strong class="red">'+(item.winTotalMoney==null?'0':item.winTotalMoney)+'</strong>${not empty cashName?cashName:"元"}</td>';
					temp += '<td>' + statusName + '</td>';
					temp += '<td><div class="show-details" pro_id="'+item.programId+'">查看</div></td></tr>';
					temp += '<tr id="tr_'+item.programId+'" style="display:none;" class="details-bgcolor"><td id="td_'+item.programId+'" colspan="9" class="td-border-left">';
					temp += '<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tablelay eng"><tbody><tr><th>序号</th><th>玩法</th><th>投注期号</th><th>倍数</th><th>本期投入</th><th>累计投入</th><th>出票状态</th><th>开奖号码</th><th>中奖情况</th><th>总奖金</th><th>每份奖金</th></tr>';
					$.each(res[item.programId],function(index,it){
						temp += '<tr><td>'+(index+1)+'</td>';
						temp += '<td>'+it.playName+'</td>';
						temp += '<td>'+it.orderId+'</td>';
						temp += '<td>'+it.multiple+'</td>';
						temp += '<td>￥'+it.buyMoney+'${not empty cashName?cashName:"元"}</td>';
						temp += '<td>￥'+it.buyMoney+'${not empty cashName?cashName:"元"}</td>';
						temp += '<td><strong class="green eng">'+(item.type==1?'出票中':item.type==2 || item.type==3 || item.type==4?'出票成功':item.type==5?'出票失败(方案失效)':'出票失败(撤单)')+'</strong></td>';
						temp += '<td><strong class="red eng">'+(it.lotteryHaoMa==null?'未开奖':it.lotteryHaoMa)+'</strong></td>';
						temp += '<td><strong class="blue eng">'+(it.status==1?'未开奖':it.status==2?'已中奖':it.status==3?'未中奖':it.status==4?'已撤单':it.status==5?'回滚成功':it.status==6?'回滚异常':it.status==7?'开奖异常':it.status==8?'和':'合买失效')+'</strong></td>';
						temp += '<td>'+(it.status==2 && it.winMoney != null?it.winMoney:'--')+'</td>';
						temp += '<td>'+(it.status==2 && it.winMoney != null?'￥<strong class="red eng">'+(it.winMoney/item.branchNum).toFixed(3)+'</strong>${not empty cashName?cashName:"元"}':'--')+'</td></tr>';
					})
					temp += '</table></td></tr>';
				})
				return temp;
		    }
		    function addTemplatePate(res){
				var temp = "",statusName="";
				$.each(res.page.list,function(index,item){
					switch (item.status) {
					case 1:
						statusName = '<span style="background-color: #428bca;">认购成功</span>';
						break;
					case 2:
						statusName = '<span style="background-color: #5cb85c;">已中奖</span>';
						break;
					case 3:
						statusName = '<span style="background-color: #d9534f;">认购失效</span>';
						break;
					}
					temp += '<tr '+ (index % 2 != 0 ? 'class="bgcolor"': '')+ '>';
					temp += '<td>'+item.programId+'</td>';
					temp += '<td>'+item.createTime+'</td>';
					temp += '<td><strong class="red">'+item.branchNum+'</strong>份</td>';
					temp += '<td><strong class="red">'+item.totalMoney+'</strong>${not empty cashName?cashName:"元"}</td>';
					temp += '<td>'+item.szBFL+'%</td>';
					temp += '<td><strong class="red">'+(item.winMoney==null?0:item.winMoney)+'</strong>${not empty cashName?cashName:"元"}</td>';
					temp += '<td>' + statusName + '</td></tr>';
				})
				return temp;
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>