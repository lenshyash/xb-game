<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="zh-cn">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>购彩中心-${_title}</title>
	<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
	<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-2.1.4.min.js"></script>
	<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/lottery.css?v=" type="text/css">
	<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/common_lottery.js"></script>
	</head>
	<style media="screen">
		.betTitle .time {
			width: 285px;
		}
		.betTitle .announced {
			width: 388px;
		}
		.betRight .today .more {
			margin: 22px 0px 0 33px;
		}
		.font_gd {
	    color: #555;
	  }
	  .betRight .box table td {
	    background: #fff;
	    height: 24px;
	    line-height: 24px;
	    text-align: center;
	    font-weight: 400;
	    color: #666;
	    border-bottom: 1px dashed #ddd;
	    font-size: 12px;
	    border-right: 1px dashed #ddd;
	  }
		.betRight .winnerListSlide {
			height: 617px
		}
		.container .logo {
			border: 1px solid #ccc;
		}
	</style>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"></jsp:include>
	<div class="container">
		<div class="">
			<div class="logo">
				<div class="betTitle fix">
					<div class="betLogo">
						<h2 class="caizhong"></h2>
						<img src="">
						<p class="yinying"></p>
					</div>
					<div class="time">
						<div class="">
							<div class="timeTitle">
								距<b class="qishu"></b>期投注截止还有：
							</div>
							<em class="djs">00:00:00</em>
						</div>
					</div>
					<div class="announced">
						<div class="announcedTitle">
							第<b class="kaijiang"></b>期开奖号码：
						</div>
						<ul class="roundNo fix">
							<div class="kjdh" style="display:none">
								<img src="${base}/common/template/lottery/lecai/images/ballOpening.gif" >
								<img src="${base}/common/template/lottery/lecai/images/ballOpening.gif" >
								<img src="${base}/common/template/lottery/lecai/images/ballOpening.gif" >
								<img src="${base}/common/template/lottery/lecai/images/ballOpening.gif" >
							</div>
							<div class="kjdh_1"  style="display: flex">
								<li class="yaohao" style>1</li>
								<li class="" style>+</li>
								<li class="yaohao" style>1</li>
								<li class="" style>+</li>
								<li class="yaohao" style>1</li>
								<li class="" style>=</li>
								<li class="yaohao_h" style>1</li>
							</div>
						</ul>
					</div>
				</div>
				<div class="betContent fix">
					<div class="betLeft">
						<div class="">
							<div class="betNavCon">
								<ul class="betNav fix" style="transform: translateX(0px);">
								</ul>
							</div>
							<a class="betNavtab left">
								<span>
									<em></em>
								</span>
							</a>
							<a class="betNavtab right">
								<span>
									<em></em>
								</span>
							</a>
						</div>
						<div>
							<div class="">
								<ul class="betFilter">

								</ul>
								<ul class="betFilterAnd">
									<%-- <li class="curr">幸运28</li> --%>
								</ul>
								<div class="betTip">
									<i class="iconfont"><img src="${base}/common/template/lottery/lecai/images/jinggao.png"></i>
									<a id="game_shows">点击查看游戏说明！</a>
								</div>
							</div>
							<div class="">
								<div class="mode">
									<div class="content">

									</div>
									<div class="Panel">
										<p class="betTotal">
											您选择了
											<em class="detail">0</em>
											注,每注
												<input refinfor="!0" type="text" class="eachPrice">
											</em>
											元
										</p>
										<a id="betBtn" class="betBtn">确认选号</a>
									</div>
									<div class="Panele" style="padding-top: 27px; display:none;">
										<div class="touzhu">
											<div class="tz_jine">
												<span class="input-group">投注金额</span>
												<input type="text" id="kjmoney" placeholder="请输入投注金额" class="class="d-i"">
											</div>
											<a id="betBtnn" class="betBtn_qr">确认投注</a>
										</div>
									</div>

								</div>
							</div>
							<div data-v-956bf8b8="" class="checkedListCon">
								<div class="checkedList">
									<table>
										<tbody>
										</tbody>
									</table>
								</div>
								<p class="betTotal">
									方案注数
							    <em class="zdsl">0</em> <!---->
							    注，金额
							    <i class="tzje">0</i> <!---->
							    元
								</p>
								<!---->
								 <a id="betbtn" class="betBtn">马上投注</a>
								 <a id="stopBet" disable="true" class="betBtn greyBtn" style="display: none;">停止开奖</a>
							 </div>
						</div>
					</div>
					<div class="betRight">
						<div class="today box">
							<h3 class="">今日开奖</h3>
							<div class="more">
								<a href="${base}/lotteryV3/trend.do" id="trendChart" target="view_window">走势图</a>
								<a href="${base}/lotteryV3/rule.do" class="" target="view_window">玩法说明</a>
							</div>
							<div class="ResultsList">
								<table data-v-509555de="" width="100%" border="0" cellspacing="0" cellpadding="0" id="fn_getoPenGame" class="ty_table_gameBet curr">
									<thead>
										<tr>
											<th scope="col">期号</th>
											<th scope="col">开奖号</th>
											<th scope="col">开奖时间</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<div class="mybet box">
							<h3 id="tz_delclass" onclick="wdtz()">我的投注</h3>
							<%-- <h3 id="zh_delclass" class="notSelect" onclick="wdzh()">我的追号</h3> --%>
							<table class="_mybetContent" width="100%" broder="0" cellspacing="0" cellpadding="0">
								<tr>
									<th>期号</th>
									<th>投注金额</th>
									<th>奖金</th>
									<th>操作</th>
								</tr>
								<tbody class="tz_list">

								</tbody>
							</table>
							<%-- <table class="_betChaseAnimation" width="100%" broder="0" cellspacing="0" cellpadding="0" style="display:none;">
								<tbody>
									<tr>
										<th>期起始期号</th>
										<th>已追/总</th>
										<th>总金额</th>
										<th>奖金</th>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table> --%>
							<table class="more">
								<tbody>
									<tr>
										<td><a href="/center/df/record/betrecord/cpDfPage.do?form=order" class="font_gd">更多>></a></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="winningList box">
							<h3>中奖信息</h3>
							<%-- <h3 class="notSelect">昨日奖金榜</h3> --%>
							<div di="Ranking" class="winnerListSlide" style="width: 100%; box-sizing: content-box;">
								<div id="bettingList" style="width: 100%; display: none;">
									<div class="optionTab clearfix">
										<div class="isSelect">本局投注</div>
										<div class="">本日盈利</div>
									</div>
									<div class="bd tempWrap">
										<ul class="winnerList betList"></ul>
									</div>
								</div>
								<div class="lotteryInfo" style="width:100%">
									<table width="100%">
										<tbody>
											<tr>
												<th>中奖信息实时更新</th>
											</tr>
										</tbody>
									</table>
									<div class="bd tempWrap" id="gundong">
										<ul id="carousel" class="winnerList">
											<li id="li_0">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/1.jpg">
													<p class="color">py***6&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥10.97</span>
													</p>
											</li>
											<li id="li_1">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/2.jpg">
													<p class="color">tp***4&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥8.80</span>
													</p>
											</li>
											<li id="li_2">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/3.jpg">
													<p class="color">zz***1&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥30.17</span>
													</p>
											</li>
											<li id="li_3">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/4.jpg">
													<p class="color">qs***9&nbsp;&nbsp;1分快3
														<br >喜中
		              					<span>¥11.27</span>
													</p>
											</li>
											<li id="li_4">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/5.jpg">
													<p class="color">静***3&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥38.46</span>
													</p>
											</li>
											<li id="li_5">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/6.jpg">
													<p class="color">st***1&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥9.37</span>
													</p>
											</li>
											<li id="li_6">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/7.jpg">
													<p class="color">ps***5&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥21.37</span>
													</p>
											</li>
											<li id="li_7">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/8.jpg">
													<p class="color">ml***8&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥22.5</span>
													</p>
											</li>
											<li id="li_8">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/9.jpg">
													<p class="color">财***6&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥88.8</span>
													</p>
											</li>
											<li id="li_9">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/10.jpg">
													<p class="color">66***6&nbsp;&nbsp;1分快3
														<br >喜中
		              					<span>¥11.2</span>
													</p>
											</li>
											<li id="li_10">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/11.jpg">
													<p class="color">he***4&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥56.7</span>
													</p>
											</li>
											<li id="li_11">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/12.jpg">
													<p class="color">yt***2&nbsp;&nbsp;1分快3
														<br >喜中
		              					<span>¥25.67</span>
													</p>
											</li>
											<li id="li_12">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/13.jpg">
													<p class="color">we***6&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥103.7</span>
													</p>
											</li>
											<li id="li_13">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/14.jpg">
													<p class="color">pe***9&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥208.17</span>
													</p>
											</li>
											<li id="li_14">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/15.jpg">
													<p class="color">li***8&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥350.12</span>
													</p>
											</li>
											<li id="li_15">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/1.jpg">
													<p class="color">qq***6&nbsp;&nbsp;1分快3
														<br >喜中
		              					<span>¥17.8</span>
													</p>
											</li>
											<li id="li_16">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/2.jpg">
													<p class="color">tt***3&nbsp;&nbsp;1分快3
														<br >喜中
		              					<span>¥67.27</span>
													</p>
											</li>
											<li id="li_17">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/4.jpg">
													<p class="color">li***2&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥358.12</span>
													</p>
											</li>
											<li id="li_18">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/7.jpg">
													<p class="color">振***7&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥120.97</span>
													</p>
											</li>
											<li id="li_19">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/10.jpg">
													<p class="color">jv***9&nbsp;&nbsp;极速快3
														<br >喜中
		              					<span>¥173.46</span>
													</p>
											</li>
											<li id="li_20">
												<img src="${base}/common/template/lottery/lecai/images/touxiang/8.jpg">
													<p class="color">hh***3&nbsp;&nbsp;1分彩
														<br >喜中
		              					<span>¥33.84</span>
													</p>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div di="lotteryCardBox" class="cardBox hide">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- 下一期提示弹窗 --%>
	<div class="nextAlert" style="display:none;">
		<div class="nextAlertBg"></div>
		<div class="section">
			<div class="content">
				<h3>温馨提示</h3>
				<span class="layermend closeNextAlert"></span>
				<p style="	margin-top: 30px;">
					<span class="upQiHao">0105862</span>期已截止
				</p>
				<p>
					当前期号<span class="nextQiHao" style="color:red;">0105862</span>
				</p>
				<p style="padding-bottom:10px;">投注时请注意期号</p>
				<div class="">
					<div class="button closeNextAlert">
						确定
					</div>
				</div>
			</div>
		</div>
	<%-- 下一期提示弹窗 --%>
	</div>
	<%-- 投注提示弹框 --%>
	<div id="layermbox16" class="layermbox layermbox0 layermshow" index="16" style="display:none;">
		<div class="laymshade" id="shade"></div>
		<div class="layermmain">
			<div class="section">
				<div class="layermchild layerBet layermanim">
					<h3 style="">投注确认</h3>
					<button class="layermend">
					</button>
					<div class="layermcont" id="layermcont">
					</div>
					<div class="layermbtn">
						<span type="0" id="quxiao">取消</span>
						<span id="submitBtn" type="1">确认投注</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- 投注提示弹框 --%>
	<!-- 游戏说明 -->
	<div class="game_shows" style="display:none;">
	  <div class="bac_color"></div>
	  <div class="page_layout">
	    <div class="section_">
	      <div class="window_ explain instructions">
					<h3 style="">游戏说明</h3>
					<button class="layermend" id="tuichu"></button>
					<div class="text_shows">

					</div>
					<div class="layermbtn"><span type="1">确定</span></div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- 游戏说明 -->
	<%-- 投注成功弹窗 --%>
	<div id="layermbox17" class="layermbox layermbox0 layermshow" style="display:none;">
		<div class="layermmain">
			<div class="section">
				<div class="layermchild layerBet layermanim" style="min-width: 160px;min-height: 50px;border: 14px solid #e4e4e4;">
					<div class="touzuTC">
						<img src="${base}/common/template/lottery/lecai/images/2222.png" alt="">
							投注成功
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- 投注成功弹窗 --%>
	<script type="text/javascript">
	var cart = [];
	var lotList=[];
	var Latest_Lottery,Latest_openLottery;  // 开奖结果和投注记录;
	// 默认执行
	htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/pcegg/xingyun28.html",async:false});
	$(".mode .content").html(htmlobj.responseText);
		// 倍数减一
	$('.sub').click(function(){
		let money = $('.money').val();
		if(!money){
			$('.money').val(1)
		}else if (money-0 > 1) {
			$('.money').val((money-0)-1)
			$('.jine').text( (($('.sscCheckNumber .curr').length)*($('.money').val()-0)*2).toFixed(2) )
		}
	})
	// 当前倍数
	$('.money').on('input propertychange', function() {
		$('.jine').html($('.money').val()*2);
  });
	// 倍数加一
	$('.add').click(function(){
		let money = $('.money').val();
		if(!money){
			$('.money').val(1)
		}else {
			$('.money').val((money-0)+1)
		}
		$('.jine').text((($('.sscCheckNumber .curr').length)*($('.money').val()-0)*2).toFixed(2))
	})
	// 点击显示/隐藏 class
	function wdtz(){
		$('#tz_delclass').toggleClass('notSelect',false)
		$('#zh_delclass').toggleClass('notSelect',true)
		$('._betChaseAnimation').attr('style','display:none');
		$('._mybetContent').removeAttr("style");
	}
	// 点击显示/隐藏 class
	function wdzh(){
		$('#tz_delclass').toggleClass('notSelect',true)
		$('#zh_delclass').toggleClass('notSelect',false)
		$('._mybetContent').attr('style','display:none');
		$('._betChaseAnimation').removeAttr("style");
	}

	// 无缝滚动
	function noticeUp(obj,top,time) {
			 $(obj).animate({
			 marginTop: top
			 }, time, function () {
			 $(this).css({marginTop:"0"}).find(":first").appendTo(this);
			 })
			}
		var MyMar = setInterval("noticeUp('#gundong ul','-51px',1000)", 2000);

	$(function(){
		// 玩法
		let lotCode = location.search.replace('?lotCode=', '');
		// lotGroup 1=时时彩，2=低频彩，3=PK10，5=11选5，6=香港彩，7=快三，8=快乐十分，9=PC蛋蛋
		// duration 封盘时间， getLoctterysLength 彩种分类长度，
		// ActiveQiHao 当前到期号，rules 所有玩法列表, rule当前选中玩法
		var lotGroup,duration,getLoctterysLength,ActiveQiHao,rules,rule,caizhong,qihao;
		var navIndex = 6;
		// 获取当前时间
	  var year,month,day;
	  var date = new Date();
	  year = date.getFullYear();
	  month = date.getMonth() + 1;
	  day = date.getDate();
	  var startTime = year + "-" + month + "-" + day + " 00.00.00";
	  var endTime = year + "-" + month + "-" + day + " 23.59.59";
<%--		<c:if test="${imgUrl == '' || imgUrl == null }">--%>
		$('.betLogo img').attr('src','${imgUrl}')
<%--		</c:if>--%>
<%--		<c:if test="${imgUrl != '' }">--%>
<%--		$('.betLogo img').attr('src','${imgUrl}')--%>
<%--		</c:if>--%>
		//获取彩种玩法到接口
		$.post("/native/getLotGamePlays.do",{lotCode:lotCode},function(result){
			caizhong = result.content.name;
			$('.caizhong').html(caizhong)
			rules = result.content.rules;
			rule = rules[0];
			// 添加玩法列表并且默认选中第一个
			rulesLi = ''
			for(let i in rules){
				if(i==0){
					rulesLi+= '<li class="curr">'+rules[i].name+'</li>'
				}else {
					rulesLi+= '<li>'+rules[i].name+'</li>'
				}
			}
			$('.betFilter').append($(rulesLi));

			// 所有玩法的切换事件

			$('.betFilter li').click(function(){
				$(this).unbind('click');
				let index = $(this).index();
				$(this).addClass('curr').siblings().removeClass('curr');
				rule = rules[index];
				ruleLi = ''

        $('.betFilterAnd').html($(ruleLi));
				// 默认删除
				$(".mode .content").html('');
				// 清空投注页
				// $('.sscCheckNumber ul li').html("");
				let curr = $('.betFilter .curr').text();
				// 判断跳转路由
				switch (curr) {
					case '幸运28':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/pcegg/xingyun28.html",async:false});
						break;
					default:
				}
				$(".mode .content").html(htmlobj.responseText);
				$('.detail').text('0');
				$('.jine').text('0');
				$('.checkedList table tbody').html('');

				// $('.betFilterAnd li').click(function(){
				// 	let index = $(this).index();
				// 	$(this).addClass('curr').siblings().removeClass('curr'); // 删除选中的class
				// 	$(".mode .content").html('');
				// 	// 清空投注页
				// 	$('.sscCheckNumber ul li').html("");
				// 	// 判断跳转路由
				// 	switch (curr) {
				// 		case '幸运28':
				// 			htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/pcegg/xingyun28.html",async:false});
				// 			break;
				// 		default:
				// 	}
				// 	$(".mode .content").html(htmlobj.responseText);
				// 	$('.detail').text('0');
				// 	$('.jine').text('0');
				// 	$('.checkedList table tbody').html('');
				// });
			});
			// let curr = $('.betFilterAnd .curr').text();
			// $('.betFilterAnd li').click(function(){
			// 	// 清空投注页
			// 	$('.sscCheckNumber ul li').html("");
			// 	let index = $(this).index();
			// 	$(this).addClass('curr').siblings().removeClass('curr'); // 删除选中的class
			//
			// });
		});

		// 获取历史开奖
		Latest_Lottery = function(first) {
			$.post("/native/open_result_detail.do",{lotCode:lotCode},function(result){
				if (result.success == false) {
					return false
				}
				var kjlist = result.content.slice(0,10);
				var kjlist_0 = result.content.slice(1,11);
				// 定时判断开奖结果，若为 ？ 则继续获取
				var a = $(".qishu").text()-0;
				// console.log(a);
				if (kjlist[0].haoMaList[0] == "?" || a - result.content[0].qiHao > 1) {
					setTimeout(function() {
						Latest_Lottery();
					},5000)
				}else {
					$('.kjdh').hide();
					$('.kjdh').siblings().show();
				}
				var lotterySesult = ''
				if(result.content[0].haoMaList[0] != '?'){
					for(var i = 0; i < result.content[0].haoMaList.length; i++){
					    if(result.content[0].haoMaList[i] == '+' ||result.content[0].haoMaList[i] == '='){
                            lotterySesult += '<li>'+result.content[0].haoMaList[i]+'</li>'
                        } else {
                            if(result.content[0].haoMaList[i] == 0 || result.content[0].haoMaList[i] == 13 || result.content[0].haoMaList[i] == 14 || result.content[0].haoMaList[i] == 27){
                                lotterySesult +='<li class="yaohao" style="color:#aaa">'+result.content[0].haoMaList[i]+'</li>'
                            } else if(result.content[0].haoMaList[i] == 1 || result.content[0].haoMaList[i] == 4 || result.content[0].haoMaList[i] == 7 || result.content[0].haoMaList[i] == 10 || result.content[0].haoMaList[i] == 16 || result.content[0].haoMaList[i] == 19 || result.content[0].haoMaList[i] == 22 || result.content[0].haoMaList[i] == 25){
                                lotterySesult +='<li class="yaohao" style="color:#5cb85c">'+result.content[0].haoMaList[i]+'</li>'
                            } else if(result.content[0].haoMaList[i] == 2 || result.content[0].haoMaList[i] == 5 || result.content[0].haoMaList[i] == 8 || result.content[0].haoMaList[i] == 11|| result.content[0].haoMaList[i] == 17 || result.content[0].haoMaList[i] == 20 || result.content[0].haoMaList[i] == 23 || result.content[0].haoMaList[i] == 26){
                                lotterySesult +='<li class="yaohao" style="color:#2a74dd">'+result.content[0].haoMaList[i]+'</li>'
                            } else if(result.content[0].haoMaList[i] == 3 || result.content[0].haoMaList[i] == 6 || result.content[0].haoMaList[i] == 9 || result.content[0].haoMaList[i] == 12 || result.content[0].haoMaList[i] == 15 || result.content[0].haoMaList[i] == 18 || result.content[0].haoMaList[i] == 21 || result.content[0].haoMaList[i] == 24){
                                lotterySesult +='<li class="yaohao" style="color:#d9534f">'+result.content[0].haoMaList[i]+'</li>'
                            } else {
                                lotterySesult +='<li class="yaohao" style>'+result.content[0].haoMaList[i]+'</li>'
							}
						}
					}
					$(".kjdh_1").html(lotterySesult)
				}
				// 开奖号码
				for(let i in kjlist[0].haoMaList || i in kjlist_0[0].haoMaList){
					if (kjlist[0].haoMaList[0] == "?") {
						$('.yaohao').eq(i).html(kjlist[1].haoMaList[i]);
						$('.yaohao_h').html(kjlist[1].haoMaList[0]+kjlist[1].haoMaList[1]+kjlist[1].haoMaList[2]);
						$('.ResultsList tbody').html('');
						$('.kjdh').show();
						$('.kjdh_1').hide();
						for(let i in kjlist){
							$('.ResultsList tbody').append($('<tr>'+
																	'<th scope="col">'+kjlist_0[i].qiHao+'</th>'+
																	'<th scope="col" class="kjhm">'+kjlist_0[i].haoMaList.join(',')+'</th>'+
																	'<th scope="col">'+kjlist_0[i].time+'</th>'+
																	'</tr>'))
						}
					}else if (kjlist[0].haoMaList[0] != "?") {
						// $('.yaohao').eq(i).html(kjlist[0].haoMaList[i]);
						// $('.yaohao_h').html((kjlist[0].haoMaList[0]-0)+(kjlist[0].haoMaList[1]-0)+(kjlist[0].haoMaList[2]-0));
						$('.ResultsList tbody').html('');
						$('.kjdh').hide();
						$('.kjdh_1').show();
						for(let i in kjlist){
							$('.ResultsList tbody').append($('<tr>'+
																	'<th scope="col">'+kjlist[i].qiHao+'</th>'+
																	'<th scope="col" class="kjhm">'+kjlist[i].haoMaList.join(',')+'</th>'+
																	'<th scope="col">'+kjlist[i].time+'</th>'+
																	'</tr>'))
						}
					}
				}
			});
		}
		// 获取倒计时函数
		var getCountDown = function(next){
			$.post("/native/getCountDown.do",{lotCode:lotCode,version: 5},function(result){
				if (result.success == false) {
					alert('尚未开盘')
					return false;
				};
				qihao = result.content.qiHao;
				$('.qishu').html(qihao)
				var activeTime = result.content.activeTime;
				var serverTime = result.content.serverTime;
				if(next){
					$('.upQiHao').html(ActiveQiHao);
					$('.nextQiHao').html(result.content.qiHao);
					$('.nextAlert').show();
					$('.kjdh').hide();
					$('.kjdh').siblings().show();
					Latest_Lottery();
					// setTimeout(function(){
					// 	Latest_Lottery();
					// },duration*1000)
				}
				ActiveQiHao = result.content.qiHao;

				// 获取倒计时的时间戳
				var times = activeTime - serverTime;
				// 获取时间戳转换成时分秒
				var timeItv = setInterval(function(){
					// 获取下一期的投注截止时间
					if (times<1000 && times > -1000){
						clearInterval(timeItv);
						getCountDown(true);
					};
					times -= 1000;
					getfpTime(times);
					$('.kaijiang').html($('.qishu').text()-1);
				},1000)
			});
		}

		// 倒计时转换时分秒格式函数
		var getfpTime = function (times) { //参数1，封盘时间，2，服务器时间
        let hour = parseInt(times / 1000 / 60 / 60) < 10 ? '0' + parseInt(times / 1000 / 60 / 60) : parseInt(times / 1000 / 60 / 60); //小时
        let Minute = parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60) < 10 ? '0' + parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60) : parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60);
        let second = parseInt(times / 1000 % 60);
        second < 10 ?  second = '0' + second : '';
        var fpTime = hour + ':' + Minute + ':' + second; //封盘时间
				let countdown = parseInt(times / 1000 % 60);
	      // console.log(countdown);
	      if (countdown < 0) {
	        $('.djs').text("封盘中...");
	      }else {
	        $('.djs').html(fpTime);
	      }
      }

		//进入页面获取一次历史开奖
		Latest_Lottery(true);
		// 彩种列表导航
		$.post("/native/getLoctterys.do",{lotGroup:lotGroup,version: 5},function(result){
			// 彩种分类
			switch (lotCode) {
					case 'FFC':
					case 'EFC':
					case 'CQSSC':
					case 'TJSSC':
					case 'WFC':
					case "HNFFC":
					case "HNWFC":
					case "AZXY5":
					case 'XJSSC':
						lotGroup = 1;
						break;
					case 'FC3D':
					case 'PL3':
						lotGroup = 2;
						break;
					case 'BJSC':
					case 'XYFT':
					case 'LXYFT':
					case 'AZXY10':
					case 'FFSC':
					case 'SFSC':
					case 'WFSC':
					case 'WFFT':
					case 'LBJSC':
					case 'SFFT':
						lotGroup = 3;
						break;
					case 'GD11X5':
					case 'SD11X5':
					case 'JX11X5':
					case 'SH11X5':
					case 'GX11X5':
						lotGroup = 5;
						break;
					case 'LHC':
					case 'SFLHC':
					case 'TMLHC':
					case 'WFLHC':
					case 'HKMHLHC':
						lotGroup = 6;
						break;
					case 'JSSB3':
					case 'HBK3':
					case 'AHK3':
					case 'SHHK3':
					case 'HEBK3':
					case 'GXK3':
					case 'GZK3':
					case 'BJK3':
					case 'JXK3':
					case 'GSK3':
					case 'FFK3':
					case 'WFK3':
					case 'JPK3':
					case 'KRK3':
					case 'HKK3':
					case 'AMK3':
						lotGroup = 7;
						break;
					case 'CQXYNC':
					case 'HNKLSF':
					case 'GDKLSF':
						lotGroup = 8;
						break;
					case 'PCEGG':
					case 'JND28':
						lotGroup = 9;
						break;
					default:
				}

			getCountDown(false);
			getLoctterysLength = result.content.length;
			for(let i in result.content){
				lotList.push(result.content[i])
				if (result.content[i].code == lotCode) {
					duration = result.content[i].duration;
				}
			}
			for(let i in lotList){
				if (lotList[i].lotGroup == lotGroup && lotList[i].code == lotCode) {
					$('.betLeft .betNav').append($('<li class="active" lotCode="'+lotList[i].code+'">'+lotList[i].name+'</li>'))
				}else if (lotList[i].lotGroup == lotGroup && lotList[i].code != lotCode) {
					$('.betLeft .betNav').append($('<li lotCode="'+lotList[i].code+'">'+lotList[i].name+'</li>'))
				}
			};
			//彩种切换
			$('.betNav li').click(function(){
				location.href = location.protocol+'//'+location.host+location.pathname+'?lotCode='+$(this).attr('lotCode');
			})
		});
		// 投注记录
	  Latest_openLottery = function(first) {
	    $.post("/native/get_lottery_order.do",{startTime:startTime,endTime:endTime,code:'',status:' ',page: 1,rows: 20,lhc: 1},function(result){
	      var tzlist = result.page.list.slice(0,5);
	      $('._mybetContent .tz_list').html('');
	      for (var i in tzlist) {
	        var zhMoney = 0;
	        switch (tzlist[i].status) {
	          case 2:
	            zhMoney = tzlist[i].buyMoney * tzlist[i].odds;
	            break;
	          default:

	        }
	        $('._mybetContent .tz_list').append($('<tr>'+
	                            '<td>'+tzlist[i].qiHao+'</td>'+
	                            '<td>'+tzlist[i].buyMoney+'</td>'+
	                            '<td>'+zhMoney+'</td>'+
	                            '<td><a href="/center/df/record/betrecord/cpDfPage.do?form=order" class="font_gd">详情</a></td>'+
	                            '</tr>'))
	      }
				// 如果注单数量不够5个，则用空白格子填充
				if(tzlist.length < 5){
					for(var i=0;i< (5-tzlist.length);i++){
						$('._mybetContent .tz_list').append($('<tr>'+
																'<td></td>'+
																'<td></td>'+
																'<td></td>'+
																'<td></td>'+
																'</tr>'))
					}
				}
	    })
	  }
	  Latest_openLottery(true);

		// 彩种列表导航动画
		$('.betLeft .left').click(function(){
			if(getLoctterysLength<=6 || navIndex === 6){
				alert('已经到顶啦')
				return false;
			}
			navIndex -= 1
				$('.betLeft .betNav').animate({left:-((navIndex-6)*112)+'px'},"fast");
		})
		$('.betLeft .right').click(function(){
			if(getLoctterysLength<=6 || getLoctterysLength === navIndex){
				alert('已经到顶啦')
				return false;
			}
			navIndex += 1
			$('.betLeft .betNav').animate({left:-((navIndex-6)*112)+'px'},"fast");
		})

		//下一期提示弹窗关闭事件
		$('.closeNextAlert').click(function(){
			$('.nextAlert').hide();
		});

		$('#betbtn').click(function(){
			$('#layermbox16').removeAttr("style");
		});
		$('.layermend').click(function(){
			$('#layermbox16').attr('style','display:none')
		});
		$('#quxiao').click(function(){
			$('#layermbox16').attr('style','display:none');
			$('.textarea').html('');
			$('.checkedList table tbody').html('');
			$('.zdsl').text('0');
			$('.tzje').text('0');
			cart = [];
		});
		$('#submitBtn').click(function(){
			$('#layermbox16').attr('style','display:none')
		});
	});
// 游戏说明隐藏
 $('#tuichu').click(function(){
	 $('.game_shows').css('display','none')
 })
 $('.layermbtn').click(function(){
 	$('.game_shows').css('display','none')
 })
 // 对象深拷贝函数
 function deepCopy(object){
 	 let obj = new Object();
 	 for(let k in object){
 		 obj[k] = object[k]
 	 }
 	 obj.isActive = false;
 	 return obj
  }
</script>
<style media="screen">
</style>
<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>
