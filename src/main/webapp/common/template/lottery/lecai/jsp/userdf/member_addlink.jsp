<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/common/js/layer/layer.js"></script>
<style type="text/css">
	table{
	    font-size: 15px;
	    width:100%;
	    border-collapse: inherit;
    	border-spacing: 0;
	}
	table tr td{text-align: center;line-height: 35px;}
	table input{
		width:260px;
		height:30px;
		padding-left:5px;
	};

</style>
<div style="overflow-x: hidden;overflow-y: hidden;padding:20px;font-size:13px;text-align: center;">
<table class="table table-bordered table-striped" style="clear: both">
		<tbody>
		<input id="zjId"  type="hidden" value="${link.id}"/>
		<input id="linkCode"  type="hidden" value="${link.linkKey}"/>
		<input id="linkType"  type="hidden" value="${link.type}"/>
		<input id="linkNum"  type="hidden" value="${link.linkUrl}"/>
			<tr>
				<td width="30%" class="text-right">推广用户类型：</td>
				<td width="55%" class="text-left">
				<select class="form-control" id="tgType" onmousedown="${tgType}" value="${link.type}" style="width: 260px;height:30px;padding-left:5px;">
					<option value="1" selected >代理</option>
					<c:if test="${trunOnoff ne 'on' }">
					<option value="2" >会员</option>
					</c:if>
				</select>
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">代理机制：</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" disabled="disabled" id="dljz" value="${dljz}"/>
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">生成策略：</td>
				<td width="55%" class="text-left" id="dailiUrl" style="text-align: left;line-height: 15px;padding-left: 40px;">
					<input type="radio" value="1" name="adddailiUrl" class="scclclicktype" style="width:13px;" checked="checked" />
					<span style="position: relative;top: -10px;">随机</span>
					<input type="radio" value="2" name="adddailiUrl" class="scclclicktype" style="width:13px;"/>
					<span style="position: relative;top: -10px;">自填</span>
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">生成链接：</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" disabled="disabled" id="urlLink" value="${link.linkUrl}"/>
				</td>
			</tr>
			
			<tr id="cpfds" >
				<td width="30%" class="text-right">为下级设置返点(<span id="minRoll" style="color:#4aa9db">0</span>-<span id="getCpFds"style="color:#4aa9db"></span>):</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" id="rollPoint" placeholder="请输入返点数!" value="${link.cpRolling}"/>
				<input id="minFd" type="hidden" value="0">
				<input id="maxFd" type="hidden" value="">
				</td>
			</tr>
			<tr id="cpZcs" >
				<td width="30%" class="text-right">占成数(<span id="minZhan" style="color:#4aa9db">0</span>-<span id="getCpZcs"style="color:#4aa9db"></span>):</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" id="zhanPoint" placeholder="请输入占成数!" value="${link.cpRolling}"/>
				<input id="minZc" type="hidden" value="0">
				<input id="maxZc" type="hidden" value="">
				</td>
			</tr>
			<tr id="cpPls" >
				<td width="30%" class="text-right">动态赔率点数(<span id="minPeilv" style="color:#4aa9db">0</span>-<span id="getCpPls"style="color:#4aa9db"></span>):</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" id="peilvPoint" placeholder="请输入动态赔率点!" value="${link.cpRolling}"/>
				<input id="minPl" type="hidden" value="0">
				<input id="maxPl" type="hidden" value="">
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">状态：</td>
				<td width="55%" class="text-left">
				<select id="status" class="form-control" value="${link.status}"  style="width: 260px;height:30px;padding-left:5px;">
						<option value="1">禁用</option>
						<option value="2" selected="selected">启用</option>
				</select>
				</td> 
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		var param = {
				scclclicktype:1,
		};
		var typeNum;
		var linkNum = $('#linkNum').val();
		$('.scclclicktype').click(function(){
			typeNum = $(this).val();
			console.log(typeNum);
			if(typeNum == '1'){
				param['generalizeType'] = 1;
				$('#urlLink').val(linkNum);
				$('#urlLink').attr("value",linkNum);
				$('#urlLink').attr('disabled','disabled');
			}else{
				param['generalizeType'] = 2;
				$('#urlLink').removeAttr('disabled');
	            $('#urlLink').attr("value","");
	            $('#urlLink').val("");
	            $('#urlLink').attr('placeholder','输入5位数字');
			};
		})
		var profitShare = '';
		var dynamicRate = '';
		var multiAgent = '';
		var mergeRebate ='';
		function loading(){
			$.ajax({
				url : "${base}/daili/generalizeLink/multidata.do",
				success : function(j) {
					var rebateMaxNum = j.rebateMaxNum;//最大返点
					var profitShareMax = j.profitShareMax;//最大占成
					var dynamicRateMax = j.dynamicRateMax;//最大赔率
					dynamicRate = j.dynamicRate;//动态赔率开关
					profitShare = j.profitShare;//占成数开关
					multiAgent = j.multiAgent;
					mergeRebate = j.mergeRebate;
					/* 返点 */
					$('#maxFd').val(rebateMaxNum);
					$('#minRoll').html(j.rebateMinNum);
					$('#getCpFds').html(rebateMaxNum);
					/* 占成 */
					$('#maxZc').val(profitShareMax);
					$('#minZhan').html(j.profitShareMin);
					$('#getCpZcs').html(profitShareMax);
					/* 赔率 */
					$('#maxPl').val(dynamicRateMax);
					$('#minPeilv').html(j.dynamicRateMin);
					$('#getCpPls').html(dynamicRateMax);
					if(multiAgent == 'on' && mergeRebate != 'on') {
						$('#cpfds').show();
					}else{//返点数开关
						$('#cpfds').hide();
					}
					if(!profitShare || profitShare == 'off'){//占成数开关
						$('#cpZcs').hide();
					}else if(profitShare == 'on'){
						$('#cpZcs').show();
					}
					if(!dynamicRate || dynamicRate == 'off'){//动态赔率开关
						$('#cpPls').hide();
					}else if(dynamicRate == 'on'){
						$('#cpPls').show();
					}
				}
			});
			$('#cpfds').show();
			/* if('${flag}'=='1'){
				$('#cpfds').show();
			}
			$("#tgType").change(function() {
				var curVal = $(this).find("option:checked").attr("value");
				$("#linkType").val(curVal);
				if (curVal == 1 || '${memberRate}' == 'on') {//代理
					$('#cpfds').show();
				} else {
					$('#cpfds').hide();
				}
			}); */
		}
	    function callbackdata() {
	    	var zjId = $('#zjId').val();
			var type = $("#tgType").val();
			var linkCode = $('#linkCode').val();
			var rollPoint = $('#rollPoint').val();
			var zhanPoint = $('#zhanPoint').val();//占成
			var peilvPoint = $('#peilvPoint').val();//赔率
			var status = $('#status').val();
			// 			console.log(zjId+"zjId  "+"type:"+type+"   linkCode:"+linkCode+"   rollPoint:"+rollPoint+"   status:"+status);
			if ("${isMulti}" == "true") {
				if (type == 1 || '${memberRate}'== 'on') {
					if (multiAgent=='on' && mergeRebate != 'on' && (rollPoint == null || rollPoint == "")) {
						layer.alert("彩票返点数为必填项!");
						return false;
					}
					var minFd = $('#minFd').val();
					var maxFd = $('#maxFd').val();
					if (rollPoint * 1 < minFd * 1) {
						layer.alert("彩票返点数不能小于" + minFd + "!");
						return false;
					}
					if (rollPoint * 1 > maxFd * 1) {
						layer.alert("彩票返点数不能大于" + maxFd + "!");
						return false;
					}
				}
			}
			if (profitShare == 'on') {//占成验证
				if (zhanPoint == null || zhanPoint == "") {
					layer.alert("占成数为必填项!");
					return false;
				}
				var minZc = $('#minZc').val();
				var maxZc = $('#maxZc').val();
				if (zhanPoint * 1 < minZc * 1) {
					layer.alert("占成数不能小于" + minZc + "!");
					return false;
				}
				if (zhanPoint * 1 > maxZc * 1) {
					layer.alert("占成数不能大于" + maxZc + "!");
					return false;
				}
			}
			if (dynamicRate == 'on') {//赔率验证
				if (peilvPoint == null || peilvPoint == "") {
					layer.alert("动态赔率点数为必填项!");
					return false;
				}
				var minPl = $('#minPl').val();
				var maxPl = $('#maxPl').val();
				if (peilvPoint * 1 < minPl * 1) {
					layer.alert("动态赔率点数不能小于" + minPl + "!");
					return false;
				}
				if (peilvPoint * 1 > maxPl * 1) {
					layer.alert("动态赔率点数不能大于" + maxPl + "!");
					return false;
				}
			}
			param["id"] = zjId;
			param["type"] = type;
			param["linkKey"] = linkCode;
			param["cpRolling"] = rollPoint;
			param["profitShare"] = zhanPoint;
			param["dynamicRate"] = peilvPoint;
			param["status"] = status;
			return param;
	    } 
</script>
</div>
