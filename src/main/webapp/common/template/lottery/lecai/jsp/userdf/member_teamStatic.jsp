<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">代理报表</div>
            <div id="Proxy_IndexPage" class="userMain">
                <div class="u_codeedit">
                    <div id="timeType" class="newTab"><a class="curr" value="0">今天</a><a class="" value="1">昨天</a><a class="" value="this">本月</a><a class="" value="last">上月</a></div>
                    <ul class="todayView mgb10" style="text-align: left;">
                    <li><span></span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   <option value="1">直属下级</option>
									<option value="2">所有下级</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    	<input type="text" placeholder="代理及下级查询" class="userInput" id="agent">&nbsp;
                    	<input type="text" placeholder="单用户查询" class="userInput" id="account">&nbsp;
                        <a class="submitBtn ClickShade" onclick="initRdsData(1)" >查询</a></ul>
                    <div id="code_cont" class="code_cont" style="">
                       <!--  <ul class="plMore">
                            <li><em>¥2264</em><span>投注金额</span></li>
                            <li><em>¥4675.19</em><span>中奖金额</span></li>
                            <li><em>¥0</em><span>活动礼金</span></li>
                            <li><em>¥75.17</em><span>团队返点</span></li>
                            <li><em>¥2486.36</em><span>团队盈利</span></li>
                        </ul>
                        <ul class="plMore">
                            <li><em>3人</em><span>投注人数</span></li>
                            <li><em>0人</em><span>首充人数</span></li>
                            <li><em>0人</em><span>注册人数</span></li>
                            <li><em>6人</em><span>下级人数</span></li>
                            <li><em>¥3502446.76</em><span>团队余额</span></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
		        $("#timeType a").click(function () {
		            $(this).addClass('curr').siblings('a').removeClass('curr')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		        	initRdsData(1);
		        })
			})
			function initRdsData(p){
				$(".notContent").hide()
		    	$(".loadingGif").show()
				if(!endTime){
					endTime = startTime
				}
				var data = {
					account: $("#account").val(),
					agent :$("#agent").val(),
					searchType: $("#lotCode").val(),
					begin:startTime+'  00:00:00',
					end:endTime+'  23:59:59',
					pageSize:10,
					pageNumber:p
				}
				$.ajax({
					url:"${base}/daili/dlreport/list.do",
					data:data,
					success:function(res){
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.total == 0){
							//无数据
						}else{
							addTemplate(res)
						} 
					}
				})
			}
			/* <li><em>¥'+moneyFormatter(res.giveRegisterTotal)+'</em><span>注册赠送总计</span></li> */
			function addTemplate(res){
				var LotteryTotal = exNumber(res.sysLotteryTotal) + exNumber(res.lotteryTotal) + exNumber(res.sfMarkSixTotal)+ exNumber(res.markSixTotal) //投注
				var lotteryAward = exNumber(res.lotteryAward) + exNumber(res.sysLotteryAward) + exNumber(res.sfMarkSixAward) + exNumber(res.markSixAward)  //派奖
				/* var allBunko = LotteryBunko - lotteryAward - exNumber(res.rebateAgentTotal) - exNumber(res.rebateTotal) */
				var deposit = exNumber(res.depositTotal) + exNumber(res.manualDepositTotal)
				var html = '<ul class="plMore"> <li><em>¥'+LotteryTotal.toFixed(2)+'</em><span>投注金额</span></li><li><em>¥'+lotteryAward.toFixed(2)+'</em><span>中奖金额</span></li><li><em>¥'+moneyFormatter(res.depositTotal)+'</em><span>存款总计</span></li><li><em>¥'+moneyFormatter(res.withdrawTotal)+'</em><span>提款总计</span></li><li><em>¥'+moneyFormatter(res.rebateAgentTotal)+'</em><span>返点总计</span></li></ul>';
              	html+='<ul class="plMore"><li><em>¥'+moneyFormatter(res.manualDepositTotal)+'</em><span>手动加款	</span></li><li><em>¥'+moneyFormatter(res.manualWithdrawTotal)+'</em><span>手动扣款</span></li><li><em>¥'+moneyFormatter(res.sumMoney)+'</em><span>团队余额</span></li><li><em>'+Math.ceil(moneyFormatter(res.betCountTotal))+'人</em><span>总投注人数</span></li></ul>'
				html+=' <ul class="plMore"><li><em>'+exNumber(res.firstDepositTotal)+'人</em><span>首冲人数</span></li><li><em>¥'+moneyFormatter(res.rebateTotal)+'</em><span>反水总计</span></li><li><em>¥'+moneyFormatter(res.giveTotal)+'</em><span>充值赠送总计</span></li><li><em>¥'+moneyFormatter(res.allBunko)+'</em><span>全部输赢</span></li></ul>'
              	$("#code_cont").html(html)
			}
			function moneyFormatter(value) {
				if (value === undefined) {
					return 0;
				}
				if (value > 0) {
					return toDecimal2(value);
				}
				return toDecimal2(value);
			}
			function exNumber(value) {
				if (value == undefined) {
					return 0;
				}else{
					return parseFloat(value)
				}
			}
		</script>
		<script id="recordtype_tpl" type="text/html">
		{{each data as option}}
        	<option value="{{option.type}}">{{option.name}}</option>
		{{/each}}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>