<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="lecai_v5">
	<c:if test="${param.code == 'CQSSC'}">
	<div class="CQSSCOPEN">
		<!-- <strong style="color: #c9171e">重庆时时彩游戏是依照中国重庆福利彩票发行中心统一发行的『时时彩』彩票的开奖数据为依据所规划的线上彩票游戏。</strong> -->
		<br> <strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br>
		<strong>重庆时时彩开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" rowspan="3" valign="middle" bgcolor="#FFF7F0"
						align="center">重庆时时彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						0:30-3:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-09</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
				<tr>
					<td class="point" bgcolor="#FFF7F0" align="center">
						7:30-23:50(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">10-59</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
				<!-- <tr>
					<td class="point" bgcolor="#FFF7F0" align="center">
						00:05-01:55(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-023</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr> -->
			</tbody>
		</table>
		<br> <strong>本公司重庆时时彩具体游戏规则如下︰</strong> <br>
	</div>
	</c:if>
	<c:if test="${param.code == 'TJSSC'}">
	<div class="TJSSCOPEN">
		<strong style="color: #c9171e;">天津时时彩由中国福利彩票发行管理中心组织，由天津市福利彩票发行中心在所辖区域内承销。</strong>

		<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong>天津时时彩开奖时间：</strong>

		<table class="awardList">

			<tbody>
				<tr>

					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>

					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>

					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>

					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>

				</tr>

				<tr>

					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">天津时时彩</td>

					<td class="point" bgcolor="#FFF7F0" align="center">

						09:20-23:00(北京时间)</td>

					<td class="point" bgcolor="#FFF7F0" align="center">01-42</td>

					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>

				</tr>

			</tbody>
		</table>

		<br> <strong> 本公司天津时时彩具体游戏规则如下： </strong> <br>
	</div>
	</c:if>
	<c:if test="${param.code == 'XJSSC'}">
	<div class="XJSSCOPEN">
		<strong style="color: #c9171e;">新疆时时彩由中国福利彩票发行管理中心组织，由新疆自治区福利彩票发行中心在所辖区域内承销。</strong><br>
		<strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br> <strong>新疆时时彩开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">新疆时时彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						10:20-2:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-48</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong>本公司新疆时时彩具体游戏规则如下：</strong><br>
	</div>
	</c:if>
	<c:if test="${param.code == 'FFC'}">
	<div class="FFCOPEN">
		<!--  <strong style="color:#c9171e">分分彩是自主研发的趣味更多，频次更快的时时彩游戏</strong>  -->
		<br> <strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br>
		<strong>分分彩开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">分分彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">全天24小时不间断</td>
					<td class="point" bgcolor="#FFF7F0" align="center">0001-1440</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong>本公司分分彩具体游戏规则如下︰</strong> <br>
	</div>
	</c:if>
	<c:if test="${param.code == 'EFC'}">
	<div class="EFCOPEN">
		<!--  <strong style="color:#c9171e">二分彩是自主研发的趣味更多，频次更快的时时彩游戏</strong>  -->
		<br> <strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br>
		<strong>二分彩开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">二分彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">全天24小时不间断</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-720</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每2分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong>本公司二分彩具体游戏规则如下︰</strong> <br>
	</div>
	</c:if>
	<c:if test="${param.code == 'WFC'}">
	<div class="WFCOPEN">
		<!--  <strong style="color:#c9171e">五分彩是自主研发的趣味更多，频次更快的时时彩游戏</strong>  -->
		<br> <strong>开奖结果为五码 (万、仟、佰、拾、个)。假设结果为1 、2 、3 、4、5。</strong> <br>
		<strong>五分彩开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">五分彩</td>
					<td class="point" bgcolor="#FFF7F0" align="center">全天24小时不间断</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong>本公司五分彩具体游戏规则如下︰</strong> <br>
	</div>
	</c:if>
	<h2>选号玩法</h2>
	<dl>
		<dt>◎一字</dt>
		<dd>
			<ul>
				<li>全五：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。如超过1颗球落在所选号码内，派彩将倍增<br>
					※举例：下注一字【5号】＄100，一字賠率2.05，（第二注开始需要扣除本金）<br> 五颗球开出1，2，3，4，5
					派彩为＄205<br> 五顆球开出1，2，3，5，5 派彩为＄205+105=310<br>
					五顆球开出1，2，5，5，5 派彩为＄205+105+105=415
				</li>
				<li>前三：0~9任选1个号进行投注，当开奖结果[万位、仟位、佰位]任一数与所选的号码相同时，即为中奖。</li>
				<li>中三：0~9任选1个号进行投注，当开奖结果[仟位、佰位、拾位]任一数与所选的号码相同时，即为中奖。</li>
				<li>后三：0~9任选1个号进行投注，当开奖结果[佰位、拾位、个位]任一数与所选的号码相同时，即为中奖。</li>
			</ul>
		</dd>
		<dt>◎二字</dt>
		<dd>
			<ul>
				<li>于前三、中三、后三0~9任选2个号进行投注，当开奖结果任二数与所选的号码相同时，即为中奖。<br>
					※举例：投注者购买二字后三，选择号码如为11，当期开奖结果如为xx11x、xx1x1、xxx11、皆视为中奖。（x=0~9任一数）<br>
					※举例：投注者购买二字后三，选择号码如为12，当期开奖结果如为xx12x、xx1x2、xx21x、xx2x1、xxx12、xxx21皆视为中奖。（x=0~9任一数）
				</li>
				<li>【附注】：以上二例赔率不同</li>
			</ul>
		</dd>
		<dt>◎三字</dt>
		<dd>
			<ul>
				<li>于前三、中三、后三0~9任选3个号进行投注，当开奖结果与所选的号码相同时（顺序不限），即为中奖。<br>
					若是开出豹子如000、111、222、333、444、555、666、777、888、999算不中奖。
				</li>
			</ul>
		</dd>
		<dt>◎二字定位</dt>
		<dd>
			<ul>
				<li>于万仟佰拾个任选二位，自0~9任选2个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
			</ul>
		</dd>

		<dt>◎三字定位</dt>
		<dd>
			<ul>
				<li>于前三、中三、后三任选三位，自0~9任选3个号进行投注，当开奖结果与所选号码相同且顺序一致时，即为中奖。</li>
			</ul>
		</dd>
		<dt>◎组选三</dt>
		<dd>
			<ul>
				<li>
					前三：会员可以挑选5~10个号码，当开奖结果[万位、仟位、佰位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[万位、仟位、佰位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br>
					※例如：112、344，若是开出豹子则不算中奖。<br> ※备注："豹子"为三字同号，例如：111、222
				</li>
				<li>
					中三：会员可以挑选5~10个号码，当开奖结果[仟位、佰位、拾位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[仟位、佰位、拾位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。<br>
					※例如：112、344，若是开出豹子则不算中奖。<br> ※备注："豹子"为三字同号，例如：111、222
				</li>
				<li>
					后三：会员可以挑选5~10个号码，当开奖结果[佰位、拾位、个位]中有且只有两个号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率。如果是选择(1、2、3、4、5)，则只要开奖结果[佰位、拾位、个位]中，有出现1、2、3、4、5中的任何两个号码，且其中有一个号码重复则中奖。
					<br>※例如：112、344，若是开出豹子则不算中奖。<br> ※备注："豹子"为三字同号，例如：111、222
				</li>
			</ul>
		</dd>
		<dt>◎组选六</dt>
		<dd>
			<ul>
				<li>前三：会员可以挑选4~8个号码，当开奖结果[万位、仟位、佰位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br>
					※例如：如果是选择(1、2、3、4)，则开奖结果[万位、仟位、佰位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
				</li>
			</ul>
		</dd>

		<dd>
			<ul>
				<li>中三：会员可以挑选4~8个号码，当开奖结果[仟位、佰位、拾位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。<br>
					※例如：如果是选择(1、2、3、4)，则开奖结果[仟位、佰位、拾位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
				</li>
			</ul>
		</dd>

		<dd>
			<ul>
				<li>
					后三：会员可以挑选4~8个号码，当开奖结果[佰位、拾位、个位]都出现在所下注的号码中且没有任何号码重复，则视为中奖。挑选不同个数的号码有其相对应的赔率，中奖赔率以所选号码中的最小赔率计算派彩。
					<br>※例如：如果是选择(1、2、3、4)，则开奖结果[仟位、佰位、拾位]为123、124、134、234都中奖，其他都是不中奖。例如：112、133、145、444等都是不中奖。
				</li>
			</ul>
		</dd>

		<dt>◎跨度</dt>
		<dd>
			<ul>
				<li>前三：以开奖结果[万位、仟位、佰位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的跨度为5。（最大号码8减最小号码3=5）。
				</li>
			</ul>
		</dd>
		<dd>
			<ul>
				<li>中三：以开奖结果[仟位、佰位、拾位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的跨度为4。（最大号码8减最小号码4=4）。
				</li>
			</ul>
		</dd>
		<dd>
			<ul>
				<li>后三：以开奖结果[佰位、拾位、个位]的最大差距（跨度），作为中奖依据。会员可以选择0~9的任一跨度。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的跨度为2。（最大号码8减最小号码6=2）。
				</li>
			</ul>
		</dd>
	</dl>
	<h2>和数玩法</h2>
	<dl>
		<dt>◎前三和数</dt>
		<dd>
			<ul>
				<li>开奖结果前面三位数的总和值与投注数字相同时，即为中奖。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的和值为15。（3+4+8=15）。
				</li>
			</ul>
		</dd>
		<dt>◎中三和数</dt>
		<dd>
			<ul>
				<li>开奖结果中间三位数的总和值与投注数字相同时，即为中奖。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的和值为19。（4+8+7=19）
				</li>
			</ul>
		</dd>
		<dt>◎后三和数</dt>
		<dd>
			<ul>
				<li>开奖结果后面三位数的总和值与投注数字相同时，即为中奖。<br>
					※举例：开奖结果为3、4、8、7、6。中奖的和值为21。（8+7+6=21）
				</li>
			</ul>
		</dd>
		<dt>◎和尾数</dt>
		<dd>
			<ul>
				<li>于万仟佰拾个任选二位数加起来的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
					尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
					尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
					※举例：开奖结果为3、4、5、0、0。万千和为7，尾数为7，属于“大”、“单”、“质”。<br>
				</li>
			</ul>
		</dd>
		<dt>◎前三和数尾数</dt>
		<dd>
			<ul>
				<li>以开奖号码的前三位数和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
					尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
					尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
					※举例：开奖结果为3、4、5、6、7。前三和为12，尾数为2，属于“小”、“双”、“质”。<br>
				</li>
			</ul>
		</dd>
		<dt>◎中三数和尾数</dt>
		<dd>
			<ul>
				<li>以开奖号码的中间三位数的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
					尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
					尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
					※举例：开奖结果为3、4、5、6、7。中三和为15，尾数为5，属于“大”、“单”、“质”。<br>
				</li>
			</ul>
		</dd>

		<dt>◎后三和数尾数</dt>
		<dd>
			<ul>
				<li>以开奖号码的后三位数的和的尾数，作为中奖依据。会员可以选择大、小、单、双、质、合 的任一号码。<br>
					尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
					尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
					※举例：开奖结果为3、4、5、6、7。后三和为18，尾数为8，属于“大”、“双”、“合”。<br>
				</li>
			</ul>
		</dd>
	</dl>
	<h2>两面玩法</h2>
	<dl>
		<dt>◎整合</dt>
		<dd>
			<ul>
				<li>总和：所有开奖号码相加，作为中奖依据。会员可以选择总和大、总和小、总和单、总和双、龙、虎、和 的任一号码。<br>
					总和大于等于23为“总和大”，小于23为“总和小”<br> 总和为单数时为“总和单”，若为双数时为“总和双”<br>
					总和大于10，且十位数字大于个位数字时为“龙”；总和小于10，或者十位数字小于个位数字时为“虎”；总和大于10，且十位数字等于个位数字时为“和”<br>
					※举例：开奖结果为3、4、5、6、7。总和为25，属于“总和大”、“总和单”、“虎”。
				</li>
			</ul>
		</dd>
		<dt>◎整合单位玩法</dt>
		<dd>
			<ul>
				<li>于万仟佰拾个任选一位中，会员选择0-9、大、小、单、双、质、合 的任一号码。<br>
					尾数大于等于5为“大”，小于5为“小”<br> 尾数为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”<br>
					尾数为1、2、3、5、7时为“质”，若为0、4、6、8、9时为“合”<br>
					※举例：开奖结果为3、4、5、6、7。万位为3，属于“小”、“单”、“质”。
				</li>
			</ul>
		</dd>
		<dt>◎万位、仟位、佰位、拾位、个位玩法</dt>
		<dd>
			<ul>
				<li>从0-9中任选一位号码进行投注，当投注数字与开奖结果相符时，即为中奖。<br>
					※举例：投注者购买佰位3，当期开奖结果如为20352，则视为中奖。
				</li>
				<li>
					开奖结果万位、仟位、佰位、拾位或个位数字为1、3、5、7、9时为“单”，若为0、2、4、6、8时为“双”，当投注位数单双与开奖结果的位数单双相符时，即为中奖。
					<br>※举例：投注者购买佰位单，当期开奖结果如为20130（1为单），则视为中奖。
					<table width="100%">
						<tbody>
							<tr>
								<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">单</th>
								<th width="50%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">双</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">1、
									3、 5、 7、 9</td>
								<td bgcolor="#FFF7F0" class="point" align="center">0、 2、 4、
									6、 8</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li>
					开奖结果万位、仟位、佰位、拾位或个位数字为5、6、7、8、9时为“大”，若为0、1、2、3、4时为“小”，当投注位数大小与开奖结果的位数大小相符时，即为中奖。
					<br>※举例：投注者购买佰位小，当期开奖结果如为20352（3为小），则视为中奖。
					<table width="100%">
						<tbody>
							<tr>
								<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">大</th>
								<th width="50%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">小</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">5、6、7、8、9</td>
								<td bgcolor="#FFF7F0" class="point" align="center">
									0、1、2、3、4</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li>
					开奖结果万位、仟位、佰位、拾位或个位数字为0、4、6、8、9时为“合”，若为1、2、3、5、7时为“质”，当投注位数大小与开奖结果的位数大小相符时，即为中奖。
					<br>※举例：投注者购买佰位质，当期开奖结果如为20352（3为质），则视为中奖。
					<table width="100%">
						<tbody>
							<tr>
								<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">质</th>
								<th width="50%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">合</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">1、2、3、5、7</td>
								<td bgcolor="#FFF7F0" class="point" align="center">
									0、4、6、8、9</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li>总和大、总和小、总和单、总和双
					都是跟相邻的下一位数进行和计算，个位将与万位进行和计算。总和＝9是，为和局，退还投注金额 <br>※举例：投注者购买佰位总和小，当期开奖结果如为20352（佰位3+十位5
					＝8为总和小），则视为中奖。
					<table width="100%">
						<tbody>
							<tr>
								<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">总和大</th>
								<th width="50%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">总和小</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">10、11、12、13、14、15、16、17、18</td>
								<td bgcolor="#FFF7F0" class="point" align="center">
									0、1、2、3、4、5、6、7、8</td>
							</tr>
						</tbody>
					</table> <br>
					<table width="100%">
						<tbody>
							<tr>
								<th width="50%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">总和单</th>
								<th width="50%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">总和双</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">1、3、5、7、9、11、13、15、17</td>
								<td bgcolor="#FFF7F0" class="point" align="center">
									0、2、4、6、8、10、12、14、16、18</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li>龙、虎、和 都是跟相邻的下一位数进行比较，个位将与万位进行比较。<br>
					<table width="100%">
						<tbody>
							<tr>
								<th width="30%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">玩法</th>
								<th width="23%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">龙</th>
								<th width="23%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">虎</th>
								<th width="23%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">和</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">万位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">大于仟位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">小于仟位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">等于仟位</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">仟位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">大于佰位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">小于佰位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">等于佰位</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">佰位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">大于拾位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">小于拾位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">等于拾位</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">拾位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">大于个位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">小于个位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">等于个位</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">个位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">大于万位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">小于万位</td>
								<td bgcolor="#FFF7F0" class="point" align="center">等于万位</td>
							</tr>
						</tbody>
					</table> ※举例：投注者购买佰位虎，当期开奖结果如为20352（佰位3小于十位5为虎），则视为中奖。
				</li>
			</ul>
		</dd>
	</dl>
	<h2>棋牌玩法</h2>
	<dl>
		<dt>◎百家乐玩法说明</dt>
		<dd>
			<ul>
				<li><p>百家乐游戏规则：百家乐是以开奖号码为基础，进行庄、闲对比的一种玩法！</p></li>
				<li><p>庄是指以开奖五个号码为基础，选择前两个数（万、千视为庄的前两张牌）。</p></li>
				<li><p>闲是指以开奖五个号码为基础，选择后两个数（十、个视为闲的前两张牌）。</p></li>
				<li><p>如第一轮未分出胜负需要再按牌例发第二轮牌，第三张牌闲先发，最多每方3张牌，谁最接近9点即为胜方，而相同点数即和局。</p></li>
				<li><p>如若投注庄/闲， 开奖结果为和局，那么不计输赢，并且退还本金，且不计水费,例如：00100。</p></li>
				<li>有一个天生赢家或者两个都大等于6(庄闲都不补)</li>
				<li>个位与百位和数的个位数，此张牌为第5张牌。万位与百位和数的个位数，此张牌为第6张牌</li>
				<li>如果闲家补牌，那么第5张牌给闲家，如果闲家不补牌，庄家补牌，那么第5张牌给庄家</li>
				<li>庄闲点数例图：
					<table>
						<tbody>
							<tr>
								<th width="25%" valign="top" bgcolor="#AFAFE4" align="center"
									class="subtitle2">闲2牌合计点数</th>
								<th width="25%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">闲家</th>
								<th width="25%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">庄2牌合计点数</th>
								<th width="25%" bgcolor="#AFAFE4" align="center"
									class="subtitle2">庄家</th>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">0</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">0</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">1</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">1</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">2</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">2</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">3</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">3</td>
								<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为8，庄无须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">4</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">4</td>
								<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为0，1，8，9，庄无须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">5</td>
								<td bgcolor="#FFF7F0" class="point" align="center">必须补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">5</td>
								<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为0，1，2，3，8，9，庄无须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">6</td>
								<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">6</td>
								<td bgcolor="#FFF7F0" class="point" align="center">若闲补牌为6，7，庄无须补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">7</td>
								<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
								<td bgcolor="#FFF7F0" class="point" align="center">7</td>
								<td bgcolor="#FFF7F0" class="point" align="center">不得补牌</td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">8或9</td>
								<td bgcolor="#FFF7F0" class="point" align="center">天生赢家，不须补牌</td>
								<td valign="top" bgcolor="#FFF7F0" class="point" align="center">8或9</td>
								<td bgcolor="#FFF7F0" class="point" align="center">天生赢家，不须补牌</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li>大小/单双/质合，以庄闲的点数来进行判断结果。 0~4为小，5~9为大;
					1、3、5、7、9为单，0、2、4、6、8为双; 1、2、3、5、7为质数，0、4、6、8、9为合数。</li>
				<li>庄对/闲对 是指两家首发(第一轮)的两张牌为对子,不含第三张。如：开奖55842，投庄对则中奖，投闲对不中奖</li>
			</ul>
		</dd>
		<dt>◎德州扑克玩法说明</dt>
		<dd>
			<ul>
				<li>德州扑克是以开奖五个号码为基准，按德州扑克牌面组合进行投注的一种玩法！组合大小的押注点举例如下：</li>
				<li>豹子：开出的五个号码都相同，如22222、33333。</li>
				<li>四张：即五个开奖号码中有四个为一样，如00001、77797。</li>
				<li>葫芦：即五个开奖号码中三个相同（三条）及两个号码相同（一对），如77997、45544。</li>
				<li>顺子：是指开出的五个号码是一串顺序的数字。09213、65743。</li>
				<li>三张：开出的五个号码中三个相同，且余下的两个号码完全不同，如：87477、65455，</li>
				<li>两对：是指开奖五个号码，能组成两个对子，如：97789、01022。</li>
				<li>一对：是指开出的五个号码，能够组成一个对子，如：65877、01322。</li>
				<li>杂牌：是指开出的五个号码全部都不一样，不能够组成任意对子或顺子，(不包含五离) 如：06587、98763。</li>
				<li>五离：是指开出的五个号码不能够组成对子，并且没有任何相邻的两个数。如：28064，19573</li>
				<li>以上投注某个点后，如果开奖的号码结果组合正好与其吻合，按赔率赢得金额，如果开奖号码组合结果不能与投注点吻合，则输！</li>
			</ul>
		</dd>
		<dt>◎牛牛玩法说明</dt>
		<dd>
			<ul>
				<li>牛牛游戏规则：牛牛是开奖结果的五个号码为基准，在开奖的五个数字中若有任意三个数字相加之和为0或10的倍数，其余下另两个数字之和的个位数作为对奖基准。</li>
				<li>如开奖结果为00017即8点（牛8）,02818即9点（牛9），99219、88400则是10点（即牛牛）；</li>
				<li>如开奖结果的五个号码为01234、12859将视为无点（任三个数组合都无法组成0或10的倍数）；</li>
				<li>再如开奖结果的五个号码相同时如：11111、22222、33333（豹子）皆视为无点，但00000则视为牛牛</li>
				<li>当前投注牛3，开奖结果为01912（牛3）时视为中奖，投注其他游戏项皆视为不中奖！</li>
				<li>承以上规则</li>
				<li>当投注于A项“小”，开奖结果为牛1、牛2、牛3、牛4、牛5时，皆视为中奖，如开出无点
					那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
				<li>当投注于A项“大”，开奖结果为牛6、牛7、牛8、牛9、牛10时，皆视为中奖，如开出无点
					那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
				<li>当投注于B项“单”，开奖结果为牛1、牛3、牛5、牛7、牛9时，皆视为中奖，如开出无点
					那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
				<li>当投注于B项“双”，开奖结果为牛2、牛4、牛6、牛8、牛10时，皆视为中奖，如开出无点
					那么不计输赢，并且退还本金，且不计水费， 其他情况均视为不中奖！</li>
			</ul>
		</dd>
		<dt>◎三公玩法说明</dt>
		<dd>
			<ul>
				<li>三公游戏规则：三公以开奖结果五个数字为基准，将左闲点数与右闲点数进行比对的一种玩法！</li>
				<li>左闲点数：取开奖五个数字的前三位（万、千、百）之和的个位数。</li>
				<li>右闲点数：取开奖五个数字的后三位（百、十、个）之和的个位数。</li>
				<li>如左闲点数为1，右闲点数为7，则投注右闲视为中奖！</li>
				<li>比对的点数中0点为10点视为最大，9点为次大，1点为最小！</li>
				<li>若开奖结果左闲点数与右闲点数相同时，则押左闲或右闲均不计输赢，并且退还本金，且不计水费；</li>
				<li>若投注和局，开奖结果为左闲大或右闲大时，视为不中奖！</li>
				<li>左闲点数/右闲点数的尾数：大小 0~4为小，5~9为大；单双 1、3、5、7、9为单，0、2、4、6、8为双；质合
					1、2、3、5、7为质数，0、4、6、8、9为合数。</li>
			</ul>
		</dd>
		<dt>◎龙虎斗玩法说明</dt>
		<dd>
			<ul>
				<li>龙虎游戏规则：龙虎斗是以开奖结果的五个数字作为基准，取任意位置（万、千、百、拾、个）的数字进行组合大小比对的一种玩法；</li>
				<li>当投注龙/虎时，开奖结果为和局，那么押注龙/虎视为不中奖，且计算有效金额退水；</li>
				<li>当投注"和"时，开奖结果为龙/虎，投注“和”视为不中奖；</li>
				<li>举例：开奖结果为：2,1,3,5,2 万为龙、千为龙虎时：结果
					龙(2）大于虎（1），即为开龙；如万为龙，个为虎时，结果一样大，即为开和局！</li>
			</ul>
		</dd>
	</dl>
</div>