<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：消息中心</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="mylottery_section3" style="margin: 0;">
				<div class="new_lottery">
					<div class="new_lottery_title title">
						<h1>消息中心</h1>
						<div class="clear"></div>
					</div>
					<div class="message_tip">
						<span class="on"><a t="0" href="javascript:void(0);">全部(<em
								id="all_all">0</em>封）
						</a></span> <span><a t="1" href="javascript:void(0);">未读(<em
								id="all_unread">0</em>封）
						</a></span>
					</div>
					<table class="new_lottery_tab" cellspacing="0"
						cellpadding="0">
						<thead>
							<tr>
								<th width="100"></th>
								<th>状态</th>
								<th>短信标题</th>
								<th>发布时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody id="record_tb">
						</tbody>
					</table>
					<style type="text/css">
.message_bot_btn #pageGro {
	float: right;
	width: auto;
}
</style>
					<div class="message_bot">
						<div class="message_bot_btn">
							<label id="allSelectType"><span class="check_span"><input
									type="checkbox"></span> 全选</label><span class="btn"
								id="del_all_message"><em></em>删除</span><span class="btn"
								id="mark_read"><em></em>标记为已读</span>
							<jsp:include page="../include/page.jsp"></jsp:include>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
	<script id="record_tpl" type="text/html">
		{{each list as row i}}
			<tr {{if i>0 && i%2!=0}}class="bggrey"{{/if}}>
				<td><span class="check_span"><input type="checkbox" name="message" i="{{row.userMessageId}}" red_id="{{row.id}}"></span></td>
				<td>{{$formatter row.status 'status'}}</td>
				<td><a href="javascript:void(0);" onclick="readMsg({{i}});">{{row.title}}</a></td>
				<td>{{$formatter row.createDatetime 'time'}}</td>
				<td><a href="javascript:void(0);" onclick="readMsg({{i}});">详情</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="delMsg({{row.userMessageId}});">删除</a></td>
			</tr>
		{{/each}}
	</script>
	<script type="text/javascript">
		var pageSize = 10;
		var pageNumber = 1;
		var first = true;
		var pageDatas = [];
		var status = 0;
		$(function() {
			searchMsg();
			first = false;
			
			$(".message_tip a").on("click",function(){
				$(this).parent().addClass("on").siblings().removeClass("on");
				var s = $(this).attr("t");
				status = s;
				searchMsg();
			})
		});

		function searchMsg() {
			if (!first) {
				pageNumber = parseInt($("#pageNumber").val());
			}
			$.ajax({
				url : "${base}/center/news/message/list.do",
				data : {
					pageNumber : pageNumber,
					pageSize : pageSize,
					status:status
				},
				success : function(result) {
					pageDatas = result.list;
					var html = template('record_tpl', result);
					$("#record_tb").html(html);
					layerPage(result.totalCount,result.currentPageNo,result.totalPageCount);
					$("#all_all").text(result.totalCount);
				}
			});
			
			var tb = $("#record_tb");
			tb.on("click","input[type=checkbox]",function(){
				if($(this).is(":checked")){
					$(this).parent().addClass("check_span_checked");
				}else{
					$(this).parent().removeClass("check_span_checked");
				}
			})
			$("#allSelectType").click(function(){
				if($(this).find("input").is(":checked")){
					$(this).find('span').addClass("check_span_checked");
					$("#record_tb :checkbox").prop("checked",true).parent().addClass("check_span_checked");
				}else{
					$(this).find('span').removeClass("check_span_checked");
					$("#record_tb :checkbox").prop("checked",false).parent().removeClass("check_span_checked");
				}
			})
			$("#del_all_message,#mark_read").click(function(event){
				var i = "",redId='';
				tb.find("input[type=checkbox]").each(function(index,item){
					if($(item).is(":checked")){
						i += $(item).attr("i") + ",";
						redId += $(item).attr("red_id") + ",";
					}
				})
				//执行删除
				if(!i){
					layer.msg('未选中',{icon:2});
					return false;
				}
				i = i.substring(0,i.length-1);
				redId = redId.substring(0,redId.length-1);
				var id = $(this).attr("id");
				if(id == 'mark_read'){
					$.post("${base}/center/news/message/batchRead.do",{id:redId},function(res){
						if(res.success){
							layer.msg('操作成功！',{icon:1});
							searchMsg();
							$("#allSelectType input[type=checkbox]").prop("checked",false).parent().removeClass("check_span_checked");
						}
					})
				}else{
					if(!i){
						layer.msg('不能删除未读信息，请先查看信息。',{icon:2});
						return false;
					}
					delMsg(i,"${base}/center/news/message/batchDelete.do");
				}
			})
			
		}
		
		
		
		template.helper('$formatter', function(content, key) {
			if (key == 'time') {
				return DateUtil.formatDatetime(content);
			} else if (key == 'status') {
				if (content == '2') {
					return '<span style="color:blue">已读</span>';
				} else {
					return '<span style="color:red">未读</span>';
				}
			}
		});

		function readMsg(index) {
			var row = pageDatas[index];
			$.ajax({
						url : "${base}/center/news/message/read.do",
						data : {
							id : row.id
						},
						success : function(result) {
							layer
									.open({
										type : 1, //page层
										area : [ '500px', '400px' ],
										skin:'layui-layer-rim',
										title : row.title,
										shade : 0.6, //遮罩透明度
										scrollbar : false,
										offset : '100px',//距离顶部
										moveType : 0, //拖拽风格，0是默认，1是传统拖动
										shift : 1, //0-6的动画形式，-1不开启
										content : '<div style="overflow-x: hidden;overflow-y: hidden;padding:20px;font-size:13px;text-align: center;">'
												+ row.message + '</div>'
									});
							searchMsg();
						}
					});

		}

		function delMsg(id,url) {
			var msg = "确定要删除全部信件吗？"
			if(!url){
				msg = "确定要删除该信件吗？";
				url = "${base}/center/news/message/delete.do";
			}
			layer.confirm(msg, {
				btn : [ '确定', '取消' ],offset : ['30%' ],icon:3
			}, function() {
				$.ajax({
					url : url,
					data : {
						id : id
					},
					success : function(result) {
						if (!result.success) {
							layer.msg(result.msg, {
								icon : 5
							});
						} else {
							layer.alert("删除成功！", {offset : ['30%' ],
								icon : 1
							}, function(index) {
								searchMsg();
								$("#allSelectType input[type=checkbox]").prop("checked",false).parent().removeClass("check_span_checked");
								layer.close(index);
							});
						}
					}
				});
			})
		}
	</script>
</body>
</html>