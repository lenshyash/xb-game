<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 账号信息</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="ordermain">
				<div class="navbox">
					<div class="prizetitle clearfix">
						<span class="float-left">账号信息</span> <span
							class="span-kf float-right">如有疑问请咨询&nbsp;<a href="${kfUrl}" target="_blank">在线客服</a></span>
					</div>
				</div>
				<div class="userinfo-edit-main">
					<div class="tab-user">
						<c:if test="${isGuest }">
						<div>试玩账号不能修改账户信息</div>
						</c:if>
						<c:if test="${!isGuest }">
						<table class="userTab" width="100%" cellspacing="0"
							cellpadding="0" border="0">
							<tbody>
								<tr class="username-tr">
									<td class="title">用户名：</td>
									<td colspan="2" class="tdinput">${loginMember.account}</td>
								</tr>
								<!-- 真实姓名设置 -->
								<c:if test="${empty member.userName}">
									<tr class="idcard-tr">
										<td class="title">真实姓名：</td>
										<td colspan="2" class="tdinput"><span class="s-word">真实姓名必须与出款银行一致！一经填写，不可以自行修改。</span></td>
									</tr>
									<tr class="idcard-tr">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input value=""
											class="text" id="userName" placeholder="请输入真实姓名" type="text"></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
									<tr class="idcard-tr" id="submit_userName"
										style="display: none;">
										<td class="title"></td>
										<td colspan="2" class="tdinput"><input
											class="submit normal" value="确认" type="submit" onclick="Base.xinXi.subUserName();"> <a class="s-a subColse"
											href="javascript:void(0);">取消</a></td>
									</tr>
								</c:if>
								<c:if test="${not empty member.userName}">
									<tr class="idcard-tr">
										<td class="title">真实姓名：</td>
										<td colspan="2" class="tdinput" id="userName">${member.userName}</td>
									</tr>
									<tr class="idcard-tr">
										<td class="title"></td>
										<td class="tdinput"><span class="s-word">真实姓名如果有误，请联系客服。</span></td>
										<td class="tdtip"><div>
												<span></span>
											</div></td>
									</tr>
								</c:if>
								<!-- 银行卡绑定 -->
								<c:if test="${empty member.cardNo}">
									<tr class="bank-tr">
										<td class="title">银行卡：</td>
										<td class="tdinput" colspan="2">暂未绑定请先<span
											class="add-btn"></span><a class="clickMember" cl="bank-tr"
											href="javascript:void(0);" dataIs="<c:out value="${empty member.userName?'1':'2'}"></c:out>">添加银行卡</a>
										</td>
									</tr>
									<tr class="bank-tr" style="display: none;">
										<td class="title">完善银行信息：</td>
										<td class="tdinput tdinput-width"><select id="bankName"
											style="width: 200px;">
												<option value="建设银行">建设银行</option>
												<option value="工商银行" selected="selected">工商银行</option>
												<option value="农业银行">农业银行</option>
												<option value="中国邮政银行">中国邮政银行</option>
												<option value="中国银行">中国银行</option>
												<option value="中国招商银行">中国招商银行</option>
												<option value="中国交通银行">中国交通银行</option>
												<option value="中国民生银行">中国民生银行</option>
												<option value="中信银行">中信银行</option>
												<option value="中国兴业银行">中国兴业银行</option>
												<option value="浦发银行">浦发银行</option>
												<option value="平安银行">平安银行</option>
												<option value="华夏银行">华夏银行</option>
												<option value="广州银行">广州银行</option>
												<option value="BEA东亚银行">BEA东亚银行</option>
												<option value="广州农商银行">广州农商银行</option>
												<option value="顺德农商银行">顺德农商银行</option>
												<option value="北京银行">北京银行</option>
												<option value="杭州银行">杭州银行</option>
												<option value="温州银行">温州银行</option>
												<option value="上海农商银行">上海农商银行</option>
												<option value="中国光大银行">中国光大银行</option>
												<option value="渤海银行">渤海银行</option>
												<option value="浙商银行">浙商银行</option>
												<option value="晋商银行">晋商银行</option>
												<option value="汉口银行">汉口银行</option>
												<option value="上海银行">上海银行</option>
												<option value="广发银行">广发银行</option>
												<option value="深圳发展银行">深圳发展银行</option>
												<option value="东莞银行">东莞银行</option>
												<option value="宁波银行">宁波银行</option>
												<option value="南京银行">南京银行</option>
												<option value="北京农商银行">北京农商银行</option>
												<option value="重庆银行">重庆银行</option>
												<option value="广西农村信用社">广西农村信用社</option>
												<option value="吉林银行">吉林银行</option>
												<option value="江苏银行">江苏银行</option>
												<option value="成都银行">成都银行</option>
												<option value="尧都区农村信用联社">尧都区农村信用联社</option>
												<option value="浙江稠州商业银行">浙江稠州商业银行</option>
												<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
										</select></td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="bank-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											id="province" type="text" placeholder="请输入开卡省份"></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
									<tr class="bank-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											id="city" type="text" placeholder="请输入开卡城市"></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
									<tr class="bank-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											id="bankcardaddress" type="text" placeholder="请输入开卡网点"></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
									<tr class="bank-tr" style="display: none;">
										<td class="title"><span class="red">*</span></td>
										<td class="tdinput tdinput-width"><input value=""
											class="text" id="bankId" placeholder="请输入银行卡号" type="text"></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
									<tr class="bank-tr" id="submit_Banks" style="display: none;">
										<td class="title"></td>
										<td colspan="2" class="tdinput"><input onclick="Base.xinXi.setBanks();" class="submit normal" value="确认" type="submit">
										 <a class="s-a memClose" cl="bank-tr" href="javascript:void(0);">取消</a></td>
									</tr>
								</c:if>
								<c:if test="${not empty member.cardNo}">
									<tr class="bank-tr">
										<td class="title">银行卡：</td>
										<td colspan="2" class="tdinput"><span class="s-word-1">已绑定
												|</span><span class="s-word-1">${member.bankName}</span><span
													<c:set value="${fn:length(member.cardNo) }" var="cardNO"></c:set>
											class="mg-l-10">（卡号：<c:forEach begin="1" end="${cardNO-4}">*</c:forEach> ${fn:substring(member.cardNo,cardNO-4,cardNO)}）</span></td>
									</tr>
									<tr class="bank-tr">
										<td class="title"></td>
										<td class="tdinput"><span class="s-word">如需更换银行卡绑定，请联系客服。</span></td>
										<td class="tdtip"><div>
												<span class="red"></span>
											</div></td>
									</tr>
								</c:if>
								<!-- 登录密码修改 -->
								<tr class="pwd-tr">
									<td class="title">登录密码：</td>
									<td class="tdinput" colspan="2"><span
										class="pwd-icon pwd-low">低</span> <a
										class="mg-l-10 clickMember" cl="pwd-tr"
										href="javascript:void(0);">修改</a></td>
								</tr>
								<tr class="pwd-tr" style="display: none;">
									<td class="title">登录密码修改：</td>
									<td class="tdinput tdinput-width" colspan="2"><div class="s-word">请输入6-16个英文或数字，且符合0~9或a~z字元</div>
									</td>
								</tr>
								<tr class="pwd-tr" style="display: none;">
									<td class="title"></td>
									<td class="tdinput tdinput-width"><input class="text"
										id="updOLoginPwd" placeholder="请输入原登录密码" type="password">
									</td>
									<td class="tdtip">
										<div>
												<span class="red"></span>
											</div>
									</td>
								</tr>
								<tr class="pwd-tr" style="display: none;">
									<td class="title"></td>
									<td class="tdinput tdinput-width"><input class="text"
										id="updLoginPwd" placeholder="请输入新登录密码" type="password"></td>
									<td class="tdtip">
									<div>
												<span class="red"></span>
											</div>
									</td>
								</tr>
								<tr class="pwd-tr" style="display: none;">
									<td class="title"></td>
									<td class="tdinput tdinput-width"><input class="text" id="updRLoginPwd"
										placeholder="请再次输入" type="password"></td>
									<td class="tdtip">
										<div>
												<span class="red"></span>
											</div>
									</td>
								</tr>
								<tr class="pwd-tr" style="display: none;">
									<td class="title"></td>
									<td colspan="2" class="tdinput"><input
										class="submit normal" value="确认" onclick="Base.xinXi.updPwd(1);" type="button">
										<a class="s-a memClose" cl="pwd-tr" href="javascript:void(0);">取消</a></td>
								</tr>
								<!-- 支付密码设置 -->
								<c:if test="${ empty member.receiptPwd }">
									<tr class="paypwd-tr">
										<td class="title">提款密码：</td>
										<td class="tdinput" colspan="2"><span class="s-word-1">未设置
												|</span> <a href="javascript:void(0);" class="clickMember"
											cl="paypwd-tr"> 立即设置</a> <span class="s-word mg-l-10">必须设置提款密码，才能进行提款！<span style="color: red;">不是您的银行卡密码，是网站的提款安全密码!</span></span></td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title">提款密码设置：</td>
										<td class="tdinput tdinput-width" colspan="2"><div class="s-word">最少6位数字</div></td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											 id="payPwd" placeholder="请输入提款密码" type="password"></td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											id="rPayPwd" placeholder="请再次输入" type="password"></td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td colspan="2" class="tdinput"><input
											class="submit normal" value="确认" onclick="Base.xinXi.setPayPwd();" type="button">
											<a class="s-a memClose" cl="paypwd-tr"
											href="javascript:void(0);">取消</a></td>
									</tr>
								</c:if>
								<c:if test="${not empty member.receiptPwd}">
									<tr class="paypwd-tr">
										<td class="title">提款密码：</td>
										<td class="tdinput" colspan="2"><span class="s-word-1">已设置
												|</span> <a href="javascript:void(0);" class="clickMember"
											cl="paypwd-tr"> 立即修改</a></td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title">提款密码修改：</td>
										<td class="tdinput tdinput-width" colspan="2"><div class="s-word">提款密码需由6-16个字母和数字(A-Z和 0-9)组成</div></td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											 id="updOPayPwd" placeholder="请输入原提款密码" type="password">
										</td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											 id="updPayPwd"
											placeholder="请输入新提款密码" type="password"></td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td class="tdinput tdinput-width"><input class="text"
											 id="updRPayPwd" placeholder="请再次输入" type="password"></td>
										<td class="tdtip">
											<div>
												<span class="red"></span>
											</div>
										</td>
									</tr>
									<tr class="paypwd-tr" style="display: none;">
										<td class="title"></td>
										<td colspan="2" class="tdinput"><input
											class="submit normal" value="确认" onclick="Base.xinXi.updPwd(2);" type="button">
											<a class="s-a memClose" cl="paypwd-tr"
											href="javascript:void(0);">取消</a></td>
									</tr>
								</c:if>
							</tbody>
						</table>
						<div class="user-guide user-no-idcard" id="user_no_idcard"
							style="display: none;">
							<a class="close-btn" id="close_idcard" href="javascript://"></a>
							<div class="inner-guide">
								<ul>
									<li class="s-word-1">&gt; 身份信息是您领取大奖的重要凭据！<br> <strong>一经填写，不可自行修改。</strong></li>
									<li class="s-word-1">&gt; 账户余额仅可提款至同名银行卡中，确保资金安全。</li>
								</ul>
							</div>
							<a class="write-btn" id="write_idcard" href="javascript://">立即填写&gt;&gt;</a>
						</div></c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			//select
			$("#bankName").chosen({
				disable_search : true,
				width : '200px'
			});

			var $tab = $(".userTab");
			$tab.on("mouseenter mouseleave", "tr", function(event) {
				var cl = $(this).attr("class");
				if (event.type == 'mouseenter') {
					$tab.find("." + cl).addClass('tr-clicked');
				} else {
					$tab.find('tr').removeClass('tr-clicked');
				}
			}).on("focus", "#userName", function() {
				$("#submit_userName").show();
			}).on("focus","input",function(){
				$(this).removeClass('text-error');
				$(this).parent().siblings('.tdtip').find("span").text("");
			}).on("click", "a.subColse", function() {
				$(this).parent().parent().hide();
			}).on("click", "a.clickMember", function() {
				var cl = $(this).attr("cl"),dataIs=$(this).attr("dataIs");
				if(!(!dataIs)){
					if(dataIs == 1){
						layer.tips("请先完善真实姓名",$("#userName"),{tips:[1,'#f13131']});
						$("#userName").focus();
						return;
					}
				}
				$(this).parent().parent().hide().siblings('.' + cl).show();
			}).on("click", "a.memClose", function() {
				var cl = $(this).attr('cl');
				$tab.find('.' + cl).each(function(index, item) {
					if ($(item).is(":hidden")) {
						$(item).show();
					} else {
						$(item).hide();
					}
				})
			})
		})
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>