<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="title">
	<div class="about_us_title">忘记登录密码了怎么办？</div>
</div>
<div class="clear" style="height: 25px;"></div>
<div class="about_us_content">
	<p>
		网站全站使用统一的密码（密码长度应为6到16个字符）。
	</p>
	<p>
		若忘记密码，可以通过在线客服或拨打客服热线来咨询，我们需要核对用户的姓名，身份证号，银行卡号，充值记录，资料核对无误之后，由工作人员为用户修改密码，用户登陆后请自行修改密码。
	</p>
</div>
<div class="clear" style="height: 32px;"></div>