<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/lhc.css"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/jquery.cookie.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/mark_six.js?v=${caipiao_version}2"></script>
<div class="lhcMain">
	<div class="lhc_navs">
		<ul id="lhcTabs" class="lhcTabs">
			<c:forEach items="${playGroupList }" var="group" varStatus="g">
				<c:choose>
					<c:when test="${g.index == 0}">
						<li class="current" value="${group.code}" type="${group.lotType}"
							default="${group.stationId}" tag="${group.id}">
					</c:when>
					<c:otherwise>
						<li value="${group.code}" type="${group.lotType}"
							default="${group.stationId}" tag="${group.id}">
					</c:otherwise>
				</c:choose>
				<a href="javascript:void(0);">${group.name}</a>
				</li>
			</c:forEach>
			<!-- <li class="shadow"></li> -->
		</ul>
	</div>
	<div id="content" class="lhc_content">
		<!-- 预设金额和提交充值按钮 -->
		<div class="skin_red">
			<div id="lhcContent">
				<jsp:include page="lhc_set_money.jsp" flush="true"><jsp:param value="true" name="kj"/></jsp:include>
				<div id="bet_panel" class="bet_panel input_panel bet_closed">
				</div>
				<jsp:include page="lhc_set_money.jsp" flush="true"><jsp:param value="false" name="kj"/> </jsp:include>
				<!-- 预设金额 -->
			</div>
			<div class="lhc_R right">
			<div class="user_R">
				<ul>
				<li>会员帐号：<strong class="red" id="userName"></strong></li>
				<li>会员余额：<strong class="red refreshM"></strong><i onclick="refreshMoney()" class="img-login-refresh icon"></i></li>
				<li class="shadow"></li>
				</ul>
			</div>
			<div id="zxzd" class="hidden">
			<table>
				<thead>
					<tr>
						<th colspan="2">最新注单</th>
					</tr>
				</thead>
				<tbody id="betResultOrder">
				</tbody>
			</table>
			</div>
			<div id="lmcl" class="hidden">
			<table>
				<thead>
					<tr>
						<th colspan="2" class="table_side">两面长龙排行</th>
					</tr>
				</thead>
				<tbody id="ommitQueue">
				</tbody>
			</table>
			</div>
			<div id="kjtz" class="">
			<table>
				<thead>
					<tr>
						<th colspan="3" class="table_side">快捷投注</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td data="sple-danM-">单码</td>
						<td data="sple-xdan-">小单</td>
						<td data="sple-hdan-">合单</td>
					</tr>
					<tr>
						<td data="sple-shuangM-">双码</td>
						<td data="sple-xshuang-">小双</td>
						<td data="sple-hshuang-">合双</td>
					</tr>
					<tr>
						<td data="sple-daM-">大码</td>
						<td data="sple-ddan-">大单</td>
						<td data="sple-hda-">合大</td>
					</tr>
					<tr>
						<td data="sple-xiaoM-">小码</td>
						<td data="sple-dshuang-">大双</td>
						<td data="sple-hxiao-">合小</td>
					</tr>
					<tr>
						<td data="sple-tou0-">0头</td>
						<td data="sple-wei0-">0尾</td>
						<td data="sple-wei5-">5尾</td>
					</tr>
					<tr>
						<td data="sple-tou1-">1头</td>
						<td data="sple-wei1-">1尾</td>
						<td data="sple-wei6-">6尾</td>
					</tr>
					<tr>
						<td data="sple-tou2-">2头</td>
						<td data="sple-wei2-">2尾</td>
						<td data="sple-wei7-">7尾</td>
					</tr>
					<tr>
						<td data="sple-tou3-">3头</td>
						<td data="sple-wei3-">3尾</td>
						<td data="sple-wei8-">8尾</td>
					</tr>
					<tr>
						<td data="sple-tou4-">4头</td>
						<td data="sple-wei4-">4尾</td>
						<td data="sple-wei9-">9尾</td>
					</tr>
					<tr>
						<td data="an-1-">鼠</td>
						<td data="an-5-">龙</td>
						<td data="an-9-">猴</td>
					</tr>
					<tr>
						<td data="an-2-">牛</td>
						<td data="an-6-">蛇</td>
						<td data="an-10-">鸡</td>
					</tr>
					<tr>
						<td data="an-3-">虎</td>
						<td data="an-7-">马</td>
						<td data="an-11-">狗</td>
					</tr>
					<tr>
						<td data="an-4-">兔</td>
						<td data="an-8-">羊</td>
						<td data="an-12-">猪</td>
					</tr>
					<tr>
						<td data="col-red-" class="red">红</td>
						<td data="col-blue-" class="blue">蓝</td>
						<td data="col-green-" class="green">绿</td>
					</tr>
					<tr>
						<td data="col-red-dan" class="red">红单</td>
						<td data="col-blue-dan" class="blue">蓝单</td>
						<td data="col-green-dan" class="green">绿单</td>
					</tr>
					<tr>
						<td data="col-red-shuang" class="red">红双</td>
						<td data="col-blue-shuang" class="blue">蓝双</td>
						<td data="col-green-shuang" class="green">绿双</td>
					</tr>
					<tr>
						<td data="col-red-da" class="red">红大</td>
						<td data="col-blue-da" class="blue">蓝大</td>
						<td data="col-green-da" class="green">绿大</td>
					</tr>
					<tr>
						<td data="col-red-xiao" class="red">红小</td>
						<td data="col-blue-xiao" class="blue">蓝小</td>
						<td data="col-green-xiao" class="green">绿小</td>
					</tr>
					<tr>
						<td data="sple-all-">全选</td>
						<td colspan="2" data="sple-clear-">取消</td>
					</tr>
				</tbody>
			</table>
			</div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>