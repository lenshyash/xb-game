$(function(){
	_Lotterys.getLotteryAll("");
})

var _Lotterys = {
	getLotteryAll:function(lotCode){
		var datas = {lotCode:lotCode,needLast:true}
		$.ajax({
			url:LECAI.base + "/lotteryV3/lotterys.do",
			data:datas,
			dataType:'json',
			type:'GET',
			success:function(result){
				$.each(result.lot,function(i,j){	//lot彩种list集合
					_Lotterys.dataLastNext(result[j.code]);
					_Lotterys.dataCurTimers(result.serverTime,result[j.code],j);
				})
			}
		})
	},
	dataLastNext:function(next){
		if(!next || !next.lastHaoMa || !next.lotCode){return;}
		var timers;
		$("#"+next.lotCode+"QiHao").text(next.qiHao);
		if(next.lastHaoMa.indexOf("?")>=0){
			$("#"+next.lotCode+"LastHaoMa").html('<span class="tabulousclear red">正在开奖...</span>');
				clearInterval(timers);
				timers = window.setInterval(function(){
					//10秒中获取一次开奖结果,正式开启
					_Lotterys.lastData(next.lastQiHao,next.lotCode,timers);
				},10000);
			//}
		}else{
			var sp = next.lastHaoMa.split(","),temp="";
			for(var i=0;i<sp.length;i++){
				temp += '<li flag="normal">'+sp[i]+'</li>';
			}
			$("#"+next.lotCode+"LastHaoMa").html(temp);
		}
	},
	dataCurTimers:function(serverTime,cur,lot){
		if(!cur || !cur.lotCode || !cur.qiHao){return;}
		var state = 1,downTimers;
		if(cur.id && cur.id == 1){
			state = 2;
		}
		var server_time = serverTime;
		var action_time = cur.actionTime;
		var diff_time = ( action_time - server_time ) / 1E3;
		var ddTime = diff_time -(action_time - new Date().getTime()) / 1E3;	//和访问机器相差的秒数
		if(LECAI.version == 5 || cur.lotCode == 'LHC' || cur.lotCode == 'SFLHC' || cur.lotCode == 'TMLHC' || cur.lotCode == 'WFLHC' || cur.lotCode == 'HKMHLHC'){
			var result_time = _Lotterys.diffTimer(diff_time-lot.ago);
		}else{
			var result_time = _Lotterys.diffTimer(diff_time);
		}
		$("#"+lot.code+"OpenTime span.leaveh-1").html(result_time);
		clearInterval(downTimers);
		downTimers = window.setInterval(function(){
			var reusltTime = _Lotterys.tick(lot.ago,action_time,diff_time,ddTime,state,lot.code,downTimers);
			diff_time = reusltTime;
			diff_time--;
		},1000);
	},
	tick:function(ago,action_time,diff_time,ddTime,state,code,clearTimer){
		var vg = 0,dTime = 0;
		dTime = diff_time - (action_time - new Date().getTime()) / 1E3;//	计算倒计时和js时间的差值
		if(dTime > ddTime && dTime - ddTime > 1){
			diff_time = diff_time - parseInt(dTime-ddTime);
		}
		if(LECAI.version == 5 || code == 'LHC' || code == 'SFLHC' || code == 'TMLHC' || code == 'WFLHC' || code == 'HKMHLHC'){
			vg = diff_time - ago;
			if(diff_time > 0 && diff_time > ago && state == 1){
				var html = _Lotterys.diffTimer(vg);
				$("#"+code+"OpenTime span.leaveh-2").text('距离封盘截止')
				$("#"+code+"OpenTime span.leaveh-1").html(html);
			}else{
				var html = _Lotterys.diffTimer(diff_time);
				$("#"+code+"OpenTime span.leaveh-2").text('距离开盘截止')
				$("#"+code+"OpenTime span.leaveh-1").html(html);
			}
		}else{
			if(state == 1){
				$("#"+code+"OpenTime span.leaveh-2").text('距离封盘截止')
				var html = _Lotterys.diffTimer(diff_time);
				$("#"+code+"OpenTime span.leaveh-1").text(html);
			}else{
				$("#"+code+"OpenTime span.leaveh-2").text('距离开盘截止')
				var html = _Lotterys.diffTimer(diff_time);
				$("#"+code+"OpenTime span.leaveh-1").text(html);
			}
		}
		if(diff_time <= -1){
			clearInterval(clearTimer);
			_Lotterys.getLotteryAll(code);
		}
		
		return diff_time;
	},
	diffTimer:function(intDiff){
		var day=0,hour=0,hours=0,minute=0,second=0,time_html='';//时间默认值		
		if(intDiff > 0){
			day = Math.floor(intDiff / (60 * 60 * 24));
			hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
			hours = Math.floor(intDiff / (60*60));
			minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
			second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
		}
		if (hours <= 9 ) hours = '0' + hours;
		if (minute <= 9) minute = '0' + minute;
		if (second <= 9) second = '0' + second;
		time_html = hours + ':' + minute + ':' + second;
		return time_html;
	},
	lastData:function(qh,code,timers){
		$.ajax({
			url:LECAI.base + '/lotteryV3/lotLast.do',
			data:{lotCode:code,qiHao:qh},
			dataType:'json',
			type:'get',
			success:function(data){
				if(data.success){
					clearInterval(timers);
					//说明上一期已开奖，刷新页面即可，停止定时器
					var sp = data.last.haoMa.split(","),temp="";
					for(var i=0;i<sp.length;i++){
						temp += '<li flag="normal">'+sp[i]+'</li>';
					}
					$("#"+code+"LastHaoMa").html(temp);
				}
			}
		})
	}
}