<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：提款中心</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="ucmain tkmain">
				<div class="gmmain draw-main">
					<div class="infobox-draw">
						<span class="draw-icon">0手续费</span>
						<div class="userinfo-draw">
							<div class="userinfo-left">
								<div class="js-xj">
									<span class="userinfo-tit">提款现金金额</span>您当前账户余额<span
										class="money gift-wallet">${loginMember.money}</span>${not empty cashName?cashName:'元'}
								</div>
							</div>
							<input id="cash_wallet" value="0" type="hidden"> <input
								id="recharge_wallet" value="0" type="hidden"> <input
								id="gift_wallet" value="0" type="hidden">
							<div class="userinfo-right">
								<div class="js-xj-btn">
									<span class="next-btn"></span><a href="${base}/center/record/hisrecord/cp3Page.do?type=2">查看提款记录</a>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<c:if test="${isGuest }">
					<div class="userinfo-edit-main">试玩账号不能提款</div>
					</c:if>
					<c:if test="${!isGuest }">
					<!-- 取款密码设置页面 -->
					<c:if test="${type eq '1' }">
						<jsp:include page="drawStep/drawPwd.jsp"></jsp:include>
					</c:if>
					<!-- 信息填写页面 -->
					<c:if test="${type eq '2' }">
						<jsp:include page="drawStep/drawData.jsp"></jsp:include>
					</c:if>
					<!-- 提款页面 -->
					<c:if test="${type eq '3' }">
						<jsp:include page="drawStep/drawCommit.jsp"></jsp:include>
					</c:if>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>