<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="part-1-left-nav mr20">
	<div class="lotterys-list-hd" id="lotterysList" style="display:none;">
		<ul class="lottery-list-box">
			<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
				<c:if test="${lotIndex.index<=6}">
					<c:choose>
						<c:when test="${lotIndex.first}">
							<li class="mainGame"
								style="padding-top: 18px; background-color: rgb(255, 255, 255);">
								<a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="mainA"><i class="icon_small ${lot.code}"></i><span
									class="color333">${lot.name}</span></a> <a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="status-desc"><em class="icon"></em><span>立即投注</span></a>
							</li>
						</c:when>
						<c:otherwise>
							<li class="mainGame"
								style="background-color: rgb(255, 255, 255);"><a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="mainA"><i class="icon_small ${lot.code}"></i><span
									class="color333">${lot.name}</span></a> <a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="status-desc"><em class="icon"></em><span>立即投注</span></a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
			<li class="allGames clearfix">
				<h3>
					<span>高频</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex" end="3">
						<c:if test="${lotVersion !=5 && (lot.type == 51 || lot.type == 52 || lot.type == 53 || lot.type == 57 || lot.type == 58)}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
						<c:if test ="${lotVersion ==5 && lot.code != 'LHC' && lot.code != 'FC3D' &&  lot.code != 'PL3' }">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
						
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_1">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>高频彩</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
									<c:choose>
										<c:when test ="${lotVersion !=5 }">
											<c:if test="${lots.type == 51 || lots.type == 52 || lots.type == 53 || lots.type == 57 || lots.type == 58}">
												<c:choose>
													<c:when test="${lots.type == 53 }">
														<li><a
															href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
															class="colorRed">${lots.name}</a></li>
													</c:when>
													<c:otherwise>
														<li><a
															href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
													</c:otherwise>
												</c:choose>
											</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${lots.code != 'LHC' && lots.code != 'FC3D' &&  lots.code != 'PL3'}">
												<c:choose>
													<c:when test="${lots.code eq 'BJSC' || lots.code eq 'XYFT'  || lots.code eq 'LXYFT'  || lots.code eq 'AZXY10'  || lots.code eq 'PCEGG' || lots.code eq 'SFSC' || lots.code eq 'JND28'}">
														<li><a
															href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
															class="colorRed">${lots.name}</a></li>
													</c:when>
													<c:otherwise>
														<li><a
															href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
													</c:otherwise>
												</c:choose>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
			<li class="allGames clearfix">
				<h3>
					<span>低频</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
						<c:if test="${lotVersion !=5 && (lot.type == 54 || lot.type == 6)}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
						<c:if test="${lotVersion ==5 && (lot.code eq 'LHC' || lot.code eq 'FC3D' ||  lot.code eq 'PL3' )}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_2">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>低频彩</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
								<c:choose>
									<c:when test ="${lotVersion !=5 }">
										<c:if test="${lots.type == 54 || lots.type == 6}">
											<c:choose>
												<c:when test="${lots.type == 54 }">
													<li><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
														class="colorRed">${lots.name}</a></li>
												</c:when>
												<c:otherwise>
													<li><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${lots.code eq 'LHC' || lots.code eq 'FC3D' ||  lots.code eq 'PL3'}">
											<c:choose>
												<c:when test="${lots.code eq 'FC3D' ||  lots.code eq 'PL3'}">
													<li><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
														class="colorRed">${lots.name}</a></li>
												</c:when>
												<c:otherwise>
													<li><a
														href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:otherwise>
									</c:choose>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
			<li class="allGames clearfix">
				<h3>
					<span>全部</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
						<c:if test="${lotIndex.index<=3}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_3">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>全部彩种</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
									<c:choose>
										<c:when test="${lots.code eq 'CQSSC' || lots.code eq 'BJSC' || lots.code eq 'PCEGG'}">
											<li><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
												class="colorRed">${lots.name}</a></li>
										</c:when>
										<c:otherwise>
											<li><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>