<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/user_content.css?v=111" type="text/css">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 真人投注记录</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能投注真人</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label></div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}</span>
				</div>
				<a class="btn recharge normal" href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox"><div class="prizetitle">真人投注记录</div><div class="clear"></div></div>
				<div class="filter">
					<ul>
						<li>
							<div class="category">
								<select style="width: 100px;" id="type">
									<option value="0">所有真人</option>
									<option value="1">AG记录</option>
									<option value="8">DS记录</option>
									<option value="2">BBIN记录</option>
									<option value="3">MG记录</option>
									<option value="5">ALLBET记录</option>
									<option value="7">OG记录</option>
									<option value="97">VR记录</option>
									<option value="98">BG记录</option>
								</select>
							</div>
						</li>
						<li>
							<div class="category_time">
								<select id="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>  
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
							<li>
							<div class="time-piece">
								<input name="startTime" id="startTime" class="text text75" readonly="readonly" value="${startTime}" type="text" data-end-date="${endTime}">
								<span>至</span> <input name="endTime" id="endTime" class="text text75" readonly="readonly" value="${endTime}" type="text" data-end-date="${endTime}">
								<input class="ddbtn" type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>三方游戏类型</td>
								<td>注单号</td>
								<td>游戏名称</td>
								<td>游戏局号</td>
								<td>投注金额</td>
								<td>输赢金额</td>
								<td>投注内容</td>
								<td>投注时间(北京)</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/real.do">立即投注</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot" style="display:none;">
							<tr>
								<td colspan="4" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td class="red" id="orderWinAmount"></td>
								<td colspan="2">&nbsp;</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div></c:if>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
<script type="text/javascript">
$(function() {
	$('#endTime,#startTime').cxCalendar({
		format : 'YYYY-MM-DD'
	});
	$("#selected_time").change(function() {
		quickSelDate(parseInt($(this).val()));
	});
	initRdsData();
});
function search(){
	$("#pageNumber").val(1);
	initRdsData();
}
function initRdsData(){
	var load = new Loading();
	load.init({
		target:"#betResultOrder"
	})
	load.start();
	var pageNumber=$("#pageNumber").val(),pageSize=10;
	var data = {
		type: $("#type").val(),
		startTime:$("#startTime").val(),
		endTime:$("#endTime").val(),
		pageSize:pageSize,
		pageNumber:pageNumber
	}
	$.ajax({
		url:"${base}/center/record/betrecord/realrecord.do",
		data:data,
		success:function(res){
			if(res.success===false){
				alertMsg(res.msg||"发生错误");
				load.stop();
				return;
			}
			if(res.total == 0){
				//无数据
				temp = '<tr><td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/real.do">立即投注</a></td></tr>';
				$("#betResultOrder").html(temp);
				$("#orderBetOrderTblFoot").hide();
				layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
			}else{
				addTemplate(res,pageNumber,pageSize);
			}
			load.stop();
		}
	})
}
var curNavFlag=null;
function addTemplate(res,pageNumber,pageSize){
	var temp ="", bd=null;
	$.each(res.rows,function(index,item){
		temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>';
		switch(item.type-0){
		case 1: temp += "AG</td>";break;
		case 2: temp += "BBIN</td>";break;
		case 3: temp += "MG</td>";break;
		case 4:temp += "QT</td>";break;
		case 5:temp += "ALLBET</td>";break;
		case 6:temp += "PT</td>";break;
		case 7:temp += "OG</td>";break;
		case 8:temp += "DS</td>";break;
		case 9:temp += "CQ9</td>";break;
		case 11:temp += "JDB</td>";break;
		case 92:temp += "TTG</td>";break;
		case 95:temp += "MW</td>";break;
		case 96:temp += "ISB</td>";break;
		case 98:temp += "BG</td>";break;
		case 97:temp += "VR</td>";break;
		}
		temp+='<td>'+item.bettingCode+'</td>';
		temp+='<td>'+item.gameType+'</td>';
		if(item.type==3){
			temp+='<td>'+item.tableCode+'</td>';
		}else{
			temp+='<td>'+item.gameCode+'</td>';
		}
		temp+='<td>'+item.bettingMoney+'</td>';
		temp+='<td>'+item.winMoney+'</td>';
		temp+='<td>'+item.bettingContent+'</td>';
		bd = new Date(item.bettingTime);
		temp+='<td>'+bd.format("MM月dd日,hh:mm:ss")+'</td></tr>';
	})
	$("#betResultOrder").html(temp);
	$("#orderBetOrderTblFoot").show();
	$("#orderBetAmount").html(res.aggsData.bettingMoneyCount||0);
	$("#orderWinAmount").html(res.aggsData.winMoneyCount||0);
	layerPage(res.total, pageNumber,  Math.ceil(res.total / pageSize));
}
</script>
</body>
</html>