<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="part-1-left-nav mr20">
	<div class="lotterys-list-hd" id="lotterysList" style="display:none;">
		<ul class="lottery-list-box">   
			<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
				<c:if test="${lotIndex.index<=6}">
					<c:choose>
						<c:when test="${lotIndex.first}">
							<li class="mainGame"
								style="padding-top: 18px; background-color: rgb(255, 255, 255);">
								<a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="mainA">
								<c:choose>
								   <c:when test="${not empty lot.imgUrl}">  
								       <i class="icon_small" style="background:url(${lot.imgUrl}) no-repeat;background-size:25px;"></i>
								   </c:when>
								   <c:otherwise> 
									   <i class="icon_small ${lot.code}"></i>
								   </c:otherwise>
								</c:choose>								
								<span
									class="color333">${lot.name}</span></a> <a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="status-desc"><em class="icon"></em><span>立即投注</span></a>
							</li>
						</c:when>
						<c:otherwise>
							<li class="mainGame"
								style="background-color: rgb(255, 255, 255);"><a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="mainA">
									<c:choose>
								   <c:when test="${not empty lot.imgUrl}">  
								       <i class="icon_small" style="background:url(${lot.imgUrl}) no-repeat;background-size:25px;"></i>
								   </c:when>
								   <c:otherwise> 
									   <i class="icon_small ${lot.code}"></i>
								   </c:otherwise>
								</c:choose>	
								<span
									class="color333">${lot.name}</span></a> <a
								href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}"
								class="status-desc"><em class="icon"></em><span>立即投注</span></a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
			<li class="allGames clearfix">
				<h3>
					<span>高频</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex"
						end="3">
						<c:if test="${lot.type == 8 || lot.type == 9 || lot.type == 10 || lot.type == 11 || lot.type == 12 || lot.type == 14 || lot.type == 66}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_1">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>高频彩</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
									<c:if
										test="${lots.type == 8 || lots.type == 9 || lots.type == 10 || lots.type == 11 || lots.type == 12 || lots.type == 14 || lots.type == 66}">
										<c:choose>
											<c:when test="${lots.type == 8 || lots.type == 11 }">
												<li><a
													href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
													class="colorRed">${lots.name}</a></li>
											</c:when>
											<c:otherwise>
												<li><a
													href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
			<li class="allGames clearfix">
				<h3>
					<span>低频</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
						<c:if test="${lot.type == 15 || lot.type == 6}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_2">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>低频彩</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
									<c:if test="${lots.type == 15 || lots.type == 6}">
										<c:choose>
											<c:when test="${lots.type == 15}">
												<li><a
													href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
													class="colorRed">${lots.name}</a></li>
											</c:when>
											<c:otherwise>
												<li><a
													href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
			<li class="allGames clearfix">
				<h3>
					<span>全部</span>
				</h3>
				<ul class="clearfix game-list">
					<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
						<c:if test="${lotIndex.index<=3}">
							<li><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></li>
						</c:if>
					</c:forEach>
				</ul> <i class="icon open-btn" style="display: block;"></i>
				<div class="line-fff" style="display: none;"></div>
				<div class="moreGames clearfix" style="display: none;"
					id="moreGames_3">
					<div class="moreGames-box fl">
						<div class="otherGames num-games">
							<h3>全部彩种</h3>
							<ol>
								<c:forEach items="${lotList}" var="lots" varStatus="lotIndexs">
									<c:choose>
										<c:when test="${lots.code eq 'CQSSC' || lots.code eq 'BJSC' || lots.code eq 'PCEGG'}">
											<li><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}"
												class="colorRed">${lots.name}</a></li>
										</c:when>
										<c:otherwise>
											<li><a
												href="${base}/lotteryV3/lotDetail.do?lotCode=${lots.code}">${lots.name}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ol>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>