<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
	<c:if test="${domainFolder == 'd00555' ||  domainFolder == 'd005100' }">
		<title>${_title}</title>
	</c:if>
	<c:if test="${domainFolder != 'd00555'}">
		<title>购彩中心-${_title}</title>
	</c:if>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/lotteryDetail.css?v=${caipiao_version}"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/lottery_nav.css?v=${caipiao_version}"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/js/flipclock/flipclock.css"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/js/loading/loading.css"
	type="text/css">
	<script>
    	var GLOBAL={
    		unCancleOrder:${unCancleOrder},
    		unChaseOrder:${unChaseOrder}
    	};
    </script>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-browser.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/flipclock/flipclock.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/loading/loading.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/compress_v3.js?v=1.0.4"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="hd_main">
		<c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'on'}">
		<div class="quick-tpis" style="margin-top:10px;">
			<div style="float:left;">
			<i class="icon-lb"></i> <b>最新公告：</b></div>
			<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/wenzi_scroll.js"></script>
			<div style="float:left; height: 30px; width: 880px;" id="scroll">
				<ul>
					<li></li>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
		$(function(){
			initGg();
		})
		function initGg() {
			// 最新消息跑馬燈
			$.ajax({
				url : "${base}/getConfig/getArticle.do",
				data : {
					code : 13
				},
				type : "post",
				dataType : 'json',
				success : function(r) {
					var col = '';
					for (var i = 0; i < r.length; i++) {
						col += r[i].content + "&nbsp;&nbsp;&nbsp;";
					}
					$("#scroll ul li").html(col);
					$("#scroll").marquee({
				        direction:"left",
				        speed:30
				    });
				}
			});
		}
		</script>
		</c:if>
		<div class="gm_con_to">
			<input type="hidden" value="1" id="navId" />
			<div style="margin-top: 5px;" class="unit_title">
				<div class="ut_l"></div>
				<div class="ut_r"></div>
			</div>
			<div class="gct_l">
				<div class="game-icon1">
				<c:choose>
					<c:when test="${not empty bly.imgUrl}">
						<img src="${bly.imgUrl}"alt="" width="114" height="114">
					</c:when>
					<c:otherwise>
						<img src="${base}/common/template/lottery/lecai/images/gclogo/${bly.code}.png" alt="" width="114" height="114">
					</c:otherwise>
				</c:choose>

				</div>
				<p class="time-title">
					<span id="openTitle">已开盘,距离封盘截止还有</span>
				</p>
				<div class="gct-time">
					<div class="gct-time-now-l" id="count_down">
					</div>
				</div>
				<h3 class="page_name">加载中...</h3>

				<div class="gct_now">
					<strong>第&nbsp;<span id="current_issue"
						class="color-green">加载中...</span>&nbsp;期
					</strong> <br> 总共:&nbsp;&nbsp;<strong><span id="current_sale"
						class="color-green">${qishu}</span></strong>&nbsp;&nbsp;期<br>
						<c:choose>
							<%-- <c:when test="${bly.code == 'LHC' || bly.code == 'SFLHC'}">
								<a href="${base}/lotteryV3/draw/hisresult.do?lotCode=${bly.code}" target="_blank" class="bt01">
							</c:when> --%>
							<c:when test="${bly.code == 'LHC' || bly.code == 'SFLHC' || bly.code == 'TMLHC' || bly.code == 'WFLHC' || bly.code == 'HKMHLHC'}">
								<a href="${base}/lotteryV3/trend.do?lotCode=${bly.code}" target="_blank" class="bt01">
							</c:when>
							<c:otherwise>
								<a href="${base}/lotteryV3/trend.do?lotCode=${bly.code}" target="_blank" class="bt01">
							</c:otherwise>
						</c:choose>

						<span class="zoushi"></span>号码走势</a>
				</div>
				<div class="clear"></div>
				<div class="gct_menu">
					<a class="gct_menu_yl" target="_blank"></a>
				</div>
			</div>
			<div id="showgd-box">
				<ul class="box-ul">
					<li><a attrid="#gd-box1" class="tabulous_active">近一期</a></li>
					<li><a class="" attrid="#gd-box2">近五期</a></li>
					<span class="tabulousclear"></span>
				</ul>
				<div id="tabs_container" style="height: 120px;" class="transition">
					<div id="gd-box1" style="position: absolute; top: 40px;"
						class="make_transist hideleft showleft">
						<div class="dsLotto-prev right">
							<!-- / 开奖(CQ/XJSSC+GX10) -->
							<div class="row dsLotto-prev-sn">
								<ul class="array ac cf">
									<li class="page_name">加载中...</li>
									<li>第<span class="red" id="last_qihao">加载中...</span>期开奖结果
									</li>
									<li style="width:50px;"><span class="red" id="readyOpen">开奖中...</span></li>
								</ul>
								<span title="关闭音效" class="dsLotto-block-sound"></span>
							</div>
							<c:choose>
								<c:when test="${bly.type == 51 || bly.type == 52 || bly.type == 55 || bly.type == 9 || bly.type == 14}">
										<jsp:include page="lotteryOpen/ssc.jsp"></jsp:include>
								</c:when>
								<c:when test="${bly.type == 53 || bly.type == 8}">
									<jsp:include page="lotteryOpen/bjsc.jsp"></jsp:include>
								</c:when>
								<c:when test="${bly.type == 54 || bly.type == 58 || bly.type == 10 || bly.type == 15}">
									<jsp:include page="lotteryOpen/k3.jsp"></jsp:include>
								</c:when>
								<c:when test="${bly.type == 57 || bly.type == 11}">
									<jsp:include page="lotteryOpen/pcegg.jsp"></jsp:include>
								</c:when>
								<c:when test="${bly.type == 12}">
									<c:choose>
										<c:when test="${bly.code == 'CQXYNC'}">
											<jsp:include page="lotteryOpen/cqxync.jsp"></jsp:include>
										</c:when>
										<c:otherwise>
											<jsp:include page="lotteryOpen/sfc.jsp"></jsp:include>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<jsp:include page="lotteryOpen/lhc.jsp"></jsp:include>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div id="gd-box2" class="hideleft make_transist" style="position: absolute; top: 40px;">
					</div>

				</div>
				<!--End tabs container-->
			</div>
			<div class="clear"></div>
		</div>
			<!-- <div class="jc-tab-out" style="position:relative;">
				<ul class="jc-tab-ul">
	               <li><h2 style="font-weight:normal;"><a class="active" href="javascript:;">选号投注</a></h2></li>
	               <c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}">
					<c:if test="${bly.code ne 'FC3D' && bly.code ne 'PL3' && bly.code ne 'LHC'}">
						<c:if test="${!isJoint}">
				               <li><h2 style="font-weight:normal;"><a href="#">参与合买</a></h2></li>
				               <li><h2 style="font-weight:normal;"><a href="#">我的方案</a></h2></li>
				               </c:if>
						</c:if>
					</c:if>
	           </ul>
			</div> -->
		<div class="tznavhd" id="tznavhd">
			<ul class="tznavhd-list" id="top_tab_list">
				<c:forEach items="${playGroupList}" var="group" varStatus="g">
					<li class="play_tab <c:if test="${g.first}">on</c:if>" value="${group.code}" tag="${group.id}" default="${group.stationId}">
						<h2>
							<a href="javascript://;" class="play_tab_first" title="标准选号">${group.name}</a>
						</h2>
					</li>
				</c:forEach>
				<!-- <li class="play_tab" id="top_tab_lucky"><h2>
						<a href="javascript://;" title="幸运选号">幸运选号</a>
					</h2></li> -->
			</ul>
			<!-- <div class="tznavhd_syndicate"><label class="for_checkbox"><input id="tab_syndicate" type="checkbox" />&nbsp;发起合买</label></div>
        	<div class="tznavhd_syndicate"><h2><a href="/hemai/?lottery_type=52" target="_blank">参与合买&gt;&gt;</a></h2></div> -->
			<!-- <div class="tznavhd_href">
				<a href="javascript:;" id="tznavhd_latest_draw"
					class="tznavhd_recentdraw">近期开奖<i class="ssqIcon-drop2"></i></a>
			</div> -->
			<c:if test="${bly.code eq 'BJSC' && saiCheSwfStatus eq 'on'}">
			<div class="tznavhd_href">
				<a href="javascript:;" id="saiCheSHiPinV3" swf="${saiCheSwfPath}" status="${saiCheSwfStatus}"
					class="tznavhd_recentdraw">赛车视频<i class="ssqIcon-drop2"></i></a>
			</div>
			</c:if>
			<div class="clear"></div>

		</div>
		<c:if test="${bly.code eq 'BJSC' && saiCheSwfStatus eq 'on'}">
			<jsp:include page="include/saiCheShiPin.jsp"></jsp:include>
		</c:if>
		<div class="trend_box" id="latest_draw_list"
			style="display: none;">
			<div class="latest_draw_list_out">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<thead>
						<tr>
							<td rowspan="2" width="116" class="border"><select
								id="choose_record_trend_time">
									<option value="5">近5期</option>
									<option value="10" selected="selected">近10期</option>
									<option value="20">近20期</option>
									<option value="30">近30期</option>
									<option value="50">近50期</option>
							</select> <i id="ssqIcon_sorts" class="hdicon icon-acending"></i></td>
							<td rowspan="2" width="50">开奖号</td>
							<td rowspan="2" width="50" class="border">试机号</td>
							<td rowspan="2" width="40" class="border">和值</td>
							<td colspan="10" class="border">
								<div class="fl">号码分布图</div>
								<div class="fl titbox">
									<span class="tit active">百位</span><span class="tit">十位</span><span
										class="tit">个位</span><span class="tit">不分位</span><i
										class="hdicon icon-ask" style="display: none;"></i>
									<div class="notetip" style="display: none; left: 230px;">
										<b class="t" style="left: 22px;"></b>
										<p>
											<i class="greenbox"></i>对子号&nbsp;&nbsp;&nbsp;&nbsp;<i
												class="bluebox"></i>豹子号
										</p>
									</div>
								</div>
							</td>
							<td colspan="4" class="border">大小比<i class="hdicon icon-ask"></i>
								<div class="notetip" style="display: none;">
									<b class="t"></b>
									<p>号码0-9中，0-4为小，5-9为大</p>
								</div>
							</td>
							<td colspan="4" class="border">奇偶比</td>
							<td colspan="3">类型</td>
						</tr>
						<tr>
							<td width="34">0</td>
							<td width="34">1</td>
							<td width="34">2</td>
							<td width="34">3</td>
							<td width="34">4</td>
							<td width="34">5</td>
							<td width="34">6</td>
							<td width="34">7</td>
							<td width="34">8</td>
							<td width="34" class="border">9</td>
							<td width="34">3:0</td>
							<td width="34">2:1</td>
							<td width="34">1:2</td>
							<td width="34" class="border">0:3</td>
							<td width="34">3:0</td>
							<td width="34">2:1</td>
							<td width="34">1:2</td>
							<td width="34" class="border">0:3</td>
							<td width="34">组三</td>
							<td width="34">组六</td>
							<td width="34">豹子</td>
						</tr>
					</thead>
					<tbody id="latest_draw_list_body">
						<tr>
							<td class="border">2017012</td>
							<td>&nbsp;<span class="red bold">1</span>&nbsp;<span
								class="red bold">2</span>&nbsp;<span class="red bold">5</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>7</span>&nbsp;<span>7</span>&nbsp;<span
								class="red">5</span>&nbsp;
							</td>
							<td class="border"><span class="bold">8</span></td>
							<td><span class="grey">18</span></td>
							<td><span class="ballbg pinkbg">1</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">8</span></td>
							<td><span class="grey">14</span></td>
							<td><span class="grey">26</span></td>
							<td><span class="grey">9</span></td>
							<td><span class="grey">5</span></td>
							<td><span class="grey">6</span></td>
							<td class="border"><span class="grey">1</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">1</span></td>
							<td class="blue">1:2</td>
							<td class="border"><span class="grey">3</span></td>
							<td><span class="grey">8</span></td>
							<td class="purple">2:1</td>
							<td><span class="grey">9</span></td>
							<td class="border"><span class="grey">3</span></td>
							<td><span class="grey">1</span></td>
							<td class="red">组六</td>
							<td><span class="grey">6</span></td>
						</tr>
						<tr>
							<td class="border">2017013</td>
							<td>&nbsp;<span class="red bold">5</span>&nbsp;<span
								class="red bold">8</span>&nbsp;<span class="red bold">2</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>9</span>&nbsp;<span>9</span>&nbsp;<span>1</span>&nbsp;
							</td>
							<td class="border"><span class="bold">15</span></td>
							<td><span class="grey">19</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="grey">9</span></td>
							<td><span class="grey">15</span></td>
							<td><span class="ballbg pinkbg">5</span></td>
							<td><span class="grey">10</span></td>
							<td><span class="grey">6</span></td>
							<td><span class="grey">7</span></td>
							<td class="border"><span class="grey">2</span></td>
							<td><span class="grey">3</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">1</span></td>
							<td class="border"><span class="grey">4</span></td>
							<td><span class="grey">9</span></td>
							<td><span class="grey">1</span></td>
							<td class="purple">1:2</td>
							<td class="border"><span class="grey">4</span></td>
							<td><span class="grey">2</span></td>
							<td class="red">组六</td>
							<td><span class="grey">7</span></td>
						</tr>
						<tr>
							<td class="border">2017014</td>
							<td>&nbsp;<span class="red bold">2</span>&nbsp;<span
								class="red bold">2</span>&nbsp;<span class="red bold">1</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>8</span>&nbsp;<span>9</span>&nbsp;<span>8</span>&nbsp;
							</td>
							<td class="border"><span class="bold">5</span></td>
							<td><span class="grey">20</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="ballbg pinkbg">2</span></td>
							<td><span class="grey">10</span></td>
							<td><span class="grey">16</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">11</span></td>
							<td><span class="grey">7</span></td>
							<td><span class="grey">8</span></td>
							<td class="border"><span class="grey">3</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">2</span></td>
							<td class="border blue">0:3</td>
							<td><span class="grey">10</span></td>
							<td><span class="grey">2</span></td>
							<td class="purple">1:2</td>
							<td class="border"><span class="grey">5</span></td>
							<td class="red">组三</td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">8</span></td>
						</tr>
						<tr>
							<td class="border">2017015</td>
							<td>&nbsp;<span class="red bold">9</span>&nbsp;<span
								class="red bold">5</span>&nbsp;<span class="red bold">0</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>4</span>&nbsp;<span>6</span>&nbsp;<span
								class="red">0</span>&nbsp;
							</td>
							<td class="border"><span class="bold">14</span></td>
							<td><span class="grey">21</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">11</span></td>
							<td><span class="grey">17</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">12</span></td>
							<td><span class="grey">8</span></td>
							<td><span class="grey">9</span></td>
							<td class="border"><span class="ballbg pinkbg">9</span></td>
							<td><span class="grey">5</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">3</span></td>
							<td class="border"><span class="grey">1</span></td>
							<td><span class="grey">11</span></td>
							<td class="purple">2:1</td>
							<td><span class="grey">1</span></td>
							<td class="border"><span class="grey">6</span></td>
							<td><span class="grey">1</span></td>
							<td class="red">组六</td>
							<td><span class="grey">9</span></td>
						</tr>
						<tr>
							<td class="border">2017016</td>
							<td>&nbsp;<span class="red bold">4</span>&nbsp;<span
								class="red bold">6</span>&nbsp;<span class="red bold">9</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>8</span>&nbsp;<span>3</span>&nbsp;<span>2</span>&nbsp;
							</td>
							<td class="border"><span class="bold">19</span></td>
							<td><span class="grey">22</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">12</span></td>
							<td><span class="ballbg pinkbg">4</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">13</span></td>
							<td><span class="grey">9</span></td>
							<td><span class="grey">10</span></td>
							<td class="border"><span class="grey">1</span></td>
							<td><span class="grey">6</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">4</span></td>
							<td class="border"><span class="grey">2</span></td>
							<td><span class="grey">12</span></td>
							<td><span class="grey">1</span></td>
							<td class="purple">1:2</td>
							<td class="border"><span class="grey">7</span></td>
							<td><span class="grey">2</span></td>
							<td class="red">组六</td>
							<td><span class="grey">10</span></td>
						</tr>
						<tr>
							<td class="border">2017017</td>
							<td>&nbsp;<span class="red bold">8</span>&nbsp;<span
								class="red bold">1</span>&nbsp;<span class="red bold">7</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>9</span>&nbsp;<span>2</span>&nbsp;<span>3</span>&nbsp;
							</td>
							<td class="border"><span class="bold">16</span></td>
							<td><span class="grey">23</span></td>
							<td><span class="grey">5</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">13</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="grey">14</span></td>
							<td><span class="grey">10</span></td>
							<td><span class="ballbg pinkbg">8</span></td>
							<td class="border"><span class="grey">2</span></td>
							<td><span class="grey">7</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">5</span></td>
							<td class="border"><span class="grey">3</span></td>
							<td><span class="grey">13</span></td>
							<td class="purple">2:1</td>
							<td><span class="grey">1</span></td>
							<td class="border"><span class="grey">8</span></td>
							<td><span class="grey">3</span></td>
							<td class="red">组六</td>
							<td><span class="grey">11</span></td>
						</tr>
						<tr>
							<td class="border">2017018</td>
							<td>&nbsp;<span class="red bold">3</span>&nbsp;<span
								class="red bold">1</span>&nbsp;<span class="red bold">5</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>4</span>&nbsp;<span>0</span>&nbsp;<span>0</span>&nbsp;
							</td>
							<td class="border"><span class="bold">9</span></td>
							<td><span class="grey">24</span></td>
							<td><span class="grey">6</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="ballbg pinkbg">3</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">5</span></td>
							<td><span class="grey">15</span></td>
							<td><span class="grey">11</span></td>
							<td><span class="grey">1</span></td>
							<td class="border"><span class="grey">3</span></td>
							<td><span class="grey">8</span></td>
							<td><span class="grey">1</span></td>
							<td class="blue">1:2</td>
							<td class="border"><span class="grey">4</span></td>
							<td class="purple">3:0</td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">2</span></td>
							<td class="border"><span class="grey">9</span></td>
							<td><span class="grey">4</span></td>
							<td class="red">组六</td>
							<td><span class="grey">12</span></td>
						</tr>
						<tr>
							<td class="border">2017019</td>
							<td>&nbsp;<span class="red bold">6</span>&nbsp;<span
								class="red bold">0</span>&nbsp;<span class="red bold">6</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>7</span>&nbsp;<span>7</span>&nbsp;<span>2</span>&nbsp;
							</td>
							<td class="border"><span class="bold">12</span></td>
							<td><span class="grey">25</span></td>
							<td><span class="grey">7</span></td>
							<td><span class="grey">5</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">6</span></td>
							<td><span class="ballbg pinkbg">6</span></td>
							<td><span class="grey">12</span></td>
							<td><span class="grey">2</span></td>
							<td class="border"><span class="grey">4</span></td>
							<td><span class="grey">9</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">1</span></td>
							<td class="border"><span class="grey">5</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">3</span></td>
							<td class="border purple">0:3</td>
							<td class="red">组三</td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">13</span></td>
						</tr>
						<tr>
							<td class="border">2017020</td>
							<td>&nbsp;<span class="red bold">8</span>&nbsp;<span
								class="red bold">9</span>&nbsp;<span class="red bold">3</span>&nbsp;
							</td>
							<td class="border">&nbsp;<span>2</span>&nbsp;<span>5</span>&nbsp;<span>7</span>&nbsp;
							</td>
							<td class="border"><span class="bold">20</span></td>
							<td><span class="grey">26</span></td>
							<td><span class="grey">8</span></td>
							<td><span class="grey">6</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">4</span></td>
							<td><span class="grey">7</span></td>
							<td><span class="grey">1</span></td>
							<td><span class="grey">13</span></td>
							<td><span class="ballbg pinkbg">8</span></td>
							<td class="border"><span class="grey">5</span></td>
							<td><span class="grey">10</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">2</span></td>
							<td class="border"><span class="grey">6</span></td>
							<td><span class="grey">2</span></td>
							<td class="purple">2:1</td>
							<td><span class="grey">4</span></td>
							<td class="border"><span class="grey">1</span></td>
							<td><span class="grey">1</span></td>
							<td class="red">组六</td>
							<td><span class="grey">14</span></td>
						</tr>
						<tr class="yellowbg">
							<td class="border">2017021 <span class="red">上一期</span></td>
							<td><span class="red bold">8</span>&nbsp;<span
								class="red bold">5</span>&nbsp;<span class="red bold">4</span>&nbsp;</td>
							<td class="border">&nbsp;<span>6</span>&nbsp;<span>1</span>&nbsp;<span>6</span>&nbsp;
							</td>
							<td class="border"><span class="bold">17</span></td>
							<td><span class="grey">27</span></td>
							<td><span class="grey">9</span></td>
							<td><span class="grey">7</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">5</span></td>
							<td><span class="grey">8</span></td>
							<td><span class="grey">2</span></td>
							<td><span class="grey">14</span></td>
							<td><span class="ballbg pinkbg">8</span></td>
							<td class="border"><span class="grey">6</span></td>
							<td><span class="grey">11</span></td>
							<td class="blue">2:1</td>
							<td><span class="grey">3</span></td>
							<td class="border"><span class="grey">7</span></td>
							<td><span class="grey">3</span></td>
							<td><span class="grey">1</span></td>
							<td class="purple">1:2</td>
							<td class="border"><span class="grey">2</span></td>
							<td><span class="grey">2</span></td>
							<td class="red">组六</td>
							<td><span class="grey">15</span></td>
						</tr>
					</tbody>
				</table>
				<div>
					<i id="latest_deaw_list_hide" class="hdicon icon-uparrow"></i>
				</div>
			</div>

		</div>
		<div class="tzmain" id="normal_area">
			<!-- <div class="layui-layer-shade" style="z-index:19891014; background-color:#000; opacity:0.3; filter:alpha(opacity=30);"></div> -->
			<c:if test="${bly.code eq 'LHC' || bly.code eq 'SFLHC' || bly.code eq 'TMLHC' || bly.code eq 'WFLHC' || bly.code eq 'HKMHLHC' || lotVersion eq 5}">
				<jsp:include page="lottery/lhc_v3.jsp"></jsp:include>
			</c:if>
			<c:if test="${bly.code ne 'LHC' && bly.code ne 'SFLHC' && bly.code ne 'TMLHC' && bly.code ne 'WFLHC' && bly.code ne 'HKMHLHC' && lotVersion ne 5}">
				<jsp:include page="lottery/other_v3.jsp"></jsp:include>
			</c:if>
		</div>
	</div>
	<div class="rightFloat" style="position: fixed; top: 326px;">
		<div class="fixed">
			<ul>
				<li class="first"><a id="tplHowToPlay" class="play"
					href="${base}/lotteryV3/rule.do?code=${bly.code}" target="_blank"><i></i><span>玩法说明</span></a></li>
				<c:if test="${bly.code ne 'LHC' && bly.code ne 'SFLHC' && bly.code ne 'WFLHC' && bly.code ne 'TMLHC' && bly.code ne 'HKMHLHC' && lotVersion ne 5}">
				 <li><a id="showNewbieGuide" class="guide" onclick="jjdz();"
					href="javascript://"><i></i><span>奖金对照</span></a></li></c:if>
				<%--<li class="second"><a class="feedback"
					href="javascript://"><i></i><span>意见反馈</span></a></li> --%>
				<li class="last"><a class="return" id="scroll_top" onclick="_Core.utils.back_to_top();"
					href="javascript://"><i></i>返回顶部</a></li>
			</ul>
		</div>
	</div>
<!-- 选中 -->
<audio id="sound_bet_select" src="${base}/common/template/lottery/lecai/js/mp3/bet_select.mp3" controls="controls" hidden="true"></audio>
<!-- 倒计时 -->
<audio id="sound_ding" src="${base}/common/template/lottery/lecai/js/mp3/ding.mp3" controls="controls" hidden="true"></audio>
<!-- 新开奖 -->
<audio id="sound_newkj" src="${base}/common/template/lottery/lecai/js/mp3/newkj.mp3" controls="controls" hidden="true"></audio>
<!-- 投注页弹窗 -->

<c:if test="${domainFolder != 'x00204'}"><jsp:include page="include/dialog_gonggao.jsp"><jsp:param value="index" name="dialog_gg"/></jsp:include></c:if>
<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/echarts/dist/echarts.js"></script>
<script type="text/javascript">
	//路径配置
	require.config({
	   paths: {
	       echarts: '${base}/common/template/lottery/lecai/js/echarts/dist'
	   }
	});

	function jjdz(){
		layer.open({
			type : 2, //page层
			area : [ '400px', '500px' ],
			skin:'layui-layer-rim',
			title : $("h3.page_name").text()+'奖金对照列表',
			shade : 0.6, //遮罩透明度
			scrollbar : false,
			offset : '100px',//距离顶部
			moveType : 0, //拖拽风格，0是默认，1是传统拖动
			shift : 0, //0-6的动画形式，-1不开启
			content : '${base}/lotteryV3/lotteryBonus.do?lotCode='+LECAI.code
		});
	}
	//判断大小单双加上样式
	// $('#gd-box2').change(function(){
	// 	for(let i=0 ;i<$('#gd-box2 p').length;i++) {
	// 		if ($('#gd-box2 p').eq(i).children().last().text() == '单') {
	// 			$('#gd-box2 p').eq(i).children().last().css({
	// 				'background': 'blue',
	// 				'color': '#ffffff'
	// 			})
	// 		} else if($('#gd-box2 p').eq(i).children().last().text() == '双'){
	// 			$('#gd-box2 p').eq(i).children().last().css({
	// 				'background': 'red',
	// 				'color': '#ffffff'
	// 			})
	// 		}
	// 		if ($('#gd-box2 p').eq(i).children().eq(-2).text() == '大') {
	// 			$('#gd-box2 p').eq(i).children().eq(-2).css({
	// 				'background': '#315a2c',
	// 				'color': '#ffffff'
	// 			})
	// 		} else if($('#gd-box2 p').eq(i).children().eq(-2).text() == '小'){
	// 			$('#gd-box2 p').eq(i).children().eq(-2).css({
	// 				'background': '#94ff05',
	// 				'color': '#ffffff'
	// 			})
	// 		}
	//
	// 	}
	//
	// })


</script>
</body>
</html>
