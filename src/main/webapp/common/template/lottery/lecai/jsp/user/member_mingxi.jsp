<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<%--<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>--%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 账号信息</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">
					您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label>
					<a id="user_level" href="javascript:void(0);" 
						title=""><!-- <img
						src="${base}/common/template/lottery/lecai/images/member/cb_0.gif"> --></a>
				</div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong
							id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}
					</span> <img id="balances_arrow"
						src="${base}/common/template/lottery/lecai/images/member/arrow_8x4_down.gif"
						style="display: none;">
				</div>
				<a class="btn recharge normal" value=" " href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" value=" "
					href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="account">
				<div class="tit">
					<div class="account_switch" id="wallet_switch"
						style="min-width: 84.4px;">
						<label><strong id="overview" style="cursor: default;">账户总览</strong></label>
						<img src="${base}/common/template/lottery/lecai/images/member/arrow_9x5_down.gif">
					</div>
				</div>
				<ul class="clearfix" id="wallet_info">
					<c:if test="${stationFolder ne 'v0012'}">
					<li class="star" style="width:390px;">充值总额：<label class="red">${comTotalMoney}</label></li></c:if>
					<li class="end" style="width:390px;">提款总额：<label class="red">${drawTotalMoney}</label></li>
				</ul>
			</div>
			<div class="detail_explain">
				<ul>
					<li>为安全考虑，订单生成之后系统会自动冻结您的交易金额，直至交易结束。</li>
				</ul>
			</div>
			<div class="detail">
				<div class="tab">
					<ul>
						<c:if test="${onlinePay eq 'on' }">
						<li class="on" attrType="1"><a href="javascript:void(0);"><span
								class="left"></span><span class="bg content">充值记录</span><span
								class="right"></span></a></li>
						</c:if>
						<li attrType="2"><a href="javascript:void(0);"><span
								class="left"></span><span class="bg content">取款记录</span><span
								class="right"></span></a></li>
						<c:if test="${changeMoney eq 'on'}">
						<li attrType="3"><a href="javascript:void(0);"><span
								class="left"></span><span class="bg content">帐变记录</span><span
								class="right"></span></a></li></c:if>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="filter">
					<ul>
						<li class="attr_type_1">
							<div class="category">
								<select name="sort_select" id="type" class="chzn-done">
									<option value="">所有类型</option>
									<option value="5">在线存款</option>
									<option value="6">快速入款</option>
									<option value="7">一般存款</option>
									<option value="801">一般存款</option>
								</select>
							</div>
						</li>
						<li class="attr_type_1 attr_type_2"><select
							name="selected_time" class="time-select chzn-done" id="state">
								<option value="">所有状态</option>
								<option value="1">处理中</option>
								<option value="2">处理成功</option>
								<option value="3">处理失败</option>
								<option value="4">已取消</option>
								<option value="5">已过期</option>
						</select></li>
						<li class="attr_type_3"><input class="inputText" id="orderId"
							value="" type="text" placeholder="订单号"></li>
						<li>
							<div class="time-piece">
								<input name="ds" id="startTime" class="text text75"
									readonly="readonly" value="${startTime }" type="text" data-end-date="${endTime}">
								<span>至</span> <input name="de" id="endTime" class="text text75"
									readonly="readonly" value="${endTime}" type="text" data-end-date="${endTime}"> <input
									class="btn normal" type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="data" id="record_tb">
				</table>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script id="record_tpl" type="text/html">
				<thead>
					<tr class="tit">
						{{each colmuns as col}}
							{{if col=='备注'}}
								<th width="300">{{col}}</th>
							{{else if col=='日期'}}
								<th width="130">{{col}}</th>
							{{else if col=='变动前金额'}}
								<th width="76">{{col}}</th>
							{{else if col=='变动金额'}}
								<th width="76">{{col}}</th>
							{{else if col=='变动后余额'}}
								<th width="76">{{col}}</th>
							{{else if col=='交易类别'}}
								<th width="120">{{col}}</th>
							{{else if col=='订单号'}}
								<th width="90">{{col}}</th>
							{{else}}
								<th>{{col}}</th>
							{{/if}}
						{{/each}}
					</tr>
				</thead>
				<tbody>
					{{each list as row count}}
					<tr {{if count>0 && count%2!=0}}class="bggrey"{{/if}}>
						{{each navKeys as key}}
							<td>{{$formatter row[key] key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
</script>
	<script type="text/javascript">
		var colmuns = [ "日期", "交易类别" ];
		var navCols = {
			1 : [ "支付名称", "交易额度", "状态", "说明","订单号" ],
			2 : [ "交易额度", "状态", "说明" ],
			3 : [ "变动前金额", "变动金额", "变动后余额", "订单号", "备注" ]
		}
		var navKeys = {
			1 : [ "createDatetime", "type", "payName", "money", "status",
					"opDesc","orderNo" ],
			2 : [ "createDatetime", "type", "drawMoney", "status", "remark" ],
			3 : [ "createDatetime", "type", "beforeMoney", "money",
					"afterMoney", "orderId", "remark" ]
		}
		var navUrls = {
			1 : "${base}/center/record/hisrecord/comrd.do",
			2 : "${base}/center/record/hisrecord/drawrd.do",
			3 : "${base}/center/record/mnyrecord/list.do"
		}

		function search() {
			initRdsData(curNavFlag);
		}

		var curNavFlag = ${type};
		var first = true;
		$(function() {
			$('.tab ul li[attrtype='+curNavFlag+']').addClass("on").siblings().removeClass();
			initRdsData(curNavFlag);
			$("#type").chosen({
				disable_search : true
			});
			$("#state").chosen({
				disable_search : true
			});
			first = false;
			$('#startTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			$('#endTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			switch (parseInt(curNavFlag)) {
			case 1:
				$(".tabs").find("div:eq(0)").addClass("on");
				$(".attr_type_3").hide();
				$(".attr_type_1").show();
				break;
			case 2:
				$(".tabs").find("div:eq(1)").addClass("on");
				$(".attr_type_1,.attr_type_3").hide();
				$(".attr_type_2").show();
				break;
			case 3:
				$(".tabs").find("div:eq(2)").addClass("on");
				$(".attr_type_1,attr_type_2").hide();
				$(".attr_type_3").show();
				break;
			}

			$(".tab ul li").click(function() {
				var attrType = parseInt($(this).attr("attrType"));
				switch (attrType) {
				case 1:
					$(".attr_type_3").hide();
					$(".attr_type_1").show();
					break;
				case 2:
					$(".attr_type_1,.attr_type_3").hide();
					$(".attr_type_2").show();
					break;
				case 3:
					$(".attr_type_1,attr_type_2").hide();
					$(".attr_type_3").show();
					break;
				}
				$("#pageNumber").val(1); //重新初始化
				$(this).addClass("on").siblings().removeClass("on");
				initRdsData(attrType);
			})
		})

		template.helper('$formatter', function(content, key) {
			console.log('ff',content);
			console.log('sss',key);
			if (key == "createDatetime") {
				return DateUtil.formatDatetime(content);
			} else if (key == "type") {
				return GlobalTypeUtil.getTypeName(1, 1, content);
			} else if (key == "status") {
				if (curNavFlag == 1) {
					return GlobalTypeUtil.getTypeName(1, 2, content);
				} else if (curNavFlag == 2) {
					return GlobalTypeUtil.getTypeName(1, 3, content);
				}
			} else if (key.toUpperCase().indexOf("MONEY") != -1) {
				if (content > 0) {
					return '<font color="blue">' + content + '</font>';
				} else {
					return '<font color="red">' + content + '</font>';
				}
			}
			return content;
		});
		var first = true;
		function initRdsData(flag) {
			curNavFlag = flag;
			var pageSize = 10;
			var pageNumber = 1;
			if (!first) {
				pageNumber = parseInt($("#pageNumber").val());
			}
			var type = 0;
			var status = $("#state").val();
			var orderId = $("#orderId").val();
			switch (parseInt(flag)) {
			case 1:
				type = $("#type").val();
				break;
			case 2:
				$("#type").val('');
				break;
			case 3:
				orderId = $("#orderId").val();
				break;
			}
			$.ajax({
				type : "POST",
				url : navUrls[flag],
				data : {
					startTime : $("#startTime").val(),
					endTime : $("#endTime").val(),
					pageNumber : pageNumber,
					pageSize : pageSize,
					type : type,
					status : status,
					orderId : orderId
				},
				success : function(result) {
					layerPage(result.totalCount,result.currentPageNo,result.totalPageCount); //分页填充参数
					result.colmuns = getNewCols();
					result.navKeys = navKeys[flag];
					var html = template('record_tpl', result);
					$("#record_tb").html(html);
				}
			});
		}

		function getNewCols() {
			var newCols = [];
			for (var i = 0; i < colmuns.length; i++) {
				newCols.push(colmuns[i]);
			}
			for (var i = 0; i < navCols[curNavFlag].length; i++) {
				newCols.push(navCols[curNavFlag][i]);
			}
			return newCols;
		}

	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>