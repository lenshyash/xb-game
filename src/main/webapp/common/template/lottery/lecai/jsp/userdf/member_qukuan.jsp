<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.userinfo-edit-main{
		border:none;
	}
	.liutextbox .for-cur em{
		background:#4aa9db;
	}
	.liulist.for-cur{
		background:#4aa9db;
	}
	.liutextbox .for-cur strong{
		color:#4aa9db;
	}
	.userinfo-edit-main input{
		font-size:14px;height:36px!important;width:280px!important;border:1px solid #e7e8e8!important;background-color:#feffff;color:#a9a9a9;padding-left:10px;margin:0;vertical-align:top!important
	}
	.nextBtn a{
		background:url(/common/template/lottery/lecai/images/member/recharge_new2.png) 0 0 no-repeat!important;
	}
	#savebank{
		margin-top:20px;
	}
	 .gmbtn .sub_btn.normal {
    	background: url(/common/template/lottery/lecai/images/member/draw_btns.gif) 0 0 no-repeat;
    	border:none!important;
	}
	 .gmbtn .sub_btn:hover {
	    background: url(/common/template/lottery/lecai/images/member/draw_btns.gif) 0 -41px no-repeat;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
				<jsp:include page="member_left_nav.jsp"></jsp:include>	
		<div class="userRight">
            <div class="userTitle mgb10">提款中心</div>
            <div class="userMain mgb10">
           			<c:if test="${isGuest }">
					<div class="userinfo-edit-main">试玩账号不能提款</div>
					</c:if>
					<c:if test="${!isGuest }">
					<!-- 取款密码设置页面 -->
					<c:if test="${type eq '1' }">
						<jsp:include page="drawStep/drawPwd.jsp"></jsp:include>
					</c:if>
					<!-- 信息填写页面 -->
					<c:if test="${type eq '2' }">
						<jsp:include page="drawStep/drawData.jsp"></jsp:include>
					</c:if>
					<!-- 提款页面 -->
					<c:if test="${type eq '3' }">
						<jsp:include page="drawStep/drawCommit.jsp"></jsp:include>
					</c:if>
					</c:if>
            </div>
        </div>
		</div>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>