<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="header">
	<div class="buttons left">
		</label> <label class="quickAmount"><span class="color_lv bold">预设金额</span>
		<input class="quickmoney" maxlength="6" type="text" /> </label>
		<button class="button commitbtn" type="button">确定</button>
		<button class="resetbtn button" type="button">重置</button>
	</div>
	<div class="_setting left" <c:if test="${param.kj eq false}">id="_settingFooter"</c:if>>
		<a class="chouma"> <i class="ui-icon-group__icon"></i><span
			class="ui-icon-group__txt">筹码设置</span>
		</a>
<%--		<c:if test="${domainFolder eq 'd00528'}">--%>
<%--			<script>--%>
<%--				var dAloneHtml = '<select><option label="5" value="5">5</option><option label="8" value="8">8</option></select>'--%>
<%--				$(function () {--%>
<%--					$("._group .i a").text(5).attr("value",5);--%>
<%--					$(".__form .i").html(dAloneHtml)--%>
<%--				})--%>
<%--				console.log('${domainFolder}')--%>
<%--			</script>--%>
<%--			<style>--%>
<%--				.__form ul li:nth-child(1) {--%>
<%--					display: none;--%>
<%--				}--%>
<%--				.dAlone{--%>
<%--					display: block;--%>
<%--				}--%>
<%--			</style>--%>
<%--		</c:if>--%>
		<div class="__form">
			<ul>
				<li class="i"><select>
						<option label="1" value="1">1</option>
						<option label="2" value="2">2</option>
						<option label="5" value="5">5</option>
						<option label="8" value="8">8</option>
				</select></li>
				<li class="ii"><select>
						<option label="1" value="10">10</option>
						<option label="2" value="20">20</option>
						<option label="5" value="50">50</option>
						<option label="8" value="80">80</option>
				</select></li>
				<li class="iii"><select>
						<option label="1" value="100">1百</option>
						<option label="2" value="200">2百</option>
						<option label="5" value="500">5百</option>
						<option label="8" value="800">8百</option>
				</select></li>
				<li class="iv"><select>
						<option label="1" value="1000">1千</option>
						<option label="2" value="2000">2千</option>
						<option label="5" value="5000">5千</option>
						<option label="8" value="8000">8千</option>
				</select></li>
				<li class="v"><select>
						<option label="1" value="10000">1万</option>
						<option label="2" value="20000">2万</option>
						<option label="5" value="50000">5万</option>
						<option label="8" value="80000">8万</option>
				</select></li>
			</ul>
			<div class="___tips">自定义筹码数额，自动保存后生效</div>
		</div>
	</div>
	<div class="_group left" <c:if test="${param.kj eq false}">id="_groupFooter"</c:if>>
		<ul>
			<li class="i" ng-class="i"><a value="1">1</a></li>
			<li class="ii" ng-class="ii"><a value="10">10</a></li>
			<li class="iii active" ng-class="iii"><a value="100">1百</a></li>
			<li class="iv" ng-class="iv"><a value="1000">1千</a></li>
			<li class="v" ng-class="v"><a value="10000">1万</a></li>
		</ul>
	</div>
	<c:if test="${param.kj eq true}">
		<div class="_rigBtn left">
			<ul>
				<li><span class="burger burger-1"></span><span
					class="burger burger-2"></span><span class="burger burger-3"></span></li>
			</ul>
			<div class="_kj_btn" <c:if test="${bly.code != 'SFLHC' && bly.code != 'LHC' && bly.code != 'WFLHC' && bly.code != 'TMLHC' && bly.code != 'HKMHLHC'}">style="width:167px;"</c:if>>
				<ul>
					<c:choose>
						<c:when test="${bly.code == 'SFLHC' || bly.code == 'LHC' || bly.code == 'TMLHC' || bly.code == 'WFLHC' || bly.code == 'HKMHLHC'}">
							<li class="i" dataid="lmcl" onclick="Mark.lotQueue(this);">两面长龙</li>
							<li class="ii cur" dataid="kjtz">快捷投注</li>
						</c:when>
						<c:otherwise>
							<li class="i cur" dataid="lmcl" onclick="Mark.lotQueue(this);">两面长龙</li>
						</c:otherwise>
					</c:choose>
					<li class="iii" dataid="zxzd" onclick="Mark.doOrder(this);">最新注单</li>
				</ul>
			</div>
			</li>
		</div>
	</c:if>
</div>