<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<strong class="p1" id="jj">简介：</strong>
<c:if test="${param.code == 'BJSC'}">
<p class="p1">“PK拾” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10
	个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2
	号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。</p>
	<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">北京赛车(PK10)</td>
			<td bgcolor="#FFFFFF">09:30-23:50(北京时间) </td>
			<td bgcolor="#FFFFFF">01-44 </td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'SFSC'}">
<p class="p1">“极速赛车” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10
	个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2
	号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。</p>
	<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">极速赛车</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间) </td>
			<td bgcolor="#FFFFFF">001-480 </td>
			<td bgcolor="#FFFFFF">每3分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'XYFT'}">
<p class="p1">“幸运飞艇” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10
	个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2
	号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。</p>
	<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">幸运飞艇</td>
			<td bgcolor="#FFFFFF">13:04-04:04(北京时间) </td>
			<td bgcolor="#FFFFFF">001-180 </td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'LXYFT'}">
<p class="p1">“老幸运飞艇” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10
	个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2
	号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。</p>
	<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">幸运飞艇</td>
			<td bgcolor="#FFFFFF">13:04:05-04:04:05(北京时间) </td>
			<td bgcolor="#FFFFFF">001-180 </td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'AZXY10'}">
<p class="p1">“老幸运飞艇” 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10
	个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2
	号位置，依次类推， 直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。</p>
	<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">澳洲幸运10</td>
			<td bgcolor="#FFFFFF">00:04:00-23:59:00(北京时间) </td>
			<td bgcolor="#FFFFFF">001-288 </td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<p class="p2">
	<br />
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dwd">定位胆</strong>
<p class="p1">从第一名到第十名任意位置上选择1个或1个以上号码。&nbsp;</p>
<p class="p1">从第一名到第十名任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
<p class="p1">投注方案： 第一名01</p>
<p class="p1">开奖号码:第一名01，即中定位胆第一名。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zx_fs">前三复式</strong>
<p class="p1">从第一名、第二名、第三名中各选择1个号码组成一注。&nbsp;</p>
<p class="p1">选号与开奖号码按位猜对3位即中奖。</p>
<p class="p1">&nbsp;投注方案： 01 02 03</p>
<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
<p class="p1">即中前三复式</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx_fs">前二复式</strong>
<p class="p1">从第一名、第二名中至少选择1个号码组成一注。&nbsp;</p>
<p class="p1">选号与开奖号码按位猜对2位即中奖。</p>
<p class="p1">&nbsp;投注方案：01 02</p>
<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
<p class="p1">即中前二复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q1zx_fs">前一复式</strong>
<p class="p1">从第一名中至少选择1个号码组成一注。</p>
<p class="p1">选号与开奖号码按位猜对1位即中奖。</p>
<p class="p1">投注方案：01</p>
<p class="p1">开奖号码:01 02 03 04 05 06 07 08 09 10</p>
<p class="p1">即中前一直选。</p>
<p>
	<br />
</p>
<strong id="dxds">
	<span>冠亚和值--大小单双</span>
</strong>
<p>
	<span>和值的数字为大小单双</span>
</p>
<p>
	<span>和值数字3-10为小，12-19为大，奇数为单，偶数为双 ${dxSetting } ${dsSetting }</span>
</p>
<p>
	<span>所选大小单双与开奖和值相符，即为中奖</span>
</p>
<p>
	<br />
</p>
<strong id="gyhz">
	<span>冠亚和值--和值</span>
</strong>
<p>
	<span>从3-19中任意选择1个或1个以上号码。</span>
</p>
<p>
	<span>所选数值等于开奖号码前两位相加之和，即为中奖。</span>
</p>
<p>
	<span>投注方案：和值15开奖号码前两位：0807或0906或1005，即中冠亚和值。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="longhu_gunjun">
	<span>冠军龙虎</span>
</strong>
<p>
	<span>选择龙虎作为投注号码</span>
</p>
<p>
	<span>“第一名”号码大于“第十名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
</p>
<p>
	<span>投注方案：龙，开奖号码：10********01</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="longhu_yajun">
	<span>亚军龙虎</span>
</strong>
<p>
	<span>选择龙虎作为投注号码</span>
</p>
<p>
	<span>“第二名”号码大于“第九名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
</p>
<p>
	<span>投注方案：龙，开奖号码：*10******01*</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="longhu_yajun">
	<span>季军龙虎</span>
</strong>
<p>
	<span>选择龙虎作为投注号码</span>
</p>
<p>
	<span>“第三名”号码大于“第八名”号码视为【龙】中奖、反之小于视为【虎】中奖，其余情形视为不中奖。</span>
</p>
<p>
	<span>投注方案：龙，开奖号码：**10****01**</span>
</p>
<p>
	<span><br /></span>
</p>
