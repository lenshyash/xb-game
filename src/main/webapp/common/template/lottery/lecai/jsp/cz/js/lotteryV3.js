var bet_result = {};
var selectedLotLastQuartz,r_t1;;	//定时器
$(function(){
	_Core.init();	// 初始化
	
	//滚动条监控
//	$(window).scroll(function() {//top=268
//		var top = $(window).scrollTop();
//        if (top > 0) {
//        	$(".rightFloat ul li.second").removeClass("last");
//            $("#scroll_top").slideDown(300);
//        } else {
//            $("#scroll_top").slideUp(300,function(){
//            	$(".rightFloat ul li.second").addClass("last");
//            });
//        }
//        //倒计时监控
//        if(top>268){
//        	//显示备用倒计时
//        }
//    });
	
	// 所有文本只能输入数字
	$(".hd_main").on("keydown","input[type=text]",function(e){
		var code = null;
		// 只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 // 48 - 57 上排数字键 96 - 105 数字键盘 8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {  
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	
	var $main = $("#lecaiMain");
	$("#showgd-box").on("click",".box-ul li a",function(){
		$(this).parent().parent().find("a").removeClass("tabulous_active");
		$(this).addClass("tabulous_active");
		var id = $(this).attr("attrid").replace("#","");
		$("#tabs_container").find("#"+id).addClass("showleft").siblings().removeClass("showleft");
	}).on("click",".dsLotto-block-sound",function(){
		if($(this).hasClass("status--off")){
			$(this).removeClass("status--off");
			_Core.audio_pause=false;
		}else{
			$(this).addClass("status--off");
			_Core.audio_pause=true;
		}
	})
	
	var hd_main = $(".hd_main");
	hd_main.on("click","#tznavhd_latest_draw",function(){	// 近期开奖
		//_Core.utils.loading("#area_content",30000);return false;
		layer.msg("敬请期待",{icon:1,offset:['30%']});return false;
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$("#latest_draw_list").slideUp();
		}else{
			$(this).addClass("active");
			$("#latest_draw_list").slideDown();
		}
	}).on("click","#saiCheSHiPinV3",function(){
		var t = $(this),swf = t.attr("swf");
		if(t.hasClass("active")){
			t.removeClass("active");
			$("#do_saiCheShiPin_standard").slideUp();
		}else{
			t.addClass("active");
			if(swf){
				var w = $("#do_saiCheShiPin_standard");
				if(!t.attr("inited")){
	        		t.attr("inited",true);
	        		var html = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="940" height="543">'
	                    +'<param name="movie" value="'+ swf+'">'
	                    +'<param name="quality" value="High">'
	                    +'<param name="_cx" value="12383">'
	                    +'<param name="_cy" value="1588">'
	                    +'<param name="FlashVars" value="">'
	                    +'<param name="Src" ref="" value="'+ swf+'">'
	                    +'<param name="WMode" value="Window">'
	                    +'<param name="Play" value="-1">'
	                    +'<param name="Loop" value="-1">'
	                    +'<param name="SAlign" value="">'
	                    +'<param name="Menu" value="-1">'
	                    +'<param name="Base" value="">'
	                    +'<param name="AllowScriptAccess" value="always">'
	                    +'<param name="Scale" value="ShowAll">'
	                    +'<param name="DeviceFont" value="0">'
	                    +'<param name="EmbedMovie" value="0">'
	                    +'<param name="BGColor" value="">'
	                    +'<param name="SWRemote" value="">'
	                    +'<param name="MovieData" value="">'
	                    +'<embed src="'+ swf+'" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="940" height="543">'
	                    +'</object>';
	        		w.find("div#saiCheShiPinStatndard").html(html);
	        	}
			}
			$("#do_saiCheShiPin_standard").slideDown();
		}
	}).on("click","#latest_deaw_list_hide",function(){
		hd_main.find("#tznavhd_latest_draw").click();
	}).on("click","#select_area_content_standard .trend_box a",function(){	// 选号辅助
		//layer.msg("敬请期待",{icon:1,offset:['30%']});return false;
		var text = $(this).text(),type=$(this).attr("data-chart");
		if($(this).hasClass("active")){
			$(this).removeClass("active").siblings().removeClass("active");
			$("#trend_chart_zl_standard").slideUp();
		}else{
			$(this).addClass("active").siblings().removeClass("active");
			$("#trend_chart_zl_standard").slideDown();
			$("#trend_chart_tit_zl_standard").text(text.indexOf("走势")>=0?text:(text+"走势"));
			_Core.trends.init(type,text);
		}
		$("#trend_chart_dot_zl_standard").attr("class",'dot '+type+'');
	}).on("click","#select_area_content_standard .trend_chart_phase a",function(){
		$(this).addClass("active").siblings().removeClass("active");
		var limit = $(this).attr("data-limit"),$sel = $("#select_area_content_standard .trend_box a.active");
		var text = $sel.text(),type = $sel.attr("data-chart");
		_Core.trends.init(type,text,limit);
	}).on("click","#trend_chart_zl_standard a.close",function(){
		$(this).parent().slideUp(function(){
			$("#select_area_content_standard .trend_box").find("a").removeClass("active");
		});
	}).on("click","#top_tab_list li",function(){	// 彩种大类
		if(!_Core.isOpen){return false;}
		if($(this).hasClass("on")){return false;}
		$(this).addClass("on").siblings().removeClass("on");
		_Core.playCode = null;
		_Core.groupCode = $(this).attr("value");
		_Core.template._GetContentHtml();
		_Core.betting.resetSumbetCount();
	}).on("click","#play_tab_list label",function(e){	// 彩种小类
		e.preventDefault();
		_Core.playCode = $(this).find("input").attr("c");
		_Core.template._GetContentHtml();
		_Core.betting.resetSumbetCount();
	}).on({
		mouseenter:function(){
			var it = $(this),tips = it.attr("tips");
			if(tips){
				layer.tips(tips,$(this),{tips: [1, '#f13131'],time:0,area:['350px','auto']});
			}
		},
		mouseleave:function(){
			layer.closeAll('tips');
		}
	},"#area_content .ball_select_text span").on({
		mouseenter:function(){
			$(this).css({'z-index':9999}).find(".notebox").slideDown();
		},
		mouseleave:function(){
			$(this).find(".notebox").slideUp(function(){
				$(this).parent().css({'z-index':0});
			});
		}
	},"#do_syndicate_standard .notebox_wrapper").on("click","#area_content .ballmain .ball_wrapper span",function(){
		if(!$(this).hasClass("ball_red")){	// 添加音频
			$(this).attr("class","ball_red");
			_Core.utils.soundBetSelectedPlay();
		}else{
			$(this).attr("class","ball_default");
		}
		_Core.num_checked();
		_Core.betting.sumbetCount($(this));
	}).on("click","#clear_selected_zl_standard",function(){
		_Core.betting.resetClearCount();
	}).on("change","#btn_list_zl_standard",function(){ 	// select框
		var selValue = $(this).val();
		_Core.machine_select($(this),parseInt(selValue));
	}).on("click","#btn_random_zl_standard",function(){ // 机选
		var selValue = $(this).siblings("select").val();
		_Core.machine_select($(this),parseInt(selValue));
	}).on("click","#btn_allselected_zl_standard",function(){	// 全选
		var forData = $(this).parent().attr("for-data");
		$(this).parent().parent().find('div.ball-number-out[for-data='+forData+'] span').attr("class","ball_red");
		_Core.utils.soundBetSelectedPlay();
		_Core.num_checked();
		_Core.betting.sumbetCount();
	}).on("click",".choose-money li",function(){	// 圆角分
		$(this).addClass("on").siblings().removeClass("on");
		var  val = $(this).attr("value"),bs=$("#beiShu").val();
		_Core.betting_hasOwnProperty(parseInt(bs));
	}).on("click",".tp-ui-number-addto",function(){
		var sjNum = $("#beiShu"),v=parseInt(sjNum.val());
    	if((v+1)>=10000000){
    		layer.tips("倍数必须小于10,000,000倍",sjNum,{tips:[1,'#f13131']});
    		return false;
    	}
    	sjNum.val(v+1);
    	_Core.betting_hasOwnProperty(v+1);
	}).on("click",".tp-ui-number-reduce",function(){
		var sjNum = $("#beiShu"),v=parseInt(sjNum.val());
    	if((v-1)<=0){
    		layer.tips("倍数必须大于1倍",sjNum,{tips:[1,'#f13131']});
    		sjNum.val(1);
    		return false;
    	}
    	sjNum.val(v-1);
    	_Core.betting_hasOwnProperty(v-1);
	}).on("keyup blur","#beiShu",function(){
		var it = $(this),v=it.val();
    	if(v){
	    	if(!/^[\d]{1,8}$/.test(v) || v>10000000){
	    		layer.tips("倍数必须大于1，小于10,000,000倍",it,{tips:[1,'#f13131']});
	    		it.val(1);
	    		return false;
	    	}
    	}
    	_Core.betting_hasOwnProperty(v);
	}).on("click","#add_to_list_zs_standard",function(){	// 添加选号
		_Core.refreshMoney();
		if(!_Core.isOpen){return false;}
		if(!bet_result.hasOwnProperty('betCount') || bet_result.betCount <= 0){
			//数据未选或者数据不全，系统提示补全或者机选
			var c2 =$("#select_area_content_standard"),_row = c2.attr('_row'), _column = c2.attr('_column'),_type=c2.attr("_type"),
			column=0,arr=[],temp="",_num=null,bool=false,n=0,_n=[],o=[];
			_number = $("#area_content .ball-number-out");
			if(!_row){
				//单式处理
				var desc = $("#lt_write_box").attr("_desc");
				if(desc){
					layer.msg(desc,{icon:2,offset:['30%']});
				}else{
					layer.msg("请在文本中根据相应规则输入正确的投注号码",{icon:2,offset:['30%']});
				}
			}else{
				if(_row>1){	//	多行处理
					if(_column == 1){	//定位胆另行处理
						var nm = 0;
						nm = _Core.rand(0,_number.length-1);
						_num = _number.eq(nm).find('.ball_wrapper span');
						arr[nm] = _Core.rand(0,_num.length-1);
						temp = '您一个号码都没选呢，小乐帮您选?';
					}else{
						_number.each(function(a,e){
							_num = $(e).find(".ball_wrapper span");
							if(!_num.hasClass("ball_red")){
								var tit = $('div.ball-number-left[for-data='+$(e).attr("for-data")+'] .ball-number-left-text span.tit').text();
								temp+=tit+"，";
								do
								{
									n = _Core.rand(0,_num.length-1);
									if(_type == 11 && $.inArray(n,arr)>=0){
										bool = true;
									}else{bool = false;}
								}while(bool);
								arr[a] = n;
								o[a]=a;
							}else{
								arr[a] = -1;	//有选中则标识-1
								o[a] = -1;
								column++;
							}
						})
						if(_type == 8){
							for(var i=0;i<(_row-column);i++){	//获取选中的索引
								do{
									n = o[_Core.rand(0,o.length-1)];
									if($.inArray(n,_n)>=0 || n == -1){
										bool = true;
									}else{bool = false;}
								}while(bool);
								_n[i]=n;
							}
						}
						temp = temp.substring(0,temp.length-1);
						if($.inArray(-1,arr)==-1){	//全未选中
							temp = '您一个号码都没选呢，小乐帮您选?';
						}else{
							if(_type == 8){
								temp = '您号码尚未选择完整，小乐帮您补全?';
							}else{
								temp = '您尚未选择<font color="red">'+temp+'</font>号码，小乐帮您补全?';
							}
						}
					}
					layer.confirm(temp,{
						btn:['帮我选','自己选'],icon:3,offset : ['30%' ]
					},function(index){
						$.each(arr,function(i,t){
							if(t == -1){return true;}
							if(_type == 8){
								if($.inArray(i,_n)>=0){
									_number.eq(i).find(".ball_wrapper span").eq(t).attr("class","ball_red");
								}
							}else{
								_number.eq(i).find(".ball_wrapper span").eq(t).attr("class","ball_red");
							}
						})
						_Core.betting.sumbetCount();
						_Core.num_checked();
						_Core.buyInfo();
						layer.close(index);
					})
				}else{	//单行处理
					var sel_len = _number.find(".ball_wrapper span.ball_red").length,_arbitrary=c2.attr("_arbitrary"),_unchecked,_checkNum=0,_selNum=0;
					_arbitrary = _arbitrary || 1;
					if(_type==4){_column = 1;}
					if(sel_len == 0){
						temp = '您一个号码都没选呢，小乐帮您选？';
					}else{
						temp = '您至少还需选择 <font color="red">'+(_column-sel_len)+'</font>个号码，小乐帮您选?';
					}
					if(sel_len<_column){
						layer.confirm(temp,{
							btn:['帮我选','自己选'],icon:3,offset:['30%']
						},function(index){
							if(_arbitrary){
								_checkNum = c2.find('.selposition').attr('wei');	//必须选中的个数
								_selNum = c2.find(".selposition label input:checked").length;
								if(_selNum<_checkNum){
									c2.find('.selposition label input').each(function(i,item){
										_n[i]=i;
									})
									for(var c=0;c<(_checkNum-_selNum);c++){
										do{
											n=_n[_Core.rand(0,_n.length-1)];
											if(c2.find('.selposition label:eq('+n+') input').is(":checked")){
												bool = true;
											}else{
												c2.find('.selposition label:eq('+n+') input').prop("checked",true);
												$(".selposition input:checkbox").change();
												bool = false;
											}
										}while(bool);
									}
								}
							}
							_column = _column - sel_len;_num=_number.find(".ball_wrapper span");
							for(var i=0;i<_column;i++){
								column = _Core.rand(0,_num.length-1);
								if(_num.eq(column).hasClass("ball_red")){	// 如果已存在则重新生成
									i=i-1;
								}else{
									_num.eq(column).attr("class","ball_red");
								}
							}
							_Core.betting.sumbetCount();
							_Core.num_checked();
							_Core.buyInfo();
							layer.close(index);
						})
					}
				}
			}
		}else{
			//正常下注
			_Core.buyInfo();
		}
	}).on("keyup blur","#lt_write_box",function(evt){
		var it=$(this),val=it.val(),pattern=new RegExp(it.attr("_pattern")),type=it.attr("_type"),desc=it.attr("_desc")||"";
    	if(val){
    		var vs=val.split(/，|;|\r|\n| /);
        	for(var i =0;i<vs.length;i++){
        		if(vs[i] && !pattern.test(vs[i])){
        			if(evt.type=="blue"||evt.type=="focusout")layer.msg(vs[i]+"格式不对。"+desc,{icon:5});
        			_Core.betting.sumTextareaBetCount(type,null);
        			return;
        		}
        	}
        	_Core.betting.sumTextareaBetCount(type,vs);
    	}
	}).on({
		mouseenter:function(){
			$(this).addClass("hov");
		},
		mouseleave:function(){
			$(this).removeClass("hov");
		}
	},"#multiple_select li").on({
		mouseenter:function(){
			$(this).addClass("ssqIcon-del-hov");
		},
		mouseleave:function(){
			$(this).removeClass("ssqIcon-del-hov");
		}
	},"#multiple_select li span.right-r a").on("click","#multiple_select a.betting-box-add",function(){
		var _bs = $(this).siblings('input'), bs =_bs.val();
		bs = bs.replace('倍','').trim();
		bs = parseInt(bs);
		if((bs+1)>=10000000){
    		layer.tips("倍数必须小于10,000,000倍",_bs,{tips:[1,'#f13131']});
    		return false;
    	}
    	_bs.val(bs+1+"倍");
    	_Core.listUpdateBs($(this),bs+1);
	}).on("click","#multiple_select a.betting-box-minus",function(){
		var _bs = $(this).siblings('input'),bs=_bs.val();
		bs = bs.replace('倍','').trim();
		bs = parseInt(bs);
    	if((bs-1)<=0){
    		layer.tips("倍数必须大于1倍",_bs,{tips:[1,'#f13131']});
    		_bs.val(1+'倍');
    		return false;
    	}
    	_bs.val(bs-1+'倍');
    	_Core.listUpdateBs($(this),bs-1);
	}).on({
		focus:function(){
			var t=$(this),v=t.val().replace('倍','').trim();
			t.addClass("hasfocused");
			t.val(v);
		},
		keyup:function(){
			var t=$(this),v=t.val().replace('倍','').trim();
			if(v){
		    	if(!/^[\d]{1,8}$/.test(v) || v>10000000){
		    		layer.tips("倍数必须大于1，小于10,000,000倍",t,{tips:[1,'#f13131']});
		    		t.val(1);
		    		return false;
		    	}
	    	}
			_Core.listUpdateBs($(this),v);
		},
		blur:function(){
			var t = $(this),v=t.val();
			t.removeClass("hasfocused");
			if(!v){
				v=1;
				_Core.listUpdateBs($(this),v);
			}
			t.val(v+'倍');
		}
	},"#multiple_select input.betting-box-multiple").on("click","#multiple_select a.bet-box-mod",function(){
		alert('暂未开放');
	}).on("click","#removeAll",function(){	//清空列表
		if($("#multiple_select li").length<=0){return false;}
		layer.confirm('确认删除全部投注列表？',{
			btn:['确认','取消'],icon:3,offset:['30%']
		},function(index){
			_Core.clearAllItem();
			layer.close(index);
		})
	}).on("click","#multiple_select a.betting-box-delete",function(){	//删除单条记录
		var t = $(this),t_li = t.parent().parent().parent();
		t_li.slideUp(300,function(){
			t_li.remove();
			_Core.countTotalBet();
	        processBetTraceCore.updateBetData();
	        jointPurchase.jointViewBefore();
		})
	}).on("click","#zl_standard_tab_random1",function(){	//机选
		var rd=1;
		_Core.machine_rand(rd,false);
	}).on("click","#zl_standard_tab_random5",function(){
		var rd=5;
		_Core.machine_rand(rd,true);
	}).on("click","#random1",function(){
		var rd=1;
		_Core.machine_rand(rd,true);
	}).on("click","#random5",function(){
		var rd=5;
		_Core.machine_rand(rd,true);
	}).on("click",".lot_data_right label",function(){	//追号，记录，合买
	}).on("click",".lot_data_right input",function(){
		if(!_Core.isOpen){return false;}
		if(!_Core.isLogin){
			_Core.utils._isLoginBtn();
			return false;
		}
		var t = $(this), f = t.attr("id"),bettr=hd_main.find("#multiple_select li");
		if(t.is(":checked")){
			switch(f){
			case "do_chase":
				if(!bettr.length){
					layer.alert("请先添加一注投注号码",{icon:5,offset : ['30%' ]});
					t.prop("checked",false);
					return false;
				}
				_Core.ajax.LotteryV3_BetTrace();
				$("#chase_multiple_all").val(processBetTraceCore.startTimes);
				hd_main.find("#do_chase_standard").show();
				hd_main.find("#do_syndicate_standard").hide();
				hd_main.find("#do_orderdetail_standard").hide();
				hd_main.find("#submit_lottery").show().siblings("input").hide();
				break;
			case "do_syndicate":
				//layer.msg('敬请期待',{icon:1,offset:['30%']});return false;
				if(!bettr.length){
					layer.alert("请先添加一注投注号码",{icon:5,offset : ['30%' ]});
					t.prop("checked",false);
					return false;
				}
				var allM = $("#all_preces_money").text();
				if(!_Core.isNum(allM)){
					layer.alert("投注总额必须为整数",{icon:5,offset:['30%']});
					t.prop("checked",false);
					return false;
				}
				jointPurchase.jointViewBefore();
				hd_main.find("#do_syndicate_standard").show();
				hd_main.find("#do_chase_standard").hide();
				hd_main.find("#do_orderdetail_standard").hide();
				hd_main.find("#submit_lottery").hide().siblings("input").show();
				$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
				break;
			default:
				getBcLotteryOrder(1);
				hd_main.find("#do_syndicate_standard").hide();
				hd_main.find("#do_chase_standard").hide();
				hd_main.find("#do_orderdetail_standard").show();
				hd_main.find("#submit_lottery").show().siblings("input").hide();
				break;
			}
			t.siblings('input').prop("checked",false);
			t.siblings('label').removeClass("for_checkbox_active");
			t.parent().find('label[for='+f+']').addClass("for_checkbox_active");
		}else{
			t.parent().find('label[for='+f+']').removeClass("for_checkbox_active");
			hd_main.find("#"+f+"_standard").hide();
			hd_main.find("#submit_lottery").show().siblings("input").hide();
		}
	}).on("change",".selposition input:checkbox",function(){
		var it=$(this).parent().parent(),
		a=it.find("input:checkbox:checked").length,
		b=parseInt(it.attr("wei")||0);
		$("#positioncount").text(a);
		var combine=function(b,d){
	    	var c=function(a){
	    		for(var b=1,c=2;c<=a;c++)
	    			b*=c;return b
	    		};
	    	return d>b?0:d==b?1:c(b)/(c(d)*c(b-d))
	    };
		$("#positioninfo").text(combine(a,b));
		_Core.betting.sumbetCount();
	}).on("click","#chase_phase_all",function(){
		if($(this).prop("checked")){
    		$("#betTraceData").find("input:checkbox").prop("checked",true);
    	}else{
    		$("#betTraceData").find("input:checkbox").prop("checked",false);
    	}
    	processBetTraceCore.calcView();
	}).on("change","#chase_select",function(){	//追号期数
		var qiShu = $(this).val(),beiShu=$("#chase_multiple_all").val();
    	if(!/^[\d]{1,2}$/.test(qiShu)){
    		layer.tips("追号期数必须大于0",$(this),{tips:[1,'#f13131']});
    		return false;
    	}
    	if(!/^[\d]{1,8}$/.test(beiShu) || beiShu <1){
    		layer.tips("倍数必须为整数","#chase_multiple_all",{tips:[1,'#f13131']});
    		return false;
    	}
    	processBetTraceCore.startQiShu=qiShu;
    	processBetTraceCore.startTimes=beiShu;// 起始倍数
        processBetTraceCore.generateBetView();
	}).on("keyup blur","#chase_multiple_all",function(){
		var qiShu = $("#chase_select").val(),beiShu=$(this).val();
    	if(!/^[\d]{1,2}$/.test(qiShu)){
    		layer.tips("追号期数必须大于0",$("#chase_select"),{tips:[1,'#f13131']});
    		return false;
    	}
    	if(!/^[\d]{1,8}$/.test(beiShu) || beiShu <1){
    		layer.tips("倍数必须为整数",$(this),{tips:[1,'#f13131']});
    		$(this).val(1);
    		return false;
    	}
    	processBetTraceCore.startQiShu=qiShu;
    	processBetTraceCore.startTimes=beiShu;// 起始倍数
        processBetTraceCore.generateBetView();
	}).on("click",".ballmain .current_omit a",function(){
		var t = $(this),fordata=t.parent().parent().parent().attr("for-data");
		if(t.parent().hasClass("active")){
			t.parent().removeClass("active").siblings().addClass("hidden");
			$(".ballmain").find('.ball-number-out[for-data='+fordata+'] div.rencent_cold_hot').addClass("hidden");
			$(".ballmain").find('.ball-number-out[for-data='+fordata+'] div.max_omit').addClass("hidden");
		}else{
			t.parent().addClass("active").siblings().removeClass("hidden");
			$(".ballmain").find('.ball-number-out[for-data='+fordata+'] div.rencent_cold_hot').removeClass("hidden");
			$(".ballmain").find('.ball-number-out[for-data='+fordata+'] div.max_omit').removeClass("hidden");
		}
	}).on("click","#dxdsq a",function(){
		var t=$(this), name= t.attr("name"),s=t.parent(),forData=s.parent().attr("for-data");
		switch(name){
		case "big":
			_Core.machine_select(s,11);
			break;
		case "small":
			_Core.machine_select(s,12);
			break;
		case "odd":
			_Core.machine_select(s,13);
			break;
		case "even":
			_Core.machine_select(s,14);
			break;
		case "clean":
			s.parent().parent().find('div.ball-number-out[for-data='+forData+'] span').attr("class","ball_default");
			_Core.num_checked();
			_Core.betting.sumbetCount();
			break;
		}
	}).on("keyup blur","#numFen",function(){
		var t = $(this);v=t.val(),am = jointPurchase.allJointPrice,om=0,omT=$("#level_parts_limited"),inputH=$("#oneFenMoney");
		if(_Core.isNum(am)){
			if(v && v != 0){
				om = _Core.__FloatDiv(am,v);
				if(om<jointPurchase.oneEndPrice){
					layer.tips("每份至少"+jointPurchase.oneEndPrice+"元起,当前方案最多可分为 "+am+" 份",t,{tips:[1,'#f13131']});
					t.val(2);
					return false;
				}
			}else{
				layer.tips('合买的份数必须大于1份',t,{tips: [1, '#f13131']});
				t.val(2);
				return false;
			}
			//计算保底和我认购信息
			omT.text("￥"+_Core.__ParseIntDecimal(om,0,3));
			inputH.val(om);
			_Core.mySubEndData(v);
		}else{
			layer.msg("非法操作",{icon:2,offset:['30%']});
		}
	}).on("blur","#myNumFen",function(){
		var t=$(this), cate = $("#do_syndicate_standard"),v=cate.find("#numFen").val(),mF = t.val(),mF=parseInt(mF);
		if(mF && mF!=0){
			if(mF>v){
				layer.tips('认购份数不能大于总份数',t,{tips:[1,'#f13131']});
				t.val(v);
			}else if(mF < jointPurchase.minBuyNum){
				layer.tips('认购份数必须大于'+jointPurchase.minBuyNum+'份',t,{tips:[1,'#f13131']});
				t.val(jointPurchase.minBuyNum);
			}
		}else{
			layer.tips('认购份数必须大于'+jointPurchase.minBuyNum+'份',t,{tips:[1,'#f13131']});
			t.val(jointPurchase.minBuyNum);
		}
		_Core.mySubEndData(v);
	}).on("keyup blur","#baodiNumFen",function(){
		var t=$(this),cate=$("#do_syndicate_standard"),v=cate.find("#numFen").val(),om=cate.find("#oneFenMoney").val(),mF=cate.find("#myNumFen").val(),
		bdF=t.val(),bdF=parseInt(bdF),mF=parseInt(mF),v=parseInt(v),my_buy_money = parseFloat($("#myMoney").text().replace("￥",""));
		if(bdF){
			if((bdF+mF)>v){
				layer.tips("保底份数不能大于" +(v-mF)+ "份",t,{tips:[1,'#f13131']});
				t.val(v-mF);
				bdF=v-mF;
			}
		}else{
			t.val(0);
			bdF=0;
		}
		if(bdF == (v-mF)){
			cate.find("#baodiMoney").text("￥"+_Core.__ParseIntDecimal(jointPurchase.allJointPrice-my_buy_money,0,3));
		}else{
			cate.find("#baodiMoney").text("￥"+_Core.__ParseIntDecimal(bdF*om,0,3));
		}
		cate.find("#baodiBFL").text(_Core.__FloatDecimal((bdF/v)*100,3));
	}).on("keyup blur","#syndicate_plan_depict",function(){
		var t=$(this),v=t.val().length,maxL=200;
		if(v>maxL){
			t.val(t.val().substring(0,200));
		}else{
			$("#readyZiFu").text(v);
		}
	}).on("click","#isbaodi",function(){
		var t=$(this),v=0;
		if(t.is(":checked")){
			$("#baodiNumFen").removeAttr("disabled");
			//计算保底和百分率信息
			v= parseInt($("#numFen").val());
			_Core.baodiData(v);
		}else{
			$("#baodiNumFen").attr("disabled","disabeld");
			$("#baodiMoney").text("￥0.00");
			$("#baodiBFL").text("0");
		}
	}).on("click","#joint_purchase",function(){	//发起合买
		if(!_Core.isLogin){
			_Core.utils._isLoginBtn();
			return false;
		}
		if(!_Core.curQiHao){
			layer.msg("当前期号为空，不能下注",{icon:5,offset:['30%']});
			return false;
		}
		var trs = hd_main.find("#multiple_select li");
		if(!trs.length){
			layer.msg("至少添加一组投注号码",{icon:5})
			return false;
		}
		if(trs.length>_Core.maxItem){
			layer.msg('投注号码数不能多于'+_Core.maxItem+'！' ,{icon:5});
			return false;
		}
		var joint = $("#do_syndicate").prop("checked"),allAmount=(parseFloat($("#all_preces_money").text())||0),nowBalance = parseFloat($("#header_user_money_hidden").val().replace(/,/g,"") || 0) || 0,
		visible = $("#do_syndicate_standard").is(":visible");
		if(nowBalance<allAmount){
			layer.msg('下注金额超过帐户可用余额！',{icon: 5});
            return false;
		}
		if(!joint || !visible){
			layer.msg('非法请求',{icon:2,offset:['30%']});
			return false;
		}
		var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
		$.getJSON(_Core.base+"/lotteryV3/betInfo/token.do",function(data){
			if(data.success==0){
				layer.msg(data.msg,{icon: 5,offset:['30%']});
	            return false;
			}
			layer.close(layerIndex);
			var content = '',myBuyArray = [],qiHao=_Core.curQiHao,
    	 	postData={lotCode:_Core.code,token:data.token,qiHao:qiHao};
			hd_main.find("#multiple_select li").each(function(i, j) {
				myBuyArray.push($(this).attr('buyinfo'));
			});
			postData.bets = myBuyArray;
			//获取合买数据信息
			if(joint && visible){
				myBuyArray = [];
				var jointData = jointPurchase.pushJointData();
				if(!jointData || jointData.split("|").length != 6){
					layer.msg("数据异常，请刷新页面",{icon:2,offset:['30%']});
					return false;
				}
				myBuyArray.push(jointData);
				postData.jointData = myBuyArray;
				content = jointPurchase.buyTsData(postData);
				if(!content){
					return false;
				}
				layer.open({
					type : 1, //page层
					area : [ '550px', 'auto' ],
					skin:'layui-layer-rim',
					title : '下注明细(请确认注单)',
					shade : 0.6, //遮罩透明度
					scrollbar : false,
					offset : '100px',//距离顶部
					moveType : 0, //拖拽风格，0是默认，1是传统拖动
					shift : 1, //0-6的动画形式，-1不开启
					content : content,
					btn: ['确定', '取消'],
					yes:function(index){
						layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
						 $.ajax({url:_Core.base+"/lotteryV3/betInfo/doBetsJoint.do",
			    			 type: 'POST',
			                 dataType: 'json',
			                 data: {data:JSON.stringify(postData)},
			                 success: function(data) {
			                	 if(!data.success){
			                		 layer.msg(data.msg,{icon: 5});
			                         return false;
			                	 }
			                	 layer.msg("下单成功",{icon: 6});
			                	 _Core.clearAllItem();
			                     $("#do_syndicate_standard").hide();
			                     $("#do_syndicate").prop("checked",false);
			                     $("label[for=do_chase]").removeClass("for_checkbox_active");
			                     hd_main.find("#submit_lottery").show().siblings("input").hide();
			                     _Core.refreshMoney();
			                 },
			                 complete: function(data){
			                	 layer.close(layerIndex);
			                	 layer.close(index);
			                 }
			    		 });
					}
				});
			}
		})
		
	}).on("click","#submit_lottery",function(){	//追号
		if(!_Core.isOpen){return false;}
		if(!_Core.isLogin){
			_Core.utils._isLoginBtn();
			return false;
		}
		if(!_Core.curQiHao){
			layer.msg("当前期号为空，不能下注",{icon:5,offset:['30%']});
			return false;
		}
		var trs = hd_main.find("#multiple_select li");
		if(!trs.length){
			layer.msg("至少添加一组投注号码",{icon:5})
			return false;
		}
		if(trs.length>_Core.maxItem){
			layer.msg('投注号码数不能多于'+_Core.maxItem+'！' ,{icon:5});
			return false;
		}
		var tracecount = parseInt($("#chase_info_num").text() || 0)||0,
	 	allAmount=0,zhuiHao=(tracecount > 0 && $('#do_chase').is(':checked')),
	 	nowbalance = parseFloat($("#header_user_money_hidden").val().replace(/,/g,"") || 0)||0;
	     if (zhuiHao) {
	    	 allAmount=(parseFloat($("#chase_info_amount").text())||0);
	         if (nowbalance < allAmount) {
	        	 layer.msg('下注金额超过帐户可用余额！无法追号',{icon: 5});
	             return false;
	         }
	     } else {
	    	 allAmount=(parseFloat($("#all_preces_money").text())||0);
	         if (nowbalance < allAmount) {
	        	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
	             return false;
	         }
	     }
	     var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
	     $.getJSON(_Core.base+"/lotteryV3/betInfo/token.do",function(data){
	    	 if(data.success==0){
	    		 layer.msg(data.msg,{icon: 5});
	             return false;
	    	 }
	    	 layer.close(layerIndex);
	    	 var content = '',myBuyArray = [],qiHao=_Core.curQiHao,
	    	 	postData={lotCode:_Core.code,token:data.token,qiHao:qiHao};
	    	 hd_main.find("#multiple_select li").each(function(i, j) {
	    		 myBuyArray.push($(this).attr('buyinfo'));
	         });
	    	 postData.bets = myBuyArray;
	    	 if (tracecount > 0 && $('#do_chase').is(':checked')) {// 追号数据
	    		 myBuyArray=[];
	    		 myBuyArray.push($("#win_operate").prop("checked") ? 1 : 0);// 中奖是否停止追号
	             var $betTr = $("#betTraceData tr");
	             $betTr.each(function(i, j) {
	            	 j=$(j);
	                 if (j.find("input[type='checkbox']").prop("checked")) {
	                	 j=j.find("input[qh]");
	                	 myBuyArray.push(j.attr("qh")+"|"+j.val());
	                 }
	             });
	             postData.tractData=myBuyArray;
	             content = jointPurchase.buyTsData(postData);
	         } else {
	        	 content = jointPurchase.buyTsData(postData);
	         }
			if(!content){
				return false;
			}
	    	 layer.open({
					type : 1, //page层
					area : [ '550px', 'auto' ],
					skin:'layui-layer-rim',
					title : '下注明细(请确认注单)',
					shade : 0.6, //遮罩透明度
					scrollbar : false,
					offset : '100px',//距离顶部
					moveType : 0, //拖拽风格，0是默认，1是传统拖动
					shift : 1, //0-6的动画形式，-1不开启
					content : content,
					btn: ['确定', '取消'],
					yes:function(index){
						layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
						 $.ajax({url:_Core.base+"/lotteryV3/betInfo/doBets.do",
			    			 type: 'POST',
			                 dataType: 'json',
			                 data: {data:JSON.stringify(postData)},
			                 success: function(data) {
			                	 if(!data.success){
			                		 layer.msg(data.msg,{icon: 5});
			                         return false;
			                	 }
			                	 layer.msg("下单成功",{icon: 6});
			                	 _Core.clearAllItem();
			                     $("#do_chase_standard").hide();
			                     $("#do_chase").prop("checked",false);
			                     $("label[for=do_chase]").removeClass("for_checkbox_active");
			                     _Core.refreshMoney();
			                     //显示下注历史...
			                     if($("#do_orderdetail").is(":checked")){
			                    	 getBcLotteryOrder(1);
			                     }else{
			                    	 $(".lot_data_right input:eq(2)").click();
			                     }
			                 },
			                 complete: function(data){
			                	 layer.close(layerIndex);
			                	 layer.close(index);
			                 }
			    		 });
					}
				});
	    	 if (tracecount > 0 && $('#do_chase').is(':checked') && processBetTraceCore.thisOrderMaybeLose) {
	        	 layer.alert('您的追号方案可能亏损，建议您修改方案！',{skin:'layui-layer-dialog layui-layer-rim',type:1,btn:["确定"],offset:150},function(i){
	        		 layer.close(i);
	        	 });
	         }
	    	 _Core.refreshMoney();
	     });
	})
})



var _Core = {
	maxItem:100,
	price:2,
	base:null,
	playCode:null,
	groupCode:null,
	code:null,
	type:null,
	curQiHao:null,
	audio_pause:false,
	isLogin:false,
	isOpen:true,
	curMoney:0,
	indexBuyData:null,
	init:function(){
		var cookieData = $.cookie("indexBuyData"),gc=null;
		cookieData = eval("("+cookieData+")");
		if(cookieData){
			//处理首页下注数据
			switch(cookieData.lotType){
			case "53":
				gc = 'qs';	//默认下注前三
				_Core.playCode = 'q3zx_fs';
				break;
			case "51":
			case "52":
				gc = 'wxwf';
				_Core.playCode = '5xzx_fs';
				break;
			case "55":
				gc = 'sm';
				_Core.playCode = 'q3zx_fs';
				break;
			case "57":
				gc = 'sxwf';
				_Core.playCode = 'sxfs';
				break;
			case "54":	//直选
				gc = "zx";
				_Core.playCode = 'zhx_fs';
				break;
			case "58":
				gc = 'hz';	//和值
				_Core.playCode = 'hz';
				break;
			case "6":	//特码
			case "66":
				gc = 'tm';
				_Core.playCode = 'tm_a';
				break;
			case "8":
				gc = 'danhao1-10';
				_Core.playCode = 'danhao1-10';
				break;
			case "9":
				gc = 'zhenghe';
				_Core.playCode = 'zhenghe';
				break;
			case "10":
				gc = 'daxiaoshaibao';
				_Core.playCode = 'daxiaoshaibao';
				break;
			case "11":
				gc = 'xingyun28';
				_Core.playCode = 'xingyun28';
				break;
			case "12":
				gc = 'diyiqiu';
				_Core.playCode = 'diyiqiu';
				break;
			case "14":
				gc = '15qiu';
				_Core.playCode = '15qiu';
				break;
			case "15":
				gc = '13qiu';
				_Core.playCode = '13qiu';
				break;
			}
			$('#top_tab_list li[value='+gc+']').addClass("on").siblings().removeClass("on");
			_Core.indexBuyData = cookieData;
		}
		_Core.code = LECAI.code;
		_Core.base = LECAI.base;
		_Core.type = LECAI.type;
		_Core.ajax.lotOp();
		_Core.refreshMoney();
		if(_Core.code != 'LHC' && _Core.code != 'SFLHC' && _Core.code != 'TMLHC' && _Core.code != 'WFLHC' && _Core.code != 'HKMHLHC' && _Core.isLogin && LECAI.version != 5){
			getBcLotteryOrder(1);
		}
	},
	_initTrigger:function(){
		$("#top_tab_list li.on").bind("click",_Core._initTriggerHandler).triggerHandler("click");
	},
	_initTriggerHandler:function(){
		_Core.groupCode = $(this).attr("value");
		_Core.template._GetContentHtml();
		$("#top_tab_list li.on").unbind("click",_Core._initTriggerHandler);
	},
	_tempPlayCodeTrigger:function(){
		$("#play_tab_list label>input[type=radio]:checked").bind("click",_Core._tempPlayCodeTriggerHandler).triggerHandler("click");
	},
	_tempPlayCodeTriggerHandler:function(){
		_Core.playCode = $(this).attr("c");
		_Core.ajax.rateOp();	//冷热遗漏加载
		if(_Core.indexBuyData){
			_Core.utils.indexCookieData();	//加载
		}
		$("#play_tab_list label>input[type=radio]:checked").unbind("click",_Core._tempPlayCodeTriggerHandler);
	},
	buyInfo:function(){
		if(!bet_result.hasOwnProperty('betCount') || bet_result.betCount <= 0){return false;}
		var tz=$("#tz_standard");
		if(tz.find("#multiple_select li").length > _Core.maxItem)return false;
		var allTotalmounts = parseFloat($("#temp_amount_zx_standard").text());	//投注额
		if(allTotalmounts && allTotalmounts>0){
			var bettr = '',tth=$("#area_content .ball_select_text span.tz_box_tit").text(),
			coin = $(".choose-money li.on").attr("value"),
			beiShu = $("#beiShu").val(),
			rate = $("#peiLv").text();
			if(bet_result.errorTagStr != ''){
				layer.msg("温馨提示：以下号码有误，已进行自动过滤。" + bet_result.errorTagStr,{icon:5});
			}
			var hm=(bet_result.hasOwnProperty('optional')&&bet_result.optional)?(bet_result.optional+"@"+bet_result.myarray):bet_result.myarray;
			// 彩票code｜玩法id｜货币单位（1=元，10=角，100=分）｜投注倍数|投注号码
			var buyinfo = _Core.code + '|' + _Core.playCode + '|' + coin + '|'+beiShu + '|' + hm;
			//拼接列表html
			bettr += '<li buyinfo="'+buyinfo+'" c="'+_Core.playCode+'" m="'+allTotalmounts+'" r="'+rate+'">';
			bettr += '<div class="betting-box-left"><span class="betting-box-type">'+$("h3.page_name").text()+" | "+tth+'</span>';
			bettr += '<span class="betting-box-content red">'+hm+'</span><span class="betting-box-sum">[<span class="betting-box-count">'+bet_result.betCount+'</span>注,<span class="betting-box-amount">'+allTotalmounts+'</span>元]</span></div>';
			bettr += '<div class="betting-box-right"><span class="right-l"><a href="javascript://" class="betting-box-minus">-</a><input class="betting-box-multiple" type="text" value="'+beiShu+'倍" maxlength="8"><a href="javascript://" class="betting-box-add">+</a></span>';
			bettr += '<span class="right-m"><a href="javascript:;" class="bet-box-mod">修改</a></span>';
			bettr += '<span class="right-r"><a href="javascript:;" class="betting-box-delete ssqIcon ssqIcon-del"></a></span>';
			bettr += '</div></li>';
			$("#multiple_select").append(bettr);
			_Core.betting.resetClearCount();
            _Core.countTotalBet();
            processBetTraceCore.updateBetData();
            jointPurchase.jointViewBefore();
		}else{
			layer.alert("投注金额不能为0",{icon:2,offset:['30%']});
		}
	},
	machine_rand:function(rd,flag){	//机选
		if(!_Core.isOpen){return false;}
		var c2 =$("#select_area_content_standard"),_type = c2.attr("_type"),_n=[],_row = c2.attr('_row'),n=0,bool=false; 
		_column = c2.attr('_column'),_number = $("#area_content .ball-number-out"),_num = _number.find(".ball_wrapper span");
		if(!_row){	//单式不处理
			layer.msg("该模式无法使用机选模式",{icon:2,offset:['30%']});
		}else{
			for(var f=0;f<rd;f++){	//随机注数
				var arr=[];
				_num.attr("class","ball_default");//初始化
				if(_row>1){	//多行处理	
					if(_column == 1){	//定位胆
						var nm = 0;
						nm = _Core.rand(0,_number.length-1);
						_num = _number.eq(nm).find('.ball_wrapper span');
						n = _Core.rand(0,_num.length-1);
						_num.eq(n).attr("class","ball_red");
					}else{
						if(_type == 8){
							for(var i=0;i<(_number.length-_row);i++){	//反选
								do{
									n = _Core.rand(0,_number.length-1);
									if($.inArray(n,_n)>=0){
										bool = true;
									}else{bool = false;}
								}while(bool);
								_n[i]=n;
							}
						}
						_number.each(function(a,e){
							if(_n && $.inArray(a,_n)>=0){return true;}
							_num = $(e).find(".ball_wrapper span");
							do
							{
								n = _Core.rand(0,_num.length-1);
								if(_type == 11 && $.inArray(n,arr)>=0){
									bool = true;
								}else{bool = false;}
							}while(bool);
							arr[a]=n;
						})
						$.each(arr,function(i,s){
							_number.eq(i).find('.ball_wrapper span').eq(s).attr("class","ball_red");
						})
					}
					_Core.betting.sumbetCount();
					_Core.num_checked();
					if(flag){
						_Core.buyInfo();
					}else{	//保留在选号区
						_Core.utils.soundBetSelectedPlay();
					}
				}else{	//单行处理
					var _arbitrary = c2.attr("_arbitrary"),_checkNum=0,_selNum=0;
					_arbitrary = _arbitrary || 1;
					if(_type==4){_column=1;}
					for(var f=0;f<rd;f++){
						_num.attr("class","ball_default"),c2.find(".selposition label input").prop("checked",false);//初始化
						if(_arbitrary){
							_checkNum = c2.find('.selposition').attr('wei');	//必须选中的个数
							_selNum = c2.find(".selposition label input:checked").length;
							if(_selNum<_checkNum){
								c2.find('.selposition label input').each(function(i,item){
									_n[i]=i;
								})
								for(var c=0;c<(_checkNum-_selNum);c++){
									do{
										n=_n[_Core.rand(0,_n.length-1)];
										if(c2.find('.selposition label:eq('+n+') input').is(":checked")){
											bool = true;
										}else{
											c2.find('.selposition label:eq('+n+') input').prop("checked",true);
											$(".selposition input:checkbox").change();
											bool = false;
										}
									}while(bool);
								}
							}
						}
						for(var i=0;i<_column;i++){
							n = _Core.rand(0,_num.length-1);
							if(_num.eq(n).hasClass("ball_red")){	// 如果已存在则重新生成
								i=i-1;
							}else{
								_num.eq(n).attr("class","ball_red");
							}
						}
						_Core.betting.sumbetCount();
						_Core.num_checked();
						if(flag){
							_Core.buyInfo();
						}else{
							_Core.utils.soundBetSelectedPlay();
							_Core.num_checked();
						}
					}
				}
			}
			//滚动
			if(flag){
				jointPurchase.jointViewBefore();
				$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
			}
		}
	},
	machine_select:function(t,selValue){
		var n=0,forData=t.parent().attr("for-data"),_num=t.parent().parent().find('div.ball-number-out[for-data='+forData+'] span');
		switch(selValue){
		case 10:	// 全
			t.parent().parent().find('div.ball-number-out[for-data='+forData+'] span').attr("class","ball_red");
			break;
		case 11:	// 大
			var l = parseInt(_num.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			_num.each(function(){
				var t=$(this),num = parseInt(t.attr("num"));
				if(num>=l){
					t.attr("class","ball_red");
				}else{
					t.attr("class","ball_default");
				}
			})
			break;
		case 12:	// 小
			var l = parseInt(_num.eq(0).attr("num"),10);
			if(l==0)l=5;else if(l==1)l=6;
			_num.each(function(){
				var t=$(this), num = parseInt(t.attr("num"));
				if(num<l){
					t.attr("class","ball_red");
				}else{
					t.attr("class","ball_default");
				}
			})
			break;
		case 13:	// 奇
			_num.each(function(){
				var t=$(this), num = parseInt(t.attr("num"));
				if(num%2!=0){
					t.attr("class","ball_red");
				}else{
					t.attr("class","ball_default");
				}
			})
			break;
		case 14:	// 偶
			_num.each(function(){
				var t=$(this), num = parseInt(t.attr("num"));
				if(num%2==0){
					t.attr("class","ball_red");
				}else{
					t.attr("class","ball_default");
				}
			})
			break;
		default:	// 个数的则走此处
			_num.attr("class","ball_default");	// 初始化
			for(var i=0;i<selValue;i++){
				n = _Core.rand(0,_num.length-1);
				if(_num.eq(n).hasClass("ball_red")){	// 如果已存在则重新生成
					i=i-1;
				}else{
					_num.eq(n).attr("class","ball_red");
				}
			}
			break;
		}
		_Core.utils.soundBetSelectedPlay();
		_Core.num_checked();
		_Core.betting.sumbetCount();
	},
	rand:function(a,e){
		var g = arguments.length;
        if (0 === g) a = 0, e = 2147483647;
        else if (1 === g) throw Error("Warning: rand() expects exactly 2 parameters, 1 given");
        return Math.floor(Math.random() * (e - a + 1)) + a;
	},
	num_checked:function(){
		var column=0,id="",flag=false;
		$("#area_content .ball-number-out").each(function(a, e) {
			column = 0,id=$(e).attr("for-data");
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		column++;
            		flag = true;
            	}
            })
    		$("#"+id).text(column);
        });
		if(flag){
        	$("#zx_standard_checked").show().siblings("#zx_standard_unchecked").hide();
        }else{
        	$("#zx_standard_unchecked").show().siblings("#zx_standard_checked").hide();
        }
	},
	refreshMoney:function(){
		var money = $("#header_user_money_hidden"),lhcAccount=$("#userName"),lhcMoney=$(".refreshM"),topMoney=$("#header_user_money");
		$.ajax({
			url:_Core.base + "/meminfo.do",
			success:function(res){
				if(res.login){
					clearTimeout(r_t1);
					money.val(res.money);
					topMoney.html(res.money);
					lhcAccount.text(res.account);
					lhcMoney.text(res.money);
					_Core.curMoney = res.money;
					_Core.isLogin = true;
					r_t1 = setTimeout(function(){	//10秒刷新一次余额
						_Core.refreshMoney();
					},10000);
				}else{
					clearTimeout(r_t1);
					money.val("0");
					topMoney.html("0");
					lhcAccount.html('请&nbsp;<a href="javascript://" onclick="_Core.utils._isLoginBtn()">登录</a>');
					lhcMoney.text('0');
					_Core.isLogin = false;
				}
			}
//		,
//			beforeSend:function(){
//				money.val("加载中...");
//				lhcMoney.text("加载中...");
//			} 
		})
	},
	listUpdateBs:function(source,bs){
		var l = _Core.price,s=source.parent().parent().parent(),zhuShu=s.find('span.betting-box-count').text(),
		buyInfo=s.attr("buyinfo"),b=buyInfo.split("|");
		l *=_Core.__FloatMul(zhuShu,bs);
        l = _Core.__FloatDiv(l, b[2]);
        s.find('span.betting-box-amount').text(l);
        b.splice(3,1,bs);
        s.attr("buyinfo",b.join("|"));
        _Core.countTotalBet();
        jointPurchase.jointViewBefore();
	},
	isNum:function(s){
		var regu = "^([0-9]*[.0-9])$"; // 小数测试
		var re = new RegExp(regu),s=s.toString();
		if (s.search(re) != -1)
		  return true;
		else
		  return false;
	},
	mySubEndData:function(v){
		var cate = $("#do_syndicate_standard"),myFen=cate.find("#myNumFen").val(),myFen=parseInt(myFen),om=parseFloat($("#oneFenMoney").val()),min=0;
		jointPurchase.minBuyNum = Math.ceil(v/10);
		
		if(myFen>v || myFen < jointPurchase.minBuyNum){	//认购数必须小于总分数或者大于最低认购份数
			cate.find("#myNumFen").val(jointPurchase.minBuyNum);	
			myFen = jointPurchase.minBuyNum;
		}
		cate.find("#myMoney").text('￥'+_Core.__ParseIntDecimal(myFen*om,0,3));
		cate.find("#myBFL").text(_Core.__FloatDecimal((myFen/v)*100,3));
		if(!$("#isbaodi").prop("checked")){
			$("#isbaodi").click();
		}else{
			_Core.baodiData(v);
		}
	},
	baodiData:function(v){
		var cate = $("#do_syndicate_standard"),my_buy_money = parseFloat($("#myMoney").text().replace("￥","")),my_num = parseInt($("#myNumFen").val()),bdF=0;
		bdF = v - my_num;
		cate.find("#baodiNumFen").val(bdF);
		cate.find("#baodiMoney").text("￥"+_Core.__ParseIntDecimal(jointPurchase.allJointPrice-my_buy_money,0,3));
		cate.find("#baodiBFL").text(_Core.__FloatDecimal((bdF/v)*100,3));
	},
	clearAllItem:function(){
		$("#multiple_select").empty();
		_Core.countTotalBet();
		processBetTraceCore.updateBetData();
		jointPurchase.jointViewBefore();
	},
	betting_hasOwnProperty:function(val){
		if(bet_result.hasOwnProperty('betCount')){
			var r = _Core.__FloatMul(bet_result.betCount, val)*_Core.price;
            r = _Core.__FloatDiv(r, $(".choose-money li.on").attr("value"));
            $("#temp_amount_zx_standard").text(r);
		}
		processBetTraceCore.startTimes = val;	// 起始倍数
	},
	__FloatDecimal:function(a,s){
		var v=Math.pow(10,s);
		return Math.round(a*v)/v;
	},
	__ParseIntDecimal:function(a,b,l){	//a/b l次方
		var l = Math.pow(10,l);	//次方
		if(b==0){
			return Math.floor(a*l)/l;
		}
		return Math.floor(a/b*l)/l;
	},
	__FloatMul: function(a, e) {
    	var g = 0,
            c = a.toString(),
            h = e.toString();
        try {
            g += c.split(".")[1].length
        } catch (l) {}
        try {
            g += h.split(".")[1].length
        } catch (m) {}
        return Number(c.replace(".", "")) * Number(h.replace(".", "")) / Math.pow(10, g)
    },
    __FloatDiv: function(a, e) {
        var g = 0,
            c = 0,
            h=a.toString(), l=e.toString();
        try {
            g = h.split(".")[1].length
        } catch (m) {}
        try {
            c = l.split(".")[1].length
        } catch (q) {}
        h = Number(h.replace(".", ""));
        l = Number(l.replace(".",""));
        return h / l * Math.pow(10, c - g)
    },
    countTotalBet: function() {
        var a = 0,e = 0;
        $("#multiple_select li").each(function(g, c) {
            var h = Number(parseFloat($(this).find("span.betting-box-amount").text()));
            e = _Core.__FloatAdd(e, h);
            h = Number(parseInt($(this).find("span.betting-box-count").text()));
            a = _Core.__FloatAdd(a, h)
        });
        $("#all_preces_money").text(parseFloat(e.toString().match(/\d+(\.\d{1,3})?/)[0] || 0));
        $("#all_pieces_count").text(a);
    },
    __FloatAdd: function(a, e) {
        var g, c;
        try {
            g = a.toString().split(".")[1].length
        } catch (h) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (l) {
            c = 0
        }
        g = Math.pow(10, Math.max(g, c));
        return (a * g + e * g) / g
    }
};

/**
 * 模版加载
 */
_Core.template = function(){
	return {
		_GetContentHtml:function(){
			var load = new Loading(),_id=_Core.code=='LHC'||_Core.code=='SFLHC'||_Core.code=='TMLHC'||_Core.code=='WFLHC'||_Core.code=='HKMHLHC' || LECAI.version == 5?"#bet_panel":"#area_content";
			$.ajax({
				url:_Core.base+'/member/'+LECAI.folder+'/module/lottery/lot_play_type_'+_Core.type+'_'+LECAI.folder+'.html?v=1.'+LECAI.caipiao_version,
				type:"GET",
				dataType:"html",
				success:function(html){
					var res = _Core.template.splitTemHtml(html);
					if(!res){
						res = '<div style="text-align:center;margin-bottom:10px;line-height:230px;font-size: 100px;background:#ffffff;">没有提供玩法</div>'
					}
					// 操作html内容分隔
					// 先记录第一次的小类加载
					if(_Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC' || LECAI.version == 5){
						$("#bet_panel").html(res);
						if(_Core.code == 'CQXYNC'){
							//替换图片
							var $span = $('#navTab_content table tbody');
							if($span.length>0){
								$span.find('span.sfc_img').each(function(i,j){
									j = $(j);
									var hm = j.text();
									j.attr('class',"xync_img xync_bj_" + hm);
								})
							}
						}
						Mark._tempPlayCodeTrigger();
					}else{
						$("#area_content").html(res);
						_Core._tempPlayCodeTrigger();
					}
				},
				beforeSend:function(){
					load.init({
						target:_id
					});
					load.start();
				},
				complete:function(){
					load.stop();
				}
			})
		},
		splitTemHtml:function(h){
			var group = "group_"+_Core.type+"_"+_Core.groupCode,gt={},pt={},resTem="",gtH="";
			gt = h.split("\u263b");
			if(_Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC' || LECAI.version == 5){
				group = group + "\u263a";
			}
			for(var i=0;i<gt.length;i++){
				if(gt[i].indexOf(group)>=0){
					gtH = gt[i];
					break;
				}
			}
			//该处判断是否有进入indexOf判断
			if(!gtH){return gtH;}else{h = gtH;}
			if(_Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC' || LECAI.version == 5){
				resTem = h.split("\u263a")[1];
			}else if(!_Core.playCode){
				h = h.split("\u263d")[0];
				resTem = h.split("\u263e")[1];
			}else{
				var play = "play_" + _Core.playCode + "_" + _Core.type;
				pt = h.split("\u263d");
				for(var i=0;i<pt.length;i++){
					if(pt[i].indexOf(play)>=0){
						h = pt[i];
					}
				}
				resTem = h.split("\u263e")[1];
			}
			return resTem;
		},
		_LengRe:function(arr){
			if(!arr){return;}
			$(".ballmain").find(".ball-number-out").each(function(index,item){
				var a = arr[index];
				$.each(a,function(len,num){
					$(item).find(".rencent_cold_hot").eq(len).text(num);
				})
			})
		},
		_YiLou:function(arr){
			if(!arr){return;}
			$(".ballmain").find(".ball-number-out").each(function(index,item){
				var a = arr[index];
				$.each(a,function(len,num){
					$(item).find(".max_omit").eq(len).text(num);
				})
			})
		}
	}
}();

_Core.betting = function(){
	return {
		resetClearCount:function(){
			$("#area_content").find(".ballmain span.ball_red").attr("class","ball_default");
			$("#zx_standard_unchecked").show().siblings("#zx_standard_checked").hide();
			$("#zx_standard_checked span.black").text(0);
			$("#lt_write_box").val('');
			_Core.betting.resetSumbetCount();
		},
		resetSumbetCount:function(){
			bet_result = {};
			$("#temp_amount_zx_standard,#temp_count_zx_standard").text(0);
			if(_Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC' || LECAI.version == 5){
				Mark.resetTableDataInfo();
			}
		},
		sumbetCount: function(eventEle) {// 计算投注注数和投注金额入口
			var c2 =$("#select_area_content_standard"),
			_type=c2.attr("_type"),
			_row = c2.attr('_row'),
	        _column = c2.attr('_column'),
	        _val = c2.attr('_val');
	        bet_result = {};
	        bet_result.types = _type;
	        switch (_type) {
            case "1":
                _Core.betting.__Duplex(_row, _column,_val);
                break;
            case "3":
            	_Core.betting.__Positioninggall(_row, _column);
                break;
            case "4":
            	_Core.betting.__ElectionsAndValues(_row, _column);break;
            case "5":
            	_Core.betting.__Constitute(_row, _column, _val, c2.attr('_arbitrary'));
                break;
            case "8":
            	_Core.betting.__Arbitrary_Duplex(_row, _column);break;
            case "11":
            	_Core.betting.__Nrepeat(_row, _column);break;
            case "12":
            	_Core.betting.__UseC(_row, _column);break;
            case "13":
           	 	_Core.betting._HuYi2Row(c2,eventEle);break;
        }
    },
    sumTextareaBetCount: function(_type,_vals) {// 计算单式投注注数和投注金额入口
    	bet_result = {};
    	bet_result.types = _type;
    	var zhuShu = 0, ma="";
    	if(_vals){
    		for(var i=0,len=_vals.length;i<len;i++){
	    		if(_vals[i]){
	        		ma=ma+";"+_vals[i];
	        		zhuShu++;
	    		}
	    	}
    	}
        bet_result.betCount = zhuShu;
        bet_result.myarray =ma?ma.substring(1):"";
        bet_result.errorTagStr = "";
        bet_result.optional = "";
        $("#temp_count_zx_standard").text(zhuShu);
        var l = _Core.price,s=$("#beiShu").val(),t=$(".choose-money li.on").attr("value");
        l *=_Core.__FloatMul(zhuShu,s);
        l = _Core.__FloatDiv(l, t);
        $("#temp_amount_zx_standard").text(l);
        if(zhuShu>=1){
        	$("#zx_standard_checked").show().siblings('span').hide();
        }else{
        	$("#zx_standard_unchecked").show().siblings('span').hide();
        }
    },
    _HuYi2Row:function(con,eventEle){
    	var arr = ["",""],n=eventEle.attr("num");
    	if(eventEle.hasClass("ball_red")){
    		if(n.length==2){
	    		con.find("span[num='"+n.substring(1)+"']").attr("class","ball_default");
	    	}else{
	    		con.find("span[num='"+n+n+"']").attr("class","ball_default");
	    	}
    	}
    	con.find(".ball-number-out").each(function(a, e) {
            $(e).find("span.ball_red").each(function(i,j) {
            	arr[a] +=$(j).attr("num");
            });
        });
    	_Core.num_checked();
    	var zhuShu=arr[0].length/2*arr[1].length;
    	_Core.betting.__showAndSaveBet(zhuShu, arr)
    },
    __Duplex: function(_row, _column,_val) {
    	_row = _row || 1;
    	_column = _column || 1;
        var arr = [],row = 0,column = 0,zhuShu = 1;
        $("#area_content .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		column++;
            		if("undefined" != typeof arr[a]){
            			arr[a] += i;
            		}else{
            			arr[a] =i;
            			row++;
            		}
            	}
            })
        });
        if (row != _row || column < _column) {
        	_Core.betting.resetSumbetCount();
        	return false;
        }
        for (var t in arr) zhuShu *= arr[t].length;
        if(_val){
        	zhuShu=zhuShu/_val;
        }
        _Core.betting.__showAndSaveBet(zhuShu, arr)
    },
    __Positioninggall: function(a, e) {
        e = e || 1;
        var g = 0,
            c = 0,
            h = 0,
            l = [],q=_Core.code,
            m = Array(parseInt(a || 5));
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		c++;
            		if("undefined" != typeof m[a]){
            			m[a] += i;
            		}else{
            			m[a] =i;
            		}
            	}
            });
            h++
        });
        if (0 > h || c < e) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        for (var t in m) g += m[t].length;
        if ("SFSC" == q || "BJSC"  == q ||"XYFT"  == q  ||"LXYFT"  == q || "SD11X5" == q || "SH11X5" == q || "GX11X5" == q || "JX11X5" == q || "GD11X5" == q) g /= 2;
        _Core.betting.__showAndSaveBet(g, m)
    },
    __ElectionsAndValues: function(a, e) {
        var c = 0,
            h = 0,
            l = 0,
            m = [];
        switch (e) {
            case "J_3":
                var q = [10, 54, 96, 126, 144, 150, 144, 126, 96, 54];
                break;
            case "S_3":
                q = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1];
                break;
            case "B_3":
            	q = [0, 0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 8, 8, 6, 6, 4, 4, 2, 2];
        		break;
            case "G_3":
                q = [-1, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1];
                break;
            case "J_2":
                q = [10, 18, 16, 14, 12, 10, 8, 6, 4, 2];
                break;
            case "S_2":
                q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
                break;
            case "G_2":
                q = [-1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1];
                break;
            case "S_1":
                q = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
                break;
            case "K_3":
                q = [0,0,0,1,3, 6, 10, 15, 21, 25, 27, 27, 25, 21, 15, 10, 6, 3, 1];
                break;
        }
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		h++;
            		m.push(i);
            	}
            });
            l++
        });
        if (l < a) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        for (var t in m) c += parseInt(q[m[t]]);
        _Core.betting.__showAndSaveBet(c, m)
    },
    __Constitute: function(_row, _column, _val, _arbitrary) {
        var h = 1,l = 1,column = 0,row = 0,arr = [];
        _arbitrary = _arbitrary || 1;
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		column++;
        			arr.push(i);
            	}
            });
            row++;
        });
        if (row != _row || column < _column) {
        	_Core.betting.resetSumbetCount();
        	return false;
        }
        var a=0;
        switch (_val) {
            case "G_2":
                for (a = arr.length; a > arr.length - _column; a--) h *= a;
                break;
            case "P_1":
            case "P_2":
            case "P_3":
            case "G_3":
            case "R_2":
                for (a = 0; a < _column; a++) h *= arr.length - a;
                for (a = _column; 0 < a; a--) l *= a;
                h /= l;
                break;
        }
        var r;
        switch (_arbitrary) {
            case 1:
                break;
            default:{
            	h *= $("#area_content .selposition span#positioninfo").text();
                r = _Core.betting.position_OptionalArr().join(",");
            }
        }
        _Core.betting.__showAndSaveBet(h, arr,false, r)
    },
    position_OptionalArr: function() {
        var e = [];
        $("#area_content .selposition input[name^='position_']").each(function() {
            $(this).prop("checked") && e.push($(this).val())
        });
        return e
    },
    __Arbitrary_Duplex: function(a, e) {
        var c = 0,
            h = 0,
            m = [],
            q = [];
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		c++;
            		if("undefined" != typeof m[a]){
            			m[a] += i;
            			q[a] += 1
            		}else{
            			m[a] =i;
            			q[a] = 1;
            			h++;
            		}
            	}
            });
        });
        if (h < a || c < e) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        var g = _Core.betting.__Elections(q, e);
        while(m.length<5){
        	m[m.length]="";
        }
        _Core.betting.__showAndSaveBet(g, m)
    },
    __Elections: function(a, e) {
        var g = 0,
            c = $.map(a, function(a) {
                if ("" !== a) return a
            }),
            c = _Core.betting.__C_combine(c, e);
        switch (e) {
            case "2":
                for (var h in c) g += c[h][0] * c[h][1];
                break;
            case "3":
                for (h in c) g += c[h][0] * c[h][1] * c[h][2];
                break;
            case "4":
                for (h in c) g += c[h][0] * c[h][1] * c[h][2] * c[h][3]
        }
        return g
    },
    __C_combine: function(a, e) {
        for (var g = [[]], c = [], h = 0, l = a.length; h < l; ++h)
            for (var m = 0,q = g.length; m < q; ++m)(g[m].length < e - 1 ? g : c).push(g[m].concat([a[h]]));
        return c
    },
    __Nrepeat: function(a, e) {
        var g = 1,
            c = 0,
            h = 0,
            l = [],
            m = [];
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		c++;
            		if("undefined" != typeof m[a]){
            			m[a].push(i);
            			l[a] += i
            		}else{
            			m[a]=[i];
            			l[a] =i;
            			h++;
            		}
            	}
            })
        });
        if (h < a || c < e) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        switch (e) {
            case "2":
                for (var t in m) g *= _Core.betting.___P(m[t].length, 1);
                g -= _Core.betting.arrayIntersection(m[0],m[1]).length;
                break;
            case "3":
                g = m[0].length * m[1].length * m[2].length;
                var q = _Core.betting.arrayIntersection(m[0], m[1]).length * m[2].length;
                var t = _Core.betting.arrayIntersection(m[1], m[2]).length * m[0].length;
                var r = _Core.betting.arrayIntersection(m[2], m[0]).length * m[1].length,
                    y = 2 * _Core.betting.count(_Core.betting.array_intersect(m[0], m[1], m[2])),
                    g = g - q - t - r + y
        }
        _Core.betting.__showAndSaveBet(g, l)
    },
    __UseC: function(a, e) {
        var g = [],h = 0,l = 0,m = $("#select_area_content_standard").attr("_g11") || false;
        $("#area_content .ballmain .ball-number-out").each(function(a, e) {
            $(e).find(".ball_wrapper span.ball_red").each(function(i,j) {
            	j=$(j);
            	i=j.attr("num");
            	if(i){
            		l++;
            		g.push(i);
        			h++;
            	}
            })
        });
        if (m) {
            if (h < a || l < e) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        } else if (h != a || l < e) return $("#temp_amount_zx_standard,#temp_count_zx_standard").text(0), !1;
        m = _Core.betting.__C_combine(g, e).length;
        _Core.betting.__showAndSaveBet(m, g)
    },
    arrayIntersection: function(a, e) {
        for (var g = 0, c = 0, h = []; g < a.length && c < e.length;) a[g] < e[c] ? g++ : (a[g] > e[c] || (h.push(a[g]), g++), c++);
        return h
    },
    ___P: function(a, e) {
        return _Core.betting.___factorial(a) / _Core.betting.___factorial(a - e)
    },
    ___C: function(a, e) {
        return e > a ? 0 : _Core.betting.___P(a, e) / _Core.betting.___factorial(e)
    },
    ___H: function(a, e) {
        return _Core.betting.___C(e + a - 1, a - 1)
    },
    ___factorial: function(a) {
        return 1 == a || 0 == a ? 1 : a * _Core.betting.___factorial(a -1)
    },
    array_intersect: function(a) {
        var e = {},
            g = arguments.length,
            c = g - 1,
            h = "",
            l = {},
            m = 0,
            q = "";
        a: for (h in a) b: for (m = 1; m < g; m++) {
            l = arguments[m];
            for (q in l)
                if (l[q] === a[h]) {
                    m === c && (e[h] = a[h]);
                    continue b;
                }
            continue a;
        }
        return e
    },
    count: function(a, e) {
        var g, c = 0;
        if (null === a || "undefined" ===
            typeof a) return 0;
        if (a.constructor !== Array && a.constructor !== Object) return 1;
        "COUNT_RECURSIVE" === e && (e = 1);
        1 != e && (e = 0);
        for (g in a) a.hasOwnProperty(g) && (c++, 1 != e || !a[g] || a[g].constructor !== Array && a[g].constructor !== Object || (c += this.count(a[g], 1)));
        return c
    },
    __showAndSaveBet: function(zhuShu, arr,c, h) {// 显示并记录投注信息
    	zhuShu = zhuShu || 0;
    	arr = arr || [];
        c = c || [];
        h = h || "";
        bet_result.betCount = zhuShu;
        var ma="";
        for(var i=0,len=arr.length;i<len;i++){
        	if(arr[i]){
        		ma=ma+","+arr[i];
        	}else{
        		ma=ma+",-";
        	}
        }
        bet_result.myarray =ma.substring(1);
        bet_result.errorTagStr = c.toString();
        bet_result.optional = h
        $("#temp_count_zx_standard").text(zhuShu);
        var l = _Core.price,s=$("#beiShu").val(),t=$(".choose-money li.on").attr("value");
        l *=_Core.__FloatMul(zhuShu,s);
        l = _Core.__FloatDiv(l, t);
        $("#temp_amount_zx_standard").text(l);
    }
	}
}();


_Core.ajax = function(){
	return {
		// 获取当前开奖期号和上五期号码
		lotOp:function(){
			var data = {
				lotCode:_Core.code
			}
			$.ajax({
				url:_Core.base + "/lotteryV3/lotOp.do",
				data:data,
				success:function(res){
					if(res.success){
						_Core.curQiHao = res.current.qiHao;
						_Core.utils._addLotName(res.lotName);
						_Core.utils._addLotQiShu(res.qishu);
						_Core.utils._addLotCurrentQiHao();
						_Core.utils._addLotLastOne(res.last);
						_Core.utils._addLotLastFive(res.history,true);
						_Core.timer.curData(res.current,res.ago);
						if(res.current.state == 1){
							_Core.isOpen = false;	//封盘
							if(_Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC' || LECAI.version == 5){
								_Core._initTrigger();
							}else{
								$("#area_content").html('<div style="text-align:center;margin-bottom:10px;line-height:230px;font-size: 100px;background:#ffffff;">目前尚未开盘</div>')
							}
						}else{
							_Core.isOpen = true;	//开盘
							_Core._initTrigger();
						}
					}
				}
//				,
//				beforeSend:function(){
//					_Core.utils.loading("body");
//				},
//				complete:function(){
//					_Core.utils.loadingStop();
//				}
			})
		},
		LotteryV3_BetTrace:function(){
			$.ajax({
				url:_Core.base + "/lotteryV3/getQiHaoForChase.do?lotCode=" + _Core.code,
				cache:false,
				type:'get',
				dataType:'json',
				success:function(json){
					if(!json.success){
	            		layer.alert(json.msg,{icon:5,offset : ['30%' ]});
	            		return;
	            	}
	            	if(json.qiHaos && json.qiHaos.length>0){
	            		processBetTraceCore.initView(json.qiHaos);
	                	$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
	            	}else{
	            		layer.alert("今日已经没有期号可追号！",{icon:5,offset : ['30%' ]});
	            	}
				}
			})
		},
		lotLastOne:function(qiHao){
			var data = {
					qiHao:qiHao,
					lotCode:_Core.code
			}
			$.ajax({
				url:_Core.base + "/lotteryV3/lotLast.do",
				data:data,
				type:'GET',
				success:function(res){
					if(res.success){
						//说明上一期已开奖，刷新页面即可，停止定时器
						_Core.utils._clearIntervalLastOne();
						_Core.utils._addLotLastOne(res.last);
						var data = {
								qiHao:res.last.qiHao,
								haoMa:res.last.haoMa,
						}
						var arr = new Array(data);
						_Core.utils._addLotLastFive(arr,false);
						if(LECAI.version == 5 || _Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC'){
							//同时如果version=5或者六合彩的话需要执行一次长龙
							Mark.lotQueue();
						}
					}
				}
			})
		},
		rateOp:function(){
			var data = {
					lotCode:_Core.code,
					playCode:_Core.playCode,
					lotType:_Core.type
			}
			$.ajax({
				url:_Core.base + "/lotteryV3/getRateOp.do",
				data:data,
				type:'GET',
				success:function(res){
					_Core.template._LengRe(res.lengRe);
					_Core.template._YiLou(res.yiLou);
					$("#peiLv").html(res.peiLv);
					$("#fanShui").html(res.fanShui);
				},
				beforeSend:function(){
					$("#peiLv").html('加载中...');
				}
			})
		}
	}
}();


//下注情况数据
function getBcLotteryOrder(page){
	$.ajax({
		url:_Core.base+'/lotteryBet/getBcLotteryOrder.do',
		data:{
			code :_Core.code,
			rows :10,
			page :page,
			},
		success:function(r){
			var betListHtml ='';
			var list = r.page.list;
			var results = r.page;
			if(list.length==0){
				var dateTime = new Date();
				var year=dateTime.getFullYear();
				var month=dateTime.getMonth()+1;
				var date=dateTime.getDate();
				if(month<10){
					month =  "0"+month;
				}else{
					month = month;
				}
				if(date<10){
					date = "0" +date;
				}else{
					date = date;
				}
				var riqi = year+"-"+month+"-"+date;
				$("#betListBetween").text(riqi);
				betListHtml = '<tr><td colspan="8">没有符合查询的资料...</td></tr>';
				$("#betOrderTblFoot").hide();
			}else{
				for(var i=0;i<list.length;i++){
					var sm = "";
					if(list[i].lotteryHaoMa == null || list[i].lotteryHaoMa==''){
						list[i].lotteryHaoMa = '- -';
	    			}
					if(list[i].zhuiHao != 1){
						sm = "(追)";
					}
					if(list[i].jointPurchase && list[i].jointPurchase != 1){
						sm = "(合)";
					}
					var dateTime = list[0].createTime.split(" ");
					var data = dateTime[0];
					$("#betListBetween").text(data);
					betListHtml+='<tr>'
					betListHtml+='<td><a style="width: 100%;" class="BetInf" orderid="'+list[i].orderId+'"lotcode="'+list[i].lotCode+'">'
						+list[i].orderId+''+sm+'</a><br>'+list[i].createTime+'</td>';
					betListHtml+='<td>'+list[i].qiHao+'<br>'+list[i].playName+'</td>';
					betListHtml+='<td>'+list[i].multiple+' | <strong class="red">'+list[i].buyMoney+'</strong></td>';
					betListHtml+='<td><div style="width: 150px;word-break: break-all;">'+strLength(list[i].haoMa)+'</div></td>'
					betListHtml+='<td><strong class="red">'+list[i].lotteryHaoMa+'</strong></td>';
					if(list[i].winMoney != null){
						betListHtml+='<td>'+list[i].winMoney+'</td>';
					}else{
						betListHtml+='<td>0.00</td>';
					}
					// 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
					if(list[i].status == 1 ){
						betListHtml+='<td><span class="label label-1">未开奖 </span></td>';
						if(!GLOBAL.unCancleOrder){
							betListHtml+='<td><a class="cheDan" orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'">撤单</a></td>'
						}else{
							betListHtml+='<td></td>';
						}
					}else if(list[i].status == 2 ){
						betListHtml+='<td><span class="label label-2">已中奖</span></td><td class="notbor"></td>'
					}else if(list[i].status == 3 ){
						betListHtml+='<td><span class="label label-3">未中奖</span></td><td class="notbor"></td>'
					}else if(list[i].status == 4 ){
						betListHtml+='<td><span class="label label-2">已撤单</span></td><td class="notbor"></td>'
					}else if(list[i].status == 5 ){
						betListHtml+='<td><span class="label label-4">回滚成功</span></td><td class="notbor"></td>'
					}else if(list[i].status == 6 ){
						betListHtml+='<td><span class="label label-4">回滚异常</span></td><td class="notbor"></td>'
					}else if(list[i].status == 7 ){
						betListHtml+='<td><span class="label label-4">开奖异常</span></td><td class="notbor"></td>'
					}else if(list[i].status == 8){
						betListHtml+='<td><span class="label label-4">和</span></td><td class="notbor"></td>'
					}else if(list[i].status == 9){
						betListHtml+='<td><span class="label label-4">合买失效</span></td><td class="notbor"></td>'
					}
				}
				$("#betPageAmount").html(r.subBuyMoney);
				$("#betAmount").html(r.sumBuyMoney);
				$("#betOrderTblFoot").show();
			}
			$("#betResultOrder").html(betListHtml);
			layerPageLotteryDetail(r.page.totalCount,r.page.currentPageNo,r.page.totalPageCount);
			//$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
		}
	})
};


function strLength(hm){
	var len = hm.length;
	if(len > 100){
		return hm.substring(0,100) + "...";
	}
	return hm;
}



_Core.timer = function(){
	var a_t=0,n_t=0,o_t=0,down_t1,agos,t=false;
	return {
		curData:function(cur,ago){
			if(!cur.activeTime || !cur.serverTime){
				//读取不到当前开盘时间处理
				this.flipClock(0);
				this.timerTitle(cur.state);
				return false;
			}
			agos = ago;
			this.timerTitle(cur.state);
			a_t = Date.parse(new Date(this.timeFormat(cur.activeTime)));
			n_t = Date.parse(new Date(this.timeFormat(cur.serverTime)));
			o_t = (a_t - n_t) / 1E3;
			if(LECAI.version == 5 || _Core.code == 'LHC' || _Core.code == 'SFLHC' || _Core.code == 'TMLHC' || _Core.code == 'WFLHC' || _Core.code == 'HKMHLHC'){
				if(o_t <= agos && !t){
					this.flipClockClose(o_t);
				}else{
					this.flipClockV2(o_t,agos);
				}
			}else{
				this.flipClock(o_t);
			}
		},
		timeFormat:function(e){
			return new Date(e.replace(/-/g,"/"));
		},
		timerTitle:function(state){
			if(state == 1){
				$("#openTitle").text('已封盘，距离开盘时间截止');
			}
		},
		flipClockV2:function(timer,ago){
			_Core.timer.clear();
			var timers = timer - ago;
			if(timers < 0){timers = 0;}
			var clock = $("#count_down").FlipClock(timers,{
				clockFace:'HourCounter',
				countdown:true,
				callbacks:{
					interval:function(){
						var time = this.factory.getTime().time;
						if(time>0 && time < 10){
							_Core.utils.soundDingPlay();
						}
					},
					stop:function(){
							//说明还未执行到封盘时间段
							//需要禁用所有投注内容
						_Core.timer.clear();
						_Core.timer.flipClockClose(ago);
					}
				}
				
			})
		},
		flipClockClose:function(ago){
			_Core.timer.clear();
			_Core.timer.timerTitle(1);
			var clock = $("#count_down").FlipClock(ago,{
				clockFace:'HourCounter',
				countdown:true,
				callbacks:{
					interval:function(){
						_Core.isOpen = false; //封盘
						Mark.closeOrOpenOddsText();
					},
					stop:function(){
						_Core.timer.clear();
						down_t1 = window.setInterval(function(){
							_Core.ajax.lotOp();
//							layer.alert("您好，新的一期号已开放下注。",{time:3000,offset : ['30%' ]})
//							_Core.utils.soundNewKjPlay();
						},1000)
					}
				}
			})
		},
		flipClock:function(timer){
			_Core.timer.clear();
			if(timer < 0){timer = 0;}
			var clock = $("#count_down").FlipClock(timer,{
				clockFace:'HourCounter',
				countdown:true,
				callbacks:{
					interval:function(){
						var time = this.factory.getTime().time;
						if(time>0 && time<10){
							_Core.utils.soundDingPlay();
						}
					},
					stop:function(){
						_Core.timer.clear();
						down_t1 = window.setInterval(function(){	//延迟一秒执行保证不会重复执行
							_Core.ajax.lotOp();
//							layer.alert("您好，第 "+_Core.curQiHao+" 期 已截止，投注时请确认您选的期号。",{time:3000,offset : ['30%' ]})
//							_Core.utils.soundNewKjPlay();
						},1000);
					}
				}
			})
		},
		clear:function(){
			clearInterval(down_t1);
		}
	}
}();

_Core.trends = function(){
	var dataArr,pageSize=30,types,texts,myChart;
	return {
		init:function(type,text,size){	//type=类型
			//先获取开奖结果数据
			if(size){pageSize=size;}
			types = type;
			texts = text;
			var data = {
					lotCode:_Core.code,
					size:pageSize,
					type:type
			}
			$.ajax({
				url:_Core.base + "/lotteryV3/trendData.do",
				data:data,
				success:function(res){
					if(res.success){
						dataArr = res.data;
						if(size){
							_Core.trends.trendQiHao();
							return;
						}
						_Core.trends.initTrend();
					}else{
						layer.msg(data.msg,{icon:2,offset:['30%']});
						//数据异常
					}
				}
			})
		},
		trendQiHao:function(){
			var hm =[],m,option=myChart.getOption(),qiHao=[],resultNum=[];
			$.each(dataArr,function(i,j){
				qiHao[i]=j.qiHao;
				m = 0;
				hm = j.haoMa.split(",");
				switch(types){
				case "big_small":	//大小
					for(var s=0;s<hm.length;s++){
						if(hm[s]>=5){
							m++; 
						}
					}
					m = {name:m,value:m,haoma:j.haoMa};
					break;
				case "odd_even":	//奇偶
					for(var s=0;s<hm.length;s++){
						if(hm[s]%2!=0){
							m++;
						}
					}
					m = {name:m,value:m,haoma:j.haoMa};
					break;
				case "sum_value":	//和值
					for(var s=0;s<hm.length;s++){
						m += parseInt(hm[s]);
					}
					m = {name:m,value:m,haoma:j.haoMa};
					break;
				case "draw_type":
					//默认前三
					var array = ['组六','组三','豹子'];
					var arrNum = [hm[0],hm[1],hm[2]];
					arrNum.sort(function(a,b){
						return a-b;
					});
					for(var s=0;s<arrNum.length;s++){
						if(arrNum[s] == arrNum[s+1]){
							m++;
						}
					}
					option.yAxis[0].type = 'category';
					option.yAxis[0].axisLabel.formatter = function(value){return array[value];}
					option.yAxis[0].data = [{name:'组六',value:0},{name:'组三',value:1},{name:'豹子',value:2}];
					m = {name:array[m],value:m,haoma:j.haoMa};
					break;
				default:
					return;
				}
				resultNum[i] = m;
			})
			if(qiHao.length == 100){
				option.xAxis[0].axisLabel.interval = 1;
			}else{
				option.xAxis[0].axisLabel.interval = 0;
			}
			option.series[0].data = resultNum;
			option.xAxis[0].data = qiHao;
			myChart.clear();
			myChart.setOption(option,true); 
		},
		initTrend:function(){
			require(
	            [
	                'echarts',
	                'echarts/chart/line'   // 按需加载所需图表，如需动态类型切换功能，别忘了同时加载相应图表
	            ],
	            function (ec) {
	                myChart = ec.init(document.getElementById('trend_chart_list_zl_standard'));
	                var option = {
	                	    tooltip : {
	                	        trigger: 'axis',
	                	        type: 'cross',
	                	        axisPointer:{
	                	        	lineStyle:{
		                	        	type:'dashed',
		                	        	color:'#f13131',
		                	        	width: 1
	                	        	}
	                	        },
	                	        formatter:function(params,ticket,callback){
	                	        	var res = '期号：' + params[0].name;
	                	        	for (var i = 0, l = params.length; i < l; i++) {
	                	        		res += '<br/>开奖号码：' + params[i].data.haoma;
	                	                res += '<br/>' + params[i].seriesName + ' : ' + params[i].data.name;
	                	            }
	                	        	return res;
	                	        }
	                	    },
	                	    grid:{
	                	    	y2:'100px',
	                	    	y:'30px',
	                	    	x:'30px',
	                	    	x2:'30px',
	                	    },
	                	    xAxis : [
	                	        {
	                	            type : 'category',
	                	            boundaryGap : false,
	                	            axisLabel : {
	                	            	interval:0,
	                	            	rotate:-90,
	                	            	textStyle:{
	                	            		baseline:'middle'
	                	            	}
	                	            },
	                	            axisTick : {
	                	            	show:false
	                	            },
	                	            axisLine : {
	                	            	lineStyle : {
	                	            		color : '#f13131'
	                	            	}
	                	            },
	                	            data : ['']
	                	        }
	                	    ],
	                	    yAxis : [
	                	        {
	                	            type : 'value',
	                	            axisLabel : {
	                	                formatter: '{value}'
	                	            },
	                	            axisLine : {
	                	            	lineStyle : {
	                	            		color : '#d5d5d5',
	                	            		width : 2
	                	            	}
	                	            }
	                	        }
	                	    ],
	                	    series : [
	                	        {
	                	            name:texts,
	                	            type:'line',
	                	            data:[''],
	                	            symbolSize:4,
	                	            itemStyle:{
	                	            	normal:{
	                	            		color:'#f13131'
	                	            	}
	                	            }
	                	        }
	                	    ]
	                	};
	                	myChart.setOption(option,true);
	                	_Core.trends.trendQiHao();
	            }
	        ); 
		}
	}
}();

_Core.utils = function(){
	var lastTime=Date.now(),loading,buyListQuartz,quartzNum=0;
	return{
		_addLotName:function(v){
			var _title = $(".page_name");
			_title.text(v);
		},
		_addLotQiShu:function(v){
			var _qishu = $("#current_sale");
			_qishu.text(v);
		},
		_addLotCurrentQiHao:function(){
			var _qiHao = $("#current_issue");
			_qiHao.text(_Core.curQiHao);
		},
		_addLotLastOne:function(last){
			_Core.utils._clearIntervalLastOne();
			var _qiHao = $("#last_qihao"),_hm=last.haoMa,_status=$(".dsLotto-result"),_ready=$("#readyOpen");
			_qiHao.text(last.qiHao);
			if(_hm.indexOf("?")>=0){	//如果结果为空，则5秒获取一次数据
				_status.removeClass("status--off").addClass("status--on");
				_ready.show();
				selectedLotLastQuartz = window.setInterval(function(){
					_Core.ajax.lotLastOne(last.qiHao);
					//两面长龙说明在获取数据中...
				},5000);
				return;
			}
			var hm = _hm.split(",");
			_status.removeClass("status--on").addClass("status--off");
			_ready.hide();
			switch(_Core.code){
				case "LHC":
				case "SFLHC":
				case "TMLHC":
				case "WFLHC":
				case "HKMHLHC":
					var sx;
					for(var i=hm.length-1;i>=0;i--){
						sx = Base.lhcHaoMa.zodiacName(hm[i],last.year);
						$("#last_result_hm_"+i).text(hm[i]).addClass("mark_"+hm[i]);
						$("#last_result_sx_"+i).text(sx);
					}
					break;
				case "HNKLSF":
				case "GDKLSF":
					for(var i=hm.length-1;i>=0;i--){
						$("#last_result_hm_"+i).text(hm[i]);
					}
					break;
				case "CQXYNC":
					for(var i=hm.length-1;i>=0;i--){
						$("#last_result_hm_"+i).text(hm[i]).addClass("_ele_cqxync_"+hm[i]);
					}
					break;
				case "PCEGG":
				case "JND28":
					var sum =0;
					for(var i=hm.length-1;i>=0;i--){
						sum+=parseInt(hm[i]);
						$("#last_result_"+i).text(hm[i]);
					}
					$("#last_result_sum").text(sum);
					break;
				case "BJSC":
				case "XYFT":
				case "LXYFT":
				case "SFSC":
					for(var i=hm.length-1;i>=0;i--){
						$("#last_result_"+i).attr("class","_ele-number ball_bjsc_"+hm[i]);
					}
					break;
				default:
					for(var i=hm.length-1;i>=0;i--){
						$("#last_result_"+i).text(hm[i]);
					}
					break;
			}
			//刷新投注记录
			if(_Core.isLogin && _Core.code != 'LHC' && _Core.code != 'SFLHC' && _Core.code != 'TMLHC' && _Core.code != 'WFLHC' && _Core.code != 'HKMHLHC' && LECAI.version != 5){
				_Core.utils.refreshBuyList();
			}
		},
		_addLotLastFive:function(arr,flag){
			var _id = $("#gd-box2"),template = "",_typeHtml="开奖号码",_typeClass="gd-box-h";
			switch(_Core.code){
				case "BJSC":
					_typeHtml = "";
					_typeClass = "gd-box-p";
					break;
				case "XYFT":
				case "LXYFT":
				case "SFSC":
					_typeHtml="";
					_typeClass = "xyft-box-h";
					break;
				case "LHC":
					_typeHtml = "";
					_typeClass = "gd-box-lhc";
					break;
				case "SFLHC":
					_typeHtml = "";
					_typeClass= "gd-box-sf-lhc";
					break;
				case "GDKLSF":
				case "HNKLSF":
				case "CQXYNC":
				case "FFC":
				case "EFC":
				case "WFC":
				case "HKWFC":
				case "AMWFC":
				case "SFC":
				case "ESFC":
				case "HNFFC":
				case "HNWFC":
				case "AZXY5":
				case "CQSSC":
				case "TJSSC":
				case "XJSSC":
					_typeHtml = "";
					break;
			}
			$.each(arr,function(index,item){
				template += '<p>第 <span class="gd-box-q">'+item.qiHao+'</span> 期'+_typeHtml+':&nbsp;&nbsp;';
				var haoMa = item.haoMa.split(","),sum=0;
				switch(_Core.code){
					case "PCEGG":
						for(var i=0;i<haoMa.length;i++){
							if(haoMa[i]!='?'){
								sum+=parseInt(haoMa[i]);
							}
							if(i<haoMa.length-1){
								template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span><span class="'+_typeClass+'">+</span>'
							}else{
								template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span><span class="'+_typeClass+'">=</span>'
							}
						}
						template += '<span class="'+_typeClass+'">'+(sum==0?"?":sum)+'</span>';
						break;
					case "LHC":
					case "SFLHC":
					case "TMLHC":
					case "WFLHC":
					case "HKMHLHC":
						for(var i=0;i<haoMa.length;i++){
							var rl = Base.lhcHaoMa.bose(haoMa[i]);
							if(i==5){
								template += '<span class="'+_typeClass+' bg'+rl+'">'+haoMa[i]+'</span><span class="plus_lhc">+</span>'
							}else{
								template += '<span class="'+_typeClass+' bg'+rl+'">'+haoMa[i]+'</span>';
							}
						}
						break;
					case "JSSB3":
					case "HBK3":
					case "HEBK3":
					case "AHK3":
					case "SHHK3":
					case "GXK3":
					case "FFK3":
					case "WFK3":
					case "JPK3":
					case "KRK3":
					case "HKK3":
					case "AMK3":
					case "GSK3":
					case "BJK3":
					case "JXK3":
						var sum = 0;
						for(var i=0;i<haoMa.length;i++){
							sum = haoMa[i]=="?"?"?":sum+parseInt(haoMa[i]);
							template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span>';
						}
						if(sum != "?"){
							template += '<span class="'+_typeClass+'">'+(sum>10?'大':'小')+'</span><span class="'+_typeClass+'">'+(sum%2==0?'双':'单')+'</span>';
						}
						break;
					case "FFC":
					case "EFC":
					case "WFC":
					case "HKWFC":
					case "AMWFC":
					case "SFC":
					case "ESFC":
					case "HNFFC":
					case "HNWFC":
					case "AZXY5":
					case "CQSSC":
					case "TJSSC":
					case "XJSSC":
						var sum = 0;
						for(var i=0;i<haoMa.length;i++){
							sum = haoMa[i]=="?"?"?":sum+parseInt(haoMa[i]);
							template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span>';
						}
						if(sum != '?'){
							template += '<span class="'+_typeClass+'">'+(sum>=23?'大':'小')+'</span><span class="'+_typeClass+'">'+(sum%2==0?'双':'单')+'</span>';
						}
						break;
					default:
						for(var i=0;i<haoMa.length;i++){
							template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span>';
						}
						break;
				}
				template += '</p>';
			})
			if(flag){
				_id.html(template);
			}else{
				_id.prepend(template);
			}
		},
		_clearIntervalLastOne:function(){
			clearInterval(selectedLotLastQuartz);
		},
		overTips:function(t){
			t=$(t);
			layer.tips(t.attr("tips"),t,{tips:[1,'#f13131'],time:0,area:['auto']})
		},
		clearOverTips:function(){
			layer.closeAll('tips');
		},
		back_to_top:function(){
			$('body,html').animate({
                scrollTop: 0
            },
            500);
            return false;
		},
		_isLoginBtn:function(){
			$('a[data-reveal-id]').trigger('click');
		},
		indexCookieData:function(){
			var data = _Core.indexBuyData;
			$("#beiShu").val(data.mul);
			if(data.lotType == "58"){
				$.each(data.num,function(i,num){
					$(".ballmain .ball-number-out").find('span[num='+num+']').attr("class","ball_red");
				})
			}else{
				$.each(data.num,function(i,num){
					$(".ballmain .ball-number-out").eq(i).find('span[num='+num+']').attr("class","ball_red");
				})
			}
			_Core.betting.sumbetCount();
			_Core.num_checked();
			//执行完后删除
			$.cookie("indexBuyData",null);
			_Core.indexBuyData = null;
		},
		loading:function(_id){
			load=new Loading();
			load.init({
				target:_id
			});
			load.start();
		},
		loadingStop:function(){
			load.stop();
		},
		refreshBuyList:function(){
			clearInterval(buyListQuartz);
			buyListQuartz = window.setInterval(function(){
				if(quartzNum == 1){
					clearInterval(buyListQuartz);
					quartzNum = 0;
					return;
				}
				getBcLotteryOrder(1);
				quartzNum++;
			},5000);
		},
		//选中
		soundBetSelectedPlay:function(){
			if(!_Core.audio_pause){
				var sound = document.getElementById("sound_bet_select");
				try{
				sound.pause();
				sound.currentTime=0;
				sound.play();
				}catch(a){}
			}
		},
		//倒计时
		soundDingPlay:function(){
			var curTime = Date.now();
			if((curTime-lastTime)<100){return false;}	//执行太频繁，直接跳过
			if(!_Core.audio_pause){
				var sound = document.getElementById("sound_ding");
				try{
				sound.pause();
				sound.currentTime=0;
				sound.play();
				lastTime = Date.now();
				}catch(a){}
			}
		},
		//新一期开奖
		soundNewKjPlay:function(){
			if(!_Core.audio_pause){
				var sound = document.getElementById("sound_newkj");
				try{
				sound.pause();
				sound.currentTime=0;
				sound.play();
				}catch(a){}
			}
		}
	}
}();

var jointPurchase = function(){
	return {
		allJointPrice:0,
		minBuyNum:0,	//发起人最低认购10%
		oneEndPrice:1,
		jointViewBefore:function(){
			var am = $("#all_preces_money").text(),am=parseFloat(am),allFen=am/jointPurchase.oneEndPrice;
			jointPurchase.allJointPrice = am;
			jointPurchase.minBuyNum = Math.ceil(am/10);
			$("#numFen").val(allFen);	//份数系统默认设定的值，用户可以修改
			$("#level_parts_limited").text("￥1.00");
			$("#oneFenMoney").val(jointPurchase.oneEndPrice);
			$("#nowTotalMoney").text("￥"+am);
			$("#myNumFen").val(jointPurchase.minBuyNum);
			//计算其余数据
			_Core.mySubEndData(allFen);
			this.updateJointBetData();
		},
		pushJointData:function(){
			//数据拼接
			var jointInfo,zF=$("#numFen").val(),tC=$("#syndicate_rebate_select").val(),zT=$("#do_syndicate_standard input[name=public_status]:checked").val(),
			mF=$("#myNumFen").val(),bF=$("#baodiNumFen").val(),desc=$("#syndicate_plan_depict").val(),isBd=$("#isbaodi").prop("checked");
			if(!isBd){
				bF=0;
			}
			jointInfo = zF+"|"+tC+"|"+zT+"|"+mF+"|"+bF+"|"+desc;
			return jointInfo;
		},
		buyTsData:function(postData){
			var temp='<div id="model_body">',betLi = $("#multiple_select li"),allZ=0,allM=0,myBuyArray=[],zt="",jt=false;
			//合买信息
			if(postData.jointData){
				myBuyArray = postData.jointData[0].split('|');
				switch(parseInt(myBuyArray[2])){
				case 1:
					zt ='上传即公开';
					break;
				case 2:
					zt = '截止后公开';
					break;
				case 3:
					zt = '完全保密';
					break;
				}
				temp += '<table class="firstTab"><tbody><tr><td width="10%" rowspan="2"><strong>合买信息</strong></td><th width="25%">总分成(份)</th><th width="25%">认购(份)</th><th width="15%">保底(份)</th><th width="15%">是否公开</th><th width="10%">提成</th></tr>';
				temp += '<tr><td><strong class="red">'+myBuyArray[0]+'</strong>份</td><td><strong class="red">'+myBuyArray[3]+'</strong>份</td><td><strong class="red">'+(myBuyArray[4]==0?'未保底':myBuyArray[4])+'</strong>'+(myBuyArray[4]==0?'':'份')+'</td><td>'+zt+'</td><td><strong class="red">'+myBuyArray[1]+'</strong>%</td></tr>';
				temp += '</tbody></table>';
			}else if(postData.tractData){	//追号信息
				var tracecount=parseInt($("#chase_info_num").text() || 0)||0,allAmount=(parseFloat($("#chase_info_amount").text())||0),allZs=(parseInt($("#all_pieces_count").text())*tracecount),ln=$('h3.page_name').text(),zjStop=$("#win_operate").prop("checked") ? '是' : '否';
				jt=true;
				temp += '<table class="firstTab"><tbody><tr><td width="10%" rowspan="2"><strong>追号信息</strong></td><th width="20%">彩票种类</th><th width="20%">追号总期数</th><th width="20%">追号总注数</th><th width="20%">追号总金额</th><th width="10%">中奖停止</th></tr>';
				temp += '<tr><td>'+ln+'</td><td><strong class="red">'+tracecount+'</strong>期</td><td><strong class="red">'+allZs+'</strong>注</td><td><strong class="red">'+allAmount+'</strong>元</td><td>'+zjStop+'</td></tr>';
				temp += '</tbody></table>';
			}
			//订单信息
			var n,h;
			temp += '<table><tbody><tr><td rowspan="'+(betLi.length+2)+'" width="10%"><strong>订单信息</strong></td><th width="25%">玩法类别</th><th width="25%">号码</th><th width="'+(jt?'40%':'15%')+'">'+(jt?'(起始)':'')+'期号</th>';
			if(!jt){
				temp += '<th width="15%">注数</th><th width="10%">金额</th>';
			}
			temp += '</tr>';
			betLi.each(function(i,j){
				j=$(j);
				n=j.find(".betting-box-type").text();h=j.find(".betting-box-content").text();
				if(n.length>11){
					n = n.substring(0,11) + "...";
				}
				if(h.length>15){
					h = h.substring(0,15) + "...";
				}
				allZ += parseInt(j.find(".betting-box-count").text());
				allM += parseFloat(j.find(".betting-box-amount").text());
				temp += '<tr>';
				temp += '<td tips="'+j.find(".betting-box-type").text()+'" onmouseover="_Core.utils.overTips(this);" onmouseout="_Core.utils.clearOverTips();">'+n+'</td><td><strong class="red" tips="'+j.find(".betting-box-content").text()+'" onmouseover="_Core.utils.overTips(this);" onmouseout="_Core.utils.clearOverTips();">'+h+'</strong></td><td>'+_Core.curQiHao+'</td>';
				if(!jt){
					temp += '<td>'+j.find(".betting-box-count").text()+'</td><td>'+j.find(".betting-box-amount").text()+'</td>';
				}
				temp += '</tr>';
			})
			if(!jt){
				temp += '<tr><td colspan="2">总注数：<strong class="red" id="infoLength">'+allZ+'</strong>注</td><td colspan="4">总金额：<strong class="red" id="allM">'+allM+'</strong>元</td></tr>'
			}
			temp += '</tbody></table></div>';
			return temp;
		},
		updateJointBetData:function(){
			var trs=$("#multiple_select").find("li[buyinfo]");
			if(!trs.length){
				$("#do_syndicate_standard").hide();
	            $("#do_syndicate").prop("checked",false);
	            $("label[for=do_syndicate]").removeClass("for_checkbox_active");
			}
		}
	}
}();


var processBetTraceCore = function(){
	return{
	    generatedTraceData: false,// 是否已经生成并显示追号期号
	    thisOrderMaybeLose: false,// 可能出现亏损
	    qiHaos:[],// 可追号得期号
	    startQiShu:10,// 追号期数
	    startTimes:1,// 起始倍数
	    totalCost:0,// 总下注金额
	    profitArr:false,// 盈利情况
	    initView:function(d){// 保存当天可追号得期号，并显示追号期号
	    	this.qiHaos = d;
	    	this.generateBetView();
	    },
	    generateBetView: function() {// 生成并显示追号期号
	    	if(!_Core.curQiHao || !this.qiHaos||this.qiHaos.length==0)return;
	        this.generatedTraceData = true;
	        var html = "",len = this.qiHaos.length,qh,cur=false,bb=this.startTimes;
            for (var a = 0,b=0; a < len && b<this.startQiShu;a++) {
            	qh = this.qiHaos[a];
            	if(qh<_Core.curQiHao){
            		continue;
            	}
                html+= '<tr><td>'+(a+1)+'</td><td align="left"><input value="'+qh+'" type="checkbox" checked>';
                if (!cur) {
                	html+=qh+ '<span class="red">[当前期]</span>';
                	cur = true;
                } else {
                	html+=qh;
                }
                html+='</td><td>倍数<input type="text" class="text" qh="'+qh+'" value="'+bb+
                	'" autocomplete="off" maxlength="4">倍</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>';
                bb = bb*2; //
                b++;
            }
            $("#betTraceData").html(html);
	        
            $("#betTraceData").find('input[type="checkbox"]').unbind('change').bind('change', function() {
	            processBetTraceCore.calcView();
	        });
            $("#betTraceData").find('input[type="text"]').unbind('keyup').bind('keyup', function() {
	            processBetTraceCore.calcView();
	        });
	        this.updateBetData();
	    },
	    updateBetData: function() {// 更新追号期号信息
	        if (!this.generatedTraceData) {
	            return false;
	        }
	        var trs=$("#multiple_select").find("li[buyinfo]"),tongJi=true,zhuiHao=$('#do_chase').is(':checked');
	    	if(trs.length>0){
	    		var playCode="",buyInfos=[],cost=0,c,e;
	    		trs.each(function(){
		    		var it = $(this);
		    		if(playCode==""){
		    			playCode=it.attr("c");
		    		}else if(playCode!=it.attr("c") && zhuiHao){
		    			tongJi=false;
		    		}
		    		cost +=(parseFloat(it.attr("m"),10)||0);
		    		c = it.attr("buyinfo").split("\|");
	    			e={};
	    			e.cash=parseInt(c[3])/parseInt(c[2]);
		    		e.totalCost=cost;
	    			e.rate=(parseFloat(it.attr("r"),10)||0);
	    			e.rateCash=e.cash*e.rate;
	    			e.balls=c[4];
	    			e.ballArgs=[];
	    			e.balls.match("@")&&(c=e.balls.split("@"),e.balls=c[1],e.ballArgs=c[0].split(","));
	    			e.balls=e.balls.split(",");
	    			if(c[1]!='hz'&&c[1]!='sthdx'&&c[1]!='ethfx'){
	    				if(c[1]=='ethdx'){
	    					e.balls=null;
	    				}else{
	    					c=(_Core.type==53 || _Core.type==55)?/\d\d|龙|虎/g:/\d|大|小|单|双/g;
			    			for(var d in e.balls)
			    				e.balls[d]=e.balls[d].match(c);
	    				}
	    			}
		    		buyInfos.push(e);
		    	});
	    		if(!tongJi){
	    			layer.msg('您选择的多种玩法无法计算盈利，将以 -- 呈现！',{icon:2,offset:['30%']});
		    	}
	    		this.totalCost=cost;
    			processBetTraceCore.profitArr=tongJi?processBetTraceCore.calcProfit(buyInfos,playCode):false;// 计算盈利
	    		processBetTraceCore.calcView();
	        } else {// 还没有下注情况，不可以追号
	            this.totalCost = 0;
	            this.profitArr = false;
	            this.generatedTraceData = false;
	            $("#do_chase_standard").hide();
	            $("#do_chase").prop("checked",false);
	            $("label[for=do_chase]").removeClass("for_checkbox_active");
	        }
	    },
	    calcView: function() {// 计算投注金额及其盈利情况，并显示
	        var traceCount = 0,amount=0;
	        this.thisOrderMaybeLose = false;
	        var _this = this,totalCost=_this.totalCost,beforeCount=0;
	        $("#betTraceData").find('tr').each(function(i, j) {
	            j = $(this).children();
	            var betRate = parseInt(j.eq(2).children().val())||0;// 赔率
	            if (j.eq(1).children().prop('checked') && betRate > 0) {
	                traceCount++;
	                i=betRate * totalCost;
                	beforeCount+=i;
                	j.eq(3).text(i.toFixed(2));
                    j.eq(4).text(beforeCount.toFixed(2));
	                if (_this.profitArr === false) {
	                    j.eq(5).text('--');
	                    j.eq(6).text('--');
	                } else {
	                    var calcRes = _this.generatorBetTrace(betRate, beforeCount);
                        j.eq(5).text(calcRes.earnStr);
                        j.eq(6).text(calcRes.percentStr);
                        if (calcRes.minEarn < 0) {
                            _this.thisOrderMaybeLose = true;
                        }
	                }
	            } else {
	                j.eq(3).text('--');
	                j.eq(4).text('--');
	                j.eq(5).text('--');
	                j.eq(6).text('--');
	            }
	        });
	        $("#chase_info_num").text(traceCount);// 追号期数
	        $("#chase_info_amount").text(beforeCount.toFixed(2));// 追号总金额
	    },
	    calcProfit:function(buyInfos,playCode){// 计算下注盈利
	    	var a,r={};
	    	this.temp=[];
	    	for(var i = 0,len=buyInfos.length;i<len;i++){
	    		a=buyInfos[i];
	    		switch(playCode){
		    	case "dwd":// 定位胆
		    		r=this.multipleWinning(a.balls,a.rateCash);
	    			break;
		    	case "q2zx_fs":
		    	case "h2zx_hz":
		    	case "q2zx_hz":
		    	case "h2zx_fs":
		    	case "q3zx_fs":
		    	case "z3zx_fs":
		    	case "h3zx_fs":
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    		break;
		    	case "bdw_q31m":
		    	case "bdw_h31m":
		    	case "bdw_z31m":
		    		this.combineProfitArray(r,a.balls,a.rateCash);break;
		    	case "rxwf_r2zx_fs":
		    		r=this.multipleWinning(this.anyDirect(2,a.balls),a.rateCash);break;
		    	case "rxwf_r3zx_fs":
		    		r=this.multipleWinning(this.anyDirect(3,a.balls),a.rateCash);break;
		    	case "rxwf_r3zux_zu3":
		    		r=this.multipleWinning(this.anyGroup(3,2,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r3zux_zu6":
		    		r=this.multipleWinning(this.anyGroup(3,3,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r4zx_fs":
		    		r=this.multipleWinning(this.anyDirect(4,a.balls),a.rateCash);break;
		    	case "q3zux_zu3":
		    	case "h3zux_zu3":
		    	case "z3zux_zu3":
		    		this.combineProfitArray(r,this.generatorGroup(2,a.balls),a.rateCash);break;
		    	case "q3zux_zu6":
		    	case "h3zux_zu6":
		    	case "z3zux_zu6":
		    		this.combineProfitArray(r,this.generatorGroup(3,a.balls),a.rateCash);break;
		    	default:
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    	}
	    	}
    		var m=Number.MIN_VALUE,x=Number.MAX_VALUE,b;
			for(var d in r){
				b=r[d];
				m=Math.max(m,b);
				x=Math.min(x,b);
			}
			if(m!=Number.MIN_VALUE){
				return {maxProfit:parseFloat(m.toFixed(2)),minProfit:parseFloat(x.toFixed(2))};
	    	}
	    	return false;
	    },
	    generatorBetTrace:function(betRate, beforeCount){// 计算每一条追号得盈利
	    	var min = this.profitArr.minProfit,max = this.profitArr.maxProfit,minEarn=0,maxEarn=0,earnStr="",percentStr="";
	    	minEarn = min*betRate -beforeCount;
	    	maxEarn=max*betRate - beforeCount;
	    	earnStr = (minEarn==maxEarn)?minEarn.toFixed(2):minEarn.toFixed(2)+"至"+maxEarn.toFixed(2);
	    	percentStr = (minEarn==maxEarn)?((minEarn/beforeCount*100).toFixed(2)+"%"):((minEarn/beforeCount*100).toFixed(2)+"%")+"至"+((maxEarn/beforeCount*100).toFixed(2)+"%");
	    	return {earnStr:earnStr,percentStr:percentStr,minEarn:minEarn,maxEarn:maxEarn};
	    },
	    multipleWinning:function(b,d){
	    	var c,a,e,f,g,h;
	    	for(e in b){
	    		this.temp[e]instanceof Array||(this.temp[e]=[]),this.combineProfitArray(this.temp[e],b[e],d);
	    	}
    		h=g=0;
    		for(e in this.temp){
    			a=[];
    			for(f in this.temp[e])
    				a.push(this.temp[e][f]);
    			c=Math.max.apply(null,a);
    			a=Math.min.apply(null,a);
    			-Infinity===c&&(c=0);
    			Infinity===a&&(a=0);
    			g+=c;
    			h+=a
    		}
    		return{maxProfit:g,minProfit:h}
	    },
	    combineProfitArray:function(b,d,c){
	    	if(!(d instanceof Array))return!1;
	    	var a;
	    	for(var e= 0,f=d.length;e<f;e++){
	    		a=d[e],null!==a&&void 0!==a&&(b.hasOwnProperty(a)?b[a]+=c:b[a]=c);
	    	}
	    	return b
    	},
    	expandArray:function(b){
    		var d,c,a,e,f,g,h,k,l,n,m;
    		if(0===b.length)return!1;
    		b=b.slice();
    		m=b.splice(0,1)[0];
    		m instanceof Array||(m=m.toString().split(","));
    		f=0;
    		for(k=b.length;f<k;f++)
    			for(a=b[f],c=m.slice(),m=[],g=0,l=a.length;g<l;g++)
    				for(e=a[g],h=0,n=c.length;h<n;h++)
    					d=c[h],m.push(""+d+e);
    		return m
    	},
    	anyDirect:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G,J,L,K,M;
    		D=[];
    		if(2===b){
    			for(r=e=0,m=d.length-b;0<=m?e<=m:e>=m;r=0<=m?++e:--e){
    				if(null!==d[r]){
    					for(q=d[r],g=0,l=q.length;g<l;g++){
    						for(c=q[g],u=f=p=r+1,t=d.length;p<=t?f<t:f>t;u=p<=t?++f:--f){
    							if(null!==d[u]){
    								for(v=d[u],k=0,n=v.length;k<n;k++){
    									a=v[k];
    									h=""+r+u;
    									D[h]instanceof Array||(D[h]=[]);
    									D[h].push(""+c+a);
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(3===b){
    			for(r=n=0,z=d.length-b;0<=z?n<=z:n>=z;r=0<=z?++n:--n){
    				if(null!==d[r]){
    					for(B=d[r],m=0,f=B.length;m<f;m++){
    						for(c=B[m],u=q=C=r+1,A=d.length-1;C<=A?q<A:q>A;u=C<=A?++q:--q){
    							if(null!==d[u]){
    								for(E=d[u],p=0,k=E.length;p<k;p++){
    									for(a=E[p],g=t=x=u+1,w=d.length;x<=w?t<w:t>w;g=x<=w?++t:--t){
    										if(null!==d[g]){
    											for(y=d[g],v=0,l=y.length;v<l;v++){
    												e=y[v];
    												h=""+r+u+g;
    												D[h]instanceof Array||(D[h]=[]);
    												D[h].push(""+c+a+e);
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(4===b){
    			for(r=F=0,p=d.length-b;0<=p?F<=p:F>=p;r=0<=p?++F:--F){
    				if(null!==d[r]){
    					for(t=d[r],I=0,k=t.length;I<k;I++){
    						for(c=t[I],u=H=v=r+1,x=d.length-2;v<=x?H<x:H>x;u=v<=x?++H:--H){
    							if(null!==d[u]){
    								for(w=d[u],G=0,l=w.length;G<l;G++){
    									for(a=w[G],g=J=y=u+1,z=d.length-1;y<=z?J<z:J>z;g=y<=z?++J:--J){
    										if(null!==d[g]){
    											for(B=d[g],L=0,n=B.length;L<n;L++){
    												for(e=B[L],q=K=C=g+1,A=d.length;C<=A?K<A:K>A;q=C<=A?++K:--K){
    													if(null!==d[q]){
    														for(E=d[q],M=0,m=E.length;M<m;M++){
    															f=E[M];
    															h=""+r+u+g+q;
    															D[h]instanceof Array||(D[h]=[]);
    															D[h].push(""+c+a+e+f);
    														}
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else {
    			layer.msg("任"+b+"直选错误",{icon:5});
    		}
    		return D
    	},
    	anyGroup:function(b,d,c,a){
    		var e,f;
    		null==b&&(b=2);
    		null==d&&(d=2);
    		null==c&&(c=[]);
    		null==a&&(a=['万','千','百','十','个']);
    		f=[];
    		b=parseInt(b);
    		d=parseInt(d);
    		c instanceof Array||(c=c.split(","));
    		if(a.length<b){
    			layer.msg("组选复式参数错误"),{icon:5};
    			return false;
    		}
    		c=this.generatorGroup(d,c);
    		e=this.generatorGroup(b,a);
    		b=0;
    		for(a=e.length;b<a;b++){
    			d=e[b];
    			f[d]=c;
    		}
    		return f
    	},
    	generatorGroup:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G;
    		null==b&&(b=2);
    		null==d&&(d=[]);
    		G=[];
    		b=parseInt(b);
    		d=d instanceof Array?d.slice():d.splt(",");
    		if(1===b)return d;
    		if(2===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=0,p=d.length;l<p;l++)
    					u=d[l],G.push(""+r+u);
    		else if(3===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=n=0,q=d.length;0<=q?n<q:n>q;l=0<=q?++n:--n)
    					for(u=d[l],p=m=k=l+1,f=d.length;k<=f?m<f:m>f;p=k<=f?++m:--m)l=d[p],G.push(""+r+u+l);
    		else if(4===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=q=0,m=d.length-2;0<=m?q<m:q>m;l=0<=m?++q:--q)
    					for(u=d[l],p=k=f=l+1,w=d.length-1;f<=w?k<w:k>w;p=f<=w?++k:--k)
    						for(l=d[p],n=g=v=p+1,y=d.length;v<=y?g<y:g>y;n=v<=y?++g:--g)p=d[n],G.push(""+r+u+l+p);
    		else if(5===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=m=0,k=d.length-3;0<=k?m<k:m>k;l=0<=k?++m:--m)
    					for(u=d[l],p=f=g=l+1,w=d.length-2;g<=w?f<w:f>w;p=g<=w?++f:--f)
    						for(l=d[p],n=v=y=p+1,h=d.length-1;y<=h?v<h:v>h;n=y<=h?++v:--v)
    							for(p=d[n],q=x=z=n+1,c=d.length;z<=c?x<c:x>c;q=z<=c?++x:--x)
    								n=d[q],G.push(""+r+u+l+p+n);
    		else if(6===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,k=d.length-4;0<=k?f<k:f>k;l=0<=k?++f:--f)
    					for(u=d[l],p=v=w=l+1,g=d.length-3;w<=g?v<g:v>g;p=w<=g?++v:--v)
    						for(l=d[p],n=x=y=p+1,h=d.length-2;y<=h?x<h:x>h;n=y<=h?++x:--x)
    							for(p=d[n],q=C=z=n+1,c=d.length-1;z<=c?C<c:C>c;q=z<=c?++C:--C)
    								for(n=d[q],m=A=B=q+1,a=d.length;B<=a?A<a:A>a;m=B<=a?++A:--A)q=d[m],G.push(""+r+u+l+p+n+q);
    		else if(7===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=w=0,f=d.length-5;0<=f?w<f:w>f;l=0<=f?++w:--w)
    					for(u=d[l],p=y=g=l+1,v=d.length-4;g<=v?y<v:y>v;p=g<=v?++y:--y)
    						for(l=d[p],n=z=h=p+1,x=d.length-3;h<=x?z<x:z>x;n=h<=x?++z:--z)
    							for(p=d[n],q=c=C=n+1,B=d.length-2;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-1;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										m=d[k],G.push(""+r+u+l+p+n+q+m);
    		else if(8===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,w=d.length-6;0<=w?f<w:f>w;l=0<=w?++f:--f)
    					for(u=d[l],p=g=v=l+1,y=d.length-5;v<=y?g<y:g>y;p=v<=y?++g:--g)
    						for(l=d[p],n=h=x=p+1,z=d.length-4;x<=z?h<z:h>z;n=x<=z?++h:--h)
    							for(p=d[n],q=c=C=n+1,B=d.length-3;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-2;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length-1;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										for(m=d[k],t=k=I=k+1,H=d.length;I<=H?k<H:k>H;t=I<=H?++k:--k)
    											t=d[t],G.push(""+r+u+l+p+n+q+m+t);
    		else layer.msg("长度组选错误",{icon:5});
    		return G
    	}
	}
}();