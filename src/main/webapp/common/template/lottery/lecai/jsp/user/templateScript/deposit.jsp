<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script id="inputdata_tpl" type="text/html">
	{{if onlines && onlines.length>0}}
	<div id="onlines_deposit_div_id" style="border-bottom: dashed 1px #ccc; margin: 0 30px 0 25px;"><ul class="ws_bank_new clearfix ws_bank_new_more">
		{{each onlines as online count}}
		<li><a href="javascript:void(0);"><input name="payId" data-fixedFlag="{{online.fixedFlag}}" data-randomFlag="{{online.randomFlag}}" data-payDesc="{{online.payDesc}}" data-fixedAmount="{{online.fixedAmount}}" data-randomAmount="{{online.randomAmount}}" min="{{online.min}}" max="{{online.max}}" iconCss="{{online.iconCss}}" payType="{{online.payType}}" value="{{online.id}}" type="radio">
			{{if online.icon}}
			<img src="${base}{{if online.icon}}{{online.icon}}{{/if}}" width="135" height="40"></a>
			{{else if online.payType == "1"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px;text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/banks.png');background-size: cover;">{{online.payName}}</span></a>
			{{else if online.payType == "3"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px;text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/weixin.jpg');background-size: cover;">{{online.payName}}</span></a>
			{{else if online.payType == "4"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg');background-size: cover;">{{online.payName}}</span></a>
			{{else if online.payType == "5"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/qqpay.png');background-size: cover;">{{online.payName}}</span></a>
			{{ else if online.payType == "6"}}
			<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/jdpay.jpg" height="40" width="123"></div>
			{{ else if online.payType == "7"}}
			<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/baidu.jpg" height="40" width="123"></div>
			{{ else if online.payType == "8"}}
			<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/union.jpg" height="40" width="123"></div>
			{{ else if online.payType == "9"}}
			<div class="zhifuImg" title="{{online.payName}}"><img src="${base}/common/template/lottery/jiebao/images/unionpay.png" height="40" width="123"></div>
			{{else if online.payType == "11"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px;text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/weixin.jpg');background-size: cover;">{{online.payName}}</span></a>
			{{else if online.payType == "12"}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg');background-size: cover;">{{online.payName}}</span></a>
			{{else}}
			<span class="icons {{online.iconCss}}" style="width:121px;display:table-caption;height:35px;">{{online.payName}}</span>
			{{/if}}
		</li>
		{{/each}}
	</ul></div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		   class="cz-table hidn" >
		<tbody>
		<tr>
			<td width="130"><div style="text-indent:22px;">充值金额：</div></td>
			<td width="135">
				<div>
					<input type="text" class="text" id="amount" name="amount"
						   style="ime-mode: disabled; width: 90px; height: 28px; line-height: 28px; margin-right: 10px;">元
				</div>
			</td>
			<td><span class="cz-limit"></span></td>
		</tr>
		<tr id="pcInputMoney"  style="display: table-row;"><td></td><td  style="text-align: left"><span>10</span><span>20</span><span>30</span><span>50</span><span>100</span></td></tr>
		<tr id="onlinesTip" style="display: table-row;">
			<td style="padding-left: 22px;">充值提示：</td>
			<td><span>我要钱a</span></td>
		</tr>
		<tr>
			<td><div style="text-indent:22px;">充值金额(大写)：</div></td>
			<td colspan="2"><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2"><div class="nextBtn" style="margin-top: 10px;">
				<a href="javascript:void(0);" id="sub-btn"></a>
			</div></td>
		</tr>
		</tbody>
	</table>
	{{else if virtuals && virtuals.length>0}}
	<div style="border-bottom: dashed 1px #ccc; margin: 0 30px 0 25px;"><ul class="ws_bank_new clearfix ws_bank_new_more ">
		{{each virtuals as virtual count}}
		<li id="{{virtual.iconCss}}" class="referenceRateConsult" qrcodeDesc="{{virtual.payDesc}}" blockChainType="{{virtual.blockChainType}}" courseVircoul="{{virtual.tipsShowUrl}}" courseVircoulOpenSrc="{{virtual.tipsRedirectUrl}}" referenceRate="{{virtual.rate}}" referenceRateConsult="1{{virtual.payChannel}}≈{{virtual.rate}}RMB"   flabel="{{virtual.frontLabel}}"><a href="javascript:void(0);"><input name="payId" min="{{virtual.min}}" max="{{virtual.max}}" iconCss="{{virtual.iconCss}}" payType="{{virtual.payType}}" value="{{virtual.id}}" type="radio">
			{{if virtual.icon}}
			<img src="${base}{{if virtual.icon}}{{virtual.icon}}{{/if}}" title="{{virtual.payName}}" width="135" height="40"></a>
			{{else}}
			<span title="{{virtual.payName}}" class="icons {{virtual.iconCss}}" style="width:121px;height:35px;display:table-caption">{{virtual.payName}}</span></a>
			{{/if}}
		</li>
		{{/each}}
	</ul></div>
	<table class="cz-table hidn" id="fastDepositTableId" style="margin: 35px 0 20px 0;width:100%;" cellspacing="0" cellpadding="0" border="0">
		<tbody>
		<tr>
			<td colspan="3">
				<div style="border:solid 1px #ffce89;width:712px;background: #fffaec;margin-left:25px;">
					<div class="gift-card-con">
						<ul class="gift-card-list clearfix">
							<li>客服上班时间为：<span class="blue">{{start}}</span>至<span class="blue">{{end}}</span></li>
							<li>转账成功后若超过五分钟未上分，请立即联系客服</li>
							<li>贴心提醒：充值信息会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。</li>
							<li>"充值个数"若与转帐金额不符，充值将无法准确到帐。</li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<c:if test="${showPayInfo}"><tr>
			<td style="text-align:right;">钱包类型：</td>
			<td colspan="2" id="pay_type_name"></td>
		</tr>
<%--			<tr>--%>
<%--				<td style="text-align:right;" width="120">收款姓名：</td>--%>
<%--				<td id="Bname_info" width="200"></td>--%>
<%--				<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>--%>
<%--					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">--%>
<%--					</div>--%>
<%--				</td>--%>
<%--			</tr>--%>
<%--			<tr>--%>
<%--				<td style="text-align:right;">收款帐号：</td>--%>
<%--				<td id="Baccount_info"></td>--%>
<%--				<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>--%>
<%--					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">--%>
<%--					</div>--%>
<%--				</td>--%>
<%--			</tr>--%>
<%--			<tr id="qrcode_img_wrap">--%>
<%--				<td style="text-align:right;">二维码：</td>--%>
<%--				<td colspan="2"><img src="#" id="qrcode_img_id" style="width: 300px; height: 300px;padding:5px 0">--%>
<%--					<div style="font-size:20px;color:red;margin-top:6px">扫码支付后，请填写充值金额，并点击确定充值，否则无法为您上分！</div></td>--%>
<%--			</tr>--%>
<%--			<tr >--%>
<%--				<td style="text-align:right;">链类型:</td>--%>
<%--				<td colspan="2" class="blockChainType"></td>--%>
<%--			</tr>--%>
			<tr >
				<td style="text-align:right;">参考汇率:</td>
				<td colspan="2" class="consultRate"></td>
			</tr>
		</c:if>
		<tr>
			<td style="text-align:right;">充值个数：</td>
			<td>
				<div>
					<input class="text" id="amount" name="amount" style="ime-mode: disabled; width: 90px; height: 28px; line-height: 28px; margin-right: 10px;" type="text">个
				</div>
			</td>
			<td id="dan_bi_limit"></td>
		</tr>
		<tr>
			<td style="text-align:right;">充值个数(大写)：</td>
			<td ><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">个</span></td>
			<td><span>约等于：</span><span id="allVirtual">0RMB</span></td>
		</tr>
		<tr >
			<td style="text-align: left" colspan="3">
				<div style="font-size:14px;color:red;margin-top:6px">温馨提示：<span class="qrcodeDesc">提示你</span></div>
			</td>
		</tr>
<%--		<tr id="deposit_pay_type_id">--%>
<%--			<td style="text-align:right;"><span class="payType"></span>：</td>--%>
<%--			<td colspan="2"><input class="text" name="bank_cards" minlength="5" maxlength="20"--%>
<%--								   style="ime-mode: disabled; width: 100px; height: 28px; line-height: 28px; margin-right: 10px;" type="text">--%>
<%--				<span class="cz-fasts">请填写正确的<span class="payType"></span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>--%>
<%--		</tr>--%>
		<tr>
<%--			<td style="width: 100px;height: 100px;border-radius: 50%;position: relative;">--%>
<%--				<img class="courseVircoul" src="" alt="" style="border-radius: 50%;height: 100%;width: 100%;position: absolute;left: -12px;top: 0">--%>
<%--			</td>--%>
	<td></td>
			<td class="czConfirm" colspan="2">
<%--				教程--%>


				<c:if test="${showPayInfo}"><div class="gmbtn"><input onclick="Base.virtuals.checkFastSub(this)" id="sub-btn" class="sub_btn js_glisten normal" type="button"><div class="clear"></c:if>
				<c:if test="${!showPayInfo}"><div class="nextBtn" style="margin-top: 10px;"><a href="javascript:void(0);" id="sub-btn"onclick="Base.virtuals.checkFastSub(this)"></a></div></c:if>
			</div>
			</div></td>
		</tr>
		</tbody>
	</table>
	{{else if fasts && fasts.length>0}}
	<div style="border-bottom: dashed 1px #ccc; margin: 0 30px 0 25px;"><ul class="ws_bank_new clearfix ws_bank_new_more">
		{{each fasts as fast count}}
		<li id="{{fast.iconCss}}" flabel="{{fast.frontLabel}}"><a href="javascript:void(0);"><input name="payId" min="{{fast.min}}" max="{{fast.max}}" iconCss="{{fast.iconCss}}" payType="{{fast.payType}}" value="{{fast.id}}" type="radio">
			{{if fast.icon}}
			<img src="${base}{{if fast.icon}}{{fast.icon}}{{/if}}" title="{{fast.payName}}" width="135" height="40"></a>
			{{else}}
			<span title="{{fast.payName}}" class="icons {{fast.iconCss}}" style="width:121px;height:35px;display:table-caption">{{fast.payName}}</span></a>
			{{/if}}
		</li>
		{{/each}}
	</ul></div>
	<table class="cz-table hidn" id="fastDepositTableId" style="margin: 35px 0 20px 0;width:100%;" cellspacing="0" cellpadding="0" border="0">
		<tbody>
		<tr class="fast_virtual_hint">
			<td colspan="3">
				<div style="border:solid 1px #ffce89;width:712px;background: #fffaec;margin-left:25px;">
					<div class="gift-card-con">
						<ul class="gift-card-list clearfix">
							<li>客服上班时间为：<span class="blue">{{start}}</span>至<span class="blue">{{end}}</span></li>
							<li>转账成功后若超过五分钟未上分，请立即联系客服</li>
							<li>贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。</li>
							<li>"充值金额"若与转帐金额不符，充值将无法准确到帐。</li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<tr class="virtual_fast_hint">
			<td colspan="3">
				<div style="border:solid 1px #ffce89;width:712px;background: #fffaec;margin-left:25px;">
					<div class="gift-card-con">
						<ul class="gift-card-list clearfix">
							<li>客服上班时间为：<span class="blue">{{start}}</span>至<span class="blue">{{end}}</span></li>
							<li>转账成功后若超过五分钟未上分，请立即联系客服</li>
							<li>贴心提醒：充值信息会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。</li>
							<li>"充值个数"若与转帐金额不符，充值将无法准确到帐。</li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<c:if test="${showPayInfo}"><tr>
			<td style="text-align:right;">充值方式：</td>
			<td colspan="2" id="pay_type_name"></td>
		</tr>
			<tr>
				<td style="text-align:right;" width="120">收款姓名：</td>
				<td id="Bname_info" width="200"></td>
				<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>
					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align:right;">收款帐号：</td>
				<td id="Baccount_info"></td>
				<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>
					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
					</div>
				</td>
			</tr>
			<tr id="qrcode_img_wrap">
				<td style="text-align:right;">二维码：</td>
				<td colspan="2"><img src="#" id="qrcode_img_id" style="width: 300px; height: 300px;padding:5px 0">
					<div style="font-size:20px;color:red;margin-top:6px">扫码支付后，请填写充值金额，并点击确定充值，否则无法为您上分！</div></td>
			</tr></c:if>
		<tr>
			<td style="text-align:right;">充值金额：</td>
			<td>
				<div>
					<input class="text" id="amount" name="amount" style="ime-mode: disabled; width: 90px; height: 28px; line-height: 28px; margin-right: 10px;" type="text"><span>元</span>
				</div>
			</td>
			<td id="dan_bi_limit"></td>
		</tr>
		<tr>
			<td style="text-align:right;">充值金额(大写)：</td>
			<td ><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
			<td><span>约等于:</span><span class="approximateRmb">0RMB</span></td>
		</tr>
		<tr class="referenceRateTr">
			<td style="text-align:right;">当日汇率：</td>
			<td class="referenceRate">1ust≈2RMB</td>
			<td> <span class="buttonChangeVirtual" style="display: inline-block;border-radius: 5px;background: #fe920ea8;cursor: pointer;color: #fff;max-height: 36px;width: 50px;text-align: center;line-height: 36px;" >兑换</span></td>
		</tr>
		<tr id="deposit_pay_type_id">
			<td style="text-align:right;"><span class="payType"></span>：</td>
			<td colspan="2"><input class="text" name="bank_cards" minlength="5" maxlength="20"
								   style="ime-mode: disabled; width: 100px; height: 28px; line-height: 28px; margin-right: 10px;" type="text">
				<span class="cz-fasts">请填写正确的<span class="payType"></span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>
		</tr>
		<tr>
			<td></td>
			<td class="czConfirm" colspan="2">
				<c:if test="${showPayInfo}"><div class="gmbtn"><input onclick="Base.fasts.checkFastSub(this)" id="sub-btn" class="sub_btn js_glisten normal" type="button"><div class="clear"></c:if>
				<c:if test="${!showPayInfo}"><div class="nextBtn" style="margin-top: 10px;"><a href="javascript:void(0);" id="sub-btn"onclick="Base.fasts.checkFastSub(this)"></a></div></c:if>
			</div>
			</div></td>
		</tr>
		</tbody>
	</table>
	{{else if banks && banks.length>0}}
	<div id="banks_deposit_divid" style="border-bottom: dashed 1px #ccc; margin: 0 30px 0 25px;"><ul class="ws_bank_new clearfix ws_bank_new_more">
		{{each banks as bank count}}
		<li id="{{bank.iconCss}}"><a href="javascript:void(0);"><input name="bankId"  min="{{bank.min}}" max="{{bank.max}}" iconCss="{{bank.iconCss}}" payType="{{bank.payType}}" value="{{bank.id}}" type="radio">
			{{if bank.icon}}
			<img src="${base}{{if bank.icon}}{{bank.icon}}{{/if}}" title="{{bank.payName}}" width="135" height="40"></a>
			{{else}}
			<span title="{{bank.payName}}" class="icons {{bank.iconCss}}" style="width:121px;height:35px;display:table-caption">{{bank.payName}}</span></a>
			{{/if}}
		</li>
		{{/each}}
	</ul></div>
	<table class="cz-table hidn" id="bank_deposit_table_id" style="margin: 35px 0 20px 0;" width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
		<tr>
			<td colspan="3">
				<div style="border:solid 1px #ffce89;width:712px;background: #fffaec;margin-left:25px;">
					<div class="gift-card-con">
						<ul class="gift-card-list clearfix">
							<li>客服上班时间为：<span class="blue">{{start}}</span>至<span class="blue">{{end}}</span></li>
							<li>贴心提醒：充值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<c:if test="${showPayInfo}"><tr>
			<td style="text-align:right;">充值银行：</td>
			<td colspan="2"id="cz_yin_hang_id"></td>
		</tr>
			<tr>
				<td style="text-align:right;">收款姓名：</td>
				<td width="200" id="Bname_info"></td>
				<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>
					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align:right;">收款帐号：</td>
				<td id="Baccount_info"></td>
				<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>
					<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align:right;">开户网点：</td>
				<td colspan="2"id="kai_hu_wang_dian"></td>
			</tr></c:if>
		<tr>
			<td style="text-align:right;">充值金额：</td>
			<td width="200">
				<input class="text" id="amount" name="amount" style="ime-mode: disabled; width: 90px; height: 28px; line-height: 28px; margin-right: 10px;" type="text">元
			</td>
			<td id="dan_bi_limit"></td>
		</tr>
		<tr>
			<td style="text-align:right;">充值金额(大写)：</td>
			<td colspan="2"><span id="amount_CH" class="bigtxt blue">零</span><span class="bigtxt blue">元整</span></td>
		</tr>
		<tr>
			<td style="text-align:right;">存款人姓名：</td>
			<td><input class="text"  name="depositor" id="depositor" style="ime-mode: disabled; width: 120px; height: 28px; line-height: 28px; margin-right: 10px;"type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td class="czConfirm" colspan="2">
				<c:if test="${showPayInfo}"><div class="gmbtn"><input onclick="Base.banks.checkBankSub(this)" id="sub-btn" class="sub_btn js_glisten normal" type="button"><div class="clear"></c:if>
				<c:if test="${!showPayInfo}"><div class="nextBtn" style="margin-top: 10px;"><a href="javascript:void(0);" id="sub-btn" onclick="Base.banks.checkBankSub(this)"></a></div></c:if>
			</td>
		</tr>
		</tbody>
	</table>
	{{else}}
	<div style="margin-left:22px;font-size:16px;">暂时没有充值入口</div>
	{{/if}}
</script>

<script type="text/html" style="display: none;" id="toOnlinePayTemplate"    >
	<form action="{{formAction}}" method="post" target='_blank'>
		{{each formHiddens as item}}
		<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
		{{/each}}
		<div class="czmain czMain" id="chargersuccess" style="">
			<div class="czConfirm">
				<table cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;border: 1px solid #ffce89;background: #fffaec;width:100%">
					<tbody>
					<tr>
						<td width="150"><span class="suc-tit">充值信息：</span></td>
						<td><div style="padding: 10px 0 10px; position: relative; float: left; width: 135px; *padding-top: 0;">
							<span title="" class="icons {{payType}}" style="width:121px;height:35px;display:table-caption"></span></a>
							<div class="clear"></div>
						</div></td>
					</tr>
					<tr>
						<td width="150"><span class="suc-tit">订单号：</span></td>
						<td><span class="blue">{{orderId}}</span></td>
					</tr>
					<tr>
						<td width="150"><span class="suc-tit">充值金额：</span></td>
						<td><span class="red">{{amount}}</span> 元</td>
					</tr>
					<tr>
						<td width="150"></td>
						<td><div class="gmbtn">
							<input class="sub_btn js_glisten normal" type="submit" value="" onclick="$(this).hide()">
							<div class="clear"></div>
						</div></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</form>
</script>

<script type="text/html" style="display: none;" id="toFastPayTemplate">
	<div class="czmain czMain" id="chargersuccess" style="">
		<div class="czConfirm">
			<table cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;border: 1px solid #ffce89;background: #fffaec;width:100%">
				<tbody>
				{{if iconCss == 'virtual'}}
				<tr>
					<td width="120"><span class="suc-tit">钱包类型：</span></td>
					<td colspan="2">{{walletType}}</td>
				</tr>
				{{/if}}
				{{if iconCss != 'virtual'}}
				<tr>
					<td width="120"><span class="suc-tit">充值方式：</span></td>
					<td colspan="2">{{payName}}</td>
				</tr>
				{{/if}}
				{{if iconCss !='virtual'}}
				<tr>
					<td width="120"><span class="suc-tit">收款姓名：</span></td>
					<td width="500">{{payUserName}}</td>
					<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
						</div>
					</td>
				</tr>
				{{/if}}
				<tr>
					{{if iconCss !='virtual'}}
						<td width="120"><span class="suc-tit">收款帐号：</span></td>
					{{/if}}
					{{if iconCss == 'virtual'}}
						<td width="120"><span class="suc-tit">收款地址：</span></td>
					{{/if}}
					<td width="500"><span class="money blue" id="orderById">{{payAccount}}</span></td>
					<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">订单号：</span></td>
					<td width="500"><span class="blue">{{orderNo}}</span></td>
					<td style="text-align: left;position: relative;"><button id="Boid" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					{{if iconCss == 'virtual'}}
					<td width="120"><span class="suc-tit">充值个数：</span></td>
					{{/if}}
					{{if iconCss != 'virtual'}}
					<td width="120"><span class="suc-tit">充值金额：</span></td>
					{{/if}}
					<td width="500"><span class="red">{{money}}</span></td>
					<td></td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">二维码：</span></td>
					<td><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 300px; height: 300px;padding:5px 0"></td>
					<td></td>
				</tr>
				<tr style="line-height: 20px;">
					<td colspan="3" style="text-align:center;font-size:12px;"><span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span></td>
				</tr>
				<tr>
					<td width="120"></td>
					<td><div class="gmbtn">
						<input onclick="goTo()" class="sub_btn js_glisten normal" type="button">
						<div class="clear"></div>
					</div></td>
					<td></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</script>

<script type="text/html" style="display: none;" id="toBankPayTemplate">
	<div class="czmain czMain" id="chargersuccess" style="">
		<div class="czConfirm">
			<table cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;border: 1px solid #ffce89;background: #fffaec;width:100%">
				<tbody>
				<tr>
					<td width="120"><span class="suc-tit">充值银行：</span></td>
					<td colspan="2">{{payName}}</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">收款姓名：</span></td>
					<td width="500" xx_xx="Bname" id="Bname_info">{{creatorName}}</td>
					<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">收款帐号：</span></td>
					<td width="500" xx_xx="Baccount" id="Baccount_info">{{bankCard}}</td>
					<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">开户网点：</span></td>
					<td colspan="2">{{bankAddress}}</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">订单号：</span></td>
					<td width="500" xx_xx="Boid" id="Boid_info"><span class="blue">{{orderNo}}</span></td>
					<td style="text-align: left;position: relative;"><button id="Boid" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">充值金额：</span></td>
					<td width="500" xx_xx="Bamount" id="Bamount_info"><span class="red">{{money}}</span></td>
					<td></td>
				</tr>
				<tr style="line-height: 20px;">
					<td colspan="3" style="text-align:center;font-size:12px;"><span class="red">&nbsp;贴心提醒：充值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</span></td>
				</tr>
				<tr>
					<td width="120"></td>
					<td><div class="gmbtn">
						<input onclick="goTo()" class="sub_btn js_glisten normal" type="button">
						<div class="clear"></div>
					</div></td>
					<td></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</script>

<%--//虚拟货币入款--%>
<script type="text/html" style="display: none;" id="toVritualPayTemplate">
	<div class="czmain czMain" id="chargersuccess" style="">
		<div class="czConfirm">
			<table cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;border: 1px solid #ffce89;background: #fffaec;width:100%">
				<tbody>
				<tr>
					<td width="120"><span class="suc-tit">充值方式：</span></td>
					<td colspan="2">{{payName}}</td>
				</tr>
<%--				<tr>--%>
<%--					<td width="120"><span class="suc-tit">收款姓名：</span></td>--%>
<%--					<td width="500">{{payUserName}}</td>--%>
<%--					<td style="text-align: left;position: relative;"><button id="Bname" type="button" class="copy_btn btn-red">复&nbsp;制</button>--%>
<%--						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">--%>
<%--						</div>--%>
<%--					</td>--%>
<%--				</tr>--%>
<%--				<tr>--%>
<%--					<td width="120"><span class="suc-tit">收款帐号：</span></td>--%>
<%--					<td width="500"><span class="money blue" id="orderById">{{payAccount}}</span></td>--%>
<%--					<td style="text-align: left;position: relative;"><button id="Baccount" type="button" class="copy_btn btn-red">复&nbsp;制</button>--%>
<%--						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">--%>
<%--						</div>--%>
<%--					</td>--%>
<%--				</tr>--%>
				<tr>
					<td width="120"><span class="suc-tit">订单号：</span></td>
					<td width="500"><span class="blue">{{orderNo}}</span></td>
					<td style="text-align: left;position: relative;"><button id="Boid" type="button" class="copy_btn btn-red">复&nbsp;制</button>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
						</div>
					</td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">充值个数：</span></td>
					<td width="500"><span class="red">{{money}}</span></td>
					<td></td>
				</tr>
				<tr>
					<td width="120"><span class="suc-tit">二维码：</span></td>
					<td><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 300px; height: 300px;padding:5px 0"></td>
					<td></td>
				</tr>
				<tr style="line-height: 20px;">
					<td colspan="3" style="text-align:center;font-size:12px;"><span class="red">&nbsp;贴心提醒：充值数据会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值个数"若与转帐个数不符，充值将无法准确到帐。</span></td>
				</tr>
				<tr>
					<td width="120"></td>
					<td><div class="gmbtn nextBtn" style="    text-align: center;">
                        <a href="javascript:void(0)" id="sub_btn_virtual"></a>
						<div class="clear"></div>
					</div></td>
					<td></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</script>

<style>
	#pcInputMoney span{
		width: 65px;
		color: #000000;
		margin-left: 20px;
		float: left;
	}
	#pcInputMoney span:hover{
		color: #f6383a;
		cursor: pointer;
	}
	.inputActive{
		color: #f6383a!important;
	}
</style>