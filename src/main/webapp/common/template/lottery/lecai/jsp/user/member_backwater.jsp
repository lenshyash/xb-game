<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：中奖记录</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">
					您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label>
					<a id="user_level" href="javascript:void(0);" target="_blank"
						title=""><!-- <img
						src="//static.lecai.com/img/caibei/level/cb_0.gif"> --></a>
				</div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong
							id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}
					</span>
				</div>
				<a class="btn recharge normal" value=" " href="${base}/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" value=" "
					href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox">
					<div class="prizetitle">反水记录</div>
					<div class="clear"></div>
				</div>
				<div class="filter">
					<ul>
						<li>
							<div class="category">
								<select name="backwaterStatus" class="chzn-done" id="backwaterStatus">
										<option value="">所有状态</option>
										<option value="4">已领取</option>
										<option value="9">未领取</option>
								</select>
							</div>
						</li>
						<li>
							<div class="category_time">
								<select name="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
							<li>
							<div class="time-piece">
								<input name="startTime" id="startTime" class="text text75"
									readonly="readonly" value="${startTime}" type="text" data-end-date="${endTime}">
								<span>至</span> <input name="endTime" id="endTime" class="text text75"
									readonly="readonly" value="${endTime}" type="text" data-end-date="${endTime}"> <input
									class="btn normal" type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>游戏类型</td>
								<td>账号</td>
								<td>下注日期</td>
								<td>反水金额</td>
								<td>反水状态</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot">
							<tr>
								<td colspan="6" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var curNavFlag = 0;
		$(function(){
			$(".category select,.category_time select").chosen({disable_search:true});
			$('#startTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			$('#endTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			
			initRdsData(curNavFlag);
			
			$(".category_time select").change(function() {
				var v = $(this).val();
				quickSelDate(parseInt(v));
			})
		})
		
		function search(){
			$("#pageNumber").val(1);
			initRdsData(curNavFlag);
		}
		
		function initRdsData(flag){
			
			var load = new Loading();
			load.init({
				target:"#betResultOrder"
			})
			load.start();
			var data = {
					status: $("#backwaterStatus").val(),
					startTime:$("#startTime").val()+" 00:00:00",
					endTime:$("#endTime").val()+" 23:59:59",
					rows:10,
					page:$("#pageNumber").val()
			}
			$.ajax({
				url:"${base}/center/record/backwater/list.do",
				data:data,
				success:function(res){
					if(res.total == 0){
						//无数据
						temp = '<tr><td colspan="8" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td></tr>';
						$("#betResultOrder").html(temp);
						$("#orderBetOrderTblFoot").hide();
					}else{
						addTemplate(res);
					}
					load.stop();
				}
			})
		}
		
		function addTemplate(res){
			var temp ="";
			$.each(res.rows,function(index,item){
				temp += '<tr '+(index%2!=0?'class="bgcolor"':'')+'><td>'+getFormat(item.betType,1)+'</td>';
				temp += '<td>'+item.account+'</td>';
				temp += '<td>'+item.betDate+'</td>';
				temp += '<td class="red">'+item.backwaterMoney+'</td>';
				temp += '<td >'+getFormat(item.backwaterStatus,2)+'</td>';
				temp += '<td>'+getFormat(item.backwaterStatus,3,item.id)+'</td></tr>';
			})
			$("#betResultOrder").html(temp);
			var totalPage = res.total%10==0?~~(res.total/10):~~(res.total/10)+1;
			layerPage(res.total, $("#pageNumber").val(), totalPage);
		}
		
		function getFormat(value,t,rowId){
			if(t == 1){
				if(value==1){
					return "彩票";
				}else if(value==2){
					return "真人";
				}else if(value==3){
					return "电子";
				}else if(value==4){
					return "体育";
				}else if(value==5){
					return "六合特码B";
				}else if(value==6){
					return "六合正码B";
				}else if(value==11){
					return "系统彩";
				}else if(value==15){
					return "十分六合彩特码B";
				}else if(value==16){
					return "十分六合彩正码B";
				}else if(value==99){
					return "三方体育";
				}
			}else if(t == 2){
				if(value==4){
					return '已反水';
				}else if(value==9){
					return '待领取';
				}else if(value==8){
					return '已过期';
				}
			}else if(t == 3){
				if(value==9){
					return '<a href="#" onclick="cashing(\''+rowId+'\')">领取</a>';
				}else{
					return '';
				}
			}
		}
		
		function cashing(rowId){
			$.ajax({
				url:"${base}/center/record/backwater/cashing.do",
				data:{
					id:rowId
				},
				success:function(res){
					if(res.success){
						initRdsData(curNavFlag);
					}else{
						layer.msg(res.msg,{icon:2});
					}
				}
			})
		}
		
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>