<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
    <meta name="description" content="${not empty webDescription?webDescription:_title}">
    <title>会员中心 - ${_title}</title>
    <link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
    <jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
    <link rel="stylesheet"
          href="${base}/common/template/lottery/lecai/css/user_content.css?v="
          type="text/css">
    <script type="text/javascript"
            src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
    <jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
    .searchDetail table th,td{
        font-size:12px;
    }
</style>
<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
<div class="container" style="margin-top:20px;min-height:800px;">
    <jsp:include page="member_left_nav.jsp"></jsp:include>
    <div class="userRight">
        <div class="userTitle mgb10" id="pageName">电子投注记录</div>
        <div class="userMain mgb10">
            <%-- <ul class="todayView mgb10">
            <li>今日概况</li>
            <li>投注金额：<span>${-allBetAmount}元</span></li>
            <li>中奖金额：<span>${yingkuiAmount}元</span></li>
            <li>盈利：<span>${allWinAmount}元</span></li>
        </ul> --%>
            <ul class="searchFirst">
                <%--                <li><span>彩种：</span>--%>
                <%--                    <ins class="selectIcon">--%>
                <%--                        <select class="userSelect" name="code" id="lotCode">--%>
                <%--                            <option value="">所有彩种</option>--%>
                <%--                            <c:forEach items="${lotList}" var="lot">--%>
                <%--                                <option value="${lot.code}"<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>--%>
                <%--                            </c:forEach>--%>
                <%--                        </select>--%>
                <%--                        <em></em>--%>
                <%--                    </ins>--%>
                <%--                </li>--%>
                <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
                </li>
                <%--<li class="_state">--%>
                    	<%--<span>类型：--%>
       						<%--<a class="userSearch time active" value=" ">全部</a>--%>
       						<%--<a class="userSearch time " value="2">已中奖</a>--%>
       						<%--<a class="userSearch time " value="3">未中奖</a>--%>
       						<%--<a class="userSearch time " value="1">等待开奖</a>--%>
       						<%--<a class="userSearch time " value="4">撤单</a>--%>
       					<%--</span>--%>
                <%--</li>--%>
            </ul>
            <ul class="searchFirst accountValue" style="display:none;">
                <li class="_state" style="display: flex;align-items: center;padding-left:21px;">
                    <span>账号:</span>
                    <input class="inputText" id="account" value="" type="text" placeholder="下级投注查询" style="background:#fff;height:30px;margin-left:10px;">
                    <input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
                </li>
            </ul>
            <div class="searchDetail">
                <table>
                    <tbody>
                    <tr>
                        <th class="touzhuName" style="display:none;">账号</th>
                        <td>三方游戏类型</td>
                        <td>注单号</td>
                        <td>游戏名称</td>
                        <td>游戏局号</td>
                        <td>投注金额</td>
                        <td>输赢金额</td>
                        <td>投注时间(北京)</td>
                    </tr>
                    </tbody>
                    <tbody class="Records_listCont">
                    <tr style="border-bottom: 0px;">
                        <td colspan="100">
                            <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                            <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                <div class="pageNav">
                    <div  class="pageNav">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>
            <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
        </div>
    </div>
</div>
<script>
    $(function () {
        initRdsData(1)
        $("._time a").click(function () {
            $(this).addClass('active').siblings('a').removeClass('active')
            today($(this).attr('value'))
            initRdsData(1)
        })
        $("._state a").click(function () {
            $(this).addClass('active').siblings('a').removeClass('active')
            lottereyStatus = $(this).attr('value')
            initRdsData(1)
        })
        $('#lotCode').change(function(){
            lottereyStatus = $(this).attr('value')
            initRdsData(1)
        })
    })

    function initRdsData(p) {
        $(".Records_listCont").html(noTakeNotes)
        $(".notContent").hide()
        $(".loadingGif").show()
        if(!endTime){
            endTime = startTime
        }
        var data = {
            startTime : startTime + " 00:00:00",
            endTime : endTime + " 23:59:59",
            rows : 10,
            page : p
        }
        $.ajax({
            url : '${base}/center/df/record/betrecord/egamerecord.do',
            data : data,
            success : function(res) {
                $(".notContent").show()
                $(".loadingGif").hide()
                if(res.total > 0){
                    addTemplate(res)
                    page = p;
                    totalPage = Math.ceil((res.total/10))
                    lotteryPage(res.total,totalPage)
                    lastPage = totalPage
                    $("#pageNum").html(p)
                }
            }
        })
    }
    function addTemplate(res){
        var temp ="", bd=null;
        $.each(res.rows,function(index,item){
            temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>';
            switch(item.type-0){
                case 1: temp += "AG</td>";break;
                case 2: temp += "BBIN</td>";break;
                case 3: temp += "MG</td>";break;
                case 4:temp += "QT</td>";break;
                case 5:temp += "ALLBET</td>";break;
                case 6:temp += "PT</td>";break;
                case 7:temp += "OG</td>";break;
                case 8:temp += "DS</td>";break;
                case 9:temp += "CQ9</td>";break;
                case 11:temp += "JDB</td>";break;
                case 92:temp += "TTG</td>";break;
                case 95:temp += "MW</td>";break;
                case 96:temp += "ISB</td>";break;
                case 98:temp += "BG</td>";break;
                case 97:temp += "VR</td>";break;
            }
            temp+='<td>'+item.bettingCode+'</td>';
            temp+='<td>'+item.gameType+'</td>';
            if(item.type==3){
                temp+='<td>'+item.tableCode+'</td>';
            }else if(item.type==2){
                temp+='<td></td>';
            }else{
                if(item.gameTypeCode=="HUNTER"){
                    temp+='<td>'+item.playType+'</td>';
                }
                temp+='<td>'+item.tableCode+'</td>';
            }
            temp+='<td>'+item.bettingMoney+'</td>';
            temp+='<td>'+item.winMoney+'</td>';
            bd = new Date(item.bettingTime);
            temp+='<td>'+bd.format("MM月dd日,hh:mm:ss")+'</td></tr>';
        })
        $("#betResultOrder").html(temp);
        $("#orderBetOrderTblFoot").show();
        $("#orderBetAmount").html(res.aggsData.bettingMoneyCount||0);
        $("#orderWinAmount").html(res.aggsData.winMoneyCount||0);
        $(".Records_listCont").html(temp)
    }
</script>
<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>
