<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">公告详情</div>
            <div class="userMain mgb10">
                <div class="pd30"><h2 class="articleTitle"></h2>
                    <div class="articlePublishTime"></div>
                    <div class="articleContent">
                    </div>
                </div>
            </div>
        </div>
		</div>
		<script>
		$(function(){
			initRdsData()
		})
		function initRdsData() {
			$.ajax({
				url : '${base}/getConfig/getArticleById.do?id='+Request['id'],
				data : {},
				success : function(res) {
					if(res.totalCount == 0){
						//无数据
					}else{
						$(".articleTitle").html(res.title)
						$(".articlePublishTime").html(getMyDate(res.updateTime))
						$(".articleContent").html(res.content)
					}
				}
			})
		}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>