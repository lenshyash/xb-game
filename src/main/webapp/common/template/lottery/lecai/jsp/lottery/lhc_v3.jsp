<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/lhc.css?v=${caipiao_version}"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/mark_six.js?v=${caipiao_version}2"></script>
<div class="lhcMain">
	<div id="content" class="lhc_content">
		<!-- 预设金额和提交充值按钮 -->
		<div class="skin_red">
			<div id="lhcContent">
				<jsp:include page="lhc_set_money.jsp" flush="true"><jsp:param
						value="true" name="kj" /></jsp:include>
				<div id="bet_panel" class="bet_panel input_panel bet_closed">
					<%-- 投注内容填充区 --%>
				</div>
				<jsp:include page="lhc_set_money.jsp" flush="true"><jsp:param
						value="false" name="kj" />
				</jsp:include>
				<!-- 预设金额 -->
			</div>
			<div class="lhc_R right">
				<div class="user_R">
					<ul>
						<li>会员帐号：<strong class="red" id="userName"></strong></li>
						<li>会员余额：<strong class="red refreshM"></strong><i
							onclick="_Core.refreshMoney()" class="img-login-refresh icon"></i></li>
						<li class="shadow"></li>
					</ul>
				</div>
				<jsp:include page="../lottery/version_v5_right.jsp"></jsp:include>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>