<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
	<meta name="description" content="${not empty webDescription?webDescription:_title}">
	<title>购彩大厅-${_title}</title>
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<%--	<c:if test="${domainFolder == 'x00204'}"><jsp:include page="${base }/member/x00204/ArticleList.jsp"></jsp:include></c:if>--%>
<%--	<c:if test="${domainFolder != 'd00528'}"><jsp:include page="/common/modelCommon/new/index.jsp"/></c:if>--%>

	<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/lottery.css?v=${caipiao_version}"  type="text/css">
	<%-- <script type="text/javascript" src="${base}/common/js/layer/layer.js"></script> --%>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="main">
		<input type="hidden" value="1" id="navId" />
		<c:forEach items="${lotterySort}" var="v">
		<div class="lcbox">
            <div class="lctit">
            <c:if test="${v != 4}">
            	<div class="jingcai" id="jc_lottery_area"><h2>${v==1?'时时彩':v==2?'低频彩':v==3?'快开':v==7?'快三':v==5?'11选5':v==8?'快乐十分':v==9?'PC蛋蛋':'香港彩' }</h2></div></div>
            </c:if>
            <div class="lcmain">
                <ul class="lot_ul">
                	<c:forEach items="${lotList}" var="lot" varStatus="lotIndex">
                	<c:if test="${lot.viewGroup == v}">
                    <li>
                    	<div class="btnbox_top">
	                        <a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">
	                        <c:choose>
								   <c:when test="${not empty lot.imgUrl}">
								    <img src="${lot.imgUrl}" alt="">
								       <%-- <img src="${lot.imgUrl}" alt=""> --%>
								   </c:when>
								   <c:otherwise> 
									    <img src="${base}/common/template/lottery/lecai/images/gclogo/${lot.code}.png" alt="">
								   </c:otherwise>
								</c:choose>	                        
	                        </a>
	                        <div class="info">
	                            <h3><a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}">${lot.name}</a></h3>
	                            <span>第&nbsp;<font color="red" id="${lot.code}QiHao">无期号</font>&nbsp;期</span>
	                            <br>
	                            <div class="gct_time"><div class="gct_time_now"><div class="gct_time_now_l" id="${lot.code}OpenTime"><span class="leaveh-2">距离封盘截止</span><span class="leaveh-1">00:00:00</span></div></div></div>
	                        </div>
                        </div>
                        <div class="clear"></div>
                        <div class="lot-fore"><span class="fl">上期开奖</span><div class="show-gd1"><ul class="big_ball" id="${lot.code}LastHaoMa">
                        <span class="tabulousclear red">正在开奖...</span>
                        </ul></div></div>
                        <div class="btnbox">
                            <a href="${base}/lotteryV3/trend.do?lotCode=${lot.code}">走势图</a>
                            <a href="${base}/lotteryV3/draw/list.do?lotCode=${lot.code}">开奖公告</a>
                            <a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}" class="buybtn">立即投注</a>
                        </div>
                    </li>
                    </c:if>
                    </c:forEach>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        </c:forEach>
	</div>
	<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/lottery.js?v=${caipiao_version}"></script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
	<c:choose>
		<c:when test="${domainFolder== 'd00528' || domainFolder == 'd003' || domainFolder == 'd005'}">
			<jsp:include page="${base}/common/modelCommon/new/pop/version_d3/index.jsp"/>
		</c:when>
		<c:otherwise>
			<jsp:include page="/common/modelCommon/new/index.jsp"/>
		</c:otherwise>
	</c:choose>
</body>
</html>