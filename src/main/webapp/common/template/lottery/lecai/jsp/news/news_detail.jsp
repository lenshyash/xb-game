<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>最新资讯-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/news.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param
			value="true" name="type" />
	</jsp:include>
	<input type="hidden" value="5" id="navId" />
	<div class="news_main_title">
		<img src="${base}/common/template/lottery/lecai/images/news/zixun_title.jpg">
		<div class="clear"></div>
	</div>
	<div class="main">
		<div class="news_left">
			<div class="focus_box">
				<div class="focus_main">
					<div class="news_box">
						<div class="news_title">
							<h1>${article.title}</h1>
							<p>
								<a href="${base}/lotteryV3/index.do" target="_blank">${_title}</a> Admin ${article.updateTime}
							</p>
						</div>
						<div class="detail">
							<p></p>
							${article.content}
							<p></p>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="news_right">
			<div class="jrkj_box" id="today_draw">
				<c:forEach items="${lotList}" var="lot" varStatus="l">
					<c:if test="${l.index<9}">
					<a href="${base}/lotteryV3/lotDetail.do?lotCode=${lot.code}" target="_blank" class="btn <c:if test="${l.index%2!=0}">red</c:if>">${lot.name}</a>
					</c:if>
				</c:forEach>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>