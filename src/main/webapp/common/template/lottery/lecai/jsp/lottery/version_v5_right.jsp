<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="zxzd" class="hidden">
	<table>
		<thead>
			<tr>
				<th colspan="2">最新注单</th>
			</tr>
		</thead>
		<tbody id="betMarkResultOrder">
		</tbody>
	</table>
</div>
<div id="lmcl" <c:if test="${bly.code == 'SFLHC' || bly.code == 'LHC' || bly.code == 'TMLHC' || bly.code == 'WFLHC' || bly.code == 'HKMHLHC'}">class="hidden"</c:if>>
	<table>
		<thead>
			<tr>
				<th colspan="2" class="table_side">两面长龙排行</th>
			</tr>
		</thead>
		<tbody id="ommitQueue">
		</tbody>
	</table>
</div>
<c:if test="${bly.code != 'SFLHC' && bly.code != 'LHC' && bly.code != 'TMLHC' && bly.code != 'WFLHC' && bly.code != 'HKMHLHC'}">
<script type="text/javascript">
	$(function(){
		Mark.lotQueue();
	})
</script>
</c:if>
<c:if test="${bly.code == 'SFLHC' || bly.code == 'LHC' || bly.code == 'TMLHC' || bly.code == 'WFLHC' || bly.code == 'HKMHLHC'}">
<div id="kjtz" class="">
	<table>
		<thead>
			<tr>
				<th colspan="3" class="table_side">快捷投注</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td
					data="01,03,05,07,09,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47">单码</td>
				<td data="01,03,05,07,09,11,13,15,17,19,21,23">小单</td>
				<td
					data="01,03,05,07,09,10,12,14,16,18,21,23,25,27,29,30,32,34,36,38,41,43,45,47">合单</td>
			</tr>
			<tr>
				<td
					data="02,04,06,08,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48">双码</td>
				<td data="02,04,06,08,10,12,14,16,18,20,22,24">小双</td>
				<td
					data="02,04,06,08,11,13,15,17,19,20,22,24,26,28,31,33,35,37,39,40,42,44,46,48">合双</td>
			</tr>
			<tr>
				<td
					data="25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48">大码</td>
				<td data="25,27,29,31,33,35,37,39,41,43,45,47">大单</td>
				<td
					data="07,08,09,16,17,18,19,25,26,27,28,29,34,35,36,37,38,39,43,44,45,46,47,48">合大</td>
			</tr>
			<tr>
				<td
					data="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24">小码</td>
				<td data="26,28,30,32,34,36,38,40,42,44,46,48">大双</td>
				<td
					data="01,02,03,04,05,06,10,11,12,13,14,15,20,21,22,23,24,30,31,32,33,40,41,42">合小</td>
			</tr>
			<tr>
				<td data="01,02,03,04,05,06,07,08,09">0头</td>
				<td data="10,20,30,40">0尾</td>
				<td data="05,15,25,35,45">5尾</td>
			</tr>
			<tr>
				<td data="10,11,12,13,14,15,16,17,18,19">1头</td>
				<td data="01,11,21,31,41">1尾</td>
				<td data="06,16,26,36,46">6尾</td>
			</tr>
			<tr>
				<td data="20,21,22,23,24,25,26,27,28,29">2头</td>
				<td data="02,12,22,32,42">2尾</td>
				<td data="07,17,27,37,47">7尾</td>
			</tr>
			<tr>
				<td data="30,31,32,33,34,35,36,37,38,39">3头</td>
				<td data="03,13,23,33,43">3尾</td>
				<td data="08,18,28,38,48">8尾</td>
			</tr>
			<tr>
				<td data="40,41,42,43,44,45,46,47,48">4头</td>
				<td data="04,14,24,34,44">4尾</td>
				<td data="09,19,29,39,49">9尾</td>
			</tr>
			<tr>
				<td data="12,24,36,48">牛</td>
				<td data="08,20,32,44">蛇</td>
				<td data="04,16,28,40">鸡</td>
			</tr>
			<tr>
				<td data="11,23,35,47">虎</td>
				<td data="07,19,31,43">马</td>
				<td data="03,15,27,39">狗</td>
			</tr>
			<tr>
				<td data="10,22,34,46">兔</td>
				<td data="06,18,30,42">羊</td>
				<td data="02,14,26,38">猪</td>
			</tr>
			<tr>
				<td data="09,21,33,45">龙</td>
				<td data="05,17,29,41">猴</td>
				<td data="01,13,25,37,49">鼠</td>
			</tr>
			<tr>
				<td data="01,02,07,08,12,13,18,19,23,24,29,30,34,35,40,45,46"
					class="red">红</td>
				<td data="03,04,09,10,14,15,20,25,26,31,36,37,41,42,47,48"
					class="blue">蓝</td>
				<td data="05,06,11,16,17,21,22,27,28,32,33,38,39,43,44"
					class="green">绿</td>
			</tr>
			<tr>
				<td data="01,07,13,19,23,29,35,45" class="red">红单</td>
				<td data="03,09,15,25,31,37,41,47" class="blue">蓝单</td>
				<td data="05,11,17,21,27,33,39,43" class="green">绿单</td>
			</tr>
			<tr>
				<td data="02,08,12,18,24,30,34,40,46" class="red">红双</td>
				<td data="04,10,14,20,26,36,42,48" class="blue">蓝双</td>
				<td data="06,16,22,28,32,38,44" class="green">绿双</td>
			</tr>
			<tr>
				<td data="29,30,34,35,40,45,46" class="red">红大</td>
				<td data="25,26,31,36,37,41,42,47,48" class="blue">蓝大</td>
				<td data="27,28,32,33,38,39,43,44" class="green">绿大</td>
			</tr>
			<tr>
				<td data="01,02,07,08,12,13,18,19,23,24" class="red">红小</td>
				<td data="03,04,09,10,14,15,20" class="blue">蓝小</td>
				<td data="05,06,11,16,17,21,22" class="green">绿小</td>
			</tr>
			<tr>
				<td
					data="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49">全选</td>
				<td colspan="2" data="clear">取消</td>
			</tr>
		</tbody>
	</table>
</div>
</c:if>