<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<input type="hidden" value="${base}" id="base" />
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/style.css?v=${caipiao_version}1.1" type="text/css">
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/header.css?v=${caipiao_version}" type="text/css">
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/layer_dialog.css?v=${caipiao_version}" type="text/css">
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-browser.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery.ajax.js?v=1.1.12" path="${base}"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/base.js?v=2.7"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/date.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<script type="text/javascript">
	LECAI = {
		base : '${base}',
		folder : "${stationFolder}",
		caipiao_version : '${caipiao_version}',
		caipiao : '${caipiao eq "on"?true:false}',
		code : '${bly.code}',
		type : '${bly.type}',
		fanShui : ${fanShui eq "on"?true:false},
		version : '${lotVersion}',
	}
</script>
