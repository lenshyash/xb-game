<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">推广链接</div>
            <div class="userMain mgb10">
                <ul class="searchFirst zhangbian">
                	<li style="width:150px;"><span> </span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   <option selected="selected" value="0">全部</option>
									<c:if test="${trunOnoff ne 'on' }">
									<option value="2">会员</option>
									</c:if>
									<option value="1">代理</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_state" style="display: flex;align-items: center;">
                    	<span>&nbsp;</span>
                    	<input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
                    	<input class="submitBtn" onclick="addLink('');" value="添加推广链接" type="button" style="border:none;margin-left:10px;">
                    	<a href="${base}/center/df/record/betrecord/rebateExplain.do" style="color:rgb(0, 138, 255); margin-left:10px;">点击查看返点赔率表</a>
       				</li>
                </ul>
                <div class="searchDetail">
                    <table>
                       <thead>
                        <tr class="title">
								<td>账号</td>
								<td>类型</td>
								<c:if test = "${isMulti }"><td>返点</td></c:if>
								<c:if test = "${profitSharFlag }"><td>占成</td></c:if>
								<c:if test = "${memberRateFlag }"><td>动态赔率</td></c:if>
								<td>邀请码</td>
								<td>推广链接</td>
								<td>短链接</td>
<!-- 								<td>加密链接</td>
 -->								<td>状态</td>
								<td>操作</td>
							</tr>
                        </thead>
                        <tbody id="betResultOrder" style="font-family: serif;">
							<tr>
								<td colspan="10" class="nodate" height="200">暂无数据，<a href="${base}/index.do">立即投注</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot" style="display:none;">
							<tr>
								<td colspan="3" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td colspan="2">&nbsp;</td>
								<td class="red" id="orderWinAmount"></td>
							</tr>
						</tfoot>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：账变记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1);
		        $('#lotCode').change(function(){
		        	initRdsData(1);
		        })
			})
/* 			var json = '{"total":2,"rows":[{"shortUrl1":"http://t.cn/EzBXg4f","userAccount":"xiaohai","linkKey":"64993","linkUrl":"d0051.xbzhanshi.com/registerMutil/link_64993.do","id":117,"type":2,"adminUrl":"pay.vad87da.com/registerMutil/link_64993.do","userId":2922,"status":2,"stationId":41},{"shortUrl1":"http://t.cn/EzBp7e4","userAccount":"xiaohai","linkKey":"70043","linkUrl":"d0051.xbzhanshi.com/registerMutil/link_70043.do","id":118,"type":2,"adminUrl":"pay.vad87da.com/registerMutil/link_70043.do","userId":2922,"status":2,"stationId":41}]}'
 */			function initRdsData(p){
				$(".notContent").hide()
		    	$(".loadingGif").show()
				var pageNumber=$("#pageNumber").val();
				if(pageNumber==null || pageNumber == ''){
					pageNumber = 1;
				}
				var data = {
					type: $("#lotCode").val(),
					pageSize:10,
					pageNumber:p,
					stationId: $("#stationId").val()
				}
				$.ajax({
					url:"${base}/daili/generalizeLink/list.do",
					data:data,
					success:function(res){
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.total == 0){
							//无数据
							
						}else{
							addTemplate(res)
								page = p;
								totalPage = Math.ceil((res.total/10))
								lotteryPage(res.total,totalPage)
								lastPage = totalPage;
								$("#pageNum").html(p)	
						}
					}
				})
			}
			function addTemplate(res){
				var temp ="", bd=null;
				$.each(res.rows,function(index,item){
					bd = new Date(item.bettingDate);
					temp += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>'+item.userAccount +'</td>';
					temp += '<td>'+typeFormatter(item.type)+'</td>';
					if(${isMulti}){
						temp += '<td>'+cpRollFormatter(item.cpRolling)+'</td>';
					}
					if(${profitSharFlag}){
						temp += '<td class="isProfitShare">'+cpRollFormatter(item.profitShare)+'</td>';
					}
					if(${memberRateFlag}){
						temp += '<td class="isDynamicRate">'+cpRollFormatter(item.dynamicRate)+'</td>';
					}
					temp += '<td>'+item.linkKey+'</td>';
					temp += '<td style="white-space:unset;">'+pasteFormatter(item.linkUrl)+'</td>';
					temp += '<td>'+shortUrlFormatter(item)+'</td>';
/* 					temp += '<td style="white-space:unset;">'+encodeUrlFormatter(item)+'</td>';
 */					temp += '<td>'+statusFormatter(item)+'</td>';
					temp += '<td>'+operateFormatter(item)+'</td>';
				})
				$("#betResultOrder").html(temp);	
			}
			function operateFormatter(row) {
				return [ '<a class="eidt" onclick="addLink('+ row.linkKey+')" href="javascript:void(0)" title="修改">',
						'<i class="glyphicon glyphicon-pencil"></i>', '修改</a>  ',
						'<a class="del" onclick="del('+ row.id+')" href="javascript:void(0)" title="删除">',
						'<i class="glyphicon glyphicon-remove"></i>', '删除</a>  ' ]
						.join('');
			}
			function statusFormatter(row){
				if(row.status==1){
					return '禁用'+'<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')" style="color:#4aa9db;" ><span id="status'+row.id+'" data-status="2"  style="color:#4aa9db;">（启用）</span></a>';
				}else{
					return '启用'+'<a  href="javascript:void(0)" onclick="changeStatus('+row.id+')"  style="color:red;"><span  id="status'+row.id+'" data-status="1"  style="color:red;">（禁用）</span></a>';
				}
			}
			function changeStatus(id){
				var status = $("#status"+id+"").attr("data-status");
				var txt="禁用成功";
				if(status==2){
					txt="启用成功";
				}
				$.ajax({
					url : "${base}/daili/generalizeLink/updateStatusById.do",
					data : {
						id : id,
						status:status
					},
					success : function() {
						alert(txt);
						window.location.reload();
					}
				});
			}
			function cpRollFormatter(value) {
				if(value){
					return value;
				}else{
					return '-';
				}
			}
			function typeFormatter(value) {
				if (value == 1) {
					return '代理';
				} else {
					return '会员';
				}
			}
			function initCombo() {
				$.ajax({
					url : "${base}/daili/dlmnyrd/money/record/type.do",
					async : false,
					success : function(data) {
						var eachdata = {
							"data" : data
						};
						typeData = toTypeMap(data);
						var html = template('recordtype_tpl', eachdata);
						$("#type").append(html);
						var url =  location.href;
						if(url.indexOf('param=')!=-1){
							var obj = $('#type');
							if(url.indexOf('param=sdkk')!=-1){
								obj.find("option[value='2']").attr("selected",true); 
							}else if(url.indexOf('param=sdjk')!=-1){
								obj.find("option[value='1']").attr("selected",true); 
							}
						}
					}
				});
			}
			function toTypeMap(data) {
				var map = {};
				if (data) {
					for (var i = 0; i < data.length; i++) {
						if (!data[i]) {
							continue;
						}
						map[data[i].type] = data[i].name;
					}
				}
				return map;
			}
			function dateFormatter(value) {
				return DateUtil.formatDatetime(value);
			}
			function shortUrlFormatter(row) {
				var r ="";
				if(row.shortUrl1){
					r+=row.shortUrl1;
				}
				if(row.shortUrl2){
					r+="<div style='padding-top:4px'>"+row.shortUrl2+'</div>';
				}
				return r;
			}
			function encodeUrlFormatter(row) {
				var url="${domain}/vote_topic_"+row.linkKey + '.do';
				return url;
			}
			function pasteFormatter(value) {
				return value;
			}
			function addLink(linkeCode){
				layer.open({
					type : 2, //page层
					area : [ '600px', '400px' ],
					skin:'layui-layer-rim',
					title : $("h3.page_name").text()+'新增推广链接',
					shade : 0.6, //遮罩透明度
					scrollbar : false,
					offset : '100px',//距离顶部
					moveType : 0, //拖拽风格，0是默认，1是传统拖动
					shift : 0, //0-6的动画形式，-1不开启
					btn:['确定','取消'],
					content : '${base}/center/df/record/betrecord/leciUserLink.do?linkCode='+linkeCode,
					yes: function (index) {
					    var param = window["layui-layer-iframe" + index].callbackdata();
					    if(param.generalizeType === 2){
							param.linkKey = $('#layui-layer-iframe'+index).contents().find('#urlLink').val();
					    }
					    
					    if(!param){
					    	return false;
					    }
						$.ajax({
							url : "${base}/daili/generalizeLink/updateOrSave.do",
							data : param,
							success : function(data) {
								if(data.success){
									layer.close(index);
									initRdsData(1)
								}else{
									layer.alert(data.msg);
								}
							}
						});
			
					}, success: function (layero, index) {
						window["layui-layer-iframe" + index].loading();
			           /*  //传入参数，并赋值给iframe的元素
						$.ajax({
							url : "${base}/daili/generalizeLink/getRandCode.do",
							dataType : "text",
							success : function(randomCode) {
								if ("${isMulti}" == "true") {
									$('#tgType').attr('onmousedown', 'return true;');
									$('#dljz').val('多级代理');
								} else {
									$('#tgType').attr('onmousedown', 'return false;');
									$('#dljz').val('一级代理');
								}
								var url = "${url}";
								url = url.replace(/link998/, randomCode);
								$('#linkCode').val(randomCode);
								$('#urlLink').val(url);
								$('#rollPoint').val('');
								if ('${memberRate}' == 'on') {
									$("#tgType").val('1');
								}else{
									$('#tgType').val('2');
								}
								if('${memberRate}' != 'on'){
									$('#cpfds').hide();
								}
							} 
			
					});*/
			        }
			
				});
			}
			//删除
			function del(id) {
				layer.confirm('是否确定删除？', function() {
					$.ajax({
						url : "${base}/daili/generalizeLink/del.do",
						data : {
							id : id
						},
						success : function() {
							window.location.reload();
						}
					});
				});

			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>