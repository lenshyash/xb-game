<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>手机够彩-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/pc_return_mobile.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<div class="wapMobile" style="display:block">
		<div class="wap_top">
			<div class="top_left">
				<img src="${base}${logo}" width="200" height="70">
				<span>手机版</span>
			</div>
		</div>
		<div class="wap_header_img">
			<img alt="" src="${base}/common/template/lottery/lecai/images/mobile_test.png">
		</div>
		<div class="wap_xiazai">
			<c:if test="${not empty appIOS&&appIOS!=''}">
			<div class="xiazai_1">
				<img alt="" src="${base}${appAndroid}" width="150" height="150">
				<span>扫描安卓二维码</span>
			</div>
			</c:if>
			<c:if test="${not empty appAndroid&&appAndroid!=''}">
			<div class="xiazai_2">
				<img alt="" src="${base}${appIOS}" width="150" height="150">
				<span>扫描苹果二维码</span>
			</div>
			</c:if>
			<div class="xiazai_3">
				<a href="${base}/mobile">
					<img alt="" src="${base}/common/template/lottery/lecai/images/mobile/return_mobile.png" width="150" height="150">
				</a>
				<span>返回手机版</span>
			</div>
		</div>
		<div class="ddz_title_mes"><span class="tit">首页：</span>有游戏可以选择。</div>
		<div class="sjimg"><img src="${base}/common/template/lottery/lecai/images/mobile/mobile4.png" height="291" width="142"></div>
		<div class="ddz_title_mes"><span class="tit">充值：</span><span>包括支付宝、微信支付、银联支付。</span></div>
		<div class="sjimg"><img src="${base}/common/template/lottery/lecai/images/mobile/mobile3.png" height="291" width="142"></div>
		<div class="ddz_title_mes"><span class="tit">玩游戏：</span><span>高速服务器，玩的更尽兴。</span></div>
		<div class="sjimg"><img src="${base}/common/template/lottery/lecai/images/mobile/mobile1.png" height="291" width="142"></div>
		<%--<div class="ddz_title_mes"><span class="tit">聊天：</span><span>自主开发聊天系统，想聊就聊。</span></div>
		<div class="sjimg"><img src="style/img/sj1.png" height="291" width="142"></div>
		<div class="ddz_title_mes"><span class="tit">分享：</span><span>好游戏就要与好友一同分享。</span></div>
		<div class="sjimg"><img src="style/img/sj1.png" height="291" width="142"></div> --%>
		<div class="ddz_title_mes"><span class="tit">回报：</span><span>贴心回报，给你一路惊喜。</span></div>
		<div class="sjimg"><img src="${base}/common/template/lottery/lecai/images/mobile/mobile5.png" height="291" width="142"></div>
		
		<div class="cp">${copyright}</div>
	</div>
</body>
</html>