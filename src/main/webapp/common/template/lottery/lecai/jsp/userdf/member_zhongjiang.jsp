<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">中奖记录</div>
            <div class="userMain mgb10">
                <ul class="todayView mgb10">
                    <li>今日概况</li>
                    <li>投注金额：<span>${-allBetAmount}元</span></li>
                    <li>中奖金额：<span>${allWinAmount}元</span></li>
                    <li>盈亏：<span>${yingkuiAmount}元</span></li>
                </ul>
                <ul class="searchFirst">
                    <li><span>彩种：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                    <option value="">所有彩种</option>
									<c:forEach items="${lotList}" var="lot">
											<option value="${lot.code}" <c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
										</c:forEach>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr>
                            <th colspan="1">彩种</th>
                            <th>期号</th>
                            <th>下注时间</th>
                            <th>玩法</th>
                            <th>倍数</th>
                            <th>开奖号码</th>
                            <th>下注内容</th>
                            <th>投注金额</th>
                            <th>中奖金额</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
		        $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1);
		        })
		        $('#lotCode').change(function(){
		        	lottereyStatus = $(this).attr('value')
		            initRdsData(1)
		        })
			})
/* 			var json = '{"sumWinMoney":18.40,"sumBuyMoney":10.00,"page":{"aggsData":{"winSum":18.40,"buySum":10.00},"currentPageNo":1,"hasNext":false,"hasPre":false,"list":[{"account":"gosick","accountId":610,"ago":120,"buyMoney":2.00,"buyZhuShu":1,"createTime":"2018-10-20 17:09:50","haoMa":"大","id":12303,"jointPurchase":"1","logId":0,"lotCode":"TJSSC","lotName":"天津时时彩","lotType":52,"lotteryHaoMa":"3,9,7,5,2","model":1,"multiple":1,"odds":3.680000,"openTime":"2018-10-20 17:19:06","orderId":"L18102000288","parents":",594,","playCode":"dxds_zh","playName":"总和","qiHao":"20181020050","rollBackStatus":2,"roomId":0,"stationId":41,"status":2,"winMoney":3.68,"winZhuShu":1,"zhuiHao":"1"},{"account":"gosick","accountId":610,"ago":120,"buyMoney":2.00,"buyZhuShu":1,"createTime":"2018-10-20 17:09:43","haoMa":"双","id":12301,"jointPurchase":"1","logId":0,"lotCode":"TJSSC","lotName":"天津时时彩","lotType":52,"lotteryHaoMa":"3,9,7,5,2","model":1,"multiple":1,"odds":3.680000,"openTime":"2018-10-20 17:19:06","orderId":"L18102000286","parents":",594,","playCode":"dxds_zh","playName":"总和","qiHao":"20181020050","rollBackStatus":2,"roomId":0,"stationId":41,"status":2,"winMoney":3.68,"winZhuShu":1,"zhuiHao":"1"},{"account":"gosick","accountId":610,"ago":70,"buyMoney":2.00,"buyZhuShu":1,"createTime":"2018-10-20 17:09:25","haoMa":"小","id":12299,"jointPurchase":"1","logId":0,"lotCode":"XJSSC","lotName":"新疆时时彩","lotType":52,"lotteryHaoMa":"2,6,6,3,5","model":1,"multiple":1,"odds":3.680000,"openTime":"2018-10-20 17:20:26","orderId":"L18102000284","parents":",594,","playCode":"dxds_zh","playName":"总和","qiHao":"20181020044","rollBackStatus":2,"roomId":0,"stationId":41,"status":2,"winMoney":3.68,"winZhuShu":1,"zhuiHao":"1"},{"account":"gosick","accountId":610,"ago":70,"buyMoney":2.00,"buyZhuShu":1,"createTime":"2018-10-20 17:09:20","haoMa":"双","id":12298,"jointPurchase":"1","logId":0,"lotCode":"XJSSC","lotName":"新疆时时彩","lotType":52,"lotteryHaoMa":"2,6,6,3,5","model":1,"multiple":1,"odds":3.680000,"openTime":"2018-10-20 17:20:26","orderId":"L18102000283","parents":",594,","playCode":"dxds_zh","playName":"总和","qiHao":"20181020044","rollBackStatus":2,"roomId":0,"stationId":41,"status":2,"winMoney":3.68,"winZhuShu":1,"zhuiHao":"1"},{"account":"gosick","accountId":610,"ago":70,"buyMoney":2.00,"buyZhuShu":1,"createTime":"2018-10-20 17:09:15","haoMa":"双","id":12297,"jointPurchase":"1","logId":0,"lotCode":"XJSSC","lotName":"新疆时时彩","lotType":52,"lotteryHaoMa":"2,6,6,3,5","model":1,"multiple":1,"odds":3.680000,"openTime":"2018-10-20 17:20:26","orderId":"L18102000282","parents":",594,","playCode":"dxds_zh","playName":"总和","qiHao":"20181020044","rollBackStatus":2,"roomId":0,"stationId":41,"status":2,"winMoney":3.68,"winZhuShu":1,"xBet":10.00,"xWin":18.40,"zhuiHao":"1"}],"nextPage":1,"pageSize":10,"prePage":1,"start":0,"totalCount":5,"totalPageCount":1},"subBuyMoney":10.00}'
 */			function initRdsData(p){
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
						code: $("#lotCode").val(),
						startTime:startTime+" 00:00:00",
						endTime:endTime +" 23:59:59",
						status:2,
						rows:10,
						page:p
				}
				$.ajax({
					url:"${base}/lotteryV3/betInfo/getBcLotteryOrder.do",
					data:data,
					success:function(res){
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.page.totalCount > 0){
							addTemplate(res)
							page = res.page.currentPageNo;
							lotteryPage(res.page.totalCount,res.page.totalPageCount)
							lastPage = res.page.totalPageCount
							$("#pageNum").html(res.page.currentPageNo)
						}else{
							$(".Records_listCont").html(noTakeNotes)
						}
					}
				})
			}
			function addTemplate(res){				
				var html = '',haoma=''/* , statusName = '', oper ='' */;
				$.each(res.page.list,function(index, item){
					haoma = item.haoMa
					if(haoma.length>20){
						haoma = '<span onclick="showMessage(&quot;'+item.haoMa+'&quot;)" style="cursor:pointer;background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;">查看下注内容</span>'
					}
					html+='<tr><td>'+item.lotName+'</td><td>'+item.qiHao+'</td><td>'+item.createTime+'</td><td>'+item.playName+'</td><td>'+item.multiple+'</td><td style="color:red;">'+item.lotteryHaoMa +'</td><td>'+haoma+'</td><td>'+item.buyMoney+'</td><td>'+item.winMoney+'</td></tr>'
				})
				html+='<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>总计：</td><td style="color:red;">'+res.page.aggsData.buySum+'</td><td style="color:red;">'+res.page.aggsData.winSum+'</td></tr>'
				$(".Records_listCont").html(html)
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>