<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="title">
	<div class="about_us_title">
		如何注册成为${_title}会员？
	</div>
</div>
<div class="clear" style="height: 25px;"></div>
<div class="about_us_content">
	<p class="dotted">
		1、在本站首页的"会员登陆"处选择"注册"；
	</p>
	<p class="dotted">
		2、进入注册页面，填写用户名、密码、电子邮箱等注册信息，填写无误后，点击"提交注册"；
	</p>
	<p>
		3、注册成功！您可以马上充值购彩，还可以丰富个人资料，提升账户安全级别。
	</p>
</div>
<div class="clear" style="height: 32px;"></div>