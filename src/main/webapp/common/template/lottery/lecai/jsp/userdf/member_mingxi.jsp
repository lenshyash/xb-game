<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10" >账户明细</div>
            <div id="newTab" class="newTab">
            <a href="javascript:(0)" class="curr" data-id="zhangbian" data-url="3">帐变记录</a>
	            <a href="javascript:(0)" class="" data-id="chongzhi" data-url="1">充值记录</a>
	            <a href="javascript:(0)" class="" data-id="qukuan" data-url="2">取款记录</a>
	            
            </div>
            <script>
	            $(".newTab a").click(function () {
	            	var dataid = $(this).attr('data-id')
		            $(this).addClass('curr').siblings('a').removeClass('curr')
		            $(".searchFirst").hide()
		            $(".searchTable").hide()
		            $("."+dataid+"").show()
		            classify  = $(this).attr('data-url')
		            initRdsData(1)
		        })
            </script>
            <div class="userMain mgb10"style="min-height:668px!important;">
                <ul class="searchFirst chongzhi" style="display:none;">
                    <li><span>充值记录：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   <option value="">所有类型</option>
									<option value="5">在线存款</option>
									<option value="6">快速入款</option>
									<option value="7">一般存款</option>
									<option value="1">人工加款</option>
                                    <option value="801">虚拟货币</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_state">
                    	<span>充值记录：
       						<a class="userSearch time active" value="">所有状态</a>
       						<a class="userSearch time " value="1">处理中</a>
       						<a class="userSearch time " value="3">处理失败</a>
       						<a class="userSearch time " value="2">处理成功</a>
       					</span>
       				</li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <ul class="searchFirst qukuan" style="display:none;">
                    <li class="_state">
                    	<span>取款记录：
       						<a class="userSearch time active" value="">所有状态</a>
       						<a class="userSearch time " value="1">处理中</a>
       						<a class="userSearch time " value="3">处理失败</a>
       						<a class="userSearch time " value="2">处理成功</a>
       					</span>
       				</li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <ul class="searchFirst zhangbian" style="position: relative;">
                    <li class="_state">
                    	<span>帐变记录：</span>
                    	<input class="inputText" id="orderId" value="" type="text" placeholder="订单号" style="position: absolute;left: 150px;top:23px;background:#fff;height:30px;">
                    	<input class="inputText"  value="查询" onclick="initRdsData(1)" type="button"  style="position: absolute;left: 313px;top:23px;background:#fff;height:30px;width:58px;border-radius: 5px;cursor: pointer;">
       				</li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr class="searchTable chongzhi" style="background:#fff;display: none;">
                            <th colspan="1">日期</th>
                            <th>交易类别</th>
                            <th>支付名称</th>
                            <th>交易额度</th>
                            <th>状态</th>
                            <th>订单号</th>
                        </tr>
                        <tr class="searchTable qukuan" style="background:#fff;display: none;">
                            <th colspan="1">日期</th>
                            <th>交易类别</th>
                            <th>交易额度</th>
                            <th>状态</th>
                            <th>说明</th>
                        </tr>
                        <tr class="searchTable zhangbian" style="background:#fff;">
                            <th colspan="1">日期</th>
                            <th>交易类别</th>
                            <th>变动前金额</th>
                            <th>变动金额</th>
                            <th>变动后余额</th>
                            <th>备注</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			var mingxiStatus = '';
			var classify = 3;
			/*var json1 ='{"aggsData":{"totalMoney":0},"currentPageNo":1,"hasNext":false,"hasPre":false,"list":[{"account":"gosick","createDatetime":1540100546764,"createUserId":610,"flagActive":1,"handlerType":2,"id":1023,"lockFlag":1,"memberId":610,"money":12,"orderNo":"4116735133403136","payId":22,"payName":"速汇宝","stationId":41,"status":1,"stauts":1,"type":5,"userName":"测试"}],"nextPage":1,"pageSize":10,"prePage":1,"results":[{"$ref":"$.list[0]"}],"set":[{"$ref":"$.list[0]"}],"start":0,"totalCount":1,"totalPageCount":1}'
 			var json2 = '{"currentPageNo":1,"hasNext":false,"hasPre":false,"list":[{"bizDatetime":1540095541002,"orderId":"J18102100051","afterMoney":998.400000,"type":139,"createDatetime":1540095686131,"accountId":610,"money":34.000,"flagActive":1,"id":60761,"beforeMoney":964.400000,"account":"gosick","stationId":41,"parents":",594,"},{"createUserId":610,"bizDatetime":1540095524475,"orderId":"L18102100052","remark":"投注彩种：重庆时时彩;期号：20181021038订单号：L18102100052","afterMoney":964.400000,"type":134,"operatorName":"gosick","createDatetime":1540095524496,"accountId":610,"money":-20.000000,"flagActive":1,"id":60758,"beforeMoney":984.400000,"operatorId":610,"account":"gosick","stationId":41,"parents":",594,"}],"nextPage":1,"pageSize":10,"prePage":1,"results":[{"$ref":"$.list[0]"},{"$ref":"$.list[1]"}],"set":[{"$ref":"$.list[0]"},{"$ref":"$.list[1]"}],"start":0,"totalCount":2,"totalPageCount":1}'
 */			var navUrls = {
					1 : "${base}/center/record/hisrecord/comrd.do",
					2 : "${base}/center/record/hisrecord/drawrd.do",
					3 : "${base}/center/record/mnyrecord/list.do"
				}
			$(function(){
				initRdsData(1)
		       /*  $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            mingxiStatus = $(this).attr('value')
		            initRdsData(1);
		        }) */
		       $("._time a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $("._state a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            lottereyStatus = $(this).attr('value')
		            mingxiStatus = $(this).attr('value')
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		            initRdsData(1)
		        })
			})
			function initRdsData(p){
 				$(".Records_listCont").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
						code: $("#lotCode").val(),
						startTime:startTime+" 00:00:00",
						endTime:endTime +" 23:59:59",
						status:mingxiStatus,
						pageSize:10,
						pageNumber:p,
						orderId:$("#orderId").val()
				}
				$.ajax({
					url:navUrls[classify],
					data:data,
					success:function(res){
						$(".notContent").show()
				    	$(".loadingGif").hide()
					    	switch (Number(classify)) {
							case 1:
								addTemplate1(res)
								break;
							case 2:
								addTemplate2(res)
								break;
							case 3:
								addTemplate3(res)
								break;
							}
							page = res.currentPageNo;
							lotteryPage(res.totalCount,res.totalPageCount)
							lastPage = res.totalPageCount
							$("#pageNum").html(res.currentPageNo)
					}
				})
			}
			function addTemplate1(res){				
				var html = '';
				$.each(res.list,function(index, item){
					html+='<tr><td>'+getMyDate(item.createDatetime)+'</td><td>'+statusType[item.type]+'</td><td>'+unExist(item.payName)+'</td><td>'+item.money+'</td><td>'+statusType[item.status]+'</td><td>'+item.orderNo+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function addTemplate2(res){				
				var html = '';
				$.each(res.list,function(index, item){
					if(!item.remark){
						item.remark = '无备注'
					}else{
						item.remark = '<span onclick="showMessage(&quot;'+item.remark+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看备注</span>'
					}
					html+='<tr><td>'+getMyDate(item.createDatetime)+'</td><td>'+statusType[item.type]+'</td><td>'+item.drawMoney+'</td><td>'+qukuanType[item.status]+'</td><td style="white-space:unset;">'+item.remark+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function addTemplate3(res){				
				var html = '';
				$.each(res.list,function(index, item){
					if(!item.remark){
						item.remark = '无备注'
					}else{
						item.remark = '<span onclick="showMessage(&quot;'+item.remark+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看备注</span>'
					}
					html+='<tr><td>'+getMyDate(item.createDatetime)+'</td><td>'+statusType[item.type]+'</td><td>'+item.beforeMoney+'</td><td style="color:#4aa9db;">'+item.money+'</td><td style="color:#e4393c;">'+item.afterMoney+'</td><td style="white-space:unset;">'+item.remark+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>