<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>购彩中心-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script src="../js/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/lottery.css?v=" type="text/css">
</head>
<body>
<jsp:include page="/member/${stationFolder}/include/header.jsp"></jsp:include>

<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>

<style media="screen">

</style>
</html>
