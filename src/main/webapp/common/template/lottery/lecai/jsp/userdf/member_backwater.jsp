<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">反水记录</div>
            <div class="userMain mgb10">
                <ul class="searchFirst">
                    <li><span>状态：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                    <option value="">所有状态</option>
                                    <option value="4">已领取</option>
                                    <option value="9">未领取</option>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr>
                            <th colspan="1">游戏类型</th>
                            <th>账号</th>
                            <th>下注日期</th>
                            <th>反水金额</th>
                            <th>反水状态</th>
                            <th>操作</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
		        $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1);
		        })
		        $('#lotCode').change(function(){
		        	lottereyStatus = $(this).attr('value')
		            initRdsData(1)
		        })
			})
		function initRdsData(p){
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
						status: $("#lotCode").val(),
						startTime:startTime+" 00:00:00",
						endTime:endTime +" 23:59:59",
						rows:10,
						page:p
				}
				$.ajax({
					url:"${base}/center/record/backwater/list.do",
					data:data,
					success:function(res){
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.rows.length > 0){
							addTemplate(res)
							page = p;
							totalPage = Math.ceil((res.total/10))
							lotteryPage(res.total,totalPage)
							lastPage = totalPage
							$("#pageNum").html(p)
						}else{
							$(".Records_listCont").html(noTakeNotes)
						}
					}
				})
			}
			function addTemplate(res){				
				var html = '';
				$.each(res.rows,function(index, item){
					html+='<tr><td>'+getFormat(item.betType,1)+'</td><td>'+item.account+'</td><td>'+item.betDate+'</td><td>'+item.backwaterMoney+'</td><td>'+getFormat(item.backwaterStatus,2)+'</td><td style="color:red;">'+getFormat(item.backwaterStatus,3,item.id) +'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function getFormat(value,t,rowId){
				if(t == 1){
					if(value==1){
						return "彩票";
					}else if(value==2){
						return "真人";
					}else if(value==3){
						return "电子";
					}else if(value==4){
						return "体育";
					}else if(value==5){
						return "六合特码B";
					}else if(value==6){
						return "六合正码B";
					}else if(value==11){
						return "系统彩";
					}else if(value==15){
						return "十分六合彩特码B";
					}else if(value==16){
						return "十分六合彩正码B";
					}else if(value==99){
						return "三方体育";
					}
				}else if(t == 2){
					if(value==4){
						return '已反水';
					}else if(value==9){
						return '待领取';
					}else if(value==8){
						return '已过期';
					}
				}else if(t == 3){
					if(value==9){
						return '<a href="#" onclick="cashing(\''+rowId+'\')">领取</a>';
					}else{
						return '';
					}
				}
			}
			function cashing(rowId){
				$.ajax({
					url:"${base}/center/record/backwater/cashing.do",
					data:{
						id:rowId
					},
					success:function(res){
						if(res.success){
							initRdsData(1);
						}else{
							layer.msg(res.msg,{icon:2});
						}
					}
				})
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>