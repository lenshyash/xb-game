<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>${lotName}开奖信息-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/draw_detail.css"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/js/chosen/chosen.min.css"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/draw.js"
	code="${lotCode}"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/chosen/chosen.jquery.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="main">
		<div class="leftnav">
			<input type="hidden" value="3" id="navId" />
			<input type="hidden" value="${curQiHao}" id="curQiHao"/>
			<ul id="top_tab" class="changebox">
				<!-- <li class="on" id="top_sort"><a href="javascript://">按分类查看</a></li> -->
			</ul>
			<div class="cznav" id="nav_sort" style="display: block;">
				<ul class="item js-lottery-container">
					<c:forEach items="${lotList}" var="list" varStatus="l">
						<li <c:if test="${list.code eq lotCode}">class="tit"</c:if>>
							<h1>
								<a href="${base}/lotteryV3/draw/list.do?lotCode=${list.code}"
									c="${list.code}">${list.name}</a>
							</h1>
						</li>
					</c:forEach>
				</ul>
			</div>
			<div class="lbot"></div>
		</div>
		<div class="right_box">
			<div class="right_top"></div>
			<div class="right_main">
				<div class="cpinfo">
					<h1>${lotName}开奖信息[${lotName eq '六合彩'?'每周二，四，六':'每日开奖'}]</h1>
					<p>
						<select id="history_phase_list" style="width:125px;" onchange="changeQiHao(this);">
							<c:forEach items="${qiHao}" var="qHao" begin="1">
								<option value="${qHao.qiHao}" <c:if test="${not empty curQiHao && (curQiHao eq qHao.qiHao)}">selected="selected"</c:if>>${qHao.qiHao}</option>
							</c:forEach>
						</select>
					</p>
					<div class="clear"></div>
				</div>
				<div class="draw_main">
					<div class="draw_left">
						<div class="draw_content">
							<ul class="draw_ul dipin_Draw_ul">
								<li class="bg1">
									<h2>
										<%-- ${lotName}第<b class="red" id="jq_latest_draw_phase">${lastData.qiHao}</b>期 <span --%>
										${lotName}第<b class="red" id="jq_latest_draw_phase">20180625</b>期 <span
											class="right" id="jq_latest_draw_time">开奖日期：<fmt:formatDate value="${lastData.endTime}" var="date" pattern="yyyy-MM-dd HH:mm:ss" />${date}
											</span>
									</h2>
								</li>
								<li class="bg2">
									<div class="ball_box">
										<h2>${lastData}
											<span class="resultName">${lotName}开奖号码：</span><label id="jq_latest_draw_result">
<%--												<c:if test="${not empty lastData && lastData != '' && lastData.length >0 }">--%>
<%--													<c:set value="${ fn:split(lastData.haoMa, ',') }" var="arr" />--%>
<%--													<c:forEach items="${arr}" var="haoMa">--%>
<%--														<span class="ball_1">${arr}</span>--%>
<%--													</c:forEach>--%>
<%--												</c:if>--%>
											</label>
										</h2>
									</div>
									<div class="clear nospace">&nbsp;</div>
								</li>
								<li class="bg3">
									<div class="money">
										<span><h2 style="font-weight: normal;">
												${lotName}本期销量：<label id="jq_volume">396,390</label>元
											</h2></span>
									</div>
									<div class="more_data" id="jq_data_link">
										<a target="_blank" href="${base}/lotteryV3/draw/hisresult.do?lotCode=${lotCode}"
											class="draw_data">历史数据</a><a target="_blank"
											href="${base}/lotteryV3/trend.do?lotCode=${lotCode}" class="analysis">走势图</a>
									</div>
									<div class="clear nospace">&nbsp;</div>
								</li>
							</ul>
						</div>
						<div class="draw_content draw_recent">
							<table class="draw_tab" id="jq_draw_detail" cellspacing="0"
								cellpadding="0">
								<tbody>
									<tr class="title">
										<td colspan="4">中奖说明</td>
									</tr>
									<tr class="tit">
										<td width="25%" ${lotCode eq 'LHC'?'':'colspan="2"'}>玩法</td>
										<td width="20%">${lotCode eq 'LHC'?'投注规则':'单注奖金(元)'}</td>
										<td width="55%">中奖条件</td>
									</tr>
									<c:choose>
										<c:when test="${not empty group}">
											<c:forEach items="${group}" var="g" varStatus="gs">
												<tr <c:if test="${gs.index%2==0}">class="bgcolor"</c:if>>
													<td rowspan="${playMap[g.id].size()}">${g.name}</td>
													<c:forEach items="${playMap[g.id]}" begin="0" end="0" var="p" varStatus="ps">
														<td>${p.name}</td>
														<td>${p.minBonusOdds}</td>
														<td>${p.winExample}</td>
													</c:forEach>
												</tr>
												<c:forEach items="${playMap[g.id]}" var="p" begin="1" varStatus="ps">
													<tr <c:if test="${gs.index%2==0}">class="bgcolor"</c:if>>
														<td>${p.name}</td>
														<td>${p.minBonusOdds}</td>
														<td>${p.winExample}</td>
													</tr>
												</c:forEach>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr>
												<td>特码</td>
												<td>48</td>
												<td>该期开奖号码中的特码为48</td>
											</tr>
											<tr class="bgcolor">
												<td>正码</td>
												<td>01</td>
												<td>该期开奖号码中的正码（第1-6位为正码）包含01</td>
											</tr>
											<tr>
												<td>正码特</td>
												<td>正一特01</td>
												<td>该期开奖号码中的第一位为01</td>
											</tr>
											<tr class="bgcolor">
												<td>正码特1-6</td>
												<td>正码一单</td>
												<td>该期开奖号码中的第一位为单数,如01,03等</td>
											</tr>
											<tr>
												<td>特码半波</td>
												<td>红波</td>
												<td>该期开奖号码中的特码是以下号码之一：01 02 07 08 12 13 18 19 23 24 29 30 34 35 40 45 46</td>
											</tr>
											<tr class="bgcolor">
												<td>一肖尾数</td>
												<td>鼠</td>
												<td>该期开奖号码包含以下号码之一：09 21 33 45</td>
											</tr>
											<tr>
												<td>特码生肖</td>
												<td>蛇</td>
												<td>该期开奖号码中的特码包含以下号码之一:04,16,28,40</td>
											</tr>
											<tr class="bgcolor">
												<td>合肖</td>
												<td>鼠&牛</td>
												<td>该期开奖号码中的特码为以下号码之一：08 09 20 21 32 33 44 45</td>
											</tr>
											<tr>
												<td>自选不中(全不中)</td>
												<td>01&02&03&04&05&06</td>
												<td>该期开奖号码不包含以下号码的任何一个：01 02 03 04 05 06</td>
											</tr>
											<tr class="bgcolor">
												<td>连肖连尾</td>
												<td>2连肖-鼠&牛</td>
												<td>该期开奖号码对应的生肖必须包含鼠和牛</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
					<div class="draw_right">
						<div class="column_box none_margin_top">
							<div class="column_title">
								<div class="draw_phase">
									第 <strong class="red" id="jq_cur_phase"></strong> 期 <label
										id="jq_cur_drawtime"></label>开奖
								</div>
							</div>
							<div class="column_main draw_info">
								<ul class="darw_info_list">
									<li><label id="jq_latest_amount">上期派奖：<strong
											class="red">302,424</strong> 元
									</label></li>
									<li class="last">投注截止：<label id="jq_cur_countdown"><strong
											class="red">04</strong>时<strong class="red">11</strong>分<strong
											class="red">42</strong>秒</label></li>
								</ul>
								<div class="button_box">
									<a target="_blank" href="${base}/lotteryV3/lotDetail.do?lotCode=${lotCode}" class="button_1"></a>
									<div class="clear nospace"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="right_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>
	<script type="text/javascript">
		var c = '${lotCode}';
		var year = '${year}';
		$(function(){
			$("#history_phase_list").chosen({
				disable_search : true
			});
			if(c=='LHC'){
				$(".dipin_Draw_ul").css({'margin-bottom':'10px'});
				initHaoMa('${lastData.haoMa}');
			}
		})
		function changeQiHao($this){
			window.location.href= "${base}/lotteryV3/draw/list.do?lotCode=${lotCode}&qiHao="+$this.value;
		}
		
		function initHaoMa(haoMa){
			if(!haoMa){
				return false;
			}
			var ma = haoMa.split(","),temp="";
			for(var i=0;i<ma.length;i++){
				if(i==6){
					temp += '<b style="display: inline-block;margin-top: -10px;">+</b>&nbsp;';
				}
				if(ma[i] == '?'){
					temp += '<span class="lhc_1">'+ma[i]+'<font style="display:inline-block;width:20px;height:20px;color:#000">?</font></span>';
				}else{
					var index = Base.lhcHaoMa.bose(ma[i]);
					var sx = Base.lhcHaoMa.zodiacName(ma[i],'${year}');
					temp += '<span class="lhc_'+index+'">'+ma[i]+'<font style="display:inline-block;width:20px;height:20px;color:#000">'+sx+'</font></span>';
				}
			}
			$("#jq_latest_draw_result").html(temp);
		}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>