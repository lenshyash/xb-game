<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/user_content.css?v=111" type="text/css">
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 额度转换</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能转换额度</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label></div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}</span>
				</div>
				<a class="btn recharge normal" href="${base }/center/banktrans/deposit/cp3Page.do">充值</a>
				<span><a class="btn withdrawal normal" href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="account">
				<div class="tit"><div class="account_switch"><label><strong style="cursor: default;">目前额度</strong></label></div></div>
				<ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">账号</li>
					<li class="end" style="width:480px;">余额</li>
				</ul><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">系统余额</li>
					<li class="end" style="width:480px;"><span id="mainCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul><c:if test="${isAgOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">AG余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ag','AGCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="AGCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isDsOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">DS余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ds','DSCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="DSCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isBbinOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">BBIN余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);"  onclick="getBalance('bbin','BBINCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="BBINCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isMgOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">MG余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('mg','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isPtOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">PT余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('pt','PTCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="PTCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isQtOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">QT余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('qt','QTCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="QTCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isAbOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">Allbet余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ab','ABCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="ABCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isOgOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">OG余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('og','OGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="OGCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isCq9OnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">CQ9余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('jdb','JDBCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="JDBCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isJdbOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">JDB余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('cq9','CQ9Credit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="CQ9Credit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isTtgOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">TTG余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ttg','TTGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="TTGCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isMWOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">MW余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('mw','MWCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MWCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isIsbOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">ISB余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('isb','ISBCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="ISBCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isBgOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">BG余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('bg','BGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="BGCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if><c:if test="${isVrOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">VR余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('vr','VRCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="VRCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if>
				<c:if test="${isKyOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">KY余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ky','KYCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="KYCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if>
				<c:if test="${isThOnOff  eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">TH余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('th','thCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="THCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if>
				<c:if test="${isIbcOnOff eq 'on'}"><ul class="clearfix" style="padding:3px 0;">
					<li class="star" style="width:300px;">IBC余额</li>
					<li class="end" style="width:480px;"><a style="color:#ff0000;" href="javascript:void(0);" onclick="getBalance('ibc','IBCCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="IBCCredit">0.00</span>&nbsp;&nbsp;RMB</li>
				</ul></c:if>
			</div>
		</div>
		
        <div style="font-size:18px;font-weight:bolder;">额度转换</div>
        <table class="userTab" style="width:50%;">
            <tbody>
            	<tr>
                    <td>转出：</td>
                    <td>
			<select name="source-transfer" id="source-transfer">
				<option selected="selected" value="">----请选择钱包----</option>
				<option value="sys">系统额度</option>
				<c:if test="${isAgOnOff eq 'on'}">
					<option value="ag">AG额度</option>
				</c:if>
				<c:if test="${isDsOnOff eq 'on'}">
					<option value="ds">DS额度</option>
				</c:if>
				<c:if test="${isBbinOnOff eq 'on'}">
					<option value="bbin">BBIN额度</option>
				</c:if>
				<c:if test="${isMgOnOff eq 'on'}">
					<option value="mg">MG额度</option>
				</c:if>
				
				<c:if test="${isPtOnOff eq 'on'}">
					<option value="pt">PT额度</option>
				</c:if>
				<c:if test="${isQtOnOff eq 'on'}">
					<option value="qt">QT额度</option>
				</c:if>
				<c:if test="${isAbOnOff eq 'on'}">
					<option value="ab">Allbet额度</option>
				</c:if>
				<c:if test="${isOgOnOff eq 'on'}">
					<option value="og">OG额度</option>
				</c:if>
				<c:if test="${isCq9OnOff eq 'on'}">
					<option value="cq9">CQ9额度</option>
				</c:if>
				<c:if test="${isJdbOnOff eq 'on'}">
					<option value="jdb">JDB额度</option>
				</c:if>
				<c:if test="${isTtgOnOff eq 'on'}">
					<option value="ttg">TTG额度</option>
				</c:if>
				<c:if test="${isMwOnOff eq 'on'}">
					<option value="mw">MW额度</option>
				</c:if>
				<c:if test="${isIsbOnOff eq 'on'}">
					<option value="isb">ISB额度</option>
				</c:if>
				<c:if test="${isBgOnOff eq 'on'}">
					<option value="bg">BG额度</option>
				</c:if>
				<c:if test="${isVrOnOff eq 'on'}">
					<option value="vr">VR额度</option>
				</c:if>
				<c:if test="${isKyOnOff eq 'on'}">
					<option value="ky">KY额度</option>
				</c:if>
				<c:if test="${isThOnOff eq 'on'}">
					<option value="Th">TH额度</option>
				</c:if>
				<c:if test="${isIbcOnOff eq 'on'}">
					<option value="ibc">IBC额度</option>
				</c:if>
			</select>
                    </td>
                </tr>
                <tr>
                    <td>转入：</td>
                    <td>
                       <select name="desc-transfer" id="desc-transfer">
				<option selected="selected" value="">----请选择钱包----</option>
				<option value="sys">系统额度</option>
				<c:if test="${isAgOnOff eq 'on'}">
					<option value="ag">AG额度</option>
				</c:if>
				<c:if test="${isDsOnOff eq 'on'}">
					<option value="ds">DS额度</option>
				</c:if>
				<c:if test="${isBbinOnOff eq 'on'}">
					<option value="bbin">BBIN额度</option>
				</c:if>
				<c:if test="${isMgOnOff eq 'on'}">
					<option value="mg">MG额度</option>
				</c:if>
				<c:if test="${isPtOnOff eq 'on'}">
					<option value="pt">PT额度</option>
				</c:if>
				<c:if test="${isQtOnOff eq 'on'}">
					<option value="qt">QT额度</option>
				</c:if>
				<c:if test="${isAbOnOff eq 'on'}">
					<option value="ab">Allbet额度</option>
				</c:if>
				<c:if test="${isOgOnOff eq 'on'}">
					<option value="og">OG额度</option>
				</c:if>
				<c:if test="${isCq9OnOff eq 'on'}">
					<option value="cq9">CQ9额度</option>
				</c:if>
				<c:if test="${isJdbOnOff eq 'on'}">
					<option value="jdb">JDB额度</option>
				</c:if>
				<c:if test="${isTtgOnOff eq 'on'}">
					<option value="ttg">TTG额度</option>
				</c:if>
				<c:if test="${isMwOnOff eq 'on'}">
					<option value="mw">MW额度</option>
				</c:if>
				<c:if test="${isIsbOnOff eq 'on'}">
					<option value="isb">ISB额度</option>
				</c:if>
				<c:if test="${isBgOnOff eq 'on'}">
					<option value="bg">BG额度</option>
				</c:if>
				<c:if test="${isVrOnOff eq 'on'}">
					<option value="vr">VR额度</option>
				</c:if>
				<c:if test="${isKyOnOff eq 'on'}">
					<option value="ky">KY额度</option>
				</c:if>
			   <c:if test="${isThOnOff eq 'on'}">
				   <option value="th">TH额度</option>
			   </c:if>
				<c:if test="${isIbcOnOff eq 'on'}">
					<option value="ibc">IBC额度</option>
				</c:if>
		   </select>
                    </td>
                </tr>
                <tr>
                    <td>金额：</td>
                    <td><input id="amount" name="amount" class="inputText" type="text"></td>
                </tr>
                <tr><td></td><td><input value="确定" onclick="trans(this);" class="ddbtn" type="button"></td></tr>
                <tr>
                	<td></td>
                	<td><span id="MSwitchResult" style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></td>
                </tr>
            </tbody>
            <input type="hidden" name="v" id="v" value="${v}">
    	</table>
    	</c:if>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
function getBalance(type,id){
	$("#"+id).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />");
	$.ajax({
        url:'${base}/rc4m/getBalance.do',
        type:'POST',
        data:"type="+type,
        success:function(json,status,xhr){
         	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(json.balance==-1 || json.balance=='-1'){
					$("#"+id).text("第三方账户还未开通");
				}else{
					$("#"+id).text(json.balance);
				}
			} else {// 后台异常
				$("#"+id).text("获取失败")
			}
        	
        }
    });
}

$(function(){
	getBalance("sys","mainCredit");
	<c:if test="${isMgOnOff eq 'on'}">
		getBalance("mg","MGCredit");
	</c:if>
	<c:if test="${isBbinOnOff eq 'on'}">
		 getBalance("bbin","BBINCredit");
	</c:if>
	<c:if test="${isAgOnOff eq 'on'}">
		 getBalance("ag","AGCredit");
	</c:if>
	
	<c:if test="${isPtOnOff eq 'on'}">
		 getBalance("pt","PTCredit");
	</c:if>
	<c:if test="${isQtOnOff eq 'on'}">
	 	getBalance("qt","QTCredit");
	</c:if>
	<c:if test="${isAbOnOff eq 'on'}">
		getBalance("ab","ABCredit");
	</c:if>
	<c:if test="${isOgOnOff eq 'on'}">
		getBalance("og","OGCredit");
	</c:if>
	<c:if test="${isDsOnOff eq 'on'}">
		getBalance("ds","DSCredit");
	</c:if>
	<c:if test="${isCq9OnOff eq 'on'}">
		getBalance("cq9","CQ9Credit");
	</c:if>
	<c:if test="${isJdbOnOff eq 'on'}">
		getBalance("jdb","JDBCredit");
	</c:if>
	<c:if test="${isIsbOnOff eq 'on'}">
		getBalance("isb","ISBCredit");
	</c:if>
	<c:if test="${isBgOnOff eq 'on'}">
		getBalance("bg","BGCredit");
	</c:if>
	<c:if test="${isVrOnOff eq 'on'}">
		getBalance("vr","VRCredit");
	</c:if>
	<c:if test="${isKyOnOff eq 'on'}">
		getBalance("ky","KYCredit");
	</c:if>
	<c:if test="${isThOnOff eq 'on'}">
		getBalance("th","THCredit");
	</c:if>
	<c:if test="${isIbcOnOff eq 'on'}">
		getBalance("ibc","IBCCredit");
	</c:if>
	$("#source-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			$("#desc-transfer").val("ag");
		}else{
			$("#desc-transfer").val("sys");
		}
	});
	
 	$("#desc-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			$("#source-transfer").val("ag");
		}else{
			$("#source-transfer").val("sys");
		}
	}); 
});

function trans(btn){
	$(btn).attr("disabled","disabled");
	var from = $("#source-transfer").val();
	var to = $("#desc-transfer").val();
	var v = $("#v").val();
	
	if(from==null || from==''){
		alertMsg("转出类型不能为空");
		$(btn).removeAttr("disabled");
		return;
	}
	if(to==null || to==''){
		alertMsg("转出类型不能为空");
		$(btn).removeAttr("disabled");
		return;
	}
	var quota = $("#amount").val();
	if(!quota){
		alertMsg("余额不能为空");
		$(btn).removeAttr("disabled");
		return;
	}
	if(from=='sys'&& !/ag|bbin|mg|pt|qt|ab|og|ds|cq9|jdb|ttg|mw|isb|bg|vr|ky|th|ibc/.test(to)){
		alertMsg("转入类型错误");
		$(btn).removeAttr("disabled");
		return;
	}
	if(to=='sys'&& !/ag|bbin|mg|pt|qt|ab|og|ds|cq9|jdb|ttg|mw|isb|bg|vr|ky|th|ibc/.test(from)){
		alertMsg("转出类型错误");
		$(btn).removeAttr("disabled");
		return;
	}
	var param = {};
	param["changeFrom"]=from;
	param["changeTo"]=to;
	param["quota"]=quota;
	param["v"]=v;
	$.ajax({
        url:'${base}/rc4m/thirdRealTransMoney.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
        	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
	        	if(json.success){
	        		alertMsg("转账成功");
					if(to=='mg'||from=='mg'){
						getBalance("mg","MGCredit");
					}
					if(to=='ag'||from=='ag'){
						 getBalance("ag","AGCredit");
					}
					if(to=='bbin'||from=='bbin'){
						 getBalance("bbin","BBINCredit");
					}
					if(to=='qt'||from=='qt'){
						 getBalance("qt","QTCredit");
					}
					if(to=='pt'||from=='pt'){
						 getBalance("pt","PTCredit");
					}
					if(to=='ab'||from=='ab'){
						 getBalance("ab","ABCredit");
					}
					if(to=='og'||from=='og'){
						 getBalance("og","OGCredit");
					}
					if(to=='ds'||from=='ds'){
						 getBalance("ds","DSCredit");
					}
					if(to=='cq9'||from=='cq9'){
						 getBalance("cq9","CQ9Credit");
					}
					if(to=='jdb'||from=='jdb'){
						 getBalance("jdb","JDBCredit");
					}
					if(to=='ttg'||from=='ttg'){
						 getBalance("ttg","TTGCredit");
					}
					if(to=='mw'||from=='mw'){
						 getBalance("mw","MWCredit");
					}
					if(to=='isb'||from=='isb'){
						 getBalance("isb","ISBCredit");
					}
					if(to=='bg'||from=='bg'){
						 getBalance("bg","BGCredit");
					}
					if(to=='vr'||from=='vr'){
						 getBalance("vr","VRCredit");
					}
					if(to=='ky'||from=='ky'){
						 getBalance("ky","KYCredit");
					}
					if(to=='th'||from=='th'){
						getBalance("th","THCredit");
					}
					if(to=='ibc'||from=='ibc'){
						 getBalance("ibc","IBCCredit");
					}
					getBalance("sys","mainCredit");
	        	}else{
	        		alertMsg(json.msg);
	        	}
			}else {// 登录异常
				alertMsg(json.msg);
			}
        	
        	$(btn).removeAttr("disabled");
        }
    });
}
</script></c:if>
</body>
</html>