<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置：投注记录</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="summary">
				<div class="welcome">
					您好：<label class="red"><strong id="user_username">${loginMember.account}</strong></label>
					<a id="user_level" href="javascript:void(0);" target="_blank"
						title=""><!-- <img
						src="${base}/common/template/lottery/lecai/images/member/cb_0.gif"> --></a>
				</div>
				<div class="balances">
					<span>账户余额：<label class="red"><strong
							id="user_money">${loginMember.money}</strong></label>${not empty cashName?cashName:'元'}
					</span> <img id="balances_arrow"
						src="${base}/common/template/lottery/lecai/images/member/arrow_9x5_down.gif">
				</div>
				<a class="btn recharge normal"
					href="${base}/center/banktrans/deposit/cp3Page.do">充值</a> <span><a
					class="btn withdrawal normal"
					href="${base}/center/banktrans/draw/cp3Page.do">提款</a></span>
			</div>
			<div class="ordermain">
				<div class="navbox">
					<ul class="list">
						<li class="on" scripttype="0"><span class="left"></span> <span
							class="bg content"><a href="javascript:void(0);">全部投注</a></span>
							<span class="right"></span></li>
						<c:if test="${!unChaseOrder}"><li scripttype="1"><span class="left"></span> <span
							class="bg content"><a href="javascript:void(0);">我的追号</a></span>
							<span class="right"></span></li></c:if>
						<c:if test="${!unCancleOrder}"><li scripttype="2"><span class="left"></span> <span
							class="bg content"><a href="javascript:void(0);">我的撤单</a></span>
							<span class="right"></span></li></c:if>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="filter">
					<ul>
						<li>
							<div class="category">
								<select name="code" style="width: 100px;" id="lotCode">
									<option value="">所有彩种</option>
									<c:forEach items="${lotList}" var="lot">
										<option value="${lot.code}"
											<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
									</c:forEach>
								</select>
							</div>
						</li>
						<li>
							<div class="statusDiv">
								<select name="status" style="width: 90px;" id="status">
									<option value="">所有状态</option>
									<option value="1">未开奖</option>
									<option value="2">已中奖</option>
									<option value="3">未中奖</option>
									<option value="4">撤单</option>
								</select>
							</div>
						</li>
						<li>
							<div class="modelDiv">
								<select name="model" id="model" style="width: 100px;">
									<option value="">全部模式</option>
									<option value="1">元</option>
									<option value="10">角</option>
									<option value="100">分</option>
								</select>
							</div>
						</li>
						<li>
							<div class="category_time">
								<select name="selected_time" style="width: 80px;">
									<option value="1" selected>今天</option>
									<option value="2">昨天</option>
									<option value="3">本周</option>
									<option value="4">上周</option>
									<option value="5">本月</option>
									<option value="6">上月</option>
								</select>
							</div>
						</li>
						<li>
							<div class="time-piece">
								<input name="startTime" id="startTime" class="text text75"
									readonly="readonly" value="${startTime}" type="text"
									data-end-date="${endTime}"> <span>至</span> <input
									name="endTime" id="endTime" class="text text75"
									readonly="readonly" value="${endTime}" type="text"
									data-end-date="${endTime}"> <input class="btn normal"
									type="button" onclick="search();" value="查询">
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="detail_explain">
					<ul>
						<li>彩种：<span id="lotNames">所有彩种</span>&nbsp;&nbsp;报表日期：<span
							id="orderBetListBetween">${startTime}——${endTime}</span></li>
						<li>贴心小提醒：点击注单号能查询详细投注明细,标明“<font color="red">合</font>”属于合买订单，标明“<font color="red">追</font>”属于追号订单</li>
						<li>贴心小提醒：可赢金额=购买金额*赔率*中奖注数(购买多注的情况下：由于未开奖时无法确定中奖注数，即计算时默认中奖1注，该字段数据仅供参考，具体以实际派奖为准)</li>
					</ul>
				</div>
				<div class="listmain">
					<table id="tz_table" class="date" width="100%" cellspacing="0"
						cellpadding="0" border="0">
						<thead>
							<tr class="title">
								<td>注单号|下注时间</td>
								<td>彩种|期号</td>
								<td>玩法</td>
								<td>倍数</td>
								<td>开奖号码</td>
								<td>下注内容</td>
								<td>投注金额</td>
								<td>中奖金额</td>
								<td>可赢金额</td>
								<td>状态</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody id="betResultOrder">
							<tr>
								<td colspan="10" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td>
							</tr>
						</tbody>
						<tfoot id="orderBetOrderTblFoot">
							<tr>
								<td colspan="6" style="text-align: right">小计</td>
								<td class="red" id="orderBetPageAmount"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="6" style="text-align: right">总计</td>
								<td class="red" id="orderBetAmount"></td>
								<td class="red" id="orderWinAmount"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<jsp:include page="../include/page.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var curNavFlag = 0;
		var statusType = 0;
		$(function() {
			$(
					".category select,.statusDiv select,.modelDiv select,.category_time select")
					.chosen({
						disable_search : true
					});
			$('#endTime,#startTime').cxCalendar({
				format : 'YYYY-MM-DD'
			});
			initRdsData(curNavFlag);

			$(".navbox ul li").click(function() {
				curNavFlag = $(this).attr("scripttype");
				$(this).addClass("on").siblings().removeClass("on");
				$("#pageNumber").val(1);
				initRdsData(curNavFlag);
			})

			$(".category_time select").change(function() {
				var v = $(this).val();
				quickSelDate(parseInt(v));
			})

		})

		function search() {
			$("#pageNumber").val(1); //初始化页数
			statusType = $("#status").val();
			initRdsData(curNavFlag);
		}

		function initRdsData(flag) {
			var zhuiHao = 1, temp = "";
			switch (parseInt(flag)) {
			case 0:
				$('#status option:eq('+statusType+')').prop("selected", "selected").siblings().removeAttr('selected');
				$("#status").trigger("chosen:updated");
				break;
			case 1:
				zhuiHao = 2;
				$('#status option:eq('+statusType+')').prop("selected", "selected").siblings().removeAttr('selected');
				$("#status").trigger("chosen:updated");
				break;
			case 2:
				$("#status option[value=4]").prop("selected", "selected").siblings().removeAttr('selected');
				$("#status").trigger("chosen:updated");
				break;
			}
			//报表日期
			$("#orderBetListBetween").html(
					$("#startTime").val() + "——" + $("#endTime").val());
			$("#lotNames").html($("#lotCode").find("option:selected").text());
			var load = new Loading();
			load.init({
				target : "#betResultOrder"
			})
			load.start();
			var data = {
				code : $("#lotCode").val(),
				status : $("#status").val(),
				model : $("#model").val(),
				zhuiHao : zhuiHao,
				startTime : $("#startTime").val() + " 00:00:00",
				endTime : $("#endTime").val() + " 23:59:59",
				rows : 10,
				page : $("#pageNumber").val()
			}
			$.ajax({
				url : '${base}/lotteryV3/betInfo/getBcLotteryOrder.do',
				data : data,
				success : function(res) {
					if (res.page.totalCount == 0) {
						//无数据
						temp = '<tr><td colspan="10" class="nodate" height="200">暂无数据，<a href="${base}/lotteryV3/lottery.do">立即购彩</a></td></tr>';
						$("#betResultOrder").html(temp);
						$("#orderBetOrderTblFoot").hide();
						layerPage(res.page.totalCount,
								res.page.currentPageNo,
								res.page.totalPageCount);
					} else {
						addTemplate(res);
					}
					load.stop();
				}
			})
		}

		function addTemplate(res) {
			var temp = "", statusName = "";
			$
					.each(
							res.page.list,
							function(index, item) {
								var oper = "",sm="";
								switch (item.status) {
								case 1:
									statusName = '<span style="background-color: #428bca;">未开奖</span>';
									<c:if test="${!unCancleOrder}">oper = '<a href="javascript:void(0);" class="cheDan" orderid="'
											+ item.orderId
											+ '" lotcode="'
											+ item.lotCode
											+ '" from="member">撤单</a>';</c:if>
									break;
								case 2:
									statusName = '<span style="background-color: #5cb85c;">已中奖</span>';
									break;
								case 3:
									statusName = '<span style="background-color: #d9534f;">未中奖</span>';
									break;
								case 4:
									statusName = '<span style="background-color: #5bc0de;">已撤单</span>';
									break;
								case 5:
									statusName = '<span style="background-color: #f0ad4e;">回滚成功</span>';
									break;
								case 6:
									statusName = '<span style="background-color: #f0ad4e;">回滚异常</span>';
									break;
								case 7:
									statusName = '<span style="background-color: #f0ad4e">开奖异常</span>';
									break;
								case 8:
									statusName = '<span style="background-color: #f0ad4e">和局</span>';
									break;
								case 9:
									statusName = '<span style="background-color: #f0ad4e">合买失效</span>';
									break;
								}
								if(item.zhuiHao != 1){
									sm = "(追)";
								}
								if(item.jointPurchase != null && item.jointPurchase != 1){
									sm = "(合)";
								}
								temp += '<tr ' + (index % 2 != 0 ? 'class="bgcolor">' : '>')
									+ '<td><a class="BetInf" href="javascript:void(0);" orderid="' + item.orderId + '" lotcode="' + item.lotCode + '">'
									+ item.orderId + ''+sm+'</a><br>'+item.createTime+'</td><td>'
									+ item.lotName+'<br>' + item.qiHao + '</td><td>'
									+ item.playName + '</td><td>'
									+ item.multiple + '</td><td class="red">'
									+ (!item.lotteryHaoMa ? "- -" : item.lotteryHaoMa) + '</td><td><div style="width:170px;word-break: break-all;">'
									+ strLength(item.haoMa) + '</div></td><td>'
									+ item.buyMoney + '</td>';
								if (!item.winMoney) {
									temp += '<td>0.00</td>';
								} else {
									temp += '<td>' + item.winMoney + '</td>';
								}
								if(item.lotCode == 'LHC' || item.lotCode == 'FC3D' || item.lotCode == 'PL3'){
									temp += '<td>'+winMoney(item.buyMoney,item.odds,item.buyZhuShu)+'</td>';
								}else{
									temp += '<td>--</td>';
								}
								temp += '<td>' + statusName + '</td><td>'
										+ oper + '</td></tr>';
							})

			$("#betResultOrder").html(temp);
			$("#orderBetOrderTblFoot").show();
			$("#orderBetPageAmount").html(res.subBuyMoney);
			$("#orderBetAmount").html(res.sumBuyMoney);
			$("#orderWinAmount").html(res.sumWinMoney);
			layerPage(res.page.totalCount, res.page.currentPageNo,
					res.page.totalPageCount);
		}
		
		function strLength(hm){
			var len = hm.length;
			if(len > 100){
				return hm.substring(0,50)+"...";
			}
			return hm;
		}
		
		function winMoney(buyMoney,peiLv,zhuShu){
			//购买金额*赔率*中奖注数 - 本金*中奖注数
			buyMoney = parseFloat(buyMoney);
			peiLv = parseFloat(peiLv);
			zhuShu = parseInt(zhuShu);
			var value = buyMoney * peiLv;
			return (value - buyMoney).toFixed(2);
		}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>
