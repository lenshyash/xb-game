<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		 <div class="userRight">
            <div class="userTitle mgb10">网站公告</div>
            <div class="announcementList" style="min-height:703px;">
                <ul>
                    
                </ul>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
			})
			function initRdsData(p) {
				$(".fudetail_list").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
				var data = {
					pageNumber : p,
					pageSize : 10,
				}
				$.ajax({
					url : '${base}/getConfig/getArticleList.do',
					data : data,
					success : function(res) {
						console.log(res)
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.totalCount == 0){
							//无数据
						}else{
							addTemplate(res)
							page = res.currentPageNo;
							lotteryPage(res.totalCount,res.totalPageCount)
							lastPage = res.totalPageCount
							$("#pageNum").html(res.currentPageNo)
						}
					}
				})
			}
			function addTemplate(res){
				var html = '';
				$.each(res.list,function(index, item){
					html+='<li class="news_li"><a href="${base}/center/df/record/betrecord/noticeDetail.do?id='+item.id+'">'+item.title+'</a> <span class="CreatTime">'+getMyDate(item.updateTime)+'</span></li>'
				})
				$(".announcementList ul").html(html)
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>