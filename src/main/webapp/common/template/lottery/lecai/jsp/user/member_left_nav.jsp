<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="grid c4 menu">
	<ul>
		<li class="line"></li>
		<li ${index}${current}><a href="${base}/center/member/meminfo/cp3Page.do?form=index">会员中心</a>
		<div class="hr"></div></li>
		<li ${order}${cur} id="menu_betting_records"><a href="${base}/center/record/betrecord/cp3Page.do?form=order">彩票投注记录</a><div class="hr"></div></li>
		<li ${win}><a href="${base}/center/record/betrecord/cp3Page.do?form=win">彩票中奖记录</a><div class="hr"></div></li>
		<c:if test="${not empty duLiCaiPiao&&duLiCaiPiao eq 'off'}">
		<c:if test="${!isJoint}"><li ${joint}><c:if test="${isGuest}"><a onclick="alertMsg('试玩帐号不允许合买功能')" href="javascript:void(0)">合买记录</a></c:if>
		     <c:if test="${!isGuest}"><a href="${base}/center/record/betrecord/cp3JointPage.do">合买记录</a></c:if>
		<div class="hr"></div></li></c:if>
		</c:if>
		<c:if test="${isTyOnOff eq 'on'}"><li ${sportTab}><a href="${base}/center/record/betrecord/cp3SportPage.do">体育记录</a><div class="hr"></div></li></c:if>
		<c:if test="${isZrOnOff eq 'on'}"><li ${realTab}><a href="${base}/center/record/betrecord/cp3RealPage.do">真人记录</a><div class="hr"></div></li></c:if>
		<c:if test="${isChessOnOff eq 'on'}"><li ${chessTab}><a href="${base}/center/record/betrecord/cp3ChessPage.do">棋牌记录</a><div class="hr"></div></li></c:if>
		<c:if test="${isDzOnOff eq 'on'}"><li ${egameTab}><a href="${base}/center/record/betrecord/cp3EgamePage.do">电子记录</a><div class="hr"></div></li></c:if>
		<li ${mingxi}><a href="${base}/center/record/hisrecord/cp3Page.do">账户明细</a>
		<div class="hr"></div></li>
		<li ${info} id="zhanl.jugmv/;,kn hb g 
		haoxinxi"><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许修改账号信息')"href="javascript:void(0)"</c:if><c:if test="${!isGuest}">href="${base}/center/member/meminfo/cp3Page.do?form=info"</c:if>>账号信息</a><span
			class="j-num-msg flag hidn"></span>
		<div class="hr"></div></li>
		<li ${deposit}><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许充值')"</c:if> href="<c:if test="${isGuest}">javascript://</c:if><c:if test="${!isGuest}">${base}/center/banktrans/deposit/cp3Page.do</c:if>">充 值</a>
		<div class="hr"></div></li>
		<li ${draw}><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许提款')"</c:if> href="<c:if test="${isGuest}">javascript://</c:if><c:if test="${!isGuest}">${base}/center/banktrans/draw/cp3Page.do</c:if>">提 款</a>
		<div class="hr"></div></li>
		<c:if test="${(not empty isZrOnOff && isZrOnOff eq 'on')||(not empty isDzOnOff && isDzOnOff eq 'on')||(not empty isChessOnOff && isChessOnOff eq 'on')}">
		<li ${change}>
		<c:if test="${isGuest}"><a onclick="alertMsg('试玩帐号不允许转换额度')" href="javascript:void(0);">额度转换</a></c:if>
		<c:if test="${!isGuest}"><a href="${base}/center/banktrans/mnychg/cp3Page.do">额度转换</a></c:if>
		<div class="hr"></div></li></c:if>
		<c:if test="${not empty exchangeScore && exchangeScore eq 'on' }">
		<li ${exchange}><a href="${base}/center/banktrans/exchange/cp3Page.do">积分兑换</a>
		<div class="hr"></div></li>
		</c:if>
		<c:if test="${not empty letters && letters eq 'on'}">
		<li ${message} id="xiaoxizhongxin"><a href="${base}/center/news/message/cp3Page.do">消息中心</a>
		<span class="j-num-msg flag hidn"></span><div class="hr"></div></li>
		</c:if>
		<c:if test="${memberRateFlag}">
		<li ${backwater} id="fangshui"><a href="${base}/center/record/backwater/cp3Page.do?form=backwater">反水记录</a></li>
		</c:if>
		<c:if test="${loginMember.accountType eq 4}">
		<li class="line"></li>
		<li><a href="${base}/daili" target="_blank">代理中心</a>
		</li>
		<li ${teamChange} id="teamChange"><a href="${base}/center/record/betrecord/teamChange.do">团队账变</a><div class="hr"></div></li>
		<li ${teamStatic} id="teamStatic"><a href="${base}/center/record/betrecord/teamStatic.do">团队统计</a><div class="hr"></div></li>
		<li ${sponsoredLinks} id="sponsoredLinks"><a href="${base}/center/record/betrecord/sponsoredLinks.do">推广链接</a><div class="hr"></div></li>
		<li ${commission} id="commission"><a href="${base}/center/record/betrecord/commission.do" >业绩提成</a><div class="hr"></div></li>
		<li ${depositLog} id="depositLog"><a href="${base}/center/record/betrecord/depositLog.do">存款日志</a><div class="hr"></div></li>
		<li ${withdrawalLog} id="withdrawalLog"><a href="${base}/center/record/betrecord/withdrawalLog.do">取款日志</a><div class="hr"></div></li>
		</c:if>
		
	</ul>
</div>
<script type="text/javascript">
	//公用信息加载
	function alertMsg(msg){
		layer.msg(msg,{icon:1,offset:['30%']});
	}
	
	$(function(){
		$.ajax({
			url:'${base}/center/news/message/messageCount.do',
			success:function(res){
				if(res.messageCount > 0){
					$("#xiaoxizhongxin span").text(res.messageCount).removeClass('hidn');
					$("#all_unread").text(res.messageCount);
				}
			}
		})
		//
		$.ajax({
			url:'${base}/center/member/meminfo/accountCount.do',
			success:function(res){
				if(res.count>0){
					$("#zhanghaoxinxi span").text(res.count).removeClass("hidn");
				}
			}
		})
	})
</script>