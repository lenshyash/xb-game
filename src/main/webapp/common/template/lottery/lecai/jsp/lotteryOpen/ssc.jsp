<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 开奖(CQ/XJSSC+GX10) -->
<div class="dsLotto-result dsLotto-result--no5 status--on">
	<ul class="_group array cf">
		<li class="_item" style="margin-left: 0;">
			<div class="_ele-number ng-binding" id="last_result_0">0</div>
			<div class="_ele-unknown" style="animation-delay: 100ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">万</div>
		</li>
		<!-- / item -->
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_1">0</div>
			<div class="_ele-unknown" style="animation-delay: 200ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">千</div>
		</li>
		<!-- / item -->
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_2">0</div>
			<div class="_ele-unknown" style="animation-delay: 300ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">百</div>
		</li>
		<!-- / item -->
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_3">0</div>
			<div class="_ele-unknown" style="animation-delay: 400ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">十</div>
		</li>
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_4">0</div>
			<div class="_ele-unknown" style="animation-delay: 500ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">个</div>
		</li>
	</ul>
</div>