<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
	<meta name="description" content="${not empty webDescription?webDescription:_title}">
	<title>用户试玩注册-${_title}</title>
	<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
	<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/register_new.css?v=${caipiao_version}"  type="text/css">
	<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
	<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<input type="hidden" value="testGuest" id="registerStatus"/>
<div class="wrap-bg">
    <div class="wrapper">
        <div class="reg">
            <div class="reg-tit">
                <div class="fr">已有账号？<a class="orange" onclick="Base.flashed('regLogin','${base}/loginPage.do');">立即登录</a></div><h3>申请试玩帐号</h3>
            </div>
            <div class="reg-info" id="reg_form_1">
                <ul>
                    <li>
                        <div class="info-left">用户名：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-account-tip-icon"></label><input id="account" value="${userName }"unique="2" class="reg-int" maxlength="11" type="text" disabled></div>
                    </li>
                    <li>
                        <div class="info-left"><span class="red">* </span>密码：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input id="passwd" class="reg-int" maxlength="16" type="password"><div class="reg-notice-ok hidn"></div></div>
                        <div class="info-txt" id="_focus" style="display:none;">请输入6-16个英文或数字，且符合0~9或a~z字元 </div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    <li>
                        <div class="info-left"><span class="red">* </span>确认密码：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input class="reg-int" maxlength="16"  id="conpasswd" type="password"><div class="reg-notice-ok hidn"></div></div>
                        <div class="info-txt" id="_focus" style="display:none;">请再次输入密码</div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    
                    <li id="verifycode_div">
                        <div class="info-left"><span class="red">* </span>验证码：</div>
                        <div class="info-right info-codes" style="position:relative;">
                        	<label class="fl-small-tip-icon fl-reg-verify-tip-icon"></label>
                            <input name="verify" id="regVerifyCode" class="reg-int codes-int " unique="2" maxlength="5" type="text"><div class="reg-notice-ok hidn"></div>
                            <div id="need_captcha" class="reg_need_captcha">
                                <div class="yanzhengma">
                                   <span class="register_captcha_span" style="position:relative;">
                                       <img name="login_img" id="regVerifyImg">
                                       <a name="btn_refresh" onclick="Base.verifyImg();"><img src="${base}/common/template/lottery/lecai/images/refresh_1.png" style="cursor: pointer;"></a>
                                   </span>
                                </div>
                            </div>
                        </div>
                        <div class="info-txt" id="_focus" style="display:none;">请输入4位数字验证码</div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    <li>
                        <div class="info-left">&nbsp;</div>
                        <div class="info-right"><input name="reg_checkbox" checked="checked" type="checkbox">我已看过并同意<a class="orange" target="_blank" href="${base}/lotteryV3/regAgreement.do">《${_title}网络服务协议》</a></div>
                    </li>
                    <li>
                        <div class="info-left">&nbsp;</div>
                        <div class="info-right"><input id="btn-sub" onclick="Base.vaildate.userRegister('${base}/registerTestGuest.do')" class="submit" value="立即注册" type="submit"></div>
                    </li>
                </ul>
            </div>
            <div style="padding:0 0 20px 30px;line-height:24px;">
	            <p><b>备注：</b></p>
	            <ul>
	                <li>1.标记有&nbsp;<span class="red">*</span>&nbsp;者为必填项目。</li>
	                <li>2.每个试玩帐号从注册开始，有效时间为72小时，超过时间系统自动回收。</li>
	                <li>3.试玩帐号仅供玩家熟悉游戏，不允许充值和提款。</li>
	                <li>4.试玩帐号不享有参加优惠活动的权利。</li>
	                <li>5.试玩帐号的赔率仅供参考，可能与正式帐号赔率不相符。</li>
	                <li>6.其他未说明事项以本公司解释为准。</li>
	            </ul>
	        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$("#passwd").focus();
	$("body").keydown(function(event) {
    	if (event.keyCode == "13") {//keyCode=13是回车键
    		Base.vaildate.userRegister('${base}/registerTestGuest.do');
	 	}
	});
})
</script>
<jsp:include page="/member/${stationFolder}/include/footers.jsp">
	<jsp:param value="false" name="isVisible"/>
</jsp:include>
</body>
</html>