<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>购彩中心-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/lottery.css?v=" type="text/css">
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/lecaicss.css" type="text/css">
<link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css">
<style media="screen">
	.wrapCTCP .foot-top ul li span:hover {
		-webkit-transform:translateY(-6px);
    transform: translateY(-6px);
    transition: all 0.1s ease-in-out;
	}
	.wrapCTCP .foot-top ul li span:active {
		-webkit-transform:translateY(-4px);
    transform: translateY(-4px);
    transition: all 0.05s ease-in-out;
	}
	.roundNo li{
		width: 65px;
		height: 58px;
		background: url(../images/open_num.png) no-repeat;
	}
	.betRight .today .more {
		margin: 22px 0px 0 33px;
	}
	.font_gd {
    color: #555;
  }
  .betRight .box table td {
    background: #fff;
    height: 24px;
    line-height: 24px;
    text-align: center;
    font-weight: 400;
    color: #666;
    border-bottom: 1px dashed #ddd;
    font-size: 12px;
    border-right: 1px dashed #ddd;
  }
	.container .logo {
		border: 1px solid #ccc;
	}
</style>
</head>
<body>
<jsp:include page="/member/${stationFolder}/include/header.jsp"></jsp:include>
<div class="container">
  <div class="">
    <div class="logo">
      <div class="betTitle fix">
        <div class="betLogo">
          <h2 class="caizhong"></h2>
          <img src="">
					<p class="yinying"></p>
        </div>
        <div class="time">
          <div class="">
            <div class="timeTitle">
              距<b class="qishu"></b>期投注截止还有：
            </div>
            <em class="djs">00:00:00</em>
          </div>
        </div>
        <div class="announced">
          <div class="announcedTitle">
            第<b class="kaijiang"></b>期开奖号码：
          </div>
          <ul class="roundNo fix">
            <div class="kjdh" style="display:none">
              <img src="${base}/common/template/lottery/lecai/images/open_num.gif" >
              <img src="${base}/common/template/lottery/lecai/images/open_num.gif" >
              <img src="${base}/common/template/lottery/lecai/images/open_num.gif" >
            </div>
            <li class="yaohao" style></li>
            <li class="yaohao" style></li>
            <li class="yaohao" style></li>
          </ul>
        </div>
      </div>
      <div class="betContent fix">
        <div class="betLeft">
          <div class="">
            <div class="betNavCon">
              <ul class="betNav fix" style="transform: translateX(0px);">
              </ul>
            </div>
            <a class="betNavtab left">
              <span>
                <em></em>
              </span>
            </a>
            <a class="betNavtab right">
              <span>
                <em></em>
              </span>
            </a>
          </div>
          <div>
            <div class="">
              <ul class="betFilter">

              </ul>
              <%-- <ul class="betFilterAnd">
								<div class="">
									<a href="#" class="curr">骰宝</a>
								</div>
								<div class="">
									<a href="#">短牌</a>
								</div>
								<div class="">
									<a href="#">长牌</a>
								</div>
								<div class="">
									<a href="#">大小骰宝</a>
								</div>
								<div class="">
									<a href="#">围骰</a>
								</div>
								<div class="">
									<a href="#">全骰</a>
								</div>
								<div class="">
									<a href="#">点数</a>
								</div>
              </ul> --%>
              <div class="betTip">
                <i class="iconfont"><img src="${base}/common/template/lottery/lecai/images/jinggao.png"></i>
                <a id="game_shows">点击查看游戏说明！</a>
              </div>
            </div>
						<%-- <div class="wrapCTCP">
							<div  class="bar-bet">
								<div >
									<%-- <div  class="checkbox pull-left w50 bet-add ">
										<label  style="margin-top: 4px;">
											<input  type="checkbox">预设</label>
									</div> --%>
									<%-- <div  class="input-group pull-left w150 bet-add ">
										<span  class="input-group-addon">预设金额</span>
										<input  type="text" maxlength="6" class="form-control">
									</div>
									<button  type="button" class="btn btn-danger fl bet-add reset" style="background: rgb(69, 89, 100); border-color: rgb(69, 89, 100);">重置</button>
									 <button  type="button" class="btn btn-danger fl bet-add betbtn1" >提交注单</button>
								 </div>
									 <div  class="foot-top fl" style="display: block;">
										 <ul class="foots">
											 <li  class="cm1">
												 <span >10</span>
											 </li>
											 <li  class="cm2">
												 <span >50</span>
											 </li>
											 <li  class="cm3">
												 <span >1百</span>
											 </li>
											 <li  class="cm4">
												 <span >5百</span>
											 </li>
											 <li  class="cm5">
												 <span >1千</span>
											 </li>
											 <li  class="cm6">
												 <span >5千</span>
											 </li>
										</ul>
									</div>
								<div  style="clear: both;">

								</div>
							</div>
						</div> --%>

            <div class="">
              <div class="mode">
                <div class="content">
                  <!-- 投注页面 -->

                  <!-- 投注页面 -->
                </div>
								<div class="Panel">
									<p class="betTotal">
										您选择了
										<em class="detail">0</em>
										注,每注
											<input refinfor="!0" type="text" class="eachPrice">
										</em>
										元
									</p>
									<a id="betBtn" class="betBtn">确认选号</a>
								</div>
								<div data-v-956bf8b8="" class="checkedListCon">
									<div class="checkedList">
										<table>
											<tbody>

											</tbody>
										</table>
									</div>
									<p class="betTotal">
										方案注数
								    <em class="zdsl">0</em> <!---->
								    注，金额
								    <i class="tzje">0</i> <!---->
								    元
									</p>
									<!---->
								 <a id="betbtn" class="betBtn">马上投注</a>
								 <a id="stopBet" disable="true" class="betBtn greyBtn" style="display: none;">停止开奖</a>
							 </div>
              </div>
            </div>
						<%-- <div class="wrapCTCP">
							<div class="bar-bet">
								<div > --%>
									<%-- <div  class="checkbox pull-left w50 bet-add ">
										<label  style="margin-top: 4px;">
											<input  type="checkbox">预设</label>
									</div> --%>
									<%-- <div  class="input-group pull-left w150 bet-add ">
										<span  class="input-group-addon">预设金额</span>
										<input  type="text" maxlength="6" class="form-control">
									</div>
									<button  type="button" class="btn btn-danger fl bet-add reset" style="background: rgb(69, 89, 100); border-color: rgb(69, 89, 100);">重置</button>
									 <button  type="button" class="btn btn-danger fl bet-add betbtn1">提交注单</button>
								 </div>
									 <div  class="foot-top fl" style="display: block;">
										 <ul class="foots">
											 <li  class="cm1">
												 <span >10</span>
											 </li>
											 <li  class="cm2">
												 <span >50</span>
											 </li>
											 <li  class="cm3">
												 <span >1百</span>
											 </li>
											 <li  class="cm4">
												 <span >5百</span>
											 </li>
											 <li  class="cm5">
												 <span >1千</span>
											 </li>
											 <li  class="cm6">
												 <span >5千</span>
											 </li>
										</ul>
									</div>
								<div  style="clear: both;">

								</div>
							</div>
						</div> --%>
          </div>
        </div>
        <div class="betRight">
          <div class="today box">
            <h3 class="">今日开奖</h3>
            <div class="more">
							<a href="${base}/lotteryV3/trend.do" id="trendChart" target="view_window">走势图</a>
							<a href="${base}/lotteryV3/rule.do" class="" target="view_window">玩法说明</a>
            </div>
            <div class="ResultsList">
              <table  width="100%" border="0" cellspacing="0" cellpadding="0" id="fn_getoPenGame" class="ty_table_gameBet curr">
                <thead>
                  <tr>
                    <th scope="col">期号</th>
                    <th scope="col">开奖号</th>
										<th scope="col">和值</th>
										<th scope="col">形态</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <div class="mybet box">
            <h3 id="tz_delclass" onclick="wdtz()">我的投注</h3>
            <%-- <h3 id="zh_delclass" class="notSelect" onclick="wdzh()">我的追号</h3> --%>
            <table class="_mybetContent" width="100%" broder="0" cellspacing="0" cellpadding="0">
              <tr>
                <th>期号</th>
                <th>投注金额</th>
                <th>奖金</th>
                <th>操作</th>
              </tr>
							<tbody class="tz_list">
              </tbody>
            </table>
            <table class="more">
              <tbody>
                <tr>
                  <td><a href="/center/df/record/betrecord/cpDfPage.do?form=order" class="font_gd">更多>></a></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="winningList box">
            <h3>中奖信息</h3>
            <%-- <h3 class="notSelect">昨日奖金榜</h3> --%>
            <div di="Ranking" class="winnerListSlide" style="width: 100%; box-sizing: content-box;">
              <div id="bettingList" style="width: 100%; display: none;">
                <div class="optionTab clearfix">
                  <div class="isSelect">本局投注</div>
                  <div class="">本日盈利</div>
                </div>
                <div class="bd tempWrap">
                  <ul class="winnerList betList"></ul>
                </div>
              </div>
              <div class="lotteryInfo" style="width:100%">
                <table width="100%">
                  <tbody>
                    <tr>
                      <th>中奖信息实时更新</th>
                    </tr>
                  </tbody>
                </table>
                <div class="bd tempWrap" id="gundong">
                  <ul id="carousel" class="winnerList">
                    <li id="li_0">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/1.jpg">
                        <p class="color">py***6&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥10.97</span>
                        </p>
                    </li>
                    <li id="li_1">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/2.jpg">
                        <p class="color">tp***4&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥8.80</span>
                        </p>
                    </li>
                    <li id="li_2">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/3.jpg">
                        <p class="color">zz***1&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥30.17</span>
                        </p>
                    </li>
                    <li id="li_3">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/4.jpg">
                        <p class="color">qs***9&nbsp;&nbsp;1分快3
                          <br >喜中
                          <span>¥11.27</span>
                        </p>
                    </li>
                    <li id="li_4">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/5.jpg">
                        <p class="color">静***3&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥38.46</span>
                        </p>
                    </li>
                    <li id="li_5">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/6.jpg">
                        <p class="color">st***1&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥9.37</span>
                        </p>
                    </li>
                    <li id="li_6">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/7.jpg">
                        <p class="color">ps***5&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥21.37</span>
                        </p>
                    </li>
                    <li id="li_7">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/8.jpg">
                        <p class="color">ml***8&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥22.5</span>
                        </p>
                    </li>
                    <li id="li_8">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/9.jpg">
                        <p class="color">财***6&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥88.8</span>
                        </p>
                    </li>
                    <li id="li_9">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/10.jpg">
                        <p class="color">66***6&nbsp;&nbsp;1分快3
                          <br >喜中
                          <span>¥11.2</span>
                        </p>
                    </li>
                    <li id="li_10">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/11.jpg">
                        <p class="color">he***4&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥56.7</span>
                        </p>
                    </li>
                    <li id="li_11">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/12.jpg">
                        <p class="color">yt***2&nbsp;&nbsp;1分快3
                          <br >喜中
                          <span>¥25.67</span>
                        </p>
                    </li>
                    <li id="li_12">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/13.jpg">
                        <p class="color">we***6&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥103.7</span>
                        </p>
                    </li>
                    <li id="li_13">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/14.jpg">
                        <p class="color">pe***9&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥208.17</span>
                        </p>
                    </li>
                    <li id="li_14">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/15.jpg">
                        <p class="color">li***8&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥350.12</span>
                        </p>
                    </li>
                    <li id="li_15">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/1.jpg">
                        <p class="color">qq***6&nbsp;&nbsp;1分快3
                          <br >喜中
                          <span>¥17.8</span>
                        </p>
                    </li>
                    <li id="li_16">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/2.jpg">
                        <p class="color">tt***3&nbsp;&nbsp;1分快3
                          <br >喜中
                          <span>¥67.27</span>
                        </p>
                    </li>
                    <li id="li_17">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/4.jpg">
                        <p class="color">li***2&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥358.12</span>
                        </p>
                    </li>
                    <li id="li_18">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/7.jpg">
                        <p class="color">振***7&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥120.97</span>
                        </p>
                    </li>
                    <li id="li_19">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/10.jpg">
                        <p class="color">jv***9&nbsp;&nbsp;极速快3
                          <br >喜中
                          <span>¥173.46</span>
                        </p>
                    </li>
                    <li id="li_20">
                      <img src="${base}/common/template/lottery/lecai/images/touxiang/8.jpg">
                        <p class="color">hh***3&nbsp;&nbsp;1分彩
                          <br >喜中
                          <span>¥33.84</span>
                        </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div di="lotteryCardBox" class="cardBox hide">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<%-- 下一期提示弹窗 --%>
<div class="nextAlert" style="display:none;">
  <div class="nextAlertBg"></div>
  <div class="section">
    <div class="content">
      <h3>温馨提示</h3>
      <span class="layermend closeNextAlert"></span>
      <p style="	margin-top: 30px;">
        <span class="upQiHao">0105862</span>期已截止
      </p>
      <p>
        当前期号<span class="nextQiHao" style="color:red;">0105862</span>
      </p>
      <p style="padding-bottom:10px;">投注时请注意期号</p>
      <div class="">
        <div class="button closeNextAlert">
          确定
        </div>
      </div>
    </div>
  </div>
<%-- 下一期提示弹窗 --%>
</div>
<%-- 投注提示弹框 --%>
<div id="layermbox16" class="layermbox layermbox0 layermshow" index="16" style="display:none;">
  <div class="laymshade" id="shade"></div>
  <div class="layermmain">
    <div class="section">
      <div class="layermchild layerBet layermanim">
        <h3 style="">投注确认</h3>
        <button class="layermend">
        </button>
        <div class="layermcont" id="layermcont">
        </div>
        <div class="layermbtn">
          <span type="0" id="quxiao">取消</span>
          <span id="submitBtn" type="1">确认投注</span>
        </div>
      </div>
    </div>
  </div>
</div>
<%-- 投注提示弹框 --%>
<!-- 游戏说明 -->
<div class="game_shows" style="display:none;">
  <div class="bac_color"></div>
  <div class="page_layout">
    <div class="section_">
      <div class="window_ explain instructions">
        <h3 style="">游戏说明</h3>
        <button class="layermend" id="tuichu"></button>
        <div class="text_shows">

        </div>
        <div class="layermbtn"><span type="1">确定</span></div>
      </div>
    </div>
  </div>
</div>
<!-- 游戏说明 -->
<%-- 投注成功弹窗 --%>
<div id="layermbox17" class="layermbox layermbox0 layermshow" style="display:none;">
  <div class="layermmain">
    <div class="section">
      <div class="layermchild layerBet layermanim" style="min-width: 160px;min-height: 50px;border: 14px solid #e4e4e4;">
        <div class="touzuTC">
          <img src="${base}/common/template/lottery/lecai/images/2222.png" alt="">
            投注成功
        </div>
      </div>
    </div>
  </div>
</div>
<%-- 投注成功弹窗 --%>
<script type="text/javascript">
var cart = [];
var lotList=[];
var Latest_Lottery,Latest_openLottery;  // 开奖结果和投注记录;
htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/daxiaoshaibao.html",async:false});
$(".mode .content").html(htmlobj.responseText);
  // 倍数减一
  $('.sub').click(function(){
    let money = $('.money').val();
    if(!money){
      $('.money').val(1)
    }else if (money-0 > 1) {
      $('.money').val((money-0)-1)
      $('.jine').text( (($('.sscCheckNumber .curr').length)*($('.money').val()-0)*2).toFixed(2) )
    }
  })
  // 当前倍数
  $('.money').on('input propertychange', function() {
    $('.jine').html($('.money').val()*2);
  });
  // 倍数加一
  $('.add').click(function(){
    let money = $('.money').val();
    if(!money){
      $('.money').val(1)
    }else {
      $('.money').val((money-0)+1)
    }
    $('.jine').text((($('.sscCheckNumber .curr').length)*($('.money').val()-0)*2).toFixed(2))
  })

  // 点击显示/隐藏 class
  function wdtz(){
    $('#tz_delclass').toggleClass('notSelect',false)
    $('#zh_delclass').toggleClass('notSelect',true)
    $('._betChaseAnimation').attr('style','display:none');
    $('._mybetContent').removeAttr("style");
  }
  // 点击显示/隐藏 class
  function wdzh(){
    $('#tz_delclass').toggleClass('notSelect',true)
    $('#zh_delclass').toggleClass('notSelect',false)
    $('._mybetContent').attr('style','display:none');
    $('._betChaseAnimation').removeAttr("style");
  }

  // 无缝滚动
  function noticeUp(obj,top,time) {
       $(obj).animate({
       marginTop: top
       }, time, function () {
       $(this).css({marginTop:"0"}).find(":first").appendTo(this);
       })
      }
    var MyMar = setInterval("noticeUp('#gundong ul','-51px',1000)", 2000);

  $(function(){
    // 玩法
    let lotCode = location.search.replace('?lotCode=', '');
    // lotGroup 1=时时彩，2=低频彩，3=PK10，5=11选5，6=香港彩，7=快三，8=快乐十分，9=PC蛋蛋
    // duration 封盘时间， getLoctterysLength 彩种分类长度，
    // ActiveQiHao 当前到期号，rules 所有玩法列表, rule当前选中玩法
    var lotGroup,duration,getLoctterysLength,ActiveQiHao,rules,rule,caizhong,qihao;
    var navIndex = 6;
		// 获取当前时间
	  var year,month,day;
	  var date = new Date();
	  year = date.getFullYear();
	  month = date.getMonth() + 1;
	  day = date.getDate();
	  var startTime = year + "-" + month + "-" + day + " 00.00.00";
	  var endTime = year + "-" + month + "-" + day + " 23.59.59";
<%--	  <c:if test="${imgUrl == '' || imgUrl == null }">--%>
    $('.betLogo img').attr('src','${imgUrl}')
<%--      </c:if>--%>
<%--      <c:if test="${imgUrl != '' }">--%>
<%--        $('.betLogo img').attr('src','${imgUrl}')--%>
<%--      </c:if>--%>
    //获取彩种玩法到接口
    $.post("/native/getLotGamePlays.do",{lotCode:lotCode},function(result){
      caizhong = result.content.name;
      $('.caizhong').html(caizhong)
      rules = result.content.rules;
      rule = rules[0];
      var results = rule.rules[0].oddPlayItems
      // 添加玩法列表并且默认选中第一个
      var ruless = []
      $.each(results,function (index,items) {
        if (items.name == "大小骰宝"){
          ruless.push({name:"大小骰宝",sortNo:items.sortNo})
        }
        if (items.name == "短牌"){
          ruless.push({name:"短牌",sortNo:items.sortNo})
        }
        if (items.name == "长牌"){
          ruless.push({name:"长牌",sortNo:items.sortNo})
        }
        if (items.name == "围骰"){
          ruless.push({name:"围骰",sortNo:items.sortNo})
        }
        if (items.name == "全骰"){
          ruless.push({name:"全骰",sortNo:items.sortNo})
        }
      })
      ruless.push({name:"点数"})
      // var ruless = [
      //     {name:"大小骰宝"},
      //     {name:"短牌"},
      //     {name:"长牌"},
      //     {name:"围骰"},
      //     {name:"全骰"},
      //     {name:"点数"},
      // ]
      rulesLi = ''
      for(let i in ruless){
        if(i==0){
          rulesLi+= '<li class="curr">'+ruless[i].name+'</li>'
        }else {
          rulesLi+= '<li>'+ruless[i].name+'</li>'
        }
      }
      $('.betFilter').append($(rulesLi));

      // 所有玩法的切换事件

      $('.betFilter li').click(function(){
        let index = $(this).index();
        $(this).addClass('curr').siblings().removeClass('curr');
        rule = rules[index];
        ruleLi = ''

        $('.betFilterAnd').html($(ruleLi));
        // 默认删除
          $(".mode .content").html('');
        let curr = $('.betFilter .curr').text();
        // console.log(curr);
        // 判断跳转路由
				switch (curr) {
					case '大小骰宝':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/daxiaoshaibao.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					case '短牌':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/duanpai.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					case '长牌':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/changpai.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					case '围骰':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/weishai.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					case '全骰':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/quanshai.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					case '点数':
						htmlobj=$.ajax({url:"${base}/common/template/lottery/lecai/jsp/cz/ks/dianshu.html",async:false});
						$(".mode .content").html(htmlobj.responseText);
						break;
					default:

				}
      });
      $('.betFilter .curr').trigger('click')
    });

    // 获取历史开奖
    Latest_Lottery = function(first) {
      $.post("/native/draw_notice_details_data.do",{lotCode:lotCode},function(result){

        if (result.success == false) {
          return false
        }

        var kjlist = result.content.list.slice(0,10);
				var kjlist_1 = result.content.list.slice(1,11);
				// 定时判断开奖结果，若为 ？ 则继续获取
				var a = $(".qishu").text()-0;
				// console.log(a);
				if (kjlist[0].haoMaList[0] == "?" || a - result.content.list[0].qiHao > 1) {
					setTimeout(function() {
						Latest_Lottery();
					},5000)
				}else {
					$('.kjdh').hide();
					$('.kjdh').siblings().show();
				}

        // 开奖号码
				if (kjlist[0].haoMaList[0] == '?') {
					$('.ResultsList tbody').html('')
	        for(let i in kjlist_1){
	          $('.ResultsList tbody').append($('<tr>'+
	                              '<th scope="col">'+kjlist_1[i].qiHao+'</th>'+
	                              '<th scope="col" class="kjhm">'+kjlist_1[i].haoMaList.join(',')+'</th>'+
	                              '<th scope="col">'+kjlist_1[i].gySum+'</th>'+
																'<th scope="col"><em class="'+ (kjlist[i].sizeStr=="小"? "xiao":kjlist[i].sizeStr=="大"? "da":"baozi") +'">'+kjlist[i].sizeStr+'</em><em class="'+ (kjlist[i].sdStr=="单"? "dan":kjlist[i].sdStr=="双"? "shuang":"baozi") +'">'+kjlist_1[i].sdStr+'</em></th>'+
	                              '</tr>'))
	        }
					$('.kjdh').show();
					$('.yaohao').hide();
				}else {
					for(let i in kjlist[0].haoMaList){
						let path = 'url(/common/template/lottery/lecai/images/ks_0'+kjlist[0].haoMaList[i]+'.png) no-repeat'
						$('.yaohao').eq(i).css('background', path)
	        }
					$('.ResultsList tbody').html('')
	        for(let i in kjlist){
	          $('.ResultsList tbody').append($('<tr>'+
	                              '<th scope="col">'+kjlist[i].qiHao+'</th>'+
	                              '<th scope="col" class="kjhm">'+kjlist[i].haoMaList.join(',')+'</th>'+
	                              '<th scope="col">'+kjlist[i].gySum+'</th>'+
																'<th scope="col" class="zhuangtai"><em class="'+ (kjlist[i].sizeStr=="小"? "xiao":kjlist[i].sizeStr=="大"? "da":"baozi") +'">'+kjlist[i].sizeStr+'</em><em class="'+ (kjlist[i].sdStr=="单"? "dan":kjlist[i].sdStr=="双"? "shuang":"baozi") +'">'+kjlist[i].sdStr+'</em></th>'+
	                              '</tr>'))
	        }
				}
      });
    }

		setTimeout(function () {
			var xt = $(".zhuangtai em").text()
			if ($(".zhuangtai em").text() == "大" || $(".zhuangtai em").text() == "单") {
				$(".zhuangtai em").addClass("dadan");
			}else if ($(".zhuangtai em").text() == "小" || $(".zhuangtai em").text() == "双") {
				$(".zhuangtai em").addClass("xiaoshuang");
			}
		}, 300);

    // 获取倒计时函数
    var getCountDown = function(next){
      $.post("/native/getCountDown.do",{lotCode:lotCode,version: 5},function(result){
        if (result.success == false) {
          alert('尚未开盘')
          return false
        }
        qihao = result.content.qiHao;
        $('.qishu').html(qihao);
        Latest_Lottery();
        var activeTime = result.content.activeTime;
        var serverTime = result.content.serverTime;
        if(next){
          $('.upQiHao').html(ActiveQiHao);
          $('.nextQiHao').html(result.content.qiHao);
          $('.nextAlert').show();
          $('.kjdh').show();
          $('.kjdh').siblings().hide();
          // setTimeout(function(){
          //   $('.kjdh').hide();
          //   $('.kjdh').siblings().show();
          // },duration*1000)
        }

        ActiveQiHao = result.content.qiHao;

          // 获取倒计时的时间戳
          var times = activeTime - serverTime;
          // 获取时间戳转换成时分秒
          var timeItv = setInterval(function(){
            // 获取下一期的投注截止时间
            if (times<1000 && times > -1000){
              clearInterval(timeItv);
              getCountDown(true);
            };
            times -= 1000;
            getfpTime(times)
						$('.kaijiang').html($('.qishu').text()-1);
          },1000)
      });
    }
    // 倒计时转换时分秒格式函数
    var getfpTime = function (times) { //参数1，封盘时间，2，服务器时间
        let hour = parseInt(times / 1000 / 60 / 60) < 10 ? '0' + parseInt(times / 1000 / 60 / 60) : parseInt(times / 1000 / 60 / 60); //小时
        let Minute = parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60) < 10 ? '0' + parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60) : parseInt((times - parseInt(hour) * 60 * 60 * 1000) / 1000 / 60); //分钟
        let second = parseInt(times / 1000 % 60);
        second < 10 ?  second = '0' + second : '';
        var fpTime = hour + ':' + Minute + ':' + second; //封盘时间
				let countdown = parseInt(times / 1000 % 60);
	      // console.log(countdown);
	      if (countdown < 0) {
	        $('.djs').text("封盘中...");
	      }else {
	        $('.djs').html(fpTime);
	      }
      }

    //进入页面获取一次历史开奖
    Latest_Lottery(true);
    // 彩种列表导航
    $.post("/native/getLoctterys.do",{lotGroup:lotGroup,version: 5},function(result){
			// 彩种分类
			switch (lotCode) {
					case 'FFC':
					case 'EFC':
					case 'CQSSC':
					case 'TJSSC':
					case 'WFC':
					case "HNFFC":
					case "HNWFC":
					case "AZXY5":
					case 'XJSSC':
						lotGroup = 1;
						break;
					case 'FC3D':
					case 'PL3':
						lotGroup = 2;
						break;
					case 'BJSC':
					case 'XYFT':
					case 'LXYFT':
					case 'AZXY10':
					case 'FFSC':
					case 'SFSC':
					case 'WFSC':
					case 'WFFT':
					case 'LBJSC':
					case 'SFFT':
					case 'XSFSC':
					case 'XWFSC':
						lotGroup = 3;
						break;
					case 'GD11X5':
					case 'SD11X5':
					case 'JX11X5':
					case 'SH11X5':
					case 'GX11X5':
						lotGroup = 5;
						break;
					case 'LHC':
                    case 'WFLHC':
					case 'SFLHC':
					case 'TMLHC':
					case 'HKMHLHC':
						lotGroup = 6;
						break;
					case 'JSSB3':
                    case 'SFK3':
					case 'HBK3':
					case 'AHK3':
					case 'SHHK3':
					case 'HEBK3':
					case 'GXK3':
					case 'GZK3':
					case 'BJK3':
					case 'JXK3':
					case 'GSK3':
					case 'FFK3':
					case 'WFK3':
					case 'JPK3':
					case 'KRK3':
					case 'HKK3':
					case 'AMK3':
					case 'TMK3':
                    case 'ESK3':
						lotGroup = 7;
						break;
					case 'CQXYNC':
					case 'HNKLSF':
					case 'GDKLSF':
						lotGroup = 8;
						break;
					case 'PCEGG':
					case 'JND28':
						lotGroup = 9;
						break;
					default:
				}

      getCountDown(false);
      getLoctterysLength = result.content.length;
      for(let i in result.content){
        lotList.push(result.content[i])
        if (result.content[i].code == lotCode) {
          duration = result.content[i].duration;
        }
      }
      for(let i in lotList){
        if (lotList[i].lotGroup == lotGroup && lotList[i].code == lotCode) {
          $('.betLeft .betNav').append($('<li class="active" lotCode="'+lotList[i].code+'">'+lotList[i].name+'</li>'))
        }else if (lotList[i].lotGroup == lotGroup && lotList[i].code != lotCode) {
          $('.betLeft .betNav').append($('<li lotCode="'+lotList[i].code+'">'+lotList[i].name+'</li>'))
        }
      };
      //彩种切换
      $('.betNav li').click(function(){
        location.href = location.protocol+'//'+location.host+location.pathname+'?lotCode='+$(this).attr('lotCode');
      })
    });
		// 投注记录
	  Latest_openLottery = function(first) {
	    $.post("/native/get_lottery_order.do",{startTime:startTime,endTime:endTime,code:'',status:' ',page: 1,rows: 20,lhc: 1},function(result){
	      var tzlist = result.page.list.slice(0,5);
	      $('._mybetContent .tz_list').html('');
	      for (var i in tzlist) {
	        var zhMoney = 0;
	        switch (tzlist[i].status) {
	          case 2:
	            zhMoney = tzlist[i].buyMoney * tzlist[i].odds;
	            break;
	          default:

	        }
	        $('._mybetContent .tz_list').append($('<tr>'+
	                            '<td>'+tzlist[i].qiHao+'</td>'+
	                            '<td>'+tzlist[i].buyMoney+'</td>'+
	                            '<td>'+zhMoney+'</td>'+
	                            '<td><a href="/center/df/record/betrecord/cpDfPage.do?form=order" class="font_gd">详情</a></td>'+
	                            '</tr>'))
	      }
				if(tzlist.length < 5){
					for(var i=0;i< (5-tzlist.length);i++){
						$('._mybetContent .tz_list').append($('<tr>'+
																'<td></td>'+
																'<td></td>'+
																'<td></td>'+
																'<td></td>'+
																'</tr>'))
					}
				}
	    })
	  }
	  Latest_openLottery(true);
    // 彩种列表导航动画
    $('.betLeft .left').click(function(){
      if(getLoctterysLength<=6 || navIndex === 6){
        alert('已经到顶啦')
        return false;
      }
      navIndex -= 1
        $('.betLeft .betNav').animate({left:-((navIndex-6)*112)+'px'},"fast");
    })
    $('.betLeft .right').click(function(){
      if(getLoctterysLength<=6 || getLoctterysLength === navIndex){
        alert('已经到顶啦')
        return false;
      }
      navIndex += 1
      $('.betLeft .betNav').animate({left:-((navIndex-6)*112)+'px'},"fast");
    })

    //下一期提示弹窗关闭事件
    $('.closeNextAlert').click(function(){
      $('.nextAlert').hide();
    });

    $('#betbtn').click(function(){
      $('#layermbox16').removeAttr("style");
    });
    $('.layermend').click(function(){
      $('#layermbox16').attr('style','display:none')
    });
    $('#quxiao').click(function(){
			$('#layermbox16').attr('style','display:none');
			$('.textarea').html('');
			$('.checkedList table tbody').html('');
			$('.zdsl').text('0');
			$('.tzje').text('0');
			cart = [];
    });
    $('#submitBtn').click(function(){
      $('#layermbox16').attr('style','display:none')
    });
  });
// 游戏说明隐藏
 $('#tuichu').click(function(){
   $('.game_shows').css('display','none')
 })
 $('.layermbtn').click(function(){
  $('.game_shows').css('display','none')
 })
 // 对象深拷贝函数
 function deepCopy(object){
   let obj = new Object();
   for(let k in object){
     obj[k] = object[k]
   }
   obj.isActive = false;
   return obj
  }

	// 计算组合方式, 此方式m不可小于或者等于n，否则会导致死循环卡住 !!!!!
var getFlagArrs = function (m, n) {
  if(m<n){
    return  []
  }else if (m===n) {
    return  [0]
  }
  if (!n || n < 1) {
    return [];
  }
  var resultArrs = [],
    flagArr = [],
    isEnd = false,
    i, j, leftCnt;
  for (i = 0; i < m; i++) {
    flagArr[i] = i < n ? 1 : 0;
  }
  resultArrs.push(flagArr.concat());
  while (!isEnd) {
    leftCnt = 0;
    for (i = 0; i < m - 1; i++) {
      if (flagArr[i] == 1 && flagArr[i + 1] == 0) {
        for (j = 0; j < i; j++) {
          flagArr[j] = j < leftCnt ? 1 : 0;
        }
        flagArr[i] = 0;
        flagArr[i + 1] = 1;
        var aTmp = flagArr.concat();
        resultArrs.push(aTmp);
        if (aTmp.slice(-n).join("").indexOf('0') == -1) {
          isEnd = true;
        }
        break;
      }
      flagArr[i] == 1 && leftCnt++;
    }
  }
  return resultArrs;
}
</script>
<style media="screen">
</style>
<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>
