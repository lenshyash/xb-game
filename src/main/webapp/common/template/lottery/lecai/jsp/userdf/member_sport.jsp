<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="zh-cn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
    <meta name="description" content="${not empty webDescription?webDescription:_title}">
    <title>会员中心 - ${_title}</title>
    <link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
    <jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
    <link rel="stylesheet"
          href="${base}/common/template/lottery/lecai/css/user_content.css?v="
          type="text/css">
    <script type="text/javascript"
            src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
    <jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
    .searchDetail table th,td{
        font-size:12px;
    }
</style>
<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
<div class="container" style="margin-top:20px;min-height:800px;">
    <jsp:include page="member_left_nav.jsp"></jsp:include>
    <div class="userRight">
        <div class="userTitle mgb10" id="pageName">体育投注记录</div>
        <div class="userMain mgb10">
            <%-- <ul class="todayView mgb10">
            <li>今日概况</li>
            <li>投注金额：<span>${-allBetAmount}元</span></li>
            <li>中奖金额：<span>${yingkuiAmount}元</span></li>
            <li>盈利：<span>${allWinAmount}元</span></li>
        </ul> --%>
            <ul class="searchFirst">
                <%--                <li><span>彩种：</span>--%>
                <%--                    <ins class="selectIcon">--%>
                    <%--                        <select class="userSelect" name="code" id="lotCode">--%>
                        <%--                            <option value="">所有彩种</option>--%>
                        <%--                            <c:forEach items="${lotList}" var="lot">--%>
                            <%--                                <option value="${lot.code}"<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>--%>
                        <%--                            </c:forEach>--%>
                    <%--                        </select>--%>
                <%--                        <em></em>--%>
                <%--                    </ins>--%>
                <%--                </li>--%>
            <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
            </li>
            <li class="_state">
                    	<span>类型：
       						<a class="userSearch time active" value=" ">全部</a>
       						<a class="userSearch time " value="2">已中奖</a>
       						<a class="userSearch time " value="3">未中奖</a>
       						<a class="userSearch time " value="1">等待开奖</a>
       						<a class="userSearch time " value="4">撤单</a>
       					</span>
            </li>
        </ul>
        <ul class="searchFirst accountValue" style="display:none;">
            <li class="_state" style="display: flex;align-items: center;padding-left:21px;">
                <span>账号:</span>
                <input class="inputText" id="account" value="" type="text" placeholder="下级投注查询" style="background:#fff;height:30px;margin-left:10px;">
                <input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
            </li>
        </ul>
        <div class="searchDetail">
            <table>
                <tbody>
                <tr>
                    <th class="touzhuName" style="display:none;">账号</th>
                    <td>单号/投注日期</td>
                    <td>投注类型</td>
                    <td>投注项</td>
                    <td>投注额</td>
                    <td>提交状态</td>
                    <td>结算状态</td>
                    <td>派彩金额</td>
                </tr>
                </tbody>
                <tbody class="Records_listCont">
                <tr style="border-bottom: 0px;">
                    <td colspan="100">
                        <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                        <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                    </td>
                </tr>
                </tbody>
                <tfoot id="orderBetOrderTblFoot" style="display:none;">
                    <tr>
                        <td colspan="3" style="text-align: right">总计</td>
                        <td class="red" id="orderBetAmount"></td>
                        <td colspan="2">&nbsp;</td>
                        <td class="red" id="orderWinAmount"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
            <div class="pageNav">
                <div  class="pageNav">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
        <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
    </div>
</div>
</div>
<script>
    $(function () {
        initRdsData(1)
        $("._time a").click(function () {
            $(this).addClass('active').siblings('a').removeClass('active')
            today($(this).attr('value'))
            initRdsData(1)
        })
        $("._state a").click(function () {
            $(this).addClass('active').siblings('a').removeClass('active')
            lottereyStatus = $(this).attr('value')
            initRdsData(1)
        })
        $('#lotCode').change(function(){
            lottereyStatus = $(this).attr('value')
            initRdsData(1)
        })
    })

    function initRdsData(p) {
        $(".Records_listCont").html(noTakeNotes)
        $(".notContent").hide()
        $(".loadingGif").show()
        if(!endTime){
            endTime = startTime
        }
        var data = {
            startTime : startTime + " 00:00:00",
            endTime : endTime + " 23:59:59",
            rows : 10,
            page : p
        }
        $.ajax({
            url : '${base}/center/df/record/betrecord/sportrd.do',
            data : data,
            success : function(res) {
                $(".notContent").show()
                $(".loadingGif").hide()
                if(res.total > 0){
                    addTemplate(res)
                    page = p;
                    totalPage = Math.ceil((res.total/10))
                    lotteryPage(res.total,totalPage)
                    lastPage = totalPage
                    $("#pageNum").html(p)
                }
            }
        })
    }
    function addTemplate(res){
        var html ='', bd=null;
        $.each(res.rows, function (index,item) {
            bd = new Date(item.bettingDate);
            html += '<tr'+(index%2!=0?' class="bgcolor"':'')+'><td>'+item.bettingCode + '<br/>' + bd.format("MM月dd日,hh:mm:ss")+'</td>';
            html += '<td>' + (item.sportType==1? '足球':'篮球')+(item.gameTimeType==1?'- 滚球':(item.gameTimeType==2?'- 今日':'- 早盘'))
            html +'<br/>'+ (item.mix == 2?'混合过关':(item.typeNames?item.typeNames.replace("-", "<br>"):''))+'</td>';
            if(item.mix != 2){
                html +='<td>'+ toBetHtml(JSON.decode(item.remark), item)+'</td>';
            }else{
                var temp = "";
                var arr = JSON.decode(item.remark)
                for (var i = 0; i < arr.length; i++) {
                    if (i != 0) {
                        temp += "<div style='border-bottom:1px #303030 dotted;'></div>";
                    }
                    temp += toBetHtml(arr[i], item);
                }
                html += '<td>'+temp+'</td>';
            }
            html += '<td>'+ item.bettingMoney+'</td>';
            switch(item.balance){
                case 1:
                    html +="<td><font color='blue'>未结算</font></td>";
                    break;
                case 2:
                    html +="<td><font color='green'>已结算</font></td>";
                    break;
                case 3:
                    html +="<td><font color='red'>结算失败</font></td>";
                    break;
                case 4:
                    html +="<td><font color='red'>比赛腰斩</font></td>";
                    break;
                case 5:
                case 6:
                    html +="<td><font color='green'>已结算</font></td>";
                    break;
            }
            switch(item.bettingStatus){
                case 1:
                    html +="<td><font color='blue'>待确认</font></td>";
                    break;
                case 2:
                    html +="<td>已确认</td>";
                    break;
                case 3:
                    html +="<td><font color='red'>取消</font></td>";
                    break;
                case 4:
                    html +="<td><font color='red'>手动取消</font></td>";
                    break;
            }
            html += '<td>'+(item.bettingResult||0)+'</td></tr>';
        })
        $("#betResultOrder").html(html);
        $("#orderBetAmount").html(res.aggsData.totalBetMoney||0);
        $("#orderWinAmount").html(res.aggsData.totalBetResult||0);
        $("#orderBetOrderTblFoot").show();
        $(".Records_listCont").html(html)
    }
    function toBetHtml(item, row) {
        var con = item.con;
        if (con.indexOf("vs") == -1) {
            con = '<span class="text-danger">' + con + '</span>';
        }
        var homeFirst = !(item.homeStrong === false);//主队是否在前
        var scoreStr = "";

        if (row.gameTimeType == 1) {
            if (homeFirst) {
                scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH + ":"
                    + row.scoreC + ")</b></font>";
            } else {
                scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC + ":"
                    + row.scoreH + ")</b></font>";
            }
        }
        var home = item.firstTeam;
        var guest = item.lastTeam;
        if (item.half === true && row.mix == 2) {
            home = home + "<font color='gray'>[上半]</font>";
            guest = guest + "<font color='gray'>[上半]</font>";
        }

        var html = item.league + "<br/>" + home + "&nbsp;" + con + "&nbsp;"
            + guest + scoreStr + "<br/>" + "<font color='red'>"
            + item.result + "</font>&nbsp;" + "@"
            + "&nbsp;<font color='red'>" + item.odds + "</font>";
        var balance = row.mix != 2 ? row.balance : item.balance;
        if (balance == 4) {
            html = "<s style='color:red;'>" + html + "</s>"
        } else if (balance == 2 || balance == 5 || balance == 6) {
            var mr = row.mix != 2 ? row.result : item.matchResult;
            if (homeFirst) {
                html = html + "&nbsp;<font color='blue'>(" + mr + ")</font>";
            } else {
                var ss = mr.split(":");
                html = html + "&nbsp;<font color='blue'>(" + ss[1] + ":"
                    + ss[0] + ")</font>";
            }
        }
        return html;
    }
</script>
<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
        </html>
