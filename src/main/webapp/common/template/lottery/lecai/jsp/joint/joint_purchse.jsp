<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>合买中心-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/joint.css" type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/jquery-browser.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="main" id="main">
		<jsp:include page="../include/index_nav.jsp"></jsp:include>
		<input type="hidden" value="2" id="navId" />
		<div class="jclq-draw-main">
			<div class="jclq-draw-bar">
				<span class="title">合买历史记录</span><input type="hidden" value="${isLogin eq false?1:2}" id="isLogin" />
					<ul class="bar-ul">
						<li><select name="code" style="width: 100px;" id="lotCode">
							<option value="" ${param.lc eq null?'selected':''}>所有彩种</option>
							<c:forEach items="${lotList}" var="lot">
								<option value="${lot.code}"
									<c:if test="${param.lc eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
							</c:forEach>
						</select></li>
						<li>
							<select name="type" style="width:100px" id="type">
								<option value="" ${param.type eq null?'selected':''}>所有状态</option>
								<option value="1" ${param.type eq 1?'selected':''}>未满员</option>
								<option value="2" ${param.type eq 2?'selected':''}>已满员</option>
								<option value="6" ${param.type eq 6?'selected':''}>已撤单</option>
							</select>
						</li>
						<li><input name="startTime" id="startTime" class=" text75" readonly="readonly" value="${startTime }" type="text" data-end-date="${startTime}">至 
							<input name="endTime" id="endTime" class=" text75" readonly="readonly" value="${endTime }" type="text" data-end-date="${endTime}">
						</li>
						<li><input type="submit" class="search-btn" value="查询" onclick="search();"></li>
						<!-- <li><a href="javascript://">我要投注</a></li> -->
					</ul>
				<div class="clear"></div>
			</div>
			<div class="jclq-draw-tab-border">
				<table cellpadding="0" cellspacing="0" class="jclq-draw-tab" id="jq_match_list">
					<colgroup>
                                    <col width="15%">
                                    <col width="10%">
                                    <col width="8%">
                                    <col width="10%">
                                    <col width="5%">
                                    <col width="8%">
                                    <col width="6%">
                                    <col width="15%">
                                    <col width="9%">
                                    <col width="7%">
                                    <col width="7%">
                                </colgroup>
					<thead>
						<tr>
							<td>方案编号</td>
							<td>发起人</td>
							<td>战绩</td>
							<td>方案金额</td>
							<td>提成</td>
							<td>每份金额</td>
							<td>方案详情</td>
							<td>进度</td>
							<td>剩余份数</td>
							<td>认购份数</td>
							<td>操作</td>
						</tr>
					</thead>
					<tbody>
						<c:if test="${fn:length(page.list) > 0}">
						<c:forEach items="${page.list}" var="joint" varStatus="j">
						<tr <c:if test="${j.index%2!=0}">class="bgcolor"</c:if>>
							<td>${joint.programId}</td>
							<td>${fn:substring(joint.account,0,2)}***</td>
							<td>0</td>
							<td>${joint.totalMoney}${not empty cashName?cashName:'元'}</td>
							<td>${joint.commission}%</td>
							<td>${joint.oneMoney}${not empty cashName?cashName:'元'}</td>
							<td><a href="${base}/lotteryV3/joint/detail.do?programId=${joint.id}&lotCode=${joint.lotCode}">查看</a></td>
							<td style="text-align:left;padding-left:5px;">
								<span style="margin-left:0px;;color:000"><fmt:formatNumber value="${(joint.surNum/joint.branchNum*100)>100?1:joint.surNum/joint.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/></span><c:if test="${joint.them ne 0}"> + <span style="margin-left:0px;color:#fff;background:#b90000;"><fmt:formatNumber value="${joint.them/joint.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/>保</span></c:if>
								<br>
								<div style="width:120px;margin-top:3px;height:6px; font-size:0px; line-height:0px;border:1px solid #b90000;background:#fff;">
									<img src="${base}/common/template/lottery/lecai/images/joint/bar_space.gif" width="<fmt:formatNumber value="${(joint.surNum/joint.branchNum*100)>100?1:joint.surNum/joint.branchNum}" type="percent" maxFractionDigits="2" minFractionDigits="2"/>" height="6">
								</div>
							</td>
							<td>${joint.branchNum-joint.surNum<0?0:joint.branchNum-joint.surNum}份</td>
							<td>
								<c:choose>
									<c:when test="${joint.branchNum-joint.surNum == 0}">
										--
									</c:when>
									<c:when test="${joint.type eq 1}">
										<input class="jointNum" type="text" sur_num="${joint.branchNum-joint.surNum}" m="${joint.oneMoney}" bfl="<fmt:formatNumber value="${1/joint.branchNum}" type="percent" maxFractionDigits="3" minFractionDigits="3"/>" value="1">
									</c:when>
									<c:otherwise>--
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${joint.branchNum-joint.surNum == 0}">满员</c:when>
									<c:when test="${joint.type eq 1}">
										<div class="show-details" pro_id="${joint.id}" p_id="${joint.programId}">购买</div>
									</c:when>
									<c:otherwise>${joint.type eq 2?'满员':joint.type eq 3?'<strong class="red">截止</strong>':joint.type eq 4?'<strong style="color:#090">完成</strong>':joint.type eq 5?'<strong class="red">失效</strong>':'<strong class="red">撤单</strong>'}</c:otherwise>
								</c:choose>
							</td>
						</tr>
						</c:forEach>
						</c:if>
						<c:if test="${fn:length(page.list) == 0}">
							<tr>
								<td colspan="11"><img src="${base}/common/template/lottery/lecai/images/joint/T1LAbBXlhnXXaMli7Z-341-110.jpg"></td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<c:if test="${fn:length(page.list) > 0}">
				<div class="page">
				<c:choose>
					<c:when test="${page.totalPageCount == 1}"><!-- 总页数为1 -->
						<strong>页数：<font color="red">${page.currentPageNo}</font>/${page.totalPageCount}页&nbsp;记录：${page.totalCount}条</strong>
					</c:when>
					<c:otherwise>
							<c:if test="${page.currentPageNo != 1}">
								<a href="javascript://" class="next" onclick="goPage(1);">首页</a>
								<a href="javascript://" class="next" onclick="goPage(${page.currentPageNo-1});">上一页</a>
							</c:if>
							<c:set value="${page.totalPageCount<=5?1:page.totalPageCount-page.currentPageNo<=4?page.totalPageCount-4:page.currentPageNo}" var="begin"></c:set>
							<c:set value="${page.totalPageCount<=5 || page.totalPageCount-page.currentPageNo<=4?page.totalPageCount:page.currentPageNo+4}" var="end"></c:set>
							<c:forEach var="pageNo" begin="${begin}" end="${end}">
							<c:choose>
								<c:when test="${page.currentPageNo == pageNo}">
									<strong>${pageNo}</strong>
								</c:when>
								<c:when test="${pageNo <= page.totalPageCount}">
									<a href="javascript://" class="next" onclick="goPage(${pageNo});">${pageNo}</a>
								</c:when>
								</c:choose>
							</c:forEach>
							<c:if test="${page.currentPageNo != page.totalPageCount}">
								<a href="javascript://" class="next"  onclick="goPage(${page.currentPageNo+1});">下一页</a>
								<a href="javascript://" class="next"  onclick="goPage(${page.totalPageCount});">末页</a>
							</c:if>
						<strong>页数：<font color="red">${page.currentPageNo}</font>/${page.totalPageCount}页&nbsp;记录：${page.totalCount}条</strong>
					</c:otherwise>
				</c:choose>
				</div>
				</c:if>
				<script type="text/javascript">
					var iL = $("#isLogin").val();
					$(function(){
						$(".bar-ul select").chosen({
							disable_search : true
						});
						$('#endTime,#startTime').cxCalendar({
							format : 'YYYY-MM-DD'
						});
					})
					
					function goPage(pageNo){
						window.location.href=urlasay(pageNo);
					}
					function search(){
						window.location.href= urlasay();
					}
					function urlasay(pageNo){
						var lc = $("#lotCode").val(),type=$("#type").val(),ds=$("#startTime").val(),de=$("#endTime").val(),url="${base}/lotteryV3/joint.do?ds="+ds+"&de="+de;
						if(lc){url+="&lc="+lc;}
						if(type){url+="&type="+type;}
						if(pageNo){url+="&page="+pageNo;}
						return url;
					}
				</script>
				<div class="clear"></div>
			</div>
		</div>

	</div>
<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>