<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<strong class="p1" id="jj">简介：</strong>
<c:if test="${param.code == 'GD11X5'}">
<p class="p1">
	广东11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。 11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">广东11选5</td>
			<td bgcolor="#FFFFFF">9:30—23:10(北京时间)</td>
			<td bgcolor="#FFFFFF">01-42</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'JX11X5'}">
<p class="p1">
	江西11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。 11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">江西11选5</td>
			<td bgcolor="#FFFFFF">9:30—23:10(北京时间)</td>
			<td bgcolor="#FFFFFF">01-42</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'SD11X5'}">
<p class="p1">
	山东11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。 11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">山东11选5</td>
			<td bgcolor="#FFFFFF">9:01—23:01(北京时间)</td>
			<td bgcolor="#FFFFFF">01-43</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
	</table>
</c:if>
<c:if test="${param.code == 'SH11X5'}">
<p class="p1">
	上海11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。 11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">上海11选5</td>
			<td bgcolor="#FFFFFF">9:20—00:00(北京时间)</td>
			<td bgcolor="#FFFFFF">01-45</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
	</table>
</c:if>
<c:if test="${param.code == 'GX11X5'}">
<p class="p1">
	广西11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。 11选5投注区号码范围为01〜11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">广西11选5</td>
			<td bgcolor="#FFFFFF">9:20—00:00(北京时间)</td>
			<td bgcolor="#FFFFFF">01-45</td>
			<td bgcolor="#FFFFFF">每20分钟</td>
		</tr>
	</tbody>
	</table>
</c:if>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zx_fs">前三复式 &nbsp; &nbsp;</strong>
<p class="p1">从万位、千位、百位中至少各选择1个号码组成一注。 &nbsp;</p>
<p class="p1">从万位、千位、百位至少个选1个号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：03 04 05</p>
<p class="p1">开奖号码:前三位03 04 05，即中前三复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zx">前三组选</strong>
<p class="p1">从01-11中任意选择3个或3个以上号码组成一注。&nbsp;</p>
<p class="p1">
	从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的万位、千位、百位相同，顺序不限，即为中奖。</p>
<p class="p1">投注方案：02 05 08</p>
<p class="p1">开奖号码:前三位08 05 02（顺序不限），即中前三组选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="z3zx_fs">中三复式</strong>
<p class="p1">从千位、百位、十位中至少各选择1个号码组成一注。&nbsp;</p>
<p class="p1">从千位、百位、十位至少个选1个号码组成一注，所选号码与开奖号码中3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：03 04 05</p>
<p class="p1">开奖号码:中三位03 04 05，即中中三复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="z3zx">中三组选</strong>
<p class="p1">从01-11中任意选择3个或3个以上号码组成一注。&nbsp;</p>
<p class="p1">
	从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的千位、百位、十位相同，顺序不限，即为中奖。</p>
<p class="p1">投注方案：02 05 08</p>
<p class="p1">开奖号码:中三位08 05 02（顺序不限），即中中三组选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zx_fs">后三复式</strong>
<p class="p1">从百位、十位、个位中至少各选择1个号码组成一注。&nbsp;</p>
<p class="p1">从百位、十位、个位至少个选1个号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案： 03 04 05</p>
<p class="p1">开奖号码:后三位03 04 05，即中后三直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zx">后三组选</strong>
<p class="p1">从01-11中任意选择3个或3个以上号码组成一注。&nbsp;</p>
<p class="p1">
	从0-11中任意选择3个或3个以上号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。</p>
<p class="p1">投注方案：02 05 08</p>
<p class="p1">开奖号码:后三位08 05 02（顺序不限），即中后三组选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx_fs">前二复式</strong>
<p class="p1">从万位、千位中至少各选择1个号码组成一注。&nbsp;</p>
<p class="p1">从万位、千位至少个选1个号码组成一注，所选号码与开奖号码前2位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：05 08</p>
<p class="p1">开奖号码:前二位05 08，即中前二复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx">前二组选</strong>
<p class="p1">从01-11中任意选择2个或2个以上号码组成一注。&nbsp;</p>
<p class="p1">从0-11中任意选择2个或2个以上号码组成一注，所选号码与开奖号码的万位、千位相同，顺序不限，即为中奖。</p>
<p class="p1">投注方案：05 08</p>
<p class="p1">开奖号码:前二位08 05 （顺序不限），即中前二组选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h2zx_fs">后二复式</strong>
<p class="p1">从十位、个位中至少各选择1个号码组成一注。&nbsp;</p>
<p class="p1">从十位、个位至少个选1个号码组成一注，所选号码与开奖号码前2位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：05 08</p>
<p class="p1">开奖号码:后二位05 08，即中后二直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h2zx">后二组选</strong>
<p class="p1">从01-11中任意选择2个或2个以上号码组成一注。&nbsp;</p>
<p class="p1">从0-11中任意选择2个或2个以上号码组成一注，所选号码与开奖号码的十位、个位相同，顺序不限，即为中奖。</p>
<p class="p1">投注方案：05 08</p>
<p class="p1">开奖号码:后二位08 05 （顺序不限），即中后二组选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_q3">前三</strong>
<p class="p1">从01-11中任意选择1个以上号码。&nbsp;</p>
<p class="p1">从0-11中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：1</p>
<p class="p1">开奖号码:前三位至少出现1个1，即中前三一码不定位。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_z3">中三</strong>
<p class="p1">从01-11中任意选择1个以上号码。&nbsp;</p>
<p class="p1">从0-11中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。</p>
<p class="p1">投注方案:1</p>
<p class="p1">开奖号码:中三位至少出现1个1，即中中三一码不定位。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_h3">后三</strong>
<p class="p1">从01-11中任意选择1个以上号码。&nbsp;</p>
<p class="p1">从0-11中选择1个号码，每注由1个号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：1</p>
<p class="p1">开奖号码:后三位至少出现1个1，即中后三一码不定位。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dwd">定位胆</strong>
<p class="p1">从万位、千位、百位、十位、个任意位置上任意选择1个或1个以上号码。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
<p class="p1">投注方案：万位1</p>
<p class="p1">开奖号码:万位1，即中定位胆万位。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx1z1">任一中一</strong>
<p class="p1">从01-11中任意选择1个或1个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任一中一。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx2z2">任二中二</strong>
<p class="p1">从01-11中任意选择2个或2个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05 04</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任二中二。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx3z3">任三中三</strong>
<p class="p1">从01-11中任意选择3个或3个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05 04 11</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任三中三。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx4z4">任四中四</strong>
<p class="p1">从01-11中任意选择4个或4个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案： 05 04 08 03</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任四中四。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx5z5">任五中五</strong>
<p class="p1">从01-11中任意选择5个或5个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05 04 11 03 08</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任五中五。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx6z5">任六中五</strong>
<p class="p1">从01-11中任意选择6个或6个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05 10 04 11 03 08</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任六中五。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx7z5">任七中五</strong>
<p class="p1">从01-11中任意选择7个或7个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案： 05 10 04 11 03 08 09</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任七中五。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxfs_rx8z5">任八中五</strong>
<p class="p1">从01-11中任意选择8个或8个以上号码。&nbsp;</p>
<p class="p1">从01-11共11号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：05 10 04 11 03 08 09 01</p>
<p class="p1">开奖号码:08 04 11 05 03，即中任八中五。</p>
<p>
	<br />
</p>