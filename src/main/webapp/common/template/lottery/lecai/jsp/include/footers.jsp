<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/footer.css?v="
	type="text/css">
<c:if test="${isLogin eq false}">
	<jsp:include page="../include/dialog_msg.jsp"></jsp:include>
</c:if>
<div class="foot_box">
	<div class="about_box">
		<div class="about_left">
			<p class="about_link">
				<a target="_blank" href="${base}/lotteryV3/help.do?code=1">关于我们</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=2">取款帮助</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=3">存款帮助</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=4">联盟方案</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=5">联盟协议</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=6">联系我们</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=7">常见问题</a>
			</p>
			<p>
				<img
					src="${base}/common/template/lottery/lecai/images/footer/wljc.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/wangan.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/wsjy.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/xylh.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/kxwz.gif">
			</p>
		</div>
		<div class="about_right">
			<p>
				${copyright}
			</p>
			<p>
				${_title} (Galaxy International) Online Game. All rights reserved.
			</p>
			<p>提醒：购买彩票有风险，在线投注需谨慎，不向未满18周岁的青少年出售彩票！</p>
		</div>
		<div class="clear"></div>
	</div>

</div>