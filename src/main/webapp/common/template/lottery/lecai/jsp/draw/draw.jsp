<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>开奖公告-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/draw.css"  type="text/css">
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<input type="hidden" value="3" id="navId" />
	<div class="main" style="position: relative;">
		<ul class="cz_nav">
			<c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}"><li class="on"><a href="#">全部彩种</a></li></c:if>
		</ul>
		<div class="clear"></div>
		<div class="cz_menu_top"></div>
		<div class="cz_menu">
			<div class="menu_left">
				<c:forEach items="${lotList}" var="drawLot" varStatus="i">
					<c:choose>
						<c:when test="${i.last}"><a href="${base}/lotteryV3/draw/list.do?lotCode=${drawLot.code}">${drawLot.name}</a></c:when>
						<c:when test="${i.index==6}"><a href="${base}/lotteryV3/draw/list.do?lotCode=${drawLot.code}">${drawLot.name}</a><br></c:when>
						<c:otherwise><a href="${base}/lotteryV3/draw/list.do?lotCode=${drawLot.code}">${drawLot.name}</a>|</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
			<div class="clear"></div>
		</div>
		<div class="kj_box">
			<h1><c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'on'}">最新${lotName}开奖结果</c:if><c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}">最新彩票开奖结果</c:if></h1>
			<table class="kj_tab" cellspacing="0" cellpadding="0">
				<tbody>
					<tr class="title">
						<td width="88">种类</td>
						<td width="100">期号</td>
						<td width="130">开奖时间</td>
						<td width="330">开奖号码</td>
						<td width="80">期数/每天</td>
						<td width="70">开奖频率</td>
						<td width="40">详情</td>
						<td width="40">走势</td>
						<td width="40">历史</td>
						<td width="80"><c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'on'}">购买${lotName}</c:if><c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}">购买彩票</c:if></td>
					</tr>
					<c:forEach items="${openList}" varStatus="i" var="list">
					<tr class="<c:if test="${i.index%2==0}">bgcolor</c:if>">
						<td><a href="${base}/lotteryV3/draw/list.do?lotCode=${list.lotCode}">${list.lotName}</a></td>
						<td>${list.qiHao}</td>
						<td>${list.date}</td>
						<td>
							<div class="ballbg">
								<c:set value="0" var="pceggSum"></c:set>
								<c:set value="${ fn:split(list.haoMa, ',') }" var="arr" />
								<c:forEach items="${arr}" var="haoMa" varStatus="is">
									<c:choose>
										<c:when test="${list.lotCode eq 'LHC' || list.lotCode eq 'SFLHC'|| list.lotCode eq 'TMLHC' || list.lotCode eq 'WFLHC' || list.lotCode eq 'HKMHLHC'}">
											<c:choose>
												<c:when test="${haoMa == '?'}">
														<c:if test="${is.index == 6}"><b>+</b>&nbsp;<span class="ball_2">${haoMa}</span></c:if>
														<c:if test="${is.index != 6 }"><span class="ball_1">${haoMa}</span></c:if>
												</c:when>
												<c:otherwise>
													<c:set var="hong" value="01,02,07,08,12,13,18,19,23,24,29,30,34,35,40,45,46"></c:set>
													<c:set var="lan" value="03,04,09,10,14,15,20,25,26,31,36,37,41,42,47,48"></c:set>
													<c:set var="lv" value="05,06,11,16,17,21,22,27,28,32,33,38,39,43,44,49"></c:set>
													<c:if test="${is.index == 6 }"><b>+</b></c:if>
													<c:if test="${fn:contains(hong, haoMa)}"><span class="ball_1">${haoMa}</span></c:if>
													<c:if test="${fn:contains(lan, haoMa)}"><span class="ball_2">${haoMa}</span></c:if>
													<c:if test="${fn:contains(lv, haoMa)}"><span class="ball_4">${haoMa}</span></c:if>
												</c:otherwise>
											</c:choose>
											<!-- <font style="display:inline-block;width:20px;height:20px;color:#000">猪</font> -->
										</c:when>
										<c:when test="${list.lotCode eq 'PCEGG'}">
											<c:choose>
												<c:when test="${haoMa eq '?'}">
													<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
													<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">?</span></c:if>
												</c:when>
												<c:otherwise>
													<c:set value="${pceggSum + haoMa}" var="pceggSum"></c:set>
													<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
													<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">${pceggSum}</span></c:if>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${list.lotCode eq 'BJSC' || list.lotCode eq 'XYFT' || list.lotCode eq 'LXYFT' || list.lotCode eq 'AZXY10'}">
											<c:if test="${haoMa eq '?'}"><span class="ball_1">${haoMa}</span></c:if>
											<c:if test="${haoMa ne '?'}"><span class="pk10 b${haoMa}"></span></c:if>
										</c:when>
										<c:otherwise>
											<span class="ball_1">${haoMa}</span>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</td>
						<td>${list.oneQiShu}</td>
						<td>${list.pinLv}</td>
						<td><a href="${base}/lotteryV3/draw/list.do?lotCode=${list.lotCode}"><img
								src="${base}/common/template/lottery/lecai/images/draw/details.gif"></a></td>
						<td><a href="${base}/lotteryV3/trend.do?lotCode=${list.lotCode}"><img
								src="${base}/common/template/lottery/lecai/images/draw/zst_01.gif"></a></td>
						<td><a href="${base}/lotteryV3/draw/list.do?lotCode=${list.lotCode}"><img
								src="${base}/common/template/lottery/lecai/images/draw/history.gif"></a></td>
						<td class="b"><a href="javascript:void(0);" onclick="__Go('${base}/lotteryV3/lotDetail.do?lotCode=${list.lotCode}')"
							class="draw-lottery-btn">投注</a> 
							<c:if test="${not empty duLiCaiPiao && duLiCaiPiao eq 'off'}">
							<c:if test="${!isJoint}"><a href="#">合买</a></c:if></c:if>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="kj_box_bottom"></div>
		<script type="text/javascript">
			function __Go(url){
				window.location.href=url;
			}
		</script>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>