<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/laypage-v1.3/laypage/laypage.js"></script>
<style type="text/css">
#pageGro {
    height: 25px;
    width: 100%;
}
#pageGro div {
    color: #999;
    float: left;
    font-size: 12px;
    line-height: 23px;
    margin-left: 10px;
}
#pageGro font {
    color: #c9171e;
    display: inline-block;
    font-weight: bold;
    line-height: 28px;
    padding: 0 5px;
}
.pageLoad a{
	color: #666 !important;
}
</style>
<div id="pageGro" class="cb" style="display:none;">
    <input type="hidden" value="" id="pageNumber">
	<div class="allTotal">共<font id="totalCount"></font>条&nbsp;&nbsp;&nbsp;当前第<font id="currentPageNo"></font>页</div>
	<div class="pageLoad"></div>
</div>

<script type="text/javascript">
function layerPage(totalCount,currentPageNo,totalPageCount) {
	totalCount = parseInt(totalCount);
	currentPageNo = parseInt(currentPageNo);
	totalPageCount = parseInt(totalPageCount);
	if (totalCount > 0) {
		$("#pageGro").show();
	} else {
		$("#pageGro").hide();
		return;
	}
	$("#totalCount").text(totalCount);
	$("#currentPageNo").text(currentPageNo);
	$("#pageNumber").val(currentPageNo);
	//分页
	laypage({
		cont : $('.pageLoad'),
		pages : totalPageCount, //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
		skin : '#777f89', //皮肤
		curr : function() { //通过url获取当前页，也可以同上（pages）方式获取
			var page = currentPageNo;
			return page;
		}(),
		jump : function(e, first) { //触发分页后的回调
			if (!first) { //一定要加此判断，否则初始时会无限刷新
				$("#pageNumber").val(e.curr);
				initRdsData(curNavFlag);
			}
		}
	});
}

function layerPages(totalCount,currentPageNo,totalPageCount){
	var count = parseInt(totalCount);
	currentPageNo = parseInt(currentPageNo);
	totalPageCount = parseInt(totalPageCount);
	if (count > 0) {
		$("#pageGro").show();
	} else {
		$("#pageGro").hide();
		return;
	}
	$("#totalCount").text(totalCount);
	$("#currentPageNo").text(currentPageNo);
	$("#pageNumber").val(currentPageNo);
	//分页
	laypage({
		cont : $('.pageLoad'),
		pages : totalPageCount, //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
		skin : '#777f89', //皮肤
		curr : function() { //通过url获取当前页，也可以同上（pages）方式获取
			var page = currentPageNo;
			return page;
		}(),
		jump : function(e, first) { //触发分页后的回调
			if (!first) { //一定要加此判断，否则初始时会无限刷新
				$("#pageNumberInput").val(e.curr);
				search();
			}
		}
	});
}

function layerPageLotteryDetail(totalCount,currentPageNo,totalPageCount){
	var count = parseInt(totalCount);
	currentPageNo = parseInt(currentPageNo);
	totalPageCount = parseInt(totalPageCount);
	if (count > 0) {
		$("#pageGro").show();
	} else {
		$("#pageGro").hide();
		return;
	}
	$("#totalCount").text(totalCount);
	$("#currentPageNo").text(currentPageNo);
	$("#pageNumber").val(currentPageNo);
	//分页
	laypage({
		cont : $('.pageLoad'),
		pages : totalPageCount, //可以叫服务端把总页数放在某一个隐藏域，再获取。假设我们获取到的是18
		skin : '#777f89', //皮肤
		curr : function() { //通过url获取当前页，也可以同上（pages）方式获取
			var page = currentPageNo;
			return page;
		}(),
		jump : function(e, first) { //触发分页后的回调
			if (!first) { //一定要加此判断，否则初始时会无限刷新
				$("#pageNumber").val(e.curr);
				getBcLotteryOrder(e.curr);
			}
		}
	});
}
</script>