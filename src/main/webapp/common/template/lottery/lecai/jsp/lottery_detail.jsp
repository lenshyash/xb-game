<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/common.css?v=${caipiao_version}"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/jquery-browser.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/lecai.js?v=${caipiao_version}"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
</head>
<body>
	<jsp:include page="include/header.jsp"></jsp:include>
	<div class="gcmain" id="lecaiMain">
		<input type="hidden" value="1" id="navId" />
		<div class="-top">
			<jsp:include page="include/lottery_nav.jsp"></jsp:include>
			<!--奖期基本信息结束-->
			<c:if test="${bly.code eq 'LHC'}">
				<jsp:include page="lottery/lhc.jsp"></jsp:include>
			</c:if>
			<c:if test="${bly.code ne 'LHC'}">
				<jsp:include page="lottery/other.jsp"></jsp:include>
			</c:if>
		</div>
	</div>
	<!-- 选中 -->
<audio id="sound_bet_select" src="${base}/common/template/lottery/lecai/js/mp3/bet_select.mp3" controls="controls" hidden="true"></audio>
<!-- 倒计时 -->
<audio id="sound_ding" src="${base}/common/template/lottery/lecai/js/mp3/ding.mp3" controls="controls" hidden="true"></audio>
<!-- 新开奖 -->
<audio id="sound_newkj" src="${base}/common/template/lottery/lecai/js/mp3/newkj.mp3" controls="controls" hidden="true"></audio>
	<jsp:include page="include/footers.jsp"></jsp:include>
</body>
</html>