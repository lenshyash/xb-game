<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
	<link rel="stylesheet"
	href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<input type="hidden" value="${isPaymentSort}" id="isPaymentSort">
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
	.online-tit, .fast-tit, .bank-tit{
		margin-left:0px;
	}
	.czmain .navbox{
		border-bottom:1px solid #4aa9db
	}
	.ucmain .infobox{
		border-bottom:2px solid #4aa9db
	}
	.czMain .navbox li a{
		height:27px;
	}
	.czMain .navbox li.on{
		height:33px;
		line-height: 33px;
		border-top:2px solid #4aa9db
	}
	.czMain .navbox li{
		height:30px;
		line-height: 33px;
		border-top:2px solid #4aa9db
	}
	.czMain .ws_bank_new li a{
		position: absolute!important;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
				<jsp:include page="member_left_nav.jsp"></jsp:include>

		<div class="userRight">
            <div class="userTitle mgb10">充值中心</div>
            <div class="userMain mgb10">
            <div class="wrap">
		<div class="grid fixed c21 main">
			<div class="ucmain" style="border:none;">
				<div class="infobox" style="position: relative; z-index: 9">
					<div class="userinfo">
						<strong>尊敬的用户：</strong><span class="user" style="color: #d20f00">${loginMember.account}</span><strong>，您好！您当前的账户余额为：</strong>
						<span class="gift-wallet"
							style="color: #d20f00; font-weight: bold">${loginMember.money}</span><strong>${not empty cashName?cashName:'元'}</strong>
					</div>
					<div class="menu">
						<span id="querenCZXX" style="display: none;">确认充值信息 | </span> <a
							href="${base}/center/banktrans/draw/cpDfPage.do" style="color:#e4393c;">提款</a> | <a href="${base}/center/record/hisrecord/cpDfPage.do" style="color:#e4393c;">账户明细</a>
					</div>

				</div>
				<c:if test="${isGuest }">
				<div class="userinfo-edit-main">试玩账号不能充值</div>
				</c:if>
				<c:if test="${!isGuest }">
				<div class="czmain czMain" id="chargerinit">
					 <div class="navbox" style="position: relative; z-index: 5;">
						<span class="info">选择充值方式：</span>
						<ul class="tab-list" style="height: 80px;float: none;padding-left:0;">
							<c:if test="${onlineFlag eq 'on'}">
								<li class="" data="online" id="desc1"
									desc="${not empty onlineDesc?onlineDesc:'及时自动到账，推荐'}"><a
									href="javascript:void(0);">在线支付</a></li>
							</c:if>
							<c:if test="${fastFlag eq 'on' }">
								<li class="" data="fast" id="desc2"
									desc="${not empty fastDesc?fastDesc:'支持微信/支付宝二维码扫描'}"><a
									href="javascript:void(0);">快速支付</a></li>
							</c:if>
							<c:if test="${bankFlag eq 'on' }">
								<li class="" data="bank" id="desc3"
									desc="${not empty bankDesc?bankDesc:'支持网银转账，ATM转账，银行柜台汇款' }"><a
									href="javascript:void(0);">公司入款</a></li>
							</c:if>
						</ul>
						<style>
							.czMain .navbox li{
								width:120px!important;
							}
						</style>
						<div class="clear"></div>
					</div>
					<div class="online-tit">
						<span></span><font id="depostDesc"></font>
					</div>
					<div class="wy-box" id="depostList">
					</div>
					<style>
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li{
							width: 180px;
							height: 59px;
							border: 1px solid #928080;
							border-radius: 12px;
						}
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li:hover{
							border: 1px solid #2798d4;
							border-radius: 12px;
						}
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li a{
							height: 50px;
							width: 180px;
							margin-top: 6px!important;
							margin-left: 6px;

						}
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li:hover{
							border-radius: 12px;
						}
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li a img:hover{
							 border: 1px solid #ccc;
							 border-radius: 5px;

						 }
						#depostList .ws_bank_new.clearfix.ws_bank_new_more li a span.icons:hover{
							border: 1px solid #ccc;
							border-radius: 5px;
						}
						.cz-platform-box .ws_bank_new li a:hover, .wy-box .ws_bank_new li a:hover, .no-wy-box .ws_bank_new li a:hover, .navbox .ws_bank_new li a:hover{
							border: none;
						}
						.czMain .ws_bank_new li .hot{
							border: none;
						}
						.czMain .ws_bank_new li .hot:hover{
							border:none !important;
						}
					</style>
				</div>
					<script>
						if($("#isPaymentSort").val() == ''){
                            $("#isPaymentSort").val('1,2,3')
                        }
                        var descLength = $(".tab-list li").length
                        var seqString = $("#isPaymentSort").val().split(',')
                        var marLeft = 0, descId = '' , oneClick = true;
                        if(seqString.length > 1){
                            $(".czMain .navbox span.info").css('margin-bottom','7px')
                            $(".czMain .navbox li").css("position","absolute")
							$(".tab-list").css('height','0px')
                            for(var i = 0; i < seqString.length; i++){
                                for(var j = 0; j < descLength; j++){
                                    if($(".tab-list li")[j].id.indexOf(seqString[i]) != -1){
                                        descId = $(".tab-list li")[j].id
										if(oneClick){
                                            $("#"+descId+"").click()
                                            oneClick = false
										}
                                        $("#"+descId+"").css("margin-left",marLeft+"px")
                                        marLeft += 120
                                    }
                                }
                            }
                        }
					</script>
				</c:if>
			</div>
		</div>
	</div>
            </div>
        </div>
		</div>
		<c:if test="${!isGuest }"><jsp:include page="templateScript/deposit.jsp"></jsp:include>
		<script type="text/javascript" src="${base }/common/js/onlinepay/pay.js?v=6.2673"></script>
		<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
		<script type="text/javascript">
		var pays = [];
		var baseUrl = '${base}';
		var hostUrl1="${hostUrl1}";
		var showPayInfo=${showPayInfo};
		$(function(){
			init();
		})

		function init(){
			if(!$(".tab-list li").hasClass("on")){
				var seqString = $("#isPaymentSort").val().split(',');
				var index = 0;
				if(seqString && seqString.length > 0){
					index = seqString[0]-1;
				}
				var t = $(".tab-list li").eq(index);
				t.addClass("on");
			}else{
				var t = $(".tab-list li.on");
			}
			var data=t.attr('data'),url = null;
			switch(data){
			case "online":
				url = '${base}/center/banktrans/deposit/dptinita.do';
				Base.onlines.initOnlines(url);
				break;
			case "fast":
				url = '${base}/center/banktrans/deposit/dptinitb.do';
				Base.fasts.initFasts(url);
				break;
			case "bank":
				url = '${base}/center/banktrans/deposit/dptinitc.do';
				Base.banks.initBanks(url);
				break;
			default:
				layer.msg("系统异常",{icon:2});
				break;
			}
			$("#depostDesc").html(t.attr('desc')).parent().attr('class',''+data+'-tit');
		}

		var tabli = $(".tab-list li");
		tabli.on("click",function(){
			$(this).addClass("on").siblings().removeClass("on");
			init();
		});

		function goTo(){
			window.location.href="${base}/center/record/hisrecord/cp3Page.do";
		}
		</script></c:if>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>