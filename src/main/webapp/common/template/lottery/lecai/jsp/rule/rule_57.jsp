<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<strong class="p1" id="jj">简介：</strong>
<c:if test="${param.code == 'PCEGG'}">
<p>
	<span>PC蛋蛋（幸运28）开奖结果来源于国家福利彩票北京快乐8(官网)开奖号码，从早上9:05至23:55，每5分钟一期不停开奖。&nbsp;</span>
</p>
<p>
	<span>北京快乐8每期开奖共开出20个数字，PC蛋蛋将这20个开奖号码按照由小到大的顺序依次排列；取其1-6位开奖号码相加，和值的末位数作为PC蛋蛋开奖第一个数值；
		取其7-12位开奖号码相加，和值的末位数作为PC蛋蛋开奖第二个数值，取其13-18位开奖号码相加，和值的末位数作为PC蛋蛋开奖第三个数值；三个数值相加即为PC蛋蛋最终的开奖结果。</span>
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">PC蛋蛋(幸运28)</td>
			<td bgcolor="#FFFFFF">9:00—23:55(北京时间)</td>
			<td bgcolor="#FFFFFF">001-179</td>
			<td bgcolor="#FFFFFF">每5分钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${param.code == 'JND28'}">
<ul>
	<li>加拿大28采用加拿大彩票公司BCLC的开奖结果为基础；</li>
	<li>【BCLC官网：lotto.bclc.com】。每三分半一期，开奖时间全天。(每天维护时间为19:00~21:30，周一可能会有延迟)</li>
	<li>将当期第三方开奖数据从小到大进行排序；</li>
	<li>排序后,分别按号码第一区[第2/5/8/11/14/17位数字] 8,17,34,42,58,69，第二区[第3/6/9/12/15/18位数字] 14,22,39,48,63,72，第三区[第4/7/10/13/16/19位数字] 16,26,41,54,64,73</li>
	<li>把每个区的数字分别相加；</li>
	<li>三个数值相加即为幸运28最终的开奖结果。</li>
</ul>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">加拿大28</td>
			<td bgcolor="#FFFFFF">全天24小时(北京时间)</td>
			<td bgcolor="#FFFFFF">由于维护时间不固定，所以期号不固定</td>
			<td bgcolor="#FFFFFF">每3分半钟</td>
		</tr>
	</tbody>
</table>
</c:if>
<p>
	<span><br /></span>
</p>
<strong id="dwd">
	<span>定位胆</span>
</strong>
<p>
	<span>从第一、第二、第三位号选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：第一位1开奖号码：第一位1，即中定位胆第一位。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="bdw">
	<span>不定位胆</span>
</strong>
<p>
	<span>选一个号码组成一注。</span>
</p>
<p>
	<span>从0-9中选择1个号码，每注由1号码组成，只要开奖号码的第一位、第二位、第三位中包含所选号码，即为中奖。</span>
</p>
<p>
	<span>投注方案：12开奖号码：至少出现1和2各一个，即中不定位胆。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="sxzx">
	<span>三星组选</span>
</strong>
<p>
	<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。注：开重复号码不算中奖。例：开奖号码161，投注号码126</span>
</p>
<p>
	<span>投注方案：2 6 9 开奖号码:2 6 9（顺序不限），即中三星组选。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong>
	<span id="sxfs">三星复式</span>
</strong>
<p>
	<span>从第一位、第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位、第三位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案： 345 &nbsp;开奖号码: 345，即中三星直选。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="q2zx">
	<span>前二组选</span>
</strong>
<p>
	<span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的第一位、第二位相同，顺序不限，即为中奖。</span>
</p>
<p>
	<span>投注方案：4 7 开奖号码:前二位4 7 （顺序不限），即中前二组选。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="q2zx_fs">
	<span>前二复式</span>
</strong>
<p>
	<span>从第一位、第二位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第一位、第二位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：58开奖号码：前二位58，即中前二复式</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="h2zx">
	<span>后二组选</span>
</strong>
<p>
	<span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的第二位、第三位相同，顺序不限，即为中奖。</span>
</p>
<p>
	<span>投注方案：4 7 开奖号码:后二位4 7 （顺序不限），即中后二组选。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="h2zx_fs">
	<span>后二复式</span>
</strong>
<p>
	<span>从第二位、第三位各选一个号码组成一注。</span>
</p>
<p>
	<span>从第二位、第三位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</span>
</p>
<p>
	<span>投注方案：58开奖号码：后二位58，即中后二复式。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="hz">
	<span>和值</span>
</strong>
<p>
	<span>从0-27中任意选择1个或1个以上号码。</span>
</p>
<p>
	<span>所选数值等于开奖号码相加之和，即为中奖。</span>
</p>
<p>
	<span>投注方案：和值1开奖号码：001或010或100，即中和值。</span>
</p>
<p>
	<span><br /></span>
</p>
<strong id="dxds">
	<span>大小单双</span>
</strong>
<p>
	<span>和值的数字为大小单双</span>
</p>
<p>
	<span>和值数字0-13为小，14-27为大，奇数为单，偶数为双</span>
</p>
<p>
	<span>所选大小单双与开奖和值相符，即为中奖</span>
</p>
<p>
	<br />
</p>