<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/footer.css?v="
	type="text/css">
<div class="jc-footer">
	<a style="display:none;" data-reveal-id="loginBtn" id="triggerLoginBtn" ></a>
	<div class="footer-cn js-lazy">
		<div class="cnLeft">
			<h4>手机彩票</h4>
			<div class="footer-code">
				<i class="code-icon"></i>
			</div>
		</div>
		<div class="cnRight">
			<div class="cnTop">
				<div class="cn-list">
					<h3>账户相关</h3>
					<ul>
						<li><a
							href="#"
							target="_blank" title="">如何注册账号</a></li>
						<li><a
							href="#"
							target="_blank" title="">怎么找回登录密码</a></li>
						<li><a
							href="#"
							target="_blank" title="">怎么找回支付密码</a></li>
						<li><a
							href="#"
							target="_blank" title="">如何修改手机号码</a></li>
					</ul>
				</div>
				<div class="cn-list">
					<h3>充值购彩</h3>
					<ul>
						<li><a href="#"
							target="_blank" title="">如何进行充值</a></li>
						<li><a
							href="#"
							target="_blank" title="">如何购买彩票</a></li>
						<li><a
							href="#"
							target="_blank" title="">如何查询购彩记录</a></li>
						<li><a
							href="#"
							target="_blank" title="">充值没到账怎么办</a></li>
					</ul>
				</div>
				<div class="cn-list">
					<h3>兑奖提款</h3>
					<ul>
						<li><a
							href="#"
							target="_blank" title="">怎样进行兑奖</a></li>
						<li><a
							href="#"
							target="_blank" title="">如何进行提款</a></li>
						<li><a
							href="#"
							target="_blank" title="">提款多久到账</a></li>
						<li><a
							href="#"
							target="_blank" title="">领奖是否收手续费</a></li>
					</ul>
				</div>
				<div class="cn-list service">
					<h3>在线客服</h3>
					<ul>
						<li>QQ咨询：<a
							href="#"
							target="_blank" title="">客服1</a><a
							href="#"
							target="_blank" title="">客服2</a><a
							href="#"
							target="_blank" title="">客服3</a></li>
						<li>客服电话：400-636-1818</li>
						<li>在线咨询时间：周一~周日09:30~17:30</li>
						<li>电话咨询时间：周一~周五09:30~17:30</li>
					</ul>
				</div>
			</div>
			<div class="cnBtn">
				<p>
					<span class="explain01 icon"></span>账户安全
				</p>
				<p>
					<span class="explain02 icon"></span>购彩便捷
				</p>
				<p>
					<span class="explain03 icon"></span>派奖快速
				</p>
				<p>
					<span class="explain04 icon"></span>提款快速
				</p>
			</div>
		</div>
	</div>
	<div class="clear nospace"></div>
	<div class="footerBox">
		<div class="about_box">
			<p class="about_link">
				<a target="_blank" href="${base}/lotteryV3/help.do?code=1">关于我们</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=2">取款帮助</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=3">存款帮助</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=4">联盟方案</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=5">联盟协议</a>| <a target="_blank" href="${base}/lotteryV3/help.do?code=6">联系我们</a>|
				<a target="_blank" href="${base}/lotteryV3/help.do?code=7">常见问题</a>
			</p>
			<p class="about_mt">${copyright}</p>
			<p class="about_mt remind">提醒：购买彩票有风险，在线投注需谨慎，不向未满18周岁的青少年出售彩票！</p>
			<p>
				<img src="${base}/common/template/lottery/lecai/images/footer/wljc.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/wangan.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/wsjy.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/xylh.gif"><img
					src="${base}/common/template/lottery/lecai/images/footer/kxwz.gif">
			</p>
		</div>
	</div>
</div>