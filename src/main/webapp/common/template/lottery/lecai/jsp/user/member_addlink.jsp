<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/common/js/layer/layer.js"></script>
<style type="text/css">
	table{
	    font-size: 15px;
	    width:100%;
	    border-collapse: inherit;
    	border-spacing: 0;
	}
	table tr td{text-align: center;line-height: 35px;}
	table input{
		width:260px;
		height:30px;
		padding-left:5px;
	};

</style>
<div style="overflow-x: hidden;overflow-y: hidden;padding:20px;font-size:13px;text-align: center;">
<table class="table table-bordered table-striped" style="clear: both">
		<tbody>
		<input id="zjId"  type="hidden" value="${link.id}"/>
		<input id="linkCode"  type="hidden" value="${link.linkKey}"/>
		<input id="linkType"  type="hidden" value="${link.type}"/>
			<tr>
				<td width="30%" class="text-right">推广用户类型：</td>
				<td width="55%" class="text-left">
				<select class="form-control" id="tgType" onmousedown="${tgType}" value="${link.type}" style="width: 260px;height:30px;padding-left:5px;">
					<option value="1" selected >代理</option>
					<c:if test="${trunOnoff ne 'on' }">
					<option value="2" >会员</option>
					</c:if>
				</select>
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">代理机制：</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" disabled="disabled" id="dljz" value="${dljz}"/>
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">生成链接：</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" disabled="disabled" id="urlLink" value="${link.linkUrl}"/>
				</td>
			</tr>
			
			<tr id="cpfds" >
				<td width="30%" class="text-right">为下级设置返点<br><span class="">(<span id="minRoll" style="color:#4aa9db">0</span>-<span id="getCpFds"style="color:#4aa9db"></span>)</span>：</td>
				<td width="55%" class="text-left">
				<input type="text" class="form-control" id="rollPoint" placeholder="请输入返点数!" value="${link.cpRolling}"/>
				<input id="minFd" type="hidden" value="0">
				<input id="maxFd" type="hidden" value="">
				</td>
			</tr>
			<tr>
				<td width="30%" class="text-right">状态：</td>
				<td width="55%" class="text-left">
				<select id="status" class="form-control" value="${link.status}"  style="width: 260px;height:30px;padding-left:5px;">
						<option value="1">禁用</option>
						<option value="2" selected="selected">启用</option>
				</select>
				</td> 
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		function loading(){
			$.ajax({
				url : "${base}/daili/generalizeLink/multidata.do",
				success : function(j) {
					var rebateMaxNum = j.rebateMaxNum;
					$('#maxFd').val(rebateMaxNum);
					$('#minRoll').html(j.rebateMinNum);
					$('#getCpFds').html(rebateMaxNum);
				}
			});
			$('#cpfds').show();
			if('${flag}'=='1'){
				$('#cpfds').show();
			}
			$("#tgType").change(function() {
				var curVal = $(this).find("option:checked").attr("value");
				$("#linkType").val(curVal);
				if (curVal == 1 || '${memberRate}' == 'on') {//代理
					$('#cpfds').show();
				} else {
					$('#cpfds').hide();
				}
			});
		}
	    function callbackdata() {
	    	console.log("${link.linkKey}");
	    	var zjId = $('#zjId').val();
			var type = $("#tgType").val();
			var linkCode = $('#linkCode').val();
			var rollPoint = $('#rollPoint').val();
			var status = $('#status').val();
			// 			console.log(zjId+"zjId  "+"type:"+type+"   linkCode:"+linkCode+"   rollPoint:"+rollPoint+"   status:"+status);
			if ("${isMulti}" == "true") {
				if (type == 1 || '${memberRate}'== 'on') {
					if (rollPoint == null || rollPoint == "") {
						layer.alert("彩票返点数为必填项!");
						return false;
					}
					var minFd = $('#minFd').val();
					var maxFd = $('#maxFd').val();
					if (rollPoint * 1 < minFd * 1) {
						layer.alert("彩票返点数不能小于" + minFd + "!");
						return false;
					}
					if (rollPoint * 1 > maxFd * 1) {
						layer.alert("彩票返点数不能大于" + maxFd + "!");
						return false;
					}
				}
			}
			var param = {};
			param["id"] = zjId;
			param["type"] = type;
			param["linkKey"] = linkCode;
			param["cpRolling"] = rollPoint;
			param["status"] = status;
			return param;
	    } 
</script>
</div>
