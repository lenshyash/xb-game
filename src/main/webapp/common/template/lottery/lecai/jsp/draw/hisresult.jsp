<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>${lotName}开奖信息-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/draw_detail.css"
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/js/chosen/chosen.min.css"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/draw.js"
	code="${lotCode}"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/chosen/chosen.jquery.min.js"></script>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param
			value="true" name="type" />
	</jsp:include>
	<div class="main">
		<div class="leftnav">
			<input type="hidden" value="3" id="navId" /> <input type="hidden"
				value="${curQiHao}" id="curQiHao" />
			<ul id="top_tab" class="changebox">
				<!-- <li class="on" id="top_sort"><a href="javascript://">按分类查看</a></li> -->
			</ul>
			<div class="cznav" id="nav_sort" style="display: block;">
				<ul class="item js-lottery-container">
					<c:forEach items="${lotList}" var="list" varStatus="l">
						<li <c:if test="${list.code eq lotCode}">class="tit"</c:if>>
							<h1>
								<a href="${base}/lotteryV3/draw/hisresult.do?lotCode=${list.code}"
									c="${list.code}">${list.name}</a>
							</h1>
						</li>
					</c:forEach>
				</ul>
			</div>
			<div class="lbot"></div>
		</div>
		<div class="right_box">
			<div class="right_top"></div>
			<div class="right_main">
				<div class="cpinfo">
					<div class="bet">
						<a target="_blank" href="${base}/lotteryV3/lotDetail.do?lotCode=${lotCode}" class="buy"
							title="立即投注"></a>
					</div>
					<div class="title title-high">
						<span class="red">${lotName}</span>历史开奖信息<span class="gray"></span>
					</div>
					<div class="search search-high">
						<c:choose>
							<c:when test="${lotCode eq 'FC3D' || lotCode eq 'PL3' || lotCode eq 'LHC'}">
								<select id="history_phase_list" style="width:80px;" onchange="changeDate(this);">
									<option value="2018" <c:if test="${year eq '2018'}">selected</c:if>>2018</option>
									<option value="2017" <c:if test="${year eq '2017'}">selected</c:if>>2017</option>
									<option value="2016" <c:if test="${year eq '2016'}">selected</c:if>>2016</option>
								</select>
								<p style="margin-top:0px;margin-left:5px;">年</p>
							</c:when>
							<c:otherwise>
								<p style="margin-top:0px;">
									<input style="margin-top:0px;" id="startTime" class="text text75" readonly="readonly" value="${startTime}" data-end-date="${time}" type="text" onchange="changeDate(this);">
								</p>
								<p style="margin-top:0px;">选择日期：</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="clear"></div>
				<div>
					<table id="draw_list">
						<thead>
							<tr>
								<td class="td1">开奖日期</td>
								<td class="td1">开奖时间</td>
								<td class="td2">期号</td>
								<td class="td3">开奖号码</td>
								<td class="td4">本期销量</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${page}" var="data" varStatus="d">
							<c:set value="0" var="pceggSum"></c:set>
							<tr class="<c:if test="${d.index%2==0}">bgcolor1</c:if><c:if test="${d.index%2!=0}">bgcolor2</c:if>">
								<td class="td1"><fmt:formatDate value="${data.endTime}" var="date" pattern="yyyy-MM-dd" />${date}</td>
								<td class="td1"><fmt:formatDate value="${data.endTime}" var="date" pattern="HH:mm:ss" />${date}</td>
								<td class="td2">${data.qiHao}</td>
								<td class="td3"><span class="result"> 
									<c:choose>
										<c:when test="${data.haoMa ne null}">
											<c:set value="${ fn:split(data.haoMa, ',') }" var="arr" />
											<c:forEach items="${arr}" var="haoMa" varStatus="is">
												<c:choose>
										<c:when test="${data.lotCode eq 'LHC' || data.lotCode eq 'SFLHC' || data.lotCode eq 'TMLHC' || data.lotCode eq 'WFLHC' || data.lotCode eq 'HKMHLHC'}">
											<c:choose>
												<c:when test="${haoMa == '?'}">
														<c:if test="${is.index == 6}"><b>+</b>&nbsp;<span class="ball_2">${haoMa}</span></c:if>
														<c:if test="${is.index != 6 }"><span class="ball_1">${haoMa}</span></c:if>
												</c:when>
												<c:otherwise>
													<c:set var="hong" value="01,02,07,08,12,13,18,19,23,24,29,30,34,35,40,45,46"></c:set>
													<c:set var="lan" value="03,04,09,10,14,15,20,25,26,31,36,37,41,42,47,48"></c:set>
													<c:set var="lv" value="05,06,11,16,17,21,22,27,28,32,33,38,39,43,44,49"></c:set>
													<c:if test="${is.index == 6 }"><b>+</b></c:if>
													<c:if test="${fn:contains(hong, haoMa)}"><span class="ball_1">${haoMa}</span></c:if>
													<c:if test="${fn:contains(lan, haoMa)}"><span class="ball_2">${haoMa}</span></c:if>
													<c:if test="${fn:contains(lv, haoMa)}"><span class="ball_4">${haoMa}</span></c:if>
												</c:otherwise>
											</c:choose>
											<!-- <font style="display:inline-block;width:20px;height:20px;color:#000">猪</font> -->
										</c:when>
										<c:when test="${data.lotCode eq 'PCEGG' || data.lotCode eq 'JND28'}">
											<c:choose>
												<c:when test="${haoMa eq '?'}">
													<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
													<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">?</span></c:if>
												</c:when>
												<c:otherwise>
													<c:set value="${pceggSum + haoMa}" var="pceggSum"></c:set>
													<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
													<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">${pceggSum}</span></c:if>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${data.lotCode eq 'BJSC' || data.lotCode eq 'XYFT' || data.lotCode eq 'LXYFT' || data.lotCode eq 'AZXY10' || data.lotCode eq 'SFSC'}">
											<c:if test="${haoMa eq '?'}"><span class="ball_1">${haoMa}</span></c:if>
											<c:if test="${haoMa ne '?'}"><span class="pk10 b${haoMa}"></span></c:if>
										</c:when>
										<c:otherwise>
											<span class="ball_1">${haoMa}</span>
										</c:otherwise>
									</c:choose>
											</c:forEach>
										</c:when>
										<c:otherwise>--</c:otherwise>
									</c:choose>
								</span></td>
								<td class="td4">0</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="buybtn clear">
					<a target="_blank" href="${base}/lotteryV3/lotDetail.do?lotCode=${lotCode}" class="btn"><img
						src="${base}/common/template/lottery/lecai/images/draw/buy_btn.gif"></a>
				</div>
			</div>
			<div class="right_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>
	<script type="text/javascript">
		var c = '${lotCode}';
		var year = '${year}';
		$(function() {
			$("#history_phase_list").chosen({
				disable_search : true
			});
			$('#startTime').cxCalendar({format : 'YYYY-MM-DD',position:'bottomLeft'});
			if (c == 'LHC') {
				$(".dipin_Draw_ul").css({
					'margin-bottom' : '10px'
				});
				initHaoMa('${lastData.haoMa}');
			}
		})
		function changeDate($this) {
			window.location.href = "${base}/lotteryV3/draw/hisresult.do?lotCode=${lotCode}&startTime="+ $this.value;
		}

		function initHaoMa(haoMa) {
			if (!haoMa) {
				return false;
			}
			var ma = haoMa.split(","), temp = "";
			for (var i = 0; i < ma.length; i++) {
				if (i == 6) {
					temp += '<b style="display: inline-block;margin-top: -10px;">+</b>&nbsp;';
				}
				if (ma[i] == '?') {
					temp += '<span class="lhc_1">'
							+ ma[i]
							+ '<font style="display:inline-block;width:20px;height:20px;color:#000">?</font></span>';
				} else {
					var index = Base.lhcHaoMa.bose(ma[i]);
					var sx = Base.lhcHaoMa.zodiacName(ma[i], '${year}');
					temp += '<span class="lhc_'+index+'">'
							+ ma[i]
							+ '<font style="display:inline-block;width:20px;height:20px;color:#000">'
							+ sx + '</font></span>';
				}
			}
			$("#jq_latest_draw_result").html(temp);
		}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>