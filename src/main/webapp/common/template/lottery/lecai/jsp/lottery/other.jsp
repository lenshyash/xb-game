<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="gm_con">
				<div class="gm_con_to">
					<div class="tz_body" id="tabbar-div-s2">
						<div class="unit">
							<div class="unit_title">
								<div class="ut_l"></div>
								<div class="u_tab_div"><c:forEach var="group" items="${playGroupList}" varStatus="groupIndex"><c:choose><c:when test="${groupIndex.first}"><span class="tab-front" value="${group.code}" tag="${group.id}" default="${group.stationId}" type="${group.lotType}" style="display: inline;"></c:when><c:otherwise><span class="tab-back" value="${group.code}" tag="${group.id}" default="${group.stationId}" type="${group.lotType}" style="display: inline;"></c:otherwise></c:choose><span class="tabbar-left"></span><span class="content">${group.name}</span><span class="tabbar-right"></span></span></c:forEach></div>
								<div class="ut_r"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="tzn_body">
						<div class="tzn_b_n" id="tzn_b_n_content"></div>
						<div class="clear"></div>
					</div>
					<div class="zh_body">
						<!--注单添加及确认内容开始-->
						<div class="tbn_bot">
							<div class="tbn_b_top bottomNone">
								<div class="tbn_bt_sel">
									<c:if test="${lotteryBetModel == '111'}">
										<ul class="choose-money">
											<li class="on" value="1">元</li>
											<li value="10">角</li>
											<li value="100">分</li>
										</ul>
									</c:if>
									<c:if test="${lotteryBetModel == '100'}">
										<ul class="choose-money">
											<li class="on" value="1">元</li>
										</ul>
									</c:if>
									<span class="bei-shu"><a href="javascript:;"
										class="tz_bei_sub">−</a><input value="1" class="text"
										id="beiShu" name="input" maxlength="7" type="text"><a
										href="javascript:;" class="tz_bei_add">+</a><span class="mr10">倍</span>
										您选中了 <strong><span class="n" id="lt_sel_nums">0</span></strong>
										注，倍数<strong><span class="n" id="lt_sel_beiShu">1</span></strong>，
										共 <strong><span class="n" id="lt_sel_money">0</span></strong>
										元，若中奖，奖金<strong><span class="n" id="lt_sel_win_money">0</span></strong>元，可盈利<strong><span
											class="n" id="lt_sel_profit_money">0</span></strong>元 
								</div>
								<div class="clear"></div>
							</div>
							<div class="tbn_b_top">
								<div class="reflushText">
									奖金返点：<span class="red" id="peiLv">0</span>元&nbsp;
								<c:if test="${rateRandomOnoff ne 'on'}">/&nbsp;
									<span class="red" id="fanShui">0</span>%
								</c:if>
								</div>
								<div class="reflushText" style="margin-left: 10px;">
									可用余额 <span class="red" id="gamePoint2">0</span>元 <a
										href="javascript:void(0);" onclick="refreshMoney();"
										class="refresh"></a>
								</div>
								<input class="direct_buy" id="submitDirectly" value="直接投注"
									type="button">
								<!-- <div class="g_submit" id="lt_sel_insert"><span class="touzhu"></span>添加注单</div> -->
								<button id="addBettingNumberId" title="添加至购彩蓝" class="lot_cart">
									<i>添加至购彩蓝</i>
								</button>
							</div>
							<div class="tbn_b_bot">
								<div class="tbn_bb_l">
									<div class="tbn_bg2">
										<table class="tz_tab_list" cellspacing="0" cellpadding="0"
											border="0">
											<thead>
												<th width="80">投注项</th>
												<th width="80">下注类型</th>
												<th width="120">号码</th>
												<th width="70">总注数</th>
												<th width="100">奖金/返点</th>
												<th width="100">金额</th>
												<th width="50">删除</th>
											</thead>
										</table>
									</div>
									<div class="tbn_bb_ln">
										<div class="tz_tab_list_box">
											<table id="lt_cf_content" class="tz_tab_list" cellspacing="0"
												cellpadding="0" border="0">
												<colgroup>
													<col width="80">
													<col width="80">
													<col width="120">
													<col width="70">
													<col width="100">
													<col width="100">
													<col width="50">
												</colgroup>
												<!-- <tr class="nr"><td class="noinfo" align="center" colspan="7">暂无投注项</td></tr> -->
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
									<div class="zh_box">
										<div class="zhuihao">
											<span class="zhuihao_wz">我要追号：</span>

											<div class="lt_trace_if">
												<input name="lt_trace_if" id="lt_trace_if" value="1"
													type="checkbox"> <label for="lt_trace_if"
													name="lt_trace_if"></label>
											</div>
										</div>

										<!--<div class="qingkuang"><span class="qingkuang_ui"></span> <a id="lt_bet_con" style="cursor:pointer" onclick="bet_con()">下注情况</a></div>-->
										<div id="lt_cf_clear" class="scall">
											<span class="scall_ui"></span> <a>删除全部</a>
										</div>
										<!--                       <div>
                                                                                                                    追号
                                                                                                                    <div class="lt_trace_if">
                                                                                                                        <input type="checkbox" value="yes" id="lt_trace_if" name="lt_trace_if">
                                                                                            <label name="lt_trace_if" for="lt_trace_if"></label>
                                                                                                                    </div>
                                                                                                                </div>-->
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="tbn_bb_2">
								<div class="bof-o-random">
									<label>
										<div
											class="tp-ui-item tp-ui-forminput tp-ui-number tp-ui-number-depart">
											<div class="tp-ui-sub tp-ui-number-refer">
												<input id="randomSelNumInput" value="1" maxlength="2"
													class="random-selection myNumClass0" type="text">
											</div>
											<div
												class="tp-ui-sub tp-ui-handle-button tp-ui-number-option tp-ui-number-addto">
												<button type="button">
													<em>+</em>
												</button>
											</div>
											<div
												class="tp-ui-sub tp-ui-handle-button tp-ui-number-option tp-ui-number-reduce">
												<button type="button">
													<em>-</em>
												</button>
											</div>
										</div>
										<h4>注</h4>
									</label>
									<button id="randomSelBtn">
										<span>机选</span>
									</button>
								</div>
								<div class="g_submit" id="lt_buy">
									<span class="tianjia"></span>确认投注
								</div>
								<div class="sub_txt">
									总注数: <strong><span class="r" id="lt_cf_nums">0</span></strong>注
								</div>
								<div class="sub_txt">
									总金额: <strong><span class="r" id="lt_cf_money">0</span></strong>元
								</div>
								<div class="clear"></div>
							</div>
							<!--注单添加及确认内容结束-->
						</div>
						<!--追号开始-->
						<div class="unit">
							<div class="unit_title">
								<div class="ut_l"></div>
								<div class="ut_r"></div>
							</div>
							<!--追号区开始-->
							<div class="trace_box" style="display: none;" id="lt_trace_box">
								<div class="unit1">
									<div class="unit_title2">
										<div id="tab02">
											<div class="bd">
												<div id="general_txt_100" class="bd2">
													<div class="tabcon_n">
														<label class="trace-number-confirm">
															<button id="creatGtjhBtn" title="生成追号方案">
																<i class="hide">生成追号方案</i>
															</button>
														</label> <label name="lt_trace_stop" class="zh_continue">
															<input value="yes" id="lt_trace_stop" checked="checked"
															name="lt_trace_stop" type="checkbox">中奖后停止追号
														</label>
													</div>
													<table class="tabbar-div-s3" width="100%">
														<tbody>
															<tr>
																<td id="lt_trace_label"><span id="lt_margin"
																	class="tab-back" style="display: none;"> <span
																		class="tabbar-left"></span> <span class="content">利润率追号</span>
																		<span class="tabbar-right"></span>
																</span> <span id="lt_sametime" class="tab-front"> <span
																		class="tabbar-left"></span> <span class="content">常规追号</span>
																		<span class="tabbar-right"></span>
																</span> <span id="lt_difftime" class="tab-back"
																	style="display: none;"> <span
																		class="tabbar-left"></span> <span class="content">翻倍追号</span>
																		<span class="tabbar-right"></span>
																</span></td>
															</tr>
														</tbody>
													</table>
													<div class="bl3p"></div>
												</div>
											</div>
										</div>
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
									<div class="zhgen">
										<table width="100%" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr>
													<td>追号计划:<span id="lt_trace_labelhtml"> <span
															id="lt_sametime_html">追号期数&nbsp;<input
																style="width: 36px" id="lt_trace_count_input" value="10"
																maxlength="2" type="text"></span>
															&nbsp;&nbsp;起始倍数&nbsp;<input id="lt_trace_times_same"
															value="1" maxlength="4" type="text">
													</span> 总期数: <span id="lt_trace_count" class="y">0</span> 期 追号总金额:
														<span id="lt_trace_hmoney" class="y">0</span> 元
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div id="lt_trace_issues" class="zhlist">
										<table id="lt_trace_issues_table" width="95%" cellspacing="0"
											cellpadding="0" border="0">
											<thead>
												<tr class="t">
													<th width="30"><input id="betTraceSelectAll"
														checked="" type="checkbox"></th>
													<th width="160">期 号</th>
													<th width="110">倍率</th>
													<th>当前投入</th>
													<th>累计投入</th>
													<th>盈利(元)</th>
													<th>盈利率</th>
												</tr>
											</thead>
											<tbody id="betTraceData">
											</tbody>
										</table>
									</div>
									<input value="5" id="lotteryid" name="lotteryid" type="hidden">
									<input value="2" id="type" name="type" type="hidden"> <input
										name="formhash" value="aDMY-WaF8Vui_JQ2pE8kAU6R4WeueKI"
										type="hidden">
								</div>
							</div>
							<!--追号区结束-->
						</div>
						<!--追号结束-->
						<div class="clear"></div>
						<!--下注情况BOX开始-->
						<div class="gm_con" style="" id="lt_bet_box">
							<div class="gm_con_to">
								<div class="yx_body">
									<div class="unit">
										<div class="unit_title1">
											<div class="ut_l"></div>
											<table class="tabbar-div-s3" width="100%">
												<tbody>
													<tr>
														<td id="lt_trace_label"><span id="lt_margin"
															class="def_title tab-front"><span class="content"><em></em>投注记录</span></span>
															<span class="def_title"><span class="content">彩种：${bly.name}</span><span
																class="content" id="betListBetween"></span><span
																class="content red">贴心小提醒：点击注单号能查询详细投注明细</span></span>
															<div class="bh-refresh" id="refreshBettingHistory">
																<a href="javascript:void(0);"><i>&nbsp;</i>刷新</a>
															</div></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="yx_box">
										<div class="yxlist" style="max-height: 1000px;">
											<table width="96%" cellspacing="0" cellpadding="0"
												border="0">
												<thead>
													<tr>
														<th>注单号</th>
														<th>下注时间</th>
														<th>期号</th>
														<th>玩法</th>
														<th>倍数</th>
														<th>开奖号码</th>
														<th>总额</th>
														<th>中奖金额</th>
														<th>状态</th>
														<th class="noBorder">操作</th>
													</tr>
												</thead>
												<tbody class="projectlist" id="betResultOrder">
												</tbody>
												<tbody id="betOrderTblFoot" style="display:none;">
													<tr>
														<td colspan="6" class="t_right">小计</td>
														<td id="betPageAmount" class="color-blue"></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td colspan="6" class="t_right">总计</td>
														<td class="red" id="betAmount"></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="10" id="pageinfo" align="center" height="37">
															<jsp:include page="../include/page.jsp"></jsp:include>
														</td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>

							</div>
							<div style="float: left; height: 1px;"></div>
						</div>
						<!--下注情况结束-->
					</div>
				</div>
			</div>