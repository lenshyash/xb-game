<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<strong class="p1" id="jj">简介</strong>
<c:if test="${param.code == 'CQSSC'}">
<!-- <p class="p1">
	重庆时时彩游戏是依照中国重庆福利彩票发行中心统一发行的『时时彩』彩票的开奖数据为依据所规划的线上彩票游戏。</p> -->
<p class="p1">
	重庆时时彩属于高频彩，投注区分为万位、千位、百位、十位和个位，各位号码范围为0~9，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0" bgcolor="#CCCCCC">
                    <thead>
                    <tr>
                        <td bgcolor="#eFeFeF" width="25%">彩种名称</td>
                        <td bgcolor="#eFeFeF" width="24%">开奖时间</td>
                        <td bgcolor="#eFeFeF" width="18%">每日期数</td>
                        <td bgcolor="#eFeFeF" width="33%">开奖频率</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td rowspan="4" bgcolor="#FFFFFF" align="center">重庆时时彩</td>
                       
                    </tr>
                    <tr>
                        <td bgcolor="#F7F7F7">00:30-03:10(北京时间)</td>
                        <td bgcolor="#F7F7F7">01-09 </td>
                        <td bgcolor="#F7F7F7">每20分钟 </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF">07:30-23:50(北京时间)</td>
                        <td bgcolor="#FFFFFF">10-59</td>
                        <td bgcolor="#FFFFFF">每20分钟 </td>
                    </tr>
                    <!-- <tr>
                        <td bgcolor="#F7F7F7">00:05-01:55(北京时间)</td>
                        <td bgcolor="#F7F7F7">001-023</td>
                        <td bgcolor="#F7F7F7">每5分钟 </td>
                    </tr> -->
                    </tbody>
                </table>
</c:if>
<c:if test="${param.code == 'TJSSC'}">
<p class="p1">天津时时彩由中国福利彩票发行管理中心组织，由天津市福利彩票发行中心在所辖区域内承销。</p>
<p class="p1">
	天津时时彩属于高频彩，投注区分为万位、千位、百位、十位和个位，各位号码范围为0~9，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0" bgcolor="#CCCCCC">
                    <thead>
                    <tr>
                        <td bgcolor="#eFeFeF" width="25%">彩种名称</td>
                        <td bgcolor="#eFeFeF" width="24%">开奖时间</td>
                        <td bgcolor="#eFeFeF" width="18%">每日期数</td>
                        <td bgcolor="#eFeFeF" width="33%">开奖频率</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	<td bgcolor="#FFFFFF" align="center">天津时时彩</td>
                        <td bgcolor="#F7F7F7">09:20-23:00(北京时间)</td>
                        <td bgcolor="#F7F7F7">01-42 </td>
                        <td bgcolor="#F7F7F7">每20分钟 </td>
                    </tr>
                    </tbody>
                </table>
</c:if>
<c:if test="${param.code == 'XJSSC'}">
<p class="p1">新疆时时彩由中国福利彩票发行管理中心组织，由新疆自治区福利彩票发行中心在所辖区域内承销。</p>
<p class="p1">
	新疆时时彩属于高频彩，投注区分为万位、千位、百位、十位和个位，各位号码范围为0~9，每期从各位上开出1个号码组成中奖号码。玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
</p>
<table class="s-table" cellspacing="1" cellpadding="0" border="0" bgcolor="#CCCCCC">
                    <thead>
                    <tr>
                        <td bgcolor="#eFeFeF" width="25%">彩种名称</td>
                        <td bgcolor="#eFeFeF" width="24%">开奖时间</td>
                        <td bgcolor="#eFeFeF" width="18%">每日期数</td>
                        <td bgcolor="#eFeFeF" width="33%">开奖频率</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	<td bgcolor="#FFFFFF" align="center">新疆时时彩</td>
                        <td bgcolor="#F7F7F7">10:20-02:00(北京时间)</td>
                        <td bgcolor="#F7F7F7">01-48 </td>
                        <td bgcolor="#F7F7F7">每20分钟 </td>
                    </tr>
                    </tbody>
                </table>
</c:if>
<p class="p2">
	<br />
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="5xzx_fs">五星复式</strong>
<p class="p1">从万、千、百、十、个位各选一个号码组成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位选择一个5位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：13456</p>
<p class="p1">开奖号码:13456，即中五星直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q4zx_fs">前四复式</strong>
<p class="p1">从万、千、百、十位号选一个号码组成一注。&nbsp;</p>
<p class="p1">从万位、千位、百位、十位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：3456</p>
<p class="p1">开奖号码:万位、千位、百位、十位分别为3456，即中前四复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zx_fs">前三复式</strong>
<p class="p1">从万位、千位、百位各选一个号码组成一注。&nbsp;</p>
<p class="p1">从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：345</p>
<p class="p1">开奖号码:前三位 345，即中前三直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zx_fs">后三复式</strong>
<p class="p1">从百位、十位、个位各选一个号码组成一注。&nbsp;</p>
<p class="p1">从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码后3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案： 345</p>
<p class="p1">开奖号码:后三位 345，即中后三直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zx_fs">后四复式</strong>
<p class="p1">从千、百、十、个位号选一个号码组成一注。</p>
<p class="p1">从千位、百位、十位、个位选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。</p>
<p class="p1">&nbsp;投注方案：3456</p>
<p class="p1">开奖号码:千位、百位、十位、个位分别为3456，即中后四复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="z3zx_fs">中三复式</strong>
<p class="p1">从千位、百位、十位各选一个号码组成一注。&nbsp;</p>
<p class="p1">从千位、百位、十位中选择一个3位数号码组成一注，所选号码与开奖号码中3位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：345</p>
<p class="p1">开奖号码:中三位 345，即中中三直选。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zux_zu3">前三组三</strong>
<p class="p1">从0-9中任意选择2个或2个以上号码。</p>
<p class="p1">从0-9选择2个数字组成二注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。</p>
<p class="p1">投注方案：588</p>
<p class="p1">开奖号码:前三位 588（顺序不限），即中前三组三。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q3zux_zu6">前三组六</strong>
<p class="p1">从0-9中任意选择3个或3个以上号码。</p>
<p class="p1">&nbsp;从0-9选择3个数字组成一注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
</p>
<p class="p1">投注方案：258</p>
<p class="p1">开奖号码:前三位 852（顺序不限），即中前三组六。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="z3zux_zu3">中三组三</strong>
<p class="p1">从0-9中任意选择2个或2个以上号码。</p>
<p class="p1">&nbsp;从0-9选择2个数字组成二注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
</p>
<p class="p1">投注方案： 588</p>
<p class="p1">开奖号码:中三位 588（顺序不限），即中中三组三。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="z3zux_zu6">中三组六</strong>
<p class="p1">从0-9中任意选择3个或3个以上号码。</p>
<p class="p1">&nbsp;从0-9选择3个数字组成一注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
</p>
<p class="p1">投注方案：258</p>
<p class="p1">开奖号码:中三位 852（顺序不限），即中中三组选六。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zux_zu3">后三组三</strong>
<p class="p1">从0-9中任意选择2个或2个以上号码。</p>
<p class="p1">从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</p>
<p class="p1">投注方案：588</p>
<p class="p1">开奖号码:后三位 588（顺序不限），即中后三组三。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h3zux_zu6">后三组六</strong>
<p class="p1">从0-9中任意选择3个或3个以上号码。</p>
<p class="p1">从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</p>
<p class="p1">投注方案： 258</p>
<p class="p1">开奖号码:后三位 852（顺序不限），即中后三组六。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx_fs">前二复式</strong>
<p class="p1">从万位、千位各选一个号码组成一注。</p>
<p class="p1">从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：58</p>
<p class="p1">开奖号码：前二位58，即中前二复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h2zx_fs">后二复式</strong>
<p class="p1">从十位、个位各选一个号码组成一注。</p>
<p class="p1">从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：58</p>
<p class="p1">开奖号码：后二位58，即中后二复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx_hz">前二和值</strong>
<p class="p1">从0-18中任意选择1个或1个以上号码。</p>
<p class="p1">所选数值等于开奖号码万位、千位二个数字相加之和，即为中奖。</p>
<p class="p1">投注方案：和值1</p>
<p class="p1">开奖号码：前二位01或10，即中前二星和值。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h2zx_hz">后二和值</strong>
<p class="p1">从0-18中任意选择1个或1个以上号码。</p>
<p class="p1">所选数值等于开奖号码十位、个位二个数字相加之和，即为中奖。</p>
<p class="p1">投注方案：和值1</p>
<p class="p1">开奖号码：后二位01或10，即中后二和值。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_q31m">前三一码</strong>
<p class="p1">从0-9中任意选择1个以上号码。&nbsp;</p>
<p class="p1">
	&nbsp;从0-9中选择1个号码，每注由1号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。</p>
<p class="p1">&nbsp;投注方案： 1</p>
<p class="p1">开奖号码：前三位至少出现1个1，即中前三一码。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_z31m">中三一码</strong>
<p class="p1">从0-9中任意选择1个以上号码。&nbsp;</p>
<p class="p1">从0-9中选择1个号码，每注由1号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：1</p>
<p class="p1">开奖号码：中三位至少出现1个1，即中三一码。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_h31m">后三一码</strong>
<p class="p1">从0-9中任意选择1个以上号码。&nbsp;</p>
<p class="p1">从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</p>
<p class="p1">投注方案：1</p>
<p class="p1">开奖号码：后三位至少出现1个1，即中后三一码。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxwf_r2zx_fs">任二复式</strong>
<p class="p1">从万位、千位、百位、十位、个位中至少两位上各选1个号码组成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位中至少两位上个选1个号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：万位5，百位8</p>
<p class="p1">开奖号码：51812，即中任二复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxwf_r3zx_fs">任三复式</strong>
<p class="p1">从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位中至少三位上各选1个号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案： 万位5，百位8，个位2</p>
<p class="p1">开奖号码：51812，即中任三复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxwf_r3zux_zu3">任三组三</strong>
<p class="p1">从万、千、百、十、个中至少选择三个位置,号码区至少选择两个号码构成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位中至少选择三个位置，号码区至少选择两个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。
</p>
<p class="p1">投注方案：位置选择万位、十位、个位，选择号码12</p>
<p class="p1">开奖号码：11812，即中任三组三。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxwf_r3zux_zu6">任三组六</strong>
<p class="p1">从万、千、百、十、个中至少选择三个位置,号码区至少选择三个号码构成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位中至少选择三个位置，号码区至少选择三个号码构成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。
</p>
<p class="p1">投注方案： 位置选择万位、十位、个位，选择号码512</p>
<p class="p1">开奖号码：512，即中任三组六。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="rxwf_r4zx_fs">任四复式</strong>
<p class="p1">从万、千、百、十、个中至少四位上各选1个号码组成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位中至少四位上各选1个号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。</p>
<p class="p1">投注方案：万位5，千位1、百位8，十位1</p>
<p class="p1">开奖号码：51812，即中任四复式。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dwd">定位胆</strong>
<p class="p1">从万、千、百、十、个位号选一个号码组成一注。&nbsp;</p>
<p class="p1">
	从万位、千位、百位、十位、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
<p class="p1">投注方案：万位1</p>
<p class="p1">开奖号码：万位1，即中定位胆万位。</p>
<p>
	<br />
</p>
<strong class="p1" id="longhudou">龙虎</strong>
<p class="p1">
   从龙 虎中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
   开奖第一球（万位）的号码 大于 第五球（个位）的号码为龙，开奖第一球（万位）的号码 小于 第五球（个位）的号码为虎。如：第一球开出 6，第五球开出 2 为龙，下注龙虎时开和不退回本金，亦算输。反之下注和开龙虎亦算输。
</p>
<p class="p1">
    投注方案：龙；
</p>
<p class="p1">
    开奖号码：93413，第一球开出 9，第五球开出 3，即中龙。反之为虎
</p>
<p>
    <br/>
</p>
<strong class="p1" id="baozi">猜豹子--豹子</strong>
<p class="p1">
  从前三、中三、后三中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
   开奖号码的所选的3位（前三，中三，后三）数字都相同，如后三豹子：XX222、XX666、XX888
</p>
<p class="p1">
    投注方案：前三豹子；
</p>
<p class="p1">
   开奖号码：55513，前三个数字相同即中前三豹子。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="shunzi">猜豹子--顺子</strong>
<p class="p1">
  从前三、中三、后三中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
  开奖号码的所选的3位（前三，中三，后三）数字都相连，不分顺序，如后三顺子：XX123、XX901、XX321、XX798
</p>
<p class="p1">
    投注方案：前三顺子；
</p>
<p class="p1">
   开奖号码：78613，前三个数字相连即中前三顺子。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="duizi">猜豹子--对子</strong>
<p class="p1">
  从前三、中三、后三中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
  开奖号码的所选的3位（前三，中三，后三）任意两位数字相同（不包括豹子）,如后三对子：XX001，XX288、XX696
</p>
<p class="p1">
    投注方案：前三对子；
</p>
<p class="p1">
   开奖号码：56513，前三个数字任意两位数字相同（不包括豹子）即中前三对子。
</p>
<p>
    <br/>
</p>

<strong class="p1" id="banshun">猜豹子--半顺</strong>
<p class="p1">
  从前三、中三、后三中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
 开奖号码的所选的3位（前三，中三，后三）任意两位数字相连，不分顺序（不包括顺子、对子）,如后三半顺：如中奖号码为：XX125、XX540、XX390、XX160
</p>
<p class="p1">
    投注方案：前三半顺；
</p>
<p class="p1">
   开奖号码：57413，前三个数字任意两位数字相连，不分顺序（不包括顺子、对子）即中前三半顺。
</p>
<p>
    <br/>
</p>

<strong class="p1" id="zaliu">猜豹子--杂六</strong>
<p class="p1">
  从前三、中三、后三中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
 开奖号码的所选的3位（前三，中三，后三）不包括豹子、对子、顺子、半顺的所有开奖号码，如后三杂六：XX157、XX268
</p>
<p class="p1">
    投注方案：前三杂六；
</p>
<p class="p1">
   开奖号码：27513，前三个数字不包括豹子、对子、顺子、半顺即中前三杂六。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="dxds_zh">大小单双--总和</strong>
<p class="p1">
从总和的“大、小、单、双”中至少选一个组成一注。&nbsp;
</p>
<p class="p1">
 对5个号码总和的“大（大于或等于 23 ）小（小于23）、单（单数）双（双数）”型态进行购买，所选号码的型态与开奖号码的型态相同，即为中奖。
</p>
<p class="p1">
    投注方案：大；
</p>
<p class="p1">
   开奖号码：55555，总和25，即中总和大。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="dxds_q3">大小单双--前三</strong>
<p class="p1">
从万位、千位、百位中的“大、小、单、双”中至少各选一个组成一注。&nbsp;
</p>
<p class="p1">
 对万位、千位和百位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p1">
    投注方案：大单大；
</p>
<p class="p1">
   开奖号码：万位、千位、百位“大单大”，即中前三大小单双。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="dxds_h3">大小单双--后三</strong>
<p class="p1">
从百位、十位、个位中的“大、小、单、双”中至少各选一个组成一注。&nbsp;
</p>
<p class="p1">
 对百位、十位和个位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p1">
    投注方案：大单大；
</p>
<p class="p1">
   开奖号码：百位、十位、个位“大单大”，即中后三大小单双。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="dxds_q2">大小单双--前二</strong>
<p class="p1">
从万位、千位中的“大、小、单、双”中至少各选一个组成一注。 &nbsp;
</p>
<p class="p1">
 对万位、千位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p1">
    投注方案：大单；
</p>
<p class="p1">
  开奖号码：万位与千位“大单”，即中前二大小单双。
</p>
<p>
    <br/>
</p>
<strong class="p1" id="dxds_h2">大小单双--后二</strong>
<p class="p1">
从十位、个位中的“大、小、单、双”中至少各选一个组成一注。 &nbsp;
</p>
<p class="p1">
 对十位、个位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p1">
    投注方案：大单；
</p>
<p class="p1">
	开奖号码：十位与个位“大单”，即中后二大小单双。
</p>
<p>
    <br/>
</p>