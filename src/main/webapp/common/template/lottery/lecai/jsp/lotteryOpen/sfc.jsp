<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="dsLotto-result dsLotto-rs dsLotto-rs--2r8c dsLotto-rs--xy dsLotto-rs--sfc status--on">
    <div class="ui-row ui-row-i">
        <ul class="array cf">
            <li class="item-1 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_0">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 200ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-2 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_1">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 400ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-3 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_2">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 600ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-4 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_3">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 800ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-5 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_4">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 1000ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-6 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_5">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 1200ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-7 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_6">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 1400ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
            <li class="item-8 ico-farm">
            	<div class="_ele-number _ele_sfc" id="last_result_hm_7">00</div>
            	<div class="_ele-unknown" style=" animation-delay: 1600ms">
                    <ul>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                        <li class="ico-farm _ele_sfc">01</li>
                        <li class="ico-farm _ele_sfc">02</li>
                        <li class="ico-farm _ele_sfc">03</li>
                        <li class="ico-farm _ele_sfc">04</li>
                        <li class="ico-farm _ele_sfc">05</li>
                        <li class="ico-farm _ele_sfc">06</li>
                        <li class="ico-farm _ele_sfc">07</li>
                        <li class="ico-farm _ele_sfc">08</li>
                        <li class="ico-farm _ele_sfc">09</li>
                        <li class="ico-farm _ele_sfc">10</li>
                        <li class="ico-farm _ele_sfc">11</li>
                        <li class="ico-farm _ele_sfc">12</li>
                        <li class="ico-farm _ele_sfc">13</li>
                        <li class="ico-farm _ele_sfc">14</li>
                        <li class="ico-farm _ele_sfc">15</li>
                        <li class="ico-farm _ele_sfc">16</li>
                        <li class="ico-farm _ele_sfc">17</li>
                        <li class="ico-farm _ele_sfc">18</li>
                        <li class="ico-farm _ele_sfc">19</li>
                        <li class="ico-farm _ele_sfc">20</li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>