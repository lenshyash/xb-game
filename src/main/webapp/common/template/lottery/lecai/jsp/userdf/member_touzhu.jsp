<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10" id="pageName">投注记录</div>
            <div class="userMain mgb10">
                <%-- <ul class="todayView mgb10">
                    <li>今日概况</li>
                    <li>投注金额：<span>${-allBetAmount}元</span></li>
                    <li>中奖金额：<span>${yingkuiAmount}元</span></li>
                    <li>盈利：<span>${allWinAmount}元</span></li>
                </ul> --%>
                <ul class="searchFirst">
                    <li><span>彩种：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                    <option value="">所有彩种</option>
									<c:forEach items="${lotList}" var="lot">
										<option value="${lot.code}"<c:if test="${not empty lotCode && lotCode eq lot.code}">selected="selected"</c:if>>${lot.name}</option>
									</c:forEach>
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                    <li class="_state">
                    	<span>类型：
       						<a class="userSearch time active" value=" ">全部</a>
       						<a class="userSearch time " value="2">已中奖</a>
       						<a class="userSearch time " value="3">未中奖</a>
       						<a class="userSearch time " value="1">等待开奖</a>
       						<a class="userSearch time " value="4">撤单</a>
       					</span>
       				</li>
                </ul>
                <ul class="searchFirst accountValue" style="display:none;">
                	<li class="_state" style="display: flex;align-items: center;padding-left:21px;">
                    	<span>账号:</span>
                    	<input class="inputText" id="account" value="" type="text" placeholder="下级投注查询" style="background:#fff;height:30px;margin-left:10px;">
                    	<input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
       				</li>                    
                 </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr>
                            <th class="touzhuName" style="display:none;">账号</th>
                            <th>彩种</th>
                            <th>期号</th>
                            <th>投注内容</th>
                            <th>投注金额</th>
                            <th>中奖金额</th>
                            <th>开奖号码</th>
                            <th>奖金状态</th>
                            <th>投注时间</th>
                            <th>操作项</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：投注记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			var url = '';
		    $(function () {
		    	lotteryType = ${lotteryNames}
	            if(Request['account']){
					$("#account").val(Request['account'])
					$(".accountValue").show()
					$(".touzhuName").show()
					$("#pageName").html('投注明细')
					url = '/daili/gameRecord/list.do'
				}else  if(Request['come'] == 'daili'){
					$(".accountValue").show()
					$(".touzhuName").show()
					$("#pageName").html('投注明细')
					url = '/daili/gameRecord/list.do'
				}else{
					url = '/lotteryV3/betInfo/getBcLotteryOrder.do'
				}
		    	initRdsData(1)
		        $("._time a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $("._state a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            lottereyStatus = $(this).attr('value')
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		        	lottereyStatus = $(this).attr('value')
		            initRdsData(1)
		        })
		    })
		    
		    function initRdsData(p) {
		    	$(".Records_listCont").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
					account:$("#account").val(),
					code : $("#lotCode").val(),
					status : lottereyStatus,
					model : '',
					zhuiHao : 1,
					startTime : startTime + " 00:00:00",
					endTime : endTime + " 23:59:59",
					rows : 10,
					page : p
				}
				$.ajax({
					url : '${base}' + url,
					data : data,
					success : function(res) {
						$(".notContent").show()
				    	$(".loadingGif").hide()
				    	if(Request['account'] || Request['come'] == 'daili'){
				    		if(res.rows.length > 0){
					    		addTemplate2(res)
					    		page = p;
								totalPage = Math.ceil((res.total/10));
								lotteryPage(res.total,totalPage);
								lastPage = totalPage;
								$("#pageNum").html(p)
					    	}else{
					    		$(".Records_listCont").html(noTakeNotes)
					    	}
						}else{
							if(res.page.list.length > 0){
					    		addTemplate(res)
					    		page = res.page.currentPageNo;
								lotteryPage(res.page.totalCount,res.page.totalPageCount);
								$("#pageNum").html(res.page.currentPageNo);
								lastPage = res.page.totalPageCount
					    	}else{
					    		$(".Records_listCont").html(noTakeNotes)
					    	}
						}
					}
				})
			}
			function addTemplate(res){
				var html = '', statusName = '', oper ='',haoma='',tzName;
				$.each(res.page.list,function(index, item){
					oper = ''
					switch (item.status) {
					case 1:
						statusName = '<span style="background-color: #428bca;color:#fff;border-radius:3px;padding:3px 5px;">未开奖</span>';
						<c:if test="${!unCancleOrder}">oper = '<a href="javascript:void(0);" class="cheDan" onclick="chedan(&quot;'+ item.orderId+ '&quot;,&quot;'+ item.lotCode+ '&quot;,&quot;member&quot;)" from="member">撤单</a>';</c:if>
						break;
					case 2:
						statusName = '<span style="background-color: #5cb85c;color:#fff;border-radius:3px;padding:3px 5px;">已中奖</span>';
						break;
					case 3:
						statusName = '<span style="background-color: #d9534f;color:#fff;border-radius:3px;padding:3px 5px;">未中奖</span>';
						break;
					case 4:
						statusName = '<span style="background-color: #5bc0de;color:#fff;border-radius:3px;padding:3px 5px;">已撤单</span>';
						break;
					case 5:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">回滚成功</span>';
						break;
					case 6:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">回滚异常</span>';
						break;
					case 7:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">开奖异常</span>';
						break;
					case 8:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">和局</span>';
						break;
					case 9:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">合买失效</span>';
						break;
					}
					haoma = item.haoMa
					if(haoma.length>20){
						haoma = '<span onclick="showMessage(&quot;'+item.haoMa+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看投注内容</span>'
					}
					if(Request['come'] == 'daili'){
						tzName = 'style="display:block;"'
					}else{
						tzName = 'style="display:none;"'
					}
					html+='<tr><td class="touzhuName" '+tzName+'>'+item.account+'</td><td>'+item.lotName+'</td><td>'+item.qiHao+'</td><td>'+haoma+'</td><td>'+item.buyMoney+'</td><td style="color:red;">'+unExist(item.winMoney)+'</td><td>'+(!item.lotteryHaoMa ? "- -" : item.lotteryHaoMa)+'</td><td>'+statusName+'</td><td>'+item.createTime+'</td><td>'+oper+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function addTemplate2(res){
				var html = '', statusName = '', oper ='',haoma='',tzName;
				$.each(res.rows,function(index, item){
					switch (item.status) {
					case 1:
						statusName = '<span style="background-color: #428bca;color:#fff;border-radius:3px;padding:3px 5px;">未开奖</span>';
						<c:if test="${!unCancleOrder}">oper = '<a href="javascript:void(0);" class="cheDan" onclick="chedan(&quot;'+ item.orderId+ '&quot;,&quot;'+ item.lotCode+ '&quot;,&quot;member&quot;)" from="member">撤单</a>';</c:if>
						break;
					case 2:
						statusName = '<span style="background-color: #5cb85c;color:#fff;border-radius:3px;padding:3px 5px;">已中奖</span>';
						break;
					case 3:
						statusName = '<span style="background-color: #d9534f;color:#fff;border-radius:3px;padding:3px 5px;">未中奖</span>';
						break;
					case 4:
						statusName = '<span style="background-color: #5bc0de;color:#fff;border-radius:3px;padding:3px 5px;">已撤单</span>';
						break;
					case 5:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">回滚成功</span>';
						break;
					case 6:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">回滚异常</span>';
						break;
					case 7:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">开奖异常</span>';
						break;
					case 8:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">和局</span>';
						break;
					case 9:
						statusName = '<span style="background-color: #f0ad4e;color:#fff;border-radius:3px;padding:3px 5px;">合买失效</span>';
						break;
					}
					haoma = item.haoMa
					if(haoma.length>20){
						haoma = '<span onclick="showMessage(&quot;'+item.haoMa+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看投注内容</span>'
					}
					if(Request['come'] == 'daili'){
						tzName = 'style="display:block;"'
					}else{
						tzName = 'style="display:none;"'
					}
					html+='<tr><td class="touzhuName" '+tzName+'>'+item.account+'</td><td>'+lotteryType[item.lotCode]+'</td><td>'+item.qiHao+'</td><td>'+haoma+'</td><td>'+item.buyMoney+'</td><td style="color:red;">'+unExist(item.winMoney)+'</td><td>'+(!item.lotteryHaoMa ? "- -" : item.lotteryHaoMa)+'</td><td>'+statusName+'</td><td>'+getMyDate(item.createTime)+'</td><td>'+oper+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function chedan(orderId,lotCode,from){
		    	layer.confirm('您确定要撤销该订单？', {
		    		  btn: ['确定','取消'],icon:3,offset : ['30%' ]
		    		}, function(){
		    			$.ajax({
		    	    		url:base+'/lotteryBet/cancelOrder.do',
		    	    		data:{
		    	    			orderId : orderId,
		    	    			lotCode : lotCode,
		    	    			},
		    	    		success:function(r){
		    	    			if(r.success){
			    	    			layer.msg("撤单成功",{icon: 6});
			    	    			initRdsData(page)
		    	    			}else{
		    	    				layer.msg(r.msg,{icon:2});
		    	    			}
		    	    		}
		    	    	})
		    		}, function(i){
		    			layer.close(i);
		    		});
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>