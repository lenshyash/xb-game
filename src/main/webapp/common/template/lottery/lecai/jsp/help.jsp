<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>帮助中心-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/help.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div id="lehecai_about">
		<div id="lehecai_about_left">
			<h1></h1>
			<ul>
				<li class="clear" style="height: 20px;"></li>
				<li><a <c:if test="${code eq 1}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=1" title="联系我们">关于我们</a></li>
				<li><a <c:if test="${code eq 2}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=2" title="取款帮助">取款帮助</a></li>
				<li><a <c:if test="${code eq 3}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=3" title="存款帮助">存款帮助</a></li>
				<c:if test="${domainFolder != 'd00538' && domainFolder != 'd00563'}">
					<li><a <c:if test="${code eq 4}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=4" title="联盟方案">联盟方案</a></li>
					<li><a <c:if test="${code eq 5}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=5" title="联盟协议">联盟协议</a></li>
				</c:if>
				<li><a <c:if test="${code eq 6}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=6" title="联系我们">联系我们</a></li>
				<li><a <c:if test="${code eq 7}">class="selected"</c:if> href="${base}/lotteryV3/help.do?code=7" title="常见问题">常见问题</a></li>
				<li class="clear" style="height: 22px;"></li>
			</ul>
		</div>
		<div id="lehecai_about_right">
			<script>
				function GetRequest() {
					var url = location.search; //获取url中"?"符后的字串
					var theRequest = new Object();
					if (url.indexOf("?") != -1) {
						var str = url.substr(1);
						strs = str.split("&");
						for(var i = 0; i < strs.length; i ++) {
							theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
						}
					}
					return theRequest;
				}
				var Request = new Object();
				Request = GetRequest();
				$(function () {
					if (Request['code'] != '100' && Request['code'] != '101' && Request['code'] != '102'){
						$.ajax({
							url: '${base}/native/getAgentArticle.do?code=' + Request['code'],
							// 设置的是请求参数
							data: {},
							// 用于设置响应体的类型 注意 跟 data 参数没关系！！！
							dataType: 'json',
							success: function (res) {
								$(".about_us_content").html(res.content)
							}
						})
					}

				})
			</script>
			<div class="block">
				<c:choose>
					<c:when test="${code eq 100}">
						<jsp:include page="static/why_reg.jsp"></jsp:include>
					</c:when>
					<c:when test="${code eq 101}">
						<jsp:include page="static/upd_pwd.jsp"></jsp:include>
					</c:when>
					<c:when test="${code eq 102}">
						<jsp:include page="static/more_user.jsp"></jsp:include>
					</c:when>
					<c:otherwise>
						<div class="title">
							<div class="about_us_title">
								<c:if test="${not empty title}">${title}</c:if>
								<c:if test="${empty title || title == '' }">${code eq 1?'关于我们':code eq 2?'取款帮助':code eq 3?'存款帮助':code eq 4?'联盟方案':code eq 5?'联盟协议':code eq 6?'联系我们':code eq 7?'常见问题':''}</c:if>
							</div>
						</div>
						<div class="clear" style="height: 25px;"></div>
						<div class="about_us_content">
<%--							${content}--%>
						</div>
						<div class="clear" style="height: 32px;"></div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>