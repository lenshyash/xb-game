<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style type="text/css">
	table{
	    font-size: 15px;
	    width:100%;
	    border-collapse: inherit;
    	border-spacing: 0;
	}
	table tr td{text-align: center;line-height: 35px;}
</style>
<div style="overflow-x: hidden;overflow-y: hidden;padding:20px;font-size:13px;text-align: center;">
<table>
	<thead>
		<tr>
			<th>玩法名称</th>
			<th>元模式奖金</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${playList}" var="play">
			<tr>
				<td>${play.name}</td>
				<td><fmt:formatNumber value="${play.minBonusOdds}" type="number" maxFractionDigits="2" minFractionDigits="2"/></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</div>
