<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="title">
	<div class="about_us_title">一个人可以注册多个帐号吗？</div>
</div>
<div class="clear" style="height: 25px;"></div>
<div class="about_us_content">
	<p>
	一个人可以注册多个本站会员名，但为了更好保障用户帐号资金安全，请如实填写个人真实资料。
</p>
</div>
<div class="clear" style="height: 32px;"></div>