<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>${lotName}开奖信息-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/draw_detail.css"
	type="text/css">
<script type="text/javascript" src="${base}/common/template/lottery/lecai/js/draw.js" code="${lotCode}"></script>
<link href="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/css/jquery.cxcalendar.css" rel="stylesheet">
<script type="text/javascript" src="${base}/common/template/lottery/jimei/js/jQuery.cxCalendar-1.5.3/js/jquery.cxcalendar.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="main">
		<div class="leftnav">
			<input type="hidden" value="3" id="navId" />
			<ul id="top_tab" class="changebox">
				<li class="on" id="top_sort"><a href="javascript://">按分类查看</a></li>
			</ul>
			<div class="cznav" id="nav_sort" style="display: block;">
				<ul class="item js-lottery-container">
					<c:forEach items="${lotList}" var="list" varStatus="l">
					<li <c:if test="${list.code eq lotCode}">class="tit"</c:if>>
						<h1>
							<a href="${base}/lotteryV3/draw/list.do?lotCode=${list.code}" c="${list.code}">${list.name}</a>
						</h1>
					</li>
					</c:forEach>
				</ul>
			</div>
			<div class="lbot"></div>
		</div>
		<div class="right_box">
			<div class="right_top"></div>
			<div class="right_main">
				<div class="cpinfo">
					<h1>${lotName}开奖信息</h1>
					<p style="margin-top:0px;">
						<input id="startTime" class="text text75" readonly="readonly" value="${startTime}" data-end-date="${time}" type="text" onchange="changeDate(this);">
					</p>
					<p style="margin-top:0px;">选择日期：</p>
					<div class="clear"></div>
				</div>
				<div class="draw_main highfreq_main">
					<div class="draw_left">
						<div class="draw_content draw_lastest">
							<ul class="draw_ul">
								<li class="bg1">
									<h1>
										${lotName}第<b class="red" id="jq_latest_draw_phase"></b>期 <span
											class="right" id="jq_latest_draw_time"></span>
									</h1>
								</li>
								<li class="bg2">
									<div class="ball_box">
										开奖号码：<label id="jq_latest_draw_result"></label>
									</div>
									<div class="clear nospace">&nbsp;</div>
								</li>
							</ul>
						</div>
						<div class="draw_content draw_recent">
							<ul class="draw_ul">
								<li class="bg3">
									<div class="result">
										<span><label id="jq_display_date">${startTime}</label>开奖结果</span>
									</div>
									<div class="more_data" id="jq_data_link">
										<a target="_blank" href="${base}/lotteryV3/draw/hisresult.do?lotCode=${lotCode}"
											class="draw_data">历史数据</a><a target="_blank"
											href="${base}/lotteryV3/trend.do?lotCode=${lotCode}" class="analysis">走势图</a>
									</div>
									<div class="clear nospace">&nbsp;</div>
								</li>
							</ul>
<%--							<style>--%>
<%--								#jq_draw_list tbody tr td:last-child span:not(:nth-last-child(-n+4)):after{--%>
<%--									content: ",";--%>
<%--									/*display: block;*/--%>
<%--									height: 1px;--%>
<%--									width: 1px;--%>
<%--									/*visibility: hidden;*/--%>
<%--								}--%>
<%--							</style>--%>
							<table class="draw_tab" id="jq_draw_list" cellspacing="0"
								cellpadding="0">
								<tbody>
									<tr class="tit">
										<td width="18%">期号</td>
										<td width="15%">开奖日期</td>
										<td width="20%">开奖时间</td>
										<td width="47%">开奖号码</td>
									</tr>
									<c:forEach items="${page}" var="result" varStatus="i">
										<tr <c:if test="${i.index%2!=0}">class="bgcolor"</c:if>>
											<td>${result.qiHao}</td>
											<td><fmt:formatDate value="${result.endTime}" var="date" pattern="yyyy-MM-dd" />${date}</td>
											<td><fmt:formatDate value="${result.endTime}" var="time" pattern="HH:mm:ss" />${time}</td>
											<td>
												<c:set value="0" var="pceggSum"></c:set>
												<c:if test="${not empty result.haoMa }">
													<c:set value="0" var="sum"></c:set>
													<c:set value="${ fn:split(result.haoMa, ',') }" var="arr" />
													<c:forEach items="${arr}"  var="haoMa" varStatus="is">
														<c:choose>
															<c:when test="${result.lotCode eq 'PCEGG'}">
																<c:choose>
																	<c:when test="${haoMa eq '?'}">
																		<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
																		<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">?</span></c:if>
																	</c:when>
																	<c:otherwise>
																		<c:set value="${pceggSum + haoMa}" var="pceggSum"></c:set>
																		<c:if test="${is.index == 0 || is.index == 1}"><span class="ball_1">${haoMa}</span>&nbsp;<b>+</b></c:if>
																		<c:if test="${is.index == 2}"><span class="ball_1">${haoMa}</span>&nbsp;<b>=</b>&nbsp;<span class="ball_2">${pceggSum}</span></c:if>
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:when test="${result.lotCode eq 'SFLHC'}">
																<c:choose>
																	<c:when test="${haoMa == '?'}">
																			<c:if test="${is.index == 6}"><b>+</b>&nbsp;<span class="ball_2">${haoMa}</span></c:if>
																			<c:if test="${is.index != 6 }"><span class="ball_1">${haoMa}</span></c:if>
																	</c:when>
																	<c:otherwise>
																		<c:set var="hong" value="01,02,07,08,12,13,18,19,23,24,29,30,34,35,40,45,46"></c:set>
																		<c:set var="lan" value="03,04,09,10,14,15,20,25,26,31,36,37,41,42,47,48"></c:set>
																		<c:set var="lv" value="05,06,11,16,17,21,22,27,28,32,33,38,39,43,44,49"></c:set>
																		<c:if test="${is.index == 6 }"><b>+</b></c:if>
																		<c:if test="${fn:contains(hong, haoMa)}"><span class="ball_1">${haoMa}</span></c:if>
																		<c:if test="${fn:contains(lan, haoMa)}"><span class="ball_2">${haoMa}</span></c:if>
																		<c:if test="${fn:contains(lv, haoMa)}"><span class="ball_4">${haoMa}</span></c:if>
																	</c:otherwise>
																</c:choose>
																<!-- <font style="display:inline-block;width:20px;height:20px;color:#000">猪</font> -->
															</c:when>
															<c:when test="${result.lotCode eq 'BJSC' || result.lotCode eq 'XYFT'  || result.lotCode eq 'LXYFT'  || result.lotCode eq 'AZXY10' || result.lotCode eq 'SFSC' || result.lotCode eq 'SFFT'}">
																<c:choose>
																	<c:when test="${haoMa eq '?' }">
<%--																		<span class="ball_1">${haoMa}</span>--%>
																		<span class="ball_1">${haoMa}</span>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${is.index == 0 || is.index == 1}">
																			<c:set value="${sum + haoMa}" var="sum"></c:set>
																		</c:if>
																		<span class="pk10 b${haoMa}">${haoMa}</span>
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:otherwise>
																<c:set value="${sum + haoMa}" var="sum"></c:set>
																<span class="ball_1">${haoMa}</span>
															</c:otherwise>
														</c:choose>
													</c:forEach>
													<c:choose>
														<c:when test="${result.lotCode eq 'FFC' || result.lotCode eq 'EFC' || result.lotCode eq 'WFC' || result.lotCode eq 'HNFFC'  || result.lotCode eq 'HNWFC'  || result.lotCode eq 'AZXY5' || result.lotCode eq 'CQSSC' || result.lotCode eq 'TJSSC' || result.lotCode eq 'XJSSC'}">
															= <span style="color:red;">${sum}</span>
															<c:choose>
																<c:when test="${sum>=23}">
																	<span style="padding: 3px;color: #06f502;background: #005ea6;border-radius: 5px;color: #ffffff">大</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #63a73e;border-radius: 5px;color: #ffffff">小</span>
																</c:otherwise>
															</c:choose>
															<c:choose>
																<c:when test="${sum%2 ==0}">
																	<span style="padding: 3px;color: #06f502;background: #ff9600;border-radius: 5px;color: #ffffff">双</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #0082fe;border-radius: 5px;color: #ffffff">单</span>
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:when test="${result.lotCode eq 'BJSC' || result.lotCode eq 'XYFT'  || result.lotCode eq 'LXYFT'  || result.lotCode eq 'AZXY10' || result.lotCode eq 'SFSC' || result.lotCode eq 'SFFT'}">
															= <span style="color:red;">${sum}</span>
															<c:choose>
																<c:when test="${sum>=11}">
																	<span style="padding: 3px;color: #06f502;background: #005ea6;border-radius: 5px;color: #ffffff">大</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #63a73e;border-radius: 5px;color: #ffffff">小</span>
																</c:otherwise>
															</c:choose>
															<c:choose>
																<c:when test="${sum%2==0}">
																	<span style="padding: 3px;color: #06f502;background: #ff9600;border-radius: 5px;color: #ffffff">双</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #0082fe;border-radius: 5px;color: #ffffff">单</span>
																</c:otherwise>
															</c:choose>
<%--															<span style="color:#000;">${sum>11?'大':'小'}</span>--%>
<%--															<span style="color:#000;">${sum%2==0?'双':'单'}</span>--%>
														</c:when>
														<c:when test="${result.lotCode eq 'JSSB3' || result.lotCode eq 'GSK3' || result.lotCode eq 'BJK3' || result.lotCode eq 'HBK3' || result.lotCode eq 'JXK3' || result.lotCode eq 'AHK3'
													  || result.lotCode eq 'SHHK3' || result.lotCode eq 'FFK3' || result.lotCode eq 'WFK3' || result.lotCode eq 'JPK3' || result.lotCode eq 'KRK3' || result.lotCode eq 'HKK3' || result.lotCode eq 'AMK3' || result.lotCode eq 'HEBK3' || result.lotCode eq 'GXK3' || result.lotCode eq 'GZK3' || result.lotCode eq 'JLK3' || result.lotCode eq 'SFK3' || result.lotCode eq 'ESK3' || result.lotCode eq 'TMK3'}">
															= <span style="color:red">${sum}</span>
															<c:choose>
																<c:when test="${sum>=11}">
																	<span style="padding: 3px;color: #06f502;background: #005ea6;border-radius: 5px;color: #ffffff">大</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #63a73e;border-radius: 5px;color: #ffffff">小</span>
																</c:otherwise>
															</c:choose>
															<c:choose>
																<c:when test="${sum%2 ==0}">
																	<span style="padding: 3px;color: #06f502;background: #ff9600;border-radius: 5px;color: #ffffff">双</span>
																</c:when>
																<c:otherwise>
																	<span style="padding: 3px;color: #06f502;background: #0082fe;border-radius: 5px;color: #ffffff">单</span>
																</c:otherwise>
															</c:choose>
<%--															<span style="color:#000;">${sum>10?'大':'小'}</span>--%>
<%--															<span style="color:#000;">${sum%2==0?'双':'单'}</span>--%>
														</c:when>
													</c:choose>
												</c:if>
												<c:if test="${empty result.haoMa}">--</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="draw_right">
						<div class="column_box none_margin_top">
							<div class="column_title_b">
								<p class="draw_phase">
									第 <strong class="red" id="jq_cur_phase">161229018</strong> 期
									<a target="_blank" href="${base}/lotteryV3/lotDetail.do?lotCode=${lotCode}" class="button_top"></a>
								</p>
								<p class="draw_countdown">
									投注截止：<label id="jq_cur_countdown"></label>
									<a href="#" class="button_bottom"></a>
								</p>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="right_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('#startTime').cxCalendar({format : 'YYYY-MM-DD',position:'bottomLeft'});
		})
		
		function changeDate($this){
			window.location.href= "${base}/lotteryV3/draw/list.do?lotCode=${lotCode}&start="+$this.value;
		}
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>