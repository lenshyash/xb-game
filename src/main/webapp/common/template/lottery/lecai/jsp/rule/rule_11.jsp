<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="lecai_v5">
	<c:if test="${param.code == 'PCEGG'}">
	<div class="PCEGGOPEN">
		<strong>PC蛋蛋开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">
						PC蛋蛋</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:00—23:55(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-179</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> PC蛋蛋具体游戏规则如下︰</strong><br>
		<h2>游戏规则:</h2>
		<dd>
			<ul>
				<li>按北京快乐8的开奖结果为基础；</li>
				<li>将当期第三方开奖数据从小到大进行排序；</li>
				<li>排序后,分别按号码第 1~6 , 7~12 , 13~18 分为【1】【2】【3】三个区；</li>
				<li>把每个区的数字分别相加；</li>
				<li>三个数值相加即为幸运28最终的开奖结果。</li>
			</ul>
		</dd>
	</div>
	</c:if>
	<c:if test="${param.code == 'JND28'}">
	<div class="JND28OPEN">
		<strong>加拿大28开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">
						开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">
						加拿大28</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">不固定</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每3分半钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 加拿大28具体游戏规则如下︰</strong><br>
		<h2>游戏规则:</h2>
	
		<dd>
			<ul>
				<li>加拿大28采用加拿大彩票公司BCLC的开奖结果为基础；</li>
				<li>【BCLC官网：lotto.bclc.com】。每三分半一期，开奖时间全天。(每天维护时间为19:00~21:30，周一可能会有延迟)</li>
				<li>将当期第三方开奖数据从小到大进行排序；</li>
				<li>排序后,分别按号码第一区[第2/5/8/11/14/17位数字]
					8,17,34,42,58,69，第二区[第3/6/9/12/15/18位数字]
					14,22,39,48,63,72，第三区[第4/7/10/13/16/19位数字] 16,26,41,54,64,73</li>
				<li>把每个区的数字分别相加；</li>
				<li>三个数值相加即为加拿大28最终的开奖结果。</li>
			</ul>
		</dd>
	</div>
	</c:if>
	<h2>投注规则:</h2>
		<c:if test="${!pceggDrawShow  }">
				<style>
					.hidePcegg{
						display:none;
					}
				</style>
			</c:if>
	<dd>
		<ul>
			<li>大：三个位置的数值相加和大于等于14，15,16,17,18,19,20,21,22,23,24,25,26，27为大。
				<span class="hidePcegg">举例出14押大回本本金。</span></li>
			<li>小：三个位置的数值相加和小于等于0，01,02,03,04,05,06,07,08,09,10,11,12，13为小。
				<span class="hidePcegg">举例出13押小回本本金。</span></li>
			<li>单：三个位置的数值相加和尾数为单时就为单。<span class="hidePcegg">举例出13押单回本本金。</span></li>
			<li>双：三个位置的数值相加和尾数为双时就为双。<span class="hidePcegg">举例出14押双回本本金。</span></li>
			<li>大单（三个数值和）：15,17,19,21,23,25,27为大单。</li>
			<li>小单（三个数值和）：01,03,05,07,09,11,13为小单。<span class="hidePcegg">举例买100元小单开13就中奖。</span></li>
			<li>大双（三个数值和）：14,16,18,20,22,24,26为大双。<span class="hidePcegg">举例买100元大双开14就中奖。</span></li>
			<li>小双（三个数值和）：00,02,04,06,08,10,12为小双。</li>
			<li>极大（三个数值和）：22,23,24,25,26,27为极大。</li>
			<li>极小（三个数值和）：00,01,02,03,04,05为极小。</li>
			<li>红波（三个数值和）：03,06，09,12,15,18,21,24为红波。</li>
			<li>绿波（三个数值和）：01,04,07,10,16,19,22，25为绿波。</li>
			<li>蓝波（三个数值和）：02,05,08,11,17,20,23,26为蓝波。</li>
			<li>购买波色如果三个数值和为0,13,14,27视为不中奖</li>
			<li>特码包三（三个数值和）：可選取三個數字投注，選取三個數字當中任何一個與開獎結果相同即中獎，（比如:开奖结果 15 选择
				11 15 20,那就视为中奖）。</li>
			<li>特码（三个数值和）：單選取一個數字投注。</li>
			<li>豹子：當期開出三個數字相同即為豹子（ 例
				0+0+0=0,1+1+1=3,2+2+2=6,3+3+3=9,4+4+4=12,5+5+5=15,6+6+6=18,7+7+7=21,8+8+8=24，9+9+9=27）。
			</li>
			<li class="mubanRule">若开奖特别号码13、14大小单双赔率1.85倍、组合小单大双2.5倍（都是含本金的）</li>
			<script>
				if(location.host == '8880cq.com' || location.host == '08877.cc'){
					$('.mubanRule').show()
				}else{
					$('.mubanRule').hide()
				}
			</script>
		</ul>
	</dd>
</div>