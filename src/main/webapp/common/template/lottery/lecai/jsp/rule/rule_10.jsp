<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="lecai_v5">
	<c:if test="${param.code == 'JSSB3'}">
	<div class="JSSB3OPEN">
		<strong>江苏骰宝（快3）开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">江苏骰宝（快3）</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						08:50—22:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司江苏骰宝（快3）具体游戏规则如下︰</strong><br>
		<h2>江苏骰宝（快3）</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'AHK3'}">
	<div class="AHK3OPEN">
		<strong>安徽快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">安徽快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:00—22:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-40</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司安徽快三具体游戏规则如下︰</strong><br>
		<h2>安徽快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'SHHK3'}">
	<div class="SHHK3OPEN">
		<strong>上海快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">上海快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						08:58-22:18(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司上海快三具体游戏规则如下︰</strong><br>
		<h2>上海快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'HBK3'}">
	<div class="HBK3OPEN">
		<strong>湖北快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">湖北快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:20—22:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-39</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司湖北快三具体游戏规则如下︰</strong><br>
		<h2>湖北快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'HEBK3'}">
	<div class="HEBK3OPEN">
		<strong>河北快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">河北快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						08:50-22:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-41</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司河北快三具体游戏规则如下︰</strong><br>
		<h2>河北快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'GXK3'}">
	<div class="GXK3OPEN">
		<strong>广西快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">广西快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:30-22:30(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-40</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司广西快三具体游戏规则如下︰</strong><br>
		<h2>广西快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'GZK3'}">
	<div class="GZK3OPEN">
		<strong>贵州快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">贵州快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:30-22:10(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-39</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司贵州快三具体游戏规则如下︰</strong><br>
		<h2>贵州快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'GSK3'}">
	<div class="GSK3OPEN">
		<strong>甘肃快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">甘肃快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						10:19~21:59(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-36</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司甘肃快三具体游戏规则如下︰</strong><br>
		<h2>甘肃快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'BJK3'}">
	<div class="BJK3OPEN">
		<strong>北京快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">北京快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:22-23:42(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-44</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司北京快三具体游戏规则如下︰</strong><br>
		<h2>北京快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'JXK3'}">
	<div class="JXK3OPEN">
		<strong>江西快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">江西快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						09:15~22:55(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-42</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司江西快三具体游戏规则如下︰</strong><br>
		<h2>江西快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'FFK3'}">
	<div class="FFK3OPEN">
		<strong>极速快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">极速快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">0001-1440</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每1分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司极速快三具体游戏规则如下︰</strong><br>
		<h2>极速快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'WFK3'}">
	<div class="WFK3OPEN">
		<c:choose>
			<c:when test="${domainFolder == 'd00561'}">
				<strong>福彩快三开奖时间：</strong>
			</c:when>
			<c:otherwise>
				<strong>幸运快三开奖时间：</strong>
			</c:otherwise>
		</c:choose>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">幸运快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司幸运快三具体游戏规则如下︰</strong><br>
		<h2>幸运快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'JPK3'}">
	<div class="JPK3OPEN">
		<strong>日本快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">日本快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司日本快三具体游戏规则如下︰</strong><br>
		<h2>日本快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'KRK3'}">
	<div class="KRK3OPEN">
		<strong>韩国快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">韩国快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-288</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每5分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司韩国快三具体游戏规则如下︰</strong><br>
		<h2>韩国快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'SFK3'}">
	<div class="SFK3OPEN">
		<strong>十分快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">十分快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						08:54-22:34(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-42</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每10分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司十分快三具体游戏规则如下︰</strong><br>
		<h2>十分快三</h2>
	</div>
	</c:if>
	<c:if test="${param.code == 'ESK3'}">
	<div class="ESK3OPEN">
		<strong>二十分快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">二十分快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						9:00-22:00(北京时间)</td>
					<td class="point" bgcolor="#FFF7F0" align="center">01-39</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每20分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司二十分快三具体游戏规则如下︰</strong><br>
		<h2>二十分快三</h2>
	</div>
	</c:if>
	
		<c:if test="${param.code == 'TMK3'}">
	<div class="TMK3OPEN">
		<strong>三分快三开奖时间：</strong>
		<table class="awardList">
			<tbody>
				<tr>
					<th class="subtitle2" width="25%" valign="top" bgcolor="#AFAFE4"
						align="center">游戏项目</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖时间</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">每日期数</th>
					<th class="subtitle2" width="25%" bgcolor="#AFAFE4" align="center">开奖频率</th>
				</tr>
				<tr>
					<td class="point" valign="middle" bgcolor="#FFF7F0" align="center">三分快三</td>
					<td class="point" bgcolor="#FFF7F0" align="center">
						全天24小时不间断</td>
					<td class="point" bgcolor="#FFF7F0" align="center">001-480</td>
					<td class="point" bgcolor="#FFF7F0" align="center">每3分钟</td>
				</tr>
			</tbody>
		</table>
		<br> <strong> 本公司三分快三具体游戏规则如下︰</strong><br>
		<h2>三分快三</h2>
	</div>
	</c:if>
	
	<dl>
	<c:if test="${!k3baoziDrawShow  }">
				<style>
					.hideKsan{
						display:none;
					}
				</style>
			</c:if>
			<script>
				console.log('${k3baoziDrawShow  }')
			</script>
		<dt>◎双面</dt>
		<dd>
			<ul>
						<li>以全部开出的三个号码、加起来的总和来判定。</li>
						<li>大小：三个开奖号码总和值11~17 为大；总和值4~10 为小；<span class="hideKsan">若三个号码相同、则不算中奖。</span></li>
						<li>单双：三个开奖号码总和值5,7,9,11,13,15,17为单；总和值4,6,8,10,12,14,16为双；<span class="hideKsan">若三个号码相同、则不算中奖。</span>
						</li>
					</ul>
		</dd>
		<dt>◎三军</dt>
		<dd>
			<ul>
				<li>三个开奖号码其中一个与所选号码相同时、即为中奖。举例：如开奖号码为1、1、3，则投注三军1或三军3皆视为中奖。 <font
					color="red">备注：不论当局指定点数出现几次，仅派彩一次(不翻倍)。 </font>
				</li>
			</ul>
		</dd>
		<dt>◎围骰/全骰</dt>
		<dd>
			<ul>
				<li>围骰：开奖号码三字同号、且与所选择的围骰组合相符时，即为中奖。</li>
				<li>全骰：全选围骰组合、开奖号码三字同号，即为中奖。</li>
			</ul>
		</dd>
		<dt>◎点数</dt>
		<dd>
			<ul>
				<li>开奖号码总和值为4、5、6、7、8、9、10、11、12、13、14、15、16、17
					时，即为中奖；若开出3、18，则不算中奖。举例：如开奖号码为1、2、3、总和值为6、则投注「6」即为中奖。</li>
			</ul>
		</dd>
		<dt>◎长牌</dt>
		<dd>
			<ul>
				<li>

					任选一长牌组合、当开奖结果任2码与所选组合相同时，即为中奖。举例：如开奖号码为1、2、3、则投注长牌12、长牌23、长牌13皆视为中奖。

				</li>
			</ul>
		</dd>
		<dt>◎短牌</dt>
		<dd>
			<ul>
				<li>开奖号码任两字同号、且与所选择的短牌组合相符时，即为中奖。举例：如开奖号码为1、1、3、则投注短牌1、1，即为中奖。</li>

			</ul>

		</dd>
		<dt>◎豹子</dt>
		<dd>
			<ul>
				<li>豹子：开奖的三个骰子点数相同为豹子，选择指定点数的豹子与开奖结果一致则中奖<span class="hideKsan">，其它不中奖</span>。</li>
				<li>如：投注豹子：666，开奖：666 则中奖<span class="hideKsan">，其它不中奖</span>。</li>

			</ul>

		</dd>
	</dl>
</div>
