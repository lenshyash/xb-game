<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="wrap bread">您当前所处的位置： 积分兑换</div>
	<div class="wrap">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="grid fixed c21 main">
			<div class="ordermain">
				<div class="navbox">
					<div class="prizetitle clearfix">
						<span class="float-left">积分兑换</span> <span
							class="span-kf float-right">如有疑问请咨询客服电话：${kfPhone}</span>
					</div>
				</div>
				<c:if test="${isGuest }">
				<div class="userinfo-edit-main">试玩账号不能兑换积分</div>
				</c:if>
				<c:if test="${!isGuest }">
				<div class="ucmain ucmainPadding">
					<table class="card_tab" cellspacing="0" cellpadding="0" border="0"
						width="100%">
						<thead>
							<tr class="bgcolor">
								<th width="115">类型</th>
								<th>余额</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<tr class="centers bgcolor">
								<td>现金</td>
								<td><span id="mnyCredit">0.00</span>&nbsp;&nbsp;${not empty cashName?cashName:'RMB'}</td>
								<td rowspan="2"><a href="#" onclick="initUserData();"
									class="buybtn">刷新</a></td>
							</tr>
							<tr class="centers bgcolor">
								<td>积分</td>
								<td id="scoreCredit">0.00</td>
							</tr>
							<tr>
								<td colspan="3">
									<ul>
										<li>兑换类型： <select id="exchangeType" style="width: 150px;">
												<option selected="selected" value="">----请选择类型----</option>
												<option value="1">现金兑换积分</option>
												<option value="2">积分兑换现金</option>
										</select> <span id="exchange_remark_span" style="margin-left: 5px;"></span></li>
										<li>兑换额度：<input value="" class="text-input" id="amount"
											name="amount" type="text"></li>
										<li class="exchangeBtn"><a onclick="exchange();"
											id="excBtn" href="javascript://"></a></li>
										<li class="gray">注：系统余额可用于彩票投注————积分可用于参与站点活动</li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</div></c:if>
			</div>
		</div>
	</div>
	<c:if test="${!isGuest }"><script type="text/javascript">
		$(function() {
			$("#exchangeType").chosen({
				disable_search : true
			});
			initUserData();
			getTypes();
			bindTypeChange();
			bindValuekeyup();
			$("#excBtn").removeAttr("disabled");
		})

		var types = [];
		var selectedType = {};
		function initUserData() {
			$.ajax({
				url : "${base}/center/banktrans/exchange/udata.do",
				success : function(result) {
					$("#mnyCredit").html(toDecimal2(result.money));
					$("#scoreCredit").html(result.score);
				}
			});
		}
		function getTypes() {
			$.ajax({
				url : "${base}/center/banktrans/exchange/exctypes.do",
				success : function(result) {
					types = result;
					if ($("#exchangeType").val()) {
						selectedType = getExcTypeByType($("#exchangeType")
								.val());
					}
				}
			});
		}

		function getExcTypeByType(val) {
			if (types) {
				for (var i = 0; i < types.length; i++) {
					if (val == types[i].type) {
						return types[i];
					}
				}
			}
			return null;
		}

		function bindTypeChange() {
			$("#exchangeType").change(
					function() {
						var selval = $(this).children('option:selected').val();
						var selHtml = $(this).children('option:selected')
								.html().split("兑换");
						if (selval > 0) {
							selectedType = getExcTypeByType(selval);
							if (selectedType) {
								$("#exchange_remark_span").html(
										selectedType.numerator + selHtml[0]
												+ "可兑换"
												+ selectedType.denominator
												+ selHtml[1] + "(兑换比例："
												+ selectedType.numerator + ":"
												+ selectedType.denominator
												+ ")");
								if ($("#amount").val()) {
									$("#amount").keyup();
								}
							} else {
								$("#exchange_remark_span").html("暂时未开放！");
							}
						} else {
							$("#exchange_remark_span").html("");
						}
					});

		}

		function bindValuekeyup() {
			$("#amount").keyup(
					function() {
						var typeVal = $("#exchangeType").val();
						if (!typeVal) {
							layer.msg("请选择兑换类型！", {
								icon : 2
							});
							return;
						}
						var selval = $(this).val();
						if (isNaN(selval)) {
							$(this).val("");
							return;
						}
						selval = Math.abs(selval);
						$(this).val(selval);
						if (selectedType != {}) {
							var result = 0;
							var typeName = "";
							if (selectedType.type == 1) {
								result = Math.floor(selval
										* selectedType.denominator
										/ selectedType.numerator);
								typeName = "积分";
							} else if (selectedType.type == 2) {
								result = Math.round(selval
										* selectedType.denominator * 100
										/ selectedType.numerator) / 100;
								typeName = "现金";
							}
							$("#exchange_result_span").html(
									"兑换可获得" + result + typeName);
						} else {
							$("#exchange_result_span").html("暂时未开放！");
						}
					});
		}

		function exchange() {
			$("#excBtn").attr("disabled", "disabled");
			if (!$("#exchangeType").val()) {
				layer.msg("请选择兑换类型！", {
					icon : 2
				});
				$("#excBtn").removeAttr("disabled");
				return;
			}

			var amount = $("#amount").val();

			if (!amount) {
				layer.msg("额度不能为空！", {
					icon : 2
				});
				$("#excBtn").removeAttr("disabled");
				return;
			}

			if (amount > selectedType.maxVal) {
				layer.msg("额度必须小等于" + selectedType.maxVal, {
					icon : 2
				});
				$("#excBtn").removeAttr("disabled");
				return;
			}

			if (amount < selectedType.minVal) {
				layer.msg("额度必须大等于" + selectedType.minVal, {
					icon : 2
				});
				$("#excBtn").removeAttr("disabled");
				return;
			}

			$.ajax({
				url : "${base}/center/banktrans/exchange/exchange.do",
				data : {
					typeId : selectedType.id,
					amount : amount
				},
				success : function(result) {
					if (!result.success) {
						layer.msg(result.msg, {
							icon : 2
						});
						return;
					} else {
						layer.alert("兑换成功", {
							icon : 1,
							offset : [ '30%' ]
						}, function(index) {
							initUserData();
							$("#excBtn").removeAttr("disabled");
							layer.close(index);
						})
					}
				}
			});
		}
	</script></c:if>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>