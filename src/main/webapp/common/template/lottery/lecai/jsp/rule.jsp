<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>玩法规则说明-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/rule.css?v=${caipiao_version}"
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<style type="text/css">
	.lecai_v5 .awardList {
	    width: 100%;
	    border-color: #ba2636 #ddd #ddd;
	    font-size: 12px;
	    margin-bottom: -1px;
	    text-align: center;
	}
	.lecai_v5 .awardList tr th,.lecai_v5 .awardList tr td {
	    font-size: 12px!important;
	    padding: 4px;
	}
	.lecai_v5 .awardList th {
	    line-height: 30px;
	    background: #dff3ff;
	    color: #717171;
	    text-align: center;
	    font-weight: bold;
	}
	.lecai_v5 ul li {
	    line-height: 25px;
	    font-size: 13px;
	}
	.lecai_v5 h2 {
	    margin-top: 10px;
	    font-weight: bold;
	}
	.lecai_v5 h2 {
	    border-top: 3px solid #ba2636;
	    margin: 0;
	    padding: 0 0 0 23px;
	    font-size: 12px;
	    color: #717171;
	    font-weight: normal;
	    background: #dcdcdc left no-repeat;
	    height: 30px;
	    line-height: 30px;
	    display: block;
	}
</style>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<div class="rule-box clearfix">
		<div id="left" class="rule-left">

			<div class="lbar">
				<span>玩法规则</span>
			</div>
			<div class="cznav" id="nav_sort" style="display: block;">
				<ul class="item js-lottery-container">
					<c:forEach items="${lotList}" var="list" varStatus="l">
						<li <c:if test="${list.code eq code}">class="tit"</c:if>>
							<h1>
								<a href="${base}/lotteryV3/rule.do?code=${list.code}"
									c="${list.code}">${list.name}</a>
							</h1>
						</li>
					</c:forEach>
				</ul>
			</div>
			<div class="lbot"></div>
		</div>
		<div class="rule-right">
			<div class="rtop"></div>
			<div id="rcont">
				<div class="rbar">
					<h1>
						<span
							style="background: url('${base}/common/template/lottery/lecai/images/gclogo/${code}.png') 20px 9px  no-repeat; padding-left:70px;background-size:32px;"><font
							id="lotName" color="red">北京赛车</font>游戏规则</span>
					</h1>
				</div>
				<c:if test="${lotVersion != 5 }">
				<div class="ruleContent">
					<strong>快捷导航：</strong>
					<div class="trend-content">
						<c:choose>
							<c:when test="${code eq 'LHC' || code eq 'SFLHC' || code eq 'TMLHC'|| code eq 'WFLHC'|| code eq 'HKMHLHC'}">
								<span><a href="#zysm">重要声明</a></span>
								<span><a href="#tbhmpsgzsm">特别号码盘势规则说明</a></span>
								<span><a href="#zhengma">正码</a></span>
								<span><a href="#zhengtema">正特码</a></span>
								<span><a href="#zonghedanshuang">总合单双</a></span>
								<span><a href="#zonghedaxiao">总合大小</a></span>
								<span><a href="#shengxiaosebo">生肖色波</a></span>
								<span><a href="#yixiao">一肖</a></span>
								<span><a href="#hexiao">合肖</a></span>
								<span><a href="#banbo">半波</a></span>
								<span><a href="#zhengxiao">正肖</a></span>
								<span><a href="#qisebo">七色波</a></span>
								<span><a href="#tematoushu">特码头数</a></span>
								<span><a href="#temaweishu">特码尾数</a></span>
								<span><a href="#zhengteweishu">正特尾数</a></span>
								<span><a href="#zongxiao">总肖</a></span>
								<span><a href="#lianxiao">连肖</a></span>
								<span><a href="#lianwei">连尾</a></span>
								<span><a href="#zixuanbuzhong">自选不中</a></span>
							</c:when>
							<c:otherwise>
								<span><a href="#jj">简介</a></span>
								<c:forEach items="${rulePlayList}" var="play" varStatus="p">
									<c:if test="${fn:indexOf(play.code,'_ds') == -1}">
										<c:if test="${code ne 'LHC' && code ne 'SFLHC' && code ne 'TMLHC' && code ne 'WFLHC' && code ne 'HKMHLHC'}">
											<span><a href="#${play.code}">${play.name}</a></span>
										</c:if>
									</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				</c:if>
				<jsp:include page="rule/rule_${type}.jsp">
					<jsp:param value="${code}" name="code"/>
				</jsp:include>
			</div>
			<div class="rcon">
				<span><a href="#">回到顶部</a></span>
			</div>
			<div class="rbot"></div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			var n = $('#nav_sort ul li.tit').text();
			$("#lotName").text(n.trim());
		})
	</script>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
</body>
</html>