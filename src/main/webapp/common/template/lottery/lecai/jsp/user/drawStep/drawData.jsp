<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../../include/baseScript.jsp"></jsp:include>
<div class="userinfo-edit-main">
	<div class="for-liucheng">
		<div class="liulist for-cur"></div>
		<div class="liulist for-cur"></div>
		<div class="liulist"></div>
		<div class="liulist"></div>
		<div class="liutextbox">
			<div class="liutext for-cur">
				<em>1</em><br> <strong>设置取款密码</strong>
			</div>
			<div class="liutext for-cur">
				<em>2</em><br> <strong>完善出款银行信息</strong>
			</div>
			<div class="liutext">
				<em>3</em><br> <strong>取款</strong>
			</div>
			<div class="liutext">
				<em>4</em><br> <strong>完成</strong>
			</div>
		</div>
	</div>
	<div class="tab-user">
		<table class="userTab" width="100%" cellspacing="0" cellpadding="0"
			border="0">
			<tbody>
				<tr>
					<td></td>
					<td colspan="2" style="text-align: left;">
						标记有<font color="red"> * </font>者为必填项目<br>
					</td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>真实姓名：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="accountname" type="text" value="${userInfo1.userName}" <c:if test="${not empty userInfo1.userName }">disabled="disabled"</c:if> /></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>出款银行：</td>
					<td class="tdinput tdinput-width"><select id="bankName" style="width:200px;">
							<option value="1" selected="selected">手动填写</option>
							<option value="建设银行">建设银行</option>
							<option value="工商银行">工商银行</option>
							<option value="农业银行">农业银行</option>
							<option value="中国邮政银行">中国邮政银行</option>
							<option value="中国银行">中国银行</option>
							<option value="中国招商银行">中国招商银行</option>
							<option value="中国交通银行">中国交通银行</option>
							<option value="中国民生银行">中国民生银行</option>
							<option value="中信银行">中信银行</option>
							<option value="中国兴业银行">中国兴业银行</option>
							<option value="浦发银行">浦发银行</option>
							<option value="平安银行">平安银行</option>
							<option value="华夏银行">华夏银行</option>
							<option value="广州银行">广州银行</option>
							<option value="BEA东亚银行">BEA东亚银行</option>
							<option value="广州农商银行">广州农商银行</option>
							<option value="顺德农商银行">顺德农商银行</option>
							<option value="北京银行">北京银行</option>
							<option value="杭州银行">杭州银行</option>
							<option value="温州银行">温州银行</option>
							<option value="上海农商银行">上海农商银行</option>
							<option value="中国光大银行">中国光大银行</option>
							<option value="渤海银行">渤海银行</option>
							<option value="浙商银行">浙商银行</option>
							<option value="晋商银行">晋商银行</option>
							<option value="汉口银行">汉口银行</option>
							<option value="上海银行">上海银行</option>
							<option value="广发银行">广发银行</option>
							<option value="深圳发展银行">深圳发展银行</option>
							<option value="东莞银行">东莞银行</option>
							<option value="宁波银行">宁波银行</option>
							<option value="南京银行">南京银行</option>
							<option value="北京农商银行">北京农商银行</option>
							<option value="重庆银行">重庆银行</option>
							<option value="广西农村信用社">广西农村信用社</option>
							<option value="吉林银行">吉林银行</option>
							<option value="江苏银行">江苏银行</option>
							<option value="成都银行">成都银行</option>
							<option value="尧都区农村信用联社">尧都区农村信用联社</option>
							<option value="浙江稠州商业银行">浙江稠州商业银行</option>
							<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
					</select>
					<input type="text" id="bankNameInputId" class="text" placeholder="请填写银行名称"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>省份：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="province" type="text"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>城市：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="city" type="text"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title">开户网点：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="bankcardaddress" type="text"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>银行帐号：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="bankId" type="text" value="${userInfo1.cardNo}" <c:if test="${not empty userInfo1.cardNo}">disabled="disabled"</c:if>></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="idcard-tr">
					<td class="title"><font color="red">*</font>提款密码：</td>
					<td class="tdinput tdinput-width"><input class="text"
						id="cashPassword" type="password"></td>
					<td class="tdtip"><div>
							<span class="red"></span>
						</div></td>
				</tr>
				<tr class="paypwd-tr paypwd-tr-input paypwd-tr-new">
					<td class="title"></td>
					<td colspan="2" class="tdinput"><div class="nextBtn"
							style="margin-top: 10px;">
							<a href="javascript:void(0);" onclick="upData()" id="sub-btn"></a>
						</div></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		//select
		$("#bankName").chosen({disable_search:true});
		$("#bankName").change(function(){
			var v=$(this).val();
			if(v==1){
				$("#bankNameInputId").show();
			}else{
				$("#bankNameInputId").hide();
			}
		});
		$(".userTab input").on("focus", function() {
			$(".userTab tr").removeClass("tr-clicked");
			$(this).removeClass("text-error").parent().parent().addClass("tr-clicked");
			$(this).parent().siblings('.tdtip').find('span').text("");
		}).on("blur", function() {
			$(this).parent().parent().removeClass("tr-clicked");
		})
	})
	
	
	var checkflag = false;
	function upData() {
		if (checkSubmit()) {
			if (!checkflag) {
				checkflag = true;
				submit();
			} else {
				layer.alert('请勿重复提交',{icon:2,offset:['30%']});
			}
		}
	}
	
	function submit() {
		var cashBankname = $("#bankName").val();
		var sheng = $("#province").val();
		var city = $("#city").val();
		var bankcardaddress = $("#bankcardaddress").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var userName = $("#accountname").val();
		if(cashBankname=="1"){
			cashBankname=$("#bankNameInputId").val();
		}

		var param = {};
		param["bankName"] = cashBankname;
		param["userName"] = userName;
		param["province"] = sheng;
		param["city"] = city;
		param["bankAddress"] = bankcardaddress;
		param["cardNo"] = cashBankaccount;
		param["repPwd"] = cashPassword;
		$.ajax({
					url : "${base}/center/banktrans/draw/cmitbkinfo.do",
					data : param,
					type : 'POST',
					success : function(result) {
						if(!result.success){
							layer.msg(result.msg,{icon:2});
						}else{
							layer.alert('绑定成功啦，请点击确认开始提款！！',{icon:1,offset:['30%']},function(index){
								window.location.reload();
								layer.close(index);
							})
						}
					},
					complete:function(){
						checkflag = false;
					}
				});
	}

	function checkSubmit() {
		var account  = $("#accountname").val();
		var cashBankname = $("#bankName").val();
		var cashBankaccount = $("#bankId").val();
		var cashBankprovince = $("#province").val();
		var cashBankCity = $("#city").val();
		var cashPassword = $("#cashPassword").val();
		var exp = /^([1-9][\d]{1,18}|x|X)$/;
		if(!account){
			message($("#accountname"),'真实姓名不能为空');
			return false;
		}
		if(cashBankname=="1"){
			cashBankname=$("#bankNameInputId").val();
		}

		if (!cashBankname) {
			$("#bankName").parent().siblings('.tdtip').find('span').text('开户银行不能为空');
			return false;
		}
		if (!cashBankprovince){
			$("#province").parent().siblings('.tdtip').find('span').text('请输入省份信息');
			// message($("#bankId"),'请输入省份信息')
			return false;
		}
		if (!cashBankCity){
			$("#city").parent().siblings('.tdtip').find('span').text('请输入城市信息');
			// message($("#bankId"),'请输入城市信息')
			return false;
		}
		if (!cashBankaccount) {
			message($("#bankId"),'银行账号不能为空');
			return false;
		}
		if (!cashBankaccount || !exp.test(cashBankaccount)) {
			message($("#bankId"),'请输入正确的银行帐号');
			return false;
		}

		if (!cashPassword) {
			message($("#cashPassword"),'取款密码不能为空');
			return false;
		}
		return true;
	}
	
	function message($id,msg){
		$id.addClass('text-error').parent().siblings('.tdtip').find('span').text(msg);
	}
</script>