<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="dsLotto-result dsLotto-result--no5 dsLotto-result--no7 status--on">
	<ul class="_group array cf">
		<li class="_item" style="margin-left: 0">
			<div class="_ele-number ng-binding" id="last_result_0">0</div>
			<div class="_ele-unknown" style="animation-delay: 100ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">百</div>
		</li>
		<!-- / item -->
		<li class="_item _item_add">
			<div class="_ele-number_add ng-binding" >+</div>
		</li>
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_1">0</div>
			<div class="_ele-unknown" style="animation-delay: 200ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">十</div>
		</li>
		<li class="_item _item_add">
			<div class="_ele-number_add ng-binding">+</div>
		</li>
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_2">0</div>
			<div class="_ele-unknown" style="animation-delay: 300ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
			</div>
			<div class="_ele-mark">个</div>
		</li>
		<li class="_item _item_add">
			<div class="_ele-number_add ng-binding">=</div>
		</li>
		<!-- / item -->
		<li class="_item">
			<div class="_ele-number ng-binding" id="last_result_sum">0</div>
			<div class="_ele-unknown" style="animation-delay: 400ms">
				<ul>
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
					<li>10</li>
					<li>11</li>
					<li>12</li>
					<li>13</li>
					<li>14</li>
					<li>15</li>
					<li>16</li>
					<li>17</li>
					<li>18</li>
					<li>19</li>
					<li>20</li>
					<li>21</li>
					<li>22</li>
					<li>23</li>
					<li>24</li>
					<li>25</li>
					<li>26</li>
					<li>27</li>
				</ul>
			</div>
			<div class="_ele-mark">总</div>
		</li>
		<!-- / item -->
	</ul>
	<!-- / group -->
</div>