<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<strong class="p1" id="jj">简介：</strong>
<c:if test="${param.code == 'FC3D'}">
<p class="p1">
	福彩3D以一个3位自然数为投注号码的彩票，投注者从000-999的数字中选择一个3位数进行投注，包括直选、组选、大小单双等多种玩法。</p>
</c:if>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">福彩3D</td>
			<td bgcolor="#FFFFFF">每晚21:15(北京时间)</td>
			<td bgcolor="#FFFFFF">1</td>
			<td bgcolor="#FFFFFF">24小时</td>
		</tr>
	</tbody>
</table>
<c:if test="${param.code == 'PL3'}">
<p class="p1">
	排列三以一个3位自然数为投注号码的彩票，投注者从000-999的数字中选择一个3位数进行投注，包括直选、组选、大小单双等多种玩法。</p>
</c:if>
<table class="s-table" cellspacing="1" cellpadding="0" border="0"
	bgcolor="#CCCCCC">
	<thead>
		<tr>
			<td bgcolor="#eFeFeF" width="25%">彩种名称</td>
			<td bgcolor="#eFeFeF" width="24%">开奖时间</td>
			<td bgcolor="#eFeFeF" width="18%">每日期数</td>
			<td bgcolor="#eFeFeF" width="33%">开奖频率</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF" align="center">排列三</td>
			<td bgcolor="#FFFFFF">每晚20:30(北京时间)</td>
			<td bgcolor="#FFFFFF">1</td>
			<td bgcolor="#FFFFFF">24小时</td>
		</tr>
	</tbody>
</table>
<p class="p2">
	<br />
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dwd">定位胆</strong>
<p class="p1">从百位、十位、个任意位置上任意选择1个或1个以上号码。</p>
<p class="p1">&nbsp;投注方案：百位1</p>
<p class="p1">开奖号码：百位1，即中定位胆百位。</p>
<p class="p1">
	&nbsp;从百位、十位、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dxds_q2">前二</strong>
<p class="p1">从百位、十位中的“大、小、单、双”中至少各选一个组成一注。</p>
<p class="p1">投注方案：大单</p>
<p class="p1">开奖号码：百位与十位“大单”，即中前二大小单双。</p>
<p class="p1">
	对百位、十位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="dxds_h2">后二</strong>
<p class="p1">&nbsp;从十位、个位中的“大、小、单、双”中至少各选一个组成一注。</p>
<p class="p1">&nbsp;投注方案：大单</p>
<p class="p1">开奖号码：十位与个位“大单”，即中后二大小单双</p>
<p class="p1">
	对十位、个位的“大（56789）小（01234）、单（13579）双（02468）”型态进行购买，所选号码的位置、型态与开奖号码的位置、型态相同，即为中奖。
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="q2zx_fs">前二直选</strong>
<p class="p1">从百位、十位各选一个号码组成一注。</p>
<p class="p1">投注方案：58</p>
<p class="p1">开奖号码：前二位58，即中前二直选</p>
<p class="p1">从百位、十位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。</p>
<p class="p2">
	<br />
</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="h2zx_fs">后二直选</strong>
<p class="p1">从十位、个位各选一个号码组成一注。</p>
<p class="p1">投注方案：58</p>
<p class="p1">开奖号码：后二位58，即中后二直选。</p>
<p class="p1">从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后2位相同，且顺序一致，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="em_q2zux">前二组选</strong>
<p class="p1">&nbsp;从百、十位号选一个号码组成一注。</p>
<p class="p1">&nbsp;投注方案：58</p>
<p class="p1">开奖号码：前二位58或是85（顺序不限，不含对子）即中前二组选。</p>
<p class="p1">从0-9中选2个号码组成一注，所选号码与开奖号码的万位、千位相同（不含对子号），顺序不限，即中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="em_h2zux">后二组选</strong>
<p class="p1">从十、个位号选一个号码组成一注。</p>
<p class="p1">投注方案：58</p>
<p class="p1">开奖号码：后二位58或是85（顺序不限，不含对子）即中后二组选。</p>
<p class="p1">从0-9中选2个号码组成一注，所选号码与开奖号码的十位、个位相同（不含对子号），顺序不限，即中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_1m">一码不定位</strong>
<p class="p1">从0-9中任意选择1个以上号码。</p>
<p class="p1">投注方案：1</p>
<p class="p1">开奖号码：至少出现1个1，即中一码不定位。</p>
<p class="p1">从0-9中选择1个号码，每注由1号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="bdw_2m">二码不定位</strong>
<p class="p1">&nbsp;从0-9中任意选择2个以上号码。</p>
<p class="p1">投注方案：12</p>
<p class="p1">开奖号码：至少出现1和2各一个，即中三星二码不定位。</p>
<p class="p1">从0-9中选择2个号码，每注由2号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="zux_z3">组三</strong>
<p class="p1">从0-9中任意选择2个或2个以上号码。</p>
<p class="p1">投注方案:588</p>
<p class="p1">开奖号码:588（顺序不限），即中三星组选三。</p>
<p class="p1">从0-9选择2个数字组成二注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="zux_z6">组六</strong>
<p class="p1">从0-9中任意选择3个或3个以上号码。</p>
<p class="p1">投注方案:258</p>
<p class="p1">开奖号码:852（顺序不限），即中三星组选六。</p>
<p class="p1">从0-9选择3个数字组成一注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。</p>
<p class="p2">
	<br />
</p>
<strong class="p1" id="zhx_fs">直选</strong>
<p class="p1">从百位、十位、个位各选一个号码组成一注。</p>
<p class="p1">投注方案:345</p>
<p class="p1">开奖号码:345，即中三星直选。</p>
<p class="p1">从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码前3位相同，且顺序一致，即为中奖。</p>
<p>
	<br />
</p>