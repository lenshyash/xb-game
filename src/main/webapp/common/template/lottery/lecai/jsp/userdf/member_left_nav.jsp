<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="/common/template/lottery/lecai/js/base.js"></script>
<script src="/common/template/lottery/lecai/js/DFbase.js?v=1.0.2"></script>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<input type="hidden" value="${base}" id="base">
<style>
	.tapli{
		font-family: -webkit-body;
	}
	.userMain{
		min-height:703px!important;
	}
</style>
<input type="hidden" value="${loginMember.account}" id="agentName">
<div class="slideUser">
            <ul class="sildeSession"><h3><span></span>账号管理</h3>
                <li class="router-link-exact-active curr "><a href="${base}/center/member/meminfo/cpDfPage.do?form=index">会员中心</a></li>
                <li><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许修改账号信息')"href="javascript:void(0)"</c:if><c:if test="${!isGuest}">href="${base}/center/member/meminfo/cpDfPage.do?form=info"</c:if>>账号信息</a></li>
            </ul>
            <input type="hidden" value="${isChessOnOff}" id="kyid">
            <ul class="sildeSession"><h3><span></span>投注管理</h3>
                <li><a href="${base}/center/df/record/betrecord/cpDfPage.do?form=order" class="">彩票投注记录</a></li>
                <li><a href="${base}/center/df/record/betrecord/cpDfPage.do?form=win" class="">彩票中奖记录</a></li>
                <c:if test="${isChessOnOff=='on'}">
                	<li><a href="${base}/center/df/record/betrecord/cpDfPage.do?form=chess" class="">棋牌投注记录</a></li>
                </c:if>
				<c:if test="${isTyOnOff=='on'}">
					<li><a href="${base}/center/df/record/betrecord/cpDfSportPage.do" class="">体育投注记录</a></li>
				</c:if>
				<c:if test="${isZrOnOff=='on'}">
					<li><a href="${base}/center/df/record/betrecord/cpDfRealPage.do" class="">真人投注记录</a></li>
				</c:if>
				<c:if test="${isDzOnOff=='on'}">
					<li><a href="${base}/center/df/record/betrecord/cpDfEgamePage.do" class="">电子投注记录</a></li>
				</c:if>
               <%--  <c:if test="${not empty duLiCaiPiao&&duLiCaiPiao eq 'off'}">
                	<c:if test="${!isJoint}">
                		<c:if test="${isGuest}"><li><a onclick="alertMsg('试玩帐号不允许合买功能')" href="javascript:void(0)">合买记录</a></li></c:if>
                		<c:if test="${!isGuest}"><li><a href="${base}/center/df/record/betrecord/cpDfJointPage.do">合买记录</a></li></c:if>
                	</c:if>
                </c:if> --%>
                <%-- <c:if test="${isTyOnOff eq 'on'}"><li ${sportTab}><a href="${base}/center/record/betrecord/cp3SportPage.do">体育记录</a></li></c:if>
				<c:if test="${isZrOnOff eq 'on'}"><li ${realTab}><a href="${base}/center/record/betrecord/cp3RealPage.do">真人记录</a></li></c:if>

				<c:if test="${isDzOnOff eq 'on'}"><li ${egameTab}><a href="${base}/center/record/betrecord/cp3EgamePage.do">电子记录</a></li></c:if> --%>
            </ul>
            <ul class="sildeSession"><h3><span></span>资金管理</h3>
	            <li><a href="${base}/center/record/hisrecord/cpDfPage.do">账户明细</a></li>
	            <li><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许充值')"</c:if> href="<c:if test="${isGuest}">javascript://</c:if><c:if test="${!isGuest}">${base}/center/banktrans/deposit/cpDfPage.do</c:if>">充 值</a></li>
	            <li ${draw}><a <c:if test="${isGuest}">onclick="alertMsg('试玩帐号不允许提款')"</c:if> href="<c:if test="${isGuest}">javascript://</c:if><c:if test="${!isGuest}">${base}/center/banktrans/draw/cpDfPage.do</c:if>">提 款</a></li>
		         <c:if test="${(not empty isZrOnOff && isZrOnOff eq 'on')||(not empty isDzOnOff && isDzOnOff eq 'on')||(not empty isChessOnOff && isChessOnOff eq 'on')}">
					<li>
						<c:if test="${isGuest}"><a onclick="alertMsg('试玩帐号不允许转换额度')" href="javascript:void(0);">额度转换</a></c:if>
						<c:if test="${!isGuest}"><a href="${base}/center/banktrans/mnychg/cpDfPage.do">额度转换</a></c:if>
					</li>
				</c:if>
				<c:if test="${not empty exchangeScore && exchangeScore eq 'on' }">
					<li><a href="${base}/center/banktrans/exchange/cpDfPage.do">积分兑换</a></li>
				</c:if>
				<c:if test="${backwaterFlag}">
					<li id="fangshui"><a href="${base}/center/df/record/betrecord/member_backwater.do">反水记录</a></li>
				</c:if>
            </ul>
            <c:if test="${loginMember.accountType eq 4 || loginMember.accountType eq 5}">
	            <ul class="sildeSession"><h3><span></span>代理中心</h3>
	                <%-- <li><a href="${base}/daili" target="_blank">代理中心</a></li> --%>
	                <li id="info"><a href="${base}/center/df/record/betrecord/info.do">代理说明</a></li>
	                <li id="teamChange"><a href="${base}/center/df/record/betrecord/teamChange.do">团队账变</a></li>
					<li id="teamStatic"><a href="${base}/center/df/record/betrecord/teamStatic.do">团队统计</a></li>
					<li id="subMember"><a href="${base}/center/df/record/betrecord/subMember.do">会员管理</a></li>
					<li id="subMember"><a href="${base}/center/df/record/betrecord/cpDfPage.do?form=order&come=daili">投注明细</a></li>
					<li id="sponsoredLinks"><a href="${base}/center/df/record/betrecord/sponsoredLinks.do">下级开户</a></li>
					<li id="commission"><a href="${base}/center/df/record/betrecord/commission.do" >业绩提成</a></li>
					<li id="depositLog"><a href="${base}/center/df/record/betrecord/depositLog.do">存款日志</a></li>
					<li id="withdrawalLog"><a href="${base}/center/df/record/betrecord/withdrawalLog.do">取款日志</a></li>
	            </ul>
            </c:if>
            <ul class="sildeSession"><h3><span></span>消息管理</h3>
                <c:if test="${not empty letters && letters eq 'on'}">
					<li id="xiaoxizhongxin"><a href="${base}/center/news/message/cpDfPage.do">消息中心</a></li>
				</c:if>
				<li id="notice"><a href="${base}/center/df/record/betrecord/notice.do">网站公告</a></li>
            </ul>
        </div><script type="text/javascript">
	//公用信息加载
	function alertMsg(msg){
		layer.msg(msg,{icon:1,offset:['30%']});
	}
	console.log('KY开关：'+$('#kyid').val());
	$(function(){
		$.ajax({
			url:'${base}/center/news/message/messageCount.do',
			success:function(res){
				if(res.messageCount > 0){
					$("#xiaoxizhongxin span").text(res.messageCount).removeClass('hidn');
					$("#all_unread").text(res.messageCount);
				}
			}
		})
		//
		$.ajax({
			url:'${base}/center/member/meminfo/accountCount.do',
			success:function(res){
				if(res.count>0){
					$("#zhanghaoxinxi span").text(res.count).removeClass("hidn");
				}
			}
		})
	})
</script>