<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/dialog_msg.css?v=1.1"
	type="text/css">
<div id="dialogLogin">
<div class="_alert_ anim" id="loginBtn">
	<div class="alert_main">
		<div class="title">
			<span>用户登录</span>
			<div class="btn_close close-reveal-modal">
				<span></span>
			</div>
		</div>
		<div class="reg-info" id="_form_login">
			<ul>
				<li>
					<div class="info-left">
						<span class="red">* </span>用户名：
					</div>
					<div class="info-right">
						<label class="fl-small-tip-icon fl-reg-account-tip-icon"></label><input
							class="reg-int" id="username_login" placeholder="请输入用户名"
							maxlength="20" type="text" data="tc">
					</div>
				</li>
				<li>
					<div class="info-left">
						<span class="red">* </span>密码：
					</div>
					<div class="info-right">
						<label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input
							class="reg-int" id="passwd_login" placeholder="请输入密码"
							maxlength="20" type="password">
					</div>
				</li>
				<li>
					<div class="info-left">
						<span class="red">* </span>验证码：
					</div>
					<div class="info-right info-codes">
						<label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input
							class="codes-int" id="logVerifyCode" onfocus="Base.verifyImg();"
							placeholder="请输入验证码" type="text">
						<div class="reg-notice-ok" id="reg-notice-icon-login"
							style="display: none;"></div>
						<div name="yzm_img_div" style="display: none;">
							<div class="codes-img">
								<img name="login_img" id="logVerifyImg">
							</div>
							<a name="btn_refresh" onclick="Base.verifyImg();" style="margin-top:10px;float:left;"> <img
								src="${base}/common/template/lottery/lecai/images/refresh_1.png"
								style="cursor: pointer;">
							</a>
						</div>
						<div name="tip_yzm_img">点击输入框后出现验证码</div>
					</div>
				</li>
				<li class="marb">
					<div class="info-left">&nbsp;</div>
					<div class="info-right">
						<input id="btn-sub" onclick="Base.vaildate.userLogin();"
							class="submit" value="登录" type="submit">
					</div>
				</li>
				<li class="marb">
					<div class="info-left">&nbsp;</div>
					<div class="info-right">
						<c:if test="${isReg eq 'on' }"><p class="message">
							还没有账户? <a href="javascript:void(0);" onclick="loginClose();" data-reveal-id="registerBtn">立刻创建</a>
						</p></c:if>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript">
	function loginClose(){
		$("#loginBtn").css({'visibility': 'hidden'});
	}
</script>
<div class="_alert_ anim" id="registerBtn">
	<div class="alert_main">
		<div class="title">
			<span>用户注册</span>
			<div class="btn_close close-reveal-modal">
				<span></span>
			</div>
		</div>
		<div class="reg-info" id="reg_form_1">
                <ul>
                    <li>
                        <div class="info-left"><span class="red">* </span>用户名：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-account-tip-icon"></label><input id="account" unique="2" class="reg-int" maxlength="11" type="text"><div class="reg-notice-ok hidn"></div></div>
                        <div class="info-txt" id="_focus" style="display:none;">请输入5-11个字元, 仅可输入英文字母以及数字的组合!!</div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    <li>
                        <div class="info-left"><span class="red">* </span>密码：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input id="passwd" class="reg-int" maxlength="16" type="password"><div class="reg-notice-ok hidn"></div></div>
                        <div class="info-txt" id="_focus" style="display:none;">请输入6-16个英文或数字，且符合0~9或a~z字元 </div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    <li>
                        <div class="info-left"><span class="red">* </span>确认密码：</div>
                        <div class="info-right"><label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label><input class="reg-int" maxlength="16"  id="conpasswd" type="password"><div class="reg-notice-ok hidn"></div></div>
                        <div class="info-txt" id="_focus" style="display:none;">请再次输入密码</div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    
                    <li id="verifycode_div">
                        <div class="info-left"><span class="red">* </span>验证码：</div>
                        <div class="info-right info-codes" style="position:relative;">
                        	<label class="fl-small-tip-icon fl-reg-verify-tip-icon"></label>
                            <input name="verify" id="regVerifyCode" class="reg-int codes-int " unique="2" maxlength="5" type="text" onfocus="Base.verifyImg();"><div class="reg-notice-ok hidn"></div>
                            <div id="need_captcha" class="reg_need_captcha">
                                <div class="yanzhengma">
                                   <span class="register_captcha_span" style="position:relative;margin-left:10px;">
                                       <img name="login	_img" id="regVerifyImg">
                                       <a name="btn_refresh" onclick="Base.verifyImg();"><img src="${base}/common/template/lottery/lecai/images/refresh_1.png" style="cursor: pointer;"></a>
                                   </span>
                                </div>
                            </div>
                        </div>
                        <div class="info-txt" id="_focus" style="display:none;">请输入4位数字验证码</div>
                        <div class="info-txt" id="_blur" style="display:none;"></div>
                    </li>
                    <li class="marb">
                        <div class="info-left">&nbsp;</div>
                        <div class="info-right"><input name="reg_checkbox" checked="checked" type="checkbox">我已看过并同意<a class="orange" target="_self" onclick="window.open('${base}/lotteryV3/regAgreement.do','_reg_xieyi','width=1000,height=653').focus()" width="1000" height="653" border="0">《${_title}网络服务协议》</a></div>
                    </li>
                    <li class="marb">
                        <div class="info-left">&nbsp;</div>
                        <div class="info-right"><input id="btn-sub" onclick="Base.vaildate.userRegister()" class="submit" value="立即注册" type="submit"></div>
                    </li>
                    <li class="marb">
					<div class="info-left">&nbsp;</div>
					<div class="info-right">
						<p class="message">
							已有账户? <a href="javascript:void(0);" onclick="registerClose();" data-reveal-id="loginBtn">立即登录</a>
						</p>
					</div>
				</li>
                </ul>
            </div>
	</div>
</div>
</div>
<script type="text/javascript">
	function registerClose(){
		$("#registerBtn").css({'visibility': 'hidden'});
	}
	$(function(){
		$("#account").focus();
		$("#username_login").focus();
		$("#dialogLogin").keydown(function(event) {
	    	if (event.keyCode == "13") {//keyCode=13是回车键
	    		var lh = $("#loginBtn").css("visibility")
	    		var rh = $("#registerBtn").css('visibility');
	    		if(lh == 'visible'){
	    			Base.vaildate.userLogin();
	    			return;
	    		}
	    		if(rh = 'visible'){
	    			Base.vaildate.userRegister();
	    			return;
	    		}
		 	}
		});
	})
</script>
<script type="text/javascript"
	src="${base}/common/template/lottery/lecai/js/jquery.reveal.js"></script>
<script type="text/javascript" src="${base}/regconf.do"></script>
	<script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
			<li>
                <div class="info-left">{{if tl.requiredVal == 2}} <span class="red">* </span>{{/if}}{{tl.name}}：</div>
                	<div class="info-right">
					{{if tl.type==1}}
					<label class="fl-small-tip-icon fl-reg-verify-tip-icon"></label>
					<input id="{{tl.key}}" type="text" class="reg-int" unique="{{tl.uniqueVal}}" t="true"/><div class="reg-notice-ok hidn"></div>
				{{/if}}
				{{if tl.type==2}}
					<select id="{{tl.key}}">
						{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
						{{/if}}
					</select>
				{{/if}}
				{{if tl.type==3}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as rad i}}
						<input type="radio" name="{{tl.key}}" value="{{i+1}}">{{rad}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<input type="checkbox" name="{{tl.key}}" value="{{i+1}}">{{chk}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<textarea id="{{tl.key}}"></textarea>
				{{/if}}
				{{if tl.type==6}}
					<label class="fl-small-tip-icon fl-reg-pwd-tip-icon"></label>
					<input type="password" id="{{tl.key}}" class="reg-int" unique="{{tl.uniqueVal}}" t="true"></input><div class="reg-notice-ok hidn"></div>
				{{/if}}
				{{$addValidateFiled tl}}
				</div>
				{{if tl.type==1 && tl.key== 'userName'}}
					<div class="info-txt" id="_focus" style="display:none;">必须与您的银行帐户名称相同，否则不能出款!</div>
                	<div class="info-txt" id="_blur" style="display:none;"></div>
				{{/if}}
				{{if tl.type==6}}
					<div class="info-txt" id="_focus" style="display:none;">提款认证必须，请务必记住! </div>
                	<div class="info-txt" id="_blur" style="display:none;"></div>
				{{/if}}
            </li>
	{{/each}}
	</script>
	<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
	<script type="text/javascript" src="${base}/common/template/member/register/register.js" version="cp_v3"></script>