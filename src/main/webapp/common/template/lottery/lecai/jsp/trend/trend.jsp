<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>走势图-${_title}</title>
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
	<input type="hidden" value="4" id="navId" />
	<c:if test="${not empty lotCode}">
		<iframe onLoad="frameInit();" id="trendIframe" style="height: 100%" frameborder="0" width="100%" scrolling=no src="${base}/lottery/trendChart/index.do?lotCode=${lotCode}"></iframe>
	</c:if>
	<c:if test="${empty lotCode}">
		<iframe id="trendIframe" src="${base}/lottery/trendChart/index.do"></iframe>
	</c:if>
	<jsp:include page="/member/${stationFolder}/include/footers.jsp"></jsp:include>
	<script type="text/javascript">
	var timer;
	function frameInit(){
		//动态加载内容，以达到自适应高度
		timer = window.setInterval("frameHeight()",400);
	}
	
	function frameHeight(){
		var iframe = document.getElementById("trendIframe");
		var bHeight = iframe.contentWindow.document.body.scrollHeight;
		var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
		var height = Math.min(bHeight,dHeight);
		if(iframe.height == height){
			//停止定时器
			window.clearInterval(timer);
		}else{
			iframe.height = height + 20;
		}
	}
	</script>
</body>
</html>