<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
		white-space:nowrap;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">团队账变</div>
            <div class="userMain mgb10">
                <ul class="searchFirst zhangbian">
                	<li><span>类型：</span>
                        <ins class="selectIcon">
                                <select class="userSelect" name="code" id="lotCode">
                                   
                                </select>
                            <em></em>
                         </ins>
                    </li>
                    <li class="_state" style="display: flex;align-items: center;">
                    	<span>&nbsp;</span>
                    	<input class="inputText" id="orderId" value="" type="text" placeholder="订单号" style="background:#fff;height:30px;">
                    	<input class="inputText" id="account" value="" type="text" placeholder="会员账号" style="background:#fff;height:30px;margin-left:10px;">
                    	<input class="submitBtn" onclick="initRdsData(1)" value="查询" type="button" style="border:none;margin-left:10px;">
       				</li>
                    <li class="_time" id="selected_time">
                    	<span>时间：
	        				<a class="userSearch time active" value="0">今天</a>
	        				<a class="userSearch time " value="1">昨天</a>
	        				<a class="userSearch time " value="7">七天</a>
        				</span>
        			</li>
                </ul>
                <div class="searchDetail">
                    <table>
                        <tbody>
                        <tr class="searchTable">
                            <th colspan="1">编号</th>
                            <th>会员账号</th>
                            <th>变动类型</th>
                            <th>变动前金额</th>
                            <th>变动金额</th>
                            <th>变动后余额</th>
                            <th>变动时间(系统)</th>
                            <th>订单号</th>
                            <th>备注</th>
                        </tr>
                        </tbody>
                        <tbody class="Records_listCont">
                        <tr style="border-bottom: 0px;">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                                <div class="loadingGif" style="padding: 100px 0px;display:none;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="userTip mgt15"><p><i></i>温馨提示：账变记录最多只保留7天。</p></div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				if(Request['account']){
					$("#account").val(Request['account'])
				}
				initCombo()
				initRdsData(1)
		        $(".searchFirst li a").click(function () {
		            $(this).addClass('active').siblings('a').removeClass('active')
		            today($(this).attr('value'))
		            initRdsData(1)
		        })
		        $('#lotCode').change(function(){
		        	initRdsData(1);
		        })

			})
			function initCombo() {
				$.ajax({
					url : "${base}/daili/dlmnyrd/money/record/type.do",
					async : false,
					success : function(data) {
						var html = '<option value="0">全部</option>';
						for(var i = 0; i < data.length; i++){
							html+='<option value="'+data[i].type+'">'+data[i].name+'</option>'
						}
						$("#lotCode").html(html)
					}
				});
			}
			var currPage = 0;
			function initRdsData(p) {
				$(".Records_listCont").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}

				var data = {
		    		account: $("#account").val(),
		    		orderId :$("#orderId").val(),
					type : $("#lotCode").val(),
					begin : startTime + " 00:00:00",
					end : endTime + " 23:59:59",
					rows : 10,
					page : p
				}
				$.ajax({
					url : '${base}/daili/dlmnyrd/list.do',
					data : data,
					success : function(res) {
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.total == 0){
							//无数据
							$(".Records_listCont").html(noTakeNotes)
						}else{
							addTemplate(res);
							page = p;
							totalPage = Math.ceil((res.total/10))
							lotteryPage(res.total,totalPage)
							lastPage = totalPage
							$("#pageNum").html(p)
						}
					}
				})
			}
			function addTemplate(res){				
				var html = '', bd=null;
				$.each(res.rows,function(index, item){
					bd = new Date(item.bettingDate);
					if(!item.remark){
						item.remark = '无备注'
					}else{
						item.remark = '<span onclick="showMessage(&quot;'+item.remark+'&quot;)" style="background-color: #4aa9db;color:#fff;border-radius:3px;padding:3px 5px;cursor:pointer;">查看备注</span>'
					}
					html+='<tr><td>'+item.id +'</td><td>'+item.account+'</td><td>'+statusType[item.type]+'</td><td>'+item.beforeMoney+'</td><td style="color:#4aa9db;">'+item.money+'</td><td style="color:#e4393c;">'+item.afterMoney+'</td><td>'+dateFormatter(item.createDatetime)+'</td><td>'+unExist(item.orderId)+'</td><td style="white-space:unset;">'+item.remark+'</td></tr>'
				})
				$(".Records_listCont").html(html)
			}
			function dateFormatter(value) {
				return DateUtil.formatDatetime(value);
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
</body>
</html>