<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="header-plus" id="header_plus">
        <div class="head-simple-toptray-bd clearfix auto990 header">
    <div class="logo-box" style="width: 195px;padding: 0">
        <a title="百度彩票" href="http://www.lecai.com/">
            <h1>百度彩票</h1>
            <img title="百度彩票" alt="百度彩票" src="#" height="36">
        </a>
    </div>
    <div class="toptray-right clearfix">
        <div class="toptray-right-l" style="float:right">
            <ul class="customerService-hd">
                <li class="online_service">
                    <a href="http://www.lecai.com/page/zhuanti/service/" target="_blank" style="color:#f13131" class="kf-top"><em class="icon20"></em>在线客服</a>
                    <a href="http://redirect.lecai.com/help?REF=http%3A%2F%2Fwww.lecai.com%2Fuser%2Fregister%2F%3Ffrom%3Dhead_nav" target="_blank" style="color:#f13131">帮助</a>
                </li>
            </ul>
        </div>
        <div class="toptray-right-r" style="float:right;">

            <ul id="toptray_login" class="hidden toptray clearfix">
                <li>您好&nbsp;<span id="toptray_username"></span><a href="http://www.lecai.com/user/logout/" class="exit-btn ml14 color666 js-trigger-logout">退出</a></li>
                <li class="m14">|</li>
                <li class="header-show-money">
                    <div id="wallet_container">
                        <span class="span-login-rmb colorRed">￥</span>
                        <span id="header_user_money" class="colorRed">*****</span><input id="header_user_money_hidden" value="" type="hidden">
                        <div id="wallet_detail" style="display:none;"></div>
                    </div>
                    <a href="javascript://" id="header_show_money" class="color666">显示余额</a>
                    <i id="header_money_refresh" class="img-login-refresh icon"></i>
                </li>
                <li class="mylottery" id="mylottery">
                    <a href="http://www.lecai.com/user/mylottery/?from=head_nav" class="my-lottery color666"><i class="nosign-hd" id="header_sign"></i>我的彩票<em class="icon"></em></a>
                    <div id="mylottery_dropdown" style="display: none;" class="lot_list">
                        <ul>
                            <li><a href="http://www.lecai.com/user/order/?from=head_nav">投注记录</a></li>
                            <li><a href="http://www.lecai.com/user/order/win/?from=head_nav">中奖记录</a></li>
                            <li><a href="http://www.lecai.com/user/view/?from=head_nav">我的战绩</a></li>
                            <li><a href="http://www.lecai.com/caidou/guess">我的彩贝</a></li>
                            
                            <li><a href="http://www.lecai.com/user/update/profile/?from=head_nav">账户信息</a></li>
                            <li><a href="http://www.lecai.com/user/order/notpaid/?from=head_nav">未支付订单</a><span id="header_notpaid_counter_hd" class="notpaid"></span></li>
                        </ul>
                    </div>
                </li>
                <li class="m14" style="margin-left:-3px">|</li>
                <li><a href="${base }/center/banktrans/deposit/cp3Page.do" class="mr10 color666 mr">充值</a><a href="${base }/center/banktrans/draw/cp3Page.do" class="color666">提款</a></li>
                <li class="toptray-r-hd ml14">
                    <div id="unread_count_num">
                        <a class="message-nums-hd color666" href="http://www.lecai.com/user/message/">消息<em id="unread_num_new" class="icon"></em></a>
                    </div>
                </li>
                <li class="m14">|</li>
            </ul>
            <ul id="toptray_not_login" class="toptray-plus clearfix">
                <li class="color666">
                    hi，欢迎来到百度彩票！&nbsp; <a href="http://www.lecai.com/user/login/?from=head_nav" class="colorRed">登录</a> &nbsp;
                    <a href="http://www.lecai.com/user/register/?from=head_nav" class="colorRed">注册</a>
                </li>
                <li class="m14">|</li>
                <li class="mylottery"><a href="http://www.lecai.com/user/mylottery/?from=head_nav" class="my-lottery color666">我的彩票<em class="icon"></em></a></li>
                <li>
                    <a href="${base }/center/banktrans/deposit/cp3Page.do" class="color666 mr10">充值</a>
                    <a href="${base }/center/banktrans/draw/cp3Page.do" class="color666">提款</a>
                </li>
                <li class="m14">|</li>
            </ul>
            <!--<ul id="toptray_not_login" class="toptray-hd clearfix">
                <li class="color666">
                    hi，欢迎来到百度彩票！&nbsp; <a href="http://www.lecai.com/user/login/?from=head_nav" style="color:#f13131">登录</a> &nbsp;
                    <a href="http://www.lecai.com/user/register/?from=head_nav" style="color:#f13131">注册</a>
                </li>
                <li class="p-zy10">|</li>
                <li class="mylottery"><a href="http://www.lecai.com/user/mylottery/?from=head_nav" class="my-lottery">我的彩票<em class="icon20"></em></a></li>
                <li class="p-zy10">|</li>
                <li>
                    <a href="http://www.lecai.com/user/recharge/">充值</a> &nbsp;
                    <a href="http://www.lecai.com/user/withdraw/">提款</a>
                </li>
                <li class="p-zy10">|</li>
            </ul>-->
        </div>
    </div>
</div>
</div>