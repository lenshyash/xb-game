<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="${not empty webKeywords?webKeywords:_title}">
<meta name="description" content="${not empty webDescription?webDescription:_title}">
<title>会员中心 - ${_title}</title>
<link rel="stylesheet" href="${base}/common/template/lottery/lecai/css/personalDF.css" type="text/css">
<jsp:include page="/member/${stationFolder}/include/ico.jsp"></jsp:include>
<link rel="stylesheet"
	href="${base}/common/template/lottery/lecai/css/user_content.css?v="
	type="text/css">
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<jsp:include page="../include/baseScript.jsp"></jsp:include>
</head>
<body>
<style>
	.searchDetail table th,td{
		font-size:12px;
	}
	.sendList table tr th,td{
		text-align:center!important;
	}
	.sendList table .messageTr:hover{
		background: #f8f8f8;
		cursor: pointer;
	}
</style>
	<jsp:include page="/member/${stationFolder}/include/header.jsp"><jsp:param value="true" name="type"/> </jsp:include>
		<div class="container" style="margin-top:20px;min-height:800px;">
		<jsp:include page="member_left_nav.jsp"></jsp:include>
		<div class="userRight">
            <div class="userTitle mgb10">站内信</div>
            <div class="userMain mgb10">
                <div class="sendList">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <th scope="col">主题</th>
                            <th scope="col">发件人</th>
                            <th scope="col">时间</th>
                        </tr>
                        </tbody>
                        <tbody class="fudetail_list">
                        <tr style="border-bottom: 0px;" class="noData">
                            <td colspan="100">
                                <div class="notContent" style="padding: 100px 0px;"><i class="iconfont"></i>暂无记录</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="page"><p>共<em id="pageCount">0</em>条记录&nbsp;&nbsp;&nbsp;当前第<em id="pageNum"></em>页</p>
                    <div class="pageNav">
                        <div  class="pageNav">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </div>
                </div>
                    <div class="userTip"><p><i></i>温馨提示：系统将自动清空7天前的用户消息记录。
                    </p></div>
                </div>
            </div>
        </div>
		</div>
		<script>
			$(function(){
				initRdsData(1)
			})
/* 			var json = '{"currentPageNo":1,"hasNext":false,"hasPre":false,"list":[{"createDatetime":1540026418506,"accountId":610,"id":138,"message":"尊敬的会员:gosick,您累计充值：1000.0,达到普通会员升级要求,从新会员注册升级到普通会员,感谢您对本站一直以来的支持！","title":"恭喜，您已经升级成为普通会员!","type":1,"userMessageId":144,"account":"gosick","stationId":41,"status":2}],"nextPage":1,"pageSize":10,"prePage":1,"results":[{"$ref":"$.list[0]"}],"set":[{"$ref":"$.list[0]"}],"start":0,"totalCount":1,"totalPageCount":1}'
 */			function initRdsData(p) {
				$(".fudetail_list").html(noTakeNotes)
		    	$(".notContent").hide()
		    	$(".loadingGif").show()
		    	if(!endTime){
					endTime = startTime
				}
				var data = {
						pageNumber : p,
						pageSize : 10,
						status:0
				}
				$.ajax({
					url : '${base}/center/news/message/list.do',
					data : data,
					success : function(res) {
						$(".notContent").show()
				    	$(".loadingGif").hide()
						if(res.totalCount == 0){
							//无数据
						}else{
							addTemplate(res)
							page = res.currentPageNo;
							lotteryPage(res.totalCount,res.totalPageCount)
							lastPage = res.totalPageCount
							$("#pageNum").html(res.currentPageNo)
						}
					}
				})
			}
			function addTemplate(res){
				var html = '';
				$.each(res.list,function(index, item){
					html+='<tr class="messageTr" onclick="showMessage(&quot;'+item.message+'&quot;)"><td>'+item.title+'</td><td>${_title}</td><td>'+getMyDate(item.createDatetime)+'</td></tr>'
				})
				$(".fudetail_list").html(html)
			}
		</script>
	<jsp:include page="/member/${stationFolder}/include/footer.jsp"></jsp:include>
	<style>
			.layui-layer-page .layui-layer-content{
				overflow: auto!important;
			}
			.layui-layer-page .layui-layer-content::-webkit-scrollbar {
		        display: none;
		    }
	</style>
</body>
</html>