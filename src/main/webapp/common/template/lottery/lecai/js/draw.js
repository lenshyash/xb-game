var t1;
$(function() {
	Draw.initData();	//初始化
//	$("#nav_sort ul li").click(function() {
//		$(this).addClass("tit").siblings().removeClass("tit");
//		var code = $(this).find("a").attr("c"),name=$(this).find("a").text();
//		$(".nowLotName").text(name);
//		// 获取上一期和当期的开奖时间倒计时
//		// 获取历史开奖结果
//		Draw.initData();
//	})
})


var Draw = {
	code:null,
	oneDay:24*60*60,
	initData:function(){
		Draw.getLotCurrentData(Draw.code);	//当前期和上一期数据
	},
	getLotCurrentData:function(c){
		Draw.stop(t1);	//加载新数据必须清除原有的定时器
		var needLast = true,curQiHao = $("#curQiHao").val();
		if(c=='FC3D' || c=="LHC" || c=='PL3'){
			if(!curQiHao){
				needLast = true;
			}else{
				needLast = false;
			}
		}
		var d = {lotCode:c,needLast:needLast}
		$.ajax({
			url: base + "/lotteryV3/lotterys.do",
			data:d,
			dataType:'json',
			success:function(res){
				Draw.addCurData(res[c]);	//当前数据
				if(needLast){
					Draw.addLastData(res[c],res.lastTime,res.year);	//上一期数据
				}
			}
		})
	},
	addLastData:function(r,lastTime,year){
		var maTemp = "", haoMa = r.lastHaoMa.split(",");
		$("#jq_latest_draw_time").html("开奖时间："+$.formatDate("yyyy-MM-dd hh:mm:ss",new Date(lastTime),0));
		$("#jq_latest_draw_phase").html(r.lastQiHao);
		switch(r.lotCode){
			case "BJSC":
				for(var i=0;i<haoMa.length;i++){
					if(haoMa[i] == '?'){
						maTemp += '<span class="ball_1">'+haoMa[i]+'</span>';
					}else{
						maTemp += '<span class="pk10 b'+haoMa[i]+'">'+haoMa[i]+'</span>';
					}
				}
				break;
			case "PCEGG":
				var sum = 0;
				for(var i=0;i<haoMa.length;i++){
					if(haoMa[i] == '?'){
						if(i==0 || i==1){
							maTemp += '<span class="ball_1">'+haoMa[i]+'</span><b>+</b>&nbsp;';
						}else{
							maTemp += '<span class="ball_1">'+haoMa[i]+'</span><b>=</b>&nbsp;';
						}
					}else{
						sum+=parseInt(haoMa[i]);
						if(i==0 || i==1){
							maTemp += '<span class="ball_1">'+haoMa[i]+'</span><b>+</b>&nbsp;';
						}else{
							maTemp += '<span class="ball_1">'+haoMa[i]+'</span><b>=</b>&nbsp;';
						}
					}
				}
				maTemp += '<span class="ball_2">'+(sum==0?'?':sum)+'</span>';
				break;
			case "LHC":
				$(".dipin_Draw_ul").css({'margin-bottom':'10px'});
				for(var i=0;i<haoMa.length;i++){
					if(i==6){
						maTemp += '<b style="display: inline-block;margin-top: -10px;">+</b>&nbsp;';
					}
					if(haoMa[i] == '?'){
						maTemp += '<span class="lhc_1">'+haoMa[i]+'<font style="display:inline-block;width:20px;height:20px;color:#000">?</font></span>';
					}else{
						var index = Base.lhcHaoMa.bose(haoMa[i]);
						var sx = Base.lhcHaoMa.zodiacName(haoMa[i],year);
						maTemp += '<span class="lhc_'+index+'">'+haoMa[i]+'<font style="display:inline-block;width:20px;height:20px;color:#000">'+sx+'</font></span>';
					}
				}
				break;
			default:
				for(var i=0;i<haoMa.length;i++){
					maTemp += '<span class="ball_1">'+haoMa[i]+'</span>';
				}
				break;
		}
		$("#jq_latest_draw_result").html(maTemp);
	},
	addCurData:function(r){
		var c = r.lotCode, state=1;
		if(!r.id){
			state = 2;
		}
		$("#jq_cur_phase").html(r.qiHao);
		var timeMoney = r.actionTime + ""
		$("#jq_volume").text(parseInt(timeMoney.substring(2,5))+parseInt(timeMoney.substring(4,6))+","+timeMoney.substring(5,8))
		$("#jq_cur_drawtime").html($.formatDate("yyyy-MM-dd hh:mm:ss",new Date(r.actionTime),0));
		Draw.timer(r.actionTime,c,state);
	},
	timer:function(t,c,s){
		var openTime = $.formatDate("yyyy-MM-dd hh:mm:ss",new Date(t),0);
		var nowTime = $.formatDate("yyyy-MM- dd hh:mm:ss",new Date(),0);
		var diff = $.baseTimerDiff(openTime,nowTime);
		Draw._tick(diff,c,s);
	},
	_tick:function(d,c,s){
		var t = $(this);
		t.stop(t1);
		if(d>0){
			var date = $.baseFormat(d,Draw.numberDay(d));
			Draw.setDateHtml(date,c);
			t1 = setTimeout(function(){
				d = d - 1;
				Draw._tick(d,c,s);
			},1000);
		}
		if(d==0){
			//重新加载下一期
			Draw.getLotCurrentData(c);
		}
	},
	setDateHtml:function(date,c){
		var d = date.split(":"),temp="",h=['时','分','秒'];
		for(var i=0;i<d.length;i++){
			temp += '<strong class="red">'+d[i]+'</strong>'+h[i]+'';
		}
		$("#jq_cur_countdown").html(temp);
	},
	numberDay:function(d){
		if(Draw.oneDay<d){
			return Math.floor(d/Draw.oneDay);
		}
		return 0;
	},
	stop:function(t){
		clearTimeout(t);
	}
};


(function() {
	var _tags = document.getElementsByTagName("script");
	Draw.code = _tags[_tags.length - 1].getAttribute("code");
})();