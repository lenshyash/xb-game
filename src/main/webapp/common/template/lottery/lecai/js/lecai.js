var bet_result = {};
var selectedLotLastQuartz,sliderHaoMa;	//定时器
$(function(){
	LeCai._init();
	//前五期
	var $main = $("#lecaiMain");
	$("#showgd-box").on("click",".box-ul li a",function(){
		$(this).parent().parent().find("a").removeClass("tabulous_active");
		$(this).addClass("tabulous_active");
		var id = $(this).attr("attrid").replace("#","");
		$("#tabs_container").find("#"+id).addClass("showleft").siblings().removeClass("showleft");
	});
	
	
	function digitOnly(e){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {
        	 layer.tips("请输入整数",$(this),{tips:[1,'#f13131']});
             return false;  
         }  
	}
	
	//随机注数
	$("#randomSelNumInput").on("keyup blur",function(){
		var it = $(this),v=it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v <1){
    		layer.tips("倍数必须为大于0的整数",it,{tips:[1,'#f13131']});
			it.val(1);
    		return false;
    	}
	}).on("keypress",digitOnly);
	//倍数
	$("#beiShu").on("keyup blur",function(){
		var it = $(this),v=it.val();
    	if(v){
	    	if(!/^[\d]{1,8}$/.test(v) || v>10000000){
	    		layer.tips("倍数必须大于0，小于10,000,000",it,{tips:[1,'#f13131']});
	    		it.val(1);
	    		return false;
	    	}
    	}
    	LeCai.betting_hasOwnProperty(v);
	}).on("keypress",digitOnly);
	//追号期数
	$("#lt_trace_count_input").on("keyup blur",function(){
		var it = $(this),v=it.val();
    	if(!/^[\d]{1,2}$/.test(v) || v>30 || v<1){
    		layer.tips("追号期数必须大于0，小于等于30",it,{tips:[1,'#f13131']});
			it.val(10);
    		return false;
    	}
	}).on("keypress",digitOnly);
	//起始倍数
	$("#lt_trace_times_same").on("keyup blur",function(){
		var it = $(this),v=it.val();
    	if(!/^[\d]{1,8}$/.test(v) || v <1){
    		layer.tips("倍数必须为大于0的整数",it,{tips:[1,'#f13131']});
			it.val(1);
    		return false;
    	}
	}).on("keypress",digitOnly);
	//追号列表中倍数
	$("#betTraceData").on("keyup blur",'input[type=text]',function(){
		 var it=$(this),v = it.val();
	    	if(!/^[\d]{1,8}$/.test(v) || v<1){
	    		layer.tips("倍数必须为大于0的整数",it,{tips:[1,'#f13131']});
				it.val(1);
	    		return false;
	    	}
	}).on("keypress",digitOnly);
	//彩种小类
	$main.on("click","#tabbar-div-s3>li>div",function(){
		LeCai.playCode = $(this).find("span").attr("c");
		//if($(this).hasClass("act")){
			//如果是当前选中状态无需重复加载数据
			//return;
		//}
		//获取奖金和反水
		//$(this).attr("class","act").siblings().attr("class","back");
		LeCai.template._GetContentHtml();
		LeCai.betting.resetSumbetCount();
	}).on("click","#tabbar-div-s2 .u_tab_div>span",function(){	//彩种大类
		$(this).attr("class","tab-front").siblings().attr("class","tab-back");
		LeCai.playCode = null;
		LeCai.groupCode = $(this).attr("value");
		LeCai.template._GetContentHtml();
		LeCai.betting.resetSumbetCount();
	}).on("mouseover",".tbn_top span",function(){
		var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#f13131'],time:0,area:['350px','auto']});
    	}
	}).on("mouseout",".tbn_top span",function(){
		layer.closeAll("tips");
	}).on("click",".optimize-control ul li input[type=checkbox]",function(){	//冷热遗漏
		var val = $(this).val();
		if($(this).is(":checked")){
			$("#lt_selector").find('div[name='+val+']').removeClass("hide");
		}else{
			$("#lt_selector").find('div[name='+val+']').addClass("hide");
		}
	}).on("click",".choose-money li",function(){	//圆角分
		$(this).addClass("on").siblings().removeClass("on");
		var  val = $(this).attr("value");
		LeCai.betting_hasOwnProperty($("#beiShu").val());
	}).on("click",".tz_bei_sub",function(){
		var val = parseInt($("#beiShu").val());
		if(!val){val = 1;}
		if(val==1){
			layer.tips("倍数必须大于0","#beiShu",{tips:[1,'#f13131']});
			$("#beiShu").val(1);
			return;
		}
		$("#beiShu").val(val-1);
		LeCai.betting_hasOwnProperty(val-1);
	}).on("click",".tz_bei_add",function(){
		var val = parseInt($("#beiShu").val());
		if(!val){val = 0}
		if(val==1000000){
			layer.tips("倍数必须小于1,000,000","#beiShu",{tips:[1,'#f13131']});
			return;
		}
		$("#beiShu").val(val+1);
		LeCai.betting_hasOwnProperty(val+1);
	}).on("click","#lt_selector .gds>.gd div",function(){	//号码单个选取
		if($(this).hasClass("on")){
			$(this).removeClass("on");
		}else{
			$(this).addClass("on");
		}
		LeCai.betting.sumbetCount();
	}).on("click","#lt_selector .gds>.to li",function(){
		var name = $(this).attr("name"),_num = $(this).parent().parent().siblings(".gd").find("div");
		$(this).addClass("on").siblings().removeClass("on");
		if(name){
			switch(name){
				case "all":
					_num.addClass("on");
					break;
				case "big":
					var l = parseInt(_num.eq(0).attr("num"),10);
					if(l==0)l=5;else if(l==1)l=6;
					_num.each(function(){
						var t=$(this),num = parseInt(t.attr("num"));
						if(num>=l){
							t.addClass("on");
						}else{
							t.removeClass("on");
						}
					})
					break;
				case "small":
					var l = parseInt(_num.eq(0).attr("num"),10);
					if(l==0)l=5;else if(l==1)l=6;
					_num.each(function(){
						var t=$(this), num = parseInt(t.attr("num"));
						if(num<l){
							t.addClass("on");
						}else{
							t.removeClass("on");
						}
					})
					break;
				case "odd":
					_num.each(function(){
						var t=$(this), num = parseInt(t.attr("num"));
						if(num%2!=0){
							t.addClass("on");
						}else{
							t.removeClass("on");
						}
					})
					break;
				case "even":
					_num.each(function(){
						var t=$(this), num = parseInt(t.attr("num"));
						if(num%2==0){
							t.addClass("on");
						}else{
							t.removeClass("on");
						}
					})
					break;
				case "clean":
					_num.removeClass("on");
					$(this).removeClass("on");
					break;
			}
		}
		LeCai.betting.sumbetCount();
	}).on("mouseover","#lt_cf_content [tips]",function(){
		var it=$(this),s = it.attr("tips");
    	if(s){
    		layer.tips(s,it,{tips: [1, '#000'],skin:""});
    	}
	}).on("mouseout","#lt_cf_content [tips]",function(){
		layer.closeAll("tips");
	}).on("change",".selposition input:checkbox",function(){
		var it=$(this).parent().parent(),
		a=it.find("input:checkbox:checked").length,
		b=parseInt(it.attr("wei")||0);
		$("#positioncount").text(a);
		var combine=function(b,d){
	    	var c=function(a){
	    		for(var b=1,c=2;c<=a;c++)
	    			b*=c;return b
	    		};
	    	return d>b?0:d==b?1:c(b)/(c(d)*c(b-d))
	    };
		$("#positioninfo").text(combine(a,b));
		LeCai.betting.sumbetCount();
	}).on("click","#addBettingNumberId",function(){	//添加到订单列表
	        if(!bet_result.hasOwnProperty('betCount') || bet_result.betCount <= 0) {return false;}
	        if ($main.find("#lt_cf_content tr").length > LeCai.maxItem) return false;
	        var allTotamounts = parseFloat($("#lt_sel_money").text());// 投注金额
	        if(allTotamounts && allTotamounts>0){
	        	var bettr = '',y = 0,n=$("#tabbar-div-s3");
	        	tth = n.find(".tz_title").text(),
	        	tts = n.find("div.act>span").text(),
	        	playCode = n.find("div.act>span").attr("c"),
	         	coin = $(".choose-money li.on").attr("value"),
	         	beiShu=$("#beiShu").val(),
	         	rate=$("#peiLv").text();
            if (bet_result.errorTagStr != '') {
            	layer.msg("温馨提示： 以下号码有误，已进行自动过滤。" + bet_result.errorTagStr,{icon:5});
            }
            var hm=(bet_result.hasOwnProperty('optional')&&bet_result.optional)?(bet_result.optional+"@"+bet_result.myarray):bet_result.myarray;
            // 彩票code｜玩法id｜货币单位（1=元，10=角，100=分）｜投注倍数|投注号码
            var buyinfo = LeCai.code + '|' + LeCai.playCode + '|' + coin + '|'+beiShu + '|' + hm;
            if($main.find("#lt_cf_content tr").length>0){
	            $main.find("#lt_cf_content tr").each(function() {
	                if ($(this).attr('buyinfo').toString() === buyinfo) {
	                    y++;
	                }
	            });
            }
            if (y) {	
            	layer.msg("此投注号码已存在，请重新选择!!",{icon:5});
            	//清楚选中的数据
            	LeCai.betting.resetClearCount();
                return false;
            }
            //拼接列表html
            bettr += '<tr buyinfo="' + buyinfo + '" c="'+playCode+'" m="'+allTotamounts+'" r="'+rate+'">';
            bettr += '<td>'+$("h3.page_name").text()+'</td><td><span tips="'+tth+':'+tts+'">'+tts+'</span></td>';
            bettr += '<td><span tips="'+hm+'">'+LeCai.strLimit(hm, 22)+'</span></td>';
            bettr += '<td>'+bet_result.betCount+'</td>';
            bettr += '<td>' + rate;
            if(LECAI.fanShui){
            	bettr += '/' + $("#fanShui").text() + "%";
            }
            bettr += '</td><td>'+allTotamounts+'</td>'
            bettr += '<td><span class="xz_delete"></span></td></tr>';
            $main.find("#lt_cf_content tbody").append(bettr);
            LeCai.betting.resetClearCount();
            LeCai.countTotalBet();
            processBetTraceCore.updateBetData();
	        }else{
	        	layer.alert("投注金额不能为0",{icon:2,offset : ['30%' ]});
	        }
	}).on("click",".xz_delete",function(){	//删除相应的注单
		var e=$(this);
    	layer.confirm('你确定删除该注单？', {
    		btn: ['确定','取消'],icon:3,offset : ['30%' ] // 按钮
		}, function(index){
			e.parent().parent().remove();
	        LeCai.countTotalBet();
	        processBetTraceCore.updateBetData();
			layer.close(index);
		});
	}).on("click","#lt_cf_clear",function(){	//全部删除
		if($("#lt_cf_content tr").length<=0)return false;
		layer.confirm('你确定清空注单？', {
    		btn: ['确定','取消'],icon:3,offset : ['30%' ] // 按钮
		}, function(index){
			LeCai._clearAllItem();
			layer.close(index);
		});
	}).on("click","#lt_trace_if",function(){
		if($(this).is(":checked")){
			var trs = $main.find("#lt_cf_content tr");
			if(!trs.length){
				layer.alert("请先添加一注投注号码!",{icon:5,offset : ['30%' ]});
				$(this).prop("checked",false);
				return false;
			}
			if(!LeCai.isLogin){
				LeCai._isLoginBtn();
				return false;
			}
			LeCai.ajax.LotteryV3_BetTrace();
			$("#lt_trace_box").css("display","block");
			$("#lt_trace_times_same").val(processBetTraceCore.startTimes);
		}else{
			$("#lt_trace_box").css("display","none");
		}
	}).on("click","#betTraceSelectAll",function(){
		if($(this).prop("checked")){
    		$("#betTraceData").find("input:checkbox").prop("checked",true);
    		$("#betTraceData").find("tr").addClass("on");
    	}else{
    		$("#betTraceData").find("input:checkbox").prop("checked",false);
    		$("#betTraceData").find("tr").removeClass("on");
    	}
    	processBetTraceCore.calcView();
	}).on("click","#creatGtjhBtn",function(){		//生成追号
		var qiShu = $("#lt_trace_count_input").val(),beiShu=$("#lt_trace_times_same").val();
    	if(!/^[\d]{1,2}$/.test(qiShu) || qiShu>30 || qiShu<1){
    		layer.tips("追号期数必须大于0","#lt_sametime_html",{tips:[1,'#f13131']});
    		return false;
    	}
    	if(!/^[\d]{1,8}$/.test(beiShu) || beiShu <1){
    		layer.tips("倍数必须为整数","#lt_trace_times_same",{tips:[1,'#f13131']});
    		return false;
    	}
    	processBetTraceCore.startQiShu=qiShu;
    	processBetTraceCore.startTimes=beiShu;// 起始倍数
        processBetTraceCore.generateBetView();
	}).on("keyup blur","#lt_selector textarea",function(evt){	//textarea
		var it=$(this),val=it.val(),pattern=new RegExp(it.attr("_pattern")),type=it.attr("_type"),desc=it.attr("_desc")||"";
    	if(val){
    		var vs=val.split(/，|;|\r|\n| /);
        	for(var i =0;i<vs.length;i++){
        		if(vs[i] && !pattern.test(vs[i])){
        			if(evt.type=="blue"||evt.type=="focusout")layer.msg(vs[i]+"格式不对。"+desc,{icon:5});
        			LeCai.betting.sumTextareaBetCount(type,null);
        			return;
        		}
        	}
        	LeCai.betting.sumTextareaBetCount(type,vs);
    	}
	}).on("click","#submitDirectly",function(){	//直接下注
		//alert("大类=" + LeCai.groupCode + ",小类=" + LeCai.playCode + ",type=" + LeCai.type + ",彩种=" + LeCai.code);
		//LeCai.ajax.LotteryV3_RateOp();
		if(!LeCai.isLogin){
			LeCai._isLoginBtn();
			return;
		}
		var num = $("#lt_selector .gd").find("div.on"),$area=$("#lt_write_box"),val = $area.val(),pattern=new RegExp($area.attr("_pattern")),desc=$area.attr("_desc")||"";
		if($area.length>0){
			if(!val){
				layer.msg("请手动输入号码!!",{icon:2});
				return false;
			}else{
				var vs=val.split(/，|;|\r|\n| /);
	        	for(var i =0;i<vs.length;i++){
	        		if(vs[i] && !pattern.test(vs[i])){
	        			layer.msg(vs[i]+"格式不对。"+desc,{icon:5});
	        			return false;
	        		}
	        	}
			}
		}else{
			if(!num.length){
				layer.msg("请选择下注号码!!",{icon:2});
				return false;
			}
		}
		if($("#lt_sel_nums").text() == 0){
			layer.msg("请继续选择号码",{icon:2});
			return false;
		}
		LeCai.direct = true;
		$("#lt_buy").click();
	}).on("click","#lt_buy",function(){		//注单提交
			if(!LeCai.isLogin){
				LeCai._isLoginBtn();
				return false;
			}
			if(!LeCai.curQiHao){
				layer.msg("当前期号为空，不能下注",{icon:5});
				return false;
			}
			if(!LeCai.direct){
				var trs = $main.find("#lt_cf_content tr");
				if(!trs.length){
					layer.msg("至少添加一组投注号码",{icon:5})
					return false;
				}
				if(trs.length>LeCai.maxItem){
					layer.msg('投注号码数不能多于'+Core.maxItem+'！' ,{icon:5});
					return false;
				}
			}
			var tracecount = parseInt($("#lt_trace_count").text() || 0)||0,
	 		allAmount=0,zhuiHao=(tracecount > 0 && $('#lt_trace_if').is(':checked')),
	 		nowbalance = parseFloat($("#gamePoint2").text().replace(/,/g,"") || 0)||0;
			if (zhuiHao) {
		    	 allAmount=(parseFloat($("#lt_trace_hmoney").text())||0);
		         if (nowbalance < allAmount) {
		        	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
		             return false;
		         }
			} else {
				if(!LeCai.direct){
					var buyNums = $('#lt_cf_nums').text();
					allAmount=(parseFloat($("#lt_cf_money").text())||0);
				}else{
					var buyNums = $('#lt_sel_nums').text();
					allAmount=(parseFloat($("#lt_sel_money").text())||0);
				}
		         if (nowbalance < allAmount) {
		        	 layer.msg('下注金额超过帐户可用余额！',{icon: 5});
		        	 LeCai.betting.resetClearCount();
		             return false;
		         }
			}
			var layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
	         $.getJSON(LeCai.url+"/lotteryBet/token.do",function(data){
	        	 if(data.success==0){
	        		 layer.msg(data.msg,{icon: 5});
	                 return false;
	        	 }
	        	 layer.close(layerIndex);
	        	 var msg = '',myBuyArray = [],dtitle='系统提示',qiHao=LeCai.curQiHao,
	        	 	postData={lotCode:LeCai.code,token:data.token,qiHao:qiHao};
	        	 if(!LeCai.direct){
	        		 $main.find("#lt_cf_content tr").each(function(i, j) {
		        		 myBuyArray.push($(this).attr('buyinfo'));
			         });
	        	 }else{
	        		 var hm=(bet_result.hasOwnProperty('optional')&&bet_result.optional)?(bet_result.optional+"@"+bet_result.myarray):bet_result.myarray;
	                 // 彩票code｜玩法id｜货币单位（1=元，10=角，100=分）｜投注倍数|投注号码
	                 var buyinfo = LeCai.code + '|' + LeCai.playCode + '|' + $(".choose-money li").attr("value") + '|'+$("#beiShu").val() + '|' + hm;
	                 myBuyArray.push(buyinfo);
	        	 }
	        	 postData.bets = myBuyArray;
	        	 if (!LeCai.direct && tracecount > 0 && $('#lt_trace_if').is(':checked')) {// 追号数据
	        		 myBuyArray=[];
	        		 myBuyArray.push($("#lt_trace_stop").prop("checked") ? 1 : 0);// 中奖是否停止追号
	                 var $betTr = $("#betTraceData tr");
	                 $betTr.each(function(i, j) {
	                	 j=$(j);
	                     if (j.find("input[type='checkbox']").prop("checked")) {
	                    	 j=j.find("input[qh]");
	                    	 myBuyArray.push(j.attr("qh")+"|"+j.val());
	                     }
	                 });
	                 dtitle = '确定要追号 ' + tracecount + ' 期 ？';
	                 msg += '<p>彩票种类 ： ' + $('h3.page_name').text() + '</p>';
	                 msg += '<p>追号总期数 ： ' + tracecount + '</p>';
	                 msg += '<p>追号总注数 ： ' + (parseInt($("#lt_cf_nums").text()) * tracecount) + ' ( 注 )</p>';
	                 msg += '<p>追号总金额 ： ' + allAmount + ' ( 元 )</p>';
	                 postData.tractData=myBuyArray;
	             } else {
	                 msg += '<p>彩票种类 ： ' + $('h3.page_name').text() + '</p>';
	                 msg += '<p>下注期号 ： 第 ' + qiHao + ' 期</p>';
	                 msg += '<p>下注注数 ： ' + buyNums + ' ( 注 )</p>';
	                 msg += '<p>下注总额 ： ' + allAmount + ' ( 元 )</p>';
	             }
	        	 layer.confirm(msg, {title:dtitle,area:["400px",'auto'],closeBtn: 0,offset:['20%']}, function(){
	        		 layerIndex = layer.load(0,{shade: [0.3,'#000'],skin:''});
	        		 $.ajax({url:LeCai.url + "/lotteryV3/betInfo/doBets.do",
	        			 type: 'POST',
	                     dataType: 'json',
	                     data: {data:JSON.stringify(postData)},
	                     success: function(data) {
	                    	 layer.close(layerIndex);
	                    	 if(!data.success){
	                    		 layer.msg(data.msg,{icon: 5});
	                    		 LeCai.betting.resetClearCount();
	                    		 LeCai.direct = false;
	                             return false;
	                    	 }
	                    	 layer.msg("下单成功",{icon: 6});
	                    	 LeCai._clearAllItem();
	                    	 $(".lt_trace_if input").prop("checked",false);
	                    	 $("#lt_trace_box").hide();
	                    	 LeCai.direct = false;
	                    	 LeCai.betting.resetClearCount();
	                    	 getBcLotteryOrder(1);
	                     },
	                     successIntercept: function(data){
	                    	 layer.close(layerIndex);
	                     }
	        		 });
	        	 }, function(i){
	        		 LeCai.direct = false;
	        		 LeCai.betting.resetClearCount();
	        		 layer.close(i);
	        	 });
	        	 if (!LeCai.direct && tracecount > 0 && $('#lt_trace_if').is(':checked') && processBetTraceCore.thisOrderMaybeLose) {
	            	 layer.alert('您的追号方案可能亏损，建议您修改方案！',{skin:'layui-layer-dialog layui-layer-rim',type:1,btn:["确定"],offset:150},function(i){
	            		 layer.close(i);
	            	 });
	             }
	         });
	}).on("click","#refreshBettingHistory",function(){	//刷新下注注单
		if(!LeCai.isLogin){
			LeCai._isLoginBtn();
			return false;
		}
		getBcLotteryOrder(1);
	}).on("keydown","#beiShu",function(e){
		var code = null;
		//只能输入数字
		 if($.browser.msie){
			 code = event.keyCode;
		 }else{
			 code = e.which;
		 }
		 //48 - 57 上排数字键    96 - 105 数字键盘   8 删除键
		 if (((code > 47) && (code < 58)) || (code == 8) || (code >= 96 && code <= 105)) {  
             return true;  
         } else {
        	 layer.tips("请输入整数",$(this),{tips:[1,'#f13131']});
             return false;  
         }  
	}).focus(function(){
		this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符
	});
	
	$("#betTraceData").on("click","input[type=checkbox]",function(){
		var it = $(this);trI = it.parent().parent();
		if(it.is(":checked")){
			trI.addClass("on");
		}else{
			trI.removeClass("on");
		}
	})
})


// 机选
$(function() {
	var rand=function(a, e) {// 随机数
        var g = arguments.length;
        if (0 === g) a = 0, e = 2147483647;
        else if (1 === g) throw Error("Warning: rand() expects exactly 2 parameters, 1 given");
        return Math.floor(Math.random() * (e - a + 1)) + a
    },
    in_array=function(a,e, g) {// 数据是否在数字里面
        var c = "";
        if (g){
        	for (c in e) {
                if (e[c] === a) return !0
            }
        }else{
        	for (c in e) {
                if (e[c] == a) return !0
            }
        }
        return !1
    };
    var h = {// 选择投注号码方法
        select: function(a, c) {
            null == c && (c = 1);
            if (c >= a.length) return a.click(), !0;
            var e;
            for (var g = 0; g < c; g++){
            	e = rand(0, a.length - 1);
            	if(g === c - 1){
            		a.eq(e).click();
            		$("#addBettingNumberId").trigger("click");
            	} else{
            		if("checkbox" === a.eq(e).attr("type")){
            			a.eq(e).prop("checked", !0)
            		} else{
            			a.eq(e).addClass("on");
            			$("#addBettingNumberId").trigger("click");
            		}
            	}
            	a.splice(e, 1)
            }
            return !0
        },
        selectByLine: function(a, c) {
            var e=this;
            null == a && (a = 1);
            null == c && (c = ".gds .gd");
            a =parseInt(a);
            return $(c).each(function(c, g) {
                return e.select($(this).find("div[num]:not(.on)"), a)
            })
        },
        selectChkbox: function(a) {
            null == a && (a = 1);
            a = parseInt(a);
            var c = $(".selposition input").prop("checked", !1);
            return this.select(c, a)
        },
        selectXor: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".gds .gd");
            a instanceof Array || (a = parseInt(a));
            var e = this;
            return $(c).each(function(g, h) {
                var l = $(this).find("div[num]:not(.on)");
                $(c).find("div[num].on").each(function(a, c) {
                    return l = l.filter(":not([num=" + $(this).attr("num") + "])")
                });
                return a instanceof Array ? e.select(l, a[g] || 1) : e.select(l, a)
            })
        },
        randSelector: function(a, c) {
            null == a && (a = 1);
            null == c && (c = ".gds .gd");
            a = parseInt(a) || 1;
            var e = $(c).length - 1;
            if (e > a - 1) {
                var h = [],l=[],g;
                while(l.length<a){
                	g = rand(0, e);
                	if(!in_array(g, l)){
                		l.push(g);
                		h.push(c + ":eq(" + g + ")")
                	}
                }
                c = h.join(",")
            }
            return c
        }
    };
    $(".tp-ui-number-addto").click(function(){
    	var sjNum = $("#randomSelNumInput"),v=parseInt(sjNum.val());
    	if((v+1)>=10){
    		layer.tips("随机注数必须小于" + 10 + "注",sjNum,{tips:[1,'#f13131']});
    		sjNum.val(10);
    		return false;
    	}
    	sjNum.val(v+1);
    })
    $(".tp-ui-number-reduce").click(function(){
    	var sjNum = $("#randomSelNumInput"),v=parseInt(sjNum.val());
    	if((v-1)<=0){
    		layer.tips("随机注数必须大于1注",sjNum,{tips:[1,'#f13131']});
    		sjNum.val(1);
    		return false;
    	}
    	sjNum.val(v-1);
    })
	$("#randomSelBtn").click(function() {// 机选
		var rs = $("#randomSelNumInput").val();
		$(".gds .on").click();
		for(var i=0;i<rs;i++){
	        switch (LeCai.playCode) {
	        case "dwd":
	        case "q2zx_hz":
	        case "h2zx_hz":
	        	h.selectByLine(1,h.randSelector(1));
	        	break;
	        case "h3zux_zu3":
	        case "q3zux_zu3":
	        case "z3zux_zu3":
	        case "bdw_2m":
	        case "zux_z3":
	        	h.selectByLine(2);
	        	break;
	        case "h3zux_zu6":
	        case "q3zux_zu6":
	        case "z3zux_zu6":
	        case "zux_z6":
	        	h.selectByLine(3);
	        	break;
	        case "rxwf_r2zx_fs":
	        	h.selectByLine(1,h.randSelector(2));
	        	break;
	        case "rxwf_r3zx_fs":
	        	h.selectByLine(1,h.randSelector(3));
	        	break;
	        case "rxwf_r4zx_fs":
	        	h.selectByLine(1,h.randSelector(4));
	        	break;
	        case "rxwf_r3zux_zu3":
	        	h.selectChkbox(3);
	        	h.selectByLine(2);
	        	break;
	        case "rxwf_r3zux_zu6":
	        	h.selectChkbox(3);
	        	h.selectByLine(3);
	        	break;
	        case "h2zx":
	        case "q2zx":
	        case "em_h2zux":
	        case "em_q2zux":
	        case "rxfs_rx2z2":
	        	h.selectXor(2);
	        	break;
	        case "q3zx_fs":
	        case "q2zx_fs":
	        case "h3zx_fs":
	        case "z3zx_fs":
	        case "h2zx_fs":
	        	if(LeCai.type==3 ||LeCai.type==5){// 北京赛车 //11选5
	        		h.selectXor();
	        		break;
	        	}
	        	h.selectByLine();
	        	break;
	        case "sxzx":
	        case "h3zx":
	        case "q3zx":
	        case "z3zx":
	        case "rxfs_rx3z3":
	        	h.selectXor(3);
	        	break;
	        case "rxfs_rx4z4":
	        	h.selectXor(4);
	        	break;
	        case "rxfs_rx5z5":
	        	h.selectXor(5);
	        	break;
	        case "rxfs_rx6z5":
	        	h.selectXor(6);
	        	break;
	        case "rxfs_rx7z5":
	        	h.selectXor(7);
	        	break;
	        case "rxfs_rx8z5":
	        	h.selectXor(8);
	        	break;
	        default:
	        	h.selectByLine();
	        	break;
	        }
		}
    });
});


//刷新余额
function refreshMoney(){
	var money = $("#gamePoint2"),lhcAccount=$("#userName"),lhcMoney=$(".refreshM");
	$.ajax({
		url:LeCai.url + "/meminfo.do",
		success:function(res){
			if(res.login){
				money.html(res.money);
				lhcAccount.text(res.account);
				lhcMoney.text(res.money);
				LeCai.curMoney = res.money;
				LeCai.isLogin = true;
			}else{
				money.html("0");
				lhcAccount.html('请&nbsp;<a href="javascript://" onclick="LeCai._isLoginBtn()">登录</a>');
				lhcMoney.text('0');
				LeCai.isLogin = false;
			}
		},
		beforeSend:function(){
			money.html("加载中...");
			lhcMoney.text("加载中...");
		} 
	})
}


//定义参数
var LeCai = {
	maxItem:100,
	price:2,	
	url:null,	//base路径
	code:null,	//彩种code
	groupCode:null,	//彩种大类
	playCode:null,	//彩种小类
	diffs:57600,
	oneDay:24*60*60,
	type:null, 	//彩种type
	curQiHao:null,	//cur期号
	direct:false,	//是否是直接下注
	isOpen:true,	//是否是开盘状态
	isLogin:false,
	curMoney:0,	//当前余额
	_init:function(){
		LeCai.code = LECAI.code;
		LeCai.url = LECAI.base;
		LeCai.type = LECAI.type;
		LeCai.ajax.LotteryV3_Op();
		$(".lt_trace_if input").prop("checked",false);
		refreshMoney();
		if(LeCai.isLogin){
			getBcLotteryOrder(1);
		}
	},
	_initTrigger:function(){
		$("#tabbar-div-s2 .u_tab_div>span.tab-front").bind("click",LeCai._initTriggerHandler).triggerHandler("click");
	},
	_initTriggerHandler:function(){
		LeCai.groupCode = $(this).attr("value");
		LeCai.template._GetContentHtml();
		$("#tabbar-div-s2 .u_tab_div>span.tab-front").unbind("click",LeCai._initTriggerHandler);
	},
	_tempPlayCodeTrigger:function(){
		$("#tabbar-div-s3 div.act").bind("click",LeCai._tempPlayCodeTriggerHandler).triggerHandler("click");
	},
	_tempPlayCodeTriggerHandler:function(){
		LeCai.playCode = $(this).find("span").attr("c");
		LeCai.ajax.LotteryV3_RateOp();
		$("#tabbar-div-s3 div.act").unbind("click",LeCai._tempPlayCodeTriggerHandler);
	},
	_addLotName:function(val){
		var _title = $(".page_name");
		_title.text(val);
	},
	_addLotQiShu:function(val){
		var _qishu = $("#current_sale");
		_qishu.text(val);
	},
	_addLotCurrentQiHao:function(val){
		var _qiHao = $("#current_issue");
		_qiHao.text(val);
	},
	_addLotLastOne:function(last){
		LeCai._clearIntervalLastOne();
		LeCai._clearIntervalsliderHaoMa();
		var _qiHao = $("#lt_gethistorycode"),_haoMa=$("#showcodebox"),template="",haoMa=last.haoMa;
		_qiHao.text(last.qiHao);
		var hs = haoMa.split(",");
		if(haoMa.indexOf("?") >=0){
			//说明还未开奖,计划执行号码循环滚动，5秒获取一次数据，直到有数据为止，提示正在开奖
			sliderHaoMa =  window.setInterval(function(){
				LeCai._initQuartzHaoMa(hs.length,last.year);
			},100);
			selectedLotLastQuartz = window.setInterval(function(){
				LeCai.ajax.LotteryV3_LastOne(last.qiHao);
			},5000);
			$("#lt_opentimebox").show();
			return;
		}
		switch(LeCai.code){
			case "BJSC":
				_haoMa.attr("class","pkshik");
				for(var i=0;i<hs.length;i++){
					template += '<li>'+hs[i]+'</li>';
				}
				break;
			case "PCEGG":
				_haoMa.attr("class","pcegghik");var sum=0;
				for(var i=0;i<hs.length;i++){
					sum += parseInt(hs[i]);
					if(i<hs.length-1){
						template += '<li>'+hs[i]+'</li><span class="plus">+</span>';
					}else{
						template += '<li>'+hs[i]+'</li><span class="plus">=</span>';
					}
				}
				template += '<li>'+sum+'</li>';
				break;
			case "LHC":
				_haoMa.attr("class","lhchik");
				for(var i=0;i<hs.length;i++){
					var rl = Base.lhcHaoMa.bose(hs[i]);
					var nl = Base.lhcHaoMa.zodiacName(hs[i],last.year);
					if(i==5){
						template += '<li class="bg'+rl+'">'+hs[i]+'<font>'+nl+'</font></li><span class="plus">+</span>';
					}else{
						template += '<li class="bg'+rl+'">'+hs[i]+'<font>'+nl+'</font></li>';
					}
				}
				break;
			default:
				for(var i=0;i<hs.length;i++){
					template += '<li>'+hs[i]+'</li>';
				}
				break;
		}
		_haoMa.html(template);
	},
	_addLotLastFive:function(arr){
		var _id = $("#gd-box2"),template = "",_typeHtml="开奖期号",_typeClass="gd-box-h";
		switch(LeCai.code){
			case "BJSC":
				_typeHtml = "";
				_typeClass = "gd-box-p";
				break;
			case "LHC":
				_typeHtml = "";
				_typeClass = "gd-box-lhc";
				break;
			case "FFSC":
				_typeHtml = "";
				break;
		}
		$.each(arr,function(index,item){
			template += '<p>第 <span class="gd-box-q">'+item.qiHao+'</span> 期'+_typeHtml+':&nbsp;&nbsp;';
			var haoMa = item.haoMa.split(","),sum=0;
			switch(LeCai.code){
				case "PCEGG":
					for(var i=0;i<haoMa.length;i++){
						if(haoMa[i]!='?'){
							sum+=parseInt(haoMa[i]);
						}
						if(i<haoMa.length-1){
							template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span><span class="'+_typeClass+'">+</span>'
						}else{
							template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span><span class="'+_typeClass+'">=</span>'
						}
					}
					template += '<span class="'+_typeClass+'">'+(sum==0?"?":sum)+'</span>';
					break;
				case "LHC":
					for(var i=0;i<haoMa.length;i++){
						var rl = Base.lhcHaoMa.bose(haoMa[i]);
						if(i==5){
							template += '<span class="'+_typeClass+' bg'+rl+'">'+haoMa[i]+'</span><span class="plus_lhc">+</span>'
						}else{
							template += '<span class="'+_typeClass+' bg'+rl+'">'+haoMa[i]+'</span>';
						}
					}
					break;
				default:
					for(var i=0;i<haoMa.length;i++){
						template += '<span class="'+_typeClass+'">'+haoMa[i]+'</span>';
					}
					break;
			}
			template += '</p>';
		})
		_id.html(template);
	},
	_initQuartzHaoMa:function(len,year){
		var random = 0;
		var sumRandom = 0;
		var _haoMa=$("#showcodebox"),template="",_typeClass="lot-gd1";
		switch(LeCai.code){
			case "BJSC":
				random = parseInt(10*Math.random());
				_typeClass="pkshik";
				break;
			case "PCEGG":
				random = parseInt(10*Math.random());
				sumRandom = parseInt(27*Math.random());
				_typeClass="pcegghik"
				break;
			case "LHC":
				random = parseInt(49*Math.random());
				_typeClass="lhchik";
				break;
			default:
				random = parseInt(10*Math.random());
				break;
		}
		_haoMa.attr("class",_typeClass);
		switch(LeCai.code){
			case "PCEGG":
				for(var i=0;i<len;i++){
					if(i<len-1){
						template += '<li>'+random+'</li><span class="plus">+</span>';
					}else{
						template += '<li>'+random+'</li><span class="plus">=</span>';
					}
				}
				template += '<li>'+sumRandom+'</li>'
				break;
			case "LHC":
				for(var i=0;i<len;i++){
					var rl = Base.lhcHaoMa.bose(random);
					//var nl = Base.lhcHaoMa.zodiacName(random,year);
					if(i==5){
						template += '<li class="bg'+rl+'">'+random+'<font></font></li><span class="plus">+</span>';
					}else{
						template += '<li class="bg'+rl+'">'+random+'<font></font></li>';
					}
				}
				break;
			default:
					for(var i=0;i<len;i++){
						template += '<li flag="move">'+random+'</li>';
					}
				break;
		}
		_haoMa.html(template);
		return;
	},
	betting_hasOwnProperty:function(val){
		if(bet_result.hasOwnProperty('betCount')){
			 var r = LeCai.__FloatMul(bet_result.betCount, val)*LeCai.price;
	            r = LeCai.__FloatDiv(r, $(".choose-money li.on").attr("value"));
	            $("#lt_sel_money").text(r);
		}
		$("#lt_sel_beiShu").text(val);
		processBetTraceCore.startTimes = val;
		//计算中奖金额和可盈利金额
		LeCai.betting.__jugdeWinMoney();
	},
	strLimit: function(a, e) {
        e = e || 25;
        if (a.length < e) return a;
        var g = a;
        a.length > e && (g = g.substring(0, e) + "...");
        return g
    },
    countTotalBet: function() {
        var a = 0,e = 0;
        $("#lt_cf_content tbody tr").each(function(g, c) {
            var h = Number(parseFloat($(this).find("td:eq(5)").text()));
            e = LeCai.__FloatAdd(e, h);
            h = Number(parseInt($(this).find("td:eq(3)").text()));
            a = LeCai.__FloatAdd(a, h)
        });
        $("#lt_cf_money").text(parseFloat(e.toString().match(/\d+(\.\d{1,3})?/)[0] || 0));
        $("#lt_cf_nums").text(a)
    },
    __FloatAdd: function(a, e) {
        var g, c;
        try {
            g = a.toString().split(".")[1].length
        } catch (h) {
            g = 0
        }
        try {
            c = e.toString().split(".")[1].length
        } catch (l) {
            c = 0
        }
        g = Math.pow(10, Math.max(g, c));
        return (a * g + e * g) / g
    },
	__FloatMul: function(a, e) {
    	var g = 0,
            c = a.toString(),
            h = e.toString();
        try {
            g += c.split(".")[1].length
        } catch (l) {}
        try {
            g += h.split(".")[1].length
        } catch (m) {}
        return Number(c.replace(".", "")) * Number(h.replace(".", "")) / Math.pow(10, g)
    },
    __FloatDiv: function(a, e) {
        var g = 0,
            c = 0,
            h=a.toString(), l=e.toString();
        try {
            g = h.split(".")[1].length
        } catch (m) {}
        try {
            c = l.split(".")[1].length
        } catch (q) {}
        h = Number(h.replace(".", ""));
        l = Number(l.replace(".",""));
        return h / l * Math.pow(10, c - g)
    },
	_clearIntervalLastOne:function(){
		$("#lt_opentimebox").hide();
		clearInterval(selectedLotLastQuartz);
	},
	_clearIntervalsliderHaoMa:function(){
		clearInterval(sliderHaoMa);
	},
	_clearAllItem:function(){
		$("#lt_cf_content tbody").empty();
		LeCai.countTotalBet();
		processBetTraceCore.updateBetData();
	},
	_isLoginBtn:function(){
		if(!LeCai.isLogin){
			$('a[data-reveal-id]').trigger('click');
		}
	}
}


//ajax
LeCai.ajax = function(){
	return {
		//获取当前开奖期号和上五期号码
		LotteryV3_Op:function(){
			var data = {
				lotCode:LeCai.code
			}
			$.ajax({
				url:LeCai.url + "/lotteryV3/lotOp.do",
				data:data,
				success:function(res){
					if(!res.success){
						//有异常
						//$("#tzn_b_n_content").html('<div style="text-align:center;line-height: 270px;font-size: 100px;">'+res.msg+'</div>');
					}else{
						LeCai._addLotName(res.lotName);
						LeCai._addLotQiShu(res.qishu);
						LeCai.curQiHao = res.current.qiHao;
						LeCai._addLotCurrentQiHao(res.current.qiHao);
						LeCai._addLotLastOne(res.last);
						LeCai._addLotLastFive(res.history);
						LeCai.time.curData(res.current);
						if(LeCai.code == 'LHC'){
							Mark._initTrigger();
						}else{
							if(res.current.state==1){
								LeCai.isOpen = false;
								$("#tzn_b_n_content").html('<div style="text-align:center;line-height: 400px;font-size: 100px;background:#e6e6e6;">目前尚未开盘</div>');
								$("#tabbar-div-s2 .u_tab_div").html("");
								$("#peiLv,#fanShui").text("0");
							}else{
								LeCai.isOpen = true;
								LeCai._initTrigger();
							}
						}
					}
				}
			})
		},
		LotteryV3_LastOne:function(qiHao){
			var data = {
					qiHao:qiHao,
					lotCode:LeCai.code
			}
			$.ajax({
				url:LeCai.url + "/lotteryV3/lotLast.do",
				data:data,
				type:'GET',
				success:function(res){
					if(res.success){
						//说明上一期已开奖，刷新页面即可，停止定时器
						LeCai._clearIntervalLastOne();
						LeCai._clearIntervalsliderHaoMa();
						LeCai.ajax.LotteryV3_Op();
						$("#refreshBettingHistory").click();	//刷新一次下注结果
					}
				}
			})
		},
		LotteryV3_RateOp:function(){
			var data = {
					lotCode:LeCai.code,
					playCode:LeCai.playCode,
					lotType:LeCai.type
			}
			$.ajax({
				url:LeCai.url + "/lotteryV3/getRateOp.do",
				data:data,
				type:'GET',
				success:function(res){
					LeCai.template._LengRe(res.lengRe);
					LeCai.template._YiLou(res.yiLou);
					$("#peiLv").html(res.peiLv);
					$("#fanShui").html(res.fanShui);
				}
			})
		},
		LotteryV3_BetTrace:function(){
			$.ajax({
				url:LeCai.url + "/lotteryV3/getQiHaoForChase.do?lotCode=" + LeCai.code,
				cache:false,
				type:'get',
				dataType:'json',
				success:function(json){
					if(!json.success){
	            		layer.alert(json.msg,{icon:5,offset : ['30%' ]});
	            		return;
	            	}
	            	if(json.qiHaos && json.qiHaos.length>0){
	            		processBetTraceCore.initView(json.qiHaos);
	                	//$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
	            	}else{
	            		layer.alert("今日已经没有期号可追号！",{icon:5,offset : ['30%' ]});
	            	}
				}
			})
		}
	}
}();

LeCai.time = function(){
	var a=0,b=0,closeTime=0,nowTime=0,t1,t2,state,diffs=57600;
	return{
		curData:function(cur){
			if(!cur){
				//读取不到当前期号处理结果
			}
			closeTime = cur.activeTime;
			nowTime = cur.serverTime;
			state = cur.state;
			LeCai.curQiHao = cur.qiHao;
			this.start();
		},
		start:function(){
			var diff = this.timeDiff();
			this.stop();
			this._tick(parseInt(diff));
		},
		stop:function(){
			clearTimeout(t1);
		},
		_setOpenData:function(temp){
			$("#count_down").html(temp);
		},
		_setCloseData:function(temp){
			$("#openTitle").html(temp);
		},
		timeFormat:function(e){
			return new Date(e.replace(/-/g,"/"));
		},
		timeDiff:function(){
			a = this.timeFormat(closeTime);
			b = this.timeFormat(nowTime);
			return (new Date(a-b).getTime())/1E3;
		},
		timeDiffServerAndNowTime:function(){
			var a = new Date();
			var b = this.timeFormat(nowTime);
			return (new Date(a-b).getTime())/1E3;
		},
		abc:function(){
			var t = this;
			return t.timeDiff() - t.timeDiffServerAndNowTime();
		},
		formatHHMMSS:function(e,o){
			var format = "hh:mm:ss";
//			if(LeCai.code == 'LHC'){
//				format = "h:mm:ss";
//			}
			return $.formatDate(format,new Date((e+diffs)*1E3),o);
		},
		_tick:function(diff){
			var a = this;
//			if(diff - a.abc()>1){
//				diff = parseInt(a.abc())+1;
//			}
			a.stop();
			if(state==1){
				LeCai.isOpen = false;
				if(LeCai.code == 'LHC'){
					Mark.closeOrOpenOddsText('close');
				}
				a._setCloseData("已封盘,距离开盘截至还有");
			}else{
				LeCai.isOpen = true;
				a._setCloseData("已开盘,距离封盘截止还有");
			}
			if(diff>0){
				//时间填充到html
				var s = a.formatHHMMSS(diff,a.numberDay(diff));
				a._setOpenData(a._addTimeClass(s));
				if(diff<=10){//是开奖状态才需要播报
					//开始音频播报
					LeCai.audio.soundDingPlay();
				}
				//执行定时器
				t1 = setTimeout(function(){
					diff = diff - 1;
					a._tick(diff);
				},1000);
			}
			if(diff == 0){
				//封盘状态。直接加载下一期进行投注,显示提示框提示该期已停止下注
				this.stop();
				LeCai.ajax.LotteryV3_Op();
				layer.alert("您好，第 "+LeCai.curQiHao+" 期 已截止，投注时请确认您选的期号。",{time:3000,offset : ['30%' ]})
				LeCai.audio.soundNewKjPlay();
			}
		},
		timeChaZhi:function(){
			
		},
		numberDay:function(d){
			if(LeCai.oneDay<d){
				return Math.floor(d/LeCai.oneDay);
			}
			return 0;
		},
		_addTimeClass:function(t){
			var temp = "";
			for(var i=0;i<t.length;i++){
				if(t[i] == ":"){
					temp += '<span class="interval">:</span>';
				}else{
					temp += '<span class="leavehms"><span>'+t[i]+'</span></span>';
				}
			}
			return temp;
		}
		
	}
}();

/**
 * 模版加载
 */
LeCai.template = function(){
	return {
		_GetContentHtml:function(){
			$.ajax({
				url:LECAI.base+'/member/'+LECAI.folder+'/module/lottery/lecai/lottype_'+LeCai.type+'.html?v='+LECAI.caipiao_version,
				type:"GET",
				dataType:"html",
				success:function(html){
					var res = LeCai.template.splitTemHtml(html);
					//操作html内容分隔
					//先记录第一次的小类加载
					if(LeCai.code == 'LHC'){
						$("#bet_panel").html(res);
						Mark._tempPlayCodeTrigger();
					}else{
						$("#tzn_b_n_content").html(res);
						LeCai._tempPlayCodeTrigger();
					}
				}
			})
		},
		splitTemHtml:function(h){
			var group = "group_"+LeCai.type+"_"+LeCai.groupCode,gt={},pt={},resTem="";
			gt = h.split("\u263b");
			if(LeCai.code == 'LHC'){
				group = group + "\u263a";
			}
			for(var i=0;i<gt.length;i++){
				if(gt[i].indexOf(group)>=0){
					h = gt[i];
				}
			}
			if(LeCai.code == 'LHC'){
				resTem = h.split("\u263a")[1];
			}else if(!LeCai.playCode){
				h = h.split("\u263d")[0];
				resTem = h.split("\u263e")[1];
			}else{
				var play = "play_" + LeCai.playCode + "_" + LeCai.type;
				pt = h.split("\u263d");
				for(var i=0;i<pt.length;i++){
					if(pt[i].indexOf(play)>=0){
						h = pt[i];
					}
				}
				resTem = h.split("\u263e")[1];
			}
			return resTem;
		},
		_LengRe:function(arr){
			if(!arr){return;}
			$("#lt_selector").find(".gds").each(function(index,item){
				var a = arr[index],temp="";
				$.each(a,function(len,num){
					temp += '<span>'+num+'</span>'
				})
				$(item).find(".leng_re>._data_arr").html(temp);
			})
		},
		_YiLou:function(arr){
			if(!arr){return;}
			$("#lt_selector").find(".gds").each(function(index,item){
				var a = arr[index],temp="";
				$.each(a,function(len,num){
					temp += '<span>'+num+'</span>'
				})
				$(item).find(".yi_lou>._data_arr").html(temp);
			})
		}
	}
}();


//投注相关
LeCai.betting = function(){
	return {
		resetClearCount:function(){
			$("#lt_selector").find("div.gd div").removeClass("on");
			$("#lt_selector").find("div.to li").removeClass("on");
			LeCai.betting.resetSumbetCount();
		},
		resetSumbetCount:function(){
			bet_result = {};
			$("#lt_sel_nums,#lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0);
			$("#lt_write_box").val("");
		},
		sumbetCount: function() {// 计算投注注数和投注金额入口
				var c2 =$("#tzn_b_n_content").find(".tbn_cen"),
				_type=c2.attr("_type"),
				_row = c2.attr('_row'),
		        _column = c2.attr('_column'),
		        _val = c2.attr('_val');
	        bet_result = {};
	        bet_result.types = _type;
	        switch (_type) {
	            case "1":
	                LeCai.betting.__Duplex(_row, _column);
	                break;
	            case "3":
	                LeCai.betting.__Positioninggall(_row, _column);
	                break;
	            case "4":
	                LeCai.betting.__ElectionsAndValues(_row, _column);break;
	            case "5":
	                LeCai.betting.__Constitute(_row, _column, _val, c2.attr('_arbitrary'));
	                break;
	            case "8":
	                LeCai.betting.__Arbitrary_Duplex(_row, _column);break;
	            case "11":
	                LeCai.betting.__Nrepeat(_row, _column);break;
	            case "12":
	                LeCai.betting.__UseC(_row, _column);break;
	        }
	    },
	    sumTextareaBetCount: function(_type,_vals) {// 计算单式投注注数和投注金额入口
	    	bet_result = {};
	    	bet_result.types = _type;
	    	var zhuShu = 0, ma="";
	    	if(_vals){
	    		for(var i=0,len=_vals.length;i<len;i++){
		    		if(_vals[i]){
		        		ma=ma+";"+_vals[i];
		        		zhuShu++;
		    		}
		    	}
	    	}
	        bet_result.betCount = zhuShu;
	        bet_result.myarray =ma?ma.substring(1):"";
	        bet_result.errorTagStr = "";
	        bet_result.optional = "";
	        $("#lt_sel_nums").text(zhuShu);
	        var l = LeCai.price,s=$("#beiShu").val(),t=$(".choose-money li.on").attr("value");
	        l *=LeCai.__FloatMul(zhuShu,s);
	        l = LeCai.__FloatDiv(l, t);
	        $("#lt_sel_money").text(l);
	    },
	    __Duplex: function(_row, _column) {
	    	_row = _row || 1;
	    	_column = _column || 1;
	        var arr = [],row = 0,column = 0,zhuShu = 1;
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
	            		if("undefined" != typeof arr[a]){
	            			arr[a] += i;
	            		}else{
	            			arr[a] =i;
	            			row++;
	            		}
	            	}
	            })
	        });
	        if (row != _row || column < _column) {
	        	LeCai.betting.resetSumbetCount();
	        	return false;
	        }
	        for (var t in arr) zhuShu *= arr[t].length;
	        LeCai.betting.__showAndSaveBet(zhuShu, arr)
	    },
	    __Positioninggall: function(a, e) {
	        e = e || 1;
	        var g = 0,
	            c = 0,
	            h = 0,
	            l = [],q=LeCai.code,
	            m = Array(parseInt(a || 5));
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            		}else{
	            			m[a] =i;
	            		}
	            	}
	            });
	            h++
	        });
	        if (0 > h || c < e) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        for (var t in m) g += m[t].length;
	        if ("BJSC"  == q || "SD11X5" == q || "SH11X5" == q || "GX11X5" == q || "JX11X5" == q || "GD11X5" == q) g /= 2;
	        LeCai.betting.__showAndSaveBet(g, m)
	    },
	    __ElectionsAndValues: function(a, e) {
	        var c = 0,
	            h = 0,
	            l = 0,
	            m = [];
	        switch (e) {
	            case "J_3":
	                var q = [10, 54, 96, 126, 144, 150, 144, 126, 96, 54];
	                break;
	            case "S_3":
	                q = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 63, 69, 73, 75, 75, 73, 69, 63, 55, 45, 36, 28, 21, 15, 10, 6, 3, 1];
	                break;
	            case "G_3":
	                q = [-1, 1, 2, 2, 4, 5, 6, 8, 10, 11, 13, 14, 14, 15, 15, 14, 14, 13, 11, 10, 8, 6, 5, 4, 2, 2, 1];
	                break;
	            case "J_2":
	                q = [10, 18, 16, 14, 12, 10, 8, 6, 4, 2];
	                break;
	            case "S_2":
	                q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
	                break;
	            case "G_2":
	                q = [-1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1];
	                break;
	            case "S_1":
	                q = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
	        }
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		h++;
	            		m.push(i);
	            	}
	            });
	            l++
	        });
	        if (l < a) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        for (var t in m) c += parseInt(q[m[t]]);
	        LeCai.betting.__showAndSaveBet(c, m)
	    },
	    __Constitute: function(_row, _column, _val, _arbitrary) {
	        var h = 1,l = 1,column = 0,row = 0,arr = [];
	        _arbitrary = _arbitrary || 1;
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		column++;
	        			arr.push(i);
	            	}
	            });
	            row++;
	        });
	        if (row != _row || column < _column) {
	        	LeCai.betting.resetSumbetCount();
	        	return false;
	        }
	        var a=0;
	        switch (_val) {
	            case "G_2":
	                for (a = arr.length; a > arr.length - _column; a--) h *= a;
	                break;
	            case "P_1":
	            case "P_2":
	            case "P_3":
	            case "G_3":
	            case "R_2":
	                for (a = 0; a < _column; a++) h *= arr.length - a;
	                for (a = _column; 0 < a; a--) l *= a;
	                h /= l;
	                break;
	        }
	        var r;
	        switch (_arbitrary) {
	            case 1:
	                break;
	            default:{
	            	h *= $("#lt_selector .selposition span#positioninfo").text();
	                r = LeCai.betting.position_OptionalArr().join(",");
	            }
	        }
	        LeCai.betting.__showAndSaveBet(h, arr,false, r)
	    },
	    position_OptionalArr: function() {
	        var e = [];
	        $("#lt_selector .selposition input[name^='position_']").each(function() {
	            $(this).prop("checked") && e.push($(this).val())
	        });
	        return e
	    },
	    __Arbitrary_Duplex: function(a, e) {
	        var c = 0,
	            h = 0,
	            m = [],
	            q = [];
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a] += i;
	            			q[a] += 1
	            		}else{
	            			m[a] =i;
	            			q[a] = 1;
	            			h++;
	            		}
	            	}
	            });
	        });
	        if (h < a || c < e) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        var g = LeCai.betting.__Elections(q, e);
	        while(m.length<5){
	        	m[m.length]="";
	        }
	        LeCai.betting.__showAndSaveBet(g, m)
	    },
	    __Elections: function(a, e) {
	        var g = 0,
	            c = $.map(a, function(a) {
	                if ("" !== a) return a
	            }),
	            c = LeCai.betting.__C_combine(c, e);
	        switch (e) {
	            case "2":
	                for (var h in c) g += c[h][0] * c[h][1];
	                break;
	            case "3":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2];
	                break;
	            case "4":
	                for (h in c) g += c[h][0] * c[h][1] * c[h][2] * c[h][3]
	        }
	        return g
	    },
	    __C_combine: function(a, e) {
	        for (var g = [[]], c = [], h = 0, l = a.length; h < l; ++h)
	            for (var m = 0,q = g.length; m < q; ++m)(g[m].length < e - 1 ? g : c).push(g[m].concat([a[h]]));
	        return c
	    },
	    __Nrepeat: function(a, e) {
	        var g = 1,
	            c = 0,
	            h = 0,
	            l = [],
	            m = [];
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		c++;
	            		if("undefined" != typeof m[a]){
	            			m[a].push(i);
	            			l[a] += i
	            		}else{
	            			m[a]=[i];
	            			l[a] =i;
	            			h++;
	            		}
	            	}
	            })
	        });
	        if (h < a || c < e) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        switch (e) {
	            case "2":
	                for (var t in m) g *= LeCai.betting.___P(m[t].length, 1);
	                g -= LeCai.betting.arrayIntersection(m[0],m[1]).length;
	                break;
	            case "3":
	                g = m[0].length * m[1].length * m[2].length;
	                var q = LeCai.betting.arrayIntersection(m[0], m[1]).length * m[2].length;
	                var t = LeCai.betting.arrayIntersection(m[1], m[2]).length * m[0].length;
	                var r = LeCai.betting.arrayIntersection(m[2], m[0]).length * m[1].length,
	                    y = 2 * LeCai.betting.count(LeCai.betting.array_intersect(m[0], m[1], m[2])),
	                    g = g - q - t - r + y
	        }
	        LeCai.betting.__showAndSaveBet(g, l)
	    },
	    __UseC: function(a, e) {
	        var g = [],h = 0,l = 0,m = $("#tzn_b_n_content .tbn_cen").attr("_g11") || false;
	        $("#lt_selector .gds").each(function(a, e) {
	            $(e).find(".gd div.on").each(function(i,j) {
	            	j=$(j);
	            	i=j.attr("num");
	            	if(i){
	            		l++;
	            		g.push(i);
	        			h++;
	            	}
	            })
	        });
	        if (m) {
	            if (h < a || l < e) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        } else if (h != a || l < e) return $("#lt_sel_nums , #lt_sel_money,#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;
	        m = LeCai.betting.__C_combine(g, e).length;
	        LeCai.betting.__showAndSaveBet(m, g)
	    },
	    arrayIntersection: function(a, e) {
	        for (var g = 0, c = 0, h = []; g < a.length && c < e.length;) a[g] < e[c] ? g++ : (a[g] > e[c] || (h.push(a[g]), g++), c++);
	        return h
	    },
	    ___P: function(a, e) {
	        return LeCai.betting.___factorial(a) / LeCai.betting.___factorial(a - e)
	    },
	    ___C: function(a, e) {
	        return e > a ? 0 : LeCai.betting.___P(a, e) / LeCai.betting.___factorial(e)
	    },
	    ___H: function(a, e) {
	        return LeCai.betting.___C(e + a - 1, a - 1)
	    },
	    ___factorial: function(a) {
	        return 1 == a || 0 == a ? 1 : a * LeCai.betting.___factorial(a -1)
	    },
	    array_intersect: function(a) {
	        var e = {},
	            g = arguments.length,
	            c = g - 1,
	            h = "",
	            l = {},
	            m = 0,
	            q = "";
	        a: for (h in a) b: for (m = 1; m < g; m++) {
	            l = arguments[m];
	            for (q in l)
	                if (l[q] === a[h]) {
	                    m === c && (e[h] = a[h]);
	                    continue b
	                }
	            continue a
	        }
	        return e
	    },
	    count: function(a, e) {
	        var g, c = 0;
	        if (null === a || "undefined" ===
	            typeof a) return 0;
	        if (a.constructor !== Array && a.constructor !== Object) return 1;
	        "COUNT_RECURSIVE" === e && (e = 1);
	        1 != e && (e = 0);
	        for (g in a) a.hasOwnProperty(g) && (c++, 1 != e || !a[g] || a[g].constructor !== Array && a[g].constructor !== Object || (c += this.count(a[g], 1)));
	        return c
	    },
	    __showAndSaveBet: function(zhuShu, arr,c, h) {// 显示并记录投注信息
	    	zhuShu = zhuShu || 0;
	    	arr = arr || [];
	        c = c || [];
	        h = h || "";
	        bet_result.betCount = zhuShu;
	        var ma="";
	        for(var i=0,len=arr.length;i<len;i++){
	        	if(arr[i]){
	        		ma=ma+","+arr[i];
	        	}else{
	        		ma=ma+",-";
	        	}
	        }
	        bet_result.myarray =ma.substring(1);
	        bet_result.errorTagStr = c.toString();
	        bet_result.optional = h
	        $("#lt_sel_nums").text(zhuShu);
	        var l = LeCai.price,s=$("#beiShu").val(),t=$(".choose-money li.on").attr("value");
	        l *=LeCai.__FloatMul(zhuShu,s);
	        l = LeCai.__FloatDiv(l, t);
	        $("#lt_sel_money").text(l);
	        
	        LeCai.betting.__jugdeWinMoney();
	    },
	    __jugdeWinMoney:function(){
	    	var cu=0;
    		$("#lt_selector .gds").each(function(a, e) {
	        	if($(e).find(".gd div").hasClass("on")){
	        		cu++;
	        	}
	    	});
	    	var a=0,l=LeCai.price,
    		d = $("#lecaiMain"),t=d.find(".choose-money li.on").attr("value"),peiLv=d.find("#peiLv").text(),zs=d.find("#lt_sel_nums").text(),bs=d.find("#beiShu").val();
	    	if(zs==0){return $("#lt_sel_win_money,#lt_sel_profit_money").text(0), !1;}
    		a = LeCai.__FloatDiv(Number(peiLv*bs),t);
    		a = Number(a).toFixed(3);
    		l *= LeCai.__FloatMul(zs,bs);
    		l = LeCai.__FloatDiv(l,t);
    		switch(bet_result.types){
	    		case "1":
	    		case "4":
	    		case "8":
	    		case "5":
	    		case "11":
	    			cu = 0;
	    			break;
    		}
	    	if(cu>1){
	    		$("#lt_sel_win_money").text(a +"~" + Number(a*cu).toFixed(3));
	    		$("#lt_sel_profit_money").text(Number(a-l).toFixed(3) + "~" + Number(a*cu-l).toFixed(3));
	    	}else{
	    		$("#lt_sel_win_money").text(a);
	    		$("#lt_sel_profit_money").text(Number(a-l).toFixed(3));
	    	}
	    }
	}
}();

(function($){  
    $.formatDate = function(pattern,date,margin){  
        //如果不设置，默认为当前时间  
        if(!date) date = new Date();  
        if(typeof(date) ==="string"){  
             if(date=="")  date = new Date();  
              else  date = new Date(date.replace(/-/g,"/"));  
        }     
        /*补00*/  
        var toFixedWidth = function(value){  
             var result = 100+value;  
             return result.toString().substring(1);  
        };  
          
        /*配置*/  
        var options = {  
                regeExp:/(yyyy|M+|d+|h+|m+|s+|ee+|ws?|p)/g,  
                months: ['January','February','March','April','May',  
                         'June','July', 'August','September',  
                          'October','November','December'],  
                weeks: ['Sunday','Monday','Tuesday',  
                        'Wednesday','Thursday','Friday',  
                            'Saturday']  
        };  
          
        /*时间切换*/  
        var swithHours = function(hours){  
            return hours<12?"AM":"PM";  
        };
          
        /*配置值*/  
        var pattrnValue = {  
                "yyyy":date.getFullYear(),                      //年份  
                "MM":toFixedWidth(date.getMonth()+1),           //月份  
                "dd":toFixedWidth(date.getDate()),              //日期  
                "hh":toFixedWidth(date.getHours()- 8 + parseInt(margin*24)),             //小时  
                "mm":toFixedWidth(date.getMinutes()),           //分钟  
                "ss":toFixedWidth(date.getSeconds()),           //秒  
                "ee":options.months[date.getMonth()],           //月份名称  
                "ws":options.weeks[date.getDay()],              //星期名称  
                "M":date.getMonth()+1,  
                "d":date.getDate(),  
                "h":date.getHours(),  
                "m":date.getMinutes(),  
                "s":date.getSeconds(),  
                "p":swithHours(date.getHours())  
        };  
          
        return pattern.replace(options.regeExp,function(){  
               return  pattrnValue[arguments[0]];  
        });  
    };  
      
})(jQuery);




var processBetTraceCore = function(){
	return{
	    generatedTraceData: false,// 是否已经生成并显示追号期号
	    thisOrderMaybeLose: false,// 可能出现亏损
	    qiHaos:[],// 可追号得期号
	    startQiShu:10,// 追号期数
	    startTimes:1,// 起始倍数
	    totalCost:0,// 总下注金额
	    profitArr:false,// 盈利情况
	    initView:function(d){// 保存当天可追号得期号，并显示追号期号
	    	this.qiHaos = d;
	    	this.generateBetView();
	    },
	    generateBetView: function() {// 生成并显示追号期号
	    	if(!LeCai.curQiHao || !this.qiHaos||this.qiHaos.length==0)return;
	        this.generatedTraceData = true;
	        var html = "",len = this.qiHaos.length,qh,cur=false;
            for (var a = 0,b=0; a < len && b<this.startQiShu;a++) {
            	qh = this.qiHaos[a];
            	if(qh<LeCai.curQiHao){
            		continue;
            	}
                html+= '<tr class="on"><td><input type="checkbox" value="' +qh + '"checked></td><td>';
                if (!cur) {
                	html+=qh+ '(当前期)';
                	cur = true;
                } else {
                	html+=qh;
                }
                html+='</td><td class="nosel"><input type="text" class="r2" qh="'+qh+'" value="'+((b+1)*this.startTimes)+
                	'" autocomplete="off" maxlength="4">倍</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>';
                b++;
            }
            $("#betTraceData").html(html);
	        
            $("#betTraceData").find('input[type="checkbox"]').unbind('change').bind('change', function() {
	            processBetTraceCore.calcView();
	        });
            $("#betTraceData").find('input[type="text"]').unbind('keyup').bind('keyup', function() {
	            processBetTraceCore.calcView();
	        });
	        this.updateBetData();
	    },
	    updateBetData: function() {// 更新追号期号信息
	        if (!this.generatedTraceData) {
	            return false;
	        }
	        var trs=$("#lt_cf_content").find("tr[buyinfo]"),tongJi=true;
	    	if(trs.length>0){
	    		var playCode="",buyInfos=[],cost=0,c,e;
	    		trs.each(function(){
		    		var it = $(this);
		    		if(playCode==""){
		    			playCode=it.attr("c");
		    		}else if(playCode!=it.attr("c")){
		    			tongJi=false;
		    		}
		    		cost +=(parseFloat(it.attr("m"),10)||0);
		    		c = it.attr("buyinfo").split("\|");
	    			e={};
	    			e.cash=parseInt(c[3])/parseInt(c[2]);
		    		e.totalCost=cost;
	    			e.rate=(parseFloat(it.attr("r"),10)||0);
	    			e.rateCash=e.cash*e.rate;
	    			e.balls=c[4];
	    			e.ballArgs=[];
	    			e.balls.match("@")&&(c=e.balls.split("@"),e.balls=c[1],e.ballArgs=c[0].split(","));
	    			e.balls=e.balls.split(",");
	    			c=(LeCai.type==3 || LeCai.type==5)?/\d\d|龙|虎/g:/\d|大|小|单|双/g;
	    			for(var d in e.balls)
	    				e.balls[d]=e.balls[d].match(c);
		    		buyInfos.push(e);
		    	});
	    		if(!tongJi){
	    			layer.msg('您选择的多种玩法无法计算盈利，将以 -- 呈现！');
		    	}
	    		this.totalCost=cost;
    			processBetTraceCore.profitArr=tongJi?processBetTraceCore.calcProfit(buyInfos,playCode):false;// 计算盈利
	    		processBetTraceCore.calcView();
	        } else {// 还没有下注情况，不可以追号
	            this.totalCost = 0;
	            this.profitArr = false;
	            this.generatedTraceData = false;
	            $("#lt_trace_box").hide();
	            $("#lt_trace_if").prop("checked",false);
	        }
	    },
	    calcView: function() {// 计算投注金额及其盈利情况，并显示
	        var traceCount = 0,amount=0;
	        this.thisOrderMaybeLose = false;
	        var _this = this,totalCost=_this.totalCost,beforeCount=0;
	        $("#betTraceData").find('tr').each(function(i, j) {
	            j = $(this).children();
	            var betRate = parseInt(j.eq(2).children().val())||0;// 赔率
	            if (j.eq(0).children().prop('checked') && betRate > 0) {
	                traceCount++;
	                i=betRate * totalCost;
                	beforeCount+=i;
                	j.eq(3).text(i.toFixed(2));
                    j.eq(4).text(beforeCount.toFixed(2));
	                if (_this.profitArr === false) {
	                    j.eq(5).text('--');
	                    j.eq(6).text('--');
	                } else {
	                    var calcRes = _this.generatorBetTrace(betRate, beforeCount);
                        j.eq(5).text(calcRes.earnStr);
                        j.eq(6).text(calcRes.percentStr);
                        if (calcRes.minEarn < 0) {
                            _this.thisOrderMaybeLose = true;
                        }
	                }
	            } else {
	                j.eq(3).text('--');
	                j.eq(4).text('--');
	                j.eq(5).text('--');
	                j.eq(6).text('--');
	            }
	        });
	        $("#lt_trace_count").text(traceCount);// 追号期数
	        $("#lt_trace_hmoney").text(beforeCount.toFixed(2));// 追号总金额
	    },
	    calcProfit:function(buyInfos,playCode){// 计算下注盈利
	    	var a,r={};
	    	this.temp=[];
	    	for(var i = 0,len=buyInfos.length;i<len;i++){
	    		a=buyInfos[i];
	    		switch(playCode){
		    	case "dwd":// 定位胆
// switch(JIEBAO.lotType){
// case '1'://系统彩
// case '2'://时时彩
		    		r=this.multipleWinning(a.balls,a.rateCash);
// break;
// }
	    			break;
		    	case "q2zx_fs":
		    	case "h2zx_hz":
		    	case "q2zx_hz":
		    	case "h2zx_fs":
		    	case "q3zx_fs":
		    	case "z3zx_fs":
		    	case "h3zx_fs":
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    		break;
		    	case "bdw_q31m":
		    	case "bdw_h31m":
		    	case "bdw_z31m":
		    		this.combineProfitArray(r,a.balls,a.rateCash);break;
		    	case "rxwf_r2zx_fs":
		    		r=this.multipleWinning(this.anyDirect(2,a.balls),a.rateCash);break;
		    	case "rxwf_r3zx_fs":
		    		r=this.multipleWinning(this.anyDirect(3,a.balls),a.rateCash);break;
		    	case "rxwf_r3zux_zu3":
		    		r=this.multipleWinning(this.anyGroup(3,2,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r3zux_zu6":
		    		r=this.multipleWinning(this.anyGroup(3,3,a.balls,a.ballArgs),a.rateCash);break;
		    	case "rxwf_r4zx_fs":
		    		r=this.multipleWinning(this.anyDirect(4,a.balls),a.rateCash);break;
		    	case "q3zux_zu3":
		    	case "h3zux_zu3":
		    	case "z3zux_zu3":
		    		this.combineProfitArray(r,this.generatorGroup(2,a.balls),a.rateCash);break;
		    	case "q3zux_zu6":
		    	case "h3zux_zu6":
		    	case "z3zux_zu6":
		    		this.combineProfitArray(r,this.generatorGroup(3,a.balls),a.rateCash);break;
		    	default:
		    		this.combineProfitArray(r,this.expandArray(a.balls),a.rateCash);
		    	}
	    	}
    		var m=Number.MIN_VALUE,x=Number.MAX_VALUE,b;
			for(var d in r){
				b=r[d];
				m=Math.max(m,b);
				x=Math.min(x,b);
			}
			if(m!=Number.MIN_VALUE){
				return {maxProfit:parseFloat(m.toFixed(2)),minProfit:parseFloat(x.toFixed(2))};
	    	}
	    	return false;
	    },
	    generatorBetTrace:function(betRate, beforeCount){// 计算每一条追号得盈利
	    	var min = this.profitArr.minProfit,max = this.profitArr.maxProfit,minEarn=0,maxEarn=0,earnStr="",percentStr="";
	    	minEarn = min*betRate -beforeCount;
	    	maxEarn=max*betRate - beforeCount;
	    	earnStr = (minEarn==maxEarn)?minEarn.toFixed(2):minEarn.toFixed(2)+"至"+maxEarn.toFixed(2);
	    	percentStr = (minEarn==maxEarn)?((minEarn/beforeCount*100).toFixed(2)+"%"):((minEarn/beforeCount*100).toFixed(2)+"%")+"至"+((maxEarn/beforeCount*100).toFixed(2)+"%");
	    	return {earnStr:earnStr,percentStr:percentStr,minEarn:minEarn,maxEarn:maxEarn};
	    },
	    multipleWinning:function(b,d){
	    	var c,a,e,f,g,h;
	    	for(e in b){
	    		this.temp[e]instanceof Array||(this.temp[e]=[]),this.combineProfitArray(this.temp[e],b[e],d);
	    	}
    		h=g=0;
    		for(e in this.temp){
    			a=[];
    			for(f in this.temp[e])
    				a.push(this.temp[e][f]);
    			c=Math.max.apply(null,a);
    			a=Math.min.apply(null,a);
    			-Infinity===c&&(c=0);
    			Infinity===a&&(a=0);
    			g+=c;
    			h+=a
    		}
    		return{maxProfit:g,minProfit:h}
	    },
	    combineProfitArray:function(b,d,c){
	    	if(!(d instanceof Array))return!1;
	    	var a;
	    	for(var e= 0,f=d.length;e<f;e++){
	    		a=d[e],null!==a&&void 0!==a&&(b.hasOwnProperty(a)?b[a]+=c:b[a]=c);
	    	}
	    	return b
    	},
    	expandArray:function(b){
    		var d,c,a,e,f,g,h,k,l,n,m;
    		if(0===b.length)return!1;
    		b=b.slice();
    		m=b.splice(0,1)[0];
    		m instanceof Array||(m=m.toString().split(","));
    		f=0;
    		for(k=b.length;f<k;f++)
    			for(a=b[f],c=m.slice(),m=[],g=0,l=a.length;g<l;g++)
    				for(e=a[g],h=0,n=c.length;h<n;h++)
    					d=c[h],m.push(""+d+e);
    		return m
    	},
    	anyDirect:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G,J,L,K,M;
    		D=[];
    		if(2===b){
    			for(r=e=0,m=d.length-b;0<=m?e<=m:e>=m;r=0<=m?++e:--e){
    				if(null!==d[r]){
    					for(q=d[r],g=0,l=q.length;g<l;g++){
    						for(c=q[g],u=f=p=r+1,t=d.length;p<=t?f<t:f>t;u=p<=t?++f:--f){
    							if(null!==d[u]){
    								for(v=d[u],k=0,n=v.length;k<n;k++){
    									a=v[k];
    									h=""+r+u;
    									D[h]instanceof Array||(D[h]=[]);
    									D[h].push(""+c+a);
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(3===b){
    			for(r=n=0,z=d.length-b;0<=z?n<=z:n>=z;r=0<=z?++n:--n){
    				if(null!==d[r]){
    					for(B=d[r],m=0,f=B.length;m<f;m++){
    						for(c=B[m],u=q=C=r+1,A=d.length-1;C<=A?q<A:q>A;u=C<=A?++q:--q){
    							if(null!==d[u]){
    								for(E=d[u],p=0,k=E.length;p<k;p++){
    									for(a=E[p],g=t=x=u+1,w=d.length;x<=w?t<w:t>w;g=x<=w?++t:--t){
    										if(null!==d[g]){
    											for(y=d[g],v=0,l=y.length;v<l;v++){
    												e=y[v];
    												h=""+r+u+g;
    												D[h]instanceof Array||(D[h]=[]);
    												D[h].push(""+c+a+e);
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else if(4===b){
    			for(r=F=0,p=d.length-b;0<=p?F<=p:F>=p;r=0<=p?++F:--F){
    				if(null!==d[r]){
    					for(t=d[r],I=0,k=t.length;I<k;I++){
    						for(c=t[I],u=H=v=r+1,x=d.length-2;v<=x?H<x:H>x;u=v<=x?++H:--H){
    							if(null!==d[u]){
    								for(w=d[u],G=0,l=w.length;G<l;G++){
    									for(a=w[G],g=J=y=u+1,z=d.length-1;y<=z?J<z:J>z;g=y<=z?++J:--J){
    										if(null!==d[g]){
    											for(B=d[g],L=0,n=B.length;L<n;L++){
    												for(e=B[L],q=K=C=g+1,A=d.length;C<=A?K<A:K>A;q=C<=A?++K:--K){
    													if(null!==d[q]){
    														for(E=d[q],M=0,m=E.length;M<m;M++){
    															f=E[M];
    															h=""+r+u+g+q;
    															D[h]instanceof Array||(D[h]=[]);
    															D[h].push(""+c+a+e+f);
    														}
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}else {
    			layer.msg("任"+b+"直选错误",{icon:5});
    		}
    		return D
    	},
    	anyGroup:function(b,d,c,a){
    		var e,f;
    		null==b&&(b=2);
    		null==d&&(d=2);
    		null==c&&(c=[]);
    		null==a&&(a=['万','千','百','十','个']);
    		f=[];
    		b=parseInt(b);
    		d=parseInt(d);
    		c instanceof Array||(c=c.split(","));
    		if(a.length<b){
    			layer.msg("组选复式参数错误"),{icon:5};
    			return false;
    		}
    		c=this.generatorGroup(d,c);
    		e=this.generatorGroup(b,a);
    		b=0;
    		for(a=e.length;b<a;b++){
    			d=e[b];
    			f[d]=c;
    		}
    		return f
    	},
    	generatorGroup:function(b,d){
    		var c,a,e,f,g,h,k,l,n,m,q,p,r,u,t,v,x,w,y,z,B,C,A,E,D,F,I,H,G;
    		null==b&&(b=2);
    		null==d&&(d=[]);
    		G=[];
    		b=parseInt(b);
    		d=d instanceof Array?d.slice():d.splt(",");
    		if(1===b)return d;
    		if(2===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=0,p=d.length;l<p;l++)
    					u=d[l],G.push(""+r+u);
    		else if(3===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=n=0,q=d.length;0<=q?n<q:n>q;l=0<=q?++n:--n)
    					for(u=d[l],p=m=k=l+1,f=d.length;k<=f?m<f:m>f;p=k<=f?++m:--m)l=d[p],G.push(""+r+u+l);
    		else if(4===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=q=0,m=d.length-2;0<=m?q<m:q>m;l=0<=m?++q:--q)
    					for(u=d[l],p=k=f=l+1,w=d.length-1;f<=w?k<w:k>w;p=f<=w?++k:--k)
    						for(l=d[p],n=g=v=p+1,y=d.length;v<=y?g<y:g>y;n=v<=y?++g:--g)p=d[n],G.push(""+r+u+l+p);
    		else if(5===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=m=0,k=d.length-3;0<=k?m<k:m>k;l=0<=k?++m:--m)
    					for(u=d[l],p=f=g=l+1,w=d.length-2;g<=w?f<w:f>w;p=g<=w?++f:--f)
    						for(l=d[p],n=v=y=p+1,h=d.length-1;y<=h?v<h:v>h;n=y<=h?++v:--v)
    							for(p=d[n],q=x=z=n+1,c=d.length;z<=c?x<c:x>c;q=z<=c?++x:--x)
    								n=d[q],G.push(""+r+u+l+p+n);
    		else if(6===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,k=d.length-4;0<=k?f<k:f>k;l=0<=k?++f:--f)
    					for(u=d[l],p=v=w=l+1,g=d.length-3;w<=g?v<g:v>g;p=w<=g?++v:--v)
    						for(l=d[p],n=x=y=p+1,h=d.length-2;y<=h?x<h:x>h;n=y<=h?++x:--x)
    							for(p=d[n],q=C=z=n+1,c=d.length-1;z<=c?C<c:C>c;q=z<=c?++C:--C)
    								for(n=d[q],m=A=B=q+1,a=d.length;B<=a?A<a:A>a;m=B<=a?++A:--A)q=d[m],G.push(""+r+u+l+p+n+q);
    		else if(7===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=w=0,f=d.length-5;0<=f?w<f:w>f;l=0<=f?++w:--w)
    					for(u=d[l],p=y=g=l+1,v=d.length-4;g<=v?y<v:y>v;p=g<=v?++y:--y)
    						for(l=d[p],n=z=h=p+1,x=d.length-3;h<=x?z<x:z>x;n=h<=x?++z:--z)
    							for(p=d[n],q=c=C=n+1,B=d.length-2;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-1;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										m=d[k],G.push(""+r+u+l+p+n+q+m);
    		else if(8===b)
    			for(;d.length>=b;)
    				for(r=d.splice(0,1)[0],l=f=0,w=d.length-6;0<=w?f<w:f>w;l=0<=w?++f:--f)
    					for(u=d[l],p=g=v=l+1,y=d.length-5;v<=y?g<y:g>y;p=v<=y?++g:--g)
    						for(l=d[p],n=h=x=p+1,z=d.length-4;x<=z?h<z:h>z;n=x<=z?++h:--h)
    							for(p=d[n],q=c=C=n+1,B=d.length-3;C<=B?c<B:c>B;q=C<=B?++c:--c)
    								for(n=d[q],m=a=A=q+1,E=d.length-2;A<=E?a<E:a>E;m=A<=E?++a:--a)
    									for(q=d[m],k=e=D=m+1,F=d.length-1;D<=F?e<F:e>F;k=D<=F?++e:--e)
    										for(m=d[k],t=k=I=k+1,H=d.length;I<=H?k<H:k>H;t=I<=H?++k:--k)
    											t=d[t],G.push(""+r+u+l+p+n+q+m+t);
    		else layer.msg("长度组选错误",{icon:5});
    		return G
    	}
	}
}();

//下注情况数据
function getBcLotteryOrder(page){
	$.ajax({
		url:LeCai.url+'/lotteryBet/getBcLotteryOrder.do',
		data:{
			code :LeCai.code,
			rows :10,
			page :page,
			},
		success:function(r){
			var betListHtml ='';
			var list = r.page.list;
			var results = r.page;
			if(list.length==0){
				var dateTime = new Date();
				var year=dateTime.getFullYear();
				var month=dateTime.getMonth()+1;
				var date=dateTime.getDate();
				if(month<10){
					month =  "0"+month;
				}else{
					month = month;
				}
				if(date<10){
					date = "0" +date;
				}else{
					date = date;
				}
				var riqi = year+"-"+month+"-"+date;
				$("#betListBetween").text('报表日期：'+riqi);
				betListHtml = '<tr><td colspan="10">没有符合查询的资料...</td></tr>';
				$("#betOrderTblFoot").hide();
			}else{
				for(var i=0;i<list.length;i++){
					if(list[i].lotteryHaoMa == null || list[i].lotteryHaoMa==''){
						list[i].lotteryHaoMa = '- -';
	    			}
					var dateTime = list[0].createTime.split(" ");
					var data = dateTime[0];
					$("#betListBetween").text('报表日期：'+data);
					betListHtml+='<tr>'
					betListHtml+='<td><a style="width: 100%;" class="BetInf" orderid="'+list[i].orderId+'"lotcode="'+list[i].lotCode+'">'+list[i].orderId+'</a></td>'
					betListHtml+='<td>'+list[i].createTime+'</td>'
					betListHtml+='<td>'+list[i].qiHao+'</td>'
					betListHtml+='<td>'+list[i].playName+'</td>'
					betListHtml+='<td>'+list[i].multiple+'</td>'
					betListHtml+='<td>'+list[i].lotteryHaoMa+'</td>'
					betListHtml+='<td>'+list[i].buyMoney+'</td>'
					if(list[i].winMoney != null){
						betListHtml+='<td>'+list[i].winMoney+'</td>'
					}else{
						betListHtml+='<td>0.00</td>'
					}
					// 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
					if(list[i].status == 1 ){
						betListHtml+='<td><span style="background-color: #428bca;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">未开奖 </span></td><td><a class="cheDan" orderid="'+list[i].orderId+'" lotcode="'+list[i].lotCode+'">撤单</a></td>'
					}else if(list[i].status == 2 ){
						betListHtml+='<td><span style="background-color: #5cb85c;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">已中奖 </span></td><td></td>'
					}else if(list[i].status == 3 ){
						betListHtml+='<td><span style="background-color: #d9534f;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">未中奖 </span></td><td></td>'
					}else if(list[i].status == 4 ){
						betListHtml+='<td><span style="background-color: #5bc0de;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">已撤单 </span></td><td></td>'
					}else if(list[i].status == 5 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">回滚成功 </span></td><td></td>'
					}else if(list[i].status == 6 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">回滚异常 </span></td><td></td>'
					}else if(list[i].status == 7 ){
						betListHtml+='<td><span style="background-color: #f0ad4e;border-radius: .25em;padding: 0.2em .6em 0.3em;color:#fff;">开奖异常 </span></td><td></td>'
					}
				}
				$("#betPageAmount").html(r.subBuyMoney);
				$("#betAmount").html(r.sumBuyMoney);
				$("#betOrderTblFoot").show();
			}
			$("#betResultOrder").html(betListHtml);
			layerPage(r.page.totalCount,r.page.currentPageNo,r.page.totalPageCount);
		}
	})
};

/**
 * 音频
 */
LeCai.audio = function(){
	return {
		//选中
		soundBetSelectedPlay:function(){
			var sound = document.getElementById("sound_bet_select");
			sound.load();
			sound.play();
		},
		//倒计时
		soundDingPlay:function(){
			var sound = document.getElementById("sound_ding");
			sound.load();
			sound.play();
		},
		//新一期开奖
		soundNewKjPlay:function(){
			var sound = document.getElementById("sound_newkj");
			sound.load();
			sound.play();
		}
	}
}();