// 计算组合方式, 此方式m不可小于或者等于n，否则会导致死循环卡住 !!!!!
var getFlagArrs = function (m, n) {
  if(m<n){
    return  []
  }else if (m===n) {
    return  [0]
  }
  if (!n || n < 1) {
    return [];
  }
  var resultArrs = [],
    flagArr = [],
    isEnd = false,
    i, j, leftCnt;
  for (i = 0; i < m; i++) {
    flagArr[i] = i < n ? 1 : 0;
  }
  resultArrs.push(flagArr.concat());
  while (!isEnd) {
    leftCnt = 0;
    for (i = 0; i < m - 1; i++) {
      if (flagArr[i] == 1 && flagArr[i + 1] == 0) {
        for (j = 0; j < i; j++) {
          flagArr[j] = j < leftCnt ? 1 : 0;
        }
        flagArr[i] = 0;
        flagArr[i + 1] = 1;
        var aTmp = flagArr.concat();
        resultArrs.push(aTmp);
        if (aTmp.slice(-n).join("").indexOf('0') == -1) {
          isEnd = true;
        }
        break;
      }
      flagArr[i] == 1 && leftCnt++;
    }
  }
  return resultArrs;
}
