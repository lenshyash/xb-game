<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_userInfo_jsp">
		<jsp:include page="../include/footer.jsp"></jsp:include>
		<!-- 这里是页面内容区 -->
		<div class="content">
			<div class="tit">
				<img
					src="${base}/images/img_wd_tx_diban.png"
					alt=""> <a class="tit-box" href="${base}/m/toUser/info.do"> <img
					src="" class="thumb">
					<p class="change nickname">${loginMember.account}</p>
					<div class="change">
						<img
							src="${base}/common/template/lottery/v4/m/images/wd_bianji.png"
							alt="">
						<p class="autograph">这个人很懒没有签名</p>
					</div> <img
					src="${base}/common/template/lottery/v4/m/images/wd_jiantou_bai_you.png"
					alt="">
				</a>
			</div>
			<div class="weui-grids">
				<a href="${base}/m/toQb.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/ico-wallet.png" alt="">
					</div>
					<p class="weui-grid__label">钱包</p>
				</a> <a href="${base}/m/toUser/betRecord.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_youxijilu.png" alt="">
					</div>
					<p class="weui-grid__label">投注记录</p>
				</a> <a href="${base}/m/toQb/billsWebZB.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_jilu.png" alt="">
					</div>
					<p class="weui-grid__label">帐变记录</p>
				</a> 
				<a external href="${base }/m/signIn.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_huishui.png" alt="">
					</div>
					<p class="weui-grid__label">我的签到</p>
				</a> 
				<a href="${base}/m/active.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_woyaofenxiang.png" alt="">
					</div>
					<p class="weui-grid__label">优惠活动</p>
				</a> <a href="${base}/m/toUser/turnlate.do" class="weui-grid weui-member external">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_xingyunchoujiang.png" alt="">
					</div>
					<p class="weui-grid__label">幸运转盘</p>
				</a> <a href="javascript:;" class="weui-grid weui-member bqingIcon">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_biaoqing.png" alt="">
					</div>
					<p class="weui-grid__label">表情管理</p>
				</a> <a href="${base}/m/toUser/toSetUp.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/ico-setUp.png" alt="">
					</div>
					<p class="weui-grid__label">设置中心</p>
				</a> <a href="${base}/m/toUser/gywm.do" class="weui-grid weui-member">
					<div class="weui-grid__icon">
						<img src="${base}/common/template/lottery/v4/m/images/img_wd_pcdd.png" alt="">
					</div>
					<p class="weui-grid__label">关于我们</p>
				</a>
			</div>
		</div>
		<script type="text/javascript">
			var account = '${loginMember.account}';
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>