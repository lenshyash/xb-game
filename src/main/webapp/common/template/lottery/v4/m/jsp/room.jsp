<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_room_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">${name}</h1>
		</header>
		<%-- 这里是页面内容区  external--%>
		<div class="content">
			<c:forEach items="${roomList}" varStatus="rIndex" var="room">
				<div class="box-box" r_id="${room.id}" lot_type="${room.lotType}"
					max_m="${empty room.maxMoney?'0':room.maxMoney}"
					level="${empty room.levelId?'0':room.levelId}"
					status="${not empty room.argot?'1':'0'}">
					<%-- external   ${base}/m/toBetRoom.do?rId=${room.id}&lotType=${room.lotType}--%>
					<a class="back-btn" href="javascript:void(0);"><img
						class="cover" src="${base}${room.roomImg}"></a>
					<div class="fixed-seo1">
						<div class="seo-left">
							<span class="name">${room.name}</span> <span class="desc">${room.roomHouse}</span>
						</div>
						<div class="seo-right">
							<img
								src="${base}/common/template/lottery/v4/web/images/img_fj_ren.png"
								alt=""> <span class="current_users">${not empty room.argot?'保密':room.initialCount}人</span>
						</div>
					</div>
					<a class="seo-btn" href="javascript:void(0);"><img
						src="${base}/common/template/lottery/v4/web/images/img_peilvshuoming.png"
						alt=""></a>
					<div class="peilvshuoming hide">${room.brief}</div>
				</div>
			</c:forEach>
		</div>
		<script type="text/javascript">
			var userMoney = '${loginMember.money}';
		</script>
	</div>
	<jsp:include page="popup/wanfa_popup.jsp"></jsp:include>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>