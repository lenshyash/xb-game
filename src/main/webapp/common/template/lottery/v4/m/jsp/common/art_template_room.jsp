<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 发言 --%>
<script type="text/html" id="html_speak">
	<div class="card card-touzhu-right">
		<div class="card-content">
			<div class="touzhu-right clear">
				<div>
					<img src="${base}/common/template/lottery/v4/web/images/touxiang/{{nickUrl}}.jpg" alt="">
				</div>
				<div class="person-money-right" style="width:initial;">
					<div class="clear">
						<p>{{account}}</p>
						<img class="money-lv" src="{{levelImg}}" alt="">
					</div>
					<a class="person-money-detail clear" href="javascript:;" style="color:#fff;">
						<img src="${base}/common/template/lottery/v4/web/images/img_duihuakuang_sanjiao.png" alt="">
						<div class="clear" style="width:initial;">{{$showHtml message}}</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</script>
<%-- 其他投注 --%>
<script type="text/html" id="html_touzhu_left">
	{{each data as value i}}
	{{if accounts != value.account}}
		<div class="card card-touzhu-right">
		<div class="card-content">
			<div class="person-money clear">
				<div>
					<img src="${base}/common/template/lottery/v4/web/images/touxiang/{{value.nickUrl}}.jpg" alt="">
				</div>
				<div class="person-money-right">
					<div class="clear">
						<p class="accountName">{{value.account}}</p>
						<img class="money-lv" src="{{value.levelImg}}" alt="">
					</div>
					<a class="person-money-detail gentou-zhudan clear" href="javascript:;">
						<img src="${base}/common/template/lottery/v4/web/images/img_duihuakuang_sanjiao.png" alt="">
						<div class="clear">
						<img src="${base}/common/template/lottery/v4/web/images/img_fj_biao.png" alt="">
						<p>{{value.qiHao}}期</p>
						</div>
						{{each value.data as v i}}
							<p>投注类型：{{v.name}}</p>
							<p>投注金额：{{v.money}}元宝</p>
							<div style="display:none;" class="dataInfoCache hide" data-code="{{v.playCode}}" data-oddsid="{{v.oddsId}}" data-qihao="{{value.qiHao}}" data-name="{{v.name}}" data-money="{{v.money}}"></div>
						{{/each}}
					</a>
				</div>
			</div>
		</div>
	</div>
	{{else}}
	<div class="card card-touzhu-right">
		<div class="card-content">
			<div class="touzhu-right clear">
				<div>
					<img src="${base}/common/template/lottery/v4/web/images/touxiang/{{value.nickUrl}}.jpg" alt="">
				</div>
				<div class="person-money-right">
					<div class="clear">
						<p class="accountName">{{value.account}}</p>
						<img class="money-lv" src="{{value.levelImg}}" alt="">
					</div>
					<a class="person-money-detail gentou-zhudan clear" href="javascript:;"> 
					<img src="${base}/common/template/lottery/v4/web/images/img_duihuakuang_sanjiao.png" alt="">
					<div class="clear">
						<img src="${base}/common/template/lottery/v4/web/images/img_fj_biao.png" alt="">
						<p>{{value.qiHao}}期</p>
					</div>
					{{each value.data as v i}}
						<p>投注类型：{{v.name}}</p>
						<p>投注金额：{{v.money}}元宝</p>
						<div style="display:none;" class="dataInfoCache hide" data-code="{{v.playCode}}" data-oddsid="{{v.oddsId}}" data-qihao="{{value.qiHao}}" data-name="{{v.name}}" data-money="{{v.money}}"></div>
					{{/each}}
					</a>
				</div>
			</div>
		</div>
	</div>
	{{/if}}
	{{/each}}
</script>

<script type="text/html" id="html_tishi">
	<div class="card card-tishi">
		<div class="card-content">
			<p>
				<span>【{{qiHao}}期】</span><br>{{$showHtml message}}
			</p>
		</div>
	</div>
</script>

<script type="text/html" id="html_time">
	<div class="card card-time">
		<div class="card-content">2017-03-06 15:30</div>
	</div>
</script>

<script type="text/html" id="html_login">
	<div class="card card-huanying">
		<div class="card-content">
			<p>欢迎</p>
			<img class="person-lv" src="{{levelImg}}">
			<span>{{account}}</span>
			<p>进入房间</p>
		</div>
	</div>
</script>

<script type="text/html" id="historyLastTpl">
			<li {{if index%2==0}}style="background:#f9f9f9"{{/if}}>
				<div class="item-inner" style="padding-left: .75rem;">
				<div class="item-title-row" style="font-size: .7rem;"><div class="item-title">第{{qiHao}}期</div>
				<div class="item-after">{{time}}</div></div>
				<div class="item-subtitle item-openAward" style="line-height: 2rem;">
						{{if type == 158}}
							{{each hmArr as hm i}}
							<i class="color_history color{{hm}}">{{hm}}</i>
							{{/each}}
						{{else}}
							{{$hmTplResult hmArr type}}
						{{/if}}
				</div></div>
			</li>
</script>


<script type="text/html" id="gameTrendTpl">
	{{if type == 158}}
	<tr {{if index%2!=0}}style="background:#fafafa;"{{/if}}>
		<td>{{qiHao}}</td>
		<td><label>{{gyhz}}</label></td>
		{{if gyhz > 11}}
			<td><em class="colorDan" style="background:#DC5D55 ">大</em></td>
			<td></td>
		{{else}}
			<td></td>
			<td><em class="colorDan" style="background:#175AAE ">小</em></td>
		{{/if}}
		{{if gyhz%2==0}}
			<td></td>
			<td><em class="colorDan" style="background:#DC5D55 ">双</em></td>
		{{else}}
			<td><em class="colorDan" style="background:#175AAE ">单</em></td>
			<td></td>
		{{/if}}
	</tr>
	{{else if type == 160}}
		<tr {{if index%2!=0}}style="background:#fafafa;"{{/if}}>
			<td>{{qiHao}}</td>
			<td><label>{{gyhz}}</label></td>
			{{if gyhz >= 11}}
				<td><em class="colorDan" style="background:#DC5D55 ">大</em></td>
				<td></td>
			{{else}}
				<td></td>
				<td><em class="colorDan" style="background:#175AAE ">小</em></td>
			{{/if}}
			{{if gyhz%2==0}}
				<td></td>
				<td><em class="colorDan" style="background:#DC5D55 ">双</em></td>
			{{else}}
				<td><em class="colorDan" style="background:#175AAE ">单</em></td>
				<td></td>
			{{/if}}
		</tr>
	{{else}}
		<tr {{if index%2!=0}}style="background:#fafafa;"{{/if}}>
			<td>{{qiHao}}</td>
			<td><label>{{gyhz}}</label></td>
			{{if gyhz > 13}}
				<td><em class="colorDan" style="background:#DC5D55 ">大</em></td>
				<td></td>
			{{else}}
				<td></td>
				<td><em class="colorDan" style="background:#175AAE ">小</em></td>
			{{/if}}
			{{if gyhz%2==0}}
				<td></td>
				<td><em class="colorDan" style="background:#DC5D55 ">双</em></td>
			{{else}}
				<td><em class="colorDan" style="background:#175AAE ">单</em></td>
				<td></td>
			{{/if}}
			{{if gyhz>13&&gyhz%2!=0}}
				<td><em class="colorDan" style="background:#175AAE ">大单</em></td><td></td>
			{{else if gyhz>13&&gyhz%2==0}}
				<td></td><td><em class="colorDan" style="background:#175AAE ">大双</em></td>
			{{else}}
				<td></td><td></td>
			{{/if}}
			{{if gyhz<14&&gyhz%2!=0}}
				<td><em class="colorDan" style="background:#DC5D55 ">小单</em></td><td></td>
			{{else if gyhz<14&& gyhz % 2 ==0}}			
				<td></td><td><em class="colorDan" style="background:#DC5D55 ">小双</em></td>
			{{else}}
				<td></td><td></td>
			{{/if}}
		</tr>
	{{/if}}
</script>

<script type="text/html" id="gameTrendThTpl">
			{{if type == 158}}
				<th width="15">期号</th>
				<th width="9">冠亚和</th>
				<th width="9">和大</th>
				<th width="9">和小</th>
				<th width="9">和单</th>
				<th width="9">和双</th>
			{{else if type == 160}}
				<th width="15">期号</th>
				<th width="9">和</th>
				<th width="9">大</th>
				<th width="9">小</th>
				<th width="9">单</th>
				<th width="9">双</th>
			{{else}}
				<th width="15">期号</th>
				<th width="9">值</th>
				<th width="9">大</th>
				<th width="9">小</th>
				<th width="9">单</th>
				<th width="9">双</th>
				<th width="10">大单</th>
				<th width="10">大双</th>
				<th width="10">小单</th>
				<th width="10">小双</th>
			{{/if}}
</script>