<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="buttons-tab" style="${isIndex?'margin:.02rem 0 0;':'width: 96%; margin: 0.3rem 2% 0;'}">
	<c:set var="isPk10" value="false"></c:set>
	<c:set var="isK3" value="false"></c:set>
	<c:set var="isPcegg" value="false"></c:set>
	<c:forEach items="${bcList}" var="bl">
		<c:if test="${bl.type == 161}">
			<c:set var="isPcegg" value="true"></c:set>
		</c:if>
		<c:if test="${bl.type == 158}">
			<c:set var="isPk10" value="true"></c:set>
		</c:if>
		<c:if test="${bl.type == 160}">
			<c:set var="isK3" value="true"></c:set>
		</c:if>
	</c:forEach>
	<c:if test="${isPcegg}">
		<a href="#tab1" class="tab-link active button">pc蛋蛋</a>
	</c:if>
	<c:if test="${isPk10}">
		<a href="#tab2" class="tab-link button">PK10</a>
	</c:if>
	<c:if test="${isK3}">
		<a href="#tab3" class="tab-link button">快三</a>
	</c:if>
</div>