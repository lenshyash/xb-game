<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_game_rule_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${backUrl}"> <span class="icon icon-left"></span>
				返回
			</a>
			<h1 class="title">玩法介绍</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="card">
				<div class="card-content">
					<div class="list-block">
						<ul>
							<c:forEach items="${bcList}" var="bc" varStatus="bcIndex">
								<li code="${bc.code}" type="${bc.type}"><a href="javascript:void(0);"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon ico_${bc.code}"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">${bc.name}</div>
										</div>
								</a></li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../popup/wanfa_popup.jsp"></jsp:include>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>