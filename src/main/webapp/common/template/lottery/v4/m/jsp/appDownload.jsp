﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
<style type="text/css">
.btn {
    width: 239px;
    height: 45px;
    margin: 20px auto 20px;
}
</style>
</head>
<body>
	<div class="page" id="appDownload">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${base}/m">
				<span class="icon icon-left"></span>返回
			</a> <a class="title">APP下载</a>
		</header>
		<nav class="bar bar-tab">
			<span
				style="display: inline-block; width: 100%; color: #FFF; text-align: center; line-height: 2.5rem; font-size: .7rem; background: #525252;">版权所有：2009-2016©${website_name }</span>
		</nav>
		<div class="content">
			<!-- 这里是页面内容区 -->
			<div class="card facebook-card" style="margin: 0;min-height: 100%;">
				<div class="card-header no-border" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<a href="${appDownloadLinkIos}" class="external" target="_blank">
							<img src="${base}/mobile/v3/images/500vip/store_btn.png"
							style="max-width: 60%;margin:0 auto;">
						</a>
					</div>
				</div>
				<c:if test="${not empty appQRCodeLinkIos && appQRCodeLinkIos!=''}">
				<div class="card-content" style="text-align: center;">
					<img src="${appQRCodeLinkIos }" style="display: inline;width: 9rem;">
				</div>
				</c:if>
				<div class="card-header no-border" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<a href="${appDownloadLinkAndroid}" class="external"
							target="_blank"> <img
							src="${base}/mobile/v3/images/500vip/android_btn.png"
							style="max-width: 60%;margin:0 auto;">
						</a>
					</div>
				</div>
				<c:if test="${not empty appQRCodeLinkAndroid && appQRCodeLinkAndroid!=''}">
				<div class="card-content" style="text-align: center;">
					<img src="${appQRCodeLinkAndroid }" style="display: inline;width: 9rem;">
				</div>
				</c:if>
				<div class="card-footer no-border">
					<span style="display: inline-block; width: 100%;text-align: center; font-size: .6rem;">温馨提示：推荐使用支付宝或者浏览器扫码二维码下载</span>
				</div>
			</div>

		</div>
	</div>
</body>
</html>
