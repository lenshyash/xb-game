var initFlag = false;
$(function(){
	
	// 备份jquery的ajax方法
	var _ajax = $.ajax;

	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			},
			successIntercept:function(data){}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}
		if (opt.successIntercept) {
			fn.successIntercept = opt.successIntercept;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					$.toast("[" + opt.url + "] 404 not found",100000000);
				} else {
					fn.error(XMLHttpRequest, textStatus, errorThrown);
				}
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					$.toast("后台异常，请联系管理员!",1000000000);
					fn.successIntercept(data);
				} else if (ceipstate == 3) { // 业务异常
					$.toast(data.msg);
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 4 || ceipstate == 5) {// 未登陆异常 或 没有权限
					$.router.loadPage(Mobile.base+"/toLogin.do");  //加载ajax页面
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		return _ajax(_opt);
	};
	
	/**
	 * 首页
	 */
	$(document).on("pageInit","#page_index_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		M.baseHeading($page);
		M.baseLunbo();
		M.baseMessageCount();
		
		$page.find('a.shouye-sw-box').on("click",function(){
			var t = $(this),href = t.attr('href');
			if(href.indexOf('javascript')>=0){
				var code = href.substring(href.indexOf("('")+2,href.indexOf("')"));
				var getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_type_"+type+".html?v=1.1.3";
				if(code == 'PCEGG' || code == "JND28"){
					getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_code_"+code+".html?v=1.1.3";
				}
				$.get(getUrl, function(res) {
					$("#wanfaPopup h1.title").text(resultName(code));
					$("#wanfaPopup .content-block").html(res);
					$.popup("#wanfaPopup");
				}, "html");
			}
		});

		function resultName(code) {
			switch (code) {
			case "PCEGG":
				return "PC蛋蛋";
			case "JND28":
				return "加拿大28";
			}
		}
		$(".shouye-fangjian a.wanfashuoming img").click(function() {
			var t = $(this), code = t.attr("code"), name = t.attr("alt"),type = t.attr("type");
			var getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_type_"+type+".html?v=1.1.3";
			if(type == 161){
				getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_code_"+code+".html?v=1.1.3";
			}
			$.get(getUrl, function(res) {
				$("#wanfaPopup h1.title").text(name);
				$("#wanfaPopup .content-block").html(res);
				$.popup("#wanfaPopup");
			}, "html");
		})
		
		$("h4.weui-media-box__title img").click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			var t = $(this), code = t.attr("code"), name = t.attr("alt"),type = t.attr("type");
			var getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_type_"+type+".html?v=1.2";
			if(type == 161){
				getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_code_"+code+".html?v=1.2";
			}
			$.get(getUrl, function(res) {
				$("#wanfaPopup h1.title").text(name);
				$("#wanfaPopup .content-block").html(res);
				$.popup("#wanfaPopup");
			}, "html");
		})
		
		if (!$.cookie("isThisOneLogin")) {
			$(".popupAlert").removeClass("hide");
			$.cookie("isThisOneLogin", true, 7);
		}

		$('.popupAlert').click(function() {
			$('.popupAlert').addClass("hide");
		})
		
	})
	
	/**
	 * 钱包
	 */
	$(document).on("pageInit","#page_qianbao_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		$page.on("touchstart","#refreshQBMoney",function(){
			$.ajax({
				url:Mobile.base + '/meminfo.do',
				dataType:'json',
				type:'post',
				success:function(res){
					if(res.login){
						$(".walletAcer em.icoAcer").text(res.money);
					}else{
						window.location.href = Mobile.base + "/toLogin.do";
					}
				},
				beforeSend:function(){
					$(".walletAcer em.icoAcer").text("加载中...");
				},
				complete:function(){
					
				}
			})
		})
		
		M.baseHeading($page);
		M.baseMessageCount();
		if(QB.pwd == 1){
			M.qb.layerPwd();
			return;
		}
		if(QB.card == 1){
			M.qb.layerCard();
			return;
		}
	});
	
	/**
	 * 个人中心
	 */
	$(document).on("pageInit","#page_userInfo_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		M.baseHeading($page);
		M.baseMessageCount();
		$(".bqingIcon").click(function() {
			$.toast("敬请期待");
		})

		var nickUrl = $.cookie("account_"+account+"_touxiang");
		if (!nickUrl) {
			nickUrl = 10;
		}
		$("img.thumb").attr("src",Mobile.base+'/common/template/lottery/v4/m/images/touxiang/'+ nickUrl + '.jpg');
	});
	
	
	/**
	 * 动态
	 */
	$(document).on("pageInit","#page_gonggao_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		var pageSize = 100,pageNo = 1,status = 0;
		M.baseHeading($page);
		M.baseLunbo();
		M.baseMessageCount();
		//预加载
		searchMsg();
		gg();
		$("#news_tab li").click(function() {
			$(this).addClass("active").siblings().removeClass("active");
			var index = $(this).index();
			$(".newsContent ul").eq(index).show().siblings("ul").hide();
		})
		
		function gg() {
			var html = '';
			$.ajax({
				url : Mobile.base+'/getConfig/getArticle.do',
				dataType : 'json',
				type : 'post',
				data : {
					code : 13
				},
				success : function(res) {
					if (res.length == 0) {
						$(".newsContent ul").eq(0).addClass("zhanwei");
						return;
					}
					$(".newsContent ul").eq(0).removeClass("zhanwei");
					$.each(res, function(i, j) {
						html += '<li onclick="M.gonggao.ggClick(this);" data-id="' + j.id
								+ '"><span>'
								+ DateUtil.formatDate(j.updateTime)
								+ '</span><h3>' + j.title + '</h3></li>'
					})
					$(".newsContent ul").eq(0).html(html);
				}
			})
		}

		function searchMsg() {
			$.ajax({
				url : Mobile.base+'/center/news/message/list.do',
				dataType : 'json',
				type : 'post',
				data : {
					pageNumber : pageNo,
					pageSize : pageSize,
					status : status
				},
				success : function(res) {
					var html = '';
					if (res.totalCount == 0) {
						$(".zhanwei").show();
						$(".newsContent ul").eq(1).addClass("zhanwei");
						return;
					}
					$(".newsContent ul").eq(1).removeClass("zhanwei");
					$.each(res.list, function(i, j) {
						pageDatas[j.id] = j;
						var type = "";
						if (j.status == 2) {
							type = "yidu";
						}
						html += '<li class="' + type + '" data-id="' + j.id
								+ '" onclick="M.gonggao.readMsg(this);"><span>'
								+ DateUtil.formatDate(j.createDatetime)
								+ '</span><h3>' + j.title + '</h3></li>'
					})
					$(".newsContent ul").eq(1).html(html);
				}
			})
		}

	});
	
	/**
	 * 房间列表
	 */
	$(document).on("pageInit","#page_room_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		var status = getUrlParam('status');
		//return result
		if(status && status == 1){
			layerToast("房间口令不能为空");
		}else if(status == 2){
			layerToast("房间口令错误");
		}else if(status == 3){
			var message = getUrlParam('message');
			layerToast("该房间元宝限额"+message+"以上");
		}else if(status == 4){
			layerToast("您所拥有的会员等级不符合房间所规定的等级");
		}
		//click事件
		$(".content").on("click","a.back-btn",function(){
			var t = $(this),_box = t.parent(),maxM=_box.attr("max_m"),level=_box.attr("level"),status=_box.attr("status");
			if(parseFloat(maxM) > parseFloat(userMoney)){
				$.toast("该房间元宝限额"+maxM+"以上");
				return;
			}
			if(status == 1){
				layer.open({
					 title: [
					         '请输入房间暗号',
					         'background-color:#dc5d55; color:#fff;font-size:.9rem;'
						  ],
						  anim: 'up',
						  content: '<div class="moneyInput">房间暗号：<input type="text" id="inputNumber" style="width:50%;"></div>',
						  btn: ['确认', '取消'],
						  yes :function(index){
							  var argot = $("#inputNumber").val();
							  if(!argot){
								  $.toast("暗号不能为空");
								  return ;
							  }
							  var data = {
										argot:argot,
										roomId:_box.attr("r_id")
								}	
								$.ajax({
									url:Mobile.base+'/m/ajax/roomArgot.do',
									dataType:'json',
									type:'post',
									data:data,
									success:function(res){
										if(res.success){
											window.location.href = Mobile.base+"/m/toBetRoom.do?rId="+_box.attr("r_id")+"&lotType="+_box.attr("lot_type")+"&argot="+argot;
											layer.close(index);
										}else{
											$.toast(res.msg);
										}
									}
								})
						  }
				})
				return ;
			}
			window.location.href = Mobile.base+"/m/toBetRoom.do?rId="+_box.attr("r_id")+"&lotType="+_box.attr("lot_type");
		})
		
		$(".content a.seo-btn").click(function(){
			var t = $(this),pvsm=t.siblings(".peilvshuoming").html(),name=t.siblings(".fixed-seo1").find("span.name").text();
			console.info(name);
			$("#wanfaPopup h1.title").text(name);
			$("#wanfaPopup .card-content-inner").html(pvsm);
			$.popup("#wanfaPopup");
		})
		

		function layerToast(message){
			layer.open({
				title: [
				         '错误信息',
				         'background-color:#dc5d55; color:#fff;font-size:.9rem;'
					  ],
					  anim: 'up',
					  content : message,
					  btn : ['我知道了'],
			})
		}
	})
	
	
	/**
	 * 房间投注页
	 */
	$(document).on("pageInit","#page_betRoom_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		//获取当前可视化的高度
		$page.find("#content").css({'min-height':$(document).height()-140});
		_BetRoom.initBetRoom();
		if (!$.cookie("betRoomOneLogin")) {
			$(".popupAlert").removeClass("hide");
			$.cookie("betRoomOneLogin", true, 1);
		}

		$('.popupAlert').click(function() {
			$('.popupAlert').addClass("hide");
		})

		var _whiteDiv = $("#whiteDiv"),_panel = $(".panel.panel-right.panel-cover");
		$("#jiahao_click").on("click", function() {
			if(_panel.hasClass("panel_active")){
				_panel.removeClass("panel_active");
			}else{
				_panel.addClass("panel_active");
				_whiteDiv.removeClass("hide");
			}
		})
		//该问题会出现跳转页面返回后不加载。后续继续研究
		window.addEventListener('load', load, false)

		window.setInterval(function() {
			if (animateName) {
				$('.roomNameAnimate').css('transform',
						'translate3d(0,-2.2rem,0)');
				animateName = false;
			} else {
				$('.roomNameAnimate')
						.css('transform', 'translate3d(0,0,0)');
				animateName = true;
			}
		}, 2000);
	});
	
	
	
	/**
	 * 关于我们
	 */
	$(document).on("pageInit","#page_gywm_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		$page.find("#ptgwID").text("http://"+window.location.host);
		$page.find("#ptgwCopy").attr("data-clipboard-text","http://"+window.location.host);
		var clipboard = new Clipboard('#ptgwCopy');
		var clipboard1 = new Clipboard("#kfqqCopy");
		var clipboard2 = new Clipboard("#kfwxCopy");
		clipboard.on('success', function(e) { 
		    var msg = e.trigger.getAttribute('aria-label'); 
		    $.toast(msg);
		    e.clearSelection(); 
		});
		clipboard1.on('success', function(e) { 
		    var msg = e.trigger.getAttribute('aria-label'); 
		    $.toast(msg);
		    e.clearSelection(); 
		}); 
		clipboard2.on('success', function(e) { 
		    var msg = e.trigger.getAttribute('aria-label'); 
		    $.toast(msg);
		    e.clearSelection(); 
		}); 
		clipboard.on('error', function(e) {
		    $.toast('请选择“拷贝”进行复制!')
		});
		clipboard1.on('error', function(e) {
		    $.toast('请选择“拷贝”进行复制!')
		});
		clipboard2.on('error', function(e) {
		    $.toast('请选择“拷贝”进行复制!')
		});
	});
	
	/**
	 * 个人中心-设置
	 */
	$(document).on("pageInit","#page_setUp_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
	});
	
	/**
	 * 个人中心-修改登录密码
	 */
	$(document).on("pageInit","#page_upd_pwd_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		$("#btn").click(function() {
			var old_psd = $("input[name=old_psd]").val();
			var new_psd = $("input[name=new_psd]").val();
			var new_psd2 = $("input[name=new_psd2]").val();
			//var reg = /^\d{6}$/;
			if (!old_psd) {
				$.toast("请输入您的原始密码");
				return false;
			}
			if(!new_psd){
				$.toast("请输入新密码");
				return false;
			}
			if(!new_psd2){
				$.toast("请再次输入新密码");
				return false;
			}
			if (new_psd != new_psd2) {
				$.toast("您输入的两次密码不一致");
				return false;
			}
			$.ajax({
				url : Mobile.base+"/center/member/meminfo/newpwd.do",
				type : "post",
				data : {
					opwd : old_psd,
					pwd : new_psd,
					rpwd : new_psd2,
					updType : 1
				},
				dataType : "json",
				success : function(data) {
					if (data.success) {
						$.popup("#loginPopup");	
					} else {
						$.toast(data.msg);
					}
				},
				error : function(error) {
					$.toast("服务器错误了");
				}
			});
		});
	});
	
	
	/**
	 * 个人中心-信息中心
	 */
	$(document).on("pageInit","#page_info_page",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		var nickUrl = $.cookie("account_"+account+"_touxiang");
		if (!nickUrl) {
			nickUrl = 10;
		}
		$(".item-inner i img").attr("src",Mobile.base+'/common/template/lottery/v4/mobile/images/touxiang/'+ nickUrl + '.jpg');
		
	});
	
	$(document).on("pageInit","#betRecordDetail",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , code = null, status = 0;
		$page.on("click",".betOrderDetail",function(){
			betOrderDetail($(this));
		})
		if(!initFlag){
			changeData();
		}
		
		function changeData(){
			page = 1;//重新初始化页数
			$("#betRecordDetail .content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			code = $("#code").val();
			status = $("#status").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/lotteryBet/getBcLotteryOrder.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					code : code,
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.page.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$("#betRecordDetail .content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.page.totalPageCount;
							var html = '';
							var xBet = data.sumBuyMoney;
							var xWin = data.sumWinMoney; 
							var dWin = parseFloat(xWin) - parseFloat(xBet);
							html += '<div style="flex-grow: 1;"><em> 投注：</em><p style="color: red">'+toDecimal2(xBet)+'</p></div>';
							html += '<div style="flex-grow: 1;"><em> 中奖：</em><p style="color: #00aa00">'+toDecimal2(xWin)+'</p></div>';
							html += '<div style="flex-grow: 1;"><em> 盈利：</em><p style="color: red">'+toDecimal2(dWin)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.page.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" order_id="'+j.orderId+'" class="item-link item-content betOrderDetail">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>期号：'+j.qiHao+'</em></div>';
			                result += '<div class="item-after">投注金额：<em style="color:red">'+j.buyMoney+'</em>元宝</div></div>';
			                result += '<div class="item-subtitle"><em style="color:red;">['+j.lotName+']</em>&nbsp;投注内容：'+j.haoMa+'</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+j.createTime+'<em style="display:inline-block;float:right;">状态：'+statusHtml(j.status)+'</em></div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller(); light7不需要此参数
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusHtml(status){
			switch(status){
			case 1:
				status = '<font color="#337ab7">未开奖</font>';
				break;
			case 2:
				status = '<font color="#5cb85c">已中奖</font>';
				break;
			case 3:
				status = '<font color="red">未中奖</font>';
				break;
			case 4:
				status = '<font color="#777">已撤单</font>';
				break;
			case 5:
				status = '<font color="#5cb85c">派奖回滚</font>';
				break;
			case 6:
				status = '<font color="red">回滚异常</font>';
				break;
			case 7:
				status = '<font color="red">开奖异常</font>';
				break;
			case 8:
				status = '<font color="#5cb85c">和局</font>';
				break;
			case 10:
				status = '<font color="#5cb85c">和局中奖</font>';
				break;
		}
		return status;
		}
		
		function betOrderDetail(source){
			source = $(source);
			$.showIndicator();
			$.ajax({
				url:Mobile.base+"/lotteryBet/getBcLotteryOrderDetail.do",
				data:{orderId:source.attr("order_id")},
				dataType:'json',
				type:'post',
				success:function(res){
					$.hideIndicator();
					var html = detailHtml(res);
					$("#betOrderDetailPopup .betOrderDetail").html(html);
					$.popup("#betOrderDetailPopup");
				}
			})
		}
		
		function detailHtml(res){
			var winMoney = res.winMoney,openHm=res.lotteryHaoMa,hmHtml='';
			if(!winMoney){
				winMoney = '0.00';
			}
			switch(res.lotType){
			case 158:
				if(!openHm){
					openHm = '? , ? , ? , ? , ? , ? , ? , ? , ? , ?';
					return;
				}
				var split = openHm.split(',');
				for(var i=0;i<split.length;i++){
					hmHtml += '<i class="color_history color'+parseInt(split[i])+'">'+split[i]+'</i>';
				}
				break;
			case 160:
			case 161:
				if(!openHm){
					openHm = '? + ? + ? = ?';
				}else{
					var split = openHm.split(",");
					var sum = parseInt(split[0]) + parseInt(split[1]) + parseInt(split[2]);
					openHm = ''+split[0]+'+'+split[1]+'+'+split[2]+'='+sum+'';
					openHm += ''+(sum>13?'大':'小')+','+(sum%2==0?'双':'单')+'';
				}
				hmHtml = openHm;
				break;
			default:
				break;
			}
			var html = '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">注&nbsp;&nbsp;单&nbsp;&nbsp;号</div>';
			html += '<div class="item-input">'+res.orderId+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注期号</div>';
			html += '<div class="item-input">'+res.qiHao+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注时间</div>';
			html += '<div class="item-input">'+res.createTime+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注金额</div>';
			html += '<div class="item-input">'+res.buyMoney+'元宝</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注内容</div>';
			html += '<div class="item-input">'+res.haoMa+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">开奖号码</div>';
			html += '<div class="item-input">'+hmHtml+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">赔&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;率</div>';
			html += '<div class="item-input">'+res.minBonusOdds+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</div>';
			html += '<div class="item-input">'+statusHtml(res.status)+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">中奖金额</div>';
			html += '<div class="item-input">'+winMoney+'元宝</div>';
			html += '</div></div></li>';
			return html;
		}
		
	})
	
	/**
	 * 个人中心-投注记录
	 */
	$(document).on("pageInit","#page_betRecord_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , code = null, status = 0;
		$page.on("click","a.button-danger",function(){
			initFlag = true;
			changeData();
			$.router.loadPage("#betRecordDetail");
		});
		
		function changeData(){
			page = 1;//重新初始化页数
			$("#betRecordDetail .content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			code = $("#code").val();
			status = $("#status").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/lotteryBet/getBcLotteryOrder.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					code : code,
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.page.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$("#betRecordDetail .content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.page.totalPageCount;
							var html = '';
							var xBet = data.sumBuyMoney;
							var xWin = data.sumWinMoney; 
							var dWin = parseFloat(xWin) - parseFloat(xBet);
							html += '<div style="flex-grow: 1;"><em> 投注：</em><p style="color: red">'+toDecimal2(xBet)+'</p></div>';
							html += '<div style="flex-grow: 1;"><em> 中奖：</em><p style="color: #00aa00">'+toDecimal2(xWin)+'</p></div>';
							html += '<div style="flex-grow: 1;"><em> 盈利：</em><p style="color: red">'+toDecimal2(dWin)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.page.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" order_id="'+j.orderId+'" class="item-link item-content betOrderDetail">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>期号：'+j.qiHao+'</em></div>';
			                result += '<div class="item-after">投注金额：<em style="color:red">'+j.buyMoney+'</em>元宝</div></div>';
			                result += '<div class="item-subtitle"><em style="color:red;">['+j.lotName+']</em>&nbsp;投注内容：'+j.haoMa+'</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+j.createTime+'<em style="display:inline-block;float:right;">状态：'+statusHtml(j.status)+'</em></div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller(); light7不需要此参数
			},1000)
		})
		
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusHtml(status){
			switch(status){
			case 1:
				status = '<font color="#337ab7">未开奖</font>';
				break;
			case 2:
				status = '<font color="#5cb85c">已中奖</font>';
				break;
			case 3:
				status = '<font color="red">未中奖</font>';
				break;
			case 4:
				status = '<font color="#777">已撤单</font>';
				break;
			case 5:
				status = '<font color="#5cb85c">派奖回滚</font>';
				break;
			case 6:
				status = '<font color="red">回滚异常</font>';
				break;
			case 7:
				status = '<font color="red">开奖异常</font>';
				break;
			case 8:
				status = '<font color="#5cb85c">和局</font>';
				break;
			case 10:
				status = '<font color="#5cb85c">和局中奖</font>';
				break;
		}
		return status;
		}
		
		function betOrderDetail(source){
			source = $(source);
			$.showIndicator();
			$.ajax({
				url:Mobile.base+"/lotteryBet/getBcLotteryOrderDetail.do",
				data:{orderId:source.attr("order_id")},
				dataType:'json',
				type:'post',
				success:function(res){
					$.hideIndicator();
					var html = detailHtml(res);
					$("#betOrderDetailPopup .betOrderDetail").html(html);
					$.popup("#betOrderDetailPopup");
				}
			})
		}
		
		function detailHtml(res){
			var winMoney = res.winMoney,openHm=res.lotteryHaoMa;
			if(!winMoney){
				winMoney = '0.00';
			}
			if(!openHm){
				openHm = '? + ? + ? = ?';
			}else{
				var split = openHm.split(",");
				var sum = parseInt(split[0]) + parseInt(split[1]) + parseInt(split[2]);
				openHm = ''+split[0]+'+'+split[1]+'+'+split[2]+'='+sum+'';
				openHm += ''+(sum>13?'大':'小')+','+(sum%2==0?'双':'单')+'';
			}
			var html = '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">注&nbsp;&nbsp;单&nbsp;&nbsp;号</div>';
			html += '<div class="item-input">'+res.orderId+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注期号</div>';
			html += '<div class="item-input">'+res.qiHao+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注时间</div>';
			html += '<div class="item-input">'+res.createTime+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注金额</div>';
			html += '<div class="item-input">'+res.buyMoney+'元宝</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">投注内容</div>';
			html += '<div class="item-input">'+res.haoMa+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">开奖号码</div>';
			html += '<div class="item-input">'+openHm+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">赔&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;率</div>';
			html += '<div class="item-input">'+res.minBonusOdds+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</div>';
			html += '<div class="item-input">'+statusHtml(res.status)+'</div>';
			html += '</div></div></li>';
			html += '<li><div class="item-content"><div class="item-inner">';
			html += '<div class="item-title label">中奖金额</div>';
			html += '<div class="item-input">'+winMoney+'元宝</div>';
			html += '</div></div></li>';
			return html;
		}
	})
	
	
	/**
	 * 钱包--支付安全
	 */
	$(document).on("pageInit","#page_zhifuanquan_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.find("#wjmm").click(function(){
			layer.open({
				anim: 'up',
				content : '您好，找回密码请联系客服',
				btn : [ '联系客服', '取消' ],
				yes : function(index) {
					window.location.href = "${kfUrl}";
					layer.close(index);
				}
			});
		})
	});
	
	/**
	 * 钱包--修改取款密码
	 */
	$(document).on("pageInit","#page_upd_pwd_1_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.find("#btn").click(function() {
			var old_psd = $("input[name=old_psd]").val();
			var new_psd = $("input[name=new_psd]").val();
			var new_psd2 = $("input[name=new_psd2]").val();
			if (!old_psd) {
				$.toast("请输入您的原始密码");
				return false;
			}
			if(!new_psd){
				$.toast("请输入新密码");
				return false;
			}
			if(!new_psd2){
				$.toast("请再次输入新密码");
				return false;
			}
			if (new_psd != new_psd2) {
				$.toast("您输入的两次密码不一致");
				return false;
			}
			$.ajax({
				url : Mobile.base+"/center/member/meminfo/newpwd.do",
				type : "post",
				dataType:'json',
				data : {
					opwd : old_psd,
					pwd : new_psd,
					rpwd : new_psd2,
					updType : 2
				},
				dataType : "json",
				success : function(data) {
					if (data.success) {
						$.popup("#drawUpdPwd");	
					} else {
						$.toast(data.msg);
					}
				},
				error : function(error) {
					$.toast("服务器错误了");
				}
			});
		});
	})
	
	/**
	 * 支付安全--首次设置密码
	 */
	$(document).on("pageInit","#page_setUp_pwd_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$(".inputPas").on("focus",function(){
            var index =$(this).val().length;
            if(index ==6){
                $(this).siblings("ul").children("li").eq(index-1).addClass("guangbiao1");
            }else{
                $(this).siblings("ul").children("li").eq(index).addClass("guangbiao");
            }
    	})
    	 $(".inputPas").on("blur",function(){
	        $(this).siblings("ul").children("li").removeClass("guangbiao");
	        $(this).siblings("ul").children("li").removeClass("guangbiao1");
	    })
	
	    $(".inputPas").on("input change",function(){
	        $(this).val($(this).val().replace(/[^\d]/g,''));
	        var index =$(this).val().length;
	        var val =$(this).val();
	        if(index > 6){
	            val=val.substring(0,6);
	            $(this).val(val);
	            return false;
	        }
	        var oLi =$(this).siblings("ul").children("li");
	        if(index){
	            oLi.eq(index).prevAll().text("*");
	            oLi.eq(index-1).nextAll().text("");
	            oLi.removeClass("guangbiao").eq(index).addClass("guangbiao");
	            if(index ==5){
	                oLi.removeClass("guangbiao1");
	            }
	            if(index ==6){
	                oLi.eq(index-1).text("*");
	                oLi.removeClass("guangbiao").eq(index-1).addClass("guangbiao1");
	            }
	
	        }
	        if(!index){
	            oLi.eq(0).text("");
	            oLi.removeClass("guangbiao").eq(0).addClass("guangbiao");
	        }
	
	    })
    	
        $("#setBtn").click(function() {
            //合法性验证
            var password = $("input[name=password]").val();
            var password2 = $("input[name=password2]").val();

            if (password.length < 6) {
                $.toast("请输入6位数的密码");
                return false;
            }
            if (password2.length == '') {
                $.toast("请再次输入密码");
                return false;
            }
            
            if (password2.length < 6) {
                $.toast("请输入6位数的密码");
                return false;
            }
            
            if (password !== password2) {
                $.toast("两次密码不一致");
                return false;                   
            }
            
            //验证通过提交数据
            $.ajax({
                url: Mobile.base+"/center/member/meminfo/repwd.do",
                type: "post",
                data: {pwd: password, rpwd: password2},
                dataType: "json",
                success: function(data) {
                    if (data.success) {
                    	$.popup("#drawPopup");
                    } else {
                    	$.toast(data.msg);
                    }
                },
                error: function(error) {
                    $.toast("服务器出错了");
                }
            });
        }); 
	})
	
	/**
	 * 钱包--绑定新银行卡
	 */
	$(document).on("pageInit","#page_bank_card_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
	})
	
	/**
	 * 钱包--绑定新银行卡第一步
	 */
	$(document).on("pageInit","#page_bank_card_detail",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.on('click',"#submitBtn",function(){
			if (!vaildateTrue()) {
				return false;
			}
			$("#bankCardForm").submit();
		})
		
		function vaildateTrue() {
			var cardName = $("input[name=cardName]"), cardNo = $("input[name=cardNo]"), nameReg = /^[\u4e00-\u9fa5]+$/, cardReg = /^([1-9][\d]{1,18}|x|X)$/;
			if (!cardName.val()) {
				$.toast("请输入持卡人");
				return false;
			}
			if (!nameReg.test(cardName.val())) {
				$.toast("持卡人格式不正确");
				return false;
			}
			if (!cardNo.val()) {
				$.toast("请输入卡号");
				return false;
			}
			if (!cardReg.test(cardNo.val())) {
				$.toast("卡号格式不正确");
				return false;
			}
			return true;
		}
	})
	
	/**
	 * 钱包--绑定新银行卡第二步
	 */
	$(document).on("pageInit","#page_bank_card_detail_1_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var checkflag = false;
		$page.on("click","#upData",function(){
			if (checkSubmit()) {
				if (!checkflag) {
					checkflag = true;
					submit();
				} else {
					$.toast("请勿重复提交");
				}
			}
		})

		function submit() {
			var cashBankname = $("#bankName").val();
			var sheng = $("#province").val();
			var city = $("#city").val();
			var bankcardaddress = $("#bankcardaddress").val();
			var cashBankaccount = $("#bankId").val();
			var cashPassword = $("#cashPassword").val();
			var userName = $("#accountname").val();
			var phone = $("#phone").val();

			var param = {};
			param["bankName"] = cashBankname;
			param["userName"] = userName;
			param["province"] = sheng;
			param["city"] = city;
			param["bankAddress"] = bankcardaddress;
			param["cardNo"] = cashBankaccount;
			param["repPwd"] = cashPassword;
			param["phone"] = phone;
			$.ajax({
						url : Mobile.base+"/center/banktrans/draw/commitbkInfo.do",
						data : param,
						type : 'POST',
						dataType:'json',
						success : function(result) {
							if (!result.success) {
								$.toast(result.msg);
							} else {
								$(".information p").eq(0).text(cashBankname);
								$(".information p").eq(1).text("************" + cashBankaccount.substring(cashBankaccount.length-4,cashBankaccount.length));
								$.popup("#infoDataPopup");
							}
						},
						complete : function() {
							checkflag = false;
						}
					});
		}

		function checkSubmit() {
			var account = $("#accountname").val();
			var cashBankname = $("#bankName").val();
			var cashBankaccount = $("#bankId").val();
			var cashPassword = $("#cashPassword").val();
			var phone = $("#phone").val();
			var exp = /^([1-9][\d]{1,18}|x|X)$/;
			if (!account) {
				$.toast("真实姓名不能为空");
				return false;
			}
			if (!cashBankname) {
				$.toast("开户银行不能为空");
				return false;
			}
			if (!cashBankaccount) {
				$.toast("银行帐号不能为空");
				return false;
			}
			if (!cashBankaccount || !exp.test(cashBankaccount)) {
				$.toast("银行卡号格式错误");
				return false;
			}
			if (!cashPassword) {
				$.toast("取款密码不能为空");
				return false;
			}
			if(!phone || !/^1\d{10}$/.test(phone)){
				$.toast("手机号不能为空或格式错误");
				return false;
			}
			return true;
		}
	})
	
	$(document).on("pageInit","#billsWebCZ",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0 ,type = 0;
		if(!initFlag){
			changeData();
		}
		function changeData(){
			page = 1;//重新初始化页数
			$page.find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#status").val();
			type = $("#type").val();
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/hisrecord/comrd.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					type : type,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$page.find(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
							var html = '';
							var czMoney = data.aggsData.totalMoney;
							html += '<div style="flex-grow: 1;"><em> 总充值：</em><p style="color: #00aa00">'+toDecimal2(czMoney)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>取款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</em></div>';
			                result += '<div class="item-after">状态：'+statusResult(j.lockFlag,j.status)+'</div></div>';
			                result += '<div class="item-subtitle">取款金额：<em style="color:red">'+toDecimal2(j.money)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return '<em style="color:#1a2f3c">处理中</em>';
				case 2:
					return '<em style="color:#f6383a">处理成功</em>';
				case 3:
					return '<em style="color:#4cd964">处理失败</em>';
				default:
					return '<em style="color:#f6383a">未知错误</em>';
			}
		}
	})

	/**
	 * 钱包--充值记录
	 */
	$(document).on("pageInit","#page_billWebCZ_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0 ,type = 0;
		
		$page.on("click","a.button-danger",function(){
			initFlag = true;
			changeData();
			$.router.loadPage("#billsWebCZ");  //加载内联页面
		})
		
		function changeData(){
			page = 1;//重新初始化页数
			$page.siblings("#billsWebCZ").find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#status").val();
			type = $("#type").val();
			ajaxData();
		}
		
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/hisrecord/comrd.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					type : type,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$page.siblings("#billsWebCZ").find("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
							var html = '';
							var czMoney = data.aggsData.totalMoney;
							html += '<div style="flex-grow: 1;"><em> 总充值：</em><p style="color: #00aa00">'+toDecimal2(czMoney)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>存款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</em></div>';
			                result += '<div class="item-after">状态：'+statusResult(j.lockFlag,j.status)+'</div></div>';
			                result += '<div class="item-subtitle">存款金额：<em style="color:red">'+toDecimal2(j.money)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return '<em style="color:#1a2f3c">处理中</em>';
				case 2:
					return '<em style="color:#f6383a">处理成功</em>';
				case 3:
					return '<em style="color:#4cd964">处理失败</em>';
				default:
					return '<em style="color:#f6383a">未知错误</em>';
			}
		}
	})
	
	$(document).on("pageInit","#billsWebQK",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0;
		if(!initFlag){
			changeData();
		}
		function changeData(){
			page = 1;//重新初始化页数
			$page.find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#drawStatus").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/hisrecord/drawrd.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$page.find(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
							var html = '';
							var drawMoney = data.aggsData.drawMoney;
							html += '<div style="flex-grow: 1;"><em> 总取款：</em><p style="color: #00aa00">'+toDecimal2(drawMoney)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>取款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</em></div>';
			                result += '<div class="item-after">状态：'+statusResult(j.lockFlag,j.status)+'</div></div>';
			                result += '<div class="item-subtitle">取款金额：<em style="color:red">'+toDecimal2(j.drawMoney)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return '<em style="color:#1a2f3c">处理中</em>';
				case 2:
					return '<em style="color:#f6383a">处理成功</em>';
				case 3:
					return '<em style="color:#4cd964">处理失败</em>';
				default:
					return '<em style="color:#f6383a">未知错误</em>';
			}
		}
	})
	
	/**
	 * 优惠活动
	 */
	$(document).on("pageInit","#page_active_jsp",function(e,pageId,$page){
		console.info("当前加载id=" + pageId);
		
		
	})
	
	/**
	 * 钱包--取款记录
	 */
	$(document).on("pageInit","#page_billWebQK_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0;
		
		$page.on("click","a.button-danger",function(){
			initFlag = true;
			changeData();
			$.router.loadPage("#billsWebQK");  //加载内联页面
		})
		
		function changeData(){
			page = 1;//重新初始化页数
			$page.siblings("#billsWebQK").find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#drawStatus").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/hisrecord/drawrd.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$page.siblings("#billsWebQK").find(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
							var html = '';
							var drawMoney = data.aggsData.drawMoney;
							html += '<div style="flex-grow: 1;"><em> 总取款：</em><p style="color: #00aa00">'+toDecimal2(drawMoney)+'</p></div>';
							$("#countData .card-content-inner").html(html);
							$("#countData").removeClass('hide');
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>取款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</em></div>';
			                result += '<div class="item-after">状态：'+statusResult(j.lockFlag,j.status)+'</div></div>';
			                result += '<div class="item-subtitle">取款金额：<em style="color:red">'+toDecimal2(j.drawMoney)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return '<em style="color:#1a2f3c">处理中</em>';
				case 2:
					return '<em style="color:#f6383a">处理成功</em>';
				case 3:
					return '<em style="color:#4cd964">处理失败</em>';
				default:
					return '<em style="color:#f6383a">未知错误</em>';
			}
		}
	});
	
	$(document).on("pageInit","#billsWebZB",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0;
		if(!initFlag){
			changeData();
		}
		function changeData(){
			page = 1;//重新初始化页数
			$page.find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#moneyType").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/mnyrecord/list.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$page.find(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2!=0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>类型：'+statusResult(2,j.type)+'</em></div></div>';
			                result += '<div class="item-subtitle">变动前金额：<em style="color:#0894ec">'+toDecimal2(j.beforeMoney)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动金额：<em style="color:red">'+toDecimal2(j.money)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动后金额：<em style="color:#4cd964">'+toDecimal2(j.afterMoney)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">变动时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		

		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return "人工加款";
				case 2:
					return '人工扣款';
				case 3:
					return '在线取款失败';
				case 4:
					return "在线取款";
				case 5:
					return "在线支付";
				case 6:
					return "快速入款";
				case 7:
					return "一般入款";
				case 9:
					return "反水加钱";
				case 10:
					return "反水失败";
				case 18:
					return "活动中奖";
				case 19:
					return "现金兑换积分";
				case 20:
					return "积分兑换现金";
				case 79:
					return "注册赠送";
				case 80:
					return "存款赠送";
				case 130:
					return "彩票投注";
				case 131:
					return "彩票派奖";
				case 132:
					return "彩票撤单";
				case 133:
					return "彩票派奖失败";
			}
		}
	})
	
	/**
	 * 钱包--帐变记录
	 */
	$(document).on("pageInit","#page_billsWebZB_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , end_time , status = 0;
		//加载moneyType
		//initMoneyType();
		
		$page.on("click",".button-danger",function(){
			initFlag = true;
			changeData();
			$.router.loadPage("#billsWebZB");  //加载内联页面
		})
		
		function changeData(){
			page = 1;//重新初始化页数
			$page.siblings("#billsWebZB").find(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#moneyType").val();
			
			ajaxData();
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/center/record/mnyrecord/list.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$page.siblings("#billsWebZB").find(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2!=0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>类型：'+statusResult(2,j.type)+'</em></div></div>';
			                result += '<div class="item-subtitle">变动前金额：<em style="color:#0894ec">'+toDecimal2(j.beforeMoney)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动金额：<em style="color:red">'+toDecimal2(j.money)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动后金额：<em style="color:#4cd964">'+toDecimal2(j.afterMoney)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">变动时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return "人工加款";
				case 2:
					return '人工扣款';
				case 3:
					return '在线取款失败';
				case 4:
					return "在线取款";
				case 5:
					return "在线支付";
				case 6:
					return "快速入款";
				case 7:
					return "一般入款";
				case 9:
					return "反水加钱";
				case 10:
					return "反水失败";
				case 18:
					return "活动中奖";
				case 19:
					return "现金兑换积分";
				case 20:
					return "积分兑换现金";
				case 79:
					return "注册赠送";
				case 80:
					return "存款赠送";
				case 130:
					return "彩票投注";
				case 131:
					return "彩票派奖";
				case 132:
					return "彩票撤单";
				case 133:
					return "彩票派奖失败";
			}
		}
		
		function initMoneyType() {
			var data = [];
			for ( var key in moneyType) {
				if (10 < key && key < 15) {
					continue;
				}
				if ("off" == '${isZrOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isDzOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isCpOnOff}' && 129 < key && key < 134) {
					continue;
				}
				if ("off" == '${isLhcOnOff}' && 139 < key && key < 143) {
					continue;
				}
				if ("off" == '${isTyOnOff}' && 200 < key && key < 205) {
					continue;
				}
				if ("off" == '${isExChgOnOff}' && 18 < key && key < 21) {
					continue;
				}

				data.push({
					"id" : key,
					"name" : moneyType[key]
				})
			}
			var eachdata = {
				"data" : data
			};
			var html = template('moneytype_tpl', eachdata);
			$("#moneyType").append(html);
		}
	});
	
	/**
	 * 钱包--充值
	 */
	$(document).on("pageInit","#page_toRegChange_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.find(".content .czList").addClass("hide").css("display","none").siblings(".card-desc").addClass("hide").css("display","none");
		$page.find(".content-block").addClass("hide");
		//选中radio
		$page.find(".content .card .radioClass li").click(function(){
			var t = $(this),btnch,_input = t.find("input");_input.prop('checked',true);
			var pay_type = _input.attr("pay_type"),desc='',quick='',_title = t.find('span').text();
			$("#"+pay_type).removeClass("hide").css("display","block").siblings(".list-block").addClass("hide").css("display","none");
			switch(pay_type){
				case "online":
					desc=$("#onlineDesc").html();
					btnch = 'PayChange.online.onlineSub(this);';
					break;
				case "fast":
					$("#flabel").text(_input.attr("flabel")).siblings(".item-input").find("input").attr({"placeholder":_input.attr("flabel")});
					desc = $("#fastDesc").html();
					quick = 'onoff_show_pay_quick';
					btnch = 'PayChange.fast.fastSub(this);';
					break;
				case "bank":
					desc = $("#bankDesc").html();
					quick = 'onoff_show_pay_normal';
					btnch = 'PayChange.bank.bankSub(this);';
					break;
			}
			$page.find(".content-block").removeClass("hide").find("a").attr("onclick",btnch);
			$page.find(".card-desc").removeClass("hide").find(".card-content-inner").text(desc);
			if(quick){
				PayChange.fastAndBank.generatePayOrder(quick,_input.val(),pay_type,_title);
				$page.find(".content-block a").text('确认提交');
			}else{
				$page.find(".content-block a").text('下一步');
			}
			$page.find('.content').animate({'scrollTop': $('.content')[0].scrollHeight}, 300);
		})
		
		$page.find('.popupAlert').click(function(){
			$page.find('.popupAlert').addClass("hide");
		})
	});
	
	/**
	 * 钱包--提现记录
	 */
	$(document).on("pageInit","#page_toDrawCommit_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.find("#drawcommitBtn").on("click",function(){
			$("#drawcommit").attr("disabled", "disabled");
			var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
			var m = $("#money").val();
			if (!exp.test(m)) {
				$.toast("请输入正确的金额");
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			m = parseInt(m);
			if (m < min) {
				$.toast("取款最小金额不能小于"+min);
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			if (m > balance) {
				$.toast("余额不足");
				$("#drawcommit").removeAttr("disabled");
				return;
			}

			if (max != 0 && max < m) {
				$.toast("取款最大金额不能大于"+max);
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			var userQxpassword = $("#cashPwd").val();
			if (userQxpassword == null || userQxpassword == "") {
				$.toast("取款密码不能为空");
				$("#drawcommit").removeAttr("disabled");
				return;
			}

			$.ajax({
					url : Mobile.base+"/center/banktrans/draw/drawcommit.do",
					dataType:'json',
					type:'post',
					data : {
						money : m,
						repPwd : userQxpassword
					},
					success : function(result) {
						if (!result.success) {
							$.toast(result.msg);
						} else {
							layer.open({
							    content: '取款信息已提交！',
							    anim:'up',
							    btn: ['<a href="'+Mobile.base+'/m/toQb/billsWebQK.do">我知道了</a>']
							  });
						}
					},
					complete : function() {
						$("#drawcommit").removeAttr("disabled");
					}

			});
		})
	});
	
	/**
	 * 玩法介绍
	 */
	$(document).on("pageInit","#page_game_rule_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		$page.find(".card ul li").click(function(){
			var t = $(this),code=t.attr("code"),type=t.attr("type"),name=t.find(".item-title").text();
			var getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_type_"+type+".html?v=1.1.2";
			if(type == 161){
				getUrl = Mobile.base+"/member/"+Mobile.folder+"/rule/rule_code_"+code+".html?v=1.1.2";
			}
			$.get(getUrl,function(res){
				$("#wanfaPopup h1.title").text(name);
				$("#wanfaPopup .content-block").html(res);
				$.popup("#wanfaPopup");
			},"html");
		})
	});
	
	/**
	 * 开奖结果
	 */
	$(document).on("pageInit","#page_open_award_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , code = null;
		$page.on("change",".changeData",function(){
			changeData();
		})
		function changeData(){
			page = 1;//重新初始化页数
			$(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			code = $("#code").val();
			ajaxData();
		}
		
		if(!($page.find(".infinite-scroll-preloader").text() == "加载完毕")){
			setTimeout(function(){
				getData();
			},1000)
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/m/ajax/openAward.do",
				dataType:'json',
				type:'post',
				data : {
					time : start_time,
					lotCode:code,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result = "",html='';
						$.each(data.list.list,function(i,j){
							if(!(j.haoMa)){
								
							}else{
								var data = {
									index : i,
									qiHao:j.qiHao,
									time: DateUtil.formatDatetime(j.endTime),
									hmArr: j.haoMa.split(","),
									type : getResultType(j.lotCode),
								}
								html += template('historyLastTpl',data);
							}
							
//							if(i%2==0){
//								result += '<li style="background:#f9f9f9">';
//							}else{
//								result += '<li>';
//							}
//							result += '<div class="item-inner" style="padding-left: .75rem;">';
//			                result += '<div class="item-title-row"><div class="item-title">第'+j.qiHao+'期</div>';
//			                result += '<div class="item-after">'+DateUtil.formatDatetime(j.endTime)+'</div></div>';
//			                if(!(j.haoMa)){
//			                	result += '<div class="item-subtitle item-openAward" style="line-height: 2rem;"><em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>=<em class="shuzi_00">-</em>';
//					            result += '<em class="colorDan" style="background:#ccc">-</em><em class="colorDan" style="background:#ccc">-</em><em class="colorDan" style="background:#ccc">-</em>';
//								result += '</div></li>';
//			                }else{
//			                	var hm = j.haoMa.split(","),sum = totalSum(hm);
//				                result += '<div class="item-subtitle item-openAward" style="line-height: 2rem;"><em class="shuzi_'+firstAddZero(hm[0])+'">'+firstAddZero(hm[0])+'</em>+<em class="shuzi_'+firstAddZero(hm[1])+'">'+firstAddZero(hm[1])+'</em>+<em class="shuzi_'+firstAddZero(hm[2])+'">'+firstAddZero(hm[2])+'</em>=<em class="shuzi_'+firstAddZero(sum)+'">'+firstAddZero(sum)+'</em>';
//				                result += '<em class="colorDan" style="background:'+(sum>=14?'#2371ff':'#f30700')+'">'+(sum>=14?'大':'小')+'</em>';
//								result += '<em class="colorDan" style="background:'+(sum%2==0?'#f30700':'#2371ff')+'">'+(sum%2==0?'双':'单')+'</em>';
//				                var anQ='',anB=1;
//								if(sum>=15&&sum%2!=0){
//									if(sum>=23){
//										anQ='极大';
//									}else{
//										anQ='大单';
//										anB=2;
//									}
//								}else if(sum>=14&&sum%2==0){
//									anQ='大双';
//								}else if(sum<14&&sum%2==0){
//									if(sum<=4){
//										anQ='极小';
//									}else{
//										anQ='小双';
//									}
//								}else{
//									anQ='小单';
//									anB=2;
//								}
//								result += '<em class="colorDan" style="background:'+(anB==1?'#f30700':'#2371ff')+'">'+anQ+'</em>';
//								result += '</div></li>';
//			                }
						})
						$('.infinite-scroll .list-container').append(html);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function firstAddZero(num){
			num = parseInt(num);
			if(num<10){
				return '0'+num;
			}
			return num
		}
		
		function totalSum(hm){
			return  parseInt(hm[0]) + parseInt(hm[1]) + parseInt(hm[2]);
		}
	});
	
	/**
	 * 开奖走势
	 */
	$(document).on("pageInit","#page_game_trend_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		var start_time , code = null;
		//复制返回上一页
		$page.on("change",".changeData",function(){
			changeData();
		})
		function changeData(){
			page = 1;//重新初始化页数
			$(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll #record_list tr").remove();
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			code = $("#code").val();
			ajaxData();
		}
		
		if(!($page.find(".infinite-scroll-preloader").text() == "加载完毕")){
			setTimeout(function(){
				getData();
			},1000)
		}
		
		function ajaxData(){
			$.ajax({
				url:Mobile.base+"/m/ajax/openAward.do",
				dataType:'json',
				type:'post',
				data : {
					time : start_time,
					lotCode:code,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$(".content").addClass("zhanwei");
						},2000)
						return;
					}
					var datas = {type:getResultType(code),}
					var html = template('gameTrendThTpl',datas);
					$("#trend_title").html(html);
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result='';
						$.each(data.list.list,function(i,j){
							if(!(j.haoMa)){
								if(getResultType(code) == 161){
									result += '<tr><td>'+j.qiHao+'</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>';
								}else{
									result += '<tr><td>'+j.qiHao+'</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>';
								}
							}else{
								var hms = j.haoMa.split(",");
								if(getResultType(code) == 158){
									var hz = parseInt(hms[0]) + parseInt(hms[1]);
								}else{
									var hz = parseInt(hms[0]) + parseInt(hms[1]) + parseInt(hms[2]);
								}
								var d = {
										index:i,
										qiHao:j.qiHao,
										gyhz:hz,
										type:getResultType(code),
								}
								result += template('gameTrendTpl',d);
							}
						})
						$('.infinite-scroll #record_list').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				//$.refreshScroller();
			},1000)
		})
		function firstAddZero(num){
			num = parseInt(num);
			if(num<10){
				return '0'+num;
			}
			return num
		}
		
		function totalSum(hm){
			return  parseInt(hm[0]) + parseInt(hm[1]) + parseInt(hm[2]);
		}
	});
	
	
	/**
	 * 登录页面
	 */
	$(document).on("pageInit","#page_login_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		M.getCookieData();
		var a = $(".denglu-body").height();
		$(".denglu-body>img").attr("height", a + 20);

		$(".denglu-fix2 img").click(function() {
			var alt = $(this).attr("alt");
			if (alt) {
				layer.open({
					anim: 'up',
					title:['温馨提示','background-color:#dc5d55; color:#fff;font-size:.8rem'],
					content: alt,
				    btn: ['复制','取消'],
				})
			}
		})
		$("#remeberJZ").click(function() {
			var check = $(this).find("input");
			if (check.is(":checked")) {
				check.prop("checked", false);
			} else {
				check.prop("checked", true);
			}
		})
		$("#verifyDiv input").val(login_key);
		$(".back").click(function() {
			window.location.href = Mobile.base+"/m/index.do";
		})
	});
	
	/**
	 * 注册页面
	 */
	$(document).on("pageInit","#page_register_jsp",function(e,pageId,$page){
		console.info("当前加载id="+pageId);
		M.verifyImg();
		var a = $(".denglu-body").height();
		$(".denglu-body>img").attr("height",a+20);
		
		$(".back").click(function(){
			window.location.href = Mobile.base+"/m/index.do";
		})
		
		initRegConf();
		
	});
	
	$.init();
})
var M = {
	baseHeading:function(_$page){	//头部加好点击事件公用
		var _menu = _$page.find(".menu"),_whiteDiv = _$page.find("#whiteDiv");
		$("#jiahao_click").on("click",function(){
			if(_menu.hasClass("hide")){
				_menu.removeClass("hide");
				_whiteDiv.removeClass("hide");
			}else{
				_menu.addClass("hide");
			}
		})
		
		$(".menu li a").click(function(){
			_whiteDiv.addClass("hide");
			_menu.addClass("hide");
		})
		
		$("#whiteDiv").click(function(){
			_whiteDiv.addClass("hide");
			_menu.addClass("hide");
		})
		// 该问题会出现跳转页面返回后不加载。后续继续研究
		window.addEventListener('load',load,false)
	},
	baseLunbo:function(){	//轮播公用
		$(".swiper-container").swiper({
			pagination : '.swiper-pagination',
			autoplay : 3000,
			autoplayDisableOnInteraction : false,
			loop : true
		});
	},
	baseMessageCount:function(){
		if(!Mobile.isLogin){return;}
		$.ajax({
			url:Mobile.base+'/center/news/message/messageCount.do',
			type:'post',
			success:function(res){
				if(res.messageCount > 0){
					$("#messageCount").removeClass("hide").text(res.messageCount);
				}else{
					$("#messageCount").addClass("hide");
				}
			}
		})
	},
	cookieData:function(key){
		if(!$.cookie(key)){
			$.cookie(key,M.randomNum(111),{expires:7});
		}
	},
	setCookieData:function(account,pswd){
		$.cookie("account",account,{expires:7,path:'/'});
		$.cookie("password",pswd,{expires:7,path:'/'});
	},
	getCookieData:function(){
		var account = $.cookie("account");
		var password = $.cookie("password");
		$(".denglu-input input[name=userName]").val(account);
		$(".denglu-input input[name=passWord]").val(password);
	},
	verifyImg:function(){
		var url = Mobile.base + "/verifycode.do?timestamp=" + (new Date().getTime());
		$("#authnumImg").attr("src", url);
	},
	randomNum:function(minNum,maxNum,cy){ 
	    switch(arguments.length){ 
	        case 1: 
	            return parseInt(Math.random()*minNum+1); 
	        break; 
	        case 2: 
	            return parseInt(Math.random()*(maxNum-minNum+1)+minNum); 
	        break; 
	        case 3:
	        	return parseFloat(Math.random()*(maxNum-minNum+1)+minNum)/cy;
	        default: 
	            return 0; 
	        break; 
	    } 
	}
}


M.gonggao = function(){
	return {
		ggClick:function(source) {
			source = $(source);
			$.ajax({
				type:'post',
				dataType:'json',
				url : Mobile.base+"/m/gg_detail.do",
				data : {
					id : source.attr("data-id")
				},
				success : function(res) {
					if (res.success) {
						$("#wanfaPopup h1.title").text('公告详情');
						$("#wanfaPopup .content-block").html(M.gonggao.ggReadHtml(res.message));
						$.popup("#wanfaPopup");
					} else {
						$.toast(res.msg);
					}
				}
			})
		},
		ggReadHtml:function(res) {
			var html = ' <div class="card"><div class="card-header" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:center;">'
					+ res.title + '</span></div>';
			html += ' <div class="card-content"><div class="card-content-inner">'
					+ res.content
					+ '</div></div><div class="card-footer" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:right;">发布时间：'
					+ DateUtil.formatDate(res.updateTime)
					+ '</span></div></div>';
			return html;
		},
		readMsg:function(source) {
			source = $(source);
			var row = pageDatas[source.attr("data-id")];
			$.ajax({
				url : Mobile.base+"/center/news/message/read.do",
				dataType : 'json',
				type : 'post',
				data : {
					id : source.attr("data-id")
				},
				success : function(res) {
					if (res.success) {
						if (!source.hasClass("yidu")) {
							source.addClass("yidu");
						}
						$("#wanfaPopup h1.title").text('公告详情');
						$("#wanfaPopup .content-block").html(M.gonggao.readHtml(row));
						$.popup("#wanfaPopup");
					} else {
						$.toast(res.msg);
					}
				}
			})
		},
		readHtml:function(res) {
			var html = ' <div class="card"><div class="card-header" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:center;">'
					+ res.title + '</span></div>';
			html += ' <div class="card-content"><div class="card-content-inner">'
					+ res.message + '</div></div></div>';
			return html;
		}
	}
}();



M.qb = function(){
	return {
		layerPwd:function(){
			layer.open({
				anim: 'up',
				title:['温馨提示','background-color:#dc5d55; color:#fff;font-size:.8rem'],
				content: '您尚未设置取款密码',
			    btn: ['<a href="'+Mobile.base+'/m/toQb/setup.do">去设置</a>','取消'],
			})
		},
		layerCard:function(){
			layer.open({
				anim: 'up',
				title:['温馨提示','background-color:#dc5d55;color:#FFF;font-size:.8rem'],
				content : '您尚未绑定任何银行卡',
				btn : ['<a href="'+Mobile.base+'/m/toQb/toBankCard.do?type=2">去绑定</a>','取消'],
			})
		}
	}
}();



M.login = function(){
	return {
		validateLoginInput:function(){
			var uN = $(".denglu-fix input[name=userName]"),pW = $(".denglu-fix input[name=passWord]"),vC = $(".denglu-fix input[name=verifyCode]"),data={};
			if(!uN.val()){
				$.toast('用户名不能为空');
				return;
			}
			if(!pW.val()){
				$.toast('密码不能为空');
				return;
			}
			if(!uN.val() && uN.val().length <4){
				$.toast('用户名不能小于4位');
				return;
			}
			if(!pW.val() && pW.val().length<6){
				$.toast('密码不能小于6位');
				return;
			}
			if(!vC.val() && $("#verifyDiv").is(":visible")){
				$.toast("验证码不能为空");
				return;
			}
			data['account'] = uN.val();
			data['password'] = pW.val();
			data['verifyCode'] = vC.val();
			$.showIndicator();
			M.login.userLoginSub(data);
			return true;
		},
		userLoginSub:function(data){
			$.ajax({
				url:Mobile.base + "/m/ajax/login.do",
				data:data,
				dataType:'json',
				type:'post',
				success:function(res){
					$.hideIndicator();
					if(res.success){
						//记录cookie
						var rember = $("#remeberJZ input").prop("checked");
						if(rember){
							M.setCookieData(data['account'],data['password']);
						}else{
							M.setCookieData('','');
						}
						//登录之后随机分配头像缓存cookie
						M.cookieData('account_'+data['account']+'_touxiang');
						window.location.href = Mobile.base + '/m/index.do';
					}else{
						$.toast(res.msg);
						M.verifyImg();
						$("#verifyDiv").show().find("input").val('');
					}
				}
			})
		}
	}
}();


M.register = function(){
	return {
		validateRegisterInput:function(url){
			var account = $(".denglu-fix input[name=account]"),pwd = $(".denglu-fix input[name=password]"),cPwd = $(".denglu-fix input[name=rPassword]"),verifyCode=$(".denglu-fix input[name=verifyCode]")
			if(!account.val()){
				$.toast('用户名不能为空');
				return;
			}
			if(!pwd.val()){
				$.toast('密码不能为空');
				return;
			}
			if(!cPwd.val()){
				$.toast('确认密码不能为空');
				return;
			}
			if(pwd.val() != cPwd.val()){
				$.toast('两次密码输入不一致');
				return;
			}
			if(!verifyCode.val()){
				$.toast("验证码不能为空");
				return;
			}
			var status = $("#registerStatus").val();
			if(!status){
				var data = getCommitData();
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				$.showIndicator();
				M.register.userRegisterSub(data,url);
			}else if(status == 'testGuest'){	//试玩
				var data = {};
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				$.showIndicator();
				M.register.userRegisterSub(data,url);
			}else{	//代理
				var data = getCommitData();
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				$.showIndicator();
				M.register.userRegisterSub(data,url);
			}
		},
		userRegisterSub:function(data,url){
			var flag = !url;
			url = url || (Mobile.base + '/registerTestGuest.do');
			$.ajax({
				url:url,
				data : {data:JSON.encode(data)},
				dataType:'json',
				type:'post',
				success:function(res){
					$.hideIndicator();
					if(res.success){
						//游客的话就不保存多天。
						if(flag){
							$.cookie('account_'+data['account']+'_touxiang',M.randomNum(111));
						}else{
							M.cookieData('account_'+data['account']+'_touxiang');
						}
						window.location.href = Mobile.base + '/m/index.do';
					}else{
						$.toast(res.msg);
						M.verifyImg();
					}
				}
			})
		}
	}
}();



//touch
function load(){
	var white = document.getElementById("whiteDiv"),_panel = $(".panel.panel-right.panel-cover");
	if(white){
		white.addEventListener('touchend',touch,false)
	}
	
	function touch(){
		$("#whiteDiv").addClass("hide");
		$(".menu").addClass("hide");
		_panel.removeClass("panel_active");
	}
}

function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

var fileds = [];
var validateFiled = [];

function initRegConf() {
	for (var i = 0; i < $_regconf.length; i++) {
		var conf = $_regconf[i];
		if (conf.source && (conf.type == 2 || conf.type == 3 || conf.type == 4)) {
			conf.sourceLst = conf.source.split(",");
		}
	}
	var eachdata = {
		"data" : $_regconf
	};
	var html = template('regconflst_tpl', eachdata);
	$("#verifycode_div").before(html);
	$("#reg_tb").append(html);
	$("#reg_tb_after").after(html);
}

template.helper('$addValidateFiled', function(conf) {
	if (conf.requiredVal == 2 || conf.validateVal == 2) {
		validateFiled.push(conf);
	}
});

function reset() {
	$('form')[0].reset();
}

function validate() {
	for (var i = 0; i < validateFiled.length; i++) {
		var filed = validateFiled[i];
		var val = getVal(filed);
		if(filed.regex){
			filed.regex = filed.regex.replace(/\\+/g,'\\');
			var regex = new RegExp(filed.regex);
		}

		if (filed.requiredVal == 2) {
			if (!val) {
				$.toast(filed.name + "必须输入");
				return false;
			}
		}

		if (filed.validateVal == 2) {
			if (!regex.test(val)) {
				$.toast(filed.name + "格式错误");
				return false;
			}
		}
	}
	return true;
}

function getVal(filed) {
	var val = "";
	if (filed.type == 1 || filed.type == 2 || filed.type == 5
			|| filed.type == 6) {
		val = $("#" + filed.key).val();
	} else if (filed.type == 3) {
		val = $("input[name='" + filed.key + "']:checked").val();
	} else if (filed.type == 4) {
		var vals = [];
		$("input[name='" + filed.key + "']:checked").each(function() {
			vals.push(this.value);
		})
		val = vals.join(",");
	}
	return val;
}

function getCommitData() {
	var data = {};
	for (var i = 0; i < $_regconf.length; i++) {
		var filed = $_regconf[i];
		var val = getVal(filed);
		data[filed.key] = val;
	}
	return data;
}

function getResultType(code){
	switch(code){
		case "BJSC":
		case "SFSC":
		case "XYFT":
		case "LXYFT":
			return 158;
		case "JND28":
		case "PCEGG":
			return 161;
		case "AHK3":
		case "FFK3":
		case "GSK3":
		case "GXK3":
		case "HBK3":
		case "HEBK3":
		case "BJK3":
		case "JSSB3":
		case "JXK3":
		case "SHHK3":
		case "WFK3":
		case "JPK3":
		case "KRK3":
		case "HKK3":
		case "AMK3":
			return 160;
		default:
			return 0;
	}
}