<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_billsWebZB_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb.do"> <span class="icon icon-left"></span>
				返回
			</a>
			<h1 class="title">帐变记录</h1>
		</header>
		<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
		<div class="content">
			<div class="list-block" style="margin:.5rem;">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">开始</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${startTime}" max="${endTime }"
										name="start_time" style="font-size: .7rem; color: red;"
										class="changeData">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">截止</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${endTime}" max="${endTime }"
										name="end_time" style="font-size: .7rem; color: red;"
										class="changeData">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block">
				<div class="row">
					<div class="col-100">
						<a href="javascript:void(0);" class="button button-big button-fill button-danger">确定</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page" id='billsWebZB'>
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb/billsWebZB.do"> <span
				class="icon icon-left"></span> 返回
			</a>
			<h1 class='title'>帐变记录</h1>
		</header>
		<div class="content infinite-scroll infinite-scroll-bottom"
			data-distance="50">
			<div class="card hide" id="countData"
				style="margin: .5rem; background: #ececec;">
				<div class="card-content">
					<div class="card-content-inner"
						style="display: flex; text-align: center;"></div>
				</div>
			</div>
			<div class="list-block media-list inset" style="margin-top:.5rem;">
				<ul class="list-container">
				</ul>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>