<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_active_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${base}/m/"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">优惠活动</h1>
		</header>
		<div class="content">
	    <!-- 这里是页面内容区 -->
	    <div class="list-block">
		     <c:forEach var="v" items="${data}">
			     <a external href="${base}/m/activeDesc.do?id=${v.id}" >
			     	 <img src="${v.img }" style="width:100%;max-height:7rem;padding:.25rem 0;">
			     </a>
		     </c:forEach>
	     </div>
	  </div>
	</div>
</body>
</html>

<jsp:include page="include/need_js.jsp" />