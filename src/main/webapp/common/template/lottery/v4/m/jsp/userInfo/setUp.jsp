<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_setUp_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toUser.do"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">设置</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="card marginBottom">
				<div class="card-content">
					<div class="list-block">
						<ul>
							<li><a href="${base}/m/toUser/toLoginPwd.do"
								class="item-link item-content">
									<div class="item-media">
										<em class="icon1 icoModifyPas"></em>
									</div>
									<div class="item-inner">
										<div class="item-title">修改登录密码</div>
									</div>
							</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="card marginBottom">
				<div class="card-content">
					<div class="list-block">
						<ul>
							<li><a href="#" class="item-link item-content"
								onclick="logout();">
									<div class="item-media">
										<em class="icon icoSignOut"></em>
									</div>
									<div class="item-inner">
										<div class="item-title">退出</div>
									</div>
							</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			function logout() {
				layer
						.open({
							content : '您确定要退出吗？',
							btn : [ '确定', '不要' ],
							anim : 'up',
							yes : function(index) {
								$
										.ajax({
											url : '${base}/m/logout.do',
											success : function(res) {
												if (res.success) {
													layer.close(index);
													window.location.href = "${base}/toLogin.do";
												}
											}
										})
							}
						});
			}
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>