<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header class="bar bar-nav">
	<a class="button button-link button-nav pull-right" href="${base}/m/appDownload.do" style="margin-right:0;"><span style="display: inline-block;vertical-align: top;"></span><span
		class="icon icon-app_down"></span> </a>
	<h1 class="title">${empty navClass?_title:navClass eq 'DATING'?_title:navClass eq 'QIANBAO'?'钱包':navClass eq 'WODE'?'个人中心':navClass eq 'DONGTAI'?'动态':_title}</h1>
</header>