<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_index_jsp">
		<jsp:include page="include/base_header_2.jsp"></jsp:include>
		<jsp:include page="include/footer.jsp"></jsp:include>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<input type="hidden" value="${loginMember.money}" id="userNowMoney" />
			<c:if test="${not empty htmlGG}">
				<jsp:include page="common/index_gg.jsp"></jsp:include>
			</c:if>
			<c:if test="${not empty lunbo && lunbo.size() >0}">
				<jsp:include page="common/lunbo.jsp"></jsp:include>
			</c:if>
			<jsp:include page="common/navTab.jsp"></jsp:include>
			<jsp:include page="common/lottery_nav.jsp"></jsp:include>
			
			<div class="content-block" style="margin: 0; padding: 0;">
				<div class="tabs">
					<div id="tab1" class="tab active">
						<div class="weui-grids">
						<c:forEach items="${bcList}" var="bc" varStatus="bIndex">
							<c:if test="${bc.type == 161}">
							<a href="${base}/m/room.do?code=${bc.code}" class="weui-grid">
								<div class="weui-media-box__hd">
			                        <img class="weui-media-box__thumb" src="${base}/common/template/lottery/v4/m/images/gclogo/${bc.code}.png" alt="">
			                    </div>
			                    <div class="weui-media-box__bd">
			                        <h4 class="weui-media-box__title">${bc.name}</h4>
			                        <p class="weui-media-box__desc">官方彩种，老牌游戏</p>
			                        <h4 class="weui-media-box__title"><img
												alt="${bc.name}" code="${bc.code}" type="${bc.type}"
												src="${base}/common/template/lottery/v4/m/images/index/img_28_anniu.png"></h4>
			                    </div>
							</a>
							</c:if>
						</c:forEach>
						</div>
					</div>
					<div id="tab2" class="tab">
						<div class="weui-grids">
						<c:forEach items="${bcList}" var="bc" varStatus="bIndex">
							<c:if test="${bc.type == 158}">
							<a href="${base}/m/room.do?code=${bc.code}" class="weui-grid">
								<div class="weui-media-box__hd">
			                        <img class="weui-media-box__thumb" src="${base}/common/template/lottery/v4/m/images/gclogo/${bc.code}.png" alt="">
			                    </div>
			                    <div class="weui-media-box__bd">
			                        <h4 class="weui-media-box__title">${bc.name}</h4>
			                        <p class="weui-media-box__desc">重金打造，多元素快三</p>
			                        <h4 class="weui-media-box__title"><img
												alt="${bc.name}" code="${bc.code}" type="${bc.type}"
												src="${base}/common/template/lottery/v4/m/images/index/img_28_anniu.png"></h4>
			                    </div>
							</a>
							</c:if>
						</c:forEach>
						</div>
					</div>
					<div id="tab3" class="tab">
						<div class="weui-grids">
							<c:forEach items="${bcList}" var="bc" varStatus="bIndex">
							<c:if test="${bc.type == 160}">
							<a href="${base}/m/room.do?code=${bc.code}" class="weui-grid">
								<div class="weui-media-box__hd">
			                        <img class="weui-media-box__thumb" src="${base}/common/template/lottery/v4/m/images/gclogo/YBK3.png" alt="">
			                    </div>
			                    <div class="weui-media-box__bd">
			                        <h4 class="weui-media-box__title">${bc.name}</h4>
			                        <p class="weui-media-box__desc">高赔率，独家推广</p>
									<h4 class="weui-media-box__title"><img
												alt="${bc.name}" code="${bc.code}" type="${bc.type}"
												src="${base}/common/template/lottery/v4/m/images/index/img_28_anniu.png"></h4>
			                    </div>
							</a>
							</c:if>
						</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="popupAlert hide">
		<div class="popup_header">
			<img src="${base}/common/template/lottery/v4/m/images/ico_kefu.png">
		</div>
		<div class="popup_top_index">
			<img
				src="${base}/common/template/lottery/v4/m/images/tishi/index_ts_1.png">
		</div>
		<div class="popup_footer">
			<img
				src="${base}/common/template/lottery/v4/m/images/tishi/index_ts_2.png">
		</div>
	</div>
	
		<!-- 红包开始 -->
		<c:if test="${onOffRedBag eq 'on'}">
		<style type="text/css">
			.red-bag-float{position:absolute;height:110px;width:90px;z-index: 999999;right: 0px;top: 50%;}
		</style>
		<div class="red-bag-float">
			 <a href="${base }/m/redPackage.do" external>
			 	<img src="${base }/common/images/redpacket/rt-ad-m.png" width="100%" height="100%"/>
			 </a>
			 <div style="width: 1.5rem;height: 1.5rem;position: absolute;bottom: 0px;margin-left: 39%;z-index: 9999999;" onclick="hideRedBag();"></div>
		</div>
		<script type="text/javascript">
			function hideRedBag(){
				$('.red-bag-float').hide();
			}
		</script>
		</c:if>
		<!-- 红包结束 -->
	
	<jsp:include page="common/noticeAlert.jsp" />
	<jsp:include page="popup/wanfa_popup.jsp"></jsp:include>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>