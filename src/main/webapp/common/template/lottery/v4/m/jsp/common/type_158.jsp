<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="card hide" id="touzhu_jianpan">
	<div class="card-header">
		<span class="leftAr"></span>
		<span class="swpier_title">双面盘</span>
		<span class="rightAr"></span>
	</div>
	<div class="swiper-container swiper-container1"><%--swiper-container swiper-container1 swiper-wrapper --%>
		<div class="swiper-wrapper">
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1 play6">
						<div class="pk10_nav">
							<i data-num="1">冠军</i><i  data-num="2">亚军</i><i  data-num="3">季军</i><i  data-num="4">第四名</i><i  data-num="5">第五名</i>
							<i  data-num="6">第六名</i><i  data-num="7">第七名</i><i  data-num="8">第八名</i><i  data-num="9">第九名</i><i  data-num="10">第十名</i>
						</div>
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul style="margin-top:15px;">
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.playCode eq 'shuangmianpan' && six.isNowYear ne 1}">
											<li><div data-id="${six.id}" data-code="${six.playCode}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span>${six.name}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play5">
							<div class="pk10_nav">
								<i data-num="1">冠军</i><i  data-num="2">亚军</i><i  data-num="3">季军</i><i  data-num="4">第四名</i><i  data-num="5">第五名</i>
								<i  data-num="6">第六名</i><i  data-num="7">第七名</i><i  data-num="8">第八名</i><i  data-num="9">第九名</i><i  data-num="10">第十名</i>
							</div>
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.playCode eq 'danhao1-10'}">
											<c:forEach begin="1" end="10" var="num">
											<li><div data-id="${six.id}" class="color${num}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span class="colorSpan">${num}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
											</c:forEach>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play4">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.playCode eq 'guan-yajun'}">
											<li><div data-code="${six.playCode}" data-id="${six.id}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}" data-desc="${six.name}">
													<span>${six.name}</span>
													<p class="kafei">
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 如果需要导航按钮 -->
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>

	<div class="swiper-pagination swiper-p1"></div>
</div>