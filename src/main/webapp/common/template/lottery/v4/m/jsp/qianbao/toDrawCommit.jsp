<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_toDrawCommit_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">提现</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="card">
				<div class="card-header" style="background: #efeff4;">
					<span
						style="width: 100%; text-align: center; display: inline-block;">提示信息</span>
				</div>
				<div class="card-content">
					<div class="card-content-inner">
						1. 每天的取款处理时间为：<font color="red">${commit.get('star')}</font> 至 <font
							color="red">${commit.get('end')}</font>;<br> 2.
						取款3分钟内到账。(如遇高峰期，可能需要延迟到十分钟内到帐);<br> 3. 用户每日最小提现 <font
							color="red">${commit.get('min')}</font> 元，最大提现 <font color="red">${commit.get('max')}</font>
						元;<br> 4. 今日可取款 ${commit.get('wnum')} 次，已取款
						${commit.get('curWnum')} 次 ;
					</div>
				</div>
				<div class="card-header" style="background: #efeff4;">
					<span
						style="width: 100%; text-align: center; display: inline-block;">消费比例</span>
				</div>
			</div>
			<div class="card">
				<div class="card-content">
					<div class="card-content-inner">
						1. 出款需达投注量：<font color="red">${commit.get('checkBetNum')}</font>
						,当前有效投注金额：<font color="red">${commit.get('member').betNum}</font>
						<br> 2. 是否能取款：<font color="red">${commit.get('drawFlag')}</font>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" style="background: #efeff4;">
					<span
						style="width: 100%; text-align: center; display: inline-block;">提款信息</span>
				</div>
			</div>
			<div class="list-block" style="font-size:.7rem;">
				<ul>
					<c:set value="${fn:length(commit.get('member').cardNo) }"
						var="cardNO"></c:set>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoBank"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">银&nbsp;&nbsp;行&nbsp;&nbsp;卡</div>
								<div class="item-input">
									<span>${commit.get('member').bankName}（尾数${fn:substring(commit.get('member').cardNo,cardNO-4,cardNO)}）</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoNickname"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">真实姓名</div>
								<div class="item-input">
									<span>${commit.get('member').userName}</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoWallet"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">账户余额</div>
								<div class="item-input">
									<fmt:formatNumber type="number"
										value="${commit.get('member').money}" maxFractionDigits="2" />
									元宝</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoMoney"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">取款金额</div>
								<div class="item-input">
									<input type="number" id="money"
										placeholder="最小提现${commit.get('min')}元宝">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon1 icoModifyPas"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">取款密码</div>
								<div class="item-input">
									<input type="password" id="cashPwd" maxlength="6"
										placeholder="请输入取款密码">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block" style="margin: 1.2rem 0;">
				<div class="row">
					<div class="col-100">
						<a href="#" id="drawcommitBtn"
							class="button button-big button-fill button-danger">申请提款</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			<c:choose>
				<c:when test="${commit.get('min') == null}">
					var min = 0;
				</c:when>
				<c:otherwise>
					var min = ${commit.get('min')};
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${commit.get('max') == null}">
					var max = 0;
				</c:when>
				<c:otherwise>
					var max = ${commit.get('max')};
				</c:otherwise>
			</c:choose>
			var balance = ${commit.get('member').money};
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>