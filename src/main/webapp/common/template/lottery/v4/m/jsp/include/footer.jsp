<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav class="bar bar-tab">
	<c:choose>
		<c:when test="${isLogin}">
			<a class="tab-item no-transition ${not empty navClass && navClass eq 'DATING'?'active':''}" href="${base}/m/index.do"> <span
				class="icon icon-home"></span> <span class="tab-label">大厅</span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'QIANBAO'?'active':''}" href="${base}/m/toQb.do"> <span class="icon icon-card"></span>
				<span class="tab-label">钱包</span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'DONGTAI'?'active':''}" href="${base}/m/gonggao.do"> <span
				class="icon icon-star"></span> <span class="tab-label">动态</span><span class="badge hide" id="messageCount"></span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'WODE'?'active':''}" href="${base}/m/toUser.do"> <span
				class="icon icon-me"></span> <span class="tab-label">我的</span>
			</a>
		</c:when>
		<c:otherwise>
			<a class="tab-item no-transition ${not empty navClass && navClass eq 'DATING'?'active':''}" href="${base}/m/index.do"> <span
				class="icon icon-home"></span> <span class="tab-label">大厅</span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'QIANBAO'?'active':''}" href="${base}/toLogin.do"> <span class="icon icon-card"></span>
				<span class="tab-label">钱包</span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'DONGTAI'?'active':''}" href="${base}/toLogin.do"> <span
				class="icon icon-star"></span> <span class="tab-label">动态</span>
			</a> <a class="tab-item no-transition ${not empty navClass && navClass eq 'WODE'?'active':''}" href="${base}/toLogin.do"> <span
				class="icon icon-me"></span> <span class="tab-label">我的</span>
			</a>
		</c:otherwise>
	</c:choose>
</nav>