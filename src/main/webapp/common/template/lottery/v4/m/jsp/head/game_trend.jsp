<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_game_trend_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${backUrl}"> <span class="icon icon-left"></span>
				返回
			</a>
			<h1 class="title">开奖走势</h1>
		</header>
		<div class="list-block"
			style="margin: 0; position: absolute; width: 100%; top: 2.2rem; z-index: 100;">
			<ul>
				<li style='background: #efeff4;'>
					<div class="item-content">
						<div class="item-inner">
							<div class="item-title label">时间</div>
							<div class="item-input" style="background: #FFF;">
								<input type="date" value="${nowTime}" max="${nowTime}"
									style="font-size: .7rem; color: red;" name="start_time" class="changeData">
							</div>
						</div>
					</div>
				</li>
				<li style='background: #efeff4;'>
					<div class="item-content">
						<div class="item-inner">
							<div class="item-title label">彩种</div>
							<div class="item-input" style="background: #FFF;">
								<select name="code" id="code" class="changeData">
									<c:forEach items="${bcList}" var="bc">
										<option value="${bc.code}">${bc.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<%-- 这里是页面内容区 --%>
		<div class="content infinite-scroll infinite-scroll-bottom"
			data-distance="50" style="top: 7.5rem; background: #FFF;">
			<div class="list-block media-list" style="padding-top: .5rem;">
				<table>
					<thead>
						<tr id="trend_title">
						</tr>
					</thead>
					<tbody id="record_list">
					</tbody>
				</table>
			</div>
			<div class="infinite-scroll-preloader">
				<div class="preloader"></div>
			</div>
		</div>
		<jsp:include page="../common/art_template_room.jsp"></jsp:include>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>