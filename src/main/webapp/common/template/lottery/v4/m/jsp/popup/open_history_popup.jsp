<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="openHistoryPopup">
	<header class="bar bar-nav">
		<h1 class="title">最近开奖结果</h1>
	</header>
	<div class="content" style="padding:0;margin:0;background: #fff;">
		<div class="content-inner">
			<div class="content-block">
				<div class="card-content card-open-hm">
					<div class="card-content-inner" style="padding: .75rem;">
						<div class="list-block media-list inset" style="padding: .5rem 0;margin:-.75rem;">
							<ul class="list-container">
							</ul>
						</div>
					</div>
					<a href="${base}/m/openAward.do" external class="button button-round button-danger close-popup">更多开奖结果</a>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);"
								class="button button-big button-fill button-danger close-popup">返回投注</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>