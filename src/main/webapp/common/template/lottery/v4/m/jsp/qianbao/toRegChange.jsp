<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_toRegChange_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb.do"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">充值</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<link rel="stylesheet"
			href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
		<div class="content">
			<div class="card">
				<div class="card-content">
					<div class="card-content-inner chongzhi_top">
						帐号：<em>${loginMember.account}</em>&nbsp;&nbsp;&nbsp;&nbsp; 余额：<em>${loginMember.money}</em>
					</div>
				</div>
			</div>
			
		    <div class="buttons-tab">
			    <a href="#onlineSaveCash" class="tab-link active button">线上存款</a>
			    <a href="#unlineSaveCash" class="tab-link button">线下存款</a>
		   </div>
		<div class="tabs">
			<div id="onlineSaveCash" class="tab active">
			<c:if test="${dptMap.get('onlineFlag') eq 'on'}">
				<div class="list-block czList hide" id="online">
					<ul>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon icoMoney"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">充值金额</div>
									<div class="item-input">
										<input type="number" id="money" placeholder="请输入金额"
											style="background: #FFF; border-radius: 2rem; text-indent: 1rem; height: 1.8rem;">
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="card">
					<div class="card-header card_bot"
						style="background: rgb(245, 245, 245);">
						<em class="icon icoTop"></em><span>线上充值</span>
					</div>
					<div class="card-content">
						<div class="card-content-inner">
							<div class="list-block media-list">
								<ul class="radioClass" id="showOnlinePay">
									<c:choose>
										<c:when test="${onlineMap.size() == 0}">
											<li
												style="line-height: 150px; text-align: center; font-size: 30px;">暂时没有配置支付方式</li>
										</c:when>
										<c:otherwise>
											<c:forEach items="${onlineMap}" var="line" varStatus="lines">
												<li><span
													<c:choose>
														<c:when test="${not empty line.icon}">style="background: url('${line.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px"</c:when>
														<c:when test="${line.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;;text-indent:-9999px" </c:when>
														<c:otherwise>
															class="icons ${line.iconCss}"
														</c:otherwise>
													</c:choose>>${line.payName}</span>
													<input id="radio${line.id}" name="radio" type="radio"
													value="${line.id}" min="${line.min}" max="${line.max}"
													iconcss="${line.iconCss}" pays_type="${line.payType}"
													pay_type="online"> <label for="radio${line.id}"><font
														style="font-size: .8rem;">最小充值金额<strong
															style="color: red;">${line.min}</strong>元
													</font></label>
													<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${line.payDesc}">${line.payDesc}</marquee>
												</li>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			</div>
			<div id="unlineSaveCash" class="tab">
			<c:if
				test="${dptMap.get('fastFlag') eq 'on' || dptMap.get('bankFlag') eq 'on'}">
				<div class="card">
					<div class="card-header card_bot"
						style="background: rgb(245, 245, 245);">
						<em class="icon icoBot"></em><span>线下充值</span>
					</div>
					<div class="card-content">
						<div class="card-content-inner">
							<div class="list-block media-list">
								<ul class="radioClass xxClass">
									<c:choose>
										<c:when test="${fastMap.size() == 0 && bankMap.size() == 0}">
											<li
												style="line-height: 150px; text-align: center; font-size: 30px;">暂时没有配置支付方式</li>
										</c:when>
										<c:otherwise>
											<c:if test="${fastMap.size() > 0}">
												<c:forEach items="${fastMap}" var="fast" varStatus="fasts">
													<li><span
														<c:choose>
												<c:when test="${not empty fast.icon}">style="background: url('${fast.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${fast.iconCss}"
												</c:otherwise>
											</c:choose>>${fast.payName}</span>
														<input id="radio${fast.id}" name="radio" type="radio"
														value="${fast.id}" min="${fast.min}" max="${fast.max}"
														flabel="${fast.frontLabel}" pay_type="fast"> <label
														for="radio${fast.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
																style="color: red;">${fast.min}</strong>元
														</font></label>
														<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${fast.qrcodeDesc}">${fast.qrcodeDesc}</marquee>
													</li>
												</c:forEach>
											</c:if>
											<c:if test="${bankMap.size() > 0}">
												<c:forEach items="${bankMap}" var="bank" varStatus="banks">
													<li><span title="${bank.payName}"
														<c:choose>
												<c:when test="${not empty bank.icon}">style="background: url('${bank.icon}') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 .5rem; background-size: 5rem 1.5rem;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${bank.iconCss}"
												</c:otherwise>
											</c:choose>>${bank.payName}</span>
														<input id="radio${bank.id}" name="radio" type="radio"
														value="${bank.id}" min="${bank.min}" max="${bank.max}"
														pay_type="bank"> <label for="radio${bank.id}"><font
															style="font-size: .8rem;">最小充值金额<strong
																style="color: red;">${bank.min}</strong>元
														</font></label>
														<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${bank.bankDesc}">${bank.bankDesc}</marquee>
													</li>
												</c:forEach>
											</c:if>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			</div>
		 </div>

			<div class="list-block czList hide" id="bank">
			</div>
			<div class="list-block czList hide" id="fast">
			</div>
			<div class="card card-desc hide">
				<div class="card-content">
					<div class="card-content-inner"></div>
				</div>
			</div>
			<div class="content-block">
				<div class="row">
					<div class="col-100">
						<a href="javascript:void(0);" id="btn"
							class="button button-big button-fill button-danger">下一步</a>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/online_cz_popup.jsp"></jsp:include>
		<jsp:include page="../common/template_pay.jsp"></jsp:include>
		<jsp:include page="${memberFolder}/${folder}/toRegister_yl.jsp"></jsp:include>
		<script type="text/javascript" src="${base}/common/js/onlinepay/pay.js?v=6.2673"></script>
		<script type="text/javascript" src="${base}/common/template/lottery/v4/m/js/pay_change.js?v=2.1"></script>
		<div id="onlineDesc" style="display:none;">${empty dptMap.get("onlineDesc")?"及时自动到账，推荐":dptMap.get("onlineDesc")}</div>
		<div id="fastDesc" style="display:none;">${empty dptMap.get("fastDesc")?"支持微信/支付宝二维码扫描":dptMap.get("fastDesc")}</div>
		<div id="bankDesc" style="display:none;">${empty dptMap.get("bankDesc")?"支持网银转账，ATM转账，银行柜台汇款":dptMap.get("bankDesc")}</div>
		<script type="text/javascript">
			var hostUrl1="${hostUrl1}";
			
			function onlineSubmit(){
				$("#onlinePayForm").submit();
			}
			
			$(function(){
				$(".buttons-tab .tab-link").click(function(){
					if('#onlineSaveCash' == $(this).attr('href')){
						$('#bank,#fast').hide();
						$('#online').show();
					}else{
						$('#online').hide();
						$('#bank,#fast').show();
					}
				});
			})

		</script>
	</div>
	<style>
		.buttons-tab .button{
			font-size: 1.1rem;
		}
		.buttons-tab .button.active{
		    color: red;
    		border-color: red;
		}
	</style>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>