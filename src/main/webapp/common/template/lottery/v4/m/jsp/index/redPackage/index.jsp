<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>抢红包</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">		
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">
		<!--在iPhone 手机上禁止了把数字转化为拨号链接-->
	    <meta content="telephone=no" name="format-detection" />
	    <!--删除默认的苹果工具栏和菜单栏-->
	    <meta content="yes" name="apple-mobile-web-app-capable">
	    <!-- 网站开启对web app程序的支持，在web app应用下状态条（屏幕顶部条）的颜色，默认值为default（白色），可以定为black（黑色）和black-translucent（灰色半透明）-->
   		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<link rel="stylesheet" href="${base }/mobile/anew/redPackage/css/demo.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${base }/mobile/anew/redPackage/css/sweetalert.css">
		<script src="${base}/common/modelCommon/down/js/jquery-1.7.2.min.js" type="text/javascript"></script>
		<script>
			var base = '${base}';
			var nowTime = '${nowTime}';
		</script>
		<script src="${base }/mobile/anew/redPackage/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/common/template/lottery/v4/m/jsp/index/redPackage/js/main.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="top-menu">
			<a href="${base }/m/"><img src="${base }/mobile/anew/redPackage/images/redbag_index.gif"/>返回首页</a>
			<div class="red-record"><marquee></marquee></div>
			<span id="redbag_time" style="float: right;"></span>
		</div>
	
		<ul class="couten"></ul>
		<div class="mo">
			<div class="sen hongbaoyu">
				<img src="${base }/mobile/anew/redPackage/images/gx.png">
				<h3 style="color: #fed261;">获得红包<font color="red;"></font>元</h3>
				<a href="javascript:void(0);">确定</a>
			</div>
		</div>
		<div class="backward">
			<span></span>
		</div>		
		<div class="backward-title">
			<span></span>
		</div>		
	</body>
</html>