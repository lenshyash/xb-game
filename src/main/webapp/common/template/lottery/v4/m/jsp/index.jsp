<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_index_jsp">
		<jsp:include page="include/base_header.jsp"></jsp:include>
		<jsp:include page="include/footer.jsp"></jsp:include>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<input type="hidden" value="${loginMember.money}" id="userNowMoney" />
			<c:if test="${not empty htmlGG}">
				<jsp:include page="common/index_gg.jsp"></jsp:include>
			</c:if>
			<c:if test="${not empty lunbo && lunbo.size() >0}">
				<jsp:include page="common/lunbo.jsp"></jsp:include>
			</c:if>
			<div class="index_shuom">
				<div class="index_shuom_left">
					<ul>
						<li><i></i><span>用户已赚&nbsp;&nbsp;&nbsp;${yonghuyizhuan}元宝</span></li>
						<li><i></i><span>注册人数&nbsp;&nbsp;&nbsp;${zhucerenshu}人</span></li>
					</ul>
				</div>
				<div class="index_shuom_right">
					<span>98</span>%<font>赚钱率</font>
				</div>
			</div>
			<div class="shouye-fangjian clear roomList">
				<c:forEach items="${bcList}" var="bc" varStatus="bIndex">
						<c:if test="${isLogin}">
							<div>
								<a class="open-indicator"
									href="${base}/m/room.do?code=${bc.code}"><img
									src="${base}/common/template/lottery/v4/m/images/index/${bc.code}.png">
								</a> <a class="wanfashuoming" href="javascript:void(0);"><img
									alt="${bc.name}" code="${bc.code}" type="${bc.type}"
									src="${base}/common/template/lottery/v4/m/images/index/img_28_anniu.png"></a>
							</div>
						</c:if>
						<c:if test="${!isLogin}">
							<div>
								<a href="${base}/toLogin.do"><img
									src="${base}/common/template/lottery/v4/m/images/index/${bc.code}.png">
								</a> <a class="wanfashuoming" href="javascript:void(0);"><img
									alt="${bc.name}" code="${bc.code}" type="${bc.type}"
									src="${base}/common/template/lottery/v4/m/images/index/img_28_anniu.png"></a>
							</div>
						</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="popupAlert hide">
		<div class="popup_header">
			<img src="${base}/common/template/lottery/v4/m/images/ico_kefu.png">
		</div>
		<div class="popup_top_index">
			<img
				src="${base}/common/template/lottery/v4/m/images/tishi/index_ts_1.png">
		</div>
		<div class="popup_footer">
			<img
				src="${base}/common/template/lottery/v4/m/images/tishi/index_ts_2.png">
		</div>
	</div>
	<jsp:include page="common/noticeAlert.jsp" />
	<jsp:include page="popup/wanfa_popup.jsp"></jsp:include>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>