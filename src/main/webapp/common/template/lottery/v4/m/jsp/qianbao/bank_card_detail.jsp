<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_bank_card_detail">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb/toBankCard.do"> <span
				class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">绑定新的银行卡</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<form method="post" id="bankCardForm"
				action="${base}/m/toQb/toBankCard.do?type=3">
				<div class="list-block">
					<ul>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon icoChika"></em>
								</div>
								<div class="item-inner">
									<div class="item-input">
										<span>请绑定持卡人本人的银行卡</span><em style="color: #ff4f4f">*</em>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon icon-form-name"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">持卡人</div>
									<div class="item-input">
										<input type="text" name="cardName" placeholder="请输入持卡人姓名">
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon icon-form-name"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">卡&nbsp;&nbsp;&nbsp;号</div>
									<div class="item-input">
										<input type="text" name="cardNo" placeholder="请输入卡号">
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);" id="submitBtn"
								class="button button-big button-fill button-danger">下一步</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>