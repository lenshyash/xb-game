<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<style type="text/css">
.bet-room-body .bar-tab .play4 li{width:20%;}
</style>
<div class="card hide" id="touzhu_jianpan">
	<div class="card-header">
		<span class="leftAr"></span>
		<span class="swpier_title">大小单双</span>
		<span class="rightAr"></span>
	</div>
	<div class="swiper-container swiper-container1"><%--swiper-container swiper-container1 swiper-wrapper --%>
		<div class="swiper-wrapper">
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 1}">
											<c:forTokens items="1,2,3,4,5,6,大,小,单,双" var="num" varStatus="k3" delims=",">
											<li><div data-id="${six.id}" data-code="${six.playCode}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span <c:if test="${k3.index <= 5}">class="k3_img k3_${num}"</c:if>>${num}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li></c:forTokens>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 2}">
											<c:forTokens items="1-1-1,2-2-2,3-3-3,4-4-4,5-5-5,6-6-6" var="num" delims=",">
											<li style="width:33.333%;"><div style="line-height: 20px;" data-id="${six.id}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<c:forTokens items="${num}" delims="-" var="nextNum" varStatus="nextIndex">
														<span class="small_k3_img k3_${nextNum}">${nextNum}<c:if test="${nextIndex.index < 2}">-</c:if></span>
													</c:forTokens>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li></c:forTokens>
										</c:if>
										<c:if test="${six.isNowYear eq 3}">
											<li style="width:33.333%;"><div style="line-height: 20px;" data-id="${six.id}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span>${six.name}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play4">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 4}">
											<li><div data-code="${six.playCode}" data-id="${six.id}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}" data-desc="${six.name}">
													<span>${six.name}</span>
													<p class="kafei">
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 5}">
											<c:forTokens items="1-2,1-3,1-4,1-5,1-6,2-3,2-4,2-5,2-6,3-4,3-5,3-6,4-5,4-6,5-6" var="num" delims=",">
											<li><div style="line-height: 20px;" data-id="${six.id}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<c:forTokens items="${num}" delims="-" var="nextNum" varStatus="nextIndex">
														<span class="small_k3_img k3_${nextNum}">${nextNum}<c:if test="${nextIndex.index == 0}">-</c:if></span>
													</c:forTokens>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li></c:forTokens>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 6}">
											<c:forTokens items="1-1,2-2,3-3,4-4,5-5,6-6" var="num" delims=",">
											<li style="width:33.333%;"><div data-id="${six.id}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<c:forTokens items="${num}" delims="-" var="nextNum" varStatus="nextIndex">
														<span class="k3_img k3_${nextNum}">${nextNum}<c:if test="${nextIndex.index == 0}">-</c:if></span>
													</c:forTokens>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li></c:forTokens>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 如果需要导航按钮 -->
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>

	<div class="swiper-pagination swiper-p1"></div>
</div>