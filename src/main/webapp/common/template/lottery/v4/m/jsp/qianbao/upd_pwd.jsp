<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_upd_pwd_1_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb/setup.do"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">修改取款密码</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="list-block">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon1 icoModifyPas"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">旧密码</div>
								<div class="item-input">
									<input type="password" name="old_psd" placeholder="请输入旧密码"
										minlength="6" maxlength="20">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="list-block">
				<ul>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon1 icoModifyPas"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">新密码</div>
								<div class="item-input">
									<input type="password" name="new_psd" placeholder="请输入新密码"
										minlength="6" maxlength="20">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon1 icoModifyPas"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">确认密码</div>
								<div class="item-input">
									<input type="password" name="new_psd2" placeholder="请再次输入新密码"
										minlength="6" maxlength="20">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block">
				<div class="row">
					<div class="col-100">
						<a href="javascript:void(0);" id="btn"
							class="button button-big button-fill button-danger">提交</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../popup/draw_pwd_popup.jsp"></jsp:include>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>