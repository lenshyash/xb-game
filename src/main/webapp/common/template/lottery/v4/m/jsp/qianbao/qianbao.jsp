<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_qianbao_jsp">
		<c:if test="${indexTwo}">
			<jsp:include page="../include/base_header_2.jsp"></jsp:include>
		</c:if>
		<c:if test="${!indexTwo}">
			<jsp:include page="../include/base_header.jsp"></jsp:include>
		</c:if>
		<jsp:include page="../include/footer.jsp"></jsp:include>
		<!-- 这里是页面内容区 -->
		<div class="content">
			<div class="walletWarp my">
				<div class="wallet">
					<p>
						账户余额<a href="#" id="refreshQBMoney"></a>
					</p>
					<div class="walletAcer">
						<em class="icoAcer">${loginMember.money}</em>
					</div>
				</div>
				<c:if test="${!isGuest}">
					<div class="card marginBottom">
						<div class="card-content">
							<div class="list-block">
								<ul>
									<li><a href="${base}/m/toQb/toRegChange.do"
										class="item-link item-content">
											<div class="item-media">
												<em class="icon icoCz1"></em>
											</div>
											<div class="item-inner">
												<div class="item-title">充值</div>
											</div>
									</a></li>
									<c:choose>
										<c:when test="${not empty rPwd && !rPwd}">
											<li><a href="javascript:void(0);"
												onclick="M.qb.layerPwd();" class="item-link item-content">
										</c:when>
										<c:when test="${not empty cardNo && !cardNo}">
											<li><a href="javascript:void(0);"
												onclick="M.qb.layerCard();" class="item-link item-content">
										</c:when>
										<c:otherwise>
											<li><a href="${base}/m/toQb/toDrawCommit.do"
												class="item-link item-content">
										</c:otherwise>
									</c:choose>
									<div class="item-media">
										<em class="icon icoCz2"></em>
									</div>
									<div class="item-inner">
										<div class="item-title">提现</div>
									</div>
									</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card marginBottom">
						<div class="card-content">
							<div class="list-block">
								<ul>
									<c:choose>
										<c:when test="${not empty rPwd && !rPwd}">
											<li><a href="javascript:void(0);"
												onclick="M.qb.layerPwd();" class="item-link item-content">
										</c:when>
										<c:otherwise>
											<li><a href="${base}/m/toQb/toBankCard.do"
												class="item-link item-content">
										</c:otherwise>
									</c:choose>
									<div class="item-media">
										<em class="icon icoCz3"></em>
									</div>
									<div class="item-inner">
										<div class="item-title">银行卡</div>
									</div>
									</a>
									</li>
									<c:if test="${onlinePay eq 'on'}">
										<li><a href="${base}/m/toQb/billsWebCZ.do"
											class="item-link item-content external">
												<div class="item-media">
													<em class="icon icoCz4"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">充值记录</div>
												</div>
										</a></li>
									</c:if>
									<li><a href="${base}/m/toQb/billsWebQK.do"
										class="item-link item-content external">
											<div class="item-media">
												<em class="icon icoAgent"></em>
											</div>
											<div class="item-inner">
												<div class="item-title">提现记录</div>
											</div>
									</a></li>
									<c:if test="${changeMoney eq 'on'}">
										<li><a href="${base}/m/toQb/billsWebZB.do"
											class="item-link item-content external">
												<div class="item-media">
													<em class="icon icoShare"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">帐变记录</div>
												</div>
										</a></li>
									</c:if>
								</ul>
							</div>
						</div>
					</div>
					<div class="card marginBottom">
						<div class="card-content">
							<div class="list-block">
								<ul>
									<li><a href="${base}/m/toQb/setup.do"
										class="item-link item-content">
											<div class="item-media">
												<em class="icon icoCz5"></em>
											</div>
											<div class="item-inner">
												<div class="item-title">支付安全</div>
											</div>
									</a></li>
								</ul>
							</div>
						</div>
					</div>
				</c:if>
			</div>
		</div>
		<script type="text/javascript">
			var QB = {
					pwd : ${(not empty rPwd && !rPwd)?1:0},
					card : ${(not empty cardNo && !cardNo)?1:0},
			}
	</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>