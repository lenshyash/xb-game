<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_gywm_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toUser.do"> <span class="icon icon-left"></span>
				返回
			</a>
			<h1 class="title">关于我们</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<jsp:include page="/member/${folder}/gywm.jsp"></jsp:include>
		</div>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>