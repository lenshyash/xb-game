<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="betCurOrderPopup">
	<header class="bar bar-nav">
		<h1 class="title">最新注单</h1>
	</header>
	<div class="content" style="padding:0;margin:0;">
		<div class="content-inner">
			<div class="content-block"  style="margin:1rem 0 0;">
				<div class="card card-history">
					<div class="card-header"><span style="width:100%;text-align: center;display: inline-block;">本期已下&nbsp;<font class="betCount" color="red"></font>&nbsp;注</span></div>
					<div class="card-content">
						<div class="card-content-inner">
							<ul>
							</ul>
						</div>
					</div>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);"
								class="button button-big button-fill button-danger close-popup">继续投注</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>