<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="weui-grids navTab">
	<a href="${base}/m/toQb/toRegChange.do" class="weui-grid none-grid">
		<div class="weui-grid__icon">
			<img src="${base}/common/template/lottery/v4/m/images/menu1.jpg" alt="">
		</div>
		<p class="weui-grid__label">存 / 取款</p>
	</a> <a href="${base}/m/toUser/betRecord.do" class="weui-grid none-grid">
		<div class="weui-grid__icon">
			<img src="${base}/common/template/lottery/v4/m/images/menu2.jpg" alt="">
		</div>
		<p class="weui-grid__label">投注记录</p>
	</a> <a href="${base}/m/active.do" class="weui-grid none-grid">
		<div class="weui-grid__icon">
			<img src="${base}/common/template/lottery/v4/m/images/menu3.jpg" alt="">
		</div>
		<p class="weui-grid__label">优惠活动</p>
	</a> <a href="${kfUrl}" class="weui-grid none-grid external">
		<div class="weui-grid__icon">
			<img src="${base}/common/template/lottery/v4/m/images/icon_kefu.jpg" alt="">
		</div>
		<p class="weui-grid__label">在线客服</p>
	</a>
</div>