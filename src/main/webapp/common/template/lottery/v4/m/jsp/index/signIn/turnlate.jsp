﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">   
<title>大转盘活动</title>
<link rel="stylesheet" href="${base }/mobile/anew/signIn/css/bootstrap3/css/bootstrap.min.css">
<link rel="stylesheet" href="${base }/mobile/anew/signIn/css/style.css">
</head>
<body>
<ul class="breadcrumb" style="margin-bottom:0px;">
<c:if test="${!isLogin }">
	<li>请先登录!<a href="${m }/login.do">去登录~</a></li>
</c:if>
<c:if test="${isLogin }">
    <li><a href="${m }/index.do">首页</a> <span class="divider"></span></li>
    <li class="active">${loginMember.account}<span class="divider"></span></li>
<%--     <li class="active">余额:${loginMember.money}<span class="divider"></span></li> --%>
    <li class="active">积分:<strong id="adminJf">${userScore }</strong></li>
</c:if>
</ul>

<div class="turbg">
	<img src="${base }/mobile/anew/signIn/images/turbanner.png" class="img-responsive">
	<img src="${base }/mobile/anew/signIn/images/1.png" id="shan-img" style="display:none;" />
	<img src="${base }/mobile/anew/signIn/images/2.png" id="sorry-img" style="display:none;" />
	<div class="banner">
		<div class="turnplate" style="background-image:url(${base }/mobile/anew/signIn/images/turnplate-bg.png);background-size:100% 100%;">
			<canvas class="item" id="wheelcanvas" width="422px" height="422px"></canvas>
			<img class="pointer" src="${base }/mobile/anew/signIn/images/turnplate-pointer.png"/>
		</div>
	</div>
	
		
	<div class="">
		<img src="${base }/mobile/anew/signIn/images/turshadow1.png" class="img-responsive" style="position:relative;top:-5px;">
		<img src="${base }/mobile/anew/signIn/images/turshadow2.png" class="img-responsive" style="position:relative;bottom:-5px;">
	</div>
	<div style="text-align: center;color: white;font-size: 18px;">抽到奖项请在24小时内联系在线客服申请</div>
</div>
<div class="pt10lr10 mt20">
	<div class="pline"></div>
	<div class="prizebox">
		<div class="ptitle"><strong>中奖榜单</strong><span class="pull-right font12">最近<span class="text-red" id="awardRecordNum">0</span>人中奖</span></div>
		<div class="prizelistwrap" id="demo" style="position:relative;">
			<div class="prizelist" id="demo1">
						
			</div>
			<div class="prizelist" id="demo2"></div>
		</div>
	</div>
</div>

<div class="maskbox"></div>
<!-- 弹窗 -->
<div class="sign-info">
	<div class="text-center text-green font18 sign-status"><strong id="fade-sign-info"></strong></div>
	<div class="text-center ptb15" id="fade-sign-body"></div>
	<div class="text-center"><button class="btn btn-login fade-sign-know">知道了</button></div>
</div>

<!-- 未登录弹窗 -->
<div class="unlogin">
	<div class="text-center text-green font18 sign-status"><strong id="fade-sign-info"></strong></div>
	<div class="text-center ptb15"></div>
	<div class="text-center"><button class="btn btn-login" id="fade-sign-body"></button></div>
</div>
<script type="text/javascript" src="${base }/mobile/anew/signIn/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="${base }/mobile/anew/signIn/js/awardRotate.js"></script>
<script type="text/javascript">
var awardsScore =  parseInt('${awardsScore}');
//弹窗start
function fadeAlert(t,b){
	$('div.sign-info #fade-sign-info').html(t);
	$('div.sign-info #fade-sign-body').html(b);
	$(".sign-info").fadeIn();
}

$(".maskbox").click(function(){
	$(".maskbox").fadeOut();
	$(".sign-info").fadeOut();
});

$(".fade-sign-know").click(function(){
	$(".sign-info").fadeOut();
});
//弹窗end
var speed=40;
var demo=document.getElementById("demo"); 
var demo2=document.getElementById("demo2"); 
var demo1=document.getElementById("demo1"); 
demo2.innerHTML=demo1.innerHTML;
function Marquee(){ 
	if(demo2.offsetTop-demo.scrollTop<=0) 
		demo.scrollTop-=demo1.offsetHeight;
	else{ 
		demo.scrollTop++;
	}
}
MyMar=setInterval(Marquee,speed)  
var turnplate={
		restaraunts:[],				//大转盘奖品名称
		colors:[],					//大转盘奖品区块对应背景颜色
		outsideRadius:192,			//大转盘外圆的半径
		textRadius:155,				//大转盘奖品位置距离圆心的距离
		insideRadius:68,			//大转盘内圆的半径
		startAngle:0,				//开始角度		
		bRotate:false				//false:停止;ture:旋转
};

$(document).ready(function(){
	var isLogin = '${isLogin}';
	if(isLogin && isLogin == 'true'){
		trunRecord();//中奖记录
	}else{
		fadeUnloginAlert('阿哦~您还没有登录哦!','<a href="${base }/mobile/login.do">去登录</a></button>');
	}
	
	//动态添加大转盘的奖品与奖品区域背景颜色
	turnplate.restaraunts = ${awards};
	turnplate.colors = ${awardsColor};
	
	var rotateTimeOut = function (){
		$('#wheelcanvas').rotate({
			angle:0,
			animateTo:2160,
			duration:8000,
			callback:function (){
				fadeAlert('消息','网络超时，请检查您的网络设置！');
			}
		});
	};

	$('.pointer').click(function (){
		//转盘动作
		if(turnplate.bRotate)return;
		turnplate.bRotate = !turnplate.bRotate;
		//获取随机数(奖品个数范围内)
		//var item = rnd(1,turnplate.restaraunts.length);
		//奖品数量等于10,指针落在对应奖品区域的中心角度[252, 216, 180, 144, 108, 72, 36, 360, 324, 288]
		//rotateFn(item, turnplate.restaraunts[item-1]);
		trunMain();//转动大转盘
	});
});

//旋转转盘 item:奖品位置; txt：提示语;
var rotateFn = function (item, txt){
	var angles = item * (360 / turnplate.restaraunts.length) - (360 / (turnplate.restaraunts.length*2));
	if(angles<270){
		angles = 270 - angles; 
	}else{
		angles = 360 - angles + 270;
	}
	$('#wheelcanvas').stopRotate();
	$('#wheelcanvas').rotate({
		angle:0,
		animateTo:angles+1800,
		duration:8000,
		callback:function (){
			fadeAlert('消息',txt);
			turnplate.bRotate = !turnplate.bRotate;
		}
	});
};

function trunMain(){
	var activeId = '${activeId}';
	if(!activeId){
		fadeAlert('消息','活动暂未开启!');
		return;
	}
	$.ajax({
		"url" : "${base}/center/active/award.do",
		"type" : "POST",
	    "dataType" : "json",
	    "data" : {
	    	'activeId' : activeId
	    },
	    "contentType" : "application/x-www-form-urlencoded",
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				rotateFn(result.index, turnplate.restaraunts[result.index-1]);
				var a = $("#adminJf").text();
				var c = parseInt(a) - awardsScore;
				if (c && c >= 0) {
					$(".breadcrumb #adminJf").text(c.toFixed(2));
				}else{
					$(".breadcrumb #adminJf").text('0.00');
				}
			} else if (ceipstate == 2) {// 后台异常
				fadeAlert('消息',"后台异常，请联系管理员!");
			} else if (ceipstate == 3) { // 业务异常
				fadeAlert('消息',result.msg);
			}
		}
	});
}

function trunRecord(){
	var activeId = '${activeId}';
	if(!activeId){
		return;
	}
	var awardHtml = new Array();
	$.ajax({
		"url" : "${base}/center/active/lastrd.do",
		"type" : "POST",
	    "dataType" : "json",
	    "data" : {
	    	'activeId' : activeId
	    },
	    "contentType" : "application/x-www-form-urlencoded",
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(result){
					for(var i in result){
						awardHtml.push('<div class="clearfix pb10">');
						awardHtml.push('<div class="col-xs-4 clearPadding text-red">'+getAdminName(result[i].account)+'</div>');
						awardHtml.push('<div class="col-xs-4 clearPadding">'+result[i].productName+'</div>');
						awardHtml.push('<div class="col-xs-4 clearPadding">'+result[i].createDatetime+'</div></div>');
					}
					$('#awardRecordNum').html(result.length);
					$('.prizebox #demo1').html(awardHtml.join(''));
				}
			
			} else if (ceipstate == 2) {// 后台异常
				fadeAlert('消息',"后台异常，请联系管理员!");
			} else if (ceipstate == 3) { // 业务异常
				fadeAlert('消息',result.msg);
			}
		}
	});
}

//会员名字处理
function getAdminName(name){
	var xName = name.substr(0,3);
	return xName+"***";
}

function fadeUnloginAlert(t,b){
	$('div.unlogin #fade-sign-info').html(t);
	$('div.unlogin #fade-sign-body').html(b);
	$(".unlogin").fadeIn();
}

function rnd(n, m){
	var random = Math.floor(Math.random()*(m-n+1)+n);
	return random;
	
}
//页面所有元素加载完毕后执行drawRouletteWheel()方法对转盘进行渲染
window.onload=function(){
	drawRouletteWheel();
};

function drawRouletteWheel() {    
  var canvas = document.getElementById("wheelcanvas");    
  if (canvas.getContext) {
	  //根据奖品个数计算圆周角度
	  var arc = Math.PI / (turnplate.restaraunts.length/2);
	  var ctx = canvas.getContext("2d");
	  //在给定矩形内清空一个矩形
	  ctx.clearRect(0,0,422,422);
	  //strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式  
	  ctx.strokeStyle = "#FFBE04";
	  //font 属性设置或返回画布上文本内容的当前字体属性
	  ctx.font = '16px Microsoft YaHei';      
	  for(var i = 0; i < turnplate.restaraunts.length; i++) {       
		  var angle = turnplate.startAngle + i * arc;
		  ctx.fillStyle = turnplate.colors[i];
		  ctx.beginPath();
		  //arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）    
		  ctx.arc(211, 211, turnplate.outsideRadius, angle, angle + arc, false);    
		  ctx.arc(211, 211, turnplate.insideRadius, angle + arc, angle, true);
		  ctx.stroke();  
		  ctx.fill();
		  //锁画布(为了保存之前的画布状态)
		  ctx.save();   
		  
		  //----绘制奖品开始----
		  ctx.fillStyle = "#E5302F";
		  var text = turnplate.restaraunts[i];
		  var line_height = 17;
		  //translate方法重新映射画布上的 (0,0) 位置
		  ctx.translate(211 + Math.cos(angle + arc / 2) * turnplate.textRadius, 211 + Math.sin(angle + arc / 2) * turnplate.textRadius);
		  
		  //rotate方法旋转当前的绘图
		  ctx.rotate(angle + arc / 2 + Math.PI / 2);
		  
		  /** 下面代码根据奖品类型、奖品名称长度渲染不同效果，如字体、颜色、图片效果。(具体根据实际情况改变) **/
		  if(text.indexOf("M")>0){//流量包
			  var texts = text.split("M");
			  for(var j = 0; j<texts.length; j++){
				  ctx.font = j == 0?'bold 20px Microsoft YaHei':'16px Microsoft YaHei';
				  if(j == 0){
					  ctx.fillText(texts[j]+"M", -ctx.measureText(texts[j]+"M").width / 2, j * line_height);
				  }else{
					  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
				  }
			  }
		  }else if(text.indexOf("M") == -1 && text.length>6){//奖品名称长度超过一定范围 
			  text = text.substring(0,6)+"||"+text.substring(6);
			  var texts = text.split("||");
			  for(var j = 0; j<texts.length; j++){
				  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
			  }
		  }else{
			  //在画布上绘制填色的文本。文本的默认颜色是黑色
			  //measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
			  ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
		  }
		  
		  //添加对应图标
// 		  if(text.indexOf("闪币")>0){
			  var img= document.getElementById("shan-img");
			  img.onload=function(){  
				  ctx.drawImage(img,-15,10);      
			  }; 
			  ctx.drawImage(img,-15,10);  
// 		  }else if(text.indexOf("谢谢参与")>=0){
// 			  var img= document.getElementById("sorry-img");
// 			  img.onload=function(){  
// 				  ctx.drawImage(img,-15,10);      
// 			  };  
// 			  ctx.drawImage(img,-15,10);  
// 		  }
		  //把当前画布返回（调整）到上一个save()状态之前 
		  ctx.restore();
		  //----绘制奖品结束----
	  }     
  } 
}
</script>
</html>