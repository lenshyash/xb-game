<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_bank_card_detail_1_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb/toBankCard.do?type=2"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">填写银行卡信息</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content">
			<div class="list-block">
				<ul>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoBank1"></em>
							</div>
							<div class="item-inner">
								<div class="item-input">
									<span>请选择出款银行</span><em style="color: #ff4f4f">*</em>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">出款银行</div>
								<div class="item-input">
									<select name="bankName" id="bankName">
										<option value="建设银行">建设银行</option>
										<option value="工商银行" selected="selected">工商银行</option>
										<option value="农业银行">农业银行</option>
										<option value="中国邮政银行">中国邮政银行</option>
										<option value="中国银行">中国银行</option>
										<option value="中国招商银行">中国招商银行</option>
										<option value="中国交通银行">中国交通银行</option>
										<option value="中国民生银行">中国民生银行</option>
										<option value="中信银行">中信银行</option>
										<option value="中国兴业银行">中国兴业银行</option>
										<option value="浦发银行">浦发银行</option>
										<option value="平安银行">平安银行</option>
										<option value="华夏银行">华夏银行</option>
										<option value="广州银行">广州银行</option>
										<option value="BEA东亚银行">BEA东亚银行</option>
										<option value="广州农商银行">广州农商银行</option>
										<option value="顺德农商银行">顺德农商银行</option>
										<option value="北京银行">北京银行</option>
										<option value="杭州银行">杭州银行</option>
										<option value="温州银行">温州银行</option>
										<option value="上海农商银行">上海农商银行</option>
										<option value="中国光大银行">中国光大银行</option>
										<option value="渤海银行">渤海银行</option>
										<option value="浙商银行">浙商银行</option>
										<option value="晋商银行">晋商银行</option>
										<option value="汉口银行">汉口银行</option>
										<option value="上海银行">上海银行</option>
										<option value="广发银行">广发银行</option>
										<option value="深圳发展银行">深圳发展银行</option>
										<option value="东莞银行">东莞银行</option>
										<option value="宁波银行">宁波银行</option>
										<option value="南京银行">南京银行</option>
										<option value="北京农商银行">北京农商银行</option>
										<option value="重庆银行">重庆银行</option>
										<option value="广西农村信用社">广西农村信用社</option>
										<option value="吉林银行">吉林银行</option>
										<option value="江苏银行">江苏银行</option>
										<option value="成都银行">成都银行</option>
										<option value="尧都区农村信用联社">尧都区农村信用联社</option>
										<option value="浙江稠州商业银行">浙江稠州商业银行</option>
										<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
									</select>
								</div>
							</div>
						</div>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoBank1"></em>
							</div>
							<div class="item-inner">
								<div class="item-input">
									<span>请填写支行信息</span><em style="color: #ff4f4f">*</em>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">省份</div>
								<div class="item-input">
									<input type="text" name="branch" id="province"
										placeholder="请输入省份">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">城市</div>
								<div class="item-input">
									<input type="text" name="branch" id="city" placeholder="请输入城市">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">开户网点</div>
								<div class="item-input">
									<input type="text" name="branch" id="bankcardaddress"
										placeholder="请输入开户网点">
								</div>
							</div>
						</div>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoModifyPas"></em>
							</div>
							<div class="item-inner">
								<div class="item-input">
									<span>请填写您的取款密码</span><em style="font-size: .7rem; color: #ff4f4f">*</em>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">取款密码</div>
								<div class="item-input">
									<input type="password" name="password" id="cashPassword"
										placeholder="请输入您的取款密码">
								</div>
							</div>
						</div>
						<p class="BanText">仅用于身份验证</p>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-media">
								<em class="icon icoPhone1"></em>
							</div>
							<div class="item-inner">
								<div class="item-input">
									<span>请填写手机号</span><em style="font-size: .7rem; color: #ff4f4f">*</em>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="item-content">
							<div class="item-media">
								<em class="icon icon-form-name"></em>
							</div>
							<div class="item-inner">
								<div class="item-title label">手机号码</div>
								<div class="item-input">
									<input type="number" name="mobile" id="phone"
										placeholder="请输入真实有效的手机号">
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="content-block">
				<input type="hidden" name="name" value="${cardName}"
					id="accountname"> <input type="hidden" name="account"
					value="${cardNo}" id="bankId">
				<div class="row">
					<div class="col-100">
						<a href="javascript:void(0);" id="upData"
							class="button button-big button-fill button-danger">下一步</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../popup/info_data_popup.jsp"></jsp:include>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>