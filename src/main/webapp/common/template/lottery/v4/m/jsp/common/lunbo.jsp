<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Slider -->
<div class="${isIndex?'shouye_index_version':'shouye-body-banner'}">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<c:forEach items="${lunbo}" var="lun">
				<div class="swiper-slide">
					<a class="shouye-sw-box" href="${lun.titleUrl}"><img
						alt="${lun.title}" src="${lun.titleImg}" /></a>
				</div>
			</c:forEach>
		</div>
		<div class="swiper-pagination"></div>
	</div>
</div>