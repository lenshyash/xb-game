<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-right panel-cover" style="background: #efeff4;position:fixed;display:block;">
	<div class="content-block" style="margin: 0; padding: 0;">
		<div class="card" style="margin: .5rem;">
			<div class="card-content">
				<div class="list-block" style="margin:0;">
					<ul>
						<li><a href="#" class="item-link item-content close-panel-active">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoSignOut"></em>
								</div>
								<div class="item-inner" style="margin-left: 2rem;">
									<div class="item-title">关闭</div>
								</div>
						</a></li>
						<li>
							<a href="${base}/m/toUser/betRecord.do?isBet=${room.id}&isType=${bl.type}" class="item-link item-content external">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoRecord"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">投注记录</div>
								</div>
						</a></li>
						<li><a
							href="${base}/m/openAward.do"
							class="item-link item-content external">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoResult"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">开奖结果</div>
								</div>
						</a></li>
						<li><a
							href="${base}/m/gameRule.do"
							class="item-link item-content external">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoIntroduce"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">玩法介绍</div>
								</div>
						</a></li>
						<li><a
							href="${base}/m/openAward.do?type=trend"
							class="item-link item-content external">
								<div class="item-media" style="padding: 0;">
									<em class="icon icoTrend"></em>
								</div>
								<div class="item-inner">
									<div class="item-title">开奖走势</div>
								</div>
						</a></li>
					</ul>
				</div>
			</div>
		</div>
		<c:forEach items="${bcList}" var="list">
			<div class="content-block-title" style="margin: .75rem .75rem .5rem;">
				${list.name}
			</div>
			<div class="card">
				<div class="card-content">
					<div class="list-block" style="margin:0;">
						<ul>
							<c:forEach items="${roomMap}" var="map">
								<c:if test="${map.key == list.code}">
									<c:forEach items="${map.value}" var="room">
									<li><a
										href="${base}/m/toBetRoom.do?rId=${room.id}&lotType=${room.lotType}"
										class="item-link item-content external">
											<div class="item-media">
												<img alt="${room.name}"
													src="${base}/mobile/v3/images/lottery/${list.code}.png"
													width="30">
											</div>
											<div class="item-inner" style="margin-left:1rem;">
												<div class="item-title">${room.name}</div>
											</div>
									</a></li>
									</c:forEach>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>