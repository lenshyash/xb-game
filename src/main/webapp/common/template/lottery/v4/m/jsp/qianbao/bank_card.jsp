<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_bank_card_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/toQb.do"> <span class="icon icon-left"></span> 返回
			</a>
			<h1 class="title">${(not empty member.cardNo && not empty member.bankName)?'银行卡管理':'绑定新的银行卡'}</h1>
		</header>
		<%-- 这里是页面内容区 --%>
		<div class="content" style="background: #fff;">
			<c:if test="${empty member.cardNo && empty member.bankName}">
				<div class="bankCard">
					<div class="binding">
						<a href="${base}/m/toQb/toBankCard.do?type=2"><i
							class="icoAdd1"></i>绑定新的银行卡</a>
					</div>
				</div>
			</c:if>
			<c:if test="${not empty member.cardNo && not empty member.bankName}">
				<div class="details">
					<p>${member.bankName}（储蓄卡）</p>
					<c:set value="${fn:length(member.cardNo) }" var="cardNO"></c:set>
					<p style="text-align: right;">
						<c:choose>
							<c:when test="${cardNO < 5}">
								**** **** **** *** ${member.cardNo}
							</c:when>
							<c:otherwise>
								<c:forEach begin="1" end="${cardNO-4}">*</c:forEach>
								${fn:substring(member.cardNo,cardNO-4,cardNO)}
							</c:otherwise>
						</c:choose>
					</p>
				</div>
				<div class="list-block">
					<ul>
						<li class="item-content">
							<div class="item-media">
								<em class="icon icoXiugai"></em>
							</div>
							<div class="item-inner">
								<div class="item-title">如需更改银行卡信息，请联系客服。</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="content-block" style="margin: 1.2rem 0;">
					<div class="row">
						<div class="col-100">
							<a href="${kfUrl}" external
								class="button button-big button-fill button-success">联系客服</a>
						</div>
					</div>
				</div>
				<div class="content-block" style="margin: 1.2rem 0;">
					<div class="row">
						<div class="col-100">
							<a href="${base}/m/toQb/toDrawCommit.do"
								class="button button-big button-fill button-danger">我要提现</a>
						</div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>