(function($){
	var add_time;
	var add2_time;
	var del_time;
	var beginDatetime;//红包开始时间
	var endDatetime;
	var red_id;//红包id
	var numz = 4;//倒计时秒数
	var win;//屏幕宽度
	var num = 0;//增加红包数量
	
	//定时器
	$.extend({
		timer : function(action, time) {
			var _timer;
			if ($.isFunction(action)) {
				(function() {
					_timer = setInterval(function() {
						action();
					}, time);
				})();
			}
			return _timer;
		}
	});
	
	//===============
	$.redbag = {
			isopen : false,
			intDiff : -3600,
			//获取红包详情,红包个数,红包总金额
			redpacket_detail : function() {
				$.ajax({
					url : base + "/center/redpacket/progress.do",
					success : function(result) {
						
						
						//=======>>>>获取屏幕宽高
						win = (parseInt($(".couten").css("width"))) - 60;
						$(".mo").css("height", $(document).height());
						$(".couten").css("height", $(document).height());
						$(".backward").css("height", $(document).height());
						$("li").css({});
						// 点击红包弹窗确认的时候关闭模态层
						$(".sen a").click(function(){
						  $(".mo").css("display", "none")
						});
						//========>>>>>>>>>>>>>
						
						
						if(!result){
							
							swal({
								  title: "暂无活动!",
								  text: "我们将在2秒后为您转到主页.",
								  timer: 2000,
								  showConfirmButton: false
								},
								function(isConfirm){
									  location.href = base + '/m/index.do';
								});
							return;
						}
						
						$('div.backward-title span').html(result.title);
						beginDatetime = result.beginDatetime;//红包开始时间
						endDatetime = result.endDatetime;
						red_id = result.id;//红包id
						var times = new Date().getTime();
						if(times < endDatetime){
							var redTimes = beginDatetime;
							if (times > redTimes) {
								redTimes = endDatetime;
								intDiff = ~~((redTimes - times) / 1000);
							} else {
								intDiff = ~~((times - redTimes) / 1000);
							}
							//===============>>>>>>>>>>>>>>>>>>>>>>>>>>
							add2_time = setTimeout(add,3000);
							backward();
							//===============>>>>>>>>>>>>>>>>>>>>>>>>>>
							redpacket_record();
						}
					}
				});	
			},
			dtimer : function() {
				var _this = this;
				_this.timer = $.timer(function() {
					var day = 0, hour = 0, minute = 0, second = 0, type = 1;// 时间默认值
//					var times = new Date().getTime();
					var times = nowTime;
					if (times < endDatetime) {
						if (intDiff > 0) {
							type = 1;
						} else {
							type = 2;
						}
					} else {
						return;
					}
					if (intDiff > 0) {
						day = Math.floor(intDiff / (60 * 60 * 24));
						hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
						minute = Math.floor(intDiff / 60) - (day * 24 * 60)
								- (hour * 60);
						second = Math.floor(intDiff) - (day * 24 * 60 * 60)
								- (hour * 60 * 60) - (minute * 60);
					} else {
						var shijian = Math.abs(intDiff)
						day = Math.floor(shijian / (60 * 60 * 24));
						hour = Math.floor(shijian / (60 * 60)) - (day * 24);
						minute = Math.floor(shijian / 60) - (day * 24 * 60)
								- (hour * 60);
						second = Math.floor(shijian) - (day * 24 * 60 * 60)
								- (hour * 60 * 60) - (minute * 60);
					}
					if (minute <= 9)
						minute = '0' + minute;
					if (second <= 9)
						second = '0' + second;
					txt = type == 2 ? "离活动开始还差:" : "离结束还差:";
					$('div span#redbag_time').html(txt + day + '天' + hour + '时' + minute + '分' + second + '秒');
					
					if (type == 2) {
						intDiff++;
						if (intDiff == 0) {
							_this.isopen = true;
							intDiff = ~~((endDatetime - new Date().getTime()) / 1000);
						} else {
							_this.isopen = false;
						}
					} else {
						intDiff--;
						if (intDiff == 0) {
							_this.isopen = false;
							clearInterval(_this.timer);
							swal("活动已结束！");
						} else {
							_this.isopen = true;
						}
					}
				}, 1000);
			}
	}
	//倒计时
	function backward(){
		numz--;
		if(numz>0){
			$(".backward span").html(numz);
		}else{
			//去掉倒计时
			$(".backward").remove();
			//去掉
			$(".backward-title").remove();
		}
		setTimeout(backward,1000)
	}
	
	//红包销毁
	function del(){
		nums++;
		$(".li" + nums).remove();
		del_time = setTimeout(del,200)
	}
	
	//点击抢红包
	function redpacket_click() {
		$.ajax({
		    url:base+'/center/redpacket/grab.do',
		    type:'POST',
		    data:{redPacketId:red_id},
		    success:function(json,status,xhr){
		     	var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					$(".mo font").html(json);
					$(".mo").css("display", "block");
				} else {// 后台异常
					if(ceipstate == 3){
						swal(json.msg);
					}else if(ceipstate == 4){
						swal({
							  title: "请先登录!",
							  text: "我们将在2秒后为您转到登录页面.",
							  timer: 2000,
							  showConfirmButton: false
							},
							function(isConfirm){
								  location.href = base + '/toLogin.do';
							});
					}else {
						swal({
							  title: "抢红包失败！",
							  text: "我们将在2秒后为您重新刷新页面.",
							  timer: 2000,
							  showConfirmButton: false
							},
							function(isConfirm){
								  location.href = location.href;
							});
					}
				}
		    	
		    },complete:function(){
//		    	console.log('执行完毕!');
		    }
		});
	}
	
	//红包记录
	function redpacket_record() {
		$.ajax({
			url : base + "/center/redpacket/marquee.do",
			data:{redPacketId:red_id},
			success : function(result) {
				if(result){
					var txt = "";
					result.forEach(function(e){
						txt += '<font color="white">'+e.account.substr(0,4)+'****:</font><font color="white">'+e.money+'元</font>&nbsp;&nbsp;';
					});
				} else {
					txt = '暂无记录';
				}
				$('div.top-menu div.red-record marquee').html(txt);
			}
		});
	}
	
	//增加红包数量
	function add(){
		var hb = parseInt(Math.random() * (3 - 1) + 1);
		var Wh = parseInt(Math.random() * (70 - 30) + 20);
		var Left = parseInt(Math.random() * (win - 0) + 0);
		var rot = (parseInt(Math.random() * (45 - (-45)) - 45)) + "deg";
		num++;
		$(".couten").append("<li class='li" + num + "' ><a href='javascript:;'><img src='"+base+"/mobile/anew/redPackage/images/hb_" + hb + ".png'></a></li>");
		$(".li" + num).css({
			"left": Left,
		});
		$(".li" + num + " a img").css({
			"width": Wh,
			"transform": "rotate(" + rot + ")",
			"-webkit-transform": "rotate(" + rot + ")",
			"-ms-transform": "rotate(" + rot + ")", /* Internet Explorer */
			"-moz-transform": "rotate(" + rot + ")", /* Firefox */
			"-webkit-transform": "rotate(" + rot + ")",/* Safari 和 Chrome */
			"-o-transform": "rotate(" + rot + ")" /* Opera */
		});	
		$(".li" + num).animate({'top':$(window).height()+20},5000,function(){
			//删掉已经显示的红包
			this.remove()
		});
		//点击红包的时候弹出模态层
		$(".li" + num).click(function(){
			redpacket_click();
			  //-------------停止红包雨
//			  clearTimeout(add_time);
//			  clearTimeout(add2_time);
//			  clearTimeout(del_time);
			//-----------------------
		});
		add_time = setTimeout(add,200)
	}
	
	$.redbag.redpacket_detail();
	$.redbag.dtimer();
})(jQuery);