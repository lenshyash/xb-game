<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="betOrderDetailPopup">
	<header class="bar bar-nav">
		<a class="button button-link button-nav pull-right close-popup">&nbsp;关闭</a>
		<h1 class="title">订单详情</h1>
	</header>
	<div class="content" style="padding: 0; margin: 0;">
		<div class="content-inner">
			<div class="content-block" style="margin: 1rem 0 0;">
				<div class="list-block">
					<ul class="betOrderDetail">
						
					</ul>
				</div>
				<%--<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);"
								class="button button-big button-fill button-danger" onclick="backBetDetail();">关闭</a>
						</div>
					</div>
				</div> --%>
			</div>
		</div>
	</div>
</div>