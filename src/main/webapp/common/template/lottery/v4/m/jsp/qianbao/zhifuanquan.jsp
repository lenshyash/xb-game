<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
		<div class="page" id="page_zhifuanquan_jsp">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/toQb.do"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">支付安全</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toQb/toUpdPwd.do"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon1 icoModifyPas"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">修改取款密码</div>
										</div>
								</a></li>
								<li><a href="#" id="wjmm"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon1 icoModifyPas"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">忘记密码</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${kfUrl}" class="item-link item-content" external>
										<div class="item-media">
											<em class="icon icoNavArt2"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">联系客服</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>