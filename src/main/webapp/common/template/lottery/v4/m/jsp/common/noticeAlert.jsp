<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
$(function(){
	noticeAlert();
});

function noticeAlert(){
	if(!localStorage.getItem('setWindowFrame')){
		noticeData();
	}
}

function showNoticeAlert(){
	noticeData();
}

function noticeData(){
	$.ajax({
		url : "${base}/getConfig/getArticle.do",
		data : {
			code : 19
		},
		type : "post",
		dataType : 'json',
		success : function(j) {
			if (j.length > 0) {
				noticeFrame(j[0].title,j[0].content);
			}
		}
	});
}

function noticeFrame(t,c){
	layer.open({
		  content: c
		  ,title: t
		  ,anim: 'up'
		  ,btn: ['我知道了'],
		  yes: function(index){
			    $('#show_notice_content').show();
			    layer.close(index);
			  }
		});
	$('.layui-m-layersection .layui-m-layerchild h3').css('font-size','1rem');
}
</script>