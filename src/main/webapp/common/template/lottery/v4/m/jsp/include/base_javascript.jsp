<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<link rel="stylesheet" href="${base}/common/template/lottery/v4/m/js/light7/css/light7.min.css">
<%--<link rel="stylesheet" href="${base}/common/template/lottery/v4/m/js/light7/css/light7-swiper.min.css"> --%>
<link rel="stylesheet" type="text/css" href="${base}/common/template/lottery/v4/m/css/swiper-3.4.1.min.css" />
<link rel="stylesheet" type="text/css" href="${base}/common/template/lottery/v4/m/js/light7/css/light7-swipeout.css" />
<link rel="stylesheet" href="${base}/common/template/lottery/v4/m/css/base.css?v=12">
<script type="text/javascript">
	Mobile = {
			base:'${base}',
			folder:'${folder}',
			isLogin:${isLogin},
	}
</script>
<script type='text/javascript' src='${base}/common/template/lottery/v4/m/js/jquery-2.1.4.js' charset='utf-8'></script>
<script type='text/javascript' src='${base}/common/template/lottery/v4/m/js/light7/js/light7.min.js' charset='utf-8'></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>