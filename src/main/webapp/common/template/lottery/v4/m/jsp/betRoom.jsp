<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body class="bet-room-body">
	<div class="page" id="page_betRoom_jsp">
		<style type="text/css">
			.roomNameAnimate{
	    		line-height: 50px;
	    		-webkit-transform: translate3d(0,0,0);
	    		transform: translate3d(0,0,0);
	    		-webkit-transition: all .6s ease;
	    		transition: all .6s ease;
	    		}
				/*其他颜色需更改css样式 007aff */
				.swiper-button-next {
					width: 1.5rem !important;
					top: 1.1rem;
					right: 1rem;
					background-image: none;
					height: 1.5rem;
				}
				.swiper-button-prev {
					width: 1.5rem !important;
					top: 1.1rem;
					left: 1rem;
					background-image: none;
					height: 1.5rem;
				}
				.swiper-pagination.swiper-pagination-fraction {
					top: 10px !important;
					bottom: initial; ! important;
					display: none;
				}
		</style>
		<!-- 这里是页面内容区 -->
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back"
				href="${base}/m/room.do?code=${bl.code}"> <span
				class="icon icon-left"></span> 返回
			</a> <a class="button button-link button-nav pull-right"
				id="jiahao_click"><span class="icon icon-add"></span> </a> <a
				class="button button-link button-nav pull-right"><span
				class="icon icon-kefu"></span> </a>
			<div class="roomNameAnimate">
				<h1 class="title">${bl.name}</h1>
				<h1 class="title" style="top: 2.2rem;">${room.name}</h1>
			</div>
		</header>
		<%--<div class="menu hide"
			style="width: 6rem;<c:if test="${!isMobile}">margin-left:14rem;right:initial;</c:if>">
			<ul>
				<c:if test="${isLogin}">
					<li><a href="${base}/m/toUser/betRecord.do?isBet=${room.id}&isType=${bl.type}" class="external"><em
							class="icon icoRecord"></em>投注记录</a></li>
					<li><a href="${base}/m/openAward.do" class="external"><em
							class="icon icoResult"></em>开奖结果</a></li>
					<li><a href="${base}/m/gameRule.do" class="external"><em
							class="icon icoIntroduce"></em>玩法介绍</a></li>
					<li class="noneBorder"><a
						href="${base}/m/openAward.do?type=trend" class="external"><em
							class="icon icoTrend"></em>开奖走势</a></li>
				</c:if>
				<c:if test="${!isLogin}">
					<li><a href="${base}/toLogin.do"><em
							class="icon icoRecord"></em>投注记录</a></li>
					<li><a href="${base}/toLogin.do"><em
							class="icon icoResult"></em>开奖结果</a></li>
					<li><a href="${base}/toLogin.do"><em
							class="icon icoIntroduce"></em>玩法介绍</a></li>
					<li class="noneBorder"><a href="${base}/toLogin.do"><em
							class="icon icoTrend"></em>开奖走势</a></li>
				</c:if>
			</ul>
		</div> --%>
		<jsp:include page="common/bet_foot.jsp"></jsp:include>
		<div class="card card-top">
			<div class="card-content">
				<div class="card-content-inner">
					<ul class="bet-room-down">
						<li class="roomLi">
							<p>
								距离<font id="issue">加载中...</font>期<font id="kp_jz">截止</font>
							</p>
							<p>
								<span class="icoTimeClose" style="display: none;">已封盘</span> <span
									class="icoTime">加载中...</span>
							</p>
						</li>
						<li>
							<p>余额</p>
							<p>
								<span class="icoAcer">加载中...</span>
							</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="card-footer">
				<span <c:if test="${bl.type == 158}">class="pk10"</c:if>></span><em></em>
			</div>
			<div class="card-header hide">
				<span><em>本期已下<font class="betCount">加载中...</font>注
				</em></span> <span id="quxiaoXz">查看注单详情<em class="jt"></em></span>
			</div>
		</div>
		<div class="content" id="content">
			<input type="hidden"
				value="${empty room.speakMoney?'0':room.speakMoney}" id="speakMoney">
			<input type="hidden" value="${room.betStatus}" id="betStatus">
			<input type="hidden" value="${room.robotStatus}" id="robotStatus">
			<input type="hidden" value="${isGuest?'1':'2'}" id="isGuest">
		</div>
		<script type="text/javascript">
			var count = 0;
			var animateName = true;
			var hostUrl = window.location.host;
			var Room = {
				lotCode : '${bl.code}',
				lotType : '${bl.type}',
				roomId : '${room.id}',
				account : '${loginMember.account}',
				levelImg : '${levelImg}',
			}
		</script>
	</div>
	<div class="hide" id="whiteDiv"
		style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0); z-index: 99;"></div>
	<div class="popupAlert hide">
		<div class="popup_header">
			<img style="position: absolute; top: -2.9rem; width: 100%; right: 0;"
				src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_1.png">
		</div>
		<div class="popup_footer">
			<img style="position: absolute;"
				src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_2.png">
		</div>
	</div>
	<jsp:include page="popup/open_right_lottery_all_popup.jsp"></jsp:include>
	<jsp:include page="common/art_template_room.jsp"></jsp:include>
	<jsp:include page="popup/open_history_popup.jsp"></jsp:include>
	<jsp:include page="popup/bet_cur_order_popup.jsp"></jsp:include>
	<jsp:include page="include/need_js.jsp" />
</body>
</html>
