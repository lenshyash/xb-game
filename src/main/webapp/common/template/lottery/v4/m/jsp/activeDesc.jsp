<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_active_desc_jsp">
		<header class="bar bar-nav">
			<a class="button button-link button-nav pull-left back" href="${base}/m/active.do"> <span class="icon icon-left"></span> 返回
			</a>
			<a class="title">优惠活动详情</a>
		</header>
		<div class="content">
			<div class="card">
				<div class="card-header"><span style="display: inline-block;text-align: center;width:100%">${data.title}</span></div>
				<div class="card-content">
					<div class="card-content-inner">${data.content}</div>
				</div>
				<div class="card-footer"><span style="display: inline-block;text-align: right;width:100%"> <fmt:formatDate pattern="yyyy-MM-dd"  value="${data.updateTime}" /></span></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.content .card-content-inner img').each(function(){
				$(this).css('width','100%');
			})
			$('.content .card-content-inner table').each(function(){
				$(this).attr('width','100%');
			})
		});
	</script>
	<style>
	.list-block img {
		width: 100%;
	}
	</style>
</body>
</html>
<jsp:include page="include/need_js.jsp" />