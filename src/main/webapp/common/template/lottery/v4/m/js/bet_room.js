var xiaolianFlag = false;
var betOrderFlag = false;
var valueArray = ['大','小','单','双','大单','大双','小单','小双','极大','极小','红波','蓝波','绿波','豹子'];
var hzArray = ['14,15,16,17,18,19,20,21,22,23,24,25,26,27','0,1,2,3,4,5,6,7,8,9,10,11,12,13','1,3,5,7,9,11,13,15,17,19,21,23,25,27',
               '0,2,4,6,8,10,12,14,16,18,20,22,24,26','15,17,19,21,23,25,27','14,16,18,20,22,24,26','1,3,5,7,9,11,13','0,2,4,6,8,10,12',
               '22,23,24,25,26,27','0,1,2,3,4,5','3,6,9,12,15,18,21,24','2,5,8,11,17,20,23,26','1,4,7,10,16,19,22,25','0,3,6,9,12,15,18,21,24,27'];
var pk10_hzArray = ['5,6,7,8,9,10','1,2,3,4,5','1,3,5,7,9','2,4,6,8,10'];
var faceJSon = ["","[微笑]","[撇嘴]","[色]","[发呆]","[得意]","[流泪]","[害羞]","[闭嘴]","[睡]","[大哭]","[尴尬]","[发怒]","[调皮]",
                "[呲牙]","[惊讶]","[难过]","[酷]","[囧]","[抓狂]","[吐]","[偷笑]","[愉快]","[白眼]","[傲慢]","[饥饿]","[困]","[惊恐]","[流汗]","[憨笑]",
                "[悠闲]","[奋斗]","[咒骂]","[疑问]","[嘘]","[晕]","[抓狂]","[衰]","[骷髅]","[敲打]","[再见]","[擦汗]","[抠鼻]","[鼓掌]","[糗大了]","[坏笑]",
                "[左哼哼]","[右哼哼]","[哈欠]","[鄙视]","[委屈]","[快哭了]","[阴险]","[亲亲]","[吓]","[可怜]","[菜刀]","[西瓜]","[啤酒]","[篮球]",
                "[乒乓球]","[咖啡]","[米饭]","[猪头]","[玫瑰]","[凋谢]","[嘴唇]","[爱心]","[心碎]","[蛋糕]","[雷电]","[炸弹]","[匕首]","[足球]",
                "[爬虫]","[便便]","[月亮]","[太阳]","[盒子]","[抱抱]","[强]","[弱]","[握手]","[胜利]","[抱拳]","[勾引]","[拳头]","[OK]"];

var dataInfo = [];
var cacheT1;
var msgQueue = new Array();
var endTime;
var lastTimer;
var closeFlag = true;
template.helper('$showHtml',function(value){
	return value;
})
/**
 * 前十期开奖结果采集。
 */
template.helper('$hmTplResult',function(arr,type){
	var targetClass = 'shuzi_',html = '',sum=0,bigs=14;
	if(arr.indexOf('?') >= 0){
		html = '<em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>=<em class="shuzi_00">-</em>';
		html += '<em class="colorDan" style="background:#2371ff">-</em>';
		html += '<em class="colorDan" style="background:#f30700">-</em>';
		return html;
	}
	if(type == 160){targetClass = 'k3_img k3_';bigs = 11;}
	for(var i=0;i<arr.length;i++){
		sum += parseInt(arr[i]);
		if(i == 2){
			html += '<em class="'+targetClass+''+_BetRoom.betRoom.firstAddZero(arr[i])+'">'+_BetRoom.betRoom.firstAddZero(arr[i])+'</em>';
		}else{
			html += '<em class="'+targetClass+''+_BetRoom.betRoom.firstAddZero(arr[i])+'">'+_BetRoom.betRoom.firstAddZero(arr[i])+'</em>+';
		}
		
	}
	html += '=<em class="shuzi_'+_BetRoom.betRoom.firstAddZero(sum)+'">'+_BetRoom.betRoom.firstAddZero(sum)+'</em>';
	html += '<em class="colorDan" style="'+(sum>=bigs?'background:#2371ff':'background:#f30700')+'">'+(sum>=bigs?'大':'小')+'</em>';
	html += '<em class="colorDan" style="'+(sum%2==0?'background:#f30700':'background:#2371ff')+'">'+(sum%2==0?'双':'单')+'</em>';
	return html;
})
$(function(){
	 var _betBody = $(".bet-room-body"), _barTab = _betBody.find(".bar-tab"),_touzhu = _betBody.find("#touzhu_jianpan"),_content = _betBody.find("#content"),_input=$("#inputValue"),
	 	 _foot = _betBody.find(".foot-expression"),_cardTop = _betBody.find(".card-top"),_cardHis = _betBody.find(".card-history");
	 
	 _barTab.on("click","#betOrder",function(){
		 var t=$(this);
		 if(t.text().trim() == "已封盘" && !_BetRoom.isOpen){
			 $.toast("期号["+$("#issue").text()+"]已封盘");
			 return;
		 }
		 if(_foot.css("display") == "block"){
			 _input.val('');
			 _foot.addClass("hide");
		 }
		 if(!betOrderFlag && _touzhu.css("display") == "none"){
			 betOrderFlag = true;
			 t.addClass("hide").siblings("#betCancel").removeClass("hide");
			 _touzhu.removeClass("hide");
			 initSwiper();
			 return;
		 }else{
			 t.addClass("hide").siblings("#betCancel").removeClass("hide");
			 _touzhu.removeClass("hide");
		 }
	 }).on("click","#betCancel",function(){
		 var t=$(this);
		 t.addClass("hide").siblings("#betOrder").removeClass("hide");
		 _touzhu.addClass("hide");
	 }).on("click","#xiaolianBtn",function(){
		 if(_touzhu.css("display") == "block"){
			_touzhu.addClass("hide");
			$("#betOrder").removeClass("hide").siblings("#betCancel").addClass("hide");
		 }
		 if (!xiaolianFlag && _foot.css("display") == "none") {
			xiaolianFlag = true;
			_foot.removeClass("hide");
			var swiper2 = new Swiper(".swiper-container2",{pagination : '.swiper-p2'})
			return;
		}
		if (_foot.css("display") == "block") {
			_foot.addClass("hide");
		} else {
			_foot.removeClass("hide");
		}
	 }).on("focus","#inputValue",function(){
		 if(_foot.css("display") == "block"){	//需要先关闭才可以
			 _foot.addClass("hide");
		 }
	 }).on("click","#speak",function(){
		 var t= $(this);
		 if(!speakMoney()){
			//$.toast("账户低于" +parseInt($("#speakMoney").val())+"元宝,不能发言"); 
			 $.toast("禁止发言");
			_input.val('');
			return false;
		 }
		 if(!_input.val()){
			 $.toast("内容不能为空");
			 _input.val('');
			 return false;
		 }
		 //判断是否是图片[];
		 var _inputS = _input.val().split(" ");
		 var content='';
		 $.each(_inputS,function(i,j){
			 var index = $.inArray(j,faceJSon);
			 if(index>0){
				 content += '<img src="'+Mobile.base+'/common/template/lottery/v4/m/images/face/'+xiaolianZero(index)+'@2x.png "/>';
			 }else{
				 content += !_inputS[i]?"":_inputS[i];
			 }
		 })
		 if(!content){
			 content = _input.val();
		 }
		 var data = {
				"message":content,
				"commandid":1006,
				"account":Room.account,
	    		"nickUrl":$.cookie('account_'+Room.account+'_touxiang')
		 }
		 _input.val('');
		 _BetRoom.betRoom.resultMsg(data);
		 _foot.addClass("hide");
	 })
	 
	 
	 //_touzhu
	 _touzhu.on("click","ul li div",function(){
		 dataInfo = [];//	重新在初始化一次，防止其他地方遗漏
		 var t = $(this);pk10 = t.parent().parent().siblings("div.pk10_nav");
		 var pk10_len = pk10.length,pk10_i = pk10.find('i[class^=color]');
		 if(Room.lotType == 158 && pk10_len > 0){
			 if(pk10_i.length == 0){
				 //必须选中一个。
				 $.toast("请选择一个名次");
				 return;
			 }
			 hzArray = pk10_hzArray;
		 }
		 var name=t.find("span").text().trim(),index = $.inArray(name,valueArray);
		 console.info(pk10_i.data("num"));
		 if(parseInt(pk10_i.data('num')) > 5 && (name == '龙' || name == '虎')){
			 $.toast('该名次不允许选择龙虎投注');
			 return false;
		 }
		 var obj = new Object(),oddsId=t.attr("data-id"),code=t.attr("data-code"),min=t.attr("data-min"),max=t.attr("data-max");
		 if(!oddsId || !code || !max || !min){
			 $.toast("参数异常，请刷新页面");
			 return false;
		 }
		 if(Room.lotType == 158){
			 name = pk10_i.text() + (pk10_i.text()?'--':'') + name;
		 }
		 obj.oddsId = oddsId;
		 obj.name = name;
		 obj.playCode = code;
		 var title = '';
		 if(!hzArray[index]){
			 title = '['+name+']';
		 }else{
			 title = '['+name+']['+hzArray[index]+']';
		 }
		 layer.open({
			 title: [
		         ''+title+'',
		         'background-color:#dc5d55; color:#fff;'
			  ],
			  anim: 'up',
			  content: '<ul class="text-ul"><li>20</li><li>50</li><li>100</li><li>200</li>'+
					'<li>500</li><li>1000</li><li>2000</li><li>5000</li><li>10000</li></ul>'+
					'<div class="moneyInput">其他金额：<input type="number" id="inputNumber"></div>',
			  btn: ['确认', '取消'],
			  yes: function(index){
				  var money = parseInt($("#inputNumber").val()), nowMoney = parseFloat($("span.icoAcer").text())||0;
				  if(!money || money <= 0 ){
					  $.toast("投注金额不能为空");
					  return;
				  }
				  if(money < min){
					  $.toast("投注金额不能低于"+min+ "元宝");
					  return false;
				  }
				  if(money > max){
					  $.toast("投注金额不能高于"+max+"元宝");
					  return false;
				  }
				  if(money > nowMoney){
					  $.toast("余额不足，请充值");
					  return;
				  }
				  if(!betStatus()){
					  $.toast("该房间暂时不能投注");
					  return;
				  }
				  obj.money = money;
				  dataInfo.push(obj);
				  //确认投注咯
				  betBuyOrder();
				  layer.close(index);
			  }
			});
	 }).on("click",".peilvshuom button",function(){
		 var t = $(this),text = t.siblings().html();
		 console.info(text);
		 layer.open({
			 title:['赔率说明','background-color:#dc5d55; color:#fff;font-size:.9rem'],
			 anim:'up',
			 btn:['关闭'],
			 content:text
		 })
	 }).on("click",".pk10_nav i",function(){
		 var $this = $(this),_num = $this.data('num');
		 $this.addClass("color"+_num).siblings().removeAttr("class");
	 })
	 
	 
	 /*body*/
	 $("body").on("click",".text-ul li",function(){
		 var t = $(this),money = t.text().trim();
		 if(money){
			 $("#inputNumber").val(money);
			 return;
		 }
		 $.toast("点击金额加载错误了....");
	 })
	 
	 _content.on("click",".person-money-right a.gentou-zhudan",function(){
		 if($("#betOrder").text().trim() == "已封盘" && !_BetRoom.isOpen){
			 $.toast("期号["+$("#issue").text()+"]已封盘");
			 return false;
		 }
		 var t = $(this),cache = t.find(".dataInfoCache"),nowMoney = parseFloat($("span.icoAcer").text())||0;
		 var dataName = cache.attr("data-name"),dataMoney = cache.attr("data-money"),dataQiHao = cache.attr("data-qihao"),dataCode = cache.attr("data-code"),dataOddsId = cache.attr("data-oddsid");
		 if(!dataName || !dataMoney || !dataQiHao || !dataCode || !dataOddsId){
			 $.toast("跟投失败,存在参数异常");
			 return false;
		 }
		 if(dataQiHao != $("#issue").text()){
			 $.toast("只能跟投当期的注单");
			 return false;
		 }
		 var obj = new Object();
		 obj.playCode = dataCode;
		 obj.name = dataName;
		 obj.oddsId = dataOddsId;
		 layer.open({
			 title:['确定跟投吗？','background-color:#dc5d55; color:#fff;font-size:.9rem'],
			 anim: 'up',
			 btn: ['确认', '取消'],
			 content: '<p class="money-lay-word">玩家：<span>'+t.parent().find("p.accountName").text()+'</span></p><p class="money-lay-word">期数：<span>'+dataQiHao+'</span></p>' + 
			 	'<p class="money-lay-word">类别：<span>'+dataName+'</span></p><p class="money-lay-word">金额：<span>'+dataMoney+'</span></p>',
			 yes:function(index){
				 if(parseFloat(dataMoney) > nowMoney){
					 $.toast("余额不足，请充值");
					 return false;
				 }
				 if(!betStatus()){
					  $.toast("该房间暂时不能投注");
					  return;
				  }
				 obj.money = dataMoney;
				 dataInfo.push(obj);
				 //跟投投注;
				 betBuyOrder();
				 layer.close(index);
			 }
		 })
	 })
	 
	 
	 $(".foot-expression").on("click",".xiaolianOK",function(){
		 var t = $(this),index=t.find("img").attr("alt");
		 _input.val(_input.val()+" "+faceJSon[parseInt(index)]);
	 }).on("click",".xiaolianDelete",function(){
		 var t = $(this),_inputSplit = _input.val().split(" ");
		 var aInput = _inputSplit.pop();
		 _input.val(_inputSplit.join(" "));
	 })
	 
	 
	 /*card*/
	 _cardTop.on("click",".card-header",function(){
		 $.popup("#betCurOrderPopup");
	 }).on("click",".card-footer",function(){
		 $.popup("#openHistoryPopup");
	 })
	 
	 
	 /*撤销*/
	 _cardHis.on("click","em.close",function(){
		 var t=$(this), orderId = t.attr("order-no");
	    	layer.open({
	    	    content: '您确定要取消该注单吗？',
	    	    btn: ['确定', '取消'],
	    	    anim: 'up',
	    	    yes: function(index){
	    	      $.ajax({
	    	    	  url:Mobile.base + '/m/ajax/cancelOrder.do',
	    	    	  data:{orderId:orderId,lotCode:_BetRoom.code},
	    	    	  dataType:'json',
	  				  type:'post',
	    	    	  success:function(res){
	    	    		  if(res.success){
	    	    			  $.toast("取消注单成功");
	    	    			  _BetRoom.betRoom.betCurOrder();
	    	    			  window.setTimeout(function(){
	    	    				  refreshMoney();
	    	    			  },2000)
	    	    		  }else{
	    	    			  $.toast(res.msg);
	    	    		  }
	    	    		  layer.close(index);
	    	    	  }
	    	      })
	    	    }
	    	  });
	 })
	 
	 
	 /**
	  * 关闭popup
	  */
	 _betBody.on("click",".backBetRoom",function(){
		 if($("#betCurOrderPopup").css("display") == 'block'){
			 $.closeModal("#betCurOrderPopup");
		 }
		 if($("#openHistoryPopup").css("display") == 'block'){
			 $.closeModal("#openHistoryPopup");
		 }
	 }).on("click",".close-panel-active",function(){
		 $(".panel.panel-right.panel-cover").removeClass("panel_active");
		 $("#whiteDiv").addClass("hide");
	 })
})


function xiaolianZero(value){
	value = parseInt(value);
	if(value>0 && value<10){
		return "00"+value;
	}else if(value<100 && value>10){
		return "0" + value;
	}else{
		return value;
	}
}

function initSwiper(){
	var swiper2 = new Swiper(".swiper-container1",{
		loop : true,
		prevButton : '.swiper-button-prev',
		nextButton : '.swiper-button-next',
		pagination : '.swiper-p1',
		paginationType : 'fraction',
		onSlideChangeEnd : function(swiper) {
			//console.info(swiper.activeIndex) //切换结束时，告诉我现在是第几个slide
			var text = $(".swiper-p1").find(".swiper-pagination-current").text(), titleText = '';
			switch (parseInt(text)) {
			case 1:
				titleText = Room.lotType==158?'双面盘':Room.lotType == 161?'大小单双':'大小骰宝';
				break;
			case 2:
				titleText = Room.lotType==158 || Room.lotType == 161?'数字':'围骰丶全骰';
				break;
			case 3:
				titleText = Room.lotType==158?'冠亚和':Room.lotType == 161?'特殊玩法':'点数';
				break;
			case 4:
				titleText = '长牌'
				break;
			case 5:
				titleText = '短牌';
				break;
			}
			$("#touzhu_jianpan .card-header span.swpier_title").text(titleText);
		}
	});
}

function betStatus(){
	var betStatus = parseInt($("#betStatus").val());
	if(betStatus && betStatus == 1){
		return false;
	}
	return true;
}

function speakMoney(){
	var money = parseInt($("#speakMoney").val());
	var nowMoney = parseFloat($(".icoAcer").text());
	if(money && money > 0 && nowMoney > money){
		return true;
	}
	return false;
}


/*投注方法*/
function betBuyOrder(){
	layer.open({
	    type: 2,
	    shadeClose:false,
	    content: '正在提交注单，请稍后...'
	  });
	var info = dataInfo;
	var data = {
			"commandid":1005,
			"data":JSON.stringify(dataInfo),
			"qiHao":_BetRoom.qiHao,
			"lotType":_BetRoom.type,
			"lotCode":_BetRoom.code,
			"roomId":Room.roomId
		}
		$.ajax({
			url:Mobile.base + '/m/ajax/toBetInfo.do',
			data:data,
			dataType:'json',
			type:'post',
			success:function(res){
				if(res.success){
					refreshMoney();
					//显示订单
					_BetRoom.betRoom.betCurOrder(); 
					data['data'] = info;	//修改data中的值，不进行JSON格式化
					data['account'] = Room.account;
					data['nickUrl'] = $.cookie('account_'+Room.account+'_touxiang');
					//使用机器人提示成功注单
					_BetRoom.betRoom.resultMsg(data);
					_BetRoom.utils.cancel();
					layer.closeAll();
				}else{
					$.toast(res.msg);
					layer.closeAll();
				}
				dataInfo = [];
			}
		})
}

var refreshTimers;	//余额定时器
function refreshMoney(){
	var money = $("span.icoAcer");
	$.ajax({
		url:Mobile.base + '/meminfo.do',
		dataType:'json',
		type:'post',
		success:function(res){
			if(res.login){
				clearTimeout(refreshTimers);
				money.text(res.money);
				_BetRoom.account = res.account;
				refreshTimers = setTimeout(function(){	//10秒刷新一次余额
					refreshMoney();
				},10000);
			}else{
				clearTimeout(refreshTimers);
				money.text('0.00');
			}
		},
		beforeSend:function(){
			money.text("加载中...");
		} 
	})
}




var _BetRoom = {
		qiHao:null,
		code:null,
		type:null,
		isSuoHa:false,	//是否梭哈
		isOpen:true,	//是否封盘
		account:null,
		initBetRoom:function(){
			_BetRoom.code = Room.lotCode;	//彩种code
			_BetRoom.type = Room.lotType;	//彩种type
			_BetRoom.betRoom.initOpen();
			_BetRoom.utils.initMsg();
			refreshMoney();
		}
}
	
_BetRoom.betRoom = function(){
	return {
		resultMsg:function(req_msg){
			var data = {
					data:JSON.stringify(req_msg)
			}
			$.ajax({
				url:Mobile.base + '/m/ajax/reqMsg.do',
				dataType:'json',
				type:'post',
				data:data,
				success:function(res){
					if(res.success){
						_BetRoom.utils.onMessage(res.msg);
					}
				}
			})
		},
		initOpen:function(){
			$.ajax({
				url:Mobile.base+'/m/ajax/roomOpen.do',
				dataType:'json',
				type:'post',
				data:{lotCode:Room.lotCode},
				success:function(res){
					if(res.success){
						_BetRoom.betRoom.roomCur(res.current);
						_BetRoom.betRoom.roomHistory(res.history);
						_BetRoom.betRoom.roomLast(res.last,false);
					}else{
						$.toast(res.msg);
					}
				}
			})
		},
		roomLast:function(last,flag){
			var html = '第'+last.qiHao+'期  ',hm=last.haoMa,sum=0;
			if(hm.indexOf("?")>=0){
				clearInterval(lastTimer);
				//没开奖
				html += '开奖中...';
				lastTimer = window.setInterval(function(){
					_BetRoom.betRoom.lastData(last.qiHao);
				},5000);
			}else{
				hm = hm.split(",");var html1='';
				switch(parseInt(_BetRoom.type)){
					case 161:
						for(var i=0;i<hm.length;i++){
							sum += parseInt(hm[i]);
							if(i==2){
								html1 += ''+_BetRoom.betRoom.firstAddZero(hm[i])+'';
							}else{
								html1 += ''+_BetRoom.betRoom.firstAddZero(hm[i])+'+';
							}
						}
						html1 += '='+_BetRoom.betRoom.firstAddZero(sum)+'(';
						html1 += sum>=14?'大':'小';
						html1 += '/';
						html1 += sum%2==0?'双':'单';
						html1 += ")";
						break;
					case 158:
						html1 += '<font style="display:inline-block;margin-bottom:.25rem;">';
						for(var i=0;i<hm.length;i++){
							html1 += '<i class="colorjieguo color'+parseInt(hm[i])+'">'+parseInt(hm[i])+'</i>';
						}
						html1 += '</font>';
						break;
					case 160:
						for(var i=0;i<hm.length;i++){
							sum += parseInt(hm[i]);
							html1 += '<i class="small_k3_img k3_'+hm[i]+'">'+hm[i]+'</i>'
						}
						html1 += '<i class="colorjieguo color2">'+sum+'</i>'
						html1 += '<i class="colorjieguo '+(sum>10?'color3':'color4')+'">'+(sum>10?'大':'小')+'</i>';
						html1 += '<i class="colorjieguo '+(sum%2==0?'color5':'color6')+'">'+(sum%2==0?'双':'单')+'</i>';
						break;
					default:
						break;
				}
				html = html + html1;
			}
			$(".card-top .card-footer span").html(html);
		},
		lastData:function(qiHao){
			var data = {
					qiHao:qiHao,
					lotCode:_BetRoom.code
			}
			$.ajax({
				url:Mobile.base + "/m/ajax/lotLast.do",
				data:data,
				type:'post',
				dataType:'json',
				success:function(res){
					if(res.success){
						//说明上一期已开奖，刷新页面即可，停止定时器
						clearInterval(lastTimer);
						_BetRoom.betRoom.roomLast(res.last,true);
						_BetRoom.betRoom.historyLast(res.last);
						//开奖提示
						//_BetRoom.betRoom.resultMsg({"commandid":1002,"typeId":2100,"roomId":Room.roomId,"qiHao":res.last.qiHao,"openTime":res.last.endTime,"haoMa":res.last.haoMa});
					}
				}
			})
		},firstAddZero:function(num){
			num = parseInt(num);
			if(num<10){
				return '0'+num;
			}
			return num;
		},historyLast:function(last){
			var html = '',hm=last.haoMa,sum=0;
			switch(parseInt(_BetRoom.type)){
				case 158:
					hm = hm.split(',');
					for(var i=0;i<hm.length;i++){
						html += '<i class="color_history color'+hm[i]+'">'+hm[i]+'</i>';
					}
					break;
				case 161:
					hm = hm.split(",");
					for(var i=0;i<hm.length;i++){
						sum += hm[i];
						if(i==2){
							html += '=<em class="shuzi_'+_BetRoom.betRoom.firstAddZero(sum)+'">'+_BetRoom.betRoom.firstAddZero(sum)+'</em>';
						}else{
							html += '<em class="shuzi_'+_BetRoom.betRoom.firstAddZero(hm[i])+'">'+_BetRoom.betRoom.firstAddZero(hm[i])+'</em>+';
						}
					}
					break;
				default:
					break;
			}
			$(".card-open-hm ul.list-container li:eq(0)").find(".item-openAward").html(html);
		},
		roomCur:function(cur){
			if(!cur){
				layer.open({
					shadeClose:false,
				    content: '暂时没有开奖信息',
				    btn:['我知道了']
				  });
				return;
			}
			_BetRoom.qiHao = cur.qiHao;
			_BetRoom.betRoom.betCurOrder();//加载当前期的下注
			$("#issue").text(cur.qiHao);
			BaseTimer.downTimer(cur);
		},
		betCurOrder:function(){	//获取当期下注的订单
			var data = {
					qiHao:_BetRoom.qiHao,
					lotCode:_BetRoom.code
			}
			$.ajax({
				url:Mobile.base+'/m/ajax/betCurOrder.do',
				data:data,
				dataType:'json',
				type:'post',
				success:function(res){
					if(res.success){
						var total=0,html='';
						$.each(res.doOrder,function(i,j){
							html += '<li><label>投注类型：'+j.haoMa+'</label><label>金额：'+j.buyMoney+'<i class="icoAcer"></i></label>';
							html += '<em class="close" order-no="'+j.orderId+'"></em></li>'
							total ++;
						})
						$(".card-top .card-header").removeClass("hide");
						$(".betCount").text(total);
						$(".card-history ul").html(html);
					}else{
						$(".card-top .card-header").addClass("hide").find("#quxiaoXz em").removeClass("rotate");
						$(".betCount").text('加载中...');
						$(".card-history ul").empty();
						$(".bet-room-body .backBetRoom").click();
					}
				}
			})
		},
		roomHistory:function(history){
			var html = '',hm,data,h='';
			$.each(history,function(i,j){
				data = {
						index:i,
						qiHao:j.qiHao,
						time: j.time,
						hmArr: j.haoMa.split(","),
						type : _BetRoom.type,
				}
				html += template('historyLastTpl',data);
			})
			$(".card-open-hm ul.list-container").html(html);
		},
		clearLastData:function(){
			dataInfo = [];
//			var data = $(".roomHead .betTixing");
//			data.hide();
//			data.find("#betCount").text(0);
//			data.find('.quxiao').hide();
//			data.find(".quxiao ul").empty();
//			//data.siblings(".userBetting").remove();
		}
	}
}();


var BaseTimer = function(){
	var downTimers,a_t=0,n_t=0,o_t=0,state,ago=0,ddTime=0,_socket_flag=true,_socket_open=true;
	return obj = {
		downTimer:function(cur){
			var t = this;
			state = cur.state;
			ago = cur.ago;
			this.timerTitle(cur.state);
			a_t = Date.parse(new Date(this.timeFormat(cur.activeTime)));
			n_t = Date.parse(new Date(this.timeFormat(cur.serverTime)));
			o_t = (a_t - n_t) / 1E3;
			ddTime = o_t-(a_t - new Date().getTime()) / 1E3;
			t.tick(o_t);
			clearInterval(downTimers);
			downTimers = window.setInterval(function(){
				BaseTimer.tick();
			},1000)
		},
		timeFormat:function(e){
			return new Date(e.replace(/-/g,"/"));
		},
		timerTitle:function(state){
			if(state == 1){
				_BetRoom.isOpen = true;
				$("#kp_jz").text('截止');
				$("#betOrder").text('投注');
			}else{
				_BetRoom.isOpen = false;
				$("#kp_jz").text('开盘');
				$("#betOrder").text('已封盘').removeClass("hide").siblings("#betCancel").addClass("hide");
				$("#touzhu_jianpan").addClass("hide");
			}
		},
		tick:function(){
			var t = this,vg=0,dTime = 0;
				dTime = o_t - (a_t - new Date().getTime()) / 1E3;//	计算倒计时和js时间的差值
				if(dTime > ddTime && dTime - ddTime > 1){
					o_t = o_t - parseInt(dTime-ddTime);
				}
				vg = o_t - ago;
				if(o_t>0 && o_t>ago && state == 1){ //	
					var html = t.diffTimer(vg);
					t.timerHtml(html,1);
					t.timerTitle(1);
					if(_socket_open){
						_socket_open = false;
						_BetRoom.utils.cacheTimer();
						_BetRoom.betRoom.resultMsg({"commandid":1001,"roomId":Room.roomId,"typeId":2001});
					}
					if(vg == 150){_BetRoom.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2004});}
					if(vg == 60){_BetRoom.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2003});}
				}else{
					//var html = t.diffTimer(o_t);
					t.timerHtml(html,2);
					t.timerTitle(2);
					if(_socket_flag && state == 1){
						_socket_flag = false;
						_BetRoom.betRoom.resultMsg({"commandid":1002,"typeId":2002,"roomId":Room.roomId});
					}
					if(state == 2 && _socket_flag){
						_socket_flag = false;
						_BetRoom.betRoom.resultMsg({"commandid":1002,"typeId":2005,"roomId":Room.roomId});
					}
					//支持机器人播报
					if(o_t == 30){_BetRoom.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2007,"hostUrl":hostUrl});}
					if(o_t == 20){_BetRoom.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2008,"hostUrl":hostUrl});}
				}
				if(o_t <= 0){
					_socket_flag = true;
					_socket_open = true;
					clearInterval(downTimers);
					_BetRoom.betRoom.initOpen();
					//清楚上一期的投注记录
					_BetRoom.betRoom.clearLastData();
					return;
				}
				o_t--;
		},
		diffTimer:function(intDiff){
			var day=0,hour=0,hours=0,minute=0,second=0,time_html='';//时间默认值		
			if(intDiff > 0){
				day = Math.floor(intDiff / (60 * 60 * 24));
				hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
				hours = Math.floor(intDiff / (60*60));
				minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
				second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
			}
			if (hours <= 9 ) hours = '0' + hours;
			if (minute <= 9) minute = '0' + minute;
			if (second <= 9) second = '0' + second;
			time_html = hours + '时' + minute + '分' + second + '秒';
			return time_html;
		},
		timerHtml:function(html,state){
			if(state==1){
				$(".icoTime").text(html).show().siblings(".icoTimeClose").hide();
			}else{
				$(".icoTime").hide().siblings(".icoTimeClose").show();
			}
		}
	}
}();


_BetRoom.utils = function(){
	var _bodyScrollHeight = 1000; //默认高度
	return {
		cancel:function(){
			 $("#betCancel").addClass("hide").siblings("#betOrder").removeClass("hide");
			 $("#touzhu_jianpan").addClass("hide");
		},
		initMsg:function(){
			//发送本身登录消息
			var initMsg= {
				"commandid":1003,
				"account":Room.account,
				"roomId":Room.roomId,
				"levelImg":Room.levelImg
			}
			_BetRoom.betRoom.resultMsg(initMsg);
			setTimeout(function(){	//2秒获取缓存投注记录
				var orderCache = {
						"commandid":1009,
						"lotCode":Room.lotCode,
						"roomId":Room.roomId,
						"isVialdate":true
				}
				_BetRoom.betRoom.resultMsg(orderCache);
			},2000);
			_BetRoom.utils.cacheTimer();
		},
		cacheTimer:function(){
			clearInterval(cacheT1);
			cacheT1 = window.setInterval(function(){	//3秒获取一次缓存投注记录
				var orderCache = {
						"commandid":1009,
						"lotCode":Room.lotCode,
						"roomId":Room.roomId,
						"isVialdate":false
				}
				_BetRoom.betRoom.resultMsg(orderCache);
			},3000)
		},
		onMessage:function(msg){
			$.each(msg,function(i,j){
				msgQueue.unshift(JSON.parse(j));
			})
			for(var i=0;i<msgQueue.length;i++){
				var msg = msgQueue.pop();
				_BetRoom.utils.appendHtml(msg);
			}
		},
		appendHtml:function(msg){
			console.info(JSON.stringify(msg));
			var html = '';
			switch(msg.commandid){
			case 1003:
				var data = {
					levelImg:msg.levelImg,
					account:msg.account
				}
				html = template('html_login',data);
				break;
			case 1002:
			case 1001:
			case 1007:
				var data = {
					qiHao : $("#issue").text(),
					message : msg.message
				}
				html = template('html_tishi',data);
				break;
			case 1005:
			case 1009:
			case 1008:
				var data = {
					accounts :Room.account,
					data:msg.message,
					isAdd:msg.isVialdate,
				}
				html = template('html_touzhu_left',data);
				if(msg.isVialdate){
					window.setTimeout(function(){
						$('body').animate({'scrollTop': $('body')[0].scrollHeight}, 300);
					},1500);
				}
				break;
			case 1006:
				var data = {
					account : msg.account,
					message : msg.message,
					levelImg : msg.levelImg,
					nickUrl : $.cookie('account_'+Room.account+'_touxiang'),
				}
				html = template('html_speak',data);
				break;
			}
			$("#content").append(html);
			if(html){
				if($('body')[0].scrollHeight - $('body').scrollTop() < _bodyScrollHeight){
					$('body').animate({'scrollTop': $('body')[0].scrollHeight}, 500);
				}
			}
		}
	}
}();