<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page" id="page_gonggao_jsp">
		<c:if test="${indexTwo}">
			<jsp:include page="../include/base_header_2.jsp"></jsp:include>
		</c:if>
		<c:if test="${!indexTwo}">
			<jsp:include page="../include/base_header.jsp"></jsp:include>
		</c:if>
		<jsp:include page="../include/footer.jsp"></jsp:include>
		<!-- 这里是页面内容区 -->
		<div class="content">
			<c:if test="${not empty lunbo && lunbo.size()>0}">
				<jsp:include page="../common/lunbo.jsp"></jsp:include>
			</c:if>
			<div class="newsTitle">
				<ul id="news_tab">
					<li class="active" data="1">公告</li>
					<li data="2" class="">信息</li>
				</ul>
			</div>
			<div class="newsContent">
				<ul class="record_list " style="display: block;">
				</ul>
				<ul class="record_list" style="display: none;"></ul>
			</div>
		</div>
		<script type="text/javascript">
			var pageDatas = [];
		</script>
	</div>
	<jsp:include page="../popup/wanfa_popup.jsp"></jsp:include>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>