<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">首次设置取款密码</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="firstSetPas">
					<dl>
						<dt>
							<em class="icon1 icoModifyPas"></em><span>请设置取款密码，请勿与银行卡密码相同</span>
						</dt>
						<dd>
							<div class="pasFrame">
								<%-- <div class="dang"></div> --%>
								<input name="password" type="number" maxlength="6"
									class="inputPas" autocomplete="off">
								<ul>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
								</ul>
							</div>
						</dd>
					</dl>
					<dl>
						<dt>
							<em class="icon1 icoModifyPas"></em><span>再次输入取款密码</span>
						</dt>
						<dd>
							<div class="pasFrame">
								<%-- <div class="dang"></div> --%>
								<input name="password2" type="number" maxlength="6"
									class="inputPas" autocomplete="off">
								<ul>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
								</ul>
							</div>
						</dd>
					</dl>
					<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);" id="setBtn"
								class="button button-big button-fill button-danger">下一步</a>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/draw_upd_popup.jsp"></jsp:include>
		<script type="text/javascript">
        $(function() {
        	$(".inputPas").on("focus",function(){
                var index =$(this).val().length;
                if(index ==6){
                    $(this).siblings("ul").children("li").eq(index-1).addClass("guangbiao1");
                }else{
                    $(this).siblings("ul").children("li").eq(index).addClass("guangbiao");
                }
        	})
        	 $(".inputPas").on("blur",function(){
		        $(this).siblings("ul").children("li").removeClass("guangbiao");
		        $(this).siblings("ul").children("li").removeClass("guangbiao1");
		    })
		
		    $(".inputPas").on("input change",function(){
		        $(this).val($(this).val().replace(/[^\d]/g,''));
		        var index =$(this).val().length;
		        var val =$(this).val();
		        if(index > 6){
		            val=val.substring(0,6);
		            $(this).val(val);
		            return false;
		        }
		        var oLi =$(this).siblings("ul").children("li");
		        if(index){
		            oLi.eq(index).prevAll().text("*");
		            oLi.eq(index-1).nextAll().text("");
		            oLi.removeClass("guangbiao").eq(index).addClass("guangbiao");
		            if(index ==5){
		                oLi.removeClass("guangbiao1");
		            }
		            if(index ==6){
		                oLi.eq(index-1).text("*");
		                oLi.removeClass("guangbiao").eq(index-1).addClass("guangbiao1");
		            }
		
		        }
		        if(!index){
		            oLi.eq(0).text("");
		            oLi.removeClass("guangbiao").eq(0).addClass("guangbiao");
		        }
		
		    })
        	
            $("#setBtn").click(function() {
                //合法性验证
                var password = $("input[name=password]").val();
                var password2 = $("input[name=password2]").val();

                if (password.length < 6) {
                    $.toast("请输入6位数的密码");
                    return false;
                }
                if (password2.length == '') {
                    $.toast("请再次输入密码");
                    return false;
                }
                
                if (password2.length < 6) {
                    $.toast("请输入6位数的密码");
                    return false;
                }
                
                if (password !== password2) {
                    $.toast("两次密码不一致");
                    return false;                   
                }
                
                //验证通过提交数据
                $.ajax({
                    url: "${base}/center/member/meminfo/repwd.do",
                    type: "post",
                    data: {pwd: password, rpwd: password2},
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                        	$.popup("#drawPopup");
                            //window.location.href = "${base}/m/toQb/setSuccess.do?title=设置取款密码&content=设置取款密码成功";
                        } else {
                        	$.toast(data.msg);
                        }
                    },
                    error: function(error) {
                        layer.open({
                            content: "<p class='alert_msg'>服务器出错了</p>",
                            btn: '我知道了'
                        });
                        $.toast("服务器出错了");
                    }
                });
            }); 
        });
    </script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>