<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/html" style="display: none;" id="toOnlinePayTemplate">
<form action="{{formAction}}" method="post" id="onlinePayForm">
	{{each formHiddens as item}}
		<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}
	<div class="list-block">
		<ul>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">会员帐号</div><div class="item-input">{{account}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">订&nbsp;&nbsp;单&nbsp;&nbsp;号</div><div class="item-input">{{orderId}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">{{amount}}</div></div></div></li>
		</ul>
	</div>
	<div class="content-block">
		<div class="row">
			<div class="col-100">
				<a href="javascript:void(0);" class="button button-big button-fill button-danger" onclick="onlineSubmit();">提&nbsp;&nbsp;交</a>
			</div>
		</div>
	</div>
</form>
</script>

<script type="text/html" style="display: none;" id="toFastPayTemplate">
	<div class="list-block">
		<ul>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值方式</div><div class="item-input">{{payName}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{payUserName}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{payAccount}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">订&nbsp;&nbsp;单&nbsp;&nbsp;号</div><div class="item-input">{{orderNo}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">{{money}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">二&nbsp;&nbsp;维&nbsp;&nbsp;码</div><div class="item-input"><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="margin: .5rem;width: 8rem; height: 8rem;display:inline-block;"></div></div></div></li>
		</ul>
	</div>
	<div class="card" style="background: #fbfbfb;">
        <div class="card-content">
        	<div class="card-content-inner" style="color:red;">贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span style="color:blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</div>
        </div>
    </div>
	<div class="content-block">
		<div class="row">
			<div class="col-100">
				<a href="${base}/m/toQb/billsWebCZ.do" class="button button-big button-fill button-danger close-popup">前往查看充值记录</a>
			</div>
		</div>
	</div>
</script>


<script type="text/html" style="display: none;" id="toBankPayTemplate">
	<div class="list-block">
		<ul>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值银行</div><div class="item-input">{{payName}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">收款姓名</div><div class="item-input">{{creatorName}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">收款帐号</div><div class="item-input">{{bankCard}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">开户网点</div><div class="item-input">{{bankAddress}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">订&nbsp;&nbsp;单&nbsp;&nbsp;号</div><div class="item-input">{{orderNo}}</div></div></div></li>
			<li><div class="item-content"><div class="item-inner"><div class="item-title label">充值金额</div><div class="item-input">{{money}}</div></div></div></li>
		</ul>
	</div>
	<div class="card" style="background: #fbfbfb;">
        <div class="card-content">
        	<div class="card-content-inner" style="color:red;">贴心提醒：充值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</div>
        </div>
    </div>
	<div class="content-block">
		<div class="row">
			<div class="col-100">
				<a href="${base}/m/toQb/billsWebCZ.do" class="button button-big button-fill button-danger close-popup">前往查看充值记录</a>
			</div>
		</div>
	</div>
</script>