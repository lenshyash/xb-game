<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header class="bar bar-nav">
	<a class="button button-link button-nav pull-right" id="jiahao_click"><span
		class="icon icon-add"></span> </a> <a
		class="button button-link button-nav pull-right" external
		href="${kfUrl}"><span class="icon icon-kefu"></span> </a>
	<h1 class="title">${empty navClass?_title:navClass eq 'DATING'?_title:navClass eq 'QIANBAO'?'钱包':navClass eq 'WODE'?'个人中心':navClass eq 'DONGTAI'?'动态':_title}</h1>
	<div class="menu hide" style="width: 5rem; <c:if test="${!isMobile}">margin-left:15rem;right:initial;</c:if>">
		<ul>
			<c:if test="${isLogin}">
				<li><a href="${base}/m/toQb/toRegChange.do"><em class="icon icoCz1"></em>充值</a></li>
				<li class="noneBorder"><a href="${base}/m/toQb/toDrawCommit.do" ><em class="icon icoCz2"></em>提现</a></li>
			</c:if>
			<c:if test="${!isLogin}">
				<li><a href="${base}/toLogin.do"><em class="icon icoCz1"></em>充值</a></li>
				<li class="noneBorder"><a href="${base}/toLogin.do"><em class="icon icoCz2"></em>提现</a></li>
			</c:if>
		</ul>
	</div>
	<div class="hide" id="whiteDiv" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0); z-index: 99;"></div>
</header>
<script type="text/javascript">
	$(function(){
		var _menu = $(".menu"),_whiteDiv = $("#whiteDiv");
		$("#jiahao_click").on("click",function(){
			if(_menu.hasClass("hide")){
				_menu.removeClass("hide");
				_whiteDiv.removeClass("hide");
			}else{
				_menu.addClass("hide");
			}
		})
		
		$(".menu li a").click(function(){
			_whiteDiv.addClass("hide");
			_menu.addClass("hide");
		})
		
		$("#whiteDiv").click(function(){
			_whiteDiv.addClass("hide");
			_menu.addClass("hide");
		})
		<%-- 该问题会出现跳转页面返回后不加载。后续继续研究 --%>
		window.addEventListener('load',load,false)
	})
	
	function load(){
		var white = document.getElementById("whiteDiv");
		white.addEventListener('touchend',touch,false)
		
		function touch(){
			$("#whiteDiv").addClass("hide");
			$(".menu").addClass("hide");
		}
	}
</script>