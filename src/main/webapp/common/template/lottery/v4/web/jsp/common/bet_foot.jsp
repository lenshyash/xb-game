<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav class="bar bar-tab">
	<p class="foot-left betBtn" id="betOrder">投注</p>
	<p class="foot-left betBtn hide" id="betCancel">取消</p>
	<input type="text" id="inputValue"/>
	<div class="foot-right" id="xiaolianBtn">
		<img class="foot-right-img"
			src="${base}/common/template/lottery/v4/web/images/img_lt_xiaolian.png"
			alt="" />
	</div>
	<p class="foot-right-btn betBtn" id="speak">发送</p>
	<div class="foot-expression hide">
		<div class="swiper-container swiper-container2">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="expression-box clear">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="1" end="27" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>=10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<div class="expression-box-box xiaolianOK">
								<img
									src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png"
									alt="${num}" />
							</div>
						</c:forEach>
						<div class="expression-box-box xiaolianDelete">
							<img
								src="${base}/common/template/lottery/v4/web/images/face/face_delete.png"
								alt="" />
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="expression-box clear">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="28" end="54" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<div class="expression-box-box xiaolianOK">
								<img
									src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png"
									alt="${num}" />
							</div>
						</c:forEach>
						<div class="expression-box-box xiaolianDelete">
							<img
								src="${base}/common/template/lottery/v4/web/images/face/face_delete.png"
								alt="" />
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="expression-box clear">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="55" end="81" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<div class="expression-box-box xiaolianOK">
								<img
									src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png"
									alt="${num}" />
							</div>
						</c:forEach>
						<div class="expression-box-box xiaolianDelete">
							<img
								src="${base}/common/template/lottery/v4/web/images/face/face_delete.png"
								alt="" />
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="expression-box clear">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="82" end="108" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<div class="expression-box-box xiaolianOK">
								<img
									src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png"
									alt="${num}" />
							</div>
						</c:forEach>
						<div class="expression-box-box xiaolianDelete">
							<img
								src="${base}/common/template/lottery/v4/web/images/face/face_delete.png"
								alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-pagination swiper-p2"></div>
		</div>
	</div>
	<jsp:include page="../common/type_${room.lotType}.jsp"></jsp:include>
</nav>