<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- popup弹窗样本 --%>
<div class="popup popup-about" id="drawPopup">
	<header class="bar bar-nav">
		<h1 class="title">设置取款密码</h1>
	</header>
	<div class="content">
		<div class="content-inner">
			<div class="content-block">
				<div class="openComplete">
					<img
						src="${base}/common/template/lottery/v4/web/images/ico-complete.png"
						style="display: inline-block;">
					<p>设置取款密码成功</p>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="${base}/m/" class="button button-big button-fill button-danger">返回首页</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>