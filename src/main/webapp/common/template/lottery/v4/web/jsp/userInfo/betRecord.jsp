<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current" id="page-infinite-scroll-bottom">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/toUser.do"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">投注记录</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="list-block"
				style="margin: 0; position: absolute; width: 100%; top: 2.2rem; z-index: 100;">
				<ul>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">开始</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${startTime}" max="${endTime}"
										style="font-size: .7rem; color: red;" name="start_time" onchange="changeData()">
								</div>
							</div>
							<div class="item-inner">
								<div class="item-title label">截止</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${endTime}" max="${endTime}"
										style="font-size: .7rem; color: red;" name="end_time" onchange="changeData()">
								</div>
							</div>
						</div>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">彩种</div>
								<div class="item-input" style="background: #FFF;">
									<select name="code" id="code" onchange="changeData()">
										<option value="">全部</option>
										<c:forEach items="${bl }" var="bl">
											<option value="${bl.code}">${bl.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="item-inner">
								<div class="item-title label">状态</div>
								<div class="item-input" style="background: #FFF;">
									<select name="status" id="status" onchange="changeData()">
										<option value="">全部</option>
										<option value="1">未开奖</option>
										<option value="2">已中奖</option>
										<option value="3">未中奖</option>
										<option value="4">撤单</option>
									</select>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
			<div class="content infinite-scroll infinite-scroll-bottom" data-distance="50"
				style="top: 7.5rem;background: #FFF;">
				<div class="card hide" id="countData" style="margin:.5rem;background: #ececec;">
			      <div class="card-content">
			        <div class="card-content-inner" style="display: flex;text-align: center;">
			        </div>
			      </div>
			    </div>
				<div class="list-block media-list inset">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/bet_order_detail_popup.jsp"></jsp:include>
		<script type="text/javascript">
			var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
			
			function changeData(){
				page = 1;//重新初始化页数
				$(".content").removeClass("zhanwei");
				$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
				$(".infinite-scroll .list-container li").remove();
				$("#countData").addClass('hide');
				setTimeout(function(){
					getData();
				},1000);
			}
			
			function getData(){
				start_time = $("input[name=start_time]").val();
				end_time = $("input[name=end_time]").val();
				code = $("#code").val();
				status = $("#status").val();
				
				ajaxData();
			}
			
			if(!($(".infinite-scroll-preloader").text() == "加载完毕")){
				setTimeout(function(){
					getData();
				},1000)
			}
			
			function ajaxData(){
				$.ajax({
					url:"${base}/lotteryBet/getBcLotteryOrder.do",
					data : {
						startTime : start_time+ " 00:00:00",
						endTime : end_time+ " 23:59:59",
						code : code,
						status : status,
						page : page,
						rows : itemsPerLoad
					},
					success:function(data){
						var dataLen = data.page.list.length;
						if(dataLen == 0){
							$("#countData").addClass("hide");
							setTimeout(function(){
								$('.infinite-scroll-preloader').addClass("hide");
								//$('.infinite-scroll .list-container').html('<li class="notDataMsg" style="text-align: center;line-height: 10rem;font-size: 1.5rem;">暂无记录</li>');
								$(".content").addClass("zhanwei");
							},2000)
							return;
						}
						if(dataLen > 0){
							if(page<2){
								totalPageCount = data.page.totalPageCount;
								var html = '';
								var xBet = data.sumBuyMoney;
								var xWin = data.sumWinMoney; 
								var dWin = parseFloat(xWin) - parseFloat(xBet);
								html += '<div style="flex-grow: 1;"><em> 投注：</em><p style="color: red">'+toDecimal2(xBet)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 中奖：</em><p style="color: #00aa00">'+toDecimal2(xWin)+'</p></div>';
								html += '<div style="flex-grow: 1;"><em> 盈利：</em><p style="color: red">'+toDecimal2(dWin)+'</p></div>';
								$("#countData .card-content-inner").html(html);
								$("#countData").removeClass('hide');
							}
							var result = "";
							$.each(data.page.list,function(i,j){
								if(i%2==0){
									result += '<li style="background:#f9f9f9">';
								}else{
									result += '<li>';
								}
								result += '<a href="javascript:void(0);" onclick="betOrderDetail(this)" order_id="'+j.orderId+'" class="item-link item-content">';
								result += '<div class="item-inner"><div class="item-title-row">';
				                result += '<div class="item-title"><em>期号：'+j.qiHao+'</em></div>';
				                result += '<div class="item-after">投注金额：<em style="color:red">'+j.buyMoney+'</em>元宝</div></div>';
				                result += '<div class="item-subtitle"><em>['+j.lotName+']</em>&nbsp;投注内容：'+j.haoMa+'</div>';
				                result += '<div class="item-text" style="height:auto;">时间：'+j.createTime+'<em style="display:inline-block;float:right;">状态：'+statusHtml(j.status)+'</em></div>';
								result += '</div></a></li>';
							})
							$('.infinite-scroll .list-container').append(result);
							$('.infinite-scroll-preloader').addClass("hide");
						}
					}
				})
			}
			
			
			$(document).on("infinite",'.infinite-scroll-bottom',function(){
				//如果正在加载，则退出
				$('.infinite-scroll-preloader').removeClass("hide");
				if(loading)return;
				//设置loading
				loading = true;
				//设置一秒加载过程
				setTimeout(function(){
					//重置加载flag
					loading = false;
					//添加新数据
					page ++;
					//如果加载出来没数据了则取消事件，防止不必要的加载
					if(page>totalPageCount){//当前加载页面大于总页面
						//$.detachInfiniteScroll($('.infinite-scroll'));
						//删除加载提示符
						//$(".infinite-scroll-preloader .preloader").remove();
						$(".infinite-scroll-preloader").text("加载完毕");
						return;
					}
					//更新最后的加载序号
					getData();
					//容器发生改变，如果是js滚动，需要刷新滚动
					$.refreshScroller();
				},1000)
			})
			function toDecimal2(x) {
	            var f = parseFloat(x);
	            if (isNaN(f)) {
	                return false;
	            }
	            var f = Math.floor(x*100)/100;
	            var s = f.toString();
	            var rs = s.indexOf('.');
	            if (rs < 0) {
	                rs = s.length;
	                s += '.';
	            }
	            while (s.length <= rs + 2) {
	                s += '0';
	            }
	            return s;
	        }
			
			function statusHtml(status){
				switch(status){
				case 1:
					status = '<font color="#337ab7">未开奖</font>';
					break;
				case 2:
					status = '<font color="#5cb85c">已中奖</font>';
					break;
				case 3:
					status = '<font color="red">未中奖</font>';
					break;
				case 4:
					status = '<font color="#777">已撤单</font>';
					break;
				case 5:
					status = '<font color="#5cb85c">派奖回滚</font>';
					break;
				case 6:
					status = '<font color="red">回滚异常</font>';
					break;
				case 7:
					status = '<font color="red">开奖异常</font>';
					break;
				case 8:
					status = '<font color="#5cb85c">和局</font>';
					break;
				case 10:
					status = '<font color="#5cb85c">和局中奖</font>';
					break;
			}
			return status;
			}
			
			function betOrderDetail(source){
				source = $(source);
				$.ajax({
					url:"${base}/lotteryBet/getBcLotteryOrderDetail.do",
					data:{orderId:source.attr("order_id")},
					success:function(res){
						var html = detailHtml(res);
						$("#betOrderDetailPopup .betOrderDetail").html(html);
						$.popup("#betOrderDetailPopup");
					}
				})
			}
			
			function detailHtml(res){
				var winMoney = res.winMoney,openHm=res.lotteryHaoMa;
				if(!winMoney){
					winMoney = '0.00';
				}
				if(!openHm){
					openHm = '? + ? + ? = ?';
				}else{
					var split = openHm.split(",");
					var sum = parseInt(split[0]) + parseInt(split[1]) + parseInt(split[2]);
					openHm = ''+split[0]+'+'+split[1]+'+'+split[2]+'='+sum+'';
					openHm += ''+(sum>13?'大':'小')+','+(sum%2==0?'双':'单')+'';
				}
				var html = '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">注&nbsp;&nbsp;单&nbsp;&nbsp;号</div>';
				html += '<div class="item-input">'+res.orderId+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">投注期号</div>';
				html += '<div class="item-input">'+res.qiHao+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">投注时间</div>';
				html += '<div class="item-input">'+res.createTime+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">投注金额</div>';
				html += '<div class="item-input">'+res.buyMoney+'元宝</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">投注内容</div>';
				html += '<div class="item-input">'+res.haoMa+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">开奖号码</div>';
				html += '<div class="item-input">'+openHm+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">赔&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;率</div>';
				html += '<div class="item-input">'+res.minBonusOdds+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</div>';
				html += '<div class="item-input">'+statusHtml(res.status)+'</div>';
				html += '</div></div></li>';
				html += '<li><div class="item-content"><div class="item-inner">';
				html += '<div class="item-title label">中奖金额</div>';
				html += '<div class="item-input">'+winMoney+'元宝</div>';
				html += '</div></div></li>';
				return html;
			}
			
			
			function backBetDetail(){
				$.closeModal("#betOrderDetailPopup");
			}
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>