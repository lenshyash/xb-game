<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<jsp:include page="../include/base_header.jsp"></jsp:include>
			<jsp:include page="../include/footer.jsp"></jsp:include>
			<!-- 这里是页面内容区 -->
			<div class="content">
				<div class="walletWarp my">
					<div class="wallet">
						<p>
							账户余额<a href="#" onclick="window.location.reload();"></a>
						</p>
						<div class="walletAcer">
							<em class="icoAcer">${loginMember.money}</em>
						</div>
					</div>
					<c:if test="${!isGuest}">
						<div class="card marginBottom">
							<div class="card-content">
								<div class="list-block">
									<ul>
										<li><a href="${base}/m/toQb/toRegChange.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoCz1"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">充值</div>
												</div>
										</a></li>
										<c:choose>
											<c:when test="${not empty rPwd && !rPwd}">
												<li><a href="javascript:void(0);" onclick="noRPwdBind();" class="item-link item-content">
											</c:when>
											<c:when test="${not empty cardNo && !cardNo}">
												<li><a href="javascript:void(0);" onclick="noBankCard();" class="item-link item-content">
											</c:when>
											<c:otherwise>
												<li><a href="${base}/m/toQb/toDrawCommit.do" class="item-link item-content">
											</c:otherwise>
										</c:choose>
												<div class="item-media">
													<em class="icon icoCz2"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">提现</div>
												</div>
										</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="card marginBottom">
							<div class="card-content">
								<div class="list-block">
									<ul>
										<c:choose>
											<c:when test="${not empty rPwd && !rPwd}">
												<li><a href="javascript:void(0);" onclick="noRPwdBind();" class="item-link item-content">
											</c:when>
											<c:otherwise>
												<li><a href="${base}/m/toQb/toBankCard.do" class="item-link item-content">
											</c:otherwise>
										</c:choose>
												<div class="item-media">
													<em class="icon icoCz3"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">银行卡</div>
												</div>
										</a></li>
										<c:if test="${onlinePay eq 'on'}">
										<li><a href="${base}/m/toQb/billsWebCZ.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoCz4"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">充值记录</div>
												</div>
										</a></li>
										</c:if>
										<li><a href="${base}/m/toQb/billsWebQK.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoAgent"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">提现记录</div>
												</div>
										</a></li>
										<c:if test="${changeMoney eq 'on'}">
										<li><a href="${base}/m/toQb/billsWebZB.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoShare"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">帐变记录</div>
												</div>
										</a></li>
										</c:if>
									</ul>
								</div>
							</div>
						</div>
						<div class="card marginBottom">
							<div class="card-content">
								<div class="list-block">
									<ul>
										<li><a href="${base}/m/toQb/setup.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoCz5"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">支付安全</div>
												</div>
										</a></li>
									</ul>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${isGuest}">
						<div class="card marginBottom">
							<div class="card-content">
								<div class="list-block">
									<ul>
										<li><a href="${base}/m/toQb/billsWeb.do"
											class="item-link item-content">
												<div class="item-media">
													<em class="icon icoCz4"></em>
												</div>
												<div class="item-inner">
													<div class="item-title">交易记录</div>
												</div>
										</a></li>
									</ul>
								</div>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>
		<jsp:include page="../include/popup.jsp"></jsp:include>
		<script type="text/javascript">
			var pwd = 0,card = 0;
			<c:choose>
				<c:when test="${not empty rPwd && !rPwd}">
					pwd = 1;
				</c:when>
				<c:otherwise>
					<c:if test="${not empty cardNo && !cardNo}">
						card = 1;
					</c:if>
				</c:otherwise>
			</c:choose>
			$(function(){
				if(pwd == 1){
					layerPwd();
					return;
				}
				if(card == 1){
					layerCard();
					return;
				}
			})
			
			function noBankCard() {
				layerCard();
			}
			function noRPwdBind() {
				layerPwd();
			}
			
			function layerPwd(){
				layer.open({
					anim: 'up',
					title:['温馨提示','background-color:#dc5d55; color:#fff;font-size:.8rem'],
					content: '您尚未设置取款密码',
				    btn: ['<a href="${base}/m/toQb/setup.do">去设置</a>','取消'],
				})
			}
			
			function layerCard(){
				layer.open({
					anim: 'up',
					title:['温馨提示','background-color:#dc5d55;color:#FFF;font-size:.8rem'],
					content : '您尚未绑定任何银行卡',
					btn : ['<a href="${base}/m/toQb/toBankCard.do?type=2">去绑定</a>','取消'],
				})
			}
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>