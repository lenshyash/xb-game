<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">个人信息</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toUser/info.do"
									class="item-link item-content">
										<div class="item-media">
											<em style="top: 1.3rem;" class="icon icoGirl"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">头像</div>
											<i><img src=""></i>
										</div>
								</a></li>
								<li><a href="#" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoNickname"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">昵称</div>
											<span>${loginMember.account}</span>
										</div>
								</a></li>
								<li><a href="#" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoChineseName"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">持卡人中文名</div>
											<span>${userInfo1['userName']}</span>
										</div>
								</a></li>
								<li><a href="#" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoEmail"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">电子邮箱</div>
											<span>${userInfo1['email']}</span>
										</div>
								</a></li>
								<li><a href="#" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoPhone"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">手机号</div>
											<span>${userInfo1['phone']}</span>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(function() {
				var nickUrl = $.fn.cookie("account_${loginMember.account}_touxiang");
				if (!nickUrl) {
					nickUrl = 10;
				}
				$(".item-inner i img").attr(
						"src",
						'${base}/common/template/lottery/v4/mobile/images/touxiang/'
								+ nickUrl + '.jpg');
			})
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>