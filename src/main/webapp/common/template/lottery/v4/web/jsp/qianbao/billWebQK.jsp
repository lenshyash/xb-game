<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page" id="page-infinite-scroll-bottom">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">提现记录</h1>
			</header>
			<div class="list-block"
				style="margin: 0; position: absolute; width: 100%; top: 2.2rem; z-index: 100;">
				<ul>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">开始</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${startTime }" max="${endTime }" name="start_time"
										style="font-size: .7rem; color: red;" onchange="changeData();">
								</div>
							</div>
							<div class="item-inner">
								<div class="item-title label">截止</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${endTime }" max="${endTime }" name="end_time"
										style="font-size: .7rem; color: red;" onchange="changeData();">
								</div>
							</div>
						</div>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">状态</div>
								<div class="item-input" style="background: #FFF;">
									<select id="drawStatus" onchange="changeData();">
										<option value="">全部</option>
										<option value="1">处理中</option>
										<option value="2">处理成功</option>
										<option value="3">处理失败</option>
									</select>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
			<div class="content infinite-scroll infinite-scroll-bottom" data-distance="50"
				style="top: 7.5rem;background: #FFF;">
				<div class="card hide" id="countData" style="margin:.5rem;background: #ececec;">
			      <div class="card-content">
			        <div class="card-content-inner" style="display: flex;text-align: center;">
			        </div>
			      </div>
			    </div>
				<div class="list-block media-list inset">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
				var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
				
				function changeData(){
					page = 1;//重新初始化页数
					$(".content").removeClass("zhanwei");
					$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
					$(".infinite-scroll .list-container li").remove();
					$("#countData").addClass('hide');
					setTimeout(function(){
						getData();
					},1000);
				}
				
				function getData(){
					start_time = $("input[name=start_time]").val();
					end_time = $("input[name=end_time]").val();
					status = $("#drawStatus").val();
					
					ajaxData();
				}
				
				if(!($(".infinite-scroll-preloader").text() == "加载完毕")){
					setTimeout(function(){
						getData();
					},1000)
				}
				
				function ajaxData(){
					$.ajax({
						url:"${base}/center/record/hisrecord/drawrd.do",
						dataType:'json',
						type:'post',
						data : {
							startTime : start_time+ " 00:00:00",
							endTime : end_time+ " 23:59:59",
							status : status,
							page : page,
							rows : itemsPerLoad
						},
						success:function(data){
							var dataLen = data.list.length;
							if(dataLen == 0){
								$("#countData").addClass("hide");
								setTimeout(function(){
									$('.infinite-scroll-preloader').addClass("hide");
									$(".content").addClass("zhanwei");
								},2000)
								return;
							}
							if(dataLen > 0){
								if(page<2){
									totalPageCount = data.totalPageCount;
									var html = '';
									var drawMoney = data.aggsData.drawMoney;
									html += '<div style="flex-grow: 1;"><em> 总取款：</em><p style="color: #00aa00">'+toDecimal2(drawMoney)+'</p></div>';
									$("#countData .card-content-inner").html(html);
									$("#countData").removeClass('hide');
								}
								var result = "";
								$.each(data.list,function(i,j){
									if(i%2==0){
										result += '<li style="background:#f9f9f9">';
									}else{
										result += '<li>';
									}
									result += '<a href="javascript:void(0);" class="item-link item-content">';
									result += '<div class="item-inner"><div class="item-title-row">';
					                result += '<div class="item-title"><em>取款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</em></div>';
					                result += '<div class="item-after">状态：'+statusResult(j.lockFlag,j.status)+'</div></div>';
					                result += '<div class="item-subtitle">取款金额：<em style="color:red">'+toDecimal2(j.drawMoney)+'</em>元宝</div>';
					                result += '<div class="item-text" style="height:auto;">时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
									result += '</div></a></li>';
								})
								$('.infinite-scroll .list-container').append(result);
								$('.infinite-scroll-preloader').addClass("hide");
							}
						}
					})
				}
				
				
				$(document).on("infinite",'.infinite-scroll-bottom',function(){
					//如果正在加载，则退出
					$('.infinite-scroll-preloader').removeClass("hide");
					if(loading)return;
					//设置loading
					loading = true;
					//设置一秒加载过程
					setTimeout(function(){
						//重置加载flag
						loading = false;
						//添加新数据
						page ++;
						//如果加载出来没数据了则取消事件，防止不必要的加载
						if(page>totalPageCount){//当前加载页面大于总页面
							//$.detachInfiniteScroll($('.infinite-scroll'));
							//删除加载提示符
							//$(".infinite-scroll-preloader .preloader").remove();
							$(".infinite-scroll-preloader").text("加载完毕");
							return;
						}
						//更新最后的加载序号
						getData();
						//容器发生改变，如果是js滚动，需要刷新滚动
						$.refreshScroller();
					},1000)
				})
				function toDecimal2(x) {
		            var f = parseFloat(x);
		            if (isNaN(f)) {
		                return false;
		            }
		            var f = Math.floor(x*100)/100;
		            var s = f.toString();
		            var rs = s.indexOf('.');
		            if (rs < 0) {
		                rs = s.length;
		                s += '.';
		            }
		            while (s.length <= rs + 2) {
		                s += '0';
		            }
		            return s;
		        }
				
				function statusResult(lock,status){
					if(lock == 1){
						return '<em style="color:#0894ec">待处理</em>';
					}
					switch(status){
						case 1:
							return '<em style="color:#1a2f3c">处理中</em>';
						case 2:
							return '<em style="color:#f6383a">处理成功</em>';
						case 3:
							return '<em style="color:#4cd964">处理失败</em>';
						default:
							return '<em style="color:#f6383a">未知错误</em>';
					}
				}
				
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>