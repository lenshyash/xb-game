<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<jsp:include page="../include/base_header.jsp"></jsp:include>
			<jsp:include page="../include/footer.jsp"></jsp:include>
			<!-- 这里是页面内容区 -->
			<div class="content">
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toUser/info.do"
									class="item-link item-content">
										<div class="item-media">
											<em style="top: 1.3rem;" class="icon icoGirl"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">头像</div>
											<i><img src=""></i>
										</div>
								</a></li>
								<li><a href="#" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoNickname"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">昵称</div>
											<span>${loginMember.account}</span>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toQb.do"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon icoWallet"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">钱包</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toUser/betRecord.do"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon icoRecord"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">投注记录</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="javascript:void(0);" id="bqingIcon"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon icoBiaoqing"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">表情管理</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toUser/gywm.do" class="item-link item-content">
										<div class="item-media">
											<em class="icon icoTeam"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">关于我们</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toUser/toSetUp.do"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon icoSetUp"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">设置</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(function() {
				$("#bqingIcon").click(function() {
					$.toast("敬请期待");
				})

				var nickUrl = $.fn
						.cookie("account_${loginMember.account}_touxiang");
				if (!nickUrl) {
					nickUrl = 10;
				}
				$(".item-inner i img").attr(
						"src",
						'${base}/common/template/lottery/v4/mobile/images/touxiang/'
								+ nickUrl + '.jpg');
			})
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>