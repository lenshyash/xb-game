<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header class="bar bar-nav">
	<a class="button button-link button-nav pull-left back" href="${base}/m/">
		<span class="icon icon-left"></span> 返回
	</a> <a class="button button-link button-nav pull-right"><span
		class="icon icon-add"></span> </a> <a
		class="button button-link button-nav pull-right"><span
		class="icon icon-kefu"></span> </a>
	<h1 class="title">${_title}</h1>
</header>