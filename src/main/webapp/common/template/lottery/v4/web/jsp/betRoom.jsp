<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group bet-room-body">
		<div class="page">
			<style type="text/css">
				.roomNameAnimate{
	    		line-height: 50px;
	    		-webkit-transform: translate3d(0,0,0);
	    		transform: translate3d(0,0,0);
	    		-webkit-transition: all .6s ease;
	    		transition: all .6s ease;
	    		/*animation:keytranName 2s infinite;
				-webkit-animation:keytranName 2s infinite; Safari and Chrome
				animation-direction:alternate;*/
	    	}
	    	@-webkit-keyframes keytranName{
	    		0%{transform:translate3d(0,0,0)}
	    		100%{transform:translate3d(0,-2.2rem,0)}
	    	}
	    	@keyframes keytranName{
	    		0%{transform:translate3d(0,0,0)}
	    		100%{transform:translate3d(0,-2.2rem,0)}
	    	}
			</style>
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back" href="#">
					<span class="icon icon-left"></span> 返回
				</a> <a class="button button-link button-nav pull-right" id="jiahao_click"><span
					class="icon icon-add"></span> </a> <a
					class="button button-link button-nav pull-right"><span
					class="icon icon-kefu"></span> </a>
				<div class="roomNameAnimate">
					<h1 class="title">${bl.name}</h1>
					<h1 class="title" style="top:2.2rem;">${room.name}</h1>
				</div>
			</header>
			<div class="menu hide" style="width: 6rem;<c:if test="${!isMobile}">margin-left:14rem;right:initial;</c:if>">
				<ul>
					<c:if test="${isLogin}">
						<li><a href="${base}/m/toUser/betRecord.do" external><em class="icon icoRecord"></em>投注记录</a></li>
						<li><a href="${base}/m/openAward.do" external><em class="icon icoResult"></em>开奖结果</a></li>
						<li><a href="${base}/m/gameRule.do" external><em class="icon icoIntroduce"></em>玩法介绍</a></li>
						<li class="noneBorder"><a href="${base}/m/openAward.do?type=trend" external><em class="icon icoTrend"></em>开奖走势</a></li>
					</c:if>
					<c:if test="${!isLogin}">
						<li><a href="${base}/toLogin.do" external><em class="icon icoRecord"></em>投注记录</a></li>
						<li><a href="${base}/toLogin.do" external><em class="icon icoResult"></em>开奖结果</a></li>
						<li><a href="${base}/toLogin.do" external><em class="icon icoIntroduce"></em>玩法介绍</a></li>
						<li class="noneBorder"><a href="${base}/toLogin.do" external><em class="icon icoTrend"></em>开奖走势</a></li>
					</c:if>
				</ul>
			</div>
			<jsp:include page="common/bet_foot.jsp"></jsp:include>
			<div class="content" id="content">
				<input type="hidden"
					value="${empty room.speakMoney?'0':room.speakMoney}"
					id="speakMoney"> <input type="hidden"
					value="${room.betStatus}" id="betStatus"> <input
					type="hidden" value="${room.robotStatus}" id="robotStatus">
				<input type="hidden" value="${isGuest?'1':'2'}" id="isGuest">
				<div class="card card-top">
					<div class="card-content">
						<div class="card-content-inner">
							<ul class="bet-room-down">
								<li class="roomLi">
									<p>
										距离<font id="issue">加载中...</font>期<font id="kp_jz">截止</font>
									</p>
									<p>
										<span class="icoTimeClose" style="display: none;">已封盘</span> <span
											class="icoTime">加载中...</span>
									</p>
								</li>
								<li>
									<p>余额</p>
									<p>
										<span class="icoAcer">加载中...</span>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-footer">
						<span></span><em></em>
					</div>
					<div class="card-header hide">
						<span><em>本期已下<font class="betCount">加载中...</font>注
						</em></span> <span id="quxiaoXz">查看注单详情<em class="jt"></em></span>
					</div>
				</div>
			</div>
		</div>
		<div class="hide" id="whiteDiv" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0); z-index: 99;"></div>
		<div class="popupAlert hide">
			<div class="popup_header">
				<img style="position: absolute; top: -2.9rem;width:100%;right:0;" src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_1.png">
			</div>
			<div class="popup_footer">
				<img style="position: absolute;" src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_2.png">
			</div>
		</div>
		<jsp:include page="common/art_template_room.jsp"></jsp:include>
		<jsp:include page="popup/open_history_popup.jsp"></jsp:include>
		<jsp:include page="popup/bet_cur_order_popup.jsp"></jsp:include>
		<script type="text/javascript"
			src="${base}/common/template/lottery/v4/web/js/jquery.ajax.js"></script>
		<script type="text/javascript"
			src="${base}/common/template/lottery/v4/web/js/bet_room.js?v=1.2.2"></script>
		<link rel="stylesheet" type="text/css"
			href="${base}/common/template/lottery/v4/mobile/css/swiper-3.4.1.min.css" />
		<script
			src="${base}/common/template/lottery/v4/mobile/js/swiper-3.4.1.jquery.min.js"></script>
		<script type="text/javascript">
			var count = 0;
			var animateName = true;
			var hostUrl = window.location.host;
			var Room = {
				lotCode : '${bl.code}',
				lotType : '${bl.type}',
				roomId : '${room.id}',
				account : '${loginMember.account}',
				levelImg : '${levelImg}',
			}
			$(function() {
				_BetRoom.initBetRoom();
				window.setTimeout(function() {
					$("body").css({
						'overflow' : 'initial',
						'position' : 'initial'

					});
				}, 1000);
				if (!$.fn.cookie("betRoomOneLogin")) {
					$(".popupAlert").removeClass("hide");
					$.fn.cookie("betRoomOneLogin", true, 1);
				}
				
				$('.popupAlert').click(function(){
					$('.popupAlert').addClass("hide");
				})
				
				var _menu = $(".menu"),_whiteDiv = $("#whiteDiv");
				$("#jiahao_click").on("click",function(){
					if(_menu.hasClass("hide")){
						_menu.removeClass("hide");
						_whiteDiv.removeClass("hide");
					}else{
						_menu.addClass("hide");
					}
				})
				
				$(".menu li a").click(function(){
					_whiteDiv.addClass("hide");
					_menu.addClass("hide");
				})
				
				$("#whiteDiv").click(function(){
					_whiteDiv.addClass("hide");
					_menu.addClass("hide");
				})
				<%-- 该问题会出现跳转页面返回后不加载。后续继续研究 --%>
				window.addEventListener('load',load,false)
				
				
				window.setInterval(function(){
					if(animateName){
						$('.roomNameAnimate').css('transform', 'translate3d(0,-2.2rem,0)');
						animateName = false;
					}else{
						$('.roomNameAnimate').css('transform', 'translate3d(0,0,0)');
						animateName = true;
					}
				},2000);
			})
			
			function load(){
				var white = document.getElementById("whiteDiv");
				white.addEventListener('touchend',touch,false)
				
				function touch(){
					$("#whiteDiv").addClass("hide");
					$(".menu").addClass("hide");
				}
			}
		</script>
		<style type="text/css">
/*其他颜色需更改css样式 007aff */
.swiper-button-next {
	width: 1.5rem !important;
	top: 1.1rem;
	right: 1rem;
	background-image: none;
	height: 1.5rem;
}

.swiper-button-prev {
	width: 1.5rem !important;
	top: 1.1rem;
	left: 1rem;
	background-image: none;
	height: 1.5rem;
}

.swiper-pagination.swiper-pagination-fraction {
	top: 10px !important;
	bottom: initial; ! important;
	display: none;
}
</style>
	</div>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>