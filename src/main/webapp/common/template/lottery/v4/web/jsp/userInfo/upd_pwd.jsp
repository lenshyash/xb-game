<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">修改登录密码</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="list-block">
					<ul>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon1 icoModifyPas"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">旧密码</div>
									<div class="item-input">
										<input type="password" name="old_psd" placeholder="请输入旧密码" minlength="6" maxlength="20">
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="list-block">
					<ul>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon1 icoModifyPas"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">新密码</div>
									<div class="item-input">
										<input type="password" name="new_psd" placeholder="请输入新密码" minlength="6" maxlength="20">
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="item-content">
								<div class="item-media">
									<em class="icon1 icoModifyPas"></em>
								</div>
								<div class="item-inner">
									<div class="item-title label">确认密码</div>
									<div class="item-input">
										<input type="password" name="new_psd2" placeholder="请再次输入新密码" minlength="6" maxlength="20">
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="content-block">
					<div class="row">
						<div class="col-100">
							<a href="javascript:void(0);" id="btn"
								class="button button-big button-fill button-danger">提交</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/login_pwd_popup.jsp"></jsp:include>
		<script type="text/javascript">
		$(function() {
			$("#btn").click(function() {
				var old_psd = $("input[name=old_psd]").val();
				var new_psd = $("input[name=new_psd]").val();
				var new_psd2 = $("input[name=new_psd2]").val();
				//var reg = /^\d{6}$/;
				if (!old_psd) {
					$.toast("请输入您的原始密码");
					return false;
				}
				if(!new_psd){
					$.toast("请输入新密码");
					return false;
				}
				if(!new_psd2){
					$.toast("请再次输入新密码");
					return false;
				}
				if (new_psd != new_psd2) {
					$.toast("您输入的两次密码不一致");
					return false;
				}
				$.ajax({
					url : "${base}/center/member/meminfo/newpwd.do",
					type : "post",
					data : {
						opwd : old_psd,
						pwd : new_psd,
						rpwd : new_psd2,
						updType : 1
					},
					dataType : "json",
					success : function(data) {
						if (data.success) {
							$.popup("#loginPopup");	
						} else {
							$.toast(data.msg);
						}
					},
					error : function(error) {
						$.toast("服务器错误了");
					}
				});
			});
		});
	</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>