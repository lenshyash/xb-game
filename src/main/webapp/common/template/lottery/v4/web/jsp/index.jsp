<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page">
			<jsp:include page="include/base_header.jsp"></jsp:include>
			<jsp:include page="include/footer.jsp"></jsp:include>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<input type="hidden" value="${loginMember.money}" id="userNowMoney" />
				<c:if test="${not empty htmlGG}">
					<jsp:include page="common/index_gg.jsp"></jsp:include>
				</c:if>
				<c:if test="${not empty lunbo && lunbo.size() >0}">
					<jsp:include page="common/lunbo.jsp"></jsp:include>
				</c:if>
				<div class="index_shuom">
					<div class="index_shuom_left">
						<ul>
							<li><i></i><span>用户已赚&nbsp;&nbsp;&nbsp;${yonghuyizhuan}元宝</span></li>
							<li><i></i><span>注册人数&nbsp;&nbsp;&nbsp;${zhucerenshu}人</span></li>
						</ul>
					</div>
					<div class="index_shuom_right">
						<span>98</span>%<font>赚钱率</font>
					</div>
				</div>
				<div class="shouye-fangjian clear roomList">
					<c:forEach items="${bcList}" var="bc" varStatus="bIndex">
						<c:if test="${isLogin}">
							<div>
								<a class="open-indicator" href="${base}/m/room.do?code=${bc.code}" ><img
									src="${base}/common/template/lottery/v4/web/images/index/${bc.code}.png">
								</a> <a class="wanfashuoming" href="javascript:void(0);"><img alt="${bc.name}" code="${bc.code}"
									src="${base}/common/template/lottery/v4/web/images/index/img_28_anniu.png"></a>
							</div>
						</c:if>
						<c:if test="${!isLogin}">
							<div>
								<a href="${base}/toLogin.do" ><img
									src="${base}/common/template/lottery/v4/web/images/index/${bc.code}.png">
								</a> <a class="wanfashuoming" href="javascript:void(0);"><img alt="${bc.name}" code="${bc.code}"
									src="${base}/common/template/lottery/v4/web/images/index/img_28_anniu.png"></a>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="popupAlert hide">
			<div class="popup_header"><img src="${base}/common/template/lottery/v4/web/images/ico_kefu.png"></div>
			<div class="popup_top_index"><img  src="${base}/common/template/lottery/v4/web/images/tishi/index_ts_1.png"> </div>
			<div class="popup_footer"><img c src="${base}/common/template/lottery/v4/web/images/tishi/index_ts_2.png"> </div>
		</div>
		<jsp:include page="popup/wanfa_popup.jsp"></jsp:include>
		<script type="text/javascript">
			$(function(){
				$(".shouye-fangjian a.wanfashuoming img").click(function(){
					var t = $(this),code=t.attr("code"),name=t.attr("alt");
					$.get("${base}/member/${folder}/rule/rule_code_"+code+".html?v=1.21",function(res){
						$("#wanfaPopup h1.title").text(name);
						$("#wanfaPopup .content-block").html(res);
						$.popup("#wanfaPopup");
					},"html");
				})
				
				if(!$.fn.cookie("isThisOneLogin")){
					$(".popupAlert").removeClass("hide");
					$.fn.cookie("isThisOneLogin",true,7);
				}
				
				$('.popupAlert').click(function(){
					$('.popupAlert').addClass("hide");
				})
			})
			
			function wfsm(code){
				if(!code){return;}
				$.get("${base}/member/${folder}/rule/rule_code_"+code+".html?v=1.21",function(res){
					$("#wanfaPopup h1.title").text(resultName(code));
					$("#wanfaPopup .content-block").html(res);
					$.popup("#wanfaPopup");
				},"html");
			}
			
			function resultName(code){
				switch(code){
					case "PCEGG":
						return "PC蛋蛋";
					case "JND28":
						return "加拿大28";
				}
			}
		</script>
	</div>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>