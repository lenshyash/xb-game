var PayChange = {};

PayChange.online = function(){
	return{
		onlineSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),
			_it = paySetting.attr("pay_type"), payId = paySetting.val(),$input=$("#"+_it).find("input[type=number]"),
	        amount1 =$input.val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	$it.removeAttr("disabled");
	        	$.toast("请选择支付方式");return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	$it.removeAttr("disabled");
	        	$.toast("请输入正确金额");
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	$it.removeAttr("disabled");
	        	$.toast("充值金额必须不小于"+min);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	$it.removeAttr("disabled");
	        	$.toast("充值金额必须不大于"+max);
	            return false;
	        }
	        var iconCss = paySetting.attr("iconcss");
	        var payType = paySetting.attr("pays_type");
	        PayChange.online.dptcommit(amount1,payId,iconCss,payType);
	        return false;
		},
		dptcommit:function(m,payId,iconCss,payType){
			topay(m, payId, iconCss, payType, function(result){
				if(result.success == false){
					$.toast(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toOnlinePayTemplate', result.data);
						$("#templateHtml").html(html);
						$.popup("#czPopup");
					}else{
						$.toast("系统发生错误");
					}
				}
			});
		}
	}
}();

PayChange.fast = function(){
	return {
		fastSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),_it = paySetting.attr("pay_type")
	        payId = paySetting.val(),$input=$("#"+_it).find("input[type=number]"),
	        amount1 =$input.val(),
	        depositor = $("#"+_it).find("#payAccountName").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	 $it.removeAttr("disabled");
	        	$.toast("请选择支付方式");return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	$.toast("请输入正确金额");
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	$.toast("充值金额必须不小于" + min);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	$.toast("充值金额必须不小于"+max);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(!depositor){
	        	var name = $("#flabel").html();
	        	$.toast("请输入"+name);
	        	$it.removeAttr("disabled");
	        	return false;
	        }
	        PayChange.fast.dptcommit(amount1,depositor,payId);
	        return false;
		},
		dptcommit:function(m,ur,payId){
			$.ajax({
				url : Mobile.base+"/m/ajax/dptcommitb.do",
				data : {
					money : m,
					depositor : ur,
					payId : payId
				},
				success : function(result) {
					
					layer.open({
					    content: '充值申请已提交，请尽快完成充值',
					    btn: '我知道了',
					    anim:'up',
					    yes:function(index){
					    	var fast = result.fast;
							fast.money = m;
							fast.orderNo = result.orderNo;
							fast.payName = result.payName;
							var html = template('toFastPayTemplate',fast);
							$("#templateHtml").html(html);
							$.popup("#czPopup");
							layer.close(index);
					    }
					  });
				}
			});
		}
	}
}();


PayChange.bank = function(){
	return {
		bankSub:function(source){
			var $from = $(".content .radioClass"),$it=$(source),bank = $from.find("input[name=radio]:checked"),_it = bank.attr("pay_type")
	         bankId =bank.val(),
	         $input=$("#"+_it).find("input[type=number]"),
	         amount1 =$input.val(),
	         depositor = $("#"+_it).find("#bankAccountName").val(),
	         min = bank.attr("min");
			 max = bank.attr("max");
	    	 $it.attr("disabled","disabled");
	    	 if(!bankId){
	    		 $it.removeAttr("disabled");
	    		 $.toast("请选择支付银行");return false;
	         }
	         try{
	             min = parseFloat(min,10);
	         }catch(e){min = 0;}
	         try{
	             max = parseFloat(max,10);
	         }catch(e){max = 0;}
	         if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	 $.toast("请输入正确金额");
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         amount1 = parseFloat(amount1,10);
	         if(min>0 && amount1<min){
	        	 $.toast("充值金额必须不小于" + min);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(max>0 && amount1>max){
	        	 $.toast("充值金额必须不大于"+max);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(!depositor) {
	        	var name = $("#flabel").html();
	        	$.toast("请输入"+name);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        PayChange.bank.dptcommit(amount1,depositor,bankId);
	        return false;
		},
		dptcommit:function(m,ur,bankId){
			$.ajax({
				url : Mobile.base+"/m/ajax/dptcommitc.do",
				data : {
					money : m,
					depositor : ur,
					bankId : bankId
				},
				success : function(result) {
					layer.open({
					    content: '充值申请已提交，线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！',
					    btn: '我知道了',
					    anim:'up',
					    yes:function(index){
					    	var bank = result.bank;
					    	bank.money = m;
					    	bank.orderNo = result.orderNo;
					    	bank.payName = result.payName;
							var html = template('toBankPayTemplate',bank);
							$("#templateHtml").html(html);
							$.popup("#czPopup");
							layer.close(index);
					    }
					  });
				}
			});
		}
	}
}();