<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">${name}</h1>
			</header>
			<%-- 这里是页面内容区  external--%>
			<div class="content">
				<c:forEach items="${roomList}" varStatus="rIndex" var="room">
					<div class="box-box" r_id="${room.id}" lot_type="${room.lotType}" max_m="${empty room.maxMoney?'0':room.maxMoney}" level="${empty room.levelId?'0':room.levelId}" status="${not empty room.argot?'1':'0'}"><%-- external   ${base}/m/toBetRoom.do?rId=${room.id}&lotType=${room.lotType}--%>
					<a class="back-btn" href="javascript:void(0);"><img class="cover" src="${base}${room.roomImg}"></a>
					<div class="fixed-seo1">
						<div class="seo-left">
							<span class="name">${room.name}</span> <span class="desc">${room.roomHouse}</span>
						</div>
						<div class="seo-right">
							<img src="${base}/common/template/lottery/v4/web/images/img_fj_ren.png" alt=""> <span
								class="current_users">${not empty room.argot?'保密':room.initialCount}人</span>
						</div>
					</div>
					<a class="seo-btn" href="javascript:void(0);"><img
						src="${base}/common/template/lottery/v4/web/images/img_peilvshuoming.png" alt=""></a>
					<div class="peilvshuoming hide">${room.brief}</div>
				</div>
				</c:forEach>
			</div>
		</div>
		<jsp:include page="popup/wanfa_popup.jsp"></jsp:include>
		<script type="text/javascript">
			var userMoney = '${loginMember.money}';
			var status = getUrlParam('status');
			$(function(){
				//return result
				if(status && status == 1){
					layerToast("房间口令不能为空");
				}else if(status == 2){
					layerToast("房间口令错误");
				}else if(status == 3){
					var message = getUrlParam('message');
					layerToast("该房间元宝限额"+message+"以上");
				}else if(status == 4){
					layerToast("您所拥有的会员等级不符合房间所规定的等级");
				}
				//click事件
				$(".content").on("click","a.back-btn",function(){
					var t = $(this),_box = t.parent(),maxM=_box.attr("max_m"),level=_box.attr("level"),status=_box.attr("status");
					if(parseFloat(maxM) > parseFloat(userMoney)){
						$.toast("该房间元宝限额"+maxM+"以上");
						return;
					}
					if(status == 1){
						layer.open({
							 title: [
							         '请输入房间暗号',
							         'background-color:#dc5d55; color:#fff;font-size:.9rem;'
								  ],
								  anim: 'up',
								  content: '<div class="moneyInput">房间暗号：<input type="text" id="inputNumber" style="width:50%;"></div>',
								  btn: ['确认', '取消'],
								  yes :function(index){
									  var argot = $("#inputNumber").val();
									  if(!argot){
										  $.toast("暗号不能为空");
										  return ;
									  }
									  var data = {
												argot:argot,
												roomId:_box.attr("r_id")
										}	
										$.ajax({
											url:'${base}/m/ajax/roomArgot.do',
											dataType:'json',
											type:'post',
											data:data,
											success:function(res){
												if(res.success){
													//$.router.load('${base}/m/toBetRoom.do?rId='+_box.attr("r_id")+'&lotType='+_box.attr("lot_type")+'&argot='+argot+'',true);
													window.location.href = "${base}/m/toBetRoom.do?rId="+_box.attr("r_id")+"&lotType="+_box.attr("lot_type")+"&argot="+argot;
													layer.close(index);
												}else{
													$.toast(res.msg);
												}
											}
										})
								  }
						})
						return ;
					}
					//$.router.load('${base}/m/toBetRoom.do?rId='+_box.attr("r_id")+'&lotType='+_box.attr("lot_type")+'',true);
					window.location.href = "${base}/m/toBetRoom.do?rId="+_box.attr("r_id")+"&lotType="+_box.attr("lot_type");
				})
			})
			
			
			function getUrlParam(name) {
	            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	            var r = window.location.search.substr(1).match(reg);  //匹配目标参数
	            if (r != null) return unescape(r[2]); return null; //返回参数值
	        }
			
			function layerToast(message){
				layer.open({
					title: [
					         '错误信息',
					         'background-color:#dc5d55; color:#fff;font-size:.9rem;'
						  ],
						  anim: 'up',
						  content : message,
						  btn : ['我知道了'],
				})
			}
			
			$(".content a.seo-btn").click(function(){
				var t = $(this),pvsm=t.siblings(".peilvshuoming").html(),name=t.siblings(".fixed-seo1").find("span.name").text();
				console.info(name);
				$("#wanfaPopup h1.title").text(name);
				$("#wanfaPopup .card-content-inner").html(pvsm);
				$.popup("#wanfaPopup");
			})
		</script>
	</div>
	<jsp:include page="include/need_js.jsp"></jsp:include>
</body>
</html>