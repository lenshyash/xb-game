<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<jsp:include page="../include/base_header.jsp"></jsp:include>
			<jsp:include page="../include/footer.jsp"></jsp:include>
			<!-- 这里是页面内容区 -->
			<div class="content">
				<c:if test="${not empty lunbo && lunbo.size()>0}">
					<jsp:include page="../common/lunbo.jsp"></jsp:include>
				</c:if>
				<div class="newsTitle">
					<ul id="news_tab">
						<li class="active" data="1">公告</li>
						<li data="2" class="">信息</li>
					</ul>
				</div>
				<div class="newsContent">
					<ul class="record_list " style="display: block;">
					</ul>
					<ul class="record_list" style="display: none;"></ul>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/wanfa_popup.jsp"></jsp:include>
		<script type="text/javascript">
			var pageSize = 100;
			var pageNo = 1;
			var status = 0;
			var pageDatas = [];
			$(function(){
				searchMsg();
				gg();
				
				
				$("#news_tab li").click(function(){
					$(this).addClass("active").siblings().removeClass("active");
					var index = $(this).index();
					$(".newsContent ul").eq(index).show().siblings("ul").hide();
				})
			})
			
			
			function gg(){
				var html = '';
				$.ajax({
					url:'${base}/getConfig/getArticle.do',
					dataType:'json',
					type:'post',
					data:{code:13},
					success:function(res){
						if(res.length==0){
							$(".newsContent ul").eq(0).addClass("zhanwei");
							return;
						}
						$(".newsContent ul").eq(0).removeClass("zhanwei");
						$.each(res,function(i,j){
							html += '<li onclick="ggClick(this);" data-id="'+j.id+'"><span>'+DateUtil.formatDate(j.updateTime)+'</span><h3>'+j.title+'</h3></li>'
						})
						$(".newsContent ul").eq(0).html(html);
					}
				})
			}
			
			
			function ggClick(source){
				source = $(source);
				$.ajax({
					url:"${base}/m/gg_detail.do",
					data:{id:source.attr("data-id")},
					success:function(res){
						if(res.success){
							$("#wanfaPopup h1.title").text('公告详情');
							$("#wanfaPopup .content-block").html(ggReadHtml(res.message));
							$.popup("#wanfaPopup");
						}else{
							$.toast(res.msg);
						}
					}
				})
			}
			
			function ggReadHtml(res){
				var html = ' <div class="card"><div class="card-header" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:center;">'+res.title+'</span></div>';
				html += ' <div class="card-content"><div class="card-content-inner">'+res.content+'</div></div><div class="card-footer" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:right;">发布时间：'+DateUtil.formatDate(res.updateTime)+'</span></div></div>';
				return html;
			}
			
			
			function searchMsg(){
				$.ajax({
					url:'${base}/center/news/message/list.do',
					dataType:'json',
					type:'post',
					data:{
						pageNumber : pageNo,
						pageSize : pageSize,
						status:status
					},
					success:function(res){
						var html ='';
						if(res.totalCount==0){
							$(".zhanwei").show();
							$(".newsContent ul").eq(1).addClass("zhanwei");
							return;
						}
						$(".newsContent ul").eq(1).removeClass("zhanwei");
						$.each(res.list,function(i,j){
							pageDatas[j.id]=j;
							var type="";
							if(j.status == 2){
								type="yidu";
							}
							html += '<li class="'+type+'" data-id="'+j.id+'" onclick="readMsg(this);"><span>'+DateUtil.formatDate(j.createDatetime)+'</span><h3>'+j.title+'</h3></li>'
						})
						$(".newsContent ul").eq(1).html(html);
					}
				})
			}
			
			function readMsg(source){	
				source = $(source);
				var row = pageDatas[source.attr("data-id")];
				$.ajax({
					url:"${base}/center/news/message/read.do",
					dataType:'json',
					type:'post',
					data:{id:source.attr("data-id")},
					success:function(res){
						if(res.success){
							if(!source.hasClass("yidu")){source.addClass("yidu");}
							$("#wanfaPopup h1.title").text('公告详情');
							$("#wanfaPopup .content-block").html(readHtml(row));
							$.popup("#wanfaPopup");
						}else{
							$.toast(res.msg);
						}
					}
				})	
			}
			
			function readHtml(res){
				var html = ' <div class="card"><div class="card-header" style="background: #f1f1f1;"><span style="display:inline-block;width:100%;text-align:center;">'+res.title+'</span></div>';
				html += ' <div class="card-content"><div class="card-content-inner">'+res.message+'</div></div></div>';
				return html;
			}
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>