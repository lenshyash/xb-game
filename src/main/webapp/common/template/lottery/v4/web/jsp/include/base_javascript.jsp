<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="${base}/common/template/lottery/v4/web/css/sui/sm.min.css">
<link rel="stylesheet" href="${base}/common/template/lottery/v4/web/css/sui/sm-extend.min.css">
<script type="text/javascript">
	Mobile = {
			base:'${base}',
			folder:'${folder}',
			isLogin:${isLogin},
	}
</script>
<%-- 重写css载入 --%>
<link rel="stylesheet" href="${base}/common/template/lottery/v4/web/css/base.css?v=1.1.2">
<script type='text/javascript' src='${base}/common/template/lottery/v4/web/js/sui/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${base}/common/template/lottery/v4/web/js/sui/sm.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${base}/common/template/lottery/v4/web/js/sui/sm-extend.min.js' charset='utf-8'></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/web/js/cookie/zepto.cookie.min.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/mobile/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/web/js/zepto/zepto.scroll.js"></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/web/js/jquery.base.js"></script>
<script type="text/javascript" src="${base}/common/js/onlinepay/pay.js?v=6.2673"></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/web/js/pay_change.js?v=2"></script>
