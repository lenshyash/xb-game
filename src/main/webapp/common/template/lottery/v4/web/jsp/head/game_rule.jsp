<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">玩法介绍</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="card">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<c:forEach items="${bcList}" var="bc" varStatus="bcIndex">
								<li code="${bc.code}"><a href="javascript:void(0);"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon ico_${bc.code}"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">${bc.name}</div>
										</div>
								</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="../popup/wanfa_popup.jsp"></jsp:include>
		<script type="text/javascript">
			$(function(){
				$(".card ul li").click(function(){
					var t = $(this),code=t.attr("code"),name=t.find(".item-title").text();
					$.get("${base}/member/${folder}/rule/rule_code_"+code+".html?v=1.1.2",function(res){
						$("#wanfaPopup h1.title").text(name);
						$("#wanfaPopup .content-block").html(res);
						$.popup("#wanfaPopup");
					},"html");
				})
			})
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>