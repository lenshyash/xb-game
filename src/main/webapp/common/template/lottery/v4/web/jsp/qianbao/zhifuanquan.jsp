<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">支付安全</h1>
			</header>
			<%-- 这里是页面内容区 --%>
			<div class="content">
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${base}/m/toQb/toUpdPwd.do"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon1 icoModifyPas"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">修改取款密码</div>
										</div>
								</a></li>
								<li><a href="javascrip:void(0);" id="wjmm"
									class="item-link item-content">
										<div class="item-media">
											<em class="icon1 icoModifyPas"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">忘记密码</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card marginBottom">
					<div class="card-content">
						<div class="list-block">
							<ul>
								<li><a href="${kfUrl}" class="item-link item-content" external>
										<div class="item-media">
											<em class="icon icoNavArt2"></em>
										</div>
										<div class="item-inner">
											<div class="item-title">联系客服</div>
										</div>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(function(){
				$("#wjmm").click(function(){
					layer.open({
						anim: 'up',
						content : '您好，找回密码请联系客服',
						btn : [ '联系客服', '取消' ],
						yes : function(index) {
							window.location.href = "${kfUrl}";
							layer.close(index);
						}
					});
				})
			})
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>