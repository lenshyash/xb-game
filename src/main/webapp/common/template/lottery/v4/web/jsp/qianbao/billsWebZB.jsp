<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page" id="page-infinite-scroll-bottom">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">帐变记录</h1>
			</header>
			<div class="list-block"
				style="margin: 0; position: absolute; width: 100%; top: 2.2rem; z-index: 100;">
				<ul>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">开始</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${startTime}" max="${endTime }" name="start_time"
										style="font-size: .7rem; color: red;" onchange="changeData();">
								</div>
							</div>
							<div class="item-inner">
								<div class="item-title label">截止</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${endTime}" max="${endTime }" name="end_time"
										style="font-size: .7rem; color: red;" onchange="changeData();">
								</div>
							</div>
						</div>
					</li>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">变化类型</div>
								<div class="item-input" style="background: #FFF;">
									<select name="status" id="moneyType">
									</select>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<%-- 这里是页面内容区 style="position: absolute;top:0;overflow: auto;height: 100%;width:100%;margin:0;padding-top:5rem;" --%>
			<div class="content infinite-scroll infinite-scroll-bottom" data-distance="50"
				style="top: 7.5rem;background: #FFF;">
				<div class="card hide" id="countData">
			      <div class="card-content">
			        <div class="card-content-inner" style="display: flex;text-align: center;">
			        </div>
			      </div>
			    </div>
				<div class="list-block media-list inset" style="margin-top:.5rem;">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
		</div>
		<script id="moneytype_tpl" type="text/html">
			<option value="">全部</option>
			{{each data as option}}
        		<option value="{{option.id}}">{{option.name}}</option>
			{{/each}}
		</script>
		<script type="text/javascript">
		var moneyType = ${moneyType};
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		//加载moneyType
		initMoneyType();
		
		function changeData(){
			page = 1;//重新初始化页数
			$(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			$("#countData").addClass('hide');
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			status = $("#moneyType").val();
			
			ajaxData();
		}
		
		if(!($(".infinite-scroll-preloader").text() == "加载完毕")){
			setTimeout(function(){
				getData();
			},1000)
		}
		
		function ajaxData(){
			$.ajax({
				url:"${base}/center/record/mnyrecord/list.do",
				dataType:'json',
				type:'post',
				data : {
					startTime : start_time+ " 00:00:00",
					endTime : end_time+ " 23:59:59",
					status : status,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result = "";
						$.each(data.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<a href="javascript:void(0);" class="item-link item-content">';
							result += '<div class="item-inner"><div class="item-title-row">';
			                result += '<div class="item-title"><em>类型：'+statusResult(2,j.type)+'</em></div></div>';
			                result += '<div class="item-subtitle">变动前金额：<em style="color:#0894ec">'+toDecimal2(j.beforeMoney)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动金额：<em style="color:red">'+toDecimal2(j.money)+'</em>元宝</div>';
			                result += '<div class="item-subtitle">变动后金额：<em style="color:#4cd964">'+toDecimal2(j.afterMoney)+'</em>元宝</div>';
			                result += '<div class="item-text" style="height:auto;">变动时间：'+DateUtil.formatDatetime(j.createDatetime)+'</div>';
							result += '</div></a></li>';
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				$.refreshScroller();
			},1000)
		})
		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function statusResult(lock,status){
			if(lock == 1){
				return '<em style="color:#0894ec">待处理</em>';
			}
			switch(status){
				case 1:
					return "人工加款";
				case 2:
					return '人工扣款';
				case 3:
					return '在线取款失败';
				case 4:
					return "在线取款";
				case 5:
					return "在线支付";
				case 6:
					return "快速入款";
				case 7:
					return "一般入款";
				case 9:
					return "反水加钱";
				case 10:
					return "反水回滚";
				case 18:
					return "活动中奖";
				case 19:
					return "现金兑换积分";
				case 20:
					return "积分兑换现金";
				case 79:
					return "注册赠送";
				case 80:
					return "存款赠送";
				case 130:
					return "彩票投注";
				case 131:
					return "彩票派奖";
				case 132:
					return "彩票撤单";
				case 133:
					return "彩票派奖回滚";
			}
		}
		
		function initMoneyType() {
			var data = [];
			for ( var key in moneyType) {
				if (10 < key && key < 15) {
					continue;
				}
				if ("off" == '${isZrOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isDzOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isCpOnOff}' && 129 < key && key < 134) {
					continue;
				}
				if ("off" == '${isLhcOnOff}' && 139 < key && key < 143) {
					continue;
				}
				if ("off" == '${isTyOnOff}' && 200 < key && key < 205) {
					continue;
				}
				if ("off" == '${isExChgOnOff}' && 18 < key && key < 21) {
					continue;
				}

				data.push({
					"id" : key,
					"name" : moneyType[key]
				})
			}
			var eachdata = {
				"data" : data
			};
			var html = template('moneytype_tpl', eachdata);
			$("#moneyType").append(html);
		}
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>