<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="card hide" id="touzhu_jianpan">
	<div class="card-header">
		<span class="leftAr"></span>
		<span class="swpier_title">大小单双</span>
		<span class="rightAr"></span>
	</div>
	<div class="swiper-container swiper-container1"><%--swiper-container swiper-container1 swiper-wrapper --%>
		<div class="swiper-wrapper">
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play1">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 1}">
											<li><div data-id="${six.id}" data-code="${six.playCode}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span>${six.name}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
						<div class="peilvshuom hide">
							<div style="display: none;">${peilvshuoming}</div>
							<button>赔率说明</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play2">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 4}">
											<li><div class="shuzi_${six.name}" data-id="${six.id}"
													data-code="${six.playCode}" data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}">
													<span>${six.name}</span>
													<p>
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="card-content swiper-slide">
				<div class="card-content-inner">
					<div class="play play3">
						<c:choose>
							<c:when test="${sixList.size()>0}">
								<p class="desc">中奖和值(红)：【3,6,9,12,15,18,21,24】</p>
								<ul>
									<c:forEach items="${sixList}" var="six" varStatus="sixIndex">
										<c:if test="${six.isNowYear eq 2 || six.isNowYear eq 3}">
											<li><div data-code="${six.playCode}" data-id="${six.id}"
													data-min="${six.minBetAmmount}"
													data-max="${six.maxBetAmmount}"
													data-desc="${six.name eq '红波'?'中奖和值(红)：【3,6,9,12,15,18,21,24】':six.name eq '蓝波'?'中奖和值(蓝)：【2,5,8,11,17,20,23,26】':six.name eq '绿波'?'中奖和值(绿)：【1,4,7,10,16,19,22,25】':'豹子：三个数一致即为中奖'}">
													<span>${six.name}</span>
													<p
														class="${six.name eq '红波'?'red1':six.name eq '蓝波'?'lan':six.name eq '绿波'?'lv':'huang'}">
														<fmt:formatNumber type="number" value="${six.odds}"
															maxFractionDigits="2" />
													</p>
												</div></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								<p class="desc" style="line-height: 40px; font-size: 40px;">赔率玩法不存在</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 如果需要导航按钮 -->
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>

	<div class="swiper-pagination swiper-p1"></div>
</div>