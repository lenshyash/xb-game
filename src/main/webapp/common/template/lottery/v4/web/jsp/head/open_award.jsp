<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${_title}</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base_javascript.jsp"></jsp:include>
</head>
<body>
	<div class="page-group">
		<div class="page page-current">
			<header class="bar bar-nav">
				<a class="button button-link button-nav pull-left back"
					href="${base}/m/"> <span class="icon icon-left"></span> 返回
				</a>
				<h1 class="title">历史开奖结果</h1>
			</header>
			<div class="list-block"
				style="margin: 0; position: absolute; width: 100%; top: 2.2rem; z-index: 100;">
				<ul>
					<li style="background: #f1f1f1;">
						<div class="item-content">
							<div class="item-inner">
								<div class="item-title label">时间</div>
								<div class="item-input" style="background: #FFF;">
									<input type="date" value="${nowTime}" max="${nowTime}"
										style="font-size: .7rem; color: red;" name="start_time"
										onchange="changeData()">
								</div>
							</div>
							<div class="item-inner">
								<div class="item-title label">彩种</div>
								<div class="item-input" style="background: #FFF;">
									<select name="code" id="code" onchange="changeData()">
										<c:forEach items="${bcList}" var="bc">
											<option value="${bc.code}">${bc.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<%-- 这里是页面内容区 --%>
			<div class="content infinite-scroll infinite-scroll-bottom" data-distance="50"
				style="top: 4.6rem;background: #FFF;">
				<div class="list-block media-list inset" style="padding-top:.5rem;">
					<ul class="list-container">
					</ul>
				</div>
				<div class="infinite-scroll-preloader">
					<div class="preloader"></div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		var loading = false,itemsPerLoad = 10,page=1,totalPageCount = 0;
		
		function changeData(){
			page = 1;//重新初始化页数
			$(".content").removeClass("zhanwei");
			$('.infinite-scroll-preloader').removeClass("hide").html('<div class="preloader"></div>');
			$(".infinite-scroll .list-container li").remove();
			setTimeout(function(){
				getData();
			},1000);
		}
		
		function getData(){
			start_time = $("input[name=start_time]").val();
			code = $("#code").val();
			
			console.info(start_time);
			console.info(code);
			ajaxData();
		}
		
		if(!($(".infinite-scroll-preloader").text() == "加载完毕")){
			setTimeout(function(){
				getData();
			},1000)
		}
		
		function ajaxData(){
			$.ajax({
				url:"${base}/m/ajax/openAward.do",
				dataType:'json',
				type:'post',
				data : {
					time : start_time,
					lotCode:code,
					page : page,
					rows : itemsPerLoad
				},
				success:function(data){
					var dataLen = data.list.list.length;
					if(dataLen == 0){
						$("#countData").addClass("hide");
						setTimeout(function(){
							$('.infinite-scroll-preloader').addClass("hide");
							$(".content").addClass("zhanwei");
						},2000)
						return;
					}
					if(dataLen > 0){
						if(page<2){
							totalPageCount = data.totalPageCount;
						}
						var result = "";
						$.each(data.list.list,function(i,j){
							if(i%2==0){
								result += '<li style="background:#f9f9f9">';
							}else{
								result += '<li>';
							}
							result += '<div class="item-inner" style="padding-left: .75rem;">';
			                result += '<div class="item-title-row"><div class="item-title">第'+j.qiHao+'期</div>';
			                result += '<div class="item-after">'+DateUtil.formatDatetime(j.endTime)+'</div></div>';
			                if(!(j.haoMa)){
			                	result += '<div class="item-subtitle item-openAward" style="line-height: 2rem;"><em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>+<em class="shuzi_00">-</em>=<em class="shuzi_00">-</em>';
					            result += '<em class="colorDan" style="background:#ccc">-</em><em class="colorDan" style="background:#ccc">-</em><em class="colorDan" style="background:#ccc">-</em>';
								result += '</div></li>';
			                }else{
			                	var hm = j.haoMa.split(","),sum = totalSum(hm);
				                result += '<div class="item-subtitle item-openAward" style="line-height: 2rem;"><em class="shuzi_'+firstAddZero(hm[0])+'">'+firstAddZero(hm[0])+'</em>+<em class="shuzi_'+firstAddZero(hm[1])+'">'+firstAddZero(hm[1])+'</em>+<em class="shuzi_'+firstAddZero(hm[2])+'">'+firstAddZero(hm[2])+'</em>=<em class="shuzi_'+firstAddZero(sum)+'">'+firstAddZero(sum)+'</em>';
				                result += '<em class="colorDan" style="background:'+(sum>=14?'#2371ff':'#f30700')+'">'+(sum>=14?'大':'小')+'</em>';
								result += '<em class="colorDan" style="background:'+(sum%2==0?'#f30700':'#2371ff')+'">'+(sum%2==0?'双':'单')+'</em>';
				                var anQ='',anB=1;
								if(sum>=15&&sum%2!=0){
									if(sum>=23){
										anQ='极大';
									}else{
										anQ='大单';
										anB=2;
									}
								}else if(sum>=14&&sum%2==0){
									anQ='大双';
								}else if(sum<14&&sum%2==0){
									if(sum<=4){
										anQ='极小'
									}else{
										anQ='小双';
									}
								}else{
									anQ='小单';
									anB=2;
								}
								result += '<em class="colorDan" style="background:'+(anB==1?'#f30700':'#2371ff')+'">'+anQ+'</em>';
								result += '</div></li>';
			                }
						})
						$('.infinite-scroll .list-container').append(result);
						$('.infinite-scroll-preloader').addClass("hide");
					}
				}
			})
		}
		
		
		$(document).on("infinite",'.infinite-scroll-bottom',function(){
			//如果正在加载，则退出
			$('.infinite-scroll-preloader').removeClass("hide");
			if(loading)return;
			//设置loading
			loading = true;
			//设置一秒加载过程
			setTimeout(function(){
				//重置加载flag
				loading = false;
				//添加新数据
				page ++;
				//如果加载出来没数据了则取消事件，防止不必要的加载
				if(page>totalPageCount){//当前加载页面大于总页面
					//$.detachInfiniteScroll($('.infinite-scroll'));
					//删除加载提示符
					//$(".infinite-scroll-preloader .preloader").remove();
					$(".infinite-scroll-preloader").text("加载完毕");
					return;
				}
				//更新最后的加载序号
				getData();
				//容器发生改变，如果是js滚动，需要刷新滚动
				$.refreshScroller();
			},1000)
		})
		function firstAddZero(num){
			num = parseInt(num);
			if(num<10){
				return '0'+num;
			}
			return num
		}
		
		function totalSum(hm){
			return  parseInt(hm[0]) + parseInt(hm[1]) + parseInt(hm[2]);
		}
		
		</script>
	</div>
	<jsp:include page="../include/need_js.jsp"></jsp:include>
</body>
</html>