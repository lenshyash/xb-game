<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>${_title}_投注记录</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<link
	href="${base}/common/template/lottery/v4/mobile/js/dropload/dropload.css"
	rel="stylesheet">
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/dropload/dropload.min.js"></script>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>投注记录</h1>

	</header>
	<section>
		<div class="bettingRecord">
			<div class="bettingRecordSearch">
				<div class="teamSearchCondition clearfix"
					style="margin-bottom: 30px">
					<div class="teamSearch">
						<span>从：</span><label> <input type="text"
							class="inputDate" id="startTime" disabled="" value="${startTime}">
							<input name="start_time" type="date" onchange="getData()" max="${startTime}"></label>
					</div>
					<div class="teamSearch">
						<span>到：</span><label> <input type="text"
							class="inputDate" id="endTime" disabled="" value="${endTime}">
							<input name="end_time" type="date" onchange="getData()" max="${endTime}"></label>
					</div>
				</div>
				<div class="BRSearchCondition clearfix" style="margin-bottom: 0">
					<div>
						<span>彩种类型：</span> <label> <em class="inputW">全部</em> <select
							class="select" name="code" id="code" onchange="getData()">
								<option value="">全部</option>
								<c:forEach items="${bl }" var="bl">
									<option value="${bl.code}">${bl.name}</option>
								</c:forEach>
						</select>
						</label>
					</div>
					<div>
						<span>交易状态：</span> <label> <em class="inputW">全部</em> <select
							class="select" name="status" id="status" onchange="getData()">
								<option value="">全部</option>
								<option value="1">未开奖</option>
								<option value="2">已中奖</option>
								<option value="3">未中奖</option>
								<option value="4">撤单</option>
						</select>
						</label>
					</div>
				</div>
			</div>
			<div class="bettingRecordList">
				<ul id="record_list">
				</ul>
				<div class="dropload-down">
					<div class="dropload-noData"></div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		var start_time = $("#startTime").val();
		var end_time = $("#endTime").val();
		var code = $("#code").val();
		var status = $("#status").val();
		$("input[name=start_time]").val(start_time);
		$("input[name=end_time]").val(end_time);
		var windowHeight = $(window).height();
		$(function(){
			pageData();
		})
		function getData() {
			start_time = $("input[name=start_time]").val();
			end_time = $("input[name=end_time]").val();
			code = $("select[name=code] option:selected").val();
			status = $("select[name=status] option:selected").val();
			$("#startTime").val(start_time);
			$("#endTime").val(end_time);
			pageData();
		}

		function pageData() {
			var page = 1; //页码
			$('#record_list').html("");
			$('.dropload-down').remove();

			var record_type = '';
			// dropload
			$('.bettingRecordList')
					.dropload(
							{
								scrollArea : window,
								domDown : {
									domNoData : '<div class="dropload-noData"></div>'
								},
								loadDownFn : function(me) {
									var result = '';
									$.ajax({
												url : '${base}/lotteryBet/getBcLotteryOrder.do',
												data : {
													startTime : start_time
															+ " 00:00:00",
													endTime : end_time
															+ " 23:59:59",
													code : code,
													status : status,
													page : page,
													rows : 10
												},
												success : function(data) {
													var dataLen = data.page.list.length;
													if (page > 2) {
														$('.zhanwei')
																.removeClass(
																		'zhanwei');
													} else if (dataLen > 0) {
														$('.zhanwei')
																.removeClass(
																		'zhanwei');
													} else {
														$('.bettingRecordList')
																.addClass(
																		'zhanwei');
														var mainTop = $(
																".bettingRecordList")
																.offset().top;
														var newHeight = windowHeight
																- mainTop;
														$(".bettingRecordList")
																.css(
																		{
																			height : newHeight
																		});
													}
													if (dataLen > 0) {
														if(page<=2){
														var xBet = data.sumBuyMoney;
														var xWin = data.sumWinMoney;
														var dWin = parseFloat(xWin) - parseFloat(xBet);
														result += '<li class="title"><div><em> 投注：</em><p style="color: red">-'+toDecimal2(xBet)+'</p></div>';
														result += '<div><em> 中奖：</em><p style="color: #00aa00">+'+toDecimal2(xWin)+'</p></div>';
														result += '<div><em> 盈利：</em><p style="color: red">'+toDecimal2(dWin)+'</p></div></li>';
														//数据处理
														}
														$.each(data.page.list,function(i,j){
															result += '<li onclick="betRecordDetail(this)" orderId="'+j.orderId+'"><div><div><label>'+j.lotName+'</label><em>期号：'+j.qiHao+'</em></div>'
															result += '<p>投注内容：'+j.haoMa+'</p><p>时间：'+j.createTime+'</p>';
															result += '</div><div><div><b style="font-size:20px;">投注金额：</b><span style="color: red">- '+j.buyMoney+'</span></div>';
															result += '<p>状态：'+statusHtml(j.status)+'</p>';
															if(j.status == 2){
																result += '<div><b style="font-size:20px;">中奖金额：</b><span style="color:#5cb85c">+'+j.winMoney+'</span></div>';
															}
															result += '</div></li>';
														})
													} else {
														// 锁定
														me.lock();
														// 无数据
														me.noData();
													}
													// 为了测试，延迟1秒加载
													setTimeout(
															function() {
																$('#record_list').append(result);
																// 每次数据加载完，必须重置
																me.resetload();
															}, 1000);

												},
												error : function(xhr, type) {
													// 即使加载出错，也得重置
													me.resetload();
												}
											});

									page++; //页面加1
								},
								threshold : 50
							});
		}

		function toDecimal2(x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.floor(x*100)/100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        }
		
		function betRecordDetail(source){
			source = $(source);
			$.ajax({
				url:'${base}/lotteryBet/getBcLotteryOrderDetail.do',
				data:{orderId:source.attr("orderId")},
				success:function(res){
					var html = templateHtml(res);
					if(res!=null){
						layer.open({
						    type: 1
						    ,content: html
						    ,anim: 'up'
						    ,style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
						  });
					}
				}
			})
		}
		
		function templateHtml(res){
			var height = $(window).height()-120;
			var winMoney = res.winMoney,openHm=res.lotteryHaoMa;
			if(!winMoney){
				winMoney = '0.00';
			}
			if(!openHm){
				openHm = '? + ? + ? = ?';
			}else{
				var split = openHm.split(",");
				var sum = parseInt(split[0]) + parseInt(split[1]) + parseInt(split[2]);
				openHm = ''+split[0]+'+'+split[1]+'+'+split[2]+'='+sum+'';
				openHm += ''+(sum>13?'大':'小')+','+(sum%2==0?'双':'单')+'';
			}
			var html = '<header><em onclick="layer.closeAll();">返回</em><h1>投注详情</h1></header>';
			html += '<section style="overflow-x:auto;height:'+height+'px;"><div class="details"><p>注单号：'+res.orderId+'</p><p>投注期号：'+res.qiHao+'</p><p>投注时间：'+res.createTime+'</p>';
			html += '<p>投注金额：'+res.buyMoney+'元宝</p><p>投注内容：'+res.haoMa+'</p><p>开奖结果：'+openHm+'</p>';
			html += '<p>赔率：'+res.minBonusOdds+'</p><p>状态：'+statusHtml(res.status)+'</p><p>中奖金额：'+winMoney+'元宝</p></div></section>';
			return html;
		}
		
		
		function statusHtml(status){
			switch(status){
			case 1:
				status = '<font color="#337ab7">未开奖</font>';
				break;
			case 2:
				status = '<font color="#5cb85c">已中奖</font>';
				break;
			case 3:
				status = '<font color="red">未中奖</font>';
				break;
			case 4:
				status = '<font color="#777">已撤单</font>';
				break;
			case 5:
				status = '<font color="#5cb85c">派奖回滚</font>';
				break;
			case 6:
				status = '<font color="red">回滚异常</font>';
				break;
			case 7:
				status = '<font color="red">开奖异常</font>';
				break;
			case 8:
				status = '<font color="#5cb85c">和局</font>';
				break;
		}
			return status;
		}
	</script>
</body>
</html>