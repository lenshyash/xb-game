<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>${_title}_填写银行卡信息</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css?v=1"
	rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<style type="text/css">
.msg_error {
	background-color: #ffe7e7;
}
</style>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>填写银行卡信息</h1>

	</header>
	<section>
		<div class="modify bgFF">
			<div class="bankCard2">
				<dl>
					<dt>
						<i class="icoBank1"></i>请选择出款银行<em style="color: #ff4f4f">*</em>
					</dt>
					<dd>
						<ul class="onLinePad">
							<li><label>出款银行：
									<div>
										<div class="inputW">工商银行</div>
										<select class="select" name="bankName" id="bankName">
											<option value="建设银行">建设银行</option>
											<option value="工商银行" selected="selected">工商银行</option>
											<option value="农业银行">农业银行</option>
											<option value="中国邮政银行">中国邮政银行</option>
											<option value="中国银行">中国银行</option>
											<option value="中国招商银行">中国招商银行</option>
											<option value="中国交通银行">中国交通银行</option>
											<option value="中国民生银行">中国民生银行</option>
											<option value="中信银行">中信银行</option>
											<option value="中国兴业银行">中国兴业银行</option>
											<option value="浦发银行">浦发银行</option>
											<option value="平安银行">平安银行</option>
											<option value="华夏银行">华夏银行</option>
											<option value="广州银行">广州银行</option>
											<option value="BEA东亚银行">BEA东亚银行</option>
											<option value="广州农商银行">广州农商银行</option>
											<option value="顺德农商银行">顺德农商银行</option>
											<option value="北京银行">北京银行</option>
											<option value="杭州银行">杭州银行</option>
											<option value="温州银行">温州银行</option>
											<option value="上海农商银行">上海农商银行</option>
											<option value="中国光大银行">中国光大银行</option>
											<option value="渤海银行">渤海银行</option>
											<option value="浙商银行">浙商银行</option>
											<option value="晋商银行">晋商银行</option>
											<option value="汉口银行">汉口银行</option>
											<option value="上海银行">上海银行</option>
											<option value="广发银行">广发银行</option>
											<option value="深圳发展银行">深圳发展银行</option>
											<option value="东莞银行">东莞银行</option>
											<option value="宁波银行">宁波银行</option>
											<option value="南京银行">南京银行</option>
											<option value="北京农商银行">北京农商银行</option>
											<option value="重庆银行">重庆银行</option>
											<option value="广西农村信用社">广西农村信用社</option>
											<option value="吉林银行">吉林银行</option>
											<option value="江苏银行">江苏银行</option>
											<option value="成都银行">成都银行</option>
											<option value="尧都区农村信用联社">尧都区农村信用联社</option>
											<option value="浙江稠州商业银行">浙江稠州商业银行</option>
											<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
										</select>
									</div> <span class="s_msg Validform_checktip"></span>
							</label></li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt>
						<i class="icoBank1"></i>请填写支行信息<em style="color: #ff4f4f">*</em>
					</dt>
					<dd>
						<ul class="onLinePad">

							<li><label class="area">省份： <input name="branch"
									id="province" style="width: 350px;" type="text"
									placeholder="请输入省份">
							</label></li>
							<li><label class="area">城市： <input name="branch"
									id="city" style="width: 350px;" type="text" placeholder="请输入城市">
							</label></li>
							<li><label>开户网点： <input name="branch"
									id="bankcardaddress" style="width: 350px;" type="text"
									placeholder="请输入开户网点">
							</label></li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt>
						<i class="icoModifyPas"></i>请填写您的取款密码<em
							style="font-size: 28px; color: #999">（非必填）</em>
					</dt>
					<dd>
						<ul class="onLinePad">

							<li><label for="pad">取款密码： <input
									name="cashpassword" style="width: 350px;" type="password"
									id="cashPassword" placeholder="请输入您的6位取款密码">
							</label></li>
						</ul>
						<p class="BanText">仅用于身份验证</p>
					</dd>
				</dl>
				<dl>
					<dt>
						<i class="icoPhone1"></i>请填写银行预留信息<em
							style="font-size: 28px; color: #999">（非必填）</em>
					</dt>
					<dd>
						<ul class="onLinePad">

							<li><label for="pad">手机号： <input name="mobile"
									id="phone" style="width: 350px;" type="number"
									placeholder="请输入真实有效的手机号">
							</label></li>
						</ul>
						<p class="BanText">信息加密处理，仅用于银行验证</p>
					</dd>
				</dl>
			</div>
			<input type="hidden" name="name" value="${cardName}" id="accountname">
			<input type="hidden" name="account" value="${cardNo}" id="bankId">
			<div class="button" style="margin-top: 100px;">
				<button type="submit" onclick="upData();">下一步</button>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		var checkflag = false;
		function upData() {
			if (checkSubmit()) {
				if (!checkflag) {
					checkflag = true;
					submit();
				} else {
					ToBase.layerMsg("请勿重复提交");
				}
			}
		}

		function submit() {
			var cashBankname = $("#bankName").val();
			var sheng = $("#province").val();
			var city = $("#city").val();
			var bankcardaddress = $("#bankcardaddress").val();
			var cashBankaccount = $("#bankId").val();
			var cashPassword = $("#cashPassword").val();
			var userName = $("#accountname").val();
			var phone = $("phone").val();

			var param = {};
			param["bankName"] = cashBankname;
			param["userName"] = userName;
			param["province"] = sheng;
			param["city"] = city;
			param["bankAddress"] = bankcardaddress;
			param["cardNo"] = cashBankaccount;
			param["repPwd"] = cashPassword;
			param["phone"] = phone;
			$.ajax({
				url : "${base}/center/banktrans/draw/commitbkInfo.do",
				data : param,
				type : 'POST',
				success : function(result) {
					if (!result.success) {
						ToBase.layerMsg(result.msg);
					} else {
						window.location.href = "${base}/m/toQb/setSuccess.do?title=银行卡绑定&content=恭喜您，银行卡绑定成功&status=ok";
					}
				},
				complete : function() {
					checkflag = false;
				}
			});
		}

		function checkSubmit() {
			var account = $("#accountname").val();
			var cashBankname = $("#bankName").val();
			var cashBankaccount = $("#bankId").val();
			var cashPassword = $("#cashPassword").val();
			var exp = /^([1-9][\d]{1,18}|x|X)$/;
			if (!account) {
				ToBase.layerMsg("真实姓名不能为空");
				return false;
			}
			if (!cashBankname) {
				ToBase.layerMsg("开户银行不能为空");
				return false;
			}
			if (!cashBankaccount) {
				ToBase.layerMsg("银行帐号不能为空");
				return false;
			}
			if (!cashBankaccount || !exp.test(cashBankaccount)) {
				ToBase.layerMsg("银行卡号格式错误");
				return false;
			}
			if (!cashPassword) {
				ToBase.layerMsg("取款密码不能为空");
				return false;
			}
			return true;
		}
	</script>
</body>
</html>