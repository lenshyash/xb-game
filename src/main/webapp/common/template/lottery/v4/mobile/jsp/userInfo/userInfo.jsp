<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>${_title}_个人中心</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../include/header.jsp"></jsp:include>
	<section>
		<div class="my">
			<ul>
				<li class="girl"
					data-href="${base}/m/toUser/info.do"><label
					class="icoGirl">头像</label><i><img src=""></i></li>
				<li style="background: #FFFFFF"><label class="icoNickname">昵称</label><span>${loginMember.account}</span></li>
			</ul>
			<ul>
				<li
					data-href="${base}/m/toQb.do"><label
					class="icoWallet">钱包</label></li>
			</ul>
			<%--<ul>
				<li
					data-href="#"><label
					class="icoAgent">代理制度</label></li> 
				<li
					data-href="#"><label
					class="icoShare">代理分享</label><span><img
						src="/statics/web/images/myEwm.png"></span></li>
			</ul>--%>
			<ul>
				<li
					data-href="${base}/m/toUser/betRecord.do"><label
					class="icoRecord">投注记录</label></li>
				<%--<li
					data-href="http://www.pc5858.com/index.php?m=web&amp;c=user&amp;a=workTeam"><label
					class="icoTeam">团队管理</label></li> --%>
			</ul>
			<ul>
				<li
					data-href="#" onclick="ToBase.layerMsg('未开放');"><label
					class="icoBiaoqing">表情</label></li>
			</ul>
			<ul>
				<li
					data-href="${base}/m/toUser/toSetUp.do"><label
					class="icoSetUp">设置</label></li>
			</ul>
		</div>
	</section>
	<jsp:include page="../include/footer.jsp"></jsp:include>
<script type="text/javascript">
$(function(){
	var nickUrl = $.cookie("account_${loginMember.account}_touxiang");
	$(".girl img").attr("src",'${base}/common/template/lottery/v4/mobile/images/touxiang/'+nickUrl+'.jpg');
})
</script>
</body>
</html>