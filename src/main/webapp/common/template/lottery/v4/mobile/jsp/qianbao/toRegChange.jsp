<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>${_title}_充值</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="${base}/common/css/core/bank/css/bank.css?v=1.3" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<script type="text/javascript" src="${base}/common/js/onlinepay/pay.js?v=6.2673"></script>
<script type="text/javascript" src="${base}/common/template/lottery/v4/mobile/js/pay_change.js"></script>
<style type="text/css">
	.bankCard2 em.xs,.bankCard2 em.xx{
		width: 40px;
	    height: 20px;
	    display: inline-block;
	    position: absolute;
	    top: 50%;
	    margin-top: -5px;
	    right:2em;
	   	transition: all 0.2s linear 0s;
    }
    .bankCard2 em.xs:after,.bankCard2 em.xx:after{
	    border: 20px solid transparent;
	    border-top: 20px solid #bfbfbf;
	    width: 0;
	    height: 0;
	    content: "";
	    position: absolute;
	    left: 0;
	}
	.radioClass span{
	    display: inline-block;
	    width: 30%;
	    float: left;
	    position: relative;
	    height: 55px;
	    text-align: center;
    	line-height: 60px;
    }
    .xxClass span{
    	background-repeat: no-repeat;
	    background-size: 145px 45px;
	    background-position: 0 5px;
    }
    .modify .bankCard2 li label{
    	width:70%;float:right;}
    .popupAlert .popup_header img{
		position: absolute;
	    top: .6%;
	    right: 13%;
	    border: 2px dashed #ffffff;
	    padding: 10px;
	    border-radius: 10px;
	}
	.popupAlert .popup_top{
		color:#fff;
		background: rgb(77, 131, 254);
		margin:0 2%;
		border-radius:10px;
		line-height: 35px;
		font-size:20px;
		padding-left:15px;
		position: absolute;
		bottom:40%;
	}
	.popupAlert .popup_top .xxyhzz{
		width:97%;
		text-align:center;
		display:inline-block;
		font-size:25px;
		color:rgba(255, 250, 118, 0.79);
		border-bottom:1px solid #fff;
		line-height: 60px;
	}
	.popupAlert .popup_top img{
		width:100%;
		position: absolute;
		top:45%;
	}
	.popupAlert .popup_footer img{
		width: 100%;
	    position: absolute;
	    top: 67%;
	}
</style>
</head>
<body style="background: #f5f5f5;">
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>充值</h1>
	</header>
	<section>
		<div class="modify">
			<h3 class="yuE">帐号：<em>${loginMember.account}</em>&nbsp;&nbsp;&nbsp;&nbsp;
				余额：<em>${loginMember.money}</em>
			</h3>
			<div class="bankCard2 chongzhi">
				<c:if test="${dptMap.get('onlineFlag') eq 'on'}">
				<dl>
					<dt>
						<i class="icoTop"></i>线上充值<em class="xs" style="transform: rotate(180deg);"></em>
					</dt>
					<dd>
						<ul class="radioClass">
							<c:choose>
								<c:when test="${onlineMap.size() == 0}">
									<li style="line-height: 150px;text-align: center;font-size:30px;">暂时没有配置支付方式</li>
								</c:when>
								<c:otherwise>
									<c:forEach items="${onlineMap}" var="line" varStatus="lines">
										<li>
										<span <c:choose>
												<c:when test="${not empty line.icon}">style="background: url('${line.icon}') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${line.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${line.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 5px; background-size: 155px 45px;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${line.iconCss}"
												</c:otherwise>
											</c:choose>>${line.payName}</span>
										<input id="radio${line.id}" name="radio" type="radio" value="${line.id}" min="${line.min}" max="${line.max}" iconcss="${line.iconCss}" pays_type="${online.payType}" pay_type="online">
										<label for="radio${line.id}"><em style="font-size:1.5rem;">最小充值金额<font color="red">${line.min}</font>元</em></label>
										<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${line.payDesc}">${line.payDesc}</marquee></li>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</ul>
					</dd>
				</dl>
				</c:if>
				<c:if test="${dptMap.get('fastFlag') eq 'on' || dptMap.get('bankFlag') eq 'on'}">
				<dl>
					<dt>
						<i class="icoBot"></i>线下充值<em class="xx" style="transform: rotate(180deg);"></em>
					</dt>
					<dd>
						<ul class="radioClass xxClass">
							<c:choose>
								<c:when test="${fastMap.size() == 0 && bankMap.size() == 0}">
									<li style="line-height: 150px;text-align: center;font-size:30px;">暂时没有配置支付方式</li>
								</c:when>
								<c:otherwise>
									<c:if test="${fastMap.size() > 0}">
									<c:forEach items="${fastMap}" var="fast" varStatus="fasts">
										<li>
										<span <c:choose>
												<c:when test="${not empty fast.icon}">style="background: url('${fast.icon}') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${fast.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 5px; background-size: 155px 45px;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${fast.iconCss}"
												</c:otherwise>
											</c:choose>>${fast.payName}</span>
										<input id="radio${fast.id}" name="radio" type="radio" value="${fast.id}" min="${fast.min}" max="${fast.max}" flabel="${fast.frontLabel}" pay_type="fast">
										<label for="radio${fast.id}"><em style="font-size:1.5rem;">最小充值金额<font color="red">${fast.min}</font>元</em></label>
										<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${fast.qrcodeDesc}">${fast.qrcodeDesc}</marquee></li>
									</c:forEach>
									</c:if>
									<c:if test="${bankMap.size() > 0}">
									<c:forEach items="${bankMap}" var="bank" varStatus="banks">
										<li>
										<span title="${bank.payName}" <c:choose>
												<c:when test="${not empty bank.icon}">style="background: url('${bank.icon}') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '4'}">style="background: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg') no-repeat 0 10px; background-size: 155px 45px;text-indent:-9999px"</c:when>
												<c:when test="${bank.payType == '3'}">style="background: url('${base}/common/template/lottery/jiebao/images/weixin.jpg') no-repeat 0 5px; background-size: 155px 45px;text-indent:-9999px" </c:when>
												<c:otherwise>
													class="icons ${bank.iconCss}"
												</c:otherwise>
											</c:choose>>${bank.payName}</span>
										<input id="radio${bank.id}" name="radio" type="radio" value="${bank.id}" min="${bank.min}" max="${bank.max}" pay_type="bank">
										<label for="radio${bank.id}"><em style="font-size:1.5rem;">最小充值金额<font color="red">${bank.min}</font>元</em></label>
										<marquee style="color: red;margin-right: 20px;margin-left: 20px;" scrollamount="3" title="${bank.bankDesc}">${bank.bankDesc}</marquee></li>
									</c:forEach>
									</c:if>
								</c:otherwise>
							</c:choose>
						</ul>
					</dd>
				</dl>
				</c:if>
			</div>
			<div class="btnNext" style="display:none;" id="online">
				<ul class="onLinePad" style="padding-top:0;padding-bottom:15px;background: #e8e7e7;">
		            <li>
		                <label class="icoMoney" for="money">充值金额：<input style="width:300px;height:60px;" type="number" id="money">
		                </label>
		            </li>
		        </ul>
		        <div class="allDesc"></div>
				<span class="button">
					<button class="colorDa" onclick="PayChange.online.onlineSub(this);">下一步</button>
				</span>
			</div>
			<div class="btnNext" style="display:none;" id="bank">
				<ul class="onLinePad" style="padding-top:0;padding-bottom:15px;background: #e8e7e7;">
		            <li>
		                <label class="icoMoney" for="money">充值金额：<input style="width:300px;height:60px;" type="number" id="money">
		                </label>
		            </li>
		             <li>
		                <label class="icoNickname" for="money">存款人姓名：<input style="width:300px;height:60px;" type="text" id="bankAccountName">
		                </label>
		            </li>
		        </ul>
		        <div class="allDesc"></div>
				<span class="button">
					<button class="colorDa" onclick="PayChange.bank.bankSub(this);">下一步</button>
				</span>
			</div>
			<div class="btnNext" style="display:none;" id="fast">
				<ul class="onLinePad" style="padding-top:0;padding-bottom:15px;background: #e8e7e7;">
		            <li>
		                <label class="icoMoney" for="money">充值金额：<input style="width:300px;height:60px;" type="number" id="money">
		                </label>
		            </li>
		            <li>
		                <label class="icoNickname" for="money"><span id="flabel"></span>：<input style="width:300px;height:60px;" type="text" id="payAccountName">
		                </label>
		            </li>
		        </ul>
		        <div class="allDesc"></div>
				<span class="button">
					<button class="colorDa" onclick="PayChange.fast.fastSub(this);">下一步</button>
				</span>
			</div>
		</div>
	</section>
	<jsp:include page="template_pay.jsp"></jsp:include>
	<jsp:include page="/member/${folder}/toRegister_yl.jsp"></jsp:include>
	<script type="text/javascript">
		var online = "${empty dptMap.get('onlineDesc')?'及时自动到账，推荐':dptMap.get('onlineDesc')}";
		var fast = "${empty dptMap.get('fastDesc')?'支持微信/支付宝二维码扫描':dptMap.get('fastDesc')}";
		var bank = "${empty dptMap.get('bankDesc')?'支持网银转账，ATM转账，银行柜台汇款':dptMap.get('bankDesc')}";
		var hostUrl1="${hostUrl1}";
		$(function(){
			$(".bankCard2 dl dt").click(function(){
				var isHidden = $(this).siblings("dd").is(":hidden");
				if(isHidden){
					$(this).find("em").css({"transform": "rotate(180deg)"});
					$(this).siblings("dd").slideDown();
				}else{
					$(this).find("em").css({"transform": "rotate(0deg)"});
					$(this).siblings("dd").slideUp();
				}
			})
			
			//选中radio
			$(".bankCard2 ul li input").click(function(){
				var pay_type = $(this).attr("pay_type"),desc='';
				$("#"+pay_type).show().siblings(".btnNext").hide();
				switch(pay_type){
					case "online":
						desc=online;
						break;
					case "fast":
						$("#flabel").text($(this).attr("flabel"));
						desc = fast;
						break;
					case "bank":
						desc = bank;
						break;
				}
				$("#"+pay_type).find(".allDesc").text(desc);
				$("body").animate({scrollTop: $("body").prop("scrollHeight")}, 300);
			})
		})
	</script>
</body>
</html>