<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html><head lang="en">
    <meta charset="UTF-8">
    <meta content="target-densitydpi=320,width=750,user-scalable=no" name="viewport">
    <meta content="no" name="apple-touch-fullscreen">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <title>${_title}_钱包</title>
    <link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
	<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
<header>
    <em onclick="history.go(-1)">返回</em>
    <h1>首次设置取款密码</h1>

</header>
<section>
    <div class="firstSetPas">
        <dl>
            <dt><em class="icoModifyPas"></em>请设置取款密码，建议勿与银行卡密码相同</dt>
            <dd>
                <div class="pasFrame">
                   <%-- <div class="dang"></div> --%>
                    <input name="password" type="number" maxlength="6" class="inputPas" autocomplete="off">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </dd>
        </dl>
        <dl>
            <dt><em class="icoModifyPas"></em>再次输入取款密码</dt>
            <dd>
                <div class="pasFrame">
                   <%-- <div class="dang"></div> --%>
                    <input name="password2" type="number" maxlength="6" class="inputPas" autocomplete="off">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </dd>
        </dl>

        <div class="button" style="margin-top: 100px"><button id="setBtn">下一步</button></div>
    </div>
</section>
    <script type="text/javascript">
        $(function() {
        	$(".inputPas").on("focus",function(){
                var index =$(this).val().length;
                if(index ==6){
                    $(this).siblings("ul").children("li").eq(index-1).addClass("guangbiao1");
                }else{
                    $(this).siblings("ul").children("li").eq(index).addClass("guangbiao");
                }
        	})
        	 $(".inputPas").on("blur",function(){
		        $(this).siblings("ul").children("li").removeClass("guangbiao");
		        $(this).siblings("ul").children("li").removeClass("guangbiao1");
		    })
		
		    $(".inputPas").on("input change",function(){
		        $(this).val($(this).val().replace(/[^\d]/g,''));
		        var index =$(this).val().length;
		        var val =$(this).val();
		        if(index > 6){
		            val=val.substring(0,6);
		            $(this).val(val);
		            return false;
		        }
		        var oLi =$(this).siblings("ul").children("li");
		        if(index){
		            oLi.eq(index).prevAll().text("*");
		            oLi.eq(index-1).nextAll().text("");
		            oLi.removeClass("guangbiao").eq(index).addClass("guangbiao");
		            if(index ==5){
		                oLi.removeClass("guangbiao1");
		            }
		            if(index ==6){
		                oLi.eq(index-1).text("*");
		                oLi.removeClass("guangbiao").eq(index-1).addClass("guangbiao1");
		            }
		
		        }
		        if(!index){
		            oLi.eq(0).text("");
		            oLi.removeClass("guangbiao").eq(0).addClass("guangbiao");
		        }
		
		    })
        	
            $("#setBtn").click(function() {
                //合法性验证
                var password = $("input[name=password]").val();
                var password2 = $("input[name=password2]").val();

                if (password.length < 6) {
                    layer.open({
                        content: "请输入6位数的密码",
                        btn: '我知道了'
                    });
                    return false;
                }
                if (password2.length == '') {
                    layer.open({
                        content: "请再次输入密码",
                        btn: '我知道了'
                    });
                    return false;
                }
                
                if (password2.length < 6) {
                    layer.open({
                        content: "请输入6位数的密码",
                        btn: '我知道了'
                    });
                    return false;
                }
                
                if (password !== password2) {
                    layer.open({
                        content: "两次密码不一致",
                        btn: '我知道了'
                    });
                    return false;                   
                }
                
                //验证通过提交数据
                $.ajax({
                    url: "${base}/center/member/meminfo/repwd.do",
                    type: "post",
                    data: {pwd: password, rpwd: password2},
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            window.location.href = "${base}/m/toQb/setSuccess.do?title=设置取款密码&content=设置取款密码成功";
                        } else {
                            layer.open({
                                content: "<p class='alert_msg'>"+ data.msg +"</p>",
                                btn: '我知道了'
                            });
                        }
                    },
                    error: function(error) {
                        layer.open({
                            content: "<p class='alert_msg'>服务器出错了</p>",
                            btn: '我知道了'
                        });
                    }
                });
            }); 
        });
    </script>
</body></html>