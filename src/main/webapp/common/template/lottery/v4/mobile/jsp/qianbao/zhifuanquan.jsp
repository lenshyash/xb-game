<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>${_title}_支付安全</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>支付安全</h1>
	</header>
	<section>
		<div class="my">
			<ul>
				<li data-href="${base}/m/toQb/toUpdPwd.do"><a href="javascript:void(0);"> <label class="icoModifyPas">修改取款密码</label></a></li>
				<li id="wjPsd"><a href="javascript:void(0);"> <label class="icoModifyPas">忘记密码</label></a></li>
			</ul>
			<ul>
				<li data-href="${kfUrl}"><a href="javascript:void(0);"><label class="icoNavArt2">联系客服</label></a></li>
			</ul>
		</div>
	</section>

	<div class="popup" style="display: none">
		<div class="config">
			<i class="configClose"></i>
			<p style="color: #ff4f4f;">您好找回密码请联系客服</p>
			<p style="color: #242424;">点击联系客服</p>
			<div class="cigBtn">
				<button
					data-href="${kfUrl}">联系客服</button>
				<button class="cancel">取消</button>
			</div>

		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#wjPsd").click(function() {
				$(".popup").fadeIn();
			});

			//点击取消关闭弹窗
			$(".cancel").click(function() {
				$(".popup").hide();
			})
		});
	</script>
</body>
</html>