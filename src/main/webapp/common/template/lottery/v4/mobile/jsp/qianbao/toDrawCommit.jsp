<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<title>${_title}提现</title>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>提现</h1>
	</header>
	<section>
		<div class="drawXinxi"
			style="line-height: 40px; padding: 10px 0 10px 30px; background: #f5f5f5; position: relative;">
			<strong
				style="width: 100%; text-align: center; display: inline-block;">提示信息</strong><br>
			1. 每天的取款处理时间为：<font color="red">${commit.get('star')}</font> 至 <font
				color="red">${commit.get('end')}</font>;<br> 2.
			取款3分钟内到账。(如遇高峰期，可能需要延迟到十分钟内到帐);<br> 3. 用户每日最小提现 <font
				color="red">${commit.get('min')}</font> 元，最大提现 <font color="red">${commit.get('max')}</font>
			元;<br> 4. 今日可取款 ${commit.get('wnum')} 次，已取款
			${commit.get('curWnum')} 次 ;<br> <strong style="width: 100%; text-align: center; display: inline-block;">消费比例</strong><br>
			1. 出款需达投注量：<font color="red">${commit.get('checkBetNum')}</font> ，
			当前有效投注金额：<font color="red">${commit.get('member').betNum}</font> <br>
			2. 是否能取款：<font color="red">${commit.get('drawFlag')}</font>
		</div>
		<div class="modify bgFF">
			<ul>
				<c:set value="${fn:length(commit.get('member').cardNo) }"
					var="cardNO"></c:set>
				<li><label class="icoBank">银&nbsp;行&nbsp;卡： <span>${commit.get('member').bankName}（尾数${fn:substring(commit.get('member').cardNo,cardNO-4,cardNO)}）</span>
				</label></li>
				<li><label class="icoNickname">真实姓名： <span>${commit.get('member').userName}</span>
				</label></li>
				<li><label class="icoWallet">账户余额： <span><fmt:formatNumber
								type="number" value="${commit.get('member').money}"
								maxFractionDigits="2" />RMB</span>
				</label></li>
				<li><label class="icoMoney" for="money">取款金额：<input
						type="number" id="money" style="width: 300px;" placeholder="最小提现${commit.get('min')}元">
				</label></li>
				<li><label class="icoModifyPas" for="cashPwd">取款密码：<input
						type="password" id="cashPwd" style="width: 300px;font-size: 20px; maxlength="6" placeholder="请输入您的6位取款密码">
				</label></li>
			</ul>
			<div class="button" id="tx_next" style="margin-top: 25px;">
				<button onclick="drawcommit();">申请提款</button>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		<c:choose>
			<c:when test="${commit.get('min') == null}">
				var min = 0;
			</c:when>
			<c:otherwise>
				var min = ${commit.get('min')};
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${commit.get('max') == null}">
				var max = 0;
			</c:when>
			<c:otherwise>
				var max = ${commit.get('max')};
			</c:otherwise>
		</c:choose>
		var balance = ${commit.get('member').money};

		function drawcommit() {
			$("#drawcommit").attr("disabled", "disabled");
			var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
			var m = $("#money").val();
			if (!exp.test(m)) {
				ToBase.layerMsg("请输入正确的金额");
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			m = parseInt(m);
			if (m < min) {
				ToBase.layerMsg("取款最小金额不能小于" + min);
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			if (m > balance) {
				ToBase.layerMsg("余额不足");
				$("#money").focus();
				$("#drawcommit").removeAttr("disabled");
				return;
			}

			if (max != 0 && max < m) {
				ToBase.layerMsg("取款最大金额不能大于"+max);
				$("#drawcommit").removeAttr("disabled");
				return;
			}
			var userQxpassword = $("#cashPwd").val();
			if (userQxpassword == null || userQxpassword == "") {
				ToBase.layerMsg("取款密码不能为空");
				$("#drawcommit").removeAttr("disabled");
				return;
			}

			$.ajax({
						url : "${base}/center/banktrans/draw/drawcommit.do",
						data : {
							money : m,
							repPwd : userQxpassword
						},
						success : function(result) {
							if (!result.success) {
								ToBase.layerMsg(result.msg);
							} else {
								layer.open({
								    content: '取款信息已提交！'
								    ,btn: ['确定', '取消']
								    ,yes: function(index){
								      //location.reload();
								     // window.location.href="${base}";
								     	window.location.href = "${base}/m/toQb/billsWeb.do?type=2";
								      	layer.close(index);
								    }
								  });
							}
						},
						complete : function() {
							$("#drawcommit").removeAttr("disabled");
						}

					});
		}
	</script>
</body>
</html>