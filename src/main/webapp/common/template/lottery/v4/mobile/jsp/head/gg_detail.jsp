<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<!DOCTYPE html>
<html><head lang="en">
    <meta charset="UTF-8">
    <meta content="target-densitydpi=320,width=750,user-scalable=no" name="viewport">
    <meta content="no" name="apple-touch-fullscreen">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>${_title}_消息</title>
   	<jsp:include page="../include/base.jsp"></jsp:include>
   	<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
</head>
<body>
<header>
    <em onclick="history.go(-1)">返回</em>
    <h1>公告详情</h1>
</header>
<section>
    <div class="rebate">
        <h2>${gg.title}</h2>
        <div class="rebateTime">
            <span class="left"><fmt:formatDate value="${gg.updateTime}" type="date" pattern="yyyy-MM-dd"/> </span>
        </div>
        <p>${gg.content}</p>
    </div>
</section>
</body></html>