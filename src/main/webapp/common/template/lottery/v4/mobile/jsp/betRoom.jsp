<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="referrer" content="always">
<meta charset="utf-8">
<%--meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"> --%>
<meta content="target-densitydpi=320,width=500,user-scalable=no"
	name="viewport">
<meta http-equiv="x-dns-prefetch-control" content="on">
<title>${_title}</title>
<script type="text/javascript"
	src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/base.js?v=1.2.2"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/layer_mobile/layer.js"></script>
<script type="text/javascript"
	src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/jquery.ajax.js"></script>
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/jquery.cookie.js"></script>
<link href="${base}/common/template/lottery/v4/mobile/css/room.css?v=1"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="${base}/common/template/lottery/v4/mobile/css/animation.css?v=v1" />
<link rel="stylesheet"
	href="${base}/common/template/lottery/v4/mobile/css/dialog_alert.css?v=v1" />
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/lottery/v4/mobile/css/swiper-3.4.1.min.css" />
<%--<script type="text/javascript" src="${base}/common/template/lottery/v4/mobile/js/socket.js?v=1.1.1"></script> --%>
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/date.js"></script>
<script type="text/javascript">
	Mobile = {
		base : '${base}',
	}
</script>
<style type="text/css">
.layui-m-layercont {
	font-size: 18px;
	padding: 25px 30px;
}

.layui-m-layerchild  .layui-m-anim-scale {
	width: 70%;
}

.room {
	-webkit-overflow-scrolling: touch;
}

.popupAlert img {
	width: 100%;
}
</style>
</head>
<body>
	<header>
		<!--<em onclick="history.go(-1)">返回</em>-->
		<em data-href="${base}/m/index.do">返回</em>
		<h1>${bl.name}${room.name}</h1>
		<div class="headerRight">
			<ul>
				<c:if test="${isLogin}">
					<li class="icoKeFu numAfter" data-href="${kfUrl}">
				</c:if>
				<c:if test="${!isLogin}">
					<li class="icoKeFu numAfter" data-href="${base}/toLogin.do">
				</c:if>
				<i></i>
				</li>
				<li class="icoAdd"><a href="#"></a></li>
			</ul>
		</div>
		<div class="menu" style="display: none">
			<ul>
				<%--icoRebate --%>
				<li class="icoRecord" data-href="${base}/m/toUser/betRecord.do">投注记录</li>
				<li class="icoResult" data-href="${base}/m/openAward.do">开奖结果</li>
				<li class="icoIntroduce" data-href="${base}/m/gameRule.do">玩法介绍</li>
				<li class="icoTrend" data-href="${base}/m/openAward.do?type=trend">开奖走势</li>
			</ul>
		</div>
	</header>
	<!--开奖结果-->
	<section>
		<div class="roomHead animated">
			<input type="hidden"
				value="${empty room.speakMoney?'0':room.speakMoney}" id="speakMoney">
			<input type="hidden" value="${room.betStatus}" id="betStatus">
			<input type="hidden" value="${room.robotStatus}" id="robotStatus">
			<input type="hidden" value="${isGuest?'1':'2'}" id="isGuest">
			<div class="roomHead1">
				<ul>

					<li class="roomLi">
						<p>
							距离<em id="issue">无期号</em>期<em id="kp_jz"></em>
						</p>
						<p>
							<span class="icoTimeClose" style="display: none;">已封盘</span> <span
								class="icoTime">00时00分00秒</span>
						</p>
					</li>
					<li>
						<p>余额</p>
						<p>
							<span class="icoAcer">${loginMember.money}</span>
						</p>
					</li>
				</ul>
			</div>
			<div class="roomHead2">
				<span></span><em></em>
			</div>
			<div class="betTixing" style="display: none;">
				<span><em>本期已下<font id="betCount">0</font>注
				</em></span><span id="quxiaoXz">查看注单详情<em class="jt"></em></span>
			</div>
		</div>
		<div class="quxiao" style="display: none;">
			<ul></ul>
		</div>
		<!--开奖结果-->
		<div class="lottery" style="display: none;">
			<dl>
				<dt>
					<ul>
						<li>期号</li>
						<li>开奖时间</li>
						<li>开奖结果</li>
					</ul>
				</dt>
				<dd></dd>
			</dl>
			<%--<div class="lotteryBut">
			<button
				data-href="#">更多开奖结果</button>
		</div> --%>
		</div>
		<!--房间开始-->
		<div class="room" id="wrapper">
			<div class="roomContent" id="scroller"></div>
		</div>
		<!--房间结束-->
		<%--客服开始 --%>
		<%--<div class="customer" id="drag" data-href="${kfUrl}"></div> --%>
		<!--客服结束-->

		<div class="customNews roomNews animated">
			<em class="bgCol" id="bettingBtu">投注</em> <em class="bgCol keyboard">取消</em>
			<div class="textArea">
				<div contenteditable="true" data-content="true"></div>
			</div>
			<img class="xiaolian" data_new="data_new"
				src="${base}/common/template/lottery/v4/mobile/images/xiaolian.png">
			<button id="speak">发送</button>
			<button id="bettingBtu1">投注</button>
			<div class="xiaolianList swiper-container swiper-container2">
				<div class="swiper-wrapper" style="margin: 15px 0;">
					<div class="swiper-slide">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="1" end="27" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>=10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<img class="img_face" data_index="${xValue}"
								src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png">
						</c:forEach>
						<img class="img_delete"
							src="${base}/common/template/lottery/v4/mobile/images/face/face_delete.png">
					</div>
					<div class="swiper-slide">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="28" end="54" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<img class="img_face" data_index="${xValue}"
								src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png">
						</c:forEach>
						<img class="img_delete"
							src="${base}/common/template/lottery/v4/mobile/images/face/face_delete.png">
					</div>
					<div class="swiper-slide">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="55" end="81" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<img class="img_face" data_index="${xValue}"
								src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png">
						</c:forEach>
						<img class="img_delete"
							src="${base}/common/template/lottery/v4/mobile/images/face/face_delete.png">
					</div>
					<div class="swiper-slide">
						<c:set value="0" var="num"></c:set>
						<c:forEach begin="82" end="108" var="xValue">
							<c:if test="${xValue<10 }">
								<c:set value="00${xValue}" var="num"></c:set>
							</c:if>
							<c:if test="${xValue>10 && xValue<100 }">
								<c:set value="0${xValue}" var="num"></c:set>
							</c:if>
							<img class="img_face" data_index="${xValue}"
								src="${base}/common/template/lottery/v4/mobile/images/face/${num}@2x.png">
						</c:forEach>
						<img class="img_delete"
							src="${base}/common/template/lottery/v4/mobile/images/face/face_delete.png">
					</div>
				</div>
				<div class="swiper-pagination swiper-p2"></div>
			</div>
			<jsp:include page="lotInclude/type_${room.lotType}.jsp"></jsp:include>
		</div>

		<div class="cenMoneyWarp">
			<div class="cenMoney">
				<h1 id="touzhuzhi"></h1>
				<ul class="text-ul">
					<li>20</li>
					<li>50</li>
					<li>100</li>
					<li>200</li>
					<li>500</li>
					<li>800</li>
					<li>1000</li>
					<li>2000</li>
					<li>5000</li>
					<li class="butzhi">梭哈</li>
				</ul>
				<div class="moneyInput">
					输入金额：<input type="number" id="inputNumber">

				</div>
				<div class="cigBtn" id="butJp">
					<button class="queren">确认</button>
					<button class="cancel">取消</button>
				</div>
			</div>
		</div>
		<!--投注金额 结束-->

	</section>
	<%--弹窗div --%>
	<div class="popup" style="display: none;">
		<div class="config1 animated">
			<i class="configClose1"></i>
		</div>
	</div>
	<%-- 首次进入提示 --%>
	<div class="popupAlert" style="display: none;">
		<div class="popup_header">
			<img class="animated fadeInDown"
				style="position: absolute; top: -8.3%;"
				src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_1.png">
		</div>
		<div class="popup_footer">
			<img class="animated fadeInUp"
				style="position: absolute; bottom: -10.3%; left: 0.3%;"
				src="${base}/common/template/lottery/v4/mobile/images/tishi/bet_order_2.png">
		</div>
	</div>
	<div class="whiteDiv"
		style="display: none; width: 100%; height: 100%; position: fixed; top: 0; left: 0; background: rgba(0, 0, 0, 0); z-index: 99;"></div>
	<script
		src="${base}/common/template/lottery/v4/mobile/js/swiper-3.4.1.jquery.min.js"></script>
	<script type="text/javascript">
		var hostUrl = window.location.host;
		//var hostUrl = "yb16v4.yb876.com:60088";
		var Room = {
			lotCode : '${bl.code}',
			lotType : '${bl.type}',
			roomId : '${room.id}',
			account : '${loginMember.account}',
			robotAccount : '${not empty robotAuto ? robotAuto.account:"管理员"}',
			robotImg : '${not empty robotAuto ? robotAuto.imgId:"/common/template/lottery/v4/mobile/images/touxiang/avatar.png"}',
		}
		$(function() {
			ToBase.initBetRoom();
			if (!ToBase.utils.getCookieData("betRoomOneLogin")) {
				$(".popupAlert").show();
				ToBase.utils.cookieData("betRoomOneLogin", true, 1);
			}
		})

		var swiper = new Swiper(
				'.swiper-container1',
				{
					pagination : '.swiper-p1',
					loop : true,
					prevButton : '.swiper-button-prev',
					nextButton : '.swiper-button-next',
					pagination : '.swiper-p1',
					paginationType : 'fraction',
					onSlideChangeEnd : function(swiper) {
						//console.info(swiper.activeIndex) //切换结束时，告诉我现在是第几个slide
						var text = $("span.swiper-pagination-current").text(), titleText = '';
						switch (parseInt(text)) {
						case 1:
							titleText = '大小单双';
							break;
						case 2:
							titleText = '数字';
							break;
						case 3:
							titleText = '特殊玩法';
							break;
						}
						$(".jianpanTitle h4").text(titleText);
					}
				});
		$("#myBtn").hide(); //隐藏投注键盘
		//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
		window.onbeforeunload = function() {
			ToBase.betRoom.clearOnline();
		}
	</script>
	<style>
/*其他颜色需更改css样式 007aff */
.swiper-button-next {
	width: 27px !important;
	top: 24%;
	right: 22px;
	background-image: none;
	height: 30px;
}

.swiper-button-prev {
	width: 27px !important;
	top: 24%;
	left: 24px;
	background-image: none;
	height: 30px;
}

.swiper-pagination.swiper-pagination-fraction {
	top: 10px !important;
	bottom: initial; ! important;
	display: none;
}

.swiper-p2 {
	
}
</style>
</body>
</html>