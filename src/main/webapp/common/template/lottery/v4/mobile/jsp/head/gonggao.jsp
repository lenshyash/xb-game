<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>${_title}_消息</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/lottery/v4/mobile/css/swiper-3.4.1.min.css" />
<style type="text/css">
	.shouye-body-banner {
	    width: 96%;
	    margin: 10px 2%;
	    background: #ffffff;
	    border-radius: 5px;
	    border: 1px solid #ccc;
	    padding: 2%;
	}
	.swiper-slide>a>img {
	    width: 100%;
	    min-height: 130px;
	    height: 100%;
	}
	.swiper-slide>a {
	    display: block;
	    width: 100%;
	}
	body{
	background: #efefef;}
</style>
</head>
<body>
	<jsp:include page="/common/template/lottery/v4/mobile/jsp/include/header.jsp"></jsp:include>
	<section>
		<div class="shouye-body-banner">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<c:forEach items="${lunbo}" var="lun">
						<div class="swiper-slide">
							<a class="shouye-sw-box" href="${lun.titleUrl}"><img alt="${lun.title}"
								src="${lun.titleImg}" /></a>
						</div>
					</c:forEach>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
		<div class="newsTitle">
			<ul id="news_tab">
				<li class="active" data="1">公告</li>
				<li data="2" class="">信息</li>
			</ul>
		</div>
		<div class="newsContent">
			<ul class="record_list animated" style="display: block;">
			</ul>
			<ul class="record_list animated" style="display: none;"></ul>
		</div>
		<div class="zhanwei" style="display: none;"></div>
	</section>
	<jsp:include page="/common/template/lottery/v4/mobile/jsp/include/footer.jsp"></jsp:include>
	<script src="${base}/common/template/lottery/v4/mobile/js/swiper-3.4.1.jquery.min.js"></script>
	<script type="text/javascript">
		var pageSize = 100;
		var pageNo = 1;
		var status = 0;
		var pageDatas = [];
		var swiper = new Swiper('.swiper-container', {
			pagination : '.swiper-pagination',
			autoplay : 3000,
			autoplayDisableOnInteraction : false,
			loop : true
		});
		$(function(){
			searchMsg();
			gg();
			
			
			$("#news_tab li").click(function(){
				$(this).addClass("active").siblings().removeClass("active");
				var index = $(this).index();
				$(".newsContent ul:eq("+index+")").show().addClass("fadeInRight").siblings("").hide();
			})
		})
		
		
		function gg(){
			var html = '';
			$.ajax({
				url:'${base}/getConfig/getArticle.do',
				data:{code:13},
				success:function(res){
					if(res.length==0){
						$(".zhanwei").show();
						return;
					}
					$(".zhanwei").hide();
					$.each(res,function(i,j){
						var timer = new Date(j.updateTime);
						html += '<li data-href="${base}/m/gg_detail.do?id='+j.id+'"><span>'+$.formatDate("yyyy-MM-dd",timer,0)+'</span><h3>'+j.title+'</h3></li>'
					})
					$(".newsContent ul:eq(0)").html(html);
				}
			})
		}
		
		
		function searchMsg(){
			$.ajax({
				url:'${base}/center/news/message/list.do',
				data:{
					pageNumber : pageNo,
					pageSize : pageSize,
					status:status
				},
				success:function(res){
					var html ='';
					if(res.totalCount==0){
						$(".zhanwei").show();
						return;
					}
					$(".zhanwei").hide();
					$.each(res.list,function(i,j){
						pageDatas[j.id]=j;
						var timer = new Date(j.createDatetime),type="";
						if(j.status == 2){
							type="yidu";
						}
						html += '<li class="'+type+'" data-id="'+j.id+'" onclick="readMsg(this);"><span>'+$.formatDate("yyyy-MM-dd",timer,0)+'</span><h3>'+j.title+'</h3></li>'
					})
					$(".newsContent ul:eq(1)").html(html);
				}
			})
		}
		
		function readMsg(source){	
			source = $(source);
			var row = pageDatas[source.attr("data-id")];
			$.ajax({
				url:"${base}/center/news/message/read.do",
				data:{id:source.attr("data-id")},
				success:function(res){
					if(res.success){
						if(!source.hasClass("yidu")){source.addClass("yidu");}
						layer.open({
						    type: 1,
						    content: readHtml(row),
						    anim: 'up',
						    style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
						});
					}else{
						ToBase.layerMsg(res.msg);
					}
				}
			})	
		}
		
		function readHtml(res){
			console.info(res);
			var timer = new Date(res.createDatetime);
			var html = '<header><em onclick="layer.closeAll()">返回</em><h1>消息详情</h1></header>';
			html += '<section><div class="rebate"><h2>'+res.title+'</h2><div class="rebateTime">';
			html += '<span class="left">'+$.formatDate("yyyy-MM-dd",timer,0)+'</span>';
			html += '</div><p>'+res.message+'</p></div></section>';
			return html;
		}
</script>
</body>
</html>