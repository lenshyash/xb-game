<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav>
	<c:choose>
		<c:when test="${isLogin}">
			<ul>
		        <li ${not empty navClass && navClass eq 'DATING'?'class="active"':''} data-href="${base}/m/index.do">大厅</li>
		        <li ${not empty navClass && navClass eq 'QIANBAO'?'class="active"':''} data-href="${base}/m/toQb.do">钱包</li>
		        <li ${not empty navClass && navClass eq 'DONGTAI'?'class="active"':''} data-href="${base}/m/gonggao.do">动态</li>
		        <li ${not empty navClass && navClass eq 'WODE'?'class="active"':''} data-href="${base}/m/toUser.do">我的</li>
		    </ul>
		</c:when>
		<c:otherwise>
			<ul>
		        <li ${not empty navClass && navClass eq 'DATING'?'class="active"':''} data-href="${base}/m/index.do">大厅</li>
		        <li ${not empty navClass && navClass eq 'QIANBAO'?'class="active"':''} data-href="${base}/toLogin.do">钱包</li>
		        <li ${not empty navClass && navClass eq 'DONGTAI'?'class="active"':''} data-href="${base}/toLogin.do">动态</li>
		        <li ${not empty navClass && navClass eq 'WODE'?'class="active"':''} data-href="${base}/toLogin.do">我的</li>
		    </ul>
		</c:otherwise>
	</c:choose>
</nav>
<div class="whiteDiv" style="display:none;width: 100%;height: 100%;position: fixed;top: 0;left: 0;background: rgba(0,0,0,0.0);z-index: 99;"></div>