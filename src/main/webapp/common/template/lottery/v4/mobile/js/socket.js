var socket; //socket变量初始化
//var port = ":1500";
var msgQueue = new Array();	//消息列队
var retryCount = 10;	//尝试重新连接次数
var websocketToken;
var downTimers = 0;
//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
window.onbeforeunload = function() {
	InitWebSocket.closeWebSocket();
}


var InitWebSocket = {
	//验证是否支持socket
	vialdate:function(){
		if (!!window.WebSocket && window.WebSocket.prototype.send){
			 return true;
		}
		return false;
	},
	hostUrl:function(){
		return hostUrl + Mobile.base + '/websocket?rId='+Room.roomId;
		//return hostUrl + port + Mobile.base + '/websocket?rId='+Room.roomId+"&account="+Room.account;
	},//可以考虑加入websocket token验证
	initKet:function(){
		if(!InitWebSocket.vialdate){
			ToBase.layerOpen("该浏览器不支持socket,请更换浏览器");
			return;
		}
		socket = new WebSocket("ws://"+InitWebSocket.hostUrl());
		// 打开Socket 
		socket.onopen = function(event) { 
			//开始计时，180秒内没活动则退出房间
			InitWebSocket.xinTiaoTimer();
			// 发送一个初始机器人消息
			var ts = parseInt($("#isGuest").val())==2?'会员':'游客';
			var initMsg= {
					"message":"欢迎"+ts+"【"+Room.account+"】进入房间",
					"commandid":1003
			}
			InitWebSocket.robotSend(initMsg);
			setTimeout(function(){	//2秒后获取缓存投注记录
				var orderCache = {
						"commandid":1009,
						"lotCode":Room.lotCode,
						"roomId":Room.roomId
				}
				InitWebSocket.robotSend(orderCache);
			},2000)
			// 监听消息
			socket.onmessage = function(event) { 
			    console.log('Client received a message',event);
			    if(event.data != 'ping'){
			    	var msgData = JSON.parse(event.data);
			    	InitWebSocket.appendHtml(msgData);
			    }
			}; 
			// 监听Socket的关闭
			socket.onclose = function(event) { 
				console.log('Client notified socket has closed',event); 
			}; 
			// 关闭Socket.... 
			//socket.close() 
			socket.onerror = function(evt)
			{
			    console.log("WebSocketError!");
			};
		};
	},
	robotSend:function(msg){
		msgQueue.unshift(msg);
		var state = socket.readyState;
		switch(state){
		case WebSocket.CLOSING:	//连接正在关闭
			break;
		case WebSocket.CONNECTING:	//未连接
			break;
		case WebSocket.CLOSED:	//连接已关闭或不可用
			InitWebSocket.retryConnect();	//尝试重新连接
			break;
		case WebSocket.OPEN:	//连接可用
			for(var i=0;i<msgQueue.length;i++){
				var msg = msgQueue.pop();
				socket.send(JSON.stringify(msg));
			}
			break;
		default:
			break;
		}
	},
	retryConnect:function(){
		var count =0;
		var retry = window.setInterval(function(){
			count ++;
			if(retryCount == count){
				clearInterval(retry);
			}
			InitWebSocket.initKet();
		},250);
	},
	appendHtml:function(msg){
		//根据类型转换格式，填充到聊天窗口
		console.info("socket result data————" + JSON.stringify(msg));
		var paramMsg = '',float="left",account='机器人',touxiang='avatar.png',html='';
		switch(msg.commandid){
			case 1001:
			case 1002:
			case 1003:
			case 1004:	//异常处理
			case 1007:
				paramMsg = msg.message;
				break;
			case 1005:
				downTimers = 0;
			case 1008:	//机器人下注
				//console.info("机器人下注"+msg.account);
				html = InitWebSocket.betOrderShow(msg);
				break;
			case 1006:
				downTimers = 0;
				account=msg.account;
				if(Room.account == account){
					float="right";
				}
				touxiang=msg.nickUrl+'.jpg';
				paramMsg = msg.message;
				break;
			case 1009:
				$.each(msg.message,function(i,j){
					html = InitWebSocket.betOrderShow(j);
					$("#wrapper .roomContent").append(html);
				})
				$(".room").animate({scrollTop: $(".roomContent").outerHeight()}, 300);	//每次有信息都滚动到底部
				return false;
			case 1010:	//后台清除，无需执行
				//清楚前端内容
				$("#scroller .userBetting").remove();
				return false;
			case 1011:
				ToBase.layerOpen("非法操作异常");
				break;
			case 1012:	//踢出
				window.location.href = Mobile.base+"/m/index.do";
				break;
		}
		if(msg.commandid != 1005 && msg.commandid != 1008){
			html+='<div class="userBetting"><h3 style="text-align:'+float+'">'+account+'</h3>';
			html+='<ul class="'+float+'"><li><img src="'+Mobile.base+'/common/template/lottery/v4/mobile/images/touxiang/'+touxiang+'">';
			html+='</li><li style="font-size:14px;"><pre>'+paramMsg+'</pre></li></ul></div>';
		}
		$("#wrapper .roomContent").append(html);
		$(".room").animate({scrollTop: $(".roomContent").outerHeight()}, 300);	//每次有信息都滚动到底部
	},
	closeWebSocket:function(){
		socket.close();
	},
	betOrderShow:function(msg){
		var account = msg.account,float="left";
		if(Room.account == account){
			float="right";
		}
		var html = '<div class="userBetting"><h3 style="text-align:'+float+'">'+account+'</h3>';
		html += '<dl class="'+float+'"><dt><img src="'+Mobile.base+'/common/template/lottery/v4/mobile/images/touxiang/'+msg.nickUrl+'.jpg"></dt><dd>';
		$.each(msg.data,function(i,j){
			html += '<p o_id="'+j.oddsId+'" p_code="'+j.playCode+'"><label>类型：</label><span>'+j.name+'</span><u class="icoAcer"></u><em>'+j.money+'</em><label style="color: #dc5d55;float:right;">金额：</label></p>'
		})
		html += '<div><span style="float: left">'+msg.endTime+'</span><em></em><span style="float: right">第'+msg.qiHao+'期</span></div><i></i></dd></dl></div>';
		return html;
	},
	xinTiaoTimer:function(){
		var t1 = window.setInterval(function(){
			if(downTimers == 180){
				InitWebSocket.robotSend({"commandid":1012});
				clearTinterval(t1);
				return;
			}
			downTimers++;
		},1000);
	}
}