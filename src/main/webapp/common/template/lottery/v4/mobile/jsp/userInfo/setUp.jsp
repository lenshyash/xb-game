<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>个人中心</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
	<header>
		<em onclick="javascript :history.go(-1);">返回</em>
		<h1>设置</h1>
	</header>
	<section>
		<div class="my">
			<c:if test="${!isGuest}">
				<ul>
					<li data-href="${base}/m/toUser/toLoginPwd.do"><label class="icoModifyPas">修改登录密码</label></li>
				</ul>
			</c:if>
			<ul>
				<%--<li data-href="#"><label class="icoAboutUs">关于我们</label></li> --%>
				<li id="logout"><a href="#"><label class="icoSignOut">退出</label></a></li>
			</ul>
		</div>
	</section>

	<div class="popup" style="display: none;">
		<div class="config">
			<i class="configClose"></i>
			<p style="color: #ff4f4f;">您真的要退出登录吗？</p>
			<p style="color: #242424;">点击确认继续</p>
			<div class="cigBtn">
				<button id="ok">确认</button>
				<button class="cancel">取消</button>
			</div>

		</div>
	</div>
	<script type="text/javascript">
		$("#logout").click(function(){
			$(".popup").fadeIn();
		})
		$("button.cancel").click(function(){
			$(".popup").hide();
		})
		$("button#ok").click(function(){
			$.ajax({
				url:'${base}/m/logout.do',
				success:function(res){
					if(res.success){
						window.location.href = "${base}/toLogin.do";
					}
				}
			})
		})
	</script>
</body>
</html>