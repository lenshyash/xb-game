<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	.numAfter i{
	    position: absolute;
	    min-width: 26px;
	    height: 26px;
	    background: #ff4f4f;
	    border-radius: 50%;
	    right: 12px;
	    top: 12px;
	    line-height: 22px;
	    font-size: 20px;
	    text-align: center;
	    padding: 2px;
	}
</style>
<header>

    <h1>${empty navClass?_title:navClass eq 'DATING'?_title:navClass eq 'QIANBAO'?'钱包':navClass eq 'WODE'?'个人中心':navClass eq 'DONGTAI'?'动态':_title}</h1>
    <div class="headerRight">
        <ul>
        	<c:if test="${isLogin}">
        		 <li class="icoKeFu numAfter" data-href="${kfUrl}">
        	</c:if>
        	<c:if test="${!isLogin}">
        		 <li class="icoKeFu numAfter" data-href="${base}/toLogin.do">
        	</c:if>
            </li>
            <li class="icoAdd"><a href="#"></a> </li>
        </ul>
    </div>
    <div class="menu" style="display: none;<c:if test="${navClass eq 'DATING'}">width:165px;</c:if>">
        <ul>
        	<c:choose>
        	<c:when test="${navClass eq 'DATING'}">
        		<li class="icoCz1" data-href="${base}/m/toQb/toRegChange.do">充值</li>
            	<li class="icoCz2" data-href="${base}/m/toQb/toDrawCommit.do">提现</li>
        	</c:when>
        	<c:otherwise>
	            <li class="icoRecord" data-href="${base}/m/toUser/betRecord.do">投注记录</li>
	            <li class="icoResult" data-href="${base}/m/openAward.do">开奖结果</li>
	            <li class="icoIntroduce" data-href="${base}/m/gameRule.do">玩法介绍</li>
	            <li class="icoTrend" data-href="${base}/m/openAward.do?type=trend">开奖走势</li>
            </c:otherwise>
            </c:choose>
        </ul>
    </div>
</header>