<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>${_title}_交易记录</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
<link href="${base}/common/template/lottery/v4/mobile/css/bills_web.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<link
	href="${base}/common/template/lottery/v4/mobile/js/dropload/dropload.css"
	rel="stylesheet">
<script type="text/javascript"
	src="${base}/common/template/lottery/v4/mobile/js/dropload/dropload.min.js"></script>
<script type="text/javascript" src="${base}/common/js/contants.js?v=2"></script>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>交易记录</h1>

	</header>
	<section>
		<div class="bettingRecord">
			<div class="bettingRecordSearch">
				<div class="teamSearchCondition clearfix"
					style="margin-bottom: 30px">
					<div class="teamSearch">
						<span>从：</span><label> <input type="text"
							class="inputDate" id="startTime" disabled="" value="${startTime}">
							<input name="start_time" type="date" onchange="getData()"
							max="${startTime}"></label>
					</div>
					<div class="teamSearch">
						<span>到：</span><label> <input type="text"
							class="inputDate" id="endTime" disabled="" value="${endTime}">
							<input name="end_time" type="date" onchange="getData()"
							max="${endTime}"></label>
					</div>
				</div>
				<div class="wrap">
					<div class="tabs">
						<c:if test="${onlinePay eq 'on'}">
						<a href="#" hidefocus="true" click_type="1" click_open="${base}/center/record/hisrecord/comrd.do">充值记录</a> 
						</c:if>
						<a href="#" hidefocus="true" click_type="2" click_open="${base}/center/record/hisrecord/drawrd.do">取款记录</a> 
						<c:if test="${changeMoney eq 'on'}">
						<a href="#" hidefocus="true" click_type="3" click_open="${base}/center/record/mnyrecord/list.do">帐变记录</a>
						</c:if>
					</div>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<c:if test="${onlinePay eq 'on'}">
							<div class="swiper-slide">
								<div class="BRSearchCondition clearfix"
									style="margin-top: 10px;">
									<div>
										<span>类型：</span> <label> <em class="inputW">全部</em>
											<select class="select" name="type" id="type"
											onchange="getData()">
												<option value="">全部</option>
												<option value="5">在线存款</option>
												<option value="6">快速入款</option>
												<option value="7">一般存款</option>
										</select>
										</label>
									</div>
									<div>
										<span>交易状态：</span> <label> <em class="inputW">全部</em>
											<select class="select" name="status" id="status"
											onchange="getData()">
												<option value="">全部</option>
												<option value="1">处理中</option>
												<option value="2">处理成功</option>
												<option value="3">处理失败</option>
										</select>
										</label>
									</div>
								</div>
							</div>
							</c:if>
							<div class="swiper-slide">
								<div class="BRSearchCondition clearfix"
									style="margin-top: 10px;">
									<div>
										<span>交易状态：</span> <label> <em class="inputW">全部</em>
											<select class="select" name="status" id="drawStatus"
											onchange="getData()">
												<option value="">全部</option>
												<option value="1">处理中</option>
												<option value="2">处理成功</option>
												<option value="3">处理失败</option>
										</select>
										</label>
									</div>
								</div>
							</div>
							<c:if test="${changeMoney eq 'on'}">
							<div class="swiper-slide">
								<div class="BRSearchCondition clearfix"
									style="margin-top: 10px;">
									<div>
										<span>交易状态：</span> <label> <em class="inputW">全部</em>
											<select class="select" name="status" id="moneyType"
											onchange="getData()">
										</select>
										</label>
									</div>
								</div>
							</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<div class="bettingRecordList">
				<ul id="record_list">
				</ul>
				<div class="dropload-down">
					<div class="dropload-noData"></div>
				</div>
			</div>
		</div>
	</section>
	<script id="moneytype_tpl" type="text/html">
		<option value="">全部</option>
		{{each data as option}}
        	<option value="{{option.id}}">{{option.name}}</option>
		{{/each}}
	</script>
	<script
		src="${base}/common/template/lottery/v4/mobile/js/idangerous.swiper.min.js"></script>
	<script type="text/javascript">
		var moneyType = ${moneyType};
	 	var start_time=$("#startTime").val(),end_time=$("#endTime").val() ,status,type;
		var windowHeight = $(window).height();
		var openUrl = '',clickType,flag=false;
		var tabsSwiper = new Swiper('.swiper-container', {
			speed : 500,
			onSlideChangeStart : function() {
				$(".tabs .active").removeClass('active');
				$(".tabs a").eq(tabsSwiper.activeIndex).addClass('active');
			}
		});

		$(".tabs a").on('touchstart mousedown', function(e) {
			e.preventDefault();
			if($(this).hasClass("active") || flag){
				return;
			}
			$(this).addClass('active').siblings().removeClass("active");
			tabsSwiper.swipeTo($(this).index());
			openUrl = $(this).attr("click_open");
			clickType = $(this).attr("click_type");
			getData();
			flag = true;
			setTimeout(function(){
				flag =false;
			},2000)
		});

		$(".tabs a").click(function(e) {
			e.preventDefault();
		});

		var type = "${type}";
		$(function() {
			if(type){
				clickType = type;	//取款的时候跳转
				$(".tabs a[click_type="+type+"]").addClass("active");
			}else{	//默认的时候跳转
				$(".tabs a:eq(0)").addClass("active");
				clickType = $(".tabs a.active").attr("click_type");
			}
			openUrl = $(".tabs a.active").attr("click_open");
			initMoneyType();
			getData();
		})
		function getData() {
			var s_time = $("input[name=start_time]").val(),e_time=$("input[name=end_time]").val();
			if(s_time){
				start_time = s_time;
			}
			if(e_time){
				end_time = e_time;
			}
			switch(parseInt(clickType)){
				case 1:	//cz
					type = $("#type").val();
					status = $("#status").val();
					break;
				case 2:	//qk
					status = $("#drawStatus").val();
					$("#type").val('');
					break;
				case 3:	//zb
					status = $("#moneyType").val();
					$("#type").val('');
					break;
				default:
					$("#startTime").val(start_time);
					$("#endTime").val(end_time);
					break;
			}
			pageData();
		}

		function pageData() {
			var page = 1; //页码
			$('#record_list').html("");
			$('.dropload-down').remove();

			var record_type = '';
			// dropload
			$('.bettingRecordList')
					.dropload(
							{
								scrollArea : window,
								domDown : {
									domNoData : '<div class="dropload-noData"></div>'
								},
								loadDownFn : function(me) {
									var result = '';
									$
											.ajax({
												url : openUrl,
												data : {
													startTime : start_time,
													endTime : end_time,
													type : type,
													status : status,
													page : page,
													rows : 10
												},
												success : function(data) {
													var dataLen = data.list.length;
													if (page > 2) {
														$('.zhanwei')
																.removeClass(
																		'zhanwei');
													} else if (dataLen > 0) {
														$('.zhanwei')
																.removeClass(
																		'zhanwei');
													} else {
														$('.bettingRecordList')
																.addClass(
																		'zhanwei');
														var mainTop = $(
																".bettingRecordList")
																.offset().top;
														var newHeight = windowHeight
																- mainTop;
														$(".bettingRecordList")
																.css(
																		{
																			height : newHeight
																		});
													}
													if (dataLen > 0) {
														if(page<=2){
															switch(parseInt(clickType)){
															case 1:
																result += '<li class="title" style="display:flex;"><div style="flex-grow:1"><em> 总充值：</em><p style="color: red">+'
																	+ toDecimal2(data.aggsData.totalMoney)
																	+ '</p></div></li>';
																//数据处理
																$.each(data.list,function(i,j) {
																	result += '<li orderId="'+j.orderNo+'"><div style="width:50%;"><div><label>'+j.payName+'</label></div>'
																	result += '<p>充值方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</p><p>'+DateUtil.formatDatetime(j.createDatetime)+'</p>';
																	result += '</div><div><div><b>金额：</b><span style="color: red">+ '+toDecimal2(j.money)+'</span></div>';
																	result += '<p>状态：'+GlobalTypeUtil.getTypeName(1, 2, j.status)+'</p></div></li>';
																	})
																break;
															case 2:
																result += '<li class="title" style="display:flex;"><div style="flex-grow:1"><em> 总取款：</em><p style="color: red">+'
																	+ toDecimal2(data.aggsData.drawMoney)
																	+ '</p></div></li>';
																//数据处理
																$.each(data.list,function(i,j) {
																	result += '<li orderId="'+j.orderNo+'"></div>'
																	result += '<p>取款方式：'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</p><p>'+DateUtil.formatDatetime(j.createDatetime)+'</p>';
																	result += '</div><div><div><b>金额：</b><span style="color: red">+ '+toDecimal2(j.drawMoney)+'</span></div>';
																	result += '<p>状态：'+GlobalTypeUtil.getTypeName(1, 3, j.status)+'</p></div></li>';
																	})
																break;
															case 3:
																//数据处理
																$.each(data.list,function(i,j) {
																	result += '<li orderId="'+j.id+'"><div><div><label>'+GlobalTypeUtil.getTypeName(1,1,j.type)+'</label></div>'
																	result += '<p>'+DateUtil.formatDatetime(j.createDatetime)+'</p>';
																	result += '</div><div><div><b style="font-size:20px;">变动前金额：</b><span style="color: red;font-size:20px"> '+toDecimal2(j.beforeMoney)+'</span></div>';
																	result += '<div><b style="font-size:20px;">变动金额：</b><span style="color: red;font-size:20px">'+toDecimal2(j.money)+'</span></div>';
																	result += '<div><b style="font-size:20px;">变动后金额：</b><span style="color: red;font-size:20px"> '+toDecimal2(j.afterMoney)+'</span></div></div></li>';
																	})
																break;
															}
														}
													} else {
														// 锁定
														me.lock();
														// 无数据
														me.noData();
													}
													// 为了测试，延迟1秒加载
													setTimeout(function() {
														$('#record_list').append(result);
															// 每次数据加载完，必须重置
															me.resetload();
														}, 1000);

												},
												error : function(xhr, type) {
													// 即使加载出错，也得重置
													me.resetload();
												}
											});

									page++; //页面加1
								},
								threshold : 50
							});
		}

		function toDecimal2(x) {
			var f = parseFloat(x);
			if (isNaN(f)) {
				return false;
			}
			var f = Math.floor(x * 100) / 100;
			var s = f.toString();
			var rs = s.indexOf('.');
			if (rs < 0) {
				rs = s.length;
				s += '.';
			}
			while (s.length <= rs + 2) {
				s += '0';
			}
			return s;
		}

		function betRecordDetail(source) {
			source = $(source);
			$.ajax({
						url : '${base}/lotteryBet/getBcLotteryOrderDetail.do',
						data : {
							orderId : source.attr("orderId")
						},
						success : function(res) {
							var html = templateHtml(res);
							if (res != null) {
								layer
										.open({
											type : 1,
											content : html,
											anim : 'up',
											style : 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
										});
							}
						}
					})
		}

		function templateHtml(res) {
			var height = $(window).height() - 120;
			var winMoney = res.winMoney, openHm = res.lotteryHaoMa;
			if (!winMoney) {
				winMoney = '0.00';
			}
			if (!openHm) {
				openHm = '? + ? + ? = ?';
			} else {
				var split = openHm.split(",");
				var sum = parseInt(split[0]) + parseInt(split[1])
						+ parseInt(split[2]);
				openHm = '' + split[0] + '+' + split[1] + '+' + split[2] + '='
						+ sum + '';
				openHm += '' + (sum > 13 ? '大' : '小') + ','
						+ (sum % 2 == 0 ? '双' : '单') + '';
			}
			var html = '<header><em onclick="layer.closeAll();">返回</em><h1>投注详情</h1></header>';
			html += '<section style="overflow-x:auto;position:absolute;height:'+height+'px;"><div class="details"><p>注单号：'
					+ res.orderId
					+ '</p><p>投注期号：'
					+ res.qiHao
					+ '</p><p>投注时间：'
					+ res.createTime + '</p>';
			html += '<p>投注金额：' + res.buyMoney + '元宝</p><p>投注内容：' + res.haoMa
					+ '</p><p>开奖结果：' + openHm + '</p>';
			html += '<p>赔率：' + res.minBonusOdds + '</p><p>状态：'
					+ statusHtml(res.status) + '</p><p>中奖金额：' + winMoney
					+ '元宝</p></div></section>';
			return html;
		}

		function statusHtml(status) {
			switch (status) {
			case 1:
				status = '未开奖';
				break;
			case 2:
				status = '已中奖';
				break;
			case 3:
				status = '未中奖';
				break;
			case 4:
				status = '已撤单';
				break;
			case 5:
				status = '派奖回滚';
				break;
			case 6:
				status = '回滚异常';
				break;
			case 7:
				status = '开奖异常';
				break;
			case 8:
				status = '和局';
				break;
			}
			return status;
		}
		
		function initMoneyType() {
			var data = [];
			for ( var key in moneyType) {
				if (10 < key && key < 15) {
					continue;
				}
				if ("off" == '${isZrOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isDzOnOff}' && 14 < key && key < 16) {
					continue;
				}
				if ("off" == '${isCpOnOff}' && 129 < key && key < 134) {
					continue;
				}
				if ("off" == '${isLhcOnOff}' && 139 < key && key < 143) {
					continue;
				}
				if ("off" == '${isTyOnOff}' && 200 < key && key < 205) {
					continue;
				}
				if ("off" == '${isExChgOnOff}' && 18 < key && key < 21) {
					continue;
				}

				data.push({
					"id" : key,
					"name" : moneyType[key]
				})
			}
			var eachdata = {
				"data" : data
			};
			var html = template('moneytype_tpl', eachdata);
			$("#moneyType").append(html);
		}
	</script>
</body>
</html>