var PayChange = {};

PayChange.online = function(){
	return{
		onlineSub:function(source){
			var $from = $(".bankCard2"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),
	        payId = paySetting.val(),$input=$it.parent().parent().find("input[type=number]");
	        amount1 =$input.val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	$it.removeAttr("disabled");
	        	ToBase.layerMsg("请选择支付方式");return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	$input.focus();
	        	$it.removeAttr("disabled");
	        	ToBase.layerMsg("请输入充值正确金额");
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	$input.focus();
	        	$it.removeAttr("disabled");
	        	ToBase.layerMsg("充值金额必须不小于"+ min);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	$input.focus();
	        	$it.removeAttr("disabled");
	        	ToBase.layerMsg("充值金额必须不大于"+ max);
	            return false;
	        }
	        var iconCss = paySetting.attr("iconcss");
	        var payType = paySetting.attr("pays_type");
	        PayChange.online.dptcommit(amount1,payId,iconCss,payType);
	        return false;
		},
		dptcommit:function(m,payId,iconCss,payType){
			topay(m, payId, iconCss, payType, function(result){
				if(result.success == false){
					ToBase.layerMsg(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toOnlinePayTemplate', result.data);
						layer.open({
						    type: 1
						    ,content: html
						    ,anim: 'up'
						    ,style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
						  });
					}else{
						ToBase.layerMsg("系统发生错误");
					}
				}
			});
		}
	}
}();

PayChange.fast = function(){
	return {
		fastSub:function(source){
			var $from = $(".bankCard2"),$it=$(source),paySetting = $from.find("input[name=radio]:checked"),
	        payId = paySetting.val(),$input=$it.parent().parent().find("input[type=number]"),
	        amount1 =$input.val(),
	        depositor = $it.parent().parent().find("#payAccountName").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	 $it.removeAttr("disabled");
	        	ToBase.layerMsg("请选择支付方式");return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	ToBase.layerMsg("请输入充值正确金额");
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	ToBase.layerMsg("充值金额必须不小于"+ min);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	ToBase.layerMsg("充值金额必须不大于"+ max);
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        if(!depositor){
	        	ToBase.layerMsg("请输入存款帐号");
	        	$it.removeAttr("disabled");
	        	return false;
	        }
	        PayChange.fast.dptcommit(amount1,depositor,payId);
	        return false;
		},
		dptcommit:function(m,ur,payId){
			$.ajax({
				url : Mobile.base+"/m/ajax/dptcommitb.do",
				data : {
					money : m,
					depositor : ur,
					payId : payId
				},
				success : function(result) {
					
					layer.open({
					    content: '充值申请已提交，请尽快完成充值',
					    btn: '我知道了',
					    yes:function(index){
					    	var fast = result.fast;
							fast.money = m;
							fast.orderNo = result.orderNo;
							fast.payName = result.payName;
							var html = template('toFastPayTemplate',fast);
							layer.open({
							    type: 1,
							    content: html,
							    anim: 'up',
							    style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
							 });
							layer.close(index);
					    }
					  });
				}
			});
		}
	}
}();


PayChange.bank = function(){
	return {
		bankSub:function(source){
			var $from = $(".bankCard2"),$it=$(source),bank = $from.find("input[name=radio]:checked"),
	         bankId =bank.val(),
	         $input=$it.parent().parent().find("input[type=number]"),
	         amount1 =$input.val(),
	         depositor = $it.parent().parent().find("#bankAccountName").val(),
	         min = bank.attr("min");
			 max = bank.attr("max");
	    	 $it.attr("disabled","disabled");
	    	 if(!bankId){
	    		 $it.removeAttr("disabled");
	    		 ToBase.layerMsg("请选择支付银行");return false;
	         }
	         try{
	             min = parseFloat(min,10);
	         }catch(e){min = 0;}
	         try{
	             max = parseFloat(max,10);
	         }catch(e){max = 0;}
	         if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	 ToBase.layerMsg("请输入充值正确金额");
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         amount1 = parseFloat(amount1,10);
	         if(min>0 && amount1<min){
	        	 ToBase.layerMsg("充值金额必须不小于"+ min);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(max>0 && amount1>max){
	        	 ToBase.layerMsg("充值金额必须不大于"+ max);
	        	 $it.removeAttr("disabled");
	             return false;
	         }
	         if(!depositor) {
	        	ToBase.layerMsg("请输入存款人姓名");
	        	$it.removeAttr("disabled");
	            return false;
	        }
	        PayChange.bank.dptcommit(amount1,depositor,bankId);
	        return false;
		},
		dptcommit:function(m,ur,bankId){
			$.ajax({
				url : Mobile.base+"/m/ajax/dptcommitc.do",
				data : {
					money : m,
					depositor : ur,
					bankId : bankId
				},
				success : function(result) {
					layer.open({
					    content: '充值申请已提交，线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！',
					    btn: '我知道了',
					    yes:function(index){
					    	var bank = result.bank;
					    	bank.money = m;
					    	bank.orderNo = result.orderNo;
					    	bank.payName = result.payName;
							var html = template('toBankPayTemplate',bank);
							layer.open({
							    type: 1,
							    content: html,
							    anim: 'up',
							    style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
							 });
							layer.close(index);
					    }
					  });
				}
			});
		}
	}
}();