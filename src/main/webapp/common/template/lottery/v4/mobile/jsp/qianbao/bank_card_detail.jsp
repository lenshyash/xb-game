<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<script type="text/javascript" src="${base}/common/template/lottery/v4/mobile/js/jquery.form.min.js"></script>
<title>绑定新的银行卡</title>
<style type="text/css">
	.msg_error{
		background-color:#ffe7e7;
	}
</style>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>绑定新的银行卡</h1>
	</header>
	<section>
		<div class="modify bgFF">
			<form method="post" id="bankCardForm" action="${base}/m/toQb/toBankCard.do?type=3">
			<div class="bankCard2">
				<dl>
					<dt>
						<i class="icoChika"></i>请绑定持卡人本人的银行卡<em style="color: #ff4f4f">*</em>
					</dt>
					<dd>
						<ul class="onLinePad">

							<li><label for="money">持卡人：<input name="cardName"
									type="text" value="" >
							</label></li>
							<li><label for="money1">卡号：
									<input name="cardNo" type="number">
							</label></li>
						</ul>
					</dd>
				</dl>
			</div>
			<div class="button">
				<button id="submitBtn">下一步</button>
			</div>
			</form>
		</div>
	</section>
	<script type="text/javascript">
		$(function(){
			$("#submitBtn").click(function(){
				if(!vaildateTrue()){
					return false;
				}
				$("#bankCardForm").submit();
			})
		})
		
		function vaildateTrue(){
			var cardName = $("input[name=cardName]"),cardNo = $("input[name=cardNo]"),nameReg = /^[\u4e00-\u9fa5]+$/,cardReg=/^([1-9][\d]{1,18}|x|X)$/;
			if(!cardName.val()){
				ToBase.layerMsg("请输入持卡人");
				cardName.addClass("msg_error").focus();
				return false;
			}
			if(!nameReg.test(cardName.val())){
				ToBase.layerMsg("持卡人格式不正确");
				cardName.addClass("msg_error").focus();
				return false;
			}
			if(!cardNo.val()){
				ToBase.layerMsg("请输入卡号");
				cardNo.addClass("msg_error").focus();
				return false;
			}
			if(!cardReg.test(cardNo.val())){
				ToBase.layerMsg("卡号格式不正确");
				cardNo.addClass("msg_error").focus();
				return false;
			}
			return true;
		}
	</script>
</body>
</html>