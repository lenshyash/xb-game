<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html><head lang="en">
    <meta charset="UTF-8">
    <meta content="target-densitydpi=320,width=750,user-scalable=no" name="viewport">
    <meta content="no" name="apple-touch-fullscreen">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>${title}</title>
    <link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
	<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
<header>
    <em data-href="${base}/m/toQb.do">返回</em>
    <h1>${title}</h1>

</header>
<section>
    <div class="openComplete">
        <img src="${base}/common/template/lottery/v4/mobile/images/ico-complete.png" style="display: inline-block;">
        <p>${content}</p>
    </div>
    <c:if test="${not empty member && not empty member.cardNo}">
	    <div class="information">
	        <p>${member.bankName}</p>
	        <c:set value="${fn:length(member.cardNo) }" var="cardNO"></c:set>
	    	<p style="text-align: right; font-size: 34px;"><c:forEach begin="1" end="${cardNO-4}">*</c:forEach> ${fn:substring(member.cardNo,cardNO-4,cardNO)}</p>
	    </div>
    </c:if>
    <c:choose>
    	<c:when test="${not empty status && status eq 'login'}">
    		<div class="button"><button data-href="${base}/m/toUser.do">完 成</button></div>
    	</c:when>
    	<c:otherwise>
    		<div class="button"><button data-href="${base}/m/toQb.do">完 成</button></div>
    	</c:otherwise>
    </c:choose>
</section>
</body></html>