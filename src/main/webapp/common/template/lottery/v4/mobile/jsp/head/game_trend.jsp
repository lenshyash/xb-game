<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>${_title}_开奖走势</title>
<jsp:include page="../include/base.jsp"></jsp:include>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>开奖走势</h1>
	</header>
	<section>
		<div class="teamWarp">
			<div class="teamSearchW teamSearchW2">
				<div class="teamSearch">
					<em></em> <input type="text" class="inputDate"
						placeholder="开奖时间：今天"> <input name="date" type="date"
						placeholder="开奖时间：今天" onchange="getData()"
						style="position: absolute;" max="${nowTime}">
				</div>
				<div class="teamSearch">
					<input type="text" class="sel" value="幸运28"> <select
						class="select" name="type" onchange="getData()">
						<c:forEach items="${bcList}" var="bc">
						<option value="${bc.code}">${bc.name}</option>
					</c:forEach>
					</select> <em></em>
				</div>
			</div>
			<table class="kjzs">
				<thead>
					<tr>
						<th width="15">期号</th>
						<th width="9">值</th>
						<th width="9">大</th>
						<th width="9">小</th>
						<th width="9">单</th>
						<th width="9">双</th>
						<th width="10">大单</th>
						<th width="10">大双</th>
						<th width="10">小单</th>
						<th width="10">小双</th>
					</tr>
				</thead>
				<tbody id="record_list">
				</tbody>
			</table>
		</div>
	</section>
	<script type="text/javascript">
		var start_time,lottery_name;
		$(function(){
			getData();
		})
		function getData(){
			start_time = $("input[name=date]").val();
	        lottery_name = $("select option:selected").text(); 
	       	$(".sel").val(lottery_name);
	       	$(".inputDate").val(start_time);
	       	getAjaxData();
		}
		function getAjaxData(){
			$.ajax({
				url:'${base}/m/ajax/openAward.do',
				data:{time:start_time,lotCode:$("select option:selected").val()},
				success:function(res){
					var result= '';
					if(res.list){
					$(".teamSearchW").removeClass("zhanwei");
					$.each(res.list,function(i,j){
						var timer = new Date(j.endTime);
						result += '<tr><td>'+j.qiHao+'</td>';
						if(!(j.haoMa)){
							result += '<td><label>-</label></td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>';
						}else{
							var hm = j.haoMa.split(","),sum=0;
							for(var i=0;i<hm.length;i++){
								sum += parseInt(hm[i]);
							}
							//间隔
							result += '<td><label>'+sum+'</label></td>';
							if(sum>13){
								result += '<td><em class="colorDan" style="background:#175AAE ">大</em></td><td></td>';
							}else{
								result += '<td></td><td><em class="colorDan" style="background:#DC5D55 ">小</em></td>';
							}
							if(sum%2!=0){
								result += '<td><em class="colorDan" style="background:#175AAE ">单</em></td><td></td>';
							}else{
								result += '<td></td><td><em class="colorDan" style="background:#DC5D55 ">双</em></td>';
							}
							if(sum>13&&sum%2!=0){
								result += '<td><em class="colorDan" style="background:#175AAE ">大单</em></td><td></td>';
							}else if(sum>13&&sum%2==0){
								result += '<td></td><td><em class="colorDan" style="background:#175AAE ">大双</em></td>';
							}else{
								result += '<td></td><td></td>';
							}
							if(sum<14&&sum%2!=0){
								result += '<td><em class="colorDan" style="background:#DC5D55 ">小单</em></td><td></td>';
							}else if(sum<14&&sum%2==0){
								result += '<td></td><td><em class="colorDan" style="background:#DC5D55 ">小双</em></td>';
							}else{
								result += '<td></td><td></td>';
							}
							result += '</tr>';
						}
					})
					}else{
						$(".teamSearchW").addClass("zhanwei");
					}
					$("#record_list").html(result);
				}
			})
		}
	</script>
</body>
</html>