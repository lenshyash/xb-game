<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>首页</title>
<meta content="target-densitydpi=320,width=800,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" type="text/css"
	href="${base}/common/template/lottery/v4/mobile/css/swiper-3.4.1.min.css" />
<link rel="stylesheet"
	href="${base}/common/template/lottery/v4/mobile/css/style.css" />
<jsp:include page="include/base.jsp"></jsp:include>
<style>
.tiaozhuan-left-up>a {
	position: relative;
}

.myXiaoxiRed:before {
	content: ' ';
	display: block;
	clear: both;
	width: 12px;
	height: 12px;
	background-color: red;
	border-radius: 50%;
	position: absolute;
	right: 10px;
	top: 16px;
}
</style>
</head>
<body>
	<jsp:include page="include/header.jsp"></jsp:include>
	<section>
		<input type="hidden" value="${loginMember.money}" id="userNowMoney" />
		<div class="horn">
			<span style="float: left;font-size:22px;">公告：</span>
			<marquee>
				<div class="gonggao" style="height: 57px; font-size: 1.5em;">${htmlGG }</div>
			</marquee>
		</div>
		<div class="shouye-body-banner">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<c:forEach items="${lunbo}" var="lun">
						<div class="swiper-slide">
							<a class="shouye-sw-box" href="${lun.titleUrl}"><img alt="${lun.title}"
								src="${lun.titleImg}" /></a>
						</div>
					</c:forEach>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
		<div class="index_shuom">
			<div class="index_shuom_left">
				<ul>
					<li><i></i><span>用户已赚&nbsp;&nbsp;&nbsp;${yonghuyizhuan}元宝</span></li>
					<li><i></i><span>注册人数&nbsp;&nbsp;&nbsp;${zhucerenshu}人</span></li>
				</ul>
			</div>
			<div class="index_shuom_right"><span>98</span>%<font>赚钱率</font></div>
		</div>
		<div class="indexContent clearfix">
			<img alt="" src="${base}/common/template/lottery/v4/mobile/images/indexBg.png">
			<div class="indexTab">
				<ul style="display:flex;width:100%;">
					<c:forEach items="${bcList}" var="bl" varStatus="blIndex">
						<li class="${blIndex.first?'active':''} ${blIndex.last?'lastMargin':''}">${bl.name}</li>
					</c:forEach>
					<%--<li class="active">幸运28</li>
					<li class="lastMargin">加拿大28</li> --%>
				</ul>
			</div>
			<div class="tabContent">
				<c:forEach items="${bcList}" var="bl" varStatus="blIndex">
					<div class="tabC" ${blIndex.first?'style="display:block;"':'style="display:none;"'}>
						<c:if test="${roomMap[bl.code].size()==0}">
							<div style="height:400px;line-height: 400px;text-align: center;font-size: 58px;">该彩种无房间号</div>
						</c:if>
						<c:if test="${roomMap[bl.code].size()>0}">
						<ul>
							<c:forEach items="${roomMap[bl.code]}" var="room" varStatus="roomIndex">
							<c:choose>
								<c:when test="${roomIndex.last && roomIndex.index % 2 == 0}">
									<li style="width:100%" toLogin="${isLogin}" status="${not empty room.argot?'1':'0'}" room_id="${room.id}" lot_type="${room.lotType}" lower="${room.levelId}" upper="${room.maxMoney}" ${roomIndex.index%2!=0?'class="lastMargin"':''}>
										<img alt="${room.brief}" src="${base}${room.roomImg}">
										<p>在线人数<em id="onlineCount">${not empty room.argot?'保密':room.initialCount}</em></p>
									</li>
								</c:when>
								<c:otherwise>
									<li toLogin="${isLogin}" status="${not empty room.argot?'1':'0'}" room_id="${room.id}" lot_type="${room.lotType}" lower="${room.levelId}" upper="${room.maxMoney}" ${roomIndex.index%2!=0?'class="lastMargin"':''}>
										<img alt="${room.brief}" src="${base}${room.roomImg}">
										<p>在线人数<em id="onlineCount">${not empty room.argot?'保密':room.initialCount}</em></p>
									</li>
								</c:otherwise>
							</c:choose>
							</c:forEach>
						</ul>
						</c:if>
					</div>
				</c:forEach>
			</div>
		</div>
	</section>
	<jsp:include page="include/footer.jsp"></jsp:include>
	<div class="popup" style="display:none;">
	    <div class="config">
	        <h3>请输入房间暗号</h3>
	        <i class="configClose"></i>
	        <div class="configInput"><input type="text" placeholder="" name="secret_pwd" autocomplete="off"></div>
	        <div class="button" style="margin-top: 30px;"><button id="secretBtn">提交</button></div>
	
	    </div>
	</div>
	<div class="popupAlert" style="display:none;">
		<div class="popup_header"><img class="animated fadeInDownBig" src="${base}/common/template/lottery/v4/mobile/images/ico_kefu.png"/></div>
		<div class="popup_top"><img class="animated fadeInLeftBig" src="${base}/common/template/lottery/v4/mobile/images/tishi/index_ts_1.png"> </div>
		<div class="popup_footer"><img class="animated fadeInRightBig" src="${base}/common/template/lottery/v4/mobile/images/tishi/index_ts_2.png"> </div>
	</div>
	<%-- <div class="fixed-erro erro1 errorContent"
		style="display: none; background-color: rgba(0, 0, 0, .5);">
		<div class="fixed-erro-box animated  fadeInUpBig-hastrans fadeOutDownBig-hastrans">
			<p>提示</p>
			<p class="errorContent_t"></p>
			<a class="erro-btn" href="javascript:;">确定</a>
		</div>
	</div>--%>
</body>
<script
	src="${base}/common/template/lottery/v4/mobile/js/swiper-3.4.1.jquery.min.js"></script>
<script>
	var swiper = new Swiper('.swiper-container', {
		pagination : '.swiper-pagination',
		autoplay : 3000,
		autoplayDisableOnInteraction : false,
		loop : true
	});

	$(".left-up-img1, .left-up-img2").click(function() {
		$(".left-up").slideUp(300)
	})
	$(".caozuo-btn").click(function() {
		$(".left-up").slideDown(300)
	})

	$(".choujiang-btn2").click(function() {
		$(".erro3").addClass("displayNone")
	})
	
	var status = "${status}";
	$(function(){
		if(status && status == 1){
			$(".popup").show();
		}else if(status == 2){
			ToBase.layerMsg("房间口令错误");
		}else if(status == 3){
			var message = "${errorMsg}";
			ToBase.layerMsg("该房间元宝限额"+message+"以上");
		}else if(status == 4){
			ToBase.layerMsg("您所拥有的会员等级不符合房间所规定的等级");
		}
		if(!ToBase.utils.getCookieData("isThisOneLogin")){
			$(".popupAlert").show();
			ToBase.utils.cookieData("isThisOneLogin",true,7);
		}
		
	})
</script>
<%--$.ajax({
			url : "${base}/getConfig/getArticle.do",
			data : {code : 13},
			type : "post",
			dataType : 'json',
			success : function(r) {
				var col = '';
				for (var i = 0; i < r.length; i++) {
					if(i==0){
						col += r[i].content + "&nbsp;&nbsp;&nbsp;";
					}
				}
				$(".gonggao").html(col);
			}
		}); --%>
</html>
