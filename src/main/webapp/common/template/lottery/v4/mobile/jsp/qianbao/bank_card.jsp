<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html><head lang="en">
    <meta charset="UTF-8">
    <meta content="target-densitydpi=320,width=750,user-scalable=no" name="viewport">
    <meta content="no" name="apple-touch-fullscreen">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>${_title}_${not empty member.cardNo && not empty member.bankName?'银行卡管理':'绑定新的银行卡'}</title>
    <link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
	<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
<header>
    <em onclick="history.go(-1)">返回</em>
    <h1>${not empty member.cardNo && not empty member.bankName?'银行卡管理':'绑定新的银行卡'}</h1>
</header>
<section>
	<c:if test="${empty member.cardNo && empty member.bankName}">
    <div class="bankCard">
        <div class="binding" data-href="${base}/m/toQb/toBankCard.do?type=2"><i class="icoAdd1"></i>绑定新的银行卡</div>
    </div>
    </c:if>
    <c:if test="${not empty member.cardNo && not empty member.bankName}">
    <div class="details">
        <p>${member.bankName}（储蓄卡）</p>
        <c:set value="${fn:length(member.cardNo) }" var="cardNO"></c:set>
        <p><c:forEach begin="1" end="${cardNO-4}">*</c:forEach> ${fn:substring(member.cardNo,cardNO-4,cardNO)}</p>
    </div>
    <div class="bankCard6"><i class="icoXiugai"></i>如需更改银行卡信息，请联系客服。</div>
    <div class="button" style="margin-top: 100px"><button class="bgCol" data-href="${base}/m/toQb/toDrawCommit.do">我要提现</button></div>
    </c:if>
</section>
</body></html>