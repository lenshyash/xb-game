<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<jsp:include page="../include/base.jsp"></jsp:include>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<title>${_title}_玩法介绍</title>
<style type="text/css">
.layui-m-layerchild{background-color: #efefef;}
.details {
    margin: 20px;
    background: #FFF;
    border-radius: 10px;
    padding: 10px;
    min-height: 300px;
    text-align: left;
    border:none;
}
.details ul li{
	font-size: 20px;
    color: #242424;
    line-height: 40px;
    text-align: left;
}
.awardList{
width:100%;}
</style>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>玩法介绍</h1>

	</header>
	<section>
		<div class="my">
			<ul>
				<c:forEach items="${bcList}" var="bc" varStatus="bcIndex">
					<li code="${bc.code}"><label class="ico_${bc.code}">${bc.name}</label></li>
				</c:forEach>
			</ul>
		</div>
	</section>
	<script type="text/javascript">
		$(function(){
			var height = $(window).height()-120;
			$(".my ul li").click(function(){
				var t = $(this),code=t.attr("code");
				$.get("${base}/common/template/lottery/v4/mobile/jsp/lotInclude/rule_code_"+code+".html?v=12",function(res){
					res = res.replace('1000px',height+"px");
					layer.open({
					    type: 1,
					    content: res,
					    anim: 'up',
					    style: 'bottom:0; left:0; width: 100%; height: 100%; padding:10px 0; border:none;'
					  });
				},"html");
			})
		})
	</script>
</body>
</html>