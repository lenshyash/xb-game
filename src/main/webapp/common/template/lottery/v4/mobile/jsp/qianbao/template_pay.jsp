<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/html" style="display: none;" id="toOnlinePayTemplate">
<header>
		<em onclick="layer.closeAll();">返回</em>
		<h1>充值</h1>
</header>
<form action="{{formAction}}" method="post" id="onlinePayForm">
	{{each formHiddens as item}}
		<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}
	<ul class="popup_template_ul topay">
			<li>
				<span class="ititle">会员帐号： </span><span>{{account}}</span>
			</li>
			<li>
				<span class="ititle">订&nbsp;单&nbsp;号： </span><span>{{orderId}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{amount}}</span>
			</li>
		</ul>
		<span class="button">
			<button class="colorDa" type="submit">确定送出</button>
		</span>
</form>
</script>

<script type="text/html" style="display: none;" id="toFastPayTemplate">
<header>
		<em onclick="layer.closeAll();">返回</em>
		<h1>充值</h1>
</header>
<ul class="popup_template_ul topay">
			<li>
				<span class="ititle">充值方式： </span><span>{{payName}}</span>
			</li>
			<li>
				<span class="ititle">收款姓名： </span><span>{{payUserName}}</span>
			</li>
			<li>
				<span class="ititle">收款帐号： </span><span>{{payAccount}}</span>
			</li>
			<li>
				<span class="ititle">订&nbsp;&nbsp;单&nbsp;&nbsp;号： </span><span>{{orderNo}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{money}}</span>
			</li>
			<li style="height:15rem;line-height:15rem;">
				<span class="ititle">二维码： </span><span><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 15rem; height: 15rem;display:inline-block;"></span>
			</li>
			<li>
				<div style="font-size: 1.4rem;width: 92%;margin:0 auto;color:red;line-height:3rem;">贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</div>
			</li>
		</ul>
		<span class="button" style="display:inline-block;margin:0 5%;width:90%;">
			<button class="colorDa" >前往查看充值记录</button>
		</span>
</script>


<script type="text/html" style="display: none;" id="toBankPayTemplate">
<header>
		<em onclick="layer.closeAll();">返回</em>
		<h1>充值</h1>
</header>
<ul class="popup_template_ul topay">
			<li>
				<span class="ititle">充值银行： </span><span>{{payName}}</span>
			</li>
			<li>
				<span class="ititle">收款姓名： </span><span>{{creatorName}}</span>
			</li>
			<li>
				<span class="ititle">收款帐号： </span><span>{{bankCard}}</span>
			</li>
			<li>
				<span class="ititle">开户网点： </span><span>{{bankAddress}}</span>
			</li>
			<li>
				<span class="ititle">订&nbsp;&nbsp;单&nbsp;&nbsp;号： </span><span>{{orderNo}}</span>
			</li>
			<li>
				<span class="ititle">充值金额： </span><span>{{money}}</span>
			</li>
			<li>
				<div style="font-size: 1.4rem;width: 92%;margin:0 auto;color:red;line-height:3rem;">贴心提醒：充值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</div>
			</li>
		</ul>
		<span class="button" style="display:inline-block;margin:0 5%;width:90%;">
			<button class="colorDa">前往查看充值记录</button>
		</span>
</script>