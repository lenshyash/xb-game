<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="cpt-dw-dialog-mask animated"
	style="background: rgba(0, 0, 0, 0.6); z-index: 10000010; transition: all 0.3s; animation-duration: 0.3s;"></div>
<div class="div_dialog animated danger"
	style="width: 70%; height: auto; background: rgb(255, 255, 255); border-radius: 4px; padding: 10px 16px; z-index: 10000011; animation-duration: 0.3s; transform: translate(-50%, -50%);">
	<div class="title_dialog">提示</div>
	<div class="discription_dialog">也许有点问题!</div>
	<div class="dialog_divOperation">
		<span class="btn_span" data-name="确定">确定</span>
	</div>
</div>