$(function(){
	ToBase.verifyImg();
});


var ToLogin = {
		validateLoginInput:function(){
			var uN = $(".denglu-fix input[name=userName]"),pW = $(".denglu-fix input[name=passWord]"),vC = $(".denglu-fix input[name=verifyCode]"),data={};
			if(!uN.val()){
				ToBase.layerMsg('用户名不能为空');
				return;
			}
			if(!pW.val()){
				ToBase.layerMsg('密码不能为空');
				return;
			}
			if(!uN.val() && uN.val().length <4){
				ToBase.layerMsg('用户名不能小于4位');
				return;
			}
			if(!pW.val() && pW.val().length<6){
				ToBase.layerMsg('密码不能小于6位');
				return;
			}
			if(!vC.val() && $("#verifyDiv").is(":visible")){
				ToBase.layerMsg("验证码不能为空");
				return;
			}
			data['account'] = uN.val();
			data['password'] = pW.val();
			data['verifyCode'] = vC.val();
			layer.open({type: 2});
			ToLogin.submit.userLoginSub(data);
			return true;
		},
		validateRegisterInput:function(url){
			var account = $(".denglu-fix input[name=account]"),pwd = $(".denglu-fix input[name=password]"),cPwd = $(".denglu-fix input[name=rPassword]"),verifyCode=$(".denglu-fix input[name=verifyCode]")
			if(!account.val()){
				ToBase.layerMsg('用户名不能为空');
				return;
			}
			if(!pwd.val()){
				ToBase.layerMsg('密码不能为空');
				return;
			}
			if(!cPwd.val()){
				ToBase.layerMsg('确认密码不能为空');
				return;
			}
			if(pwd.val() != cPwd.val()){
				ToBase.layerMsg('两次密码输入不一致');
				return;
			}
			if(!verifyCode.val()){
				ToBase.layerMsg("验证码不能为空");
				return;
			}
			var status = $("#registerStatus").val();
			if(!status){
				var data = getCommitData();
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				layer.open({type: 2});
				ToLogin.submit.userRegisterSub(data,url);
			}else if(status == 'testGuest'){	//试玩
				var data = {};
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				layer.open({type: 2});
				ToLogin.submit.userRegisterSub(data,url);
			}else{	//代理
				var data = getCommitData();
				data['account'] = account.val();
				data['password'] = pwd.val();
				data['rpassword'] = cPwd.val();
				data['verifyCode'] = verifyCode.val();
				layer.open({type: 2});
				ToLogin.submit.userRegisterSub(data,url);
			}
		}
}


ToLogin.submit = function(){
	return {
		userLoginSub:function(data){
			$.ajax({
				url:Mobile.base + "/m/ajax/login.do",
				data:data,
				success:function(res){
					layer.closeAll();
					if(res.success){
						//记录cookie
						var rember = $("#remeberJZ input").prop("checked");
						if(rember){
							ToLogin.submit.setCookieData(data['account'],data['password']);
						}else{
							ToLogin.submit.setCookieData('','');
						}
						//登录之后随机分配头像缓存cookie
						ToLogin.submit.cookieData('account_'+data['account']+'_touxiang');
						window.location.href = Mobile.base + '/m/index.do';
					}else{
						ToBase.layerMsg(res.msg);
						ToBase.verifyImg();
						$("#verifyDiv").show().find("input").val('');
					}
				}
			})
		},
		userRegisterSub:function(data,url){
			var flag = !url;
			url = url || (Mobile.base + '/registerTestGuest.do');
			$.ajax({
				url:url,
				data : {data:JSON.encode(data)},
				success:function(res){
					layer.closeAll();
					if(res.success){
						//游客的话就不保存多天。
						if(flag){
							$.cookie('account_'+data['account']+'_touxiang',ToBase.utils.randomNum(111));
						}else{
							ToLogin.submit.cookieData('account_'+data['account']+'_touxiang');
						}
						window.location.href = Mobile.base + '/m/index.do';
					}else{
						ToBase.layerMsg(res.msg);
						ToBase.verifyImg();
					}
				}
			})
		},
		cookieData:function(key){
			if(!$.cookie(key)){
				$.cookie(key,ToBase.utils.randomNum(111),{expires:7});
			}
		},
		setCookieData:function(account,pswd){
			$.cookie("account",account,{expires:7,path:'/'});
			$.cookie("password",pswd,{expires:7,path:'/'});
		},
		getCookieData:function(){
			var account = $.cookie("account");
			var password = $.cookie("password");
			$(".denglu-input input[name=userName]").val(account);
			$(".denglu-input input[name=passWord]").val(password);
		}
	}
}();