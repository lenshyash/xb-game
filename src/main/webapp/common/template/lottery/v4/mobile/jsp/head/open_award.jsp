<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<title>${_title}开奖结果</title>
<jsp:include page="../include/base.jsp"></jsp:include>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<style type="text/css">
	.shuzi_01,.shuzi_04,.shuzi_07,.shuzi_10,.shuzi_16,.shuzi_19,.shuzi_22,.shuzi_25{background:#00DB00;}
	.shuzi_02,.shuzi_05,.shuzi_08,.shuzi_11,.shuzi_17,.shuzi_20,.shuzi_23,.shuzi_26{background:#0000E3;}
	.shuzi_03,.shuzi_06,.shuzi_09,.shuzi_12,.shuzi_15,.shuzi_18,.shuzi_21,.shuzi_24{background:#FF0000;}
	.shuzi_00,.shuzi_13,.shuzi_14,.shuzi_27{background: #ccc;}
	.prizeResult font{
		display: inline-block;
		width:48px;
		height:48px;
		border-radius:50%;
		font-size: 32px;
	    text-align: center;
	    line-height: 48px;
	    color:#fff;
	}
</style>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>开奖结果</h1>
	</header>
	<section>
		<div class="teamSearchW teamSearchW2">
			<div class="teamSearch">
				<input type="text" class="inputDate" placeholder="开奖时间：今天">
				<input name="date" type="date" placeholder="开奖时间：今天"
					onchange="getData()" max="${nowTime}"> <em></em>
			</div>
			<div class="teamSearch">
				<input type="text" class="sel" value=""> <select
					class="select" name="type" onchange="getData()">
					<c:forEach items="${bcList}" var="bc">
						<option value="${bc.code}">${bc.name}</option>
					</c:forEach>
				</select><em></em>
			</div>
		</div>
		<div class="prize">
			<ul id="record_list">
			</ul>
		</div>
	</section>
	<script type="text/javascript">
		var start_time,lottery_name;
		$(function(){
			getData();
		})
		function getData(){
			start_time = $("input[name=date]").val();
	        lottery_name = $("select option:selected").text(); 
	       	$(".sel").val(lottery_name);
	       	$(".inputDate").val(start_time);
	       	getAjaxData();
		}
		
		function getAjaxData(){
			$.ajax({
				url:'${base}/m/ajax/openAward.do',
				data:{time:start_time,lotCode:$("select option:selected").val()},
				success:function(res){
					var result= '';
					if(res.list){
						$(".prize").removeClass("zhanwei");
						$.each(res.list,function(i,j){
							var timer = new Date(j.endTime);
							result += '<li><div class="prizeNo"><h3>第'+j.qiHao+'期</h3><span>'+$.formatDate("yyyy-MM-dd hh:mm:ss",timer,0)+'</span></div>';
							if(!(j.haoMa)){
								result += '<div class="prizeResult"><h4><font class="shuzi_00">--</font>+<font class="shuzi_00">--</font>+<font class="shuzi_00">--</font>=<em class="colorH">--</em></h4><span>';
								result += '<em class="colorDan" style="background:#f30700">--</em>';
								result += '<em class="colorDan" style="background:#2371ff ">--</em>';
								result += '<em class="colorDan" style="background:#f30700 ">--</em></span></div></li>';
							}else{
								var hm = j.haoMa.split(","),sum=0,hmHtml='';
								for(var i=0;i<hm.length;i++){
									if(i==2){
										hmHtml += '<font class="shuzi_'+ToBase.betRoom.firstAddZero(hm[i])+'">'+ToBase.betRoom.firstAddZero(hm[i])+'</font>';
									}else{
										hmHtml += '<font class="shuzi_'+ToBase.betRoom.firstAddZero(hm[i])+'">'+ToBase.betRoom.firstAddZero(hm[i])+'</font>+';
									}
									sum += parseInt(hm[i]);
								}
								
								result += '<div class="prizeResult"><h4>'+hmHtml+'=<em class="shuzi_'+ToBase.betRoom.firstAddZero(sum)+'">'+ToBase.betRoom.firstAddZero(sum)+'</em></h4><span>';
								result += '<em class="colorDan" style="background:'+(sum>=14?'#2371ff':'#f30700')+'">'+(sum>=14?'大':'小')+'</em>';
								result += '<em class="colorDan" style="background:'+(sum%2==0?'#f30700':'#2371ff')+'">'+(sum%2==0?'双':'单')+'</em>';
								var anQ='',anB=1;
								if(sum>=15&&sum%2!=0){
									if(sum>=23){
										anQ='极大';
									}else{
										anQ='大单';
										anB=2;
									}
								}else if(sum>=14&&sum%2==0){
									anQ='大双';
								}else if(sum<14&&sum%2==0){
									if(sum<=4){
										anQ='极小'
									}else{
										anQ='小双';
									}
								}else{
									anQ='小单';
									anB=2;
								}
								result += '<em class="colorDan" style="background:'+(anB==1?'#f30700':'#2371ff')+'">'+anQ+'</em></span></div></li>';
							}
						})
					}else{
						$(".prize").addClass("zhanwei");
					}
					$("#record_list").html(result);
				}
			})
		}
	</script>
</body>
</html>