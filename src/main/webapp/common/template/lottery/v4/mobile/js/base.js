//页面加载
var cacheT1;
var betDataInfo = [];
var faceJSon = ["","[微笑]","[撇嘴]","[色]","[发呆]","[得意]","[流泪]","[害羞]","[闭嘴]","[睡]","[大哭]","[尴尬]","[发怒]","[调皮]",
                "[呲牙]","[惊讶]","[难过]","[酷]","[囧]","[抓狂]","[吐]","[偷笑]","[愉快]","[白眼]","[傲慢]","[困]","[惊恐]","[流汗]","[憨笑]",
                "[悠闲]","[奋斗]","[咒骂]","[疑问]","[嘘]","[晕]","[衰]","[骷髅]","[敲打]","[再见]","[擦汗]","[抠鼻]","[鼓掌]","[坏笑]",
                "[左哼哼]","[右哼哼]","[哈欠]","[鄙视]","[委屈]","[快哭了]","[阴险]","[亲亲]","[可怜]","[菜刀]","[西瓜]","[啤酒]","[篮球]",
                "[乒乓球]","[咖啡]","[米饭]","[猪头]","[玫瑰]","[凋谢]","[嘴唇]","[爱心]","[心碎]","[蛋糕]","[雷电]","[炸弹]","[匕首]","[足球]",
                "[爬虫]","[便便]","[月亮]","[太阳]","[盒子]","[抱抱]","[强]","[弱]","[握手]","[胜利]","[抱拳]","[勾引]","[拳头]","[OK]"];
$(function(){
	$('body').bind('touchstart',function(e){
        startX = e.originalEvent.changedTouches[0].pageX;
        startY = e.originalEvent.changedTouches[0].pageY;
    });
    $("#wrapper").bind("touchmove",function(e){
        //获取滑动屏幕时的X,Y
        endX = e.originalEvent.changedTouches[0].pageX;
        endY = e.originalEvent.changedTouches[0].pageY;
        //获取滑动距离
        distanceX = endX-startX;
        distanceY = endY-startY;
        //判断滑动方向
        if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX>0){
        	if($(".customNews").hasClass("xiaolianRoom") || $(".customNews").hasClass("betRoom")){
        		$(".customNews .keyboard").click();
    		}
//        	if($(".headerRight").hasClass("rotate")){
//        		$(".icoAdd").click();
//        	}
        }else if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX<0){
        	if($(".customNews").hasClass("xiaolianRoom") || $(".customNews").hasClass("betRoom")){
        		$(".customNews .keyboard").click();
    		}
//        	if($(".headerRight").hasClass("rotate")){
//        		$(".icoAdd").click();
//        	}
        }else
        if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY<0){
        	if($(".customNews").hasClass("xiaolianRoom") || $(".customNews").hasClass("betRoom")){
        		$(".customNews .keyboard").click();
    		}
//        	if($(".headerRight").hasClass("rotate")){
//        		$(".icoAdd").click();
//        	}
        } else if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY>0){
        	if($(".customNews").hasClass("xiaolianRoom") || $(".customNews").hasClass("betRoom")){
        		$(".customNews .keyboard").click();
    		}
//        	if($(".headerRight").hasClass("rotate")){
//        		$(".icoAdd").click();
//        	}
        }
    });
	
	$("body").css({"opacity":1});
	
	$("*").on("click","[data-href]",function(){
		var t = $(this);
		ToBase.toLocationHref(t.attr("data-href"));
	})
	
	$(".whiteDiv").click(function(){
		$(this).hide();
		if(fag){
			$(".roomHead2").click();
		}
		if($('.quxiao').is(":visible")){
			$(".roomHead #quxiaoXz").click();
		}
	})
	
	$(".icoAdd").click(function(event){
		event.stopPropagation();
		var t = $(this), menu = $(".menu");
		if(t.hasClass("rotate")){
			t.removeClass("rotate");
			menu.hide();
			$(".whiteDiv").hide();
		}else{
			t.addClass("rotate");
			menu.addClass("animated fadeInRight").removeClass("fadeOutRight").show();
			$(".whiteDiv").show();
		}
    })
    $(".menu li").click(function(){
        $(".menu").hide();
    })
    
    $("body").on("click",function(event){
    	event.stopPropagation();
		if($(".icoAdd").hasClass("rotate")){
			$(".icoAdd").click();
		}
	})
	
	$(".popupAlert").on("click",function(){
		$(this).hide();
	})
	
	$(".tabContent").on("click",".tabC ul li",function(){
		var t = $(this),status=t.attr("status"),rId=t.attr("room_id"),lType=t.attr("lot_type"),toLogin = t.attr("toLogin");
		if(toLogin == 'false'){
			ToBase.toLocationHref(Mobile.base + "/toLogin.do");
			return;
		}
		var upper = t.attr("upper");
		if(upper && parseFloat(upper)>parseFloat($("#userNowMoney").val())){
			ToBase.layerMsg("该房间元宝限额"+upper+"以上");
			return;
		}
		//后续添加进入房间条件
		if(status && status == 1){
			//弹窗输入口号
			$(".popup").fadeIn();
			$("input[name=secret_pwd]").attr("r_id",rId).attr("l_type",lType);
			return;
		}
		ToBase.toLocationHref(Mobile.base + "/m/toBetRoom.do?rId="+rId+"&lotType="+lType);
	})
	
	$(".configClose").on("click",function(){
		$(this).parent().parent().css({display:'none'});
		$("input[name=secret_pwd]").val('').removeAttr("r_id");
	});
	
	$(".indexTab").on("click","ul li",function() {
        $(this).addClass("active").siblings().removeClass("active");
        var index = $(this).index()
        $(".tabContent .tabC").eq(index).show().siblings().hide();
    });
	
	//*********************select 下拉列表********************************/
	$(".select").change(function(){
        $(this).siblings(".inputW").text($(this).children('option:selected').text())
    })
	
	
	$("#secretBtn").click(function(){
		var input = $("input[name=secret_pwd]"),argot=input.val();
		if(!argot){
			ToBase.layerMsg('房间口令不能为空');
			return;
		}
		var data = {
				argot:argot,
				roomId:input.attr("r_id")
		}	
		$.ajax({
			url:Mobile.base+'/m/ajax/roomArgot.do',
			data:data,
			success:function(res){
				if(res.success){
					ToBase.toLocationHref(Mobile.base + "/m/toBetRoom.do?rId="+input.attr("r_id")+"&lotType="+input.attr("l_type")+"&argot="+argot);
				}else{
					ToBase.layerMsg(res.msg);
				}
			}
		})
	})
	
	/*最新开奖结果*/
	var fag =false;
    $(".roomHead2").click(function(){
        if(fag){
        	$(this).children("em").css("transform","rotate(0deg)");
        	$(".whiteDiv").hide();
            fag =false;
        }else{
            $(this).children("em").css("transform","rotate(180deg)");
            $(".whiteDiv").show();
            fag =true;
        }
        $(".lottery").toggle();
    })
    
    //弹出隐藏
     $(".customNews").on("click","#bettingBtu", function(){
    	 if(!ToBase.isOpen && $("#kp_jz").text().trim() == '开盘'){
    		 ToBase.layerMsg("期号："+ToBase.qiHao+"已封盘");
    		 return;
    	 }
    	 if(!betStatus()){
    		 ToBase.layerMsg("该房间暂时不能投注");
    		 return false;
    	 }
    	 //如果存在表情展示则先关闭
    	 if($(this).parent().hasClass("xiaolianRoom")){
    		$(this).parent().removeClass("xiaolianRoom");
    		$(this).siblings("img.xiaolian").removeClass("true");
    	 }
    	 $(this).parent().addClass("betRoom");
    	 $("#myBtn").show();
    	 //$(".room").animate({bottom:"310px",top:"45px",scrollTop: $(".roomContent").outerHeight()},300);
    	// $('body').animate({'padding-bottom':'310px','scrollTop': $('body')[0].scrollHeight}, 400);
         $(".bettingKey").css({"opacity":1});
         $(".textArea div").attr("contenteditable",false).empty();
    }).on("click",".keyboard",function(){
    	var t = $(this);
    	if(t.parent().hasClass("betRoom")){
    		t.parent().removeClass("betRoom");
    		$("#myBtn").hide();
    		if($(".textArea div").attr("contenteditable") == "false"){
        		$(".textArea div").attr("contenteditable",true).empty();
        	}
    		betDataInfo = [];	//防止未初始化
    		//$(".room").animate({bottom:"50px",top:"158px",scrollTop: $(".roomContent").outerHeight()},300);
    		//$('body').animate({'padding-bottom':'50px','scrollTop': $('body')[0].scrollHeight}, 400);
    	}else if(t.parent().hasClass("xiaolianRoom") && ToBase.isOpen){
    		t.parent().removeClass("xiaolianRoom");
    		t.siblings("img.xiaolian").removeClass("true");
    		//$(".room").animate({bottom:"50px",scrollTop: $(".roomContent").outerHeight()},300);
    		//$('body').animate({'padding-bottom':'50px','scrollTop': $('body')[0].scrollHeight}, 400);
    	}else{}
    }).on("keyup",".textArea div",function(e){
        e = e? e : (window.event ? window.event : null);
        if(e.keyCode==13)//Enter
        {
            $("#speak").click();
        }
    }).on("click",".xiaolian",function(event){
    	event.stopPropagation();
    	if($(this).hasClass("true")){
    		$(this).removeClass("true");
    		//$(".room").animate({bottom:"50px",scrollTop: $(".roomContent").outerHeight()},300);
    		//$('body').animate({'padding-bottom':'50px','scrollTop': $('body')[0].scrollHeight}, 400);
    		$(".xiaolianList").parent().removeClass("xiaolianRoom");
    	}else{
    		$(this).addClass("true");
    		$(".xiaolianList").parent().addClass("xiaolianRoom");
    		//$(".room").animate({bottom:"288px",top:"45px",scrollTop: $(".roomContent").outerHeight()},300);
    		//$('body').animate({'padding-bottom':'238px','scrollTop': $('body')[0].scrollHeight}, 400);
    		if($(this).attr("data_new") && $(this).attr("data_new") == "data_new"){
    			var swiper2 = new Swiper(".swiper-container2",{
        			pagination : '.swiper-p2',
        		})
    			$(this).removeAttr("data_new");
    		}
    	}
    }).on("click",".xiaolianList img.img_face",function(){
    	var t = $(this),src=t.attr("src");
    	$(".textArea div").append('<img src="'+src+'" />');
    }).on("click",".xiaolianList img.img_delete",function(){
    	var textArea = $(".textArea img");
    	if(textArea.length>0){
    		textArea.eq(-1).remove();
    	}
    }).on("click",".textArea div",function(){
    	var t = $(this);
    	if(t.parent().parent().hasClass("xiaolianRoom")){
    		$(".customNews .keyboard").click();
    	}
    })
    
    /**
     * 投注
     */
    $("#myBtn").on("click","ul li div",function(){
    	if(ToBase.isSuoHa){
    		ToBase.layerMsg('您已经梭哈了');
    		return;
    	}
    	var t = $(this),text=t.find("span").text(),moneyWarp = $(".cenMoneyWarp"),dataDesc=t.attr("data-desc");
    	if(dataDesc){
    		t.parent().parent().parent().find("p.desc").text(dataDesc);
    	}
    	moneyWarp.css({"display":"block"}).find("#touzhuzhi").text(text).attr({"data-code":t.attr("data-code"),"data-id":t.attr("data-id"),"data-min":t.attr("data-min"),"data-max":t.attr("data-max")});
    }).on("click",".peilvshuom button",function(){
    	var t = $(this) , pv = t.siblings().html(),betGT=$(".popup");
    	var pv = '<div id="gentouDiv" style="line-height:25px;font-size:13px;">'+pv+'</div>';
    	betGT.find(".config1").append(pv);
        betGT.fadeIn().find(".config1").addClass("fadeInUpBig");
    })
    
    /**
     * 取消与确认
     */
    $("#butJp").on("click",".cancel",function(){
    	$(".cenMoneyWarp").css({"display":"none"}).find("#touzhuzhi").text('').removeAttr("data-code").removeAttr("data-id").removeAttr("data-max").removeAttr("data-min");
    	$("#inputNumber").val('');
    }).on("click",".queren",function(){
    	var inputM=$("#inputNumber"),warp=$(".cenMoneyWarp").find("h1"),mVal=parseFloat(inputM.val())||0;
    	if(!mVal){
    		ToBase.layerMsg('请输入投注金额');
    		return;
    	}
    	var min = parseFloat(warp.attr("data-min"))||0;
    	var max = parseFloat(warp.attr("data-max"))||0;
    	if(mVal<min){ToBase.layerMsg("投注金额不能低于"+min+"元!");return;}
    	if(mVal>max){ToBase.layerMsg("投注金额不能高于"+max+"元!");return;}
    	$(".textArea div").append('<p data-id="'+warp.attr("data-id")+'" data-code="'+warp.attr("data-code")+'" data-min="'+min+'" data-max="'+max+'" ><label>'+$("#touzhuzhi").text().trim()+'</label>&nbsp;&nbsp;<span>'+inputM.val()+'</span><em onclick="dataClose(this);"></em></p>');
    	$(".cenMoneyWarp").css({"display":"none"}).find("#touzhuzhi").text('');
    	$("#inputNumber").val('');
    })
    
    /**
     * 选取金额
     */
    $(".cenMoneyWarp").on("click",".text-ul li",function(){
    	var t = $(this),isClass=t.attr("class"),warp=$(".cenMoneyWarp").find("h1"),betDiv = $(".textArea div");
    	if(isClass){
    		//梭哈
    		if(betDiv.find("p").length>0){
    			layer.open({
    			    content: '您已存在注单，是否取消现有注单？'
    			    ,btn: ['确定', '取消']
    			    ,yes: function(index){
    			    	ToBase.clearZD();
    			    	betDiv.append('<p data-id="'+warp.attr("data-id")+'" data-code="'+warp.attr("data-code")+'" data-sh="1" data-max="'+warp.attr("data-max")+'"><label>'+$("#touzhuzhi").text().trim()+'</label>&nbsp;&nbsp;<span>梭哈</span><em onclick="dataClose(this);"></em></p>');
    		    		$(".cenMoneyWarp").css({"display":"none"}).find("#touzhuzhi").text('');
    		        	$("#inputNumber").val('');
    		        	ToBase.isSuoHa = true;
    		        	layer.close(index);
    			    }
    			  });
    		}else{
	    		betDiv.append('<p data-id="'+warp.attr("data-id")+'" data-code="'+warp.attr("data-code")+'" data-sh="1" data-max="'+warp.attr("data-max")+'"><label>'+$("#touzhuzhi").text().trim()+'</label>&nbsp;&nbsp;<span>梭哈</span><em onclick="dataClose(this);"></em></p>');
	    		$(".cenMoneyWarp").css({"display":"none"}).find("#touzhuzhi").text('');
	        	$("#inputNumber").val('');
	        	ToBase.isSuoHa = true;
    		}
    	}else{
    		$("#inputNumber").val(t.text());
    	}
    })
    /**
     * 查看该期注单详情
     */
    $(".roomHead").on("click","#quxiaoXz",function(){
    	if(!$('.quxiao').is(":visible")){
    		$(this).children("em").css("transform","rotate(180deg)");
    		$(".whiteDiv").show();
//    		(".quxiao").show();
    	}else{
    		$(this).children("em").css("transform","rotate(0deg)");
    		$(".whiteDiv").hide();
//    		$(".quxiao").hide();
    	}
        $('.quxiao').toggle();
    })
    $(".quxiao").on("click","em",function(){
    	var t=$(this), orderId = t.attr("order-no");
    	layer.open({
    	    content: '您确定要取消该注单吗？'
    	    ,btn: ['确定', '取消']
    	    ,yes: function(index){
    	      $.ajax({
    	    	  url:Mobile.base + '/m/ajax/cancelOrder.do',
    	    	  data:{orderId:orderId,lotCode:ToBase.code},
    	    	  success:function(res){
    	    		  if(res.success){
    	    			  ToBase.layerMsg('取消注单成功');
    	    			  ToBase.betRoom.betCurOrder();
    	    			  window.setTimeout(function(){
    	    				  refreshMoney();
    	    			  },2000)
    	    		  }else{
    	    			  ToBase.layerMsg(res.msg);
    	    		  }
    	    		  layer.close(index);
    	    	  }
    	      })
    	    }
    	  });
    })
    $("#scroller").on("click",".userBetting li.click_gt",function(){
    	 if(!ToBase.isOpen && $("#kp_jz").text().trim() == '开盘'){
    		 ToBase.layerMsg("期号："+ToBase.qiHao+"已封盘");
    		 return;
    	 }
    	var t = $(this),betGT=$(".popup");
    	var nameMz =t.siblings("li").eq(1).text();
        var timeTz =t.parent().siblings("h3").text();
        var gentou ='<div id="gentouDiv"><p style="font-size: 15px; text-align: left;"><em>'+nameMz+'</em>&nbsp;&nbsp;&nbsp;&nbsp;'+timeTz+'<br>第<span id="betQiHao" style="color:red;">'+$('#issue').text()+'</span>期</p><div class="configContent">';
        var genZhi =t.children("p");
        for(var i=0; i<genZhi.length; i++){
            gentou+='<p o_id="'+genZhi.eq(i).attr("o_id")+'" p_code="'+genZhi.eq(i).attr("p_code")+'" style="font-size: 15px; text-align: left;">投注类型：<span>'+genZhi.eq(i).children("span").text()+'</span> <label class="configAcer">金额：<em>'+genZhi.eq(i).children("em").text()+'</em></label></p>'
        }
        gentou+='</div><div class="button" style="margin-top: 30px;"><button id="genTou">跟他投</button></div></div>';
        betGT.find(".config1").append(gentou);
        betGT.fadeIn().find(".config1").addClass("fadeInUpBig");
    })
    
    /**
     * 跟投
     */
    $(".popup").on("click","#genTou",function(){
    	var t = $(this),allMoney = 0,betQiHao = t.parent().siblings("p").find("span").text();
    	t.attr("disabled","disabled");
    	$.each(t.parent().siblings("div.configContent").find("p"),function(i,j){
    		j = $(j);
    		var p_code = j.attr("p_code"),o_id = j.attr("o_id"),name = j.find("span").text().trim(),money = j.find("em").text().trim();
    		if(!p_code || !o_id || !name || !money || !betQiHao){
    			ToBase.layerMsg("投注失败,请刷新页面");
    			return false;
    		}
    		var obj = new Object();
    		obj.money = money;
    		obj.oddsId = o_id;
    		obj.playCode = p_code;
    		obj.name = name;
    		allMoney += parseFloat(money);
    		betDataInfo.push(obj);
    	})
    	var nowMoney = parseFloat($("span.icoAcer").text());
    	if(!betStatus()){
    		ToBase.layerMsg("该房间暂时不能投注");
    		return false;
    	}
    	if(allMoney > nowMoney){
    		ToBase.layerMsg("余额不足，请充值");
    		betDataInfo=[];
    		return false;
    	}
    	ToBase.betRoom.betInfo(betQiHao);
    	$(".configClose1").click();
    }).on("click",".configClose1",function(){
    	$(this).parent().parent().hide().find("#gentouDiv").remove();
    })
    
    //发言
    $("#speak").on("click",function(){
    	var betDiv = $(".textArea div"),isType = betDiv.attr("contenteditable");
    	//betDiv.text('');
    	//ToBase.layerMsg("账户金额低于1000000，不能发言");
    	//return;
    	if(isType != 'true'){
    		ToBase.layerMsg('聊天参数异常，请刷新页面');
    		return ;
    	}
    	var textArea = betDiv.html().trim();
    	var htmlArea = betDiv.text().trim();
    	if(!textArea || textArea.indexOf("img") == -1){
    		if(!htmlArea){
	    		ToBase.layerMsg("消息不能为空");
	    		betDiv.empty();
	    		return false;
    		}
    		textArea = htmlArea;
    	}
    	betDiv.text('');
    	var nowMoney = parseFloat($("span.icoAcer").text());
    	var speakMoney = parseFloat($("#speakMoney").val());
    	if(speakMoney > nowMoney){
    		ToBase.layerMsg("账户金额低于"+speakMoney+",禁止发言");
    		return false;
    	}
    	var data = {
    			"message":textArea,
    			"commandid":1006,
    			"account":Room.account,
    			"nickUrl":$.cookie('account_'+Room.account+'_touxiang')
    	}
    	//InitWebSocket.robotSend(data);
    	ToBase.betRoom.resultMsg(data);
    	//$(".customNews .keyboard").click();
    })
    
    
    //投注
    $("#bettingBtu1").on("click",function(){
    	var betDiv = $(".textArea div"),isType = betDiv.attr("contenteditable"),allMoney=0;
    	if(isType != 'false'){
    		ToBase.layerMsg('投注参数异常,请刷新页面');
    		return ;
    	}
    	if(betDiv.find("p").length==0){
    		ToBase.layerMsg("请添加一组投注号码");
    		return;
    	}
    	var nowMoney = parseFloat($("span.icoAcer").text())||0,isSH=false;
    	$.each(betDiv.find("p"),function(i,j){
    		j = $(j);
    		var code = j.attr("data-code"),id=j.attr("data-id"),max=parseFloat(j.attr("data-max"))||0,name=j.find('label').text().trim(),money=j.find("span").text().trim();
    		if(!code || !id || !name || !money){
    			ToBase.layerMsg('投注异常，请刷新页面重新投注');
    			return;
    		}
    		if(money == '梭哈'){
    			isSH=true;
    			money = nowMoney>max?max:nowMoney;
    		}
    		var obj = new Object();
    		obj.money = money;
    		obj.oddsId = id;
    		obj.playCode = code;
    		obj.name = name;
    		allMoney += parseFloat(money);
    		betDataInfo.push(obj);
    	});
    	if(!betStatus()){
    		ToBase.layerMsg("该房间暂时不能投注");
    		return false;
    	}
    	if(allMoney > nowMoney){
    		ToBase.layerMsg("余额不足，请充值");
    		betDataInfo=[];
    		return false;
    	}
    	if(isSH){
    		//询问框
    		  layer.open({
    		    content: '您确定要梭哈吗？',
    		    btn: ['梭哈', '算了'],
    		    yes: function(index){
    		    	ToBase.betRoom.betInfo(ToBase.qiHao);
    		    	ToBase.isSuoHa = false;
    		    	layer.close(index);
    		    }
    		  });
    	}else{
    		ToBase.betRoom.betInfo(ToBase.qiHao);
    	}
    })
})

function betStatus(){
	var betStatus = parseInt($("#betStatus").val());
	if(betStatus && betStatus == 1){
		return false;
	}
	return true;
}

function dataClose(source){
	source = $(source);
	var isSuoHa = source.parent().attr("data-sh");
	if(isSuoHa && isSuoHa == 1){
		ToBase.isSuoHa = false;
	}
	source.parent().remove();
}

var refreshTimers;	//余额定时器
function refreshMoney(){
	var money = $("span.icoAcer");
	$.ajax({
		url:Mobile.base + '/meminfo.do',
		success:function(res){
			if(res.login){
				clearTimeout(refreshTimers);
				money.text(res.money);
				ToBase.account = res.account;
				refreshTimers = setTimeout(function(){	//10秒刷新一次余额
					refreshMoney();
				},10000);
			}else{
				clearTimeout(refreshTimers);
				money.text('0.00');
			}
		},
		beforeSend:function(){
			money.text("加载中...");
		} 
	})
}

var lastTimer;
var ToBase = {
	qiHao:null,
	code:null,
	type:null,
	isSuoHa:false,	//是否梭哈
	isOpen:true,	//是否封盘
	account:null,
	layerMsg:function(msg){
		layer.open({
			  content: msg,
			  style: 'border:none;background-color: rgba(0,0,0,.6); color:#fff;margin:0 auto;width:initial',
			  time: 2,
			  shade:false
		});
	},
	layerOpen:function(msg){
		layer.open({
		    content: msg,
		    btn: '确定'
		});
	},
	verifyImg:function(){
		var url = Mobile.base + "/verifycode.do?timestamp=" + (new Date().getTime());
		$("#authnumImg").attr("src", url);
	},
	clearZD:function(){
		$(".textArea div").empty();
	},
	toLocationHref:function(url){
		window.location.href = url;
	},
	toOpen:function(url){
		window.open(url);
	},
	initBetRoom:function(){
		ToBase.code = Room.lotCode;	//彩种code
		ToBase.type = Room.lotType;	//彩种type
		ToBase.betRoom.initOpen();
		ToBase.utils.initMsg();
		refreshMoney();
	}
}


ToBase.betRoom = function(){
	return {
		resultMsg:function(req_msg){
			var data = {
					data:JSON.stringify(req_msg)
			}
			$.ajax({
				url:Mobile.base + '/m/ajax/reqMsg.do',
				data:data,
				success:function(res){
					if(res.success){
						ToBase.utils.onMessage(res.msg);
					}
				}
			})
		},
		clearOnline:function(){
			var data = {
					account:Room.account,
					roomId:Room.roomId
			}
			$.ajax({
				url:Mobile.base + '/m/ajax/clearCur.do',
				async:false,
				data:data
			})
		},
		initOpen:function(){
			$.ajax({
				url:Mobile.base+'/m/ajax/roomOpen.do',
				data:{lotCode:Room.lotCode},
				success:function(res){
					if(res.success){
						ToBase.betRoom.roomCur(res.current);
						ToBase.betRoom.roomHistory(res.history);
						ToBase.betRoom.roomLast(res.last,false);
					}else{
						layer.open({
						    title: [
						      '异常警告',
						      'background-color: #dc5d55; color:#fff;'
						    ]
						    ,content: res.msg
						  });
					}
				}
			})
		},
		lastData:function(qiHao){
			var data = {
					qiHao:qiHao,
					lotCode:ToBase.code
			}
			$.ajax({
				url:Mobile.base + "/m/ajax/lotLast.do",
				data:data,
				type:'GET',
				success:function(res){
					if(res.success){
						//说明上一期已开奖，刷新页面即可，停止定时器
						clearInterval(lastTimer);
						ToBase.betRoom.roomLast(res.last,true);
						ToBase.betRoom.historyLast(res.last);
						//开奖提示
						ToBase.betRoom.resultMsg({"commandid":1002,"typeId":2100,"roomId":Room.roomId,"qiHao":res.last.qiHao,"openTime":res.last.endTime,"haoMa":res.last.haoMa});
					}
				}
			})
		},
		betInfo:function(qiHao){
			var dataInfo = betDataInfo;
			var data = {
					"commandid":1005,
					"data":JSON.stringify(betDataInfo),
					"qiHao":qiHao,
					"lotType":ToBase.type,
					"lotCode":ToBase.code,
					"roomId":Room.roomId
			}
			$.ajax({
				url:Mobile.base + '/m/ajax/toBetInfo.do',
				data:data,
				success:function(res){
					if(res.success){
						refreshMoney();
						//显示订单
						ToBase.betRoom.betCurOrder();
						data['data'] = dataInfo;	//修改data中的值，不进行JSON格式化
						data['account'] = Room.account;
						data['nickUrl'] = $.cookie('account_'+Room.account+'_touxiang');
						//使用机器人提示成功注单
						ToBase.betRoom.resultMsg(data);
					}else{
						ToBase.betRoom.resultMsg({"commandid":1004,"message":res.msg});
					}
					betDataInfo = [];
					if($(".textArea div").attr("contenteditable") == "false"){
		        		$(".textArea div").empty();
		        	}
				}
			})
		},
		betCurOrder:function(){	//获取当期下注的订单
			var data = {
					qiHao:ToBase.qiHao,
					lotCode:ToBase.code
			}
			$.ajax({
				url:Mobile.base+'/m/ajax/betCurOrder.do',
				data:data,
				success:function(res){
					if(res.success){
						var total=0,html='';
						$.each(res.doOrder,function(i,j){
							html += '<li><label>投注类型：'+j.haoMa+'</label><label>金额：'+j.buyMoney+'<i class="icoAcer"></i></label>';
							html += '<em class="close" order-no="'+j.orderId+'"></em></li>'
							total ++;
						})
						$("#betCount").text(total);
						$(".quxiao ul").html(html);
						$(".roomHead .betTixing").show();
					}else{
						$(".roomHead .betTixing").hide();
						$(".quxiao").hide();
					}
				}
			})
		},
		roomCur:function(cur){
			if(!cur){
				layer.open({
				    title: [
				      '异常警告',
				      'background-color: #dc5d55; color:#fff;'
				    ]
				    ,content: '暂时没有开奖信息'
				  });
				return;
			}
			ToBase.qiHao = cur.qiHao;
			ToBase.betRoom.betCurOrder();//加载当前期的下注
			$("#issue").text(cur.qiHao);
			BaseTimer.downTimer(cur);
		},
		roomHistory:function(history){
			var html = '',hm;
			$.each(history,function(i,j){
				var sum=0;
				html += '<ul><li>'+j.qiHao+'</li><li>'+j.time+'</li>';
				if(j.haoMa.indexOf("?")>=0){
					html += '<li>--</li>';
				}else{
					html += '<li>';
					hm = j.haoMa.split(",");
					switch(parseInt(ToBase.type)){
					case 161:
						for(var i=0;i<hm.length;i++){
							sum += parseInt(hm[i]);
							if(i==2){
								html += ''+ToBase.betRoom.firstAddZero(hm[i])+'';
							}else{
								html += ''+ToBase.betRoom.firstAddZero(hm[i])+'+';
							}
						}
						html += '='+ToBase.betRoom.firstAddZero(sum)+'(';
						html += sum>=14?'大':'小';
						html += '/';
						html += sum%2==0?'双':'单';
						break;
					default:
						break;
					}
					html += ')</li>';
				}
				html += '</ul>';
			})
			$(".lottery dl dd").html(html);
		},
		historyLast:function(last){
			var html = '',hm=last.haoMa,sum=0;
			switch(parseInt(ToBase.type)){
				case 161:
					hm = hm.split(",");
					for(var i=0;i<hm.length;i++){
						sum += parseInt(hm[i]);
						if(i==2){
							html += ''+ToBase.betRoom.firstAddZero(hm[i])+'';
						}else{
							html += ''+ToBase.betRoom.firstAddZero(hm[i])+'+';
						}
					}
					html += '='+ToBase.betRoom.firstAddZero(sum)+'(';
					html += sum>=14?'大':'小';
					html += '/';
					html += sum%2==0?'双':'单';
					html += ')';
					break;
				default:
					break;
			}
			$(".lottery dl dd ul:eq(0) li:eq(2)").text(html);
		},
		roomLast:function(last,flag){
			var html = '第'+last.qiHao+'期  ',hm=last.haoMa,sum=0;
			if(hm.indexOf("?")>=0){
				clearInterval(lastTimer);
				//没开奖
				html += '开奖中...';
				lastTimer = window.setInterval(function(){
					ToBase.betRoom.lastData(last.qiHao);
				},5000);
			}else{
				hm = hm.split(",");var html1='';
				switch(parseInt(ToBase.type)){
					case 161:
						for(var i=0;i<hm.length;i++){
							sum += parseInt(hm[i]);
							if(i==2){
								html1 += ''+ToBase.betRoom.firstAddZero(hm[i])+'';
							}else{
								html1 += ''+ToBase.betRoom.firstAddZero(hm[i])+'+';
							}
						}
						html1 += '='+ToBase.betRoom.firstAddZero(sum)+'(';
						html1 += sum>=14?'大':'小';
						html1 += '/';
						html1 += sum%2==0?'双':'单';
						html1 += ")";
						break;
					default:
						break;
				}
				if(flag){	//
					ToBase.socket.socketDataMessage(last,html1);
				}
				html = html + html1;
			}
			$(".roomHead2 span").text(html);
		},
		firstAddZero:function(num){
			num = parseInt(num);
			if(num<10){
				return '0'+num;
			}
			return num;
		},
		clearLastData:function(){
			var data = $(".roomHead .betTixing");
			data.hide();
			data.find("#betCount").text(0);
			data.find('.quxiao').hide();
			data.find(".quxiao ul").empty();
			//data.siblings(".userBetting").remove();
		}
	}
}();


var BaseTimer = function(){
	var downTimers,a_t=0,n_t=0,o_t=0,state,ago=0;
	return obj = {
		downTimer:function(cur){
			var t = this;
			state = cur.state;
			ago = cur.ago;
			this.timerTitle(cur.state);
			a_t = Date.parse(new Date(this.timeFormat(cur.activeTime)));
			n_t = Date.parse(new Date(this.timeFormat(cur.serverTime)));
			o_t = (a_t - n_t) / 1E3;
			t.tick(o_t);
		},
		timeFormat:function(e){
			return new Date(e.replace(/-/g,"/"));
		},
		timerTitle:function(state){
			if(state == 1){
				ToBase.isOpen = true;
				$("#kp_jz").text('截止');
				$("#bettingBtu").text('投注');
			}else{
				ToBase.isOpen = false;
				$("#kp_jz").text('开盘');
				$("#bettingBtu").text('已封盘');
				$(".customNews .keyboard").click();
			}
		},
		tick:function(intDiff){
			clearInterval(downTimers);
			var t = this,vg=0,_socket_flag=true,_socket_open=true;
			downTimers = window.setInterval(function(){
				vg = intDiff - ago;
				if(intDiff>0 && intDiff>ago && state == 1){ //
					var html = t.diffTimer(vg);
					t.timerHtml(html,1);
					t.timerTitle(1);
					if(_socket_open){
						_socket_open = false;
						ToBase.utils.cacheTimer();
						ToBase.betRoom.resultMsg({"commandid":1001,"roomId":Room.roomId,"typeId":2001,"qiHao":$("#issue").text()});
					}
					if(vg == 150){ToBase.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2004});}
					if(vg == 60){ToBase.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2003});}
				}else{
					clearInterval(cacheT1);
					var html = t.diffTimer(intDiff);
					t.timerHtml(html,1);
					t.timerTitle(2);
					if(_socket_flag && state == 1){
						_socket_flag = false;
						ToBase.betRoom.resultMsg({"commandid":1002,"typeId":2002,"roomId":Room.roomId,"qiHao":$("#issue").text()});
					}
					if(state == 2 && _socket_flag){
						_socket_flag = false;
						ToBase.betRoom.resultMsg({"commandid":1002,"typeId":2005,"roomId":Room.roomId,"qiHao":$("#issue").text()});
					}
					//支持机器人播报
					if(intDiff == 30){ToBase.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2007,"hostUrl":hostUrl});}
					if(intDiff == 20){ToBase.betRoom.resultMsg({"commandid":1007,"roomId":Room.roomId,"typeId":2008,"hostUrl":hostUrl});}
				}
				if(intDiff == 0){
					clearInterval(downTimers);
					ToBase.betRoom.initOpen();
					//清楚上一期的投注记录
					ToBase.betRoom.clearLastData();
					return;
				}
				intDiff--;
			},1000);
		},
		diffTimer:function(intDiff){
			var day=0,hour=0,hours=0,minute=0,second=0,time_html='';//时间默认值		
			if(intDiff > 0){
				day = Math.floor(intDiff / (60 * 60 * 24));
				hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
				hours = Math.floor(intDiff / (60*60));
				minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
				second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
			}
			if (hours <= 9 ) hours = '0' + hours;
			if (minute <= 9) minute = '0' + minute;
			if (second <= 9) second = '0' + second;
			time_html = hours + '时' + minute + '分' + second + '秒';
			return time_html;
		},
		timerHtml:function(html,state){
			if(state==1){
				$(".icoTime").text(html).show().siblings(".icoTimeClose").hide();
			}else{
				$(".icoTime").text(html).hide().siblings(".icoTimeClose").show();
			}
		}
	}
}();

ToBase.utils = function(){
	return {
		diu_Randomize:function(b,e){   
		    if(!b && b!=0 || !e){return 0;}   
		    return Math.floor( ( Math.random() * e ) + b );   
		},
		cookieData:function(key,value,expires){
			if(!$.cookie(key)){
				$.cookie(key,value,{expires:expires});
			}
		},
		getCookieData:function(key){
			return $.cookie(key);
		},
		randomNum:function(minNum,maxNum,cy){ 
		    switch(arguments.length){ 
		        case 1: 
		            return parseInt(Math.random()*minNum+1); 
		        break; 
		        case 2: 
		            return parseInt(Math.random()*(maxNum-minNum+1)+minNum); 
		        break; 
		        case 3:
		        	return parseFloat(Math.random()*(maxNum-minNum+1)+minNum)/cy;
		        default: 
		            return 0; 
		        break; 
		    } 
		},
		initMsg:function(){
			// 发送一个初始机器人消息
			var ts = parseInt($("#isGuest").val())==2?'会员':'游客';
			var initMsg= {
					"message":"欢迎"+ts+"【"+Room.account+"】进入房间",
					"commandid":1003,
					"account":Room.account,
					"roomId":Room.roomId
			}
			//InitWebSocket.robotSend(initMsg);
			ToBase.betRoom.resultMsg(initMsg);
			setTimeout(function(){	//2秒获取缓存投注记录
				var orderCache = {
						"commandid":1009,
						"lotCode":Room.lotCode,
						"roomId":Room.roomId,
						"isVialdate":true
				}
				ToBase.betRoom.resultMsg(orderCache);
			},2000);
			ToBase.utils.cacheTimer();
		},
		cacheTimer:function(){
			clearInterval(cacheT1);
			cacheT1 = window.setInterval(function(){	//3秒获取一次缓存投注记录
				var orderCache = {
						"commandid":1009,
						"lotCode":Room.lotCode,
						"roomId":Room.roomId,
						"isVialdate":false
				}
				ToBase.betRoom.resultMsg(orderCache);
			},3000)
		},
		onMessage:function(msg){
			console.info(msg);
			var msgData;
			$.each(msg,function(i,j){
				msgData = JSON.parse(j);
				ToBase.utils.appendHtml(msgData);
			})
			//var msgData = JSON.parse(msg);
			//ToBase.utils.appendHtml(msgData);
		},
		appendHtml:function(msg){
			//根据类型转换格式，填充到聊天窗口JSON.stringify(msg)
			console.info("result data————" + JSON.stringify(msg));
			var paramMsg = '',float="left",account=Room.robotAccount,touxiang=Room.robotImg,html='';
			switch(msg.commandid){
				case 1001:
				case 1002:
				case 1003:
				case 1004:	//异常处理
				case 1007:
					paramMsg = msg.message;
					break;
				case 1005:
				case 1008:	//机器人下注
					//console.info("机器人下注"+msg.account);
					msg['isVialdate'] = true;
					html = ToBase.utils.betOrderShow(msg);
					break;
				case 1006:
					account=msg.account;
					if(Room.account == account){
						float="right";
					}
					touxiang='/common/template/lottery/v4/mobile/images/touxiang/'+msg.nickUrl+'.jpg';
					paramMsg = msg.message;
					break;
				case 1009:
					$.each(msg.message,function(i,j){
						j['isVialdate'] = msg.isVialdate;
						html = ToBase.utils.betOrderShow(j);
						$("#wrapper .roomContent").append(html);
					})
					if(html){
						//$("body").animate({scrollTop: $(".roomContent").outerHeight()}, 300);	//每次有信息都滚动到底部
						$('body').animate({
			        		'scrollTop': $('body')[0].scrollHeight
			        	}, 400);
					}
					return false;
				case 1010:	//后台清除，无需执行
					//清楚前端内容
					//$("#scroller .userBetting").remove();
					return false;
				case 1011:
					ToBase.layerMsg("非法操作异常");
					break;
				case 1012:	//踢出
					window.location.href = Mobile.base+"/m/index.do";
					break;
			}
			if(msg.commandid != 1005 && msg.commandid != 1008){
				if(!paramMsg){return;}
				//html += '<div class="userBetting"><h3><span class="date_span">'+paramMsg+'<span></h3></div>';
				html+='<div class="userBetting"><h3><span class="date_span">'+$.formatDate("yyyy-MM-dd hh:mm",null,0)+'</span></h3>';
				html+='<ul class="'+float+'"><li><img src="'+Mobile.base+''+touxiang+'">';
				html+='</li><li class="sys_gly_'+float+'"><span style="float:'+float+'">'+account+'</span>';
				if(msg.levelImg){
					html += '<img src="'+msg.levelImg+'">';
				}
				html+='</li><li style="font-size:14px;"><pre>'+paramMsg+'</pre></li></ul></div>';
			}
			$("#wrapper .roomContent").append(html);
			//$("body").animate({scrollTop: $(".roomContent").outerHeight()}, 300);	//每次有信息都滚动到底部
			$('body').animate({
        		'scrollTop': $('body')[0].scrollHeight
        	}, 400);
		},
		betOrderShow:function(msg){
			var account = msg.account,float="left";
			if(Room.account == account){
				if(!msg.isVialdate){return false;}
				float="right";
			}
			var timer = new Date(msg.endTime);
			var html = '<div class="userBetting"><h3><span class="date_span">'+$.formatDate("yyyy-MM-dd hh:mm",timer,0)+'</span></h3>';
			html += '<ul class="'+float+'"><li><img src="'+Mobile.base+'/common/template/lottery/v4/mobile/images/touxiang/'+msg.nickUrl+'.jpg"></li><li class="sys_gly_'+float+'">';
			html += '<span style="float:'+float+';">'+account+'</span>';
			if(msg.levelImg){
				html += '<img src="'+msg.levelImg+'">';
			}
			html += '</li><li style="width:250px;" class="click_gt">';
			$.each(msg.data,function(i,j){
				if(i == msg.data.length-1){
					html += '<p style="margin-bottom:17px;" o_id="'+j.oddsId+'" p_code="'+j.playCode+'"><label>投注类型：</label><span>'+j.name+'</span><u class="icoAcer"></u><em>'+j.money+'</em><label style="color: #dc5d55;float:right;">金额：</label></p>'
				}else{
					html += '<p o_id="'+j.oddsId+'" p_code="'+j.playCode+'"><label>投注类型：</label><span>'+j.name+'</span><u class="icoAcer"></u><em>'+j.money+'</em><label style="color: #dc5d55;float:right;">金额：</label></p>'
				}
			})
			html += '<div><span style="float: left">第'+msg.qiHao+'期</span></div><i></i></li></ul></div>';
			return html;
		}
	}
}();

ToBase.socket = function(){
	return {
		socketDataMessage:function(last,html){
		//	var formTimer = new Date(last.endTime.replace(/-/g,"/"));
		//	var time = Date.parse(new Date(formTimer));
			var timer = new Date(last.endTime);
			var message = '期号：'+last.qiHao+'<br>开奖时间：'+$.formatDate("yyyy-MM-dd hh:mm:ss",timer,0)+'<br>开奖结果：'+html+'';
			//后续在添加统计结果
			var data = {
					"commandid":1007,
					"message":message
			}
			//InitWebSocket.robotSend(data);
		},
		socketMessage:function(commandid){
			var message = '';
			switch(commandid){
				case 1001:
					message = '*****尊敬的会员们*****<br><br>------------<br><br>'+$("#issue").text()+'期现在可以下注 '
					break;
				case 1002:
					message = '*****尊敬的会员们*****<br><br>------------<br><br>'+$("#issue").text()+'期已封盘，请耐心等待下一期';
					break;
				case 1004:	//暂不使用
					message = msg;
					break;
				case 1007:
					message += '☆☆☆☆☆开奖中☆☆☆☆☆<br>开奖网址：www.pc8686.com<br>诚信第一，玩家首选，提款3分钟火速到账，不限次数且免手续费，更多优惠请查看（'+hostUrl+'）优惠活动，祝您愉快！';
					message +='<br>尊敬的苹果用户：请重新下载APP的用户，安装时如果遇到授权问题请点击：“设置---通用---设备管理”进行授权。';
					break;
			}
			var data = {
					"commandid":commandid,
					"message":message
			}
			//InitWebSocket.robotSend(data);
		}
	}
}();

