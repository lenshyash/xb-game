<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>${_title}_钱包</title>
<link href="${base}/common/template/lottery/v4/mobile/css/base.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
</head>
<body>
	<header>
		<h1>钱包</h1>
		<div class="headerRight">
			<ul>
				<li class="icoRefresh"><a href="#"
					onclick="window.location.reload();"></a></li>
			</ul>

		</div>
	</header>
	<section>
		<div class="walletWarp my">
			<div class="wallet">
				<p>账户余额</p>
				<div class="walletAcer">
					<em class="icoAcer">${loginMember.money}</em>
				</div>
			</div>
			<div class="walletUl">
				<c:if test="${!isGuest}">
				<ul>
					<li
						data-href="${base}/m/toQb/toRegChange.do"><label
						class="icoCz1">充值</label></li>
					<c:choose>
						<c:when test="${not empty rPwd && !rPwd}">
							<li onclick="noRPwdBind();"><label
							class="icoCz2">提现</label></li>
						</c:when>
						<c:when test="${not empty cardNo && !cardNo}">
							<li onclick="noBankCard();">
							<label class="icoCz2">提现</label>
							</li>
						</c:when>
						<c:otherwise>
							<li data-href="${base}/m/toQb/toDrawCommit.do"><label
								class="icoCz2">提现</label></li>
						</c:otherwise>
					</c:choose>	
				</ul>
				<ul>
					<c:choose>
						<c:when test="${not empty rPwd && !rPwd}">
							<li onclick="noRPwdBind();"><label
							class="icoCz3">银行卡</label></li>
						</c:when>
						<c:otherwise>
							<li
							data-href="${base}/m/toQb/toBankCard.do"><label
							class="icoCz3">银行卡</label></li>
						</c:otherwise>
					</c:choose>
					<li
						data-href="${base}/m/toQb/billsWeb.do"><label
						class="icoCz4">交易记录</label></li>

				</ul>
				<ul>
					<li data-href="${base}/m/toQb/setup.do"><label class="icoCz5">支付安全</label></li>
				</ul>
				</c:if>
				<c:if test="${isGuest}">
					<ul>
						<li
							data-href="${base}/m/toQb/billsWeb.do"><label
							class="icoCz4">交易记录</label></li>
					</ul>
				</c:if>
			</div>
		</div>
	</section>
	<c:choose>
		<c:when test="${not empty rPwd && !rPwd}">
		</c:when>
		<c:otherwise>
			<c:if test="${not empty cardNo && !cardNo}">
				<div class="popup bindBank" style="display: block">
					<div class="config">
						<i class="configClose"></i>
						<p style="color: #242424;">您尚未绑定任何银行卡</p>
						<a href="${base}/m/toQb/toBankCard.do?type=2" style="text-decoration: underline; color: #ff4f4f"><p
								style="color: #ff4f4f;">立刻去绑定</p></a>
					</div>
				</div>
			</c:if>
		</c:otherwise>
	</c:choose>
	<div class="popup rPwdBind" style="display: none;">
		<div class="config">
			<i class="configClose"></i>
			<p style="color: #242424;">您尚未设置取款密码</p>
			<a href="${base}/m/toQb/setup.do" style="text-decoration: underline; color: #ff4f4f"><p
					style="color: #ff4f4f;">立刻去设置</p></a>
		</div>
	</div>
	<jsp:include page="../include/footer.jsp"></jsp:include>
	<script type="text/javascript">
		function noBankCard(){
			$(".bindBank").fadeIn();
		}
		function noRPwdBind(){
			$(".rPwdBind").fadeIn();
		}
	</script>
</body>
</html>