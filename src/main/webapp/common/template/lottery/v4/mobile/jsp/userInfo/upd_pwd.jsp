<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=750,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
	<jsp:include page="../include/base.jsp"></jsp:include>
<title>${_title}修改登录密码</title>
</head>
<body>
	<header>
		<em onclick="history.go(-1)">返回</em>
		<h1>修改登录密码</h1>
	</header>
	<style type="text/css">
		.my li{padding-right: 0px;}
	</style>
	<section>
		<div class="my">
			<ul>
				<li style="background: #ffffff;"><label class="icoModifyPas">旧密码<input
						type="password" placeholder="请输入旧密码" name="old_psd" maxlength="6"></label></li>
			</ul>
			<ul>
				<li style="background: #ffffff;"><label class="icoModifyPas">新密码<input
						type="password" placeholder="请输入新密码" name="new_psd" maxlength="6"></label></li>
				<li style="background: #ffffff;"><label class="icoModifyPas">确认密码<input
						type="password" placeholder="请再次输入新密码" name="new_psd2" maxlength="6"></label></li>
			</ul>
			<div class="button" style="margin-top: 100px">
				<button id="btn">下一步</button>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(function() {
			$("#btn").click(function() {
				var old_psd = $("input[name=old_psd]").val();
				var new_psd = $("input[name=new_psd]").val();
				var new_psd2 = $("input[name=new_psd2]").val();
				var reg = /^\d{6}$/;
				if (!reg.test(old_psd) || !reg.test(new_psd)
						|| !reg.test(new_psd2)) {
					ToBase.layerMsg("请输入6位数的密码");
					return false;
				}
				if (new_psd !== new_psd2) {
					ToBase.layerMsg("两次密码不一致");
					return false;
				}

				$.ajax({
					url : "${base}/center/member/meminfo/newpwd.do",
					type : "post",
					data : {
						opwd : old_psd,
						pwd : new_psd,
						rpwd : new_psd2,
						updType : 1
					},
					dataType : "json",
					success : function(data) {
						if (data.success) {
							window.location.href = "${base}/m/toQb/setSuccess.do?title=修改登录密码&content=修改登录密码成功&status=login";
						} else {
							ToBase.layerMsg(data.msg);
						}
					},
					error : function(error) {
						ToBase.layerMsg("服务器错误了");
					}
				});
			});
		});
	</script>
</body>
</html>