<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta content="target-densitydpi=320,width=800,user-scalable=no"
	name="viewport">
<meta content="no" name="apple-touch-fullscreen">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link href="${base}/common/template/lottery/v4/mobile/css/base.css" rel="stylesheet" type="text/css">
<jsp:include page="../include/base.jsp"></jsp:include>
<title>${_title }_个人中心</title>
</head>
<body>
	<header>
		<em
			data-href="${base}/m/toUser.do">返回</em>
		<h1>个人信息</h1>
		<div class="headerRight">
			<ul>
				<li class="icoAdd"><a href="#"></a></li>
			</ul>
		</div>
		 <div class="menu" style="display: none">
        <ul>
            <li class="icoRebate" data-href="#">天天返利</li>
            <li class="icoResult" data-href="${base}/m/openAward.do">开奖结果</li>
            <li class="icoIntroduce" data-href="${base}/m/gameRule.do">玩法介绍</li>
            <li class="icoTrend" data-href="${base}/m/gameTrend.do">开奖走势</li>
        </ul>
    </div>
	</header>
	<section>
		<div class="my">
			<ul>
				<li class="girl"><label class="icoGirl">头像</label><i><img src=""></i></li>
				<li
					data-href="#"><label
					class="icoNickname">昵称</label><span>${loginMember.account}</span></li>
				<li style="background: url('')"><label class="icoChineseName">持卡人中文名</label><span>${userInfo1['userName']}</span></li>
				<li
					data-href="#"><label
					class="icoEmail">电子邮箱</label><span>${userInfo1['email']}</span></li>
				<li style="background: url('')"><label class="icoPhone">手机号</label><span>${userInfo1['phone']}</span></li>
			</ul>
		</div>
	</section>
	<script type="text/javascript">
	$(function(){
		var nickUrl = $.cookie("account_${loginMember.account}_touxiang");
		$(".girl img").attr("src",'${base}/common/template/lottery/v4/mobile/images/touxiang/'+nickUrl+'.jpg');
	})
	</script>
</body>
</html>