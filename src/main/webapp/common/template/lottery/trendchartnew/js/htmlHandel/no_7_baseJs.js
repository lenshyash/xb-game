var chnNumChar = ["日","一","二","三","四","五","六","七","八","九"]
var chnUnitSection = ["","万","亿","万亿","亿亿"]
var chnUnitChar = ["","十","百","千"]

function NumberToChinese(num){
    var unitPos = 0;
    var strIns = '', chnStr = '';
    var needZero = false;

    if(num === 0){
        return chnNumChar[0];
    }

    while(num > 0){
        var section = num % 10000;
        if(needZero){
            chnStr = chnNumChar[0] + chnStr;
        }
        strIns = SectionToChinese(section);
        strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
        chnStr = strIns + chnStr;
        needZero = (section < 1000) && (section > 0);
        num = Math.floor(num / 10000);
        unitPos++;
    }

    return chnStr;
}
function SectionToChinese(section){
    var strIns = '', chnStr = '';
    var unitPos = 0;
    var zero = true;
    while(section > 0){
        var v = section % 10;
        if(v === 0){
            if(!zero){
                zero = true;
                chnStr = chnNumChar[v] + chnStr;
            }
        }else{
            zero = false;
            strIns = chnNumChar[v];
            strIns += chnUnitChar[unitPos];
            chnStr = strIns + chnStr;
        }
        unitPos++;
        section = Math.floor(section / 10);
    }
    return chnStr;
}
function  formatDate(dateLong) {
    var date = new Date(dateLong);
    return '星期'+NumberToChinese(date.getDay())+' '+add(date.getMonth(),1)+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes();
}
function lssj(data) {
    var str = '';
    $.each(data, function (index, value) {
        str += '<tr>';
        str += '<td>' + value.qiHao + '</td>';
        str += '<td>' + formatDate(value.openTime) + '</td>';
        str += '<td><table class="table-noborder"><tbody><tr>';
        var arr = value.haoMa.split(",");
        for (var i = 0; i < arr.length; ++i) {
            if (i == (arr.length - 1)) {
                str += '<td>+</td>';
                str += '<td><span class="icon_2 ball-style ball' + arr[i] + ' b-">' + arr[i] + '</span></td>';
            } else {
                str += '<td><span class="icon_2 ball-style ball' + arr[i] + ' b-">' + arr[i] + '</span></td>';
            }
        }
        str += '</tr><tr>';
        for (var i = 0; i < arr.length; ++i) {
            var haoma = Tools.parseInt(arr[i]);
            if (i == (arr.length - 1)) {
                str += '<td></td>';
                str += '<td><span class="icon_2 ball-style b-">' + getSxName(haoma) + '</span></td>';
            } else {
                str += '<td><span class="icon_2 ball-style b-">' + getSxName(haoma) + '</span></td>';
            }
        }
        str += '</tr></tbody></table></td>';
        var sum = 0;
        var boseArr = [0, 0, 0];
        $.each(arr, function (index, value) {
            sum += Tools.parseInt(value);
            boseArr[getBose(value)]++;
        });
        var maxBose = 0;
        var maxValue = 0;
        $.each(boseArr, function (index, value) {
            if (value > maxValue) {
                maxValue = value;
                maxBose = index;
            }
        });
        if (maxBose == 0) {
            maxBose = '<font class="color-red">红波</font>';
        } else if (maxBose == 1) {
            maxBose = '<font class="color-blue">蓝波</font>';
        } else if (maxBose == 2) {
            maxBose = '<font class="color-green">绿波</font>';
        }
        str += '<td>' + sum + '</td>';
        var tm = Tools.parseInt(arr[6]);
        str += '<td>' + (tm % 2 == 0 ? '<font class="color-red">双</font>' : '<font>单</font>') + '</td>';
        str += '<td>' + (tm >= 25 ? (tm != 49 ? '<font class="color-red">大</font>' : '和') : '<font>小</font>') + '</td>';
        str += '<td>' + ((Math.floor(tm / 10) + tm % 10) % 2 == 0 ? '<font class="color-red">合双</font>' : '<font>合单</font>') + '</td>';
        str += '<td>' + (sum % 2 == 0 ? '总和双' : '总和单') + '</td>';
        str += '<td>' + (sum >= 175 ? '总和大' : '总和小') + '</td>';
        str += '<td>' + maxBose + '</td>';
        str += '</tr>';
    });
    $("#shuju5 table tbody").html(str);
}

function zhifang(data) {
    return;
    // 基于准备好的dom，初始化echarts实例
    var myChart2 = echarts.init(document.getElementById('main2'));
    // 指定图表的配置项和数据
    var option = {
        grid: {
            left: 20,
            right: 20,
            top: 35,
            bottom: 0,
            containLabel: true
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            //show: true,   //default true
            showDelay: 0, //显示延时，添加显示延时可以避免频繁切换
            hideDelay: 50, //隐藏延时
            transitionDuration: 0, //动画变换时长
            backgroundColor: 'rgba(0,0,0,0.7)', //背景颜色（此时为默认色）
            borderRadius: 8, //边框圆角
            padding: 10, // [5, 10, 15, 20] 内边距
            position: function (p) {
                // 位置回调
                // console.log && console.log(p);
                return [p[0] + 10, p[1] - 10];
            },
            formatter: function (params, ticket, callback) {
                var res = "基本号码" + ' : ' + params[0].name;
                for (var i = 0, l = params.length; i < l; i++) {
                    res += '<br/>' + params[i].seriesName + ' : ' + params[i].value; //鼠标悬浮显示的字符串内容
                }
                return res;
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            data: []
        },
        yAxis: [{
            type: 'value',
            splitNumber: 10,
            triggerEvent: true,
        }],
        series: [{
            name: '出现次数',
            type: 'bar',
            data: [],
            legendHoverLink: true,
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            markPoint: {
                symbol: 'pin',
                symbolSize: 50,
                silent: true,
                animation: true,
            },
            barWidth: '27px',
            barGap: '30%',
            //            barCategoryGap:'30%',
            markArea: {
                //              silent:true
            },
            itemStyle: {
                normal: {
                    color: ['#6DB8FF']
                }
            }
        }]
    };
    var yData = [];
    for (var i = 0; i < 10; ++i) {
        yData[i] = 0;
        option.xAxis.data.push(i);
    }
    $.each(data, function (index, value) {
        var haoMa = value.haoMa;
        var arr = haoMa.split(",");
        for (var i = 0; i < arr.length; ++i) {
            yData[Tools.parseInt(arr[i])]++;
        }
    });
    option.series[0].data = yData;
    console.log(option);
    // 使用刚指定的配置项和数据显示图表。
    myChart2.setOption(option);
}

function kxian(data) {
    // 基于准备好的dom，初始化echarts实例
    var myChart1 = echarts.init(document.getElementById('main1'));

    var subtext = "";
    if (data.length > 0) {
        subtext += " 第" + data[0].qiHao + '期 ~ ' + "第" + data[data.length - 1].qiHao + '期';
    }

    // 指定图表的配置项和数据
    var option = {
        title: {
            text: "K线图",
            subtext: subtext,
            x: 'center'
        },
        legend: {
            data: ['第1位', '第2位', '第3位', '第4位', '第5位', '第6位', '第7位'],
            y: 530
        },
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 60
        },
        xAxis: [{
            axisLabel: {
                rotate: -60,
            },
            type: 'category',
            boundaryGap: false,
            data: []
        }],
        tooltip: {
            trigger: 'axis',
            formatter: function (params) {
                var haoMa = params[0].data.haoMa;

                var res = params[0].seriesName;
                res += '<br/>期号：' + params[0].name;
                res += '<br/>号码：' + params[0].data;
                return res;
            }
        },
        yAxis: [{
            type: 'value',
            name: '号码'
        }],
        grid: {
            left: '1%',
            right: '2%',
            containLabel: true,
            y2: 120
        },
        series: [{
            name: '第1位',
            type: 'line',
            data: []
        }, {
            name: '第2位',
            type: 'line',
            data: []
        }, {
            name: '第3位',
            type: 'line',
            data: []
        }, {
            name: '第4位',
            type: 'line',
            data: []
        }, {
            name: '第5位',
            type: 'line',
            data: []
        }, {
            name: '第6位',
            type: 'line',
            data: []
        }, {
            name: '第7位',
            type: 'line',
            data: []
        },]
    };
    $.each(data, function (index, value) {
        option.xAxis[0].data.push(value.haoMa);

        var tmpArr = value.haoMa.split(",");
        $.each(tmpArr, function (index, value) {
            var v = Tools.parseInt(value);
            option.series[index].data.push(v);
        });

    });

    myChart1.setOption(option);
}

function zhexian(data) {
    var str = '';
    for (var i = 0; i < data.length; i++) {
        // qihao
        str += '<div class="cl-30 clean">';
        str += '<div class="left cl-31 number" style="height:45px;line-height:45px;">' + data[i].qiHao + '</div>';

        var kjData = data[i].haoMa.split(",");
        str += '<div class="left cl-32 openCode" style="width:170px;height:45px;line-height:45px;">';
        for (var j = 0; j < 7; ++j) {
            var haoma = Tools.parseInt(kjData[j]);
            var bg = getBose(haoma);
            if (bg == 0) {
                bg = 'ball-red';
            } else if (bg == 1) {
                bg = 'ball-blue';
            } else {
                bg = 'ball-green';
            }
            if (kjData[j].length == 1) {
                str += '<span style="padding:3px;margin-right:3px;" class="round ' + bg + '">' + '0' + kjData[j] + '</span>';
            } else {
                str += '<span style="padding:3px;margin-right:3px;" class="round ' + bg + '">' + kjData[j] + '</span>';
            }

        }
        str += '</div>';

        str += '<div class="cl-35 cl-36">';
        for (var j = 0; j < kjData.length; ++j) {
            var haoma = Tools.parseInt(kjData[j]);

            str += '<var  style="width:50px;height:45px;">';
            var bg = getBose(haoma);
            if (bg == 0) {
                bg = 'ball-red';
            } else if (bg == 1) {
                bg = 'ball-blue';
            } else {
                bg = 'ball-green';
            }
            str += '<i style="width:25px;line-height:25px;height:25px;margin:auto;" data-num="' + haoma + '" class="round ' + bg + '">';
            str += haoma;
            str += '</i>';
            str += '<span class="clean">' + getSxName(haoma) + '</span>';
            str += '</var>';
        }

        var sum = 0;
        var boseArr = [0, 0, 0];
        $.each(kjData, function (index, value) {
            sum += Tools.parseInt(value);
            boseArr[getBose(value)]++;
        });
        var maxBose = 0;
        var maxValue = 0;
        $.each(boseArr, function (index, value) {
            if (value > maxValue) {
                maxValue = value;
                maxBose = index;
            }
        });
        if (maxBose == 0) {
            maxBose = '<font class="color-red">红波</font>';
        } else if (maxBose == 1) {
            maxBose = '<font class="color-blue">蓝波</font>';
        } else if (maxBose == 2) {
            maxBose = '<font class="color-green">绿波</font>';
        }
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + sum + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (sum % 2 == 0 ? '总和双' : '总和单') + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (sum >= 175 ? '总和大' : '总和小') + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + maxBose + '</div>';

        var tm = Tools.parseInt(kjData[6]);
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (tm % 2 == 0 ? '<font class="color-red">双</font>' : '<font>单</font>') + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (tm >= 25 ? (tm != 49 ? '<font class="color-red">大</font>' : '和') : '<font>小</font>') + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + ((Math.floor(tm / 10) + tm % 10) % 2 == 0 ? '<font class="color-red">合双</font>' : '<font>合单</font>') + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (tm == 49 ? '和' : ((Math.floor(tm / 10) + tm % 10) >= 7 ? '<font class="color-red">合大</font>' : '<font>合小</font>')) + '</div>';
        str += '<div class="left cl-31 number" style="background:none;width:53px;height:45px;line-height:45px;">' + (tm == 49 ? '和' : (tm % 10 >= 5 ? '<font class="color-red">尾大</font>' : '<font>尾小</font>')) + '</div>';

        str += '</div>';
    }
    $("#zhexianData").html(str);

    //        for (var i = 0; i < data.length-1 ;++i) {
    //            for (var j = 0; j < 5; ++j) {
    //                bc1 = Tools.parseInt(data[i].openCode.split(',')[j]) - Tools.parseInt(data[i + 1].openCode.split(',')[j]);
    //                var Left = 0;
    //                if (bc1 < 0) {
    //                    bc1 = -bc1;
    //                    Left = (bc1) * (-20);
    //                }
    //                var canvas = document.getElementById("canvas" + i + j);
    //                var context = canvas.getContext("2d");
    //                var bc1 = (data[i].openCode.split(',')[j]) - data[i + 1].openCode.split(',')[j];
    //                if (bc1 < 0) {
    //                    context.moveTo(17, 13);
    //                    context.lineTo(canvas.width - 13, canvas.height - 8);
    //                } else if (bc1 > 0) {
    //                    context.moveTo(canvas.width - 13, 13);
    //                    context.lineTo(8, canvas.height - 5);
    //                } else {
    //                    context.moveTo(10, 12);
    //                    context.lineTo(10, 30);
    //                }
    //                if (j % 2 != 0) {
    //                    context.strokeStyle = "#cc0000";
    //                }
    //                context.stroke();
    //            }
    //        }
    //        if ($("#checkboxZhexian").is(":checked")) {
    //            $(".zhexian").show();
    //        } else {
    //            $(".zhexian").hide();
    //        }
    //
    //        // 遗漏
    //        renderYilou(data);
    //
    //        if ($("#checkboxYlsj").is(":checked")) {
    //            $(".transparent").addClass("not-transparent");
    //        } else {
    //            $(".transparent").removeClass("not-transparent");
    //        }
}

function renderYilou(data) {
    // 遗漏统计
    var yilou = [];
    for (var i = 0; i < 5; ++i) {
        yilou[i] = [];
        for (var j = 0; j < 10; ++j) {
            yilou[i][j] = {
                cxCs: 0, // 出现次数
                maxLcCs: 0, // 最大连出次数
                ylArr: [] // 遗漏次数
            };
        }
    }

    for (var i = 0; i < 5; ++i) {
        for (var j = 0; j < 10; ++j) {
            var obj = yilou[i][j];

            var tmpYlCs = 0; // 连续遗漏次数
            var tmpLcCs = 0; // 连出次数
            $.each(data, function (index, value) {
                var openCodeArr = value.haoMa.split(",");
                var tmpValue = Tools.parseInt(openCodeArr[i]);

                if (tmpValue != j) { // 遗漏
                    tmpYlCs++;
                    if (tmpLcCs > obj.maxLcCs) {
                        obj.maxLcCs = tmpLcCs;
                    }
                    tmpLcCs = 0;
                } else { // 中
                    obj.cxCs++;
                    tmpLcCs++;

                    obj.ylArr.push(tmpYlCs);
                    tmpYlCs = 0;
                }
            });
        }
    }

    var str1 = '',
        str2 = '',
        str3 = '',
        str4 = '';
    for (var i = 0; i < 5; ++i) {
        for (var j = 0; j < 10; ++j) {
            var obj = yilou[i][j];
            // 出现次数
            str1 += '<var><i>' + obj.cxCs + '</i></var>';

            // 平均遗漏值&最大遗漏值
            var maxYl = 0;
            if (obj.ylArr.length > 0) {
                var sumYl = 0;
                $.each(obj.ylArr, function (index, value) {
                    sumYl += value;
                    maxYl = value > maxYl ? value : maxYl;
                });
                str2 += '<var><i>' + Math.floor(sumYl / obj.ylArr.length) + '</i></var>';
                str3 += '<var><i>' + maxYl + '</i></var>';
            } else {
                str2 += '<var><i>0</i></var>';
                str3 += '<var><i>0</i></var>';
            }

            // 最大连出值
            str4 += '<var><i>' + obj.maxLcCs + '</i></var>';
        }
    }

    $("#cxzcs").html(str1);
    $("#pjylz").html(str2);
    $("#zdylz").html(str3);
    $("#zdlcz").html(str4);

    var str5 = '';
    for (var i = 0; i < 5; ++i) {
        for (var j = 0; j < 10; ++j) {
            str5 += '<tr>';
            if (j == 0) {
                if (i == 0) {
                    str5 += '<td rowspan="10">万位</td>';
                } else if (i == 1) {
                    str5 += '<td rowspan="10">千位</td>';
                } else if (i == 2) {
                    str5 += '<td rowspan="10">百位</td>';
                } else if (i == 3) {
                    str5 += '<td rowspan="10">十位</td>';
                } else if (i == 4) {
                    str5 += '<td rowspan="10">个位</td>';
                }
            }

            str5 += '<td>' + j + '</td>';

            var obj = yilou[i][j];
            // 出现次数
            str5 += '<td>' + obj.cxCs + '</td>';

            // 平均遗漏值&最大遗漏值
            var maxYl = 0;
            if (obj.ylArr.length > 0) {
                var sumYl = 0;
                $.each(obj.ylArr, function (index, value) {
                    sumYl += value;
                    maxYl = value > maxYl ? value : maxYl;
                });
                str5 += '<td>' + Math.floor(sumYl / obj.ylArr.length) + '</td>';
                str5 += '<td>' + maxYl + '</td>';
            } else {
                str5 += '<td>0</td>';
                str5 += '<td>0</td>';
            }

            // 最大连出值
            str5 += '<td>' + obj.maxLcCs + '</td>';
            str5 += '</tr>';
        }
    }
    $("#shuju4 table tbody").html(str5);

    // 遗漏数据
    for (var i = 0; i < 5; ++i) {
        for (var j = 0; j < 10; ++j) {
            var tmpCount = 0;
            var obj = $(".i_" + i + "_" + j + " i");
            $(obj).each(function () {
                if (typeof $(this).data('num') == 'undefined') {
                    tmpCount = tmpCount + 1;
                    $(this).html(tmpCount).addClass("transparent");
                } else {
                    tmpCount = 0;
                }
            });
        }
    }

    // 遗漏分层
    for (var i = 0; i < 5; ++i) {
        for (var j = 0; j < 10; ++j) {
            var tmpCount = 0;
            var obj = $(".i_" + i + "_" + j + " i");
            for (var k = obj.length - 1; k >= 0; --k) {
                var tmpObj = $(obj).eq(k);
                if (typeof $(tmpObj).data('num') == 'undefined') {
                    $(tmpObj).parent().addClass("ylfc")
                } else {
                    break;
                }
            }
        }
    }
}