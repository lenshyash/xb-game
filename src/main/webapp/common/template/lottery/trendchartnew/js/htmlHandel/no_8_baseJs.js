//历史开奖结果
var chnNumChar = ["日","一","二","三","四","五","六","七","八","九"]
var chnUnitSection = ["","万","亿","万亿","亿亿"]
var chnUnitChar = ["","十","百","千"]

function NumberToChinese(num){
    var unitPos = 0;
    var strIns = '', chnStr = '';
    var needZero = false;

    if(num === 0){
        return chnNumChar[0];
    }

    while(num > 0){
        var section = num % 10000;
        if(needZero){
            chnStr = chnNumChar[0] + chnStr;
        }
        strIns = SectionToChinese(section);
        strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
        chnStr = strIns + chnStr;
        needZero = (section < 1000) && (section > 0);
        num = Math.floor(num / 10000);
        unitPos++;
    }

    return chnStr;
}
function SectionToChinese(section){
    var strIns = '', chnStr = '';
    var unitPos = 0;
    var zero = true;
    while(section > 0){
        var v = section % 10;
        if(v === 0){
            if(!zero){
                zero = true;
                chnStr = chnNumChar[v] + chnStr;
            }
        }else{
            zero = false;
            strIns = chnNumChar[v];
            strIns += chnUnitChar[unitPos];
            chnStr = strIns + chnStr;
        }
        unitPos++;
        section = Math.floor(section / 10);
    }
    return chnStr;
}
function  formatDate(dateLong) {
    var date = new Date(dateLong);
    return '星期'+NumberToChinese(date.getDay())+' '+add(date.getMonth(),1)+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes();
}
function lssj(data) {
    var str = '';
    $.each(data, function(index, value) {
        str += '<tr>';
        str += '<td>' + value.qiHao + '</td>';
        str += '<td>' + formatDate(value.openTime) + '</td>';
        str += '<td>';
        var arr = value.haoMa.split(",");
        for(var i = 0; i < arr.length; ++i) {
            str += '<span class="bg-10">' + arr[i] + '</span>';
        }
        str += '</td>';
        str += '<td>'+G10_Auto(arr,1)+'</td>';
        str += '<td>'+G10_Auto(arr,2)+'</td>';
        str += '<td>'+G10_Auto(arr,3)+'</td>';
        str += '<td>'+G10_Auto(arr,4)+'</td>';
        str += '<td>'+G10_Auto(arr,5)+'</td>';
        str += '<td>'+G10_Auto(arr,6)+'</td>';
        str += '<td>'+G10_Auto(arr,7)+'</td>';
        str += '<td>'+G10_Auto(arr,8)+'</td>';
        str += '</tr>';
    });
    $("#shuju5 table tbody").html(str);
}

function zhifang(data) {

    // 基于准备好的dom，初始化echarts实例
    var myChart2 = echarts.init(document.getElementById('main2'));
    // 指定图表的配置项和数据
    var subtext = "重庆快乐十分";
    if(data.length > 0) {
        subtext += " 第" + data[0].qiHao + '期 ~ ' + "第" + data[data.length - 1].qiHao + '期';
    }
    var option = {
        title: {
            text: "重庆快乐十分直方图",
            subtext: subtext,
            x: 'center'
        },
        grid: {
            left: '1%',
            right: '5%',
            containLabel: true
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            showDelay: 0, //显示延时，添加显示延时可以避免频繁切换
            hideDelay: 50, //隐藏延时
            transitionDuration: 0, //动画变换时长
            backgroundColor: 'rgba(0,0,0,0.7)', //背景颜色（此时为默认色）
            borderRadius: 8, //边框圆角
            padding: 10, // [5, 10, 15, 20] 内边距
            position: function(p) {
                return [p[0] + 10, p[1] - 10];
            },
            formatter: function(params, ticket, callback) {
                var res = "基本号码" + ' : ' + params[0].name;
                for(var i = 0, l = params.length; i < l; i++) {
                    res += '<br/>' + params[i].seriesName + ' : ' + params[i].value; //鼠标悬浮显示的字符串内容
                }
                return res;
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            data: [],
            name: '号码'
        },
        yAxis: [{
            type: 'value',
            splitNumber: 10,
            triggerEvent: true,
            name: '出现次数'
        }],
        series: [{
            name: '出现次数',
            type: 'bar',
            data: [],
            legendHoverLink: true,
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            markPoint: {
                symbol: 'pin',
                symbolSize: 50,
                silent: true,
                animation: true,
            },
            barWidth: '27px',
            barGap: '30%',
            markArea: {},
            itemStyle: {
                normal: {
                    color: ['#6DB8FF']
                }
            }
        }]
    };
    var yData = [];
    for(var i = 0; i < 20; ++i) {
        yData[i] = 0;
        option.xAxis.data.push(i + 1);
    }
    $.each(data, function(index, value) {
        var haoMa = value.haoMa;
        var arr = haoMa.split(",");
        for(var i = 0; i < arr.length; ++i) {
            yData[parseInt(arr[i]) - 1]++;
        }
    });
    option.series[0].data = yData;
    // 使用刚指定的配置项和数据显示图表。
    myChart2.setOption(option);
}

function kxian(data) {
    // 基于准备好的dom，初始化echarts实例
    var myChart1 = echarts.init(document.getElementById('main1'));

    // 指定图表的配置项和数据
    var subtext = "";
    if(data.length > 0) {
        subtext += " 第" + data[0].qiHao + '期 ~ ' + "第" + data[data.length - 1].qiHao + '期';
    }
    var option = {
        title: {
            text: "K线图",
            subtext: subtext,
            x: 'center'
        },
        xAxis: [{
            axisLabel: {
                rotate: -60,
                interval: 0
            },
            type: 'category',
            boundaryGap: false,
            data: []
        }],
        tooltip: {
            trigger: 'axis',
            formatter: function(params) {
                var haoMa = params[0].data.haoMa;

                var res = '';
                res += '数值：' + parseInt(haoMa.split(",")[0]);
                res += '<br/>期数：' + params[0].name;
                res += '<br/><font class="red">奖号：' + haoMa + "</font>";
                return res;
            }
        },
        yAxis: [{
            type: 'value',
            name: '号码'
        }],
        grid: {
            left: '1%',
            right: '5%',
            containLabel: true
        },
        series: [{
            symbol: 'circle', //图标形状
            symbolSize: 6, //图标尺寸
            type: 'line',
            stack: '',
            itemStyle: {
                normal: {
                    color: "#6DB8FF",
                    lineStyle: {
                        color: '#6DB8FF'
                    }
                }
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            areaStyle: {
                normal: {
                    textStyle: {
                        fontSize: 20,
                        color: 'red'
                    }
                }
            },
            data: []
        }]
    };
    $.each(data, function(index, value) {
        option.xAxis[0].data.push(value.qiHao);
        option.series[0].data.push({
            value: parseInt(value.haoMa.split(",")[0]),
            haoMa: value.haoMa
        });
    });

    myChart1.setOption(option);
}

function zhexian(data) {
    var str = '';
    for(var i = 0; i < data.length; i++) {
        // qihao
        str += '<div class="cl-30 clean">';
        str += '<div class="left cl-31 number noFixedQh">' + data[i].qiHao + '</div>';
        str += '<div class="left cl-32 openCode noFixedJh" style="width:150px">' + data[i].haoMa + '</div>';
        var kjData = data[i].haoMa.split(",");

        var tmp = [];
        $.each(kjData, function(index, value) {
            tmp[parseInt(value)] = 1;
        });

        str += '<div class="cl-35 cl-36">';
        for(var j = 1; j <= 20; ++j) {
            var s = j >= 11 ? 'bg-1' : 'bg-2';
            var s2 = j >= 11 ? 'bg-4' : 'bg-5';
            if(tmp[j] == 1) {
                str += '<var class="' + s + ' i_' + j + '"><i data-num="' + j + '" class="' + s2 + '">' + j + '</i></var>';
            } else {
                str += '<var class="' + s + ' i_' + j + '"><i></i></var>';
            }
        }

        // 012路
        var str1 = [],
            str2 = [],
            str3 = [];
        for(var j = 1; j <= 20; ++j) {
            if(tmp[j] == 1) {
                if(j % 3 == 0) {
                    str1.push(j);
                } else if(j % 3 == 1) {
                    str2.push(j);
                } else {
                    str3.push(j);
                }
            }
        }
        str += '<var><i style="width:100px">' + str1.join("&nbsp;") + '</i></var>';
        str += '<var><i style="width:100px">' + str2.join("&nbsp;") + '</i></var>';
        str += '<var><i style="width:100px">' + str3.join("&nbsp;") + '</i></var>';

        //跨度
        var kuadu = 0;
        $.each(kjData, function(index1, value1) {
            $.each(kjData, function(index2, value2) {
                var tmp = Math.abs(parseInt(value1) - parseInt(value2));
                kuadu = tmp > kuadu ? tmp : kuadu;
            })
        });
        str += '<var><i style="width:97px">' + kuadu + '</i></var>';

        str += '</div>';
    }
    $("#zhexianData").html(str);
    // 遗漏
    renderYilou(data);

    if($("#checkboxYlsj").is(":checked")) {
        $(".transparent").addClass("not-transparent");
    } else {
        $(".transparent").removeClass("not-transparent");
    }
}

function renderYilou(data) {
    // 遗漏统计
    var yilou = [];
    for(var j = 1; j <= 20; ++j) {
        yilou[j] = {
            cxCs: 0, // 出现次数
            maxLcCs: 0, // 最大连出次数
            ylArr: [] // 遗漏次数
        };
    }

    var tmpArr = [];

    $.each(data, function(index, value) {
        tmpArr[index] = [];
        for(var i = 1; i <= 20; ++i) {
            tmpArr[index][i] = 0;
        }
        var openCodeArr = value.haoMa.split(",");
        $.each(openCodeArr, function(index2, value2) {
            tmpArr[index][parseInt(value2)]++;
        });
    });

    for(var j = 1; j <= 20; ++j) {
        var obj = yilou[j];
        var tmpYlCs = 0; // 连续遗漏次数
        var tmpLcCs = 0; // 连出次数

        $.each(tmpArr, function(index, value) {
            var tmpValue = parseInt(value[j]);

            if(tmpValue != 1) { // 遗漏
                tmpYlCs++;
                if(tmpLcCs > obj.maxLcCs) {
                    obj.maxLcCs = tmpLcCs;
                }
                tmpLcCs = 0;
            } else { // 中
                obj.cxCs++;
                tmpLcCs++;

                obj.ylArr.push(tmpYlCs);
                tmpYlCs = 0;
            }
        });
    }
    var str1 = '',
        str2 = '',
        str3 = '',
        str4 = '';
    for(var j = 1; j <= 20; ++j) {
        var obj = yilou[j];
        // 出现次数
        str1 += '<var><i>' + obj.cxCs + '</i></var>';

        // 平均遗漏值&最大遗漏值
        var maxYl = 0;
        if(obj.ylArr.length > 0) {
            var sumYl = 0;
            $.each(obj.ylArr, function(index, value) {
                sumYl += value;
                maxYl = value > maxYl ? value : maxYl;
            });
            str2 += '<var><i>' + Math.floor(sumYl / obj.ylArr.length) + '</i></var>';
            str3 += '<var><i>' + maxYl + '</i></var>';
        } else {
            str2 += '<var><i>0</i></var>';
            str3 += '<var><i>0</i></var>';
        }

        // 最大连出值
        str4 += '<var><i>' + obj.maxLcCs + '</i></var>';
    }

    str1 += '<var><i style="width:100px"></i></var>';
    str1 += '<var><i style="width:100px"></i></var>';
    str1 += '<var><i style="width:100px"></i></var>';
    str1 += '<var><i style="width:97px"></i></var>';
    str2 += '<var><i style="width:100px"></i></var>';
    str2 += '<var><i style="width:100px"></i></var>';
    str2 += '<var><i style="width:100px"></i></var>';
    str2 += '<var><i style="width:97px"></i></var>';
    str3 += '<var><i style="width:100px"></i></var>';
    str3 += '<var><i style="width:100px"></i></var>';
    str3 += '<var><i style="width:100px"></i></var>';
    str3 += '<var><i style="width:97px"></i></var>';

    $("#cxzcs").html(str1);
    $("#pjylz").html(str2);
    $("#zdylz").html(str3);
    $("#zdlcz").html(str4);

    // 遗漏数据
    for(var i = 1; i <= 20; ++i) {
        var tmpCount = 0;
        var obj = $(".i_" + i + " i");
        $(obj).each(function() {
            if(typeof $(this).data('num') == 'undefined') {
                tmpCount = tmpCount + 1;
                $(this).html(tmpCount).addClass("transparent");
            } else {
                tmpCount = 0;
            }
        });
    }

    // 遗漏分层
    for(var i = 1; i <= 20; ++i) {
        var obj = $(".i_" + i + " i");
        for(var j = obj.length - 1; j >= 0; --j) {
            var o = obj[j];
            if(typeof $(o).data('num') == 'undefined') {
                $(o).parent().addClass("ylfc");
            } else {
                break;
            }
        }
    }

    if($("#checkboxYlfc").is(":checked")) {
        $(".ylfc").addClass("ylfc-bg");
    } else {
        $(".ylfc").removeClass("ylfc-bg");
    }
}