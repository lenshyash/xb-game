//历史开奖结果
var chnNumChar = ["日","一","二","三","四","五","六","七","八","九"]
var chnUnitSection = ["","万","亿","万亿","亿亿"]
var chnUnitChar = ["","十","百","千"]

function NumberToChinese(num){
    var unitPos = 0;
    var strIns = '', chnStr = '';
    var needZero = false;

    if(num === 0){
        return chnNumChar[0];
    }

    while(num > 0){
        var section = num % 10000;
        if(needZero){
            chnStr = chnNumChar[0] + chnStr;
        }
        strIns = SectionToChinese(section);
        strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
        chnStr = strIns + chnStr;
        needZero = (section < 1000) && (section > 0);
        num = Math.floor(num / 10000);
        unitPos++;
    }

    return chnStr;
}
function SectionToChinese(section){
    var strIns = '', chnStr = '';
    var unitPos = 0;
    var zero = true;
    while(section > 0){
        var v = section % 10;
        if(v === 0){
            if(!zero){
                zero = true;
                chnStr = chnNumChar[v] + chnStr;
            }
        }else{
            zero = false;
            strIns = chnNumChar[v];
            strIns += chnUnitChar[unitPos];
            chnStr = strIns + chnStr;
        }
        unitPos++;
        section = Math.floor(section / 10);
    }
    return chnStr;
}
function  formatDate(dateLong) {
    var date = new Date(dateLong);
    return '星期'+NumberToChinese(date.getDay())+' '+add(date.getMonth(),1)+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes();
}
function lssj(data) {
    var str = '';
    $.each(data, function(index, value) {   //each() 方法为每个匹配元素规定要运行的函数
        str += '<tr>';
        str += '<td>' + value.qiHao + '</td>';
        str += '<td>' + formatDate(value.openTime) + '</td>';
        str += '<td>';
        var arr = value.haoMa.split(",");
        for(var i = 0; i < arr.length; ++i) {
            str += '<span class="bg-10">' + arr[i] + '</span>';
        }
        str += '</td>';
        str += '<td>'+Ssc_Auto(arr,1)+'</td>';
        str += '<td>'+Ssc_Auto(arr,12)+'</td>';
        str += '<td>'+Ssc_Auto(arr,3)+'</td>';
        str += '<td>'+Ssc_Auto(arr,4)+'</td>';
        str += '</tr>';
    });
    $("#shuju5 table tbody").html(str);
}
function zhifang(data) {

    // 基于准备好的dom，初始化echarts实例
    var myChart2 = echarts.init(document.getElementById('main2'));
    // 指定图表的配置项和数据
    var subtext = "";
    if(data.length > 0) {
        subtext += " 第" + data[0].qiHao + '期 ~ 第' + data[data.length - 1].qiHao + '期';
    }
    var option = {
        title: {
            text: '直方图',    //主标题文本
            subtext: subtext,     //副标题文本
            x: 'center'
        },
        grid: {
            left: '1%',
            right: '5%',
            containLabel: true
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            //show: true,   //default true
            showDelay: 0, //显示延时，添加显示延时可以避免频繁切换
            hideDelay: 50, //隐藏延时
            transitionDuration: 0, //动画变换时长
            backgroundColor: 'rgba(0,0,0,0.7)', //背景颜色（此时为默认色）
            borderRadius: 8, //边框圆角
            padding: 10, // [5, 10, 15, 20] 内边距
            position: function(p) {
                // 位置回调
                // console.log && console.log(p);
                return [p[0] + 10, p[1] - 10];
            },
            formatter: function(params, ticket, callback) {
                var res = "基本号码" + ' : ' + params[0].name;
                for(var i = 0, l = params.length; i < l; i++) {
                    res += '<br/>' + params[i].seriesName + ' : ' + params[i].value; //鼠标悬浮显示的字符串内容
                }
                return res;
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            data: [],
            name: '号码'
        },
        yAxis: [{
            type: 'value',
            splitNumber: 10,
            triggerEvent: true,
            name: '出现次数'
        }],
        series: [{
            name: '出现次数',
            type: 'bar',
            data: [],
            legendHoverLink: true,
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            markPoint: {
                symbol: 'pin',
                symbolSize: 50,
                silent: true,
                animation: true,
            },
            barWidth: '27px',
            barGap: '10px',
            //            barCategoryGap:'30%',
            markArea: {
                //              silent:true
            },
            itemStyle: {
                normal: {
                    color: ['#6DB8FF']
                }
            }
        }]
    };
    var yData = [];
    for(var i = 0; i < 10; ++i) {
        yData[i] = 0;
        option.xAxis.data.push(i);
    }
    $.each(data, function(index, value) {
        var haoMa = value.haoMa;
        var arr = haoMa.split(",");
        for(var i = 0; i < arr.length; ++i) {
            yData[Tools.parseInt(arr[i])]++;
        }
    });
    option.series[0].data = yData;
    // 使用刚指定的配置项和数据显示图表。
    myChart2.setOption(option);
}
function kxian(data) {
    // 基于准备好的dom，初始化echarts实例
    var myChart1 = echarts.init(document.getElementById('main1'));

    // 指定图表的配置项和数据
    var subtext = "";
    if(data.length > 0) {
        subtext += " 第" + data[0].qiHao + '期 ~ ' + "第" + data[data.length - 1].qiHao + '期';
    }
    var option = {
        title: {
            text: "K线图",
            subtext: subtext,
            x: 'center'
        },
        legend: {
            data: ['第一球', '第二球', '第三球', '第四球', '第五球'],
            y: 550
        },
        xAxis: [{
            axisLabel: {
                rotate: -60,
                interval: 0
            },
            type: 'category',
            boundaryGap: false,
            data: []
        }],
        tooltip: {
            trigger: 'axis',
            formatter: function(params) {
                var haoMa = params[0].data.haoMa;

                var res = params[0].seriesName;
                res += '<br/>期号：' + params[0].name;
                res += '<br/>号码：' + params[0].data;
                return res;
            }
        },
        yAxis: [{
            type: 'value',
            name: '号码'
        }],
        grid: {
            left: '1%',
            right: '2%',
            containLabel: true,
            y2: 120
        },
        series: [{
            name: '第一球',
            type: 'line',
            data: []
        }, {
            name: '第二球',
            type: 'line',
            data: []
        }, {
            name: '第三球',
            type: 'line',
            data: []
        }, {
            name: '第四球',
            type: 'line',
            data: []
        }, {
            name: '第五球',
            type: 'line',
            data: []
        }]
    };
    $.each(data, function(index, value) {
        option.xAxis[0].data.push(value.qiHao);

        var tmpArr = value.haoMa.split(",");
        $.each(tmpArr, function(index, value) {
            var v = Tools.parseInt(value);
            option.series[index].data.push(v);
        });
    });

    myChart1.setOption(option);
}
function zhexian(data) {  //折线
    var str = '';
    for(var i = 0; i < data.length; i++) {
        // qihao
        str += '<div class="cl-30 clean">';
        str += '<div class="left cl-31 number noFixedQh">' + data[i].qiHao.substr(2) + '</div>';
        str += '<div class="left cl-32 openCode noFixedJh">' + data[i].haoMa + '</div>';
        var kjData = data[i].haoMa.split(",");


        for(var j = 0; j < kjData.length; ++j) {
            var haoma = kjData[j];

            var bc1 = 0;
            var Left = 0;

            str += '<div class="cl-35 cl-36">';

            if(i < data.length - 1) {
                bc1 = (data[i].haoMa.split(",")[j]) - (data[i + 1].haoMa.split(",")[j]);
            }

            if(bc1 > 0) {
                Left = (bc1) * (-20);
            } else if(bc1 < 0) {
                bc1 = -bc1;
            }


            for(var k =0; k <11; ++k) {
                str += '<var class="' + (j % 2 == 0 ? 'bg-1' : 'bg-2') + ' i_' + j + "_" + k + '">';

                if(k == haoma-1) {
                    str += '<i data-num="' + k + '" class="' + (j % 2 == 0 ? 'bg-4' : 'bg-5') + '">';

                    str += k+1;
                    str += '<canvas class="zhexian" id="canvas' + i + j + '" width="' + (bc1 + 1) * 20 + '" height="32px" style="z-index: 10; left:' + Left + 'px; display: none;"></canvas>';
                    str += '</i>';
                } else {
                    str += '<i></i>';
                }

                str += '</var>';
            }



            str += '</div>';
        }
        str += '</div>';
    }


    $("#zhexianData").html(str); //html() 方法返回或设置被选元素的内容  //如果该方法未设置参数，则返回被选元素的当前内容

    for(var i = 0; i < data.length - 1; ++i) {
        for(var j = 0; j < 5; ++j) {
            bc1 = Tools.parseInt(data[i].haoMa.split(',')[j]) - Tools.parseInt(data[i + 1].haoMa.split(',')[j]);
            var Left = 0;
            if(bc1 < 0) {
                bc1 = -bc1;
                Left = (bc1) * (-20);
            }
            var canvas = document.getElementById("canvas" + i + j);
            var context = canvas.getContext("2d");
            var bc1 = (data[i].haoMa.split(',')[j]) - data[i + 1].haoMa.split(',')[j];
            if(bc1 < 0) {
                context.moveTo(17, 13);
                context.lineTo(canvas.width - 13, canvas.height - 8);
            } else if(bc1 > 0) {
                context.moveTo(canvas.width - 13, 13);
                context.lineTo(8, canvas.height - 5);
            } else {
                context.moveTo(10, 12);
                context.lineTo(10, 30);
            }
            if(j % 2 != 0) {
                context.strokeStyle = "#cc0000";
            }
            context.stroke();
        }
    }


    if($("#checkboxZhexian").is(":checked")) {
        $(".zhexian").show();
    } else {
        $(".zhexian").hide();
    }

    // 遗漏
    renderYilou(data);

    if($("#checkboxYlsj").is(":checked")) {
        $(".transparent").addClass("not-transparent");
    } else {
        $(".transparent").removeClass("not-transparent");
    }
}
function renderYilou(data) {
    // 遗漏统计
    var yilou = [];
    for(var i = 0; i < 5; ++i) {
        yilou[i] = [];
        for(var j = 0; j <11; ++j) {
            yilou[i][j] = {
                cxCs: 0, // 出现次数
                maxLcCs: 0, // 最大连出次数
                ylArr: [] // 遗漏次数
            };
        }
    }

    for(var i = 0; i < 5; ++i) {
        for(var j = 0; j <11; ++j) {
            var obj = yilou[i][j];

            var tmpYlCs = 0; // 连续遗漏次数
            var tmpLcCs = 0; // 连出次数
            $.each(data, function(index, value) {
                var openCodeArr = value.haoMa.split(",");
                var tmpValue = Tools.parseInt(openCodeArr[i]);


                if(tmpValue != j+1) { // 遗漏
                    tmpYlCs++;
                    if(tmpLcCs > obj.maxLcCs) {
                        obj.maxLcCs = tmpLcCs;
                    }
                    tmpLcCs = 0;
                } else { // 中
                    obj.cxCs++;
                    tmpLcCs++;

                    obj.ylArr.push(tmpYlCs);
                    tmpYlCs = 0;
                }
            });
        }
    }
    var str1 = '',
        str2 = '',
        str3 = '',
        str4 = '';

    for(var i = 0; i < 5; ++i) {
        for(var j = 0; j <11; ++j) {

            var obj = yilou[i][j];
            // 出现次数
            str1 += '<var><i>' + obj.cxCs + '</i></var>';

            // 平均遗漏值&最大遗漏值
            var maxYl = 0;
            if(obj.ylArr.length > 0) {
                var sumYl = 0;
                $.each(obj.ylArr, function(index, value) {
                    sumYl += value;
                    maxYl = value > maxYl ? value : maxYl;
                });
                str2 += '<var><i>' + Math.floor(sumYl / obj.ylArr.length) + '</i></var>';
                str3 += '<var><i>' + maxYl + '</i></var>';
            } else {
                str2 += '<var><i>0</i></var>';
                str3 += '<var><i>0</i></var>';
            }

            // 最大连出值
            str4 += '<var><i>' + obj.maxLcCs + '</i></var>';
        }



    }

    $("#cxzcs").html(str1);




    $("#pjylz").html(str2);
    $("#zdylz").html(str3);
    $("#zdlcz").html(str4);

    var str5 = '';
    for(var i = 0; i < 5; ++i) {
        for(var j = 0; j < 11; ++j) {
            str5 += '<tr>';
            if(j == 0) {
                if(i == 0) {
                    str5 += '<td rowspan="10">万位</td>';
                } else if(i == 1) {
                    str5 += '<td rowspan="10">千位</td>';
                } else if(i == 2) {
                    str5 += '<td rowspan="10">百位</td>';
                } else if(i == 3) {
                    str5 += '<td rowspan="10">十位</td>';
                } else if(i == 4) {
                    str5 += '<td rowspan="10">个位</td>';
                }
            }

            str5 += '<td>' + (j+1) + '</td>';

            var obj = yilou[i][j];
            // 出现次数
            str5 += '<td>' + obj.cxCs + '</td>';

            // 平均遗漏值&最大遗漏值
            var maxYl = 0;
            if(obj.ylArr.length > 0) {
                var sumYl = 0;
                $.each(obj.ylArr, function(index, value) {
                    sumYl += value;
                    maxYl = value > maxYl ? value : maxYl;
                });
                str5 += '<td>' + Math.floor(sumYl / obj.ylArr.length) + '</td>';
                str5 += '<td>' + maxYl + '</td>';
            } else {
                str5 += '<td>0</td>';
                str5 += '<td>0</td>';
            }

            // 最大连出值
            str5 += '<td>' + obj.maxLcCs + '</td>';
            str5 += '</tr>';
        }
    }
    $("#shuju4 table tbody").html(str5);

    // 遗漏数据


    for(var i = 0; i < 5; ++i) {
        for(var j = 0; j <11; ++j) {
            var tmpCount = 0;
            var obj = $(".i_" + i + "_" + j + " i");
            $(obj).each(function() {
                if(typeof $(this).data('num') == 'undefined') {
                    tmpCount = tmpCount + 1;
                    $(this).html(tmpCount).addClass("transparent");
                } else {
                    tmpCount = 0;
                }
            });
        }
    }

    // 遗漏分层
    for(var i = 0; i < 5; ++i) {
        for(var j =0; j < 11; ++j) {
            var tmpCount = 0;
            var obj = $(".i_" + i + "_" + j + " i");
            for(var k = obj.length - 1; k >= 0; --k) {
                var tmpObj = $(obj).eq(k);
                if(typeof $(tmpObj).data('num') == 'undefined') {
                    $(tmpObj).parent().addClass("ylfc")
                } else {
                    break;
                }
            }
        }
    }
}