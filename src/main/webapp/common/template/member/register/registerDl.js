var RC = {
	version:null, //null值 为PC版使用,cp_v3 彩票版V3注册入口
	alert:function(msg,key){
		if(this.version = "cp_v3"){
			$("#"+key).addClass('error').parent().siblings("#_blur").show().html('<div class="reg-notice-icon"></div><font color="#e51937">'+key+'不能为空</font>').siblings("#_focus").hide();
			return;
		}else if(this.version = "cp_v4"){
			ToBase.dialogAlert(key + "不能为空");
		}
		alert(msg);
	}
};


(function() {
	var _tags = document.getElementsByTagName("script");
	RC.version = _tags[_tags.length - 1].getAttribute("version");
})();

var fileds = [];
var validateFiled = [];
var base = $("#baseD").val()
var linkCode= 0;
var $_regconfData;
$(function() {
	arrcookie = document.cookie.split("; ");
	$.each(arrcookie,function(index,item){
		if(item.indexOf('linkCode') >= 0){
			linkCode = item.substring(9)
			$("#promoCode").attr("disabled","disabled");
			$("#promoCode").val(linkCode)
			$("#wrongok").show()
		}
	})
	initRegConf();
	$("#promoCode").blur(function(){
		initRegConf();
	});
})

function initRegConf() {
	linkCode = $("#promoCode").val()
	$.ajax({
		url:base+"/checkPromo.do",
		data : {
			linkCode : linkCode
		},
		type:"POST",
		success : function(data) {
			$_regconfData = data
			if(data.length>0){
				$("#wrongok").show()
				$("#wrong").hide()
				for (var i = 0; i < $_regconfData.length; i++) {
					var conf = $_regconfData[i];
					if (conf.source && (conf.type == 2 || conf.type == 3 || conf.type == 4)) {
						conf.sourceLst = conf.source.split(",");
					}
				}
				var eachdata = {
					"data" : $_regconfData
				};
				var html = template('regconflst_tpl', eachdata);
				$("#reg_tb_after").html(html);
			}else{
				$("#reg_tb_after").html('');
				$("#wrong").show()
				$("#wrongok").hide()
			}
		}
	});
	
}

template.helper('$addValidateFiled', function(conf) {
	if (conf.requiredVal == 2 || conf.validateVal == 2) {
		validateFiled.push(conf);
	}
});

function reset() {
	$('form')[0].reset();
}

function validate() {
	for (var i = 0; i < validateFiled.length; i++) {
		var filed = validateFiled[i];
		var val = getVal(filed);
		if(filed.regex){
			filed.regex = filed.regex.replace(/\\+/g,'\\');
			var regex = new RegExp(filed.regex);
		}

		if (filed.requiredVal == 2) {
			if (!val) {
				RC.alert(filed.name + "必须输入！",filed.key);
				return false;
			}
		}

		if (filed.validateVal == 2) {
			if (!regex.test(val)) {
				RC.alert(filed.name + "格式错误！",filed.key);
				return false;
			}
		}
	}
	return true;
}

function getVal(filed) {
	var val = "";
	if (filed.type == 1 || filed.type == 2 || filed.type == 5
			|| filed.type == 6) {
		val = $("#" + filed.key).val();
	} else if (filed.type == 3) {
		val = $("input[name='" + filed.key + "']:checked").val();
	} else if (filed.type == 4) {
		var vals = [];
		$("input[name='" + filed.key + "']:checked").each(function() {
			vals.push(this.value);
		})
		val = vals.join(",");
	}
	return val;
}

function getCommitData() {
	var data = {};
	for (var i = 0; i < $_regconfData.length; i++) {
		var filed = $_regconfData[i];
		var val = getVal(filed);
		data[filed.key] = val;
	}
	return data;
}