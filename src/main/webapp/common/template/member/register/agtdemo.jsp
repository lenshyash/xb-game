<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/agtregconf.do"></script>
<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>

<div style="margin: 0 auto; width: 900px;">
	<form action="" id="register">
		<table id="reg_tb">
			<tr>
				<td>用户名:</td>
				<td><input type="text" id="account"></input></td>
			</tr>
			<tr>
				<td>密码:</td>
				<td><input type="password" id="password"></input></td>
			</tr>
			<tr>
				<td>确认密码:</td>
				<td><input type="password" id="rpassword"></input></td>
			</tr>
		</table>
		<a href="#" onclick="commit();">提交</a> <a href="#" onclick="reset();">重置</a>
	</form>
</div>
<script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
			<tr>
				<td>{{tl.name}}:</td>
				<td>
				{{if tl.type==1}}
					<input type="text" id="{{tl.key}}"></input>
				{{/if}}
				{{if tl.type==2}}
					<select id="{{tl.key}}">
						{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
						{{/if}}
					</select>
				{{/if}}
				{{if tl.type==3}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as rad i}}
						<input type="radio" name="{{tl.key}}" value="{{i+1}}">{{rad}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<input type="checkbox" name="{{tl.key}}" value="{{i+1}}">{{chk}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<textarea id="{{tl.key}}"></textarea>
				{{/if}}
				{{if tl.type==6}}
					<input type="password" id="{{tl.key}}"></input>
				{{/if}}
				
				{{$addValidateFiled tl}}
				</td>
			</tr>
	{{/each}}
	</script>

<script type="text/javascript">
	function commit() {
		var account = $.trim($("#account").val());
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if (!account) {
			alert("用户名不能为空！");
			return;
		}
		if (!password) {
			alert("密码不能为空！");
			return;
		}
		if (!rpassword) {
			alert("确认不能为空！");
			return;
		}

		if (password !== rpassword) {
			alert("两次密码不一致！");
			return;
		}

		if (!validate()) {
			return;
		}
		var data = getCommitData();
		data["password"] = password;
		data["rpassword"] = rpassword;
		data["account"] = account;
		$.ajax({
			url : "${base}/agtregister.do",
			data : {
				data : JSON.encode(data)
			},
			success : function(result) {
				alert("注册成功！");
				window.location.href = "${base}/daili";
			}
		});
	}
</script>
