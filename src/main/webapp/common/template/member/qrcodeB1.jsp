<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-cn">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>${_title }</title>
    <link rel="shortcut icon" href="${base }/images/logo.ico" type="image/x-icon">
    <style>
        *{margin: 0px;padding: 0px;}
        body,html{max-width: 100%;}
		.inner-wrap{width: 80%;margin: 0 auto;position: relative;}
	
        ul li{list-style-type: none}
        a{cursor: pointer;}
        .outer-wrap{width: 100%;}
		
        .menu-wrap{height:69px;background: #030;color: #ffffff;}
        .menu-wrap .logo{display:inline-block;width: 59px;height: 49px;margin:10px 0 0 0;float:left;background: url("${base}/common/images/phoneB1/app1-logo-black.png");}
        .menu-wrap .logo-text{display:inline-block;height: 49px;line-height: 49px;margin: 10px 0 0 10px;float:left;font-size: 24px;font-family: "Microsoft YaHei","SimSun",Arial, Helvetica, sans-serif;}
        .main-content-wrap{display:bold;height:auto}
        .main-content .main-content-left{width:45%;float:left; padding:100px 0; }
        .main-content .main-content-left .demo-code-background{height:314px;width:228px;background: url("${base}/common/images/phoneB1/demo-code-background.png") no-repeat; margin:0 auto}
        .main-content .main-content-left .demo-code-background img{width:200px}
		.main-content .main-content-left .demo-code-background .demo-code-img{display:block;padding:35px 0 0 0;margin: 0 0 0 15px;}
        .main-content .main-content-left .demo-code-background .demo-code-text{text-align: center;font-size: 12px;color: #9fa0a0;line-height: 20px;margin: 10px 0 0 0;}
        .main-content .main-content-left .download-app{margin: 70px 0 0 0;position: relative;}
        .main-content .main-content-left .download-app ul li a{display: block;width: 178px;height: 59px;margin:30px auto 0;}
        .main-content .main-content-left .download-app ul li .download-android{background: url("${base}/common/images/phoneB1/download-android.png") no-repeat;}
        .main-content .main-content-left .download-app ul li .download-ios{background: url("${base}/common/images/phoneB1/download-ios.png") no-repeat;}
        .main-content .main-content-left .download-app ul li .download-android:hover{background: url("${base}/common/images/phoneB1/download-android-hover.png") no-repeat;}
        .main-content .main-content-left .download-app ul li .download-ios:hover{background: url("${base}/common/images/phoneB1/download-ios-hover.png") no-repeat;}
		 ul li img{width:280px;height:490px}
		 
        .main-content .main-content-right{float:left;width:50%; padding-left:50px; }
        .main-content .main-content-right .iphone-background{width: 338px;height: 663px;overflow:hidden;float:left;background: url("${base}/common/images/phoneB1/iphone.png") no-repeat;}
        .main-content .main-content-right .iphone-background .iphone-inner-img{float:left;margin: 110px 0 0 47px;}
        .main-content .main-content-right .android-background{width: 350px;height: 663px;overflow:hidden;float:left;background: url("${base}/common/images/phoneB1/androids.png") no-repeat;}
        .main-content .main-content-right .android-background .android-inner-img{float:left;margin: 80px 0 0 45px;overflow:hidden;}		
        .footer-wrap{display: block;height:40px;background: #030;color: #999;}
        .footer-wrap .footer{text-align: center;}
        .footer-wrap .footer p{display: block;text-align: center;margin:0 auto;padding: 10px 0 0 0;}
    	
		@media (max-width:480px){
                  .inner-wrap{width: 100%;}
				  .main-content .main-content-left{margin:0px;width:100%; padding:0}
				  .main-content .main-content-right {margin:0px; width:100%; padding:0}
				
           }
	</style>
</head>
<body>
<div id="container" style="position: fixed; width:100%; height:100%;z-index:-100"> 
    <div id="anitOut"></div>
</div>
    <div class="outer-wrap menu-wrap" style="color: rgb(255, 255, 255); background: none 0% 0% / auto repeat scroll padding-box border-box rgb(0, 51, 0);">
        <div class="tip-menu-small tip-change-background">
      
        </div>
        <div class="inner-wrap menu">
<!--             <a class="logo" id="logo" style=""></a> -->
            <span class="logo-text" id="logo_text" style="">${_title }</span>
        </div>
    </div>
    <div class="outer-wrap main-content-wrap" id="main_content" style="height: 801px; background: none 0% 0% / auto repeat scroll padding-box border-box rgba(0, 0, 0, 0);">
        <div class="inner-wrap main-content">
            <div class="main-content-left">
        
                <div class="demo-code-background">
                    <span class="demo-code-img"><img id="imgId" src="${base}/images/android.png"></span>
                    <p class="demo-code-text">
						扫描访问<font color="red" id="driveName">安卓</font>手机端APP
                    </p>
                </div>
                <div class="download-app">
                    <ul>
                        <li id="download_android" style="">
                        <a class="download-android" href="javascript:qiehuan('a')"></a></li>
                        <li id="download_ios" style="">
                        <a class="download-ios" href="javascript:qiehuan('i')"></a></li>
                    </ul>
                </div>
            </div>
            <div class="main-content-right">
                <div class="iphone-background" style="display:none">
                    <span class="iphone-inner-img" id="slider_iphone" style="overflow: hidden; position: relative;">
					<ul style="display: block; position: relative; margin: 0px; width: 9e+06px; transform: translate(0px, -192px); transition: transform 600ms ease-in-out;">
					<li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (5).png" style="height: 500px; width: 281px;"></li><li style="float: left; list-style: none; display: block; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (1).png" style="height: 500px; width: 281px;"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (2).png" style="height: 500px; width: 281px;"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (3).png" style="height: 500px; width: 281px;"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (4).png" style="height: 500px; width: 281px;"></li></ul>
                      </span><span id="controls"></span>
                </div>
				
                <div class="android-background" style="">
                    <span class="android-inner-img" id="slider_android" style="overflow: hidden; position: relative; height: 502px; width: 281px;">
						<ul style="display: block; position: relative; margin: 0px; width: 9e+06px; transform: translate(-561px, 0px); transition: transform 600ms ease-in-out;">
						<li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (5).png" style="height: 500px; width: 281px;"></li><li style="float: left; list-style: none; display: block; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (1).png"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (2).png" style="height: 500px; width: 281px;"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (3).png" style="height: 500px; width: 281px;"></li><li style="display: block; float: left; list-style: none; position: relative; margin: 0px;"><img src="${base}/common/images/phoneB1/1 (4).png" style="height: 500px; width: 281px;"></li></ul>
                      </span><span id="controls"></span>
                </div>		
            </div>
        </div>
    </div>
    <div class="outer-wrap footer-wrap" style="color: rgb(153, 153, 153); background: none 0% 0% / auto repeat scroll padding-box border-box rgb(0, 51, 0);">
        <div class="inner-wrap footer">
            <p>${copyright }</p>
        </div>
    </div>

<script type="Text/Javascript" src="${base}/common/js/phone/jquery.js"></script>
<script type="text/javascript" src="${base}/common/js/phone/jquery.sudoSlider.min.js"></script>
<script type="text/javascript">
	function qiehuan(t){
		if(t=='a'){
			$('#driveName').html('安卓');
			$("#imgId").attr("src","${base}/images/android.png");
		}else{
			$('#driveName').html('苹果');
			$("#imgId").attr("src","${base}/images/ios.png");
		}
	}
</script>
<script>

    $(document).ready(function(){
        //中间主内容区，高度自适应
        function auto_height()
        {           
                document.getElementById("main_content").style.height= document.body.scrollHeight+"px";         
        }
        auto_height();


       //根据 li的个数决定是否使用走马灯 
	   if ($("#slider_android ul li").size()>=2) {


		   $("#slider_android ul li").eq(0).show();
		   
			 //调用sudoslider插件，实现手机画面切换效果
	        var sudoSliderArdroid = $("#slider_android").sudoSlider({
	            speed:'600',
	            auto:true,
	            width:277,
	            height:493,
	            continuous:true,
	            prevNext:false
	        });
	   }else if ( $("#slider_android ul li").size()==1 ) {
		   //如果只有一张图片，则显示第一张图片
		   $("#slider_android ul li").eq(0).show();
	   }

       //根据 li的个数决定是否使用走马灯 
	   if ($("#slider_iphone ul li").size()>=2) {

		   $("#slider_iphone ul li").eq(0).show();
		   
			 //调用sudoslider插件，实现手机画面切换效果
	        var sudoSliderIphone = $("#slider_iphone").sudoSlider({
	            speed:'600',
	            auto:true,
	            continuous:true,
	            prevNext:false
	        });
	   }else if ( $("#slider_iphone ul li").size()==1 ) {
		   //如果只有一张图片，则显示第一张图片
		   $("#slider_iphone ul li").eq(0).show();
	   }	   



		var initMenuBackground = $(".menu-wrap").css('background');
		var initMenuColor = $(".menu-wrap").css('color');
		var initMainContentBackground = $(".main-content-wrap").css('background');
		var initFooterBackground = $(".footer-wrap").css('background');
		var initFooterColor = $(".footer-wrap").css('color');        
		var themeStyle0 = {
				menuBackground : initMenuBackground,
				menuColor : initMenuColor,
				mainContentBackground : initMainContentBackground,
				footerBackground : initFooterBackground,
				footerColor : initFooterColor
			}
			var themeStyle1 = {
				menuBackground : '#f6f7ff',
				menuColor : '#656565',
				mainContentBackground : '#626eb3',
				footerBackground :'#f6f7ff',
				footerColor :'#9297b6'
			}
			var themeStyle2 = {
				menuBackground : '#636a97',
				menuColor : '#ffffff',
				mainContentBackground : '#777eab',
				footerBackground :'#636a97',
				footerColor :'#9297b6'
			}
			var themeStyle3 = {
				menuBackground : '#49505f',
				menuColor : '#ffffff',
				mainContentBackground : 'url("${base}/common/images/phoneB1/1-4background.png") no-repeat',
				footerBackground :'#272b3c',
				footerColor :'#9fa0a0'
			}

			var changeTheme = function(themeStyle){
				$(".menu-wrap").css({'background':themeStyle.menuBackground,'color':themeStyle.menuColor});
				$(".main-content-wrap").css({'background':themeStyle.mainContentBackground});
				$(".footer-wrap").css({'background':themeStyle.footerBackground,'color':themeStyle.footerColor});
			}
		
			//根据用户的默认值设置模板
			var userDefaultTheme=0;
			
			switch (userDefaultTheme){
				case 1:
					userDefaultTheme = themeStyle1;
					break;
				case 2:
					userDefaultTheme = themeStyle2;
					break;
				case 3:
					userDefaultTheme = themeStyle3;
					break;
				default :
					userDefaultTheme = themeStyle0;
					break;
			}
			changeTheme(userDefaultTheme);

    });    
</script>

<script type="text/javascript">
$(function () {
    if (!window.ActiveXObject && !!document.createElement("canvas").getContext) {
	 $.getScript("http://im-img.qq.com/pcqq/js/200/cav.js?_=1428576021379",
       // $.getScript("js/cav.js?_=1428576021379",
                function () {
                    var t = {
                        width: 1.5,
                        height: 1.5,
                        depth: 10,
                        segments: 12,
                        slices: 6,
                        xRange: 0.8,
                        yRange: 0.1,
                        zRange: 1,
                        ambient: "#525252",
                        diffuse: "#FFFFFF",
                        speed: 0.0002
                    };
                    var G = {
                        count: 2,
                        xyScalar: 1,
                        zOffset: 100,
                        ambient: "#005500",
                        diffuse: "#005500",
                        speed: 0.001,
                        gravity: 1200,
                        dampening: 0.95,
                        minLimit: 10,
                        maxLimit: null,
                        minDistance: 20,
                        maxDistance: 400,
                        autopilot: false,
                        draw: false,
                        bounds: CAV.Vector3.create(),
                        step: CAV.Vector3.create(Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1))
                    };
                    var m = "canvas";
                    var E = "svg";
                    var x = {
                        renderer: m
                    };
                    var i, n = Date.now();
                    var L = CAV.Vector3.create();
                    var k = CAV.Vector3.create();
                    var z = document.getElementById("container");
                    var w = document.getElementById("anitOut");
                    var D, I, h, q, y;
                    var g;
                    var r;

                    function C() {
                        F();
                        p();
                        s();
                        B();
                        v();
                        K(z.offsetWidth, z.offsetHeight);
                        o()
                    }

                    function F() {
                        g = new CAV.CanvasRenderer();
                        H(x.renderer)
                    }

                    function H(N) {
                        if (D) {
                            w.removeChild(D.element)
                        }
                        switch (N) {
                            case m:
                                D = g;
                                break
                        }
                        D.setSize(z.offsetWidth, z.offsetHeight);
                        w.appendChild(D.element)
                    }

                    function p() {
                        I = new CAV.Scene()
                    }

                    function s() {
                        I.remove(h);
                        D.clear();
                        q = new CAV.Plane(t.width * D.width, t.height * D.height, t.segments, t.slices);
                        y = new CAV.Material(t.ambient, t.diffuse);
                        h = new CAV.Mesh(q, y);
                        I.add(h);
                        var N, O;
                        for (N = q.vertices.length - 1; N >= 0; N--) {
                            O = q.vertices[N];
                            O.anchor = CAV.Vector3.clone(O.position);
                            O.step = CAV.Vector3.create(Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1));
                            O.time = Math.randomInRange(0, Math.PIM2)
                        }
                    }

                    function B() {
                        var O, N;
                        for (O = I.lights.length - 1; O >= 0; O--) {
                            N = I.lights[O];
                            I.remove(N)
                        }
                        D.clear();
                        for (O = 0; O < G.count; O++) {
                            N = new CAV.Light(G.ambient, G.diffuse);
                            N.ambientHex = N.ambient.format();
                            N.diffuseHex = N.diffuse.format();
                            I.add(N);
                            N.mass = Math.randomInRange(0.5, 1);
                            N.velocity = CAV.Vector3.create();
                            N.acceleration = CAV.Vector3.create();
                            N.force = CAV.Vector3.create()
                        }
                    }

                    function K(O, N) {
                        D.setSize(O, N);
                        CAV.Vector3.set(L, D.halfWidth, D.halfHeight);
                        s()
                    }

                    function o() {
                        i = Date.now() - n;
                        u();
                        M();
                        requestAnimationFrame(o)
                    }

                    function u() {
                        var Q, P, O, R, T, V, U, S = t.depth / 2;
                        CAV.Vector3.copy(G.bounds, L);
                        CAV.Vector3.multiplyScalar(G.bounds, G.xyScalar);
                        CAV.Vector3.setZ(k, G.zOffset);
                        for (R = I.lights.length - 1; R >= 0; R--) {
                            T = I.lights[R];
                            CAV.Vector3.setZ(T.position, G.zOffset);
                            var N = Math.clamp(CAV.Vector3.distanceSquared(T.position, k), G.minDistance, G.maxDistance);
                            var W = G.gravity * T.mass / N;
                            CAV.Vector3.subtractVectors(T.force, k, T.position);
                            CAV.Vector3.normalise(T.force);
                            CAV.Vector3.multiplyScalar(T.force, W);
                            CAV.Vector3.set(T.acceleration);
                            CAV.Vector3.add(T.acceleration, T.force);
                            CAV.Vector3.add(T.velocity, T.acceleration);
                            CAV.Vector3.multiplyScalar(T.velocity, G.dampening);
                            CAV.Vector3.limit(T.velocity, G.minLimit, G.maxLimit);
                            CAV.Vector3.add(T.position, T.velocity)
                        }
                        for (V = q.vertices.length - 1; V >= 0; V--) {
                            U = q.vertices[V];
                            Q = Math.sin(U.time + U.step[0] * i * t.speed);
                            P = Math.cos(U.time + U.step[1] * i * t.speed);
                            O = Math.sin(U.time + U.step[2] * i * t.speed);
                            CAV.Vector3.set(U.position, t.xRange * q.segmentWidth * Q, t.yRange * q.sliceHeight * P, t.zRange * S * O - S);
                            CAV.Vector3.add(U.position, U.anchor)
                        }
                        q.dirty = true
                    }

                    function M() {
                        D.render(I)
                    }

                    function J(O) {
                        var Q, N, S = O;
                        var P = function (T) {
                            for (Q = 0, l = I.lights.length; Q < l; Q++) {
                                N = I.lights[Q];
                                N.ambient.set(T);
                                N.ambientHex = N.ambient.format()
                            }
                        };
                        var R = function (T) {
                            for (Q = 0, l = I.lights.length; Q < l; Q++) {
                                N = I.lights[Q];
                                N.diffuse.set(T);
                                N.diffuseHex = N.diffuse.format()
                            }
                        };
                        return {
                            set: function () {
                                P(S[0]);
                                R(S[1])
                            }
                        }
                    }

                    function v() {
                        window.addEventListener("resize", j)
                    }

                    function A(N) {
                        CAV.Vector3.set(k, N.x, D.height - N.y);
                        CAV.Vector3.subtract(k, L)
                    }

                    function j(N) {
                        K(z.offsetWidth, z.offsetHeight);
                        M()
                    }

                    C();
                })
    } else {
        alert('调用cav.js失败');
    }
});
</script>

</body></html>