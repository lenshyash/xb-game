<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" uri="/permission"%>
<jsp:include page="/common/include/base.jsp"></jsp:include>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }</title>
<!-- css common -->
<link rel="stylesheet" type="text/css" href="${base}/common/template/member/index/index__data/index.css">
<link rel="stylesheet" type="text/css" href="${base}/common/template/member/index/index__data/default.css">
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js"></script>
<style type="text/css">
pre {
	white-space: pre-wrap; /* css-3 */
	white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	word-wrap: break-word; /* Internet Explorer 5.5+ */
	word-break: break-all;
	overflow: hidden;
}
</style>
<style type="text/css">
.mask {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	opacity: 0.1;
	filter: alpha(opacity = 50);
	z-index: 99;
	display: none;
}

.mess {
	position: absolute;
	width: 600px;
	padding: 10px;
	border: 1px solid #ccc;
	background: #fff;
	text-align: center;
	z-index: 101;
}

.mess p {
	margin: 0;
	padding: 0;
}

.Poptop {
	overflow: hidden;
	height: 40px;
	line-height: 40px;
	font-family: microsoft yahei;
}

.Poptitle {
	color: #333;
	font-size: 14px;
	line-height: 40px;
	float: left;
}

.Popcontent img {
	width: 600px;
	height: 400px;
}

.closePopbtn {
	background: #E7D767;
	width: 80px;
	height: 24px;
	line-height: 24px;
	font-size: 14px;
	margin-top: 8px;
	float: right;
	border-radius: 5px;
	cursor: pointer;
	color: #333;
}

span.closePopbtn:hover {
	background: #E7D767;
}
</style>
</head>



<body>
	<ul id="TplFloatPic_0" class="TplFloatSet" style="position: absolute; cursor: pointer; z-index: 1000; top: 150px; left: 5px; width: 155px;" picfloat="left">
		<li><a href="http://messenger3.providesupport.com/messenger/0nxjukzi9xmr811k3sdlzwefb7.html" style="width: 125px; height: 350px;" id="adHref" target="_blank"> <img
				src="${base}/common/template/member/index/index__data/tu.png" alt="" class="png_fix" id="adImg" height="350" align="middle" width="125">
		</a></li>
		<li><a href="#" onclick="FloatClose(this);" style="width: 155px; height: 39px;"> <img src="${base}/common/template/member/index/index__data/143263530879.png" alt="" class="png_fix"
				height="39" align="middle" width="155"> <img src="${base}/common/template/member/index/index__data/143263690583.png" style="display: none;" alt="" class="png_fix" height="39" align="middle"
				width="145"></a></li>
	</ul>
	<style>
.TplFloatSet a {
	display: block;
	margin: 0 auto;
	text-align: center;
}

.TplFloatSet img {
	vertical-align: bottom;
}

.TplFloatSet li {
	list-style: none;
	font-size: 0px;
}
</style>
	<input id="userid" name="userid" value="w12345" type="hidden">
	<div id="topbg">
		<div class="top" style="overflow: hidden">
			<p:login login="false">
				<form style="margin: 0" autocomplete="off">
					<table>
						<tr>
							<td>用户名：<input type="text" id="account" class="text js" /></td>
							<td>密码：<input type="password" value="" id="password" class="text" /></td>
							<td>验证码： <input class="text" value="验证码" title="验证码" id="verifyCode" onfocus="if(this.value=='验证码')this.value=''" onblur="if(this.value=='')this.value='验证码'" type="text"> <img
								style="cursor: pointer; width: 50px; height: 20px;" src="${base}/verifycode.do" class="fl verifycode" id="vPic" align="absmiddle" onclick="refreshVerifyCode(this)">
							</td>

							<td><div class="submit">
									<a href="javascript:void(0)" onclick="javascript:doLogin(); " class="link">登录</a>
								</div></td>
							<td><a href="javascript:void(0)" onclick="javascript:window.location.href='${base}/regpage.do'" class="link">立即注册</a><a href="javascript:void(0)"
								onclick="javascript:window.location.href='${base}/agtregpage.do'" class="link">代理注册</a> <!-- <a href="javascript:void(0);" onclick="javascript: window.open('Forgetpwd.do', 'a', 'height=400, width=400, top=90, left=200,toolbar=no, menubar=no, scrollbars=no, resizable=no, location=yes, status=no');" class="link">忘记密码</a> -->
							</td>
						</tr>
					</table>
				</form>
				<script language="javascript">
					function refreshVerifyCode(img) {
						var time = (new Date()).getTime();
						img.src = "${base}/verifycode.do?time=" + time;
					}
					function doLogin() {
						var account = $.trim($("#account").val());
						var password = $("#password").val();
						if (!account) {
							alert("用户名不能为空！");
							return;
						}
						if (!password) {
							alert("密码不能为空！");
							return;
						}
				
						var data = {};
						data["password"] = password;
						data["account"] = account;
						data["verifyCode"] = $("#verifyCode").val();
						$.ajax({
							url : "${base}/login.do",
							type : "post",
							data : data,
							success : function(result) {
								parent.location.href = '${base}';
							}
						});
					}
				</script>
			</p:login>
			<p:login login="true">
				<div id="userinfo" style="width: 720px; float: left; height: 35px; line-height: 35px;">
					<strong>欢迎您,${loginMember.account }</strong>&nbsp;&nbsp;余额：
					<!-- ￥ -->
					<span class="col-advertis-red" id="Balance">${loginMember.money }RMB</span>&nbsp; <a href="javascript:void(0)" onclick="javascript:window.open('${base}/center/member/meminfo/page.do');">会员中心</a>&nbsp;|&nbsp;
					<a href="javascript:void(0)" onclick=" javascript:window.open('${base}/center/banktrans/mnychg/page.do');">额度转换</a>&nbsp;|&nbsp; <a href="javascript:void(0)"
						onclick=" javascript:window.open('${base}/center/banktrans/deposit/page.do');">在线存款</a>&nbsp;|&nbsp; <a href="javascript:void(0)"
						onclick=" javascript:window.open('${base}/center/banktrans/draw/page.do');">在线取款</a>&nbsp;|&nbsp; <a href="javascript:void(0)"
						onclick=" javascript:window.open('${base}/center/record/betrecord/page.do');">交易记录</a>&nbsp;|&nbsp; <a href="javascript:void(0)" onclick="javascript:window.location.href='${base}/logout.do';">登出</a>
				</div>
			</p:login>
			<ul>
				<li><a href="javascript:void(0);" onclick="setFirst('http://' + window.location.hostname) ">设为首页</a></li>
				<li class="line">|</li>
				<li><a href="javascript:void(0)" onclick="bookMarksite('http://' + window.location.hostname, document.title);">加入最爱</a></li>
				<li class="line">|</li>
				<li><a id="customerServiceUrlLink"
					href="javascript:%20window.open('http://messenger3.providesupport.com/messenger/0nxjukzi9xmr811k3sdlzwefb7.html',%20'%E5%9C%A8%E7%BA%BF%E5%AE%A2%E6%9C%8D'%20,%20'width=670,height=580,top=0,left=0,status=no,toolbar=no,scrollbars=yes,resizable=no,personalbar=no')"
					style="color: #ff0000;">在线客服</a></li>
				<li><a href="javascript:void(0)"><img src="${base}/common/template/member/index/index__data/icon_lang02.jpg"></a></li>
			</ul>
		</div>
	</div>

	<div id="headerbg">
		<div class="header">
			<div class="nav">
				<ul style="width: 732px;">
					<li style="left: 8px;" class="move" id="topmove"></li>
					<li class="on" id="LS-index"><a style="color: rgb(102, 47, 2);" href="javascript:void(0)" onclick="f_com.getPager('-', 'index_.do' , '' ); ">首页<br> <span>HOME</span></a></li>
					<li class="line"></li>
					<li id="LS-lottery"><a style="color: red;" href="javascript:void(0)" onclick="window.location.href='${base}/lottery/index.do'">彩票游戏<br> <span>LOTTERY</span></a></li>
					<li class="line"></li>
					<li id="LS-live"><a style="color: red;" href="javascript:void(0)" onclick="window.location.href='${base}/live.do'">真人娱乐<br> <span>LIVE DETEAR</span></a></li>
					<li class="line"></li>
					<li id="LS-sport"><a style="color: red;" href="${base}/sports/hg/index.do">体育赛事<br> <span>SPORT</span></a></li>
					<li class="line"></li>
					<li id="LS-lhc"><a style="color: red;" href="javascript:void(0)" onclick="window.location.href='${base}/marksix/index.do'">六合彩<br> <span>LHC</span></a></li>
					<li class="line"></li>
					<li id="LS-eslotgame"><a style="color: red;" href="javascript:void(0)" onclick="window.location.href='${base}/egame.do'">电子游艺<br> <span>CASINO</span></a></li>
					<li class="line"></li>
					<li id="LS-activity"><a href="javascript:void(0)" onclick="f_com.getPager('-', 'activity.do' , '' ); ">优惠活动<br> <span>PROMOTIONS</span></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="banner">
		<!-- notice -->
		<div class="notice">
			<div class="w_1100">
				<div id="news">
					<h2>公告NEWS:</h2>
					<div id="announ">
						<marquee id="notice_content" behavior="scroll" scrollamount="3" scrolldelay="150" direction="left" onmouseover="this.stop();" onmouseout="this.start();" onclick="// HotNewsHistory();"
							style="cursor: pointer; width: 500px; margin: 0; height: 30px; color: #fff;">尊敬的会员：由于入款账号会不定期更新，请入款前，务必点击【线上存款】查看最新入款账号，以免存入过期账号哦！</marquee>
					</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
		$(function(){
			getArticle();
		})
		//最新公告
		function getArticle(){
			$.ajax({
				url:'${base}/getConfig/getArticle.do',
				data:{code:13},
				success:function(r){
					var notice_content="";
					for(var i=0;i<r.length;i++){
						notice_content += r[i].content+"    ";
					}
					$("#notice_content").text(notice_content);
				}
			})
		}
		</script>

		<div class="fullSlide">
			<div class="bd">
				<ul style="position: relative; width: 1411px; height: 502px;" class="bannerIndex">
					<li style="position: absolute; width: 1411px; left: 0px; top: 0px; background-image: url(&quot;http://chuantu.biz/t3/18/1461158202x3738746595.jpg&quot;); display: none;"></li>
					<li style="position: absolute; width: 1411px; left: 0px; top: 0px; background-image: url(&quot;http://chuantu.biz/t3/18/1461158287x3738746595.jpg&quot;); display: list-item;"></li>
					<li style="position: absolute; width: 1411px; left: 0px; top: 0px; background-image: url(&quot;http://chuantu.biz/t3/18/1461158395x3738746595.jpg&quot;); display: none;"></li>
				</ul>
			</div>
			<div class="hd">
				<ul>
					<li class="">1</li>
					<li class="on">2</li>
					<li class="">3</li>
				</ul>
			</div>
			<span class="prev"></span> <span class="next"></span>
		</div>
	</div>
	<div id="mainbg">
		<div class="main">
			<div class="items">
				<div class="item">
					<a href="javascript:void(0)" onclick="f_com.getPager('-', 'lottery.do' , '' ); "><img src="${base}/common/template/member/index/index__data/icon_index03.png"></a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" onclick="f_com.getPager('-', 'live.do' , '' ); "><img src="${base}/common/template/member/index/index__data/icon_index01.png"></a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" onclick="f_com.getPager('-', 'eslotGame.do' , '' ); "><img src="${base}/common/template/member/index/index__data/icon_index02.png"></a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" onclick="f_com.getPager('-', 'sport.do' , '' ); "><img src="${base}/common/template/member/index/index__data/icon_index04.png"></a>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="bg">
			<div class="top">
				<div class="footlogo" style="position: relative;">
					<img src="${base}/common/template/member/index/index__data/foot_logo.png">
				</div>
			</div>
			<div class="bottom">
				<div class="link">
					<ul class="nav">
						<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'about.do' , '' ); ">关于我们</a></li>
						<li class="line">|</li>
						<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'teller.do', '');">取款帮助</a></li>
						<li class="line">|</li>
						<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'desposit.do', '');">存款帮助</a></li>
						<li class="line">|</li>
						<!-- 					<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'cooperation.do' , '' ); ">合作伙伴</a></li> -->
						<!-- 					<li class="line">|</li> -->
						<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'contact.do' , '' ); ">联系我们</a></li>
						<li class="line">|</li>
						<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'generalq.do', '');">常见问题</a></li>
					</ul>
					<p>58娱乐城所提供的产品和服务，是有菲律宾政府卡格扬河经济特区 FIRST CAGAYN LEISURE AND RESORT CORPORATION 授权和监督</p>
					<p>我们将不余遗力的为您提供优质的服务和可靠的资金保障</p>
					<!-- 				<p>Copyright © 2006-2026 58娱乐城赌场 Reserved　　e-mail:helpvnsr@gmail.com</p> -->
				</div>

				<div class="info">
					<div class="list service">
						<h2>
							<img src="${base}/common/template/member/index/index__data/footer_title01.png">
						</h2>
						<div>
							<dl class="pro m_in">
								<dt>18</dt>
								<dd class="progress" style="width: 50px;"></dd>
								<dd class="opacity"></dd>
								<dd></dd>
							</dl>
							<dl class="pro m_out">
								<dt>2`18</dt>
								<dd class="progress" style="width: 171px;"></dd>
								<dd class="opacity"></dd>
								<dd></dd>
							</dl>
							<dl class="mon">
								<dt>
									<span>35</span>
								</dt>
								<dd>
									<img src="${base}/common/template/member/index/index__data/pic_shangjia.png">
								</dd>
							</dl>
						</div>
					</div>
					<div class="list product">
						<h2>
							<img src="${base}/common/template/member/index/index__data/footer_title02.png">
						</h2>
						<dl>
							<dt>
								快乐彩 <span>KENO</span>
							</dt>
							<dd>游戏玩法丰富，高返奖高赔率，中奖率更高，为用户提供更激情的体验</dd>
						</dl>
						<dl>
							<dt>
								体育平台 <span>SPORT BET</span>
							</dt>
							<dd>在一个公平、公正的环境下进行游戏，能够让您轻松、怡静的享受体育投注的乐趣。</dd>
						</dl>
						<dl>
							<dt>
								真人娱乐城 <span>LIVE CASINO</span>
							</dt>
							<dd>想要让自己一夜致富的梦想成真吗?不要错过机会，下一个千万富翁很可能就是您。</dd>
						</dl>
					</div>
					<div class="list help">
						<h2>
							<img src="${base}/common/template/member/index/index__data/footer_title03.png">
						</h2>
						<dl>
							<dt>
								颠覆性产品 <span>SUBVERSIVE PRODUCTS</span>
							</dt>
							<dd>58娱乐城在31/08/2010拥有英国属地马恩岛颁发的执照并受其监督。</dd>
						</dl>
						<dl>
							<dt>
								合作伙伴 <span>PARTNERS</span>
							</dt>
							<dd>
								<img src="${base}/common/template/member/index/index__data/pic_logo.png">
							</dd>
						</dl>
						<dl>
							<dt>
								服务帮助 <span>USE HELP</span>
							</dt>
							<dd>
								<ul>
									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'about.do' , '' ); ">关于我们</a></li>
									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'teller.do', '');">取款帮助</a></li>
									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'desposit.do', '');">存款帮助</a></li>
									<!-- 									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'cooperation.do' , '' ); ">合作伙伴</a></li> -->
									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'contact.do' , '' ); ">联系我们</a></li>
									<li><a href="javascript:void(0)" onclick="f_com.getPager('-', 'generalq.do', '');">常见问题</a></li>
								</ul>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>