+function(fn){fn(window.jQuery,window,document)}(function($,w,d){
	


	

	$(function(){
		
		$('input.js').focus(function(){
            var txt_value = $(this).val();
            if(txt_value == this.defaultValue){$(this).val('');}
        }).blur(function(){
            var txt_value = $(this).val();
            if(txt_value == ''){$(this).val(this.defaultValue);}
        });
		 $("#showPwd").focus(function() {  
            var text_value = $(this).val();  
            if (text_value == this.defaultValue) {$("#showPwd").hide(); $("#password").show().focus();}  
        });  
        $("#password").blur(function() {  
            var text_value = $(this).val();  
            if (text_value == "") {$("#showPwd").show();$("#password").hide();}  
        }); 
	


		var $el, leftPos,$move = $("#headerbg .nav .move"),$on = $move.parent().find('.on');
		var moveSize = $move.width();
		var liSize = $move.siblings().not('.line').width();
 		if(!$on.length){
 			$move.css("left", -moveSize).data("origLeft",-moveSize);
 	 	}else{
	 		 $move.css("left", $on.position().left-(moveSize-liSize) / 2).data("origLeft", $on.position().left); 
 	 	}
	    $move.siblings().find('>a').hover(function() {
	        $el = $(this);
	        if(!$el.parent().hasClass('on')) $('#headerbg .nav ul li.on a').stop(true,false).animate({'color':'#1f1f1f'});
	        leftPos = $el.parent().position().left;
	        $move.stop(true,false).animate({
	            left: leftPos-(moveSize-liSize)/2
	        },{duration:200});
	        $el.stop(true,false).animate({'color':'#662f02'});
	    }, function() {
	    	if(!$el.parent().hasClass('on')) $el.stop(true,false).animate({'color':'#1f1f1f'});
	        $move.stop(true,false).animate({
	            left: $move.data("origLeft") -(moveSize-liSize)/2
	        },{duration:200});   
	        $('#headerbg .nav ul li.on a').stop(true,false).animate({'color':'#662f02'});
	    });

		//if($(".progress")[0]){progressbar([18,138,35],1000); }



		
		
	});
});

function progressbar(arr,delay){
	var divL1 = $('.m_in .progress')[0];
	var divL2 = $('.m_out .progress')[0];
	var s1 = $('.m_in dt')[0];
	var s2 = $('.m_out dt')[0];
	var s3 = $('.mon dt span')[0];
	var load1 =load2=load3=0;
	var n1 = arr[0];
	var n2 = arr[1];
	var n3 = arr[2];
	var htmlnum;
	var htmlstr;
	timer=setInterval(function(){
		load1++;
		divL1.style.width =load1+'px';
		s1.innerHTML = Math.round(load1*n1/100);
		if(load1==100){clearInterval(timer);}
	},30);
	setTimeout(function(){
		timer2=setInterval(function(){
			load2++;
			htmlnum = Math.round(load2*n2/100);
			if(htmlnum>=60){
				htmlstr = Math.floor(htmlnum/60) + '`' + (htmlnum - Math.floor(htmlnum/60)*60);
			}else{
				htmlstr = htmlnum;
			}
			divL2.style.width = n1 > n2 ? Math.round(load2*0.5)+'px' : Math.round(load2*1.5)+'px';
			s2.innerHTML = htmlstr;
			if(load2==100){ 
				clearInterval(timer2);
			}
		},30);
	},delay);
	setTimeout(function(){
		timer3=setInterval(function(){
			load3++;
			s3.innerHTML = Math.round(load3*n3/100);
			if(load3==100){clearInterval(timer3);	}
		},30);
	},delay*2);
 }	