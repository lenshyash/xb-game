﻿var base;
$(function() {
	base = $("#base").val();

	reloadImg();
	$('#authnumImg').click(reloadImg);
	$('#rauthnumImg').click(reloadImg);
})


// 验证码
function reloadImg() {
	var url = base + "/verifycode.do?timestamp=" + (new Date().getTime());
	$('#authnumImg').attr("src", url);
	$('#rauthnumImg').attr("src", url);
}

//公共公告页面
function newWinOpen() {
	var features = 'height=600,width=800,top=0, left=0,scrollbars=yes,resizable=yes';
    window.open(base+'/index/news.do','HotNewsHistory', features);
}

// 游客测试账号
/**
 * id = A a站 B b站
 */
function testAccount(id) {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/registerTestGuest.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if (id == 'A') {
					location.href = base + "/lottery/index.do";
				} else {
					location.href = base + "/index.do";
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//iframe游客测试账号
/**
 * id = A a站 B b站
 */
function iframeTestAccount(id) {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/registerTestGuest.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if (id == 'A') {
					top.location.href = base + "/lottery/index.do";
				} else {
					top.location.href = base + "/index.do";
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

function mfsw() {
	$.ajax({
		url : base + "/registerGuestPage.do",
		type : "post",
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + '/lottery/index.do';
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}


// A登录
function doLoginA() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(!result.success){
					reloadImg();
					alert(result.msg);
				}else{
					location.href = base + '/lottery/index.do';
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A登录
function doLoginAV3() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + '/lotteryV3/index.do';
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B登录
function doLoginB() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if (!result.success){
					alert(result.msg)
				}
					location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A注册
function commitA() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/lottery/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//V3注册
function commitV3A() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/lotteryV3/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B注册
function commitB() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//A注册
function iframeCommitA() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/lottery/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B注册
function iframeCommitB() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 代理注册
function iframedlcommit() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/agtregister.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/daili";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 代理注册
function dlcommit() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/agtregister.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/daili";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 未登录，请先登录再进行游戏
function wdl() {
	alert("请先登录，再进行游戏！");
}

//刷新账户余额
function refreshmoney() {
	$.ajax({
		url : base + "/curstinfo.do",
		type : "post",
		success : function(result) {
			$("#money_span").html(result.member.money);
		}
	});
}

/** 加入收藏* */
function addFavorite() {
	var url = window.location.href;
	var title = document.title;
	var ua = navigator.userAgent.toLowerCase();
	if (ua.indexOf("360se") > -1) {
		alert("由于360浏览器功能限制，请按 Ctrl+D 手动收藏！");
	} else if (ua.indexOf("msie 8") > -1) {
		window.external.AddToFavoritesBar(url, title); // IE8
	} else if (document.all) {
		try {
			window.external.addFavorite(url, title);
		} catch (e) {
			alert('加入收藏失败，请使用Ctrl+D进行添加，或手动在浏览器里进行设置！');
		}
	} else if (window.sidebar) {
		window.sidebar.addPanel(title, url, "");
	} else {
		alert('加入收藏失败，请使用Ctrl+D进行添加，或手动在浏览器里进行设置！');
	}
}

/**
 * 设为首页 author: adaf Modified by: adaf
 */
function setHomepage() {
	try {
		document.body.style.behavior = 'url(#default#homepage)';
		document.body.setHomePage(window.location.href);
	} catch (e) {
		try {
			netscape.security.PrivilegeManager
					.enablePrivilege('UniversalXPConnect');
		} catch (e1) {
			try { // Firefox
				var prefs = Components.classes['@mozilla.org/preferences-service;1']
						.getService(Components.interfaces.nsIPrefBranch);
				prefs.setCharPref('browser.startup.homepage',
						window.location.href);
			} catch (e2) {
				alert("您好，您的浏览器不支持自动设置页面为首页功能，请您手动在浏览器里设置该页面为首页！");
			}
		}
	}
}

function tabColor(id) {
	var str = $("#"+id).attr("datacolor").split("||");
	var co1 = str[0];
	var co2 = str[1];
//	$("#" + id).css("color", co1);
	var color = $("#" + id).css("color");
	if (color == co1) {
		$("#" + id).css("color", co2);
	} else {
		$("#" + id).css("color", co1);
	}
}

/**文字闪烁*/
//toggleColor('.textGlitter', [ 'yellow', '#FF0000' ], 600);
//function toggleColor( id , arr , s ){
//    var self = this;
//    self._i = 0;
//    self._timer = null;
//    
//    self.run = function(){
//        if(arr[self._i]){
//            $(id).css('color', arr[self._i]);
//        }
//        self._i == 0 ? self._i++ : self._i = 0;
//        self._timer = setTimeout(function(){
//            self.run( id , arr , s);
//        }, s);
//    }
//    self.run();
//}

/**
 * 首页需要一些第三方支持
 * @param type
 *            1,cp 2dz 3,agdz 4,mgdz
 * @param id
 */
function typeLogin(type, id) {
	var username = $("#user").val();
	if (username == "") {
		alert("请先登录再进行游戏");
	} else {
		if (type == 1) {
			window.location.href = base + "/index/games.do?lotCode=" + id;
		} else if (type == 2) {
			window.location.href = base + "/index/egame.do";
		} else if (type == 3) {
			toAG(id);
		} else {
			toMG(id);
		}
	}
}

//时间
var estObj = {
	pre0 : function(num) {
		if (num < 10) {
			num = '0' + num;
		}
		return num;
	},
	/* 即時時間顯示 */
	dispTime : function() {
		// var nowNew = (estObj.now += 1000),
		var dateObj = new Date(), p0 = estObj.pre0, Y = dateObj.getFullYear(), Mh = dateObj
				.getMonth() + 1, D = p0(dateObj.getDate()), H = p0(dateObj
				.getHours()), M = p0(dateObj.getMinutes()), S = p0(dateObj
				.getSeconds());

		if (Mh > 12) {
			Mh = 01;
		} else if (Mh < 10) {
			Mh = '0' + Mh;
		}

		document.getElementById('vlock').innerHTML = '北京时间：' + Y + '年' + Mh
				+ '月' + D + '日 ' + H + ':' + M + ':' + S;
	}
};
//(function() {
//	setInterval(estObj.dispTime, 1000);
//}());


//var i,x,y,z,crand,xrand;
//$(document).ready(function() {
//  	//x上限，y下限
//  	i = 8353221;
//    x = 10000.99;
//    y = 100.00;
//    z = 99;	//小数
//    c = 00;	//小数
//    $("#caijin").html(i);
//	setInterval("caijin()", 3000);
//})
function caijin(){
	rand = parseInt(Math.random() * (x - y + 1) + y);
	xrand = parseInt(Math.random() * (z - c + 1) + c);
	i = rand+i;
	var caijin = i+'.'+ xrand;
   $("#caijin").html(caijin);
}


function cz(obj){
	var lotName = '';
	switch(obj){
	case 'CQSSC':
		lotName = "重庆时时彩";
		break;
	case 'HNFFC':
		lotName = "河内分分彩";
		break;
	case 'HNWFC':
		lotName = "河内五分彩";
		break;
	case 'AZXY5':
		lotName = "澳洲幸运5";
		break;
	case 'PL3':
		lotName = "排列三";
		break;
	case 'SH11X5':
		lotName = "上海11选5";
		break;
	case 'GX11X5':
		lotName = "广西11选5";
		break;
	case 'FC3D':
		lotName = "福彩3D";
		break;
	case 'BJSC':
		lotName = "北京赛车";
		break;
	case 'EFC':
		lotName = "二分彩";
		break;
	case 'WFC':
		lotName = "五分彩";
		break;
	case 'HKWFC':
		lotName = "香港五分彩";
		break;
	case 'AMWFC':
		lotName = "澳门五分彩";
		break;
	case 'SFC':
		lotName = "十分彩";
		break;
	case 'ESFC':
		lotName = "二十分彩";
		break;
	case 'XJSSC':
		lotName = "新疆时时彩";
		break;
	case 'PCEGG':
		lotName = "PC蛋蛋";
		break;
	case 'JX11X5':
		lotName = "江西11选5";
		break;
	case 'GD11X5':
		lotName = "广东11选5";
		break;
	case 'SD11X5':
		lotName = "山东11选5";
		break;
	case 'TJSSC':
		lotName = "天津时时彩";
		break;
	case 'FFC':
		lotName = "分分彩";
		break;
	case 'HNKLSF':
		lotName = "湖南快乐十分";
		break;
	case 'GDKLSF':
		lotName = "广东快乐十分";
		break;
	case 'JSSB3':
		lotName = "江苏骰宝(快3)";
		break;
	case 'XYFT':
		lotName = "幸运飞艇";
		break;
	case 'LXYFT':
		lotName = "老幸运飞艇";
		break;
	case 'CQXYNC':
		lotName = "重庆幸运农场";
		break;
	case 'AHK3':
		lotName = "安徽快三";
		break;
	case 'HBK3':
		lotName = "湖北快三";
		break;
	case 'SHHK3':
		lotName = "上海快三";
		break;
	case 'HEBK3':
		lotName = "河北快三";
		break;
	case 'GXK3':
		lotName = "广西快三";
		break;
	case 'SFLHC':
		lotName = "十分六合彩";
		break;
	case 'TMLHC':
		lotName = "三分六合彩";
		break;
	case 'WFLHC':
		lotName = "五分六合彩";
		break;
	case 'HKMHLHC':
		lotName = "香港马会六合彩";
		break;
	case 'JND28':
		lotName = "加拿大28";
		break;
	case 'BJK3':
		lotName = "北京快三";
		break;
	case "GSK3":
		lotName = "甘肃快三";
		break;
	case "JXK3":
		lotName = "江西快三";
		break;
	case "FFK3":
		lotName = "极速快三";
		break;
	case "WFK3":
		lotName = "幸运快三";
		break;
	case "JPK3":
		lotName = "日本快三";
		break;
	case "KRK3":
		lotName = "韩国快三";
		break;
	case "HKK3":
		lotName = "香港快三";
		break;
	case "AMK3":
		lotName = "澳门快三";
		break;
	case "SFSC":
		lotName = "极速赛车";
		break;
	}
	return lotName;
}

// 回车按键
//document.onkeydown = function (event) {
//    var e = event || window.event || arguments.callee.caller.arguments[0];
//    if (e && e.keyCode == 13) { 
//    	doLoginB();
//    }
//};

//$(window).resize();
//$("#hongbao").animate({
//    left:$(window).width()*0.183
//},150)
//$(window).resize(function(){
//    $("#hongbao").stop(true,true).animate({
//        left:$(window).width()*0.183
//    },150)
//});

//$(".activity-box-header").click(function () {
//    $(this).parents(".activity-box").siblings().find(".activity-box-body").hide();
//    $(this).next(".activity-box-body").toggle();
//})

/*-----------聊天室start--------------*/
var service = "https://chat.xbxb555.com/";
//var service = "http://localhost:8080/chat/";
var tokenUrl = "getToken";
function openChat(isLogin,platformId,ptToken,level,name,grade){
	if(!isLogin){
		var chatUrl= service+"?platId="+platformId;
	    openwindow(chatUrl,"balabala聊天室","1380","600");
		return false;
	}
	var token;
	$.ajax({
		async:false,
		type: "get",
		url: service+tokenUrl,
		data: {platformId:platformId,token:ptToken,level:level,name:name,grade:grade},
		dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
        		token=data.data;
        },
	    error:function(data){
            throw new Error('请求失败:'+data.msg);
         }
	})	
	var chatUrl= service+"?token="+token;
    openwindow(chatUrl,"balabala聊天室","1380","600");
}
function openwindow(url,name,iWidth,iHeight) {  
	var iTop = (window.screen.height-30-iHeight)/2; //获得窗口的垂直位置;  
	var iLeft = (window.screen.width-10-iWidth)/2; //获得窗口的水平位置;  
	window.open(url,name,'height='+iHeight+',innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');  
}  
/*-----------聊天室end--------------*/
