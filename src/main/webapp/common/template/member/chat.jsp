<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<title>${_title }</title>
<head>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
</head>
<body style="margin: 0;padding: 0;overflow:scroll;overflow:hidden;">

<input type="hidden" id="platId" value="${platId }">
<input type="hidden" id="platToken" value="${platToken }">
<input type="hidden" id="account" value="${loginMember.account }">
<input type="hidden" id="levelName" value="${loginMember.levelName }">
<input type="hidden" id="isLogn" value="${isLogin }">
<%-- ${isLogin }<br> --%>

<script type="text/javascript">

var service = "https://chat.xbxb555.com/";
var chatUrl;
// var service = "http://localhost:8080/chat/";
$(function(){
	if(window.location.search.substring(1,6) == 'token'){
		$("#chat").attr('src',service + window.location.search)
	}
	var tokenUrl = "getToken";
	var platId = $("#platId").val();
	var platToken = $("#platToken").val();
	var account = $("#account").val();
	var levelName = $("#levelName").val();
	var isLogn = $("#isLogn").val();
	
	if(isLogn == "false"){
		chatUrl = service+"?platId="+platId;
	}else{
		var token;
		$.ajax({
			async:false,
			type: "get",
			url: service+tokenUrl,
			data: {platformId:platId,token:platToken,level:'0',name:account,grade:levelName},
			dataType: 'json',
	      xhrFields: {
	          withCredentials: true
	      },
	      success: function (data) {
	      		token=data.data;
	      		localStorage.setItem('sessToken',token)
	      },
		    error:function(data){
	          throw new Error('请求失败:'+data.msg);
	       }
		})
		chatUrl= service+"?token="+token;
	}
	$("#chat").attr("src",chatUrl);
})

</script>

<iframe id="chat" src="" style="width: 100%;height: 100%;"></iframe>
</body>
</html>