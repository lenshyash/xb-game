<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>RSA密钥生成</title>
<meta name="renderer" content="webkit">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<style type="text/css">
	.row {
		width: 100%;
		margin-top: 10px;
	}
	.content {
		word-wrap: break-word;
		/*word-break设置强行换行;normal 亚洲语言和非亚洲语言的文本规则，允许在字内换行word-break: normal;*/
	}
</style>
</head>
<body>
<div class="row">
	<span>私钥：</span>
	<div class="content">${privateKey }</div>
</div>
<div class="row">
	<span>公钥：</span>
	<div class="content">${publicKey }</div>
</div>
</body>
</html>

