<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${base}/common/template/member/news/js/jquery-1.8.3.min.js"></script>
<link href="${base}/common/template/member/news/css/standard.css" rel="stylesheet"
	type="text/css">
<link href="${base}/common/template/member/news/css/HotNewsHistory.css" rel="stylesheet"
	type="text/css">
<style type="text/css">
#top {
	background: url(${base}/common/template/member/news/images/bg_t.png) left top no-repeat;
	width: 720px;
	height: 58px;
}

#container {
	background: url(${base}/common/template/member/news/images/bg_c.png) left top repeat-y;
	margin: 0 auto;
	padding: 0 37px;
	width: 646px;
}

#footer {
	background: url(${base}/common/template/member/news/images/bg_f.png) left top no-repeat;
	padding-top: 20px;
	width: 720px;
	height: 35px;
}
</style>
</head>
<body>
	<div id="main-wrap">
		<div id="top">
			<div id="title"></div>
		</div>
		<div id="container">
			<div class="content">
				<div class="date">
					<span class="content-title">日期</span>
				</div>
				<div class="msg">
					<span class="content-title">内容</span>
				</div>
				<div class="clear"></div>
			</div>
			<div class="line"></div>
<!-- 			<div class="content"> -->
<!-- 				<div class="date"> -->
<!-- 					<span class="inner">2017-07-31 23:12:01</span> -->
<!-- 				</div> -->
<!-- 				<div class="msg"> -->
<!-- 					<p class="inner-title">尊敬的客户您好：AGQ平台例行维护已完成，可正常游戏，如有疑问请联系在线客服。给您带来不便敬请谅解，祝您游戏愉快！</p> -->
<!-- 				</div> -->
<!-- 				<div class="clear"></div> -->
<!-- 			</div> -->
<!-- 			<div class="line"></div> -->
		</div>
		<div id="footer"></div>
	</div>
	<script type="text/javascript">
		$(function(){
			$.ajax({
				url : "${base}/getConfig/getArticle.do",
				data:{code:13},
				type : "post",
				dataType : 'json',
				success : function(r) {
					var col='';
					for(var i=0;i<r.length;i++){
						col+='<div class="content">';
						col+='<div class="date">';
						col+='<span class="inner">'+timeStamp2String(r[i].updateTime)+'</span>';
						col+='</div>';
						col+='<div class="msg">';
						col+='<p class="inner-title">'+r[i].content+'</p>';
						col+='</div>';
						col+='<div class="clear"></div>';
						col+='</div>';
						col+='<div class="line"></div>';
					}
					$("#container").append(col);
				}
			});
		})
		
		function timeStamp2String(time){  
			var datetime = new Date();
			datetime.setTime(time);
			var year = datetime.getFullYear();
			var month = datetime.getMonth() + 1 < 10 ? "0"
					+ (datetime.getMonth() + 1) : datetime.getMonth() + 1;
			var date = datetime.getDate() < 10 ? "0" + datetime.getDate()
					: datetime.getDate();
			var hour = datetime.getHours() < 10 ? "0" + datetime.getHours()
					: datetime.getHours();
			var minute = datetime.getMinutes() < 10 ? "0"
					+ datetime.getMinutes() : datetime.getMinutes();
			var second = datetime.getSeconds() < 10 ? "0"
					+ datetime.getSeconds() : datetime.getSeconds();
			return year + "-" + month + "-" + date + " " + hour + ":" + minute
					+ ":" + second;
		}
	</script>
</body>
</html>