<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<input type="hidden" id="plat" value="${chatPlatToken}">
<input type="hidden" id="accounts" value="${loginMember.account }">
<input type="hidden" id="levelName" value="${loginMember.levelName }">
<input type="hidden" id="isLogn" value="${isLogin }">
<%-- ${isLogin }<br> --%>
<c:choose>
	<c:when test="${domainFolder == 'a108'}">
		<a id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		<img src="https://tpxb.me/a108/3.png" style="width:100%" id="openChatImg" />
		</div>
	</c:when>
	<c:when test="${domainFolder == 'd00527'}">
		<a id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		<img src="https://xb336.com/common/template/member/center/img/15166769590370633.png" style="width:100%" id="openChatImg" />
		</div>
	</c:when>
	<c:when test="${domainFolder == 'b00301'}">
		<a id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		</div>
	</c:when>
	<c:when test="${domainFolder == 'd00308'}">
		<a style="display: none;" id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		<img src="${base}/common/template/member/center/img/1516676959037063.png" style="width:100%" id="openChatImg" />
		</div>
	</c:when>
	<c:when test="${domainFolder == 'd00311'}">
		<a id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		<img src="https://xb336.com/common/template/member/center/img/15166769590370633.png" style="width:100%" id="openChatImg" />
		</div>
	</c:when>
	<c:otherwise>
		<a id="locaChatLink" target="_blank"><div id="chatImgModule" style="position:fixed;right:10px;top:15px;width:90px;height:90px;z-index:99999;cursor: pointer;">
		<img src="${base}/common/template/member/center/img/1516676959037063.png" style="width:100%" id="openChatImg" />
		</div>
	</c:otherwise>
</c:choose>
</a>
<div id="chatIframeModule" onmousedown="startDrag()">
	<img id="closeChat"
		src="${base}/common/template/member/center/img/close.png" />
	<iframe style="height: 100%; width: 100%;" id="chatIframe" src=""></iframe>
</div>
<script type="text/javascript">
	var plat =  $("#plat").val();
	var platId = plat.substring(0,plat.indexOf(','))
	var platToken = plat.substring(plat.indexOf(',')+1)
	var service = "https://chat.xbxb555.com/";
	var chatUrl;
	var leftIs = 0;
	// var service = "http://localhost:8080/chat/";
	$(function() {
		/* openChatIframe(0) */
		$("#closeChat").click(function() {
			// openChatNewPage()
			$("#chatIframeModule").css('transition', '.5s').css('left', '100%')
			$(this).hide(500)
			localStorage.setItem("wi",'1');
			leftIs = 1;
		})
		$('#locaChatLink').click(function(){
			openChatNewPage()
			if($('#isLogn').val() == 'false'){
				alert('请先登录')
			}
		})
	})
	function openChatIframe(is) {
		$("#chatIframeModule").css('transition', 'none')
		if(is == 1){
			if(localStorage.getItem("wi") == null || localStorage.getItem("wi") == '1'){
				if(localStorage.getItem("wis") != null){
					wi = localStorage.getItem("wis")
					finalwidth = localStorage.getItem("wis")
					localStorage.setItem("wi",wi)
				}else{
					wi = document.body.clientWidth/2;
					finalwidth = document.body.clientWidth/2
					localStorage.setItem("wi",wi);
				}
			}else{
				wi = localStorage.getItem("wi")
				finalwidth = localStorage.getItem("wi")
			}
		}
		if(leftIs == 1){
			$("#chatIframeModule").css('left', '50%')
			$("#closeChat").show()
			return false;
		}
		/* 初始化 */
		if (is == 0 && localStorage.getItem("wi") == null) {
			if (window.location.search.substring(1, 6) == 'token') {
				$("#chatIframe").attr('src', service + window.location.search)
			}
			var tokenUrl = "getToken";
			var account = $("#accounts").val();
			var levelName = $("#levelName").val();
			var isLogn = $("#isLogn").val();
			/* 如果未登录 */
			if (isLogn == "false") {
				chatUrl = service+ 'pcChat/index.html' + "?platId=" + platId;
			} else {
				/* //已登陆，获取token */
				var token;
				$.ajax({
					async : false,
					type : "get",
					url : service + tokenUrl,
					data : {
						platformId : platId,
						token : platToken,
						level : '0',
						name : account,
						grade : levelName
					},
					dataType : 'json',
					xhrFields : {
						withCredentials : true
					},
					success : function(data) {
						token = data.data;
						localStorage.setItem('sessToken', token)
					},
					error : function(data) {
						throw new Error('请求失败:' + data.msg);
					}
				})
				chatUrl = service+ 'pcChat/index.html' + "?token=" + token;
			}
			$("#chatIframe").attr("src", chatUrl);
		/* 点击聊天室图标	 */
		} else if(localStorage.getItem("wi") != '1' || is == 1) {
			wi = localStorage.getItem("wi")
			finalwidth = localStorage.getItem("wi")
			if (window.location.search.substring(1, 6) == 'token') {
				$("#chatIframe").attr('src', service + window.location.search)
			}
			var tokenUrl = "getToken";
			var account = $("#accounts").val();
			var levelName = $("#levelName").val();
			var isLogn = $("#isLogn").val();

			if (isLogn == "false") {
				chatUrl = service + 'pcChat/index.html' + "?platId=" + platId;
			} else {
				var token;
				$.ajax({
					async : false,
					type : "get",
					url : service + tokenUrl,
					data : {
						platformId : platId,
						token : platToken,
						level : '0',
						name : account,
						grade : levelName
					},
					dataType : 'json',
					xhrFields : {
						withCredentials : true
					},
					success : function(data) {
						token = data.data;
						localStorage.setItem('sessToken', token)
					},
					error : function(data) {
						throw new Error('请求失败:' + data.msg);
					}
				})
				chatUrl = service + 'pcChat/index.html' + "?token=" + token;
			}
			$("#chatIframe").attr("src", chatUrl);
			$("#chatIframeModule").css('left', wi + 'px')
			$("#closeChat").show()
		}
	}
	/* function openChatNewPage(){
		var tokenUrl = "getToken";
		var account = $("#accounts").val();
		var levelName = $("#levelName").val();
		var isLogn = $("#isLogn").val();
		if (isLogn == "false") {
			chatUrl = service+ "?platId=" + platId;
			$("#locaChatLink").attr('href',chatUrl)
		} else {
			var token;
			$.ajax({
				async : false,
				type : "get",
				url : service + tokenUrl,
				data : {
					platformId : platId,
					token : platToken,
					level : '0',
					name : account,
					grade : levelName
				},
				dataType : 'json',
				xhrFields : {
					withCredentials : true
				},
				success : function(data) {
					token = data.data;
					localStorage.setItem('sessToken', token)
				},
				error : function(data) {
					throw new Error('请求失败:' + data.msg);
				}
			})
			chatUrl = service + "?token=" + token;
			$("#locaChatLink").attr('href',chatUrl)
		}
	} */
	function openChatNewPage(){
		var isLogn = $("#isLogn").val();
		if (isLogn == 'true'){
			$.ajax({
				async : false,
				type : "get",
				url : '${base}/index/loginchat.do',
				data : {},
				dataType : 'json',
				xhrFields : {
					withCredentials : true
				},
				success : function(data) {
					if(!data.success){
					    alert(data.msg)
                    }
				},
				error : function(data) {
					if(data.status == 200){
						var url  = data.responseText
						var str = url.substring(0,5)
						if(str == 'https'){
							$("#locaChatLink").attr('href',data.responseText)
						} else {
							$("#locaChatLink").attr('href','https://'+data.responseText)
						}
					}
				}
			})
		}
	}
</script>
<style>
#chatIframeModule {
	position: fixed;
	right: 0;
	left: 100%;
	bottom: 0;
	top: 0;
	z-index: 99999;
	border-left: 5px solid #01AAED;
}

#chatIframeModule:hover {
	cursor: e-resize;
}

#chatIframeModule img {
	position: fixed;
	top: 15px;
	right: 10px;
	width: 50px;
	transition: .5s;
	display: none;
	cursor: pointer;
}

#chatIframeModule img:hover {
	transform: scale(1.3)
}

#chatIframeModule iframe {
	border: none;
}
</style>
<!-- 拖拽 -->
<!-- <script type="text/javascript">
	var finalwidth, wi;
	var dragable = false;//默认不可拖拽
	var oldX = '';//记录第一次的鼠标位置
	var startDrag = function(event) {
		dragable = true;
		var e = event ? event : window.event;
		oldX = e.pageX; //记录第一次的鼠标位置
	};
	var unDrop = function() {
		dragable = false;
		window.event ? window.event.cancelBubble = true : e.stopPropagation();
	};
	var endDrop = function() {
		if (dragable) {
			finalwidth = wi;
			dragable = false;
		}
		;
	};
	document.onmouseup = function() {
		endDrop();
	};
	document.onmousemove = function(event) {
		if (dragable) {
			var e = event ? event : window.event;
			box = document.getElementById('chatIframeModule');
			wi = e.pageX - oldX + parseInt(finalwidth);
			//鼠标的位移 + div的最后宽度 = div的新宽度
			//向上拉  wi =  oldX - e.pageX  + parseInt(finalwidth);
			//向下拉  wi =  e.pageX - oldX  + parseInt(finalwidth);
			if (dragable) {
				var width = document.body.clientWidth
						- document.body.clientWidth / 4
				if (wi < 50 || wi == 50) {
					$("#chatIframeModule").css('left', 50 + 'px'), wi = 50;
					return;
				}
				if (wi > width || wi == width) {
					$("#chatIframeModule").css('left', width + 'px'),
							wi = width;
					return;
				}
				$("#chatIframeModule").css('left', wi + 'px')
				localStorage.setItem("wi",wi);
				localStorage.setItem("wis",wi);
			}
			;
		}
		;
	};
</script> -->
