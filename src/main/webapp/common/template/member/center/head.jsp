<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="MAHeader">
      <div id="MALogo"><img <c:choose><c:when test="${not empty logo }">src="${logo }"</c:when><c:otherwise>src="${base}/common/images/member/center/default.png"</c:otherwise></c:choose> height="80" width="170"></div><!--logo-->
      <div id="MATime">
          <div id="timepic"></div>
          <div class="time_text" id="est_bg">北京时间：<span id="EST_reciprocal">2016/08/26 10:20:22</span></div>
    </div><!--MATime 时间-->
    <div class="MADeposit-pos" style="right:295px;">
          <a id="onlineCk" href="${base}/index.do" title="安全退出">
              <span class="MADeposit-bg-out"></span>
              <span class="MADeposit-bg">返回主页</span>
              <span class="MADeposit-bg-R"></span>
          </a>
      </div>
    <div class="MADeposit-pos" style="right:203px;">
          <a id="onlineCk" href="${base}/center/banktrans/deposit/page.do" title="线上存款">
              <span class="MADeposit-bg-L"></span>
              <span class="MADeposit-bg">线上存款</span>
              <span class="MADeposit-bg-R"></span>
          </a>
      </div>
      <div class="MADeposit-pos" style="right:112px;">
          <a id="onlineCk" href="" target="_blank" title="在线客服" class="onlineCkKfUrl">
              <span class="MADeposit-bg-K"></span>
              <span class="MADeposit-bg" id="kfurl">在线客服</span>
              <span class="MADeposit-bg-R"></span>
          </a>
      </div>

      <div class="MADeposit-pos">
          <a id="onlineCk" href="${base}/logout.do" title="安全退出">
              <span class="MADeposit-bg-Exit"></span>
              <span class="MADeposit-bg">安全退出</span>
              <span class="MADeposit-bg-R"></span>
          </a>
      </div>
</div>
<script>
    $(function () {
        $.ajax({
            url:"${base}/native/getServiceUrl.do",
            type:"get",
            success:function (res) {
                if(res.success){
                    $('.onlineCkKfUrl').attr("href",res.content)
                }
            }
        })
    })
</script>