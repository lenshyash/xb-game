<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="MALeft">
	<div id="welcome">会员中心</div>
	<div class="sidebar">
		<div id="sidebar-mem">会员专区</div>
		<a ${myAccountFlag } href="${base}/center/member/meminfo/page.do">我的账户</a>
		<a ${bankTransFlagDeposit } href="${base }/center/banktrans/deposit/page.do">在线存款</a>
		<a ${bankTransFlagDraw } href="${base }/center/banktrans/draw/page.do">在线提款</a>
		<c:if test="${isZrOnOff eq 'on' || isDzOnOff eq 'on'  || isChessOnOff eq 'on'  || isThdLotOnOff eq 'on' || isTsOnOff eq 'on' }">
			<a ${bankTransFlag } href="${base}/center/banktrans/mnychg/page.do">额度转换</a>
		</c:if>
		<a ${betHisFlag } href="${base}/center/record/betrecord/page.do">交易记录</a>
		<c:if test="${isExChgOnOff eq 'on'}">
			<a ${exchangeFlag } href="${base}/center/banktrans/exchange/page.do">积分兑换</a>
		</c:if>
		<c:if test="${isZJGLOnOff}">
			<a ${zjglFlag } href="${base}/center/banktrans/mnychg/zjglPage.do">资金管理</a>
		</c:if>
		<c:if test="${loginMember.accountType eq 4}">
		<a href="${base}/center/banktrans/mnychg/rebateExplain.do" target="_blank">返点赔率表</a>
		</c:if>
	</div>
	<!---->

	<div class="sidebar">
		<div id="sidebar-msg" >信息公告</div>
		<a ${messageFlag } href="${base}/center/news/message/page.do" style="position: relative;">站内信<span id="xiaoxi"></span></a>

	</div>
	<%-- 站点ID --%>
	<input type="hidden" value="${domainFolder}" id="domainFolder" />
</div>
<style media="screen">
	.xiaoxi {
		width: 32px;
    height: 18px;
    background: red;
    float: left;
    border-radius: 50%;
    color: #fff;
    display: inline-table;
    text-align: center;
    transform: scale(.5);
    font-size: 20px;
    position: absolute;
    top: -6px;
    left: 74px;
	}
</style>
<script type="text/javascript">
	function message() {
		$.ajax({
			url:'${base}/center/news/message/list.do',
			data : {pageNumber : 1,pageSize : 10},
			dataType:"json",
			type:"POST",
			success:function(center){
				var message = center.totalCount;
				var yidu = 0;
				for (let i in center.list) {
					if (center.list[i].status == 2) {
						yidu += 1
					}
				}
				if ($("#domainFolder").val() == "b05201") {
					$("#xiaoxi").html('<span class="xiaoxi">'+(message - yidu)+'</span>')
				}
			}
		})
	}
	message(true);

</script>
<!--MALeft左边-->
