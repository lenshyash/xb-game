<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<script src="${base}/common/js/layer/layer.src.js"></script>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div class="mnav">
					<a href="#" class="current" title="站内信">站内信</a>
				</div>
				<!--菜单mnav-->
				<div class="grxx-grxx" id="record_tb" style="margin-top: 17px;"></div>
				<jsp:include page="/common/include/page.jsp"></jsp:include>
			</div>
			<!--MAContent下-->

			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
		<!--MACenter-->
	</div>
</body>
</html>
<script id="record_tpl" type="text/html">
	<table border="0" cellpadding="5" cellspacing="0" width="98%">
		<tbody>
			<tr>
				<th align="center" width="40"><input type="checkbox" class="all_checked"></th>
				<th align="center" width="40">状态</th>
				<th align="center">短信标题</th>
				<th align="center" width="160">发布时间</th>
				<th align="center" width="100">操作</th>
			</tr>
			{{each list as row i}}
			<tr data-id="{{i}}" style="background-color: rgb(245, 245, 245);" onmouseover="this.style.backgroundColor='#FFFFCC'" onmouseout="this.style.backgroundColor='#F5F5F5'" align="center" bgcolor="#F5F5F5">
				<td align="center"><input type="checkbox" value="{{row.userMessageId}}" /> </td>
				<td align="center"><font color="#FF0000">{{$formatter row.status 'status'}}</font></td>
				<td align="left"><a href="javascript:void(0);" onclick="readMsg({{i}});">{{row.title}}</a></td>
				<td align="center">{{$formatter row.createDatetime 'time'}}</td>
				<td align="center"><a href="javascript:void(0);" onclick="delMsg({{row.id}});" style="color: #00F">点击删除</a></td>
			</tr>
			{{/each}}
		</tbody>
	</table>
	{{if list}}
		<div>
			<input type="button" value="标记已读" class="batch_reader">
			<input type="button" value="批量删除" class="batch_delete">
		</div>
	{{/if}}
</script>
<script type="text/javascript">
	var pageSize = 10;
	var pageNumber = 1;
	var first = true;
	var pageDatas = [];
	$(function() {
		searchMsg();
		first = false;
		
		$("#record_tb").on("click",".all_checked",function(){
			var $this = $(this),isActive = $this.prop('checked');
			$this.parent().parent().siblings('tr').find('td input:checkbox').prop('checked',isActive);
		}).on("click",".batch_delete",function(){
			/* var $this = $(this),$tab = $("#record_tb table tr td input:checkbox:checked");
			if($tab.length == 0){
				alert('请先选择要删除的选项');
				return;
			}
			var ids = '';
			$tab.each(function(i,j){
				j = $(j);
				ids += j.val() + ',';
			})
			ids = ids.substring(0,ids.length-1);
			if (confirm("确认删除选中的选项吗？")) {
				$.ajax({
					url : "${base}/center/news/message/batchDelete.do",
					data : {
						id : ids
					},
					success : function(result) {
						searchMsg();
					}
				});
			} */
			var $this = $(this),$mesList = $('#record_tb table tr td input:checkbox:checked'),ids='';
			if($mesList.length == 0){
				alert("请选择选项!");
				return;
			}
			var rowId = 0;
			var flag =true;
			$mesList.each(function(i,j){
					j = $(j);
					var row = pageDatas[j.parent().parent().data('id')];
					ids += row.id + ",";
					if(j.parent().siblings().find('font').text() == '未读'){
						flag = false;	
					}
			})
			
			ids = ids.substring(0,ids.length-1);
			if(!flag && !confirm("有未读信息确定要删除吗？")){
				return;
			}
			$.ajax({
  					url:'${base}/center/news/message/batchNewDelete.do',
  					data:{id:ids},
  					success:function(res){
  						if(res.success){
  							alert('删除成功');
  							searchMsg();
  						}else{
  							alert(res.msg);
  						}
  					}
  			})
		}).on("click",".batch_reader",function(){
			var $this = $(this),$mesList = $('#record_tb table tr td input:checkbox:checked'),ids='';
			if($mesList.length == 0){
				alert("请选择选项!");
				return;
			}
			var rowId = 0;
			$mesList.each(function(i,j){
				j = $(j);
				if(j.parent().siblings().find('font').text() == '未读'){
					var row = pageDatas[j.parent().parent().data('id')];
					ids += row.id + ",";
				}
			})
			ids = ids.substring(0,ids.length-1);
			if (confirm("确认标记为已读吗？")) {
				$.ajax({
   					url:'${base}/center/news/message/batchRead.do',
   					data:{id:ids},
   					success:function(res){
   						if(res.success){
   							alert('标记成功');
   							searchMsg();
   						}else{
   							alert(res.msg);
   						}
   					}
   				})
			}
		})
	});

	function searchMsg() {

		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}

		$.ajax({
			url : "${base}/center/news/message/list.do",
			data : {
				pageNumber : pageNumber,
				pageSize : pageSize
			},
			success : function(result) {
				pageDatas = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					searchMsg();
				});
			}
		});
	}

	template.helper('$formatter', function(content, key) {
		if (key == 'time') {
			return DateUtil.formatDatetime(content);
		} else if (key == 'status') {
			if (content == '2') {
				return '已读';
			} else {
				return '未读';
			}
		}
	});

	function readMsg(index) {
		var row = pageDatas[index];
		$
				.ajax({
					url : "${base}/center/news/message/read.do",
					data : {
						id : row.id
					},
					success : function(result) {
						layer
								.open({
									type : 1, //page层
									area : [ '550px', '550px' ],
									title : row.title,
									shade : 0.6, //遮罩透明度
									scrollbar : false,
									offset : '120px',//距离顶部
									moveType : 0, //拖拽风格，0是默认，1是传统拖动
									shift : 1, //0-6的动画形式，-1不开启
									content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'
											+ row.message + '</div>'
								});
						searchMsg();
					}
				});

	}

	function delMsg(id) {
		if (confirm("确认删除此信吗？")) {
			$.ajax({
				url : "${base}/center/news/message/batchNewDelete.do",
				data : {
					id : id
				},
				success : function(result) {
					searchMsg();
				}
			});
		}
	}
</script>