<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">

				<!-- 内容区菜单 -->
				<jsp:include page="../transhead.jsp"></jsp:include>
				<!--菜单mnav-->
				<div id="_MMainData" class="member-data">
				<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能线上兑换</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">
					<div class="memdata-content">
						<div class="info-table-area">
							<table border="1" style="table-layout: fixed" class="M-info">
								<tbody>
									<tr>
										<th nowrap="">类型</th>
										<th nowrap="">余额</th>
										<th nowrap="">操作</th>
									</tr>
									<tr>
										<td class="MNumber">现金</td>
										<td class="MNumber"><span id="mnyCredit">0.00</span>&nbsp;&nbsp;RMB</td>
										<td class="MNumber" rowspan="2"><input id="loadCredit" value="刷新" onclick='initUserData()' style="margin-left: 15px;" type="button"></td>
									</tr>
									<tr id="mmgid">
										<td class="MNumber">积分</td>
										<td class="MNumber"><span id="scoreCredit">0.00</span>&nbsp;&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3" style="text-align: left; padding-left: 10px;">
											<!--info-table-area余额-->
											<ul class="">
												<li>兑换类型： <select id="exchangeType">
														<option selected="selected" value="">----请选择类型----</option>
														<option value="1">现金兑换积分</option>
														<option value="2">积分兑换现金</option>
												</select> <span id="exchange_remark_span"></span>
												</li>
												<li>兑换额度： <input id="amount" name="amount" class="inp" type="text"> <input value="确定" onclick="exchange();" id="excBtn" class="btn2" type="button"> <span
													id="exchange_result_span"></span>
												</li>
												<li><span style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></li>
												<li><span style="font-size: 14px; font-weight: bold; color: #c00">积分可用于参与站点活动</span></li>
											</ul>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div></c:if>
					<!--_MMainData 基本信息-->
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript" src="${base }/common/template/member/center/js/memexchangedemo.js"></script></c:if>