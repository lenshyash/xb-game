<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
	     <!-- 内容区菜单 -->
	  <div class="mnav">
			<c:if test="${isZrOnOff eq 'on' }">
        		<a ${mnyChgFlag } title="额度转换" class="current">额度转换</a>
        	</c:if>
            <div class="navSeparate"></div>
            <a ${comFlag } href="${base}/center/banktrans/deposit/page.do" title="线上存款">线上存款</a>
         	<div class="navSeparate"></div>
            <a ${drawFlag } href="${base}/center/banktrans/draw/page.do" title="线上取款">线上取款</a>
            <div class="navSeparate"></div>
            <c:if test="${isExChgOnOff eq 'on'}">
            	<a href="${base}/center/banktrans/deposit/page.do" title="线上兑换">线上兑换</a>
            </c:if>
        </div><!--菜单mnav-->
        
        <div class="yhjy-edzh">
        	<h2>目前额度</h2>
			<table border="1" style="margin-bottom: 8px;" class="yhjy-MMain">
			<form id="form1" name="form1" action="?save=ok" method="post"></form>
                <thead>
                    <tr>
                        <th nowrap="" style="width: 30%;">帐户</th>
                        <th nowrap="" style="width: 70%;">余额</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>系统余额 </td>
                        <td><span id="mainCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
                    </tr>
                    <c:if test="${isBbinOnOff eq 'on'}">
                    <tr>
                        <td width="35%" class="">BBIN余额</td>
                        <td class="65%"><a style="color:#ff0000;" href="javascript:void(0);" id="loadbbinCredit" onclick="getBalance('bbin','BBINCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="BBINCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
                    </tr>
                    </c:if>
                    
                    
                    <c:if test="${isAgOnOff eq 'on'}">
                    <tr>
                        <td class="">AG余额</td>
                        <td class=""><a style="color:#ff0000;" href="javascript:void(0);" id="loadAgCredit" onclick="getBalance('ag','AGCredit');">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="AGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
                    </tr>
                    </c:if>
                    
                   <c:if test="${isMgOnOff eq 'on'}">
                    <tr>
                        <td class="">MG余额</td>
                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('mg','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
                    </tr>
                    </c:if>
                    
                   <c:if test="${isPtOnOff eq 'on'}">
	                    <tr>
	                        <td class="">PT余额</td>
	                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('pt','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isQtOnOff eq 'on'}">
	                    <tr>
	                        <td class="">QT余额</td>
	                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('qt','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isAbOnOff eq 'on'}">
	                    <tr>
	                        <td class="">Allbet余额</td>
	                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ab','MGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
	                    </tr>
                    </c:if>
                    <c:if test="${isOgOnOff eq 'on'}">
	                    <tr>
	                        <td class="">OG余额</td>
	                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('og','OGCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="OGCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
	                    </tr>
                    </c:if>
                     <c:if test="${isDsOnOff eq 'on'}">
	                    <tr>
	                        <td class="">DS余额</td>
	                        <td class=""><a style="color:#ff0000;" id="" href="javascript:void(0);" onclick="getBalance('ds','DSCredit')">刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="DSCredit">0.00</span>&nbsp;&nbsp;RMB <img align="absmiddle" alt="人民币" title="人民币" src="${base}/common/template/member/center/images/money_RMB.gif"> <span style=" color:red  "></span></td>
	                    </tr>
                    </c:if>
                </tbody>
        	</table>
            <h2 class="MSubTitle">额度转换</h2>
            <table style="width: auto;" class="edzh-MNoBorder">
                <tbody>
                	<tr>
                        <td nowrap="" class="">转出：</td>
                        <td class="">
                           <select name="source-transfer" class="MFormStyle" id="source-transfer">
								<option selected="selected" value="">----请选择钱包----</option>
								<option value="sys">系统额度</option>
								<c:if test="${isMgOnOff eq 'on'}">
									<option value="mg">MG额度</option>
								</c:if>
								<c:if test="${isBbinOnOff eq 'on'}">
									<option value="bbin">BBIN额度</option>
								</c:if>
								<c:if test="${isAgOnOff eq 'on'}">
									<option value="ag">AG额度</option>
								</c:if>
								<c:if test="${isPtOnOff eq 'on'}">
									<option value="pt">PT额度</option>
								</c:if>
								<c:if test="${isQtOnOff eq 'on'}">
									<option value="qt">QT额度</option>
								</c:if>
								<c:if test="${isAbOnOff eq 'on'}">
									<option value="ab">Allbet额度</option>
								</c:if>
								<c:if test="${isOgOnOff eq 'on'}">
									<option value="og">OG额度</option>
								</c:if>
								<c:if test="${isOgOnOff eq 'on'}">
									<option value="og">OG额度</option>
								</c:if>
								<c:if test="${isDsOnOff eq 'on'}">
									<option value="ds">Ds额度</option>
								</c:if>
								<c:if test="${isCq9OnOff eq 'on'}">
									<option value="cq9">CQ9额度</option>
								</c:if>
							</select>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="" class="">转入：</td>
                        <td class="">
                           <select name="desc-transfer" id="desc-transfer">
								<option selected="selected" value="">----请选择钱包----</option>
								<option value="sys">系统额度</option>
								<c:if test="${isMgOnOff eq 'on'}">
									<option value="mg">MG额度</option>
								</c:if>
								<c:if test="${isBbinOnOff eq 'on'}">
									<option value="bbin">BBIN额度</option>
								</c:if>
								<c:if test="${isAgOnOff eq 'on'}">
									<option value="ag">AG额度</option>
								</c:if>
								<c:if test="${isPtOnOff eq 'on'}">
									<option value="pt">PT额度</option>
								</c:if>
								<c:if test="${isQtOnOff eq 'on'}">
									<option value="qt">QT额度</option>
								</c:if>
								<c:if test="${isAbOnOff eq 'on'}">
									<option value="ab">Allbet额度</option>
								</c:if>
								<c:if test="${isOgOnOff eq 'on'}">
									<option value="og">OG额度</option>
								</c:if>
								<c:if test="${isDsOnOff eq 'on'}">
									<option value="ds">DS额度</option>
								</c:if>
						   </select>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="" class="">金额：</td>
                        <td class="">
                            <input id="amount" name="amount" class="inp" type="text"> <input value="确定" onclick="trans();" id="add" class="btn2" type="button">
                        </td>
                    </tr>
                    <tr>
                    	<td nowrap="" class=""></td>
                    	<td>
                    		<span id="MSwitchResult" style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span>
                    	</td>
                    </tr>
                </tbody>
                <input type="hidden" name="v" id="v" value="${v}">
        	</table>
        </div><!--yhjy-edzh额度转换	-->
        </div>
        <div id="MAContentBottom"></div>
  		<div id="MAFoot">${copyright }</div>
     <script type="text/javascript">
function getBalance(type,id){
	var param = {};
	param["type"]=type;
	$("#"+id).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />");
	/* $.post('${base}/rc4m/getBalance.do',param,function(json){
		if(json.balance==-1 || json.balance=='-1'){
			$("#"+id).text("获取三方余额失败或者未三方账户还未开通");
		}else{
			$("#"+id).text(json.balance);
		}
	},'json'); */
	
	$ajax({
        url:'${base}/rc4m/getBalance.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
         	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(json.balance==-1 || json.balance=='-1'){
					$("#"+id).text("第三方账户还未开通");
				}else{
					$("#"+id).text(json.balance);
				}
			} else {// 后台异常
				$("#"+id).text("获取失败")
			}
        	
        }
    });
}

$(function(){
		getBalance("sys","mainCredit");
	<c:if test="${isMgOnOff eq 'on'}">
		getBalance("mg","MGCredit");
	</c:if>
	<c:if test="${isBbinOnOff eq 'on'}">
		 getBalance("bbin","BBINCredit");
	</c:if>
	<c:if test="${isAgOnOff eq 'on'}">
		 getBalance("ag","AGCredit");
	</c:if>
	<c:if test="${isPtOnOff eq 'on'}">
		 getBalance("pt","PTCredit");
	</c:if>
	<c:if test="${isQtOnOff eq 'on'}">
		getBalance("qt","QTCredit");
	</c:if>
	<c:if test="${isAbOnOff eq 'on'}">
		getBalance("ab","ABCredit");
	</c:if>
	<c:if test="${isOgOnOff eq 'on'}">
		getBalance("og","OGCredit");
	</c:if>
	<c:if test="${isDsOnOff eq 'on'}">
		getBalance("ds","DSCredit");
	</c:if>
});

$(function(){
	$("#source-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			var html = '<option  value="">----请选择钱包----</option><option value="sys">系统额度</option>';
			<c:if test="${isMgOnOff eq 'on'}">
			html+='<option value="mg" selected="selected">MG额度</option>';
			</c:if>
			<c:if test="${isBbinOnOff eq 'on'}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAgOnOff eq 'on'}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			<c:if test="${isPtOnOff eq 'on'}">
				html+='<option value="pt">PT额度</option>';
			</c:if>
			<c:if test="${isQtOnOff eq 'on'}">
				html+='<option value="qt">QT额度</option>';
			</c:if>
			<c:if test="${isAbOnOff eq 'on'}">
				html+='<option value="ab">Allbet额度</option>';
			</c:if>
			<c:if test="${isOgOnOff eq 'on'}">
				html+='<option value="og">OG额度</option>';
			</c:if>
			<c:if test="${isDsOnOff eq 'on'}">
				html+='<option value="ds">DS额度</option>';
			</c:if>
			<c:if test="${isCq9OnOff eq 'on'}">
				html+='<option value="cq9">CQ9额度</option>';
			</c:if>
			$("#desc-transfer").html(html);
		}else{
			 var html = '<option  value="">----请选择钱包----</option><option value="sys" selected="selected">系统额度</option>';
			<c:if test="${isMgOnOff eq 'on'}">
			html+='<option value="mg" >MG额度</option>';
			</c:if>
			<c:if test="${isBbinOnOff eq 'on'}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAgOnOff eq 'on'}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			<c:if test="${isPtOnOff eq 'on'}">
				html+='<option value="pt">PT额度</option>';
			</c:if>
			<c:if test="${isQtOnOff eq 'on'}">
				html+='<option value="qt">QT额度</option>';
			</c:if>
			<c:if test="${isAbOnOff eq 'on'}">
				html+='<option value="ab">Allbet额度</option>';
			</c:if>
			<c:if test="${isOgOnOff eq 'on'}">
				html+='<option value="og">OG额度</option>';
			</c:if>
			<c:if test="${isDsOnOff eq 'on'}">
				html+='<option value="ds">DS额度</option>';
			</c:if>
			<c:if test="${isCq9OnOff eq 'on'}">
				html+='<option value="cq9">CQ9额度</option>';
			</c:if>
			$("#desc-transfer").html(html);
		}
	});
	
 	$("#desc-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			var html = '<option value="">----请选择钱包----</option><option value="sys" >系统额度</option>';
			<c:if test="${isMgOnOff eq 'on'}">
			html+='<option value="mg" selected="selected">MG额度</option>';
			</c:if>
			<c:if test="${isBbinOnOff eq 'on'}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAgOnOff eq 'on'}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			<c:if test="${isPtOnOff eq 'on'}">
				html+='<option value="pt">PT额度</option>';
			</c:if>
			<c:if test="${isQtOnOff eq 'on'}">
				html+='<option value="qt">QT额度</option>';
			</c:if>
			<c:if test="${isAbOnOff eq 'on'}">
				html+='<option value="ab">Allbet额度</option>';
			</c:if>
			<c:if test="${isOgOnOff eq 'on'}">
				html+='<option value="og">OG额度</option>';
			</c:if>
			<c:if test="${isDsOnOff eq 'on'}">
				html+='<option value="ds">DS额度</option>';
			</c:if>
			<c:if test="${isCq9OnOff eq 'on'}">
				html+='<option value="cq9">CQ9额度</option>';
			</c:if>
			$("#source-transfer").html(html);
		}else{
			 var html = '<option value="">----请选择钱包----</option><option value="sys"  selected="selected">系统额度</option>';
			<c:if test="${isMgOnOff eq 'on'}">
			html+='<option value="mg">MG额度</option>';
			</c:if>
			<c:if test="${isBbinOnOff eq 'on'}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAgOnOff eq 'on'}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			<c:if test="${isPtOnOff eq 'on'}">
				html+='<option value="pt">PT额度</option>';
			</c:if>
			<c:if test="${isQtOnOff eq 'on'}">
				html+='<option value="qt">QT额度</option>';
			</c:if>
			<c:if test="${isAbOnOff eq 'on'}">
				html+='<option value="ab">Allbet额度</option>';
			</c:if>
			<c:if test="${isOgOnOff eq 'on'}">
				html+='<option value="og">OG额度</option>';
			</c:if>
			<c:if test="${isDsOnOff eq 'on'}">
				html+='<option value="ds">DS额度</option>';
			</c:if>
			<c:if test="${isCq9OnOff eq 'on'}">
				html+='<option value="cq9">CQ9额度</option>';
			</c:if>
			$("#source-transfer").html(html);
		}
	}); 
});

function trans(){
	$("#add").attr("disabled","disabled");
	var from = $("#source-transfer").val();
	var to = $("#desc-transfer").val();
	var v = $("#v").val();
	console.debug("from"+from);
	console.debug("to"+to);
	if(from==null || from==''){
		alert("转出类型不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	if(to==null || to==''){
		alert("转出类型不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	var quota = $("#amount").val();
	if(!quota){
		alert("余额不能为空");
		$("#add").removeAttr("disabled");
		return;
	}
	if(from=='sys'&& !/ag|bbin|mg/.test(to)){
		alert("转入类型错误");
		$("#add").removeAttr("disabled");
		return;
	}
	if(to=='sys'&& !/ag|bbin|mg/.test(from)){
		alert("转出类型错误");
		$("#add").removeAttr("disabled");
		return;
	}
	var platform = "";
	if(to=='sys'){
		platform = from;
	}else if(from=='sys'){
		platform = to;
	}
	var param = {};
	param["changeFrom"]=from;
	param["changeTo"]=to;
	param["platform"]=platform;
	param["quota"]=quota;
	param["v"]=v;
	/* $.post('${base}/rc4m/thirdRealTransMoney.do',param,function(json){
		if(json.code==0||json.code=='0'){
			alert("转账成功");
			if(to=='mg'||from=='mg'){
				getBalance("mg","MGCredit");
			}
			if(to=='ag'||from=='ag'){
				 getBalance("ag","AGCredit");
			}
			if(to=='bbin'||from=='bbin'){
				 getBalance("bbin","BBINCredit");
			}
			getBalance("sys","mainCredit");
			$("#v").val(json.v);
		}else{
			alert(json.msg);
			$("#v").val(json.v);
		}
		$("#add").removeAttr("disabled");
	}); */
	$ajax({
        url:'${base}/rc4m/thirdRealTransMoney.do',
        type:'POST',
        data:param,
        success:function(json,status,xhr){
        	var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
	        	if(json.success){
		        	alert("转账成功");
					if(to=='mg'||from=='mg'){
						getBalance("mg","MGCredit");
					}
					if(to=='ag'||from=='ag'){
						 getBalance("ag","AGCredit");
					}
					if(to=='bbin'||from=='bbin'){
						 getBalance("bbin","BBINCredit");
					}
					if(to=='qt'||from=='qt'){
						 getBalance("qt","QTCredit");
					}
					if(to=='pt'||from=='pt'){
						 getBalance("pt","PTCredit");
					}
					if(to=='ab'||from=='ab'){
						 getBalance("ab","ABCredit");
					}
					if(to=='og'||from=='og'){
						 getBalance("og","OGCredit");
					}
					if(to=='ds'||from=='ds'){
						 getBalance("ds","DSCredit");
					}
					getBalance("sys","mainCredit");
	        	}else{
	        		alert(json.msg);
	        	}
			}else {// 登录异常
				alert(json.msg);
			}
        	
        	$("#add").removeAttr("disabled");
        }
    });
}
</script>
</body>
</html>