<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能在线存款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">
						<div class="member-data secrity-setting">
							<div class="imformation">
								<div class="info-img img-scy"></div>
								<div class="info-content">
									<p class="info-title">
										<span class="info-title-acname">${loginMember.account}</span>&nbsp;欢迎进入线上存款
									</p>
									<p class="info-txt">帐号：${loginMember.account}</p>
									<c:if test="${accountLevelSwitch == 'on' }"><p class="info-txt">等级：${loginMember.levelName}</p></c:if>
<!-- 									<p class="info-txt">最后登录时间：2016-08-29 15:27:48</p> -->
									<div class="info-sec-title">
										<div title="诚实守信，服务至上" class="info-sec-txt">诚实守信，服务至上</div>
									</div>
									<!--info-sec-title即时帐号资讯<-->
								</div>
								<!--info-content欢迎进入基本信息-->
							</div>
							<!--imformation基本信息 top-->
							<input type="hidden" value="${isPaymentSort }" id="isPaymentSort">
							<div class="cunkuan-scy">
<!-- 								<div id="online_div" class="hidn"> -->
<%-- 									<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpga.do')"><div class="scy-list scy-zxzf" id="zxzf"> --%>
<!-- 											<span class="scy-link-txt">在线支付 -->
<!-- 												<p id="onlineDesc_div" style="color: red;">即时到账，推荐入款时使用</p> -->
<!-- 											</span> -->
<!-- 										</div></a> -->
<!-- 									scy-psd登人密码 -->
<!-- 								</div> -->
<!-- 								<div id="fast_div" class="hidn"> -->
<%-- 									<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpgb.do');return false"><div class="scy-list scy-wxzf"> --%>
<!-- 											<span class="scy-link-txt">快速入款 -->
<!-- 												<p id="fastDesc_div" style="color: red;">支持微信/支付宝二维码扫描</p> -->
<!-- 											</span> -->
<!-- 										</div></a> -->
<!-- 									scy-info银行资料 -->
<!-- 								</div> -->
<!-- 								<div id="bank_div" class="hidn"> -->
<%-- 									<a href="javascript:void(0);" onclick="Go('${base}/center/banktrans/deposit/dptpgc.do');return false"><div class="scy-list scy-yhhk"> --%>
<!-- 											<span class="scy-link-txt">公司入款 -->
<!-- 												<p id="bankDesc_div" style="color: red;">支持网银转账，ATM转账，银行柜台汇款</p> -->
<!-- 											</span> -->
<!-- 										</div></a> -->
<!-- 								</div> -->
							</div>
							<input type="hidden" value="${domainFolder}" id="domainFolder" />
							<script>
								var lists = {
										"1":{
											"id":"online_div",
											"name":"在线支付",
											"url":"Go('${base}/center/banktrans/deposit/dptpga.do')",
											"explainId":"onlineDesc_div",
											"explain":"即时到账，推荐入款时使用"
										},
										"2":{
											"id":"fast_div",
											"name":"快速入款",
											"url":"Go('${base}/center/banktrans/deposit/dptpgb.do');return false",
											"explainId":"fastDesc_div",
											"explain":"支持微信/支付宝二维码扫描"
										},
										"3":{
											"id":"bank_div",
											"name":"公司入款",
											"url":"Go('${base}/center/banktrans/deposit/dptpgc.do');return false",
											"explainId":"bankDesc_div",
											"explain":"支持网银转账，ATM转账，银行柜台汇款"
										},
										"4":{
											"id":"virtual_div",
											"name":"虚拟货币入款",
											"url":"Go('${base}/center/banktrans/deposit/dptpgd.do');return false",
											"explainId":"virtualDesc_div",
											"explain":"支持虚拟货币转账，ATM转账，银行柜台汇款"
										},

								}
								if ($("#domainFolder").val() == "b21601") {
									lists[3].name = "银行卡充值";
								}
								var sort = $('#isPaymentSort').val();
								if(sort){}else{sort = '1,2,3,4'};
								var sortl = sort.split(',');
								var html = '';
								for(var i=0;i<sortl.length;i++){
									var event = lists[sortl[i]];
									html += '<div id="'+event.id+'" class="hidn">';
									if(event.id == 'online_div'){
										html += '<a href="javascript:void(0);" onclick="'+event.url+'"><div class="scy-list scy-zxzf" id="zxzf">';
									}else if(event.id == 'fast_div'){
										html += '<a href="javascript:void(0);" onclick="'+event.url+'"><div class="scy-list scy-wxzf">';
									}else if (event.id == 'bank_div'){
										html += '<a href="javascript:void(0);" onclick="'+event.url+'"><div class="scy-list scy-yhhk">';
									}else {
										html += '<a href="javascript:void(0);" onclick="'+event.url+'"><div class="scy-list scy-virtual">';
									}
									html += '		<span class="scy-link-txt">'+event.name;
									html += '			<p id="'+event.explainId+'" style="color: red;overflow: auto;height: 85px;">'+event.explain+'</p>';
									html += '		</span>';
									html += '	</div></a>';
									html += '</div>';
								}
								$('.cunkuan-scy').html(html);
							</script>
							<!--aqsz-scy安全设置四个图片-->
						</div>
						<!--_MMainData 安全设置--></c:if>
					</div>
				</div>

				<style>
					.scy-link-txt p::-webkit-scrollbar {/*滚动条整体样式*/
						width: 10px;     /*高宽分别对应横竖滚动条的尺寸*/
						height: 10px;
					}
					.scy-link-txt p::-webkit-scrollbar-thumb {/*滚动条里面小方块*/
						border-radius: 5px;
						-webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
						background: #9b8e8e;
					}
					.scy-link-txt p::-webkit-scrollbar-track {/*滚动条里面轨道*/
						-webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
						border-radius: 5px;
						background: #ffffff;
					}
				</style>
			</div>
			<!--MAContent下-->

			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
		<!--MACenter-->
	</div>
</body>
</html>
<c:if test="${userInfo.accountType!=6 }"><script>
	$(function() {
		initDptData();
	});
	var pays = [];
	function initDptData() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptdata.do",
			success : function(result) {
				if (result.onlineFlag == 'on') {
					$("#online_div").removeClass("hidn");
					$("#onlineDesc_div").html(result.onlineDesc);
				}
				if (result.fastFlag == 'on') {
					$("#fast_div").removeClass("hidn");
					$("#fastDesc_div").html(result.fastDesc);
				}
				if (result.bankFlag == 'on') {
					$("#bank_div").removeClass("hidn");
					$("#bankDesc_div").html(result.bankDesc);
				}
				if (result.virtualFlag == 'on') {
					$("#virtual_div").removeClass("hidn");
					$("#virtualDesc_div").html(result.virtualDesc );
				}
			}
		});
	}
</script></c:if>
