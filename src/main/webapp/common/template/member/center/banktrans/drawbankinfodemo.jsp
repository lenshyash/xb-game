<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<style>
#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

/*20141208-改版 cindy*/
.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: #FFF;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}

.input-200 {
	height: 20px;
	line-height: 20px;
	width: 200px;
}

.red {
	color: red;
}
-->
</style>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<span class="step-deposit">步骤1:设置取款密码</span> <span class="step-deposit current-deposit">步骤2:完善出款银行信息</span> <span class="step-deposit">步骤3:取款</span>
						<div class="br_line"></div>
						<div class="memdata-content">
							<div class="info-table-area">
								<table id="savebank" border="1" style="table-layout: fixed" class="M-info">
								</table>
							</div>
						</div>
						<!--_MMainData 基本信息-->
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	var checkflag = false;
	function as() {
		if (checkSubmit()) {
			if (!checkflag) {
				checkflag = true;
				submit()
			} else {
				alert("请勿重复提交");
			}
		}
	}

	$(function() {
		initDrawdata();
	})

	function initDrawdata() {
		$.ajax({
			url : base + "/center/banktrans/draw/drawbkinfo.do",
			success : function(result) {
				var html = template('draw_tpl', result);
				$("#savebank").html(html);
				$("#bankName").change(function() {
					var isQita = this.value;
					if(isQita == 'qita'){
						$("#bankName2").show();
					}else{
						$("#bankName2").val('');
						$("#bankName2").hide();
					}
				});
			}
		});
	}

	function j() {
		self.opener = null;
		self.close();
	}

	function submit() {
		var cashBankname = $("#bankName").val();
		var sheng = $("#province").val();
		var city = $("#city").val();
		var bankcardaddress = $("#bankcardaddress").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var userName = $("#accountname").val();
		
		
		var qita = $('#bankName2').val();
		
		if(qita){
			cashBankname = qita;
		}else{
			if (!cashBankname) {
				alert("开户银行不能为空");
				return false;
			}else if(cashBankname == 'qita'){
				if(!qita){
					alert("开户银行不能为空");
					return false;
				}
			}
		}
		
		var param = {};
		param["bankName"] = cashBankname;
		param["userName"] = userName;
		param["province"] = sheng;
		param["city"] = city;
		param["bankAddress"] = bankcardaddress;
		param["cardNo"] = cashBankaccount;
		param["repPwd"] = cashPassword;
		$
				.ajax({
					url : "${base}/center/banktrans/draw/cmitbkinfo.do",
					data : param,
					success : function(result) {
						
						window.location.href = "${base}/center/banktrans/draw/drawpg.do";
					},
					complete:function(){
						checkflag = false;
					}
				});
	}

	function checkSubmit() {
		var cashBankname = $("#bankName").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var cashProvince = $("#province").val();
		var cashCity = $("#city").val();

		var exp = /^([1-9][\d]{1,18}|x|X)$/;
		
		var qita = $('#bankName2').val();
		
		if(qita){
			cashBankname = qita;
		}else{
			if (!cashBankname) {
				alert("开户银行不能为空");
				return false;
			}else if(cashBankname == 'qita'){
				if(!qita){
					alert("开户银行不能为空");
					return false;
				}
			}
		}
		if (!cashProvince){
			alert("省份不能为空！");
			return false;
		}
		if (!cashCity){
			alert("城市不能为空！");
			return  false;
		}
		if (!cashBankaccount) {
			alert("银行账号不能为空");
			return false;
		}
		if (!exp.test(cashBankaccount)) {
			alert("请输入正确的银行账号");
			return false;
		}
		if (!cashPassword) {
			alert("取款密码不能为空");
			return false;
		}
		return true;
	}
</script>
<script id="draw_tpl" type="text/html">
	<tbody>
		<tr>
			<td class="MNumber" width="20%"><span class="red">*</span> 真实姓名：</td>
			<td width="30%">
				<input class="input-200" id="accountname" {{if userName}}disabled="disabled"{{/if}} value="{{userName}}" type="text">
			</td>
			<td> &nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2" style="text-align: left;">
				<font color="red">必须与您的银行帐户名称相同，否则无法出款</font>
			</td>
		</tr>
		<tr>
			<td class="MNumber"><span class="red">*</span> 出款银行：</td>
			<td>
			<select class="input-200" name="bankName" id="bankName">
					<option value="建设银行">建设银行</option>
					<option value="工商银行" selected="selected">工商银行</option>
					<option value="农业银行">农业银行</option>
					<option value="中国邮政银行">中国邮政银行</option>
					<option value="中国银行">中国银行</option>
					<option value="中国招商银行">中国招商银行</option>
					<option value="中国交通银行">中国交通银行</option>
					<option value="中国民生银行">中国民生银行</option>
					<option value="中信银行">中信银行</option>
					<option value="中国兴业银行">中国兴业银行</option>
					<option value="浦发银行">浦发银行</option>
					<option value="平安银行">平安银行</option>
					<option value="华夏银行">华夏银行</option>
					<option value="广州银行">广州银行</option>
					<option value="BEA东亚银行">BEA东亚银行</option>
					<option value="广州农商银行">广州农商银行</option>
					<option value="顺德农商银行">顺德农商银行</option>
					<option value="北京银行">北京银行</option>
					<option value="杭州银行">杭州银行</option>
					<option value="温州银行">温州银行</option>
					<option value="上海农商银行">上海农商银行</option>
					<option value="中国光大银行">中国光大银行</option>
					<option value="渤海银行">渤海银行</option>
					<option value="浙商银行">浙商银行</option>
					<option value="晋商银行">晋商银行</option>
					<option value="汉口银行">汉口银行</option>
					<option value="上海银行">上海银行</option>
					<option value="广发银行">广发银行</option>
					<option value="深圳发展银行">深圳发展银行</option>
					<option value="东莞银行">东莞银行</option>
					<option value="宁波银行">宁波银行</option>
					<option value="南京银行">南京银行</option>
					<option value="北京农商银行">北京农商银行</option>
					<option value="重庆银行">重庆银行</option>
					<option value="广西农村信用社">广西农村信用社</option>
					<option value="吉林银行">吉林银行</option>
					<option value="江苏银行">江苏银行</option>
					<option value="成都银行">成都银行</option>
					<option value="尧都区农村信用联社">尧都区农村信用联社</option>
					<option value="浙江稠州商业银行">浙江稠州商业银行</option>
					<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
					<option value="qita">其他</option>
			</select>
			</td>
			<td align="left">
				<input class="input-200" name="bankName" placeholder="请输入银行名称" id="bankName2" style="display:none;"/>
			</td>
		</tr>
		<tr>
			<td class="MNumber"><span class="red">*</span>省份：</td>
			<td><input class="input-200" id="province" name="province" type="text"></td>
				<td rowspan="5" width="50%">
				<div style="line-height: 24px;padding: 10px;">
					<b>备注:</b><br> 标记有 <span class="red">*</span> 者为必填项目。
				</div>
			</td>
		</tr>
		<tr>
			<td class="MNumber"><span class="red">*</span>城市：</td>
			<td><input class="input-200" id="city" name="city" type="text"></td>
		</tr>
		<tr>
			<td class="MNumber">开户行网点：</td>
			<td><input class="input-200" id="bankcardaddress" name="bankcardaddress" type="text"></td>
		</tr>
		<tr>
			<td class="MNumber"><span class="red">*</span> 银行账号：</td>
			<td><input class="input-200" id="bankId" name="bankId" type="text" value="{{cardNo}}" {{if cardNo}}disabled="disabled"{{/if}}></td>
		</tr>
		<tr>
			<td class="MNumber"><span class="red">*</span> 取款密码：</td>
			<td><input class="input-200" id="cashPassword" name="cashPassword" type="password"></td>
		</tr>
		<tr>
			<td class="MNumber" height="29">&nbsp;</td>
			<td colspan="2"><input onclick="as();" class="pwd-btn-submit pwd-btn" type="button" name="OK" value="下一步" style="margin-left: -100px;margin-top: 5px;"></td>
		</tr>
	</tbody>
</script>