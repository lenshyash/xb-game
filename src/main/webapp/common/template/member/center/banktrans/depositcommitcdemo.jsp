<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<style>
#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

/*20141208-改版 cindy*/
.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: #FFF;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}

.input-155 {
	height: 20px;
	line-height: 20px;
	width: 155px;
}

.pay-list {
	margin: 10px 20px;
}

.border_dot {
	border: 1px dotted #898;
	border-radius: 5px;
	line-height: 1.8em;
	padding: 15px 25px;
	text-align: left;
}
.wxts-div{
 	margin: auto 20px;
    padding: 5px;
}
-->
</style>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<div id="depositTabs"></div>
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<script id="inputdata_tpl" type="text/html">
<span class="step-deposit current-deposit">1.选择要存入的银行，填入您的帐号资料</span> <span class="step-deposit">2.完成</span>
<div class="br_line"></div>
<form id="depostList">
	<div class="pay-list">
		{{each banks as bank count}}
			{{if count > 0 && count %4 ==0}}
				<br>
			{{/if}}
			<div style="width: 159px;height: 85px;float:left;">
			<label class="pointer"><input name="bankId" class="required" value="{{bank.id}}" min="{{bank.min}}" max="{{bank.max}}" type="radio">
				<span class="bankicon {{if !bank.icon}}{{bank.iconCss}}{{/if}}" {{if bank.icon}}style="background-size: 120px 35px; text-indent: -9999px; background-image: url('{{bank.icon}}');"{{/if}}>{{bank.payName}}</span></label>
			<marquee style="color: red;width: 130px;margin-left: 15px;" scrollamount="3" title="{{bank.bankDesc}}">{{bank.bankDesc}}</marquee>
			</div>
		{{/each}}
	</div>
	<div id="dpt_info" class="memdata-content hidn">
		<div class="info-table-area">
			<table border="1" style="table-layout: fixed" class="M-info">
				<colgroup><col width="25%"><col width="75%"></colgroup>
				<tbody>
					<td colspan="2">
						<div class="border_dot">
                        	<p>充值须知：</p>
                         	<p>客服上班时间为：<span class="c_blue">{{start}}</span>至<span class="c_blue">{{end}}</span></p>
                         	<p>转账成功后若超过五分钟未上分，请立即联系客服</p>
                    	 </div>
					</td>
		<c:if test="${showPayInfo}"><tr>
			<td style="text-align: right;">充值银行：</td>
			<td style="text-align: left;" id="cz_yin_hang_id"></td>
		</tr>
		<tr>
			<td style="text-align: right;">收款姓名：</td>
			<td style="text-align: left;" id="Bname_info"></td>
		</tr>
		<tr>
			<td style="text-align: right;">收款帐号：</td>
			<td style="text-align: left;" id="Baccount_info"></td>
		</tr>
		<tr>
			<td style="text-align: right;">开户网点：</td>
			<td style="text-align: left;" id="kai_hu_wang_dian"></td>
		</tr></c:if>
					<tr>
						<td style="text-align:right;">充值金额：</td>
						<td style="text-align:left;"><input id="amount" class="input-155 required" type="text"><span id="amount_desc_id"></span></td>
					</tr>
					<tr>
						<td style="text-align:right;">充值金额(大写)：</td>
						<td style="text-align:left;"><span id="amount_CH" class="c_blue">零</span><span class="c_blue">元整 </span></td>
					</tr>
					<tr>
						<td style="text-align:right;">存款人姓名：</td>
						<td style="text-align:left;"><input class="input-155 required" id="depositor" type="text"></td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${domainFolder == 'b167'}">
								<td style="text-align: center;padding: 10px;" colspan="2"><span class="red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap>请使用微信、支付宝、网银转账至公司入款银行卡号，转账成功后联系客服提供存款人姓名、游戏账号上分。</snap></span></td>
							</c:when>
							<c:otherwise>
								<td  name="${domainFolder}" style="text-align: center;padding: 10px;" colspan="2"><span class="red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap>贴心提示：线下充值非系统自动充值，需会员自行转账到我公司账户，请保证充值金额和存款人姓名与实际转账信息一致！</snap></span></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</tbody>
			</table>
		</div>
		<!--info-table-area余额-->
		<p>&nbsp;</p>
		<div class="br_line"></div>
		<input class="pwd-btn-submit pwd-btn" type="button" id="checkBankSubId" value="<c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if>" onclick="checkBankSub(this);" style="margin-left: 300px;margin-top: 5px;border-radius:11px;11px;">
	</div>
</form>
</script>
<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
<script>
var banks = [],curBank = {};
$(function() {
	var convertCurrency=function(a) {
        var e, g, c, h, l, m, q, t, r, y, w;
        a = a.toString();
        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
        a = a.replace(/,/g, "");
        a = a.replace(/^0+/, "");
        if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
        e = a.split(".");
        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
        h = ["", "\u62fe", "\u4f70", "\u4edf"];
        l = ["", "\u4e07", "\u4ebf"];
        m = ["", ""];
        g = "";
        if (0 < Number(a)) {
            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
            g += ""
        }
        if ("" != e)
            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
        "" == g && (g = "\u96f6");
        "" == e && (g += "");
        return "" + g
    };
    
	$.ajax({
		url : "${base}/center/banktrans/deposit/dptinitc.do",
		success : function(result) {
			var html = template('inputdata_tpl', result);
			$("#depositTabs").html(html);
			var $from = $("#depostList");
//	 	    var $from = $("#uc_onlinecash011_form");
		    $from.on('keyup',"#amount", function(){
		        /* 取出输入的金额,并转换成中文 */
		        $("#amount_CH").text(convertCurrency($( this ).val()));
		    });
			banks = result.banks;
			$from.on("click","label.pointer",function(){
				var $it =$(this),id=$it.find("input").val();
				if(!id)return false;
				for (var i = 0; i < banks.length; i++) {
					if(banks[i].id == id){
						curBank = banks[i];
						break;
					}
				}
				$("#dpt_info").removeClass("hidn");
				<c:if test="${showPayInfo}">
				$("#cz_yin_hang_id").html(curBank.payName);
				$("#Bname_info").html(curBank.creatorName);
				$("#Baccount_info").html(curBank.bankCard+"<span style='color:#000;font-size:20px;'>（转账后请输入金额提交订单）</span>");
				$("#kai_hu_wang_dian").html(curBank.bankAddress);
				</c:if>
				$("#amount_desc_id").html('（单笔充值限额：最低 <span class="red">'+curBank.min+'</span> 元 ，最高<span class="red">'+curBank.max+'</span> 元）');
			});
			
		}
	});
});

function alertfocue(msg, ele) {
	alert(msg);
	if(ele)ele.focus();
	$('#checkBankSubId').removeAttr("disabled");
}

function checkBankSub(obj){
	var $it=$(obj),eleForm = $('#depostList'),paySetting = $("input[name='bankId']:checked"),
       $input=$('#depostList').find("#amount"),bankId=paySetting.val(),
       min = paySetting.attr("min"),
       max = paySetting.attr("max");
	if($it.attr("disabled")){
       	return false;
    }
    $it.attr("disabled","disabled");
    var amount1 = $input.val();
    var depositor = $('#depositor').val();
    
  try{
      min = parseFloat(min,10);
  }catch(e){min = 0;}
  if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
 	 alertfocue("请输入充值正确金额",$input);
      return false;
  }
  amount1 = parseFloat(amount1,10);
  if(amount1<min){
 	 alertfocue("充值金额必须不小于"+ min,$input);
      return false;
  }
 if (!depositor ) {
 	alertfocue("请输入存款人姓名",$('#depositor'));
     return false;
 }
	var cmtData = {
			money : amount1,
			depositor : depositor,
			bankId : bankId
		};
	$.ajax({
		url : base + "/center/banktrans/deposit/dptcommitc.do",
		data : cmtData,
		success : function(result) {
			if(result && result.msg){
				alertfocue(result.msg);
				return;
			}
			for (var i = 0; i < banks.length; i++) {
				if(banks[i].id == bankId){
					curBank = banks[i];
					break;
				}
			}
			curBank.money = amount1;
			curBank.orderNo = result;
			
			$("#depositTabs").html(template('confirm_tpl', curBank));
			bindCopy(curBank);
		}
	});
}
function bindCopy(curBank) {
	$("#Bname2").zclip({
        path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.creatorName,
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Bname2')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Baccount2").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.bankCard,
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Baccount2')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Bamount2").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.money+"",
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Bamount2')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
    $("#Boid2").zclip({
    	path: '${base}/common/js/pasteUtil/ZeroClipboard.swf',
        copy: curBank.orderNo+"",
        afterCopy: function() {
            $("<span id='msg'/>").insertAfter($('#Boid2')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
        }
    });
}
</script>
<script id="confirm_tpl" type="text/html">
<span class="step-deposit">1.选择要存入的银行，填入您的帐号资料</span> <span class="step-deposit current-deposit">2.完成</span>
	<div class="br_line"></div>
<div class="memdata-content">
	<div class="info-table-area">
		<table border="1" style="table-layout: fixed" class="M-info">
			<colgroup>
				<col width="140"><col width="265"><col width="240">
			</colgroup>
			<tbody>
				<tr>
					<td class="MNumber" style="text-align: right;">充值银行：</td>
					<td class="MNumber" style="text-align: left;">{{payName}}</td>
					<td class="MNumber" style="text-align: left;"></td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: right;">收款姓名：</td>
					<td class="MNumber" style="text-align: left;">{{creatorName}}</td>
					<td class="MNumber" style="text-align: left;position: relative;">
						<span class="step-deposit" id="Bname2"><i class="fa fa-files-o"></i> 复 制</span><div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_1" class="zclip"></div>
					</td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: right;">收款帐号：</td>
					<td class="MNumber" style="text-align: left;" xx_xx="Baccount" id="Baccount_info">{{bankCard}}</td>
					<td class="MNumber" style="text-align: left;position: relative;">
						<span class="step-deposit" id="Baccount2"><i class="fa fa-files-o"></i> 复 制</span><div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip"></div>
					</td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: right;">开户网点：</td>
					<td class="MNumber" style="text-align: left;">{{bankAddress}}</td>
					<td class="MNumber" style="text-align: left;"></td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: right;">订单号：</td>
					<td class="MNumber" style="text-align: left;"><span class="c_blue">{{orderNo}}</span></td>
					<td class="MNumber" style="text-align: left;position: relative;">
						<span class="step-deposit" id="Boid2"><i class="fa fa-files-o"></i> 复 制</span>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip"></div>
					</td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: right;">充值金额：</td>
					<td class="MNumber" style="text-align: left;"><span class="red">{{money}}</span></td>
					<td class="MNumber" style="text-align: left;position: relative;">
						<span class="step-deposit" id="Bamount2"><i class="fa fa-files-o"></i> 复 制</span><div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip"></div>
					</td>
				</tr>
				<tr>
					<td class="MNumber" style="text-align: center;" colspan="3"><span class="red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap xx_xx="Note">贴心提醒：充值完成后，请静待3-5分钟重新刷新页面，财务收到款项后会立即为您上分。</snap></span></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!--info-table-area余额-->
</div>
<p>&nbsp;</p>
<div class="br_line"></div>
<input class="pwd-btn-submit pwd-btn" type="button" value="确定" onclick="javascript:Go('${base}/center/banktrans/deposit/page.do');" style="margin-left: 300px;margin-top: 5px;border-radius:11px;11px;">
</script>