<%@ page language="java" pageEncoding="utf-8"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<style>
<!--
body {
	margin: 0;
	padding: 0;
	background: #FFF;
}

#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-new-mny {
	background:
		url(${base}/common/template/member/center/images/login04.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: black;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}
-->
</style>
<form id="JS-forgetpwd-form" class="pwd-form" name="chgFORM">
	<div id="forgetPwd" class="box effect7">
		<div class="pwd-title-area">
			<div class="pwd-title-txt">资金转移</div>
		</div>
		<div class="pwd-input-content">
			<div class="pwd-input-area ">

				<p class="pwd-input-unit">
					<input class="pwd-input" type="text" placeholder="转移会员"
						id="chgAccount" name="chgAccount" value="">
				</p>
				<p class="pwd-input-unit pwd-new-mny ">
					<input class="pwd-input" type="text" placeholder="转移金额"
						id="chgMoney" name="chgMoney" value="">
				</p>

				<p class="pwd-input-unit pwd-new-chk ">
					<input class="pwd-input" type="password" placeholder="资金密码"
						name="password" id="password" value="">
				</p>
			</div>
			<div class="pwd-info-area">
				<div class="userCashPassWord">*资金转移必需输入资金密码</div>
			</div>
			<div>
				<div class="pwd-btn-wrap">
					<input onclick="change();" class=" pwd-btn-submit pwd-btn"
						type="button" name="OK" value="确认"> <input
						class="ui-dialog-titlebar-close pwd-btn-reset pwd-btn "
						type="button" name="cancel" value="取消" onclick=" javascript:i(); ">
					<input name="userid" id="userid" type="hidden" value="">

				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	function change() {
		var chgAccount = $("#chgAccount").val();
		var chgMoney = $("#chgMoney").val();
		var password = $("#password").val();
		if (!chgAccount) {
			parent.layer.msg("转移会员不能为空！");
			return;
		}
		if (!chgMoney) {
			parent.layer.msg("转移金额不能为空！");
			return;
		}
		if (!password) {
			parent.layer.msg("资金密码不能为空！");
			return;
		}
		$.ajax({
			url : "${base}/center/banktrans/mnychg/moneychg.do",
			data : {
				account : chgAccount,
				money : chgMoney,
				password : password
			},
			success : function(result) {
				parent.layer.msg(result.msg || "转移成功！");
				i();
			}
		});
	}

	function i() {
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
</script>
<style type="text/css">
.top_testresult {
	position: absolute;
	width: 232px;
	top: 45px;
	right: 0;
	width: 232px;
	text-align: center;
	padding: 0;
	margin: 0 0 2px 0;
	font-size: 12px;
	font-family: arail, helvetica, san-serif;
}

.top_testresult span {
	display: inline-block;
	width: 50px;
	height: 4px;
	margin: 0 2px;
}

.top_testresult div {
	text-align: right;
	padding-right: 10px;
}

.pwdefault {
	color: #808080;
	background: #808080;
}

div.top_strongPass, div.top_shortPass, div.top_badPass, div.top_goodPass
	{
	background: none;
}

.top_shortPass {
	color: #FF6060;
	background: #FF6060;
}

.top_badPass {
	color: #FCA658;
	background: #FC9130;
}

.top_goodPass {
	color: #00A4AA;
	background: #00A4AA;
}

.top_strongPass {
	color: #1CA032;
	background: #1CA032;
}
</style>