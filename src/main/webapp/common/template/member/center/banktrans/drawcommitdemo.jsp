<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<style>
#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

/*20141208-改版 cindy*/
.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: #FFF;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}

.input-200 {
	height: 20px;
	line-height: 20px;
	width: 200px;
}

.red {
	color: red;
}
-->
</style>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<span class="step-deposit">步骤1:设置取款密码</span> <span class="step-deposit">步骤2:完善出款银行信息</span> <span class="step-deposit current-deposit">步骤3:取款</span>
						<div class="br_line"></div>
						<div class="memdata-content">
							<div class="info-table-area">
								<table id="savebank" border="1" style="table-layout: fixed;text-align: left;" class="M-info">
								</table>
							</div>
						</div>
						<!--_MMainData 基本信息-->
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	var min = 0;
	var max = 0;
	var balance = 0;
	$(function() {
		initDrawdata();
	})

	function initDrawdata() {
		$.ajax({
			url : base + "/center/banktrans/draw/drawdata.do",
			success : function(result) {
				min = result.min;
				max = result.max;
				balance = result.member.money;
				if(result.member.cardNo.length > 10){
					var startFour = result.member.cardNo.substring(0,4)
					var endFour = result.member.cardNo.substring(result.member.cardNo.length - 4)
					result.member.cardNo = startFour + '*******' + endFour
				}
				var html = template('draw_tpl', result);
				$("#savebank").append(html);
			}
		});
	}

	function drawcommit() {
		$("#drawcommit").attr("disabled","disabled");
		var exp = /^[1-9]([\d]{0,7})?$/;
		var m = $("#money").val();
		if (!exp.test(m)) {
			alert("取款金额必须是整数");
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		m = parseInt(m);
		if (m < min) {
			alert("取款最小金额不能小于" + min);
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		if (m > balance) {
			alert("余额不足");
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		if (max != 0 && max < m) {
			alert("取款最大金额不能大于" + max);
			$("#money").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}
		var userQxpassword = $("#userQxpassword").val();
		if (userQxpassword == null || userQxpassword == "") {
			alert("取款密码不能为空");
			$("#userQxpassword").focus();
			$("#drawcommit").removeAttr("disabled");
			return;
		}

		$.ajax({
			url : base + "/center/banktrans/draw/drawcommit.do",
			data : {
				money : m,
				repPwd : userQxpassword
			},
			success : function(result) {
				alert("取款信息已提交!");
				window.location.href = "${base}/center/banktrans/draw/page.do";
			},
			complete:function(){
				$("#drawcommit").removeAttr("disabled");
			}
			
		});
	}

	function j() {
		self.opener = null;
		self.close();
	}
</script>
<script id="draw_tpl" type="text/html">
		<colgroup>
			<col width="25%">
			<col width="75%">
		</colgroup>
		<tbody>
			<tr>
				<td class="MNumber">提示信息：</td>
				<td>每天的取款处理时间为：<span style="font-size: 15px; color: #ffb400;"> {{star}} 至 {{end}}</span>;<br>取款1-3分钟内到账。(如遇高峰期，可能需要延迟到5-10分钟内到帐)<br>用户每日最小提现&nbsp;<span
					style="color: #ffb400; font-size: 15px;">{{min}}</span>&nbsp;元，最大提现&nbsp;<span style="color: #ffb400; font-size: 15px;">{{max}}</span>&nbsp;元<br>今日可取款<span
					style="font-size: 15px; color: #ffb400;"> {{wnum}}</span> 次，已取款<span style="font-size: 15px; color: #ffb400;"> {{curWnum}}</span> 次
				</td>
			</tr>
			<tr>
				<td class="MNumber">消费比例：</td>
				<td>出款需达投注量：<span style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{checkBetNum}}</span>有效投注金额：<span style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{member.betNum}}</span><br>是否能取款：<span
					style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{drawFlag}}</span></td>
			</tr>
			<tr>
				<td class="MNumber">会员账号：</td>
				<td>{{member.account}}</td>
			</tr>
			<tr>
				<td class="MNumber">真实姓名：</td>
				<td>{{member.userName}}</td>
			</tr>
			<tr>
				<td class="MNumber">账户余额：</td>
				<td><span class="red">{{member.money}}</span>RMB</td>
			</tr>
			<tr>
				<td class="MNumber">银行：</td>
				<td>{{member.bankName}}</td>
			</tr>
			<tr>
				<td class="MNumber">开户行：</td>
				<td>{{member.bankAddress}}</td>
			</tr>
			<tr>
				<td class="MNumber">银行账号：</td>
				<td>{{member.cardNo}}</td>
			</tr>
			<tr>
				<td class="MNumber">提款说明：</td>
				<td style="color:red;padding:0 5px 0 5px;line-height:25px;">{{desc}}</td>
			</tr>
			<tr>
				<td class="MNumber"><span class="red">*</span>取款金额：</td>
				<td><input class="input-200" id="money" name="money" type="text"></td>
			</tr>
			<tr>
				<td class="MNumber"><span class="red">*</span>取款密码：</td>
				<td><input class="input-200" id="userQxpassword" name="userQxpassword" type="password"></td>
			</tr>
			<tr>
				<td class="MNumber" height="29">&nbsp;</td>
				<td><input onclick="drawcommit();" id="drawcommit" class="pwd-btn-submit pwd-btn" type="button" name="OK" value="确认" style="margin-left: 100px;margin-top: 5px;"></td>
			</tr>
		</tbody>
</script>