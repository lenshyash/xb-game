<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico"
	type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<script type="text/javascript" src="${base}/common/js/layer/layer.js"></script>
<style>
#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

/*20141208-改版 cindy*/
.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: #FFF;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}

.input-155 {
	height: 20px;
	line-height: 20px;
	width: 155px;
}

.pay-list {
	margin: 10px 20px;
}
-->
</style>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<div id="depositTabs"></div>
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	var baseUrl = "${base}";
	var hostUrl1="${hostUrl1}";
</script>
<script type="text/javascript"
	src="${base }/common/js/onlinepay/pay.js?v=9.1401"></script>
<script id="inputdata_tpl" type="text/html">
			<span class="step-deposit current-deposit">步骤1:请填写付款人资料</span> <span class="step-deposit">步骤2:请确认您的转帐资料</span> <span class="step-deposit">步骤3:完成</span>
			<div class="br_line"></div>
			<form id="onlinePayAFormId">
				<div class="pay-list">
					{{each onlines as online count}}
						{{if count > 0 && count %4 ==0}}
							<br>
						{{/if}}
						<div style="width: 159px;height: 85px;float:left;">
						<label class="pointer"><input name="payId" class="required" data-fixedFlag="{{online.fixedFlag}}" data-randomFlag="{{online.randomFlag}}" data-fixedAmount="{{online.fixedAmount}}" data-randomAmount="{{online.randomAmount}}" min="{{online.min}}" max="{{online.max}}" iconCss="{{online.iconCss}}" payType="{{online.payType}}" value="{{online.id}}" type="radio">
							<!--
							<span class="bankicon {{if !online.icon}}{{online.iconCss}}{{/if}}" {{if online.icon}}style="background-size: 120px 35px; text-indent: -9999px; background-image: url('{{online.icon}}');"{{/if}}>
								{{online.payName}}
							</span>
							 -->
							{{if online.icon}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('{{online.icon}}');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "1"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/banks.png');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "3"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/weixin.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "4"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "5"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/qqpay.png');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "6"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/jdpay.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "7"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/baidu.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "8"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/union.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "9"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/unionpay.png');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "11"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/weixin.jpg');">
									{{online.payName}}
								</span>
							{{ else if online.payType == "12"}}
								<span class="bankicon" style="background-size: 120px 35px; text-indent: -9999px; background-image: url('${base}/common/template/lottery/jiebao/images/zhifubao.jpg');">
									{{online.payName}}
								</span>
							{{else}}
								<span class="bankicon {{online.iconCss}}">
									{{online.payName}}
								</span>
							{{/if}}
						</label>
						<marquee style="color: red;width: 130px;margin-left: 15px;" scrollamount="3" title="{{online.payDesc}}">{{online.payDesc}}</marquee>
						</div>
					{{/each}}
				</div>
				<div id="dpt_info" class="memdata-content hidn">
					<div class="info-table-area">
						<table border="1" style="table-layout: fixed" class="M-info">
							<colgroup><col width="25%"><col width="75%"></colgroup>
							<tbody>
								<tr>
									<td class="MNumber">会员帐号：</td>
									<td class="MNumber" style="text-align:left;">{{account}}</td>
								</tr>
								<tr>
									<td class="MNumber">存入金额：</td>
									<td class="MNumber" style="text-align:left;"><input name="amount" class="input-155 required" id="amountMoney" type="text">（单笔充值限额：最低
                               			<span class="c_blue" id="min_amount">100.00</span> 元）</td>
								</tr>
								<tr id="pcInputMoney">

								</tr>
								<tr>
									<td class="MNumber">存入金额(大写)：</td>
									<td class="MNumber" style="text-align:left;"><span id="amount_CH" class="c_blue">零</span><span class="c_blue">元整 </span></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--info-table-area余额-->
					<style>
						#pcInputMoney span{
							width: 65px;
							color: #000000;
							margin-left: 20px;
						}
						#pcInputMoney span:hover{
							color: #f6383a;
							cursor: pointer;
						}
						.inputActive{
							color: #f6383a!important;
						}
					</style>
               		<div class="br_line"></div>
	            	<div class="btn_submit">
	            		<button class="pwd-btn-submit pwd-btn" id="nextBtn" type="button" style="margin-left: 300px;margin-top: 5px;">下一步</button>
	            	</div>
				</div>
            </form>
</script>
<script>
    var rechargeHtml = '',rechargeNumber = 0;
	$(function() {
		initDptb();
	});
	var pays = [];
	function initDptb() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptinita.do",
			success : function(result) {
				var html = template('inputdata_tpl', result);
				$("#depositTabs").html(html);
				bindIptData();
				pays = result.onlines;
			}
		});
	}
	
	function bindIptData(){
		var convertCurrency=function(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    };
	    var $from = $("#onlinePayAFormId");
	    $from.delegate( "input[name='amount']", 'keyup', function(){
	        /* 取出输入的金额,并转换成中文 */
	        $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
	    $from.on('click', "label", function(e) {
	        var $radio = $("#"+$(this).attr("for"));
	        if (!$radio.prop('checked')) {
	            $radio.trigger('change');
	        }
	    }).on('change', ':radio', function() {
            try{
                var fixedFlag = $(this).attr('data-fixedFlag')
                var randomFlag = $(this).attr('data-randomFlag')
                var fixedAmount = $(this).attr('data-fixedAmount').split(',')
                var randomAmount = $(this).attr('data-randomAmount').split(',')
                $("#amountMoney").attr("disabled",false).css('background','#ffffff')
                $("#pcInputMoney").hide()
                rechargeHtml = ''
                if((fixedFlag == 2 && randomFlag == 1) || (fixedFlag == 2 && randomFlag == 2)){
                    rechargeHtml +='<td></td><td style="text-align: left">'
                    $.each(fixedAmount,function(index,item){
                        rechargeHtml+='<span>'+item+'</span>'
                    })
                    rechargeHtml +='</td>'
                    $("#pcInputMoney").html(rechargeHtml)
                    $("#pcInputMoney").show()
                    $("#amountMoney").attr("disabled","disabled").css('background','#eeeeee')
                    $("#pcInputMoney span").click(function () {
                        $(this).siblings().removeClass('inputActive')
                        $(this).addClass('inputActive')
                        $("#amountMoney").val($(this).html())
                    })
                } else if(fixedFlag == 1 && randomFlag == 2){
                    var index  = Math.floor(Math.random()*randomAmount.length)
                    rechargeNumber = randomAmount[index]
                }
			}catch (e) {

            }
	        var $it = $(this);$it.prop('checked', true);
	        $("#min_amount").html($it.attr("min"));
	        $("#dpt_info").removeClass("hidn");
	    });
	    
	    $('#nextBtn').click(function() {
	        var $it=$(this),eleForm = $(this.form),paySetting = eleForm.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=eleForm.find("input[name='amount']"),
	        amount1 =$input.val(),
            amount1 = parseFloat(amount1) + parseFloat(rechargeNumber)
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	alertfocue("请选择支付方式",$it);return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	alertfocue("请输入充值正确金额",$input);
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	alertfocue("充值金额必须不小于"+ min,$input);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        var iconCss = paySetting.attr("iconCss");
	        var payType = paySetting.attr("payType");
	        dptcommit(amount1,payId,iconCss,payType);
	        return false;
		});
	}

	function alertfocue(msg, ele) {
		layer.alert(msg,{icon:2});
		ele.focus();
		$("#nextBtn").removeAttr("disabled");
	}
	function dptcommit(m, payId, iconCss, payType) {
		topay(m, payId, iconCss, payType, function(result){
			if(result.success == false){
				alert(result.msg);
			} else if(result.success == true){
				if(result && result.data && result.data.formParams){
					var formHiddens = [];
					for(var key in result.data.formParams){
						var value = result.data.formParams[key];
						formHiddens.push({name: key, value: value});
					}
					result.data.formHiddens = formHiddens;
					var html = template('toPayTemplate', result.data);
					$("#depositTabs").html(html);
				}else{
					alert("系统发生错误");
				}
			}
			$("#nextBtn").removeAttr("disabled");
		});
	}
</script>
<script type="text/html" style="display: none;" id="toPayTemplate">
<form action="{{formAction}}" method="post" target='_blank'>
	{{each formHiddens as item}}
	<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}

<span class="step-deposit">步骤1:请填写付款人资料</span> 
<span class="step-deposit current-deposit">步骤2:请确认您的转帐资料</span> 
<span class="step-deposit">步骤3:完成</span>
<div class="br_line"></div>
	<div class="memdata-content">
			<div class="info-table-area">
				<table border="1" style="table-layout: fixed" class="M-info">
					<colgroup><col width="25%"><col width="75%"></colgroup>
					<tbody>
						<tr>
							<td class="MNumber">订单号：</td>
							<td class="MNumber" style="text-align:left;"><span class="c_blue">{{orderId}}</span></td>
						</tr>
						<tr>
							<td class="MNumber">会员帐号：</td>
							<td class="MNumber" style="text-align:left;">{{account}}</td>
						</tr>
						<tr>
							<td class="MNumber">充值金额：</td>
							<td class="MNumber" style="text-align:left;"><span class="red">{{amount}}</span></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--info-table-area余额-->
		</div>
<div class="br_line"></div>
<div class="btn_submit">
<button class="pwd-btn-submit pwd-btn" type='submit' style="margin-left: 300px;margin-top: 5px;" onclick="$(this).hide()">确认送出</button>
</form>
</script>