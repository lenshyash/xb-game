<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
					<c:if test="${userInfo.accountType==6 }"><div>试玩账号不能提款</div></c:if>
		<c:if test="${userInfo.accountType!=6 }">
						<div class="memdata-content">
							<div class="info-table-area">
								<table border="1" style="table-layout: fixed" class="M-info">
									<tbody>
										<tr>
											<th nowrap="">账号</th>
											<th nowrap="">余额</th>
											<th nowrap="">密码</th>
											<th nowrap="">操作</th>
										</tr>
										<tr>
											<td class="MNumber" id="account_td">${userInfo.account}</td>
											<td class="MNumber" id="money_td">${loginMember.money}</td>
											<td class="MNumber"><a href="#" onclick="memberUrl('${base }/center/member/meminfo/pwdpg.do' , '373px' , '434px');return false" class="btn">修改密码</a></td>
											<td class="MNumber"><a href="${base}/center/banktrans/draw/drawpg.do" class="info-sec-txt">取款</a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--info-table-area余额-->
						</div>
						<!--_MMainData 基本信息--></c:if>
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<c:if test="${userInfo.accountType!=6 }"><script type="text/javascript">
	$(function() {
		initMemdata();
	})

	function initMemdata() {
		$.ajax({
			url : base + "/center/member/meminfo/data.do",
			success : function(result) {
				$("#account_td").html(result.account);
				$("#money_td").html(toDecimal2(result.money));
			}
		});
	}
</script></c:if>