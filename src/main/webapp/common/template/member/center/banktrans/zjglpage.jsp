<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico"
	type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>
		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>
			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<div class="mnav">
						<a href="#" class="current">资金管理</a>
					</div>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<div class="imformation">
							<div class="info-img"></div>
							<div class="info-content">
								<p class="info-title">
									<span class="info-title-acname">${loginMember.account}</span>&nbsp;欢迎进入基本信息
								</p>
								<p class="info-txt">帐户：${loginMember.account}</p>
								<p class="info-txt">
									币别：<img align="absmiddle" alt="人民币" title="人民币"
										src="${base }/common/template/member/center/images/money_RMB.gif">&nbsp;人民币(RMB)&nbsp;
								</p>
								<p class="info-txt">等级：${loginMember.levelName}</p>
								<div class="info-sec-title">
									<div title="即时帐号资讯" class="info-sec-txt">即时帐号资讯</div>
								</div>
								<!--info-sec-title即时帐号资讯<-->
							</div>
							<!--info-content欢迎进入基本信息-->
						</div>
						<!--imformation基本信息 top-->

						<div class="memdata-content">
							<div class="info-table-area">
								<table border="1" style="table-layout: fixed" class="M-info">
									<tbody>
										<tr>
											<th nowrap="">系统投注余额</th>
											<th nowrap="">操作</th>
										</tr>
										<tr>
											<td class="MNumber"><fmt:formatNumber type="number"
													value="${loginMember.money}" pattern="0.00"
													maxFractionDigits="2" /></td>
											<td class="MNumber"><a href="#"
												onclick="memberUrl('${base }/center/banktrans/mnychg/chgmnyPage.do' , '373px' , '330px',function(){setTimeout(function(){window.location.reload();},500)});return false"
												class="btn">资金转移</a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--info-table-area余额-->
						</div>
						<!--_MMainData 基本信息-->
					</div>
				</div>
			</div>
			<!--MAContent下-->
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
		<!--MACenter-->
	</div>
</body>
</html>
