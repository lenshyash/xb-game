<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>返点赔率表</title>
    <meta charset="utf-8">
    <script src="${base}/common/js/jquery.min.js"></script>
</head>
<body>
    <div class="userRight">
    <div class="userTitle mgb10">返点赔率表</div>
    <div class="userMain mgb10">
        <ul class="searchFirst">
            <li style="text-align:left;padding-left:20px;"><span>彩种：</span>
                <ins class="selectIcon">
                        <select class="userSelect" name="code" id="lotCode">
                            <c:forEach items="${bigGroup}" var="lot">
                                <option value="${lot.type}">${lot.name}</option>
                            </c:forEach>
                        </select>
                    <em></em>
                 </ins>
                 <ins style="color:#e4393c">*由于数据量较多，如进行查询时出现卡顿请耐心等待或刷新页面</ins>
            </li>
        </ul>
        <div id="leftNav" style="position: relative;"><div id="wanfa">玩法</div><div id="fandian" style="color:#fff;">返点</div></div>
        <div class="searchDetail" style="overflow: auto;height:670px;position: relative;" id="div-table">
            <div class="peilvHead peilvPublic">
                
            </div>
            <div class="peilvBody peilvPublic" >
                
            </div>
            <div class="loadingGif" style="padding: 100px 0px;display:none;text-align:center;"><img src="${base}/common/template/lottery/lecai/images/loadingMobile.gif" /></div>
        </div>
    </div>
</div>
<script>
    $(function(){
        initRdsData($('#lotCode').val())
        $('#lotCode').change(function(){
            initRdsData($(this).val())
        })
    })
    $("#div-table").scroll(function(event){
        var t = document.getElementById("div-table").scrollTop
        , e = document.getElementById("div-table").scrollLeft
        $(".peilvHead").css('left',e+'px')
        $(".heads").css('top',t+'px')
        $(".peilvBody").css('padding-top','35px')
    });
    function initRdsData(type) {
        $(".peilvPublic").html('')
        $(".loadingGif").show()
        $("#leftNav").hide()
         $(".peilvBody").css('padding-top','35px')
        $.ajax({
            url : '${base}/center/df/record/betrecord/getRebateOddsByLot.do',
            data : {
                lotType:type
            },
            success : function(res) {
            	if(!res.success){
            		$(".loadingGif").hide()
            		alert(res.msg)
            		return false;
            	}
                $(".loadingGif").hide()
                var headJson = []
                var head = '<ul style="position: relative;margin-top:36px;padding: 0;">';
                for(var i = 0; i < res.content.length; i++){
                	if(i == 0){
                        head += '<li style="border-top: 1px solid #33963a;">'+res.content[i].name+'</li>'
                	}else{
                        head += '<li>'+res.content[i].name+'</li>'		
                	}

                }
                head += '</ul>'
                $(".peilvHead").html(head)
                var head2 = '<ul class="heads"  style="position: absolute;top: 0px;margin: 0px;padding: 0;">';
                $.each(res.content[0].items,function(index, item){
                    headJson.push(index)
                })
                headJson.sort(sortNumber)
                $.each(headJson,function(index, item){
                    head2 += '<li style="border-right: 1px solid #fff;">'+item+'</li>'
                })
                $(".peilvBody").css('width',headJson.length*117+'px')
                head2 += '</ul>'
                var html = '';
                for(var i = 0; i < res.content.length; i++){
                    var peilvJson = []
                    if(i == 0){
                    	html+='<ul style="margin: 0px;padding: 0;border-top: 2px solid #33963a;">'
                    }else{
                    	html+='<ul style="margin: 0px;padding: 0;">'
                    }
                   
                        $.each(res.content[i].items,function(index, item){
                            peilvJson.push(item)
                        })
                        peilvJson.sort(sortNumber)
                        $.each(peilvJson,function(index, item){
                            html+='<li>'+item+'</li>'
                        })
                    html+='</ul>'
                }
                head2+=html
                $(".peilvBody").html(head2)
                $(".heads").css('width',headJson.length*117+'px')
                $("#leftNav").show()
            },
            error: function(res){
                
            }
        })
    }
    function sortNumber(a,b){
          return b - a;
    }
    </script>
<style>
    .userRight {
    float: left;
    width: 100%;
    box-sizing: border-box;
    margin-left: 10px;
    margin: 0 auto;
}
.userTitle {
    border: 1px solid #ddd;
    background: #f8f8f8;
    line-height: 32px;
    font-weight: 700;
    font-size: 14px;
    padding-left: 10px;
    color: #fff;
    background: #33963a;
}
.mgb10 {
    margin-bottom: 10px;
}
.userMain {
    border: 1px solid #33963a;
    min-height: 703px;
    background: #fff;
}
.searchFirst {
    margin-bottom: 1px;
	padding-left: 0;
    display: table;
    width: 100%;
    text-align: center;
    line-height: 76px;
    font-weight: 700;
    background: #f8f8f8;
    border-bottom: 1px solid #33963a;
}
.searchFirst li {
    display: table-cell;
    vertical-align: middle;
}
.searchFirst li span {
    font-size: 14px;
    color: #000;
}
.selectIcon {
    position: relative;
}
ins {
    margin: 0 4px;
}

ins {
    text-decoration: none;
}
.userSelect {
    height: 30px;
    width: 125px;
    line-height: 28px;
    border: 1px solid #33963a;
    color: #666;
    padding: 0 5px;
    background: #fff;
    border-radius: 0;
}
option {
    font-weight: normal;
    display: block;
    white-space: pre;
    min-height: 1.2em;
    padding: 0px 2px 1px;
}
.selectIcon em {
    right: 10px;
    top: 9px;
    position: absolute;
    width: 0;
    height: 0;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 6px solid #33963a;
  /*  display: none\0/IE89;*/
}
#leftNav {
    width: 219px;
    z-index: 9999;
}
#wanfa {
    position: absolute;
    color: #000;
    z-index: 10;
    top: 11px;
    left: 4px;
    font-weight: 600;
    line-height: 29px;
}
#fandian {
    position: absolute;
    color: #000;
    z-index: 10;
    right: 4px;
    font-weight: 600;
    line-height: 29px;
}
#leftNav:after {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    border-bottom: 36px solid #f3f3f3;
    border-right: 219px solid #33963a;
    content: "";
}
.searchDetail .peilvHead {
    width: 220px;
    border-right: 1px solid #33963a;
    position: absolute;
}
.searchDetail .peilvPublic {
    float: left;
}
ul {
    list-style: none;
}
li {
    list-style: none;
}
.searchDetail .peilvHead li {
    width: 100%;
    height: 36px;
    border-bottom: 1px solid #33963a;
    background: #f3f3f3;
    text-align: center;
    line-height: 39px;
    font-weight: 600;
    margin-left: -1px;
}
.searchDetail .peilvBody {
    /*width: 610px;*/
    padding-left: 220px;
    box-sizing: content-box;
}
.searchDetail .peilvPublic {
    float: left;
}
.searchDetail .peilvBody .heads {
    background: #33963a;
}
.searchDetail .peilvBody ul {
    float: left;
    width: 100%;
    border-right: 1px solid #33963a;
}
.searchDetail .peilvBody li {
    width: 116px;
    height: 36px;
    border-bottom: 1px solid #33963a;
    text-align: center;
    line-height: 39px;
    float: left;
    border-right: 1px solid #33963a;
}
</style>
</body>
</html>