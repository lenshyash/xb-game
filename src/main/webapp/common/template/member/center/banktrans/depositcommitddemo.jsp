<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>${_title}</title>
    <link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <jsp:include page="/common/template/member/base.jsp"></jsp:include>
    <style>
        #forgetPwd {
            margin: 0 auto;
            /*padding: 10px 0;*/
            width: 373px;
            color: #000;
            font-size: 12px;
        }

        .pwd-remind {
            margin: 0 auto 20px;
            width: 340px;
            text-align: center;
        }

        .chg_passwd_flag {
            font-size: 14px;
            color: #666;
            text-align: center;
            margin: 0 10px;
        }

        /*20141208-改版 cindy*/
        .box {
            position: relative;
        }

        .pwd-title-area {
            width: 373px;
            height: 84px;
            background:
                    url(${base}/common/template/member/center/images/bg_gray.png) center
                    top no-repeat;
        }

        .pwd-input-content {
            padding-bottom: 30px;
            background:
                    url(${base}/common/template/member/center/images/bg_gray01.png) center
                    top repeat-y;
        }

        .pwd-title-txt {
            float: left;
            height: 38px;
            font-size: 16px;
            color: #56606C;
            line-height: 35px;
            padding-left: 34px;
            margin: 21px 0 9px 28px;
            background:
                    url(${base}/common/template/member/center/images/titleicon.png) left
                    center no-repeat;
        }

        .pwd-input-unit {
            display: block;
            position: relative;
            width: 193px;
            height: 34px;
            margin: 0 auto 6px;
            padding-left: 55px;
            background:
                    url(${base}/common/template/member/center/images/login01.png) 0 0
                    no-repeat;
        }

        .pwd-new {
            margin-bottom: 33px;
            background:
                    url(${base}/common/template/member/center/images/login02.png) 0 0
                    no-repeat;
        }

        .pwd-new-chk {
            background:
                    url(${base}/common/template/member/center/images/login03.png) 0 0
                    no-repeat;
        }

        .pwd-placeholder-txt {
            position: absolute;
            width: 185px;
            top: 9px;
            left: 60px;
            color: #FFF;
            z-index: 1
        }

        .pwd-input {
            position: relative;
            z-index: 2;
            width: 180px;
            height: 32px;
            color: #FFF;
            border: none;
            outline: none;
            line-height: 32px;
            background: transparent;
        }

        .pwd-info-area {
            color: #504C4C;
            line-height: 20px;
            text-align: center;
            margin-top: 10px;
        }

        .pwd-btn-wrap {
            width: 250px;
            margin: 15px auto 5px;
        }

        .pwd-btn {
            display: inline-block;
            width: 115px;
            height: 34px;
            outline: none;
            border: none;
            padding: 0;
            cursor: pointer;
        }

        .pwd-btn:hover {
            background-position: center bottom;
        }

        .pwd-btn-submit {
            background: url(${base}/common/template/member/center/images/btn01.png)
            0 0 no-repeat;
        }

        .pwd-btn-reset {
            /* 	background: url(${base}/common/template/member/center/images/btn02.png) */
            /* 		0 0 no-repeat; */
            border-radius:10px;
        }

        .pwerror-wrap {
            position: relative;
            display: block;
            z-index: 2;
            width: 322px;
            height: 35px;
            font-size: 12px;
            line-height: 15px;
            color: #F00;
            font-weight: bold;
            text-align: left;
            padding: 10px 0 0 48px;
            margin: 0 auto -25px;
            background:
                    url(${base}/common/template/member/center/images/top_txt.png) center
                    top no-repeat;
        }

        .en .pwerror-wrap {
            line-height: 15px;
        }

        .input-155 {
            height: 20px;
            line-height: 20px;
            width: 155px;
        }

        .pay-list {
            margin: 10px 20px;
        }

        .border_dot {
            border: 1px dotted #898;
            border-radius: 5px;
            line-height: 1.8em;
            padding: 15px 25px;
            text-align: left;
        }
        .wxts-div{
            margin: auto 20px;
            padding: 5px;
        }
        -->
    </style>
</head>
<body>
<div id="MACenter">
    <!--MAHeader头部-->
    <jsp:include page="../head.jsp"></jsp:include>

    <div id="MAContent">
        <!--MALeft左边菜单栏-->
        <jsp:include page="../leftdemo.jsp"></jsp:include>

        <!-- 主内容区 -->
        <div class="MAMain tzzx-height">
            <div id="main_wrap">
                <!-- 内容区菜单 -->
                <jsp:include page="../transhead.jsp"></jsp:include>
                <!--菜单mnav-->
                <!-- 内容区 -->
                <div id="_MMainData" class="member-data">
                    <div id="depositTabs" style="position: relative;"></div>
                </div>
            </div>
            <!--yhjy-edzh额度转换	-->
        </div>
        <div id="MAContentBottom"></div>
        <div id="MAFoot">${copyright }</div>
    </div>
</div>
</body>
</html>
<script id="inputdata_tpl" type="text/html">
    <span class="step-deposit current-deposit">1.选择要存入的方式，填入您的帐号资料</span> <span class="step-deposit">2.完成</span>
    <div class="br_line"></div>
    <form id="onlineQuickPayFormId">
        <div class="pay-list">
            {{each virtuals as virtual count}}
            {{if count > 0 && count %4 ==0}}
            <br>
            {{/if}}
            <div style="width: 159px;height: 85px;float:left;">
                <label class="pointer" id="{{virtual.iconCss}}" courseVircoul="{{virtual.tipsShowUrl}}" courseVircoulOpenSrc="{{virtual.tipsRedirectUrl}}" referenceRateRmb="{{virtual.rate}}"referenceRate="1{{virtual.payChannel}}≈{{virtual.rate}}RMB" flabel="{{virtual.frontLabel}}"><input name="payId" class="required"iconCss="{{virtual.iconCss}}" min="{{virtual.min}}" max="{{virtual.max}}" value="{{virtual.id}}" type="radio">
                    <span class="bankicon {{if !virtual.icon}}{{virtual.iconCss}}{{/if}}" {{if virtual.icon}}style="background-size: 120px 35px; text-indent: -9999px; background-image: url('{{virtual.icon}}');"{{/if}}>{{virtual.payName}}</span>
                </label>
                <marquee style="color: red;width: 130px;margin-left: 15px;" scrollamount="3" title="{{virtual.qrcodeDesc}}">{{virtual.qrcodeDesc}}</marquee>
            </div>
            {{/each}}
        </div>
        <div id="dpt_info" class="memdata-content hidn">
            <div class="info-table-area">
                <table border="1" style="table-layout: fixed" class="M-info">
                    <colgroup><col width="22%"><col width="25%"><col width="53%"></colgroup>
                    <tbody>
                    <tr>
                        <td colspan="3">
                            <div class="border_dot">
                                <div>虚拟币充值使用须知：</div>
                                <div>客服上班时间为：<span class="c_blue">{{start}}</span>至<span class="c_blue">{{end}}</span> <br>选择充值方式，填写充值金额及其账户信息，扫描二维码充值后，<span class="c_blue">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即客服联系</div>
                            </div>
                        </td></tr>
                    <c:if test="${showPayInfo}">
<%--                        <tr>--%>
<%--                            <td style="text-align: right;">收款姓名：</td>--%>
<%--                            <td id="Bname_info"></td>--%>
<%--                            <td style="text-align: left;position: relative;">--%>
<%--                                <span class="step-deposit" id="Bname"> 复 制</span>--%>
<%--                                <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" class="zclip"></div>--%>
<%--                            </td>--%>
<%--                        </tr>--%>
                        <tr>
                            <td style="text-align: right;">收款帐号：</td>
                            <td id="Baccount_info"></td>
                            <td style="text-align: left;position: relative;">
                                <span class="step-deposit" id="Baccount"> 复 制</span>
                                <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" class="zclip"></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">二维码：</td>
                            <td style="text-align: left;" colspan='2'><img src="#" id="qrcode_img_id"style="width: 300px; height: 300px;">
<%--                                <div style="font-size:20px;color:red;margin-top:6px">扫码转账付款时请备注会员账号，支付成功后，请填写充值金额，并点击确定充值，否则无法为您上分</div></td>--%>
                        </tr>
                        <tr>
                            <td style="text-align: right;">链类型：</td>
                            <td id="pay_type_name"></td>
                            <td></td>
                        </tr>
                    </c:if>
                    <tr class="referenceRateTr">
                        <td style="text-align:right;">参考汇率：</td>
                        <td class="referenceRate">1ust≈2RMB</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">充值数量：</td>
                        <td><input id="amount" class="input-155 required" type="text"></td>
                        <td id="dan_bi_limit">（单笔充值限额：最低<span class="c_blue" id="min_amount"></span> 个）</td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">充值个数(大写)：</td>
                        <td style="text-align:left;">
                            <span id="amount_CH" class="c_blue">零</span><span class="c_blue">个 </span>
                        </td>
                        <td><span>约等于:</span><span class="approximateRmb">0RMB</span></td>
                    </tr>
<%--                    <tr id="payType_trId">--%>
<%--                        <td style="text-align:right;"><span class="payType">充微信或支付宝</span>：</td>--%>
<%--                        <td><input class="input-155 required number" minlength="5" maxlength="20" id="bank_cards" type="text"></td>--%>
<%--                        <td><span class="red">请填写正确的<span class="payType">微信号或支付宝</span>，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</span></td>--%>
<%--                    </tr>--%>
                    </tbody>
                </table>
            </div>
<%--            教程--%>
<%--            <div style="width: 100px;height: 100px;border-radius: 50%;position: absolute;">--%>
<%--                <img class="courseVircoul" src="" alt="" style="border-radius: 50%;height: 100%;width: 100%;position: absolute;left: 0;">--%>
<%--            </div>--%>
            <!--info-table-area余额-->
<%--            <div class="br_line"></div>--%>
<%--            <div class="wxts-div">--%>
<%--                <span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="c_blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>--%>
<%--            </div>--%>
            <p>&nbsp;</p>
            <div class="br_line"></div>
            <input class="pwd-btn-submit pwd-btn" type="button" id="checkFastSubId" value="<c:if test="${showPayInfo}">确定充值</c:if><c:if test="${!showPayInfo}">下一步</c:if>" onclick="javaScript:checkFastSub(this);" style="margin-left: 300px;margin-top: 5px;">
        </div>
    </form>
</script>
<script type="text/javascript">
    var baseUrl = "${base}";
    var hostUrl1="${hostUrl1}";
</script>
<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
<script type="text/javascript"
        src="${base }/common/js/onlinepay/pay.js?v=9.1401"></script>
<script>
    var pays = [],bank_cards_check=true,curPay={},referenceRateRmb='';
    $(function() {
        var convertCurrency=function(a) {
            var e, g, c, h, l, m, q, t, r, y, w;
            a = a.toString();
            if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
            a = a.replace(/,/g, "");
            a = a.replace(/^0+/, "");
            if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
            e = a.split(".");
            1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
            c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
            h = ["", "\u62fe", "\u4f70", "\u4edf"];
            l = ["", "\u4e07", "\u4ebf"];
            m = ["", ""];
            g = "";
            if (0 < Number(a)) {
                for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
                g += ""
            }
            if ("" != e)
                for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
            "" == g && (g = "\u96f6");
            "" == e && (g += "");
            return "" + g
        };

        $.ajax({
            url : "${base}/center/banktrans/deposit/dptinitd.do",
            success : function(result) {
                var html = template('inputdata_tpl', result);
                $("#depositTabs").html(html);
                pays = result.virtuals;
                $('.pointer').click(function(){


                    $('#amount').val('')
                    $( ".approximateRmb" ).text(0+'RMB');
                    $( "#amount_CH" ).text('零');

                    var $it = $(this),flabel=$it.attr("flabel");referenceRate=$it.attr('referenceRate');referenceRateRmb=$it.attr('referenceRateRmb')

                    //教程
                    $(".courseVircoul").attr('src',$it.attr('courseVircoul'))
                    $(".courseVircoul").on('click',function () {
                        window.open($it.attr('courseVircoulOpenSrc'))
                    })

                    if(flabel){
                        $("#payType_trId").show();
                        $('.payType').html(flabel);
                        bank_cards_check=true;
                        payType = flabel;
                    }else{
                        $("#payType_trId").hide();
                        bank_cards_check=false;
                    }
                    if (referenceRate){
                        $('.referenceRateTr').show();
                        $('.referenceRate').html(referenceRate);
                    }else {
                        $('.referenceRateTr').hide();
                    }
                    dptcommit($it.find("input").val());
                });
            }
        });
        function dptcommit(payId) {
            if(!payId)return;
            for (var i = 0; i < pays.length; i++) {
                if(pays[i].id == payId){
                    curPay = pays[i];
                    break;
                }
            }
            $("#dpt_info").removeClass("hidn");
            <c:if test="${showPayInfo}">
            $("#pay_type_name").html(curPay.payName);
            $("#qrcode_img_id").attr("src",curPay.qrCodeImg);
            $('#Bname_info').html(curPay.payUserName);
            $('#Baccount_info').html(curPay.payAccount);
            </c:if>

            $('#dan_bi_limit').html('（单笔充值限额：最低 <span id="min_amount" class="red">'+curPay.min+'</span> 个 ，最高<span id="max_amount" class="red">'+curPay.max+'</span> 个）');
            copyInitFn();
            $('#onlineQuickPayFormId').on('keyup', "#amount", function(){
                /* 取出输入的金额,并转换成中文 */
                $( ".approximateRmb" ).text(((parseFloat(Number($( this ).val()) * 1000) * parseFloat(Number(referenceRateRmb)* 1000)) / 1000000 )+'RMB');
                $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
            });

        }
        var copyBtnIsInit=false;
        function copyInitFn(){
            if(copyBtnIsInit)return;
            copyBtnIsInit=true;
            $("#Bname").zclip({
                path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
                copy : curPay.payUserName,
                afterCopy : function() {
                    $("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
                }
            });
            $("#Baccount").zclip({
                path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
                copy : curPay.payAccount,
                afterCopy : function() {
                    $("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
                }
            });
        }
    });
    function alertfocue(msg, ele) {
        layer.msg(msg);
        if(ele)ele.focus();
        $('#checkFastSubId').removeAttr("disabled");
    }
    function checkFastSub(obj){
        var $it=$(obj),eleForm = $('#onlineQuickPayFormId'),paySetting = eleForm.find("input[name='payId']:checked"),
            min = paySetting.attr("min"),
            max = paySetting.attr("max");
        if($it.attr("disabled")){
            return false;
        }
        $it.attr("disabled","disabled");
        var payId=paySetting.val();
        if(!payId){
            alertfocue("请选择支付方式",$it);return false;
        }
        var amount1 = $('#amount').val();
        var bank_cards = $('#bank_cards').val();
        try{
            min = parseFloat(min,10);
        }catch(e){min = 0;}
        try{
            max = parseFloat(max,10);
        }catch(e){max = 0;}
        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
            alertfocue("请输入充值正确个数",$('#amount'));
            return false;
        }
        amount1 = parseFloat(amount1,10);
        if(amount1<min){
            alertfocue("充值金额必须不小于"+ min,$('#amount'));
            return false;
        }
        if(max>0 && amount1 > max){
            alertfocue("充值金额必须不大于"+ max,$('#amount'));
            return false;
        }
        // if (bank_cards_check &&( !bank_cards ||bank_cards.length == 0) ){
        //     alertfocue("请输入付款帐号",$('#bank_cards'));
        //     return false;
        // }
        var iconCss = paySetting.attr("iconCss");
        var payType = paySetting.attr("payType");
        dptcommit(amount1,payId,iconCss,payType);
        return false;
    }
    function dptcommit(m, payId, iconCss, payType) {
        topayVirtual(m, payId, iconCss, payType, function(result){
            if(result.success == false){
                alert(result.msg);
            } else if(result.success == true){
                if(result && result.data && result.data.formParams){
                    var formHiddens = [];
                    for(var key in result.data.formParams){
                        var value = result.data.formParams[key];
                        formHiddens.push({name: key, value: value});
                    }
                    result.data.formHiddens = formHiddens;
                    var html = template('toPayTemplate', result.data);
                    $("#depositTabs").html(html);
                }else{
                    alert("系统发生错误");
                }
            }
            $("#nextBtn").removeAttr("disabled");
        });
    }

    function bindCopy(curPay) {
        $("#Bname1").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.payUserName,
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Bname1')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Baccount1").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.payAccount,
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Baccount1')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Bamount1").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.money+"",
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Bamount1')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
        $("#Boid1").zclip({
            path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
            copy : curPay.orderNo+"",
            afterCopy : function() {
                $("<span id='msg'/>").insertAfter($('#Boid1')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
            }
        });
    }
    function goDepositIndex(){
        alert("已为您提交充值！扫描您要入款的金额付款、即可到账");
        Go('${base}/center/banktrans/deposit/page.do');
    }
</script>
<script id="confirm_tpl" type="text/html">
    <span class="step-deposit">1.选择要存入的方式，填入您的帐号资料</span> <span class="step-deposit current-deposit">2.完成</span>
    <div class="br_line"></div>
    <div class="memdata-content">
        <div class="info-table-area">
            <table border="1" style="table-layout: fixed" class="M-info">
                <colgroup><col width="140"><col width="265"><col width="240"></colgroup>
                <tbody>
                <tr>
                    <td class="MNumber" style="text-align: right;">充值方式：</td>
                    <td class="MNumber" style="text-align: left;">{{payName}}</td>
                    <td class="MNumber" style="text-align: left;"></td>
                </tr>
<%--                <tr>--%>
<%--                    <td class="MNumber" style="text-align: right;">收款姓名：</td>--%>
<%--                    <td class="MNumber" style="text-align: left;">{{payUserName}}</td>--%>
<%--                    <td class="MNumber" style="text-align: left;position: relative;">--%>
<%--                        <span class="step-deposit" id="Bname1"> 复 制</span>--%>
<%--                        <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">--%>
<%--                        </div>--%>
<%--                    </td>--%>
<%--                </tr>--%>
                <tr>
                    <td class="MNumber" style="text-align: right;">收款帐号：</td>
                    <td class="MNumber" style="text-align: left;">{{payAccount}}</td>
                    <td class="MNumber" style="text-align: left;position: relative;">
                        <span class="step-deposit" id="Baccount1"> 复 制</span>
                        <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="MNumber" style="text-align: right;">订单号：</td>
                    <td class="MNumber" style="text-align: left;"><span class="c_blue">{{orderNo}}</span></td>
                    <td class="MNumber" style="text-align: left;position: relative;"><span class="step-deposit" id="Boid1"> 复 制</span>
                        <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="MNumber" style="text-align: right;">充值金额：</td>
                    <td class="MNumber" style="text-align: left;"><span class="red">{{money}}</span></td>
                    <td class="MNumber" style="text-align: left;position: relative;">
                        <span class="step-deposit" id="Bamount1"> 复 制</span>
                        <div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="MNumber" style="text-align: right;">二维码：</td>
                    <td class="MNumber" style="text-align: left;"><img src="{{qrCodeImg}}" title="{{qrCodeImg}}" style="width: 300px; height: 300px;"></td>
                    <td class="MNumber" style="text-align: left;"></td>
                </tr>
                <tr>
                    <td class="MNumber" style="text-align: center;" colspan="3"><span class="red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap xx_xx="Note"></snap></span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--info-table-area余额-->
    </div>
    <div class="wxts-div">
        <span class="red">&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="c_blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
    </div>
    <p>&nbsp;</p>
    <div class="br_line"></div>
    <input class="pwd-btn-submit pwd-btn" type="button" name="cancel" value="确定" onclick="javaScript:goDepositIndex();" style="margin-left: 300px;margin-top: 5px;">
</script>