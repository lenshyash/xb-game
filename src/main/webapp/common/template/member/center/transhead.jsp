<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mnav">
	<div class="navSeparate"></div>
	<a ${comFlag } href="${base}/center/banktrans/deposit/page.do">线上存款</a>
	<div class="navSeparate"></div>
	<a ${drawFlag } href="${base}/center/banktrans/draw/page.do">线上取款</a>
	<div class="navSeparate"></div>
	<c:if test="${isExChgOnOff eq 'on'}">
		<a ${exchangeFlag } href="${base}/center/banktrans/exchange/page.do">积分兑换</a>
	</c:if>
</div>
<!--菜单mnav-->