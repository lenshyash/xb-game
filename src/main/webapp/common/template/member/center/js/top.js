﻿function urlparent(url){
	window.open(url,"newFrame");
}

function url_bb(url){
	window.open(url,"iframepage"); 
}

function t_url(url){
    window.open(url,"iframepage"); 
}

function memberUrl(url,wid,hei,callback) {
	$.layer({
		type : 2,
		shadeClose : false,
		fix : false,
		title : "会员中心",
		offset : ['60px' , '570px'],
		closeBtn : [0, true],
		iframe : { src : url },
		area : [wid , hei],
		end: function () {
			callback();
	    }
	});
}

//链接转换
function c_url(url){
	window.open(url, "_self", "width=1024,height=610,scrollbars=1",true);
}


function reflash() {
	window.location.reload();
}
function Go(url){
	window.location.href=url;
}


//制保留2位小数，如：2，会在2后面补上00.即2.00 
function toDecimal2(x) {
	var f = parseFloat(x);
	if (isNaN(f)) {
		return false;
	}
	var f = Math.round(x * 100) / 100;
	var s = f.toString();
	var rs = s.indexOf('.');
	if (rs < 0) {
		rs = s.length;
		s += '.';
	}
	while (s.length <= rs + 2) {
		s += '0';
	}
	return s;
}