<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<style>
#forgetPwd {
	margin: 0 auto;
	/*padding: 10px 0;*/
	width: 373px;
	color: #000;
	font-size: 12px;
}

.pwd-remind {
	margin: 0 auto 20px;
	width: 340px;
	text-align: center;
}

.chg_passwd_flag {
	font-size: 14px;
	color: #666;
	text-align: center;
	margin: 0 10px;
}

/*20141208-改版 cindy*/
.box {
	position: relative;
}

.pwd-title-area {
	width: 373px;
	height: 84px;
	background:
		url(${base}/common/template/member/center/images/bg_gray.png) center
		top no-repeat;
}

.pwd-input-content {
	padding-bottom: 30px;
	background:
		url(${base}/common/template/member/center/images/bg_gray01.png) center
		top repeat-y;
}

.pwd-title-txt {
	float: left;
	height: 38px;
	font-size: 16px;
	color: #56606C;
	line-height: 35px;
	padding-left: 34px;
	margin: 21px 0 9px 28px;
	background:
		url(${base}/common/template/member/center/images/titleicon.png) left
		center no-repeat;
}

.pwd-input-unit {
	display: block;
	position: relative;
	width: 193px;
	height: 34px;
	margin: 0 auto 6px;
	padding-left: 55px;
	background:
		url(${base}/common/template/member/center/images/login01.png) 0 0
		no-repeat;
}

.pwd-new {
	margin-bottom: 33px;
	background:
		url(${base}/common/template/member/center/images/login02.png) 0 0
		no-repeat;
}

.pwd-new-chk {
	background:
		url(${base}/common/template/member/center/images/login03.png) 0 0
		no-repeat;
}

.pwd-placeholder-txt {
	position: absolute;
	width: 185px;
	top: 9px;
	left: 60px;
	color: #FFF;
	z-index: 1
}

.pwd-input {
	position: relative;
	z-index: 2;
	width: 180px;
	height: 32px;
	color: #FFF;
	border: none;
	outline: none;
	line-height: 32px;
	background: transparent;
}

.pwd-info-area {
	color: #504C4C;
	line-height: 20px;
	text-align: center;
	margin-top: 10px;
}

.pwd-btn-wrap {
	width: 250px;
	margin: 15px auto 5px;
}

.pwd-btn {
	display: inline-block;
	width: 115px;
	height: 34px;
	outline: none;
	border: none;
	padding: 0;
	cursor: pointer;
}

.pwd-btn:hover {
	background-position: center bottom;
}

.pwd-btn-submit {
	background: url(${base}/common/template/member/center/images/btn01.png)
		0 0 no-repeat;
}

.pwd-btn-reset {
	background: url(${base}/common/template/member/center/images/btn02.png)
		0 0 no-repeat;
}

.pwerror-wrap {
	position: relative;
	display: block;
	z-index: 2;
	width: 322px;
	height: 35px;
	font-size: 12px;
	line-height: 15px;
	color: #F00;
	font-weight: bold;
	text-align: left;
	padding: 10px 0 0 48px;
	margin: 0 auto -25px;
	background:
		url(${base}/common/template/member/center/images/top_txt.png) center
		top no-repeat;
}

.en .pwerror-wrap {
	line-height: 15px;
}
-->
</style>
</head>
<body>
	<div id="MACenter">
		<!--MAHeader头部-->
		<jsp:include page="../head.jsp"></jsp:include>

		<div id="MAContent">
			<!--MALeft左边菜单栏-->
			<jsp:include page="../leftdemo.jsp"></jsp:include>

			<!-- 主内容区 -->
			<div class="MAMain tzzx-height">
				<div id="main_wrap">
					<!-- 内容区菜单 -->
					<jsp:include page="../transhead.jsp"></jsp:include>
					<!--菜单mnav-->
					<!-- 内容区 -->
					<div id="_MMainData" class="member-data">
						<span class="step-deposit current-deposit">步骤1:设置取款密码</span> <span class="step-deposit">步骤2:完善出款银行信息</span> <span class="step-deposit">步骤3:取款</span>
						<div class="br_line"></div>
						<div class="pwd-input-area ">
							<p class="pwd-input-unit pwd-new">
								<input type="password" class="password_adv pwd-input" name="password" id="password" value="" size="12" maxlength="12" placeholder="取款密码">
							</p>
							<p class="pwd-input-unit pwd-new-chk ">
								<input class="pwd-input" type="password" name="rpassword" id="rpassword" value="" size="12" maxlength="12" placeholder="确认密码">
							</p>
						</div>
						<div class="pwd-info-area">
							*密码规则：须由<font color="red"><b>6位数字<br>。
							</b></font>
						</div>
						<div>
							<div class="pwd-btn-wrap">
								<input onclick="updpwd();" class="pwd-btn-submit pwd-btn" type="button" name="OK" value="下一步" style="margin-left: 50px;"><input name="userid" id="userid" type="hidden" value="">
							</div>
						</div>
						<!--_MMainData 基本信息-->
					</div>
				</div>
				<!--yhjy-edzh额度转换	-->
			</div>
			<div id="MAContentBottom"></div>
			<div id="MAFoot">${copyright }</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	function updpwd() {
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if (!password) {
			alert("新密码不能为空！");
			return;
		}
		if (!rpassword) {
			alert("确认不能为空！");
			return;
		}

		if (password !== rpassword) {
			alert("两次密码不一致！");
			return;
		}
		$.ajax({
			url : "${base}/center/member/meminfo/repwd.do",
			data : {
				pwd : password,
				rpwd : rpassword
			},
			success : function(result) {
				alert("修改成功！");
				top.location.href = "${base}/center/banktrans/draw/drawpg.do";
			}
		});
	}

	function i() {
		self.opener = null;
		self.close();
	}
</script>
