<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
   	 
     	 <div class="main">
  <h1>
    <span>更改密码</span>
    <!--span class="close_box" onClick="window.close();">关闭</span-->
  </h1>
  <input type="hidden" name="action" value="1"> <input type="hidden" name="uid" value="13d111a9e000cbd82ff99"> <input type="hidden" name="flag" value="1">
  <div class="main_bg">
    <div class="main_box">
      <h2></h2>
      <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
        <tbody>
          <tr>
            <td class="keyin">
              <p>
                <em>新密码：</em><input type="password" name="password" id="password" value="" size="12" maxlength="12">
              </p>
              <p>
                <em>确认新密码：</em><input type="password" name="rpassword" id="rpassword" value="" size="12" maxlength="12">
              </p>
            </td>
            <td class="info"><span><tt class="bb">说明:</tt></span> <span><tt>1.</tt> 请您重新设置您的密码</span> <span><tt>2.</tt> 您的新密码必须由6-16个字母和数字<br>(A-Z 和 0-9)组成。</span></td>
          </tr>
        </tbody>
      </table>
      <div style="color:red;margin:13px 0 0 55px;">您已经很久没有更改密码 为了您的账号及资金安全，请不定期的更改登陆密码</div>
    </div>
    <div class="foot">
      <input type="button" name="OK" value="确认" class="yes" onclick="updpwd();"> <input type="button" name="cancel" value="返回首页" class="no" onclick="javascript:top.window.location='${base}/'">
    </div>
  </div>
  <div class="bottom"></div>
</div>

<script type="text/javascript">
  function updpwd() {
    var password = $("#password").val();
    var rpassword = $("#rpassword").val();
    if (!password) {
      alert("新密码不能为空！");
      return;
    }
    if (!rpassword) {
      alert("确认不能为空！");
      return;
    }

    if (password !== rpassword) {
      alert("两次密码不一致！");
      return;
    }
    $.ajax({
      url : "${base}/center/member/meminfo/newpwd.do",
      data : {
        pwd : password,
        rpwd : rpassword
      },
      success : function(result) {
        alert("修改成功！");
        //window.location.href = "${base}";
        top.window.location = "${base}/";
      }
    });
  }
</script>
 		
 		
  </div><!--MAContent下-->
  
  <div id="MAContentBottom"></div>
  <div id="MAFoot">${copyright }</div>
</div><!--MACenter-->
</div>
</body>
</html>
