<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
   	 
     	 <div id="main_wrap">
	     <!-- 内容区菜单 -->
	     <div class="mnav">
			<a  href="${base}/center/member/meminfo/page.do">用户信息</a>
			<a  href="#" class="current">个人资料</a>
		</div><!--菜单mnav-->
		  <!-- 内容区 -->
		  <div id="_MMainData" class="member-data">
        	<div class="imformation">
            	<div class="info-img"></div>
            	<div class="info-content">
                	<p class="info-title">
                      <span class="info-title-acname">${loginMember.account}</span>&nbsp;欢迎进入个人资料
               		</p>
                    <p class="info-txt">帐户：${loginMember.account} <c:if test="${empty userInfo1.cardNo or empty userInfo1.userName }"><a style="color: red;" href="${base}/center/banktrans/draw/drawpg.do">完善个人资料</a></c:if></p>
                    <p class="info-txt">币别：<img align="absmiddle" alt="人民币" title="人民币" src="${base }/common/template/member/center/images/money_RMB.gif">&nbsp;人民币(RMB)&nbsp;</p>
                    <c:if test="${accountLevelSwitch == 'on' }"><p class="info-txt">等级：${loginMember.levelName}</p></c:if>
                    <div class="info-sec-title">
                      <div title="即时帐号资讯" class="info-sec-txt"> 即时帐号资讯</div>
               	    </div><!--info-sec-title即时帐号资讯<-->
                </div><!--info-content欢迎进入基本信息-->
            </div><!--imformation基本信息 top-->
            
            <div class="memdata-content">
            	<div class="info-table-area">
               		<table border="1" style="table-layout:fixed" class="M-info">
       					<tbody>
                        	<tr>
                                <th nowrap="">姓名</th>
                                <th nowrap="">QQ</th>
                                <th nowrap="">邮箱</th>
                                <th nowrap="">银行账号</th>
                                <th nowrap="">取现银行</th>
                                <th nowrap="">银行地址</th>
                             </tr>
       						 <tr>
       						 	<td class="MNumber">
       						 	${fn:replace(userInfo1.userName, fn:substring(userInfo1.userName, 0, fn:length(userInfo1.userName)-1), "*")}</td>
                                <td class="MNumber">${userInfo1.qq}</td>
                                <td class="MNumber">${userInfo1.email}</td>
                                <td class="MNumber">
                                <c:choose>
                                	<c:when test="${fn:length(userInfo1.cardNo) <8 }">${userInfo1.cardNo }</c:when>
                                	<c:otherwise>${fn:substring(userInfo1.cardNo, 0,4)}****${fn:substring(userInfo1.cardNo, fn:length(userInfo1.cardNo)-4,fn:length(userInfo1.cardNo))}</c:otherwise>
                                </c:choose>
                                </td>
                                <td class="MNumber">${userInfo1.bankName}</td>
                                <td class="MNumber">${userInfo1.bankAddress}</td>
                          	</tr>
   						 </tbody>
                     </table>
                	</div><!--info-table-area余额-->
        		</div><!--_MMainData 基本信息-->
     		</div>
 		</div>
 		
 		
  </div><!--MAContent下-->
  
  <div id="MAContentBottom"></div>
  <div id="MAFoot">${copyright }</div>
</div><!--MACenter-->
</div>
</body>
</html>
