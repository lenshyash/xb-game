<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
	     <!-- 内容区菜单 -->
	     <div class="mnav">
        	<a href="${base}/center/record/betrecord/page.do">投注记录</a>
            <div class="navSeparate"></div>
            <a href="#" class="current">往来记录</a>
            <c:if test="${not empty isChangeMoney && isChangeMoney eq 'on'}">
            <a id="historyrecord" href="${base}/center/record/mnyrecord/page.do" title="账变记录">账变记录</a>
            </c:if>
         </div><!--菜单mnav-->
         
		  <!-- 内容区 -->
			<div id="MNavLv2">
			<a href="javascript:void(0);" class="current" onclick="javascript:initRdsData(1);">充值记录</a>
				|<a href="javascript:void(0);" onclick="javascript:initRdsData(2);">取款记录</a>
				<c:if test="${isZrOnOff eq 'on' }">
				|<a href="javascript:void(0);" onclick="javascript:initRdsData(3);">真人转款记录</a>
				</c:if>
			</div>
			
			<div  class="yhjy-tzjl">
			<div class="ml20">
				<select id="type" name="type" class="mycss" style="height: 30px;cursor:pointer;">
					<option value="" selected="selected">所有类型</option>
					<option value="5">在线存款</option>
					<option value="6">快速入款</option>
					<option value="7">一般入款</option>
					<option value="801">虚拟货币</option>
				</select> <select id="state" name="state" class="mycss" style="height: 30px;cursor:pointer;">
					<option value="" selected="selected">所有状态</option>
					<option value="1">处理中</option>
					<option value="2">处理成功</option>
					<option value="3">处理失败</option>
				</select> 
				
				<select id="status" name="status" class="mycss" style="height: 30px;cursor:pointer;">
					<option value="" selected="selected">所有状态</option>
					<option value="1">处理成功</option>
					<option value="2">处理失败</option>
				</select>
				<select id="gameType" name="gameType" class="mycss" style="height: 30px;cursor:pointer;">
					<option value="" selected="selected">三方类型</option>
					<option value="1">AG</option>
					<option value="2">BBIN</option>
					<option value="2">KY</option>
					<option value="3">MG</option>
				</select>
				<input id="startTime" class="laydate-icon mycss" value="${startTime }" style="font-family: Times New Roman;height: 30px;width: 110px;cursor:pointer;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <font class="mycss">至 </font> <input
					id="endTime" class="laydate-icon mycss" value="${endTime }" style="font-family: Times New Roman;height: 30px;width: 110px;cursor:pointer;margin-left: 3px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <input value="查 询"
					class="btn search mycss" style="height: 30px;cursor:pointer;" onclick="search();" type="button">
			</div>
			<div id="record_tb" class="yhjy-edzh" style="height:500px;overflow:auto"></div>
			<jsp:include page="/common/include/page.jsp"></jsp:include>
        	
          </div><!--_MMainData 基本信息-->
 		</div>
 		
 		
  </div><!--MAContent下-->
  
  <div id="MAContentBottom"></div>
  <div id="MAFoot">${copyright }</div>
</div><!--MACenter-->
</div>
</body>
</html>
<script id="record_tpl" type="text/html">
		<table class="tab yhjy-MMain" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						{{each colmuns as col}}
							<th>{{col}}</th>
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "支付名称", "交易额度", "状态" ],
		2 : [ "交易额度", "状态", "说明" ],
		3 : [ "游戏类型", "转账单号","转账金额","转账状态" ]
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "payName", "money", "status" ],
		2 : [ "createDatetime", "type", "drawMoney", "status", "remark" ],
		3 : [ "createDatetime", "transType", "gameType","transId", "transMoney","transStatus"]
	}
	var navUrls = {
		1 : "${base}/center/record/hisrecord/comrd.do",
		2 : "${base}/center/record/hisrecord/drawrd.do",
		3 : "${base}/center/record/hisrecord/realtslog.do"
	}
	var curNavFlag = 1;
	$(function() {
		initRdsData(curNavFlag);
		first = false;
	})

	function search() {
		initRdsData(curNavFlag);
	}

	template.helper('$formatter', function(content, key) {

		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		} else if (key == "type") {
			return GlobalTypeUtil.getTypeName(1, 1, content);
		} else if (key == "status") {
			if (curNavFlag == 1) {
				return GlobalTypeUtil.getTypeName(1, 2, content);
			} else if (curNavFlag == 2) {
				return GlobalTypeUtil.getTypeName(1, 3, content);
			}
		}
		
		if(curNavFlag==3){
			if(key=='transStatus'){
				if(content==1|| content == '1'){
					return "成功";
				}else if(content==2|| content == '2'){
					return "失败";
				}else{
					return "待处理";
				}
			}else if(key=='gameType'){
				if (content == 1) {
					return 'AG';
				} else if (content == 2) {
					return 'BBIN';
				} else if (content == 3) {
					return 'MG';
				} else if (content == 12) {
					return 'KY';
				}
			}else if(key=='transType'){
				if (content == 1) {
					return '系统->三方';
				} else{
					return '三方->系统';
				} 
			}
		}
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		var a = $(".category").find("a");
		a.removeClass("current");
		if(flag==1){
			a.eq(0).addClass("current");
		}else if(flag==2){
			a.eq(1).addClass("current");
		}else if(flag==3){
			a.eq(2).addClass("current");
		}
		var type = 0;
		var status = $("#state").val();
		if (flag == 1) {
			$("#type").removeClass("hidn");
			type = $("#type").val();
		} else {
			$("#type").addClass("hidn");
			$("#type").val("");
		}
		if(flag==3){
			$("#type").addClass("hidn");
			$("#state").addClass("hidn");
			$("#gameType").removeClass("hidn");
			$("#status").removeClass("hidn");
			type = $("#gameType").val();
			status = $("#status").val();
		}else{
			$("#gameType").addClass("hidn");
			$("#status").addClass("hidn");
			$("#state").removeClass("hidn");
		}
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}
		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : type,
				status:status
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					search();
				});
			}
		});
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}
</script>