<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<script src="${base}/common/js/layer/layer.js"></script>
<link href="${base}/common/js/layer/skin/layer.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
	     <!-- 内容区菜单 -->
	     <div class="mnav">
        	<a title="投注记录" href="#" class="current">投注记录</a>
            <div class="navSeparate"></div>
            <a id="historyrecord" href="${base}/center/record/hisrecord/page.do" title="往来记录">往来记录</a>
         </div><!--菜单mnav-->
         
		  <!-- 内容区 -->
			<div id="MNavLv2">
			<c:if test="${isTyOnOff eq 'on' }">
				<a class="current MGameType" cur_id="1" id="category_footballrecord" href="#" onclick="javascript:initRdsData(1);">体育记录</a>|
			</c:if>
			<c:if test="${isLhcOnOff eq 'on' }">
				<a id="category_lotteryrecord" class="MGameType" cur_id="3" href="#" onclick="javascript:initRdsData(3);">六合记录</a>|
			</c:if>
			<c:if test="${isZrOnOff eq 'on' }">
				<a id="category_realrecord" cur_id="4" class="MGameType" href="#" onclick="javascript:initRdsData(4);">真人记录</a>|
			</c:if>
			<c:if test="${isDzOnOff eq 'on' }">
				<a id="category_egamerecord" cur_id="5" class="MGameType" href="#" onclick="javascript:initRdsData(5);">电子记录</a>|
			</c:if>
			<c:if test="${isCpOnOff eq 'on' }">
				<a id="category_lotteryrecord" cur_id="2" class="MGameType" href="#" onclick="javascript:initRdsData(2);">彩票记录</a>
			</c:if>
			</div>
			
			<div  class="yhjy-tzjl">
			<div class="ml20">
				<select id="type" name="type" class="mycss" style="height: 30px;cursor:pointer;">
				</select> <input id="startTime" class="laydate-icon mycss" value="${startTime }" style="font-family: Times New Roman;height: 30px;cursor:pointer;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <font class="mycss">至 </font><input
					id="endTime" class="laydate-icon mycss" value="${endTime }" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" style="font-family: Times New Roman;height: 30px;cursor:pointer;margin-left: 3px;" onclick="this.focus();"> <input value="查询"
					type="button" class="mycss" style="height: 30px;cursor:pointer;" onclick="javascript:search();">
			</div>
			<div id="record_tb" class="yhjy-edzh" style="height:500px;overflow:auto"></div>
			<jsp:include page="/common/include/page.jsp"></jsp:include>
        	
          </div><!--_MMainData 基本信息-->
 		</div>
 		
 		
  </div><!--MAContent下-->
  
  <div id="MAContentBottom"></div>
  <div id="MAFoot">${copyright }</div>
</div><!--MACenter-->
</div>
</body>
</html>
<script id="record_tpl" type="text/html">
		<table class="tab yhjy-MMain" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						{{each colmuns as col}}
							<th>{{col}}</th>
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row}}
					<tr class="text-c" style="text-align:center">
						{{each navKeys as key}}
							<td>{{$formatter row key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var combo = {
		1 : [ {
			"value" : 0,
			"name" : "所有体育"
		}, {
			"value" : 1,
			"name" : "足球"
		}, {
			"value" : 2,
			"name" : "篮球"
		} ],
		2 : [],
		3 : [ {
			"value" : "LHC",
			"name" : "六合彩"
		} ],
		4 : [  {
					"value" : "0",
					"name" : "所有真人"
				},{
					"value" : "1",
					"name" : "AG记录"
				},
				{
					"value" : "2",
					"name" : "BBIN记录"
				},{
					"value" : "3",
					"name" : "MG记录"
				}
			],
		5 : [ {
			"value" : "",
			"name" : "所有电子"
		},{
			"value" : "4",
			"name" : "mg电子游艺"
		},{
			"value" : "5",
			"name" : "ag电子游艺"
		},{
			"value" : "6",
			"name" : "bbin电子游艺"
		},{
			"value" : "7",
			"name" : "AG捕鱼王"
		}]
	}
	var navCols = {
		1 : [ "单号/投注日期", "投注类型", "投注项", "投注额", "提交状态", "结算状态", "派彩金额" ],
		2 : [ "注单号", "下注时间", "彩种", "期号", "玩法", "倍数", "开奖号码", "投注额", "中奖金额",
				"状态" ],
		3 : [ "注单号", "下注时间", "彩种", "期号", "玩法", "投注号码", "投注额", "中奖金额","状态" ],
		4 : [ "三方游戏类型", "注单号", "游戏局号", "投注金额", "输赢金额", "投注内容", "投注时间(北京)"],
		5 : [ "游戏类型", "游戏模块", "游戏类型", "投注金额", "输赢金额", "投注时间(游戏GMT-0)"]
	}
	var navKeys = {
		1 : [ "codeAndDate", "sportType", "remark", "bettingMoney",
				"bettingStatus", "balance", "bettingResult" ],
		2 : [ "orderId", "createTime", "lotName", "qiHao", "playName",
				"multiple", "lotteryHaoMa", "buyMoney", "winMoney", "status" ],
		3 : [ "orderId", "createTime", "lotName", "qiHao", "playName",
				 "haoMa", "buyMoney", "winMoney", "status" ],
		4 : [ "type", "bettingCode", "gameCode", "bettingMoney", "winMoney",
				"thirdExtInfo", "bettingTime"],
		5 : [ "type", "bettingCode", "gameType", "bettingMoney", "winMoney",
				 "bettingTime"]
	}
	var navUrls = {
		1 : "${base}/center/record/betrecord/sportrd.do",
		2 : "${base}/center/record/betrecord/lotteryrd.do",
		3 : "${base}/center/record/betrecord/marksixrd.do",
		4 : "${base}/center/record/betrecord/realrecord.do",
		5 : "${base}/center/record/betrecord/egamerecord.do"
	}
	
	function initCombo() {
		$.ajax({
			url : "${base}/center/record/betrecord/betrdtypes.do",
			success : function(result) {
				combo[2] = result.lotCombo;
			}
		});
	}
	function search() {
		initRdsData(curNavFlag);
	}

	template
			.helper(
					'$formatter',
					function(row, key) {
						var value = row[key];
						if (curNavFlag == 1) {
							if (key == "codeAndDate") {
								var bd = new Date(row.bettingDate);
								return row.bettingCode + "<br/>"
										+ bd.format("MM月dd日,hh:mm:ss");
							} else if (key == "sportType") {
								var st = row.sportType
								var html = "";
								if (st == 1) {
									html += "足球";
								} else if (st == 2) {
									html += "篮球";
								}

								if (row.gameTimeType == 1) {
									html += " - 滚球";
								} else if (row.gameTimeType == 2) {
									html += " - 今日";
								} else if (row.gameTimeType == 3) {
									html += " - 早盘";
								}

								html += "<br/>";
								if (row.mix == 2) {
									return html + "混合过关";
								}
								var ts = row.typeNames;
								if (!ts) {
									return html;
								}
								return html + ts.replace("-", "<br>");

							} else if (key == "remark") {
								if (row.mix != 2) {
									return toBetHtml(JSON.decode(value), row);
								}
								var html = "";
								var arr = JSON.decode(value)
								for (var i = 0; i < arr.length; i++) {
									if (i != 0) {
										html += "<div style='border-bottom:1px #303030 dotted;'></div>";
									}
									html += toBetHtml(arr[i], row);
								}
								return html;
							} else if (key == "bettingStatus") {
								if (value == 1) {
									return "<font color='blue'>待确认</font>";
								}
								if (value == 2) {
									return "已确认";
								}

								if (value == 3) {
									return "<font color='red'>取消</font>";
								}

								if (value == 4) {
									return "<font color='red'>手动取消</font>";
								}
							} else if (key == "balance") {

								if (value == 1) {
									return "<font color='blue'>未结算</font>";
								}
								if (value == 2) {
									return "<font color='green'>已结算</font>";
								}

								if (value == 3) {
									return "<font color='red'>结算失败</font>";
								}

								if (value == 4) {
									return "<font color='red'>比赛腰斩</font>";
								}

								if (value == 5) {
									return "<font color='red'>结算中</font>";
								}
							}
						}else if(curNavFlag==4 || curNavFlag==5) {
							if(key=='type'){
								if (value == 1) {
									return 'ag';
								} else if (value == 2) {
									return 'bbin';
								} else if (value == 3) {
									return 'mg';
								}
							}
							
							if (key == "bettingTime") {
								var bd = new Date(value);
								return bd.format("yyyy-MM-dd hh:mm:ss");
							}
						}else {
							if (key == "orderId") {
								return  "<font style=cursor:pointer color=red lotCode="+row.lotCode+" orderId="+value+" class=orderBtn>" + value + "</font>";
							}else if (key == "createTime") {
								var bd = new Date(value);
								return bd.format("yyyy-MM-dd hh:mm:ss");
							} else if (key == "status") {
								if (value == 1) {
									return '<font color="blue">未开奖</font>';
								} else if (value == 2) {
									return '<font color="green">已中奖</font>';
								} else if (value == 3) {
									return '<font color="red">未中奖</font>';
								} else if (value == 4) {
									return '<font color="orange">撤单</font>';
								} else if (value == 5) {
									return '<font color="green">派奖回滚成功</font>';
								} else if (value == 6) {
									return '<font color="red">回滚异常</font>';
								} else if (value == 7) {
									return '<font color="red">开奖异常</font>';
								}
							}
						}
						return value;
					});

	function search() {
		initRdsData(curNavFlag);
	}
	function initRdsData(flag) {
		var a = $(".category").find("a");
		for(var i=0;i<6;i++){
			if(flag != (i+1)){
				a.eq(i).removeClass("current");
			}else{
				a.eq(i).addClass("current");
			}
		}
		if (curNavFlag != flag) {
			$("#jump_page").val(1);
			$("#jump_page_size").val(10);
		}

		var pageSize = 10;
		var pageNumber = 1;
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
			if (curNavFlag != flag) {
				chgTypeCombo(flag);
			}

		} else {
			chgTypeCombo(flag);
		}
		curNavFlag = flag;

		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : $("#type").val()
			},
			success : function(result) {
				result.list = result.rows;
				result.currentPageNo =pageNumber;
				result.pageSize =pageSize;
				var totalCount = result.total;
				
				result.totalPageCount = Math.ceil(totalCount / pageSize);
				
				result.hasPre = result.currentPageNo >1;
				if(result.hasPre){
					result.prePage = result.currentPageNo-1;
				}else{
					result.prePage = result.currentPageNo;
				}
				
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				var html = template('record_tpl', result);
				$("#record_tb").html(html);
				initPage(result, function() {
					search();
				});
			}
		});
	}

	function chgTypeCombo(flag) {
		var html = "";
		for (var i = 0; i < combo[flag].length; i++) {
			var opt = combo[flag][i];
			html += "<option value='"+opt.value+"'>" + opt.name + "</option>";
		}
		if (combo[flag]) {
			$("#type").html(html);
		}
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}

	function toBetHtml(item, row) {
		var con = item.con;
		if (con.indexOf("vs") == -1) {
			con = '<span class="text-danger">' + con + '</span>';
		}
		var homeFirst = row.homeTeam == item.firstTeam;//主队是否在前
		var scoreStr = "";

		if (row.gameTimeType == 1) {
			if (homeFirst) {
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH + ":"
						+ row.scoreC + ")</b></font>";
			} else {
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC + ":"
						+ row.scoreH + ")</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if (item.half === true && row.mix == 2) {
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}

		var html = item.league + "<br/>" + home + "&nbsp;" + con + "&nbsp;"
				+ guest + scoreStr + "<br/>" + "<font color='red'>"
				+ item.result + "</font>&nbsp;" + "@"
				+ "&nbsp;<font color='red'>" + item.odds + "</font>";
		var balance = row.mix != 2 ? row.balance : item.balance;
		if (balance == 4) {
			html = "<s style='color:red;'>" + html + "</s>"
		} else if (balance == 2 || balance == 5 || balance == 6) {
			var mr = row.mix != 2 ? row.result : item.matchResult;
			if (homeFirst) {
				html = html + "&nbsp;<font color='blue'>(" + mr + ")</font>";
			} else {
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>(" + ss[1] + ":"
						+ ss[0] + ")</font>";
			}
		}
		return html;
	}
	
// 	var curNavFlag = $('.category>a:first').attr('cur_id');
	var curNavFlag = 1;
	var first = true;
	$(function() {
		var curid = $('#MNavLv2>a:first').attr('cur_id');
		if(curid){
			curNavFlag = curid
		}
		initCombo();
		initRdsData(curNavFlag);
		first = false;
	})
	
	$("#record_tb").on("click",".orderBtn", function(){
    	var it = $(this),orderId = it.attr("orderid"),lotCode=it.attr("lotcode");
    	$.ajax({
    		url:'${base}/lotteryBet/getBcLotteryOrderDetail.do',
    		data:{
    			orderId : orderId,
    			lotCode : lotCode,
    			},
    		success:function(r){
    			if(r.winZhuShu == null || r.winZhuShu==''){
    				r.winZhuShu = 0;
    			}
    			if(r.winMoney == null || r.winMoney==''){
    				r.winMoney = 0;
    			}
    			if(r.rollBackMoney == null || r.rollBackMoney==''){
    				r.rollBackMoney = 0;
    			}
    			if(r.lotteryHaoMa == null || r.lotteryHaoMa==''){
    				r.lotteryHaoMa = '- -';
    			}
    			//1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
    			var yingkui = 0;
				if(r.status == 1 ){
					r.status='未开奖'
				}else if(r.status == 2 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='已中奖'
				}else if(r.status == 3 ){
					yingkui =(r.winMoney - r.buyMoney+r.rollBackMoney);
					r.status='未中奖'
				}else if(r.status == 4 ){
					r.status='撤单'
				}else if(r.status == 5 ){
					r.status='回滚成功'
				}else if(r.status == 6 ){
					r.status='回滚异常'
				}else if(r.status == 7 ){
					r.status='开奖异常'
				}
				if(r.lotCode == "LHC"){
					$.ajax({url:'${base}/marksix/resMarkSixOdds.do',data:{markId:r.markSixId},success:function(res){
						if(res.success){
							r.multiple = res.odds;
							alertData(r,lotCode);
						}
					}});
				}else{
					alertData(r,lotCode);
				}
    			
    		}
    	})
    })
    
    function alertData(r,lotCode){
		layer.alert(
				'<table width="100%" border="0" cellspacing="0" cellpadding="0"class="message_table" style="font-size: 16px; color: #000;"><tbody>' +
				'<tr class="bg_black3"><td colspan="4" class="open-title">订单号 ：<span style="color: green">'+r.orderId+'</span></td></tr>'+
				'<tr><th class="bg_black3">帐号：</th><td>'+r.account+'</td><th class="bg_black3">单注金额：</th><td>'+(r.buyMoney/r.buyZhuShu).toFixed(2)+'</td></tr>'+
				'<tr><th class="bg_black3">下注时间：</th><td>'+r.createTime+'</td><th class="bg_black3">投注注数：</th><td>'+r.buyZhuShu+'</td></tr>'+
				'<tr><th class="bg_black3">彩种：</th><td>'+r.lotName+'</td><th class="bg_black3">'+(lotCode=="LHC"?'赔率':'倍数')+'</th><td>'+r.multiple+'</td></tr>'+
				'<tr><th class="bg_black3">期号：</th><td>'+r.qiHao+'</td><th class="bg_black3">投注总额：</th><td>'+r.buyMoney.toFixed(2)+'</td></tr>'+
				'<tr><th class="bg_black3">玩法：</th><td>'+r.playName+'</td><th class="bg_black3">中奖注数：</th><td>'+r.winZhuShu+'</td></tr>'+
				'<tr><th class="bg_black3">开奖号码：</th><td>'+r.lotteryHaoMa+'</td><th class="bg_black3">中奖金额：</th><td>'+r.winMoney.toFixed(2)+'</td></tr>'+
				'<tr><th class="bg_black3">状态：</th><td>'+r.status+'</td><td></td><td></td></tr>'+
				'<tr><td colspan="4">'+
				'<textarea name="textfield" readonly="readonly" style="color: #000; width: 95%; height: 100px; border: 1px solid #ccc; font-size: 16px;">'+r.haoMa+'</textarea>'+
				'</td></tr>'+
				'</tbody></table>'
				,{title:"系统提示",area:['700px','auto'],offset : ['5%' ]});
	}
</script>