<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${_title}</title>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<script src="${base}/common/js/layer/layer.src.js"></script>
</head>
<body>
<div id="MACenter">
  <!--MAHeader头部-->
  <jsp:include page="../head.jsp"></jsp:include>
  
  <div id="MAContent">
  <!--MALeft左边菜单栏-->
   <jsp:include page="../leftdemo.jsp"></jsp:include>
  
  	 <!-- 主内容区 -->
   	 <div class="MAMain tzzx-height">
   	 
	     <!-- 内容区菜单 -->
	     <div class="mnav">
        	<a href="${base}/center/record/betrecord/page.do">投注记录</a>
            <div class="navSeparate"></div>
            <a id="historyrecord" href="${base}/center/record/hisrecord/page.do" title="往来记录">往来记录</a>
            <a href="#" class="current">账变记录</a>
         </div><!--菜单mnav-->
         
		  <!-- 内容区 -->
			<div id="MNavLv2">
			<a href="javascript:void(0);" class="current" onclick="javascript:initRdsData(1);">账变记录</a>
			</div>
			
			<div  class="yhjy-tzjl">
			<div class="ml20">
				订单号:
				<input type="text" id="orderId" class="mycss" style="height: 30px;width: 170px;margin-left: 3px;" />
				<select id="moneyType" class="mycss" style="height: 30px;cursor:pointer;">
					<option value="" selected="selected">选择类型</option>
				</select>
				<input id="startTime" class="laydate-icon mycss" value="${startTime }" style="font-family: Times New Roman;height: 30px;width: 110px;cursor:pointer;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <font class="mycss">至 </font> <input
					id="endTime" class="laydate-icon mycss" value="${endTime }" style="font-family: Times New Roman;height: 30px;width: 110px;cursor:pointer;margin-left: 3px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <input value="查 询"
					class="btn search mycss" style="height: 30px;cursor:pointer;" onclick="search();" type="button">
			</div>
			<div id="record_tb" class="yhjy-edzh" style="height:500px;overflow:auto"></div>
			<jsp:include page="/common/include/page.jsp"></jsp:include>
        	
          </div><!--_MMainData 基本信息-->
 		</div>
 		
 		
  </div><!--MAContent下-->
  
  <div id="MAContentBottom"></div>
  <div id="MAFoot">${copyright }</div>
</div><!--MACenter-->
</div>
</body>
</html>
<script id="record_tpl" type="text/html">
		<table class="tab yhjy-MMain" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						{{each colmuns as col}}
							<th>{{col}}</th>
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row index}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key index row.type}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script id="moneytype_tpl" type="text/html">
	{{each data as option}}
        <option value="{{option.id}}">{{option.name}}</option>
	{{/each}}
</script>
<script type="text/javascript">
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "变动前金额", "变动金额", "变动后余额", "订单号","备注" ],
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "beforeMoney", "money", "afterMoney", "orderId","remark" ],
	}
	var navUrls = {
		1 : "${base}/center/record/mnyrecord/list.do",
	}
	var curNavFlag = 1;
	var moneyType = ${moneyType};
	var curDates = [];
	$(function() {
		initMoneyType();
		initRdsData(curNavFlag);
		first = false;
	})

	function search() {
		initRdsData(curNavFlag);
	}
	
	function initMoneyType(){
		var data = [];
		for ( var key in moneyType) {
			if(10<key && key<15){
				continue;
			}
			if("off" == '${isZrOnOff}' && 14<key && key<16){
				continue;
			}
			if("off" == '${isDzOnOff}' && 14<key && key<16){
				continue;
			}
			if("off" == '${isCpOnOff}' && 129<key && key<134){
				continue;
			}
			if("off" == '${isLhcOnOff}' && 139<key && key<143){
				continue;
			}
			if("off" == '${isTyOnOff}' && 200<key && key<205){
				continue;
			}
			if("off" == '${isExChgOnOff}' && 18<key && key<21){
				continue;
			}
			
			data.push({"id":key,"name":moneyType[key]})
		}
		var eachdata = {
			"data" : data
		};
		var html = template('moneytype_tpl', eachdata);
		$("#moneyType").append(html);
	}

	template.helper('$formatter', function(content, key,index,mType) {
		if(!key || !content){
			return "";
		}
		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		} else if (key == "type") {
			return moneyType[content];
		} else if (key.toUpperCase().indexOf("MONEY") != -1) {
			if(content > 0){
				return '<font color="blue">'+content+'</font>';
			}else{
				return '<font color="red">'+content+'</font>';
			}
		} else if(key == "remark" ){
			if(mType == 9 ){
				if(content && content.indexOf('金额') > -1){
					return content.substr(0,content.indexOf('金额'))+"已反水成功！"
				}
			}else if(mType == 10){
				return '';
			}
		}
		/* else if (key == "orderId") {
			content = "<a href='javascript:void(0);' onclick='mnyrdDetail("+index+");'><font color='blue'>"+content+"</font></a>";
		}  */
		
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		var a = $(".category").find("a");
		a.removeClass("current");
		if(flag==1){
			a.eq(0).addClass("current");
		}else if(flag==2){
			a.eq(1).addClass("current");
		}else if(flag==3){
			a.eq(2).addClass("current");
		}
		var type = $("#moneyType").val();
		var orderId = $("#orderId").val();
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}
		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : type,
				orderId :orderId
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				curDates = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					search();
				});
			}
		});
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}
	
	/* function mnyrdDetail(index){
		var row = curDates[index];
		layer.open({
			type : 1, //page层
			area : [ '550px', '550px' ],
			title : row.orderId,
			shade : 0.6, //遮罩透明度
			scrollbar : false,
			offset : '120px',//距离顶部
			moveType : 0, //拖拽风格，0是默认，1是传统拖动
			shift : 1, //0-6的动画形式，-1不开启
			content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'
					+ row + '</div>'
		});
	} */
</script>