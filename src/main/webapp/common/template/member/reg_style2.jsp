<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/regconf.do"></script>
<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>
<style type="text/css">
/* body { */
/* 	background: #000 !important; */
/* 	background-color: #000 !important; */
/* } */
.JoinMemForm {
	width: 700px;
	margin: 0px auto;
}

#memCash_body tr {
	line-height: 12px;
	height: 20px;
}

#memCash_body label {
	float: left;
	height: 25px;
	line-height: 25px;
	text-align: right;
	width: 135px;
}

#memCash_body .memCash_text {
	display: block;
	padding: 5px 0 0 135px;
	min-height: 26px;
}

input, select {
	font-size: 12px;
}

input[placeholder], [placeholder], *[placeholder] {
	font-size: 12px;
}

input[type="text"], input[type="password"], select {
	display: inline-block;
	height: 20px;
	padding: 2px 6px;
	font-size: 12px;
	line-height: 20px;
	color: #555;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	border: 1px solid #ccc;
	background-color: #fff;
	border: 1px solid #ccc;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-webkit-transition: border linear .2s, box-shadow linear .2s;
	-moz-transition: border linear .2s, box-shadow linear .2s;
	-o-transition: border linear .2s, box-shadow linear .2s;
	transition: border linear .2s, box-shadow linear .2s;
	/*background: #ffffff;
              border: 1px solid #333;
              -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
              border-radius: 5px;
              -webkit-box-shadow: 1px 3px 2px rgba(0, 0, 0, 0.5) inset;
              -moz-box-shadow: 1px 3px 2px rgba(0, 0, 0, 0.5) inset;
              box-shadow: 1px 3px 2px rgba(0, 0, 0, 0.5) inset;
              -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
              -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
              -ms-transition: border linear 0.2s, box-shadow linear 0.2s;
              -o-transition: border linear 0.2s, box-shadow linear 0.2s;
              transition: border linear 0.2s, box-shadow linear 0.2s;
              padding-left: 4px;
              padding-right: 4px;
              padding-top:0 !important;
              padding-bottom:0 !important;*/
}

select {
	padding: 0 6px; height22px;
	line-height: 22px;
}

.btn {
	height: auto;
	line-height: normal;
	display: inline-block;
	padding: 4px 14px;
	margin-bottom: 0;
	font-size: 14px;
	line-height: 20px;
	color: #333;
	text-align: center;
	text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
	vertical-align: middle;
	cursor: pointer;
	background-color: #f5f5f5;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff),
		to(#e6e6e6));
	background-image: -webkit-linear-gradient(top, #fff, #e6e6e6);
	background-image: -o-linear-gradient(top, #fff, #e6e6e6);
	background-image: linear-gradient(to bottom, #fff, #e6e6e6);
	background-image: -moz-linear-gradient(top, #fff, #e6e6e6);
	background-repeat: repeat-x;
	border: 1px solid #bbb;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	border-color: #e6e6e6 #e6e6e6 #bfbfbf;
	border-bottom-color: #a2a2a2;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ffffffff',
		endColorstr='#ffe6e6e6', GradientType=0);
	filter: progid:dximagetransform.microsoft.gradient(enabled=false);
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
}

.btn:active, .btn.active, .btn.disabled, .btn[disabled] {
	color: #333;
	background-color: #e6e6e6;
	*background-color: #d9d9d9;
}

.btn:active, .btn.active {
	background-color: #ccc;
	\9
}

.btn:first-child {
	*margin-left: 0;
}

.btn:hover {
	color: #333;
	text-decoration: none;
	background-color: #e6e6e6;
	*background-color: #d9d9d9;
	background-position: 0 -15px;
	-webkit-transition: background-position .1s linear;
	-moz-transition: background-position .1s linear;
	-o-transition: background-position .1s linear;
	transition: background-position .1s linear;
}

.btn:focus {
	outline: thin dotted #333;
	outline: 5px auto -webkit-focus-ring-color;
	outline-offset: -2px;
}

.btn.active, .btn:active {
	background-color: #e6e6e6;
	background-color: #d9d9d9 \9;
	background-image: none;
	outline: 0;
	-webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
}

.btn.disabled, .btn[disabled] {
	cursor: default;
	background-color: #e6e6e6;
	background-image: none;
	opacity: .65;
	filter: alpha(opacity = 65);
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

.btn-info {
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	background-color: #ab7d23;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#cba13e),
		to(#ab7d23));
	background-image: -webkit-linear-gradient(top, #cba13e, #ab7d23);
	background-image: -o-linear-gradient(top, #cba13e, #ab7d23);
	background-image: linear-gradient(to bottom, #cba13e, #ab7d23);
	background-image: -moz-linear-gradient(top, #cba13e, #ab7d23);
	background-repeat: repeat-x;
	border-color: #2f96b4 #2f96b4 #1f6377;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ffcba13e',
		endColorstr='#ffab7d23', GradientType=0);
	filter: progid:dximagetransform.microsoft.gradient(enabled=false);
}

.btn-info:hover {
	color: #fff;
	background-color: #C69631;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position .1s linear;
	-moz-transition: background-position .1s linear;
	-o-transition: background-position .1s linear;
	transition: background-position .1s linear;
}

.btn-info:active, .btn-info.active {
	background-color: #cba13e;
	\9
}

select.err {
	color: red;
}

#memCash_body #myFORM p {
	height: auto;
	min-height: 26px;
}

#myFORM input[type=text], #myFORM input[type=password], #myFORM select {
	border: 1px solid #666666;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	box-shadow: 0 0 6px #3A3A3A;
	-moz-box-shadow: 0 0 6px #3A3A3A;
	-webkit-box-shadow: 0 0 6px #3A3A3A;
}

#myFORM input[type=text]:hover, #myFORM input[type=password]:hover,
	#myFORM select:hover {
	border: 1px solid #E29EFC;
}

#myFORM input[type=text]:focus, #myFORM input[type=password]:focus,
	#myFORM select:focus {
	border: 1px solid #9900FF;
}

#myFORM #HiddenInput {
	width: 0px;
	height: 0px;
	background: transparent;
	border: 0;
}

#myFORM input[type=text][id=HiddenInput] {
	width: 0;
	height: 0;
	background: transparent;
	border: 0;
}

/* li { */
/* 	display: list-item; */
/* 	text-align: -webkit-match-parent; */
/* 	list-style: disc; */
/* 	margin-left: 25px; */
/* 	line-height: 18px; */
/* } */

/* ul { */
/* 	display: block; */
/* 	list-style-type: disc; */
/* 	-webkit-margin-start: 0px; */
/* 	-webkit-margin-end: 0px; */
/* 	-webkit-padding-start: 10px; */
/* } */

.ui-datepicker-trigger {
	margin-left: 5px;
	cursor: pointer;
}

#memCash_body #ui-datepicker-div a {
	color: #000;
}

.ui-datepicker th {
	word-break: break-all;
	padding: 2px 1px;
}

.ui-datepicker {
	width: 260px;
}

/* us by validation */
.FormError {
	position: absolute;
	top: 300px;
	left: 300px;
	display: block;
	z-index: 500;
}

.FormError .FormErrorC {
	width: 100%;
	background: #ee0101;
	position: relative;
	z-index: 501;
	color: #fff;
	font-family: tahoma;
	font-size: 11px;
	line-height: 14px;
	border: 2px solid #ddd;
	box-shadow: 0 0 6px #000;
	-moz-box-shadow: 0 0 6px #000;
	-webkit-box-shadow: 0 0 6px #000;
	padding: 4px 10px 4px 10px;
	border-radius: 6px;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
}

.FormError .FormErrorA {
	width: 15px;
	margin: -2px 0 0 13px;
	position: relative;
	z-index: 506;
}

.FormError .FormErrorABottom {
	box-shadow: none;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
	margin: 0px 0 0 12px;
	top: 2px;
}

.FormError .FormErrorA div {
	border-left: 2px solid #ddd;
	border-right: 2px solid #ddd;
	box-shadow: 0 2px 3px #444;
	-moz-box-shadow: 0 2px 3px #444;
	-webkit-box-shadow: 0 2px 3px #444;
	font-size: 0px;
	height: 1px;
	background: #ee0101;
	margin: 0 auto;
	line-height: 0;
	font-size: 0;
	display: block;
}

.FormError .FormErrorABottom div {
	box-shadow: none;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
}

/*use by jquery ui dialog*/
.ui-dialog-content {
	padding: 5px 1px;
}

.ui-dialog-title {
	font-size: 14px;
}

#Dialog {
	font-size: 12px;
}

#Dialog p {
	line-height: 16px;
	margin: 10px 0px 10px 10px;
	height: auto;
}

#SendForm input[type=text], #SendForm input[type=password] {
	border: 1px solid #949494;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	box-shadow: 0 0 6px #D3D3D3;
	-moz-box-shadow: 0 0 6px #D3D3D3;
	-webkit-box-shadow: 0 0 6px #D3D3D3;
}

#SendForm input[type=text]:hover, #SendForm input[type=password]:hover {
	border: 1px solid #E29EFC;
}

#SendForm input[type=text]:focus, #SendForm input[type=password]:focus {
	border: 1px solid #9900FF;
}

/*  ADVANCED STYLES */
.top_testresult {
	font-weight: bold;
	font-size: 13px;
	font-family: arail, helvetica, san-serif;
	color: #FFF;
	padding: 0;
	margin: 0 0 2px 0;
}

.top_testresult span {
	padding: 6px;
	margin: 0;
}

.top_shortPass {
	background: #FF0000;
}

.top_badPass {
	background: #000000;
}

.top_goodPass {
	background: #0029AA;
}

.top_strongPass {
	background: #128511;
}

.JM_content {
	padding: 10px;
}

.JM_content b, .JM_content strong {
	font-weight: bolder;
}

#memCash_body .JM_content p {
	height: auto;
	line-height: 16px;
}

.loading_pic {
	display: none;
}

#rmNum {
	color: #999;
	text-align: center;
}

.joinform_submit, .joinform_cancel {
	
}

.memCash_code_wrap {
	padding-bottom: 5px;
}

.memCash_code {
	float: left;
	padding-right: 5px;
	min-width: 133px;
	height: 25px;
	line-height: 25px;
	font-family: verdana;
	font-weight: bold;
}

/*共用*/
input {
	/*padding-left: 6px;
            padding-right: 6px;*/
	
}

.bodyStyle {
	background-color: #FFFFFF;
}

.skinbg {
	background-color: #FBE9AC;
}

#MACenter #MALogo {
	float: left;
	background: url(${base}/images/mlogo.png) top left no-repeat;
	display: inline;
	margin: 12px 0 0 30px;
	width: 170px;
	height: 80px;
}

/*忘記密碼*/
#forgetPwd {
	width: 300px;
	margin: 35px auto;
	font-size: 12px;
	color: #000000;
}

#forgetPwd table {
	background-color: #550700;
}

#forgetPwd table thead, #forgetPwd table tfoot {
	color: #FFFFFF;
	text-align: center;
}

#forgetPwd table tbody {
	background-color: #FFFFFF;
}

#forgetPwd table td.style1 {
	text-align: right;
	text-indent: 1em;
	padding-right: 5px;
	width: 50%;
}

#bottom-text {
	text-align: left;
}

.error {
	color: #FF0000;
}

/*修改密碼、修改帳號共用*/
#memAccTable {
	text-align: center;
}

#memAccTable table {
	background-color: #550700;
	margin: 0 auto;
	text-align: left;
}

#memAccTable table tr td {
	border: 1px solid #550700;
	height: 30px;
}

#memAccTable table thead, #memAccTable table tfoot {
	color: #FFFFFF;
	font-size: 12px;
	padding-left: 59px;
	height: 37px;
	text-align: center;
}

#memAccTable table thead td {
	text-align: left;
	padding-left: 59px;
}

#memAccTable table tbody {
	color: #000000;
	background-color: #FFFFFF;
	font-size: 12px;
	text-align: left;
}

#memAccTable table .tipText {
	width: 50%;
	text-align: right;
	padding-right: 5px;
}

#memAccTable table .tipDwText {
	width: 40%;
	text-align: right;
	padding-right: 5px;
}

#memAccTable table .bottomTip {
	font-size: 11px;
	text-align: center;
	background-color: #C9CACA;
	height: 40px;
}

#memAccTable table .modifyProfileTitle {
	font-weight: normal;
	font-size: 12px;
	text-align: center;
	padding-left: 0;
}

a.chg_passwd_flag {
	color: #FFFFFF;
	text-decoration: none;
}

/*額度轉換、會員資料*/
#mainContent {
	font-size: 12px;
	border: 1px solid #707161;
}

#pageTitle {
	background-color: #616A74;
	color: #FFFFFF;
	padding: 4px 0 0 10px;
}

#tableWrap {
	background-color: #CBD1D4;
	border-width: 0 1px 1px;
	border-color: #707161;
	border-style: solid;
	padding: 1px 2px 2px;
}

.tableStyle01 {
	border: 1px solid #666666;
	margin: 0px auto;
	text-align: center;
	width: 100%;
}

.tableStyle01 th {
	background-color: #999999;
	border: 1px solid #666666;
	color: #000000;
	font-weight: normal;
	text-align: center;
	height: 23px;
	line-height: 23px;
}

.tableStyle01 td {
	background-color: #FFFFFF;
	border: 1px solid #666666;
	padding: 2px;
}

.tableStyle01 tr .gameLimit {
	background-color: #FBE9AC;
	color: #000000;
}

.za_button {
	font-size: 12px;
}

/*下注狀況*/
.tableStyle01 .wagers th {
	background-color: #F1EFEF;
	text-align: center;
}

#pageTitle #pageTitleEx {
	float: right;
	color: #FF0000;
}

#pageTitle #pageTitleEx a {
	color: #CC0000;
	text-decoration: none;
	font-weight: bold;
}

.tableStyle01 .b_rig .amountGold {
	background-color: #CCCCCC;
	text-align: right;
}

.tableStyle01 .b_rig .awinGold {
	background-color: #990000;
	color: #FFFFFF;
	text-align: right;
}

.tableStyle01 .red_border {
	border: 2px solid #FF0000;
}

.tableStyle01 .b_rig_mor {
	background-color: #EDEDED;
	text-align: right;
}

.tableStyle01 .b_rig_mor td {
	background-color: #EDEDED;
}

.tableStyle01 .b_rig {
	background-color: #FFFFFF;
	text-align: right;
}

.b_cen {
	background-color: #FFFFFF;
	text-align: center;
	height: 70px;
}

/*往來紀錄*/
#mainContent p.subject {
	width: 82px;
	height: 18px;
	line-height: 18px;
	text-indent: 5px;
}

.Wdate {
	width: 90px;
}

.BookTable td {
	padding: 1px 0px;
}

#foot {
	text-align: left;
	color: #000000;
	font-size: 12px;
	padding-left: 10px;
}

#foot a {
	color: #CC0000;
	font-weight: bold;
	text-decoration: none;
}

/*線上存取款*/
#bank_body {
	background-color: #0C0606;
	font-family: 'Microsoft YaHei', Sans-serif;
}

#bank_header {
	height: 90px;
	margin: 0px auto;
	width: 800px;
	background: url(${base}/images/rule_LOGObg.jpg) repeat-x;
}

#bank_logo {
	height: 90px;
	background: url(${base}/images/rule_logo.jpg) no-repeat;
}

#bank_content {
	width: 800px;
	margin: 0px auto;
	background-color: #FFFFFF;
}

#bank_footer {
	margin: 0px auto;
	width: 800px;
	height: 30px;
	color: #FFFFFF;
	background-color: #8A1820;
	font-size: 11px;
	line-height: 30px;
	font-weight: normal;
}

#bank_footer font {
	color: #FFFFFF;
}

#pay_system_option a {
	color: #FF0000;
	text-decoration: none;
}

/*會員註冊*/
#joinMember iframe {
	width: 730px;
	height: 1000px;
}

#memCash_body {
	background-color: transparent !important;
	color: #FFFFFF;
	font-size: 12px;
}

#memCash_body .memCash_tit {
	margin: 0px 0px 10px 10px;
}

.JM_content {
	padding-left: 50px;
}

.JM_content ol li {
	list-style-type: decimal;
}

span.join-acc {
	position: absolute;
	left: 775px;
    top: 722px;
    font-size: 13px;
	font-family: microsoft yahei;
	padding: 0px 10px;
	color: #FFFECA;
	background-color: #9B836F;
	border-radius: 5px;
}

span.join-info {
	position: absolute;
	left: 775px;
    top: 970px;
    font-size: 13px;
	font-family: microsoft yahei;
	padding: 0px 10px;
	color: #FFFECA;
	background-color: #9B836F;
	border-radius: 5px;
}

#memCash_body table {
	padding: 20px 10px;
	margin: 10px;
	border-collapse: separate;
	border: 1px solid #9b836f;
}

#memCash_body table td {
	height: 22px;
	line-height: 20px;
}

#memCash_body legend {
	color: #FFFFFF;
	font-size: 12px;
	font-weight: bold;
	text-align: left;
}

#memCash_body .star {
	font-family: verdana, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	font-weight: bold;
	vertical-align: -2px;
}

#memCash_body a {
	text-decoration: none;
	color: #BF0F0F;
}

#memCash_body .memCash_text {
	line-height: 15px;
	height: auto;
}

#myFORM input[type=text], #myFORM input[type=password], #myFORM select {
	border: 1px solid #ABADB3;
	/*fx*/
	-moz-border-radius: 3px;
	/*gc*/
	-webkit-border-radius: 3px;
	border-radius: 3px;
	/*fx*/
	-moz-box-shadow: 0 0 6px #CFD1D8;
	/*gc*/
	-webkit-box-shadow: 0 0 6px #CFD1D8;
	box-shadow: 0 0 6px #CFD1D8;
	background-color: #5a3f2a;
	color: #FFF;
}

#myFORM input[type="text"]:hover, #myFORM input[type="password"]:hover,
	#myFORM select:hover {
	border: 1px solid #CFD1D8;
}

#memCash_body label {
	color: #FED778;
}

/*CL共用頁面*/
body.TPL {
	background-color: #FFF;
}

.Tpl-table01 {
	background-color: #FFFFFF;
	border: 1px solid #666;
	color: #000;
	font-size: 12px;
}

table .Tpl-title01 th {
	background-color: #660000;
	height: 24px;
	text-align: center;
	font-weight: bold;
	color: #FFFFFF;
	/* IE10 */
	background: -ms-linear-gradient(top, #AD0000 0%, #660000 100%);
	/* FX 	*/
	background: -moz-linear-gradient(top, #AD0000 0%, #660000 100%);
	/* OP 	*/
	background: -o-linear-gradient(top, #AD0000 0%, #660000 100%);
	/* GC 	*/
	background: -webkit-linear-gradient(top, #AD0000 0%, #660000 100%);
	/* IE69 */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#AD0000',
		endColorstr='#660000', GradientType=0);
	/* W3C 	*/
	background: linear-gradient(top, #AD0000 0%, #660000 100%);
}

.Tpl-table01 th, .Tpl-table01 td {
	height: 22px;
	border: 1px solid #666;
}

tr.Tpl-trcolor1 {
	background: #FFFFFF;
	/* IE10 */
	background: -ms-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* FX 	*/
	background: -moz-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* OP 	*/
	background: -o-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* GC 	*/
	background: -webkit-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* IE69 */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFF',
		endColorstr='#EEEEEE', GradientType=0);
	/* W3C	*/
	background: linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
}

tr.Tpl-trcolor2 {
	background: #EEEEEE;
}

tr.Tpl-trmouseenter {
	background: #DEF;
}

.txt-err {
	font-size: 12px !important;
}

tr {
	height: 30px;
}

.STYLE1 {
	font-size: 12px;
	font-weight: bold;
}
</style>
<div id="memCash_body" style="width: 700px;">
	<div id="">
		<form name="form1" id="form1" class="JoinMemForm" action="New_User"
			method="POST">
			<!--會員資料-->
			<span class="join-acc">注册帐号</span>
			<table id="memdata" style="width: 641px">
				<tbody>
					<!-- 帳號 -->
					<tr id="js-username">
						<td style="width: 150px;"><label><span class="star">*&nbsp;</span>帐 号：</label></td>
						<td><input type="text" name="accountR" id="accountR" size="15"
							maxlength="11" class="g-ipt">
						</td>
					</tr>
					<tr id="js-password">
						<td></td>
						<td colspan="2">
							<div id="usFixTip" style="padding: 0px; margin: 0px;">
								<div class="txt-info-mouseout">5~11个字符，包括字母、数字，以字母开头，字母或数字结尾</div>
							</div>
						</td>
					</tr>
					<!-- 密碼 -->
					<tr id="js-password">
						<td style="width: 150px;"><label><span class="star">*&nbsp;</span>密 码：</label></td>
						<td><input type="password" name="passwordR" id="passwordR"
							class="g-ipt" size="15" maxlength="12"></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<div id="password1FixTip" style="padding: 0px; margin: 0px;">
								<div class="txt-info-mouseout">6~12个字符，包括字母、数字、特殊符号，区分大小写</div>
							</div>
						</td>

					</tr>
					<!-- 確認密碼 -->
					<tr id="js-passwd">
						<td style="width: 150px;"><label title="确认密码"><span class="star">*&nbsp;</span>确认密码：</label>
						</td>
						<td><input type="password" name="renewpassword" id="renewpassword"
							size="15" maxlength="12" class="g-ipt"></td>
					</tr>
					<tr id="js-passwd">
						<td></td>
						<td>
							<div id="password2FixTip" style="padding: 0px; margin: 0px;">
								<div class="txt-info-mouseout">请再次输入密码</div>
							</div>
						</td>
						<td></td>
					</tr>
					<!-- 驗證碼 -->
					<tr id="js-rmNum" style="position: relative; margin-bottom: 40px;">
						<td style="width: 150px;"><label><span class="star">*&nbsp;</span>验 证 码：</label></td>
						<td style="overflow: hidden;"><input type="text" onfocus="reloadImg()"
							id="verifyCodeR" name="verifyCodeR" class="input_tip input_tip_45" size="6"
							style="float: left;"> <img src="#" alt="验证码" id="rauthnumImg"
							width="52" style="float: left; margin-left: 5px; margin-top: 2px;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<!--次要資料-->
			<span class="join-info">会员资料</span>
			<table id="memdata2" style="margin-top: 40px; width: 640px;">
				<!-- 真實姓名 -->
				<tbody id="reg_tb">
<!-- 					<tr id="js-real_name"> -->
<!-- 						<td style="width: 150px;"><label><span class="star">*&nbsp;</span>真实姓名：</label> -->
<!-- 						</td> -->
<!-- 						<td><input type="text" name="realname" id="realname" value="" -->
<!-- 							size="15" maxlength="30" class="g-ipt" -->
<!-- 							style="color: rgb(153, 153, 153);"></td> -->
<!-- 						<td id="realnameTip" -->
<!-- 							style="width: 280px; margin: 0px; padding: 0px; background: transparent;" -->
<!-- 							class="onShow"></td> -->
<!-- 					</tr> -->
<!-- 					<tr id="js-real_name"> -->
<!-- 						<td></td> -->
<!-- 						<td colspan="2"> -->
<!-- 							<div style="color: #999">必须与您的银行帐户名称相同，否则不能出款!</div> -->
<!-- 						</td> -->
<!-- 					</tr> -->
				</tbody>
			</table>
			<br>
			<!-- 確認/重設 -->
			<div id="memCash-confirm" align="center">
				<input type="button" name="OK2" id="OK2" class="btn btn-info"
					value="确认" onclick="commitB();"> <input type="reset"
					name="CANCEL2" id="CANCEL2" class="btn" value="重设">
			</div>
	<script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
		<tr id="js-real_name">
				{{if tl.type==1}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					<td><input type="text" name="{{tl.key}}" id="{{tl.key}}" size="15" maxlength="30" class="g-ipt"></td>
					<tr id="js-real_name"></tr>
				{{/if}}
				{{if tl.type==2}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					<td><select name="{{tl.key}}" id="{{tl.key}}" size="15" class="g-ipt">
					{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
					{{/if}}
					</select></td>
					<tr id="js-real_name"></tr>
				{{/if}}
				{{if tl.type==3}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					{{if tl.sourceLst}}
					{{each tl.sourceLst as rad i}}
						<td><input class="g-ipt" type="radio" name="{{tl.key}}" id="{{tl.key}}" size="15" value="{{i+1}}">{{rad}}</input></td>
						<tr id="js-real_name"></tr>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<td><input class="g-ipt" type="radio" name="{{tl.key}}" id="{{tl.key}}" size="15" value="{{i+1}}">{{chk}}</input></td>
						<tr id="js-real_name"></tr>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					<td><textarea id="{{tl.key}}" class="g-ipt"></textarea></td>
					<tr id="js-real_name"></tr>
				{{/if}}
				{{if tl.type==6}}
					<td style="width: 150px;"><label><span class="star">*&nbsp;</span>{{tl.name}}：</label></td>
					<td><input type="password" name="{{tl.key}}" id="{{tl.key}}" size="15" maxlength="30" class="g-ipt"></td>
					<tr id="js-real_name"></tr>
				{{/if}}
				{{$addValidateFiled tl}}
				{{if tl.type==1 && tl.key== 'userName'}}
					<tr id="js-real_name">
						<td></td>
						<td colspan="2">
							<div style="color: #999">必须与您的银行帐户名称相同，否则不能出款!</div>
						</td>
					</tr>
				{{/if}}
				{{if tl.type==6}}
					<tr id="js-real_name">
						<td></td>
						<td colspan="2">
							<div style="color: #999">提款认证必须，请务必记住!</div>
						</td>
					</tr>
				{{/if}}
		</tr>
		{{/each}}
	</script>
			<!-- 備註 -->
			<div id="memCash-remark" align="left">
				备注：
				<ul>
					<li>1.标记有&nbsp;<span class="star">*</span>&nbsp;者为必填项目。
					</li>
				</ul>
			</div>
		</form>
	</div>
</div>