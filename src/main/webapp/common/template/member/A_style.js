﻿var base;
$(function() {
	base = $("#base").val();

	reloadImg();
	refreshmoney()
	$('#authnumImg').click(reloadImg);
	$('#rauthnumImg').click(reloadImg);
})

// 最新消息
function zxxx() {
	$.ajax({
		url : base + "/getConfig/getArticle.do",
		data : {
			code : 13
		},
		type : "post",
		dataType : 'json',
		success : function(r) {
			var col = '';
			for (var i = 0; i < r.length; i++) {
				col += r[i].content + "&nbsp;&nbsp;&nbsp;";
			}
			$("#noticeList").html(col);
		}
	});
}

//公告公告页面
function newWinOpen() {
	var features = 'height=600,width=800,top=0, left=0,scrollbars=yes,resizable=yes';
    window.open(base+'/index/news.do','HotNewsHistory', features);
}

// 验证码
function reloadImg() {
	var url = base + "/verifycode.do?timestamp=" + (new Date().getTime());
	$('#authnumImg').attr("src", url);
	$('#rauthnumImg').attr("src", url);
}

// 游客测试账号
/**
 * id = A a站 B b站
 */
function testAccount(id) {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/registerTestGuest.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if (id == 'A') {
					location.href = base + "/lottery/index.do";
				} else {
					location.href = base + "/index.do";
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//iframe游客测试账号
/**
 * id = A a站 B b站
 */
function iframeTestAccount(id) {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/registerTestGuest.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if (id == 'A') {
					top.location.href = base + "/lottery/index.do";
				} else {
					top.location.href = base + "/index.do";
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A登录
function doLoginA() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(!result.success){
					reloadImg();
					alert(result.msg);
				}else{
					location.href = base + '/lottery/index.do';
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A登录
function doLoginAV3() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + '/lotteryV3/index.do';
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A登录
function doLoginAnew() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	var w = window.open();
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				w.location = base + '/lottery/index.do';
				parent.location.href = base + '/index.do';
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B登录
function doLoginB() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				if(!result.success){
					reloadImg();
					alert(result.msg);
				}else{
					location.href = base + "/index.do";
				}
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// b014 在regpage.do页面中弹窗切换登录按钮时会存在同id处理方法
function doLoginBs() {
	var account = $.trim($("#accounts").val());
	var password = $("#passwords").val();
	var verifyCode = $("#verifyCodes").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B登录
function doLoginBNew() {
	var account = $.trim($("#account").val());
	var password = $("#password").val();
	var verifyCode = $("#verifyCode").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}

	var data = {};
	data["password"] = password;
	data["account"] = account;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/login.do",
		type : "post",
		data : data,
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/loginPage.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// A注册
function commitA() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/lottery/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//V3注册
function commitV3A() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/lotteryV3/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B注册
function commitB() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				alert("注册成功")
				location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

//A注册
function iframeCommitA() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/lottery/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// B注册
function iframeCommitB() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/register.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/index.do";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 代理注册
function iframedlcommit() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/agtregister.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				top.location.href = base + "/daili";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 代理注册
function dlcommit() {
	var account = $.trim($("#accountR").val());
	var password = $("#passwordR").val();
	var rpassword = $("#renewpassword").val();
	var verifyCode = $("#verifyCodeR").val();
	if (!account) {
		alert("用户名不能为空！");
		return;
	}
	if (!password) {
		alert("密码不能为空！");
		return;
	}
	if (!rpassword) {
		alert("确认密码不能为空！");
		return;
	}

	if (password !== rpassword) {
		alert("两次密码不一致！");
		return;
	}

	var data = getCommitData();
	data["account"] = account;
	data["password"] = password;
	data["rpassword"] = rpassword;
	data["verifyCode"] = verifyCode;
	$.ajax({
		url : base + "/agtregister.do",
		type : "post",
		data : {
			data : JSON.encode(data)
		},
		success : function(result, textStatus, xhr) {
			var ceipstate = xhr.getResponseHeader("ceipstate")
			if (!ceipstate || ceipstate == 1) {// 正常响应
				location.href = base + "/daili";
			} else if (ceipstate == 2) {// 后台异常
				alert("后台异常，请联系管理员!");
				reloadImg();
			} else if (ceipstate == 3) { // 业务异常
				alert(result.msg);
				reloadImg();
			}
		}
	});
}

// 未登录，请先登录再进行游戏
function wdl() {
	alert("请先登录，再进行游戏！");
}

// 在线客服
function zxkf() {
	var open = window.open();
	$.ajax({
		url : base + "/curstinfo.do",
		type : "post",
		success : function(result) {
			var url = result.olserurl;
			if (url) {
				if (url.indexOf("http://") < 0 && url.indexOf("https://") < 0) {
					url = "http://" + url;
				}
			}
			open.location = url;
		}
	});
}

function refreshmoney() {
	$.ajax({
		url : base + "/curstinfo.do",
		type : "post",
		success : function(result) {
			$("#account_span").html(result.member.account);
			$("#money_span").html(result.member.money);
			$("#userid").val(result.member.account);
		}
	});
}

/** 加入收藏* */
function addFavorite() {
	var url = window.location.href;
	var title = document.title;
	var ua = navigator.userAgent.toLowerCase();
	if (ua.indexOf("360se") > -1) {
		alert("由于360浏览器功能限制，请按 Ctrl+D 手动收藏！");
	} else if (ua.indexOf("msie 8") > -1) {
		window.external.AddToFavoritesBar(url, title); // IE8
	} else if (document.all) {
		try {
			window.external.addFavorite(url, title);
		} catch (e) {
			alert('加入收藏失败，请使用Ctrl+D进行添加，或手动在浏览器里进行设置！');
		}
	} else if (window.sidebar) {
		window.sidebar.addPanel(title, url, "");
	} else {
		alert('加入收藏失败，请使用Ctrl+D进行添加，或手动在浏览器里进行设置！');
	}
}

/**
 * 设为首页 author: adaf Modified by: adaf
 */
function setHomepage() {
	try {
		document.body.style.behavior = 'url(#default#homepage)';
		document.body.setHomePage(window.location.href);
	} catch (e) {
		try {
			netscape.security.PrivilegeManager
					.enablePrivilege('UniversalXPConnect');
		} catch (e1) {
			try { // Firefox
				var prefs = Components.classes['@mozilla.org/preferences-service;1']
						.getService(Components.interfaces.nsIPrefBranch);
				prefs.setCharPref('browser.startup.homepage',
						window.location.href);
			} catch (e2) {
				alert("您好，您的浏览器不支持自动设置页面为首页功能，请您手动在浏览器里设置该页面为首页！");
			}
		}
	}
}

function tabColor(id) {
	var co1 = "rgb(255, 0, 0)";
	var co2 = "rgb(0, 0, 255)";
	var color = $("#" + id).css("color");
	$("#" + id).css("color", co1);
	if (color == co1) {
		$("#" + id).css("color", co2);
	} else {
		$("#" + id).css("color", co1);
	}
}

/**
 * @param type
 *            1,cp 2dz 3,agdz 4,mgdz
 * @param id
 */
function typeLogin(type, id) {
	var username = $("#user").val();
	if (username == "") {
		alert("请先登录再进行游戏");
	} else {
		if (type == 1) {
			window.location.href = base + "/index/games.do?lotCode=" + id;
		} else if (type == 2) {
			window.location.href = base + "/index/egame.do";
		} else if (type == 3) {
			toAG(id);
		} else {
			toMG(id);
		}
	}
}

//时间
//var estObj = {
//	pre0 : function(num) {
//		if (num < 10) {
//			num = '0' + num;
//		}
//		return num;
//	},
//	/* 即時時間顯示 */
//	dispTime : function() {
//		// var nowNew = (estObj.now += 1000),
//		var dateObj = new Date(), p0 = estObj.pre0, Y = dateObj.getFullYear(), Mh = dateObj
//				.getMonth() + 1, D = p0(dateObj.getDate()), H = p0(dateObj
//				.getHours()), M = p0(dateObj.getMinutes()), S = p0(dateObj
//				.getSeconds());
//
//		if (Mh > 12) {
//			Mh = 01;
//		} else if (Mh < 10) {
//			Mh = '0' + Mh;
//		}
//
//		document.getElementById('vlock').innerHTML = '美東時間：' + Y + '年' + Mh
//				+ '月' + D + '日 ' + H + ':' + M + ':' + S;
//	}
//};
//(function() {
//	setInterval(estObj.dispTime, 1000);
//}());


//var i,x,y,z,crand,xrand;
//$(document).ready(function() {
//  	//x上限，y下限
//  	i = 8353221;
//    x = 10000.99;
//    y = 100.00;
//    z = 99;	//小数
//    c = 00;	//小数
//    $("#caijin").html(i);
//	setInterval("caijin()", 3000);
//})
//function caijin(){
//	rand = parseInt(Math.random() * (x - y + 1) + y);
//	xrand = parseInt(Math.random() * (z - c + 1) + c);
//	i = rand+i;
//	var caijin = i+'.'+ xrand;
//   $("#caijin").html(caijin);
//}

// 回车按键
//document.onkeydown = function (event) {
//    var e = event || window.event || arguments.callee.caller.arguments[0];
//    if (e && e.keyCode == 13) { 
//    	doLoginB();
//    }
//};

//$(window).resize();
//$("#hongbao").animate({
//    left:$(window).width()*0.183
//},150)
//$(window).resize(function(){
//    $("#hongbao").stop(true,true).animate({
//        left:$(window).width()*0.183
//    },150)
//});

//$(".activity-box-header").click(function () {
//    $(this).parents(".activity-box").siblings().find(".activity-box-body").hide();
//    $(this).next(".activity-box-body").toggle();
//})
