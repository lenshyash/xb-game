<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${base}/common/css/core/bank/css/bank.css?v=1.3" rel="stylesheet" type="text/css">
<link href="${base}/common/template/member/center/css/yueact.css?=20160421" rel="stylesheet" type="text/css">
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script src="${base}/common/template/member/center/js/top.js?=20160422" type="text/javascript"></script>
<script src="${base}/common/template/member/center/js/layer.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script src="${base}/common/js/contants.js?v=2"></script>
<script src="${base}/common/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script language="JavaScript">
var estObj = {
  now: ((new Date().getTime())-12*3600000) || 0,
  pre0: function(num){
    if (num < 10) {num = '0' + num;}
    return num;
  },
  /* 即時時間顯示 */
  dispTime: function() {
//     var nowNew = (estObj.now += 1000),
      var dateObj = new Date(),
      p0 = estObj.pre0,
      Y  = dateObj.getFullYear(),
      Mh = dateObj.getMonth() + 1,
      D  = p0(dateObj.getDate()),
      H  = p0(dateObj.getHours()),
      M  = p0(dateObj.getMinutes()),
      S  = p0(dateObj.getSeconds());
    
	
    if(Mh > 12) {Mh = 01;}
    else if(Mh < 10) {Mh = '0'+Mh;}

    document.getElementById('EST_reciprocal').innerHTML = Y+'/'+Mh+'/'+D+' '+H+':'+M+':'+S;
  }
};
(function() {setInterval(estObj.dispTime, 1000);}() );
</script>

