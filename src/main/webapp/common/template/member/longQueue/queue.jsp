<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css"  href="${base}/common/template/member/longQueue/queue.css"/>
<div class="qq-client">
    <a href="javascript:void(0);" class="qq-client-open">两<br/>面<br/>长<br/>龙</a>
</div>
<div class="qq-client-content">
    <h1>两面长龙<span class="qq-client-close">关闭</span></h1>
    <div class="qq-client-list">
    	
    </div>
</div>
<script type="text/javascript">
	$(function(){
	    H_qqServer={};
	    H_qqServer.clickOpenServer = function () {
	        $('.qq-client-open').click(function(){
	        	var divH = $(".qq-client-content").height();
	            $('.qq-client').animate({
	                left: '-50'
	            },400);
	            $('.qq-client-content').animate({
	                left: '0',
	                marginTop:-divH/2,
	                opacity: 'show'
	            }, 800 );
	        });
	        $('.qq-client-close').click(function(){
	            $('.qq-client').animate({
	            	left: '0',
	                opacity: 'show'
	            },400);
	            $('.qq-client-content').animate({
	            	left: '-250',
	                opacity: 'show'
	            }, 800 );
	        });
	    };
	    H_qqServer.run= function () {
	        this.clickOpenServer();
	    };
	    H_qqServer.run();
	});
</script>