<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<link href="${base}/common/template/member/center/css/userCenter.css" rel="stylesheet" type="text/css">
<div class="uc_headerbox">
	<span>方式B：快速存款</span>
</div>
<div class="uc_linebg"></div>
<div id="uc_bank_content">
	<div id="depositTabs"></div>
</div>
<div class="uc_footerbox">${copyright }</div>
<script id="inputdata_tpl" type="text/html">
		<form id="onlineQuickPayFormId" method="post" action="">
			<div class="step">
				<span class="bo current">1.选择要存入的方式，填入您的帐号资料</span><i></i><span class="bo">2.完成</span>
			</div>
			<div class="br_line"></div>
			<table class="tables" border="0" cellpadding="0" cellspacing="0" width="100%">
				<colgroup>
					<col width="15%">
					<col width="165">
					<col width="85%">
				</colgroup>
				<tbody>
					<tr>
						<td colspan="3" class="radio_va positionre" xx_xx="uc_banks_list" id="uc_fast_list">
							{{each fasts as fast}}
								<label class="pointer"><input name="payId" class="required" min="{{fast.min}}" max="{{fast.max}}" value="{{fast.id}}" type="radio"><span
									title="{{fast.payName}}" class="icon {{fast.iconCss}}">{{fast.payName}}</span></label>
							{{/each}}
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="border_dot">

								<div>自动充值使用须知：</div>
								<div>
									客服上班时间为：<span class="c_blue">{{start}}</span>至<span class="c_blue">{{end}}</span> <br>选择充值方式，填写充值金额及其账户信息，扫描二维码充值后，<span class="c_blue">请手动刷新您的余额</span>及查看相关帐变信息，若超过五分钟未上分，请立即客服联系
								</div>
							</div>
						</td>
					</tr>
					<tr class="oot hidn">
						<th>充值金额：</th>
						<td><input name="amount" class="w155 required" type="text"></td>
						<td>（单笔充值限额：最低&nbsp;<span class="c_blue" id="min_amount">10.00</span>&nbsp;元）
						</td>
					</tr>
					<tr class="oot hidn">
						<th nowrap="nowrap">充值金额（大写）：</th>
						<td class="c_blue" colspan="2"><span id="amount_CH">零</span>元整</td>
					</tr>
					<tr class="oot hidn">
						<th>存款人姓名：</th>
						<td colspan="2"><input class="w155 required" name="depositor" type="text"></td>
					</tr>
					<tr class="oot hidn">
						<th>微信或支付宝账号：</th>
						<td><input class="w155 required number" minlength="5" maxlength="20" name="bank_cards" type="text"></td>
						<td><div class="note_w">请填写正确的微信号或支付宝账号，否则无法到帐（如遇充值成功后仍未及时到账请及时联系客服）</div></td>
					</tr>
				</tbody>
			</table>
			<div class="br_line"></div>
			<div class="btn_submit">
				<button class="btn" id="nextBtn" type="button">下一步</button>
			</div>
		</form>
</script>
<script id="confirm_tpl" type="text/html">
		<div class="step">
			<span class="bo">1.选择要存入的方式，填入您的帐号资料</span><i></i><span class="bo current">2.完成</span>
		</div>
		<div class="br_line"></div>
		<table class="table_online" border="0" cellpadding="0" cellspacing="0" width="645">
			<colgroup>
				<col width="140">
				<col width="265">
				<col width="240">
			</colgroup>
			<tbody>
				<tr>
					<th>充值方式：</th>
					<td><span class="radio_va" id="bPayWay">{{payCom}}</span></td>
					<td></td>
				</tr>
				<tr>
					<th>收款姓名：</th>
					<td xx_xx="Bname" id="Bname_info">{{payUserName}}</td>
					<td class="positionre"><span class="bo" id="Bname"><i class="fa fa-files-o"></i> 复 制</span>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_1" class="zclip">
						</div></td>
				</tr>
				<tr>
					<th>收款帐号：</th>
					<td xx_xx="Baccount" id="Baccount_info">{{payAccount}}</td>
					<td class="positionre"><span class="bo" id="Baccount"><i class="fa fa-files-o"></i> 复 制</span>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_2" class="zclip">
						</div></td>
				</tr>
				<tr>
					<th>订单号：</th>
					<td xx_xx="Boid" id="Boid_info">{{orderNo}}</td>
					<td class="positionre"><span class="bo" id="Boid"><i class="fa fa-files-o"></i> 复 制</span>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_4" class="zclip">
						</div></td>
				</tr>
				<tr>
					<th>充值金额：</th>
					<td><span class="c_blue" xx_xx="Bamount" id="Bamount_info">{{money}}</span></td>
					<td class="positionre"><span class="bo" id="Bamount"><i class="fa fa-files-o"></i> 复 制</span>
						<div style="position: absolute; left: 10px; top: 15px; width: 80px; height: 36px; z-index: 99;" id="zclip-ZeroClipboardMovie_3" class="zclip">
						</div></td>
				</tr>
				<tr>
					<th>二维码：</th>
					<td id="BQR_code"><img src="{{qrCodeImg}}" style="width: 300px; height: 300px;"></td>
					<td class="positionre"></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td colspan="2"><span class="note c_red" style="font-size: 15px"><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;<snap xx_xx="Note">务必将此订单编号填写到附言里，否则无法入款成功。</snap></span></td>
				</tr>
			</tbody>
		</table>
		<div class="note_w">
			<span><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;贴心提醒：收款帐号、收款姓名和二维码会不定期更换，<span class="c_blue">请在获取页面最新信息后在进行充值</span>，以避免充值无法到帐。<br>"充值金额"若与转帐金额不符，充值将无法准确到帐。</span>
		</div>
		<p>&nbsp;</p>
		<div class="br_line"></div>
		<div class="btn_submit" style="height: 68px; line-height: 68px; text-align: center;">
			<button class="btn" onclick="window.close()" type="button">关闭</button>
		</div>
</script>
<script src="${base}/common/js/pasteUtil/jquery.zclip.min.js"></script>
<script>
	$(function() {
		initDptb();
	});
	var pays = [];
	function initDptb() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptinitb.do",
			success : function(result) {
				var html = template('inputdata_tpl', result);
				$("#depositTabs").html(html);
				bindIptData();
				pays = result.fasts;
			}
		});
	}
	
	function bindIptData(){
		var convertCurrency=function(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    };
	    var $from = $("#onlineQuickPayFormId");
	    $from.delegate( "input[name='amount']", 'keyup', function(){
	        /* 取出输入的金额,并转换成中文 */
	        $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
	    $from.on('click', "label", function(e) {
	        var $radio = $(this).find("input");
	        if (!$radio.prop('checked')) {
	            $radio.trigger('change');
	        }
	    }).on('change', ':radio', function() {
	        var $it = $(this);$it.prop('checked', true);
	        $("#min_amount").html($it.attr("min"));
	        $("tr.hidn").removeClass("hidn");
	    });
	    
	    $('#nextBtn').click(function() {
	        var $it=$(this),eleForm = $(this.form),paySetting = eleForm.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=eleForm.find("input[name='amount']"),
	        amount1 =$input.val(),
	        depositor = eleForm.find("input[name='depositor']").val(),
	        bank_cards = eleForm.find("input[name='bank_cards']").val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	alertfocue("请选择支付方式",$it);return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	alertfocue("请输入充值正确金额",$input);
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	alertfocue("充值金额必须不小于"+ min,$input);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        if (!depositor ) {
	        	alertfocue("请输入存款人姓名",eleForm.find("input[name='depositor']"));
	            return false;
	        }
	        if (!bank_cards ||bank_cards.length<5) {
	        	alertfocue("请输入存款帐号后五码",eleForm.find("input[name='bank_cards']"));
	            return false;
	        }
	        dptcommit(amount1,depositor,bank_cards,payId);
	        return false;
		});
	}

	function alertfocue(msg, ele) {
		alert(msg);
		ele.focus();
		$('#nextBtn').removeAttr("disabled");
	}

	function dptcommit(m, ur, bc, payId) {
		$.ajax({
			url : base + "/center/banktrans/deposit/dptcommitb.do",
			data : {
				money : m,
				depositor : ur,
				bankCards : bc,
				payId : payId
			},
			success : function(result) {
				var curPay = {};
				for (var i = 0; i < pays.length; i++) {
					if(pays[i].id == payId){
						curPay = pays[i];
						break;
					}
				}
				curPay.money = m;
				curPay.orderNo = result;
				var html = template('confirm_tpl',curPay);
				$("#depositTabs").html(html);
				bindCopy(curPay);
			}
		});
	}

		function bindCopy(curPay) {
			$("#Bname").zclip({
				path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
				copy : curPay.payUserName,
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Bname')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
			$("#Baccount").zclip({
				path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
				copy : curPay.payAccount,
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Baccount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
			$("#Bamount").zclip({
				path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
				copy : curPay.money+"",
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Bamount')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
			$("#Boid").zclip({
				path : '${base}/common/js/pasteUtil/ZeroClipboard.swf',
				copy : curPay.orderNo+"",
				afterCopy : function() {
					$("<span id='msg'/>").insertAfter($('#Boid')).html(' <i class="fa fa-check"></i> 复制成功').fadeOut(4000);
				}
			});
		}
</script>