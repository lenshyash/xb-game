<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--top 开始-->
<jsp:include page="../transhead.jsp"></jsp:include>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="tit">目前额度</div>
			<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th width="304">帐户</th>
						<th width="426">余额</th>
					</tr>
					<tr>
						<td>系统余额</td>
						<td><span id="mainCredit">0.00</span>&nbsp;&nbsp;RMB<input id="loadmainCredit" value="刷新" onclick='getBalance("sys","mainCredit")' style="margin-left: 15px;" type="button"></td>
					</tr>
					<c:if test="${isMG }">
						<tr id="mmgid">
							<td>Mg余额</td>
							<td><span id="MGCredit">0.00</span>&nbsp;&nbsp;RMB<input id="loadMgCredit" value="刷新" onclick='getBalance("mg","MGCredit")' style="margin-left: 15px;" type="button"></td>
						</tr>
					</c:if>
					<c:if test="${isBBIN}">
						<tr id="mbbinid">
							<td>BBIN余额</td>
							<td><span id="BBINCredit">0.00</span>&nbsp;&nbsp;RMB<input id="loadbbinCredit" value="刷新" onclick='getBalance("bbin","BBINCredit");' style="margin-left: 15px;" type="button"></td>
						</tr>
					</c:if>
					 <c:if test="${isAG}">
						<tr id="maginid">
							<td>AG余额</td>
							<td><span id="AGCredit">0.00</span>&nbsp;&nbsp;RMB<input id="loadAgCredit" value="刷新" onclick='getBalance("ag","AGCredit");' style="margin-left: 15px;" type="button"></td>
						</tr>
					 </c:if>
				</tbody>
			</table>
			
			<c:if test="${isZrOnOff eq 'on' }">
			<div class="tit mt10">额度转换</div>
			<ul class="tabLi mt10">
				<li>转出： <select name="source-transfer" id="source-transfer">
						<option selected="selected" value="">----请选择钱包----</option>
						<option value="sys">系统额度</option>
						<c:if test="${isMG}">
							<option value="mg">MG额度</option>
						</c:if>
						<c:if test="${isBBIN}">
							<option value="bbin">BBIN额度</option>
						</c:if>
						<c:if test="${isAG}">
							<option value="ag">AG额度</option>
						</c:if>
				</select>
				</li>
				<li>转入： <select name="desc-transfer" id="desc-transfer">
						<option selected="selected" value="">----请选择钱包----</option>
						<option value="sys">系统额度</option>
						<c:if test="${isMG}">
							<option value="mg">MG额度</option>
						</c:if>
						<c:if test="${isBBIN}">
							<option value="bbin">BBIN额度</option>
						</c:if>
						<c:if test="${isAG}">
							<option value="ag">AG额度</option>
						</c:if>
				</select>
				</li>
				<li>金额： <input id="amount" name="amount" class="inp" type="text"> <input value="确定" onclick="trans();" id="add" class="btn2" type="button">
				</li>
				<li><span style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></li>
			</ul>
			</c:if>
			
			<input type="hidden" name="v" id="v" value="${v}">
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<script type="text/javascript">
function getBalance(type,id){
	var param = {};
	param["type"]=type;
	$("#"+id).html("<img width='20px' heigth='20px' src='${base }/common/template/third/images/ajax-loader.gif' />");
	$.post('${base}/rc4m/getBalance.do',param,function(json){
		if(json.balance==-1 || json.balance=='-1'){
			$("#"+id).text("获取三方余额失败或者未三方账户还未开通");
		}else{
			$("#"+id).text(json.balance);
		}
	},'json');
}

$(function(){
		getBalance("sys","mainCredit");
	<c:if test="${isMG}">
		getBalance("mg","MGCredit");
	</c:if>
	<c:if test="${isBBIN}">
		 getBalance("bbin","BBINCredit");
	</c:if>
	<c:if test="${isAG}">
		 getBalance("ag","AGCredit");
	</c:if>
});

$(function(){
	$("#source-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			var html = '<option  value="">----请选择钱包----</option><option value="sys">系统额度</option>';
			<c:if test="${isMG}">
			html+='<option value="mg" selected="selected">MG额度</option>';
			</c:if>
			<c:if test="${isBBIN}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAG}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			$("#desc-transfer").html(html);
		}else{
			 var html = '<option  value="">----请选择钱包----</option><option value="sys" selected="selected">系统额度</option>';
			<c:if test="${isMG}">
			html+='<option value="mg" >MG额度</option>';
			</c:if>
			<c:if test="${isBBIN}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAG}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			$("#desc-transfer").html(html);
		}
	});
	
 	$("#desc-transfer").on("change",function(){
		var val = $(this).val();
		if(val=='sys'){//另外的只能为三方的
			var html = '<option value="">----请选择钱包----</option><option value="sys" >系统额度</option>';
			<c:if test="${isMG}">
			html+='<option value="mg" selected="selected">MG额度</option>';
			</c:if>
			<c:if test="${isBBIN}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAG}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			$("#source-transfer").html(html);
		}else{
			 var html = '<option value="">----请选择钱包----</option><option value="sys"  selected="selected">系统额度</option>';
			<c:if test="${isMG}">
			html+='<option value="mg">MG额度</option>';
			</c:if>
			<c:if test="${isBBIN}">
			 html+='<option value="bbin">BBIN额度</option>';
			</c:if>
			<c:if test="${isAG}">
				html+='<option value="ag">AG额度</option>';
			</c:if>
			$("#source-transfer").html(html);
		}
	}); 
});

function trans(){
	var from = $("#source-transfer").val();
	var to = $("#desc-transfer").val();
	var v = $("#v").val();
	console.debug("from"+from);
	console.debug("to"+to);
	if(from==null || from==''){
		alert("转出类型不能为空");
		return;
	}
	if(to==null || to==''){
		alert("转出类型不能为空");
		return;
	}
	var quota = $("#amount").val();
	if(!quota){
		alert("余额不能为空");
		return;
	}
	if(from=='sys'&& !/ag|bbin|mg/.test(to)){
		alert("转入类型错误");
		return;
	}
	if(to=='sys'&& !/ag|bbin|mg/.test(from)){
		alert("转出类型错误");
		return;
	}
	var param = {};
	param["changeFrom"]=from;
	param["changeTo"]=to;
	param["quota"]=quota;
	param["v"]=v;
	$.post('${base}/rc4m/thirdRealTransMoney.do',param,function(json){
		if(json.code==0||json.code=='0'){
			alert("转账成功");
			if(to=='mg'||from=='mg'){
				getBalance("mg","MGCredit");
			}
			if(to=='ag'||from=='ag'){
				 getBalance("ag","AGCredit");
			}
			if(to=='bbin'||from=='bbin'){
				 getBalance("bbin","BBINCredit");
			}
			getBalance("sys","mainCredit");
			$("#v").val(json.v);
		}else{
			alert(json.msg);
			$("#v").val(json.v);
		}
	});
}
</script>
<jsp:include page="../footerdemo.jsp"></jsp:include>