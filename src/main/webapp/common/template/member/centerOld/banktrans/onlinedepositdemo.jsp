<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<jsp:include page="../transhead.jsp"></jsp:include>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box" id="content_div">
			<div class="main_top"></div>
			<div class="txt">
				<div id="online_div" class="hidn">
					<h3>
						<a href="javascript:;" onclick='window.open("${base}/center/banktrans/deposit/dptpga.do", "saveB", "height=650,width=1100,top=150,left=200,scrollbars=yes");' style="text-decoration: underline">方式A：第三方支付(第三方在线支付）</a>
					</h3>
					<p>
					<div id="onlineDesc_div"></div>
					</p>
				</div>
				<div id="fast_div" class="hidn">
					<h3 class="mt10">
						<a href="javascript:;" onclick='window.open("${base}/center/banktrans/deposit/dptpgb.do", "saveB", "height=650,width=1100,top=150,left=200,scrollbars=yes");' style="text-decoration: underline">方式B:快速存款(支持微信、支付宝等扫描快速入款功能)</a>
					</h3>
					<p>
					<div id="fastDesc_div"></div>
					</p>
				</div>
				<div id="bank_div" class="hidn">
					<h3 class="mt10">
						<a href="javascript:;" onclick="window.open('${base}/center/banktrans/deposit/dptpgc.do','save','height=650,width=1100,top=150,left=200,scrollbars=yes');" style="text-decoration: underline">方式C：银行卡转账入款</a>
					</h3>
					<p>
					<div id="bankDesc_div"></div>
					</p>
				</div>
			</div>

		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>
<script>
	$(function() {
		initDptData();
	});
	var pays = [];
	function initDptData() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptdata.do",
			success : function(result) {
				if (result.onlineFlag == 'on') {
					$("#online_div").removeClass("hidn");
					$("#onlineDesc_div").html(result.onlineDesc);
				}
				if (result.fastFlag == 'on') {
					$("#fast_div").removeClass("hidn");
					$("#fastDesc_div").html(result.fastDesc);
				}
				if (result.bankFlag == 'on') {
					$("#bank_div").removeClass("hidn");
					$("#bankDesc_div").html(result.bankDesc);
				}
			}
		});
	}
</script>