<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<jsp:include page="../transhead.jsp"></jsp:include>
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<!--左侧分类 结束-->

	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div id="content_tb"></div>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<jsp:include page="../footerdemo.jsp"></jsp:include>

<script id="meminfo_tpl" type="text/html">
		<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th width="304">帐户</th>
						<th width="426">余额</th>
						<th width="426">取款</th>
					</tr>
					<tr>
						<td>{{account}}</td>
						<td>{{money}}</td>
						<td><a href="javascript:;" onclick="window.open('${base}/center/banktrans/draw/drawpg.do','取款','height=450,width=630,top=200,left=450,scrollbars=yes');" class="yellow red">取款</a></td>
					</tr>
				</tbody>
			</table>
</script>
<script type="text/javascript">
	$(function() {
		initMemdata();
	})

	function initMemdata() {
		$.ajax({
			url : base + "/center/member/meminfo/data.do",
			success : function(result) {
				var html = template('meminfo_tpl', result);
				$("#content_tb").append(html);
			}
		});
	}
</script>
