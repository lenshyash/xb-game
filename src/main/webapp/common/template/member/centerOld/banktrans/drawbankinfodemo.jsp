<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<div class="layer">
	<div class="layer_top"></div>
	<div class="layer_box">
		<p class="tips red">请先添加银行卡，为保护您的资金安全，请确实填写您的出款银行资料，以免有心人士窃取，谢谢！</p>
		<table id="savebank" border="0" cellpadding="3" cellspacing="3" width="100%">

		</table>
		<div class="di2"></div>
	</div>
	<div class="layer_footer">${copyright }</div>
</div>
<script type="text/javascript">
	var checkflag = false;
	function as() {
		if (checkSubmit()) {
			if (!checkflag) {
				checkflag = true;
				submit()
			} else {
				alert("请勿重复提交");
			}
		}
	}

	$(function() {
		initDrawdata();
	})

	function initDrawdata() {
		$.ajax({
			url : base + "/center/banktrans/draw/drawbkinfo.do",
			success : function(result) {
				var html = template('draw_tpl', result);
				$("#savebank").html(html);
			}
		});
	}

	function j() {
		self.opener = null;
		self.close();
	}

	function submit() {
		var cashBankname = $("#bankName").val();
		var sheng = $("#province").val();
		var city = $("#city").val();
		var bankcardaddress = $("#bankcardaddress").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var userName = $("#accountname").val();
		
		var param = {};
		param["bankName"] = cashBankname;
		param["userName"] = userName;
		param["province"] = sheng;
		param["city"] = city;
		param["bankAddress"] = bankcardaddress;
		param["cardNo"] = cashBankaccount;
		param["repPwd"] = cashPassword;
		$.ajax({
			url : "${base}/center/banktrans/draw/cmitbkinfo.do",
			data : param,
			success : function(result) {
				checkflag = false;
				window.location.href="${base}/center/banktrans/draw/drawpg.do";
			}
		});
	}

	function checkSubmit() {
		var cashBankname = $("#bankName").val();
		var cashBankaccount = $("#bankId").val();
		var cashPassword = $("#cashPassword").val();
		var cashProvince = $("#province").val();
		var cashCity = $("#city").val();

		var exp = /^([1-9][\d]{1,18}|x|X)$/;

		if (!cashBankname) {
			alert("开户银行不能为空");
			return false;
		}
		if (!cashProvince){
			alert("省份不能为空！");
			return false;
		}
		if (!cashCity){
			alert("城市不能为空！");
			return  false;
		}
		if (!cashBankaccount) {
			alert("银行账号不能为空");
			return false;
		}
		if (!exp.test(cashBankaccount)) {
			alert("请输入正确的银行账号");
			return false;
		}
		if (!cashPassword) {
			alert("取款密码不能为空");
			return false;
		}
		return true;
	}
</script>
<script id="draw_tpl" type="text/html">
	<tbody>
		<tr>
			<td class="txtright" width="24%">真实姓名：</td>
			<td width="26%"><input class="inp3" id="accountname" {{if userName}}disabled="disabled"{{/if}} value="{{userName}}" type="text"></td>
			<td rowspan="7" width="50%">
				<div class="explain">
					<b>备注:</b><br> 1. 标记有 <span class="red">*</span> 者为必填项目。<br> 2. 手机写取款 密码为取款 金额时的凭证请会员务必填写详细资料。
				</div>
			</td>
		</tr>
		<tr>
			<td class="txtright"><span class="red">*</span> 出款银行：</td>
			<td><select name="bankName" id="bankName">
					<option value="建设银行">建设银行</option>
					<option value="工商银行" selected="selected">工商银行</option>
					<option value="农业银行">农业银行</option>
					<option value="中国邮政银行">中国邮政银行</option>
					<option value="中国银行">中国银行</option>
					<option value="中国招商银行">中国招商银行</option>
					<option value="中国交通银行">中国交通银行</option>
					<option value="中国民生银行">中国民生银行</option>
					<option value="中信银行">中信银行</option>
					<option value="中国兴业银行">中国兴业银行</option>
					<option value="浦发银行">浦发银行</option>
					<option value="平安银行">平安银行</option>
					<option value="华夏银行">华夏银行</option>
					<option value="广州银行">广州银行</option>
					<option value="BEA东亚银行">BEA东亚银行</option>
					<option value="广州农商银行">广州农商银行</option>
					<option value="顺德农商银行">顺德农商银行</option>
					<option value="北京银行">北京银行</option>
					<option value="杭州银行">杭州银行</option>
					<option value="温州银行">温州银行</option>
					<option value="上海农商银行">上海农商银行</option>
					<option value="中国光大银行">中国光大银行</option>
					<option value="渤海银行">渤海银行</option>
					<option value="浙商银行">浙商银行</option>
					<option value="晋商银行">晋商银行</option>
					<option value="汉口银行">汉口银行</option>
					<option value="上海银行">上海银行</option>
					<option value="广发银行">广发银行</option>
					<option value="深圳发展银行">深圳发展银行</option>
					<option value="东莞银行">东莞银行</option>
					<option value="宁波银行">宁波银行</option>
					<option value="南京银行">南京银行</option>
					<option value="北京农商银行">北京农商银行</option>
					<option value="重庆银行">重庆银行</option>
					<option value="广西农村信用社">广西农村信用社</option>
					<option value="吉林银行">吉林银行</option>
					<option value="江苏银行">江苏银行</option>
					<option value="成都银行">成都银行</option>
					<option value="尧都区农村信用联社">尧都区农村信用联社</option>
					<option value="浙江稠州商业银行">浙江稠州商业银行</option>
					<option value="珠海市农村信用合作联社">珠海市农村信用合作联社</option>
			</select></td>
		</tr>
		<tr>
			<td class="txtright">省份：</td>
			<td><input class="inp3" id="province" name="province" type="text"></td>
		</tr>
		<tr>
			<td class="txtright">城市：</td>
			<td><input class="inp3" id="city" name="city" type="text"></td>
		</tr>
		<tr>
			<td class="txtright">开户行网点：</td>
			<td><input class="inp3" id="bankcardaddress" name="bankcardaddress" type="text"></td>
		</tr>
		<tr>
			<td class="txtright"><span class="red">*</span> 银行账号：</td>
			<td><input class="inp3" id="bankId" name="bankId" type="text"></td>
		</tr>
		<tr>
			<td class="txtright"><span class="red">*</span> 取款密码：</td>
			<td><input class="inp3" id="cashPassword" name="cashPassword" type="password"></td>
		</tr>
		<tr>
			<td class="txtright" height="29">&nbsp;</td>
			<td colspan="2"><input onclick="javascript:as();" value="确 定" class="btn-sub" type="button"><input onclick=" javascript: j(); " value="取 消" class="btn-sub" type="button"></td>
		</tr>
	</tbody>
</script>