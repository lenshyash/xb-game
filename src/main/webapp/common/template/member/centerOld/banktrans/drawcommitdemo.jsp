<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<div class="layer">
	<div class="layer_top"></div>
	<div class="layer_box">
		<p class="tips red"></p>
		<div id="draw_tb"></div>
		<div class="di2"></div>
	</div>
	<div class="layer_footer">${copyright }</div>
</div>
<script id="draw_tpl" type="text/html">
	<table id="savebank" border="0" cellpadding="3" cellspacing="3" width="100%">
			<colgroup>
				<col width="25%">
				<col width="75%">
			</colgroup>
			<tbody>
				<tr>
					<td class="txtright">提示信息：</td>
					<td>每天的取款处理时间为：<span style="font-size: 15px; color: #ffb400;"> {{star}} 至 {{end}}</span>;<br>取款10分钟内到账。(如遇高峰期，可能需要延迟到三十分钟内到帐)<br>用户每日最小提现&nbsp;<span
						style="color: #ffb400; font-size: 15px;">{{min}}</span>&nbsp;元，最大提提现&nbsp;<span style="color: #ffb400; font-size: 15px;">{{max}}</span>&nbsp;元<br>今日可取款<span
						style="font-size: 15px; color: #ffb400;"> {{wnum}}</span> 次，已取款<span style="font-size: 15px; color: #ffb400;"> {{curWnum}}</span> 次
					</td>
				</tr>
				<tr>
					<td class="txtright">消费比例：</td>
					<td>出款需达投注量：<span style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{checkBetNum}}</span>有效投注金额：<span style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{member.betNum}}</span><br>是否能取款：<span
						style="font-size: 15px; color: #ffb400; margin-right: 10px;">{{drawFlag}}</span></td>
				</tr>
				<tr>
					<td class="txtright">会员账号：</td>
					<td>{{member.account}}</td>
				</tr>
				<tr>
					<td class="txtright">真实姓名：</td>
					<td>{{member.userName}}</td>
				</tr>
				<tr>
					<td class="txtright">账户余额：</td>
					<td><span class="red">{{member.money}}</span>RMB</td>
				</tr>
				<tr>
					<td class="txtright">银行：</td>
					<td>{{member.bankName}}</td>
				</tr>
				<tr>
					<td class="txtright">开户行：</td>
					<td>{{member.bankAddress}}</td>
				</tr>
				<tr>
					<td class="txtright">银行账号：</td>
					<td>{{member.cardNo}}</td>
				</tr>

				<tr>
					<td class="txtright"><span class="red">*</span>取款金额：</td>
					<td><input class="inp3" id="money" name="money" type="text"></td>
				</tr>
				<tr>
					<td class="txtright"><span class="red">*</span>取款密码：</td>
					<td><input class="inp3" id="userQxpassword" name="userQxpassword" type="password"></td>
				</tr>
				<tr>
					<td class="txtright" height="29">&nbsp;</td>
					<td><input onclick="javascript: drawcommit();" value="确 定" class="btn-sub" type="button" style="cursor: pointer;"> <input onclick=" javascript: j(); " value="取 消" class="btn-sub" type="button" style="cursor: pointer;"></td>
				</tr>
			</tbody>
		</table>
</script>
<script type="text/javascript">
	var min = 0;
	var max = 0;
	var balance = 0;
	$(function() {
		initDrawdata();
	})

	function initDrawdata() {
		$.ajax({
			url : base + "/center/banktrans/draw/drawdata.do",
			success : function(result) {
				min = result.min;
				max = result.max;
				balance = result.member.money;
				var html = template('draw_tpl', result);
				$("#draw_tb").append(html);
			}
		});
	}

	function drawcommit() {
		var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
		var m = $("#money").val();
		if (!exp.test(m)) {
			alert("请输入正确的金额");
			$("#money").focus();
			return;
		}
		m = parseInt(m);
		if (m < min) {
			alert("取款最小金额不能小于" + min);
			$("#money").focus();
			return;
		}
		if (m > balance) {
			alert("余额不足");
			$("#money").focus();
			return;
		}

		if (max != 0 && max < m) {
			alert("取款最大金额不能大于" + max);
			$("#money").focus();
			return;
		}
		var userQxpassword = $("#userQxpassword").val();
		if (userQxpassword == null || userQxpassword == "") {
			alert("取款密码不能为空");
			$("#userQxpassword").focus();
			return;
		}

		$.ajax({
			url : base + "/center/banktrans/draw/drawcommit.do",
			data : {
				money : m,
				repPwd : userQxpassword
			},
			success : function(result) {
				alert("取款信息已提交!");
				j();
			}
		});
	}

	function j() {
		self.opener = null;
		self.close();
	}
</script>