<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<jsp:include page="../transhead.jsp"></jsp:include>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="tit">用户余额</div>
			<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th width="304">帐户</th>
						<th width="426">余额</th>
						<th width="150">操作</th>
					</tr>
					<tr>
						<td>现金</td>
						<td><span id="mnyCredit">0.00</span>&nbsp;&nbsp;RMB</td>
						<td rowspan="2"><input id="loadCredit" value="刷新" onclick='initUserData()' style="margin-left: 15px;" type="button"></td>
					</tr>
					<tr id="mmgid">
						<td>积分</td>
						<td><span id="scoreCredit">0.00</span>&nbsp;&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<div class="tit mt10">兑换</div>
			<ul class="tabLi mt10">
				<li>兑换类型： <select id="exchangeType">
						<option selected="selected" value="">----请选择类型----</option>
						<option value="1">现金兑换积分</option>
						<option value="2">积分兑换现金</option>
				</select> <span id="exchange_remark_span"></span>
				</li>
				<li>兑换额度： <input id="amount" name="amount" class="inp" type="text"> <input value="确定" onclick="exchange();" id="excBtn" class="btn2" type="button"> <span
					id="exchange_result_span"></span>
				</li>
				<li><span style="font-size: 14px; font-weight: bold; color: #c00">系统余额可用于彩票和体育投注</span></li>
				<li><span style="font-size: 14px; font-weight: bold; color: #c00">积分可用于参与站点活动</span></li>
			</ul>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>
<script type="text/javascript" src="${base }/common/template/member/center/js/memexchangedemo.js"></script>