<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<link href="${base}/common/template/member/center/css/userCenter.css" rel="stylesheet" type="text/css">
<div class="uc_headerbox">
	<span>方式A：在线支付</span>
</div>
<div class="uc_linebg"></div>
<div id="uc_bank_content">
	<div id="depositTabs"></div>
</div>
<div class="uc_footerbox">${copyright }</div>
<script id="inputdata_tpl" type="text/html">
		<div class="step"><span class="bo current" id="tabTit1">步骤1:请填写付款人资料</span>
	    		<span class="bo" id="tabTit2">步骤2:请确认您的转帐资料</span>
	    		<span class="bo">步骤3:完成</span></div>
        	<div class="br_line"></div>
			<form id="onlinePayAFormId">
				<table class="tables" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody><tr>
						
						<td colspan="2" style="vertical-align: middle;" height="40" align="left" width="5">
							{{each onlines as online}}
								<label class="pointer"><input name="payId" class="required" min="{{online.min}}" max="{{online.max}}" value="{{online.id}}" type="radio"><span
									title="{{online.payName}}" class="icon {{if !online.icon}}{{online.iconCss}}{{/if}}" {{if online.icon}}style="background-size: 120px 35px; text-indent: -9999px; background-image: url('{{online.icon}}');"{{/if}}>{{online.payName}}</span></label>
							{{/each}}
						</td>
					</tr>
					<tr class="hidn">
						<th>会员帐号：</th>
						<td>{{account}}</td>
					</tr>
					<tr class="hidn">
						<th>存入金额：</th>
						<td><input name="amount" class="w155 required" type="text">（单笔充值限额：最低
                               <span class="c_blue" id="min_amount">100.00</span> 元）</td>
					</tr>
					<tr class="hidn">
						<th>存入金额(大写)：</th>
						<td class="c_blue"><span id="amount_CH">零</span>元整</td>
					</tr>
               </tbody></table>
               <div class="br_line"></div>
	            <div class="btn_submit">
	                <button class="btn" id="nextBtn" type="button">下一步</button>
	            </div>
            </form>
</script>
<script>
	$(function() {
		initDptb();
	});
	var pays = [];
	function initDptb() {
		$.ajax({
			url : "${base}/center/banktrans/deposit/dptinita.do",
			success : function(result) {
				var html = template('inputdata_tpl', result);
				$("#depositTabs").html(html);
				bindIptData();
				pays = result.onlines;
			}
		});
	}
	
	function bindIptData(){
		var convertCurrency=function(a) {
	        var e, g, c, h, l, m, q, t, r, y, w;
	        a = a.toString();
	        if ("" == a || null != a.match(/[^,.\d]/) || null == a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/)) return "";
	        a = a.replace(/,/g, "");
	        a = a.replace(/^0+/, "");
	        if (9.999999999999E10 < Number(a)) return alert("\u60a8\u8f93\u5165\u7684\u91d1\u989d\u592a\u5927\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165!"), "";
	        e = a.split(".");
	        1 < e.length ? (a = e[0], e = e[1], e = e.substr(0, 2)) : (a = e[0], e = "");
	        c = "\u96f6\u58f9\u8d30\u53c1\u8086\u4f0d\u9646\u67d2\u634c\u7396".split("");
	        h = ["", "\u62fe", "\u4f70", "\u4edf"];
	        l = ["", "\u4e07", "\u4ebf"];
	        m = ["", ""];
	        g = "";
	        if (0 < Number(a)) {
	            for (t = q = 0; t < a.length; t++) r = a.length - t - 1, y = a.substr(t, 1), w = r / 4, r %= 4, "0" == y ? q++ : (0 < q && (g += c[0]), q = 0, g += c[Number(y)] + h[r]), 0 == r && 4 > q && (g += l[w]);
	            g += ""
	        }
	        if ("" != e)
	            for (t = 0; t < e.length; t++) y = e.substr(t, 1), "0" != y && (g += c[Number(y)] + m[t]);
	        "" == g && (g = "\u96f6");
	        "" == e && (g += "");
	        return "" + g
	    };
	    var $from = $("#onlinePayAFormId");
	    $from.delegate( "input[name='amount']", 'keyup', function(){
	        /* 取出输入的金额,并转换成中文 */
	        $( "#amount_CH" ).text(convertCurrency( $( this ).val() ) );
	    });
	    $from.on('click', "label", function(e) {
	        var $radio = $("#"+$(this).attr("for"));
	        if (!$radio.prop('checked')) {
	            $radio.trigger('change');
	        }
	    }).on('change', ':radio', function() {
	        var $it = $(this);$it.prop('checked', true);
	        $("#min_amount").html($it.attr("min"));
	        $("tr.hidn").removeClass("hidn");
	    });
	    
	    $('#nextBtn').click(function() {
	        var $it=$(this),eleForm = $(this.form),paySetting = eleForm.find("input[name='payId']:checked"),
	        payId = paySetting.val(),$input=eleForm.find("input[name='amount']"),
	        amount1 =$input.val(),
	        min = paySetting.attr("min"),
	        max = paySetting.attr("max");
	        $it.attr("disabled","disabled");
	        if(!payId){
	        	alertfocue("请选择支付方式",$it);return false;
	        }
	        try{
	            min = parseFloat(min,10);
	        }catch(e){min = 0;}
	        try{
	            max = parseFloat(max,10);
	        }catch(e){max = 0;}
	        if (!amount1 || !/^[0-9]+(\.[0-9]{1,2})?$/.test(amount1)) {
	        	alertfocue("请输入充值正确金额",$input);
	            return false;
	        }
	        amount1 = parseFloat(amount1,10);
	        if(amount1<min){
	        	alertfocue("充值金额必须不小于"+ min,$input);
	            return false;
	        }
	        if(max>0 && amount1 > max){
	        	alertfocue("充值金额必须不大于"+ max,$input);
	            return false;
	        }
	        dptcommit(amount1,payId);
	        return false;
		});
	}

	function alertfocue(msg, ele) {
		alert(msg);
		ele.focus();
		$("#nextBtn").removeAttr("disabled");
	}

	function dptcommit(m, payId) {
		/*
		$.ajax({
			url : base + "/center/banktrans/deposit/dptcommita.do",
			data : {
				money : m,
				payId : payId
			},
			success : function(result) {
				var curPay = {};
				for (var i = 0; i < pays.length; i++) {
					if(pays[i].id == payId){
						curPay = pays[i];
						break;
					}
				}
				curPay.money = m;
				curPay.orderNo = result;
				// var html = template('confirm_tpl',curPay);
				// $("#depositTabs").html(html);
				// bindCopy(curPay);
			}
		});
		*/
		$.ajax({
			type : "POST",
			dataType: "json",
			url : base + "/onlinepay/pay.do",
			data : {
				amount: m,
				payId: payId
			},
			success : function(result) {
				if(result.success == false){
					alert(result.msg);
				} else if(result.success == true){
					if(result && result.data && result.data.formParams){
						var formHiddens = [];
						for(var key in result.data.formParams){
							var value = result.data.formParams[key];
							formHiddens.push({name: key, value: value});
						}
						result.data.formHiddens = formHiddens;
						var html = template('toPayTemplate', result.data);
						$("#depositTabs").html(html);
					}else{
						alert("系统发生错误");
					}
				}
			}
		});
	}
</script>
<script type="text/html" style="display: none;" id="toPayTemplate">
<form action="{{formAction}}" method="post" target='_blank'>
	{{each formHiddens as item}}
	<input type="hidden" name="{{item.name}}" value="{{item.value}}"/>
	{{/each}}

<div class="step">
<span class="btn b0">1.请填写付款人资料</span>
<i></i>
<span class="btn b0 current">2.请确认您的转帐资料</span>
<i></i><span class="btn b0">3.完成</span>
</div>
<div class="br_line"></div>
<table width="450" border="0" cellspacing="0" cellpadding="0" class="tables">
<colgroup><col width="30%"><col width="70%"></colgroup>
<tbody>
<tr><th>订单号：</th><td>{{orderId}}</td></tr>
<tr><th>会员帐号：</th><td>{{account}}</td></tr>
<tr><th>充值金额：</th><td>{{amount}}</td></tr>
</tbody>
</table>
<div class="br_line"></div>
<div class="btn_submit">
<button class="btn" type='submit'>确认送出</button>
</form>
</script>