<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<script src="${base}/common/js/layer/layer.js"></script>
<script src="${base}/common/js/layer/layer.src.js"></script>
<!--top 开始-->
<div class="header clear">
	<div class="nav fr">
		<ul>
			<li class="current"><a href="#">站内信</a></li>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
</div>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="headinfo ml20 mt10">
				<select id="status" name="status">
					<option value="" selected="selected">所有状态</option>
					<option value="1">未读</option>
					<option value="2">已读</option>
				</select>
				<%-- <input id="startTime" class="laydate-icon" value="${startTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> - <input
					id="endTime" class="laydate-icon" value="${endTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> --%>
				<input value="查 询" class="btn search" onclick="searchMsg();" type="button">
			</div>
			<div id="record_tb"></div>

			<jsp:include page="/common/include/page.jsp"></jsp:include>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>
<script id="record_tpl" type="text/html">
		<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th>标题</th>
						<th>状态</th>
						<th>创建时间</th>
						<th>操作</th>
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row i}}
					<tr class="text-c">
						<td><a href="javascript:void(0)" onclick="readMsg({{i}});">{{row.title}}</a></td>
						<td>{{$formatter row.status 'status'}}</td>
						<td>{{$formatter row.createDatetime 'time'}}</td>
						<td> <a href="javascript:void(0)" onclick="readMsg({{i}});">阅读</a> | <a href="javascript:void(0)" onclick="delMsg({{row.userMessageId}});">删除</a></td>
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var pageSize = 10;
	var pageNumber = 1;
	var first = true;
	var pageDatas = [];
	$(function() {
		searchMsg();
		first = false;
	});

	function searchMsg() {

		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}

		$.ajax({
			url : "${base}/center/news/message/list.do",
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				status : $("#status").val()
			},
			success : function(result) {
				pageDatas = result.list;
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					searchMsg();
				});
			}
		});
	}

	template.helper('$formatter', function(content, key) {
		if (key == 'time') {
			return DateUtil.formatDatetime(content);
		} else if (key == 'status') {
			if (content == '1') {
				return '未读';
			} else {
				return '已读';
			}
		}
	});

	function readMsg(index) {
		var row = pageDatas[index];
		$
				.ajax({
					url : "${base}/center/news/message/read.do",
					data : {
						id : row.id
					},
					success : function(result) {
						layer
								.open({
									type : 1, //page层
									area : [ '550px', '550px' ],
									title : row.title,
									shade : 0.6, //遮罩透明度
									scrollbar : false,
									offset : '120px',//距离顶部
									moveType : 0, //拖拽风格，0是默认，1是传统拖动
									shift : 1, //0-6的动画形式，-1不开启
									content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'
											+ row.message + '</div>'
								});
						searchMsg();
					}
				});

	}

	function delMsg(id) {
		if (confirm("确认删除此信吗？")) {
			$.ajax({
				url : "${base}/center/news/message/delete.do",
				data : {
					id : id
				},
				success : function(result) {
					searchMsg();
				}
			});
		}
	}
</script>