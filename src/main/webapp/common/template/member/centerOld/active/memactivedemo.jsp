<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<div class="header clear">
	<div class="nav fr">
		<ul>
			<li class="current"><a href="http://58cpqt.com/userCenter/user_1_a.do">基本资讯</a></li>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
</div>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="tit">转盘</div>
			<jsp:include page="/common/include/active.jsp"></jsp:include>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>