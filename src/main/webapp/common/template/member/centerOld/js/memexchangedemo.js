$(function() {
	initUserData();
	getTypes();
	bindTypeChange();
	bindValuekeyup();
	$("#excBtn").removeAttr("disabled");
});
var types = [];
var selectedType = {};
function initUserData() {
	$.ajax({
		url : base + "/center/banktrans/exchange/udata.do",
		success : function(result) {
			$("#mnyCredit").html(result.money);
			$("#scoreCredit").html(result.score);
		}
	});
}
function getTypes() {
	$.ajax({
		url : base + "/center/banktrans/exchange/exctypes.do",
		success : function(result) {
			types = result;
			if ($("#exchangeType").val()) {
				selectedType = getExcTypeByType($("#exchangeType").val());
			}
		}
	});
}

function getExcTypeByType(val) {
	if (types) {
		for (var i = 0; i < types.length; i++) {
			if (val == types[i].type) {
				return types[i];
			}
		}
	}
	return null;
}

function bindTypeChange() {
	$("#exchangeType")
			.change(
					function() {
						var selval = $(this).children('option:selected').val();
						var selHtml = $(this).children('option:selected')
								.html().split("兑换");
						if (selval > 0) {
							selectedType = getExcTypeByType(selval);
							if (selectedType) {
								$("#exchange_remark_span").html(
										selectedType.numerator + selHtml[0]
												+ "可兑换"
												+ selectedType.denominator
												+ selHtml[1] + "(兑换比例："
												+ selectedType.numerator + ":"
												+ selectedType.denominator
												+ ")");
								if ($("#amount").val()) {
									$("#amount").blur();
								}
							} else {
								$("#exchange_remark_span").html("暂时未开放！");
							}
						} else {
							$("#exchange_remark_span").html("");
						}
					});

}

function bindValuekeyup() {
	$("#amount").keyup(
			function() {
				var typeVal = $("#exchangeType").val();
				if (!typeVal) {
					alert("请选择兑换类型！");
					return;
				}
				var selval = $(this).val();
				if (isNaN(selval)) {
					$(this).val("");
					return;
				}
				selval = Math.abs(selval);
				$(this).val(selval);
				if (selectedType != {}) {
					var result = 0;
					var typeName = "";
					if (selectedType.type == 1) {
						result = Math.floor(selval * selectedType.denominator
								/ selectedType.numerator);
						typeName = "积分";
					} else if (selectedType.type == 2) {
						result = Math.round(selval * selectedType.denominator
								* 100 / selectedType.numerator) / 100;
						typeName = "现金";
					}
					$("#exchange_result_span")
							.html("兑换可获得" + result + typeName);
				} else {
					$("#exchange_result_span").html("暂时未开放！");
				}
			});
}

function exchange() {
	$("#excBtn").attr("disabled", "disabled");
	if (!$("#exchangeType").val()) {
		alert("请选择兑换类型！");
		$("#excBtn").removeAttr("disabled");
		return;
	}

	var amount = $("#amount").val();

	if (!amount) {
		alert("额度不能为空！");
		$("#excBtn").removeAttr("disabled");
		return;
	}

	if (amount > selectedType.maxVal) {
		alert("额度必须小等于" + selectedType.maxVal);
		$("#excBtn").removeAttr("disabled");
		return;
	}

	if (amount < selectedType.minVal) {
		alert("额度必须大等于" + selectedType.minVal);
		$("#excBtn").removeAttr("disabled");
		return;
	}

	$.ajax({
		url : base + "/center/banktrans/exchange/exchange.do",
		data : {
			typeId : selectedType.id,
			amount : amount
		},
		success : function(result) {
			initUserData();
			alert("兑换成功！");
			$("#excBtn").removeAttr("disabled");
		}
	});
}
