<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
/*弹出框*/
#myHiddenDiv, #myHiddenGzDiv {
	display: none;
}

.myHiddenDiv {
	display: none;
}

.popup {
	border: 1px solid #ccc;
	background-color: #fff;
	padding-bottom: 10px;
	position: relative;
}

.popup a:hover {
	text-decoration: none;
	color: #fff;
}

.popup-header {
	padding-top: 1px;
	height: 30px;
	line-height: 15px;
	background-color: #E7E7E7
}

.popup-header h2 {
	font-size: 14px;
	line-height: 30px;
	width: 100%;
	text-align: center;
}

.popup-body {
	width: 100%;
}

a.close {
	display: block;
	color: #fff;
	text-decoration: none;
	font-size: 12px;
	font-weight: 700;
	background: url(../imgs/b1-bg07.gif) no-repeat;
	width: 106px;
	height: 24px;
	line-height: 24px;
	text-align: center;
	margin: 0 auto;
}

.popup .closeBtn {
	position: absolute;
	top: 2px;
	right: 2px;
	display: block;
	width: 50px;
	padding: 4px 0;
	text-align: center;
	color: #333;
}

.popup .closeBtn:hover {
	color: #EC6D51;
}
</style>
<!--top 开始-->
<div class="header clear">
	<div class="nav fr">
		<ul>
			<li class="current"><a href="${base}/center/record/betrecord/page.do">投注记录</a></li>
			<li><a href="${base}/center/record/hisrecord/page.do">往来记录</a></li>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
	<!-- <div class="time">美东时间：2015/10/20 9:56:14</div> -->
</div>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="category">
			<c:if test="${isTyOnOff eq 'on' }">
				<a class="current" cur_id="1" id="category_footballrecord" href="#" onclick="javascript:initRdsData(1);">体育</a>|
			</c:if>
			<c:if test="${isCpOnOff eq 'on' }">
				<a id="category_lotteryrecord" cur_id="2" href="#" onclick="javascript:initRdsData(2);">彩票</a>|
			</c:if>
			<c:if test="${isLhcOnOff eq 'on' }">
				<a id="category_lotteryrecord" cur_id="3" href="#" onclick="javascript:initRdsData(3);">六合彩</a>|
			</c:if>
			<c:if test="${isZrOnOff eq 'on' }">
				<a id="category_realrecord" cur_id="4" href="#" onclick="javascript:initRdsData(4);">真人投注记录</a>|
			</c:if>
			<c:if test="${isDzOnOff eq 'on' }">
				<a id="category_egamerecord" cur_id="5" href="#" onclick="javascript:initRdsData(5);">电子游艺投注记录</a>
			</c:if>
			</div>
			<div class="main_top"></div>
			<div class="ml20">
				<select id="type" name="type">
				</select> <input id="startTime" class="laydate-icon" value="${startTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> - <input
					id="endTime" class="laydate-icon" value="${endTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <input value="查询"
					type="button" onclick="javascript:search();">
			</div>
			<div id="record_tb"></div>
			<jsp:include page="/common/include/page.jsp"></jsp:include>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>
<!-- <div id="myHiddenRecordDetailView" class="myHiddenDiv" style="display: none">
	<div class="popup">
		<div class="popup-header">
			<h2>投注记录明细</h2>
			<a href="javascript:void(0)" onclick="$.closePopupLayer('openStaticTZDetailPopup')" class="closeBtn">关闭</a>
		</div>
		<div class="popup-body">
			<iframe src="" id="iframeName" marginwidth="0" marginheight="0" border="0" scrolling="yes" frameborder="0" height="500" width="759"></iframe>
		</div>
		<a href="javascript:;" class="close" onclick="$.closePopupLayer('openStaticTZDetailPopup')" title="关闭窗口">关闭窗口</a>
	</div>
</div> -->

<script id="record_tpl" type="text/html">
		<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						{{each colmuns as col}}
							<th>{{col}}</th>
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var combo = {
		1 : [ {
			"value" : 0,
			"name" : "所有体育"
		}, {
			"value" : 1,
			"name" : "足球"
		}, {
			"value" : 2,
			"name" : "篮球"
		} ],
		2 : [],
		3 : [ {
			"value" : "LHC",
			"name" : "六合彩"
		} ],
		4 : [  {
					"value" : "0",
					"name" : "所有记录"
				},{
					"value" : "1",
					"name" : "AG记录"
				},
				{
					"value" : "2",
					"name" : "BBIN记录"
				},{
					"value" : "3",
					"name" : "MG记录"
				}
			],
		5 : [ {
			"value" : "",
			"name" : "所有电子游艺"
		},{
			"value" : "4",
			"name" : "mg电子游艺"
		},{
			"value" : "5",
			"name" : "ag电子游艺"
		}]
	}
	var navCols = {
		1 : [ "单号/投注日期", "投注类型", "投注项", "投注额", "提交状态", "结算状态", "派彩金额" ],
		2 : [ "注单号", "下注时间", "彩种", "期号", "玩法", "倍数", "开奖号码", "投注额", "中奖金额",
				"状态" ],
		3 : [ "注单号", "下注时间", "彩种", "期号", "玩法", "倍数", "开奖号码", "投注额", "中奖金额",
				"状态" ],
		4 : [ "三方游戏类型", "注单号", "游戏局号", "投注金额", "输赢金额", "投注内容", "投注时间(游戏)", "投注时间(北京)"],
		5 : [ "游戏类型", "游戏模块", "游戏类型", "投注金额", "输赢金额", "投注时间(游戏GMT-0)"]
	}
	var navKeys = {
		1 : [ "codeAndDate", "sportType", "remark", "bettingMoney",
				"bettingStatus", "balance", "bettingResult" ],
		2 : [ "orderId", "createTime", "lotName", "qiHao", "playName",
				"multiple", "lotteryHaoMa", "buyMoney", "winMoney", "status" ],
		3 : [ "orderId", "createTime", "lotName", "qiHao", "playName",
				"multiple", "lotteryHaoMa", "buyMoney", "winMoney", "status" ],
		4 : [ "gameType", "betId", "thirdBetCode", "betMoney", "payMoney",
				"playName", "betTime", "bjBetTime"],
		5 : [ "gameType", "thirdGameType", "gameCode", "betMoney", "payMoney",
				"betTime"]
	}
	var navUrls = {
		1 : "${base}/center/record/betrecord/sportrd.do",
		2 : "${base}/center/record/betrecord/lotteryrd.do",
		3 : "${base}/center/record/betrecord/marksixrd.do",
		4 : "${base}/center/record/betrecord/realrecord.do",
		5 : "${base}/center/record/betrecord/egamerecord.do"
	}
	
	var curNavFlag = $('.category>a:first').attr('cur_id');
	var first = true;
	$(function() {
		initCombo();
		initRdsData(curNavFlag);
		first = false;
	})

	function initCombo() {
		$.ajax({
			url : "${base}/center/record/betrecord/betrdtypes.do",
			success : function(result) {
				combo[2] = result.lotCombo;
			}
		});
	}
	function search() {
		initRdsData(curNavFlag);
	}

	template
			.helper(
					'$formatter',
					function(row, key) {
						var value = row[key];
						if (curNavFlag == 1) {
							if (key == "codeAndDate") {
								var bd = new Date(row.bettingDate);
								return row.bettingCode + "<br/>"
										+ bd.format("MM月dd日,hh:mm:ss");
							} else if (key == "sportType") {
								var st = row.sportType
								var html = "";
								if (st == 1) {
									html += "足球";
								} else if (st == 2) {
									html += "篮球";
								}

								if (row.gameTimeType == 1) {
									html += " - 滚球";
								} else if (row.gameTimeType == 2) {
									html += " - 今日";
								} else if (row.gameTimeType == 3) {
									html += " - 早盘";
								}

								html += "<br/>";
								if (row.mix == 2) {
									return html + "混合过关";
								}
								var ts = row.typeNames;
								if (!ts) {
									return html;
								}
								return html + ts.replace("-", "<br>");

							} else if (key == "remark") {
								if (row.mix != 2) {
									return toBetHtml(JSON.decode(value), row);
								}
								var html = "";
								var arr = JSON.decode(value)
								for (var i = 0; i < arr.length; i++) {
									if (i != 0) {
										html += "<div style='border-bottom:1px #303030 dotted;'></div>";
									}
									html += toBetHtml(arr[i], row);
								}
								return html;
							} else if (key == "bettingStatus") {
								if (value == 1) {
									return "<font color='blue'>待确认</font>";
								}
								if (value == 2) {
									return "已确认";
								}

								if (value == 3) {
									return "<font color='red'>取消</font>";
								}

								if (value == 4) {
									return "<font color='red'>手动取消</font>";
								}
							} else if (key == "balance") {

								if (value == 1) {
									return "<font color='blue'>未结算</font>";
								}
								if (value == 2) {
									return "<font color='green'>已结算</font>";
								}

								if (value == 3) {
									return "<font color='red'>结算失败</font>";
								}

								if (value == 4) {
									return "<font color='red'>比赛腰斩</font>";
								}

								if (value == 5) {
									return "<font color='red'>结算中</font>";
								}
							}
						}else if(curNavFlag==4 || curNavFlag==5) {
							if(key=='gameType' && curNavFlag==4){
								if (value == 1) {
									return 'ag';
								} else if (value == 2) {
									return 'bbin';
								} else if (value == 3) {
									return 'mg';
								}
							}
							if(key=='gameType' && curNavFlag==5){
								if (value == 4) {
									return 'mg电子';
								} else if (value == 5) {
									return 'ag电子';
								} else{
									return '未知';
								}
							}
							
							if (key == "betTime" || key == 'bjBetTime') {
								var bd = new Date(value);
								return bd.format("yyyy-MM-dd hh:mm:ss");
							}
						}else {
							if (key == "createTime") {
								var bd = new Date(value);
								return bd.format("yyyy-MM-dd hh:mm:ss");
							} else if (key == "status") {
								if (value == 1) {
									return '未开奖';
								} else if (value == 2) {
									return '已中奖';
								} else if (value == 3) {
									return '未中奖';
								} else if (value == 4) {
									return '撤单';
								} else if (value == 5) {
									return '派奖回滚成功';
								} else if (value == 6) {
									return '回滚异常';
								} else if (value == 7) {
									return '开奖异常';
								}
							}
						}
						return value;
					});

	function search() {
		initRdsData(curNavFlag);
	}
	function initRdsData(flag) {
		var a = $(".category").find("a");
		for(var i=0;i<6;i++){
			if(flag != (i+1)){
				a.eq(i).removeClass("current");
			}else{
				a.eq(i).addClass("current");
			}
		}
		if (curNavFlag != flag) {
			$("#jump_page").val(1);
			$("#jump_page_size").val(10);
		}

		var pageSize = 10;
		var pageNumber = 1;
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
			if (curNavFlag != flag) {
				chgTypeCombo(flag);
			}

		} else {
			chgTypeCombo(flag);
		}
		curNavFlag = flag;

		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : $("#type").val()
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				var html = template('record_tpl', result);
				$("#record_tb").html(html);
				initPage(result, function() {
					search();
				});
			}
		});
	}

	function chgTypeCombo(flag) {
		var html = "";
		for (var i = 0; i < combo[flag].length; i++) {
			var opt = combo[flag][i];
			html += "<option value='"+opt.value+"'>" + opt.name + "</option>";
		}
		if (combo[flag]) {
			$("#type").html(html);
		}
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}

	function toBetHtml(item, row) {
		var con = item.con;
		if (con.indexOf("vs") == -1) {
			con = '<span class="text-danger">' + con + '</span>';
		}
		var homeFirst = row.homeTeam == item.firstTeam;//主队是否在前
		var scoreStr = "";

		if (row.gameTimeType == 1) {
			if (homeFirst) {
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreH + ":"
						+ row.scoreC + ")</b></font>";
			} else {
				scoreStr = "&nbsp;<font color='red'><b>(" + row.scoreC + ":"
						+ row.scoreH + ")</b></font>";
			}
		}
		var home = item.firstTeam;
		var guest = item.lastTeam;
		if (item.half === true && row.mix == 2) {
			home = home + "<font color='gray'>[上半]</font>";
			guest = guest + "<font color='gray'>[上半]</font>";
		}

		var html = item.league + "<br/>" + home + "&nbsp;" + con + "&nbsp;"
				+ guest + scoreStr + "<br/>" + "<font color='red'>"
				+ item.result + "</font>&nbsp;" + "@"
				+ "&nbsp;<font color='red'>" + item.odds + "</font>";
		var balance = row.mix != 2 ? row.balance : item.balance;
		if (balance == 4) {
			html = "<s style='color:red;'>" + html + "</s>"
		} else if (balance == 2 || balance == 5 || balance == 6) {
			var mr = row.mix != 2 ? row.result : item.matchResult;
			if (homeFirst) {
				html = html + "&nbsp;<font color='blue'>(" + mr + ")</font>";
			} else {
				var ss = mr.split(":");
				html = html + "&nbsp;<font color='blue'>(" + ss[1] + ":"
						+ ss[0] + ")</font>";
			}
		}
		return html;
	}
</script>