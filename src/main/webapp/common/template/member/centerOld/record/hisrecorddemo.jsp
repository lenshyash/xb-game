<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<!--top 开始-->
<div class="header clear">
	<div class="nav fr">
		<ul>
			<li><a href="${base}/center/record/betrecord/page.do">投注记录</a></li>
			<li class="current"><a href="${base}/center/record/hisrecord/page.do">往来记录</a></li>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
	<!-- <div class="time">美东时间：2015/10/20 9:56:14</div> -->
</div>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="category">
				<a href="javascript:void(0);" class="current" onclick="javascript:initRdsData(1);">充值记录</a>
				|<a href="javascript:void(0);" onclick="javascript:initRdsData(2);">取款记录</a>
				<c:if test="${isZrOnOff eq 'on' }">
				|<a href="javascript:void(0);" onclick="javascript:initRdsData(3);">真人转款记录</a>
				</c:if>
			</div>
			<div class="main_top"></div>
			<div class="headinfo ml20 mt10">
				<select id="type" name="type">
					<option value="" selected="selected">所有类型</option>
					<option value="5">在线存款</option>
					<option value="6">快速入款</option>
					<option value="7">一般入款</option>
				</select> <select id="state" name="state">
					<option value="" selected="selected">所有状态</option>
					<option value="1">处理中</option>
					<option value="2">处理成功</option>
					<option value="3">处理失败</option>
				</select> 
				
				<select id="status" name="status">
					<option value="" selected="selected">所有状态</option>
					<option value="1">处理成功</option>
					<option value="2">处理失败</option>
				</select>
				<select id="gameType" name="gameType">
					<option value="" selected="selected">三方类型</option>
					<option value="1">AG</option>
					<option value="2">BBIN</option>
					<option value="3">MG</option>
				</select>
				<input id="startTime" class="laydate-icon" value="${startTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> - <input
					id="endTime" class="laydate-icon" value="${endTime }" style="width: 100px;" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'});" onclick="this.focus();"> <input value="查 询"
					class="btn search" onclick="search();" type="button">
			</div>
			<div id="record_tb"></div>

			<jsp:include page="/common/include/page.jsp"></jsp:include>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>

<script id="record_tpl" type="text/html">
		<table class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						{{each colmuns as col}}
							<th>{{col}}</th>
						{{/each}}
					</tr>
				</tbody>
				<tbody id="content" class="table_b_tr">
					{{each list as row}}
					<tr class="text-c">
						{{each navKeys as key}}
							<td>{{$formatter row[key] key}}</td>
						{{/each}}
					</tr>
					{{/each}}
				</tbody>
			</table>
</script>
<script type="text/javascript">
	var colmuns = [ "日期", "交易类别" ];
	var navCols = {
		1 : [ "支付名称", "交易额度", "状态", "说明" ],
		2 : [ "交易额度", "状态", "说明" ],
		3 : [ "游戏类型", "转账id本地","转账id三方","转账金额","转账状态" ]
	}
	var navKeys = {
		1 : [ "createDatetime", "type", "payName", "money", "status", "opDesc" ],
		2 : [ "createDatetime", "type", "drawMoney", "status", "remark" ],
		3 : [ "createDatetime", "transType", "gameType","transId", "thirdTransId", "transMoney","transStatus"]
	}
	var navUrls = {
		1 : "${base}/center/record/hisrecord/comrd.do",
		2 : "${base}/center/record/hisrecord/drawrd.do",
		3 : "${base}/center/record/hisrecord/realtslog.do"
	}
	var curNavFlag = 1;
	$(function() {
		initRdsData(curNavFlag);
		first = false;
	})

	function search() {
		initRdsData(curNavFlag);
	}

	template.helper('$formatter', function(content, key) {

		if (key == "createDatetime") {
			return DateUtil.formatDatetime(content);
		} else if (key == "type") {
			return GlobalTypeUtil.getTypeName(1, 1, content);
		} else if (key == "status") {
			if (curNavFlag == 1) {
				return GlobalTypeUtil.getTypeName(1, 2, content);
			} else if (curNavFlag == 2) {
				return GlobalTypeUtil.getTypeName(1, 3, content);
			}
		}
		
		if(curNavFlag==3){
			if(key=='transStatus'){
				if(content==1|| content == '1'){
					return "成功";
				}else{
					return "失败";
				}
			}else if(key=='gameType'){
				if (content == 1) {
					return 'AG';
				} else if (content == 2) {
					return 'BBIN';
				} else if (content == 3) {
					return 'MG';
				}
			}else if(key=='transType'){
				if (content == 1) {
					return '系统->三方';
				} else{
					return '三方->系统';
				} 
			}
		}
		return content;
	});
	var first = true;
	function initRdsData(flag) {
		var a = $(".category").find("a");
		a.removeClass("current");
		if(flag==1){
			a.eq(0).addClass("current");
		}else if(flag==2){
			a.eq(1).addClass("current");
		}else if(flag==3){
			a.eq(2).addClass("current");
		}
		var type = 0;
		var status = $("#state").val();
		if (flag == 1) {
			$("#type").removeClass("hidn");
			type = $("#type").val();
		} else {
			$("#type").addClass("hidn");
			$("#type").val("");
		}
		if(flag==3){
			$("#type").addClass("hidn");
			$("#state").addClass("hidn");
			$("#gameType").removeClass("hidn");
			$("#status").removeClass("hidn");
			type = $("#gameType").val();
			status = $("#status").val();
		}else{
			$("#gameType").addClass("hidn");
			$("#status").addClass("hidn");
			$("#state").removeClass("hidn");
		}
		curNavFlag = flag;
		var pageSize = 10;
		var pageNumber = 1;
		if (!first) {
			pageSize = $("#jump_page_size").val();
			pageNumber = $("#jump_page").val();
		}
		$.ajax({
			url : navUrls[flag],
			data : {
				startTime : $("#startTime").val(),
				endTime : $("#endTime").val(),
				pageNumber : pageNumber,
				pageSize : pageSize,
				type : type,
				status:status
			},
			success : function(result) {
				result.colmuns = getNewCols();
				result.navKeys = navKeys[flag];
				var html = template('record_tpl', result);
				$("#record_tb").html(html);

				initPage(result, function() {
					search();
				});
			}
		});
	}

	function changeNav(flag) {
		initRdsData(flag);
	}

	function getNewCols() {
		var newCols = [];
		for (var i = 0; i < colmuns.length; i++) {
			newCols.push(colmuns[i]);
		}
		for (var i = 0; i < navCols[curNavFlag].length; i++) {
			newCols.push(navCols[curNavFlag][i]);
		}

		return newCols;
	}
</script>