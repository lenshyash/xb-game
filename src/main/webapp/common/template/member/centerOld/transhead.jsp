<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header clear">
	<div class="nav fr">
		<ul>
			<c:if test="${isZrOnOff eq 'on' }">
				<li ${mnyChgFlag }><a href="${base}/center/banktrans/mnychg/page.do">额度转换</a></li>
			</c:if>
			<li ${comFlag }><a href="${base}/center/banktrans/deposit/page.do">线上存款</a></li>
			<li ${drawFlag }><a href="${base}/center/banktrans/draw/page.do">线上取款</a></li>
			<c:if test="${isExChgOnOff eq 'on'}">
				<li ${exchangeFlag }><a href="${base}/center/banktrans/exchange/page.do">线上兑换</a></li>
			</c:if>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
</div>