<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/common/template/member/base.jsp"></jsp:include>
<div class="header clear">
	<div class="nav fr">
		<ul>
			<li class="current"><a href="http://58cpqt.com/userCenter/user_1_a.do">基本资讯</a></li>
		</ul>
		<a href="${base}/center/banktrans/deposit/page.do" class="deposit_btn">线上存款</a>
	</div>
	<!-- <div class="time">美东时间：2015/10/17 9:37:59</div> -->
</div>
<!--top 结束-->
<!--content 开始-->
<div class="content">
	<!--左侧分类 开始-->
	<jsp:include page="../leftdemo.jsp"></jsp:include>
	<div class="main">
		<div class="main_box">
			<div class="main_top"></div>
			<div class="tit">基本资讯</div>
			<table id="meminfo_tb" class="tab" border="0" cellpadding="0" cellspacing="0" width="730">
				<tbody>
					<tr>
						<th width="161">帐户</th>
						<th width="178">币别</th>
						<th width="169">余额</th>
						<th width="169">积分</th>
						<th width="100">手机</th>
						<!-- <th width="100">最后登入时间</th> -->
						<th width="122">密码</th>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="main_footer"></div>
	</div>
</div>
<!--content 结束-->
<jsp:include page="../footerdemo.jsp"></jsp:include>
<script id="meminfo_tpl" type="text/html">
	<tr>
		<td>{{account}}</td>
		<td>人民币(RMB)</td>
		<td><span id="GCCredit">{{money}}</span></td>
		<td><span id="GCCredit">{{score}}</span></td>
		<td><span id="mobile">{{phone}}</span></td>
		<td><a href="javascript:void(0);" onclick='window.open("${base}/center/member/meminfo/pwdpg.do", "save", "height=300,width=560,top=200,left=470 ")' class="btn">修改密码</a></td>
	</tr>

</script>
<script type="text/javascript">
	$(function() {
		initMemdata();
	})

	function initMemdata() {
		$.ajax({
			url : base + "/center/member/meminfo/data.do",
			success : function(result) {
				/* var eachdata = {
					"data" : result
				}; */
				var html = template('meminfo_tpl', result);
				$("#meminfo_tb").append(html);
			}
		});
	}
</script>
