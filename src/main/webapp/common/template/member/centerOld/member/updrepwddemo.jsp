<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link href="${base}/common/template/member/center/css/mem_pass.css" rel="stylesheet" type="text/css">
<link href="${base}/common/template/member/center/css/standard.css" rel="stylesheet" type="text/css">
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<!--top 开始-->
<div class="main">
	<h1>
		<span>更改取款密码</span>
		<!--span class="close_box" onClick="window.close();">关闭</span-->
	</h1>
	<input type="hidden" name="action" value="1"> <input type="hidden" name="uid" value="13d111a9e000cbd82ff99"> <input type="hidden" name="flag" value="1">
	<div class="main_bg">
		<div class="main_box">
			<h2></h2>
			<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
				<tbody>
					<tr>
						<td class="keyin">
							<p>
								<em>密码：</em><input type="password" name="password" id="password" value="" size="12" maxlength="12">
							</p>
							<p>
								<em>确认密码：</em><input type="password" name="rpassword" id="rpassword" value="" size="12" maxlength="12">
							</p>
						</td>
						<td class="info"><span><tt class="bb">说明:</tt></span> <span><tt>1.</tt> 您的新密码必须由6-12个字母和数字<br>(A-Z 和 0-9)组成。</span> <span><tt>2.</tt> 您的新密码不能和现用的密码相同。</span></td>
					</tr>
				</tbody>
			</table>

		</div>
		<div class="foot">
			<input type="button" name="OK" value="确认" class="yes" onclick="updpwd();"> <input type="button" name="cancel" value="取消" class="no" onclick=" javascript:i(); ">
		</div>
	</div>
	<div class="bottom"></div>
</div>

<script type="text/javascript">
	function updpwd() {
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if (!password) {
			alert("新密码不能为空！");
			return;
		}
		if (!rpassword) {
			alert("确认不能为空！");
			return;
		}

		if (password !== rpassword) {
			alert("两次密码不一致！");
			return;
		}
		$.ajax({
			url : "${base}/center/member/meminfo/repwd.do",
			data : {
				pwd : password,
				rpwd : rpassword
			},
			success : function(result) {
				alert("修改成功！");
				i();
			}
		});
	}

	function i() {
		self.opener = null;
		self.close();
	}
</script>


