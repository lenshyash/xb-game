<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="sidebar fl">
	<div class="title">
		<img src="${base}/common/template/member/center/img/memIcon.png">会员专区
	</div>
	<div class="secondary">
		<ul>
			<li ${myAccountFlag } id="sidebar_user_1_a"><a href="${base}/center/member/meminfo/page.do">我的账户</a></li>
			<li ${bankTransFlag } id="sidebar_user_2_b"><a href="${base}/center/banktrans/mnychg/page.do">银行交易</a></li>
			<li ${betHisFlag } id="sidebar_footballrecord"><a href="${base}/center/record/betrecord/page.do">交易记录</a></li>
		</ul>
	</div>
	<div class="title title2">
		<img src="${base}/common/template/member/center/img/msgIcon.png">信息公告
	</div>
	<div class="secondary">
		<ul>
			<li ${messageFlag } id="sidebar_user_4_a"><a href="${base}/center/news/message/page.do">站内信</a></li>
		</ul>
	</div>
</div>
