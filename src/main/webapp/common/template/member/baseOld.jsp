<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${base}/common/template/member/center/css/css.css" rel="stylesheet" type="text/css">
<link href="${base}/common/css/core/bank/css/bank.css?v=1.3" rel="stylesheet" type="text/css">
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script src="${base}/common/js/contants.js?v=2"></script>
<script src="${base}/common/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

