<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<c:choose>
	<c:when test="${openMethod == 'href' }">
		<script>
			location.href = '${redirectUrl}';
		</script>
	</c:when>
	<c:when test="${openMethod == 'agethref' }">
		<a href="${redirectUrl }?" id="aHref">点击跳转</a>
		<div id="redirectUrlForm">
			<c:forEach items="${params }" var="p">
				<input name='${p.key }' value='${p.value }' type="hidden" >
			</c:forEach>
		</div>
		<script> 
	        let aHrefOldtemp = document.getElementById('redirectUrlForm').getElementsByTagName('input')
	            let aHrefOld = document.getElementById('aHref').getAttribute("href")
	            let aHref = document.getElementById('aHref')
	            let insertStr = (soure,start, newStr) => {
	              return soure.slice( start) + newStr + soure.slice(0,start)
	            } 
	        let data = new Array()
	
	        for (let i =0;i<aHrefOldtemp.length;i++){
	            aHrefOld = insertStr(aHrefOld,0,aHrefOldtemp[i].name+'='+aHrefOldtemp[i].value+'&')
	        }
	        aHrefOld =aHrefOld.slice(0,-1);
	        
	        aHref.setAttribute('href',aHrefOld)
	        window.onload=()=>{
	        	window.location.href= aHrefOld
	        }
		</script>
	</c:when>
	<c:otherwise>
		<form action="${redirectUrl}" method="${formMethod }" id="redirectUrlFormId">
			<c:choose>
				<c:when test="${quotesType == 'single' }">
					<c:forEach items="${params }" var="p">
						<input name='${p.key }' value='${p.value }' type="hidden">
					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach items="${params }" var="p">
						<input name="${p.key }" value="${p.value }" type="hidden">
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</form>
		<script>
			document.getElementById("redirectUrlFormId").submit();
		</script>
	</c:otherwise>
</c:choose>
</body>
</html>