<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }_DNS教程</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<link href="${base }/common/template/member/mysdns/css/base.css" type="text/css" rel="stylesheet">
<link href="${base }/common/template/member/mysdns/css/css.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="${base }/common/template/member/mysdns/js/jquery-1.11.1.min.js"></script>
<script>
	$(function() {
		function nav(btn, div) {
			this.btn = btn;
			this.div = div;
		}
		nav.prototype.pn = function() {
			var s = $(this.btn);
			var d = $(this.div);
			s.click(function() {
				i = $(this).index();
				s.removeClass('cur');
				$(this).addClass('cur');
				d.css({
					display : 'none'
				});
				d.eq(i).css({
					display : 'block'
				});
			})
		}
		var p1 = new nav('.nav li', '.c_centet div');
		p1.pn();
	})
</script>
</head>
<body>
	<div class="top">
		<div class="tops">
			<div class="xs"></div>
			<div class="logo"><img src="${base }/images/logo.png" style="width: 100%;height: 100%;"></div>
			<div class="by"></div>
		</div>
	</div>
	<div class="center">
		<div class="icon_1"></div>
		<div class="nav">
			<ul>
				<li class="cur">
					<p>电脑教程</p>
				</li>
				<li class="">
					<p>Android端教程</p> <img src="${base }/common/template/member/mysdns/images/shou.gif">
				</li>
				<li class="">
					<p>IOS端教程</p> <img src="${base }/common/template/member/mysdns/images/shou.gif">
				</li>
			</ul>
		</div>
		<div class="center_zz">
			<div class="c_centet">
				<div class="n1" style="display: block;">
					<a href="javascript:void(0);"> <img
						style="margin-top: 30px;" src="${base }/common/template/member/mysdns/images/pc_03.jpg">
					</a> <img src="${base }/common/template/member/mysdns/images/pc_06.jpg"> <img
						src="${base }/common/template/member/mysdns/images/pc_09.jpg"> <img
						src="${base }/common/template/member/mysdns/images/pc_12.jpg"> <img
						src="${base }/common/template/member/mysdns/images/pc_14.jpg"> <img
						style="margin-bottom: 10px;" src="${base }/common/template/member/mysdns/images/pc_16.jpg">
				</div>
				<div class="n2" style="display: none;">
					<img style="margin-top: 20px;" src="${base }/common/template/member/mysdns/images/az_03.jpg">
					<img src="${base }/common/template/member/mysdns/images/az_05.jpg"> <img
						src="${base }/common/template/member/mysdns/images/az_09.jpg" style="margin-bottom: 30px;">
				</div>
				<div class="n3" style="display: none;">
					<img style="margin-top: 20px;" src="${base }/common/template/member/mysdns/images/ios_03.jpg">
					<img src="${base }/common/template/member/mysdns/images/ios_06.jpg"> <img
						src="${base }/common/template/member/mysdns/images/ios_08.jpg" style="margin-bottom: 30px;">
				</div>
			</div>
		</div>
	</div>
	<div class="footer">Copyright © ${_title } Reserved</div>

</body>
</html>