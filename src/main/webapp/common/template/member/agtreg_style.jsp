<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/agtregconf.do"></script>
<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>
<style type="text/css">
.JoinMemForm {
	width: 700px;
	margin: 0px auto;
}

#memCash_div p {
	line-height: 12px;
	height: 26px;
}

#memCash_div label {
	float: left;
	height: 25px;
	line-height: 25px;
	text-align: right;
	width: 135px;
}

#memCash_div .memCash_text {
	display: block;
	padding: 5px 0 0 135px;
	min-height: 26px;
}

input, select {
	height: 20px;
	line-height: 20px;
}

select.err {
	color: red;
}

#memCash_div #myFORM p {
	height: auto;
	min-height: 26px;
}

#myFORM input[type=text], #myFORM input[type=password], #myFORM select {
	border: 1px solid #666666;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	box-shadow: 0 0 6px #3A3A3A;
	-moz-box-shadow: 0 0 6px #3A3A3A;
	-webkit-box-shadow: 0 0 6px #3A3A3A;
}

#myFORM input[type=text]:hover, #myFORM input[type=password]:hover,
	#myFORM select:hover {
	border: 1px solid #E29EFC;
}

#myFORM input[type=text]:focus, #myFORM input[type=password]:focus,
	#myFORM select:focus {
	border: 1px solid #9900FF;
}

#myFORM #HiddenInput {
	width: 0px;
	height: 0px;
	background: transparent;
	border: 0;
}

#myFORM input[type=text][id=HiddenInput] {
	width: 0;
	height: 0;
	background: transparent;
	border: 0;
}

li {
	display: list-item;
	text-align: -webkit-match-parent;
	list-style: disc;
/* 	margin-left: 25px; */
	line-height: 18px;
}

ul {
	display: block;
	list-style-type: disc;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
/* 	-webkit-padding-start: 10px; */
}

.ui-datepicker-trigger {
	margin-left: 5px;
	cursor: pointer;
}

#memCash_div #ui-datepicker-div a {
	color: #000;
}

.ui-datepicker th {
	word-break: break-all;
	padding: 2px 1px;
}

.ui-datepicker {
	width: 260px;
}
/* us by validation */
.FormError {
	position: absolute;
	top: 300px;
	left: 300px;
	display: block;
	z-index: 500;
}

.FormError .FormErrorC {
	width: 100%;
	background: #ee0101;
	position: relative;
	z-index: 501;
	color: #fff;
	font-family: tahoma;
	font-size: 11px;
	line-height: 14px;
	border: 2px solid #ddd;
	box-shadow: 0 0 6px #000;
	-moz-box-shadow: 0 0 6px #000;
	-webkit-box-shadow: 0 0 6px #000;
	padding: 4px 10px 4px 10px;
	border-radius: 6px;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
}

.FormError .FormErrorA {
	width: 15px;
	margin: -2px 0 0 13px;
	position: relative;
	z-index: 506;
}

.FormError .FormErrorABottom {
	box-shadow: none;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
	margin: 0px 0 0 12px;
	top: 2px;
}

.FormError .FormErrorA div {
	border-left: 2px solid #ddd;
	border-right: 2px solid #ddd;
	box-shadow: 0 2px 3px #444;
	-moz-box-shadow: 0 2px 3px #444;
	-webkit-box-shadow: 0 2px 3px #444;
	font-size: 0px;
	height: 1px;
	background: #ee0101;
	margin: 0 auto;
	line-height: 0;
	font-size: 0;
	display: block;
}

.FormError .FormErrorABottom div {
	box-shadow: none;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
}
/*use by jquery ui dialog*/
.ui-dialog-content {
	padding: 5px 1px;
}

.ui-dialog-title {
	font-size: 14px;
}

#Dialog {
	font-size: 12px;
}

#Dialog p {
	line-height: 16px;
	margin: 10px 0px 10px 10px;
	height: auto;
}

#SendForm input[type=text], #SendForm input[type=password] {
	border: 1px solid #949494;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	box-shadow: 0 0 6px #D3D3D3;
	-moz-box-shadow: 0 0 6px #D3D3D3;
	-webkit-box-shadow: 0 0 6px #D3D3D3;
}

#SendForm input[type=text]:hover, #SendForm input[type=password]:hover {
	border: 1px solid #E29EFC;
}

#SendForm input[type=text]:focus, #SendForm input[type=password]:focus {
	border: 1px solid #9900FF;
}
/*  ADVANCED STYLES */
.top_testresult {
	font-weight: bold;
	font-size: 13px;
	font-family: arail, helvetica, san-serif;
	color: #FFF;
	padding: 0;
	margin: 0 0 2px 0;
}

.top_testresult span {
	padding: 6px;
	margin: 0;
}

.top_shortPass {
	background: #FF0000;
}

.top_badPass {
	background: #000000;
}

.top_goodPass {
	background: #0029AA;
}

.top_strongPass {
	background: #128511;
}

.JM_content {
	padding: 10px;
}

.JM_content b, .JM_content strong {
	font-weight: bolder
}

#memCash_div .JM_content p {
	height: auto;
	line-height: 16px;
}

.loading_pic {
	display: none;
}

#rmNum {
	color: #999;
	text-align: center;
}

.joinform_submit, .joinform_cancel {
	padding-bottom: 20px \9;
	height: 24px;
}

.memCash_code_wrap {
	padding-bottom: 5px;
}

.memCash_code {
	float: left;
	padding-right: 5px;
	min-width: 133px;
	height: 25px;
	line-height: 25px;
	font-family: verdana;
	font-weight: bold;
}
/*共用*/
input {
	padding-left: 6px;
	padding-right: 6px;
}

body {
	background-color: #FFFFFF;
}

.bodyStyle {
	background-color: #FFFFFF;
}

.skinbg {
	background-color: #FBE9AC;
}
/*會員中心*/
#MACenter #MALogo {
	float: left;
	background: url(${base}/images/venice_mlogo.png) left bottom no-repeat;
	_display: inline;
	margin: 12px 0 0 30px;
	width: 170px;
	height: 80px;
}
/*忘記密碼*/
#forgetPwd {
	width: 300px;
	margin: 35px auto;
	font-size: 12px;
	color: #000000;
}

#forgetPwd table {
	background-color: #4E321C;
}

#forgetPwd table thead, #forgetPwd table tfoot {
	color: #F2E1B0;
	text-align: center;
}

#forgetPwd table tbody {
	background-color: #FFFFFF;
}

#forgetPwd table td.style1 {
	text-align: right;
	text-indent: 1em;
	padding-right: 5px;
	width: 50%;
}

#bottom-text {
	text-align: left;
}

.error {
	color: #FF0000;
}

/*修改密碼、修改帳號共用*/
#memAccTable {
	color: #FFFFFF;
	border-collapse: collapse;
	text-align: center;
}

#memAccTable table {
	background-color: #4E321C;
	border-collapse: collapse;
	margin: 0 auto;
	text-align: left;
}

#memAccTable table tr td {
	border: 1px solid #4E321C;
	height: 30px;
}

#memAccTable table thead, #memAccTable table tfoot {
	color: #F2E1B0;
	font-size: 12px;
	padding-left: 59px;
	height: 37px;
	text-align: center;
}

#memAccTable table thead td {
	text-align: left;
	padding-left: 59px;
}

#memAccTable table tbody {
	color: #000000;
	background-color: #FFFFFF;
	font-size: 12px;
	text-align: left;
}

#memAccTable table .tipText {
	width: 50%;
	text-align: right;
	padding-right: 5px;
}

#memAccTable table .tipDwText {
	width: 40%;
	text-align: right;
	padding-right: 5px;
}

#memAccTable table .bottomTip {
	font-size: 11px;
	text-align: center;
	background-color: #C9CACA;
	height: 40px;
}

#memAccTable table  .modifyProfileTitle {
	font-weight: normal;
	font-size: 12px;
	text-align: center;
	padding-left: 0;
}

a.chg_passwd_flag {
	color: #FFFFFF;
	text-decoration: none;
}
/*額度轉換、會員資料*/
#mainContent {
	font-size: 12px;
	border: 1px solid #707161;
}

#pageTitle {
	background-color: #616A74;
	color: #FFFFFF;
	padding: 4px 0 0 10px;
}

#tableWrap {
	background-color: #CBD1D4;
	border-width: 0 1px 1px;
	border-color: #707161;
	border-style: solid;
	padding: 1px 2px 2px;
}

.tableStyle01 {
	border: 1px solid #666666;
	margin: 0px auto;
	text-align: center;
	width: 100%;
}

.tableStyle01 th {
	background-color: #999999;
	border: 1px solid #666666;
	color: #000000;
	font-weight: normal;
	text-align: center;
	height: 23px;
	line-height: 23px;
}

.tableStyle01 td {
	background-color: #FFFFFF;
	border: 1px solid #666666;
	padding: 2px;
}

.tableStyle01 tr .gameLimit {
	background-color: #FBE9AC;
	color: #000000;
}

.za_button {
	font-size: 12px;
}
/*下注狀況*/
.tableStyle01 .wagers th {
	background-color: #F1EFEF;
	text-align: center;
}

#pageTitle #pageTitleEx {
	float: right;
	color: #FF0000;
}

#pageTitle #pageTitleEx a {
	color: #CC0000;
	text-decoration: none;
	font-weight: bold;
}

.tableStyle01 .b_rig .amountGold {
	background-color: #CCCCCC;
	text-align: right;
}

.tableStyle01 .b_rig .awinGold {
	background-color: #990000;
	color: #FFFFFF;
	text-align: right;
}

.tableStyle01 .red_border {
	border: 2px solid #FF0000;
}

.tableStyle01 .b_rig_mor {
	background-color: #EDEDED;
	text-align: right;
}

.tableStyle01 .b_rig_mor td {
	background-color: #EDEDED;
}

.tableStyle01 .b_rig {
	background-color: #FFFFFF;
	text-align: right;
}

.b_cen {
	background-color: #FFFFFF;
	text-align: center;
	height: 70px;
}

/*往來紀錄*/
#mainContent p.subject {
	width: 82px;
	height: 18px;
	line-height: 18px;
	text-indent: 5px;
}

.Wdate {
	width: 90px;
}

.BookTable td {
	padding: 1px 0px;
}

#foot {
	text-align: left;
	color: #000000;
	font-size: 12px;
	padding-left: 10px;
}

#foot a {
	color: #CC0000;
	font-weight: bold;
	text-decoration: none;
}
/*線上存取款*/
#bank_body {
	background: #201C19;
}

#bank_header {
	margin: 0px auto;
	width: 800px;
	height: 135px;
	background: url(${base}/images/rule_logo_bg.jpg) left top repeat-x;
}

#bank_logo {
	width: 306px;
	height: 135px;
	margin: 0px auto;
	background: url(${base}/images/rule_logo.jpg) left top no-repeat;
}

#bank_content {
	width: 800px;
	margin: 0px auto;
	background-color: #E7D48E;
}

#bank_footer {
	margin: 0px auto;
	width: 800px;
	height: 100px;
	color: #65432A;
	background-color: #000000;
	line-height: 100px;
}

#bank_footer font {
	color: #FFFFFF;
}

#pay_system_option a {
	color: #FF0000;
	text-decoration: none;
}
/*會員註冊*/
#joinMember iframe {
	width: 730px;
	height: 1000px;
}

#memCash_div {
	background-color: transparent;
	color: #FFFFCD;
	font-size: 12px;
}

#memCash_div .memCash_tit {
	width: 452px;
	height: 25px;
	_height: 12px;
	padding: 25px 0 0 28px;
	color: #FFFEE4;
}

.JM_content {
	padding: 10px 0 10px 15px;
}

#memCash_div fieldset {
	padding: 10px;
	margin: 10px;
	border: 2px solid #63432E;
}

#memCash_div legend {
	color: #925D35;
	font-size: 12px;
	font-weight: bold;
}

#memCash_div .star {
	font-family: verdana, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	font-weight: bold;
	vertical-align: -2px;
}

#memCash_div a {
	text-decoration: none;
	color: #BF0F0F;
}

#memCash_div .memCash_text {
	line-height: 15px;
	height: auto;
}

#myFORM input[type=text], #myFORM input[type=password], #myFORM select {
	border: 1px solid #ABADB3;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	box-shadow: 0 0 6px #CFD1D8;
	-moz-box-shadow: 0 0 6px #CFD1D8;
	-webkit-box-shadow: 0 0 6px #CFD1D8;
}
/*CL共用頁面*/
body.TPL {
	background-color: #FFF;
}

.Tpl-table01 {
	background-color: #FFFFFF;
	border: 1px solid #CCC;
	color: #000;
	font-size: 12px;
}

table .Tpl-title01 th {
	background-color: #FED54E;
	height: 24px;
	text-align: center;
	font-weight: bold;
	color: #FFFFFF;
	/* IE10 */
	background: -ms-linear-gradient(top, #FED54E 0%, #A68B33 100%);
	/* Mozilla Firefox */
	background: -moz-linear-gradient(top, #FED54E 0%, #A68B33 100%);
	/* Opera */
	background: -o-linear-gradient(top, #FED54E 0%, #A68B33 100%);
	/* Webkit (Chrome 11+) */
	background: -webkit-linear-gradient(top, #FED54E 0%, #A68B33 100%);
	/* IE6-9 */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FED54E',
		endColorstr='#A68B33', GradientType=0);
	/* Proposed W3C Markup */
	background: linear-gradient(top, #FED54E 0%, #A68B33 100%);
}

.Tpl-table01 th, .Tpl-table01 td {
	height: 22px;
	border: 1px solid #CCC;
}

tr.Tpl-trcolor1 {
	background: #FFFFFF;
	/* IE10 */
	background: -ms-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* Mozilla Firefox */
	background: -moz-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* Opera */
	background: -o-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* Webkit (Chrome 11+) */
	background: -webkit-linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
	/* Proposed W3C Markup */
	background: linear-gradient(top, #FFFFFF 0%, #EEEEEE 100%);
}

tr.Tpl-trcolor2 {
	background: #EEEEEE;
}

tr.Tpl-trmouseenter {
	background: #DEF;
}
</style>
<div id="memCash_div">
	<h3 class="memCash_tit">【威尼斯人娱乐城】欢迎您的加入！</h3>
	<div class="JM_content JM_content_t">
		<div id="joinmem">
			<div>
				<p>
					<strong><span style="color: rgb(255, 0, 0)">全网首家天天返水，周周返水任您选择的娱乐城，让您随心所欲！</span></strong>
				</p>
				<ul style="margin-left: 25px;">
					<li>新注册会员默认为周周返水，如需申请天天反水请联系客服人员！</li>
				</ul>
			</div>
		</div>
	</div>
	<form target="_self" name="myFORM" id="myFORM" class="JoinMemForm"
		style="display: block;">
		<!--會員資料-->
		<fieldset>
			<legend class="join-acc">注册帐号</legend>
			<p style="position: relative; min-height: 1px;">
				<a style="position: absolute; top: 0px;"></a>
			</p>
			<!-- 帳號 -->
			<p id="js-username" style="display: block;">
				<label><span class="star" style="display: inline;">*&nbsp;</span>帐
					号：</label> <input type="text" name="accountR" id="accountR" value=""
					size="15" maxlength="11"><span
					class="memCash_text">帐号：请输入5-11个字元, 仅可输入英文字母以及数字的组合!!</span>
			</p>
			<!-- 密碼 -->
			<p id="js-password" style="display: block;">
				<label><span class="star" style="display: inline;">*&nbsp;</span>密
					码：</label> <input type="password" name="passwordR" id="passwordR" value=""
					class="password_adv" size="15" maxlength="12"> <span
					class="memCash_text">*密码规则：须为<font color="red"><b>6~12码英文或数字</b></font>且符合0~9或a~z字元
				</span>
			</p>
			<!-- 確認密碼 -->
			<p id="js-passwd" style="display: block;">
				<label title="确认密码"><span class="star"
					style="display: inline;">*&nbsp;</span>确认密码：</label> <input type="password"
					name="renewpassword" id="renewpassword" value="" size="15" maxlength="12">
					<span class="memCash_text">*密码规则：须与上面密码一致</span>
			</p>
			<!-- 驗證碼  -->
			<p id="js-rmNum"
				style="position: relative; margin-bottom: 40px; display: block;">
				<label><span class="star" style="display: inline;">*&nbsp;</span>验
					证 码：</label> <input name="verifyCodeR" type="text" id="verifyCodeR" size="4" onclick="reloadImg()" >
				<img id="rauthnumImg" src="#" 
					style="border: 1px solid #000;margin: -5px 5px;cursor: pointer;position: absolute;"
					alt="( 点选此处产生新验证码 )">
			</p>
		</fieldset>
		<!--次要資料-->
		<fieldset name="memdata" style="display: block;">
			<legend class="join-info">会员资料</legend>
			<p style="position: relative; min-height: 1px;">
				<a style="position: absolute; top: 0px;"></a>
			</p>
			<div id="reg_tb"></div>
			<!-- 真實姓名 -->
<!-- 			<p id="js-real_name" style="display: block;"> -->
<!-- 				<label><span class="star" style="display: inline;">*&nbsp;</span>真实姓名：</label> -->
<!-- 				<input type="text" name="real_name" id="real_name" value="" -->
<!-- 					size="15" maxlength="30"> <span class="memCash_text" -->
<!-- 					style="color: red;">必须与您的银行帐户名称相同，否则不能出款!</span> -->
<!-- 			</p> -->
			
		</fieldset>
		<!-- 確認/重設 -->
		<div id="memCash-confirm" align="center">
			<input type="button" name="OK2" id="OK2" class="joinform_submit" onclick="dlcommit()"
				value="确认"> <input type="reset" name="CANCEL2" id="CANCEL2"
				class="joinform_cancel" value="重设">
		</div>
	<script id="regconflst_tpl" type="text/html">
	{{each data as tl}}
		<p id="js-real_name">
				{{if tl.type==1}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					<input type="text" name="{{tl.key}}" id="{{tl.key}}" size="15">
				{{/if}}
				{{if tl.type==2}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					<select name="{{tl.key}}" id="{{tl.key}}" size="15">
					{{if tl.sourceLst}}
						{{each tl.sourceLst as sel i}}
							<option value="{{i+1}}">{{sel}}</option>
						{{/each}}
					{{/if}}
					</select>
				{{/if}}
				{{if tl.type==3}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					{{if tl.sourceLst}}
					{{each tl.sourceLst as rad i}}
						<input type="radio" name="{{tl.key}}" id="{{tl.key}}" size="15" value="{{i+1}}">{{rad}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==4}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					{{if tl.sourceLst}}
					{{each tl.sourceLst as chk i}}
						<input type="radio" name="{{tl.key}}" id="{{tl.key}}" size="15" value="{{i+1}}">{{chk}}</input>
					{{/each}}
					{{/if}}
				{{/if}}
				{{if tl.type==5}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					<textarea id="{{tl.key}}"></textarea>
				{{/if}}
				{{if tl.type==6}}
					<label><span class="star" style="display: inline;">*&nbsp;</span>{{tl.name}}：</label>
					<input type="password" name="{{tl.key}}" id="{{tl.key}}" size="15">
				{{/if}}
				{{$addValidateFiled tl}}
				{{if tl.type==1 && tl.key== 'userName'}}
					<span class="memCash_text" style="color: red;">必须与您的银行帐户名称相同，否则不能出款!</span>
				{{/if}}
				{{if tl.type==6}}
					<span class="memCash_text" style="color: red;">提款认证必须，请务必记住!</span>
				{{/if}}
		</p>
		{{/each}}
	</script>
		<!-- 備註 -->
		<div class="JM_content JM_content_b">
			<div id="joinmem">
				<div>
					<p>備註：</p>
					<ul style="margin-left: 25px;">
						<li>1.標記有&nbsp;<span class="star">*</span>&nbsp;者為必填項目。
						</li>
						<li>2.手機與取款密碼為取款金額時的憑證,請會員務必填寫詳細資料。</li>
						<li>3.若公司有其他活動會E-MAIL通知，請客戶填寫清楚</li>
						<li>4.請會員填寫真實資料，如查出非本人使用，一律不出款處理。</li>
					</ul>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
	var linkUrl  = location.href
	$(function(){
		// function getCookie(name){
		// 	var cookies = document.cookie;
		// 	var list = cookies.split(";");
		//
		// 	for (var i=0;i< list.length;i++){
		// 		var arr =list[i].split("=")
		// 		if (arr[0] == name){
		// 			return arr[1]
		// 		}
		// 	}
		// 	return ""
		// }
		if(linkUrl.indexOf('link') != -1) {
			var linkLen = linkUrl.indexOf('link')
			var str = linkUrl.substring(linkLen+5, linkLen+10)
			console.log(str)
			$("#promoCode").val(str)
			if ($("#promoCode").val){

				$("#promoCode").attr('disabled','disabled')

			}
		}
	})
</script>