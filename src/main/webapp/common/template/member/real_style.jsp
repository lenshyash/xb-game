<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
/* 視訊直播 - hover樣式 */
.ele-img-hover-effect {
	display: block;
}

.ele-live-wrap {
	max-width: 960px;
	padding: 3px 0 20px;
	margin: 0 auto;
}

.ele-live-wrap img {
	width: 100%;
	height: auto;
}

.ele-live-bg {
	background-color: #000;
}

.ele-live-menu-wrap {
	margin-bottom: 4px;
	background-color: #2A2A2A;
}

.ele-live-menu {
	position: relative;
	padding-left: 170px;
}

.ele-live-item {
	position: relative;
	float: left;
	width: 20%;
}

.ele-live-item a {
	display: block;
	height: 80px;
	line-height: 80px;
	color: #ACADB2;
	font-size: 16px;
	text-align: center;
	text-decoration: none;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	padding: 0 8px;
}

.live-menu-special .ele-live-item {
	width: 12.5%;
}

.live-menu-special .ele-live-item a {
	height: 40px;
	line-height: 40px;
}

.ele-live-item:before {
	position: absolute;
	top: 0;
	right: 0;
	width: 2px;
	height: 100%;
	content: "";
	background-color: #202020;
	border-left: solid 1px #3D3D3D;
}

.ele-live-item:after {
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 2px;
	content: "";
	background-color: #202020;
	border-bottom: solid 1px #3D3D3D;
}

.ele-live-drop {
	position: absolute;
	top: 0;
	left: 0;
	width: 170px;
	height: 100%;
	background:
		url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiP…B3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSJ1cmwoI2dyYWQpIiAvPjwvc3ZnPiA=);
	background: linear-gradient(45deg, #202020 50%, #3D3D3D 100%);
}

.ele-live-drop:before {
	position: absolute;
	top: 0;
	right: 0;
	width: 2px;
	height: 100%;
	content: "";
	background-color: #202020;
	border-left: solid 1px #3D3D3D;
}

.ele-live-drop:after {
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 2px;
	content: "";
	background-color: #202020;
	border-bottom: solid 1px #3D3D3D;
}

.live-type-0 .ele-live-drop:after, .live-type-1 .ele-live-drop:after,
	.live-type-2 .ele-live-drop:after {
	opacity: 0;
}

.ele-live-drop:hover .item-bbin:before {
	position: absolute;
	top: 0;
	left: 0;
	display: block;
	width: 130px;
	height: 77px;
	content: "";
	background: url("${base}/images/live_bbin_mask.png") no-repeat;
}

.ele-live-drop:hover .item-bbin:after {
	background-position: 0 100%;
}

.ele-live-drop .item-bbin {
	height: 100%;
	height: 80px;
	line-height: 80px;
	color: #ACADB2;
	font-size: 16px;
	text-align: center;
	text-overflow: ellipsis;
	padding: 0 8px;
	overflow: hidden;
	white-space: nowrap;
}

.ele-live-drop .item-bbin:after {
	position: absolute;
	top: 26px;
	right: 16px;
	width: 28px;
	height: 28px;
	content: "";
	background: url("${base}/images/icon_arrow_down.png") no-repeat;
}

.ele-live-menu-wrap .ele-live-submenu {
	position: absolute;
	top: 77px;
	left: 0;
	z-index: 5;
	width: 168px;
	font-size: 15px;
	background:
		url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiP…Igd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0idXJsKCNncmFkKSIgLz48L3N2Zz4g);
	background: linear-gradient(to right, #927443 0%, #B29057 50%, #927443 100%);
	padding-top: 3px;
}

.ele-live-submenu li {
	position: relative;
	background-color: #333;
}

.ele-live-submenu li:hover {
	background:
		url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiP…B3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSJ1cmwoI2dyYWQpIiAvPjwvc3ZnPiA=);
	background: linear-gradient(45deg, #202020 50%, #2B2A2A 100%);
}

.ele-live-submenu .ele-livehall {
	display: block;
	line-height: 56px;
	color: #9F9F9F;
	text-align: left;
	text-decoration: none;
}

.ele-live-submenu a:hover:before {
	position: absolute;
	top: 0;
	left: 0;
	display: block;
	width: 4px;
	height: 56px;
	content: "";
	background-color: #E0BB77;
}

.icon-livehall, .ele-livehall-name {
	display: inline-block;
	height: 33px;
	vertical-align: middle;
}

.icon-livehall {
	width: 44px;
	background: url("${base}/images/icon_livecasino_small.png") no-repeat;
	margin: 0 15px 0 16px;
}

.ele-livehall:hover .icon-livehall {
	background-position: 0 100%;
}

.ele-livehall-38 .icon-livehall {
	background-position: -44px 0;
}

.ele-livehall-38:hover .icon-livehall {
	background-position: -44px 100%;
}

.ele-livehall-39 .icon-livehall {
	background-position: -88px 0;
}

.ele-livehall-39:hover .icon-livehall {
	background-position: -88px 100%;
}

.ele-livehall-80 .icon-livehall {
	background-position: -132px 0;
}

.ele-livehall-80:hover .icon-livehall {
	background-position: -132px 100%;
}

.ele-livehall-49 .icon-livehall {
	background-position: -176px 0;
}

.ele-livehall-49:hover .icon-livehall {
	background-position: -176px 100%;
}

.ele-livehall-41 .icon-livehall {
	background-position: -220px 0;
}

.ele-livehall-41:hover .icon-livehall {
	background-position: -220px 100%;
}

.ele-livehall-name {
	width: 80px;
	line-height: 33px;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.ele-live-main-wrap {
	margin-bottom: 4px;
}

.ele-link-bbin {
	position: relative;
	display: block;
}

.bbin-livehall {
	display: inline-block;
	width: 78px;
	height: 98px;
	color: #9F9F9F;
	text-align: center;
	text-decoration: none;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	background-color: #202020;
	background-color: rgba(32, 32, 32, .5);
	border: solid 1px #626262;
	padding: 0 10px;
	margin: 8px 24px 8px 0;
}

.bbin-livehall:hover {
	color: #FFF;
	background-color: #202020;
	border: solid 1px #EDC580;
}

.ele-icon-bbin {
	width: 52px;
	height: 40px;
	background: url("${base}/images/livecasino_icon.png") no-repeat;
	margin: 13px auto;
}

.bbin-livehall:hover .ele-icon-bbin {
	background-position: 0 100%;
}

.bbin-livehall-38 .ele-icon-bbin {
	background-position: -52px 0;
}

.bbin-livehall-38:hover .ele-icon-bbin {
	background-position: -52px 100%;
}

.bbin-livehall-39 .ele-icon-bbin {
	background-position: -104px 0;
}

.bbin-livehall-39:hover .ele-icon-bbin {
	background-position: -104px 100%;
}

.bbin-livehall-80 .ele-icon-bbin {
	background-position: -156px 0;
}

.bbin-livehall-80:hover .ele-icon-bbin {
	background-position: -156px 100%;
}

.bbin-livehall-49 .ele-icon-bbin {
	background-position: -208px 0;
}

.bbin-livehall-49:hover .ele-icon-bbin {
	background-position: -208px 100%;
}

.bbin-livehall-41 .ele-icon-bbin {
	background-position: -260px 0;
}

.bbin-livehall-41:hover .ele-icon-bbin {
	background-position: -260px 100%;
}

.ele-btn-play {
	position: absolute;
	bottom: 26px;
	right: 24px;
	z-index: 2;
	display: block;
	width: 142px;
	height: 38px;
	background: url("${base}/images/live_bbin_play.png") no-repeat;
}

.ele-btn-play:hover {
	background-position: 0 100%;
}

.ele-bbin-rule-wrap {
	position: absolute;
	top: 0;
	left: 0;
	z-index: 2;
	display: none;
	width: 100%;
	height: 100%;
}

.ele-img-liverule {
	display: block;
}

.ele-live-rule-open, .ele-live-rule-close {
	position: absolute;
	top: 12px;
	right: 16px;
	z-index: 2;
	width: 41px;
	height: 41px;
	cursor: pointer;
}

.ele-live-rule-open:hover, .ele-live-rule-close:hover {
	background-position: 0 100%;
}

.ele-live-rule-open {
	background-image: url("${base}/images/btn_question.png");
}

.ele-live-rule-close {
	background-image: url("${base}/images/btn_back.png");
}

.ele-rule-text-wrap {
	position: absolute;
	top: 78px;
	right: 54px;
	color: #6D6D6D;
	text-align: right;
}

.ele-rule-text-wrap .ele-rule-title {
	color: #000;
	font-size: 45px;
	margin-bottom: 10px;
}

.ele-rule-text-wrap a {
	line-height: 38px;
	color: #494949;
	font-size: 14px;
	font-weight: bold;
	text-decoration: none;
}

.ele-bbin-rule-wrap a:hover {
	color: #2E2E2E;
}

.ele-rule-line {
	margin: 0 18px;
}

.ele-live-slider-wrap {
	position: relative;
	float: right;
	width: 24.6875%;
}

.js-ele-live-slider {
	height: 100%;
}

.js-ele-live-slider li {
	position: absolute;
	opacity: 0;
	visibility: hidden;
}

.js-ele-live-slider .fade-active {
	opacity: 1;
	visibility: visible;
}

.js-ele-live-slider a, .js-ele-live-slider img {
	display: block;
}

.ele-live-ctrol-wrap {
	position: absolute;
	bottom: 3px;
	right: 3px;
}

.ctrol-wrap-leftbg, .js-ele-live-ctrol, .ctrol-wrap-rightbg {
	float: left;
	width: 4px;
	height: 28px;
	background: url("${base}/images/slider_nav_bg.png") repeat-x;
}

.js-ele-live-ctrol {
	width: auto;
	line-height: 28px;
	background-position: 0 50%;
	padding: 0 6px;
}

.js-ele-live-ctrol li {
	display: inline-block;
	margin: 0 5px;
}

.js-ele-live-ctrol .ele-live-nav {
	display: block;
	width: 8px;
	height: 8px;
	cursor: pointer;
	background: url("${base}/images/slider_nav_circle.png") no-repeat;
}

.js-ele-live-ctrol .active .ele-live-nav, .js-ele-live-ctrol .ele-live-nav:hover
	{
	background-position: left bottom;
}

.ctrol-wrap-rightbg {
	background-position: 0 100%;
}

.livehall-row, .livehall-row-ad {
	float: left;
	width: 25%;
	margin-bottom: 4px;
}

.ele-live-special-wrap .livehall-row {
	width: 20%;
}

.live-type-0 .livehall-row-ad {
	display: none;
	width: 100%;
}

.livehall-row-lg, .live-type-1 .livehall-row-ad, .live-type-2 .livehall-row-ad
	{
	width: 50%;
}

.live-type-4 .livehall-row-ad, .ele-live-special-wrap .livehall-row-ad {
	display: none;
}

.livehall-playnow {
	position: relative;
	display: block;
	margin: 0 2px;
}

.img-tablet, .img-mobile {
	display: none;
}

.live-type-1 .ele-live-ad .img-mobile, .live-type-2 .ele-live-ad .img-mobile,
	.live-type-3 .ele-live-ad .img-tablet {
	display: block;
}

.ele-live-special-wrap .ele-live-ad, .ele-live-default-wrap .ele-live-ad
	{
	display: none;
	margin: 0 1px;
}

.live-type-1 .ele-live-ad, .live-type-2 .ele-live-ad {
	display: block;
	background-image: url("${base}/images/ad01_m.png");
}

.live-type-1 .ele-default-ad {
	background-image: url("${base}/images/ad02_m.png");
}

.live-type-2 .ele-live-ad {
	background-image: url("${base}/images/ad01_m.png");
}

.live-type-2 .ele-default-ad {
	background-image: url("${base}/images/ad02_m.png");
}

.live-type-3 .ele-live-ad {
	display: block;
	background-image: url("${base}/images/ad01_s.png");
}

.live-type-3 .ele-default-ad {
	background-image: url("${base}/images/ad02_s.png");
}

@media screen and (max-width:767px) {
	.img-web {
		display: none;
	}
	.ele-img-active {
		display: none;
	}
	.ele-live-menu {
		padding-left: 0;
	}
	.ele-live-drop {
		position: relative;
		float: left;
		width: 33.3%;
		height: 80px;
		background: #2A2A2A;
	}
	.ele-live-drop:hover .item-bbin:before, .ele-live-drop .item-bbin:after
		{
		display: none;
	}
	.ele-live-item, .live-menu-special .ele-live-item {
		width: 33.3%;
	}
	.live-menu-special .ele-live-item a {
		height: 80px;
		line-height: 80px;
	}
	.ele-live-special-wrap .ele-live-item-3:after, .ele-live-special-wrap .ele-live-item-4:after,
		.ele-live-special-wrap .ele-live-item-5:after, .live-type-0 .ele-live-drop:after,
		.live-type-1 .ele-live-drop:after, .live-type-2 .ele-live-drop:after,
		.live-type-3 .ele-live-item-3:after, .live-type-4 .ele-live-item-4:after
		{
		opacity: 0;
	}
	.live-type-3 .ele-live-item-1:after, .live-type-3 .ele-live-item-2:after
		{
		opacity: 1;
	}
	.ele-bbin-links-wrap, #ele-btn-play, .ele-live-slider-wrap {
		display: none;
	}
	.ele-live-default-wrap .livehall-row, .ele-live-special-wrap .livehall-row
		{
		width: 50%;
	}
	.live-type-2 .livehall-row-ad, .live-type-4 .livehall-row-ad {
		display: block;
		width: 100%;
	}
	.live-type-3 .livehall-row-ad, .ele-live-special-wrap .livehall-row-ad {
		display: block;
		width: 50%;
	}
	.ele-live-special-wrap .ele-live-ad, .ele-live-default-wrap .ele-live-ad,
		.live-type-0 .livehall-row-ad {
		display: block;
	}
	.live-type-2 .ele-live-ad, .live-type-4 .ele-live-ad {
		margin: 0;
	}
	.live-type-1 .ele-live-ad .img-mobile {
		display: none;
	}
	.ele-live-special-wrap .img-tablet, .ele-live-default-wrap .livehall-playnow .img-tablet,
		.live-type-0 .ele-live-ad .img-mobile, .live-type-1 .ele-live-ad .img-tablet,
		.live-type-3 .ele-live-ad .img-tablet, .live-type-4 .ele-live-ad .img-mobile
		{
		display: block;
	}
	.ele-live-bbin-inner {
		display: none;
	}
}

@media screen and (max-width:480px) {
	.ele-live-drop {
		height: 50px;
	}
	.ele-live-drop .item-bbin, .ele-live-item a, .live-menu-special .ele-live-item a
		{
		height: 50px;
		line-height: 50px;
	}
	.ele-live-default-wrap .livehall-row, .ele-live-special-wrap .livehall-row,
		.live-type-1 .livehall-row-ad, .live-type-3 .livehall-row-ad,
		.ele-live-special-wrap .livehall-row-ad {
		width: 100%;
	}
	.ele-live-special-wrap .livehall-playnow, .ele-live-default-wrap .livehall-playnow
		{
		margin: 0;
	}
	.ele-live-special-wrap .img-tablet, .ele-live-default-wrap .livehall-playnow .img-tablet,
		.live-type-1 .ele-live-ad .img-tablet, .live-type-3 .ele-live-ad .img-tablet
		{
		display: none;
	}
	.ele-live-special-wrap .img-mobile, .ele-live-default-wrap .livehall-playnow .img-mobile,
		.live-type-1 .ele-live-ad .img-mobile, .live-type-3 .ele-live-ad .img-mobile
		{
		display: block;
	}
	.ele-live-special-wrap .ele-live-ad, .ele-live-default-wrap .ele-live-ad
		{
		margin: 0;
	}
}

@media screen and (min-width:768px) {
	.live-type-0 .ele-live-main-wrap {
		margin-bottom: 0;
	}
	.ele-img-default {
		position: relative;
		z-index: 1;
	}
	.ele-img-active {
		position: absolute;
		top: 0;
		left: 0;
	}
	.ele-live-menu {
		min-height: 80px;
	}
	.ele-live-drop:after, .ele-live-item:after {
		opacity: 0;
	}
	.live-menu-special .ele-live-item:after {
		opacity: 1;
	}
	.ele-live-drop .item-bbin {
		cursor: pointer;
	}
	.ele-live-bbin-wrap {
		position: relative;
		float: left;
		width: 75%;
	}
	.ele-live-bbin-inner {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		transition: all .3s;
	}
	.ele-live-bbin-wrap:hover .ele-live-bbin-inner {
		opacity: 1;
	}
	.ele-bbin-links-wrap {
		position: absolute;
		top: 90px;
		right: 48px;
		z-index: 2;
		width: 380px;
	}
}
</style>
<div class="ele-live-wrap">
	<div class="ele-live-bg ele-live-special-wrap">
		<div id="ele-live-menu-wrap" class="ele-live-menu-wrap clearfix"
			style="display: none;">
			<ul class="ele-live-menu clearfix">
				<li id="ele-live-drop" class="ele-live-drop">
					<div id="item-bbin" class="item-bbin">BB视讯</div>
					<ul id="js-ele-live-submenu" class="ele-live-submenu"
						style="display: none;">
						<!-- 排除遊戲大廳賭神廳、超級多台、html5 -->
						<li><a id="ele-livehall-40"
							class="ele-livehall ele-livehall-40" href="###" title="多台下注"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">多台下注</div></a></li>
						<li><a id="ele-livehall-38"
							class="ele-livehall ele-livehall-38" href="###" title="快速厅"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">快速厅</div></a></li>
						<li><a id="ele-livehall-39"
							class="ele-livehall ele-livehall-39" href="###" title="旗舰厅"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">旗舰厅</div></a></li>
						<li><a id="ele-livehall-80"
							class="ele-livehall ele-livehall-80" href="###" title="竞咪厅"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">竞咪厅</div></a></li>
						<li><a id="ele-livehall-49"
							class="ele-livehall ele-livehall-49" href="###" title="包桌厅"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">包桌厅</div></a></li>
						<li><a id="ele-livehall-41"
							class="ele-livehall ele-livehall-41" href="###" title="金臂厅"><div
									class="icon-livehall"></div>
								<div class="ele-livehall-name">金臂厅</div></a></li>

					</ul>
				</li>
				<li class="ele-live-item ele-live-item-1"><a href="###"
					data-livehall="19">AG视讯</a></li>
				<li class="ele-live-item ele-live-item-2"><a href="###"
					data-livehall="22">欧博视讯</a></li>
				<li class="ele-live-item ele-live-item-3"><a href="###"
					data-livehall="24">OG视讯</a></li>
				<li class="ele-live-item ele-live-item-4"><a href="###"
					data-livehall="27">GD视讯</a></li>
				<li class="ele-live-item ele-live-item-5"><a href="###"
					data-livehall="26">SA视讯</a></li>
			</ul>
		</div>

		<div class="ele-live-main-wrap clearfix">
			<div class="ele-live-bbin-wrap">
				<c:choose>
					<c:when test="${isBbinOnOff eq 'on'}">
						<a class="ele-link-bbin"
							href="javascript: go('${base}/forwardBbin.do?type=live', '2');">
							<img class="ele-img-hover-effect ele-img-default"
							src="${base}/images/bbin_n.png" border="0"> <img
							class="ele-img-hover-effect ele-img-active"
							src="${base}/images/bbin_h.png" border="0">
						</a>
					</c:when>
					<c:otherwise>
						<a class="ele-link-bbin" href="javascript:alert('系统维护');"> <img
							class="ele-img-hover-effect ele-img-default"
							src="${base}/images/bbin_n.png" border="0"> <img
							class="ele-img-hover-effect ele-img-active"
							src="${base}/images/bbin_h.png" border="0">
						</a>
					</c:otherwise>
				</c:choose>
				<div class="ele-live-bbin-inner">
					<div class="ele-bbin-links-wrap">
						<!--排除遊戲大廳賭神廳、超級多台、html5 -->
						<c:choose>
							<c:when test="${isBbinOnOff eq 'on'}">
								<a id="bbin-livehall-40" class="bbin-livehall bbin-livehall-40"
									href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><div
										class="ele-icon-bbin"></div>多台下注</a>
								<a id="bbin-livehall-38" class="bbin-livehall bbin-livehall-38"
									href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><div
										class="ele-icon-bbin"></div>快速厅</a>
								<a id="bbin-livehall-39" class="bbin-livehall bbin-livehall-39"
									href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><div
										class="ele-icon-bbin"></div>旗舰厅</a>
								<a id="bbin-livehall-80" class="bbin-livehall bbin-livehall-80"
									href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><div
										class="ele-icon-bbin"></div>竞咪厅</a>
								<a id="bbin-livehall-49" class="bbin-livehall bbin-livehall-49"
									href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><div
										class="ele-icon-bbin"></div>包桌厅</a>
								<a id="bbin-livehall-41" class="bbin-livehall bbin-livehall-41"><div
										class="ele-icon-bbin"></div>金臂厅</a>
							</c:when>
							<c:otherwise>
								<a id="bbin-livehall-40" class="bbin-livehall bbin-livehall-40"
									href="javascript:alert('系统维护');"><div class="ele-icon-bbin"></div>多台下注</a>
								<a id="bbin-livehall-38" class="bbin-livehall bbin-livehall-38"
									href="javascript:alert('系统维护');"><div class="ele-icon-bbin"></div>快速厅</a>
								<a id="bbin-livehall-39" class="bbin-livehall bbin-livehall-39"
									href="javascript:alert('系统维护');"><div class="ele-icon-bbin"></div>旗舰厅</a>
								<a id="bbin-livehall-80" class="bbin-livehall bbin-livehall-80"
									href="javascript:alert('系统维护');"><div class="ele-icon-bbin"></div>竞咪厅</a>
								<a id="bbin-livehall-49" class="bbin-livehall bbin-livehall-49"
									href="javascript:alert('系统维护');"><div class="ele-icon-bbin"></div>包桌厅</a>
								<a id="bbin-livehall-41" class="bbin-livehall bbin-livehall-41"><div
										class="ele-icon-bbin"></div>金臂厅</a>
							</c:otherwise>
						</c:choose>

					</div>
					<a id="ele-btn-play" href="###"></a>
				</div>
			</div>
			<div class="ele-live-slider-wrap">
				<ul id="js-ele-live-slider" class="js-ele-live-slider"
					style="height: 488px;">
					<c:choose>
						<c:when test="${isBbinOnOff eq 'on'}">
							<li class="fade-active" style="transition: all 0.5s;"><a
								href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><img
									class="ele-live-slider-img" src="${base}/images/ad01_l.png"
									border="0"></a></li>
							<li class="" style="transition: all 0.5s;"><a
								href="javascript: go('${base}/forwardBbin.do?type=live', '2');"><img
									class="ele-live-slider-img" src="${base}/images/ad02_l.png"
									border="0"></a></li>
						</c:when>
						<c:otherwise>
							<li class="fade-active" style="transition: all 0.5s;"><a
								href="javascript:alert('系统维护');"><img
									class="ele-live-slider-img" src="${base}/images/ad01_l.png"
									border="0"></a></li>
							<li class="" style="transition: all 0.5s;"><a
								href="javascript:alert('系统维护');"><img
									class="ele-live-slider-img" src="${base}/images/ad02_l.png"
									border="0"></a></li>
						</c:otherwise>
					</c:choose>
				</ul>
				<div class="ele-live-ctrol-wrap">
					<div class="ctrol-wrap-leftbg"></div>
					<ul id="js-ele-live-ctrol" class="js-ele-live-ctrol">

					</ul>
					<div class="ctrol-wrap-rightbg"></div>
				</div>
			</div>
		</div>

		<div class="ele-live-default-wrap clearfix">
			<!-- 判斷基本欄位若餘1且為最後一個欄位時，使用大圖 -->

			<div class="livehall-row ">
				<c:choose>
					<c:when test="${isAgOnOff eq 'on'}">
						<a class="livehall-playnow livehall-playnow-ag "
							href="javascript: go('${base}/forwardAg.do', '1');">
					</c:when>
					<c:otherwise>
						<a class="livehall-playnow livehall-playnow-ag "
							href="javascript:alert('系统维护');">
					</c:otherwise>
				</c:choose>
				<img class="img-mobile" src="${base}/images/ag_l_n.png" border="0">
				<img class="img-tablet" src="${base}/images/ag_m_n.png" border="0">
				<img class="img-web ele-img-hover-effect ele-img-default"
					src="${base}/images/ag_s_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-active"
					src="${base}/images/ag_s_h.png" border="0"> </a>
			</div>
			<!-- 判斷基本欄位若餘1且為最後一個欄位時，使用大圖 -->

			<div class="livehall-row ">
				<c:choose>
					<c:when test="${isMgOnOff eq 'on'}">
						<a class="livehall-playnow livehall-playnow-allbet "
							href="javascript:go('${base}/forwardMg.do?gameType=1', '3');">
					</c:when>
					<c:otherwise>
						<a class="livehall-playnow livehall-playnow-allbet "
							href="javascript:alert('系统维护');">
					</c:otherwise>
				</c:choose>
				<img class="img-mobile" src="${base}/images/allbet_l_n.png"
					border="0"> <img class="img-tablet"
					src="${base}/images/allbet_m_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-default"
					src="${base}/images/allbet_s_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-active"
					src="${base}/images/allbet_s_h.png" border="0"> </a>
			</div>
			<!-- 判斷基本欄位若餘1且為最後一個欄位時，使用大圖 -->

			<div class="livehall-row ">
				<a class="livehall-playnow livehall-playnow-og "
					href="javascript:alert('暂未开放');"> <img class="img-mobile"
					src="${base}/images/og_l_n.png" border="0"> <img
					class="img-tablet" src="${base}/images/og_m_n.png" border="0">
					<img class="img-web ele-img-hover-effect ele-img-default"
					src="${base}/images/og_s_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-active"
					src="${base}/images/og_s_h.png" border="0">
				</a>
			</div>
			<!-- 判斷基本欄位若餘1且為最後一個欄位時，使用大圖 -->

			<div class="livehall-row ">
				<a class="livehall-playnow livehall-playnow-gd "
					href="javascript:alert('暂未开放');"> <img class="img-mobile"
					src="${base}/images/gd_l_n.png" border="0"> <img
					class="img-tablet" src="${base}/images/gd_m_n.png" border="0">
					<img class="img-web ele-img-hover-effect ele-img-default"
					src="${base}/images/gd_s_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-active"
					src="${base}/images/gd_s_h.png" border="0">
				</a>
			</div>
			<!-- 判斷基本欄位若餘1且為最後一個欄位時，使用大圖 -->

			<div class="livehall-row livehall-row-lg">
				<a class="livehall-playnow livehall-playnow-sa "
					href="javascript:alert('暂未开放');"> <img class="img-mobile"
					src="${base}/images/sa_l_n.png" border="0"> <img
					class="img-tablet" src="${base}/images/sa_m_n.png" border="0">
					<img class="img-web ele-img-hover-effect ele-img-default"
					src="${base}/images/sa_s_n.png" border="0"> <img
					class="img-web ele-img-hover-effect ele-img-active"
					src="${base}/images/sa_s_h.png" border="0">
				</a>
			</div>
			<div class="livehall-row-ad">
				<!-- 固定廣宣 -->
				<!-- 活動廣宣 -->
				<a class="ele-live-ad" href="###"> <img class="img-mobile"
					src="${base}/images/ad01_m.png" border="0"> <img
					class="img-tablet" src="${base}/images/ad01_s.png" border="0">
				</a>
			</div>
		</div>
	</div>
</div>

<!--未登入彈跳視窗-->
<script>
	(function() {
		'use strict';

		var liveTop = {
			login_tips : "请先登入",
			ls_api : "",
			$elements : $('.ele-live-item'),
			is_login : "N",
			init : function() {
				liveTop.OpenDropmenu();
				liveTop.OpenLiveLogin();
				liveTop.LiveWrapType();
				liveTop.LiveImageEffect();
				liveTop.LiveSlider();
				liveTop.LiveSliderPic();
				liveTop.LiveAd();
				liveTop.LiveOpenLiverule();
				$(window).resize(function() {
					liveTop.OpenDropmenu();
					liveTop.LiveImageEffect();
					liveTop.LiveSliderPic();
					liveTop.LiveCloseLiverule();
				});
			},
			/**
			 *
			 * 下拉子選單
			 **/
			OpenDropmenu : function() {
				$('.ele-live-drop').unbind("mouseenter mouseleave");
				if ($(window).width() > 768) {
					$('.ele-live-drop').bind(
							{
								mouseenter : function() {
									$(this).find('#js-ele-live-submenu').stop(
											true, true).slideDown(300);
								},
								mouseleave : function() {
									$(this).find('#js-ele-live-submenu').stop(
											true, true).slideUp(300);
								}
							});
				}
			},
			/**
			 *
			 * 開啟登入彈窗
			 **/
			OpenLiveLogin : function() {
				if (liveTop.is_login === 'Y') {
					return;
				}
				$('.ele-live-bg')
						.on(
								'click',
								'.ele-live-bg a',
								function() {
									if ($(this).hasClass("ele-item-liverule")) {
										return;
									}
									(liveTop.ls_api == true) ? alert(liveTop.login_tips)
											: loginWrap.showUp();
									event.preventDefault();
								});
			},
			/**
			 *
			 * 視訊大廳寬度呈現方式
			 **/
			LiveWrapType : function() {
				var live_length = $('.ele-live-item').length;

				// 開放視訊大廳六家以上(不含BB視訊)，.ele-live-menu的遊戲連結縮小
				if (live_length > 5) {
					$(".ele-live-menu").addClass("live-menu-special");
				}

				// 特例 - 開放視訊大廳為五家(不含BB視訊)
				if (live_length === 5) {
					$(".ele-live-bg").addClass("ele-live-special-wrap");
				} else {
					// 設定欄位的呈現方式
					switch (live_length % 4) {
					case 1:
						$(".ele-live-bg").addClass('live-type-1');
						break;
					case 2:
						$(".ele-live-bg").addClass('live-type-2');
						break;
					case 3:
						$(".ele-live-bg").addClass('live-type-3');
						break;
					default:
						if (live_length === 0) {
							$(".ele-live-bg").addClass('live-type-0');

						} else {
							$(".ele-live-bg").addClass('live-type-4');
						}
					}
				}
			},
			/**
			 *
			 * 圖片hover效果
			 **/
			LiveImageEffect : function() {
				$(".ele-live-bbin-wrap, .livehall-playnow").unbind("hover");
				if ($(window).width() > 768) {
					$(".ele-live-bbin-wrap, .livehall-playnow").hover(
							function() {
								$(this).find(".ele-img-default").stop(true,
										true).animate({
									opacity : "0"
								}, 300);
							},
							function() {
								$(this).find(".ele-img-default").stop(true,
										true).animate({
									opacity : "1"
								}, 300);
							});
				}
			},
			/**
			 *
			 * 輪播圖
			 **/
			LiveSlider : function() {
				var slider = $("#js-ele-live-slider"), sliderArray = $("#js-ele-live-slider li"), sliderLength = sliderArray.length, index = 0, sliderNavWrap = $("#js-ele-live-ctrol"), sliderNav = "<li><span class='ele-live-nav'></span></li>", timer;

				$('.ele-live-slider-img').load(
						function() {
							$("#js-ele-live-slider").css("height",
									$(".ele-live-slider-img").height());
						});

				// 輪播圖
				slider.find(sliderArray).css('transition', 'all .5s').eq(0)
						.addClass('fade-active');

				// 輪播切換
				for (var i = 0; i < sliderLength; i++) {
					$(sliderNavWrap).append(sliderNav);
				}

				sliderNavWrap.find("li").eq(0).addClass("active");

				$("#js-ele-live-ctrol li").on(
						"click",
						function() {
							clearTimer(timer);
							setTimer();

							index = $(this).index();

							$(sliderArray).removeClass('fade-active').eq(index)
									.addClass('fade-active');

							$("#js-ele-live-ctrol li").removeClass('active')
									.eq(index).addClass('active');
						});

				var setTimer = function() {
					timer = setInterval(function() {
						next(index);
					}, 8000);
				}

				var clearTimer = function() {
					window.clearInterval(timer)
				}
				setTimer();

				function next(id) {
					clearTimer(timer);
					setTimer();

					// 讀取下一張圖片
					index = (id + 1);
					$(sliderArray).removeClass('fade-active');
					$("#js-ele-live-ctrol li").removeClass('active');

					// 判斷是否為最後一張圖片
					if (index === sliderLength) {
						// 若為最後一張圖片,則將index值設為0
						index = 0;
						$(sliderArray).eq(index).addClass('fade-active');
						$("#js-ele-live-ctrol li").eq(index).addClass('active');
						return;
					}

					$(sliderArray).eq(index).addClass('fade-active');
					$("#js-ele-live-ctrol li").eq(index).addClass('active');
				}
			},
			/**
			 *
			 * 重設輪播圖高度
			 **/
			LiveSliderPic : function() {
				$("#js-ele-live-slider").css("height",
						$(".ele-live-slider-img").height());
			},
			/**
			 *
			 * 補位廣宣圖
			 **/
			LiveAd : function() {
				// 判斷有活動廣宣圖時，移除預設廣宣圖
				if ($(".ele-live-ad").length > 1) {
					$(".ele-default-ad").remove();
				}
			},
			/**
			 *
			 * 開啟遊戲規則
			 **/
			LiveOpenLiverule : function() {
				$("#ele-live-rule-open").on(
						"click",
						function() {
							if ($(window).width() > 768) {
								$(".ele-bbin-rule-wrap")
										.css("display", "block").stop(true,
												true).animate({
											opacity : "1"
										}, 300);
							}
						});

				$("#ele-live-rule-close").on("click", function() {
					liveTop.LiveCloseLiverule();
				});
			},
			/**
			 *
			 * 關閉遊戲規則
			 **/
			LiveCloseLiverule : function() {
				$(".ele-bbin-rule-wrap").css("display", "none")
						.stop(true, true).animate({
							opacity : "0"
						}, 300);
			}
		};

		liveTop.init();
	}());
</script>
