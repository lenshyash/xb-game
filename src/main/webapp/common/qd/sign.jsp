<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="p" uri="/permission"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>每日签到-${_title }</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="width=device-width">
<link rel="shortcut icon" href="${base}/common/qd/images/favicon.ico">
<link href="${base}/common/qd/css/ztBase.css" rel="stylesheet">
<link rel="stylesheet" href="${base}/common/qd/css/sign.css">
<script src="${base}/common/qd/js/hm.js"></script>
<script src="${base}/common/qd/js/jquery-1.9.1.min.js"></script>
<style>
.licaipop {
	position: fixed;
	left: 0;
	top: 0;
	z-index: 900;
	width: 100%;
	height: 100%
}

.licaipop .f-content {
	position: absolute;
	left: 50%;
	top: 50%;
	margin: -154px 0 0 -382px;
	width: 765px;
	height: 308px;
	background: url(${base}/common/qd/images/tca.png) no-repeat
}

.licaipop .mask {
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	background-color: #000;
	opacity: .8;
	filter: alpha(opacity = 80);
	-ms-filter: "alpha(Opacity=80)";
	filter: alpha(Opacity = 80)
}

.licaipop .f_close {
	position: fixed;
	left: 50%;
	top: 50%;
	color: #fff;
	font-size: 24px;
	color: #fffc00;
	margin: -104px 0 0 -382px;
	width: 765px;
	height: 258px;
	cursor: pointer;
	text-indent: -2000px;
	overflow: hidden;
	text-align: left;
}

.licaipop .o_close {
	position: absolute;
	right: 0;
	top: 0;
	width: 50px;
	height: 50px;
	margin-top: 0px;
	cursor: pointer
}

.licaipop .f_close:hover {
	color: #fffe8c
}
</style>
</head>
<body>
	<!-- 登录框 -->
	<div class="pop-login" style="display:none;">
		<div class="mask"></div>
		<div class="wrapper" style="height:374px;">
		<div class="title">会员登录<span class="close">X</span></div>
	    <div class="con">
	        <form name="popup_login_form" id="popup_login_form" method="post">
	        <div class="br txt">
				<label>账号：</label><span style="text-indent: -2000px;">帐号</span>
				<input name="Username" id="account" type="text">
				<p id="popup_username_msg"></p>
			</div>
			<div class="br pas">
				<label>密码：</label><span>请输入您的密码</span>
				<input name="Password" id="password" type="password" maxlength="16">
	            <!-- <p id ="verifyHtml1" class="clearfix" style="display:block;">
	      
	            </p> -->
	            <p id="popup_password_msg"></p>
			</div>
			<div class="br pas" id="verifyHtml1" style="display:;padding-left: 60px; border-left-width: 0px; margin-left: -10px;">
				<label>验证码：</label>
			 	<input name="verify" id="verifyCode" type="text" maxlength="5" style="margin-top:0px;width:100px;">
			 	
			 	<img style="cursor:pointer;width:150px;height:40px;margin-top:-5px;" src="${base }/verifycode.do" onclick="refreshVerifyCode(this)" id="vPic">
	       		<p id="geetest_verify_msg"></p>
	       </div>
			<div class="br sub">
				<input type="button" value="登录" id="popupLoginButton" onclick="doLogin();">
	        </div>
	        </form>
	    </div>
	  </div>
	</div>
	<!-- 登录框end -->
	<div class="top" style="display: none;">
		<div class="section">
			<a class="logo" href="https://www.ttz.com/">天天钻</a>
			<div class="login">
				<div id="loginBefore" style="display: block;" class="login-before">
					<span>您好，欢迎来到天天钻！</span>请 <a target="_blank" href="#"
						id="header_login">[登录]</a> <a target="_blank" href="#">[注册]</a>
				</div>
			</div>

			<div class="right" style="display: none;">
				<a target="_blank" href="#">首页</a> <a target="_blank" href="#">新人任务</a>
				<a target="_blank" href="#">游戏平台</a> <a target="_blank" href="#">打码平台</a>
				<a target="_blank" href="#">理财专区</a> <a target="_blank" href="#">推荐好友</a>
				<a target="_blank" href="#">兑换商城</a>
			</div>
		</div>
	</div>


	<div class="main">
<!-- 		<button class="dj-btn ">查看签到公式</button> -->
<!-- 		<button class="dj-btn1">查看等级公式</button> -->
		<div class="qdgz">
			
		</div>
		<div id="signSt" class="sign-st">
			<marquee direction="left" scrollamount="3" loop="infinite"
				onmouseover="this.stop()" onmouseout="this.start()">
			<div class="sign-st-w">
				<div class="roll-item">
					<h3>y*bin55</h3>
					<div class="br">
						签到领取了：<em>30621</em><span class="diamond-icon"></span>
					</div>
				</div>
				<div class="roll-item">
					<h3>h*ohao2012519</h3>
					<div class="br">
						签到领取了：<em>30892</em><span class="diamond-icon"></span>
					</div>
				</div>
			</div>
			</marquee>
		</div>
		
		
			<div class="section head">
				<p:login login="false">
					<img src="${base}/common/qd/images/taskpic.jpg" style="display: none">
					<div class="un-login">
						<button class="popLogin">登录立即签到领黄钻吧！</button>
<%-- 						<p> --%>
<%-- 							没有帐号,<a target="_blank" href="${base }/regpage.do">立即注册</a> --%>
<%-- 						</p> --%>
					</div>
				</p:login>
				
				<p:login login="true">
					<div class="info">
					<c:if test="${signed}">
					    <div class="br text">
					       <span>昵称: </span> ${signAccount }      
					       <span>已连续签到: </span>${signCount}<span>天
					    </div>
					    <br>
					    <div class="br btn">
					      <button class="singedd">已经签到</button>
					     </div>
					 </c:if>
					 <c:if test="${signed == false}">
					    <div class="br text">
					       <span>昵称: </span> ${signAccount }      
					       <span>已连续签到: </span>${signCount}<span>天
					    </div>
					    <br>
					 	 <div class="br btn" onclick="sign()"><a class="signinNow">还未签到</a></div>
					 </c:if>
				  </div>
				</p:login>
			</div>
		<!-- 广告位 -->

		<div class="section bar-progress">
			<div class="barbar" style="display: none;">
				<a target="_blank" href="http://sc.ttz.com/Task/taskHall">快速赚经验：在天天商城进行金钻兑换黄钻，10000金钻奖励500网站经验！</a>
			</div>
			<div class="nav">
				<span id="pre" class="pre"></span> 
				<span id="next" class="next"></span>
			</div>
			<div class="con calendar">
				<div id="scroll">
					<div id="group" class="group clearfix" style="width: 3920px;">						
					</div>
				</div>
			</div>
		</div>

		<div id="table" class="table"
			style="left: 1283.5px; top: 255px; display: none;">
			<h2>等级公式说明</h2>

			<div class="table-wrapper">
				<table>
					<thead>
						<tr>
							<td>用户等级</td>
							<td>公式系数</td>
						</tr>
					</thead>
					<tbody id="activRuleList_box">
						<tr>
							<td class="th">LV.1</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.2</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.3</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.4</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.5</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.6</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.7</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.8</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.9</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
						<tr>
							<td class="th">LV.10</td>
							<td>添财宝余额奖励 ≈ 黄钻总数 x 0.03%</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2"><span
								onclick="activRuleList('pre');">上一页 </span> <b
								id="activRuleListPage" style="color: red;">1</b>/6 <span
								onclick="activRuleList('next');">下一页</span></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>


		<div id="table1" class="table">
			<h2>连续签到公式</h2>

			<div class="table-wrapper">
				<table>
					<thead>
						<tr>
							<td>连续天数</td>
							<td>公式系数</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="th">签到1天</td>
							<td>1 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续2天</td>
							<td>2 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续3天</td>
							<td>3 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续4天</td>
							<td>4 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续5天</td>
							<td>5 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续6天</td>
							<td>6 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续7天</td>
							<td>7 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续8天</td>
							<td>8 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续9天</td>
							<td>9 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续10天</td>
							<td>10 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续11天</td>
							<td>11 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续12天</td>
							<td>12 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续13天</td>
							<td>13 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续14天</td>
							<td>14 × 您的等级</td>
						</tr>
						<tr>
							<td class="th">连续15天</td>
							<td>15 × 您的等级</td>
						</tr>
						<tr>
							<td class="th" style="">15天以上</td>
							<td>15 × 您的等级</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	
</div>
		<script>
</script>

		<div class="f">
			<div class="section footer copy-right mt38">
				<div class="br">
					<p>
						Copyright 2014-2018 <a href="#">${_title }</a>
						<span class="cnzz">
							Reserved
						</span>
					</p>
				</div>
				<div class="br">
					<img src="${base}/common/qd/images/rz03.gif">
					<img src="${base}/common/qd/images/hangye_bottom_large.png">
					<img src="${base}/common/qd/images/cert.jpg" border="0">
					<img src="${base}/common/qd/images/hy_124x47.png">
				</div>
			</div>

		</div>
		<!-- 广告位：全站左侧-方形广告位 -->
		<div class="setDisplay">
			<a class="f-expend" id="customerServiceUrlLink" style="display: block;" href="${kfUrl }" target="black"></a>
		</div>
<script>
  <p:login login="true">
  	var records = ${record};  	
  </p:login>
  $(function () {
	  zxqd();
	  qdgz();
    var controller_name = 'Signin';
    var paramAdverPage = "";
    if(paramAdverPage == 'experience'){controller_name = 'Game';}
    if(paramAdverPage == 'questions'){controller_name = 'Game';}
    switch (controller_name) {
      case 'Index':
        $(".home").addClass('active');
        break;
      case 'Course':
        $(".Course").addClass('active');
        break;
      case 'Task':
        $(".newtask").addClass('active');
        break;
      case 'Game':
        $(".try").addClass('active');
        break;
      case 'Captcha':
        $(".paid").addClass('active');
        break;
      case 'Adver':
        $(".ad").addClass('active');
        break;
      case 'Friend':
        $(".recommend").addClass('active');
        break;
      case 'Goods':
        $(".luck").addClass('active');
        break;

      case 'Shike':
    	$(".sk").addClass('active');
        break;
      case 'Luck28':
        $(".yz").addClass('active');
        break;
      case 'Vip':
        $(".vipm").addClass('active');
        break;
      case 'Go':
    	  $(".go").addClass('active');
          break;
	  case 'Lottery':
        $(".zx").addClass('active');
        break;
      case 'TaskCenter':
        $(".taskcenter").addClass('active');
        break;
    }
  });
  
  
	function refreshVerifyCode() {
		var time = (new Date()).getTime();
		$("#vPic").attr("src","${base}/verifycode.do?time=" + time);
	}

	function doLogin() {
		var account = $.trim($("#account").val());
		var password = $("#password").val();
		if (!account) {
			alert("用户名不能为空！");
			return;
		}
		if (!password) {
			alert("密码不能为空！");
			return;
		}
		var verifyCode = $("#verifyCode").val();
		if(!verifyCode){
			alert("请输入验证码！");
			return;
		}		
		var data = {};
		data["password"] = password;
		data["account"] = account;
		data["verifyCode"] = verifyCode;
		$.ajax({
			url : "${base}/login.do",
			type : "post",
			data : data,
			success : function(result, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					//parent.location.href = '${base}' || '/';
					parent.location.reload();
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
					reVerifyCode();
				} else if (ceipstate == 3) { // 业务异常
					alert(result.msg);
					reVerifyCode();
				}
			}
		});
	}
	
	var signInNow = false;
	function sign(){
		if(!signInNow){
			signInNow = true;
		}else{
			return;
		}
		$.ajax({
			url : "${base}/sign/signIn.do",
			type : "post",
			success : function(result, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					alert("签到成功，获取"+result.score+"积分");
					signDay();
					parent.location.reload();
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					alert(result.msg);
					parent.location.reload();
				}
			}
		});
	}
	function signDay(){
		var y = new Date().getFullYear();
		var m = new Date().getMonth()+1; 
		var month = parseInt(m, 10);
	  	var d= new Date(y, month, 0);
	  	var day = d.getDate()
	  	$.ajax({
			url : "${base}/sign/signByMonth.do",
			type : "post",
			data:{
				signYear: y,
				signMonth: m,
				signDay: day
			},
			success : function(today) {

			}
		});
	}
	/* /sign/signByMonth.do 
	var y = new Date().getFullYear();
		var m = new Date().getMonth()+1; 
		var month = parseInt(m, 10);
	  	var d= new Date(y, month, 0);
	  	var day = d.getDate()
	  	data:{
				signYear: y,
				signMonth: m,
				signDay: day
			},
	  	*/
	  	function zxqd(){
			var y = new Date().getFullYear();
			var m = new Date().getMonth()+1; 
			var month = parseInt(m, 10);
		  	var d= new Date(y, month, 0);
		  	var day = d.getDate()
		  	$.ajax({
				url : "${base}/sign/signByMonth.do",
				type : "post",
				data:{
					signYear: y,
					signMonth: m,
					signDay: day
				},
				success : function(today) {
					$('.toMonth').each(function(index){//页面展示两个月份
						if(index == 1){
						var thismos = $(this).html();//日历月份
						if(thismos.length == 11){
							var mos = thismos.substring(5,6);
							if(parseInt(mos) == m){//当月月份		
								$(this).parent().next().next().find('span').each(function(){
									var tian = $(this).text();
									var ty = typeof(tian);
									var counts = 0;							
									if(tian.length>1 && tian.substring(0,1) == '今'){
										tian = new Date().getDate();
									};
									if(tian < 10){
										tian = '0' + tian
									}
									for(var i=0;i<today.length;i++ ){
										var signdays = today[i].signDay;
										if(Number(tian) == Number(signdays)){
											counts++;
											$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signY.png" style="position: absolute;width: 62px;top: -4px;left: 39px;">');
										};
									};
									if(counts == 0 && tian < new Date().getDate() && Number(tian) && ${isLogin}) { 
										$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signN.png" style="position: absolute;width: 69px;top: -7px;left: 36px;">');
									};
								});			
							}else{//当月月份
								/* $.ajax({
									url : "${base}/sign/signByMonth.do",
									type : "post",
									data:{
										signYear: y,
										signMonth: m - 1,
										signDay: day
									},
									success : function(utoday) {
										console.log(mos)
										$(this).parent().next().next().find('span').each(function(){
											var tian = $(this).text();
											var ty = typeof(tian);
											var counts = 0;
											if(!utoday){
												$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signN.png" style="position: absolute;width: 69px;top: -7px;left: 36px;">');
											}else{
												for(var i=0;i<utoday.length;i++ ){
													var signdays = utoday[i].signDay;
													if(tian == signdays){
														counts++;
														$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signY.png" style="position: absolute;width: 62px;top: -4px;left: 39px;">');
													}
												}
												if(counts == 0 && Number(tian)) { 
													$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signN.png" style="position: absolute;width: 69px;top: -7px;left: 36px;">');
												}
											}
											
										});	
									}
								}) */
							}
						}else{//mos.length == 12
							/* console.log('月 ',thismos.substring(5,7)) */
							var mos = thismos.substring(5,7);
							if(mos == m){//当月月份
								$(this).parent().next().next().find('span').each(function(){
									var tian = $(this).text();
									var ty = typeof(tian);
									var counts = 0;
									if(tian.length>2 && tian.substring(0,1) == '今'){
										tian = new Date().getDate();
									};
									if(tian < 10){
										tian = '0' + tian
									}
									for(var i=0;i<today.length;i++ ){
										var signdays = today[i].signDay;
										if(tian == signdays){
											counts++;
											$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signY.png" style="position: absolute;width: 62px;top: -4px;left: 39px;">')
										};
									};
									if(counts == 0 && tian < new Date().getDate() && Number(tian) && ${isLogin}) { 
										$(this).html('<i class="p">'+tian+'</i><img src="${base}/common/qd/images/signN.png" style="position: absolute;width: 69px;top: -7px;left: 36px;">');
									};
								});	
						};
						/* for(var i=0;i<today.length;i++ ){
							var signdays = today[i].signDay;
							
						} */
						}
						}
					});
					
				}
			});
			$.ajax({
				url:"${base}/sign/zxqd.do",
				dataType:"json",
				success:function(j){
					if(j!=null){
						html='';
						for(var i=0;i<j.length;i++){
							j[i].memberName = j[i].memberName.substring(0,4);					
							html+='<div class="roll-item">';
							html+='<h3>'+j[i].memberName+'****</h3>';
							html+='<div class="br">';
							html+='签到领取了：<em>'+j[i].score+'</em><span class="diamond-icon"></span>';
							html+='</div>';
							html+='</div>';
						}
						$(".sign-st-w").html(html);
					}
				}
			});
		}
	
	//签到规则
	function qdgz(){
		$.ajax({
			url : "${base}/getConfig/getArticle.do",
			data:{code:17},
			type : "post",
			dataType : 'json',
			success : function(r) {
				var col='';
				var data =JSON.stringify(r);
				if(r.length>0){
					/* var content;
					for(var i=0;i<r.length;i++){
						 content+=r[i].content;
						}
					content=content.replace(/undefined/,"");
					content=content.replace(new RegExp("<p>","g"),"");
					content=content.replace(new RegExp("</p>","g"),""); */
					col="<button type='button' onclick='alertQdgz("+data+")' style='background-color:black;color:white;font-size:20px;'>签到规则</button>";
					var html = "<div id='qdmodule' style='display:none;top:0;left:0;right:0;bottom:0;z-index:99999;position:fixed;background:rgba(0,0,0,0.5);align-items:center;justify-content:center;'><img onclick='hidenImg()' src='${base}/common/qd/images/close.png' style='position:fixed;top:10px; right:10px;width:50px;cursor: pointer;transition:  0.3s;'><div id='qdgzImg'></div>"
				}
				/* for(var i=0;i<r.length;i++){
					col+=r[i].content+"&nbsp;&nbsp;&nbsp;";
				} */
				$(".qdgz").html(col);
				$(".main").append(html);
			}
		});
	}
	
	function alertQdgz(r){
		r = r[0].content
		document.getElementById("qdmodule").style.display = "flex"
		var img=document.getElementById("qdgzImg");
		img.innerHTML=r;
	}
	
	function hidenImg(){
		document.getElementById("qdmodule").style.display = "none"
		/* var img=document.getElementById("qdgzImg");
		if(value!=null){
			img.style.display=value;
			return ;
		}
		
		if(img.style.display=="none"){
			img.style.display="block";
		}else{
			img.style.display="none";
		} */
	}
</script>
		<!--[if lt IE 10]>
<script>
  $(".try-game").on("mouseenter", ".item", function () {
    var obj = $(this).find(".hover")
    obj.css("top", "100%");
    obj.animate({"top": 0}, 500,function(){
    });
  });
  $(".try-game").on("mouseleave", ".item", function () {
    var obj = $(this).find(".hover")
    obj.animate({"top": "100%"}, 500,function(){
      //console.log($(this).queue());
        $(this).stop(true, true)

    });
  })
</script>
<![endif]-->
		<!--<script src="https://www.ttz.com/scripts/looper/looper.min.js"></script>-->
		<script src="${base}/common/js/artTemplate/template.js"></script>
		<script src="${base}/common/qd/js/sign.js"></script>
	</div>
</body>
</html>

<script type="text/html" id="DATA_TR">
<div class="panel">
	<div class="month">
		<em id="today" class="toMonth">{{title}}</em>
	</div>
	<div class="week clearfix">
		<span>MON</span><span>TUE</span><span>WED</span><span>THU</span><span>FRI</span><span>SAT</span><span>SUN</span>
	</div>
	
	<div class="day clearfix">
		{{each days as day}}
			{{if day.isToday}}
				<span class="signed today" id="date_str_{{day.date}}"><b>今</b><i class="p">{{day.index}}</i></span>
			{{/if}}
			{{if day.isToday != true}}
				<span class="p" id="date_str_{{day.date}}">{{day.index}}</span>
			{{/if}}
		{{/each}}
	</div>
	
</div>
</script>
