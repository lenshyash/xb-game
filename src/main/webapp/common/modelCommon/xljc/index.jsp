<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${_title }--线路检测</title>
<link href="${base}/common/modelCommon/xljc/css/style.css" rel="stylesheet" type="text/css">
<link href="${base}/common/modelCommon/xljc/css/animate.css" rel="stylesheet" type="text/css">
<link href="${base}/common/modelCommon/xljc/css/med.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${base}/common/modelCommon/xljc/js/jquery.min.js.js"></script>

</head>
<body>
	<div class="header">
    	<div class="a_auto">
        	<div class="head">
            	<div class="head_1"><img src="${base}/images/logo.png" style="width: 300px;height: 100px;margin-top: -18px;"></div>
                <div class="head_3">
                    <a target="_blank" href="${kfUrl }">
                        <img src="${base}/common/modelCommon/xljc/images/img3.png" alt=""></a><a target="_blank" href="${kfUrl }">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="bar">
    	<div class="bar_1"><img src="${base}/common/modelCommon/xljc/images/img4.jpg" alt=""></div>
        <div class="bar_2">
        	<div class="a_auto">
        	<div class="bar_n">
            	<ul id="xljc_domain">
            		<!--
                	<li class="li_1">
                    	<a href="https://pc26067.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            </a>
                            <b>
						<a target="_blank" href="https://pc26067.com/"><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></a></b><a href="https://pc26067.com/" title="" target="_blank">
                            <div class="am_1">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img12.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img21.png" alt=""></h4>
                                <p>pc26067.com</p>
                                <input type="text" class="pus" id="sudu1" value="">
                                <em></em>
                                <span>香港线路</span>
                            </div>
                        </a>
                    </li>
                    <li class="li_2">
                    	<a href="https://www.cp26067.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            </a>
                            <b>
						<a target="_blank" href="https://www.cp26067.com/"><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></a></b><a href="https://www.cp26067.com/" title="" target="_blank">
                            <div class="am_2">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img13.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img22.png" alt=""></h4>
                                <p>cp26067.com</p>
                                <input type="text" class="pus" id="sudu2" value="">
                                <em></em>
                                <span>全部线路</span>
                            </div>
                        </a>
                    </li>
                    <li class="li_3">
                    	<a href="https://f26067.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            <b><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></b>
                            <div class="am_3">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img14.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img23.png" alt=""></h4>
                                <p>f26067.com</p>
                                <input type="text" class="pus" id="sudu3" value="">
                                <em></em>
                                <span>全部线路</span>
                            </div>
                        </a>
                    </li>
                    <li class="li_4">
                    	<a href="https://www.ssc26067.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            </a>
                            <b>
						<a target="_blank" href="https://ssc26067.com/"><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></a></b><a href="https://ssc26067.com/" title="" target="_blank">
                            <div class="am_4">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img15.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img24.png" alt=""></h4>
                                <p>ssc26067.com</p>
                                <input type="text" class="pus" id="sudu4" value="">
                                <em></em>
                                <span>广东线路</span>
                            </div>
                        </a>
                    </li>
                    <li class="li_5">
                    	<a href="https://pk26067.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            <b><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></b>
                            <div class="am_5">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img16.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img25.png" alt=""></h4>
                                <p>pk26067.com</p>
                                <input type="text" class="pus" id="sudu5" value="">
                                <span>美国线路</span>
                            </div>
                        </a>
                    </li>
                    <li class="li_6">
                    	<a href="https://26067k3.com/" title="" target="_blank">
                        	<i><img src="${base}/common/modelCommon/xljc/images/img10.png" alt=""></i>
                            </a>
                            <b>
						<a target="_blank" href="https://26067k3.com/"><img src="${base}/common/modelCommon/xljc/images/img11.png" alt=""></a></b><a href="https://26067k3.com/" title="" target="_blank">
                            <div class="am_6">
                                <h3><img src="${base}/common/modelCommon/xljc/images/img17.png" alt=""></h3>
                                <h4><img src="${base}/common/modelCommon/xljc/images/img26.png" alt=""></h4>
                                <p>26067k3.com</p>
                                <input type="text" class="pus" id="sudu6" value="">
                                <em></em>
                                <span>大陆线路</span>
                            </div>
                        </a>
                    </li>
                    -->
                </ul>
            </div>
        </div>
        </div>
    </div>
    
    <p class="about_link" style="margin: 0px; padding: 0px; color: rgb(102, 102, 102); line-height: 30px; font-family: tahoma, arial, 宋体, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">
	<a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=1">
	关于我们</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=2">取款帮助</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=3">存款帮助</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=4">联盟方案</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=5">联盟协议</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=6">联系我们</a>|<span class="Apple-converted-space">&nbsp;</span><a target="_blank" style="margin: 0px 10px; padding: 0px; text-decoration: none; outline: none; color: rgb(102, 102, 102);" href="/lotteryV3/help.do?code=7">常见问题</a></p>
	<p class="about_mt remind" style="margin: 0px; padding: 3px 0px; color: rgb(153, 153, 153); height: 20px; line-height: 20px; font-family: tahoma, arial, 宋体, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">
	提醒：购买彩票有风险，在线投注需谨慎，不向未满18周岁的青少年出售彩票！</p>
	<p style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: tahoma, arial, 宋体, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 18px; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">
	<img src="${base}/common/modelCommon/xljc/images/1.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/2.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/3.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/4.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/7.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/9.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"><span class="Apple-converted-space">&nbsp;</span><img src="${base}/common/modelCommon/xljc/images/10.png" style="margin: 0px 15px 5px 0px; padding: 0px; vertical-align: middle; border: 0px;"></p>

<script src="${base }/index/xlDomain.do" type="text/javascript"></script>
<script>
	var col = '';
	for(var i = 1;i<=domain.length;i++){
		col += '<li class="li_'+i+'"><a href="http:\/\/'+domain[i-1]+'" target="_blank">';
		col += '<i><img src="${base}/common/modelCommon/xljc/images/img10.png"></i></a>';
		col += '<b><a target="_blank" href="http:\/\/'+domain[i-1]+'"><img src="${base}/common/modelCommon/xljc/images/img11.png"></a></b>';
		col += '<a href="http:\/\/'+domain[i-1]+'" target="_blank"><div class="am_1">';
		col += '<p>'+domain[i-1]+'</p><input type="text" class="pus" id="sudu'+i+'"><em></em>';
		col += '<span>线路'+i+'</span></div></a></li>';
	}
	$('#xljc_domain').html(col);	
</script>
</body>
</html>