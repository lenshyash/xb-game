<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="${base}/common/template/member/core.js" path="${base}"></script>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/agtregconf.do"></script>
<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>
<script type="text/javascript" src="${base}/common/modelCommon/js/reAjax.js"></script>
<script>
// $(function(){
// 	reloadImgB();
// });

function reg(){
	 var account = $('#accountnoB').val();
     var password1 = $('#password1B').val();
     var password2 = $('#password2B').val();
     var code = $('#codeB').val();//验证码
     if(!account){
    	 alert("用户名不能为空!");
    	 return false;
     }
     if(!password1){
    	 alert("密码不能为空!");
    	 return false;
     }
     if(password1.length<6){
    	 alert("密码不能小于6位!");
    	 return false;
     }
     if(!password2){
    	 alert("确认密码不能为空!");
    	 return false;
     }
     if(password1 != password2){
    	 alert("两次密码不一致!");
    	 return false;
     }
     if(!code){
    	 alert("验证码不能为空!");
    	 return false;
     }
     
     var data = getCommitData();
     data["account"] = account;
     data["password"] = password1;
 	 data["rpassword"] = password2;
     data["verifyCode"] = code;

     	$.ajax({
     		url:"${base}/agtregister.do",
     		data : {
				data : JSON.encode(data)
			},
     		type:"POST",
     		success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = "${base}/daili";
					}
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
						reloadImg();
						rb();
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
						reloadImg();
						rb();
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
						reloadImg();
						rb();
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
						reloadImg();
						rb();
					alert("没有权限");
				} else if (ceipstate == 6) {// 登录异常
					alert(data.msg);
					top.location.href = base + "/loginError.do";
				} else {
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = "${base}/daili";
					}
					fn.success(data, textStatus, xhr);
				}
			}
     	});
}

function regB(){
	 var account = $('#accountnoB').val();
     var password1 = $('#password1B').val();
     var password2 = $('#password2B').val();
     var code = $('#codeB').val();//验证码
     if(!account){
    	 alert("用户名不能为空!");
    	 return false;
     }
     if(!password1){
    	 alert("密码不能为空!");
    	 return false;
     }
     if(password1.length<6){
    	 alert("密码不能小于6位!");
    	 return false;
     }
     if(!password2){
    	 alert("确认密码不能为空!");
    	 return false;
     }
     if(password1 != password2){
    	 alert("两次密码不一致!");
    	 return false;
     }
     if(!code){
    	 alert("验证码不能为空!");
    	 return false;
     }
     
     var data = getCommitData();
     data["account"] = account;
     data["password"] = password1;
 	 data["rpassword"] = password2;
     data["verifyCode"] = code;

     	$.ajax({
     		url:"${base}/agtregister.do",
     		data : {
				data : JSON.encode(data)
			},
     		type:"POST",
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = "${base}/daili";
					}
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
						reloadImg();
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
						reloadImg();
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
						reloadImg();
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
						reloadImg();
					alert("没有权限");
				} else if (ceipstate == 6) {// 登录异常
					alert(data.msg);
					top.location.href = base + "/loginError.do";
				} else {
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						parent.location.href = "${base}/daili";
					}
					fn.success(data, textStatus, xhr);
				}
			}
// 				success : function(j) {
// 					console.log(j);
// 					if(!j.success){
// 						alert(j.msg);
// 					}else{
// 						alert("注册成功!");
// 						parent.location.href = "${base}/daili";
// 					}
					
// 				}
     	});
}

document.onkeydown = function(event_e){  
    if(window.event) {  
        event_e = window.event;  
    }  

    var int_keycode = event_e.charCode||event_e.keyCode;  
    if( int_keycode == '13' ) {  
    	reg(); 
        return false;  
    }  
}

function reloadImgB(){
	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
	$("#reg-validCodeB").attr("src", url);
}

function reloadImg(){
	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
	$("#reg-validCodeB").attr("src", url);
	$("#reg-validCode").attr("src", url);
}
</script>
	