<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=1.0; user-scalable=yes;">
<title>${_title }APP下载</title>
<link href="${base }/images/favicon.ico" rel="shortcut icon">
<meta content="${_title }" name="description">
<link href="${base}/common/modelCommon/down/css/index.css"
	rel="stylesheet" type="text/css">
<script src="${base}/common/modelCommon/down/js/jquery-1.7.2.min.js"
	type="text/javascript"></script>
<script src="${base}/common/js/qrcode.min.js"></script>
<link type="text/css" rel="stylesheet"
	href="${base}/common/modelCommon/down/css/layer.css" id="skinlayercss">
<style type="text/css">
.tips_center {
	width: 86%;
	margin: 0 auto;
	margin-top: 20%;
	margin-bottom: 6%
}

.tips_center img {
	width: 100%
}

.tips_bottom {
	position: absolute;
	width: 100%
}

.tips_bottom_1 {
	width: 18%;
	margin-left: 41%;
	margin-bottom: 2%
}

.tips_bottom_1 img {
	width: 100%
}

.tips_bottom_2 {
	width: 36%;
	margin-left: 32%
}

.tips_bottom_2 img {
	width: 100%
}

.device {
	background: #01ce9b;
	position: absolute;
	width: 100%;
	height: 100%
}

.abodytips {
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
/* 	background: url(${base}/images/alltips.png); */
/* 	background-size: 100% 100%; */
	z-index: 10;
	display: none;
	cursor: pointer
}

.line_je {
	overflow: hidden;
	text-align: center;
	padding: 8px 0 0 0;
	position: absolute;
	margin-top: 67px;
	width: 100%;
	display: none;
}

.line_je a {
	padding: 0 8px;
	color: #627F96;
	display: inline;
}

.line_je a img {
	width: 20px;
	padding-right: 4px;
}

.line_je a img:nth-of-type(2) {
	width: 17px;
}

.installed {
	text-align: center;
	padding-bottom: 14px;
	color: #2B91E3;
}

@
-webkit-keyframes rotate { 0% {
	-webkit-transform: rotate(0);
	transform: rotate(0)
}

100%
{
-webkit-transform
:rotate(360deg)
;


transform
:rotate(360deg)


}
}
@
keyframes rotate { 0% {
	-webkit-transform: rotate(0);
	transform: rotate(0)
}

100%
{
-webkit-transform
:rotate(360deg)
;


transform
:rotate(360deg)


}
}
.loading {
	display: block;
	background-color: transparent;
	width: 40px;
	height: 40px;
	margin: 0 auto;
	border: 1px solid #2B91E3;
	border-radius: 50%;
	border-top-color: transparent;
	border-left-color: transparent;
	-webkit-transition: all .25s;
	transition: all .25s;
	-webkit-animation: rotate .6s linear infinite;
	animation: rotate .6s linear infinite
}

.help-tip {
	text-align: left;
	background-color: #1E2021;
	padding: 4px;
	width: 120px;
	position: relative;
	border-radius: 3px;
	box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
	margin-top: 4px;
	left: 40%;
	color: #FFF;
	font-size: 12px;
	line-height: 10px;
	display: none;
}

.help-tip i {
	position: absolute;
	content: '';
	width: 0;
	height: 0;
	border: 6px solid transparent;
	border-bottom-color: #1E2021;
	right: 10px;
	top: -10px;
}

.miaoshu {
	margin-top: 93px;
}
</style>
<body>
	<!--微信qq提示-->
	<div class="abodytips" id="alltip" style="display: none;">
		<div class="tips_center">
			<img src="${base}/common/modelCommon/down/images/androidtips.png"
				id="androidtips" style="display: none;"><img
				src="${base}/common/modelCommon/down/images/iostips.png"
				id="iostips"><img
				src="${base}/common/modelCommon/down/images/tip2.png" id="tips3"
				style="display: none;">
		</div>
		<div class="tips_bottom">
			<div class="tips_bottom_2">
				<a href="javascript:;" id="iknown"> <img
					src="${base}/common/modelCommon/down/images/zhi.png"></a>
			</div>
		</div>
	</div>
	<!----微信qq提示--->
	<input type="hidden" value="${base}${logo}" id="logoUrl">
	<div class="xiazai_top">
		<div class="xiazai_top_xq">
			<div class="tubao_ming">
				<a href="${base}/"><img src="${base}/images/logo.png?v=1"></a>
<%-- 				<span>${_title}</span> --%>
			</div>
			<div class="banben_m">版本号：2.0.3 应用介绍：${_title }</div>
			<div style="clear: both"></div>
		</div>
	</div>
	<script>
	$(function(){
		let logourl = $('#logoUrl').val();
		if(logourl){
			$('.tubao_ming').find('img').attr('src',logourl)
		}
	});
	</script>
	<input type="hidden" value="${domainFolder}" id="templateId">
	<input type="hidden" value="${iosapp}" id="iosApp">
	<input type="hidden" value="${Androidapp}" id="AndroidApp">
	<div class="xiazai_center">
		<div class="xiazai_center_nav">
<!-- 			<div class="xiazai_shouji"> -->
<!-- 				<img -->
<%-- 					src="${base}/common/modelCommon/down/images/iconsplash103457(1).png"> --%>
<!-- 			</div> -->
			<div class="xiazai_xuanze" style="text-align:center;">
				<h2>
					<img src="${base}/common/modelCommon/down/images/xz_1.png">
				</h2>
				<div class="xiazai_xuanze_sj" style="margin-left:180px; ">
					<div class="xiazai_xuanze_sj_zuo"></div>
					<div class="xiazai_xuanze_sj_you">
						<div class="btn-wrap">

							<c:if test="${domainFolder != 't006' }">
							<div class="wrap">
								<a class="btn-ios" id="download_android"
									href="${iosapp }" target="_blank"><i
									class="icon-clitw ictw-ios"></i><span>iPhone版下载</span> <em>版本：2.0.3</em>
								</a><a id="download_ios" class="btn-android"
									href="${Androidapp }" target="_blank"><i
									class="icon-clitw ictw-android"></i><span>Android版下载</span> <em>版本：2.0.3</em>
								</a>
							</div>
							</c:if>

							<c:if test="${domainFolder == 't006'}">
							<style>
								.beeline{
									position: absolute;
									right: 120px;
								}
								.beelineWrap{
									position: absolute;
									right: 475px;
								}
								.btn-wrap .beelineBw-code{
									position: absolute;
									right: 277px;
								}
								.wrapHeng{
									position: absolute;
									left: 683px;
								}
								.btn-wrap .hengBw-code{
									position: absolute;
									right:-161px;
								}
								.btn-wrap .hengBw-code em{
									background: red;
									color: #ffffff;
								}
								.wrapHeng a{
									color: red;
								}
							</style>
							<div class=" beeline">
								<div class="wrap beelineWrap">
									<a class="btn-ios" id="download_android" href="${iosapp }" target="_blank">
										<i class="icon-clitw ictw-ios"></i>
										<span>iPhone<strong style="color: #b92323e3;">原版</strong>下载</span> <em>版本：2.0.3</em>
									</a>
									<a id="download_ios" class="btn-android" href="${Androidapp }" target="_blank">
										<i class="icon-clitw ictw-android"></i>
										<span>Android<strong style="color: #b92323e3;">原版</strong>下载</span> <em>版本：2.0.3</em>
									</a>
								</div>
								<div class="bw-code beelineBw-code">
									<em>
										<div id="iosQrcodeCreat" style="background: url('${qrcode }');height: 140px;background-size:100%"></div>iOS客户端下载二维码
									</em>
								</div>
								<div class="bw-code beelineBw-code" style="left:-251px;">
									<em>
										<div id="androidQrcodeCreat" style="background: url('${qrcodeAndroid }');height: 140px;background-size:100%"></div>安卓客户端下载二维码
									</em>
								</div>
							</div>
<%--								//横版app--%>
							<div class="heng">
								<div class="wrapHeng wrap">
									<a class="btn-ios" id="download_android" href="https://www.cslfys.com/yKEyDD" target="_blank">
										<i class="icon-clitw ictw-ios"></i>
										<span>iPhone<strong style="color: red;">横版</strong>下载</span> <em>版本：2.0.3</em>
									</a>
									<a id="download_ios" class="btn-android" href="http://www.cslfys.com/OjQNuR" target="_blank">
										<i class="icon-clitw ictw-android"></i>
										<span>Android<strong style="color: red;">横版</strong>下载</span> <em>版本：2.0.3</em>
									</a>
								</div>
								<div class="hengBw-code bw-code">
									<em>
										<div style="background: url('https://tpxb.me/b202/iOS.png');height: 140px;background-size:100%"></div>iOS客户端下载二维码
									</em>
								</div>
								<div class="hengBw-code bw-code" style="left:307px;">
									<em>
										<div  style="background: url('https://tpxb.me/b202/android.png');height: 140px;background-size:100%"></div>安卓客户端下载二维码
									</em>
								</div>
							</div>
							</c:if>
							<c:if test="${domainFolder != 'a101' && domainFolder != 't006'}">
							<div class="bw-code">
								<em>
								<div id="iosQrcodeCreat" style="background: url('${qrcode }');height: 140px;background-size:100%"></div>iOS客户端下载二维码
								</em>
							</div>
							<div class="bw-code" style="left:510px;">
								<em>
								<div id="androidQrcodeCreat" style="background: url('${qrcodeAndroid }');height: 140px;background-size:100%"></div>安卓客户端下载二维码
								</em>
							</div>
							</c:if>
							<c:if test="${domainFolder == 'a101' }">
							<div class="bw-code">
								<em> 
								<img src="${qrcodeAndroid }" alt="${_title }">iOS<span style="color:red">普通签,安卓</span>二维码
								</em>
							</div>
							<div class="bw-code" style="left:510px;">
								<em> 
								<img src="${qrcode }" alt="${_title }">iOS<span style="color:red">超级签</span>二维码
								</em>
							</div>
							</c:if>
						</div>
					</div>
					<div style="clear: both"></div>
				</div>
			</div>
			<br /><br /><br />
			<h2 style="color:yellow;text-align:center;">${downTitle }</h2>
			<div style="clear: both"></div>
		</div>
	</div>
	<div class="xiazai_phone">
			<!-- 这里是页面内容区 -->
			<div class="card facebook-card" style="margin: 0;min-height: 100%;">
				<div class="card-header no-border" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<a href="${iosapp }" class="external" target="_blank">
							<img src="/mobile/v3/images/500vip/store_btn.png" style="max-width: 60%;">
						</a>
					</div>
				</div>
				
				<div class="card-content" style="display: flex;justify-content: center;">
					<div id="iosQrcodeCreatMobile" style="margin: 20px 0;width:256px;height: 256px;background: url('${qrcode }');background-size:100%"></div>
				</div>
				
				<div class="card-header no-border" style="text-align: center;">
					<div class="facebook-avatar" style="float: initial;">
						<a href="${Androidapp }" class="external" target="_blank"> <img src="/mobile/v3/images/500vip/android_btn.png" style="max-width: 60%;">
						</a>
					</div>
				</div>
				
				<div class="card-content" style="display: flex;justify-content: center;">
					<div id="androidQrcodeCreatMobile" style="margin: 20px 0;width:256px;height: 256px;background: url('${qrcodeAndroid }');background-size:100%"></div>
				</div>
				
				<div class="card-footer no-border">
					<span style="display: inline-block; width: 100%;text-align: center; font-size: .6rem;">温馨提示：推荐使用支付宝或者浏览器扫码二维码下载</span>
				</div>
			</div>
	</div>
	<script>
        $(function () {
            var iosImgCode = '${qrcode }'
            var androidImgCode = '${qrcodeAndroid }'
            iosImgCode = iosImgCode.substring(iosImgCode.length - 6, iosImgCode.length)
			if((iosImgCode.indexOf('jpg') == -1) && (iosImgCode.indexOf('jpeg') == -1) && (iosImgCode.indexOf('png') == -1)){
                new QRCode(document.getElementById("iosQrcodeCreat"), "${ qrcode}");
                new QRCode(document.getElementById("iosQrcodeCreatMobile"), "${ qrcode}");
            }
			if((androidImgCode.indexOf('jpg') == -1) && (androidImgCode.indexOf('jpeg') == -1) && (androidImgCode.indexOf('png') == -1)){
                new QRCode(document.getElementById("androidQrcodeCreat"), "${ qrcodeAndroid}");
                new QRCode(document.getElementById("androidQrcodeCreatMobile"), "${ qrcodeAndroid}");
            }
        })
	</script>
	<style>
		.xiazai_phone{
			display:none;
		}
		@media screen and (max-width: 500px) {
		    .xiazai_center,.banben_m{
		    	display:none;
		    }
		    .xiazai_phone{
		    	width:100%;
		    	height:100%;
		    	padding-top:20px;
		    }
		    .xiazai_phone{
			display:block;
			}
		}
	</style>
</body>
</html>