<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">

<title>${_title}-手机下注</title>
<link rel="stylesheet" href="${base}/common/modelCommon/phone2/css/onepage-scroll.css">
<link href="${base}/common/modelCommon/phone2/css/style.css" rel="stylesheet" type="text/css">
<style>
.di1_zuo_wz{ width:540px; float:left; height:100%; margin-left:-95px;background:url(${base}/images/phone/di1_wz_03.png) no-repeat;}
.di1_you{ width:500px; height:100%; float:right; background:url(${base}/images/phone/di1_iphone.png) no-repeat;}
.page1 {
	background: url(${base}/images/phone/di1-bj.jpg) repeat;
	width: 100%;
	height: 100%;
}

.page2 {
	background: url(${base}/images/phone/di2_bj.jpg) repeat;
	width: 100%;
	height: 100%;
}

.page3 {
	background: url(${base}/images/phone/di3_bj.jpg) repeat;
	width: 100%;
	height: 100%;
}

.page4 {
	background: url(${base}/images/phone/di4_bj.jpg) repeat;
	width: 100%;
	height: 100%;
}

.page5 {
	background: url(${base}/images/phone/di5_bj.jpg) repeat;
	width: 100%;
	height: 100%;
}

.page6 {
	background: url(${base}/images/phone/di6_bj.jpg) repeat;
	width: 100%;
	height: 100%;
}
</style>
<script src="${base}/common/modelCommon/phone2/js/jquery.min.js"></script>
<script src="${base}/common/modelCommon/phone2/js/jquery.onepage-scroll.min.js"></script>
<script>
	$(function() {
		$('.main').onepage_scroll({
			sectionContainer : '.page'
		});
	});
</script>

</head>

<body class="viewing-page-1">
	<ul class="onepage-pagination">
		<li><a data-index="1" href="#1"
			class="active"></a></li>
		<li><a data-index="2" href="#2"></a></li>
		<li><a data-index="3" href="#3"></a></li>
		<li><a data-index="4" href="#4"></a></li>
		<li><a data-index="5" href="#5"></a></li>
		<li><a data-index="6" href="#6"></a></li>
	</ul>
	<div class="main onepage-wrapper" style="position: relative;">
		<div class="page page1 section active" data-index="1"
			style="position: absolute; top: 0%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"></div>
				</div>
				<div class="di1_you"></div>
			</div>
		</div>
		<div class="page page2 section" data-index="2"
			style="position: absolute; top: 100%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"
						style="background: url(${base}/images/phone/di2_05.png) no-repeat;"></div>
				</div>
				<div class="di1_you"
					style="background: url(${base}/images/phone/di2_02.png) no-repeat;"></div>
			</div>
		</div>
		<div class="page page3 section" data-index="3"
			style="position: absolute; top: 200%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"
						style="background: url(${base}/images/phone/di3_05.png) no-repeat;"></div>
				</div>
				<div class="di1_you"
					style="background: url(${base}/images/phone/di3_02.png) no-repeat;"></div>
			</div>
		</div>
		<div class="page page4 section" data-index="4"
			style="position: absolute; top: 300%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"
						style="background: url(${base}/images/phone/di4_05.png) no-repeat;"></div>
				</div>
				<div class="di1_you"
					style="background: url(${base}/images/phone/di4_02.png) no-repeat;"></div>
			</div>
		</div>
		<div class="page page5 section" data-index="5"
			style="position: absolute; top: 400%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"
						style="background: url(${base}/images/phone/di5_05.png) no-repeat;"></div>
				</div>
				<div class="di1_you"
					style="background: url(${base}/images/phone/di5_02.png) no-repeat;"></div>
			</div>
		</div>
		<div class="page page6 section" data-index="6"
			style="position: absolute; top: 500%; left: 0px;">
			<div class="di_bj">
				<div class="di1_zuo">
					<div class="di1_zuo_wz"
						style="background: url(${base}/images/phone/di6_05.png) no-repeat;"></div>
				</div>
				<div class="di1_you"
					style="background: url(${base}/images/phone/di6_02.png) no-repeat;"></div>
			</div>
		</div>
	</div>
</body>
</html>