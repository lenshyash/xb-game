/**
 * Created by peter on 2015/9/9.
 */
(function($) {
    $.extend({
        cellPhone: function() {
            //手机页面点击切换选项卡公共方法
            function clickBtn(btn,box,parentBox){
                $(btn).click(function(){
                    var inx = $(this).index();
                    $(btn).removeClass("active");
                    $(this).addClass("active");
                    /*console.log($(box).parents(parentBox).find(box));*/
                    $(box).parents(parentBox).find(box).addClass("E02-none");
                    $(box).parents(parentBox).find(box).eq(inx).removeClass("E02-none");
                });
            }
            function scrollPlay(){
                var st = $(window).scrollTop();
                if(st >= 300){
                    $("#E02-cellPhone .article2 .bg div").addClass("scrollPlay");
                    $("#E02-cellPhone .article2 .bg .yellow.small").removeClass("scrollPlay").addClass("scrollPlay2");
                    $("#E02-cellPhone .article2 .bg .blue.small").removeClass("scrollPlay").addClass("scrollPlay3");
                    $("#E02-cellPhone .article2 .bg .green.small").removeClass("scrollPlay").addClass("scrollPlay4");
                }else{
                    $("#E02-cellPhone .article2 .bg div").removeClass("scrollPlay");
                    $("#E02-cellPhone .article2 .bg .yellow.small").removeClass("scrollPlay").removeClass("scrollPlay2");
                    $("#E02-cellPhone .article2 .bg .blue.small").removeClass("scrollPlay").removeClass("scrollPlay3");
                    $("#E02-cellPhone .article2 .bg .green.small").removeClass("scrollPlay").removeClass("scrollPlay4");
                }
                if(st <= 849){
                    $("#E02-cellPhone .article2 .bg div").addClass("scrollPlay");
                    $("#E02-cellPhone .article2 .bg .yellow.small").removeClass("scrollPlay").addClass("scrollPlay2");
                    $("#E02-cellPhone .article2 .bg .blue.small").removeClass("scrollPlay").addClass("scrollPlay3");
                    $("#E02-cellPhone .article2 .bg .green.small").removeClass("scrollPlay").addClass("scrollPlay4");
                }else{
                    $("#E02-cellPhone .article2 .bg div").removeClass("scrollPlay");
                    $("#E02-cellPhone .article2 .bg .yellow.small").removeClass("scrollPlay").removeClass("scrollPlay2");
                    $("#E02-cellPhone .article2 .bg .blue.small").removeClass("scrollPlay").removeClass("scrollPlay3");
                    $("#E02-cellPhone .article2 .bg .green.small").removeClass("scrollPlay").removeClass("scrollPlay4");
                }
            }
            $(document).ready(function () {
                scrollPlay();
                clickBtn("#tabCon1 .btn1 .E02-button",".tabCon1","#tabCon1");
                clickBtn("#tabCon2 .btn2 .E02-button",".tabCon2","#tabCon2");
                clickBtn("#fadetab li",".sublist",".tool-content");
                var hash = document.domain;
//                $("#E02-cellPhone .E02-banner .f15 b").html(hash);
                $('.slideCellPhone').flexslider({
                    animation: "fade",
                    controlNav: true,
                    directionNav: true,
                    prevText: "",
                    pauseOnAction:true,
                    pauseOnHover: true,
                    nextText: "",
                    slideshow: false
                    //slideshowSpeed: 7000,
                   /* pausePlay: false,
                    pauseOnAction:false,
                    smoothHeight: false*/
                });
                $('.slideAd').flexslider({
                    animation: "fade",
                    controlNav: true,
                    directionNav: true,
                    prevText: "",
                    pauseOnAction:true,
                    pauseOnHover: true,
                    nextText: ""
                    //slideshow: false
                    //slideshowSpeed: 7000,
                    /* pausePlay: false,
                     pauseOnAction:false,
                     smoothHeight: false*/
                });

            });
            $(window).scroll(function(){
                scrollPlay();
            });
        }
    });
})(jQuery);
$(document).ready(function(){
    $.cellPhone()
});