<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script src="${base}/common/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" href="${base}/common/modelCommon/layer/css/layer.css"/>
<script src="${base}/common/modelCommon/layer/js/layer.min.js"></script>
<link rel="stylesheet" href="${base}/common/modelCommon/layer/css/style.css">
<link rel="stylesheet" href="${base}/common/css/lsj_module.css"/>
<div class="Lsj-Modal">
    <div class="Lsj_modal-content" style="overflow:initial;">
        <img src="${base}/common/images/close.png" class="lsj_close" id="lsj_close_img" style="position:absolute;top:-14px;right:-14px;cursor: pointer;">
        <div class="lsj_modal_content" style="width:100%;height:100%;overflow:auto;">
        </div>
    </div>
    <div class="Lsj_modal-footer">
        <div class="Lsjbtn-bottom-two-right">
            <button class="Lsj_okBtn Lsj_outline noPop">近期不再显示</button>
            <button class="Lsj_okBtn Lsj_filled lsj_close">确定</button>
        </div>
    </div>
</div>
<script>
$(function(){
    $.ajax({
        url : "${base}/getConfig/getArticle.do",
        data : {
            code : 14
        },
        type : "post",
        dataType : 'json',
        success : function(j) {
            var title = "";
            var content = "";
            var temp = -1;
            if (j.length > 0) {
                if(j[0].index){
                    var fw = 550;
                    var fh = 550;
                    if(j[0].frameWidth && j[0].frameHeight){
                        fw = j[0].frameWidth;
                        fh = j[0].frameHeight;
                    }
                    title = j[0].title;
                    if(!title){
                        title = " ";
                        temp = 1;
                    }
                    $(".Lsj_modal-content").height(fh).width(fw)
                    console.log((new Date().getTime() - localStorage.getItem("popDate")))
                    if(localStorage.getItem("popModule") == 'hide'){
                        if((new Date().getTime() - localStorage.getItem("popDate")) > 86400000){
                            $(".lsj_modal_content").html(j[0].content)
                            $(".Lsj-Modal").show()
                            /* layer.open({
                                type : 1, //page层
                                area : [ fw + 'px', fh + 'px' ],
                                title : title,
                                shade : 0.6, //遮罩透明度
                                scrollbar : false,
                                offset : '16%',//距离顶部
                                moveType : 0, //拖拽风格，0是默认，1是传统拖动
                                shift : 1, //0-6的动画形式，-1不开启
                                content : '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'
                                        + j[0].content + '</div><input type ="button" value="近期不再显示" id="noPop" style="position:absolute;bottom:33px;right:9px;padding:3px 5px; border:none;"/>'
                                        +'<input type ="button" value="近期显示" id="yesPop" style="position:absolute;bottom:33px;right:9px;padding:3px 5px; border:none;display:none;background:red;color:#fff;boder-radius:3px;"/>'
                            }); */
                        }
                    }else{
                        $(".lsj_modal_content").html(j[0].content)
                        $(".Lsj-Modal").show()
                    }
                    $(".noPop").click(function(){
                        $(this).hide()
                        $("#yesPop").show()
                        localStorage.setItem("popModule",'hide');
                        localStorage.setItem("popDate",new Date().getTime())
                        layer.msg('设置成功')
                        $(".Lsj-Modal").hide()
                    })
                    /* $("#yesPop").click(function(){
                        $(this).hide()
                        $(".noPop").show()
                         localStorage.setItem("popModule",'show');
                         layer.msg('设置成功')
                    }) */
                    $(".lsj_close").click(function(){
                        $(".Lsj-Modal").hide()
                    })
                    if(temp < 0){
                        $('.layui-layer-title').css({
                            'height': '10px',
                            'line-height': '0px',
                            'font-size': '16px',
                            'color':'#4c2515',
                            'font-weight': 'bold',
                            'text-align': 'center',
                            'margin-bottom': '10px',
//		 							    'overflow': 'hidden',
                            'text-overflow': 'ellipsis',
                            'white-space': 'nowrap',
                            'padding-top':'18px'
                        })
                    }
                    if($(window).height() > 700){
                        $("#lsj_close_img").hide()
                    }
                }
            }

        }
    });
});
function removers(i) {
    if (i == 1) {
        $.cookie('SHOW_FRAME', 'Y', {path: '/', expires: ''});
    }
    $('#LsjmyModal').remove();
}
//手机端地址
function phoneUrl(){
	$.ajax({
		url:"${base }/getConfig/getPhoneSite.do",
		dataType:"json",
		type:"GET",
		success:function(j){
			if(j.code==1){
				var url = j.url;
				if(url.indexOf("http://")==-1){
					url = "http://"+url;
				}
				$('.phoneHome').html('<a href="'+url+'" target="_blank">'+url+'</a>');
				$('.phoneSite').html('<a href="'+url+'" target="_blank">手机下注</a>');
			}
		}
	});
}
</script>
