<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script src="${base}/common/jquery/jquery.cookie.js"></script>
<script src="${base}/common/modelCommon/layer/js/layer.min.js"></script>
<link rel="stylesheet" href="${base}/common/css/lsj_module.css"/>
<div class="Lsj-Modal">
    <div class="Lsj_modal-content">
    	<img src="${base}/common/images/close.png" class="lsj_close" id="lsj_close_img" style="position:absolute;top:-14px;right:-14px;cursor: pointer;">
    	<div class="lsj_modal_content" style="width:100%;height:100%;overflow:auto;">
    	</div>
    </div>
    <div class="Lsj_modal-footer">
        <div class="Lsjbtn-bottom-two-right">
            <button class="Lsj_okBtn Lsj_outline noPop" style="color:#fff;border:1px solid #fff;">近期不再显示</button>
            <button class="Lsj_okBtn Lsj_filled lsj_close">确定</button>
        </div>
    </div>
</div>
<script>
$(function(){
	$.ajax({
		url : "${base}/getConfig/getArticle.do",
		data:{code:14},
		type : "post",
		dataType : 'json',
		success : function(j) {
			var title="";
			var content="";
			if(j.length>0){
				if(j[0].reg){
					var title = "";
					var content = "";
					var temp = -1;
					if (j.length > 0) {
						if(j[0].index){
							var fw = 550;
							var fh = 550;
							if(j[0].frameWidth && j[0].frameHeight){
								fw = j[0].frameWidth;
								fh = j[0].frameHeight;
							}
							title = j[0].title;
							if(!title){
								title = " ";
								temp = 1;
							}
							$(".Lsj_modal-content").height(fh).width(fw)
							if(localStorage.getItem("popModule") == 'hide'){
					            if((new Date().getTime() - localStorage.getItem("popDate")) > 86400000){
					            	$(".lsj_modal_content").html(j[0].content)
					            	$(".Lsj-Modal").show()
					            }
					        }else{
					        	$(".lsj_modal_content").html(j[0].content)
				            	$(".Lsj-Modal").show()
					        }
							$(".noPop").click(function(){
								 $(this).hide()
								 $("#yesPop").show()
								 localStorage.setItem("popModule",'hide');
						         localStorage.setItem("popDate",new Date().getTime())
						         layer.msg('设置成功')
						         $(".Lsj-Modal").hide()
							})

							$(".lsj_close").click(function(){
								$(".Lsj-Modal").hide()
							})
							if($(window).height() > 700){
								$("#lsj_close_img").hide()
							}
					}
				}
				}
			}
		}
	});
	$('.onlineCustem').attr('href','${kfUrl}');
	$('.phoneAttr').attr('href','${kfUrl}');
	$('.gonggao_center').append('${publicInfo}');
});

//手机端地址
function phoneUrl(){
	$.ajax({
		url:"${base }/getConfig/getPhoneSite.do",
		dataType:"json",
		type:"GET",
		success:function(j){
			if(j.code==1){
				var url = j.url;
				if(url.indexOf("http://")==-1){
					url = "http://"+url;
				}
				$('.phoneHome').html('<a href="'+url+'" target="_blank">'+url+'</a>');
				$('.phoneSite').html('<a href="'+url+'" target="_blank">手机下注</a>');
			}
		}
	});
}
</script>
