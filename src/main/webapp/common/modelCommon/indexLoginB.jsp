<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script>
<script>
//B站模板
$(function(){
	reloadImg();
});

function login(){
	memberInfo();
}

document.onkeydown = function(event_e){  
    if(window.event) {  
        event_e = window.event;  
    }  

    var int_keycode = event_e.charCode||event_e.keyCode;  
    if( int_keycode == '13' ) {  
//     	memberInfo(); 
        return false;  
    }  
}

function memberInfo(){
 	var accountno = $("#accountno").val();
	var password = $("#password").val();
	var captcha = $("#code").val();

	if(!accountno){
		alert("用户名不能为空");
		return false;
	} else if(!password){
		alert("密码不能为空");
		return false;
	} else if(!captcha){
		alert("验证码不能为空");
		return false;
	} else if(password.length<6){
		alert("密码不能小于6位!");
		return false;
	}
		var data = {};
		data["password"] = password;
		data["account"] = accountno;
		data["verifyCode"] = captcha;
		$.ajax({
			url : "${base}/login.do",
			type : "post",
			data : data,
			success : function(result, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					if(!result.success){
						$('#code').val('');
						reloadImg();
						alert(result.msg);
					}else{
						parent.location.href = '${base}/index.do';
					}
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
					reloadImg();
				} else if (ceipstate == 3) { // 业务异常
					alert(result.msg);
					reloadImg();
				}
			}
		});
}

function reloadImg(){
		var url = "${base}/verifycode.do?timestamp=" + (new Date().getTime());
		$("#reg-validCode").attr("src", url);
}
</script>