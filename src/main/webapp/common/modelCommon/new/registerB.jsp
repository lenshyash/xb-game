<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${base}/common/js/artTemplate/template.js"></script>
<script type="text/javascript" src="${base}/common/modelCommon/js/reAjax.js"></script>
<script type="text/javascript" src="${base}/regconf.do"></script>
<script type="text/javascript" src="${base}/common/template/member/register/register.js"></script>
<script>
function reg(){
	 var account = $('#accountnoB').val();
     var password1 = $('#password1B').val();
     var password2 = $('#password2B').val();
     var code = $('#codeB').val();//验证码
     if(!account){
    	 alert("用户名不能为空!");
    	 return false;
     }
     if(!password1){
    	 alert("密码不能为空!");
    	 return false;
     }
     if(password1.length<6){
    	 alert("密码不能小于6位!");
    	 return false;
     }
     if(!password2){
    	 alert("确认密码不能为空!");
    	 return false;
     }
     if(password1 != password2){
    	 alert("两次密码不一致!");
    	 return false;
     }
     if(!code){
    	 alert("验证码不能为空!");
    	 return false;
     }
     var isParam = $('#isParam').val();
     var data = getCommitData();
     data["account"] = account;
     data["password"] = password1;
 	 data["rpassword"] = password2;
     data["verifyCode"] = code;

     	$.ajax({
     		url:"${base}/register.do",
     		data : {
				data : JSON.encode(data)
			},
     		type:"POST",
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (ceipstate == 1) {// 正常响应
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						if(!isParam){
							parent.location.href = '${base}/index.do';
						}else{
							parent.location.href = '${base}/_index.do';
						}
					}
					fn.success(data, textStatus, xhr);
				} else if (ceipstate == 2) {// 后台异常
					validCodes();
					alert("后台异常，请联系管理员!");
				} else if (ceipstate == 3) { // 业务异常
					validCodes();
					alert(data.msg);
				} else if (ceipstate == 4) {// 未登陆异常
					validCodes();
					alert("登陆超时，请重新登陆");
				} else if (ceipstate == 5) {// 没有权限
					validCodes();
					alert("没有权限");
				} else if (ceipstate == 6) {// 登录异常
					alert(data.msg);
					top.location.href = base + "/loginError.do";
				} else {
					if(!data.success){
						alert(data.msg);
					}else{
						alert("注册成功!");
						if(!isParam){
							parent.location.href = '${base}/index.do';
						}else{
							parent.location.href = '${base}/_index.do';
						}
					}
					fn.success(data, textStatus, xhr);
				}
			}
     	});
}

document.onkeydown = function(event_e){  
    if(window.event) {  
        event_e = window.event;  
    }  

    var int_keycode = event_e.charCode||event_e.keyCode;  
    if( int_keycode == '13' ) {  
    	reg(); 
        return false;  
    }  
}

function validCodes(){
 	var url = "${base}/regVerifycode.do?timestamp=" + (new Date().getTime());
 	$("#validCode").attr("src", url);
}
</script>
