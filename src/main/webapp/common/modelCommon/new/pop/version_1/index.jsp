<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${base}/common/modelCommon/new/pop/version_1/css/main.css">
    <link rel="stylesheet" href="${base}/common/modelCommon/new/pop/version_1/css/ui-dialog.css">
</head>
    <input type="hidden" value="${domainFolder}" id="domainFolder" />
    <div style="opacity: 0.7; background: rgb(0, 0, 0); position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; user-select: none; z-index: 1024;" class="ui-popup-backdrop background"></div>
    <div class="ui-popup ui-popup-modal ui-popup-show ui-popup-focus contents" role="alertdialog" style="outline: 0px;z-index: 1111;position:fixed;margin:auto;top: 140px;left:0;right:0;width: 997px;">
        <div class=" dialog-notice">
            <table class="ui-dialog-grid">
                <tbody>
                    <tr>
                        <td class="ui-dialog-header"><button class="ui-dialog-close" title="取消">×</button>
                            <div class="ui-dialog-title" id="title:tpl-message">${_title }公告</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ui-dialog-body">
                            <div class="ui-dialog-content" id="content:tpl-message">
                                <div id="DIALOG-VM" style="display: block;">
                                    <div>
                                        <div>
                                            <div class="side_left">
					                                  <!-- 标题 -->
                                            </div>
                                            <div class="notice_main">
                                                <!-- 内容 -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
          console.log($("#domainFolder").val());
            $.ajax({
                url: "${base}/getConfig/getArticle.do",
                data: {
                    code: 14
                },
                type: "post",
                dataType: 'json',
                success: function(data) {
                	if(!data.length){//没有内容隐藏
                		$('.background').hide();
                		$('.contents').hide();
                	}else if ($("#domainFolder").val() == "b06301") {
                    let leftHtml = '';//标题
                    let contentHtml = '';//内容

                    for(var i=0;i<data.length;i++){
                      var overTime = getActivetime(data[i].overTime);
                        leftHtml += '<div class="side_item dataCode'+i+'" data="'+i+'"><a href="javascript:;"><span>'+data[i].title+'</span></a></div>';
                        contentHtml += '<div class="contentList'+i+'"><div class="notice_title"><h1>'+data[i].title+'</h1></div>';
                        contentHtml += '<div class="notice_text">';
                        contentHtml += '<div class="fleft notice_item" style="text-align:left;word-break:break-all;">'+data[i].content+'</div>';
                        contentHtml += '</div></div>';
                    };
                      $('#DIALOG-VM .side_left').html(leftHtml);//标题
                      $('#DIALOG-VM .notice_main').html(contentHtml);//内容
                      $('.contentList0').css('display','block');
                      $('.contentList0').siblings('div').css('display','none');
                      $('.dataCode0').addClass('active');
                      $('.notice_main .notice_item').find('img').css('width','100%');

                      $('.side_item').click(function(){//标题点击
                        var num = $(this).attr('data');
                        $(this).addClass('active');
                        $(this).siblings('div').removeClass('active');
                        $('.contentList'+num).show();
                        $('.contentList'+num).siblings('div').hide();
                      })
                	}else {
                    let leftHtml = '';//标题
                    let contentHtml = '';//内容

                    for(var i=0;i<data.length;i++){
                      var overTime = getActivetime(data[i].overTime);
                        leftHtml += '<div class="side_item dataCode'+i+'" data="'+i+'"><a href="javascript:;"><span>'+data[i].title+'</span></a></div>';
                        contentHtml += '<div class="contentList'+i+'"><div class="notice_title"><h1>'+data[i].title+'</h1></div>';
                        contentHtml += '<div class="notice_text">';
                        contentHtml += '<div class="fleft notice_item" style="text-align:left;word-break:break-all;">'+data[i].content+'</div>';
                        contentHtml += '<div class="fright">结束时间：【<span>'+overTime+'</span>】</div></div></div>';
                    };

                      $('#DIALOG-VM .side_left').html(leftHtml);//标题
                      $('#DIALOG-VM .notice_main').html(contentHtml);//内容
                      $('.contentList0').css('display','block');
                      $('.contentList0').siblings('div').css('display','none');
                      $('.dataCode0').addClass('active');
                      $('.notice_main .notice_item').find('img').css('width','100%');

                      $('.side_item').click(function(){//标题点击
                        var num = $(this).attr('data');
                        $(this).addClass('active');
                        $(this).siblings('div').removeClass('active');
                        $('.contentList'+num).show();
                        $('.contentList'+num).siblings('div').hide();

                    })
                	}
                }
            });


            $('.ui-dialog-close').click(function(){//close
                $('.background').hide();
                $('.contents').hide();
            })
        });

        function getActivetime(time){//时间戳转换函数
        	 var date = new Date(time);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
             Y = date.getFullYear() + '-';
             M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
             D = date.getDate() + ' ';
             h = date.getHours() + ':';
             m = (date.getMinutes() < 10 ? '0'+(date.getMinutes()) : date.getMinutes()) + ':';
             s = (date.getSeconds() < 10 ? '0'+(date.getSeconds()) : date.getSeconds());
             return Y+M+D+h+m+s;
        }


    </script>
    <c:if test="${isRedPacket eq 'on'}">
	<script src="${base}/common/js/redpacket/redbag.js?v=1.2.3" path="${base}"></script>
</c:if>
<!-- 转盘功能 -->
<%-- /index/luckyLottery.do --%>
<!-- 签到功能 -->
<%-- /index/signNew.do --%>
<!-- 统计代码 -->
<div style="position: fixed; z-index: 99999999; bottom: 0;">${accessStatisticsCode}</div>
<!-- 弹窗 -->

<div style="position: fixed; left: 0; bottom: 0;display: flex;align-items:flex-end;z-index:99999;">
	<c:if test="${isZpOnOff eq 'on'}" >
		<jsp:include page="/common/include/turnlate.jsp"></jsp:include>
	</c:if>
	<c:if test="${isQdOnOff eq 'on'}">
		<jsp:include page="/common/include/exchange.jsp"></jsp:include>
	</c:if>
</div>
    </body>

</html>
