<%@ page language="java" pageEncoding="utf-8"%>
<script>
function memberInfo(){
 	var accountno = $("#accountno").val();
	var password = $("#password").val();
	var captcha = $("#code").val();

	if(!accountno){
		alert("用户名不能为空");
		return false;
	} else if(!password){
		alert("密码不能为空");
		return false;
	} else if(!captcha){
		alert("验证码不能为空");
		return false;
	} else if(password.length<6){
		alert("密码不能小于6位!");
		return false;
	}
		var data = {};
		data["password"] = password;
		data["account"] = accountno;
		data["verifyCode"] = captcha;
		$.ajax({
			url : "${base}/login.do",
			type : "post",
			data : data,
			success : function(result, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate")
				if (!ceipstate || ceipstate == 1) {// 正常响应
					if(!result.success){
						$('#code').val('');
						reloadImg();
						alert(result.msg);
					}else{
						var showIndex = $('#showIndex').val();
						if(showIndex){
							parent.location.href = '${base}/lottery/index.do';
						}else{
							parent.location.href = '${base}/index.do';
						}
					}
				} else if (ceipstate == 2) {// 后台异常
					alert("后台异常，请联系管理员!");
					reloadImg();
				} else if (ceipstate == 3) { // 业务异常
					alert(result.msg);
					reloadImg();
				}
			}
		});
}

function reloadImg(){
		var url = "${base}/verifycode.do?timestamp=" + (new Date().getTime());
		$("#reg-validCode").attr("src", url);
}

$(function(){
	$('#reg-validCode').click(function(){
		reloadImg();
	});
});

</script>