<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="${base}/common/modelCommon/layer/css/layer.css">
<script src="${base}/common/modelCommon/layer/js/layer.min.js"></script>
<script>
    $(function(){
        $.ajax({
            url : "${base}/getConfig/getArticle.do",
            data:{code:14},
            type : "post",
            dataType : 'json',
            success : function(j) {
                var title="";
                var content="";
                var temp = -1;
                if(j.length>0){
                    if(j[0].reg){
                        var fw = 550;
                        var fh = 550;
                        if(j[0].frameWidth && j[0].frameHeight){
                            fw = j[0].frameWidth;
                            fh = j[0].frameHeight;
                        }
                        title = j[0].title;
                        if(!title){
                            title = " ";
                            temp = 1;
                        }
                        layer.open({
                            type: 1, //page层
                            area: [fw+'px', fh+'px'],
                            title: title,
                            shade: 0.6, //遮罩透明度
                            scrollbar: false,
                            offset: '16%',//距离顶部
                            moveType: 0, //拖拽风格，0是默认，1是传统拖动
                            shift: 1, //0-6的动画形式，-1不开启
                            content: '<div style="overflow-x: hidden;overflow-y: hidden;width:100%;height:100%;">'+j[0].content+'</div>'
                        });
                        if(temp < 0){
                            $('.layui-layer-title').css({
// 								'height': '10px',
// 							    'line-height': '0px',
                                'font-size': '16px',
                                'color':'#4c2515',
                                'font-weight': 'bold',
                                'text-align': 'center',
// 							    'margin-bottom': '10px',
// 							    'overflow': 'hidden',
                                'text-overflow': 'ellipsis',
                                'white-space': 'nowrap',
                                'padding':'10px'
                            })
                        }
                    }
                }

            }
        });
    });
</script>