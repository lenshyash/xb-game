<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="${base }/images/favicon.ico" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="手机投注" />
<title>${_title } - 手机投注</title>
</head>
<!-- 手机页面 -->
<body>
	<!-- 页面主体 开始-->
	<link href="${base}/common/modelCommon/phone/css/WdatePicker.css" rel="stylesheet" type="text/css">
	<link href="${base}/common/modelCommon/phone/css/user.css" rel="stylesheet" type="text/css">
	<link href="${base}/common/modelCommon/phone/css/cellPhone.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="${base}/common/modelCommon/phone/css/phoneStyle.css">
	<link rel="stylesheet" type="text/css" href="${base}/common/modelCommon/phone/css/style.css">
<%-- 	<script src="${base}/common/jquery/jquery-1.12.3.min.js"></script> --%>
	<script type="text/javascript" src="${base}/common/modelCommon/phone/js/jquery.carouFredSel-6.0.4-packed.js" charset="utf-8"></script>
	<script type="text/javascript" src="${base}/common/modelCommon/phone/js/jquery.carousel.js" charset="utf-8"></script>
	<script type="text/javascript" src="${base}/common/modelCommon/phone/js/cellPhone.js" charset="utf-8"></script>
	<script type="text/javascript" src="${base}/common/modelCommon/phone/js/jquery.flexslider-min.js" charset="utf-8"></script>
	<style>
		#E02-cellPhone .E02-banner .E02-section .top{
		    width: 1000px;
		    height: 370px;
		    background: url("${base}/images/cell_bannerTop.png") center center no-repeat;
		    position: absolute;
		    left: 0;
		    top: 15px;
		    z-index: 2;
		}
	</style>
	<div id="E02-cellPhone">
		<div class="E02-banner">
			<div class="E02-section clearfix">
				<div style="position: absolute;
						    width: 89px;
						    height: 89px;
						    right: 91px;
						    background-size: 100%;
						    background: url(${appQRCodeLinkAndroid }) no-repeat;
						    top: 70px;
						    z-index: 9;" id="appArcode"></div>
				<div class="f15">
<%-- 					例如 <b>${hostUrl1 }/mobile</b> --%>
				</div>
				<div class="top"></div>
				<div class="ico ico1"></div>
				<div class="ico ico2"></div>
				<div class="ico ico3"></div>
				<div class="ico ico4"></div>
				<div class="ico ico5"></div>
				<div class="ico ico6"></div>
				<div class="ico clone3"></div>
				<div class="ico clone4"></div>
				<div class="ico clone2"></div>
				<div class="ico ico7"></div>
			</div>
		</div>
		<div class="article2">
			<div class="E02-section clearfix">
				<div class="tabBox" id="tabCon1">
					<div class="btnGrp btn1">
						<a href="javascript:;" class="E02-button mr58 active">手机站</a> 
						<a href="javascript:;" class="E02-button">新手指南</a>
					</div>
					<div class="tabCon1">
						<div class="flexslider slideAd">
							<ul class="slides">
								<li class="helpCon"
									style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<img
									src="${base}/common/modelCommon/phone/images/webMobile.png"
									alt="" draggable="false">
									<div class="conTxt">
										<h2>资金信息，尽在手中</h2>
										<div class="f16">存款、取款、会员资料，资金动向尽在弹指间。</div>
									</div>
								</li>
								<li class="helpCon"
									style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<img
									src="${base}/common/modelCommon/phone/images/webMobile2.png"
									alt="" draggable="false">
									<div class="conTxt">
										<h2>手机投注</h2>
										<div class="f16">各种玩法任您点，无需下载，随时随地赢大奖。</div>
									</div>
								</li>
								<li class="helpCon"
									style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0.0244717; display: block; z-index: 1;">
									<img
									src="${base}/common/modelCommon/phone/images/webMobile3.png"
									alt="" draggable="false">
									<div class="conTxt">
										<h2>最佳体育赛事</h2>
										<div class="f16">业界顶尖掌上体育投注平台，客户体验，没有最好，只有更好。</div>
									</div>
								</li>
								<li class="helpCon flex-active-slide"
									style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0.975528; display: block; z-index: 2;">
									<img
									src="${base}/common/modelCommon/phone/images/webMobile4.png"
									alt="" draggable="false">
									<div class="conTxt">
										<h2>随时滚求随时赢</h2>
										<div class="f16">不管您身在何处，超过1000场实时赔率体育比赛让您娱乐无穷。</div>
									</div>
								</li>
							</ul>
<!-- 							<ol class="flex-control-nav flex-control-paging"> -->
<!-- 							<li><a class="flex-active">1</a></li> -->
<!-- 							<li><a>2</a></li> -->
<!-- 							<li><a>3</a></li> -->
<!-- 							<li><a>4</a></li> -->
<!-- 							</ol> -->
<!-- 							<ul class="flex-direction-nav"> -->
<!-- 								<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li> -->
<!-- 								<li class="flex-nav-next"><a class="flex-next" href="#"></a></li> -->
<!-- 							</ul> -->
						</div>
					</div>
					<div class="mobileHelp tabCon1 E02-none" id="tabCon2">
						<div class="btnGrp btn2">
<!-- 							<a href="javascript:;" class="E02-button">免费注册</a>  -->
								<a href="javascript:;" class="E02-button">如何存款</a>
								<a href="javascript:;" class="E02-button">如何取款</a> 
<!-- 								<a href="javascript:;" class="E02-button">常见问题</a>  -->
								<a href="javascript:;" class="E02-button mr0 active">修改资料</a>
						</div>
<!-- 						<div class="tabCon2 E02-none"> -->
<!-- 							<div class="flexslider slideCellPhone"> -->
<!-- 								<ul class="slides"> -->
<!-- 									<li class="helpCon flex-active-slide" -->
<!-- 										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"> -->
<!-- 										<img -->
<%-- 										src="${base}/common/modelCommon/phone/images/help11.png" --%>
<!-- 										alt="" draggable="false"> -->
<!-- 										<div class="conTxt"> -->
<!-- 											<h2>第1步</h2> -->
<!-- 											<div class="f16">点击“免费注册”，进入注册页面。</div> -->
<!-- 										</div> -->
<!-- 									</li> -->
<!-- 									<li class="helpCon" -->
<!-- 										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> -->
<!-- 										<img -->
<%-- 										src="${base}/common/modelCommon/phone/images/help12.png" --%>
<!-- 										alt="" draggable="false"> -->
<!-- 										<div class="conTxt"> -->
<!-- 											<h2>第2步</h2> -->
<!-- 											<div class="f16">按照界面内容，输入您的注册信息，点击“注册”即可马上成为威尼斯人的一员。</div> -->
<!-- 										</div> -->
<!-- 									</li> -->
<!-- 								</ul> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<div class="tabCon2 E02-none">
							<div class="flexslider slideCellPhone">
								<ul class="slides">
									<li class="helpCon flex-active-slide"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
										<img
										src="${base}/common/modelCommon/phone/images/help21.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第1步</h2>
											<div class="f16">登录会员账号，进入“会员中心”。</div>
										</div>
									</li>
									<li class="helpCon"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
										<img
										src="${base}/common/modelCommon/phone/images/help22.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第2步</h2>
											<div class="f16">您可以点击“充值”,选择快速入款,公司入款或者在线充值。</div>
										</div>
									</li>
								</ul>
<!-- 								<ol class="flex-control-nav flex-control-paging"> -->
<!-- 									<li><a class="flex-active">1</a></li> -->
<!-- 									<li><a>2</a></li> -->
<!-- 								</ol> -->
<!-- 								<ul class="flex-direction-nav"> -->
<!-- 									<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li> -->
<!-- 									<li class="flex-nav-next"><a class="flex-next" href="#"></a></li> -->
<!-- 								</ul> -->
							</div>
						</div>
						<div class="tabCon2 E02-none">
							<div class="flexslider slideCellPhone">
								<ul class="slides">
									<li class="helpCon flex-active-slide"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
										<img
										src="${base}/common/modelCommon/phone/images/help31.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第1步</h2>
											<div class="f16">登录会员账号，进入“会员中心”，选择“提款”。</div>
										</div>
									</li>
									<li class="helpCon"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
										<img
										src="${base}/common/modelCommon/phone/images/help32.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第2步</h2>
											<div class="f16">输入您的取款金额及网站取款密码，点击“下一步”,即可提款。</div>
										</div>
									</li>
									<li class="helpCon"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
										<img
										src="${base}/common/modelCommon/phone/images/help33.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第3步</h2>
											<div class="f16">提交取款申请成功，取款最快30分钟至2小时内到账。</div>
										</div>
									</li>
								</ul>
<!-- 								<ol class="flex-control-nav flex-control-paging"> -->
<!-- 									<li><a class="flex-active">1</a></li> -->
<!-- 									<li><a>2</a></li> -->
<!-- 									<li><a>3</a></li> -->
<!-- 								</ol> -->
<!-- 								<ul class="flex-direction-nav"> -->
<!-- 									<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li> -->
<!-- 									<li class="flex-nav-next"><a class="flex-next" href="#"></a></li> -->
<!-- 								</ul> -->
							</div>
						</div>
<!-- 						<div class="tabCon2 E02-none"> -->
<!-- 							<div class="flexslider slideCellPhone"> -->
<!-- 								<ul class="slides"> -->
<!-- 									<li class="helpCon flex-active-slide" -->
<!-- 										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"> -->
<!-- 										<img -->
<%-- 										src="${base}/common/modelCommon/phone/images/help41.png" --%>
<!-- 										alt="" draggable="false"> -->
<!-- 										<div class="conTxt"> -->
<!-- 											<h2>第1步</h2> -->
<!-- 											<div class="f16">登录会员账号，进入“会员中心”，选择“自助洗码”。</div> -->
<!-- 										</div> -->
<!-- 									</li> -->
<!-- 									<li class="helpCon" -->
<!-- 										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> -->
<!-- 										<img -->
<%-- 										src="${base}/common/modelCommon/phone/images/help42.png" --%>
<!-- 										alt="" draggable="false"> -->
<!-- 										<div class="conTxt"> -->
<!-- 											<h2>第2步</h2> -->
<!-- 											<div class="f16">选择您想要洗码的游戏类型或者大厅，单厅有效投注额累计1000即可结算，点击提交之后即可到账。</div> -->
<!-- 										</div> -->
<!-- 									</li> -->
<!-- 								</ul> -->
<!-- 								<ol class="flex-control-nav flex-control-paging"> -->
<!-- 									<li><a class="flex-active">1</a></li> -->
<!-- 									<li><a>2</a></li> -->
<!-- 								</ol> -->
<!-- 								<ul class="flex-direction-nav"> -->
<!-- 									<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li> -->
<!-- 									<li class="flex-nav-next"><a class="flex-next" href="#"></a></li> -->
<!-- 								</ul> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<div class="tabCon2">
							<div class="flexslider slideCellPhone">
								<ul class="slides">
									<li class="helpCon flex-active-slide"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
										<img
										src="${base}/common/modelCommon/phone/images/help51.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第1步</h2>
											<div class="f16">登录会员账号，进入“会员中心”，点击“查看个人资料”。</div>
										</div>
									</li>
									<li class="helpCon"
										style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
										<img
										src="${base}/common/modelCommon/phone/images/help52.png"
										alt="" draggable="false">
										<div class="conTxt">
											<h2>第2步</h2>
											<div class="f16">您可以选择修改邮箱、取款密码、登录密码及取款银行卡信息。</div>
										</div>
									</li>
								</ul>
<!-- 								<ol class="flex-control-nav flex-control-paging"> -->
<!-- 									<li><a class="flex-active">1</a></li> -->
<!-- 									<li><a>2</a></li> -->
<!-- 								</ol> -->
<!-- 								<ul class="flex-direction-nav"> -->
<!-- 									<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li> -->
<!-- 									<li class="flex-nav-next"><a class="flex-next" href="#"></a></li> -->
<!-- 								</ul> -->
							</div>
						</div>
					</div>
				</div>
				<div class="bg">
					<div class="circle Green scrollPlay"></div>
					<div class="circle2 yellow small scrollPlay2"></div>
					<div class="circle3 orange scrollPlay"></div>
					<div class="circle4 pink scrollPlay"></div>
					<div class="circle5 blue small scrollPlay3"></div>
					<div class="circle6 yellow scrollPlay"></div>
					<div class="circle7 blue scrollPlay"></div>
					<div class="circle8 green small scrollPlay4"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>