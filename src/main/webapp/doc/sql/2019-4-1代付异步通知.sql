ALTER TABLE "public"."agent_payment"
ADD COLUMN "is_notity" int4;
COMMENT ON COLUMN "public"."agent_payment"."is_notity" IS '是否回调 0无回调1有回调';

ALTER TABLE "public"."mny_draw_record"
ADD COLUMN "pay_id" int4;
COMMENT ON COLUMN "public"."mny_draw_record"."pay_id" IS '代付对象ID';

