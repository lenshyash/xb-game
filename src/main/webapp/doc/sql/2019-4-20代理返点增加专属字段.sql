
ALTER TABLE "public"."member_genrt_link"
ADD COLUMN "dynamic_rate" numeric(12,2);

COMMENT ON COLUMN "public"."member_genrt_link"."dynamic_rate" IS '动态赔率点';


INSERT INTO "public"."agent_base_config" ("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('动态赔率最大值', 'member_dynamic_rate_max_num', '', 'text', '动态赔率最大值', '', '12', '3', '', '9', '1', '16');


ALTER TABLE "public"."proxy_multi_rebate_record"
ADD COLUMN "rebate_type" int4;

COMMENT ON COLUMN "public"."proxy_multi_rebate_record"."rebate_type" IS '返点类型：1多级代理返点2动态赔率返点';

