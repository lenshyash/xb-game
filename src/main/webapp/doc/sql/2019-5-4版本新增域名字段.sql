ALTER TABLE "public"."app_update"
ADD COLUMN "domain" varchar(255);

COMMENT ON COLUMN "public"."app_update"."domain" IS '域名链接';
