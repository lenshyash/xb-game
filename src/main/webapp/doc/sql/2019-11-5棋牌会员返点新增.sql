ALTER TABLE "public"."proxy_daily_betting_statistics"
ADD COLUMN "chess_bet_amount" numeric(16,5),
ADD COLUMN "chess_profit_amount" numeric(16,5),
ADD COLUMN "chess_rebate_amount" numeric(16,5);

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."chess_bet_amount" IS '棋牌投注金额';

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."chess_profit_amount" IS '棋牌盈利金额';

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."chess_rebate_amount" IS '棋牌返点金额';


ALTER TABLE "public"."proxy_manual_rebate_record"
ADD COLUMN "chess_profit_amount" numeric(16,5),
ADD COLUMN "chess_rebate" numeric(5,2),
ADD COLUMN "chess_rebate_amount" numeric(16,5);

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."chess_profit_amount" IS '棋牌盈利金额';

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."chess_rebate" IS '棋牌返点';

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."chess_rebate_amount" IS '棋牌返点金额';

