INSERT INTO "public"."admin_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('租户彩票额度管理', '/admin/agentLotteryQuoto/index.do', (select id from admin_menu where name = '平台管理'), 20, 2, 2, NULL, '', '/admin/agentLotteryQuoto', '');

CREATE SEQUENCE "public"."agent_lottery_quoto_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."agent_lottery_quoto_seq"', 1, true);

ALTER SEQUENCE "public"."agent_lottery_quoto_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_lottery_quoto" (
  "id" int4 NOT NULL DEFAULT nextval('agent_lottery_quoto_seq'::regclass),
  "station_id" int4,
  "buy_money" numeric(20,2),
  "win_money" numeric(20,2),
  "last_update_datetime" timestamp(6),
  CONSTRAINT "agent_lottery_quoto_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_lottery_quoto" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."agent_lottery_quoto"."buy_money" IS '投注金额';

COMMENT ON COLUMN "public"."agent_lottery_quoto"."win_money" IS '赢钱金额';

COMMENT ON COLUMN "public"."agent_lottery_quoto"."last_update_datetime" IS '最后修改时间';
