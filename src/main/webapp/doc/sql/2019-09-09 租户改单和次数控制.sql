CREATE SEQUENCE "public"."bc_lottery_order_change_count_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

ALTER SEQUENCE "public"."bc_lottery_order_change_count_id_seq" OWNER TO "postgres";
SELECT setval('"public"."bc_lottery_order_change_count_id_seq"', 1, true);

INSERT INTO "public"."agent_base_config"( "name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ( '彩票改单次数限制(0为不限制)', 'bc_lottery_change_order_count_limit', '', 'text', '彩票改单次数限制(0为不限制)', '', '0', 3, '', 7, 1, 199);
INSERT INTO "public"."agent_base_config"("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ( '租户彩票改单开关', 'onoff_lottery_change_order', '', 'select', '租户彩票改单开关', '', 'off', 3, '[{"off":"关闭"},{"on":"开启"}]', 7, 1, 199);

CREATE TABLE "public"."bc_lottery_order_change_count" (
  "id" int4 NOT NULL DEFAULT nextval('bc_lottery_order_change_count_id_seq'::regclass),
  "station_id" int4 NOT NULL DEFAULT NULL,
  "stat_date" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "count" int4 DEFAULT NULL,
  CONSTRAINT "station_statdate" UNIQUE ("station_id", "stat_date")
)
;

ALTER TABLE "public"."bc_lottery_order_change_count" 
  OWNER TO "postgres";
