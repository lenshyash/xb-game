CREATE SEQUENCE "public"."agent_profit_share_strategy_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."agent_profit_share_strategy_id_seq" OWNER TO "postgres";


CREATE SEQUENCE "public"."agent_profit_share_record_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."agent_profit_share_record_id_seq" OWNER TO "postgres";


ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "third_deposit_amount" numeric(20,5) DEFAULT 0;

CREATE TABLE "public"."agent_profit_share_record" (
	"id" int4 NOT NULL DEFAULT nextval('agent_profit_share_record_id_seq'::regclass),
	"station_id" int4,
	"account_id" int4,
	"bet_date" date,
	"account" varchar(50) COLLATE "default",
	"profit_share_desc" varchar(500) COLLATE "default",
	"profit_share_status" int2,
	"agent_id" int4,
	"operator" varchar(50) COLLATE "default",
	"agent_name" varchar(20) COLLATE "default",
	"live_bet_money" numeric(26,4) DEFAULT 0,
	"live_win_money" numeric(26,4) DEFAULT 0,
	"live_platform_fee" numeric(26,4) DEFAULT 0,
	"egame_bet_money" numeric(26,4) DEFAULT 0,
	"egame_win_money" numeric(26,4) DEFAULT 0,
	"egame_platform_fee" numeric(26,4) DEFAULT 0,
	"sport_bet_money" numeric(26,4) DEFAULT 0,
	"sport_win_money" numeric(26,4) DEFAULT 0,
	"sport_platform_fee" numeric(26,4) DEFAULT 0,
	"chess_bet_money" numeric(26,4) DEFAULT 0,
	"chess_win_money" numeric(26,4) DEFAULT 0,
	"chess_platform_fee" numeric(26,4) DEFAULT 0,
	"third_sport_bet_money" numeric(26,4) DEFAULT 0,
	"third_sport_win_money" numeric(26,4) DEFAULT 0,
	"third_sport_platform_fee" numeric(26,4) DEFAULT 0,
	"third_lottery_bet_money" numeric(26,4) DEFAULT 0,
	"third_lottery_win_money" numeric(26,4) DEFAULT 0,
	"third_lottery_platform_fee" numeric(26,4) DEFAULT 0,
	"third_deposit" numeric(26,4) DEFAULT 0,
	"lottery_bet_money" numeric(26,4) DEFAULT 0,
	"lottery_win_money" numeric(26,4) DEFAULT 0,
	"lottery_platform_fee" numeric(26,4) DEFAULT 0,
	"egame_rebate_money" numeric(26,4) DEFAULT 0,
	"live_rebate_money" numeric(26,4) DEFAULT 0,
	"sport_rebate_money" numeric(26,4) DEFAULT 0,
	"lottery_rebate_money" numeric(26,4) DEFAULT 0,
	"chess_rebate_money" numeric(26,4) DEFAULT 0,
	"third_sport_rebate_money" numeric(26,4) DEFAULT 0,
	"third_lottery_rebate_money" numeric(26,4) DEFAULT 0,
	CONSTRAINT "agent_profit_share_record_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."agent_profit_share_record" OWNER TO "postgres";

CREATE TABLE "public"."agent_profit_share_strategy" (
	"id" int4 NOT NULL DEFAULT nextval('agent_profit_share_strategy_id_seq'::regclass),
	"type" int2 NOT NULL,
	"status" int2 NOT NULL,
	"create_datetime" timestamp(6) NOT NULL,
	"station_id" int4 NOT NULL,
	"platform_fee_rate" numeric(12,2) NOT NULL DEFAULT 0,
	"memo" text COLLATE "default",
	"platform_fee_type" int2 NOT NULL DEFAULT 1,
	CONSTRAINT "agent_profit_share_strategy_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE,
	CONSTRAINT "station_type" UNIQUE ("type","station_id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."agent_profit_share_strategy" OWNER TO "postgres";
COMMENT ON COLUMN "public"."agent_profit_share_strategy"."platform_fee_rate" IS '平台抽取费率';
COMMENT ON COLUMN "public"."agent_profit_share_strategy"."platform_fee_type" IS '费率类型(1 盈利 2 流水）';

insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ('0', '', '', '代理设置－最大占成数', '1', '', '9', 'agent_profit_share_max_num', 'text', '203', '3', '代理设置－最大占成数');
insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( 'off', '', '[{"on":"开启"},{"off":"关闭"}]', '代理占成开关', '1', '', '9', 'onoff_profit_share', 'select', '203', '3', '代理占成开关');


insert into "public"."agent_menu" ( "status", "sort", "remark", "icon", "level", "module_path", "parent_id", "type", "name", "url") 
values ( '2', '27', '', '', '2', '/agent/profitsharerecord', '3', null, '代理按日占成记录', '/agent/profitsharerecord/index.do');
insert into "public"."agent_menu" ( "status", "sort", "remark", "icon", "level", "module_path", "parent_id", "type", "name", "url") 
values ( '2', '16', '', '', '2', '/agent/profitsharestrategy', '1', null, '代理按日占成策略', '/agent/profitsharestrategy/index.do');