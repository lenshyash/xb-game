ALTER TABLE "public"."sys_account" ADD COLUMN "report_type" int2 DEFAULT 1;
COMMENT ON COLUMN "public"."sys_account"."report_type" IS '报表类型（默认 1普通，2引导）';

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "report_type" int4 DEFAULT 1;

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "report_type" int4 DEFAULT 1;

CREATE OR REPLACE  FUNCTION "public"."auto_insert_into_daily_money_partition"() RETURNS "trigger" 
	AS $BODY$  
DECLARE  
    stat_date_column_name    varchar ;          -- 父表中用于分区的时间字段的名称[必须首先初始化!!]  
    curMM varchar;     -- 'YYYYMM'字串,用做分区子表的后缀  
startTime varchar;
endTime varchar;
    isExist boolean;        -- 分区子表,是否已存在  
    strSQL varchar;  
tab_cols record;
col_val varchar;
BEGIN  
    -- 调用前,必须首先初始化(时间字段名):stat_date_column_name [直接从调用参数中获取!!]  
    stat_date_column_name := TG_ARGV[0];  
     
    -- 判断对应分区表 是否已经存在?  
    EXECUTE 'SELECT $1.'||stat_date_column_name INTO strSQL USING NEW;  
    curMM := to_char( strSQL::date , 'YYYYMM' );  
    select count(*) INTO isExist from pg_class where relname = (TG_TABLE_NAME||'_'||curMM);  
  
    -- 若不存在, 则插入前需 先创建子分区  
    IF ( isExist = false ) THEN    
        -- 创建子分区表  
        startTime := curMM||'01';  
        endTime := to_char( startTime::date + interval '1 month', 'YYYY-MM-DD');  
        strSQL := 'CREATE TABLE IF NOT EXISTS '||TG_TABLE_NAME||'_'||curMM||' 
(CHECK('||stat_date_column_name||'>='''|| startTime ||''' AND '  
                             ||stat_date_column_name||'< '''|| endTime ||''' )) 
INHERITS ('||TG_TABLE_NAME||');';    
        EXECUTE strSQL;  
  
        -- 创建主键
        strSQL := 'ALTER TABLE '||TG_TABLE_NAME||'_'||curMM||' ADD PRIMARY KEY ("id");';
        EXECUTE strSQL;
        -- 创建索引  
        strSQL := 'CREATE INDEX '||TG_TABLE_NAME||'_'||curMM||'_INDEX_stat_date ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(stat_date);' ;  
        EXECUTE strSQL;
        strSQL := 'CREATE INDEX '||TG_TABLE_NAME||'_'||curMM||'_INDEX_station_id ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(station_id);' ;  
        EXECUTE strSQL;
strSQL := 'CREATE UNIQUE INDEX '||TG_TABLE_NAME||'_'||curMM||'_s_a_unique_key ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(stat_date,account_id);' ;  
        EXECUTE strSQL;
        
    END IF;  
  
    -- 插入数据到子分区!  
    strSQL := 'INSERT INTO '||TG_TABLE_NAME||'_'||curMM||' AS a SELECT $1.*
on CONFLICT(account_id,stat_date) DO UPDATE SET ';

FOR tab_cols IN SELECT "column_name","data_type" from information_schema.columns
WHERE table_schema='public' AND table_name=TG_TABLE_NAME 
LOOP
 EXECUTE 'SELECT $1.'||tab_cols."column_name" INTO col_val USING NEW;  
      IF tab_cols."column_name" <> 'account_id' AND tab_cols."column_name" <> 'station_id'
AND tab_cols."column_name" <> 'id' AND tab_cols."column_name" <> 'agent_id'
AND tab_cols."column_name" <> 'balance' AND tab_cols."column_name" <> 'account_type'
AND tab_cols."column_name" <> 'report_type'
AND (tab_cols."data_type" = 'integer' 
OR tab_cols."data_type" = 'numeric')
AND col_val::numeric <> 0 
THEN
        strSQL := strSQL ||tab_cols."column_name"||'=COALESCE(a.'||tab_cols."column_name"||',0)+EXCLUDED.'||tab_cols."column_name"||',';
      END IF;     
    END LOOP;
strSQL := strSQL ||'balance=EXCLUDED.balance;';

    EXECUTE strSQL USING NEW;  
    RETURN NULL;   
END  
$BODY$
	LANGUAGE plpgsql
	COST 100
	CALLED ON NULL INPUT
	SECURITY INVOKER
	VOLATILE;
ALTER FUNCTION "public"."auto_insert_into_daily_money_partition"() OWNER TO "postgres";


CREATE OR REPLACE FUNCTION "public"."auto_insert_into_guide_money_partition"() RETURNS "trigger" 
	AS $BODY$  
DECLARE  
    stat_date_column_name    varchar ;          -- 父表中用于分区的时间字段的名称[必须首先初始化!!]  
    curMM	varchar;     -- 'YYYYMM'字串,用做分区子表的后缀  
startTime	varchar;
endTime	varchar;
    isExist	boolean;        -- 分区子表,是否已存在  
    strSQL	varchar;  
tab_cols	record;
col_val	varchar;
BEGIN  
    -- 调用前,必须首先初始化(时间字段名):stat_date_column_name [直接从调用参数中获取!!]  
    stat_date_column_name := TG_ARGV[0];  
     
    -- 判断对应分区表 是否已经存在?  
    EXECUTE 'SELECT $1.'||stat_date_column_name INTO strSQL USING NEW;  
    curMM := to_char( strSQL::date , 'YYYYMM' );  
    select count(*) INTO isExist from pg_class where relname = (TG_TABLE_NAME||'_'||curMM);  
  
    -- 若不存在, 则插入前需 先创建子分区  
    IF ( isExist = false ) THEN    
        -- 创建子分区表  
        startTime := curMM||'01';  
        endTime := to_char( startTime::date + interval '1 month', 'YYYY-MM-DD');  
        strSQL := 'CREATE TABLE IF NOT EXISTS '||TG_TABLE_NAME||'_'||curMM||' 
(CHECK('||stat_date_column_name||'>='''|| startTime ||''' AND '  
                             ||stat_date_column_name||'< '''|| endTime ||''' )) 
INHERITS ('||TG_TABLE_NAME||');';    
        EXECUTE strSQL;  
  
        -- 创建主键
        strSQL := 'ALTER TABLE '||TG_TABLE_NAME||'_'||curMM||' ADD PRIMARY KEY ("id");';
        EXECUTE strSQL;
        -- 创建索引  
        strSQL := 'CREATE INDEX '||TG_TABLE_NAME||'_'||curMM||'_INDEX_stat_date ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(stat_date);' ;  
        EXECUTE strSQL;
        strSQL := 'CREATE INDEX '||TG_TABLE_NAME||'_'||curMM||'_INDEX_station_id ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(station_id);' ;  
        EXECUTE strSQL;
strSQL := 'CREATE UNIQUE INDEX '||TG_TABLE_NAME||'_'||curMM||'_s_a_unique_key ON '  
                  ||TG_TABLE_NAME||'_'||curMM||' USING btree(stat_date,account_id);' ;  
        EXECUTE strSQL;
        
    END IF;  
  
    -- 插入数据到子分区!  
    strSQL := 'INSERT INTO '||TG_TABLE_NAME||'_'||curMM||' AS a SELECT $1.*
on CONFLICT(account_id,stat_date) DO UPDATE SET ';

FOR tab_cols IN SELECT "column_name","data_type" from information_schema.columns
WHERE table_schema='public' AND table_name=TG_TABLE_NAME 
LOOP
 EXECUTE 'SELECT $1.'||tab_cols."column_name" INTO col_val USING NEW;  
      IF tab_cols."column_name" <> 'account_id' AND tab_cols."column_name" <> 'station_id'
AND tab_cols."column_name" <> 'id' AND tab_cols."column_name" <> 'agent_id'
AND tab_cols."column_name" <> 'balance' AND tab_cols."column_name" <> 'account_type'
AND tab_cols."column_name" <> 'report_type'
AND (tab_cols."data_type" = 'integer' 
OR tab_cols."data_type" = 'numeric')
AND col_val::numeric <> 0 
THEN
        strSQL := strSQL ||tab_cols."column_name"||'=COALESCE(a.'||tab_cols."column_name"||',0)+EXCLUDED.'||tab_cols."column_name"||',';
      END IF;     
    END LOOP;
strSQL := strSQL ||'balance=EXCLUDED.balance;';

    EXECUTE strSQL USING NEW;  
    RETURN NULL;   
END  
$BODY$
	LANGUAGE plpgsql
	COST 100
	CALLED ON NULL INPUT
	SECURITY INVOKER
	VOLATILE;
ALTER FUNCTION "public"."auto_insert_into_guide_money_partition"() OWNER TO "postgres";