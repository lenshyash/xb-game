
ALTER TABLE "public"."sys_account_daily_money" 
 ADD COLUMN "third_lottery_bet_amount" numeric(20,5) DEFAULT 0,
 ADD COLUMN "third_lottery_win_amount" numeric(20,5) DEFAULT 0,
 ADD COLUMN "third_lottery_bet_times" numeric(20,5) DEFAULT 0,
 ADD COLUMN "third_lottery_win_times" numeric(20,5) DEFAULT 0,
  ADD COLUMN "third_lottery_rebate_amount" numeric(20,5) DEFAULT 0,
 
 ADD COLUMN "chess_bet_amount" numeric(20,5) DEFAULT 0,
 ADD COLUMN "chess_win_amount" numeric(20,5) DEFAULT 0,
 ADD COLUMN "chess_bet_times" numeric(20,5) DEFAULT 0,
 ADD COLUMN "chess_win_times" numeric(20,5) DEFAULT 0,
  ADD COLUMN "chess_rebate_amount" numeric(20,5) DEFAULT 0,
 
 ADD COLUMN "third_sports_rebate_amount" numeric(20,5) DEFAULT 0;

COMMENT ON COLUMN "public"."sys_account_daily_money"."chess_bet_amount" IS '棋牌投注金额';
COMMENT ON COLUMN "public"."sys_account_daily_money"."chess_win_amount" IS '棋牌中奖金额';
COMMENT ON COLUMN "public"."sys_account_daily_money"."chess_bet_times" IS '棋牌投注次数';
COMMENT ON COLUMN "public"."sys_account_daily_money"."chess_win_times" IS '棋牌中奖次数';
COMMENT ON COLUMN "public"."sys_account_daily_money"."chess_rebate_amount" IS '棋牌反水金额';

COMMENT ON COLUMN "public"."sys_account_daily_money"."third_lottery_bet_amount" IS '三方彩票投注金额';
COMMENT ON COLUMN "public"."sys_account_daily_money"."third_lottery_win_amount" IS '三方彩票中奖金额';
COMMENT ON COLUMN "public"."sys_account_daily_money"."third_lottery_bet_times" IS '三方彩票投注次数';
COMMENT ON COLUMN "public"."sys_account_daily_money"."third_lottery_win_times" IS '三方彩票中奖次数';
COMMENT ON COLUMN "public"."sys_account_daily_money"."third_lottery_rebate_amount" IS '三方彩票反水金额';

COMMENT ON COLUMN "public"."sys_account_daily_money"."third_sports_rebate_amount" IS '三方体育反水金额';


INSERT INTO "public"."agent_menu"( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") 
VALUES ('三方彩票记录', '/agent/rlotbet/index.do', 6, 7, 2, 2, NULL, '', '/agent/rlotbet', '');

INSERT INTO "public"."agent_menu"( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") 
VALUES ('棋牌记录', '/agent/rchessbet/index.do', 7, 7, 2, 2, NULL, '', '/agent/rchessbet', '');


insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values (  'on', '', '[{"off":"关闭"},{"on":"开启"}]', '棋牌开关(大栏目)', '1', '', '6', 'onoff_chess', 'select', '25', '3', '棋牌开关(大栏目)');

insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values (  'on', '', '[{"off":"关闭"},{"on":"开启"}]', '第三方彩票开关(大栏目)', '1', '', '6', 'onoff_third_lottery', 'select', '25', '3', '第三方彩票开关(大栏目)');