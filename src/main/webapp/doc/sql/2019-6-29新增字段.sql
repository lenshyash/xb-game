ALTER TABLE "public"."mny_com_record"
ADD COLUMN "first_deposit" int4;

COMMENT ON COLUMN "public"."mny_com_record"."first_deposit" IS '首充标识 1普通充值 2首次充值';

ALTER TABLE "public"."mny_draw_record"
ADD COLUMN "first_draw" int4;

COMMENT ON COLUMN "public"."mny_draw_record"."first_draw" IS '首提标识 1普通提款 2首次提款';

INSERT INTO "public"."agent_base_config" ("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('会员银行卡账号显示开关', 'member_cardno_show', '', 'select', '会员银行卡账号显示开关', '', 'off', '2', '[{"off":"关闭"},{"on":"开启"}]', '6', '1', '60');
