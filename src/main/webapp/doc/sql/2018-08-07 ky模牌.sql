insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( 'off', '', '[{"off":"关闭"},{"on":"开启"}]', 'KY维护开关', '2', '', '18', 'sys_ky_maintenance_onoff', 'select', '15', '2', 'KY维护开关');
insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( 'on', '', '[{"off":"关闭"},{"on":"开启"}]', 'KY游戏开关(真人娱乐小栏目)', '1', '', '6', 'onoff_ky_game', 'select', '99', '3', 'KY游戏开关(真人娱乐小栏目)');
insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( '1', '', '', '真人KY转换限制', '1', '', '14', 'live_transfor_ky_limit', 'text', '20', '3', '真人KY转换限制');
