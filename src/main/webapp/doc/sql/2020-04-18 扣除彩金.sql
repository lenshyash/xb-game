ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "withdraw_gift_amount" numeric(32,5);
COMMENT ON COLUMN "public"."sys_account_daily_money"."withdraw_gift_amount" IS '扣除彩金总计';

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "withdraw_gift_times" numeric(20);
COMMENT ON COLUMN "public"."sys_account_daily_money"."withdraw_gift_times" IS '扣除彩金次数';

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "withdraw_gift_amount" numeric(32,5);
COMMENT ON COLUMN "public"."sys_account_guide_money"."withdraw_gift_amount" IS '扣除彩金总计';

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "withdraw_gift_times" numeric(20);
COMMENT ON COLUMN "public"."sys_account_guide_money"."withdraw_gift_times" IS '扣除彩金次数';