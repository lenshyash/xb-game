INSERT INTO BC_LOTTERY_TIME(lot_code,action_no,action_time) VALUES ('HKMHLHC','1','20:30:00');


INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('香港马会六合彩', 2, 'HKMHLHC', 600, 33, 6, 7, 0, 66, 2, 3, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('香港马会六合彩', 1, 'HKMHLHC', 600, 60, 6, 7, 0, 66, 1, 1, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('香港马会六合彩', 2, 'HKMHLHC', 600, 165, 6, 7, 0, 66, 2, 2, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('香港马会六合彩', 2, 'HKMHLHC', 600, 165, 6, 7, 0, 66, 2, 5, NULL, 1, 1);

INSERT INTO "public"."agent_base_config"("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('香港马会六合彩开奖模式', 'model_hkmhlhc_generate_haoMa', '', 'select', '香港马会六合彩开奖模式', '', 'pct', 2, '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', 7, 1, 16);
