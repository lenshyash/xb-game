ALTER TABLE "public"."sys_register_config_group"
ADD COLUMN "name_val" varchar(50),
ADD COLUMN "remind_text" varchar(100);

COMMENT ON COLUMN "public"."sys_register_config_group"."name_val" IS '配置名称';

COMMENT ON COLUMN "public"."sys_register_config_group"."remind_text" IS '提示文字';

