
CREATE SEQUENCE "public"."agent_kefu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."agent_kefu_id_seq"', 1, true);

ALTER SEQUENCE "public"."agent_kefu_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_kefu" (
  "id" int4 NOT NULL DEFAULT nextval('agent_kefu_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "head_url" varchar(50) COLLATE "pg_catalog"."default",
  "type" int2,
  "hao_ma" varchar(50) COLLATE "pg_catalog"."default",
  "station_id" int4,
  "status" int2,
  CONSTRAINT "agent_kefu_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_kefu" 
  OWNER TO "postgres";

INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('客服信息管理', '/agent/system/agentkefu/index.do', 82, 19, 2, 2, NULL, '', '/agent/system/agentkefu', '');
