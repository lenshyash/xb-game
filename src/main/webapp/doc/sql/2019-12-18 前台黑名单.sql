ALTER TABLE "public"."member_white_ip"
ADD COLUMN "type" int2;

COMMENT ON COLUMN "public"."member_white_ip"."type" IS '1 白名单 2黑名单';


INSERT INTO "public"."agent_base_config" ( "name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('前台访问黑名单', 'front_black_ip_permission', '', 'select', '前台访问黑名单', '', 'off', '3', '[{"off":"关闭"},{"on":"开启"}]', '6', '1', '151');

INSERT INTO "public"."agent_menu" ("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('前台黑名单', '/agent/front/black/index.do', '1', '102', '2', '2', NULL, '', '/agent/front/black', '');
