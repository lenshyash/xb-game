ALTER TABLE "public"."mny_com_record" 
  ADD COLUMN "rate" numeric(12,2);

COMMENT ON COLUMN "public"."mny_com_record"."virtual_currency_amount" IS '数字货币充值数量';

COMMENT ON COLUMN "public"."mny_com_record"."rate" IS '数字货币汇率';

ALTER TABLE "public"."agent_deposit_virtual"
  ADD COLUMN "rate" numeric(12,2);

COMMENT ON COLUMN "public"."agent_deposit_virtual"."rate" IS '兑换汇率';

ALTER TABLE "public"."mny_com_record"
  ADD COLUMN "pay_account" varchar(50);

COMMENT ON COLUMN "public"."mny_com_record"."pay_account" IS '支付账号';