ALTER TABLE "public"."app_update"
ADD COLUMN "station_id" int4;

COMMENT ON COLUMN "public"."app_update"."station_id" IS '站点ID';