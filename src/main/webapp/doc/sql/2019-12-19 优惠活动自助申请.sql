ALTER TABLE "public"."agent_activity" ADD COLUMN "apply_selected" VARCHAR(200);
COMMENT ON COLUMN "public"."agent_activity"."apply_selected" IS '活动申请得selected';


ALTER TABLE "public"."agent_activity" ADD COLUMN "apply_flag" int4;
COMMENT ON COLUMN "public"."agent_activity"."apply_flag" IS '是否可以申请标识 2可以申请 1禁止申请';

CREATE SEQUENCE "public"."member_activity_apply_record_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."member_activity_apply_record_id_seq"', 55, true);

ALTER SEQUENCE "public"."member_activity_apply_record_id_seq" OWNER TO "postgres";


CREATE TABLE "public"."member_activity_apply_record" (
  "id" int4 NOT NULL DEFAULT nextval('member_activity_apply_record_id_seq'::regclass),
  "activity_name" varchar(100) COLLATE "pg_catalog"."default",
  "activity_id" int4,
  "status" int4,
  "station_id" int4 NOT NULL,
  "remark" varchar(50) COLLATE "pg_catalog"."default",
  "account_id" int4,
  "account" varchar(20) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "activity_option" varchar(100) COLLATE "pg_catalog"."default",
  CONSTRAINT "member_activity_record_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."member_activity_apply_record" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."member_activity_apply_record"."activity_name" IS '优惠活动名字';

COMMENT ON COLUMN "public"."member_activity_apply_record"."activity_id" IS '优惠活动Id';

COMMENT ON COLUMN "public"."member_activity_apply_record"."status" IS '处理状态(1、未处理，2、已处理，3、处理失败)';

COMMENT ON COLUMN "public"."member_activity_apply_record"."station_id" IS '站点id';

COMMENT ON COLUMN "public"."member_activity_apply_record"."remark" IS '备注';

COMMENT ON COLUMN "public"."member_activity_apply_record"."account_id" IS '用户id';

COMMENT ON COLUMN "public"."member_activity_apply_record"."account" IS '用户名';

COMMENT ON COLUMN "public"."member_activity_apply_record"."create_time" IS '创建时间';

COMMENT ON COLUMN "public"."member_activity_apply_record"."activity_option" IS '优惠活动申请option';