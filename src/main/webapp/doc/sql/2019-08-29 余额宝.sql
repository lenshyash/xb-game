
CREATE SEQUENCE "public"."member_balance_gem_record_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

ALTER TABLE "public"."member_balance_gem_record_id_seq" OWNER TO "postgres";

SELECT setval('"public"."member_balance_gem_record_id_seq"',1, true);



CREATE SEQUENCE "public"."member_balance_gem_strategy_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

ALTER TABLE "public"."member_balance_gem_strategy_id_seq" OWNER TO "postgres";

SELECT setval('"public"."member_balance_gem_strategy_id_seq"', 1, true);


CREATE SEQUENCE "public"."member_balance_gem_strategy_level_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

ALTER TABLE "public"."member_balance_gem_strategy_level_id_seq" OWNER TO "postgres";

SELECT setval('"public"."member_balance_gem_strategy_level_id_seq"', 1, true);




CREATE TABLE "public"."member_balance_gem_record" (
"id" int4 DEFAULT nextval('member_balance_gem_record_id_seq'::regclass) NOT NULL,
"order_no" varchar(20) COLLATE "default",
"account_id" int4,
"money" numeric(20,6),
"create_datetime" timestamp(6),
"create_user_id" int4,
"status" int4,
"station_id" int4,
"remark" varchar(200) COLLATE "default",
"account" varchar(20) COLLATE "default",
"type" int4 DEFAULT 1,
"before_money" numeric(20,6),
"back_money" numeric(20,6),
"rebate_date" date,
CONSTRAINT "member_balance_gem_record_pkey" PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."member_balance_gem_record" OWNER TO "postgres";

COMMENT ON TABLE "public"."member_balance_gem_record" IS '余额宝记录表';

COMMENT ON COLUMN "public"."member_balance_gem_record"."order_no" IS '订单号';

COMMENT ON COLUMN "public"."member_balance_gem_record"."account_id" IS '会员ID';

COMMENT ON COLUMN "public"."member_balance_gem_record"."money" IS '存取金额';

COMMENT ON COLUMN "public"."member_balance_gem_record"."create_datetime" IS '创建时间';

COMMENT ON COLUMN "public"."member_balance_gem_record"."create_user_id" IS '创建人';

COMMENT ON COLUMN "public"."member_balance_gem_record"."status" IS '状态';

COMMENT ON COLUMN "public"."member_balance_gem_record"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."member_balance_gem_record"."remark" IS '备注';

COMMENT ON COLUMN "public"."member_balance_gem_record"."account" IS '用户账号';

COMMENT ON COLUMN "public"."member_balance_gem_record"."type" IS '类型1余额宝盈利 2转入金额 3转出金额';

COMMENT ON COLUMN "public"."member_balance_gem_record"."before_money" IS '变动前金额';

COMMENT ON COLUMN "public"."member_balance_gem_record"."back_money" IS '变动后金额';

COMMENT ON COLUMN "public"."member_balance_gem_record"."rebate_date" IS '返利日期';



CREATE INDEX "mbg_order_no" ON "public"."member_balance_gem_record" USING btree ("order_no");




CREATE TABLE "public"."member_balance_gem_strategy" (
"id" int4 DEFAULT nextval('member_balance_gem_strategy_id_seq'::regclass) NOT NULL,
"station_id" int4,
"bet_multiple" varchar(255) COLLATE "default",
"create_datetime" timestamp(6),
"min_money" numeric(11,2),
"max_money" numeric(11,2),
"benchmark_annual_yield" numeric(11,2),
"hongyun_annual_yield" numeric(11,2),
"status" int2,
"remark" varchar(255) COLLATE "default",
CONSTRAINT "member_balance_gem_strategy_pkey" PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."member_balance_gem_strategy" OWNER TO "postgres";

COMMENT ON TABLE "public"."member_balance_gem_strategy" IS '余额宝策略表';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."bet_multiple" IS '打码量倍数';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."create_datetime" IS '创建时间';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."min_money" IS '最小余额';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."max_money" IS '最大余额';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."benchmark_annual_yield" IS '基准年化收益率';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."hongyun_annual_yield" IS '红运年华收益率';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."status" IS '状态 1=禁用，2=启用';

COMMENT ON COLUMN "public"."member_balance_gem_strategy"."remark" IS '备注';






CREATE TABLE "public"."member_balance_gem_strategy_level" (
"id" int4 DEFAULT nextval('member_balance_gem_strategy_level_id_seq'::regclass) NOT NULL,
"member_level_id" int4,
"station_id" int4,
"strategy_id" int4
)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."member_balance_gem_strategy_level" OWNER TO "postgres";

COMMENT ON TABLE "public"."member_balance_gem_strategy_level" IS '余额宝策略等级关联表';

COMMENT ON COLUMN "public"."member_balance_gem_strategy_level"."member_level_id" IS '会员等级id';

COMMENT ON COLUMN "public"."member_balance_gem_strategy_level"."station_id" IS '站点id';

COMMENT ON COLUMN "public"."member_balance_gem_strategy_level"."strategy_id" IS '存款策略id';






ALTER TABLE "public"."mny_money"
ADD COLUMN "balance_gem_money" numeric(20,6);

COMMENT ON COLUMN "public"."mny_money"."balance_gem_money" IS '余额宝余额';

ALTER TABLE "public"."mny_money"
ADD COLUMN "balance_gem_income" numeric(20,6);

COMMENT ON COLUMN "public"."mny_money"."balance_gem_income" IS '余额宝累计收益';




CREATE OR REPLACE FUNCTION "public"."optbgmoney"(userid int4, bgmoney numeric, bgincome numeric)
  RETURNS "pg_catalog"."_numeric" AS $BODY$ DECLARE 
	mny numeric(19,6);
	mnyi numeric(19,6);
	sql1 varchar;
	sql2 varchar;
	sql3 VARCHAR;
	result numeric[2];
 BEGIN
    sql1 := 'select balance_gem_money as bgm,balance_gem_income as bgi from mny_money where account_id = ''' || userId ||''' for update ';
    execute sql1 into mny;
		IF mny is NULL THEN mny = 0; END IF;
    IF mny + bgMoney < 0 AND bgMoney < 0 THEN
			result[0] := 0;
			result[1] := mny;
			result[2]	:= bgIncome;
			return result;
		END IF;
		bgMoney = mny + bgMoney;
		IF bgIncome <= 0 THEN
			sql2 := 'update mny_money set balance_gem_money = ''' || bgMoney ||''' where  account_id = ''' || userId ||'''';
    ELSE
			sql3 := 'select balance_gem_income as bgm from mny_money where account_id = ''' || userId ||''' for update ';
			execute sql3 into mnyi;
			IF mnyi is NULL THEN mnyi = 0; END IF;
			bgIncome = mnyi + bgIncome;
			sql2 := 'update mny_money set balance_gem_money = ''' || bgMoney ||''',balance_gem_income = ''' || bgIncome ||''' where  account_id = ''' || userId ||'''';
		END IF;
		execute sql2;
    result[0] := 1;
    result[1] := bgMoney;
		result[2] := bgIncome;
    RETURN result;
 END;
 $BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."optbgmoney"(userid int4, bgmoney numeric, bgincome numeric) OWNER TO "postgres";


INSERT INTO "public"."agent_menu" ( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ( '余额宝管理', NULL, '0', '75', '1', '2', NULL, '', NULL, 'glyphicon glyphicon-gift');
INSERT INTO "public"."agent_menu" ( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ( '余额宝列表', '/agent/finance/balanceGem/index.do',(select id from agent_menu where "name"='余额宝管理'), '1', '2', '2', NULL, '', '/agent/finance/balanceGem', '');
INSERT INTO "public"."agent_menu" ( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ( '余额宝记录', '/agent/balanceGemRecord/index.do',(select id from agent_menu where "name"='余额宝管理'), '2', '2', '2', NULL, '', '/agent/balanceGemRecord', '');
INSERT INTO "public"."agent_menu" ( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ( '余额宝策略', '/agent/finance/balanceGemStrategy/index.do',(select id from agent_menu where "name"='余额宝管理'), '3', '2', '2', NULL, '', '/agent/finance/balanceGemStrategy', '');


INSERT INTO "public"."agent_base_config" ("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('站点余额宝开关', 'onoff_balance_gem', '', 'select', '站点余额宝开关', '', 'off', '2', '[{"off":"关闭"},{"on":"开启"},{"ban":"禁止转入"}]', '9', '1', '300');

ALTER TABLE "public"."sys_account_daily_money"
ADD COLUMN "balance_gem_amount" numeric(20,5);

COMMENT ON COLUMN "public"."sys_account_daily_money"."balance_gem_amount" IS '余额宝返利';

ALTER TABLE "public"."sys_account_guide_money"
ADD COLUMN "balance_gem_amount" numeric(20,5);

COMMENT ON COLUMN "public"."sys_account_guide_money"."balance_gem_amount" IS '余额宝返利';


