CREATE SEQUENCE "public"."sys_account_warning_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

ALTER TABLE "public"."sys_account_warning_id_seq" OWNER TO "postgres";
CREATE TABLE "public"."sys_account_warning" (
"id" int4 DEFAULT nextval('sys_account_warning_id_seq'::regclass) NOT NULL,
"account_id" int4,
"account" varchar(255) COLLATE "default",
"station_id" int4,
"type" int4,
"status" int2,
"create_time" date,
"remark" varchar(255) COLLATE "default",
"modify_time" date,
"modifyed" varchar(50) COLLATE "default",
"level" int2,
CONSTRAINT "sys_account_warning_pkey" PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."sys_account_warning" OWNER TO "postgres";

COMMENT ON TABLE "public"."sys_account_warning" IS '预警用户表';

COMMENT ON COLUMN "public"."sys_account_warning"."account_id" IS '用户ID';

COMMENT ON COLUMN "public"."sys_account_warning"."account" IS '用户账号';

COMMENT ON COLUMN "public"."sys_account_warning"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."sys_account_warning"."type" IS '类型';

COMMENT ON COLUMN "public"."sys_account_warning"."status" IS '状态';

COMMENT ON COLUMN "public"."sys_account_warning"."create_time" IS '创建时间';

COMMENT ON COLUMN "public"."sys_account_warning"."remark" IS '描述';

COMMENT ON COLUMN "public"."sys_account_warning"."modify_time" IS '修改时间';

COMMENT ON COLUMN "public"."sys_account_warning"."modifyed" IS '修改者';

COMMENT ON COLUMN "public"."sys_account_warning"."level" IS '预警等级';


ALTER TABLE "public"."sys_account"
ADD COLUMN "abnormal_flag" int2;
COMMENT ON COLUMN "public"."sys_account"."abnormal_flag" IS '预警标识';





INSERT INTO "public"."sys_cache" ("name", "key", "expression", "remark", "data_type", "db", "timeout") VALUES ('用户预警', 'AGENT_STATION_ACCOUNT_WARN_COUNT', '{key}_{station}', 'stationId:站点ID', 'java.lang.Integer', '1', '300');



INSERT INTO "public"."agent_base_config" ("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('预警功能关闭开关', 'on_off_user_warning', '', 'select', '预警功能关闭开关', '', 'on', '3', '[{"off":"关闭"},{"on":"开启"}]', '9', '1', '220');

INSERT INTO "public"."agent_menu" ("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ( '预警用户管理', '/agent/member/waring/index.do', '3', '37', '2', '2', NULL, '', '/agent/member/warning', '');




