ALTER TABLE "public"."bc_lottery_order" ADD COLUMN "bet_ip" VARCHAR(100);
COMMENT ON COLUMN "public"."bc_lottery_order"."bet_ip" IS '投注IP'; 

ALTER TABLE "public"."mny_com_record" ADD COLUMN "com_times" int4;
COMMENT ON COLUMN "public"."mny_com_record"."com_times" IS '第几次存款'; 

ALTER TABLE "public"."mny_draw_record" ADD COLUMN "draw_times" int4;
COMMENT ON COLUMN "public"."mny_draw_record"."draw_times" IS '第几次取款'; 