--三分11选五
insert into bc_lottery_time (lot_code,action_no,action_time) select 'SF11X5',action_no,action_time from bc_lottery_time where lot_code = 'TMK3';

INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分11选5', 1, 'SF11X5', 30, 30, 5, 5, 0, 5, 1, 1, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分11选5', 2, 'SF11X5', 30, 30, 5, 5, 0, 55, 2, 3, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分11选5', 1, 'SF11X5', 30, 137, 5, 5, 0, 14, 1, 2, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分11选5', 2, 'SF11X5', 30, 137, 5, 5, 0, 14, 2, 5, NULL, 1, 1, NULL, NULL);

insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('pct', '', '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', '三分11选5开奖模式', '1', '', '7', 'model_sf11x5_generate_haoMa', 'select', '15', '2', '三分11选5开奖模式') RETURNING *;
insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('50', '', '', '三分11选5开奖中奖百分比，开奖模式为中奖百分比后才生效', '1', '', '7', 'sys_sf11x5_run_percentage', 'text', '16', '2', '三分11选5开奖中奖百分比') RETURNING *;

--五分11选5
insert into bc_lottery_time (lot_code,action_no,action_time) select 'WF11X5',action_no,action_time from bc_lottery_time where lot_code = 'WFC';

INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分11选5', 1, 'WF11X5', 30, 30, 5, 5, 0, 5, 1, 1, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分11选5', 2, 'WF11X5', 30, 30, 5, 5, 0, 55, 2, 3, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分11选5', 1, 'WF11X5', 30, 137, 5, 5, 0, 14, 1, 2, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分11选5', 2, 'WF11X5', 30, 137, 5, 5, 0, 14, 2, 5, NULL, 1, 1, NULL, NULL);

insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('pct', '', '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', '五分11选5开奖模式', '1', '', '7', 'model_wf11x5_generate_haoMa', 'select', '15', '2', '五分11选5开奖模式') RETURNING *;
insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('50', '', '', '五分11选5开奖中奖百分比，开奖模式为中奖百分比后才生效', '1', '', '7', 'sys_wf11x5_run_percentage', 'text', '16', '2', '五分11选5开奖中奖百分比') RETURNING *;

--三分快乐十分

insert into bc_lottery_time (lot_code,action_no,action_time) select 'SFKLSF',action_no,action_time from bc_lottery_time where lot_code = 'TMK3';

INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分快乐十分', 2, 'SFKLSF', 30, 170, 8, 8, 0, 12, 2, 2, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('三分快乐十分', 2, 'SFKLSF', 30, 170, 8, 8, 0, 12, 2, 5, NULL, 1, 1, NULL, NULL);

insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('pct', '', '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', '三分快乐十分开奖模式', '1', '', '7', 'model_sfklsf_generate_haoMa', 'select', '15', '2', '三分快乐十分开奖模式') RETURNING *;
insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('50', '', '', '三分快乐十分开奖中奖百分比，开奖模式为中奖百分比后才生效', '1', '', '7', 'sys_sfklsf_run_percentage', 'text', '16', '2', '三分快乐十分开奖中奖百分比') RETURNING *;

--五分快乐十分

insert into bc_lottery_time (lot_code,action_no,action_time) select 'WFKLSF',action_no,action_time from bc_lottery_time where lot_code = 'WFC';

INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分快乐十分', 2, 'WFKLSF', 30, 170, 8, 8, 0, 12, 2, 2, NULL, 1, 1, NULL, NULL);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game", "limit_account", "gif_url") VALUES ('五分快乐十分', 2, 'WFKLSF', 30, 170, 8, 8, 0, 12, 2, 5, NULL, 1, 1, NULL, NULL);

insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('pct', '', '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', '五分快乐十分开奖模式', '1', '', '7', 'model_wfklsf_generate_haoMa', 'select', '15', '2', '五分快乐十分开奖模式') RETURNING *;
insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('50', '', '', '五分快乐十分开奖中奖百分比，开奖模式为中奖百分比后才生效', '1', '', '7', 'sys_wfklsf_run_percentage', 'text', '16', '2', '五分快乐十分开奖中奖百分比') RETURNING *;

--五分3D

insert into bc_lottery_time (lot_code,action_no,action_time) select 'WF3D',action_no,action_time from bc_lottery_time where lot_code = 'WFC';

INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('五分3D', 2, 'WF3D', 10, 55, 2, 3, 0, 4, 2, 1, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('五分3D', 2, 'WF3D', 10, 55, 2, 3, 0, 54, 2, 3, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('五分3D', 1, 'WF3D', 10, 142, 2, 3, 0, 15, 1, 2, NULL, 1, 1);
INSERT INTO "public"."bc_lottery"("name", "status", "code", "ago", "sort_no", "view_group", "balls", "station_id", "type", "model_status", "identify", "img_url", "hot_game", "new_game") VALUES ('五分3D', 2, 'WF3D', 10, 142, 2, 3, 0, 15, 2, 5, NULL, 1, 1);

insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('pct', '', '[{"rand":"随机"},{"pct":"中奖百分比"},{"push":"从正式站推送开奖结果"}]', '五分3D开奖模式', '1', '', '7', 'model_wf3d_generate_haoMa', 'select', '15', '2', '五分3D开奖模式') RETURNING *;
insert into "public"."agent_base_config" ("init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") values ('50', '', '', '五分3D开奖中奖百分比，开奖模式为中奖百分比后才生效', '1', '', '7', 'sys_wf3d_run_percentage', 'text', '16', '2', '五分3D开奖中奖百分比') RETURNING *;





