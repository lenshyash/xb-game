ALTER TABLE "public"."agent_deposit_online" ADD COLUMN "random_flag" int4;
COMMENT ON COLUMN "public"."agent_deposit_online"."random_flag" IS '随机金额开关';

ALTER TABLE "public"."agent_deposit_online" ADD COLUMN "fixed_flag" int4;
COMMENT ON COLUMN "public"."agent_deposit_online"."fixed_flag" IS '固定金额开关';

ALTER TABLE "public"."agent_deposit_online" ADD COLUMN "fixed_amount" varchar(255);
COMMENT ON COLUMN "public"."agent_deposit_online"."fixed_amount" IS '用户充值固定金额比如100,200,300';

ALTER TABLE "public"."agent_deposit_online" ADD COLUMN "random_amount" varchar(255);
COMMENT ON COLUMN "public"."agent_deposit_online"."random_amount" IS '随机金额0.11,0.12';