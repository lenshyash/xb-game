ALTER TABLE "public"."sys_account"
  ADD COLUMN "login_odd_flag" int2;

COMMENT ON COLUMN "public"."sys_account"."login_odd_flag" IS '异常登陆标识1正常2异常3解除';