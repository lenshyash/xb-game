CREATE SEQUENCE "public"."sys_account_guide_money_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."sys_account_guide_money_id_seq" OWNER TO "postgres";


CREATE TABLE "public"."sys_account_guide_money" (
"id" int4 DEFAULT nextval('sys_account_guide_money_id_seq'::regclass) NOT NULL,
"stat_date" date NOT NULL,
"account_id" int4 NOT NULL,
"account" varchar(50) COLLATE "default",
"account_type" int4,
"parents" text COLLATE "default" NOT NULL,
"station_id" int4 NOT NULL,
"withdraw_amount" numeric(20,5) DEFAULT 0,
"withdraw_times" int4 DEFAULT 0,
"withdraw_artificial" numeric(20,5) DEFAULT 0,
"deposit_amount" numeric(20,5) DEFAULT 0,
"deposit_times" int4 DEFAULT 0,
"deposit_artificial" numeric(20,5) DEFAULT 0,
"deposit_gift_amount" numeric(20,5) DEFAULT 0,
"deposit_gift_times" int4 DEFAULT 0,
"lottery_bet_amount" numeric(20,5) DEFAULT 0,
"lottery_win_amount" numeric(20,5) DEFAULT 0,
"lottery_rebate_amount" numeric(20,5) DEFAULT 0,
"mark_six_bet_amount" numeric(20,5) DEFAULT 0,
"mark_six_win_amount" numeric(20,5) DEFAULT 0,
"mark_six_rebate_amount" numeric(20,5) DEFAULT 0,
"real_bet_amount" numeric(20,5) DEFAULT 0,
"real_win_amount" numeric(20,5) DEFAULT 0,
"real_rebate_amount" numeric(20,5) DEFAULT 0,
"egame_bet_amount" numeric(20,5) DEFAULT 0,
"egame_win_amount" numeric(20,5) DEFAULT 0,
"egame_rebate_amount" numeric(20,5) DEFAULT 0,
"sports_bet_amount" numeric(20,5) DEFAULT 0,
"sports_win_amount" numeric(20,5) DEFAULT 0,
"sports_rebate_amount" numeric(20,5) DEFAULT 0,
"balance" numeric(20,5) DEFAULT 0,
"proxy_rebate_amount" numeric(20,5) DEFAULT 0,
"hunter_bet_amount" numeric(20,5) DEFAULT 0,
"hunter_win_amount" numeric(20,5) DEFAULT 0,
"hunter_rebate_amount" numeric(20,5) DEFAULT 0,
"agent_id" int4,
"agent_name" varchar(50) COLLATE "default",
"lottery_bet_times" int4 DEFAULT 0,
"lottery_win_times" int4 DEFAULT 0,
"mark_six_bet_times" int4 DEFAULT 0,
"mark_six_win_times" int4 DEFAULT 0,
"sports_bet_times" int4 DEFAULT 0,
"sports_win_times" int4 DEFAULT 0,
"egame_bet_times" int4 DEFAULT 0,
"egame_win_times" int4 DEFAULT 0,
"real_bet_times" int4 DEFAULT 0,
"real_win_times" int4 DEFAULT 0,
"hunter_bet_times" int4 DEFAULT 0,
"hunter_win_times" int4 DEFAULT 0,
"deposit_handler_artificial" numeric(20,5) DEFAULT 0,
"deposit_handler_artificial_times" int4 DEFAULT 0,
"register_gift_amount" numeric(20,5) DEFAULT 0,
"sys_lottery_bet_amount" numeric(20,5) DEFAULT 0,
"sys_lottery_win_amount" numeric(20,5) DEFAULT 0,
"sys_lottery_bet_times" int4 DEFAULT 0,
"sys_lottery_win_times" int4 DEFAULT 0,
"sys_lottery_rebate_amount" numeric(20,5) DEFAULT 0,
"active_award_amount" numeric(20,5) DEFAULT 0,
"active_award_times" int4 DEFAULT 0,
"sf_mark_six_bet_amount" numeric(20,5) DEFAULT 0,
"sf_mark_six_win_amount" numeric(20,5) DEFAULT 0,
"sf_mark_six_bet_times" int4 DEFAULT 0,
"sf_mark_six_win_times" int4 DEFAULT 0,
"sf_mark_six_rebate_amount" numeric(20,5) DEFAULT 0,
"sys_api_deposit_amount" numeric(20,5) DEFAULT 0,
"sys_api_deposit_times" int4 DEFAULT 0,
"third_sports_bet_amount" numeric(20,5) DEFAULT 0,
"third_sports_win_amount" numeric(20,5) DEFAULT 0,
"third_sports_bet_times" numeric(20,5) DEFAULT 0,
"third_sports_win_times" numeric(20,5) DEFAULT 0,
"level_up_gift_amount" numeric(20,5) DEFAULT 0,
"third_lottery_bet_amount" numeric(20,5) DEFAULT 0,
"third_lottery_win_amount" numeric(20,5) DEFAULT 0,
"third_lottery_bet_times" numeric(20,5) DEFAULT 0,
"third_lottery_win_times" numeric(20,5) DEFAULT 0,
"third_lottery_rebate_amount" numeric(20,5) DEFAULT 0,
"chess_bet_amount" numeric(20,5) DEFAULT 0,
"chess_win_amount" numeric(20,5) DEFAULT 0,
"chess_bet_times" numeric(20,5) DEFAULT 0,
"chess_win_times" numeric(20,5) DEFAULT 0,
"chess_rebate_amount" numeric(20,5) DEFAULT 0,
"third_sports_rebate_amount" numeric(20,5) DEFAULT 0,
"first_deposit" int4 DEFAULT 0,
"third_deposit_amount" numeric(20,5)
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."sys_account_guide_money"."stat_date" IS '统计日期';
COMMENT ON COLUMN "public"."sys_account_guide_money"."account_id" IS '系统用户id';
COMMENT ON COLUMN "public"."sys_account_guide_money"."account" IS '系统用户名';
COMMENT ON COLUMN "public"."sys_account_guide_money"."account_type" IS '系统用户类型';
COMMENT ON COLUMN "public"."sys_account_guide_money"."parents" IS '系统用户层级关系';
COMMENT ON COLUMN "public"."sys_account_guide_money"."station_id" IS '站点id';
COMMENT ON COLUMN "public"."sys_account_guide_money"."withdraw_amount" IS '取款金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."withdraw_times" IS '取款次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."withdraw_artificial" IS '手动扣款金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_amount" IS '存款金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_times" IS '存款次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_artificial" IS '手动加款金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_gift_amount" IS '存款赠送金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_gift_times" IS '存款赠送次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."lottery_bet_amount" IS '彩票投注';
COMMENT ON COLUMN "public"."sys_account_guide_money"."lottery_win_amount" IS '彩票中奖';
COMMENT ON COLUMN "public"."sys_account_guide_money"."lottery_rebate_amount" IS '彩票反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."mark_six_bet_amount" IS '六合彩投注';
COMMENT ON COLUMN "public"."sys_account_guide_money"."mark_six_win_amount" IS '六合彩中奖';
COMMENT ON COLUMN "public"."sys_account_guide_money"."mark_six_rebate_amount" IS '六合彩反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."real_bet_amount" IS '真人投注';
COMMENT ON COLUMN "public"."sys_account_guide_money"."real_win_amount" IS '真热中奖';
COMMENT ON COLUMN "public"."sys_account_guide_money"."real_rebate_amount" IS '真人反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."egame_bet_amount" IS '电子投注';
COMMENT ON COLUMN "public"."sys_account_guide_money"."egame_win_amount" IS '电子中奖';
COMMENT ON COLUMN "public"."sys_account_guide_money"."egame_rebate_amount" IS '电子反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sports_bet_amount" IS '体育投注';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sports_win_amount" IS '体育中奖';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sports_rebate_amount" IS '体育反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."balance" IS '系统用户账号上的余额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."proxy_rebate_amount" IS '代理反水';
COMMENT ON COLUMN "public"."sys_account_guide_money"."agent_id" IS '上级ID';
COMMENT ON COLUMN "public"."sys_account_guide_money"."lottery_bet_times" IS '彩票投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."lottery_win_times" IS '彩票中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."mark_six_bet_times" IS '六合彩投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."mark_six_win_times" IS '六合彩中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sports_bet_times" IS '体育投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sports_win_times" IS '体育中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."egame_bet_times" IS '电子投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."egame_win_times" IS '电子中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."real_bet_times" IS '真人投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."real_win_times" IS '真人中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."hunter_bet_times" IS '捕鱼王投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."hunter_win_times" IS '捕鱼王中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_handler_artificial" IS '手动处理存款数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_handler_artificial_times" IS '手动处理存款次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."register_gift_amount" IS '注册赠送金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."active_award_amount" IS '活动中奖金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."active_award_times" IS '活动中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sys_api_deposit_amount" IS '系统接口充值金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."sys_api_deposit_times" IS '系统接口充值次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_lottery_bet_amount" IS '三方彩票投注金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_lottery_win_amount" IS '三方彩票中奖金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_lottery_bet_times" IS '三方彩票投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_lottery_win_times" IS '三方彩票中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_lottery_rebate_amount" IS '三方彩票反水金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."chess_bet_amount" IS '棋牌投注金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."chess_win_amount" IS '棋牌中奖金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."chess_bet_times" IS '棋牌投注次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."chess_win_times" IS '棋牌中奖次数';
COMMENT ON COLUMN "public"."sys_account_guide_money"."chess_rebate_amount" IS '棋牌反水金额';
COMMENT ON COLUMN "public"."sys_account_guide_money"."third_sports_rebate_amount" IS '三方体育反水金额';


CREATE TRIGGER "auto_insert_into_guide_money_partition_trigger" BEFORE INSERT ON "public"."sys_account_guide_money"
FOR EACH ROW
EXECUTE PROCEDURE "auto_insert_into_guide_money_partition"('stat_date');



