ALTER TABLE "public"."agent_login_otp"
ADD COLUMN "type" int2,
ADD COLUMN "email" varchar(100);

COMMENT ON COLUMN "public"."agent_login_otp"."type" IS '1 动态密码，2谷歌验证码';

COMMENT ON COLUMN "public"."agent_login_otp"."email" IS '邮箱';

ALTER TABLE "public"."agent_login_otp"
ADD COLUMN "url" varchar(100);

COMMENT ON COLUMN "public"."agent_login_otp"."url" IS '扫码链接';

ALTER TABLE "public"."agent_login_otp_escape"
ADD COLUMN "otp_id" int4;

INSERT INTO "public"."agent_base_config" ("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ( '租户后台谷歌验证码开关', 'onoff_bg_login_google_verify_code', '', 'select', '租户后台谷歌验证码开关', '', 'off', '3', '[{"off":"关闭"},{"on":"开启"}]', '9', '1', '305');


CREATE INDEX "order_status_lotcode" ON "public"."bc_lottery_order" USING btree ("station_id", "lot_code", "status");

CREATE INDEX "order_station_acc" ON "public"."bc_lottery_order" USING btree ("account_id", "station_id", "create_time");