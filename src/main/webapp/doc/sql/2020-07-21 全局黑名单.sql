CREATE SEQUENCE "public"."admin_black_ip_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."admin_black_ip_seq"', 1, true);

ALTER SEQUENCE "public"."admin_black_ip_seq" OWNER TO "postgres";

CREATE TABLE "public"."admin_black_ip" (
  "id" int4 NOT NULL DEFAULT nextval('admin_black_ip_seq'::regclass),
  "ip" varchar(30) COLLATE "pg_catalog"."default",
  "create_datetime" timestamp(6)
)
;

ALTER TABLE "public"."admin_black_ip" 
  OWNER TO "postgres";
  
INSERT INTO "public"."admin_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('黑名单管理', '/admin/blackIp/index.do', (select id from admin_menu where name = '数据维护'), 50, 2, 2, NULL, '', '/admin/blackIp', '');