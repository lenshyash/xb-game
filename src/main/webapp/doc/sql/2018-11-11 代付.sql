CREATE SEQUENCE "public"."agent_payment_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."agent_payment_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_payment" (
	"id" int4 NOT NULL DEFAULT nextval('agent_payment_id_seq'::regclass),
	"merchant_code" varchar(36) COLLATE "default",
	"merchant_key" varchar(5000) COLLATE "default",
	"url" varchar(256) COLLATE "default",
	"account" varchar(5000) COLLATE "default",
	"status" int4,
	"station_id" int4,
	"pay_platform_id" int4,
	"pay_getway" varchar(500) COLLATE "default",
	"appid" varchar(500) COLLATE "default",
	"sort_no" int4,
	"pay_desc" text COLLATE "default",
	"pay_starttime" varchar COLLATE "default",
	"pay_endtime" varchar COLLATE "default",
	CONSTRAINT "agent_payment_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."agent_payment" OWNER TO "postgres";

insert into "public"."agent_menu" ( "status", "sort", "remark", "icon", "level", "module_path", "parent_id", "type", "name", "url") values ( '2', '46', '',  '', '2', '/agent/finance/payment', '1', null, '网站代付设置', '/agent/finance/payment/index.do');