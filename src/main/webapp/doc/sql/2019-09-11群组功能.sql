ALTER TABLE "public"."bc_lottery_order" ADD COLUMN "qz_group_name" VARCHAR(32);
COMMENT ON COLUMN "public"."bc_lottery_order"."qz_group_name" IS '群组名称';


ALTER TABLE "public"."sys_account_info" ADD COLUMN "qz_group_name" VARCHAR(32);
COMMENT ON COLUMN "public"."sys_account_info"."qz_group_name" IS '群组名称';


CREATE SEQUENCE "public"."qz_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."qz_group_id_seq"', 1, true);

ALTER SEQUENCE "public"."qz_group_id_seq" OWNER TO "postgres";


CREATE TABLE "public"."qz_group" (
  "id" int4 NOT NULL DEFAULT nextval('qz_group_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "station_id" int4,
  "status" int4,
  "remark" varchar(50) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."qz_group"."name" IS '群组名字';
COMMENT ON COLUMN "public"."qz_group"."station_id" IS '站点Id';
COMMENT ON COLUMN "public"."qz_group"."status" IS '状态 1 是正常 2是逻辑删除';
COMMENT ON COLUMN "public"."qz_group"."remark" IS '备注 ';

INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('群组报表管理', '/agent/report/group/index.do', 8, 50, 2, 2, NULL, '', '/agent/report/group', '');
INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('群组管理', '/agent/agent/qzGroup/index.do', 3, 80, 2, 2, NULL, '', '/agent/agent/qzGroup', '');
