ALTER TABLE "public"."sys_station_domain" 
  ADD COLUMN "interface_limit" int4,
  ADD COLUMN "limit_num" numeric(6,2);

COMMENT ON COLUMN "public"."sys_station_domain"."interface_limit" IS '域名访问限制开关 1禁用 2启用';

COMMENT ON COLUMN "public"."sys_station_domain"."limit_num" IS '域名访问单秒限制次数';

ALTER TABLE "public"."sys_station_domain"
  ADD COLUMN "remark" varchar(255);

COMMENT ON COLUMN "public"."sys_station_domain"."remark" IS '域名备注';