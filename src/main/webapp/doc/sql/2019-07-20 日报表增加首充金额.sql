ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "first_deposit_amount" numeric(20,5) DEFAULT 0;
COMMENT ON COLUMN "public"."sys_account_daily_money"."first_deposit_amount" IS '首充金额';

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "first_deposit_amount" numeric(20,5) DEFAULT 0;
COMMENT ON COLUMN "public"."sys_account_guide_money"."first_deposit_amount" IS '首充金额';