CREATE SEQUENCE "public"."agent_lottery_model_config_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."agent_lottery_model_config_id_seq"', 1, false);

ALTER SEQUENCE "public"."agent_lottery_model_config_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_lottery_model_config" (
  "id" int4 NOT NULL DEFAULT nextval('agent_lottery_model_config_id_seq'::regclass),
  "lot_code" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "station_id" int4 NOT NULL,
  "lottery_mode" int4,
  "percentage" numeric(20,2),
  "lot_type" int4,
  "lot_id" int4,
  CONSTRAINT "bc_lottery_model_config_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_lottery_model_config" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."agent_lottery_model_config"."lot_code" IS '彩种Code';

COMMENT ON COLUMN "public"."agent_lottery_model_config"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."agent_lottery_model_config"."lottery_mode" IS '开奖模式1随机2百分比3正式站推送';

COMMENT ON COLUMN "public"."agent_lottery_model_config"."percentage" IS '百分比';

COMMENT ON COLUMN "public"."agent_lottery_model_config"."lot_type" IS '彩种类型';

COMMENT ON COLUMN "public"."agent_lottery_model_config"."lot_id" IS '彩种ID';

COMMENT ON TABLE "public"."agent_lottery_model_config" IS '站点彩种模式配置表';

INSERT INTO "public"."agent_menu"( "name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('彩票开奖模式管理', '/agent/lotteryModel/index.do', 6, 15, 2, 2, NULL, '', '/agent/lotteryModel', '');


insert into agent_menu_group(group_id, station_id,menu_id) select a.group_id,a.station_id,b.uid from (
SELECT u.id,g.group_id,station_id FROM agent_menu u inner join agent_menu_group g on u."id" = g.menu_id where name in('彩票记录')
) a
left join  (
SELECT id as uid,parent_id FROM agent_menu where url 
in('/agent/lotteryModel/index.do')
) b on a.ID = b.parent_id
