CREATE TABLE "public"."sys_station_folder_url" (
"id" int4 NOT NULL,
"ios_dl_url" varchar(255),
"folder" varchar(255),
"station_id" int4,
"android_dl_url" varchar(255),
"ios_qr_url" varchar(255),
"android_qr_url" varchar(255),
"status" int4,
PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;

COMMENT ON TABLE "public"."sys_station_folder_url" IS '多模板站点app链接配置表';

COMMENT ON COLUMN "public"."sys_station_folder_url"."ios_dl_url" IS 'IOS版本app下载地址';

COMMENT ON COLUMN "public"."sys_station_folder_url"."folder" IS '模板编号';

COMMENT ON COLUMN "public"."sys_station_folder_url"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."sys_station_folder_url"."android_dl_url" IS 'Android版本app下载地址';

COMMENT ON COLUMN "public"."sys_station_folder_url"."ios_qr_url" IS 'IOS版本app二维码地址';

COMMENT ON COLUMN "public"."sys_station_folder_url"."status" IS '状态 1-停用 2-启用';

COMMENT ON COLUMN "public"."sys_station_folder_url"."android_qr_url" IS 'Android版本app二维码地址';

CREATE SEQUENCE "public"."sys_station_folder_url_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

ALTER TABLE "public"."sys_station_folder_url_id_seq" OWNER TO "postgres";
ALTER TABLE "public"."sys_station_folder_url"
ALTER COLUMN "id" SET DEFAULT nextval('sys_station_folder_url_id_seq'::regclass);

INSERT INTO "public"."agent_menu" ("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('模板APP链接管理', '/agent/system/applink/index.do', '82', '1', '2', '2', NULL, '模板APP链接管理', '/agent/system/applink', '');




