ALTER TABLE "public"."member_red_packet"   ADD COLUMN "deposit_grab_number1" varchar(200) ;
ALTER TABLE "public"."member_red_packet"   ADD COLUMN "deposit_grab_number2" varchar(200) ;
ALTER TABLE "public"."member_red_packet"   ADD COLUMN "deposit_grab_number3" varchar(200) ;
ALTER TABLE "public"."member_red_packet"   ADD COLUMN "deposit_grab_number4" varchar(200) ;
ALTER TABLE "public"."member_red_packet"   ADD COLUMN "deposit_grab_number5" varchar(200);
 
COMMENT ON COLUMN "public"."member_red_packet"."deposit_grab_number1" IS '充值达到多少可以抢多少次的区间1';
COMMENT ON COLUMN "public"."member_red_packet"."deposit_grab_number2" IS '充值达到多少可以抢多少次的区间2';
COMMENT ON COLUMN "public"."member_red_packet"."deposit_grab_number3" IS '充值达到多少可以抢多少次的区间3';
COMMENT ON COLUMN "public"."member_red_packet"."deposit_grab_number4" IS '充值达到多少可以抢多少次的区间4';
COMMENT ON COLUMN "public"."member_red_packet"."deposit_grab_number5" IS '充值达到多少可以抢多少次的区间5';