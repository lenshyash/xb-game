
CREATE SEQUENCE "public"."agent_adress_book_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."agent_adress_book_id_seq"', 1, true);

ALTER SEQUENCE "public"."agent_adress_book_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_adress_book" (
  "id" int4 NOT NULL DEFAULT nextval('agent_adress_book_id_seq'::regclass),
  "contact_name" varchar(50) COLLATE "pg_catalog"."default",
  "station_id" int4,
  "contact_phone" varchar(50) COLLATE "pg_catalog"."default",
  CONSTRAINT "agent_kefu_copy1_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_adress_book" 
  OWNER TO "postgres";

ALTER TABLE "public"."agent_adress_book" ADD COLUMN "account_id" int4;
ALTER TABLE "public"."agent_adress_book" ADD COLUMN "create_time" timestamp(6);

--总控
INSERT INTO "public"."admin_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('站点通讯录管理', '/admin/adressBook/index.do', (select id from admin_menu where name = '数据维护'), 50, 2, 2, NULL, '', '/admin/adressBook', '');
--平台
INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('通讯录管理', '/agent/system/agentAdressBook/index.do', (select id from agent_menu where name = '信息公告'), 19, 2, 2, NULL, '', '/agent/system/agentAdressBook', '');
INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('导出通讯录记录', '/agent/system/agentAdressBook/export.do', (select id from agent_menu where name = '通讯录管理'), 1, 3, 2, NULL, NULL, NULL, NULL);