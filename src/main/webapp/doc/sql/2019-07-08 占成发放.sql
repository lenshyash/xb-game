CREATE SEQUENCE "public"."agent_profit_share_handler_record_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."agent_profit_share_handler_record_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_profit_share_handler_record" (
	"id" int4 NOT NULL DEFAULT nextval('agent_profit_share_handler_record_id_seq'::regclass),
	"station_id" int4,
	"account_id" int4,
	"modify_datetime" timestamp(6) NULL,
	"account" varchar(50) COLLATE "default",
	"status" int2 DEFAULT 1,
	"agent_id" int4,
	"operator" varchar(50) COLLATE "default",
	"agent_name" varchar(20) COLLATE "default",
	"parents" varchar(255) COLLATE "default",
	"money" numeric(16,2),
	"create_datetime" timestamp(6) NULL,
	"parent_names" varchar(255) COLLATE "default",
	CONSTRAINT "agent_profit_share_record_copy_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."agent_profit_share_handler_record" OWNER TO "postgres";
COMMENT ON COLUMN "public"."agent_profit_share_handler_record"."status" IS '1未发放 2已发放';

insert into "public"."agent_menu" ( "status", "sort", "remark", "icon", "level", "module_path", "parent_id", "type", "name", "url") values ( '2', '28', '',  '', '2', '/agent/profitsharehandlerrecord', '3', null, '代理占成发放记录', '/agent/profitsharehandlerrecord/index.do');