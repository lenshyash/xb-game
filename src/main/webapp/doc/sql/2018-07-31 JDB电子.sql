insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( 'off', '', '[{"off":"关闭"},{"on":"开启"}]', 'JDB维护开关', '2', '', '18', 'sys_jdb_maintenance_onoff', 'select', '15', '2', 'JDB维护开关');
insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( 'on', '', '[{"off":"关闭"},{"on":"开启"}]', 'JDB游戏开关(真人娱乐小栏目)', '1', '', '6', 'onoff_jdb_game', 'select', '99', '3', 'JDB游戏开关(真人娱乐小栏目)');
insert into "public"."agent_base_config" ( "init_value", "expand", "source", "title", "platform", "remark", "group_id", "key", "type", "order_no", "status", "name") 
values ( '1', '', '', '真人JDB转换限制', '1', '', '14', 'live_transfor_jdb_limit', 'text', '20', '3', '真人JDB转换限制');
