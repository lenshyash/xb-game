ALTER TABLE "public"."sys_account" 
  ADD COLUMN "im_chat_id" varchar(32),
  ADD COLUMN "im_user_id" varchar(32);

COMMENT ON COLUMN "public"."sys_account"."im_chat_id" IS '新IM聊天室ID';

COMMENT ON COLUMN "public"."sys_account"."im_user_id" IS '新IM用户ID';

INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('新IM管理', '/agent/system/im/index.do', 1, 21, 2, 2, NULL, '', '/agent/system/im', '');

ALTER TABLE "public"."common_redpacket_record" 
  ADD COLUMN "im_user_id" varchar(32),
  ADD COLUMN "im_chat_id" varchar(32);

COMMENT ON COLUMN "public"."common_redpacket_record"."im_user_id" IS '新IM用户ID';

COMMENT ON COLUMN "public"."common_redpacket_record"."im_chat_id" IS '新IM聊天室ID';

INSERT INTO "public"."admin_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('新聊天室管理', '/admin/im/index.do', 15, 27, 2, 2, NULL, '', '/admin/im', '');
INSERT INTO "public"."agent_base_config"("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('新版聊天室开关', 'onoff_im_chat', '', 'select', '新版聊天室开关', '', 'off', 3, '[{"off":"关闭"},{"on":"开启"}]', 6, 1, 22);
INSERT INTO "public"."agent_base_config"("name", "key", "remark", "type", "title", "expand", "init_value", "status", "source", "group_id", "platform", "order_no") VALUES ('新聊天室服务器地址', 'sys_im_url', '', 'text', '新聊天室服务器地址', '', 'http://im.com:8050/im', 2, '', 15, 2, 6);
