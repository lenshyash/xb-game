CREATE TABLE "public"."member_withdraw_info" (
  "account_id" int4 NOT NULL,
  "withdraw_count" int4,
  "withdraw_total" numeric(13,2),
  CONSTRAINT "member_withdrawal_info_pkey" PRIMARY KEY ("account_id")
)
;

ALTER TABLE "public"."member_withdraw_info" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."member_withdraw_info"."account_id" IS '会员ID';

COMMENT ON COLUMN "public"."member_withdraw_info"."withdraw_count" IS '提款次数';

COMMENT ON COLUMN "public"."member_withdraw_info"."withdraw_total" IS '提款总额';



CREATE OR REPLACE FUNCTION "public"."opt_withdraw_info"("userid" int4, "count1" int4, "total" numeric)
  RETURNS "pg_catalog"."int4" AS $BODY$
    DECLARE 
	sql1 varchar;
	ncou1 int4;
 BEGIN
    sql1 := 'insert into member_withdraw_info as a (account_id,withdraw_count,withdraw_total) VALUES('
	||userid||','||count1||','||total||') ON CONFLICT (account_id) DO UPDATE '
	||'set withdraw_count=a.withdraw_count+EXCLUDED.withdraw_count,'
	||'withdraw_total=a.withdraw_total+EXCLUDED.withdraw_total RETURNING withdraw_count';
    execute sql1 into ncou1 ;
    RETURN ncou1;
 END;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  
  
ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "first_withdraw" int4;

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "first_withdraw_amount" numeric(20,5);
   
COMMENT ON COLUMN "public"."sys_account_daily_money"."first_withdraw" IS '首提次数';

COMMENT ON COLUMN "public"."sys_account_daily_money"."first_withdraw_amount" IS '首提金额';