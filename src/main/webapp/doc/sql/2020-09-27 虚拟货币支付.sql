CREATE SEQUENCE "public"."agent_deposit_virtual_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 138
CACHE 1;

SELECT setval('"public"."agent_deposit_virtual_id_seq"', 311, true);

ALTER SEQUENCE "public"."agent_deposit_virtual_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."agent_deposit_virtual" (
  "id" int4 NOT NULL DEFAULT nextval('agent_deposit_virtual_id_seq'::regclass),
  "pay_account" varchar(50) COLLATE "pg_catalog"."default",
  "qr_code_img" varchar(255) COLLATE "pg_catalog"."default",
  "min" numeric,
  "max" numeric,
  "status" int4,
  "station_id" int4,
  "icon" varchar(256) COLLATE "pg_catalog"."default",
  "pay_platform_id" int4,
  "level_group" int4 DEFAULT 0,
  "front_label" varchar(20) COLLATE "pg_catalog"."default",
  "sort_no" int4 DEFAULT 0,
  "pay_starttime" varchar COLLATE "pg_catalog"."default",
  "pay_endtime" varchar COLLATE "pg_catalog"."default",
  "desc" varchar(200) COLLATE "pg_catalog"."default",
  "block_chain_type" varchar(50) COLLATE "pg_catalog"."default",
  "reference_rate" varchar(50) COLLATE "pg_catalog"."default",
  "qrcode_desc" text COLLATE "pg_catalog"."default"
)
;

ALTER TABLE "public"."agent_deposit_virtual" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_account" IS '支付账号';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."qr_code_img" IS '二维码路径';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."min" IS '最小金额';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."max" IS '最大金额';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."status" IS '状态 ';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."icon" IS '图标样式';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_platform_id" IS '支付平台ID';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."level_group" IS '限制会员等级使用';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."sort_no" IS '排序';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_starttime" IS '时间段开始时间';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_endtime" IS '时间段结束时间';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."desc" IS '描述';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."block_chain_type" IS '链类型';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."reference_rate" IS '参考汇率 ';

ALTER TABLE "public"."agent_deposit_virtual" ADD COLUMN "tips_show_url" VARCHAR(200);
ALTER TABLE "public"."agent_deposit_virtual" ADD COLUMN "tips_redirect_url" VARCHAR(200);

ALTER TABLE "public"."mny_com_record" ADD COLUMN "virtual_currency_amount" numeric;

--------------------------------------------------新的虚拟----------------------------------------------------------------------------

drop table agent_deposit_virtual;

CREATE TABLE "public"."agent_deposit_virtual" (
  "id" int4 NOT NULL DEFAULT nextval('agent_deposit_virtual_id_seq'::regclass),
  "merchant_code" varchar(36) COLLATE "pg_catalog"."default",
  "merchant_key" varchar(5000) COLLATE "pg_catalog"."default",
  "url" varchar(256) COLLATE "pg_catalog"."default",
  "account" varchar(5000) COLLATE "pg_catalog"."default",
  "min" numeric,
  "max" numeric,
  "status" int4,
  "station_id" int4,
  "icon" varchar(256) COLLATE "pg_catalog"."default",
  "pay_platform_id" int4,
  "pay_getway" varchar(500) COLLATE "pg_catalog"."default",
  "level_group" varchar COLLATE "pg_catalog"."default" DEFAULT 0,
  "appid" varchar(5000) COLLATE "pg_catalog"."default",
  "sort_no" int4 DEFAULT 0,
  "show_type" varchar(20) COLLATE "pg_catalog"."default",
  "pay_desc" text COLLATE "pg_catalog"."default",
  "pay_starttime" varchar COLLATE "pg_catalog"."default",
  "pay_endtime" varchar COLLATE "pg_catalog"."default",
  "pay_channel" varchar(50) COLLATE "pg_catalog"."default",
  CONSTRAINT "agent_deposit_virtual_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_deposit_virtual" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."agent_deposit_virtual"."merchant_code" IS '商户编号';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."merchant_key" IS '商户密钥';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."url" IS '接口域名';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."account" IS '账号';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."min" IS '最小金额';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."max" IS '最大金额';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."status" IS '状态 ';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."icon" IS '图标样式';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_platform_id" IS '支付平台ID';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."level_group" IS '限制会员等级';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."sort_no" IS '排序';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."show_type" IS '显示类型: all - 所有终端都显示、 pc - pc端显示、 mobile - 手机端显示、 app - app端显示';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_desc" IS '备注';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_starttime" IS '时间段开始时间';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_endtime" IS '时间段结束时间';

COMMENT ON COLUMN "public"."agent_deposit_virtual"."pay_channel" IS '支付渠道';
