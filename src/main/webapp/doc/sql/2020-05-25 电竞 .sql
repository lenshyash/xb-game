ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "esports_bet_amount" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "esports_win_amount" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "esports_bet_times" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "esports_win_times" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "esports_rebate_amount" numeric(20,5) DEFAULT 0;

--引导
ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "esports_bet_amount" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "esports_win_amount" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "esports_bet_times" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "esports_win_times" numeric(20,5) DEFAULT 0;

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "esports_rebate_amount" numeric(20,5) DEFAULT 0;