ALTER TABLE "public"."sys_account" ADD COLUMN "last_login_device" VARCHAR(20);
COMMENT ON COLUMN "public"."sys_account"."last_login_device" IS '最后登录设备';