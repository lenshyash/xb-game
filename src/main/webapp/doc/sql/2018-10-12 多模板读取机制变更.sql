ALTER TABLE "public"."agent_activity" ADD COLUMN "folder_code" varchar(20);
ALTER TABLE "public"."agent_article" ADD COLUMN "folder_code" varchar(20);
ALTER TABLE "public"."agent_lunbo" ADD COLUMN "folder_code" varchar(20);

update agent_activity a set folder_code = b.folder from sys_station_domain b where a.domain_id = b.id and a.domain_id != 0;
update agent_lunbo a set folder_code = b.folder from sys_station_domain b where a.domain_id = b.id and a.domain_id != 0;