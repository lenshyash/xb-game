--agent菜单添加
INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('彩票投注限制管理', '/agent/agentLotteryBetLimit/index.do', 1, 151, 2, 2, NULL, '', '/agent/agentLotteryBetLimit', '');

--限制投注表序列
CREATE SEQUENCE "public"."agent_lottery_bet_limit_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."agent_lottery_bet_limit_id_seq"', 12, true);

ALTER SEQUENCE "public"."agent_lottery_bet_limit_id_seq" OWNER TO "postgres";

--限制投注表建立
CREATE TABLE "public"."agent_lottery_bet_limit" (
  "name" varchar(20) COLLATE "pg_catalog"."default",
  "group_name" varchar(20) COLLATE "pg_catalog"."default",
  "bet_limit" varchar(20) COLLATE "pg_catalog"."default",
  "station_id" int4 NOT NULL,
  "id" int4 NOT NULL DEFAULT nextval('agent_lottery_bet_limit_id_seq'::regclass),
  "status" int4,
  "code" varchar(20) COLLATE "pg_catalog"."default",
  CONSTRAINT "agent_lottery_bet_limit_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."agent_lottery_bet_limit" 
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."name" IS '彩种名称';

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."group_name" IS '彩票大玩法';

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."bet_limit" IS '投注限制';

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."station_id" IS '站点id';

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."status" IS '状态 2是启动 1是禁用';

COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."code" IS '彩种编号';


ALTER TABLE "public"."agent_lottery_bet_limit" ADD COLUMN "limit_type" int4;
COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."limit_type" IS '限制类型  1是赛车类限制  2是数字类限制';

ALTER TABLE "public"."agent_lottery_bet_limit" ADD COLUMN "lot_type" int4;
COMMENT ON COLUMN "public"."agent_lottery_bet_limit"."lot_type" IS '彩种类型';