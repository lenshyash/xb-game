ALTER TABLE "public"."sys_login_log"
ADD COLUMN "domain" varchar(50);

COMMENT ON COLUMN "public"."sys_login_log"."domain" IS '登录域名';