ALTER TABLE "public"."sys_login_log" ADD COLUMN "device_id" VARCHAR(50);
COMMENT ON COLUMN "public"."sys_login_log"."device_id" IS '设备ID';
ALTER TABLE "public"."sys_login_log" ADD COLUMN "device_type" VARCHAR(50);
COMMENT ON COLUMN "public"."sys_login_log"."device_type" IS '设备类型';