ALTER TABLE "public"."sys_station_folder_url"
ADD COLUMN "kf_url" varchar(200);

COMMENT ON COLUMN "public"."sys_station_folder_url"."kf_url" IS '客服链接';