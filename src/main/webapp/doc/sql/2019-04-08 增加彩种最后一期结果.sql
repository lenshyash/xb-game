CREATE SEQUENCE "public"."bc_lottery_data_last_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."bc_lottery_data_last_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."bc_lottery_data_last" (
	"station_id" int4 NOT NULL,
	"lot_code" varchar(50) NOT NULL COLLATE "default",
	"start_time" timestamp(6) NULL,
	"end_time" timestamp(6) NULL,
	"hao_ma" varchar(255) COLLATE "default",
	"qi_hao" varchar(200) COLLATE "default",
	"id" int4 NOT NULL DEFAULT nextval('bc_lottery_data_last_id_seq'::regclass),
	CONSTRAINT "bc_lottery_data_last_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE,
	CONSTRAINT "station_code" UNIQUE ("station_id","lot_code") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."bc_lottery_data_last" OWNER TO "postgres";