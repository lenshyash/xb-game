
INSERT INTO "public"."agent_menu"("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('电竞记录', '/agent/esportsbet/index.do', (select id from agent_menu where "name"='真人记录'), 8, 2, 2, NULL, '', '/agent/esportsbet', '');


ALTER TABLE "public"."proxy_manual_rebate_record"
ADD COLUMN "esports_profit_amount" numeric(16,5),
ADD COLUMN "esports_rebate" numeric(5,2),
ADD COLUMN "esports_rebate_amount" numeric(16,5);

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."esports_profit_amount" IS '电竞盈利金额';

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."esports_rebate" IS '电竞返点';

COMMENT ON COLUMN "public"."proxy_manual_rebate_record"."esports_rebate_amount" IS '电竞返点金额';



ALTER TABLE "public"."proxy_daily_betting_statistics"
ADD COLUMN "esports_bet_amount" numeric(16,5),
ADD COLUMN "esports_profit_amount" numeric(16,5),
ADD COLUMN "esports_rebate_amount" numeric(16,5);

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."esports_bet_amount" IS '电竞投注金额';

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."esports_profit_amount" IS '电竞盈利金额';

COMMENT ON COLUMN "public"."proxy_daily_betting_statistics"."esports_rebate_amount" IS '电竞返点金额';