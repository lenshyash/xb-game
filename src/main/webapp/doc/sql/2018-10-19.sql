CREATE SEQUENCE "public"."app_update_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
ALTER TABLE "public"."app_update_id_seq" OWNER TO "postgres";
ALTER TABLE "public"."app_update"
ADD COLUMN "flag" varchar(100);
ALTER TABLE "public"."app_update"
ADD COLUMN "create_time" timestamp(6);
ALTER TABLE "public"."app_update"
ADD PRIMARY KEY ("id");
ALTER TABLE "public"."app_update"
ALTER COLUMN "id" SET DEFAULT nextval('app_update_id_seq'::regclass);

