ALTER TABLE "public"."sys_account_info"
ADD COLUMN "head_url" varchar(100);

COMMENT ON COLUMN "public"."sys_account_info"."head_url" IS '头像链接';