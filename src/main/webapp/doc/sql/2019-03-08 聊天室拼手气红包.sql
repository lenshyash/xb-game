
CREATE SEQUENCE "public"."common_redpacket_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."common_redpacket_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."common_redpacket" (
	"id" int4 NOT NULL DEFAULT nextval('common_redpacket_id_seq'::regclass),
	"total_number" int4 NOT NULL,
	"total_money" numeric(20,5) NOT NULL,
	"min_money" numeric(20,5),
	"remain_money" numeric(20,5),
	"remain_number" int4,
	"create_datetime" timestamp(6) NULL,
	"end_datetime" timestamp(6) NULL,
	"title" varchar(100) COLLATE "default",
	"creator" varchar(50) COLLATE "default",
	"status" int2,
	"station_id" int4,
	"account_id" int4,
	CONSTRAINT "common_redpacket_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."common_redpacket" OWNER TO "postgres";
COMMENT ON COLUMN "public"."common_redpacket"."total_number" IS '总人数';
COMMENT ON COLUMN "public"."common_redpacket"."total_money" IS '总金额';
COMMENT ON COLUMN "public"."common_redpacket"."min_money" IS '最小红包金额';
COMMENT ON COLUMN "public"."common_redpacket"."remain_money" IS '剩余金额';
COMMENT ON COLUMN "public"."common_redpacket"."remain_number" IS '剩余次数';
COMMENT ON COLUMN "public"."common_redpacket"."title" IS '红包标题';
COMMENT ON COLUMN "public"."common_redpacket"."status" IS '启用状态';
COMMENT ON COLUMN "public"."common_redpacket"."station_id" IS '站点id';


CREATE SEQUENCE "public"."common_redpacket_record_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."common_redpacket_record_id_seq" OWNER TO "postgres";

CREATE TABLE "public"."common_redpacket_record" (
	"id" int4 NOT NULL DEFAULT nextval('common_redpacket_record_id_seq'::regclass),
	"account_id" int4 NOT NULL,
	"money" numeric(20,2),
	"create_datetime" timestamp(6) NULL,
	"redpacket_id" int4 NOT NULL,
	"station_id" int4,
	"status" int4,
	"account" varchar(20) COLLATE "default",
	"remark" text COLLATE "default",
	"redpacket_name" varchar(100) COLLATE "default",
	CONSTRAINT "member_red_packet_record_copy_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE,
	CONSTRAINT "accid_and_redpacketid" UNIQUE ("account_id","redpacket_id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."common_redpacket_record" OWNER TO "postgres";

ALTER TABLE "public"."sys_account" ADD COLUMN "redpacket_limit" int2 DEFAULT 2;