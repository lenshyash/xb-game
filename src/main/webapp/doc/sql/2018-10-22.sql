ALTER TABLE "public"."app_update"
ADD COLUMN "url" varchar(255);

COMMENT ON COLUMN "public"."app_update"."url" IS 'app下载链接';

ALTER TABLE "public"."app_update"
ADD COLUMN "is_update" int4;

COMMENT ON COLUMN "public"."app_update"."is_update" IS '是否强制更新 1是0否';
