ALTER TABLE "public"."bc_lottery_order" ADD COLUMN "agent_rebate" int2 DEFAULT 1;
COMMENT ON COLUMN "public"."bc_lottery_order"."roll_back_status" IS '会员返水状态 （1，还未返水 2，已经返水,还未到账 3，返水已经回滚 ,4 反水已经到账 ）';
COMMENT ON COLUMN "public"."bc_lottery_order"."agent_rebate" IS '多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)';