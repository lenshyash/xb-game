ALTER TABLE "public"."sys_account_daily_money" ADD COLUMN "deposit_handler_times" int4;
COMMENT ON COLUMN "public"."sys_account_daily_money"."deposit_handler_times" IS '手动加款次数';

ALTER TABLE "public"."sys_account_guide_money" ADD COLUMN "deposit_handler_times" int4;
COMMENT ON COLUMN "public"."sys_account_guide_money"."deposit_handler_times" IS '手动加款次数';