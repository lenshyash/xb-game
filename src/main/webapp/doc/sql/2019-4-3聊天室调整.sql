ALTER TABLE "public"."sys_account"
ADD COLUMN "chat_token" varchar(200);

COMMENT ON COLUMN "public"."sys_account"."chat_token" IS '用户指定聊天室token';

ALTER TABLE "public"."sys_station_domain"
ADD COLUMN "is_chat_switch" int4 DEFAULT 2;

COMMENT ON COLUMN "public"."sys_station_domain"."is_chat_switch" IS '域名聊天室开关 1禁言 2启用';

INSERT INTO "public"."agent_menu" ("name", "url", "parent_id", "sort", "level", "status", "type", "remark", "module_path", "icon") VALUES ('聊天室管理', '/agent/system/chat/index.do', '1', '20', '2', '2', NULL, '', '/agent/system/chat', '');


