CREATE SEQUENCE "public"."sys_account_virtual_address_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

SELECT setval('"public"."sys_account_virtual_address_seq"', 1, true);

ALTER SEQUENCE "public"."sys_account_virtual_address_seq" OWNER TO "postgres";

CREATE TABLE "public"."sys_account_virtual_address" (
  "id" int4 NOT NULL DEFAULT nextval('sys_account_virtual_address_seq'::regclass),
  "account_id" int4,
  "station_id" int4,
  "pay_id" int4,
  "money_address" varchar(50) COLLATE "pg_catalog"."default",
  "create_datetime" timestamp(6),
  CONSTRAINT "sys_account_virtual_address_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."sys_account_virtual_address"
  OWNER TO "postgres";

COMMENT ON COLUMN "public"."sys_account_virtual_address"."account_id" IS '用户ID';

COMMENT ON COLUMN "public"."sys_account_virtual_address"."station_id" IS '站点ID';

COMMENT ON COLUMN "public"."sys_account_virtual_address"."pay_id" IS '支付通道ID';

COMMENT ON COLUMN "public"."sys_account_virtual_address"."money_address" IS '钱包地址';

COMMENT ON COLUMN "public"."sys_account_virtual_address"."create_datetime" IS '创建时间';

COMMENT ON TABLE "public"."sys_account_virtual_address" IS '用户数字充值钱包地址表';

CREATE SEQUENCE "public"."sys_account_virtual_address_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;



ALTER TABLE "public"."agent_deposit_fast"
  ADD COLUMN "wallet_type" varchar(32),
  ADD COLUMN "rate" numeric(12,2),
  ADD COLUMN "platform_address" varchar(255);

COMMENT ON COLUMN "public"."agent_deposit_fast"."wallet_type" IS '数字钱包类型（货币主网）';

COMMENT ON COLUMN "public"."agent_deposit_fast"."rate" IS '货币兑换汇率';

COMMENT ON COLUMN "public"."agent_deposit_fast"."platform_address" IS '数字货币交易平台地址';

INSERT INTO "public"."sys_pay_platform"("name", "type", "icon_css", "img_path", "remark", "status", "create_datetime", "modify_datetime", "create_user_id", "modify_user_id", "flag_active", "company", "sort_no") VALUES ('链游货币支付', 801, 'lianyou', NULL, NULL, 2, '2020-11-04 16:16:57.809', NULL, 1, NULL, 1, '链游货币支付', NULL);
INSERT INTO "public"."sys_pay_platform"("name", "type", "icon_css", "img_path", "remark", "status", "create_datetime", "modify_datetime", "create_user_id", "modify_user_id", "flag_active", "company", "sort_no") VALUES ('数字货币', 6, 'virtual', NULL, NULL, 2, '2020-11-06 18:42:54.633', NULL, 1, NULL, 1, '数字货币', NULL);
