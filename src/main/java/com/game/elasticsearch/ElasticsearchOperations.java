package com.game.elasticsearch;

import com.game.common.data.Page;
import com.game.elasticsearch.operations.*;
import org.elasticsearch.script.Script;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface ElasticsearchOperations {

    String DEFAULT_TYPE = "_doc";

    /**
     * @return
     */
    Client getClient();

    /**
     * @param indexOperation
     * @return
     */
    String index(IndexOperation<?> indexOperation);

    /**
     * @param indexOperation
     * @return doc id
     */
    CompletableFuture<String> indexAsync(IndexOperation<?> indexOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     * @return
     */
    <T> String index(String index, String type, String id, T source);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     * @return
     */
    <T> CompletableFuture<String> indexAsync(String index, String type, String id, T source);

    <T> String index(String index, String type, T source);

    <T> CompletableFuture<String> indexAsync(String index, String type, T source);

    /**
     * @param indexOps
     */
    void bulkIndex(List<IndexOperation<?>> indexOps);

    /**
     * @param indexOps
     * @return
     */
    CompletableFuture<Void> bulkIndexAsync(List<IndexOperation<?>> indexOps);

    /**
     * @param index
     * @param type
     * @param id    doc id
     * @return
     */
    String delete(String index, String type, String id, String routing);

    /**
     * @param index
     * @param type
     * @param id
     * @param routing
     * @return future with doc id
     */
    CompletableFuture<String> deleteAsync(String index, String type, String id, String routing);

    /**
     * @param deleteByQueryOperation
     */
    void deleteByQuery(DeleteByQueryOperation deleteByQueryOperation);

    /**
     * @param deleteByQueryOperation
     * @return
     */
    CompletableFuture<Void> deleteByQueryAsync(DeleteByQueryOperation deleteByQueryOperation);

    /**
     * Get a single Object
     *
     * @param getOperation operation params
     * @param <T>
     * @return
     */
    <T> T get(GetOperation<T> getOperation);

    /**
     * Get a single Object async
     *
     * @param getOperation operation params
     * @param <T>
     * @return
     */
    <T> CompletableFuture<T> getAsync(GetOperation<T> getOperation);

    /**
     * Get a single Object
     *
     * @param index       index name
     * @param type        type name
     * @param id          object id
     * @param sourceClass object class
     * @param <T>
     * @return
     */
    <T> T get(final String index, final String type, final String id, Class<T> sourceClass);

    /**
     * Get a single Object async
     *
     * @param index       index name
     * @param type        type name
     * @param id          object id
     * @param sourceClass object class
     * @param <T>
     * @return future with object
     */
    <T> CompletableFuture<T>
    getAsync(final String index, final String type, final String id, Class<T> sourceClass);

    /**
     * @param multiGetOperation
     * @param <T>
     * @return
     */
    <T> List<T> multiGet(MultiGetOperation<T> multiGetOperation);

    /**
     * @param multiGetOperation
     * @param <T>
     * @return
     */
    <T> CompletableFuture<List<T>> multiGetAsync(MultiGetOperation<T> multiGetOperation);

    /**
     * @param updateOperation
     */
    void update(UpdateOperation<?> updateOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     */
    <T> void update(String index, String type, String id, T source);

    /**
     * @param index
     * @param type
     * @param id
     * @param script
     */
    void update(String index, String type, String id, Script script);

    /**
     * @param updateOperation
     * @return
     */
    CompletableFuture<Void> updateAsync(UpdateOperation<?> updateOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     * @return
     */
    <T> CompletableFuture<Void>
    updateAsync(String index, String type, String id, T source);

    /**
     * @param index
     * @param type
     * @param id
     * @param script
     * @return
     */
    CompletableFuture<Void> updateAsync(String index, String type, String id, Script script);

    /**
     * @param updateOps
     */
    void bulkUpdate(List<UpdateOperation<?>> updateOps);

    /**
     * @param updateOps
     * @return
     */
    CompletableFuture<Void> bulkUpdateAsync(List<UpdateOperation<?>> updateOps);

    /**
     * @param updateByQueryOperation
     */
    void updateByQuery(UpdateByQueryOperation updateByQueryOperation);

    /**
     * @param updateByQueryOperation
     * @return
     */
    CompletableFuture<Void> updateByQueryAsync(UpdateByQueryOperation updateByQueryOperation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> T searchOne(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> CompletableFuture<T> searchOneAsync(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> List<T> search(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> CompletableFuture<List<T>> searchAsync(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> Page<T> searchForPage(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> CompletableFuture<Page<T>> searchForPageAsync(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> long count(SearchOperation<T> operation);

    /**
     * @param operation
     * @param <T>
     * @return
     */
    <T> CompletableFuture<Long> countAsync(SearchOperation<T> operation);

    /**
     * @param operation
     * @param field
     * @return
     */
    Map<String, Long> termsBucketCount(SearchOperation<?> operation, String field);

    /**
     * @param operation
     * @param field
     * @return
     */
    CompletableFuture<Map<String, Long>> termsBucketCountAsync(SearchOperation<?> operation, String field);

    /**
     * @param operation
     * @param field
     * @return
     */
    Map<String, Long> termsBucketCount(SearchOperation<?> operation, String field, int bucketSize);

    /**
     * @param operation
     * @param field
     * @return
     */
    CompletableFuture<Map<String, Long>> termsBucketCountAsync(SearchOperation<?> operation, String field, int bucketSize);

    /**
     * @param operation
     * @param sumField
     * @return
     */
    double sum(SearchOperation<?> operation, String sumField);

    /**
     * @param operation
     * @param sumField
     * @return
     */
    CompletableFuture<Double> sumAsync(SearchOperation<?> operation, String sumField);

    /**
     * @param operation
     * @param bucketField
     * @param sumField
     * @return
     */
    Map<String, Double> termsBucketSum(SearchOperation<?> operation, String bucketField, String sumField);

    /**
     * @param operation
     * @param bucketField
     * @param sumField
     * @return
     */
    Map<String, Double> termsBucketSum(SearchOperation<?> operation, String bucketField, String sumField, int bucketSize);

    /**
     * @param operation
     * @param bucketField
     * @param sumField
     * @return
     */
    CompletableFuture<Map<String, Double>> termsBucketSumAsync(SearchOperation<?> operation, String bucketField, String sumField);

    /**
     * @param operation
     * @param bucketField
     * @param sumField
     * @return
     */
    CompletableFuture<Map<String, Double>> termsBucketSumAsync(SearchOperation<?> operation, String bucketField, String sumField, int bucketSize);

    /**
     * @param operation
     * @param scrollTimeInMillis
     * @param <T>
     * @return
     */
    <T> ScrollPage<T> startScroll(SearchOperation<T> operation, long scrollTimeInMillis);

    /**
     * @param operation
     * @param scrollTimeInMillis
     * @param <T>
     * @return
     */
    <T> CompletableFuture<ScrollPage<T>>
    startScrollAsync(SearchOperation<T> operation, long scrollTimeInMillis);

    /**
     * @param scrollId
     * @param scrollTimeInMillis
     * @param <T>
     * @return
     */
    <T> ScrollPage<T>
    continueScroll(String scrollId, long scrollTimeInMillis, Class<T> sourceClass);

    /**
     * @param scrollId
     * @param scrollTimeInMillis
     * @param sourceClass
     * @param <T>
     * @return
     */
    <T> CompletableFuture<ScrollPage<T>>
    continueScrollAsync(String scrollId, long scrollTimeInMillis, Class<T> sourceClass);

    /**
     * @param scrollId
     */
    void clearScroll(String scrollId);

    /**
     * @param scrollId
     * @return
     */
    CompletableFuture<Void> clearScrollAsync(String scrollId);
}
