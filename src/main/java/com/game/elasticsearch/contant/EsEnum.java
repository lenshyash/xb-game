package com.game.elasticsearch.contant;

import com.game.core.SystemConfig;

public enum EsEnum {

	BC_LOTTERY_DATA("bc_lottery_data","ld"),
	SYS_ACCOUNT_DAILY_MONEY("account_daily_money","_doc"),
	SYS_ACCOUNT_GUIDE_MONEY("account_guide_money","agm"),
	MNY_COM_RECORD("mny_com_record","mcr"), //暂时去除 实时操作太多
//	MNY_DRAW_RECORD("mny_draw_record","mdr");
	MNY_MONEY_RECORD("mny_money_record","mmr");
	private String index;
	private String type;


	EsEnum(String _index, String _type) {
		this.index = _index;
		this.type = _type;
	}

	public String getIndexTag(){
		return SystemConfig.ELASTICSEARCH_SERVER_PREFIX+"_"+this.index;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
