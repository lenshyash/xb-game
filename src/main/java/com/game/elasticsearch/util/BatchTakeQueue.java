package com.game.elasticsearch.util;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.component.AbstractLifecycleComponent;
import org.elasticsearch.common.component.LifecycleComponent;
import org.elasticsearch.common.settings.Setting;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.util.concurrent.EsAbortPolicy;
import org.elasticsearch.common.util.concurrent.EsExecutors;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by terry on 2017/6/13.
 */
@Slf4j
public abstract class BatchTakeQueue<E> extends AbstractLifecycleComponent implements Queue<E>, Runnable,
        LifecycleComponent {

    private BlockingQueue<E> queue;

    private int capacity;

    private int takeSize;
    private Long takeInterval;
    private ScheduledExecutorService scheduler;

    private ScheduledFuture takeFuture;

    private static final Setting<Integer> QUEUE_CAPACITY = Setting.intSetting("queue.capacity", 100000);

    private static final Setting<Integer> QUEUE_TAKE_SIZE = Setting.intSetting("queue.take.size", 1000);

    private static final Setting<TimeValue> QUEUE_TAKE_INTERVAL = Setting.timeSetting("queue.take.interval",
            TimeValue.timeValueSeconds(3));

    private Settings settings;

    /**
     * @param settings
     */
    public BatchTakeQueue(Settings settings) {
        this.settings = settings;
        takeSize = QUEUE_TAKE_SIZE.get(settings);
        capacity = QUEUE_CAPACITY.get(settings);
        takeInterval = QUEUE_TAKE_INTERVAL.get(settings).getMillis();
        if (capacity <= 0)
            throw new IllegalArgumentException();
        if (takeSize <= 0)
            throw new IllegalArgumentException("Non-positive size.");
        if (takeSize > capacity)
            throw new IllegalArgumentException("Take Size can not gt capacity.");
        scheduler = new ScheduledThreadPoolExecutor(1,
                EsExecutors.daemonThreadFactory("batch-take-scheduler"), new EsAbortPolicy());
        this.queue = new LinkedBlockingQueue<>(capacity);
    }


    @Override
    protected void doStart() {
        takeFuture = scheduler.scheduleWithFixedDelay(this, 0, takeInterval, TimeUnit.MILLISECONDS);
    }

    @Override
    protected void doStop() {
        takeFuture.cancel(true);
    }

    @Override
    protected void doClose() throws IOException {
        doStop();
    }

    /**
     * @return
     * @see Collection#size()
     */
    public int size() {
        return queue.size();
    }

    /**
     * @return
     * @see Collection#isEmpty()
     */
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    /**
     * @param e
     * @return
     * @see BlockingQueue#add(Object)
     */
    public boolean add(E e) {
        return queue.add(e);
    }

    /**
     * @return
     * @see Collection#iterator()
     */
    public Iterator<E> iterator() {
        return queue.iterator();
    }

    /**
     * @return
     * @see Queue#remove()
     */
    public E remove() {
        return queue.remove();
    }

    /**
     * @param e
     * @return
     * @see BlockingQueue#offer(Object)
     */
    public boolean offer(E e) {
        return queue.offer(e);
    }

    /**
     * @return
     * @see Queue#poll()
     */
    public E poll() {
        return queue.poll();
    }

    /**
     * @return
     * @see Collection#toArray()
     */
    public Object[] toArray() {
        return queue.toArray();
    }

    /**
     * @return
     * @see Queue#element()
     */
    public E element() {
        return queue.element();
    }

    /**
     * @return
     * @see Queue#peek()
     */
    public E peek() {
        return queue.peek();
    }

    /**
     * @param a
     * @return
     * @see Collection#toArray(Object[])
     */
    public <T> T[] toArray(T[] a) {
        return queue.toArray(a);
    }

    /**
     * @param e
     * @throws InterruptedException
     * @see BlockingQueue#put(Object)
     */
    public void put(E e) throws InterruptedException {
        queue.put(e);
    }

    /**
     * @param e
     * @param timeout
     * @param unit
     * @return
     * @throws InterruptedException
     * @see BlockingQueue#offer(Object, long,
     * TimeUnit)
     */
    public boolean offer(E e, long timeout, TimeUnit unit)
            throws InterruptedException {
        return queue.offer(e, timeout, unit);
    }

    /**
     * @return
     * @throws InterruptedException
     * @see BlockingQueue#take()
     */
    public E take() throws InterruptedException {
        return queue.take();
    }

    /**
     * @param timeout
     * @param unit
     * @return
     * @throws InterruptedException
     * @see BlockingQueue#poll(long,
     * TimeUnit)
     */
    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        return queue.poll(timeout, unit);
    }

    /**
     * @return
     * @see BlockingQueue#remainingCapacity()
     */
    public int remainingCapacity() {
        return queue.remainingCapacity();
    }

    /**
     * @param o
     * @return
     * @see BlockingQueue#remove(Object)
     */
    public boolean remove(Object o) {
        return queue.remove(o);
    }

    /**
     * @param o
     * @return
     * @see BlockingQueue#contains(Object)
     */
    public boolean contains(Object o) {
        return queue.contains(o);
    }

    /**
     * @param c
     * @return
     * @see BlockingQueue#drainTo(Collection)
     */
    public int drainTo(Collection<? super E> c) {
        return queue.drainTo(c);
    }

    /**
     * @param c
     * @return
     * @see Collection#containsAll(Collection)
     */
    public boolean containsAll(Collection<?> c) {
        return queue.containsAll(c);
    }

    /**
     * @param c
     * @param maxElements
     * @return
     * @see BlockingQueue#drainTo(Collection,
     * int)
     */
    public int drainTo(Collection<? super E> c, int maxElements) {
        return queue.drainTo(c, maxElements);
    }

    /**
     * @param c
     * @return
     * @see Collection#addAll(Collection)
     */
    public boolean addAll(Collection<? extends E> c) {
        return queue.addAll(c);
    }

    /**
     * @param c
     * @return
     * @see Collection#removeAll(Collection)
     */
    public boolean removeAll(Collection<?> c) {
        return queue.removeAll(c);
    }

    /**
     * @param c
     * @return
     * @see Collection#retainAll(Collection)
     */
    public boolean retainAll(Collection<?> c) {
        return queue.retainAll(c);
    }

    /**
     * @see Collection#clear()
     */
    public void clear() {
        queue.clear();
    }


    @Override
    public void run() {
        List<E> lst = new LinkedList<>();
        int size = queue.size() >= takeSize ? takeSize : queue.size();
        for (int i = 0; i < size; i++) {
            lst.add(queue.poll());
        }
        if (lst.size() > 0) {
            try {
                log.debug("take >>>>>>>>>>>:[{}]", lst.size());
                take(lst);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void take(List<E> lst);
}
