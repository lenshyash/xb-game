package com.game.elasticsearch.util;


import com.game.util.DateUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

/**
 * @author yangpeng
 */
public class StringConvertUtil {


    private static String[] PARSE_PATTERNS = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};


    public static Object convertType(Class<?> type, String strValue) {
        if (strValue == null) {
            return null;
        } else if (type.equals(Integer.class)) {
            return Integer.parseInt(strValue);
        } else if (type.equals(Long.class)) {
            return Long.parseLong(strValue);
        } else if (type.equals(Boolean.class)) {
            return convertToBoolean(strValue);
        } else if (type.equals(BigDecimal.class)) {
            return new BigDecimal(strValue);
        } else if (type.equals(Double.class)) {
            return Double.parseDouble(strValue);
        } else if (type.equals(Float.class)) {
            return Float.parseFloat(strValue);
        } else if (type.equals(Date.class)) {
            return parseDate(strValue);
        } else if (type.equals(java.sql.Date.class)) {
            return parseDate(strValue);
        } else if (type.equals(String.class)){
            return strValue;
        }else {
            throw new IllegalArgumentException("Unsupported type :" + type);
        }
    }


    private static Date parseDate(String str) {
        if (str == null) {
            return null;
        }
        try {
            return DateUtils.parseDate(str, PARSE_PATTERNS);
        } catch (ParseException e) {
            return null;
        }
    }


    private static boolean convertToBoolean(String value) {
        return "1".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value);
    }

}
