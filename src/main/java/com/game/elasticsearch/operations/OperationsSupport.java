package com.game.elasticsearch.operations;

import com.alibaba.fastjson.JSON;
import com.game.common.data.Page;
import com.game.common.data.XSort;
import com.game.common.data.annotation.CreateAt;
import com.game.elasticsearch.ScrollPage;
import com.game.elasticsearch.util.StringConvertUtil;
import com.game.util.DateUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

public abstract class OperationsSupport {

    public static <T> IndexRequest indexRequest(IndexOperation<T> indexOperation) {

        checkNotNull(indexOperation.getSource(), "source is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(indexOperation.getIndex()), "index name is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(indexOperation.getType()), "type name is required");
        try {
            writeIdValue(indexOperation.getSource(), null);
            writeCreateAtValue(indexOperation.getSource());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return new IndexRequest(indexOperation.getIndex(), indexOperation.getType(), indexOperation.getId())
                .opType(indexOperation.getOpType())
                .routing(indexOperation.getRouting())
                .timeout(indexOperation.getTimeout())
                .setRefreshPolicy(indexOperation.getRefreshPolicy())
                .setPipeline(indexOperation.getPipeline())
                .source(JSON.toJSONBytes(indexOperation.getSource()), XContentType.JSON);
    }

    public static UpdateRequest updateRequest(UpdateOperation<?> updateOperation) {
        checkNotNull(updateOperation);
        checkArgument(org.elasticsearch.common.Strings.hasText(updateOperation.getIndex()), "index name is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(updateOperation.getType()), "type name is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(updateOperation.getId()), "id is required");
        UpdateRequest request = new UpdateRequest(updateOperation.getIndex(), updateOperation.getType(),
                updateOperation.getId())
                .routing(updateOperation.getRouting())
                .detectNoop(updateOperation.getDetectNoop())
                //.version(updateOperation.getVersion())
                //.versionType(updateOperation.getVersionType())
                .retryOnConflict(updateOperation.getRetryOnConflict());
        if (Objects.nonNull(updateOperation.getSource())) {
            Object doc = updateOperation.getSource();
            //doc.set_id(null);
            try {
                writeIdValue(doc, null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            request.doc(JSON.toJSONBytes(doc), XContentType.JSON)
                    .docAsUpsert(updateOperation.getDocAsUpsert());
        }
        if (Objects.nonNull(updateOperation.getScript())) {
            request.script(updateOperation.getScript()).scriptedUpsert(updateOperation.getScriptedUpsert());
        }
        return request;
    }

    public static GetRequest getRequest(GetOperation getOperation) {
        checkNotNull(getOperation);
        checkArgument(org.elasticsearch.common.Strings.hasText(getOperation.getIndex()), "index name is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(getOperation.getType()), "type name is required");
        checkArgument(org.elasticsearch.common.Strings.hasText(getOperation.getId()), "id is required");
        checkNotNull(getOperation.getSourceClass(), "source class is required");
        GetRequest request = new GetRequest(getOperation.getIndex(),
                getOperation.getType(), getOperation.getId()).routing(getOperation.getRouting());
        request.preference(getOperation.getPreference())
                .refresh(getOperation.getRefresh())
                .version(getOperation.getVersion())
                .versionType(getOperation.getVersionType())
                .fetchSourceContext(getOperation.getFetchSourceContext());
        return request;
    }

    public static MultiGetRequest multiGetRequest(MultiGetOperation<?> multiGetOperation) {
        checkNotNull(multiGetOperation);
        checkArgument(org.elasticsearch.common.Strings.hasText(multiGetOperation.getIndex()), "index name is required");
        checkArgument(Strings.hasText(multiGetOperation.getType()), "type name is required");
        checkArgument(multiGetOperation.getIds() != null && !multiGetOperation.getIds().isEmpty(),
                "ids is required");
        checkNotNull(multiGetOperation.getSourceClass(), "source class is required");
        MultiGetRequest request = new MultiGetRequest().preference(multiGetOperation.getPreference())
                .refresh(multiGetOperation.getRefresh()).realtime(multiGetOperation.getRealtime());

        multiGetOperation.getIds().forEach(id ->
                request.add(
                        new MultiGetRequest.Item(multiGetOperation.getIndex(), multiGetOperation.getType(), id)
                                .fetchSourceContext(multiGetOperation.getFetchSourceContext())
                                .routing(multiGetOperation.getRouting())
                                .version(multiGetOperation.getVersion())
                                .versionType(multiGetOperation.getVersionType()))
        );

        return request;

    }

    public static DeleteByQueryRequest deleteByQueryRequest(DeleteByQueryOperation deleteByQueryOperation) {
        return new DeleteByQueryRequest(deleteByQueryOperation.getIndices().toArray(new String[]{}))
                .setDocTypes(deleteByQueryOperation.getTypes().toArray(new String[]{}))
                .setQuery(deleteByQueryOperation.getQuery())
                .setRouting(deleteByQueryOperation.getRouting())
                .setScroll(deleteByQueryOperation.getScroll())
                .setSlices(deleteByQueryOperation.getSlices())
                .setRefresh(deleteByQueryOperation.getRefresh())
                .setTimeout(deleteByQueryOperation.getTimeout())
                .setAbortOnVersionConflict(deleteByQueryOperation.getAbortOnVersionConflict());
    }

    public static UpdateByQueryRequest updateByQueryRequest(UpdateByQueryOperation updateByQueryOperation) {
        checkNotNull(updateByQueryOperation, "operation is required");
        checkNotNull(updateByQueryOperation.getScript(), "script is required");
        checkNotNull(updateByQueryOperation.getQuery(), "query is required");
        checkArgument(updateByQueryOperation.getIndices() != null && !updateByQueryOperation.getIndices().isEmpty(),
                "indices is required");
        checkArgument(updateByQueryOperation.getTypes() != null && !updateByQueryOperation.getTypes().isEmpty(),
                "types is required");
        UpdateByQueryRequest request =
                new UpdateByQueryRequest(updateByQueryOperation.getIndices().toArray(new String[]{}))
                        .setScript(updateByQueryOperation.getScript())
                        .setDocTypes(updateByQueryOperation.getTypes().toArray(new String[]{}))
                        .setQuery(updateByQueryOperation.getQuery())
                        .setRouting(updateByQueryOperation.getRouting())
                        .setRefresh(updateByQueryOperation.getRefresh())
                        .setAbortOnVersionConflict(updateByQueryOperation.getAbortOnVersionConflict())
                        .setMaxRetries(updateByQueryOperation.getMaxRetries())
                        .setSlices(updateByQueryOperation.getSlices())
                        .setTimeout(updateByQueryOperation.getTimeout());
        if (Objects.nonNull(updateByQueryOperation.getScroll())) {
            request.setScroll(updateByQueryOperation.getScroll());
        }
        return request;
    }

    public static SearchRequest searchRequest(SearchOperation<?> operation) {
        SearchRequest request = new SearchRequest(operation.getIndices().toArray(new String[]{}));
        request.preference(operation.getPreference())
                .routing(operation.getRouting())
                //.types(operation.getTypes().toArray(new String[]{}))
                .searchType(Objects.isNull(operation.getSearchType()) ? SearchType.DEFAULT : operation.getSearchType())
                .requestCache(operation.getRequestCache());
        SearchSourceBuilder source = new SearchSourceBuilder()
                .query(operation.getQuery())
                .fetchSource(operation.getFetchSourceContext())
                .postFilter(operation.getFilter())
                .from(operation.getFrom()).size(operation.getSize());
        if (operation.getSort() != null) {
            operation.getSort().forEach(order ->
                    source.sort(order.getProperty(),
                            order.getDirection() == XSort.Direction.ASC ? SortOrder.ASC : SortOrder.DESC)
            );
        }
        request.source(source);
        return request;
    }

    public static SearchRequest countRequest(SearchOperation<?> operation) {
        SearchRequest request = new SearchRequest(operation.getIndices().toArray(new String[]{}));
        request.preference(operation.getPreference())
                .routing(operation.getRouting())
                .searchType(Objects.isNull(operation.getSearchType()) ? SearchType.DEFAULT : operation.getSearchType())
                .requestCache(operation.getRequestCache());
        SearchSourceBuilder source = new SearchSourceBuilder()
                .query(operation.getQuery())
                .fetchSource(operation.getFetchSourceContext())
                .postFilter(operation.getFilter()).size(0);
        request.source(source);
        return request;
    }

    public static SearchRequest sumRequest(SearchOperation<?> operation, String field) {
        SearchRequest request = new SearchRequest(operation.getIndices().toArray(new String[]{}));
        request.preference(operation.getPreference())
                .routing(operation.getRouting())
                .searchType(Objects.isNull(operation.getSearchType()) ? SearchType.DEFAULT : operation.getSearchType())
                .requestCache(operation.getRequestCache());
        SearchSourceBuilder source = new SearchSourceBuilder()
                .query(operation.getQuery())
                .postFilter(operation.getFilter()).size(0).aggregation(AggregationBuilders.sum(field).field(field));
        request.source(source);
        return request;
    }

    public static SearchRequest termsBucketCountRequest(SearchOperation<?> operation, String field, int bucketSize) {
        SearchRequest request = new SearchRequest(operation.getIndices().toArray(new String[]{}));
        request.preference(operation.getPreference())
                .routing(operation.getRouting())
                .searchType(Objects.isNull(operation.getSearchType()) ? SearchType.DEFAULT : operation.getSearchType())
                .requestCache(operation.getRequestCache());
        TermsAggregationBuilder termsAggregationBuilder = terms(field).field(field);
        if (bucketSize > 0)
            termsAggregationBuilder.size(bucketSize);
        termsAggregationBuilder.shardSize(termsAggregationBuilder.size() * 3);
        SearchSourceBuilder source = new SearchSourceBuilder()
                .query(operation.getQuery())
                .postFilter(operation.getFilter()).size(0).aggregation(termsAggregationBuilder);
        request.source(source);
        return request;
    }

    public static SearchRequest termsBucketSumRequest(SearchOperation<?> operation, String bucketField, String sumField, int bucketSize) {
        SearchRequest request = new SearchRequest(operation.getIndices().toArray(new String[]{}));
        request.preference(operation.getPreference())
                .routing(operation.getRouting())
                .searchType(Objects.isNull(operation.getSearchType()) ? SearchType.DEFAULT : operation.getSearchType())
                .requestCache(operation.getRequestCache());
        TermsAggregationBuilder termsAggregationBuilder =
                terms(bucketField).field(bucketField).subAggregation(AggregationBuilders.sum(sumField).field(sumField));
        if (bucketSize > 0)
            termsAggregationBuilder.size(bucketSize);
        SearchSourceBuilder source = new SearchSourceBuilder()
                .query(operation.getQuery())
                .postFilter(operation.getFilter()).size(0).aggregation(termsAggregationBuilder);
        request.source(source);
        return request;
    }

    public static <T> List<T> convertToList(SearchResponse response, Class<T> sourceClass) {
        List<T> result = new ArrayList<>();
        if (response.getHits().getTotalHits().value == 0) {
            return result;
        }
        Arrays.stream(response.getHits().getHits()).forEach(hit -> {
            T object = JSON.parseObject(
                    hit.getSourceAsString(), sourceClass);
            //object.set_id(hit.getId());
            try {
                writeIdValue(object, hit.getId());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            result.add(object);
        });
        return result;
    }

    public static <T> List<T> convertToList(MultiGetResponse response, Class<T> sourceClass) {
        List<T> result = new LinkedList<>();
        response.iterator().forEachRemaining(multiGetItemResponse -> {
            if (multiGetItemResponse.isFailed())
                return;
            T item = JSON.parseObject(
                    multiGetItemResponse.getResponse().getSourceAsBytes(), sourceClass);
            //item.set_id(multiGetItemResponse.getId());
            try {
                writeIdValue(item, multiGetItemResponse.getId());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            result.add(item);
        });
        return result;
    }

    public static Long getCount(SearchResponse response) {
        return response.getHits().getTotalHits().value;
    }

    public static Map<String, Long> getTermsBucketCount(SearchResponse response, String field) {
        Terms terms = response.getAggregations().get(field);
        Map<String, Long> ret = new HashMap<>();
        terms.getBuckets().forEach(bucket -> ret.put(bucket.getKey().toString(), bucket.getDocCount()));
        return ret;
    }

    public static Map<String, Double> getTermsBucketSum(SearchResponse response, String bucketField, String sumField) {
        Terms terms = response.getAggregations().get(bucketField);
        Map<String, Double> ret = new HashMap<>();
        terms.getBuckets().forEach(bucket -> ret.put(bucket.getKey().toString(), ((Sum) bucket.getAggregations().get(sumField)).getValue()));
        return ret;
    }

    public static <T> Page<T> convertToPage(SearchResponse response, Class<T> sourceClass) {
        if (response.getHits().getTotalHits().value == 0) {
            return Page.empty();
        }
        List<T> ret = Arrays.stream(response.getHits().getHits()).map(hit -> {
            T object = JSON.parseObject(
                    hit.getSourceAsString(), sourceClass);
            try {
                writeIdValue(object, hit.getId());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return object;
        }).collect(Collectors.toList());
        return Page.newPage(response.getHits().getTotalHits().value, ret);
    }

    public static <T> ScrollPage<T> convertToScrollPage(SearchResponse response,
                                                        Class<T> sourceClass) {
        if (response.getHits().getTotalHits().value == 0) {
            return ScrollPage.empty();
        }
        List<T> ret = Arrays.stream(response.getHits().getHits()).map(hit -> {
            T object = JSON.parseObject(
                    hit.getSourceAsString(), sourceClass);
            //object.set_id(hit.getId());
            try {
                writeIdValue(object, hit.getId());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return object;
        }).collect(Collectors.toList());
        return ScrollPage.newScrollPage(response.getHits().getTotalHits().value, ret, response.getScrollId());
    }

    public static <T> T convertSingle(SearchResponse response, Class<T> sourceClass) {
        if (response.getHits().getTotalHits().value == 0) {
            return null;
        }
        T object = JSON.parseObject(
                response.getHits().getAt(0).getSourceAsString(), sourceClass);
        //object.set_id(response.getHits().getAt(0).getId());
        try {
            writeIdValue(object, response.getHits().getAt(0).getId());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static <T> T convertSingle(GetResponse response, Class<T> sourceClass) {
        if (!response.isExists())
            return null;
        T result = JSON.parseObject(response.getSourceAsBytes(), sourceClass);
        try {
            writeIdValue(result, response.getId());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <T> Field getIdField(Class<T> sourceClass) {
        List<Field> lst = FieldUtils.getFieldsListWithAnnotation(sourceClass, Id.class);
        return lst.isEmpty() ? null : lst.get(0);
    }

    public static <T> Field getCreateAtField(Class<T> sourceClass) {
        List<Field> lst = FieldUtils.getFieldsListWithAnnotation(sourceClass, CreateAt.class);
        return lst.isEmpty() ? null : lst.get(0);
    }

    public static <T> void writeCreateAtValue(T obj) throws IllegalAccessException {
        Field createAtField = getCreateAtField(obj.getClass());
        if (createAtField == null)
            return;
        Class<?> type = createAtField.getType();
        createAtField.setAccessible(true);

        try {
            if (type.isAssignableFrom(String.class)) {
                createAtField.set(obj, DateUtils.formatDate(new Date()));
                return;
            }
            if (type.isAssignableFrom(Date.class)) {
                createAtField.set(obj, new Date());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static <T> void writeIdValue(T obj, String value) throws IllegalAccessException {
        Field idField = getIdField(obj.getClass());
        if (idField == null)
            return;
        Class<?> type = idField.getType();
        Object v = StringConvertUtil.convertType(type, value);
        idField.setAccessible(true);
        idField.set(obj, v);
    }

    public static <T> String getIdStrValue(T obj) throws IllegalAccessException {
        Field idField = getIdField(obj.getClass());
        if (idField == null)
            return null;
        idField.setAccessible(true);
        Object v = idField.get(obj);
        return v == null ? null : v.toString();
    }

}
