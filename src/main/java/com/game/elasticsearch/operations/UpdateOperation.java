package com.game.elasticsearch.operations;

import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.common.lucene.uid.Versions;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.script.Script;

public class UpdateOperation<T> {

    private String index;
    private String type;
    private String id;
    private String routing;
    private String parent;
    private T source;
    private Script script;
    private long version = Versions.MATCH_ANY;
    private VersionType versionType = VersionType.INTERNAL;
    private int retryOnConflict = 0;
    private WriteRequest.RefreshPolicy refreshPolicy = WriteRequest.RefreshPolicy.NONE;
    private boolean scriptedUpsert = false;
    private boolean docAsUpsert = false;
    private boolean detectNoop = true;
    public UpdateOperation() {
    }

    public UpdateOperation(String index, String type, String id) {
        this.index = index;
        this.type = type;
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public UpdateOperation<T> setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getType() {
        return type;
    }

    public UpdateOperation<T> setType(String type) {
        this.type = type;
        return this;
    }

    public String getId() {
        return id;
    }

    public UpdateOperation<T> setId(String id) {
        this.id = id;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public UpdateOperation<T> setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public T getSource() {
        return source;
    }

    public UpdateOperation<T> setSource(T source) {
        this.source = source;
        return this;
    }

    public Script getScript() {
        return script;
    }

    public UpdateOperation<T> setScript(Script script) {
        this.script = script;
        return this;
    }

    public long getVersion() {
        return version;
    }

    public UpdateOperation<T> setVersion(long version) {
        this.version = version;
        return this;
    }

    public VersionType getVersionType() {
        return versionType;
    }

    public UpdateOperation<T> setVersionType(VersionType versionType) {
        this.versionType = versionType;
        return this;
    }

    public int getRetryOnConflict() {
        return retryOnConflict;
    }

    public UpdateOperation<T> setRetryOnConflict(int retryOnConflict) {
        this.retryOnConflict = retryOnConflict;
        return this;
    }

    public WriteRequest.RefreshPolicy getRefreshPolicy() {
        return refreshPolicy;
    }

    public UpdateOperation<T> setRefreshPolicy(WriteRequest.RefreshPolicy refreshPolicy) {
        this.refreshPolicy = refreshPolicy;
        return this;
    }

    public boolean getScriptedUpsert() {
        return scriptedUpsert;
    }

    public UpdateOperation<T> setScriptedUpsert(boolean scriptedUpsert) {
        this.scriptedUpsert = scriptedUpsert;
        return this;
    }

    public boolean getDocAsUpsert() {
        return docAsUpsert;
    }

    public UpdateOperation<T> setDocAsUpsert(boolean docAsUpsert) {
        this.docAsUpsert = docAsUpsert;
        return this;
    }

    public boolean getDetectNoop() {
        return detectNoop;
    }

    public UpdateOperation<T> setDetectNoop(boolean detectNoop) {
        this.detectNoop = detectNoop;
        return this;
    }

    public String getParent() {
        return parent;
    }

    public UpdateOperation<T> setParent(String parent) {
        this.parent = parent;
        return this;
    }
}
