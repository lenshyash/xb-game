package com.game.elasticsearch.operations;

import org.elasticsearch.common.lucene.uid.Versions;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiGetOperation<T> {

    private String index;
    private String type;
    private List<String> ids = new ArrayList<>();
    private String routing;
    private String parent;
    private String preference;
    private boolean refresh = false;
    private boolean realtime = true;
    private VersionType versionType = VersionType.INTERNAL;
    private long version = Versions.MATCH_ANY;
    private FetchSourceContext fetchSourceContext;
    private Class<T> sourceClass;

    public String getIndex() {
        return index;
    }

    public MultiGetOperation(String index, String type,Class<T> sourceClass) {
        this.index = index;
        this.type = type;
        this.sourceClass = sourceClass;
    }

    public MultiGetOperation<T> setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getType() {
        return type;
    }

    public MultiGetOperation<T> setType(String type) {
        this.type = type;
        return this;
    }

    public List<String> getIds() {
        return ids;
    }

    public MultiGetOperation<T> setIds(List<String> ids) {
        this.ids = ids;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public MultiGetOperation<T> setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public String getParent() {
        return parent;
    }

    public MultiGetOperation<T> setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public String getPreference() {
        return preference;
    }

    public MultiGetOperation<T> setPreference(String preference) {
        this.preference = preference;
        return this;
    }

    public boolean getRefresh() {
        return refresh;
    }

    public MultiGetOperation<T> setRefresh(boolean refresh) {
        this.refresh = refresh;
        return this;
    }

    public boolean getRealtime() {
        return realtime;
    }

    public MultiGetOperation<T> setRealtime(boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    public VersionType getVersionType() {
        return versionType;
    }

    public MultiGetOperation<T> setVersionType(VersionType versionType) {
        this.versionType = versionType;
        return this;
    }

    public long getVersion() {
        return version;
    }

    public MultiGetOperation<T> setVersion(long version) {
        this.version = version;
        return this;
    }

    public FetchSourceContext getFetchSourceContext() {
        return fetchSourceContext;
    }

    public MultiGetOperation<T> setFetchSourceContext(FetchSourceContext fetchSourceContext) {
        this.fetchSourceContext = fetchSourceContext;
        return this;
    }

    public Class<T> getSourceClass() {
        return sourceClass;
    }

    public MultiGetOperation<T> setSourceClass(Class<T> sourceClass) {
        this.sourceClass = sourceClass;
        return this;
    }

    public MultiGetOperation<T> addId(String... ids) {
        this.ids.addAll(Arrays.asList(ids));
        return this;
    }
}
