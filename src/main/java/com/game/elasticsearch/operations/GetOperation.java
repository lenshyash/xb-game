package com.game.elasticsearch.operations;

import org.elasticsearch.common.Strings;
import org.elasticsearch.common.lucene.uid.Versions;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class GetOperation<T> {

    private String type;
    private String id;
    private String routing;
    private String parent;
    private String preference;
    private String index;
    /* private boolean fetchSource;
     private String[] includes;
     private String[] excludes;*/
    private boolean refresh = false;
    private VersionType versionType = VersionType.INTERNAL;
    private long version = Versions.MATCH_ANY;
    private FetchSourceContext fetchSourceContext;
    private Class<T> sourceClass;

    public GetOperation(String index, String type, String id, Class<T> sourceClass) {
        checkArgument(Strings.hasText(index), "index name is required");
        checkArgument(Strings.hasText(type), "type name is required");
        checkArgument(Strings.hasText(id), "id is required");
        checkNotNull(sourceClass,"source class is required");
        this.type = type;
        this.id = id;
        this.index = index;
        this.sourceClass = sourceClass;
    }

    public String getType() {
        return type;
    }

    public GetOperation<T> setType(String type) {
        this.type = type;
        return this;
    }

    public String getId() {
        return id;
    }

    public GetOperation<T> setId(String id) {
        this.id = id;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public GetOperation<T> setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public String getParent() {
        return parent;
    }

    public GetOperation<T> setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public String getPreference() {
        return preference;
    }

    public GetOperation<T> setPreference(String preference) {
        this.preference = preference;
        return this;
    }

    public String getIndex() {
        return index;
    }

    public GetOperation<T> setIndex(String index) {
        this.index = index;
        return this;
    }

    public boolean getRefresh() {
        return refresh;
    }

    public GetOperation<T> setRefresh(boolean refresh) {
        this.refresh = refresh;
        return this;
    }

    public VersionType getVersionType() {
        return versionType;
    }

    public GetOperation<T> setVersionType(VersionType versionType) {
        this.versionType = versionType;
        return this;
    }

    public long getVersion() {
        return version;
    }

    public GetOperation<T> setVersion(long version) {
        this.version = version;
        return this;
    }

    public FetchSourceContext getFetchSourceContext() {
        return fetchSourceContext;
    }

    public GetOperation<T> setFetchSourceContext(FetchSourceContext fetchSourceContext) {
        this.fetchSourceContext = fetchSourceContext;
        return this;
    }

    public Class<T> getSourceClass() {
        return sourceClass;
    }

    public GetOperation<T> setSourceClass(Class<T> sourceClass) {
        this.sourceClass = sourceClass;
        return this;
    }
}
