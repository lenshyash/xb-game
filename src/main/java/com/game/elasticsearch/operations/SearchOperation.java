package com.game.elasticsearch.operations;

import com.game.common.data.XSort;
import com.google.common.collect.ImmutableList;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;

import java.util.LinkedList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by terry on 2017/6/13.
 */
public class SearchOperation<T> {

    private QueryBuilder query;
    private QueryBuilder filter;
    private List<String> indices = new LinkedList<>();
    private List<String> types = new LinkedList<>();
    private String preference;
    private String routing;
    private SearchType searchType;
    private Class<T> sourceClass;
    private Boolean requestCache;
    private XSort sort;
    private FetchSourceContext fetchSourceContext;
    private int from = 0;
    private int size = 10;

    public SearchOperation(final String index,
                           final QueryBuilder query) {
        this(ImmutableList.of(index), ImmutableList.of(), query, null);
    }

    public SearchOperation(final String index,
                           final QueryBuilder query, final Class<T> sourceClass) {
        this(ImmutableList.of(index), ImmutableList.of(), query, sourceClass);
    }

    public SearchOperation(final String index,
                           final String type,
                           final QueryBuilder query,
                           final Class<T> sourceClass) {
        this(ImmutableList.of(index), ImmutableList.of(type), query, sourceClass);
    }

    public SearchOperation(final List<String> indices,
                           final List<String> types,
                           final QueryBuilder query,
                           final Class<T> sourceClass) {
        checkNotNull(query, "query object is required");
        checkArgument(indices != null && !indices.isEmpty(), "indices is empty");
        //checkArgument(types != null && !types.isEmpty(), "types is empty");
        //checkNotNull(sourceClass, "source class is required");
        this.query = query;
        this.indices = indices;
        this.types = types;
        this.sourceClass = sourceClass;
    }

    public QueryBuilder getQuery() {
        return query;
    }

    public SearchOperation setQuery(QueryBuilder query) {
        this.query = query;
        return this;
    }

    public QueryBuilder getFilter() {
        return filter;
    }

    public SearchOperation setFilter(QueryBuilder filter) {
        this.filter = filter;
        return this;
    }

    public List<String> getIndices() {
        return indices;
    }

    /*public SearchOperation setIndices(List<String> indices) {
        this.indices = indices;
        return this;
    }*/

    public List<String> getTypes() {
        return types;
    }

    /*public SearchOperation setTypes(List<String> types) {
        this.types = types;
        return this;
    }*/

    public Class<T> getSourceClass() {
        return sourceClass;
    }

    /*public SearchOperation<T> setSourceClass(Class<T> sourceClass) {
        this.sourceClass = sourceClass;
        return this;
    }*/

    public String getPreference() {
        return preference;
    }

    public SearchOperation<T> setPreference(String preference) {
        this.preference = preference;
        return this;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public SearchOperation<T> setSearchType(SearchType searchType) {
        this.searchType = searchType;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public SearchOperation<T> setRouting(String routing) {
        this.routing = routing;
        return this;
    }


    public Boolean getRequestCache() {
        return requestCache;
    }

    public SearchOperation<T> setRequestCache(Boolean requestCache) {
        this.requestCache = requestCache;
        return this;
    }

    public XSort getSort() {
        return sort;
    }

    public SearchOperation<T> setSort(XSort sort) {
        this.sort = sort;
        return this;
    }

    public FetchSourceContext getFetchSourceContext() {
        return fetchSourceContext;
    }

    public SearchOperation<T> setFetchSourceContext(FetchSourceContext fetchSourceContext) {
        this.fetchSourceContext = fetchSourceContext;
        return this;
    }

    public SearchOperation<T> setFetchSource(String[] includes, String[] excludes) {
        FetchSourceContext fetchSourceContext = this.fetchSourceContext != null ? this.fetchSourceContext
                : FetchSourceContext.FETCH_SOURCE;
        this.fetchSourceContext = new FetchSourceContext(fetchSourceContext.fetchSource(), includes, excludes);
        return this;
    }

    public SearchOperation<T> setFetchSource(String include, String exclude) {
        return setFetchSource(include == null ? Strings.EMPTY_ARRAY : new String[]{include}, exclude == null ? Strings.EMPTY_ARRAY
                : new String[]{exclude});
    }

    public int getFrom() {
        return from;
    }

    public SearchOperation<T> setFrom(int from) {
        this.from = from;
        return this;
    }

    public int getSize() {
        return size;
    }

    public SearchOperation<T> setSize(int size) {
        this.size = size;
        return this;
    }
}
