package com.game.elasticsearch.bulk;

import com.game.elasticsearch.Client;
import com.game.elasticsearch.ElasticsearchTemplate;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-12-30.
 */
public class BatchTakeQueueFactoryBean implements FactoryBean<BulkIndexQueue>, InitializingBean {

    private Properties settings;

    private ElasticsearchTemplate elasticsearchTemplate;

    private BulkIndexQueue bulkIndexQueue;

    @Override
    public BulkIndexQueue getObject() {
        return bulkIndexQueue;
    }

    @Override
    public Class<?> getObjectType() {
        return BulkIndexQueue.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        Settings.Builder settingsBuilder = Settings.builder();
        if (this.settings != null) {
            settingsBuilder.put("queue.capacity", Integer.parseInt(settings.getProperty("queue.capacity", "500000")));
            settingsBuilder.put("queue.take.size", Integer.parseInt(settings.getProperty("queue.take.size", "10000")));
            settingsBuilder.put("queue.take.interval",
                    TimeValue.timeValueMillis(Long.parseLong(settings.getProperty("queue.take.interval", "1000"))));
        }
        Settings _settings = settingsBuilder.build();
        if (_settings.isEmpty()) {
            throw new RuntimeException("BulkIndexQueue configuration not found");
        }
        this.bulkIndexQueue = new BulkIndexQueue(_settings, elasticsearchTemplate);
    }

    public void start(){
        this.bulkIndexQueue.start();
    }

    public void stop(){
        this.bulkIndexQueue.stop();
    }

    public Properties getSettings() {
        return settings;
    }

    public void setSettings(Properties settings) {
        this.settings = settings;
    }

    public ElasticsearchTemplate getElasticsearchTemplate() {
        return elasticsearchTemplate;
    }

    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }
}
