package com.game.elasticsearch;



import com.game.common.data.PageImpl;

import java.util.List;

public class ScrollPageImpl<T> extends PageImpl<T> implements ScrollPage<T> {

    private String scrollId;

    public ScrollPageImpl(String scrollId) {
        this.scrollId = scrollId;
    }

    public ScrollPageImpl(long total, List<T> records, String scrollId) {
        super(total, records);
        this.scrollId = scrollId;
    }

    @Override
    public String getScrollId() {
        return this.scrollId;
    }
}
