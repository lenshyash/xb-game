package com.game.elasticsearch;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-12-30.
 */
public class ESClientFactoryBean implements FactoryBean<Client>, InitializingBean {

    private Properties settings;

    private Client client;

    @Override
    public Client getObject() throws Exception {
        return client;
    }

    @Override
    public Class<?> getObjectType() {
        return Client.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Settings.Builder settingsBuilder = Settings.builder();
        if (this.settings != null) {
            settingsBuilder.putList("es.hosts", Stream.of(settings.getProperty("es.hosts")
                    .split(";")).map(String::trim).collect(Collectors.toList()));
        }
        Settings _settings = settingsBuilder.build();
        if (_settings.isEmpty()) {
            throw new RuntimeException("ElasticSearch Client configuration not found");
        }
        this.client = new Client(_settings);
    }

    public Properties getSettings() {
        return settings;
    }

    public void setSettings(Properties settings) {
        this.settings = settings;
    }
}
