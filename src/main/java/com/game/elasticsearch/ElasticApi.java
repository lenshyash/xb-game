package com.game.elasticsearch;

import com.alibaba.fastjson.JSON;
import com.game.common.data.Page;
import com.game.core.SystemConfig;
import com.game.elasticsearch.bulk.BulkIndexQueue;
import com.game.elasticsearch.operations.IndexOperation;
import com.game.elasticsearch.operations.SearchOperation;
import com.game.elasticsearch.operations.UpdateOperation;
import com.game.model.MnyMoneyRecord;
import com.game.util.BeanDtoVoUtils;
import org.apache.poi.ss.formula.functions.T;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class ElasticApi {

    private  static Client client;

    private  static ElasticsearchTemplate elasticsearchTemplate;

    private  static BulkIndexQueue bulkIndexQueue;

    public static String DEFAULT_TYPE= "_doc";

    /**
     * 初始化Es服务及内存队列
     *
     * @throws IOException
     */


    public static void index(IndexOperation<?> indexOperation) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return;
        }

        String id = elasticsearchTemplate
                .index(indexOperation);
        System.err.println(id);
    }

    public static void updateOne(UpdateOperation<?> updateOperation){
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return;
        }
        elasticsearchTemplate.update(updateOperation);
//                new UpdateOperation<Pojo>("pojo", DEFAULT_TYPE, "AWyDigpiKYSqiRRMtVWm").setSource(new Pojo("bgc", 35)));
    }

    /**
     * 添加对象记录到队列  自动定时同步到es
     * @param indexOperation
     */
    public static void addQueue(IndexOperation<?> indexOperation) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return;
        }
        bulkIndexQueue.add(indexOperation);
    }

    /**
     * 搜索单个doc
     */
    public static <T> T searchOne( String index, String type, QueryBuilder query, Class<T> sourceClass) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return null;
        }
        T t = (T) elasticsearchTemplate.searchOne(new SearchOperation(index, type,query
               ,sourceClass));
//        Thread.sleep(2000);
        return t;
    }

    /**
     * 获取list数据
     * @param operation
     * @param <T>
     * @return
     */
    public static <T> List<T> searchForList(SearchOperation<T> operation) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return null;
        }
        List list = elasticsearchTemplate.search(operation);
        return list;
    }
    /**
     * 分页获取doc,转换成平台分页对象
     * @return
     */
    public static <T> org.jay.frame.jdbc.Page<T> searchForPage(SearchOperation<T> operation,Class<T> sourceClass) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return null;
        }
        Page page = elasticsearchTemplate.searchForPage(operation);
        if(page!=null){
            return  new org.jay.frame.jdbc.Page(operation.getFrom(),page.getTotal(),operation.getSize(), BeanDtoVoUtils.listVo(page.getRecords(),sourceClass));
        }
        return null;
    }

    public static Double sum(SearchOperation sO, String money) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return null;
        }
        return elasticsearchTemplate.sum(sO,money);
    }

    public static <T> org.jay.frame.jdbc.Page<T> searchForPageMap(SearchOperation<T> operation,Class<T> sourceClass) {
        //判断是否启用es
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
            return null;
        }
        Page page = elasticsearchTemplate.searchForPage(operation);
        if(page!=null){
            return  new org.jay.frame.jdbc.Page(operation.getFrom(),page.getTotal(),operation.getSize(), BeanDtoVoUtils.listConvert(page.getRecords()));
        }
        return null;
    }

    public static Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ElasticsearchTemplate getElasticsearchTemplate() {
        return elasticsearchTemplate;
    }

    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }


    public static BulkIndexQueue getBulkIndexQueue() {
        return bulkIndexQueue;
    }

    public void setBulkIndexQueue(BulkIndexQueue bulkIndexQueue) {
        this.bulkIndexQueue = bulkIndexQueue;
    }
}
