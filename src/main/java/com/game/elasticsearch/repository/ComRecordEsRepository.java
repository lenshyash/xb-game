package com.game.elasticsearch.repository;

import com.game.common.data.XSort;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.SearchOperation;
import com.game.model.MnyComRecord;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.ReportParamVo;
import com.game.util.StationUtil;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ComRecordEsRepository {

    public Page<MnyComRecord> getPage(MnyComRecordVo mcrVo) {
        QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(ActionUtil.getRequest());
        Page<MnyComRecord> page;
        Long stationId = mcrVo.getStationId();
        Long type = mcrVo.getType();
        Long handlerType = mcrVo.getHandlerType();
        Long status = mcrVo.getStatus();
        Long lock = mcrVo.getLockFlag();
        Long accountId = mcrVo.getAccountId();
        String account = StringUtil.trim2Empty(mcrVo.getAccount());
        String orderNo = StringUtil.trim2Empty(mcrVo.getOrderNo());
        BigDecimal minMoney = mcrVo.getMinMoney();
        BigDecimal maxMoney = mcrVo.getMaxMoney();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.matchQuery("stationId", stationId));
        if (Validator.isNotNull(type)) {
            queryBuilder.must(QueryBuilders.matchQuery("type", type));
        }
        if (Validator.isNotNull(status)) {
            queryBuilder.must(QueryBuilders.matchQuery("status", status));
        }
        if (StringUtil.isNotEmpty(orderNo)) {
            queryBuilder.must(QueryBuilders.matchQuery("orderNo", orderNo));
        }

        if (Validator.isNotNull(lock)) {
            queryBuilder.must(QueryBuilders.matchQuery("lockFlag", lock));
        }
        if (Validator.isNotNull(handlerType)) {
            queryBuilder.must(QueryBuilders.matchQuery("handlerType", handlerType));
        }

        if (StringUtil.isNotEmpty(account)) {
            queryBuilder.must(QueryBuilders.matchQuery("account", account.toLowerCase()));
        }

        if (Validator.isNotNull(accountId)) {
            queryBuilder.must(QueryBuilders.matchQuery("memberId", accountId));
        }

        if (minMoney != null && minMoney.compareTo(BigDecimal.ZERO) == 1) {
            queryBuilder.filter(QueryBuilders.rangeQuery("money").gte(minMoney));
        }

        if (maxMoney != null && maxMoney.compareTo(BigDecimal.ZERO) == 1) {
            queryBuilder.filter(QueryBuilders.rangeQuery("money").lte(maxMoney));
        }

        if (StringUtils.isNotEmpty(mcrVo.getOperator())) {
            queryBuilder.must(QueryBuilders.matchQuery("modifyUser",  mcrVo.getOperator()));
        }

        if (StringUtils.isNotEmpty(mcrVo.getOpDesc())) {
            queryBuilder.must(QueryBuilders.wildcardQuery("opDesc", "*" + mcrVo.getOpDesc() + "*"));
        }

        if (StringUtils.isNotEmpty(mcrVo.getPaySmallName()) && !"0".equals(mcrVo.getPaySmallName())) {
            queryBuilder.must(QueryBuilders.matchQuery("payName", mcrVo.getPaySmallName()));
        }

        if (Validator.isNotNull(mcrVo.getBegin())) {
            queryBuilder.filter(QueryBuilders.rangeQuery("createDatetime").gte( mcrVo.getBegin().getTime()));
        }

        if (Validator.isNotNull(mcrVo.getEnd())) {
            queryBuilder.filter(QueryBuilders.rangeQuery("createDatetime").lte( mcrVo.getEnd().getTime()));
        }

        if (Validator.isNotNull(mcrVo.getReportType())) {
            queryBuilder.must(QueryBuilders.matchQuery("reportType",mcrVo.getReportType()));
        }

        if (Validator.isNotNull(mcrVo.getAccountType())) {
            queryBuilder.must(QueryBuilders.matchQuery("accountType",mcrVo.getAccountType()));
        }
        if (Validator.isNotNull(mcrVo.getComTimes())) {
            queryBuilder.must(QueryBuilders.matchQuery("comTimes", mcrVo.getComTimes()));
        }
        if (Validator.isNotNull(mcrVo.getRemark())) {
            queryBuilder.must(QueryBuilders.wildcardQuery( "remark","*"+mcrVo.getRemark()+"*"));
        }
        if (StationUtil.isDailiStation()) {
            if (StringUtil.isNotEmpty(mcrVo.getParents())) {
                String parents;
                if (StringUtil.equals(mcrVo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
                    parents =  mcrVo.getParents();
                }else {
                    parents =  mcrVo.getParents()+ "*";
                }
                if (mcrVo.getSearchSelf() != null && mcrVo.getSearchSelf()) {
                    BoolQueryBuilder bqb1 = QueryBuilders.boolQuery();
                    bqb1.must(QueryBuilders.wildcardQuery("parents", parents));
                    bqb1.should(QueryBuilders.termQuery("memberId", mcrVo.getSelfId()));
                    queryBuilder.must(bqb1);
                } else {
                    queryBuilder.must(QueryBuilders.wildcardQuery( "parents",parents));
                }
            }
        } else {
            if (StringUtils.isNotEmpty(mcrVo.getAgentName())) {
                queryBuilder.must(QueryBuilders.wildcardQuery( "parentNames","*" + mcrVo.getAgentName() + "*"));
            }
        }
        SearchOperation sO = new SearchOperation(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),queryBuilder, MnyComRecord.class );

        sO.setFrom((webParam.getPageNo()-1)*webParam.getPageSize()).setSize(webParam.getPageSize());
        sO.setSort(new XSort(XSort.Direction.DESC,"createDatetime"));
        page  = (Page<MnyComRecord>) ElasticApi.searchForPage(sO,MnyComRecord.class);
        //求和
        queryBuilder.must(QueryBuilders.matchQuery("status",  MnyComRecord.STATUS_SUCCESS));
        SearchOperation s1 = new SearchOperation(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),queryBuilder, MnyComRecord.class );
        double sum = ElasticApi.sum(s1,"money");
        Map<String,Object> map = new HashMap<>();
        map.put("money",sum);
        page.setAggsData(map);
        return page;
    }
}
