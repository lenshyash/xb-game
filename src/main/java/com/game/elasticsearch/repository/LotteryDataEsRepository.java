package com.game.elasticsearch.repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.redis.RedisAPI;
import com.game.common.data.XSort;
import com.game.dao.lottery.LongRowMapper;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.SearchOperation;
import com.game.model.lottery.BcLotteryData;
import com.game.model.vo.BcLotteryDataVo;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.ActionUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class LotteryDataEsRepository {
    public Page<BcLotteryDataVo> page(Long stationId, String code, Integer actionNo, String qihao, Date startDate,
                                      Date endDate, String sortName, String sortOrder) {
        QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(ActionUtil.getRequest());
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.filter(QueryBuilders.rangeQuery("endTime").gte(startDate.getTime()).lt(endDate.getTime()));
        Page<BcLotteryDataVo> page;
        if (stationId != null) {
            queryBuilder.must(QueryBuilders.termQuery("stationId", stationId));
        }
        if (StringUtils.isNotEmpty(code)) {
            queryBuilder.must(QueryBuilders.matchQuery("lotCode", code));
        }
        if (actionNo != null) {
            queryBuilder.must(QueryBuilders.matchQuery("actionNo", actionNo));
        }
        if (StringUtils.isNotEmpty(qihao)) {
            queryBuilder.must(QueryBuilders.wildcardQuery("qiHao", "*" + qihao + "*"));
        }
        SearchOperation sO = new SearchOperation(EsEnum.BC_LOTTERY_DATA.getIndexTag(),EsEnum.BC_LOTTERY_DATA.getType(),queryBuilder, BcLotteryData.class );
        sO.setFrom((webParam.getPageNo()-1)*webParam.getPageSize()).setSize(webParam.getPageSize());
        sO.setSort(new XSort(XSort.Direction.DESC,"qiHao.keyword"));
        page  = (Page<BcLotteryDataVo>) ElasticApi.searchForPage(sO,BcLotteryDataVo.class);
        return page;
    }

    public List<BcLotteryData> find(String lotCode, Date startTime, Date endTime, Long stationId, Integer pageSize) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.filter(QueryBuilders.rangeQuery("endTime").gte(startTime.getTime()).lt(endTime.getTime()));
        List<BcLotteryData> list;
        if (!StringUtils.isEmpty(lotCode)) {
            queryBuilder.must(QueryBuilders.matchQuery("lotCode", lotCode));
        }
        if (stationId != null) {
            queryBuilder.must(QueryBuilders.matchQuery("stationId", stationId));
        }
        SearchOperation sO = new SearchOperation(EsEnum.BC_LOTTERY_DATA.getIndexTag(),EsEnum.BC_LOTTERY_DATA.getType(),queryBuilder, BcLotteryData.class );
        if (pageSize != null && pageSize > 0) {
            sO.setFrom(0).setSize(pageSize);
        }
        sO.setSort(new XSort(XSort.Direction.DESC,"qiHao.keyword"));
        list  = (List<BcLotteryData>) ElasticApi.searchForList(sO);
        return list;
    }
}
