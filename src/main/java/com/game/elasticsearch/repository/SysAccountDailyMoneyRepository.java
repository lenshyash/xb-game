package com.game.elasticsearch.repository;

import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.ElasticsearchTemplate;
import com.game.elasticsearch.contant.EsEnum;
import com.game.model.vo.ReportParamVo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2021-02-07.
 */
@Component
public class SysAccountDailyMoneyRepository {



    public Map<String, Object> getReport(ReportParamVo vo) {
        BoolQueryBuilder baseQuery = QueryBuilders.boolQuery().must(termQuery("stationId", vo.getStationId()));
        SearchRequest searchRequest = new SearchRequest(EsEnum.SYS_ACCOUNT_DAILY_MONEY.getIndexTag());

        if (StringUtil.isNotEmpty(vo.getAccountId())) {
            baseQuery.must(termQuery("accountId", vo.getAccountId()));
        }
        if (StringUtil.isNotEmpty(vo.getAccount())) {
            baseQuery.must(termQuery("account", vo.getAccount()));
        }
        if (StringUtil.isNotEmpty(vo.getAccount())) {
            baseQuery.must(termQuery("account", vo.getAccount()));
        }
        if (StringUtil.isNotEmpty(vo.getReportType())) {
            baseQuery.must(termQuery("reportType", vo.getReportType()));
        }
        SearchSourceBuilder source = new SearchSourceBuilder().query(baseQuery).size(0)
                .aggregation(sum("first_deposit_total").field("firstDeposit"))
                //SUM(deposit_amount) AS deposit_total;
                .aggregation(sum("deposit_total").field("depositAmount"))
                //sum(withdraw_times) as withdraw_times
                .aggregation(sum("withdraw_times").field("withdrawTimes"))
                //COUNT(DISTINCT CASE WHEN deposit_amount <>0 OR deposit_artificial <>0  THEN account_id ELSE NULL END) as deposit_member_count
                .aggregation(cardinality("deposit_member_count").script(new Script("doc.depositAmount.value != 0 && doc.positArtificial.value != 0 " +
                        "? doc.accountId.value: null")))
                //COUNT(DISTINCT CASE WHEN withdraw_amount <>0 THEN account_id ELSE NULL END) as withdraw_member_count
                .aggregation(cardinality("withdraw_member_count").script(new Script("doc.withdrawAmount.value !=0 ? doc.accountId.value: null")))
                //SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_total
                .aggregation(sum("manual_deposit_total").script(new Script("doc.depositArtificial.value + doc.sysApiDepositAmount.value")))
                //SUM(balance_gem_amount) AS balance_gem_total
                .aggregation(sum("balance_gem_total").field("balanceGemAmount"))
                //SUM(withdraw_artificial) AS manual_withdraw_total
                .aggregation(sum("manual_withdraw_total").field("withdrawArtificial"))
                //SUM(deposit_times+deposit_handler_times) AS deposit_times
                .aggregation(sum("deposit_times").script(new Script("doc.depositTimes.value + doc.depositHandlerTimes.value")))
                /*sql_sb.append(" COUNT(DISTINCT CASE WHEN egame_bet_amount <>0 OR ");
                sql_sb.append(" hunter_bet_amount <>0 OR ");
                sql_sb.append(" sports_bet_amount <>0 OR ");
                sql_sb.append(" lottery_bet_amount <>0 OR ");
                sql_sb.append(" sys_lottery_bet_amount <>0 OR ");
                sql_sb.append(" mark_six_bet_amount <>0 OR ");
                sql_sb.append(" sf_mark_six_bet_amount <>0 OR ");
                sql_sb.append(" third_sports_bet_amount <>0 OR ");
                sql_sb.append(" third_lottery_bet_amount <>0 OR ");
                sql_sb.append(" chess_bet_amount <>0 OR ");
                sql_sb.append(" esports_bet_amount <>0 OR ");
                sql_sb.append(" real_bet_amount <>0");
                sql_sb.append(" THEN account_id ELSE NULL END) AS bet_count_total,");*/
                .aggregation(cardinality("bet_count_total").script(new Script("doc.egameBetAmount.value !=0 || doc.hunterBetAmount.value !=0 || " +
                        "doc.sportsBetAmount.value !=0 || doc.lotteryBetAmount.value !=0 || doc.sysLotteryBetAmount.value !=0 || " +
                        "doc.sfMarkSixBetAmount.value !=0 || doc.thirdSportsBetAmount.value !=0 || doc.thirdLotteryBetAmount.value !=0 || " +
                        "doc.chessBetAmount.value != 0 || doc.esportsBetAmount.value !=0 || doc.realBetAmount.value !=0 ? doc.accountId.value : " +
                        "null")))
                /* sql_sb.append(" SUM(lottery_rebate_amount");
                sql_sb.append("+sys_lottery_rebate_amount");
                sql_sb.append("+mark_six_rebate_amount");
                sql_sb.append("+sports_rebate_amount");
                sql_sb.append("+egame_rebate_amount");
                sql_sb.append("+hunter_rebate_amount");
                sql_sb.append("+real_rebate_amount");
                sql_sb.append("+chess_rebate_amount");
                sql_sb.append("+third_lottery_rebate_amount");
                sql_sb.append("+third_sports_rebate_amount");
                sql_sb.append("+esports_rebate_amount");
                sql_sb.append(") AS rebate_total,");*/
                .aggregation(sum("rebate_total").script(new Script("doc.lotteryRebateAmount.value + doc.sysLotteryRebateAmount.value + doc" +
                        ".markSixRebateAmount.value + doc.sportsRebateAmount.value + doc.egameRebateAmount.value + doc.hunterRebateAmount.value " +
                        "+ doc.realRebateAmount.value + doc.chessRebateAmount.value + doc.thirdLotteryRebateAmount.value " +
                        "+ doc.thirdSportsRebateAmount.value + doc.esportsRebateAmount.value")));
        SearchResponse searchResponse = null;
        try {
            searchResponse = ElasticApi.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        MapBuilder<String, Object> retBuilder = MapBuilder.newMapBuilder();
        Map<String, Object> ret = retBuilder.put("first_deposit_total", BigDecimal.valueOf(
                    ((Sum) searchResponse.getAggregations().get("first_deposit_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("balance_gem_total", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("balance_gem_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("deposit_times", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("deposit_times")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("deposit_total", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("deposit_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("withdraw_times", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("withdraw_times")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("manual_deposit_total", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("manual_deposit_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("manual_withdraw_total", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("manual_withdraw_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .put("deposit_member_count", ((Cardinality) searchResponse.getAggregations().get("deposit_member_count")).getValue())
                .put("withdraw_member_count", ((Cardinality) searchResponse.getAggregations().get("withdraw_member_count")).getValue())
                .put("bet_count_total", ((Cardinality) searchResponse.getAggregations().get("bet_count_total")).getValue())
                .put("rebate_total", BigDecimal.valueOf(
                        ((Sum) searchResponse.getAggregations().get("rebate_total")).getValue()
                ).setScale(2, RoundingMode.HALF_UP))
                .immutableMap();
        return ret;
    }
}
