package com.game.elasticsearch.repository;

import com.alibaba.fastjson.JSONObject;
import com.game.common.data.XSort;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.SearchOperation;
import com.game.model.MnyMoneyRecord;
import com.game.model.vo.MnyMoneyRecordVo;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;
import java.util.Map;

@Repository
public class MoneyRecordEsRepository {

    public Page<Map> getPage(MnyMoneyRecordVo moneyVo) {
        QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(ActionUtil.getRequest());
        Page<Map> page;
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        String account = StringUtil.trim2Empty(moneyVo.getAccount());
        if (StringUtil.isNotEmpty(account)) {
            queryBuilder.must(QueryBuilders.matchQuery("account", account));
        }
        if (Validator.isNotNull(moneyVo.getAccountType())) {
            queryBuilder.must(QueryBuilders.matchQuery("accountType",  moneyVo.getAccountType()));
        }
        if (Validator.isNotNull(moneyVo.getReportType())) {
            queryBuilder.must(QueryBuilders.matchQuery("reportType", moneyVo.getReportType()));
        }
        if (Validator.isNotNull(moneyVo.getStationId())) {
            queryBuilder.must(QueryBuilders.matchQuery("stationId", moneyVo.getStationId()));
        }
        // TODO 过滤返点反水回滚 18/1/2
//		if (moneyVo.isShowRollBack()) {
//			sql_sb.append(" AND  m.type != 10 AND m.type != 12 AND m.type != 14");
//		}

        // 过滤掉人工加扣款
        if (moneyVo.isShowHandMoney()) {
            queryBuilder.mustNot(QueryBuilders.matchQuery("moneyType",2));
        } else {
            if (Validator.isNotNull(moneyVo.getType())) {
                queryBuilder.must(QueryBuilders.matchQuery("moneyType",moneyVo.getType()));
            }
        }
        if (moneyVo.isShowChangeOrder()) {
            queryBuilder.mustNot(QueryBuilders.matchQuery("moneyType",171));
            queryBuilder.mustNot(QueryBuilders.matchQuery("moneyType",172));
        }
        if (Validator.isNotNull(moneyVo.getAccountId())) {
            queryBuilder.must(QueryBuilders.matchQuery("accountId", moneyVo.getAccountId()));
        }
        if (StringUtil.isNotEmpty(moneyVo.getParents())) {
            String parents =  moneyVo.getParents() + "*";
            if (moneyVo.getSearchSelf() != null && moneyVo.getSearchSelf()) {
                BoolQueryBuilder bqb1 = QueryBuilders.boolQuery();
                bqb1.must(QueryBuilders.wildcardQuery("parents",  parents));
                bqb1.should(QueryBuilders.termQuery("accountId", moneyVo.getSelfId()));
                queryBuilder.must(bqb1);
            } else {
                queryBuilder.must(QueryBuilders.wildcardQuery( "parents",parents));
            }
        }

        if (StringUtil.isNotEmpty(moneyVo.getRemark())) {
            queryBuilder.must(QueryBuilders.wildcardQuery( "remark","*"+moneyVo.getRemark()+ "*"));
        }

        if (StringUtil.isNotEmpty(moneyVo.getBegin())) {
            queryBuilder.filter(QueryBuilders.rangeQuery("createDatetime").gte( moneyVo.getBegin().getTime()));
        }
        if (StringUtil.isNotEmpty(moneyVo.getType()) && moneyVo.getType() != 0) {
            queryBuilder.must(QueryBuilders.matchQuery("moneyType", moneyVo.getType()));
        }
        if (StringUtil.isNotEmpty(moneyVo.getEnd())) {
            queryBuilder.filter(QueryBuilders.rangeQuery("createDatetime").lte( moneyVo.getEnd().getTime()));
        }
        if (StringUtil.isNotEmpty(moneyVo.getOrderId())) {
            queryBuilder.must(QueryBuilders.matchQuery("orderId", moneyVo.getOrderId()));
        }
        SearchOperation sO = new SearchOperation(EsEnum.MNY_MONEY_RECORD.getIndexTag(),EsEnum.MNY_MONEY_RECORD.getType(),queryBuilder, MnyMoneyRecord.class );

        sO.setFrom((webParam.getPageNo()-1)*webParam.getPageSize()).setSize(webParam.getPageSize());
        sO.setSort(new XSort(XSort.Direction.DESC,"createDatetime"));
        page  = ElasticApi.searchForPageMap(sO,Map.class);
        return page;
    }


}
