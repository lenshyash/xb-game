package com.game.elasticsearch;


import com.google.common.collect.ImmutableList;
import org.apache.http.HttpHost;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.cluster.node.tasks.list.ListTasksResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.DeleteStoredScriptRequest;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptRequest;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.PutStoredScriptRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.explain.ExplainRequest;
import org.elasticsearch.action.explain.ExplainResponse;
import org.elasticsearch.action.fieldcaps.FieldCapabilitiesRequest;
import org.elasticsearch.action.fieldcaps.FieldCapabilitiesResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.core.*;
import org.elasticsearch.client.tasks.TaskSubmissionResponse;
import org.elasticsearch.common.settings.Setting;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.rankeval.RankEvalRequest;
import org.elasticsearch.index.rankeval.RankEvalResponse;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.ReindexRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.mustache.MultiSearchTemplateRequest;
import org.elasticsearch.script.mustache.MultiSearchTemplateResponse;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.script.mustache.SearchTemplateResponse;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

public class Client {


    private RestHighLevelClient _client;
    private Setting<List<URI>> HOSTS_SETTING =
            Setting.listSetting("es.hosts", ImmutableList.of(), URI::create);
    private Settings settings;
    public Client(Settings settings) {
        this(settings, null);
    }

    public Client(Settings settings, RestHighLevelClient restHighLevelClient) {
        this.settings = settings;
        this._client = restHighLevelClient == null ? buildUnderlyingClient() : restHighLevelClient;
    }

    public RestHighLevelClient getUnderlyingClient() {
        return this._client;
    }

    private RestHighLevelClient buildUnderlyingClient() {
        List<HttpHost> hosts = HOSTS_SETTING.get(settings)
                .stream().map(uri -> new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme())).collect(Collectors.toList());
        return new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[]{})));
    }

    public RestClient getLowLevelClient() {
        return _client.getLowLevelClient();
    }

    public void close() throws IOException {
        _client.close();
    }

    public IndicesClient indices() {
        return _client.indices();
    }

    public ClusterClient cluster() {
        return _client.cluster();
    }

    public IngestClient ingest() {
        return _client.ingest();
    }

    public SnapshotClient snapshot() {
        return _client.snapshot();
    }

    public RollupClient rollup() {
        return _client.rollup();
    }

    public CcrClient ccr() {
        return _client.ccr();
    }

    public TasksClient tasks() {
        return _client.tasks();
    }

    public XPackClient xpack() {
        return _client.xpack();
    }

    public WatcherClient watcher() {
        return _client.watcher();
    }

    public GraphClient graph() {
        return _client.graph();
    }

    public LicenseClient license() {
        return _client.license();
    }

    public IndexLifecycleClient indexLifecycle() {
        return _client.indexLifecycle();
    }

    public MigrationClient migration() {
        return _client.migration();
    }

    public MachineLearningClient machineLearning() {
        return _client.machineLearning();
    }

    public SecurityClient security() {
        return _client.security();
    }

    public BulkResponse bulk(BulkRequest bulkRequest, RequestOptions options) throws IOException {
        return _client.bulk(bulkRequest, options);
    }

    public void bulkAsync(BulkRequest bulkRequest, RequestOptions options, ActionListener<BulkResponse> listener) {
        _client.bulkAsync(bulkRequest, options, listener);
    }


    public BulkByScrollResponse reindex(ReindexRequest reindexRequest, RequestOptions options) throws IOException {
        return _client.reindex(reindexRequest, options);
    }

    public TaskSubmissionResponse submitReindexTask(ReindexRequest reindexRequest, RequestOptions options) throws IOException {
        return _client.submitReindexTask(reindexRequest, options);
    }

    public void reindexAsync(ReindexRequest reindexRequest, RequestOptions options, ActionListener<BulkByScrollResponse> listener) {
        _client.reindexAsync(reindexRequest, options, listener);
    }

    public BulkByScrollResponse updateByQuery(UpdateByQueryRequest updateByQueryRequest, RequestOptions options) throws IOException {
        return _client.updateByQuery(updateByQueryRequest, options);
    }

    public void updateByQueryAsync(UpdateByQueryRequest updateByQueryRequest, RequestOptions options, ActionListener<BulkByScrollResponse> listener) {
        _client.updateByQueryAsync(updateByQueryRequest, options, listener);
    }

    public BulkByScrollResponse deleteByQuery(DeleteByQueryRequest deleteByQueryRequest, RequestOptions options) throws IOException {
        return _client.deleteByQuery(deleteByQueryRequest, options);
    }

    public void deleteByQueryAsync(DeleteByQueryRequest deleteByQueryRequest, RequestOptions options, ActionListener<BulkByScrollResponse> listener) {
        _client.deleteByQueryAsync(deleteByQueryRequest, options, listener);
    }

    public ListTasksResponse deleteByQueryRethrottle(RethrottleRequest rethrottleRequest, RequestOptions options) throws IOException {
        return _client.deleteByQueryRethrottle(rethrottleRequest, options);
    }

    public void deleteByQueryRethrottleAsync(RethrottleRequest rethrottleRequest, RequestOptions options, ActionListener<ListTasksResponse> listener) {
        _client.deleteByQueryRethrottleAsync(rethrottleRequest, options, listener);
    }

    public ListTasksResponse updateByQueryRethrottle(RethrottleRequest rethrottleRequest, RequestOptions options) throws IOException {
        return _client.updateByQueryRethrottle(rethrottleRequest, options);
    }

    public void updateByQueryRethrottleAsync(RethrottleRequest rethrottleRequest, RequestOptions options, ActionListener<ListTasksResponse> listener) {
        _client.updateByQueryRethrottleAsync(rethrottleRequest, options, listener);
    }

    public ListTasksResponse reindexRethrottle(RethrottleRequest rethrottleRequest, RequestOptions options) throws IOException {
        return _client.reindexRethrottle(rethrottleRequest, options);
    }

    public void reindexRethrottleAsync(RethrottleRequest rethrottleRequest, RequestOptions options, ActionListener<ListTasksResponse> listener) {
        _client.reindexRethrottleAsync(rethrottleRequest, options, listener);
    }

    public boolean ping(RequestOptions options) throws IOException {
        return _client.ping(options);
    }

    public MainResponse info(RequestOptions options) throws IOException {
        return _client.info(options);
    }

    public GetResponse get(GetRequest getRequest, RequestOptions options) throws IOException {
        return _client.get(getRequest, options);
    }

    public void getAsync(GetRequest getRequest, RequestOptions options, ActionListener<GetResponse> listener) {
        _client.getAsync(getRequest, options, listener);
    }

    public MultiGetResponse mget(MultiGetRequest multiGetRequest, RequestOptions options) throws IOException {
        return _client.mget(multiGetRequest, options);
    }

    public void mgetAsync(MultiGetRequest multiGetRequest, RequestOptions options, ActionListener<MultiGetResponse> listener) {
        _client.mgetAsync(multiGetRequest, options, listener);
    }

    public boolean exists(GetRequest getRequest, RequestOptions options) throws IOException {
        return _client.exists(getRequest, options);
    }

    public void existsAsync(GetRequest getRequest, RequestOptions options, ActionListener<Boolean> listener) {
        _client.existsAsync(getRequest, options, listener);
    }

    public boolean existsSource(GetRequest getRequest, RequestOptions options) throws IOException {
        return _client.existsSource(getRequest, options);
    }

    public void existsSourceAsync(GetRequest getRequest, RequestOptions options, ActionListener<Boolean> listener) {
        _client.existsSourceAsync(getRequest, options, listener);
    }

    public IndexResponse index(IndexRequest indexRequest, RequestOptions options) throws IOException {
        return _client.index(indexRequest, options);
    }

    public void indexAsync(IndexRequest indexRequest, RequestOptions options, ActionListener<IndexResponse> listener) {
        _client.indexAsync(indexRequest, options, listener);
    }

    public CountResponse count(CountRequest countRequest, RequestOptions options) throws IOException {
        return _client.count(countRequest, options);
    }

    public void countAsync(CountRequest countRequest, RequestOptions options, ActionListener<CountResponse> listener) {
        _client.countAsync(countRequest, options, listener);
    }

    public UpdateResponse update(UpdateRequest updateRequest, RequestOptions options) throws IOException {
        return _client.update(updateRequest, options);
    }

    public void updateAsync(UpdateRequest updateRequest, RequestOptions options, ActionListener<UpdateResponse> listener) {
        _client.updateAsync(updateRequest, options, listener);
    }

    public DeleteResponse delete(DeleteRequest deleteRequest, RequestOptions options) throws IOException {
        return _client.delete(deleteRequest, options);
    }

    public void deleteAsync(DeleteRequest deleteRequest, RequestOptions options, ActionListener<DeleteResponse> listener) {
        _client.deleteAsync(deleteRequest, options, listener);
    }

    public SearchResponse search(SearchRequest searchRequest, RequestOptions options) throws IOException {
        return _client.search(searchRequest, options);
    }

    public void searchAsync(SearchRequest searchRequest, RequestOptions options, ActionListener<SearchResponse> listener) {
        _client.searchAsync(searchRequest, options, listener);
    }

    public MultiSearchResponse msearch(MultiSearchRequest multiSearchRequest, RequestOptions options) throws IOException {
        return _client.msearch(multiSearchRequest, options);
    }

    public void msearchAsync(MultiSearchRequest searchRequest, RequestOptions options, ActionListener<MultiSearchResponse> listener) {
        _client.msearchAsync(searchRequest, options, listener);
    }

    public SearchResponse scroll(SearchScrollRequest searchScrollRequest, RequestOptions options) throws IOException {
        return _client.scroll(searchScrollRequest, options);
    }

    public void scrollAsync(SearchScrollRequest searchScrollRequest, RequestOptions options, ActionListener<SearchResponse> listener) {
        _client.scrollAsync(searchScrollRequest, options, listener);
    }

    public ClearScrollResponse clearScroll(ClearScrollRequest clearScrollRequest, RequestOptions options) throws IOException {
        return _client.clearScroll(clearScrollRequest, options);
    }

    public void clearScrollAsync(ClearScrollRequest clearScrollRequest, RequestOptions options, ActionListener<ClearScrollResponse> listener) {
        _client.clearScrollAsync(clearScrollRequest, options, listener);
    }

    public SearchTemplateResponse searchTemplate(SearchTemplateRequest searchTemplateRequest, RequestOptions options) throws IOException {
        return _client.searchTemplate(searchTemplateRequest, options);
    }

    public void searchTemplateAsync(SearchTemplateRequest searchTemplateRequest, RequestOptions options, ActionListener<SearchTemplateResponse> listener) {
        _client.searchTemplateAsync(searchTemplateRequest, options, listener);
    }

    public ExplainResponse explain(ExplainRequest explainRequest, RequestOptions options) throws IOException {
        return _client.explain(explainRequest, options);
    }

    public void explainAsync(ExplainRequest explainRequest, RequestOptions options, ActionListener<ExplainResponse> listener) {
        _client.explainAsync(explainRequest, options, listener);
    }

    public TermVectorsResponse termvectors(TermVectorsRequest request, RequestOptions options) throws IOException {
        return _client.termvectors(request, options);
    }

    public void termvectorsAsync(TermVectorsRequest request, RequestOptions options, ActionListener<TermVectorsResponse> listener) {
        _client.termvectorsAsync(request, options, listener);
    }

    public MultiTermVectorsResponse mtermvectors(MultiTermVectorsRequest request, RequestOptions options) throws IOException {
        return _client.mtermvectors(request, options);
    }

    public void mtermvectorsAsync(MultiTermVectorsRequest request, RequestOptions options, ActionListener<MultiTermVectorsResponse> listener) {
        _client.mtermvectorsAsync(request, options, listener);
    }

    public RankEvalResponse rankEval(RankEvalRequest rankEvalRequest, RequestOptions options) throws IOException {
        return _client.rankEval(rankEvalRequest, options);
    }

    public void rankEvalAsync(RankEvalRequest rankEvalRequest, RequestOptions options, ActionListener<RankEvalResponse> listener) {
        _client.rankEvalAsync(rankEvalRequest, options, listener);
    }

    public MultiSearchTemplateResponse msearchTemplate(MultiSearchTemplateRequest multiSearchTemplateRequest, RequestOptions options) throws IOException {
        return _client.msearchTemplate(multiSearchTemplateRequest, options);
    }

    public void msearchTemplateAsync(MultiSearchTemplateRequest multiSearchTemplateRequest, RequestOptions options, ActionListener<MultiSearchTemplateResponse> listener) {
        _client.msearchTemplateAsync(multiSearchTemplateRequest, options, listener);
    }

    public FieldCapabilitiesResponse fieldCaps(FieldCapabilitiesRequest fieldCapabilitiesRequest, RequestOptions options) throws IOException {
        return _client.fieldCaps(fieldCapabilitiesRequest, options);
    }

    public GetStoredScriptResponse getScript(GetStoredScriptRequest request, RequestOptions options) throws IOException {
        return _client.getScript(request, options);
    }

    public void getScriptAsync(GetStoredScriptRequest request, RequestOptions options, ActionListener<GetStoredScriptResponse> listener) {
        _client.getScriptAsync(request, options, listener);
    }

    public AcknowledgedResponse deleteScript(DeleteStoredScriptRequest request, RequestOptions options) throws IOException {
        return _client.deleteScript(request, options);
    }

    public void deleteScriptAsync(DeleteStoredScriptRequest request, RequestOptions options, ActionListener<AcknowledgedResponse> listener) {
        _client.deleteScriptAsync(request, options, listener);
    }

    public AcknowledgedResponse putScript(PutStoredScriptRequest putStoredScriptRequest, RequestOptions options) throws IOException {
        return _client.putScript(putStoredScriptRequest, options);
    }

    public void putScriptAsync(PutStoredScriptRequest putStoredScriptRequest, RequestOptions options, ActionListener<AcknowledgedResponse> listener) {
        _client.putScriptAsync(putStoredScriptRequest, options, listener);
    }

    public void fieldCapsAsync(FieldCapabilitiesRequest fieldCapabilitiesRequest, RequestOptions options, ActionListener<FieldCapabilitiesResponse> listener) {
        _client.fieldCapsAsync(fieldCapabilitiesRequest, options, listener);
    }
}
