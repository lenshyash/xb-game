package com.game.elasticsearch;

import com.game.common.data.Page;
import com.game.elasticsearch.operations.*;
import org.elasticsearch.script.Script;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ReactiveElasticsearchOperations {

    /**
     * @return
     */
    Client getClient();

    /**
     * @param indexOperation
     * @return
     */
    Mono<String> index(IndexOperation<?> indexOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     * @return
     */
    <T> Mono<String> index(String index, String type, String id, T source);

    /**
     * @param indexOps
     * @return
     */
    Mono<Void> bulkIndex(List<IndexOperation<?>> indexOps);

    /**
     * @param index
     * @param type
     * @param id
     * @param routing
     * @return
     */
    Mono<String> delete(String index, String type, String id, String routing);

    /**
     * @param deleteByQueryOperation
     * @return
     */
    Mono<Void> deleteByQuery(DeleteByQueryOperation deleteByQueryOperation);

    /**
     * @param getOperation
     * @param <T>
     * @return
     */
    <T> Mono<T> get(GetOperation<T> getOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param sourceClass
     * @param <T>
     * @return
     */
    <T> Mono<T>
    get(final String index, final String type, final String id, Class<T> sourceClass);

    /**
     * @param multiGetOperation
     * @param <T>
     * @return
     */
    <T> Flux<T> multiGet(MultiGetOperation<T> multiGetOperation);

    /**
     * @param updateOperation
     * @return
     */
    Mono<Void> update(UpdateOperation<?> updateOperation);

    /**
     * @param index
     * @param type
     * @param id
     * @param source
     * @param <T>
     * @return
     */
    <T> Mono<Void> update(String index, String type, String id, T source);

    /**
     * @param index
     * @param type
     * @param id
     * @param script
     * @return
     */
    Mono<Void> update(String index, String type, String id, Script script);

    /**
     * @param updateOps
     * @return
     */
    Mono<Void> bulkUpdate(List<UpdateOperation<?>> updateOps);

    /**
     * @param updateByQueryOperation
     * @return
     */
    Mono<Void> updateByQuery(UpdateByQueryOperation updateByQueryOperation);

    /**
     * @param searchOperation
     * @param <T>
     * @return
     */
    <T> Mono<T> searchOne(SearchOperation<T> searchOperation);

    /**
     * @param searchOperation
     * @param <T>
     * @return
     */
    <T> Flux<T> search(SearchOperation<T> searchOperation);

    /**
     * @param searchOperation
     * @param <T>
     * @return
     */
    <T> Mono<Page<T>> searchForPage(SearchOperation<T> searchOperation);

    /**
     *
     * @param searchOperation
     * @param <T>
     * @return
     */
    <T> Mono<Long> count(SearchOperation<T> searchOperation);

    /**
     * @param searchOperation
     * @param scrollTimeInMillis
     * @param <T>
     * @return
     */
    <T> Mono<ScrollPage<T>>
    startScroll(SearchOperation<T> searchOperation, long scrollTimeInMillis);

    /**
     * @param scrollId
     * @param scrollTimeInMillis
     * @param sourceClass
     * @param <T>
     * @return
     */
    <T> Mono<ScrollPage<T>>
    continueScroll(String scrollId, long scrollTimeInMillis, Class<T> sourceClass);

    /**
     * @param scrollId
     * @return
     */
    Mono<Void> clearScroll(String scrollId);
}
