package com.game.elasticsearch;

import com.game.common.data.Page;
import com.game.elasticsearch.operations.*;
import org.elasticsearch.script.Script;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public class ReactiveElasticsearchTemplate implements ReactiveElasticsearchOperations {

    private ElasticsearchTemplate elasticsearchTemplate;

    public ReactiveElasticsearchTemplate(Client client) {
        this.elasticsearchTemplate = new ElasticsearchTemplate(client);
    }

    public ReactiveElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Client getClient() {
        return elasticsearchTemplate.getClient();
    }

    @Override
    public Mono<String> index(IndexOperation<?> indexOperation) {
        return Mono.fromFuture(elasticsearchTemplate.indexAsync(indexOperation));
    }

    @Override
    public <T> Mono<String> index(String index, String type, String id, T source) {
        return index(new IndexOperation<>(index, type, id).setSource(source));
    }

    @Override
    public Mono<Void> bulkIndex(List<IndexOperation<?>> indexOps) {
        return Mono.fromFuture(elasticsearchTemplate.bulkIndexAsync(indexOps));
    }

    @Override
    public Mono<String> delete(String index, String type, String id, String routing) {
        return Mono.fromFuture(elasticsearchTemplate.deleteAsync(index, type, id, routing));
    }

    @Override
    public Mono<Void> deleteByQuery(DeleteByQueryOperation deleteByQueryOperation) {
        return Mono.fromFuture(elasticsearchTemplate.deleteByQueryAsync(deleteByQueryOperation));
    }

    @Override
    public <T> Mono<T> get(GetOperation<T> getOperation) {
        return Mono.fromFuture(elasticsearchTemplate.getAsync(getOperation));
    }

    @Override
    public <T> Mono<T> get(String index, String type, String id, Class<T> sourceClass) {
        return Mono.fromFuture(elasticsearchTemplate.getAsync(index, type, id, sourceClass));
    }

    @Override
    public <T> Flux<T> multiGet(MultiGetOperation<T> multiGetOperation) {
        return Mono.fromFuture(elasticsearchTemplate.multiGetAsync(multiGetOperation)).flatMapMany(Flux::fromIterable);
    }

    @Override
    public Mono<Void> update(UpdateOperation<?> updateOperation) {
        return Mono.fromFuture(elasticsearchTemplate.updateAsync(updateOperation));
    }

    @Override
    public <T> Mono<Void> update(String index, String type, String id, T source) {
        return update(new UpdateOperation<>(index, type, id).setSource(source));
    }

    @Override
    public Mono<Void> update(String index, String type, String id, Script script) {
        return update(new UpdateOperation<>(index, type, id).setScript(script));
    }

    @Override
    public Mono<Void> bulkUpdate(List<UpdateOperation<?>> updateOps) {
        return Mono.fromFuture(elasticsearchTemplate.bulkUpdateAsync(updateOps));
    }

    @Override
    public Mono<Void> updateByQuery(UpdateByQueryOperation updateByQueryOperation) {
        return Mono.fromFuture(elasticsearchTemplate.updateByQueryAsync(updateByQueryOperation));
    }

    @Override
    public <T> Mono<T> searchOne(SearchOperation<T> searchOperation) {
        return Mono.fromFuture(elasticsearchTemplate.searchOneAsync(searchOperation));
    }

    @Override
    public <T> Flux<T> search(SearchOperation<T> searchOperation) {
        return Mono.fromFuture(elasticsearchTemplate.searchAsync(searchOperation)).flatMapMany(Flux::fromIterable);
    }

    @Override
    public <T> Mono<Page<T>> searchForPage(SearchOperation<T> searchOperation) {
        return Mono.fromFuture(elasticsearchTemplate.searchForPageAsync(searchOperation));
    }

    @Override
    public <T> Mono<Long> count(SearchOperation<T> searchOperation) {
        return Mono.fromFuture(elasticsearchTemplate.countAsync(searchOperation));
    }

    @Override
    public <T> Mono<ScrollPage<T>>
    startScroll(SearchOperation<T> searchOperation, long scrollTimeInMillis) {
        return Mono.fromFuture(elasticsearchTemplate.startScrollAsync(searchOperation, scrollTimeInMillis));
    }

    @Override
    public <T> Mono<ScrollPage<T>>
    continueScroll(String scrollId, long scrollTimeInMillis, Class<T> sourceClass) {
        return Mono.fromFuture(elasticsearchTemplate.continueScrollAsync(scrollId, scrollTimeInMillis, sourceClass));
    }

    @Override
    public Mono<Void> clearScroll(String scrollId) {
        return Mono.fromFuture(elasticsearchTemplate.clearScrollAsync(scrollId));
    }

}
