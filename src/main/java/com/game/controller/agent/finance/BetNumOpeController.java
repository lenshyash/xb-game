package com.game.controller.agent.finance;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.service.SysAccountService;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/betnumope")
public class BetNumOpeController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		return super.goPage("/page/finance/betnumope.jsp");
	}

	@ResponseBody
	@RequestMapping("/meminfo")
	public void list(@RequestParam(value="account") String account) {
		super.renderJson(accountService.findOneByAccountAndStationId(account, StationUtil.getStationId()));
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/save")
	public void save() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long accountId = super.$cl("id");
		Long type = super.$cl("type");
		String betNum = super.$c("betNum");
		String remark = super.$("remark");
		accountService.accountDrawNeedOpe(accountId, StringUtil.toBigDecimal(betNum), type, remark);
		super.renderSuccess();
	}
}
