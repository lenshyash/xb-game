package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MnyComStrategy;
import com.game.service.AgentBaseConfigService;
import com.game.service.MemberLevelService;
import com.game.service.MnyComStrategyLevelService;
import com.game.service.MnyComStrategyService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

//新的存款策略
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/mnyComStrategy")
public class MnyComStrategyController extends BaseAgentController {
	@Autowired
	private MnyComStrategyService mnyComStrategyService;
	@Autowired
	private MnyComStrategyLevelService mnyComStrategyLevelService;
	@Autowired
	private AgentBaseConfigService baseConfigService;
	@Autowired
	private MemberLevelService memberLevelService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		map.put("sysApiDeposit",
				baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), StationUtil.getStationId()));
		return super.goPage("/page/finance/mnyComStrategy.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Integer depositType, Integer giftType, Integer valueType, String begin, String end) {
		super.render(mnyComStrategyService.getPage(depositType, giftType, valueType, DateUtil.toDate(begin),
				DateUtil.getTomorrow(end), StationUtil.getStationId()));
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		Long stationId = StationUtil.getStationId();
		map.put("sysApiDeposit", baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), stationId));
		map.put("levelList", memberLevelService.getStationLevels(stationId));
		Calendar c = Calendar.getInstance();
		map.put("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.toDateStr(c.getTime()));
		return super.prepareLayout("/newpage/finance/mnyComStrategyAdd.jsp");
	}

	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave(MnyComStrategy com, BigDecimal rollBackRate, Long[] groupLevelId, String startDate,
			String endDate) {
		if (com.getGiftType() == MnyComStrategy.GIFT_TYPE_PERCENT) {
			com.setGiftValue(rollBackRate);
		}
		mnyComStrategyService.addSave(com, groupLevelId, startDate, endDate, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		Long stationId = StationUtil.getStationId();
		map.put("sysApiDeposit", baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), stationId));
		map.put("levelList", memberLevelService.getStationLevels(stationId));
		map.put("com", mnyComStrategyService.getOne(id, stationId));
		map.put("levelSet", mnyComStrategyLevelService.getLevelSet(id, stationId));
		return super.prepareLayout("/newpage/finance/mnyComStrategyModify.jsp");
	}

	@RequestMapping("/update")
	@ResponseBody
	public void update(MnyComStrategy com, BigDecimal rollBackRate, Long[] groupLevelId, String startDate,
			String endDate) {
		if (com.getGiftType() == MnyComStrategy.GIFT_TYPE_PERCENT) {
			com.setGiftValue(rollBackRate);
		}
		mnyComStrategyService.update(com, groupLevelId, startDate, endDate,StationUtil.getStationId());
		super.renderSuccess();
	}

	/**
	 * 租户公告状态修改
	 */
	@RequestMapping("/updStatus")
	@ResponseBody
	public void updStatus(Integer status, Long id) {
		mnyComStrategyService.updStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		mnyComStrategyService.delete(id, stationId);
		super.renderSuccess();
	}
}
