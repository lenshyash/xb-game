package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentDepositBank;
import com.game.model.AgentDepositFast;
import com.game.model.MnyComRecord;
import com.game.model.SysAccount;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.PayPlatformVo;
import com.game.permission.PermissionManager;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.permission.annotation.SortMapping;
import com.game.service.AgentDepositBankService;
import com.game.service.AgentDepositFastService;
import com.game.service.MnyComRecordService;
import com.game.service.SysPayPlatformService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/payonlinerd")
public class PayOnlineRdController extends BaseAgentController {

	@Autowired
	private MnyComRecordService mcrService;
	@Autowired
	private SysPayPlatformService payPlatformService;
	@Autowired
	private AgentDepositBankService depositBankService;
	@Autowired
	private AgentDepositFastService depositFastService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		String begin = request.getParameter("begin");
		String end = request.getParameter("end");
		super.getRequest().setAttribute("startTime", begin == null ? curDate : begin);
		super.getRequest().setAttribute("endTime", end == null ? curDate : end);
		super.getRequest().setAttribute("saccount", request.getParameter("account"));
		
		super.getRequest().setAttribute("operator", request.getParameter("operator"));
		super.getRequest().setAttribute("minMoney", request.getParameter("minMoney"));
		super.getRequest().setAttribute("maxMoney", request.getParameter("maxMoney"));
		super.getRequest().setAttribute("remark", request.getParameter("remark"));
		
		super.getRequest().setAttribute("handlerType", request.getParameter("handlerType"));
		super.getRequest().setAttribute("isCancle", StationConfigUtil.get(StationConfig.deposit_handler_cancle));
		super.getRequest().setAttribute("exportPerm",
				PermissionManager.getAgentPermission().contains("/agent/finance/payonlinerd/export.do"));
		super.getRequest().setAttribute("rechargePerm",
				PermissionManager.getAgentPermission().contains("/agent/finance/payonlinerd/lock.do"));
		super.getRequest().setAttribute("onoffcopy", StationConfigUtil.get(StationConfig.agent_copy_info_botton));
		super.getRequest().setAttribute("straightFailed", StationConfigUtil.get(StationConfig.on_off_payonlinerd_straight_failed));
		super.getRequest().setAttribute("warnFlag","off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning)));
		return super.goPage("/page/finance/payonlinerd.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping={"createDatetime","create_datetime","money","r.money"})
	public void list() {
		Long type = super.$long("type");
		String payName = super.$("payName");
		Long status = super.$long("status");
		Long handlerType = super.$long("handlerType");
		String account = super.$("account");
		String orderNo = super.$("orderNo");
		String agentName = super.$("agentName");
		String begin = super.$("begin");
		String end = super.$("end");
		
		String operator = super.$("operator");
		String minMoney = super.$("minMoney");
		String maxMoney = super.$("maxMoney");
		String opDesc = super.$("opDesc");
		String remark = super.$("remark");

		Long reportType = super.$long("reportType");
		Long accountType = super.$long("accountType");
		Long comTimes = super.$long("comTimes");
		
		Date beginDate = DateUtil.toDatetime(begin);
		Date endDate = DateUtil.toDatetime(end);

		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setType(type);
		mcrVo.setAccount(account);
		mcrVo.setPaySmallName(payName);
		mcrVo.setStatus(status);
		mcrVo.setBegin(beginDate);
		mcrVo.setEnd(endDate);
		mcrVo.setHandlerType(handlerType);
		mcrVo.setOrderNo(orderNo);
		mcrVo.setAgentName(agentName);
		mcrVo.setReportType(reportType);
		mcrVo.setAccountType(accountType);
		mcrVo.setOpDesc(opDesc);
		mcrVo.setRemark(remark);
		mcrVo.setMaxMoney(StringUtil.toBigDecimal(maxMoney));
		mcrVo.setMinMoney(StringUtil.toBigDecimal(minMoney));
		mcrVo.setOperator(operator);
		mcrVo.setComTimes(comTimes);
		
		super.render(mcrService.getPage(mcrVo));
	}

	/**
	 * 导出功能
	 */
	@CheckLastLoginIp
	@Permission(CheckType.FUNCTION)
	@ResponseBody
	@RequestMapping("/export")
	public void export() {
		Long type = super.$long("type");
		String payName = super.$("payName");
		Long status = super.$long("status");
		String account = super.$("account");

		String begin = super.$("begin");
		String end = super.$("end");
		String opDesc =StringUtils.isEmpty(super.$("opDesc")) ? "": super.$("opDesc");
		Date beginDate = DateUtil.toDate(begin);
		Date endDate = DateUtil.getTomorrow(end);

		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setType(type);
		mcrVo.setAccount(account);
		mcrVo.setPaySmallName(payName);
		mcrVo.setStatus(status);
		mcrVo.setBegin(beginDate);
		mcrVo.setEnd(endDate);
		mcrVo.setOpDesc(opDesc);
		mcrService.export(mcrVo);
	}

	/**
	 * 支付类型种类
	 */
	@CheckLastLoginIp
	@RequestMapping("payType")
	@ResponseBody
	public void paySmallType() {
		String payName = super.$("payName");
		Long type = super.$long("type");
		PayPlatformVo ppvo = new PayPlatformVo();
		ppvo.setName(payName);
		ppvo.setType(type);

		super.renderJson(payPlatformService.getPaysCombo(ppvo));
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/lock")
	public void lock() {

		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long comId = super.$cl("id");
		Long lockFlag = super.$cl("lockFlag");
		mcrService.lock(comId, lockFlag);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/doStraightFailed")
	public void doStraightFailed(Long id, String opDesc) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		
		//先锁定
		Long comId = super.$cl("id");
		Long lockFlag = super.$cl("lockFlag");
		mcrService.lock(comId, lockFlag);
		
		//在直接处理失败
		MnyComRecordVo cvrVo = new MnyComRecordVo();
		cvrVo.setStatus(MnyComRecord.STATUS_FAILED);
		cvrVo.setId(comId);
		cvrVo.setOpDesc(opDesc);
		mcrService.comHandler(cvrVo);

		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/straightFailed")
	public String straightFailed(Map<String, Object> map, Long id) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		map.put("com", com);
		if (com.getType() == 7) {// 公司转账
			AgentDepositBank b = depositBankService.getOne(com.getPayId(), com.getStationId());
			if (b != null) {
				String bankCard = b.getBankCard();
				if (StringUtils.length(bankCard) > 4) {
					bankCard = "***" + bankCard.substring(bankCard.length() - 4);
				}
				map.put("card", bankCard);
				map.put("name", b.getCreatorName());
			}
		} else if (com.getType() == 6) {// 快速入款
			AgentDepositFast f = depositFastService.getOne(com.getPayId(), com.getStationId());
			if (f != null) {
				String pa = f.getPayAccount();
				if (StringUtils.length(pa) > 4) {
					pa = "***" + pa.substring(pa.length() - 4);
				}
				map.put("card", pa);
				map.put("name", f.getPayUserName());
			}
		}
		return super.prepareLayout("/newpage/finance/straight_failed.jsp");
	}
	
	@CheckLastLoginIp
	@RequestMapping("/payonlinerdFailed")
	public String payonlinerdFailed(Map<String, Object> map, Long id) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		map.put("com", com);
		if (com.getType() == 7) {// 公司转账
			AgentDepositBank b = depositBankService.getOne(com.getPayId(), com.getStationId());
			if (b != null) {
				String bankCard = b.getBankCard();
				if (StringUtils.length(bankCard) > 4) {
					bankCard = "***" + bankCard.substring(bankCard.length() - 4);
				}
				map.put("card", bankCard);
				map.put("name", b.getCreatorName());
			}
		} else if (com.getType() == 6) {// 快速入款
			AgentDepositFast f = depositFastService.getOne(com.getPayId(), com.getStationId());
			if (f != null) {
				String pa = f.getPayAccount();
				if (StringUtils.length(pa) > 4) {
					pa = "***" + pa.substring(pa.length() - 4);
				}
				map.put("card", pa);
				map.put("name", f.getPayUserName());
			}
		}
		return super.prepareLayout("/newpage/finance/payonlinerd_failed.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/reback")
	public void reback() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long comId = super.$cl("id");
//		Long lockFlag = super.$cl("lockFlag");
		mcrService.reback(comId);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/confirm")
	public void confirm() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long comId = super.$cl("id");
		String fee = super.$("fee");
		String opDesc = super.$("opDesc");
		BigDecimal money = BigDecimalUtil.toBigDecimal(super.$("money"));
		MnyComRecordVo cvrVo = new MnyComRecordVo();
		cvrVo.setStatus(MnyComRecord.STATUS_SUCCESS);
		cvrVo.setFee(StringUtil.toBigDecimal(fee));
		cvrVo.setId(comId);
		cvrVo.setOpDesc(opDesc);
		cvrVo.setMoney(money);
		mcrService.comHandler(cvrVo);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/cancle")
	public String cancle(Map<String, Object> map, Long id) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		map.put("com", com);
		return super.prepareLayout("/newpage/finance/payonlinerd_cancle.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/doCancle")
	public void doCancle(Long id, String opDesc) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		MnyComRecordVo cvrVo = new MnyComRecordVo();
		cvrVo.setId(id);
		cvrVo.setOpDesc(opDesc);
		mcrService.cancelHandler(cvrVo);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/failed")
	public void failed() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long comId = super.$cl("id");
		String opDesc = super.$("opDesc");
		MnyComRecordVo cvrVo = new MnyComRecordVo();
		cvrVo.setStatus(MnyComRecord.STATUS_FAILED);
		cvrVo.setId(comId);
		cvrVo.setOpDesc(opDesc);
		mcrService.comHandler(cvrVo);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/handler")
	public String handler(Map<String, Object> map, Long id) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		map.put("com", com);
		if (com.getType() == 7) {// 公司转账
			AgentDepositBank b = depositBankService.getOne(com.getPayId(), com.getStationId());
			if (b != null) {
				String bankCard = b.getBankCard();
				if (StringUtils.length(bankCard) > 4) {
					bankCard = "***" + bankCard.substring(bankCard.length() - 4);
				}
				map.put("card", bankCard);
				map.put("name", b.getCreatorName());
			}
		} else if (com.getType() == 6) {// 快速入款
			AgentDepositFast f = depositFastService.getOne(com.getPayId(), com.getStationId());
			if (f != null) {
				String pa = f.getPayAccount();
				if (StringUtils.length(pa) > 4) {
					pa = "***" + pa.substring(pa.length() - 4);
				}
				map.put("card", pa);
				map.put("name", f.getPayUserName());
			}
		}
		map.put("straightFailed", StationConfigUtil.get(StationConfig.on_off_payonlinerd_straight_failed));
		map.put("onCopy", "on".equals(StationConfigUtil.get(StationConfig.agent_copy_info_botton)));
		return super.prepareLayout("/newpage/finance/payonlinerd_handler.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/doHandler")
	public void doHandler(Long id, Integer status, String opDesc, BigDecimal money, BigDecimal fee,BigDecimal rate,BigDecimal virtualCurrencyAmount) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		MnyComRecordVo cvrVo = new MnyComRecordVo();
		if (status != null && status == 1) {
			cvrVo.setStatus(MnyComRecord.STATUS_SUCCESS);
			cvrVo.setFee(fee);
			cvrVo.setMoney(money);
			cvrVo.setRate(rate);
			cvrVo.setVirtualCurrencyAmount(virtualCurrencyAmount);
		} else {
			cvrVo.setStatus(MnyComRecord.STATUS_FAILED);
		}
		cvrVo.setId(id);
		cvrVo.setOpDesc(opDesc);
		mcrService.comHandler(cvrVo);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/updateRemark")
	public String updateRemark(Map<String, Object> map, Long id) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		map.put("com", com);
		return super.prepareLayout("/newpage/finance/payonlinerd_update_remark.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/saveRemark")
	public void saveRemark(Long id,String remark) {
		MnyComRecord com = mcrService.getOne(id, StationUtil.getStationId());
		com.setRemark(remark);
		mcrService.saveRemark(id, remark);
		
		super.renderSuccess();
	}
}
