package com.game.controller.agent.finance;

import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentDepositBank;
import com.game.model.AgentDepositFast;
import com.game.model.AgentDepositLevel;
import com.game.model.AgentDepositOnline;
import com.game.model.AgentDepositVirtual;
import com.game.model.SysPayPlatform;
import com.game.model.vo.PayPlatformVo;
import com.game.service.AgentDepositBankService;
import com.game.service.AgentDepositFastService;
import com.game.service.AgentDepositLevelService;
import com.game.service.AgentDepositOnlineService;
import com.game.service.AgentDepositvirtualService;
import com.game.service.SysPayPlatformService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/depositset")
public class DepositSetController extends BaseAgentController {

	@Autowired
	private AgentDepositBankService bankService;

	@Autowired
	private AgentDepositFastService fastService;

	@Autowired
	private AgentDepositOnlineService onlineService;
	
	@Autowired
	private AgentDepositvirtualService virtualService;

	@Autowired
	private SysPayPlatformService payPlatformService;
	@Autowired
	private AgentDepositLevelService depositLevelService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(6L);
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		if (isNewPlatform()) {
			return super.prepareLayout("/newpage/finance/depositset/depositset.jsp");
		}
		map.put("levels", JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
		return super.goPage("/page/finance/depositset.jsp");
	}

	@ResponseBody
	@RequestMapping("/banklst")
	public void banklst() {
		super.render(bankService.getBankPage());
	}

	@ResponseBody
	@RequestMapping("/fastlst")
	public void fastlst() {
		Long payPlatformId = super.$long("payPlatformId");
		super.render(fastService.getFastPage(payPlatformId));
	}

	@ResponseBody
	@RequestMapping("/onlinelst")
	public void onlinelst() {
		String name = super.$("name");
		Long status = super.$long("status");
		super.render(onlineService.getOnlinePage(name,status));
	}
	
	@ResponseBody
	@RequestMapping("/virtuallst")
	public void virtuallst() {
		String name = super.$("name");
		Long status = super.$long("status");
		super.render(virtualService.getOnlinePage(name,status));
	}

	@RequestMapping("/addBank")
	public String addBank(Map<String, Object> map) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(7L);
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		return super.prepareLayout("/newpage/finance/depositset/depositset_bank_add.jsp");
	}

	@RequestMapping("/modifyBank")
	public String modifyBank(Map<String, Object> map, Long id) {
		map.put("bank", bankService.getOne(id, StationUtil.getStationId()));
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(7L);
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("glevelIds", depositLevelService.getLevelIdsByDepositId(id,  StationUtil.getStationId(), AgentDepositLevel.TYPE_BANK));
		return super.prepareLayout("/newpage/finance/depositset/depositset_bank_modify.jsp");
	}

	@ResponseBody
	@RequestMapping("/savebank")
	public void savebank(AgentDepositBank bank, Long[] groupLevelId) {
		bankService.save(bank, groupLevelId);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updBankStatus")
	public void updBankStatus(Map<String, Object> map, Integer status, Long id) {
		bankService.updateStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/addFast")
	public String addFast(Map<String, Object> map) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(6L);
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		return super.prepareLayout("/newpage/finance/depositset/depositset_fast_add.jsp");
	}

	@RequestMapping("/modifyFast")
	public String modifyFast(Map<String, Object> map, Long id) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(6L);
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("fast", fastService.getOne(id, StationUtil.getStationId()));
		map.put("glevelIds", depositLevelService.getLevelIdsByDepositId(id,  StationUtil.getStationId(), AgentDepositLevel.TYPE_FAST));
		return super.prepareLayout("/newpage/finance/depositset/depositset_fast_modify.jsp");
	}

	@ResponseBody
	@RequestMapping("/savefast")
	public void savefast(AgentDepositFast fast, Long[] groupLevelId) {
		fastService.save(fast,groupLevelId);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updFastStatus")
	public void updFastStatus(Map<String, Object> map, Integer status, Long id) {
		fastService.updateStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/addOnline")
	public String addOnline(Map<String, Object> map) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(5L);
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("levels1", MemberLevelUtil.getStationLevels());
		return super.prepareLayout("/newpage/finance/depositset/depositset_online_add.jsp");
	}
	
	@RequestMapping("/addVirtual")
	public String addVirtual(Map<String, Object> map) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(801L); 
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("levels1", MemberLevelUtil.getStationLevels());
		return super.prepareLayout("/newpage/finance/depositset/depositset_virtual_add.jsp");
	}
	
	@RequestMapping("/modifyVirtual")
	public String modifyVirtual(Map<String, Object> map, Long id) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(801L);
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("glevelIds", depositLevelService.getLevelIdsByDepositId(id,  StationUtil.getStationId(), AgentDepositLevel.TYPE_VIRTAUL));
		map.put("virtual",  virtualService.getOnline(id));
		return super.prepareLayout("/newpage/finance/depositset/depositset_virtual_modify.jsp");
	}
	
	@RequestMapping("/modifyOnline")
	public String modifyOnline(Map<String, Object> map, Long id) {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		ppVo.setType(5L);
		map.put("payCombos", payPlatformService.getPaysCombo(ppVo));
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("glevelIds", depositLevelService.getLevelIdsByDepositId(id,  StationUtil.getStationId(), AgentDepositLevel.TYPE_ONLINE));
		map.put("online",  onlineService.getOnline(id));
		return super.prepareLayout("/newpage/finance/depositset/depositset_online_modify.jsp");
	}

	@ResponseBody
	@RequestMapping("/saveonline")
	public void saveonline(AgentDepositOnline online, Long[] groupLevelId) {
		onlineService.save(online,groupLevelId);
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/savevirtual")
	public void savevirtual(AgentDepositVirtual virtual, Long[] groupLevelId) {
		virtualService.save(virtual, groupLevelId);
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/updOnlineStatus")
	public void updOnlineStatus(Map<String, Object> map, Integer status, Long id) {
		onlineService.updateStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/updVirtualStatus")
	public void updVirtualStatus(Map<String, Object> map, Integer status, Long id) {
		virtualService.updateStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delbank")
	public void delbank() {
		Long id = super.$cl("id");
		bankService.delBank(id);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delfast")
	public void delfast() {
		Long id = super.$cl("id");
		fastService.delFast(id);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delonline")
	public void delonline() {
		Long id = super.$cl("id");
		onlineService.delOnline(id);
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/delvirtual")
	public void delvirtual() {
		Long id = super.$cl("id");
		virtualService.delOnline(id);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/paycombo")
	public void paycombo() {
		PayPlatformVo ppVo = new PayPlatformVo();
		ppVo.setStatus(SysPayPlatform.STATUS_ENABLED);
		super.renderJson(payPlatformService.getPaysCombo(ppVo));
	}
}
