package com.game.controller.agent.finance;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.SysApiDepositService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/sysApiPayRd")
public class AysApiPayRdController extends BaseAgentController {
	@Autowired
	private SysApiDepositService sysApiDepositService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		String curDate = DateUtil.getCurrentDate();
		map.put("curDate", curDate);
		return super.goPage("/page/finance/sysApiPayRd.jsp");
	}
	@ResponseBody
	@RequestMapping("/list")
	public void list(String account, String begin, String end, String orderId) {
		Date beginDate = DateUtil.toDatetime(begin);
		if (beginDate == null) {
			beginDate = DateUtil.dayFirstTime(new Date(), 0);
		}
		Date endDate = DateUtil.toDatetime(end);
		if(endDate==null){
			endDate = DateUtil.dayFirstTime(new Date(), 1);
		}
		super.render(sysApiDepositService.getPage(account, beginDate, endDate, orderId, StationUtil.getStationId()));
	}
}
