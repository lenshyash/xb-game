package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MnyComStrategy;
import com.game.model.SysAccount;
import com.game.model.vo.MnyComRecordVo;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.service.BatchOptMoneyService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyComStrategyService;
import com.game.service.MnyMoneyService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/memmnyope")
public class MemMnyOpeController extends BaseAgentController {

	@Autowired
	private MnyComRecordService mnyComRecordService;
	@Autowired
	private BatchOptMoneyService batchOptMoneyService;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private MnyComStrategyService comStrategyService;

	@RequestMapping("/index")
	public String index() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		HttpServletRequest request = getRequest();
		request.setAttribute("consumeRate", StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.consume_rate)));
		request.setAttribute("strategies",
				comStrategyService.getByType(MnyComStrategy.TYPE_ARTIFICIAL, StationUtil.getStationId()));
		return super.goPage("/page/finance/memmnyope.jsp");
	}

	@ResponseBody
	@RequestMapping("/memmny")
	public void list(@RequestParam(value="account") String account) {
		super.renderJson(mnyMoneyService.getMoneyByAccount(account));
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/save")
	synchronized public void save() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long accountId = super.$cl("id");
		Long type = super.$cl("type");
		String money = super.$c("money");
		String remark = super.$("remark");
		String account = super.$("searchText");
		Long checkBetNum = super.$long("checkBetNum");
		String betNumMultiple = super.$("betNumMultiple");

		String key = "artificial_" + accountId + "_" + money;
		
		//站点配置时间间隔
		String interval = "30";
		String onoffInterval = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.station_artificial_interval);
		if(StringUtil.isNotEmpty(onoffInterval)) {
			interval = onoffInterval; 
		}
		
		if (RedisAPI.exists(key)) {
			throw new GenericException(interval+"秒内，不能为同一个用户添加同样金额");
		}else {
			RedisAPI.addCache(key, "1", StringUtil.toInt(interval));
		}
		
		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setAccountId(accountId);
		mcrVo.setAccount(account);
		mcrVo.setDepositDate(new Date());
		mcrVo.setType(type);
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setMoney(StringUtil.toBigDecimal(money));
		mcrVo.setCheckBetNum(checkBetNum);
		mcrVo.setRemark(remark);
		mcrVo.setBetNumMultiple(StringUtil.toBigDecimal(betNumMultiple));
		mnyComRecordService.artificial(mcrVo);
		super.renderSuccess();
	}
	
	@RequestMapping("/showBatchAdd")
	public String showBatchAdd() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		return super.goPage("/page/finance/memmnyBatchAdd.jsp");
	}

	@RequestMapping("/showBatchSub")
	public String showBatchSub() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		return super.goPage("/page/finance/memmnyBatchSub.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/batchAddMoney")
	synchronized public void batchAddMoney(String accounts,BigDecimal betNumMultiple, BigDecimal giftBetNumMultiple, String remark) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		HttpSession session = UserUtil.getSession();
		Long lastOpTime = (Long) session.getAttribute("batchAddMoney");
		if (lastOpTime != null && System.currentTimeMillis() - lastOpTime < 3000) {
			throw new GenericException("3秒内，不能重复此操作");
		}
		session.setAttribute("batchAddMoney", System.currentTimeMillis());
		try {
			batchOptMoneyService.batchAddMoney(accounts, betNumMultiple, giftBetNumMultiple, remark);
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(MixUtil.newHashMap("success", true, "msg", e.getMessage()));
			return;
		}
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("success", true);
		map.put("msg", "批量加款成功");
		super.renderJson(map);
	}
	
	@ResponseBody
	@RequestMapping("/batchSubMoney")
	synchronized public void batchSubMoney(String accounts, BigDecimal money, String remark) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		HttpSession session = UserUtil.getSession();
		Long lastOpTime = (Long) session.getAttribute("batchSubMoney");
		if (lastOpTime != null && System.currentTimeMillis() - lastOpTime < 3000) {
			throw new GenericException("3秒内，不能重复此操作");
		}
		session.setAttribute("batchSubMoney", System.currentTimeMillis());
		try {
			batchOptMoneyService.batchSubMoney(accounts, money, remark);
		} catch (Exception e) {
			renderJson(MixUtil.newHashMap("success", true, "msg", e.getMessage()));
			return;
		}
		super.renderSuccess();
	}
}
