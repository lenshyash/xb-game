package com.game.controller.agent.finance;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.vo.MnyScoreReocrdVo;
import com.game.service.MnyScoreService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/mnyscorerd")
public class MnyScoreRdController extends BaseAgentController {

	@Autowired
	private MnyScoreService mnyScoreService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		String begin = request.getParameter("begin");
		String end = request.getParameter("end");
		super.getRequest().setAttribute("startTime", begin == null ? curDate : begin);
		super.getRequest().setAttribute("endTime", end == null ? curDate : end);
		super.getRequest().setAttribute("account", request.getParameter("account"));
		return super.goPage("/page/finance/mnyscorerd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		Long type = super.$long("type");

		MnyScoreReocrdVo scoreRecordVo = new MnyScoreReocrdVo();
		scoreRecordVo.setAccount(account);
		scoreRecordVo.setType(type);
		scoreRecordVo.setBegin(DateUtil.toDate(begin));
		scoreRecordVo.setEnd(DateUtil.getTomorrow(end));
		scoreRecordVo.setStationId(StationUtil.getStationId());
		super.render(mnyScoreService.getScoreReocrd(scoreRecordVo));
	}

	@RequestMapping("/add")
	public String add() {
		return super.goPage("/page/finance/mnyscorerd_add.jsp");
	}

	@ResponseBody
	@RequestMapping("/save")
	public void save(Integer type, String accountName, Integer score, String remark) {
		mnyScoreService.operatScore(type, accountName, score, remark);
		super.renderSuccess();
	}
}
