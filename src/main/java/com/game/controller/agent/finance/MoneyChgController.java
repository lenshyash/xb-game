package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.service.MnyComRecordService;
import com.game.service.MnyMoneyService;
import com.game.third.util.ThirdUtil;
import com.game.util.StationConfigUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/moneychg")
public class MoneyChgController extends BaseAgentController {

	@Autowired
	private MnyComRecordService mnyComRecordService;
	@Autowired
	private MnyMoneyService mnyMoneyService;

	@RequestMapping("/index")
	public String index() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		HttpServletRequest request = getRequest();
		ThirdUtil.initThirdParams();
		request.setAttribute("consumeRate", StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.consume_rate)));
		return super.goPage("/page/finance/moneychg.jsp");
	}

	@ResponseBody
	@RequestMapping("/memmny")
	public void list() {
		String account = super.$c("accountout", "转出会员不能为空");
		String account2 = super.$c("accountin", "转入会员不能为空");
		Map outAcc = mnyMoneyService.getMoneyByAccount(account);
		Map inAcc = mnyMoneyService.getMoneyByAccount(account2);
		super.renderJson(MixUtil.newHashMap("outAcc", outAcc, "inAcc", inAcc));
	}

	@ResponseBody
	@RequestMapping("/save")
	synchronized public void save() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long outAccId = super.$cl("id");
		Long inAccId = super.$cl("id2");
		BigDecimal money = StringUtil.toBigDecimal(super.$c("money"));
		String remark = super.$("remark");
		Long checkBetNum = super.$long("checkBetNum");
		BigDecimal betNumMultiple = StringUtil.toBigDecimal(super.$("betNumMultiple"));

		String key = "moneychange_add_" + inAccId + "_" + money;
		if (RedisAPI.exists(key)) {
			throw new GenericException("3秒内，不能转入相同会员同样金额");
		} else {
			RedisAPI.addCache(key, "1", 3);
		}

		mnyComRecordService.moneyChange(outAccId, inAccId, money, checkBetNum, betNumMultiple, remark);
		super.renderSuccess();
	}
}
