package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.game.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MnyDrawRecord;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.permission.PermissionManager;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.permission.annotation.SortMapping;
import com.game.service.AgentBaseConfigService;
import com.game.service.MnyDrawRecordService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAccountService;
import com.game.service.pay.AgentPaymentService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/memdrawrd")
public class MemDrawRdController extends BaseAgentController {

	@Autowired
	private MnyDrawRecordService mdrService;

	@Autowired
	private SysAccountDailyMoneyService dailyService;
	@Autowired
	private AgentBaseConfigService configService;
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private AgentPaymentService paymentService;

	@RequestMapping("/index")
	public String index(String begin, String end, String account, Map<String, Object> map) {
		String curDate = DateUtil.getCurrentDate();
		map.put("startTime", begin == null ? curDate : begin);
		map.put("endTime", end == null ? curDate : end);
		map.put("saccount", account);
		super.getRequest().setAttribute("isCancle", StationConfigUtil.get(StationConfig.withdraw_handler_cancle));
		super.getRequest().setAttribute("exportPerm",
				PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/export.do"));
		super.getRequest().setAttribute("withdrawalPerm",
				PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/lock.do"));
		super.getRequest().setAttribute("rollBack",
				PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/rollBack.do"));
		super.getRequest().setAttribute("warnFlag","off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning)));
		super.getRequest().setAttribute("onoffcopy", StationConfigUtil.get(StationConfig.agent_copy_info_botton));
		super.getRequest().setAttribute("onRollBackClose", StationConfigUtil.get(StationConfig.onoff_draw_rollback_close));
		super.getRequest().setAttribute("payments", paymentService.getPayments(StationUtil.getStationId()));
		return super.goPage("/page/finance/memdrawrd.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping={"createDatetime","create_datetime","drawMoney","r.draw_money"})
	public void list(String agentName, String account, Long status, Long type, String begin, String end) {
		Date beginDate = DateUtil.toDatetime(begin);
		Date endDate = DateUtil.toDatetime(end);
		String minMoney = super.$("minMoney");
		String maxMoney = super.$("maxMoney");
		String operator = super.$("operator");
		Long reportType = super.$long("reportType");
		String remark = super.$("remark");
		Long drawTimes = super.$long("drawTimes");
		Long payId = super.$long("payId");

		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		mdrvo.setAccount(account);
		mdrvo.setStatus(status);
		mdrvo.setBegin(beginDate);
		mdrvo.setEnd(endDate);
		mdrvo.setAccountType(type);
		mdrvo.setStationId(StationUtil.getStationId());
		mdrvo.setAgentName(agentName);
		mdrvo.setMaxMoney(StringUtil.toBigDecimal(maxMoney));
		mdrvo.setMinMoney(StringUtil.toBigDecimal(minMoney));
		mdrvo.setOperator(operator);
		mdrvo.setReportType(reportType);
		mdrvo.setRemark(remark);
		mdrvo.setDrawTimes(drawTimes);
		mdrvo.setPayId(payId);
		super.render(mdrService.getPage(mdrvo));
	}

	/**
	 * 导出功能
	 */
	@CheckLastLoginIp
	@Permission(CheckType.FUNCTION)
	@ResponseBody
	@RequestMapping("/export")
	public void export() {
		String account = super.$("account");
		Long status = super.$long("status");
		Long type = super.$long("type");

		String begin = super.$("begin");
		String end = super.$("end");

		Date beginDate = DateUtil.toDate(begin);
		Date endDate = DateUtil.getTomorrow(end);

		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		mdrvo.setAccount(account);
		mdrvo.setStatus(status);
		mdrvo.setBegin(beginDate);
		mdrvo.setEnd(endDate);
		mdrvo.setAccountType(type);
		mdrvo.setStationId(StationUtil.getStationId());
		mdrService.export(mdrvo);
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/lock")
	public void lock() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long comId = super.$cl("id");
		Long lockFlag = super.$cl("lockFlag");
		mdrService.lock(comId, lockFlag);
		super.renderSuccess();
	}
	@ResponseBody
	@RequestMapping("/rollBack")
	public void rollBack() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long comId = super.$cl("id");
		mdrService.rollBack(comId);
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/failedRollBack")
	public void failedRollBack() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long comId = super.$cl("id");
		mdrService.failedRollBack(comId);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/handler")
	public String handler(Map<String, Object> map, Long id) {
		Long stationId = StationUtil.getStationId();
		MnyDrawRecord draw = mdrService.getOne(id, stationId);
		SysAccount account = accountService.getOne(draw.getMemberId(), stationId);
		SysAccountInfo accountInfo = accountService.getAccountInfo(account.getId());
		// 会员创建天数
		long createDate = (new Date().getTime() - account.getCreateDatetime().getTime()) / (24 * 3600 * 1000);
		map.put("dailyData", dailyService.getLastSucDepositHandlerData(draw.getMemberId()));
		map.put("draw", draw);
		map.put("account", account);
		map.put("accountInfo", accountInfo);
		map.put("createDate", createDate);
		map.put("curDateTime", DateUtil.getCurrentTime());
		map.put("payments", paymentService.getPayments(stationId));
		map.put("abnormalFlag", account.getAbnormalFlag());
		
		String paymentPassword = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.station_payment_password);
		if(StringUtil.isNotEmpty(paymentPassword)) {
			map.put("needPassword", "on");
		}else {
			map.put("needPassword", "off");
		}
		
		super.getRequest().setAttribute("paymentPerm",
				PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/handler.do"));
		if (StringUtils.equals(configService.getSettingValueByKey(StationConfig.onoff_draw_show_copy_btn.name(), stationId),"off")) {
			return super.prepareLayout("/newpage/finance/memdrawrd_handler.jsp");
		}
		return super.prepareLayout("/newpage/finance/memdrawrd_handler2.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/doHandler")
	public void doHandler(Long id, Integer status, String remark,Long payId, String paymentPassword) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		if (status != null && status == 1) {
			mdrService.drawHandler(id, MnyDrawRecord.STATUS_SUCCESS, new BigDecimal(0), "提款成功 "+remark,payId, paymentPassword);
		} else {
			mdrService.drawHandler(id, MnyDrawRecord.STATUS_FAILED, new BigDecimal(0), remark, paymentPassword);
		}
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/confirm")
	public void confirm() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long drawId = super.$cl("id");
		String fee = super.$("fee");
		String paymentPassword = super.$("paymentPassword");
		mdrService.drawHandler(drawId, MnyDrawRecord.STATUS_SUCCESS, StringUtil.toBigDecimal(fee), "提款成功", paymentPassword);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/failed")
	public void failed() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long drawId = super.$cl("id");
		String remark = super.$("remark");
		String paymentPassword = super.$("paymentPassword");
		mdrService.drawHandler(drawId, MnyDrawRecord.STATUS_FAILED, new BigDecimal(0), remark, paymentPassword);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/drawinfo")
	public void drawinfo() {
		Long accountId = super.$long("accountId");
		Map account = accountService.getAccountById(accountId, StationUtil.getStationId());
		super.renderJson(MixUtil.newHashMap("betNum", StringUtil.toBigDecimal(account.get("betNum")), "drawNeed",
				StringUtil.toBigDecimal(account.get("drawNeed"))));
	}
	
	@CheckLastLoginIp
	@RequestMapping("/cancle")
	public String cancle(Map<String, Object> map, Long id) {
		MnyDrawRecord draw = mdrService.getOne(id, StationUtil.getStationId());
		map.put("draw", draw);
		return super.prepareLayout("/newpage/finance/memdrawrd_cancle.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/doCancle")
	public void doCancle(Long id, String opDesc) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		MnyDrawRecordVo cvrVo = new MnyDrawRecordVo();
		cvrVo.setId(id);
		cvrVo.setOpDesc(opDesc);
		mdrService.cancelHandler(cvrVo);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping("/paymentHandler")
	public String paymentHandler(Map<String, Object> map, Long id) {
		map.put("id", id);
		return super.prepareLayout("/newpage/finance/payment_draw_handler.jsp");
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/doPaymentHandler")
	public void doPaymentHandler(Long id, Integer status,String opDesc) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		MnyDrawRecordVo cvrVo = new MnyDrawRecordVo();
		cvrVo.setId(id);
		cvrVo.setFee(BigDecimal.ZERO);
		cvrVo.setOpDesc(opDesc);
		if(status == MnyDrawRecord.STATUS_SUCCESS){
			cvrVo.setStatus(MnyDrawRecord.STATUS_SUCCESS);
		}else if(status == MnyDrawRecord.STATUS_FAILED){
			cvrVo.setStatus( MnyDrawRecord.STATUS_FAILED);
		}
		mdrService.onlinePaymentHandler(cvrVo);
		super.renderSuccess();
	}
}
