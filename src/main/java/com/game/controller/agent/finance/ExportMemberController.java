package com.game.controller.agent.finance;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.service.ExportService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/export")
public class ExportMemberController extends BaseAgentController {

	@Autowired
	ExportService exportService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/finance/memexport.jsp");
	}

	@ResponseBody
	@RequestMapping("/export")
	public void export(Boolean exportAll) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		AccountVo avo = new AccountVo();
		avo.setAccount(super.$("account"));
		avo.setBegin(DateUtil.toDate(super.$("begin")));
		avo.setEnd(DateUtil.getTomorrow(super.$("end")));
		avo.setStationId(StationUtil.getStationId());
		avo.setStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
		exportService.export(avo, exportAll);
	}
}
