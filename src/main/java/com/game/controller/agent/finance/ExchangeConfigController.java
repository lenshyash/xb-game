package com.game.controller.agent.finance;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MnyExchangeConfig;
import com.game.model.SysPayPlatform;
import com.game.model.vo.ExchangeConfigVo;
import com.game.model.vo.PayPlatformVo;
import com.game.service.MnyExchangeService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/exchange")
public class ExchangeConfigController extends BaseAgentController {

	@Autowired
	MnyExchangeService exchangeService;

	@RequestMapping("/index")
	public String index() {
		//积分兑换现金打码量开关
		super.getRequest().setAttribute("betNumShowFlag","on".equals(StationConfigUtil.get(StationConfig.exchange_score_bet_num)));
		return super.goPage("/page/finance/exchange.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String name = super.$("name");
		Long type = super.$long("type");
		ExchangeConfigVo ecvo = new ExchangeConfigVo();
		ecvo.setType(type);
		ecvo.setName(name);
		ecvo.setStationId(StationUtil.getStationId());
		super.render(exchangeService.getConfigPage(ecvo));
	}
	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		//积分兑换现金打码量开关
		map.put("betNumShowFlag",StationConfigUtil.get(StationUtil.getStation().getId(),StationConfig.exchange_score_bet_num));
		return super.prepareLayout("/newpage/finance/exchange_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("exchange", exchangeService.getOne(id, StationUtil.getStationId()));
		map.put("betNumShowFlag",StationConfigUtil.get(StationUtil.getStation().getId(),StationConfig.exchange_score_bet_num));
		return super.prepareLayout("/newpage/finance/exchange_modify.jsp");
	}
	@RequestMapping("/save")
	@ResponseBody
	public void save(MnyExchangeConfig conf) {

		exchangeService.saveConfig(conf);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long status = super.$long("status");
		exchangeService.updStatus(id, status);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delete")
	public void delete() {
		Long id = super.$long("id");
		exchangeService.delete(id);
		super.renderSuccess();
	}
}
