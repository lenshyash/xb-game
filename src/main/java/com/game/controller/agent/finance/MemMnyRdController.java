package com.game.controller.agent.finance;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.service.MnyMoneyRecordService;
import com.game.service.OrderDetailService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;
import com.game.util.LotteryVersionUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/memmnyrd")
public class MemMnyRdController extends BaseAgentController {

	@Autowired
	private MnyMoneyRecordService mnyMoneyService;
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private OrderDetailService orderDetailService;
	
	@RequestMapping("/index")
	public String index(String begin ,String end ,String account,String agentName,String remark,Map<String,Object> map) {
		String curDate = DateUtil.getCurrentDate();
		end = end == null ? curDate : end;
		if(end.length() <= 10){
			end += " 23:59:59"; 
		}
		map.put("startTime", begin == null ? curDate : begin);
		map.put("endTime", end);
		map.put("account", StringUtil.trim2Empty(account));
		map.put("agentName", agentName);
		map.put("remark", remark);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));

		return super.goPage("/page/finance/memmnyrd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list(String account,String begin,String end ,Long type,String orderId,String agentName,String remark,Long reportType,String operatorName) {
		Long accountType = super.$long("accountType");
		MnyMoneyRecordVo moneyRecordVo = new MnyMoneyRecordVo();
		moneyRecordVo.setAccount(StringUtil.trim2Empty(account));
		moneyRecordVo.setShowRollBack(true);
		moneyRecordVo.setType(type);
		moneyRecordVo.setBegin(DateUtil.toDatetime(begin));
		moneyRecordVo.setEnd(DateUtil.toDatetime(end));
		moneyRecordVo.setOrderId(orderId);
		moneyRecordVo.setRemark(remark);
		moneyRecordVo.setReportType(reportType);
		moneyRecordVo.setOperatorName(operatorName);
		if(accountType!=null && accountType!=0){
			moneyRecordVo.setAccountType(accountType);
		}
		if(StringUtils.isNotEmpty(agentName)){
			SysAccount agent =null;
			try{
			  agent = sysAccountService.findOneByAccountAndStationId(agentName, StationUtil.getStationId());
			}catch(Exception e){
				throw new GenericException("代理不存在！");
			}
			if (agent == null || agent.getAccountType()!=SysAccount.ACCOUNT_PLATFORM_AGENT) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			moneyRecordVo.setParents(parents + agent.getId() + ",");
			moneyRecordVo.setSearchSelf(true);
			moneyRecordVo.setSelfId(agent.getId());
			moneyRecordVo.setShowRollBack(true);
		}
		super.render(mnyMoneyService.getMoneyRecord(moneyRecordVo));
	}

	@RequestMapping("/showLotteryOrderDesc")
	public String showSportOrderDesc(String orderId, String account, String lotCode, Map<String, Object> map) {
		BcLotteryOrder order = orderDetailService.getLotteryOrderDetailForList(orderId, account, StationUtil.getStationId(), lotCode);
		map.put("order", order);
		map.put("isMulti", StringUtils.equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_multi_agent), BusinessConstant.SWITCH_ON));
		if (order.getLotType() == 6 ||order.getLotType() == 66) {
			return super.prepareLayout("/newpage/orderDetail/order_markSix.jsp");
		}
		map.put("version", LotteryVersionUtils.resultVersion(StationUtil.getStationId()));
		return super.prepareLayout("/newpage/orderDetail/order_lottery.jsp");
	}
	
	@RequestMapping("/showSportOrderDesc")
	public String showSportOrderDesc(String orderId, Map<String, Object> map) {
		map.put("order", orderDetailService.getSportOrderDetail(orderId, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/orderDetail/order_sport.jsp");
	}
	
	@RequestMapping("/orderDesc")
	@ResponseBody
	public void orderDesc(String orderId) {
		if (StringUtils.isEmpty(orderId)) {
			throw new GenericException("参数错误!");
		}
		if (orderId.startsWith("S")) {
			super.renderJson(orderDetailService.getSportOrderDetail(orderId, StationUtil.getStationId()));
		} else {
			super.renderJson(orderDetailService.getLotteryOrderDetailForList(orderId, null, StationUtil.getStationId(), null));
		}
	}
}
