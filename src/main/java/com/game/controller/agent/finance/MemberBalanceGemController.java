package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberBalanceGemStrategy;
import com.game.model.vo.AccountVo;
import com.game.service.AgentBaseConfigService;
import com.game.service.MemberBalanceGemStrategyLevelService;
import com.game.service.MemberBalanceGemStrategyService;
import com.game.service.MemberLevelService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

/**
 * 余额宝管理
 * @author admin
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/balanceGem")
public class MemberBalanceGemController extends BaseAgentController {
	@Autowired
	private SysAccountService sysAccountService;

	@RequestMapping("/index")
	public String index(String account) {
		super.getRequest().setAttribute("account", account);
		return super.goPage("/page/finance/memberBalanceGem.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String account = super.$("account");
		String agent = super.$("agent");
		Long reportType = super.$long("reportType");
		Long accountType = super.$long("accountType");
		AccountVo acc = new AccountVo();
		acc.setStationId(StationUtil.getStationId());
		acc.setAccount(account);
		acc.setAccountType(accountType);
		acc.setReportType(reportType);
		super.render(sysAccountService.getBalanceGemPage(acc));
	}
}
