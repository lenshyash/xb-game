package com.game.controller.agent.finance;

import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.MemberDepositStrategy;
import com.game.service.MemberDepositStrategyService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/depositGiftActivity")
public class DepositGiftActivityController extends BaseAgentController {

	@Autowired
	private MemberDepositStrategyService memberDepositStrategyService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/finance/depositGiftActivity.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Long stationId = StationUtil.getStationId();
		super.render(memberDepositStrategyService.page(stationId));
	}

	/**
	 * 租户公告状态修改
	 */
	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Integer status, Long id) {
		Long stationId = StationUtil.getStationId();
		memberDepositStrategyService.openCloseH(status, id, stationId);
		super.renderSuccess();
	}
	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		return super.prepareLayout("/newpage/finance/depositGiftActivity_add.jsp");
	}
	/**
	 * 新增类型
	 */
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(MemberDepositStrategy alb) {
		alb.setStationId(StationUtil.getStationId());
		memberDepositStrategyService.addSave(alb);
		super.renderSuccess();
	}
	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("gift", memberDepositStrategyService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/finance/depositGiftActivity_modify.jsp");
	}
	@RequestMapping("/eidtSaveNew")
	@ResponseBody
	public void eidtSaveNew(MemberDepositStrategy alb) {
		alb.setStationId(StationUtil.getStationId());
		memberDepositStrategyService.eidtSave(alb);
		super.renderSuccess();
	}
	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave() {
		String json = super.$c("data");
		MemberDepositStrategy alb = JsonUtil.toBean(json, MemberDepositStrategy.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		memberDepositStrategyService.addSave(alb);
		super.renderSuccess();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/eidtSave")
	@ResponseBody
	public void eidtSave() {
		String json = super.$c("data");
		MemberDepositStrategy alb = JsonUtil.toBean(json, MemberDepositStrategy.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		memberDepositStrategyService.eidtSave(alb);
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		memberDepositStrategyService.delete(id, stationId);
		super.renderSuccess();
	}

}
