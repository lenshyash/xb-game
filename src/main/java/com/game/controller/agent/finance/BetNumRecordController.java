package com.game.controller.agent.finance;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.BetNumRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/betnumrd")
public class BetNumRecordController extends BaseAgentController {
	@Autowired
	private BetNumRecordService betNumRecordService;

	@RequestMapping("/index")
	public String index(String begin, String end, String account, Map<String, Object> map) {
		Date beginDate = DateUtil.parseDate(begin);
		if(beginDate==null){
			map.put("begin", DateUtil.getCurrentDate()+" 00:00:00");
		}else{
			map.put("begin",DateUtil.toDatetimeStr(beginDate));
		}
		Date endDate = DateUtil.parseDate(end);
		if(endDate==null){
			map.put("end", DateUtil.getCurrentDate()+" 23:59:59");
		}else{
			map.put("end",DateUtil.toDatetimeStr(endDate));
		}
		map.put("account", account);
		return prepareLayout("/newpage/finance/betnumrd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list(String account, String begin, String end, Integer type) {
		Date beginDate = DateUtil.toDatetime(begin);
		if (beginDate == null) {
			beginDate = DateUtil.dayFirstTime(new Date(), 0);
		}
		Date endDate = DateUtil.toDatetime(end);
		if(endDate==null){
			endDate = DateUtil.dayFirstTime(new Date(), 1);
		}
		super.render(betNumRecordService.getPage(account, beginDate, endDate, type, StationUtil.getStationId()));
	}

}
