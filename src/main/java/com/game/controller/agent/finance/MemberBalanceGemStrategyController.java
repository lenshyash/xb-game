package com.game.controller.agent.finance;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberBalanceGemStrategy;
import com.game.service.AgentBaseConfigService;
import com.game.service.MemberBalanceGemStrategyLevelService;
import com.game.service.MemberBalanceGemStrategyService;
import com.game.service.MemberLevelService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

/**
 * 余额宝策略管理
 * @author admin
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/finance/balanceGemStrategy")
public class MemberBalanceGemStrategyController extends BaseAgentController {
	@Autowired
	private MemberBalanceGemStrategyService MemberBalanceGemStrategyService;
	@Autowired
	private MemberBalanceGemStrategyLevelService MemberBalanceGemStrategyLevelService;
	@Autowired
	private AgentBaseConfigService baseConfigService;
	@Autowired
	private MemberLevelService memberLevelService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		map.put("sysApiDeposit",
				baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), StationUtil.getStationId()));
		return super.goPage("/page/finance/memberBalanceGemStrategy.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list( String begin, String end) {
		super.render(MemberBalanceGemStrategyService.getPage(DateUtil.toDate(begin),
				DateUtil.getTomorrow(end), StationUtil.getStationId()));
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		Long stationId = StationUtil.getStationId();
		map.put("sysApiDeposit", baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), stationId));
		map.put("levelList", memberLevelService.getStationLevels(stationId));
		Calendar c = Calendar.getInstance();
		map.put("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.toDateStr(c.getTime()));
		return super.prepareLayout("/newpage/finance/memberBalanceGemStrategyAdd.jsp");
	}

	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave(MemberBalanceGemStrategy com, BigDecimal rollBackRate, Long[] groupLevelId, String startDate,
			String endDate) {
		MemberBalanceGemStrategyService.addSave(com, groupLevelId, startDate, endDate, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		Long stationId = StationUtil.getStationId();
		map.put("sysApiDeposit", baseConfigService.isOn(StationConfig.sys_api_deposit_key.name(), stationId));
		map.put("levelList", memberLevelService.getStationLevels(stationId));
		map.put("gem", MemberBalanceGemStrategyService.getOne(id, stationId));
		map.put("levelSet", MemberBalanceGemStrategyLevelService.getLevelSet(id, stationId));
		return super.prepareLayout("/newpage/finance/memberBalanceGemStrategyModify.jsp");
	}
	@RequestMapping("/rule")
	public String rule(Map<String, Object> map, Long id) {
		return super.prepareLayout("/newpage/finance/memberBalanceGemStrategyRule.jsp");
	}
	@RequestMapping("/update")
	@ResponseBody
	public void update(MemberBalanceGemStrategy com, BigDecimal rollBackRate, Long[] groupLevelId, String startDate,
			String endDate) {
		MemberBalanceGemStrategyService.update(com, groupLevelId, startDate, endDate,StationUtil.getStationId());
		super.renderSuccess();
	}

	/**
	 * 租户公告状态修改
	 */
	@RequestMapping("/updStatus")
	@ResponseBody
	public void updStatus(Integer status, Long id) {
		MemberBalanceGemStrategyService.updStatus(status, id, StationUtil.getStationId());
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		MemberBalanceGemStrategyService.delete(id, stationId);
		super.renderSuccess();
	}
}
