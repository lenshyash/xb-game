package com.game.controller.agent;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.service.SysAnnouncementService;
import com.game.util.StationConfigUtil;

public class BaseAgentController extends ActionUtil {
	@Autowired
	private SysAnnouncementService announcementService;

	/**
	 * 获取资源目录
	 * 
	 * @return
	 */
	public String getDomainFolder() {
		return SystemConfig.SOURCE_FOLDER_AGENT;
	}

	/**
	 * 是否使用新后台模版
	 * 
	 * @return
	 */
	public boolean isNewPlatform() {
		return true;
	}

	public String prepareLayout(String jspPath) {
		HttpServletRequest request = SysUtil.getRequest();
		StringBuilder path = new StringBuilder(getDomainFolder());
		if (jspPath.startsWith("/")) {
			jspPath = jspPath.substring(1);
		}
		if (jspPath.startsWith("newpage")) {
			path.append("/").append(jspPath);
		} else {
			path.append("/newpage");
			if (jspPath.startsWith("page")) {
				path.append(jspPath.substring(4));
			} else {
				if (!jspPath.startsWith("/")) {
					path.append("/");
				}
				path.append(jspPath);
			}
		}
		if (ActionUtil.isAjaxRequest(request) || path.toString().equals("/agent/newpage/index.jsp") || path.toString().equals("/agent/newpage/home.jsp")) {
			request.setAttribute("otp", StationConfigUtil.get(StationConfig.onoff_agent_login_otp_key));
			return path.toString();
		} else {
			request.setAttribute("content_page", path.toString());
			String url = request.getRequestURI();
			url = url.substring(request.getContextPath().length());
			request.setAttribute("content_url", url);
			request.setAttribute("_home_ams", JSON.toJSON(announcementService.getLastAnnouncement(10)));
			return getDomainFolder() + "/newpage/home.jsp";
		}
	}

	public String goPage(String jspPath) {
		return prepareLayout(jspPath);
	}
}
