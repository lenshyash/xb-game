package com.game.controller.agent;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.lottery.BcLotteryOrder;
import com.game.service.OrderDetailService;
import com.game.util.LotteryVersionUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/orderDetail")
public class AgentOrderDetailController extends BaseAgentController {
	@Autowired
	private OrderDetailService orderDetailService;

	@RequestMapping("/showSportOrderDesc")
	public String showSportOrderDesc(String orderId, Map<String, Object> map) {
		map.put("order", orderDetailService.getSportOrderDetail(orderId, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/orderDetail/order_sport.jsp");
	}

	@RequestMapping("/showLotteryOrderDesc")
	public String showSportOrderDesc(String orderId, String account, String lotCode, Map<String, Object> map) {
		BcLotteryOrder order = orderDetailService.getLotteryOrderDetailForList(orderId, account, StationUtil.getStationId(), lotCode);
		map.put("order", order);
		map.put("isMulti", StringUtils.equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_multi_agent), BusinessConstant.SWITCH_ON));
		if (order.getLotType() == 6 ||order.getLotType() == 66) {
			return super.prepareLayout("/newpage/orderDetail/order_markSix.jsp");
		}
		map.put("version", LotteryVersionUtils.resultVersion(StationUtil.getStationId()));
		return super.prepareLayout("/newpage/orderDetail/order_lottery.jsp");
	}

	@RequestMapping("/orderDesc")
	@ResponseBody
	public void orderDesc(String orderId) {
		if (StringUtils.isEmpty(orderId)) {
			throw new GenericException("参数错误!");
		}
		if (orderId.startsWith("S")) {
			super.renderJson(orderDetailService.getSportOrderDetail(orderId, StationUtil.getStationId()));
		} else {
			super.renderJson(orderDetailService.getLotteryOrderDetailForList(orderId, null, StationUtil.getStationId(), null));
		}
	}
}
