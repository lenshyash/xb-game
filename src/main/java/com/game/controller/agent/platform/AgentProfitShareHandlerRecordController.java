package com.game.controller.agent.platform;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.AgentProfitShareHandlerRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/profitsharehandlerrecord")
public class AgentProfitShareHandlerRecordController extends BaseAgentController {
	@Autowired
	private AgentProfitShareHandlerRecordService profitShareHandlerRecordService;

	@RequestMapping("/index")
	public String index() {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startDate", curDate);
		super.getRequest().setAttribute("endDate", curDate);
		return super.prepareLayout("/newpage/platform/profitshare/agentProfitShareHandlerRecord.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String account, String startDate, String endDate, Integer status,String agentName,String parentNames) {
		super.render(profitShareHandlerRecordService.page(account,agentName,parentNames, DateUtil.validStartDateTime(startDate),
				DateUtil.validEndDateTime(endDate), status, StationUtil.getStationId()));
	}

	/**
	 * 根据操作记录id进行确认发放
	 */
	@RequestMapping("/doConfirm")
	@ResponseBody
	public void doConfirm(String[] ids,String[] moneys) {
		if (moneys == null || moneys.length == 0 || ids == null || ids.length == 0  ) {
			throw new GenericException("参数不正确!");
		}
		profitShareHandlerRecordService.doProfitShare(StationUtil.getStationId(), ids, moneys);
		renderJson(MixUtil.newHashMap("msg", "操作成功！"));
	}
}
