package com.game.controller.agent.platform;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentDepositLevel;
import com.game.model.platform.MemberBackwaterStrategy;
import com.game.service.MemberBackwaterStrategyLevelService;
import com.game.service.MemberBackwaterStrategyService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/strategy")
public class MemberBackwaterStrategyController extends BaseAgentController {
	@Autowired
	private MemberBackwaterStrategyService memberBackwaterStrategyService;
	@Autowired
	private MemberBackwaterStrategyLevelService memberBackwaterStrategyLevelService;

	@RequestMapping("/index")
	public String index() {
		super.getRequest().setAttribute("zhenRenKaiGuan", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("levels1", MemberLevelUtil.getStationLevels());
		super.getRequest().setAttribute("isV4", "v4".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name)));
		super.getRequest().setAttribute("replaceFont", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_replace_font));
		return super.prepareLayout("/newpage/platform/backwaterStrategy/memberBackwaterStrategy.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Integer type) {
		Long stationId = StationUtil.getStationId();
		super.render(memberBackwaterStrategyService.page(type, stationId));
	}

	@RequestMapping("/closeOrOpen")
	@ResponseBody
	public void closeOrOpen(Integer status, Long id) {
		Long stationId = StationUtil.getStationId();
		memberBackwaterStrategyService.closeOrOpen(status, stationId, id);
		super.renderSuccess();
		SysLogUtil.log("修改按日反水策略id=" + id + " 状态＝" + status, LogType.AGENT_SYSTEM);
	}
	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		super.getRequest().setAttribute("zhenRenKaiGuan", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le));
		boolean isV4 = "v4".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		super.getRequest().setAttribute("isV4", isV4);
		if(isV4){
			super.getRequest().setAttribute("roomList", memberBackwaterStrategyService.getRoomStationId(StationUtil.getStationId()));
		}
		super.getRequest().setAttribute("levels1", MemberLevelUtil.getStationLevels());
		super.getRequest().setAttribute("replaceFont", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_replace_font));
		return super.prepareLayout("/newpage/platform/backwaterStrategy/backwaterStrategy_add.jsp");
	}
	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("rollback", memberBackwaterStrategyService.getOne(id, StationUtil.getStationId()));
		super.getRequest().setAttribute("zhenRenKaiGuan", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le));
		boolean isV4 = "v4".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		super.getRequest().setAttribute("isV4", isV4);
		if(isV4){
			super.getRequest().setAttribute("roomList", memberBackwaterStrategyService.getRoomStationId(StationUtil.getStationId()));
		}
		map.put("levels1", MemberLevelUtil.getStationLevels());
		map.put("glevelIds", memberBackwaterStrategyLevelService.getLevelIdsByStrategyId(id,  StationUtil.getStationId()));
		return super.prepareLayout("/newpage/platform/backwaterStrategy/backwaterStrategy_modify.jsp");
	}
	@RequestMapping("/save")
	@ResponseBody
	public void save( MemberBackwaterStrategy mrbs, Long[] groupLevelId) {
		Long stationId = StationUtil.getStationId();
		mrbs.setStationId(stationId);
		memberBackwaterStrategyService.insert(mrbs,groupLevelId);
		super.renderSuccess();
		SysLogUtil.log("保存按日反水策略" + JSON.toJSONString(mrbs), LogType.AGENT_SYSTEM);
	}

	@RequestMapping("/update")
	@ResponseBody
	public void update(MemberBackwaterStrategy mrbs, Long[] groupLevelId) {
		Long stationId = StationUtil.getStationId();
		mrbs.setStationId(stationId);
		memberBackwaterStrategyService.update(mrbs,groupLevelId);
		super.renderSuccess();
		SysLogUtil.log("修改反水策略" + JSON.toJSONString(mrbs), LogType.AGENT_SYSTEM);
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		MemberBackwaterStrategy old = memberBackwaterStrategyService.getOne(id, stationId);
		if(old!=null){
			memberBackwaterStrategyService.delete(old);
			SysLogUtil.log("删除反水策略" + JSON.toJSONString(old), LogType.AGENT_SYSTEM);
		}
		super.renderSuccess();
	}

}
