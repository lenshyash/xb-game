package com.game.controller.agent.platform;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.AgentProfitShareRecordService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/profitsharerecord")
public class AgentProfitShareRecordController extends BaseAgentController {
	@Autowired
	private AgentProfitShareRecordService profitShareRecordService;

	@RequestMapping("/index")
	public String index() {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startDate", curDate);
		super.getRequest().setAttribute("endDate", curDate);
		return super.prepareLayout("/newpage/platform/profitshare/agentProfitShareRecord.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String account, String startDate, String endDate, Integer status) {
		super.render(profitShareRecordService.page(account, DateUtil.validStartDateTime(startDate),
				DateUtil.validEndDateTime(endDate), status, StationUtil.getStationId()));
	}

	/**
	 * 占成详情
	 */
	@RequestMapping("/info")
	public String info(Long recordId) {
		super.getRequest().setAttribute("row",
				profitShareRecordService.findOneMap(recordId, StationUtil.getStationId()));
		super.getRequest().setAttribute("zrOnoff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("dzOnoff", StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("chessOnoff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("sportOnoff", StationConfigUtil.get(StationConfig.onoff_sports_game));
		super.getRequest().setAttribute("thirdLotteryOnoff", StationConfigUtil.get(StationConfig.onoff_third_lottery));
		super.getRequest().setAttribute("thirdSportOnoff", StationConfigUtil.get(StationConfig.onoff_third_sports));
		super.getRequest().setAttribute("lotteryOnoff", StationConfigUtil.get(StationConfig.onoff_lottery_game));
		return super.prepareLayout("/newpage/platform/profitshare/agentProfitShareRecord_info.jsp");
	}
}
