package com.game.controller.agent.platform;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.service.AgentProfitShareStrategyService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/profitsharestrategy")
public class AgentProfitShareStrategyController extends BaseAgentController {
	@Autowired
	private AgentProfitShareStrategyService agentProfitShareStrategyService;

	@RequestMapping("/index")
	public String index() {
		super.getRequest().setAttribute("zrOnoff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("dzOnoff", StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("chessOnoff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("sportOnoff", StationConfigUtil.get(StationConfig.onoff_sports_game));
		super.getRequest().setAttribute("thirdLotteryOnoff", StationConfigUtil.get(StationConfig.onoff_third_lottery));
		super.getRequest().setAttribute("thirdSportOnoff", StationConfigUtil.get(StationConfig.onoff_third_sports));
		super.getRequest().setAttribute("lotteryOnoff", StationConfigUtil.get(StationConfig.onoff_lottery_game));
		return super.prepareLayout("/newpage/platform/profitshare/profitShareStrategy.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Integer type) {
		Long stationId = StationUtil.getStationId();
		super.render(agentProfitShareStrategyService.page(type, stationId));
	}

	@RequestMapping("/closeOrOpen")
	@ResponseBody
	public void closeOrOpen(Integer status, Long id) {
		Long stationId = StationUtil.getStationId();
		agentProfitShareStrategyService.closeOrOpen(status, stationId, id);
		super.renderSuccess();
		SysLogUtil.log("修改按日占成策略id=" + id + " 状态＝" + status, LogType.AGENT_SYSTEM);
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		super.getRequest().setAttribute("zrOnoff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("dzOnoff", StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("chessOnoff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("sportOnoff", StationConfigUtil.get(StationConfig.onoff_sports_game));
		super.getRequest().setAttribute("thirdLotteryOnoff", StationConfigUtil.get(StationConfig.onoff_third_lottery));
		super.getRequest().setAttribute("thirdSportOnoff", StationConfigUtil.get(StationConfig.onoff_third_sports));
		super.getRequest().setAttribute("lotteryOnoff", StationConfigUtil.get(StationConfig.onoff_lottery_game));
		return super.prepareLayout("/newpage/platform/profitshare/profitShareStrategy_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		super.getRequest().setAttribute("zrOnoff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("dzOnoff", StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("chessOnoff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("sportOnoff", StationConfigUtil.get(StationConfig.onoff_sports_game));
		super.getRequest().setAttribute("thirdLotteryOnoff", StationConfigUtil.get(StationConfig.onoff_third_lottery));
		super.getRequest().setAttribute("thirdSportOnoff", StationConfigUtil.get(StationConfig.onoff_third_sports));
		super.getRequest().setAttribute("lotteryOnoff", StationConfigUtil.get(StationConfig.onoff_lottery_game));
		map.put("profitShare", agentProfitShareStrategyService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/platform/profitshare/profitShareStrategy_modify.jsp");
	}

	@RequestMapping("/save")
	@ResponseBody
	public void save(AgentProfitShareStrategy mrbs) {
		Long stationId = StationUtil.getStationId();
		mrbs.setStationId(stationId);
		agentProfitShareStrategyService.insert(mrbs);
		super.renderSuccess();
		SysLogUtil.log("保存按日占成策略" + JSON.toJSONString(mrbs), LogType.AGENT_SYSTEM);
	}

	@RequestMapping("/update")
	@ResponseBody
	public void update(AgentProfitShareStrategy mrbs) {
		Long stationId = StationUtil.getStationId();
		mrbs.setStationId(stationId);
		agentProfitShareStrategyService.update(mrbs);
		super.renderSuccess();
		SysLogUtil.log("修改按日占成策略" + JSON.toJSONString(mrbs), LogType.AGENT_SYSTEM);
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		AgentProfitShareStrategy old = agentProfitShareStrategyService.getOne(id, stationId);
		if (old != null) {
			agentProfitShareStrategyService.delete(old);
			SysLogUtil.log("删除按日占成策略" + JSON.toJSONString(old), LogType.AGENT_SYSTEM);
		}
		super.renderSuccess();
	}

}
