package com.game.controller.agent.platform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.MemberBackwaterStrategyService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/backwater")
public class MemberBackwaterRecordController extends BaseAgentController {
	@Autowired
	private MemberBackwaterRecordService memberBackwaterRecordService;
	@Autowired
	private MemberBackwaterStrategyService memberBackwaterStrategyService;

	@RequestMapping("/index")
	public String index() {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startDate", curDate);
		super.getRequest().setAttribute("endDate", curDate);
		super.getRequest().setAttribute("zhenRenKaiGuan",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le));
		boolean isV4 = "v4"
				.equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		super.getRequest().setAttribute("isV4", isV4);
		if (isV4) {
			super.getRequest().setAttribute("roomList",
					memberBackwaterStrategyService.getRoomStationId(StationUtil.getStationId()));
		}
		super.getRequest().setAttribute("replaceFont", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_replace_font));
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.prepareLayout("/newpage/platform/memberBackwaterRecord.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String account, String startDate, String endDate, Integer betType, Integer status,
			Long backwaterRoomId,Long reportType) {
		super.render(memberBackwaterRecordService.page(account, DateUtil.validStartDateTime(startDate),
				DateUtil.validEndDateTime(endDate), betType, status, StationUtil.getStationId(), backwaterRoomId,reportType));
	}

	@RequestMapping(value = "/cancel", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public void cancel(Integer betType, String account, String betDate, Long roomId) {
		memberBackwaterRecordService.cancel(betType, account, betDate, StationUtil.getStationId(), roomId);
		renderJson(MixUtil.newHashMap("msg", "反水回滚成功！"));
	}

	/**
	 * 手动反水结算
	 *
	 * @param betType
	 * @param betId
	 * @param stationId
	 * @return
	 */
	@RequestMapping(value = "/manualRollback", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public void manualRollback(Integer betType, String account, String betDate, Long roomId, BigDecimal money) {
		memberBackwaterRecordService.manualRollback(betType, account, betDate, StationUtil.getStationId(), roomId, money);
		renderJson(MixUtil.newHashMap("msg", "反水成功！"));
	}

	/**
	 * 根据反水记录id进行返水
	 */
	@RequestMapping("/dealRollIds")
	@ResponseBody
	public void dealRollIds(String[] moneys, String startDate, String endDate) {
		if (moneys == null || moneys.length == 0) {
			throw new GenericException("参数不正确!");
		}
		memberBackwaterRecordService.doBackwaterMoneyByBatch(moneys, StationUtil.getStationId(),
				DateUtil.validStartDateTime(startDate), DateUtil.validEndDateTime(endDate));
		renderJson(MixUtil.newHashMap("msg", "反水成功！"));
	}

	@RequestMapping("/collectDayBetMoney")
	@ResponseBody
	public void collectDayBetMoney(String startDate, String endDate, Integer type) {
		Date start = DateUtil.toDate(startDate);
		if (start == null) {
			renderJson(MixUtil.newHashMap("msg", "请选择开始时间"));
			return;
		}
		Date end = DateUtil.getTomorrow(endDate);
		if (end == null) {
			renderJson(MixUtil.newHashMap("msg", "请选择结束时间"));
			return;
		}
		if (end.before(start)) {
			renderJson(MixUtil.newHashMap("msg", "开始时间不能大于结束时间"));
			return;
		}
		if (end.getTime() - start.getTime() > 2678400000L) {// 时间范围大于31天
			renderJson(MixUtil.newHashMap("msg", "2个时间范围不能大于31天"));
			return;
		}
		if (type == null) {
			type = 2;
		}
		Long stationId = StationUtil.getStationId();
		if (StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent), "on")
				&& !StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_platform_backwater), "on")) {
			renderJson(MixUtil.newHashMap("msg", "您的站点使用多极代理，不需要反水"));
			return;
		}
		List<Long> sIdList = new ArrayList<>();
		sIdList.add(stationId);
		List<MemberBackwaterRecord> list = null;
		switch (type) {
		case 1:
			list = memberBackwaterRecordService.collectMarkSixDayBetMoney(start, end, sIdList);
			break;
		case 2:
			list = memberBackwaterRecordService.collectLotteryDayBetMoney(start, end, sIdList);
			break;
		case 3:
			list = memberBackwaterRecordService.collectSportDayBetMoney(start, end, sIdList);
			break;
		}
		memberBackwaterRecordService.saveAll(list);
		renderSuccess();
	}
}
