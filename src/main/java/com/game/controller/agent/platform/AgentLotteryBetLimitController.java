package com.game.controller.agent.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentLotteryBetLimit;
import com.game.model.lottery.BcLottery;
import com.game.model.lottery.BcLotteryPlayGroup;
import com.game.service.AgentLotteryBetLimitService;
import com.game.service.lottery.BcLotteryService;
import com.game.service.lottery.impl.BcLotteryPlayGroupServiceImpl;
import com.game.util.LotteryVersionUtils;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/agentLotteryBetLimit")
public class AgentLotteryBetLimitController extends BaseAgentController {
	@Autowired
	private AgentLotteryBetLimitService agentLotteryBetLimitService;
	@Autowired
	private BcLotteryService bcLotteryService;
	@Autowired
	private BcLotteryPlayGroupServiceImpl bcLotteryPlayGroupService;
	
	@RequestMapping("/index")
	public String index() {
		return super.prepareLayout("/newpage/platform/lottery_bet_limit.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void List() {
		Long stationId = StationUtil.getStationId();
		Page page = agentLotteryBetLimitService.getPage(stationId);
		render(page);
	}
	
	@RequestMapping("/updateStatus")
	@ResponseBody
	public void updateStatus(Long id,Long status) {
		Long stationId = StationUtil.getStationId();
		super.isNotNull(stationId, "stationId为空");
		super.isNotNull(id, "id为空");
		super.isNotNull(status, "status为空");
		agentLotteryBetLimitService.updateStatus(id, status, stationId);
		super.renderSuccess();
	}
	
	@RequestMapping("/add")
	public String add() {
		Long stationId = StationUtil.getStationId();
		super.isNotNull(stationId, "stationId为空");
		Integer version = LotteryVersionUtils.resultVersion(stationId);
		List<BcLottery> allActive = bcLotteryService.getAllActive(stationId, version);
		HttpServletRequest request = super.getRequest();
		request.setAttribute("allActive", allActive);
		return super.prepareLayout("/newpage/platform/lottery_bet_limit_add.jsp");
	}
	
	@RequestMapping("/getPlayGroup")
	@ResponseBody
	public void getPlayGroup(Integer type) {
		Long stationId = StationUtil.getStationId();
		List<BcLotteryPlayGroup> lotteryPlayGroup = bcLotteryPlayGroupService.findAllActiveList(type, stationId);
		super.renderJson(JSON.toJSONString(lotteryPlayGroup));
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public void save(String code,Long betLimit,Long limitType,Long type,String groupName) {
		Long stationId = StationUtil.getStationId();
		super.isNotNull(stationId, "站点id为空");
		super.isNotNull(code, "大玩法为空");
		super.isNotNull(betLimit, "限制个数为空");
		super.isNotNull(limitType, "限制类型为空");
		super.isNotNull(type, "type为空");
		super.isNotNull(groupName, "groupName为空");
		
		AgentLotteryBetLimit one = agentLotteryBetLimitService.getOne(stationId, groupName, code, null, type);
		if(StringUtil.isNotEmpty(one)) {
			throw new GenericException("存在相同限制");
		}
		
		AgentLotteryBetLimit agentLotteryBetLimit = new AgentLotteryBetLimit();
		agentLotteryBetLimit.setStationId(stationId);
		agentLotteryBetLimit.setLimitType(limitType);
		agentLotteryBetLimit.setCode(code);
		agentLotteryBetLimit.setBetLimit(betLimit);
		agentLotteryBetLimit.setLimitType(limitType);
		agentLotteryBetLimit.setLotType(type);
		agentLotteryBetLimit.setGroupName(groupName);
		//默认保存就是启动状态
		agentLotteryBetLimit.setStatus(2L);
		agentLotteryBetLimitService.save(agentLotteryBetLimit);
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		super.isNotNull(id, "id为空");
		agentLotteryBetLimitService.delete(id);
		super.renderSuccess();
	}
}
