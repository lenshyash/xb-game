package com.game.controller.agent.platform;

import java.util.Map;

import com.game.model.MnyComRecord;
import com.game.permission.annotation.CheckLastLoginIp;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysStationDomain;
import com.game.service.SysStationDomainService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/domain")
public class AgentDomainController extends BaseAgentController {
	@Autowired
	private SysStationDomainService sysDomainService;

	@RequestMapping("/index")
	public String index() {
		return super.prepareLayout("/newpage/platform/domainList.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String domain, String agentName) {
		super.render(sysDomainService.getPageForAgent(domain, agentName, StationUtil.getStationId()));
	}

	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/platform/domain_add.jsp");
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(String domain, String agentName, String defaultHome, Long id) {
		String domainNames = StringUtils.lowerCase(StringUtils.trim(domain));
		if (id != null && id > 0) {
			if (StringUtils.containsAny(domainNames, '\n', ',', ' ')) {
				throw new GenericException("编辑时，只能存在一个域名");
			}
			sysDomainService.updateDomainForAgent(id, domainNames, agentName, defaultHome, StationUtil.getStationId());
		} else {
			String r = sysDomainService.saveDomain(domainNames, agentName, defaultHome, StationUtil.getStationId());
			if (r != null && !r.isEmpty()) {
				renderFailure(r);
				return;
			}
		}
		super.renderSuccess();

	}

	@RequestMapping("/modify")
	public String add(Map<String, Object> map, Long id) {
		map.put("domain", sysDomainService.findOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/platform/domain_modify.jsp");
	}

	@RequestMapping(value = "/updStatus")
	@ResponseBody
	public void updStatus(Long status, Long id) {
		sysDomainService.updStatusForAgent(id, status, StationUtil.getStationId());
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delete")
	public void delete(Long id) {
		sysDomainService.delete(id, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/updateRemark")
	public String updateRemark(Map<String, Object> map, Long id) {
		SysStationDomain domain = sysDomainService.findOne(id,StationUtil.getStationId());
		map.put("domain", domain);
		return super.prepareLayout("/newpage/platform/domain_update_remark.jsp");
	}
	@ResponseBody
	@RequestMapping("/saveRemark")
	public void saveRemark(Long id,String remark) {
		sysDomainService.saveRemark(id, remark);
		super.renderSuccess();
	}
}
