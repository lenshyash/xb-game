package com.game.controller.agent.platform;

import org.apache.commons.codec.binary.StringUtils;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.service.ProxyMultiRebateRecordService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;


@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH+"/pmrr")
public class ProxyMultiRebateRecordController extends BaseAgentController {
	
	@Autowired
	private ProxyMultiRebateRecordService proxyMultiRebateRecordService;
	@Autowired
	private ProxyMultiRebateCoreService proxyMultiRebateCoreService;
	
	@RequestMapping("/index")
	public String index(){
		super.getRequest().setAttribute("curDate", DateUtil.getCurrentDate());
		super.getRequest().setAttribute("fixedRebate", StringUtils.equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_proxy_fixed_rebate), "on"));
		
		return super.goPage("/page/platform/proxyMultiRebateRecord.jsp");
	}
	@RequestMapping("/list")
	@ResponseBody
	public void list(ProxyMultiRebateRecord proxyMultiRebateRecord,String startTime,String endTime){
		proxyMultiRebateRecord.setStationId(StationUtil.getStationId());
		Page<ProxyMultiRebateRecord> page = proxyMultiRebateRecordService.getPageConfig(proxyMultiRebateRecord, startTime, endTime);
		 super.render(page);
	}
	
	/**
	 * 返点回滚提交
	 * @return
	 */
	@RequestMapping(value="/cancel",produces={"application/json;charset=UTF-8"})
	@ResponseBody
	public String cancle(Long betId,Long stationId){
		int i = proxyMultiRebateRecordService.cancleProxyMultiRebate(betId,StationUtil.getStationId());
		JSONObject result = new JSONObject();
		if(i==1){
			result.put("code", 0);
			result.put("msg", "返点回滚提交成功");
		}else{
			result.put("code", -1);
			result.put("msg", "返点回滚提交失败");
		}
		return result.toJSONString();
	}
	
	
}
