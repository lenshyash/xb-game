package com.game.controller.agent.platform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/prr")
public class ProxyRebateRecordController extends BaseAgentController {

	@Autowired
	private ProxyDailyBettingStatisticsService pbssService;

	/**
	 * 每日代理返点统计
	 * 
	 * @return
	 */
	@RequestMapping("/index")
	public String index() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		super.getRequest().setAttribute("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DAY_OF_MONTH, -1);
		super.getRequest().setAttribute("endDate", DateUtil.toDateStr(c.getTime()));
		super.getRequest().setAttribute("rebateType", StationConfigUtil.get(StationConfig.proxy_daily_rebate));
		return super.goPage("/page/platform/proxyRebateRecord.jsp");
	}

	/**
	 * startDate 统计日期
	 * 
	 * @param startDate
	 * @param endDate
	 * @param account
	 */
	@RequestMapping("/list")
	@ResponseBody
	public void list(String startDate, String endDate, String account,Integer status,Integer searchType) {
		Long stationId = StationUtil.getStationId();
		super.render(pbssService.page(DateUtil.toDate(startDate), DateUtil.toDate(endDate), account, stationId,status,searchType));
	}

	/**
	 * 二级代理返点 PointIds: 返点ID 以,隔开 point:返点数
	 * 
	 * @return
	 */
	@RequestMapping("/toProfit")
	@ResponseBody
	public void toProfit(String pointIds, Long accountId, BigDecimal lotteryPoint, BigDecimal markSixPoint, BigDecimal realPoint, BigDecimal egamePoint, BigDecimal sportsPoint, BigDecimal chessPoint,BigDecimal esportsPoint) {
		Long stationId = StationUtil.getStationId();
		JSONObject jobj = new JSONObject();
		if (StringUtils.isEmpty(pointIds)) {
			jobj.put("success", "0");
			jobj.put("msg", "请选择需要返点的记录");
			renderJson(jobj.toJSONString());
			return;
		}
		if (accountId == null) {
			jobj.put("success", "0");
			jobj.put("msg", "请选择需要返点的代理");
			renderJson(jobj.toJSONString());
			return;
		}
		if (lotteryPoint == null && realPoint == null && markSixPoint == null && egamePoint == null && sportsPoint == null && chessPoint == null && esportsPoint == null) {
			jobj.put("success", "0");
			jobj.put("msg", "至少填写一个返点数");
			renderJson(jobj.toJSONString());
			return;
		}
		super.renderJson(pbssService.toProfit(pointIds, accountId, stationId, lotteryPoint, markSixPoint, realPoint, egamePoint, sportsPoint,chessPoint,esportsPoint));
	}

	@RequestMapping("/collectProxyDailyMoney")
	@ResponseBody
	public void collectProxyDailyMoney(String startDate, String endDate) {
		Date start = DateUtil.toDate(startDate);
		if (start == null) {
			renderJson(MixUtil.newHashMap("msg", "请选择开始时间"));
			return;
		}
		Date end = DateUtil.getTomorrow(endDate);
		if (end == null) {
			renderJson(MixUtil.newHashMap("msg", "请选择结束时间"));
			return;
		}
		if (end.before(start)) {
			renderJson(MixUtil.newHashMap("msg", "开始时间不能大于结束时间"));
			return;
		}
		if (end.getTime() - start.getTime() > 2678400000L) {// 时间范围大于31天
			renderJson(MixUtil.newHashMap("msg", "2个时间范围不能大于31天"));
			return;
		}
		Long stationId = StationUtil.getStationId();
		if (StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent), "on")) {
			renderJson(MixUtil.newHashMap("msg", "您的站点使用多极代理，不需要反点"));
			return;
		}
		List<Long> sIdList = new ArrayList<>();
		sIdList.add(stationId);
		pbssService.collectDailyAmount2(start, end, sIdList);
		renderSuccess();
	}
}
