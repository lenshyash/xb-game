package com.game.controller.agent.platform;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberBalanceGemRecord;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.service.ProxyMultiRebateRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;


@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH+"/balanceGemRecord")
public class MemberBalanceGemRecordController extends BaseAgentController {
	
	@Autowired
	private MemberBalanceGemRecordService balanceGemRecordService;
	@RequestMapping("/index")
	public String index(String startTime, String endTime,String account){
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", startTime == null ? curDate : startTime);
		super.getRequest().setAttribute("endTime", endTime == null ? curDate : endTime);
		super.getRequest().setAttribute("account", account);
//		super.getRequest().setAttribute("curDate", DateUtil.getCurrentDate());
		return super.goPage("/page/platform/balanceGemRecord.jsp");
	}
	@RequestMapping("/list")
	@ResponseBody
	public void list(MemberBalanceGemRecord memberBalanceGemRecord,String startTime,String endTime){
		memberBalanceGemRecord.setStationId(StationUtil.getStationId());
		Page<MemberBalanceGemRecord> page = balanceGemRecordService.getPage(memberBalanceGemRecord, startTime, endTime);
		super.render(page);
	}
	
	/**
	 * 返点回滚提交
	 * @return
	 */
//	@RequestMapping(value="/cancel",produces={"application/json;charset=UTF-8"})
//	@ResponseBody
//	public String cancle(Long betId,Long stationId){
//		int i = balanceGemRecordService.cancleProxyMultiRebate(betId,StationUtil.getStationId());
//		JSONObject result = new JSONObject();
//		if(i==1){
//			result.put("code", 0);
//			result.put("msg", "返点回滚提交成功");
//		}else{
//			result.put("code", -1);
//			result.put("msg", "返点回滚提交失败");
//		}
//		return result.toJSONString();
//	}
	
	
}
