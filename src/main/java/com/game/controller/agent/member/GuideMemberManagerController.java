package com.game.controller.agent.member;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.AccountVo;
import com.game.permission.PermissionManager;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
/**
 * 复制了MemberManagerController这个类   只修改了index add返回的jsp和list里的AccountType
 * @author Administrator
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/guideMember/manager")
public class GuideMemberManagerController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		request.setAttribute("scoreShowFlag", "on".equals(StationConfigUtil.get(StationConfig.mny_score_show)));
		request.setAttribute("memberRateFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		if (isNewPlatform()) {
			request.setAttribute("levels", MemberLevelUtil.getStationLevels());
		} else {
			request.setAttribute("levels", JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
		}
		request.setAttribute("level", $long("level"));
		request.setAttribute("moveFlag", StationConfigUtil.get(StationConfig.onoff_member_move_agent));
		request.setAttribute("score20", StationConfigUtil.get(StationConfig.onoff_score_to_zero));
		return super.goPage("/page/member/guide_member_manager.jsp");
	}

	@RequestMapping("/online")
	public String online(HttpServletRequest request) {
		request.setAttribute("scoreShowFlag", "on".equals(StationConfigUtil.get(StationConfig.mny_score_show)));
		request.setAttribute("agentShowFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_agent_participation_online)));
		Calendar c = Calendar.getInstance();
		request.setAttribute("endTime", DateUtil.toDatetimeStr(c.getTime()));
		c.add(Calendar.MONTH, -1);
		request.setAttribute("beginTime", DateUtil.toDatetimeStr(c.getTime()));
		return super.goPage("/page/member/online.jsp");
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long id = super.$long("id");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		accountService.updStatus(account);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping = { "money", "m.money", "score", "a.score", "createDatetime", "a.create_datetime",
			"lastLoginDatetime", "a.last_login_datetime", "remark", "a.remark", "online", "a.online", "accountStatus",
			"a.account_status" })
	public void list(Integer depositStatus, String moneyMin, String unloginDay) {
		String account = super.$("account");
		String agentName = super.$("agentName");
		String userName = super.$("userName");
		String phone = super.$("phone");
		String qq = super.$("qq");
		String email = super.$("email");
		String ip = super.$("lastLoginIp");
		Long levelGroup = super.$long("levelGroup");
		String cardNo = super.$("cardNo");
		String startDate = super.$("startDate");
		String endDate = super.$("endDate");
		String regIp = super.$("regIp");
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setAgentName(agentName);
		accountVo.setUserName(userName);
		accountVo.setQq(qq);
		accountVo.setEmail(email);
		accountVo.setPhone(phone);
		accountVo.setCardNo(cardNo);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_GUIDE);
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setLastLoginIp(ip);
		accountVo.setLevelGroup(levelGroup);
		accountVo.setRegisterUrl(StringUtil.trim2Null(super.$("registerUrl")));
		accountVo.setBegin(DateUtil.toDate(startDate));
		accountVo.setEnd(DateUtil.getTomorrow(endDate));
		accountVo.setRegIp(regIp);
		accountVo.setDepositStatus(depositStatus);
		accountVo.setMoneyMin(BigDecimalUtil.toBigDecimal(moneyMin));
		if (StringUtils.isNotEmpty(unloginDay)) {
			accountVo.setUnloginDay(NumberUtils.toInt(unloginDay.trim()));
		}
		super.render(accountService.getAccountPage(accountVo));
	}

	@ResponseBody
	@RequestMapping("/ollist")
	public void ollist() {
		String account = super.$("account");
		String agentName = super.$("agentName");
		String sortOrder = super.$("sortOrder");
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setAgentName(agentName);
		String flag =  StationConfigUtil.get(StationConfig.onoff_agent_participation_online);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
		if(flag!=null && flag.equals("on")){
			accountVo.setAccountType(null);
		}
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setOnline(SysAccount.ONLINE_FLAG_ON);
		accountVo.setSortOrder(sortOrder);
		Page<Map> page = accountService.getOnlineAccountPage(accountVo);
		List<Map> map = new  ArrayList<Map>();
		super.render(page);
	}

	@ResponseBody
	@RequestMapping("/load")
	public void load() {
		Long id = super.$long("id");
		super.renderJson(accountService.loadOnlineMember(id));
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		map.put("memberRateFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		map.put("levels", MemberLevelUtil.getStationLevels());
		return super.prepareLayout("/newpage/member/guide_member_add.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/multidata")
	public void multidata() {
		String account = super.$("agentName");
		AccountVo avo = new AccountVo();
		avo.setAccount(account);
		super.renderJson(accountService.getAgentMultiData(avo));
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		SysAccount user = (SysAccount) UserUtil.getCurrentUser();
		SysAccount account = accountService.getOne(id, StationUtil.getStationId());
		SysAccountInfo accountInfo = accountService.getAccountInfo(id);
		boolean modifyPerm = PermissionManager.getAgentPermission().contains("/agent/member/manager/advanced.do");
		boolean onlyReadUserName = PermissionManager.getAgentPermission().contains("/agent/member/manager/onlyReadUserName.do");
		if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
			modifyPerm = false;
		}
		if (!modifyPerm) {
			accountInfo.setEmail("");
			accountInfo.setQq("");
			accountInfo.setPhone("");
			accountInfo.setWechat("");
			accountInfo.setBankAddress("");
			accountInfo.setBankName("");
			accountInfo.setCardNo("");
			accountInfo.setCardNoStatus(0l);
		}
		map.put("modifyPerm", modifyPerm);
		map.put("onlyReadUserName", onlyReadUserName);

		map.put("member", account);
		map.put("memberInfo", accountInfo);
		boolean memberRateFlag = "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random));
		if(memberRateFlag) {
			map.put("memberRateFlag", memberRateFlag);
			AccountVo av = new AccountVo();
			av.setId(id);
			map.put("multiAgent1", accountService.getAgentMultiData(av));
		}
		map.put("levels", MemberLevelUtil.getStationLevels());
		if (account.getParentNames() != null) {
			String parentNames = account.getParentNames();
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				map.put("parentNames", parentNames);
			}
		}
		return super.prepareLayout("/newpage/member/member_modify.jsp");
	}

	@Permission(CheckType.FUNCTION)
	@ResponseBody
	@RequestMapping("/advanced")
	public void modifyInfo(Long id) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		super.renderJson(JsonUtil.toJson(accountService.getAccountInfo(id)));
	}

	@RequestMapping("/modifyPwd")
	public String modifyPwd(Map<String, Object> map, Long id) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		map.put("member", accountService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/member/member_modify_pwd.jsp");
	}

	@ResponseBody
	@RequestMapping("/save")
	public void save() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long id = super.$long("id");
		String account = super.$("account");
		String bankName = super.$("bankName");
		String agentName = super.$("agentName");
		Long accountStatus = super.$long("accountStatus");
		Long accountType= super.$long("accountType");
		String cardNo = super.$("cardNo");
		String userName = super.$("userName");
		String phone = super.$("phone");
		String qq = super.$("qq");
		String email = super.$("email");
		String wechat = super.$("wechat");
		String bankAddress = super.$("bankAddress");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		Long levelGroup = super.$long("levelGroup");
		Long cardNoStatus = super.$long("cardNoStatus");
		Long modifyType = super.$cl("modifyType");
		Long moveFlag = super.$long("moveFlag");
		String remark = super.$("remark");
		String rate = super.$("rate");
		Long redpacketLimit = super.$long("redpacketLimit");
		String chatToken = super.$("chatToken");
		AccountVo accountVo = new AccountVo();
		accountVo.setId(id);
		accountVo.setAccount(account);
		accountVo.setAgentName(agentName);
		accountVo.setStatus(accountStatus);
		accountVo.setCardNo(cardNo);
		accountVo.setBankName(bankName);
		accountVo.setPhone(phone);
		accountVo.setQq(qq);
		accountVo.setEmail(email);
		accountVo.setBankAddress(bankAddress);
		accountVo.setPwd(pwd);
		accountVo.setWechat(wechat);
		accountVo.setRpwd(rpwd);
		accountVo.setCardNoStatus(cardNoStatus);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
		accountVo.setAgentId(StationUtil.getStation().getAgentId());
		accountVo.setLevelGroup(levelGroup);
		accountVo.setMoveFlag(moveFlag);
		accountVo.setRemark(remark);
		accountVo.setAccountType(accountType);
//		accountVo.setRebateNum(StringUtil.toBigDecimal(rate));
		accountVo.setDynamicRate(StringUtil.toBigDecimal(rate));
		accountVo.setRedpacketLimit(redpacketLimit);
		accountVo.setChatToken(chatToken);
		boolean onlyReadUserName = PermissionManager.getAgentPermission().contains("/agent/member/manager/onlyReadUserName.do");
		if(onlyReadUserName){
			accountVo.setUserName(userName);
		}else{
			SysAccountInfo info=accountService.getAccountInfo(id);
			if(info!=null) {
				accountVo.setUserName(info.getUserName());
			}
		}
		
		if(!StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_MEMBER) 
				&& !StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
			throw new GenericException("会员类型错误");
		}
		
		accountService.saveAccount(accountVo, modifyType);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		accountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		accountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/checkBankCard")
	@ResponseBody
	public void checkBankCard() {
		Long userId = super.$long("id");
		String bankCard = super.$("cardNo");
		super.renderJson(accountService.checkBankCard(userId, bankCard));
	}

	@RequestMapping(value = "/forced")
	@ResponseBody
	public void forced() {
		Long id = super.$long("id");
		accountService.forcedOffLine(id);
		super.renderSuccess();
	}

	@RequestMapping("/view")
	public String viewDetail(Long id, Model model) {
		String ag = StationConfigUtil.get(StationConfig.onoff_ag_game);
		String bbin = StationConfigUtil.get(StationConfig.onoff_bbin_game);
		String mg = StationConfigUtil.get(StationConfig.onoff_mg_game);
		String pt = StationConfigUtil.get(StationConfig.onoff_pt_game);
		String qt = StationConfigUtil.get(StationConfig.onoff_qt_game);
		String ab = StationConfigUtil.get(StationConfig.onoff_ab_game);
		String og = StationConfigUtil.get(StationConfig.onoff_og_game);
		String ds = StationConfigUtil.get(StationConfig.onoff_ds_game);
		String isb = StationConfigUtil.get(StationConfig.onoff_isb_game);
		String ttg = StationConfigUtil.get(StationConfig.onoff_ttg_game);
		String mw = StationConfigUtil.get(StationConfig.onoff_mw_game);
		String cq9 = StationConfigUtil.get(StationConfig.onoff_cq9_game);
		String jdb = StationConfigUtil.get(StationConfig.onoff_jdb_game);
        String m8 = StationConfigUtil.get(StationConfig.onoff_m8_game);
        String ibc = StationConfigUtil.get(StationConfig.onoff_ibc_game);
        String m8h = StationConfigUtil.get(StationConfig.onoff_m8h_game);
        String bg = StationConfigUtil.get(StationConfig.onoff_bg_game);
        String vr = StationConfigUtil.get(StationConfig.onoff_vr_game);
        String ky = StationConfigUtil.get(StationConfig.onoff_ky_game);
        String ebet = StationConfigUtil.get(StationConfig.onoff_ebet_game);

		boolean isAG = StringUtils.equals(ag, "on");
		boolean isBBIN = StringUtils.equals(bbin, "on");
		boolean isMG = StringUtils.equals(mg, "on");
		boolean isPT = StringUtils.equals(pt, "on");
		boolean isQT = StringUtils.equals(qt, "on");
		boolean isAB = StringUtils.equals(ab, "on");
		boolean isOG = StringUtils.equals(og, "on");
		boolean isDS = StringUtils.equals(ds, "on");
		boolean isCQ9 = StringUtils.equals(cq9, "on");
		boolean isJDB = StringUtils.equals(jdb, "on");
		boolean isISB = StringUtils.equals(isb, "on");
		boolean isTTG = StringUtils.equals(ttg, "on");
		boolean isMW = StringUtils.equals(mw, "on");
        boolean isM8 = StringUtils.equals(m8, "on");
        boolean isIBC = StringUtils.equals(ibc, "on");
        boolean isM8H = StringUtils.equals(m8h, "on");
        boolean isBg = StringUtils.equals(bg, "on");
        boolean isVR = StringUtils.equals(vr, "on");
        boolean isKY = StringUtils.equals(ky, "on");
        boolean isEBET = StringUtils.equals(ebet, "on");

		model.addAttribute("isAG", isAG);
		model.addAttribute("isBBIN", isBBIN);
		model.addAttribute("isMG", isMG);

		model.addAttribute("isPT", isPT);
		model.addAttribute("isQT", isQT);
		model.addAttribute("isAB", isAB);
		model.addAttribute("isOG", isOG);
		model.addAttribute("isDS", isDS);
		model.addAttribute("isCQ9", isCQ9);
		model.addAttribute("isJDB", isJDB);
		model.addAttribute("isISB", isISB);
		model.addAttribute("isTTG", isTTG);
		model.addAttribute("isMW", isMW);
        model.addAttribute("isM8", isM8);
        model.addAttribute("isM8H", isM8H);
        model.addAttribute("isIBC", isIBC);
        model.addAttribute("isBg", isBg);
        model.addAttribute("isVR", isVR);
        model.addAttribute("isKY", isKY);
        model.addAttribute("isEBET", isEBET);
        
		if (isAG || isBBIN || isMG || isPT || isQT || isAB || isOG || isDS || isCQ9 || isJDB|| isISB|| isMW|| isTTG || isM8 || isIBC || isM8H || isBg || isVR|| isKY || isEBET) {
			model.addAttribute("third", true);
		}

		if (id == null || id <= 0) {
			throw new ParameterException("账户不存在");
		}
		Long stationId = StationUtil.getStationId();
		Map map = accountService.getAccountById(id, stationId);
		if (map == null) {
			throw new ParameterException("账户不存在");
		}
		if (map.containsKey("parentNames")) {
			String parentNames = (String) map.get("parentNames");
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				model.addAttribute("parentNames", parentNames);
			}
		}
		// map.put("cardNo", yincang((String) map.get("cardNo"), 4));
		// map.put("userName", yincang((String) map.get("userName"), 1));
		boolean viewPerm = PermissionManager.getAgentPermission().contains("/agent/member/manager/viewAdvanced.do");
		if (!viewPerm) {
			map.put("email", "");
			map.put("qq", "");
			map.put("phone", "");
			map.put("wechat", "");
		}
		model.addAttribute("viewPerm", viewPerm);
		model.addAttribute("map", map);
		model.addAttribute("levels", MemberLevelUtil.getStationLevels());
		return super.goPage("/page/member/viewDetail.jsp");
	}
	
	@RequestMapping("/movepage")
	public String movepage(Map<String, Object> map, Long id) {
		return super.prepareLayout("/newpage/member/member_move_agent.jsp");
	}
	
	@RequestMapping(value = "/moveAgent")
	@ResponseBody
	public void moveAgent() {
		String agentName = super.$("agentName");
		String members = super.$("members");
		String result = accountService.batchMoveAgent(StationUtil.getStationId(),agentName, members,SysAccount.ACCOUNT_PLATFORM_MEMBER);
		if(StringUtil.isEmpty(result)) {
			super.renderSuccess();
		}else {
			super.renderFailure(result);
		}
	}

	private String yincang(String str, int baoliuHouJiwei) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}

	@RequestMapping(value = "/scoreToZero")
	@ResponseBody
	public void scoreToZero() {
		accountService.scoreToZero(StationUtil.getStationId());
		super.renderSuccess();
	}
}
