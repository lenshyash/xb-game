package com.game.controller.agent.member;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberPermission;
import com.game.service.MemberPermissionService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/member/permission")
public class MemberPermissionController extends BaseAgentController {
	
	@Autowired
	private MemberPermissionService permissionService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String real = StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le);
		request.setAttribute("realgame","on".equals(real) );
		return super.goPage("page/member/permission.jsp");
	}
	
	
	@ResponseBody
	@RequestMapping("/getPermission")
	public void getPermission(){
		String account = $c("account","请输入会员账号").trim();
		MemberPermission mp = permissionService.getPermission(account);
		super.renderJson(mp);
	}
	@ResponseBody
	@RequestMapping("/savePermissionNew")
	public void savePermissionNew(MemberPermission mp ){
		if(mp == null){
			throw new GenericException("非法操作");
		}
		super.isNotNull(mp.getAccountId(), "非法操作");
		mp.setStationId(StationUtil.getStationId());
		permissionService.savePermission(mp);
		super.renderSuccess();
	}
	@ResponseBody
	@RequestMapping("/savePermission")
	public void savePermission(){
		String json = $c("json","非法操作");
		MemberPermission mp = null;
		try{
			mp = JSONObject.parseObject(json, MemberPermission.class);
		}catch(Exception e){
			throw new GenericException("数据格式有误");
		}
		if(mp == null){
			throw new GenericException("非法操作");
		}
		mp.setStationId(StationUtil.getStationId());
		super.isNotNull(mp.getAccountId(), "非法操作");
		//super.isNotNull(mp.getRealGameTransfer(), "请选填真人转账");
		permissionService.savePermission(mp);
		super.renderSuccess();
	}
}
