package com.game.controller.agent.member;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.service.SysAccountService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/member/search")
public class MemberSearchController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		request.setAttribute("scoreShowFlag", "on".equals(StationConfigUtil.get(StationConfig.mny_score_show)));
		if (isNewPlatform()) {
			request.setAttribute("levels", MemberLevelUtil.getStationLevels());
		} else {
			request.setAttribute("levels", JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
		}
		return super.goPage("/page/member/search.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		if (StringUtil.isNotEmpty(account)) {
			AccountVo accountVo = new AccountVo();
			accountVo.setAccount(account);
			accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
			accountVo.setStationId(StationUtil.getStationId());
			super.render(accountService.getAccountPage(accountVo));
		} else {
			super.render(new Page());
		}
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		accountService.updStatus(account);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/save")
	public void save() {
		Long id = super.$long("id");
		String bankName = super.$("bankName");
		Long accountStatus = super.$long("accountStatus");
		String cardNo = super.$("cardNo");
		String userName = super.$("userName");
		String phone = super.$("phone");
		String qq = super.$("qq");
		String email = super.$("email");
		String agentName = super.$("agentName");
		String bankAddress = super.$("bankAddress");

		AccountVo accountVo = new AccountVo();
		accountVo.setId(id);
		accountVo.setBankName(bankName);
		accountVo.setStatus(accountStatus);
		accountVo.setCardNo(cardNo);
		accountVo.setBankName(bankName);
		accountVo.setUserName(userName);
		accountVo.setPhone(phone);
		accountVo.setQq(qq);
		accountVo.setEmail(email);
		accountVo.setBankAddress(bankAddress);
		accountVo.setAgentName(agentName);
		accountService.saveAccount(accountVo);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		accountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		accountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}
	
	@RequestMapping(value = "/checkBankCard")
	@ResponseBody
	public void checkBankCard() {
		Long userId = super.$long("id");
		String bankCard = super.$("cardNo");
		super.renderJson(accountService.checkBankCard(userId,bankCard));
	}
}
