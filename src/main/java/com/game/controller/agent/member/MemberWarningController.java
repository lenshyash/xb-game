package com.game.controller.agent.member;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bouncycastle.asn1.ocsp.Request;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysAccountWarning;
import com.game.model.vo.AccountVo;
import com.game.model.vo.AccountWarningVo;
import com.game.permission.PermissionManager;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountService;
import com.game.service.SysAccountWarningService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/member/warning")
public class MemberWarningController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;
	
	@Autowired
	private SysAccountWarningService accountWarningService;

	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		return super.goPage("/page/member/warning.jsp");
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long id = super.$long("id");
		Long accountStatus = super.$long("status");
		Long accountId = super.$long("accountId");

		SysAccountWarning account = new SysAccountWarning();
		account.setId(id);
		account.setStatus(accountStatus);
		account.setAccountId(accountId);
//		accountWarningService.updStatus(account);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		Long type = super.$long("type");
		Long level = super.$long("level");
		Long status = super.$long("status");
		String startDate = super.$("startDate");
		String endDate = super.$("endDate");
		AccountWarningVo accountVo = new AccountWarningVo();
		accountVo.setAccount(account);
		accountVo.setType(type);
		accountVo.setLevel(level);
		accountVo.setStatus(status);
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setBegin(DateUtil.toDate(startDate));
		accountVo.setEnd(DateUtil.getTomorrow(endDate));
		Page<Map> page = accountWarningService.getPage(accountVo);
		super.render(page);
	}
}
