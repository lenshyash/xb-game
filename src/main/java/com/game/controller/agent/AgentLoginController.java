package com.game.controller.agent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.AgentLotteryQuoto;
import com.game.model.SysAccount;
import com.game.model.SysStationDomain;
import com.game.model.vo.AgentNavVo;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentLotteryQuotoService;
import com.game.service.AgentMenuService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyDrawRecordService;
import com.game.service.SysAccountService;
import com.game.service.SysAccountWarningService;
import com.game.service.SysAnnouncementService;
import com.game.service.SysStationDomainService;
import com.game.third.service.RealGameTransLogService;
import com.game.user.online.OnlineManager;
import com.game.util.ErrorUtil;
import com.game.util.SecurityCardUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.VerifyCodeUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH)
public class AgentLoginController extends BaseAgentController {

	@Autowired
	private SysAccountService sysAccountService;

	@Autowired
	private AgentMenuService agentMenuService;

	@Autowired
	private MnyDrawRecordService mdrService;

	@Autowired
	private MnyComRecordService mcrService;
	@Autowired
	private SysAnnouncementService announcementService;
	@Autowired
	private RealGameTransLogService realGameTransLogService;
	@Autowired
	private SysStationDomainService stationDomainService;
	
	@Autowired
	private SysAccountWarningService accountWarningService;

	@Autowired
	private AgentLotteryQuotoService agentLotteryQuotoService;
	
	@NotNeedLogin
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		if (SysUtil.isLogin()) {
			getRequest().setAttribute("_home_ams", JSON.toJSON(announcementService.getLastAnnouncement(10)));
			getRequest().setAttribute("warnOnoff","off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning)));
			
			getRequest().setAttribute("lotteryQuotoOnoff","on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_lottery_quoto)));
			return super.goPage("/page/home.jsp");
		} else {
			getRequest().setAttribute("showOtpKey", StationConfigUtil.get(StationConfig.onoff_agent_login_otp_key_btn));
			getRequest().setAttribute("googleFlag", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_bg_login_google_verify_code)));
			return super.goPage("/index.jsp");
		}
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/login")
	public void login() {
		String account = $c("account").trim().toLowerCase();
		String password = $c("password");
		String verifyCode = $("verifyCode");
		String otpPwd = $("otpPwd");
		String googleCode = $("googleCode");
		sysAccountService.doLoginForAgent(account, password,otpPwd, verifyCode,googleCode);
		if (SecurityCardUtil.reponseCardInfo()) {
			return;
		}
		renderSuccess();
	}

	@NotNeedLogin
	@RequestMapping("/loginDialog")
	public String loginDialog() {
		getRequest().setAttribute("googleFlag", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_bg_login_google_verify_code)));
		return super.goPage("/newpage/loginDialog.jsp");
	}

	@RequestMapping("/modifyPwd")
	public String modifyPwd(HttpServletRequest request) {
		request.setAttribute("username", SysUtil.getCurrentUser().getLoginAccount());
		return super.goPage("/newpage/modifyPwd.jsp");
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/login4Card")
	public void login4Card() {
		String code = $c("code", "请输入密保卡").trim();
		sysAccountService.login4Card(code);
		renderSuccess();
	}

	@NotNeedLogin
	@RequestMapping("/logout")
	public String logout() {
		sysAccountService.doLoginOut();
		getRequest().setAttribute("showOtpKey", StationConfigUtil.get(StationConfig.onoff_agent_login_otp_key_btn));
		getRequest().setAttribute("googleFlag", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_bg_login_google_verify_code)));
		return super.goPage("/index.jsp");
	}

	@ResponseBody
	@RequestMapping("/nav1")
	public void nav1(HttpServletRequest request) {
		Long groupId = 0L;
		if (!UserUtil.isSuperAgent()) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			groupId = user.getGroupId();
		}
		renderJson(agentMenuService.getNavMenuVo(groupId));
	}

	@ResponseBody
	@RequestMapping("/checknavcount")
	public void checknavcount() {
		AgentNavVo nav = new AgentNavVo();
		long stationId = StationUtil.getStationId();
		nav.setDepositCount(mcrService.getCountOfUntreated(stationId));
		nav.setWithdrawCount(mdrService.getCountOfUntreated(stationId));
		nav.setOnlineUser(OnlineManager.getOnlineCount(stationId, null));
		List<Map> liveQuoto = realGameTransLogService.getLiveQuoto();
		
		if("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning))){
			//三集群更新GC overhead limit exceeded
//			nav.setWarningUser(accountWarningService.getCountOfUntreated(stationId));
			nav.setWarningUser(sysAccountService.getWarningCount(stationId));
		}

		//平台彩票额度
		boolean onoffQuoto = "on".equals(StationConfigUtil.get(stationId, StationConfig.onoff_agent_lottery_quoto));
		if(onoffQuoto) {
			BigDecimal quoto = BigDecimal.ZERO;
			if(StringUtil.isNotEmpty(StationConfigUtil.get(stationId, StationConfig.agent_lottery_quoto))) {
				quoto = new BigDecimal(StationConfigUtil.get(stationId, StationConfig.agent_lottery_quoto));
			}
			BigDecimal buyMoney = BigDecimal.ZERO;
			AgentLotteryQuoto one = agentLotteryQuotoService.getOneByStationId(stationId);
			if(one!=null) {
				buyMoney = one.getBuyMoney();
			}
			Integer integer = new Integer(quoto.subtract(buyMoney).setScale(0, BigDecimal.ROUND_DOWN).toString());
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", "彩票额度");
			map.put("value", MixUtil.newHashMap("remainingQuota", integer));
			if(liveQuoto==null) {
				liveQuoto = new ArrayList<>();
			}
			liveQuoto.add(map);
		}
		
		nav.setQuotos(liveQuoto);
		super.renderJson(nav);
	}

	@ResponseBody
	@RequestMapping("/getNotice")
	public void getNotice() {
		super.renderJson(announcementService.getAgentNotices(StationUtil.getStationId()));
	}

	@RequestMapping("/chatlist")
	@ResponseBody
	public void chatlist() {
		Map<String,Object> page = new HashMap<>();
		if("on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat))){
			QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
			String key = Contants.STATION_CHAT_LIST+StationUtil.getStationId()+":"+webParam.getPageNo()+":"+webParam.getPageSize();
			String json  = RedisAPI.getCache(key);
			if(StringUtil.isEmpty(json)){
				json = new RequestProxy() {
					
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> params = getBasicParams();
						if (params == null) {
							return params;
						}
						return params;
					};
				}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/api/system/getServerAgents?",PostType.POST, true);
				
				if(StringUtil.isNotEmpty(json)){
					JSONObject temp = JSONObject.parseObject(json);
					if(temp.getInteger("code")==1 && temp.getJSONObject("data")!=null){
						RedisAPI.addCache(key,json, Contants.STATION_CHAT_LIST_TIMEOUT);
					}
				}
			}
			//获取站点配置聊天室域名
			List<SysStationDomain> list = stationDomainService.getListByStationType(StationUtil.getStationId(), SysStationDomain.TYPE_CHAT);
			if(list!=null && list.size()>0){
				page.put("domain",list.get(0).getDomain());
			}else{
				page.put("domain",StationConfigUtil.getSys(StationConfig.sys_chat_url));
			}
			JSONObject jsonObj = JSONObject.parseObject(json);
			JSONObject data = jsonObj.getJSONObject("data");
			page.put("rows", data.get("content"));
			page.put("total", data.getInteger("totalElements"));
		}
		super.renderJson(page);
	}
	public List<NameValuePair> getBasicParams() {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("authToken", StationConfigUtil.getSys(StationConfig.sys_real_center_token)));
		params.add(new BasicNameValuePair("page", webParam.getPageNo() + ""));
		params.add(new BasicNameValuePair("quantity", webParam.getPageSize() + ""));
		params.add(new BasicNameValuePair("stationId",StationUtil.getStationId()+""));
		params.add(new BasicNameValuePair("platformId", super.$("platformId")));
		return params;
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/verifycode")
	public void verifycode() throws Exception {
		VerifyCodeUtil.createVerifyCode(true, null);
	}

	@NotNeedLogin
	@RequestMapping("/error")
	public String error(Integer code, HttpServletRequest request) {
		ErrorUtil.error(code, request);
		return "/common/template/erro/erro.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/loginError")
	public String loginError(String errorCode, HttpServletRequest request) {
		request.setAttribute("errorCode", errorCode);
		request.setAttribute("homepage", "/agent/index.do");
		return "/common/handler/loginerror.jsp";
	}

}
