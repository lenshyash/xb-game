package com.game.controller.agent.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.LogType;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysLog;
import com.game.model.vo.SysLogVo;
import com.game.service.SysLogService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/oplog")
public class AgentOplogController extends BaseAgentController {

	@Autowired
	SysLogService logService;

	@RequestMapping("/index")
	public String index(String begin,String end,Map<String,Object> map) {
		String curDate = DateUtil.getCurrentDate();
		map.put("startTime", begin==null?curDate:begin);
		map.put("endTime", end==null?curDate:end);
		return super.goPage("/page/system/oplog.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		Long type = super.$long("type");
		String account = super.$("account");
		String content = super.$("content");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		SysLogVo slvo = new SysLogVo();
		slvo.setStationId(StationUtil.getStationId());
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDatetime(begin));
		slvo.setEnd(DateUtil.toDatetime(end));
		slvo.setType(type);
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setIp(ip);
		slvo.setContent(content);
		super.render(logService.getLogsPage(slvo));
	}

	@RequestMapping("/typecombo")
	@ResponseBody
	public void typecombo() {
		List<Map> lts = new ArrayList<Map>();
		LogType[] types = LogType.values();
		for (int i = 0; i < types.length; i++) {
			if (StringUtil.equals(types[i].getPlatform(), SysLog.PLATFORM_PLATFORM)) {
				lts.add(MixUtil.newHashMap("id", types[i].getType(), "name", types[i].getDesc(), "platform",
						types[i].getPlatform()));
			}
		}
		super.renderJson(JsonUtil.toJson(lts));
	}@RequestMapping("/detail")
	public String detail(Map<String,Object> map,Long id) {
		SysLog log=logService.getOne(id,StationUtil.getStationId());
		map.put("log",log);
		for(LogType lt:LogType.values()){
			if(Objects.equals(lt.getType(),log.getLogType())){
				map.put("typeName",lt.getDesc());
			}
		}
		return super.prepareLayout("/newpage/system/oplog_detail.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/export")
	public void export() {
		Long type = super.$long("type");
		String account = super.$("account");
		String content = super.$("content");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		SysLogVo slvo = new SysLogVo();
		slvo.setStationId(StationUtil.getStationId());
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDatetime(begin));
		slvo.setEnd(DateUtil.toDatetime(end));
		slvo.setType(type);
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setIp(ip);
		slvo.setContent(content);
		logService.exportOperationLog(slvo);
	}
}
