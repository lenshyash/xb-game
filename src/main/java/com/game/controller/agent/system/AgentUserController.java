package com.game.controller.agent.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentUserGroup;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.service.AgentUserGroupService;
import com.game.service.SysAccountService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/user")
public class AgentUserController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;
	@Autowired
	private AgentUserGroupService grouService;

	@ResponseBody
	@RequestMapping("/newList")
	public void newList(AccountVo accountVo) {
		super.render(accountService.getStationAgentList(accountVo));
	}
	
	@CheckLastLoginIp
	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		map.put("groups", grouService.getGroupCombo(false));
		return super.prepareLayout("/newpage/system/user_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("groups", grouService.getGroupCombo(false));
		map.put("agentUser", accountService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/user_modify.jsp");
	}

	@RequestMapping("/modifyPwd")
	public String modifyPwd(Map<String, Object> map, Long id) {
		map.put("agentUser", accountService.getFromCache(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/user_modify_pwd.jsp");
	}

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		List<AgentUserGroup> groupCombo = grouService.getGroupCombo(false);
		map.put("groups", JSON.toJSON(groupCombo));
		map.put("groups2", groupCombo);
		return super.goPage("/page/system/usermanager.jsp");
	}

	@RequestMapping(value = "/updateGroup")
	@ResponseBody
	public void updateGroup(Long id, Long groupId,String remark) {
		accountService.updateGroup(id, StationUtil.getStationId(), groupId,remark);
		super.renderSuccess();
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(SysAccount user) {
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");

		accountService.saveAgentUser(user, pwd, rpwd);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		accountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@Permission(CheckType.OPEN)
	@RequestMapping(value = "/updloginpwd")
	@ResponseBody
	public void updloginpwd() {
		String opwd = super.$("opwd");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		accountService.updCurUserPwd(opwd, pwd, rpwd, SysAccount.PASSWORD_TYPE_LOGIN);
		super.renderSuccess();
	}

	/**
	 * 状态改变
	 * 
	 * @param status
	 */
	@RequestMapping(value = "/updStatus")
	@ResponseBody
	public void closeOrOpen(SysAccount at) {
		accountService.updStatus(at);
		super.renderSuccess();
	}
	/**
	 * 删除
	 */
	@CheckLastLoginIp
	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete() {
		Long id = super.$long("id");
		super.isNotNull(id, "ID不能为空!");
		accountService.deleteById(id);
		super.renderSuccess();
	}
	
	
}
