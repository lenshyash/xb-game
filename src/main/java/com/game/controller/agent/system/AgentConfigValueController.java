package com.game.controller.agent.system;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentBaseConfigValue;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.service.AgentBaseConfigService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/baseconfig")
public class AgentConfigValueController extends BaseAgentController {

	@Autowired
	AgentBaseConfigService agentBaseConfigService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/baseconfig.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		super.renderJson(
				MixUtil.newHashMap("confs", agentBaseConfigService.getStationConfigs(StationUtil.getStationId())));
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/save")
	public void save(Long pk,String value) {
		agentBaseConfigService.saveConfigValue(pk,value,StationUtil.getStationId());
		super.renderSuccess();
	}
}
