package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysStationFolderUrl;
import com.game.model.platform.AgentArticle;
import com.game.service.AgentArticleService;
import com.game.service.SysStationDomainService;
import com.game.service.SysStationFolderUrlService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.WebUtil;

/**
 * 多模板app下载链接管理
 * @author Tassel
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/applink")
public class AgentAppLinkManageController extends BaseAgentController {

	@Autowired
	private SysStationDomainService domainService;
	@Autowired
	private SysStationFolderUrlService folderUrlService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/applink.jsp");
	}
	/**
	 * 获取记录列表
	 * @param folder
	 */
	@RequestMapping("/list")
	@ResponseBody
	public void list(String folder) {
		Long stationId = StationUtil.getStationId();
		super.render(folderUrlService.page(stationId, folder));
	}
	/**
	 * 状态修改
	 */
	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Integer modelStatus, Long id) {
		Long stationId = StationUtil.getStationId();
		folderUrlService.openCloseH(modelStatus, id, stationId);
		super.renderSuccess();
	}
	/**
	 * 跳转新增页面
	 * @return
	 */
	@RequestMapping("/add")
	public String add() {
		Calendar c= Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		super.getRequest().setAttribute("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 12);
		c.add(Calendar.DAY_OF_MONTH, -1);
		super.getRequest().setAttribute("endDate", DateUtil.toDateStr(c.getTime()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/applink_add.jsp");
	}
	/**
	 * 保存记录
	 * @param aacle
	 */
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(SysStationFolderUrl aacle) {
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
		folderUrlService.addSave(aacle);
		super.renderSuccess();
	}
	/**
	 * 跳转修改界面
	 * @param map
	 * @param id
	 * @return
	 */
	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("link", folderUrlService.getOne(id));
		return super.prepareLayout("/newpage/system/applink_modify.jsp");
	}
	/**
	 * 修改记录
	 * @param aacle
	 */
	@RequestMapping("/eidtSaveNew")
	@ResponseBody
	public void eidtSaveNew(SysStationFolderUrl aacle) {
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
		folderUrlService.eidtSave(aacle);
		super.renderSuccess();
	}
	/**
	 * 删除记录
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		folderUrlService.delete(id, stationId);
		super.renderSuccess();
	}
}
