package com.game.controller.agent.system;

import java.util.Map;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentSignRule;
import com.game.service.SignService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/sign")
public class AgentSignSettingController extends BaseAgentController {

	@Autowired
	private SignService signService;

	@RequestMapping("/index")
	public String index() {
		if (isNewPlatform()) {
			return super.prepareLayout("/newpage/system/signSettingList.jsp");
		}
		return super.goPage("/page/system/signSetting.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		Page page = signService.getRulePage();
		super.render(page);
	}

	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/signSetting_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("rule", signService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/signSetting_modify.jsp");
	}

	@ResponseBody
	@RequestMapping("/delRule")
	public void delRule() {
		long id = $cl("id", "非法请求");
		signService.delRule(id);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/saveRule")
	public void saveRule() {
		Long id = $long("id");
		long score = $cl("score", "非法请求");
		long days = $cl("days", "非法请求");
		long signType = $cl("signType", "非法请求");
		long signClear = $cl("signClear", "非法请求");
		AgentSignRule rule = new AgentSignRule();
		rule.setId(id);
		rule.setScore(score);
		rule.setDays(days);
		rule.setSignType(signType);
		rule.setSignClear(signClear);
		signService.saveRule(rule);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/getSignRecords")
	public void getSignRecords() {
		Page page = signService.getSignRecords();
		super.render(page);
	}
}
