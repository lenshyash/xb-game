package com.game.controller.agent.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysStationDomain;
import com.game.service.MemberLevelService;
import com.game.service.SysAccountService;
import com.game.service.SysStationDomainService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/im")
public class AgentImController extends BaseAgentController {

	@Autowired
	private MemberLevelService levelService;

	@Autowired
	private SysAccountService accountService;
	
	@Autowired
	private SysStationDomainService stationDomainService;
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/agent_im.jsp");
	}
	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Map<String,Object> page = new HashMap<>();
		if("on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_im_chat))){
			QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
			String key = Contants.STATION_IM_LIST+StationUtil.getStationId()+":"+webParam.getPageNo()+":"+webParam.getPageSize();
			String json  = RedisAPI.getCache(key);
			if(StringUtil.isEmpty(json)){
				json = new RequestProxy() {
					
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> params = getBasicParams();
						if (params == null) {
							return params;
						}
						return params;
					};
				}.doRequest(StationConfigUtil.getSys(StationConfig.sys_im_url) + "/api/system/getServerChats?",PostType.POST, true);
				
				if(StringUtil.isNotEmpty(json)){
					JSONObject temp = JSONObject.parseObject(json);
					if(temp.getInteger("code")==1 && temp.getJSONObject("data")!=null){
						RedisAPI.addCache(key,json, Contants.STATION_CHAT_LIST_TIMEOUT);
					}
				}
			}
			//获取站点配置聊天室域名
			List<SysStationDomain> list = stationDomainService.getListByStationType(StationUtil.getStationId(), SysStationDomain.TYPE_CHAT);
			if(list!=null && list.size()>0){
				page.put("domain",list.get(0).getDomain());
			}else{
				page.put("domain",StationConfigUtil.getSys(StationConfig.sys_chat_url));
			}
			JSONObject jsonObj = JSONObject.parseObject(json);
			JSONObject data = jsonObj.getJSONObject("data");
			page.put("rows", data.get("content"));
			page.put("total", data.getInteger("totalElements"));
		}
		super.renderJson(page);
	}
	public List<NameValuePair> getBasicParams() {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("authToken", StationConfigUtil.getSys(StationConfig.sys_real_center_token)));
		params.add(new BasicNameValuePair("page", webParam.getPageNo() + ""));
		params.add(new BasicNameValuePair("quantity", webParam.getPageSize() + ""));
		params.add(new BasicNameValuePair("stationId",StationUtil.getStationId()+""));
		params.add(new BasicNameValuePair("chatId", super.$("chatId")));
		return params;
	}

	@RequestMapping("/removpg")
	public String modify(Map<String, Object> map,String imChatId) {
		map.put("accountStr", accountService.getAccountStrByChatId(imChatId, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/im_remove.jsp");
	}

	@RequestMapping("/importpg")
	public String importpg(Map<String, Object> map,String imChatId) {
//		map.put("level", levelService.getOne(id, StationUtil.getStationId()));
		map.put("imChatId", imChatId);
		map.put("accountStr", accountService.getAccountStrByChatId(imChatId, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/im_import.jsp");
	}

	@RequestMapping("/import")
	@ResponseBody
	public void importMember(final String imChatId) {
		String failed = "";
		final String success;
		String temp = accountService.updateAccountImChatId($c("members", "参数不合法"), imChatId,StationUtil.getStationId());
		JSONObject json = JSONObject.parseObject(temp);
		failed = json.getString("failed");
		success = json.getString("success");
//		try{
//			//聊天室账号清除
//			if(StringUtil.isNotEmpty(success)){
//				new RequestProxy() {
//					public List<Header> getHeaders() {
//						List<Header> headerList = new ArrayList<Header>();
//						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
//						headerList.add(new BasicHeader("Connection", "close")); // 短链接
//						return headerList;
//					};
//					public List<NameValuePair> getParameters() {
//						List<NameValuePair> params = new ArrayList<NameValuePair>();
//						params.add(new BasicNameValuePair("authToken", StationConfigUtil.getSys(StationConfig.sys_real_center_token)));
//						params.add(new BasicNameValuePair("stationId",StationUtil.getStationId()+""));
//						params.add(new BasicNameValuePair("token",token));
//						params.add(new BasicNameValuePair("accounts",success));
//						return params;
//					};
//				}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/api/system/clearAccounts?",PostType.POST, true);
//			}
//		}catch(Exception e){
//		}
		
		if (StringUtil.isEmpty(failed)) {
			super.renderSuccess();
		} else {
			super.renderFailure(failed);
		}
	}
	@RequestMapping("/remove")
	@ResponseBody
	public void removeMember() {
		String failed = "";
		final String success;
		String temp = accountService.updateAccountImChatId($c("members", "参数不合法"),"",StationUtil.getStationId());
		JSONObject json = JSONObject.parseObject(temp);
		failed = json.getString("failed");
		success = json.getString("success");
//		try{
//			//聊天室账号清除
//			if(StringUtil.isNotEmpty(success)){
//				new RequestProxy() {
//					public List<Header> getHeaders() {
//						List<Header> headerList = new ArrayList<Header>();
//						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
//						headerList.add(new BasicHeader("Connection", "close")); // 短链接
//						return headerList;
//					};
//					public List<NameValuePair> getParameters() {
//						List<NameValuePair> params = new ArrayList<NameValuePair>();
//						params.add(new BasicNameValuePair("authToken", StationConfigUtil.getSys(StationConfig.sys_real_center_token)));
//						params.add(new BasicNameValuePair("stationId",StationUtil.getStationId()+""));
//						params.add(new BasicNameValuePair("token",""));
//						params.add(new BasicNameValuePair("accounts",success));
//						return params;
//					};
//				}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/api/system/clearAccounts?",PostType.POST, true);
//			}
//		}catch(Exception e){
//		}
		
		if (StringUtil.isEmpty(failed)) {
			super.renderSuccess();
		} else {
			super.renderFailure(failed);
		}
	}
	@RequestMapping("/all")
	@ResponseBody
	public void all() {
		super.renderJson(JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
	}

}
