package com.game.controller.agent.system;

import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentAppKeyConfig;
import com.game.service.AgentAppKeyConfigService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/apppayconfig")
public class AgentAppKeyConfigController extends BaseAgentController
{
	

	@Autowired
	private AgentAppKeyConfigService agentAppKeyConfigService;
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/apppayconfig.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(){
		Long stationId = StationUtil.getStationId();
		super.render(agentAppKeyConfigService.page(stationId));
	}
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/apppayconfig_add.jsp");
	}@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(AgentAppKeyConfig alb){
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentAppKeyConfigService.addSave(alb);
		super.renderSuccess();
	}
	@RequestMapping("/modify")
	public String modify(Map<String,Object> map,Long id) {
		map.put("apconfig",agentAppKeyConfigService.getOne(id,StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/apppayconfig_modify.jsp");
	}@RequestMapping("/editSaveNew")
	@ResponseBody
	public void editSaveNew(AgentAppKeyConfig alb){
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentAppKeyConfigService.eidtSave(alb);
		super.renderSuccess();
	}
	/**
	 * 应用开关修改
	 */
	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Long status, Long id){
		Long stationId = StationUtil.getStationId();
		agentAppKeyConfigService.openCloseH(id,stationId,status);
		super.renderSuccess();
	}
	
	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave(){
		String json = super.$c("data");
		AgentAppKeyConfig alb = JsonUtil.toBean(json, AgentAppKeyConfig.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentAppKeyConfigService.addSave(alb);
		super.renderSuccess();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/eidtSave")
	@ResponseBody
	public void eidtSave(){
		String json = super.$c("data");
		AgentAppKeyConfig alb = JsonUtil.toBean(json, AgentAppKeyConfig.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentAppKeyConfigService.eidtSave(alb);
		super.renderSuccess();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id){
		Long stationId = StationUtil.getStationId();
		agentAppKeyConfigService.delete(id,stationId);
		super.renderSuccess();
	}

}
