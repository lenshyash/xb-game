package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.service.AgentAdressBookService;
import com.game.service.AgentKeFuService;
import com.game.service.AgentLunBoService;
import com.game.service.SysStationDomainService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentAdressBook")
public class AgentAdressBookController extends BaseAgentController{
	
	@Autowired
	private AgentAdressBookService agentAdressBookService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("/page/system/agentAdressBook.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(String account){
		Long stationId = StationUtil.getStationId();
		super.render(agentAdressBookService.page(stationId,account));
	}	
	
	/**
	 * 导出功能
	 */
	@Permission(CheckType.FUNCTION)
	@ResponseBody
	@RequestMapping("/export")
	public void export() {
		String account = super.$("account");
		Long stationId = StationUtil.getStationId();
		
		agentAdressBookService.export(stationId, account);
		super.renderSuccess();
	}
}
