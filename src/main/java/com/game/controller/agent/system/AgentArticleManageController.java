package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentArticle;
import com.game.service.AgentArticleService;
import com.game.service.SysStationDomainService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.WebUtil;
import com.game.util.check.CheckUtils;

/**
 * 系统公告
 * @author Tassel
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentarticle")
public class AgentArticleManageController extends BaseAgentController {

	@Autowired
	private AgentArticleService agentArticleService;
	
	@Autowired
	private SysStationDomainService domainService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/agentarticle.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Long stationId, Integer code) {
		if (stationId == null) {
			stationId = StationUtil.getStationId();
		}
		super.render(agentArticleService.page(stationId, code));
	}

	/**
	 * 租户公告状态修改
	 */
	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Integer modelStatus, Long id) {
		Long stationId = StationUtil.getStationId();
		agentArticleService.openCloseH(modelStatus, id, stationId);
		super.renderSuccess();
	}

	@RequestMapping("/add")
	public String add() {
		Calendar c= Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		super.getRequest().setAttribute("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 12);
		c.add(Calendar.DAY_OF_MONTH, -1);
		super.getRequest().setAttribute("endDate", DateUtil.toDateStr(c.getTime()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentarticle_add.jsp");
	}

	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(AgentArticle aacle) {
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
		
		String content = WebUtil.getOrgRequest().getParameter("content");
		
		aacle.setContent(content);
		CheckUtils.checkScript(aacle);
		agentArticleService.addSave(aacle);
		super.renderSuccess();
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("article", agentArticleService.getOne(id));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentarticle_modify.jsp");
	}

	@RequestMapping("/eidtSaveNew")
	@ResponseBody
	public void eidtSaveNew(AgentArticle aacle) {
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
	
		String content = WebUtil.getOrgRequest().getParameter("content");
		
		aacle.setContent(content);
		CheckUtils.checkScript(aacle);
		agentArticleService.eidtSave(aacle);
		super.renderSuccess();
	}

	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave() {
		String json =WebUtil.getOrgRequest().getParameter("data");
		AgentArticle aacle = JsonUtil.toBean(json, AgentArticle.class);
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
		agentArticleService.addSave(aacle);
		super.renderSuccess();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/eidtSave")
	@ResponseBody
	public void eidtSave() {
		String json = WebUtil.getOrgRequest().getParameter("data");
		AgentArticle aacle = JsonUtil.toBean(json, AgentArticle.class);
		Long stationId = StationUtil.getStationId();
		aacle.setStationId(stationId);
		agentArticleService.eidtSave(aacle);
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id, Long stationId, Integer code) {
		stationId = StationUtil.getStationId();
		agentArticleService.delete(id, stationId, code);
		super.renderSuccess();
	}
}
