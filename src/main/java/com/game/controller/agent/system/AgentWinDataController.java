package com.game.controller.agent.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentWinData;
import com.game.service.AgentWinDataService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentwindata")
public class AgentWinDataController extends BaseAgentController{
	
	@Autowired
	AgentWinDataService agentWinDataService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("/page/system/agentwindata.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(){
		Long stationId = StationUtil.getStationId();
		super.render(agentWinDataService.page(stationId));
	}
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/agentwindata_add.jsp");
	}
	
	/**
	 * 新增
	 */
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(){
		AgentWinData awd = new AgentWinData();
		awd.setAccount(super.$("account"));
		awd.setType(super.$("type"));
		awd.setWinDate(DateUtil.toDate(super.$("winDate")));
		awd.setWinMoney(BigDecimalUtil.toBigDecimal(super.$("winMoney")));
		Long stationId = StationUtil.getStationId();
		awd.setStationId(stationId);
		agentWinDataService.addSave(awd);
		super.renderSuccess();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id){
		Long stationId = StationUtil.getStationId();
		agentWinDataService.delete(id,stationId);
		super.renderSuccess();
	}

}
