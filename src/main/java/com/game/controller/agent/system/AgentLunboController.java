package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentLunBo;
import com.game.service.AgentLunBoService;
import com.game.service.SysStationDomainService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentlunbo")
public class AgentLunboController extends BaseAgentController{
	
	@Autowired
	private AgentLunBoService agentLunBoService;
	
	@Autowired
	private SysStationDomainService domainService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("/page/system/agentlunbo.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(){
		Long stationId = StationUtil.getStationId();
		super.render(agentLunBoService.page(stationId));
	}
	@RequestMapping("/add")
	public String add() {
		Calendar c= Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		super.getRequest().setAttribute("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 12);
		c.add(Calendar.DAY_OF_MONTH, -1);
		super.getRequest().setAttribute("endDate", DateUtil.toDateStr(c.getTime()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentlunbo_add.jsp");
	}
	/**
	 * 新增类型
	 */
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(AgentLunBo alb ){
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		alb.setModelStatus(alb.getStatus());
		agentLunBoService.addSave(alb);
		super.renderSuccess();
	}
	@RequestMapping("/modify")
	public String modify(Map<String,Object> map,Long id) {
		map.put("lunbo",agentLunBoService.getOne(id,StationUtil.getStationId()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentlunbo_modify.jsp");
	}
	/**
	 * 修改
	 */
	@RequestMapping("/editSaveNew")
	@ResponseBody
	public void editSaveNew(AgentLunBo alb){
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentLunBoService.eidtSave(alb);
		super.renderSuccess();
	}
	/**
	 * 租户公告状态修改
	 */
	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Integer modelStatus, Long id){
		Long stationId = StationUtil.getStationId();
		agentLunBoService.openCloseH(modelStatus,id,stationId);
		super.renderSuccess();
	}
	
	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave(){
		String json = super.$c("data");
		AgentLunBo alb = JsonUtil.toBean(json, AgentLunBo.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentLunBoService.addSave(alb);
		super.renderSuccess();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/eidtSave")
	@ResponseBody
	public void eidtSave(){
		String json = super.$c("data");
		AgentLunBo alb = JsonUtil.toBean(json, AgentLunBo.class);
		Long stationId = StationUtil.getStationId();
		alb.setStationId(stationId);
		agentLunBoService.eidtSave(alb);
		super.renderSuccess();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id){
		Long stationId = StationUtil.getStationId();
		agentLunBoService.delete(id,stationId);
		super.renderSuccess();
	}
	
}
