package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;
import com.game.service.AgentKeFuService;
import com.game.service.AgentLunBoService;
import com.game.service.SysStationDomainService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentkefu")
public class AgentKeFuController extends BaseAgentController{
	
	@Autowired
	private AgentKeFuService agentKeFuService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("/page/system/agentkefu.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(){
		Long stationId = StationUtil.getStationId();
		super.render(agentKeFuService.page(stationId));
	}	
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/agentkefu_add.jsp");
	}
	
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(AgentKeFu kefu){
		Long stationId = StationUtil.getStationId();
		kefu.setStationId(stationId);
		agentKeFuService.addSave(kefu);
		super.renderSuccess();
	}
	
	@RequestMapping("/modify")
	public String modify(Map<String,Object> map,Long id) {
		map.put("kefu",agentKeFuService.getOne(id));
		return super.prepareLayout("/newpage/system/agentkefu_modify.jsp");
	}

	@RequestMapping("/editSaveNew")
	@ResponseBody
	public void editSaveNew(AgentKeFu kefu){
		Long stationId = StationUtil.getStationId();
		kefu.setStationId(stationId);
		agentKeFuService.addSave(kefu);
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id){
		agentKeFuService.delete(id);
		super.renderSuccess();
	}
	
	@RequestMapping("/openCloseH")
	@ResponseBody
	public void openCloseH(Long id, Integer status){
		agentKeFuService.openCloseH(id,status);
		super.renderSuccess();
	}
}
