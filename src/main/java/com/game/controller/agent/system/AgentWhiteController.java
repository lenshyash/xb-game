package com.game.controller.agent.system;

import com.game.constant.LogType;
import com.game.dao.AgentWhiteIpDao;
import com.game.dao.MemberWhiteIpDao;
import com.game.model.MemberWhiteIp;
import com.game.util.SysLogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentWhiteIp;
import com.game.service.AgentWhiteIpService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/white")
public class AgentWhiteController extends BaseAgentController {
	
	@Autowired
	private AgentWhiteIpService ipService;

	@Autowired
	private AgentWhiteIpDao whiteIpDao;

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		super.render(ipService.queryPage());
	}
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/newpage/system/white/index.jsp");
	}
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/white/add.jsp");
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public void savegp() {
		String ipAddress = $c("ip","请输入IP").trim();
		AgentWhiteIp ip = new AgentWhiteIp();
		ip.setStatus(AgentWhiteIp.STATUS_ENABLED);
		ip.setStationId(StationUtil.getStationId());
		ip.setIp(ipAddress);
		ipService.save(ip);
		try {
			SysLogUtil.log("后台IP白名单添加:" + ipAddress, LogType.AGENT_SYSTEM);

		}catch (Exception e){
			logger.error("后台IP白名单添加--加日志异常",e);
		}
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(){
		long id = $cl("id","非法操作");
		ipService.deleteIp(id);
		try {
			AgentWhiteIp memberWhiteIp =  whiteIpDao.get(id);
			SysLogUtil.log("后台IP白名单删除:" + memberWhiteIp.getIp(), LogType.AGENT_SYSTEM);
		}catch (Exception e){
			logger.error("后台IP白名单删除--加日志异常",e);
		}
		super.renderSuccess();
	}
	
	@RequestMapping("/updateStatus")
	@ResponseBody
	public void updateStatus(){
		long id = $cl("id","非法操作");
		long status = $cl("status","非法操作");
		ipService.updateStatus(id, status);
		try {
			String statusStr = status==2?"可用":"禁用";
			AgentWhiteIp memberWhiteIp =  whiteIpDao.get(id);
			SysLogUtil.log("后台IP白名单更新:" + memberWhiteIp.getIp()+"更改为:"+statusStr, LogType.AGENT_SYSTEM);
		}catch (Exception e){
			logger.error("后台IP白名单更新--加日志异常",e);
		}
		super.renderSuccess();
	}
}
