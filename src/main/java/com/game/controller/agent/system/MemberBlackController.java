package com.game.controller.agent.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberWhiteIp;
import com.game.service.MemberWhiteIpService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/front/black")
public class MemberBlackController extends BaseAgentController {
	
	@Autowired
	private MemberWhiteIpService memberIpService;
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		super.render(memberIpService.queryPage(MemberWhiteIp.TYPE_BLACK));
	}
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/newpage/front/black/index.jsp");
	}
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/front/black/add.jsp");
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public void savegp() {
		String ipAddress = $c("ip","请输入IP").trim();
		MemberWhiteIp ip = new MemberWhiteIp();
		ip.setStatus(MemberWhiteIp.STATUS_ENABLED);
		ip.setStationId(StationUtil.getStationId());
		ip.setIp(ipAddress);
		ip.setType(MemberWhiteIp.TYPE_BLACK);
		memberIpService.save(ip);
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(){
		long id = $cl("id","非法操作");
		memberIpService.deleteIp(id);
		super.renderSuccess();
	}
	
	@RequestMapping("/updateStatus")
	@ResponseBody
	public void updateStatus(){
		long id = $cl("id","非法操作");
		long status = $cl("status","非法操作");
		memberIpService.updateStatus(id, status);
		super.renderSuccess();
	}
}
