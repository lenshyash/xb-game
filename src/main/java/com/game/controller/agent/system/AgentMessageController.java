package com.game.controller.agent.system;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysMessage;
import com.game.model.vo.MessageVo;
import com.game.service.SysMessageService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/message")
public class AgentMessageController extends BaseAgentController {

	@Autowired
	private SysMessageService messageService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/message.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String title = super.$("title");
		MessageVo mvo = new MessageVo();
		mvo.setTitle(title);
		mvo.setStationId(StationUtil.getStationId());
		super.render(messageService.getMessagePage(mvo));
	}
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/message_add.jsp");
	}
	@RequestMapping("/modify")
	public String modify(Map<String,Object> map,Long id) {
		map.put("message",messageService.getOne(id,StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/message_modify.jsp");
	}
	@RequestMapping("/update")
	@ResponseBody
	public void update(SysMessage message) {
		messageService.updMessage(message);
		super.renderSuccess();
	}

	@RequestMapping("/send")
	@ResponseBody
	public void send() {
		String account = super.$("account");
		String title = super.$("title");
		Long type = super.$long("type");
		String message = super.$("message");

		MessageVo mvo = new MessageVo();
		mvo.setAccount(account);
		mvo.setTitle(title);
		mvo.setMessage(message);
		mvo.setType(type);
		messageService.sendMessage(mvo);
		super.renderSuccess();
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void delete() {
		Long messageId = super.$long("id");
		messageService.delMessage(messageId);
		super.renderSuccess();
	}
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	public void batchDelete() {
		long[] ids = super.$la("batchId",",");
		for(long id:ids) {
			messageService.delMessage(id);
		}
		super.renderSuccess();
	}

}
