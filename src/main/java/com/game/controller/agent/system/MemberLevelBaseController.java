package com.game.controller.agent.system;

import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.service.MemberLevelService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/level")
public class MemberLevelBaseController extends BaseAgentController {

	@Autowired
	private MemberLevelService levelService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/level.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		super.render(levelService.getLevelPage(StationUtil.getStationId(), null));
	}

	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/level_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("level", levelService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/level_modify.jsp");
	}

	@RequestMapping("/importpg")
	public String importpg(Map<String, Object> map, Long id) {
		map.put("level", levelService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/level_import.jsp");
	}

	@RequestMapping("/import")
	@ResponseBody
	public void importMember(MemberLevelBase level) {
		level.setStationId(StationUtil.getStationId());
		Long importType = $long("importType");
		String result = "";
		if (StringUtil.equals(importType, MemberLevelBase.IMPORT_TYPE_AGENT)) {
			result = levelService.importMember(level, $c("agentName", "参数不合法"), importType);
		} else if (StringUtil.equals(importType, MemberLevelBase.IMPORT_TYPE_COMMON)) {
			result = levelService.importMember(level, $c("members", "参数不合法"), importType);
		} else {
			result = "参数不合法";
		}
		if (StringUtil.isEmpty(result)) {
			super.renderSuccess();
		} else {
			super.renderFailure(result);
		}
	}

	@RequestMapping("/save")
	@ResponseBody
	public void savegp(MemberLevelBase level) {
		level.setStationId(StationUtil.getStationId());
		levelService.saveLevel(level);
		super.renderSuccess();
	}

	@RequestMapping("/all")
	@ResponseBody
	public void all() {
		super.renderJson(JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
	}

	@RequestMapping("/move")
	public String move(Map<String, Object> map, Long id) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		map.put("level", levelService.getOne(id, StationUtil.getStationId()));
		map.put("allLevel", levelService.getStationLevels(StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/level_move.jsp");
	}

	@RequestMapping("/lvlMove")
	@ResponseBody
	public void lvlMove(Long curId, Long nextId) {
		levelService.levelMove(curId, nextId);
		super.renderSuccess();
	}

	@RequestMapping("/lvlDel")
	@ResponseBody
	public void lvlDel() {
		Long id = super.$long("id");
		levelService.levelDel(id);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updStatus")
	@ResponseBody
	public void updStatus(Long id, Integer status) {
		levelService.updLevelStatus(id, status);
		super.renderSuccess();
	}

	@RequestMapping("/reStat")
	@ResponseBody
	public void reStat() {
		levelService.reStatUserNum();
		super.renderSuccess();
	}

}
