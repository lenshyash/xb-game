package com.game.controller.agent.system;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysProduct;
import com.game.model.vo.ProductVo;
import com.game.service.SysProductService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/product")
public class SysProductController extends BaseAgentController {

	@Autowired
	private SysProductService productService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/product.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		ProductVo pvo = new ProductVo();
		pvo.setStationId(StationUtil.getStationId());
		super.render(productService.getPage(pvo));
	}
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/system/product_add.jsp");
	}
	@RequestMapping("/modify")
	public String modify(Map<String,Object> map,Long id) {
		map.put("product",productService.getOne(id,StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/product_modify.jsp");
	}
	@RequestMapping("/combo")
	@ResponseBody
	public void combo() {
		ProductVo pvo = new ProductVo();
		pvo.setStationId(StationUtil.getStationId());
		super.renderJson(productService.getCombo(pvo));
	}

	@RequestMapping("/save")
	@ResponseBody
	public void save(SysProduct product) {
		productService.saveProduct(product);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delete")
	public void delete() {
		Long id = super.$long("id");
		ProductVo productVo = new ProductVo();
		productVo.setProductId(id);
		productService.delProduct(productVo);
		super.renderSuccess();
	}
}
