package com.game.controller.agent.system;

import java.math.BigDecimal;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberRedPacket;
import com.game.service.MemberRedPacketService;
import com.game.util.DateUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/redPacket")
public class AgentRedPacketController extends BaseAgentController {

	@Autowired
	private MemberRedPacketService redPacketService;
	@RequestMapping("/index")
	public String index() {
		return super.prepareLayout("/newpage/system/redpacketList.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		super.render(redPacketService.getPage(StationUtil.getStationId()));
	}

	@RequestMapping("/records")
	@ResponseBody
	public void records(String begin,String end ,String account) {
		super.render(redPacketService.getRecordPage(account,StationUtil.getStationId(),DateUtil.toDate(begin),DateUtil.getTomorrow(end)));
	}
	
	@RequestMapping("/fictitious")
	@ResponseBody
	public void fictitious(String begin,String end) {
		super.render(redPacketService.getFictitiousPage(StationUtil.getStationId(),DateUtil.toDate(begin),DateUtil.getTomorrow(end)));
	}

	@RequestMapping("/addRedPacket")
	public String addRedPacket() {
		getRequest().setAttribute("levels1", MemberLevelUtil.getStationLevels());
		String grapmanyonoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.grab_many_red_packet);
		
		return super.prepareLayout("/newpage/system/redPacket_add.jsp");
	}
	@RequestMapping("/handle")
	public String handle(Long id) {
		getRequest().setAttribute("record", redPacketService.getRedPacketRecord(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/redPacketRecord_handle.jsp");
	}
	
	@RequestMapping("/handlerRecord")
	@ResponseBody
	public void handlerRecord(Long id,Integer status,String remark) {
		redPacketService.handlerRecord(id, status, remark);
		super.renderSuccess();
	}

	@RequestMapping("/save")
	@ResponseBody
	public void save(MemberRedPacket mrp,Long[] groupLevelId,String begin,String end,String sort) {
		
		Long createType = $long("createType");
		Long recordNum = $long("recordNum");
		String beginMoney = $("beginMoney");
		String endMoney = $("endMoney");

		mrp.setBeginDatetime(DateUtil.toDatetime(begin));
		mrp.setEndDatetime(DateUtil.toDatetime(end));
		redPacketService.saveRedPacket(mrp,groupLevelId,createType,recordNum
				,StringUtil.toBigDecimal(beginMoney),StringUtil.toBigDecimal(endMoney));
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus(Long id,Integer status) {
		redPacketService.updStatus(id, status);
		super.renderSuccess();
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public void delete(Long id) {
		redPacketService.delRedPacket(id);
		super.renderSuccess();
	}
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	public void batchDelete() {
		long[] ids = super.$la("batchId",",");
		for(long id:ids) {
			redPacketService.delRedPacket(id);
		}
		super.renderSuccess();
	}
	/**
	 * 抢多个红包拼接参数
	 * @param mrp
	 * @return
	 */
	public MemberRedPacket grabManyRedPacket(MemberRedPacket mrp) {
		String min1 = $("min1");
		String max1 = $("max1");
		String number1 = $("number1");
		
		if(StringUtil.isNotEmpty(min1) && StringUtil.isNotEmpty(max1) &&StringUtil.isNotEmpty(number1)) {
			JSONObject jsonObject1 = new JSONObject();
			jsonObject1.put("min1", min1);
			jsonObject1.put("max1", max1);
			jsonObject1.put("number1", number1);
			String jsonString1 = jsonObject1.toJSONString(jsonObject1);
			mrp.setDepositGrabNumber1(jsonString1);
			//让限制当天充值 和最小充值金额相等  
			mrp.setTodayDeposit(new BigDecimal(min1));
		}
		
		String min2 = $("min2");
		String max2 = $("max2");
		String number2 = $("number2");
		
		if(StringUtil.isNotEmpty(min2) && StringUtil.isNotEmpty(max2) &&StringUtil.isNotEmpty(number2)) {
			JSONObject jsonObject2 = new JSONObject();
			jsonObject2.put("min2", min2);
			jsonObject2.put("max2", max2);
			jsonObject2.put("number2", number2);
			String jsonString2 = jsonObject2.toJSONString(jsonObject2);
			mrp.setDepositGrabNumber2(jsonString2);
		}
		
		String min3 = $("min3");
		String max3 = $("max3");
		String number3 = $("number3");
		
		if(StringUtil.isNotEmpty(min3) && StringUtil.isNotEmpty(max3) &&StringUtil.isNotEmpty(number3)) {
			JSONObject jsonObject3 = new JSONObject();
			jsonObject3.put("min3", min3);
			jsonObject3.put("max3", max3);
			jsonObject3.put("number3", number3);
			String jsonString3 = jsonObject3.toJSONString(jsonObject3);
			mrp.setDepositGrabNumber3(jsonString3);
		}
		
		String min4 = $("min4");
		String max4 = $("max4");
		String number4 = $("number4");
		
		if(StringUtil.isNotEmpty(min4) && StringUtil.isNotEmpty(max4) &&StringUtil.isNotEmpty(number4)) {
			JSONObject jsonObject4 = new JSONObject();
			jsonObject4.put("min4", min4);
			jsonObject4.put("max4", max4);
			jsonObject4.put("number4", number4);
			String jsonString4 = jsonObject4.toJSONString(jsonObject4);
			mrp.setDepositGrabNumber4(jsonString4);
		}
		
		String min5 = $("min5");
		String max5 = $("max5");
		String number5 = $("number5");
		
		if(StringUtil.isNotEmpty(min5) && StringUtil.isNotEmpty(max5) &&StringUtil.isNotEmpty(number5)) {
			JSONObject jsonObject5 = new JSONObject();
			jsonObject5.put("min5", min5);
			jsonObject5.put("max5", max5);
			jsonObject5.put("number5", number5);
			String jsonString5 = jsonObject5.toJSONString(jsonObject5);
			mrp.setDepositGrabNumber5(jsonString5);
		}
		return mrp;
	}
}
