package com.game.controller.agent.system;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentUserGroup;
import com.game.service.AgentUserGroupService;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/group")
public class AgentUserGroupController extends BaseAgentController {

	@Autowired
	private AgentUserGroupService grouService;

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		super.render(grouService.getGroups());
	}

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		return super.goPage("/page/system/usergroup.jsp");
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		map.put("isSuperAgent", UserUtil.isSuperAgent());
		return super.prepareLayout("/newpage/system/usergroup_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("group", grouService.getGroupById(id));
		map.put("isSuperAgent", UserUtil.isSuperAgent());
		return super.prepareLayout("/newpage/system/usergroup_modify.jsp");
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(AgentUserGroup group) {
		grouService.saveGroup(group);
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/del")
	@ResponseBody
	public void del(Long id) {
		grouService.del(id);
		super.renderSuccess();
	}

}
