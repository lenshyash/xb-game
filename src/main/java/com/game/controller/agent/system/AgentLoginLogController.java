package com.game.controller.agent.system;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysLog;
import com.game.model.vo.SysLoginLogVo;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysLogService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/loginLog")
public class AgentLoginLogController extends BaseAgentController {

	@Autowired
	SysLogService logService;

	@RequestMapping("/index")
	public String index(String begin,String end,Map<String,Object> map) {
		String curDate = DateUtil.getCurrentDate();
		map.put("startTime", begin==null?curDate:begin);
		map.put("endTime", end==null?curDate:end);
		return super.goPage("/page/system/loginlog.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping = {"accountIp","l.account_ip"})
	public void list(String loginOs,String deviceId,String deviceType) {
		Long type = super.$long("type");
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		SysLoginLogVo slvo = new SysLoginLogVo();
		slvo.setStationId(StationUtil.getStationId());
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDatetime(begin));
		slvo.setEnd(DateUtil.toDatetime(end));
		slvo.setType(type);
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setIp(ip);
		slvo.setLoginOs(loginOs);
		slvo.setDeviceId(deviceId);
		slvo.setDeviceType(deviceType);
		super.render(logService.getLoginLogPage(slvo));
	}
	
	@ResponseBody
	@RequestMapping("/export")
	public void export(String loginOs) {
		Long type = super.$long("type");
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		SysLoginLogVo slvo = new SysLoginLogVo();
		slvo.setStationId(StationUtil.getStationId());
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDatetime(begin));
		slvo.setEnd(DateUtil.toDatetime(end));
		slvo.setType(type);
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setIp(ip);
		slvo.setLoginOs(loginOs);
		logService.exportLoginLog(slvo);
	}
}
