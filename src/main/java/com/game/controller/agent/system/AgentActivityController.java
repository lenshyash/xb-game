package com.game.controller.agent.system;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberActivityApplyRecord;
import com.game.model.platform.AgentActivity;
import com.game.service.AgentActivityService;
import com.game.service.MemberActivityApplyRecordService;
import com.game.service.SysStationDomainService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.WebUtil;
import com.game.util.check.CheckUtils;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/agentactivity")
public class AgentActivityController extends BaseAgentController {

	@Autowired
	private AgentActivityService agentActivityService;
	
	@Autowired
	private SysStationDomainService domainService;
	
	@Autowired
	private MemberActivityApplyRecordService applyRecordService;
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/MemberActivityRecord.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Long stationId = StationUtil.getStationId();
		super.render(agentActivityService.page(stationId));
	}

	@RequestMapping("/zhopenClose")
	@ResponseBody
	public void openCloseH(Integer modelStatus, Long id) {
		Long stationId = StationUtil.getStationId();
		agentActivityService.openCloseH(modelStatus, id, stationId);
		super.renderSuccess();
	}

	@RequestMapping("/add")
	public String add() {
		Calendar c= Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		super.getRequest().setAttribute("startDate", DateUtil.toDateStr(c.getTime()));
		c.add(Calendar.MONTH, 12);
		c.add(Calendar.DAY_OF_MONTH, -1);
		super.getRequest().setAttribute("endDate", DateUtil.toDateStr(c.getTime()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentactivity_add.jsp");
	}

	/**
	 * 新增类型
	 */
	@RequestMapping("/addSaveNew")
	@ResponseBody
	public void addSaveNew(AgentActivity aaty) {
		Long stationId = StationUtil.getStationId();
		if(StringUtil.equals(aaty.getApplyFlag(), AgentActivity.APPLY_FLAG_ON)) {
			if(StringUtil.isEmpty(aaty.getApplySelected())) {
				throw new GenericException("申请选项为空");
			}
		}
		aaty.setStationId(stationId);
		aaty.setReadFlag(0);
		String content = WebUtil.getOrgRequest().getParameter("content");
		
		aaty.setContent(content);
		CheckUtils.checkScript(aaty);
		agentActivityService.addSave(aaty);
		super.renderSuccess();
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("activity", agentActivityService.getOne(id, StationUtil.getStationId()));
		super.getRequest().setAttribute("dlst", domainService.getFolderLstDomain());
		return super.prepareLayout("/newpage/system/agentactivity_modify.jsp");
	}

	/**
	 * 修改
	 */
	@RequestMapping("/eidtSaveNew")
	@ResponseBody
	public void eidtSaveNew(AgentActivity aaty ) {
		if(StringUtil.equals(aaty.getApplyFlag(), AgentActivity.APPLY_FLAG_ON)) {
			if(StringUtil.isEmpty(aaty.getApplySelected())) {
				throw new GenericException("申请选项为空");
			}
		}
		Long stationId = StationUtil.getStationId();
		aaty.setStationId(stationId);
		
		String content = WebUtil.getOrgRequest().getParameter("content");

		aaty.setContent(content);
		CheckUtils.checkScript(aaty);
		agentActivityService.eidtSave(aaty);
		super.renderSuccess();
	}

	/**
	 * 新增类型
	 */
	@RequestMapping("/addSave")
	@ResponseBody
	public void addSave() {
		String json = WebUtil.getOrgRequest().getParameter("data");
		AgentActivity aaty = JsonUtil.toBean(json, AgentActivity.class);
		Long stationId = StationUtil.getStationId();
		aaty.setStationId(stationId);
		agentActivityService.addSave(aaty);
		super.renderSuccess();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/eidtSave")
	@ResponseBody
	public void eidtSave() {
		String json = WebUtil.getOrgRequest().getParameter("data");
		AgentActivity aaty = JsonUtil.toBean(json, AgentActivity.class);
		Long stationId = StationUtil.getStationId();
		aaty.setStationId(stationId);
		agentActivityService.eidtSave(aaty);
		super.renderSuccess();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		Long stationId = StationUtil.getStationId();
		agentActivityService.delete(id, stationId);
		super.renderSuccess();
	}
	
	@RequestMapping("/listApplyRecord")
	@ResponseBody
	public void listApplyRecord() {
		Long stationId = StationUtil.getStationId();
		
		String account = $("account");
		Date beginDate = DateUtil.toDate($("begin"));
		Date endDate =DateUtil.getTomorrow(DateUtil.toDate($("end")));
		super.render(applyRecordService.getPage(stationId, account, beginDate, endDate));
	}
	
	@RequestMapping("/handler")
	public String handler(Map<String, Object> map, Long id) {
		MemberActivityApplyRecord one = applyRecordService.getOne(id);
		map.put("record", one);
		return super.prepareLayout("/newpage/system/activity_handler.jsp");
	}
	
	/**
	 * 处理优惠活动处理
	 */
	@RequestMapping("/handlerRecord")
	@ResponseBody
	public void handlerRecord() {
		Long stationId = StationUtil.getStationId();
		Long id = $long("id");
		Long status = $long("status");
		String remark = $("remark");
		applyRecordService.handlerRecord(id, stationId, status, remark);
		super.renderSuccess();
	}
	
}
