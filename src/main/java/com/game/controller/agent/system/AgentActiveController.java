package com.game.controller.agent.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberActive;
import com.game.model.MemberActiveAward;
import com.game.model.MemberActiveVirtualRecord;
import com.game.model.vo.ActiveRecordVo;
import com.game.model.vo.ActiveVo;
import com.game.service.MemberActiveService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/active")
public class AgentActiveController extends BaseAgentController {

	@Autowired
	private MemberActiveService activeService;

	@RequestMapping("/index")
	public String index( ) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		if (isNewPlatform()) {
			return super.prepareLayout("/newpage/system/activeList.jsp");
		}
		return super.goPage("/page/system/active.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {

		ActiveVo avo = new ActiveVo();

		avo.setStationId(StationUtil.getStationId());
		super.render(activeService.getPage(avo));
	}

	@RequestMapping("/records")
	@ResponseBody
	public void records() {
		String begin = super.$("begin");
		String end = super.$("end");
		String account = super.$("account");
		Long type = super.$long("type");
		Long activeId = super.$long("activeId");

		ActiveRecordVo avo = new ActiveRecordVo();

		avo.setBegin(DateUtil.toDate(begin));
		avo.setEnd(DateUtil.getTomorrow(end));
		avo.setAccount(account);
		avo.setRecordType(type);
		avo.setActiveId(activeId);
		avo.setStationId(StationUtil.getStationId());
		super.render(activeService.getAwardRecordPage(avo));
	}

	@RequestMapping("/addActive")
	public String addActive() {
		return super.prepareLayout("/newpage/system/active_add.jsp");
	}
	
	@RequestMapping("/modifyActive")
	public String modifyActive(Map<String, Object> map, Long id) {
		map.put("active", activeService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/active_modify.jsp");
	}

	@RequestMapping("/setRule")
	public String setRule(Map<String, Object> map, Long id) {
		map.put("active", activeService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/active_setRule.jsp");
	}

	@RequestMapping("/save")
	@ResponseBody
	public void save(MemberActive ma) {
		String begin = super.$("begin");
		String end = super.$("end");

		ma.setBeginDatetime(DateUtil.toDatetime(begin));
		ma.setEndDatetime(DateUtil.toDatetime(end));

		activeService.saveActive(ma);
		super.renderSuccess();
	}

	@RequestMapping("/saveAward")
	@ResponseBody
	public void saveAward() {
		String json = super.$c("data");
		Long activeId = super.$long("activeId");
		List<MemberActiveAward> awards = JSONArray.parseArray(json, MemberActiveAward.class);
		activeService.saveAwards(activeId, awards);
		super.renderSuccess();
	}

	@RequestMapping("/handle")
	public String handle(Map<String, Object> map, Long id) {
		map.put("record", activeService.getActiveRecord(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/activeResult_handle.jsp");
	}

	@RequestMapping("/handlerRecord")
	@ResponseBody
	public void handlerRecord() {
		Long recordId = super.$long("id");
		Long status = super.$long("status");
		String remark = super.$("remark");
		activeService.handlerRecord(recordId, status, remark);
		super.renderSuccess();
	}

	@RequestMapping("/awards")
	@ResponseBody
	public void awards() {
		Long activeId = super.$long("activeId");
		List<MemberActiveAward> awards = activeService.getAwardsByActiveId(activeId);
		super.renderJson(awards);
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long status = super.$long("status");
		activeService.updStatus(id, status);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/delete")
	public void delete() {
		Long id = super.$long("id");
		ActiveVo avo = new ActiveVo();
		avo.setActiveId(id);
		activeService.delActive(avo);
		super.renderSuccess();
	}
	
	/**
	 * 查下虚拟活动记录
	 *
	 * @author lj   
	 * @date 2018年1月8日
	 */
	@RequestMapping("/virtualrecord")
	@ResponseBody
	public void virtualrecord() {
		String begin = super.$("begin");
		String end = super.$("end");
		String account = super.$("account");
		Long type = super.$long("type");
		Long activeId = super.$long("activeId");

		ActiveRecordVo avo = new ActiveRecordVo();

		avo.setBegin(DateUtil.toDate(begin));
		avo.setEnd(DateUtil.getTomorrow(end));
		avo.setAccount(account);
		avo.setRecordType(type);
		avo.setActiveId(activeId);
		avo.setStationId(StationUtil.getStationId());
		super.render(activeService.getAwardVirtualRecordPage(avo));
	}
	/**
	 * 添加虚拟活动记录
	 *
	 * @author lj   
	 * @date 2018年1月8日
	 */
	@RequestMapping("/saveVirtualrecord")
	@ResponseBody
	public void saveVirtualrecord(Integer records,Long activeType ,Long award_type,String productName) {
		 activeService.saveVirtualRecord( records, StationUtil.getStationId(), activeType , award_type, productName);
		 super.renderSuccess();
	}
	/**
	 * 删除虚拟活动记录
	 *
	 * @author lj   
	 * @date 2018年1月8日
	 */
	@RequestMapping("/delVirtualrecord")
	@ResponseBody
	public void delVirtualrecord(Long id) {
		 activeService.delVirtualRecord(id);
		 super.renderSuccess();
	}
	/**
	 * 添加虚拟活动 
	 *
	 * @author lj   
	 * @date 2018年1月9日
	 */
	@RequestMapping("/addActiveVirtualrecord")
	public String addActiveVirtualrecord() {
		return super.prepareLayout("/newpage/system/add_active_virtualrecord.jsp");
	}
	/**
	 * 修改虚拟活动 
	 *
	 * @author lj   
	 * @date 2018年1月9日
	 */
	@RequestMapping("/editActiveVirtualrecord")
	public String updateActiveVirtualrecord(Map<String, Object> map, Long id) {
		map.put("virtualRecord", activeService.getActiveVirtualRecord(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/system/edit_active_virtualrecord.jsp");
	}
	/**
	 * 修改虚拟记录
	 *
	 * @author lj   
	 * @date 2018年1月9日
	 */
	@RequestMapping("/updateVirtualrecord")
	@ResponseBody
	public void saveVirtualrecord(MemberActiveVirtualRecord memberActiveVirtualRecord) {
		 activeService.updateVirtualRecord(memberActiveVirtualRecord);
		 super.renderSuccess();
	}
}
