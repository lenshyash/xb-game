package com.game.controller.agent.system;

import com.game.constant.LogType;
import com.game.dao.MemberWhiteIpDao;
import com.game.util.SysLogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.MemberWhiteIp;
import com.game.service.MemberWhiteIpService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/front/white")
public class MemberWhiteController extends BaseAgentController {
	
	@Autowired
	private MemberWhiteIpService memberIpService;


	@Autowired
	private MemberWhiteIpDao whiteIpDao;
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		super.render(memberIpService.queryPage(MemberWhiteIp.TYPE_WHITE));
	}
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/newpage/front/white/index.jsp");
	}
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/front/white/add.jsp");
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public void savegp() {
		String ipAddress = $c("ip","请输入IP").trim();
		MemberWhiteIp ip = new MemberWhiteIp();
		ip.setStatus(MemberWhiteIp.STATUS_ENABLED);
		ip.setStationId(StationUtil.getStationId());
		ip.setIp(ipAddress);
		ip.setType(MemberWhiteIp.TYPE_WHITE);
		memberIpService.save(ip);
		try {
			SysLogUtil.log("前台IP白名单添加:" + ipAddress, LogType.AGENT_SYSTEM);

		}catch (Exception e){
			logger.error("前台IP白名单添加--加日志异常",e);
		}
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(){
		long id = $cl("id","非法操作");
		try {
			MemberWhiteIp memberWhiteIp =  whiteIpDao.get(id);
			SysLogUtil.log("前台IP白名单删除:" + memberWhiteIp.getIp(), LogType.AGENT_SYSTEM);
		}catch (Exception e){
			logger.error("前台IP白名单删除--加日志异常",e);
		}

		memberIpService.deleteIp(id);

		super.renderSuccess();
	}
	
	@RequestMapping("/updateStatus")
	@ResponseBody
	public void updateStatus(){
		long id = $cl("id","非法操作");
		long status = $cl("status","非法操作");
		try {
			String statusStr = status==2?"可用":"禁用";
			MemberWhiteIp memberWhiteIp =  whiteIpDao.get(id);
			SysLogUtil.log("前台IP白名单更新:" + memberWhiteIp.getIp()+"更改为:"+statusStr, LogType.AGENT_SYSTEM);
		}catch (Exception e){
			logger.error("前台IP白名单更新--加日志异常",e);
		}
		memberIpService.updateStatus(id, status);
		super.renderSuccess();
	}
}
