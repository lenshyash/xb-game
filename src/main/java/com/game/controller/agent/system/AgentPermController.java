package com.game.controller.agent.system;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.AgentMenuService;
import com.game.service.AgentUserGroupService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/system/permission")
public class AgentPermController extends BaseAgentController {

	@Autowired
	private AgentMenuService menuService;

	@Autowired
	private AgentUserGroupService grouService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		map.put("groups", grouService.getGroupCombo(true));
		return super.goPage("/page/system/permission.jsp");
	}

	@RequestMapping("/getGroupPermission")
	@ResponseBody
	public void getGroupPermission(Long groupId) {
		Map<String, Object> data = new HashMap<>();
		data.put("menus", menuService.getStationMenus());
		data.put("pm", menuService.getAgentPermission(groupId, StationUtil.getStationId()));
		super.renderJson(data);
	}

	@RequestMapping("/savePermission")
	@ResponseBody
	public void savePermission(Long groupId, String ids) {
		menuService.saveAgentPermission(groupId, StationUtil.getStationId(), ids);
		super.renderSuccess();
	}

}
