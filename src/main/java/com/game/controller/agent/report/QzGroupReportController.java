package com.game.controller.agent.report;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.dao.ComprehensiveCountDao;
import com.game.model.Qzgroup;
import com.game.model.lottery.BcLottery;
import com.game.model.vo.ReportParamVo;
import com.game.service.QzGroupReportService;
import com.game.service.QzGroupService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.lottery.BcLotteryOrderService;
import com.game.service.lottery.BcLotteryService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.WebUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/group")
public class QzGroupReportController extends BaseAgentController {

	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;
	@Autowired
	private BcLotteryOrderService bcLotteryOrderService;
	@Autowired
	private QzGroupService qzGroupService;
	@Autowired
	private QzGroupReportService groupReportService;
	@Autowired
	private BcLotteryService lottteryService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		
		String curDate = DateUtil.getCurrentDate();
		String begin = request.getParameter("begin");
		String end = request.getParameter("end");
		super.getRequest().setAttribute("startTime", begin == null ? curDate : begin);
		super.getRequest().setAttribute("endTime", end == null ? curDate : end);
		
		String groupName = $("groupName");
		request.setAttribute("groupName", groupName);
		List<BcLottery> lots = lottteryService.lotSortList(StationUtil.getStationId(),null);
		super.getRequest().setAttribute("lots",lots);
		return super.goPage("/page/report/qz_group_report.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		
		String key = "qzgroupreport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		
		Long stationId = StationUtil.getStationId();
		String lotCode = super.$("lotCode");
		String begin = super.$("begin");
		String end = super.$("end");
		Date beginDate = DateUtil.toDatetime(begin);
		Date endDate = DateUtil.toDatetime(end);
		//防止数据量过大
		if(beginDate==null || endDate==null) {
			throw new GenericException("日期不能为空");
		}
		//先不允许查超过7天数据 有改动再说
		int dayMargin = DateUtil.getDayMargin(beginDate, endDate);
		if(dayMargin>7) {
			throw new GenericException("查询日期间隔不能超过7天");
		}
		String qihao = super.$("qihao");
		String groupName = super.$("groupName");
		//lotCode为空就写死  数据量太大
		if(StringUtil.isEmpty(lotCode)) {
			lotCode  = "SFSC";
		}
		Page page = groupReportService.getGroupPage(stationId, lotCode, groupName,qihao,beginDate,endDate);
		super.render(page);
		
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
