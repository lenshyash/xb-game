package com.game.controller.agent.report;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.AgentBaseConfigService;
import com.game.service.ComprehensiveCountService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/comprehensive")
public class ComprehensiveCountController extends BaseAgentController {

	@Autowired
	private ComprehensiveCountService ccService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(Model model) {
		Long stationId = StationUtil.getStationId();
		//默认查询近两个月
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		String date = sdf.format(new Date());
				
		model.addAttribute("startTime", date);
		model.addAttribute("endTime", date);
		
		model.addAttribute("isDuLiCaiPiao", agentBaseConfigService.isDuLiCaiPiao(stationId));
		model.addAttribute("multiAgent",
				agentBaseConfigService.isOn(StationConfig.onoff_multi_agent.name(), stationId));
		String reportSwitch = StationConfigUtil.get(stationId, StationConfig.comprehensive_only_report_type_normal);
		model.addAttribute("reportSwitch", reportSwitch);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/report/comprehensive2.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Long reportType = super.$long("reportType");
		String begin = super.$("begin");
		String end = super.$("end");
 		String key = "comprehensive:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		// super.renderJson(ccService.getComprehensiveCountMap(StationUtil.getStationId()));
		super.renderJson(dailyMoneyService.getComprehensiveCountMap(StationUtil.getStationId(),reportType,begin,end));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}


	@RequestMapping("/yearlist")
	@ResponseBody
	public void yearlist(String begin, String end,Long reportType) {
		String key = "comprehensive:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		// super.renderJson(ccService.getComprehensiveCountMap(StationUtil.getStationId()));
		super.renderJson(dailyMoneyService.getComprehensiveYearDataMap(StationUtil.getStationId(),DateUtil.parseDate(begin.toString(), "yyyy-MM"),DateUtil.parseDate(end.toString(), "yyyy-MM"),reportType));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
	
	@RequestMapping("/statsAll")
	@ResponseBody
	public void statsAll(Long stationId, String startDate, String endDate) {
		ccService.statsStations(stationId, startDate, endDate);
		renderText("成功");
	}
}
