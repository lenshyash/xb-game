package com.game.controller.agent.report;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.vo.ReportParamVo;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.ValidateUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/global")
public class GlobalReportController extends BaseAgentController {

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(String begin, String end, String account,String agent, Map<String, Object> map) {
		HttpServletRequest request = super.getRequest();
		String curDate = DateUtil.getCurrentDate();
		request.setAttribute("startTime", curDate);
		request.setAttribute("endTime", curDate);
		long stationId = StationUtil.getStationId();
		String lottery = StationConfigUtil.get(stationId, StationConfig.onoff_lottery_game);
		String real = StationConfigUtil.get(stationId, StationConfig.onoff_zhen_ren_yu_le);
		String dianZi = StationConfigUtil.get(stationId, StationConfig.onoff_dian_zi_you_yi);
		String sport = StationConfigUtil.get(stationId, StationConfig.onoff_sports_game);
		String markSix = StationConfigUtil.get(stationId, StationConfig.onoff_liu_he_cai);
		String thirdSports = StationConfigUtil.get(stationId, StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(stationId, StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(stationId, StationConfig.onoff_chess);
		String balanceGem = StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem);
		String dj = StationConfigUtil.get(stationId, StationConfig.onoff_dian_jing_yu_le);
		request.setAttribute("lottery", lottery);
		request.setAttribute("real", real);
		request.setAttribute("sport", sport);
		request.setAttribute("markSix", markSix);
		request.setAttribute("dianZi", dianZi);
		request.setAttribute("thirdSports", thirdSports);
		request.setAttribute("thirdLottery", thirdLottery);
		request.setAttribute("chess", chess);
		request.setAttribute("dj", dj);
		request.setAttribute("balanceGem", balanceGem);
		request.setAttribute("saccount", account);
		request.setAttribute("agent", agent);
		request.setAttribute("rdo", "on".equals(StationConfigUtil.get(stationId, StationConfig.onoff_agent_report_default_ordinary)));
		request.setAttribute("replaceFont", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_replace_font));
		return super.goPage("/page/report/global.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String key = "globallreport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		String account = super.$("account");
		String agentName = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long refresh = super.$long("refresh");
		Long reportType = super.$long("reportType");
		String filterAccount = super.$("filterAccount");
		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.getTomorrow(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setAgentName(agentName);
		paramVo.setRefresh(refresh);
		paramVo.setReportType(reportType);
		paramVo.setFilterAccount(filterAccount);
		// super.renderJson(reportService.getGlobalReport(paramVo));
		super.renderJson(dailyMoneyService.getGlobalReportAdmin(paramVo));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
