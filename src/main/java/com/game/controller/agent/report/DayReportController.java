package com.game.controller.agent.report;

import javax.servlet.http.HttpServletRequest;

import com.game.constant.StationConfig;
import com.game.util.StationConfigUtil;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.dao.ComprehensiveCountDao;
import com.game.model.vo.ReportParamVo;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.WebUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/day")
public class DayReportController extends BaseAgentController {

	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/report/day.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String key = "dayreport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		
		String account = super.$("account");
		String agentName = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long reportType = super.$long("reportType");
		
		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setAgentName(agentName);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.toDate(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setCacheName("day");
		paramVo.setReportType(reportType);
		super.render(dailyMoneyService.getDayReport(paramVo));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
