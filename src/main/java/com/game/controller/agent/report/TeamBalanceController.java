package com.game.controller.agent.report;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/teamBalance")
public class TeamBalanceController extends BaseAgentController {
//	@Autowired
//	private TotalStatisticReportService tsReportService;

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;
	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		request.setAttribute("agentMulti", "on".equals(StationConfigUtil.get(StationConfig.onoff_multi_agent)));
		request.setAttribute("mergeFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_agent_merge_Rebate_odds_switch)));
		request.setAttribute("profitShareFlag", StationConfigUtil.get(StationConfig.onoff_profit_share));
		request.setAttribute("moveFlag", StationConfigUtil.get(StationConfig.onoff_agent_move_agent));
		
		if (isNewPlatform()) {
			request.setAttribute("levels", MemberLevelUtil.getStationLevels());
		} else {
			request.setAttribute("levels", JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
		}
		request.setAttribute("level", $long("level"));
		
		request.setAttribute("memberRateFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		
		String groupSwitch = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.qz_group_switch);
		String groupName = $("groupName");
		request.setAttribute("groupSwitch", groupSwitch);
		request.setAttribute("groupName", groupName);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/agent/team_balance.jsp");
	
	}

	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping = {"accountStatus","a.account_status", "money", "m.money","createDatetime", "a.create_datetime","lastLoginDatetime", "a.last_login_datetime"})
	public void list() {
		String account = super.$("account");
		String agentName = super.$("agentName");
		String parents = super.$("parents");
		String userName = super.$("userName");
		Long levelGroup = super.$long("levelGroup");
		Long reportType = super.$long("reportType");
		String qzGroupName = super.$("qzGroupName");
		Long accountStatus =  super.$long("accountStatus");
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setAgentName(agentName);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
		accountVo.setParents(parents);
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setGeneralSearch(true);
		accountVo.setUserName(userName);
		accountVo.setLevelGroup(levelGroup);
		accountVo.setReportType(reportType);
		accountVo.setQzGroupName(qzGroupName);
		accountVo.setAccount(account);
		accountVo.setAccountStatus(accountStatus);
		accountVo.setDailiTopSearch("on".equals(StationConfigUtil.get(StationConfig.onoff_agent_daili_search_type_default)));
		super.render(dailyMoneyService.getAccountPageWithTeamBalance(accountVo));
	}

}
