package com.game.controller.agent.report;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/totalStatistic")
public class TotalStatisticController extends BaseAgentController {
//	@Autowired
//	private TotalStatisticReportService tsReportService;

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/report/totalStatistic.jsp");
	
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String account, String agentName, String startTime, String endTime,Long searchType, Integer pageNumber,
			Integer pageSize, String sortName, String sortOrder, Long reportType) {
		String key = "totalstat:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		Long stationId = StationUtil.getStationId();
		// super.render(tsReportService.getTotalStatistic(stationId, account,
		// agentName, DateUtil.validStartDateTime(startTime),
		// DateUtil.validEndDateTime(endTime), pageNumber, pageSize));
		super.render(dailyMoneyService.getTotalStatistic(stationId, account, agentName,
				DateUtil.validStartDateTime(startTime), DateUtil.validEndDateTime(endTime),searchType, pageNumber, pageSize, sortName, sortOrder, reportType));

		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}

}
