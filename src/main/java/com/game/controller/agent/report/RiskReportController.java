package com.game.controller.agent.report;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.vo.AccountVo;
import com.game.model.vo.ReportParamVo;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/risk")
public class RiskReportController extends BaseAgentController {

	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;
	
	@Autowired
	SysAccountService accountService;

	@RequestMapping("/index")
	public String index(String account,String navName,String agent) {
		HttpServletRequest request = super.getRequest();
		String curDate = DateUtil.getCurrentDate();
		
		String lottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_lottery_game);
		String real = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		String egame = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_zi_you_yi);
		String sport = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_sports_game);
		String markSix = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_liu_he_cai);
		String thirdSports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_chess);
		String esports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_jing_yu_le);
		String appDomain = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.app_create_station_domain);
		Map gameMap = MixUtil.newHashMap();
		handlerGameMap(gameMap, "lottery", lottery, "彩票");
		handlerGameMap(gameMap, "sysLottery", lottery, "系统彩");
		handlerGameMap(gameMap, "sfMarkSix", lottery, "十分六合彩");
		handlerGameMap(gameMap, "real", real, "真人");
		handlerGameMap(gameMap, "egame", egame, "电子");
		handlerGameMap(gameMap, "sports", sport, "体育");
		handlerGameMap(gameMap, "markSix", markSix, "六合彩");
		handlerGameMap(gameMap, "hunter", egame, "捕鱼王");
		handlerGameMap(gameMap, "thirdSports", thirdSports, "三方体育");
		handlerGameMap(gameMap, "thirdLottery", thirdLottery, "三方彩票");
		handlerGameMap(gameMap, "chess", chess, "棋牌");
		handlerGameMap(gameMap, "esports", esports, "电竞");
		if(StringUtil.isNotEmpty(appDomain)){
			gameMap.put("conversion", "转化分析");
		}

		request.setAttribute("gameOnOff", JsonUtil.toJson(gameMap));
		request.setAttribute("startTime", curDate);
		request.setAttribute("endTime", curDate);
		request.setAttribute("navName", navName);
		request.setAttribute("account", account);
		request.setAttribute("agent", agent);
		request.setAttribute("replaceFont", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_replace_font));
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/report/risk.jsp");
	}

	private void handlerGameMap(Map gameMap, String key, String onOff, String gameName) {
		if ("on".equals(onOff)) {
			gameMap.put(key, gameName);
		}
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String key = "riskreport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		
		String type = super.$("type");
		String account = super.$("account");
		String userName = super.$("userName");
		String agent = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		String sortName = super.$("sortName");
		String sortOrder = super.$("sortOrder");
		Long reportType = super.$long("reportType");
		
		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setUserName(userName);
		paramVo.setAgentName(agent);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.getTomorrow(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setSortName(sortName);
		paramVo.setSortOrder(sortOrder);
		paramVo.setType(type);
		paramVo.setReportType(reportType);
		
		super.render(dailyMoneyService.getRiskReport(paramVo));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
	
	@ResponseBody
	@RequestMapping("/account")
	public void account() {
		String account = super.$("account");
		String agent = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long reportType = super.$long("reportType");
		
		AccountVo accountVo = new AccountVo();
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setAccount(account);
		accountVo.setAgentName(agent);
		accountVo.setBegin(DateUtil.toDate(begin));
		accountVo.setEnd(DateUtil.getTomorrow(end));
		accountVo.setReportType(reportType);
		super.render(accountService.getAccountRiskPage(accountVo));
	}
	
	@ResponseBody
	@RequestMapping("/conversion")
	public void conversion() {
		String account = super.$("account");
		String agent = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		AccountVo accountVo = new AccountVo();
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setAccount(account);
		accountVo.setAgentName(agent);
		accountVo.setBegin(DateUtil.toDate(begin));
		accountVo.setEnd(DateUtil.getTomorrow(end));
		super.render(accountService.getConversionRiskPage(accountVo));
	}
}
