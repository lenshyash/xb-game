package com.game.controller.agent.report;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.MemberDataReportService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
/**
 * 单个会员数据统计
 * @author macair
 *
 */
@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/memberData")
public class MemberDataReportController extends BaseAgentController {
	@Autowired
	private MemberDataReportService memberDataReportService;

	@RequestMapping("/index")
	public String index(String begin, String end, String account, Map<String, Object> map) {
		Date beginDate = DateUtil.parseDate(begin);
		if(beginDate==null){
			map.put("begin", DateUtil.getCurrentDate()+" 00:00:00");
		}else{
			map.put("begin",DateUtil.toDatetimeStr(beginDate));
		}
		Date endDate = DateUtil.parseDate(end);
		if(endDate==null){
			map.put("end", DateUtil.getCurrentDate()+" 23:59:59");
		}else{
			map.put("end",DateUtil.toDatetimeStr(endDate));
		}
		map.put("account", account);
		long stationId = StationUtil.getStationId();
		String real = StationConfigUtil.get(stationId, StationConfig.onoff_zhen_ren_yu_le);
		String dianZi = StationConfigUtil.get(stationId, StationConfig.onoff_dian_zi_you_yi);
		String sport = StationConfigUtil.get(stationId, StationConfig.onoff_sports_game);
		String markSix = StationConfigUtil.get(stationId, StationConfig.onoff_liu_he_cai);
		String chess = StationConfigUtil.get(stationId, StationConfig.onoff_chess);
		map.put("real", real);
		map.put("sport", sport);
		map.put("markSix", markSix);
		map.put("dianZi", dianZi);
		map.put("chess", chess);
		return super.prepareLayout("newpage/report/memberData.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/list")
	public void list(String account, String begin, String end) {
		if(StringUtils.isEmpty(account)){
			renderFailure("请输入会员帐号");
			return;
		}
		if(StringUtils.isEmpty(begin) || StringUtils.isEmpty(end)){
			renderFailure("请选择查询时间");
			return;
		}
		String key = "member:data:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");

		super.renderJson(memberDataReportService.getMemberData(account,StationUtil.getStationId(),begin,end));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
