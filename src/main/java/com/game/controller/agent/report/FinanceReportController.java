package com.game.controller.agent.report;

import javax.servlet.http.HttpServletRequest;

import com.game.constant.StationConfig;
import com.game.util.StationConfigUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.vo.ReportParamVo;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/report/finance")
public class FinanceReportController extends BaseAgentController {

	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		return super.goPage("/page/report/finance.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping={"depositTotal","deposit_total","withdrawTotal","withdraw_total","rebateTotal","rebate_total","rebateAgentTotal","rebate_agent_total","depositGiftMoney","deposit_gift_money",
			"registerGiftMoney","register_gift_money","manualDepositTotal","manual_deposit_total","manualWithdrawTotal","manual_withdraw_total"})
	public void list() {
		String key = "financereport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");

		String account = super.$("account");
		String agentName = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long reportType = $long("reportType");

		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.getTomorrow(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setAgentName(agentName);
		paramVo.setReportType(reportType);
		// super.render(reportService.getFinanceReport(paramVo));
		super.render(dailyMoneyService.getFinanceReport(paramVo));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
