package com.game.controller.agent.agent;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.service.AgentBaseConfigService;
import com.game.service.GeneralizeLinkService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/generalizeLink")
public class AgentGeneralizeLinkController extends BaseAgentController{
	
	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		Long stationId = StationUtil.getStationId();
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(stationId)&&!"on".equals(StationConfigUtil.get(stationId, StationConfig.onoff_agent_merge_Rebate_odds_switch));//匹配多级代理开关
		request.setAttribute("dynamicRate","on".equals(StationConfigUtil.get(stationId, StationConfig.onoff_member_rate_random)));
		request.setAttribute("isDuLiCaiPiao", isMulti);
		return super.goPage("/page/agent/generalizeLink.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(HttpServletRequest request,Integer type,String userAccount , String linkKey){
		super.render(generalizeLinkService.page(StationUtil.getStationId(),request,userAccount,type,linkKey));
	}
	
	@RequestMapping("/updateStatusById")
	@ResponseBody
	public void updateStatusById(Long id , Integer status){
		generalizeLinkService.updateStatusById(id, status,StationUtil.getStationId());
		super.renderSuccess();
	}
}
