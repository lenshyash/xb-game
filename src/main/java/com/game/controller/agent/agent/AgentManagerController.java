package com.game.controller.agent.agent;

import java.lang.reflect.Array;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.AccountVo;
import com.game.permission.PermissionManager;
import com.game.permission.annotation.CheckLastLoginIp;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.permission.annotation.SortMapping;
import com.game.service.SysAccountService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/agent/manager")
public class AgentManagerController extends BaseAgentController {

	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		request.setAttribute("agentMulti", "on".equals(StationConfigUtil.get(StationConfig.onoff_multi_agent)));
		request.setAttribute("mergeFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_agent_merge_Rebate_odds_switch)));
		request.setAttribute("profitShareFlag", StationConfigUtil.get(StationConfig.onoff_profit_share));
		request.setAttribute("moveFlag", StationConfigUtil.get(StationConfig.onoff_agent_move_agent));
		
		if (isNewPlatform()) {
			request.setAttribute("levels", MemberLevelUtil.getStationLevels());
		} else {
			request.setAttribute("levels", JsonUtil.toJson(MemberLevelUtil.getStationLevels()));
		}
		request.setAttribute("level", $long("level"));
		
		request.setAttribute("memberRateFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		
		String groupSwitch = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.qz_group_switch);
		String groupName = $("groupName");
		request.setAttribute("groupSwitch", groupSwitch);
		request.setAttribute("groupName", groupName);
		super.getRequest().setAttribute("rdo", "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_report_default_ordinary)));
		request.setAttribute("warnFlag","off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning)));
		return super.goPage("/page/agent/manager.jsp");
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		String remark = super.$("remark");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		if(StringUtils.isNotEmpty(remark)){
			account.setRemark(remark);
		}
		accountService.updStatus(account);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/multidata")
	public void multidata() {
		Long accountId = super.$long("accountId");
		AccountVo avo = new AccountVo();
		avo.setId(accountId);
		super.renderJson(accountService.getAgentMultiData(avo));
	}
	
	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/list")
	@SortMapping(mapping = {"accountStatus","a.account_status", "money", "m.money","createDatetime", "a.create_datetime","lastLoginDatetime", "a.last_login_datetime","levelGroup","a.level_group"})
	public void list() {
		String account = super.$("account");
		String agentName = super.$("agentName");
		String parents = super.$("parents");
		String userName = super.$("userName");
		Long levelGroup = super.$long("levelGroup");
		Long reportType = super.$long("reportType");
		String qzGroupName = super.$("qzGroupName");
		Long accountStatus =  super.$long("accountStatus");
		String levelArray = super.$("levelArray");
		String phone = super.$("phone");
		String qq = super.$("qq");
		String wechat = super.$("wechat");
		String email = super.$("email");
		String ip = super.$("lastLoginIp");
		String regIp = super.$("regIp");
		String cardNo = super.$("cardNo");
		String remark = super.$("remark");
		Long searchType = super.$long("searchType");
		Long abnormalFlag = super.$long("abnormalFlag");
		
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setAgentName(agentName);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
		accountVo.setParents(parents);
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setGeneralSearch(true);
		accountVo.setUserName(userName);
		accountVo.setLevelGroup(levelGroup);
		accountVo.setReportType(reportType);
		accountVo.setQzGroupName(qzGroupName);
		accountVo.setAccount(account);
		accountVo.setAccountStatus(accountStatus);
		accountVo.setDailiTopSearch("on".equals(StationConfigUtil.get(StationConfig.onoff_agent_daili_search_type_default)));
		accountVo.setAbnormalFlag(abnormalFlag);
		accountVo.setQq(qq);
		accountVo.setWechat(wechat);
		accountVo.setEmail(email);
		accountVo.setPhone(phone);
		accountVo.setLastLoginIp(ip);
		accountVo.setRegIp(regIp);
		accountVo.setRegisterUrl(StringUtil.trim2Null(super.$("registerUrl")));
		accountVo.setCardNo(cardNo);
		accountVo.setRemark(remark);
		if(searchType!=null){
			accountVo.setSearchType(searchType);
//			accountVo.setParents(account);
		}

		
		//x00242特殊查询处理
		String string = StationConfigUtil.get(StationConfig.agent_page_query_special);
		if("on".equals(string)) {
			accountVo.setAgentPageQuery(true);
		}
		if(StringUtil.isNotEmpty(levelArray)) {
			accountVo.setLevelArray(StringToInt(levelArray.split(",")));
		}
		super.render(accountService.getAccountPage(accountVo));
	}
	
	private Integer[] StringToInt(String[] arrs) {
		Integer[] ints = new Integer[arrs.length];
		for(int i=0;i<arrs.length;i++){
			ints[i] = Integer.parseInt(arrs[i]);
		}
		return ints;
	}

	@CheckLastLoginIp
	@ResponseBody
	@RequestMapping("/save")
	public void save() {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		Long id = super.$long("id");
		String account = super.$("account");
		Long accountType = super.$long("accountType");
		Long cardNoStatus = super.$long("cardNoStatus");
		String bankName = super.$("bankName");
		Long accountStatus = super.$long("accountStatus");
		String cardNo = super.$("cardNo");
		String userName = super.$("userName");
		String phone = super.$("phone");
		String qq = super.$("qq");
		String wechat = super.$("wechat");
		String email = super.$("email");
		String bankAddress = super.$("bankAddress");
		String province = super.$("province");
		String city = super.$("city");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		String rebateNum = super.$("rebateNum");
		String profitShare = super.$("profitShare");
		Long modifyType = super.$cl("modifyType");
		Long reportType = super.$cl("reportType");
		Long levelGroup = super.$long("levelGroup");
		Long redpacketLimit = super.$long("redpacketLimit");
		String dynamicRate = super.$("dynamicRate");
		String remark = super.$("remark");
		AccountVo accountVo = new AccountVo();
		accountVo.setId(id);
		accountVo.setAccount(account);
		accountVo.setBankName(bankName);
		accountVo.setStatus(accountStatus);
		accountVo.setCardNoStatus(cardNoStatus);
		accountVo.setCardNo(cardNo);
		accountVo.setBankName(bankName);
		accountVo.setUserName(userName);
		accountVo.setPhone(phone);
		accountVo.setQq(qq);
		accountVo.setWechat(wechat);
		accountVo.setEmail(email);
		accountVo.setBankAddress(bankAddress);
		accountVo.setProvince(province);
		accountVo.setCity(city);
		accountVo.setPwd(pwd);
		accountVo.setReportType(reportType);
		accountVo.setRpwd(rpwd);
		accountVo.setLevelGroup(levelGroup);
		accountVo.setRedpacketLimit(redpacketLimit);
		accountVo.setRemark(remark);
		accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
		if (StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
			accountVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL);
		}
		accountVo.setStationId(StationUtil.getStationId());
		accountVo.setAgentId(0l);
		accountVo.setRebateNum(StringUtil.toBigDecimal(rebateNum));
		accountVo.setProfitShare(StringUtil.toBigDecimal(profitShare));
		accountVo.setDynamicRate(StringUtil.toBigDecimal(dynamicRate));//动态赔率值
		accountService.saveAccount(accountVo,modifyType);
		super.renderSuccess();
	}

	@RequestMapping(value = "/setGeneral")
	@ResponseBody
	public void setGeneral() {
		Long agentId = super.$long("agentId");
		String generalAgent = super.$("generalAgent");
		accountService.setGeneralAgent(agentId, generalAgent);
		super.renderSuccess();
	}

	@RequestMapping(value = "/delGeneral")
	@ResponseBody
	public void delGeneral(Long id) {
		accountService.delGeneralAgent(id);
		super.renderSuccess();
	}
	
	@CheckLastLoginIp
	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		
		boolean updpwd = PermissionManager.getAgentPermission().contains("/agent/agent/manager/updpwd.do");
		if(!updpwd) {
			throw new GenericException("没有修改密码的权限");
		}
		
		accountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}
	
	@Permission(CheckType.FUNCTION)
	@ResponseBody
	@RequestMapping("/advanced")
	public void modifyInfo(Long id) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				renderFailure("超级租户不能执行该操作");
				return;
			}
		}
		super.renderJson(JsonUtil.toJson(accountService.getAccountInfo(id)));
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		
		boolean updpwd = PermissionManager.getAgentPermission().contains("/agent/agent/manager/updpwd.do");
		if(!updpwd) {
			throw new GenericException("没有修改密码的权限");
		}
		accountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/checkBankCard")
	@ResponseBody
	public void checkBankCard() {
		Long userId = super.$long("id");
		String bankCard = super.$("cardNo");
		super.renderJson(accountService.checkBankCard(userId, bankCard));
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map) {
		map.put("multiAgent", accountService.getAgentMultiData(new AccountVo()));
		map.put("levels", MemberLevelUtil.getStationLevels());
		return super.prepareLayout("/newpage/agent/agent_add.jsp");
	}

	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		if (!SystemConfig.SYS_MODE_DEVELOP) {
			SysAccount user = (SysAccount) UserUtil.getCurrentUser();
			if (user.getAccountType() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				throw new GenericException("超级租户不能执行该操作");
			}
		}
		boolean modifyLevel = PermissionManager.getAgentPermission().contains("/agent/agent/manager/updLevel.do");
		boolean modifyRate = PermissionManager.getAgentPermission().contains("/agent/agent/manager/updRate.do");
		boolean modifyName = PermissionManager.getAgentPermission().contains("/agent/agent/manager/updName.do");
		
		SysAccount account = accountService.getOne(id, StationUtil.getStationId());
		SysAccountInfo accountInfo = accountService.getAccountInfo(id);
		accountInfo.setEmail("");
		accountInfo.setQq("");
		accountInfo.setPhone("");
		accountInfo.setWechat("");
		accountInfo.setBankAddress("");
		accountInfo.setProvince("");
		accountInfo.setCity("");
		accountInfo.setBankName("");
		accountInfo.setCardNo("");
		accountInfo.setCardNoStatus(0l);
		map.put("memberInfo", accountInfo);
		map.put("member", account);
		
		map.put("modifyLevel", modifyLevel);
		map.put("modifyRate", modifyRate);
		map.put("modifyName", modifyName);
		
		map.put("levels", MemberLevelUtil.getStationLevels());
		AccountVo av = new AccountVo();
		av.setId(id);
		map.put("multiAgent1", accountService.getAgentMultiData(av));
		if (account.getParentNames() != null) {
			String parentNames = account.getParentNames();
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				map.put("parentNames", parentNames);
			}
		}
		return super.prepareLayout("/newpage/agent/agent_modify.jsp");
	}

	@RequestMapping("/modifyPwd")
	public String modifyPwd(Map<String, Object> map, Long id) {
		map.put("member", accountService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/agent/agent_modify_pwd.jsp");
	}

	@RequestMapping("/modifyZongDai")
	public String modifyZongDai(Map<String, Object> map, Long id) {
		map.put("member", accountService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/agent/agent_modify_zong_dai.jsp");
	}
	
	@RequestMapping("/movepage")
	public String movepage(Map<String, Object> map, Long id) {
		return super.prepareLayout("/newpage/agent/agent_move_agent.jsp");
	}
	
	@RequestMapping(value = "/moveAgent")
	@ResponseBody
	public void moveAgent() {
		String agentName = super.$("agentName");
		String agents = super.$("agents");
		String result = accountService.batchMoveAgent(StationUtil.getStationId(),agentName, agents,SysAccount.ACCOUNT_PLATFORM_AGENT);
		if(StringUtil.isEmpty(result)) {
			super.renderSuccess();
		}else {
			super.renderFailure(result);
		}
	}

	@RequestMapping("/view")
	public String viewDetail(Long id, Model model) {
		String ag = StationConfigUtil.get(StationConfig.onoff_ag_game);
		
		String bbin = StationConfigUtil.get(StationConfig.onoff_bbin_game);
		String bbindz = StationConfigUtil.get(StationConfig.onoff_bbin_dz_game);
		String mg = StationConfigUtil.get(StationConfig.onoff_mg_game);
		String pt = StationConfigUtil.get(StationConfig.onoff_pt_game);
		String qt = StationConfigUtil.get(StationConfig.onoff_qt_game);
		String ab = StationConfigUtil.get(StationConfig.onoff_ab_game);
		String og = StationConfigUtil.get(StationConfig.onoff_og_game);
		String ds = StationConfigUtil.get(StationConfig.onoff_ds_game);
		String isb = StationConfigUtil.get(StationConfig.onoff_isb_game);
		String ttg = StationConfigUtil.get(StationConfig.onoff_ttg_game);
		String mw = StationConfigUtil.get(StationConfig.onoff_mw_game);
		String cq9 = StationConfigUtil.get(StationConfig.onoff_cq9_game);
		String jdb = StationConfigUtil.get(StationConfig.onoff_jdb_game);
        String m8 = StationConfigUtil.get(StationConfig.onoff_m8_game);
        String ibc = StationConfigUtil.get(StationConfig.onoff_ibc_game);
        String m8h = StationConfigUtil.get(StationConfig.onoff_m8h_game);
        String bg = StationConfigUtil.get(StationConfig.onoff_bg_game);
        String vr = StationConfigUtil.get(StationConfig.onoff_vr_game);
        String ky = StationConfigUtil.get(StationConfig.onoff_ky_game);
        String ebet = StationConfigUtil.get(StationConfig.onoff_ebet_game);
        String th = StationConfigUtil.get(StationConfig.onoff_th_game);
        
		boolean isAG = StringUtils.equals(ag, "on");
		boolean isBBIN = false;
		if("on".equals(bbin) || "on".equals(bbindz)) {
			isBBIN = true;
		}
		boolean isMG = StringUtils.equals(mg, "on");
		boolean isPT = StringUtils.equals(pt, "on");
		boolean isQT = StringUtils.equals(qt, "on");
		boolean isAB = StringUtils.equals(ab, "on");
		boolean isOG = StringUtils.equals(og, "on");
		boolean isDS = StringUtils.equals(ds, "on");
		boolean isCQ9 = StringUtils.equals(cq9, "on");
		boolean isJDB = StringUtils.equals(jdb, "on");
		boolean isISB = StringUtils.equals(isb, "on");
		boolean isTTG = StringUtils.equals(ttg, "on");
		boolean isMW = StringUtils.equals(mw, "on");
        boolean isM8 = StringUtils.equals(m8, "on");
        boolean isIBC = StringUtils.equals(ibc, "on");
        boolean isM8H = StringUtils.equals(m8h, "on");
        boolean isBg = StringUtils.equals(bg, "on");
        boolean isVR = StringUtils.equals(vr, "on");
        boolean isKY = StringUtils.equals(ky, "on");
        boolean isEBET = StringUtils.equals(ebet, "on");
        boolean isTH = StringUtils.equals(th, "on");

		model.addAttribute("isAG", isAG);
		model.addAttribute("isBBIN", isBBIN);
		model.addAttribute("isMG", isMG);

		model.addAttribute("isPT", isPT);
		model.addAttribute("isQT", isQT);
		model.addAttribute("isAB", isAB);
		model.addAttribute("isOG", isOG);
		model.addAttribute("isDS", isDS);
		model.addAttribute("isCQ9", isCQ9);
		model.addAttribute("isJDB", isJDB);
		model.addAttribute("isISB", isISB);
		model.addAttribute("isTTG", isTTG);
		model.addAttribute("isMW", isMW);
        model.addAttribute("isM8", isM8);
        model.addAttribute("isM8H", isM8H);
        model.addAttribute("isIBC", isIBC);
        model.addAttribute("isBg", isBg);
        model.addAttribute("isVR", isVR);
        model.addAttribute("isKY", isKY);
        model.addAttribute("isEBET", isEBET);
        model.addAttribute("isTH", isTH);
        
		if (isAG || isBBIN || isMG || isPT || isQT || isAB || isOG || isDS || isCQ9 || isJDB|| isISB|| isMW|| isTTG || isM8 || isIBC || isM8H || isBg || isVR|| isKY || isEBET || isTH) {
			model.addAttribute("third", true);
		}

		if (id == null || id <= 0) {
			throw new ParameterException("账户不存在");
		}
		Long stationId = StationUtil.getStationId();
		Map map = accountService.getAccountAndDrawComById(id, stationId);
		if (map == null) {
			throw new ParameterException("账户不存在");
		}
		if (map.containsKey("parentNames")) {
			String parentNames = (String) map.get("parentNames");
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				model.addAttribute("parentNames", parentNames);
			}
		}
		// map.put("cardNo", yincang((String) map.get("cardNo"), 4));
		// map.put("userName", yincang((String) map.get("userName"), 1));
		boolean viewPerm = PermissionManager.getAgentPermission().contains("/agent/member/manager/viewAdvanced.do");
		if (!viewPerm) {
			map.put("email", "");
			map.put("qq", "");
			map.put("phone", "");
			map.put("wechat", "");
		}
		model.addAttribute("viewPerm", viewPerm);
		model.addAttribute("map", map);
		model.addAttribute("levels", MemberLevelUtil.getStationLevels());
		return super.goPage("/page/member/viewDetail.jsp");
	}
}
