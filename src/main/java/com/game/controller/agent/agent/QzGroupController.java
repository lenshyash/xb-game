package com.game.controller.agent.agent;

import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.Qzgroup;
import com.game.model.SysAccount;
import com.game.permission.annotation.SortMapping;
import com.game.service.QzGroupService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/agent/qzGroup")
public class QzGroupController extends BaseAgentController {

	@Autowired
	private QzGroupService qzGroupService;
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/agent/qz_group.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		Long stationId = StationUtil.getStationId();
		String groupName = $("groupName");
		Page<Map> Page = qzGroupService.getGroupPage(stationId,groupName);
		super.render(Page);
	}
	
	@ResponseBody
	@RequestMapping("/moveAccountToGroup")
	public void moveAccountToGroup(String groupName,String account) {
		qzGroupService.moveAccountToGroup(groupName,account);
		super.renderSuccess();
	}
	
	
	@RequestMapping("/setAccountGroup")
	public String setAccountGroup(Map<String, Object> map,String groupName) {
		Long stationId = StationUtil.getStationId();
		Qzgroup group = qzGroupService.getOneGroup(groupName,stationId);
		map.put("groupName", group.getName());
		return super.prepareLayout("/newpage/agent/set_account_group.jsp");
	}
	
	@RequestMapping("/add")
	public String add() {
		return super.prepareLayout("/newpage/agent/group_add.jsp");
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public void save(String groupName) {
		Long stationId = StationUtil.getStationId();

		Qzgroup group = qzGroupService.getOneGroup(groupName,stationId);
		if(StringUtil.isNotEmpty(group)) {
			throw new GenericException("群组名称不能重复");
		}
		String remark = $("remark");
		Qzgroup qzgroup = new Qzgroup();
		qzgroup.setName(groupName);
		qzgroup.setRemark(remark);
		qzgroup.setStationId(stationId);
		qzgroup.setStatus(Qzgroup.GROUP_STATUS_NORMAL);
		qzGroupService.save(qzgroup);
		super.renderSuccess();
		
	}
	
	@RequestMapping("/setCloseGroup")
	public String setCloseGroup(Map<String, Object> map,String groupName) {
		Long stationId = StationUtil.getStationId();
		Qzgroup group = qzGroupService.getOneGroup(groupName,stationId);
		map.put("groupName", group.getName());
		return super.prepareLayout("/newpage/agent/close_group.jsp");
	}
	
	@RequestMapping("/closeGroup")
	@ResponseBody
	public void closeGroup(String groupName) {
		Long stationId = StationUtil.getStationId();
		qzGroupService.closeGroup(stationId, groupName);
		super.renderSuccess();
		
	}
	
}
