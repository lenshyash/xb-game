package com.game.controller.agent.agent;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.AgentDepositLevel;
import com.game.model.platform.AgentLotteryModelConfig;
import com.game.model.platform.MemberBackwaterStrategy;
import com.game.service.AgentLotteryModelConfigService;
import com.game.service.MemberBackwaterStrategyLevelService;
import com.game.service.MemberBackwaterStrategyService;
import com.game.service.lottery.BcLotteryService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/lotteryModel")
public class AgentLotteryModelConfigController extends BaseAgentController {
	@Autowired
	private AgentLotteryModelConfigService agentLotteryModelConfigService;
	@Autowired
	private BcLotteryService bcLotteryService;
	
	@RequestMapping("/index")
	public String index() {
		
		super.getRequest().setAttribute("cpType", bcLotteryService.lotteryGroup(StationUtil.getStationId()));
		return super.prepareLayout("/newpage/lottery/lotteryModelConfig.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Integer type,String name) {
		Long stationId = StationUtil.getStationId();
		super.render(agentLotteryModelConfigService.page(type, stationId,name));
	}
	@RequestMapping("/modify")
	public String modify(Map<String, Object> map, Long id) {
		map.put("rollback", agentLotteryModelConfigService.getOne(id, StationUtil.getStationId()));
		return super.prepareLayout("/newpage/lottery/lotteryModelConfig_modify.jsp");
	}
	@RequestMapping("/save")
	@ResponseBody
	public void save( AgentLotteryModelConfig almc) {
		agentLotteryModelConfigService.save(almc, StationUtil.getStationId());
		super.renderSuccess();
		
	}
}
