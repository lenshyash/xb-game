package com.game.controller.agent;

import java.math.BigDecimal;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyDrawRecordService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.AGENT_CONTROL_PATH + "/drawrecord")
public class MnyDrawRecordController extends BaseAgentController {

	@Autowired
	private MnyDrawRecordService drawRecordService;

	@ResponseBody
	@RequestMapping("/index")
	public void index() {
		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		super.render(drawRecordService.getPage(mdrvo));
	}

	@ResponseBody
	@RequestMapping("/withdraw")
	public void withdraw() {
		long memberId = super.$long("memberId");
		String moneyStr = super.$("money");

		super.isNotNull(memberId, "会员不存在!");
		super.isNotNull(moneyStr, "金额格式错误!");

		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(memberId);
		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ARTIFICIAL);
		moneyVo.setRemark("提款处理中");

		if (StringUtil.isNumber(moneyStr)) {
			moneyVo.setMoney(new BigDecimal(moneyStr));
		}

		drawRecordService.withdraw(moneyVo);
	}
}
