package com.game.controller.dashboard;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysStation;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysStationService;
import com.game.util.DateUtil;
import com.game.util.MD5Util;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/dashboard")
public class DashboardController extends BaseAdminController {
	
	private static final Logger LOGGER = Logger.getLogger(DashboardController.class);

	private static final String Key = "4b538a3ea4ed4a828bdeacbdd2db850d";

	@Autowired
	private SysStationService stationService;

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/getStation")
	public void getStationList(HttpServletRequest request, HttpServletResponse response) {
		// 获取请求数据
		SortedMap<String, String> map = getRequestData(request);
		// 获得加密源
		String signSrc = getEncryptionSrc(map, Key);

		String sign = com.game.util.MD5Util.MD5(signSrc);

		if (sign.equals(map.get("sign"))) {
			SortedMap<String, String> ersultMap = new TreeMap<String, String>();
			ersultMap.put("haId", map.get("haId"));
			ersultMap.put("haNo", map.get("haNo"));
			ersultMap.put("respTime", DateUtil.getCurrentTime());
			List<SysStation> list = stationService.getAllActive();

			List<Map<String, String>> stationList = new ArrayList<Map<String, String>>();

			for (SysStation station : list) {
				Map<String, String> sMap = new HashMap<>();
				sMap.put("stationName", station.getName());
				sMap.put("stationNo", station.getFloder());
				stationList.add(sMap);
			}
			String json = JSONObject.toJSONString(stationList);
			ersultMap.put("data", json);
			String resultsignSrc = getEncryptionSrc(ersultMap, Key);
			ersultMap.put("sign", MD5Util.MD5(resultsignSrc));
			renderJson(ersultMap);
		} else {
			renderFailure();
		}
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/getDashboardData")
	public void getDashBoardData(HttpServletRequest request, HttpServletResponse response) {
		// 获取请求数据
		SortedMap<String, String> map = getRequestData(request);
		// 获得加密源
		String signSrc = getEncryptionSrc(map, Key);

		String sign = com.game.util.MD5Util.MD5(signSrc);
		if (sign.equals(map.get("sign"))) {

			String calYm = map.get("calYm");

			int year = Integer.valueOf(calYm.substring(0, 4));
			int month = Integer.valueOf(calYm.substring(4, calYm.length()));
			
			calendar = Calendar.getInstance();
			Date begin = FirstDay(year,month);
			Date end = LastDay(year,month);

			List<Map> list = dailyMoneyService.getGlobalReportForAdmin(begin, end);
			
			LOGGER.error("------------------------财务统计start---------------------------");
			logger.error("统计开始日期："+DateUtil.formatDate(begin, "yyyy-MM-dd HH:mm:ss"));
			logger.error("统计结束日期："+DateUtil.formatDate(end, "yyyy-MM-dd HH:mm:ss"));
			LOGGER.error("此集群的彩票财务数据："+ JSONObject.toJSONString(list));
			LOGGER.error("------------------------ 财务统计end ---------------------------");
			List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

			for (Map map2 : list) {
				Map<String, Object> map3 = new HashMap<>();
				map3.put("stationNo", map2.get("folder"));
				map3.put("totalIn", map2.get("lotBetAmount"));
				map3.put("totalOut", map2.get("lotWinAmount"));
				map3.put("typeCode", "lotGame");
				data.add(map3);
				Map<String, Object> map4 = new HashMap<>();
				map4.put("stationNo", map2.get("folder"));
				map4.put("totalIn", map2.get("sixBetAmount"));
				map4.put("totalOut", map2.get("sixWinAmount"));
				map4.put("typeCode", "sixGame");
				data.add(map4);
			}
			SortedMap<String, String> ersultMap = new TreeMap<String, String>();
			ersultMap.put("haNo", map.get("haNo"));
			ersultMap.put("calYm", calYm);
			String json = JSONObject.toJSONString(data);
			ersultMap.put("data", json);
			String signsrc = getEncryptionSrc(ersultMap, Key);
			ersultMap.put("sign", MD5Util.MD5(signsrc));
			LOGGER.error("此集群的彩票财务数据：" + JSONObject.toJSONString(ersultMap));
			renderJson(ersultMap);
		} else {
			renderFailure();
		}
	}

	private SortedMap<String, String> getRequestData(HttpServletRequest request) {
		Enumeration<String> enums = request.getParameterNames();
		SortedMap<String, String> map = new TreeMap<String, String>();

		while (enums.hasMoreElements()) {
			String paramName = (String) enums.nextElement();
			String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}

		return map;
	}

	private String getEncryptionSrc(SortedMap<String, String> map, String key) {
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = (Entry<String, String>) iterator.next();
			if (!"sign".equals(entry.getKey())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		buffer.append("key=").append(key);
		return buffer.toString();
	}

	static Calendar calendar = null;

	public static Date FirstDay(int year, int month) {
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		int maxday = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, maxday);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date date = calendar.getTime();
		return date;
	}

	public static Date LastDay(int year, int month) {
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		int minday = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, minday);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date date = calendar.getTime();
		return date;
	}

	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
	}
}
