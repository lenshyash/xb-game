package com.game.controller.member;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.AgentSignRecord;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysStationDomain;
import com.game.model.platform.AgentActivity;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentArticleService;
import com.game.service.ChatService;
import com.game.service.GetConfigService;
import com.game.service.SignService;
import com.game.service.SysAccountService;
import com.game.service.SysStationDomainService;
import com.game.util.AESUtil;
import com.game.util.AuthTokenUtil;
import com.game.util.DateUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationOnOffUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/index")
public class MemberIndexController extends BaseMemberController {

	@Autowired
	AgentArticleService articleService;

	@Autowired
	GetConfigService gcService;

	@Autowired
	private GetConfigService getConfigService;

	@Autowired
	private SignService signService;

	@Autowired
	private SysStationDomainService sysDomainService;

	@Autowired
	private SysAccountService sysAccountService;
	
	@Autowired
	private ChatService chatService;
	
	/**
	 * 体育
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/sport")
	public String sport() {
		String hgOnoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_sports_game);
		String syshgOnoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_hg_sport_system);
		String bbOnoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_bbty_sports);
		String ibcOnoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_ibc_game);
		if("off".equalsIgnoreCase(hgOnoff) && !"on".equals(bbOnoff) && !"on".equals(ibcOnoff) ) {
			return "redirect:/";
		}
		
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(11, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "sport");
		super.getRequest().setAttribute("isHgSysOnOff", hgOnoff);
		super.getRequest().setAttribute("ishgSportOnOff", syshgOnoff);
		super.getRequest().setAttribute("isTsOnOff", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports));
		super.getRequest().setAttribute("isBBTYOnOff", bbOnoff);
		String sportUrl = $("sportUrl");
		if (StringUtil.isNotEmpty(sportUrl)) {
			sportUrl = URLDecoder.decode(sportUrl);
			super.getRequest().setAttribute("sportUrl", sportUrl);
		}
		return goPage("/sport.jsp");
	}
	
	/**
	 * 电竞
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/esport")
	public String esport() {
		
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(11, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "esport");
		if (UserUtil.getCurrentUser() != null) {
			super.getRequest().setAttribute("accountno", UserUtil.getCurrentUser().getLoginAccount());
		}
		return goPage("/esport.jsp");
	}
	
	/**
	 * 体育
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_sport")
	public String _sport() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "_sport");
		return goPage("/_sport.jsp");
	}

	/**
	 * 真人
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/real")
	public String real() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(10, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "real");
		if (UserUtil.getCurrentUser() != null) {
			super.getRequest().setAttribute("accountno", UserUtil.getCurrentUser().getLoginAccount());
		}
		return goPage("/real.jsp");
	}
	
	/**
	 * 棋牌
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/openChess")
	public String openChess() {
		StationOnOffUtil.initConfig();
		return "/common/template/third/chess/kaiyuanChess.jsp";
	}
	/**
	 * 棋牌
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/chess")
	public String chess(HttpServletResponse response) {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(77, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "chess");
		if (UserUtil.getCurrentUser() != null) {
			super.getRequest().setAttribute("accountno", UserUtil.getCurrentUser().getLoginAccount());
		}
		response.addHeader("x-frame-options", "SAMEORIGIN");
		return goPage("/chess.jsp");
	}
	
	/**
	 * 真人
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_real")
	public String _real() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "_real");
		if (UserUtil.getCurrentUser() != null) {
			super.getRequest().setAttribute("accountno", UserUtil.getCurrentUser().getLoginAccount());
		}
		return goPage("/_real.jsp");
	}

	/**
	 * 电子
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/egame")
	public String egame(String param) {
		StationOnOffUtil.initConfig();
		if (param != null) {
			super.getRequest().setAttribute("isAgNav", true);
		}
		super.getRequest().setAttribute("helplst", gcService.getArticle(12, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "egame");
		super.getRequest().setAttribute("egameNavBar", param);

		return goPage("/egame.jsp");
	}

	/**
	 * 电子
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_egame")
	public String _egame(String param) {
		StationOnOffUtil.initConfig();
		if (param != null) {
			super.getRequest().setAttribute("isAgNav", true);
		}
		super.getRequest().setAttribute("navTab", "egame");
		return goPage("/_egame.jsp");
	}

	/**
	 * 第二版电子
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/egameV2")
	public String egameV2(String param) {
		StationOnOffUtil.initConfig();
		if (param != null) {
			super.getRequest().setAttribute("isAgNav", true);
		}
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/third/page/egameV2.jsp";
	}

	/**
	 * 六合彩
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/marksix")
	public String marksix(String lotCode) {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(9, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "marksix");
		super.getRequest().setAttribute("lotCode", lotCode);
		return goPage("/marksix.jsp");
	}

	/**
	 * 彩票中心
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/games")
	public String games(String lotCode) {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(9, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "games");
		super.getRequest().setAttribute("lotCode", lotCode);
		return goPage("/games.jsp");
	}

	/**
	 * 彩票中心
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_games")
	public String _games() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "games");
		return goPage("/_games.jsp");
	}

	/**
	 * 手机下注
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/phone")
	public String phone() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(9, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "phone");
		Long stationId = StationUtil.getStationId();
		String qrCodeios = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_ios);
		String qrCodeandroid = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_android);
		super.getRequest().setAttribute("appQRCodeLinkIos", qrCodeios);
		super.getRequest().setAttribute("appQRCodeLinkAndroid", qrCodeandroid);
		return goPage("/phone.jsp");
	}

	/**
	 * 手机下注
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_phone")
	public String _phone() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "phone");
		return goPage("/_phone.jsp");
	}

	/**
	 * 优惠活动
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/active")
	public String active() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("helplst", gcService.getArticle(21, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "active");
		return goPage("/active.jsp");
	}

	/**
	 * 优惠活动
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_active")
	public String _active() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "active");
		return goPage("/_active.jsp");
	}

	/**
	 * 合法牌照
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/hfpz")
	public String hfpz() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "hfpz");
		return goPage("/hfpz.jsp");
	}

	/**
	 * 线路检测
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/xljc")
	public String xljc() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "xljc");
		String domain = "var domain = "
				+ JSONObject.toJSONString(sysDomainService.getDomain(StationUtil.getStationId())) + ";";
		super.getRequest().setAttribute("domain", domain);
		return goPage("/xljc.jsp");
	}

	/**
	 * 线路检测数据
	 */
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/xlDomain")
	public void xlDomain() {
		String domain = "var domain = "
				+ JSONObject.toJSONString(sysDomainService.getDomain(StationUtil.getStationId())) + ";";
		super.renderJson(domain);
	}

	/**
	 * 线路检测
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_xljc")
	public String _xljc() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "xljc");
		return goPage("/_xljc.jsp");
	}

	/**
	 * 登录新窗口
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/loginInterface")
	public String loginInterface() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("navTab", "loginInterface");
		return goPage("/loginInterface.jsp");
	}

	/**
	 * 转盘活动
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/luckyLottery")
	public String luckyLottery() {
		StationOnOffUtil.initConfig();
		if (UserUtil.isLogin()) {
			Map map = sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId());
			Object o = map.get("score");
			String userScore = "0.00";
			if (o != null) {
				userScore = String.valueOf(o);
			}
			super.getRequest().setAttribute("userScore", userScore);
		}
		return SystemConfig.SOURCE_FOLDER_COMMON + "/include/active2.jsp";
	}

	/**
	 * 手机二维码
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/qrcode")
	public String qrcode() {
		StationOnOffUtil.initConfig();
		return goPage("/qrcode.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/systemmaintenance")
	public String systemmaintenance(HttpServletRequest request) {
		Long stationId = StationUtil.getStationId();
		request.setAttribute("system_maintenance", StationConfigUtil.get(stationId, StationConfig.system_maintenance));
		request.setAttribute("isSysOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_system));
		String kfUrl = StationConfigUtil.get(stationId, StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		request.setAttribute("kfUrl", kfUrl);
		return SystemConfig.SOURCE_FOLDER_COMMON + "/erro/systemmaintenance.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/preferential")
	public String preferential() {
		StationOnOffUtil.initConfig();
		List<AgentActivity> prelst = getConfigService.getPreferential(StationUtil.getStationId());
		super.getRequest().setAttribute("prelst", prelst);
		super.getRequest().setAttribute("navTab", "preferential");
		return goPage("/preferential.jsp");
	}

	/**
	 * 关于我们
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/help")
	public String help(Long id) {
		StationOnOffUtil.initConfig();
		List<Map> hplst = articleService.getStationAtcs(StationUtil.getStationId());
		String curCode = super.$("curCode");
		if (StringUtil.isNotEmpty(curCode)) {
			for (Map map : hplst) {
				if (StringUtil.isEmpty(map.get("id"))) {
					break;
				}
				if (curCode.equals(map.get("id").toString())) {
					super.getRequest().setAttribute("curContent", map.get("content"));
					break;
				}
			}
		} else {
			for (Map map : hplst) {
				String code = map.get("code") == null ? "" : map.get("code").toString();
				if (StringUtil.isEmpty(code)) {
					break;
				}
				if ("4".equals(code)) {
					super.getRequest().setAttribute("lmfaContent", map.get("content"));
				} else if ("5".equals(code)) {
					super.getRequest().setAttribute("lmxyContent", map.get("content"));
				}
			}
		}
		super.getRequest().setAttribute("type", getRequest().getParameter("type"));
		super.getRequest().setAttribute("hplst", hplst);
		super.getRequest().setAttribute("curCode", curCode);
		super.getRequest().setAttribute("navTab", "help");
		if(Validator.isNotNull(id)) {
			super.getRequest().setAttribute("hid", id);
			super.getRequest().setAttribute("content", articleService.getOne(id));
		}
		return goPage("/help.jsp");
	}

	/**
	 * code:1:关于我们,2:取款帮助,3:存款帮助,4:合作伙伴->联盟方案,5:合作伙伴->联盟协议,6:联系我们,7:常见问题 title:
	 * 为空就取这个值
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/rookieHelp")
	public String rookieHelp(Integer code) {
		StationOnOffUtil.initConfig();
		String content = "";
		String title = "暂无内容";
		List<Map> hplst = articleService.getStationAtcs(StationUtil.getStationId());
		if (code != null) {
			for (Map map : hplst) {
				if (code.toString().equals(map.get("code").toString())) {
					content = (String) map.get("content");
					title = (String) map.get("title");
					break;
				}
			}
		}

		String stationName = StationUtil.getDomainStationName();
		if(StringUtil.isNotEmpty(stationName)) {
			content = content.replace("该文本需要替换网站名称", stationName);
		}
		super.getRequest().setAttribute("content", content);
		super.getRequest().setAttribute("title", title);
		super.getRequest().setAttribute("code", code);
		super.getRequest().setAttribute("navTab", "rookieHelp");
		return goPage("/help.jsp");
	}

	/**
	 * code:1:关于我们,2:取款帮助,3:存款帮助,4:合作伙伴->联盟方案,5:合作伙伴->联盟协议,6:联系我们,7:常见问题 title:
	 * 为空就取这个值
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_rookieHelp")
	public String _rookieHelp(Integer code) {
		StationOnOffUtil.initConfig();
		String content = "";
		String title = "暂无内容";
		List<Map> hplst = articleService.getStationAtcs(StationUtil.getStationId());
		if (code != null) {
			for (Map map : hplst) {
				if (code.toString().equals(map.get("code").toString())) {
					content = (String) map.get("content");
					title = (String) map.get("title");
					break;
				}
			}
		}
		super.getRequest().setAttribute("content", content);
		super.getRequest().setAttribute("title", title);
		super.getRequest().setAttribute("code", code);
		super.getRequest().setAttribute("navTab", "rookieHelp");
		return goPage("/_help.jsp");
	}

	/**
	 * 签到
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/sign")
	public String sign() {
		HttpServletRequest request = super.getRequest();
		request.setAttribute("now", (new Date()).getTime());
		SysAccount account = (SysAccount) UserUtil.getCurrentUser();
		if (account != null) {
			List<AgentSignRecord> record = signService.getMemberSignRecord();
			// request.setAttribute("signed", false);
			int signCount = 0;
			Date lastSignDate = account.getLastSignDate();
			Date today = DateUtil.dayFirstTime(new Date(), 0);
			boolean signed = false;
			if (lastSignDate != null && today.getTime() <= lastSignDate.getTime()) {// 已签到
				signed = true;
			}
			if (lastSignDate != null && today.getTime() - lastSignDate.getTime() <= 24 * 60 * 60 * 1000) {// 判断是否是连续签到
				signCount = StringUtil.toInt(account.getSignCount());
			}
			request.setAttribute("signed", signed);
			request.setAttribute("signCount", signCount);
			request.setAttribute("signAccount", account.getAccount());
			request.setAttribute("record", JSONObject.toJSONString(record));
		}
		return goPage("/sign.jsp");
	}

	/**
	 * 签到
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/signNew")
	public String signNew() {
		StationOnOffUtil.initConfig();
		HttpServletRequest request = super.getRequest();
		request.setAttribute("now", (new Date()).getTime());
		SysAccount account = (SysAccount) UserUtil.getCurrentUser();
		if (account != null) {
			List<AgentSignRecord> record = signService.getMemberSignRecord();
			// request.setAttribute("signed", false);
			int signCount = 0;
			Date lastSignDate = account.getLastSignDate();
			Date today = DateUtil.dayFirstTime(new Date(), 0);
			boolean signed = false;
			if (lastSignDate != null && today.getTime() <= lastSignDate.getTime()) {// 已签到
				signed = true;
			}
			if (lastSignDate != null && today.getTime() - lastSignDate.getTime() <= 24 * 60 * 60 * 1000) {// 判断是否是连续签到
				signCount = StringUtil.toInt(account.getSignCount());
			}
			request.setAttribute("signed", signed);
			request.setAttribute("signCount", signCount);
			request.setAttribute("signAccount", account.getAccount());
			request.setAttribute("record", JSONObject.toJSONString(record));
		}
		// return goPage("/sign.jsp");
		return SystemConfig.SOURCE_FOLDER_COMMON + "/qd/sign.jsp";
	}

	/**
	 * 手机站
	 * 
	 * @return
	 */
	@RequestMapping("/phoneCommon")
	@NotNeedLogin
	public String phoneCommon() {
		StationOnOffUtil.initConfig();
		return SystemConfig.SOURCE_FOLDER_COMMON + "/modelCommon/iframephone/phone.jsp";
	}

	/**
	 * 第二版手机页面
	 * 
	 * @return
	 */
	@RequestMapping("/phone2")
	@NotNeedLogin
	public String phone2() {
		StationOnOffUtil.initConfig();
		return SystemConfig.SOURCE_FOLDER_COMMON + "/modelCommon/phone2/phone.jsp";
	}

	/**
	 * 注册公有 rA A站公用注册 regGuestA A站公用免费试玩 agt 代理公用注册 regGuest B站公用免费试玩 其他（r） B站公用注册
	 */
	@RequestMapping("/styleReg")
	@NotNeedLogin
	public String styleReg(String type) {
		StationOnOffUtil.initConfig();
		if (type.equals("rA")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/regA.jsp";
		} else if (type.equals("regGuestA")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/registerGuestA.jsp";
		} else if (type.equals("agt")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/agtreg.jsp";
		} else if (type.equals("regGuest")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/registerGuest.jsp";
		} else if (type.equals("rB")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/regB.jsp";
		}else if (type.equals("agtB")) {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/agtregB.jsp";
		}else {
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/publicREG/reg.jsp";
		}
	}

	/**
	 * dns
	 */
	@RequestMapping("/dns")
	@NotNeedLogin
	public String dns() {
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/mysdns/dns.jsp";
	}

	/**
	 * 手机版导航页
	 */
	@RequestMapping("/phoneNav")
	@NotNeedLogin
	public String phoneNav() {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("qrcodeAndroid", StringUtil.trim2Null(StationConfigUtil.get(StationConfig.app_qr_code_link_android)));// Android版本的app二维码地址
		super.getRequest().setAttribute("navTab", "phoneNav");
		return goPage("/wapLogin.jsp");
	}

	/**
	 * 公告页面
	 */
	@RequestMapping("/news")
	@NotNeedLogin
	public String news() {
		StationOnOffUtil.initConfig();
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/news/news.jsp";
	}

	/**
	 * 红包页面
	 */
	@RequestMapping("/redPackage")
	@NotNeedLogin
	public String redPackage() {
		StationOnOffUtil.initConfig();
		return SystemConfig.SOURCE_FOLDER_COMMON + "/redpackage/index.jsp";
	}
	
	/**
	 * 聊天室页面
	 */
	@RequestMapping("/chat")
	@NotNeedLogin
	public String chat(String platId,String platToken) {
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("platId", platId);
		super.getRequest().setAttribute("platToken", platToken);
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/chat.jsp";
	}

	
	@RequestMapping("/gohtml/{page}")
	@NotNeedLogin
	public String gohtml(@PathVariable String page) {
		return goPage(page + ".html");
	}
	/**
	 * 获取聊天室平台token
	 */
	@ResponseBody
	@RequestMapping("getChaToken")
	@NotNeedLogin
	public void getPlatChaToken(HttpServletRequest request) {
		 String chatOn = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat);
    	 if(chatOn==null || !"on".equals(chatOn)){
    		 throw new GenericException("聊天室功能未开启");
    	 }
		 Map<String, Object> content = new HashMap<String, Object>();
		 String platToken =  StationUtil.getDomainPlatToken();
		 Map<String,String> chatMap = new HashMap<>();
		 if(StringUtil.isEmpty(platToken)){
			 chatMap = AuthTokenUtil.getChatToken();
			 String newToken = chatMap.get("newToken");
			 if(StringUtil.isEmpty(newToken)){
				 throw new GenericException("获取聊天室token失败");
			 }else{
				 String url = WebUtil.getDomain(request.getRequestURI());
				 SysStationDomain domain =  sysDomainService.getDomainByFolder(url,StationUtil.getDomainFolder(),StationUtil.getStationId());
				 //保存token到当前域名
				 if(domain !=null){
					 sysDomainService.updateDomainChatToken(domain.getId(), newToken);
				 }
				 platToken = newToken;
			 }
		 }
		 String[] temp = platToken.split(",");
		 if(UserUtil.isLogin()){
			 if(temp!=null&&temp.length<1){
				 throw new GenericException("聊天室token格式错误");
			 }
			 String securitySalt = "lks234#$(gfdg>r4w";
			 //组装用户token
			 User user =  UserUtil.getCurrentUser();
			 MemberLevelBase level = UserUtil.getMemberLevel();
			 MemberLevelBase defaultLevel = MemberLevelUtil.getDefaultLevel();
			 SysAccount account = sysAccountService.getOne(user.getSysUserId(), StationUtil.getStationId());
			 Map map=new HashMap<>();
		     map.put("token",temp[1]);
		     map.put("platformId",temp[0]);
		     map.put("name",UserUtil.getUserAccount());
		     //没有用户等级采用站点默认等级
		     map.put("level",0);
		     if(level !=null && StringUtil.isNotEmpty(level.getLevelName())){
		    	 map.put("grade", level.getLevelName());
		     }else{
		    	 map.put("grade", defaultLevel.getLevelName());
		     }
		     map.put("accountId", UserUtil.getUserId());
		     map.put("accountType", UserUtil.getType());
		     String json= JSONObject.toJSONString(map);
		     String s = AESUtil.encrypt(json, securitySalt);
		     content.put("content", s);
		 }else{
			
			 content.put("content", temp[0]);
		 }
	     content.put("success", true);
	     
	     super.renderJson(content);
	}
	
	
	/**
	 * 聊天室
	 */
	@ResponseBody
	@RequestMapping("loginchat")
	public void loginchat() {
		super.renderJson(chatService.login());
	}
	/**
	 * 真人跳转
	 */
	@NotNeedLogin
	@RequestMapping("otherLive")
	public String otherLive() {
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/third/otherlive/index.jsp";
	}
	
	/**
	 * 代理界面
	 */
	@RequestMapping("/dailiPage")
	@NotNeedLogin
	public String dailiPage() {
		StationOnOffUtil.initConfig();
		//获取模板号
		return goPage("/jztDaili.jsp");
		
	}
	
}
