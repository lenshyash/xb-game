package com.game.controller.member;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.game.core.SystemConfig;
import com.game.permission.annotation.NotNeedLogin;
import com.game.util.RSAUtils;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH)
public class MemberUtilsController extends BaseMemberController {

	@NotNeedLogin
	@RequestMapping("/makersakey")
	public String index(Model model) throws Exception {
		HashMap<String, Object> keyMap = RSAUtils.getKeys();
		String publicKey = RSAUtils.getPublicKey(keyMap);
		String privateKey = RSAUtils.getPrivateKey(keyMap);
		model.addAttribute("publicKey", publicKey);
		model.addAttribute("privateKey", privateKey);

		// return super.goPage("newpage/utils/makersakey.jsp");
		return "/common/template/member/utils/makersakey.jsp";
	}

}
