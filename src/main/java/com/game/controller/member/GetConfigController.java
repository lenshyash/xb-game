package com.game.controller.member;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.dao.lottery.BcLotteryDao;
import com.game.model.AgentAppKeyConfig;
import com.game.model.SysAccount;
import com.game.model.lottery.BcLottery;
import com.game.model.platform.AgentArticle;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentAppKeyConfigService;
import com.game.service.GetConfigService;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationOnOffUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping("getConfig")
public class GetConfigController extends BaseMemberController {

	@Autowired
	private GetConfigService getConfigService;
	@Autowired
	private BcLotteryDao lotteryDao;
	@Autowired
	private AgentAppKeyConfigService appKeyConfigService;

	/**
	 * 公告接口
	 * 
	 * @param code
	 * 公告类型
	 */
	@RequestMapping("/getArticle")
	@ResponseBody
	@NotNeedLogin
	public void getArticle(Integer code) {
		List<AgentArticle> list= null;
		Long stationId = StationUtil.getStationId();
		//判断是否是试玩账号
		SysAccount acc=(SysAccount)UserUtil.getCurrentUser();
		if (acc!=null && Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
			list = getConfigService.getArticle(25, stationId);
			if(list!=null && list.size()>0){
				super.renderJson(list);
				return;
			}
		}
		list =getConfigService.getArticle(code, stationId);
		super.renderJson(list);
	}
	/**
	 * 获取公告列表
	 * 
	 * @param code
	 * 公告类型
	 */
	@RequestMapping("/getArticleList")
	@ResponseBody
	@NotNeedLogin
	public void getArticleList(Integer code) {
		Long stationId = StationUtil.getStationId();
		super.renderJson(getConfigService.getArticleList(code, stationId));
	}
	/**
	 * 获取指定公告
	 * 
	 * @param code
	 * 公告类型
	 */
	@RequestMapping("/getArticleById")
	@ResponseBody
	@NotNeedLogin
	public void getArticleById(Long id) {
		super.renderJson(getConfigService.getArticleById(id));
	}
	
	/**
	 * 优惠活动
	 */
	@RequestMapping("/getPreferential")
	@ResponseBody
	@NotNeedLogin
	public void getPreferential() {
		Long stationId = StationUtil.getStationId();
		super.renderJson(getConfigService.getPreferential(stationId));
	}

	/**
	 * 图片轮播
	 */
	@RequestMapping("/getLunBo")
	@ResponseBody
	@NotNeedLogin
	public void getLunBo() {
		Long stationId = StationUtil.getStationId();
		Integer code = 1;
		super.renderJson(getConfigService.getLunBo(stationId,code));
	}
	
	/**
	 * 中奖数据
	 */
	@RequestMapping("/getWinData")
	@ResponseBody
	@NotNeedLogin
	public void getWinData() {
		Long stationId = StationUtil.getStationId();
		super.renderJson(getConfigService.getWinData(stationId));
	}

	/**
	 * 游戏大厅
	 */
	@RequestMapping(value = "/getLotList")
	@ResponseBody
	@NotNeedLogin
	public void lotteryList() {
		super.renderJson(getConfigService.getLotList(StationUtil.getStationId()));
	}

	/**
	 * 获取后台配置的手机网站地址 code:0 请求失败 code:1 请求成功
	 * 
	 * @return
	 */
	@RequestMapping("/getPhoneSite")
	@ResponseBody
	@NotNeedLogin
	public String getPhoneSite() {
		Map json = new HashMap<>();
		if(StringUtils.equals((String) super.getRequest().getSession().getAttribute(BusinessConstant.MOBILE_JUMP_SESSION_KEY), "pc")){
			//手机上，添加跳转页面的返回pc端
			json.put("code", 0);
			json.put("url", "");
			return JsonUtil.toJson(json);
		}
		String phoneUrl = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.basic_info_url_phone);
		if (StringUtils.isEmpty(phoneUrl)) {
			phoneUrl = MobileUtil.getMobileDomain();
		}
		if (phoneUrl != null) {
			json.put("code", 1);
			json.put("url", phoneUrl);
			return JsonUtil.toJson(json);
		}
		json.put("code", 0);
		json.put("url", "");
		return JsonUtil.toJson(json);
	}

	/**
	 * 游戏大厅模板
	 * 
	 * @param code
	 * @return 模板2支持换背景色
	 */
	@RequestMapping("/gameModel")
	@NotNeedLogin
	public String gameModel(Integer code, String bgColor, HttpServletRequest request, String ticai) {
		if (code == null) {
			code = 1;
		}
		if (StringUtils.isEmpty(ticai)) {
			request.setAttribute("ticai", false);
		} else {
			request.setAttribute("ticai", true);
		}
		request.setAttribute("bgColor", "#" + bgColor);
		request.setAttribute("isLogin", UserUtil.isLogin());
		request.setAttribute("lotteryVersion", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		return "/common/gameModel/gameStyle_" + code + "/game.jsp";
	}

	@RequestMapping("/gameRule")
	@NotNeedLogin
	public String gameRule(String code, HttpServletRequest request) {
		if (StringUtils.isEmpty(code)) {
			code = "CQSSC";
		}
		boolean isLhc = "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_liu_he_cai));
		List<BcLottery> activeList = lotteryDao.find(StationUtil.getStationId(), BusinessConstant.status_normal, BusinessConstant.status_normal, null, null, BusinessConstant.lottery_identify_V1);
		if (activeList != null && activeList.size() > 0) {
			request.setAttribute("isLhc", isLhc);
			request.setAttribute("code", code);
			return "/common/gameModel/gameRule/rule.jsp";
		} else {

			StationOnOffUtil.initBJSCSetting();
			request.setAttribute("lotteryV2List", activeList);
			request.setAttribute("caipiao_version", StationConfigUtil.get(StationConfig.lottery_page_version));
			request.setAttribute("website_name", StationUtil.getGloalStationName());
			request.setAttribute("caipiao", StationConfigUtil.get(StationConfig.onoff_du_li_cai_piao));
			request.setAttribute("mark_on_off", StationConfigUtil.get(StationConfig.onoff_liu_he_cai));
			return SystemConfig.SOURCE_FOLDER_COMMON + "/template/lottery/jimei/jsp/ruleIndex.jsp";
		}

	}

	@RequestMapping("/appInfo")
	@NotNeedLogin
	@ResponseBody
	public String appInfo(String sKeyVersion) {
		JSONObject obj = new JSONObject();
		if (StringUtils.isEmpty(sKeyVersion)) {
			return obj.toJSONString();
		}
		AgentAppKeyConfig config = appKeyConfigService.getVersionAndStationId(sKeyVersion, StationUtil.getStationId());
		if (config != null) {
			obj.put("versioninfo", config.getStatus());
			obj.put("appurl", config.getUrl());
		}
		return obj.toJSONString();
	}

}
