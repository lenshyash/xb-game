package com.game.controller.member.api;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentBaseConfigService;
import com.game.service.SysAccountService;
import com.game.service.SysApiDepositService;
import com.game.util.BigDecimalUtil;
import com.game.util.MD5Util;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/api")
public class SysDepositController extends BaseMemberController {

	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private SysAccountService accountService;

	@Autowired
	private SysApiDepositService apiDepositService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public void deposit(String account, String money, String orderId, String remark, String betMultiple, String sign) {
		if (StringUtils.isEmpty(account) || StringUtils.isEmpty(money) || StringUtils.isEmpty(sign)
				|| StringUtils.isEmpty(orderId)) {
			renderFailure("参数错误");
			return;
		}
		BigDecimal mon = BigDecimalUtil.toBigDecimalDefaultZero(money);
		if (mon.compareTo(BigDecimal.ZERO) <= 0) {
			renderFailure("金额必须大于0");
			return;
		}
		BigDecimal betMul = BigDecimalUtil.toBigDecimalDefaultZero(betMultiple);
		if (betMul.compareTo(BigDecimal.ZERO) < 0) {
			renderFailure("打码量倍数不能小于0");
			return;
		}
		Long stationId = StationUtil.getStationId();
		String key = agentBaseConfigService.getSettingValueByKey(StationConfig.sys_api_deposit_key.name(), stationId);
		if (StringUtils.isEmpty(key)) {
			renderFailure("请联系客服设置系统充值接口KEY");
			return;
		}
		String ips = agentBaseConfigService.getSettingValueByKey(StationConfig.sys_api_deposit_ips.name(), stationId);
		if (StringUtils.isEmpty(ips)) {
			renderFailure("请联系客服设置系统充值接口IP白名单");
			return;
		}
		String ip = UserUtil.getIpAddress();
		if (("," + ips + ",").indexOf("," + ip + ",") < 0) {
			renderFailure("该IP没有访问接口的权限");
			return;
		}
		String signOrg = account + money + orderId;
		if (StringUtils.isNotEmpty(betMultiple)) {
			signOrg += betMultiple;
		}
		if (StringUtils.isNotEmpty(remark)) {
			signOrg += remark;
		}
		signOrg = MD5Util.MD5(signOrg + key);
		if (!StringUtils.equals(sign, signOrg)) {
			renderFailure("验签失败");
			return;
		}
		try {
			SysAccount acc = accountService.findOneByAccountAndStationId(account, stationId);
			apiDepositService.deposit(stationId, account, acc.getId(), ip, mon, betMul, orderId, remark);
			renderSuccess();
		} catch (Exception e) {
			renderFailure(e.getMessage());
			return;
		}
	}
}
