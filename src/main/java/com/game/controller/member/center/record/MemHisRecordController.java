package com.game.controller.member.center.record;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.service.LotteryService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyDrawRecordService;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/record/hisrecord")
public class MemHisRecordController extends BaseMemberController {
	@Autowired
	private MnyDrawRecordService drawRDService;
	@Autowired
	private MnyComRecordService comRDService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("betHisFlag", "class=\"current\"");
		return "/common/template/member/center/record/hisrecorddemo.jsp";
	}
	
	/**
	 * c001独立模版交易记录
	 * type 1充值，2取款
	 * @return
	 */
	@RequestMapping("/cPage")
	public String cPage(Integer type){
		if(type == null){type = 1;}
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		Map moneyType = MixUtil.newHashMap();
		for (MoneyRecordType types : MoneyRecordType.values()) {
			moneyType.put(types.getType(), types.getName());
		}
		super.getRequest().setAttribute("moneyType", JsonUtil.toJson(moneyType));
		return "/common/template/lottery/jimei/jsp/v_1/hisrecord.jsp";
	}
	
	
	/**
	 * 彩票第三版充值取款记录
	 * @param model
	 * @return
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model,Integer type){
		model.addAttribute("mingxi", "class=\"on\"");
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		if(type == null){type = 1;}
		Map map = model.asMap();
		String onlinePay = (String) map.get("onlinePay");
		if(!StringUtils.equals(onlinePay, "on")){
			type = 2;
		}
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		Long stationId = StationUtil.getStationId();
		Long accountId = SysUtil.getUserId();
		super.getRequest().setAttribute("comTotalMoney", comRDService.getComTotalMoney(stationId,accountId));	//总充值
		super.getRequest().setAttribute("drawTotalMoney", drawRDService.getDrawTotalMoney(accountId,stationId));	//总提款
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		return "/common/template/lottery/lecai/jsp/user/member_mingxi.jsp";
	}
	/**
	 * 大发版彩票充值取款记录
	 * @param model
	 * @return
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model,Integer type){
		model.addAttribute("mingxi", "class=\"on\"");
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		if(type == null){type = 1;}
		Map map = model.asMap();
		String onlinePay = (String) map.get("onlinePay");
		if(!StringUtils.equals(onlinePay, "on")){
			type = 2;
		}
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		Long stationId = StationUtil.getStationId();
		Long accountId = SysUtil.getUserId();
		super.getRequest().setAttribute("comTotalMoney", comRDService.getComTotalMoney(stationId,accountId));	//总充值
		super.getRequest().setAttribute("drawTotalMoney", drawRDService.getDrawTotalMoney(accountId,stationId));	//总提款
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_mingxi.jsp";
	}
	@RequestMapping("/drawrdpg")
	public String drawrdpg() {
		return "/common/template/member/center/record/hisrecorddemo.jsp";
	}

	@ResponseBody
	@RequestMapping("/drawrd")
	public void drawrd() {
		Long status = super.$long("status");
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		mdrvo.setAccountId(UserUtil.getUserId());
		mdrvo.setStationId(StationUtil.getStationId());
		mdrvo.setStatus(status);
		mdrvo.setBegin(DateUtil.toDate(startTime));
		mdrvo.setEnd(DateUtils.addDays(DateUtil.toDate(endTime), 1));
		super.renderJson(drawRDService.getPage2(mdrvo));
	}
	
	
	@ResponseBody
	@RequestMapping("/realtslog")
	public void realtslog() {
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		String type = super.$("type");
		String status = super.$("status");
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("stationId", StationUtil.getStationId());
		params.put("accountId", UserUtil.getUserId());
		params.put("type", type);
		params.put("status", status);
		params.put("startTime", DateUtil.toDate(startTime));
		params.put("endTime", DateUtils.addDays(DateUtil.toDate(endTime), 1));
		super.renderJson(drawRDService.realtslog(params));
	}

	/**
	 * 会员充值记录
	 */
	@ResponseBody
	@RequestMapping("/comrd")
	public void comrdc() {
		Long status = super.$long("status");
		Long type = super.$long("type");
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setAccountId(UserUtil.getUserId());
		mcrVo.setStatus(status);
		mcrVo.setType(type);
		mcrVo.setBegin(DateUtil.toDate(startTime));
		mcrVo.setEnd(DateUtils.addDays(DateUtil.toDate(endTime), 1));
		super.renderJson(comRDService.getPage2(mcrVo));
	}
}
