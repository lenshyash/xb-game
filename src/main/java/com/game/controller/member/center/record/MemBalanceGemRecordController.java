package com.game.controller.member.center.record;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MemberBalanceGemRecord;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.service.LotteryService;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.MnyMoneyRecordService;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/record/balancegem")
public class MemBalanceGemRecordController extends BaseMemberController {

	@Autowired
	private MemberBalanceGemRecordService balanceGemRecordService;
	@Autowired
	MnyMoneyRecordService mnyMoneyService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("betHisFlag", "class=\"current\"");
		return "/common/template/member/center/record/balancegem_record.jsp";
	}
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model,Integer type) {
		model.addAttribute("balancegem", "class=\"on\"");
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/balancegem_record.jsp";
	}
	/**
	 *彩票v2-1版
	 * @param model
	 * @return
	 */
	@RequestMapping("/cp2Page")
	public String cp2Page(Model model,Integer type){
		model.addAttribute("balancegem", "class=\"on\"");
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/jimei/jsp/v_1/balancegem_record.jsp";
	}
	
	/**
	 * 彩票第三版充值取款记录
	 * @param model
	 * @return
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model,Integer type){
		model.addAttribute("balancegem", "class=\"on\"");
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/user/balancegem_record.jsp";
	}
	
	/**
	 * 会员账变记录
	 */
	@ResponseBody
	@RequestMapping("/list")
	public void sportrd() {
		String begin = super.$("startTime");
		String end = super.$("endTime");
		Long type = super.$long("type");
		//默认查询今日记录
		if(StringUtil.isEmpty(begin)){
			begin = DateUtil.getCurrentDate()+" 00:00:00";
		}
		if(StringUtil.isEmpty(end)){
			end = DateUtil.getCurrentDate()+" 23:59:59";
		}
		MemberBalanceGemRecord mbg = new MemberBalanceGemRecord();
		mbg.setAccountId(UserUtil.getUserId());
		mbg.setAccount(UserUtil.getUserAccount());
		mbg.setStationId(StationUtil.getStationId());
		mbg.setType(type);
		mbg.setStauts(MemberBalanceGemRecord.STATUS_SUCCESS);
		super.renderJson(balanceGemRecordService.getPage(mbg, begin, end));
	}
}
