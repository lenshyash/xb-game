package com.game.controller.member.center.banktrans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.service.*;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.AgentDepositOnline;
import com.game.model.MnyComRecord;
import com.game.model.SysStation;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyVo;
import com.game.util.LotteryUtils;
import com.game.util.Snowflake;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/banktrans/deposit")
public class MemOnlineDepositController extends BaseMemberController {
	@Autowired
	private MnyComRecordService mcrService;
	@Autowired
	private AgentDepositBankService bankService;
	@Autowired
	private AgentDepositFastService fastService;
	@Autowired
	private AgentDepositOnlineService onlineService;
	@Autowired
	private AgentDepositvirtualService virtualService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isPaymentSort", StationConfigUtil.get( StationConfig.payment_sort));
		super.getRequest().setAttribute("isRateOnOff", StationConfigUtil.get( StationConfig.onoff_member_rate_random));
		super.getRequest().setAttribute("kfUrl", StationConfigUtil.get( StationConfig.online_customer_service_url));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		String payUrl = StationConfigUtil.get(StationConfig.pay_third_url);
		if (StringUtils.isNotEmpty(payUrl)) {
			return "redirect:" + payUrl;
		}
		super.getRequest().setAttribute("bankTransFlagDeposit", "class=\"current\"");
		super.getRequest().setAttribute("comFlag", "class=\"current\"");
		return "/common/template/member/center/banktrans/onlinedepositdemo.jsp";
	}

	/**
	 * c001独立模版充值页面
	 * 
	 * @return
	 */
	@RequestMapping("/cPage")
	public String cPage() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("folder", station.getFloder());
		super.getRequest().setAttribute("kfsb",
				StationConfigUtil.get(station.getId(), StationConfig.online_customer_start_time));
		super.getRequest().setAttribute("kfxb",
				StationConfigUtil.get(station.getId(), StationConfig.online_customer_end_time));
		super.getRequest().setAttribute("isPaymentSort", StationConfigUtil.get( StationConfig.payment_sort));
		return "/common/template/lottery/jimei/jsp/v_1/deposit.jsp";
	}

	/**
	 * 彩票第三版充值入口
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("cp3Page")
	public String cp3Page(Model model) {
		String payUrl = StationConfigUtil.get(StationConfig.pay_third_url);
		if (StringUtils.isNotEmpty(payUrl)) {
			return "redirect:" + payUrl;
		}
		super.getRequest().setAttribute("showPayInfo", !StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("deposit", "class=\"on\"");
		model.addAllAttributes(dptDataMap());
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("isPaymentSort", StationConfigUtil.get( StationConfig.payment_sort));
		return "/common/template/lottery/lecai/jsp/user/member_chongzhi.jsp";
	}

	/**
	 * 大发彩票充值入口
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("cpDfPage")
	public String cpDfPage(Model model) {
		String payUrl = StationConfigUtil.get(StationConfig.pay_third_url);
		if (StringUtils.isNotEmpty(payUrl)) {
			return "redirect:" + payUrl;
		}
		super.getRequest().setAttribute("showPayInfo", !StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("deposit", "class=\"on\"");
		model.addAllAttributes(dptDataMap());
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("isPaymentSort", StationConfigUtil.get( StationConfig.payment_sort));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_chongzhi.jsp";
	}
	
	@RequestMapping("chongzhiIframe")
	public String chongzhiIframe(Model model) {
		String payUrl = StationConfigUtil.get(StationConfig.pay_third_url);
		if (StringUtils.isNotEmpty(payUrl)) {
			return "redirect:" + payUrl;
		}
		super.getRequest().setAttribute("showPayInfo", !StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("deposit", "class=\"on\"");
		model.addAllAttributes(dptDataMap());
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("isPaymentSort", StationConfigUtil.get( StationConfig.payment_sort));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		super.getRequest().setAttribute("virtualFlag",StationConfigUtil.get(StationConfig.onoff_show_pay_virtual));
		super.getRequest().setAttribute("isVirtualDesc",StationConfigUtil.get(StationConfig.pay_tips_deposit_virtual));
		return "/common/template/lottery/lecai/jsp/userdf/member_chongzhiAicai.jsp";
	}
	
	private Map dptDataMap() {
		Map dptMap = new HashMap();
		String bankFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_normal);
		String fastFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_quick);
		String onlineFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_third);
		String virtualFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_virtual);
		String fastDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_fast);
		String bankDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_general);
		String onlineDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_third);
		String virtualDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_virtual);
		dptMap.put("bankFlag", bankFlag);
		dptMap.put("fastFlag", fastFlag);
		dptMap.put("onlineFlag", onlineFlag);
		dptMap.put("virtualFlag", virtualFlag);
		dptMap.put("fastDesc", fastDesc);
		dptMap.put("bankDesc", bankDesc);
		dptMap.put("onlineDesc", onlineDesc);
		dptMap.put("virtualDesc", virtualDesc);
		return dptMap;
	}

	@ResponseBody
	@RequestMapping("/dptdata")
	public void dptdata() {
		super.renderJson(dptDataMap());
	}

	/**
	 * 在线支付页面
	 * 
	 * @return
	 */
	@RequestMapping("/dptpga")
	public String dptpga() {
		String type = super.$("type");
		String onlineFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_third);
		super.getRequest().setAttribute("bankTransFlagDeposit", "class=\"current\"");
		super.getRequest().setAttribute("comFlag", "class=\"current\"");
		if (!"on".equals(onlineFlag)) {
			return page();
		}
		// type用于c001独立模版使用
		if (StringUtils.isNotEmpty(type) && StringUtils.equals(type, "aType")) {
			return "/common/template/lottery/jimei/jsp/v_1/dptp/deposita.jsp";
		}
		return "/common/template/member/center/banktrans/depositcommitademo.jsp";
	}

	/**
	 * 快速入款页面
	 * 
	 * @return
	 */
	@RequestMapping("/dptpgb")
	public String dptpgb() {
		String type = super.$("type");
		String fastFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_quick);
		super.getRequest().setAttribute("bankTransFlagDeposit", "class=\"current\"");
		super.getRequest().setAttribute("comFlag", "class=\"current\"");
		if (!"on".equals(fastFlag)) {
			return page();
		}
		super.getRequest().setAttribute("showPayInfo",! StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		// type用于c001独立模版使用
		if (StringUtils.isNotEmpty(type) && StringUtils.equals(type, "bType")) {
			super.getRequest().setAttribute("kfsb",
					StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_start_time));
			super.getRequest().setAttribute("kfxb",
					StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_end_time));
			return "/common/template/lottery/jimei/jsp/v_1/dptp/depositb.jsp";
		}
		return "/common/template/member/center/banktrans/depositcommitbdemo.jsp";
	}

	/**
	 * 一般存款页面
	 * 
	 * @return
	 */
	@RequestMapping("/dptpgc")
	public String dptpgc() {
		String type = super.$("type");
		String bankFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_normal);
		super.getRequest().setAttribute("bankTransFlagDeposit", "class=\"current\"");
		super.getRequest().setAttribute("comFlag", "class=\"current\"");
		if (!"on".equals(bankFlag)) {
			return page();
		}
		super.getRequest().setAttribute("showPayInfo", !StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		// type用于c001独立模版使用
		if (StringUtils.isNotEmpty(type) && StringUtils.equals(type, "cType")) {
			super.getRequest().setAttribute("kfsb",
					StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_start_time));
			super.getRequest().setAttribute("kfxb",
					StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_end_time));
			return "/common/template/lottery/jimei/jsp/v_1/dptp/depositc.jsp";
		}
		return "/common/template/member/center/banktrans/depositcommitcdemo.jsp";
	}
	/**
	 * 虚拟支付页面
	 *
	 * @return
	 */
	@RequestMapping("/dptpgd")
	public String dptpgd() {
		String type = super.$("type");
		String virtualFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_virtual);
		super.getRequest().setAttribute("bankTransFlagDeposit", "class=\"current\"");
		super.getRequest().setAttribute("comFlag", "class=\"current\"");
		if (!"on".equals(virtualFlag)) {
			return page();
		}
		// type用于c001独立模版使用
		if (StringUtils.isNotEmpty(type) && StringUtils.equals(type, "dType")) {
			return "/common/template/lottery/jimei/jsp/v_1/dptp/depositd.jsp";
		}
		return "/common/template/member/center/banktrans/depositcommitddemo.jsp";
	}
	/**
	 * 在线支付页面数据接口
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/dptinita")
	public void dptinita() {
		Map<String, Object> initMap = new HashMap<>();
		long stationId = StationUtil.getStationId();
		String min = StationConfigUtil.get(stationId, StationConfig.withdraw_min_money);
		String star = StationConfigUtil.get(stationId, StationConfig.online_customer_start_time);
		String end = StationConfigUtil.get(stationId, StationConfig.online_customer_end_time);
		List<Map> onlines = onlineService.getStationOnlines(AgentDepositOnline.SHOW_TYPE_PC);
		initMap.put("min", min);
		initMap.put("start", star);
		initMap.put("end", end);
		initMap.put("account", UserUtil.getUserAccount());
		initMap.put("onlines", onlines);
		super.renderJson(initMap);
	}

	/**
	 * 快速入款页面数据接口
	 */
	@ResponseBody
	@RequestMapping("/dptinitb")
	public void dptinitb() {
		Map<String, Object> initMap = new HashMap<>();
		long stationId = StationUtil.getStationId();
		String min = StationConfigUtil.get(stationId, StationConfig.withdraw_min_money);
		String star = StationConfigUtil.get(stationId, StationConfig.online_customer_start_time);
		String end = StationConfigUtil.get(stationId, StationConfig.online_customer_end_time);
		List<Map> fasts = fastService.periodPay();
		initMap.put("min", min);
		initMap.put("start", star);
		initMap.put("end", end);
		initMap.put("fasts", fasts);
		super.renderJson(initMap);
	}

	/**
	 * 生成订单号
	 */
	@ResponseBody
	@RequestMapping("/generateOrder")
	public void generateOrder() {
		super.renderJson(Snowflake.getOrderNo());
	}

	/**
	 * 一般存款页面数据接口MemberIndexController
	 */
	@ResponseBody
	@RequestMapping("/dptinitc")
	public void dptinitc() {
		Map<String, Object> initMap = new HashMap<>();
		long stationId = StationUtil.getStationId();
		String min = StationConfigUtil.get(stationId, StationConfig.withdraw_min_money);
		String star = StationConfigUtil.get(stationId, StationConfig.online_customer_start_time);
		String end = StationConfigUtil.get(stationId, StationConfig.online_customer_end_time);
		List<Map> banks = bankService.getStationBanks();
		initMap.put("min", min);
		initMap.put("start", star);
		initMap.put("end", end);
		initMap.put("banks", banks);
		super.renderJson(initMap);
	}
	/**
	 * 虚拟支付页面数据接口
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/dptinitd")
	public void dptinitd() {
		Map<String, Object> initMap = new HashMap<>();
		long stationId = StationUtil.getStationId();
		String min = StationConfigUtil.get(stationId, StationConfig.withdraw_min_money);
		String star = StationConfigUtil.get(stationId, StationConfig.online_customer_start_time);
		String end = StationConfigUtil.get(stationId, StationConfig.online_customer_end_time);
		List<Map> virtuals = virtualService.getStationOnlines(AgentDepositOnline.SHOW_TYPE_PC);
		initMap.put("min", min);
		initMap.put("start", star);
		initMap.put("end", end);
		initMap.put("account", UserUtil.getUserAccount());
		initMap.put("virtuals", virtuals);
		super.renderJson(initMap);
	}
	/**
	 * 在线支付提交数据接口
	 */
	@ResponseBody
	@RequestMapping("/dptcommita")
	public void dptcommita() {
		String onlineFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_third);
		if (!"on".equals(onlineFlag)) {
			throw new GenericException("非法请求！");
		}
		String money = super.$("money");
		long payId = super.$cl("payId");

		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setMoney(StringUtil.toBigDecimal(money));
		moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_ONLINE_THIRD);
		moneyVo.setPayId(payId);
		MnyComRecord mcr = mcrService.deposit(moneyVo);
		super.renderJson(mcr.getOrderNo());
	}

	/**
	 * 快速入款提交数据接口
	 */
	@ResponseBody
	@RequestMapping("/dptcommitb")
	public void dptcommitb() {
		String fastFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_quick);
		if (!"on".equals(fastFlag)) {
			throw new GenericException("非法请求！");
		}
		String money = super.$("money");
		String depositor = super.$("depositor");
		String bankCards = super.$("bankCards");
		long payId = super.$cl("payId");

		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setMoney(StringUtil.toBigDecimal(money));
		moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_ONLINE_FAST);
		moneyVo.setDepositor(depositor);
		moneyVo.setRemark(bankCards);
		moneyVo.setPayId(payId);
		MnyComRecord mcr = mcrService.deposit(moneyVo);
		super.renderJson(mcr.getOrderNo());
	}

	/**
	 * 一般存款提交数据接口
	 */
	@ResponseBody
	@RequestMapping("/dptcommitc")
	public void dptcommitc() {
		String bankFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_normal);
		if (!"on".equals(bankFlag)) {
			throw new GenericException("非法请求！");
		}
		String money = super.$("money");
		String depositor = super.$("depositor");
		long bankId = super.$cl("bankId");
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setMoney(StringUtil.toBigDecimal(money));
		moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_ONLINE_BANK);
		moneyVo.setDepositor(depositor);
		moneyVo.setPayId(bankId);
		MnyComRecord mcr = mcrService.deposit(moneyVo);
		super.renderJson(mcr.getOrderNo());
	}


	/**
	 * 虚拟支付提交数据接口
	 */
	@ResponseBody
	@RequestMapping("/dptcommitd")
	public void dptcommitd() {
		String onlineFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_virtual);
		if (!"on".equals(onlineFlag)) {
			throw new GenericException("充值通道已关闭！");
		}
		String money = super.$("money");
		long payId = super.$cl("payId");

		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setMoney(StringUtil.toBigDecimal(money));
		moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_VIRTUAL_BANK);
		moneyVo.setPayId(payId);
		MnyComRecord mcr = mcrService.deposit(moneyVo);
		super.renderJson(mcr.getOrderNo());
	}
}
