package com.game.controller.member.center.record;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.vo.BackwaterParam;
import com.game.service.LotteryService;
import com.game.service.MemberBackwaterRecordService;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/record/backwater")
public class MemBackWaterRecordController extends BaseMemberController {

	@Autowired
	MemberBackwaterRecordService memberBackWaterRecordService;
	
	@Autowired
	private LotteryService lotteryService;

	/**
	 * 彩票第三版反水记录
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model, String form) {
		String curDate = DateUtil.getCurrentDate();
		model.addAttribute("startTime", curDate);
		model.addAttribute("endTime", curDate);
		model.addAttribute(form, "class=\"on\"");
		model.addAttribute("unCancleOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_cancle_switch), "off"));
		model.addAttribute("unChaseOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_chase_switch), "off"));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		return "/common/template/lottery/lecai/jsp/user/member_backwater.jsp";
	}
	/**
	 * 大发版彩票反水记录
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model, String form) {
		String curDate = DateUtil.getCurrentDate();
		model.addAttribute("startTime", curDate);
		model.addAttribute("endTime", curDate);
		model.addAttribute(form, "class=\"on\"");
		model.addAttribute("unCancleOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_cancle_switch), "off"));
		model.addAttribute("unChaseOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_chase_switch), "off"));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		return "/common/template/lottery/lecai/jsp/userdf/member_backwater.jsp";
	}
	/**
	 * 会员反水记录
	 */
	@ResponseBody
	@RequestMapping("/list")
	public void list() {

		Long status = super.$long("status");
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		BackwaterParam params = new BackwaterParam();
		params.setBegin(DateUtil.toDatetime(startTime));
		params.setEnd(DateUtil.toDatetime(endTime));
		params.setStatus(status);
		params.setMemberId(UserUtil.getUserId());
		params.setStationId(StationUtil.getStationId());
		super.render(memberBackWaterRecordService.page(params));
	}
	
	/**
	 * 会员领取反水
	 */
	@ResponseBody
	@RequestMapping("/cashing")
	public void cashing() {
		Long rid = super.$long("id");
		memberBackWaterRecordService.cashingBackwater(rid);
		super.renderSuccess();
	}
}
