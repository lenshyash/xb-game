package com.game.controller.member.center.banktrans;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.service.LotteryService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.third.util.ThirdUtil;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MD5Util;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/banktrans/mnychg")
public class MemMoneyChangeController extends BaseMemberController {
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MnyComRecordService mnyComRecordService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private SysAccountService sysAccountService;

	@RequestMapping("/page")
	public String page(Model model, HttpSession httpSession) {
		String ag = StationConfigUtil.get(StationConfig.onoff_ag_game);
		String bbin = StationConfigUtil.get(StationConfig.onoff_bbin_game);
		String mg = StationConfigUtil.get(StationConfig.onoff_mg_game);
		String pt = StationConfigUtil.get(StationConfig.onoff_pt_game);
		String qt = StationConfigUtil.get(StationConfig.onoff_qt_game);
		String ab = StationConfigUtil.get(StationConfig.onoff_ab_game);
		String og = StationConfigUtil.get(StationConfig.onoff_og_game);
		String ds = StationConfigUtil.get(StationConfig.onoff_ds_game);
		String cq9 = StationConfigUtil.get(StationConfig.onoff_cq9_game);
		String jdb = StationConfigUtil.get(StationConfig.onoff_jdb_game);
		String isb = StationConfigUtil.get(StationConfig.onoff_isb_game);
		String ttg = StationConfigUtil.get(StationConfig.onoff_ttg_game);
		String mw = StationConfigUtil.get(StationConfig.onoff_mw_game);
		String m8 = StationConfigUtil.get(StationConfig.onoff_m8_game);
		String ibc = StationConfigUtil.get(StationConfig.onoff_ibc_game);
		String m8h = StationConfigUtil.get(StationConfig.onoff_m8h_game);
		String bg = StationConfigUtil.get(StationConfig.onoff_bg_game);
		String vr = StationConfigUtil.get(StationConfig.onoff_vr_game);
		String ky = StationConfigUtil.get(StationConfig.onoff_ky_game);
		String ebet = StationConfigUtil.get(StationConfig.onoff_ebet_game);
		String kx = StationConfigUtil.get(StationConfig.onoff_kx_game);

		String v = MD5Util.MD5(MD5Util.MD5(UserUtil.getUserAccount() + new Date()) + new Date().getTime());
		model.addAttribute("isAG", StringUtils.equals(ag, "on"));
		model.addAttribute("isBBIN", StringUtils.equals(bbin, "on"));
		model.addAttribute("isMG", StringUtils.equals(mg, "on"));

		model.addAttribute("isPT", StringUtils.equals(pt, "on"));
		model.addAttribute("isQT", StringUtils.equals(qt, "on"));
		model.addAttribute("isAB", StringUtils.equals(ab, "on"));

		model.addAttribute("isOG", StringUtils.equals(og, "on"));
		model.addAttribute("isDS", StringUtils.equals(ds, "on"));

		model.addAttribute("isBG", StringUtils.equals(bg, "on"));
		model.addAttribute("isCQ9", StringUtils.equals(cq9, "on"));
		model.addAttribute("isJDB", StringUtils.equals(jdb, "on"));
		model.addAttribute("isISB", StringUtils.equals(isb, "on"));
		model.addAttribute("isTTG", StringUtils.equals(ttg, "on"));
		model.addAttribute("isMW", StringUtils.equals(mw, "on"));
		model.addAttribute("isM8", StringUtils.equals(m8, "on"));
		model.addAttribute("isIBC", StringUtils.equals(ibc, "on"));
		model.addAttribute("isM8H", StringUtils.equals(m8h, "on"));
		model.addAttribute("isVR", StringUtils.equals(vr, "on"));
		model.addAttribute("isKY", StringUtils.equals(ky, "on"));
		model.addAttribute("isEBET", StringUtils.equals(ebet, "on"));
		model.addAttribute("isKx", StringUtils.equals(kx, "on"));
		model.addAttribute("v", v);
		httpSession.setAttribute("vc", v);
		ThirdUtil.initThirdParams();
		String memChgs = StationConfigUtil.get(StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if (StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if (memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("bankTransFlag", "class=\"current\"");
		super.getRequest().setAttribute("mnyChgFlag", "class=\"current\"");
		String onoff = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		if (!"on".equals(onoff)) {
			return "/common/template/member/center/banktrans/onlinedepositdemo.jsp";
		}
		return "/common/template/member/center/banktrans/moneychangedemo.jsp";
	}

	@ResponseBody
	@RequestMapping("/memdata")
	public void memdata() {
		User curUser = UserUtil.getCurrentUser();
		MnyMoney memMny = moneyService.getMoneyById(curUser.getSysUserId());
		Map memdata = MixUtil.newHashMap("account", curUser.getLoginAccount(), "money", memMny.getMoney());
		super.renderJson(memdata);
	}

	/**
	 * 彩票第三版充值取款记录
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model, Integer type) {
		model.addAttribute("isPtOnOff", StationConfigUtil.get(StationConfig.onoff_pt_game));
		model.addAttribute("isQtOnOff", StationConfigUtil.get(StationConfig.onoff_qt_game));
		model.addAttribute("isAbOnOff", StationConfigUtil.get(StationConfig.onoff_ab_game));
		model.addAttribute("isBbinOnOff", StationConfigUtil.get(StationConfig.onoff_bbin_game));
		model.addAttribute("isMgOnOff", StationConfigUtil.get(StationConfig.onoff_mg_game));
		model.addAttribute("isAgOnOff", StationConfigUtil.get(StationConfig.onoff_ag_game));
		model.addAttribute("isOgOnOff", StationConfigUtil.get(StationConfig.onoff_og_game));
		model.addAttribute("isDsOnOff", StationConfigUtil.get(StationConfig.onoff_ds_game));
		model.addAttribute("isCq9OnOff", StationConfigUtil.get(StationConfig.onoff_cq9_game));
		model.addAttribute("isJdbOnOff", StationConfigUtil.get(StationConfig.onoff_jdb_game));
		model.addAttribute("isIsbOnOff", StationConfigUtil.get(StationConfig.onoff_isb_game));
		model.addAttribute("isTtgOnOff", StationConfigUtil.get(StationConfig.onoff_ttg_game));
		model.addAttribute("isMwOnOff", StationConfigUtil.get(StationConfig.onoff_mw_game));
		model.addAttribute("isM8OnOff", StationConfigUtil.get(StationConfig.onoff_m8_game));
		model.addAttribute("isIbcOnOff", StationConfigUtil.get(StationConfig.onoff_ibc_game));
		model.addAttribute("isM8HOnOff", StationConfigUtil.get(StationConfig.onoff_m8h_game));
		model.addAttribute("isBgOnOff", StationConfigUtil.get(StationConfig.onoff_bg_game));
		model.addAttribute("isVrOnOff", StationConfigUtil.get(StationConfig.onoff_vr_game));
		model.addAttribute("isByOnOff", StationConfigUtil.get(StationConfig.onoff_by_game));
		model.addAttribute("isKyOnOff", StationConfigUtil.get(StationConfig.onoff_ky_game));
		model.addAttribute("isEbetOnOff", StationConfigUtil.get(StationConfig.onoff_ebet_game));
		
		model.addAttribute("change", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("v",
				MD5Util.MD5(MD5Util.MD5(UserUtil.getUserAccount() + new Date()) + new Date().getTime()));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));

		return "/common/template/lottery/lecai/jsp/user/member_change.jsp";
	}
	/**
	 * 大发版额度转换记录
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model, Integer type) {
		model.addAttribute("isPtOnOff", StationConfigUtil.get(StationConfig.onoff_pt_game));
		model.addAttribute("isQtOnOff", StationConfigUtil.get(StationConfig.onoff_qt_game));
		model.addAttribute("isAbOnOff", StationConfigUtil.get(StationConfig.onoff_ab_game));
		
		String bbin = StationConfigUtil.get(StationConfig.onoff_bbin_game);
		String bbindz = StationConfigUtil.get(StationConfig.onoff_bbin_dz_game);
		String bbinonoff = "off";
		if("on".equals(bbin) || "on".equals(bbindz)) {
			bbinonoff = "on";
		}
		model.addAttribute("isBbinOnOff", bbinonoff);
		
		model.addAttribute("isMgOnOff", StationConfigUtil.get(StationConfig.onoff_mg_game));
		model.addAttribute("isAgOnOff", StationConfigUtil.get(StationConfig.onoff_ag_game));
		model.addAttribute("isOgOnOff", StationConfigUtil.get(StationConfig.onoff_og_game));
		model.addAttribute("isDsOnOff", StationConfigUtil.get(StationConfig.onoff_ds_game));
		model.addAttribute("isCq9OnOff", StationConfigUtil.get(StationConfig.onoff_cq9_game));
		model.addAttribute("isJdbOnOff", StationConfigUtil.get(StationConfig.onoff_jdb_game));
		model.addAttribute("isIsbOnOff", StationConfigUtil.get(StationConfig.onoff_isb_game));
		model.addAttribute("isTtgOnOff", StationConfigUtil.get(StationConfig.onoff_ttg_game));
		model.addAttribute("isMwOnOff", StationConfigUtil.get(StationConfig.onoff_mw_game));
		model.addAttribute("isM8OnOff", StationConfigUtil.get(StationConfig.onoff_m8_game));
		model.addAttribute("isIbcOnOff", StationConfigUtil.get(StationConfig.onoff_ibc_game));
		model.addAttribute("isM8HOnOff", StationConfigUtil.get(StationConfig.onoff_m8h_game));
		model.addAttribute("isBgOnOff", StationConfigUtil.get(StationConfig.onoff_bg_game));
		model.addAttribute("isVrOnOff", StationConfigUtil.get(StationConfig.onoff_vr_game));
		model.addAttribute("isByOnOff", StationConfigUtil.get(StationConfig.onoff_by_game));
		model.addAttribute("isKyOnOff", StationConfigUtil.get(StationConfig.onoff_ky_game));
		model.addAttribute("isEbetOnOff", StationConfigUtil.get(StationConfig.onoff_ebet_game));
		model.addAttribute("isThOnOff", StationConfigUtil.get(StationConfig.onoff_th_game));
		
		model.addAttribute("change", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("v",
				MD5Util.MD5(MD5Util.MD5(UserUtil.getUserAccount() + new Date()) + new Date().getTime()));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_change.jsp";
	}

	/**
	 * 资金管理页面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/zjglPage")
	public String zjglPage(Model model, Integer type) {
		super.getRequest().setAttribute("zjglFlag", "class=\"current\"");
		return "/common/template/member/center/banktrans/zjglpage.jsp";
	}

	@RequestMapping("/chgmnyPage")
	public String chgmnyPage() {
		return "/common/template/member/center/banktrans/chgmnypage.jsp";
	}

	@RequestMapping("/conversion")
	public String conversion(Model model) {
		SysAccountInfo member = sysAccountService.getAccountInfo(UserUtil.getUserId());
		model.addAttribute("isPtOnOff", StationConfigUtil.get(StationConfig.onoff_pt_game));
		model.addAttribute("isQtOnOff", StationConfigUtil.get(StationConfig.onoff_qt_game));
		model.addAttribute("isAbOnOff", StationConfigUtil.get(StationConfig.onoff_ab_game));
		model.addAttribute("isBbinOnOff", StationConfigUtil.get(StationConfig.onoff_bbin_game));
		model.addAttribute("isMgOnOff", StationConfigUtil.get(StationConfig.onoff_mg_game));
		model.addAttribute("isAgOnOff", StationConfigUtil.get(StationConfig.onoff_ag_game));
		model.addAttribute("isOgOnOff", StationConfigUtil.get(StationConfig.onoff_og_game));
		model.addAttribute("isDsOnOff", StationConfigUtil.get(StationConfig.onoff_ds_game));
		model.addAttribute("isCq9OnOff", StationConfigUtil.get(StationConfig.onoff_cq9_game));
		model.addAttribute("isJdbOnOff", StationConfigUtil.get(StationConfig.onoff_jdb_game));
		model.addAttribute("isIsbOnOff", StationConfigUtil.get(StationConfig.onoff_isb_game));
		model.addAttribute("isTtgOnOff", StationConfigUtil.get(StationConfig.onoff_ttg_game));
		model.addAttribute("isMwOnOff", StationConfigUtil.get(StationConfig.onoff_mw_game));
		model.addAttribute("isM8OnOff", StationConfigUtil.get(StationConfig.onoff_m8_game));
		model.addAttribute("isIbcOnOff", StationConfigUtil.get(StationConfig.onoff_ibc_game));
		model.addAttribute("isM8HOnOff", StationConfigUtil.get(StationConfig.onoff_m8h_game));
		model.addAttribute("isBgOnOff", StationConfigUtil.get(StationConfig.onoff_bg_game));
		model.addAttribute("isVrOnOff", StationConfigUtil.get(StationConfig.onoff_vr_game));
		model.addAttribute("isByOnOff", StationConfigUtil.get(StationConfig.onoff_by_game));
		model.addAttribute("isKyOnOff", StationConfigUtil.get(StationConfig.onoff_ky_game));
		model.addAttribute("isEbetOnOff", StationConfigUtil.get(StationConfig.onoff_ebet_game));
		
		model.addAttribute("change", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("v",
				MD5Util.MD5(MD5Util.MD5(UserUtil.getUserAccount() + new Date()) + new Date().getTime()));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));

		return "/common/template/member/center/banktrans/member_jimei_change.jsp";
	}
	
	@RequestMapping("/chgmnyPages")
	public String chgmnyPages() {
		SysAccountInfo member = sysAccountService.getAccountInfo(UserUtil.getUserId());
		if (StringUtil.isEmpty(member.getReceiptPwd())) {
			return "/common/template/lottery/jimei/jsp/v_1/member_money_upw.jsp";
		}
		return "/common/template/lottery/jimei/jsp/v_1/member_money_change.jsp";
	}

	@ResponseBody
	@RequestMapping("/moneychg")
	public void moneychg() {
		String account = $c("account", "转移会员不能为空");
		String money = $c("money", "转移金额不能为空");
		String password = $c("password", "资金密码不能空");
		mnyComRecordService.moneyChangeOfMember(account, StringUtil.toBigDecimal(money), password);
		super.renderSuccess();
	}
	
	/**
	 * 赔率表
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/rebateExplain")
	public String rebateExplain(Model model, Integer type) {
		super.getRequest().setAttribute("rebateExplain", "class=\"current\"");
		model.addAttribute("lotterys", lotteryService.getLottery(model));
		model.addAttribute("bigGroup", lotteryService.getBigGroup(LotteryVersionUtils.resultVersion(StationUtil.getStationId())));
		return "/common/template/member/center/banktrans/rebateExplain.jsp";
	}

}
