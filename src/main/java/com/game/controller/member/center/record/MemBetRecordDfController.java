package com.game.controller.member.center.record;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.exception.lottery.EmptyLotteryException;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.DailyMoney;
import com.game.model.SysAccount;
import com.game.model.lottery.BcLotteryPlay;
import com.game.model.lottery.BcLotteryPlayGroup;
import com.game.model.lottery.LotteryEnum;
import com.game.model.lottery.six.BcMarkSix;
import com.game.model.platform.MemberGenrtLink;
import com.game.model.vo.LotteryParamVo;
import com.game.model.vo.SearchRecordParam;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentBaseConfigService;
import com.game.service.GeneralizeLinkService;
import com.game.service.LotteryService;
import com.game.service.MemberBettingRecordService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.service.lottery.BcLotteryPlayGroupService;
import com.game.service.lottery.BcLotteryPlayService;
import com.game.service.lottery.BcMarkSixService;
import com.game.util.AuthTokenUtil;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/df/record/betrecord")
public class MemBetRecordDfController extends BaseMemberController {
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	MemberBettingRecordService betRecordService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	@Autowired
	private BcLotteryPlayService lotteryPlayService;
	@Autowired
	private BcLotteryPlayGroupService lotteryPlayGroupService;
	
	@Autowired
	private BcMarkSixService markSixService;
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@RequestMapping("/page")
	public String page() {
		String curDate = DateUtil.getCurrentDate();
		HttpServletRequest request = super.getRequest();
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		request.setAttribute("isCpOnOff", StationConfigUtil.get(StationConfig.onoff_lottery_game));
		request.setAttribute("isTyOnOff", StationConfigUtil.get(StationConfig.onoff_sports_game));
		request.setAttribute("isDzOnOff", StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi));
		request.setAttribute("isLhcOnOff", StationConfigUtil.get(StationConfig.onoff_liu_he_cai));
		request.setAttribute("isChessOnOff", StationConfigUtil.get(StationConfig.onoff_chess));
		request.setAttribute("isTsOnOff", StationConfigUtil.get(StationConfig.onoff_third_sports));
		request.setAttribute("isThdLotOnOff", StationConfigUtil.get(StationConfig.onoff_third_lottery));
		
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isKyOnOff", StationConfigUtil.get(StationConfig.onoff_ky_game));
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("betHisFlag", "class=\"current\"");
		return "/common/template/member/center/record/betrecorddemo.jsp";
	}

	/**
	 大发彩票投注记录
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model, String form) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute(form, "class=\"on\"");
		if (StringUtils.isEmpty(form)) {
			form = "order";
		}
		String url = null;
		String navClass = null;
		model.addAttribute("unCancleOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_cancle_switch), "off"));
		model.addAttribute("unChaseOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_chase_switch), "off"));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		model.addAttribute("backwaterFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_platform_backwater)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		switch (form) {
		case "order": // 投注记录
			super.getRequest().setAttribute("lotteryNames", JsonUtil.toJson(lotteryService.getLotteryMap()));
			navClass = LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER;
			url = "/common/template/lottery/lecai/jsp/userdf/member_touzhu.jsp";
			break;
		case "win": // 中奖记录
			navClass = LotteryUtils.LOTTERY_V3_NAV_WIN_ORDER;
			DailyMoney daily = moneyService.findByAccountAndDate(UserUtil.getUserId(), new Date());
			BigDecimal allBetAmount = BigDecimal.ZERO;
			BigDecimal allWinAmount = BigDecimal.ZERO;
			BigDecimal yingkuiAmount = BigDecimal.ZERO;
			if (daily != null) {
				allBetAmount = BigDecimalUtil.addAll(daily.getLotteryBetAmount(), daily.getMarkSixBetAmount(),
						daily.getSysLotteryBetAmount()).setScale(2);
				allWinAmount = BigDecimalUtil.addAll(daily.getLotteryWinAmount(), daily.getMarkSixWinAmount(),
						daily.getSysLotteryWinAmount()).setScale(2);
				yingkuiAmount = BigDecimalUtil.addAll(allWinAmount, allBetAmount).setScale(2);
			}
			super.getRequest().setAttribute("allBetAmount", allBetAmount);
			super.getRequest().setAttribute("allWinAmount", allWinAmount);
			super.getRequest().setAttribute("yingkuiAmount", yingkuiAmount);
			url = "/common/template/lottery/lecai/jsp/userdf/member_zhongjiang.jsp";
			break;
		case "chess":
			url = "/common/template/lottery/lecai/jsp/userdf/member_chess.jsp";
			break;
		default:
			super.getRequest().setAttribute("lotteryNames", JsonUtil.toJson(lotteryService.getLotteryMap()));
			model.addAttribute("cur", "class=\"on\"");
			navClass = LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER;
			url = "/common/template/lottery/lecai/jsp/userdf/member_touzhu.jsp";
		}
		lotteryService.headerLot(model, navClass);
		return url;
	}

	/**
	 * 大发版体育投注记录
	 */
	@RequestMapping("/cpDfSportPage")
	public String cpDfSportPage(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("sportTab", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_sport.jsp";
	}

	/**
	 * 大发版真人投注记录
	 */
	@RequestMapping("/cpDfRealPage")
	public String cpDfRealPage(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("realTab", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_real.jsp";
	}
	/**
	 * 大发版棋牌投注记录
	 */
	@RequestMapping("/cpDfChessPage")
	public String cpDfChessPage(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("chessTab", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_chess.jsp";
	}
	/**
	 * 大发版电子投注记录
	 */
	@RequestMapping("/cpDfEgamePage")
	public String cpDfEgamePage(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("egameTab", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_egame.jsp";
	}

	@RequestMapping("/cpDfJointPage")
	public String cpDfJointPage(Model model) {
		String curDate = DateUtil.getCurrentDate();
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("joint", "class=\"on\"");
		SysAccount acc = (SysAccount) SysUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
			model.addAttribute("isGuest", true);
		} else {
			model.addAttribute("isGuest", false);
		}
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		return "/common/template/lottery/lecai/jsp/userdf/member_joint.jsp";

	}

	/**
	 * 会员体育投注记录
	 */
	@ResponseBody
	@RequestMapping("/sportrd")
	public void sportrd() {

		Long type = super.$long("type");
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		SearchRecordParam params = new SearchRecordParam();
		params.setBegin(DateUtil.toDate(startTime));
		params.setEnd(DateUtils.addDays(DateUtil.toDate(endTime), 1));
		params.setSportType(type);
		params.setMemberId(UserUtil.getUserId());
		super.render(betRecordService.getSportBettingRecord(params));
	}

	/**
	 * 彩票采种类型
	 */
	@ResponseBody
	@RequestMapping("/betrdtypes")
	public void betrdtypes() {
		Map comboMap = new HashMap();
		List<Map> lotTypes = new ArrayList<Map>();
		lotTypes.add(MixUtil.newHashMap("name", "所有彩种", "value", ""));
		for (LotteryEnum lenum : LotteryEnum.values()) {
			lotTypes.add(MixUtil.newHashMap("name", lenum.getLotName(), "value", lenum.name()));
		}
		comboMap.put("lotCombo", lotTypes);
		super.renderJson(comboMap);
	}

	/**
	 * 会员彩票投注记录
	 */
	@ResponseBody
	@RequestMapping("/lotteryrd")
	public void lotteryrd() {
		LotteryParamVo params = new LotteryParamVo();
		params.setStationId(StationUtil.getStationId());
		params.setAccount(UserUtil.getUserAccount());
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		String code = super.$("type");
		params.setCode(code);
		params.setStartTime(DateUtil.toDate(startTime));
		params.setEndTime(DateUtils.addDays(DateUtil.toDate(endTime), 1));
		super.render(betRecordService.getLotteryBettingRecord(params));
	}

	@ResponseBody
	@RequestMapping("/realrecord")
	public void realrecord() {

		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/member/getLiveBettingRecord.do",
				PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		super.renderJson(jsonObj);

		// String startTime = super.$("startTime");
		// String endTime = super.$("endTime");
		// String type = super.$("type");
		// HashMap<String, Object> params = new HashMap<String, Object>();
		// params.put("stationId", StationUtil.getStationId());
		// params.put("account", UserUtil.getUserAccount());
		// params.put("type", type);
		// params.put("startTime", DateUtil.toDate(startTime));
		// params.put("endTime", DateUtils.addDays(DateUtil.toDate(endTime),
		// 1));
		// super.renderJson(betRecordService.getRealrecord(params));
	}
	
	@ResponseBody
	@RequestMapping("/chessrecord")
	public void chessrecord() {

		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/member/getChessBettingRecord.do",
				PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		super.renderJson(jsonObj);
	}
	
	@ResponseBody
	@RequestMapping("/thirdlotteryrecord")
	public void thirdlotteryrecord() {

		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/member/getLotteryBettingRecord.do",
				PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		super.renderJson(jsonObj);

	}

	@ResponseBody
	@RequestMapping("/egamerecord")
	public void egamerecord() {
		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(
				StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/member/getEgameBettingRecord.do",
				PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		super.renderJson(jsonObj);
		// String startTime = super.$("startTime");
		// String endTime = super.$("endTime");
		// String type = super.$("type");
		// HashMap<String, Object> params = new HashMap<String, Object>();
		// params.put("stationId", StationUtil.getStationId());
		// params.put("account", UserUtil.getUserAccount());
		// params.put("startTime", DateUtil.toDate(startTime));
		// params.put("endTime", DateUtils.addDays(DateUtil.toDate(endTime),
		// 1));
		// super.renderJson(betRecordService.getEgamerecord(params));
	}

	@ResponseBody
	@RequestMapping("/sportsrecord")
	public void sportsrecord() {
		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(
				StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/member/getSportsBettingRecord.do",
				PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		super.renderJson(jsonObj);
	}

	/**
	 * 会员六合彩投注记录
	 */
	@ResponseBody
	@RequestMapping("/marksixrd")
	public void marksixrd() {
		LotteryParamVo params = new LotteryParamVo();
		params.setStationId(StationUtil.getStationId());
		params.setAccount(UserUtil.getUserAccount());
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		params.setCode(LotteryEnum.LHC.name());
		params.setStartTime(DateUtil.toDate(startTime));
		params.setEndTime(DateUtils.addDays(DateUtil.toDate(endTime), 1));
		super.render(betRecordService.getLotteryBettingRecord(params));
	}

	/**
	 * station:json字符串 {stationCode:'a001',stationName:'金沙国际',stationId:111}
	 * 
	 * @return
	 */
	private String getRecordVoJson() {
		String startTime = super.$("startTime");
		String endTime = super.$("endTime");
		JSONObject json = new JSONObject();
		json.put("userId", UserUtil.getUserId());
		json.put("stationId", StationUtil.getStationId());
		json.put("begin", DateUtil.toDate(startTime));
		json.put("end", DateUtils.addDays(DateUtil.toDate(endTime), 1));
		json.put("platform", super.$long("type"));
		return json.toJSONString();
	}

	public List<NameValuePair> getBasicParams() {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("recordVo", getRecordVoJson()));
		params.add(new BasicNameValuePair("token", AuthTokenUtil.getToken()));
		params.add(new BasicNameValuePair("pageNumber", webParam.getPageNo() + ""));
		params.add(new BasicNameValuePair("pageSize", webParam.getPageSize() + ""));
		return params;
	}
	/**
	 * 团队账变记录
	 * @param model
	 * @return
	 */
	@RequestMapping("/teamChange")
	public String teamChange(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("teamChange", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_teamChange.jsp";

	}
	/**
	 * 团队统计
	 * @param model
	 * @return
	 */
	@RequestMapping("/teamStatic")
	public String teamStatic(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		String lottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_lottery_game);
		String real = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		String sport = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_sports_game);
		String markSix = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_liu_he_cai);
		String dianZi = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_zi_you_yi);
		String thirdSports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_chess);
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("lottery", lottery);
		super.getRequest().setAttribute("real", real);
		super.getRequest().setAttribute("sport", sport);
		super.getRequest().setAttribute("markSix", markSix);
		super.getRequest().setAttribute("dianZi", dianZi);
		super.getRequest().setAttribute("thirdSports", thirdSports);
		super.getRequest().setAttribute("thirdLottery", thirdLottery);
		super.getRequest().setAttribute("chess", chess);
		model.addAttribute("teamStatic", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_teamStatic.jsp";

	}
	/**
	 * 反水记录
	 */
	@RequestMapping("/member_backwater")
	public String member_backwater(Model model, String form) {
		String curDate = DateUtil.getCurrentDate();
		model.addAttribute("startTime", curDate);
		model.addAttribute("endTime", curDate);
//		model.addAttribute(form, "class=\"on\"");
		model.addAttribute("unCancleOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_cancle_switch), "off"));
		model.addAttribute("unChaseOrder", StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_order_chase_switch), "off"));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		model.addAttribute("backwaterFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_platform_backwater)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		return "/common/template/lottery/lecai/jsp/userdf/member_backwater.jsp";

	}
	/**
	 * 推广链接
	 * @param model
	 * @return
	 */
	@RequestMapping("/sponsoredLinks")
	public String sponsoredLinks(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		StringBuffer requestUrl = super.getRequest().getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		Long stationId = StationUtil.getStationId();
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(stationId);// 匹配多级代理开关
		String url = "";
		if (isMulti) {
			url = domain + "/registerMutil/link_link998.do";
		} else {
			url = domain + "/registersAlone.do?init=link998";
		}
		model.addAttribute("url", url);
		model.addAttribute("domain", domain);
		model.addAttribute("isMulti", isMulti && !"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_agent_merge_Rebate_odds_switch)));
		model.addAttribute("memberRate", StationConfigUtil.get(StationConfig.onoff_member_rate_random));
		model.addAttribute("sponsoredLinks", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		model.addAttribute("profitSharFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_profit_share)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		model.addAttribute("trunOnoff", StationConfigUtil.get(StationConfig.onoff_register_account_trun_agent));
		return "/common/template/lottery/lecai/jsp/userdf/member_sponsoredLinks.jsp";

	}
	/**
	 * 业绩提成
	 * @param model
	 * @return
	 */
	@RequestMapping("/commission")
	public String commission(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("commission", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_commission.jsp";

	}
	/**
	 * 存款日志
	 * @param model
	 * @return
	 */
	@RequestMapping("/depositLog")
	public String depositLog(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("depositLog", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_depositLog.jsp";

	}
	/**
	 * 取款日志
	 * @param model
	 * @return
	 */
	@RequestMapping("/withdrawalLog")
	public String withdrawalLog(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		model.addAttribute("withdrawalLog", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_withdrawalLog.jsp";

	}
	/**
	 * 代理说明
	 * @param model
	 * @return
	 */
	@RequestMapping("/info")
	public String announcement(Model model) {
		model.addAttribute("announcement", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_agentIntro.jsp";

	}
	/**
	 * 公告列表页
	 * @param model
	 * @return
	 */
	@RequestMapping("/notice")
	public String notice(Model model) {
		model.addAttribute("notice", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		model.addAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_notice.jsp";

	}
	/**
	 * 下属会员列表
	 * @param model
	 * @return
	 */
	@RequestMapping("/subMember")
	public String subMember(Model model){
		model.addAttribute("subMember", "class=\"on\"");
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_agentMember.jsp";

	}
	/**
	 * 返点赔率表
	 * @param model
	 * @return
	 */
	@RequestMapping("/rebateExplain")
	public String rebateOdds(Model model){
		model.addAttribute("subMember", "class=\"on\"");
		model.addAttribute("lotterys", lotteryService.getLottery(model));
		model.addAttribute("bigGroup", lotteryService.getBigGroup(LotteryVersionUtils.resultVersion(StationUtil.getStationId())));
		
		
		return "/common/template/lottery/lecai/jsp/userdf/member_rebateExplain.jsp";
	}
	/**
	 * 获取指定彩种返点赔率数据
	 */
	@ResponseBody
	@RequestMapping("/getRebateOddsByLot")
	public void getRebateOddsByLot(Integer lotType) {
		
		String onoffRateRandom = StationConfigUtil.get(StationConfig.onoff_member_rate_random);
		Long stationId = StationUtil.getStationId();
		BigDecimal rate = null;
		//添加赔率百分比
		if(!"on".equalsIgnoreCase(onoffRateRandom)) {
			throw new EmptyLotteryException("未开启动态赔率");
		}
		Long userId  = UserUtil.getUserId();
		SysAccount ac = accountService.getOne(userId, stationId);
		rate = ac.getRate();
		if(rate==null || rate.compareTo(new BigDecimal(0))==0){
			throw new EmptyLotteryException("代理返点值未设置");
		}
		JSONArray arr = new JSONArray();
		//判断缓存
		String redisKey = Contants.MNY_MONEY_ACCOUNT_ID +stationId+":"+ userId+":"+lotType;
		String arrJson  = RedisAPI.getCache(redisKey);
		if(!StringUtil.isEmpty(arrJson)){
			arr = JSONArray.parseArray(arrJson);
			Map result = new HashMap();
			result.put("success",true);
			result.put("content", arr);
			super.renderJson(result);
		}else{
			List<BcLotteryPlayGroup> groupList = lotteryPlayGroupService.findAllActiveList(lotType,stationId);
			for(BcLotteryPlayGroup group:groupList){
				//获取彩种下玩法
				List<BcLotteryPlay> playList = lotteryPlayService.listByGroupId(stationId,group.getId());
				for(BcLotteryPlay play:playList){
					//获取玩法下赔率列表
					List<BcMarkSix> oddsList = markSixService.listByPlayCode(stationId,play.getCode(),-1,lotType);
					for(BcMarkSix ms:oddsList){
						JSONObject json = new JSONObject();
						String flag= "";
						if("shuzi".equals(ms.getMarkType())){
							flag = "数值-";
						}
						String name = play.getName()+"-"+flag+ms.getName();
						json.put("name", name);
						json.put("odds", ms.getOdds());
						json.put("sortNo", ms.getSortNo());
						json.put("playCode", ms.getPlayCode());
						json.put("items",this.getRebateOdds(rate,ms.getOdds(),ac));
	 					arr.add(json);
					}
				}
			}
			//添加缓存 
			if(arr!=null&& arr.size()>0){
				RedisAPI.addCache(redisKey, arr.toJSONString(), Contants.DAILI_REBATE_ODDS_TIMEOUT);
			}
			Map result = new HashMap();
			result.put("success",true);
			result.put("content", arr);
			super.renderJson(result);
		}
	}
	private Map<BigDecimal, BigDecimal> getRebateOdds(BigDecimal rate, BigDecimal odds,SysAccount ac) {
		Map<BigDecimal, BigDecimal> map = new LinkedHashMap<BigDecimal, BigDecimal>();
		BigDecimal temp = rate.setScale(1, BigDecimal.ROUND_UP);
		do{
	        BigDecimal tempRate = mnyMoneyService.getRate(temp,ac);
	        BigDecimal odd = BigDecimal.ZERO;
	        if(BigDecimalUtil.getNumberDecimalDigits(odds.doubleValue())>3){
	        	 odd = odds.multiply(tempRate).divide(new BigDecimal(100)).setScale(4,RoundingMode.DOWN);
			}else{
				 odd = odds.multiply(tempRate).divide(new BigDecimal(100)).setScale(3,RoundingMode.DOWN);
			}
	        
	        map.put(temp, odd);
	        temp = temp.subtract(new BigDecimal(0.1)).setScale(1, BigDecimal.ROUND_UP);
	     }while( temp.compareTo(new BigDecimal(5))>0);
		return map;
	}
	/**
	 * 公告详情页
	 * @param model
	 * @return
	 */
	@RequestMapping("/noticeDetail")
	public String noticeDetail(Model model) {
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		return "/common/template/lottery/lecai/jsp/userdf/member_noticeDetail.jsp";

	}
	/**
	 * 奖金对照
	 */
	@NotNeedLogin
	@RequestMapping("/leciUserLink")
	public String leciUserLink(String linkCode, Model model) {
		/*Long stationId = StationUtil.getStationId();
		Integer version = LotteryVersionUtils.resultVersion(stationId);
		BcLottery lot = lotteryService.findActiveByLotCode(leciUserLink, stationId, version);
		if (lot != null) {
			model.addAttribute("lot", lot);
			model.addAttribute("playList", lotteryPlayService.bonusPage(lot.getType(), stationId));
		}
		// return SystemConfig.SOURCE_FOLDER_COMMON +
		// "/template/lottery/jiebao/jsp/lottery/lotteryBonus.jsp";
		 * */		
		MemberGenrtLink link =new MemberGenrtLink();
		Long stationId = StationUtil.getStationId();
		
		StringBuffer requestUrl = super.getRequest().getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(stationId);// 匹配多级代理开关
		String randN = generalizeLinkService.getRandCode(StationUtil.getStationId());
		String memberRate = StationConfigUtil.get(StationConfig.onoff_member_rate_random);
		String url = "";
		String tgType ="";
		String dljz ="";
		Integer flag = 2;//返点开关 1开启 2关闭
		if(isMulti){
			tgType = "return true";
			dljz = "多级代理";
		}else{
			tgType = "return false";
			dljz = "一级代理";
		}
		if (isMulti) {
			url = domain + "/registerMutil/link_link998.do";
		} else {
			url = domain + "/registersAlone.do?init=link998";
		}
		if(StringUtil.isEmpty(linkCode)){
			
			if("on".equals(memberRate)){
				link.setType(2);
				flag = 1;
			}else{
				link.setType(1);
			}
			url = url.replace("link998", randN);
			link.setLinkUrl(url);
			link.setLinkKey(randN);
			
		}else{
			link = generalizeLinkService.getByStationIdAndLinkCode(stationId, linkCode);
			url = url.replace("link998", link.getLinkKey());
			link.setLinkUrl(url);
			tgType = "return false";
			if (isMulti) {
				if (link.getType() == 1 || "on".equals(memberRate)) {
					flag = 1;
				} 
			}
		}
		model.addAttribute("flag", flag);
		model.addAttribute("tgType", tgType);
		model.addAttribute("dljz", dljz);
		model.addAttribute("isMulti", isMulti);
		model.addAttribute("memberRate", StationConfigUtil.get(StationConfig.onoff_member_rate_random));
		model.addAttribute("link",link);
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_TOUZHU_ORDER);
		model.addAttribute("trunOnoff", StationConfigUtil.get(StationConfig.onoff_register_account_trun_agent));
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/lottery/lecai/jsp/userdf/member_addlink.jsp";
	}

}
