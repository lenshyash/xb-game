package com.game.controller.member.center.news;

import java.util.Arrays;

import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.MessageVo;
import com.game.service.LotteryService;
import com.game.service.SysMessageService;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/news/message")
public class MemberMessageController extends BaseMemberController {

	@Autowired
	private SysMessageService messageService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("messageFlag", "class=\"current\"");
		return "/common/template/member/center/news/messagedemo.jsp";
	}
	
	@RequestMapping("cPage")
	public String cPage(){
		return "/common/template/lottery/jimei/jsp/v_1/message.jsp";
	}
	
	/**
	 * 彩票第三版消息入口
	 * @param model
	 * @return
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("message", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		return "/common/template/lottery/lecai/jsp/user/member_message.jsp";
	}
	/**
	 * 大发版彩票消息入口
	 * @param model
	 * @return
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("message", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_message.jsp";
	}
	@RequestMapping("/messageCount")
	@ResponseBody
	public void messageCount(){
		super.renderJson(MixUtil.newHashMap("messageCount",messageService.getMessageCount()));
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Long status = super.$long("status");
		MessageVo mvo = new MessageVo();
		mvo.setAccountId(UserUtil.getUserId());
		mvo.setStationId(StationUtil.getStationId());
		mvo.setStatus(status);
		super.renderJson(messageService.getMessagePage(mvo));
	}

	@RequestMapping("/read")
	@ResponseBody
	public void read() {
		Long userMessageId = super.$long("id");
		messageService.readMessage(userMessageId);
		super.renderSuccess();
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void delete() {
		Long userMessageId = super.$long("id");
		messageService.delUserMessage(userMessageId);
		super.renderSuccess();
	}
	
	/**
	 * 批量删除
	 */
	@RequestMapping("/batchDelete")
	@ResponseBody
	public void batchDelete(){
		long[] userMessageId = super.$la("id",",");
		for(long l:userMessageId){
			messageService.delUserMessage(l);
		}
		super.renderSuccess();
	}
	
	/**
	 * 批量标记已读
	 */
	@RequestMapping("/batchRead")
	@ResponseBody
	public void batchRead(){
		long[] userMessageId = super.$la("id", ",");
		for(long l:userMessageId){
			messageService.readMessage(l);
		}
		super.renderSuccess();
	}
	
	@RequestMapping("/batchNewDelete")
	@ResponseBody
	public void batchNewDelete(){
		long[] userMessageId = super.$la("id",",");
		for(long l:userMessageId){
			messageService.delUserNewMessage(l);
		}
		super.renderSuccess();
	}
}
