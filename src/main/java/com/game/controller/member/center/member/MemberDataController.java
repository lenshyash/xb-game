package com.game.controller.member.center.member;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.DailyMoney;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.model.vo.AccountVo;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.LotteryService;
import com.game.service.MemberLevelService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/member/meminfo")
public class MemberDataController extends BaseMemberController {
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private MemberLevelService memberLevelService;
	@Autowired
	private MnyComRecordService mnyComRecordService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/member/center/member/meminfodemo.jsp";
	}
	
	@RequestMapping("/basepg")
	public String basepage() {
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/member/center/member/baseinfo.jsp";
	}

	@RequestMapping("/pwdpg")
	public String pwdpg() {
		return "/common/template/member/center/member/updpwddemo.jsp";
	}
	
	/**
	 * c001独立模版个人中心
	 * @return
	 */
	@RequestMapping("/cPage")
	public String cPage(){
		super.getRequest().setAttribute("isScore", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.mny_score_show));
		return "/common/template/lottery/jimei/jsp/v_1/member.jsp";
	}
	
	/**
	 * c001独立模版修改密码
	 * @return
	 */
	@RequestMapping("/pwdpgc")
	public String pwdpgc(){
		return "/common/template/lottery/jimei/jsp/v_1/update_pword.jsp";
	}
	
	@RequestMapping("/resetpwdpg")
	public String resetoldpwdpg() {
		return "/common/template/member/center/member/resetoldpwddemo.jsp";
	}
	
	/**
	 * 更改用户取款密码
	 */
	@ResponseBody
	@RequestMapping("/repwd")
	public void repwd() {
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		sysAccountService.updRepwdByAccount(pwd, rpwd);
		super.renderSuccess();
	}

	/**
	 * 更改用户登录密码
	 */
	@ResponseBody
	@RequestMapping("/newpwd")
	public void newpwd() {
		String opwd = super.$("opwd");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		Long updType = super.$long("updType");
		sysAccountService.updPwdByMember(opwd, pwd, rpwd,updType);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/data")
	public void memdata() {
		User curUser = UserUtil.getCurrentUser();
		super.renderJson(sysAccountService.getAccountById(curUser.getSysUserId(),StationUtil.getStationId()));
	}
	
	@NotNeedLogin
	@RequestMapping("/levelList")
	@ResponseBody
	public void list() {
		super.render(memberLevelService.getLevelPage(StationUtil.getStationId(), null));
	}
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/level")
	public void level() {
		if("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.set_account_pc_level))){
			super.renderJson("电脑会员等级未打开！");
		}
		super.renderJson(MemberLevelUtil.getLevelInfoJson());
	}
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/mobieLevel")
	public void mobieLevel() {
		if("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.set_account_mobie_level))){
			super.renderJson("手机端会员等级未打开");
		}
		super.renderJson(MemberLevelUtil.getLevelInfoJson());
	}
	/**
	 * 彩票第三版
	 */
	@RequestMapping("/cp3Page")
	public String cp_v3Page(Model model,String form){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		SysStation station = StationUtil.getStation();
		model.addAttribute("stationFolder", station.getFloder());
		if(StringUtils.isEmpty(form)){
			form = "index";
		}
		model.addAttribute(form, "class=\"on\"");
		Long memberId = UserUtil.getUserId();
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		switch(form){
			case "index":	//会员中心
				DailyMoney daily = moneyService.findByAccountAndDate(memberId,new Date());
				BigDecimal allBetAmount = BigDecimal.ZERO;
				BigDecimal allWinAmount = BigDecimal.ZERO;
				BigDecimal yingkuiAmount = BigDecimal.ZERO;
				if(daily != null){
					allBetAmount = BigDecimalUtil.addAll(daily.getLotteryBetAmount(),daily.getMarkSixBetAmount(),daily.getSysLotteryBetAmount()).setScale(2);
					allWinAmount = BigDecimalUtil.addAll(daily.getLotteryWinAmount(),daily.getMarkSixWinAmount(),daily.getSysLotteryWinAmount()).setScale(2);
					yingkuiAmount = BigDecimalUtil.addAll(allWinAmount, allBetAmount).setScale(2);
				}
				super.getRequest().setAttribute("allBetAmount", allBetAmount);
				super.getRequest().setAttribute("allWinAmount", allWinAmount);
				super.getRequest().setAttribute("yingkuiAmount", yingkuiAmount);
				super.getRequest().setAttribute("daily", daily);
				//从数据库获取levelGroup 
				SysAccount sysAccount = sysAccountService.getOne(memberId, station.getId());
				//会员权限处理
				List<MemberLevelBase> levelBase = memberLevelService.getStationLevels(station.getId());
				if(levelBase.size() ==1){
					super.getRequest().setAttribute("isLevel", false);
				}else{
					super.getRequest().setAttribute("isLevel", true);
					BigDecimal nextMoney = BigDecimal.ZERO;
					MemberLevelBase level = null;
					for(int i=0;i<levelBase.size();i++){
						level = levelBase.get(i);
						if(Objects.equals(level.getId(), sysAccount.getLevelGroup())){
							super.getRequest().setAttribute("levelName",level.getLevelName());	//获取当前会员的权限等级
							if(i < levelBase.size()-1){
								nextMoney = levelBase.get(i+1).getDepositMoney();	//下一级要求
								super.getRequest().setAttribute("nextLevel", levelBase.get(i+1));
							}else if(i==levelBase.size()-1){
								super.getRequest().setAttribute("nextLevel", levelBase.get(i));
							}
							break;
						}
					}
					BigDecimal allMoney = BigDecimal.ZERO;
					JSONObject json = MemberLevelUtil.getLevelInfoJson();
					if(json.getBigDecimal("total")!=null){
						allMoney = json.getBigDecimal("total");
					}
					
//					Long allMoney = mnyComRecordService.getComTotalMoney(station.getId(), memberId);	//获取总充值量
					super.getRequest().setAttribute("allMoney", allMoney);
					if(!nextMoney.equals(BigDecimal.ZERO)){
						super.getRequest().setAttribute("diffMoney", nextMoney.subtract(allMoney));
					}else{
						super.getRequest().setAttribute("diffMoney", BigDecimal.ZERO);
					}
				}
				Map map = sysAccountService.getAccountById(memberId,station.getId());
				map.put("lastLoginDatetime", DateUtil.toDatetimeStr((Date)map.get("lastLoginDatetime")));
				super.getRequest().setAttribute("userInfo1",map);
				return "/common/template/lottery/lecai/jsp/user/member.jsp";
			case "info":	//账户信息
				SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
				model.addAttribute("member", member);
				return "/common/template/lottery/lecai/jsp/user/member_xinxi.jsp";
			default:
				model.addAttribute("current", "class=\"on\"");
				return "/common/template/lottery/lecai/jsp/user/member.jsp";
		}
	}
	/**
	 * 大发彩票版
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model,String form){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		SysStation station = StationUtil.getStation();
		model.addAttribute("stationFolder", station.getFloder());
		if(StringUtils.isEmpty(form)){
			form = "index";
		}
		model.addAttribute(form, "class=\"on\"");
		Long memberId = UserUtil.getUserId();
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		model.addAttribute("backwaterFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_platform_backwater)));
		switch(form){
			case "index":	//会员中心
				DailyMoney daily = moneyService.findByAccountAndDate(memberId,new Date());
				BigDecimal allBetAmount = BigDecimal.ZERO;
				BigDecimal allWinAmount = BigDecimal.ZERO;
				BigDecimal yingkuiAmount = BigDecimal.ZERO;
				if(daily != null){
					allBetAmount = BigDecimalUtil.addAll(daily.getLotteryBetAmount(),daily.getMarkSixBetAmount(),daily.getSysLotteryBetAmount()).setScale(2);
					allWinAmount = BigDecimalUtil.addAll(daily.getLotteryWinAmount(),daily.getMarkSixWinAmount(),daily.getSysLotteryWinAmount()).setScale(2);
					yingkuiAmount = BigDecimalUtil.addAll(allWinAmount, allBetAmount).setScale(2);
				}
				super.getRequest().setAttribute("allBetAmount", allBetAmount);
				super.getRequest().setAttribute("allWinAmount", allWinAmount);
				super.getRequest().setAttribute("yingkuiAmount", yingkuiAmount);
				super.getRequest().setAttribute("daily", daily);
				//从数据库获取levelGroup 
				SysAccount sysAccount = sysAccountService.getOne(memberId, station.getId());
				//会员权限处理
				List<MemberLevelBase> levelBase = memberLevelService.getStationLevels(station.getId());
				if(levelBase.size() ==1){
					super.getRequest().setAttribute("isLevel", false);
				}else{
					super.getRequest().setAttribute("isLevel", true);
					BigDecimal nextMoney = BigDecimal.ZERO;
					MemberLevelBase level = null;
					for(int i=0;i<levelBase.size();i++){
						level = levelBase.get(i);
						if(Objects.equals(level.getId(), sysAccount.getLevelGroup())){
							super.getRequest().setAttribute("levelName",level.getLevelName());	//获取当前会员的权限等级
							if(i < levelBase.size()-1){
								nextMoney = levelBase.get(i+1).getDepositMoney();	//下一级要求
								super.getRequest().setAttribute("nextLevel", levelBase.get(i+1));
							}else if(i==levelBase.size()-1){
								super.getRequest().setAttribute("nextLevel", levelBase.get(i));
							}
							break;
						}
					}
//					Long allMoney = mnyComRecordService.getComTotalMoney(station.getId(), memberId);	//获取总充值量
					BigDecimal allMoney = BigDecimal.ZERO;
					JSONObject json = MemberLevelUtil.getLevelInfoJson();
					if(json.getBigDecimal("total")!=null){
						allMoney = json.getBigDecimal("total");
					}
					//等級頭像
					super.getRequest().setAttribute("levelHead",json.getString("icon"));
					super.getRequest().setAttribute("allMoney", allMoney);
					if(!nextMoney.equals(BigDecimal.ZERO)){
						super.getRequest().setAttribute("diffMoney", nextMoney.subtract(allMoney));
					}else{
						super.getRequest().setAttribute("diffMoney", BigDecimal.ZERO);
					}
				}
				Map map = sysAccountService.getAccountById(memberId,station.getId());
				map.put("lastLoginDatetime", DateUtil.toDatetimeStr((Date)map.get("lastLoginDatetime")));
				super.getRequest().setAttribute("userInfo1",map);
				
				return "/common/template/lottery/lecai/jsp/userdf/member.jsp";
			case "info":	//账户信息
				SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
				model.addAttribute("member", member);
				return "/common/template/lottery/lecai/jsp/userdf/member_xinxi.jsp";
			default:
				model.addAttribute("current", "class=\"on\"");
				return "/common/template/lottery/lecai/jsp/userdf/member.jsp";
		}
	}
	@RequestMapping("/commitUserName")
	@ResponseBody
	public void commitUserName(){
		SysAccount acc=(SysAccount)UserUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
			throw new GenericException("此账号不能修改账户信息");
		}
		String userName = super.$("userName");
		if(StringUtils.isEmpty(userName)){
			throw new GenericException("真实姓名不能为空");
		}
		if(sysAccountService.userNameExits(userName)){
			throw new GenericException("真实姓名已存在");
		}
		SysAccountInfo info = sysAccountService.getAccountInfo(UserUtil.getUserId());
		if(info != null){
			info.setUserName(userName);
		}
		sysAccountService.saveAccountUserName(info);
		renderSuccess();
	}
	
	@RequestMapping("/accountCount")
	@ResponseBody
	public void accountCount(){
		int count = 0;
		SysAccountInfo info = sysAccountService.getAccountInfo(UserUtil.getUserId());
		if(info != null){
			if(StringUtils.isEmpty(info.getUserName())){
				count ++;
			}
			if(StringUtils.isEmpty(info.getReceiptPwd())){
				count ++;
			}
			if(StringUtils.isEmpty(info.getCardNo()) || StringUtils.isEmpty(info.getBankName())){
				count ++ ;
			}
		}
		super.renderJson(MixUtil.newHashMap("count",count));
	}
	
	@RequestMapping("/accountLevel")
	@ResponseBody
	public void getAccountMeminfo() {
		Long memberId = UserUtil.getUserId();
		Long stationId = StationUtil.getStationId();
		SysAccount sysAccount = sysAccountService.getOne(memberId, stationId);
		//会员权限处理
		MemberLevelBase level = memberLevelService.getOne(sysAccount.getLevelGroup(), stationId);
		if(level != null) {
			renderJson(MixUtil.newHashMap("icon",level.getIcon(),"success",true));
			return;
		}
		renderSuccess();
		
	}
	@ResponseBody
	@RequestMapping("/accountList")
	public void accountList() {
		String account = super.$("account");
		Long agentId = super.$long("agentId");
		Long agentParentId = super.$long("agentParentId");
		Long online = super.$long("online");
		Long accountType = super.$long("accountType");
		Long searchType = super.$long("searchType");
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setParents(UserUtil.getChildren());
		accountVo.setAgentId(agentId);
		accountVo.setAgentParentId(agentParentId);
		accountVo.setOnline(online);
		accountVo.setAccountType(accountType);
		accountVo.setSearchType(searchType);
		Page page = sysAccountService.getAccountPage4DaiLi(accountVo);
		
		String contactWayFlag = StationConfigUtil.get(StationConfig.onoff_daili_member_user_contact_way_hidden);
		String showAllFlag = StationConfigUtil.get(StationConfig.onoff_daili_member_user_show_all);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<Map> list = page.getList();
			for (Map m : list) {
				if(!"on".equals(showAllFlag)) {
					m.put("cardNo", yincang((String) m.get("cardNo"), 4));
					m.put("userName", yincang((String) m.get("userName"), 1,true));
					m.put("bankAddress", yincang((String) m.get("bankAddress"), 3));
					if(!"off".equalsIgnoreCase(contactWayFlag)) {
						m.put("phone", yincang((String) m.get("phone"), 4));
						m.put("qq", yincang((String) m.get("qq"), 4));
						m.put("wechat", yincang((String) m.get("wechat"), 4));
						m.put("email", yincang((String) m.get("email"), 4));
					}
				}
				m.remove("receiptPwd");
				String parentNames = (String) m.get("parentNames");
				String accountName = UserUtil.getCurrentUser().getLoginAccount();
				if (StringUtils.contains(parentNames, accountName)) {
					String[] pns = parentNames.split(",");
					parentNames = "";
					boolean begin = false;
					for (String pn : pns) {
						if (StringUtils.isNotEmpty(pn)) {
							if (StringUtils.equals(accountName, pn)) {
								begin = true;
							}
							if (!begin) {
								continue;
							}
							parentNames = parentNames + " > " + pn;
						}
					}
					if (parentNames.length() > 3) {
						parentNames = parentNames.substring(3);
					}
					m.put("parentNames", parentNames);
				} else {
					m.put("parentNames", "");
				}
				m.put("parents", "");
			}
		}
		super.renderJson(page);
	}
	private String yincang(String str,int baoliuHouJiwei) {
		return yincang( str, baoliuHouJiwei,false);
	}
	private String yincang(String str, int baoliuHouJiwei,boolean enabledOnoff) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if(enabledOnoff) {
			String yicangOnOff = StationConfigUtil.get(StationConfig.onoff_daili_member_user_name_hidden);
			if(StringUtil.isNotEmpty(yicangOnOff) && "off".equals(yicangOnOff)) {
				return str;
			}
		}
		
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}
	
	
}
