package com.game.controller.member.center.redpacket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.MemberRedPacketService;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/redpacket")
public class MemberRedPacketController extends BaseMemberController {
	@Autowired
	MemberRedPacketService redPacketService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/progress")
	public void progress() {
		super.renderJson(redPacketService.getCurrentRedPacket(StationUtil.getStationId()));
	}

	@ResponseBody
	@RequestMapping("/grab")
	public void grab(Long redPacketId) {
		super.renderJson(redPacketService.grabRedPacket(redPacketId));
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/marquee")
	public void marquee(Long redPacketId,String sort) {
		super.renderJson(redPacketService.getMarqueeRecord(StationUtil.getStationId(), redPacketId));
	}
}
