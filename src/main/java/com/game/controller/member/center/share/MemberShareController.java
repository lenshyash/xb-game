package com.game.controller.member.center.share;

import javax.servlet.http.HttpSession;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.service.ChatService;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/share")
public class MemberShareController extends BaseMemberController {
	@Autowired
	private ChatService chatService;

	@ResponseBody
	@RequestMapping("/order")
	public void order(HttpSession session) {
		String data = $("data");
		JSONArray arr = JSONArray.parseArray(data);
		JSONObject obj = new JSONObject();
		obj.put("data", arr);
		obj.put("qiHao", $("qiHao"));
		obj.put("lotType", $("lotType"));
		obj.put("groupCode", $("groupCode"));
		obj.put("qiHao", $("qiHao"));
		obj.put("version", $("version"));
		obj.put("lotCode", $("lotCode"));
		obj.put("lotName", $("lotName"));
		obj.put("playName", $("playName"));
		SysAccount user = (SysAccount) UserUtil.getCurrentUser();
		chatService.shareOrder(user, obj);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/report")
	public void report(HttpSession session) {
		JSONObject obj = new JSONObject();
		obj.put("bet", StringUtil.toBigDecimal($("bet")));
		obj.put("win", StringUtil.toBigDecimal($("win")));
		obj.put("bunko", StringUtil.toBigDecimal($("bunko")));
		SysAccount user = (SysAccount) UserUtil.getCurrentUser();
		chatService.shareReport(user, obj);
		super.renderSuccess();
	}
}
