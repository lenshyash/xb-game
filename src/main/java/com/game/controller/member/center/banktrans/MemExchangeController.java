package com.game.controller.member.center.banktrans;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MnyExchangeConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.ExchangeConfigVo;
import com.game.service.LotteryService;
import com.game.service.MnyExchangeService;
import com.game.service.SysAccountService;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/banktrans/exchange")
public class MemExchangeController extends BaseMemberController {
	@Autowired
	private SysAccountService sysAccountService;

	@Autowired
	private MnyExchangeService exchangeService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("exchangeFlag", "class=\"current\"");
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfEmail", StationConfigUtil.get(StationConfig.basic_info_customer_email));
		return "/common/template/member/center/banktrans/memexchangedemo.jsp";
	}
	/**
	 * 彩票v2-1版积分兑换
	 */
	@RequestMapping("/cpPage")
	public String cpPage(Model model){
		return "/common/template/lottery/jimei/jsp/v_1/member_exchange.jsp";
	}
	
	/**
	 * 彩票第三版积分兑换
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		super.getRequest().setAttribute("exchange", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		return "/common/template/lottery/lecai/jsp/user/member_exchange.jsp";
	}
	/**
	 * 大发彩票积分兑换
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		super.getRequest().setAttribute("exchange", "class=\"on\"");
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get(StationConfig.onoff_chess));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_exchange.jsp";
	}

	/**
	 * 会员信息
	 */
	@RequestMapping("/udata")
	@ResponseBody
	public void udata() {
		Map map=sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId());
		SysAccount account =(SysAccount) SysUtil.getCurrentUser();
		SysAccountInfo accountInfo =UserUtil.getCurrentInfo();
		account.setScore( (BigDecimal)map.get("score") );
		getSession().setAttribute(SystemConfig.SESSION_MEMBER_KEY, account);
		getSession().setAttribute(SystemConfig.SESSION_MEMBER_KEY+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX, accountInfo);
		super.renderJson(map);
	}

	/**
	 * 获得兑换配置
	 */
	@RequestMapping("/exctypes")
	@ResponseBody
	public void exctypes() {
		ExchangeConfigVo ecvo = new ExchangeConfigVo();
		ecvo.setStationId(StationUtil.getStationId());
		ecvo.setStatus(MnyExchangeConfig.STATUS_ENABLED);
		super.renderJson(exchangeService.getProgressConfig(ecvo));
	}

	/**
	 * 兑换
	 */
	@RequestMapping("/exchange")
	@ResponseBody
	public void exchange() {
		Long typeId = super.$long("typeId");
		String amount = super.$("amount");
		exchangeService.exchange(typeId, StringUtil.toBigDecimal(amount));
		super.renderSuccess();
	}

}
