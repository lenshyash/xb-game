package com.game.controller.member.center.active;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MemberActive;
import com.game.model.MemberActiveAward;
import com.game.model.vo.ActiveRecordVo;
import com.game.model.vo.ActiveVo;
import com.game.service.MemberActiveService;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/active")
public class MemberActiveController extends BaseMemberController {
	@Autowired
	MemberActiveService activeService;

	@RequestMapping("/awardpg")
	public String awardpg() {
		return super.goPage("/module/center/active/memactive.jsp");
	}

	@ResponseBody
	@RequestMapping("/activedata")
	public void activedata() {
		ActiveVo activeVo = new ActiveVo();
		activeVo.setStationId(StationUtil.getStationId());
		activeVo.setStatus(MemberActive.STATUS_ENABLED);
		activeVo.setActiveType(MemberActive.ACTIVE_TYPE_TURNTABLE);
		super.renderJson(activeService.getProgressActive(activeVo));
	}

	@ResponseBody
	@RequestMapping("/award")
	public void award() {
		Long activeId = super.$long("activeId");
		MemberActiveAward maa = activeService.play(activeId, UserUtil.getUserId());
		super.renderJson(MixUtil.newHashMap("index", maa.getAwardIndex(), "awardName", maa.getAwardName()));
	}

	@ResponseBody
	@RequestMapping("/records")
	public void records() {
		Long activeId = super.$long("activeId");
		ActiveRecordVo recordVo = new ActiveRecordVo();
		recordVo.setActiveId(activeId);
		recordVo.setAccountId(UserUtil.getUserId());
		super.render(activeService.getAwardRecordPage(recordVo));
	}

	@ResponseBody
	@RequestMapping("/lastrd")
	public void lastrd() {
		Long activeId = super.$long("activeId");
		ActiveRecordVo recordVo = new ActiveRecordVo();
		recordVo.setActiveId(activeId);
		recordVo.setLimit(50);
		recordVo.setOrder(" create_datetime DESC");
		super.renderJson(JSON.toJSONString(activeService.getAwardRecordList(recordVo), SerializerFeature.WriteDateUseDateFormat));
	}
}
