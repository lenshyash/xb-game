package com.game.controller.member.center.bgem;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MemberBalanceGemStrategy;
import com.game.model.MnyExchangeConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.ExchangeConfigVo;
import com.game.service.LotteryService;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.MemberBalanceGemStrategyService;
import com.game.service.MnyExchangeService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.LotteryUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationOnOffUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/bgem/balancegem")
public class MemBalanceGemController extends BaseMemberController {
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private MemberBalanceGemRecordService balanceGemRecordService;

	@Autowired
	private MemberBalanceGemStrategyService balanceGemStrategyService;
	
	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isBgemOnOff", StationConfigUtil.get(StationConfig.onoff_balance_gem));
		//获取余额宝余额和累积收益
		Map map = mnyMoneyService.getMoneyByAccount(UserUtil.getUserAccount()+"", StationUtil.getStationId());
		BigDecimal bgMoney = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
		BigDecimal bgIncome = StringUtil.toBigDecimal(map.get("balanceGemIncome"));
		super.getRequest().setAttribute("bgMoney", bgMoney);
		super.getRequest().setAttribute("bgIncome", bgIncome);
		super.getRequest().setAttribute("account", UserUtil.getUserAccount());
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/member/center/bgem/balance_gem.jsp";
	}
	/**
	 * 彩票v2-1版积分兑换
	 */
	@RequestMapping("/cpPage")
	public String cpPage(Model model){
		super.getRequest().setAttribute("isBgemOnOff", StationConfigUtil.get(StationConfig.onoff_balance_gem));
		//获取余额宝余额和累积收益
		Map map = mnyMoneyService.getMoneyByAccount(UserUtil.getUserAccount()+"", StationUtil.getStationId());
		BigDecimal bgMoney = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
		BigDecimal bgIncome = StringUtil.toBigDecimal(map.get("balanceGemIncome"));
		super.getRequest().setAttribute("bgMoney", bgMoney);
		super.getRequest().setAttribute("bgIncome", bgIncome);
		super.getRequest().setAttribute("account", UserUtil.getUserAccount());
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/jimei/jsp/v_1/balance_gem.jsp";
	}
	
	/**
	 * 彩票第三版积分兑换
	 */
	@RequestMapping("/cp3Page")
	public String cp3Page(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		super.getRequest().setAttribute("balancegem", "class=\"on\"");
		//获取余额宝余额和累积收益
		Map map = mnyMoneyService.getMoneyByAccount(UserUtil.getUserAccount()+"", StationUtil.getStationId());
		BigDecimal bgMoney = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
		BigDecimal bgIncome = StringUtil.toBigDecimal(map.get("balanceGemIncome"));
		super.getRequest().setAttribute("bgMoney", bgMoney);
		super.getRequest().setAttribute("bgIncome", bgIncome);
		super.getRequest().setAttribute("account", UserUtil.getUserAccount());
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/user/balance_gem.jsp";
	}
	/**
	 * 大发彩票积分兑换
	 */
	@RequestMapping("/cpDfPage")
	public String cpDfPage(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		super.getRequest().setAttribute("balancegem", "class=\"on\"");
		//获取余额宝余额和累积收益
		Map map = mnyMoneyService.getMoneyByAccount(UserUtil.getUserAccount()+"", StationUtil.getStationId());
		BigDecimal bgMoney = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
		BigDecimal bgIncome = StringUtil.toBigDecimal(map.get("balanceGemIncome"));
		super.getRequest().setAttribute("bgMoney", bgMoney);
		super.getRequest().setAttribute("bgIncome", bgIncome);
		super.getRequest().setAttribute("account", UserUtil.getUserAccount());
		super.getRequest().setAttribute("kfPhone", StationConfigUtil.get(StationUtil.getStationId(),StationConfig.basic_info_customer_phone));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/balance_gem.jsp";
	}
	/**
	 * 获取会员余额宝数据
	 */
	@ResponseBody
	@RequestMapping("/bgMemberInfo")
	public void memberBalanceGemInfo(HttpSession session) {
		Map<String, Object> result = new HashMap<>();
	    Map<String, Object> json = new HashMap<>();
	    Long stationId = StationUtil.getStationId();
		//获取余额宝余额和累积收益
		Map map = mnyMoneyService.getMoneyByAccount(UserUtil.getUserAccount()+"", StationUtil.getStationId());
		BigDecimal bgMoney = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
		BigDecimal bgIncome = StringUtil.toBigDecimal(map.get("balanceGemIncome"));
		List<MemberBalanceGemStrategy> strategys = balanceGemStrategyService.findStationStrategy(stationId);
		//获取站点策略
		MemberBalanceGemStrategy strategy= balanceGemRecordService.filterByMemberLevelandMoney(UserUtil.getUserId(), StationUtil.getStationId(), strategys, bgMoney);
		if(strategy!=null){
			strategy = balanceGemRecordService.countTheThousand(strategy);
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH,-1);
		//获取昨日收益
		BigDecimal yesterdayIncome = balanceGemRecordService.getYesterdayIncome(cal.getTime(),UserUtil.getUserId());
		if(yesterdayIncome==null){
			yesterdayIncome = BigDecimal.ZERO;
		}
		result.put("yesterdayIncome", yesterdayIncome.setScale(2, BigDecimal.ROUND_DOWN));
		result.put("bgMoney", bgMoney.setScale(2, BigDecimal.ROUND_DOWN));
		result.put("bgIncome", bgIncome.setScale(2, BigDecimal.ROUND_DOWN));
		result.put("account", UserUtil.getUserAccount());
		result.put("strategy", strategy);
		json.put("accessToken", session.getId());
        json.put("content", result);
        json.put("success", true);
        super.renderJson(json);
	}
	
	/**
	 * 转入转出余额宝
	 */
	@ResponseBody
	@RequestMapping("/bgTransfer")
	public void transferBalanceGem(HttpSession session) {
		String money = super.$("money");
		//操作类型 1转入 2转出
		Long type = super.$long("type");
		boolean isAdd = false;
		if(StringUtil.isEmpty(money)||type==null){
			throw new GenericException("必填参数校验失败");
		}
		BigDecimal tranMoney = StringUtil.toBigDecimal(money);
		if(type==1){
			isAdd=true;
		}else if(type==2){
			isAdd=false;
		}else{
			throw new GenericException("类型有误");
		}
		balanceGemRecordService.transferBalanceGem(tranMoney,isAdd);
		Map<String, Object> json = new HashMap<>();
	    json.put("success", true);
	    json.put("accessToken", session.getId());
	    super.renderJson(json);
	}
}
