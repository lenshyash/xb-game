package com.game.controller.member.center.chess;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.model.lottery.BcLottery;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.LotteryService;
import com.game.service.MemberLevelService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MD5Util;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/member/chess")
public class CenterIndexController extends BaseMemberController {
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private LotteryService lotteryService;
	@RequestMapping("/messagelist")
	public String messagelist() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		String kfUrl = StationConfigUtil.get(station.getId(), StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		super.getRequest().setAttribute("kfUrl", kfUrl);
		return "/common/template/chess/center/messageList.jsp";
	}
	
	@RequestMapping("/userCenter")
	public String userCenter() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/userCenter.jsp";
	}
	@RequestMapping("/UpdateInfo")
	public String UpdateInfo() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/UpdateInfo.jsp";
	}
	@RequestMapping("/BindAccountAlipay")
	public String BindAccountAlipay() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/BindAccountAlipay.jsp";
	}
	@RequestMapping("/BindEmail")
	public String BindEmail() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/BindEmail.jsp";
	}
	
	@RequestMapping("/UpdateLoginPass")
	public String UpdateLoginPass() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/UpdateLoginPass.jsp";
	}
	
	@RequestMapping("/BankDeposit")
	public String BankDeposit(Model model) {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		String payUrl = StationConfigUtil.get(StationConfig.pay_third_url);
		if (StringUtils.isNotEmpty(payUrl)) {
			return "redirect:" + payUrl;
		}
		super.getRequest().setAttribute("showPayInfo", !StringUtils.equals(
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_payment_show_info), "off"));
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("deposit", "class=\"on\"");
		model.addAllAttributes(dptDataMap());
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		return "/common/template/chess/center/BankDeposit.jsp";
	}
	private Map dptDataMap() {
		Map dptMap = new HashMap();
		String bankFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_normal);
		String fastFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_quick);
		String onlineFlag = StationConfigUtil.get(StationConfig.onoff_show_pay_third);
		String fastDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_fast);
		String bankDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_general);
		String onlineDesc = StationConfigUtil.get(StationConfig.pay_tips_deposit_third);
		dptMap.put("bankFlag", bankFlag);
		dptMap.put("fastFlag", fastFlag);
		dptMap.put("onlineFlag", onlineFlag);
		dptMap.put("fastDesc", fastDesc);
		dptMap.put("bankDesc", bankDesc);
		dptMap.put("onlineDesc", onlineDesc);
		return dptMap;
	}

	@RequestMapping("/BankDrawMoney")
	public String BankDrawMoney(Model model) {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("draw", "class=\"on\"");
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		if(StringUtils.isEmpty(member.getReceiptPwd())){
			//取款密码设置页面
			model.addAttribute("type", 1); 
		} else if(StringUtils.isEmpty(member.getCardNo()) || StringUtils.isEmpty(member.getUserName()) || StringUtils.isEmpty(member.getBankName())){
			//真实姓名信息填写页面
			model.addAttribute("type", 2);
			model.addAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		}else{
			//取款页面
			model.addAttribute("type", 3);
		}
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		
		
		return "/common/template/chess/center/BankDrawMoney.jsp";
	}
	
	@RequestMapping("/RecordDrawList")
	public String RecordDrawList() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/RecordDrawList.jsp";
	}
	
	@RequestMapping("/Promotion")
	public String Promotion() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/Promotion.jsp";
	}
	
	@RequestMapping("/FinanceRevenue")
	public String FinanceRevenue() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/FinanceRevenue.jsp";
	}
	
	@RequestMapping("/UnderlingAccountList")
	public String UnderlingAccountList() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/UnderlingAccountList.jsp";
	}
	
	
	@RequestMapping("/UnderlingInfo")
	public String UnderlingInfo() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/UnderlingInfo.jsp";
	}
	
	@RequestMapping("/RecordUserRevenueList")
	public String RecordUserRevenueList() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/RecordUserRevenueList.jsp";
	}
	
	@RequestMapping("/accountBreakdown")
	public String accountBreakdown() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/accountBreakdown.jsp";
	}
	@RequestMapping("/accountDraw")
	public String accountDraw() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/accountDraw.jsp";
	}
	@RequestMapping("/recharge")
	public String recharge() {
		SysStation station = StationUtil.getStation();
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/recharge.jsp";
	}
	@RequestMapping("/limitChange")
	public String limitChange(Model model) {
		SysStation station = StationUtil.getStation();
		SysAccountInfo member = sysAccountService.getAccountInfo(UserUtil.getUserId());
		model.addAttribute("isPtOnOff", StationConfigUtil.get(StationConfig.onoff_pt_game));
		model.addAttribute("isQtOnOff", StationConfigUtil.get(StationConfig.onoff_qt_game));
		model.addAttribute("isAbOnOff", StationConfigUtil.get(StationConfig.onoff_ab_game));
		model.addAttribute("isBbinOnOff", StationConfigUtil.get(StationConfig.onoff_bbin_game));
		model.addAttribute("isMgOnOff", StationConfigUtil.get(StationConfig.onoff_mg_game));
		model.addAttribute("isAgOnOff", StationConfigUtil.get(StationConfig.onoff_ag_game));
		model.addAttribute("isOgOnOff", StationConfigUtil.get(StationConfig.onoff_og_game));
		model.addAttribute("isDsOnOff", StationConfigUtil.get(StationConfig.onoff_ds_game));
		model.addAttribute("isCq9OnOff", StationConfigUtil.get(StationConfig.onoff_cq9_game));
		model.addAttribute("isJdbOnOff", StationConfigUtil.get(StationConfig.onoff_jdb_game));
		model.addAttribute("isIsbOnOff", StationConfigUtil.get(StationConfig.onoff_isb_game));
		model.addAttribute("isTtgOnOff", StationConfigUtil.get(StationConfig.onoff_ttg_game));
		model.addAttribute("isMwOnOff", StationConfigUtil.get(StationConfig.onoff_mw_game));
		model.addAttribute("isM8OnOff", StationConfigUtil.get(StationConfig.onoff_m8_game));
		model.addAttribute("isIbcOnOff", StationConfigUtil.get(StationConfig.onoff_ibc_game));
		model.addAttribute("isM8HOnOff", StationConfigUtil.get(StationConfig.onoff_m8h_game));
		model.addAttribute("isBgOnOff", StationConfigUtil.get(StationConfig.onoff_bg_game));
		model.addAttribute("isVrOnOff", StationConfigUtil.get(StationConfig.onoff_vr_game));
		model.addAttribute("isByOnOff", StationConfigUtil.get(StationConfig.onoff_by_game));
		model.addAttribute("isKyOnOff", StationConfigUtil.get(StationConfig.onoff_ky_game));
		model.addAttribute("isEbetOnOff", StationConfigUtil.get(StationConfig.onoff_ebet_game));
		
		model.addAttribute("change", "class=\"on\"");
//		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("v",
				MD5Util.MD5(MD5Util.MD5(UserUtil.getUserAccount() + new Date()) + new Date().getTime()));
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("stationFolder", station.getFloder());
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("myAccountFlag", "class=\"current\"");
		super.getRequest().setAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		return "/common/template/chess/center/limitChange.jsp";
		
	}


	
}
