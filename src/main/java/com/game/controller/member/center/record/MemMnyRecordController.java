package com.game.controller.member.center.record;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.service.MnyMoneyRecordService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/record/mnyrecord")
public class MemMnyRecordController extends BaseMemberController {

	@Autowired
	MnyMoneyRecordService mnyMoneyService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		String curDate = DateUtil.getCurrentDate();
		Map moneyType = MixUtil.newHashMap();
		for (MoneyRecordType type : MoneyRecordType.values()) {
			moneyType.put(type.getType(), type.getName());
		}
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("betHisFlag", "class=\"current\"");
		super.getRequest().setAttribute("moneyType", JsonUtil.toJson(moneyType));
		return "/common/template/member/center/record/mnyrecorddemo.jsp";
	}
	
	@RequestMapping("/cashRecord")
	public String cashRecord(HttpServletRequest request){
		request.setAttribute("now", DateUtil.getCurrentDate("yyyy-MM-dd"));
		Map moneyType = MixUtil.newHashMap();
		for (MoneyRecordType type : MoneyRecordType.values()) {
			moneyType.put(type.getType(), type.getName());
		}
		super.getRequest().setAttribute("betHisFlag", "class=\"current\"");
		super.getRequest().setAttribute("moneyType", JsonUtil.toJson(moneyType));
		return "/common/template/lottery/jimei/jsp/v_1/cashRecord.jsp";
	}

	/**
	 * 会员账变记录
	 */
	@ResponseBody
	@RequestMapping("/list")
	public void sportrd() {

		//修改了startTime，endTime参数。
		String account = super.$("account");
		String begin = super.$("startTime");
		String end = super.$("endTime");
		Long type = super.$long("type");
		String orderId = super.$("orderId");

		MnyMoneyRecordVo moneyRecordVo = new MnyMoneyRecordVo();
		moneyRecordVo.setAccountId(UserUtil.getUserId());
		moneyRecordVo.setAccount(account);
		moneyRecordVo.setType(type);
		moneyRecordVo.setBegin(DateUtil.toDate(begin));
		moneyRecordVo.setEnd(DateUtil.getTomorrow(end));
		moneyRecordVo.setOrderId(orderId);
		moneyRecordVo.setShowChangeOrder(true);
		if("on".equals(StationConfigUtil.get(StationConfig.onoff_showhandmoney)) == false){
			moneyRecordVo.setShowHandMoney(true);
		}else {
			moneyRecordVo.setShowHandMoney(false);
		}
		
		super.renderJson(mnyMoneyService.getMoneyRecord(moneyRecordVo));
	}

}
