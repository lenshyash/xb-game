package com.game.controller.member.center.banktrans;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.LotteryService;
import com.game.service.MnyDrawRecordService;
import com.game.service.SysAccountService;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/center/banktrans/draw")
public class MemOnlineDrawController extends BaseMemberController {
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private MnyDrawRecordService mdrService;
	@Autowired
	private LotteryService lotteryService;

	@RequestMapping("/page")
	public String page() {
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		String memChgs = StationConfigUtil.get( StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		super.getRequest().setAttribute("isZJGLOnOff", isZJGLOnOff);
		super.getRequest().setAttribute("bankTransFlagDraw", "class=\"current\"");
		super.getRequest().setAttribute("drawFlag", "class=\"current\"");
		return "/common/template/member/center/banktrans/onlinedrawdemo.jsp";
	}

	@RequestMapping("/drawpg")
	public String drawpg() {

		super.getRequest().setAttribute("bankTransFlagDraw", "class=\"current\"");
		super.getRequest().setAttribute("drawFlag", "class=\"current\"");
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		if (StringUtil.isEmpty(member.getReceiptPwd())) {
			return "/common/template/member/center/banktrans/updrepwddemo.jsp";
		} else if (StringUtil.isEmpty(member.getCardNo()) || StringUtil.isEmpty(member.getUserName())
				|| StringUtil.isEmpty(member.getBankName())) {
			return "/common/template/member/center/banktrans/drawbankinfodemo.jsp";
		}
		return "/common/template/member/center/banktrans/drawcommitdemo.jsp";
	}
	
	/**
	 * c001独立模版取款
	 * @return
	 */
	@RequestMapping("/drawpgs")
	public String drawpgs(Model model){
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		if (StringUtil.isEmpty(member.getReceiptPwd())) {
			return "/common/template/lottery/jimei/jsp/v_1/draw.jsp";
		} else if (StringUtil.isEmpty(member.getCardNo()) || StringUtil.isEmpty(member.getUserName())
				|| StringUtil.isEmpty(member.getBankName())) {
			return "/common/template/lottery/jimei/jsp/v_1/drawinfo.jsp";
		}
		return "/common/template/lottery/jimei/jsp/v_1/drawcommit.jsp";
	}
	
	/**
	 * 彩票第三版取款入口
	 * @param model
	 * @return
	 */
	@RequestMapping("cp3Page")
	public String cp3Page(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("draw", "class=\"on\"");
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		if(StringUtils.isEmpty(member.getReceiptPwd())){
			//取款密码设置页面
			model.addAttribute("type", 1); 
		} else if(StringUtils.isEmpty(member.getCardNo()) || StringUtils.isEmpty(member.getUserName()) || StringUtils.isEmpty(member.getBankName())){
			//真实姓名信息填写页面
			model.addAttribute("type", 2);
			model.addAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		}else{
			//取款页面
			model.addAttribute("type", 3);
		}
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		return "/common/template/lottery/lecai/jsp/user/member_qukuan.jsp";
	}
	/**
	 * 大发版彩票取款入口
	 * @param model
	 * @return
	 */
	@RequestMapping("cpDfPage")
	public String cpDfPage(Model model){
		lotteryService.headerLot(model,LotteryUtils.LOTTERY_V3_NAV_NULL);
		model.addAttribute("draw", "class=\"on\"");
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		if(StringUtils.isEmpty(member.getReceiptPwd())){
			//取款密码设置页面
			model.addAttribute("type", 1); 
		} else if(StringUtils.isEmpty(member.getCardNo()) || StringUtils.isEmpty(member.getUserName()) || StringUtils.isEmpty(member.getBankName())){
			//真实姓名信息填写页面
			model.addAttribute("type", 2);
			model.addAttribute("userInfo1",sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
		}else{
			//取款页面
			model.addAttribute("type", 3);
		}
		model.addAttribute("memberRateFlag",
				"on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		super.getRequest().setAttribute("isChangeMoney", StationConfigUtil.get(StationConfig.onoff_change_money));
		super.getRequest().setAttribute("isExChgOnOff", StationConfigUtil.get( StationConfig.exchange_score));
		super.getRequest().setAttribute("isZrOnOff", StationConfigUtil.get( StationConfig.onoff_zhen_ren_yu_le));
		super.getRequest().setAttribute("isDzOnOff", StationConfigUtil.get( StationConfig.onoff_dian_zi_you_yi));
		super.getRequest().setAttribute("isLhcOnOff", StationConfigUtil.get( StationConfig.onoff_liu_he_cai));
		super.getRequest().setAttribute("isChessOnOff", StationConfigUtil.get( StationConfig.onoff_chess));
		super.getRequest().setAttribute("kfUrl",StationConfigUtil.get(StationConfig.online_customer_service_url));
		return "/common/template/lottery/lecai/jsp/userdf/member_qukuan.jsp";
	}
	@ResponseBody
	@RequestMapping("/drawbkinfo")
	public void drawbkinfo() {
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		super.renderJson(member);
	}

	@ResponseBody
	@RequestMapping("/cmitbkinfo")
	public void cmitbkinfo() {
		SysAccount acc=(SysAccount)UserUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
            throw new GenericException("此账号不能进行此操作");
		}
		String bankName = super.$("bankName");
		String userName = super.$("userName");
		String cardNo = super.$("cardNo");
		String province = super.$("province");
		String phone = super.$("phone");
		String city = super.$("city");
		String bankAddress = super.$("bankAddress");
		String repPwd = super.$("repPwd");
		AccountVo avo = new AccountVo();
		avo.setBankName(bankName);
		avo.setUserName(userName);
		avo.setCardNo(cardNo);
		avo.setProvince(province);
		avo.setPhone(phone);
		avo.setCity(city);
		avo.setBankAddress(bankAddress);
		avo.setReceiptPwd(repPwd);
		avo.setId(UserUtil.getUserId());
		sysAccountService.saveBankInfo(avo,true);

		super.renderSuccess();
	}
	
	/**
	 * 彩票第三版信息中心绑定银行资料
	 */
	@ResponseBody
	@RequestMapping("/commitbkInfo")
	public void commitbkInfo() {
		SysAccount acc=(SysAccount)UserUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
            throw new GenericException("此账号不能进行此操作");
		}
		String bankName = super.$("bankName");
		String userName = super.$("userName");
		String cardNo = super.$("cardNo");
		String province = super.$("province");
		String phone = super.$("phone");
		String city = super.$("city");
		String bankAddress = super.$("bankAddress");
		String repPwd = super.$("repPwd");
		AccountVo avo = new AccountVo();
		avo.setBankName(bankName);
		avo.setUserName(userName);
		avo.setCardNo(cardNo);
		avo.setProvince(province);
		avo.setPhone(phone);
		avo.setCity(city);
		avo.setBankAddress(bankAddress);
		avo.setReceiptPwd(repPwd);
		avo.setId(UserUtil.getUserId());
		sysAccountService.saveBankInfo(avo,LotteryVersionUtils.resultVersion(StationUtil.getStationId()) == BusinessConstant.lottery_identify_V4?true:false);

		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/drawdata")
	public void drawdata() {
		Long memberId = UserUtil.getUserId();
		Map map = mdrService.getMemberDrawCheckMap(memberId, StationUtil.getStationId());
		Map member =(Map) map.get("member");
		if(StringUtil.isNotEmpty((String)member.get("cardNo"))
				&&!"on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.member_cardno_show))){
			member.put("cardNo",MobileUtil.yincang((String)member.get("cardNo"),8));
		}
		
		super.renderJson(map);
	}
	
	@ResponseBody
	@RequestMapping("/drawcommit")
	public void drawcommit() {
		String moneyStr = super.$("money");
		if(!moneyStr.matches("^[1-9]([\\d]{0,7})?$")){
			throw new GenericException("取款金额必须是整数");
		}
		SysAccountInfo member = sysAccountService.getAccountInfo(UserUtil.getUserId());
		if (StringUtil.isEmpty(member.getReceiptPwd())) {
			throw new GenericException("请先设置提款密码");
		} else if (StringUtil.isEmpty(member.getCardNo()) || StringUtil.isEmpty(member.getUserName())
				|| StringUtil.isEmpty(member.getBankName())) {
			throw new GenericException("请先完善出款银行信息");
		}
		
		BigDecimal money = StringUtil.toBigDecimal(moneyStr);
		String repPwd = super.$("repPwd");

		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setReceiptPwd(repPwd);
		moneyVo.setMoney(money);
		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ONLINE);
		moneyVo.setRemark("提款申请处理中");
		mdrService.withdraw(moneyVo);
		super.renderSuccess();
	}
}
