 package com.game.controller.member;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysRegisterConfig;
import com.game.model.SysRegisterConfigGroup;
import com.game.model.platform.MemberGenrtLink;
import com.game.model.vo.RegisterConfigVo;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentArticleService;
import com.game.service.GeneralizeLinkService;
import com.game.service.LotteryService;
import com.game.service.SysAccountService;
import com.game.service.SysRegisterConfigService;
import com.game.third.constant.Platform;
import com.game.third.service.RealGame4CenterService;
import com.game.util.CookieHelper;
import com.game.util.HttpUtils;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationOnOffUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.check.CheckUtils;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH)
public class MemberRegsiterController extends BaseMemberController {

	@Autowired
	AgentArticleService articleService;

	@Autowired
	private SysRegisterConfigService regConfService;

	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	@Autowired
	private RealGame4CenterService realGame4CenterService;
	

	@NotNeedLogin
	@RequestMapping("/regpage")
	public String regpage(Model model) {
		StationOnOffUtil.initConfig();
		String kfUrl = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		super.getRequest().setAttribute("kfUrl", kfUrl);
		super.getRequest().setAttribute("navTab", "regpage");
		if (MobileUtil.isMoblie(false)) {
			return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + "/register.do";
			// return SystemConfig.MOBILE_CONTROL_PATH + "/register.jsp";
		}
		if (LotteryVersionUtils.isV3OrV5(StationUtil.getStationId())) {
			lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		}
		return super.goPage("register.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/registerGuestPage")
	public String registerGuestPage(Map<String, Object> map) {
		// map.put("userName", sysAccountService.getTestGuestName());
		// if (MobileUtil.isMoblie(false)) {
		// return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH +
		// "/register.do";
		// }
		// return super.goPage("registerGuest.jsp");
		String userName = sysAccountService.registerTestGuest();
		String ver = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name);
		if (MobileUtil.isMoblie(false)) {
			if (StringUtils.equals("v4", ver)) {
				CookieHelper.set(super.getResponse(), "account_" + userName + "_touxiang",
						String.valueOf(RandomUtils.nextInt(1, 111)));
				return "redirect:/m/index.do";
			}
			return "redirect:/mobile/index.do";
		}
		if (StringUtils.equals("v3", ver)|| StringUtils.equals("v5", ver)) {
			return "redirect:/lotteryV3/lottery.do";
		}
		if (StringUtils.equals("v2", ver) || StringUtils.equals("v2_1", ver)) {
			return "redirect:/lotteryV2/index.do";
		}
		if (StringUtils.equals("v4", ver)) {
			CookieHelper.set(super.getResponse(), "account_" + userName + "_touxiang",
					String.valueOf(RandomUtils.nextInt(1, 111)));
			return "redirect:/m/index.do";
		}
		return "redirect:/lottery/cp.do";
	}
	
	/**
	 * 固定往首页跳 不进行版本区分
	 * @param map
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/registerGuestPage2")
	public String registerGuestPage2(Map<String, Object> map) {
		String userName = sysAccountService.registerTestGuest();
		return "redirect:/index.do";
	}
	
	/**
	 * 真人试玩  直接跳转ag试玩接口
	 * @param map
	 * @return
	 */
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/realGuest")
	public void realGuest(Map<String, Object> map) {
		SysAccount acc = null;
		if(!UserUtil.isLogin()){
			acc = sysAccountService.registerTestGuestForNative();
		}else{
			acc = (SysAccount) SysUtil.getCurrentUser();
		}
		Long isH5 = $long("h5");
		String isH5Val = "false";
		if (Validator.isNotNull(isH5) && isH5 != 1l) {
			throw new GenericException("参数错误");
		} else if (StringUtil.equals(isH5, 1l)) {
			isH5Val = "true";
		}
		HttpServletRequest request = UserUtil.getRequest();
		Map params = MixUtil.newHashMap("isH5", isH5Val, "dmBase", HttpUtils.getDomain(request),"realGuest","true");
		super.renderJson(realGame4CenterService.login(Platform.AG, params));
	}
	
	/**
	 * 应客户要求 同一个模板做两个注册页面
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_regpage")
	public String _regpage() {
		if (MobileUtil.isMoblie(false)) {
			return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + "/register.do";
			// return SystemConfig.MOBILE_CONTROL_PATH + "/register.jsp";
		}
		return super.goPage("_register.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/agtregpage")
	public String agtregpage(Model model) {
		StationOnOffUtil.initConfig();
		if (LotteryVersionUtils.isV3OrV5(StationUtil.getStationId())) {
			lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_DAILI_JIAMENG);
		}
		return super.goPage("agtregister.jsp");
	}

	/**
	 * 带有下划线“_”都是模板第二套
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_agtregpage")
	public String _agtregpage() {
		return super.goPage("_agtregister.jsp");
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/regconf")
	public void regconf() {
		RegisterConfigVo rcvo = new RegisterConfigVo();
		rcvo.setPlatform(SysRegisterConfig.TYPE_MEMBER);
		rcvo.setShowVal(SysRegisterConfigGroup.VALUE_ENALBED);
		List<Map> regConfs = regConfService.getRegConfVal(rcvo);
//		long linkId = NumberUtils.toLong(RedisAPI.getCache(Contants.registerKey + SysUtil.getSession().getId()), 0);
//		if (linkId != 0) {
//			for (Iterator<Map> iterator = regConfs.iterator(); iterator.hasNext();) {
//				Map map = iterator.next();
//				if (map.get("key").equals("promoCode")) {
//					iterator.remove();
//					break;
//				}
//			}
//		}
		super.renderJson("var $_regconf = " + JSONObject.toJSONString(regConfs) + ";");
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/agtregconf")
	public void agtregconf() {
		RegisterConfigVo rcvo = new RegisterConfigVo();
		rcvo.setPlatform(SysRegisterConfig.TYPE_AGENT);
		rcvo.setShowVal(SysRegisterConfigGroup.VALUE_ENALBED);
		List<Map> regConfs = regConfService.getRegConfVal(rcvo);
		super.renderJson("var $_regconf = " + JSONObject.toJSONString(regConfs) + ";");
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/checkPromo")
	public void checkPromo() {
		MemberGenrtLink link = generalizeLinkService.getByStationIdAndLinkCode(StationUtil.getStationId(), $("linkCode"));
		if(link == null) {
			super.renderFailure("邀请码不存在");
			return;
		}
		Long type = SysRegisterConfig.TYPE_AGENT;
		if(StringUtil.equals(link.getType(), 2)){
			type = SysRegisterConfig.TYPE_MEMBER;
		}
		RegisterConfigVo rcvo = new RegisterConfigVo();
		rcvo.setPlatform(type);
		rcvo.setShowVal(SysRegisterConfigGroup.VALUE_ENALBED);
		List<Map> regConfs = regConfService.getRegConfVal(rcvo);
		super.renderJson( JSONObject.toJSONString(regConfs));
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/commonRegister")
	public void commonRegister(@RequestParam(value = "data", required = true) String data, HttpServletRequest request,
			HttpServletResponse response) {
		logger.error("注册时Content-Type = " + request.getHeader("Content-Type"));
		if (StringUtils.isEmpty(data)) {
			BufferedReader br;
			try {
				br = request.getReader();
				String str, wholeStr = "";
				while ((str = br.readLine()) != null) {
					wholeStr += str;
				}
				logger.error("注册时data 为空" + wholeStr);
			} catch (IOException e) {
			}
			throw new ParameterException("参数[data]值为空");
		}
		
		CheckUtils.checkWebShell(data);
		
		Map atMap = JsonUtil.toBean(data, Map.class);
		Object promoCode = atMap.get("promoCode");
		if(promoCode == null) {
			super.renderFailure("邀请码不存在");
			return;
		}
		
		MemberGenrtLink link = generalizeLinkService.getByStationIdAndLinkCode(StationUtil.getStationId(),promoCode.toString() );
		if(link == null) {
			super.renderFailure("邀请码不存在");
			return;
		}
		Long type = SysRegisterConfig.TYPE_AGENT;
		if(StringUtil.equals(link.getType(), 2)){
			type = SysRegisterConfig.TYPE_MEMBER;
		}
		
		// 从Cookie中读取linkId
		atMap.put("linkId", link.getId());
		sysAccountService.register(atMap, data, type);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		super.renderSuccess();
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/commonRegistertwo")
	public void commonRegistertwo(HttpServletRequest request,HttpServletResponse response) {
		
        String username = request.getParameter("account");
        String password = request.getParameter("password");
        String rpassword = request.getParameter("rpassword");
        String realname = request.getParameter("userName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String qqnum = request.getParameter("qq");
        String wechat = request.getParameter("wechat");
        String cardNoNum = request.getParameter("cardNo");
        String cashpass = request.getParameter("receiptPwd");
        String verifyCode = request.getParameter("verifyCode");
        String promoCode = request.getParameter("promoCode");
		
        Map<String, String> params = new HashMap<>();
        params.put("account", username);
        params.put("password", password);
        params.put("rpassword", rpassword);
        params.put("userName", realname);
        params.put("email", email);
        params.put("phone", phone);
        params.put("qq", qqnum);
        params.put("wechat", wechat);
        params.put("cardNo", cardNoNum);
        params.put("receiptPwd", cashpass);
        params.put("verifyCode", verifyCode);
        params.put("promoCode", promoCode);
        
        String paramJson = JsonUtil.toJson(params);
        
		if(promoCode == null) {
			super.renderFailure("邀请码不存在");
			return;
		}
		
		MemberGenrtLink link = generalizeLinkService.getByStationIdAndLinkCode(StationUtil.getStationId(),promoCode.toString() );
		if(link == null) {
			super.renderFailure("邀请码不存在");
			return;
		}
		Long type = SysRegisterConfig.TYPE_AGENT;
		if(StringUtil.equals(link.getType(), 2)){
			type = SysRegisterConfig.TYPE_MEMBER;
		}
		
		// 从Cookie中读取linkId
		params.put("linkId", link.getId()+"");
		sysAccountService.register(params, paramJson, type);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		super.renderSuccess();
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/register")
	public void register(@RequestParam(value = "data", required = true) String data, HttpServletRequest request,
			HttpServletResponse response) {
		// String json = request.getParameter("data");
		logger.error("注册时Content-Type = " + request.getHeader("Content-Type"));
		if (StringUtils.isEmpty(data)) {
			BufferedReader br;
			try {
				br = request.getReader();
				String str, wholeStr = "";
				while ((str = br.readLine()) != null) {
					wholeStr += str;
				}
				logger.error("注册时data 为空" + wholeStr);
			} catch (IOException e) {
			}
			throw new ParameterException("参数[data]值为空");
		}
		
		CheckUtils.checkWebShell(data);
		
		Map atMap = JsonUtil.toBean(data, Map.class);
		// 从Cookie中读取linkId
		atMap.put("linkId", CookieHelper.get(request, "linkId"));
		sysAccountService.register(atMap, data, SysRegisterConfig.TYPE_MEMBER);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		super.renderSuccess();
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/registertwo")
	public void registertwo(HttpServletRequest request,HttpServletResponse response) {
		
        String username = request.getParameter("account");
        String password = request.getParameter("password");
        String rpassword = request.getParameter("rpassword");
        String realname = request.getParameter("userName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String qqnum = request.getParameter("qq");
        String wechat = request.getParameter("wechat");
        String cardNoNum = request.getParameter("cardNo");
        String cashpass = request.getParameter("receiptPwd");
        String verifyCode = request.getParameter("verifyCode");
        String promoCode = request.getParameter("promoCode");
        String platForm = request.getParameter("platForm");

        Map<String, String> params = new HashMap<>();
        params.put("account", username);
        params.put("password", password);
        params.put("rpassword", rpassword);
        params.put("userName", realname);
        params.put("email", email);
        params.put("phone", phone);
        params.put("qq", qqnum);
        params.put("wechat", wechat);
        params.put("cardNo", cardNoNum);
        params.put("receiptPwd", cashpass);
        params.put("verifyCode", verifyCode);
        params.put("promoCode", promoCode);
        
		CheckUtils.checkWebShell(params);
		
		String paramJson = JsonUtil.toJson(params);
		
		// 从Cookie中读取linkId
		params.put("linkId", CookieHelper.get(request, "linkId"));
		sysAccountService.register(params, paramJson, SysRegisterConfig.TYPE_MEMBER);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		super.renderSuccess();
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/agtregister")
	public void agtregister(HttpServletRequest request, HttpServletResponse response) {
		
		// 跨域
		response.addHeader("Access-Control-Allow-Origin",request.getHeader("Origin"));//设置允许跨域请求
    	response.addHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    	response.addHeader("Access-Control-Allow-Credentials", "true");
		
		String json = super.$c("data");
		
		CheckUtils.checkWebShell(json);
		
		Map atMap = JsonUtil.toBean(json, Map.class);
		// 从Cookie中读取linkId
		String linkId = CookieHelper.get(request, "linkId");
		atMap.put("linkId", linkId);
		SysAccount ac = sysAccountService.register(atMap, json, SysRegisterConfig.TYPE_AGENT);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		if (ac.getAccountStatus().longValue() == SysAccount.ACCOUNT_STATUS_REVIEW) {
			renderJson(MixUtil.newHashMap("success", false, "msg", "注册成功，等待审核"), new String[] { HEADER_CEIPSTATE_BIZ });
			return;
		}
		super.renderSuccess();
	}
	
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/agtregistertwo")
	public void agtregistertwo(HttpServletRequest request, HttpServletResponse response) {
		//
        String username = request.getParameter("account");
        String password = request.getParameter("password");
        String rpassword = request.getParameter("rpassword");
        String realname = request.getParameter("userName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String qqnum = request.getParameter("qq");
        String wechat = request.getParameter("wechat");
        String cardNoNum = request.getParameter("cardNo");
        String cashpass = request.getParameter("receiptPwd");
        String verifyCode = request.getParameter("verifyCode");
        String promoCode = request.getParameter("promoCode");
        String platForm = request.getParameter("platForm");

        Map<String, String> params = new HashMap<>();
        params.put("account", username);
        params.put("password", password);
        params.put("rpassword", rpassword);
        params.put("userName", realname);
        params.put("email", email);
        params.put("phone", phone);
        params.put("qq", qqnum);
        params.put("wechat", wechat);
        params.put("cardNo", cardNoNum);
        params.put("receiptPwd", cashpass);
        params.put("verifyCode", verifyCode);
        params.put("promoCode", promoCode);
		
		CheckUtils.checkWebShell(params);
		
		String paramJson = JsonUtil.toJson(params);
		// 从Cookie中读取linkId
		String linkId = CookieHelper.get(request, "linkId");
		params.put("linkId", linkId);
		SysAccount ac = sysAccountService.register(params, paramJson, SysRegisterConfig.TYPE_AGENT);
		// 从Cookie中读取linkId
		CookieHelper.delete(response, request, "linkId");
		CookieHelper.delete(response, request, "linkCode");
		if (ac.getAccountStatus().longValue() == SysAccount.ACCOUNT_STATUS_REVIEW) {
			renderJson(MixUtil.newHashMap("success", false, "msg", "注册成功，等待审核"), new String[] { HEADER_CEIPSTATE_BIZ });
			return;
		}
		super.renderSuccess();
	}

	/**
	 * 第四版手机版用户登录/注册/试玩/代理
	 */
	@NotNeedLogin
	@RequestMapping("/toLogin")
	public String toLogin() {
		super.getRequest().setAttribute("registerTestGuest", StringUtils.equals("on",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_register_test_guest_station)));
		long time = System.currentTimeMillis();
		super.getRequest().setAttribute("loginKey", time);
		super.getSession().setAttribute("LOGIN_MEMBER_ONE_NOT_VERIFCODE", String.valueOf(time));
		String appId = StringUtil.trim2Null(StationConfigUtil.get(StationConfig.wechat_appid));
		String secret = StringUtil.trim2Null(StationConfigUtil.get(StationConfig.wechat_appsecret));
		String domain = StringUtil.trim2Null(StationConfigUtil.get(StationConfig.wechat_login_domain));
		if (StringUtils.isNotEmpty(appId) && StringUtils.isNotEmpty(secret) && StringUtils.isNotEmpty(domain)) {
			super.getRequest().setAttribute("wxChat", true);
		} else {
			super.getRequest().setAttribute("wxChat", false);
		}
		super.getRequest().setAttribute("isMobile", MobileUtil.JudgeIsMoblie());
		return goPage("/login.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/toRegister")
	public String toRegister() {
		super.getRequest().setAttribute("isMobile", MobileUtil.JudgeIsMoblie());
		super.getRequest().setAttribute("isWechatPop",StationConfigUtil.get(StationUtil.getStationId(), StationConfig.mobile_wechat_pop_tips));
		return goPage("/register.jsp");
	}

	@RequestMapping("/toArgRegister")
	@NotNeedLogin
	public String toArgRegister() {
		return goPage("/agtregister.jsp");
	}

}
