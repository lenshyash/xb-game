package com.game.controller.member;

import java.util.Objects;

import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.SysUtil;

import com.game.cache.redis.RedisAPI;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.SysStation;
import com.game.util.MobileUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

public class BaseMemberController extends ActionUtil{
	
	/**
	 * 获取站点资源目录
	 * @return
	 */
	public String getDomainFolder(){
		SysStation station =StationUtil.getStation();
		return StationUtil.getMemberFolder() + "/" + station.getFloder();
	}

	public String goPage(String jspPath){
		return getDomainFolder() + "/" + jspPath;
	}
	
	public String layoutRender(String path){
		SysStation station = StationUtil.getStation();
		path = path.replace("_{replacePath}_", "m");
		if(UserUtil.isLogin()){	//试玩帐号判断
			SysAccount acc = (SysAccount) SysUtil.getCurrentUser();
			if(Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
				super.getRequest().setAttribute("isGuest",true);
			}else{
				super.getRequest().setAttribute("isGuest",false);
			}
		}
		super.getRequest().setAttribute("folder", station.getFloder());
		super.getRequest().setAttribute("isMobile", MobileUtil.JudgeIsMoblie());
		return path; 
	}
	
}
