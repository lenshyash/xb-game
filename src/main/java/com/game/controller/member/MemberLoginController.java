package com.game.controller.member;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysStationFolderUrl;
import com.game.model.platform.AgentLunBo;
import com.game.model.vo.AccountVo;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentArticleService;
import com.game.service.GetConfigService;
import com.game.service.LotteryService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.service.SysStationFolderUrlService;
import com.game.util.ErrorUtil;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MobileUtil;
import com.game.util.ShellUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationOnOffUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.VerifyCodeUtil;

@Controller
public class MemberLoginController extends BaseMemberController {

	@Autowired
	private SysAccountService sysAccountService;

	@Autowired
	private MnyMoneyService moneyService;
	
	@Autowired
	GetConfigService gcService;
	
	@Autowired
	AgentArticleService articleService;

	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private GetConfigService getConfigService;
	@Autowired
	private SysStationFolderUrlService folderUrlService;
	@NotNeedLogin
	@RequestMapping("/login")
	@ResponseBody
	public void login() {
		String username = $c("account").trim().toLowerCase();
		String password = $c("password");
		String verifyCode = $("verifyCode");
		SysAccount acc;
		try {
			acc = sysAccountService.doLoginForMember(username, password, verifyCode);
		}catch (Exception e) {
	    	 Map<String, Object> json = new HashMap<String, Object>();
	         json.put("success", false);
	         json.put("msg", e.getMessage());
	         super.renderJson(json);
	         return;
		}
		if(acc == null){
			 Map<String, Object> json = new HashMap<String, Object>();
	         json.put("success", false);
	         json.put("msg", "您的账号存在可疑操作已被系统自动封禁，请联系客服！");
	         super.renderJson(json);
	         return;
		}
		super.renderSuccess();
	}

	@NotNeedLogin
	@RequestMapping({ "/zhandianbianhao", "/xbbh" })
	@ResponseBody
	public void zhanDianBianHao() {
		StringBuilder sb = new StringBuilder(StationUtil.getStation().getFloder());
		sb.append("\n彩票版本:").append(StationConfigUtil.get(StationConfig.lottery_template_name));
		String dl = StationConfigUtil.get(StationConfig.onoff_multi_agent);
		sb.append("\n代理模式：");
		if (StringUtils.equals(dl, "on")) {
			sb.append("多级代理");
		} else {
			sb.append("二级代理");
		}
		sb.append("\n集群：").append(StationConfigUtil.get(StationConfig.sys_server_name));
		String mobileVersion = StationConfigUtil.get(StationConfig.onoff_mobile_version);
		String mobileName = "";
		switch (mobileVersion) {
		case "v1":
		case "off":
			mobileName = "经典版V1";
			break;
		case "v3":
		case "on":
			mobileName = "优化版V3";
			break;
		case "v4":
			mobileName = "威尼斯V4";
			break;
		default:
			break;
		}
		sb.append("\n手机版本：").append(mobileName);
		super.renderText(sb.toString());
	}
	/**
	 * 模版中心
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/tmpPage")
	public String tmpPage(Model model) {
		String codesString = StationConfigUtil.get(StationConfig.templete_station_code_list);
		String[] codes = {};
		if (StringUtil.isNotEmpty(codesString)) {
			codes = codesString.split(",");
		}
		model.addAttribute("codes", codes);
		return "/common/template/templete.jsp";
	}

	/**
	 * 登录窗口
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/loginPage")
	public String loginPage(Model model) {
		if (LotteryVersionUtils.isV3OrV5(StationUtil.getStationId())) {
			lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		}
		String kfUrl = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		model.addAttribute("kfUrl",kfUrl);
		model.addAttribute("testAccount",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_register_test_guest_station));
		return goPage("/login.jsp");
	}

	/**
	 * APP下载
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/down")
	public String down(Model model) {
		SysStationFolderUrl link = folderUrlService.checkAppLink();
		model.addAttribute("downTitle",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.app_qr_code_link_title));
		model.addAttribute("iosapp", StringUtil.trim2Null(link.getIosDlUrl()));// IOS版本app下载地址
		model.addAttribute("Androidapp", StringUtil.trim2Null(link.getAndroidDlUrl()));// Android版本app下载地址
		model.addAttribute("qrcode", StringUtil.trim2Null(link.getIosQrUrl()));// IOS版本的app二维码地址
		model.addAttribute("qrcodeAndroid", StringUtil.trim2Null(link.getAndroidQrUrl()));// Android版本的app二维码地址
		model.addAttribute("logo",StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_page_logo_url));
		return SystemConfig.SOURCE_FOLDER_COMMON + "/modelCommon/down/down.jsp";
	}

	/**
	 * 线路检测
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/user_xljc")
	public String user_xljc() {
		return SystemConfig.SOURCE_FOLDER_COMMON + "/modelCommon/xljc/index.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/netNavV3")
	public String v3NetNav(Model model) {
		lotteryService.headerLot(model, LotteryUtils.LOTTERY_V3_NAV_NULL);
		List<AgentLunBo> lunBoList = getConfigService.getLunBo(StationUtil.getStationId(), new Integer[] { 4 });
		model.addAttribute("lunbo", lunBoList);
		model.addAttribute("isMobile", MobileUtil.JudgeIsMoblie());
		return goPage("/netNav.jsp");
	}

	/**
	 * 网站导航
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/netNav")
	public String netNav() {
		return SystemConfig.SOURCE_FOLDER_COMMON + "/modelCommon/netNav/netNav.jsp";
	}

	/**
	 * 登录窗口
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/_index")
	public String _index() {
		return goPage("/login.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/logout")
	public String logout() {
		sysAccountService.doLoginOut();
		return "redirect:/";
	}

	@NotNeedLogin
	@RequestMapping("/_logout")
	public String _logout() {
		sysAccountService.doLoginOut();
		return super.goPage("login.jsp");
	}

	/**
	 * 前端获取会员信息
	 */
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/meminfo")
	public void meminfo() {
		Map map = new HashMap();
		if (UserUtil.isLogin()) {
			MnyMoney m = moneyService.getMoneyById(UserUtil.getUserId());
			map.put("account", UserUtil.getUserAccount());
			map.put("money", m.getMoney());
			map.put("login", true);
		} else {
			map.put("login", false);
		}
		super.renderJson(map);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/verifycode")
	public void verifycode(Boolean flag) throws Exception {
		if (flag == null) {
			flag = true;
		}
		VerifyCodeUtil.createVerifyCode(flag, null);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/regVerifycode")
	public void regVerifycode(Boolean flag) throws Exception {
		if (flag == null) {
			flag = true;
		}
		VerifyCodeUtil.createVerifyCode(flag, StationConfig.register_verify_code_type);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/curstinfo")
	public void curstinfo() {
		AccountVo loginMember = new AccountVo();
		if (UserUtil.isLogin()) {
			MnyMoney m = moneyService.getMoneyById(UserUtil.getUserId());
			loginMember.setAccount(UserUtil.getUserAccount());
			loginMember.setMoney(m.getMoney());
			loginMember.setAccountType(UserUtil.getType());
		}
		String olserurl = StationConfigUtil.get(StationConfig.online_customer_service_url);
		super.renderJson(MixUtil.newHashMap("member", loginMember, "olserurl", olserurl));
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/navfooter")
	public void navfooter() {
		super.renderJson(articleService.getStationAtcs(StationUtil.getStationId()));
	}

	@NotNeedLogin
	@RequestMapping("/error")
	public String error(Integer code, HttpServletRequest request) {
		ErrorUtil.error(code, request);
		return "/common/template/erro/erro.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/loginError")
	public String loginError(String errorCode, HttpServletRequest request) {
		request.setAttribute("errorCode", errorCode);
		request.setAttribute("homepage", "/index.do");
		return "/common/handler/loginerror.jsp";
	}

//	@NotNeedLogin
//	@ResponseBody
//	@RequestMapping("/addgodcode")
//	public void addGodCode(String code) {
//		// 运行shell脚本
//		renderText("运行结果：" + ShellUtil.addGodCode(code));
//	}

	@NotNeedLogin
	@RequestMapping("/toContinue")
	public String toContinue() {
		return goPage("/continue.jsp");
	}
	
	/**
	 * 电竞
	 * 
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/esport")
	public String esport() {
		
		StationOnOffUtil.initConfig();
		super.getRequest().setAttribute("isIbcOnOff", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_ibc_dj_game));
		super.getRequest().setAttribute("isKxOnOff", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_kx_game));
		super.getRequest().setAttribute("helplst", gcService.getArticle(11, StationUtil.getStationId()));
		super.getRequest().setAttribute("navTab", "esport");
		if (UserUtil.getCurrentUser() != null) {
			super.getRequest().setAttribute("accountno", UserUtil.getCurrentUser().getLoginAccount());
		}
		return "/common/template/third/esport/index.jsp";
	}
}
