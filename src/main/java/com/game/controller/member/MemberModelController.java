package com.game.controller.member;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.JsonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.permission.annotation.NotNeedLogin;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/memInfo/common")
public class MemberModelController extends BaseMemberController {

	@NotNeedLogin
	@RequestMapping(value = "/{menu}.do")
	public String menu(@PathVariable String menu, HttpServletRequest request) {
		return goPage("/" + menu + ".jsp");

	}

	/**
	 * 获取在线客服地址 在线客服的url地址 :1 客服Skype:2 客服邮箱:3 客服电话 :4 网站手机端地址 :5
	 * 
	 * @return
	 */
	@RequestMapping(value = "/siteInfo/base")
	@ResponseBody
	@NotNeedLogin
	public String getOnlineCustomerServiceUrl(Integer type) {
		if (type == null) {
			type = 1;
		}
		StationConfig typeName = null;
		switch (type) {
			case 1:
				typeName = StationConfig.online_customer_service_url;
				break;
			case 2:
				typeName = StationConfig.basic_info_customer_skype;
				break;
			case 3:
				typeName = StationConfig.basic_info_customer_email;
				break;
			case 4:
				typeName = StationConfig.basic_info_customer_phone;
				break;
			case 5:
				typeName = StationConfig.basic_info_url_phone;
				break;
		}
		String val = StationConfigUtil.get(StationUtil.getStationId(), typeName);
		if (type == 5 && StringUtils.isEmpty(val)) {
			val = MobileUtil.getMobileDomain();
		}
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotEmpty(val)) {
			map.put("success", true);
			map.put("item", val);
			return JsonUtil.toJson(map);
		}
		map.put("success", false);
		return JsonUtil.toJson(map);

	}
}
