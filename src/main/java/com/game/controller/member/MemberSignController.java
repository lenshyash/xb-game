package com.game.controller.member;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.AgentSignRecord;
import com.game.service.SignService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH + "/sign")
public class MemberSignController extends BaseMemberController {
	
	@Autowired
	private SignService signService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("sign.jsp");
	}
	
	@RequestMapping("/signIn")
	@ResponseBody
	public void sign(){
		if("on".equals(StationConfigUtil.get(StationConfig.onoff_sign_in)) == false){
			throw new GenericException("未开启签到功能");
		}
		AgentSignRecord record = signService.sign();
		super.renderJson(MixUtil.newHashMap("success",true,"score",record.getScore(),"days",record.getSignDays()));
	}
	
	/**
	 * 获取最新30条签到纪录
	 */
	@RequestMapping("/zxqd")
	@ResponseBody
	public void zxqd(){
		Long stationId = StationUtil.getStationId();
		super.renderJson(signService.zxqd(stationId));
	}
	/**
	 * 分页获取签到纪录
	 */
	@RequestMapping("/zxqdRecord")
	@ResponseBody
	public void zxqdRecord(){
		Long stationId = StationUtil.getStationId();
		super.render(signService.zxqdRecord(stationId));
	}
	/**
	 * 根据月份获取会员的签到记录
	 */
	@RequestMapping("/signByMonth")
	@ResponseBody
	public void signByMonth(){
		String signYear = super.$c("signYear","年份参数为空!");//当前年份
		String signMonth = super.$c("signMonth","月份参数为空!");//当前月份
		String signDay = super.$c("signDay","月份天数为空!");//当前月份总天数
		Long userId = UserUtil.getUserId();
		super.renderJson(signService.signList(signYear,signMonth,signDay,userId));
	}
	
	
}
