package com.game.controller.member;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.game.core.SystemConfig;
import com.game.util.DateUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.MEMBER_CONTROL_PATH+"/memberV2")
public class MemberV2 extends BaseMemberController{
	
	/**
	 * 第二版会员中心
	 * @return
	 */
	@RequestMapping("/index")
	public String memberV2Center(){
		if(UserUtil.isLogin()){
			return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/index.jsp";
		}
		return getDomainFolder() + "/index.do";
	}
	
	/**
	 * 用户中心
	 * @return
	 */
	@RequestMapping("/userInfo")
	public String userInfo(){
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/userInfo.jsp";
	}
	
	/**
	 * 银行交易
	 * 额度转换
	 * @return
	 */
	@RequestMapping("/banktrans")
	public String banktrans(){
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/banktrans.jsp";
	}
	
	/**
	 * 交易记录
	 * @return
	 */
	@RequestMapping("/traderecord")
	public String traderecord(){
		super.getRequest().setAttribute("startTime", DateUtil.getCurrentDate("yyyy-MM-dd"));
		super.getRequest().setAttribute("endTime", DateUtil.getCurrentDate("yyyy-MM-dd"));
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/traderecord.jsp";
	}
	/**
	 * 往来记录
	 * @return
	 */
	@RequestMapping("/historyrecord")
	public String historyrecord(){
		super.getRequest().setAttribute("startTime", DateUtil.getCurrentDate("yyyy-MM-dd"));
		super.getRequest().setAttribute("endTime", DateUtil.getCurrentDate("yyyy-MM-dd"));
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/historyrecord.jsp";
	}
	
	/**
	 * 交易记录
	 * 在线存款
	 * @return
	 */
	@RequestMapping("/onlineCk")
	public String onlineCk(){
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/banktrans_1.jsp";
	}
	
	/**
	 * 交易记录
	 * 在线取款
	 * @return
	 */
	@RequestMapping("/onlineQk")
	public String onlineQk(){
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/banktrans_2.jsp";
	}
	
	/**
	 * 交易记录
	 * 在线兑换
	 * @return
	 */
	@RequestMapping("/onlineDh")
	public String onlineDh(){
		return SystemConfig.SOURCE_FOLDER_COMMON+"/template/member/centerV2/banktrans_3.jsp";
	}
}
