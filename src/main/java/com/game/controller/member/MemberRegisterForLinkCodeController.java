package com.game.controller.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.game.model.SysStation;
import com.game.service.SysStationService;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.platform.MemberGenrtLink;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AgentBaseConfigService;
import com.game.service.GeneralizeLinkService;
import com.game.service.LotteryService;
import com.game.util.CookieHelper;
import com.game.util.LotteryUtils;
import com.game.util.LotteryVersionUtils;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

/**
 * 根据推广链接跳转注册页
 */
@Controller
public class MemberRegisterForLinkCodeController extends BaseMemberController {

	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private SysStationService sysStationService;
	/**
	 * 多级代理
	 * 
	 * @param request
	 * @param linkCode
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/registerMutil/link_{linkCode}")
	public String registerMutilForLinkCode(Model request, @PathVariable String linkCode, HttpServletRequest req, HttpServletResponse response) {
		MemberGenrtLink mgl = generalizeLinkService.getByStationIdAndLinkCode(StationUtil.getStationId(), linkCode);
		if (mgl != null && mgl.getStatus() == 2) {
			RedisAPI.addCache(Contants.registerKey + SysUtil.getSession().getId(), mgl.getId() + "", 60 * 60 * 24);
			// 将原先放在session中的linkId放在cookie中
			CookieHelper.set(response, "linkId", mgl.getId() + "");
			CookieHelper.set(response, "linkCode", mgl.getLinkKey());
			String url;
			if (LotteryVersionUtils.isV3OrV5(StationUtil.getStationId())) {
				lotteryService.headerLot(request, LotteryUtils.LOTTERY_V3_NAV_NULL);
			}
			if (MobileUtil.isMoblie(false)) {
				url = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.link_code_mobile_index_page);
				if (StringUtil.isNotEmpty(url)) {
					return "redirect:" + url;
				}
				// return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH +
				// "/register.do";
				if (StringUtil.equals(mgl.getType(), 1)) {
					return SystemConfig.MOBILE_CONTROL_PATH + "/anew/agtRegister.jsp";
				}else if(StringUtil.equals(mgl.getType(), 2)) {
					return SystemConfig.MOBILE_CONTROL_PATH + "/anew/register.jsp";
				}
				
			}
			//新版X版邀请链接跳转
			String folder = StationUtil.getFirstFolder();
			if(StringUtil.isEmpty(folder)){
				SysStation sysStation = sysStationService.findOneById(StationUtil.getStationId());
				folder= sysStation.getFloder();
			}
			if("x".equals(folder.substring(0,1))){
				System.out.println("return \"common/template/lottery/x002/agtregister.jsp\";");
				return "/common/template/lottery/x002/agtregister.jsp";
			}

			if (StringUtil.equals(mgl.getType(), 1)) {
				url = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.link_code_daili_index_page);
				if (StringUtil.isNotEmpty(url)) {
					return "redirect:" + url;
				}
				return super.goPage("agtregister.jsp");
			}
			url = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.link_code_member_index_page);
			if (StringUtil.isNotEmpty(url)) {
				return "redirect:" + url;
			}

			return super.goPage("register.jsp");
		} else {
			request.addAttribute("code", 404);
			request.addAttribute("msg", "该推广链接不存在,请核对后重试!");
			return super.goPage("erro.jsp");
		}

	}

	/**
	 * 一级代理
	 * 
	 * @param request
	 * @param init
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/registersAlone")
	public String registerAloneForLinkCode(HttpServletRequest request, String init, HttpServletResponse response) {
		MemberGenrtLink mgl = generalizeLinkService.getByStationIdAndLinkCode(StationUtil.getStationId(), init);
		if (mgl != null && mgl.getStatus() == 2) {
			RedisAPI.addCache(Contants.registerKey + SysUtil.getSession().getId(), mgl.getId() + "", 60 * 60 * 24);
			// 将原先放在session中的linkId放在cookie中
			CookieHelper.set(response, "linkId", mgl.getId() + "");
			CookieHelper.set(response, "linkCode", mgl.getLinkKey());
			if (StringUtil.equals(mgl.getType(), 2)) {
				String url = null;
				if (MobileUtil.isMoblie(false)) {
					//v4版本需要使用
					url = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.link_code_mobile_index_page);
					//获取微信弹窗开关
					super.getRequest().setAttribute("isWechatPop",StationConfigUtil.get(StationUtil.getStationId(), StationConfig.mobile_wechat_pop_tips));
					if (StringUtil.isNotEmpty(url)) {
						return "redirect:" + url;
					}
					String version = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name);
					if("v4".equals(version)){
						return "redirect:/toRegister.do";
					}
					// return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH +
					// "/register.do";
					return SystemConfig.MOBILE_CONTROL_PATH + "/anew/register.jsp";
				}
				url = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.link_code_member_index_page);
				if (StringUtil.isNotEmpty(url)) {
					return "redirect:" + url;
				}
				return super.goPage("register.jsp");
			}
			return super.goPage("erro.jsp");
		} else {
			request.setAttribute("code", 404);
			request.setAttribute("msg", "该推广链接不存在,请核对后重试!");
			return super.goPage("erro.jsp");
		}
	}

	/**
	 * 加密推广链接，为了防止qq等聊天攻击标识为危险网址，顾使用vote_topic_开头，伪装成投票系统
	 * 
	 * @param request
	 * @param linkCode
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/vote_topic_{linkKey}")
	public String memberVote(HttpServletRequest request, @PathVariable String linkKey, Model model) {
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(StationUtil.getStationId());// 匹配多级代理开关
		String domain = getDomain(request);
		String url = "";
		if (isMulti) {
			url = domain + "/registerMutil/link_" + linkKey + ".do";
		} else {
			url = domain + "/registersAlone.do?init=" + linkKey;
		}
		model.addAttribute("url", encodeUrl(url));
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/jump.jsp";
	}

	private String getDomain(HttpServletRequest request) {
		String scheme = request.getScheme();
		int serverPort = request.getServerPort();

		StringBuilder currentUrl = new StringBuilder();
		currentUrl.append(scheme);
		currentUrl.append("://");
		currentUrl.append(request.getServerName());
		if (!(("http".equals(scheme) && serverPort == 80) || ("https".equals(scheme) && serverPort == 443))) {
			currentUrl.append(":").append(serverPort);
		}
		currentUrl.append(request.getContextPath());

		return currentUrl.toString();
	}

	/**
	 * 将url根据规则生成为加密串
	 * 
	 * @param url
	 * @return
	 */
	private String encodeUrl(String url) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < url.length(); i++) {
			if (i > 0) {
				sb.append("+");
			}
			sb.append("l(").append(encodeUrlChar(url.charAt(i))).append(")");
		}
		return sb.toString();
	}

	/**
	 * url字符加密规则
	 * 
	 * @param c
	 * @return
	 */
	private int encodeUrlChar(char c) {
		return c * 78 * 20000 + (int) (Math.random() * 20000);
	}
	
	/**
	 * 页面跳转
	 * 
	 * @param request
	 * @param linkCode
	 * @return
	 */
	@NotNeedLogin
	@RequestMapping("/registerMutil")
	public String registerMutilForLinkCode(HttpServletRequest req, HttpServletResponse response) {
		return SystemConfig.SOURCE_FOLDER_COMMON + "/template/member/registerMutil.jsp";
	}
}
