package com.game.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.game.model.SysStation;
import com.game.service.SysStationService;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.controller.member.BaseMemberController;
import com.game.core.SystemConfig;
import com.game.model.AdminUser;
import com.game.model.SysStationDomain;
import com.game.model.SysStationFolderUrl;
import com.game.model.vo.AdminMenuNode;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AdminMenuService;
import com.game.service.GetConfigService;
import com.game.service.SysStationFolderUrlService;
import com.game.util.MobileUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
public class HomepageController extends BaseMemberController {

	@Autowired
	private AdminMenuService adminMenuService;

	@Autowired
	private GetConfigService getConfigService;
	
	@Autowired
	private SysStationFolderUrlService folderUrlService;
	@Autowired
	private SysStationService sysStationService;
	@NotNeedLogin
	@RequestMapping("/index")
	public String index(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (StationUtil.isAdminPage()) {// 总控后台
			if (SysUtil.isLogin()) {
				AdminUser user = (AdminUser) UserUtil.getCurrentUser();
				AdminMenuNode menu = adminMenuService.getMenuCache(user.getGroupId());
				request.setAttribute("menu", menu.getNodes());
				return SystemConfig.SOURCE_FOLDER_ADMIN + "/page/home.jsp";
			} else {
				return SystemConfig.SOURCE_FOLDER_ADMIN + "/index.jsp";
			}
		}

		Long domainType = StationUtil.getDomainType();
		if (domainType != null && domainType == SysStationDomain.TYPE_AGENT) {// 租户后台
			if (SysUtil.isLogin()) {
				return SystemConfig.SOURCE_FOLDER_AGENT + "/page/home.jsp";
			}
			response.setStatus(404);
			return null;
		}

		Long stationId = StationUtil.getStationId();

		String defaultHome = StationUtil.getDefaultHome();
		
		request.setAttribute("isCpOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_lottery_game));
		request.setAttribute("isTyOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_sports_game));
		request.setAttribute("isZrOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_zhen_ren_yu_le));
		request.setAttribute("isDzOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_dian_zi_you_yi));
		request.setAttribute("isLhcOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_liu_he_cai));
		request.setAttribute("isChessOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_chess));
		request.setAttribute("isDjOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_dian_jing_yu_le));
		request.setAttribute("accessStatisticsCode", StationConfigUtil.get(stationId, StationConfig.access_statistics_code));
		request.setAttribute("appAccessStatisticsCode",
				StationConfigUtil.get(stationId, StationConfig.app_access_statistics_code));
		request.setAttribute("isAppASCOnoff",
				StationConfigUtil.get(stationId, StationConfig.app_onoff_access_statistics_code));
		request.setAttribute("testAccount", StationConfigUtil.get(stationId, StationConfig.on_off_register_test_guest_station));
		
		request.setAttribute("copyright", StationConfigUtil.get(stationId, StationConfig.basic_info_website_copyright));

		request.setAttribute("isReg", StationConfigUtil.get(stationId, StationConfig.onoff_register));
		request.setAttribute("isExChgOnOff", StationConfigUtil.get(stationId, StationConfig.exchange_score));
		request.setAttribute("isChangeMoney", StationConfigUtil.get(stationId, StationConfig.onoff_change_money));
		request.setAttribute("isQdOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_sign_in));
		request.setAttribute("isZpOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_turnlate));
		request.setAttribute("isProxyDailyRebate", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.proxy_daily_rebate));
		request.setAttribute("logo", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_page_logo_url));
		request.setAttribute("isRedPacket", StationConfigUtil.get(stationId, StationConfig.onoff_member_red_packet));
		request.setAttribute("chatroom", StationConfigUtil.get(stationId, StationConfig.basic_info_customer_chatroom));
		request.setAttribute("appQRCodeLinkIos", StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_ios));
		request.setAttribute("appQRCodeLinkAndroid", StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_android));
		request.setAttribute("memberRegisterOnFlag", StationConfigUtil.get(stationId, StationConfig.onoff_member_register));
		request.setAttribute("dailiRegisterOnFlag", StationConfigUtil.get(stationId, StationConfig.onoff_daili_register));
		
		String kfUrl = StationConfigUtil.get(stationId, StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		//检查在线客服是否配置模板指定URL地址
		SysStationFolderUrl link2 = folderUrlService.findByFolder(StationUtil.getDomainFolder(), stationId,2); 
		if(link2!=null){
			if(StringUtil.isNotEmpty(link2.getKfUrl())){
				kfUrl = link2.getKfUrl();
			}
		}
		request.setAttribute("kfUrl", kfUrl);
		
		if (MobileUtil.isMoblie(false)) {
			request.setAttribute("onoffMobileChat", StationConfigUtil.get(StationConfig.onoff_mobile_chat));
			if (StringUtils.equals("v4", StationConfigUtil.get(stationId, StationConfig.lottery_template_name))) {
				request.setAttribute("isMobile", true);
				request.setAttribute("lunbo", getConfigService.getLunBo(stationId, 1));
				return indexPage(defaultHome, response);
			}
			String showPc = StationConfigUtil.get(stationId, StationConfig.wap_first_page_is_pc);
			if (StringUtils.equals("on", showPc)) {
				request.setAttribute("showPc", showPc);
				return indexPage(defaultHome, response);
			}
			if (StringUtils.equals("on", StationConfigUtil.get(stationId, StationConfig.wap_first_jump_page))) {
				String mobileTo = request.getParameter(BusinessConstant.MOBILE_JUMP_SESSION_KEY);
				if (StringUtils.equals(mobileTo, "pc")) {
					request.getSession().setAttribute(BusinessConstant.MOBILE_JUMP_SESSION_KEY, "pc");
					return indexPage(defaultHome, response);
				}
				mobileTo = (String) request.getSession().getAttribute(BusinessConstant.MOBILE_JUMP_SESSION_KEY);
				if (StringUtils.isNotEmpty(mobileTo)) {
					if (StringUtils.equals(mobileTo, "pc")) {
						return indexPage(defaultHome, response);
					}
					return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + "/index.do";
				}
				request.setAttribute("indexLogo1",
						StationConfigUtil.get(stationId, StationConfig.lottery_page_logo_url));
				SysStationFolderUrl link = folderUrlService.checkAppLink();
				request.setAttribute("iosapp", StringUtil.trim2Null(link.getIosDlUrl()));// IOS版本app下载地址
				request.setAttribute("Androidapp", StringUtil.trim2Null(link.getAndroidDlUrl()));// Android版本app下载地址
				request.setAttribute("qrcode", StringUtil.trim2Null(link.getIosQrUrl()));// IOS版本的app二维码地址
				request.setAttribute("qrcodeAndroid", StringUtil.trim2Null(link.getAndroidQrUrl()));// Android版本的app二维码地址
				return SystemConfig.MOBILE_CONTROL_PATH + "/jumpIndex.jsp";
			}
			if(StringUtils.isNotEmpty(defaultHome) && defaultHome.startsWith("regpage") || defaultHome.startsWith("/regpage")){
				String mobileVersion  = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_version);
				switch (mobileVersion) {
				case "v1":
				case "off":
					return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + defaultHome;
				case "on":
				case "v3":
					return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH+"/v3" + defaultHome;
				case "v4":
					return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH+"/v4" + defaultHome;
				default:
					return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + defaultHome;
				}
			}
			return "redirect:" + SystemConfig.MOBILE_CONTROL_PATH + "/index.do";
		}
		if(StringUtils.equals(request.getParameter("toindex"),"1")) {
			return super.goPage("/index.jsp");
		}
		return indexPage(defaultHome, response);
	}

	private String indexPage(String defaultHome, HttpServletResponse response) throws IOException {
		//判断是否存在导航页
		/*String isNav = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.offon_station_first_nav_page);
		if(isNav!=null && "on".equals(isNav)){
			return super.goPage("nav.jsp");
		}*/
		
		if (StringUtils.isNotEmpty(defaultHome)) {
			if (StringUtils.startsWith(defaultHome, "forward:")) {
				return defaultHome;
			}
			if ((StringUtils.equals("regpage.do", defaultHome) || StringUtils.equals("loginPage.do", defaultHome)
					|| StringUtils.equals("index/loginInterface.do", defaultHome)) && SysUtil.isLogin()) {
				return super.goPage("/index.jsp");
			}
			response.sendRedirect(defaultHome);
			return null;
		}
		//如果是x版则走统一模板
		String folder = StationUtil.getFirstFolder();
		if(StringUtil.isEmpty(folder)){
			SysStation sysStation = sysStationService.findOneById(StationUtil.getStationId());
			folder= sysStation.getFloder();
		}
		if("x".equals(folder.substring(0,1))){
			return "/common/template/lottery/x002/dist/index.html";
		}
		return super.goPage("/index.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/timeout")
	public String timeout() {
		return "timeout.jsp";
	}
	
	
	@NotNeedLogin
	@RequestMapping("/nav")
	public String nav(HttpServletRequest request) {
		String kfUrl = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		request.setAttribute("kfUrl", kfUrl);
		return super.goPage("/nav.jsp");
	}
}
