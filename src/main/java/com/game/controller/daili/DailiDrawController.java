package com.game.controller.daili;

import java.util.HashMap;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.SysAccountInfo;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyDrawRecordService;
import com.game.service.SysAccountService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dldraw")
public class DailiDrawController extends BaseDailiController {
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	private MnyDrawRecordService mdrService;

	@RequestMapping("/index")
	public String page() {
		Long accountId = UserUtil.getUserId();
		SysAccountInfo atInfo = sysAccountService.getAccountInfo(accountId);
		if (StringUtil.isEmpty(atInfo.getReceiptPwd())) {
			return super.goPage("/page/dlrepwdset.jsp");
		} else if (StringUtil.isEmpty(atInfo.getCardNo()) || StringUtil.isEmpty(atInfo.getUserName())
				|| StringUtil.isEmpty(atInfo.getBankName())) {
			return super.goPage("/page/dlbankset.jsp");
		}
		return super.goPage("/page/dldrawpg.jsp");
	}

	@ResponseBody
	@RequestMapping("/bkinfo")
	public void drawbkinfo() {
		Long memberId = UserUtil.getUserId();
		SysAccountInfo member = sysAccountService.getAccountInfo(memberId);
		super.renderJson(member);
	}

	@ResponseBody
	@RequestMapping("/savebank")
	public void savebank() {
		String bankName = super.$("bankName");
		String userName = super.$("userName");
		String cardNo = super.$("cardNo");
		String province = super.$("province");
		String phone = super.$("phone");
		String city = super.$("city");
		String bankAddress = super.$("bankAddress");
		String repPwd = super.$("repPwd");
		AccountVo avo = new AccountVo();
		avo.setBankName(bankName);
		avo.setUserName(userName);
		avo.setCardNo(cardNo);
		avo.setProvince(province);
		avo.setPhone(phone);
		avo.setCity(city);
		avo.setBankAddress(bankAddress);
		avo.setReceiptPwd(repPwd);
		avo.setId(UserUtil.getUserId());
		sysAccountService.saveBankInfo(avo,true);

		super.renderSuccess();
	}

	/**
	 * 更改用户取款密码
	 */
	@ResponseBody
	@RequestMapping("/repwd")
	public void repwd() {
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		sysAccountService.updRepwdByAccount(pwd, rpwd);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/drawdata")
	public void drawdata() {
		Long accountId = UserUtil.getUserId();
		Map data = new HashMap();
		Map account = sysAccountService.getAccountById(accountId, StationUtil.getStationId());
		String max = StationConfigUtil.get(StationConfig.withdraw_max_money);
		String min = StationConfigUtil.get(StationConfig.withdraw_min_money);
		String star = StationConfigUtil.get(StationConfig.money_start_time);
		String end = StationConfigUtil.get(StationConfig.money_end_time);
		String desc = StationConfigUtil.get(StationConfig.withdraw_desc);
		data.put("max", max);
		data.put("min", min);
		data.put("star", star);
		data.put("end", end);
		data.put("account", account);
		data.put("desc", desc);
		super.renderJson(data);
	}

	@ResponseBody
	@RequestMapping("/drawcommit")
	public void drawcommit() {
		String money = super.$("money");
		String repPwd = super.$("repPwd");
		if(!money.matches("^[1-9]([\\d]{0,7})?$")){
			throw new GenericException("取款金额必须是整数");
		}
		SysAccountInfo member = sysAccountService.getAccountInfo(UserUtil.getUserId());
		if (StringUtil.isEmpty(member.getReceiptPwd())) {
			throw new GenericException("请先设置提款密码");
		} else if (StringUtil.isEmpty(member.getCardNo()) || StringUtil.isEmpty(member.getUserName())
				|| StringUtil.isEmpty(member.getBankName())) {
			throw new GenericException("请先完善出款银行信息");
		}
		
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(UserUtil.getUserId());
		moneyVo.setAccount(UserUtil.getCurrentUser().getLoginAccount());
		moneyVo.setReceiptPwd(repPwd);
		moneyVo.setMoney(StringUtil.toBigDecimal(money));
		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ONLINE);
		moneyVo.setRemark("提款申请处理中");
		mdrService.withdraw(moneyVo);
		super.renderSuccess();
	}
}
