package com.game.controller.daili;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.service.AgentBaseConfigService;
import com.game.service.MnyMoneyRecordService;
import com.game.service.OrderDetailService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlmnyrd")
public class DailiMnyRdController extends BaseDailiController {

	@Autowired
	private MnyMoneyRecordService mnyMoneyService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private OrderDetailService orderDetailService;

	@RequestMapping("/index")
	public String index() {
		String curDate = DateUtil.getCurrentDate();
		String isDuLiCaiPiao = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_du_li_cai_piao);
		if("on".equals(isDuLiCaiPiao)){
			super.getRequest().setAttribute("isDuLiCaiPiao", true);
		}
		String third = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_show_pay_third);//第三方存款
		String quick = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_show_pay_quick);//快速存款
		String normal = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_show_pay_normal);//一般存款
		if("on".equals(third)){
			super.getRequest().setAttribute("isThird", true);
		}
		if("on".equals(normal)){
			super.getRequest().setAttribute("isNormal", true);
		}
		if("on".equals(quick)){
			super.getRequest().setAttribute("isQuick", true);
		}
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(StationUtil.getStationId());
		boolean isFanShui = "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_fan_shui));
		super.getRequest().setAttribute("isFanShui", isFanShui);
		super.getRequest().setAttribute("isMutil", isMulti);
		return super.goPage("/page/dlmnyrd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		String orderId = super.$("orderId");
		Long type = super.$long("type");

		MnyMoneyRecordVo moneyRecordVo = new MnyMoneyRecordVo();
		moneyRecordVo.setAccount(account);
		moneyRecordVo.setType(type);
		moneyRecordVo.setBegin(DateUtil.toDatetime(begin));
		moneyRecordVo.setEnd(DateUtil.toDatetime(end));
		moneyRecordVo.setParents(UserUtil.getChildren());
		moneyRecordVo.setSearchSelf(true);
		moneyRecordVo.setOrderId(orderId);
		moneyRecordVo.setSelfId(UserUtil.getUserId());
		super.render(mnyMoneyService.getMoneyRecord(moneyRecordVo));
	}

	@Permission(CheckType.OPEN)
	@ResponseBody
	@RequestMapping("/money/record/type")
	public void queryMnyRdType() {
		List<Map> mrts = new ArrayList<Map>();
		MoneyRecordType[] types = MoneyRecordType.values();
		for (int i = 0; i < types.length; i++) {
			mrts.add(MixUtil.newHashMap("type", types[i].getType(), "name", types[i].getName()));
		}
		super.renderJson(JsonUtil.toJson(mrts));
	}
	
	@RequestMapping("/orderDesc")
	@ResponseBody
	public void orderDesc(String orderId) {
		if (StringUtils.isEmpty(orderId)) {
			throw new GenericException("参数错误!");
		}
		if (orderId.startsWith("S")) {
			super.renderJson(orderDetailService.getSportOrderDetail(orderId, StationUtil.getStationId()));
		} else {
			super.renderJson(orderDetailService.getLotteryOrderDetailForList(orderId, null, StationUtil.getStationId(), null));
		}
	}
}
