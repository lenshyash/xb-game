package com.game.controller.daili;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.SysLog;
import com.game.model.vo.SysLoginLogVo;
import com.game.service.SysLogService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlloginlog")
public class DailiLoginLogController extends BaseDailiController {

	@Autowired
	SysLogService logService;

	@RequestMapping("/index")
	public String index(String begin, String end, Map<String, Object> map) {
		String curDate = DateUtil.getCurrentDate();
		map.put("startTime", begin == null ? curDate : begin);
		map.put("endTime", end == null ? curDate : end);
		return super.goPage("/page/dlloginlog.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list(String loginOs) {
		Long type = super.$long("type");
		Long accountType = super.$long("accountType");
		Long searchType = super.$long("searchType");
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		String agentName = super.$("agentName");
		SysLoginLogVo slvo = new SysLoginLogVo();
		slvo.setStationId(StationUtil.getStationId());
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDatetime(begin));
		slvo.setEnd(DateUtil.toDatetime(end));
		slvo.setType(type);
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setIp(ip);
		slvo.setLoginOs(loginOs);
		
		slvo.setAgentName(agentName);
		slvo.setParents(UserUtil.getChildren());
		slvo.setSelfId(UserUtil.getUserId());
		slvo.setSearchSelf(true);
		slvo.setAccountType(accountType);
		slvo.setSearchType(searchType);
		super.render(logService.getLoginLogPage(slvo));
	}
}
