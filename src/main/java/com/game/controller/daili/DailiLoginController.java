package com.game.controller.daili;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.SysAccountService;
import com.game.util.ErrorUtil;
import com.game.util.SecurityCardUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.VerifyCodeUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH)
public class DailiLoginController extends BaseDailiController {

	@Autowired
	private SysAccountService sysAccountService;

	@NotNeedLogin
	@RequestMapping("/index")
	public String index() {
		if (SysUtil.isLogin()) {
			return super.goPage("/page/home.jsp");
		} else {
			return super.goPage("/index.jsp");
		}
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/login")
	public void login() {
		String account = $c("account").trim().toLowerCase();
		String password = $c("password");
		String verifyCode = $("verifyCode");
		sysAccountService.doLoginForDaili(account, password, verifyCode);
		if (SecurityCardUtil.reponseCardInfo()) {
			return;
		}
		renderSuccess();
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/login4Card")
	public void login4Card() {
		String code = $c("code", "请输入密保卡").trim();
		sysAccountService.login4Card(code);
		renderSuccess();
	}

	@RequestMapping(value = "/updloginpwd")
	@ResponseBody
	public void updloginpwd() {
		Long pwdType = super.$long("pwdType");
		String opwd = super.$("opwd");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		sysAccountService.updCurUserPwd(opwd, pwd, rpwd, pwdType);
		super.renderSuccess();
	}

	@RequestMapping(value = "/repwdset")
	public String repwdset() {
		return super.goPage("/page/dlresetpwdset.jsp");
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		Long id = UserUtil.getUserId();
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		sysAccountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/perinfopg")
	public String perinfopg() {
		return super.goPage("/page/dlperinfo.jsp");
	}

	@RequestMapping(value = "/perinfo")
	@ResponseBody
	public void perinfo() {
		super.renderJson(sysAccountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId()));
	}

	@NotNeedLogin
	@RequestMapping("/logout")
	public String logout() {
		sysAccountService.doLoginOut();
		return super.goPage("/index.jsp");
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/verifycode")
	public void verifycode() throws Exception {
		VerifyCodeUtil.createVerifyCode(true, null);
	}

	@NotNeedLogin
	@RequestMapping("/error")
	public String error(Integer code, HttpServletRequest request) {
		ErrorUtil.error(code, request);
		return "/common/template/erro/erro.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/loginError")
	public String loginError(String errorCode, HttpServletRequest request) {
		request.setAttribute("errorCode", errorCode);
		request.setAttribute("homepage", "/daili/index.do");
		return "/common/handler/loginerror.jsp";
	}

	@NotNeedLogin
	@RequestMapping("/systemmaintenance")
	public String systemmaintenance(HttpServletRequest request) {
		Long stationId = StationUtil.getStationId();
		request.setAttribute("system_maintenance", StationConfigUtil.get(stationId, StationConfig.system_maintenance));
		request.setAttribute("isSysOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_system));
		return SystemConfig.SOURCE_FOLDER_COMMON + "/erro/systemmaintenance.jsp";
	}
}
