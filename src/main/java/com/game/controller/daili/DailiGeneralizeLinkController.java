package com.game.controller.daili;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.platform.MemberGenrtLink;
import com.game.model.vo.AccountVo;
import com.game.service.AgentBaseConfigService;
import com.game.service.GeneralizeLinkService;
import com.game.service.SysAccountService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

import groovyjarjarantlr.collections.Stack;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/generalizeLink")
public class DailiGeneralizeLinkController extends BaseDailiController {

	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private SysAccountService accountService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		StringBuffer requestUrl = request.getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		Long stationId = StationUtil.getStationId();
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(stationId)&&!"on".equals(StationConfigUtil.get(StationConfig.onoff_agent_merge_Rebate_odds_switch));// 匹配多级代理开关
		String url = "";
		if (isMulti) {
			url = domain + "/registerMutil/link_link998.do";
		} else {
			url = domain + "/registersAlone.do?init=link998";
		}
		request.setAttribute("url", url);
		request.setAttribute("domain", domain);
		request.setAttribute("isMulti", isMulti);
		request.setAttribute("profitShare", StationConfigUtil.get(StationConfig.onoff_profit_share));
		request.setAttribute("memberRate", StationConfigUtil.get(StationConfig.onoff_member_rate_random));
		request.setAttribute("mergeRebate", "on".equals(StationConfigUtil.get(StationConfig.onoff_agent_merge_Rebate_odds_switch)));
		request.setAttribute("trunOnoff", StationConfigUtil.get(StationConfig.onoff_register_account_trun_agent));
		return super.goPage("/page/generalizeLink.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(HttpServletRequest request, Integer type) {
		String userAccount = UserUtil.getUserAccount();
		super.render(generalizeLinkService.page(StationUtil.getStationId(), request, userAccount, type,null));
	}

	@RequestMapping("/updateStatusById")
	@ResponseBody
	public void updateStatusById(Long id, Integer status) {
		generalizeLinkService.updateStatusById(id, status, StationUtil.getStationId());
		super.renderSuccess();
	}

	@RequestMapping("/getRandCode")
	@ResponseBody
	public void getRandCode() {
		super.renderJson(generalizeLinkService.getRandCode(StationUtil.getStationId()));
	}

	@ResponseBody
	@RequestMapping("/multidata")
	public void multidata() {
		AccountVo avo = new AccountVo();
		avo.setId(UserUtil.getUserId());
		avo.setGeneralize(true);
		super.renderJson(accountService.getAgentMultiData(avo));
	}

	@RequestMapping("/updateOrSave")
	@ResponseBody
	public void updateOrSave(MemberGenrtLink mgl, HttpServletRequest request) {
		generalizeLinkService.updateOrSave(mgl);
		super.renderSuccess();
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(Long id) {
		Long stationId = StationUtil.getStationId();
		generalizeLinkService.del(id, stationId);
		super.renderSuccess();
	}

}
