package com.game.controller.daili;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.core.SystemConfig;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlstatistic")
public class DailiTotalStatisticController extends BaseDailiController {

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		super.getRequest().setAttribute("account", $("account"));
		super.getRequest().setAttribute("agentName", $("agentName"));
		super.getRequest().setAttribute("searchType", $("searchType"));
		return super.goPage("/page/dltotalStatistic.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String account, String agentName, String startTime, String endTime, Long searchType,
			Integer pageNumber, Integer pageSize) {
		String key = "totalstat:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		Long stationId = StationUtil.getStationId();
		super.render(dailyMoneyService.getTotalStatistic(stationId, account, agentName,
				DateUtil.validStartDateTime(startTime), DateUtil.validEndDateTime(endTime), searchType, pageNumber,
				pageSize, null, null, null));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}

}
