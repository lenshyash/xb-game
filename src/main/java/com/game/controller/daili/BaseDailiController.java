package com.game.controller.daili;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.ActionUtil;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.util.StationConfigUtil;

public class BaseDailiController extends ActionUtil {
	/**
	 * 获取资源目录
	 * 
	 * @return
	 */
	public String getDomainFolder() {
		return SystemConfig.SOURCE_FOLDER_DAILI;
	}

	public String goPage(String jspPath) {
		HttpServletRequest request = super.getRequest(); 
		String lottery = StationConfigUtil.get(StationConfig.onoff_lottery_game);
		String six = StationConfigUtil.get(StationConfig.onoff_liu_he_cai);
		String real = StationConfigUtil.get(StationConfig.onoff_zhen_ren_yu_le);
		String electronic = StationConfigUtil.get(StationConfig.onoff_dian_zi_you_yi);
		String sport = StationConfigUtil.get(StationConfig.onoff_sports_game);
		String realSport = StationConfigUtil.get(StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(StationConfig.onoff_chess);
		String dj = StationConfigUtil.get(StationConfig.onoff_dian_jing_yu_le);
		String showDepositRecord = StationConfigUtil.get(StationConfig.agent_member_deposit_record);

		request.setAttribute("isLotteryRecordOpened", (lottery!=null&&lottery.equals("on"))?true:false);
		request.setAttribute("isSixRecordOpened",  (six!=null&&six.equals("on"))?true:false);
		request.setAttribute("isRealRecordOpened",  (real!=null&&real.equals("on"))?true:false);
		request.setAttribute("isElectronicRecordOpened",  (electronic!=null&&electronic.equals("on"))?true:false);
		request.setAttribute("isSportRecordOpened",  (sport!=null&&sport.equals("on"))?true:false);
		request.setAttribute("isRealSportOpened",  (realSport!=null&&realSport.equals("on"))?true:false);
		request.setAttribute("isThirdLotteryOpened",  (thirdLottery!=null&&thirdLottery.equals("on"))?true:false);
		request.setAttribute("isChessOpened",  (chess!=null&&chess.equals("on"))?true:false);
		request.setAttribute("isDjOpened",  (dj!=null&&dj.equals("on"))?true:false);
		request.setAttribute("isMemberDepositOpened",  "on".equals(showDepositRecord));
		return getDomainFolder() + "/" + jspPath;
	}
}
