package com.game.controller.daili;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.MnyComRecord;
import com.game.model.vo.MnyComRecordVo;
import com.game.service.MnyComRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/deposit")
@Controller
public class DailiDepositController extends BaseDailiController {

	@Autowired
	MnyComRecordService mcrService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		String curDate = DateUtil.getCurrentDate();
		String begin = request.getParameter("begin");
		String end = request.getParameter("end");
		super.getRequest().setAttribute("startTime", begin == null ? curDate : begin);
		super.getRequest().setAttribute("endTime", end == null ? curDate : end);
		super.getRequest().setAttribute("saccount", request.getParameter("account"));
		return super.goPage("/page/payonlinerd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		Long type = super.$long("type");
		// Long status = super.$long("status");
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		Long status = super.$long("status");
		String agentName = super.$("agent");
		Long searchType = super.$long("searchType");
		Long comTimes = super.$long("comTimes");
		
		Date beginDate = DateUtil.toDatetime(begin);
		Date endDate = DateUtil.toDatetime(end);

		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setType(type);
		mcrVo.setAccount(account);
		mcrVo.setStatus(status);
		mcrVo.setBegin(beginDate);
		mcrVo.setEnd(endDate);
		mcrVo.setParents(UserUtil.getChildren());
		mcrVo.setSelfId(UserUtil.getUserId());
		mcrVo.setSearchSelf(true);
		mcrVo.setSearchType(searchType);
		mcrVo.setAgentName(agentName);
		mcrVo.setComTimes(comTimes);
		super.render(mcrService.getPage(mcrVo));
	}
}
