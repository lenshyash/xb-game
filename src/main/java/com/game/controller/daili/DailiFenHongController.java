package com.game.controller.daili;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.service.AgentBaseConfigService;
import com.game.service.MnyMoneyRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlfh")
public class DailiFenHongController extends BaseDailiController{

	@Autowired
	private MnyMoneyRecordService mnyMoneyService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/dlfh.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String begin = super.$("begin");
		String end = super.$("end");
		Long type = MoneyRecordType.PROXY_REBATE_ADD.getType();
		if(agentBaseConfigService.onoff_multi_agent(StationUtil.getStationId())){
			type= MoneyRecordType.PROXY_MULTI_REBATE_ADD.getType();
		}

		MnyMoneyRecordVo moneyRecordVo = new MnyMoneyRecordVo();
		moneyRecordVo.setAccount(UserUtil.getUserAccount());
		moneyRecordVo.setType(type);
		moneyRecordVo.setBegin(DateUtil.toDatetime(begin));
		moneyRecordVo.setEnd(DateUtil.toDatetime(end));
		moneyRecordVo.setParents(UserUtil.getChildren());
		moneyRecordVo.setSearchSelf(true);
		moneyRecordVo.setSelfId(UserUtil.getUserId());
		super.render(mnyMoneyService.getMoneyRecord(moneyRecordVo));
	}
}
