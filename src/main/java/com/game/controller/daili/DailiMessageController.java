package com.game.controller.daili;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.vo.MessageVo;
import com.game.service.SysMessageService;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlmessage")
public class DailiMessageController extends BaseDailiController {

	@Autowired
	private SysMessageService messageService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/dlmessage.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String title = super.$("title");
		MessageVo mvo = new MessageVo();
		mvo.setAccountId(UserUtil.getUserId());
		mvo.setTitle(title);
		mvo.setStationId(StationUtil.getStationId());
		super.render(messageService.getMessagePage(mvo));
	}

	@RequestMapping("/read")
	@ResponseBody
	public void read() {
		Long userMessageId = super.$long("id");
		messageService.readMessage(userMessageId);
		super.renderSuccess();
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void delete() {
		Long userMessageId = super.$long("id");
		messageService.delUserMessage(userMessageId);
		super.renderSuccess();
	}
}
