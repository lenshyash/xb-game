package com.game.controller.daili;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.util.IPSeeker;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlaccount")
public class DailiAccountController extends BaseDailiController {

	@Autowired
	private SysAccountService accountService;
	@Autowired
	private MnyMoneyService mnyMoneyService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		int code = dealCz();
		if (code > 0) {
			request.setAttribute("isCzkg", true);
		} else {
			request.setAttribute("isCzkg", false);
		}
		Long curId = UserUtil.getUserId();// 当前代理ID
		Long online = super.$long("online");
		Long searchType = super.$long("searchType");
		request.setAttribute("levels", MemberLevelUtil.getStationLevels());
		request.setAttribute("online", online);
		request.setAttribute("agentId", curId);
		request.setAttribute("searchType", searchType);
		request.setAttribute("accountType", super.$long("accountType"));
		request.setAttribute("scoreShowFlag", "on".equals(StationConfigUtil.get(StationConfig.mny_score_show)));
		request.setAttribute("profitShareFlag", StationConfigUtil.get(StationConfig.onoff_profit_share));
		request.setAttribute("memberRateFlag", "on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)));
		request.setAttribute("reviewClose", "on".equals(StationConfigUtil.get(StationConfig.onoff_daili_review_close)));
		return super.goPage("/page/dlaccount.jsp");
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		accountService.updStatus(account);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		String userName = super.$("userName");
		Long agentId = super.$long("agentId");
		Long agentParentId = super.$long("agentParentId");
		Long online = super.$long("online");
		Long accountType = super.$long("accountType");
		Long searchType = super.$long("searchType");
		Long levelGroup = super.$long("levelGroup");
		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setParents(UserUtil.getChildren());
		accountVo.setAgentId(agentId);
		accountVo.setAgentParentId(agentParentId);
		accountVo.setOnline(online);
		accountVo.setAccountType(accountType);
		accountVo.setSearchType(searchType);
		accountVo.setLevelGroup(levelGroup);
		accountVo.setUserName(userName);
		Page page = accountService.getAccountPage4DaiLi(accountVo);
		
		Map accountById = accountService.getAccountById(UserUtil.getUserId(), StationUtil.getStationId());
		String useraccount = (String)accountById.get("account"); //当前用户名
		
		String contactWayFlag = StationConfigUtil.get(StationConfig.onoff_daili_member_user_contact_way_hidden);
		String showAllFlag = StationConfigUtil.get(StationConfig.onoff_daili_member_user_show_all);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<Map> list = page.getList();
			for (Map m : list) {
				if(!"on".equals(showAllFlag)) {
					m.put("cardNo", yincang((String) m.get("cardNo"), 4));
					m.put("userName", yincang((String) m.get("userName"), 1,true));
					m.put("bankAddress", yincang((String) m.get("bankAddress"), 3));
					m.put("bankName", yincang((String) m.get("bankName"), 2));
					if(!"off".equalsIgnoreCase(contactWayFlag)) {
						m.put("phone", yincang((String) m.get("phone"), 4));
						m.put("qq", yincang((String) m.get("qq"), 4));
						m.put("wechat", yincang((String) m.get("wechat"), 4));
						m.put("email", yincang((String) m.get("email"), 4));
					}
				}
				m.remove("receiptPwd");
				String parentNames = (String) m.get("parentNames");
				String accountName = UserUtil.getCurrentUser().getLoginAccount();
				if (StringUtils.contains(parentNames, accountName)) {
					String[] pns = parentNames.split(",");
					parentNames = "";
					boolean begin = false;
					for (String pn : pns) {
						if (StringUtils.isNotEmpty(pn)) {
							if (StringUtils.equals(accountName, pn)) {
								begin = true;
							}
							if (!begin) {
								continue;
							}
							parentNames = parentNames + " > " + pn;
						}
					}
					if (parentNames.length() > 3) {
						parentNames = parentNames.substring(3);
					}
					m.put("parentNames", parentNames);
				} else {
					m.put("parentNames", "");
				}
				m.put("parents", "");
				
				//去掉自己的上级名字
				String agentName = (String)m.get("account");
				if(useraccount.equals(agentName)) {
					m.remove("agentName");
				}
				//ip地址
				IPSeeker ips = IPSeeker.getInstance();
				if (StringUtil.isNotEmpty(m.get("lastLoginIp"))) {
					String ip = (String) m.get("lastLoginIp");
					String ipArea = ips.getIpAddr(ip);
					if(StringUtil.isEmpty(ipArea)){
						ipArea = ips.getCountry((String) m.get("lastLoginIp"));
						if (ipArea.equals("IANA")) {
							ipArea = "";
						}
					}
					m.put("lastLoginIpAddress", ipArea);
				} else {
					m.put("lastLoginIpAddress", "");
				}
			}
		}
		super.render(page);
	}

	@ResponseBody
	@RequestMapping("/save")
	public void save() {
		String account = super.$("account");
		Long accountStatus = super.$long("accountStatus");
		Long type = super.$long("type");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		String rebateNum = super.$("rebateNum");
		String profitShare = super.$("profitShare");
		String dynamicRate = super.$("dynamicRate");
		
		if (type == null) {
			throw new GenericException("用户类型错误");
		}
		if (StringUtils.equals("on",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_multi_agent))) {
			if ((!type.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER) && !type.equals(SysAccount.ACCOUNT_PLATFORM_AGENT))) {
				throw new GenericException("用户类型错误");
			}
		} else {
			SysAccount acc = (SysAccount) SysUtil.getCurrentUser();
			if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)
					&& !type.equals(SysAccount.ACCOUNT_PLATFORM_AGENT)) {// 总代理只能添加代理
				throw new GenericException("用户类型错误");
			}
			if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
					&& !type.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
				throw new GenericException("用户类型错误");
			}
		}

		AccountVo accountVo = new AccountVo();
		accountVo.setAccount(account);
		accountVo.setStatus(accountStatus);
		accountVo.setPwd(pwd);
		accountVo.setRpwd(rpwd);
		accountVo.setAccountType(type);
		accountVo.setAgentId(UserUtil.getUserId());
		accountVo.setRebateNum(StringUtil.toBigDecimal(rebateNum));
		accountVo.setProfitShare(StringUtil.toBigDecimal(profitShare));
		accountVo.setDynamicRate(StringUtil.toBigDecimal(dynamicRate));
		accountService.saveAccount(accountVo);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/multidata")
	public void multidata() {
		Long id = super.$long("id");
		Long limit = super.$long("limit");
		AccountVo avo = new AccountVo();
		avo.setId(id);
		if (StringUtil.equals(limit, 1L)) {
			avo.setGeneralize(true);
		}
		super.renderJson(accountService.getAgentMultiData(avo));
	}

	/**
	 * 代理给下级代理和会员充值(只有直接下级) code为正数代表开启 负数代表关闭
	 */
	public int dealCz() {
		String isCz = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_withdrawals);
		int code = -1;
		if (StringUtil.isNotEmpty(isCz) && isCz.equals("on")) {
			code = 1;
		}
		return code;
	}

	/**
	 * 保存加款信息
	 */
	@ResponseBody
	@RequestMapping("/saveCash")
	public void saveCash() {

		Long accountId = super.$cl("id");
		String money = super.$c("money");
		BigDecimal moneyOld = StringUtil.toBigDecimal(money);
		// 代理信息
		Long userId = UserUtil.getUserId();
		Long stationId = StationUtil.getStationId();
		mnyMoneyService.saveCash(accountId, money, moneyOld, userId, stationId);
		super.renderSuccess();
	}

	/**
	 * 根据会员/代理账号查余额
	 */
	@ResponseBody
	@RequestMapping("/memmny")
	public void list2() {
		String account = super.$("account");
		super.renderJson(mnyMoneyService.getMoneyByAccount(account));
	}

	@ResponseBody
	@RequestMapping("/saveFds")
	public void saveFds() {
		Long id = super.$long("id");
		String rebateNum = super.$("rebateNum");
		String profitShare = super.$("profitShare");
		String dynamicRate = super.$("dynamicRate");
		accountService.updateRebateNum(id, StringUtil.toBigDecimal(rebateNum),StringUtil.toBigDecimal(profitShare),StringUtil.toBigDecimal(dynamicRate));
		super.renderSuccess();
	}

	@RequestMapping("/view")
	public String viewDetail(Long id, Model model) {
		if (id == null || id <= 0) {
			throw new ParameterException("账户不存在");
		}
		Long stationId = StationUtil.getStationId();
		Map map = accountService.getAccountById(id, stationId);
		if (map == null) {
			throw new ParameterException("账户不存在");
		}
		if (!map.containsKey("parentNames")) {
			throw new ParameterException("账户不存在");
		}
		String parentNames = (String) map.get("parentNames");
		String accountName = UserUtil.getCurrentUser().getLoginAccount();
		if (!StringUtils.contains(parentNames, accountName)) {
			throw new ParameterException("账户不存在");
		}
		String[] pns = parentNames.split(",");
		parentNames = "";
		boolean begin = false;
		for (String pn : pns) {
			if (StringUtils.isNotEmpty(pn)) {
				if (StringUtils.equals(accountName, pn)) {
					begin = true;
				}
				if (!begin) {
					continue;
				}
				parentNames = parentNames + " > " + pn;
			}
		}
		parentNames = parentNames.substring(3);
		model.addAttribute("parentNames", parentNames);
		model.addAttribute("map", map);
		map.put("cardNo", yincang((String) map.get("cardNo"), 4));
		map.put("userName", yincang((String) map.get("userName"), 1,true));
		return super.goPage("/page/memberDetail.jsp");
	}

	
	private String yincang(String str,int baoliuHouJiwei) {
		return yincang( str, baoliuHouJiwei,false);
	}
	
	private String yincang(String str, int baoliuHouJiwei,boolean enabledOnoff) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if(enabledOnoff) {
			String yicangOnOff = StationConfigUtil.get(StationConfig.onoff_daili_member_user_name_hidden);
			if(StringUtil.isNotEmpty(yicangOnOff) && "off".equals(yicangOnOff)) {
				return str;
			}
		}
		
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}
}
