package com.game.controller.daili;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.model.vo.ReportParamVo;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dlreport")
public class DailiReportController extends BaseDailiController {

	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index() {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);

		String lottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_lottery_game);
		String real = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		String sport = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_sports_game);
		String markSix = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_liu_he_cai);
		String dianZi = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_zi_you_yi);
		String thirdSports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_chess);

		super.getRequest().setAttribute("lottery", lottery);
		super.getRequest().setAttribute("real", real);
		super.getRequest().setAttribute("sport", sport);
		super.getRequest().setAttribute("markSix", markSix);
		super.getRequest().setAttribute("dianZi", dianZi);
		super.getRequest().setAttribute("thirdSports", thirdSports);
		super.getRequest().setAttribute("thirdLottery", thirdLottery);
		super.getRequest().setAttribute("chess", chess);

		return super.goPage("/page/dlreport.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		String agentName = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long searchType = super.$long("searchType");

		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setSearchType(searchType);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.getTomorrow(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setAgentName(agentName);
		paramVo.setChildren(UserUtil.getChildren());
		paramVo.setSearchSelf(true);
		paramVo.setSelfId(UserUtil.getUserId());
		super.renderJson(dailyMoneyService.getGlobalReport(paramVo));
	}
}
