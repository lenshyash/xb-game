package com.game.controller.daili;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.service.MnyDrawRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Controller
@RequestMapping(SystemConfig.DAILI_CONTROL_PATH + "/dldrawrd")
public class DailiDrawRdController extends BaseDailiController {

	@Autowired
	private MnyDrawRecordService mdrService;

	@RequestMapping("/index")
	public String index() {
		super.getRequest().setAttribute("begin", DateUtil.toDateStr(new Date()));
		return super.goPage("/page/dldrawrd.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String account = super.$("account");
		Long status = super.$long("status");
		Long accountType = super.$long("accountType");
		
		String agentName = super.$("agent");
		Long searchType = super.$long("searchType");
		Long drawTimes = super.$long("drawTimes");
		String begin = super.$("begin");
		String end = super.$("end");

		Date beginDate = DateUtil.toDatetime(begin);
		Date endDate = DateUtil.toDatetime(end);

		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		mdrvo.setAccount(account);
		mdrvo.setStatus(status);
		mdrvo.setBegin(beginDate);
		mdrvo.setEnd(endDate);
		mdrvo.setStationId(StationUtil.getStationId());
		mdrvo.setParents(UserUtil.getChildren());
		mdrvo.setSelfId(UserUtil.getUserId());
		mdrvo.setSearchSelf(true);
		mdrvo.setAccountType(accountType);
		mdrvo.setSearchType(searchType);
		mdrvo.setAgentName(agentName);
		mdrvo.setDrawTimes(drawTimes);
		Page<Map<String, Object>> page = mdrService.getPage(mdrvo);
		List<Map<String, Object>> mnyDrawRecords = page.getList();
		int mnyDrawRecordsSize;
		if (mnyDrawRecords != null && (mnyDrawRecordsSize = mnyDrawRecords.size()) > 0) {
			for (int i = 0; i < mnyDrawRecordsSize; i++) {
				Map<String, Object> mnyDrawRecord = mnyDrawRecords.get(i);
				String cardNo = (String) mnyDrawRecord.get("cardNo");
				String userName = (String) mnyDrawRecord.get("userName");
				mnyDrawRecord.put("cardNo", yincang(cardNo, 4));
				mnyDrawRecord.put("userName", yincang(userName, 1));
			}
		}
		super.render(page);
	}

	private String yincang(String str, int baoliuHouJiwei) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		// for(int i = 0; i < str.length() - baoliuHouJiwei; i++){
		// aaa += "*";
		// }
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}
}
