package com.game.controller.admin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.model.SysStation;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.service.AgentProfitShareCoreService;
import com.game.service.DataConversionService;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.service.SysAccountWarningService;
import com.game.service.SysStationService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/station")
public class DataConversionController extends BaseAdminController {

	@Autowired
	private DataConversionService dataService;
	@Autowired
	private SysStationService stationService;
	@Autowired
	private SysAccountDailyMoneyDao dailyMoneyDao;
	@Autowired
	private MemberBackwaterRecordService backwaterRecordService;
	@Autowired
	private ProxyDailyBettingStatisticsService pbssService;
	@Autowired
	private AgentProfitShareCoreService profitShareCoreService;
	@Autowired
	private MemberBalanceGemRecordService balanceGemRecordService;
	@Autowired
	private SysAccountWarningService accountWarningService;
	
	@RequestMapping("/accountDailyMoney")
	public String index(Model model) {
		model.addAttribute("stationList1", stationService.getAllActive());
		return super.goPage("/page/data.jsp");
	}

	@ResponseBody
	@RequestMapping("/accountDailyMoneyMerge")
	public void accountDailyMoneyMerge(String startDate, String endDate, Long stationId) {
		Long lastTime = (Long) SysUtil.getSession().getAttribute("accountDailyMoneyMerge_time");
		long curTime = System.currentTimeMillis();
		if (lastTime != null && curTime - lastTime < 30000) {
			renderFailure("执行数据合并的2次间隔必须大于30秒");
			return;
		}
		SysUtil.getSession().setAttribute("accountDailyMoneyMerge_time", curTime);
		Date today = DateUtil.dayFirstTime(new Date(), 0);
		Date start = DateUtil.toDate(startDate);
		Date end = DateUtil.toDate(endDate);
		if (end == null || end.getTime() > today.getTime()) {
			end = today;
		}
		Date minDate = dailyMoneyDao.minBizDatetime();
		if (start == null || start.getTime() < minDate.getTime()) {
			start = DateUtil.dayFirstTime(minDate, 0);
		}
		if (end.getTime() < start.getTime()) {
			renderFailure("结束时间必须大于开始时间");
			return;
		}
		long startTimes = System.currentTimeMillis();
		dataService.startMergeDailyData(start, end, stationId);
		long seconds = (System.currentTimeMillis() - startTimes) / 1000;
		renderFailure("数据合并结束～～～～耗时：" + seconds + "秒；");
		SysUtil.getSession().removeAttribute("accountDailyMoneyMerge_time");
	}

	@ResponseBody
	@RequestMapping("/dailyBetStat")
	public void dailyBetStat(String startDate, String endDate, Long stationId) {
		Long lastTime = (Long) SysUtil.getSession().getAttribute("dailyBetStat_time");
		long curTime = System.currentTimeMillis();
		if (lastTime != null && curTime - lastTime < 30000) {
			renderFailure("执行数据合并的2次间隔必须大于30秒");
			return;
		}
		SysUtil.getSession().setAttribute("dailyBetStat_time", curTime);
		Date today = DateUtil.dayFirstTime(new Date(), 0);
		Date start = DateUtil.toDate(startDate);
		Date end = DateUtil.toDate(endDate);
		if (end == null || end.getTime() > today.getTime()) {
			end = today;
		}
		if (end.before(start)) {
			renderJson(MixUtil.newHashMap("msg", "开始时间不能大于结束时间"));
			return;
		}
		if (end.getTime() - start.getTime() > 2678400000L) {// 时间范围大于31天
			renderJson(MixUtil.newHashMap("msg", "2个时间范围不能大于31天"));
			return;
		}

		List<Long> sIdList = null;
		if (stationId != null) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent), "on")
					&& StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_platform_backwater), "off")) {
				renderJson(MixUtil.newHashMap("msg", "您的站点使用多极代理或者未开启本平台反水开关，不需要反点"));
				return;
			}
			sIdList = new ArrayList<>();
			sIdList.add(stationId);
		} else {
			sIdList = getStationIdList();
		}
		if (sIdList == null) {
			renderJson(MixUtil.newHashMap("msg", "站点为空"));
			return;
		}

		long startTimes = System.currentTimeMillis();
		statForMember(start,end,sIdList);
		pbssService.collectDailyAmount2(start, end, sIdList);
		long seconds = (System.currentTimeMillis() - startTimes) / 1000;
		renderFailure("数据合并结束～～～～耗时：" + seconds + "秒；");
		SysUtil.getSession().removeAttribute("dailyBetStat_time");
	}
	
	@ResponseBody
	@RequestMapping("/agentProfitStat")
	public void agentProfitStat(String startDate, String endDate, Long stationId) {
		Long lastTime = (Long) SysUtil.getSession().getAttribute("agentProfitStat_time");
		long curTime = System.currentTimeMillis();
		if (lastTime != null && curTime - lastTime < 30000) {
			renderFailure("执行数据合并的2次间隔必须大于30秒");
			return;
		}
		SysUtil.getSession().setAttribute("agentProfitStat_time", curTime);
		Date today = DateUtil.dayFirstTime(new Date(), 0);
		Date start = DateUtil.toDate(startDate);
		Date end = DateUtil.toDate(endDate);
		if (end == null || end.getTime() > today.getTime()) {
			end = today;
		}
		if (end.before(start)) {
			renderJson(MixUtil.newHashMap("msg", "开始时间不能大于结束时间"));
			return;
		}
		if (end.getTime() - start.getTime() > 2678400000L) {// 时间范围大于31天
			renderJson(MixUtil.newHashMap("msg", "2个时间范围不能大于31天"));
			return;
		}

		List<Long> sIdList = null;
		if (stationId != null) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(!StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_profit_share), "on")) {
				renderJson(MixUtil.newHashMap("msg", "您的站点使用未开启占成"));
				return;
			}
			sIdList = new ArrayList<>();
			sIdList.add(stationId);
		} else {
			sIdList = getStationIdList();
		}
		if (sIdList == null) {
			renderJson(MixUtil.newHashMap("msg", "站点为空"));
			return;
		}

		long startTimes = System.currentTimeMillis();
		for (Long long1 : sIdList) {
			profitShareCoreService.collectAgentProfitShareRecord(start, end, long1);
		}
		long seconds = (System.currentTimeMillis() - startTimes) / 1000;
		renderFailure("数据合并结束～～～～耗时：" + seconds + "秒；");
		SysUtil.getSession().removeAttribute("agentProfitStat_time");
	}

	@ResponseBody
	@RequestMapping("/balanceGemProfitStat")
	public void balanceGemProfitStat(String startDate, String endDate, Long stationId) {
		Long lastTime = (Long) SysUtil.getSession().getAttribute("agentProfitStat_time");
		long curTime = System.currentTimeMillis();
		if (lastTime != null && curTime - lastTime < 30000) {
			renderFailure("执行数据合并的2次间隔必须大于30秒");
			return;
		}
		SysUtil.getSession().setAttribute("agentProfitStat_time", curTime);
		Date today = DateUtil.dayFirstTime(new Date(), 0);
		Date start = DateUtil.toDate(startDate);
		Date end = DateUtil.toDatetime(endDate);
//		if (end == null || end.getTime() > today.getTime()) {
//			end = today;
//		}
		if (end.before(start)) {
			renderJson(MixUtil.newHashMap("msg", "开始时间不能大于结束时间"));
			return;
		}
		if (end.getTime() - start.getTime() > 2678400000L) {// 时间范围大于31天
			renderJson(MixUtil.newHashMap("msg", "2个时间范围不能大于31天"));
			return;
		}

		List<Long> sIdList = null;
		if (stationId != null) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(!StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem), "on")
					&&!StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem), "ban")) {
				renderJson(MixUtil.newHashMap("msg", "站点余额宝功能未开启"));
				return;
			}
			sIdList = new ArrayList<>();
			sIdList.add(stationId);
		} else {
			sIdList = getStationIdList();
		}
		if (sIdList == null) {
			renderJson(MixUtil.newHashMap("msg", "站点为空"));
			return;
		}

		long startTimes = System.currentTimeMillis();
		for (Long long1 : sIdList) {
			balanceGemRecordService.collectBalanceGemRecord(start, end, stationId);
		}
		long seconds = (System.currentTimeMillis() - startTimes) / 1000;
		renderFailure("数据合并结束～～～～耗时：" + seconds + "秒；");
		SysUtil.getSession().removeAttribute("agentProfitStat_time");
	}
	
	@ResponseBody
	@RequestMapping("/userWarning")
	public void userWarning(Long stationId) {

		if (stationId != null) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.on_off_user_warning), "off")) {
				renderJson(MixUtil.newHashMap("msg", "用户预警功能未开启"));
				return;
			}
		} 
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, -7);
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Date start = now.getTime();
		long startTimes = System.currentTimeMillis();
		accountWarningService.executeUserWarning(start, stationId);
		long seconds = (System.currentTimeMillis() - startTimes) / 1000;
		renderFailure("数据合并结束～～～～耗时：" + seconds + "秒；");
	}
	
	
	
	
	private void statForMember(Date start1,Date end1,List<Long> sIdList) {
		List<MemberBackwaterRecord> all = new ArrayList<>();
		List<MemberBackwaterRecord> list = null;
		try {
			list = backwaterRecordService.collectLotteryDayBetMoney(start1, end1, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总彩票投注金额发生错误");
		}
		// 汇总昨天派奖的所有投注信息
		try {
			list = backwaterRecordService.collectMarkSixDayBetMoney(start1, end1, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总六合投注金额发生错误");
		}
		try {
			list = backwaterRecordService.collectSportDayBetMoney(start1, end1, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总体育投注金额发生错误");
		}
		
		try {
			list = backwaterRecordService.collectSysLotteryDayBetMoney(start1, end1, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总系统彩票投注金额发生错误");
		}
		backwaterRecordService.saveAll(all);
	}

	/**
	 * 获取需要反水的站点id
	 * 
	 * @return
	 */
	private List<Long> getStationIdList() {
		List<SysStation> stationList = stationService.getAllActive();
		if (stationList == null || stationList.isEmpty()) {
			logger.info("站点列表为空，不需要计算反水");
			return null;
		}
		List<Long> sIdList = new ArrayList<>();
		for (SysStation s : stationList) {
			if (StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_multi_agent), "on")
					&& !StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_platform_backwater), "on")) {
				// 多极代理不需要反水
				continue;
			}
			sIdList.add(s.getId());
		}
		if (sIdList.isEmpty()) {
			logger.info("需要反水的站点为空，不需要计算反水");
			return null;
		}
		return sIdList;
	}
}
