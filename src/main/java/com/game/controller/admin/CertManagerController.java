package com.game.controller.admin;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.game.cert.CertUtil;
import com.game.cert.CertVo;
import com.game.core.SystemConfig;
import com.game.service.SysCertificateService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/cert")
public class CertManagerController extends BaseAdminController {
	@Autowired
	private SysCertificateService certService;

	@RequestMapping("/index")
	public String index(Model model) {
		return super.goPage("/page/cert/certList.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		File file = new File("/usr/local/nginx/conf/");
		File[] subFile = file.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.matches("server[\\d]+\\.key");
			}
		});
		if (subFile != null && subFile.length > 0) {
			List<CertVo> list = new ArrayList<>();
			CertVo c = null;
			String name = null;
			for (File f : subFile) {
				c = new CertVo();
				name = f.getName();
				c.setId(NumberUtils.toInt(name.substring(6, name.indexOf("."))));
				c.setKeyFile(name);
				f=new File(file, "server" + c.getId() + ".crt");
				if (f.exists()) {
					c.setFileName("server" + c.getId() + ".crt");
					c.setName(CertUtil.getName(f));
				}
				if (new File(file, "server" + c.getId() + ".csr").exists()) {
					c.setCsrFile("server" + c.getId() + ".csr");
				}
				list.add(c);
			}
			renderJson(MixUtil.newHashMap("data", list));
		}
	}

	@RequestMapping("/viewCsr")
	@ResponseBody
	public void viewCsr(Integer id) {
		if (id == null || id < 1) {
			renderText("请选择证书");
			return;
		}
		File file = new File("/usr/local/nginx/conf/server" + id + ".csr");
		if (file.exists()) {
			renderText(CertUtil.readFileContent(file));
		} else {
			renderText("csr文件不存在");
		}
	}

	@RequestMapping(value = "/upload")
	@ResponseBody
	public void upload(@RequestParam("crt1") MultipartFile crt1, @RequestParam("crt2") MultipartFile crt2,
			@RequestParam("keyFile") MultipartFile keyFile, @RequestParam("csrFile") MultipartFile csrFile,
			Integer certId) {
		String msg=certService.uploadCertificate(crt1, crt2, keyFile, csrFile, certId);
		renderJson(MixUtil.newHashMap("success", true, "msg", msg));
	}

}
