package com.game.controller.admin;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.core.SystemConfig;
import com.game.service.SysCertificateService;
import com.game.service.SysStationService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/certDomain")
public class CertDomainManagerController extends BaseAdminController {
	@Autowired
	private SysStationService stationService;
	@Autowired
	private SysCertificateService certService;

	@RequestMapping("/index")
	public String index(Model model) {
		super.getRequest().setAttribute("stations", stationService.getAllActive());
		return super.goPage("/page/cert/certDomainList.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		renderJson(certService.getDomains());
	}

	@RequestMapping("/save")
	@ResponseBody
	public void save(String data) {
		String msg=certService.saveConfig(data);
		renderJson(MixUtil.newHashMap("success", true, "msg", msg));
	}

}
