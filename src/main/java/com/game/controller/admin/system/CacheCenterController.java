package com.game.controller.admin.system;

import java.util.Arrays;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.cache.redis.RedisCallback;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.SysCacheService;

import redis.clients.jedis.Jedis;

/**
 * 缓存中心
 * 
 * @author admin
 *
 */
@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/cache")
public class CacheCenterController extends BaseAdminController {

	@Autowired
	private SysCacheService cahceService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/system/cache.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		super.render(cahceService.getPage());
	}

	/**
	 * 获取缓存
	 */
	@RequestMapping("/getCacheList")
	@ResponseBody
	public void getCacheList(Integer db) {
		if (db == null || db > 16 || db < 0) {
			super.renderFailure("请选中要查看的库");
			return;
		}
		super.renderJson(RedisAPI.getCacheList("", db));
	}

	/**
	 * 刷新缓存
	 */
	@RequestMapping("/flushCache")
	@ResponseBody
	public void flushCache(final Integer db) {
		if (db == null || db > 16 || db < 0) {
			super.renderFailure("请选中要刷新的库");
			return;
		}
		RedisAPI.operateCache(new RedisCallback() {

			@Override
			public void execute(Jedis jedis) {
				jedis.select(db);
				jedis.flushDB();
			}
		});
		super.renderSuccess();
	}

	/**
	 * 删除一条或多条缓存记录
	 */
	@RequestMapping("/delCache")
	@ResponseBody
	public void delCache(String key, Integer db) {
		if (db == null || db > 16 || db < 0 || StringUtils.isEmpty(key)) {
			super.renderFailure("请选中要删除的库和缓存key");
			return;
		}
		Set<String> set = RedisAPI.getCacheList(key, db);
		if (set == null || set.isEmpty()) {
			super.renderFailure("库＝" + db + "  key=" + key + " 缓存不存在");
			return;
		}
		String[] keys = new String[set.size()];
		keys = set.toArray(keys);
		RedisAPI.delCache(db, keys);
		super.renderJson(MixUtil.newHashMap("success", true, "msg", "总共清理缓存数：" + set.size()));
	}
	/**
	 * 移除指定keySet中的value
	 */
	@RequestMapping("/delCacheValue")
	@ResponseBody
	public void delCache(String key,String value, Integer db) {
		if (db == null || db > 16 || db < 0 || StringUtils.isEmpty(key)) {
			super.renderFailure("请选中要删除的库和缓存key");
			return;
		}
		
		Set<String> set = RedisAPI.smembers(key, db);
		if (set == null || set.isEmpty()) {
			super.renderFailure("库＝" + db + "  key=" + key + " 缓存不存在");
			return;
		}
		String[] keys = new String[set.size()];
		keys = set.toArray(keys);
		RedisAPI.srem(key,db,value);
		super.renderJson(MixUtil.newHashMap("success", true, "msg", "当前所有值"+Arrays.toString(keys)));
	}
	@RequestMapping("/getCacheContext")
	@ResponseBody
	public void getCacheContext(String key, Integer db) {
		super.renderJson(MixUtil.newHashMap("content", RedisAPI.getCache(key, db)));
	}
}
