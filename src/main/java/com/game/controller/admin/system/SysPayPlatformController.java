package com.game.controller.admin.system;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysPayPlatform;
import com.game.model.vo.PayPlatformVo;
import com.game.service.SysPayPlatformService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/payplatform")
public class SysPayPlatformController extends BaseAdminController {

	@Autowired
	private SysPayPlatformService payPlatformService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		return super.goPage("/page/system/payplatform.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String payName = super.$("payName");
		Long type = super.$long("type");
		PayPlatformVo ppvo = new PayPlatformVo();
		ppvo.setName(payName);
		ppvo.setType(type);
		
		super.render(payPlatformService.getPage(ppvo));
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(SysPayPlatform payPlatform) {

		payPlatformService.savePayPlatform(payPlatform);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updstatus")
	@ResponseBody
	public void updstatus() {
		Long ppId = super.$long("id");
		Long status = super.$long("status");
		payPlatformService.updStatus(ppId, status);
		super.renderSuccess();
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete() {
		Long ppId = super.$long("id");
		payPlatformService.delPayPlatform(ppId);
		super.renderSuccess();
	}
}
