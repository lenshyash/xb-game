package com.game.controller.admin.system;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.AgentBaseConfigService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/setting")
public class AdminSettingController extends BaseAdminController {

	@Autowired
	AgentBaseConfigService agentBaseConfigService;

	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		return super.goPage("/page/system/setting.jsp");
	}

	@ResponseBody
	@RequestMapping("/settings")
	public void list() {
		super.renderJson(MixUtil.newHashMap("confs", agentBaseConfigService.getSysConfigs()));
	}

	@ResponseBody
	@RequestMapping("/saveval")
	public void save(Long pk,String value ) {
		agentBaseConfigService.saveSysConfigVal(pk,value);
		super.renderSuccess();
	}
}
