package com.game.controller.admin.platform;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SecCard;
import com.game.model.SysAccount;
import com.game.service.SecCardService;
import com.game.util.SecurityCardUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/security")
public class SecurityController extends BaseAdminController {
	
	@Autowired
	private SecCardService cardService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		return super.goPage("/page/platform/card.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(){
		super.render(cardService.getPage());
	}
	
	@RequestMapping("/getCard")
	@ResponseBody
	public void getCard() throws IOException {
		long id = $cl("id","非法请求");
		SecCard card = cardService.getCard(id);
		if(card == null){
			throw new GenericException("密保卡不存在");
		}
		SecurityCardUtil.responseCardImage(card);
	}
	
	@RequestMapping("/createCard")
	@ResponseBody
	public void createCard() {
		SecCard card = cardService.createCard();
		super.renderSuccess();
	}
	
	@RequestMapping("/bind")
	@ResponseBody
	public void bind(){
		String cardNo = $c("cardNo","请输入卡号");
		long userId = $cl("userId","非法请求");
		cardService.bind(cardNo, userId,SecCard.STATION_TYPE_AGENT);
		super.renderSuccess();
	}
	
	@RequestMapping("/unbind")
	@ResponseBody
	public void unbind(){
		long cardId = $cl("cardId","非法请求");
		long userId = $cl("userId","非法请求");
		cardService.unbind(cardId, userId, SecCard.STATION_TYPE_AGENT);
		super.renderSuccess();
	}
	
	@RequestMapping("/getAgentUsers")
	@ResponseBody
	public void getAgentUsers(){
		long stationId = $cl("stationId","非法请求");
		List accounts = cardService.getAgentUsers(stationId);
		super.renderJson(MixUtil.newHashMap("data",accounts));
		//super.renderSuccess();
	}
	
	@RequestMapping("/getBindCards")
	@ResponseBody
	public void getBindCards(){
		long userId = $cl("userId","非法请求");
		List<SecCard> cards = cardService.getBindCards(userId);
		super.renderJson(MixUtil.newHashMap("data",cards));
	}
}
