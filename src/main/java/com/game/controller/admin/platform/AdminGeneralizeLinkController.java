package com.game.controller.admin.platform;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.GeneralizeLinkService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/generalizeLink")
public class AdminGeneralizeLinkController extends BaseAdminController{
	
	@Autowired
	private GeneralizeLinkService generalizeLinkService;
	
	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/generalizeLink.jsp");
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(Long stationId,HttpServletRequest request,Integer type){
		super.render(generalizeLinkService.page(stationId,request,null,type,null));
	}
	
	@RequestMapping("/updateStatusById")
	@ResponseBody
	public void updateStatusById(Long id , Integer status,Long stationId){
		generalizeLinkService.updateStatusById(id, status, stationId);
		super.renderSuccess();
	}
}
