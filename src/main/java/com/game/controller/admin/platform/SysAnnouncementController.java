package com.game.controller.admin.platform;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysAnnouncement;
import com.game.model.vo.AnnouncementVo;
import com.game.service.SysAnnouncementService;
import com.game.service.SysStationService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/announcement")
public class SysAnnouncementController extends BaseAdminController {

	@Autowired
	private SysAnnouncementService sysAnnouncementService;
	@Autowired
	private SysStationService stationService;

	@RequestMapping("/index")
	public String index() {
		super.getRequest().setAttribute("stations", stationService.getAllActive());
		return super.goPage("/page/platform/announcement.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(AnnouncementVo ancVo) {
		super.render(sysAnnouncementService.getPage(ancVo));
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(SysAnnouncement announcement, @RequestParam(value = "stationIds[]", required = false, defaultValue = "") Long[] stationIds) {
		if (StringUtils.isEmpty(announcement.getContent())) {
			throw new GenericException("请输入公告内容");
		}
		if (announcement.getType() == SysAnnouncement.TYPE_SOME_STATION && (stationIds == null || stationIds.length == 0)) {
			throw new GenericException("请选中站点");
		}
		if (announcement.getEndDatetime() == null) {
			throw new GenericException("请选中结束时间");
		}
		if (announcement.getEndDatetime().compareTo(new Date()) <= 0) {
			throw new GenericException("结束时间必须大于当前时间");
		}
		sysAnnouncementService.saveAnnouncement(announcement, stationIds);
		super.renderSuccess();
	}
	@RequestMapping(value = "/getStationIds")
	@ResponseBody
	public void getStationIds(Long id) {
		super.isNotNull(id, "不存在此公告!");
		renderJson(sysAnnouncementService.getStationIds(id));
	}
	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete(Long id) {
		super.isNotNull(id, "不存在此公告!");
		sysAnnouncementService.deleteAnc(id);
		super.renderSuccess();
	}
}
