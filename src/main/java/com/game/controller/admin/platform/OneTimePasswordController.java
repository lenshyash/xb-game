package com.game.controller.admin.platform;

import java.util.Map;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.AgentLoginOtp;
import com.game.service.AgentLoginOtpService;
import com.game.service.SysStationService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/otpConfig")
public class OneTimePasswordController extends BaseAdminController {

	@Autowired
	private SysStationService stationService;
	@Autowired
	private AgentLoginOtpService loginOtpService;

	@RequestMapping("/index")
	public String index(Map<String, Object> map) {
		map.put("stations", stationService.getStationCombo(null));
		return super.goPage("/page/platform/otpConfig.jsp");
	}

	@ResponseBody
	@RequestMapping("/list")
	public void list(Long stationId, String iccid) {
		super.render(loginOtpService.getPage(stationId, iccid));
	}

	@ResponseBody
	@RequestMapping("/escapeList")
	public void escapeList(Long stationId, String account) {
		super.render(loginOtpService.getEscapePage(stationId, account));
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(AgentLoginOtp otp) {
		loginOtpService.save(otp);
		super.renderSuccess();
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete(Long id) {
		loginOtpService.delete(id);
		super.renderSuccess();
	}

	@RequestMapping(value = "/getAgentUsers")
	@ResponseBody
	public void getAgentUsers(Long stationId) {
		if (!loginOtpService.existOtp(stationId)) {
			renderFailure("该站点没有绑定动态口令卡");
			return;
		}
		super.renderJson(MixUtil.newHashMap("data", loginOtpService.getAgentUsers(stationId)));
	}

	@RequestMapping(value = "/changeEscapeStatus")
	@ResponseBody
	public void changeEscapeStatus(Long stationId, String account, Boolean esc) {
		loginOtpService.changeEscapeStatus(stationId, account, esc);
		super.renderSuccess();
	}
}
