package com.game.controller.admin.platform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.MixUtil;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.SysAccount;
import com.game.model.SysStationDomain;
import com.game.model.vo.AccountVo;
import com.game.service.ChatService;
import com.game.service.SysAccountService;
import com.game.service.SysStationDomainService;
import com.game.service.SysStationService;
import com.game.util.AuthTokenUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/chat")
public class ChatController extends BaseAdminController {
	@Autowired
	private SysStationService stationService;
	
	@Autowired
	private SysAccountService sysAccountService;

	@Autowired
	private SysStationDomainService domainService;
	
	@Autowired
	private ChatService chatService;
	
	@RequestMapping("/index")
	public String index(Model model) {
		model.addAttribute("stationList", stationService.getAllActive());
		return super.goPage("/page/platform/chat.jsp");
	}

	@RequestMapping("/stagentmulti")
	@ResponseBody
	public void stagentmulti() {
		Long stationId = super.$long("stationId");
		super.renderJson(
				MixUtil.newHashMap("agentMulti", StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent)));
	}
	@RequestMapping("/folderlist")
	@ResponseBody
	public void floderlist() {
		Long stationId = super.$long("stationId");
		String onflag = StationConfigUtil.get(stationId,StationConfig.onoff_chat_multiple_template);
		Map<String, Object> result = new HashMap<String, Object>();
		List<SysStationDomain> list = new ArrayList<SysStationDomain>();
		result.put("success", true);
		if(onflag!=null && "on".equals(onflag)){
			list =  domainService.getFolderListByStation(stationId);
	        result.put("folderList", list);
		}
		List<SysStationDomain> dlist = new ArrayList<SysStationDomain>();
		dlist =  domainService.getListByStationType(stationId,SysStationDomain.TYPE_CHAT);
		result.put("domainList", dlist);
		super.renderJson(result);
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public void list(String startTime, String endTime) {
		String json = new RequestProxy() {

			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};
			public List<NameValuePair> getParameters() {
				List<NameValuePair> params = getBasicParams();
				if (params == null) {
					return params;
				}
				return params;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/api/system/getServerAgents?",PostType.POST, true);
//		}.doRequest("http://chat666.com:8080/chat/api/system/getServerAgents?", PostType.POST, true);
		JSONObject jsonObj = JSONObject.parseObject(json);
		JSONObject data = jsonObj.getJSONObject("data");
		Map<String,Object> page = new HashMap<>();
		page.put("rows", data.get("content"));
		page.put("total", data.getInteger("totalElements"));
		super.renderJson(page);
	}
	public List<NameValuePair> getBasicParams() {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(super.getRequest());
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("authToken", StationConfigUtil.getSys(StationConfig.sys_real_center_token)));
		params.add(new BasicNameValuePair("page", webParam.getPageNo() + ""));
		params.add(new BasicNameValuePair("quantity", webParam.getPageSize() + ""));
		params.add(new BasicNameValuePair("stationId", super.$("stationId")));
		params.add(new BasicNameValuePair("platformId", super.$("platformId")));
		return params;
	}
	
	
	
	/*
	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String platformId = super.$("platformId");
		Long stationId = super.$long("stationId");
//		super.render(sysAccountService.getPage(avo));
	}
*/
	@RequestMapping(value = "/save")
	@ResponseBody
	public void save() {
		Long id = super.$long("stationId");
		String platformNo = super.$("platformNo");
		String domain = super.$("domain");
		if (id == null || StringUtils.isEmpty(domain)) {
			throw new ParameterException("必填参数不能为空");
		}
		
		Map<String,String> map = chatService.generateChatRoom(id,platformNo,domain);
		super.renderJson(map);
	}

	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		sysAccountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		sysAccountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		sysAccountService.updStatus(account);
		super.renderSuccess();
	}
	
	@RequestMapping("/view")
	public String viewDetail(Long id, Long stationId,Model model) {
		if (id == null || id <= 0) {
			throw new ParameterException("账户不存在");
		}
		Map map = sysAccountService.getAccountById(id, stationId);
		if (map == null) {
			throw new ParameterException("账户不存在");
		}
		if (map.containsKey("parentNames")) {
			String parentNames = (String) map.get("parentNames");
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				model.addAttribute("parentNames", parentNames);
			}
		}
		map.put("cardNo",yincang((String)map.get("cardNo"),4));
		map.put("userName",yincang((String)map.get("userName"),1));
		model.addAttribute("map", map);
		return super.goPage("/page/platform/accountDetail.jsp");
	}
	
	private String yincang(String str, int baoliuHouJiwei) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}
}
