package com.game.controller.admin.platform;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigGroup;
import com.game.service.AgentBaseConfigService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/agentconfig")
public class AgentConfigController extends BaseAdminController {

	@Autowired
	private AgentBaseConfigService configService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/agentconfig.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(String name, Long platform, String groupName, Long groupId) {
		super.render(configService.getPageConfig(name, platform, groupName, groupId));
	}

	@RequestMapping("/gplst")
	@ResponseBody
	public void gplst(Long platform, String groupName) {
		super.render(configService.getPageConfigGroup(platform, groupName));
	}

	@RequestMapping("/gpcombo")
	@ResponseBody
	public void gpcombo(Long platform) {
		super.renderJson(configService.getConfigGroupLst(platform));
	}

	@RequestMapping("/savegp")
	@ResponseBody
	public void savegp(AgentBaseConfigGroup cg) {
		configService.saveConfigGroup(cg);
		super.renderSuccess();
	}

	@RequestMapping(value = "/delgp")
	@ResponseBody
	public void delgp() {
		Long confId = super.$long("id");
		super.isNotNull(confId, "不存在此配置组别!");
		configService.delGroup(confId);
		super.renderSuccess();
	}

	@RequestMapping(value = "/stationset")
	@ResponseBody
	public void stationset() {
		Long stationId = super.$long("stationId");
		Map<String, Object> data = new HashMap<>();
		data.put("gps", configService.getConfigGroupLst(AgentBaseConfig.PLATFORM_AGENT));
		data.put("confs", configService.getConfigAll());
		data.put("pm", configService.getConfValsByStationId(stationId));
		super.renderJson(data);
	}

	@RequestMapping(value = "/stationvals")
	@ResponseBody
	public void stationvals(Long stationId) {
		Map<String, Object> data = new HashMap<>();
		data.put("confs", configService.getStationConfigs(stationId));
		super.renderJson(data);
	}

	@ResponseBody
	@RequestMapping("/savestval")
	public void savestval(Long pk,String value) {
		configService.saveConfigValue(pk,value,null);
		super.renderSuccess();
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(AgentBaseConfig config) {
		configService.saveConfig(config);
		super.renderSuccess();
	}

	@RequestMapping(value = "/saveset")
	@ResponseBody
	public void saveset(Long stationId,String ids) {
		configService.saveAgentConfVals(stationId, ids);
		super.renderSuccess();
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete(Long id) {
		configService.delete(id);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updStatus")
	@ResponseBody
	public void updStatus(	Long id,Long status) {
		super.isNotNull(id, "不存在此配置!");
		configService.updStatus(id, status);
		super.renderSuccess();
	}
	
	@RequestMapping(value = "/updateRealMoneyConfig")
	@ResponseBody
	public void updateRealMoneyConfig() {
		configService.updateRealMoneyConfig();
		super.renderSuccess();
	}
}
