package com.game.controller.admin.platform;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysStationDomain;
import com.game.model.vo.StationVo;
import com.game.service.SysStationDomainService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/domain")
public class SysStationDomainController extends BaseAdminController {

	@Autowired
	private SysStationDomainService sysDomainService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/domain.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list(Long stationId, String domain, String agentName) {
		StationVo svo = new StationVo();
		svo.setDomain(StringUtils.lowerCase(StringUtils.trim(domain)));
		svo.setId(stationId);
		svo.setAgentName(StringUtils.lowerCase(StringUtils.trim(agentName)));
		super.render(sysDomainService.getPage(svo));
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save(SysStationDomain domain) {
		super.isNotNull(domain.getStationId(), "站点不能为空!");
		super.isNotNull(domain.getDomain(), "域名不能为空!");
		super.isNotNull(domain.getStatus(), "状态不能为空!");
		String domainNames = StringUtils.lowerCase(StringUtils.trim(domain.getDomain()));
		if (domain.getId() != null) {
			if (StringUtils.containsAny(domainNames, '\n', ',')) {
				throw new GenericException("编辑时，只能存在一个域名");
			}
			sysDomainService.saveDomain(domain);
		} else {
			String[] ds = domainNames.split("\n");
			StringBuilder info = new StringBuilder();
			String[] dif = null;
			String defaultAgent=domain.getAgentName();
			for (String dom : ds) {
				try {
					dom = StringUtils.trim(dom);
					if (StringUtils.isNotEmpty(dom)) {
						dom = StringUtils.replacePattern(dom, " +", " ");
						dif = dom.split(",| ");
						if (StringUtils.isNotEmpty(dif[0].trim())) {
							domain.setId(null);
							domain.setDomain(dif[0].trim());
							if(dif.length>1 && StringUtils.isNotEmpty(dif[1].trim())){
								domain.setAgentName(dif[1].trim());
							}else{
								domain.setAgentName(defaultAgent);
							}
							sysDomainService.saveDomain(domain);
						}
					}
				} catch (Exception e) {
					info.append(e.getMessage()).append("<br>");
				}
			}
			if (info.length() > 0) {
				renderFailure(info.toString());
				return;
			}
		}
		super.renderSuccess();
	}

	@RequestMapping(value = "/open")
	@ResponseBody
	public void open() {

		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updStatus(domainId, SysStationDomain.STATUS_ENABLE);
		super.renderSuccess();
	}

	@RequestMapping(value = "/close")
	@ResponseBody
	public void close() {
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updStatus(domainId, SysStationDomain.STATUS_DISABLE);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updateIsRegSwitch")
	@ResponseBody
	public void updateIsRegSwitch(Long isRegSwitch) {
		if (isRegSwitch == 2) {
			isRegSwitch = SysStationDomain.STATUS_DISABLE;
		} else {
			isRegSwitch = SysStationDomain.STATUS_ENABLE;
		}
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updateIsRegSwitch(domainId, isRegSwitch);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updateIsXlSwitch")
	@ResponseBody
	public void updateIsXlSwitch(Long isXlSwitch) {
		if (isXlSwitch == 2) {
			isXlSwitch = SysStationDomain.STATUS_DISABLE;
		} else {
			isXlSwitch = SysStationDomain.STATUS_ENABLE;
		}
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updateIsXlSwitch(domainId, isXlSwitch);
		super.renderSuccess();
	}
	@RequestMapping(value = "/updateIsChatSwitch")
	@ResponseBody
	public void updateIsChatSwitch(Long isChatSwitch) {
		if (isChatSwitch == 2) {
			isChatSwitch = SysStationDomain.STATUS_DISABLE;
		} else {
			isChatSwitch = SysStationDomain.STATUS_ENABLE;
		}
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updateIsChatSwitch(domainId, isChatSwitch);
		super.renderSuccess();
	}
	@RequestMapping(value = "/updateInterfaceLimit")
	@ResponseBody
	public void updateInterfaceLimit(Long interfaceLimit) {
		if (interfaceLimit == 2) {
			interfaceLimit = SysStationDomain.STATUS_DISABLE;
		} else {
			interfaceLimit = SysStationDomain.STATUS_ENABLE;
		}
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.updateInterfaceLimit(domainId, interfaceLimit);
		super.renderSuccess();
	}
	@RequestMapping(value = "/delete")
	@ResponseBody
	public void delete() {
		Long domainId = super.$long("domainId");
		super.isNotNull(domainId, "不存在此域名!");
		sysDomainService.deleteDomain(domainId);
		super.renderSuccess();
	}
}
