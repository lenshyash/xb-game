package com.game.controller.admin.platform;

import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.AgentLotteryQuoto;
import com.game.model.SysStation;
import com.game.model.vo.StationVo;
import com.game.permission.annotation.CheckType;
import com.game.permission.annotation.Permission;
import com.game.service.AgentLotteryQuotoService;
import com.game.service.SysStationService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/agentLotteryQuoto")
public class AgentLotteryQuotoController extends BaseAdminController {
	@Autowired
	private SysStationService sysStationService;
		
	@Autowired
	private AgentLotteryQuotoService agentLotteryQuotoService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/quoto.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		super.render(agentLotteryQuotoService.getPage());
	}
}
