package com.game.controller.admin.platform;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.service.ProxyMultiRebateRecordService;


@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH+"/pmrr")
public class AdminProxyMultiRebateRecordController extends BaseAdminController {
	
	@Autowired
	private ProxyMultiRebateRecordService proxyMultiRebateRecordService;
	
	@RequestMapping("/index")
	public String index(){
		return super.goPage("/page/platform/proxyMultiRebateRecord.jsp");
	}
	@RequestMapping("/list")
	@ResponseBody
	public void list(ProxyMultiRebateRecord proxyMultiRebateRecord,String startTime,String endTime){
		Page<ProxyMultiRebateRecord> page = proxyMultiRebateRecordService.getPageConfig(proxyMultiRebateRecord, startTime, endTime);
		 super.render(page);
	}
	
	/**
	 * 返点回滚提交
	 * @return
	 */
	@RequestMapping(value="/cancel",produces={"application/json;charset=UTF-8"})
	@ResponseBody
	public String cancle(Long betId,Long stationId){
		int i = proxyMultiRebateRecordService.cancleProxyMultiRebate(betId,stationId);
		JSONObject result = new JSONObject();
		if(i==1){
			result.put("code", 0);
			result.put("msg", "返点回滚提交成功");
		}else{
			result.put("code", -1);
			result.put("msg", "返点回滚提交失败");
		}
		return result.toJSONString();
	}
}
