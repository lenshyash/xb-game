package com.game.controller.admin.platform;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.AgentMenuService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/agentpermission")
public class AgentPermissionController extends BaseAdminController {

	@Autowired
	private AgentMenuService menuService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/permission.jsp");
	}

	@RequestMapping("/getPermission")
	@ResponseBody
	public void getGroupPermission(Long stationId) {
		Map<String, Object> data = new HashMap<>();
		data.put("menus", menuService.getAllLevelThirdMenu());
		data.put("pm", menuService.getAgentPermission(0l,stationId));
		super.renderJson(data);
	}

	@RequestMapping("/savePermission")
	@ResponseBody
	public void savePermission(Long stationId, String ids) {
		menuService.saveAgentPermission(0l, stationId, ids);
		super.renderSuccess();
	}
}
