package com.game.controller.admin.platform;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.StationConfig;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.service.SysAccountService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/account")
public class SysAccountController extends BaseAdminController {

	@Autowired
	private SysAccountService sysAccountService;

	@RequestMapping("/index")
	public String index() {
		return super.goPage("/page/platform/account.jsp");
	}

	@RequestMapping("/stagentmulti")
	@ResponseBody
	public void stagentmulti() {
		Long stationId = super.$long("stationId");
		super.renderJson(
				MixUtil.newHashMap("agentMulti", StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent)));
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String account = super.$("account");
		Long stationId = super.$long("stationId");
		Long status = super.$long("accountStatus");
		Long accountType = super.$long("accountType");
		AccountVo avo = new AccountVo();
		avo.setStationId(stationId);
		avo.setAccountType(accountType);
		avo.setStatus(status);
		avo.setAccount(account);
		super.render(sysAccountService.getPage(avo));
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public void save() {

		Long id = super.$long("id");
		String account = super.$("account");
		Long accountType = super.$long("accountType");
		Long accountStatus = super.$long("accountStatus");
		Long stationId = super.$long("stationId");

		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");

		AccountVo accountVo = new AccountVo();
		accountVo.setId(id);
		accountVo.setAccount(account);
		accountVo.setStatus(accountStatus);
		accountVo.setAccountType(accountType);
		accountVo.setStationId(stationId);
		accountVo.setPwd(pwd);
		accountVo.setRpwd(rpwd);
		sysAccountService.saveAccount(accountVo);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updpwd")
	@ResponseBody
	public void updpwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		sysAccountService.updPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@RequestMapping(value = "/updreppwd")
	@ResponseBody
	public void updreppwd() {
		Long id = super.$long("id");
		String pwd = super.$("pwd");
		String rpwd = super.$("rpwd");
		super.isNotNull(pwd, "新密码不能为空!");
		super.isNotNull(rpwd, "确认密码不能为空!");
		sysAccountService.updRepPwd(id, pwd, rpwd);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/updStatus")
	public void updStatus() {
		Long id = super.$long("id");
		Long accountStatus = super.$long("accountStatus");

		SysAccount account = new SysAccount();
		account.setId(id);
		account.setAccountStatus(accountStatus);
		sysAccountService.updStatus(account);
		super.renderSuccess();
	}
	
	@RequestMapping("/view")
	public String viewDetail(Long id, Long stationId,Model model) {
		if (id == null || id <= 0) {
			throw new ParameterException("账户不存在");
		}
		Map map = sysAccountService.getAccountById(id, stationId);
		if (map == null) {
			throw new ParameterException("账户不存在");
		}
		if (map.containsKey("parentNames")) {
			String parentNames = (String) map.get("parentNames");
			if (StringUtils.isNotEmpty(parentNames)) {
				String[] pns = parentNames.split(",");
				parentNames = "";
				for (String pn : pns) {
					if (StringUtils.isNotEmpty(pn)) {
						parentNames = parentNames + " > " + pn;
					}
				}
				parentNames = parentNames.substring(3);
				model.addAttribute("parentNames", parentNames);
			}
		}
		map.put("cardNo",yincang((String)map.get("cardNo"),4));
		map.put("userName",yincang((String)map.get("userName"),1));
		model.addAttribute("map", map);
		return super.goPage("/page/platform/accountDetail.jsp");
	}
	
	private String yincang(String str, int baoliuHouJiwei) {
		if (StringUtils.isEmpty(str)) {
			return "";
		}
		if (str.length() < baoliuHouJiwei + 1) {
			return str;
		}
		String aaa = "**";
		return aaa + str.substring(str.length() - baoliuHouJiwei, str.length());
	}
}
