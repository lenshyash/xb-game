package com.game.controller.admin;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.core.SystemConfig;
import com.game.util.StationUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH)
public class MemberFolderController extends BaseAdminController {

	/**
	 * 切换member文件夹
	 
	@RequestMapping("/changeMember")
	@ResponseBody
	public void changeMember() throws IOException {
		if(StringUtils.equals(StationUtil.getMemberFolder(), SystemConfig.SOURCE_FOLDER_MEMBER_2)){
			RedisAPI.delCache(SystemConfig.SOURCE_FOLDER_MEMBER_KEY);
		}else{
			RedisAPI.addCache(SystemConfig.SOURCE_FOLDER_MEMBER_KEY, SystemConfig.SOURCE_FOLDER_MEMBER_2, 0);
		}
		
		super.renderJson(MixUtil.newHashMap("success",true,"msg","文件夹成功切换到【" + StationUtil.getMemberFolder() + "】"));
	}
	*/
	
	/**
	 * 查看当前member文件夹目录
	 */
	@RequestMapping("/getMember")
	@ResponseBody
	public void getMember(){
		super.renderJson(StationUtil.getMemberFolder());
	}
}
