package com.game.controller.admin.report;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/global")
public class AdGlobalReportController extends BaseAdminController {

	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;
	
	private static Logger logger = Logger.getLogger(AdGlobalReportController.class);

	@RequestMapping("/index")
	public String index(String begin, String end, String account, Map<String, Object> map) {
		HttpServletRequest request = super.getRequest();
		request.setAttribute("curDate", DateUtil.getCurrentDate());
		return super.goPage("/page/report/global.jsp");
	}
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String key = "globallreport:stat:time:all";
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		String begin = super.$("begin");
		String end = super.$("end");
		super.renderJson(MixUtil.newHashMap("data",dailyMoneyService.getGlobalReportForAdmin(DateUtil.toDate(begin),DateUtil.getTomorrow(end))));
		
		
		logger.error("------------全局报表统计开始-----------");
		logger.error("------------全局报表数据》》-----------"+MixUtil.newHashMap("data",dailyMoneyService.getGlobalReportForAdmin(DateUtil.toDate(begin), DateUtil.getTomorrow(end))));
		logger.error("------------全局报表统计结束-----------");
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
}
