package com.game.controller.admin.log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.constant.LogType;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysLog;
import com.game.model.vo.SysLogVo;
import com.game.service.SysLogService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/operationLog")
public class AdminOperationLogController extends BaseAdminController {
	@Autowired
	private SysLogService logService;

	@RequestMapping("/index")
	public String index(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/oplog.jsp");
	}

	@RequestMapping("/control")
	@ResponseBody
	public void control() {
		Long type = super.$long("type");
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		String ip = super.$("ip");
		String content = super.$("content");
		
		SysLogVo slvo = new SysLogVo();
		slvo.setPlatform(SysLog.PLATFORM_ADMIN);
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDate(begin));
		slvo.setEnd(DateUtil.getTomorrow(end));
		slvo.setType(type);
		slvo.setIp(ip);
		slvo.setContent(content);
		super.render(logService.getLogsPage(slvo));
	}

	@RequestMapping("/platform")
	@ResponseBody
	public void platform() {
		Long type = super.$long("type");
		String account = super.$("account");
		String content = super.$("content");
		String begin = super.$("begin");
		String end = super.$("end");
		Long stationId = super.$long("stationId");
		String ip = super.$("ip");
		SysLogVo slvo = new SysLogVo();
		slvo.setPlatform(SysLog.PLATFORM_PLATFORM);
		slvo.setStationId(stationId);
		slvo.setAccount(account);
		slvo.setBegin(DateUtil.toDate(begin));
		slvo.setEnd(DateUtil.getTomorrow(end));
		slvo.setType(type);
		slvo.setIp(ip);
		slvo.setContent(content);
		super.render(logService.getLogsPage(slvo));
	}

	@RequestMapping("/typecombo")
	@ResponseBody
	public void typecombo() {
		List<Map> lts = new ArrayList<Map>();
		LogType[] types = LogType.values();
		for (int i = 0; i < types.length; i++) {
			lts.add(MixUtil.newHashMap("id", types[i].getType(), "name", types[i].getDesc(), "platform",
					types[i].getPlatform()));
		}
		super.renderJson(JsonUtil.toJson(lts));
	}
}
