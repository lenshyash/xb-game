package com.game.controller.admin.log;

import java.util.List;

import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigValue;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.service.AgentBaseConfigService;
import com.game.service.DataRecordDeleteService;
import com.game.service.SysStationDomainService;
import com.game.service.SysStationService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/dataRecordDelete")
public class DataRecordDeleteController extends BaseAdminController {
	@Autowired
	private SysStationService stationService;
	@Autowired
	private DataRecordDeleteService brService;
	@Autowired
	private SysStationDomainService sysStationDomainService;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	
	@RequestMapping("/index")
	public String index(Model model) {
		model.addAttribute("stationList", stationService.getAllActive());
		List<AgentBaseConfig> list = agentBaseConfigService.getSysConfigs();
		for (AgentBaseConfig agentBaseConfig : list) {
			String name = agentBaseConfig.getName();
			if(StringUtil.isNotEmpty(name) && "服务器名称".equals(name)) {
				String initValue = agentBaseConfig.getInitValue();
				if("信博展示".equals(initValue)) {
					model.addAttribute("isZhanShi", "on");
					break;
				}
			}
		}
		return super.goPage("/page/log/dataRecordDelete.jsp");
	}

	/**
	 * 删除彩票、体育、真人、电子投注记录 lottery sport real electron
	 */
	@ResponseBody
	@RequestMapping("/bettingLog")
	public void bettingLog(Long stationId, Integer timeLen, String type) {
		renderJson(brService.bettingLog(stationId, timeLen, type));
	}

	/**
	 * 删除账变记录
	 */
	@RequestMapping("/financeLog")
	@ResponseBody
	public void financeLog(Long stationId, Integer timeLen) {
		renderJson(brService.financeLog(stationId, timeLen));
	}

	/**
	 * 删除充值记录
	 */
	@RequestMapping("/payonlineLog")
	@ResponseBody
	public void payonlineLog(Long stationId, Integer timeLen) {
		renderJson(brService.payonlineLog(stationId, timeLen));
	}

	/**
	 * 删除提款记录
	 */
	@RequestMapping("/withdrawLog")
	@ResponseBody
	public void withdrawLog(Long stationId, Integer timeLen) {
		renderJson(brService.withdrawLog(stationId, timeLen));
	}

	/**
	 * 删除真人转账记录
	 */
	@RequestMapping("/realchangerdLog")
	@ResponseBody
	public void realchangerdLog(Long stationId, Integer timeLen) {
		renderJson(brService.realchangerdLog(stationId, timeLen));
	}

	/**
	 * 删除管理日志
	 */
	@RequestMapping("/operationLog")
	@ResponseBody
	public void operationLog(Long stationId, Integer timeLen) {
		renderJson(brService.operationLog(stationId, timeLen));
	}

	/**
	 * 删除登录日志
	 */
	@RequestMapping("/loginLog")
	@ResponseBody
	public void loginLog(Long stationId, Integer timeLen) {
		renderJson(brService.loginLog(stationId, timeLen));
	}
	
	/**
	 * 删除登录日志
	 */
	@RequestMapping("/reportLog")
	@ResponseBody
	public void reportLog(Long stationId, Integer timeLen) {
		renderJson(brService.reportLog(stationId, timeLen));
	}
	
	/**
	 * 获取站点基本信息并返回回去
	 */
	@ResponseBody
	@RequestMapping("/getbackupStationData")
	public void getbackupStationData(Long stationId) {
		SysStation station = stationService.findOneById(stationId);
		List<SysStationDomain> stationdomain = sysStationDomainService.getDomainList(stationId);
		List<AgentBaseConfigValue> stationconfigval = agentBaseConfigService.getAllConfValsByStationId(stationId);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("station", station);
		jsonObject.put("stationdomain", stationdomain);
		jsonObject.put("stationconfigval", stationconfigval);
		String string = JSON.toJSONString(jsonObject);
		renderText(string);
	}
	
	/**
	 * 物理删除站点  站点domain 站点配置
	 */
	@ResponseBody
	@RequestMapping("/deleteStationData")
	public void deleteStationData(Long stationId) {
		stationService.deleteStationById(stationId);

		renderJson(MixUtil.newHashMap("success", true, "msg", "站点已被删除!"));
	}
	
	/**
	 * 删除站内信
	 */
	@RequestMapping("/userMessage")
	@ResponseBody
	public void userMessage(Long stationId, Integer timeLen) {
		renderJson(brService.userMessage(stationId, timeLen));
	}
	
}
