package com.game.controller.admin.log;

import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.service.GlobalStatService;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/onlineStat")
public class OnlineStatController extends BaseAdminController {
	@Autowired
	private GlobalStatService globalStatService;

	@RequestMapping("/index")
	public String index(Model model) {
		return super.goPage("/page/log/onlineStat.jsp");
	}

	@ResponseBody 
	@RequestMapping("/list")
	public void list() {
		renderJson(MixUtil.newHashMap("data",globalStatService.getOnLineCountMap()));
	}
}
