package com.game.controller.admin.log;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.permission.annotation.NotNeedLogin;
import com.game.util.ShellUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH)
public class NginxLogController extends BaseAdminController {
	
	@ResponseBody
	@RequestMapping("/catgrepnginxlog")
	public void catGrepNginxLog(String key) {
//		if (StringUtils.isEmpty(key)) {
//			renderText("请输入key");
//			return;
//		}
//		renderText("运行结果："+ShellUtil.catGrepNginxLog(key));
	}
	
	@ResponseBody
	@RequestMapping("/cleanNginxCache")
	public void cleanNginxCache() {
//		renderText("运行结果："+ShellUtil.cleanNginxCache());
	}
}
