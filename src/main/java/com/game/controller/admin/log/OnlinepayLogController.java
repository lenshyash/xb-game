package com.game.controller.admin.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.vo.OnlinepayLogVo;
import com.game.model.vo.SysLogVo;
import com.game.service.AgentOnlinepayLogService;
import com.game.service.SysLogService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/onlinepaylog")
public class OnlinepayLogController extends BaseAdminController {
	@Autowired
	private SysLogService logService;

	@Autowired
	private AgentOnlinepayLogService agentOnlinepayLogService;

	@RequestMapping("/index")
	public String index(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/onlinepaylog.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		Long stationId = super.$long("stationId");
		String payType = super.$("payType");
		String content = super.$("content");
		String begin = super.$("begin");
		String end = super.$("end");

		OnlinepayLogVo onlinepayLogVo = new OnlinepayLogVo();
		onlinepayLogVo.setBegin(DateUtil.toDate(begin));
		onlinepayLogVo.setContent(content);
		onlinepayLogVo.setEnd(DateUtil.getTomorrow(end));
		onlinepayLogVo.setPayType(payType);
		onlinepayLogVo.setStationId(stationId);

		super.render(agentOnlinepayLogService.getPage(onlinepayLogVo));
	}

}
