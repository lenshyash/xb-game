package com.game.controller.admin.log;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.platform.AdminBlackIp;
import com.game.model.vo.OnlinepayLogVo;
import com.game.model.vo.SysLogVo;
import com.game.service.AdminblackipService;
import com.game.service.AgentAdressBookService;
import com.game.service.AgentOnlinepayLogService;
import com.game.service.SysLogService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/blackIp")
public class AdminBlackIpController extends BaseAdminController {
	@Autowired
	private AdminblackipService adminblackipService;

	@RequestMapping("/index")
	public String index(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/adminBlackIp.jsp");
	}

	@RequestMapping("/list")
	@ResponseBody
	public void list() {
		String ip = super.$("ip");
		
		super.render(adminblackipService.page(ip));
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public void add(String ip) {
		AdminBlackIp blackIp = new AdminBlackIp();
		blackIp.setIp(ip);
		blackIp.setCreateDatetime(new Date());
		
		adminblackipService.add(blackIp);
		
		super.renderSuccess();
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(Long id) {
		adminblackipService.delete(id);
		
		super.renderSuccess();
	}

}
