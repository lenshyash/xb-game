package com.game.controller.admin.log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.redis.RedisAPI;
import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysAccount;
import com.game.service.DataRecordDeleteService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAccountService;
import com.game.service.SysStationService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/dataSynchronization")
public class DataSynchronizationController extends BaseAdminController {
	@Autowired
	private SysStationService stationService;
	@Autowired
	private DataRecordDeleteService brService;
	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;
	@Autowired
	private SysAccountService sysAccountService;
	
	@RequestMapping("/index")
	public String index(Model model) {
		model.addAttribute("stationList", stationService.getAllActive());
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/dataSynchronization.jsp");
	}

	/**
	 * 删除彩票、体育、真人、电子投注记录 lottery sport real electron
	 */
	@ResponseBody
	@RequestMapping("/synDailiData")
	public void synDailiData(Long stationId,String end,String begin) {
		dailyMoneyService.synDailiData(stationId, end, begin);
		renderSuccess();
	}
	
	/**
	 * 修复上下级关系功能
	 */
	@ResponseBody
	@RequestMapping("/fixUpLowRelationship")
	public void fixUpLowRelationship(Long stationId) {
		dailyMoneyService.fixUpLowRelationship(stationId);
		renderSuccess();
	}
	
	/**
	 * 清除当前用户和他所有下级的缓存
	 */
	@ResponseBody
	@RequestMapping("/delSysaccountCache")
	public void delSysaccountCache(Long stationId,String account) {
//		if(StringUtil.isEmpty(account)) {
//			throw new GenericException("账号为空");
//		}
//		Map map = sysAccountService.queryAccount(account, stationId);
//		String parents = map.get("parents").toString();
//		if (StringUtil.isEmpty(parents)) {
//			parents = ",";
//		}
//		parents = parents+map.get("id").toString() + ",";
//		List<SysAccount> list = sysAccountService.getAllDownMember(account, parents, stationId);
//		ArrayList<String> idList = new ArrayList<String>();
//		for (SysAccount sysAccount : list) {
//			String id = sysAccount.getId().toString();
//			id = "sysAccount_"+id;
//			idList.add(id);
//		}
		
		Long[] l = {SysAccount.ACCOUNT_PLATFORM_MEMBER, SysAccount.ACCOUNT_PLATFORM_AGENT};
		List<Long> ids = sysAccountService.queryAccountIdsByType(stationId, l);
		
		ArrayList<String> idList = new ArrayList<String>();
		for (Long id2 : ids) {
			String id = id2.toString();
			id = "sysAccount_"+id;
			idList.add(id);
		}
		if(idList!=null && idList.size()>0) {
			RedisAPI.delCache(0, idList);
		}
		renderSuccess();
	}

}
