package com.game.controller.admin.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysLoginLog;
import com.game.model.vo.SysLoginLogVo;
import com.game.service.SysLogService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/loginLog")
public class AdminLoginLogController extends BaseAdminController {
	@Autowired
	private SysLogService logService;

	@RequestMapping("/index")
	public String index(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/loginlog.jsp");
	}

	@RequestMapping("/control")
	@ResponseBody
	public void control() {
		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");

		SysLoginLogVo sllvo = new SysLoginLogVo();
		sllvo.setAccount(account);
		sllvo.setPlatform(SysLoginLog.PLATFORM_ADMIN);
		sllvo.setBegin(DateUtil.toDate(begin));
		sllvo.setEnd(DateUtil.getTomorrow(end));
		super.render(logService.getLoginLogPage(sllvo));
	}

	@RequestMapping("/platform")
	@ResponseBody
	public void platform() {

		String account = super.$("account");
		String begin = super.$("begin");
		String end = super.$("end");
		long stationId = super.$long("stationId");
		SysLoginLogVo sllvo = new SysLoginLogVo();
		sllvo.setPlatform(SysLoginLog.PLATFORM_PLATFORM);
		sllvo.setStationId(stationId);
		sllvo.setAccount(account);
		sllvo.setBegin(DateUtil.toDate(begin));
		sllvo.setEnd(DateUtil.getTomorrow(end));
		super.render(logService.getLoginLogPage(sllvo));
	}
}
