package com.game.controller.admin.log;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.controller.admin.BaseAdminController;
import com.game.core.SystemConfig;
import com.game.model.SysLoginLog;
import com.game.model.vo.SysLoginLogVo;
import com.game.permission.annotation.SortMapping;
import com.game.service.StationHandMoneyService;
import com.game.service.SysLogService;
import com.game.util.DateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/stationHandMoney")
public class StationHandMoneyController extends BaseAdminController {
	@Autowired
	private StationHandMoneyService handMoneyService;

	@RequestMapping("/index")
	public String index(Model model) {
		String curDate = DateUtil.getCurrentDate();
		super.getRequest().setAttribute("startTime", curDate);
		super.getRequest().setAttribute("endTime", curDate);
		return super.goPage("/page/log/stationHandMoney.jsp");
	}

	@RequestMapping("/control")
	@ResponseBody
	@SortMapping(mapping = { "money","d.money" })
	public void control() {
		String begin = super.$("begin");
		String end = super.$("end");
		long stationId = super.$long("stationId");
		SysLoginLogVo sllvo = new SysLoginLogVo();
		sllvo.setBegin(DateUtil.toDate(begin));
		sllvo.setEnd(DateUtil.getTomorrow(end));
		sllvo.setStationId(stationId);
		super.render(handMoneyService.getStaionHandMoneyData(sllvo));
	}


}
