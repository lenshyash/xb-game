package com.game.controller.admin.log;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.controller.admin.BaseAdminController;
import com.game.controller.agent.BaseAgentController;
import com.game.core.SystemConfig;
import com.game.model.vo.ReportParamVo;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAnnouncementService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.ValidateUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH + "/stationGlobalReport")
public class StationGlobalReportController extends BaseAdminController {
	

	
	
	
	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	@RequestMapping("/index")
	public String index(String begin, String end, String account, Map<String, Object> map) {
		HttpServletRequest request = super.getRequest();
		String curDate = DateUtil.getCurrentDate();
		request.setAttribute("startTime", curDate);
		request.setAttribute("endTime", curDate);
		
		//index.do获取不到站点ID  这里先手动设置属性 让JS可用
		request.setAttribute("lottery", "on");
		request.setAttribute("real", "on");
		request.setAttribute("sport", "on");
		request.setAttribute("markSix", "on");
		request.setAttribute("dianZi", "on");
		request.setAttribute("thirdSports", "on");
		request.setAttribute("thirdLottery", "on");
		request.setAttribute("chess", "on");

		request.setAttribute("saccount", null);
		return super.goPage("/page/log/stationGlobal.jsp");

	}
	
	@ResponseBody
	@RequestMapping("/list")
	public void list() {
		String key = "globallreport:stat:time:" + SysUtil.getSession().getId();
		Long last = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, Long.class);
		long t = System.currentTimeMillis();
		if (last != null && t - last < 3000) {
			renderFailure("由于统计比较耗性能，2次查询需要间隔3秒以上");
			return;
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, t + "");
		String account = super.$("account");
		String agentName = super.$("agent");
		String begin = super.$("begin");
		String end = super.$("end");
		Long refresh = super.$long("refresh");
		Long reportType = super.$long("reportType");
		String filterAccount = super.$("filterAccount");
		ReportParamVo paramVo = new ReportParamVo();
		paramVo.setAccount(account);
		paramVo.setBegin(DateUtil.toDate(begin));
		paramVo.setEnd(DateUtil.getTomorrow(end));
		paramVo.setStationId(StationUtil.getStationId());
		paramVo.setAgentName(agentName);
		paramVo.setRefresh(refresh);
		paramVo.setFilterAccount(filterAccount);
		// super.renderJson(reportService.getGlobalReport(paramVo));
		
		long stationId = super.$long("stationId");
		paramVo.setReportType(reportType);
		paramVo.setStationId(stationId);
		super.renderJson(dailyMoneyService.getGlobalReport(paramVo));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, "0");
	}
	
	

	
}
