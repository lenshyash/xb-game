package com.game.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.core.SystemConfig;
import com.game.model.MemberRedPacket;
import com.game.permission.annotation.NotNeedLogin;
import com.game.service.AdminUserService;
import com.game.service.MemberRedPacketService;
import com.game.util.VerifyCodeUtil;

@Controller
@RequestMapping(SystemConfig.ADMIN_CONTROL_PATH)
public class AdminLoginController extends BaseAdminController {

	@Autowired
	private AdminUserService adminUserService;
	@Autowired
	private MemberRedPacketService redPacketService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/login")
	public void login() {
		String account = $c("account").trim().toLowerCase();
		String password = $c("password", "请输入密码");
		String verifyCode = $c("verifyCode", "请输入验证码");
		adminUserService.doLogin(account, password, verifyCode);
		renderSuccess();
	}

	@NotNeedLogin
	@RequestMapping("/logout")
	public String logout() {
		HttpSession session = SysUtil.getSession();
		session.removeAttribute(SystemConfig.SESSION_ADMIN_KEY);
		return super.goPage("/index.jsp");
	}

	@NotNeedLogin
	@RequestMapping("/timeout")
	public String timeout() {
		return super.goPage("/index.jsp");
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/verifycode")
	public void verifycode() throws Exception {
		VerifyCodeUtil.createVerifyCode(true, null);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/initRedPacket")
	public void initRedPacket() {
		CacheUtil.flashDB(CacheType.MEMBER_RED_PACKET);
		List<MemberRedPacket> list = redPacketService.getRedPacketList(null);
		if (list != null && !list.isEmpty()) {
			Date now = new Date();
			for (MemberRedPacket m : list) {
				if (m.getEndDatetime().after(now)) {
					redPacketService.initRedPacketRedis(m);
				}
			}
		}
		renderText("红包初始化成功");
	}
}
