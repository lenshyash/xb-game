package com.game.event;

public class LotteryPlayModifyEvent extends GameEvent {
	public LotteryPlayModifyEvent() {
	}

	public LotteryPlayModifyEvent(Long stationId,Integer lotType) {
		super(stationId,lotType);
	}
}
