package com.game.event;

import com.game.model.SysStation;

public class SysStationSaveEvent extends GameEvent {
	private SysStation station;

	public SysStationSaveEvent(SysStation station) {
		super(station.getId());
		this.station = station;
	}

	public SysStation getStation() {
		return station;
	}

	public void setStation(SysStation station) {
		this.station = station;
	}

}
