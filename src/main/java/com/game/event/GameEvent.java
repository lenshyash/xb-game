package com.game.event;

public class GameEvent {
	private Long stationId;
	private Integer lotType;
	private String playCode;

	public GameEvent() {
	}

	public GameEvent(Long stationId) {
		this.stationId = stationId;
	}
	
	public GameEvent(Long stationId,String playCode){
		this.stationId = stationId;
		this.playCode = playCode;
	}
	
	public GameEvent(Long stationId,Integer lotType){
		this.stationId = stationId;
		this.lotType = lotType;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getPlayCode()
	{
		return playCode;
	}

	public void setPlayCode(String playCode)
	{
		this.playCode = playCode;
	}

	public Integer getLotType()
	{
		return lotType;
	}

	public void setLotType(Integer lotType)
	{
		this.lotType = lotType;
	}
	

	
}
