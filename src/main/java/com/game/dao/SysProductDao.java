package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysProduct;
import com.game.model.vo.ProductVo;

@Repository
public class SysProductDao extends JdbcUtilImpl<SysProduct> {

	public Page getProductPage(ProductVo pvo) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_product");
		sql_sb.append(" WHERE station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", pvo.getStationId());
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List getProductList(ProductVo pvo) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_product");
		sql_sb.append(" WHERE station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", pvo.getStationId());
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}
}
