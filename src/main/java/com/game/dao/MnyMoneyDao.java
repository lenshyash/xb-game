package com.game.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.model.vo.ReportParamVo;
import com.game.util.StationUtil;

@Repository
public class MnyMoneyDao extends JdbcUtilImpl<MnyMoney> {
	public BigDecimal[] updateMoney(long accountId, BigDecimal optMoney) throws SQLException {
		String sql = "SELECT optmoney(" + accountId + "," + optMoney + ")";
		BigDecimal [] bs= (BigDecimal[]) (super.queryForArray(sql).getArray());
		RedisAPI.delCache( Contants.MNY_MONEY_ACCOUNT_ID + accountId);
		return bs;
	}

	public BigDecimal[] updateBalanceGem(long accountId, BigDecimal bgMoney,BigDecimal bgIncome ) throws SQLException {
		String sql = "SELECT optbgmoney(" + accountId + "," + bgMoney + ","+bgIncome+")";
		BigDecimal [] bs= (BigDecimal[]) (super.queryForArray(sql).getArray());
//		RedisAPI.delCache( Contants.BALANCE_GEM_MONEY_ACCOUNT_ID + accountId);
		return bs;
	}
	
	public Map getMoneyByAccount(String account) {
		return getMoneyByAccount(account,StationUtil.getStationId());
	}
	
	public Map getMoneyByAccount(String account,Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.account,a.account_type,a.report_type,m.*,i.user_name");
		sql_sb.append(" FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" INNER JOIN sys_account_info i on a.id = i.account_id");
		sql_sb.append(" WHERE a.flag_active >=1 AND a.station_id = :stationId");
		sql_sb.append(" AND lower(a.account) = :account");
		return super.selectSingleCamelMap(sql_sb.toString(),
				MixUtil.newHashMap("stationId", stationId, "account", StringUtil.isEmpty(account)?"":account.toLowerCase()));
	}

	public BigDecimal getsumMoney(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT sum(m.money) FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >=1 AND a.station_id = :stationId and a.account_type<>6 and a.account_type<>9");
		
		Map paramsMap = MixUtil.newHashMap("stationId", StationUtil.getStationId());
		if(StringUtil.isNotEmpty(paramVo.getChildren())) {
			paramsMap.put("selfId", paramVo.getSelfId());
			sql_sb.append(" AND (a.id = :selfId OR a.parents LIKE :parents)");
			if (StringUtil.equals(paramVo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
				paramsMap.put("parents", paramVo.getChildren());
			}else {
				paramsMap.put("parents", paramVo.getChildren()+"%");
			}
		}
		
		return super.queryForBigDecimal(sql_sb.toString(), paramsMap);
	}

	public BigDecimal getsumMoneyAdmin(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT sum(m.money) FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >=1 AND a.station_id = :stationId and a.account_type<>6 and a.account_type<>9");

		Map paramsMap = MixUtil.newHashMap("stationId", StationUtil.getStationId());

		return super.queryForBigDecimal(sql_sb.toString(), paramsMap);
	}

	public Map getMoneyByAccountAndStationId(Long stationId, String account) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.account,m.*");
		sql_sb.append(" FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >=1 ");
		sql_sb.append(" AND a.account_type IN (");
		sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER).append(",").append(SysAccount.ACCOUNT_PLATFORM_AGENT);
		sql_sb.append(") AND a.station_id = :stationId");
		sql_sb.append(" AND lower(a.account) = :account");
		return super.selectSingleCamelMap(sql_sb.toString(),
				MixUtil.newHashMap("stationId", stationId, "account", StringUtil.isEmpty(account)?"":account.toLowerCase()));
	}

	public BigDecimal getBalance(Long accountId) {
		return super.queryForBigDecimal("select money from mny_money where account_id=:accountId",
				MixUtil.newHashMap("accountId", accountId));
	}

	/**
	 * 得到站点的会员和代理的余额总和
	 * 
	 * @param stationId
	 * @return
	 */
	public String getStationMoney(Long stationId,Long reportType) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT sum(m.money) as balance");
		sql_sb.append(" FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >=1");
		sql_sb.append(" AND a.account_type IN (");
		sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER).append(",").append(SysAccount.ACCOUNT_PLATFORM_AGENT);
		sql_sb.append(") and a.station_id=:stationId");
		
		Map<Object, Object> paramsMap = MixUtil.newHashMap("stationId", stationId);
		if(StringUtil.isNotEmpty(reportType) && 0!=reportType) {
			sql_sb.append(" and report_type = :reportType");
			paramsMap.put("reportType", reportType);
		}
		return queryForString(sql_sb.toString(), paramsMap);
	}

	public void deleteByAccountId(Long id) {
		// TODO Auto-generated method stub
		String sql = "delete from mny_money where account_id = :accountId";
		super.update(sql, MixUtil.newHashMap("accountId", id));
	}
	/**
	 * 获取代理线的的余额
	 * @param accountVo
	 * @return
	 */
	public BigDecimal getTeamSumMoney(AccountVo accountVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT sum(m.money) FROM sys_account a INNER JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.station_id = :stationId and a.account_type<>6 and a.account_type<>9");
		
		Map paramsMap = MixUtil.newHashMap();
		paramsMap.put("stationId", accountVo.getStationId());
		if(StringUtil.isNotEmpty(accountVo.getParents())) {
			paramsMap.put("account", accountVo.getAccount());
			sql_sb.append(" AND (a.account = :account OR a.parents LIKE :parents)");
			if (StringUtil.equals(accountVo.getSearchType(), accountVo.SEARCHTYPE_NEXT)) {
				paramsMap.put("parents", accountVo.getParents());
			}else {
				paramsMap.put("parents", accountVo.getParents()+"%");
			}
		}
		
		return super.queryForBigDecimal(sql_sb.toString(), paramsMap);
	}

}
