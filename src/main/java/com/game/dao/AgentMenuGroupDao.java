package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.AgentMenuGroup;

@Repository
public class AgentMenuGroupDao extends JdbcUtilImpl<AgentMenuGroup> {

	public List<AgentMenuGroup> getAgentPermission(Long groupId, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("groupId", groupId);
		return super.query2Model("SELECT * FROM agent_menu_group WHERE station_id=:stationId AND group_id=:groupId ",
				paramMap);
	}

	private void deleteAll(Long groupId, Long stationId, List<Long> delIds) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("DELETE FROM agent_menu_group WHERE station_id = :stationId");

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (delIds != null && delIds.size() > 0) {
			sql_sb.append(" AND menu_id IN ( ");
			for (int i = 0; i < delIds.size(); i++) {
				if (i > 0) {
					sql_sb.append(",");
				}
				sql_sb.append(":menuId_" + i);
				paramMap.put("menuId_" + i, delIds.get(i));
			}
			sql_sb.append(")");
		}
		if (Validator.isNotNull(groupId)) {
			sql_sb.append(" AND group_id = :groupId");
			paramMap.put("groupId", groupId);
		}
		super.update(sql_sb.toString(), paramMap);
	}

	public void batchOpe(Long groupId, Long stationId, List<Long> delIds, List<AgentMenuGroup> ms) {
		if (delIds != null && delIds.size() > 0) {
			deleteAll(groupId, stationId, delIds);
		}
		batchInsert(ms);
		CacheUtil.delCache(CacheType.AGENT_MENU, "stationId_" + stationId + "_groupId_" + groupId);
		CacheUtil.delCache(CacheType.AGENT_GROUP_PERMISSION_MAP, "agent_perm_" + stationId + "_" + groupId);
	}

	public void deleteByGroupId(Long stationId, Long groupId) {
		deleteAll(groupId, stationId, null);
		CacheUtil.delCache(CacheType.AGENT_MENU, "stationId_" + stationId + "_groupId_" + groupId);
		CacheUtil.delCache(CacheType.AGENT_GROUP_PERMISSION_MAP, "agent_perm_" + stationId + "_" + groupId);
	}
}
