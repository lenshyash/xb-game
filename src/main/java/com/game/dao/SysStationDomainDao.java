package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.SysStationDomain;
import com.game.model.vo.StationVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SysStationDomainDao extends JdbcUtilImpl<SysStationDomain> {
	public Page<Map> getPage(StationVo svo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT d.*,s.name,COALESCE(d.folder,s.floder) as floder");
		sql_sb.append(" FROM sys_station_domain d LEFT JOIN sys_station s ON d.station_id = s.id");
		sql_sb.append(" WHERE d.flag_active >=1");

		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotEmpty(svo.getDomain())) {
			paramMap.put("domain", svo.getDomain() + "%");
			sql_sb.append(" AND d.domain LIKE :domain");
		}

		if (Validator.isNotNull(svo.getId())) {
			paramMap.put("stationId", svo.getId());
			sql_sb.append(" AND s.id = :stationId");
		}

		if (StringUtils.isNotEmpty(svo.getName())) {
			paramMap.put("name", svo.getName());
			sql_sb.append(" AND s.name = :name");
		}

		if (StringUtils.isNotEmpty(svo.getAgentName())) {
			paramMap.put("agentName", svo.getAgentName());
			sql_sb.append(" AND d.agent_name = :agentName");
		}

		sql_sb.append(" ORDER BY s.floder desc,s.id desc");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<SysStationDomain> list(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM sys_station_domain WHERE flag_active >=1");

		Map paramMap = MixUtil.newHashMap();
		if (stationId != null) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND station_id = :stationId");
		}

		sql_sb.append(" ORDER BY id DESC");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public List<Map<String, Object>> getAllActive() {
		String sql = "SELECT d.domain,s.floder FROM sys_station_domain d LEFT JOIN sys_station s ON d.station_id = s.id WHERE d.flag_active >=1 and d.status=2";
		return super.selectMapList(sql);
	}

	public Page<SysStationDomain> getPageForAgent(String domain, String agentName, Long stationId, Long type) {
		StringBuilder sql = new StringBuilder("SELECT * FROM sys_station_domain WHERE flag_active>=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotEmpty(domain)) {
			paramMap.put("domain", domain + "%");
			sql.append(" AND domain LIKE :domain");
		}
		if (stationId != null) {
			paramMap.put("stationId", stationId);
			sql.append(" AND station_id=:stationId");
		}
		if (StringUtils.isNotEmpty(agentName)) {
			paramMap.put("agentName", agentName);
			sql.append(" AND agent_name=:agentName");
		}
		if (type != null) {
			paramMap.put("type", type);
			sql.append(" AND type=:type");
		}
		sql.append(" ORDER BY create_datetime desc");
		return super.paged2Obj(sql.toString(), paramMap);
	}

	public SysStationDomain findOne(Long id, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("stationId", stationId);
		return query21Model("select * from sys_station_domain where id=:id and station_id=:stationId", paramMap);
	}

	public void updStatus(Long id, Long status, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("stationId", stationId);
		paramMap.put("status", status);
		update("update sys_station_domain set status=:status where id=:id and station_id=:stationId", paramMap);
	}

	public void updateDomainChatToken(Long domainId, String domainChatToken) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", domainId);
		paramMap.put("domainChatToken", domainChatToken);
		update("update sys_station_domain set domain_plat_token=:domainChatToken where id=:id", paramMap);
	}
	
	public void updateDomain(Long id, String domain, Long agentId, String agentName, String defaultHome,
			Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("stationId", stationId);
		paramMap.put("agentId", agentId);
		paramMap.put("agentName", agentName);
		paramMap.put("defaultHome", defaultHome);
		paramMap.put("date", new Date());
		paramMap.put("domain", domain);
		paramMap.put("userId", UserUtil.getUserId());
		update("update sys_station_domain set domain=:domain,agent_id=:agentId,agent_name=:agentName,modify_user_id=:userId,modify_datetime=:date,default_home=:defaultHome where id=:id and station_id=:stationId",
				paramMap);
	}

	public SysStationDomain findByDomain(String domain) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("domain", domain);
		return query21Model("select * from sys_station_domain where domain=:domain", paramMap);
	}
	
	public List<SysStationDomain> getFolderLst() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", StationUtil.getStationId());
		return super.query2Model("select DISTINCT folder from sys_station_domain where station_id=:stationId AND folder IS NOT NULL AND trim(folder) <>''", paramMap);
	}
	
	public List<SysStationDomain> getFilterFolderList() {
		Map<String, Object> paramMap = new HashMap<>();
		String url= "select DISTINCT t1.folder from sys_station_domain t1 where t1.station_id = :stationId  AND folder IS NOT NULL AND trim(t1.folder) <>'' and t1.folder not in (select DISTINCT folder from sys_station_folder_url t2 where t2.station_id  = :stationId)";
		paramMap.put("stationId", StationUtil.getStationId());
		return super.query2Model(url, paramMap);
	}
	
	public List<SysStationDomain> getFolderListByStation(Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		String url= "select DISTINCT t1.folder from sys_station_domain t1 where t1.station_id = :stationId  AND folder IS NOT NULL AND trim(t1.folder) <>'' and t1.folder not in (select DISTINCT folder from sys_station_folder_url t2 where t2.station_id  = :stationId)";
		paramMap.put("stationId", stationId);
		return super.query2Model(url, paramMap);
	}
	public SysStationDomain getDomainByFolder(String domain,String domainFolder, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("domain", domain);
		paramMap.put("domainFolder", domainFolder);
		paramMap.put("stationId", stationId);
		return query21Model("select * from sys_station_domain where domain=:domain and folder=:domainFolder and station_id = :stationId", paramMap);
		
	}

	public List<SysStationDomain> getListByStationType(Long stationId, Long type) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("type", type);
		paramMap.put("stationId", stationId);
		return super.query2Model("select * from sys_station_domain where status = "+SysStationDomain.STATUS_ENABLE+" and  type=:type  and station_id = :stationId", paramMap);
	}
	
	public List<SysStationDomain> getDomainList(Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		return query2Model("select * from sys_station_domain where station_id=:stationId", paramMap);
	}
	
	public void deleteDomainByStationId(Long stationId) {
		StringBuffer sql_sb = new StringBuffer();
		sql_sb.append("delete from sys_station_domain where station_id = :stationId");
		Map<String, Object> paramMap = new HashMap<>(); 
		paramMap.put("stationId", stationId);
		super.update(sql_sb.toString(), paramMap);
	}

	public SysStationDomain getAccessDomain(Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		return query21Model("select * from sys_station_domain where status = "+SysStationDomain.STATUS_ENABLE+" and station_id = :stationId and (type = 2 or type is null) ORDER BY type limit 1", paramMap);
	}
}
