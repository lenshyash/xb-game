package com.game.dao;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.SysLoginLog;
import com.game.model.vo.ReportParamVo;
import com.game.model.vo.SysLoginLogVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class StationHandMoneyDao extends JdbcUtilImpl<SysLoginLog> {

	/**
	 * 
	 * @return
	 */
	public Page getStaionHandMoneyData(SysLoginLogVo sllvo) {
		
		Map paramMap = MixUtil.newHashMap();
		StringBuffer buffer2 = new StringBuffer();
		buffer2.append("select concat_ws('-',c.name,c.floder)as name ,d.account,d.money from sys_station c,(");
		buffer2.append("SELECT a.account,b.money,a.station_id from sys_account a INNER join ");
		buffer2.append("(SELECT member_id,station_id,sum(money) as money from mny_com_record a where 1=1 ");
		if (Validator.isNotNull(sllvo.getStationId())) {
			buffer2.append("and station_id = :stationId");
			paramMap.put("stationId", sllvo.getStationId());
		}
		if (StringUtil.isNotEmpty(sllvo.getBegin())) {
			buffer2.append(" and a.create_datetime > :begin");
			paramMap.put("begin", sllvo.getBegin());
		}
		if (StringUtil.isNotEmpty(sllvo.getEnd())) {
			buffer2.append(" AND a.create_datetime < :end ");
			paramMap.put("end", sllvo.getEnd());
		}
		buffer2.append(" and not exists");
		buffer2.append("(SELECT * from mny_com_record b where a.member_id = b.member_id and  a.station_id = b.station_id and type <> 1)");
		buffer2.append("GROUP BY member_id,station_id) b on a.id = b.member_id) d where c.id= d.station_id order by d.money desc");
		String string = buffer2.toString();
		return super.page2CamelMap(string,paramMap);
	}


}
