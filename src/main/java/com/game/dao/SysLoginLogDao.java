package com.game.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.SysLoginLog;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.ReportParamVo;
import com.game.model.vo.SysLoginLogVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SysLoginLogDao extends JdbcUtilImpl<SysLoginLog> {

	/**
	 * 
	 * @return
	 */
	public Page getLoginLogPage(SysLoginLogVo sllvo) {

		StringBuilder sql_cols = new StringBuilder("");
		StringBuilder sql_tabs = new StringBuilder("");
		StringBuilder sql_whs = new StringBuilder("");
		StringBuilder sql_order = new StringBuilder("");
		sql_cols.append("SELECT l.*");
		sql_tabs.append(" FROM sys_login_log l");
		sql_whs.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();

		if (StationUtil.isAgentStation()) {
			sql_cols.append(",a.account_type");
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			if (!StringUtil.equals(UserUtil.getType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
				sql_whs.append(" AND a.account_type != :accountType");
				paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			}
		}
		
		if (StationUtil.isDailiStation()) {
			sql_cols.append(",a.account_type,a.agent_name");
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			
			if (StringUtil.isNotEmpty(sllvo.getParents())) {
				if (sllvo.getSearchSelf() != null && sllvo.getSearchSelf()) {
					paramMap.put("curUserId", sllvo.getSelfId());
					sql_whs.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
				} else {
					sql_whs.append(" AND a.parents LIKE :parents");
				}
				if (StringUtil.equals(sllvo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
					paramMap.put("parents", sllvo.getParents());
				}else {
					paramMap.put("parents", sllvo.getParents()+ "%");
				}
			}
		}

		if (StringUtil.isNotEmpty(sllvo.getAccount())) {
			sql_whs.append(" AND lower(l.account) = :account");
			paramMap.put("account", sllvo.getAccount().toLowerCase());
		}

		if (Validator.isNotNull(sllvo.getPlatform())) {
			sql_whs.append(" AND l.platform = :platform");
			paramMap.put("platform", sllvo.getPlatform());
		}

		if (Validator.isNotNull(sllvo.getStationId())) {
			sql_whs.append(" AND l.station_id = :stationId");
			paramMap.put("stationId", sllvo.getStationId());
		}

		if (StringUtil.isNotEmpty(sllvo.getBegin())) {
			sql_whs.append(" AND l.create_datetime >= :begin");
			paramMap.put("begin", sllvo.getBegin());
		}

		if (StringUtil.isNotEmpty(sllvo.getEnd())) {
			sql_whs.append(" AND l.create_datetime < :end");
			paramMap.put("end", sllvo.getEnd());
		}

		if (StringUtil.isNotEmpty(sllvo.getIp())) {
			sql_whs.append(" AND l.account_ip LIKE :ip");
			paramMap.put("ip", sllvo.getIp() + "%");
		}
		if(StringUtils.isNotEmpty(sllvo.getLoginOs())) {
			sql_whs.append(" AND l.login_os=:loginOs");
			paramMap.put("loginOs", sllvo.getLoginOs());
		}
		if(StringUtils.isNotEmpty(sllvo.getDeviceId())) {
			sql_whs.append(" AND l.device_id=:deviceId");
			paramMap.put("deviceId", sllvo.getDeviceId());
		}
		if(StringUtils.isNotEmpty(sllvo.getDeviceType())) {
			sql_whs.append(" AND l.device_type=:deviceType");
			paramMap.put("deviceType", sllvo.getDeviceType());
		}
		sql_order.append(" ORDER BY l.create_datetime DESC");

		return super.page2CamelMap(sql_cols.append(sql_tabs).append(sql_whs).append(sql_order).toString(), paramMap);
	}

	/**
	 * 根据站点和投注时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from sys_login_log where");
		Map map = MixUtil.newHashMap();
		sb.append(" create_datetime <= :createTime");
		sb.append(" and station_id = :stationId");
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}
	
	/**
	 * 用作导出表格
	 * @param begin
	 * @param end
	 * @param account
	 * @param loginOs
	 * @param ip
	 * @return
	 */
	public List<Map> getList(SysLoginLogVo sllvo) {
		StringBuilder sql_cols = new StringBuilder("");
		StringBuilder sql_tabs = new StringBuilder("");
		StringBuilder sql_whs = new StringBuilder("");
		StringBuilder sql_order = new StringBuilder("");
		sql_cols.append("SELECT l.*");
		sql_tabs.append(" FROM sys_login_log l");
		sql_whs.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();
		boolean agentStation = StationUtil.isAgentStation();
		if (agentStation) {
			sql_cols.append(",a.account_type");
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			if (!StringUtil.equals(UserUtil.getType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
				sql_whs.append(" AND a.account_type != :accountType");
				paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			}
		}
		
		if (StationUtil.isDailiStation()) {
			sql_cols.append(",a.account_type,a.agent_name");
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			
			if (StringUtil.isNotEmpty(sllvo.getParents())) {
				if (sllvo.getSearchSelf() != null && sllvo.getSearchSelf()) {
					paramMap.put("curUserId", sllvo.getSelfId());
					sql_whs.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
				} else {
					sql_whs.append(" AND a.parents LIKE :parents");
				}
				if (StringUtil.equals(sllvo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
					paramMap.put("parents", sllvo.getParents());
				}else {
					paramMap.put("parents", sllvo.getParents()+ "%");
				}
			}
		}

		if (StringUtil.isNotEmpty(sllvo.getAccount())) {
			sql_whs.append(" AND lower(l.account) = :account");
			paramMap.put("account", sllvo.getAccount().toLowerCase());
		}

		if (Validator.isNotNull(sllvo.getPlatform())) {
			sql_whs.append(" AND l.platform = :platform");
			paramMap.put("platform", sllvo.getPlatform());
		}

		if (Validator.isNotNull(sllvo.getStationId())) {
			sql_whs.append(" AND l.station_id = :stationId");
			paramMap.put("stationId", sllvo.getStationId());
		}

		if (StringUtil.isNotEmpty(sllvo.getBegin())) {
			sql_whs.append(" AND l.create_datetime >= :begin");
			paramMap.put("begin", sllvo.getBegin());
		}

		if (StringUtil.isNotEmpty(sllvo.getEnd())) {
			sql_whs.append(" AND l.create_datetime < :end");
			paramMap.put("end", sllvo.getEnd());
		}

		if (StringUtil.isNotEmpty(sllvo.getIp())) {
			sql_whs.append(" AND l.account_ip LIKE :ip");
			paramMap.put("ip", sllvo.getIp() + "%");
		}
		if(StringUtils.isNotEmpty(sllvo.getLoginOs())) {
			sql_whs.append(" AND l.login_os=:loginOs");
			paramMap.put("loginOs", sllvo.getLoginOs());
		}

		sql_order.append(" ORDER BY l.create_datetime DESC");
		String sqlString = sql_cols.append(sql_tabs).append(sql_whs).append(sql_order).toString();
		 List list = super.selectCamelListMap(sqlString, paramMap);
		 System.out.println(list);
		 return list;
	}

    public Map getLastLogByAccount(Integer id) {
		StringBuilder sb = new StringBuilder("select case when cast(t1.account_ip as VARCHAR) =  cast(t2.account_ip as VARCHAR)  then '0' else '1' end as ip_flag,\n" +
				"case when cast(t1.login_os as VARCHAR) =  cast(t2.login_os as VARCHAR)  then '0' else '1' end as os_flag,\n" +
				"case when cast(t1.domain as VARCHAR) =  cast(t2.domain as VARCHAR)  then '0' else '1' end as domain_flag\n" +
				"from(\n" +
				"select account_id,account_ip,login_os,domain from sys_login_log where account_id = :id  ORDER BY create_datetime desc limit 1 OFFSET 0\n" +
				") t1 \n" +
				"LEFT JOIN (select account_id,account_ip,login_os,domain from sys_login_log where account_id = :id  ORDER BY create_datetime desc limit 1 OFFSET 1) t2 on t1.account_id = t2.account_id ");
		Map map = MixUtil.newHashMap();
		map.put("id", id);
		return super.selectSingleCamelMap(sb.toString(), map);
    }


	public Integer getMonthAfterLoginRecord(Date start,Date end, Long accountId, String oddOs) {
		StringBuilder sb = new StringBuilder("select count(l.id) from sys_login_log l left join sys_account a on a.id = l.account_id ");
		sb.append(" where account_id = :accountId and a.create_datetime < :end and l.create_datetime <:end and l.create_datetime >:start and login_os !=:oddOs");
		Map map = MixUtil.newHashMap();
		map.put("accountId", accountId);
		map.put("end", end);
		map.put("start", start);
		map.put("oddOs", oddOs);
		return super.queryForInt(sb.toString(),map);
	}

	public Integer getCountByIpOrOs(Long accountId, String ip, String os, String host,String oddOs) {
		StringBuilder sb = new StringBuilder("select count(id) from sys_login_log ");
		sb.append(" where account_id = :accountId and login_os!=:oddOs and (account_ip = :ip");
		Map map = MixUtil.newHashMap();
		map.put("accountId", accountId);
		map.put("ip", ip);
		map.put("oddOs", oddOs);
		if(Validator.isNotNull(os)){
			sb.append(" or login_os=:os ");
			map.put("os", os);
		}
		if(Validator.isNotNull(host)){
			sb.append(" or domain=:host ");
			map.put("host", host);
		}
		sb.append(" )");
		return super.queryForInt(sb.toString(),map);
	}
}
