package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import com.game.model.AgentLoginOtpEscape;

@Repository
public class AgentLoginOtpEscapeDao extends JdbcUtilImpl<AgentLoginOtpEscape> {

	public AgentLoginOtpEscape findOne(Long stationId, String account) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("account", account);
		return query21Model("select * from agent_login_otp_escape where account=:account and station_id=:stationId",
				map);
	}

	public Page<AgentLoginOtpEscape> getPage(Long stationId, String account) {
		Map<String, Object> paramMap = new HashMap<>();
		StringBuilder sql = new StringBuilder("select * from agent_login_otp_escape where 1=1");
		if (StringUtils.isNotEmpty(account)) {
			sql.append(" and account=:account");
			paramMap.put("account", account);
		}
		if (stationId != null && stationId > 0) {
			sql.append(" and station_id=:stationId");
			paramMap.put("stationId", stationId);
		}
		return paged2Obj(sql.toString(), paramMap);
	}

	public List<Map> getAgentUsers(Long stationId) {
		StringBuilder sql = new StringBuilder("select a.account,a.station_id,b.id as esc_id from sys_account a ");
		sql.append(" left join agent_login_otp_escape b on a.account=b.account and a.station_id=b.station_id");
		sql.append(" where a.flag_active >= 1 and (a.account_type=3 or a.account_type=2) and a.station_id=");
		sql.append(stationId);
		return selectCamelListMap(sql.toString());
	}

	public void delete(Long stationId, String account) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("account", account);
		update("delete from agent_login_otp_escape where account=:account and station_id=:stationId", map);
	}

}
