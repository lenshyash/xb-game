package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MemberActiveRecord;
import com.game.model.MemberActiveVirtualRecord;
import com.game.model.vo.ActiveRecordVo;

@Repository
public class MemberActiveVirtualRecordDao extends JdbcUtilImpl<MemberActiveVirtualRecord> {

	public Page getVirtualRecordPage(ActiveRecordVo arvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active_virtual_record");
		sql_sb.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();

		if (Validator.isNotNull(arvo.getActiveId())) {
			sql_sb.append(" AND active_id = :activeID");
			paramMap.put("activeID", arvo.getActiveId());
		}

		if (Validator.isNotNull(arvo.getRecordType())) {
			sql_sb.append(" AND award_type = :recordType");
			paramMap.put("recordType", arvo.getRecordType());
		}

		if (Validator.isNotNull(arvo.getStatus())) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", arvo.getStatus());
		}

		if (Validator.isNotNull(arvo.getAccountId())) {
			sql_sb.append(" AND account_id = :accountId");
			paramMap.put("accountId", arvo.getAccountId());
		}

		if (Validator.isNotNull(arvo.getStationId())) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", arvo.getStationId());
		}
		if (StringUtil.isNotEmpty(arvo.getAccount())) {
			sql_sb.append(" AND account = :account");
			paramMap.put("account", arvo.getAccount());
		}

//		if (StringUtil.isNotEmpty(arvo.getBegin())) {
//			sql_sb.append(" AND create_datetime >= :begin");
//			paramMap.put("begin", arvo.getBegin());
//		}
//
//		if (StringUtil.isNotEmpty(arvo.getEnd())) {
//			sql_sb.append(" AND create_datetime < :end");
//			paramMap.put("end", arvo.getEnd());
//		}

		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}
	
	public void updateVirtualRecord(MemberActiveVirtualRecord mavr) {
		StringBuilder sql_sb = new StringBuilder("");
		Map paramMap = MixUtil.newHashMap();
		sql_sb.append("update member_active_virtual_record set ");
		if (StringUtil.isNotEmpty(mavr.getAccount())) {
			sql_sb.append(" account = :account");
			paramMap.put("account", mavr.getAccount());
		}
		if (StringUtil.isNotEmpty(mavr.getProductName())) {
			sql_sb.append(" , product_name = :productName");
			paramMap.put("productName", mavr.getProductName());
		}
		if (StringUtil.isNotEmpty(mavr.getAwardType())) {
			sql_sb.append(" , award_type = :awardType");
			paramMap.put("awardType", mavr.getAwardType());
		}
		if (StringUtil.isNotEmpty(mavr.getAccountId())) {
			sql_sb.append(" , active_id = :activeId");
			paramMap.put("activeId", mavr.getAccountId());
		}
		sql_sb.append(" WHERE id = :id");
		paramMap.put("id", mavr.getId());
		super.update(sql_sb.toString(), paramMap);
	}
	
	public List<MemberActiveVirtualRecord> getVirtualRecordList(ActiveRecordVo arvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active_virtual_record");
		sql_sb.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();

		if (Validator.isNotNull(arvo.getRecordType())) {
			sql_sb.append(" AND award_type = :recordType");
			paramMap.put("recordType", arvo.getRecordType());
		}
		if(Validator.isNotNull(arvo.getStationId())) {
			sql_sb.append("AND station_id = :stationId");
			paramMap.put("stationId", arvo.getStationId());
		}
		if (arvo.getRecordTypes() != null && !arvo.getRecordTypes().isEmpty()) {
			sql_sb.append(" AND award_type IN(");
			for (Long type : arvo.getRecordTypes()) {
				sql_sb.append(type).append(",");
			}
			sql_sb.delete(sql_sb.length()-1, sql_sb.length());
			sql_sb.append(") ");
		}

		if (Validator.isNotNull(arvo.getStatus())) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", arvo.getStatus());
		}

		if (StringUtil.isNotEmpty(arvo.getOrder())) {
			sql_sb.append(" ORDER BY ").append(arvo.getOrder());
		}

		if (Validator.isNotNull(arvo.getLimit())) {
			sql_sb.append(" LIMIT ").append(arvo.getLimit());
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}
	public void delByActiveId(Long activeId) {
		Map<String, Object> map = new HashMap<>();
		map.put("activeId", activeId);
		update("DELETE FROM member_active_virtual_record WHERE active_id  = :activeId ",
				map);
	}
}
