package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import com.game.model.SysApiDeposit;

@Repository
public class SysApiDepositDao extends JdbcUtilImpl<SysApiDeposit> {

	public Page<SysApiDeposit> getPage(String account, Date beginDate, Date endDate, String orderId, Long stationId) {
		Map<String, Object> param = new HashMap<>();
		StringBuilder sb = new StringBuilder("select * from sys_api_deposit where station_id=:stationId");
		param.put("stationId", stationId);
		if (StringUtils.isNotEmpty(account)) {
			param.put("account", account.toLowerCase());
			sb.append(" and lower(account) = :account");
		}
		if (beginDate != null) {
			param.put("beginDate", beginDate);
			sb.append(" and create_datetime>=:beginDate");
		}
		if (endDate != null) {
			param.put("endDate", endDate);
			sb.append(" and create_datetime<:endDate");
		}
		if (StringUtils.isNotEmpty(orderId)) {
			param.put("orderId", orderId);
			sb.append(" and order_id=:orderId");
		}
		sb.append(" order by create_datetime desc");
		return paged2Obj(sb.toString(), param);
	}

}
