package com.game.dao.pay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentPayment;

@Repository
public class AgentPaymentDao extends JdbcUtilImpl<AgentPayment> {

	public Page getAgentPaymentPage(String name, Long status, Long stationId) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.*,p.name AS pay_name,p.company AS pay_com,p.icon_css");
		sql_sb.append(" FROM agent_payment a LEFT JOIN sys_pay_platform p ON a.pay_platform_id = p.id");
		sql_sb.append(" WHERE 1=1 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(name)) {
			paramMap.put("name", "%" + name.toLowerCase() + "%");
			sql_sb.append(" AND p.name LIKE :name");
		}

		if (Validator.isNotNull(status)) {
			paramMap.put("status", status);
			sql_sb.append(" AND a.status=:status");
		}

		if (Validator.isNotNull(stationId)) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND a.station_id=:stationId");
		}

		sql_sb.append(" ORDER BY a.id DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List getPayments(Long stationId) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.*,p.name AS pay_name,p.company AS pay_com,p.icon_css");
		sql_sb.append(" FROM agent_payment a LEFT JOIN sys_pay_platform p ON a.pay_platform_id = p.id");
		sql_sb.append(" WHERE a.status = 2 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(stationId)) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND a.station_id=:stationId");
		}

		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public int updateStatus(Integer status, Long id, Long stationId) {
		return super.update("UPDATE agent_payment SET status = :status WHERE id =:id AND station_id =:stationId",
				MixUtil.newHashMap("status", status, "id", id, "stationId", stationId));
	}

	public int deletePayment(Long id, Long stationId) {
		return super.update("DELETE FROM agent_payment WHERE id = :id AND station_id =:stationId",
				MixUtil.newHashMap("id", id, "stationId", stationId));
	}
}
