package com.game.dao;

import com.game.model.AgentMenu;
import com.game.model.AgentUserGroup;
import com.game.model.AppUpdate;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author johnson
 * app check update dao
 */
@Repository
public class AppCheckUpdateDao extends JdbcUtilImpl<AppUpdate> {

    public List<AppUpdate> getAppUpdates() {
        String sql = "SELECT * FROM app_update WHERE status <> " + AppUpdate.STATUS_ENABLED;
        List<AppUpdate> appUpdates = super.query2Model(sql);
        return appUpdates;
    }

    public List<AppUpdate> getLastUpdateInfo(String version) {
        String sql = "SELECT * FROM app_update WHERE version > :version AND status = " + AppUpdate.STATUS_ENABLED;
        List<AppUpdate> updates = super.query2Model(sql, MixUtil.newHashMap("version", version));
        return updates;
    }

    public int deleteAppUpdate(String version) {
        Map<String, Object> map = new HashMap<>();
        map.put("version", version);
        return super.update(
                "delete from app_update where version=:version", map);
    }
    public AppUpdate getLastAppVerByFlag(String flag,Long stationId,String domain,String type) {
//        String sql = "SELECT * FROM app_update WHERE flag = :flag AND station_id=:stationId AND status = " + AppUpdate.STATUS_ENABLED +" order by create_time desc limit 1";
        StringBuilder sql = new StringBuilder("SELECT * FROM app_update WHERE flag = :flag AND station_id=:stationId AND status = " + AppUpdate.STATUS_ENABLED);
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("flag", flag);
		if (StringUtil.isNotEmpty(domain)) {
			sql.append(" and domain =:domain");
			map.put("domain",domain);
		}
		//考虑之前存储  如果type是空 那么查询非完美版数据
		if(StringUtil.isEmpty(type)) {
			sql.append(" and(type!=:type or type is null)");
			map.put("type", AppUpdate.PERFECT);
		}else {
			//不为空查询type
			sql.append(" and type=:type");
			map.put("type", type);
		}
		sql.append(" order by create_time desc limit 1");
        
        AppUpdate app = super.query21Model(sql.toString(),map);
        return app;
    }


}
