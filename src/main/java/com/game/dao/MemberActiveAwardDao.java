package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberActiveAward;

@Repository
public class MemberActiveAwardDao extends JdbcUtilImpl<MemberActiveAward> {

	public void delAllByActiveId(Long activeId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("DELETE FROM member_active_award WHERE active_id = :activeId");
		super.update(sql_sb.toString(), MixUtil.newHashMap("activeId", activeId));
	}

	public List<MemberActiveAward> getAwardByActiveId(Long activeId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM member_active_award WHERE active_id = :activeId ORDER BY award_index");
		Map paramMap = MixUtil.newHashMap("activeId", activeId);
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public Integer getCountByUsingProduct(Long productId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT COUNT(1)");
		sql_sb.append(" FROM member_active_award");
		sql_sb.append(" WHERE product_id=:productId");
		Map paramMap = MixUtil.newHashMap("productId", productId);
		return super.queryForInt(sql_sb.toString(), paramMap);
	}
}
