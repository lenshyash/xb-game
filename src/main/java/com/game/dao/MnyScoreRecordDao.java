package com.game.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MnyScoreRecord;
import com.game.model.vo.MnyScoreReocrdVo;

@Repository
public class MnyScoreRecordDao extends JdbcUtilImpl<MnyScoreRecord> {

	public BigDecimal[] updateScore(long accountId, BigDecimal optScore) throws SQLException {
		String sql = "SELECT optscore(" + accountId + "," + optScore + ")";
		return (BigDecimal[]) (super.queryForArray(sql).getArray());
	}

	public Page getScoreRecordPage(MnyScoreReocrdVo msvo) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT r.*,a.account");
		sql_sb.append(" FROM mny_score_record r INNER JOIN sys_account a ON r.account_id = a.id");
		sql_sb.append(" WHERE a.flag_active >=1 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(msvo.getAccount())) {
			paramMap.put("account", msvo.getAccount().toLowerCase());
			sql_sb.append(" AND lower(a.account) = :account");
		}

		if (Validator.isNotNull(msvo.getStationId())) {
			paramMap.put("stationId", msvo.getStationId());
			sql_sb.append(" AND a.station_id=:stationId");
		}

		if (Validator.isNotNull(msvo.getType())) {
			paramMap.put("type", msvo.getType());
			sql_sb.append(" AND r.type=:type");
		}

		if (Validator.isNotNull(msvo.getAccountId())) {
			paramMap.put("accountId", msvo.getAccountId());
			sql_sb.append(" AND a.id=:accountId");
		}

		if (StringUtil.isNotEmpty(msvo.getParents())) {
			paramMap.put("parents", msvo.getParents() + "%");
			if (msvo.getSearchSelf() != null && msvo.getSearchSelf()) {
				paramMap.put("curUserId", msvo.getSelfId());
				sql_sb.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_sb.append(" AND a.parents LIKE :parents");
			}
		}

		if (StringUtil.isNotEmpty(msvo.getBegin())) {
			paramMap.put("begin", msvo.getBegin());
			sql_sb.append(" AND r.create_datetime >=:begin");
		}

		if (StringUtil.isNotEmpty(msvo.getEnd())) {
			paramMap.put("end", msvo.getEnd());
			sql_sb.append(" AND r.create_datetime <=:end");
		}
		sql_sb.append(" ORDER BY r.create_datetime DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

}
