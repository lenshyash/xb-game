package com.game.dao;

import java.util.Date;
import java.util.HashMap;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysMessage;
import com.game.model.SysUserMessage;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SysUserMessageDao extends JdbcUtilImpl<SysUserMessage> {

	public void delByMessageId(Long messageId) {
		super.update("DELETE FROM sys_user_message WHERE message_id = :messageId",
				MixUtil.newHashMap("messageId", messageId));
	}

	public Integer getMessageCount() {
		return super.queryForInt(
				"SELECT Count(1) FROM sys_message m LEFT JOIN sys_user_message u ON u.message_id = m.id AND u.account_id = :accountId"
				+ " WHERE m.station_id = :stationId AND COALESCE(u.status,1) != :status"
				+ " AND m.create_datetime > :createDatetime"
				+ " AND CASE WHEN m.type = 2 THEN COALESCE(u.account_id,:accountId) =:accountId "
				+ " WHEN m.type = 1 THEN u.account_id=:accountId END AND COALESCE(u.status,1) != 3",
				MixUtil.newHashMap("stationId",StationUtil.getStationId(),"createDatetime",UserUtil.getCreateDatetime(), "status", SysMessage.TYPE_ALL, "accountId", UserUtil.getUserId()));
	}

	public void readMessage(Long userMessageId) {
		super.update(
				"UPDATE sys_user_message SET status = " + SysUserMessage.STATUS_READ
						+ " WHERE id = :userMessageId AND status = " + SysUserMessage.STATUS_UNREAD,
				MixUtil.newHashMap("userMessageId", userMessageId));
	}
	
	public void deleteMessage(Long userMessageId) {
		super.update(
				"UPDATE sys_user_message SET status = 3"
						+ " WHERE message_id = :userMessageId ",
				MixUtil.newHashMap("userMessageId", userMessageId));
	}
	
	public void delMessage(Long userMessageId) {
		super.update(
				"UPDATE sys_user_message SET status = 3"
						+ " WHERE id = :userMessageId AND account_id = :accountId" ,
						MixUtil.newHashMap("userMessageId",userMessageId, "accountId", UserUtil.getUserId()));
	}
	
	public Integer getCount(Long messageId){
		return super.queryForInt(
				"SELECT Count(1) FROM sys_user_message "
				+ " WHERE message_id = :messageId AND account_id = :accountId",
				MixUtil.newHashMap("messageId",messageId, "accountId", UserUtil.getUserId()));
	}
	
	public Long getUserMsgId(Long messageId){
		return super.queryForLong(
				"SELECT id FROM sys_user_message "
				+ " WHERE message_id = :messageId AND account_id = :accountId AND status = :status",
				MixUtil.newHashMap("messageId",messageId, "accountId", UserUtil.getUserId(),"status",SysUserMessage.STATUS_UNREAD));
	}
	
	public void deleteSysUserMessageByDate(Date createDatetime) {
		StringBuilder builder = new StringBuilder();
		builder.append("delete from sys_user_message where message_id in (select id from sys_message where create_datetime<=:createDatetime)");
		HashMap<Object,Object> map = new HashMap<>();
		map.put("createDatetime", createDatetime);
		
		super.update(builder.toString(), map);
	}
}
