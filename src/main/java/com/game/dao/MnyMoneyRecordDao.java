package com.game.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.model.MnyComRecord;
import org.jay.frame.FrameProperites;
import org.jay.frame.exception.JayFrameException;
import org.jay.frame.jdbc.*;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.game.model.MnyMoneyRecord;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MemberDataVo;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.model.vo.ReportParamVo;

@Repository
public class MnyMoneyRecordDao extends JdbcUtilImpl<MnyMoneyRecord> {

	public long getId() {
		String sql = "select nextval('mny_record_id_seq');";
		return super.queryForLong(sql);
	}

	public synchronized Page<Map> getMemMnyRdPage(MnyMoneyRecordVo moneyVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT m.*,a.account_type,a.report_type FROM mny_money_record m LEFT JOIN sys_account a ON m.account_id = a.id where 1 = 1");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String account = StringUtil.trim2Empty(moneyVo.getAccount());
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(m.account) = :account");
		}
		
		if (Validator.isNotNull(moneyVo.getAccountType())) {
			sql_sb.append(" AND a.account_type = :accountType");
			paramMap.put("accountType", moneyVo.getAccountType());
		}
		if (Validator.isNotNull(moneyVo.getReportType())) {
			sql_sb.append(" AND a.report_type = :reportType");
			paramMap.put("reportType", moneyVo.getReportType());
		}
		if (Validator.isNotNull(moneyVo.getStationId())) {
			paramMap.put("stationId", moneyVo.getStationId());
			sql_sb.append(" AND m.station_id=:stationId");
		}
		if (Validator.isNotNull(moneyVo.getOperatorName())) {
			paramMap.put("operatorName", moneyVo.getOperatorName());
			sql_sb.append(" AND m.operator_name=:operatorName");
		}
		// TODO 过滤返点反水回滚 18/1/2
//		if (moneyVo.isShowRollBack()) {
//			sql_sb.append(" AND  m.type != 10 AND m.type != 12 AND m.type != 14");
//		}

		// 过滤掉人工加扣款
		if (moneyVo.isShowHandMoney()) {
			sql_sb.append(" AND m.type!=2");
		} else {
			if (Validator.isNotNull(moneyVo.getType())) {
				paramMap.put("type", moneyVo.getType());
				sql_sb.append(" AND m.type=:type");
			}
		}
		if (moneyVo.isShowChangeOrder()) {
			sql_sb.append(" AND m.type not in (171,172)");
		}
		if (Validator.isNotNull(moneyVo.getAccountId())) {
			paramMap.put("accountId", moneyVo.getAccountId());
			sql_sb.append(" AND m.account_id=:accountId");
		}

		if (StringUtil.isNotEmpty(moneyVo.getParents())) {
			paramMap.put("parents", moneyVo.getParents() + "%");
			if (moneyVo.getSearchSelf() != null && moneyVo.getSearchSelf()) {
				paramMap.put("curUserId", moneyVo.getSelfId());
				sql_sb.append(" AND (m.parents LIKE :parents OR m.account_id = :curUserId)");
			} else {
				sql_sb.append(" AND m.parents LIKE :parents");
			}
		}
		
		if (StringUtil.isNotEmpty(moneyVo.getRemark())) {
			sql_sb.append(" AND m.remark LIKE :remark");
			paramMap.put("remark","%"+moneyVo.getRemark()+ "%");
		}

		if (StringUtil.isNotEmpty(moneyVo.getBegin())) {
			paramMap.put("begin", moneyVo.getBegin());
			sql_sb.append(" AND m.create_datetime >=:begin");
		}
		if (StringUtil.isNotEmpty(moneyVo.getType()) && moneyVo.getType() != 0) {
			paramMap.put("type", moneyVo.getType());
			sql_sb.append(" AND m.type = :type");
		}
		if (StringUtil.isNotEmpty(moneyVo.getEnd())) {
			paramMap.put("end", moneyVo.getEnd());
			sql_sb.append(" AND m.create_datetime <=:end");
		}
		if (StringUtil.isNotEmpty(moneyVo.getOrderId())) {
			paramMap.put("orderId", moneyVo.getOrderId());
			sql_sb.append(" AND m.order_Id = :orderId");
		}
		sql_sb.append(" ORDER BY m.create_datetime DESC");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public MnyMoneyRecord getByOrderId(String orderId, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM mny_money_record where ");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		sql_sb.append(" AND lower(order_id) = :orderId");
		paramMap.put("stationId", stationId);
		sql_sb.append(" AND lower(station_id) = :stationId");
		return super.query21Model(sql_sb.toString(), paramMap);
	}
	public MnyMoneyRecord getByOrderIdByLottery(String orderId, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM mny_money_record where ");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		sql_sb.append(" order_id = :orderId");
		paramMap.put("stationId", stationId);
		sql_sb.append(" AND station_id = :stationId");
		sql_sb.append(" AND type in(140,130)");

		return super.query21Model(sql_sb.toString(), paramMap);
	}
	public List<Map> getReport(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT SUM (money) AS total,type");
		sql_sb.append(" FROM mny_money_record r INNER JOIN sys_account a ON a.id = r.account_id");
		sql_sb.append(" WHERE a.flag_active >=1 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Long> types = paramVo.getTypes();
		Long stationId = paramVo.getStationId();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Boolean searchSelf = paramVo.getSearchSelf();

		if (Validator.isNotNull(stationId)) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND r.station_id =:stationId");
		}

		if (types != null && types.size() > 0) {
			sql_sb.append(" AND r.type IN (");
			for (int i = 0; i < types.size(); i++) {
				if (i > 0) {
					sql_sb.append(",");
				}
				sql_sb.append(":type" + i);
				paramMap.put("type" + i, types.get(i));
			}
			sql_sb.append(")");
		}

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(a.account) = :account");
		}

		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (StringUtil.isNotEmpty(searchSelf) && searchSelf) {
				sql_sb.append(" AND (a.id = :selfId OR a.parents LIKE :children)");
				paramMap.put("selfId", paramVo.getSelfId());
			} else {
				sql_sb.append(" AND a.parents LIKE :children");
			}
		}

		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND r.create_datetime >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_sb.append(" AND r.create_datetime <=:end");
		}
		sql_sb.append(" GROUP BY r.type");

		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public synchronized Page getFinanceReport(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select * from (SELECT r.account_id,sum(CASE WHEN r.type IN(")
				.append(MoneyRecordType.DEPOSIT_ONLINE_BANK.getType()).append(",")
				.append(MoneyRecordType.DEPOSIT_ONLINE_FAST.getType()).append(",")
				.append(MoneyRecordType.DEPOSIT_ONLINE_THIRD.getType())
				.append(") THEN money ELSE 0 END) AS deposit_total,");
		sql_sb.append(" sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_ARTIFICIAL.getType())
				.append(" THEN money ELSE 0 END) AS manual_deposit_total,");
		sql_sb.append(" sum(CASE WHEN r.type = ").append(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType())
				.append(" THEN money ELSE 0 END) AS manual_withdraw_total,");
		sql_sb.append(" sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType())
				.append(" THEN money ELSE 0 END) AS deposit_gift_money,");
		sql_sb.append(" sum(CASE WHEN r.type IN( ").append(MoneyRecordType.WITHDRAW_ONLINE.getType()).append(",")
				.append(MoneyRecordType.WITHDRAW_ONLINE_FAILED.getType())
				.append(") THEN money ELSE 0 END) AS withdraw_total,");

		sql_sb.append(" sum(CASE WHEN r.type IN(").append(MoneyRecordType.MEMBER_ROLL_BACK_ADD.getType()).append(",")
				.append(MoneyRecordType.MEMBER_ROLL_BACK_SUB.getType())
				.append(") THEN money ELSE 0 END) AS rebate_total,");

		sql_sb.append(" sum(CASE WHEN r.type IN(").append(MoneyRecordType.PROXY_REBATE_ADD.getType()).append(",")
				.append(MoneyRecordType.PROXY_REBATE_SUB.getType()).append(",")
				.append(MoneyRecordType.PROXY_MULTI_REBATE_ADD.getType()).append(",")
				.append(MoneyRecordType.PROXY_MULTI_REBATE_SUB.getType())
				.append(") THEN money ELSE 0 END) AS rebate_agent_total");

		Map<String, Object> paramMap = new HashMap<>();
		Long stationId = paramVo.getStationId();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		paramMap.put("stationId", stationId);
		sql_sb.append(" FROM mny_money_record r WHERE r.station_id =:stationId");
		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND r.create_datetime >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_sb.append(" AND r.create_datetime <=:end");
		}
		sql_sb.append(" GROUP BY r.account_id)rr");
		sql_sb.append(" INNER JOIN (select a.account,a.account_type,a.id from sys_account a ");
		sql_sb.append(" WHERE a.flag_active >=1 and a.station_id =:stationId");

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(a.account) = :account");
		}

		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				sql_sb.append(" AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				sql_sb.append(" AND a.parents LIKE :children");
			}
		}
		sql_sb.append(") aa ON aa.id = rr.account_id");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	/**
	 * 根据站点和时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(
				"delete from mny_money_record where station_id=:stationId and create_datetime <= :createTime", map);
	}

	public Map getMemberDataVo(Long accountId, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sb = new StringBuilder();
		sb.append("select sum(CASE WHEN type = 1 THEN money ELSE 0 END) AS manual_deposit_total,");
		sb.append("sum(CASE WHEN type = 2 THEN -money ELSE 0 END) AS manual_withdraw_total,");
		sb.append("sum(CASE WHEN type =80 THEN money ELSE 0 END) AS give_total,");
		sb.append("sum(CASE WHEN type =81 THEN -money ELSE 0 END) AS withdraw_give_total,");
		sb.append("sum(CASE WHEN type =79 THEN money ELSE 0 END) AS give_register_total,");
		sb.append("sum(CASE WHEN type IN(9,10) THEN money ELSE 0 END) AS rebate_total,");
		sb.append("sum(CASE WHEN type IN(12,11) THEN money ELSE 0 END) AS rebate_agent_total,");
		sb.append("sum(CASE WHEN type=18 THEN money ELSE 0 END) AS active_award_total,");
		sb.append("sum(CASE WHEN type IN(201,203) THEN -money ELSE 0 END) AS sport_total,");
		sb.append("sum(CASE WHEN type IN(202,204) THEN money ELSE 0 END) AS sport_award");
		sb.append(" from mny_money_record where station_id=:stationId");
		sb.append(" and create_datetime>=:beginDate");
		sb.append(" and create_datetime<=:endDate");
		sb.append(" and account_id=:accountId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("accountId", accountId);
		return selectSingleCamelMap(sb.toString(), map);
	}

	public int snyMoneyRecord(Long stationId, Date end, Date begin) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE mny_money_record m SET parents = a.parents");
		sql_sb.append(" FROM sys_account a ");
		sql_sb.append(" WHERE a.id = m.account_id and a.station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtil.isNotEmpty(begin)) {
			sql_sb.append(" AND m.create_datetime >= :startTime");
			map.put("startTime", begin);
		}
		if (StringUtil.isNotEmpty(end)) {
			sql_sb.append(" AND m.create_datetime <= :end");
			map.put("end", end);
		}
		return super.update(sql_sb.toString(), map);
		
	}
	
	public synchronized Page<Map> getAccountxiMaPage(MnyMoneyRecordVo moneyVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT m.*,a.account_type FROM mny_money_record m LEFT JOIN sys_account a ON m.account_id = a.id where 1 = 1");
		sql_sb.append(" and (m.type= 15 or m.type= 16 ) ");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String account = StringUtil.trim2Empty(moneyVo.getAccount());
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(m.account) = :account");
		}
		if (Validator.isNotNull(moneyVo.getStationId())) {
			paramMap.put("stationId", moneyVo.getStationId());
			sql_sb.append(" AND m.station_id=:stationId");
		}
		if(StringUtil.isNotEmpty(moneyVo.getRemark())) {
			sql_sb.append(" AND m.remark like :remark");
			paramMap.put("remark", "%"+moneyVo.getRemark()+"%");
		}
		sql_sb.append(" order by create_datetime desc ");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
		
	}

	public int checkAccountWarningRecord(Date start, Date end, Long accountId) {
		StringBuilder sb = new StringBuilder();
		sb.append(" select count(id) from mny_money_record mr where mr.account_id =:accountId ");
		sb.append(" and create_datetime >=:start and create_datetime<=:end");
		sb.append(" and type in(15,4,16,130,201,140,998,996) ");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("accountId",accountId);
		paramMap.put("start",start);
		paramMap.put("end",end);
		return super.queryForInt(sb.toString(), paramMap);
	}


	public MnyMoneyRecord insertOne(MnyMoneyRecord d) {
		try {
			JdbcModel model = get();
			final JdbcColumn primaryCol = model.getPrimaryCol();
			primaryCol.setColumnName("id");
			final String sql;
			if (primaryCol != null && primaryCol.getGenerator() == 1) {
				ListParameter parameterMaker = new ListParameter();
				SqlMaker.getAddSql(model, parameterMaker);
				sql = parameterMaker.getSql();
				final List paramList = parameterMaker.toParameters(d, 2);
				JdbcDAOImpl.getLogInfo(sql, paramList);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				super.getJdbcOperations().update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = null;
						if (FrameProperites.DB_TYPE == DBType.POSTGRESQL) {
							String primaryColumn = primaryCol.getColumnName();
							ps = conn.prepareStatement(sql, new String[]{primaryColumn});
						} else {
							ps = conn.prepareStatement(sql, 1);
						}

						for(int i = 0; i < paramList.size(); ++i) {
							Object val = paramList.get(i);
							setSqlParameter(ps, i + 1, val);
						}
						return ps;
					}
				}, keyHolder);
				Integer id = (Integer)keyHolder.getKeyList().get(0).get("id");
				d.setId(id.longValue());
			} else {
				if (primaryCol != null && primaryCol.getGenerator() == 3) {
					primaryCol.setValue(d, this.getSequence(primaryCol.getSequence()));
				}
				MapParameter parameterMaker = new MapParameter();
				SqlMaker.getAddSql(model, parameterMaker);
				sql = parameterMaker.getSql();
				Map params = parameterMaker.toParameters(d, 2);
				super.update(sql, params);
			}
			return d;
		}catch (Exception e){}
		return null;
	}

	private JdbcModel get() {
		Class c = this.getGenericType(0);
		JdbcModel jdbcModel = JdbcModelSet.get(c);
		if (jdbcModel == null) {
			throw new JayFrameException(10004, new String[]{c.getName()});
		} else {
			return jdbcModel;
		}
	}
	private void setSqlParameter(PreparedStatement ps, int i, Object val) throws SQLException {
		if (val != null && FrameProperites.DB_TYPE == DBType.POSTGRESQL && val.getClass() == Date.class) {
			ps.setTimestamp(i, new Timestamp(((Date)val).getTime()));
		} else {
			ps.setObject(i, val);
		}

	}
}
