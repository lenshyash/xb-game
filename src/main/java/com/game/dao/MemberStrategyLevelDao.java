package com.game.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.game.model.AgentDepositLevel;

@Repository
public class MemberStrategyLevelDao extends JdbcUtilImpl<AgentDepositLevel> {

	public List<AgentDepositLevel> findByDepositId(Long depositId, Long stationId, int type) {
		return query2Model("select * from agent_deposit_level where deposit_id=" + depositId + " and station_id="
				+ stationId + " and type=" + type);
	}

	public List<Long> getLevelIdsByDepositId(Long depositId, Long stationId, int type) {
		Map<String, Object> map = new HashMap<>();
		map.put("depositId", depositId);
		map.put("stationId", stationId);
		map.put("type", type);
		return query2Obj(
				"select member_level_id from agent_deposit_level where deposit_id=:depositId and station_id=:stationId and type=:type",
				map, new RowMapper<Long>() {
					@Override
					public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getLong(1);
					}
				});
	}

	public void batchDelete(final Long depositId, final Long stationId, final int type, final List<Long> levelIdList) {
		StringBuilder sql = new StringBuilder("delete from agent_deposit_level WHERE");
		sql.append(" deposit_id=? and station_id=? and type=? and member_level_id=?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				int k = 1;
				ps.setLong(k++, depositId);
				ps.setLong(k++, stationId);
				ps.setInt(k++, type);
				ps.setLong(k++, levelIdList.get(i));
			}

			@Override
			public int getBatchSize() {
				return levelIdList.size();
			}
		});
	}

	public void deleteByDeposiId(Long depositId, Long stationId, int type) {
		Map<String, Object> map = new HashMap<>();
		map.put("depositId", depositId);
		map.put("stationId", stationId);
		map.put("type", type);
		update("delete from agent_deposit_level WHERE deposit_id=:depositId and station_id=:stationId and type=:type",
				map);
	}

}
