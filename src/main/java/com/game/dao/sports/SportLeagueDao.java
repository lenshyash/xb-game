package com.game.dao.sports;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportLeague;

@Repository
public class SportLeagueDao extends JdbcUtilImpl<SportLeague>{
	
	public SportLeague getLeague(int sportType,String leagueName){
		String sql = "select * from sport_league where sport_type_id = :sportType and  name = :leagueName";
		return super.query21Model(sql, MixUtil.newHashMap("sportType",sportType,"leagueName",leagueName));
	}
}
