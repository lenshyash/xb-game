package com.game.dao.sports;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportNotice;
import com.game.util.DateUtil;

@Repository
public class SportNoticeDao extends JdbcUtilImpl<SportNotice>{
	
	public boolean isExsist(Date date,String md5key){
		String sql = "select count(1) from sport_notice where notice_date = :date and md5_key = :md5key";
		return super.queryForInt(sql,MixUtil.newHashMap("date",date,"md5key",md5key)) > 0;
	}
	
	public List<SportNotice> getTenNotice(){
		String sql = "select * from sport_notice order by notice_date desc limit 10";
		return super.query2Model(sql);
	}
	
	public List<SportNotice> getNoticeByDate(int type){
		Map paramMap = new HashMap<>(); 
		StringBuffer sb = new StringBuffer(500);
		sb.append("select * from sport_notice where 1 = 1 ");
		Date now = new Date();
		if(type == 1){ //今天
			sb.append(" and notice_date = :startDate ");
			paramMap.put("startDate",DateUtil.dayFirstTime(now, 0));
		}else if(type == 2){//昨日
			sb.append(" and notice_date = :startDate ");
			paramMap.put("startDate",DateUtil.dayFirstTime(now, -1));
		}else if(type == 3){//昨日之前
			sb.append(" and notice_date < :endDate ");
			Date endDate= DateUtil.dayFirstTime(now, -1);
			paramMap.put("endDate",endDate);
		}
		
		sb.append(" order by notice_date desc limit 100 ");
		return super.query2Model(sb.toString(),paramMap);
	}
}
