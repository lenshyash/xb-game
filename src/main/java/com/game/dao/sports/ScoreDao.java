package com.game.dao.sports;

import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;

import com.game.model.sports.Score;
import com.game.model.sports.SportType;


public abstract class ScoreDao<T extends Score> extends JdbcUtilImpl<T>{
	
	public T getScore(SportType st,long matchId){
		Map paramMap = MixUtil.newHashMap("matchId",matchId,"st",st.getType());
		String sql = " select * from bfw_match_result where match_id = :matchId and sport_type = :st";
		T score = super.query21Model(sql, paramMap);
		return score;
	}
	
	
}
