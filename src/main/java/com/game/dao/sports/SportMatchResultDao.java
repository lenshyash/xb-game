package com.game.dao.sports;

import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportMatchResult;
import com.game.util.ComputerUtil;
import com.game.util.StationUtil;

@Repository
public class SportMatchResultDao extends JdbcUtilImpl<SportMatchResult>{
	
	public SportMatchResult getUnlockResult(){
		String sql = "select * from sport_match_result "
				+ " where balance = "+SportMatchResult.BALANCE_UNDO+" limit 1";
		return super.query21Model(sql);
	}
	
	public boolean lockResult(long gid,long balance){
		String macCode = "[IP]: "+ComputerUtil.getIP() + "[NAME]:"+ComputerUtil.getName();
		String sql = " update sport_match_result set balance = " + SportMatchResult.BALANCE_DOING+", "
				+ "lock_mac_name = :macCode,"
				+ "lock_time = now() "
				+ " where balance = :balance and gid = :gid ";
		return super.update(sql, MixUtil.newHashMap("gid",gid,"macCode",macCode,"balance",balance)) > 0;
	}
	
	public void updateLockResultStatus(long gid,long status){
		String sql = " update sport_match_result set balance =  :status "
		+ " where balance = "+SportMatchResult.BALANCE_DOING +" and gid = :gid ";
		super.update(sql, MixUtil.newHashMap("gid",gid,"status",status));
	}
	
	public SportMatchResult getByGid(long gid){
		String sql = "select * from sport_match_result where gid = :gid ";
		return super.query21Model(sql, MixUtil.newHashMap("gid",gid));
	}
	
	public List<SportMatchResult> getResultsByDate(int st,Date startDate){
		Date endDate = new Date(startDate.getTime() +  24 * 60 * 60 * 1000);
		StringBuffer sql = new StringBuffer("select * from sport_match_result "
				+ " where start_time > :startDate "
				+ " and start_time < :endDate "
				+ " and sport_type = :sportType ");
	   if(StationUtil.isMobileStation()){
		   sql.append(" order by league,start_time ");
	   }else{
		   sql.append(" order by start_time,league ");
	   }
		return super.query2Model(sql.toString(),MixUtil.newHashMap("startDate",startDate,"endDate",endDate,"sportType",st));
	}
}
