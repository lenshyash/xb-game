package com.game.dao.sports;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportMatchResultHand;

@Repository
public class SportMatchResultHandDao extends JdbcUtilImpl<SportMatchResultHand> {
	
	public SportMatchResultHand getResult(long stationId,long matchId){
		String sql = "select * from "+super.getTableName()+" where "
				+ " station_id = :stationId and match_id = :matchId ";
		return super.query21Model(sql,MixUtil.newHashMap("stationId",stationId,"matchId",matchId));
	}

}
