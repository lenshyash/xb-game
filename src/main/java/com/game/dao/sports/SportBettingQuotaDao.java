package com.game.dao.sports;

import java.util.Date;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportBettingQuota;

@Repository
public class SportBettingQuotaDao extends JdbcUtilImpl<SportBettingQuota>{
	
	public boolean checkBettingQuota(SportBettingQuota quota,int limitMoney){
		String sql = " INSERT INTO sport_betting_quota as a "
		+ " (member_id,gid,station_id,betting_money,create_datetime,betting_count) "
		+ " VALUES (:memberId, :gid, :stationId, :bettingMoney,:now,1) "
		+ " ON CONFLICT (member_id, gid) DO UPDATE  "
		+ " SET betting_count = a.betting_count + 1,  "
		+ "  betting_money = a.betting_money + :bettingMoney, "
		+ "  modify_datetime = :now "  
		+ " where a.betting_money <= (:limitMoney - :bettingMoney) returning a.betting_money ";     
		return super.queryForBigDecimal(sql,MixUtil.newHashMap(
				"memberId",quota.getMemberId(),
				"gid",quota.getGid(),
				"stationId",quota.getStationId(),
				"bettingMoney",quota.getBettingMoney(),
				"limitMoney",limitMoney,
				"now",new Date()
		)) != null;
	}
}
