package com.game.dao.sports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.model.sports.SportMatch;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.sports.SportBettingOrder;
import com.game.model.vo.MobileSportRecordParameter;
import com.game.model.vo.SearchRecordParam;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SportBettingOrderDao extends JdbcUtilImpl<SportBettingOrder> {

	public Page getPage(SearchRecordParam params, long stationId) {
		Map param = MixUtil.newHashMap("stationId", stationId);
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(" select a.* from sport_betting a ");
		boolean isDaili = StationUtil.isDailiStation();
		boolean isAgent = StationUtil.isAgentStation();
		
		if (isDaili) {
			sql_sb.append(" left join sys_account b on(b.id = a.member_id) ");
		}
		//代理页面
		if (isAgent && StringUtil.isNotEmpty(params.getAgentName())) {
			sql_sb.append(" left join sys_account b on(b.id = a.member_id) ");
		}
		sql_sb.append(" where a.station_id = :stationId ");
		if (isDaili) {
			sql_sb.append(" AND b.parents LIKE :parent");
			param.put("parent", UserUtil.getChildren() + "%");
		}
		//代理页面
		if (isAgent && StringUtil.isNotEmpty(params.getAgentName())) {
			sql_sb.append(" AND (b.parents LIKE :parent or b.account = :account )");
			param.put("account", params.getAgentName());
			param.put("parent", params.getChildren() + "%");
		}
		if (params != null) {
			if (Validator.isNotNull(params.getMemberId())) {
				sql_sb.append(" AND a.member_id = :memberId ");
				param.put("memberId", params.getMemberId());
			}

			if (Validator.isNotNull(params.getBegin())) {
				
				if(StringUtil.equals(SearchRecordParam.DATETYPE_MATCHDATE, params.getDateType())){
					sql_sb.append(" AND a.account_datetime > :begin ");
				}else{
					sql_sb.append(" AND a.betting_date > :begin ");
				}
				
				param.put("begin", params.getBegin());
			}

			if (Validator.isNotNull(params.getEnd())) {
				if(StringUtil.equals(SearchRecordParam.DATETYPE_MATCHDATE, params.getDateType())){
					sql_sb.append(" AND a.account_datetime < :end ");
				}else{
					sql_sb.append(" AND a.betting_date < :end ");
				}
				//sql_sb.append(" AND a.betting_date < :end ");
				param.put("end", params.getEnd());
			}

			if (Validator.isNotNull(params.getSportType())) {
				sql_sb.append(" AND a.sport_type = :type ");
				param.put("type", params.getSportType());
			}

			if (Validator.isNotNull(params.getBettingCode())) {
				sql_sb.append(" AND a.betting_code = :bettingCode ");
				param.put("bettingCode", params.getBettingCode());
			}

			if (Validator.isNotNull(params.getMemberAccount())) {
				sql_sb.append(" AND lower(a.member_name) = :account ");
				param.put("account", params.getMemberAccount().toLowerCase());
			}

			if (Validator.isNotNull(params.getHomeTeam())) {
				sql_sb.append(" AND a.home_team like :home ");
				param.put("home", "%" + params.getHomeTeam() + "%");
			}

			if (Validator.isNotNull(params.getGuestTeam())) {
				sql_sb.append(" AND a.guest_team like :guest ");
				param.put("guest", "%" + params.getGuestTeam() + "%");
			}
			Long balanceStatus = params.getBalanceStatus();
			if (Validator.isNotNull(balanceStatus)) {
				int bs = balanceStatus.intValue();
				if (bs == 1) {
					sql_sb.append(" AND a.balance = " + SportBettingOrder.BALANCE_UNDO + " ");
				} else if (bs == 2) {
					sql_sb.append(" AND (a.balance = " + SportBettingOrder.BALANCE_DONE + " OR a.balance = "
							+ SportBettingOrder.BALANCE_BFW_DONE + " OR a.balance = "
							+ SportBettingOrder.BALANCE_AGENT_HAND_DONE + ")");
				} else if (bs == 3) {
					sql_sb.append(" AND a.balance = " + SportBettingOrder.BALANCE_CUT_GAME + " ");
				}
			}

			Long resultStatus = params.getResultStatus();
			if (Validator.isNotNull(resultStatus)) {
				int rs = resultStatus.intValue();
				if (rs == 1) {
					sql_sb.append(" AND a.result_status = " + SportBettingOrder.RESULT_STATUS_LOST + " ");
				} else if (rs == 2) {
					sql_sb.append(" AND a.result_status = " + SportBettingOrder.RESULT_STATUS_LOST_HALF + " ");
				} else if (rs == 3) {
					sql_sb.append(" AND a.result_status = " + SportBettingOrder.RESULT_STATUS_DRAW + " ");
				} else if (rs == 4) {
					sql_sb.append(" AND a.result_status = " + SportBettingOrder.RESULT_STATUS_WIN_HALF + " ");
				} else if (rs == 5) {
					sql_sb.append(" AND a.result_status = " + SportBettingOrder.RESULT_STATUS_WIN + " ");
				}
			}
			Long bettingStatus = params.getBettingStatus();
			if (Validator.isNotNull(bettingStatus)) {
				int rs = bettingStatus.intValue();
				if (rs == 1) {
					sql_sb.append(" AND a.betting_status = " + SportBettingOrder.BETTING_STATUS_UNCONFIRMED + " ");
				} else if (rs == 2) {
					sql_sb.append(" AND a.betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM + " ");
				} else if (rs == 3) {
					sql_sb.append(" AND a.betting_status = " + SportBettingOrder.BETTING_STATUS_SYS_REJECT + " ");
				} else if (rs == 4) {
					sql_sb.append(" AND a.betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + " ");
				} 
			}
			if (Validator.isNotNull(params.getTypeNames())) {
				sql_sb.append(" AND a.type_names like concat('%','"+params.getTypeNames()+"','%') ");
			}
		}
		sql_sb.append(" and mix != " + SportBettingOrder.MIX_CHILD + " ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.betting_money", "totalBetMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.betting_result", "totalBetResult"));
		//添加排序
		if(StringUtil.equals(SearchRecordParam.DATETYPE_MATCHDATE, params.getDateType())){
			sql_sb.append("  order by  a.account_datetime desc ");
		}else{
			sql_sb.append(" order by  a.betting_date desc ");
		}
		return super.page2CamelMap(sql_sb.toString(), param, aggs);
	}

	public List<SportBettingOrder> getUnbalanceOrder(String matchIds) {
		String sql = "select * from sport_betting where " + " balance = " + SportBettingOrder.BALANCE_UNDO // 未结算状态
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM // 注单已确认
				+ " and match_id in (" + matchIds + ")";
		return super.query2Model(sql);
	}

	/**
	 * 获取 @param minLimit 分钟内未检测过 且 状态为未结算的混合过关注单 检验间隔分钟
	 * 
	 * @return
	 */
	public SportBettingOrder getUnbalanceMixOrder(Date time) {
		String sql = "select * from sport_betting where " + " balance = " + SportBettingOrder.BALANCE_UNDO
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM
				+ " and (account_datetime is null or :time > account_datetime ) " + " and mix = "
				+ SportBettingOrder.MIX_MAIN + " limit 1 ";
		return super.query21Model(sql, MixUtil.newHashMap("time", time));
	}

	public void refreshMixCheckTime(long orderId, Date time) {
		String sql = "update sport_betting set account_datetime = :time where id = :orderId";
		super.update(sql, MixUtil.newHashMap("time", time, "orderId", orderId));
	}

	public List<SportBettingOrder> getChildOrder(long parentId) {
		String sql = "select * from sport_betting where " + " parent_id = :parentId order by id  ";
		return super.query2Model(sql, MixUtil.newHashMap("parentId", parentId));
	}

	/**
	 * 结算注单
	 * 
	 * @return
	 */
	public boolean balanceOrder(SportBettingOrder order, Long stationId) {

		StringBuffer sql = new StringBuffer("update sport_betting set  " + " data_version = data_version + 1 ," // 乐观锁版本号+1
				+ " result = :result, " + " remark = :remark," + " balance = :balance, "
				+ " account_datetime = :accountDatetime," + " betting_result = :bettingResult,"
				+ " result_status = :resultStatus " + " where id = :id " + " and data_version = :dataVersion "
				+ " and balance = " + SportBettingOrder.BALANCE_UNDO // 未结算状态
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM // 注单已确认
		);

		Map paramMap = MixUtil.newHashMap("dataVersion", order.getDataVersion(), "id", order.getId(), "result",
				order.getResult(), "remark", order.getRemark(), "balance", order.getBalance(), "accountDatetime",
				order.getAccountDatetime(), "resultStatus", order.getResultStatus(), "bettingResult",
				order.getBettingResult());

		if (stationId != null) {
			sql.append(" and station_id = :stationId ");
			paramMap.put(stationId, stationId);
		}

		return super.update(sql.toString(), paramMap) > 0;

	}

	public boolean balanceOrder(SportBettingOrder order) {
		return balanceOrder(order, null);
	}

	public boolean updateMixOrderLastCheckDate(SportBettingOrder order) {
		String sql = "update sport_betting set  " + " data_version = data_version + 1 ," // 乐观锁版本号+1
				+ " account_datetime = :accountDatetime " + " where id = :id " + " and data_version = :dataVersion "
				+ " and mix = " + SportBettingOrder.MIX_MAIN + " and balance = " + SportBettingOrder.BALANCE_UNDO // 未结算状态
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM; // 注单已确认
		Map paramMap = MixUtil.newHashMap("dataVersion", order.getDataVersion(), "id", order.getId(), "accountDatetime",
				new Date());
		return super.update(sql.toString(), paramMap) > 0;
	}

	/**
	 * 赛果腰斩
	 * 
	 * @param msg
	 * @return
	 */
	public boolean cutGame(SportBettingOrder order) {
		StringBuffer sql = new StringBuffer("update sport_betting set  " + " data_version = data_version + 1 ," // 乐观锁版本号+1
				+ " result = :result, " + " balance = " + SportBettingOrder.BALANCE_CUT_GAME + ", "
				+ " account_datetime = :accountDatetime," + " betting_result = :bettingResult,"
				+ " result_status = :resultStatus " + " where id = :id " + " and data_version = :dataVersion "
				+ " and balance = " + SportBettingOrder.BALANCE_UNDO // 未结算状态
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM // 注单已确认
		);

		Map paramMap = MixUtil.newHashMap("dataVersion", order.getDataVersion(), "id", order.getId(), "result",
				order.getResult(), "accountDatetime", order.getAccountDatetime(), "resultStatus",
				order.getResultStatus(), "bettingResult", order.getBettingResult());
		return super.update(sql.toString(), paramMap) > 0;
	}

	public boolean updateUncheckOrderStatus(long orderId, long status, BigDecimal bettingMoney, String msg) {
		StringBuffer sql = new StringBuffer("update sport_betting set ");
		sql.append("betting_status = :status,");
		if (status != SportBettingOrder.BETTING_STATUS_CONFIRM) {
			sql.append(" betting_result = :bettingResult ,");
		}
		sql.append(" status_remark = :msg ");
		sql.append(" where id = :orderId ");
		sql.append(" and betting_status = " + SportBettingOrder.BETTING_STATUS_UNCONFIRMED + " ");
		return super.update(sql.toString(), MixUtil.newHashMap("orderId", orderId, "msg", msg, "status", status,
				"bettingResult", bettingMoney)) > 0;
	}

	public boolean rollbackOrder(SportBettingOrder order) {
		StringBuffer sql = new StringBuffer("update sport_betting set  " + " data_version = data_version + 1 ," // 乐观锁版本号+1
				+ " result = null, " + " balance = " + SportBettingOrder.BALANCE_UNDO + ", "
				+ " account_datetime = null," + " betting_result = null," + " result_status = null "
				+ " where id = :id " + " and data_version = :dataVersion ");

		Map paramMap = MixUtil.newHashMap("dataVersion", order.getDataVersion(), "id", order.getId());
		return super.update(sql.toString(), paramMap) > 0;
	}

	public List<SportBettingOrder> getOrderByBalanceStatus(Long stationId, long matchId, long... balanceStatus) {
		Map paramMap = MixUtil.newHashMap("matchId", matchId);

		StringBuffer sql = new StringBuffer("select * from sport_betting where " + " match_id = :matchId "
				+ " and ( mix = " + SportBettingOrder.MIX_CHILD + "  or mix = " + SportBettingOrder.MIX_SINGLE + ")"
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM);
		sql.append(" and ( ");
		for (int i = 0; i < balanceStatus.length; i++) {
			if (i != 0) {
				sql.append(" OR ");
			}
			String key = "balanceStatus" + i;
			sql.append("balance = :" + key + " ");
			paramMap.put(key, balanceStatus[i]);
		}
		sql.append(" ) ");

		if (stationId != null) {
			sql.append(" and station_id =  :stationId ");
			paramMap.put("stationId", stationId);
		}
		return super.query2Model(sql.toString(), paramMap);
	}

	/**
	 * 用于反水时使用 (二级代理反水)
	 * 
	 * @param startDate
	 * @param endDate
	 * @param rollBackStatus
	 * @return
	 */
	public List<SportBettingOrder> get4Rebate(Long stationId, Date startDate, Date endDate, Integer rollBackStatus) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from sport_betting where station_id=:stationId ");
		sb.append(" and (roll_back_status is null or roll_back_status = :rollBackStatus)");
		sb.append(" and (balance =:bal1 or balance=:bal2 or balance=:bal3) ");// 已结算
		sb.append(" and mix !=  " + SportBettingOrder.MIX_CHILD + " "); // 忽略混合过关子单
		map.put("rollBackStatus", rollBackStatus);
		map.put("stationId", stationId);
		map.put("bal1", SportBettingOrder.BALANCE_DONE);
		map.put("bal2", SportBettingOrder.BALANCE_AGENT_HAND_DONE);
		map.put("bal3", SportBettingOrder.BALANCE_BFW_DONE);
		if (startDate != null) {
			sb.append(" and betting_date >= :startDate ");
			map.put("startDate", startDate);
		}
		if (endDate != null) {
			sb.append(" and betting_date <= :endDate ");
			map.put("endDate", endDate);
		}
		return super.query2Model(sb.toString(), map);
	}

	public List<SportBettingOrder> getCancelMemberRollBackRecord4SportRollBack() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		sb.append(" select sb.* from sport_betting sb  ");
		sb.append(
				" join agent_base_config_value abcv1 on abcv1.station_id=sb.station_id and abcv1.config_id=133 and abcv1.\"value\"='off'");// 多级代理关闭
		sb.append(" and sb.roll_back_status = 2  ");
		sb.append(" and sb.balance <> 2 and sb.balance<>5 and sb.balance<>6");// 状态为非结算的
		return super.query2Model(sb.toString(), map);
	}

	/**
	 * 反水专用
	 * 
	 * @param betId
	 * @param status
	 * @return
	 */
	public Integer updateSportBetById4RollBack(Long betId, Integer roll_back_status, Integer srcroll_back_status) {
		String sql = "update sport_betting set roll_back_status=:roll_back_status where id = :betId and (roll_back_status is null or roll_back_status=:srcroll_back_status)";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("betId", betId);
		map.put("roll_back_status", roll_back_status);
		map.put("srcroll_back_status", srcroll_back_status);
		return super.update(sql.toString(), map);
	}

	public void updateMixMainOrderRemark(long orderId, String remark) {
		String sql = "update sport_betting set remark = :remark " + " where id = :orderId ";
		super.update(sql, MixUtil.newHashMap("remark", remark, "orderId", orderId));
	}

	/**
	 * 单独手动反水时使用
	 * 
	 * @param id
	 * @return
	 */
	public SportBettingOrder findById4RollBack(Long id, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		sb.append(" select sb.* from sport_betting sb  ");
		sb.append(" where sb.roll_back_status = 1 ");
		sb.append(" and (sb.balance = 2 or sb.balance=5 or sb.balance=6)");// 已结算
		sb.append(" and sb.id=:id");// 已结算
		sb.append(" and sb.station_id=:stationId");// 已结算
		map.put("id", id);
		map.put("stationId", stationId);
		return super.query21Model(sb.toString(), map);
	}

	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from sport_betting where 1=1");
		Map map = MixUtil.newHashMap();
		sb.append(" and betting_date <= :createTime");
		map.put("createTime", createTime);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}

	public List<SportBettingOrder> getTenUnbalanceOrders(long memberId, Date startDate, Date endDate) {
		StringBuffer sql = new StringBuffer("select * from sport_betting " + " where member_id = :memberId "
				+ " and balance =  " + SportBettingOrder.BALANCE_UNDO + " " + " and betting_status <> "
				+ SportBettingOrder.BETTING_STATUS_REJECT + " and betting_status <> "
				+ SportBettingOrder.BETTING_STATUS_SYS_REJECT + " and mix = " + SportBettingOrder.MIX_SINGLE + " ");
		Map paramMap = MixUtil.newHashMap("memberId", memberId);
		if (startDate != null) {
			sql.append(" and betting_date > :startDate ");
			paramMap.put("startDate", startDate);
		}

		if (endDate != null) {
			sql.append(" and betting_date < :endDate ");
			paramMap.put("endDate", endDate);
		}
		sql.append(" order by id desc limit 10 ");
		return super.query2Model(sql.toString(), paramMap);
	}

	public Page getTodayOrders(long memberId) {
		Date now = new Date();
		Date startDate = DateUtil.dayFirstTime(now, -6);
		Date endDate = DateUtil.dayFirstTime(now, 1);

		StringBuffer sql = new StringBuffer(
				"select * from sport_betting " + " where member_id = :memberId " + " and balance =  "
						+ SportBettingOrder.BALANCE_UNDO + " " +" and betting_status <> " + SportBettingOrder.BETTING_STATUS_REJECT + "  "
								+ " and betting_status <>  " + SportBettingOrder.BETTING_STATUS_SYS_REJECT +  " and mix != " + SportBettingOrder.MIX_CHILD + " ");
		sql.append(" and betting_date > :startDate ");
		sql.append(" and betting_date < :endDate ");
		sql.append(" order by id desc ");
		Map paramMap = MixUtil.newHashMap("memberId", memberId, "startDate", startDate, "endDate", endDate);
		List<Aggregation> aggs = new ArrayList<>();

		String condition = " case when betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + "  "
				+ " OR betting_status =  " + SportBettingOrder.BETTING_STATUS_SYS_REJECT + " " + " THEN 1 ELSE 0 END ";
		aggs.add(new Aggregation(AggregationFunction.SUM, condition, "rejectOrderCount"));
		return super.page2CamelMap(sql.toString(), paramMap, aggs);
	}

	public List getHistoryTotalData(long memberId, Long sportType) {
		Date now = new Date();
		Date startDate = DateUtil.dayFirstTime(now, -7);
		Date endDate = DateUtil.dayFirstTime(now, 1);

		Map paramMap = MixUtil.newHashMap("memberId", memberId, "startDate", startDate, "endDate", endDate);
		StringBuffer sql = new StringBuffer("" + " select to_char(betting_date,'YYYY-MM-DD') as date_str,"
				+ " sum(betting_money) as bet_money_count, " + " sum(betting_result) as result_count "
				+ " from sport_betting " + " where member_id = :memberId " + " and mix != "
				+ SportBettingOrder.MIX_CHILD + " ");
		if (sportType != null) {
			sql.append(" and sport_type = :sportType ");
			paramMap.put("sportType", sportType);
		}

		sql.append(" and (balance <>  " + SportBettingOrder.BALANCE_UNDO + " or betting_status = "
				+ SportBettingOrder.BETTING_STATUS_REJECT + " or  betting_status = "
				+ SportBettingOrder.BETTING_STATUS_SYS_REJECT + ")  ");

		// sql.append("and ( balance = " +SportBettingOrder.BALANCE_DONE
		// +" or balance = "+SportBettingOrder.BALANCE_AGENT_HAND_DONE
		// +" or balance = "+SportBettingOrder.BALANCE_CONTROL_HAND_DONE +")");
		sql.append(" and betting_date > :startDate ");
		sql.append(" and betting_date < :endDate ");

		sql.append(" group by date_str ");
		return super.selectCamelListMap(sql.toString(), paramMap);
	}

	public Page getHistoryData(Date startDate, long memberId, Long sportType) {
		Date endDate = DateUtil.dayFirstTime(startDate, 1);
		Map paramMap = MixUtil.newHashMap("memberId", memberId, "startDate", startDate, "endDate", endDate);

		StringBuffer sql = new StringBuffer("" + " select * " + " from sport_betting " + " where member_id = :memberId "
				+ " and mix != " + SportBettingOrder.MIX_CHILD + " ");

		if (sportType != null) {
			sql.append(" and sport_type = :sportType ");
			paramMap.put("sportType", sportType);
		}
		sql.append(" and (balance <>  " + SportBettingOrder.BALANCE_UNDO + " or betting_status = "
				+ SportBettingOrder.BETTING_STATUS_REJECT + " or  betting_status = "
				+ SportBettingOrder.BETTING_STATUS_SYS_REJECT + ")  ");
		// sql.append("and ( balance = " +SportBettingOrder.BALANCE_DONE
		// +" or balance = "+SportBettingOrder.BALANCE_AGENT_HAND_DONE
		// +" or balance = "+SportBettingOrder.BALANCE_CONTROL_HAND_DONE +")");
		sql.append(" and betting_date > :startDate ");
		sql.append(" and betting_date < :endDate ");
		sql.append(" order by id desc ");
		return super.page2CamelMap(sql.toString(), paramMap);
	}

	public List<SportBettingOrder> queryOrders(MobileSportRecordParameter parameter) {
		long recordType = parameter.getRecordType();
		Map paramMap = MixUtil.newHashMap("memberId", parameter.getMemberId(), "startDate", parameter.getStartDate(),
				"endDate", parameter.getEndDate(), "recordType", recordType);

		StringBuffer sql = new StringBuffer("" + " select * " + " from sport_betting " + " where member_id = :memberId "
				+ " and mix != " + SportBettingOrder.MIX_CHILD + " ");
		Long sportType = parameter.getSportType();
		if (sportType != null) {
			sql.append(" and sport_type = :sportType ");
			paramMap.put("sportType", sportType);
		}

		if (recordType == MobileSportRecordParameter.RECORD_TYPE_REJECT) {// 未成功
			sql.append(" and (betting_status = " + SportBettingOrder.BETTING_STATUS_SYS_REJECT + " or "
					+ " betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + ") ");
		} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_UNBALANCE) { // 未结算
			sql.append(" and balance = " + SportBettingOrder.BALANCE_UNDO + " " + " and (betting_status = "
					+ SportBettingOrder.BETTING_STATUS_CONFIRM + "  or betting_status = "
					+ SportBettingOrder.BETTING_STATUS_UNCONFIRMED + ") ");
		} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_WIN) {// 中奖
			sql.append("and ( balance = " + SportBettingOrder.BALANCE_DONE + " or balance = "
					+ SportBettingOrder.BALANCE_AGENT_HAND_DONE + " or balance = " + SportBettingOrder.BALANCE_BFW_DONE
					+ ")");
			sql.append(" and betting_result > 0 ");
		}

		sql.append(" and betting_date > :startDate ");
		sql.append(" and betting_date < :endDate ");
		sql.append(" order by id desc ");

		return super.selectCamelListMap(sql.toString(), paramMap);
	}

	public Page queryOrdersPage(MobileSportRecordParameter parameter) {
		long recordType = parameter.getRecordType();
		Map paramMap = MixUtil.newHashMap("memberId", parameter.getMemberId(), "startDate", parameter.getStartDate(),
				"endDate", parameter.getEndDate(), "recordType", recordType);

		StringBuffer sql = new StringBuffer("" + " select * " + " from sport_betting " + " where member_id = :memberId "
				+ " and mix != " + SportBettingOrder.MIX_CHILD + " ");
		Long sportType = parameter.getSportType();
		if (sportType != null) {
			sql.append(" and sport_type = :sportType ");
			paramMap.put("sportType", sportType);
		}

		if (recordType == MobileSportRecordParameter.RECORD_TYPE_REJECT) {// 未成功
			sql.append(" and (betting_status = " + SportBettingOrder.BETTING_STATUS_SYS_REJECT + " or "
					+ " betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + ") ");
		} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_UNBALANCE) { // 未结算
			sql.append(" and balance = " + SportBettingOrder.BALANCE_UNDO + " " + " and (betting_status = "
					+ SportBettingOrder.BETTING_STATUS_CONFIRM + "  or betting_status = "
					+ SportBettingOrder.BETTING_STATUS_UNCONFIRMED + ") ");
		} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_WIN) {// 中奖
			sql.append("and ( balance = " + SportBettingOrder.BALANCE_DONE + " or balance = "
					+ SportBettingOrder.BALANCE_AGENT_HAND_DONE + " or balance = " + SportBettingOrder.BALANCE_BFW_DONE
					+ ") and betting_result is not null and betting_result > 0");
			sql.append(" and betting_result > 0 ");
		}

		sql.append(" and betting_date > :startDate ");
		sql.append(" and betting_date < :endDate ");
		sql.append(" order by id desc ");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "betting_money", "totalBetMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "betting_result", "totalBetResult"));
		return super.page2CamelMap(sql.toString(), paramMap, aggs);
	}

	public Page queryOrdersApi(MobileSportRecordParameter parameter) {

		Map paramMap = MixUtil.newHashMap();
		StringBuffer sql = new StringBuffer("" + " select * " + " from sport_betting " + " where  mix != " + SportBettingOrder.MIX_CHILD + " ");
		if (parameter.getStartDate() != null) {
			sql.append(" and betting_date > :startDate ");
			paramMap.put("startDate", parameter.getStartDate());
		}
		if (parameter.getEndDate() != null) {
			sql.append(" and betting_date < :endDate ");
			paramMap.put("endDate", parameter.getEndDate() );
		}
		Long sportType = parameter.getSportType();
		if (sportType != null) {
			sql.append(" and sport_type = :sportType ");
			paramMap.put("sportType", sportType);
		}
		Long memberId = parameter.getMemberId();
		if(memberId!=null){
			sql.append(" and member_id = :memberId ");
			paramMap.put("memberId", memberId);
		}
		Long stationId = parameter.getStationId();
		if(stationId!=null){
			sql.append(" and station_id = :stationId ");
			paramMap.put("stationId", stationId);
		}
		Long recordType = parameter.getRecordType();
		if(recordType!=null){
			if (recordType == MobileSportRecordParameter.RECORD_TYPE_REJECT) {// 未成功
				sql.append(" and (betting_status = " + SportBettingOrder.BETTING_STATUS_SYS_REJECT + " or "
						+ " betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + ") ");
			} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_UNBALANCE) { // 未结算
				sql.append(" and balance = " + SportBettingOrder.BALANCE_UNDO + " " + " and (betting_status = "
						+ SportBettingOrder.BETTING_STATUS_CONFIRM + "  or betting_status = "
						+ SportBettingOrder.BETTING_STATUS_UNCONFIRMED + ") ");
			} else if (recordType == MobileSportRecordParameter.RECORD_TYPE_WIN) {// 中奖
				sql.append("and ( balance = " + SportBettingOrder.BALANCE_DONE + " or balance = "
						+ SportBettingOrder.BALANCE_AGENT_HAND_DONE + " or balance = " + SportBettingOrder.BALANCE_BFW_DONE
						+ ") and betting_result is not null and betting_result > 0");
				sql.append(" and betting_result > 0 ");
			}

		}
		sql.append(" order by id desc ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "betting_money", "totalBetMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "betting_result", "totalBetResult"));
		return super.page2CamelMap(sql.toString(), paramMap, aggs);
	}



	public boolean cancelOrder(SportBettingOrder order) {

		StringBuffer sql = new StringBuffer("update sport_betting set  " + " data_version = data_version + 1 ," // 乐观锁版本号+1
				+ " result = null, " + " betting_status = " + SportBettingOrder.BETTING_STATUS_REJECT + ", "
				+ " status_remark = :statusRemark, " + " betting_result = :bettingResult " + " where id = :id "
				+ " and data_version = :dataVersion ");

		Map paramMap = MixUtil.newHashMap("dataVersion", order.getDataVersion(), "id", order.getId(), "statusRemark",
				order.getStatusRemark(), "bettingResult", order.getBettingMoney());
		return super.update(sql.toString(), paramMap) > 0;
	}

	public void cancelChildOrders(long parentId) {
		StringBuffer sql = new StringBuffer("update sport_betting set  " + " betting_status = "
				+ SportBettingOrder.BETTING_STATUS_REJECT + " " + " where parent_id = :parentId ");
		Map paramMap = MixUtil.newHashMap("parentId", parentId);
		super.update(sql.toString(), paramMap);
	}

	public SportBettingOrder getOrderByBettingCode(String bettingCode, Long stationId) {
		String sql = "select * from sport_betting " + " where betting_code = :bettingCode "
				+ " and station_id = :stationId ";
		Map paramMap = MixUtil.newHashMap("bettingCode", bettingCode, "stationId", stationId);
		return super.query21Model(sql, paramMap);
	}

	public List<SportBettingOrder> getOrderByDate(Long stationId, Date startDate, Date endDate) {
		StringBuilder sb = new StringBuilder("select  * from sport_betting where station_id=:stationId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("stationId", stationId);
		if (startDate != null) {
			sb.append(" and betting_date >=:startDate");
			map.put("startDate", startDate);
		}
		if (endDate != null) {
			sb.append(" and betting_date <=:endDate");
			map.put("endDate", endDate);
		}
		return super.query2Model(sb.toString(), map);
	}

	public SportBettingOrder getOrderByBettingCode(String bettingCode) {
		String sql = "select * from sport_betting " + " where betting_code = :bettingCode ";
		Map paramMap = MixUtil.newHashMap("bettingCode", bettingCode);
		return super.query21Model(sql, paramMap);
	}

	public List<SportBettingOrder> findNeedCheckOrders(Date start, Date end) {
		StringBuilder sb = new StringBuilder("select  * from sport_betting");
		sb.append(" where game_time_type=1 and betting_status=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (start != null) {
			sb.append(" and betting_date >=:startDate");
			map.put("startDate", start);
		}
		if (end != null) {
			sb.append(" and betting_date <=:endDate");
			map.put("endDate", end);
		}
		return super.query2Model(sb.toString(), map);
	}


    public List<SportBettingOrder> getOrderTopHalf(Long id, long balanceUndo) {
		Map paramMap = MixUtil.newHashMap("matchId", id);

		StringBuffer sql = new StringBuffer("select * from sport_betting where " + " match_id = :matchId "
				+ " and  mix = " + SportBettingOrder.MIX_SINGLE
				+ " and betting_status = " + SportBettingOrder.BETTING_STATUS_CONFIRM);
		sql.append(" and balance = "+balanceUndo);
		sql.append("  and type_names like concat('%','上半','%')");
		return super.query2Model(sql.toString(), paramMap);

    }
}
