package com.game.dao.sports;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.dao.lottery.LongRowMapper;
import com.game.model.sports.SportMatch;
import com.game.model.vo.SportMatchParameter;

@Repository
public class SportMatchDao extends JdbcUtilImpl<SportMatch>{
	
	public Page queryPage(SportMatchParameter param,long stationId){
		
		Map paramMap = MixUtil.newHashMap("stationId",stationId);
		
		StringBuffer sql = new StringBuffer("select a.*,c.id as hand_result_id,r.balance "
				+ " from sport_match  a "
				+ " left join sport_match_result_hand c "
				+ " on(a.id = c.match_id and c.station_id = :stationId) "
				+ " left join sport_match_result r on r.gid = a.match_result_gid ") ;

		sql.append(" WHERE 1 = 1 ");
		if(Validator.isNotNull(param.getBegin())) {
			sql.append(" AND a.start_time > :begin ");
			paramMap.put("begin", param.getBegin());
		}

		if (Validator.isNotNull(param.getEnd())) {
			sql.append(" AND a.start_time < :end ");
			paramMap.put("end", param.getEnd());
		}
		
		if(Validator.isNotNull(param.getHomeTeam())){
			sql.append(" AND a.h_team like :home ");
			paramMap.put("home","%"+param.getHomeTeam()+"%");
		}
		
		if(Validator.isNotNull(param.getGuestTeam())){
			sql.append(" AND a.g_team like :guest ");
			paramMap.put("guest","%"+param.getGuestTeam()+"%");
		}
		
		if (Validator.isNotNull(param.getSportType())) {
			sql.append(" AND a.sport_type_id = :type ");
			paramMap.put("type", param.getSportType());
		}
		
		if(Validator.isNotNull(param.getMatchId())){
			sql.append(" AND a.id = :id ");
			paramMap.put("id", param.getMatchId());
		}
		
		Long resultStatus = param.getResultStatus();
		if (Validator.isNull(resultStatus)){
			
		}else if(StringUtil.equals(resultStatus, SportMatchParameter.RESULT_STATUS_NONE)){
			sql.append(" AND c.id is null AND a.match_result_gid is null ");
		}else if(StringUtil.equals(resultStatus, SportMatchParameter.RESULT_STATUS_SYS)){
			sql.append(" AND a.match_result_gid is not null ");
		}else if(StringUtil.equals(resultStatus, SportMatchParameter.RESULT_STATUS_HAND)){
			sql.append(" AND c.id is not null ");
		}
		Long billingStatus = param.getBillingStatus();
		if (Validator.isNull(billingStatus)){
			
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_BET_NONE)){
			sql.append(" AND (a.match_result_gid is null or r.balance = 5)  and c.id is null and (select 1 from sport_betting b where b.station_id = :stationId and b.match_id = a.id and b.balance = 1 and b.betting_status<3 limit 1) is not null ");
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_NONE)){
			sql.append(" AND (a.match_result_gid is null or r.balance = 5) and  a.bfw_result_id is null and c.id is null ");
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_SYS)){
			sql.append(" AND a.match_result_gid is not null and r.balance = 2 and c.id is null");
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_HAND)){
			sql.append(" AND a.match_result_gid is not null and r.balance = 2 AND c.id is not null ");
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_HAlF)){
			sql.append(" AND a.match_result_gid is not null and r.balance = 5 AND c.id is null ");
		}else if(StringUtil.equals(billingStatus, SportMatchParameter.BILLING_STATUS_BET_HALF_NONE)){
			sql.append(" AND (a.match_result_gid is null or r.balance = 5)  and c.id is null and (select 1 from sport_betting b where b.station_id = :stationId and b.match_id = a.id and b.balance = 1 and b.betting_status<3 and  b.type_names like concat('%','上半','%')limit 1) is not null ");
		}
		sql.append(" order by a.id desc ");
		return super.page2CamelMap(sql.toString(),paramMap);
	}
	
	public SportMatch getMatchByGid(long gid){
		String sql = "select * from sport_match where gid = :gid ";
		return super.query21Model(sql,MixUtil.newHashMap("gid",gid));
	}
	
	public void updateSportMatchBfwResultId(String ids,long matchId){
		String sql = "update sport_match set bfw_result_id = :matchId ,bfw_last_time = :now where id in("+ids+")";
		Map paramMap = MixUtil.newHashMap("matchId",matchId,"now",new Date());
		super.update(sql, paramMap);
	}

	public void deleteMatchByGid(long gid){
		String sql = "delete from sport_match where gid = :gid ";
		super.update(sql,MixUtil.newHashMap("gid",gid));
	}
	
	public List<SportMatch> getSameMatch(Date startTime,String onlyKey,long sportType,Long bfwResultId){
		StringBuffer sql = new StringBuffer("select * from sport_match "
				+ " where " 
//				( bfw_result_id is null or "
//				+ " bfw_result_id = :bfwResultId ) and  "
				//+ " and start_time = :startTime "
				+ "  bfw_only_key = :onlyKey "
				+ " and sport_type_id = :sportType ");
	
		return super.query2Model(sql.toString(), MixUtil.newHashMap("startTime",startTime,
				"onlyKey",onlyKey,"sportType",sportType,"bfwResultId",bfwResultId));

	}
	
	public List<SportMatch> getSameMatch(long matchResultGid,String onlyKey,long sportType){
		String sql = "select * from sport_match "
				+ " where "
				+ " gid = :gid "
				+ "or (gid > :gid "
				+ " and only_key = :onlyKey "
				+ " and match_result_gid is null "
				+ " and sport_type_id = :sportType ) ";
		return super.query2Model(sql, MixUtil.newHashMap("gid",matchResultGid,"onlyKey",onlyKey,"sportType",sportType));
	}
	
	public boolean updateMatchResultGid(long matchId,long matchResultGid){
		String sql = "update sport_match "
				+ " set match_result_gid = :matchResultGid "
				+ " where id = :matchId ";
		return super.update(sql, MixUtil.newHashMap("matchId",matchId,"matchResultGid",matchResultGid)) > 0;
	}
	
	public List<SportMatch> getBfwBalanceMatch(int size){
		String sql = "select * from sport_match "
				+ " where bfw_last_time is not null "
				+ " and bfw_result_id is not null "
				+ " and (bfw_balance_time is null or bfw_balance_time < bfw_last_time)  "
				+ " order by bfw_last_time desc limit :size ";
		return super.query2Model(sql,MixUtil.newHashMap("size",size));
	}

	public List<Long> findMatchIdBySameOnlyKey(Long matchId) {
		String sql = "select id from sport_match where only_key=(select only_key from sport_match where id=:id) and id<>:id";
		return query2Obj(sql, MixUtil.newHashMap("id",matchId), new LongRowMapper());
	}

	public List<SportMatch> findMatchIdBySameOnlyKey(String onlyKey) {
		return query2Model("select * from sport_match where only_key=:onlyKey", MixUtil.newHashMap("onlyKey",onlyKey));
	}

    public List<SportMatch> getUnSetTopHalfMatch() {
		String sql = "select m.* from (\n" +
				"select match_id,gid from sport_betting \n" +
				"where betting_date > now()- interval '3 Days' and betting_status = 2 and balance =1 AND type_names like concat('%','上半','%') \n" +
				" and mix =1 and sport_type = 1 GROUP BY match_id,gid ) t \n" +
				"LEFT JOIN sport_match m on m.id = t.match_id \n" +
				"where m.start_time  + interval '12 Hours 45 Minutes'  < now()   and now() < m.start_time + interval '12 Hours 90 Minutes' ";
		return super.query2Model(sql);

    }
}
