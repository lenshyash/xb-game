package com.game.dao;

import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberLevelLog;

@Repository
public class MemberLevelLogDao extends JdbcUtilImpl<MemberLevelLog> {

	public Page<MemberLevelLog> getPage(Long stationId) {
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId", stationId);
		return super.paged2Obj(
				"SELECT * FROM member_level_log WHERE station_id=:stationId ORDER BY create_datetime DESC", paramMap);
	}

}
