package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysAnnouncement;
import com.game.model.vo.AnnouncementVo;

@Repository
public class SysAnnouncementDao extends JdbcUtilImpl<SysAnnouncement> {

	public Page<Map> getPage(AnnouncementVo ancvo) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM sys_announcement WHERE 1=1 ");
		Long type = ancvo.getType();
		Date begin = ancvo.getBegin();
		Date end = ancvo.getEnd();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (type != null && type != 0l) {
			paramMap.put("type", type);
			sql_sb.append(" AND type=:type");
		}
		if (begin != null) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND end_datetime >=:begin");
		}

		if (end != null) {
			paramMap.put("end", end);
			sql_sb.append(" AND end_datetime <=:end");
		}
		sql_sb.append(" order by end_datetime desc");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}
}
