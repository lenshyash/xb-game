package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.dao.lottery.LongRowMapper;
import com.game.model.AgentBaseConfig;

@Repository
public class AgentBaseConfigDao extends JdbcUtilImpl<AgentBaseConfig> {

	public boolean updateSysConfigValue(String key, String value) {
		String sql = " update agent_base_config set init_value = :value where key = :key ";
		if (super.update(sql, MixUtil.newHashMap("value", value, "key", key)) > 0) {
			CacheUtil.delCache(CacheType.AGENT_SETTING, "_sys_" + key);
			return true;
		}
		return false;
	}

	public boolean updateSysConfigValue(Long id, String value) {
		if (id != null && id > 0) {
			AgentBaseConfig old = get(id);
			if (old != null) {
				String sql = " update agent_base_config set init_value = :value where id = :id ";
				if (super.update(sql, MixUtil.newHashMap("value", value, "id", id)) > 0) {
					CacheUtil.delCache(CacheType.AGENT_SETTING, "_sys_" + old.getKey());
					return true;
				}
			}
		}
		return false;
	}

	public Page<AgentBaseConfig> getPageConfig(String name, Long platform, String groupName, Long groupId) {
		StringBuilder sql_sb = new StringBuilder("SELECT c.*,g.name AS group_name");
		sql_sb.append(" FROM agent_base_config c LEFT JOIN agent_base_config_group g ON c.group_id = g.id");
		sql_sb.append(" WHERE 1=1");

		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtil.isNotEmpty(name)) {
			paramMap.put("name", name.trim() + "%");
			sql_sb.append(" AND c.name LIKE :name");
		}

		if (StringUtil.isNotEmpty(groupName)) {
			paramMap.put("groupName", groupName.trim() + "%");
			sql_sb.append(" AND g.name LIKE :groupName");
		}

		if (Validator.isNotNull(groupId)) {
			paramMap.put("groupId", groupId);
			sql_sb.append(" AND g.id = :groupId");
		}

		if (Validator.isNotNull(platform)) {
			paramMap.put("platform", platform);
			sql_sb.append(" AND c.platform = :platform");
		}
		sql_sb.append(" ORDER BY g.order_no,c.order_no");

		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<AgentBaseConfig> getConfigAll(String name, Long platform) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM agent_base_config WHERE status!=:status");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("status", AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE);
		if (StringUtil.isNotEmpty(name)) {
			paramMap.put("name", name.trim() + "%");
			sql_sb.append(" AND name LIKE :name");
		}
		if (Validator.isNotNull(platform)) {
			paramMap.put("platform", platform);
			sql_sb.append(" AND platform = :platform");
		}
		sql_sb.append(" order by id asc");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public List<AgentBaseConfig> getSysConfs() {
		StringBuilder sql_sb = new StringBuilder("SELECT b.*,g.name as group_name");
		sql_sb.append(" FROM agent_base_config b LEFT JOIN agent_base_config_group g ON g.id=b.group_id");
		sql_sb.append(" WHERE b.platform =").append(AgentBaseConfig.PLATFORM_SYSTEM);
		sql_sb.append(" and status!=").append(AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE);
		sql_sb.append(" ORDER BY g.order_no,b.order_no");
		return super.query2Model(sql_sb.toString());
	}

	/**
	 * 获取 总控配置
	 * 
	 * @param acvo
	 * @return
	 */
	public AgentBaseConfig getConfig(String key) {
		String ckey = "_sys_" + key;
		AgentBaseConfig v = CacheUtil.getCache(CacheType.AGENT_SETTING, ckey, AgentBaseConfig.class);
		if (v != null) {
			return v;
		}
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("status", AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE);
		paramMap.put("key", key);
		v = super.query21Model("SELECT * FROM agent_base_config WHERE status!=:status AND key=:key", paramMap);
		if (v != null) {
			CacheUtil.addCache(CacheType.AGENT_SETTING, ckey, v);
		}
		return v;
	}
//
//	public List<Long> findAllRealMoneyConfig() {
//		Map<String, Object> paramMap = new HashMap<>();
//		paramMap.put("ag", StationConfig.live_transfor_ag_limit.name());
//		paramMap.put("bbin", StationConfig.live_transfor_bbin_limit.name());
//		paramMap.put("mg", StationConfig.live_transfor_mg_limit.name());
//		paramMap.put("pt", StationConfig.live_transfor_pt_limit.name());
//		paramMap.put("qt", StationConfig.live_transfor_qt_limit.name());
//		paramMap.put("ab", StationConfig.live_transfor_ab_limit.name());
//		return selectList("select id from agent_base_config where key in(:ag,:bbin,:mg,:pt,:qt,:ab)", paramMap,
//				new LongRowMapper());
//	}
}
