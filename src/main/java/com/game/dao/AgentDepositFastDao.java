package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentDepositBank;
import com.game.model.AgentDepositFast;
import com.game.util.StationUtil;

@Repository
public class AgentDepositFastDao extends JdbcUtilImpl<AgentDepositFast> {
	public Page<Map> getPage(Long payPlatformId) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT f.*,p.name AS pay_name,p.company AS pay_com,f.sort_no,p.icon_css");
		sql_sb.append(" FROM agent_deposit_fast f LEFT JOIN sys_pay_platform p ON f.pay_platform_id = p.id");
		sql_sb.append(" WHERE f.station_id= :stationId ");
		if(payPlatformId!=null && payPlatformId.intValue()!=0){
			sql_sb.append(" and  p.id = :payPlatformId");
		}
		sql_sb.append(" order by f.sort_no asc");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(),"payPlatformId",payPlatformId);
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<Map> getFasts(String code) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT f.*,p.name AS pay_name,p.company AS pay_com,p.icon_css");
		sql_sb.append(" FROM agent_deposit_fast f LEFT JOIN sys_pay_platform p ON f.pay_platform_id = p.id");
		sql_sb.append(" WHERE f.station_id= :stationId");
		sql_sb.append(" AND f.status= :status");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", AgentDepositBank.STATUS_ENABLE);
		if(StringUtil.isNotEmpty(code)) {
			if("支付宝".equals(code) || "微信".equals(code)) {
				sql_sb.append(" AND p.name = :code");
				paramMap.put("code", code);
			}else {
				sql_sb.append(" AND p.name not in ('支付宝','微信')");
			}
		}
		sql_sb.append(" order by f.sort_no asc");
		
		
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}
	
	public void updateStatus(Integer status, Long id, Long stationId) {
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", status, "id", id);
		update("update agent_deposit_fast set status=:status where id=:id and station_id=:stationId", paramMap);
	}
}
