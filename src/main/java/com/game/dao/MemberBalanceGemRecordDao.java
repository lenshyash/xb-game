package com.game.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.model.MemberBalanceGemRecord;
import com.game.model.MnyComRecord;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.ReportParamVo;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class MemberBalanceGemRecordDao extends JdbcUtilImpl<MemberBalanceGemRecord> {

	public Page<MemberBalanceGemRecord> getPage(MemberBalanceGemRecord mcrVo,Date startTime,Date endTime) {
		StringBuilder sql_sb = new StringBuilder("SELECT r.* FROM member_balance_gem_record r ");
		sql_sb.append(" LEFT JOIN sys_account a on a.id = r.account_id");
		sql_sb.append(" WHERE r.station_id= :stationId");
		Long stationId = mcrVo.getStationId();
		Long type = mcrVo.getType();
		String orderNo = StringUtil.trim2Empty(mcrVo.getOrderNo());
		String account =  StringUtil.trim2Empty(mcrVo.getAccount());
		Integer accountType = mcrVo.getAccountType();
		Integer reportType = mcrVo.getReportType();
		Long status = mcrVo.getStauts();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (Validator.isNotNull(type)) {
			sql_sb.append(" AND r.type= :type");
			paramMap.put("type", type);
		}
		if (StringUtil.isNotEmpty(orderNo)) {
			sql_sb.append(" AND r.order_no= :orderNo");
			paramMap.put("orderNo", orderNo);
		}
		if (Validator.isNotNull(startTime)) {
			sql_sb.append(" AND r.create_datetime >= :begin");
			paramMap.put("begin",startTime);
		}
		if (Validator.isNotNull(endTime)) {
			sql_sb.append(" AND r.create_datetime <= :end");
			paramMap.put("end",endTime);
		}
		if (StringUtil.isNotEmpty(account)) {
			sql_sb.append(" AND r.account= :account");
			paramMap.put("account", account.trim().toLowerCase());
		}
		if (Validator.isNotNull(accountType)) {
			sql_sb.append(" AND a.account_type = :accountType");
			paramMap.put("accountType",accountType);
		}
		if (Validator.isNotNull(reportType)) {
			sql_sb.append(" AND a.report_type = :reportType");
			paramMap.put("reportType",reportType);
		}
		if (Validator.isNotNull(status)) {
			sql_sb.append(" AND r.status = :status");
			paramMap.put("status",status);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"CASE WHEN r.status =" + MnyComRecord.STATUS_SUCCESS + " THEN r.money ELSE 0 END ", "totalMoney"));
		return super.paged2Obj(sql_sb.toString(), paramMap, aggs);
	}

	public List<MemberBalanceGemRecord> selMemberDayBalanceGemRecord(Date start, Date end, Long stationId) {
		StringBuilder sql_sb = new StringBuilder("select t.* from member_balance_gem_record t INNER JOIN (select max(id) as id,account_id,max(create_datetime) from member_balance_gem_record where station_id = :stationId and  create_datetime > :start and create_datetime < :end and status = :status"); 
		sql_sb.append(" GROUP BY account_id");
		sql_sb.append(" ) t1 on t1.id = t.id");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("start", start);
		paramMap.put("end", end);
		paramMap.put("status", MemberBalanceGemRecord.STATUS_SUCCESS);
		return super.query2Model(sql_sb.toString(), paramMap);
	}
	
	public BigDecimal selMemberDayInvalidAmount(Date start, Date end, Long accountId) {
		StringBuilder sql_sb = new StringBuilder("select sum(money) from member_balance_gem_record "); 
		sql_sb.append(" where create_datetime > :start and create_datetime < :end ");
		sql_sb.append(" and account_id = :accountId and type in(2,3)");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("start", start);
		paramMap.put("end", end);
		return super.queryForBigDecimal(sql_sb.toString(), paramMap);
	}

	public int checkMemberCurDateRebate(Date start,Long accountId) {
		StringBuilder sql_sb = new StringBuilder("SELECT count(1) from member_balance_gem_record where rebate_date = :start and account_id = :accountId and type = 1 and status=2"); 
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("start", start);
		paramMap.put("accountId", accountId);
		return super.queryForInt(sql_sb.toString(), paramMap);
	}
	public BigDecimal getYesterdayIncome(Date start,Long accountId) {
		StringBuilder sql_sb = new StringBuilder("SELECT money from member_balance_gem_record where rebate_date = :start and account_id = :accountId and type = 1 and status=2"); 
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("start", start);
		paramMap.put("accountId", accountId);
		return super.queryForBigDecimal(sql_sb.toString(), paramMap);
	}
}
