package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentBaseConfigGroup;

@Repository
public class AgentBaseConfigGroupDao extends JdbcUtilImpl<AgentBaseConfigGroup> {

	public Page<AgentBaseConfigGroup> getPageGroups(Long platform,String groupName) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM agent_base_config_group WHERE 1=1");
		Map<String,Object> paramMap =new HashMap<>();
		if (Validator.isNotNull(platform)) {
			sql_sb.append(" AND platform = :platform");
			paramMap.put("platform", platform);
		}
		if (StringUtil.isNotEmpty(groupName)) {
			sql_sb.append(" AND name = :groupName");
			paramMap.put("groupName", groupName);
		}
		sql_sb.append(" order by order_no asc");
		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<AgentBaseConfigGroup> getGroups(Long platform) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM agent_base_config_group WHERE 1=1");
		Map<String,Object> paramMap =new HashMap<>();
		if (Validator.isNotNull(platform)) {
			sql_sb.append(" AND platform = :platform");
			paramMap.put("platform", platform);
		}
		sql_sb.append(" order by order_no");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

}
