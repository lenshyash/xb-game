package com.game.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.SysLog;
import com.game.model.vo.SysLogVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SysLogDao extends JdbcUtilImpl<SysLog> {

	/**
	 * 
	 * @return
	 */
	public Page getLogsPage(SysLogVo slvo) {
		StringBuilder sql_cols = new StringBuilder("");
		StringBuilder sql_tabs = new StringBuilder("");
		StringBuilder sql_whs = new StringBuilder("");
		StringBuilder sql_order = new StringBuilder("");
		sql_cols.append("SELECT l.*");
		sql_tabs.append(" FROM sys_log l");
		sql_whs.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();

		if (StationUtil.isAgentStation()
				&& StringUtil.equals(UserUtil.getType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			sql_whs.append(" AND a.account_type in (:accountType1,:accountType2,:accountType3,:accountType4)");
			paramMap.put("accountType1", SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
			paramMap.put("accountType2", SysAccount.ACCOUNT_PLATFORM_MEMBER);
			paramMap.put("accountType3", SysAccount.ACCOUNT_PLATFORM_AGENT);
			paramMap.put("accountType4", SysAccount.ACCOUNT_PLATFORM_GUIDE);
		}

		if (Validator.isNotNull(slvo.getType())) {
			sql_whs.append(" AND l.log_type = :type");
			paramMap.put("type", slvo.getType());
		}

		if (StringUtil.isNotEmpty(slvo.getAccount())) {
			sql_whs.append(" AND lower(l.account) = :account");
			paramMap.put("account", slvo.getAccount().toLowerCase());
		}
		
		if (StringUtil.isNotEmpty(slvo.getContent())) {
			sql_whs.append(" AND l.log_content LIKE :content");
			paramMap.put("content", "%"+slvo.getContent()+"%");
		}

		if (Validator.isNotNull(slvo.getPlatform())) {
			sql_whs.append(" AND l.platform = :platform");
			paramMap.put("platform", slvo.getPlatform());
		}

		if (Validator.isNotNull(slvo.getStationId())) {
			sql_whs.append(" AND l.station_id = :stationId");
			paramMap.put("stationId", slvo.getStationId());
		}

		if (StringUtil.isNotEmpty(slvo.getBegin())) {
			sql_whs.append(" AND l.create_datetime >= :begin");
			paramMap.put("begin", slvo.getBegin());
		}

		if (StringUtil.isNotEmpty(slvo.getEnd())) {
			sql_whs.append(" AND l.create_datetime < :end");
			paramMap.put("end", slvo.getEnd());
		}

		if (StringUtil.isNotEmpty(slvo.getIp())) {
			sql_whs.append(" AND l.account_ip LIKE :ip");
			paramMap.put("ip", slvo.getIp()+"%");
		}

		sql_order.append(" ORDER BY l.create_datetime DESC");
		
		return super.page2CamelMap(sql_cols.append(sql_tabs).append(sql_whs).append(sql_order).toString(), paramMap);
	}

	/**
	 * 根据站点和投注时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from sys_log where");
		Map map = MixUtil.newHashMap();
		sb.append(" create_datetime <= :createTime");
		sb.append(" and station_id = :stationId");
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}
	
	/**
	 * 用作导出
	 * @param slvo
	 * @return
	 */
	public List<Map> getlist(SysLogVo slvo) {
		StringBuilder sql_cols = new StringBuilder("");
		StringBuilder sql_tabs = new StringBuilder("");
		StringBuilder sql_whs = new StringBuilder("");
		StringBuilder sql_order = new StringBuilder("");
		sql_cols.append("SELECT l.*");
		sql_tabs.append(" FROM sys_log l");
		sql_whs.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();

		if (StationUtil.isAgentStation()
				&& StringUtil.equals(UserUtil.getType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			sql_tabs.append(" INNER JOIN sys_account a ON l.account_id = a.id");
			sql_whs.append(" AND a.account_type = :accountType");
			paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
		}

		if (Validator.isNotNull(slvo.getType())) {
			sql_whs.append(" AND l.log_type = :type");
			paramMap.put("type", slvo.getType());
		}

		if (StringUtil.isNotEmpty(slvo.getAccount())) {
			sql_whs.append(" AND lower(l.account) = :account");
			paramMap.put("account", slvo.getAccount().toLowerCase());
		}
		
		if (StringUtil.isNotEmpty(slvo.getContent())) {
			sql_whs.append(" AND l.log_content LIKE :content");
			paramMap.put("content", "%"+slvo.getContent()+"%");
		}

		if (Validator.isNotNull(slvo.getPlatform())) {
			sql_whs.append(" AND l.platform = :platform");
			paramMap.put("platform", slvo.getPlatform());
		}

		if (Validator.isNotNull(slvo.getStationId())) {
			sql_whs.append(" AND l.station_id = :stationId");
			paramMap.put("stationId", slvo.getStationId());
		}

		if (StringUtil.isNotEmpty(slvo.getBegin())) {
			sql_whs.append(" AND l.create_datetime >= :begin");
			paramMap.put("begin", slvo.getBegin());
		}

		if (StringUtil.isNotEmpty(slvo.getEnd())) {
			sql_whs.append(" AND l.create_datetime < :end");
			paramMap.put("end", slvo.getEnd());
		}

		if (StringUtil.isNotEmpty(slvo.getIp())) {
			sql_whs.append(" AND l.account_ip LIKE :ip");
			paramMap.put("ip", slvo.getIp()+"%");
		}

		sql_order.append(" ORDER BY l.create_datetime DESC");
		
		List<Map> list = super.selectCamelListMap(sql_cols.append(sql_tabs).append(sql_whs).append(sql_order).toString(), paramMap);
		return list;
	}

}
