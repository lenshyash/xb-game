package com.game.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.game.model.MemberRedPacketLevel;

@Repository
public class MemberRedPacketLevelDao extends JdbcUtilImpl<MemberRedPacketLevel> {

	public void deleteByRedPacketId(Long redPacketId) {
		if (redPacketId == null || redPacketId <= 0)
			return;
		update("delete from member_red_packet_level where packet_id=:id", MixUtil.newHashMap("id", redPacketId));
	}

	public List<Long> findByRedPacketId(Long redPacketId) {
		if (redPacketId == null || redPacketId <= 0) {
			return null;
		}
		return query2Obj("select member_level_id from member_red_packet_level where packet_id=:id",
				MixUtil.newHashMap("id", redPacketId), new RowMapper<Long>() {
					@Override
					public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getLong(1);
					}
				});
	}

}
