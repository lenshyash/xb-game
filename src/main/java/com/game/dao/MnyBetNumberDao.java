package com.game.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.model.BetNumRecord;
import com.game.model.SysAccount;
import com.game.util.BigDecimalUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class MnyBetNumberDao extends JdbcUtilImpl<SysAccount> {
	@Autowired
	private BetNumRecordDao betNumRecordDao;

	/**
	 * 累加打码量
	 */
	public void addBetNumberForMember(Long memberId, BigDecimal betNumber, Integer type, String remark, String orderId,
			Date bizDatetime) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account a SET bet_num = COALESCE(bet_num,0) +:betNumber WHERE id = :memberId");
		sql_sb.append(" RETURNING id,account,bet_num,station_id,parents,draw_need");
		SysAccount sa = query21Model(sql_sb.toString(),
				MixUtil.newHashMap("memberId", memberId, "betNumber", betNumber));
		BetNumRecord bnr = new BetNumRecord();
		bnr.setAccount(sa.getAccount());
		bnr.setAccountId(sa.getId());
		bnr.setAfterNum(sa.getBetNum());
		bnr.setBetNum(betNumber);
		bnr.setBeforeNum(BigDecimalUtil.subtract(sa.getBetNum(), betNumber));
		bnr.setStationId(sa.getStationId());
		bnr.setType(type);
		bnr.setRemark(remark);
		if (!StationUtil.isAdminStation()) {
			SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
			if (acc != null) {
				bnr.setOperatorId(acc.getId());
				if (acc.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
					bnr.setOperatorName("--");
				} else {
					bnr.setOperatorName(acc.getAccount());
				}
			}
		} else {
			bnr.setOperatorName("--");
		}
		bnr.setCreateDatetime(new Date());
		bnr.setOrderId(orderId);
		bnr.setParents(sa.getParents());
		bnr.setBeforeDrawNeed(sa.getDrawNeed());
		bnr.setAfterDrawNeed(sa.getDrawNeed());
		bnr.setDrawNeed(BigDecimal.ZERO);
		if (bizDatetime == null) {
			bnr.setBizDatetime(new Date());
		} else {
			bnr.setBizDatetime(bizDatetime);
		}
		betNumRecordDao.insert(bnr);
	}

	/**
	 * 会员余额亏损完毕，打码量清0
	 */
	public void clearBetNumberForMember(Long memberId, BigDecimal betNumber, Integer type, String remark,
			String orderId, Date bizDatetime, BigDecimal betNumberAfter) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account a SET bet_num = :betNumber WHERE id = :memberId");
		sql_sb.append(" RETURNING id,account,bet_num,station_id,parents,draw_need");
		SysAccount sa = query21Model(sql_sb.toString(),
				MixUtil.newHashMap("memberId", memberId, "betNumber", betNumber));
		BetNumRecord bnr = new BetNumRecord();
		bnr.setAccount(sa.getAccount());
		bnr.setAccountId(sa.getId());
		bnr.setAfterNum(betNumberAfter);
		bnr.setBetNum(betNumber);
		bnr.setBeforeNum(betNumber);
		bnr.setStationId(sa.getStationId());
		bnr.setType(type);
		bnr.setRemark(remark);
		if (!StationUtil.isAdminStation()) {
			SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
			if (acc != null) {
				bnr.setOperatorId(acc.getId());
				if (acc.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
					bnr.setOperatorName("--");
				} else {
					bnr.setOperatorName(acc.getAccount());
				}
			}
		} else {
			bnr.setOperatorName("--");
		}
		bnr.setCreateDatetime(new Date());
		bnr.setOrderId(orderId);
		bnr.setParents(sa.getParents());
		bnr.setBeforeDrawNeed(sa.getDrawNeed());
		bnr.setAfterDrawNeed(sa.getDrawNeed());
		bnr.setDrawNeed(BigDecimal.ZERO);
		if (bizDatetime == null) {
			bnr.setBizDatetime(new Date());
		} else {
			bnr.setBizDatetime(bizDatetime);
		}
		betNumRecordDao.insert(bnr);
	}

	public void updateDrawCheck(Long memberId, Long stationId, BigDecimal curDrawNeed, Integer type, String remark,
			boolean clearBetNumber) {
		updateDrawCheck(memberId, stationId, curDrawNeed, type, remark, clearBetNumber, BigDecimal.ZERO);
	}

	public void updateDrawCheck(Long memberId, Long stationId, BigDecimal curDrawNeed, Integer type, String remark,
			boolean clearBetNumber, BigDecimal gifMoney) {
		Map<String, Object> map = null;
		if (clearBetNumber) {
			BigDecimal minmoney = BigDecimalUtil
					.toBigDecimalDefaultZero(StationConfigUtil.get(stationId, StationConfig.bet_num_draw_need_money))
					.add(StringUtil.toBigDecimal(gifMoney));
			map = selectSingleCamelMap(
					"select * from optbetnumber(" + memberId + "," + curDrawNeed + ",true," + minmoney + ")");
		} else {
			map = selectSingleCamelMap("select * from optbetnumber(" + memberId + "," + curDrawNeed + ",false,null)");
		}
		if (map == null)
			return;
		BetNumRecord bnr = new BetNumRecord();
		bnr.setAccount((String) map.get("account"));
		bnr.setAccountId(new Long((Integer) map.get("id")));
		bnr.setAfterNum(BigDecimal.ZERO);
		bnr.setBetNum(BigDecimal.ZERO);
		bnr.setBeforeNum((BigDecimal) map.get("beforeBetNum"));
		bnr.setStationId(new Long((Integer) map.get("stationId")));
		bnr.setType(type);
		bnr.setRemark(remark);
		SysAccount acc = null;
		if(BetNumRecord.TYPE_BALANCE_GEM_REBATE != type){
			acc = (SysAccount) UserUtil.getCurrentUser();
		}
		if (acc != null) {
			bnr.setOperatorId(acc.getId());
			if (acc.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				bnr.setOperatorName("--");
			} else {
				bnr.setOperatorName(acc.getAccount());
			}
		}
		bnr.setCreateDatetime(new Date());
		bnr.setParents((String) map.get("parents"));

		bnr.setAfterDrawNeed((BigDecimal) map.get("afterDrawNeed"));
		bnr.setDrawNeed(curDrawNeed);
		bnr.setBeforeDrawNeed((BigDecimal) map.get("beforeDrawNeed"));
		bnr.setBizDatetime(new Date());
		betNumRecordDao.insert(bnr);
	}

	public boolean addDrawNeed(Long memberId, BigDecimal drawNeed, Integer type, String remark, Date bizDatetime) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account SET draw_need = draw_need + :draw_need");
		sql_sb.append(" WHERE id = :memberId AND draw_need +:draw_need >= 0");
		sql_sb.append(" RETURNING id,account,bet_num,station_id,parents,draw_need");
		SysAccount sa = query21Model(sql_sb.toString(),
				MixUtil.newHashMap("memberId", memberId, "draw_need", drawNeed));
		if (sa == null) {
			return false;
		}
		BetNumRecord bnr = new BetNumRecord();
		bnr.setAccount(sa.getAccount());
		bnr.setAccountId(sa.getId());
		bnr.setAfterNum(sa.getBetNum());
		bnr.setBetNum(BigDecimal.ZERO);
		bnr.setBeforeNum(sa.getBetNum());
		bnr.setStationId(sa.getStationId());
		bnr.setType(type);
		bnr.setRemark(remark);
		SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
		if (acc != null) {
			bnr.setOperatorId(acc.getId());
			if (acc.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
				bnr.setOperatorName("--");
			} else {
				bnr.setOperatorName(acc.getAccount());
			}
		}
		bnr.setCreateDatetime(new Date());
		bnr.setParents(sa.getParents());

		bnr.setAfterDrawNeed(sa.getDrawNeed());
		bnr.setDrawNeed(drawNeed);
		bnr.setBeforeDrawNeed(BigDecimalUtil.subtract(sa.getDrawNeed(), drawNeed));
		if (bizDatetime == null) {
			bnr.setBizDatetime(new Date());
		} else {
			bnr.setBizDatetime(bizDatetime);
		}
		betNumRecordDao.insert(bnr);
		return true;
	}
}
