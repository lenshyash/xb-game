package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccountInfo;
import com.game.util.StationUtil;

@Repository
public class SysAccountInfoDao extends JdbcUtilImpl<SysAccountInfo> {

    public boolean existCardNo(String cardNo, Long excludeId) {
        if (StringUtil.isEmpty(cardNo)) {
            return false;
        }
        StringBuilder sql = new StringBuilder("SELECT count(1)");
        sql.append(" FROM sys_account_info i");
        sql.append(" WHERE i.station_id = :stationId AND i.card_no = :cardNo");

        Map map = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "cardNo", cardNo);
        if (excludeId != null) {
            sql.append(" and account_id<>:excludeId");
            map.put("excludeId", excludeId);
        }
        return queryForInt(sql.toString(), map) > 0;
    }

    public Map getAccountInfoByAccount(String account) {
        StringBuilder sql = new StringBuilder("SELECT i.user_name,i.qz_group_name");
        sql.append(" FROM sys_account_info i");
        sql.append(" WHERE i.account = :account and i.station_id = :stationId");
        Map map = new HashMap();
        map.put("account", account);
        map.put("stationId", StationUtil.getStationId());
        return super.selectSingleCamelMap(sql.toString(), map);
    }
    
    public List<SysAccountInfo> getAccountInfoByGroupName(String groupName,Long stationId){
    	StringBuilder sql_sb = new StringBuilder();
    	sql_sb.append(" select i.account_id,i.account from sys_account_info i where station_id = :stationId and qz_group_name = :groupName");
    	HashMap<Object,Object> paramMap = new HashMap<>();
    	paramMap.put("stationId", stationId);
    	paramMap.put("groupName", groupName);
    	
    	return super.query2Model(sql_sb.toString(), paramMap);
    	
    }

	public void deleteByAccountId(Long id) {
		// TODO Auto-generated method stub
		String sql = "delete from sys_account_info where account_id = :accountId";
		super.update(sql, MixUtil.newHashMap("accountId", id));
		
	}

	public int updateAccountHeadUrl(Long userId, String headUrl) {
		String sql = "update sys_account_info set head_url=:headUrl where account_id = :accountId";
		return super.update(sql, MixUtil.newHashMap("accountId", userId,"headUrl",headUrl));
	}
    public String getUserNameByAccount(Long accountId) {
        StringBuilder sql = new StringBuilder("SELECT i.user_name");
        sql.append(" FROM sys_account_info i");
        sql.append(" WHERE i.account_id = :accountId");
        Map map = new HashMap();
        map.put("accountId", accountId);
        return super.queryForString(sql.toString(), map);
    }
}
