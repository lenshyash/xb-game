package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentComprehensiveCount;
import com.game.model.dictionary.MoneyRecordType;

@Repository
public class ComprehensiveCountDao extends JdbcUtilImpl<AgentComprehensiveCount> {

	public Map getComprehensiveCount(Long stationId, Date countDate) {
		return super.selectSingleCamelMap("SELECT * FROM agent_comprehensive_count WHERE station_id = :stationId AND count_date = :countDate", MixUtil.newHashMap("stationId", stationId, "countDate", countDate));
	}
	
	public List<AgentComprehensiveCount> getComprehensives(Long stationId, Date startDate,Date endDate) {
		return super.query2Model("SELECT * FROM agent_comprehensive_count WHERE station_id = :stationId AND count_date >= :startDate AND count_date <= :endDate", MixUtil.newHashMap("stationId", stationId, "startDate", startDate, "endDate", endDate));
	}

	public List<Map> getMonthReports(Long stationId, Date startTime, Date endTime) {
		StringBuilder sql = new StringBuilder("SELECT TO_CHAR(count_date,'YYYY-MM') AS ym,");
		sql.append("sum(bet_amount) AS bet_amount,sum(lottery_profit) AS lottery_profit,");
		sql.append("sum(sport_profit) AS sport_profit,sum(real_game_profit) AS real_game_profit,sum(electron_profit) AS electron_profit,");
		sql.append("sum(mark_six_profit) AS mark_six_profit from agent_comprehensive_count");
		sql.append(" where count_date <:endTime and count_date>=:startTime and station_id=:stationId");
		sql.append(" GROUP BY ym order by ym desc");
		Map map = MixUtil.newHashMap("startTime", startTime, "endTime", endTime, "stationId", stationId);
		return selectCamelListMap(sql.toString(), map);
	}

	/**
	 * 查询站点是否存在时间内的统计记录
	 * 
	 * @param curDate
	 * @return
	 */
	public AgentComprehensiveCount queryModel(Long stationId, Date countDate) {
		return super.query21Model("SELECT * FROM agent_comprehensive_count WHERE station_id = :stationId and count_date = :countDate", MixUtil.newHashMap("countDate", countDate, "stationId", stationId));
	}

	/**
	 * 根据第三方投注记录，统计投注金额，派奖金额和第三方盈亏
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Map<Long, Map> statisticThirdForOneDay(Date startTime, Date endTime) {
		StringBuilder sql = new StringBuilder("SELECT station_id,sum(bet_amount) as bet_amount,");
		sql.append("sum(award_amount) as award_amount,sum(real_game_profit) as real_game_profit,sum(electron_profit) as electron_profit from (");
		sql.append("(SELECT station_id,sum(-bet_money) AS bet_amount,");
		sql.append("sum(CASE WHEN game_type IN(1,2) THEN (bet_money+pay_money) WHEN game_type=3 THEN pay_money ELSE 0 END) AS award_amount,");
		sql.append("sum(CASE WHEN game_type IN(1,2) THEN -pay_money WHEN game_type=3 THEN (bet_money-pay_money) ELSE 0 END) AS real_game_profit,0 as electron_profit");
		sql.append(" FROM real_game_bet WHERE bet_time >=:startTime AND bet_time <=:endTime group by station_id)");
		sql.append("UNION all");
		sql.append("(SELECT station_id,sum(-bet_money) AS bet_amount,sum(pay_money) AS award_amount,0,sum(bet_money-pay_money) AS electron_profit");
		sql.append(" FROM real_egame_bet WHERE bet_time >=:startTime AND bet_time <=:endTime group by station_id)");
		sql.append(")a group by station_id");
		List list = selectCamelListMap(sql.toString(), MixUtil.newHashMap("startTime", startTime, "endTime", endTime));
		Map<Long, Map> map = new HashMap<>();
		if (list != null && !list.isEmpty()) {
			Map m = null;
			Long stationId = null;
			for (int i = 0, len = list.size(); i < len; i++) {
				m = (Map) list.get(i);
				if (m.get("stationId") != null) {
					try {
						stationId = new Long((Integer) m.get("stationId"));
					} catch (Exception e) {
						stationId = (Long) m.get("stationId");
					}
					map.put(stationId, m);
				}
			}
		}
		return map;
	}

}
