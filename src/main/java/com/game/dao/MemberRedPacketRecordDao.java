package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor.STRING;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberRedPacketRecord;

@Repository
public class MemberRedPacketRecordDao extends JdbcUtilImpl<MemberRedPacketRecord> {

	public Page<MemberRedPacketRecord> getRecordPage(Long stationId, String account, Date begin, Date end) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_red_packet_record WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (StringUtils.isNotEmpty(account)) {
			sql_sb.append(" AND account = :account");
			paramMap.put("account", account);
		}
		if (begin != null) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", begin);
		}
		if (end != null) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<MemberRedPacketRecord> getRecordList(Long stationId, Integer status, Long redPacketId, Date begin, Date end, Integer limit) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_red_packet_record WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (redPacketId != null && redPacketId > 0) {
			sql_sb.append(" AND red_packet_id = :redPacketId");
			paramMap.put("redPacketId", redPacketId);
		}

		if (status != null && status > 0) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		if (begin != null) {
			sql_sb.append(" AND create_datetime => :begin");
			paramMap.put("begin", begin);
		}

		if (end != null) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" order by create_datetime desc");
		if (limit != null && limit > -1) {
			sql_sb.append(" LIMIT :count");
			paramMap.put("count", limit);
		}
		return query2Model(sql_sb.toString(), paramMap);
	}

	public int hanlderUntreated(MemberRedPacketRecord record) {
		StringBuilder sql_sb = new StringBuilder("UPDATE member_red_packet_record SET status = :status,remark = :remark");
		sql_sb.append(" WHERE id = :recordId AND status = ").append(MemberRedPacketRecord.STATUS_UNTREATED);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("recordId", record.getId());
		paramMap.put("status", record.getStatus());
		paramMap.put("remark", record.getRemark());
		return super.update(sql_sb.toString(), paramMap);
	}
	
	/**
	 * 查询红包剩余额度 和 剩余个数
	 * @param rid
	 * @return
	 */
	public Map<Object,Object> getMoneyAndCount(Long rid){
		StringBuilder sql_sb = new StringBuilder("SELECT sum(mrpr.money) money,count(*) num from member_red_packet_record mrpr where mrpr.red_packet_id = :rid");
		Map<Object,Object> paramMap = MixUtil.newHashMap("rid",rid);
		return super.selectSingleMap(sql_sb.toString(), paramMap);
	}
	
	/**
	 * 用户抢了多少个红包记录数
	 * @param rid
	 * @return
	 */
	public int getTodayredPacketNum(String account,Long stationId,Date begin,Date end,Long redPacketId){
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select count(account) from member_red_packet_record");
		sql_sb.append(" where station_id = :stationId");
		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("stationId", stationId);
		if(StringUtil.isNotEmpty(account)) {
			sql_sb.append(" and account = :account");
			params.put("account", account);
		}
		if(StringUtil.isNotEmpty(redPacketId)) {
			sql_sb.append(" and red_packet_id = :redPacketId");
			params.put("redPacketId", redPacketId);
		}
		if(StringUtil.isNotEmpty(begin)) {
			sql_sb.append(" and create_datetime >= :begin");
			params.put("begin", begin);
		}
		if(StringUtil.isNotEmpty(end)) {
			sql_sb.append(" and create_datetime < :end");
			params.put("end", end);
		}
		return super.queryForInt(sql_sb.toString(), params);
		
	}
	/**
	 * 根据用户id 红包id 查询有多少条红包记录
	 * @param accountId
	 * @param redPacketId
	 * @return
	 */
	public int accountRedPacketCount(Long accountId,Long redPacketId) {
		StringBuilder sb = new StringBuilder();
		sb.append("select count(id) from member_red_packet_record where account_id = :accountId and red_packet_id = :redPacketId");
		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("accountId", accountId);
		params.put("redPacketId", redPacketId);
		
		return super.queryForInt(sb.toString(), params);
	}
}
