package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.CommonRedpacketRecord;
import com.game.model.MemberRedPacketRecord;

@Repository
public class CommonRedpacketRecordDao extends JdbcUtilImpl<CommonRedpacketRecord> {

	public Page<CommonRedpacketRecord> getRecordPage(Long stationId, String account, Date begin, Date end) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM common_redpacket_record WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (StringUtils.isNotEmpty(account)) {
			sql_sb.append(" AND account = :account");
			paramMap.put("account", account);
		}
		if (begin != null) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", begin);
		}
		if (end != null) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<CommonRedpacketRecord> getRecordList(Long stationId, Integer status, Long redPacketId, Date begin,
			Date end, Integer limit) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM common_redpacket_record WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (redPacketId != null && redPacketId > 0) {
			sql_sb.append(" AND redpacket_id = :redPacketId");
			paramMap.put("redPacketId", redPacketId);
		}

		if (status != null && status > 0) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		if (begin != null) {
			sql_sb.append(" AND create_datetime => :begin");
			paramMap.put("begin", begin);
		}

		if (end != null) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" order by create_datetime desc");
		if (limit != null && limit > 0) {
			sql_sb.append(" LIMIT :count");
			paramMap.put("count", limit);
		}
		return query2Model(sql_sb.toString(), paramMap);
	}

	public int hanlderUntreated(CommonRedpacketRecord record) {
		StringBuilder sql_sb = new StringBuilder(
				"UPDATE common_redpacket_record SET status = :status, remark = :remark");
		sql_sb.append(" WHERE id = :recordId AND status = ").append(MemberRedPacketRecord.STATUS_UNTREATED);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("recordId", record.getId());
		paramMap.put("status", MemberRedPacketRecord.STATUS_SUCCESS);
		paramMap.put("remark", record.getRemark());
		return super.update(sql_sb.toString(), paramMap);
	}
	
	public CommonRedpacketRecord getAccountRecordById(Long redpacketId,Long userId) {
		StringBuilder sql_sb = new StringBuilder(
				"SELECT * from common_redpacket_record where redpacket_id = :rid");
		sql_sb.append(" AND account_id = :accountId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("rid", redpacketId,"accountId",userId);
		return super.query21Model(sql_sb.toString(), paramMap);
	}

	/**
	 * 查询红包剩余额度 和 剩余个数
	 * 
	 * @param rid
	 * @return
	 */
	public Map<Object, Object> getMoneyAndCount(Long rid) {
		StringBuilder sql_sb = new StringBuilder(
				"SELECT sum(mrpr.money) money,count(*) num from common_redpacket_record mrpr where mrpr.redpacket_id = :rid");
		Map<Object, Object> paramMap = MixUtil.newHashMap("rid", rid);
		return super.selectSingleMap(sql_sb.toString(), paramMap);
	}
}
