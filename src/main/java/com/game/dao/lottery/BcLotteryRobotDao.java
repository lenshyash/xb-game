package com.game.dao.lottery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryRobot;

@Repository
public class BcLotteryRobotDao extends JdbcUtilImpl<BcLotteryRobot>{

	public List<BcLotteryRobot> getRobotRoomIdAll(Long roomId,Long stationId,Integer status) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_robot where room_id=:roomId and station_id=:stationId and status=:status");
		return query2Model(sql.toString(), MixUtil.newHashMap("roomId",roomId,"stationId",stationId,"status",status));
	}

	public Page getAll(Long roomId, Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_robot where station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if(roomId != null && roomId > 0){
			sb.append(" and room_id = :roomId");
			map.put("roomId", roomId);
		}
		sb.append(" order by id");
		return super.paged2Obj(sb.toString(), map);
	}

}
