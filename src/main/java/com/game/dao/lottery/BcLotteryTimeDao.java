package com.game.dao.lottery;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.game.model.lottery.BcLotteryTime;

@Repository
public class BcLotteryTimeDao extends JdbcUtilImpl<BcLotteryTime> {
	/**
	 * @param qihao
	 * @return
	 */
	public Page<BcLotteryTime> page(Integer qihao, String lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_time where 1=1");
		if (qihao != null) {
			sb.append(" and action_no = :qihao");
		}
		sb.append(" and lot_code = :lotType");
		sb.append(" order by action_no ASC");
		return super.paged2Obj(sb.toString(), MixUtil.newHashMap("qihao", qihao, "lotType", lotType));
	}
	
	public List<BcLotteryTime> list(String lotCode) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_time where 1=1");
		if (!StringUtils.isEmpty(lotCode)) {
			sb.append(" and lot_code = :lotCode");
		}sb.append(" order by action_no asc");
		return super.query2Model(sb.toString(),MixUtil.newHashMap("lotCode", lotCode));
	}
	
	

	public void updateTime(Date timeVal, Integer openId) {
		StringBuilder sb = new StringBuilder("update bc_lottery_time set action_time=:timeVal where id= :openId");
		super.update(sb.toString(), MixUtil.newHashMap("timeVal", timeVal, "openId", openId));
	}

	public BcLotteryTime findOneForLot(String lotCode, Date time, Integer minActionNO) {
		Map<Object,Object> paramMap = MixUtil.newHashMap("lotCode", lotCode, "actionNO", minActionNO);
		StringBuilder sb = new StringBuilder("select * from bc_lottery_time where lot_code = :lotCode");
		if (null != time) {
			sb.append(" and action_time>:time");
			paramMap.put("time", new Time(time.getTime()));
		}
		if (minActionNO != null) {
			sb.append(" and action_no >= :actionNO");
		}
		sb.append(" order by action_time asc limit 1");
		return super.query21Model(sb.toString(),paramMap);
	}

	public BcLotteryTime findOneForLotAndActionNO(String lotCode, Integer actionNO) {
		return super.query21Model("select * from bc_lottery_time where lot_code = :lotCode and action_no  = :actionNO",
				MixUtil.newHashMap("lotCode", lotCode, "actionNO", actionNO));
	}
	
}
