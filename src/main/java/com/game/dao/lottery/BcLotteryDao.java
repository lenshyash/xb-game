package com.game.dao.lottery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.BusinessConstant;
import com.game.model.lottery.BcLottery;

@Repository
public class BcLotteryDao extends JdbcUtilImpl<BcLottery> {

	/**
	 * 查询彩种列表
	 * 
	 * @return
	 */
	public Page page(String name, Long stationId, Integer status, Integer modelStatus, Integer identify) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (status != null && status > 0) {
			sb.append(" and status = :status");
			map.put("status", status);
		}

		if (modelStatus != null && modelStatus > 0) {
			sb.append(" and model_status = :modelStatus");
			map.put("modelStatus", modelStatus);
		}

		if (!StringUtils.isEmpty(name)) {
			sb.append(" and name like :name");
			map.put("name", "%" + name.toUpperCase() + "%");
		}

		if (stationId != null && stationId >= 0L) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (identify != null && identify != 0) {
			sb.append(" and identify = :identify");
			map.put("identify", identify);
		}
		sb.append(" order by sort_no desc");
		return super.paged2Obj(sb.toString(), map);
	}

	/**
	 * 获取站点彩种总数
	 * 
	 * @return
	 */
	public Integer countByStationId(Long stationId, Integer identify) {
		StringBuilder sb = new StringBuilder("select count(*) from bc_lottery where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		sb.append("  and identify = :identify");
		map.put("identify", identify);
		return super.queryForInt(sb.toString(), map);
	}

	/**
	 * 查询不在模板彩种里面的数据
	 * 
	 * @param stationId
	 * @return
	 */
	public List<BcLottery> codeByStationId(Long stationId, Integer identify) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery where station_id = 0 and code NOT in(select code from bc_lottery where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		sb.append("  and identify = :identify");
		map.put("identify", identify);
		sb.append(")");
		sb.append("  and identify = :identify");
		map.put("identify", identify);
		return super.query2Model(sb.toString(), map);
	}

	public BcLottery getOne(Long stationId, String code, Integer identify) {
		BcLottery lot = CacheUtil.getCache(CacheType.BC_LOTTERY, stationId + "_" + code + "_" + identify, BcLottery.class);
		if (lot == null) {
			StringBuilder sb = new StringBuilder("select * from bc_lottery where model_status=2");
			Map<Object, Object> map = new HashMap<Object, Object>();
			if (stationId != null) {
				sb.append(" and station_id = :stationId");
				map.put("stationId", stationId);
			}
			if (StringUtils.isNotEmpty(code)) {
				sb.append(" and code = :code");
				map.put("code", code);
			}
			if (identify != null) {
				sb.append(" and identify = :identify");
				map.put("identify", identify);
			}
			List<BcLottery> lots = super.query2Model(sb.toString(), map);
			if (lots != null && !lots.isEmpty()) {
				lot = lots.get(0);
				CacheUtil.addCache(CacheType.BC_LOTTERY, stationId + "_" + code + "_" + identify, lot);
			}
		}
		return lot;
	}
	
	public BcLottery getBcOne(Long stationId, String code, Integer identify) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery where station_id=:stationId and code=:code and identify=:identify");
		return super.query21Model(sql.toString(), MixUtil.newHashMap("stationId",stationId,"code",code,"identify",identify));
	}

	public List<BcLottery> getCzGroup(Long stationId, Integer status, Integer identify) {
		String key = stationId + "_status_" + status + "_i_" + identify;
		List<String> lotCodeIdentifyList = CacheUtil.getCache(CacheType.BC_LOTTERY, key, List.class);
		if (lotCodeIdentifyList != null) {
			List<BcLottery> list = new ArrayList<>();
			String[] codeIdentifies = null;
			for (String codeIdentify : lotCodeIdentifyList) {
				codeIdentifies = StringUtils.split(codeIdentify, "__");
				list.add(getOne(stationId, codeIdentifies[0], NumberUtils.toInt(codeIdentifies[1], BusinessConstant.lottery_identify_V1)));
			}
			return list;
		} else {
			StringBuilder sb = new StringBuilder("SELECT name,code,identify from bc_lottery where 1=1");
			Map map = MixUtil.newHashMap();
			if (stationId != null) {
				sb.append(" and station_id = :stationId");
				map.put("stationId", stationId);
			}
			if (status != null) {
				sb.append(" and status = :status");
				map.put("status", status);
			}
			sb.append(" and model_status=2");
			if (identify != null) {
				sb.append(" and identify= :identify");
				map.put("identify", identify);
			}
			sb.append(" order by sort_no desc");
			List<BcLottery> list = query2Model(sb.toString(), map);
			if (list != null && !list.isEmpty()) {
				lotCodeIdentifyList = new ArrayList<>();
				for (BcLottery bcLottery : list) {
					lotCodeIdentifyList.add(bcLottery.getCode() + "__" + bcLottery.getIdentify());
				}
				CacheUtil.addCache(CacheType.BC_LOTTERY, key, lotCodeIdentifyList);
			}
			return list;
		}
	}

	/**
	 * stationId = 0时,同时改变其他站点的彩种状态 根据code改变所有站点的状态
	 */
	public void updateModelStatusByCode(Integer modelStatus, String code,Integer identify) {
		StringBuilder sb = new StringBuilder("UPDATE bc_lottery set model_status = :modelStatus where code = :code and identify=:identify");
		super.update(sb.toString(), MixUtil.newHashMap("modelStatus", modelStatus, "code", code,"identify",identify));
		CacheUtil.flashDB(CacheType.BC_LOTTERY);
	}

	public List<BcLottery> find(Long stationId, Integer status, Integer modelStatus, Integer type, Integer viewGroup, Integer identify) {
		String key = stationId + "_status_" + status + "_ms_" + modelStatus + "_type_" + type + "_vg_" + viewGroup + "_identify_" + identify;
		List<String> lotCodeIdentifyList = CacheUtil.getCache(CacheType.BC_LOTTERY, key, List.class);
		if (lotCodeIdentifyList != null) {
			List<BcLottery> list = new ArrayList<>();
			String[] codeIdentifies = null;
			BcLottery l=null;
			for (String codeIdentify : lotCodeIdentifyList) {
				codeIdentifies = StringUtils.split(codeIdentify, "__");
				l=getOne(stationId, codeIdentifies[0], NumberUtils.toInt(codeIdentifies[1], BusinessConstant.lottery_identify_V1));
				if(l!=null){
					list.add(l);
				}
			}
			return list;
		} else {
			StringBuilder sb = new StringBuilder("select * from bc_lottery where 1=1");
			Map<Object, Object> paramMap = new HashMap<Object, Object>();
			if (stationId != null) {
				sb.append(" and station_id=:stationId");
				paramMap.put("stationId", stationId);
			}
			if (status != null) {
				sb.append(" and status=:status");
				paramMap.put("status", status);
			}
			if (modelStatus != null) {
				sb.append(" and model_status=:modelStatus");
				paramMap.put("modelStatus", modelStatus);
			}
			if (type != null) {
				sb.append(" and type=:type");
				paramMap.put("type", type);
			}
			if (viewGroup != null) {
				sb.append(" and view_group=:viewGroup");
				paramMap.put("viewGroup", viewGroup);
			}
			if (identify != null && identify != 0) {
				sb.append(" and identify= :identify");
				paramMap.put("identify", identify);
			}
			sb.append(" order by sort_no desc");
			List<BcLottery> list = query2Model(sb.toString(), paramMap);
			if (list != null && !list.isEmpty()) {
				lotCodeIdentifyList = new ArrayList<>();
				for (BcLottery bcLottery : list) {
					lotCodeIdentifyList.add(bcLottery.getCode() + "__" + bcLottery.getIdentify());
				}
				CacheUtil.addCache(CacheType.BC_LOTTERY, key, lotCodeIdentifyList);
			}
			return list;
		}
	}

	// 得到所有匹配站点的可用彩种类型
	public List<Integer> getBigGroup(Long stationId,Integer identify) {
		StringBuilder sb = new StringBuilder("select type from bc_lottery where model_status = 2 and status = 2  and station_id =:stationId ");
		Map<Object, Object> paramMap =  new MixUtil().newHashMap("stationId", stationId);
		if(identify!=null){
			sb.append(" and identify= :identify");
			paramMap.put("identify", identify);
		}
		sb.append(" GROUP BY type order by type");
		return super.selectSingleColList(sb.toString(),paramMap, Integer.class);

	}
	
	public List<BcLottery> findByNotVersion(Long stationId,Integer identify){
		StringBuilder sql = new StringBuilder("select * from bc_lottery where station_id=:stationId and identify!=:identify");
		return super.query2Model(sql.toString(), MixUtil.newHashMap("stationId",stationId,"identify",identify));
	}

	/**
	 * 关闭其他彩种
	 */
	public void closeOtherGames(Long stationId, Integer identify) {
		StringBuilder sb = new StringBuilder("update bc_lottery set model_status = 1 where station_id = :stationId and identify not in (:identify)");
		Map map = MixUtil.newHashMap("stationId", stationId, "identify", identify);
		super.update(sb.toString(), map);
		CacheUtil.flashDB(CacheType.BC_LOTTERY);
	}

	public List<BcLottery> findListByViewGroup(Long stationId, Integer viewGroup, Integer identify) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery where station_id=:stationId and view_group=:viewGroup and identify=:identify");
		return super.query2Model(sql.toString(), MixUtil.newHashMap("stationId",stationId,"viewGroup",viewGroup,"identify",identify));
	}
}
