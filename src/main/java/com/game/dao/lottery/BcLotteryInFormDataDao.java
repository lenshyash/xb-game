package com.game.dao.lottery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryInFormData;

@Repository
public class BcLotteryInFormDataDao extends JdbcUtilImpl<BcLotteryInFormData>{

	public List<BcLotteryInFormData> getPage(Long stationId, Long roomId, Integer typeId) {
		StringBuilder sql  = new StringBuilder("select * from bc_lottery_inform_data where station_id =:stationId and status = 2");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if(roomId != null){
			sql.append(" and room_id =:roomId");
			map.put("roomId", roomId);
		}
		if(typeId != null){
			sql.append(" and type_id =:typeId");
			map.put("typeId", typeId);
		}
		return super.query2Model(sql.toString(), map);
	}

	public Page page(Long roomId, Long stationId, Integer typeId) {
		StringBuilder sql  = new StringBuilder("select * from bc_lottery_inform_data where station_id =:stationId ");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if(roomId != null){
			sql.append(" and room_id =:roomId");
			map.put("roomId", roomId);
		}
		if(typeId != null){
			sql.append(" and type_id =:typeId");
			map.put("typeId", typeId);
		}
		sql.append(" order by id desc");
		return super.paged2Obj(sql.toString(), map);
	}

	public int countByStationId(Long stationId, Long roomId) {
		StringBuilder sb = new StringBuilder("select count(*) from bc_lottery_inform_data where station_id = :stationId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("stationId", stationId);
		if(roomId != null && roomId >0){
			sb.append(" and room_id = :roomId");
			map.put("roomId", roomId);
		}
		return super.queryForInt(sb.toString(), map);
	}

	public List<BcLotteryInFormData> typeIdStationId(Long roomId, Long stationId) {
		//select * from bc_lottery_inform_data where station_id = 0 and type_id not in(select type_id from bc_lottery_inform_data where station_id='28' and room_id='2')
		StringBuilder sql = new StringBuilder("select * from bc_lottery_inform_data where station_id = 0 and type_id not in (");
		sql.append("select type_id from bc_lottery_inform_data where station_id =:stationId and room_id=:roomId )");
		return super.query2Model(sql.toString(), MixUtil.newHashMap("stationId",stationId,"roomId",roomId));
	}

}
