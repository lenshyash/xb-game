package com.game.dao.lottery;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.SendResult;

@Repository
public class SendResultDao extends JdbcUtilImpl<SendResult> {

	public List<SendResult> listByStatus(Date start, Integer status) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_send_result where faile_num<5 and create_time>:start");// 失败次数大于5的不执行
		Map map = MixUtil.newHashMap();
		map.put("start", start);
		if (status != null) {
			sb.append(" and status=:status");
			map.put("status", status);
		}
		return super.query2Model(sb.toString(), map);
	}

	public void deleteData(Date time) {
		update("delete from bc_lottery_send_result where create_time<:time", MixUtil.newHashMap("time", time));
	}
}
