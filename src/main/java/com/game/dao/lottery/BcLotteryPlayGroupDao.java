package com.game.dao.lottery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.lottery.BcLotteryPlayGroup;

@Repository
public class BcLotteryPlayGroupDao extends JdbcUtilImpl<BcLotteryPlayGroup> {

	public Page page(String name, Integer type, Long stationId, Integer status,Integer modelStatus) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (null != type) {
			sb.append(" and lot_type =:type");
			map.put("type", type);
		}
		if (null != status) {
			sb.append(" and status =:status");
			map.put("status", status);
		}
		if(null != modelStatus){
			sb.append(" and model_status =:modelStatus");
			map.put("modelStatus", modelStatus);
		}
		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}
		sb.append(" order by sort_no desc");
		return super.paged2Obj(sb.toString(), map);
	}

	/**
	 * 添加的玩法是否存在
	 * 
	 * @param code
	 * @param type
	 * @param stationId
	 * @return
	 */
	public Integer isExit(String code, Integer type, Long stationId) {
		BcLotteryPlayGroup g = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY_GROUP, getCacheKey(code, stationId, type), BcLotteryPlayGroup.class);
		if (g != null) {
			return 1;
		}
		StringBuilder sb = new StringBuilder("select count(*) from bc_lottery_play_group where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (!StringUtils.isEmpty(code)) {
			sb.append(" and code =:code");
			map.put("code", code);
		}
		if (type != null) {
			sb.append(" and lot_type =:type");
			map.put("type", type);
		}
		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}
		return super.queryForInt(sb.toString(), map);
	}

	/**
	 * 查询站点id stationId
	 * 
	 * @param id
	 * @return
	 */
	public Long getStationId(Long id) {
		if (id == null)
			return 0L;
		BcLotteryPlayGroup g = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY_GROUP, id.toString(), BcLotteryPlayGroup.class);
		if (g != null)
			return g.getStationId();
		StringBuilder sb = new StringBuilder("select station_id from bc_lottery_play_group where id= :id");
		return queryForLong(sb.toString(), MixUtil.newHashMap("id", id));
	}

	/**
	 * 根据code,lotType改变所有站点的状态
	 */
	public void updateModelStatusByCode(Integer status, String code, Integer lotType) {
		StringBuilder sb = new StringBuilder("UPDATE bc_lottery_play_group set model_status = :status where code = :code and lot_type = :lotType");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "code", code, "lotType", lotType));
		CacheUtil.flashDB(CacheType.BC_LOTTERY_PLAY_GROUP);
		CacheUtil.delCacheByPrefix(CacheType.BC_LOTTERY, "gamemenu_script_");
	}

	/**
	 * 获取站点彩种总数
	 * 
	 * @return
	 */
	public Integer countByStationId(Long stationId,Integer identify) {
		StringBuilder sb = new StringBuilder("select count(*) from bc_lottery_play_group where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(identify!=null && identify>0){
			appendsql(sb,identify);
		}
		return super.queryForInt(sb.toString(), map);
	}
	
	private void appendsql(StringBuilder sb,Integer identify){
		switch (identify) {
		case 1:
			sb.append(" and lot_type in (1,2,3,4,5,6,7)");
			break;
		case 2:
			sb.append(" and lot_type in (8,9,10,11,12,13)");
			break;
		}
	}

	/**
	 * 查询不在模板彩种里面的数据
	 * 
	 * @param stationId
	 * @return
	 */
	public List<BcLotteryPlayGroup> codeByStationId(Long stationId,Integer identify) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where station_id = 0 and code NOT in(select code from bc_lottery_play_group where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		sb.append(")");
		if(identify!=null && identify>0){
			appendsql(sb,identify);
		}
		return super.query2Model(sb.toString(), map);
	}

	public void closeOrOpen(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update bc_lottery_play_group set status= :status");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (Validator.isNotNull(id)) {
			sb.append(" where id=:id");
			map.put("id", id);
		}
		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}
		map.put("status", status);
		super.update(sb.toString(), map);
		CacheUtil.flashDB(CacheType.BC_LOTTERY_PLAY);
		if (stationId != null) {
			CacheUtil.delCache(CacheType.BC_LOTTERY, "gamemenu_script_" + stationId);
		} else {
			CacheUtil.delCacheByPrefix(CacheType.BC_LOTTERY, "gamemenu_script_");
		}
	}

	public List<BcLotteryPlayGroup> listByLotTypeAndStationId(Integer type, Long stationId, Integer status, Integer modelStatus) {
		String key = "s_" + stationId + "_t_" + type + "_s_" + status + "_ms_" + modelStatus;
		String idsStr = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY_GROUP, key, String.class);
		if (idsStr != null) {
			List<Long> ids = JSON.parseArray(idsStr, Long.class);
			if (ids != null && !ids.isEmpty()) {
				List<BcLotteryPlayGroup> l = new ArrayList<>();
				for (Long id : ids) {
					l.add(getById(id));
				}
				return l;
			}
		}
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (Validator.isNotNull(type)) {
			sb.append(" and lot_type=:lotType");
			map.put("lotType", type);
		}
		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}
		if (status != null) {
			sb.append(" and status =:status");
			map.put("status", status);
		}
		if (status != null) {
			sb.append(" and model_status =:modelStatus");
			map.put("modelStatus", modelStatus);
		}
		sb.append(" order by sort_no desc");
		List<BcLotteryPlayGroup> l = super.query2Model(sb.toString(), map);
		if (l != null && !l.isEmpty()) {
			List<Long> ids = new ArrayList<>();
			for (BcLotteryPlayGroup pg : l) {
				ids.add(pg.getId());
				setCache(pg);
			}
			CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY_GROUP, key, ids);
		}
		return l;
	}

	public BcLotteryPlayGroup getById(Long id) {
		if (id == null)
			return null;
		BcLotteryPlayGroup g = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY_GROUP, id.toString(), BcLotteryPlayGroup.class);
		if (g == null) {
			StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where id = :id");
			g = super.query21Model(sb.toString(), MixUtil.newHashMap("id", id));
			setCache(g);
		}
		return g;
	}
	
	public BcLotteryPlayGroup getByCodeAndLotType(String code,Integer type,Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where station_id = :stationId");
		sb.append(" and lot_type = :type and code =:code");
		return super.query21Model(sb.toString(), MixUtil.newHashMap("stationId",stationId,"code",code,"type",type));
	}

	private void setCache(BcLotteryPlayGroup g) {
		if (g == null)
			return;
		CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY_GROUP, g.getId().toString(), g);
		CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY_GROUP, getCacheKey(g.getCode(), g.getStationId(), g.getLotType()), g);
	}

	private String getCacheKey(String code, Long stationId, Integer type) {
		return "c_" + code + "_t_" + type + "_s_" + stationId;
	}

	public BcLotteryPlayGroup getByCodeAndStationIdAndType(String code, Long stationId, Integer type) {
		BcLotteryPlayGroup g = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY_GROUP, getCacheKey(code, stationId, type), BcLotteryPlayGroup.class);
		if (g != null) {
			return g;
		} else {
			StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where 1=1");
			sb.append(" and station_id=:stationId");
			sb.append(" and code=:code");
			sb.append(" and lot_type=:type");
			g = super.query21Model(sb.toString(), MixUtil.newHashMap("stationId", stationId, "code", code, "type", type));
			setCache(g);
			return g;
		}
	}
	
	public List<BcLotteryPlayGroup> listByStationId(Long stationId){
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where station_id = :stationId ");
		return super.query2Model(sb.toString(), MixUtil.newHashMap("stationId",stationId));
	}

	public List<BcLotteryPlayGroup> findGroupsByCodes(Long stationId, List<String> list, Integer lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play_group where 1 = 1");
		Map map = MixUtil.newHashMap();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and lot_type = :lotType");
		map.put("lotType", lotType);
		
		if (list!=null && list.size()>0) {
			sb.append(" and code in (");
			for(int i =0;i<list.size();i++){
				if(i<list.size()-1){
					sb.append("'"+list.get(i)+"',");
				}else{
					sb.append("'"+list.get(i)+"'");
				}
				
			}
			sb.append(")");
		}
		return super.query2Model(sb.toString(), map);
	}
}
