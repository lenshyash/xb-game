package com.game.dao.lottery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.redis.RedisAPI;
import com.game.model.lottery.six.BcMarkSix;
import com.game.util.ValidateUtil;

@Repository
public class BcMarkSixDao extends JdbcUtilImpl<BcMarkSix> {

	private static final int CACHE_DB_INDEX = 14;

	// 根据小类ID和租户ID查询相应的选项和赔率
	public List<BcMarkSix> find(Long stationId, String playCode, Integer moduleStatus, Integer status, Integer pageSize,
			Integer lotType, Long roomId) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (moduleStatus != null) {
			sb.append(" and module_status = :moduleStatus");
			map.put("moduleStatus", moduleStatus);
		}
		if (status != null) {
			sb.append(" and status= :status");
			map.put("status", status);
		}
		if (!StringUtil.isEmpty(playCode)) {
			sb.append(" and play_code = :playCode");
			map.put("playCode", playCode);
		}
		if (lotType >= 0) {
			sb.append(" and lot_type= :lotType");
			map.put("lotType", lotType);
		}
		if (roomId != null && roomId > 0) {
			sb.append(" and room_id =:roomId");
			map.put("roomId", roomId);
		}
		sb.append(" order by sort_no desc");
		if (pageSize != null && pageSize != -1) {
			sb.append(" limit :pageSize");
			map.put("pageSize", pageSize);
		}
		return super.query2Model(sb.toString(), map);
	}

	public BcMarkSix findByMarkSixIdAndStationId(Long markSixId, Long stationId, Integer lotType) {
		if (markSixId == null || markSixId <= 0) {
			return null;
		}
		String key = PREFIX + markSixId + "_stationId_" + stationId + "_lot_type_" + lotType;
		String json = RedisAPI.getCache(key, CACHE_DB_INDEX);
		BcMarkSix ms = null;
		if (StringUtils.isNotEmpty(json)) {
			ms = JSON.parseObject(json, BcMarkSix.class);
		}
		if (ms != null) {
			return ms;
		}
		Map<String, Object> map = new HashMap<>();
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (markSixId != null && markSixId > 0) {
			sb.append(" and id = :markSixId");
			map.put("markSixId", markSixId);
		}
		if (lotType != null && lotType > 0) {
			sb.append(" and lot_type =:lotType");
			map.put("lotType", lotType);
		}
		ms = super.query21Model(sb.toString(), map);
		if (ms != null) {
			RedisAPI.addCache(key, JSON.toJSONString(ms), CACHE_DB_INDEX);
			RedisAPI.rpush(PREFIX,CACHE_DB_INDEX, key);
		}
		return ms;
	}

	private static final String PREFIX = "marksix:cachekey:msId_";

	@Override
	public int[] batchUpdate(List<BcMarkSix> list) {
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		return super.batchUpdate(list);
	}

	@Override
	public int update(BcMarkSix t) {
		if (t == null)
			return -1;
		int r = super.update(t);
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		return r;
	}

	@Override
	public BcMarkSix insert(BcMarkSix t) {
		t = super.insert(t);
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		return t;
	}

	@Override
	public BcMarkSix save(BcMarkSix t) {
		if (t == null)
			return t;
		t = super.save(t);
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		return t;
	}
	
	public BcMarkSix getStationMarkSixOdds(String playCode, Long stationId ,Integer lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (StringUtils.isNotEmpty(playCode)) {
			sb.append(" and play_code = :playCode");
			map.put("playCode", playCode);
		}
		if (lotType != null) {
			sb.append(" and lot_type=:lotType");
			map.put("lotType", lotType);
		}
		return super.query21Model(sb.toString(), map);
	}
	
	public BcMarkSix getStationMarkSixOddsByRakeBack(Integer rakeBack, Long stationId ,Integer lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where status = 2");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (Validator.isNotNull(rakeBack)) {
			sb.append(" and rake_back = :rakeBack");
			map.put("rakeBack", rakeBack);
		}
		if (lotType != null) {
			sb.append(" and lot_type=:lotType");
			map.put("lotType", lotType);
		}
		return super.query21Model(sb.toString(), map);
	}

	public BcMarkSix getMarkSixIsNowYearOdds(String playCode, Long stationId, Integer status, Integer moduleStatus,
			Integer lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		sb.append(" and is_now_year=1");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (StringUtils.isNotEmpty(playCode)) {
			sb.append(" and play_code = :playCode");
			map.put("playCode", playCode);
		}
		if (moduleStatus != null) {
			sb.append(" and module_status = :moduleStatus");
			map.put("moduleStatus", moduleStatus);
		}
		if (status != null) {
			sb.append(" and status= :status");
			map.put("status", status);
		}
		if (lotType != null) {
			sb.append(" and lot_type=:lotType");
			map.put("lotType", lotType);
		}
		return super.query21Model(sb.toString(), map);
	}

	public List<BcMarkSix> findByPlayCode(String playCode, String markType, Long stationId, Integer lotType,Integer status) {
		String key = PREFIX + "pc_" + playCode + "_mt_" + markType + "_sid_" + stationId + "_lt_" + lotType;
		String json = RedisAPI.getCache(key, CACHE_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseArray(json, BcMarkSix.class);
		}
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
		}
		if (playCode != null) {
			sb.append(" and play_code= :playCode");
		}
		if (markType != null) {
			sb.append(" and mark_type= :markType");
		}
		if (lotType != null) {
			sb.append(" and lot_type= :lotType");
		}
		if (status != null) {
			sb.append(" and status= :status");
		}
		sb.append(" ORDER BY sort_no DESC");
		List<BcMarkSix> list = super.query2Model(sb.toString(), MixUtil.newHashMap("stationId", stationId, "playCode",
				playCode, "markType", markType, "lotType", lotType,"status",status));
		if (list != null && !list.isEmpty()) {
			RedisAPI.addCache(key, JSON.toJSONString(list), CACHE_DB_INDEX);
			RedisAPI.rpush(PREFIX, CACHE_DB_INDEX,key);
		}
		return list;
	}

	public List<BcMarkSix> findByPlayCode(String playCode, String markType, Long stationId, Integer lotType) {
		String key = PREFIX + "pc_" + playCode + "_mt_" + markType + "_sid_" + stationId + "_lt_" + lotType;
		String json = RedisAPI.getCache(key, CACHE_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseArray(json, BcMarkSix.class);
		}
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		if (stationId != null && stationId > 0) {
			sb.append(" and station_id = :stationId");
		}
		if (playCode != null) {
			sb.append(" and play_code= :playCode");
		}
		if (markType != null) {
			sb.append(" and mark_type= :markType");
		}
		if (lotType != null) {
			sb.append(" and lot_type= :lotType");
		}
		sb.append(" ORDER BY sort_no DESC");
		List<BcMarkSix> list = super.query2Model(sb.toString(), MixUtil.newHashMap("stationId", stationId, "playCode",
				playCode, "markType", markType, "lotType", lotType));
		if (list != null && !list.isEmpty()) {
			RedisAPI.addCache(key, JSON.toJSONString(list), CACHE_DB_INDEX);
			RedisAPI.rpush(PREFIX, CACHE_DB_INDEX,key);
		}
		return list;
	}
	
	public Page page(String playCode, Long stationId, Integer moduleStatus, Integer lotType, Long roomId) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotEmpty(playCode)) {
			sb.append(" and play_code = :playCode");
			map.put("playCode", playCode);
		}
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (moduleStatus != null) {
			sb.append(" and module_status = :moduleStatus");
			map.put("moduleStatus", moduleStatus);
		}
		if (lotType != null) {
			sb.append(" and lot_type = :lotType");
			map.put("lotType", lotType);
		}
		if (roomId != null && roomId != 0) {
			sb.append(" and room_id = :roomId");
			map.put("roomId", roomId);
		}
		sb.append(" order by sort_no desc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public Long getStationId(Long id) {
		StringBuilder sb = new StringBuilder("select station_id from bc_mark_six where id=" + id);
		return super.queryForLong(sb.toString());
	}

	public List<BcMarkSix> findByTeMaAShuZiId(String playCode, String markSixEnum, Long stationId, Integer lotType) {
		StringBuilder sql = new StringBuilder("select * from bc_mark_six where play_code= '" + playCode + "'");
		sql.append(" and mark_type='" + markSixEnum + "'");
		sql.append(" and station_id=" + stationId);
		sql.append(" and lot_type=" + lotType);
		return super.query2Model(sql.toString());
	}

	public BcMarkSix findByOldSix(Long stationId, Integer rakeBack) {
		StringBuilder sql = new StringBuilder(
				"select * from bc_mark_six where station_id = :stationId and rake_back = :rakeBack");
		return super.query21Model(sql.toString(), MixUtil.newHashMap("stationId", stationId, "rakeBack", rakeBack));
	}

	public List<BcMarkSix> findByRakeBack(Integer rakeBack) {
		StringBuilder sql = new StringBuilder(
				"select * from bc_mark_six where station_id != 0 and rake_back=" + rakeBack);
		return super.query2Model(sql.toString());
	}

	public int findByMarkSixCount(Long stationId) {
		return super.queryForInt("select count(id) from bc_mark_six where station_id=" + stationId);
	}

	/**
	 * 根据站点ID查询所有的赔率
	 * 
	 * @return
	 */
	public List<BcMarkSix> listByStationId(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_mark_six");
		sb.append(" where station_id=:stationId");
		return super.query2Model(sb.toString(), MixUtil.newHashMap("stationId", stationId));
	}

	public void delByStationId(Long stationId) {
		if (stationId > 0L) {
			StringBuilder sb = new StringBuilder("delete from bc_mark_six");
			sb.append(" where station_id=:stationId");
			super.update(sb.toString(), MixUtil.newHashMap("stationId", stationId));
			RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		}
	}

	public BcMarkSix findByMark(Long stationId, Integer lotType, Integer rakeBack) {
		StringBuilder sql = new StringBuilder(
				"select * from bc_mark_six where station_id = :stationId and rake_back = :rakeBack and lot_type = :lotType");
		return super.query21Model(sql.toString(),
				MixUtil.newHashMap("stationId", stationId, "rakeBack", rakeBack, "lotType", lotType));
	}

	public BcMarkSix findByCodeAndNameAndType(String playCode, String name, Integer lotType, Long stationId) {
		StringBuilder sql = new StringBuilder(
				"select * from bc_mark_six where play_code=:playCode and name=:name and lot_type=:lotType and station_id=:stationId and room_id=0");
		return super.query21Model(sql.toString(),
				MixUtil.newHashMap("stationId", stationId, "name", name, "lotType", lotType, "playCode", playCode));
	}

	public List<BcMarkSix> findByStationAndLotTypeList(Long stationId, Set<Integer> typeIds) {
		Map<String, Object> map = new HashMap<>();
		StringBuilder sql = new StringBuilder("select * from bc_mark_six where station_id =:stationId");
		map.put("stationId", stationId);
		if (typeIds != null && typeIds.size() > 0) {
			sql.append(" and ( ");
			for (Integer type : typeIds) {
				sql.append(" lot_type=:type_").append(type).append(" or");
				map.put("type_" + type, type);
			}
			sql.deleteCharAt(sql.length() - 1);
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" )");
		}
		return query2Model(sql.toString(), map);
	}

	/**
	 * 以下为六合彩更新数据赔率使用
	 */

	public int findMaxRakeBack() {
		return super.queryForInt("select max(rake_back) from bc_mark_six");
	}

	public BcMarkSix selectRate(String code, Long stationId) {
		String sql = "select * from bc_mark_six where play_code =:code and station_id=:stationId and name=:name";
		return super.query21Model(sql, MixUtil.newHashMap("code", code, "stationId", stationId, "name", "号码1-49"));
	}

	@Override
	public void batchInsert(List<BcMarkSix> list) {
		super.batchInsert(list);
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
	}

	@Override
	public int deletes(long... ids) {
		int i = super.deletes(ids);
		RedisAPI.delCacheByPrefix(PREFIX, CACHE_DB_INDEX);
		return i;
	}

	/**
	 * 以下仅有房间号使用
	 * 
	 * @param lotCode
	 * @param id
	 * @param lotType
	 * @return
	 */
	public List<BcMarkSix> findByRoomId(Long id, Integer lotType, Long stationId, boolean flag) {
		String key = PREFIX + "roomId_" + id + "_lotType_" + lotType + "_stationId_" + stationId + "_flag_" + flag;
		String json = RedisAPI.getCache(key, CACHE_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseArray(json, BcMarkSix.class);
		}
		StringBuilder sb = new StringBuilder("select * from bc_mark_six");
		sb.append(" where station_id=:stationId and room_id=:roomId and lot_type=:lotType and status = 2 ");
		if (flag) {
			sb.append(" and is_now_year >= 6");
		}
		List<BcMarkSix> list = super.query2Model(sb.toString(),
				MixUtil.newHashMap("stationId", stationId, "roomId", id, "lotType", lotType));
		if (list != null && !list.isEmpty()) {
			RedisAPI.addCache(key, JSON.toJSONString(list), CACHE_DB_INDEX);
			RedisAPI.rpush(PREFIX,CACHE_DB_INDEX, key);
		}
		return list;
	}

	public BcMarkSix findByMarkSixIdAndRoomId(Long markSixId, Long stationId, Long roomId) {
		String key = PREFIX + markSixId + "_stationId_" + stationId + "_roomId_" + roomId;
		String json = RedisAPI.getCache(key,CACHE_DB_INDEX);
		BcMarkSix ms = null;
		if (StringUtils.isNotEmpty(json)) {
			ms = JSON.parseObject(json, BcMarkSix.class);
		}
		if (ms != null) {
			return ms;
		}
		StringBuilder sb = new StringBuilder("select * from bc_mark_six");
		sb.append(" where station_id=:stationId and room_id=:roomId and id=:id");
		ms = super.query21Model(sb.toString(),
				MixUtil.newHashMap("stationId", stationId, "roomId", roomId, "id", markSixId));
		if (ms != null) {
			RedisAPI.addCache(key, JSON.toJSONString(ms), CACHE_DB_INDEX);
			RedisAPI.rpush(PREFIX,CACHE_DB_INDEX, key);
		}
		return ms;
	}

	public BcMarkSix findMarkSixByOne(Long stationId, Integer lotType,String playCode, String playName) {
		StringBuilder sql = new StringBuilder(
				"select id,odds,name,play_code,odds,max_bet_ammount,min_bet_ammount from bc_mark_six where lot_type = :lotType and play_code = :playCode and station_id = :stationId and name = :playName");
		return super.query21Model(sql.toString(), MixUtil.newHashMap("lotType", lotType,"playCode",playCode,"stationId", stationId,"playName",playName));
	}
}
