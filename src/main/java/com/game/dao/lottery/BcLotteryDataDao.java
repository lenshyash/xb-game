package com.game.dao.lottery;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.game.common.data.XSort;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.SearchOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jay.frame.FrameProperites;
import org.jay.frame.exception.JayFrameException;
import org.jay.frame.jdbc.*;
import org.jay.frame.jdbc.model.BaseModel;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.constant.BusinessConstant;
import com.game.model.lottery.BcLotteryData;
import com.game.model.lottery.LotteryEnum;
import com.game.model.vo.BcLotteryDataVo;
import com.game.util.DateUtil;

@Repository
public class BcLotteryDataDao extends JdbcUtilImpl<BcLotteryData> {
	public static int BC_LOTTERY_DATA_DB_INDEX = 15;// redis 缓存 db
	public static String CACHE_KEY_PREFIX = "DATA_";// redis 缓存key前缀

	public Page<BcLotteryDataVo> page(Long stationId, String code, Integer actionNo, String qihao, Date startDate,
			Date endDate, String sortName, String sortOrder) {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(ActionUtil.getRequest());
		String key = CACHE_KEY_PREFIX + stationId + "_" + code + "_" + actionNo + "_" + qihao + "_"
				+ DateUtil.toDateStr(startDate) + "_" + DateUtil.toDateStr(endDate) + "_" + webParam.getPageNo() + "_"
				+ webParam.getPageSize()+"_"+ sortName +"_"+ sortOrder;
		Page<Long> idPage = idCacheToPageLong(key);
		if (idPage == null || idPage.getList().isEmpty() || idPage.getList() == null) {
			StringBuilder sb = new StringBuilder(
					"select id from bc_lottery_data where end_time>=:startDate and end_time<:endDate");
			Map<String, Object> map = new HashMap<>();
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			if (stationId != null) {
				sb.append(" and station_id =:stationId");
				map.put("stationId", stationId);
			}
			if (StringUtils.isNotEmpty(code)) {
				sb.append(" and lot_code =:code");
				map.put("code", code);
			}

			if (actionNo != null) {
				sb.append(" and action_no =:actionNo");
				map.put("actionNo", actionNo);
			}
			if (StringUtils.isNotEmpty(qihao)) {
				sb.append(" and qi_hao like :qihao");
				map.put("qihao", "%" + qihao + "%");
			}
			if (StringUtil.isNotEmpty(sortName)) {
				String underline = replaceUpperToUnderline(sortName);
				sb.append(" order by ").append(underline).append(" " + sortOrder);
			} else {
				sb.append(" order by qi_hao desc");
			}
			
			idPage = pagedQuery(sb.toString(), map, webParam, new LongRowMapper(), null);
			idPageToCache(idPage, key, 5356800);// 缓存2个月
		}
		return idPage2ObjPage(idPage, key);
	}

	private Page<Long> idCacheToPageLong(String key) {
		String json = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			JSONObject jobj = JSON.parseObject(json);
			JSONArray arr = jobj.getJSONArray("list");
			List<Long> ids = new ArrayList<>();
			for (int i = 0; i < arr.size(); i++) {
				ids.add(arr.getLong(i));
			}
			return new Page<>(jobj.getIntValue("start"), jobj.getLongValue("totalSize"), jobj.getIntValue("pageSize"),
					ids);
		}
		return null;
	}

	private void idPageToCache(Page<Long> idPage, String key, int timeout) {
		if (idPage != null && idPage.getList() != null) {
			JSONObject jobj = new JSONObject();
			jobj.put("start", idPage.getStart());
			jobj.put("pageSize", idPage.getPageSize());
			jobj.put("totalSize", idPage.getTotalCount());
			JSONArray arr = new JSONArray();
			arr.addAll(idPage.getList());
			jobj.put("list", arr);
			RedisAPI.addCache(key, jobj.toJSONString(), timeout, BC_LOTTERY_DATA_DB_INDEX);
		}
	}

	private Page<BcLotteryDataVo> idPage2ObjPage(Page<Long> idPage, String key) {
		if (idPage != null && idPage.getList() != null) {
			List<BcLotteryDataVo> list = new ArrayList<>();
			BcLotteryDataVo vo = null;
			for (Long id : idPage.getList()) {
				vo = getToVo(id);
				if (vo != null) {
					list.add(vo);
				}
			}
			return new Page<>(idPage.getStart(), idPage.getTotalCount(), idPage.getPageSize(), list);
		}
		return new Page<>();
	}

	public JSONArray findTrendChart(String lotCode, Date startTime, Date endTime, Long stationId, Integer pageSize) {
		String key = CACHE_KEY_PREFIX + stationId + "_" + lotCode + "_" + DateUtil.toDateStr(startTime) + "_"
				+ DateUtil.formatDate(endTime, "yyyy-MM-dd_HH:mm") + "_" + pageSize;
		String json = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		List<Long> ids = null;
		if (StringUtils.isNotEmpty(json)) {
			JSONArray arr = JSON.parseArray(json);
			ids = new ArrayList<>();
			for (int i = 0; i < arr.size(); i++) {
				ids.add(arr.getLong(i));
			}
		}
		if (ids == null) {
			StringBuilder sb = new StringBuilder(
					"select id from bc_lottery_data where hao_ma is not null and hao_ma != ''");
			Map<String, Object> map = new HashMap<>();
			if (stationId != null) {
				sb.append(" and station_id =:stationId");
				map.put("stationId", stationId);
			}
			if (!StringUtils.isEmpty(lotCode)) {
				sb.append(" and lot_code=:lotCode");
				map.put("lotCode", lotCode);
			}
			sb.append(" and end_time >=:startTime and end_time <= :endTime");
			map.put("endTime", endTime);
			map.put("startTime", startTime);
			sb.append(" order by qi_hao desc ");
			if (pageSize != null && pageSize > 0) {
				sb.append(" limit :pageSize");
				map.put("pageSize", pageSize);
			}
			ids = query2Obj(sb.toString(), map, new LongRowMapper());
			// 缓存10分钟
			RedisAPI.addCache(key, JSON.toJSONString(ids), 600, BC_LOTTERY_DATA_DB_INDEX);
		}
		if (ids != null && !ids.isEmpty()) {
			JSONArray jsonArray = new JSONArray();
			BcLotteryDataVo d = null;
			for (Long id : ids) {
				JSONObject jsonObject = new JSONObject();
				d = getToVo(id);
				if (d != null) {
					jsonObject.put("qiHao", d.getQiHao());
					jsonObject.put("haoMa", d.getHaoMa());
					jsonObject.put("openTime", d.getOpenTime());
					jsonObject.put("endTime", d.getEndTime());
					jsonArray.add(jsonObject);
				}
			}
			return jsonArray;
		}
		return null;
	}

	public List<BcLotteryData> find(String lotCode, Date startTime, Date endTime, Long stationId, Integer pageSize) {
		String key = CACHE_KEY_PREFIX + "l_" + lotCode + "_" + DateUtil.formatDate(startTime, "yyyy-MM-dd_HH:mm")
				+ "_e_" + DateUtil.formatDate(endTime, "yyyy-MM-dd_HH:mm") + "_s_" + stationId + "_p_" + pageSize;
		String json = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		List<Long> ids = null;
		if (StringUtils.isNotEmpty(json)) {
			JSONArray arr = JSON.parseArray(json);
			ids = new ArrayList<>();
			for (int i = 0; i < arr.size(); i++) {
				ids.add(arr.getLong(i));
			}
		}
		if (ids == null) {
			StringBuilder sb = new StringBuilder(
					"select id from bc_lottery_data where end_time >=:startTime and end_time <= :endTime");
			Map<String, Object> map = new HashMap<>();
			if (!StringUtils.isEmpty(lotCode)) {
				sb.append(" and lot_code=:lotCode");
				map.put("lotCode", lotCode);
			}
			map.put("endTime", endTime);
			map.put("startTime", startTime);
			if (stationId != null) {
				sb.append(" and station_id = :stationId");
				map.put("stationId", stationId);
			}
			sb.append(" order by qi_hao desc ");
			if (pageSize != null && pageSize > 0) {
				sb.append(" limit :pageSize");
			}
			map.put("pageSize", pageSize);
			map.put("stationId", stationId);
			ids = query2Obj(sb.toString(), map, new LongRowMapper());
			RedisAPI.addCache(key, JSON.toJSONString(ids), 600, BC_LOTTERY_DATA_DB_INDEX);// 缓存10分钟
		}
		if (ids != null && !ids.isEmpty()) {
			List<BcLotteryData> list = new ArrayList<>();
			BcLotteryData d = null;
			for (Long id : ids) {
				d = get(id);
				if (d != null) {
					list.add(d);
				}
			}
			return list;
		}
		return null;
	}

	public BcLotteryData findOne(String lotCode, String qiHao, Long stationId) {
		//String key = CACHE_KEY_PREFIX + lotCode + "_" + qiHao + "_" + stationId;
		//String str = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		/*if (StringUtils.isNotEmpty(str)) {
			Long id = NumberUtils.toLong(str);
			if (id > 0) {
				return get(id);
			}
		}*/
//		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery()
//				.must(QueryBuilders.termQuery("lotCode", lotCode))
//				.must(QueryBuilders.termQuery("qiHao", qiHao))
//				.must(QueryBuilders.termQuery("stationId", stationId));
//		BcLotteryData data =  ElasticApi.searchOne("bc_lottery_data",ElasticApi.DEFAULT_TYPE, queryBuilder,BcLotteryData.class );
//		if(data!=null){
//			return data;
//		}
		String sb = "select * from bc_lottery_data where lot_code=:lotCode and qi_hao = :qiHao and station_id = :stationId";
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("qiHao", qiHao);
		map.put("stationId", stationId);
		BcLotteryData d = query21Model(sb, map);
		//addCache(d);
		return d;

	}


	public BcLotteryData findUnOpenOne(String lotCode, String qiHao, Long stationId) {

		String sb = "select * from bc_lottery_data where lot_code=:lotCode and qi_hao = :qiHao and station_id = :stationId";
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("qiHao", qiHao);
		map.put("stationId", stationId);
		BcLotteryData d = query21Model(sb, map);
		addCache(d);
		return d;

	}


	public Page<BcLotteryData> pageResults(String code, Long stationId, Date startTime, Date endTime) {
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(ActionUtil.getRequest());
		String key = CACHE_KEY_PREFIX + stationId + "_" + code + "_" + DateUtil.toDateStr(startTime) + "_"
				+ DateUtil.formatDate(endTime, "yyyy-MM-dd_HH:mm") + "_" + webParam.getPageNo() + "_"
				+ webParam.getPageSize();
		Page<Long> idPage = idCacheToPageLong(key);
		if (idPage == null) {
			StringBuilder sb = new StringBuilder(
					"select id from bc_lottery_data where end_time >=:startTime and end_time <= :endTime");
			sb.append(" and lot_code =:code and station_id =:stationId order by qi_hao desc");
			idPage = pagedQuery(sb.toString(), MixUtil.newHashMap("code", code, "stationId", stationId, "startTime",
					startTime, "endTime", endTime), webParam, new LongRowMapper(), null);
			idPageToCache(idPage, key, 60000);// 缓存1分钟
		}
		if (idPage != null && idPage.getList() != null) {
			List<BcLotteryData> list = new ArrayList<>();
			BcLotteryData db = null;
			for (Long id : idPage.getList()) {
				db = get(id);
				if (db != null) {
					list.add(db);
				}
			}
			return new Page<>(idPage.getStart(), idPage.getTotalCount(), idPage.getPageSize(), list);
		}
		return new Page<>();
	}

	public BcLotteryData getResultByCode(Long stationId, String lotCode, Date startTime, Date endTime) {
		
		String keyPrefix = CACHE_KEY_PREFIX + "_rbc_" + stationId + "_" + lotCode + "_";
		String key = CACHE_KEY_PREFIX + "_rbc_" + stationId + "_" + lotCode + "_" + DateUtil.toDateStr(startTime) + "_"
				+ DateUtil.formatDate(endTime, "yyyy-MM-dd_HH:mm");
		String str = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		if (StringUtils.isNotEmpty(str)) {
			Long id = NumberUtils.toLong(str);
			if (id > 0) {
				return get(id);
			}
		}
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		if (LotteryEnum.isSysLot(lotCode)) {
			map.put("stationId", stationId);
		} else {
			map.put("stationId", 0);
		}
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		StringBuilder sb = new StringBuilder(
				"select * from bc_lottery_data where end_time >=:startTime and end_time <= :endTime ");
		sb.append(" and hao_ma IS NOT NULL and hao_ma != ''");
		sb.append("and station_id =:stationId and lot_code=:lotCode order by qi_hao desc limit 1");
		BcLotteryData d = super.query21Model(sb.toString(), map);
		addCache(d);
		if (d != null) {
			RedisAPI.addCache(key, d.getId().toString(), 60, BC_LOTTERY_DATA_DB_INDEX);// 缓存1分钟
			RedisAPI.rpush(keyPrefix, BC_LOTTERY_DATA_DB_INDEX,key);
		}
		return d;
	}

	/**
	 * 获取开奖号码
	 * 
	 * @param qiHao
	 * @param lotCode
	 * @param stationId
	 * @return
	 */
	public String getLotteryHaoMa(String qiHao, String lotCode, Long stationId) {
		BcLotteryData d = findOne(lotCode, qiHao, stationId);
		if (d != null) {
			return d.getHaoMa();
		}
		return "";
	}

	// 得到上一期的不为空遗漏
	public String findPreOmmit(String qiHao, String lotCode, Long stationId, Date startTime, Date endTime) {
		StringBuilder sb = new StringBuilder("select ommit from bc_lottery_data where ");
		sb.append(" end_time >=:startTime and end_time <= :endTime");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(qiHao)) {
			sb.append(" and qi_hao < :qiHao");
			map.put("qiHao", qiHao);
		}

		if (org.apache.commons.lang3.StringUtils.isNotEmpty(lotCode)) {
			sb.append(" and lot_code = :lotCode");
			map.put("lotCode", lotCode);
		}

		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}

		sb.append(" and (ommit is not null and ommit<>'') order by end_time desc limit 1");
		return super.queryForString(sb.toString(), map);
	}

	public List<BcLotteryData> findUnAward() {
		Calendar c = Calendar.getInstance();
		Date end = c.getTime();
		c.add(Calendar.DAY_OF_MONTH, -10);
		Date start = c.getTime();
		return query2Model(
				"select * from bc_lottery_data where end_time >= :start and end_time<:end and (open_status=2 or open_status=4)",
				MixUtil.newHashMap("start", start, "end", end));
		// return query2Model("select * from bc_lottery_data where
		// LENGTH(hao_ma)>0 and open_status=2");
	}

	/**
	 * 明天是否有数据
	 */
	// public Integer countByDate(Date start, Date end) {
	// StringBuilder sb = new StringBuilder("select count(*) from
	// bc_lottery_data where end_time >= :start and end_time<:end");
	// return super.queryForInt(sb.toString(), MixUtil.newHashMap("start",
	// start, "end", end));
	// }

	public List<BcLotteryData> findUnOpen(String lotCode, Date start, Date end, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("start", start);
		map.put("end", end);
		map.put("stationId", stationId);
		return query2Model(
				"select * from bc_lottery_data where lot_code=:lotCode and station_id=:stationId and open_status=1 and end_time>:start and end_time<:end",
				map);
	}

	private final static Lock lock = new ReentrantLock();

	/**
	 * 获取最近未开奖的期号
	 * 
	 * @param lotCode
	 * @param date
	 * @param stationId
	 * @return
	 */
	public BcLotteryData getNewest(String lotCode, Date date, Long stationId) {
		String key = CACHE_KEY_PREFIX + "_getNewest_" + lotCode + "_" + DateUtil.formatDate(date, "yyyy-MM-dd_HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if(LotteryEnum.FFSC.name().equals(lotCode)) {
			long index = cal.getTimeInMillis()/1000/75;
			key += "_" + index;
		}
		key += "_" + stationId;
		BcLotteryData d = getNewestFromCache(key);
		if (d != null)
			return d;
		try {
			if (lock.tryLock(10, TimeUnit.SECONDS)) {
				try {
					d = getNewestFromCache(key);
					if (d != null)
						return d;
					Map<String, Object> map = new HashMap<>();
					map.put("lotCode", lotCode);
					map.put("date", date);
					map.put("stationId", stationId);
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					if (StringUtils.equals(lotCode, "LHC") || StringUtils.equals(lotCode, "AMLHC"))  {
						c.add(Calendar.DAY_OF_MONTH, 10);
					} else {
						c.add(Calendar.DAY_OF_MONTH, 1);
					}
					map.put("endTime", c.getTime());
					d = query21Model(
							"select * from bc_lottery_data where lot_code=:lotCode and station_id=:stationId and end_time>:date and end_time < :endTime order by qi_hao asc limit 1",
							map);
					addCache(d);
					if (d != null) {
						RedisAPI.addCache(key, d.getId().toString(), 60, BC_LOTTERY_DATA_DB_INDEX);// 缓存1分钟
					}
					return d;
				} finally {
					lock.unlock();
				}
			}
		} catch (Exception e) {
		}
		return d;
	}

	private BcLotteryData getNewestFromCache(String key) {
		String str = RedisAPI.getCache(key, BC_LOTTERY_DATA_DB_INDEX);
		if (StringUtils.isNotEmpty(str)) {
			Long id = NumberUtils.toLong(str);
			if (id > 0) {
				return get(id);
			}
		}
		return null;
	}

	public int updateStatus(Long id, Date endTime, Integer oldOpenStatus, Date dateTime, Integer newOpenStatus) {
		Map<String, Object> map = new HashMap<>();
		map.put("oldOpenStatus", oldOpenStatus);
		map.put("dateTime", dateTime);
		map.put("newOpenStatus", newOpenStatus);
		map.put("id", id);
		map.put("endTime", endTime);
		int i = update(
				"update bc_lottery_data set open_time=:dateTime,open_status=:newOpenStatus where id=:id and open_status=:oldOpenStatus and end_time=:endTime",
				map);
		if (i == 1) {
			RedisAPI.delCache(BC_LOTTERY_DATA_DB_INDEX, CACHE_KEY_PREFIX + id);
		}
		return i;
	}

	@Override
	public BcLotteryData get(Serializable id) {
		/*if (id == null) {
			return null;
		}
		String json = RedisAPI.getCache(CACHE_KEY_PREFIX + id, BC_LOTTERY_DATA_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseObject(json, BcLotteryData.class);
		}*/
		BcLotteryData d = query21Model("select * from bc_lottery_data where id=:id", MixUtil.newHashMap("id", id));
		//addCache(d);
		return d;
	}

	private void addCache(BcLotteryData d) {
		if (d != null) {// 缓存2天
			RedisAPI.addCache(CACHE_KEY_PREFIX + d.getId(), JSON.toJSONString(d), 3600*24*2, BC_LOTTERY_DATA_DB_INDEX);
			RedisAPI.addCache(CACHE_KEY_PREFIX + d.getLotCode() + "_" + d.getQiHao() + "_" + d.getStationId(),
					d.getId().toString(), 3600*24*2, BC_LOTTERY_DATA_DB_INDEX);
		}
	}

	public BcLotteryDataVo getToVo(Long id) {
		if (id == null) {
			return null;
		}
		String json = RedisAPI.getCache(CACHE_KEY_PREFIX + id, BC_LOTTERY_DATA_DB_INDEX);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseObject(json, BcLotteryDataVo.class);
		}
		List<BcLotteryDataVo> l = query2Obj("select * from bc_lottery_data where id=:id", MixUtil.newHashMap("id", id),
				new BeanPropertyRowMapper<>(BcLotteryDataVo.class));
		if (l != null && l.size() > 0) {// 缓存永远不过期
			RedisAPI.addCache(CACHE_KEY_PREFIX + id, JSON.toJSONString(l.get(0)), 5356800, BC_LOTTERY_DATA_DB_INDEX);
			RedisAPI.addCache(CACHE_KEY_PREFIX + l.get(0).getLotCode() + "_" + l.get(0).getQiHao() + "_"
					+ l.get(0).getStationId(), id.toString(), 5356800, BC_LOTTERY_DATA_DB_INDEX);
			return l.get(0);
		}
		return null;
	}

	@Override
	public int update(BcLotteryData t) {
		if (t == null || t.getId() == null)
			return 0;
		int i = super.update(t);
		if (i == 1) {
			addCache(t);
		}
		return i;
	}

	@Override
	public int delete(BcLotteryData t) {
		if (t == null || t.getId() == null)
			return 0;
		int i = super.delete(t);
		RedisAPI.delCache(BC_LOTTERY_DATA_DB_INDEX, CACHE_KEY_PREFIX + t.getId());
		return i;
	}

	@Override
	public int delete(Serializable id) {
		if (id == null)
			return 0;
		int i = super.delete(id);
		RedisAPI.delCache(BC_LOTTERY_DATA_DB_INDEX, CACHE_KEY_PREFIX + id);
		return i;
	}

	@Override
	public BcLotteryData save(BcLotteryData t) {
		t = super.save(t);
		addCache(t);
		return t;
	}

	public void reopenLHC(BcLotteryData bcData) {
		if (bcData == null || bcData.getId() == null || bcData.getEndTime() == null) {
			return;
		}
		Map<String, Object> map = new HashMap<>();
		map.put("id", bcData.getId());
		map.put("haoMa", null);
		map.put("endTime", bcData.getEndTime());
		map.put("status", BusinessConstant.LOTTERY_OPEN_STATUS_UNOPEN);
		update("update bc_lottery_data set hao_ma=:haoMa,open_status=:status where id=:id and end_time=:endTime", map);
		bcData.setHaoMa(null);
		bcData.setOpenStatus(BusinessConstant.LOTTERY_OPEN_STATUS_UNOPEN);
		addCache(bcData);
	}

	public void updateEndTime(BcLotteryData bcData, Date endTime) {
		if (bcData == null || bcData.getId() == null || bcData.getEndTime() == null || endTime == null) {
			return;
		}
		Map<String, Object> map = new HashMap<>();
		map.put("id", bcData.getId());
		map.put("endTime", bcData.getEndTime());
		map.put("newEndTime", endTime);
		update("update bc_lottery_data set end_time=:newEndTime where id=:id and end_time=:endTime", map);
		bcData.setEndTime(endTime);
		addCache(bcData);
	}

	public void updateHaoMaAndOmmit(BcLotteryData bcData, int oldStatus) {
		if (bcData == null || bcData.getId() == null || bcData.getEndTime() == null
				|| StringUtils.isEmpty(bcData.getHaoMa())) {
			return;
		}
		if (oldStatus != BusinessConstant.LOTTERY_OPEN_STATUS_UNOPEN
				&& oldStatus != BusinessConstant.LOTTERY_OPEN_STATUS_ROLL) {
			return;
		}
		Map<String, Object> map = new HashMap<>();
		map.put("id", bcData.getId());
		map.put("haoMa", bcData.getHaoMa());
		map.put("endTime", bcData.getEndTime());
		map.put("status", bcData.getOpenStatus());
		map.put("ommit", bcData.getOmmit());
		map.put("oldStatus", oldStatus);
		update("update bc_lottery_data set hao_ma=:haoMa,open_status=:status,ommit=:ommit where id=:id and end_time=:endTime and open_status=:oldStatus",
				map);
		String key = CACHE_KEY_PREFIX + "_rbc_" + bcData.getStationId() + "_" + bcData.getLotCode() + "_";
		RedisAPI.delCacheByPrefix(key, BC_LOTTERY_DATA_DB_INDEX);
		addCache(bcData);
	}

	public long countForOneDay(String lotCode, Long stationId, Date startTime, Date endTime) {
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		map.put("stationId", stationId);
		return queryForLong(
				"select count(*) from bc_lottery_data where lot_code=:lotCode and end_time>=:startTime and end_time<=:endTime and station_id = :stationId ",
				map);
	}

	public List<BcLotteryData> findLastList(String lotCode, String qiHao, Date endTime, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("qiHao", qiHao);
		map.put("endTime", endTime);
		map.put("stationId", stationId);
		return query2Model(
				"select * from bc_lottery_data where lot_code=:lotCode and qi_hao<=:qiHao and end_time>:endTime and station_id = :stationId order by end_time desc limit 1",
				map);
	}

	public void updateLimitBetAccount(BcLotteryData bcData) {
		if (bcData == null || bcData.getId() == null || bcData.getEndTime() == null) {
			return;
		}
		Map<String, Object> map = new HashMap<>();
		map.put("id", bcData.getId());
		map.put("limitBetAccount", bcData.getLimitBetAccount());
		map.put("endTime", bcData.getEndTime());
		update("update bc_lottery_data set limit_bet_account=:limitBetAccount where id=:id and end_time=:endTime", map);
		addCache(bcData);
	}
	
	public void deleteLotteryData(Long lotteryDataId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("delete from bc_lottery_data where id = "+lotteryDataId);
		super.update(buffer.toString());
	}
	
	public List<String> findLastHistoryForCode(String lotCode,  Long stationId,Integer limitNum) {
		Map<String, Object> map = new HashMap<>();
		map.put("lotCode", lotCode);
		map.put("stationId", stationId);
		map.put("limitNum", limitNum);
		return query2Obj(
				"select * from bc_lottery_data where lot_code = :lotCode and  station_id = :stationId and open_status in (2,3) ORDER BY end_time desc LIMIT :limitNum",
				map, new LongRowMapper());
	}
	public String findLimitBetAccount(Long id) {
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		return queryForString(
				"select limit_bet_account from bc_lottery_data where id = :id",map);
	}
	
	private String replaceUpperToUnderline(String str) {
		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile("[A-Z]+");
		Matcher m1 = p.matcher(str); // 判断是否含有大写字符
		while (m1.find()) {
			m1.appendReplacement(sb, "_" + m1.group().toLowerCase());
		}
		m1.appendTail(sb);
		return sb.toString();
	}


	public void updateDataTime(Long id, Date startTime, Date endTime) {

		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("endTime", endTime);
		map.put("startTime", startTime);
		update("update bc_lottery_data set end_time=:endTime, start_time=:startTime where id=:id", map);
	}

	/**
	 * 针对插入处理 同步返回新增ID
	 * @param d
	 * @return
	 * @throws IllegalAccessException
	 */
	public BcLotteryData insertOne(BcLotteryData d) throws IllegalAccessException, InvocationTargetException {
		JdbcModel model = get();
		final JdbcColumn primaryCol = model.getPrimaryCol();
		primaryCol.setColumnName("id");
		final String sql;
		if (primaryCol != null && primaryCol.getGenerator() == 1) {
			ListParameter parameterMaker = new ListParameter();
			SqlMaker.getAddSql(model, parameterMaker);
			sql = parameterMaker.getSql();
			final List paramList = parameterMaker.toParameters(d, 2);
			JdbcDAOImpl.getLogInfo(sql, paramList);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			super.getJdbcOperations().update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = null;
					if (FrameProperites.DB_TYPE == DBType.POSTGRESQL) {
						String primaryColumn = primaryCol.getColumnName();
						ps = conn.prepareStatement(sql, new String[]{primaryColumn});
					} else {
						ps = conn.prepareStatement(sql, 1);
					}

					for(int i = 0; i < paramList.size(); ++i) {
						Object val = paramList.get(i);
						setSqlParameter(ps, i + 1, val);
					}
					return ps;
				}
			}, keyHolder);
			Integer id = (Integer)keyHolder.getKeyList().get(0).get("id");
			d.setId(id.longValue());
		} else {
			if (primaryCol != null && primaryCol.getGenerator() == 3) {
				primaryCol.setValue(d, this.getSequence(primaryCol.getSequence()));
			}
			MapParameter parameterMaker = new MapParameter();
			SqlMaker.getAddSql(model, parameterMaker);
			sql = parameterMaker.getSql();
			Map params = parameterMaker.toParameters(d, 2);
			super.update(sql, params);
		}
		return d;
	}

	private JdbcModel get() {
		Class c = this.getGenericType(0);
		JdbcModel jdbcModel = JdbcModelSet.get(c);
		if (jdbcModel == null) {
			throw new JayFrameException(10004, new String[]{c.getName()});
		} else {
			return jdbcModel;
		}
	}
	private void setSqlParameter(PreparedStatement ps, int i, Object val) throws SQLException {
		if (val != null && FrameProperites.DB_TYPE == DBType.POSTGRESQL && val.getClass() == Date.class) {
			ps.setTimestamp(i, new Timestamp(((Date)val).getTime()));
		} else {
			ps.setObject(i, val);
		}

	}
}
