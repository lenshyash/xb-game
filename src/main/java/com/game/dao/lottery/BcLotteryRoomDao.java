package com.game.dao.lottery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryRoom;

@Repository
public class BcLotteryRoomDao extends JdbcUtilImpl<BcLotteryRoom>{

	public List<BcLotteryRoom> findByCodeRoom(Long stationId, String code, Integer statusEnable) {
		return query2Model("select * from bc_lottery_room where station_id=:stationId and lot_code =:code and status =:status order by sort_no desc", MixUtil.newHashMap("stationId", stationId, "code", code,"status",statusEnable));
	}

	public List<BcLotteryRoom> getRoomAll(Integer status,String code) {
		return query2Model("select * from bc_lottery_room where status =:status and lot_code=:code order by id desc",MixUtil.newHashMap("status",status,"code",code));
	}

	public List<BcLotteryRoom> findByLotTypeAndStationId(Integer lotType, Long stationId,Integer status) {
		return query2Model("select * from bc_lottery_room where station_id=:stationId and lot_type =:lotType and status =:status order by sort_no desc", MixUtil.newHashMap("stationId", stationId, "lotType", lotType,"status",status));
	}

	public Page page(Integer lotType, Long stationId, String lotCode) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_room where station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if(lotType != null && lotType >0){
			sb.append(" lot_type = :lotType");
			map.put("lotType", lotType);
		}
		if(StringUtils.isNotEmpty(lotCode)){
			sb.append(" lot_code = :lotCode");
			map.put("lotCode", lotCode);
		}
		sb.append(" order by sort_no desc");
		return super.paged2Obj(sb.toString(), map);
	}
	
	public List<BcLotteryRoom> getRoomStationId(Long stationId){
		return query2Model("select * from bc_lottery_room where station_id=:stationId order by sort_no desc", MixUtil.newHashMap("stationId", stationId));
	}

}
