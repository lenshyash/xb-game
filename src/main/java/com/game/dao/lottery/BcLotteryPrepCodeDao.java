package com.game.dao.lottery;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryPrepCode;

@Repository
public class BcLotteryPrepCodeDao extends JdbcUtilImpl<BcLotteryPrepCode> {

	public BcLotteryPrepCode findByLotCodeAndQiHao(String lotCode, String qiHao, Long stationId) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_prep_code where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotEmpty(lotCode)) {
			sql.append(" and lot_code=:lotCode");
			map.put("lotCode", lotCode);
		}
		if (StringUtils.isNotEmpty(qiHao)) {
			sql.append(" and qi_hao=:qiHao");
			map.put("qiHao", qiHao);
		}
		if (stationId != null) {
			sql.append(" and station_id=:stationId");
			map.put("stationId", stationId);
		}
		return query21Model(sql.toString(), map);
	}

	public BcLotteryPrepCode getByLotAndQihaoAndStationId(String lotCode, String qihao, Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_prep_code where 1=1");

		Map map = MixUtil.newHashMap();
		if (StringUtils.isNotEmpty(lotCode)) {
			sb.append(" and lot_code =:lotCode");
			map.put("lotCode", lotCode);
		}

		if (StringUtils.isNotEmpty(qihao)) {
			sb.append(" and qi_hao =:qihao");
			map.put("qihao", qihao);
		}

		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}

		return super.query21Model(sb.toString(), map);

	}

	public Page<BcLotteryPrepCode> page(String qiHao, String operator, String lotCode, Date startTime, Date endTime, Long stationId) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_prep_code where station_id=:stationId ");
		Map<String,Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if(StringUtils.isNotEmpty(qiHao)){
			sql.append(" and qi_hao=:qiHao");
			map.put("qiHao", qiHao);
		}
		if(StringUtils.isNotEmpty(operator)){
			sql.append(" and operator=:operator");
			map.put("operator", operator);
		}
		if(StringUtils.isNotEmpty(lotCode)){
			sql.append(" and lot_code=:lotCode");
			map.put("lotCode", lotCode);
		}
		if(startTime != null){
			sql.append(" and create_time>=:startTime");
			map.put("startTime", startTime);
		}
		if(endTime != null){
			sql.append(" and create_time<:endTime");
			map.put("endTime", endTime);
		}
		sql.append(" order by create_time desc");
		return super.paged2Obj(sql.toString(), map);
	}
}
