package com.game.dao.lottery;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryJointPurchase;
import com.game.util.DateUtil;

@Repository
public class BcLotteryJointPurchaseDao extends JdbcUtilImpl<BcLotteryJointPurchase> {

	public Page<BcLotteryJointPurchase> page(String lotCode, String createTime, String endTime, Integer type, String account,String programId, Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_joint_purchase where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotEmpty(lotCode)) {
			sb.append(" and lot_code=:lotCode ");
			map.put("lotCode", lotCode);
		}
		if (StringUtils.isNotEmpty(createTime)) {
			sb.append(" and create_time>=:createTime ");
			map.put("createTime", DateUtil.toDatetime(createTime));
		}
		if (StringUtils.isNotEmpty(endTime)) {
			sb.append(" and create_time<=:endTime ");
			map.put("endTime", DateUtil.toDatetime(endTime));
		}
		sb.append(" and station_id=:stationId");
		map.put("stationId", stationId);
		if (type != null) {
			sb.append(" and type =:type ");
			map.put("type", type);
		}
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(account) = :account");
			map.put("account", account.toLowerCase());
		}
		if(StringUtils.isNotEmpty(programId)){
			sb.append(" and program_id=:programId");
			map.put("programId", programId);
		}
		sb.append(" order by create_time desc");
		return super.paged2Obj(sb.toString(), map);
	}

	public List<BcLotteryJointPurchase> findByTimeEndAndType(int type, Date endTime) {
		return query2Model("select * from bc_lottery_joint_purchase where type=:type and end_time<=:endTime", MixUtil.newHashMap("type", type, "endTime", endTime));
	}

	public BcLotteryJointPurchase findByProgramId(String programId, Long stationId) {
		return query21Model("select * from bc_lottery_joint_purchase where program_id=:programId and station_id=:stationId", MixUtil.newHashMap("programId", programId, "stationId", stationId));
	}

	public List<BcLotteryJointPurchase> findByLotCode(String lotCode, Long stationId) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_joint_purchase where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotEmpty(lotCode)) {
			sql.append(" and lot_code =:lotCode");
			map.put("lotCode", lotCode);
		}
		sql.append(" and station_id =:stationId ");
		map.put("stationId", stationId);
		sql.append(" order by create_time desc limit 10");
		return query2Model(sql.toString(), map);
	}

	public int addAwardInfo(String programId, Boolean isWin, BigDecimal winMoney) {
		if (StringUtils.isEmpty(programId)) {
			return 0;
		}
		String sql = "update bc_lottery_joint_purchase set cal_num=COALESCE(cal_num,0)+:calNum,win_num=COALESCE(win_num,0)+:winNum,win_total_money=COALESCE(win_total_money,0)+:winMoney where program_id=:programId";
		Map<String, Object> map = new HashMap<>();
		map.put("calNum", 1);
		map.put("winNum", (isWin == null || !isWin) ? 0 : 1);
		map.put("winMoney", winMoney == null ? BigDecimal.ZERO : winMoney);
		map.put("programId", programId);
		return update(sql, map);
	}

	public List<BcLotteryJointPurchase> findUnAward() {
		Calendar c = Calendar.getInstance();
		Date end = c.getTime();
		c.add(Calendar.DAY_OF_MONTH, -10);
		Date start = c.getTime();
		return query2Model("select * from bc_lottery_joint_purchase where end_time >= :start and end_time<:end and (type=2 or type=3) and cal_num=buy_num", MixUtil.newHashMap("start", start, "end", end));
	}

	public int updateType(Long id, Date endTime, Integer type, Integer newType) {
		if (id == null || endTime == null || newType == null) {
			return 0;
		}
		String sql = "update bc_lottery_joint_purchase set type=:newType where id=:id and end_time=:endTime and type=:type";
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("endTime", endTime);
		map.put("type", type);
		map.put("newType", newType);
		return update(sql, map);
	}

}
