package com.game.dao.lottery;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryDataStation;

@Repository
public class BcLotteryDataStationDao extends JdbcUtilImpl<BcLotteryDataStation> {

	public Integer saveOrUpdate(Long lotDataId, String lotCode, String qiHao, String haoMa, Long stationId, Integer openStatus) {
		if (StringUtils.isEmpty(lotCode) || StringUtils.isEmpty(qiHao) || stationId == null || stationId == 0L) {
			return 0;
		}
		BcLotteryDataStation old = findByLot(lotCode, qiHao, stationId);
		if (old != null) {
			old.setOpenStatus(openStatus);
			return update(old);
		} else {
			old = new BcLotteryDataStation();
			old.setHaoMa(haoMa);
			old.setQiHao(qiHao);
			old.setLotCode(lotCode);
			old.setLotDataId(lotDataId);
			old.setStationId(stationId);
			old.setOpenStatus(openStatus);
			insert(old);
			return 1;
		}
	}

	public BcLotteryDataStation findByLot(String lotCode, String qiHao, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("qiHao", qiHao);
		map.put("lotCode", lotCode);
		return query21Model("select * from bc_lottery_data_station where station_id=:stationId and lot_code=:lotCode and qi_hao=:qiHao", map);
	}

}
