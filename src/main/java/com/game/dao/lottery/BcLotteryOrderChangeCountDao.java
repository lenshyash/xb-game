package com.game.dao.lottery;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryOrderChangeCount;

@SuppressWarnings("unchecked")
@Repository
public class BcLotteryOrderChangeCountDao extends JdbcUtilImpl<BcLotteryOrderChangeCount> {

	public BcLotteryOrderChangeCount queryChangeCount(Long stationId, String statDate) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM bc_lottery_order_change_count");
		sb.append("  WHERE station_id =:stationId AND stat_date =:statDate");
		return super.query21Model(sb.toString(), MixUtil.newHashMap("stationId", stationId,"statDate", statDate));
	}
	
	public int updateCount(Long stationId, String statDate) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE bc_lottery_order_change_count SET");
		sb.append(" COUNT = COUNT+1 ");
		sb.append("  WHERE station_id =:stationId AND stat_date =:statDate");
		return super.update(sb.toString(), MixUtil.newHashMap("stationId", stationId,"statDate", statDate));
	}

}
