package com.game.dao.lottery;

import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryDataLast;

@Repository
public class BcLotteryDataLastDao extends JdbcUtilImpl<BcLotteryDataLast> {

	public List<BcLotteryDataLast> getAll() {
		return super.query2Model("SELECT * FROM bc_lottery_data_last");
	}

	public BcLotteryDataLast findOne(Long stationId, String lotCode) {
		return super.query21Model(
				"SELECT * FROM bc_lottery_data_last WHERE station_id = :stationId AND lot_code = :lotCode",
				MixUtil.newHashMap("stationId", stationId, "lotCode", lotCode));
	}

}
