package com.game.dao.lottery;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.lottery.BcLotteryPlay;
import com.game.model.lottery.BcLotteryPlayGroup;

@Repository
public class BcLotteryPlayDao extends JdbcUtilImpl<BcLotteryPlay> {
	public Page page(Long groupId, Long stationId,Integer status) {
		if(groupId==null || groupId==0){
			return new Page<>();
		}
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1=1");
		sb.append(" and group_id = :groupId");
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
		}
		if (status != null) {
			sb.append(" and status = :status");
		}
		sb.append(" order by sort_no desc");
		return super.page2CamelMap(sb.toString(), MixUtil.newHashMap("groupId", groupId, "stationId", stationId,"status",status));
	}

	public Long getStationId(Long id) {
		if (id == null)
			return 0L;
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, id.toString(), BcLotteryPlay.class);
		if (p != null)
			return p.getStationId();
		p = getById(id);
		if (p != null) {
			return p.getStationId();
		}
		return 0L;
	}

	public int isExist(Long stationId, Integer lotType, String playCode) {
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, getCacheKey(playCode, stationId, lotType), BcLotteryPlay.class);
		if (p != null) {
			return 1;
		}
		p = getByStationIdAndCodeAndType(stationId, playCode, lotType);
		if (p != null) {
			return 1;
		}
		return 0;
	}

	private void setCache(BcLotteryPlay p) {
		if (p == null)
			return;
		CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY, p.getId().toString(), p);
		CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY, getCacheKey(p.getCode(), p.getStationId(), p.getLotType()), p);
	}

	private String getCacheKey(String code, Long stationId, Integer type) {
		return "c_" + code + "_t_" + type + "_s_" + stationId;
	}

	/**
	 * 根据code改变所有站点的状态
	 */
	public void updateModelStatusByCodeAndLotType(Integer ModelStatus, String code, Integer lotType) {
		StringBuilder sb = new StringBuilder("UPDATE bc_lottery_play set model_status = :ModelStatus where code = :code");
		sb.append(" and lot_Type = :lotType");
		super.update(sb.toString(), MixUtil.newHashMap("ModelStatus", ModelStatus, "code", code, "lotType", lotType));
		CacheUtil.flashDB(CacheType.BC_LOTTERY_PLAY);
		CacheUtil.delCacheByPrefix(CacheType.BC_LOTTERY, "gamemenu_script_");
	}

	public void closeOrOpen(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update bc_lottery_play set status= :status where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (Validator.isNotNull(id)) {
			sb.append(" and id=:id");
			map.put("id", id);
		}
		if (stationId != null) {
			sb.append(" and station_id =:stationId");
			map.put("stationId", stationId);
		}
		map.put("status", status);
		super.update(sb.toString(), map);
		CacheUtil.flashDB(CacheType.BC_LOTTERY_PLAY);
		if (stationId != null) {
			CacheUtil.delCache(CacheType.BC_LOTTERY, "gamemenu_script_" + stationId);
		} else {
			CacheUtil.delCacheByPrefix(CacheType.BC_LOTTERY, "gamemenu_script_");
		}
	}

	public List<BcLotteryPlay> listByGroupId(Long stationId, Long groupId, Integer status, Integer modelStatus) {
		String key = "s_" + stationId + "_g_" + groupId + "_ms_" + modelStatus;
		List<BcLotteryPlay> l = getListFromCache(key);
		if (l != null)
			return l;
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}

		if (status != null) {
			sb.append(" and status = :status");
			map.put("status", status);
		}

		if (modelStatus != null) {
			sb.append(" and model_status = :modelStatus");
			map.put("modelStatus", modelStatus);
		}

		if (!StringUtil.isEmpty(groupId)) {
			sb.append(" and group_id = :groupId");
			map.put("groupId", groupId);
		}
		sb.append(" order by sort_no desc");
		return listToCache(super.query2Model(sb.toString(), map), key);
	}

	public BcLotteryPlay getByStationIdAndCodeAndType(Long stationId, String playCode, Integer lotType) {
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, getCacheKey(playCode, stationId, lotType), BcLotteryPlay.class);
		if (p != null) {
			return p;
		}
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1=1");
		Map<String, Object> map = new HashMap<>();
		if (stationId != null) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if (!StringUtil.isEmpty(playCode)) {
			sb.append(" and code = :code");
			map.put("code", playCode);
		}
		if (lotType != null) {
			sb.append(" and lot_type = :type");
			map.put("type", lotType);
		}
		p = super.query21Model(sb.toString(), map);
		setCache(p);
		return p;

	}

	public void changeStatus(Long id, Integer status) {
		StringBuilder sb = new StringBuilder("update from bc_lottery_play set status = :status where id = :id");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "id", id));
		CacheUtil.flashDB(CacheType.BC_LOTTERY_PLAY);
		BcLotteryPlay p = getById(id);
		if (p != null) {
			CacheUtil.delCache(CacheType.BC_LOTTERY, "gamemenu_script_" + p.getStationId());
		}
	}

	public BcLotteryPlay getById(Long id) {
		if (id == null)
			return null;
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, id.toString(), BcLotteryPlay.class);
		if (p != null)
			return p;
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where id = :id");
		p = super.query21Model(sb.toString(), MixUtil.newHashMap("id", id));
		setCache(p);
		return p;
	}

	public List<BcLotteryPlay> find(Long stationId, Integer lotType, Integer status, Integer modelStatus) {
		String key = "s_" + stationId + "_t_" + lotType + "_s_" + status + "_ms_" + modelStatus;
		List<BcLotteryPlay> l = getListFromCache(key);
		if (l != null)
			return l;
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1=1");
		if (!StringUtil.isEmpty(stationId)) {
			sb.append(" and station_id = :stationId");
		}
		if (!StringUtil.isEmpty(lotType)) {
			sb.append(" and lot_type = :lotType");
		}
		if (status != null) {
			sb.append(" and status =:status");
		}
		if (modelStatus != null) {
			sb.append(" and model_status =:modelStatus");
		}
		sb.append(" order by sort_no desc");
		return listToCache(super.query2Model(sb.toString(), MixUtil.newHashMap("stationId", stationId, "lotType", lotType, "status", status, "modelStatus", modelStatus)), key);
	}

	/**
	 * 从缓存中获取列表
	 * 
	 * @param key
	 * @return
	 */
	private List<BcLotteryPlay> getListFromCache(String key) {
		String idsStr = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, key, String.class);
		if (idsStr != null) {
			List<Long> ids = JSON.parseArray(idsStr, Long.class);
			if (ids != null && !ids.isEmpty()) {
				List<BcLotteryPlay> l = new ArrayList<>();
				for (Long id : ids) {
					l.add(getById(id));
				}
				return l;
			}
		}
		return null;
	}

	/**
	 * 将列表存到缓存中
	 * 
	 * @param l
	 * @param key
	 * @return
	 */
	private List<BcLotteryPlay> listToCache(List<BcLotteryPlay> l, String key) {
		if (l != null && !l.isEmpty()) {
			List<Long> ids = new ArrayList<>();
			for (BcLotteryPlay p : l) {
				ids.add(p.getId());
				setCache(p);
			}
			CacheUtil.addCache(CacheType.BC_LOTTERY_PLAY, key, ids);
		}
		return l;
	}

	public List<BcLotteryPlay> bonusPage(Integer lotType, Long stationId, Integer status, Integer modelStatus) {
		StringBuilder sb = new StringBuilder("select name,min_bonus_odds,sort_no,code from bc_lottery_play where 1 = 1");
		Map map = MixUtil.newHashMap();
		if (status != null) {
			sb.append(" and status = :status");
			map.put("status", status);
		}
		if (modelStatus != null) {
			sb.append(" and model_status = :modelStatus");
			map.put("modelStatus", modelStatus);
		}
		sb.append(" and lot_type = :lotType");
		sb.append(" and station_id = :stationId");
		map.put("lotType", lotType);
		map.put("stationId", stationId);
		return super.query2Model(sb.toString(), map);
	}

	/**
	 * 获取小类名称
	 */
	public String getPlayName(String playCode, Long stationId, Integer lotType) {
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, getCacheKey(playCode, stationId, lotType), BcLotteryPlay.class);
		if (p != null)
			return p.getName();
		StringBuilder sb = new StringBuilder("select name from bc_lottery_play where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and code = :playCode");
		map.put("playCode", playCode);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and lot_type = :lotType");
		map.put("lotType", lotType);
		return super.queryForString(sb.toString(), map);
	}

	/**
	 * 获取小类奖金
	 */
	public BigDecimal getPlayMinMoney(String playCode, Long stationId, Integer lotType) {
		BcLotteryPlay p = getByStationIdAndCodeAndType(stationId, playCode, lotType);
		if (p != null)
			return p.getMinBonusOdds();
		return BigDecimal.ZERO;
	}

	public List<BcLotteryPlay> playDataToJSONDanShi(Long stationId) {
		return query2Model("select * from bc_lottery_play where station_id=:stationId and code like :code and lot_type<:lotType",MixUtil.newHashMap("stationId",stationId,"code","%_ds","lotType",7));
	}
	
	public BcLotteryPlay getNameByCodeAndType(String code, Integer lotType,Long stationId) {
		BcLotteryPlay p = CacheUtil.getCache(CacheType.BC_LOTTERY_PLAY, getCacheKey(code, stationId, lotType), BcLotteryPlay.class);
		if(p != null){
			return p;
		}else{
			StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1=1");
			sb.append(" and station_id=:stationId");
			sb.append(" and code=:code");
			sb.append(" and lot_type=:type");
			p = super.query21Model(sb.toString(), MixUtil.newHashMap("stationId", stationId, "code", code, "type", lotType));
			setCache(p);
			return p;
		}
	}

	public Map<String, String> findPlayNameByCode(Long stationId, String playCode,Integer lotType) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select p.name as playName,g.name as groupName,g.code as groupCode from bc_lottery_play p LEFT JOIN bc_lottery_play_group g on g.id = p.group_id");
		sql_sb.append(" where p.station_id = :stationId");
		sql_sb.append(" and p.code = :playCode");
		sql_sb.append(" and p.lot_type = :lotType"); 
		sql_sb.append(" limit 1");
		return super.selectSingleCamelMap(sql_sb.toString(),MixUtil.newHashMap("stationId", stationId, "playCode", playCode, "lotType", lotType));
		
	}

	public List<BcLotteryPlay> findPlaysByCodes(Long stationId, List<String> list,Integer lotType) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where 1 = 1 and status = 2");
		Map map = MixUtil.newHashMap();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and lot_type = :lotType");
		map.put("lotType", lotType);
		
		if (list!=null && list.size()>0) {
			sb.append(" and code in (");
			for(int i =0;i<list.size();i++){
				if(i<list.size()-1){
					sb.append("'"+list.get(i)+"',");
				}else{
					sb.append("'"+list.get(i)+"'");
				}
				
			}
			sb.append(")");
		}
		return super.query2Model(sb.toString(), map);
	}

	public List<BcLotteryPlay> getPlaysByGroupId(Long stationId, Long groupId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_play where station_id =:stationId and group_id = :groupId");
		Map map = MixUtil.newHashMap();
		map.put("stationId", stationId);
		map.put("groupId", groupId);
		return super.query2Model(sb.toString(), map);
	}

	public Map getPlayNameAndGroupCode(String playCode, Long stationId, Integer lotType) {

		StringBuilder sb = new StringBuilder("select p.name as play_name,g.code as group_code from bc_lottery_play p left join bc_lottery_play_group g on p.group_id = g.id where");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" p.code = :playCode");
		map.put("playCode", playCode);
		sb.append(" and p.station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and p.lot_type = :lotType");
		map.put("lotType", lotType);
		return super.selectSingleCamelMap(sb.toString(), map);

	}
}
