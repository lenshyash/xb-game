package com.game.dao.lottery;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.lottery.BcLotteryJointPurchase;
import com.game.model.lottery.BcLotteryJointPurchasePate;
import com.game.util.DateUtil;

@Repository
public class BcLotteryJointPurchasePateDao extends JdbcUtilImpl<BcLotteryJointPurchasePate>{

	public Integer getSumBranchNum(String programId) {
		return super.queryForInt("select sum(branch_num) from bc_lottery_joint_purchase_pate where program_id=:programId", "programId", programId);
	}

	public List<BcLotteryJointPurchasePate> findByProgramIds(String programId,Long stationId) {
		return super.query2Model("select * from bc_lottery_joint_purchase_pate where program_id=:programId and station_id=:stationId", MixUtil.newHashMap("programId",programId,"stationId",stationId));
	}

	public Page<BcLotteryJointPurchasePate> page(String startTime, String endTime, Integer status, String account, Long stationId) {
		StringBuilder sb = new StringBuilder("select * from bc_lottery_joint_purchase_pate where 1=1");
		Map<String, Object> map = new HashMap<>();
		if(StringUtils.isNotEmpty(startTime)){
			sb.append(" and create_time>=:createTime ");
			map.put("createTime", DateUtil.toDatetime(startTime));
		}
		if(StringUtils.isNotEmpty(endTime)){
			sb.append(" and create_time<=:endTime ");
			map.put("endTime", DateUtil.toDatetime(endTime));
		}
		sb.append(" and station_id=:stationId");
		map.put("stationId", stationId);
		if(status != null){
			sb.append(" and status =:status ");
			map.put("status", status);
		}
		if(StringUtils.isNotEmpty(account)){
			sb.append(" and account=:account");
			map.put("account", account);
		}
		sb.append(" order by create_time desc");
		return super.paged2Obj(sb.toString(), map);
	}

	public int updateWinMoney(Long id, Long accountId, BigDecimal win) {
		if(id==null || accountId==null||win==null || win.compareTo(BigDecimal.ZERO)<=0){
			return 0;
		}
		String sql = "update bc_lottery_joint_purchase_pate set win_money=:win where id=:id and account_id=:accountId";
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("win", win);
		map.put("accountId", accountId);
		return update(sql, map);
	}
}
