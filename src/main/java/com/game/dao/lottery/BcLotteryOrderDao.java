package com.game.dao.lottery;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.redis.RedisAPI;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.model.SysAccount;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.lottery.LotteryEnum;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@SuppressWarnings("unchecked")
@Repository
public class BcLotteryOrderDao extends JdbcUtilImpl<BcLotteryOrder> {
	public static int BC_LOTTERY_ORDER_DB_INDEX = 15;// redis 缓存 db
	public static String CACHE_KEY_PREFIX = "ORDER_";// redis 缓存key前缀

	/**
	 * 获取需要反水的彩票投注记录
	 * 
	 * @param stationId
	 * @param startDate
	 * @param endDate
	 * @param agentRebate
	 * @param le如果为空，则查下非六合彩的数据，否则查六合彩的特码B，正码B的投注记录
	 * @return
	 */
	public List<BcLotteryOrder> get4Rebate(Long stationId, Date startDate, Date endDate, Integer agentRebate, boolean isMarkSix) {
		StringBuilder sb = new StringBuilder("select  * from bc_lottery_order where station_id=:stationId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(!isMarkSix && "win".equalsIgnoreCase(StationConfigUtil.get(stationId, StationConfig.member_lottery_rebate_choice))) {
			sb.append(" and agent_rebate = :agentRebate and status=:status1");
			map.put("status1", BusinessConstant.LOTTERY_ORDER_STATUS_PRIZE);
		}if(!isMarkSix && "unwin".equalsIgnoreCase(StationConfigUtil.get(stationId, StationConfig.member_lottery_rebate_choice))) {
			sb.append(" and agent_rebate = :agentRebate and status=:status1");
			map.put("status1", BusinessConstant.LOTTERY_ORDER_STATUS_UNPRIZE);
		}else {
			sb.append(" and agent_rebate = :agentRebate and (status=:status1 or status=:status2 or status=:status3)");
			map.put("status2", BusinessConstant.LOTTERY_ORDER_STATUS_UNPRIZE);
			map.put("status3", BusinessConstant.LOTTER_ORDER_STATUS_TIE_V4);
			map.put("status1", BusinessConstant.LOTTERY_ORDER_STATUS_PRIZE);
		}
		boolean marksixRebate = "on".equals(StationConfigUtil.get(stationId,StationConfig.onoff_multi_proxy_daily_rebate_marksix));
		if(!marksixRebate) {
			if (isMarkSix) {
				sb.append(" and (lot_code=:lotCode or lot_code=:sfLHC or lot_code=:wfLHC  or lot_code=:tmLHC or lot_code=:hkmhLHC or lot_code=:amLHC) and (play_code ='tm_b' or play_code='zm_b')");
			} else {
				sb.append(" and lot_code!=:lotCode and lot_code!=:sfLHC and lot_code!=:wfLHC and lot_code!=:tmLHC and lot_code!=:hkmhLHC and lot_code!=:amLHC");
			}
			map.put("sfLHC", LotteryEnum.SFLHC.name());
			map.put("wfLHC", LotteryEnum.WFLHC.name());
			map.put("tmLHC", LotteryEnum.TMLHC.name());
			map.put("hkmhLHC", LotteryEnum.HKMHLHC.name());
			map.put("lotCode", LotteryEnum.LHC.name());
			map.put("amLHC", LotteryEnum.AMLHC.name());
		}
		map.put("agentRebate", agentRebate);
		map.put("stationId", stationId);
		if (startDate != null) {
			sb.append(" and open_time >=:startDate");
			map.put("startDate", startDate);
			sb.append(" and create_time >=:createDate");// 添加此过滤，为了使用上分表索引
			Calendar c = Calendar.getInstance();
			c.setTime(startDate);
			c.add(Calendar.DAY_OF_MONTH, -7);
			map.put("createDate", c.getTime());
		}
		if (endDate != null) {
			sb.append(" and open_time <=:endDate");
			map.put("endDate", endDate);
		}
		String count = StationConfigUtil.getSys(StationConfig.onoff_multi_rebate_record_count);
		sb.append(" order by create_time");
		if(StringUtil.isNotEmpty(count)) {
			sb.append(" limit "+count);
		}else {
			sb.append(" limit 4000");
		}
		return super.query2Model(sb.toString(), map);
	}

	/**
	 * 多级代理返点取消时,状态更改
	 * 
	 * @param id
	 * @param rollBackStatus
	 * @return
	 */
	public Integer updateBcLotteryOrdertById4MultiRebate(Long id, Long stationId, Date endTime, Date betOrderDatetime, Integer rollBackStatus, Integer srcrollBackStatus) {
		return updateBcLotteryOrdertById4RollBack(id, stationId, endTime, betOrderDatetime, rollBackStatus, srcrollBackStatus);
	}

	/**
	 * 修改反水状态
	 * 
	 * @param id
	 * @param agentRebate
	 * @return
	 */
	public Integer updateBcLotteryOrdertById4RollBack(Long id, Long stationId, Date endTime, Date betOrderDatetime, Integer agentRebate, Integer srcAgentRebate) {
		StringBuilder sb = new StringBuilder("update bc_lottery_order set ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" agent_rebate = :agentRebate");
		map.put("agentRebate", agentRebate);
		sb.append(" where id =  :id");
		sb.append(" and station_id =  :stationId");
		sb.append(" and agent_rebate =  :srcAgentRebate");
		if (betOrderDatetime != null) {
			sb.append(" and create_time=:betOrderDatetime");
			map.put("betOrderDatetime", betOrderDatetime);
		} else {
			Calendar c = Calendar.getInstance();
			c.setTime(endTime);
			c.add(Calendar.DAY_OF_MONTH, 1);
			Date end = c.getTime();
			c.add(Calendar.DAY_OF_MONTH, -10);
			Date start = c.getTime();
			sb.append(" and create_time>=:start and create_time<:end");
			map.put("end", end);
			map.put("start", start);
		}
		map.put("srcAgentRebate", srcAgentRebate);
		map.put("stationId", stationId);
		map.put("id", id);
		return super.update(sb.toString(), map);
	}
	
	/**
	 * 修改反水状态
	 * 
	 * @param id
	 * @param rollBackStatus
	 * @return
	 */
	public Integer updateOrderRollStatus(Long stationId, Date startTime, Date endTime ,Integer rollBackStatus) {
		StringBuilder sb = new StringBuilder("update bc_lottery_order set ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" roll_back_status = :rollBackStatus");
		map.put("rollBackStatus", rollBackStatus);
		sb.append(" where station_id =  :stationId");
		sb.append(" and roll_back_status !=  :srcRollBackStatus");
		sb.append(" and create_time >=:startTime");
		map.put("startTime", startTime);
		sb.append(" and create_time <=:endTime");
		map.put("endTime", endTime);
		map.put("srcRollBackStatus", ProxyMultiRebateRecord.STATUS_ALREADY_REBATE);
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}

	/**
	 * 查询下注订单
	 * 
	 * @param qiHao
	 * @return
	 */
	public Page getBcLotteryOrder(String code, Date startTime, Date endTime, Integer status, String zhuiHao, Integer model, String orderId, String account, Long stationId, Boolean hasLHC) {
		StringBuilder sb = new StringBuilder();
		User u = SysUtil.getCurrentUser();
		if (u != null && u instanceof SysAccount && (Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
			sb.append("select * from bc_lottery_order_test where 1=1");
		} else {
			sb.append("select * from bc_lottery_order where 1=1");
		}
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (StringUtils.isNotEmpty(code)) {
			if(code.equals(LotteryEnum.LHC.name())) {
				sb.append(" and (lot_code = :code or lot_code=:amlhc)");
				map.put("code", code);
				map.put("amlhc",  LotteryEnum.AMLHC.name());
			}else {
				sb.append(" and lot_code = :code");
				map.put("code", code);
			}
		} else {
			if (hasLHC == null || hasLHC == false) {
				sb.append(" and lot_code != :excludeCode and lot_code!= :amlhc");
				map.put("excludeCode", LotteryEnum.LHC.name());
				map.put("amlhc", LotteryEnum.AMLHC.name());
			}
		}
		sb.append(" and lower(account) = :account ");
		map.put("account", StringUtils.isEmpty(account)?"":account.toLowerCase());
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		if (status != null && status > 0) {
			sb.append(" and status = :status");
			map.put("status", status);
		}
		if (!StringUtils.isEmpty(zhuiHao) && !zhuiHao.equals("1")) {
			sb.append(" and zhui_hao != '1'");
		}
		if (model != null && model > 0) {
			sb.append(" and model = :model");
			map.put("model", model);
		}
		if (!StringUtils.isEmpty(orderId)) {
			sb.append(" and order_id = :orderId");
			map.put("orderId", orderId);
		}
		if (startTime != null) {
			sb.append(" and create_time >=:startTime");
			map.put("startTime", startTime);
		}
		if (endTime != null) {
			sb.append(" and create_time <=:endTime");
			map.put("endTime", endTime);
		}
		sb.append(" ORDER BY create_time desc ,qi_hao desc ,order_id desc");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		String WinMoneycondition = "case when status in (1,2,3) then win_money else 0 end";
		String buyMoneycondition = "case when status in (1,2,3) then buy_money else 0 end";
		aggs.add(new Aggregation(AggregationFunction.SUM, WinMoneycondition, "winSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM, buyMoneycondition, "buySum"));

		return super.paged2Obj(sb.toString(), map, aggs);
	}

	/**
	 * 查询总下注和总中奖金额
	 * 
	 * @param qiHao
	 * @return
	 */
	public Map<String, String> getSumForQiHao(Date startTime, Date endTime, String code, Long stationId, String qihao, boolean cached) {
		String key = CACHE_KEY_PREFIX + "_xz_zj_" + code + "_" + stationId + "_" + qihao;
		if (cached) {
			String json = RedisAPI.getCache(key, BC_LOTTERY_ORDER_DB_INDEX);
			if (StringUtils.isNotEmpty(json)) {
				return JSON.parseObject(json, Map.class);
			}
		}
		StringBuilder sb = new StringBuilder("select SUM(coalesce(buy_money,0)) as buymoney");
		sb.append(",SUM(coalesce(win_money,0)) as winmoney from bc_lottery_order where  station_id = :stationId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("stationId", stationId);
		if (!StringUtils.isEmpty(code)) {
			sb.append(" and lot_code = :code");
			map.put("code", code);
		}
		sb.append(" and qi_hao = :qihao");
		map.put("qihao", qihao);
		sb.append(" and status !=4");
		sb.append(" and create_time >=:startTime");
		map.put("startTime", startTime);
		sb.append(" and create_time <=:endTime");
		map.put("endTime", endTime);
		Map<String, String> m = selectSingleMap(sb.toString(), map);
		if (cached && m != null && !m.isEmpty()) {
			RedisAPI.addCache(key, JSON.toJSONString(m), 0, BC_LOTTERY_ORDER_DB_INDEX);
		}
		return m;
	}

	/**
	 * 查询下注订单详情
	 * 
	 * @param qiHao
	 * @return
	 */

	public BcLotteryOrder getBcLotteryOrderDetail(String orderId, String account, Long stationId, String lotCode) {
		if (orderId == null)
			return null;
		// 从订单中解析出时间， 有利于分表查询
		Date orderTime = DateUtil.parseDate(orderId.substring(1, 7), "yyMMdd");
		Date start = DateUtil.dayFirstTime(orderTime, -2);
		Date end = DateUtil.dayFirstTime(orderTime, 2);
		StringBuilder sb = new StringBuilder();
		User u = SysUtil.getCurrentUser();
		if (u != null && u instanceof SysAccount &&( Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
			sb.append("select * from bc_lottery_order_test");
		} else {
			sb.append("select * from bc_lottery_order");
		}
		sb.append("  where create_time>=:start and create_time<:end");
		sb.append(" and order_id = :orderId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("start", start);
		map.put("end", end);
		map.put("orderId", orderId);
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(account) = :account");
			map.put("account", account.toLowerCase());
		}
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		if (StringUtils.isNotEmpty(lotCode)) {
			sb.append(" and lot_code = :lotCode");
			map.put("lotCode", lotCode);
		}
		return super.query21Model(sb.toString(), map);
	}

	public List<BcLotteryOrder> findByLotCodeAndQiHao(Long userId, String lotCode, String qiHao, Long stationId, Date endDate, Integer status) {
		StringBuilder sql = new StringBuilder();
		User u = SysUtil.getCurrentUser();
		if (u != null && u instanceof SysAccount && (Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
			sql.append("select * from bc_lottery_order_test where lot_code=:lotCode and qi_hao=:qiHao");
		} else {
			sql.append("select * from bc_lottery_order where lot_code=:lotCode and qi_hao=:qiHao");
		}
		if (stationId != null && stationId > 0L) {
			sql.append(" and station_id=:stationId");
		}
		if (userId != null && userId > 0L) {
			sql.append("  and account_id=:userId");
		}
		if (status != null && status > 0) {
			sql.append(" and status = :status");
		}
		sql.append(" and create_time>=:startDate and create_time<:endDate");
		sql.append(" ORDER by order_id desc");
		Map map = MixUtil.newHashMap("lotCode", lotCode, "qiHao", qiHao, "stationId", stationId, "userId", userId);
		map.put("status", status);
		Calendar c = Calendar.getInstance();
		c.setTime(endDate);
		c.add(Calendar.DAY_OF_MONTH, -7);
		Date startDate = c.getTime();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		return query2Model(sql.toString(), map);
	}

	/**
	 * TODO
	 * 
	 * @param startTime
	 * @param endTime
	 * @param account
	 * @param stationId
	 * @param lotCode
	 * @return
	 */
	public List<BcLotteryOrder> getBcLotteryOrderList(Date startTime, Date endTime, String account, Long stationId, String lotCode) {
		StringBuilder sb = new StringBuilder("select SUM(buy_zhu_shu) as effective_Bi_Zhu,SUM(buy_money) as effective_Tou_Zhu_Total,");
		sb.append("SUM(coalesce(win_money,0) - coalesce(buy_money,0)) as actual_Ying_Kui,to_char(create_time, 'YYYY-MM-DD') as date ");
		
		User u = SysUtil.getCurrentUser();
		if (u != null && u instanceof SysAccount && (Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
			sb.append("from bc_lottery_order_test ");
		} else {
			sb.append("from bc_lottery_order ");
		}
		sb.append(" where status !=4");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (StringUtils.isNotEmpty(lotCode)) {
			sb.append(" and lot_code = :lotCode");
			map.put("lotCode", lotCode);
		}
		sb.append(" and lower(account) = :account");
		map.put("account", StringUtils.isEmpty(account)?"":account.toLowerCase());
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		if (startTime != null) {
			sb.append(" and create_time >=:startTime");
			map.put("startTime", startTime);
		}
		if (endTime != null) {
			sb.append(" and create_time <=:endTime");
			map.put("endTime", endTime);
		}
		sb.append(" group by date order by date desc");
		return super.selectCamelListMap(sb.toString(), map);
	}

	/**
	 * 查询下注订单统计
	 * 
	 * @param qiHao
	 * @return
	 */
	public Page<BcLotteryOrder> getBcLotteryOrderTotal(Date startTime, Date endTime, Integer detailType, String account, Long stationId) {
		StringBuilder sb = new StringBuilder();
		User u = SysUtil.getCurrentUser();
		if (u != null && u instanceof SysAccount && (Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
			sb.append("select * from bc_lottery_order_test where account = :account");
		} else {
			sb.append("select * from bc_lottery_order where account = :account");
		}
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("account", account);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		if (detailType == 1) {
			sb.append(" and status = 2");
		} else if (detailType == 2) {
			sb.append(" and status = 4");
		} else if (detailType == 3) {
			sb.append(" and status != 4");
		} else if (detailType == 4) {
			sb.append(" and status = 3");
		}
		if (startTime != null) {
			sb.append(" and create_time >=:startTime");
			map.put("startTime", startTime);
		}
		if (endTime != null) {
			sb.append(" and create_time <=:endTime");
			map.put("endTime", endTime);
		}
		sb.append(" ORDER BY create_time desc");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "win_money", "winSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "buy_money", "buySum"));
		return super.paged2Obj(sb.toString(), map, aggs);
	}

	public List<BcLotteryOrder> findUnAwardByIds(List<Long> ids, Date startTime, Date endTime) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order where create_time BETWEEN :start and :end");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("start", startTime);
		paramMap.put("end", endTime);
		sql.append(" and (");
		for (Long id : ids) {
			sql.append(" id=:id").append(id).append(" or");
			paramMap.put("id" + id, id);
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.deleteCharAt(sql.length() - 1);
		sql.append(")");
		return query2Model(sql.toString(), paramMap);
	}

	public List<Long> getUnAwardIds(String lotCode, String qiHao, Integer[] statuses, Long stationId, Date startTime, Date endTime) {
		StringBuilder sql = new StringBuilder("select id from bc_lottery_order where lot_code=:lotCode and qi_hao=:qiHao");
		sql.append(" and create_time BETWEEN :start and :end");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("lotCode", lotCode);
		paramMap.put("stationId", stationId);
		paramMap.put("qiHao", qiHao);
		paramMap.put("start", startTime);
		paramMap.put("end", endTime);
		if (stationId != null && stationId > 0L) {
			sql.append(" and station_id=:stationId");
		}
		if (statuses != null) {
			sql.append(" and (");
			for (Integer s : statuses) {
				sql.append(" status=:status").append(s).append(" or");
				paramMap.put("status" + s, s);
			}
			sql.deleteCharAt(sql.length() - 1);
			sql.deleteCharAt(sql.length() - 1);
			sql.append(")");
		}
		return selectSingleColList(sql.toString(), paramMap, Long.class);
	}
	
	public int updateAwarding(Long id,String haoMa,Long markSixId, BigDecimal money) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("haoMa", haoMa);
		paramMap.put("bcMarkSixId", markSixId);
		paramMap.put("status", BusinessConstant.LOTTERY_ORDER_STATUS_WAIT);
		paramMap.put("money", money);
		return update("update bc_lottery_order set hao_ma=:haoMa,mark_six_id=:bcMarkSixId,buy_money=:money where id=:id and status =:status", paramMap);
	}

	public int updateAwardInfo(Long id, Integer oldStatus, Integer newStatus, Date orderCreateTime, Date openTime, Integer zhuShu, BigDecimal winMoney) {
		return updateAwardInfo(id,oldStatus,newStatus,orderCreateTime,openTime,zhuShu,winMoney,null);
	}
	
	public int updateAwardInfo(Long id, Integer oldStatus, Integer newStatus, Date orderCreateTime, Date openTime, Integer zhuShu, BigDecimal winMoney,BigDecimal odds) {
		Map<String, Object> paramMap = new HashMap<>();
		String sql =  "update bc_lottery_order set status=:newStatus,open_time=:openTime,win_zhu_shu=:zhuShu,win_money=:winMoney ";
		paramMap.put("id", id);
		paramMap.put("oldStatus", oldStatus);
		paramMap.put("winMoney", winMoney);
		paramMap.put("zhuShu", zhuShu);
		paramMap.put("openTime", openTime);
		paramMap.put("newStatus", newStatus);
		paramMap.put("createTime", orderCreateTime);
		if(StringUtil.isNotEmpty(odds)) {
			paramMap.put("odds", odds);
			sql += ",odds=:odds";
		}
		
		sql += " where id=:id and status=:oldStatus and create_time=:createTime";
		
		return update(sql, paramMap);
	}

	public int updateStatus(Long id, Integer oldStatus, Date createTime, Integer newStatus, boolean isTestOrder) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("oldStatus", oldStatus);
		paramMap.put("newStatus", newStatus);
		paramMap.put("createTime", createTime);
		User u = SysUtil.getCurrentUser();
		if (isTestOrder || (u != null && u instanceof SysAccount && (Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
				|| Objects.equals(((SysAccount) u).getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)))) {
			return update("update bc_lottery_order_test set status=:newStatus where id=:id and status=:oldStatus and create_time=:createTime", paramMap);
		} else {
			return update("update bc_lottery_order set status=:newStatus where id=:id and status=:oldStatus and create_time=:createTime", paramMap);
		}
	}

	/**
	 * 获得追号列表
	 * 
	 * @param order
	 * @return
	 */
	public List<BcLotteryOrder> findZhuiHao(BcLotteryOrder order) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order where account=:account");
		sql.append(" and station_id=:stationId and zhui_hao=:orderId and qi_hao>:qiHao");
		sql.append(" and create_time >=:startDate");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("qiHao", order.getQiHao());
		paramMap.put("orderId", order.getZhuiHao());
		paramMap.put("account", order.getAccount());
		paramMap.put("stationId", order.getStationId());
		Calendar c = Calendar.getInstance();
		c.setTime(order.getCreateTime());
		c.add(Calendar.MINUTE, -10);// 为了使用到分表索引。减10分钟是防止追号的create_time小于order
									// 的create_time
		paramMap.put("startDate", c.getTime());
		return query2Model(sql.toString(), paramMap);
	}
	public Page<BcLotteryOrder> getBcLotteryOrder2(String orderCode, String qihao, String lotCode, Date start, Date end, Integer status, Integer model, String parents, Long stationId, String account,Long roomId,String playCode,String groupName,String betIp) {
		return getBcLotteryOrder2(orderCode, qihao, lotCode, start, end, status,
				model, parents, stationId, account, roomId, playCode,null,null,null,groupName,null,betIp);
	}
	public Page<BcLotteryOrder> getBcLotteryOrder2(String orderCode, String qihao, String lotCode, Date start, Date end, Integer status, Integer model, String parents, Long stationId, String account,Long roomId,String playCode,Integer level,BigDecimal min,BigDecimal max,String groupName,Long reportType,String betIp) {
		StringBuilder sb = new StringBuilder("select o.id,o.order_id,o.account_id,o.account,o.parents,o.station_id,o.lot_code,o.qi_hao,o.play_code,o.hao_ma,o.create_time,o.open_time,o.buy_zhu_shu,o.win_zhu_shu,o.multiple,o.buy_money,o.win_money,o.status,o.zhui_hao,o.lot_type,o.model,o.ago,o.odds,o.joint_purchase,o.agent_rebate,o.qz_group_name,o.bet_ip,a.agent_name,a.abnormal_flag");
		if(StationUtil.isAgentStation()) {
			sb.append(",a.remark");
		}
		sb.append(" from bc_lottery_order o left join sys_account a on a.id = o.account_id where ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		List<LotteryEnum> sysLots = LotteryEnum.getSysLottery();
		List<LotteryEnum> normalLots = LotteryEnum.getNormalLottery();
		sb.append(" o.station_id = :stationId");
		map.put("stationId", stationId);
		if (!StringUtils.isEmpty(lotCode)) {
			if(BcLotteryOrder.SYS_ALL.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < sysLots.size(); i++) {
					lotteryEnum = sysLots.get(i);
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else if(BcLotteryOrder.SYS_NO_SF.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < sysLots.size(); i++) {
					lotteryEnum = sysLots.get(i);
					if(lotteryEnum.name().equals(LotteryEnum.SFLHC.name()) || lotteryEnum.name().equals(LotteryEnum.WFLHC.name()) || lotteryEnum.name().equals(LotteryEnum.TMLHC.name()) || lotteryEnum.name().equals(LotteryEnum.HKMHLHC.name())) {
						continue;
					}
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else if(BcLotteryOrder.COMMON.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < normalLots.size(); i++) {
					lotteryEnum = normalLots.get(i);
					if(lotteryEnum.name().equals(LotteryEnum.LHC.name()) || lotteryEnum.name().equals(LotteryEnum.AMLHC.name())) {
						continue;
					}
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else if(BcLotteryOrder.OFFICIALLHC.equals(lotCode)){
				sb.append(" and lot_code IN (");
				sb.append(" :").append(LotteryEnum.LHC.name().toLowerCase());
				sb.append(",");
				sb.append(" :").append(LotteryEnum.AMLHC.name().toLowerCase());
				sb.append(")");
				map.put(LotteryEnum.LHC.name().toLowerCase(), LotteryEnum.LHC.name());
				map.put(LotteryEnum.AMLHC.name().toLowerCase(), LotteryEnum.AMLHC.name());
			}else {
				sb.append(" and lot_code = :lotCode");
				map.put("lotCode", lotCode);
			}
		} else {
			sb.append(" and lot_code<>:lotCode");
			map.put("lotCode", LotteryEnum.LHC.name());
		}

		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(o.account) = :account");
			map.put("account", account.toLowerCase());
		}
		if (StringUtils.isNotEmpty(parents) && !"agent,".equals(parents)) {
			sb.append(" and o.parents LIKE :parents");
			map.put("parents", parents + "%");
		}
		if (status != null && status > 0) {
			sb.append(" and o.status = :status");
			map.put("status", status);
		}
		if (model != null && model > 0) {
			sb.append(" and o.model = :model");
			map.put("model", model);
		}

		if (StringUtils.isNotEmpty(qihao)) {
			sb.append(" and o.qi_hao=:qihao");
			map.put("qihao", qihao);
		}

		if (StringUtils.isNotEmpty(orderCode)) {
			sb.append(" and o.order_id =:orderCode");
			map.put("orderCode", orderCode);
		}

		if (start != null) {
			sb.append(" and o.create_time >=:startDate");
			map.put("startDate", start);
		}
		if (end != null) {
			sb.append(" and o.create_time <:endDate");
			map.put("endDate", end);
		}
		if(roomId != null && roomId > 0) {
			sb.append(" and o.room_id = :roomId");
			map.put("roomId", roomId);
		}
		if(level != null ) {
			sb.append(" and a.level_group = :levelId");
			map.put("levelId", level);
		}
		if(min != null ) {
			sb.append(" and o.buy_money >= :min");
			map.put("min", min.doubleValue());
		}
		if(max != null ) {
			sb.append(" and o.buy_money <= :max");
			map.put("max", max.doubleValue());
		}
		if(reportType != null ) {
			sb.append(" and a.report_type = :reportType");
			map.put("reportType",reportType);
		}
		if(StringUtils.isNotEmpty(playCode)) {
			sb.append(" and o.play_code in (");
			String[] pcode = playCode.split("\\|");
			for(int i=0;i<pcode.length;i++) {
				if(i>0) {
					sb.append(",");
				}
				sb.append(":play_code_" + i);
				map.put("play_code_"+i, pcode[i]);
			}
			sb.append(" )");
		}
		if(StringUtil.isNotEmpty(groupName)) {
			sb.append(" and o.qz_group_name =:groupName");
			map.put("groupName", groupName);
		}
		if(StringUtil.isNotEmpty(betIp)) {
			sb.append(" and o.bet_ip =:betIp");
			map.put("betIp", betIp);
		}
		Map<String,?> aggsData=getAggsDataForOrder2(sb.toString(),map);
		
		sb.append(" ORDER BY o.id desc");
//		List<Aggregation> aggs = new ArrayList<Aggregation>();
//		aggs.add(new Aggregation(AggregationFunction.SUM, "win_money", "winSum"));
//		aggs.add(new Aggregation(AggregationFunction.SUM, "buy_money", "buySum"));
		 Page<BcLotteryOrder> page= super.paged2Obj(sb.toString(), map);
		 page.setAggsData(aggsData);
		 return page;
	}
	/**
	 * 获取引导账号的订单 
	 * @param orderCode
	 * @param qihao
	 * @param lotCode
	 * @param start
	 * @param end
	 * @param status
	 * @param model
	 * @param parents
	 * @param stationId
	 * @param account
	 * @param roomId
	 * @param playCode
	 * @param level
	 * @param min
	 * @param max
	 * @return
	 */
	public Page<BcLotteryOrder> getBcLotteryGuideOrder(String orderCode, String qihao, String lotCode, Date start, Date end, Integer status, Integer model, String parents, Long stationId, String account,Long roomId,String playCode,Integer level,BigDecimal min,BigDecimal max) {
		StringBuilder sb = new StringBuilder("select o.*,a.agent_name from bc_lottery_order_test o left join sys_account a on a.id = o.account_id where 1=1 and a.account_type=9 ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		List<LotteryEnum> sysLots = LotteryEnum.getSysLottery();
		List<LotteryEnum> normalLots = LotteryEnum.getNormalLottery();
		if (!StringUtils.isEmpty(lotCode)) {
			
			if(BcLotteryOrder.SYS_ALL.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < sysLots.size(); i++) {
					lotteryEnum = sysLots.get(i);
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else if(BcLotteryOrder.SYS_NO_SF.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < sysLots.size(); i++) {
					lotteryEnum = sysLots.get(i);
					if(lotteryEnum.name().equals(LotteryEnum.SFLHC.name()) || lotteryEnum.name().equals(LotteryEnum.WFLHC.name()) || lotteryEnum.name().equals(LotteryEnum.TMLHC.name()) || lotteryEnum.name().equals(LotteryEnum.HKMHLHC.name())) {
						continue;
					}
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else if(BcLotteryOrder.COMMON.equals(lotCode)) {
				sb.append(" and lot_code IN (");
				LotteryEnum lotteryEnum = null;
				for (int i = 0; i < normalLots.size(); i++) {
					lotteryEnum = normalLots.get(i);
					if(lotteryEnum.name().equals(LotteryEnum.LHC.name()) ||lotteryEnum.name().equals(LotteryEnum.AMLHC.name())) {
						continue;
					}
					if(i > 0) {
						sb.append(",");
					}
					sb.append(" :").append(lotteryEnum.name().toLowerCase());
					map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
				}
				sb.append(")");
			}else {
				sb.append(" and lot_code = :lotCode");
				map.put("lotCode", lotCode);
			}
		} else {
			sb.append(" and lot_code<>:lotCode");
			map.put("lotCode", LotteryEnum.LHC.name());
		}

		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(o.account) = :account");
			map.put("account", account.toLowerCase());
		}
		if (StringUtils.isNotEmpty(parents) && !"agent,".equals(parents)) {
			sb.append(" and o.parents LIKE :parents");
			map.put("parents", parents + "%");
		}
		sb.append(" and o.station_id = :stationId");
		map.put("stationId", stationId);
		if (status != null && status > 0) {
			sb.append(" and o.status = :status");
			map.put("status", status);
		}
		if (model != null && model > 0) {
			sb.append(" and o.model = :model");
			map.put("model", model);
		}

		if (StringUtils.isNotEmpty(qihao)) {
			sb.append(" and o.qi_hao=:qihao");
			map.put("qihao", qihao);
		}

		if (StringUtils.isNotEmpty(orderCode)) {
			sb.append(" and o.order_id =:orderCode");
			map.put("orderCode", orderCode);
		}

		if (start != null) {
			sb.append(" and o.create_time >=:startDate");
			map.put("startDate", start);
		}
		if (end != null) {
			sb.append(" and o.create_time <:endDate");
			map.put("endDate", end);
		}
		if(roomId != null && roomId > 0) {
			sb.append(" and o.room_id = :roomId");
			map.put("roomId", roomId);
		}
		if(level != null ) {
			sb.append(" and a.level_group = :levelId");
			map.put("levelId", level);
		}
		if(min != null ) {
			sb.append(" and o.buy_money >= :min");
			map.put("min", min.doubleValue());
		}
		if(max != null ) {
			sb.append(" and o.buy_money <= :max");
			map.put("max", max.doubleValue());
		}
		
		if(StringUtils.isNotEmpty(playCode)) {
			sb.append(" and o.play_code in (");
			String[] pcode = playCode.split("\\|");
			for(int i=0;i<pcode.length;i++) {
				if(i>0) {
					sb.append(",");
				}
				sb.append(":play_code_" + i);
				map.put("play_code_"+i, pcode[i]);
			}
			sb.append(" )");
		}
		Map<String,?> aggsData=getAggsDataForOrder2(sb.toString(),map);
		
		sb.append(" ORDER BY o.id desc");
//		List<Aggregation> aggs = new ArrayList<Aggregation>();
//		aggs.add(new Aggregation(AggregationFunction.SUM, "win_money", "winSum"));
//		aggs.add(new Aggregation(AggregationFunction.SUM, "buy_money", "buySum"));
		 Page<BcLotteryOrder> page= super.paged2Obj(sb.toString(), map);
		 page.setAggsData(aggsData);
		 return page;
	}
	
	private Map<String, ?> getAggsDataForOrder2(String sql,Map<Object, Object> map) {
		Map<String,?> r=new HashMap<>();
		sql=sql.replace(" and o.status = :status", "");
		StringBuilder builder = new StringBuilder("select o.id,o.order_id,o.account_id,o.account,o.parents,o.station_id,o.lot_code,o.qi_hao,o.play_code,o.hao_ma,o.create_time,o.open_time,o.buy_zhu_shu,o.win_zhu_shu,o.multiple,o.buy_money,o.win_money,o.status,o.zhui_hao,o.lot_type,o.model,o.ago,o.odds,o.joint_purchase,o.agent_rebate,o.qz_group_name,o.bet_ip,a.agent_name,a.abnormal_flag");
		if(StationUtil.isAgentStation()) {
			builder.append(",a.remark");
		}
		sql=sql.replace(builder.toString(), "select sum(win_money) as win_sum,sum(buy_money) as buy_sum,count(distinct(o.account)) as buy_account_count ");
		sql = sql+" and o.status in (1,2,3,5,6,7,10)";
		List<Map> list=selectCamelListMap(sql,map);
		if(list!=null && !list.isEmpty()){
			for (Map m : list) {
				r.putAll(m);
			}
		}
		return r;
	}

	/**
	 * 根据站点和投注时间来删除
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from bc_lottery_order where 1=1");
		Map map = MixUtil.newHashMap();
		sb.append(" and create_time <= :createTime");
		map.put("createTime", createTime);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}

	/**
	 * 计算返点总数
	 * 
	 * @return
	 */
	public BigDecimal sumForFanDian(Long stationId, String lotCode, String qiHao, Date startDate, Date endDate) {
		String key = CACHE_KEY_PREFIX + "_fd_" + lotCode + "_" + stationId + "_" + qiHao;
		String str = RedisAPI.getCache(key, BC_LOTTERY_ORDER_DB_INDEX);
		if (StringUtils.isNotEmpty(str)) {
			return BigDecimalUtil.toBigDecimal(str);
		}
		StringBuilder sb = new StringBuilder("select sum(a.rebate_money) from proxy_multi_rebate_record  a");
		sb.append(" join (select id from bc_lottery_order where lot_code = :lotCode");
		sb.append(" and create_time>=:startDate and create_time<:endDate");
		sb.append(" and station_id = :stationId and qi_hao = :qiHao) b");
		sb.append(" on a.bet_id=b.id  and a.create_datetime>=:endDate and a.create_datetime<:pendDate");
		Date pendDate = DateUtil.dayFirstTime(endDate, 7);// 反水，返点 的时间，大于期号的结束时间
		Map map = MixUtil.newHashMap("stationId", stationId, "lotCode", lotCode, "qiHao", qiHao);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("pendDate", pendDate);
		BigDecimal d = super.queryForBigDecimal(sb.toString(), map);
		if (d != null) {
			RedisAPI.addCache(key, d.toString(), 0, BC_LOTTERY_ORDER_DB_INDEX);
		}
		return d;
	}


	public List<BcLotteryOrder> getOrderByDate(Long stationId, Date startDate, Date endDate) {
		StringBuilder sb = new StringBuilder("select  * from bc_lottery_order where station_id=:stationId");
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("stationId", stationId);
		if (startDate != null) {
			sb.append(" and create_time >=:startDate");
			map.put("startDate", startDate);
		}
		if (endDate != null) {
			sb.append(" and create_time <=:endDate");
			map.put("endDate", endDate);
		}
		return super.query2Model(sb.toString(), map);
	}

	public List<BcLotteryOrder> getTmByCode(Long stationId, String orderCode, String qihao, String code, Date startTime, Date endTime, Integer status, String account,Long reportType) {
		StringBuilder sb = new StringBuilder("select o.hao_ma,o.play_code,sum(o.buy_money) buy_money,sum(o.win_money) win_money from bc_lottery_order o left join sys_account a on a.id = o.account_id where play_code in ('tm_a','tm_b')");
		sb.append(" and o.station_id=:stationId");
		Map map = MixUtil.newHashMap("stationId", stationId);
		if (startTime != null) {
			sb.append(" and o.create_time >=:startTime");
			map.put("startTime", startTime);
		}

		if (endTime != null) {
			sb.append(" and o.create_time <=:endTime");
			map.put("endTime", endTime);
		}

		if (StringUtils.isNotEmpty(qihao)) {
			sb.append(" and o.qi_hao =:qihao");
			map.put("qihao", qihao);
		}

		if (StringUtils.isNotEmpty(orderCode)) {
			sb.append(" and o.order_id = :orderCode");
			map.put("orderCode", orderCode);
		}

		if (StringUtils.isNotEmpty(code)) {
			sb.append(" and o.lot_code = :code");
			map.put("code", code);
		}

		if (!StringUtils.isEmpty(account)) {
			sb.append(" and lower(o.account) = :account");
			map.put("account", account.toLowerCase());
		}

		if (status != null && status != 4) {
			sb.append(" and o.status = :status");
			map.put("status", status);
		}
		
		if(Validator.isNotNull(reportType)) {
			sb.append(" and a.report_type = :reportType");
			map.put("reportType", reportType);
		}

		sb.append(" and o.status!=4");

		sb.append(" GROUP BY o.hao_ma,o.play_code");
		return super.query2Model(sb.toString(), map);
	}

	public void batchInsertTest(final List<BcLotteryOrder> list) {
		StringBuilder sql = new StringBuilder("insert into bc_lottery_order_test(order_id, account_id, account, parents,");
		sql.append("station_id, lot_code, qi_hao, play_code, hao_ma, create_time,  buy_zhu_shu,multiple, buy_money, ");
		sql.append("status, zhui_hao, roll_back_status, lot_type, model, mark_six_id, ago, odds)");
		sql.append(" values(?,?,?,?, ?,?,?,?, ?,?,?,?, ?, ?,?,?,?, ?,?,?,?)");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				BcLotteryOrder o = list.get(i);
				int k = 1;
				ps.setString(k++, o.getOrderId());
				ps.setLong(k++, o.getAccountId());
				ps.setString(k++, o.getAccount());
				ps.setString(k++, o.getParents());
				ps.setLong(k++, o.getStationId());
				ps.setString(k++, o.getLotCode());
				ps.setString(k++, o.getQiHao());
				ps.setString(k++, o.getPlayCode());
				ps.setString(k++, o.getHaoMa());
				ps.setTimestamp(k++, new java.sql.Timestamp(o.getCreateTime().getTime()));
				ps.setInt(k++, o.getBuyZhuShu());
				ps.setInt(k++, o.getMultiple());
				ps.setBigDecimal(k++, o.getBuyMoney());
				ps.setInt(k++, o.getStatus());
				ps.setString(k++, o.getZhuiHao());
				ps.setInt(k++, o.getRollBackStatus());
				ps.setInt(k++, o.getLotType());
				ps.setInt(k++, o.getModel());
				if (o.getMarkSixId() == null) {
					ps.setLong(k++, 0L);
				} else {
					ps.setLong(k++, o.getMarkSixId());
				}
				ps.setInt(k++, o.getAgo());
				ps.setBigDecimal(k++, o.getOdds());
			}

			@Override
			public int getBatchSize() {
				return list.size();
			}
		});
	}

	public List<BcLotteryOrder> findTestUnAward(String lotCode, String qiHao, Date startTime, Date endTime, Long stationId) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order_test where lot_code=:lotCode and qi_hao=:qiHao");
		sql.append(" and create_time BETWEEN :start and :end and status=:status ");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("lotCode", lotCode);
		paramMap.put("qiHao", qiHao);
		paramMap.put("start", startTime);
		paramMap.put("end", endTime);
		paramMap.put("status", BusinessConstant.LOTTERY_ORDER_STATUS_WAIT);
		if (stationId != null && stationId > 0L) {
			sql.append(" and station_id=:stationId");
		}
		paramMap.put("stationId", stationId);
		return query2Model(sql.toString(), paramMap);
	}

	public int updateTestAwardInfo(Long id, Integer oldStatus, Integer newStatus, Date openTime, Integer zhuShu, BigDecimal winMoney,BigDecimal odds) {
		String sql = "update bc_lottery_order_test set status=:newStatus,open_time=:openTime,win_zhu_shu=:zhuShu,win_money=:winMoney";
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("oldStatus", oldStatus);
		paramMap.put("winMoney", winMoney);
		paramMap.put("zhuShu", zhuShu);
		paramMap.put("openTime", openTime);
		paramMap.put("newStatus", newStatus);
		if(StringUtil.isNotEmpty(odds)) {
			paramMap.put("odds", odds);
			sql += ",odds=:odds";
		}
		sql +=" where id=:id and status=:oldStatus";
		return update(sql, paramMap);
	}

	public List<BcLotteryOrder> findTestZhuiHao(BcLotteryOrder order) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order_test where account=:account");
		sql.append(" and station_id=:stationId and zhui_hao=:orderId and qi_hao>:qiHao");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("qiHao", order.getQiHao());
		paramMap.put("orderId", order.getZhuiHao());
		paramMap.put("account", order.getAccount());
		paramMap.put("stationId", order.getStationId());
		return query2Model(sql.toString(), paramMap);
	}

	public List<BcLotteryOrder> getOrdersDetail(String programIds, Long stationId, String lotCode) {
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order where station_id=:stationId ");
		sql.append(" and lot_code=:lotCode and (");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("lotCode", lotCode);
		String[] ids = programIds.split(",");
		for (String proId : ids) {
			sql.append(" joint_purchase=:jointPurchase").append(proId).append(" or");
			paramMap.put("jointPurchase" + proId, proId);
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.deleteCharAt(sql.length() - 1);
		sql.append(" )");
		return query2Model(sql.toString(), paramMap);
	}

	public List<Map> findByWinOrder(Long stationId, int status, int pageSize, Date start, Date end) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("status", status);
		paramMap.put("pageSize", pageSize);
		paramMap.put("start", start);
		paramMap.put("end", end);
		StringBuilder sql = new StringBuilder("select account,max(win_money) AS money,lot_code from bc_lottery_order ");
		sql.append(" where station_id=:stationId  and status=:status ");
		sql.append(" and create_time >=:start and create_time<:end ");
		sql.append(" GROUP BY account,lot_code LIMIT :pageSize");
		return super.selectCamelListMap(sql.toString(), paramMap);
	}

	public void clearTestData(Date endTime) {
		update("delete from bc_lottery_order_test where create_time<:end", MixUtil.newHashMap("end", endTime));
	}

	public Map getForMemberData(Long accountId, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sb = new StringBuilder("select ");
		sb.append("sum(CASE WHEN lot_code != 'LHC' THEN buy_money ELSE 0 END) AS lottery_total,");
		sb.append("sum(CASE WHEN lot_code != 'LHC' THEN win_money ELSE 0 END) AS lottery_award,");
		sb.append("sum(CASE WHEN lot_code = 'LHC' THEN buy_money ELSE 0 END) AS mark_six_total,");
		sb.append("sum(CASE WHEN lot_code = 'LHC' THEN win_money ELSE 0 END) AS mark_six_award");
		sb.append(" from bc_lottery_order where status in (1,2,3,5,6,7,10)");
		sb.append(" and station_id=:stationId and account_id=:accountId");
		sb.append(" and create_time>=:beginDate and create_time<=:endDate");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("accountId", accountId);
		return selectSingleCamelMap(sb.toString(), map);
	}
	
	public void syncParents(Long accountId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE bc_lottery_order m SET parents = a.parents");
		sql_sb.append(" FROM sys_account a ");
		sql_sb.append(" WHERE a.id = m.account_id AND a.id = :accountId;");
		super.update(sql_sb.toString(), MixUtil.newHashMap("accountId",accountId));
	}

	public int snyLotteryOrder(Long stationId, Date end, Date begin) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE bc_lottery_order m SET parents = a.parents");
		sql_sb.append(" FROM sys_account a ");
		sql_sb.append(" WHERE a.id = m.account_id and a.station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtil.isNotEmpty(begin)) {
			sql_sb.append(" AND m.create_time >= :startTime");
			map.put("startTime", begin);
		}
		if (StringUtil.isNotEmpty(end)) {
			sql_sb.append(" AND m.create_time <= :end");
			map.put("end", end);
		}
		return super.update(sql_sb.toString(), map);
		
	}
	
	public void deleteOrderByStationId(Long stationId) {
		String sql = "delete from bc_lottery_order where station_id = :stationId";
		super.update(sql, MixUtil.newHashMap("stationId",stationId));
	}
	
	public void deleteTestOrderByStationId(Long stationId) {
		String sql = "delete from bc_lottery_order_test where station_id = :stationId";
		super.update(sql, MixUtil.newHashMap("stationId",stationId));
	}

	public BigDecimal getMemberFreeAmount(Long id) {
		Calendar c = Calendar.getInstance();
		Date endTime = c.getTime();
		c.add(Calendar.DAY_OF_MONTH, -1);
		StringBuilder sql = new StringBuilder("select sum(buy_money) from bc_lottery_order where account_id = :id");
		sql.append(" and create_time BETWEEN :start and :end and status=:status ");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("start",c.getTime() );
		paramMap.put("end", endTime);
		paramMap.put("status", BcLotteryOrder.tz_status_end);
		return super.queryForBigDecimal(sql.toString(), paramMap);
	}

    public List<BcLotteryOrder> getShareList(Long stationId, Long userId) {
		Calendar c = Calendar.getInstance();
		Date endTime = c.getTime();
		c.add(Calendar.DAY_OF_MONTH, -1);
		StringBuilder sql = new StringBuilder("select * from bc_lottery_order where account_id = :id and station_id = :stationId and status=1");
		sql.append(" and create_time BETWEEN :start and :end");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", userId);
		paramMap.put("stationId", stationId);
		paramMap.put("start",c.getTime() );
		paramMap.put("end", endTime);
		return super.query2Model(sql.toString(), paramMap);
    }
}
