package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberLevelBase;

@Repository
public class MemberLevelBaseDao extends JdbcUtilImpl<MemberLevelBase> {

	public Page<MemberLevelBase> getPage(Long stationId, Integer status) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_level_base ");
		sql_sb.append(" WHERE station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);

		if (status != null && status > 0) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		sql_sb.append(" ORDER BY deposit_money");

		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<MemberLevelBase> getStationLevels(Long stationId) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_level_base ");
		sql_sb.append(" WHERE station_id= :stationId AND status = ").append(MemberLevelBase.STATUS_ENALBED);
		sql_sb.append(" ORDER BY deposit_money");
		return super.query2Model(sql_sb.toString(), MixUtil.newHashMap("stationId", stationId));
	}

	public boolean addLevelCount(long levelId) {
		return addLevelCount(levelId, 1);
	}

	public boolean addLevelCount(long levelId, Integer num) {
		return super.update("UPDATE member_level_base SET member_count = member_count + :num WHERE id = :levelId",
				MixUtil.newHashMap("levelId", levelId, "num", num)) > 0;
	}

	public boolean subLevelCount(long levelId) {
		return subLevelCount(levelId, 1);
	}

	public boolean subLevelCount(long levelId, Integer num) {
		return super.update("UPDATE member_level_base SET member_count = member_count - :num WHERE id = :levelId",
				MixUtil.newHashMap("levelId", levelId, "num", num)) > 0;
	}
}
