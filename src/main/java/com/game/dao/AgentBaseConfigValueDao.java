package com.game.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigValue;
import com.game.util.StationUtil;

@Repository
public class AgentBaseConfigValueDao extends JdbcUtilImpl<AgentBaseConfigValue> {

	public List<AgentBaseConfigValue> getConfVals(Long stationId, Long status) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM agent_base_config_value WHERE station_id = :stationId");

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (Validator.isNotNull(status)) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		sql_sb.append(" ORDER BY id");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public AgentBaseConfigValue getConfVal(String key, Long stationId) {
		String ckey = "skey_" + stationId + "_" + key;
		AgentBaseConfigValue v = CacheUtil.getCache(CacheType.AGENT_SETTING, ckey, AgentBaseConfigValue.class);
		if (v != null)
			return v;
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(
				"SELECT v.*  FROM agent_base_config c INNER JOIN agent_base_config_value v ON c.id = v.config_id");
		sql_sb.append(" WHERE v.station_id = :stationId AND c.status != :status");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("status", AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE);
		if (StringUtil.isNotEmpty(key)) {
			paramMap.put("key", key);
			sql_sb.append(" AND c.key = :key");
		}

		v = super.query21Model(sql_sb.toString(), paramMap);
		if (v != null) {
			CacheUtil.addCache(CacheType.AGENT_SETTING, ckey, v);
		}
		return v;
	}
	
	public List<Map> getStationConfs(Long stationId, Long vStatus,boolean allStatus) {
		StringBuilder sql_sb = new StringBuilder("SELECT b.group_id,g.name as group_name,b.id as config_id");
		sql_sb.append(",b.name,b.key,b.remark,b.type,b.title,b.expand,v.id");
		sql_sb.append(",COALESCE(v.value, b.init_value) as value,b.source FROM agent_base_config b");
		sql_sb.append(" LEFT JOIN agent_base_config_group g ON g.id=b.group_id");
		sql_sb.append(" INNER JOIN agent_base_config_value v ON b.id=v.config_id");
		sql_sb.append(" WHERE v.station_id = :stationId");
		if (allStatus) {
			sql_sb.append(" AND b.status != ");
			sql_sb.append(AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE);
		} else {
			sql_sb.append(" AND b.status = ");
			sql_sb.append(AgentBaseConfig.AGENT_BASE_CONFIG_ENABLE);
		}
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (Validator.isNotNull(vStatus)) {
			sql_sb.append(" AND v.status = :status");
			paramMap.put("status", vStatus);

		}
		sql_sb.append(" ORDER BY g.order_no,b.order_no");
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public void batchSaveAndUpdate(List<AgentBaseConfigValue> inLst, List<AgentBaseConfigValue> updLst,
			Long stationId) {
		batchInsert(inLst);
		batchUpdate(updLst);
		CacheUtil.delCacheByPrefix(CacheType.AGENT_SETTING, "skey_" + stationId + "_");
	}

	public void updateValue(AgentBaseConfigValue saveAbcv, String key) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", saveAbcv.getId());
		paramMap.put("value", saveAbcv.getValue());
		if (update("update agent_base_config_value set value=:value where id=:id", paramMap) == 1) {
			CacheUtil.addCache(CacheType.AGENT_SETTING, "skey_" + saveAbcv.getStationId() + "_" + key, saveAbcv);
		}
	}

	public void updateRealMoneyConfig() {
		List<String> list = new ArrayList<>();
		StringBuilder sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=300000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_ag_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=200000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_bbin_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=100000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_mg_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_pt_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_qt_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_ab_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_og_limit.name()).append("';");
		list.add(sql.toString());

		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_ds_limit.name()).append("';");
		list.add(sql.toString());
		
		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_cq9_limit.name()).append("';");
		list.add(sql.toString());
		
		sql = new StringBuilder();
		sql.append("update agent_base_config_value set value=20000 from agent_base_config b");
		sql.append(" where agent_base_config_value.config_id=b.id and b.key='");
		sql.append(StationConfig.live_transfor_ebet_limit.name()).append("';");
		list.add(sql.toString());
		
		
		String[] sqls =new String[list.size()];
		list.toArray(sqls);
		batchUpdate(sqls);
	}
	
	public void deleteAgentConfigValByStationId(Long stationId) {
		StringBuffer sql_sb = new StringBuffer();
		sql_sb.append("delete from agent_base_config_value where station_id =:stationId");
		HashMap<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		super.update(sql_sb.toString(), paramMap);
	}
}
