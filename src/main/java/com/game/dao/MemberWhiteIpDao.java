package com.game.dao;

import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONArray;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.MemberWhiteIp;

@Repository
public class MemberWhiteIpDao extends JdbcUtilImpl<MemberWhiteIp> {
	
	public List<MemberWhiteIp> getStationWhiteIpList(long staionId,Long type){
		String json = CacheUtil.getCache(CacheType.MEMBER_WHITE_IP_LIST, staionId+":"+type,String.class);
		if(json != null){
			List<MemberWhiteIp> array = JSONArray.parseArray(json, MemberWhiteIp.class);
			if (!array.isEmpty()) {
				return array;
			}
		}
		String sql = "select * from member_white_ip where station_id = :stationId and type =:type and status = "+MemberWhiteIp.STATUS_ENABLED ;
		List<MemberWhiteIp> ips = super.query2Model(sql,MixUtil.newHashMap("stationId",staionId,"type",type));
		CacheUtil.addCache(CacheType.MEMBER_WHITE_IP_LIST, staionId+":"+type, ips);
		return ips;
	}
	
	public Page queryPage(Long stationId,Long type){
		StringBuffer sql = new StringBuffer("select * from member_white_ip where 1 = 1 ");
		if(Validator.isNotNull(stationId)){
			sql.append(" and station_id = :stationId ");
		}
		if(Validator.isNotNull(type)){
			sql.append(" and type = :type ");
		}
		sql.append(" order by create_datetime desc ");
		return super.page2CamelMap(sql.toString(), MixUtil.newHashMap("stationId",stationId,"type",type));
	}
}
