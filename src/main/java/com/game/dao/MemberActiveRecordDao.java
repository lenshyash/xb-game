package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MemberActiveRecord;
import com.game.model.vo.ActiveRecordVo;

@Repository
public class MemberActiveRecordDao extends JdbcUtilImpl<MemberActiveRecord> {

	public Page getRecordPage(ActiveRecordVo arvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active_record");
		sql_sb.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();

		if (Validator.isNotNull(arvo.getActiveId())) {
			sql_sb.append(" AND active_id = :activeID");
			paramMap.put("activeID", arvo.getActiveId());
		}

		if (Validator.isNotNull(arvo.getRecordType())) {
			sql_sb.append(" AND award_type = :recordType");
			paramMap.put("recordType", arvo.getRecordType());
		}

		if (Validator.isNotNull(arvo.getStatus())) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", arvo.getStatus());
		}

		if (Validator.isNotNull(arvo.getAccountId())) {
			sql_sb.append(" AND account_id = :accountId");
			paramMap.put("accountId", arvo.getAccountId());
		}

		if (Validator.isNotNull(arvo.getStationId())) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", arvo.getStationId());
		}
		if (StringUtil.isNotEmpty(arvo.getAccount())) {
			sql_sb.append(" AND account = :account");
			paramMap.put("account", arvo.getAccount());
		}

		if (StringUtil.isNotEmpty(arvo.getBegin())) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", arvo.getBegin());
		}

		if (StringUtil.isNotEmpty(arvo.getEnd())) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", arvo.getEnd());
		}

		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<MemberActiveRecord> getRecordList(ActiveRecordVo arvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active_record");
		sql_sb.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();

		if (Validator.isNotNull(arvo.getActiveId())) {
			sql_sb.append(" AND active_id = :activeID");
			paramMap.put("activeID", arvo.getActiveId());
		}

		if (Validator.isNotNull(arvo.getRecordType())) {
			sql_sb.append(" AND award_type = :recordType");
			paramMap.put("recordType", arvo.getRecordType());
		}
		
		if (arvo.getRecordTypes() != null && !arvo.getRecordTypes().isEmpty()) {
			sql_sb.append(" AND award_type IN(");
			for (Long type : arvo.getRecordTypes()) {
				sql_sb.append(type).append(",");
			}
			sql_sb.delete(sql_sb.length()-1, sql_sb.length());
			sql_sb.append(") ");
		}

		if (Validator.isNotNull(arvo.getStatus())) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", arvo.getStatus());
		}

		if (StringUtil.isNotEmpty(arvo.getOrder())) {
			sql_sb.append(" ORDER BY ").append(arvo.getOrder());
		}

		if (Validator.isNotNull(arvo.getLimit())) {
			sql_sb.append(" LIMIT ").append(arvo.getLimit());
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int hanlderUntreated(MemberActiveRecord mar) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("UPDATE member_active_record");
		sql_sb.append(" SET data_version = data_version+1,");
		sql_sb.append(" status = :status,remark = :remark");
		sql_sb.append(" WHERE id = :recordId");
		sql_sb.append(" AND data_version = :version");
		sql_sb.append(" AND status = ").append(MemberActiveRecord.STATUS_UNTREATED);

		Map paramMap = MixUtil.newHashMap("recordId", mar.getId(), "version", mar.getDataVersion(), "status",
				mar.getStatus(), "remark", mar.getRemark());
		return super.update(sql_sb.toString(), paramMap);
	}
	
}
