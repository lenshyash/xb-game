package com.game.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentLotteryQuoto;
import com.game.model.vo.StationVo;
@Repository
public class AgentLotteryQuotoDao extends JdbcUtilImpl<AgentLotteryQuoto> {
	
	public Boolean updateBuyMoney(Long stationId, BigDecimal money) {
		StringBuilder sb_sql = new StringBuilder();
		HashMap<String,Object> map = new HashMap<String, Object>();
	
		sb_sql.append("update agent_lottery_quoto set buy_money = buy_money+ :money,last_update_datetime = :date WHERE station_id = :stationId");
		map.put("money", money);
		map.put("stationId", stationId);
		map.put("date", new Date());
		return super.update(sb_sql.toString(), map)==1;
	}

	public Boolean updateBuyMoneySubtract(Long stationId, BigDecimal money) {
		StringBuilder sb_sql = new StringBuilder();
		HashMap<String,Object> map = new HashMap<String, Object>();

		sb_sql.append("update agent_lottery_quoto set buy_money = buy_money- :money,last_update_datetime = :date WHERE station_id = :stationId");
		map.put("money", money);
		map.put("stationId", stationId);
		map.put("date", new Date());
		return super.update(sb_sql.toString(), map)==1;
	}


	public AgentLotteryQuoto getOneByStationId(Long stationId) {
		StringBuilder sb_sql = new StringBuilder();
		HashMap<String,Object> map = new HashMap<String, Object>();
		
		sb_sql.append("select * from agent_lottery_quoto where station_id = :stationId");
		map.put("stationId", stationId);
		
		return super.query21Model(sb_sql.toString(), map);
	}
	
	public Page<Map> getPage() {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select q.*,s.floder,s.name from agent_lottery_quoto q left join sys_station s on q.station_id = s.id");
		Map<String, Object> paramMap = new HashMap<>();

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}
}
