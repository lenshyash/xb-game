package com.game.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.CommonRedpacket;

@Repository
public class CommonRedpacketDao extends JdbcUtilImpl<CommonRedpacket> {

	public Page<CommonRedpacket> getPage(Long stationId) {
		return super.paged2Obj(
				"SELECT * FROM common_redpacket WHERE station_id =:stationId ORDER BY create_datetime DESC",
				MixUtil.newHashMap("stationId", stationId));
	}

	public List<CommonRedpacket> getRedpacketList(Long stationId, Long status) {

		StringBuilder sql_sb = new StringBuilder("SELECT * FROM common_redpacket WHERE 1 = 1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (status != null && status > 0) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int grabRedPacket(Long id, BigDecimal money) {
		StringBuilder sql_sb = new StringBuilder("UPDATE common_redpacket SET ");
		sql_sb.append(" remain_money = remain_money - :money,");
		sql_sb.append(" remain_number = remain_number - 1");
		sql_sb.append(" WHERE id =:id AND remain_money > 0 AND remain_number > 0");
		return super.update(sql_sb.toString(), MixUtil.newHashMap("money", money, "id", id));
	}

	public boolean checkDatetime(Long stationId, Integer status, Date begin, Date end) {
		StringBuilder sql_sb = new StringBuilder("SELECT COUNT(1) FROM common_redpacket");
		sql_sb.append(" WHERE station_id = :stationId AND status = :status");
		sql_sb.append(" AND ((create_datetime <:begin AND end_datetime >= :begin)");
		sql_sb.append(" or (create_datetime <:end AND end_datetime >= :end)");
		sql_sb.append(" or (create_datetime >=:begin AND end_datetime <= :end))");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("status", status);
		paramMap.put("begin", begin);
		paramMap.put("end", end);
		return queryForInt(sql_sb.toString(), paramMap) > 0;
	}

	public List<CommonRedpacket> getExpireRedpacketList(Long stationId) {

		StringBuilder sql_sb = new StringBuilder("SELECT * FROM common_redpacket WHERE status = 2");
		Map<String, Object> paramMap = new HashMap<>();
		sql_sb.append(" AND end_datetime <= :now");
		paramMap.put("now", new Date());
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int expireRedPacket(Long id) {
		StringBuilder sql_sb = new StringBuilder("UPDATE common_redpacket SET ");
		sql_sb.append(" status = ").append(CommonRedpacket.STATUS_EXPIRED);
		sql_sb.append(" WHERE id =:id AND status = :status");
		return super.update(sql_sb.toString(), MixUtil.newHashMap("id", id, "status", CommonRedpacket.STATUS_ENDALBED));
	}

}
