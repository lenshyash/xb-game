package com.game.dao;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.stereotype.Repository;

import com.game.model.MemberActivePlayNum;

@Repository
public class MemberActivePlayNumDao extends JdbcUtilImpl<MemberActivePlayNum> {
}
