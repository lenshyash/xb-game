package com.game.dao;

import java.math.BigDecimal;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.model.MemberDepositInfo;
import com.game.model.MemberWithdrawInfo;

@Repository
public class MemberDrawInfoDao extends JdbcUtilImpl<MemberWithdrawInfo> {

	public Long handlerDraw(long accountId, Long count, BigDecimal total) {
		RedisAPI.delCache(Contants.MEMBER_LEVEL_INFO_MEMBER_ID + accountId);
		return super.queryForLong("SELECT opt_withdraw_info(" + accountId + "," + count + "," + total + ")");
	}
}
