package com.game.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.AccountVo;
import com.game.model.vo.TotalStatisticVo;
import com.game.util.BigDecimalUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class TotalStatisticReportDao extends JdbcUtilImpl<TotalStatisticVo> {

	@Autowired
	private SysAccountDao accountDao;

	public Page<TotalStatisticVo> findTeamStatistic(Long stationId, String account, String parentIds, Long agentId, Date startTime, Date endTime) {
		StringBuilder rquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		StringBuilder bquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtils.isNotEmpty(account)) {
			rquery.append(" AND a.account =:account");
			bquery.append(" AND a.account =:account");
			map.put("account", account);
		}
		if (StringUtils.isNotEmpty(parentIds)) {
			map.put("parentIds", parentIds);
			map.put("agentId", agentId);
			rquery.append(" AND (a.parents = :parentIds or a.id=:agentId)");
			bquery.append(" AND (a.parents = :parentIds or a.id=:agentId)");
		}
		if (startTime != null) {
			map.put("startTime", startTime);
			rquery.append(" AND r.create_datetime >=:startTime");
			bquery.append(" AND b.bet_time >=:startTime");
		}
		if (endTime != null) {
			map.put("endTime", endTime);
			rquery.append(" AND r.create_datetime <=:endTime");
			bquery.append(" AND b.bet_time <=:endTime");
		}

		StringBuilder sql = new StringBuilder("select  m.money,d.*  from (");
		sql.append("select account,id,agent_id,agent_name,account_type,");
		sql.append("sum(betting_amount) as betting_amount,sum(award_amount) as award_amount,sum(deposit_gift_amount) as deposit_gift_amount,");
		sql.append("sum(deposit_amount) as deposit_amount,sum(manual_deposit_amount) as manual_deposit_amount,");
		sql.append("sum(manual_withdraw_amount) as manual_withdraw_amount,sum(withdraw_amount) as withdraw_amount,");
		sql.append("sum(rebate_amount) as rebate_amount,sum(rebate_agent_amount)as rebate_agent_amount  from (");

		// 从帐变中统计
		sql.append("(SELECT a.account,a.id,a.parents,a.agent_id,a.agent_name,a.account_type,");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.SPORT_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_CANCEL_ORDER.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ORDER_CANCEL.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ORDER_CANCEL.getType()).append(") THEN money ELSE 0 END) AS betting_amount,");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.SPORT_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ROLLBACK.getType()).append(") THEN money ELSE 0 END) AS award_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.DEPOSIT_ONLINE_THIRD.getType()).append(",");
		sql.append(MoneyRecordType.DEPOSIT_ONLINE_FAST.getType()).append(",");
		sql.append(MoneyRecordType.DEPOSIT_ONLINE_BANK.getType()).append(") THEN money ELSE 0 END) AS deposit_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_ARTIFICIAL.getType()).append(" THEN money ELSE 0 END) AS manual_deposit_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType()).append(" THEN money ELSE 0 END) AS manual_withdraw_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.WITHDRAW_ONLINE.getType()).append(",");
		sql.append(MoneyRecordType.WITHDRAW_ONLINE_FAILED.getType()).append(") THEN money ELSE 0 END) AS withdraw_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.MEMBER_ROLL_BACK_ADD.getType()).append(",");
		sql.append(MoneyRecordType.MEMBER_ROLL_BACK_SUB.getType()).append(") THEN money ELSE 0 END) AS rebate_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType()).append(" THEN money ELSE 0 END) AS deposit_gift_amount,");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.PROXY_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_REBATE_SUB.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_SUB.getType()).append(") THEN money ELSE 0 END) AS rebate_agent_amount ");
		sql.append("FROM mny_money_record r INNER JOIN sys_account a ON a.id = r.account_id ");
		sql.append(rquery.toString()).append(" GROUP BY a.id)");
		// 从真人投注中统计
		sql.append("UNION all");
		sql.append("(select a.account,a.id,a.parents,a.agent_id,a.agent_name,a.account_type, sum(-bet_money) AS betting_amount, ");
		sql.append("sum(CASE WHEN b.game_type IN(1,2) THEN (b.bet_money+b.pay_money) WHEN b.game_type=3 THEN pay_money ELSE 0 END) AS award_amount,");
		sql.append("0,0,0,0,0,0,0 from real_game_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append(" GROUP BY a.id)");
		// 从电子游艺投注中统计
		sql.append("UNION all");
		sql.append("(select a.account,a.id,a.parents,a.agent_id,a.agent_name,a.account_type, sum(-bet_money) AS betting_amount, sum(pay_money) AS award_amount,");
		sql.append("0,0,0,0,0,0,0 from real_egame_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append(" GROUP BY a.id)");
		sql.append(")e GROUP BY account,id,agent_id,agent_name,account_type");
		sql.append(")d join mny_money m ON  d.id=m.account_id");
		return voPage(page2CamelMap(sql.toString(), map), stationId, startTime, endTime);
	}

	/**
	 * 重构分页数据
	 * 
	 * @param page
	 * @return
	 */
	private Page<TotalStatisticVo> voPage(Page<Map> page, Long stationId, Date startTime, Date endTime) {
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<TotalStatisticVo> list = new ArrayList<>();
			TotalStatisticVo vo = null;
			for (Map rmap : page.getList()) {
				vo = new TotalStatisticVo();
				vo.setAccount((String) rmap.get("account"));
				vo.setBalance((BigDecimal) rmap.get("money"));
				if (rmap.containsKey("id")) {
					vo.setAccountId(Long.valueOf(rmap.get("id").toString()));
				}
				if (rmap.containsKey("agentId")) {
					vo.setAgentId(Long.valueOf(rmap.get("agentId").toString()));
				}
				vo.setAgentName((String) rmap.get("agentName"));
				vo.setAccountType((Integer) rmap.get("accountType"));
				vo.setBettingAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("bettingAmount")));
				vo.setAwardAmount((BigDecimal) rmap.get("awardAmount"));
				vo.setDepositAmount((BigDecimal) rmap.get("depositAmount"));
				vo.setManualDepositAmount((BigDecimal) rmap.get("manualDepositAmount"));
				vo.setDepositGiftAmount((BigDecimal) rmap.get("depositGiftAmount"));
				vo.setManualWithdrawAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("manualWithdrawAmount")));
				vo.setWithdrawAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("withdrawAmount")));
				vo.setRebateAmount((BigDecimal) rmap.get("rebateAmount"));
				vo.setRebateAgentAmount((BigDecimal) rmap.get("rebateAgentAmount"));
				setTeamProfit(vo, stationId, startTime, endTime);
				list.add(vo);
			}
			return new Page<>(page.getStart(), page.getTotalCount(), page.getPageSize(), list);
		}
		return new Page<>();
	}

	/**
	 * 设置团队盈亏
	 * 
	 * @param vo
	 */
	private void setTeamProfit(TotalStatisticVo vo, Long stationId, Date startTime, Date endTime) {
		if (vo.getAccountType() != null && vo.getAccountType() == 4) {// 代理才设置团队盈亏
			String parentIds = null;
			Long agentId = null;
			if (StringUtils.isNotEmpty(vo.getAccount())) {
				AccountVo avo = new AccountVo();
				avo.setStationId(stationId);
				avo.setAccount(vo.getAccount());
				avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
				SysAccount agent = accountDao.queryAccount(avo);
				if (agent == null) {
					return;
				}
				parentIds = agent.getParents();
				if (StringUtil.isEmpty(parentIds)) {
					parentIds = ",";
				}
				parentIds = parentIds + agent.getId() + ",";
				agentId = agent.getId();
			}
			vo.setTeamProfit(calcTeamProfit(stationId, parentIds, agentId, startTime, endTime));
		}
	}

	private BigDecimal calcTeamProfit(Long stationId, String parentIds, Long agentId, Date startTime, Date endTime) {
		StringBuilder rquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		StringBuilder bquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtils.isNotEmpty(parentIds)) {
			map.put("agentId", agentId);
			map.put("parentIds", parentIds);
			rquery.append(" AND (a.parents =:parentIds OR a.id = :agentId)");
			bquery.append(" AND (a.parents =:parentIds OR a.id = :agentId)");
		}
		if (startTime != null) {
			map.put("startTime", startTime);
			rquery.append(" AND r.create_datetime >=:startTime");
			bquery.append(" AND b.bet_time >=:startTime");
		}
		if (endTime != null) {
			map.put("endTime", endTime);
			rquery.append(" AND r.create_datetime <=:endTime");
			bquery.append(" AND b.bet_time <=:endTime");
		}
		StringBuilder sql = new StringBuilder("select  -sum(amount) from(");

		// 从帐变中统计
		sql.append("(SELECT sum(CASE WHEN r.type IN(").append(MoneyRecordType.SPORT_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_CANCEL_ORDER.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ORDER_CANCEL.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ORDER_CANCEL.getType()).append(",");
		sql.append(MoneyRecordType.MEMBER_ROLL_BACK_ADD.getType()).append(",");
		sql.append(MoneyRecordType.MEMBER_ROLL_BACK_SUB.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_REBATE_SUB.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_SUB.getType()).append(") THEN money ELSE 0 END) AS amount ");
		sql.append("FROM mny_money_record r INNER JOIN sys_account a ON a.id = r.account_id ").append(rquery.toString()).append(")");
		// 从真人投注中统计
		sql.append("UNION all");
		sql.append("(select sum((CASE WHEN b.game_type IN(1,2) THEN (b.bet_money+b.pay_money) WHEN b.game_type=3 THEN pay_money ELSE 0 END)-bet_money) AS award_amount");
		sql.append(" from real_game_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append(")");
		// 从电子游艺投注中统计
		sql.append("UNION all");
		sql.append("(select sum(pay_money-bet_money) AS amount");
		sql.append(" from real_egame_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append("))e");
		String amountStr = queryForString(sql.toString(), map);
		return BigDecimalUtil.toBigDecimal(amountStr);
	}

	public TotalStatisticVo getTotalStatistic(Long stationId, String account, String parentIds, Long agentId, Date startTime, Date endTime) {
		StringBuilder rquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		StringBuilder bquery = new StringBuilder(" WHERE a.flag_active >=1 and a.station_id=:stationId ");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtils.isNotEmpty(account)) {
			rquery.append(" AND a.account =:account");
			bquery.append(" AND a.account =:account");
			map.put("account", account);
		}
		if (StringUtils.isNotEmpty(parentIds)) {
			map.put("agentId", agentId);
			map.put("parentIds", parentIds);
			rquery.append(" AND (a.parents =:parentIds OR a.id = :agentId)");
			bquery.append(" AND (a.parents =:parentIds OR a.id = :agentId)");
		}
		if (startTime != null) {
			map.put("startTime", startTime);
			rquery.append(" AND r.create_datetime >=:startTime");
			bquery.append(" AND b.bet_time >=:startTime");
		}
		if (endTime != null) {
			map.put("endTime", endTime);
			rquery.append(" AND r.create_datetime <=:endTime");
			bquery.append(" AND b.bet_time <=:endTime");
		}

		StringBuilder sql = new StringBuilder("select sum(m.money) as money,sum(betting_amount) as betting_amount,sum(award_amount) as award_amount,");
		sql.append("sum(deposit_amount) as deposit_amount,sum(manual_deposit_amount) as manual_deposit_amount,sum(deposit_gift_amount) as deposit_gift_amount,");
		sql.append("sum(manual_withdraw_amount) as manual_withdraw_amount,sum(withdraw_amount) as withdraw_amount,");
		sql.append("sum(rebate_amount) as rebate_amount,sum(rebate_agent_amount)as rebate_agent_amount  from (");

		// 从帐变中统计
		sql.append("(SELECT a.id, sum(CASE WHEN r.type IN(").append(MoneyRecordType.SPORT_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_CANCEL_ORDER.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_BETTING.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ORDER_CANCEL.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ORDER_CANCEL.getType()).append(") THEN money ELSE 0 END) AS betting_amount,");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.SPORT_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.SPORT_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_AWARD.getType()).append(",");
		sql.append(MoneyRecordType.MARK_SIX_ROLLBACK.getType()).append(",");
		sql.append(MoneyRecordType.LOTTERY_ROLLBACK.getType()).append(") THEN money ELSE 0 END) AS award_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.DEPOSIT_ONLINE_THIRD.getType()).append(",");
		sql.append(MoneyRecordType.DEPOSIT_ONLINE_FAST.getType()).append(",");
		sql.append(MoneyRecordType.DEPOSIT_ONLINE_BANK.getType()).append(") THEN money ELSE 0 END) AS deposit_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_ARTIFICIAL.getType()).append(" THEN money ELSE 0 END) AS manual_deposit_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType()).append(" THEN money ELSE 0 END) AS manual_withdraw_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.WITHDRAW_ONLINE.getType()).append(",");
		sql.append(MoneyRecordType.WITHDRAW_ONLINE_FAILED.getType()).append(") THEN money ELSE 0 END) AS withdraw_amount, ");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.MEMBER_ROLL_BACK_ADD.getType()).append(",");
		sql.append(MoneyRecordType.MEMBER_ROLL_BACK_SUB.getType()).append(") THEN money ELSE 0 END) AS rebate_amount, ");
		sql.append("sum(CASE WHEN r.type = ").append(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType()).append(" THEN money ELSE 0 END) AS deposit_gift_amount,");
		sql.append("sum(CASE WHEN r.type IN(").append(MoneyRecordType.PROXY_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_REBATE_SUB.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_ADD.getType()).append(",");
		sql.append(MoneyRecordType.PROXY_MULTI_REBATE_SUB.getType()).append(") THEN money ELSE 0 END) AS rebate_agent_amount ");
		sql.append("FROM mny_money_record r INNER JOIN sys_account a ON a.id = r.account_id ");
		sql.append(rquery.toString()).append(" GROUP BY a.id)");
		// 从真人投注中统计
		sql.append("UNION all");
		sql.append("(select a.id, sum(-bet_money) AS betting_amount, ");
		sql.append("sum(CASE WHEN b.game_type IN(1,2) THEN (b.bet_money+b.pay_money) WHEN b.game_type=3 THEN pay_money ELSE 0 END) AS award_amount,");
		sql.append("0,0,0,0,0,0,0 from real_game_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append(" GROUP BY a.id)");
		// 从电子游艺投注中统计
		sql.append("UNION all");
		sql.append("(select a.id, sum(-bet_money) AS betting_amount, sum(pay_money) AS award_amount,");
		sql.append("0,0,0,0,0,0,0 from real_egame_bet b INNER join sys_account a ON a.account=b.account and a.station_id=b.station_id ");
		sql.append(bquery.toString()).append(" GROUP BY a.id)");
		sql.append(")e join mny_money m ON  e.id=m.account_id");
		Map<String, Object> rmap = selectSingleCamelMap(sql.toString(), map);

		TotalStatisticVo vo = new TotalStatisticVo();
		vo.setAccount("总计:");
		vo.setBettingAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("bettingAmount")));
		vo.setAwardAmount((BigDecimal) rmap.get("awardAmount"));
		vo.setDepositAmount((BigDecimal) rmap.get("depositAmount"));
		vo.setManualDepositAmount((BigDecimal) rmap.get("manualDepositAmount"));
		vo.setManualWithdrawAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("manualWithdrawAmount")));
		vo.setWithdrawAmount(BigDecimalUtil.abs((BigDecimal) rmap.get("withdrawAmount")));
		vo.setRebateAmount((BigDecimal) rmap.get("rebateAmount"));
		vo.setDepositGiftAmount((BigDecimal) rmap.get("depositGiftAmount"));
		vo.setRebateAgentAmount((BigDecimal) rmap.get("rebateAgentAmount"));
		vo.setBalance((BigDecimal) rmap.get("money"));
		vo.setTeamProfit(BigDecimalUtil.subtract(vo.getBettingAmount(), vo.getAwardAmount(), vo.getRebateAgentAmount(), vo.getRebateAmount()));
		return vo;
	}
}
