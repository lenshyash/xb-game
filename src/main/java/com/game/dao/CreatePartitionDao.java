package com.game.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.stereotype.Repository;

import com.game.util.DateUtil;

@Repository
public class CreatePartitionDao extends JdbcUtilImpl {

	public int createLotteryDataPartition(Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", "bc_lottery_data_" + DateUtil.formatDate(date, "yyyyMM"));
		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
		return queryForInt("select create_lottery_data_partition(:tableName,:startDate ,:endDate )", map);
	}

	public int createLotteryOrderPartition(Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", "bc_lottery_order_" + DateUtil.formatDate(date, "yyyyMM"));
		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
		return queryForInt("select create_lottery_order_partition(:tableName,:startDate ,:endDate )", map);
	}

//	public int createMemberRollBackRecordPartition(Date date) {
//		Map<String, Object> map = new HashMap<>();
//		map.put("tableName", "member_roll_back_record_" + DateUtil.formatDate(date, "yyyyMM"));
//		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
//		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		c.add(Calendar.MONTH, 1);
//		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
//		return queryForInt("select create_member_roll_back_record_partition(:tableName,:startDate ,:endDate )", map);
//	}

	public int createProxyMultiRebateRecordPartition(Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", "proxy_multi_rebate_record_" + DateUtil.formatDate(date, "yyyyMM"));
		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
		return queryForInt("select create_proxy_multi_rebate_record_partition(:tableName,:startDate ,:endDate )", map);
	}

	public int createMemberBackwaterRecordPartition(Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", "member_backwater_record_" + DateUtil.formatDate(date, "yyyyMM"));
		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
		return queryForInt("select create_member_backwater_record_partition(:tableName,:startDate ,:endDate )", map);
	}

	public int createBetNumRecordPartition(Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", "bet_num_record_" + DateUtil.formatDate(date, "yyyyMM"));
		map.put("startDate", DateUtil.formatDate(date, "yyyy-MM-01"));
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		map.put("endDate", DateUtil.formatDate(c.getTime(), "yyyy-MM-01"));
		return queryForInt("select create_bet_num_record_partition(:tableName,:startDate ,:endDate )", map);
	}

}
