package com.game.dao;

import java.math.BigDecimal;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.model.MemberDepositInfo;

@Repository
public class MemberDepositInfoDao extends JdbcUtilImpl<MemberDepositInfo> {

	public Long handlerDeposit(long accountId, Long count, BigDecimal total) {
		RedisAPI.delCache(Contants.MEMBER_LEVEL_INFO_MEMBER_ID + accountId);
		return super.queryForLong("SELECT opt_deposit_info(" + accountId + "," + count + "," + total + ")");
	}

	public void deleteByAccountId(Long id) {
		// TODO Auto-generated method stub
		String sql = "delete from member_deposit_info where account_id = :accountId";
		super.update(sql, MixUtil.newHashMap("accountId", id));
	}
}
