package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentDepositBank;
import com.game.util.StationUtil;

@Repository
public class AgentDepositBankDao extends JdbcUtilImpl<AgentDepositBank> {
	public Page<Map> getPage() {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT b.*,p.name AS pay_name,p.company AS pay_com,b.sort_no,p.icon_css");
		sql_sb.append(" FROM agent_deposit_bank b LEFT JOIN sys_pay_platform p ON b.pay_platform_id = p.id");
		sql_sb.append(" WHERE b.station_id= :stationId order by b.sort_no asc");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId());
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<Map> getBanks() {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT b.*,p.name AS pay_name,p.company AS pay_com,p.icon_css");
		sql_sb.append(" FROM agent_deposit_bank b LEFT JOIN sys_pay_platform p ON b.pay_platform_id = p.id");
		sql_sb.append(" WHERE b.station_id= :stationId");
		sql_sb.append(" AND b.status= :status order by b.sort_no asc");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status",
				AgentDepositBank.STATUS_ENABLE);
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public void updateStatus(Integer status, Long id, Long stationId) {
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", status, "id", id);
		update("update agent_deposit_bank set status=:status where id=:id and station_id=:stationId", paramMap);
	}
}
