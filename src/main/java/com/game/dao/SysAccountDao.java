package com.game.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.DocFlavor.STRING;

import com.game.cache.CacheManager;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.dao.lottery.LongRowMapper;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.model.vo.ReportParamVo;
import com.game.user.online.OnlineInfo;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.ValidateUtil;

@Repository
public class SysAccountDao extends JdbcUtilImpl<SysAccount> {
	public SysAccount queryDdgd(String account, Long type,Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * ");
		sql_sb.append(" FROM sys_account a");
		sql_sb.append(" WHERE lower(a.account) = :account AND a.account_type = :type");
		sql_sb.append(" AND station_id = :stationId");

		return super.query21Model(sql_sb.toString(),
				MixUtil.newHashMap("account", StringUtil.isEmpty(account) ? "" : account.toLowerCase(), "type", type,"stationId",stationId));
	}

	public List<SysAccount> queryAccountsByType(Long type) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * ");
		sql_sb.append(" FROM sys_account a");
		sql_sb.append(" WHERE a.account_type = :type");

		return super.query2Model(sql_sb.toString(), MixUtil.newHashMap("type", type));
	}

	/**
	 * 通过账号和密码获取用户 用于登陆验证
	 * 
	 * @param account
	 * @param pwd
	 * @return
	 */
	public SysAccount getLoginAccount(String account, Long type) {
		account = account.trim();
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.*");
		sql_sb.append(" FROM sys_account a");
		sql_sb.append(" WHERE flag_active >= 1 AND a.station_id = :stationId");
		sql_sb.append(" AND lower(account) = :account");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "account",
				StringUtil.isEmpty(account) ? "" : account.toLowerCase());
		if (type == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER || type == SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER) {
			sql_sb.append(" AND account_type IN (");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			sql_sb.append(",");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
			sql_sb.append(")");
		} else if (type == SysAccount.ACCOUNT_PLATFORM_AGENT) {
			sql_sb.append(" AND account_type IN (");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT);
			sql_sb.append(",");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL);
			sql_sb.append(")");
		} else if (type == SysAccount.ACCOUNT_PLATFORM_MEMBER) {
			// 开启代理登录到会员，判断语句加入代理类型
			if ("on".equals(StationConfigUtil.get(StationConfig.onoff_daili_login_member_platfrom))) {
				sql_sb.append(" AND account_type IN (");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_GUIDE);
				sql_sb.append(")");
			} else {
				sql_sb.append(" AND account_type IN (");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
				sql_sb.append(",");
				sql_sb.append(SysAccount.ACCOUNT_PLATFORM_GUIDE);
				sql_sb.append(")");
			}
		} else {
			sql_sb.append(" AND account_type = :type");
			paramMap.put("type", type);
		}
		return super.query21Model(sql_sb.toString(), paramMap);
	}

	/**
	 * 通过账号和密码获取用户 用于登陆验证
	 * 
	 * @param accountVo
	 * @return
	 */
	public SysAccount queryAccount(AccountVo avo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_account");
		sql_sb.append(" WHERE flag_active >= 1");

		Map paramMap = MixUtil.newHashMap();
		if (StringUtil.isNotEmpty(avo.getAccount())) {
			sql_sb.append(" AND lower(account) = :account");
			paramMap.put("account", avo.getAccount().trim().toLowerCase());
		}
		
		if (StringUtil.isNotEmpty(avo.getParents())) {
			sql_sb.append(" AND parents LIKE :parents");
			paramMap.put("parents", avo.getParents()+"%");
		}

		if (Validator.isNotNull(avo.getStationId())) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", avo.getStationId());
		}

		if (Validator.isNotNull(avo.getAccountType())) {
			sql_sb.append(" AND account_type=:type");
			paramMap.put("type", avo.getAccountType());
		}
		List<SysAccount> accounts = super.query2Model(sql_sb.toString(), paramMap);
		if (accounts == null || accounts.size() == 0) {
			return null;
		}
		return accounts.get(0);
	}

	public SysAccount getByAccountAndTypeAndStationId(String account, Long stationId, Long accountType) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM sys_account WHERE flag_active >= 1 AND lower(account) = :account");

		Map paramMap = MixUtil.newHashMap();
		paramMap.put("account", StringUtil.isEmpty(account) ? "" : account.toLowerCase());
		sql_sb.append(" AND station_id = :stationId");
		paramMap.put("stationId", stationId);
		if(accountType!=null){
			sql_sb.append(" AND account_type=:type");
			paramMap.put("type", accountType);
		}
		List<SysAccount> accounts = super.query2Model(sql_sb.toString(), paramMap);
		if (accounts == null || accounts.size() == 0) {
			return null;
		}
		return accounts.get(0);
	}

	public Page<Map> getPage(AccountVo accountVo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whs = new StringBuilder();
		StringBuilder sql_odr = new StringBuilder();
		sql_cols.append("SELECT a.id, a.create_datetime, a.account_status, a.account_type,a.register_ip,a.report_type,a.change_daili_time");
		sql_cols.append(",a.register_url,a.register_os,a.agent_id,a.agent_name,a.parent_names,a.parents");
		sql_cols.append(",a.group_id,a.station_id,a.last_login_datetime,a.rate");
		sql_cols.append(",a.level,a.last_login_ip,a.score,a.online,a.level_group,a.abnormal_flag, i.*, m.money,a.remark");
		sql_cols.append(",s.game_share,s.profit_share");
		sql_tabs.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_tabs.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_tabs.append(" LEFT JOIN proxy_multi_rebate_setting s ON a.id = s.account_id");
		sql_whs.append(" WHERE a.flag_active >=1");

		boolean onlySearch = true;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			if(accountVo.getSearchType()==null || accountVo.getSearchType()==0){
				paramMap.put("account", accountVo.getAccount() + "%");
				sql_whs.append(" AND a.account LIKE :account");
				onlySearch = false;
			}else if(accountVo.getSearchType()==AccountVo.SEARCHTYPE_NEXT){
				paramMap.put("parent_names", "%," + accountVo.getAccount() + ",");
				sql_whs.append(" AND a.parent_names LIKE :parent_names");
			}else if(accountVo.getSearchType()==AccountVo.SEARCHTYPE_NEXT_ALL){
				paramMap.put("parent_names", "%," + accountVo.getAccount() + ",%");
				sql_whs.append(" AND a.parent_names LIKE :parent_names");
			}
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			if (StationUtil.isAgentStation()) {
				String parents = accountVo.getParents();
//				if (StringUtil.equals(accountVo.getSearchType(), AccountVo.SEARCHTYPE_NEXT_ALL)) {
//					parents += "%";
//				}
				paramMap.put("parents", parents);
			} else {
				paramMap.put("parents", accountVo.getParents() + "%");
			}

			if (accountVo.getSearchSelf() != null && accountVo.getSearchSelf()) {
				paramMap.put("curUserId", accountVo.getSelfId());
				sql_whs.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_whs.append(" AND a.parents LIKE :parents");
			}
			onlySearch = false;
		}

		if (Validator.isNotNull(accountVo.getStatus())) {
			paramMap.put("status", accountVo.getStatus());
			sql_whs.append(" AND a.account_status=:status");
		}
		Long accountType = accountVo.getAccountType();
		if (Validator.isNotNull(accountType)) {
			paramMap.put("accountType", accountType);
			if (accountVo.getGeneralSearch() != null && accountVo.getGeneralSearch()) {
				sql_whs.append(" AND a.account_type IN (:accountType,")
						.append(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL).append(")");
			} else if (StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_MEMBER) || StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
				
				//x00242特殊处理 有层级条件时会员中也要显示代理数据
				if(accountVo.getMemberPageQuery()!=null && accountVo.getMemberPageQuery() && accountVo.getLevelArray()!=null &&accountVo.getLevelArray().length>0) {
					sql_whs.append(" AND (a.account_type IN (:accountType").append(")")
					.append(" or").append("(").append("account_type=").append(SysAccount.ACCOUNT_PLATFORM_AGENT);
					
					Integer[] levelArray = accountVo.getLevelArray();
					sql_whs.append(" and (");
					for(Integer l:levelArray){
						sql_whs.append(" a.level =:level").append(l).append(" or");
						paramMap.put("level"+l, l);
					}
					sql_whs.delete(sql_whs.length()-3, sql_whs.length());
					sql_whs.append(" ) )");
					accountVo.setLevelArray(null);
				}else {
					sql_whs.append(" AND (a.account_type IN (:accountType").append(")");
				}
				
				sql_whs.append(")");
				}
		} else {
			paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
			sql_whs.append(" AND a.account_type<>:accountType");
		}

		if (Validator.isNotNull(accountVo.getStationId())) {
			paramMap.put("stationId", accountVo.getStationId());
			sql_whs.append(" AND a.station_id=:stationId");
		}

		if (Validator.isNotNull(accountVo.getAgentId())) {
			paramMap.put("agentId", accountVo.getAgentId());
			sql_whs.append(" AND a.id=:agentId");
		}

		if (Validator.isNotNull(accountVo.getAgentParentId())) {
			paramMap.put("agentParentId", accountVo.getAgentParentId());
			sql_whs.append(" AND a.agent_id=:agentParentId");
		}
		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			paramMap.put("agentName", accountVo.getAgentName());
			sql_whs.append(" AND a.agent_name=:agentName");
			onlySearch = false;
		}

		if (Validator.isNotNull(accountVo.getLevelGroup())) {
			paramMap.put("levelGroup", accountVo.getLevelGroup());
			sql_whs.append(" AND a.level_group=:levelGroup");
		}

		if (StringUtil.isNotEmpty(accountVo.getUserName())) {
			paramMap.put("userName", accountVo.getUserName() + "%");
			sql_whs.append(" AND i.user_name LIKE :userName");
			onlySearch = false;
		}

		if (StringUtil.isNotEmpty(accountVo.getQq())) {
			paramMap.put("qq", accountVo.getQq());
			sql_whs.append(" AND i.qq=:qq");
		}

		if (StringUtil.isNotEmpty(accountVo.getWechat())) {
			paramMap.put("wechat", accountVo.getWechat());
			sql_whs.append(" AND i.wechat=:wechat");
		}
		
		if (StringUtil.isNotEmpty(accountVo.getEmail())) {
			paramMap.put("email", accountVo.getEmail());
			sql_whs.append(" AND i.email=:email");
		}

		if (StringUtil.isNotEmpty(accountVo.getPhone())) {
			paramMap.put("phone", accountVo.getPhone());
			sql_whs.append(" AND i.phone=:phone");
		}

		if (StringUtil.isNotEmpty(accountVo.getCardNo())) {
			paramMap.put("cardNo", accountVo.getCardNo());
			sql_whs.append(" AND i.card_no=:cardNo");
		}

		if (StringUtil.isNotEmpty(accountVo.getLastLoginIp())) {
			paramMap.put("lastLoginIp", accountVo.getLastLoginIp() + "%");
			sql_whs.append(" AND a.last_login_ip LIKE :lastLoginIp");
		}
		if (StringUtil.isNotEmpty(accountVo.getRegIp())) {
			paramMap.put("regIp", accountVo.getRegIp() + "%");
			sql_whs.append(" AND a.register_ip LIKE :regIp");
		}
		if (Validator.isNotNull(accountVo.getLevel())) {
			paramMap.put("level", accountVo.getLevel());
			sql_whs.append(" AND a.level >:level");
		}
		
		if (Validator.isNotNull(accountVo.getReportType())) {
			paramMap.put("reportType", accountVo.getReportType());
			sql_whs.append(" AND a.report_type =:reportType");
		}

		if (Validator.isNotNull(accountVo.getAccountStatus())) {
			paramMap.put("accountStatus", accountVo.getAccountStatus());
			sql_whs.append(" AND a.account_status =:accountStatus");
		}

		
		if (Validator.isNotNull(accountVo.getOnline())) {
			paramMap.put("online", accountVo.getOnline());
			sql_whs.append(" AND a.online =:online");
		}

		if (StringUtil.isNotEmpty(accountVo.getRegisterUrl())) {
			paramMap.put("registerUrl", accountVo.getRegisterUrl());
			sql_whs.append(" AND a.register_url =:registerUrl");
		}
		if (accountVo.getBegin() != null) {
			paramMap.put("begin", accountVo.getBegin());
			sql_whs.append(" AND a.create_datetime>=:begin");
		}
		if (accountVo.getEnd() != null) {
			paramMap.put("end", accountVo.getEnd());
			sql_whs.append(" AND a.create_datetime<:end");
		}

		if (accountVo.getDepositStatus() != null) {
			if (accountVo.getDepositStatus() == 1) {
				sql_whs.append(
						" AND a.id in(select DISTINCT member_id from mny_com_record where station_id=:stationId and status=2)");
			} else if (accountVo.getDepositStatus() == 2) {
				sql_whs.append(
						" AND a.id not in(select DISTINCT member_id from mny_com_record where station_id=:stationId and status=2)");
			}
		}

		if (accountVo.getMoneyMin() != null) {
			paramMap.put("min1", accountVo.getMoneyMin());
			sql_whs.append(" AND m.money>=:min1");
		}
		if (accountVo.getUnloginDay() != null) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -accountVo.getUnloginDay());
			paramMap.put("lldate", c.getTime());
			sql_whs.append(" AND (a.last_login_datetime is null or a.last_login_datetime<=:lldate)");
		}
		
		if (onlySearch && accountVo.getDailiTopSearch() != null && accountVo.getDailiTopSearch().booleanValue()) {
			sql_whs.append(" AND a.agent_id=0");
		}
		if(StringUtil.isNotEmpty(accountVo.getQzGroupName())) {
			sql_whs.append(" and i.qz_group_name = :qzGroupName");
			paramMap.put("qzGroupName", accountVo.getQzGroupName());
		}
		if(StringUtil.isNotEmpty(accountVo.getRemark())) {
			sql_whs.append(" and a.remark like :remark");
			paramMap.put("remark", "%"+accountVo.getRemark()+"%");
		}
		if(accountVo.getLevelArray()!=null &&accountVo.getLevelArray().length>0) {
			Integer[] levelArray = accountVo.getLevelArray();
			sql_whs.append(" and (");
			for(Integer l:levelArray){
				sql_whs.append(" a.level =:level").append(l).append(" or");
				paramMap.put("level"+l, l);
			}
			sql_whs.delete(sql_whs.length()-3, sql_whs.length());
			sql_whs.append(" ) ");
		}
		if (Validator.isNotNull(accountVo.getAbnormalFlag())) {
			Long flag = accountVo.getAbnormalFlag();
			if(flag==1){
				sql_whs.append(" AND a.abnormal_flag = 1");
			}else{
				sql_whs.append(" AND (abnormal_flag is null or abnormal_flag <>1)");
			}
		}

		//x00242特殊处理 代理界面只允许查询代理数据
		if(accountVo.getAgentPageQuery()!=null && accountVo.getAgentPageQuery()) {
			sql_whs.append(" and a.account_type = :accountTypeAgent");
			paramMap.put("accountTypeAgent", SysAccount.ACCOUNT_PLATFORM_AGENT);
		}
		if (StringUtil.isNotEmpty(accountVo.getSortOrder())) {
			sql_odr.append(" ORDER BY m.money " + accountVo.getSortOrder() + ",a.create_datetime DESC");
		} else if (StationUtil.isAgentStation()) {
			sql_odr.append(" ORDER BY a.create_datetime DESC");
		} else {
			sql_odr.append(" ORDER BY a.account_type DESC,a.create_datetime DESC");

		}
		// if ((StationUtil.isAgentStation() &&
		// StringUtil.equals(accountVo.getAccountType(),
		// SysAccount.ACCOUNT_PLATFORM_AGENT)) || StationUtil.isDailiStation())
		// {
		// sql_cols.append(" ,game_share");
		// sql_tabs.append(" LEFT JOIN proxy_multi_rebate_setting p ON a.id =
		// p.account_id");
		// }

		return super.page2CamelMap(
				sql_cols.append(sql_tabs.toString()).append(sql_whs.toString()).append(sql_odr.toString()).toString(),
				paramMap);
	}

	public Page<Map> getOnlinePage(AccountVo accountVo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whs = new StringBuilder();
		StringBuilder sql_odr = new StringBuilder();
		sql_cols.append("SELECT a.id, a.create_datetime, a.account_status, a.account_type,a.register_ip");
		sql_cols.append(",a.register_url,a.register_os,a.agent_id,a.agent_name,a.parent_names,a.parents");
		sql_cols.append(",a.group_id,a.station_id,a.last_login_datetime,a.rate,a.report_type");
		sql_cols.append(",a.level,a.last_login_ip,a.last_login_device,a.score,a.online,a.level_group,a.abnormal_flag,i.*, m.money,m.balance_gem_money,a.remark");
		sql_tabs.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_tabs.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_whs.append(" WHERE a.flag_active >=1");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			paramMap.put("account", accountVo.getAccount() + "%");
			sql_whs.append(" AND a.account LIKE :account");
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			if (StringUtil.equals(accountVo.getSearchType(), accountVo.SEARCHTYPE_NEXT_ALL)) {
				paramMap.put("parentIds", accountVo.getParents() + "%"); //所有下级  
			} else {
				paramMap.put("parentIds", accountVo.getParents());  //直属下级 
			}

			paramMap.put("agentId", accountVo.getAgentId());
			sql_whs.append(" AND (a.parents LIKE :parentIds or a.id = :agentId)");
		}

		if (Validator.isNotNull(accountVo.getStatus())) {
			paramMap.put("status", accountVo.getStatus());
			sql_whs.append(" AND a.account_status=:status");
		}
		Long accountType = accountVo.getAccountType();
		if (Validator.isNotNull(accountType)) {
			paramMap.put("accountType", accountType);
			if (accountVo.getGeneralSearch() != null && accountVo.getGeneralSearch()) {
				sql_whs.append(" AND a.account_type IN (:accountType,").append(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL).append(")");
			} else if (StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
				sql_whs.append(" AND a.account_type IN (:accountType").append(")");
			} else {
				sql_whs.append(" AND a.account_type=:accountType");
			}
		} else {
			paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
			sql_whs.append(" AND a.account_type not in (:accountType,").append(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER).append(")");
		}
		if(Validator.isNotNull(accountVo.getReportType())) {
			paramMap.put("reportType", accountVo.getReportType());
			sql_whs.append(" AND a.report_type=:reportType");
		}
		if (Validator.isNotNull(accountVo.getStationId())) {
			paramMap.put("stationId", accountVo.getStationId());
			sql_whs.append(" AND a.station_id=:stationId");
		}
		
		if (Validator.isNotNull(accountVo.getAgentParentId())) {
			paramMap.put("agentParentId", accountVo.getAgentParentId());
			sql_whs.append(" AND a.agent_id=:agentParentId");
		}
		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			paramMap.put("agentName", accountVo.getAgentName());
			sql_whs.append(" AND a.agent_name=:agentName");
		}

		if (Validator.isNotNull(accountVo.getLevelGroup())) {
			paramMap.put("levelGroup", accountVo.getLevelGroup());
			sql_whs.append(" AND a.level_group=:levelGroup");
		}

		if (StringUtil.isNotEmpty(accountVo.getUserName())) {
			paramMap.put("userName", accountVo.getUserName() + "%");
			sql_whs.append(" AND i.user_name LIKE :userName");
		}

		if (Validator.isNotNull(accountVo.getOnline())) {
			paramMap.put("online", accountVo.getOnline());
			sql_whs.append(" AND a.online =:online");
		}
		if (accountVo.getUnloginDay() != null) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -accountVo.getUnloginDay());
			paramMap.put("lldate", c.getTime());
			sql_whs.append(" AND (a.last_login_datetime is null or a.last_login_datetime<=:lldate)");
		}
		if(StringUtil.isEmpty(accountVo.getSortName())) {
			sql_odr.append(" order by money desc");
		}else {
			String underline = replaceUpperToUnderline(accountVo.getSortName());
			sql_odr.append(" order by ").append(underline).append(" "+accountVo.getSortOrder()).append(" "+" nulls last");
		}
		
		
		return super.page2CamelMap(
				sql_cols.append(sql_tabs.toString()).append(sql_whs.toString()).append(sql_odr.toString()).toString(),
				paramMap);
	}
	
	
	public Page<Map> getPage4DaiLi(AccountVo accountVo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whs = new StringBuilder();
		StringBuilder sql_odr = new StringBuilder();
		sql_cols.append("SELECT a.id, a.create_datetime, a.account_status, a.account_type,a.report_type,a.bet_num,a.draw_need");
		sql_cols.append(
				",a.agent_id,a.agent_name,a.parent_names,a.parents,a.group_id,a.station_id,a.last_login_datetime");
		sql_cols.append(",a.level,a.last_login_ip,a.score,a.online,a.level_group,a.rate, i.*, m.money,m.balance_gem_money");
		sql_tabs.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_tabs.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_whs.append(" WHERE a.flag_active >=1");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			paramMap.put("account", accountVo.getAccount() + "%");
			paramMap.put("parent_names", "%," + accountVo.getAccount() + ",");
			sql_whs.append(" AND (a.parent_names LIKE :parent_names or a.account LIKE :account)");
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			String parents = accountVo.getParents();
			if (StringUtil.equals(accountVo.getSearchType(), AccountVo.SEARCHTYPE_NEXT_ALL)) {
				parents += "%";
			}
			paramMap.put("parents", parents);

			if (accountVo.getSearchSelf() != null && accountVo.getSearchSelf()) {
				paramMap.put("curUserId", accountVo.getSelfId());
				sql_whs.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_whs.append(" AND a.parents LIKE :parents");
			}
		}

		if (Validator.isNotNull(accountVo.getStatus())) {
			paramMap.put("status", accountVo.getStatus());
			sql_whs.append(" AND a.account_status=:status");
		}
		if (Validator.isNotNull(accountVo.getStartTime())) {
			paramMap.put("startTime", accountVo.getStartTime());
			sql_whs.append(" AND a.create_datetime >=:startTime");
		}
		if (Validator.isNotNull(accountVo.getEndTime())) {
			paramMap.put("endtTime", accountVo.getEndTime());
			sql_whs.append(" AND a.create_datetime <=:endtTime");
		}
		if (Validator.isNotNull(accountVo.getAccountType())) {
			paramMap.put("accountType", accountVo.getAccountType());
			sql_whs.append(" AND a.account_type=:accountType");
		} else {
			paramMap.put("accountType", SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
			sql_whs.append(" AND a.account_type<>:accountType");
		}

		if (Validator.isNotNull(accountVo.getStationId())) {
			paramMap.put("stationId", accountVo.getStationId());
			sql_whs.append(" AND a.station_id=:stationId");
		}

		if (Validator.isNotNull(accountVo.getAgentId())) {
			paramMap.put("agentId", accountVo.getAgentId());
			sql_whs.append(" AND a.id=:agentId");
		}

		if (Validator.isNotNull(accountVo.getAgentParentId())) {
			paramMap.put("agentParentId", accountVo.getAgentParentId());
			sql_whs.append(" AND a.agent_id=:agentParentId");
		}
		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			paramMap.put("agentName", accountVo.getAgentName());
			sql_whs.append(" AND a.agent_name=:agentName");
		}
		if (StringUtil.isNotEmpty(accountVo.getUserName())) {
			paramMap.put("userName", accountVo.getUserName()+"%");
			sql_whs.append(" AND i.user_name like :userName");
		}
		if (Validator.isNotNull(accountVo.getLevel())) {
			paramMap.put("level", accountVo.getLevel());
			sql_whs.append(" AND a.level >:level");
		}
		if (Validator.isNotNull(accountVo.getOnline())) {
			paramMap.put("online", accountVo.getOnline());
			sql_whs.append(" AND a.online =:online");
		}
		if (Validator.isNotNull(accountVo.getLevelGroup())) {
			paramMap.put("levelGroup", accountVo.getLevelGroup());
			sql_whs.append(" AND a.level_group =:levelGroup");
		}

		sql_odr.append(" ORDER BY a.create_datetime DESC,a.account_type DESC");
		if ((StationUtil.isAgentStation()
				&& StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT))
				|| StationUtil.isDailiStation()) {

//			sql_cols.append(" ,CASE WHEN a.account_type IN(1,9) THEN a.rate ELSE p.game_share END AS game_share,p.profit_share");
			sql_cols.append(" ,p.game_share ,p.profit_share");
			sql_tabs.append(" LEFT JOIN proxy_multi_rebate_setting p ON a.id = p.account_id");
		}

		return super.page2CamelMap(
				sql_cols.append(sql_tabs.toString()).append(sql_whs.toString()).append(sql_odr.toString()).toString(),
				paramMap);
	}

	/**
	 * 获取用户信息
	 * 
	 * @param accountId
	 * @return
	 */
	public Map getAccountById(Long accountId, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.score,a.account,a.account_type,a.account_status,a.bet_num,a.draw_need,a.level_group,a.report_type,a.agent_name,a.abnormal_flag");
		sql_sb.append(
				",a.parents,a.parent_names,a.score,a.online,a.last_login_datetime,i.bank_name,i.user_name,i.card_no,i.phone,");
		sql_sb.append("i.province,i.city,i.email,i.bank_address,i.qq,i.sex,i.wechat,i.head_url,m.money,m.money_type_id,m.balance_gem_money");
		sql_sb.append(",a.register_url,a.register_os,a.register_ip,a.id,a.remark,a.chat_token,i.card_no_status ,a.create_datetime,a.im_chat_id,a.im_user_id");
		sql_sb.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id");
		sql_sb.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >= 1");
		sql_sb.append(" AND a.id =");
		sql_sb.append(accountId);
		sql_sb.append(" AND a.station_id =");
		sql_sb.append(stationId);
        return super.selectSingleCamelMap(sql_sb.toString());
	}
	
	/**
	 * 获取用户信息有充值和提款的信息
	 * 
	 * @param accountId
	 * @return
	 */
	public Map getAccountAndDrawComById(Long accountId, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(
				"SELECT a.score,a.account,a.account_type,a.account_status,a.bet_num,a.draw_need,a.level_group,a.report_type");
		sql_sb.append(
				",a.parent_names,a.score,a.online,a.last_login_datetime,a.last_login_ip,i.bank_name,i.user_name,i.card_no,i.phone,");
		sql_sb.append(
				"i.province,i.city,i.email,i.bank_address,i.qq,i.sex,i.wechat,m.money,m.money_type_id,m.balance_gem_money");
		sql_sb.append(
				",a.register_url,a.register_os,a.register_ip,a.id,a.remark,a.chat_token,i.card_no_status ,a.create_datetime");
		sql_sb.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id");
		sql_sb.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >= 1");
		sql_sb.append(" AND a.id =");
		sql_sb.append(accountId);
		sql_sb.append(" AND a.station_id =");
		sql_sb.append(stationId);
		Map accountMap = super.selectSingleCamelMap(sql_sb.toString());
	
		String cacheKey = getCacheKey(accountId, stationId);
		String json = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, cacheKey);
		if(json!=null) {
			JSONObject object = JSON.parseObject(json);
			accountMap.put("countDraw", object.getString("countDraw"));
			accountMap.put("totalDrawMoney", object.getString("totalDrawMoney"));
			accountMap.put("firstDrawDate", DateUtil.timestamp2Data(object.getString("firstDrawDate")));
			accountMap.put("firstDrawMoney", object.getString("firstDrawMoney"));
			accountMap.put("maxDrawDate", DateUtil.timestamp2Data(object.getString("maxDrawDate")));
			accountMap.put("maxDrawMoney", object.getString("maxDrawMoney"));
			accountMap.put("todayDrawTimes", object.getString("todayDrawTimes"));
			accountMap.put("todayDrawMoney", object.getString("todayDrawMoney"));
			
			accountMap.put("countCom", object.getString("countCom"));
			accountMap.put("totalComMoney", object.getString("totalComMoney"));
			accountMap.put("firstComDate", DateUtil.timestamp2Data(object.getString("firstComDate")));
			accountMap.put("firstComMoney", object.getString("firstComMoney"));
			accountMap.put("maxComDate", DateUtil.timestamp2Data(object.getString("maxComDate")));
			accountMap.put("maxComMoney", object.getString("maxComMoney"));
			accountMap.put("todayComTimes", object.getString("todayComTimes"));
			accountMap.put("todayComMoney", object.getString("todayComMoney"));
			
			return accountMap;
		}
		
		Map drawcomMap = getDrawCom(stationId, accountId, DateUtil.toDate(DateUtil.toDateStr(new Date())));
		
		JSONObject string = (JSONObject)JSON.toJSON(drawcomMap);
			
		accountMap.put("countDraw", string.get("countDraw"));
		accountMap.put("totalDrawMoney", string.get("totalDrawMoney"));
		accountMap.put("firstDrawDate", string.get("firstDrawDate"));
		accountMap.put("firstDrawMoney", string.get("firstDrawMoney"));
		accountMap.put("maxDrawDate", string.get("maxDrawDate"));
		accountMap.put("maxDrawMoney", string.get("maxDrawMoney"));
		accountMap.put("todayDrawTimes", string.get("todayDrawTimes"));
		accountMap.put("todayDrawMoney", string.get("todayDrawMoney"));
		
		accountMap.put("countCom", string.get("countCom"));
		accountMap.put("totalComMoney", string.get("totalComMoney"));
		accountMap.put("firstComDate", string.get("firstComDate"));
		accountMap.put("firstComMoney", string.get("firstComMoney"));
		accountMap.put("maxComDate", string.get("maxComDate"));
		accountMap.put("maxComMoney", string.get("maxComMoney"));
		accountMap.put("todayComTimes", string.get("todayComTimes"));
		accountMap.put("todayComMoney", string.get("todayComMoney"));
		
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, cacheKey, drawcomMap);

		return accountMap;
	}

	public List<Map> list(AccountVo accountVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(
				"SELECT a.id, a.create_datetime, a.account_status, a.account_type, a.agent_id,a.agent_name,a.parent_names,a.group_id,a.station_id,a.level,a.level_group,a.last_login_datetime, i.*, m.money ");
		sql_sb.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_sb.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE flag_active >=1");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			paramMap.put("account", accountVo.getAccount() + "%");
			sql_sb.append(" AND a.account LIKE :account");
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			paramMap.put("parents", accountVo.getParents() + "%");
			if (accountVo.getSearchSelf() != null && accountVo.getSearchSelf()) {
				paramMap.put("curUserId", accountVo.getSelfId());
				sql_sb.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_sb.append(" AND a.parents LIKE :parents");
			}
		}

		if (Validator.isNotNull(accountVo.getStatus())) {
			paramMap.put("status", accountVo.getStatus());
			sql_sb.append(" AND a.account_status=:status");
		}
		if (Validator.isNotNull(accountVo.getAccountType())) {
			paramMap.put("accountType", accountVo.getAccountType());
			sql_sb.append(" AND a.account_type=:accountType");
		}

		if (Validator.isNotNull(accountVo.getStationId())) {
			paramMap.put("stationId", accountVo.getStationId());
			sql_sb.append(" AND a.station_id=:stationId");
		}

		if (Validator.isNotNull(accountVo.getAgentId())) {
			paramMap.put("agentId", accountVo.getAgentId());
			sql_sb.append(" AND a.id=:agentId");
		}

		if (Validator.isNotNull(accountVo.getAgentParentId())) {
			paramMap.put("agentParentId", accountVo.getAgentParentId());
			sql_sb.append(" AND a.agent_id=:agentParentId");
		}
		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			paramMap.put("agentName", accountVo.getAgentName());
			sql_sb.append(" AND a.agent_name=:agentName");
		}

		if (Validator.isNotNull(accountVo.getLevel())) {
			paramMap.put("level", accountVo.getLevel());
			sql_sb.append(" AND a.level >:level");
		}
		sql_sb.append(" ORDER BY a.create_datetime DESC");
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public SysAccount findOneByAccountAndStationId(String account, Long stationId) {
		return query21Model("select * from sys_account where station_id=:stationId and lower(account) = :account",
				MixUtil.newHashMap("account", StringUtil.isEmpty(account) ? "" : account.toLowerCase(), "stationId",
						stationId));
	}

	public SysAccount findOneByAccountIdAndStationId(Long id, Long stationId) {
		return query21Model("select * from sys_account where station_id=:stationId and id=:id",
				MixUtil.newHashMap("id", id, "stationId", stationId));
	}

	/**
	 * 通过日期获取站点代理数量，会员数量，该日期注册的数量
	 * 
	 * @param dateCount
	 * @return
	 */
	public Map<Long, Map> countStation(String dateCount, Date endTime) {
		StringBuilder sql_sb = new StringBuilder();
		Map paramMap = MixUtil.newHashMap();
		sql_sb.append("SELECT station_id,sum(CASE WHEN account_type=4 THEN 1 ELSE 0 END) as agent_count,");
		sql_sb.append("sum(CASE WHEN account_type=1 THEN 1 ELSE 0 END) as member_count,");
		sql_sb.append(
				"sum(CASE WHEN TO_CHAR(create_datetime,'YYYY-MM-DD')=:dateCount THEN 1 ELSE 0 END) as register_count");
		sql_sb.append(" FROM sys_account  WHERE flag_active >= 1");
		sql_sb.append(" AND (account_type =:agentType or account_type=:memberType)");
		if (endTime != null) {
			sql_sb.append(" AND create_datetime<:endTime");
			paramMap.put("endTime", endTime);
		}
		sql_sb.append(" group by station_id");
		paramMap.put("dateCount", dateCount);
		paramMap.put("agentType", SysAccount.ACCOUNT_PLATFORM_AGENT);
		paramMap.put("memberType", SysAccount.ACCOUNT_PLATFORM_MEMBER);

		List list = selectCamelListMap(sql_sb.toString(), paramMap);
		Map<Long, Map> map = new HashMap<>();
		if (list != null && !list.isEmpty()) {
			Map m = null;
			Long stationId = null;
			for (int i = 0, len = list.size(); i < len; i++) {
				m = (Map) list.get(i);
				if (m.get("stationId") != null) {
					try {
						stationId = new Long((Integer) m.get("stationId"));
					} catch (Exception e) {
						stationId = (Long) m.get("stationId");
					}
					map.put(stationId, m);
				}
			}
		}
		return map;
	}

	public List<Map> getExportDate(AccountVo accountVo, Boolean exportAll) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.account,a.account_type,i.user_name,i.card_no ");
		sql_sb.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_sb.append(" WHERE flag_active >=1 ");
		if (exportAll == null || !exportAll) {
			sql_sb.append(" AND i.card_no IS NOT NULL AND i.user_name IS NOT NULL");
			sql_sb.append(" AND i.card_no !='' AND i.user_name != ''");
		}
		sql_sb.append(" AND a.account_type IN (");
		sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER).append(",").append(SysAccount.ACCOUNT_PLATFORM_AGENT);
		sql_sb.append(")");

		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(accountVo.getStatus())) {
			paramMap.put("status", accountVo.getStatus());
			sql_sb.append(" AND a.account_status=:status");
		}

		if (Validator.isNotNull(accountVo.getStationId())) {
			paramMap.put("stationId", accountVo.getStationId());
			sql_sb.append(" AND a.station_id=:stationId");
		}

		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			paramMap.put("account", accountVo.getAccount().toLowerCase());
			sql_sb.append(" AND lower(a.account) = :account");
		}

		if (StringUtil.isNotEmpty(accountVo.getBegin())) {
			paramMap.put("begin", accountVo.getBegin());
			sql_sb.append(" AND a.create_datetime >=:begin");
		}

		if (StringUtil.isNotEmpty(accountVo.getEnd())) {
			paramMap.put("end", accountVo.getEnd());
			sql_sb.append(" AND a.create_datetime <:end");
		}
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	/**
	 * 签到
	 * 
	 * @param memberId
	 *            会员
	 * @param date
	 *            签到日期
	 * @return 连续签到次数 -1：：表示已经签到，导致未更新到数据
	 */
	public int sign(long memberId, Date date) {
		long time = date.getTime();
		Map paramMap = MixUtil.newHashMap("date", new java.sql.Date(time), "memberId", memberId, "preDate",
				new java.sql.Date(time - 24 * 60 * 60 * 1000));
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE sys_account ");
		sql.append(" SET last_sign_date = :date, ");
		sql.append(" sign_count =  ");
		// sql.append(" CASE WHEN sign_count is null or last_sign_date is null
		// ");
		// sql.append(" THEN 1 ");
		sql.append(" CASE WHEN last_sign_date = :preDate ");
		sql.append(" THEN sign_count + 1 ");
		sql.append(" ELSE 1 END ");
		sql.append("WHERE id = :memberId ");
		sql.append("AND (last_sign_date < :date or last_sign_date is null) ");
		sql.append(" RETURNING sign_count ");
		Map map = super.selectSingleMap(sql.toString(), paramMap);
		if (map == null) {
			return -1;
		}
		return StringUtil.toInt(map.get("sign_count"));
	}

	/**
	 * 
	 */
	public void updOfflineAll(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account AS a");
		sql_sb.append(" SET online = ").append(SysAccount.ONLINE_FLAG_OFF);
		sql_sb.append(" WHERE a.station_id = :stationId AND a.online = ").append(SysAccount.ONLINE_FLAG_ON);
		super.update(sql_sb.toString(), MixUtil.newHashMap("stationId", stationId));
	}

	public void updAccountByVo(AccountVo avo) {
		StringBuilder sql_tab = new StringBuilder();
		StringBuilder sql_set = new StringBuilder();
		StringBuilder sql_where = new StringBuilder();
		Map paramMap = MixUtil.newHashMap();
		sql_tab.append("UPDATE sys_account AS a");
		sql_set.append(" SET ");
		sql_where.append(" WHERE a.id=:accountId");
		paramMap.put("accountId", avo.getId());
		if (StringUtil.isNotEmpty(avo.getLastLoginIp())) {
			sql_set.append(" last_login_ip = :lastLoginIp").append(",");
			paramMap.put("lastLoginIp", avo.getLastLoginIp());
		}

		if (StringUtil.isNotEmpty(avo.getLastLoginDatetime())) {
			sql_set.append(" last_login_datetime = :lastLoginDatetime").append(",");
			paramMap.put("lastLoginDatetime", avo.getLastLoginDatetime());
		}

		if (StringUtil.isNotEmpty(avo.getOnline())) {
			sql_set.append(" online = :online").append(",");
			paramMap.put("online", avo.getOnline());
		}

		super.update(sql_tab.append(sql_set.substring(0, sql_set.length() - 1)).append(sql_where).toString(), paramMap);
	}

	public List getAgentUsers(long stationId) {
		String sql = "select a.account,a.id,b.cs,a.account_status from sys_account a "
				+ " left join ( select count(1) cs ,user_id from sec_card_user group by user_id ) b"
				+ " on (a.id = b.user_id)  " + " where a.station_id = :stationId " + " and a.flag_active >= 1 "
				+ " and ( a.account_type =  " + SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER + " OR a.account_type = "
				+ SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER + " ) ";
		return super.selectCamelListMap(sql, MixUtil.newHashMap("stationId", stationId));
	}

	public void updAgentParents(Long stationId, String agentParents, String generalParents, String generalParentsName) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account SET");
		sql_sb.append(" parents = :generalParents||parents,parent_names = :generalParentsName||parent_names");
		sql_sb.append(" WHERE station_id = :stationId AND parents LIKE :agentParents");
		super.update(sql_sb.toString(), MixUtil.newHashMap("generalParents", generalParents, "generalParentsName",
				generalParentsName, "stationId", stationId, "agentParents", agentParents + "%"));
		RedisAPI.delCacheByPrefix("sysAccount_", RedisAPI.DEFAULT_DB_INDEX);
	}

	public void delAgentParents(Long stationId, String agentParents, String generalParents, String generalParentsName) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account SET");
		sql_sb.append(" parents=:generalParents,parent_names=:generalParentsName");
		sql_sb.append(" WHERE station_id = :stationId AND parents LIKE :agentParents");
		super.update(sql_sb.toString(), MixUtil.newHashMap("generalParents", generalParents, "generalParentsName",
				generalParentsName, "stationId", stationId, "agentParents", agentParents + "%"));
		RedisAPI.delCacheByPrefix("sysAccount_", RedisAPI.DEFAULT_DB_INDEX);
	}

	public boolean updateLevelGroup(long accountId, long levelId) {
		return super.update("UPDATE sys_account SET level_group = " + levelId + " WHERE id =" + accountId) > 0;
	}

	public int levelChange(Long curId, Long nextId, Long stationId) {
		return queryForInt("select change_member_level_group(" + curId + "," + nextId + "," + stationId + ")");
	}

	public Map queryAccount(String account, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(
				"SELECT a.id, a.create_datetime, a.account_status, a.account_type,a.register_ip,a.register_url,a.register_os,a.report_type");
		sql_sb.append(
				",a.agent_id,a.agent_name,a.parent_names,a.parents,a.group_id,a.station_id,a.last_login_datetime");
		sql_sb.append(",a.level,a.last_login_ip,a.score,a.online,a.level_group, i.*, m.money");
		sql_sb.append(" FROM sys_account a LEFT JOIN sys_account_info i ON a.id = i.account_id ");
		sql_sb.append(" LEFT JOIN mny_money m ON a.id = m.account_id");
		sql_sb.append(" WHERE a.flag_active >=1");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(a.account) = :account");
		}

		if (Validator.isNotNull(stationId)) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND a.station_id=:stationId");
		}

		return super.selectSingleCamelMap(sql_sb.toString(), paramMap);
	}

	@Override
	public int update(SysAccount t) {
		if (t.getId() != null) {
			RedisAPI.delCache("sysAccount_" + t.getId());
			RedisAPI.lrem("sysAccount_", "sysAccount_" + t.getId());
		}
		return super.update(t);
	}

	@Override
	public SysAccount save(SysAccount model) {
		if (model.getId() != null) {
			RedisAPI.delCache("sysAccount_" + model.getId());
			RedisAPI.lrem("sysAccount_", "sysAccount_" + model.getId());
		}
		return super.save(model);
	}

	/**
	 * 只返回部分属性值，常变得属性没有返回
	 * 
	 * @param id
	 * @return
	 */
	public SysAccount getFromCache(Long id) {
		if (id == null || id == 0)
			return null;
		String json = RedisAPI.getCache("sysAccount_" + id);
		SysAccount a = null;
		if (StringUtils.isNotEmpty(json)) {
			a = JSON.parseObject(json, SysAccount.class);
		}
		if (a != null) {
			return a;
		}
		// 此处不要乱添加返回得属性，否则可能出现缓存问题
		String sql = "SELECT id,account,password,account_type,report_type,create_datetime,flag_active,agent_id,agent_name,station_id,parent_names,parents,group_id,level from sys_account where id=:id";
		a = query21Model(sql, MixUtil.newHashMap("id", id));
		if (a != null) {
			RedisAPI.addCache("sysAccount_" + id, JSON.toJSONString(a), 0);
			RedisAPI.rpush("sysAccount_", "sysAccount_" + id);
		}
		return a;
	}

	public Page<Map> getRiskPage(AccountVo accountVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_account");
		sql_sb.append(" WHERE flag_active = 1 AND account_type IN (1,4) ");

		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(accountVo.getStationId())) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", accountVo.getStationId());
		}

		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			sql_sb.append(" AND lower(account) = :account");
			paramMap.put("account", accountVo.getAccount().toLowerCase());
		}

		if (StringUtil.isNotEmpty(accountVo.getBegin())) {
			paramMap.put("begin", accountVo.getBegin());
			sql_sb.append(" AND create_datetime >=:begin");
		}
		if (StringUtil.isNotEmpty(accountVo.getEnd())) {
			paramMap.put("end", accountVo.getEnd());
			sql_sb.append(" AND create_datetime <:end");
		}
		if (Validator.isNotNull(accountVo.getReportType())) {
			paramMap.put("reportType", accountVo.getReportType());
			sql_sb.append(" AND report_type =:reportType");
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			paramMap.put("parents", accountVo.getParents() + "%");
			if (accountVo.getSearchSelf() != null && accountVo.getSearchSelf()) {
				paramMap.put("curUserId", accountVo.getSelfId());
				sql_sb.append(" AND (parents LIKE :parents OR id = :curUserId)");
			} else {
				sql_sb.append(" AND parents LIKE :parents");
			}
		}

		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public Page<Map> getConversionRiskPage(AccountVo accountVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(
				"SELECT *,(a.app_register_num*1.2)::int4+5 as app_num FROM (SELECT count(1) AS register_num,to_char(create_datetime,'yyyy-mm-dd') as stat_date,");
		sql_sb.append(
				" sum(CASE WHEN register_url is not null AND register_url =:registerUrl THEN 1 ELSE 0 END) AS app_register_num");
		sql_sb.append(" FROM sys_account");
		sql_sb.append(" WHERE flag_active = 1 AND account_type =1 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();

		paramMap.put("registerUrl", accountVo.getRegisterUrl());

		if (Validator.isNotNull(accountVo.getStationId())) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", accountVo.getStationId());
		}

		if (StringUtil.isNotEmpty(accountVo.getAccount())) {
			sql_sb.append(" AND lower(account) = :account");
			paramMap.put("account", accountVo.getAccount().toLowerCase());
		}

		if (StringUtil.isNotEmpty(accountVo.getBegin())) {
			paramMap.put("begin", accountVo.getBegin());
			sql_sb.append(" AND create_datetime >=:begin");
		}
		if (StringUtil.isNotEmpty(accountVo.getEnd())) {
			paramMap.put("end", accountVo.getEnd());
			sql_sb.append(" AND create_datetime <:end");
		}

		if (StringUtil.isNotEmpty(accountVo.getParents())) {
			paramMap.put("parents", accountVo.getParents() + "%");
			if (accountVo.getSearchSelf() != null && accountVo.getSearchSelf()) {
				paramMap.put("curUserId", accountVo.getSelfId());
				sql_sb.append(" AND (parents LIKE :parents OR id = :curUserId)");
			} else {
				sql_sb.append(" AND parents LIKE :parents");
			}
		}

		sql_sb.append(" GROUP BY to_char(create_datetime,'yyyy-mm-dd')) a");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.register_num", "registerNumTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "(a.app_register_num*1.2)::int4+5", "appNumTotal"));

		return super.page2CamelMap(sql_sb.toString(), paramMap, aggs);
	}

	public void delGeneralAgent(Long accountId) {
		if (accountId == null)
			return;
		update("update sys_account set agent_id=0,agent_name=null,parents='',parent_names=null where id=:id",
				MixUtil.newHashMap("id", accountId));
		RedisAPI.delCache("sysAccount_" + accountId);
		RedisAPI.lrem("sysAccount_", "sysAccount_" + accountId);
	}

	public void updateGroup(Long id, Long stationId, Long groupId,String remark) {
		update("update sys_account set group_id=:groupId,remark=:remark where id=:id and station_id=:stationId",
				MixUtil.newHashMap("id", id, "stationId", stationId, "groupId", groupId,"remark",remark));
	}

	public void batchUpdateAccountLoginInfo(final List<OnlineInfo> list) {
		if (list == null || list.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET");
		sql.append(" last_login_ip =?,last_login_datetime =?,last_login_device =?");
		sql.append(" WHERE id=?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				OnlineInfo o = list.get(i);
				int k = 1;
				ps.setString(k++, o.getIp());
				ps.setTimestamp(k++, new java.sql.Timestamp(o.getTime().getTime()));
				ps.setString(k++, o.getDeviceName());
				ps.setLong(k++, o.getId());
			}

			@Override
			public int getBatchSize() {
				return list.size();
			}
		});
	}

	public void clearGuestTestData(Date endTime) {
		Map<?, ?> map = MixUtil.newHashMap("end", endTime);
		update("delete from sys_account_info where account_id in(select id from sys_account where account_type=6 and create_datetime<=:end)",
				map);
		update("delete from mny_money where  account_id in(select id from sys_account where account_type=6 and create_datetime<=:end)",
				map);
		update("delete from sys_account where account_type=6 and create_datetime<=:end", map);
	}

	/**
	 * 获取测试帐号的下一个编号
	 * 
	 * @return
	 */
	public Long getNextTestGuestId() {
		return queryForLong("select nextval('sys_account_test_guest_id_seq')");
	}

	public void getReSetTestGuestId() {
		queryForLong("select setval('sys_account_test_guest_id_seq', 10, true)");
	}

	public boolean existAccount(String userName, Long stationId) {
		Map<?, ?> map = MixUtil.newHashMap("account", userName, "stationId", stationId);
		return queryForLong("select count(*) from sys_account where account=:account and station_id=:stationId",
				map) > 0;
	}

	public void updateOnlineStatus(final List<Long> ids) {
		if (ids == null || ids.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET online=");
		sql.append(SysAccount.ONLINE_FLAG_ON);
		sql.append(" WHERE id=?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, ids.get(i));
			}

			@Override
			public int getBatchSize() {
				return ids.size();
			}
		});
	}

	public void updateUserOffline(Long accountId) {
		if (accountId == null || accountId <= 0l)
			return;
		update("UPDATE sys_account SET online=" + SysAccount.ONLINE_FLAG_OFF + " where id=" + accountId);
	}
	
	public void updateUserOnline(Long accountId) {
		if (accountId == null || accountId <= 0l)
			return;
		update("UPDATE sys_account SET online=" + SysAccount.ONLINE_FLAG_ON + " where id=" + accountId);
	}
	
	public void updateUserLastLoginIp(Long accountId, String lastLoginIp) {
		if (accountId == null || accountId <= 0l)
			return;
		update("UPDATE sys_account SET last_login_ip=" +"'" +lastLoginIp +"'"+" where id=" + accountId);
	}

	public Page<SysAccount> getStationAgentPage(Long stationId, Long stationAccountId,AccountVo accountVo) {
		Map<String, Object> map = new HashMap<>();
		StringBuilder sql = new StringBuilder("select id,account,group_id,account_status,remark,(select t2.account from sys_account t2 where t2.id = t1.create_user_id) as agent_name");
		sql.append(" from sys_account t1 where station_id=:stationId");
		map.put("stationId", stationId);
		if (stationAccountId != null) {
			sql.append(" and ( account_type=:at1 or account_type=:at2)");
			map.put("at1", SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
			map.put("at2", SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			sql.append(" and id!=:stationAccountId");
			map.put("stationAccountId", stationAccountId);
			//条件查询
			if(StringUtil.isNotEmpty(accountVo.getGroupId()) && 0!=accountVo.getGroupId()) {
				sql.append(" AND group_id = :groupId");
				map.put("groupId", accountVo.getGroupId());
			}
			if(StringUtil.isNotEmpty(accountVo.getStatus())) {
				sql.append(" and account_status = :status");
				map.put("status", accountVo.getStatus());
			}
			if(StringUtil.isNotEmpty(accountVo.getAccount())) {
				sql.append(" and account = :account");
				map.put("account", accountVo.getAccount());
			}
		} else {
			sql.append(" and account_type=:at1");
			//条件查询
			if(StringUtil.isNotEmpty(accountVo.getGroupId()) && 0!=accountVo.getGroupId()) {
				sql.append(" AND group_id = :groupId");
				map.put("groupId", accountVo.getGroupId());
			}
			if(StringUtil.isNotEmpty(accountVo.getStatus())) {
				sql.append(" AND account_status = :status");
				map.put("status", accountVo.getStatus());
			}
			if(StringUtil.isNotEmpty(accountVo.getAccount())) {
				sql.append(" AND account = :account");
				map.put("account", accountVo.getAccount());
			}
			map.put("at1", SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
		}
		sql.append(" order by t1.create_datetime desc");
		return paged2Obj(sql.toString(), map);
	}

	public void updateAllOffline() {
		update("UPDATE sys_account SET online=" + SysAccount.ONLINE_FLAG_OFF + " where online="
				+ SysAccount.ONLINE_FLAG_ON);
	}

	public void scoreToZero(Long stationId) {
		update("UPDATE sys_account SET score=0 where account_type=1 and station_id=" + stationId);
	}

	public int[] batchUpdateAgentName(final SysAccount agent, final String[] accounts, final Long accountType,
			final Long stationId) {
		if (accounts == null || accounts.length == 0) {
			return null;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET agent_id=?,agent_name=?,parents = ?,");
		sql.append("parent_names = ?");
		sql.append(" WHERE lower(account)=? and station_id =? and account_type = ?");
		return getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, agent.getId());
				ps.setString(2, agent.getAccount());
				ps.setString(3, UserUtil.getChildren(agent));
				ps.setString(4, UserUtil.getChildrenName(agent));
				ps.setString(5, accounts[i]);
				ps.setLong(6, stationId);
				ps.setLong(7, accountType);
			}

			@Override
			public int getBatchSize() {
				return accounts.length;
			}
		});
	}
	
	public int[] batchUpdateLevelGroup(final Long levelGroup, final String[] accounts, final Long stationId) {
		if (accounts == null || accounts.length == 0) {
			return null;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET level_group=?");
		sql.append(" WHERE lower(account)=? and station_id =?");
		return getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, levelGroup);
				ps.setString(2, accounts[i]);
				ps.setLong(3, stationId);
			}

			@Override
			public int getBatchSize() {
				return accounts.length;
			}
		});
	}
	public int[] batchUpdateChatToken(final String[] accounts, final String token,final Long stationId) {
		if (accounts == null || accounts.length == 0) {
			return null;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET chat_token=?");
		sql.append(" WHERE lower(account)=? and station_id =?");
		return getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setString(1, token);
				ps.setString(2, accounts[i]);
				ps.setLong(3, stationId);
			}
			@Override
			public int getBatchSize() {
				return accounts.length;
			}
		});
	}
	
	public int[] batchUpdateImChatId(final String[] accounts, final String imChatId,final Long stationId) {
		if (accounts == null || accounts.length == 0) {
			return null;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET im_chat_id=?");
		sql.append(" WHERE lower(account)=? and station_id =?");
		return getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setString(1, imChatId);
				ps.setString(2, accounts[i]);
				ps.setLong(3, stationId);
			}
			@Override
			public int getBatchSize() {
				return accounts.length;
			}
		});
	}
	
	public void updateLevelGroupsByParens(Long levelGroup, String parents, Long stationId) {
		update("UPDATE sys_account SET level_group=:levelGroup where station_id=:stationId and parents LIKE :parents",
				MixUtil.newHashMap("levelGroup", levelGroup, "stationId", stationId, "parents", parents));
	}

	public int checkSubAccount(String parAccount, String subAccount, Long stationId) {
		String parentName = "%,"+parAccount+",";
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(1) ");
		sql_sb.append(" FROM sys_account a");
		sql_sb.append(" WHERE a.account = :account AND station_id = :stationId AND parent_names like :parentName");
		return super.queryForInt(sql_sb.toString(),
				MixUtil.newHashMap("account",subAccount, "stationId", stationId,"parentName",parentName));
	}
	
	public int queryRegisterCount(Long stationId, String parents,Date start,Date end) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		String redisKey = "daily_register_count_s_"+stationId+"_p_"+parents+"_s_"+sdf.format(start)+"_e_"+sdf.format(end);
		String count = RedisAPI.getCache(redisKey);
		if(StringUtil.isNotEmpty(count)) {
			return StringUtil.toInt(count);
		}
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(1) ");
		sql_sb.append(" FROM sys_account");
		sql_sb.append(" WHERE station_id = :stationId AND parents LIKE :parents");
		sql_sb.append(" AND create_datetime >= :start AND create_datetime <= :end");
		count = super.queryForInt(sql_sb.toString(),
				MixUtil.newHashMap("stationId",stationId,"parents",parents+"%","start",start,"end",end))+"";
		RedisAPI.addCache(redisKey, count, 300);
		return StringUtil.toInt(count);
	}

	public String getAccountStrByChat(String token, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select string_agg(account||'' , ',') from sys_account where chat_token = :token and station_id = :stationId");
		return super.queryForString(sql_sb.toString(),
				MixUtil.newHashMap("token",token, "stationId", stationId));
	}
	

	public String getAccountStrByImChatId(String imChatId, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select string_agg(account||'' , ',') from sys_account where im_chat_id = :imChatId and station_id = :stationId");
		return super.queryForString(sql_sb.toString(),
				MixUtil.newHashMap("imChatId",imChatId, "stationId", stationId));
	}

	public void clearSignCount(Long userId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account");
		sql_sb.append(" SET sign_count = 0");
		sql_sb.append(" WHERE id = :userId");
		super.update(sql_sb.toString(), MixUtil.newHashMap("userId", userId));
	}
	
	/**
	 * 根据名字和站点id获取上上级代理姓名
	 * @param stationId
	 * @param account
	 * @return
	 */
	public Map getUpUpAgentName(Long stationId, String account) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(" select agent_name from sys_account where account = ");
		sql_sb.append(" (select agent_name from sys_account where account = :account and station_id = :stationId)");
		sql_sb.append(" and station_id = :stationId ");
		
		HashMap<Object,Object> map = new HashMap<>();
		map.put("account", account);
		map.put("stationId", stationId);
		
		Map selectSingleCamelMap = super.selectSingleCamelMap(sql_sb.toString(), map);
		return selectSingleCamelMap;
		
	}
	
	public Page<Map> getBalanceGemPage(AccountVo avo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whs = new StringBuilder();
		StringBuilder sql_odr = new StringBuilder();
		sql_cols.append(" select a.id,a.account,m.balance_gem_money,m.balance_gem_income");
		sql_tabs.append(" from sys_account a INNER JOIN mny_money m ON a.id = m.account_id ");
		sql_whs.append(" where a.station_id = :stationId and m.balance_gem_money is not NULL");
		sql_odr.append("  ORDER BY balance_gem_money desc ");
		Map paramMap = MixUtil.newHashMap();
		paramMap.put("stationId", avo.getStationId());
		if (StringUtil.isNotEmpty(avo.getAccount())) {
			sql_whs.append(" AND lower(account) = :account");
			paramMap.put("account", avo.getAccount().trim().toLowerCase());
		}
		if (Validator.isNotNull(avo.getAccountType())) {
			sql_whs.append(" AND a.account_type = :accountType");
			paramMap.put("accountType", avo.getAccountType());
		}
		if (Validator.isNotNull(avo.getReportType())) {
			sql_whs.append(" AND a.report_type = :reportType");
			paramMap.put("reportType", avo.getReportType());
		}
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "m.balance_gem_money", "bgMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "m.balance_gem_income", "bgIncome"));
		
		return super.page2CamelMap(
				sql_cols.append(sql_tabs.toString()).append(sql_whs.toString()).append(sql_odr.toString()).toString(),
				paramMap,aggs);
	}
		
	/**
	 * 获取指定的所有下级(目前只获取id和姓名)
	 * @param children
	 * @param stationId
	 * @return
	 */
	public List<SysAccount> getAllDownMember(String account,String children, Long stationId){
		StringBuilder sql_sb = new StringBuilder();
		HashMap<Object,Object> map = new HashMap<>();
		
		sql_sb.append("select a.id,a.account from sys_account a");
		if(StringUtil.isNotEmpty(account)) {
			sql_sb.append(" where 1= 1 and (lower(account) = :account");
			map.put("account", account);
		}
		
		if(StringUtil.isNotEmpty(children)) {
			sql_sb.append(" or parents LIKE :children )");
			map.put("children", children+"%");
		}else {
			sql_sb.append(")");
		}
		sql_sb.append(" AND a.account_type<>:accountType");
		map.put("accountType", SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
		sql_sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		
		List<SysAccount> list = super.query2Model(sql_sb.toString(),map);
		return list;
	}

	public void deleteByAccountId(Long id) {
		// TODO Auto-generated method stub
		String sql = "delete from sys_account where id = :accountId";
		super.update(sql, MixUtil.newHashMap("accountId", id));
	}

	public void updateDisableAccount(Long id) {
		update("UPDATE sys_account SET account_status=:status where id=:accountId",
				MixUtil.newHashMap("status", SysAccount.ACCOUNT_STATUS_DISABLED, "accountId", id));
	}
	public void updateDisableOddAccount(Long id) {
		update("UPDATE sys_account SET account_status=:status,login_odd_flag=:flag where id=:accountId",
				MixUtil.newHashMap("status", SysAccount.ACCOUNT_STATUS_DISABLED,"flag", SysAccount.LOGIN_ODD_EXECUTE, "accountId", id));
	}

	
	public void updateAccountType(Long accountType,Long stationId,Long accountId,Date changeDailiTime) {
		StringBuilder sb = new StringBuilder();
		sb.append("update sys_account set account_type = :accountType,change_daili_time =:changeDailiTime where station_id=:stationId and id=:accountId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("accountType",accountType);
		paramMap.put("stationId", stationId);
		paramMap.put("accountId", accountId);
		paramMap.put("changeDailiTime", changeDailiTime);
		super.update(sb.toString(), paramMap);
	}
	
	public void updataRate(BigDecimal rate,Long stationId,Long accountId) {
		StringBuilder sb = new StringBuilder();
		sb.append("update sys_account set rate = :rate where station_id=:stationId and id=:accountId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("rate",rate);
		paramMap.put("stationId", stationId);
		paramMap.put("accountId", accountId);
		super.update(sb.toString(), paramMap);
	}
	
	public String MaptoString(Long accountId,Long stationId,Map accountMap) {
		Object totalDrawMoney = accountMap.get("totalDrawMoney");
		Object countDraw = accountMap.get("countDraw");
		Object maxDrawMoney = accountMap.get("maxDrawMoney");
		Object firstDrawDate = accountMap.get("firstDrawDate");
		Object firstDrawMoney = accountMap.get("firstDrawMoney");
		Object maxDrawDate = accountMap.get("maxDrawDate");
		
		Object totalComMoney = accountMap.get("totalComMoney");
		Object countCom = accountMap.get("countCom");
		Object maxComMoney = accountMap.get("maxComMoney");
		Object firstComDate = accountMap.get("firstComDate");
		Object firstComMoney = accountMap.get("firstComMoney");
		Object maxComDate = accountMap.get("maxComDate");
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("totalDrawMoney", totalDrawMoney!=null?totalDrawMoney:"");
		jsonObject.put("countDraw", countDraw!=null?countDraw:"");
		jsonObject.put("maxDrawMoney", maxDrawMoney!=null?maxDrawMoney:"");
		jsonObject.put("firstDrawDate", firstDrawDate!=null?firstDrawDate:"");
		jsonObject.put("firstDrawMoney", firstDrawMoney!=null?firstDrawMoney:"");
		jsonObject.put("maxDrawDate", maxDrawDate!=null?maxDrawDate:"");
		jsonObject.put("totalComMoney", totalComMoney!=null?totalComMoney:"");
		jsonObject.put("countCom", countCom!=null?countCom:"");
		jsonObject.put("maxComMoney", maxComMoney!=null?maxComMoney:"");
		jsonObject.put("firstComDate", firstComDate!=null?firstComDate:"");
		jsonObject.put("firstComMoney", firstComMoney!=null?firstComMoney:"");
		jsonObject.put("maxComDate", maxComDate!=null?maxComDate:"");
		
		return jsonObject.toString();
	}
	
	public String getCacheKey(Long accountId,Long stationId) {
		StringBuilder sb = new StringBuilder();
		sb.append("_");
		sb.append("_account");
		sb.append("_draw");
		sb.append("_com");
		sb.append(accountId);
		sb.append("_");
		sb.append(stationId);
		sb.append("_a");
		sb.append("_b");
		sb.append("_c");
		
		return sb.toString();
	}

	public void updateAbnormalFlag(SysAccount acc) {
		StringBuilder sb = new StringBuilder();
		sb.append("update sys_account set abnormal_flag = :abnormalFlag where  id=:accountId");
		Map<Object, Object> paramMap = new HashMap<>();
		paramMap.put("abnormalFlag", acc.getAbnormalFlag());
		paramMap.put("accountId", acc.getId());
		super.update(sb.toString(), paramMap);
	}
	
	public Map getDrawCom(Long stationId,Long memberId, Date date) {
		StringBuilder builder = new StringBuilder();
		builder.append(" select account_idd,first_draw_money,first_draw_date,max_draw_money,max_draw_date,count_draw,total_draw_money,first_com_money,first_com_date,max_com_money,max_com_date,count_com,total_com_money,bb.* from ");
		builder.append(" (");
		builder.append(" (select id from sys_account where id =:memberId ) s");
		builder.append(" left join ");
		builder.append(" (select member_id,draw_money as first_draw_money,create_datetime as first_draw_date from mny_draw_record  where station_id = :stationId and member_id = :memberId and status = 2 ORDER BY create_datetime LIMIT 1) a");
		builder.append(" on s.id = a.member_id");
		builder.append(" left join ");
		builder.append(" (select member_id,draw_money as max_draw_money,create_datetime as max_draw_date from mny_draw_record  where station_id = :stationId and member_id = :memberId and status = 2 ORDER BY draw_money desc LIMIT 1) b");
		builder.append(" on s.id = b.member_id");
		builder.append(" left join ");
		builder.append(" (select account_id,sum(withdraw_times) as count_draw,sum(-withdraw_amount) as total_draw_money from sys_account_daily_money where station_id= :stationId and account_id = :memberId GROUP BY account_id) c");
		builder.append(" on s.id = c.account_id");
		builder.append(" left join ");
		builder.append(" (select member_id,money as first_com_money,create_datetime as first_com_date from mny_com_record  where station_id = :stationId and member_id = :memberId and status = 2 ORDER BY create_datetime LIMIT 1) d");
		builder.append(" on s.id = d.member_id");
		builder.append(" left join ");
		builder.append(" (select member_id,money as max_com_money,create_datetime as max_com_date from mny_com_record  where station_id = :stationId and member_id = :memberId and status = 2 ORDER BY money desc LIMIT 1) e");
		builder.append(" on s.id = e.member_id");
		builder.append(" left join ");
		builder.append(" (select account_id as account_idd,sum(deposit_amount+deposit_artificial) as total_com_money from sys_account_daily_money where station_id= :stationId and account_id = :memberId GROUP BY account_id) f");
		builder.append(" on s.id = f.account_idd");
		builder.append(" left join");
		builder.append(" (select count(*) as count_com,member_id from mny_com_record where station_id =:stationId and member_id =:memberId and status = 2 GROUP BY member_id) g");
		builder.append(" on s.id= g.member_id");
		builder.append(" ) aa");
		
		builder.append(" left join");
		builder.append(" (select account_id,sum(deposit_amount+deposit_artificial) as today_com_money,sum(deposit_times+deposit_handler_times) as today_com_times,sum(-withdraw_amount) as today_draw_money, sum(withdraw_times) as today_draw_times from sys_account_daily_money");
		builder.append(" where account_id =:memberId and station_id =:stationId and stat_date =:statDate GROUP BY account_id");
		builder.append(" ) bb");
		builder.append(" on aa.account_idd = bb.account_id");
		
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("stationId", stationId);
		paramMap.put("memberId", memberId);
		paramMap.put("statDate", date);
		
		return super.selectSingleCamelMap(builder.toString(), paramMap);
	}
	
	/**
	 * 通过账号和密码获取用户 用于登陆验证
	 * 
	 * @param account
	 * @param pwd
	 * @return
	 */
	public SysAccount getLoginAccount(String account, Long type,Long stationId) {
		account = account.trim();
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.*");
		sql_sb.append(" FROM sys_account a");
		sql_sb.append(" WHERE flag_active >= 1 AND a.station_id = :stationId");
		sql_sb.append(" AND lower(account) = :account");
		Map paramMap = MixUtil.newHashMap("stationId", stationId, "account",
				StringUtil.isEmpty(account) ? "" : account.toLowerCase());
		if (type == SysAccount.ACCOUNT_PLATFORM_AGENT) {
			sql_sb.append(" AND account_type IN (");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT);
			sql_sb.append(",");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL);
			sql_sb.append(")");
		} else if (type == SysAccount.ACCOUNT_PLATFORM_MEMBER) {
			sql_sb.append(" AND account_type IN (");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_MEMBER);
			sql_sb.append(",");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
			sql_sb.append(",");
			sql_sb.append(SysAccount.ACCOUNT_PLATFORM_GUIDE);
			sql_sb.append(")");
		} else {
			sql_sb.append(" AND account_type = :type");
			paramMap.put("type", type);
		}
		return super.query21Model(sql_sb.toString(), paramMap);
	}

	public boolean updateImUserId(long accountId, String imUserId) {
		return super.update("UPDATE sys_account SET im_user_id = " + imUserId + " WHERE id =" + accountId) > 0;
	}

	public boolean warningRevise(Integer flag, Long id) {
		return super.update("UPDATE sys_account SET abnormal_flag = " + flag + " WHERE id =" + id) > 0;
	}

	public Integer getWarningCount(long stationId) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		String redisKey = "warning_count:::"+stationId;
		String count = RedisAPI.getCache(redisKey,db);
		if(StringUtil.isNotEmpty(count)) {
			return StringUtil.toInt(count);
		}
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(1) ");
		sql_sb.append(" FROM sys_account");
		sql_sb.append(" WHERE station_id = :stationId AND abnormal_flag =1");
		count = super.queryForInt(sql_sb.toString(),
				MixUtil.newHashMap("stationId",stationId))+"";
		RedisAPI.addCache(redisKey, count, 600,db);
		return StringUtil.toInt(count);
	}
	
	public List<Long> queryAccountIdsByType(Long stationId,Long... type){
		
		List<Long> ids = null;
		
		StringBuilder sb = new StringBuilder();
		HashMap<String,Object> map = new HashMap<String,Object>();
		
		sb.append("select id from sys_account where 1=1");
		
		if(Validator.isNotNull(stationId)) {
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(StringUtil.isNotEmpty(type)) {
			sb.append(" and account_type in( ");
			
			for (Long t : type) {
				sb.append(t).append(",");;
			}
			sb = new StringBuilder(sb.substring(0, sb.length()-1));
			sb.append(" )");
		}
		
		ids = super.query2Obj(sb.toString(), map, new LongRowMapper());
		return ids;
	}
	
	public void saveRemark(Long accountId, String remark) {
		StringBuilder sql = new StringBuilder();
		sql.append("update sys_account set remark =:remark where id =:accountId ");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("remark", remark);
		map.put("accountId", accountId);
		
		super.update(sql.toString(), map);
		
	}
	
	private String replaceUpperToUnderline(String str) {
		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile("[A-Z]+");
		Matcher m1 = p.matcher(str); // 判断是否含有大写字符
		while (m1.find()) {
			m1.appendReplacement(sb, "_" + m1.group().toLowerCase());
		}
		m1.appendTail(sb);
		return sb.toString();
	}

	public Integer getRegisterCount(ReportParamVo paramVo) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("SELECT count(id) from sys_account a where a.station_id = :stationId ");
		builder.append(" and to_char(a.create_datetime, 'yyyy-mm-dd')=:date ");
		builder.append(" and account_type in (");
		builder.append(SysAccount.ACCOUNT_PLATFORM_MEMBER).append(",");
		builder.append(SysAccount.ACCOUNT_PLATFORM_AGENT).append(")");
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("date", paramVo.getSeachDate());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			builder.append(" AND a.account =:account");
		}
		if (reportType!=null && reportType!=0) {
			paramMap.put("reportType", reportType);
			builder.append(" AND a.report_type =:reportType");
		}
		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				builder.append("  AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				builder.append("  AND a.parents LIKE :children");
			}
		}
		return super.queryForInt(builder.toString(),paramMap);
	}
}
