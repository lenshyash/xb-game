package com.game.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.support.rowset.ResultSetWrappingSqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.DaemonThreadFactory;
import com.game.constant.StationConfig;
import com.game.model.MnyComRecord;
import com.game.model.MnyDrawRecord;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.model.vo.ReportParamVo;
import com.game.util.StationConfigUtil;
import com.game.util.UserUtil;

@Repository
public class MnyDrawRecordDao extends JdbcUtilImpl<MnyDrawRecord> {

	public Page<Map> getPage(MnyDrawRecordVo mdrvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT r.*,i.bank_address,a.report_type,m.account as modify_user,a.create_datetime AS at_create_datetime,a.level_group,a.remark AS memberRemark,a.account_type as accountType,a.agent_name As agentName,a.abnormal_flag,r.draw_times");
		sql_sb.append(" FROM mny_draw_record r INNER JOIN sys_account a ON r.member_id = a.id");
		sql_sb.append(" LEFT JOIN sys_account m ON m.id = r.modify_user_id");
		sql_sb.append(" LEFT JOIN sys_account_info i ON i.account_id = r.member_id");
		sql_sb.append(" WHERE a.station_id = :stationId");
		BigDecimal minMoney = mdrvo.getMinMoney();
		BigDecimal maxMoney = mdrvo.getMaxMoney();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", mdrvo.getStationId());
		String account = StringUtil.trim2Empty(mdrvo.getAccount());
		if (Validator.isNotNull(mdrvo.getStatus())) {
			if("110".equals(mdrvo.getStatus()+"")) {
				sql_sb.append(" AND status = :status");
				paramMap.put("status", MnyDrawRecord.STATUS_SUCCESS);
				
				sql_sb.append(" and pay_id  is not null");
			}else if("111".equals(mdrvo.getStatus()+"")){
				sql_sb.append(" AND status = :status");
				paramMap.put("status", MnyDrawRecord.STATUS_SUCCESS);
				
				sql_sb.append(" and pay_id  is null");
			}else {
				sql_sb.append(" AND status = :status");
				paramMap.put("status", mdrvo.getStatus());
			}

		}

		if (Validator.isNotNull(mdrvo.getLockFlag())) {
			sql_sb.append(" AND r.lock_flag = :lock");
			paramMap.put("lock", mdrvo.getLockFlag());
		}
		
		if (Validator.isNotNull(mdrvo.getReportType())) {
			sql_sb.append(" AND a.report_type = :reportType");
			paramMap.put("reportType", mdrvo.getReportType());
		}
		if (Validator.isNotNull(mdrvo.getPayId())) {
			sql_sb.append(" AND r.pay_id = :payId");
			paramMap.put("payId", mdrvo.getPayId());
		}
		if (minMoney != null && minMoney.compareTo(BigDecimal.ZERO) == 1) {
			sql_sb.append(" AND r.draw_money >= :minMoney");
			paramMap.put("minMoney", minMoney);
		}
		
		if (maxMoney != null && maxMoney.compareTo(BigDecimal.ZERO) == 1) {
			sql_sb.append(" AND r.draw_money <= :maxMoney");
			paramMap.put("maxMoney", maxMoney);
		}

		if (StringUtils.isNotEmpty(mdrvo.getOperator())) {
			sql_sb.append(" AND m.account = :operator");
			paramMap.put("operator", mdrvo.getOperator());
		}
		
		if (StringUtil.isNotEmpty(account)) {
			sql_sb.append(" AND lower(a.account) = :account");
			paramMap.put("account", account.toLowerCase());
		}

		if (Validator.isNotNull(mdrvo.getAccountType())) {
			sql_sb.append(" AND a.account_type = :accountType");
			paramMap.put("accountType", mdrvo.getAccountType());
		}

		if (mdrvo.getAccountId() != null) {
			sql_sb.append(" AND a.id =:accountId");
			paramMap.put("accountId", mdrvo.getAccountId());
		}

		if (Validator.isNotNull(mdrvo.getBegin())) {
			sql_sb.append(" AND r.create_datetime >= :begin");
			paramMap.put("begin", mdrvo.getBegin());
		}

		if (Validator.isNotNull(mdrvo.getEnd())) {
			sql_sb.append(" AND r.create_datetime <= :end");
			paramMap.put("end", mdrvo.getEnd());
		}
		
		if (Validator.isNotNull(mdrvo.getRemark())) {
			sql_sb.append(" AND r.remark like :remark");
			paramMap.put("remark", "%"+mdrvo.getRemark()+"%");
		}
		if (Validator.isNotNull(mdrvo.getDrawTimes())) {
			sql_sb.append(" AND r.draw_times =:drawTimes");
			paramMap.put("drawTimes", mdrvo.getDrawTimes());
		}
		
		if (StringUtil.isNotEmpty(mdrvo.getParents())) {
			if (mdrvo.getSearchSelf() != null && mdrvo.getSearchSelf()) {
				paramMap.put("curUserId", mdrvo.getSelfId());
				sql_sb.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_sb.append(" AND a.parents LIKE :parents");
			}
			if (StringUtil.equals(mdrvo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
				paramMap.put("parents", mdrvo.getParents());
			}else {
				paramMap.put("parents", mdrvo.getParents()+ "%");
			}
		}

		sql_sb.append(" ORDER BY r.create_datetime DESC");
		
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"CASE WHEN r.status is not null THEN r.draw_money ELSE 0 END", "drawMoney"));
		return super.page2CamelMap(sql_sb.toString(), paramMap, aggs);
	}

	/**
	 * 会员提款记录导出功能用
	 * 
	 * @param mdrvo
	 * @return
	 */
	public List<Map> export(MnyDrawRecordVo mdrvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT r.*,a.agent_name,m.account as modify_user");
		sql_sb.append(" FROM mny_draw_record r INNER JOIN sys_account a ON r.member_id = a.id");
		sql_sb.append(" LEFT JOIN sys_account m ON m.id = r.modify_user_id");
		sql_sb.append(" WHERE a.station_id = :stationId");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", mdrvo.getStationId());

		if (Validator.isNotNull(mdrvo.getStatus())) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", mdrvo.getStatus());
		}
		if (StringUtil.isNotEmpty(mdrvo.getAccount())) {
			sql_sb.append(" AND lower(a.account) = :account");
			paramMap.put("account", mdrvo.getAccount().toLowerCase());
		}

		if (Validator.isNotNull(mdrvo.getAccountType())) {
			sql_sb.append(" AND a.account_type = :accountType");
			paramMap.put("accountType", mdrvo.getAccountType());
		}

		if (mdrvo.getAccountId() != null) {
			sql_sb.append(" AND a.id =:accountId");
			paramMap.put("accountId", mdrvo.getAccountId());
		}

		if (Validator.isNotNull(mdrvo.getBegin())) {
			sql_sb.append(" AND r.create_datetime >= :begin");
			paramMap.put("begin", mdrvo.getBegin());
		}

		if (Validator.isNotNull(mdrvo.getEnd())) {
			sql_sb.append(" AND r.create_datetime < :end");
			paramMap.put("end", mdrvo.getEnd());
		}

		if (StringUtil.isNotEmpty(mdrvo.getParents())) {
			if (mdrvo.getSearchSelf() != null && mdrvo.getSearchSelf()) {
				paramMap.put("curUserId", UserUtil.getUserId());
				sql_sb.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
			} else {
				sql_sb.append(" AND a.parents LIKE :parents");
			}
			paramMap.put("parents", mdrvo.getParents() + "%");
		}

		sql_sb.append(" ORDER BY r.create_datetime DESC");

		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public Integer getCountOfUntreated(MnyDrawRecordVo mdrvo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(1)");
		sql_sb.append(" FROM mny_draw_record r ");
		sql_sb.append(" WHERE 1=1 ");

		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(mdrvo.getStationId())) {
			paramMap.put("stationId", mdrvo.getStationId());
			sql_sb.append(" AND station_id=:stationId");
		}

		if (Validator.isNotNull(mdrvo.getStatus())) {
			paramMap.put("status", mdrvo.getStatus());
			sql_sb.append(" AND r.status =:status");
		}

		if (Validator.isNotNull(mdrvo.getLockFlag())) {
			paramMap.put("lock", mdrvo.getLockFlag());
			sql_sb.append(" AND r.lock_flag =:lock");
		}

		if (StringUtil.isNotEmpty(mdrvo.getBegin())) {
			paramMap.put("begin", mdrvo.getBegin());
			sql_sb.append(" AND r.create_datetime >:begin");
		}

		if (StringUtil.isNotEmpty(mdrvo.getEnd())) {
			paramMap.put("end", mdrvo.getEnd());
			sql_sb.append(" AND r.create_datetime <:end");
		}

		return super.queryForInt(sql_sb.toString(), paramMap);
	}

	/**
	 * 根据站点和投注时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(
				"delete from mny_draw_record where station_id = :stationId and create_datetime <= :createTime", map);
	}

	/**
	 * 计算给定日期内的用户提款次数，提款状态必须是未处理或处理成功才纳入计算
	 * 
	 * @param accountId
	 * @param stationId
	 * @param start
	 * @param end
	 * @return
	 */
	public int countMemDrawNum(Long accountId, Long stationId, Date start, Date end) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(*) FROM mny_draw_record ");
		sql_sb.append(" WHERE station_id = :stationId and member_id=:accountId");
		sql_sb.append(" AND create_datetime>=:start and create_datetime<:end");

		Map<String, Object> map = new HashMap<>();
		map.put("accountId", accountId);
		map.put("stationId", stationId);
		map.put("start", start);
		map.put("end", end);

		if ("on".equals(StationConfigUtil.get(StationConfig.withdraw_failed_time_onoff))) {
			map.put("status2", MnyDrawRecord.STATUS_SUCCESS);
			sql_sb.append(" AND status=:status2");
		}

		return queryForInt(sql_sb.toString(), map);
	}

	public Long getDrawTotalMoney(Long accountId, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("accountId", accountId);
		map.put("status", MnyDrawRecord.STATUS_SUCCESS);
		return queryForLong(
				"select (case when sum(draw_money) is not null then sum(draw_money) else 0 end) from mny_draw_record where station_id = :stationId and member_id=:accountId and status =:status",
				map);
	}

	public List<MnyDrawRecord> getUntreatedReocrd(Long stationId, Date expireTime) {
		return super.query2Model(
				"SELECT * FROM mny_draw_record WHERE status = " + MnyDrawRecord.STATUS_UNTREATED + " AND lock_flag = "
						+ MnyDrawRecord.LOCK_FLAG_UNLOCKED
						+ " AND station_id = :stationId AND create_datetime <= :expireTime;",
				MixUtil.newHashMap("stationId", stationId, "expireTime", expireTime));
	}

	public Date getLastSuccessTime(Long userId) {

		ResultSetWrappingSqlRowSet rs = super.queryForRowSet(
				"SELECT MAX(modify_datetime) FROM mny_draw_record WHERE member_id = :userId AND status = "
						+ MnyDrawRecord.STATUS_SUCCESS,
				MixUtil.newHashMap("userId", userId));
		if (rs.next()) {
			return rs.getDate(1);
		}
		return null;
	}

	public Date getLastSuccessCreateTime(Long userId) {
		ResultSetWrappingSqlRowSet rs = super.queryForRowSet(
				"SELECT MAX(create_datetime) FROM mny_draw_record WHERE member_id = :userId AND status = "
						+ MnyDrawRecord.STATUS_SUCCESS,
				MixUtil.newHashMap("userId", userId));
		if (rs.next()) {
			return rs.getDate(1);
		}
		return null;
	}

	public int updateStatusToExpired(Long drawId) {
		return super.update(
				"UPDATE mny_draw_record SET status = " + MnyDrawRecord.STATUS_EXPIRED + ", lock_flag = "
						+ MnyComRecord.LOCK_FLAG_COMPLETED + " WHERE status = " + MnyDrawRecord.STATUS_UNTREATED
						+ " AND lock_flag = " + MnyDrawRecord.LOCK_FLAG_UNLOCKED + " AND id = :drawId",
				MixUtil.newHashMap("drawId", drawId));
	}

	public int drawSuccess(Long drawId, Long status, String remark,long firstDraw,Long drawTimes) {
		return drawSuccess(drawId, status, remark, new Date(),firstDraw,drawTimes);
	}

	public int drawSuccess(Long drawId, Long status, String remark, Date modifyDatetime,long firstDraw,Long drawTimes) {
		return super.update(
				"UPDATE mny_draw_record SET status = :status,modify_datetime = :modifyDatetime,remark=:remark, lock_flag = "
						+ MnyComRecord.LOCK_FLAG_COMPLETED + ",first_draw =:firstDraw,draw_times = :drawTimes WHERE status = " + MnyDrawRecord.STATUS_UNTREATED
						+ " AND lock_flag = " + MnyDrawRecord.LOCK_FLAG_LOCKED + " AND id = :drawId",
				MixUtil.newHashMap("status", status, "drawId", drawId, "remark", remark, "modifyDatetime",
						modifyDatetime,"firstDraw",firstDraw,"drawTimes",drawTimes));
	}
	
	public int drawSuccessNotLock(Long drawId, Long status, String remark, Date modifyDatetime,long firstDraw,Long drawTimes) {
		return super.update(
				"UPDATE mny_draw_record SET status = :status,modify_datetime = :modifyDatetime,remark=:remark, lock_flag = "
						+ MnyComRecord.LOCK_FLAG_COMPLETED + ",first_draw =:firstDraw,draw_times = :drawTimes WHERE status = " + MnyDrawRecord.STATUS_UNTREATED
						+ " AND lock_flag = " + MnyDrawRecord.LOCK_FLAG_UNLOCKED + " AND id = :drawId",
				MixUtil.newHashMap("status", status, "drawId", drawId, "remark", remark, "modifyDatetime",
						modifyDatetime,"firstDraw",firstDraw,"drawTimes",drawTimes));
	}
	
	public int drawPayId(Long payId,Long status, Long drawId,String remark,Date modifyDatetime) {
		return super.update(
				"UPDATE mny_draw_record SET pay_id = :payId, status = :status,modify_datetime = :modifyDatetime,remark=:remark , lock_flag = "
						+ MnyDrawRecord.LOCK_FLAG_LOCKED_WAIT + " WHERE status = " + MnyDrawRecord.STATUS_UNTREATED
						+ " AND lock_flag = " + MnyDrawRecord.LOCK_FLAG_LOCKED + " AND id = :drawId",
				MixUtil.newHashMap("payId", payId,"status", status, "remark", remark, "modifyDatetime",
						modifyDatetime,"drawId", drawId));
	}
	public int drawNotitySuccess(Long drawId, Long status, String remark,long firstDraw,Long drawTimes) {
		return drawNotitySuccess(drawId, status, remark, new Date(),firstDraw,drawTimes);
	}

	
	public int drawNotitySuccess(Long drawId, Long status, String remark, Date modifyDatetime,long firstDraw,Long drawTimes) {
		return super.update(
				"UPDATE mny_draw_record SET status = :status,modify_datetime = :modifyDatetime,remark=:remark, lock_flag = "
						+ MnyDrawRecord.LOCK_FLAG_COMPLETED + ",first_draw = :firstDraw,draw_times = :drawTimes WHERE status = " + MnyDrawRecord.STATUS_WAIT
						+ " AND lock_flag = " + MnyDrawRecord.LOCK_FLAG_LOCKED_WAIT + " AND id = :drawId",
				MixUtil.newHashMap("status", status, "drawId", drawId, "remark", remark, "modifyDatetime",
						modifyDatetime,"firstDraw",firstDraw,"drawTimes",drawTimes));
	}
	public List<MnyDrawRecord> getSuccessList(Long accountId, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM mny_draw_record");
		sql_sb.append(" WHERE station_id= :stationId and status=").append(MnyDrawRecord.STATUS_SUCCESS);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (accountId != null) {
			sql_sb.append(" AND member_id= :accountId");
			paramMap.put("accountId", accountId);
		}

		if (beginDate != null) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", beginDate);
		}

		if (endDate != null) {
			sql_sb.append(" AND create_datetime <= :end");
			paramMap.put("end", endDate);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int updateLock(long id, long oldLock, long lockFlag, long userId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("id", id);
		paramMap.put("lockFlag", lockFlag);
		paramMap.put("date", new Date());
		paramMap.put("oldLock", oldLock);
		return update(
				"update mny_draw_record set lock_flag=:lockFlag,modify_user_id=:userId,modify_datetime=:date where id=:id and status=1 and lock_flag=:oldLock",paramMap);
	}
	
	public int cancleResult(Long comId, String opDesc) {
		StringBuilder sql = new StringBuilder("update mny_draw_record set ");
		sql.append("status=:status,remark=:opDesc,modify_datetime = :modifyDatetime");
		sql.append(" where id=:id and status=:oldStatus");
		Map map = MixUtil.newHashMap();
		map.put("status", MnyDrawRecord.STATUS_FAILED);
		map.put("oldStatus", MnyDrawRecord.STATUS_SUCCESS);
		map.put("modifyDatetime", new Date());
		map.put("opDesc", opDesc);
		map.put("id", comId);
		return super.update(sql.toString(), map);
	}
	
	public long countWithdrawCount(Long accountId, Date time, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("time", time);
		paramMap.put("stationId", stationId);
		return queryForLong(
				"select count(*) from mny_draw_record where station_id=:stationId and create_datetime>=:time and member_id=:accountId",
				paramMap);
	}

	public MnyDrawRecord getOrderNo(String orderNo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM mny_draw_record");
		sql_sb.append(" WHERE 1=1 ");
		sql_sb.append(" AND order_no=:orderNo");

		return super.query21Model(sql_sb.toString(), MixUtil.newHashMap("orderNo", orderNo));
	}
	
	public int selSuccessCount(Long accountId, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("stationId", stationId);
		return queryForInt(
				"select count(id) from mny_draw_record where station_id = :stationId and member_id = :accountId and status = "+MnyDrawRecord.STATUS_SUCCESS+" and lock_flag = "+MnyDrawRecord.LOCK_FLAG_COMPLETED,
				paramMap);
	}

	public int reback(Long drawId, long lockFlag) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", drawId);
		paramMap.put("lockFlag", lockFlag);
		paramMap.put("date", new Date());
		return update(
				"update mny_draw_record set lock_flag=:lockFlag,status= 1,first_draw= null,modify_datetime=:date where id=:id and status=2 and lock_flag=3",
				paramMap);
	}
	
	public int failReback(Long drawId, Date date, Long oldLockFlag, Long oldStatus, Long newLockFlag, Long newStatus) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("update mny_draw_record set lock_flag= :newLockFlag,status= :newStatus,modify_datetime=:date where id=:id and status=:oldStatus and lock_flag=:oldLockFlag");
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("newLockFlag", newLockFlag);
		paramMap.put("newStatus", newStatus);
		paramMap.put("date", date);
		paramMap.put("id", drawId);
		paramMap.put("oldStatus", oldStatus);
		paramMap.put("oldLockFlag", oldLockFlag);
		
		return super.update(sql_sb.toString(), paramMap);
	}
}
