package com.game.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentSignRecord;
import com.game.util.StationUtil;

@Repository
public class AgentSignRecordDao extends JdbcUtilImpl<AgentSignRecord>{
	
	public Page<Map> queryPage(){
		Map paramMap = MixUtil.newHashMap("stationId",StationUtil.getStationId());
		String sql =  "select * from agent_sign_record where station_id = :stationId order by id desc ";
		return super.page2CamelMap(sql, paramMap);
	}
	
	
	public List<AgentSignRecord> getMemberRecords(long memberId,Date startDate,Date endDate){
		Map paramMap = MixUtil.newHashMap("memberId",memberId,"startDate",startDate,"endDate",endDate);
		String sql = "select * from agent_sign_record "
				+ " where member_id = :memberId "
				+ " and sign_date > :startDate "
				+ " and sign_date < :endDate ";
		return super.query2Model(sql,paramMap);
	}
	
	public List<AgentSignRecord> getMemZxqd(Long stationId){
		Map paramMap = MixUtil.newHashMap("stationId",stationId);
		String sql = "select * from agent_sign_record "
				+ " where station_id = :stationId "
				+ " order by sign_date DESC "
				+ " limit 30 ";
		return super.query2Model(sql,paramMap);
	}


	public Page<Map> getMemZxqdPage(Long stationId) {
		Map paramMap = MixUtil.newHashMap("stationId",stationId);
		String sql = "select * from agent_sign_record "
				+ " where station_id = :stationId "
				+ " order by sign_date DESC ";
		return super.page2CamelMap(sql,paramMap);
	}
}
