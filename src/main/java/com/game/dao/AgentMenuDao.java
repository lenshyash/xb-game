package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AdminMenu;
import com.game.model.AgentMenu;

@Repository
public class AgentMenuDao extends JdbcUtilImpl<AgentMenu> {

	/**
	 * 用于权限判断
	 * 
	 * @param groupId
	 * @return
	 */
	public List<AgentMenu> getGroupPermissionMenu(Long groupId, Long stationId) {
		if (groupId == null) {
			return null;
		}
		Map<String, Object> paramMap = new HashMap<>();
		StringBuilder sql_sb = new StringBuilder("SELECT m.* FROM agent_menu_group a ");
		sql_sb.append(" INNER JOIN agent_menu m ON a.menu_id = m.id ");
		if (groupId > 0) {
			sql_sb.append(" INNER JOIN agent_user_group g on g.id=a.group_id ");
		}
		sql_sb.append(" WHERE m.status <>").append(AdminMenu.STATUS_DISABLED);
		sql_sb.append(" and a.station_id=:stationId AND a.group_id=:groupId ");
		paramMap.put("stationId", stationId);
		paramMap.put("groupId", groupId);
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	/**
	 * 获取超级管理员显示菜单栏 用于显示菜单栏
	 * 
	 * @return
	 */
	public List<AgentMenu> getRootPermissionMenu() {
		String sql = "SELECT * FROM agent_menu WHERE status <> " + AgentMenu.STATUS_DISABLED + " and level <= 3 ";
		return super.query2Model(sql);
	}

	public Page<Map> getMenuPage(Long parentId) {
		String sql = "SELECT * FROM agent_menu  WHERE parent_id = :parentId   ORDER BY sort ";
		return super.page2CamelMap(sql, MixUtil.newHashMap("parentId", parentId));
	}

	public List<AgentMenu> getMenuByLevel(int level) {
		String sql = "SELECT * FROM agent_menu WHERE level <= :level AND status <> " + AgentMenu.STATUS_DISABLED
				+ " order by sort";
		return super.query2Model(sql, MixUtil.newHashMap("level", level));
	}
}
