package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import com.game.model.AgentLoginOtp;

@Repository
public class AgentLoginOtpDao extends JdbcUtilImpl<AgentLoginOtp> {

	public Page<AgentLoginOtp> getPage(Long stationId, String iccid) {
		Map<String, Object> paramMap = new HashMap<>();
		StringBuilder sql = new StringBuilder("select * from agent_login_otp where 1=1");
		if (StringUtils.isNotEmpty(iccid)) {
			sql.append(" and iccid=:iccid");
			paramMap.put("iccid", iccid);
		}
		if (stationId != null && stationId > 0) {
			sql.append(" and station_id=:stationId");
			paramMap.put("stationId", stationId);
		}
		
		sql.append(" order by create_datetime desc");
		return paged2Obj(sql.toString(), paramMap);
	}

	public List<AgentLoginOtp> find(Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		return query2Model("select * from agent_login_otp where station_id=:stationId", map);
	}

	public boolean existOtp(Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		return queryForLong("select count(*) from agent_login_otp where station_id=:stationId",map) > 0;
	}

	public AgentLoginOtp findGoogleOtp(Long stationId, Long type) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("type", type);
		return query21Model("select * from agent_login_otp where station_id=:stationId and type=:type", map);
	}

}
