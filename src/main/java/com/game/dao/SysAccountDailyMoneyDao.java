package com.game.dao;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.game.model.lottery.BcLotteryData;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.jay.frame.FrameProperites;
import org.jay.frame.exception.JayFrameException;
import org.jay.frame.jdbc.*;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.model.vo.ReportParamVo;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Repository
public class SysAccountDailyMoneyDao extends JdbcUtilImpl<SysAccountDailyMoney> {

	public SysAccountDailyMoney findOneByAccountIdAndStatDate(Long accountId, Date statDate) {
		if (accountId == null || accountId == 0L || statDate == null) {
			return null;
		}
		return query21Model("select * from sys_account_daily_money where account_id=:accountId and stat_date=:statDate",
				MixUtil.newHashMap("accountId", accountId, "statDate", statDate));
	}

	public void updateThirdAmount(SysAccountDailyMoney dailyMoney) {
		StringBuilder sql = new StringBuilder("update sys_account_daily_money set real_bet_amount=:realBetAmount");
		sql.append(",real_win_amount=:realWinAmount,egame_bet_amount=:egameBetAmount,egame_win_amount=:egameWinAmount");
		sql.append(",hunter_bet_amount=:hunterBetAmount,hunter_win_amount=:hunterWinAmount ");
		sql.append(",real_bet_times=:realBetTimes,real_win_times=:realWinTimes");
		sql.append(",egame_bet_times=:egameBetTimes,egame_win_times=:egameWinTimes");
		sql.append(",hunter_bet_times=:hunterBetTimes,hunter_win_times=:hunterWinTimes");
		sql.append(",third_sports_bet_amount=:thirdSportsBetAmount,third_sports_win_amount=:thirdSportsWinAmount ");
		sql.append(",third_sports_bet_times=:thirdSportsBetTimes,third_sports_win_times=:thirdSportsWinTimes");

		sql.append(",third_lottery_bet_amount=:thirdLotteryBetAmount,third_lottery_win_amount=:thirdLotteryWinAmount ");
		sql.append(",third_lottery_bet_times=:thirdLotteryBetTimes,third_lottery_win_times=:thirdLotteryWinTimes");

		sql.append(",chess_bet_amount=:chessBetAmount,chess_win_amount=:chessWinAmount ");
		sql.append(",chess_bet_times=:chessBetTimes,chess_win_times=:chessWinTimes");
		
		sql.append(",esports_bet_amount=:esportsBetAmount,esports_win_amount=:esportsWinAmount ");
		sql.append(",esports_bet_times=:esportsBetTimes,esports_win_times=:esportsWinTimes");

		sql.append(" where account_id=:accountId and stat_date=:statDate");
		Map<String, Object> map = new HashMap<>();
		map.put("realBetAmount", getBigdecimalByDefaultZero(dailyMoney.getRealBetAmount()));
		map.put("realWinAmount", getBigdecimalByDefaultZero(dailyMoney.getRealWinAmount()));
		map.put("egameBetAmount", getBigdecimalByDefaultZero(dailyMoney.getEgameBetAmount()));
		map.put("egameWinAmount", getBigdecimalByDefaultZero(dailyMoney.getEgameWinAmount()));
		map.put("hunterBetAmount", getBigdecimalByDefaultZero(dailyMoney.getHunterBetAmount()));
		map.put("hunterWinAmount", getBigdecimalByDefaultZero(dailyMoney.getHunterWinAmount()));
		map.put("thirdSportsBetAmount", getBigdecimalByDefaultZero(dailyMoney.getThirdSportsBetAmount()));
		map.put("thirdSportsWinAmount", getBigdecimalByDefaultZero(dailyMoney.getThirdSportsWinAmount()));

		map.put("thirdLotteryBetAmount", getBigdecimalByDefaultZero(dailyMoney.getThirdLotteryBetAmount()));
		map.put("thirdLotteryWinAmount", getBigdecimalByDefaultZero(dailyMoney.getThirdLotteryWinAmount()));

		map.put("chessBetAmount", getBigdecimalByDefaultZero(dailyMoney.getChessBetAmount()));
		map.put("chessWinAmount", getBigdecimalByDefaultZero(dailyMoney.getChessWinAmount()));
		
		map.put("esportsBetAmount", getBigdecimalByDefaultZero(dailyMoney.getEsportsBetAmount()));
		map.put("esportsWinAmount", getBigdecimalByDefaultZero(dailyMoney.getEsportsWinAmount()));

		map.put("realBetTimes", getLongByDefaultZero(dailyMoney.getRealBetTimes()));
		map.put("realWinTimes", getLongByDefaultZero(dailyMoney.getRealWinTimes()));
		map.put("egameBetTimes", getLongByDefaultZero(dailyMoney.getEgameBetTimes()));
		map.put("egameWinTimes", getLongByDefaultZero(dailyMoney.getEgameWinTimes()));
		map.put("hunterBetTimes", getLongByDefaultZero(dailyMoney.getHunterBetTimes()));
		map.put("hunterWinTimes", getLongByDefaultZero(dailyMoney.getHunterWinTimes()));

		map.put("thirdSportsBetTimes", getLongByDefaultZero(dailyMoney.getThirdSportsBetTimes()));
		map.put("thirdSportsWinTimes", getLongByDefaultZero(dailyMoney.getThirdSportsWinTimes()));

		map.put("thirdLotteryBetTimes", getLongByDefaultZero(dailyMoney.getThirdLotteryBetTimes()));
		map.put("thirdLotteryWinTimes", getLongByDefaultZero(dailyMoney.getThirdLotteryWinTimes()));

		map.put("chessBetTimes", getLongByDefaultZero(dailyMoney.getChessBetTimes()));
		map.put("chessWinTimes", getLongByDefaultZero(dailyMoney.getChessWinTimes()));
		
		map.put("esportsBetTimes", getLongByDefaultZero(dailyMoney.getEsportsBetTimes()));
		map.put("esportsWinTimes", getLongByDefaultZero(dailyMoney.getEsportsWinTimes()));

		map.put("accountId", dailyMoney.getAccountId());
		map.put("statDate", dailyMoney.getStatDate());
		update(sql.toString(), map);
	}

	private Long getLongByDefaultZero(Long l) {
		if (l == null) {
			return 0L;
		}
		return l;
	}

	private BigDecimal getBigdecimalByDefaultZero(BigDecimal num) {
		if (num == null) {
			return BigDecimal.ZERO;
		}
		return num;
	}

	public void updateAmount(SysAccountDailyMoney dm) {
		StringBuilder sql = new StringBuilder("update sys_account_daily_money set");
		sql.append(
				" withdraw_amount=:withdrawAmount,withdraw_times=:withdrawTimes,withdraw_artificial=:withdrawArtificial");
		sql.append(",deposit_amount=:depositAmount,deposit_times=:depositTimes,deposit_artificial=:depositArtificial");
		sql.append(",deposit_gift_amount=:depositGiftAmount,deposit_gift_times=:depositGiftTimes ");
		sql.append(
				",lottery_bet_amount=:lotteryBetAmount,lottery_win_amount=:lotteryWinAmount,lottery_rebate_amount=:lotteryRebateAmount ");
		sql.append(
				",mark_six_bet_amount=:mark_six_bet_amount,mark_six_win_amount=:mark_six_win_amount,mark_six_rebate_amount=:mark_six_rebate_amount");
		sql.append(",real_rebate_amount=:real_rebate_amount,egame_rebate_amount=:egame_rebate_amount ");
		sql.append(
				",sports_bet_amount=:sports_bet_amount,sports_win_amount=:sports_win_amount,sports_rebate_amount=:sports_rebate_amount");
		sql.append(",balance=:balance,proxy_rebate_amount=:proxy_rebate_amount ");
		sql.append(" where  account_id=:accountId and stat_date=:statDate");
		Map<String, Object> map = new HashMap<>();
		map.put("withdrawAmount", dm.getWithdrawAmount() == null ? BigDecimal.ZERO : dm.getWithdrawAmount());
		map.put("withdrawTimes", dm.getWithdrawTimes() == null ? 0 : dm.getWithdrawTimes());
		map.put("withdrawArtificial",
				dm.getWithdrawArtificial() == null ? BigDecimal.ZERO : dm.getWithdrawArtificial());
		map.put("depositAmount", dm.getDepositAmount() == null ? BigDecimal.ZERO : dm.getDepositAmount());
		map.put("depositTimes", dm.getDepositTimes() == null ? 0 : dm.getDepositTimes());
		map.put("depositArtificial", dm.getDepositArtificial() == null ? BigDecimal.ZERO : dm.getDepositArtificial());
		map.put("depositGiftAmount", dm.getDepositGiftAmount() == null ? BigDecimal.ZERO : dm.getDepositGiftAmount());
		map.put("depositGiftTimes", dm.getDepositGiftTimes() == null ? BigDecimal.ZERO : dm.getDepositGiftTimes());
		map.put("lotteryBetAmount", dm.getLotteryBetAmount() == null ? BigDecimal.ZERO : dm.getLotteryBetAmount());
		map.put("lotteryWinAmount", dm.getLotteryWinAmount() == null ? BigDecimal.ZERO : dm.getLotteryWinAmount());
		map.put("lotteryRebateAmount",
				dm.getLotteryRebateAmount() == null ? BigDecimal.ZERO : dm.getLotteryRebateAmount());
		map.put("mark_six_bet_amount", dm.getMarkSixBetAmount() == null ? BigDecimal.ZERO : dm.getMarkSixBetAmount());
		map.put("mark_six_win_amount", dm.getMarkSixWinAmount() == null ? BigDecimal.ZERO : dm.getMarkSixWinAmount());
		map.put("mark_six_rebate_amount",
				dm.getMarkSixRebateAmount() == null ? BigDecimal.ZERO : dm.getMarkSixRebateAmount());
		map.put("real_rebate_amount", dm.getRealRebateAmount() == null ? BigDecimal.ZERO : dm.getRealRebateAmount());
		map.put("egame_rebate_amount", dm.getEgameRebateAmount() == null ? BigDecimal.ZERO : dm.getEgameRebateAmount());
		map.put("sports_bet_amount", dm.getSportsBetAmount() == null ? BigDecimal.ZERO : dm.getSportsBetAmount());
		map.put("sports_win_amount", dm.getSportsWinAmount() == null ? BigDecimal.ZERO : dm.getSportsWinAmount());
		map.put("sports_rebate_amount",
				dm.getSportsRebateAmount() == null ? BigDecimal.ZERO : dm.getSportsRebateAmount());
		map.put("balance", dm.getBalance() == null ? BigDecimal.ZERO : dm.getBalance());
		map.put("proxy_rebate_amount", dm.getProxyRebateAmount() == null ? BigDecimal.ZERO : dm.getProxyRebateAmount());
		map.put("accountId", dm.getAccountId());
		map.put("statDate", dm.getStatDate());
		update(sql.toString(), map);
	}

	public Page<Map> getFinanceReport(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT account_id,account,account_type,SUM(deposit_amount) AS deposit_total,");
		sql_sb.append(" SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_total,");
		sql_sb.append(" SUM(-withdraw_artificial) AS manual_withdraw_total,");
		sql_sb.append(" SUM(deposit_gift_amount) AS deposit_gift_money,");
		sql_sb.append(" SUM(-withdraw_gift_amount) AS withdraw_gift_amount,");
		sql_sb.append(" SUM(register_gift_amount) AS register_gift_money,");
		sql_sb.append(" SUM(-withdraw_amount) AS withdraw_total,");
		sql_sb.append(" SUM(proxy_rebate_amount) AS rebate_agent_total,");
		sql_sb.append(" SUM(lottery_rebate_amount");
		sql_sb.append("+sys_lottery_rebate_amount");
		sql_sb.append("+mark_six_rebate_amount");
		sql_sb.append("+sports_rebate_amount");
		sql_sb.append("+egame_rebate_amount");
		sql_sb.append("+hunter_rebate_amount");
		sql_sb.append("+real_rebate_amount");
		sql_sb.append("+third_sports_rebate_amount");
		sql_sb.append("+third_lottery_rebate_amount");
		sql_sb.append("+chess_rebate_amount");
		sql_sb.append(") AS rebate_total");
		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Long stationId = paramVo.getStationId();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		paramMap.put("stationId", stationId);
		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_sb.append(" AND stat_date <:end");
		}

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql_sb.append(" AND lower(account) = :account");
		}

		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				sql_sb.append(" AND (parents LIKE :children OR account_id = :curUserId)");
			} else {
				sql_sb.append(" AND.parents LIKE :children");
			}
		}
		
		if(StringUtil.isNotEmpty(reportType) && reportType!=0) {
			sql_sb.append(" and report_type=:reportType");
			paramMap.put("reportType", reportType);
		}

		sql_sb.append(" GROUP BY account_id,account,account_type");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public Page getRiskCunQuReport(ReportParamVo paramVo) {
//		paramVo.setSortName(null);
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,agent_name,");
		sql.append(" SUM(deposit_amount) AS deposit_total,");
		sql.append(" SUM(-withdraw_amount) AS withdraw_total,");
		sql.append(" SUM(deposit_artificial) AS deposit_artificial_total,");
		sql.append(" SUM(-withdraw_artificial) AS withdraw_artificial_total,");
		sql.append(" SUM(deposit_times) AS deposit_times,");
		sql.append(" SUM(withdraw_times) AS withdraw_times, ");
		
		sql.append(" SUM(");
		//有效投注
		sql.append(" (-lottery_bet_amount-sys_lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sports_bet_amount+egame_bet_amount+hunter_bet_amount+real_bet_amount+third_sports_bet_amount+third_lottery_bet_amount+chess_bet_amount)");
		sql.append(" -");
		//中奖金额
		sql.append(" (lottery_win_amount+sys_lottery_win_amount+sf_mark_six_win_amount+mark_six_win_amount+sports_win_amount+egame_win_amount+hunter_win_amount+real_win_amount+third_sports_win_amount+third_lottery_win_amount+chess_win_amount)");
		sql.append(" -");
		//反水
		sql.append(" (proxy_rebate_amount)");
		sql.append(" ) AS profit ");
		
		StringBuilder sql_where = new StringBuilder(
				" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id  WHERE (deposit_amount>0 or withdraw_amount<0 OR deposit_artificial>0 or withdraw_artificial<0) and ");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_total", "depositAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.withdraw_total", "withdrawAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_times", "depositTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.withdraw_times", "withdrawTimesTotal"));
		aggs.add(
				new Aggregation(AggregationFunction.SUM, "a.deposit_artificial_total", "depositArtificialAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.withdraw_artificial_total",
				"withdrawArtificialAmountTotal"));

		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskLotteryReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "lottery")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(-lottery_bet_amount) AS lottery_bet_total,");
		sql.append(" SUM(lottery_win_amount) AS lottery_win_total,");
		sql.append(" SUM(lottery_bet_times) AS lottery_bet_times_total,");
		sql.append(" SUM(lottery_rebate_amount) AS lottery_rebate_total,");
		sql.append(" SUM(lottery_win_times) AS lottery_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE lottery_bet_amount<0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.lottery_bet_total", "lotteryBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.lottery_win_total", "lotteryWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.lottery_bet_times_total", "lotteryBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.lottery_rebate_total", "lotteryRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.lottery_win_times_total", "lotteryWinTimesTotal"));

		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskSysLotteryReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "sysLottery")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(-sys_lottery_bet_amount) AS sys_lottery_bet_total,");
		sql.append(" SUM(sys_lottery_win_amount) AS sys_lottery_win_total,");
		sql.append(" SUM(sys_lottery_bet_times) AS sys_lottery_bet_times_total,");
		sql.append(" SUM(sys_lottery_rebate_amount) AS sys_lottery_rebate_total,");
		sql.append(" SUM(sys_lottery_win_times) AS sys_lottery_win_times_total");
		StringBuilder sql_where = new StringBuilder(
				" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id  WHERE sys_lottery_bet_amount<0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sys_lottery_bet_total", "sysLotteryBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sys_lottery_win_total", "sysLotteryWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sys_lottery_bet_times_total", "sysLotteryBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sys_lottery_rebate_total", "sysLotteryRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sys_lottery_win_times_total", "sysLotteryWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskThirdSportsReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "thirdSports")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(third_sports_rebate_amount) AS third_sports_rebate_total,");
		sql.append(" SUM(third_sports_bet_amount) AS third_sports_bet_total,");
		sql.append(" SUM(third_sports_win_amount) AS third_sports_win_total,");
		sql.append(" SUM(third_sports_bet_times) AS third_sports_bet_times_total,");
		sql.append(" SUM(third_sports_win_times) AS third_sports_win_times_total");
		StringBuilder sql_where = new StringBuilder(
				" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE third_sports_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_sports_rebate_total", "thirdSportsRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_sports_bet_total", "thirdSportsBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_sports_win_total", "thirdSportsWinTotal"));
		aggs.add(
				new Aggregation(AggregationFunction.SUM, "a.third_sports_bet_times_total", "thirdSportsBetTimesTotal"));
		aggs.add(
				new Aggregation(AggregationFunction.SUM, "a.third_sports_win_times_total", "thirdSportsWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskThirdLotteryReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "thirdLottery")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(third_lottery_rebate_amount) AS third_lottery_rebate_total,");
		sql.append(" SUM(third_lottery_bet_amount) AS third_lottery_bet_total,");
		sql.append(" SUM(third_lottery_win_amount) AS third_lottery_win_total,");
		sql.append(" SUM(third_lottery_bet_times) AS third_lottery_bet_times_total,");
		sql.append(" SUM(third_lottery_win_times) AS third_lottery_win_times_total");
		StringBuilder sql_where = new StringBuilder(
				" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE third_lottery_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_lottery_rebate_total", "thirdLotteryRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_lottery_bet_total", "thirdLotteryBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_lottery_win_total", "thirdLotteryWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_lottery_bet_times_total",
				"thirdLotteryBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.third_lottery_win_times_total",
				"thirdLotteryWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskChessReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "chess")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(chess_rebate_amount) AS chess_rebate_total,");
		sql.append(" SUM(chess_bet_amount) AS chess_bet_total,");
		sql.append(" SUM(chess_win_amount) AS chess_win_total,");
		sql.append(" SUM(chess_bet_times) AS chess_bet_times_total,");
		sql.append(" SUM(chess_win_times) AS chess_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE chess_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.chess_rebate_total", "chessRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.chess_bet_total", "chessBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.chess_win_total", "chessWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.chess_bet_times_total", "chessBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.chess_win_times_total", "chessWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskSfMarkSixReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "sfMarkSix")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(-sf_mark_six_bet_amount) AS sf_mark_six_bet_total,");
		sql.append(" SUM(sf_mark_six_win_amount) AS sf_mark_six_win_total,");
		sql.append(" SUM(sf_mark_six_bet_times) AS sf_mark_six_bet_times_total,");
		sql.append(" SUM(sf_mark_six_rebate_amount) AS sf_mark_six_rebate_total,");
		sql.append(" SUM(sf_mark_six_win_times) AS sf_mark_six_win_times_total");
		StringBuilder sql_where = new StringBuilder(
				" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE sf_mark_six_bet_amount<0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sf_mark_six_bet_total", "sfMarkSixBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sf_mark_six_win_total", "sfMarkSixWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sf_mark_six_bet_times_total", "sfMarkSixBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sf_mark_six_rebate_total", "sfMarkSixRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sf_mark_six_win_times_total", "sfMarkSixWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskSportReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "sports")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(-sports_bet_amount) AS sports_bet_total,");
		sql.append(" SUM(sports_win_amount) AS sports_win_total,");
		sql.append(" SUM(sports_rebate_amount) AS sports_rebate_total,");
		sql.append(" SUM(sports_bet_times) AS sports_bet_times_total,");
		sql.append(" SUM(sports_win_times) AS sports_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE sports_bet_amount<0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sports_bet_total", "sportsBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sports_win_total", "sportsWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sports_rebate_total", "sportsRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sports_bet_times_total", "sportsBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.sports_win_times_total", "sportsWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskRealReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "real")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(real_bet_amount) AS real_bet_total,");
		sql.append(" SUM(real_win_times) AS real_win_times_total,");
		sql.append(" SUM(real_win_amount) AS real_win_total,");
		sql.append(" SUM(real_rebate_amount) AS real_rebate_total,");
		sql.append(" SUM(real_bet_times) AS real_bet_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE real_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.real_bet_total", "realBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.real_win_times_total", "realWinTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.real_win_total", "realWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.real_rebate_total", "realRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.real_bet_times_total", "realBetTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskEgameReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "egame")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(egame_rebate_amount) AS egame_rebate_total,");
		sql.append(" SUM(egame_bet_amount) AS egame_bet_total,");
		sql.append(" SUM(egame_win_amount) AS egame_win_total,");
		sql.append(" SUM(egame_bet_times) AS egame_bet_times_total,");
		sql.append(" SUM(egame_win_times) AS egame_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE egame_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.egame_rebate_total", "egameRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.egame_bet_total", "egameBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.egame_win_total", "egameWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.egame_bet_times_total", "egameBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.egame_win_times_total", "egameWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskHunterReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "hunter")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(hunter_rebate_amount) AS hunter_rebate_total,");
		sql.append(" SUM(hunter_bet_amount) AS hunter_bet_total,");
		sql.append(" SUM(hunter_win_amount) AS hunter_win_total,");
		sql.append(" SUM(hunter_bet_times) AS hunter_bet_times_total,");
		sql.append(" SUM(hunter_win_times) AS hunter_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE hunter_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.hunter_rebate_total", "hunterRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.hunter_bet_total", "hunterBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.hunter_win_total", "hunterWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.hunter_bet_times_total", "hunterBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.hunter_win_times_total", "hunterWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	public Page getRiskMarkSixReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "markSix")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(mark_six_rebate_amount) AS mark_six_rebate_total,");
		sql.append(" SUM(-mark_six_bet_amount) AS mark_six_bet_total,");
		sql.append(" SUM(mark_six_win_amount) AS mark_six_win_total,");
		sql.append(" SUM(mark_six_bet_times) AS mark_six_bet_times_total,");
		sql.append(" SUM(mark_six_win_times) AS mark_six_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE mark_six_bet_amount<0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.mark_six_rebate_total", "markSixRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.mark_six_bet_total", "markSixBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.mark_six_win_total", "markSixWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.mark_six_bet_times_total", "markSixBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.mark_six_win_times_total", "markSixWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	private Page getRiskReport1(ReportParamVo paramVo, StringBuilder sql, StringBuilder sql_where) {
		return getRiskReport1(paramVo, sql, sql_where, null);
	}

	private Page getRiskReport1(ReportParamVo paramVo, StringBuilder sql, StringBuilder sql_where,
			List<Aggregation> aggs) {
		sql_where.append(" a.station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("stationId", paramVo.getStationId());
		if (StringUtil.isNotEmpty(paramVo.getBegin())) {
			paramMap.put("begin", paramVo.getBegin());
			sql_where.append(" AND a.stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(paramVo.getEnd())) {
			paramMap.put("end", paramVo.getEnd());
			sql_where.append(" AND a.stat_date <:end");
		}

		if (Validator.isNotNull(paramVo.getAccountId())) {
			paramMap.put("accountId", paramVo.getAccountId());
			sql_where.append(" AND a.account_id =:accountId");
		}

		if (StringUtil.isNotEmpty(paramVo.getAccount())) {
			paramMap.put("account", paramVo.getAccount());
			sql_where.append(" AND a.account =:account");
		}
		
		if (StringUtil.isNotEmpty(paramVo.getUserName())) {
			paramMap.put("userName", paramVo.getUserName());
			sql_where.append(" AND i.user_name =:userName");
		}

		
		if (Validator.isNotNull(paramVo.getReportType())) {
			paramMap.put("reportType", paramVo.getReportType());
			sql_where.append(" AND a.report_type =:reportType");
		}
		
		if (StringUtil.isNotEmpty(paramVo.getChildren())) {
			paramMap.put("children", paramVo.getChildren() + "%");
			Boolean searchSelf = paramVo.getSearchSelf();
			if (StringUtil.isNotEmpty(searchSelf) && searchSelf) {
				sql_where.append(" AND (a.account_id = :selfId OR a.parents LIKE :children)");
				paramMap.put("selfId", paramVo.getSelfId());
			} else {
				sql_where.append(" AND a.parents LIKE :children");
			}
		}

		sql_where.append(" GROUP BY a.account_id,a.account,a.account_type,i.user_name,agent_name");

		String sortName = paramVo.getSortName();
		if (StringUtil.isNotEmpty(sortName)) {
			//不是存取款分析
			if(!"cunqu".equals(paramVo.getType())) {
				
				sql_where.append(" ORDER BY ");
					
				String preFix = "";
				if (sortName.indexOf("Bunko") != -1) {
					preFix = sortName.substring(0, sortName.indexOf("Bunko"));
					preFix = replaceUpperToUnderline(preFix);
					sql.append(",COALESCE(SUM(ABS(").append(preFix).append("_bet_amount)-");
					sql.append(preFix).append("_win_amount),0) AS ").append(preFix).append("_bunko");
					sql_where.append(" ").append(preFix).append("_bunko");
				} else if (sortName.indexOf("WinRate") != -1) {
					preFix = sortName.substring(0, sortName.indexOf("WinRate"));
					preFix = replaceUpperToUnderline(preFix);
					sql.append(",COALESCE(CASE WHEN SUM(").append(preFix).append("_bet_times) > 0 THEN");
					sql.append(" SUM(").append(preFix).append("_win_times) / SUM(").append(preFix);
					sql.append("_bet_times) ::float").append(" WHEN SUM(").append(preFix);
					sql.append("_win_times) > 0 THEN 1 ::float END,0 ) AS ").append(preFix).append("_rate ");
					sql_where.append(preFix).append("_rate");
				} else {
					sortName = replaceUpperToUnderline(sortName);
					sql_where.append(sortName);
				}
	
				sql_where.append(" " + paramVo.getSortOrder());
			}
		}

		if (aggs != null) {
			String mainSql = sql.append(sql_where.toString()).toString();
			StringBuilder sql_total = new StringBuilder("SELECT sa.last_login_datetime,sa.create_datetime,m.money,a.*,dd.new_total_deposit,dd.new_total_withdraw FROM (");
			sql_total.append(mainSql);
			sql_total.append("	) A LEFT JOIN mny_money m on a.account_id = m.account_id");
			
			sql_total.append(" left join sys_account sa on a.account_id = sa.id");
			
			sql_total.append(" left join");
			sql_total.append(" (select account_id,sum(deposit_amount+deposit_artificial+sys_api_deposit_amount) as new_total_deposit,sum(-withdraw_amount) as new_total_withdraw from sys_account_daily_money where 1 =1 and station_id = 25 GROUP BY account_id) dd");
			sql_total.append(" on a.account_id = dd.account_id");
			
			StringBuilder new_sql_total = new StringBuilder("SELECT * from (");
			new_sql_total.append(sql_total);
			new_sql_total.append(") a");
			
			if("cunqu".equals(paramVo.getType())) {
				if(StringUtil.isNotEmpty(paramVo.getSortName())) {
					sortName = replaceUpperToUnderline(paramVo.getSortName());
					new_sql_total.append(" order by ").append(sortName).append(" "+paramVo.getSortOrder());
				}
				
			}
			
			return super.page2CamelMap(new_sql_total.toString(), paramMap, aggs);
		}
		return super.page2CamelMap(sql.append(sql_where.toString()).toString(), paramMap);
	}

	public Page getRiskReport(ReportParamVo paramVo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_from = new StringBuilder();
		StringBuilder sql_where = new StringBuilder();
		StringBuilder sql_group = new StringBuilder();
		StringBuilder sql_order = new StringBuilder();
		sql_cols.append("SELECT account_id,account,account_type,");
		// 存取款
		sql_cols.append(" SUM(deposit_amount) AS deposit_total,");
		sql_cols.append(" SUM(-withdraw_amount) AS withdraw_total,");
		sql_cols.append(" SUM(withdraw_times) AS withdraw_times,");
		sql_cols.append(" SUM(deposit_times) AS deposit_times,");
		// 反水
		sql_cols.append(" SUM(lottery_rebate_amount) AS lottery_rebate_total,");
		sql_cols.append(" SUM(mark_six_rebate_amount) AS mark_six_rebate_total,");
		sql_cols.append(" SUM(sports_rebate_amount) AS sports_rebate_total,");
		sql_cols.append(" SUM(egame_rebate_amount) AS egame_rebate_total,");
		sql_cols.append(" SUM(hunter_rebate_amount) AS hunter_rebate_total,");
		sql_cols.append(" SUM(real_rebate_amount) AS real_rebate_total,");
		// 投注
		sql_cols.append(" SUM(-lottery_bet_amount) AS lottery_bet_total,");
		sql_cols.append(" SUM(-mark_six_bet_amount) AS mark_six_bet_total,");
		sql_cols.append(" SUM(-sports_bet_amount) AS sports_bet_total,");
		sql_cols.append(" SUM(egame_bet_amount) AS egame_bet_total,");
		sql_cols.append(" SUM(hunter_bet_amount) AS hunter_bet_total,");
		sql_cols.append(" SUM(real_bet_amount) AS real_bet_total,");
		// 中奖
		sql_cols.append(" SUM(lottery_win_amount) AS lottery_win_total,");
		sql_cols.append(" SUM(mark_six_win_amount) AS mark_six_win_total,");
		sql_cols.append(" SUM(sports_win_amount) AS sports_win_total,");
		sql_cols.append(" SUM(egame_win_amount) AS egame_win_total,");
		sql_cols.append(" SUM(hunter_win_amount) AS hunter_win_total,");
		sql_cols.append(" SUM(real_win_amount) AS real_win_total,");
		// 投注次数
		sql_cols.append(" SUM(lottery_bet_times) AS lottery_bet_times_total,");
		sql_cols.append(" SUM(mark_six_bet_times) AS mark_six_bet_times_total,");
		sql_cols.append(" SUM(sports_bet_times) AS sports_bet_times_total,");
		sql_cols.append(" SUM(egame_bet_times) AS egame_bet_times_total,");
		sql_cols.append(" SUM(hunter_bet_times) AS hunter_bet_times_total,");
		sql_cols.append(" SUM(real_bet_times) AS real_bet_times_total,");
		// 中奖次数
		sql_cols.append(" SUM(lottery_win_times) AS lottery_win_times_total,");
		sql_cols.append(" SUM(mark_six_win_times) AS mark_six_win_times_total,");
		sql_cols.append(" SUM(sports_win_times) AS sports_win_times_total,");
		sql_cols.append(" SUM(egame_win_times) AS egame_win_times_total,");
		sql_cols.append(" SUM(hunter_win_times) AS hunter_win_times_total,");
		sql_cols.append(" SUM(real_win_times) AS real_win_times_total");

		sql_from.append(" FROM sys_account_daily_money");
		sql_where.append(" WHERE station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Long stationId = paramVo.getStationId();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		Long accountId = paramVo.getAccountId();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Boolean searchSelf = paramVo.getSearchSelf();
		paramMap.put("stationId", stationId);
		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_where.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_where.append(" AND stat_date <:end");
		}

		if (Validator.isNotNull(accountId)) {
			paramMap.put("accountId", accountId);
			sql_where.append(" AND account_id =:accountId");
		}

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			sql_where.append(" AND account =:account");
		}

		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (StringUtil.isNotEmpty(searchSelf) && searchSelf) {
				sql_where.append(" AND (account_id = :selfId OR parents LIKE :children)");
				paramMap.put("selfId", paramVo.getSelfId());
			} else {
				sql_where.append(" AND parents LIKE :children");
			}
		}

		sql_group.append(" GROUP BY account_id,account,account_type");

		String sortName = paramVo.getSortName();
		if (StringUtil.isNotEmpty(sortName)) {
			sql_order.append(" ORDER BY ");

			String preFix = "";
			if (sortName.indexOf("Bunko") != -1) {
				preFix = sortName.substring(0, sortName.indexOf("Bunko"));
				preFix = replaceUpperToUnderline(preFix);
				sql_cols.append(",COALESCE(SUM(ABS(").append(preFix).append("_bet_amount)-").append(preFix)
						.append("_win_amount),0) AS ").append(preFix).append("_bunko");
				sql_order.append(" ").append(preFix).append("_bunko");
			} else if (sortName.indexOf("WinRate") != -1) {
				preFix = sortName.substring(0, sortName.indexOf("WinRate"));
				preFix = replaceUpperToUnderline(preFix);
				sql_cols.append(",COALESCE(CASE WHEN SUM(").append(preFix).append("_bet_times) > 0 THEN");
				sql_cols.append(" SUM(").append(preFix).append("_win_times) / SUM(").append(preFix)
						.append("_bet_times) ::float").append(" WHEN SUM(").append(preFix)
						.append("_win_times) > 0 THEN 1 ::float END,0 ) AS ").append(preFix).append("_rate ");
				sql_order.append(preFix).append("_rate");
			} else {
				sortName = replaceUpperToUnderline(sortName);
				sql_order.append(sortName);
			}

			sql_order.append(" " + paramVo.getSortOrder());
		}

		return super.page2CamelMap(
				sql_cols.append(sql_from).append(sql_where).append(sql_group).append(sql_order).toString(), paramMap);
	}

	private String replaceUpperToUnderline(String str) {
		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile("[A-Z]+");
		Matcher m1 = p.matcher(str); // 判断是否含有大写字符
		while (m1.find()) {
			m1.appendReplacement(sb, "_" + m1.group().toLowerCase());
		}
		m1.appendTail(sb);
		return sb.toString();
	}

	public Map getReport(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT ");
		sql_sb.append(" sum(deposit_times+deposit_handler_times) as deposit_times,sum(withdraw_times) as withdraw_times,COUNT(DISTINCT CASE WHEN deposit_amount <>0 OR deposit_artificial <>0  THEN account_id ELSE NULL END) as deposit_member_count,COUNT(DISTINCT CASE WHEN withdraw_amount <>0 THEN account_id ELSE NULL END) as withdraw_member_count,");
		sql_sb.append(" SUM(deposit_amount) AS deposit_total,");
		sql_sb.append(" SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_total,");
		sql_sb.append(" SUM(withdraw_artificial) AS manual_withdraw_total,");
		sql_sb.append(" SUM(deposit_gift_amount) AS give_total,");
		sql_sb.append(" SUM(-withdraw_gift_amount) AS withdraw_give_total,");
		sql_sb.append(" SUM(register_gift_amount) AS give_register_total,");
		sql_sb.append(" SUM(level_up_gift_amount) AS give_level_up_total,");
		sql_sb.append(" SUM(withdraw_amount) AS withdraw_total,");
		sql_sb.append(" SUM(proxy_rebate_amount) AS rebate_agent_total,");

		sql_sb.append(" SUM(sports_bet_amount) AS sport_total,");
		sql_sb.append(" SUM(sports_win_amount) AS sport_award,");

		sql_sb.append(" SUM(active_award_amount) AS active_award_total,");

		sql_sb.append(" SUM(lottery_bet_amount) AS lottery_total,");
		sql_sb.append(" SUM(lottery_win_amount) AS lottery_award,");

		sql_sb.append(" SUM(sys_lottery_bet_amount) AS sys_lottery_total,");
		sql_sb.append(" SUM(sys_lottery_win_amount) AS sys_lottery_award,");

		sql_sb.append(" SUM(sf_mark_six_bet_amount) AS sf_mark_six_total,");
		sql_sb.append(" SUM(sf_mark_six_win_amount) AS sf_mark_six_award,");

		sql_sb.append(" SUM(mark_six_bet_amount) AS mark_six_total,");
		sql_sb.append(" SUM(mark_six_win_amount) AS mark_six_award,");

		sql_sb.append(" SUM(real_bet_amount) AS real_total,");
		sql_sb.append(" SUM(real_win_amount) AS real_award,");

		sql_sb.append(" SUM(third_sports_bet_amount) AS third_sports_total,");
		sql_sb.append(" SUM(third_sports_win_amount) AS third_sports_award,");

		sql_sb.append(" SUM(third_lottery_bet_amount) AS third_lottery_total,");
		sql_sb.append(" SUM(third_lottery_win_amount) AS third_lottery_award,");

		sql_sb.append(" SUM(chess_bet_amount) AS chess_total,");
		sql_sb.append(" SUM(chess_win_amount) AS chess_award,");

		sql_sb.append(" SUM(esports_bet_amount) AS esports_total,");
		sql_sb.append(" SUM(esports_win_amount) AS esports_award,");
		
		sql_sb.append(" SUM(egame_bet_amount+hunter_bet_amount) AS dian_zi_total,");
		sql_sb.append(" SUM(egame_win_amount+hunter_win_amount) AS dian_zi_award,");
		sql_sb.append(" COUNT(DISTINCT CASE WHEN egame_bet_amount <>0 OR ");
		sql_sb.append(" hunter_bet_amount <>0 OR ");
		sql_sb.append(" sports_bet_amount <>0 OR ");
		sql_sb.append(" lottery_bet_amount <>0 OR ");
		sql_sb.append(" sys_lottery_bet_amount <>0 OR ");
		sql_sb.append(" mark_six_bet_amount <>0 OR ");
		sql_sb.append(" sf_mark_six_bet_amount <>0 OR ");
		sql_sb.append(" third_sports_bet_amount <>0 OR ");
		sql_sb.append(" third_lottery_bet_amount <>0 OR ");
		sql_sb.append(" chess_bet_amount <>0 OR ");
		sql_sb.append(" esports_bet_amount <>0 OR ");
		sql_sb.append(" real_bet_amount <>0");
		sql_sb.append(" THEN account_id ELSE NULL END) AS bet_count_total,");

		sql_sb.append(" SUM(first_deposit) AS first_deposit_total,");
		sql_sb.append(" SUM(balance_gem_amount) AS balance_gem_total,");

		sql_sb.append(" SUM(lottery_rebate_amount");
		sql_sb.append("+sys_lottery_rebate_amount");
		sql_sb.append("+mark_six_rebate_amount");
		sql_sb.append("+sports_rebate_amount");
		sql_sb.append("+egame_rebate_amount");
		sql_sb.append("+hunter_rebate_amount");
		sql_sb.append("+real_rebate_amount");
		sql_sb.append("+chess_rebate_amount");
		sql_sb.append("+third_lottery_rebate_amount");
		sql_sb.append("+third_sports_rebate_amount");
		sql_sb.append("+esports_rebate_amount");
		sql_sb.append(") AS rebate_total,");
		sql_sb.append(" SUM(deposit_handler_artificial) AS deposit_handler_artificial");
		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE station_id=:stationId");
		Long stationId = paramVo.getStationId();
		Long reportType = paramVo.getReportType();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Boolean searchSelf = paramVo.getSearchSelf();
		String filterAccount = paramVo.getFilterAccount();
		Map paramMap = MixUtil.newHashMap("stationId", stationId);

		if (paramVo.getAccountId() != null) {
			paramMap.put("accountId", paramVo.getAccountId());
			sql_sb.append(" AND account_id =:accountId");
		} else {
			if (StringUtil.isNotEmpty(account)) {
				paramMap.put("account", account);
				sql_sb.append(" AND account =:account");
			}
		}
		AggregationBuilders.terms("player_count ").field("team");
		if (StringUtil.isNotEmpty(children)) {
			if (!StationUtil.isDailiStation()
					|| StringUtil.equals(paramVo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT_ALL)) {
				paramMap.put("children", children + "%");
			} else {
				paramMap.put("children", children);
			}

			if (StringUtil.isNotEmpty(searchSelf) && searchSelf) {
				sql_sb.append(" AND (account_id = :selfId OR parents LIKE :children)");
				paramMap.put("selfId", paramVo.getSelfId());
			} else {
				sql_sb.append(" AND parents LIKE :children");
			}
		}

		if (Validator.isNotNull(reportType)) {
			paramMap.put("reportType", reportType);
			sql_sb.append(" AND report_type =:reportType");
		}
		
		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_sb.append(" AND stat_date <:end");
		}
		if (StringUtil.isNotEmpty(filterAccount)) {
			String [] arr = filterAccount.split(",| |\n");
			sql_sb.append(" AND account not in (");
			for(int i =0;i<arr.length;i++){
				if(i<arr.length-1){
					sql_sb.append("'"+arr[i]+"',");
				}else{
					sql_sb.append("'"+arr[i]+"'");
				}
			}
			sql_sb.append(")");
		}
		//会员类型是引导的进去了正常的报表   余额宝计算  这里先把引导账号过滤  先这样处理
		sql_sb.append(" and account_type!=:accountType");
		paramMap.put("accountType", 9);
		
		return super.selectSingleCamelMap(sql_sb.toString(), paramMap);
	}

	public Page getDayReport(ReportParamVo paramVo) {
		String sql_sb = "select * from (select m.stat_date as stat_date, SUM(m.deposit_amount) AS deposit_total,"
				+ " SUM(m.first_deposit) AS first_deposit,"
				+ " SUM(m.first_deposit_amount) AS first_deposit_amount,"
				+ " SUM(m.first_withdraw) AS first_withdraw,"
				+ " SUM(m.first_withdraw_amount) AS first_withdraw_amount,"
				+ " SUM(m.deposit_artificial+m.sys_api_deposit_amount) AS manual_deposit_total,"
				+ " SUM(m.withdraw_artificial) AS manual_withdraw_total," + " SUM(m.withdraw_amount) AS withdraw_total "
				+ " from sys_account_daily_money m where m.station_id=:stationId and m.stat_date>=:begin and m.stat_date<=:end";
		Map<String, Object> paramMap = new ConcurrentHashMap<String, Object>();
		if(StringUtil.isNotEmpty(paramVo.getReportType()) && paramVo.getReportType()!=0) {
			sql_sb = sql_sb + " and m.report_type=:reportType";
			paramMap.put("reportType", paramVo.getReportType());
		}
		
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("begin", paramVo.getBegin());
		paramMap.put("end", paramVo.getEnd());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			sql_sb = sql_sb + " AND account =:account";
		}

		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				sql_sb = sql_sb + " AND (parents LIKE :children OR account_id = :curUserId)";
			} else {
				sql_sb = sql_sb + " AND parents LIKE :children";
			}
		}

		sql_sb = sql_sb + " group by m.stat_date order by m.stat_date desc ) a ";

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_total", "depositTotalSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.manual_deposit_total", "manualDepositTotalSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.manual_withdraw_total", "manualWithdrawTotalSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "withdraw_total", "withdrawTotalSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"a.deposit_total+manual_deposit_total+manual_withdraw_total+withdraw_total", "lossSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM,"a.first_deposit", "firstDepositSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM,"a.first_deposit_amount", "firstDepositAmountSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM,"a.first_withdraw", "firstWithdrawSum"));
		aggs.add(new Aggregation(AggregationFunction.SUM,"a.first_withdraw_amount", "firstWithdrawAmountSum"));

		return super.page2CamelMap(sql_sb.toString(), paramMap, aggs);
	}

	public Page<Map> findTeamStatistic(Long stationId, String account, String parentIds, Long agentId, Date startTime,
			Date endTime, Long searchType, String sortName, String sortOrder, Long reportType) {
		

		
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT a.*,s.last_login_datetime,s.create_datetime,(betting_amount-award_amount-rebate_agent_amount) AS profit");
		sql_sb.append(",(betting_amount-award_amount-rebate_agent_amount-rebate_amount) AS team_profit FROM (");
		sql_sb.append(
				"SELECT account_id,account,agent_id,agent_name,account_type,SUM(deposit_amount) AS deposit_amount,");
		sql_sb.append(" SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_amount,");
		sql_sb.append(" SUM(-withdraw_artificial) AS manual_withdraw_amount,");
		sql_sb.append(" SUM(deposit_gift_amount) AS deposit_gift_amount,");
		sql_sb.append(" SUM(-withdraw_gift_amount) AS withdraw_gift_amount,");
		sql_sb.append(" SUM(register_gift_amount) AS register_gift_amount,");
		sql_sb.append(" SUM(-withdraw_amount) AS withdraw_amount,");
		sql_sb.append(" SUM(proxy_rebate_amount) AS rebate_agent_amount,");

		sql_sb.append(" SUM(-lottery_bet_amount");
		sql_sb.append("-sys_lottery_bet_amount");
		sql_sb.append("-mark_six_bet_amount");
		sql_sb.append("-sf_mark_six_bet_amount");
		sql_sb.append("-sports_bet_amount");
		sql_sb.append("+egame_bet_amount");
		sql_sb.append("+hunter_bet_amount");
		sql_sb.append("+real_bet_amount");
		sql_sb.append("+third_sports_bet_amount");
		sql_sb.append("+third_lottery_bet_amount");
		sql_sb.append("+chess_bet_amount");
		sql_sb.append(") AS betting_amount,");
		sql_sb.append(" SUM(lottery_win_amount");
		sql_sb.append("+sys_lottery_win_amount");
		sql_sb.append("+sf_mark_six_win_amount");
		sql_sb.append("+mark_six_win_amount");
		sql_sb.append("+sports_win_amount");
		sql_sb.append("+egame_win_amount");
		sql_sb.append("+hunter_win_amount");
		sql_sb.append("+real_win_amount");
		sql_sb.append("+third_sports_win_amount");
		sql_sb.append("+third_lottery_win_amount");
		sql_sb.append("+chess_win_amount");

		sql_sb.append(") AS award_amount,");

		sql_sb.append(" SUM(lottery_rebate_amount");
		sql_sb.append("+sys_lottery_rebate_amount");
		sql_sb.append("+mark_six_rebate_amount");
		sql_sb.append("+sf_mark_six_rebate_amount");
		sql_sb.append("+sports_rebate_amount");
		sql_sb.append("+egame_rebate_amount");
		sql_sb.append("+hunter_rebate_amount");
		sql_sb.append("+real_rebate_amount");
		sql_sb.append("+third_sports_rebate_amount");
		sql_sb.append("+third_lottery_rebate_amount");
		sql_sb.append("+chess_rebate_amount");
		sql_sb.append(") AS rebate_amount,");
		sql_sb.append("SUM(deposit_times+deposit_handler_times) AS deposit_times");
		
		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE station_id=:stationId");
		Map paramMap = MixUtil.newHashMap("stationId", stationId);

		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			sql_sb.append(" AND account =:account");
		}
		
		if (StringUtil.isNotEmpty(parentIds)) {

			if (StringUtil.equals(searchType, ReportParamVo.SEARCHTYPE_NEXT_ALL)) {
				paramMap.put("parentIds", parentIds + "%"); //所有下级  
			} else {
				paramMap.put("parentIds", parentIds);  //直属下级 
			}

			paramMap.put("agentId", agentId);
			sql_sb.append(" AND (parents LIKE :parentIds or account_id=:agentId)");
		}
		
//		if("on".equals(dailiSwitch)) {
//			sql_sb.append(" and account_type="+SysAccount.ACCOUNT_PLATFORM_AGENT);
//		}
		if (StringUtil.isNotEmpty(startTime)) {
			paramMap.put("begin", startTime);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(endTime)) {
			paramMap.put("end", endTime);
			sql_sb.append(" AND stat_date <:end");
		}
		if(StringUtil.isNotEmpty(reportType)) {
			paramMap.put("reportType", reportType);
			sql_sb.append(" AND report_type =:reportType");
		}
		
		sql_sb.append(" GROUP BY account_id,account,agent_id,agent_name,account_type");

      sql_sb.append(") a");
      sql_sb.append(" left join sys_account s on a.account_id = s.id");
      if(StringUtil.isNotEmpty(sortName)) {
    	  String underline = replaceUpperToUnderline(sortName);
    	  sql_sb.append(" order by ").append(underline).append(" "+sortOrder);
      }else {
    	  sql_sb.append(" order by team_profit desc");
      }
      
      
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_amount", "depositAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.manual_deposit_amount", "manualDepositAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.manual_withdraw_amount", "manualWithdrawAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_gift_amount", "depositGiftAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.withdraw_gift_amount", "withdrawGiftAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.register_gift_amount", "registerGiftAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.withdraw_amount", "withdrawAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.rebate_agent_amount", "rebateAgentAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.betting_amount", "bettingAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.award_amount", "awardAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.rebate_amount", "rebateAmountTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.betting_amount-a.award_amount-a.rebate_agent_amount",
				"profitTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"a.betting_amount-a.award_amount-a.rebate_agent_amount-a.rebate_amount", "teamProfitTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.deposit_times", "depositTimesTotal"));
		Page<Map> page = super.page2CamelMap(sql_sb.toString(), paramMap, aggs);
		 return page;
	}
	
	public static void main(String[] args) {
		String a = "bettingAmount";
		
		for (int i = 0; i < a.length(); i++) {
			char charAt = a.charAt(i);
			boolean upperCase = Character.isUpperCase(charAt);
			
			if(upperCase) {
				char upperCase2 = Character.toUpperCase(charAt);
				
			}
			
			System.out.println(charAt);
		}
	}
	
	public Page<Map> findTeamStatistic2(Long stationId, String account, String parents, Long agentIdL, Long selfId,Date startTime,
			Date endTime, Long searchType,Long reportType) {
		int level = 0;
		if(StringUtil.isNotEmpty(parents) || StringUtil.isNotEmpty(agentIdL)){
			
			String[] ps = parents.split(",");
			
			for (int i = 0; i < ps.length; i++) {
				if(ps[i].equalsIgnoreCase(agentIdL.toString())) {
					level = i+1;
					break;
				}
			}
		}
		String levels = level+"";
		StringBuffer sql_sb = new StringBuffer();
		Map paramMap = MixUtil.newHashMap("stationId", stationId);
		
		sql_sb.append("SELECT agent_id,account_type,SUM(deposit_amount) AS deposit_amount, SUM(manual_deposit_amount) AS manual_deposit_amount, SUM(manual_withdraw_amount) AS manual_withdraw_amount, SUM(deposit_gift_amount) AS deposit_gift_amount, SUM(register_gift_amount) AS register_gift_amount, SUM(withdraw_amount) AS withdraw_amount, SUM(rebate_agent_amount) AS rebate_agent_amount, SUM(betting_amount) AS betting_amount, SUM(award_amount) AS award_amount, SUM(rebate_amount) AS rebate_amount,SUM(deposit_times) AS deposit_times, SUM(profit) as profit,SUM(team_profit) as team_profit");
		sql_sb.append(" from (");
		sql_sb.append(" select a.*,(betting_amount-award_amount-rebate_agent_amount) AS profit,(betting_amount-award_amount-rebate_agent_amount-rebate_amount) AS team_profit from (SELECT ");
		
		//两个都是空 index的时候
		if(StringUtil.isEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" split_part(m.parents, ',', 2)::int4 as agent_id");
		}
		//传入了一个最上级代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" split_part(m.parents, ',', 3)::int4 as agent_id");
		}
		//除最上级以下的所有代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isNotEmpty(parents)) {
			
			if("2".equals(levels)) {
				sql_sb.append(" split_part(m.parents, ',',4)::int4 as agent_id");
			}
			if("3".equals(levels)) {
				sql_sb.append(" split_part(m.parents, ',',5)::int4 as agent_id");
			}
			if("4".equals(levels)) {
				sql_sb.append(" split_part(m.parents, ',',6)::int4 as agent_id");
			}
			if("5".equals(levels)) {
				sql_sb.append(" split_part(m.parents, ',',7)::int4 as agent_id");
			}
			if("6".equals(levels)) {
				sql_sb.append(" split_part(m.parents, ',',8)::int4 as agent_id");
			}
		}
		
		sql_sb.append(",4 as account_type,SUM(deposit_amount) AS deposit_amount, SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_amount, SUM(-withdraw_artificial) AS manual_withdraw_amount, SUM(deposit_gift_amount) AS deposit_gift_amount, SUM(register_gift_amount) AS register_gift_amount, SUM(-withdraw_amount) AS withdraw_amount, SUM(proxy_rebate_amount) AS rebate_agent_amount, SUM(-lottery_bet_amount-sys_lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sports_bet_amount+egame_bet_amount+hunter_bet_amount+real_bet_amount+third_sports_bet_amount+third_lottery_bet_amount+chess_bet_amount) AS betting_amount, SUM(lottery_win_amount+sys_lottery_win_amount+sf_mark_six_win_amount+mark_six_win_amount+sports_win_amount+egame_win_amount+hunter_win_amount+real_win_amount+third_sports_win_amount+third_lottery_win_amount+chess_win_amount) AS award_amount, SUM(lottery_rebate_amount+sys_lottery_rebate_amount+mark_six_rebate_amount+sf_mark_six_rebate_amount+sports_rebate_amount+egame_rebate_amount+hunter_rebate_amount+real_rebate_amount+third_sports_rebate_amount+third_lottery_rebate_amount+chess_rebate_amount) AS rebate_amount,SUM(deposit_times) AS deposit_times FROM sys_account_daily_money m inner join sys_account a on m.agent_id = a.id WHERE ");
		sql_sb.append(" a.station_id=:stationId ");
		
		if (StringUtil.isNotEmpty(startTime)) {
			paramMap.put("begin", startTime);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(endTime)) {
			paramMap.put("end", endTime);
			sql_sb.append(" AND stat_date <:end");
		}
		//两个都是空 index的时候
		if(StringUtil.isEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" and split_part(m.parents, ',', 2) != '' ");
		}
		//传入了一个最上级代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" and split_part(m.parents, ',', 3) != '' ");
		}
		//除最上级以下的所有代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isNotEmpty(parents)) {
			
			if("2".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 4) != '' ");
			}
			if("3".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 5) != '' ");
			}
			if("4".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 6) != '' ");
			}
			if("5".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 7) != '' ");
			}
			if("6".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 8) != '' ");
			}
			
		}
		
		//传入了一个最上级代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" and split_part(m.parents, ',', 2) = :agentId1 ");
			String agentId1 = selfId.toString();
			paramMap.put("agentId1", agentId1);
		}
		//除最上级以下的所有代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isNotEmpty(parents)) {
			String agentId1 = selfId.toString();
			paramMap.put("agentId1", agentId1);

			if("2".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 3) = :agentId1 ");
			}
			if("3".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 4) = :agentId1 ");
			}
			if("4".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 5) = :agentId1 ");
			}
			if("5".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 6) = :agentId1 ");
			}
			if("6".equals(levels)) {
				sql_sb.append(" and split_part(m.parents, ',', 7) = :agentId1 ");
			}
			
		}
		if(StringUtil.isNotEmpty(reportType)) {
			sql_sb.append(" and m.report_type = :reportType");
			paramMap.put("reportType", reportType);
		}
		//index的时候
		if(StringUtil.isEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" GROUP BY split_part(m.parents, ',', 2))a ");
		}
		//最上级代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isEmpty(parents)) {
			sql_sb.append(" GROUP BY split_part(m.parents, ',', 3))a ");
		}
		//除最上级以外所有代理
		if(StringUtil.isNotEmpty(account) && StringUtil.isNotEmpty(parents)) {
			
			if("2".equals(levels)) {
				sql_sb.append(" GROUP BY split_part(m.parents, ',', 4))a ");
			}
			if("3".equals(levels)) {
				sql_sb.append(" GROUP BY split_part(m.parents, ',', 5))a ");
			}
			if("4".equals(levels)) {
				sql_sb.append(" GROUP BY split_part(m.parents, ',', 6))a ");
			}
			if("5".equals(levels)) {
				sql_sb.append(" GROUP BY split_part(m.parents, ',', 7))a ");
			}
			if("6".equals(levels)) {
				sql_sb.append(" GROUP BY split_part(m.parents, ',', 8))a ");
			}
			
		}
		
		sql_sb.append(" union");
		sql_sb.append(" select b.*,(betting_amount-award_amount-rebate_agent_amount) AS profit,(betting_amount-award_amount-rebate_agent_amount-rebate_amount) AS team_profit from (SELECT account_id ,account_type,SUM(deposit_amount) AS deposit_amount, SUM(deposit_artificial+sys_api_deposit_amount) AS manual_deposit_amount, SUM(-withdraw_artificial) AS manual_withdraw_amount, SUM(deposit_gift_amount) AS deposit_gift_amount, SUM(register_gift_amount) AS register_gift_amount, SUM(-withdraw_amount) AS withdraw_amount, SUM(proxy_rebate_amount) AS rebate_agent_amount, SUM(-lottery_bet_amount-sys_lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sports_bet_amount+egame_bet_amount+hunter_bet_amount+real_bet_amount+third_sports_bet_amount+third_lottery_bet_amount+chess_bet_amount) AS betting_amount, SUM(lottery_win_amount+sys_lottery_win_amount+sf_mark_six_win_amount+mark_six_win_amount+sports_win_amount+egame_win_amount+hunter_win_amount+real_win_amount+third_sports_win_amount+third_lottery_win_amount+chess_win_amount) AS award_amount, SUM(lottery_rebate_amount+sys_lottery_rebate_amount+mark_six_rebate_amount+sf_mark_six_rebate_amount+sports_rebate_amount+egame_rebate_amount+hunter_rebate_amount+real_rebate_amount+third_sports_rebate_amount+third_lottery_rebate_amount+chess_rebate_amount) AS rebate_amount,SUM(deposit_times) AS deposit_times FROM sys_account_daily_money WHERE ");
		sql_sb.append(" station_id=:stationId ");
		
		if (StringUtil.isNotEmpty(startTime)) {
			paramMap.put("begin", startTime);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(endTime)) {
			paramMap.put("end", endTime);
			sql_sb.append(" AND stat_date <:end");
		}
		if(searchType!=null && searchType==2){
			sql_sb.append(" and parents like :agentId2 ");
			Long agentId2 = selfId;
			paramMap.put("agentId2", "%,"+agentId2+",%");
		}else{
			if(StringUtil.isEmpty(account) && StringUtil.isEmpty(parents)) {
				sql_sb.append(" and agent_id = 0 ");
			}else {
				//TO　DO
				sql_sb.append(" and agent_id = :agentId2 ");
				Long agentId2 = selfId;
				paramMap.put("agentId2", agentId2);
			}
		}


		if(StringUtil.isNotEmpty(reportType)) {
			sql_sb.append(" and report_type = :reportType");
			paramMap.put("reportType", reportType);
		}
		sql_sb.append(" and account_type = 4 ");
		sql_sb.append(" GROUP BY account_id,account_type) b ");
		sql_sb.append(" ) a group by a.agent_id,a.account_type");
		

		Page<Map> page = super.page2CamelMap(sql_sb.toString(),paramMap);
		 return page;
	}

	public Map getComprehensiveCount(Long stationId, Date countDate,Long reportType) {

		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT SUM(deposit_amount) AS deposit_amount,");
		sql_sb.append(" SUM(-withdraw_amount) AS withdraw_amount,");
		sql_sb.append(" SUM(proxy_rebate_amount) AS agent_rebate,");
		sql_sb.append(" SUM(CASE WHEN deposit_times >0 OR deposit_artificial >0 THEN 1 ELSE 0 END) AS deposit_count,");
		sql_sb.append(" SUM(first_deposit) AS first_deposit,");

		sql_sb.append(" SUM(-lottery_bet_amount");
		sql_sb.append("-sys_lottery_bet_amount");
		sql_sb.append("-sf_mark_six_bet_amount");
		sql_sb.append("-mark_six_bet_amount");
		sql_sb.append("-sports_bet_amount");
		sql_sb.append("+egame_bet_amount");
		sql_sb.append("+hunter_bet_amount");
		sql_sb.append("+real_bet_amount");
		sql_sb.append("+chess_bet_amount");
		sql_sb.append("+third_lottery_bet_amount");
		sql_sb.append("+third_sports_bet_amount");
		sql_sb.append(") AS bet_amount,");
		sql_sb.append(" SUM(lottery_win_amount");
		sql_sb.append("+sys_lottery_win_amount");
		sql_sb.append("+sf_mark_six_win_amount");
		sql_sb.append("+mark_six_win_amount");
		sql_sb.append("+sports_win_amount");
		sql_sb.append("+egame_win_amount");
		sql_sb.append("+hunter_win_amount");
		sql_sb.append("+real_win_amount");
		sql_sb.append("+chess_win_amount");
		sql_sb.append("+third_lottery_win_amount");
		sql_sb.append("+third_sports_win_amount");
		sql_sb.append(") AS award_amount,");

		sql_sb.append(" SUM(lottery_rebate_amount");
		sql_sb.append("+sys_lottery_rebate_amount");
		sql_sb.append("+mark_six_rebate_amount");
		sql_sb.append("+sf_mark_six_rebate_amount");
		sql_sb.append("+sports_rebate_amount");
		sql_sb.append("+egame_rebate_amount");
		sql_sb.append("+hunter_rebate_amount");
		sql_sb.append("+real_rebate_amount");
		sql_sb.append("+chess_rebate_amount");
		sql_sb.append("+third_lottery_rebate_amount");
		sql_sb.append("+third_sports_rebate_amount");
		sql_sb.append(") AS member_rebate");

		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE station_id=:stationId AND stat_date = :countDate ");
		
		Map<Object, Object> paramsMap = MixUtil.newHashMap("stationId", stationId, "countDate", countDate);
		
		if(StringUtil.isNotEmpty(reportType) && 0!=reportType) {
			sql_sb.append(" and report_type=:reportType");
			paramsMap.put("reportType", reportType);
		}
		sql_sb.append(" GROUP BY stat_date");
		
		return super.selectSingleCamelMap(sql_sb.toString(),paramsMap);
	}

	public List<Map> getMonthReports(Long stationId, Date startTime, Date endTime,Long reportType) {
		StringBuilder sql_sb = new StringBuilder("SELECT TO_CHAR(stat_date,'YYYY-MM') AS ym,");
		sql_sb.append("SUM(-lottery_bet_amount");
		sql_sb.append("-sys_lottery_bet_amount");
		sql_sb.append("-sf_mark_six_bet_amount");
		sql_sb.append("-mark_six_bet_amount");
		sql_sb.append("-sports_bet_amount");
		sql_sb.append("+egame_bet_amount");
		sql_sb.append("+hunter_bet_amount");
		sql_sb.append("+real_bet_amount");
		sql_sb.append("+third_sports_bet_amount");
		sql_sb.append("+third_lottery_bet_amount");
		sql_sb.append("+chess_bet_amount");

		sql_sb.append(") AS bet_amount,");
		sql_sb.append("SUM(-lottery_bet_amount - lottery_win_amount - sys_lottery_bet_amount - sys_lottery_win_amount");
		sql_sb.append("- sf_mark_six_bet_amount - sf_mark_six_win_amount) AS lottery_profit,");
		sql_sb.append("SUM(-mark_six_bet_amount-mark_six_win_amount) AS mark_six_profit,");
		sql_sb.append("SUM(-sports_bet_amount-sports_win_amount) AS sport_profit,");
		sql_sb.append("SUM(egame_bet_amount+hunter_bet_amount-egame_win_amount-hunter_win_amount) AS electron_profit,");
		sql_sb.append("SUM(real_bet_amount-real_win_amount) AS real_game_profit,");
		sql_sb.append("SUM(third_sports_bet_amount-third_sports_win_amount) AS third_sports_game_profit,");
		sql_sb.append("SUM(third_lottery_bet_amount-third_lottery_win_amount) AS third_lottery_game_profit,");
		sql_sb.append("SUM(chess_bet_amount-chess_win_amount) AS chess_game_profit");

		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE stat_date <:endTime AND stat_date>=:startTime AND station_id=:stationId ");
		Map map = MixUtil.newHashMap("startTime", startTime, "endTime", endTime, "stationId", stationId);
		
		if(StringUtil.isNotEmpty(reportType) && 0!=reportType) {
			sql_sb.append(" and report_type = :reportType");
			map.put("reportType", reportType);
		}
		sql_sb.append(" GROUP BY ym");
		
		return selectCamelListMap(sql_sb.toString(), map);
	}

	// 以下是数据转换

	public Date minBizDatetime() {
		String datet = super.queryForString("select min(biz_datetime) from mny_money_record;");
		Date d = DateUtil.toDate(datet);
		if (d == null) {
			d = new Date();
		}
		return d;
	}

	public List<SysAccountDailyMoney> getAccountDailyMoneyFromMnyMoneyRecord(Long stationId, Date startDate,
			Date endDate) {
		Map paramMap = MixUtil.newHashMap("stationId", stationId, "startDate", startDate, "endDate", endDate);
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(" SELECT account_id");
		// 提款
		sql_sb.append(",sum(CASE WHEN type IN(3,4) THEN money ELSE 0 END) AS withdraw_amount");
		sql_sb.append(",sum(CASE WHEN type=3 THEN -1 WHEN type=4 THEN 1 ELSE 0 END) AS withdraw_times");
		sql_sb.append(",sum(CASE WHEN type=2 THEN money ELSE 0 END) AS withdraw_artificial");
		// 存款
		sql_sb.append(",sum(CASE WHEN type IN(5,6,7) THEN money ELSE 0 END) AS deposit_amount");
		sql_sb.append(",sum(CASE WHEN type IN(5,6,7) THEN 1 ELSE 0 END) AS deposit_times");
		sql_sb.append(",sum(CASE WHEN type=1 THEN money ELSE 0 END) AS deposit_artificial");
		sql_sb.append(",sum(CASE WHEN type=80 THEN money ELSE 0 END) AS deposit_gift_amount");
		sql_sb.append(",sum(CASE WHEN type=80  THEN 1 ELSE 0 END) AS deposit_gift_times");
		// 彩票
		sql_sb.append(",sum(CASE WHEN type IN(130,132) THEN money ELSE 0 END) AS lottery_bet_amount");
		sql_sb.append(",sum(CASE WHEN type IN(131,134) THEN money ELSE 0 END) AS lottery_win_amount");
		// 六合
		sql_sb.append(",sum(CASE WHEN type IN(140,143) THEN money ELSE 0 END) AS mark_six_bet_amount");
		sql_sb.append(",sum(CASE WHEN type in(141,142) THEN money ELSE 0 END) AS mark_six_win_amount");
		// 真人 电子
		// 体育
		sql_sb.append(",sum(CASE WHEN type IN(201,203) THEN money ELSE 0 END) AS sports_bet_amount");
		sql_sb.append(",sum(CASE WHEN type IN(202,204) THEN money ELSE 0 END) AS sports_win_amount");
		// 代理返点
		sql_sb.append(",sum(CASE WHEN type IN(11,12,13,14) THEN money ELSE 0 END) AS proxy_rebate_amount");

		sql_sb.append(" FROM mny_money_record");
		sql_sb.append(" WHERE station_id =:stationId AND biz_datetime >= :startDate AND biz_datetime < :endDate");
		sql_sb.append(" GROUP BY account_id");
		return query2Model(sql_sb.toString(), paramMap);
	}

	public Map<String, SysAccountDailyMoney> getAccountDailyMoneyFromMemberRollbackRecord(Long stationId,
			Date startDate, Date endDate) {
		Map paramMap = MixUtil.newHashMap("stationId", stationId, "startDate", startDate, "endDate", endDate);
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append(" SELECT account_id,to_char(bet_order_datetime,'YYYY-MM-DD') as stat_date");
		sql_sb.append(",sum(CASE WHEN bet_type=1 THEN roll_back_money ELSE 0 END) AS lottery_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=2 THEN roll_back_money ELSE 0 END) AS real_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=3 THEN roll_back_money ELSE 0 END) AS egame_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=4 THEN roll_back_money ELSE 0 END) AS sports_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=5 THEN roll_back_money ELSE 0 END) AS mark_six_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=10 THEN roll_back_money ELSE 0 END) AS chess_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=91 THEN roll_back_money ELSE 0 END) AS third_lottery_rebate_amount");
		sql_sb.append(",sum(CASE WHEN bet_type=99 THEN roll_back_money ELSE 0 END) AS third_sports_rebate_amount");
		sql_sb.append(" from member_roll_back_record where station_id =:stationId AND bet_order_datetime>=:startDate ");
		sql_sb.append("and bet_order_datetime<:endDate and roll_back_status=4 GROUP BY account_id,stat_date");
		List<SysAccountDailyMoney> list = query2Model(sql_sb.toString(), paramMap);
		Map<String, SysAccountDailyMoney> map = new HashMap<>();
		if (list != null && !list.isEmpty()) {
			for (SysAccountDailyMoney dm : list) {
				map.put(dm.getAccountId() + "_" + DateUtil.toDateStr(dm.getStatDate()), dm);
			}
		}
		return map;
	}

	public Map getLastSucDepositHandlerData(Long userId, Date startTime) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT SUM(deposit_handler_artificial) AS deposit_handler_artificial,");
		sql_sb.append("SUM(deposit_handler_artificial_times) AS deposit_handler_artificial_times,");
		sql_sb.append("SUM(deposit_amount) AS deposit_amount,");
		sql_sb.append("SUM(deposit_times) AS deposit_times,");
		sql_sb.append("SUM(-withdraw_amount) AS withdraw_amount,");
		sql_sb.append("SUM(withdraw_times) AS withdraw_times");
		sql_sb.append(" FROM sys_account_daily_money");
		sql_sb.append(" WHERE account_id=:userId");
		Map map = MixUtil.newHashMap("userId", userId);
		if (StringUtil.isNotEmpty(startTime)) {
			sql_sb.append(" AND stat_date>=:startTime");
			map.put("startTime", startTime);
		}
		sql_sb.append(" GROUP BY account_id");

		return selectSingleCamelMap(sql_sb.toString(), map);
	}

	public List<Map> findByLotteryAndMarkWinTotal(Long stationId, Date statDate, Date endDate) {
		// StringBuilder sql = new StringBuilder("SELECT
		// daily.lottery_bet_amount+daily.lottery_win_amount+daily.mark_six_bet_amount+daily.mark_six_win_amount
		// AS result,daily.account AS account FROM sys_account_daily_money AS
		// daily ");
		// sql.append(" WHERE station_id=:stationId and stat_date=:date ");
		// sql.append(" and
		// (daily.lottery_bet_amount+daily.lottery_win_amount+daily.mark_six_bet_amount+daily.mark_six_win_amount>0)
		// ");
		// sql.append(" ORDER BY result DESC LIMIT 6");
		// return super.selectSingleColList(sql.toString(),
		// MixUtil.newHashMap("stationId",stationId,"date",date),BigDecimal.class);
		StringBuilder sql = new StringBuilder(
				"SELECT daily.account AS account,(sum(daily.lottery_bet_amount)+sum(daily.lottery_win_amount)+sum(daily.mark_six_bet_amount)+sum(daily.mark_six_win_amount)) AS result from sys_account_daily_money as daily where ");
		sql.append(" daily.station_id=:stationId and daily.stat_date<=:statDate and daily.stat_date>=:endDate ");
		sql.append(
				" GROUP BY daily.account HAVING sum(daily.lottery_bet_amount)+sum(daily.lottery_win_amount)+sum(daily.mark_six_bet_amount)+sum(daily.mark_six_win_amount)>0 ");
		sql.append(" ORDER BY result DESC LIMIT 6");
		return super.selectCamelListMap(sql.toString(),
				MixUtil.newHashMap("stationId", stationId, "statDate", statDate, "endDate", endDate));
	}

	/**
	 * 根据站点和投注时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		StringBuilder sb = new StringBuilder("DELETE FROM sys_account_daily_money where");
		Map map = MixUtil.newHashMap();
		sb.append(" stat_date <= :createTime");
		sb.append(" AND station_id = :stationId");
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(sb.toString(), map);
	}

	public List<Map> getGlobalReportForAdmin(Date start, Date end) {
		StringBuilder sb = new StringBuilder("select station_id,sum(-lottery_bet_amount) as lot_bet_amount");
		sb.append(",sum(lottery_win_amount) as lot_win_amount");
		sb.append(",sum(-mark_six_bet_amount) as six_bet_amount");
		sb.append(",sum(mark_six_win_amount) as six_win_amount");
		// sb.append(",sum(-sys_lottery_bet_amount) as sys_lot_bet_amount");
		// sb.append(",sum(sys_lottery_win_amount) as sys_lot_win_amount");
		// sb.append(",sum(-sf_mark_six_bet_amount) as sf_six_bet_amount");
		// sb.append(",sum(sf_mark_six_win_amount) as sf_six_win_amount");
		sb.append(",SUM(deposit_amount) AS deposit_total");
		sb.append(",SUM(-withdraw_amount) AS withdraw_total");
		sb.append(",SUM(deposit_artificial) AS manual_deposit_total");
		sb.append(",SUM(-withdraw_artificial) AS manual_withdraw_total");
		sb.append(",SUM(deposit_handler_artificial) AS deposit_handler_artificial");
		sb.append(",SUM(sports_bet_amount) AS sports_bet_amount");
		sb.append(",SUM(sports_win_amount) AS sports_win_amount");
		sb.append(" from sys_account_daily_money");
		sb.append(" where stat_date >= :start");
		sb.append(" and stat_date < :end");
		sb.append(" group by station_id");
		Map<Object, Object> map = MixUtil.newHashMap();
		map.put("start", start);
		map.put("end", end);
		return selectCamelListMap(sb.toString(), map);
	}

	public Map getRealAndEgame(Long accountId, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sb = new StringBuilder("select sum(real_bet_amount) as real_total,");
		sb.append("sum(real_win_amount) as real_award,");
		sb.append("sum(egame_bet_amount) as egame_total,");
		sb.append("sum(egame_win_amount) as egame_award,");
		sb.append("sum(chess_bet_amount) as chess_total,");
		sb.append("sum(chess_win_amount) as chess_award");
		sb.append(" from sys_account_daily_money");
		sb.append(" where stat_date>=:beginDate and stat_date<=:endDate");
		sb.append(" and station_id=:stationId and account_id=:accountId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("accountId", accountId);
		return selectSingleCamelMap(sb.toString(), map);
	}

	public Map getLotteryAmountCount(String account, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sb = new StringBuilder(
				"select sum(-lottery_bet_amount-sys_lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount) as bet_amount,");
		sb.append(
				"sum(lottery_win_amount+sys_lottery_win_amount+sf_mark_six_win_amount+mark_six_win_amount) as win_amount,");
		sb.append("sum(deposit_amount) deposit_amount,");
		sb.append("sum(withdraw_amount) withdraw_amount,");
		sb.append("sum(active_award_amount)active_amount,");
		sb.append(
				"sum(lottery_rebate_amount+sys_lottery_rebate_amount+mark_six_rebate_amount+sf_mark_six_rebate_amount) rebate_amount");
		sb.append(" from sys_account_daily_money");
		sb.append(
				" where account=:account and station_id=:stationId and stat_date>=:beginDate and stat_date<=:endDate");
		Map<String, Object> map = new HashMap<>();
		map.put("account", account);
		map.put("stationId", stationId);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		return selectSingleCamelMap(sb.toString(), map);
	}

	public List<SysAccountDailyMoney> getRealAndEgameBetAmountForAgent(Long stationId, Date beginDate, Date endDate) {
		StringBuilder sb = new StringBuilder("select agent_id,stat_date,sum(real_bet_amount) as real_bet_amount,");
		sb.append("sum(egame_bet_amount+hunter_bet_amount) as egame_bet_amount,");
		sb.append("sum(chess_bet_amount) as chess_bet_amount");
		sb.append(" from sys_account_daily_money where station_id=:stationId");
		sb.append(" and stat_date>=:beginDate and stat_date<:endDate");
		sb.append(" group by agent_id ,stat_date");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		return query2Model(sb.toString(), map);
	}

	public void syncParents(Long accountId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account_daily_money m SET parents = a.parents");
		sql_sb.append(" FROM sys_account a ");
		sql_sb.append(" WHERE a.id = m.account_id AND a.id = :accountId;");
		super.update(sql_sb.toString(), MixUtil.newHashMap("accountId", accountId));
	}

	public void synDailiData(Long stationId, Date end, Date begin) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account_daily_money m SET parents = a.parents,agent_id = a.agent_id,agent_name=a.agent_name");
		sql_sb.append(" FROM sys_account a ");
		sql_sb.append(" WHERE a.id = m.account_id and a.station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (StringUtil.isNotEmpty(begin)) {
			sql_sb.append(" AND m.stat_date >= :startTime");
			map.put("startTime", begin);
		}
		if (StringUtil.isNotEmpty(end)) {
			sql_sb.append(" AND m.stat_date <= :end");
			map.put("end", end);
		}
		super.update(sql_sb.toString(), map);
	}
	

	public Map findBetCountAndFirstDepositTotal(String accountId,String children,String stationId,Date begin,Date end,Long reportType) {
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		
		sb.append(" COUNT(DISTINCT CASE WHEN egame_bet_amount <>0 OR ");
		sb.append(" hunter_bet_amount <>0 OR ");
		sb.append(" sports_bet_amount <>0 OR ");
		sb.append(" lottery_bet_amount <>0 OR ");
		sb.append(" sys_lottery_bet_amount <>0 OR ");
		sb.append(" mark_six_bet_amount <>0 OR ");
		sb.append(" sf_mark_six_bet_amount <>0 OR ");
		sb.append(" third_sports_bet_amount <>0 OR ");
		sb.append(" third_lottery_bet_amount <>0 OR ");
		sb.append(" chess_bet_amount <>0 OR ");
		sb.append(" real_bet_amount <>0");
		sb.append(" THEN account_id ELSE NULL END) AS bet_count_total");
		
		sb.append(",SUM(first_deposit) AS first_deposit_total FROM sys_account_daily_money WHERE station_id=:stationId AND (account_id = :accountId OR parents LIKE :children)");
		
		long accountIdL = Long.parseLong(accountId);
		long stationIdL = Long.parseLong(stationId);
		
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationIdL);
		map.put("accountId", accountIdL);
		map.put("children", children+"%"); //
		
		if (StringUtil.isNotEmpty(begin)) {
			map.put("begin", begin);
			sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			map.put("end", end);
			sb.append(" AND stat_date <:end");
		}
		if(Validator.isNotNull(reportType)) {
			map.put("reportType", reportType);
			sb.append(" AND report_type =:reportType");
		}
		
		Map camelMap = selectSingleCamelMap(sb.toString(), map);
		return camelMap;
	}
	
	public void fixUpLowRelationship(Long stationId) {
		StringBuilder sb = new StringBuilder();
		sb.append("update sys_account a set parents = COALESCE((Case when trim(b.parents) =''");
		sb.append(" then null ");
		sb.append(" ELSE b.parents ");
		sb.append(" end),',')||b.id||',', parent_names = COALESCE((Case when trim(b.parent_names) ='' ");
		sb.append(" then null ");
		sb.append(" ELSE b.parent_names");
		sb.append(" end),',')||b.account||',' from sys_account b where a.agent_id = b.id");
		sb.append(" and a.station_id =:stationId;");
		
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		
		super.update(sb.toString(), map);
		
		
	}
	
	public Map getRebateAgentTotal(ReportParamVo paramVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT  SUM(proxy_rebate_amount) AS rebate_agent_total FROM sys_account_daily_money WHERE station_id=:stationId ");
		
		Long stationId = paramVo.getStationId();
		Long reportType = paramVo.getReportType();
		Date begin = paramVo.getBegin();
		Date end = paramVo.getEnd();
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Boolean searchSelf = paramVo.getSearchSelf();
		String filterAccount = paramVo.getFilterAccount();
		Map paramMap = MixUtil.newHashMap("stationId", stationId);

		if (paramVo.getAccountId() != null) {
			paramMap.put("accountId", paramVo.getAccountId());
			sql_sb.append(" AND account_id =:accountId");
		} else {
			if (StringUtil.isNotEmpty(account)) {
				paramMap.put("account", account);
				sql_sb.append(" AND account =:account");
			}
		}
		if (StringUtil.isNotEmpty(children)) {
			if (!StationUtil.isDailiStation()
					|| StringUtil.equals(paramVo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT_ALL)) {
				paramMap.put("children", children + "%");
			} else {
				paramMap.put("children", children);
			}

			if (StringUtil.isNotEmpty(searchSelf) && searchSelf) {
				sql_sb.append(" AND (account_id = :selfId OR parents LIKE :children)");
				paramMap.put("selfId", paramVo.getSelfId());
			} else {
				sql_sb.append(" AND parents LIKE :children");
			}
		}
		
		if (StringUtil.isNotEmpty(begin)) {
			paramMap.put("begin", begin);
			sql_sb.append(" AND stat_date >=:begin");
		}
		if (StringUtil.isNotEmpty(end)) {
			paramMap.put("end", end);
			sql_sb.append(" AND stat_date <=:end");
		}
		return super.selectSingleCamelMap(sql_sb.toString(), paramMap);
	}

	public void updateBalaceGemAmount(SysAccountDailyMoney dailyMoney) {
		StringBuilder sql = new StringBuilder("update sys_account_daily_money set balance_gem_amount=:balanceGemAmount");
		sql.append(" where account_id=:accountId and stat_date=:statDate");
		Map<String, Object> map = new HashMap<>();
		map.put("balanceGemAmount", getBigdecimalByDefaultZero(dailyMoney.getBalanceGemAmount()));
		map.put("accountId", dailyMoney.getAccountId());
		map.put("statDate", dailyMoney.getStatDate());
		update(sql.toString(), map);
	}
	
	/**
	 * 取出用户的盈亏和总存总取
	 * @param stationId
	 * @param account
	 * @param begin
	 * @param end
	 * @return
	 */
	public Map getProfit(Long stationId,String account,Date begin,Date end) {
		StringBuilder sql = new StringBuilder();
		sql.append("select a.account,b.total_deposit,b.total_withdraw,(a.betting_amount-a.award_amount-a.rebate_agent_amount) AS profit from (");
		sql.append(" select account,");
		sql.append(" SUM(-lottery_bet_amount-sys_lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sports_bet_amount+egame_bet_amount+hunter_bet_amount+real_bet_amount+third_sports_bet_amount+third_lottery_bet_amount+chess_bet_amount) AS betting_amount,");
		sql.append(" SUM(lottery_win_amount+sys_lottery_win_amount+sf_mark_six_win_amount+mark_six_win_amount+sports_win_amount+egame_win_amount+hunter_win_amount+real_win_amount+third_sports_win_amount+third_lottery_win_amount+chess_win_amount) AS award_amount,");
		sql.append(" SUM(proxy_rebate_amount) AS rebate_agent_amount ");
		sql.append(" from sys_account_daily_money");
		sql.append(" where 1 =1 ");
		
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		if(StringUtil.isNotEmpty(stationId)) {
			sql.append(" and station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if(StringUtil.isNotEmpty(account)) {
			sql.append(" and account = :account");
			paramMap.put("account", account);
		}
		if(StringUtil.isNotEmpty(begin)) {
			sql.append(" and stat_date >= :begin");
			paramMap.put("begin", begin);
		}
		if(StringUtil.isNotEmpty(end)) {
			sql.append(" and stat_date < :end");
			paramMap.put("end", end);
		}
		sql.append(" GROUP BY account )a ");
		sql.append(" join");
		sql.append(" (select account,sum(deposit_amount+deposit_artificial+sys_api_deposit_amount) as total_deposit,sum(-withdraw_amount) as total_withdraw");
		sql.append(" from sys_account_daily_money");
		sql.append(" where 1 =1");
		if(StringUtil.isNotEmpty(account)) {
			sql.append(" and account = :account");
			paramMap.put("stationId", stationId);
		}
		if(StringUtil.isNotEmpty(stationId)) {
			sql.append(" and station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		sql.append(" GROUP BY account) b on a.account = b.account");
		return super.selectSingleCamelMap(sql.toString(), paramMap);
	}

	public Page getRiskEsportsReport(ReportParamVo paramVo) {
		if (!StringUtils.startsWith(paramVo.getSortName(), "esports")) {
			paramVo.setSortName(null);
		}
		StringBuilder sql = new StringBuilder("SELECT a.account_id,a.account,a.account_type,i.user_name,");
		sql.append(" SUM(esports_rebate_amount) AS esports_rebate_total,");
		sql.append(" SUM(esports_bet_amount) AS esports_bet_total,");
		sql.append(" SUM(esports_win_amount) AS esports_win_total,");
		sql.append(" SUM(esports_bet_times) AS esports_bet_times_total,");
		sql.append(" SUM(esports_win_times) AS esports_win_times_total");
		StringBuilder sql_where = new StringBuilder(" FROM sys_account_daily_money a LEFT JOIN sys_account_info i ON a.account_id = i.account_id WHERE esports_bet_amount>0 and ");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.esports_rebate_total", "esportsRebateTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.esports_bet_total", "esportsBetTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.esports_win_total", "esportsWinTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.esports_bet_times_total", "esportsBetTimesTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "a.esports_win_times_total", "esportsWinTimesTotal"));
		return getRiskReport1(paramVo, sql, sql_where, aggs);
	}

	/**
	 * 针对插入处理 同步返回新增ID
	 * @param d
	 * @return
	 */
	public SysAccountDailyMoney insertOne(SysAccountDailyMoney d) {
		try {


		JdbcModel model = get();
		final JdbcColumn primaryCol = model.getPrimaryCol();
		primaryCol.setColumnName("id");
		final String sql;
		if (primaryCol != null && primaryCol.getGenerator() == 1) {
			ListParameter parameterMaker = new ListParameter();
			SqlMaker.getAddSql(model, parameterMaker);
			sql = parameterMaker.getSql();
			final List paramList = parameterMaker.toParameters(d, 2);
			JdbcDAOImpl.getLogInfo(sql, paramList);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			super.getJdbcOperations().update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = null;
					if (FrameProperites.DB_TYPE == DBType.POSTGRESQL) {
						String primaryColumn = primaryCol.getColumnName();
						ps = conn.prepareStatement(sql, new String[]{primaryColumn});
					} else {
						ps = conn.prepareStatement(sql, 1);
					}

					for(int i = 0; i < paramList.size(); ++i) {
						Object val = paramList.get(i);
						setSqlParameter(ps, i + 1, val);
					}
					return ps;
				}
			}, keyHolder);
			Integer id = (Integer)keyHolder.getKeyList().get(0).get("id");
			d.setId(id.longValue());
		} else {
			if (primaryCol != null && primaryCol.getGenerator() == 3) {
				primaryCol.setValue(d, this.getSequence(primaryCol.getSequence()));
			}
			MapParameter parameterMaker = new MapParameter();
			SqlMaker.getAddSql(model, parameterMaker);
			sql = parameterMaker.getSql();
			Map params = parameterMaker.toParameters(d, 2);
			super.update(sql, params);
		}
		return d;
		}catch (Exception e){}
		return null;
	}

	private JdbcModel get() {
		Class c = this.getGenericType(0);
		JdbcModel jdbcModel = JdbcModelSet.get(c);
		if (jdbcModel == null) {
			throw new JayFrameException(10004, new String[]{c.getName()});
		} else {
			return jdbcModel;
		}
	}
	private void setSqlParameter(PreparedStatement ps, int i, Object val) throws SQLException {
		if (val != null && FrameProperites.DB_TYPE == DBType.POSTGRESQL && val.getClass() == Date.class) {
			ps.setTimestamp(i, new Timestamp(((Date)val).getTime()));
		} else {
			ps.setObject(i, val);
		}

	}
}
