package com.game.dao;

import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SecCard;

@Repository
public class SecCardDao extends JdbcUtilImpl<SecCard>{
	
	public Page getPage(){
		String sql = "select a.*,b.name,b.floder from sec_card  a left join sys_station b on(a.station_id = b.id) order by id desc ";
		return super.page2CamelMap(sql);
	}
	
	public SecCard getCardByCode(String cardNo){
		String sql = " select * from sec_card where code = :cardNo ";
		return super.query21Model(sql,MixUtil.newHashMap("cardNo",cardNo));
	}
	
	public boolean hasBind(long userId,long cardId,long stationType){
		String sql = "select count(*) from sec_card_user where user_id = :userId and card_id = :cardId and station_type = :stationType";
		return super.queryForInt(sql,MixUtil.newHashMap("cardId",cardId,"userId",userId,"stationType",stationType)) > 0;
	}
	
	public void bindCard(long userId,long cardId,long stationType){
		String sql = "insert into sec_card_user(card_id,user_id,station_type) values(:cardId,:userId,:stationType)";
		super.update(sql, MixUtil.newHashMap("cardId",cardId,"userId",userId,"stationType",stationType));
	}
	
	public void updateBindStatus(long stationId,long cardId){
		String sql = "update sec_card set station_id = :stationId ,use_count = use_count + 1 where id = :cardId ";
		super.update(sql, MixUtil.newHashMap("stationId",stationId,"cardId",cardId));
	}
	
	public void subtrUseCount(long cardId){
		String sql = "update sec_card set use_count = use_count - 1 where id = :cardId ";
		super.update(sql, MixUtil.newHashMap("cardId",cardId));
	}
	
	public List<SecCard> getBindCards(long userId,long stationType){
		String sql = " select a.* from sec_card_user b "
				+ " left join sec_card a  on(a.id = b.card_id) "
				+ " where b.station_type = :stationType "
				+ " and b.user_id = :userId ";
		return super.query2Model(sql,MixUtil.newHashMap("userId",userId,"stationType",stationType));
	}
	public int unbind(long cardId, long userId, long stationType){
		String sql = "delete from sec_card_user where user_id = :userId and card_id = :cardId and station_type = :stationType ";
		return super.update(sql,MixUtil.newHashMap("userId",userId,"cardId",cardId,"stationType",stationType));
	}
}
