package com.game.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.ProxyMultiRebateSetting;

@SuppressWarnings("unchecked")
@Repository
public class ProxyMultiRebateSettingDao extends JdbcUtilImpl<ProxyMultiRebateSetting> {
	/**
	 * 根据投注会员账号,查询所有上级账号以及上级账号的返点数据
	 * 
	 * @param Account
	 * @param stationId
	 * @return
	 */
	public List<ProxyMultiRebateSetting> getProxyRebateSettingByAccount(String Account, Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		sql_sb.append("WITH RECURSIVE sys_account_result AS ( ");
		sql_sb.append(" SELECT");
		sql_sb.append("	sa.\"id\",sa.account,sa.agent_name,sa.agent_id,sa.station_id,sa.rate ");
		sql_sb.append(" FROM");
		sql_sb.append("	sys_account sa");
		sql_sb.append(" WHERE");
		sql_sb.append(" sa.account = :account");
		sql_sb.append(" and sa.station_id = :station_id");
		sql_sb.append(" UNION ALL");
		sql_sb.append(" SELECT");
		sql_sb.append(" sa.\"id\",sa.account,sa.agent_name,sa.agent_id,sa.station_id,sa.rate");
		sql_sb.append(" FROM");
		sql_sb.append(" sys_account sa");
		sql_sb.append(" JOIN sys_account_result sar ON sar.agent_id = sa.\"id\" ");
		sql_sb.append(" and sar.station_id = :station_id");
		sql_sb.append(" and sa.station_id = :station_id");
		sql_sb.append(" and ( sa.account_type = 4 or  sa.account_type = 2)");
		sql_sb.append(" ) SELECT");
		sql_sb.append(" sare.\"id\" AS account_id,sare.station_id as station_id,sare.account AS account,sare.agent_name AS agent_name,sare.rate as dynamic_rate,");
		sql_sb.append(" CASE WHEN pmrs.game_share IS NULL THEN 0 ELSE pmrs.game_share END AS game_share");
		sql_sb.append(" FROM ");
		sql_sb.append(" sys_account_result sare");
		sql_sb.append(" LEFT JOIN proxy_multi_rebate_setting pmrs ON sare.\"id\" = pmrs.account_id");
		sql_sb.append("	ORDER BY sare.\"id\" ASC");
		paramMap.put("account", Account);
		paramMap.put("station_id", stationId);
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public Map getChildrenMaxNum(Long accountId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT MAX(pmrs.game_share) AS game_share,MAX(pmrs.profit_share) AS profit_share,MAX(a.rate) AS game_rate ");
		sql_sb.append(" FROM sys_account a INNER JOIN proxy_multi_rebate_setting pmrs ON a.id = pmrs.account_id");
		sql_sb.append(" WHERE a.agent_id = :accountId");

		return super.selectSingleCamelMap(sql_sb.toString(), MixUtil.newHashMap("accountId", accountId));
	}
	
	public Map getAccountRebate(Long accountId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT pmrs.game_share,pmrs.profit_share ");
		sql_sb.append(" FROM proxy_multi_rebate_setting pmrs");
		sql_sb.append(" WHERE pmrs.account_id = :accountId");

		return super.selectSingleCamelMap(sql_sb.toString(), MixUtil.newHashMap("accountId", accountId));
	}
	
	public void HandldInsertPMRS(ProxyMultiRebateSetting setting) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into proxy_multi_rebate_setting (account_id,game_share,flag_active,profit_share) values (:accountId,:gameShare,:flagActive,:profitShare)");
		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("accountId", setting.getAccountId());
		params.put("gameShare", setting.getGameShare());
		params.put("flagActive", setting.getFlagActive());
		params.put("profitShare", setting.getProfitShare());
		super.update(sb.toString(), params);
	}
}
