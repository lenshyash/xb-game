package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysAnnouncementStation;
import com.game.util.StationUtil;

@Repository
public class SysAnnouncementStationDao extends JdbcUtilImpl<SysAnnouncementStation> {

	public List<String> findNotices(Long stationId, Date endTime) {
		StringBuilder sql = new StringBuilder("select content from sys_announcement m left join sys_announcement_station ms on ms.announcement_id=m.id");
		sql.append(" where m.end_datetime>:endTime and ((m.type=2 and ms.station_id=:stationId) or m.type=1)");
		return selectSingleColList(sql.toString(), MixUtil.newHashMap("stationId", stationId, "endTime", endTime), String.class);
	}

	public List<Long> findStationIds(Long announcementId) {
		return selectSingleColList("select station_id from sys_announcement_station where announcement_id=:announcementId", MixUtil.newHashMap("announcementId", announcementId), Long.class);
	}

	public void deleteSomes(Long announcementId, List<Long> stationIdList) {
		if (stationIdList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder("delete from sys_announcement_station where announcement_id=:announcementId");
		Map<String, Object> map = new HashMap<>();
		map.put("announcementId", announcementId);
		sql.append(" and(");
		for (Long sid : stationIdList) {
			sql.append(" station_id=:sid").append(sid).append(" or");
			map.put("sid" + sid, sid);
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.deleteCharAt(sql.length() - 1);
		sql.append(")");
		update(sql.toString(), map);
	}

	public void deleteByAnnouncementId(Long ancId) {
		update("delete from sys_announcement_station where announcement_id=:announcementId", MixUtil.newHashMap("announcementId", ancId));
	}
	
	public List<Map> getLastAnnouncement(Integer count){
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM sys_announcement m LEFT JOIN sys_announcement_station s");
		sql_sb.append(" ON m.id = s.announcement_id");
		sql_sb.append(" WHERE type = 1 OR (TYPE = 2 AND s.station_id = :stationId) ");
		sql_sb.append(" and m.end_datetime>:endTime");
		sql_sb.append(" ORDER BY m.create_datetime DESC LIMIT :count");
		return super.selectCamelListMap(sql_sb.toString(),
				MixUtil.newHashMap("stationId", StationUtil.getStationId(), "count", count,"endTime", new Date()));
	}
}
