package com.game.dao;

import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONArray;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.AgentWhiteIp;

@Repository
public class AgentWhiteIpDao extends JdbcUtilImpl<AgentWhiteIp>{
	
	public List<AgentWhiteIp> getStationWhiteIpList(long staionId){
		String json = CacheUtil.getCache(CacheType.STATION_WHITE_IP_LIST, staionId+"",String.class);
		if(json != null){
		 	return JSONArray.parseArray(json, AgentWhiteIp.class);
		}
		String sql = "select * from agent_white_ip where station_id = :stationId and status = "+AgentWhiteIp.STATUS_ENABLED ;
		List<AgentWhiteIp> ips = super.query2Model(sql,MixUtil.newHashMap("stationId",staionId));
		CacheUtil.addCache(CacheType.STATION_WHITE_IP_LIST, staionId+"", ips);
		return ips;
	}
	
	public Page queryPage(Long stationId){
		StringBuffer sql = new StringBuffer("select * from agent_white_ip where 1 = 1 ");
		if(Validator.isNotNull(stationId)){
			sql.append(" and station_id = :stationId ");
		}
		sql.append(" order by create_datetime desc ");
		return super.page2CamelMap(sql.toString(), MixUtil.newHashMap("stationId",stationId));
	}
}
