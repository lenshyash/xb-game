package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MnyExchangeConfig;
import com.game.model.vo.ExchangeConfigVo;

@Repository
public class MnyExchangeConfigDao extends JdbcUtilImpl<MnyExchangeConfig> {

	public Page getExchangeConfigPage(ExchangeConfigVo avo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM mny_exchange_config c");
		sql_sb.append(" WHERE c.station_id = :stationId");

		Map paramMap = MixUtil.newHashMap("stationId", avo.getStationId());
		if (Validator.isNotNull(avo.getType())) {
			sql_sb.append(" AND c.type = :type");
			paramMap.put("type", avo.getType());
		}

		if (StringUtil.isNotEmpty(avo.getName())) {
			sql_sb.append(" AND c.name LIKE :name");
			paramMap.put("name", avo.getName() + "%");
		}
		sql_sb.append(" ORDER BY id DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public MnyExchangeConfig getExchangeConfig(ExchangeConfigVo avo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM mny_exchange_config c");
		sql_sb.append(" WHERE c.station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", avo.getStationId());

		if (Validator.isNotNull(avo.getType())) {
			sql_sb.append(" AND c.type = :type");
			paramMap.put("type", avo.getType());
		}

		if (Validator.isNotNull(avo.getStatus())) {
			sql_sb.append(" AND c.status = :status");
			paramMap.put("status", avo.getStatus());
		}
		return super.query21Model(sql_sb.toString(), paramMap);
	}

	public List<MnyExchangeConfig> getProgressConfig(ExchangeConfigVo ecvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM mny_exchange_config c");
		sql_sb.append(" WHERE c.station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", ecvo.getStationId());

		if (Validator.isNotNull(ecvo.getType())) {
			sql_sb.append(" AND c.type = :type");
			paramMap.put("type", ecvo.getType());
		}

		if (Validator.isNotNull(ecvo.getStatus())) {
			sql_sb.append(" AND c.status = :status");
			paramMap.put("status", ecvo.getStatus());
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}
}
