package com.game.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.SysAccountWarning;
import com.game.model.SysMessage;
import com.game.model.vo.AccountWarningVo;
import com.game.model.vo.MessageVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class SysAccountWarningDao extends JdbcUtilImpl<SysAccountWarning> {

	public Page getPage(AccountWarningVo mvo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whes = new StringBuilder();
		StringBuilder sql_orders = new StringBuilder();
		sql_cols.append("select w.id,w.account_id,w.account,w.station_id,w.type,w.status,w.create_time,w.remark,w.modify_time,w.modifyed,w.level");
		sql_cols.append(" ,a.account_status,a.register_ip,a.last_login_ip,a.last_login_datetime,a.create_datetime as register_time");
		sql_tabs.append(" from sys_account_warning w left join sys_account a on a.id = w.account_id ");
		sql_whes.append(" WHERE w.station_id = :stationId");
		Map paramMap = MixUtil.newHashMap();
		paramMap.put("stationId", mvo.getStationId());
		if (StringUtil.isNotEmpty(mvo.getAccount())) {
			sql_whes.append(" AND w.account = :account");
			paramMap.put("account", mvo.getAccount());
		}

		if (Validator.isNotNull(mvo.getStatus())) {
			sql_whes.append(" AND w.status > :status");
			paramMap.put("status", mvo.getStatus());
		}

		if (Validator.isNotNull(mvo.getLevel())) {
			sql_whes.append(" AND w.level > :level");
			paramMap.put("level", mvo.getLevel());
		}
		
		if (Validator.isNotNull(mvo.getType())) {
			sql_whes.append(" AND w.type > :type");
			paramMap.put("type", mvo.getType());
		}
		sql_orders.append(" ORDER BY w.level desc,w.modify_time desc,w.create_time DESC");
		String sql = sql_cols.append(sql_tabs).append(sql_whes).append(sql_orders).toString();
		return super.page2CamelMap(sql, paramMap);
	}
	
	public Page getMessageCount(){
		StringBuilder sb = new StringBuilder();
		Map paramMap = MixUtil.newHashMap();
		sb.append("SELECT id FROM sys_account_warning WHERE type = :type");
		paramMap.put("type", SysMessage.TYPE_ALL);
		return super.page2CamelMap(sb.toString(), paramMap);
	}

	public List<Map> executeStationUserWarning(Date start, Long stationId) {
		StringBuilder sb = new StringBuilder();
		sb.append("select t.id,t.account,t.station_id,w.id as warn_id from ( ");
		sb.append(" select a.id,a.account,a.station_id from sys_account a ");
		sb.append(" left join mny_com_record c on c.member_id = a.id and c.status = 2 and lock_flag = 3 where ");
		sb.append(" a.station_id = :stationId and a.create_datetime > :start and a.account_type in (1,4,9)  GROUP BY a.id HAVING count(c.id) = 0 ");
		sb.append(" ) t left join mny_money_record_"+stationId+" mr on  mr.account_id = t.id and mr.type in(15,4,16,130,131,134,140,141,996,998) ");
		sb.append(" left join sys_account_warning w on w.account_id = t.id ");
		sb.append(" GROUP BY t.id,t.account,t.station_id,w.id HAVING count(mr.id)>0 ");
		Map paramMap = MixUtil.newHashMap();
		paramMap.put("start",start);
		paramMap.put("stationId",stationId);
		return super.selectCamelListMap(sb.toString(), paramMap);
	}

	public Integer getCountOfUntreated(Long stationId) {
		StringBuilder sb = new StringBuilder();
		sb.append(" select count(id) from sys_account_warning where station_id = :stationId and status = :status");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("stationId",stationId);
		paramMap.put("status",SysAccountWarning.STAUS_ENABLE);
		return super.queryForInt(sb.toString(), paramMap);
	}

	public void updStatus(SysAccountWarning account) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("UPDATE sys_account_warning AS a");
		sql_sb.append(" SET status = :status");
		sql_sb.append(" ,level =").append(SysAccountWarning.LEVEL_GENERAL);
		sql_sb.append(" WHERE a.id = :id");
		super.update(sql_sb.toString(), MixUtil.newHashMap("status",account.getStatus(),"id", account.getId()));
	}

	public void updateHighLevel(final List<Long> ids) {
		if (ids == null || ids.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder("UPDATE sys_account_warning SET level=");
		sql.append(SysAccountWarning.LEVEL_HIGH);
		sql.append(" ,status=");
		sql.append(SysAccountWarning.STAUS_ENABLE);
		sql.append(" WHERE id=?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, ids.get(i));
			}
			@Override
			public int getBatchSize() {
				return ids.size();
			}
		});
		
	}
	
	
	
}
