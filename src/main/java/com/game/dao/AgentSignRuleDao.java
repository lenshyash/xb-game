package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentSignRule;
import com.game.util.StationUtil;

@Repository
public class AgentSignRuleDao extends JdbcUtilImpl<AgentSignRule>{
	
	public Page<Map> queryStationRulePage(){
		Map paramMap = MixUtil.newHashMap("stationId",StationUtil.getStationId());
		String sql =  "select * from agent_sign_rule where station_id = :stationId order by days ";
		return super.page2CamelMap(sql, paramMap);
	}
	
	public AgentSignRule getMatchRule(long stationId,int signCount){
		Map paramMap = MixUtil.newHashMap("stationId",stationId,"signCount",signCount);
		String sql =  "select * from agent_sign_rule "
				+ " where station_id = :stationId "
				+ " AND CASE WHEN sign_type = 1 THEN "
				+ "(:signCount % days) = 0 and :signCount>=days "
				+ " ELSE :signCount = days END "
				+ " order by days desc limit 1 ";
		return super.query21Model(sql,paramMap);
	}
}
