package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentBaseConfigValue;
import com.game.model.SysRegisterConfig;
import com.game.model.SysRegisterConfigGroup;
import com.game.model.vo.RegisterConfigVo;

@Repository
public class SysRegisterConfigGroupDao extends JdbcUtilImpl<SysRegisterConfigGroup> {

	public List<Map> getRegConfValsByStationId(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_register_config_group");
		sql_sb.append(" WHERE station_id = :stationId ");
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("stationId", stationId);

		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public void delStationConfByPlat(Long stationId, Long platform) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("DELETE");
		sql_sb.append(" FROM sys_register_config_group g USING sys_register_config r");
		sql_sb.append(" WHERE g.conf_id = r.id AND r.platform = :platform AND station_id = :stationId");
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("platform", platform);
		paramMap.put("stationId", stationId);
		super.update(sql_sb.toString(), paramMap);
	}

	public List<Map> getRegConfVal(RegisterConfigVo rcvo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select r.*,g.show_val,g.validate_val,g.required_val,g.unique_val,g.remind_text,COALESCE(g.name_val,r.name) as name");
		sql_sb.append(" FROM sys_register_config r LEFT JOIN sys_register_config_group g ON r.id = g.conf_id");
		sql_sb.append(" WHERE r.flag_active >= 1");
		Map paramMap = MixUtil.newHashMap();
		Long platform = rcvo.getPlatform();
		Long stationId = rcvo.getStationId();
		Long showVal = rcvo.getShowVal();
		Long status = rcvo.getStatus();

		if (Validator.isNotNull(stationId)) {
			paramMap.put("stationId", stationId);
			sql_sb.append(" AND g.station_id = :stationId");
		}

		if (Validator.isNotNull(status)) {
			paramMap.put("status", status);
			sql_sb.append(" AND r.status = :status");
		}
		if (Validator.isNotNull(platform)) {
			paramMap.put("platform", platform);
			sql_sb.append(" AND r.platform = :platform");
		}
		if (Validator.isNotNull(showVal)) {
			paramMap.put("showVal", showVal);
			sql_sb.append(" AND g.show_val = :showVal");
		}

		sql_sb.append(" ORDER BY r.id");

		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}
	public SysRegisterConfigGroup getOneByExample(Long confId,Long stationId){
		List<SysRegisterConfigGroup> l = query2Model("select * from sys_register_config_group where conf_id=:confId and station_id=:stationId",MixUtil.newHashMap("stationId", stationId, "confId", confId));
		if(l==null || l.isEmpty()){
			return null;
		}
		if(l.size()>1){
			for(int i=1;i<l.size();i++){
				delete(l.get(i).getId());
			}
		}
		return l.get(0);
	}

	public int updateProp(Long id, String prop, Long value) {
		return update("update sys_register_config_group set " + prop + "_val=:value where id=:id", MixUtil.newHashMap("id", id, "value", value));
	}
	public int updateText(Long id, String name, String remindText) {
		return update("update sys_register_config_group set name_val =:name,remind_text =:remindText where id=:id", MixUtil.newHashMap("id", id, "name", name,"remindText",remindText));
	}
	
}
