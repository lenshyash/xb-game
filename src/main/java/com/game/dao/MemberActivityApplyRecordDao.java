package com.game.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MnyExchangeConfig;
import com.game.model.MemberActivityApplyRecord;
import com.game.model.vo.ExchangeConfigVo;

@Repository
public class MemberActivityApplyRecordDao extends JdbcUtilImpl<MemberActivityApplyRecord> {
	public Page<MemberActivityApplyRecord> getPage(Long stationId,String account,Date begin,Date end) {
		StringBuilder sb_sql = new StringBuilder();
		sb_sql.append("select* from member_activity_apply_record where 1 = 1 and station_id = :stationId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId",stationId);
		if(StringUtil.isNotEmpty(account)) {
			sb_sql.append(" and account = :account");
			paramMap.put("account", account);
		}
		if(StringUtil.isNotEmpty(begin)) {
			sb_sql.append(" and create_time >=:begin");
			paramMap.put("begin", begin);
		}
		if(StringUtil.isNotEmpty(end)) {
			sb_sql.append(" and create_time <=:end");
			paramMap.put("end", end);
		}
		sb_sql.append(" order by create_time desc");
		return super.paged2Obj(sb_sql.toString(), paramMap);
	}
}
