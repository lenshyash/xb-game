package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.MemberDepositStrategy;
import com.game.model.vo.DepositStrategyVo;

@Repository
public class MemberDepositStrategyDao extends JdbcUtilImpl<MemberDepositStrategy> {

	public Page page(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from member_deposit_strategy where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by id desc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public List<MemberDepositStrategy> getStrategys(DepositStrategyVo dsvo) {
		StringBuilder sb = new StringBuilder("SELECT * FROM member_deposit_strategy WHERE status=")
				.append(MemberDepositStrategy.STATUS_ENABLED);
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" AND station_id = :stationId");
		map.put("stationId", dsvo.getStationId());
		sb.append(" AND deposit_type = :depositType");
		map.put("depositType", dsvo.getDepositType());
		sb.append(" AND (deposit_count = :depositCount");
		sb.append(" OR deposit_count = ").append(MemberDepositStrategy.DEPOSIT_COUNT_DEFAULT).append(")");
		map.put("depositCount", dsvo.getDepositCount());
		sb.append(" AND start_time <= :depositTime");
		sb.append(" AND end_time > :depositTime");
		map.put("depositTime", dsvo.getDepositDatetime());
		return super.query2Model(sb.toString(), map);
	}

	public void openCloseH(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update member_deposit_strategy set status = :status where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "id", id, "stationId", stationId));
	}

	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder(
				"delete from member_deposit_strategy where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}

}
