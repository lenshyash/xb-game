package com.game.dao.platform;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentProfitShareRecord;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.util.UserUtil;

@Repository
public class AgentProfitShareRecordDao extends JdbcUtilImpl<AgentProfitShareRecord> {
	public Page page(String account, Date startDate, Date endDate, Integer status, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("account", StringUtils.isEmpty(account) ? "" : account.toLowerCase());
		map.put("stationId", stationId);
		map.put("status", status);
		String betReg = "(live_bet_money+egame_bet_money+sport_bet_money+chess_bet_money+third_sport_bet_money+third_lottery_bet_money+lottery_bet_money)";
		String winReg = "(live_win_money+egame_win_money+sport_win_money+chess_win_money+third_sport_win_money+third_lottery_win_money+lottery_win_money)";
		String feeReg = "(live_platform_fee+egame_platform_fee+sport_platform_fee+chess_platform_fee+third_sport_platform_fee+third_lottery_platform_fee+lottery_platform_fee)";
		String rebateReg = "(live_rebate_money+egame_rebate_money+sport_rebate_money+chess_rebate_money+third_sport_rebate_money+third_lottery_rebate_money+lottery_rebate_money) ";
		StringBuilder sb = new StringBuilder("select r.*,");
		sb.append(betReg);
		sb.append(" AS bet_money,");
		sb.append(winReg);
		sb.append(" AS win_money,");
		sb.append(feeReg);
		sb.append(" AS platform_fee,");
		sb.append(rebateReg);
		sb.append(" AS rebate_money");
		sb.append(" from agent_profit_share_record r where 1=1");
		if (stationId != null) {
			sb.append(" and station_id=:stationId");
		}
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(account) = :account");
		}
		if (startDate != null) {
			sb.append(" and bet_date>=:startDate");
		}
		if (endDate != null) {
			sb.append(" and bet_date<:endDate");
		}
		if (status != null && status > 0) {
			sb.append(" and profit_share_status=:status");
		}
		sb.append(" order by bet_date desc");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, betReg, "betMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, winReg, "winMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "give_money", "giveMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, feeReg, "platformFee"));
		aggs.add(new Aggregation(AggregationFunction.SUM, rebateReg, "rebateMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "third_deposit", "thirdDeposit"));
		return super.page2CamelMap(sb.toString(), map, aggs);
	}

	public Map findOneMap(Long recordId, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("recordId", recordId);
		map.put("stationId", stationId);
		String betReg = "(live_bet_money+egame_bet_money+sport_bet_money+chess_bet_money+third_sport_bet_money+third_lottery_bet_money+lottery_bet_money)";
		String winReg = "(live_win_money+egame_win_money+sport_win_money+chess_win_money+third_sport_win_money+third_lottery_win_money+lottery_win_money)";
		String feeReg = "(live_platform_fee+egame_platform_fee+sport_platform_fee+chess_platform_fee+third_sport_platform_fee+third_lottery_platform_fee+lottery_platform_fee)";
		String rebateReg = "(live_rebate_money+egame_rebate_money+sport_rebate_money+chess_rebate_money+third_sport_rebate_money+third_lottery_rebate_money+lottery_rebate_money) ";
		StringBuilder sb = new StringBuilder("select r.*,");
		sb.append(betReg);
		sb.append(" AS bet_money,");
		sb.append(winReg);
		sb.append(" AS win_money,");
		sb.append(feeReg);
		sb.append(" AS platform_fee,");
		sb.append(rebateReg);
		sb.append(" AS rebate_money");
		sb.append(" from agent_profit_share_record r where 1=1");
		if (stationId != null) {
			sb.append(" and station_id=:stationId");
		}
		if (recordId != null) {
			sb.append(" and id=:recordId");
		}
		return super.selectSingleCamelMap(sb.toString(), map);
	}

	public AgentProfitShareRecord getReocrdById(Long id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("recordId", id);
		return query21Model("SELECT * FROM agent_profit_share_record WHERE id =:recordId", map);
	}

	public Page handlerPage(String account, Date startDate, Date endDate, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("account", StringUtils.isEmpty(account) ? "" : account.toLowerCase());
		map.put("stationId", stationId);
		String betReg = "SUM(live_bet_money+egame_bet_money+sport_bet_money+chess_bet_money+third_sport_bet_money+third_lottery_bet_money+lottery_bet_money)";
		String winReg = "SUM(live_win_money+egame_win_money+sport_win_money+chess_win_money+third_sport_win_money+third_lottery_win_money+lottery_win_money)";
		String feeReg = "SUM(live_platform_fee+egame_platform_fee+sport_platform_fee+chess_platform_fee+third_sport_platform_fee+third_lottery_platform_fee+lottery_platform_fee)";
		String rebateReg = "SUM(live_rebate_money+egame_rebate_money+sport_rebate_money+chess_rebate_money+third_sport_rebate_money+third_lottery_rebate_money+lottery_rebate_money) ";
		StringBuilder sb = new StringBuilder("select r.account,r.account_id,");
		sb.append(betReg);
		sb.append(" AS bet_money,");
		sb.append(winReg);
		sb.append(" AS win_money,");
		sb.append(feeReg);
		sb.append(" AS platform_fee,");
		sb.append(rebateReg);
		sb.append(" AS rebate_money,");
		sb.append(" SUM(third_deposit) AS third_deposit");
		sb.append(" from agent_profit_share_record r where profit_share_status=");
		sb.append(AgentProfitShareRecord.STATUS_NOT_PROFIT);
		if (stationId != null) {
			sb.append(" and station_id=:stationId");
		}
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(account) = :account");
		}
		if (startDate != null) {
			sb.append(" and bet_date>=:startDate");
		}
		if (endDate != null) {
			sb.append(" and bet_date<:endDate");
		}

		sb.append(" group by account,account_id");
		return super.page2CamelMap(sb.toString(), map);
	}
	
	public int updateToHandler(Long recordId) {
		Map map = MixUtil.newHashMap("recordId", recordId);
		StringBuilder sb = new StringBuilder("UPDATE agent_profit_share_record SET ");
		sb.append(" profit_share_status = ").append(AgentProfitShareRecord.STATUS_OK);
		if(!UserUtil.isSuperAgent()) {
			sb.append(" ,operator = :operator");
			map.put("operator", "系统操作");
		}
		sb.append(" WHERE id=:recordId AND profit_share_status =").append(AgentProfitShareRecord.STATUS_NOT_PROFIT);
		return super.update(sb.toString(), map);
	}

	public int updateToHandler(Long accountId, Date startDate, Date endDate, Long stationId) {
		Map map = MixUtil.newHashMap("stationId", stationId, "accountId", accountId,
				"startDate", startDate, "endDate", endDate);
		StringBuilder sb = new StringBuilder("UPDATE agent_profit_share_record SET ");
		sb.append(" profit_share_status = ").append(AgentProfitShareRecord.STATUS_OK);
		if(!UserUtil.isSuperAgent()) {
			sb.append(" ,operator = :operator");
			map.put("operator", UserUtil.getUserAccount());
		}
		sb.append(" WHERE station_id=:stationId and account_id = :accountId and bet_date>=:startDate and bet_date<:endDate");
		return super.update(sb.toString(), map);
	}

	public List<Map> accountDataMap(String[] accountIds, Date startDate, Date endDate, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("stationId", stationId);
		String betReg = "SUM(live_bet_money+egame_bet_money+sport_bet_money+chess_bet_money+third_sport_bet_money+third_lottery_bet_money+lottery_bet_money)";
		String winReg = "SUM(live_win_money+egame_win_money+sport_win_money+chess_win_money+third_sport_win_money+third_lottery_win_money+lottery_win_money)";
		String feeReg = "SUM(live_platform_fee+egame_platform_fee+sport_platform_fee+chess_platform_fee+third_sport_platform_fee+third_lottery_platform_fee+lottery_platform_fee)";
		String rebateReg = "SUM(live_rebate_money+egame_rebate_money+sport_rebate_money+chess_rebate_money+third_sport_rebate_money+third_lottery_rebate_money+lottery_rebate_money) ";
		StringBuilder sb = new StringBuilder("select r.account,r.account_id,");
		sb.append(betReg);
		sb.append(" AS bet_money,");
		sb.append(winReg);
		sb.append(" AS win_money,");
		sb.append(feeReg);
		sb.append(" AS platform_fee,");
		sb.append(rebateReg);
		sb.append(" AS rebate_money,");
		sb.append(" SUM(third_deposit) AS third_deposit");
		sb.append(" from agent_profit_share_record r where profit_share_status=");
		sb.append(AgentProfitShareRecord.STATUS_NOT_PROFIT);
		if (stationId != null) {
			sb.append(" and station_id=:stationId");
		}
		sb.append(" and account_id IN (");
		for (int i = 0; i < accountIds.length; i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append(":accountId").append(i);
			map.put("accountId" + i, StringUtil.toLong(accountIds[i]));
		}
		sb.append(") ");
		if (startDate != null) {
			sb.append(" and bet_date>=:startDate");
		}
		if (endDate != null) {
			sb.append(" and bet_date<:endDate");
		}

		sb.append(" group by account,account_id");
		return super.selectCamelListMap(sb.toString(), map);
	}

	public List<AgentProfitShareRecord> collectAgentProfitShareRecord(Long stationId, Date begin, Date end,
			List<AgentProfitShareStrategy> strategys) {
		Map<String, Object> map = new HashMap<>();
		map.put("startDate", begin);
		map.put("endDate", end);
		map.put("stationId", stationId);
		StringBuilder sb = new StringBuilder(
				"SELECT station_id,agent_id AS account_id,agent_name AS account,stat_date AS bet_date,SUM(third_deposit_amount) as third_deposit");
		sb.append(",SUM(deposit_gift_amount+level_up_gift_amount) as give_money");
		Long platformFeeType = null;
		BigDecimal han = new BigDecimal(100);
		BigDecimal fee = null;
		for (AgentProfitShareStrategy strategy : strategys) {
			platformFeeType = strategy.getPlatformFeeType();
			fee = strategy.getPlatformFeeRate().divide(han, 2, RoundingMode.DOWN);
			// 1、彩票，2、真人，3、电子，4、体育，5、三方体育，6、三方彩票，7、棋牌
			switch (strategy.getType().intValue()) {
			case 1:
				sb.append(",SUM(-lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sys_lottery_bet_amount) AS lottery_bet_money");
				sb.append(",SUM(lottery_win_amount+mark_six_win_amount+sf_mark_six_win_amount+sys_lottery_win_amount) AS lottery_win_money");
				sb.append(",SUM(lottery_rebate_amount+sys_lottery_rebate_amount+sf_mark_six_rebate_amount+mark_six_rebate_amount) AS lottery_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(-lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sys_lottery_bet_amount");
					sb.append("-lottery_win_amount-mark_six_win_amount-sf_mark_six_win_amount-sys_lottery_win_amount)");
					sb.append("*:lotteryFee::numeric(20,2) AS lottery_platform_fee");
				} else {
					sb.append(",SUM(-lottery_bet_amount-mark_six_bet_amount-sf_mark_six_bet_amount-sys_lottery_bet_amount)*:lotteryFee::numeric(20,2) AS lottery_platform_fee");
				}
				map.put("lotteryFee", fee);
				break;
			case 2:
				sb.append(",SUM(real_bet_amount) AS live_bet_money,SUM(real_win_amount) AS live_win_money,SUM(real_rebate_amount) AS live_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(real_bet_amount-real_win_amount)*:liveFee::numeric(20,2) AS live_platform_fee");
				} else {
					sb.append(",SUM(real_bet_amount)*:liveFee::numeric(20,2) AS live_platform_fee");
				}
				map.put("liveFee", fee);
				break;
			case 3:
				sb.append(",SUM(egame_bet_amount+hunter_bet_amount) AS egame_bet_money,SUM(egame_win_amount+hunter_win_amount) AS egame_win_money,SUM(egame_rebate_amount+hunter_rebate_amount) AS egame_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(egame_bet_amount+hunter_bet_amount-egame_win_amount-hunter_win_amount)*:egameFee::numeric(20,2) AS egame_platform_fee");
				} else {
					sb.append(",SUM(egame_bet_amount+hunter_bet_amount)*:egameFee::numeric(20,2) AS egame_platform_fee");
				}
				map.put("egameFee", fee);
				break;
			case 4:
				sb.append(",SUM(-sports_bet_amount) AS sport_bet_money,SUM(sports_win_amount) AS sport_win_money,SUM(sports_rebate_amount) AS sport_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(-sports_bet_amount-sports_win_amount)*:sportFee::numeric(20,2) AS sport_platform_fee");
				} else {
					sb.append(",SUM(-sports_bet_amount)*:sportFee::numeric(20,2) AS sport_platform_fee");
				}
				map.put("sportFee", fee);
				break;
			case 5:
				sb.append(",SUM(third_sports_bet_amount) AS third_sport_bet_money,SUM(third_sports_win_amount) AS third_sport_win_money,SUM(third_sports_rebate_amount) AS third_sport_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(third_sports_bet_amount-third_sports_win_amount)*:thirdSportFee::numeric(20,2) AS third_sport_platform_fee");
				} else {
					sb.append(",SUM(third_sports_bet_amount)*:thirdSportFee::numeric(20,2) AS third_sport_platform_fee");
				}
				map.put("thirdSportFee", fee);
				break;
			case 6:
				sb.append(",SUM(third_lottery_bet_amount) AS third_lottery_bet_money,SUM(third_lottery_win_amount) AS third_lottery_win_money,SUM(third_lottery_rebate_amount) AS third_lottery_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(third_lottery_bet_amount-third_lottery_win_amount)*:thirdLotteryFee::numeric(20,2) AS third_lottery_platform_fee");
				} else {
					sb.append(",SUM(third_lottery_bet_amount)*:thirdLotteryFee::numeric(20,2) AS third_lottery_platform_fee");
				}
				map.put("thirdLotteryFee", fee);
				break;
			case 7:
				sb.append(",SUM(chess_bet_amount) AS chess_bet_money,SUM(chess_win_amount) AS chess_win_money,SUM(chess_rebate_amount) AS chess_rebate_money");
				if (StringUtil.equals(platformFeeType, AgentProfitShareStrategy.PLATFORM_FEE_TYPE_YL)) {
					sb.append(",SUM(chess_bet_amount-chess_win_amount)*:chessFee::numeric(20,2) AS chess_platform_fee");
				} else {
					sb.append(",SUM(chess_bet_amount)*:chessFee::numeric(20,2) AS chess_platform_fee");
				}
				map.put("chessFee", fee);
				break;
			}
		}
		sb.append(" FROM sys_account_daily_money r ");
		sb.append(" WHERE agent_id<>0 AND station_id=:stationId AND stat_date>=:startDate AND stat_date<:endDate");
		sb.append(" GROUP BY station_id,stat_date,agent_id,agent_name");
		return super.query2Model(sb.toString(), map);
	}

	public AgentProfitShareRecord findOne(Date betDate, Long accountId, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder(
				"select * from agent_profit_share_record where bet_date=:betDate and account_id=:accountId and station_id=:stationId");
		map.put("betDate", betDate);
		map.put("accountId", accountId);
		map.put("stationId", stationId);
		return query21Model(sql.toString(), map);
	}

	public void updateBetMoney(AgentProfitShareRecord r) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("betDate", r.getBetDate());
		map.put("accountId", r.getAccountId());
		map.put("thirdDeposit", nullToZero(r.getThirdDeposit()));
		map.put("giveMoney", nullToZero(r.getGiveMoney()));
		// 真人
		map.put("liveBetMoney", nullToZero(r.getLiveBetMoney()));
		map.put("liveWinMoney", nullToZero(r.getLiveWinMoney()));
		map.put("livePlatfromFee", nullToZero(r.getLivePlatformFee()));
		map.put("liveRebateMoney", nullToZero(r.getLiveRebateMoney()));
		// 电子
		map.put("egameBetMoney", nullToZero(r.getEgameBetMoney()));
		map.put("egameWinMoney", nullToZero(r.getEgameWinMoney()));
		map.put("egamePlatfromFee", nullToZero(r.getEgamePlatformFee()));
		map.put("egameRebateMoney", nullToZero(r.getEgameRebateMoney()));
		// 体育
		map.put("sportBetMoney", nullToZero(r.getSportBetMoney()));
		map.put("sportWinMoney", nullToZero(r.getSportWinMoney()));
		map.put("sportPlatfromFee", nullToZero(r.getSportPlatformFee()));
		map.put("sportRebateMoney", nullToZero(r.getSportRebateMoney()));
		// 彩票
		map.put("lotteryBetMoney", nullToZero(r.getLotteryBetMoney()));
		map.put("lotteryWinMoney", nullToZero(r.getLotteryWinMoney()));
		map.put("lotteryPlatfromFee", nullToZero(r.getLotteryPlatformFee()));
		map.put("lotteryRebateMoney", nullToZero(r.getLotteryRebateMoney()));
		// 棋牌
		map.put("chessBetMoney", nullToZero(r.getChessBetMoney()));
		map.put("chessWinMoney", nullToZero(r.getChessWinMoney()));
		map.put("chessPlatfromFee", nullToZero(r.getChessPlatformFee()));
		map.put("chessRebateMoney", nullToZero(r.getChessRebateMoney()));
		// 三方体育
		map.put("thirdSportBetMoney", nullToZero(r.getThirdSportBetMoney()));
		map.put("thirdSportWinMoney", nullToZero(r.getThirdSportWinMoney()));
		map.put("thirdSportPlatfromFee", nullToZero(r.getThirdSportPlatformFee()));
		map.put("thirdSportRebateMoney", nullToZero(r.getThirdSportRebateMoney()));
		// 三方彩票
		map.put("thirdLotteryBetMoney", nullToZero(r.getThirdLotteryBetMoney()));
		map.put("thirdLotteryWinMoney", nullToZero(r.getThirdLotteryWinMoney()));
		map.put("thirdLotteryPlatfromFee", nullToZero(r.getThirdLotteryPlatformFee()));
		map.put("thirdLotteryRebateMoney", nullToZero(r.getThirdLotteryRebateMoney()));

		map.put("stationId", r.getStationId());
		StringBuilder sql = new StringBuilder("UPDATE agent_profit_share_record SET ");
		// 彩票
		sql.append("third_deposit=:thirdDeposit,give_money = :giveMoney");
		// 彩票
		sql.append(
				",lottery_bet_money=:lotteryBetMoney,lottery_win_money=:lotteryWinMoney,lottery_platform_fee=:lotteryPlatfromFee,lottery_rebate_money=:lotteryRebateMoney ");
		// 真人
		sql.append(
				",live_bet_money=:liveBetMoney,live_win_money=:liveWinMoney,live_platform_fee=:livePlatfromFee,live_rebate_money=:liveRebateMoney ");
		// 电子
		sql.append(
				",egame_bet_money=:egameBetMoney,egame_win_money=:egameWinMoney,egame_platform_fee=:egamePlatfromFee,egame_rebate_money=:egameRebateMoney ");
		// 体育
		sql.append(
				",sport_bet_money=:sportBetMoney,sport_win_money=:sportWinMoney,sport_platform_fee=:sportPlatfromFee,sport_rebate_money=:sportRebateMoney ");
		// 棋牌
		sql.append(
				",chess_bet_money=:chessBetMoney,chess_win_money=:chessWinMoney,chess_platform_fee=:chessPlatfromFee,chess_rebate_money=:chessRebateMoney ");
		// 三方体育
		sql.append(
				",third_sport_bet_money=:thirdSportBetMoney,third_sport_win_money=:thirdSportWinMoney,third_sport_platform_fee=:thirdSportPlatfromFee,third_sport_rebate_money=:thirdSportRebateMoney ");
		// 三方彩票
		sql.append(
				",third_lottery_bet_money=:thirdLotteryBetMoney,third_lottery_win_money=:thirdLotteryWinMoney,third_lottery_platform_fee=:thirdLotteryPlatfromFee,third_lottery_rebate_money=:thirdLotteryRebateMoney ");
		sql.append(" WHERE bet_date=:betDate and account_id=:accountId and station_id=:stationId ");
		update(sql.toString(), map);
	}

	private BigDecimal nullToZero(Object obj) {
		return StringUtil.toBigDecimal(obj);
	}
}
