package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.asn1.esf.SPUserNotice;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentAdressBook;
import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.util.StationUtil;

@Repository
public class AgentAdressBookDao extends JdbcUtilImpl<AgentAdressBook> {

	public Page page(Long stationId, String account) {
		StringBuilder sb = new StringBuilder("select a.*,s.account from Agent_adress_book a");
		sb.append(" left join sys_account s on a.account_id = s.id");
		sb.append(" where 1=1 ");
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(!Validator.isNull(stationId)) {
			sb.append(" and a.station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(StringUtil.isNotEmpty(account)) {
			sb.append(" and s.account = :account");
			map.put("account", account);
		}
		
		sb.append(" order by id desc");
		return super.page2CamelMap(sb.toString(), map);
	}
	
	public int checkUnique(Long stationId,Long accountId, String contactName,String contactPhone) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(*) from agent_adress_book");
		sql.append(" where station_id = :stationId");
		sql.append(" and account_id = :accountId");
		sql.append(" and contact_name = :contactName");
		sql.append(" and contact_phone = :contactPhone");
		
		HashMap<String, Object> paramMap = new HashMap<String ,Object>();
		paramMap.put("stationId", stationId);
		paramMap.put("accountId", accountId);
		paramMap.put("contactName", contactName);
		paramMap.put("contactPhone", contactPhone);
		
		return super.queryForInt(sql.toString(), paramMap);
	}
	
	public List<Map> export(Long stationId, String account) {
		StringBuilder sb = new StringBuilder("select a.*,s.account from Agent_adress_book a");
		sb.append(" left join sys_account s on a.account_id = s.id");
		sb.append(" where 1=1 ");
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(!Validator.isNull(stationId)) {
			sb.append(" and a.station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(StringUtil.isNotEmpty(account)) {
			sb.append(" and s.account = :account");
			map.put("account", account);
		}
		
		sb.append(" order by id desc");
		return super.selectCamelListMap(sb.toString(), map);
	}
}
