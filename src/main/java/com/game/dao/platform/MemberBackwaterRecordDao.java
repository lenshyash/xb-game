package com.game.dao.platform;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.constant.BusinessConstant;
import com.game.model.MnyComRecord;
import com.game.model.MnyDrawRecord;
import com.game.model.lottery.LotteryEnum;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.sports.SportBettingOrder;
import com.game.model.vo.BackwaterParam;

@Repository
public class MemberBackwaterRecordDao extends JdbcUtilImpl<MemberBackwaterRecord> {
	public Page<MemberBackwaterRecord> page(String account, Date startDate, Date endDate, Integer betType,
			Integer status, Long stationId, Long roomId,Long reportType) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("account", StringUtils.isEmpty(account) ? "" : account.toLowerCase());
		map.put("betType", betType);
		map.put("stationId", stationId);
		map.put("status", status);
		StringBuilder sb = new StringBuilder("select b.* from member_backwater_record b left join sys_account a on b.account_id=a.id where 1=1");
		if (stationId != null) {
			sb.append(" and b.station_id=:stationId");
		}
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(b.account) = :account");
		}
		if (betType != null && betType > 0) {
			sb.append(" and b.bet_type=:betType");
		}
		if (startDate != null) {
			sb.append(" and b.bet_date>=:startDate");
		}
		if (endDate != null) {
			sb.append(" and b.bet_date<:endDate");
		}
		if (status != null && status > 0) {
			sb.append(" and b.backwater_status=:status");
		}
		if (roomId != null && roomId > 0) {
			sb.append(" and b.backwater_room_id=:roomId");
			map.put("roomId", roomId);
		}
		if (reportType != null && reportType > 0) {
			sb.append(" and a.report_type=:reportType");
			map.put("reportType", reportType);
		}
		sb.append(" order by bet_date desc,bet_money desc");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		String condition = "case when backwater_status = " + BusinessConstant.STATUS_ALREADY_ROLL_BACK_AND_MONEY
				+ " then backwater_money else 0 end";
		aggs.add(new Aggregation(AggregationFunction.SUM, condition, "backwaterMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "bet_money", "betMoney"));
		aggs.add(new Aggregation(AggregationFunction.SUM, "win_money", "winMoney"));
		return paged2Obj(sb.toString(), map, aggs);
	}

	public Page page(BackwaterParam param) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		Long stationId = param.getStationId();
		Long memberId = param.getMemberId();
		Date startDate = param.getBegin();
		Date endDate = param.getEnd();
		Long status = param.getStatus();
		StringBuilder sb = new StringBuilder("select * from member_backwater_record where 1=1");
		if (Validator.isNotNull(memberId)) {
			map.put("memberId", memberId);
			sb.append(" and account_id=:memberId");
		}
		if (Validator.isNotNull(stationId)) {
			map.put("stationId", stationId);
			sb.append(" and station_id=:stationId");
		}
		if (startDate != null) {
			map.put("startDate", startDate);
			sb.append(" and bet_date>=:startDate");
		}
		if (endDate != null) {
			map.put("endDate", endDate);
			sb.append(" and bet_date<=:endDate");
		}
		if (Validator.isNotNull(status)) {
			sb.append(" and backwater_status=:status");
			map.put("status", status);
		}else {
			sb.append(" and (backwater_status=:status2 or backwater_status=:status3 or backwater_status=:status4)");
			map.put("status2", MemberBackwaterRecord.STATUS_MONEY_UNCLAIMED);
			map.put("status3", MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
			map.put("status4", MemberBackwaterRecord.STATUS_MONEY_EXPIRED);
		}
		sb.append(" order by bet_date desc");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		return paged2Obj(sb.toString(), map, aggs);
	}

	public List<MemberBackwaterRecord> findByIds(List<Long> ids, Date startDate, Date endDate, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("stationId", stationId);
		StringBuilder sql = new StringBuilder("select * from member_backwater_record where bet_date>=:startDate");
		sql.append(" and bet_date<:endDate and station_id=:stationId and id in(");
		for (Long id : ids) {
			sql.append(id).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(")");
		return query2Model(sql.toString(), map);
	}

	public MemberBackwaterRecord findOne(Date betDate, String account, Integer betType, Long stationId, Long roomId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder(
				"select * from member_backwater_record where bet_date=:betDate and account=:account and bet_type=:betType and station_id=:stationId");
		map.put("betDate", betDate);
		map.put("account", account);
		map.put("betType", betType);
		map.put("stationId", stationId);
		if (roomId != null && roomId > 0) {
			sql.append(" and backwater_room_id=:roomId");
			map.put("roomId", roomId);
		}
		return query21Model(sql.toString(), map);
	}

	public void updateBetMoney(MemberBackwaterRecord r) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("betDate", r.getBetDate());
		map.put("account", r.getAccount());
		map.put("betType", r.getBetType());
		map.put("betMoney", r.getBetMoney());
		map.put("winMoney", r.getWinMoney());
		map.put("stationId", r.getStationId());
		StringBuilder sql = new StringBuilder(
				"update member_backwater_record set bet_money=:betMoney,win_money=:winMoney where bet_date=:betDate and account=:account and bet_type=:betType and station_id=:stationId ");
		if (r.getBackwaterRoomId() != null && r.getBackwaterRoomId() > 0) {
			sql.append(" and backwater_room_id=:roomId");
			map.put("roomId", r.getBackwaterRoomId());
		}
		update(sql.toString(), map);
	}

	public int updateStatus(Date betDate, String account, Integer betType, Long stationId, Integer srcStatus,
			Integer newStatus, String operator, Long operatorId, Long roomId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder(
				"update member_backwater_record set backwater_status=:newStatus,operator=:operator,operator_id=:operatorId where bet_date=:betDate and account=:account and bet_type=:betType and station_id=:stationId and backwater_status=:srcStatus ");
		map.put("betDate", betDate);
		map.put("account", account);
		map.put("betType", betType);
		map.put("srcStatus", srcStatus);
		map.put("newStatus", newStatus);
		map.put("stationId", stationId);
		map.put("operator", operator);
		map.put("operatorId", operatorId);
		if (roomId != null && roomId > 0) {
			sql.append(" and backwater_room_id=:roomId");
			map.put("roomId", roomId);
		}
		return update(sql.toString(), map);
	}

	public int updateBackInfo(MemberBackwaterRecord r, Integer oldStatus) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("betDate", r.getBetDate());
		map.put("account", r.getAccount());
		map.put("betType", r.getBetType());
		map.put("backwater_money", r.getBackwaterMoney());
		map.put("backwater_rate", r.getBackwaterRate());
		map.put("backwater_desc", r.getBackwaterDesc());
		map.put("draw_need", r.getDrawNeed());
		map.put("backwater_status", r.getBackwaterStatus());
		map.put("oldStatus", oldStatus);
		map.put("stationId", r.getStationId());
		map.put("operator", r.getOperator());
		map.put("operatorId", r.getOperatorId());
		StringBuilder sql = new StringBuilder("update member_backwater_record set backwater_money=:backwater_money, ");
		sql.append("backwater_rate=:backwater_rate,backwater_desc=:backwater_desc,draw_need=:draw_need,");
		sql.append("backwater_status=:backwater_status,operator=:operator,operator_id=:operatorId");
		sql.append(
				" where bet_date=:betDate and account=:account and bet_type=:betType and station_id=:stationId  and backwater_status=:oldStatus");
		if (r.getBackwaterRoomId() != null && r.getBackwaterRoomId() > 0) {
			sql.append(" and backwater_room_id=:roomId");
			map.put("roomId", r.getBackwaterRoomId());
		}
		return update(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> collectMarkSixDayBetMoney(Date start, Date end, List<Long> sIdList) {
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id,account");
		sql.append(",station_id,to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(",(case when (lot_code=:lhc and play_code='tm_b') then 5");
		sql.append(" when (lot_code=:lhc and play_code='zm_b') then 6");
		
		sql.append(" when (lot_code=:wflhc and play_code='tm_b') then 15");
		sql.append(" when (lot_code=:wflhc and play_code='zm_b') then 16");
		
		sql.append(" when (lot_code=:hkmhlhc and play_code='tm_b') then 15");
		sql.append(" when (lot_code=:hkmhlhc and play_code='zm_b') then 16");

		sql.append(" when (lot_code=:amlhc and play_code='tm_b') then 15");
		sql.append(" when (lot_code=:amlhc and play_code='zm_b') then 16");

		sql.append(" when (lot_code=:tmlhc and play_code='tm_b') then 15");
		sql.append(" when (lot_code=:tmlhc and play_code='zm_b') then 16");
		
		sql.append(" when (lot_code=:sflhc and play_code='tm_b') then 15");
		sql.append(" when (lot_code=:sflhc and play_code='zm_b') then 16 end) as bet_type");
		sql.append(" from bc_lottery_order where create_time>=:start");
		sql.append(" and create_time<:end and station_id in(");
		for (Long sId : sIdList) {
			sql.append(sId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(") and status in (2,3,6) and roll_back_status=1");
		sql.append(" and (play_code='tm_b' or play_code='zm_b')");
		sql.append(" and (lot_code=:lhc or lot_code=:sflhc or lot_code=:amlhc or lot_code=:wflhc or lot_code=:hkmhlhc or lot_code=:tmlhc)");
//		sql.append(" and lot_code=:lhc");
		sql.append(" group by account_id,account,station_id,bet_date,bet_type");
		Map<Object, Object> map = MixUtil.newHashMap("start", start, "end", end);
		map.put("lhc", LotteryEnum.LHC.name());
		map.put("sflhc", LotteryEnum.SFLHC.name());
		map.put("amlhc", LotteryEnum.AMLHC.name());
		map.put("wflhc", LotteryEnum.WFLHC.name());
		map.put("tmlhc", LotteryEnum.TMLHC.name());
		map.put("hkmhlhc", LotteryEnum.HKMHLHC.name());
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> collectLotteryDayBetMoney(Date start, Date end, List<Long> sIdList) {
		Map<Object, Object> map = MixUtil.newHashMap("start", start, "end", end,"lhc", LotteryEnum.LHC.name(),"amlhc", LotteryEnum.AMLHC.name(),"sflhc", LotteryEnum.SFLHC.name(),"wflhc", LotteryEnum.WFLHC.name(),"hkmhlhc", LotteryEnum.HKMHLHC.name());
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id,account");
		sql.append(",station_id,to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(",1 as bet_type");
		sql.append(" from bc_lottery_order where create_time>=:start");
		sql.append(" and create_time<:end and station_id in(");
		for (Long sId : sIdList) {
			sql.append(sId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(") and status in (2,3,6,10) and roll_back_status=1 and lot_code<>:lhc and lot_code<>:sflhc and lot_code<>:amlhc and lot_code<>:wflhc and lot_code<>:hkmhlhc");
		List<LotteryEnum> sysLots = LotteryEnum.getSysLottery();
		for (LotteryEnum lotteryEnum : sysLots) {
			if(lotteryEnum.name().equals(LotteryEnum.SFLHC.name())) {
				continue;
			}
			sql.append(" and lot_code<>:").append(lotteryEnum.name().toLowerCase());
			map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
		}
		sql.append(" group by account_id,account,station_id,bet_date,bet_type");
		return super.query2Model(sql.toString(), map);
	}
	
	public List<MemberBackwaterRecord> collectSysLotteryDayBetMoney(Date start, Date end, List<Long> sIdList) {
		Map<Object, Object> map = MixUtil.newHashMap("start", start, "end", end);
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id,account");
		sql.append(",station_id,to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(",11 as bet_type");
		sql.append(" from bc_lottery_order where create_time>=:start");
		sql.append(" and create_time<:end and station_id in(");
		for (Long sId : sIdList) {
			sql.append(sId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(") and status in (2,3,6,10) and roll_back_status=1 and lot_code IN ("); 
		List<LotteryEnum> sysLots = LotteryEnum.getSysLottery();
		LotteryEnum lotteryEnum = null;
		for (int i = 0; i < sysLots.size(); i++) {
			lotteryEnum = sysLots.get(i);
			if(lotteryEnum.name().equals(LotteryEnum.SFLHC.name())) {
				continue;
			}
			if(i > 0) {
				sql.append(",");
			}
			sql.append(" :").append(lotteryEnum.name().toLowerCase());
			map.put(lotteryEnum.name().toLowerCase(), lotteryEnum.name());
		}
		sql.append(")"); 
		sql.append(" group by account_id,account,station_id,bet_date,bet_type");
		return super.query2Model(sql.toString(), map);
	}

	// rIds 房间标识，ids 站点Id
	public List<MemberBackwaterRecord> collectLotteryV4DayBetMoney(Date start, Date end, Set<Long> ids,
			Set<Long> rIds) {
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id,account");
		sql.append(",station_id,to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(",1 as bet_type");
		sql.append(",room_id as backwater_room_id");
		sql.append(" from bc_lottery_order where create_time>=:start and create_time<:end and station_id in(");
		for (Long sId : ids) {
			sql.append(sId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(") and room_id in(");
		for (Long rId : rIds) {
			sql.append(rId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(") and status in (2,3,6,10) and roll_back_status=1 and lot_code<>:lhc and lot_code<>:sflhcand lot_code<>:amlhc");
		sql.append(" group by account_id,account,station_id,bet_date,bet_type,backwater_room_id");
		Map<Object, Object> map = MixUtil.newHashMap("start", start, "end", end);
		map.put("lhc", LotteryEnum.LHC.name());
		map.put("sflhc", LotteryEnum.SFLHC.name());
		map.put("amlhc", LotteryEnum.AMLHC.name());
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> collectSportDayBetMoney(Date start, Date end, List<Long> sIdList) {
		StringBuilder sql = new StringBuilder(
				"select sum(betting_money) as bet_money,sum(betting_result) as win_money,member_id as account_id,member_name as account");
		sql.append(",station_id,to_char(account_datetime,'YYYY-MM-dd') as bet_date,4 as bet_type");
		sql.append(" from sport_betting where (balance =:bal1 or balance=:bal2 or balance=:bal3) ");// 已结算
		sql.append(" and mix!=").append(SportBettingOrder.MIX_CHILD); // 忽略混合过关子单
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("bal1", SportBettingOrder.BALANCE_DONE);
		map.put("bal2", SportBettingOrder.BALANCE_AGENT_HAND_DONE);
		map.put("bal3", SportBettingOrder.BALANCE_BFW_DONE);
		map.put("start", start);
		map.put("end", end);
		sql.append(" and account_datetime >= :start and account_datetime<:end and station_id in(");
		for (Long sId : sIdList) {
			sql.append(sId).append(",");
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(
				") and (roll_back_status=1 or roll_back_status is null) group by account_id,account,station_id,bet_date");
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> findForProxyDailyStat(Long sid, Date startDate, Date endDate) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("stationId", sid);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		StringBuilder sql = new StringBuilder("select agent_id,bet_date,bet_type, sum(bet_money) as bet_money,");
		sql.append(
				"sum(win_money) as win_money,suM(COALESCE(backwater_money,0)) as backwater_money from member_backwater_record");
		sql.append(
				" where bet_date>=:startDate and bet_date<:endDate and station_id=:stationId group by agent_id,bet_date,bet_type");
		return query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> findLHCBetAndWin(Long stationId, Date startDate, Date endDate) {
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id");
		sql.append(",to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(" from bc_lottery_order where create_time>=:startDate");
		sql.append(" and create_time<:endDate and station_id=:stationId");
		sql.append(" and status in (2,3,6) and (lot_code=:lotCode or lot_code=:amLhc)");
		sql.append(" group by account_id,bet_date");
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("stationId", stationId);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("lotCode", LotteryEnum.LHC.name());
		map.put("amLhc", LotteryEnum.AMLHC.name());
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> findLotteryBetAndWin(Long stationId, Date startDate, Date endDate) {
		StringBuilder sql = new StringBuilder(
				"select sum(buy_money) as bet_money,sum(win_money) as win_money,account_id");
		sql.append(",to_char(create_time,'YYYY-MM-dd') as bet_date");
		sql.append(" from bc_lottery_order where create_time>=:startDate");
		sql.append(" and create_time<:endDate and station_id=:stationId");
		sql.append(" and status in (2,3,6,10) and lot_code<>:lotCode and lot_code<>:amlhc");
		sql.append(" group by account_id,bet_date");
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("stationId", stationId);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("lotCode", LotteryEnum.LHC.name());
		map.put("amlhc", LotteryEnum.AMLHC.name());
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> findSportBetAndWin(Long stationId, Date startDate, Date endDate) {
		StringBuilder sql = new StringBuilder(
				"select sum(betting_money) as bet_money,sum(betting_result) as win_money,member_id as account_id");
		sql.append(",to_char(betting_date,'YYYY-MM-dd') as bet_date");
		sql.append(" from sport_betting where (balance =:bal1 or balance=:bal2 or balance=:bal3) ");// 已结算
		sql.append(" and mix!=").append(SportBettingOrder.MIX_CHILD); // 忽略混合过关子单
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("bal1", SportBettingOrder.BALANCE_DONE);
		map.put("bal2", SportBettingOrder.BALANCE_AGENT_HAND_DONE);
		map.put("bal3", SportBettingOrder.BALANCE_BFW_DONE);
		map.put("start", startDate);
		map.put("end", endDate);
		sql.append(" and betting_date >= :start and betting_date<:end and station_id=:stationId");
		map.put("stationId", stationId);
		sql.append(" group by account_id,bet_date");
		return super.query2Model(sql.toString(), map);
	}

	public List<MemberBackwaterRecord> getUnBackwater(Date startDate, Date endDate, Long agentId, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("stationId", stationId);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("agentId", agentId);
		map.put("success", MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
		StringBuilder sql = new StringBuilder("select * from member_backwater_record where ");
		sql.append(" bet_date>=:startDate and bet_date<:endDate and station_id=:stationId");
		sql.append(" and agent_id=:agentId and backwater_status!=:success and bet_type in(2,3,4)");
		return query2Model(sql.toString(), map);
	}

	public void addMoney(MemberBackwaterRecord r) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("betDate", r.getBetDate());
		map.put("account", r.getAccount());
		map.put("betType", r.getBetType());
		map.put("betMoney", r.getBetMoney());
		map.put("winMoney", r.getWinMoney());
		map.put("stationId", r.getStationId());
		map.put("success", MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
		update("update member_backwater_record set bet_money=bet_money+:betMoney,win_money=win_money+:winMoney where bet_date=:betDate and account=:account and bet_type=:betType and station_id=:stationId  and backwater_status!=:success",
				map);
	}
	
	public MemberBackwaterRecord getReocrdById(Long id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("recordId", id);
		return query21Model("SELECT * FROM member_backwater_record WHERE id =:recordId",
				map);
	}
	
	public int updateStatusToExpired(Date time,Long stationId) {
		return super.update(
				"UPDATE member_backwater_record SET backwater_status = " + MemberBackwaterRecord.STATUS_MONEY_EXPIRED 
				+" WHERE backwater_status = " + MemberBackwaterRecord.STATUS_MONEY_UNCLAIMED
						+ " AND bet_date < :time AND station_id = :stationId",
				MixUtil.newHashMap("time",time,"stationId", stationId));
	}
}
