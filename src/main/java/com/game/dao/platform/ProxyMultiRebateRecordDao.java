package com.game.dao.platform;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MnyComRecord;
import com.game.model.platform.ProxyMultiRebateRecord;

@SuppressWarnings("unchecked")
@Repository
public class ProxyMultiRebateRecordDao extends JdbcUtilImpl<ProxyMultiRebateRecord> {
	
	
	@SuppressWarnings("rawtypes")
	public Page page(ProxyMultiRebateRecord proxyMultiRebateRecord,Date startTime,Date endTime,boolean forAdmin){
		StringBuilder sb = new StringBuilder();
		if(forAdmin){
			sb.append("select pmrr.*,ss.name as domain from proxy_multi_rebate_record pmrr");
			sb.append(" JOIN sys_station ss ON ss.id = pmrr.station_id");
		}else{
			sb.append("select pmrr.* from proxy_multi_rebate_record pmrr");
		}
		sb.append(" where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if(!StringUtil.isEmpty(proxyMultiRebateRecord.getStationId())){
			sb.append(" and pmrr.station_id = :stationId");
			map.put("stationId", proxyMultiRebateRecord.getStationId());
		}
		if(!StringUtil.isEmpty(proxyMultiRebateRecord.getAccount())){
			sb.append(" and lower(pmrr.account) = :account");
			map.put("account", proxyMultiRebateRecord.getAccount().toLowerCase());
		}
		if(proxyMultiRebateRecord.getRebateType()!=null){
			sb.append(" and rebate_type = :rebateType");
			map.put("rebateType", proxyMultiRebateRecord.getRebateType());
		}
		
		if(startTime!=null){
			sb.append(" and pmrr.create_datetime >= :startTime");
			map.put("startTime", startTime);
		}
		if(endTime!=null){
			sb.append(" and pmrr.create_datetime <= :endTime");
			map.put("endTime", endTime);
		}
		sb.append(" ORDER BY pmrr.create_datetime desc");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"pmrr.rebate_money", "totalMoney"));
		
		return super.page2CamelMap(sb.toString(), map,aggs);
	}
	
	public List<ProxyMultiRebateRecord> getMultiRebateByBetId(Long betId,Long stationId){
		StringBuilder sb = new StringBuilder("select * from proxy_multi_rebate_record where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and bet_id = :bet_id");
		map.put("bet_id", betId);
		return super.query2Model(sb.toString(), map);
	}

	public void deleteById(Long id) {
		update("delete from proxy_multi_rebate_record where id="+id);
	}
}

