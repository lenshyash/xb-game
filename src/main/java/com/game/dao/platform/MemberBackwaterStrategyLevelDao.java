package com.game.dao.platform;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.MemberBackwaterStrategyLevel;

@Repository
public class MemberBackwaterStrategyLevelDao extends JdbcUtilImpl<MemberBackwaterStrategyLevel> {

	public List<MemberBackwaterStrategyLevel> findByStrategyId(Long strategyId, Long stationId) {
		return query2Model("select * from member_backwater_strategy_level where strategy_id=" + strategyId
				+ " and station_id=" + stationId);
	}

	public List<Long> getLevelIdsByStrategyId(Long strategyId, Long stationId) {
		String key = "s_" + stationId + "_s_" + strategyId+"_level_group";
		String ids = CacheUtil.getCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, String.class);
		List<Long> node = null;
		if (ids != null) {
			node = JSON.parseArray(ids, Long.class);
		}
		if(node == null || node.isEmpty()) {
			Map<String, Object> map = new HashMap<>();
			map.put("strategyId", strategyId);
			map.put("stationId", stationId);
			node = query2Obj(
					"select member_level_id from member_backwater_strategy_level where strategy_id=:strategyId and station_id=:stationId",
					map, new RowMapper<Long>() {
						@Override
						public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getLong(1);
						}
					});
			if (node != null && !node.isEmpty()) {
				CacheUtil.addCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, node);
			}
		}
		return node;
	}

	public void batchDelete(final Long strategyId, final Long stationId, final List<Long> levelIdList) {
		StringBuilder sql = new StringBuilder("delete from member_backwater_strategy_level WHERE");
		sql.append(" strategy_id=? and station_id=? and member_level_id=?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				int k = 1;
				ps.setLong(k++, strategyId);
				ps.setLong(k++, stationId);
				ps.setLong(k++, levelIdList.get(i));
			}

			@Override
			public int getBatchSize() {
				return levelIdList.size();
			}
		});
	}

	public void deleteByStrategyId(Long strategyId, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("strategyId", strategyId);
		map.put("stationId", stationId);
		update("delete from member_backwater_strategy_level WHERE strategy_id=:strategyId and station_id=:stationId",
				map);
	}

}
