package com.game.dao.platform;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentProfitShareStrategy;

@Repository
public class AgentProfitShareStrategyDao extends JdbcUtilImpl<AgentProfitShareStrategy> {

	public Page page(Integer type, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("type", type);
		StringBuilder sql = new StringBuilder("select * from agent_profit_share_strategy where 1=1");
		if (type != null && type > 0) {
			sql.append(" and type=:type");
		}
		if (stationId != null && stationId > 0L) {
			sql.append(" and station_id=:stationId");
		}
		sql.append(" order by type asc");
		return paged2Obj(sql.toString(), map);
	}

	public void closeOrOpen(Integer status, Long stationId, Long id) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("status", status);
		map.put("id", id);
		update("update agent_profit_share_strategy set status=:status where station_id=:stationId and id=:id", map);
	}

	@Override
	public AgentProfitShareStrategy insert(AgentProfitShareStrategy t) {
		t = super.insert(t);
		return t;
	}

	@Override
	public int update(AgentProfitShareStrategy t) {
		int f = super.update(t);
		return f;
	}

	public AgentProfitShareStrategy getOne(Long id, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("id", id);
		return query21Model("select * from agent_profit_share_strategy where station_id=:stationId and id=:id", map);
	}

	public int delete(AgentProfitShareStrategy old) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", old.getStationId());
		map.put("id", old.getId());
		int f = update("delete from agent_profit_share_strategy where station_id=:stationId and id=:id", map);
		return f;
	}

	public List<AgentProfitShareStrategy> findStationStrategy(Long stationId) {
		if (stationId == null) {
			return null;
		}
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		List<AgentProfitShareStrategy> node = query2Model(
				"select * from agent_profit_share_strategy where station_id=:stationId and status=2", map);
		return node;
	}

}
