package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentActivity;
import com.game.model.platform.AgentArticle;
import com.game.util.StationUtil;

@Repository
public class AgentActivityDao extends JdbcUtilImpl<AgentActivity> {

	public Page page(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from agent_activity where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by pai_xu asc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public Integer getStatus(Long id) {
		StringBuilder sb = new StringBuilder("select status from agent_activity where id = " + id);
		return queryForInt(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public Long getStationId(Long id) {
		StringBuilder sb = new StringBuilder("select station_id from agent_activity where id = " + id);
		return queryForLong(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_activity set model_status = :modelStatus where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("modelStatus", modelStatus, "id", id, "stationId", stationId));
	}

	public void openCloseK(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_activity set status = :status where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "id", id, "stationId", stationId));
	}

	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from agent_activity where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}

	public List<AgentActivity> getActivity(Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select v.*  ");
		StringBuilder tabs = new StringBuilder(" from agent_activity v");
		StringBuilder whes = new StringBuilder(" where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(StationUtil.isDomainFolderSource()) {
			whes.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='') ");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		whes.append(" and v.station_id=:stationId");
		map.put("stationId", stationId);
		whes.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		whes.append(" and model_status = 2");
		whes.append(" order by pai_xu asc");
		return super.query2Model(sb.append(tabs).append(whes).toString(), map);
	}
	
	public List<AgentActivity> getUnreadActivity(Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select v.*  ");
		StringBuilder tabs = new StringBuilder(" from agent_activity v");
		StringBuilder whes = new StringBuilder(" where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(StationUtil.isDomainFolderSource()) {
			whes.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		whes.append(" and v.station_id=:stationId");
		map.put("stationId", stationId);
		whes.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		whes.append(" and model_status = 2");
		whes.append(" and read_flag = 0");
		whes.append(" order by pai_xu asc");
		return super.query2Model(sb.append(tabs).append(whes).toString(), map);
	}

	public void readActive(Long id) {
		super.update(
				"UPDATE agent_activity SET read_flag = " + AgentActivity.READ_STATUS
						+ " WHERE id = :id AND read_flag = " + AgentActivity.UNREAD_STATUS,
				MixUtil.newHashMap("id", id));
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getActivityImg(Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select title_img_url img , v.id ");
		StringBuilder tabs = new StringBuilder(" from agent_activity v");
		StringBuilder whes = new StringBuilder(" where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(StationUtil.isDomainFolderSource()) {
			whes.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		whes.append(" and v.station_id=:stationId");
		map.put("stationId", stationId);
		whes.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		whes.append(" and model_status = 2");
		whes.append(" order by pai_xu asc");
		return super.selectCamelListMap(sb.append(tabs).append(whes).toString(), map);
	}


}
