package com.game.dao.platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.model.lottery.LotteryEnum;
import com.game.model.platform.AgentLotteryModelConfig;


@Repository
public class AgentLotteryModelConfigDao extends JdbcUtilImpl<AgentLotteryModelConfig> {
	public  Page page(Integer type, Long stationId,String name,List<LotteryEnum> list) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		
		StringBuilder sql = new StringBuilder("select b.id as lot_id,b.name as lot_name,b.code as lot_code,b.type as lot_type,b.station_id,a.lottery_mode,a.percentage from bc_lottery b ");
		sql.append(" left join agent_lottery_model_config a on b.code = a.lot_code and a.station_id = b.station_id where b.station_id = :stationId");
		sql.append("  and model_status = 2 and status = 2 and b.code in(");
		for(LotteryEnum le:list) {
			sql.append("'"+le.name()+"',");
		}
		sql.delete(sql.length()-1, sql.length());
		sql.append(" )");
		if (type != null ) {
			sql.append(" and b.view_group=:type");
			map.put("type", type);
		}
		if (StringUtils.isNotBlank(name)) {
			sql.append(" and b.name like '%"+name+"%'");
		}
		sql.append(" ORDER BY sort_no desc");
		return paged2Obj(sql.toString(), map);
	}

	public AgentLotteryModelConfig getOne(Long id, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("id", id);
		return query21Model("select b.id as lot_id,b.name as lot_name,b.code as lot_code,b.type as lot_type,b.station_id,a.id,a.lottery_mode,a.percentage  from bc_lottery b left join agent_lottery_model_config a on b.code = a.lot_code and a.station_id = b.station_id where b.station_id = :stationId and b.id= :id", map);
	}
	public AgentLotteryModelConfig getOne(String lotCode, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("lotCode", lotCode);
		return query21Model("select a.lot_code,a.lottery_mode,a.percentage  from  agent_lottery_model_config a where a.station_id = :stationId and a.lot_code= :lotCode", map);
	}
}
