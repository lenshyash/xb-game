package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentArticle;
import com.game.model.vo.ArticleVo;
import com.game.util.StationUtil;

@Repository
public class AgentArticleDao extends JdbcUtilImpl<AgentArticle> {

	static private String help_code = "1,2,3,4,5,6,7,18,21,20";
	
	/**
	 * 根据站点id与code查询公告
	 */
	public Page page(Long stationId, Integer code) {
		StringBuilder sb = new StringBuilder("select * from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (Validator.isNotNull(code)) {
			sb.append(" and code = :code");
			map.put("code", code);
		}
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by code asc, sort asc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public List<AgentArticle> list(Long stationId, Integer code) {
		StringBuilder sb = new StringBuilder("select * from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (Validator.isNotNull(code)) {
			sb.append(" and code = :code");
			map.put("code", code);
		}
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and model_status = :modelStatus");
		map.put("modelStatus", 2);
		sb.append(" order by code asc, sort asc");
		return super.query2Model(sb.toString(), map);
	}

	/**
	 * 根据站点id与code查询公告
	 */
	public List getAtcsLst(ArticleVo avo) {
		StringBuilder sb = new StringBuilder("select * from agent_article where 1=1 and model_status = 2");
		Map<Object, Object> map = new HashMap<Object, Object>();
		Long stationId = avo.getStationId();
		List<Long> codes = avo.getCodes();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		if (codes != null && codes.size() > 0) {
			sb.append(" AND code IN (");
			for (int i = 0; i < codes.size(); i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append(":code" + i);
				map.put("code" + i, codes.get(i));
			}
			sb.append(")");
		}
		sb.append(" order by code asc, sort asc");
		return super.selectCamelListMap(sb.toString(), map);
	}
	
	public List<Map<String,Object>> getArticleForMobile(Integer code, Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select title,content,code from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(code != null){
			sb.append(" and code = :code");
			map.put("code", code);
		}else{
			sb.append(" and code in ("+ help_code +")");
		}
		
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		
		if(overTime != null){
			sb.append(" and over_time >= :overTime");
			map.put("overTime", overTime);
		}
		
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		sb.append(" and model_status = 2");
		sb.append(" order by sort asc");
		return super.selectCamelListMap(sb.toString(), map);
	}

	public Integer getModelStatus(Long id) {
		StringBuilder sb = new StringBuilder("select model_status from agent_article where id = " + id);
		return queryForInt(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public Long getStationId(Long id) {
		StringBuilder sb = new StringBuilder("select station_id from agent_article where id = " + id);
		return queryForLong(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_article set model_status = :modelStatus where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("modelStatus", modelStatus, "id", id, "stationId", stationId));
	}

	public void openCloseK(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_article set status = :status where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "id", id, "stationId", stationId));
	}

	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from agent_article where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}

	public List<AgentArticle> getArticle(Integer code, Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select * from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		
		sb.append(" and code = :code");
		map.put("code", code);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		sb.append(" and model_status = 2");
		sb.append(" order by sort asc");
		return super.query2Model(sb.toString(), map);
	}
	/**
	 * 根据站点id与code查询公告
	 */
	public Page listByPage(Long stationId, Integer code) {
		StringBuilder sb = new StringBuilder("select id,title,code,update_time,over_time from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (Validator.isNotNull(code)) {
			sb.append(" and code = :code");
			map.put("code", code);
		}
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		sb.append(" and status = 2");
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by code asc, sort asc");
		return super.page2CamelMap(sb.toString(), map);
	}
	
	/**
	 *根据站点id和code数组 
	 * @param code
	 * @param stationId
	 * @param overTime
	 * @return
	 */
	public List<AgentArticle> getArticle(Integer[] code, Long stationId, Date overTime) {
		StringBuilder sb = new StringBuilder("select * from agent_article where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		if(!StationUtil.isAgentStation() && StationUtil.isDomainFolderSource()) {
			sb.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}
		sb.append(" and (");
		for(Integer c:code){
			sb.append(" code =:code").append(c).append(" or");
			map.put("code"+c, c);
		}
		String substring = sb.substring(0, sb.length()-3);
		substring=substring+")";
		
		sb = new StringBuilder(substring);
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		sb.append(" and model_status = 2");
		sb.append(" order by sort asc");
		return super.query2Model(sb.toString(), map);
	}

}
