package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentLunBo;
import com.game.util.StationUtil;

@Repository
public class AgentLunBoDao extends JdbcUtilImpl<AgentLunBo> {

	public Page page(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from agent_lunbo where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by pai_xu asc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public Integer getStatus(Long id) {
		StringBuilder sb = new StringBuilder("select status from agent_lunbo where id = " + id);
		return queryForInt(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public Long getStationId(Long id) {
		StringBuilder sb = new StringBuilder("select station_id from agent_lunbo where id = " + id);
		return queryForLong(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_lunbo set model_status = :modelStatus where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("modelStatus", modelStatus, "id", id, "stationId", stationId));
	}

	public void openCloseK(Integer status, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update agent_lunbo set status = :status where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("status", status, "id", id, "stationId", stationId));
	}

	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from agent_lunbo where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}
	
	public List<AgentLunBo> getLunBo(Long stationId,Date overTime,Integer... code){
		StringBuilder sb = new StringBuilder("select l.* ");
		StringBuilder tabs = new StringBuilder(" from agent_lunbo l");
		StringBuilder whes = new StringBuilder(" where model_status=2");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if(StationUtil.isDomainFolderSource()) {
			whes.append(" and (COALESCE(folder_code,:folder) = :folder OR folder_code ='')");
			map.put("folder", StationUtil.getDomainFolder());
		}

		whes.append(" and l.station_id = :stationId");
		map.put("stationId", stationId);
		whes.append(" and over_time >= :overTime");
		map.put("overTime", overTime);
		if(code != null && code.length != 0){
			whes.append(" and (");
			for(Integer c:code){
				whes.append(" code =:code").append(c).append(" or");
				map.put("code"+c, c);
			}
			whes.delete(whes.length()-3, whes.length());
			whes.append(" ) ");
		}
		whes.append(" order by pai_xu asc");
		return super.query2Model(sb.append(tabs).append(whes).toString(), map);
	}

}
