package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.asn1.esf.SPUserNotice;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;
import com.game.util.StationUtil;

@Repository
public class AgentKeFuDao extends JdbcUtilImpl<AgentKeFu> {

	public Page page(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from agent_kefu where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by id desc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from agent_kefu where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}
	
	public void openCloseH(Long id,Integer status) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE agent_kefu set status = :status WHERE id = :id");
		Map<Object, Object> paramMap = MixUtil.newHashMap("id", id, "status", status);
		super.update(sb.toString(), paramMap);
	}
	
	public List<AgentKeFu> getActiveList(Long stationId,Integer type){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from agent_kefu where status = 2 and station_id = :stationId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId", stationId );

		if(Validator.isNotNull(type)) {
			sb.append(" and type = :type");
			paramMap.put("type", type);
		}
		
		return super.query2Model(sb.toString(), paramMap);
	}
}
