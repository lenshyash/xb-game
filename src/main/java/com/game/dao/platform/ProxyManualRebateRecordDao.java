package com.game.dao.platform;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.ProxyManualRebateRecord;

@Repository
public class ProxyManualRebateRecordDao extends JdbcUtilImpl<ProxyManualRebateRecord> {
	
	public Page page(Date startTime, Date endTime, String account, Long stationId){
		StringBuilder sb = new StringBuilder("select pbs.*,a.account from proxy_manual_rebate_record pbs");
		Map<Object, Object> map = MixUtil.newHashMap();
		sb.append(" JOIN sys_account a on a.id = pbs.account_id");
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and a.account = :account");
			map.put("account", account);
		}

		if (Validator.isNotNull(startTime)) {
			sb.append(" and pbs.start_date >= :startTime");
			map.put("startTime", startTime);
		}
		
		if (Validator.isNotNull(endTime)) {
			sb.append(" and pbs.end_date <= :endTime");
			map.put("endTime", endTime);
		}
		
		sb.append(" and pbs.station_id = :stationId");
		map.put("stationId", stationId);
		return super.page2CamelMap(sb.toString(), map);
	}
	
}
