package com.game.dao.platform;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.platform.MemberBackwaterStrategy;

@Repository
public class MemberBackwaterStrategyDao extends JdbcUtilImpl<MemberBackwaterStrategy> {

	public Page page(Integer type, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("type", type);
		StringBuilder sql = new StringBuilder("select * from member_backwater_strategy where 1=1");
		if (type != null && type > 0) {
			sql.append(" and type=:type");
		}
		if (stationId != null && stationId > 0L) {
			sql.append(" and station_id=:stationId");
		}
		sql.append(" order by type asc,rate desc,qualified asc");
		return paged2Obj(sql.toString(), map);
	}

	public void closeOrOpen(Integer status, Long stationId, Long id) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("status", status);
		map.put("id", id);
		update("update member_backwater_strategy set status=:status where station_id=:stationId and id=:id", map);
		CacheUtil.delCacheByPrefix(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + stationId + "_bt_");
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + stationId + "_s_" + id+"_level_group");
	}

	@Override
	public MemberBackwaterStrategy insert(MemberBackwaterStrategy t) {
		t = super.insert(t);
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + t.getStationId() + "_bt_" + t.getType());
		return t;
	}

	@Override
	public int update(MemberBackwaterStrategy t) {
		int f = super.update(t);
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + t.getStationId() + "_bt_" + t.getType());
		return f;
	}

	public MemberBackwaterStrategy getOne(Long id, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("id", id);
		return query21Model("select * from member_backwater_strategy where station_id=:stationId and id=:id", map);
	}

	public int delete(MemberBackwaterStrategy old) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", old.getStationId());
		map.put("id", old.getId());
		int f = update("delete from member_backwater_strategy where station_id=:stationId and id=:id", map);
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + old.getStationId() + "_bt_" + old.getType());
		
		return f;
	}

	public List<MemberBackwaterStrategy> findByTypeFromCache(Integer betType, Long stationId) {
		if (stationId == null || betType == null) {
			return null;
		}
		String key = "s_" + stationId + "_bt_" + betType;
		String nodeStr = CacheUtil.getCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, String.class);
		List<MemberBackwaterStrategy> node = null;
		if (nodeStr != null) {
			node = JSON.parseArray(nodeStr, MemberBackwaterStrategy.class);
		}
		if (node == null || node.isEmpty()) {
			Map<String, Object> map = new HashMap<>();
			map.put("stationId", stationId);
			map.put("betType", betType);
			node = query2Model("select * from member_backwater_strategy where station_id=:stationId and type=:betType and status=2 order by qualified asc", map);
			if (node != null && !node.isEmpty()) {
				CacheUtil.addCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, node);
			}
		}
		return node;
	}
	

	public List<MemberBackwaterStrategy> findByTypeFromCache(Integer betType, Long stationId, Long roomId) {
		if(stationId == null || betType == null || roomId == null){
			return null;
		}
		String key = "s_" + stationId + "_bt_" + betType + "_roomId_" + roomId;
		String nodeStr = CacheUtil.getCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, String.class);
		List<MemberBackwaterStrategy> node = null;
		if (nodeStr != null) {
			node = JSON.parseArray(nodeStr, MemberBackwaterStrategy.class);
		}
		if (node == null || node.isEmpty()) {
			Map<String, Object> map = new HashMap<>();
			map.put("stationId", stationId);
			map.put("betType", betType);
			map.put("roomId", roomId);
			node = query2Model("select * from member_backwater_strategy where station_id=:stationId and type=:betType and room_id=:roomId and status=2 order by qualified asc", map);
			if (node != null && !node.isEmpty()) {
				CacheUtil.addCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, key, node);
			}
		}
		return node;
	}

	public BigDecimal getLotteryFanShui(Long stationId) {
		String r = queryForString("select rate from member_backwater_strategy where station_id = :stationId and status = 2 and type=1 ORDER BY rate desc limit 1", MixUtil.newHashMap("stationId", stationId));
		if (r != null) {
			try{
				return new BigDecimal(r);
			}catch(Exception e){
			}
		}
		return BigDecimal.ZERO;
	}

}
