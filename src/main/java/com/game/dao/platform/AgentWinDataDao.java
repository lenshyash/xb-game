package com.game.dao.platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentWinData;

@Repository
public class AgentWinDataDao extends JdbcUtilImpl<AgentWinData>{
	
	public Page page(Long stationId) {
		StringBuilder sb = new StringBuilder("select * from agent_win_data where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by win_money desc");
		return super.page2CamelMap(sb.toString(), map);
	}
	
	public void delete(Long id ,Long stationId){
		StringBuilder sb = new StringBuilder("delete from agent_win_data where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}

	public List<AgentWinData> getWinData(Long stationId) {
		StringBuilder sql = new StringBuilder("select * from agent_win_data where 1 = 1");
		sql.append(" and station_id = :stationId");
		sql.append(" order by win_money desc");
		Map<Object,Object> map = new HashMap<Object,Object>();
		map.put("stationId", stationId);
		return query2Model(sql.toString(), map);
	}
	
}
