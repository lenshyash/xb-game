package com.game.dao.platform;

import java.util.HashMap;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentAppKeyConfig;
import com.game.model.platform.AgentLunBo;

@Repository
public class AgentAppKeyConfigDao extends JdbcUtilImpl<AgentAppKeyConfig>
{

	public Page page(Long stationId)
	{
		StringBuilder sb = new StringBuilder("select * from agent_app_key_config where 1=1");
		Map<Object, Object> map = new HashMap<Object, Object>();
		sb.append(" and station_id = :stationId");
		map.put("stationId", stationId);
		sb.append(" order by version desc");
		return super.page2CamelMap(sb.toString(), map);
	}

	public Long getStationId(Long id)
	{
		StringBuilder sb = new StringBuilder("select station_id from agent_app_key_config where id = " + id);
		return queryForLong(sb.toString(), MixUtil.newHashMap("id", id));
	}

	public void openCloseH(Long id, Long stationId,Long status)
	{
		StringBuilder sb = new StringBuilder("update agent_app_key_config set status = :status  where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("status",status, "id", id, "stationId", stationId));
	}

	public void delete(Long id, Long stationId)
	{
		StringBuilder sb = new StringBuilder("delete from agent_app_key_config where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}

	public AgentAppKeyConfig getVersionAndStationId(String version, Long stationId)
	{
		StringBuilder sb = new StringBuilder("select * from agent_app_key_config where version = :version");
		sb.append(" and station_id=:stationId");
		return super.query21Model(sb.toString(), MixUtil.newHashMap( "version", version, "stationId", stationId));
	}

}
