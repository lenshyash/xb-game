package com.game.dao.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.asn1.esf.SPUserNotice;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentAdressBook;
import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;
import com.game.model.platform.AdminBlackIp;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.util.StationUtil;

@Repository
public class AdminBlackIpDao extends JdbcUtilImpl<AdminBlackIp> {
	
	public Page<AdminBlackIp> getPage(String ip) {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from admin_black_ip where 1=1");
		HashMap<String,Object> hashMap = new HashMap<>();
		
		if(StringUtil.isNotEmpty(ip)) {
			buffer.append(" and ip=:ip");
			hashMap.put("ip", ip);
		}
		
		buffer.append("  ORDER BY create_datetime desc");
		
		return super.paged2Obj(buffer.toString(), hashMap);
	}
	
	public AdminBlackIp getOne(String ip){
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from admin_black_ip where 1=1  and ip=:ip limit 1");
		
		AdminBlackIp blackIp = super.query21Model(buffer.toString(), MixUtil.newHashMap("ip", ip));
		
		return blackIp;
		
	}
	
	public void addOne(AdminBlackIp adminBlackIp) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("INSERT INTO admin_black_ip (ip, create_datetime) SELECT :ip, :createDatetime WHERE NOT EXISTS(SELECT id FROM admin_black_ip WHERE ip=:ip) limit 1");
		
		super.update(sql_sb.toString(), MixUtil.newHashMap("ip", adminBlackIp.getIp(), "createDatetime", adminBlackIp.getCreateDatetime()));
		
	}
	
	public List<AdminBlackIp> getList(){
		return super.query2Model("select * from admin_black_ip");
	}
}
