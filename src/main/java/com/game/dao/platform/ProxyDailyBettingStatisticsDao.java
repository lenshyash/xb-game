package com.game.dao.platform;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.model.vo.AccountVo;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.model.platform.ProxyDailyBettingStatistics;
import com.game.util.BigDecimalUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Repository
public class ProxyDailyBettingStatisticsDao extends JdbcUtilImpl<ProxyDailyBettingStatistics> {

	public void updateAmount(ProxyDailyBettingStatistics dbs) {
		Map<String, Object> map = new HashMap<>();
		StringBuilder sql = new StringBuilder("update proxy_daily_betting_statistics set ");
		if (dbs.getLotteryBetAmount() != null) {
			sql.append(" lottery_bet_amount= :lotteryBetAmount,");
			map.put("lotteryBetAmount", dbs.getLotteryBetAmount());
		}
		if (dbs.getLotteryProfitAmount() != null) {
			sql.append(" lottery_profit_amount= :lotteryProfitAmount,");
			map.put("lotteryProfitAmount", dbs.getLotteryProfitAmount());
		}
		if (dbs.getLotteryRebateAmount() != null) {
			sql.append(" lottery_rebate_amount= :lotteryRebateAmount,");
			map.put("lotteryRebateAmount", dbs.getLotteryRebateAmount());
		}
		if (dbs.getSportsBetAmount() != null) {
			sql.append(" sports_bet_amount= :sportsBetAmount,");
			map.put("sportsBetAmount", dbs.getSportsBetAmount());
		}
		if (dbs.getSportsProfitAmount() != null) {
			sql.append(" sports_profit_amount= :sportsProfitAmount,");
			map.put("sportsProfitAmount", dbs.getSportsProfitAmount());
		}
		if (dbs.getSportsRebateAmount() != null) {
			sql.append(" sports_rebate_amount= :sportsRebateAmount,");
			map.put("sportsRebateAmount", dbs.getSportsRebateAmount());
		}
		if (dbs.getEgameBetAmount() != null) {
			sql.append(" egame_bet_amount= :egameBetAmount,");
			map.put("egameBetAmount", dbs.getEgameBetAmount());
		}
		if (dbs.getEgameProfitAmount() != null) {
			sql.append(" egame_profit_amount= :egameProfitAmount,");
			map.put("egameProfitAmount", dbs.getEgameProfitAmount());
		}
		if (dbs.getEgameRebateAmount() != null) {
			sql.append(" egame_rebate_amount= :egameRebateAmount,");
			map.put("egameRebateAmount", dbs.getEgameRebateAmount());
		}
		if (dbs.getRealBetAmount() != null) {
			sql.append(" real_bet_amount= :realBetAmount,");
			map.put("realBetAmount", dbs.getRealBetAmount());
		}
		if (dbs.getRealProfitAmount() != null) {
			sql.append(" real_profit_amount= :realProfitAmount,");
			map.put("realProfitAmount", dbs.getRealProfitAmount());
		}
		if (dbs.getRealRebateAmount() != null) {
			sql.append(" real_rebate_amount= :realRebateAmount,");
			map.put("realRebateAmount", dbs.getRealRebateAmount());
		}
		if (dbs.getMarkSixBetAmount() != null) {
			sql.append(" mark_six_bet_amount= :markSixBetAmount,");
			map.put("markSixBetAmount", dbs.getMarkSixBetAmount());
		}
		if (dbs.getMarkSixProfitAmount() != null) {
			sql.append(" mark_six_profit_amount= :markSixProfitAmount,");
			map.put("markSixProfitAmount", dbs.getMarkSixProfitAmount());
		}
		if (dbs.getMarkSixRebateAmount() != null) {
			sql.append(" mark_six_rebate_amount= :markSixRebateAmount,");
			map.put("markSixRebateAmount", dbs.getMarkSixRebateAmount());
		}
		if (sql.toString().endsWith(",")) {
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" where account_id=:accountId and stat_date=:statDate");
			map.put("accountId", dbs.getAccountId());
			map.put("statDate", dbs.getStatDate());
		}else{
			return;
		}
		super.update(sql.toString(), map);
	}

	public Page page(Date startTime, Date endTime, String account, Long stationId,Integer status,Integer searchType) {
			StringBuilder sb = new StringBuilder("select d.id,d.stat_date,d.lottery_bet_amount,d.mark_six_bet_amount,");
			sb.append("d.sports_profit_amount,d.real_profit_amount,d.egame_profit_amount,d.status,p.lottery_rebate,p.mark_six_rebate,d.chess_profit_amount,d.esports_profit_amount, ");
			
			if("profit".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.proxy_daily_rebate))){
				sb = new StringBuilder("select d.id,d.stat_date,d.lottery_profit_amount,d.mark_six_profit_amount,");
				sb.append("d.sports_profit_amount,d.real_profit_amount,d.egame_profit_amount,d.status,p.lottery_rebate,p.mark_six_rebate,d.chess_profit_amount,d.esports_profit_amount, ");
			}
			sb.append("p.real_rebate,p.egame_rebate,p.sport_rebate,p.chess_rebate,p.esports_rebate,a.account,d.account_id,");
			sb.append("d.sports_profit_amount*p.sport_rebate/100 as sport_rebate_amount,");
			sb.append("d.real_profit_amount*p.real_rebate/100 as real_rebate_amount,");
			sb.append("d.egame_profit_amount*p.egame_rebate/100 as egame_rebate_amount,");
			sb.append("d.lottery_bet_amount*p.lottery_rebate/100 as lottery_rebate_amount,");
			sb.append("d.mark_six_bet_amount*p.mark_six_rebate/100 as mark_six_rebate_amount,");
			sb.append("d.chess_profit_amount*p.chess_rebate/100 as chess_rebate_amount,");
			sb.append("d.esports_profit_amount*p.esports_rebate/100 as esports_rebate_amount");
			sb.append(" from proxy_daily_betting_statistics d left join proxy_manual_rebate_record  p on d.rebate_record_id=p.id");
			sb.append(" left join  sys_account a on a.id = d.account_id where d.station_id = :stationId");
			Map<Object, Object> map = MixUtil.newHashMap();
			map.put("stationId", stationId);
			if (StringUtils.isNoneEmpty(account)) {
				if(searchType==null || searchType==0){
					map.put("account", account + "%");
					sb.append(" AND a.account LIKE :account");
				}else if(searchType== AccountVo.SEARCHTYPE_NEXT){
					map.put("account", account);
					map.put("parent_names", "%," + account + ",");
					sb.append(" AND (a.account = :account or a.parent_names LIKE :parent_names )");
				}else if(searchType==AccountVo.SEARCHTYPE_NEXT_ALL){
					map.put("account", account);
					map.put("parent_names", "%," + account+ ",%");
					sb.append(" AND (a.account = :account or a.parent_names LIKE :parent_names )");
				}

				sb.append(" and lower(a.account) = :account");
				map.put("account", account.toLowerCase());
			}
			if (status!=null) {
				sb.append(" and d.status = :status");
				map.put("status",status);
			}

			sb.append(" and d.stat_date BETWEEN :startTime and :endTime order by stat_date desc");
			map.put("startTime", startTime);
			map.put("endTime", endTime);
			return super.page2CamelMap(sb.toString(), map);
	}

	public int updateRebateInfo(Long id, Long accountId, Integer oldStatus, Integer newStatus, Date now) {
		Map<Object, Object> map = MixUtil.newHashMap();
		map.put("id", id);
		map.put("accountId", accountId);
		map.put("oldStatus", oldStatus);
		map.put("newStatus", newStatus);
		map.put("now", now);
		return update("update proxy_daily_betting_statistics set status=:newStatus,rebate_datetime=:now where id=:id and account_id=:accountId and status=:oldStatus", map);
	}

	public List<ProxyDailyBettingStatistics> findByIdsAndStatusAndAccountId(List<Long> idList, Integer status, Long accountId) {
		if (idList == null || idList.isEmpty())
			return null;
		StringBuilder sb = new StringBuilder("select * from proxy_daily_betting_statistics where ");
		sb.append("account_id=").append(accountId);
		sb.append(" and status=").append(status);
		sb.append(" and id in(");
		for (Long id : idList) {
			sb.append(id).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return query2Model(sb.toString());
	}

	public int updateRecordId(List<Long> idList, Long accountId, Long rebateRecordId) {
		if (idList == null || idList.isEmpty() || rebateRecordId == null)
			return 0;
		StringBuilder sb = new StringBuilder("update proxy_daily_betting_statistics set rebate_record_id=");
		sb.append(rebateRecordId).append(" where ");
		sb.append("account_id=").append(accountId);
		sb.append(" and id in(");
		for (Long id : idList) {
			sb.append(id).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return update(sb.toString());
	}

	public ProxyDailyBettingStatistics getByRidAndStationId(Long rid, Long stationId) {
		StringBuilder sb = new StringBuilder("select * from proxy_daily_betting_statistics where station_id=:stationId and rebate_record_id = :rid");
		return super.query21Model(sb.toString(), MixUtil.newHashMap("rid", rid, "stationId", stationId));
	}

	public boolean exist(Date date, Long accountId) {
		String sql = "select count(*) from proxy_daily_betting_statistics where stat_date=:date and account_id=:accountId";
		return queryForInt(sql, MixUtil.newHashMap("date", date, "accountId", accountId)) > 0;
	}

	public int addThirdAmount(ProxyDailyBettingStatistics dbs) {
		Map<String, Object> map = new HashMap<>();
		StringBuilder sql = new StringBuilder("update proxy_daily_betting_statistics set ");
		if (dbs.getEgameBetAmount() != null) {
			sql.append(" egame_bet_amount=coalesce(egame_bet_amount,0)+:egameBetAmount,");
			map.put("egameBetAmount", dbs.getEgameBetAmount());
		}
		if (dbs.getEgameProfitAmount() != null) {
			sql.append(" egame_profit_amount=coalesce(egame_profit_amount,0)+:egameProfitAmount,");
			map.put("egameProfitAmount", dbs.getEgameProfitAmount());
		}
		if (dbs.getEgameRebateAmount() != null) {
			sql.append(" egame_rebate_amount=coalesce(egame_rebate_amount,0)+:egameRebateAmount,");
			map.put("egameRebateAmount", dbs.getEgameRebateAmount());
		}
		if (dbs.getRealBetAmount() != null) {
			sql.append(" real_bet_amount=coalesce(real_bet_amount,0)+:realBetAmount,");
			map.put("realBetAmount", dbs.getRealBetAmount());
		}
		if (dbs.getRealProfitAmount() != null) {
			sql.append(" real_profit_amount=coalesce(real_profit_amount,0)+:realProfitAmount,");
			map.put("realProfitAmount", dbs.getRealProfitAmount());
		}
		if (dbs.getRealRebateAmount() != null) {
			sql.append(" real_rebate_amount=coalesce(real_rebate_amount,0)+:realRebateAmount,");
			map.put("realRebateAmount", dbs.getRealRebateAmount());
		}
		if (sql.toString().endsWith(",")) {
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" where account_id=:accountId and stat_date=:statDate");
			map.put("accountId", dbs.getAccountId());
			map.put("statDate", dbs.getStatDate());
		}
		return super.update(sql.toString(), map);
	}

	public int saveOrAddThirdAmount(ProxyDailyBettingStatistics dbs) {
		if (exist(dbs.getStatDate(), dbs.getAccountId())) {
			return addThirdAmount(dbs);
		} else {
			try {
				insert(dbs);
				return 1;
			} catch (Exception e) {
				return addThirdAmount(dbs);
			}
		}
	}

	public Map<Long, Map> statisticForOneDay(Date day) {
		StringBuilder sql = new StringBuilder("SELECT station_id,sum(real_bet_amount) as real_bet_amount,");
		sql.append("sum(real_profit_amount) as real_profit_amount,sum(real_rebate_amount) as real_rebate_amount");
		sql.append(",sum(egame_bet_amount) as egame_bet_amount,sum(egame_profit_amount) as egame_profit_amount");
		sql.append(",sum(egame_rebate_amount) as egame_rebate_amount from proxy_daily_betting_statistics");
		sql.append(" where stat_date=:day group by station_id");
		List list = selectCamelListMap(sql.toString(), MixUtil.newHashMap("day", day));
		Map<Long, Map> map = new HashMap<>();
		if (list != null && !list.isEmpty()) {
			Map m = null;
			Long stationId = null;
			for (int i = 0, len = list.size(); i < len; i++) {
				m = (Map) list.get(i);
				if (m.get("stationId") != null) {
					try {
						stationId = new Long((Integer) m.get("stationId"));
					} catch (Exception e) {
						stationId = (Long) m.get("stationId");
					}
					m.put("realAwardAmount", BigDecimalUtil.subtract((BigDecimal) m.get("realBetAmount"), (BigDecimal) m.get("realProfitAmount"), (BigDecimal) m.get("realRebateAmount")));
					m.put("egameAwardAmount", BigDecimalUtil.subtract((BigDecimal) m.get("egameBetAmount"), (BigDecimal) m.get("egameProfitAmount"), (BigDecimal) m.get("egameRebateAmount")));
					map.put(stationId, m);
				}
			}
		}
		return map;
	}

}
