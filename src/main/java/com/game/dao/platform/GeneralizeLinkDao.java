package com.game.dao.platform;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.platform.MemberGenrtLink;

@Repository
public class GeneralizeLinkDao extends JdbcUtilImpl<MemberGenrtLink>{
	
	public Page page(Long stationId,String userAccount,Integer type , String queryLinkKey){
		StringBuilder sb = new StringBuilder("select * from member_genrt_link where 1=1");
		Map<Object,Object> map = MixUtil.newHashMap();
		if(!StringUtils.isEmpty(userAccount)){
			sb.append(" and user_account = :userAccount");
			map.put("userAccount", userAccount);
		}
		if(!StringUtils.isEmpty(queryLinkKey)){
			sb.append(" and link_key = :linkKey");
			map.put("linkKey", queryLinkKey);
		}
		if(type != null && type >0){
			sb.append(" and type = :type");
			map.put("type", type);
		}
		
		sb.append(" and station_id = :stationId");
		sb.append(" order by id desc");
		map.put("stationId", stationId);
		return super.page2CamelMap(sb.toString(),map);
	}
	
	public MemberGenrtLink getByLinkKeyAndStationId(Long stationId , String linkKey) {
		StringBuilder sb = new StringBuilder("select * from member_genrt_link where 1=1");
		Map<Object,Object> map = MixUtil.newHashMap();
		if(stationId != null){
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		
		if(linkKey != null){
			sb.append(" and link_key = :linkKey");
			map.put("linkKey", linkKey);
		}
		return super.query21Model(sb.toString(),map);
	}
	
	public List<MemberGenrtLink> getListLink(Long stationId , Long accountId, Long defaultLink){
		StringBuilder sb = new StringBuilder("select * from member_genrt_link where 1=1");
		Map<Object,Object> map = MixUtil.newHashMap();
		if(Validator.isNotNull(stationId)){
			sb.append(" and station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(Validator.isNotNull(accountId)){
			sb.append(" and user_id = :accountId");
			map.put("accountId", accountId);
		}
		if(Validator.isNotNull(defaultLink)){
			sb.append(" and default_Link = :defaultLink");
			map.put("defaultLink", defaultLink);
		}
		sb.append(" order by id desc");
		return super.query2Model(sb.toString(), map);
	}
	
	public void updateNullDefaultLink(Long stationId , Long accountId, Integer type) {
		StringBuilder sb = new StringBuilder();
		Map<Object,Object> map = MixUtil.newHashMap();
		sb.append("update member_genrt_link set default_link = 1");
		if(Validator.isNotNull(stationId)){
			sb.append(" where station_id = :stationId");
			map.put("stationId", stationId);
		}
		if(Validator.isNotNull(accountId)){
			sb.append(" and user_id = :accountId");
			map.put("accountId", accountId);
		}
		if(Validator.isNotNull(type)) {
			sb.append(" and type = :type");
			map.put("type", type);
		}
		super.update(sb.toString(), map);
	}
}
