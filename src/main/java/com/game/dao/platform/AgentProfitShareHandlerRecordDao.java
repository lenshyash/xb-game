package com.game.dao.platform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.platform.AgentProfitShareHandlerRecord;
import com.game.util.UserUtil;

@Repository
public class AgentProfitShareHandlerRecordDao extends JdbcUtilImpl<AgentProfitShareHandlerRecord> {
	public Page page(String account,String agentName,String children, Date startDate, Date endDate, Integer status, Long stationId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("account", StringUtils.isEmpty(account) ? "" : account.toLowerCase());
		map.put("stationId", stationId);
		map.put("status", status);
		StringBuilder sb = new StringBuilder("select r.*");
		sb.append(" from agent_profit_share_handler_record r where 1=1");
		if (stationId != null) {
			sb.append(" and station_id=:stationId");
		}
		if (StringUtils.isNotEmpty(account)) {
			sb.append(" and lower(account) = :account");
			map.put("account", account);
		}
		if (StringUtils.isNotEmpty(agentName)) {
			sb.append(" and lower(agent_name) = :agentName");
			map.put("agentName", agentName);
		}
		if (StringUtils.isNotEmpty(children)) {
			sb.append(" and parents LIKE :children");
			map.put("children", children+"%");
		}
		if (startDate != null) {
			sb.append(" and modify_datetime>=:startDate");
		}
		if (endDate != null) {
			sb.append(" and modify_datetime<:endDate");
		}
		if (status != null && status > 0) {
			sb.append(" and status=:status");
		}
		sb.append(" order by create_datetime desc");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, "money", "money"));
		return super.page2CamelMap(sb.toString(), map, aggs);
	}

	public AgentProfitShareHandlerRecord queryNoHandlerRecord(Long accountId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("accountId", accountId);
		map.put("status", AgentProfitShareHandlerRecord.STATUS_NO);
		return query21Model(
				"SELECT * FROM agent_profit_share_handler_record WHERE account_id =:accountId AND status = :status",
				map);
	}
	
	public int updateMoney(Long recordId,BigDecimal money,BigDecimal oldMoney,String remark) {
		Map map = MixUtil.newHashMap("money", money, "recordId", recordId,"oldMoney",oldMoney,"remark",remark);
		StringBuilder sb = new StringBuilder("UPDATE agent_profit_share_handler_record SET ");
		sb.append(" money = money+ :money");
		sb.append(" ,modify_datetime = :modifyDatetime");
		sb.append(" ,remark = :remark");
		map.put("modifyDatetime", new Date());
		sb.append(" WHERE id = :recordId AND money = :oldMoney");
		return super.update(sb.toString(), map);
	}

	public int updateToHandler(Long recordId, Long stationId,BigDecimal money,String remark) {
		Map map = MixUtil.newHashMap("stationId", stationId, "recordId", recordId,"remark",remark);
		StringBuilder sb = new StringBuilder("UPDATE agent_profit_share_handler_record SET ");
		sb.append(" status = ").append(AgentProfitShareHandlerRecord.STATUS_OK);
		sb.append(" ,money = :money");
		sb.append(" ,modify_datetime = :modifyDatetime");
		sb.append(" ,remark = :remark");
		map.put("modifyDatetime", new Date());
		map.put("money", money);
		if (!UserUtil.isSuperAgent()) {
			sb.append(" ,operator = :operator");
			map.put("operator", UserUtil.getUserAccount());
		}
		sb.append(" WHERE id = :recordId and status =").append(AgentProfitShareHandlerRecord.STATUS_NO);
		return super.update(sb.toString(), map);
	}
}
