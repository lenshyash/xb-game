package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MemberActive;
import com.game.model.vo.ActiveVo;

@Repository
public class MemberActiveDao extends JdbcUtilImpl<MemberActive> {

	public Page getActivePage(ActiveVo avo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active a");
		sql_sb.append(" WHERE a.station_id = :stationId");

		Map paramMap = MixUtil.newHashMap("stationId", avo.getStationId());
		if (Validator.isNotNull(avo.getActiveType())) {
			sql_sb.append(" AND a.active_type = :acitveType");
			paramMap.put("acitveType", avo.getActiveType());
		}
		
		if (StringUtil.isNotEmpty(avo.getBegin())) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", avo.getBegin());
		}

		if (StringUtil.isNotEmpty(avo.getEnd())) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", avo.getEnd());
		}
		
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<MemberActive> getActivesList(ActiveVo avo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active a");
		sql_sb.append(" WHERE a.station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", avo.getStationId());

		if (Validator.isNotNull(avo.getActiveType())) {
			sql_sb.append(" AND a.active_type = :acitveType");
			paramMap.put("acitveType", avo.getActiveType());
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public MemberActive getProgressActive(ActiveVo avo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM member_active a");
		sql_sb.append(" WHERE a.station_id = :stationId");
		Map paramMap = MixUtil.newHashMap("stationId", avo.getStationId());

		if (Validator.isNotNull(avo.getActiveType())) {
			sql_sb.append(" AND a.active_type = :acitveType");
			paramMap.put("acitveType", avo.getActiveType());
		}

		if (Validator.isNotNull(avo.getStatus())) {
			sql_sb.append(" AND a.status = :status");
			paramMap.put("status", avo.getStatus());
		}
		return super.query21Model(sql_sb.toString(), paramMap);
	}
}
