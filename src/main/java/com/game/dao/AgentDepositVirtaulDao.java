package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentDepositBank;
import com.game.model.AgentDepositOnline;
import com.game.model.AgentDepositVirtual;
import com.game.util.StationUtil;

@Repository
public class AgentDepositVirtaulDao extends JdbcUtilImpl<AgentDepositVirtual> {
	public Page<Map> getPage(String name, Long status) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT o.*,p.name AS pay_name,p.company AS pay_com,o.sort_no,p.icon_css");
		sql_sb.append(" FROM agent_deposit_virtual o LEFT JOIN sys_pay_platform p ON o.pay_platform_id = p.id");
		sql_sb.append(" WHERE o.station_id= :stationId ");

		if (StringUtils.isNotEmpty(name)) {
			sql_sb.append("and pay_platform_id in (select id from sys_pay_platform oo where oo.name like :name) ");
		}

		if (status != null && status > 0) {
			sql_sb.append("AND o.status = :status ");
		}

		sql_sb.append("order by o.sort_no asc, id desc");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", status, "name", "%" + name + "%");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}
	
	public void updateStatus(Integer status, Long id, Long stationId) {
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", status, "id", id);
		update("update agent_deposit_virtual set status=:status where id=:id and station_id=:stationId", paramMap);
	}
	
	public List<AgentDepositVirtual> getVirtuals(Long status){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from agent_deposit_virtual where 1 =1");
		HashMap<String,Object> map = new HashMap<>();
		
		if(Validator.isNotNull(status)) {
			sb.append(" and status =:status");
			map.put("status", status);
		}
		
		return super.query2Model(sb.toString(), map);
		
	}

	public List<Map> getOnlines(String showType, String code) {
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId(), "status", AgentDepositBank.STATUS_ENABLE);

		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT o.*,p.name AS pay_name,p.company AS pay_com,p.icon_css");
		sql_sb.append(" FROM agent_deposit_virtual o LEFT JOIN sys_pay_platform p ON o.pay_platform_id = p.id");
		sql_sb.append(" WHERE o.station_id= :stationId");
		sql_sb.append(" AND o.status= :status");

		if (StringUtils.isNotEmpty(showType)) {
			sql_sb.append(" AND (o.show_type= :showType OR o.show_type='all' OR o.show_type='' OR o.show_type IS NULL)");
			paramMap.put("showType", showType);
		}
		if (StringUtils.isNotEmpty(code)) {
			if(code.indexOf(",")==1) {
				String [] arr = code.split(",");
				sql_sb.append(" AND o.pay_type in (");
				for(int i=0;i<arr.length;i++) {
					sql_sb.append("'"+arr[i]+"'");
					if(i<arr.length-1) {
						sql_sb.append(",");
					}
				}
				sql_sb.append(")");
			}else {
				sql_sb.append(" AND o.pay_type = :code");
				paramMap.put("code", code);
			}
		}
		sql_sb.append(" order by o.sort_no asc");
		return super.selectCamelListMap(sql_sb.toString(), paramMap);

	}
}
