package com.game.dao;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberWechat;

@Repository
public class MemberWechatDao extends JdbcUtilImpl<MemberWechat>{
	
	public MemberWechat getWechatByOpenId(String openId,long stationId){
		String sql = "select * from member_wechat where openid = :openId "
				+ " and station_id = :stationId "
				+ " and account_status = " + MemberWechat.ACCOUNT_STATUS_BINDED;
		return super.query21Model(sql, MixUtil.newHashMap("openId",openId,"stationId",stationId));
	}
}
