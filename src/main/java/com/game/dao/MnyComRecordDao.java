package com.game.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.util.*;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.FrameProperites;
import org.jay.frame.exception.JayFrameException;
import org.jay.frame.jdbc.*;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.model.MnyComRecord;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.ReportParamVo;

@Repository
public class MnyComRecordDao extends JdbcUtilImpl<MnyComRecord> {

	public Page<MnyComRecord> getPage(MnyComRecordVo mcrVo) {
		StringBuilder sql_sb = new StringBuilder("SELECT r.*,i.user_name,a.agent_name,a.account_type,a.report_type,a.abnormal_flag,b.level_name,m.account as modify_user,r.com_times,r.remark,r.virtual_currency_amount  FROM mny_com_record r");
		sql_sb.append(" LEFT JOIN sys_account a on(a.id  = r.member_id) ");
		sql_sb.append(" LEFT JOIN sys_account_info i on(a.id  = i.account_id) ");
		sql_sb.append(" LEFT JOIN member_level_base b on b.id = a.level_group ");
		sql_sb.append(" LEFT JOIN sys_account m on m.id = r.modify_user_id ");
		sql_sb.append(" WHERE r.station_id= :stationId");
		Long stationId = mcrVo.getStationId();
		Long type = mcrVo.getType();
		Long handlerType = mcrVo.getHandlerType();
		Long status = mcrVo.getStatus();
		Long lock = mcrVo.getLockFlag();
		Long accountId = mcrVo.getAccountId();
		String account = StringUtil.trim2Empty(mcrVo.getAccount());
		String orderNo = StringUtil.trim2Empty(mcrVo.getOrderNo());
		BigDecimal minMoney = mcrVo.getMinMoney();
		BigDecimal maxMoney = mcrVo.getMaxMoney();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (Validator.isNotNull(type)) {
			sql_sb.append(" AND r.type= :type");
			paramMap.put("type", type);
		}
		if (Validator.isNotNull(status)) {
			sql_sb.append(" AND r.status= :status");
			paramMap.put("status", status);
		}
		if (StringUtil.isNotEmpty(orderNo)) {
			sql_sb.append(" AND r.order_no= :orderNo");
			paramMap.put("orderNo", orderNo);
		}

		if (Validator.isNotNull(lock)) {
			sql_sb.append(" AND r.lock_flag = :lock");
			paramMap.put("lock", lock);
		}
		if (Validator.isNotNull(handlerType)) {
			sql_sb.append(" AND r.handler_type = :handlerType");
			paramMap.put("handlerType", handlerType);
		}

		if (StringUtil.isNotEmpty(account)) {
			sql_sb.append(" AND lower(r.account) = :account");
			paramMap.put("account", account.toLowerCase());
		}

		if (Validator.isNotNull(accountId)) {
			sql_sb.append(" AND r.member_id = :accountId");
			paramMap.put("accountId", accountId);
		}
		
		if (minMoney != null && minMoney.compareTo(BigDecimal.ZERO) == 1) {
			sql_sb.append(" AND r.money >= :minMoney");
			paramMap.put("minMoney", minMoney);
		}
		
		if (maxMoney != null && maxMoney.compareTo(BigDecimal.ZERO) == 1) {
			sql_sb.append(" AND r.money <= :maxMoney");
			paramMap.put("maxMoney", maxMoney);
		}
		
		if (StringUtils.isNotEmpty(mcrVo.getOperator())) {
			sql_sb.append(" AND m.account = :operator");
			paramMap.put("operator", mcrVo.getOperator());
		}
		
		if (StringUtils.isNotEmpty(mcrVo.getOpDesc())) {
			sql_sb.append(" AND r.op_desc LIKE :opDesc");
			paramMap.put("opDesc", "%"+mcrVo.getOpDesc()+"%");
		}

		if (StringUtils.isNotEmpty(mcrVo.getPaySmallName()) && !"0".equals(mcrVo.getPaySmallName())) {
			sql_sb.append(" AND r.pay_name = :payName");
			paramMap.put("payName", mcrVo.getPaySmallName());
		}

		if (Validator.isNotNull(mcrVo.getBegin())) {
			sql_sb.append(" AND r.create_datetime >= :begin");
			paramMap.put("begin", mcrVo.getBegin());
		}

		if (Validator.isNotNull(mcrVo.getEnd())) {
			sql_sb.append(" AND r.create_datetime <= :end");
			paramMap.put("end", mcrVo.getEnd());
		}
		
		if (Validator.isNotNull(mcrVo.getReportType())) {
			sql_sb.append(" AND a.report_type =:reportType");
			paramMap.put("reportType", mcrVo.getReportType());
		}
		
		if (Validator.isNotNull(mcrVo.getAccountType())) {
			sql_sb.append(" AND a.account_type =:accountType");
			paramMap.put("accountType", mcrVo.getAccountType());
		}
		if (Validator.isNotNull(mcrVo.getComTimes())) {
			sql_sb.append(" AND r.com_times =:comTimes");
			paramMap.put("comTimes", mcrVo.getComTimes());
		}
		if (Validator.isNotNull(mcrVo.getRemark())) {
			sql_sb.append(" AND r.remark LIKE :remark");
			paramMap.put("remark", "%"+mcrVo.getRemark()+"%");
		}
		if (StationUtil.isDailiStation()) {
			if (StringUtil.isNotEmpty(mcrVo.getParents())) {
				if (mcrVo.getSearchSelf() != null && mcrVo.getSearchSelf()) {
					paramMap.put("curUserId", mcrVo.getSelfId());
					sql_sb.append(" AND (a.parents LIKE :parents OR a.id = :curUserId)");
				} else {
					sql_sb.append(" AND a.parents LIKE :parents");
				}
				if (StringUtil.equals(mcrVo.getSearchType(), ReportParamVo.SEARCHTYPE_NEXT)) {
					paramMap.put("parents", mcrVo.getParents());
				}else {
					paramMap.put("parents", mcrVo.getParents()+ "%");
				}
			}
		} else {
			if (StringUtils.isNotEmpty(mcrVo.getAgentName())) {
				sql_sb.append(" AND a.parent_names like :parent ");
				paramMap.put("parent", "%," + mcrVo.getAgentName() + ",%");
			}
		}

		sql_sb.append(" ORDER BY create_datetime DESC");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"CASE WHEN r.status =" + MnyComRecord.STATUS_SUCCESS + " THEN r.money ELSE 0 END ", "totalMoney"));

		return super.paged2Obj(sql_sb.toString(), paramMap, aggs);
	}

	/**
	 * 用作报表
	 * 
	 * @param mcrVo
	 * @return
	 */
	public List<Map> getList(MnyComRecordVo mcrVo) {
		Boolean isAgent = StationUtil.isAgentStation();
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT r.*");

		if (isAgent != null && isAgent) {
			sql_sb.append(",s.agent_name,m.account as modify_user");
		}
		sql_sb.append(" FROM mny_com_record r");

		if (isAgent != null && isAgent) {
			sql_sb.append(" LEFT JOIN sys_account m ON (r.modify_user_id = m.id)");
			sql_sb.append(" LEFT JOIN sys_account s ON (r.member_id = s.id)");
		}

		if (StationUtil.isDailiStation()) {
			sql_sb.append(" LEFT JOIN sys_account a on(a.id  = r.member_id) ");
		}

		sql_sb.append(" WHERE r.station_id= :stationId");
		Long stationId = mcrVo.getStationId();
		Long type = mcrVo.getType();
		Long status = mcrVo.getStatus();
		Long accountId = mcrVo.getAccountId();
		String account = mcrVo.getAccount();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (Validator.isNotNull(type)) {
			sql_sb.append(" AND r.type= :type");
			paramMap.put("type", type);
		}
		if (Validator.isNotNull(status)) {
			sql_sb.append(" AND r.status= :status");
			paramMap.put("status", status);
		}
		if (StringUtil.isNotEmpty(account)) {
			sql_sb.append(" AND r.account LIKE :account");
			paramMap.put("account", account + "%");
		}

		if (Validator.isNotNull(accountId)) {
			sql_sb.append(" AND r.member_id = :accountId");
			paramMap.put("accountId", accountId);
		}

		if (StringUtils.isNotEmpty(mcrVo.getPaySmallName()) && !"0".equals(mcrVo.getPaySmallName())) {
			sql_sb.append(" AND r.pay_name = :payName");
			paramMap.put("payName", mcrVo.getPaySmallName());
		}

		if (Validator.isNotNull(mcrVo.getBegin())) {
			sql_sb.append(" AND r.create_datetime >= :begin");
			paramMap.put("begin", mcrVo.getBegin());
		}

		if (Validator.isNotNull(mcrVo.getEnd())) {
			sql_sb.append(" AND r.create_datetime < :end");
			paramMap.put("end", mcrVo.getEnd());
		}

		if (StationUtil.isDailiStation()) {
			sql_sb.append(" AND a.parents like :parent ");
			paramMap.put("parent", UserUtil.getChildren() + "%");
		}

		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

	public Integer getCountOfUntreated(MnyComRecordVo mcrVo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT count(1)");
		sql_sb.append(" FROM mny_com_record r");
		if (StringUtils.equals("on", StationConfigUtil.get(StationConfig.online_deposit_voice_remind))) {
			sql_sb.append(" where 1=1 ");
		} else {
			sql_sb.append(" WHERE r.type IN(");
			sql_sb.append(MoneyRecordType.DEPOSIT_ONLINE_BANK.getType());
			sql_sb.append(",");
			sql_sb.append(MoneyRecordType.DEPOSIT_ONLINE_FAST.getType());
			sql_sb.append(")");
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(mcrVo.getStationId())) {
			paramMap.put("stationId", mcrVo.getStationId());
			sql_sb.append(" AND station_id=:stationId");
		}

		if (Validator.isNotNull(mcrVo.getStatus())) {
			paramMap.put("status", mcrVo.getStatus());
			sql_sb.append(" AND r.status =:status");
		}

		if (Validator.isNotNull(mcrVo.getLockFlag())) {
			paramMap.put("lock", mcrVo.getLockFlag());
			sql_sb.append(" AND r.lock_flag =:lock");
		}

		return super.queryForInt(sql_sb.toString(), paramMap);
	}

	public Map getLastComMoney(Long userId, Long lastComId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT sum(money) AS money,max(id) AS com_id");
		sql_sb.append(" FROM mny_com_record r");
		sql_sb.append(" WHERE status = ");
		sql_sb.append(MnyComRecord.STATUS_SUCCESS);

		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (Validator.isNotNull(userId)) {
			paramMap.put("userId", userId);
			sql_sb.append(" AND member_id=:userId");
		}

		if (Validator.isNotNull(lastComId)) {
			paramMap.put("lastComId", lastComId);
			sql_sb.append(" AND id >:lastComId");
		}
		sql_sb.append(" GROUP BY member_id");
		return super.selectSingleCamelMap(sql_sb.toString(), paramMap);
	}

	public MnyComRecord getByOrderNo(String orderNo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM mny_com_record");
		sql_sb.append(" WHERE 1=1 ");
		sql_sb.append(" AND order_no=:orderNo");

		return super.query21Model(sql_sb.toString(), MixUtil.newHashMap("orderNo", orderNo));
	}

	/**
	 * 根据站点和投注时间来获得总条数
	 */
	public int delByCreateTimeAndStationId(Date createTime, Long stationId) {
		Map<String, Object> map = new HashMap<>();
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(
				"delete from mny_com_record where station_id = :stationId and create_datetime <= :createTime", map);
	}

	public int updatePayResult(MnyComRecord mcr, Long oldStatus) {
		StringBuilder sql = new StringBuilder("update mny_com_record set ");
		sql.append(
				"status=:status,lock_flag=:lockFlag,handler_type=:handlerType,op_desc=:opDesc,money=:money,remark=:remark,modify_datetime = :modifyDatetime,first_deposit=:firstDeposit,com_times=:comTimes,virtual_currency_amount=:amount,rate=:rate");
		sql.append(" where id=:id and status=:oldStatus");
		Map<String, Object> map = new HashMap<>();
		map.put("status", mcr.getStauts());
		map.put("handlerType", mcr.getHandlerType());
		map.put("lockFlag", mcr.getLockFlag());
		map.put("opDesc", mcr.getOpDesc());
		map.put("money", mcr.getMoney());
		map.put("remark", mcr.getRemark());
		map.put("id", mcr.getId());
		map.put("oldStatus", oldStatus);
		map.put("modifyDatetime", mcr.getModifyDatetime());
		map.put("firstDeposit", mcr.getFirstDeposit());
		map.put("comTimes", mcr.getComTimes());
		map.put("amount", mcr.getVirtualCurrencyAmount());
		map.put("rate", mcr.getRate());
		return super.update(sql.toString(), map);
	}

	public Long getComTotalMoney(Long stationId, Long userId) {
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		map.put("memberId", userId);
		map.put("status", MnyComRecord.STATUS_SUCCESS);
		return super.queryForLong(
				"select (case when sum(money) is not null then sum(money) else 0 end) from mny_com_record where station_id =:stationId and member_id =:memberId and status =:status",
				map);
	}

	public void updateStatusToExpired(Date expireTime, Long stationId) {
		super.update(
				"UPDATE mny_com_record SET status = " + MnyComRecord.STATUS_EXPIRED + ", lock_flag = "
						+ MnyComRecord.LOCK_FLAG_COMPLETED + " WHERE status = " + MnyComRecord.STATUS_UNTREATED
						+ " AND station_id = :stationId" + " AND create_datetime <= :expireTime;",
				MixUtil.newHashMap("stationId", stationId, "expireTime", expireTime));
	}

	public List<MnyComRecord> getSuccessList(Long accountId, Long stationId, Date beginDate, Date endDate) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM mny_com_record");
		sql_sb.append(" WHERE type<>1 and station_id= :stationId and status=").append(MnyComRecord.STATUS_SUCCESS);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if (accountId != null) {
			sql_sb.append(" AND member_id= :accountId");
			paramMap.put("accountId", accountId);
		}

		if (beginDate != null) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", beginDate);
		}

		if (endDate != null) {
			sql_sb.append(" AND create_datetime <= :end");
			paramMap.put("end", endDate);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int updateLock(long id, long oldLock, long lockFlag, long userId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("id", id);
		paramMap.put("lockFlag", lockFlag);
		paramMap.put("date", new Date());
		paramMap.put("oldLock", oldLock);
		return update(
				"update mny_com_record set lock_flag=:lockFlag,modify_user_id=:userId,modify_datetime=:date where id=:id and status=1 and lock_flag=:oldLock",
				paramMap);
	}
	
	public int reback(long id,long lockFlag) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("lockFlag", lockFlag);
		paramMap.put("date", new Date());
		return update(
				"update mny_com_record set lock_flag=:lockFlag,status= 1,modify_datetime=:date where id=:id and status=3 and lock_flag=3",
				paramMap);
	}
	
	
	public long countDayPayCount(Long accountId, Date depositDate, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("date", DateUtil.toDateStr(depositDate));
		paramMap.put("stationId", stationId);
		return queryForLong(
				"select count(*) from mny_com_record where station_id=:stationId and to_char(create_datetime,'YYYY-MM-DD')=:date and member_id=:accountId and status=2",
				paramMap);
	}
	
	public BigDecimal countDayPayBigDecimal(Long accountId, Date depositDate, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("date", DateUtil.toDateStr(depositDate));
		paramMap.put("stationId", stationId);
		return queryForBigDecimal(
				"select sum(money) from mny_com_record where station_id=:stationId and to_char(create_datetime,'YYYY-MM-DD')=:date and member_id=:accountId and status=2",
				paramMap);
	}

	public long countPayCount(Long accountId, Date depositDate, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("accountId", accountId);
		paramMap.put("depositDate", depositDate);
		paramMap.put("stationId", stationId);
		return queryForLong(
				"select count(*) from mny_com_record where station_id=:stationId and create_datetime>=:depositDate and member_id=:accountId",
				paramMap);
	}
	
	public int cancleResult(Long comId, String opDesc) {
		StringBuilder sql = new StringBuilder("update mny_com_record set ");
		sql.append("status=:status,op_desc=:opDesc,modify_datetime = :modifyDatetime");
		sql.append(" where id=:id and status=:oldStatus");
		Map map = MixUtil.newHashMap();
		map.put("status", MnyComRecord.STATUS_FAILED);
		map.put("oldStatus", MnyComRecord.STATUS_SUCCESS);
		map.put("modifyDatetime", new Date());
		map.put("opDesc", opDesc);
		map.put("id", comId);
		return super.update(sql.toString(), map);
	}
	
	public void saveOne(MnyComRecord record) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("orderNo", record.getOrderNo());
		paramMap.put("memberId", record.getMemberId());
		paramMap.put("money", record.getMoney());
		paramMap.put("createDatetime", record.getCreateDatetime());
		paramMap.put("createUserId", record.getCreateUserId());
		paramMap.put("modifyDatetime", record.getModifyDatetime());
		paramMap.put("modifyUserId", record.getModifyUserId());
		paramMap.put("status", record.getStatus());
		paramMap.put("stationId", record.getStationId());
		paramMap.put("type", record.getType());
		paramMap.put("remark", record.getRemark());
		paramMap.put("flagActive", record.getFlagActive());
		paramMap.put("depositor", record.getDepositor());
		paramMap.put("account", record.getAccount());
		paramMap.put("lockFlag", record.getLockFlag());
		paramMap.put("payId", record.getPayId());
		paramMap.put("fee", record.getFee());
		paramMap.put("payName", record.getPayName());
		paramMap.put("opDesc", record.getOpDesc());
		paramMap.put("handlerType", record.getHandlerType());
		paramMap.put("firstDeposit", record.getFirstDeposit());
		paramMap.put("comTimes", record.getComTimes());
		StringBuilder sql = new StringBuilder("INSERT INTO mny_com_record(order_no, member_id, money");
		sql.append(",create_datetime,create_user_id,modify_datetime,modify_user_id,status,station_id,type");
		sql.append(",remark,flag_active,depositor,account,lock_flag,pay_id,fee,pay_name,op_desc,handler_type,first_deposit,com_times) ");
		sql.append("VALUES(:orderNo,:memberId,:money,:createDatetime,:createUserId,:modifyDatetime,:modifyUserId,");
		sql.append(":status,:stationId,:type,:remark,:flagActive,:depositor,:account,:lockFlag,:payId");
		sql.append(",:fee,:payName,:opDesc,:handlerType,:firstDeposit,:comTimes)");
		update(sql.toString(), paramMap);
	}
	
	/*
	 * 用于修改回调的订单金额  
	 */
	public void updateComMoney(MnyComRecordVo mcrVo) {
		StringBuilder sb_sql = new StringBuilder();
		sb_sql.append("update mny_com_record set money =:money where id = "+mcrVo.getId());
		
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("money", mcrVo.getMoney());
		
		super.update(sb_sql.toString(), paramMap);
	}
	
	public int getRecordCountByStatus(Long stationId,Long memberId,Long status) {
		StringBuilder builder = new StringBuilder();
		builder.append("select count(id) from mny_com_record where station_id = :stationId and member_id = :memberId and status =:status and type in(6,7) ");
		
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("stationId", stationId);
		paramMap.put("memberId", memberId);
		paramMap.put("status", status);
		return super.queryForInt(builder.toString(), paramMap);
	}
	
	public void  saveRemark(Long id, String remark) {
		super.update("update mny_com_record set remark = "+"'" + remark + "'"+" where id = "+id);
	}

    public Map countRechargeNum(Long stationId,String date) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("SELECT\n" +
				" count(com_times = 1 OR NULL) AS firstCom,\n" +
				" count(com_times = 2 OR NULL) AS secondCom,\n" +
				" count(com_times = 3 OR NULL)  AS thirdCom\n" +
				"FROM mny_com_record where station_id = :stationId and com_times is not null and com_times <=3");
		paramMap.put("stationId", stationId);
		if(StringUtil.isNotEmpty(date)){
			builder.append(" and to_char(create_datetime, 'yyyy-mm')=:date");
			paramMap.put("date", date);
		}
		return super.selectSingleCamelMap(builder.toString(),paramMap);
    }


	public Map countRechargeNumDay(ReportParamVo paramVo) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("SELECT\n" +
				" count(com_times = 1 OR NULL) AS firstCom,\n" +
				" count(com_times = 2 OR NULL) AS secondCom,\n" +
				" count(com_times = 3 OR NULL)  AS thirdCom\n" +
				"FROM mny_com_record r left join sys_account a on r.account = a.account and r.station_id = a.station_id " +
				"where r.station_id = :stationId and r.com_times is not null and r.com_times <=3");
		builder.append(" and to_char(r.create_datetime, 'yyyy-mm-dd')=:date ");
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("date", paramVo.getSeachDate());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			builder.append(" AND r.account =:account");
		}
		if (reportType!=null && reportType!=0) {
			paramMap.put("reportType", reportType);
			builder.append(" AND a.report_type =:reportType");
		}
		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				builder.append("  AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				builder.append("  AND a.parents LIKE :children");
			}
		}
		return super.selectSingleCamelMap(builder.toString(),paramMap);
	}
	public Map countRechargeTotal(ReportParamVo paramVo) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("SELECT\n" +
				" count(com_times = 1 OR NULL) AS firstTotal,\n" +
				" count(com_times = 2 OR NULL) AS secondTotal,\n" +
				" count(com_times = 3 OR NULL)  AS thirdTotal\n" +
				"FROM mny_com_record r left join sys_account a on r.account = a.account and r.station_id = a.station_id " +
				"where r.station_id = :stationId and r.com_times is not null and r.com_times <=3");
		builder.append(" and r.create_datetime>=:begin and r.create_datetime<=:end");
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("begin", paramVo.getBegin());
		paramMap.put("end", paramVo.getEnd());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			builder.append(" AND r.account =:account");
		}
		if (reportType!=null && reportType!=0) {
			paramMap.put("reportType", reportType);
			builder.append(" AND a.report_type =:reportType");
		}
		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				builder.append("  AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				builder.append("  AND a.parents LIKE :children");
			}
		}
		return super.selectSingleCamelMap(builder.toString(),paramMap);
	}
    public Integer getMcrByAccountAndPayId(Long accountId, Long payId) {
		StringBuilder builder = new StringBuilder();
		builder.append("select count(id) from mny_com_record where pay_id = :payId and member_id = :memberId and status =:status and type in(8) ");

		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("payId", payId);
		paramMap.put("memberId", accountId);
		paramMap.put("status", MnyComRecord.STATUS_UNTREATED);
		return super.queryForInt(builder.toString(), paramMap);
    }

    public MnyComRecord getByMoneyAndAccount(BigDecimal money, Long accountId) {
		StringBuilder builder = new StringBuilder();
		builder.append("select * from mny_com_record where money = :money and member_id = :memberId and status =:status and type in(8) ");
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("money", money);
		paramMap.put("memberId", accountId);
		paramMap.put("status", MnyComRecord.STATUS_UNTREATED);
		return super.query21Model(builder.toString(), paramMap);

    }
    
	public Map todayFirstCom(ReportParamVo paramVo) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("select count(com_times = 1 OR NULL) AS todayFirstCom from mny_com_record r left join sys_account a on r.member_id = a.id where 1=1");
		builder.append(" and r.station_id = :stationId and r.com_times is not null and r.com_times =1");
		builder.append(" and to_char(a.create_datetime, 'yyyy-mm-dd')= :date");
		builder.append(" and to_char(r.create_datetime, 'yyyy-mm-dd')= :date");
		
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("date", paramVo.getSeachDate());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			builder.append(" AND r.account =:account");
		}
		if (reportType!=null && reportType!=0) {
			paramMap.put("reportType", reportType);
			builder.append(" AND a.report_type =:reportType");
		}
		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				builder.append("  AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				builder.append("  AND a.parents LIKE :children");
			}
		}
		return super.selectSingleCamelMap(builder.toString(),paramMap);
	}
    
	public Map previousFirstCom(ReportParamVo paramVo) {
		StringBuilder builder = new StringBuilder();
		HashMap<String, Object> paramMap = new HashMap<>();
		builder.append("select count(com_times = 1 OR NULL) AS previousFirstCom from mny_com_record r left join sys_account a on r.member_id = a.id where 1=1");
		builder.append(" and r.station_id = :stationId and r.com_times is not null and r.com_times =1");
		builder.append(" and to_char(a.create_datetime, 'yyyy-mm-dd')< :date");
		builder.append(" and to_char(r.create_datetime, 'yyyy-mm-dd')= :date");
		
		paramMap.put("stationId", paramVo.getStationId());
		paramMap.put("date", paramVo.getSeachDate());
		String account = paramVo.getAccount();
		String children = paramVo.getChildren();
		Long reportType = paramVo.getReportType();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			builder.append(" AND r.account =:account");
		}
		if (reportType!=null && reportType!=0) {
			paramMap.put("reportType", reportType);
			builder.append(" AND a.report_type =:reportType");
		}
		if (StringUtil.isNotEmpty(children)) {
			paramMap.put("children", children + "%");
			if (paramVo.getSearchSelf()) {
				paramMap.put("curUserId", paramVo.getSelfId());
				builder.append("  AND (a.parents LIKE :children OR a.id = :curUserId)");
			} else {
				builder.append("  AND a.parents LIKE :children");
			}
		}
		return super.selectSingleCamelMap(builder.toString(),paramMap);
	}
	
    public MnyComRecord insertOne(MnyComRecord d) {
		try {
			JdbcModel model = get();
			final JdbcColumn primaryCol = model.getPrimaryCol();
			primaryCol.setColumnName("id");
			final String sql;
			if (primaryCol != null && primaryCol.getGenerator() == 1) {
				ListParameter parameterMaker = new ListParameter();
				SqlMaker.getAddSql(model, parameterMaker);
				sql = parameterMaker.getSql();
				final List paramList = parameterMaker.toParameters(d, 2);
				JdbcDAOImpl.getLogInfo(sql, paramList);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				super.getJdbcOperations().update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = null;
						if (FrameProperites.DB_TYPE == DBType.POSTGRESQL) {
							String primaryColumn = primaryCol.getColumnName();
							ps = conn.prepareStatement(sql, new String[]{primaryColumn});
						} else {
							ps = conn.prepareStatement(sql, 1);
						}

						for(int i = 0; i < paramList.size(); ++i) {
							Object val = paramList.get(i);
							setSqlParameter(ps, i + 1, val);
						}
						return ps;
					}
				}, keyHolder);
				Integer id = (Integer)keyHolder.getKeyList().get(0).get("id");
				d.setId(id.longValue());
			} else {
				if (primaryCol != null && primaryCol.getGenerator() == 3) {
					primaryCol.setValue(d, this.getSequence(primaryCol.getSequence()));
				}
				MapParameter parameterMaker = new MapParameter();
				SqlMaker.getAddSql(model, parameterMaker);
				sql = parameterMaker.getSql();
				Map params = parameterMaker.toParameters(d, 2);
				super.update(sql, params);
			}
			return d;
		}catch (Exception e){}
		return null;
    }

	private JdbcModel get() {
		Class c = this.getGenericType(0);
		JdbcModel jdbcModel = JdbcModelSet.get(c);
		if (jdbcModel == null) {
			throw new JayFrameException(10004, new String[]{c.getName()});
		} else {
			return jdbcModel;
		}
	}
	private void setSqlParameter(PreparedStatement ps, int i, Object val) throws SQLException {
		if (val != null && FrameProperites.DB_TYPE == DBType.POSTGRESQL && val.getClass() == Date.class) {
			ps.setTimestamp(i, new Timestamp(((Date)val).getTime()));
		} else {
			ps.setObject(i, val);
		}

	}
}
