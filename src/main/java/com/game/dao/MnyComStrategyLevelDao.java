package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.stereotype.Repository;

import com.game.dao.lottery.LongRowMapper;
import com.game.model.MnyComStrategyLevel;

@Repository
public class MnyComStrategyLevelDao extends JdbcUtilImpl<MnyComStrategyLevel> {

	public List<Long> findLevels(Long strategyId, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("strategyId", strategyId);
		paramMap.put("stationId", stationId);
		return query2Obj(
				"SELECT member_level_id FROM mny_com_strategy_level WHERE strategy_id=:strategyId and station_id=:stationId",
				paramMap, new LongRowMapper());
	}

	public void deleteByStrategyId(Long strategyId, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("strategyId", strategyId);
		paramMap.put("stationId", stationId);
		update("delete FROM mny_com_strategy_level WHERE strategy_id=:strategyId and station_id=:stationId", paramMap);
	}

	public void deletes(List<Long> list, Long strategyId, Long stationId) {
		if (list == null || list.isEmpty())
			return;
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("strategyId", strategyId);
		StringBuilder sb = new StringBuilder("delete from mny_com_strategy_level where member_level_id in(");
		for (Long sid : list) {
			sb.append(sid).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") and  strategy_id=:strategyId and station_id=:stationId");
		update(sb.toString(), paramMap);
	}

}
