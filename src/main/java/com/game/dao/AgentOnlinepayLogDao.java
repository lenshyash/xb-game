package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentOnlinepayLog;
import com.game.model.SysAccount;
import com.game.model.vo.OnlinepayLogVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class AgentOnlinepayLogDao extends JdbcUtilImpl<AgentOnlinepayLog> {
	public Page<Map> getPage(OnlinepayLogVo onlinepayLogVo) {
		StringBuilder sql_cols = new StringBuilder("");
		StringBuilder sql_tabs = new StringBuilder("");
		StringBuilder sql_whs = new StringBuilder("");
		StringBuilder sql_order = new StringBuilder("");
		sql_cols.append("SELECT *");
		sql_tabs.append(" FROM agent_onlinepay_log");
		sql_whs.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();

		if (StringUtil.isNotEmpty(onlinepayLogVo.getContent())) {
			sql_whs.append(" AND (params LIKE :params OR content LIKE :content)");
			paramMap.put("params", "%" + onlinepayLogVo.getContent() + "%");
			paramMap.put("content", "%" + onlinepayLogVo.getContent() + "%");
		}

		if (Validator.isNotNull(onlinepayLogVo.getStationId())) {
			sql_whs.append(" AND station_id = :stationId");
			paramMap.put("stationId", onlinepayLogVo.getStationId());
		}

		if (StringUtil.isNotEmpty(onlinepayLogVo.getBegin())) {
			sql_whs.append(" AND create_datetime >= :begin");
			paramMap.put("begin", onlinepayLogVo.getBegin());
		}

		if (StringUtil.isNotEmpty(onlinepayLogVo.getEnd())) {
			sql_whs.append(" AND create_datetime < :end");
			paramMap.put("end", onlinepayLogVo.getEnd());
		}

		sql_order.append(" ORDER BY create_datetime DESC");
		
		return super.page2CamelMap(sql_cols.append(sql_tabs).append(sql_whs).append(sql_order).toString(), paramMap);
	}

	public List<Map> getOnlines() {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM agent_onlinepay_log");
		sql_sb.append(" WHERE o.station_id= :stationId");
		sql_sb.append(" order by id desc");
		Map paramMap = MixUtil.newHashMap("stationId", StationUtil.getStationId());
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}

}
