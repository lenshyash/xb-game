package com.game.dao.third;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.third.ThirdDataReceive;

@Repository
public class ThirdDataReceiveDao extends JdbcUtilImpl<ThirdDataReceive> {

	public Page<ThirdDataReceive> getPage(Date start, Date end, Integer status) {
		StringBuilder sb = new StringBuilder("SELECT id,unique_key,create_time,status,parse_time FROM third_data_receive where 1=1 ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (start != null) {
			sb.append(" AND create_time>=:start");
			map.put("start", start);
		}
		if (end != null) {
			sb.append(" AND create_time<=:end");
			map.put("end", end);
		}
		if (status != null && status != 0) {
			sb.append(" and status=:status");
			map.put("status", status);
		}
		sb.append(" order by create_time desc");
		return super.paged2Obj(sb.toString(), map);
	}

	public int countByUniqueKey(String uniqueKey) {
		return queryForInt("select count(*) from third_data_receive where unique_key=:uniqueKey ", MixUtil.newHashMap("uniqueKey", uniqueKey));
	}

	public ThirdDataReceive getByUniqueKey(String uniqueKey) {
		return query21Model("select * from third_data_receive where unique_key=:uniqueKey ", MixUtil.newHashMap("uniqueKey", uniqueKey));
	}

	public int updateParseInfo(Long id, Integer oldStatus, Integer newStatus, Date parseTime) {
		StringBuilder sql = new StringBuilder("update third_data_receive set status=:newStatus");
		if (parseTime != null) {
			sql.append(",parse_time=:parseTime");
		}
		sql.append(" where id=:id and status=:oldStatus");
		Map<String,Object> map = new HashMap<>();
		map.put("id", id);
		map.put("oldStatus", oldStatus);
		map.put("newStatus", newStatus);
		map.put("parseTime", parseTime);
		return update(sql.toString(), map);
	}

	public List<ThirdDataReceive> getUnParse(Integer pageSize) {
		return query2Model("SELECT * FROM third_data_receive where status=1 limit :pageSize",MixUtil.newHashMap("pageSize",pageSize));
	}

}
