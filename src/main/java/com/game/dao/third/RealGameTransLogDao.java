package com.game.dao.third;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.game.model.third.RealGameTransLog;
import com.game.util.DateUtil;

@Repository
public class RealGameTransLogDao extends JdbcUtilImpl<RealGameTransLog> {

	public Page<RealGameTransLog> page(RealGameTransLog rgtl, String startTime, String endTime) {
		StringBuilder sb = new StringBuilder(
				"" + "SELECT rgtl.*, ss.\"name\" as domain,sa.account as account,ss.floder as floder FROM "
						+ "real_game_trans_log rgtl " + " JOIN sys_account sa ON rgtl.account_id = sa.\"id\" ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (!StringUtils.isEmpty(rgtl.getAccount())) {
			rgtl.setAccount(rgtl.getAccount() + '%');
			sb.append(" AND sa.account LIKE :account");
			map.put("account", rgtl.getAccount());
		}
		if (!StringUtils.isEmpty(rgtl.getGameType())) {
			sb.append(" and rgtl.game_type = :game_type");
			map.put("game_type", rgtl.getGameType());
		}
		if (!StringUtils.isEmpty(rgtl.getTransType())) {
			sb.append(" and rgtl.trans_type = :trans_type");
			map.put("trans_type", rgtl.getTransType());
		}
		if (!StringUtils.isEmpty(rgtl.getTransId())) {
			sb.append(" and rgtl.trans_id = :trans_id");
			map.put("trans_id", rgtl.getTransId());
		}
		if (!StringUtils.isEmpty(rgtl.getTransStatus())) {
			sb.append(" and rgtl.trans_status = :trans_status");
			map.put("trans_status", rgtl.getTransStatus());
		}
		if (!StringUtils.isEmpty(rgtl.getThirdTransId())) {
			sb.append(" and rgtl.third_trans_id = :third_trans_id");
			map.put("third_trans_id", rgtl.getThirdTransId());
		}
		if (!StringUtils.isEmpty(rgtl.getStationId())) {
			sb.append(" and  sa.station_id = :station_id");
			map.put("station_id", rgtl.getStationId());
		}

		if (StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
			sb.append(" and rgtl.result_time <=:endTime");
			map.put("endTime", DateUtil.toDatetime(endTime));
		} else if (!StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime)) {
			sb.append(" and rgtl.result_time >=:startTime");
			map.put("startTime", DateUtil.toDatetime(startTime));
		} else if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
			sb.append(" and (rgtl.result_time BETWEEN :startTime and :endTime)");
			map.put("startTime", DateUtil.toDatetime(startTime));
			map.put("endTime", DateUtil.toDatetime(endTime));
		}

		sb.append("  JOIN sys_station ss ON ss.\"id\" = sa.station_id");
		if (!StringUtils.isEmpty(rgtl.getStationId())) {
			sb.append(" and  ss.\"id\" = :station_id");
			map.put("station_id", rgtl.getStationId());
		}
		sb.append("  order by rgtl.result_time desc");

		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM, " case when rgtl.trans_status = 3 then 1 else 0 end ",
				"unkonwCount"));
		aggs.add(new Aggregation(AggregationFunction.SUM, " case when rgtl.trans_status = 1 and rgtl.trans_type =2 then trans_money else 0 end ",
				"sysOutTotal"));
		aggs.add(new Aggregation(AggregationFunction.SUM, " case when rgtl.trans_status = 1 and rgtl.trans_type =1 then trans_money else 0 end ",
				"sysInTotal"));
		return super.paged2Obj(sb.toString(), map, aggs);
	}

	public Page<RealGameTransLog> pageRecord(RealGameTransLog rgtl, Date startTime, Date endTIme) {
		StringBuilder sb = new StringBuilder("" + "SELECT rgtl.*, ss.\"name\" as domain,sa.account as account FROM "
				+ "real_game_trans_log rgtl " + " JOIN sys_account sa ON rgtl.account_id = sa.\"id\" ");
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (!StringUtils.isEmpty(rgtl.getAccount())) {
			rgtl.setAccount(rgtl.getAccount() + '%');
			sb.append(" AND sa.account LIKE :account");
			map.put("account", rgtl.getAccount());
		}

		if (!StringUtils.isEmpty(rgtl.getAccountId())) {
			sb.append(" AND sa.id = :accountId");
			map.put("accountId", rgtl.getAccountId());
		}
		if (!StringUtils.isEmpty(rgtl.getGameType())) {
			sb.append(" and rgtl.game_type = :game_type");
			map.put("game_type", rgtl.getGameType());
		}
		if (!StringUtils.isEmpty(rgtl.getTransType())) {
			sb.append(" and rgtl.trans_type = :trans_type");
			map.put("trans_type", rgtl.getTransType());
		}
		if (!StringUtils.isEmpty(rgtl.getTransId())) {
			sb.append(" and rgtl.trans_id = :trans_id");
			map.put("trans_id", rgtl.getTransId());
		}
		if (!StringUtils.isEmpty(rgtl.getTransStatus())) {
			sb.append(" and rgtl.trans_status = :trans_status");
			map.put("trans_status", rgtl.getTransStatus());
		}
		if (!StringUtils.isEmpty(rgtl.getThirdTransId())) {
			sb.append(" and rgtl.third_trans_id = :third_trans_id");
			map.put("third_trans_id", rgtl.getThirdTransId());
		}
		if (startTime != null) {
			sb.append(" and rgtl.create_datetime >= :startTime");
			map.put("startTime", startTime);
		}
		if (endTIme != null) {
			sb.append(" and rgtl.create_datetime < :endTIme");
			map.put("endTIme", endTIme);
		}
		if (!StringUtils.isEmpty(rgtl.getStationId())) {
			sb.append(" and  sa.station_id = :station_id");
			map.put("station_id", rgtl.getStationId());
		}
		sb.append("  JOIN sys_station ss ON ss.\"id\" = sa.station_id");
		if (!StringUtils.isEmpty(rgtl.getStationId())) {
			sb.append(" and  ss.\"id\" = :station_id");
			map.put("station_id", rgtl.getStationId());
		}
		sb.append("  order by rgtl.result_time desc");
		return super.paged2Obj(sb.toString(), map);
	}

	public long getTransaferBillNo() {
		StringBuilder sql_sb = new StringBuilder("select nextval('transafer_billno_seq')");
		return super.queryForLong(sql_sb.toString());
	}

	public boolean updateTransferStatus(Long id, Integer tranferStatus, String thirdBillno) {
		StringBuilder sql_sb = new StringBuilder("UPDATE real_game_trans_log SET trans_status = :status");
		if (Validator.isNotNull(thirdBillno)) {
			sql_sb.append("	,third_trans_id=:thirdBillno ");
		}
		sql_sb.append(" WHERE id =:id AND trans_status =").append(RealGameTransLog.TRANS_STATUS_UNKNOW);
		return super.update(sql_sb.toString(),
				MixUtil.newHashMap("status", tranferStatus, "thirdBillno", thirdBillno, "id", id)) > 0;
	}

	public Integer delByCreateTimeAndStationId(Date createTime, Long stationId) {
		Map map = MixUtil.newHashMap();
		map.put("createTime", createTime);
		map.put("stationId", stationId);
		return super.update(
				"delete from real_game_trans_log rgtl USING sys_account sa where rgtl.account_id = sa.id and sa.station_id = :stationId and rgtl.create_datetime <= :createTime",
				map);
	}

	public Integer updateRealGameStatus(long id, int status) {
		// TODO Auto-generated method stub
		Map map = MixUtil.newHashMap();
		map.put("id", id);
		map.put("status", status);
		return super.update(
				"update real_game_trans_log set trans_status = :status where id = :id",
				map);
	}
	
	public RealGameTransLog findAccountLastTransLog(Long id,Date start) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select * from real_game_trans_log");
		sql_sb.append(" where account_id = :id ");
		sql_sb.append(" and create_datetime > :start");
		sql_sb.append(" and trans_type = ").append(RealGameTransLog.TRANS_TYPE_ADD);
		sql_sb.append(" and trans_status = ").append(RealGameTransLog.TRANS_STATUS_SUCCESS);
		sql_sb.append(" ORDER BY create_datetime desc limit 1");
		
		Map map = MixUtil.newHashMap();
		map.put("id", id);
		map.put("start", start);
		return super.query21Model(sql_sb.toString(), map);
	}
}
