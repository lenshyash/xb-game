package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.SysPayPlatform;
import com.game.model.vo.PayPlatformVo;

@Repository
public class SysPayPlatformDao extends JdbcUtilImpl<SysPayPlatform> {

	/**
	 * 
	 * @return
	 */
	public Page getPaysPage(PayPlatformVo ppvo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT *");
		sql_sb.append(" FROM sys_pay_platform");
		sql_sb.append(" WHERE 1=1 ");
		Map paramMap = MixUtil.newHashMap();
		if (StringUtil.isNotEmpty(ppvo.getName())) {
			sql_sb.append(" AND name LIKE :name");
			paramMap.put("name", ppvo.getName() + "%");
		}
		if (Validator.isNotNull(ppvo.getType())) {
			sql_sb.append(" AND type = :type");
			paramMap.put("type", ppvo.getType());
		}
		return super.page2CamelMap(sql_sb.toString(),paramMap);
	}

	/**
	 * 
	 * @return
	 */
	public List getPays(PayPlatformVo ppVo) {
		StringBuilder sql_sb = new StringBuilder("");
		sql_sb.append("SELECT p.*");
		sql_sb.append(" FROM sys_pay_platform p");
		sql_sb.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();
		if (Validator.isNotNull(ppVo.getStatus())) {
			sql_sb.append(" AND p.status = :status");
			paramMap.put("status", ppVo.getStatus());
		}
		if (Validator.isNotNull(ppVo.getType())) {
			sql_sb.append(" AND p.type = :type");
			paramMap.put("type", ppVo.getType());
		}
		return super.selectCamelListMap(sql_sb.toString(), paramMap);
	}
}
