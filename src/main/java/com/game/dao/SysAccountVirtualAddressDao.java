package com.game.dao;

import com.game.model.SysAccountVirtualAddress;
import com.game.model.SysMessage;
import com.game.model.vo.MessageVo;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SysAccountVirtualAddressDao extends JdbcUtilImpl<SysAccountVirtualAddress> {

	public Page getPage(MessageVo mvo) {
		StringBuilder sql_cols = new StringBuilder();
		StringBuilder sql_tabs = new StringBuilder();
		StringBuilder sql_whes = new StringBuilder();
		StringBuilder sql_orders = new StringBuilder();
		sql_cols.append("SELECT m.*");
		sql_tabs.append(" FROM sys_message m");
		sql_whes.append(" WHERE 1=1");
		Map paramMap = MixUtil.newHashMap();
		if (Validator.isNotNull(mvo.getStationId())) {
			sql_whes.append(" AND m.station_id = :stationId");
			paramMap.put("stationId", mvo.getStationId());
		}

		if (StringUtil.isNotEmpty(mvo.getTitle())) {
			sql_whes.append(" AND m.title LIKE :title");
			paramMap.put("title", mvo.getTitle() + "%");
		}
		
		if(!StationUtil.isAgentStation()) {
			sql_whes.append(" AND m.create_datetime > :createDatetime");
			paramMap.put("createDatetime", UserUtil.getCreateDatetime());
		}

		sql_orders.append(" ORDER BY ");

		if (Validator.isNotNull(mvo.getAccountId())) {
			sql_cols.append(",u.id AS user_message_id,u.account_id,u.account,u.status");
			sql_tabs.append(" LEFT JOIN sys_user_message u ON m.id = u.message_id");
			sql_tabs.append(" AND account_id = :accountId");
			sql_whes.append(" AND CASE WHEN type = ").append(SysMessage.TYPE_ALL);
			sql_whes.append(" THEN COALESCE(u.account_id,:accountId) =:accountId");
			sql_whes.append(" WHEN type = ").append(SysMessage.TYPE_DEFAULT);
			sql_whes.append(" THEN u.account_id =:accountId END");
			sql_whes.append(" AND COALESCE(u.status,1) != 3");
			sql_orders.append(" u.status,");
			paramMap.put("accountId", mvo.getAccountId());

			if (Validator.isNotNull(mvo.getStatus())) {
				sql_whes.append(" AND COALESCE(u.status,1) = :status");
				paramMap.put("status", mvo.getStatus());
			}
		}

		sql_orders.append(" m.create_datetime DESC");
		String sql = sql_cols.append(sql_tabs).append(sql_whes).append(sql_orders).toString();
		return super.page2CamelMap(sql, paramMap);
	}
	
	public SysAccountVirtualAddress getAddressByAccount(Long accountId,Long stationId){
		StringBuilder sb = new StringBuilder();
		Map paramMap = MixUtil.newHashMap();
		sb.append("SELECT * FROM sys_account_virtual_address WHERE account_id = :accountId and station_id = :stationId limit 1");
		paramMap.put("accountId", accountId);
		paramMap.put("stationId", stationId);
		return super.query21Model(sb.toString(), paramMap);
	}
	
	public void deleteSysMessageByDate(Date createDatetime) {
		StringBuilder builder = new StringBuilder();
		builder.append("delete from sys_message where create_datetime<=:createDatetime");
		HashMap<Object,Object> map = new HashMap<>();
		map.put("createDatetime", createDatetime);
		
		super.update(builder.toString(), map);
	}

    public SysAccountVirtualAddress getByAddress(String toAddress) {
		StringBuilder sb = new StringBuilder();
		Map paramMap = MixUtil.newHashMap();
		sb.append("SELECT id FROM sys_account_virtual_address WHERE money_address = :address limit 1");
		paramMap.put("address", toAddress);
		return super.query21Model(sb.toString(), paramMap);
    }
}
