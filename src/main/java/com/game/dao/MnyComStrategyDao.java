package com.game.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import com.game.model.MnyComStrategy;

@Repository
public class MnyComStrategyDao extends JdbcUtilImpl<MnyComStrategy> {

	public Page<MnyComStrategy> getPage(Integer depositType, Integer giftType, Integer valueType, Date begin, Date end,
			Long stationId) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM mny_com_strategy WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (depositType != null && depositType > 0) {
			sql_sb.append(" AND deposit_type = :depositType");
			paramMap.put("depositType", depositType);
		}
		if (giftType != null && giftType > 0) {
			sql_sb.append(" AND gift_type = :giftType");
			paramMap.put("giftType", giftType);
		}
		if (valueType != null && valueType > 0) {
			sql_sb.append(" AND value_type = :valueType");
			paramMap.put("valueType", valueType);
		}
		if (begin != null) {
			sql_sb.append(" AND start_datetime >= :begin");
			paramMap.put("begin", begin);
		}
		if (end != null) {
			sql_sb.append(" AND start_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" ORDER BY start_datetime DESC");
		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public MnyComStrategy getOne(Long id, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("stationId", stationId);
		return query21Model("SELECT * FROM mny_com_strategy WHERE id=:id and station_id=:stationId", paramMap);
	}

	public void delete(Long id, Long stationId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("stationId", stationId);
		update("delete from mny_com_strategy WHERE id=:id and station_id=:stationId", paramMap);
	}

	public List<MnyComStrategy> findByDepositType(Integer depositType, Long stationId, Integer status, Date date,BigDecimal money) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		StringBuilder sql = new StringBuilder("SELECT * FROM mny_com_strategy WHERE station_id=:stationId");
		if (depositType != null && depositType > 0) {
			sql.append(" and deposit_type=:depositType");
			paramMap.put("depositType", depositType);
		}
		if (status != null && status > 0) {
			sql.append(" and status=:status");
			paramMap.put("status", status);
		}
		if (date != null) {
			sql.append(" and start_datetime<=:date and end_datetime>=:date");
			paramMap.put("date", date);
		}
		if(money!=null){
			sql.append(" and min_money<=:money and max_money>=:money");
			paramMap.put("money", money);
		}
		return query2Model(sql.toString(), paramMap);
	}

	public void updStatus(Long id, Integer status) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("status", status);
		update("update mny_com_strategy set status=:status WHERE id=:id", paramMap);
	}

}
