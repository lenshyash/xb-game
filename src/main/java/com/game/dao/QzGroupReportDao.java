package com.game.dao;

import java.util.Date;
import java.util.HashMap;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.Qzgroup;
@Repository
public class QzGroupReportDao extends JdbcUtilImpl<Qzgroup> {
	public Page page (Long stationId,String LotCode,String groupName,String qihao,Date begin,Date end) {
		StringBuffer sql_sb = new StringBuffer();
		HashMap<Object, Object> paramMap = new HashMap<>();
		sql_sb.append("SELECT COUNT(DISTINCT(a.account_id)) as buy_count,SUM ( A.buy_money ) as buy_money,A.qi_hao ,qz_group_name");
		sql_sb.append(" FROM");
		sql_sb.append(" ( SELECT account_id,buy_money,qi_hao,qz_group_name FROM bc_lottery_order WHERE station_id = :stationId ");
		sql_sb.append(" AND lot_code = :LotCode");
		sql_sb.append(" and qz_group_name is not null  ");
		if(StringUtil.isNotEmpty(groupName)) {
			sql_sb.append(" and qz_group_name = :groupName");
			paramMap.put("groupName", groupName);
		}
		if(StringUtil.isNotEmpty(qihao)) {
			sql_sb.append(" and qi_hao = :qihao");
			paramMap.put("qihao", qihao);
		}
		
		if(StringUtil.isNotEmpty(qihao)) {
			sql_sb.append(" and qi_hao = :qihao");
			paramMap.put("qihao", qihao);
		}
		if(StringUtil.isNotEmpty(begin)) {
			sql_sb.append(" and create_time >= :begin");
			paramMap.put("begin", begin);
		}
		if(StringUtil.isNotEmpty(end)) {
			sql_sb.append(" and create_time <= :end");
			paramMap.put("end", end);
		}
		//不包含撤单情况
		sql_sb.append(" and status !=4");
		sql_sb.append(" ) A");
		sql_sb.append( " GROUP BY qi_hao,qz_group_name ORDER BY qi_hao desc");
		sql_sb.append("");
		paramMap.put("stationId", stationId);
		paramMap.put("LotCode", LotCode);
		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

}
