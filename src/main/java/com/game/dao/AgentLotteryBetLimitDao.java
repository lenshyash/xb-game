package com.game.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.AgentLotteryBetLimit;

@Repository
public class AgentLotteryBetLimitDao extends JdbcUtilImpl<AgentLotteryBetLimit> {
	public Page getPage(Long stationId) {
		String sql = "select * from agent_lottery_bet_limit where station_id = :stationId";
		return super.paged2Obj(sql, MixUtil.newHashMap("stationId",stationId));
	}
	
	public void updateStatus(Long id,Long status,Long stationId) {
		String sql = "update agent_lottery_bet_limit set status = :status where id = :id and station_id = :stationId";
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId",stationId);
		paramMap.put("status", status);
		paramMap.put("id", id);
		super.update(sql, paramMap);
	}
	
	public List<AgentLotteryBetLimit> getList(Long stationId,Long status){
		String sql = "select * from agent_lottery_bet_limit where station_id = :stationId";
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId",stationId);
		if(Validator.isNotNull(status)) {
			sql+=" and status =:status";
			paramMap.put("status", status);
		}
		return super.query2Model(sql, paramMap);
	}
	
	public AgentLotteryBetLimit getOne(Long stationId,String groupName,String code,Long limitType,Long lotType) {
		StringBuilder sb_sql = new StringBuilder();
		sb_sql.append("select * from agent_lottery_bet_limit where station_id = :stationId");
		Map<Object, Object> paramMap = MixUtil.newHashMap("stationId", stationId);
		
		if(StringUtil.isNotEmpty(groupName)) {
			sb_sql.append(" and group_name = :groupName");
			paramMap.put("groupName", groupName);
		}
		if(StringUtil.isNotEmpty(code)) {
			sb_sql.append(" and code = :code");
			paramMap.put("code", code);
		}
		if(Validator.isNotNull(limitType)) {
			sb_sql.append(" and limit_type = :limitType");
			paramMap.put("limitType", limitType);
		}
		if(StringUtil.isNotEmpty(lotType)) {
			sb_sql.append(" and lot_type = :lotType");
			paramMap.put("lotType", lotType);
		}
		
		return super.query21Model(sb_sql.toString(), paramMap);
	}
}
