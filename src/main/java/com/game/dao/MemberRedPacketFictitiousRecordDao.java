package com.game.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.game.model.MemberRedPacketFictitiousRecord;

@Repository
public class MemberRedPacketFictitiousRecordDao extends JdbcUtilImpl<MemberRedPacketFictitiousRecord> {

	public Page<MemberRedPacketFictitiousRecord> getRecordPage(Long stationId, Date begin, Date end) {
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_red_packet_record_fictitious WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (begin != null) {
			sql_sb.append(" AND create_datetime >= :begin");
			paramMap.put("begin", begin);
		}
		if (end != null) {
			sql_sb.append(" AND create_datetime < :end");
			paramMap.put("end", end);
		}
		sql_sb.append(" ORDER BY create_datetime DESC");
		return super.paged2Obj(sql_sb.toString(), paramMap);
	}

	public List<MemberRedPacketFictitiousRecord> getRecordList(Long stationId,Long redPacketId, Integer limit,boolean catchFlag) {
		if(Validator.isNull(limit)) {
			return null;
		}
		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_red_packet_record_fictitious WHERE 1=1");
		Map<String, Object> paramMap = new HashMap<>();
		if (Validator.isNotNull(stationId)) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (Validator.isNotNull(redPacketId)) {
			if(catchFlag) {
				sql_sb.append(" AND red_packet_id = :redPacketId");
			}else {
				sql_sb.append(" AND create_datetime < :end");
				paramMap.put("end", new Date());
				sql_sb.append(" AND red_packet_id != :redPacketId");
			}
			paramMap.put("redPacketId", redPacketId);
		}
		
		sql_sb.append(" order by create_datetime desc");
		if (limit != null && limit > 0) {
			sql_sb.append(" LIMIT :count");
			paramMap.put("count", limit);
		}
		return query2Model(sql_sb.toString(), paramMap);
	}
}
