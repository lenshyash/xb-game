package com.game.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.dao.lottery.LongRowMapper;
import com.game.model.AgentBaseConfig;
import com.game.model.Qzgroup;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.vo.AccountVo;
import com.game.util.StationUtil;

@Repository
public class QzGroupDao extends JdbcUtilImpl<Qzgroup> {

	public Page<Map> getPage(Long stationId,String groupName) {
		StringBuffer sql_sb = new StringBuffer();
		sql_sb.append("select * from qz_group");
		sql_sb.append(" where station_id = :stationId");
		sql_sb.append(" and status = :status");
		HashMap<Object,Object> hashMap = new HashMap<>();
		hashMap.put("stationId", stationId);
		hashMap.put("status", Qzgroup.GROUP_STATUS_NORMAL);
		if(StringUtil.isNotEmpty(groupName)) {
			sql_sb.append(" and name = :groupName");
			hashMap.put("groupName", groupName);
		}
		sql_sb.append(" order by id DESC");
		return super.page2CamelMap(sql_sb.toString(),hashMap);
		
	}
	
	public Qzgroup getGroupByName(String groupName,Long stationId) {
		StringBuffer sql_sb = new StringBuffer();
		sql_sb.append("select * from qz_group ");
		sql_sb.append(" where name = :groupName");
		sql_sb.append(" and station_id = :stationId");
		
		HashMap<Object,Object> hashMap = new HashMap<>();
		hashMap.put("groupName", groupName);
		hashMap.put("stationId", stationId);
		return super.query21Model(sql_sb.toString(), hashMap);
	}
	
	public void batchUpdateGroup(final String groupName, final Long stationId, final List<SysAccount> list) {
		StringBuilder sql = new StringBuilder("UPDATE sys_account_info set qz_group_name = ? where station_id = ? and account_id = ?");
		
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccount sysAccount = list.get(i);
				int k = 1; 
				ps.setString(k++, groupName);
				ps.setLong(k++, stationId);
				ps.setLong(k++, sysAccount.getId());
			}

			@Override
			public int getBatchSize() {
				return list.size();
			}
		});
	}
	
	public void closeGroup(Long stationId,String groupName) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("update qz_group set status=:status where station_id=:stationId and name =:name");
		HashMap<Object,Object> paramMap = new HashMap<>();
		paramMap.put("status", Qzgroup.GROUP_STATUS_CLOSE);
		paramMap.put("stationId", stationId);
		paramMap.put("name", groupName);
		
		super.update(sql_sb.toString(), paramMap);
		
	}
	
	public void batchCloseGroupSysaccountInfo(final Long stationId, final List<SysAccountInfo> list) {
		StringBuilder sql = new StringBuilder("UPDATE sys_account_info set qz_group_name = null where station_id = ? and account_id = ?");
		
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccountInfo info = list.get(i);
				int k = 1; 
				ps.setLong(k++, stationId);
				ps.setLong(k++, info.getAccountId());
			}

			@Override
			public int getBatchSize() {
				return list.size();
			}
		});
	}
}
