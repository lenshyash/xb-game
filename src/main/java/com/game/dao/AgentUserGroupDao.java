package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.AgentUserGroup;

@Repository
public class AgentUserGroupDao extends JdbcUtilImpl<AgentUserGroup> {

	/**
	 * 
	 * @return
	 */
	public Page<AgentUserGroup> getGroups(long stationId, boolean byType) {
		StringBuilder sql = new StringBuilder("SELECT * FROM agent_user_group WHERE station_id = :stationId");
		Map<String, Object> map = new HashMap<>();
		map.put("stationId", stationId);
		if (byType) {
			sql.append(" and type!=:type");
			map.put("type", AgentUserGroup.TYPE_HIDDEN);
		}
		sql.append(" order by id asc");
		return super.paged2Obj(sql.toString(), map);
	}

	/**
	 * 
	 * @return
	 */
	public List<AgentUserGroup> getGroupAll(long stationId, int... types) {
		StringBuilder sql = new StringBuilder("SELECT * FROM agent_user_group WHERE station_id = :stationId");
		if (types != null) {
			sql.append(" and (");
			for (int t : types) {
				sql.append(" type=").append(t).append(" or");
			}
			sql.delete(sql.length() - 3, sql.length());
			sql.append(" )");
		}
		sql.append(" order by id asc");
		return super.query2Model(sql.toString(), MixUtil.newHashMap("stationId", stationId));
	}

	/**
	 * 
	 * @param groupId
	 * @return
	 */
	public AgentUserGroup getGroupById(Long groupId) {
		if (groupId == null || groupId <= 0) {
			return null;
		}
		return super.query21Model("SELECT * FROM agent_user_group WHERE  id = :groupId",
				MixUtil.newHashMap("groupId", groupId));
	}
}
