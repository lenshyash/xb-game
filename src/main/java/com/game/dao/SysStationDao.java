package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.model.vo.StationVo;
import com.game.util.ValidateUtil;

@Repository
public class SysStationDao extends JdbcUtilImpl<SysStation> {
	/**
	 * 通过域名获取站点信息
	 * 
	 * @param domain
	 * @return
	 */

	public SysStation getStation(String domain) {

		String sql = "select b.id,b.floder,b.id,b.name,b.status,b.create_datetime,b.close_time,b.open_time,b.account_id"
				+ ",b.agent_id,a.agent_id as domain_agent_id,is_reg_switch, type, default_home,a.domain_station_name"
				+ ",a.folder as domain_folder,a.domain_plat_token as domain_plat_token from  sys_station_domain a "
				+ "LEFT JOIN sys_station b  " + " ON(b.id = a.station_id) " + " WHERE b.status = "
				+ SysStation.STATUS_ENABLE + " AND a.status = " + SysStationDomain.STATUS_ENABLE
				+ " AND a.domain = :domain ";

		return super.query21Model(sql, MixUtil.newHashMap("domain", domain));
	}

	/**
	 * 通过租户获取站点信息
	 * 
	 * @param domain
	 * @return
	 */

	public SysStation getStationByAgentId(long agentId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT s.*");
		sql_sb.append(" FROM sys_station s ");
		sql_sb.append(" WHERE s.account_id = :agentId");
		return super.query21Model(sql_sb.toString(), MixUtil.newHashMap("agentId", agentId));
	}

	public SysStation findOneById(Long stationId) {
		return super.query21Model("select * from sys_station WHERE id = :stationId",
				MixUtil.newHashMap("stationId", stationId));
	}

	public Page<Map> getPage(StationVo svo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT s.*,a.account,g.account AS agent_name");
		sql_sb.append(" FROM sys_station s LEFT JOIN sys_account a ON s.account_id = a.id");
		sql_sb.append(" LEFT JOIN sys_account g ON s.agent_id = g.id");
		sql_sb.append(" WHERE a.flag_active >= 1");
		Map<String, Object> paramMap = new HashMap<>();
		String account = svo.getAccount();
		String name = svo.getName();
		String floder = svo.getFloder();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			sql_sb.append(" AND a.account = :account");
		}

		if (StringUtil.isNotEmpty(name)) {
			paramMap.put("name", name);
			sql_sb.append(" AND s.name = :name");
		}

		if (StringUtil.isNotEmpty(floder)) {
			paramMap.put("floder", floder);
			sql_sb.append(" AND s.floder = :floder");
		}

		sql_sb.append(" ORDER BY s.floder asc");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<Map> getStationCombo(Long status) {
		StringBuilder sql_sb = new StringBuilder();
		HashMap<String,Object> map = new HashMap<String, Object>();
		
		sql_sb.append("SELECT id,'('||floder||')'||name AS name");
		sql_sb.append(" FROM sys_station where 1 = 1");
		if(Validator.isNotNull(status)) {
			sql_sb.append(" and status = :status");
			map.put("status", status);
		}
		
		sql_sb.append(" ORDER BY floder");
		return super.selectCamelListMap(sql_sb.toString(), map);
	}

	public List<SysStation> findByStatus(Long status) {
		String json = null;
		try {
			json = CacheUtil.getCache(CacheType.STATION_DOMAIN, "status_" + status);
		} catch (Exception e) {
		}
		List<SysStation> list = null;
		if (StringUtils.isNotEmpty(json)) {
			try {
				list = JSON.parseArray(json, SysStation.class);
			} catch (Exception e) {
			}
			if (list != null && !list.isEmpty()) {
				return list;
			}
		}
		list = query2Model("select * from sys_station where status=:status ORDER BY floder", MixUtil.newHashMap("status", status));
		if (list != null && !list.isEmpty()) {
			try {
				CacheUtil.addCache(CacheType.STATION_DOMAIN, "status_" + status, list);
			} catch (Exception e) {
			}
		}
		return list;
	}

	public List<Map> getStationFolderByMulti(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT DISTINCT COALESCE(d.folder,s.floder) as folder");
		sql_sb.append(" FROM sys_station_domain d LEFT JOIN sys_station s ON d.station_id = s.id");
		sql_sb.append(" WHERE d.flag_active >=1 AND s.id = :stationId");
		return super.selectCamelListMap(sql_sb.toString(), MixUtil.newHashMap("stationId", stationId));
	}
	
	public void deleteStationById(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("delete from sys_station where id = :id ");
		super.update(sql_sb.toString(), MixUtil.newHashMap("id", stationId));
	}

    public SysStation findOneByFolder(String folder) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("select * from sys_station where floder=:folder and status=:status");
		return super.query21Model(sql_sb.toString(), MixUtil.newHashMap("folder", folder,"status",SysStation.STATUS_ENABLE));
    }
}
