package com.game.dao;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.game.model.SysCodeMaker;

@Repository
public class SysCodeMakerDao extends JdbcUtilImpl<SysCodeMaker>{
	
	public long getNextVal(String key){
		String sql = "update sys_code_maker set seq =  seq + 1 where key = :key returning seq";
		return super.queryForLong(sql,MixUtil.newHashMap("key",key));
	}
	
	public int updateSeq(String key,long seq){
		String sql = "update sys_code_maker set seq =  :seq  where key = :key ";
		return super.update(sql, MixUtil.newHashMap("key",key,"seq",seq));
	}
}
