package com.game.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.MemberRedPacket;

@Repository
public class MemberRedPacketDao extends JdbcUtilImpl<MemberRedPacket> {

	public Page<MemberRedPacket> getPage(Long stationId) {
		return super.paged2Obj("SELECT * FROM member_red_packet WHERE station_id =:stationId ORDER BY begin_datetime DESC", MixUtil.newHashMap("stationId", stationId));
	}

	public int updStatus(Long id, Integer status) {
		return super.update("UPDATE member_red_packet SET status = :status WHERE id =:id",
				MixUtil.newHashMap("status", status, "id", id));
	}

	public List<MemberRedPacket> getRedPacketList(Long stationId, Integer status) {

		StringBuilder sql_sb = new StringBuilder("SELECT * FROM member_red_packet WHERE 1 = 1");
		Map<String, Object> paramMap = new HashMap<>();
		if (stationId != null && stationId > 0) {
			sql_sb.append(" AND station_id = :stationId");
			paramMap.put("stationId", stationId);
		}
		if (status != null && status > 0) {
			sql_sb.append(" AND status = :status");
			paramMap.put("status", status);
		}
		return super.query2Model(sql_sb.toString(), paramMap);
	}

	public int grabRedPacket(Long id, BigDecimal money, BigDecimal remainMoney) {
		StringBuilder sql_sb = new StringBuilder("UPDATE member_red_packet SET ");
		sql_sb.append(" remain_money = remain_money - :money,");
		sql_sb.append(" remain_number = remain_number - 1");
		sql_sb.append(" WHERE id =:id AND remain_money = :remainMoney AND remain_number > 0");
		return super.update(sql_sb.toString(),
				MixUtil.newHashMap("money", money, "id", id, "remainMoney", remainMoney));
	}

	public boolean checkDatetime(Long stationId, Integer status, Date begin, Date end) {
		StringBuilder sql_sb = new StringBuilder("SELECT COUNT(1) FROM member_red_packet");
		sql_sb.append(" WHERE station_id = :stationId AND status = :status");
		sql_sb.append(" AND ((begin_datetime <:begin AND end_datetime >= :begin)");
		sql_sb.append(" or (begin_datetime <:end AND end_datetime >= :end)");
		sql_sb.append(" or (begin_datetime >=:begin AND end_datetime <= :end))");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		paramMap.put("status", status);
		paramMap.put("begin", begin);
		paramMap.put("end", end);
		return queryForInt(sql_sb.toString(), paramMap) > 0;
	}

}
