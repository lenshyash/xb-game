package com.game.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.game.model.StationPlatformTransfer;
import com.game.model.third.RealGameTransLog;

@Repository
public class StationPlatformTransferDao extends JdbcUtilImpl<StationPlatformTransfer> {

	public StationPlatformTransfer getTransferMoney(Long stationId, Integer platform) {
		StringBuilder sql_sb = new StringBuilder("SELECT *");
		sql_sb.append(" FROM station_platform_transfer");
		sql_sb.append(" WHERE station_id = :stationId AND platform = :platform");
		return super.query21Model(sql_sb.toString(), MixUtil.newHashMap("stationId", stationId, "platform", platform));
	}
	
	public StationPlatformTransfer getTransferMoneyByPids(Long stationId, List<Integer> pids) {
		StringBuilder sql_sb = new StringBuilder("SELECT sum(out_money) as out_money");
		sql_sb.append(",sum(into_money) as into_money");
		sql_sb.append(" FROM station_platform_transfer");
		sql_sb.append(" WHERE station_id = :stationId AND platform IN (");
		Map paramMap = MixUtil.newHashMap("stationId", stationId);
		for (int i = 0; i < pids.size(); i++) {
			if(i>0) {
				sql_sb.append(",");
			}
			sql_sb.append(" :platform_").append(i);
			paramMap.put("platform_"+i, pids.get(i).intValue());
		}
		sql_sb.append(" )");
		
		return super.query21Model(sql_sb.toString(), paramMap);
	}

	public boolean updateTransferMoney(Long stationId, Integer platform, BigDecimal money, BigDecimal oldMoney,
			Integer type) {
		StringBuilder tab_sb = new StringBuilder("UPDATE station_platform_transfer SET");
		StringBuilder whe_sb = new StringBuilder(" WHERE station_id = :stationId AND platform = :platform");
		if (StringUtil.equals(RealGameTransLog.TRANS_TYPE_SUB, type)) {
			tab_sb.append(" into_money =into_money+ :money");
			whe_sb.append(" AND into_money= :oldMoney");
		} else if (StringUtil.equals(RealGameTransLog.TRANS_TYPE_ADD, type)) {
			tab_sb.append(" out_money =out_money+ :money");
			whe_sb.append(" AND out_money= :oldMoney");
		}
		return super.update(tab_sb.append(whe_sb).toString(), MixUtil.newHashMap("stationId", stationId, "platform",
				platform, "money", money, "oldMoney", oldMoney)) > 0;
	}

	public void updateRealInOutMoney() {
		update("update station_platform_transfer SET into_money=0 , out_money=0");
	}
}
