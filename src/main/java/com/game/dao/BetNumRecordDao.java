package com.game.dao;

import java.math.BigDecimal;
import java.util.*;

import com.game.model.MnyComRecord;
import com.game.model.vo.BetNumRecordVo;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.Aggregation;
import org.jay.frame.jdbc.support.AggregationFunction;
import org.springframework.stereotype.Repository;

import com.game.model.BetNumRecord;

@Repository
public class BetNumRecordDao extends JdbcUtilImpl<BetNumRecord> {

	public Page<BetNumRecord> getPage(String account, String parents, Date beginDate, Date endDate, Integer type,Long stationId) {
		StringBuilder sql = new StringBuilder( "SELECT * FROM bet_num_record");
		sql.append(" where biz_datetime between :start and :end and station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("start", beginDate);
		paramMap.put("end", endDate);
		paramMap.put("stationId", stationId);
		 account = StringUtils.trimToEmpty(account);
		if (StringUtils.isNotEmpty(account)) {
			paramMap.put("account", account.toLowerCase());
			sql.append(" AND lower(account) = :account");
		}
		if (StringUtils.isNotEmpty(parents)) {
			paramMap.put("parents", parents+ "%");
			sql.append(" AND parents like :parents");
		}

		if (type!=null && type>0) {
			paramMap.put("type",type);
			sql.append(" AND type=:type");
		}
		sql.append(" ORDER BY create_datetime DESC");
		return super.paged2Obj(sql.toString(), paramMap);
	}

	public Page<BetNumRecord> getPageByApi(BetNumRecordVo recordVo) {
		StringBuilder sql = new StringBuilder( "SELECT * FROM bet_num_record");
		sql.append(" where create_datetime between :start and :end and station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("start", recordVo.getBegin());
		paramMap.put("end", recordVo.getEnd());
		paramMap.put("stationId", recordVo.getStationId());
		if (StringUtils.isNotEmpty(recordVo.getAccount())) {
			paramMap.put("account", recordVo.getAccount().toLowerCase());
			sql.append(" AND lower(account) = :account");
		}
		if (recordVo.getAccountId()!=null) {
			paramMap.put("accountId", recordVo.getAccountId());
			sql.append(" AND account_id = :accountId");
		}

		if (recordVo.getType()!=null && recordVo.getType()>0) {
			paramMap.put("type",recordVo.getType());
			sql.append(" AND type = :type");
		}else{
			sql.append(" AND type in (1,2,3,4,5,99,92)");
		}
		sql.append(" ORDER BY create_datetime DESC");
		List<Aggregation> aggs = new ArrayList<Aggregation>();
		aggs.add(new Aggregation(AggregationFunction.SUM,
				"bet_num ", "totalNum"));
		return super.paged2Obj(sql.toString(), paramMap, aggs);
	}

    public BigDecimal getBetNumByDate(BetNumRecordVo recordVo) {
		StringBuilder sql = new StringBuilder( "SELECT SUM(bet_num) FROM bet_num_record");
		sql.append(" where create_datetime > :start  and station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("start", recordVo.getBegin());
		paramMap.put("stationId", recordVo.getStationId());
		if (recordVo.getAccountId()!=null) {
			paramMap.put("accountId", recordVo.getAccountId());
			sql.append(" AND account_id = :accountId");
		}
		if (recordVo.getType()!=null && recordVo.getType()>0) {
			paramMap.put("type",recordVo.getType());
			sql.append(" AND type = :type");
		}else{
			sql.append(" AND type in (1,2,3,4,5,99,92)");
		}
		return super.queryForBigDecimal(sql.toString(), paramMap);
    }

	public BetNumRecord getLastOneRecord(BetNumRecordVo recordVo) {
		StringBuilder sql = new StringBuilder( "SELECT * FROM bet_num_record");
		sql.append(" where create_datetime > :start and station_id=:stationId");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("start", recordVo.getBegin());
		paramMap.put("stationId", recordVo.getStationId());
		if (recordVo.getAccountId()!=null) {
			paramMap.put("accountId", recordVo.getAccountId());
			sql.append(" AND account_id = :accountId");
		}
		if (recordVo.getType()!=null && recordVo.getType()>0) {
			paramMap.put("type",recordVo.getType());
			sql.append(" AND type = :type");
		}else{
			sql.append(" AND type in (1,2,3,4,5,99,92)");
		}
		sql.append(" order by create_datetime desc limit 1");
		return super.query21Model(sql.toString(), paramMap);
	}
}
