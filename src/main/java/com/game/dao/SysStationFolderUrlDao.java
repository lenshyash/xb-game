package com.game.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.model.SysStationFolderUrl;
import com.game.model.vo.StationVo;

@Repository
public class SysStationFolderUrlDao extends JdbcUtilImpl<SysStationFolderUrl> {

	public Page<Map> getPage(StationVo svo) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT s.*,a.account,g.account AS agent_name");
		sql_sb.append(" FROM sys_station s LEFT JOIN sys_account a ON s.account_id = a.id");
		sql_sb.append(" LEFT JOIN sys_account g ON s.agent_id = g.id");
		sql_sb.append(" WHERE a.flag_active >= 1");
		Map<String, Object> paramMap = new HashMap<>();
		String account = svo.getAccount();
		String name = svo.getName();
		String floder = svo.getFloder();
		if (StringUtil.isNotEmpty(account)) {
			paramMap.put("account", account);
			sql_sb.append(" AND a.account = :account");
		}

		if (StringUtil.isNotEmpty(name)) {
			paramMap.put("name", name);
			sql_sb.append(" AND s.name = :name");
		}

		if (StringUtil.isNotEmpty(floder)) {
			paramMap.put("floder", floder);
			sql_sb.append(" AND s.floder = :floder");
		}

		sql_sb.append(" ORDER BY s.floder asc");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}

	public List<Map> getStationCombo() {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT id,'('||floder||')'||name AS name");
		sql_sb.append(" FROM sys_station ORDER BY floder");

		return super.selectCamelListMap(sql_sb.toString());
	}

	public List<Map> getStationFolderByMulti(Long stationId) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT DISTINCT COALESCE(d.folder,s.floder) as folder");
		sql_sb.append(" FROM sys_station_domain d LEFT JOIN sys_station s ON d.station_id = s.id");
		sql_sb.append(" WHERE d.flag_active >=1 AND s.id = :stationId");
		return super.selectCamelListMap(sql_sb.toString(), MixUtil.newHashMap("stationId", stationId));
	}

	public Page page(Long stationId, String folder) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * ");
		sql_sb.append("FROM sys_station_folder_url ");
		sql_sb.append("WHERE station_id = :stationId ");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("stationId", stationId);
		if(StringUtil.isNotEmpty(folder)){
			paramMap.put("folder", folder);
			sql_sb.append(" AND folder = :folder");
		}
		sql_sb.append(" ORDER BY folder asc");

		return super.page2CamelMap(sql_sb.toString(), paramMap);
	}
	public void delete(Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("delete from sys_station_folder_url where id = :id and station_id = :stationId");
		super.update(sb.toString(), MixUtil.newHashMap("id", id, "stationId", stationId));
	}
	
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		StringBuilder sb = new StringBuilder("update sys_station_folder_url set status = :modelStatus where id = :id");
		sb.append(" and station_id=:stationId");
		super.update(sb.toString(), MixUtil.newHashMap("modelStatus", modelStatus, "id", id, "stationId", stationId));
	}
	
	public SysStationFolderUrl findByFolder(String folder, Long stationId,int status) {
		String url ="select * from sys_station_folder_url where station_id=:stationId and folder=:folder and status=:status";
		return super.query21Model(url, MixUtil.newHashMap("stationId", stationId, "folder", folder ,"status",status));
	}
}
