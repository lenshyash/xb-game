package com.game.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.game.exception.HttpResponseException;
import com.game.exception.NetworkException;

public abstract class RequestProxy {

	/**
	 * 浏览器
	 */
	public static final String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 8.0; Windows XP)";

	private Logger logger = Logger.getLogger(RequestProxy.class);

	private HttpRequestBase requestBase = null;

	private PostType postType = null;

	public String doRequest(String url, PostType type) {
		return doRequest(url, type,false,false);
	}
	public String doRequest(String url, PostType type,boolean checkSysErro) {
		return doRequest(url, type,checkSysErro,false);
	}
	public String doRequest(String url, PostType type,boolean checkSysError,boolean outflag) {
		postType = type;
		HttpEntity httpEntity = null;
		try {
			HttpResponse httpResponse = null;
			if (type == PostType.GET) {
				httpResponse = this.doGet(url);
			} else {
				httpResponse = this.doPost(url,outflag);
			}
			StatusLine statusLine = httpResponse.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			httpEntity = httpResponse.getEntity();
			if (statusCode == 200) {
				String content = getContent(httpEntity);
				if(!checkSysError){
					return content;
				}
				Header []hs = httpResponse.getHeaders("ceipstate");
				if(hs == null || hs.length == 0){
					return content;
				}
				String responseStatus = hs[0].getValue();
				String defErrorMsg = "数据中心异常[ceipstate:"+responseStatus+"]";
				if(!"1".equals(responseStatus)){
					try{
						JSONObject json = JSONObject.parseObject(content);
						String msg = json.getString("msg");
						if(msg == null){
							msg = defErrorMsg;
						}
						throw new GenericException(msg);
					}catch(JSONException e){
						throw new GenericException(defErrorMsg);
					}
				}
				return content;
			} else if (statusCode > 500) {
				String content = getContent(httpEntity);
				throw new HttpResponseException(statusCode, content);
			}
			throw new HttpResponseException(statusCode);
		} catch (URISyntaxException e) {
			throw new HttpResponseException(404, "URL[" + url + "]异常");
		} catch(HttpHostConnectException | ConnectTimeoutException | UnknownHostException e){
			throw new HttpResponseException(404, e.getClass().getName()+":"+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new NetworkException(e);
		} finally {
			if (requestBase != null) {
				requestBase.abort();
			}
			if (httpEntity != null) {
				try {
					EntityUtils.consume(httpEntity);
				} catch (IOException e) {

				}
			}
		}
	}

	private String getContent(HttpEntity httpEntity) throws ParseException, IOException {
		if (httpEntity == null) {
			return "";
		}
		String content = EntityUtils.toString(httpEntity, "UTF-8");
		return content;
	}

	public String doRequest(String url) {
		return doRequest(url, PostType.GET);
	}

	private HttpResponse doGet(String url) throws ClientProtocolException, IOException, URISyntaxException {
		HttpGet httpGet = new HttpGet(url);

		// 请求头部信息
		List<Header> headerList = this.getHeaders();
		if (headerList != null && headerList.size() > 0) {
			httpGet.setHeaders(headerList.toArray(new Header[] {}));
		}
		// 请求参数
		List<NameValuePair> parameters = this.getParameters();
		if (parameters != null && parameters.size() > 0) {
			String paramsStr = EntityUtils.toString(new UrlEncodedFormEntity(parameters));
			httpGet.setURI(new URI(httpGet.getURI().toString() + "?" + paramsStr));
		}
		requestBase = httpGet;
		HttpClient httpClient = HttpManager.getHttpClient();

		return httpClient.execute(httpGet);
	}

	private HttpResponse doPost(String url,boolean outflag) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(url);
		// 请求头部信息
		List<Header> headerList = this.getHeaders();
		if (headerList != null && headerList.size() > 0) {
			httpPost.setHeaders(headerList.toArray(new Header[] {}));
		}
		if (this.postType == PostType.POST_XML) {
			String xml = this.getXml();
			if (xml != null) {
				StringEntity entity = new StringEntity(xml, HTTP.UTF_8);
				httpPost.setEntity(entity);
			}
		} else {
			// 请求参数
			List<NameValuePair> parameters = this.getParameters();
			if (parameters != null && parameters.size() > 0) {
				HttpEntity entity = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
				httpPost.setEntity(entity);
			}
		}
		requestBase = httpPost;
		if(outflag){
			HttpClient httpClient = getHttpClientShort();
			return httpClient.execute(httpPost);
		}else{
			HttpClient httpClient = HttpManager.getHttpClient();
			return httpClient.execute(httpPost);
		}
	}
	public static CloseableHttpClient getHttpClientShort() {
		int SOCKET_TIMEOUT = 3500;         
		int CONNECT_TIMEOUT = 3500;        
		int CONNECT_REQUEST_TIMEOUT = 3500;
		int MAX_TOTAL = 200;      
		int REQUEST_RETRY = 3;    
		RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIMEOUT).setConnectTimeout(CONNECT_TIMEOUT).setConnectionRequestTimeout(CONNECT_REQUEST_TIMEOUT).build();

		PoolingHttpClientConnectionManager conMgr = new PoolingHttpClientConnectionManager();
		// 设置整个连接池最大连接数 根据自己的场景决定
		conMgr.setMaxTotal(MAX_TOTAL);
		// 是路由的默认最大连接（该值默认为2），限制数量实际使用DefaultMaxPerRoute并非MaxTotal。
		// 设置过小无法支持大并发(ConnectionPoolTimeoutException: Timeout waiting for connection from pool)，路由是对maxTotal的细分。
		//（目前只有一个路由，因此让他等于最大值）
		conMgr.setDefaultMaxPerRoute(conMgr.getMaxTotal());

		//另外设置http client的重试次数，默认是3次；当前是禁用掉（如果项目量不到，这个默认即可）
		DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(REQUEST_RETRY, true);

		//设置重定向策略  
		LaxRedirectStrategy redirectStrategy = new LaxRedirectStrategy();

		// 创建HttpClientBuilder
		// HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		// httpClient = httpClientBuilder.setDefaultRequestConfig(defaultRequestConfig).build();
		return HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).setConnectionManager(conMgr).setRetryHandler(retryHandler).setRedirectStrategy(redirectStrategy).build();
	}
	public List<Header> getHeaders() {
		List<Header> headerList = new ArrayList<Header>();
		headerList.add(new BasicHeader("User-Agent", USER_AGENT));
		headerList.add(new BasicHeader("Connection", "close")); // 短链接
		headerList.add(new BasicHeader("Accept-Encoding", "identity")); // 短链接
		return headerList;
	}

	/**
	 * 请求参数
	 * 
	 * @return
	 */
	public List<NameValuePair> getParameters() {
		return null;
	}

	public String getXml() {
		return null;
	}
}
