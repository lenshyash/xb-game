package com.game.core;

public enum StationType {
	/**
	 * 总控后台
	 */
	ADMIN,
	/**
	 * 会员端
	 */
	MEMBER,
	/**
	 * 租户端
	 */
	AGENT,
	/**
	 * 代理端
	 */
	DAILI,
	/**
	 * 手机端
	 */
	MOBILE,
	/**
	 * 接收数据端
	 */
	RECEIVE_DATA,
	/**
	 * 原生手机端
	 */
	NATIVE,
	/**
	 * 平台API端
	 */
	API,
}
