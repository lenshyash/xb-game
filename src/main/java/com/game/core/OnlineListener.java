package com.game.core;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.game.model.SysAccount;
import com.game.user.online.OnlineManager;

public class OnlineListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		if (session == null) {
			return;
		}
		try {
			SysAccount member = (SysAccount) session.getAttribute(SystemConfig.SESSION_MEMBER_KEY);
			SysAccount agent = (SysAccount) session.getAttribute(SystemConfig.SESSION_DAILI_KEY);
			SysAccount manager = (SysAccount) session.getAttribute(SystemConfig.SESSION_AGENT_KEY);
			if (member != null) {
				OnlineManager.doOffLine(session, member);
			}
			if (agent != null) {
				OnlineManager.doOffLine(session, agent);
			}
			if (manager != null) {
				OnlineManager.doOffLine(session, manager);
			}
		} catch (Exception e) {
		}
	}
}
