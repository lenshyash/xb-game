package com.game.core;

import com.game.model.SysAccountInfo;
import com.game.model.SysStation;

public class CustomThreadData {
	
	private SysStation station;
	
	private StationType stationType;
	
	private SortInfo sortInfo;
	
	private SysAccountInfo accountInfo;
	
	public SortInfo getSortInfo() {
		return sortInfo;
	}

	public void setSortInfo(SortInfo sortInfo) {
		this.sortInfo = sortInfo;
	}

	public SysStation getStation() {
		return station;
	}

	public void setStation(SysStation station) {
		this.station = station;
	}

	public StationType getStationType() {
		return stationType;
	}

	public void setStationType(StationType stationType) {
		this.stationType = stationType;
	}

	public SysAccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(SysAccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}
}
