package com.game.core;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.game.constant.StationConfig;
import com.game.rocketmq.Producer;
import com.game.rocketmq.RocketMqUtil;
import com.game.util.StationConfigUtil;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.PropertiesUtil;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheManager;
import com.game.cache.redis.RedisAPI;
import com.game.model.CommonRedpacket;
import com.game.model.MemberRedPacket;
import com.game.model.dictionary.MoneyRecordType;
import com.game.service.CommonRedpacketService;
import com.game.service.MemberRedPacketService;
import com.game.service.SysCacheService;
import com.game.util.CodeMaker;
import com.game.util.SpringUtil;
import com.game.util.TemplateUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SystemListener implements ServletContextListener {
	private Logger logger = Logger.getLogger(SystemListener.class);

	public void contextInitialized(ServletContextEvent sce) {
		String projectPath = sce.getServletContext().getRealPath("/");
		TemplateUtil.TEMPLATE_FOLDER = new File(projectPath + "/template");
		// 启动redis服务器
		openRedisServer();
		// 读取系统配置
		initSysConfig();
		// 读取缓存配置
		CacheManager.loadConfigMap();
		if (SystemConfig.GENERATE_LOTTERY_STATIC_PAGE) {
			generateMarkSixTemplate();
		}
		// 启用体育滚球待确认注单 检验线程
		runSportThread();
		
		// 检察金额账变枚举类型的VALUE值是否存在相同
		checkMoneyTypeUnique();
		
		clearRedisKeys();

		initRedPacket();

		//开启rocket服务
		openRocketMqServer();
		//开启es服务
		openEsServer();
	}

	private void checkMoneyTypeUnique() {
		if (MoneyRecordType.isNotUnique()) {
			logger.error("【金额账变枚举类型存在相同的VALUE值，服务器停止运行】");
			System.exit(0);
		}
	}

	private void generateMarkSixTemplate() {
		try {
			Thread thread = new Thread(
					(Runnable) Class.forName("com.game.lottery.utils.marksix.MarkSixTemplateGenerate").newInstance());
			thread.start();
			logger.info("【六合彩生成静态模版的线程启动】");
		} catch (Exception e) {
			logger.error("【六合彩生成静态模版的线程启动失败】", e);
		}
	}

	public void runSportThread() {
		try {
			Thread thread = (Thread) Class.forName("com.game.sports.hg.CheckOrderThread").newInstance();
			thread.start();
			logger.info("【体育滚球待确认线程启动】");
		} catch (Exception e) {
			logger.error("【体育滚球待确认线程启动失败】", e);
		}
	}

	public void clearRedisKeys() {
		try {
			logger.info("【程序启动清空REDIS中的指定缓存】");
			SysCacheService cacheService = (SysCacheService) SpringUtil.getBean("sysCacheServiceImpl");
			cacheService.clearRedisKeys();
		} catch (Exception e) {
			logger.error("【程序启动清空REDIS中的指定缓存失败】", e);
		}
	}

	private void openRedisServer() {
		try {
			RedisAPI.initPool();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【Redis启动失败，服务器停止运行】");
			System.exit(0);
		}
	}

	private void openRocketMqServer() {
		try {
			final String serverName = StationConfigUtil.getSys(StationConfig.sys_server_name);
			if(RocketMqUtil.DEFAULT_SERVER_NAME.equals(serverName)){
				ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext-producer.xml");
				Producer producer = context.getBean(Producer.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【rocketMq启动失败，服务器停止运行】");
		}
	}

	private void openEsServer() {
		try {
			if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
				ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext-elasticsearch.xml");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【Es启动失败，服务器停止运行】");
		}
	}
	private void initSysConfig() {
		try {
			PropertiesUtil pu = new PropertiesUtil("application.properties");
			// Map<String,String> data = pu.readAllProperties();
			SystemConfig.CONTROL_DOMAIN = pu.$("sys.control.domain", "localhost");
			SystemConfig.SYS_RECEIVE_DATA_IPS = pu.$("sys.receive.data.ip");
			SystemConfig.SYS_ADMIN_IP_WHITE_LIST = pu.$("sys.admin.ip.white.list", "");
			CodeMaker.GENERATE_BY_DB = "db".equalsIgnoreCase(pu.$("sys.code.maker.generate"));
			SystemConfig.GENERATE_LOTTERY_STATIC_PAGE = pu.$b("generate.lottery.static.page", true);
			SystemConfig.LOTTERY_STATIC_PAGE_BY_CACHE = pu.$b("lottery.static.page.by.cache", false);
			SystemConfig.SYS_MODE_DEVELOP = pu.$b("sys.mode.develop", false);
			SystemConfig.SYS_DEVELOP_IP = pu.$b("sys.develop.ip", false);
			SystemConfig.PLATFROM_EGAME_VERSION = pu.$("platform.egame.version", "v1");
			SystemConfig.SYS_ACCESS_API_DATA_DOMAIN = pu.$("sys.access.api.data.domain");
			SystemConfig.ELASTICSEARCH_SERVER_PREFIX = pu.$("sys.elasticsearch.server.prefix");
			SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN = pu.$b("sys.elasticsearch.server.open", false);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("【classpath:system.properties读取失败】");
			System.exit(0);
		}
	}

	private void initRedPacket() {
		try {
			MemberRedPacketService redPacketService = SpringUtil.getBean(MemberRedPacketService.class);
			List<MemberRedPacket> list = redPacketService.getRedPacketList(null);
			if (list != null && !list.isEmpty()) {
				Date now = new Date();
				for (MemberRedPacket m : list) {
					if (m.getEndDatetime().after(now)) {
						try {
							redPacketService.initRedPacketRedis(m);
						} catch (Exception e) {
							logger.error("初始化红包错误" + JSON.toJSONString(m), e);
						}
					}
				}
			}
			
			CommonRedpacketService commonRedpacketService = SpringUtil.getBean(CommonRedpacketService.class);
			List<CommonRedpacket> commonRedpacketList = commonRedpacketService.getRedpacketList(null);
			if (commonRedpacketList != null && !commonRedpacketList.isEmpty()) {
				Date now = new Date();
				for (CommonRedpacket m : commonRedpacketList) {
					if (m.getEndDatetime().after(now)) {
						try {
							commonRedpacketService.initRedpacketRedis(m);
						} catch (Exception e) {
							logger.error("初始化红包错误" + JSON.toJSONString(m), e);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("初始化红包错误", e);
		}
	}

	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}
}
