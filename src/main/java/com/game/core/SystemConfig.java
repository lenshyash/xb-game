package com.game.core;

import com.game.constant.StationConfig;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

public class SystemConfig {
	/**
	 * 总控后台域名
	 */
	public static String CONTROL_DOMAIN = null;
	/**
	 * 推送或者接收数据的ip
	 */
	public static String SYS_RECEIVE_DATA_IPS = null;
	/**
	 * 第三方访问API接口数据的IP
	 */
	public static String SYS_ACCESS_API_DATA_DOMAIN = null;
	
	/**
	 * 总控后台IP白名单
	 */
	public static String SYS_ADMIN_IP_WHITE_LIST = null;
	/**
	 * 启动是否执行生成彩票静态页面
	 */
	public static boolean GENERATE_LOTTERY_STATIC_PAGE = false;
	/**
	 * 使用redis缓存保存彩票静态页面
	 */
	public static boolean LOTTERY_STATIC_PAGE_BY_CACHE = false;

	/**
	 * 代理平台首页浏览器访问路径
	 */
	public final static String AGENT_CONTROL_PATH = "/agent";
	/**
	 * 总控后台首页浏览器访问路径
	 */
	public final static String ADMIN_CONTROL_PATH = "/admin";
	/**
	 * 会员端首页浏览器访问路径
	 */
	public final static String MEMBER_CONTROL_PATH = "";

	/**
	 * 代理访问路径
	 */
	public final static String DAILI_CONTROL_PATH = "/daili";

	/**
	 * 手机端首页浏览器访问路径
	 */
	public final static String MOBILE_CONTROL_PATH = "/mobile";
	
	/**
	 * 原生手机版数据接口访问路径
	 */
	public static final String NATIVE_CONTROL_PATH = "/native";
	
	/**
	 * 第三方调用平台接口访问路径
	 */
	public static final String API_CONTROL_PATH = "/api";


	/**
	 * 会员资源目录 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_MEMBER = "/member";
	/**
	 * 会员资源目录2 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_MEMBER_2 = "/member2";
	
	/**
	 * 总控后台资源目录 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_ADMIN = "/admin";

	/**
	 * 租户后台资源目录 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_AGENT = "/agent";

	/**
	 * 代理后台资源目录 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_DAILI = "/daili";

	/**
	 * 手机端资源目录 存放css、js、jsp 图片等
	 */
	public final static String SOURCE_FOLDER_MOBILE = "/mobile";

	/**
	 * 公共资源目录 存放css、js、jsp 图片等
	 */
	public static String SOURCE_FOLDER_COMMON = "/common";

	/**
	 * 总控用户 session key
	 */
	public final static String SESSION_ADMIN_KEY = "USER_SESSION_ADMIN";

	/**
	 * 会员账号 session key
	 */
	public final static String SESSION_MEMBER_KEY = "USER_SESSION_MEMBER";

	/**
	 * 租户账号 session key
	 */
	public final static String SESSION_AGENT_KEY = "USER_SESSION_AGENT";

	/**
	 * 代理账号 session key
	 */
	public final static String SESSION_DAILI_KEY = "USER_SESSION_DAILI";
	
	
	public final static String SESSION_ACCOUNT_INFO_SUFFIX = "_INFO";

	/**
	 * 开发环境
	 */
	public static boolean SYS_MODE_DEVELOP = false;
	
	/**
	 * 开发环境固定IP
	 */
	public static boolean SYS_DEVELOP_IP = false;
	
	/**
	 * 平台电子游戏版本
	 */
	public static String PLATFROM_EGAME_VERSION = "v1";
	
	/**
	 * 模板切换
	 */
	public final static String SOURCE_FOLDER_MEMBER_KEY = "SOURCE_FOLDER_MEMBER";

	/**
	 * es集群前缀
	 */
	public static String ELASTICSEARCH_SERVER_PREFIX = null;
	/**
	 * 是否开启es服务
	 */
	public static boolean SYS_ELASTICSEARCH_SERVER_OPEN = false;

}
