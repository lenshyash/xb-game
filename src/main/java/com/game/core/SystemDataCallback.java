package com.game.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.filter.DataCallback;
import org.jay.frame.filter.SysThreadData;
import org.jay.frame.util.StringUtil;

import com.game.constant.StationConfig;
import com.game.exception.LoginErrorException;
import com.game.exception.NotStationException;
import com.game.model.AdminUser;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.user.online.OnlineManager;
import com.game.util.StationConfigUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

public class SystemDataCallback implements DataCallback {
	private Logger logger = Logger.getLogger(SystemDataCallback.class);

	public Object initData(SysThreadData threadData) {
		CustomThreadData data = new CustomThreadData();
		threadData.setSysData(data);
		HttpServletRequest request = threadData.getRequest();
		HttpSession session = threadData.getSession();

		StringBuffer requestUrl = request.getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		if (domain.equals(SystemConfig.CONTROL_DOMAIN)) { // 总控后台
			String curIp = UserUtil.getIpAddress();
			data.setStationType(StationType.ADMIN);
			if (!isAdminWhiteIp(curIp)) {
				throw new GenericException("您的IP禁止访问后台" + curIp);
			}
			AdminUser user = (AdminUser) session.getAttribute(SystemConfig.SESSION_ADMIN_KEY);

			// if (user != null && curIp.equals(user.getLoginIp()) == false) {
			// logger.error(user.getAccount() + " CONTROL_DOMAIN ip变动，前ip＝" +
			// user.getLoginIp() + " 后Ip＝" + curIp);
			// UserUtil.logIpAddress();
			// user = null;
			// }
			threadData.setSysUser(user);
			return data;
		}
		if (isReceiveDomain(domain)) {
			data.setStationType(StationType.RECEIVE_DATA);
			return data;
		}
		if (isApiDomain(domain)) {
			data.setStationType(StationType.API);
			return data;
		}
		SysStation station = WebUtil.getStation(request);
		if (station == null) {
			throw new NotStationException(domain);
		}
		data.setStation(station);
		String url = request.getRequestURI();
		String basePath = request.getContextPath();
		if (url.startsWith(basePath + SystemConfig.AGENT_CONTROL_PATH + "/")) {// 租户后台
			data.setStationType(StationType.AGENT);
			setLoginAccount(request, session, SystemConfig.SESSION_AGENT_KEY, station,data,threadData);
		} else if (url.startsWith(basePath + SystemConfig.DAILI_CONTROL_PATH + "/")) {// 代理后台
			data.setStationType(StationType.DAILI);
			setLoginAccount(request, session, SystemConfig.SESSION_DAILI_KEY, station,data,threadData);
		} else if (url.startsWith(basePath + SystemConfig.MOBILE_CONTROL_PATH + "/")) {// 手机端
			data.setStationType(StationType.MOBILE);
			setLoginAccount(request, session, SystemConfig.SESSION_MEMBER_KEY, station,data,threadData);
		} else if (url.startsWith(basePath + SystemConfig.NATIVE_CONTROL_PATH + "/")) {// 原生手机端
			data.setStationType(StationType.NATIVE);
			setLoginAccount(request, session, SystemConfig.SESSION_MEMBER_KEY, station,data,threadData);
		} else if (url.startsWith(basePath + SystemConfig.API_CONTROL_PATH + "/")) {// 三方访问API端
			data.setStationType(StationType.API);
			setLoginAccount(request, session, SystemConfig.SESSION_MEMBER_KEY, station,data,threadData);
		}else {// 会员前端
			data.setStationType(StationType.MEMBER);
			setLoginAccount(request, session, SystemConfig.SESSION_MEMBER_KEY, station,data,threadData);
		}
		return data;
	}

	private boolean isAdminWhiteIp(String ip) {
		if (StringUtils.isNotEmpty(SystemConfig.SYS_ADMIN_IP_WHITE_LIST)) {
			String[] dds = SystemConfig.SYS_ADMIN_IP_WHITE_LIST.split(",");
			for (String d : dds) {
				if (StringUtils.equals(d, ip)) {
					return true;
				}
			}
			return false;
		}
		return true;
	}

	private boolean isReceiveDomain(String domain) {
		if (StringUtils.isNotEmpty(SystemConfig.SYS_RECEIVE_DATA_IPS)) {
			String[] dds = SystemConfig.SYS_RECEIVE_DATA_IPS.split(",");
			for (String d : dds) {
				if (StringUtils.equals(d, domain)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断是否是第三方访问平台API数据接口
	 * @param domain
	 * @return
	 */
	private boolean isApiDomain(String domain) {
		if (StringUtils.isNotEmpty(SystemConfig.SYS_ACCESS_API_DATA_DOMAIN)) {
			String[] dds = SystemConfig.SYS_ACCESS_API_DATA_DOMAIN.split(",");
			for (String d : dds) {
				if (StringUtils.equals(d, domain)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 检验ip是否一致
	 * 
	 * @param request
	 * @param session
	 * @param key
	 * @return
	 */
	private void setLoginAccount(HttpServletRequest request, HttpSession session, String key,
			SysStation station,CustomThreadData data,SysThreadData threadData) {
		SysAccount account = null;
		SysAccountInfo accountInfo = null;
		OnlineManager.checkLoginError(session);

		account = (SysAccount) session.getAttribute(key);
		String infoKey =key+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX;
		accountInfo = (SysAccountInfo) session.getAttribute(infoKey);
		if (account == null) {
			if(SystemConfig.SESSION_MEMBER_KEY.equals(key)) {
				account = (SysAccount) session.getAttribute(SystemConfig.SESSION_DAILI_KEY);
				infoKey =SystemConfig.SESSION_DAILI_KEY+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX;
				accountInfo = (SysAccountInfo) session.getAttribute(infoKey);
			}
			if(account == null) {
				return;
			}
			
			//没开启代理登录到会员端就返回null;
			if(!"on".equals(StationConfigUtil.get(account.getStationId(),StationConfig.onoff_daili_login_member_platfrom))) {
				return;
			}
		}
		
		if(account != null && accountInfo != null && !StringUtil.equals(account.getId(), accountInfo.getAccountId())){
			accountInfo = null;
		}

		String lastLoginIp = account.getLastLoginIp();
		String ip = UserUtil.getIpAddress();
		// 总控后台
		if (SystemConfig.SESSION_ADMIN_KEY.equals(key)) {
			return;
		}
		if (SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER == account.getAccountType().longValue()) {
			if (!isAdminWhiteIp(ip)) {
				session.removeAttribute(key);
				throw new LoginErrorException("您的IP禁止访问租户后台" + ip);
			}
		} else if (SystemConfig.SESSION_AGENT_KEY.equals(key) && !ip.equals(lastLoginIp)) {// 租户后台
			if (!StringUtils.equals(StationConfigUtil.get(station.getId(), StationConfig.onoff_agent_ip_change),
					"off")) {
				logger.error(account.getAccount() + "  ip变动，前ip＝" + lastLoginIp + "  后Ip＝" + ip);
				UserUtil.logIpAddress();
				session.removeAttribute(key);
				throw new LoginErrorException("您的IP发生变动!", LoginErrorException.LOGIN_ERROR_IP_CHANGED);
			}
		}

		// OnlineManager.resetOnlineSessionId(session, account);
		// if(SysAccount.ACCOUNT_PLATFORM_MEMBER==account.getAccountType().longValue()){
		// account=OnlineManager.validOnlineInfo(account);
		// }
		data.setAccountInfo(accountInfo);
		threadData.setSysUser(account);
	}

}
