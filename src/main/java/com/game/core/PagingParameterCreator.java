package com.game.core;

import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebParameterCreator;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.StringUtil;

import com.game.permission.annotation.SortMapping;
import com.game.util.UserUtil;

public class PagingParameterCreator implements QueryWebParameterCreator{
	
	private final Map EMPTY_MAP = new TreeMap();
	
	@Override
	public QueryWebParameter generate(ServletRequest request) {
		HttpServletRequest req = (HttpServletRequest)request;
		int pageNo = 1;//默认第一页
		int pageSize = 20; //默认20条
		String pageNoParam = req.getParameter("page");
		String pageSizeParam = req.getParameter("rows");
		if(pageNoParam != null && pageSizeParam != null){
			//彩票中心 query分页插件
			pageNo = StringUtil.toInt(pageNoParam);
			pageSize = StringUtil.toInt(pageSizeParam);
		}else if((pageNoParam = req.getParameter("pageNumber")) != null && (pageSizeParam = req.getParameter("pageSize")) != null ){
			//bootstrap table
			pageNo = StringUtil.toInt(pageNoParam);
			pageSize = StringUtil.toInt(pageSizeParam);
		}
		String column = null;
		String sortType = null;
		SortInfo si = UserUtil.getSortInfo();
		if(si != null){
			column = si.getColumn();
			sortType = si.getSortType();
			if(SortMapping.SORT_ASC.equalsIgnoreCase(sortType) == false && SortMapping.SORT_DESC.equals(sortType)){
				sortType = SortMapping.SORT_DESC;
			}
		}
		return 	QueryWebUtils.generateQueryWebParameter(EMPTY_MAP, pageNo, pageSize,column, sortType);
	}
	
}
