package com.game.core;

public class SortInfo {
	
	public SortInfo(String column,String sortType){
		this.column = column;
		this.sortType = sortType;
	}
	
	private String column;
	
	private String sortType;

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
}
