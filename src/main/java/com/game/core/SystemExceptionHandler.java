package com.game.core;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.game.exception.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jay.frame.exception.AppException;
import org.jay.frame.exception.ExceptionHandler;
import org.jay.frame.exception.ExceptionUtil;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.NoLoginException;
import org.jay.frame.exception.PageNotFoundException;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;

import com.alibaba.fastjson.JSONObject;
import com.game.model.SysStationDomain;
import com.game.util.StationUtil;

public class SystemExceptionHandler implements ExceptionHandler {
	private Logger logger = Logger.getLogger(SystemExceptionHandler.class);

	public void resolveException(Exception exception, HttpServletResponse response, HttpServletRequest request) {
		if (exception!=null)
			exception.printStackTrace();
		try {
			AppException appEx = ExceptionUtil.handle(exception);
			//无站点时不打印日志
			if (appEx.getClass() != NotStationException.class) {
				doLog(appEx);
			}
			if (ActionUtil.isAjaxRequest(request)) {// 响应json
				if (StationUtil.isNativeStation() && (appEx.getClass() == NoLoginException.class || appEx.getClass() == LoginErrorException.class)) { // 未登入，跳回登入界面
					JSONObject json = JSONObject.parseObject(appEx.toJsonMsg());
					json.put("isLogin",false);
					ActionUtil.render("application/json", JsonUtil.toJson(json), appEx.getResponseHeader(), response);
				}else {
					ActionUtil.render("application/json", appEx.toJsonMsg(), appEx.getResponseHeader(), response);
				}
			} else {
				if (appEx.getClass() == NoLoginException.class) { // 未登入，跳回登入界面
					try {
						String base = request.getContextPath();
						if (StationUtil.isAgentStation()) {
							response.sendRedirect(base + "/agent");
						} else if (StationUtil.isAdminPage()) {
							response.sendRedirect(base + "/admin/timeout.do");
						} else if (StationUtil.isMobilePage()) {
							response.sendRedirect(base + "/mobile");
						} else if (StationUtil.isDailiStation()) {
							response.sendRedirect(base + "/daili");
						} else if(StationUtil.isNativeStation()){
							JSONObject json = JSONObject.parseObject(appEx.toJsonMsg());
							json.put("isLogin",false);
							ActionUtil.render("application/json", JsonUtil.toJson(json), appEx.getResponseHeader(), response);
						}else {
							response.sendRedirect(base + "/timeout.do");
						}
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
					return;
				} else if (appEx.getClass() == LoginErrorException.class) { // 登录状态发行异常
					LoginErrorException error = (LoginErrorException) appEx;
					try {
						String base = request.getContextPath();
						if (StationUtil.isAgentStation()) {
							response.sendRedirect(base + "/agent/loginError.do?errorCode=" + error.getErrorCode());
						} else if (StationUtil.isDailiStation()) {
							response.sendRedirect(base + "/daili/loginError.do?errorCode=" + error.getErrorCode());
						} else if(StationUtil.isNativeStation()){
							JSONObject json = JSONObject.parseObject(appEx.toJsonMsg());
							json.put("isLogin",false);
							ActionUtil.render("application/json", JsonUtil.toJson(json), appEx.getResponseHeader(), response);
						}else {
							response.sendRedirect(base + "/loginError.do?errorCode=" + error.getErrorCode());
						}
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
					return;
				}

				if (StationUtil.isAdminStation() || appEx.getClass() == NotStationException.class) {
					ActionUtil.renderHtml("<html><body>500 ERROR </br> " + appEx.getMessage() + "</body></html>", null);
					return;
				}
				
				if (StationUtil.isNativeStation() && appEx.getClass() == GenericException.class) {
					ActionUtil.render("application/json", appEx.toJsonMsg(), appEx.getResponseHeader(), response);
					return;
				}
				if (StationUtil.isApiStation() && appEx.getClass() == GenericException.class) {
					ActionUtil.render("application/json", appEx.toJsonMsg(), appEx.getResponseHeader(), response);
					return;
				}
				if (StationUtil.isNativeStation() && appEx.getClass() == RequestBusyException.class) {
					ActionUtil.render("application/json", appEx.toJsonMsg(), appEx.getResponseHeader(), response);
					return;
				}
				if( appEx.getClass()== PayDomainException.class){
					ActionUtil.renderHtml("<html><head><title>支付</title></head><body><H>支付平台欢迎您！</H></body></html>", null);
					return;
				}
				if (StationUtil.isReceiveStation()) {
					ActionUtil.renderHtml("It is OK!", null);
					return;
				}
				try {
					String base = StringUtil.equals(StationUtil.getDomainType(), SysStationDomain.TYPE_AGENT) ? "/agent"
							: "";
					if (appEx.getClass() == PageNotFoundException.class) {
						response.sendRedirect(request.getContextPath() + base + "/error.do?code=404");
					}
					if (appEx.getClass() == RequestBusyException.class) {
						response.sendRedirect(request.getContextPath() + base + "/error.do?code=408");
					}
					if (appEx.getClass() == ForbiddenException.class) {
						response.sendRedirect(request.getContextPath() + base + "/error.do?code=403");
					} else {
						response.sendRedirect(request.getContextPath() + base + "/error.do?code=500");
					}
				} catch (Exception e) {

				}

			}
		} catch (Exception e) {
			logger.error("错误处理对应的错误：", exception);
			logger.error("错误处理发生错误", e);
		}
	}

	private void doLog(AppException appEx) {
		Exception e = (Exception) appEx;
		String msg = appEx.getErrorMessage() + ":" + appEx.getStackTraceMessage();
		// tomcat catalina.out文件
		if (!SystemConfig.SYS_MODE_DEVELOP
				&& (appEx.getClass() == GenericException.class || appEx.getClass() == NotStationException.class)) {
			// 生产环境下，不输出业务异常的堆栈信息
			System.err.println(msg);
			e = null;
		} else if (appEx.logStackTrace()) {
			Throwable cause = appEx.getCause();
			if (cause != null) {
				cause.printStackTrace();// 控制台输出堆栈信息
			} else if (appEx instanceof Exception) {
				e.printStackTrace();
			}
		} else {
			System.err.println(msg);
		}

		// 日志文件
		String fullMessage = appEx.getFullMessage();
		if (appEx.logStackTrace() == false) {
			e = null;
			fullMessage = msg;
		}
		Level level = appEx.getLogLevel();
		if (level == Level.DEBUG) {
			logger.debug(fullMessage, e);
		} else if (level == Level.INFO) {
			logger.info(fullMessage, e);
		} else if (level == Level.WARN) {
			logger.warn(fullMessage, e);
		} else if (level == Level.ERROR) {
			logger.error(fullMessage, e);
		} else if (level == Level.FATAL) {
			logger.fatal(fullMessage, e);
		}
	}
}
