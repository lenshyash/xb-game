package com.game.user.online;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class OnlineInfo {
	private Long id;
	private String ip;
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date time;
	private Integer on;//在线=1，离线=0
	private String deviceName;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getOn() {
		return on;
	}

	public void setOn(Integer on) {
		this.on = on;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
}
