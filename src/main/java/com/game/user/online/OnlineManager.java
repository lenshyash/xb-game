package com.game.user.online;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheManager;
import com.game.cache.CacheType;
import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.exception.LoginErrorException;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.service.SysAccountService;
import com.game.service.SysStationService;
import com.game.util.DateUtil;
import com.game.util.SpringUtil;
import com.game.util.StationConfigUtil;

public class OnlineManager {
	private static Logger logger = Logger.getLogger(OnlineManager.class);
	private static String USER_ONLINE_INFO_KEY = "GAME:CORE:USER:ONLINE:LIST";
	public static final String USER_ONLINE_SESSION_ID = "online:sid:";
//	public static final String USER_ONLINE_LIST = "online:list:sid";
	public static final String USER_ONLINE_SET = "online:set:sid";
	public static final String SPRING_SESSION_KEY = "spring:session:sessions:";
	public static final String SPRING_SESSION_ATTR_KEY = "sessionAttr:";

	public static final int OVERDATETIME = 7 * 24 * 60 * 60;

	public static void doOnline(HttpServletRequest request, HttpSession session, String userSessionKey, SysAccount user,SysAccountInfo userInfo, String ip) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		SysAccount old = (SysAccount) session.getAttribute(userSessionKey);
		Long stationId = StringUtil.toLong(user.getStationId());
		
		//登录设备
		String useragent = request.getHeader("User-Agent");
		System.out.println("登录设备是"+useragent);
		String deviceName = getDeviceName(useragent);
		
		// 该session 已经有用户登录，需要先将他踢下线
		if (old != null && !old.getId().equals(user.getId())) {
			updateAccountOffline(old.getId());
			RedisAPI.delCache(db,
					getOnlineSessionId(old.getStationId(), old.getId(), old.getAccountType(), old.getParents()));

			RedisAPI.rpush(USER_ONLINE_INFO_KEY, db, getOnlineInfoJSON(old.getId(), old.getLastLoginIp(),
					SysAccount.ONLINE_FLAG_OFF, old.getLastLoginDatetime(), deviceName));
		}
		
		String stationListKey = getStationSetKey(stationId,SysAccount.ACCOUNT_PLATFORM_MEMBER);
		String key = getOnlineSessionId(stationId, user.getId(), user.getAccountType(), user.getParents());
		
		if(!"on".equals(StationConfigUtil.get(stationId,StationConfig.onoff_daili_multi_login))
				|| !SystemConfig.SESSION_DAILI_KEY.equals(userSessionKey)) {
			String sId = RedisAPI.getCache(key, db);
			// 用户重复登录
			if (StringUtils.isNotEmpty(sId) && !StringUtils.equals(sId, session.getId())) {
				RedisAPI.addCache(LoginErrorException.getRedisKeyBySid(sId),
						LoginErrorException.LOGIN_ERROR_OTHER_LOGIN + "", LoginErrorException.LOGIN_ERROR_KEY_TIMEOUT, db);
				RedisAPI.hdel(SPRING_SESSION_KEY + sId, SPRING_SESSION_ATTR_KEY + userSessionKey);
				RedisAPI.hdel(SPRING_SESSION_KEY + sId, SPRING_SESSION_ATTR_KEY + userSessionKey+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX);
			}
		}

		user.setLastLoginIp(ip);
		Date lastLoginDate = new Date();
		user.setLastLoginDatetime(lastLoginDate);
		session.setAttribute(userSessionKey, user);
		session.setAttribute(userSessionKey+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX, userInfo);
		
		if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
				|| ("on".equals(StationConfigUtil.get(StationConfig.onoff_guide_participation_online)) && StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))
				|| ("on".equals(StationConfigUtil.get(StationConfig.onoff_agent_participation_online)) && StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT))) {
			RedisAPI.zadd(stationListKey,key, 1d,db);
			updateAccountOnline(user.getId());
		}
		//租户管理员和租户超级管理员 登录后马上修改最后登录ip
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) || StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			updateAccountLastLoginIp(user.getId(), ip);
		}
		//
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) || StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			RedisAPI.addCache(key, session.getId(), OVERDATETIME, db);
		}else {
			RedisAPI.rpush(USER_ONLINE_INFO_KEY, db, getOnlineInfoJSON(user.getId(), ip, SysAccount.ONLINE_FLAG_ON, lastLoginDate, deviceName));
			RedisAPI.addCache(key, session.getId(), OVERDATETIME, db);
		}

		
		
	}

	public static void doOffLine(HttpSession session, SysAccount user) {
		if (session == null) {
			return;
		}
		if (user == null) {
			return;
		}
		try {
			updateAccountOffline(user.getId());
			String userSessionKey = SystemConfig.SESSION_MEMBER_KEY;
			if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)
					|| StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
				userSessionKey = SystemConfig.SESSION_AGENT_KEY;
			} else if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
					|| StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
				userSessionKey = SystemConfig.SESSION_DAILI_KEY;
			}
			session.removeAttribute(userSessionKey);
			session.removeAttribute(userSessionKey+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX);
			int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
			RedisAPI.rpush(USER_ONLINE_INFO_KEY, db, getOnlineInfoJSON(user.getId(), user.getLastLoginIp(),
					SysAccount.ONLINE_FLAG_OFF, user.getLastLoginDatetime(), user.getLastLoginDevice()));
			String key = getOnlineSessionId(user.getStationId(), user.getId(), user.getAccountType(), user.getParents());
			RedisAPI.delCache(db,key);
			String stationListKey = getStationSetKey(user.getStationId(),SysAccount.ACCOUNT_PLATFORM_MEMBER);
			RedisAPI.zrem(stationListKey, key, db);
			
			
		} catch (Exception e) {
			logger.error("注销发生错误", e);
		}
	}

	public static SysAccount getMemberBySid(String sessionId) {
		Session session = SpringUtil.getBean(RedisOperationsSessionRepository.class).getSession(sessionId);
		if (session != null) {
			return session.getAttribute(SystemConfig.SESSION_MEMBER_KEY);
		}
		return null;
	}

	public static void forcedOffLine(SysAccount user) {
		if (user == null)
			return;
		updateAccountOffline(user.getId());
		String key = getOnlineSessionId(user.getStationId(), user.getId(), user.getAccountType(), user.getParents());
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		String sessionId = RedisAPI.getAndDel(key, db);
		if (StringUtils.isNotEmpty(sessionId)) {
			String userSessionKey = SystemConfig.SESSION_MEMBER_KEY;
			if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)
					|| StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
				userSessionKey = SystemConfig.SESSION_AGENT_KEY;
			} else if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
					|| StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
				userSessionKey = SystemConfig.SESSION_DAILI_KEY;
			}
			RedisAPI.hdel(SPRING_SESSION_KEY + sessionId, SPRING_SESSION_ATTR_KEY + userSessionKey);
			RedisAPI.hdel(SPRING_SESSION_KEY + sessionId, SPRING_SESSION_ATTR_KEY + userSessionKey+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX);
			RedisAPI.rpush(USER_ONLINE_INFO_KEY, db, getOnlineInfoJSON(user.getId(), user.getLastLoginIp(),
					SysAccount.ONLINE_FLAG_OFF, user.getLastLoginDatetime(), user.getLastLoginDevice()));
			String stationListKey = getStationSetKey(user.getStationId(),SysAccount.ACCOUNT_PLATFORM_MEMBER);
			RedisAPI.zrem(stationListKey, key, db);
		}
	}

	public static List<OnlineInfo> getOnlineInfos(Integer recordNum) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		List<String> list = RedisAPI.lpopByPipe(USER_ONLINE_INFO_KEY, db, recordNum);
		List<OnlineInfo> oiList = null;
		if (list != null && !list.isEmpty()) {
			oiList = new ArrayList<>();
			for (String s : list) {
				if (StringUtils.isNotEmpty(s)) {
					oiList.add(JSON.parseObject(s, OnlineInfo.class));
				}
			}
		}
		return oiList;
	}

	public static Set<Long> getOnlineUserIds() {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		SysStationService service = SpringUtil.getBean(SysStationService.class);
		List<SysStation> stations = service.getAllActive();
		Set<String> lpopSet = null;
		Long stationId = null;
		String stationSetKey = "";
		Set<Long> set = new HashSet<>();
		for (SysStation sysStation : stations) {
			stationId = sysStation.getId();
			stationSetKey = getStationSetKey(stationId,SysAccount.ACCOUNT_PLATFORM_MEMBER);
			lpopSet = RedisAPI.zrange(stationSetKey, 0, -1,db);
			if(lpopSet != null && !lpopSet.isEmpty()) {
				String sId = null;
				long id;
				for (String s : lpopSet) {
					sId = RedisAPI.getCache(s, db);
					id = NumberUtils.toLong(s.substring(s.lastIndexOf(":") + 1));
					if (RedisAPI.exists(SPRING_SESSION_KEY + sId)) {
						set.add(id);
					} else {
						updateAccountOffline(id);
						RedisAPI.delCache(db, s);
						RedisAPI.zrem(stationSetKey, s,db);
					}
				}
			}
		}
		
		return set;
	}

	public static Integer getOnlineCount(Long stationId, String parents) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		String stationKey = getStationSetKey(stationId,SysAccount.ACCOUNT_PLATFORM_MEMBER);
		Set<String> keys = RedisAPI.zrange(stationKey, 0, -1,db);
		if (keys == null) {
			return 0;
		}
		int size = 0;
		if(parents == null) {
			size = keys.size();
		}else {
			for (String key : keys) {
				if(key.indexOf(parents) != -1) {
					size ++;
				}
			}
		}
		return size;
	}

	private static void updateAccountOffline(Long accountId) {
		SysAccountService service = SpringUtil.getBean(SysAccountService.class);
		service.updateUserOffline(accountId);
	}
	private static void updateAccountOnline(Long accountId) {
		SysAccountService service = SpringUtil.getBean(SysAccountService.class);
		service.updateUserOnline(accountId);
	}
	private static void updateAccountLastLoginIp(Long accountId, String lastLoginIp) {
		SysAccountService service = SpringUtil.getBean(SysAccountService.class);
		service.updateUserLastLoginIp(accountId, lastLoginIp);
	}
	public static void checkLoginError(HttpSession session) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		String err = RedisAPI.getAndDel(LoginErrorException.getRedisKeyBySid(session.getId()), db);
		if (StringUtils.isNotEmpty(err)) {
			if (err.equals("1")) {
				throw new LoginErrorException("账号在其他地方登录!", LoginErrorException.LOGIN_ERROR_OTHER_LOGIN);
			} else if (err.equals("2")) {
				throw new LoginErrorException("登录超时!", LoginErrorException.LOGIN_ERROR_FORCED_OFFLINE);
			}
		}
	}

	/**
	 * 获取在线状态json
	 * 
	 * @param id
	 * @param ip
	 * @param onStatus在线=1，离线=0
	 * @param date
	 * @return
	 */
	private static String getOnlineInfoJSON(Long id, String ip, Long onStatus, Date date, String deviceName) {
		JSONObject json = new JSONObject();
		json.put("id", id);
		json.put("ip", ip);
		json.put("on", onStatus);
		json.put("time", DateUtil.toDatetimeStr(date));
		json.put("deviceName", deviceName);
		return json.toJSONString();
	}

	private static String getOnlineSessionId(Long stationId, Long uId, Long accountType, String parents) {
		return new StringBuilder(USER_ONLINE_SESSION_ID).append(stationId).append(":").append(accountType).append(":")
				.append(parents).append(":").append(uId).toString();
	}
	
//	private static String getStationListKey(Long stationId, Long accountType) {
//		return new StringBuilder(USER_ONLINE_LIST).append(stationId).append(":").append(accountType).toString();
//	}
	
	private static String getStationSetKey(Long stationId, Long accountType) {
		return new StringBuilder(USER_ONLINE_SET).append(stationId).append(":").append(accountType).toString();
	}

	/**
	 * 用户有登录，而redis 和数据库没有登录信息时调用，刷上线
	 * 
	 * @param session
	 * @param user
	 */
	public static SysAccount resetOnlineSessionId(HttpSession session, SysAccount user) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		String key = getOnlineSessionId(user.getStationId(), user.getId(), user.getAccountType(), user.getParents());
		if (!RedisAPI.exists(key, db)) {
			RedisAPI.addCache(key, session.getId(), OVERDATETIME, db);
		}
		return user;
	}

	public static void upholdUserOnlineStatus(Long stationId) {
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		SysAccountService accountService = SpringUtil.getBean(SysAccountService.class);
		Set<String> lpopSet = null;
		String stationSetKey = "";
		//先将站点所有会员下线
		accountService.updateOfflineStatus(stationId);
		stationSetKey = getStationSetKey(stationId,SysAccount.ACCOUNT_PLATFORM_MEMBER);
		lpopSet = RedisAPI.zrange(stationSetKey, 0, -1,db);
		Set<Long> set = new HashSet<>();
		if(lpopSet != null && !lpopSet.isEmpty()) {
			String sId = null;
			long id;
			for (String s : lpopSet) {
				sId = RedisAPI.getCache(s, db);
				id = NumberUtils.toLong(s.substring(s.lastIndexOf(":") + 1));
				if (RedisAPI.exists(SPRING_SESSION_KEY + sId)) {
					set.add(id);
				} else {
					updateAccountOffline(id);
					RedisAPI.delCache(db, s);
					RedisAPI.zrem(stationSetKey, s,db);
				}
			}
			if(!set.isEmpty() && set.size()>0) {
				accountService.updateOnlineStatus(set);
			}
		}
	}

	/**
	 * 用户有登录，而redis 和数据库没有登录信息时调用,刷下线
	 * 
	 * @param user
	 */
	// public static SysAccount validOnlineInfo(SysAccount user) {
	// int db =
	// CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
	// String key = getOnlineSessionId(user.getStationId(), user.getId(),
	// user.getAccountType());
	// if (!RedisAPI.exists(key, db)) {
	// forcedOffLine(user);
	// return null;
	// }
	// return user;
	// }
	
	public static String getDeviceName(String useragent) {
		String deviceName = "未知设备";
		
		if(StringUtil.isEmpty(useragent)) {
			return deviceName;
		}
		
		useragent = useragent.toLowerCase();
		if(useragent.contains("windows")) {
			deviceName = "PC电脑";																//安卓请求框架
		}else if(useragent.contains("android") || useragent.contains("adr") || useragent.contains("okhttp") || useragent.contains("okgo")) { 
			deviceName = "android设备";
		}else if(useragent.contains("ios") || useragent.contains("iphone") || useragent.contains("ipad") || useragent.contains("mac") || useragent.contains("ipod")) {
			deviceName = "苹果设备";
		}
		
		return deviceName;
	}
	public static void main(String[] args) {
		String deviceName = getDeviceName("Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X; zh-CN) AppleWebKit/537.51.1 (KHTML, like Gecko) Mobile/17F80 UCBrowser/12.6.2.1219 Mobile  AliApp(TUnionSDK/0.1.20.3)");
		
		System.out.println(deviceName);
	}
}
