package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "mny_score_record")
public class MnyScoreRecord {
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 变动前积分
	 */
	@Column(name = "before_score")
	private BigDecimal beforeScore;

	/**
	 * 变动积分
	 */
	@Column(name = "score")
	private BigDecimal score;

	/**
	 * 变动后积分
	 */
	@Column(name = "after_score")
	private BigDecimal afterScore;

	/**
	 * 账变类型
	 */
	@Column(name = "type")
	private Long type;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 用户ID
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 用户账号
	 */
	@Column(name = "account", length = 20)
	private String account;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 备注说明
	 */
	@Column(name = "remark")
	private String remark;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getBeforeScore() {
		return this.beforeScore;
	}

	public void setBeforeScore(BigDecimal beforeScore) {
		this.beforeScore = beforeScore;
	}

	public BigDecimal getScore() {
		return this.score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public BigDecimal getAfterScore() {
		return this.afterScore;
	}

	public void setAfterScore(BigDecimal afterScore) {
		this.afterScore = afterScore;
	}

	public Long getType() {
		return this.type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
