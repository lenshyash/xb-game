package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_deposit_info")
public class MemberDepositInfo {
	/**
	 * 用户ID
	 */
	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	/**
	 * 存款次数
	 */
	@Column(name = "deposit_count")
	private Long depositCount;

	/**
	 * 存款总额
	 */
	@Column(name = "deposit_total")
	private BigDecimal depositTotal;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getDepositCount() {
		return depositCount;
	}

	public void setDepositCount(Long depositCount) {
		this.depositCount = depositCount;
	}

	public BigDecimal getDepositTotal() {
		return depositTotal;
	}

	public void setDepositTotal(BigDecimal depositTotal) {
		this.depositTotal = depositTotal;
	}
}
