
package com.game.model;

import java.io.Serializable;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.util.StringUtil;

import com.game.util.ValidateUtil;

@Table(name = "sys_account_info")
public class SysAccountInfo  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2644874454254936413L;

	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	@Column(name = "account")
	private String account;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "card_no")
	private String cardNo;

	@Column(name = "receipt_pwd")
	private String receiptPwd;

	@Column(name = "phone")
	private String phone;

	@Column(name = "province")
	private String province;

	@Column(name = "city")
	private String city;

	@Column(name = "email")
	private String email;

	@Column(name = "bank_address")
	private String bankAddress;

	@Column(name = "qq")
	private String qq;

	@Column(name = "sex")
	private Long sex;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "wechat")
	private String wechat;
	
	@Column(name = "card_no_status")
	@InsertValue(value = "1")
	private Long cardNoStatus;
	
	@Column(name = "is_group")
	private Long isGroup;
	
	@Column(name = "qz_group_name")
	private String qzGroupName;
	
	@Column(name = "head_url")
	private String headUrl;
	
	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		if(ValidateUtil.isUnsafeChar(userName)){
			throw new GenericException("出款人姓名包含非法字符");
		}
		if(StringUtil.isNotEmpty(userName) && !ValidateUtil.isXingMing(userName)) {
			throw new GenericException("出款人姓名包含非法字符");
		}
		this.userName = userName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(ValidateUtil.isUnsafeChar(userName)){
			throw new GenericException("银行卡号包含非法字符");
		}
		if(StringUtil.isNotEmpty(cardNo) && !StringUtil.isNumber(cardNo)) {
			throw new GenericException("银行卡号包含非法字符");
		}
		this.cardNo = cardNo;
	}

	public String getReceiptPwd() {
		return receiptPwd;
	}

	public void setReceiptPwd(String receiptPwd) {
		this.receiptPwd = receiptPwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		if(ValidateUtil.isUnsafeChar(phone)){
			throw new GenericException("手机号码包含非法字符");
		}
		if(StringUtil.isNotEmpty(phone) && !ValidateUtil.isNumber(phone)) {
			throw new GenericException("手机号码包含非法字符");
		}
		this.phone = phone;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		if(ValidateUtil.isUnsafeChar(province)){
			throw new GenericException("省份包含非法字符");
		}
		if(StringUtil.isNotEmpty(province) && !ValidateUtil.isChinese(province)) {
			throw new GenericException("省份包含非法字符");
		}
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		if(ValidateUtil.isUnsafeChar(province)){
			throw new GenericException("省份包含非法字符");
		}
		if(StringUtil.isNotEmpty(city) && !ValidateUtil.isChinese(city)) {
			throw new GenericException("省份包含非法字符");
		}
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(ValidateUtil.isUnsafeChar(email)){
			throw new GenericException("电子邮箱包含非法字符");
		}
		if(StringUtil.isNotEmpty(email) && !ValidateUtil.isEmail(email)) {
			throw new GenericException("电子邮箱包含非法字符");
		}
		this.email = email;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		if(ValidateUtil.isUnsafeChar(bankAddress)){
			throw new GenericException("银行地址包含非法字符");
		}
		if(StringUtil.isNotEmpty(bankAddress) && !ValidateUtil.isChinese(bankAddress)) {
			throw new GenericException("银行地址包含非法字符");
		}
		this.bankAddress = bankAddress;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		if(ValidateUtil.isUnsafeChar(qq)){
			throw new GenericException("QQ包含非法字符");
		}
		if(StringUtil.isNotEmpty(qq) && !ValidateUtil.isNumber(qq)) {
			throw new GenericException("QQ包含非法字符");
		}
		this.qq = qq;
	}

	public Long getSex() {
		return sex;
	}

	public void setSex(Long sex) {
		this.sex = sex;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		if(ValidateUtil.isUnsafeChar(bankName)){
			throw new GenericException("银行名字包含非法字符");
		}
		if(StringUtil.isNotEmpty(bankName) && !ValidateUtil.isChinese(bankName)) {
			throw new GenericException("银行名字包含非法字符");
		}
		this.bankName = bankName;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		if(StringUtil.isNotEmpty(wechat) && !ValidateUtil.isWechat(wechat)) {
			throw new GenericException("微信包含非法字符");
		}
		this.wechat = wechat;
	}

	public Long getCardNoStatus() {
		return cardNoStatus;
	}

	public void setCardNoStatus(Long cardNoStatus) {
		this.cardNoStatus = cardNoStatus;
	}

	public Long getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(Long isGroup) {
		this.isGroup = isGroup;
	}

	public String getQzGroupName() {
		return qzGroupName;
	}

	public void setQzGroupName(String qzGroupName) {
		this.qzGroupName = qzGroupName;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		if(StringUtil.isNotEmpty(headUrl) && !ValidateUtil.isUrl(headUrl)) {
			throw new GenericException("头像链接包含非法字符");
		}
		this.headUrl = headUrl;
	}

}