package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "sys_account_virtual_address")
public class SysAccountVirtualAddress {


	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 操作员ID
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "money_address")
	private String moneyAddress;

	@Column(name = "pay_id")
	private Long payId;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getMoneyAddress() {
		return moneyAddress;
	}

	public void setMoneyAddress(String moneyAddress) {
		this.moneyAddress = moneyAddress;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}
}
