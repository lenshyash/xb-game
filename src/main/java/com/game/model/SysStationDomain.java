package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

import java.math.BigDecimal;

@Table(name = "sys_station_domain")
public class SysStationDomain extends BaseModel {
	/**
	 * 禁用 、关闭
	 */
	public static final long STATUS_DISABLE = 1L;
	/**
	 * 启用
	 */
	public static final long STATUS_ENABLE = 2L;

	/**
	 * 域名类型 1-后台
	 */
	public static final long TYPE_AGENT = 1L;
	/**
	 * 域名类型 2-前台
	 */
	public static final long TYPE_MEMBER = 2L;
	/**
	* 支付域名
	*/
	public static final long TYPE_PAY = 3L;
	/**
	* 聊天室域名
	*/
	public static final long TYPE_CHAT = 4L;
	/**
	* app域名
	*/
	public static final long TYPE_APP = 5L;
	

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "domain", length = 200)
	private String domain;

	@Column(name = "agent_name")
	private String agentName;

	@Column(name = "agent_id")
	private Long agentId;

	// -- 状态 1-停用 2-启用,
	@Column(name = "status")
	private Long status;

	// -- 注册开关1-关闭 2-打开,
	@Column(name = "is_reg_switch")
	private Long isRegSwitch;
	
	// -- 线路检测开关1-关闭 2-打开,
	@Column(name = "is_xl_switch")
	private Long isXlSwitch;

	// 域名类型（1-后台，2-前台）
	@Column(name = "type")
	private Long type;

	// 前台默认主页，不填时默认访问index.do
	@Column(name = "default_home")
	private String defaultHome;
	
	@Column(name="folder")
	private String folder;
	
	@Column(name="domain_station_name")
	private String domainStationName;
	
	@Column(name="domain_plat_token")
	private String domainPlatToken;
	/**
	 * 域名聊天室开关
	 */
	@Column(name="is_chat_switch")
	private Long isChatSwitch;

	/**
	 * 域名访问限制开关
	 */
	@Column(name="interface_limit")
	private Long interfaceLimit;

	/**
	 * 域名每秒访问限制次数 开启默认3次
	 */
	@InsertValue("3")
	@Column(name="limit_num")
	private BigDecimal limitNum;

	/**
	 * 域名备注
	 */
	@Column(name="remark")
	private String remark;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Long getIsRegSwitch() {
		return isRegSwitch;
	}

	public void setIsRegSwitch(Long isRegSwitch) {
		this.isRegSwitch = isRegSwitch;
	}

	public Long getIsXlSwitch() {
		return isXlSwitch;
	}

	public void setIsXlSwitch(Long isXlSwitch) {
		this.isXlSwitch = isXlSwitch;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getDefaultHome() {
		return defaultHome;
	}

	public void setDefaultHome(String defaultHome) {
		this.defaultHome = defaultHome;
	}
	
	public String getDomainPlatToken() {
		return domainPlatToken;
	}

	public void setDomainPlatToken(String domainPlatToken) {
		this.domainPlatToken = domainPlatToken;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getDomainStationName() {
		return domainStationName;
	}

	public void setDomainStationName(String domainStationName) {
		this.domainStationName = domainStationName;
	}

	public Long getIsChatSwitch() {
		return isChatSwitch;
	}

	public void setIsChatSwitch(Long isChatSwitch) {
		this.isChatSwitch = isChatSwitch;
	}

	public Long getInterfaceLimit() {
		return interfaceLimit;
	}

	public void setInterfaceLimit(Long interfaceLimit) {
		this.interfaceLimit = interfaceLimit;
	}

	public BigDecimal getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(BigDecimal limitNum) {
		this.limitNum = limitNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
