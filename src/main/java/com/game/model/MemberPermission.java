package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="member_permission")
public class MemberPermission {
	
	//没有权限
	public static final long COMMON_NO = 1L;
	//有权限
	public static final long COMMON_YES = 2L;
	
	
	@Column(name="account_id",primarykey=true,generator=Column.PK_BY_HAND)
	private Long accountId;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="real_game_transfer")
	private Long realGameTransfer;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getRealGameTransfer() {
		return realGameTransfer;
	}

	public void setRealGameTransfer(Long realGameTransfer) {
		this.realGameTransfer = realGameTransfer;
	}
	
}
