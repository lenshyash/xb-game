package com.game.model.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class AnnouncementVo {
	private Long type;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date begin;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end;

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
