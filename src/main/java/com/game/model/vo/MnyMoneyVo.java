package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.game.model.dictionary.MoneyRecordType;

public class MnyMoneyVo {

	public static long HANDLERTYPE_ARTIFICIAL = 1l;
	public static long HANDLERTYPE_NOTIFY = 2l;

	/**
	 * 金额
	 */
	private BigDecimal money;

	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 货币类型
	 */
	private MoneyRecordType moneyRecordType;

	/**
	 * 备注
	 */
	private String remark;
	// 订单号
	private String orderId;

	private String receiptPwd;

	private String depositor;

	private Long payId;
	private String payName;

	private BigDecimal fee;
	private Long stationId;

	private Long lastComId;

	private BigDecimal betNum;

	private Date bizDatetime;// 账变对应的订单时间

	private Long handlerType;
	private Integer betType;
	private String code;//彩票lotCode，或者其他code
	private Integer size;//订单条数或者其他导致账变的记录条数
	
	private String bankcode;
	private Boolean firstDeposit;
	private Boolean firstWithdraw;
	/**
	 * 余额宝余额
	 */
	private BigDecimal bgMoney;
	/**
	 * 余额宝累积收益
	 */
	private BigDecimal bgIncome;
	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public MnyMoneyVo() {
	}

	public MoneyRecordType getMoneyRecordType() {
		return moneyRecordType;
	}

	public void setMoneyRecordType(MoneyRecordType moneyRecordType) {
		this.moneyRecordType = moneyRecordType;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getReceiptPwd() {
		return receiptPwd;
	}

	public void setReceiptPwd(String receiptPwd) {
		this.receiptPwd = receiptPwd;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getLastComId() {
		return lastComId;
	}

	public void setLastComId(Long lastComId) {
		this.lastComId = lastComId;
	}

	public BigDecimal getBetNum() {
		return betNum;
	}

	public void setBetNum(BigDecimal betNum) {
		this.betNum = betNum;
	}

	public Date getBizDatetime() {
		return bizDatetime;
	}

	public void setBizDatetime(Date bizDatetime) {
		this.bizDatetime = bizDatetime;
	}

	public Long getHandlerType() {
		return handlerType;
	}

	public void setHandlerType(Long handlerType) {
		this.handlerType = handlerType;
	}

	public Integer getBetType() {
		return betType;
	}

	public void setBetType(Integer betType) {
		this.betType = betType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Boolean getFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(Boolean firstDeposit) {
		this.firstDeposit = firstDeposit;
	}

	public BigDecimal getBgMoney() {
		return bgMoney;
	}

	public void setBgMoney(BigDecimal bgMoney) {
		this.bgMoney = bgMoney;
	}

	public BigDecimal getBgIncome() {
		return bgIncome;
	}

	public void setBgIncome(BigDecimal bgIncome) {
		this.bgIncome = bgIncome;
	}

	public Boolean getFirstWithdraw() {
		return firstWithdraw;
	}

	public void setFirstWithdraw(Boolean firstWithdraw) {
		this.firstWithdraw = firstWithdraw;
	}

	
	
	
}
