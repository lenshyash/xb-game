package com.game.model.vo;

import java.util.Date;

public class MobileSportRecordParameter {
	//所有
	public static final long RECORD_TYPE_ALL = 1;
	//会员赢钱
	public static final long RECORD_TYPE_WIN = 2;
	//未开奖
	public static final long RECORD_TYPE_UNBALANCE = 3;
	//未成功
	public static final long RECORD_TYPE_REJECT = 4;
	
	private Date startDate;
	
	private Date endDate;
	
	private Long recordType;
	
	private Long sportType;
	
	private Long memberId;

	private Long stationId;

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getRecordType() {
		return recordType;
	}

	public void setRecordType(Long recordType) {
		this.recordType = recordType;
	}

	public Long getSportType() {
		return sportType;
	}

	public void setSportType(long sportType) {
		this.sportType = sportType;
	}

	public static long getRecordTypeAll() {
		return RECORD_TYPE_ALL;
	}

	public static long getRecordTypeWin() {
		return RECORD_TYPE_WIN;
	}

	public static long getRecordTypeUnbalance() {
		return RECORD_TYPE_UNBALANCE;
	}

	public static long getRecordTypeReject() {
		return RECORD_TYPE_REJECT;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
}
