package com.game.model.vo;

import java.util.List;

public class MemberSignRecordVo {
	
	private int signDays;

	private List<Child>[] months;
	
	public int getSignDays() {
		return signDays;
	}

	public void setSignDays(int signDays) {
		this.signDays = signDays;
	}

	public List<Child>[] getMonths() {
		return months;
	}

	public void setMonths(List<Child>[] months) {
		this.months = months;
	}

}

class Child{
	
	private boolean signed;
	
	private int score;
	
	private boolean isToday;
	
	public boolean isSigned() {
		return signed;
	}

	public void setSigned(boolean signed) {
		this.signed = signed;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isToday() {
		return isToday;
	}

	public void setToday(boolean isToday) {
		this.isToday = isToday;
	}

}