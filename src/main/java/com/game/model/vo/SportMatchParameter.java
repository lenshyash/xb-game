package com.game.model.vo;

import java.util.Date;

public class SportMatchParameter {
	
	public static final long RESULT_STATUS_ALL = 0L;
	
	public static final long RESULT_STATUS_NONE = 1L;//无赛果
	
	public static final long RESULT_STATUS_SYS = 2L;//系统赛果
	
	public static final long RESULT_STATUS_HAND = 3L;//手结赛果
	
	public static final long BILLING_STATUS_BET_NONE = 1L;//投注未结算
	
	public static final long BILLING_STATUS_NONE = 2L;//所有未结算
	
	public static final long BILLING_STATUS_SYS = 3L;//系统结算
	
	public static final long BILLING_STATUS_HAND = 4L;//非系统结算

	public static final long BILLING_STATUS_HAlF = 5L;//半场结算

	public static final long BILLING_STATUS_BET_HALF_NONE = 6L;//半场投注未结算
	


	private Date begin;//开始时间

	private Date end;//结束时间

	private Long sportType;//类型
	
	private String homeTeam; //主队
	
	private String guestTeam; //客队
	
	private Long resultStatus; //赛果状态
	
	private Long billingStatus; //结算状态
	
	private Long matchId;
	
	private String typeNames;
	
	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getSportType() {
		return sportType;
	}

	public void setSportType(Long sportType) {
		this.sportType = sportType;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getGuestTeam() {
		return guestTeam;
	}

	public void setGuestTeam(String guestTeam) {
		this.guestTeam = guestTeam;
	}

	public Long getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(Long resultStatus) {
		this.resultStatus = resultStatus;
	}

	public Long getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(Long billingStatus) {
		this.billingStatus = billingStatus;
	}

	public String getTypeNames() {
		return typeNames;
	}

	public void setTypeNames(String typeNames) {
		this.typeNames = typeNames;
	}
}
