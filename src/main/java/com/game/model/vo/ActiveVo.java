package com.game.model.vo;

import java.util.Date;

public class ActiveVo {

	private Long activeId;

	private Long stationId;

	private Long activeType;

	private Long status;
	
	private Date begin;
	
	private Date end;

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getActiveType() {
		return activeType;
	}

	public void setActiveType(Long activeType) {
		this.activeType = activeType;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
}
