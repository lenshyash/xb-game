package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.game.util.BigDecimalUtil;

public class MemberDataVo {
	private String account;// 账号
	private String agentName;// 所属代理
	private Date registerDatetime;// 注册时间
	private BigDecimal balance;// 余额
	private BigDecimal betNum;// 打码量
	private BigDecimal drawNeed;// 出款所需打码量
	private Integer accountType;

	/**
	 * 充值
	 */
	private BigDecimal lastDepositMoney;// 最后充值金额
	private String lastDepositDesc;// 最后充值简介
	private Date lastDepositTime;// 最后充值时间
	private BigDecimal depositTotal;// 在线存款总计
	private BigDecimal manualDepositTotal;// 手动加款总计 --账变
	private BigDecimal depositHandlerArtificial;// 全部手动确认充值金额
	/**
	 * 提款
	 */
	private BigDecimal lastDrawMoney;// 最后提款金额
	private Date lastDrawTime;// 最后提款时间
	private String lastDrawDesc;// 最后提款简介
	private BigDecimal withdrawTotal;// 在线提款总计
	private BigDecimal manualWithdrawTotal;// 手动扣款总计 --账变

	private BigDecimal giveTotal;// 存款赠送总计 --账变
	private BigDecimal giveRegisterTotal;// 注册赠送总计 --账变

	private BigDecimal withdrawGiveTotal;// 彩金扣除 --账变
	
	private BigDecimal rebateTotal;// 反水总计 --账变
	private BigDecimal rebateAgentTotal;// 代理返点总计 --账变
	private BigDecimal activeAwardTotal;// 全部活动中奖 --账变

	/**
	 * 体育总计
	 */
	private BigDecimal sportTotal;
	private BigDecimal sportAward;
	/**
	 * 彩票总计
	 */
	private BigDecimal lotteryTotal;
	private BigDecimal lotteryAward;
	/**
	 * 六合彩总计
	 */
	private BigDecimal markSixTotal;
	private BigDecimal markSixAward;
	/**
	 * 真人总计
	 */
	private BigDecimal realTotal;
	private BigDecimal realAward;
	/**
	 * 电子
	 */
	private BigDecimal egameTotal;
	private BigDecimal egameAward;
	/**
	 * 棋牌
	 */
	private BigDecimal chessTotal;
	private BigDecimal chessAward;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Date getRegisterDatetime() {
		return registerDatetime;
	}

	public void setRegisterDatetime(Date registerDatetime) {
		this.registerDatetime = registerDatetime;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public BigDecimal getBetNum() {
		return betNum;
	}

	public void setBetNum(BigDecimal betNum) {
		this.betNum = betNum;
	}

	public BigDecimal getDrawNeed() {
		return drawNeed;
	}

	public void setDrawNeed(BigDecimal drawNeed) {
		this.drawNeed = drawNeed;
	}

	public BigDecimal getLastDepositMoney() {
		return lastDepositMoney;
	}

	public void setLastDepositMoney(BigDecimal lastDepositMoney) {
		this.lastDepositMoney = lastDepositMoney;
	}

	public String getLastDepositDesc() {
		return lastDepositDesc;
	}

	public void setLastDepositDesc(String lastDepositDesc) {
		this.lastDepositDesc = lastDepositDesc;
	}

	public Date getLastDepositTime() {
		return lastDepositTime;
	}

	public void setLastDepositTime(Date lastDepositTime) {
		this.lastDepositTime = lastDepositTime;
	}

	public BigDecimal getLastDrawMoney() {
		return lastDrawMoney;
	}

	public void setLastDrawMoney(BigDecimal lastDrawMoney) {
		this.lastDrawMoney = lastDrawMoney;
	}

	public Date getLastDrawTime() {
		return lastDrawTime;
	}

	public void setLastDrawTime(Date lastDrawTime) {
		this.lastDrawTime = lastDrawTime;
	}

	public String getLastDrawDesc() {
		return lastDrawDesc;
	}

	public void setLastDrawDesc(String lastDrawDesc) {
		this.lastDrawDesc = lastDrawDesc;
	}

	public BigDecimal getDepositTotal() {
		return depositTotal;
	}

	public void setDepositTotal(BigDecimal depositTotal) {
		this.depositTotal = depositTotal;
	}

	public BigDecimal getWithdrawTotal() {
		return withdrawTotal;
	}

	public void setWithdrawTotal(BigDecimal withdrawTotal) {
		this.withdrawTotal = withdrawTotal;
	}

	public BigDecimal getManualDepositTotal() {
		return manualDepositTotal;
	}

	public void setManualDepositTotal(BigDecimal manualDepositTotal) {
		this.manualDepositTotal = manualDepositTotal;
	}

	public BigDecimal getManualWithdrawTotal() {
		return manualWithdrawTotal;
	}

	public void setManualWithdrawTotal(BigDecimal manualWithdrawTotal) {
		this.manualWithdrawTotal = manualWithdrawTotal;
	}

	public BigDecimal getDepositHandlerArtificial() {
		return depositHandlerArtificial;
	}

	public void setDepositHandlerArtificial(BigDecimal depositHandlerArtificial) {
		this.depositHandlerArtificial = depositHandlerArtificial;
	}

	public BigDecimal getGiveTotal() {
		return giveTotal;
	}

	public void setGiveTotal(BigDecimal giveTotal) {
		this.giveTotal = giveTotal;
	}

	public BigDecimal getGiveRegisterTotal() {
		return giveRegisterTotal;
	}

	public void setGiveRegisterTotal(BigDecimal giveRegisterTotal) {
		this.giveRegisterTotal = giveRegisterTotal;
	}

	public BigDecimal getRebateTotal() {
		return rebateTotal;
	}

	public void setRebateTotal(BigDecimal rebateTotal) {
		this.rebateTotal = rebateTotal;
	}

	public BigDecimal getRebateAgentTotal() {
		return rebateAgentTotal;
	}

	public void setRebateAgentTotal(BigDecimal rebateAgentTotal) {
		this.rebateAgentTotal = rebateAgentTotal;
	}

	public BigDecimal getActiveAwardTotal() {
		return activeAwardTotal;
	}

	public void setActiveAwardTotal(BigDecimal activeAwardTotal) {
		this.activeAwardTotal = activeAwardTotal;
	}

	public BigDecimal getSportTotal() {
		return sportTotal;
	}

	public void setSportTotal(BigDecimal sportTotal) {
		this.sportTotal = sportTotal;
	}

	public BigDecimal getSportAward() {
		return sportAward;
	}

	public void setSportAward(BigDecimal sportAward) {
		this.sportAward = sportAward;
	}

	public BigDecimal getLotteryTotal() {
		return lotteryTotal;
	}

	public void setLotteryTotal(BigDecimal lotteryTotal) {
		this.lotteryTotal = lotteryTotal;
	}

	public BigDecimal getLotteryAward() {
		return lotteryAward;
	}

	public void setLotteryAward(BigDecimal lotteryAward) {
		this.lotteryAward = lotteryAward;
	}

	public BigDecimal getMarkSixTotal() {
		return markSixTotal;
	}

	public void setMarkSixTotal(BigDecimal markSixTotal) {
		this.markSixTotal = markSixTotal;
	}

	public BigDecimal getMarkSixAward() {
		return markSixAward;
	}

	public void setMarkSixAward(BigDecimal markSixAward) {
		this.markSixAward = markSixAward;
	}

	public BigDecimal getRealTotal() {
		return realTotal;
	}

	public void setRealTotal(BigDecimal realTotal) {
		this.realTotal = realTotal;
	}

	public BigDecimal getRealAward() {
		return realAward;
	}

	public void setRealAward(BigDecimal realAward) {
		this.realAward = realAward;
	}

	public BigDecimal getEgameTotal() {
		return egameTotal;
	}

	public void setEgameTotal(BigDecimal egameTotal) {
		this.egameTotal = egameTotal;
	}

	public BigDecimal getEgameAward() {
		return egameAward;
	}

	public void setEgameAward(BigDecimal egameAward) {
		this.egameAward = egameAward;
	}

	public BigDecimal getEgameProfit() {
		return BigDecimalUtil.subtract(egameTotal, egameAward);
	}

	public BigDecimal getChessTotal() {
		return chessTotal;
	}

	public void setChessTotal(BigDecimal chessTotal) {
		this.chessTotal = chessTotal;
	}

	public BigDecimal getChessAward() {
		return chessAward;
	}

	public void setChessAward(BigDecimal chessAward) {
		this.chessAward = chessAward;
	}

	public BigDecimal getWithdrawGiveTotal() {
		return withdrawGiveTotal;
	}

	public void setWithdrawGiveTotal(BigDecimal withdrawGiveTotal) {
		this.withdrawGiveTotal = withdrawGiveTotal;
	}
	
	
}
