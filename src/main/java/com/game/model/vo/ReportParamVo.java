package com.game.model.vo;

import java.util.Date;
import java.util.List;

public class ReportParamVo {
	
	//刷新标志
	public static long REFRESH_FLAG = 1l;
	
	public static long SEARCHTYPE_NEXT = 1;

	public static long SEARCHTYPE_NEXT_ALL = 2;

	private Long stationId;
	private String account;
	private String userName;
	private String agentName;
	private String children;
	private List<Long> types;
	private Long agentId;
	private Boolean searchSelf;
	private Long selfId;
	private Date begin;
	private Date end;
	private Long accountId;
	private String sortName;
	private String sortOrder;
	private String type;
	private Long refresh;
	private String cacheName;
	private Long searchType;
	private String filterAccount;
	private Long reportType;
	private String seachDate;

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}

	public List<Long> getTypes() {
		return types;
	}

	public void setTypes(List<Long> types) {
		this.types = types;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getChildren() {
		return children;
	}

	public void setChildren(String children) {
		this.children = children;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getRefresh() {
		return refresh;
	}

	public void setRefresh(Long refresh) {
		this.refresh = refresh;
	}

	public Long getSearchType() {
		return searchType;
	}

	public void setSearchType(Long searchType) {
		this.searchType = searchType;
	}

	public String getFilterAccount() {
		return filterAccount;
	}

	public void setFilterAccount(String filterAccount) {
		this.filterAccount = filterAccount;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public String getSeachDate() {
		return seachDate;
	}

	public void setSeachDate(String seachDate) {
		this.seachDate = seachDate;
	}
}
