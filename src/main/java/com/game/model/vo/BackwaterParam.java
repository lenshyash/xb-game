package com.game.model.vo;

import java.util.Date;

/**
 * 查询注单 传递条件对象
 * 
 * @author admin
 *
 */
public class BackwaterParam {

	private Long memberId;// 会员id

	private Date begin;// 开始时间

	private Date end;// 结束时间

	private Long status;// 类型
	
	private Long stationId;// 类型

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
}
