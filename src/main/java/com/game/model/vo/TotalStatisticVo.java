package com.game.model.vo;

import java.math.BigDecimal;

import com.game.util.BigDecimalUtil;

public class TotalStatisticVo {
	private Long accountId;// 账户id
	private Long agentId;// 代理Id
	private String account;// 用户账号
	private String agentName;// 代理账号
	private String parents;// 用户层级关系
	private Integer accountType;// 用户类型
	private BigDecimal bettingAmount;// 投注金额
	private BigDecimal awardAmount;// 中奖金额
	private BigDecimal depositAmount;// 存款总计
	private BigDecimal withdrawAmount;// 提款总计
	private BigDecimal rebateAmount;// 返水总计
	private BigDecimal rebateAgentAmount;// 代理返点总计
	private BigDecimal depositGiftAmount;// 赠送总计
	private BigDecimal manualDepositAmount;// 手动加款总计
	private BigDecimal manualWithdrawAmount;// 手动扣款总计
	private BigDecimal balance;// 余额
	// private BigDecimal profit;// 盈亏
	private BigDecimal teamProfit;// 团队盈亏 会员没有团队盈亏

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public BigDecimal getBettingAmount() {
		return bettingAmount;
	}

	public void setBettingAmount(BigDecimal bettingAmount) {
		this.bettingAmount = bettingAmount;
	}

	public BigDecimal getAwardAmount() {
		return awardAmount;
	}

	public void setAwardAmount(BigDecimal awardAmount) {
		this.awardAmount = awardAmount;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public BigDecimal getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(BigDecimal withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public BigDecimal getRebateAmount() {
		return rebateAmount;
	}

	public void setRebateAmount(BigDecimal rebateAmount) {
		this.rebateAmount = rebateAmount;
	}

	public BigDecimal getRebateAgentAmount() {
		return rebateAgentAmount;
	}

	public void setRebateAgentAmount(BigDecimal rebateAgentAmount) {
		this.rebateAgentAmount = rebateAgentAmount;
	}

	public BigDecimal getDepositGiftAmount() {
		return depositGiftAmount;
	}

	public void setDepositGiftAmount(BigDecimal depositGiftAmount) {
		this.depositGiftAmount = depositGiftAmount;
	}

	public BigDecimal getManualDepositAmount() {
		return manualDepositAmount;
	}

	public void setManualDepositAmount(BigDecimal manualDepositAmount) {
		this.manualDepositAmount = manualDepositAmount;
	}

	public BigDecimal getManualWithdrawAmount() {
		return manualWithdrawAmount;
	}

	public void setManualWithdrawAmount(BigDecimal manualWithdrawAmount) {
		this.manualWithdrawAmount = manualWithdrawAmount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/**
	 * 会员盈亏＝有效投注-中奖金额
	 * 
	 * 代理的个人盈亏就是返点
	 * 
	 * 合计和小计算的为 有效投注-中奖金额＋返点
	 * 
	 * @return
	 */
	public BigDecimal getProfit() {
		return BigDecimalUtil.subtract(bettingAmount, awardAmount, rebateAgentAmount);
	}

	public BigDecimal getTeamProfit() {
		return teamProfit;
	}

	public void setTeamProfit(BigDecimal teamProfit) {
		this.teamProfit = teamProfit;
	}

}
