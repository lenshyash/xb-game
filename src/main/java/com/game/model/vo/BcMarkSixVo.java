package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Map;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

public class BcMarkSixVo
{
	//数字号码或者类型(例如：波类，单双，大小，尾数)
	private String markType;
	
	//名称(例如：特码大，特么小)
	private String name;

	//赔率
	private BigDecimal odds;
	
	//六合彩小类code
	private String playCode;
	
	//序号
	private Integer sortNo;
	
	private Map<BigDecimal,BigDecimal> items;

	public String getMarkType() {
		return markType;
	}

	public void setMarkType(String markType) {
		this.markType = markType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getOdds() {
		return odds;
	}

	public void setOdds(BigDecimal odds) {
		this.odds = odds;
	}

	public String getPlayCode() {
		return playCode;
	}

	public void setPlayCode(String playCode) {
		this.playCode = playCode;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public Map<BigDecimal, BigDecimal> getItems() {
		return items;
	}

	public void setItems(Map<BigDecimal, BigDecimal> items) {
		this.items = items;
	}
	
	
	
	
	
	
}
