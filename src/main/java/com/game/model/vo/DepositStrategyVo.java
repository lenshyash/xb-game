package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

public class DepositStrategyVo {

	private Long stationId;

	private Date depositDatetime;

	private Long depositType;

	private Long execution;

	private Long depositCount;

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getDepositDatetime() {
		return depositDatetime;
	}

	public void setDepositDatetime(Date depositDatetime) {
		this.depositDatetime = depositDatetime;
	}

	public Long getDepositType() {
		return depositType;
	}

	public void setDepositType(Long depositType) {
		this.depositType = depositType;
	}

	public Long getExecution() {
		return execution;
	}

	public void setExecution(Long execution) {
		this.execution = execution;
	}

	public Long getDepositCount() {
		return depositCount;
	}

	public void setDepositCount(Long depositCount) {
		this.depositCount = depositCount;
	}
}
