package com.game.model.vo;

import java.math.BigDecimal;

public class WithdrawCheckVo {

	private Long lastComId;

	private BigDecimal lastComMoney;

	public Long getLastComId() {
		return lastComId;
	}

	public void setLastComId(Long lastComId) {
		this.lastComId = lastComId;
	}

	public BigDecimal getLastComMoney() {
		return lastComMoney;
	}

	public void setLastComMoney(BigDecimal lastComMoney) {
		this.lastComMoney = lastComMoney;
	}
}
