package com.game.model.vo;

import java.util.List;

import com.game.model.CommonRedpacket;
import com.game.model.CommonRedpacketRecord;

public class CommonRedpacketVo {

	private CommonRedpacket redpacket;
	private List<CommonRedpacketRecord> records;

	public CommonRedpacket getRedpacket() {
		return redpacket;
	}

	public void setRedpacket(CommonRedpacket redpacket) {
		this.redpacket = redpacket;
	}

	public List<CommonRedpacketRecord> getRecords() {
		return records;
	}

	public void setRecords(List<CommonRedpacketRecord> records) {
		this.records = records;
	}
}
