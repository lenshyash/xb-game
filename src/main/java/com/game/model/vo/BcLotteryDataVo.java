package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

public class BcLotteryDataVo {
	private String id;

	/**
	 * 彩票编码
	 */
	private String lotCode;

	/**
	 * 期号
	 */
	private String qiHao;

	/**
	 * 号码
	 */
	private String haoMa;

	/**
	 * 遗漏
	 */
	private String ommit;
	/**
	 * 开始投注时间
	 */
	private Date startTime;
	/**
	 * 投注结束后，开盘时间
	 */
	private Date endTime;

	/**
	 * 开奖状态{0未开奖,1都开奖了,2未开奖完, 3,已经取消 4，已经回滚}
	 */
	private Integer openStatus;

	/**
	 * 开奖时间
	 */
	private Date openTime;

	/**
	 * 站点ID
	 */
	private Long stationId;
	/**
	 * bc_lottery_time表中的action_no
	 */
	private Integer actionNo;

	/**
	 * 投注状态 1=投注结束 2=未开始投注 3=投注中
	 */
	private Integer tzState;

	/**
	 * 返水金额
	 */
	private BigDecimal rollBackMoney;

	/**
	 * 中奖金额
	 */
	private BigDecimal winMoney;

	/**
	 * 购买金额
	 */
	private BigDecimal buyMoney;

	private BigDecimal sumBuyMoney;
	private BigDecimal sumWinMoney;
	private BigDecimal sumRollMoney;
	
	
	private String limitBetAccount;
	

	public String getLimitBetAccount() {
		return limitBetAccount;
	}

	public void setLimitBetAccount(String limitBetAccount) {
		this.limitBetAccount = limitBetAccount;
	}

	public BigDecimal getRollBackMoney() {
		return rollBackMoney;
	}

	public void setRollBackMoney(BigDecimal rollBackMoney) {
		this.rollBackMoney = rollBackMoney;
	}

	public BigDecimal getWinMoney() {
		return winMoney;
	}

	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}

	public BigDecimal getBuyMoney() {
		return buyMoney;
	}

	public void setBuyMoney(BigDecimal buyMoney) {
		this.buyMoney = buyMoney;
	}

	public Integer getTzState() {
		return tzState;
	}

	public void setTzState(Integer tzState) {
		this.tzState = tzState;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLotCode() {
		return this.lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getQiHao() {
		return this.qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getHaoMa() {
		return this.haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public String getOmmit() {
		return this.ommit;
	}

	public void setOmmit(String ommit) {
		this.ommit = ommit;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getOpenStatus() {
		return this.openStatus;
	}

	public void setOpenStatus(Integer openStatus) {
		this.openStatus = openStatus;
	}

	public Date getOpenTime() {
		return this.openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getActionNo() {
		return actionNo;
	}

	public void setActionNo(Integer actionNo) {
		this.actionNo = actionNo;
	}

	public BigDecimal getSumBuyMoney() {
		return sumBuyMoney;
	}

	public void setSumBuyMoney(BigDecimal sumBuyMoney) {
		this.sumBuyMoney = sumBuyMoney;
	}

	public BigDecimal getSumWinMoney() {
		return sumWinMoney;
	}

	public void setSumWinMoney(BigDecimal sumWinMoney) {
		this.sumWinMoney = sumWinMoney;
	}

	public BigDecimal getSumRollMoney() {
		return sumRollMoney;
	}

	public void setSumRollMoney(BigDecimal sumRollMoney) {
		this.sumRollMoney = sumRollMoney;
	}

}
