package com.game.model.vo;

import java.util.Date;

public class MnyScoreReocrdVo {

	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 类型
	 */
	private Long type;

	private Long stationId;
	
	/**
	 * 下级
	 */
	private String parents;
	
	private Date begin;
	
	private Date end;
	
	private Long selfId;
	
	private Boolean searchSelf;

	public MnyScoreReocrdVo() {
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}
}
