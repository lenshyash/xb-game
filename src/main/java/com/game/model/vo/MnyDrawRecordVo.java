package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

public class MnyDrawRecordVo {

	private Long id;
	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 记录类型
	 */
	private Long type;

	private Long stationId;

	private Date begin;
	private Date end;
	private Long status;
	private Long lockFlag;
	private String parents;
	private Long accountType;
	private String agentName;
	
	private String opDesc;
	// 是否查询自身
	private Boolean searchSelf;
	private Long selfId;
	private Long searchType;

	private BigDecimal fee;
	private BigDecimal minMoney;
	private BigDecimal maxMoney;
	private String operator;
	private String remark;
	private Long drawTimes;
	//报表类型(默认1普通， 2引导)
	private Long reportType;
	private Long payId;

	
	public MnyDrawRecordVo() {
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(Long lockFlag) {
		this.lockFlag = lockFlag;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpDesc() {
		return opDesc;
	}

	public void setOpDesc(String opDesc) {
		this.opDesc = opDesc;
	}

	public Long getSearchType() {
		return searchType;
	}

	public void setSearchType(Long searchType) {
		this.searchType = searchType;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	
	public BigDecimal getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(BigDecimal minMoney) {
		this.minMoney = minMoney;
	}

	public BigDecimal getMaxMoney() {
		return maxMoney;
	}

	public void setMaxMoney(BigDecimal maxMoney) {
		this.maxMoney = maxMoney;
	}
	
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getDrawTimes() {
		return drawTimes;
	}

	public void setDrawTimes(Long drawTimes) {
		this.drawTimes = drawTimes;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}
}
