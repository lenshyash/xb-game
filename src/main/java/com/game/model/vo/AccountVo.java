package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.util.StringUtil;

public class AccountVo {

	public static long SEARCHTYPE_NEXT = 1;

	public static long SEARCHTYPE_NEXT_ALL = 2;
	
	public static long UPDATE_ACCOUNT = 1L;
	
	public static long UPDATE_ACCOUNT_ADVANCED = 2L;
	
	public static long MOVE_FLAG_NO = 1l;
	public static long MOVE_FLAG_YES = 2l;

	// 用记基础信息
	private Long id;
	private String account;
	private Long status;
	private Long accountType;
	private Long stationId;
	private Long agentId;
	private String agentName;
	private String password;
	private Long level;
	private Long levelGroup;
	private Long groupId;
	private String parents;
	private String lastLoginIp;
	private Date lastLoginDatetime;
	private Date startTime;
	private Date endTime;
	private String remark;
	private String regIp ;
	// 用户拓展信息

	private String userName;
	private String parentNames;
	private String cardNo;
	private String receiptPwd;
	private String phone;
	private String province;
	private String city;
	private String email;
	private String bankAddress;
	private String qq;
	private Long sex;
	private Long cardNoStatus;
	private String bankName;
	private String registerUrl;
	private String wechat;
	private String sortOrder;
	private String sortName;
	private Long redpacketLimit;
	private String chatToken;
	
	private Date begin;
	private Date end;
	//是否是群组 1是群组  其他都不是
	private Long isgroup;

	//群组名称
	private String qzGroupName;
	
	private Long agentParentId;

	private Long searchType;

	// 返点数
	private BigDecimal rebateNum;
	
	// 动态赔率点
	private BigDecimal dynamicRate;
	
	//占成
	private BigDecimal profitShare;
	
	private String levelName;

	// 是否查询自身
	private Boolean searchSelf;

	private Long selfId;

	private Long online;
	
	private Long moveFlag;

	private Boolean generalize;

	private Boolean generalSearch;
	
	//报表类型(默认1普通， 2引导)
	private Long reportType;

	// 用户金额
	private BigDecimal money;
	
	private Integer depositStatus;//充值情况 1=有存款，2=从未存款
	private BigDecimal moneyMin;//账户余额大于
	private Integer unloginDay;//最近多少天没登录
	private String registerCode; 
	private Boolean dailiTopSearch;
	
	private Long accountStatus;

	private String levelIcon;
	private Integer[] levelArray;
	private Boolean pageQuerySpecial;
	private Boolean memberPageQuery;
	private Boolean agentPageQuery;

	private Long  abnormalFlag;
	
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	// 密码
	private String pwd;
	// 确认密码
	private String rpwd;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getSex() {
		return sex;
	}

	public void setSex(Long sex) {
		this.sex = sex;
	}

	public Long getAgentId() {
		return agentId;
	}

	public Long getLevelGroup() {
		return levelGroup;
	}

	public void setLevelGroup(Long levelGroup) {
		this.levelGroup = levelGroup;
	}

	public String getParentNames() {
		return parentNames;
	}

	public void setParentNames(String parentNames) {
		this.parentNames = parentNames;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAccount() {
		if (StringUtil.isNotEmpty(account)) {
			return account.trim().toLowerCase();
		}
		return account;
	}

	public void setAccount(String account) {
		if (StringUtil.isNotEmpty(account)) {
			this.account = account.trim().toLowerCase();
		} else {
			this.account = account;
		}
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getReceiptPwd() {
		return receiptPwd;
	}

	public void setReceiptPwd(String receiptPwd) {
		this.receiptPwd = receiptPwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getRpwd() {
		return rpwd;
	}

	public void setRpwd(String rpwd) {
		this.rpwd = rpwd;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Long getAgentParentId() {
		return agentParentId;
	}

	public void setAgentParentId(Long agentParentId) {
		this.agentParentId = agentParentId;
	}

	public BigDecimal getRebateNum() {
		return rebateNum;
	}

	public void setRebateNum(BigDecimal rebateNum) {
		this.rebateNum = rebateNum;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public Long getOnline() {
		return online;
	}

	public void setOnline(Long online) {
		this.online = online;
	}

	public Long getSearchType() {
		return searchType;
	}

	public void setSearchType(Long searchType) {
		this.searchType = searchType;
	}

	public Boolean getGeneralize() {
		return generalize;
	}

	public void setGeneralize(Boolean generalize) {
		this.generalize = generalize;
	}

	public Boolean getGeneralSearch() {
		return generalSearch;
	}

	public void setGeneralSearch(Boolean generalSearch) {
		this.generalSearch = generalSearch;
	}

	public Date getLastLoginDatetime() {
		return lastLoginDatetime;
	}

	public void setLastLoginDatetime(Date lastLoginDatetime) {
		this.lastLoginDatetime = lastLoginDatetime;
	}

	public String getRegisterUrl() {
		return registerUrl;
	}

	public void setRegisterUrl(String registerUrl) {
		this.registerUrl = registerUrl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public Long getCardNoStatus() {
		return cardNoStatus;
	}

	public void setCardNoStatus(Long cardNoStatus) {
		this.cardNoStatus = cardNoStatus;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getRegIp() {
		return regIp;
	}

	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}

	public Integer getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}

	public BigDecimal getMoneyMin() {
		return moneyMin;
	}

	public void setMoneyMin(BigDecimal moneyMin) {
		this.moneyMin = moneyMin;
	}

	public Integer getUnloginDay() {
		return unloginDay;
	}

	public void setUnloginDay(Integer unloginDay) {
		this.unloginDay = unloginDay;
	}

	public Long getMoveFlag() {
		return moveFlag;
	}

	public void setMoveFlag(Long moveFlag) {
		this.moveFlag = moveFlag;
	}

	public String getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getLevelIcon() {
		return levelIcon;
	}

	public void setLevelIcon(String levelIcon) {
		this.levelIcon = levelIcon;
	}

	public Long getRedpacketLimit() {
		return redpacketLimit;
	}

	public void setRedpacketLimit(Long redpacketLimit) {
		this.redpacketLimit = redpacketLimit;
	}

	public BigDecimal getProfitShare() {
		return profitShare;
	}

	public void setProfitShare(BigDecimal profitShare) {
		this.profitShare = profitShare;
	}

	public String getChatToken() {
		return chatToken;
	}

	public void setChatToken(String chatToken) {
		this.chatToken = chatToken;
	}

	public BigDecimal getDynamicRate() {
		return dynamicRate;
	}

	public void setDynamicRate(BigDecimal dynamicRate) {
		this.dynamicRate = dynamicRate;
	}

	public Boolean getDailiTopSearch() {
		return dailiTopSearch;
	}

	public void setDailiTopSearch(Boolean dailiTopSearch) {
		this.dailiTopSearch = dailiTopSearch;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public Long getIsgroup() {
		return isgroup;
	}

	public void setIsgroup(Long isgroup) {
		this.isgroup = isgroup;
	}
	
	public String getQzGroupName() {
		return qzGroupName;
	}

	public void setQzGroupName(String qzGroupName) {
		this.qzGroupName = qzGroupName;
	}

	public Long getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(Long accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Integer[] getLevelArray() {
		return levelArray;
	}

	public void setLevelArray(Integer[] levelArray) {
		this.levelArray = levelArray;
	}

	public Boolean getPageQuerySpecial() {
		return pageQuerySpecial;
	}

	public void setPageQuerySpecial(Boolean pageQuerySpecial) {
		this.pageQuerySpecial = pageQuerySpecial;
	}

	public Boolean getMemberPageQuery() {
		return memberPageQuery;
	}

	public void setMemberPageQuery(Boolean memberPageQuery) {
		this.memberPageQuery = memberPageQuery;
	}

	public Boolean getAgentPageQuery() {
		return agentPageQuery;
	}

	public void setAgentPageQuery(Boolean agentPageQuery) {
		this.agentPageQuery = agentPageQuery;
	}

	public Long getAbnormalFlag() {
		return abnormalFlag;
	}

	public void setAbnormalFlag(Long abnormalFlag) {
		this.abnormalFlag = abnormalFlag;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	
}
